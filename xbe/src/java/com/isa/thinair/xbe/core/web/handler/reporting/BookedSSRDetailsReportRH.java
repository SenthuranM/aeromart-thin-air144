package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Request handler for Booked SSR Details/Summary reports.
 * 
 * @author M.Rikaz
 * @since 22/09/2011
 * 
 */
public class BookedSSRDetailsReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(BookedSSRDetailsReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static final String NOREC_STATUS = "NOREC";
	private static final String FLOWN_STATUS = "FLOWN";
	private static final String NOSHOW_STATUS = "NOSHOW";
	private static final String CONFIRM_STATUS = "CONFIRM";
	private static final String GOSHOW_STATUS = "GOSHOW";

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportViewAll(request, response);
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("BookedHalaServicesReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			forward = S2Constants.Result.ERROR;
			log.error("Error in BookedHalaServicesReportRH execute()" + e.getMessage());
		}
		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setStationComboList(request);
		setReportingPeriod(request);
		setSSRCodeList(request);
		setServiceCategory(request);
		setSSRCodesWithCategory(request);
		setPaxStatus(request);

		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setSSRCodesWithCategory(HttpServletRequest request) throws ModuleException {

		List list = SelectListGenerator.createSSRCodeListWithCategory();

		Iterator itr = list.iterator();
		StringBuilder sb = new StringBuilder();
		while (itr.hasNext()) {
			Map codeMap = (Map) itr.next();

			sb.append(codeMap.get("SSR_CODE"));
			sb.append("|");
			sb.append(codeMap.get("SSR_CAT_ID"));
			sb.append(",");
		}

		request.setAttribute(WebConstants.REQ_SSR_CODE_WITH_CATEGORY, sb.toString());
	}

	private static void setServiceCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createSsrCategoryList();
		request.setAttribute(WebConstants.REQ_SERVICE_CATEGORY_LIST, strHtml);
	}

	private static String createPFSStatusList() throws ModuleException {

		StringBuilder jsb = new StringBuilder("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var pfsStatus = new Array();");

		Map<String, String> paxStatus = new HashMap<String, String>();
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.FLOWN, FLOWN_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.CONFIRMED, CONFIRM_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.NO_SHORE, NOSHOW_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.NO_REC, NOREC_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.GO_SHORE, GOSHOW_STATUS);

		int temp = 0;
		if (paxStatus != null && !paxStatus.isEmpty()) {
			for (Entry<String, String> entry : paxStatus.entrySet()) {
				jsb.append("pfsStatus[" + temp + "] = new Array('" + entry.getValue() + "','" + entry.getKey() + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = pfsStatus; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var pfsStatuss = new Listbox('lstPFSStatus', 'lstSelectedPFSStatus', 'spnPFSStatus','pfsStatuss');");
		jsb.append("pfsStatuss.group1 = arrData[0];" + "pfsStatuss.list2 = arrData2;");
		jsb.append("pfsStatuss.headingLeft = '&nbsp;&nbsp;PFS Status';");
		jsb.append("pfsStatuss.headingRight = '&nbsp;&nbsp;Selected PFS Status';");

		return jsb.toString();

	}

	private static String createSSRCodesHtml() throws ModuleException {
		StringBuilder jsb = new StringBuilder("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var ssr = new Array();");

		List lstSsr = SelectListGenerator.createSSRCodeList();
		Iterator iteBc = lstSsr.iterator();
		int temp = 0;
		if (lstSsr != null && !lstSsr.isEmpty()) {
			while (iteBc.hasNext()) {
				Map keyValues = (Map) iteBc.next();
				String code = (String) keyValues.get("SSR_CODE");
				jsb.append("ssr[" + temp + "] = new Array('" + code + "','" + code + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = ssr; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var ssrs = new Listbox('lstSsr', 'lstSelectedSsr', 'spnSSR','ssrs');");
		jsb.append("ssrs.group1 = arrData[0];" + "ssrs.list2 = arrData2;");
		jsb.append("ssrs.headingLeft = '&nbsp;&nbsp;SSR Codes';");
		jsb.append("ssrs.headingRight = '&nbsp;&nbsp;Selected SSR Codes';");

		return jsb.toString();
	}

	private static void setSSRCodeList(HttpServletRequest request) throws ModuleException {
		String strHtml = createSSRCodesHtml();
		request.setAttribute(WebConstants.REQ_HTML_SSR_CODE_LIST, strHtml);
	}

	private static void setPaxStatus(HttpServletRequest request) throws ModuleException {
		String strHtml = createPFSStatusList();
		request.setAttribute(WebConstants.REQ_PFS_STATUS_LIST, strHtml);
	}

	/**
	 * Sets the values list to populate from and to airports lists
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createAirportsList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the initial reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	protected static void setReportViewAll(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		try {

			String fromDate = request.getParameter("txtFromDate");
			String toDate = request.getParameter("txtToDate");

			String bookedFromDate = request.getParameter("txtBookedFromDate");
			String bookedToDate = request.getParameter("txtBookedToDate");

			String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

			String flightNo = request.getParameter("txtFlightNumber");
			String ssrCodes = request.getParameter("hdnSsrCodes");

			String segments = request.getParameter("hdnSegments");

			String serviceCategory = request.getParameter("selCategory");
			String reportType = request.getParameter("selReportType");

			String strReportFormat = request.getParameter("radRptNumFormat");

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			String pfsStatusList = request.getParameter("hdnPfsStatus");

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}

			if ((bookedFromDate != null && !bookedFromDate.isEmpty()) && (bookedToDate != null && !bookedToDate.isEmpty())) {
				search.setDateRange2From(ReportsHTMLGenerator.convertDate(bookedFromDate) + " 00:00:00");
				search.setDateRange2To(ReportsHTMLGenerator.convertDate(bookedToDate) + " 23:59:59");
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo);
			}
			if (ssrCodes != null && !ssrCodes.equals("")) {
				String ssrCodesArr[] = ssrCodes.split(",");
				ArrayList<String> ssrCodesCol = new ArrayList<String>();
				for (String ssr : ssrCodesArr) {
					ssrCodesCol.add(ssr);
				}
				search.setSsrCodes(ssrCodesCol);
			}

			if (pfsStatusList != null && !pfsStatusList.equals("")) {
				String pfsStatusArr[] = pfsStatusList.split(",");
				ArrayList<String> pfsStatusCol = new ArrayList<String>();
				for (String pfsStatus : pfsStatusArr) {
					pfsStatusCol.add(pfsStatus);
				}
				search.setPaxStatusList(pfsStatusCol);
			}
			// AAN/ALA,AIR/ADE/ATZ/AMS
			if (segments != null && !segments.equals("")) {
				String segmentsArr[] = segments.split(",");
				ArrayList<String> segmentsCol = new ArrayList<String>();
				for (String seg : segmentsArr) {
					segmentsCol.add(seg);
				}
				// need to add new param if required
				search.setSegmentCodes(segmentsCol);
			}

			if (serviceCategory != null && !serviceCategory.equals("")) {
				search.setSsrCategory(serviceCategory);
			}
			boolean isIncludeCancelledPnr = ModuleServiceLocator.getGlobalConfig().isIncludeCancelledSSRDetailsInReport();
			search.setIncludeCancelledDataToReport(isIncludeCancelledPnr);
			ResultSet rs = null;
			if (reportType != null && reportType.equals(ReportsSearchCriteria.SSR_DETAIL_REPORT)) {
				rs = ModuleServiceLocator.getDataExtractionBD().getBookedSSRDetailDataforReports(search);
			} else {
				rs = ModuleServiceLocator.getDataExtractionBD().getBookedSSRSummaryDataforReports(search);
			}

			viewJasperReport(request, response, rs, search, fromDate, toDate);

		} catch (Exception e) {
			log.error("download method is failed :", e);
			throw new ModuleException("Error in report data retrieval");
		}
	}

	private static void viewJasperReport(HttpServletRequest request, HttpServletResponse response, ResultSet resultSet,
			ReportsSearchCriteria search, String fromDate, String toDate) throws ModuleException {
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		String strReportId = "UC_REPM_026";
		String reportTemplate = null;
		String reportName = null;
		String serviceCategory = request.getParameter("selCategory");
		String reportType = request.getParameter("selReportType");

		if (reportType != null && reportType.equals(ReportsSearchCriteria.SSR_DETAIL_REPORT)) {

			reportTemplate = "BookedSSRDetailsReport.jasper";

			if (serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.SSR_SERVICE_CAT)) {
				reportName = "BookedSSRDetails";
			}
			if (serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.AIRPORT_TRANSFER_CAT)) {
				reportName = "BookedAirportTransferDetails";
			} else {
				reportName = "BookedAirportServicesDetails";
			}

		} else {
			reportTemplate = "BookedSSRSummaryReport.jasper";

			if (serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.SSR_SERVICE_CAT)) {
				reportName = "BookedSSRSummary";
			}
			if (serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.AIRPORT_TRANSFER_CAT)) {
				reportName = "BookedAirportTransferSummary";
			} else {
				reportName = "BookedAirportServicesSummary";
			}
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_ID", strReportId);
		parameters.put("FROM_DATE", fromDate);
		parameters.put("SSR_CAT", serviceCategory);
		parameters.put("TO_DATE", toDate);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("CURRENCY", "(" + strBase + ")");

		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals(WebConstants.REPORT_HTML)) {
			parameters.put("IMG", strPath);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet, null,
					null, response);
		} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
			strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", strPath);
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.pdf", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.csv", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.xls", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		}
	}

}
