package com.isa.thinair.xbe.core.web.action.user;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PUBLIC)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = ""),
	@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class XbeUserLanguagesAction extends BaseRequestAwareAction {
	
	private static Log log = LogFactory.getLog(XbeUserLanguagesAction.class);

	private Map<String, String> suppotedLanguages;
	
	public String execute() {

		String forward = S2Constants.Result.SUCCESS;

		try {

			suppotedLanguages = AppSysParamsUtil.getXBESupportedLanguages();
			

		} catch (RuntimeException re) {

			forward = S2Constants.Result.ERROR;
			String msg = BasicRH.getErrorMessage(re, WebConstants.DEFAULT_LANGUAGE);
			log.error(msg, re);
			JavaScriptGenerator.setServerError(request, msg, "top", "");
		}

		return forward;

	}

	public Map<String, String> getSuppotedLanguages() {
		return suppotedLanguages;
	}
	

	public void setSuppotedLanguages(Map<String, String> suppotedLanguages) {
		this.suppotedLanguages = suppotedLanguages;
	}
	
	
}

