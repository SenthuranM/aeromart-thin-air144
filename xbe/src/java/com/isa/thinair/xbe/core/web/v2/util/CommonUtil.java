package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.generator.reservation.ReservationHG;

public class CommonUtil {

	private static Log log = LogFactory.getLog(CommonUtil.class);

	public static boolean enableCurrencyRounding(String strCurrencyCode) throws ModuleException {
		boolean enable = false;

		try {
			Currency curreny = ModuleServiceLocator.getCommonServiceBD().getCurrency(strCurrencyCode);
			if (curreny.getBoundryValue() != null && curreny.getBreakPoint() != null) {
				enable = true;
			}
		} catch (Exception e) {
			// this should not stop the search return false;
			// TODO: handle exception
		}

		return enable;
	}

	public static String getLoggedInAgentBalance(HttpServletRequest request) {
		try {
			return ReservationHG.getUsersAvailableCredit(request);
		} catch (Exception e) {
			log.error("Loading agent balance failed!", e);
		}
		return null;
	}
	
	public static String getLoggedInAgentBalanceInAgentCurrency(HttpServletRequest request) {
		try {
			return ReservationHG.getUserAvailableCreditInUserCurrency(request, AppSysParamsUtil.getBaseCurrency());
		} catch (Exception e) {
			log.error("Loading agent balance failed!", e);
		}
		return null;
	}

	public static List<Calendar> valiedPeriod(Calendar depStart, Calendar depEnd, Calendar validFrom, Calendar validTo) {
		List<Calendar> period = new ArrayList<Calendar>();
		if (validFrom.before(depStart) && (validTo.before(depStart) || validTo.equals(depStart))) {
			period.add(depStart);
			period.add(depStart);
		} else if (validFrom.before(depStart) && validTo.after(depStart)) {
			period.add(depStart);
			if (validTo.before(depEnd)) {
				period.add(validTo);
			} else {
				period.add(depEnd);
			}
		} else if ((validFrom.after(depStart) || validFrom.equals(depStart)) && validTo.before(depEnd)) {
			period.add(validFrom);
			period.add(validTo);
		} else if (validFrom.after(depStart) && validFrom.before(depEnd) && (validTo.after(depEnd) || validTo.equals(depEnd))) {
			period.add(validFrom);
			period.add(depEnd);
		} else if (validFrom.after(depStart) && (validFrom.after(depEnd) || validFrom.equals(depEnd)) && validTo.after(depEnd)) {
			period.add(depEnd);
			period.add(depEnd);
		} else {
			period.add(depStart);
			period.add(depEnd);
		}

		return period;
	}
	
	public static Map<Integer, BigDecimal> getPaxWisePromoDiscount(BookingShoppingCart bookingShoppingCart) {
		Map<Integer, BigDecimal> paxWisePromoDiscount = new HashMap<Integer, BigDecimal>();
		DiscountedFareDetails discountedFareDetails = bookingShoppingCart.getFareDiscount();
		String promotionType = discountedFareDetails.getPromotionType();
		String discountType = discountedFareDetails.getDiscountType();
		Collection<ReservationPaxTO> colPax = bookingShoppingCart.getPaxList();
		List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>(colPax);
		Collections.sort(paxList);
		PriceInfoTO priceInfoTO = bookingShoppingCart.getPriceInfoTO();
		int ondCount = priceInfoTO.getFareTypeTO().getOndWiseFareType().keySet().size();

		int paxCount = 0;
		if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promotionType)) {
			paxCount = bookingShoppingCart.getPayablePaxCount();
		} else {
			for (ReservationPaxTO reservationPaxTO : paxList) {
				if (reservationPaxTO.getIsParent()) {
					paxCount += 2;
				} else {
					paxCount++;
				}
			}
		}

		BigDecimal[] discAmountArr = PromotionsUtils.getEffectivePaxDiscountValueWithoutLoss(
				discountedFareDetails.getFarePercentage(), discountedFareDetails.getDiscountApplyTo(),
				discountedFareDetails.getDiscountType(), paxCount, discountedFareDetails.getApplicableOnds().size());

		BigDecimal[] effectiveDiscAmountArr = null;
		if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
			effectiveDiscAmountArr = PromotionsUtils.getPaxWiseTotalDiscount(discAmountArr, paxCount);
		} else {
			effectiveDiscAmountArr = discAmountArr;
		}

		float effectiveDiscountValue;

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promotionType)) {
			List<String> applicableAncis = discountedFareDetails.getApplicableAncillaries();
			if (applicableAncis != null && !applicableAncis.isEmpty()) {
				BigDecimal totalAnciDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				int count = 0;
				for (ReservationPaxTO reservationPaxTO : paxList) {
					BigDecimal paxAnciTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal paxAnciDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

					List<LCCClientExternalChgDTO> externalCharges = reservationPaxTO.getExternalCharges();
					if (externalCharges != null && !externalCharges.isEmpty()) {
						for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalCharges) {
							if (applicableAncis.contains(lccClientExternalChgDTO.getExternalCharges().toString())
									&& lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.INSURANCE) {
								paxAnciTotalAmount = AccelAeroCalculator.add(paxAnciTotalAmount,
										lccClientExternalChgDTO.getAmount());
							} else if (applicableAncis.contains(lccClientExternalChgDTO.getExternalCharges().toString())
									&& StringUtil.isMatchingStringFound(discountedFareDetails.getApplicableOnds(),
											FlightRefNumberUtil.getSegmentCodeFromFlightRPH(lccClientExternalChgDTO
													.getFlightRefNumber()))) {
								paxAnciTotalAmount = AccelAeroCalculator.add(paxAnciTotalAmount,
										lccClientExternalChgDTO.getAmount());
							}
						}

						if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
							effectiveDiscountValue = effectiveDiscAmountArr[0].floatValue();
							paxAnciDiscount = AccelAeroCalculator.calculatePercentage(paxAnciTotalAmount, effectiveDiscountValue,
									RoundingMode.UP);
						} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
							BigDecimal offeredDiscount = effectiveDiscAmountArr[count];
							if (AccelAeroCalculator.isGreaterThan(offeredDiscount, paxAnciTotalAmount)) {
								paxAnciDiscount = paxAnciTotalAmount;
							} else {
								paxAnciDiscount = offeredDiscount;
							}
						}

						paxWisePromoDiscount.put(reservationPaxTO.getSeqNumber(), paxAnciDiscount);
						totalAnciDiscount = AccelAeroCalculator.add(totalAnciDiscount, paxAnciDiscount);
					}

					count++;
				}
				bookingShoppingCart.setFareDiscountAmount(totalAnciDiscount);
			}

		} else {

			int adultItr = 0, childItr = 0, infantItr = 0;
			BigDecimal adultDiscount = null, childDiscount = null, infantDiscount = null;
			Map<String, Integer> applicablePaxCount = null;

			if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(promotionType)) {
				applicablePaxCount = discountedFareDetails.getApplicablePaxCount();
			} else if (PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT.equals(promotionType)) {
				applicablePaxCount = new HashMap<String, Integer>();
				applicablePaxCount.put(PaxTypeTO.ADULT, 0);
				applicablePaxCount.put(PaxTypeTO.CHILD, 0);
				applicablePaxCount.put(PaxTypeTO.INFANT, null);
				for (ReservationPaxTO reservationPaxTO : paxList) {
					if (PaxTypeTO.ADULT.equals(reservationPaxTO.getPaxType())) {
						applicablePaxCount.put(PaxTypeTO.ADULT, applicablePaxCount.get(PaxTypeTO.ADULT) + 1);
					} else if (PaxTypeTO.CHILD.equals(reservationPaxTO.getPaxType())) {
						applicablePaxCount.put(PaxTypeTO.CHILD, applicablePaxCount.get(PaxTypeTO.CHILD) + 1);
					} else if (PaxTypeTO.INFANT.equals(reservationPaxTO.getPaxType())) {
						if (applicablePaxCount.get(PaxTypeTO.INFANT) == null) {
							applicablePaxCount.put(PaxTypeTO.INFANT, 0);
						}
						applicablePaxCount.put(PaxTypeTO.INFANT, applicablePaxCount.get(PaxTypeTO.INFANT) + 1);
					}
				}
			}

			int count = 0;
			int infCount = paxList.size();
			for (ReservationPaxTO reservationPaxTO : paxList) {

				if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
					effectiveDiscountValue = effectiveDiscAmountArr[0].floatValue();
				} else {
					effectiveDiscountValue = effectiveDiscAmountArr[count].floatValue();
				}

				if (PaxTypeTO.ADULT.equals(reservationPaxTO.getPaxType())) {
					BigDecimal discountAmount = null;

					if (adultItr < applicablePaxCount.get(PaxTypeTO.ADULT)) {
						adultDiscount = priceInfoTO.getPerPaxDiscountAmount(reservationPaxTO.getPaxType(),
								effectiveDiscountValue, discountedFareDetails.getDiscountType(),
								discountedFareDetails.getDiscountApplyTo(), discountedFareDetails.getApplicableOnds());
						discountAmount = adultDiscount;
						adultItr++;
					}

					if (reservationPaxTO.getIsParent()) {
						float infEffectiveDiscAmount = 0;
						if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
							infEffectiveDiscAmount = effectiveDiscAmountArr[0].floatValue();
						} else {
							infEffectiveDiscAmount = effectiveDiscAmountArr[infCount].floatValue();
						}

						if ((applicablePaxCount.get(PaxTypeTO.INFANT) == null || (infantItr < applicablePaxCount
								.get(PaxTypeTO.INFANT)))) {
							infantDiscount = priceInfoTO.getPerPaxDiscountAmount(PaxTypeTO.INFANT, infEffectiveDiscAmount,
									discountedFareDetails.getDiscountType(), discountedFareDetails.getDiscountApplyTo(),
									discountedFareDetails.getApplicableOnds());
							discountAmount = (discountAmount == null) ? infantDiscount : AccelAeroCalculator.add(infantDiscount,
									discountAmount);
							infantItr++;
						}

						infCount++;
					}

					paxWisePromoDiscount.put(reservationPaxTO.getSeqNumber(), discountAmount);

				} else if (PaxTypeTO.CHILD.equals(reservationPaxTO.getPaxType())) {
					if (childItr < applicablePaxCount.get(PaxTypeTO.CHILD)) {
						childDiscount = priceInfoTO.getPerPaxDiscountAmount(reservationPaxTO.getPaxType(),
								effectiveDiscountValue, discountedFareDetails.getDiscountType(),
								discountedFareDetails.getDiscountApplyTo(), discountedFareDetails.getApplicableOnds());
						paxWisePromoDiscount.put(reservationPaxTO.getSeqNumber(), childDiscount);
						childItr++;
					}
				}

				count++;
			}
		}

		return paxWisePromoDiscount;
	}

	public static BigDecimal getFareDiscountAmount(PriceInfoTO priceInfoTO, String paxType, float fareDiscPerc,
			boolean isDomFareDiscApplied, int domFareDiscPerc, ArrayList<String> domesticSegmentCodeList) {
		BigDecimal fareDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		List<String> discountEnabledPaxType = AppSysParamsUtil.fareDiscountEnabledPaxTypes();

		if (discountEnabledPaxType != null && discountEnabledPaxType.contains(paxType)) {
			BigDecimal adultFareDisc = priceInfoTO.getPerPaxDiscountFareAmount(paxType, fareDiscPerc);
			BigDecimal adultDomFareDisc = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (isDomFareDiscApplied) {
				adultDomFareDisc = priceInfoTO.getPerPaxDomesticDiscountFareAmount(paxType, domFareDiscPerc,
						domesticSegmentCodeList);
				adultFareDisc = AccelAeroCalculator.add(adultFareDisc, adultDomFareDisc);
			}
			fareDiscountAmount = adultFareDisc;
		}

		return fareDiscountAmount;
	}

	public static String getPendingAgentCommissions(String agentCode, String pnr) throws ModuleException {
		BigDecimal pendingCommissions = null;
		if (agentCode != null && !agentCode.isEmpty() && pnr != null && !pnr.isEmpty()) {
			pendingCommissions = ModuleServiceLocator.getTravelAgentFinanceBD().getPendingAgentCommissions(agentCode, pnr);
		}
		pendingCommissions = (pendingCommissions == null) ? AccelAeroCalculator.getDefaultBigDecimalZero() : pendingCommissions;

		return AccelAeroCalculator.formatAsDecimal(pendingCommissions);
	}
}
