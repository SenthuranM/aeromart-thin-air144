/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCCPaymentMetaInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.RefundInfoTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ChargeAdjustmentTypeUtil;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.dto.ReservationConstants;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.facade.CardPaymentFacade;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ParameterUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentHolder;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class to handle passenger refunds.
 * 
 * @author Abheek Das Gupta
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class PaxAccountPaymentConfirmAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(PaxAccountPaymentConfirmAction.class);

	/** Operation success status indicator */
	private boolean success = true;

	/** Success/Failure message for UI display */
	private String messageTxt;

	/** Passenger refund amount */
	private String txtPayAmount;

	/** Selected passenger traveler reference number */
	private String selPax;

	/** Selected pax's payments tnx id To be used for refund */
	private String payRef;

	/** User notes for refund */
	private String txtPayUN;

	private String selAgent; // {CarrierCode-AgentCode} Empty when refund to own account

	/** Mode of refund */
	private String radPaymentType;

	/** Credit card type */
	private String selCardType;

	private String selAuthId;

	/** Credit card holder name */
	private String cardHoldersName;

	/** Credit card number */
	private String cardNo;

	/** Credit card expiry date */
	private String cardExpiryDate;

	/** Credit card security code */
	private String cardCVV;

	private String cccdId;

	private PaymentTO payment;

	private CreditCardTO creditInfo;

	private String agentBalance;

	private String groupPNR;

	private String pnr;

	private boolean convertedToGroupReservation;

	/** User selected currency */
	private String hdnSelCurrency;

	/** Payment Gateway ID */
	private String hdnIPGId;

	private String tempPayId;

	/** Payment mode */
	private String hdnMode;

	private String version;

	private String ownerAgentCode;

	private String lastUserNote;

	private String refundTypeOrder;

	private String txtPayMode;

	private String pmtdate;

	private Date payDateTime;

	private String carrierVisePayments; // used for refund

	private String lccUniqueTnxId; // selected payments lccUniqueId used for refund

	private String paymentCarrier; // Carrier which did the payment. used for refund

	private String taxRefundedSegment;

	private List<Long> pnrPaxOndChgIDs;

	private boolean doAdjustmentOnly;

	private String isFullRefundOperation;

	private boolean actualPayment;

	private boolean removeAgentCommission = true;

	private BigDecimal totalAmount;

	private GroupBookingRequestTO groupBookingRequest;

	private String groupBookingReference;

	private boolean isOriginallyGroupPNR;

	private boolean nonRefundable;

	private boolean isOhdResExpired;

	private String agentBalanceInAgentCurrency;

	public String execute() {
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		setOriginallyGroupPNR(isGroupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		try {
			BookingShoppingCart bookingShopingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			IPGResponseDTO externalPGWResponseDTO = (IPGResponseDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE);

			String resContactInfo = request.getParameter("resContactInfo");
			CommonReservationContactInfo contactInfo = ReservationUtil.transformJsonContactInfo(resContactInfo);

			// set tracking info
			TrackInfoDTO trackInfoDTO = this.getTrackInfo();

			if (!isGroupPNR) {
				this.groupPNR = pnr;
			}
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					&& resInfo.getExistingReleaseTimestamp().before(CalendarUtil.getCurrentZuluDateTime())
					&& AppSysParamsUtil.isCancelExpiredOhdResEnable()) {
				this.success = false;
				this.messageTxt = "Your on hold reservation has expired. Please reload";
				this.isOhdResExpired = true;
				return S2Constants.Result.SUCCESS;
			}

			if (hdnMode != null && hdnMode.equals(WebConstants.COMMAND_REFUND)) {
				boolean isTaxRefundForNoShow = (taxRefundedSegment != null && !taxRefundedSegment.isEmpty());

				validate(resInfo.getAvailableBalance(), resInfo.getOriginAgentCode(), isTaxRefundForNoShow);
				String resPax = request.getParameter("resPaxs");
				Collection<LCCClientReservationPax> colPaxes = ReservationUtil.transformJsonPassengers(resPax);
				LCCClientReservationPax reservationPax = getReservationPaxForSelectedPax(colPaxes);
				if (reservationPax != null) {
					BigDecimal refundAmount = new BigDecimal(txtPayAmount);
					BigDecimal actualCredit = reservationPax.getTotalActualCredit();
					if (actualCredit != null && actualCredit.doubleValue() <= 0
							&& Math.ceil(refundAmount.doubleValue()) > Math.ceil(actualCredit.abs().doubleValue())) {
						throw new ModuleException("airreservation.modify.refund.exceeds.actual.credit");
					}
					Date paymentdate = new Date();
					try {
						if (payDateTime != null) {
							paymentdate = payDateTime;
						} else {
							SimpleDateFormat smdf = new SimpleDateFormat("dd-MM-yyyy");
							paymentdate = smdf.parse(pmtdate);
						}
					} catch (ParseException e) {

					}

					ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(paymentdate);
					CommonReservationAssembler reservationAssembler = null;
					if (Boolean.valueOf(getIsFullRefundOperation())) {
						reservationAssembler = getReservationAssembler(groupPNR, colPaxes, exchangeRateProxy);
					} else {

						RefundInfoTO refundInfo = new RefundInfoTO(payRef, lccUniqueTnxId, paymentdate, carrierVisePayments,
								paymentCarrier);
						reservationAssembler = getReservationAssembler(groupPNR, refundAmount, exchangeRateProxy, reservationPax,
								refundInfo);
					}

					reservationAssembler.getLccreservation().setVersion(version);
					reservationAssembler.addContactInfo(lastUserNote, contactInfo, null);

					/*
					 * Get the refund order from the front end and send. Sending a list to support future need to choose
					 * refund order on normal refund flow. Only the front end is left to be completed for that now.
					 */
					Collection<String> refundTypeOrderList = null;
					if (!StringUtil.isNullOrEmpty(refundTypeOrder) && !refundTypeOrder.equalsIgnoreCase("null")) {
						refundTypeOrderList = Arrays.asList(refundTypeOrder);
					}

					// Skip refund if only adjustment is selected.
					if (!doAdjustmentOnly) {
						String txtPayUNNew = txtPayUN.replaceAll("\\r|\\n", " ");
						success = ModuleServiceLocator.getAirproxyPassengerBD().refundPassenger(reservationAssembler, txtPayUN,
								isGroupPNR, trackInfoDTO, refundTypeOrderList, pnrPaxOndChgIDs, removeAgentCommission, true);
					}

					/*
					 * If refund portion of automatic tax refund is successful then we need to do a automated charge
					 * adjustment. because thats how air arabia wants it. So do the related charge adjustment if the app
					 * parameter is set and its a TAX refund. Also if the doAdjustmentOnly is selected then it's not
					 * necessary for success to be true.
					 */

					if ((success || doAdjustmentOnly) && refundTypeOrderList != null
							&& AppSysParamsUtil.doAdjustmentForChargeRefund()) {
						List<LCCClientCarrierOndGroup> colONDs = ReservationUtil
								.transformJsonONDs(request.getParameter("resONDs"));

						LCCClientChargeAdustment chargeAdjustment = new LCCClientChargeAdustment();
						chargeAdjustment.setAmount(refundAmount.negate());

						for (LCCClientCarrierOndGroup carrierONDGroup : colONDs) {
							if (taxRefundedSegment.equals(carrierONDGroup.getCarrierOndGroupRPH())) {
								chargeAdjustment.setCarrierCode(carrierONDGroup.getCarrierCode());
								chargeAdjustment.setCarrierOndGroupRPH(carrierONDGroup.getCarrierOndGroupRPH());
								chargeAdjustment.setSegmentCode(carrierONDGroup.getSegmentCode());
							}
						}

						// Front end constraint to make this true for negative amounts
						chargeAdjustment.setRefundable(true);
						chargeAdjustment.setTravelerRefNumber(selPax);

						chargeAdjustment.setUserNote("Automated Adjustment For Tax Refund.");

						chargeAdjustment.setAdjustmentTypeId(
								ChargeAdjustmentTypeUtil.getChargeAdjustTypeForTaxRefund().getChargeAdjustmentTypeId());

						// If refund is skipped no need for the new version.
						if (!doAdjustmentOnly) {
							version = ReservationUtil.loadMinimumReservation(pnr, isGroupPNR, trackInfoDTO).getVersion();
						}

						LCCClientReservation reservation = ModuleServiceLocator.getAirproxyReservationBD().adjustGroupCharge(
								groupPNR, version, Arrays.asList(chargeAdjustment), isGroupPNR, getClientInfoDTO(),
								getTrackInfo(), null, null);

						version = reservation.getVersion();
					}

					if (success) {
						messageTxt = "Refund Successfull";
						agentBalance = CommonUtil.getLoggedInAgentBalance(request);
						agentBalanceInAgentCurrency = CommonUtil.getLoggedInAgentBalanceInAgentCurrency(request);
					} else {
						messageTxt = "Refund Failed";
					}
				} else {
					success = false;
					messageTxt = "Selected passenger does not belong to the reservation.";
				}
			} else {
				ReservationProcessParams resProcessParams = new ReservationProcessParams(request,
						resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
				PaymentMethod paymentMethod = payment.getPaymentMethod();
				if ((payment.getType() == null)
						|| (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode()) && !resProcessParams.isCashPayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())
								&& !resProcessParams.isOnAccountPayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())
								&& !resProcessParams.isOfflinePayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode()) && !resProcessParams.isCreditPayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode()) && !resProcessParams.isBspPayments())) {
					throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
				}
				Integer ipgId = null;
				if (payment.getPaymentMethod().getCode().equals(PaymentMethod.CRED.getCode())) {
					ipgId = new Integer(hdnIPGId);
				}
				if (payment.getPaymentMethod().getCode().equals(PaymentMethod.OFFLINE.getCode())) {
					if (hdnIPGId != null) {
						ipgId = new Integer(hdnIPGId);
					} else {
						Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();
						ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(exchangeRateByDate);
						Object[] pgObj = CardPaymentFacade.getPaymentGatewayValues(payRef, exchangeRateProxy, true);
						ipgId = ((Integer) pgObj[0]);
					}
				}

				// transform the pnr passengers
				Collection<LCCClientReservationPax> colPaxes = ReservationUtil
						.transformJsonPassengers(request.getParameter("resPax"));
				Set<LCCClientReservationPax> passengers = new HashSet<LCCClientReservationPax>(colPaxes);

				// transform the pnr segments
				Collection<LCCClientReservationSegment> segments = ModifyReservationJSONUtil
						.transformJsonSegments(request.getParameter("pnrSegments"));

				this.validate(segments);

				if (groupBookingReference != null && groupBookingReference.length() > 0) {

					long requestID = Long.parseLong(groupBookingReference);

					int adultCount = 0, childCount = 0, infantCount = 0;
					BigDecimal airFare = AccelAeroCalculator.getDefaultBigDecimalZero();
					for (LCCClientReservationPax reservationPax : colPaxes) {

						if (reservationPax.getPaxType().equals("AD")) {
							adultCount++;
						} else if (reservationPax.getPaxType().equals("CH")) {
							childCount++;
						} else {
							infantCount++;
						}
						airFare = airFare.add(reservationPax.getTotalFare());
					}
					String paxCountStr = adultCount + "|" + childCount + "|" + infantCount;
					ArrayList<String> flightList = new ArrayList<String>();
					for (LCCClientReservationSegment reservationSeg : segments) {
						flightList.add(reservationSeg.getFlightSegmentRefNumber());
					}

					groupBookingRequest = ModuleServiceLocator.getGroupBookingBD().validateModifyResGroupBookingRequest(requestID,
							airFare, paxCountStr, flightList, user.getAgentCode());
					// commonReservationAssembler.setGroupBookingRequestID(Long.parseLong(groupBookingReference));
				}
				if ((groupBookingRequest != null && groupBookingRequest.getRequestID() > 0)) {
					if (totalAmount != null) {
						payment.setAmount(totalAmount.toString());
					} else {
						payment.setAmount(payment.getCcAmount());
						totalAmount = new BigDecimal(payment.getCcAmount());
					}
				}

				boolean isPaymentCarrierSame = resInfo.getMarketingAirlineCode() == null
						? true
						: resInfo.getMarketingAirlineCode().equals(AppSysParamsUtil.getDefaultCarrierCode());
				boolean isReservationOnHold = ReservationInternalConstants.ReservationStatus.ON_HOLD
						.equals(resInfo.getReservationStatus());

				boolean isReservationForcedConfirmed = isForcedConfirmed(resInfo, passengers);

				if ((AppSysParamsUtil.isAdminFeeRegulationEnabled()
						&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, user.getAgentCode()))
						&& !isPaymentCarrierSame && isReservationOnHold) {
					BigDecimal totalPaymentAmountExcludingCCCharge = getTotalPaxAvailableBalance(passengers);
					BigDecimal ccChargeAmount = getCCChargeAmount(totalPaymentAmountExcludingCCCharge);
					ccChargeAmount = ReservationUtil.addTaxToServiceCharge(bookingShopingCart, ccChargeAmount);
				}

				// create balance payment assembler
				LCCClientBalancePayment balancePayment = getBalancePayment(payment, creditInfo, ipgId, bookingShopingCart,
						request.getParameter("paxPayments"), contactInfo, passengers, resInfo.getReservationStatus(),
						actualPayment, groupBookingRequest == null ? 0 : groupBookingRequest.getRequestID(),
						externalPGWResponseDTO);

				balancePayment.setReservationStatus(resInfo.getReservationStatus());
				balancePayment.setPnrSegments(segments);
				balancePayment.setPassengers(passengers);

				if (balancePayment.isSplittedBooking()) {
					ReservationProcessParams processParams = new ReservationProcessParams(request,
							resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(),
							false, null, null, false, false);
					if (isGroupPNR) {
						processParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
								resInfo.getReservationStatus(), null, resInfo.isModifiableReservation(), null);
					}
					if (!processParams.isPartialPayments()) {
						throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
					}
				}

				if (groupBookingRequest == null || (groupBookingRequest != null && groupBookingRequest.getRequestID() > 0)) {

					ServiceResponce responce = ModuleServiceLocator.getAirproxyReservationBD().balancePayment(balancePayment,
							version, isGroupPNR, AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
							getClientInfoDTO(), trackInfoDTO, false,
							Boolean.parseBoolean(request.getParameter("isInfantPaymentSeparated")));

					boolean isOtherCarrierCreditUsed = (Boolean) responce
							.getResponseParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED);

					if (isOtherCarrierCreditUsed) {
						ReservationUtil.reconcileDummyCarrierReservation(balancePayment.getGroupPNR(), trackInfoDTO);
						// set the group pnr and flag so the reservation will be loaded as a interline after the
						// payments
						convertedToGroupReservation = true;
						groupPNR = balancePayment.getGroupPNR();
					}
					if (groupBookingRequest != null && groupBookingRequest.getRequestID() > 0) {
						ModuleServiceLocator.getGroupBookingBD().updateGroupBookingStatus(groupBookingRequest.getRequestID(),
								totalAmount, pnr);

					}
					// When merging re factor this method using the existing method
					// updated agent credit
					agentBalance = CommonUtil.getLoggedInAgentBalance(request);
				}
			}
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String groupBookingValidate() {
		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			long requestID = Long.parseLong(groupBookingReference);
			Collection<LCCClientReservationPax> colPaxes = ReservationUtil
					.transformJsonPassengers(request.getParameter("resPax"));
			Collection<LCCClientReservationSegment> segments = ModifyReservationJSONUtil
					.transformJsonSegments(request.getParameter("pnrSegments"));
			int adultCount = 0, childCount = 0, infantCount = 0;
			for (LCCClientReservationPax reservationPax : colPaxes) {

				if (reservationPax.getPaxType().equals("AD")) {
					adultCount++;
				} else if (reservationPax.getPaxType().equals("CH")) {
					childCount++;
				} else {
					infantCount++;
				}
			}
			String paxCountStr = adultCount + "|" + childCount + "|" + infantCount;
			ArrayList<String> flightList = new ArrayList<String>();
			for (LCCClientReservationSegment reservationSeg : segments) {
				flightList.add(reservationSeg.getFlightSegmentRefNumber());
			}

			groupBookingRequest = ModuleServiceLocator.getGroupBookingBD().validateModifyResGroupBookingRequest(requestID,
					getTotalAmount(), paxCountStr, flightList, user.getAgentCode());

			// If the agreed fare is set as a percentage then calculate it accordingly
			if (groupBookingRequest != null && groupBookingRequest.isAgreedFarePercentage()) {
				groupBookingRequest.setMinPaymentRequired(groupBookingRequest.getAgreedFare().divide(new BigDecimal("100"))
						.multiply(groupBookingRequest.getMinPaymentRequired()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @param segments
	 * @throws ModuleException
	 */
	private void validate(Collection<LCCClientReservationSegment> segments) throws ModuleException {

		PaymentMethod paymentMethod = payment.getPaymentMethod();
		if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {

			if (ReservationUtil.getNoOfCnfSegments(segments) > ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT) {
				XBERuntimeException ex = new XBERuntimeException("bsp.payment.segment.count.exceeded");
				List<String> messageParameters = new ArrayList<String>();
				messageParameters.add(ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT + "");
				ex.setMessageParameters(messageParameters);
				throw ex;
			}
		}
	}

	private void validate(BigDecimal resBalance, String strOriginAgentCode, boolean isTaxRefundForNoShow) throws Exception {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams processParams = new ReservationProcessParams(request,
				resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), false, null,
				null, false, false);
		if (resInfo.getInterlineModificationParams() != null && resInfo.getInterlineModificationParams().size() > 0) {
			processParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
					resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), null);
		}

		// isTaxRefundForNoShow functionality is obsolete recommend to use “No Show Tax Refund” feature, which will
		// allow only reversed refundable charges only
		// if (resBalance.compareTo(new BigDecimal(0)) >= 0) {
		// if (!(processParams.isAllowRefundNoCredit() || isTaxRefundForNoShow)) {
		// throw new ModuleException("airreservation.modify.refund.no.credits");
		// }
		// }

		if (isNonRefundable()) {
			throw new ModuleException("airreservation.modify.refund.not.allowed.payment");
		}

		// Temporally disable refund for multiple carrier and allow to only one opCarrier reservations only
		List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(request.getParameter("resONDs"));
		Set<String> opCarrierList = new HashSet<String>();
		for (LCCClientCarrierOndGroup ondGroup : colONDs) {
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(ondGroup.getSegmentCode())) {
				opCarrierList.add(ondGroup.getCarrierCode());
			}
		}

		if (opCarrierList.size() > 1) {
			throw new ModuleException("airreservation.modify.refund.multiple.carrier.not.allowed");
		}

		PaymentMethod paymentMethod = PaymentMethod.getPaymentType(radPaymentType);

		if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
			if (!processParams.isAllowOnAccountRefund()) {
				throw new ModuleException("airreservation.modify.refund.onaccount.not.allowed");
			} else {
				if (isRefundToOthreAgent(selAgent) && !(processParams.isAllowAnyOnAccountRefund()
						|| processParams.isAllowOnAccountRptRefund() || processParams.isAllowAnyOpCarrierOnAccountRefund())) {
					throw new ModuleException("airreservation.modify.refund.other.onaccount.not.allowed");
				}
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
			if (!processParams.isAllowCashRefund()) {
				throw new ModuleException("airreservation.modify.refund.cash.not.allowed");
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
			if (!processParams.isAllowCreditCardRefund()) {
				throw new ModuleException("airreservation.modify.refund.creditcard.not.allowed");
			} else {

				// TODO: Add logic to identify the Credit Card originally paid
				// with, and if the selected card is not same as the one
				// originally used to pay then check the privilege : processParams.isAllowAnyCreditCardRefund();
			}
		}

	}

	private LCCClientBalancePayment getBalancePayment(PaymentTO paymentTO, CreditCardTO creditInfoTO, Integer ipgId,
			BookingShoppingCart bookingShoppingCart, String paxCredit, CommonReservationContactInfo contactInfo,
			Set<LCCClientReservationPax> passengers, String reservationStatus, boolean actualPayment, long groupRequestID,
			IPGResponseDTO externalPGWResponseDTO) throws Exception {

		LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
		lccClientBalancePayment.setGroupPNR(this.groupPNR);
		lccClientBalancePayment.setOwnerAgent(ownerAgentCode);
		lccClientBalancePayment.setContactInfo(contactInfo);

		UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
		boolean infantPaymentSeparated = Boolean.parseBoolean(request.getParameter("isInfantPaymentSeparated"));

		// to identify whether the payment is done by the same carrier
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
		boolean isPaymentCarrierSame = resInfo.getMarketingAirlineCode().equals(AppSysParamsUtil.getDefaultCarrierCode());

		if (groupRequestID > 0) {
			lccClientBalancePayment.setForceConfirm(true);
		} else {
			lccClientBalancePayment.setForceConfirm((paymentTO.getMode().equals("FORCE")));
		}
		lccClientBalancePayment.setActualPayment(actualPayment);

		StringBuilder debugLog = null;
		if (log.isDebugEnabled()) {
			debugLog = new StringBuilder();
			debugLog.append("Balance Payment debug" + CommonsConstants.NEWLINE)
					.append("[pnr=" + this.groupPNR + "]" + CommonsConstants.NEWLINE)
					.append("[paymentMode=" + paymentTO.getMode() + "]" + CommonsConstants.NEWLINE);
		}

		PaymentHolder paymentHolder = new PaymentHolder();
		if (BookingUtil.isDoubleAgentPay(paymentTO)) {
			if (paymentTO.getAgentOneAmount() != null && !"".equals(paymentTO.getAgentOneAmount().trim())) {
				paymentTO.setAmount(paymentTO.getAgentOneAmount());

			}

			PaymentTO second = paymentTO.clone();
			second.setAgent(paymentTO.getAgentTwo());
			second.setAmount(paymentTO.getAgentTwoAmount());

			paymentHolder.addPayment(paymentTO);
			paymentHolder.addPayment(second);

		} else if (creditInfoTO != null) {
			paymentHolder.addCardPayment(paymentTO, creditInfoTO);
		} else {
			paymentHolder.addPayment(paymentTO);
		}

		if (bookingShoppingCart.getPayByVoucherInfo() != null) {
			paymentHolder.addVoucherPayment(bookingShoppingCart.getPayByVoucherInfo().getVoucherDTOList());
		}

		PayCurrencyDTO payCurrencyDTO = null;
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		BigDecimal totalPaxCredit = BigDecimal.ZERO;

		JSONArray jsonPaxPayArray = (JSONArray) new JSONParser().parse(paxCredit);

		boolean hasPaxWithPayments = false;
		boolean forceConfirmed = lccClientBalancePayment.isForceConfirm();

		Iterator<?> itIter = jsonPaxPayArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			if (BeanUtils.nullHandler(jPPObj.get("hasPayment")).equals("true")) {
				if (forceConfirmed) {
					hasPaxWithPayments = true;
					break;
				}
			} else {
				if (!forceConfirmed) {
					lccClientBalancePayment.setSplittedBooking(true);
					break;
				}
			}
		}

		if (log.isDebugEnabled())
			debugLog.append("[hasPaxWithPayments=" + hasPaxWithPayments + "]" + CommonsConstants.NEWLINE);
		ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

		ExternalChargesMediator externalChargesMediator;
		if ((forceConfirmed && !hasPaxWithPayments) || new BigDecimal(paymentTO.getAmount()).compareTo(BigDecimal.ZERO) <= 0) {

			if ((AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode()))
					&& isPaymentCarrierSame) {
				externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
			} else {
				externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false);
			}

		} else {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart,
					paymentTO.getPaymentMethod().getCode().equals(PaymentMethod.CRED.getCode()));

			if ((AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode()))
					&& !isPaymentCarrierSame
					&& ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(resInfo.getReservationStatus())
					&& !payment.getPaymentMethod().getCode().equals(PaymentMethod.CRED.getCode())) {
				externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
			}
		}

		boolean adminFeeEnabled = false;
		if ((AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode()))
				&& !isPaymentCarrierSame
				&& ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(resInfo.getReservationStatus())
				&& !payment.getPaymentMethod().getCode().equals(PaymentMethod.CRED.getCode())) {
			adminFeeEnabled = true;
		}

		int noofCreditPax[] = getNoOfAdults(passengers, jsonPaxPayArray, debugLog, infantPaymentSeparated, adminFeeEnabled);

		if (log.isDebugEnabled()) {
			debugLog.append("[noofCreditPax=" + noofCreditPax[0] + " noofCreditInfantPax=" + noofCreditPax[1] + "] "
					+ CommonsConstants.NEWLINE);
		}

		externalChargesMediator.setCalculateJNTaxForCCCharge(true);

		if (!forceConfirmed && paymentTO.getPaymentMethod().getCode().equals(PaymentMethod.BSP.getCode())) {
			externalChargesMediator.setBSPPayment(true);
		} else {
			bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.BSP_FEE);
		}

		// LinkedList<ExternalChgDTO> infantReservationCharges = new LinkedList<>();
		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForABalancePayment(noofCreditPax[0],
				noofCreditPax[1]);

		// Map<String, BigDecimal> agentAmountMap = new ConcurrentHashMap<String, BigDecimal>();
		// if (BookingUtil.isDoubleAgentPay(payment)) {
		// agentAmountMap.put(payment.getAgent(), new BigDecimal(payment.getAgentOneAmount()));
		// agentAmountMap.put(payment.getAgentTwo(), new BigDecimal(payment.getAgentTwoAmount()));
		// }

		Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();

		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)
				&& bookingShoppingCart.getSelectedCurrencyTimestamp() != null
				&& !AppSysParamsUtil.getBaseCurrency().equals(hdnSelCurrency)) {

			exchangeRateByDate = bookingShoppingCart.getSelectedCurrencyTimestamp();
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(exchangeRateByDate);

		// int creditPax = 0;
		// BigDecimal amountSpend = AccelAeroCalculator.getDefaultBigDecimalZero();
		// BigDecimal amountPerPassenger = AccelAeroCalculator.getDefaultBigDecimalZero();

		List<LCCClientReservationPax> passengerList = new ArrayList<LCCClientReservationPax>(passengers);
		Collections.sort(passengerList, new Comparator<LCCClientReservationPax>() {
			public int compare(LCCClientReservationPax p1, LCCClientReservationPax p2) {
				return p1.getPaxSequence().compareTo(p2.getPaxSequence());
			}
		});
		// int paxCount = passengerList.size();

		BigDecimal totalExternalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (LCCClientReservationPax pax : passengerList) {
			BigDecimal amountDue = pax.getTotalAvailableBalance();

			if (log.isDebugEnabled()) {
				debugLog.append(
						"[PaxSeq=" + pax.getPaxSequence() + ",amountDueBefore=" + amountDue + "]" + CommonsConstants.NEWLINE);
			}

			if ((infantPaymentSeparated || !pax.getPaxType().equals(PaxTypeTO.INFANT))
					&& (amountDue.compareTo(BigDecimal.ZERO) >= 0)) {
				LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();

				Integer paxsequence = pax.getPaxSequence();
				boolean creditFlag = false;

				itIter = jsonPaxPayArray.iterator();
				JSONArray jPCInfo = null;
				// checking against the name no paxid found here
				boolean hasPayment = true;
				while (itIter.hasNext()) {
					JSONObject jPPObj = (JSONObject) itIter.next();
					Integer creditPaxId = new Integer(BeanUtils.nullHandler(jPPObj.get("displayPaxID")));
					if (creditPaxId.intValue() == paxsequence.intValue()) {
						jPCInfo = (JSONArray) jPPObj.get("paxCreditInfo");
						hasPayment = BeanUtils.nullHandler(jPPObj.get("hasPayment")).equals("true") ? true : false;
						break;
					}

				}
				if (hasPayment) {
					// if (groupRequestID == 0 && jPCInfo != null) {
					if (jPCInfo != null) {
						Iterator<?> uIter = jPCInfo.iterator();
						while (uIter.hasNext()) {
							JSONObject jUObj = (JSONObject) uIter.next();
							BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
							amountDue = AccelAeroCalculator.add(amountDue, amount.negate());
							creditFlag = true;

							if (log.isDebugEnabled())
								debugLog.append("[usedCreditAmount=" + amount + ",amountDueAfter=" + amountDue + "]"
										+ CommonsConstants.NEWLINE);
						}
					}

					boolean validPaymentExist = PaymentUtil.isValidPaymentExist(creditFlag, amountDue, reservationStatus);

					if (validPaymentExist) {
						Collection<ExternalChgDTO> paxExternalCharges = new ArrayList<>();
						if (perPaxExternalCharges.size() > 0) {
							Collection<ExternalChgDTO> extCharges = (Collection<ExternalChgDTO>) perPaxExternalCharges.pop();
							if(extCharges != null) {
								paxExternalCharges.addAll(extCharges);
							}
						}
						Collection<ExternalChgDTO> externalServiceTaxCharges = externalChargesMediator
								.getPaxServiceTaxExternalCharge(pax.getPaxSequence());
						if (paxExternalCharges != null) {
							paxExternalCharges.addAll(externalServiceTaxCharges);
						}
						if (pax.getPaxType().equals(PaxTypeTO.INFANT)) {
							if (pax.getParent() != null && pax.getParent().getPaxSequence() != null) {
								Collection<ExternalChgDTO> externalInfantServiceTaxCharges = externalChargesMediator
										.getInfantServiceTaxExternalCharge(pax.getParent().getPaxSequence());
								if (paxExternalCharges != null) {
									paxExternalCharges.addAll(externalInfantServiceTaxCharges);
								}
							}
						}

						BigDecimal totalPaxExtCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (paxExternalCharges != null && paxExternalCharges.size() > 0) {
							for (ExternalChgDTO extChgDTO : paxExternalCharges) {

								if (isPaymentCarrierSame) {
									if (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
											.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {
										boolean isOnHold = ReservationInternalConstants.ReservationStatus.ON_HOLD
												.equals(reservationStatus);
										boolean isForceConfirmed = ReservationInternalConstants.ReservationStatus.CONFIRMED
												.equals(reservationStatus);

										if (((isOnHold || isForceConfirmed)
												&& extChgDTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.CREDIT_CARD))
												|| ((isOnHold || isForceConfirmed) && extChgDTO.getExternalChargesEnum()
														.equals(EXTERNAL_CHARGES.JN_OTHER))) {
											// skip adding admin fee
										} else {
											totalPaxExtCharges = AccelAeroCalculator.add(totalPaxExtCharges,
													extChgDTO.getAmount());
										}
									} else {
										totalPaxExtCharges = AccelAeroCalculator.add(totalPaxExtCharges, extChgDTO.getAmount());
									}
								} else {
									if (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
											.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {
										boolean isOnHold = ReservationInternalConstants.ReservationStatus.ON_HOLD
												.equals(reservationStatus);
										boolean isForceConfirmed = ReservationInternalConstants.ReservationStatus.CONFIRMED
												.equals(reservationStatus);

										if (((isOnHold || isForceConfirmed)
												&& extChgDTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.CREDIT_CARD))
												|| ((isOnHold || isForceConfirmed) && extChgDTO.getExternalChargesEnum()
														.equals(EXTERNAL_CHARGES.JN_OTHER))) {
											totalPaxExtCharges = AccelAeroCalculator.add(totalPaxExtCharges,
													extChgDTO.getAmount());
										} else {
											totalPaxExtCharges = AccelAeroCalculator.add(totalPaxExtCharges,
													extChgDTO.getAmount());
										}
									} else {
										totalPaxExtCharges = AccelAeroCalculator.add(totalPaxExtCharges, extChgDTO.getAmount());
									}
								}
							}
						}
						if (validPaymentExist
								|| AccelAeroCalculator.getDefaultBigDecimalZero().compareTo(totalPaxExtCharges) != 0) {

							totalExternalCharges = AccelAeroCalculator.add(totalExternalCharges, totalPaxExtCharges);

							BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
							if (validPaymentExist) {
								totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amountDue, totalPaxExtCharges);
							} else {
								totalPayAmountWithServiceCharges = totalPaxExtCharges;
							}
							paymentHolder.consumePayment(amountDue, totalPayAmountWithServiceCharges);

							if (!(AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
									.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode()))) {
								paymentAssembler.addExternalCharges(paxExternalCharges);
							} else {
								if (isPaymentCarrierSame) {
									// skip admin fee
								} else {
									paymentAssembler.addExternalCharges(paxExternalCharges);
								}
							}

							while (paymentHolder.hasNonRecordedConsumedPayments()) {
								String consumedPayRef = paymentHolder.getNextConsumedPaymentRef();
								BigDecimal consumedPayAmount = paymentHolder.getConsumedPaymentAmount(consumedPayRef);
								PaymentTO payment = paymentHolder.getPayment(consumedPayRef);
								CreditCardTO creditInfo = paymentHolder.getCreditCardTO(consumedPayRef);
								VoucherDTO voucherDTO = paymentHolder.getVoucherPayment(consumedPayRef);

								if (payment != null) {
									// TODO IMPROVE
									PaymentMethod paymentMethod = payment.getPaymentMethod();

									if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
										payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(getHdnSelCurrency(),
												exchangeRateProxy);
										// paymentAssembler.addCashPayment(amountDue, cashPayCurrencyDTO,
										// exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
										// payment.getActualPaymentMethod(), null, null);
										paymentAssembler.addCashPayment(consumedPayAmount, payCurrencyDTO,
												exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
												payment.getActualPaymentMethod(), null, null);

									} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {

										String loggedInAgencyCode = RequestHandlerUtil.getAgentCode(request);
										payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(getHdnSelCurrency(),
												loggedInAgencyCode, payment.getAgent(), exchangeRateProxy);

										if (log.isDebugEnabled())
											debugLog.append("[ONACC Payment, pay agent =" + payment.getAgent() + ",amount="
													+ amountDue + "]" + CommonsConstants.NEWLINE);

										paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount,
												payCurrencyDTO, exchangeRateProxy.getExecutionDate(),
												payment.getPaymentReference(), payment.getActualPaymentMethod(),
												Agent.PAYMENT_MODE_ONACCOUNT, null);

									} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {

										String loggedInAgencyCode = RequestHandlerUtil.getAgentCode(request);
										// Override the selected currency with Agent
										// camountDue.subtract(extCharge)ountry
										// currency code for BSP
										String bspCurrency = ModuleServiceLocator.getCommonServiceBD()
												.getCurrencyCodeForStation(RequestHandlerUtil.getAgentStationCode(request));
										payCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgencyCode,
												payment.getAgent(), exchangeRateProxy);

										if (lccClientBalancePayment.getExternalChargesMap() == null) {
											lccClientBalancePayment.setExternalChargesMap(getExtChgMap(
													bookingShoppingCart.getExternalChargesType(), paymentMethod.getCode()));
										}

										if (log.isDebugEnabled())
											debugLog.append("[BSP Payment, pay agent =" + payment.getAgent() + ",amount="
													+ amountDue + "]" + CommonsConstants.NEWLINE);
										paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount,
												payCurrencyDTO, exchangeRateProxy.getExecutionDate(),
												payment.getPaymentReference(), payment.getActualPaymentMethod(),
												Agent.PAYMENT_MODE_BSP, null);

									} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
										payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(getHdnSelCurrency(), ipgId,
												exchangeRateProxy, false);
										ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
												payCurrencyDTO.getPayCurrencyCode());

										if (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
												.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {

											if (lccClientBalancePayment.getExternalChargesMap() == null) {
												lccClientBalancePayment.setExternalChargesMap(getExtChgMap(
														bookingShoppingCart.getExternalChargesType(), paymentMethod.getCode()));
											}
										} else {

											boolean onhold = false;

											if (ReservationStatus.ON_HOLD
													.equals(lccClientBalancePayment.getReservationStatus())) {
												onhold = true;
											}

											if (lccClientBalancePayment.getExternalChargesMap() == null && !onhold) {
												lccClientBalancePayment.setExternalChargesMap(getExtChgMap(
														bookingShoppingCart.getExternalChargesType(), paymentMethod.getCode()));

											}
										}

										if (externalPGWResponseDTO != null
												&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL
														.equals(externalPGWResponseDTO.getBrokerType())) {
											LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo = new LCCClientCCPaymentMetaInfo();
											lccClientCCPaymentMetaInfo
													.setTemporyPaymentId(externalPGWResponseDTO.getTemporyPaymentId());
											lccClientCCPaymentMetaInfo
													.setPaymentBrokerId(externalPGWResponseDTO.getPaymentBrokerRefNo());
											paymentAssembler.addExternalCardPayment(PaymentType.CARD_GENERIC.getTypeValue(), "",
													consumedPayAmount, AppIndicatorEnum.APP_XBE, TnxModeEnum.SECURE_3D,
													lccClientCCPaymentMetaInfo, ipgIdentificationParamsDTO, payCurrencyDTO,
													new Date(), null, null, false,
													externalPGWResponseDTO.getTemporyPaymentId().toString());
										} else {
											if (log.isDebugEnabled())
												debugLog.append("[CRED Payment, IPG =" + ipgId + ",currency="
														+ ipgIdentificationParamsDTO.getPaymentCurrencyCode() + ",amount="
														+ amountDue + "]" + CommonsConstants.NEWLINE);
											UserInputDTO userInputDTO = new UserInputDTO();
											userInputDTO.setBuyerAddress1(creditInfo.getBillingAddress1());
											userInputDTO.setBuyerAddress2(creditInfo.getBillingAddress2());
											userInputDTO.setBuyerCity(creditInfo.getCity());
											userInputDTO.setPostalCode(creditInfo.getPostalCode());
											userInputDTO.setBuyerState(creditInfo.getState());
											// userInputDTO.setPayerCountry(contactInfo.getCountry());
											userInputDTO.setBuyerFirstName(contactInfo.getFirstName());
											userInputDTO.setBuyerLastName(contactInfo.getLastName());

											// paymentAssembler.addInternalCardPayment(new
											// Integer(creditInfo.getCardType().trim()).intValue(),
											// creditInfo.getExpiryDateYYMMFormat().trim(),
											// creditInfo.getCardNo().trim(),
											// creditInfo
											// .getCardHoldersName().trim(), creditInfo.getAddress(),
											// creditInfo.getCardCVV()
											// .trim(), consumedPayAmount, AppIndicatorEnum.APP_XBE,
											// TnxModeEnum.MAIL_TP_ORDER,
											// ipgIdentificationParamsDTO, payCurrencyDTO,
											// exchangeRateProxy.getExecutionDate(),
											// creditInfo.getCardHoldersName(), payment.getPaymentReference(), payment
											// .getActualPaymentMethod(), null, userInputDTO);

											boolean onhold = true;
											BigDecimal actualConsumedPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
											if (validPaymentExist) {
												actualConsumedPayAmount = consumedPayAmount;
											} else if (AccelAeroCalculator.getDefaultBigDecimalZero()
													.compareTo(totalPaxExtCharges) != 0) {
												lccClientBalancePayment.setAcceptPaymentsThanPayable(true);
											}
											paymentAssembler.addInternalCardPayment(
													new Integer(creditInfo.getCardType().trim()).intValue(),
													creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
													creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(),
													creditInfo.getCardCVV().trim(), actualConsumedPayAmount,
													AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER,
													ipgIdentificationParamsDTO, payCurrencyDTO,
													exchangeRateProxy.getExecutionDate(), creditInfo.getCardHoldersName(),
													payment.getPaymentReference(), payment.getActualPaymentMethod(), null,
													userInputDTO);
										}
									} else if (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
										payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(getHdnSelCurrency(), ipgId,
												exchangeRateProxy, true);
										ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
												payCurrencyDTO.getPayCurrencyCode());
										paymentAssembler.addOfflineCardPayment(consumedPayAmount, TnxModeEnum.MAIL_TP_ORDER,
												ipgIdentificationParamsDTO, payment.getPaymentReference(),
												payment.getActualPaymentMethod(), payCurrencyDTO);
									}
								} else if (voucherDTO != null) {
									voucherDTO.setRedeemdAmount(consumedPayAmount.toString());
									paymentAssembler.addVoucherPayment(voucherDTO, payCurrencyDTO,
											exchangeRateProxy.getExecutionDate());
								}
							}
						}
					}
					if (jPCInfo != null) {
						Iterator<?> uIter = jPCInfo.iterator();
						while (uIter.hasNext()) {
							JSONObject jUObj = (JSONObject) uIter.next();
							String carrierCode = (String) jUObj.get("carrier");
							Integer debitPaxRefNo = new Integer((String) jUObj.get("paxCreditId"));
							BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
							BigDecimal residingCarrierAmount = new BigDecimal((String) jUObj.get("residingCarrierAmount"));
							if (log.isDebugEnabled())
								debugLog.append("[Pax Credit Payment, debitPaxRefNo =" + debitPaxRefNo + ",amount=" + amount + "]"
										+ CommonsConstants.NEWLINE);
							String pnr = (String) jUObj.get("pnr");
							paymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, amount,
									exchangeRateProxy.getExecutionDate(), residingCarrierAmount, null, null, paxsequence, null,
									pnr, null);
							totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, amount);
						}
					}
				}
				lccClientBalancePayment.addPassengerPayments(pax.getPaxSequence(), paymentAssembler);
			}

		}

		int paidPaxCount = 0;
		Iterator<?> it = jsonPaxPayArray.iterator();
		while (it.hasNext()) {
			JSONObject jPPObj = (JSONObject) it.next();
			if (BeanUtils.nullHandler(jPPObj.get("hasPayment")).equals("true")) {
				paidPaxCount++;
			} else {
				if (BeanUtils.nullHandler(jPPObj.get("displayOriginalTobePaid")).equals("0.00")) {
					paidPaxCount++;
				}
			}
		}

		if (paidPaxCount == lccClientBalancePayment.getPaxSeqWisePayAssemblers().size()) {
			lccClientBalancePayment.setSplittedBooking(false);
		}

		if (!lccClientBalancePayment.isForceConfirm() && !lccClientBalancePayment.isSplittedBooking()) {
			Object[] arrObj = ReservationUtil.loadPaxPriceSummary(passengers,
					Boolean.parseBoolean(request.getParameter("isInfantPaymentSeparated")));
			String paymentAmount = AccelAeroCalculator.formatAsDecimal(paymentHolder.getTotalPaymentAmount());
			String balance = AccelAeroCalculator.formatAsDecimal(
					AccelAeroCalculator.add((BigDecimal) arrObj[1], totalPaxCredit.negate(), totalExternalCharges));

			if (log.isDebugEnabled()) {
				debugLog.append("[totalPrice=" + arrObj[1] + ",externalCharges=" + totalExternalCharges + ",totalPaxCredit="
						+ totalPaxCredit + ",paymentAmount=" + paymentAmount + "]" + CommonsConstants.NEWLINE);
				log.debug(debugLog.toString());
			}
			if (groupRequestID == 0) {
				if (paymentAmount.compareTo(balance) != 0) {
					throw new Exception("For the pnr [" + this.groupPNR + "] Payment amount [" + paymentAmount
							+ "] is not equal to amount due [" + balance + "]");
				}
			}
		}

		return lccClientBalancePayment;
	}

	private Map getExtChgMap(Integer externalChargeType, String paymentMethod) throws ModuleException {
		Set<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();

		if (PaymentMethod.CRED.getCode().equals(paymentMethod)) {
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		} else if (PaymentMethod.BSP.getCode().equals(paymentMethod)) {
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.BSP_FEE);
		}

		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_OTHER);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SERVICE_TAX);
		Map extExternalChgDTOMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
				externalChargeType);
		return extExternalChgDTOMap;
	}

	private int[] getNoOfAdults(Set<LCCClientReservationPax> passengers, JSONArray jsonPaxPayArray, StringBuilder debugLog,
			boolean infantPaymentSeparated, boolean isAdminFeeEnabled) {
		int noOfAdults = 0;
		int noOfInfants = 0;

		for (LCCClientReservationPax pax : passengers) {
			BigDecimal amountDue = pax.getTotalAvailableBalance();

			if (debugLog != null)
				debugLog.append("[paxSeq=" + pax.getPaxSequence() + ",due=" + amountDue + "]").append(CommonsConstants.NEWLINE);

			if ((infantPaymentSeparated || !pax.getPaxType().equals(PaxTypeTO.INFANT))
					&& amountDue.compareTo(BigDecimal.ZERO) == 1) {

				Iterator<?> itIter = jsonPaxPayArray.iterator();
				JSONArray jPCInfo = null;

				while (itIter.hasNext()) {
					JSONObject jPPObj = (JSONObject) itIter.next();
					Integer creditPaxId = new Integer(BeanUtils.nullHandler(jPPObj.get("displayPaxID")));
					if (creditPaxId.intValue() == pax.getPaxSequence().intValue()) {
						jPCInfo = (JSONArray) jPPObj.get("paxCreditInfo");
						break;
					}
				}

				if (jPCInfo != null) {
					if (debugLog != null)
						debugLog.append("[Credit Utilization paxSeq=" + pax.getPaxSequence() + ",amountDueBefore=" + amountDue);

					Iterator<?> uIter = jPCInfo.iterator();
					while (uIter.hasNext()) {
						JSONObject jUObj = (JSONObject) uIter.next();
						BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
						amountDue = AccelAeroCalculator.add(amountDue, amount.negate());

						if (debugLog != null)
							debugLog.append(",usedAmount=" + amount);
					}
					if (debugLog != null)
						debugLog.append(",amountDueAfter=" + amountDue + "]").append(CommonsConstants.NEWLINE);
				}
				if ((payment.getPaymentMethod().getCode().equals(PaymentMethod.CRED.getCode()) || isAdminFeeEnabled
						|| payment.getPaymentMethod().getCode().equals(PaymentMethod.BSP.getCode())
								&& amountDue.compareTo(BigDecimal.ZERO) > 0)) {
					if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
						noOfAdults++;
					} else {
						noOfInfants++;
					}

				}
			}

		}
		int[] payablePaxCount = { noOfAdults, noOfInfants };
		return payablePaxCount;
	}

	private CommonReservationAssembler getReservationAssembler(String groupPNR, Collection<LCCClientReservationPax> colPaxes,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		CommonReservationAssembler reservationAssembler = new CommonReservationAssembler();
		reservationAssembler.getLccreservation().setPNR(groupPNR);
		String authorizationId = "";
		Map<String, Collection<LCCClientReservationPax>> colPaxMap = new HashMap<String, Collection<LCCClientReservationPax>>();

		for (LCCClientReservationPax reservationPax : colPaxes) {
			for (LCCClientPaymentInfo payment : reservationPax.getLccClientPaymentHolder().getPayments()) {
				if (payment instanceof CommonCreditCardPaymentInfo) {
					CommonCreditCardPaymentInfo paymentInfo = (CommonCreditCardPaymentInfo) payment;
					if (paymentInfo.getCarrierVisePayments().equals(carrierVisePayments)) {
						authorizationId = paymentInfo.getAuthorizationId();
					}
					if ((colPaxMap.get(paymentInfo.getAuthorizationId()) == null)
							|| (colPaxMap.get(paymentInfo.getAuthorizationId()).size() == 0)) {
						colPaxMap.put(paymentInfo.getAuthorizationId(), new ArrayList<LCCClientReservationPax>());
					}
					colPaxMap.get(paymentInfo.getAuthorizationId()).add(reservationPax);
				}
			}
		}

		for (Entry<String, Collection<LCCClientReservationPax>> entry : colPaxMap.entrySet()) {
			if (authorizationId.equals(entry.getKey())) {
				Collection<LCCClientReservationPax> colPaxList = entry.getValue();

				for (LCCClientReservationPax reservationPax : colPaxList) {
					if (!ReservationApiUtils.isInfantType(reservationPax)) {
						BigDecimal refundAmount = null;
						RefundInfoTO refundInfo = null;

						for (LCCClientPaymentInfo payment : reservationPax.getLccClientPaymentHolder().getPayments()) {
							if (payment instanceof CommonCreditCardPaymentInfo) {
								CommonCreditCardPaymentInfo paymentInfo = (CommonCreditCardPaymentInfo) payment;
								if (authorizationId.equals(paymentInfo.getAuthorizationId())) {
									refundAmount = paymentInfo.getTotalAmount();
									refundInfo = new RefundInfoTO(paymentInfo.getPaymentTnxReference(),
											paymentInfo.getLccUniqueTnxId(), paymentInfo.getPaymentTnxDateTime(),
											paymentInfo.getCarrierVisePayments(), paymentInfo.getCarrierCode());
								}
							}
						}

						LCCClientPaymentAssembler paymentAssembler = getPaymentAssembler(refundAmount, exchangeRateProxy,
								refundInfo);

						boolean isParent = reservationPax.getInfants().size() > 0 ? true : false;

						PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
						String arrivalIntlFltNo = "";
						Date intlFltArrivalDate = null;
						String departureIntlFltNo = "";
						Date intlFltDepartureDate = null;
						String pnrPaxGruopId = "";

						if (reservationPax.getLccClientAdditionPax() != null) {
							paxAdditionalInfoDTO.setPassportNo(reservationPax.getLccClientAdditionPax().getPassportNo());
							paxAdditionalInfoDTO.setPassportExpiry(reservationPax.getLccClientAdditionPax().getPassportExpiry());
							paxAdditionalInfoDTO
									.setPassportIssuedCntry(reservationPax.getLccClientAdditionPax().getPassportIssuedCntry());
							paxAdditionalInfoDTO.setEmployeeId(reservationPax.getLccClientAdditionPax().getEmployeeId());
							paxAdditionalInfoDTO.setDateOfJoin(reservationPax.getLccClientAdditionPax().getDateOfJoin());
							paxAdditionalInfoDTO.setIdCategory(reservationPax.getLccClientAdditionPax().getIdCategory());
							arrivalIntlFltNo = reservationPax.getLccClientAdditionPax().getArrivalIntlFltNo();
							intlFltArrivalDate = reservationPax.getLccClientAdditionPax().getIntlFltArrivalDate();
							departureIntlFltNo = reservationPax.getLccClientAdditionPax().getDepartureIntlFltNo();
							intlFltDepartureDate = reservationPax.getLccClientAdditionPax().getIntlFltDepartureDate();
							pnrPaxGruopId = reservationPax.getLccClientAdditionPax().getPnrPaxGroupId();

						}

						if (isParent) {

							// Passenger with a Infant - Parent
							reservationAssembler.addParent(reservationPax.getFirstName(), reservationPax.getLastName(),
									reservationPax.getTitle(), reservationPax.getDateOfBirth(),
									reservationPax.getNationalityCode(), reservationPax.getPaxSequence(),
									reservationPax.getAttachedPaxId(), reservationPax.getTravelerRefNumber(),
									paxAdditionalInfoDTO, reservationPax.getPaxCategory(), paymentAssembler, null, null, null,
									null, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate,
									pnrPaxGruopId);
						} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxType())) {
							reservationAssembler.addChild(reservationPax.getFirstName(), reservationPax.getLastName(),
									reservationPax.getTitle(), reservationPax.getDateOfBirth(),
									reservationPax.getNationalityCode(), reservationPax.getPaxSequence(),
									reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO, reservationPax.getPaxCategory(),
									paymentAssembler, null, null, null, null, null, arrivalIntlFltNo, intlFltArrivalDate,
									departureIntlFltNo, intlFltDepartureDate, pnrPaxGruopId);
						} else {
							// Passenger without a Infant - Single
							reservationAssembler.addSingle(reservationPax.getFirstName(), reservationPax.getLastName(),
									reservationPax.getTitle(), reservationPax.getDateOfBirth(),
									reservationPax.getNationalityCode(), reservationPax.getPaxSequence(),
									reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO, reservationPax.getPaxCategory(),
									paymentAssembler, null, null, null, null, null, arrivalIntlFltNo, intlFltArrivalDate,
									departureIntlFltNo, intlFltDepartureDate, pnrPaxGruopId);
						}
					}
				}
			}
		}
		return reservationAssembler;

	}

	private CommonReservationAssembler getReservationAssembler(String groupPNR, BigDecimal refundAmount,
			ExchangeRateProxy exchangeRateProxy, LCCClientReservationPax reservationPax, RefundInfoTO refundInfo)
			throws ModuleException {

		CommonReservationAssembler reservationAssembler = new CommonReservationAssembler();
		reservationAssembler.getLccreservation().setPNR(groupPNR);

		LCCClientPaymentAssembler paymentAssembler = getPaymentAssembler(refundAmount, exchangeRateProxy, refundInfo);

		boolean isParent = reservationPax.getInfants().size() > 0 ? true : false;

		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
		String arrivalIntlFltNo = "";
		Date intlFltArrivalDate = null;
		String departureIntlFltNo = "";
		Date intlFltDepartureDate = null;
		String pnrPaxGroupId = "";

		if (reservationPax.getLccClientAdditionPax() != null) {
			paxAdditionalInfoDTO.setPassportNo(reservationPax.getLccClientAdditionPax().getPassportNo());
			paxAdditionalInfoDTO.setPassportExpiry(reservationPax.getLccClientAdditionPax().getPassportExpiry());
			paxAdditionalInfoDTO.setPassportIssuedCntry(reservationPax.getLccClientAdditionPax().getPassportIssuedCntry());
			paxAdditionalInfoDTO.setEmployeeId(reservationPax.getLccClientAdditionPax().getEmployeeId());
			paxAdditionalInfoDTO.setDateOfJoin(reservationPax.getLccClientAdditionPax().getDateOfJoin());
			paxAdditionalInfoDTO.setIdCategory(reservationPax.getLccClientAdditionPax().getIdCategory());
			arrivalIntlFltNo = reservationPax.getLccClientAdditionPax().getArrivalIntlFltNo();
			intlFltArrivalDate = reservationPax.getLccClientAdditionPax().getIntlFltArrivalDate();
			departureIntlFltNo = reservationPax.getLccClientAdditionPax().getDepartureIntlFltNo();
			intlFltDepartureDate = reservationPax.getLccClientAdditionPax().getIntlFltDepartureDate();
			pnrPaxGroupId = reservationPax.getLccClientAdditionPax().getPnrPaxGroupId();

		}

		if (isParent) {

			// Passenger with a Infant - Parent
			reservationAssembler.addParent(reservationPax.getFirstName(), reservationPax.getLastName(), reservationPax.getTitle(),
					reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(), reservationPax.getPaxSequence(),
					reservationPax.getAttachedPaxId(), reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO,
					reservationPax.getPaxCategory(), paymentAssembler, null, null, null, null, arrivalIntlFltNo,
					intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
		} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxType())) {
			reservationAssembler.addChild(reservationPax.getFirstName(), reservationPax.getLastName(), reservationPax.getTitle(),
					reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(), reservationPax.getPaxSequence(),
					reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO, reservationPax.getPaxCategory(),
					paymentAssembler, null, null, null, null, null, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo,
					intlFltDepartureDate, pnrPaxGroupId);
		} else {
			// Passenger without a Infant - Single
			reservationAssembler.addSingle(reservationPax.getFirstName(), reservationPax.getLastName(), reservationPax.getTitle(),
					reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(), reservationPax.getPaxSequence(),
					reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO, reservationPax.getPaxCategory(),
					paymentAssembler, null, null, null, null, null, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo,
					intlFltDepartureDate, pnrPaxGroupId);
		}

		return reservationAssembler;
	}

	private LCCClientPaymentAssembler getPaymentAssembler(BigDecimal refundAmount, ExchangeRateProxy exchangeRateProxy,
			RefundInfoTO refundInfo) throws ModuleException {

		LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();

		Set<String> agentcolMethod = null;
		PaymentMethod paymentMethod = PaymentMethod.getPaymentType(radPaymentType);
		if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())
				|| paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(getSelAgent(selAgent));
			if (hdnSelCurrency == null) {
				hdnSelCurrency = agent.getCurrencyCode();
			}
			if (agent != null) {
				agentcolMethod = agent.getPaymentMethod();
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())
				|| paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userPrincipal.getAgentCode());
			if (agent != null) {
				agentcolMethod = agent.getPaymentMethod();
			}
		}

		String loggedInAgentCode = ((UserPrincipal) request.getUserPrincipal()).getAgentCode();
		String loggedInAgentCurrencyCode = ((UserPrincipal) request.getUserPrincipal()).getAgentCurrencyCode();

		if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
			if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_ONACCOUNT)) {
				PayCurrencyDTO payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(hdnSelCurrency, loggedInAgentCode,
						getSelAgent(selAgent), exchangeRateProxy);
				paymentAssembler.addAgentCreditPayment(getSelAgent(selAgent), refundAmount, payCurrencyDTO, new Date(), null,
						null, Agent.PAYMENT_MODE_ONACCOUNT, refundInfo);
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
			if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_BSP)) {
				// Override the selected currency with Agent country currency code for BSP
				String bspCurrency = ModuleServiceLocator.getCommonServiceBD()
						.getCurrencyCodeForStation(RequestHandlerUtil.getAgentStationCode(request));
				PayCurrencyDTO payCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgentCode,
						getSelAgent(selAgent), exchangeRateProxy);
				paymentAssembler.addAgentCreditPayment(getSelAgent(selAgent), refundAmount, payCurrencyDTO, new Date(), null,
						null, Agent.PAYMENT_MODE_BSP, refundInfo);
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
			if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_CASH)) {
				PayCurrencyDTO payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(loggedInAgentCurrencyCode, loggedInAgentCode,
						loggedInAgentCode, exchangeRateProxy);
				paymentAssembler.addCashPayment(refundAmount, payCurrencyDTO, new Date(), null, null, null, refundInfo);
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
			if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_CREDITCARD)) {
				Integer ipgId = 0;
				Integer intCCdId = null;
				if (!BeanUtils.nullHandler(hdnIPGId).equals("")) {
					ipgId = new Integer(hdnIPGId);
				}
				if (!BeanUtils.nullHandler(cccdId).equals("")) {
					intCCdId = new Integer(cccdId);
				}

				PayCurrencyDTO payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(BeanUtils.nullHandler(hdnSelCurrency),
						ipgId, exchangeRateProxy, false);
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, hdnSelCurrency);
				ipgIdentificationParamsDTO.setFQIPGConfigurationName(ipgId + "_" + hdnSelCurrency);

				paymentAssembler.addInternalCardPayment(new Integer(BeanUtils.nullHandler(selCardType)),
						BeanUtils.nullHandler(cardExpiryDate), BeanUtils.nullHandler(cardNo),
						BeanUtils.nullHandler(cardHoldersName), null, BeanUtils.nullHandler(cardCVV), refundAmount,
						AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, payCurrencyDTO,
						new Date(), BeanUtils.nullHandler(cardHoldersName), new Integer(BeanUtils.nullHandler(tempPayId)),
						tempPayId, intCCdId, null, null, refundInfo);

			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
			Integer ipgId = 0;
			if (!BeanUtils.nullHandler(hdnIPGId).equals("")) {
				ipgId = new Integer(hdnIPGId);
			}
			PayCurrencyDTO payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(getHdnSelCurrency(), ipgId,
					exchangeRateProxy, false);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
					payCurrencyDTO.getPayCurrencyCode());

			paymentAssembler.addOfflineCardPayment(refundAmount, TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO,
					refundInfo.getOriginalPaymentTnxId(), null, payCurrencyDTO);

		}

		return paymentAssembler;
	}

	/**
	 * Checks if the passenger is valid for the currently loaded reservation, i.e. if the traveler reference number
	 * passed from the UI belongs to a passenger in the reservation.
	 * 
	 * If valid then the relevant {@link LCCClientReservationPax} object is returned.
	 * 
	 * @param reservation
	 *            the currently loaded reservation object.
	 * @return true if passenger found, else false.
	 */
	private LCCClientReservationPax getReservationPaxForSelectedPax(Collection<LCCClientReservationPax> passengers) {

		for (LCCClientReservationPax reservationPax : passengers) {

			if (reservationPax.getTravelerRefNumber().equals(selPax)) {
				return reservationPax;
			}
		}
		return null;
	}

	/**
	 * get the selected refund agent
	 * 
	 * @param selAgnet
	 * @return
	 */
	private String getSelAgent(String selAgent) {
		if (selAgent != null && !selAgent.isEmpty()) {
			return selAgent;
		} else {
			return ((UserPrincipal) request.getUserPrincipal()).getAgentCode();
		}
	}

	private boolean isRefundToOthreAgent(String selAgnet) {
		if (selAgnet == null || selAgnet.isEmpty()) {
			return false;
		} else {
			return !selAgnet.equals(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		}
	}

	private BigDecimal getCCChargeAmount(BigDecimal totalPaymentAmountExcludingCCCharge) throws ModuleException {
		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		ExternalChgDTO externalChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD);
		externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();

		if (externalChgDTO.getAmount() == null || BigDecimal.ZERO.equals(externalChgDTO.getAmount())) {
			if (externalChgDTO.isRatioValueInPercentage()) {
				externalChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
			}
		}

		// Updating the booking shopping cart
		bookingShoppingCart.addSelectedExternalCharge(externalChgDTO);

		return externalChgDTO.getAmount();
	}

	private boolean isForcedConfirmed(XBEReservationInfoDTO resInfo, Set<LCCClientReservationPax> passengers) {
		List<LCCClientReservationPax> passengerList = new ArrayList<LCCClientReservationPax>(passengers);

		boolean isAmountDue = false;
		boolean isForcedConfirmed = false;

		for (LCCClientReservationPax pax : passengerList) {
			BigDecimal amountDue = BigDecimal.ZERO;
			amountDue = pax.getTotalAvailableBalance();
			if (amountDue.compareTo(BigDecimal.ZERO) > 0) {
				isAmountDue = true;
			}
		}

		isForcedConfirmed = ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(resInfo.getReservationStatus())
				&& isAmountDue;

		return isForcedConfirmed;
	}

	private BigDecimal getTotalPaxAvailableBalance(Set<LCCClientReservationPax> passengers) {
		List<LCCClientReservationPax> passengerList = new ArrayList<LCCClientReservationPax>(passengers);

		BigDecimal totalAvailableBalance = BigDecimal.ZERO;

		for (LCCClientReservationPax pax : passengerList) {
			BigDecimal amountDue = BigDecimal.ZERO;
			amountDue = pax.getTotalAvailableBalance();
			if (amountDue.compareTo(BigDecimal.ZERO) > 0) {
				totalAvailableBalance = AccelAeroCalculator.add(totalAvailableBalance, pax.getTotalAvailableBalance());
			}
		}

		return totalAvailableBalance;
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(
				payment != null ? payment.getAgent() : ((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public CreditCardTO getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(CreditCardTO creditInfo) {
		this.creditInfo = creditInfo;
	}

	public String getAgentBalance() {
		return agentBalance;
	}

	// Setter methods for OGNL
	public void setTxtPayAmount(String txtPayAmount) {
		this.txtPayAmount = txtPayAmount;
	}

	public void setSelPax(String selPax) {
		this.selPax = selPax;
	}

	public void setTxtPayUN(String txtPayUN) {
		this.txtPayUN = txtPayUN;
	}

	public void setRadPaymentType(String radPaymentType) {
		this.radPaymentType = radPaymentType;
	}

	public void setSelAgent(String selAgent) {
		this.selAgent = selAgent;
	}

	public void setSelCardType(String selCardType) {
		this.selCardType = selCardType;
	}

	public void setCardHoldersName(String cardHoldersName) {
		this.cardHoldersName = cardHoldersName;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}

	public void setCardCVV(String cardCVV) {
		this.cardCVV = cardCVV;
	}

	public void setSelAuthId(String selAuthId) {
		this.selAuthId = selAuthId;
	}

	public String getHdnSelCurrency() {
		return hdnSelCurrency;
	}

	public void setHdnSelCurrency(String hdnSelCurrency) {
		this.hdnSelCurrency = hdnSelCurrency;
	}

	public void setHdnIPGId(String hdnIPGId) {
		this.hdnIPGId = hdnIPGId;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public void setTempPayId(String tempPayId) {
		this.tempPayId = tempPayId;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	public void setLastUserNote(String lastUserNote) {
		this.lastUserNote = lastUserNote;
	}

	public void setTxtPayMode(String txtPayMode) {
		this.txtPayMode = txtPayMode;
	}

	public String getCccdId() {
		return cccdId;
	}

	public void setCccdId(String cccdId) {
		this.cccdId = cccdId;
	}

	public String getPmtdate() {
		return pmtdate;
	}

	public void setPmtdate(String pmtdate) {
		this.pmtdate = pmtdate;
	}

	/**
	 * @return the convertedToGroupReservation
	 */
	public boolean isConvertedToGroupReservation() {
		return convertedToGroupReservation;
	}

	/**
	 * @param convertedToGroupReservation
	 *            the convertedToGroupReservation to set
	 */
	public void setConvertedToGroupReservation(boolean convertedToGroupReservation) {
		this.convertedToGroupReservation = convertedToGroupReservation;
	}

	/**
	 * @param carrierVisePayments
	 *            the carrierVisePayments to set
	 */
	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

	/**
	 * @param lccUniqueTnxId
	 *            the lccUniqueTnxId to set
	 */
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	/**
	 * @param paymentCarrier
	 *            the paymentCarrier to set
	 */
	public void setPaymentCarrier(String paymentCarrier) {
		this.paymentCarrier = paymentCarrier;
	}

	/**
	 * @return the payRef
	 */
	public String getPayRef() {
		return payRef;
	}

	/**
	 * @param payRef
	 *            the payRef to set
	 */
	public void setPayRef(String payRef) {
		this.payRef = payRef;
	}

	public Date getPayDateTime() {
		return payDateTime;
	}

	public void setPayDateTime(Date payDateTime) {
		this.payDateTime = payDateTime;
	}

	public void setRefundTypeOrder(String refundOrder) {
		this.refundTypeOrder = refundOrder;
	}

	public void setTaxRefundedSegment(String taxRefundedSegment) {
		this.taxRefundedSegment = taxRefundedSegment;
	}

	public void setPnrPaxOndChgIDs(String pnrPaxOndChgIDs) throws org.json.simple.parser.ParseException {
		this.pnrPaxOndChgIDs = pnrPaxOndChgIDs != null ? AncillaryJSONUtil.parseLongList(pnrPaxOndChgIDs) : null;
	}

	/**
	 * @return the doAdjustmentOnly
	 */
	public boolean isDoAdjustmentOnly() {
		return doAdjustmentOnly;
	}

	/**
	 * @param doAdjustmentOnly
	 *            the doAdjustmentOnly to set
	 */
	public void setDoAdjustmentOnly(boolean doAdjustmentOnly) {
		this.doAdjustmentOnly = doAdjustmentOnly;
	}

	/**
	 * @return the isFullRefundOperation
	 */
	public String getIsFullRefundOperation() {
		return isFullRefundOperation;
	}

	/**
	 * @param isFullRefundOperation
	 *            the isFullRefundOperation to set
	 */
	public void setIsFullRefundOperation(String isFullRefundOperation) {
		this.isFullRefundOperation = isFullRefundOperation;
	}

	public boolean isActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public void setRemoveAgentCommission(boolean removeAgentCommission) {
		this.removeAgentCommission = removeAgentCommission;
	}

	public GroupBookingRequestTO getGroupBookingRequest() {
		return groupBookingRequest;
	}

	public void setGroupBookingRequest(GroupBookingRequestTO groupBookingRequest) {
		this.groupBookingRequest = groupBookingRequest;
	}

	public String getGroupBookingReference() {
		return groupBookingReference;
	}

	public void setGroupBookingReference(String groupBookingReference) {
		this.groupBookingReference = groupBookingReference;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public boolean isOriginallyGroupPNR() {
		return isOriginallyGroupPNR;
	}

	public void setOriginallyGroupPNR(boolean isOriginallyGroupPNR) {
		this.isOriginallyGroupPNR = isOriginallyGroupPNR;
	}

	public boolean isNonRefundable() {
		return nonRefundable;
	}

	public void setNonRefundable(boolean nonRefundable) {
		this.nonRefundable = nonRefundable;
	}

	public boolean isOhdResExpired() {
		return isOhdResExpired;
	}

	public void setOhdResExpired(boolean isOhdResExpired) {
		this.isOhdResExpired = isOhdResExpired;
	}

	public String getAgentBalanceInAgentCurrency() {
		return agentBalanceInAgentCurrency;
	}

	public void setAgentBalanceInAgentCurrency(String agentBalanceInAgentCurrency) {
		this.agentBalanceInAgentCurrency = agentBalanceInAgentCurrency;
	}
}