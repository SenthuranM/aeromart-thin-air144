package com.isa.thinair.xbe.core.web.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author Lasantha Pambagoda
 * 
 *         This is a special action file to show JSP files directly with out any presentations logic being executed.
 *         File Name itself is used instead of separate result value to make mappings simple. So in the result
 *         annotation filename itself is specified as name as well as the value.
 * 
 *         eg : @Result(name=S2Constants.Jsp.Common.EXAMPLE_FILE, value=S2Constants.Jsp.Common.EXAMPLE_FILE)
 * 
 * 
 *         To work this <constant name="struts.enable.DynamicMethodInvocation" value="true" /> should be set in the
 *         struts.xml file
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({
		@Result(name = S2Constants.Jsp.Common.LOAD_MSG, value = S2Constants.Jsp.Common.LOAD_MSG),
		@Result(name = S2Constants.Jsp.Flight.FLIGHT_REPORT_GRID, value = S2Constants.Jsp.Flight.FLIGHT_REPORT_GRID),
		@Result(name = S2Constants.Jsp.Reporting.ANCILLARY_DETAILS_FLIGHT_REPORT_GRID, value = S2Constants.Jsp.Reporting.ANCILLARY_DETAILS_FLIGHT_REPORT_GRID),

		@Result(name = S2Constants.Jsp.Reservation.SEARCH, value = S2Constants.Jsp.Reservation.SEARCH),
		@Result(name = S2Constants.Jsp.Reservation.FARE_CONDS, value = S2Constants.Jsp.Reservation.FARE_CONDS),

		@Result(name = S2Constants.Jsp.System.ATTACH_FILE, value = S2Constants.Jsp.System.ATTACH_FILE),
		@Result(name = S2Constants.Jsp.User.USER_ADMIN_GRID, value = S2Constants.Jsp.User.USER_ADMIN_GRID),
		@Result(name = S2Constants.Jsp.User.DUMMY_USER, value = S2Constants.Jsp.User.DUMMY_USER),
		@Result(name = S2Constants.Jsp.Reservation.DISPLAY_MEALS, value = S2Constants.Jsp.Reservation.DISPLAY_MEALS),

		@Result(name = S2Constants.Jsp.Reservation.V2_SHOW_MEALS, value = S2Constants.Jsp.Reservation.V2_SHOW_MEALS),
		@Result(name = S2Constants.Jsp.Reservation.TAXES_SURCHARGES, value = S2Constants.Jsp.Reservation.TAXES_SURCHARGES),
		@Result(name = S2Constants.Jsp.Common.BLANK_JSP, value = S2Constants.Jsp.Common.BLANK_JSP),
        @Result(name = S2Constants.Jsp.Alert.SHOW_FLIGHT_PNR_NOTIFICATION_CONTACT_DETAILS_MESSAGE, value = S2Constants.Jsp.Alert.SHOW_FLIGHT_PNR_NOTIFICATION_CONTACT_DETAILS_MESSAGE),
		@Result(name = S2Constants.Jsp.Alert.SHOW_PENDING_EMAIL_SMS_DETAILS_MESSAGE, value = S2Constants.Jsp.Alert.SHOW_PENDING_EMAIL_SMS_DETAILS_MESSAGE)
})
public class ShowFileAction {

	public String loadMsg() {
		return S2Constants.Jsp.Common.LOAD_MSG;
	}

	public String flightGrid() {
		return S2Constants.Jsp.Flight.FLIGHT_REPORT_GRID;
	}

	public String fareConds() {
		return S2Constants.Jsp.Reservation.FARE_CONDS;
	}

	public String attach() {
		return S2Constants.Jsp.System.ATTACH_FILE;
	}

	public String userAdminGrid() {
		return S2Constants.Jsp.User.USER_ADMIN_GRID;
	}

	public String dummyUser() {
		return S2Constants.Jsp.User.DUMMY_USER;
	}

	public String displayMeals() {
		return S2Constants.Jsp.Reservation.DISPLAY_MEALS;
	}

	public String displayMealsV2() {
		return S2Constants.Jsp.Reservation.V2_SHOW_MEALS;
	}

	public String taxesSurcharges() {
		return S2Constants.Jsp.Reservation.TAXES_SURCHARGES;
	}

	public String ancillaryDetailsFlightGrid() {
		return S2Constants.Jsp.Reporting.ANCILLARY_DETAILS_FLIGHT_REPORT_GRID;
	}

	public String dummyPage() {
		return S2Constants.Jsp.Common.BLANK_JSP;
	}

    public String contactDetails() {
        return S2Constants.Jsp.Alert.SHOW_FLIGHT_PNR_NOTIFICATION_CONTACT_DETAILS_MESSAGE;
    }

	public String pendingEmailSMSDetails() {
		return S2Constants.Jsp.Alert.SHOW_PENDING_EMAIL_SMS_DETAILS_MESSAGE;
	}
}
