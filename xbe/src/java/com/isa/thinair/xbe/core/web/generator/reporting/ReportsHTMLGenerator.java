/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.xbe.core.web.generator.reporting;

import com.isa.thinair.xbe.core.web.handler.reporting.CompanyPaymentReportRH;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.model.PaymentMode;
import com.isa.thinair.webplatform.core.commons.AgentStatus;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

/**
 * @author Chamindap
 * 
 */
public class ReportsHTMLGenerator {

	private static Log log = LogFactory.getLog(ReportsHTMLGenerator.class);

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static String createPaymentModeHtml() throws ModuleException {
		Collection payments = null;
		StringBuffer sb = new StringBuffer();

		String strRolesHtml = "var arrData = new Array();" + "arrData[0] = new Array(";

		payments = ModuleServiceLocator.getDataExtractionBD().getPaymentsMode();
		int tempInt = 0;
		PaymentMode paymentMode = null;

		Iterator rolesIter = payments.iterator();
		while (rolesIter.hasNext()) {
			paymentMode = (PaymentMode) rolesIter.next();

			strRolesHtml += "new Array('" + paymentMode.getPaymentDescription() + "','" + paymentMode.getPaymentCode() + "'), ";
			tempInt++;
		}

		strRolesHtml = strRolesHtml.substring(0, strRolesHtml.length() - 2);
		sb.append(strRolesHtml);

		String strRoleslistHtml = "); var lspm = new Listbox('lstPModes', " + "'lstAssignedPModes', 'spn2', 'lspm');"
				+ "lspm.group1 = arrData[0];" + "lspm.height = '100px';" + "lspm.width = '170px';"
				+ "lspm.headingLeft = '&nbsp;&nbsp;All Payment ModesR';"
				+ "lspm.headingRight = '&nbsp;&nbsp;Selected Payment Modes';" + "lspm.drawListBox();";

		sb.append(strRoleslistHtml);

		return sb.toString();
	}
	
	/**
	 * Method is overloaded for translations. Extra parameter ResourceBundle is added.
	 *
	 * @param rb
	 *            Resource bundle which contains key-value pairs for session locale.
	 * 
	 * @throws ModuleException
	 */
	public final static String createPaymentModeHtml(ResourceBundle rb) throws ModuleException {
		Collection payments = null;
		StringBuffer sb = new StringBuffer();

		String strRolesHtml = "var arrData = new Array();" + "arrData[0] = new Array(";

		payments = ModuleServiceLocator.getDataExtractionBD().getPaymentsMode();
		int tempInt = 0;
		PaymentMode paymentMode = null;

		Iterator rolesIter = payments.iterator();
		while (rolesIter.hasNext()) {
			paymentMode = (PaymentMode) rolesIter.next();

			strRolesHtml += "new Array('" + paymentMode.getPaymentDescription() + "','" + paymentMode.getPaymentCode() + "'), ";
			tempInt++;
		}

		strRolesHtml = strRolesHtml.substring(0, strRolesHtml.length() - 2);
		sb.append(strRolesHtml);
		
		String allPaymentModes = null;
		String selectedPaymentModes = null;
		try{
			allPaymentModes = rb.getString("All_Payment_Modes");
			selectedPaymentModes = rb.getString("Selected_Payment_Modes");
		}catch(MissingResourceException e){
			allPaymentModes = "All Payment Modes";
			selectedPaymentModes = "Selected Payment Modes";
		}


		String strRoleslistHtml = "); var lspm = new Listbox('lstPModes', " + "'lstAssignedPModes', 'spn2', 'lspm');"
				+ "lspm.group1 = arrData[0];" + "lspm.height = '100px';" + "lspm.width = '170px';"
				+ "lspm.headingLeft = '&nbsp;&nbsp;"+allPaymentModes+"';"
				+ "lspm.headingRight = '&nbsp;&nbsp;"+selectedPaymentModes+"';" + "lspm.drawListBox();";

		sb.append(strRoleslistHtml);

		return sb.toString();
	}

	/**
	 * Returns the First date of reporting
	 * 
	 * @return String the first date of reporting
	 */
	public final static String getReportingPeriod(int noofmonths) throws ModuleException {

		String date = null;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MONTH, -noofmonths);
		date = dateFormat.format(cal.getTime());

		return date;

	}

	/**
	 * Creates Carrier Code with default carrier Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCarrierCodeWithDefaultHtml(Collection carrierCodess) throws ModuleException {

		Iterator cCodeIter = carrierCodess.iterator();
		StringBuffer sb = new StringBuffer();
		if (carrierCodess != null && carrierCodess.size() > 0) {
			sb.append("var showCarrierCombo = true;");
			sb.append("var arrData = new Array();");
			sb.append("var methods = new Array();");
			int tempInt = 0;
			String carrierCode;
			while (cCodeIter.hasNext()) {
				carrierCode = (String) cCodeIter.next();
				sb.append("methods[" + tempInt + "] = new Array('" + carrierCode + "','" + carrierCode + "'); ");
				tempInt++;
			}

			sb.append("arrData[0] = methods;");
			sb.append("var lscc2 = new Listbox('lstCarrierCodes1', 'lstAssignedCarrierCodes1', 'spn3', 'lscc2');");
			sb.append("lscc2.group1 = arrData[0];");
			sb.append("lscc2.height = '75px'; lscc2.width = '170px';");
			sb.append("lscc2.headingLeft = '&nbsp;&nbsp;All Carrier Codes';");
			sb.append("lscc2.headingRight = '&nbsp;&nbsp;Selected Carrier Codes';");
			sb.append("lscc2.drawListBox();");
		} else {
			sb.append("var showCarrierCombo = false;");
		}

		return sb.toString();

	}

	/**
	 * Gets users agent wise.
	 * 
	 * @param strAgentType
	 * @param blnWithTAs
	 * 
	 * @return String an arry of agents js file
	 */
	public final static String createUserAgentMultiSelect(String strAgents) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Map userMap = null;

		sb.append("var userAgentArr = new Array(); ");
		sb.append("var usersArr = new Array();");

		if (strAgents != null) {
			String[] AgentArr = strAgents.split(",");
			userMap = ModuleServiceLocator.getSecurityBD().getAgentUserSortedMap(new ArrayList<String>(Arrays.asList(AgentArr)));
			Collection tempAgents = userMap.keySet();
			int agentCount = 0;
			for (Iterator iter = tempAgents.iterator(); iter.hasNext();) {
				String agentCode = (String) iter.next();

				Collection<UserTO> tempUsers = (Collection) userMap.get(agentCode);

				String agentName = "";
				int userCount = 0;
				for (UserTO user : tempUsers) {
					agentName = user.getAgentName();
					if (userCount == 0) {
						sb.append("usersArr[" + agentCount + "] = new Array(); ");
					}
					sb.append("usersArr[" + agentCount + "][" + userCount + "] = new Array('" + user.getUserId() + " - "
							+ user.getDisplayName() + "','" + user.getUserId() + "'); ");
					userCount++;
				}

				sb.append("userAgentArr[" + agentCount + "] = '" + agentName + "'; ");

				agentCount++;
			}

		}
		sb.append("var arrGroup3 = new Array();" + "var arrData3 = new Array(); arrData3[0] = new Array();");
		sb.append("var ls2 = new Listbox('lstUsers', 'lstAssignedUsers', 'spn2', 'ls2'); ls2.group1 = userAgentArr; ls2.group2 = arrGroup3;");
		sb.append("ls2.list1 = usersArr; ls2.list2 = arrData3;");

		sb.append("ls2.height = '125px'; ls2.width  = '350px';");
		sb.append("ls2.headingLeft = '&nbsp;&nbsp;All Users';");
		sb.append("ls2.headingRight = '&nbsp;&nbsp;Selected Users';");
		sb.append("ls2.drawListBox();");

		return sb.toString();
	}

	public final static String createAgentGSAMultiSelect(String strAgentType, boolean blnWithTAs, boolean blnWithCOs)
			throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		// String strRoleslistHtml = "";
		StringBuffer strRoleslistHtml = new StringBuffer("");
		StringBuffer strAssignGSAHtml = new StringBuffer("");
		StringBuffer strAgenciesHtml = new StringBuffer("var arrData = new Array();arrData[0] = new Array(");

		strAssignGSAHtml.append("var ls = new Listbox('lstRoles', ");
		strAssignGSAHtml.append("'lstAssignedRoles', 'spn1');");
		strAssignGSAHtml.append("ls.height = '190px';");
		strAssignGSAHtml.append("ls.width  = '350px';");
		strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		strAssignGSAHtml.append("ls.filter = true;");
		strAssignGSAHtml.append("ls.drawListBox();");
		// strAssignGSAHtml.append("ls.filter = true;");

		if (strAgentType != null && !strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

			String agentTypes[] = { strAgentType };
			agents = SelectListGenerator.createAgentsByType(agentTypes);
			if (!agents.isEmpty()) {
				int tempInt = 0;
				Iterator agentIter = agents.iterator();

				while (agentIter.hasNext()) {
					String str[] = (String[]) agentIter.next();
					strAgenciesHtml.append("new Array('" + str[0]);
					strAgenciesHtml.append("','" + str[1] + "'), ");
					tempInt++;
				}

				strAgenciesHtml.setLength(strAgenciesHtml.length() - 2);
				sb.append(strAgenciesHtml.toString());

				strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");

				strAssignGSAHtml.append("); var ls = new Listbox('lstRoles', ");
				strAssignGSAHtml.append("'lstAssignedRoles', 'spn1');");
				strAssignGSAHtml.append("ls.group1 = arrData[0];");
				strAssignGSAHtml.append("ls.height = '190px';");
				strAssignGSAHtml.append("ls.width  = '350px';");
				strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
				strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
				strAssignGSAHtml.append("ls.filter = true;");
				strAssignGSAHtml.append("ls.drawListBox();");

			}

		} else if (strAgentType != null && !strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

			String[] agentType = new String[5];
			agentType[0] = strAgentType;
			String[] gsaAraay = null;
			String strGsaCode = "";
			if (blnWithTAs && blnWithCOs) {
				agentType[1] = "SGSA";
				agentType[2] = "TA";
				agentType[3] = "STA";
				agentType[4] = "CO";
			} else if (blnWithCOs) {
				agentType[1] = "CO";
				agentType[2] = "";
				agentType[3] = "";
				agentType[4] = "";
			} else {
				agentType[1] = "SGSA";
				agentType[2] = "TA";
				agentType[3] = "STA";
				agentType[4] = "";
			}
			gsas = SelectListGenerator.createGSATAList(agentType);
			if (!gsas.isEmpty()) {
				strAgenciesHtml.replace(0, strAgenciesHtml.length(), "");
				strAgenciesHtml.append("var arrGroup1 = new Array(");
				strRoleslistHtml.append("var privArray = new Array();");
				int tempCat = -1;
				Iterator gsaIter = gsas.iterator();

				while (gsaIter.hasNext()) {
					gsaAraay = (String[]) gsaIter.next();
					if (strGsaCode.equals(gsaAraay[0])) {
						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("new Array('");
						strRoleslistHtml.append(gsaAraay[5] + "','");
						strRoleslistHtml.append(gsaAraay[4] + "'), ");
					} else {
						if (tempCat != -1) {

							strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
							strRoleslistHtml.append(");");
						}
						strAgenciesHtml.append("new Array('");
						strAgenciesHtml.append(gsaAraay[1]);
						strAgenciesHtml.append("','");
						strAgenciesHtml.append(gsaAraay[0]);
						strAgenciesHtml.append("'), ");

						tempCat++;

						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("privArray[" + tempCat + "] = new Array(  ");
						if (gsaAraay[4] != null) {

							strRoleslistHtml.append("new Array('");
							strRoleslistHtml.append(gsaAraay[5] + "','");
							strRoleslistHtml.append(gsaAraay[4] + "'), ");
						}

					}

				}

				strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
				strRoleslistHtml.append(");");
				strAgenciesHtml.setLength(strAgenciesHtml.length() - 2);
				strAgenciesHtml.append(");");

				sb.append(strAgenciesHtml.toString());
				sb.append(strRoleslistHtml.toString());

				strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
				strAssignGSAHtml.append("var arrGroup2 = new Array();");
				strAssignGSAHtml.append("var arrData2 = new Array();");
				strAssignGSAHtml.append("arrData2[0] = new Array();");
				strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
				strAssignGSAHtml.append("ls.group1 = arrGroup1;");
				strAssignGSAHtml.append("ls.group2 = arrGroup2;");
				strAssignGSAHtml.append("ls.list1 = privArray;");
				strAssignGSAHtml.append("ls.list2 = arrData2;");
				strAssignGSAHtml.append("ls.height = '190px';");
				strAssignGSAHtml.append("ls.width  = '350px';");
				strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
				strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
				strAssignGSAHtml.append("ls.filter = true;");
				strAssignGSAHtml.append("ls.drawListBox();");

			}
		} else if (strAgentType != null && strAgentType.equals("All")) {
			List lstAgents = SelectListGenerator.createAgentTypesList();
			Iterator ite = lstAgents.iterator();
			String strAgent = "";
			String group = "var g1 = new Array( ";
			String arr1 = "";
			arr1 += "var agents = new Array();";
			int tempCat = 0;

			while (ite.hasNext()) {
				Map keyValues = (Map) ite.next();
				Set keys = keyValues.keySet();
				Iterator keyIterator = keys.iterator();
				while (keyIterator.hasNext()) {
					String agentTpes[] = new String[1];
					String agentType = (String) keyValues.get(keyIterator.next());
					group += "'" + agentType + "', ";
					agentTpes[0] = agentType;

					Collection col = SelectListGenerator.createAgentsByType(agentTpes);
					strAgent = "";
					if (col != null) {
						arr1 += "agents[" + tempCat + "] = new Array( ";

					}
					if (col.size() == 0) {
						arr1 += "new Array(), ";

					} else {
						Iterator iteAgents = col.iterator();

						while (iteAgents.hasNext()) {

							String str[] = (String[]) iteAgents.next();
							String strVal = "";
							for (int j = 0; j < str.length; j++) {
								strVal += str[j] + "','";
							}
							int index1 = strVal.lastIndexOf(",");
							strAgent += "new Array('" + strVal.replaceAll("\"", "").substring(0, index1) + "),";

						}

						int ind1 = strAgent.lastIndexOf(",");
						arr1 += strAgent.substring(0, ind1) + ");";

					}

				}
				tempCat++;

			}

			arr1 = arr1.replace("\',)", "\')");
			if (group.indexOf(',') != -1) {
				sb.append(group.substring(0, group.lastIndexOf(',')) + ");");

			} else {
				sb.append(");");
			}
			sb.append(arr1);

			strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
			strAssignGSAHtml.append("var arrGroup2 = new Array();");
			strAssignGSAHtml.append("var arrData2 = new Array();");
			strAssignGSAHtml.append("arrData2[0] = new Array();");

			strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
			strAssignGSAHtml.append("ls.group1 = g1;");
			strAssignGSAHtml.append("ls.list1 = agents;");
			strAssignGSAHtml.append("ls.height = '190px';");
			strAssignGSAHtml.append("ls.width  = '350px';");
			strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
			strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
			strAssignGSAHtml.append("ls.filter = true;");
			strAssignGSAHtml.append("ls.drawListBox();");

		}
		sb.append(strAssignGSAHtml.toString());
		return sb.toString();
	}
	
	public final static String createAgentGSAMultiSelectV2(String strAgentType, HttpServletRequest request)
			throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		// String strRoleslistHtml = "";
		StringBuffer strRoleslistHtml = new StringBuffer("");
		StringBuffer strAssignGSAHtml = new StringBuffer("");
		StringBuffer strAgenciesHtml = new StringBuffer("var arrData = new Array();arrData[0] = new Array(");

		strAssignGSAHtml.append("var ls = new Listbox('lstRoles', ");
		strAssignGSAHtml.append("'lstAssignedRoles', 'spn1');");
		strAssignGSAHtml.append("ls.height = '190px';");
		strAssignGSAHtml.append("ls.width  = '350px';");
		strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		strAssignGSAHtml.append("ls.filter = true;");
		strAssignGSAHtml.append("ls.drawListBox();");
		// strAssignGSAHtml.append("ls.filter = true;");
		if (strAgentType != null && !strAgentType.equals("") && !strAgentType.equals("All")) {

			String[] agentType = new String[5];
			agentType[0] = strAgentType;
			String[] gsaAraay = null;
			String strGsaCode = "";
			Collection<String> selectedAgentTypes = Arrays.asList(request.getParameter("hdnReportingAgentTypes").split(","));
			Iterator<String> iterator = selectedAgentTypes.iterator();

			for (int i = 1; i < 5; i++) {
				if (iterator.hasNext()) {
					String subAgentType = iterator.next();
					if (subAgentType != null && !subAgentType.isEmpty()) {
						agentType[i] = subAgentType;
					} else {
						i--;
					}
				} else {
					agentType[i] = "";
				}
			}

			if (AppSysParamsUtil.isGSAStructureVersion2Enabled() && !AppSysParamsUtil.getCarrierAgent()
					.equals(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode())) {
				Collection<String> agentTypes = Arrays.asList(agentType);
				Collection<String> gsaTypes = new ArrayList<>();
				gsaTypes.add(strAgentType);
				boolean includeGsa = agentTypes.contains(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode());
				Collection<Agent> agentList = ModuleServiceLocator.getTravelAgentBD().getReportingAgentsForGSA(
						((UserPrincipal) request.getUserPrincipal()).getAgentCode(), gsaTypes, includeGsa);
				Collection<String> agentCodes = new ArrayList<>();
				for (Agent agent : agentList) {
					agentCodes.add(agent.getAgentCode());
				}
				Map<String, Collection> params = new HashMap<>();
				params.put("agentCodes", agentCodes);
				params.put("agentTypeCodes", agentTypes);
				gsas = SelectListGenerator.createGivenSubAgentList(params);
			} else if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
				Map<String, Collection> params = new HashMap<>();
				Collection<String> agentTypeCodes = new ArrayList<>();
				Collection<String> subAgentTypeCodes = new ArrayList<>();
				agentTypeCodes.addAll(selectedAgentTypes);
				agentTypeCodes.add(strAgentType);
				subAgentTypeCodes.addAll(selectedAgentTypes);
				params.put("agentTypeCodes", agentTypeCodes);
				params.put("subAgentTypeCodes", subAgentTypeCodes);
				gsas = SelectListGenerator.createGSATAV2List(params);
			} else {
				gsas = SelectListGenerator.createGSATAList(agentType);
			}
			if (!gsas.isEmpty()) {
				strAgenciesHtml.replace(0, strAgenciesHtml.length(), "");
				strAgenciesHtml.append("var arrGroup1 = new Array(");
				strRoleslistHtml.append("var privArray = new Array();");
				int tempCat = -1;
				Iterator gsaIter = gsas.iterator();

				while (gsaIter.hasNext()) {
					gsaAraay = (String[]) gsaIter.next();
					if (strGsaCode.equals(gsaAraay[0])) {
						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("new Array('");
						strRoleslistHtml.append(gsaAraay[5] + "','");
						strRoleslistHtml.append(gsaAraay[4] + "'), ");
					} else {
						if (tempCat != -1) {

							strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
							strRoleslistHtml.append(");");
						}
						strAgenciesHtml.append("new Array('");
						strAgenciesHtml.append(gsaAraay[1]);
						strAgenciesHtml.append("','");
						strAgenciesHtml.append(gsaAraay[0]);
						strAgenciesHtml.append("'), ");

						tempCat++;

						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("privArray[" + tempCat + "] = new Array(  ");
						if (gsaAraay[4] != null) {

							strRoleslistHtml.append("new Array('");
							strRoleslistHtml.append(gsaAraay[5] + "','");
							strRoleslistHtml.append(gsaAraay[4] + "'), ");
						}

					}

				}

				strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
				strRoleslistHtml.append(");");
				strAgenciesHtml.setLength(strAgenciesHtml.length() - 2);
				strAgenciesHtml.append(");");

				sb.append(strAgenciesHtml.toString());
				sb.append(strRoleslistHtml.toString());

				strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
				strAssignGSAHtml.append("var arrGroup2 = new Array();");
				strAssignGSAHtml.append("var arrData2 = new Array();");
				strAssignGSAHtml.append("arrData2[0] = new Array();");
				strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
				strAssignGSAHtml.append("ls.group1 = arrGroup1;");
				strAssignGSAHtml.append("ls.group2 = arrGroup2;");
				strAssignGSAHtml.append("ls.list1 = privArray;");
				strAssignGSAHtml.append("ls.list2 = arrData2;");
				strAssignGSAHtml.append("ls.height = '190px';");
				strAssignGSAHtml.append("ls.width  = '350px';");
				strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
				strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
				strAssignGSAHtml.append("ls.filter = true;");
				strAssignGSAHtml.append("ls.drawListBox();");

			}
		} else if (strAgentType != null && strAgentType.equals("All")) {
			List lstAgents = null;
			if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
				lstAgents = SelectListGenerator
						.createAgentSubTypesList(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode());
			} else {
				lstAgents = SelectListGenerator.createAgentTypesList();
			}
			Iterator ite = lstAgents.iterator();
			String strAgent = "";
			String group = "var g1 = new Array( ";
			String arr1 = "";
			arr1 += "var agents = new Array();";
			int tempCat = 0;
			OUTER: while (ite.hasNext()) {
				Map keyValues = (Map) ite.next();
				Set keys = keyValues.keySet();
				Iterator keyIterator = keys.iterator();
				while (keyIterator.hasNext()) {
					String agentTpes[] = new String[1];
					String agentType = (String) keyValues.get(keyIterator.next());
					agentTpes[0] = agentType;
					Collection col = null;
					if (AppSysParamsUtil.isGSAStructureVersion2Enabled() && !AppSysParamsUtil.getCarrierAgent()
							.equals(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode())) {
						Collection<String> selectedAgentTypes = new ArrayList<>();
						selectedAgentTypes.addAll(Arrays.asList(request.getParameter("hdnReportingAgentTypes").split(",")));
						selectedAgentTypes.add(agentType);
						boolean includeGSA = selectedAgentTypes
								.contains(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode());
						Collection<Agent> agentList = ModuleServiceLocator.getTravelAgentBD().getReportingAgentsForGSA(
								((UserPrincipal) request.getUserPrincipal()).getAgentCode(), selectedAgentTypes, includeGSA);
						col = SelectListGenerator.createAgentList(agentList);
					} else {
						col = SelectListGenerator.createAgentsByType(agentTpes);
					}

					strAgent = "";

					if (col != null) {
						if (col.size() == 0) {
							continue OUTER;
						}
						arr1 += "agents[" + tempCat + "] = new Array( ";

					}

					group += "'" + agentType + "', ";

					if (col.size() == 0) {
						arr1 += "new Array() );";

					} else {
						Iterator iteAgents = col.iterator();

						while (iteAgents.hasNext()) {

							String str[] = (String[]) iteAgents.next();
							String strVal = "";
							for (int j = 0; j < str.length; j++) {
								strVal += str[j] + "','";
							}
							int index1 = strVal.lastIndexOf(",");
							strAgent += "new Array('" + strVal.replaceAll("\"", "").substring(0, index1) + "),";

						}

						int ind1 = strAgent.lastIndexOf(",");
						arr1 += strAgent.substring(0, ind1) + ");";

					}

				}
				tempCat++;

			}

			arr1 = arr1.replace("\',)", "\')");
			if (group.indexOf(',') != -1) {
				sb.append(group.substring(0, group.lastIndexOf(',')) + ");");

			} else {
				sb.append(");");
			}
			sb.append(arr1);

			strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
			strAssignGSAHtml.append("var arrGroup2 = new Array();");
			strAssignGSAHtml.append("var arrData2 = new Array();");
			strAssignGSAHtml.append("arrData2[0] = new Array();");

			strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
			strAssignGSAHtml.append("ls.group1 = g1;");
			strAssignGSAHtml.append("ls.list1 = agents;");
			strAssignGSAHtml.append("ls.height = '190px';");
			strAssignGSAHtml.append("ls.width  = '350px';");
			strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
			strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
			strAssignGSAHtml.append("ls.filter = true;");
			strAssignGSAHtml.append("ls.drawListBox();");

		}
		sb.append(strAssignGSAHtml.toString());
		return sb.toString();
	}
	/**
	 * Method is overloaded for translations. Extra parameter ResourceBundle is added.
	 *
	 * @param strAgentType
	 * 
	 * @param blnWithTAs
	 * 
	 * @param blnWithCOs
	 *  
	 * @param rb
	 *            Resource bundle which contains key-value pairs for session locale.
	 * 
	 * @throws ModuleException
	 */
	public final static String createAgentGSAMultiSelect(String strAgentType, boolean blnWithTAs, boolean blnWithCOs, ResourceBundle rb, String agentStatus)
			throws ModuleException {
		// 3 3 3 
		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		// String strRoleslistHtml = "";
		StringBuffer strRoleslistHtml = new StringBuffer("");
		StringBuffer strAssignGSAHtml = new StringBuffer("");
		StringBuffer strAgenciesHtml = new StringBuffer("var arrData = new Array();arrData[0] = new Array(");
		
		//for translations
		String allAgents=null;
		String selectedAgents=null;
		try{
			allAgents = rb.getString("All_Agents");
			selectedAgents = rb.getString("Selected_Agents");
		}catch(MissingResourceException e){
			allAgents = "All Agents";
			selectedAgents = "Selected Agents";
		}

		strAssignGSAHtml.append("var ls = new Listbox('lstRoles', ");
		strAssignGSAHtml.append("'lstAssignedRoles', 'spn1');");
		strAssignGSAHtml.append("ls.height = '190px';");
		strAssignGSAHtml.append("ls.width  = '350px';");
		strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;"+allAgents+"';");
		strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;"+selectedAgents+"';");
		strAssignGSAHtml.append("ls.filter = true;");
		strAssignGSAHtml.append("ls.drawListBox();");
		// strAssignGSAHtml.append("ls.filter = true;");

		if (strAgentType != null && !strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

			if(agentStatus.equals(AgentStatus.ACTIVE.getStatus()) || agentStatus.equals(AgentStatus.INACTIVE.getStatus())){
				String agentTypes[] = { strAgentType, agentStatus};
				agents = SelectListGenerator.createAgentsByTypeAndStatus(agentTypes);
				
			}else{
				
				String agentTypes[] = { strAgentType };
				agents = SelectListGenerator.createAgentsByType(agentTypes);
			}
			
			if (!agents.isEmpty()) {
				int tempInt = 0;
				Iterator agentIter = agents.iterator();

				while (agentIter.hasNext()) {
					String str[] = (String[]) agentIter.next();
					strAgenciesHtml.append("new Array('" + str[0]);
					strAgenciesHtml.append("','" + str[1] + "'), ");
					tempInt++;
				}

				strAgenciesHtml.setLength(strAgenciesHtml.length() - 2);
				sb.append(strAgenciesHtml.toString());

				strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");

				strAssignGSAHtml.append("); var ls = new Listbox('lstRoles', ");
				strAssignGSAHtml.append("'lstAssignedRoles', 'spn1');");
				strAssignGSAHtml.append("ls.group1 = arrData[0];");
				strAssignGSAHtml.append("ls.height = '190px';");
				strAssignGSAHtml.append("ls.width  = '350px';");
				strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;"+allAgents+"';");
				strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;"+selectedAgents+"';");
				strAssignGSAHtml.append("ls.filter = true;");
				strAssignGSAHtml.append("ls.drawListBox();");

			}

		} else if (strAgentType != null && !strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

			List<String> agentTypeList = new ArrayList<>();
			
			agentTypeList.add(strAgentType);
			
			if (agentStatus.equals(AgentStatus.ACTIVE.getStatus())
					|| agentStatus.equals(AgentStatus.INACTIVE.getStatus())) {
				agentTypeList.add(agentStatus);
			} 

			String[] gsaAraay = null;
			String strGsaCode = "";
			if (blnWithTAs && blnWithCOs) {
				agentTypeList.add("SGSA");
				agentTypeList.add("TA");
				agentTypeList.add("STA");
				agentTypeList.add("CO");

			} else if (blnWithCOs) {

				agentTypeList.add("CO");
				agentTypeList.add("");
				agentTypeList.add("");
				agentTypeList.add("");
			} else {
				agentTypeList.add("SGSA");
				agentTypeList.add("TA");
				agentTypeList.add("STA");
				agentTypeList.add("");
			}
			 

			if (agentStatus.equals(AgentStatus.ACTIVE.getStatus())
					|| agentStatus.equals(AgentStatus.INACTIVE.getStatus())) {
				
				String[] agentType = new String[agentTypeList.size()];
				agentTypeList.toArray(agentType);
				gsas = SelectListGenerator.createGSATAListByStatus(agentType);

			} else {
				String[] agentType = new String[agentTypeList.size()];
				agentTypeList.toArray(agentType);
				gsas = SelectListGenerator.createGSATAList(agentType);
			}

			if (!gsas.isEmpty()) {
				strAgenciesHtml.replace(0, strAgenciesHtml.length(), "");
				strAgenciesHtml.append("var arrGroup1 = new Array(");
				strRoleslistHtml.append("var privArray = new Array();");
				int tempCat = -1;
				Iterator gsaIter = gsas.iterator();

				while (gsaIter.hasNext()) {
					gsaAraay = (String[]) gsaIter.next();
					if (strGsaCode.equals(gsaAraay[0])) {
						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("new Array('");
						strRoleslistHtml.append(gsaAraay[5] + "','");
						strRoleslistHtml.append(gsaAraay[4] + "'), ");
					} else {
						if (tempCat != -1) {

							strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
							strRoleslistHtml.append(");");
						}
						strAgenciesHtml.append("new Array('");
						strAgenciesHtml.append(gsaAraay[1]);
						strAgenciesHtml.append("','");
						strAgenciesHtml.append(gsaAraay[0]);
						strAgenciesHtml.append("'), ");

						tempCat++;

						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("privArray[" + tempCat + "] = new Array(  ");
						if (gsaAraay[4] != null) {

							strRoleslistHtml.append("new Array('");
							strRoleslistHtml.append(gsaAraay[5] + "','");
							strRoleslistHtml.append(gsaAraay[4] + "'), ");
						}

					}

				}

				strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
				strRoleslistHtml.append(");");
				strAgenciesHtml.setLength(strAgenciesHtml.length() - 2);
				strAgenciesHtml.append(");");

				sb.append(strAgenciesHtml.toString());
				sb.append(strRoleslistHtml.toString());

				strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
				strAssignGSAHtml.append("var arrGroup2 = new Array();");
				strAssignGSAHtml.append("var arrData2 = new Array();");
				strAssignGSAHtml.append("arrData2[0] = new Array();");
				strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
				strAssignGSAHtml.append("ls.group1 = arrGroup1;");
				strAssignGSAHtml.append("ls.group2 = arrGroup2;");
				strAssignGSAHtml.append("ls.list1 = privArray;");
				strAssignGSAHtml.append("ls.list2 = arrData2;");
				strAssignGSAHtml.append("ls.height = '190px';");
				strAssignGSAHtml.append("ls.width  = '350px';");
				strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;"+allAgents+"';");
				strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;"+selectedAgents+"';");
				strAssignGSAHtml.append("ls.filter = true;");
				strAssignGSAHtml.append("ls.drawListBox();");

			}
		} else if (strAgentType != null && strAgentType.equals("All")) {
	
			List lstAgents = SelectListGenerator.createAgentTypesList();
			Iterator ite = lstAgents.iterator();
			String strAgent = "";
			String group = "var g1 = new Array( ";
			String arr1 = "";
			arr1 += "var agents = new Array();";
			int tempCat = 0;

			while (ite.hasNext()) {
				Map keyValues = (Map) ite.next();
				Set keys = keyValues.keySet();
				Iterator keyIterator = keys.iterator();
				while (keyIterator.hasNext()) {
					List<String> agentTypeList = new ArrayList<>();
				
					String agentType = (String) keyValues.get(keyIterator.next());
					group += "'" + agentType + "', ";
					
					agentTypeList.add(agentType);
					Collection col = null;

					if (agentStatus.equals(AgentStatus.ACTIVE.getStatus())
							|| agentStatus.equals(AgentStatus.INACTIVE.getStatus())) {
						agentTypeList.add(agentStatus);
						String[] agentArray = new String[agentTypeList.size()];
						agentTypeList.toArray(agentArray);
						col = SelectListGenerator.createAgentsByTypeAndStatus(agentArray);

					} else {
						String[] agentArray = new String[agentTypeList.size()];
						agentTypeList.toArray(agentArray);
						col = SelectListGenerator.createAgentsByType(agentArray);
					}

					strAgent = "";
					if (col != null) {
						arr1 += "agents[" + tempCat + "] = new Array( ";

					}
					if (col.size() == 0) {
						arr1 += "new Array(), ";

					} else {
						Iterator iteAgents = col.iterator();

						while (iteAgents.hasNext()) {

							String str[] = (String[]) iteAgents.next();
							String strVal = "";
							for (int j = 0; j < str.length; j++) {
								strVal += str[j] + "','";
							}
							int index1 = strVal.lastIndexOf(",");
							strAgent += "new Array('" + strVal.replaceAll("\"", "").substring(0, index1) + "),";

						}

						int ind1 = strAgent.lastIndexOf(",");
						arr1 += strAgent.substring(0, ind1) + ");";

					}

				}
				tempCat++;

			}

			arr1 = arr1.replace("\',)", "\')");
			if (group.indexOf(',') != -1) {
				sb.append(group.substring(0, group.lastIndexOf(',')) + ");");

			} else {
				sb.append(");");
			}
			sb.append(arr1);

			strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
			strAssignGSAHtml.append("var arrGroup2 = new Array();");
			strAssignGSAHtml.append("var arrData2 = new Array();");
			strAssignGSAHtml.append("arrData2[0] = new Array();");

			strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
			strAssignGSAHtml.append("ls.group1 = g1;");
			strAssignGSAHtml.append("ls.list1 = agents;");
			strAssignGSAHtml.append("ls.height = '190px';");
			strAssignGSAHtml.append("ls.width  = '350px';");
			strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;"+allAgents+"';");
			strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;"+selectedAgents+"';");
			strAssignGSAHtml.append("ls.filter = true;");
			strAssignGSAHtml.append("ls.drawListBox();");

		}
		sb.append(strAssignGSAHtml.toString());
		return sb.toString();
	}

	public final static String createAgentGSAMultiSelectV2(String strAgentType, HttpServletRequest request, ResourceBundle rb)
			throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		// String strRoleslistHtml = "";
		StringBuffer strRoleslistHtml = new StringBuffer("");
		StringBuffer strAssignGSAHtml = new StringBuffer("");
		StringBuffer strAgenciesHtml = new StringBuffer("var arrData = new Array();arrData[0] = new Array(");

		// for translations
		String allAgents = null;
		String selectedAgents = null;
		try {
			allAgents = rb.getString("All_Agents");
			selectedAgents = rb.getString("Selected_Agents");
		} catch (MissingResourceException e) {
			allAgents = "All Agents";
			selectedAgents = "Selected Agents";
		}

		strAssignGSAHtml.append("var ls = new Listbox('lstRoles', ");
		strAssignGSAHtml.append("'lstAssignedRoles', 'spn1');");
		strAssignGSAHtml.append("ls.height = '190px';");
		strAssignGSAHtml.append("ls.width  = '350px';");
		strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;" + allAgents + "';");
		strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;" + selectedAgents + "';");
		strAssignGSAHtml.append("ls.filter = true;");
		strAssignGSAHtml.append("ls.drawListBox();");
		// strAssignGSAHtml.append("ls.filter = true;");

		if (strAgentType != null && !strAgentType.equals("") && !strAgentType.equals("All")) {

			String[] agentType = new String[5];
			agentType[0] = strAgentType;
			String[] gsaAraay = null;
			String strGsaCode = "";
			Collection<String> selectedAgentTypes = Arrays.asList(request.getParameter("hdnReportingAgentTypes").split(","));
			Iterator<String> iterator = selectedAgentTypes.iterator();

			for (int i = 1; i < 5; i++) {
				if (iterator.hasNext()) {
					String subAgentType = iterator.next();
					if (subAgentType != null && !subAgentType.isEmpty()) {
						agentType[i] = subAgentType;
					} else {
						i--;
					}
				} else {
					agentType[i] = "";
				}
			}
			if (AppSysParamsUtil.isGSAStructureVersion2Enabled() && !AppSysParamsUtil.getCarrierAgent()
					.equals(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode())) {

				Collection<String> agentTypes = Arrays.asList(agentType);
				Collection<String> gsaTypes = new ArrayList<>();
				gsaTypes.add(strAgentType);
				boolean includeGsa = agentTypes.contains(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode());
				Collection<Agent> agentList = ModuleServiceLocator.getTravelAgentBD().getReportingAgentsForGSA(
						((UserPrincipal) request.getUserPrincipal()).getAgentCode(), gsaTypes, includeGsa);
				Collection<String> agentCodes = new ArrayList<>();
				for (Agent agent : agentList) {
					agentCodes.add(agent.getAgentCode());
				}
				agentCodes.add(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
				Map<String, Collection> params = new HashMap<>();
				params.put("agentCodes", agentCodes);
				params.put("agentTypeCodes", agentTypes);
				gsas = SelectListGenerator.createGivenSubAgentList(params);
			} else if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
				Map<String, Collection> params = new HashMap<>();
				Collection<String> agentTypeCodes = new ArrayList<>();
				Collection<String> subAgentTypeCodes = new ArrayList<>();
				agentTypeCodes.add(strAgentType);
				subAgentTypeCodes.addAll(selectedAgentTypes);
				params.put("agentTypeCodes", agentTypeCodes);
				params.put("subAgentTypeCodes", subAgentTypeCodes);
				gsas = SelectListGenerator.createGSATAV2List(params);
			} else {
				gsas = SelectListGenerator.createGSATAList(agentType);
			}
			if (!gsas.isEmpty()) {
				strAgenciesHtml.replace(0, strAgenciesHtml.length(), "");
				strAgenciesHtml.append("var arrGroup1 = new Array(");
				strRoleslistHtml.append("var privArray = new Array();");
				int tempCat = -1;
				Iterator gsaIter = gsas.iterator();

				while (gsaIter.hasNext()) {
					gsaAraay = (String[]) gsaIter.next();
					if (strGsaCode.equals(gsaAraay[0])) {
						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("new Array('");
						strRoleslistHtml.append(gsaAraay[5] + "','");
						strRoleslistHtml.append(gsaAraay[4] + "'), ");
					} else {
						if (tempCat != -1) {

							strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
							strRoleslistHtml.append(");");
						}
						strAgenciesHtml.append("new Array('");
						strAgenciesHtml.append(gsaAraay[1]);
						strAgenciesHtml.append("','");
						strAgenciesHtml.append(gsaAraay[0]);
						strAgenciesHtml.append("'), ");

						tempCat++;

						strGsaCode = gsaAraay[0];
						strRoleslistHtml.append("privArray[" + tempCat + "] = new Array(  ");
						if (gsaAraay[4] != null) {

							strRoleslistHtml.append("new Array('");
							strRoleslistHtml.append(gsaAraay[5] + "','");
							strRoleslistHtml.append(gsaAraay[4] + "'), ");
						}

					}

				}

				strRoleslistHtml.setLength(strRoleslistHtml.length() - 2);
				strRoleslistHtml.append(");");
				strAgenciesHtml.setLength(strAgenciesHtml.length() - 2);
				strAgenciesHtml.append(");");

				sb.append(strAgenciesHtml.toString());
				sb.append(strRoleslistHtml.toString());

				strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
				strAssignGSAHtml.append("var arrGroup2 = new Array();");
				strAssignGSAHtml.append("var arrData2 = new Array();");
				strAssignGSAHtml.append("arrData2[0] = new Array();");
				strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
				strAssignGSAHtml.append("ls.group1 = arrGroup1;");
				strAssignGSAHtml.append("ls.group2 = arrGroup2;");
				strAssignGSAHtml.append("ls.list1 = privArray;");
				strAssignGSAHtml.append("ls.list2 = arrData2;");
				strAssignGSAHtml.append("ls.height = '190px';");
				strAssignGSAHtml.append("ls.width  = '350px';");
				strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;" + allAgents + "';");
				strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;" + selectedAgents + "';");
				strAssignGSAHtml.append("ls.filter = true;");
				strAssignGSAHtml.append("ls.drawListBox();");

			}
		} else if (strAgentType != null && strAgentType.equals("All")) {
			List lstAgents = SelectListGenerator.createAgentTypesList();
			Iterator ite = lstAgents.iterator();
			String strAgent = "";
			String group = "var g1 = new Array( ";
			String arr1 = "";
			arr1 += "var agents = new Array();";
			int tempCat = 0;

			while (ite.hasNext()) {
				Map keyValues = (Map) ite.next();
				Set keys = keyValues.keySet();
				Iterator keyIterator = keys.iterator();
				while (keyIterator.hasNext()) {
					String agentTpes[] = new String[1];
					String agentType = (String) keyValues.get(keyIterator.next());
					group += "'" + agentType + "', ";
					agentTpes[0] = agentType;

					Collection col = SelectListGenerator.createAgentsByType(agentTpes);
					strAgent = "";
					if (col != null) {
						arr1 += "agents[" + tempCat + "] = new Array( ";

					}
					if (col.size() == 0) {
						arr1 += "new Array(), ";

					} else {
						Iterator iteAgents = col.iterator();

						while (iteAgents.hasNext()) {

							String str[] = (String[]) iteAgents.next();
							String strVal = "";
							for (int j = 0; j < str.length; j++) {
								strVal += str[j] + "','";
							}
							int index1 = strVal.lastIndexOf(",");
							strAgent += "new Array('" + strVal.replaceAll("\"", "").substring(0, index1) + "),";

						}

						int ind1 = strAgent.lastIndexOf(",");
						arr1 += strAgent.substring(0, ind1) + ");";

					}

				}
				tempCat++;

			}

			arr1 = arr1.replace("\',)", "\')");
			if (group.indexOf(',') != -1) {
				sb.append(group.substring(0, group.lastIndexOf(',')) + ");");

			} else {
				sb.append(");");
			}
			sb.append(arr1);

			strAssignGSAHtml.replace(0, strAssignGSAHtml.length(), "");
			strAssignGSAHtml.append("var arrGroup2 = new Array();");
			strAssignGSAHtml.append("var arrData2 = new Array();");
			strAssignGSAHtml.append("arrData2[0] = new Array();");

			strAssignGSAHtml.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
			strAssignGSAHtml.append("ls.group1 = g1;");
			strAssignGSAHtml.append("ls.list1 = agents;");
			strAssignGSAHtml.append("ls.height = '190px';");
			strAssignGSAHtml.append("ls.width  = '350px';");
			strAssignGSAHtml.append("ls.headingLeft = '&nbsp;&nbsp;" + allAgents + "';");
			strAssignGSAHtml.append("ls.headingRight = '&nbsp;&nbsp;" + selectedAgents + "';");
			strAssignGSAHtml.append("ls.filter = true;");
			strAssignGSAHtml.append("ls.drawListBox();");

		}
		sb.append(strAssignGSAHtml.toString());
		return sb.toString();
	}

	public final static String createAgentGSAMultiSelectStation(String strAgentType, boolean blnWithTAs, boolean blnWithCOs)
			throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		String strAssignGSAHtml = "";
		boolean blnTA = false;
		boolean blnAll = false;
		String defaultAirlineCode = AppSysParamsUtil.getDefaultAirlineIdentifierCode();

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		if (strAgentType != null) {

			if (strAgentType.equals("TA") || strAgentType.equals("All")) { // if
																			// TA
																			// or
																			// All
																			// selected

				if (strAgentType.equals("TA")) { // For TA get Station list
					blnTA = true;
					agents = SelectListGenerator.createOwnStationCodes();
				}

				if (strAgentType.equals("All")) { // for All get Agent Type
													// List
					blnAll = true;
					agents = SelectListGenerator.createOwnStationCodes();
				}

				Iterator ite = agents.iterator();
				sb.append("var stns = new Array(); ");

				sb.append(" var agentsArr = new Array();");
				int tempCat = 0;
				int tinc = 0;
				Collection col = null;
				Map keyValues = null;
				Set keys = null;
				Iterator keyIterator = null;
				String agentTpes[] = new String[2];
				String agentType = "";

				while (ite.hasNext()) {

					keyValues = (Map) ite.next();
					keys = keyValues.keySet();
					keyIterator = keys.iterator();

					while (keyIterator.hasNext()) {

						agentType = (String) keyValues.get(keyIterator.next());

						// Populate the Group Array
						sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
						agentTpes[0] = agentType;
						agentTpes[1] = defaultAirlineCode;

						if (blnTA) { // For TA get Station wise agents
							col = SelectListGenerator.createOwnTAsByStation(agentTpes);
						}
						if (blnAll) {// For All get Agent type wise Agents
							// col =
							// SelectListGenerator.createAgentsByType(agentTpes);
							// sudheera
							col = SelectListGenerator.createOwnAgentsByStation(agentTpes);
						}

						if (col != null) {
							sb.append("var agents_" + tempCat + " = new Array(); ");

						}
						if (col.size() == 0) {
							sb.append("new Array();");

						} else {
							Iterator iteAgents = col.iterator();
							tinc = 0;
							while (iteAgents.hasNext()) {

								String str[] = (String[]) iteAgents.next();
								sb.append("agents_" + tempCat + "[" + tinc + "] = ");
								sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
								tinc++;

							}

						}
					}
					sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
					tempCat++;

				}

				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

				+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

				sb.append("var agts = new Array(); ");
				String params[] = { strAgentType, defaultAirlineCode };
				agents = SelectListGenerator.createOwnAgentsByType(params);
				if (!agents.isEmpty()) {
					int tempInt = 0;
					Iterator agentIter = agents.iterator();

					while (agentIter.hasNext()) {
						String str[] = (String[]) agentIter.next();
						sb.append("agts[" + tempInt + "] = new Array('" + str[0] + "','" + str[1] + "'); ");
						tempInt++;
					}
					strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.group1 = agts;";
				}

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

				String[] agentType = new String[5];
				agentType[0] = strAgentType;
				agentType[1] = defaultAirlineCode;
				String[] gsaAraay = null;
				String strGsaCode = "";
				int tinc = 0;
				if (blnWithTAs && blnWithCOs) {
					agentType[2] = "TA";
					agentType[3] = "CO";
				} else if (blnWithCOs) {
					agentType[2] = "CO";
					agentType[3] = "";
				} else {
					agentType[2] = "TA";
					agentType[3] = "";
				}
				agentType[4] = defaultAirlineCode;

				gsas = SelectListGenerator.createOwnGSATAList(agentType);
				if (!gsas.isEmpty()) {

					sb.append("var stns = new Array();");
					sb.append("var agentsArr = new Array();");
					int tempCat = -1;
					int check = 0;
					Iterator gsaIter = gsas.iterator();
					while (gsaIter.hasNext()) {
						gsaAraay = (String[]) gsaIter.next();
						if (strGsaCode.equals(gsaAraay[0])) {
							strGsaCode = gsaAraay[0];
							sb.append(" agents_" + tempCat + "[" + tinc + "] = ");
							sb.append("	new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "');");

							tinc++;
						} else {
							tempCat++;

							tinc = 0;
							sb.append("stns[" + tempCat + "] = new Array('" + gsaAraay[1] + "','" + gsaAraay[0] + "'); ");

							sb.append("var agents_" + tempCat + " = new Array(); ");

							strGsaCode = gsaAraay[0];

							if (gsaAraay[4] != null) {

								sb.append("agents_" + tempCat + "[" + tinc + "] = new Array('" + gsaAraay[5] + "','"
										+ gsaAraay[4] + "'); ");
								tinc++;
							}

						}

						if (tempCat > check) {
							sb.append("agentsArr[" + check + "] = agents_" + check + ";");
							check++;
						}

					}
					sb.append("agentsArr[" + check + "] = agents_" + check + ";");
					strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();"
							+ "arrData2[0] = new Array();"

							+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
							+ "ls.group1 = stns; ls.group2 = arrGroup2;" + "ls.list1 = agentsArr; ls.list2 = arrData2;";
				}
			}

		}
		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '190px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	public final static String createTAsOfGSAMultiSelect(String gsaCode, boolean blnWithTAs) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		List gsas = null;
		String strRoleslistHtml = "";
		String strAssignGSAHtml = "";
		String strAgenciesHtml = "";

		strAgenciesHtml = "var arrData = new Array();arrData[0] = new Array(";
		strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.height = '200px';"
				+ "ls.width  = '350px';" + "ls.headingLeft = '&nbsp;&nbsp;All Agents';"
				+ "ls.headingRight = '&nbsp;&nbsp;Selected Agents';" + "ls.drawListBox();";
		if (gsaCode != null && !gsaCode.equals("") && !gsaCode.equals("All") && blnWithTAs) {

			String[] agentType = new String[1];
			agentType[0] = gsaCode;
			String[] gsaAraay = null;

			gsas = (List) SelectListGenerator.createTasFromGSA(agentType);
			if (!gsas.isEmpty()) {
				strAgenciesHtml = "var arrGroup1 = new Array(";
				strRoleslistHtml += "var privArray = new Array();";
				if (gsas.size() > 0) {
					strAgenciesHtml += "new Array('" + ((String[]) gsas.get(0))[1] + "','" + ((String[]) gsas.get(0))[0] + "') ";
				}
				Iterator gsaIter = gsas.iterator();

				while (gsaIter.hasNext()) {
					gsaAraay = (String[]) gsaIter.next();
					strAgenciesHtml += ", new Array('" + gsaAraay[3] + "','" + gsaAraay[2] + "') ";
				}
				strAgenciesHtml += ");";
				sb.append(strAgenciesHtml);
				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"
						+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = arrGroup1;"
						+ "ls.group2 = arrGroup2;" + "ls.height = '200px';" + "ls.width = '350px';"
						+ "ls.headingLeft = '&nbsp;&nbsp;All Agents';" + "ls.headingRight = '&nbsp;&nbsp;Selected Agents';"
						+ "ls.drawListBox();";
			}
		}
		sb.append(strAssignGSAHtml);
		return sb.toString();
	}

	/**
	 * Creates Booking Code Multiselect Box
	 * 
	 * @return String the Booking codes Multi Select
	 * @throws ModuleException
	 *             the ModuleException
	 */

	public final static String createBookingCodeHtml() throws ModuleException {
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var bcs = new Array();");

		List<Map<String, String>> lstBc = SelectListGenerator.createBookingClasses();
		Iterator<Map<String, String>> iteBc = lstBc.iterator();
		int temp = 0;
		if (lstBc != null && !lstBc.isEmpty()) {
			while (iteBc.hasNext()) {
				Map<String, String> keyValues = iteBc.next();
				String code = keyValues.get("BOOKING_CODE");
				jsb.append("bcs[" + temp + "] = new Array('" + code + "','" + code + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = bcs; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var bcls = new Listbox('lstBcs', 'lstAssignedBcs', 'spnBC','bcls');");
		jsb.append("bcls.group1 = arrData[0];" + "bcls.list2 = arrData2;");
		jsb.append("bcls.headingLeft = 'Available';");
		jsb.append("bcls.headingRight = 'Assigned';");

		return jsb.toString();
	}

	public final static String createPaxStatusHtml() throws ModuleException {
		StringBuffer jsb = new StringBuffer("var arrGroupPs = new Array();");
		jsb.append(" arrGroupPs[0] = '' ; var arrDataPs = new Array();");
		jsb.append(" var arrPs = new Array();");

		Collection<String[]> paxStatus = SelectListGenerator.getPaxStatusList();
		int temp = 0;
		if (paxStatus != null && !paxStatus.isEmpty()) {
			for (String[] ps : paxStatus) {
				jsb.append("arrPs[" + temp++ + "] = new Array('" + ps[1] + "','" + ps[0] + "');");
			}
		}
		jsb.append(" arrDataPs[0] = arrPs ; arrDataPs[1] = new Array();");
		jsb.append(" var arrGroupPs2 = new Array();");
		jsb.append(" var arrDataPs2 = new Array();");
		jsb.append(" arrDataPs2[0] = new Array();");
		jsb.append(" var listBoxPs = new Listbox('lstPs', 'lstAssignedPs', 'spnPS','listBoxPs');");
		jsb.append(" listBoxPs.group1 = arrDataPs[0];" + "listBoxPs.list2 = arrDataPs2;");
		jsb.append(" listBoxPs.headingLeft = 'Available';");
		jsb.append(" listBoxPs.headingRight = 'Assigned';");

		return jsb.toString();
	}

	/**
	 * Method convert date to dd-MMM-YYYY format
	 * 
	 * @param dateString
	 * @return String
	 * @throws ModuleException
	 */
	public static String convertDate(String dateString) throws ModuleException {
		return convertDate(dateString, false, null, false);
	}

	/**
	 * Converts the date to dd-MMM-YYYY format if convertToZule parameter is false, otherwise converts the date to
	 * dd-MMM-yyyy HH:mm:ss format.
	 * 
	 * The GMT difference is takes from the airport agent is assigned in.
	 * 
	 * When converting to Zulu(GMT) time fromDate will be takes at 00:00:00 time and toDates will be taken as 23:59:59.
	 * This will be determined by the toDate boolean parameter.
	 * 
	 * @param dateString
	 *            The date string in dd/MM/YYYY format
	 * @param convertToZulu
	 *            Whether this date should be converted to Zulu(GMT) time or not.
	 * @param userID
	 *            currentUser's ID(user name)
	 * @param toDate
	 *            Whether the date is going to be used as an end of a date range (to date of a from date and to date
	 *            pair).
	 * @return the formatted date according to the given parameters
	 * @throws ModuleException
	 */
	public static String convertDate(String dateString, boolean convertToZulu, String userID, boolean toDate)
			throws ModuleException {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		if (convertToZulu && userID != null) {
			if (toDate) {
				cal = new GregorianCalendar(year, (month - 1), day, 23, 59, 59);
			}
			int time_difference = ModuleServiceLocator.getSecurityBD().getUserGMTTimeDifference(userID);
			cal.add(Calendar.MINUTE, time_difference);
			dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		}
		date = dateFormat.format(cal.getTime());

		return date;
	}

	/**
	 * Gets the reports file path.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getReportTemplate(String fileName) {
		String reportsRootDir = null;

		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;

		return reportsRootDir;
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public final static String getClientErrors(HttpServletRequest request) throws ModuleException {

		String usrLang = (String)request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("um.report.todate.futureDate", "toDtExceedsToday");
		moduleErrs.setProperty("um.report.fromdate.futureDate", "fromDtExceedsToday");
		moduleErrs.setProperty("um.report.fromdate.exeeedsToDate", "fromDtExceed");
		moduleErrs.setProperty("um.report.fromdate.empty", "fromDtEmpty");
		moduleErrs.setProperty("um.report.fromdate.invalid", "fromDtInvalid");
		moduleErrs.setProperty("um.report.bookedFromDate.invalid", "bookedFromDtInvalid");
		moduleErrs.setProperty("um.report.bookedToDate.invalid", "bookedToDtInvalid");
		moduleErrs.setProperty("um.report.fromBookedFromoDate.invalid", "deptrFromDtInvalid");
		moduleErrs.setProperty("um.report.fromBookedToDate.invalid", "deptrToDtInvalid");
		moduleErrs.setProperty("um.report.todate.empty", "toDtEmpty");
		moduleErrs.setProperty("um.report.todate.invalid", "toDtinvalid");
		moduleErrs.setProperty("um.report.station.empty", "stationEmpty");
		moduleErrs.setProperty("um.report.printDetails.invalid", "printDTinvalid");
		moduleErrs.setProperty("um.report.search.startdate.exceeds", "rptReriodExceeds");

		moduleErrs.setProperty("um.report.segmentFrom.empty", "segmentFromEmpty");
		moduleErrs.setProperty("um.report.segmentTo.empty", "segmentToEmpty");
		moduleErrs.setProperty("um.report.flightNo.empty", "flightNoEmpty");
		moduleErrs.setProperty("um.report.flightOrSegment.required", "flightOrSegmentEmpty");
		moduleErrs.setProperty("um.report.nationality.required", "nationalityEmpty");
		moduleErrs.setProperty("um.report.validation.success", "validationSuccess");
		moduleErrs.setProperty("um.report.country.required", "countryEmpty");
		moduleErrs.setProperty("um.report.firstName.required", "fNameEmpty");
		moduleErrs.setProperty("um.report.lastName.required", "lNameEmpty");
		moduleErrs.setProperty("um.report.sectorFrom.required", "sectorFromEmpty");
		moduleErrs.setProperty("um.report.sectorTo.required", "sectorToEmpty");
		moduleErrs.setProperty("um.report.year.required", "yearEmpty");
		moduleErrs.setProperty("um.report.month.required", "monthEmpty");
		moduleErrs.setProperty("um.report.gsa.required", "gsaEmpty");
		moduleErrs.setProperty("um.report.fromDate.past", "fromDtPast");
		moduleErrs.setProperty("um.report.toDate.past", "toDtPast");
		moduleErrs.setProperty("um.report.noOfAgents.required", "noOfAgentsRqrd");
		moduleErrs.setProperty("um.report.stationTo.required", "stationToRqrd");
		moduleErrs.setProperty("um.report.stationFrom.required", "stationFromRqrd");
		moduleErrs.setProperty("um.report.agentType.required", "agentTypeRqrd");
		moduleErrs.setProperty("um.report.either.sales.refunds.required", "salesOrRefundsNotSelected");
		moduleErrs.setProperty("um.report.agents.required", "agentsRqrd");
		moduleErrs.setProperty("um.report.agentsStations.required", "agentsOrStationsRqrd");
		moduleErrs.setProperty("um.report.paymentModes.required", "paymentmodesRqrd");
		moduleErrs.setProperty("um.report.segmentFrom.and.flight.empty", "segmentandflight");
		moduleErrs.setProperty("um.report.segmentto.and.flight.empty", "segmenttoandflight");
		moduleErrs.setProperty("um.report.agency.null", "agencyNull");
		moduleErrs.setProperty("um.report.select.type", "reportRqrd");
		moduleErrs.setProperty("um.report.common.invaliddate", "invaliddate");
		moduleErrs.setProperty("um.report.reservation.criterianull", "criterianull");
		moduleErrs.setProperty("um.report.agency.list.null", "agencyListNull");
		moduleErrs.setProperty("um.report.airports.required", "airportRqrd");
		moduleErrs.setProperty("um.report.month.greater", "monthGreater");
		moduleErrs.setProperty("um.report.segment.same", "sameSegment");
		moduleErrs.setProperty("um.report.via.equal", "sameVia");
		moduleErrs.setProperty("um.report.via.and.departure", "sameViaDept");
		moduleErrs.setProperty("um.report.via.and.arrival", "sameViaArival");
		moduleErrs.setProperty("um.report.selected.agency.null", "selectedAgency");
		moduleErrs.setProperty("um.report.selected.agent.users", "selectedUsers");
		moduleErrs.setProperty("um.report.agenceyWeb.required", "agenceyWebRqrd");
		moduleErrs.setProperty("um.report.reportTypeValue.required", "rptTypeRqrd");
		moduleErrs.setProperty("um.report.period.sevenexceed", "sevenexceed");
		moduleErrs.setProperty("um.report.carrierCode.required", "carrierCodeRqrd");

		moduleErrs.setProperty("um.report.ond.exist", "OnDExists");
		moduleErrs.setProperty("um.report.ond.departure.required", "depatureRqrd");
		moduleErrs.setProperty("um.report.ond.arrival.required", "arrivalRqrd");
		moduleErrs.setProperty("um.report.ond.via.not.in.sequence", "vianotinSequence");
		moduleErrs.setProperty("um.report.ssr.code.required", "selSSRCodeRqrd");
		moduleErrs.setProperty("um.report.service.cat.required", "selServCateRqrd");

		moduleErrs.setProperty("um.report.flightdate.empty", "flightDtEmpty");
		moduleErrs.setProperty("um.report.flightdate.invalid", "flightDtInvalid");
		moduleErrs.setProperty("um.report.flightno.required", "flightNoRqrd");
		moduleErrs.setProperty("um.report.flightno.invalid", "flightNoInvalid");
		moduleErrs.setProperty("um.agent.form.acccode.empty", "accCodeEmpty");
		moduleErrs.setProperty("um.airadmin.accountCode.invalid", "accCodeInvalid");
		moduleErrs.setProperty("um.report.charge.adjustment.charge.codes.cannot.be.null", "adjustmentChargeCodesRqrd");

		// Fare details report error messages
		moduleErrs.setProperty("um.report.sal.effec.frm.date.empty", "slEffecFrmDtEmp");
		moduleErrs.setProperty("um.report.sal.effec.to.date.empty", "slEffecToDtEmp");
		moduleErrs.setProperty("um.report.sal.effec.date.range.from.grt.to", "slEffecFrmDtGrt");
		moduleErrs.setProperty("um.report.dep.val.frm.date.empty", "depFrmDtEmp");
		moduleErrs.setProperty("um.report.dep.val.to.date.empty", "depToDtEmp");
		moduleErrs.setProperty("um.report.dep.val.date.range.from.grt.to", "depFrmDtGrt");
		moduleErrs.setProperty("um.report.sal.effec.frm.date.invalid", "slEffecFrmDtInvld");
		moduleErrs.setProperty("um.report.sal.effec.to.date.invalid", "slEffecToDtInvld");
		moduleErrs.setProperty("um.report.dep.val.frm.date.invalid", "depFrmDtInvld");
		moduleErrs.setProperty("um.report.dep.val.to.date.invalid", "depToDtInvld");
		moduleErrs.setProperty("um.report.ond.empty", "ondEmpty");
		moduleErrs.setProperty("um.report.invalid.liveReport", "invalidLiveReport");

		moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureRequired.required", "depatureRequired");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.arrivalRequired.required", "arrivalRequired");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureArriavlSame.Same", "depatureArriavlSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.arriavlViaSame.Same", "arriavlViaSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureViaSame.Same", "depatureViaSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.vianotinSequence.notin", "vianotinSequence");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.via.same", "viaSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.OnD.exist", "OnDExists");
		moduleErrs.setProperty("um.report.book.status.transition.req", "bookStatusReq");
		moduleErrs.setProperty("um.report.release.hours.invalid", "invalidReleaseHours");
		moduleErrs.setProperty("um.report.user.incentive.segment.quantifier.required", "segQuantifierReq");
		moduleErrs.setProperty("um.report.user.incentive.segment.count.required", "segCountReq");
		moduleErrs.setProperty("um.report.user.incentive.segment.valid.count.required", "segValidCountReq");
		moduleErrs.setProperty("um.report.user.only.one.agent.allowed", "oneAgentCode");
		moduleErrs.setProperty("um.report.agentType.required", "agentTypeRqrd");

		// These messages are used in segment Contribution report
		moduleErrs.setProperty("um.report.depature.required", "depatureRqrd");
		moduleErrs.setProperty("um.report.arrival.required", "arrivalRqrd");
		moduleErrs.setProperty("um.report.via.required", "viaPointRqrd");
		moduleErrs.setProperty("um.report.arrivalDepature.same", "depatureArriavlSame");
		moduleErrs.setProperty("um.report.arrivalVia.same", "arriavlViaSame");
		moduleErrs.setProperty("um.report.depatureVia.same", "depatureViaSame");
		moduleErrs.setProperty("um.report.via.sequence", "vianotinSequence");
		moduleErrs.setProperty("um.report.departure.inactive", "departureInactive");
		moduleErrs.setProperty("um.report.arrival.inactive", "arrivalInactive");
		moduleErrs.setProperty("um.report.via.inactine", "viaInactive");
		moduleErrs.setProperty("um.report.ond.required", "ondRequired");
		moduleErrs.setProperty("um.report.seg.required", "segmentsNotSelected");
		moduleErrs.setProperty("um.report.seg.flightnum.notallowed", "segmetntAndFlightNumCannotSelected");
		moduleErrs.setProperty("um.report.atleast.depdate.and.salesdate.empty", "atleastOneDateSetRequired");
		moduleErrs.setProperty("um.report.search.validity.invalid", "searchValidityInvalid");
		moduleErrs.setProperty("um.report.date.format.invalid", "fltDatePerInvld");
		
		//	return JavaScriptGenerator.createClientErrors(moduleErrs);
		 return JavaScriptGenerator.createClientErrorsTempMethod(moduleErrs,usrLang);
	}
	
	

	public static void setPreferedReportFormat(String prefRptFormat, Map<String, Object> parameters) {
		parameters.put("LOCALE", "US");
		parameters.put("DELIMITER", ",");
		if (prefRptFormat != null && !prefRptFormat.equals("")) {
			if (prefRptFormat.equalsIgnoreCase("FR") || prefRptFormat.equalsIgnoreCase("IT")) {
				parameters.put("LOCALE", "IT");
				parameters.put("DELIMITER", ";");
			}
		}
	}

	public final static void createPreferedReportOptions(HttpServletRequest request) throws ModuleException {
		
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());

		String prefReportFormat = agent.getPrefferedRptFormat();
		request.setAttribute("AgentRptFormat", prefReportFormat);
		StringBuilder sb = new StringBuilder();
		sb.append("<table><tr>");
		sb.append("<td width='110' align='left'>");
		sb.append("<input type='radio' name='radRptNumFormat' id='radUSFormat' value='US'  class='noBorder'");
		if (prefReportFormat.equals("US")) {
			sb.append("checked");
		}
		sb.append("><font>US Format</font>");
		sb.append("</td>");
		sb.append("<td width='110' align='left'>");
		sb.append("<input type='radio' name='radRptNumFormat' id='radEUROFormat' value='IT'  class='noBorder'");
		if (!prefReportFormat.equals("US")) {
			sb.append("checked");
		}

		// sb.append("value='"+prefReportFormat+"' checked");

		sb.append("><font>EURO Format</font>");
		sb.append("</td>");
		sb.append("</tr></table>");

		request.setAttribute(WebConstants.REPORT_FORMAT_OPTIONS, sb.toString());
	}
	
	/**
	 * Method is overloaded for translations. Extra parameter ResourceBundle is added.
	 *
	 * @param request
	 *  
	 * @param rb
	 *            Resource bundle which contains key-value pairs for session locale.
	 * 
	 * @throws ModuleException
	 */
	public final static void createPreferedReportOptions(HttpServletRequest request, ResourceBundle rb) throws ModuleException {
		
		String usFormat = null;
		String euroFormat = null;
		try{
			usFormat = rb.getString("US_Format");
			euroFormat = rb.getString("EURO_Format");
		}catch(MissingResourceException e){
			usFormat = "US Format";
			euroFormat = "EURO Format";
		}
		
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());

		String prefReportFormat = agent.getPrefferedRptFormat();
		request.setAttribute("AgentRptFormat", prefReportFormat);
		StringBuilder sb = new StringBuilder();
		sb.append("<table><tr>");
		sb.append("<td width='110' align='left'>");
		sb.append("<input type='radio' name='radRptNumFormat' id='radUSFormat' value='US'  class='noBorder'");
		if (prefReportFormat.equals("US")) {
			sb.append("checked");
		}
		sb.append("><font>"+usFormat+"</font>");
		sb.append("</td>");
		sb.append("<td width='110' align='left'>");
		sb.append("<input type='radio' name='radRptNumFormat' id='radEUROFormat' value='IT'  class='noBorder'");
		if (!prefReportFormat.equals("US")) {
			sb.append("checked");
		}

		// sb.append("value='"+prefReportFormat+"' checked");

		sb.append("><font>"+euroFormat+"</font>");
		sb.append("</td>");
		sb.append("</tr></table>");

		request.setAttribute(WebConstants.REPORT_FORMAT_OPTIONS, sb.toString());
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static String createChargeAdjustmentTypesHtml() throws ModuleException {
		Collection payments = null;
		StringBuffer sb = new StringBuffer();

		String strChargeCodesHtml = "var arrData = new Array();" + "arrData[0] = new Array(";

		payments = ModuleServiceLocator.getDataExtractionBD().getPaymentsMode();

		List<ChargeAdjustmentTypeDTO> adjustmentTypes = new ArrayList<ChargeAdjustmentTypeDTO>();
		adjustmentTypes.addAll(ModuleServiceLocator.getGlobalConfig().getChargeAdjustmentTypes());
		adjustmentTypes.remove(ModuleServiceLocator.getGlobalConfig().getDefaultChargeAdjustmentType());

		Map<QuotedChargeDTO, Boolean> chargesMap = new HashMap<QuotedChargeDTO, Boolean>();

		for (ChargeAdjustmentTypeDTO adjsutmentType : adjustmentTypes) {

			if (StringUtils.isNotEmpty(adjsutmentType.getRefundableChargeCode())) {
				QuotedChargeDTO quotedChargeDTO = ModuleServiceLocator.getChargeBD().getCharge(
						adjsutmentType.getRefundableChargeCode(), null, null, null, false);
				if (quotedChargeDTO != null) {
					chargesMap.put(quotedChargeDTO, true);
				}
			}

			if (StringUtils.isNotEmpty(adjsutmentType.getNonRefundableChargeCode())) {
				QuotedChargeDTO quotedChargeDTO = ModuleServiceLocator.getChargeBD().getCharge(
						adjsutmentType.getNonRefundableChargeCode(), null, null, null, false);
				if (quotedChargeDTO != null) {
					chargesMap.put(quotedChargeDTO, false);
				}
			}
		}

		for (QuotedChargeDTO charge : chargesMap.keySet()) {
			String chargeDesc = charge.getChargeDescription();

			if (chargesMap.get(charge)) {
				chargeDesc = chargeDesc + " (Refundable)";
			} else {
				chargeDesc = chargeDesc + " (Non Refundable)";
			}
			strChargeCodesHtml += "new Array('" + chargeDesc + "','" + charge.getChargeCode() + "'), ";
		}

		strChargeCodesHtml = strChargeCodesHtml.substring(0, strChargeCodesHtml.length() - 2);
		sb.append(strChargeCodesHtml);

		String strChargeCodeslistHtml = "); var lscc = new Listbox('lstCCodes', "
				+ "'lstAssignedCCodes', 'spnChargeCodes', 'lscc');" + "lscc.group1 = arrData[0];" + "lscc.height = '100px';"
				+ "lscc.width = '350px';" + "lscc.headingLeft = '&nbsp;&nbsp;All Charge Codes';"
				+ "lscc.headingRight = '&nbsp;&nbsp;Selected Charge Codes';" + "lscc.drawListBox();";

		sb.append(strChargeCodeslistHtml);

		return sb.toString();
	}

	/**
	 * Creates Fare Rule Multi Select Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createFareRuleList() throws ModuleException {
		List<Map<String, String>> fareRules = null;
		StringBuffer sb = new StringBuffer("var arrFareRuleData = new Array();");
		sb.append("var fareRules = new Array();");
		String frule = null;
		fareRules = SelectListGenerator.createFareRuleCodeList();
		int tempInt = 0;
		Iterator<Map<String, String>> fareRuleIter = fareRules.iterator();
		while (fareRuleIter.hasNext()) {
			Map<String, String> keyValues = fareRuleIter.next();
			Set<String> keys = keyValues.keySet();

			Iterator<String> keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {
				frule = keyValues.get(keyIterator.next());
				sb.append("fareRules[" + tempInt + "] = new Array('" + frule + "','" + frule + "'); ");
				tempInt++;
			}
		}

		sb.append("arrFareRuleData[0] = fareRules;");
		sb.append(" var lsfrules = new Listbox('lstFReules', 'lstAssignedFRules', 'spnFareRules', 'lsfrules');");
		sb.append("lsfrules.group1 = arrFareRuleData[0];");
		sb.append("lsfrules.height = '100px'; lsfrules.width = '120px';");
		sb.append("lsfrules.headingLeft = '&nbsp;&nbsp;All Fare Rules';");
		sb.append("lsfrules.headingRight = '&nbsp;&nbsp;Selected Fare Rules';");
		sb.append("lsfrules.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates Fare Visibility Multi Select Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createVisibilityList() throws ModuleException {
		List<Map<String, String>> fvisiblityRules = null;
		StringBuffer sb = new StringBuffer("var arrFareVisiData = new Array();");
		sb.append("var fareVisibility = new Array();");

		String fvisiValue = null;
		fvisiblityRules = SelectListGenerator.createSalesChannelVisibilityMultiList();
		int tempInt = 0;

		Iterator<Map<String, String>> fareVisiIter = fvisiblityRules.iterator();

		while (fareVisiIter.hasNext()) {
			Map<String, String> keyValues = fareVisiIter.next();
			Set<String> keys = keyValues.keySet();

			Iterator<String> keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {
				fvisiValue = keyValues.get(keyIterator.next());
				sb.append("fareVisibility[" + tempInt + "] = new Array('" + fvisiValue + "','" + fvisiValue + "'); ");
				tempInt++;
			}
		}

		sb.append("arrFareVisiData[0] = fareVisibility;");
		sb.append(" var lsfvisibility = new Listbox('lstFVisibility', 'lstAssignedFVisibility', 'spnFareVisibility', 'lsfvisibility');");
		sb.append("lsfvisibility.group1 = arrFareVisiData[0];");
		sb.append("lsfvisibility.height = '100px'; lsfrules.width = '120px';");
		sb.append("lsfvisibility.headingLeft = '&nbsp;&nbsp;All Fare Visibility';");
		sb.append("lsfvisibility.headingRight = '&nbsp;&nbsp;Selected Fare Visibility';");
		sb.append("lsfvisibility.drawListBox();");
		return sb.toString();

	}
	/**
	 * Creates a Multiselect box According to Agent type
	 * 
	 * @param strAgentType
	 *            the Agent Type
	 * @return String the Created Multi select Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createUserMultiSelect(String AgentCode) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection<String[]> users = null;
		Collection<Agent> agents = null;
		String user[] = null;
		Agent gsa = null;

		if (AgentCode!= null && !AgentCode.equals("")) {
			agents = ModuleServiceLocator.getTravelAgentBD().getReportingAgentsForGSA(AgentCode);
			agents.add(ModuleServiceLocator.getTravelAgentBD().getAgent(AgentCode));
			
			if (!agents.isEmpty()) {

				sb.append("var arrAgncs = new Array();");
				sb.append("var agArray = new Array();");
				int tempCat = 0;
				int tempInt = 0;
				Iterator<Agent> gsaIter = agents.iterator();
				String agentCode[] = new String[1];
				Iterator<String[]> userIter = null;

				while (gsaIter.hasNext()) {
					gsa = gsaIter.next();
					sb.append("arrAgncs[" + tempCat + "] = ");
					sb.append(" new Array('" + gsa.getAgentDisplayName() + "','" + gsa.getAgentCode() + "'); ");

					agentCode[0] = gsa.getAgentCode();
					users = SelectListGenerator.createAgentUsers(agentCode);

					if (!users.isEmpty()) {
						userIter = users.iterator();
					}

					sb.append("agitem_" + tempCat + " = new Array();  ");

					if (users.size() == 0) {
						sb.append("new Array();");
					}
					tempInt = 0;
					while (userIter != null && userIter.hasNext()) {
						user = userIter.next();

						if (user != null && gsa.getAgentCode() != null) {
							sb.append("agitem_" + tempCat + "[" + tempInt + "] = ");
							sb.append(" new Array('" + user[0] + "','" + user[1] + "'); ");
							tempInt++;
						}
					}
					sb.append("agArray[" + tempCat + "] = agitem_" + tempCat + ";");
					tempCat++;

				}

				sb.append("var arrGroup2 = new Array();");
				sb.append("var arrData2 = new Array(); arrData2[0] = new Array();");
				sb.append("var ls = new Listbox('lstUsers', 'lstAssignedUsers', 'spn1');");
				sb.append("ls.group1 = arrAgncs;" + "ls.group2 = arrGroup2;");
				sb.append("ls.list1 = agArray;" + "ls.list2 = arrData2;");

			}
		} else {

			sb.append("var ls = new Listbox('lstUsers', 'lstAssignedUsers', 'spn1');");
		}
		sb.append("ls.height = '165px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agent Users';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agent Users';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}
	/**
	 * 
	 * @TODO : Should make this a common method.
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static String createMultiSelect() throws ModuleException {

		// String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		Collection<String> data = new ArrayList<String>();
		data.add("Fully paid bookings");
		data.add("Partially paid bookings");
		// data.add("Atleast one payment bookings");

		StringBuffer sb = new StringBuffer();
		if (data != null && data.size() > 0) {
			sb.append("var showCarrierCombo = true;");
			// sb.append("var defaultCarrier = '" + defaultCarrierCode + "';");
			sb.append("var arrData = new Array();");
			sb.append("var methods = new Array();");
			int tempInt = 0;
			String carrierCode;

			Iterator<String> carrierIter = data.iterator();
			
			int code = 1;
			while (carrierIter.hasNext()) {
				carrierCode = carrierIter.next();
				sb.append("methods[" + tempInt + "] = new Array('" + carrierCode + "','" + code + "'); ");
				tempInt++;
				code++;
			}

			sb.append("arrData[0] = methods;");
			sb.append("var lscc2 = new Listbox('lstBookingPayments', 'lstAssignedBookingPayments', 'spnPaymentTypes', 'lscc2');");
			sb.append("lscc2.group1 = arrData[0];");
			sb.append("lscc2.height = '75px'; lscc2.width = '350px';");
			sb.append("lscc2.headingLeft = '&nbsp;&nbsp;All payment modes';");
			sb.append("lscc2.headingRight = '&nbsp;&nbsp;Select reservations payment modes';");
			sb.append("lscc2.drawListBox();");
		} else {
			sb.append("var showCarrierCombo = false;");
		}

		return sb.toString();

	}
	/**
	 * Method String date to util date.
	 * 
	 * @param dateString
	 * @return String
	 */
	public static Timestamp toDateType(String dateString, boolean fromDate) {
		Timestamp dateTime = null;

		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		Calendar cal = new GregorianCalendar(year, (month - 1), day);

		if (fromDate) {
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
		} else {
			cal.set(Calendar.HOUR, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
		}

		dateTime = new Timestamp(cal.getTimeInMillis());

		return dateTime;
	}

	public final static String createStationsByCountry(ResourceBundle languageBundle) throws ModuleException {
		StringBuffer jsb = new StringBuffer();
		String strCountry[] = new String[1];
		strCountry[0] = "";
		Collection<String[]> stnCol = null;
		Iterator<String[]> stnite = null;
		String stncode = null;
		String stndesc = null;
		String strStnArr[] = new String[2];
		jsb.append(" var countries = new Array();");
		jsb.append(" var cntryData = new Array();");

		int i = 0;
		int tempint = 0;
		Collection<Map<String, String>> lstcntry = SelectListGenerator.createAllCountry(strCountry);
		Iterator<Map<String, String>> itecntry = lstcntry.iterator();
		if (lstcntry != null && !lstcntry.isEmpty()) {
			while (itecntry.hasNext()) {
				Map<String, String> keyValues = itecntry.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();

				if (keyIterator.hasNext()) {
					String code = keyValues.get(keyIterator.next());
					String desc = keyValues.get(keyIterator.next());
					strCountry[0] = code;
					jsb.append("countries[" + i + "] = '" + desc + "'; ");
					jsb.append("var cities_" + i + " = new Array();");
					tempint = 0;
					stnCol = SelectListGenerator.createStationCountry(strCountry);

					stnite = stnCol.iterator();
					while (stnite.hasNext()) {
						strStnArr = stnite.next();
						stncode = strStnArr[0];
						stndesc = strStnArr[1];
						jsb.append("cities_" + i + "[" + tempint + "] = new Array('" + stndesc + "','" + stncode + "');");
						tempint++;
					}
					jsb.append("cntryData[" + i + "] = cities_" + i + ";");
					i++;
				}
			}
		}
		String agentStations = null;
		String assignedAgentStation = null;
		try {
			agentStations = languageBundle.getString("Agent_Station");
			assignedAgentStation = languageBundle.getString("Assigned_Agent_Station");
		} catch (MissingResourceException e) {
			log.error("Translation properties are missing for the requested language");
			agentStations = "Agent Stations";
			assignedAgentStation = "Assigned Agent Station";
		}

		jsb.append("var cntryGroup1 = countries; var cntryGroup2 = new Array();");
		jsb.append("var cntryData2 = new Array();");
		jsb.append("cntryData2[0] = new Array();");
		jsb.append("var stls = new Listbox('lstSTC', 'lstAssignedSTC', 'spnSTC','stls');");
		jsb.append("stls.group1 = cntryGroup1; stls.group2 = cntryGroup2;");
		jsb.append("stls.list1 = cntryData; stls.list2 = cntryData2;");
		jsb.append("stls.height = '125px'; stls.width = '160px';");
		jsb.append("stls.headingLeft = '&nbsp;&nbsp;"+agentStations+"';");
		jsb.append("stls.headingRight = '&nbsp;&nbsp;"+assignedAgentStation+"';");
		jsb.append("stls.drawListBox();");
		return jsb.toString();
	}
	
	// AEROMART-2923
	public final static String createStationsByCountryForComapanyPamentRptGSA(UserPrincipal user, ResourceBundle languageBundle)
			throws ModuleException {

		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
		String strAgentCode = agent.getAgentCode();
		StringBuffer jsb = new StringBuffer();
		String strCountry[] = new String[1];
		strCountry[0] = "";
		Collection<String[]> stnCol = null;
		Iterator<String[]> stnite = null;
		String stncode = null;
		String stndesc = null;
		String strStnArr[] = new String[2];
		jsb.append(" var countries = new Array();");
		jsb.append(" var cntryData = new Array();");

		int i = 0;
		int tempint = 0;
		Collection<Map<String, String>> lstcntry = SelectListGenerator.createAllCountry(strCountry);
		Iterator<Map<String, String>> itecntry = lstcntry.iterator();
		if (lstcntry != null && !lstcntry.isEmpty()) {
			while (itecntry.hasNext()) {
				Map<String, String> keyValues = itecntry.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();

				if (keyIterator.hasNext()) {
					String[] params = new String[3];

					String code = keyValues.get(keyIterator.next());
					String desc = keyValues.get(keyIterator.next());

					params[0] = strAgentCode;
					params[1] = strAgentCode;
					params[2] = code;
					strCountry[0] = code;
					
					tempint = 0;
					stnCol = SelectListGenerator.createStationCountryForGSA(params);
					
					if(!stnCol.isEmpty()){
						jsb.append("countries[" + i + "] = '" + desc + "'; ");
						jsb.append("var cities_" + i + " = new Array();");
					}
					stnite = stnCol.iterator();
					while (stnite.hasNext()) {
						strStnArr = stnite.next();
						stncode = strStnArr[0];
						stndesc = strStnArr[1];
						jsb.append(
								"cities_" + i + "[" + tempint + "] = new Array('" + stndesc + "','" + stncode + "');");
						tempint++;
						
					}
					if(!stnCol.isEmpty()){
						jsb.append("cntryData[" + i + "] = cities_" + i + ";");
						i++;
					}
					
				}
			}
		}
		String agentStations = null;
		String assignedAgentStation = null;
		try {
			agentStations = languageBundle.getString("Agent_Station");
			assignedAgentStation = languageBundle.getString("Assigned_Agent_Station");
		} catch (MissingResourceException e) {
			log.error("Translation properties are missing for the requested language");
			agentStations = "Agent Stations";
			assignedAgentStation = "Assigned Agent Station";
		}

		jsb.append("var cntryGroup1 = countries; var cntryGroup2 = new Array();");
		jsb.append("var cntryData2 = new Array();");
		jsb.append("cntryData2[0] = new Array();");
		jsb.append("var stls = new Listbox('lstSTC', 'lstAssignedSTC', 'spnSTC','stls');");
		jsb.append("stls.group1 = cntryGroup1; stls.group2 = cntryGroup2;");
		jsb.append("stls.list1 = cntryData; stls.list2 = cntryData2;");
		jsb.append("stls.height = '125px'; stls.width = '160px';");
		jsb.append("stls.headingLeft = '&nbsp;&nbsp;"+agentStations+"';");
		jsb.append("stls.headingRight = '&nbsp;&nbsp;"+assignedAgentStation+"';");
		jsb.append("stls.drawListBox();");
		return jsb.toString();
	}
}
