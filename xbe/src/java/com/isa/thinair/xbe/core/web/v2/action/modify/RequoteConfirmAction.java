package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airreservation.api.dto.ssr.MedicalSsrDTO;
import com.isa.thinair.webplatform.api.util.ReservationPaxUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.CodeshareUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ServiceTaxCCParamsDTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.ConfirmUpdateUtil;
import com.isa.thinair.xbe.core.web.v2.util.ParameterUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class RequoteConfirmAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(RequoteConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private String balanceQueryData;

	private String selCurrency;

	private boolean isNoPay = false;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private String resContactInfo;

	private CurrencyExchangeRate countryCurrExchangeRate;

	private boolean convertedToGroupReservation;

	private String groupBookingReference;

	private String paxNameDetails;

	private boolean isCsPnr;

	private String flightRPHList;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		try {

			// XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
			// S2Constants.Session_Data.XBE_SES_RESDATA);
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			// String strTxnIdntifier = resInfo.getTransactionId();
			boolean isAllowOverbookAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME);
			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, AppIndicatorEnum.APP_XBE);
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(isAllowOverbookAfterCutOffTime);

			RequoteModifyRQ requoteModifyRQ = JSONFETOParser.parse(RequoteModifyRQ.class, balanceQueryData);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			CommonReservationContactInfo contactInfo = ReservationUtil.transformJsonContactInfo(resContactInfo);
			boolean isInfantPaymentSeparated = resInfo.isInfantPaymentSeparated();
			boolean adminFeeEnabled = AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, userPrincipal.getAgentCode());
			if (adminFeeEnabled) {
				bookingShoppingCart.removeServiceTaxAppliedToCCFee();
			}
			if (bookingShoppingCart.getPriceInfoTO() != null) {
				QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(
						bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO(), flightPriceRQ,
						bookingShoppingCart.getFareDiscount());
				requoteModifyRQ.setFareInfo(fareInfo);
				requoteModifyRQ.setLastFareQuoteDate(bookingShoppingCart.getPriceInfoTO().getLastFareQuotedDate());
				requoteModifyRQ.setFQWithinValidity(bookingShoppingCart.getPriceInfoTO().isFQWithinValidity());
				fareInfo.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
			}
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = ReservationUtil.getPassengerExtChgMap(bookingShoppingCart);
			requoteModifyRQ.setPaxExtChgMap(paxExtChgMap);
			requoteModifyRQ.setExcludedSegFarePnrSegIds(fareQuoteParams.getExcludedSegFarePnrSegIds());

			requoteModifyRQ.setContactInfo(contactInfo);
			requoteModifyRQ.setRequoteSegmentMap(ReservationBeanUtil.getReQuoteResSegmentMap(fareQuoteParams.getOndList()));
			requoteModifyRQ.setReservationStatus(resInfo.getReservationStatus());

			Map<Integer, NameDTO> paxNamesMap = null;
			if (paxNameDetails != null) {
				boolean skipDuplicateNameCheck = BasicRH.hasPrivilege(request,
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP);
				paxNamesMap = ReservationBeanUtil.getNameChangedPaxMap(paxNameDetails);
				boolean dupNameCheck = AppSysParamsUtil.isDuplicateNameCheckEnabled() && !skipDuplicateNameCheck;
				requoteModifyRQ.setNameChangedPaxMap(paxNamesMap);
				requoteModifyRQ.setDuplicateNameCheck(dupNameCheck);
			}

			Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();

			requoteModifyRQ.setOndFareTypeByFareIdMap(
					ReservationBeanUtil.getONDFareTypeByFareIdMap(fareQuoteParams.getOndList(), oldFareIdByFltSegIdMap));

			requoteModifyRQ.setOldFareIdByFltSegIdMap(oldFareIdByFltSegIdMap);

			ReservationBalanceTO reservationBalanceTO;

			if (flightPriceRQ.getAvailPreferences().getSearchSystem().equals(SYSTEM.INT)) {
				requoteModifyRQ.setSystem(SYSTEM.INT);
				if (requoteModifyRQ.getGroupPnr() == null || requoteModifyRQ.getGroupPnr().equals("")) {
					requoteModifyRQ.setGroupPnr(requoteModifyRQ.getPnr());
				}
				groupBookingReference = requoteModifyRQ.getGroupPnr();
				convertedToGroupReservation = true;
				requoteModifyRQ
						.setCarrierWiseFlightRPHMap(ReservationCommonUtil.getCarrierWiseRPHs(fareQuoteParams.getOndList()));
				requoteModifyRQ.setVersion(ReservationCommonUtil.getCorrectInterlineVersion(requoteModifyRQ.getVersion()));
				requoteModifyRQ.setTransactionIdentifier(resInfo.getTransactionId());
			}
			reservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD().getRequoteBalanceSummary(requoteModifyRQ,
					getTrackInfo());

			int payablePaxCount = ReservationPaxUtil.getPayablePaxCount(reservationBalanceTO.getPassengerSummaryList())
					+ ReservationPaxUtil.getPayableInfantCount(reservationBalanceTO.getPassengerSummaryList(),
							isInfantPaymentSeparated);
			bookingShoppingCart.setPayablePaxCount(payablePaxCount);
			// Apply cc charge + jn when adminfee is enabled

			if (adminFeeEnabled) {

				BigDecimal totalPaymentAmountExcludingCCCharge = reservationBalanceTO.getTotalAmountDue();

				ExternalChgDTO externalCcChgDTO = bookingShoppingCart.getAllExternalChargesMap()
						.get(EXTERNAL_CHARGES.CREDIT_CARD);
				externalCcChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
				bookingShoppingCart.addSelectedExternalCharge(externalCcChgDTO);

				// Calculate CC & Handling charge JN tax
				BigDecimal jnTaxForCC = BigDecimal.ZERO;
				jnTaxForCC = com.isa.thinair.xbe.core.web.v2.util.ReservationUtil.getApplicableServiceTax(bookingShoppingCart,
						EXTERNAL_CHARGES.JN_OTHER);

				BigDecimal adminCCChargeAmountWithJN = AccelAeroCalculator.add(jnTaxForCC, externalCcChgDTO.getAmount());

				// apply service tax
				BigDecimal serviceTaxAmountForCC = BigDecimal.ZERO;
				ServiceTaxCCParamsDTO serviceTaxCCParamsDTO = new ServiceTaxCCParamsDTO(fareQuoteParams, contactInfo.getState(),
						contactInfo.getCountryCode(), isPaxTaxRegistered(contactInfo.getTaxRegNo()), flightRPHList,
						requoteModifyRQ.getGroupPnr());
				serviceTaxAmountForCC = ReservationUtil.calculateServiceTaxForTransactionFee(bookingShoppingCart,
						externalCcChgDTO.getAmount(), resInfo.getTransactionId(), serviceTaxCCParamsDTO, getTrackInfo(),
						EXTERNAL_CHARGES.CREDIT_CARD, false);
				// add service tax also
				adminCCChargeAmountWithJN = AccelAeroCalculator.add(adminCCChargeAmountWithJN, serviceTaxAmountForCC);

				reservationBalanceTO.setTotalAmountDue(
						AccelAeroCalculator.add(totalPaymentAmountExcludingCCCharge, adminCCChargeAmountWithJN));
				BigDecimal currentTotalPrice = reservationBalanceTO.getTotalPrice();
				reservationBalanceTO.setTotalPrice(AccelAeroCalculator.add(currentTotalPrice, adminCCChargeAmountWithJN));

				reservationBalanceTO.getSegmentSummary().setNewSurchargeAmount(AccelAeroCalculator
						.add(reservationBalanceTO.getSegmentSummary().getNewSurchargeAmount(), adminCCChargeAmountWithJN));

				if (payablePaxCount > 0) {
					BigDecimal[] bdEffectiveChgAmount = AccelAeroCalculator.roundAndSplit(adminCCChargeAmountWithJN,
							payablePaxCount);

					int iterations = 0;
					for (LCCClientPassengerSummaryTO pax : reservationBalanceTO.getPassengerSummaryList()) {

						if ((isInfantPaymentSeparated && PaxTypeTO.INFANT.equals(pax.getPaxType())
								&& pax.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0)
								|| (!PaxTypeTO.INFANT.equals(pax.getPaxType())
										&& pax.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0)) {
							pax.setTotalAmountDue(
									AccelAeroCalculator.add(pax.getTotalAmountDue(), bdEffectiveChgAmount[iterations]));
							pax.setTotalPrice(AccelAeroCalculator.add(pax.getTotalPrice(), bdEffectiveChgAmount[iterations]));
							iterations++;
						}
					}
				}
			}

			Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap = reservationBalanceTO.getPaxWiseAdjustmentAmountMap();

			isCsPnr = CodeshareUtil.isCodeshareMCPnr(flightPriceRQ);

			if (reservationBalanceTO.hasBalanceToPay()) {
				bookingShoppingCart.setReservationBalance(reservationBalanceTO);
				bookingShoppingCart.setPaidAmount(reservationBalanceTO.getTotalPaidAmount());
				// Make sure this is executed first to identify Anci charges
				if (paxNamesMap != null) {
					passengerPayment = ConfirmUpdateUtil.getPaxPaymentSummaryList(reservationBalanceTO, paxNamesMap.values(),
							resInfo.isInfantPaymentSeparated());
				} else {
					passengerPayment = ConfirmUpdateUtil.getPaxPaymentSummaryList(reservationBalanceTO, null,
							resInfo.isInfantPaymentSeparated());
				}

				// Add the credit used here so that payment sum only have the balance to pay
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				paymentSummaryTO = PaymentUtil.createInterlinePaymentSummary(bookingShoppingCart, selCurrency, exchangeRateProxy);

				this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);
				if (this.countryCurrExchangeRate != null && this.paymentSummaryTO != null) {
					this.paymentSummaryTO.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
				}

				request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShoppingCart);
			} else {

				requoteModifyRQ.setPaxWiseAdjustmentAmountMap(paxWiseAdjustmentAmountMap);
				requoteModifyRQ.setOwnerChannelId(resInfo.getOwnerChannelId());
				requoteModifyRQ.setNoBalanceToPay(true);

				if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
						|| resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
					@SuppressWarnings("unchecked")
					Map<String, String> mapPrivileges = (Map<String, String>) request.getSession()
							.getAttribute(WebConstants.SES_PRIVILEGE_IDS);
					@SuppressWarnings("unchecked")
					List<FlightSegmentTO> openFltSegments = JSONFETOParser.parseCollection(List.class, FlightSegmentTO.class,
							request.getParameter("openSegments"));
					Date releaseTimestamp = BookingUtil.getReleaseTimestamp(openFltSegments, mapPrivileges,
							userPrincipal.getAgentCode(), bookingShoppingCart.getPriceInfoTO().getFareTypeTO(), null, null);
					requoteModifyRQ.setReleaseTimeStampZulu(releaseTimestamp);
				}

				requoteModifyRQ
						.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);
				addExtChargesWithDummyPayments(requoteModifyRQ, bookingShoppingCart);
				requoteModifyRQ.setExternalChargesMap(ReservationUtil.getExternalChargesMap(bookingShoppingCart));
				BookingUtil.populateInsurance(bookingShoppingCart.getInsuranceQuotes(), requoteModifyRQ,
						fareQuoteParams.getAdultCount() + fareQuoteParams.getChildCount(), getTrackInfo());
				ModuleServiceLocator.getAirproxyReservationBD().requoteModifySegmentsWithAutoRefund(requoteModifyRQ,
						getTrackInfo());
				this.isNoPay = true;

				// send medical ssr email
				boolean hasBalanceToPay = false;
				MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(requoteModifyRQ.getPnr(),
						ReservationPaxUtil.toLccClientReservationPax(bookingShoppingCart.getPaxList()),
						requoteModifyRQ.getPaxExtChgMap(), contactInfo, hasBalanceToPay);
				medicalSsrDTO.sendEmail();
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private boolean isPaxTaxRegistered(String taxRegNo) {
		boolean isPaxTaxRegistered = false;
		if (taxRegNo != null && !taxRegNo.equals("")) {
			isPaxTaxRegistered = true;
		}
		return isPaxTaxRegistered;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setSelCurrency(String selCurrency) {
		this.selCurrency = selCurrency;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public boolean isNoPay() {
		return isNoPay;
	}

	private void addExtChargesWithDummyPayments(RequoteModifyRQ requoteModifyRQ, BookingShoppingCart bookingShoppingCart) {
		Map<String, List<LCCClientExternalChgDTO>> paxExhChgMap = ReservationUtil
				.getPaxRefNumberWiseExtChgMap(bookingShoppingCart);
		if (paxExhChgMap != null && paxExhChgMap.size() > 0) {
			for (Entry<String, List<LCCClientExternalChgDTO>> extChgEntry : paxExhChgMap.entrySet()) {
				LCCClientPaymentAssembler payAsm = new LCCClientPaymentAssembler();
				List<LCCClientExternalChgDTO> paxExternalCharge = extChgEntry.getValue();
				// mark the external chargers already consume as we do not need any external payment for this case
				for (LCCClientExternalChgDTO chgDTO : paxExternalCharge) {
					chgDTO.setAmountConsumedForPayment(true);
				}
				payAsm.getPerPaxExternalCharges().addAll(paxExternalCharge);

				// TODO:RW set correct pax type
				// if (PaxTypeTO.ADULT.equals(pax.getPaxType()) && (pax.getInfantName() != null &&
				// !"".equals(pax.getInfantName()))) {
				// payAsm.setPaxType(PaxTypeTO.PARENT);
				// } else {
				// payAsm.setPaxType(pax.getPaxType());
				// }

				/**
				 * In order to record the payment breakdown for refundable we need to have a payment. in this case a
				 * zero payment coz all the chargers will be payed by refundables itself. No external payment is
				 * required
				 */
				payAsm.addCashPayment(BigDecimal.ZERO, null, new Date(), null, null, null, null);
				requoteModifyRQ.getLccPassengerPayments().put(extChgEntry.getKey(), payAsm);
			}
		}
	}

	public String getResContactInfo() {
		return resContactInfo;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	public boolean isConvertedToGroupReservation() {
		return convertedToGroupReservation;
	}

	public String getGroupBookingReference() {
		return groupBookingReference;
	}

	public String getPaxNameDetails() {
		return paxNameDetails;
	}

	public void setPaxNameDetails(String paxNameDetails) {
		this.paxNameDetails = paxNameDetails;
	}

	public boolean isCsPnr() {
		return isCsPnr;
	}

	public void setCsPnr(boolean isCsPnr) {
		this.isCsPnr = isCsPnr;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}
}
