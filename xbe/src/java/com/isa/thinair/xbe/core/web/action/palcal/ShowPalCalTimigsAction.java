package com.isa.thinair.xbe.core.web.action.palcal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airreservation.api.dto.palCal.PalCalTimingDTO;
import com.isa.thinair.airreservation.api.model.PalCalTiming;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.palcal.PalCalHG;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.PalCal.PAL_CAL_TIMING_CONFIG),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT),
		@Result(name = S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX, type = JSONResult.class, value = "") })
public class ShowPalCalTimigsAction extends BaseRequestAwareAction {

	private PalCalTimingDTO palCaltimingDTO;
	private String airportCode;
	private int startRec;
	private int page;
	private int records;
	private int total;
	private String messageString;
	private String messageType;
	private String flightNo;

	private ArrayList<Map<String, Object>> palCalTimingsList;

	public String execute() {

		try {
			PalCalHG.setHtmlComp(request);
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PalCalHG.getClientErrors());

		} catch (ModuleException e) {
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String searchPalCalTimings() {
		try {
			
				int start = (page - 1) * 20;
				Page<PalCalTiming> palCalTimingPage;
				
				if (flightNo != null && flightNo.trim().equals("")) {
					flightNo = null;
				} else if (flightNo != null) {
					flightNo = flightNo.trim();

				}
	
				palCalTimingPage = ModuleServiceLocator.getReservationAuxilliaryBD().getAllPALCALTimings(airportCode, startRec, 20, flightNo);
	
				setPalCalTimingsList(getPalCalTimingGridData((List<PalCalTiming>) palCalTimingPage.getPageData()));
				this.setRecords(palCalTimingPage.getTotalNoOfRecords());
				this.total = palCalTimingPage.getTotalNoOfRecords() / 20;
				int mod = palCalTimingPage.getTotalNoOfRecords() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}
			

		} catch (ModuleException e) {
			return S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
		}
		return S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
	}

	public String savePAlCalTiming() {
		try {
			PalCalTiming palCaltiming = createPalCalTiming();
			ModuleServiceLocator.getReservationAuxilliaryBD().savePalCalTiming(palCaltiming);
			saveMessage(XBEConfig.getServerMessage("thrucheck.record.save.success", "en", null), WebConstants.MSG_SUCCESS);

		} catch (ModuleException me) {
			saveMessage(me.getMessageString(), WebConstants.MSG_ERROR);
			return S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
		} 
		return S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
	}
	
	public String deletePAlCalTiming(){
		try {
			PalCalTiming palCaltiming = createPalCalTiming();
			ModuleServiceLocator.getReservationAuxilliaryBD().deletePAlCalTiming(palCaltiming);
			saveMessage(XBEConfig.getServerMessage("thrucheck.record.cancel.success", "en", null), WebConstants.MSG_SUCCESS);

		} catch (ModuleException me) {
			saveMessage(me.getMessageString(), WebConstants.MSG_ERROR);
			return S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
		} 
		return S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
	}

	public void saveMessage(String userMessage, String msgType) {
		this.setMessageString(userMessage);
		this.setMessageType(msgType);
	}
	
	private PalCalTiming createPalCalTiming() throws ModuleException {
		PalCalTiming palCalTiming = new PalCalTiming();

		palCalTiming.setPalCalTimingId(palCaltimingDTO.getPalCalTimingId());
		palCalTiming.setAirportCode(palCaltimingDTO.getAirportCode());		
		palCalTiming.setPalDepGap(getminutesVaule(palCaltimingDTO.getPalDepGap()));
		palCalTiming.setCalRepIntv(getminutesVaule(palCaltimingDTO.getCalRepIntv()));
		palCalTiming.setCalRepIntvAfterCutoff(getminutesVaule(palCaltimingDTO.getCalRepIntvAfterCutoff()));
		palCalTiming.setLastCalGap(getminutesVaule(palCaltimingDTO.getLastCalGap()));
		palCalTiming.setStartingZuluDate(getDate(palCaltimingDTO.getStrStartingZuluDate(), true));
		palCalTiming.setEndingZuluDate(getDate(palCaltimingDTO.getStrEndingZuluDate(), false));
		palCalTiming.setStatus(palCaltimingDTO.getStatus());
		palCalTiming.setVersion(palCaltimingDTO.getVersion());
		
		if (palCaltimingDTO.isAllowAll()) {
			palCalTiming.setApplyToAllFlights(PalConstants.PalCalTimings.APPLY_TO_ALL_YES);
		} else {
			palCalTiming.setApplyToAllFlights(PalConstants.PalCalTimings.APPLY_TO_ALL_NO);
			palCalTiming.setFlightNo(palCaltimingDTO.getFlightNumber());
		}
		return palCalTiming;
	}

	private int getminutesVaule(String timeString) throws ModuleException {
		if (timeString == null || "".equals(timeString)) {
			throw new ModuleException("xbe.palcal.timing.invalid.time.string");
		}
		String[] time = timeString.split(PalCalTimingDTO.timing_splitter);
		int days = Integer.parseInt(time[0]);
		int hours = Integer.parseInt(time[1]);
		int mins = Integer.parseInt(time[2]);

		return mins + hours * 60 + days * 24 * 60;
	}

	private Date getDate(String dateStr, boolean isStartingDate) throws ModuleException {
		try {
			if (isStartingDate) {
				dateStr = dateStr + PalCalTimingDTO.START_DATE_TIME;
			} else {
				dateStr = dateStr + PalCalTimingDTO.END_DATE_TIME;
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat(PalCalTimingDTO.PAL_CAL_DATE_FORMAT);			
			return dateFormat.parse(dateStr);
		} catch (ParseException pe) {
			throw new ModuleException("");
		}
	}

	private ArrayList<Map<String, Object>> getPalCalTimingGridData(List<PalCalTiming> timingList) {
		ArrayList<Map<String, Object>> palCalTimingList = new ArrayList<Map<String, Object>>();

		PalCalTiming timing = null;
		int i = 0;
		Iterator<PalCalTiming> iterator = timingList.iterator();
		while (iterator.hasNext()) {
			timing = iterator.next();
			PalCalTimingDTO palacalDto = createPalaCalDto(timing);
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("PalCalTimingDTO", palacalDto);
			row.put("id", i++);
			palCalTimingList.add(row);
		}

		return palCalTimingList;
	}

	private PalCalTimingDTO createPalaCalDto(PalCalTiming timing) {
		PalCalTimingDTO palcalDto = new PalCalTimingDTO();

		palcalDto.setPalCalTimingId(timing.getPalCalTimingId());
		palcalDto.setAirportCode(timing.getAirportCode());
		palcalDto.setPalDepGap(getTimeGapString(timing.getPalDepGap()));
		palcalDto.setCalRepIntv(getTimeGapString(timing.getCalRepIntv()));
		palcalDto.setStrStartingZuluDate(getDateString(timing.getStartingZuluDate()));
		palcalDto.setStrEndingZuluDate(getDateString(timing.getEndingZuluDate()));		
		palcalDto.setLastCalGap(getTimeGapString(timing.getLastCalGap()));
		palcalDto.setCalRepIntvAfterCutoff(getTimeGapString(timing.getCalRepIntvAfterCutoff()));
		palcalDto.setStatus(timing.getStatus());
		palcalDto.setVersion(timing.getVersion());
		if (timing.getFlightNo() == null || timing.getFlightNo().isEmpty() ){
			palcalDto.setFlightNumber(PalCalTimingDTO.ALL_FLIGHTS);
		}else{
			palcalDto.setFlightNumber(timing.getFlightNo());
		}
		return palcalDto;
	}

	private String getTimeGapString(int palDepGap) {
		StringBuilder sb = new StringBuilder();
		int days  =  palDepGap / 1440;
		palDepGap =  palDepGap % 1440;
		int hours =  palDepGap / 60;
		palDepGap = palDepGap  % 60;
		int mins = palDepGap;

		sb.append(getStringValue(days)).append(":").append(getStringValue(hours)).append(":").append(getStringValue(mins));

		return sb.toString();
	}

	private String getStringValue(int i) {
		if (i < 10) {
			return "0"+i;
		} else {
			return String.valueOf(i);
		}
	}

	private static String getDateString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(CalendarUtil.PATTERN1);
		return dateFormat.format(date);
	}



	public PalCalTimingDTO getPalCaltimingDTO() {
		return palCaltimingDTO;
	}

	public void setPalCaltimingDTO(PalCalTimingDTO palCaltimingDTO) {
		this.palCaltimingDTO = palCaltimingDTO;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public int getStartRec() {
		return startRec;
	}

	public void setStartRec(int startRec) {
		this.startRec = startRec;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public ArrayList<Map<String, Object>> getPalCalTimingsList() {
		return palCalTimingsList;
	}

	public void setPalCalTimingsList(ArrayList<Map<String, Object>> customerList) {
		this.palCalTimingsList = customerList;
	}

	public String getMessageString() {
		return messageString;
	}

	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
}
