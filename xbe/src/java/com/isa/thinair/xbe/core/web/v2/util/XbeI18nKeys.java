package com.isa.thinair.xbe.core.web.v2.util;

public interface XbeI18nKeys {

	String CLIENT_MESSAGES = "client";

	String SERVER_MESSAGES = "server";

	String TRANSLATION = "translation";

	String MENU = "menu";

	String LOGIN = "login";
	
	
	String EMPTY_MESSAGE = "xbe.empty.msg";

	String XBE_TRANSLATION_DATA = "resources/i18n/XBETranslationData";

	String XBE_MENU_DATA = "resources/i18n/MenuData";

	String XBE_LOGIN_DATA = "resources/i18n/LoginData";

	String XBE_SERVER_MESSAGES_DATA = "resources/i18n/XBEServerMessages";

	String XBE_CLIENT_MESSAGES_DATA = "resources/i18n/XBEClientMessages";

}
