package com.isa.thinair.xbe.core.web.handler.checkin;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.flight.FlightHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.util.FlightUtils;

/**
 * @author Charith
 * 
 */
public class CheckinRH extends BasicRH {

	private static Log log = LogFactory.getLog(CheckinRH.class);

	/**
	 * Execute Method for Passenger Checkin Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = S2Constants.Result.SUCCESS;
		String hdnCheckinMode = request.getParameter("hdnCheckinMode");
		String flightId = request.getParameter("hdnFlightId");
		String airportCode = request.getParameter("selFromStn6");
		String newStatus = null;
		String errorMessage = "";
		String manualCheckinEnable = "";

		if (hdnCheckinMode != null && !"".equals(hdnCheckinMode) && flightId != null && !"".equals(flightId)) {
			try {
				manualCheckinEnable = ModuleServiceLocator.getAirportServiceBD().getAirport(airportCode).getManualCheckin();
				Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(new Integer(flightId).intValue());
				String currentStatus = FlightUtils.getSegmentCheckinStatus(flight.getFlightSegements(), airportCode);
				if (flight.getStatus().equals(Flight.FLIGHT_ACTIVE) && Airport.YES.equals(manualCheckinEnable)) {
					if (hdnCheckinMode.equals("OPENCHECKIN")) {
						if (currentStatus == null || currentStatus.equals(FlightSegement.CHECKIN_NOT_OPEN)
								|| currentStatus.equals(FlightSegement.CHECKIN_COMPLETE)) {
							newStatus = FlightSegement.CHECKIN_OPEN;
						} else {
							errorMessage = "Manual Checkin not open and complete segments are only allowed to open for checkin";
						}
					} else if (hdnCheckinMode.equals("COMPLETECHECKIN")) {
						if (currentStatus != null
								&& (currentStatus.equals(FlightSegement.CHECKIN_OPEN) || currentStatus
										.equals(FlightSegement.CHECKIN_RE_OPEN))) {
							newStatus = FlightSegement.CHECKIN_COMPLETE;

							// PFS process
							FlightSegement flightSegement = ModuleServiceLocator.getFlightServiceBD().getFlightSegmentForAirport(
									flight.getFlightId(), airportCode + "%");
							processPFS(request, flight.getOriginAptCode(), flight.getFlightNumber(),
									flightSegement.getEstTimeDepatureLocal());

						} else {
							errorMessage = "Manual Checkin open and re-open segments are only allowed to complete checkin";
						}
					} else if (hdnCheckinMode.equals("CLOSECHECKIN")) {
						if (currentStatus != null && (currentStatus.equals(FlightSegement.CHECKIN_COMPLETE))) {
							newStatus = FlightSegement.CHECKIN_CLOSE;
						} else {
							errorMessage = "Manual Checkin close segments are only allowed to complete checkin";
						}
					} else if (hdnCheckinMode.equals("REOPENCHECKIN")) {
						if (currentStatus != null && currentStatus.equals(FlightSegement.CHECKIN_CLOSE)) {
							newStatus = FlightSegement.CHECKIN_RE_OPEN;
						} else {
							errorMessage = "Manual Checkin close segments are only allowed to reopen for checkin";
						}
					}
				} else {
					errorMessage = "flight is in-active or airport is not enabled for manual checkin";
				}

				if (newStatus != null) {
					ModuleServiceLocator.getFlightServiceBD().updateFlightSegmentCheckinStatus(new Integer(flightId).intValue(),
							airportCode + "%", newStatus);
				} else {
					throw new ModuleException(errorMessage);
				}

			} catch (ModuleException moduleException) {
				log.error(
						"Exception in PassengerCheckinRH.execute - load flight [origin module=" + moduleException.getModuleDesc()
								+ "]", moduleException);
				if (moduleException.getExceptionCode().equals("module.unidentified.error")) {
					moduleException.setExceptionCode("unauthorized.operation");
				}
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		}

		try {
			setDisplayData(request, response);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in PassengerCheckinRH.execute - setDisplayData(request) [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

			if (moduleException.getExceptionCode().equals("reporting.date.exceeds.max.duration")) {
				forward = S2Constants.Result.SUCCESS;
			}

		} catch (Exception e) {
			log.error("Exception in PassengerCheckinRH.execute - setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Sets the Display Data for the MANIFEST Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		setClientErrors(request);
		setCheckinStatusHtml(request);
		setStatusHtml(request);
		setFlightRowHtml(request, response);
		setOnlineAirportHtml(request);
		// setPaxDet(request);
	}

	/**
	 * Sets the Status of the Fligt to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightStatus();
		request.getSession().setAttribute(WebConstants.SES_HTML_STATUS_DATA, strHtml);
	}

	/**
	 * Sets the Status of the Checkins to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCheckinStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createCheckinStatus();
		request.getSession().setAttribute(WebConstants.SES_HTML_CHECKIN_STATUS_DATA, strHtml);
	}

	/**
	 * Sets the Flight Grid Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightRowHtml(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
		String strHtml = htmlGen.getFlightRowHtml(request, response);
		request.setAttribute(WebConstants.REQ_HTML_FLIGHT_DATA, strHtml);
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = FlightHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets Online & Active Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createActiveAirportCodeList();
		request.getSession().setAttribute(WebConstants.SES_HTML_AIRPORT_DATA, strHtml);
	}

	private static void processPFS(HttpServletRequest request, String fromAirport, String flightNumber, Date departureDate)
			throws ModuleException {
		int pfsId = ModuleServiceLocator.getReservationBD().getPFS(fromAirport, flightNumber, departureDate);
		if (pfsId == 0) {
			createdPFS(fromAirport, flightNumber, departureDate);
			pfsId = ModuleServiceLocator.getReservationBD().getPFS(fromAirport, flightNumber, departureDate);
		}
		ServiceResponce serviceResponce = ModuleServiceLocator.getReservationBD().reconcileReservations(pfsId, true, true, null,
				AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
		if (serviceResponce != null && !serviceResponce.isSuccess()) {
			// Handle error
			saveMessage(request, serviceResponce.getResponseCode(), WebConstants.MSG_ERROR);
		}
	}

	private static void createdPFS(String fromAirport, String flightNumber, Date departureDate) throws ModuleException {
		Pfs presentPfs = new Pfs();
		presentPfs.setDateDownloaded(new Date());// TODO get from jsp file
		presentPfs.setDepartureDate(departureDate);
		presentPfs.setFlightNumber(flightNumber);
		presentPfs.setFromAirport(fromAirport);
		presentPfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);
		ModuleServiceLocator.getReservationBD().savePfs(presentPfs);
	}
}
