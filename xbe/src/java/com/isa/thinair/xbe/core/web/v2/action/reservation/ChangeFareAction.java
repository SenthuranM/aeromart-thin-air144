package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import org.apache.struts2.json.annotations.JSON;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentsFareDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO.FareType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.SegmentInvFareTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.OpenReturnInfo;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.AllFaresInformationDTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.ChangeFareInfoFETO;
import com.isa.thinair.xbe.api.dto.v2.FareTypeInfoTO;
import com.isa.thinair.xbe.api.dto.v2.FlightInfoFETO;
import com.isa.thinair.xbe.api.dto.v2.InvAllocationDTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the available fares
 * 
 * @author Dhanya
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ChangeFareAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ChangeFareAction.class);

	private static String MODE_SHOW_ALL_FARES = "SHOWALLFARES";
	private static String MODE_CHANGE_FARES = "CHANGEFARES";

	private FlightSearchDTO changeFareQuoteParams;

	private String selectedFlightSegments;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private Collection<FlightInfoTO> flightInfo;

	private List<AllFaresInformationDTO> allFaresInfo;

	private HashMap<String, List<AllFaresInformationDTO>> allFaresInfoMap;

	private String selectedSegmentRefNumber;

	private String mode;

	private String changedFaresInfo;

	private FareTypeInfoTO availableFare;

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private boolean success = true;

	private String messageTxt;

	private String airLine;

	private String carrierWiseOnd;

	private String carrierWiseSeg;

	private boolean modifyBooking = false;

	private String classOfServiceDesc;

	private OpenReturnInfo openReturnInfo;

	private boolean isOpenReturn = false;

	private boolean showAllBC = false;

	private String logicalCCSelection;

	private boolean multicitySearch = false;

	private String pnr;

	private boolean isRequote = false;

	private boolean domFareDiscountApplied = false;

	private DiscountedFareDetails appliedFareDiscount = null;

	private String cabinClassRanking;

	private boolean showFareDiscountCodes = false;

	private List<String[]> fareDiscountCodeList = null;

	private boolean showNestedAvailableSeats = false;

	private String dummyCreditDiscountAmount = "0.00";

	private boolean showAgentCommission = false;

	private String allFaresInfoStr;

	private boolean allowHalfRetFareForOpenReturn = false;

	private List<String> fareQuotedFltRefList = new ArrayList<String>();

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();

	private boolean splitOperationForBusInvoked;

	private boolean displayBasedOnTemplates = false;
	
	private String ondListString;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			// boolean hasPrivViewAvailability = BasicRH.hasPrivilege(request,
			// PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			// boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request,
			// PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rParm = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
			boolean isGoshowAgent = false;

			this.splitOperationForBusInvoked = isOndSplitInvokedForBusFareQuote(bookingShoppingCart.getPriceInfoTO());

			showAgentCommission = AppSysParamsUtil.isAgentCommmissionEnabled();

			showNestedAvailableSeats = AppSysParamsUtil.showNestedAvailableSeats();

			allowHalfRetFareForOpenReturn = AppSysParamsUtil.isAllowHalfReturnFaresForOpenReturnBookings();

			displayBasedOnTemplates = AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed();

			if (mode != null && mode.equals(MODE_SHOW_ALL_FARES) && selectedSegmentRefNumber != null) {
				boolean hasPrivInterlineSearch = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)
						&& AppSysParamsUtil.isLCCConnectivityEnabled();

				boolean isAllowOverrideSameOrHigherFare = BasicRH.hasPrivilege(request,
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALLOW_OVERRIDE_SAME_OR_HIGHER_FARE);

				FareAvailRQ fareAvailRQ = ReservationBeanUtil.createFareAvailRQ(changeFareQuoteParams, isRequote,
						multicitySearch);

				// TODO Remove this
				// if (logicalCCSelection != null && !"".equals(logicalCCSelection) &&
				// !"null".equals(logicalCCSelection)) {
				// Map<String, String> segWiseLogicalCC =
				// ReservationBeanUtil.getSegmentWiseLogicalCC(logicalCCSelection);
				// String logicalCabinClass = segWiseLogicalCC.get(FlightRefNumberUtil
				// .getSegmentCodeFromFlightRPH(selectedSegmentRefNumber));
				// fareAvailRQ.getClassOfServicePrefTO().getLogicalCabinClassSelection().put(logicalCabinClass, null);
				// }

				if (modifyBooking) {
					ReservationBeanUtil.setExistingSegInfo(fareAvailRQ, request.getParameter("resSegments"),
							request.getParameter("modifySegment"));
					fareAvailRQ.getAvailPreferences().setModifyBooking(true);
				}

				if (!isRequote && !multicitySearch) {
					setOndOptions(fareAvailRQ, selectedFlightSegments, null);
				}
				ReservationApiUtils.replaceOnDInformationForFareQuoteAfterCityBaseSearch(fareAvailRQ);
				fareAvailRQ.setSelectedOBFltRefNo(selectedSegmentRefNumber);

				fareAvailRQ.getAvailPreferences().setAllowOverrideSameOrHigherFareMod(isAllowOverrideSameOrHigherFare);
				fareAvailRQ.getAvailPreferences().setRequoteFlightSearch(isRequote);

				String strAgent = null;
				Integer channelCode = -1;
				boolean excludeNonLowestPublicFare = false;
				boolean excludePublicOnewayForReturn = true;
				if (rParm.isAllowOverrideAgentFareOnly()) {
					strAgent = userPrincipal.getAgentCode();
					excludeNonLowestPublicFare = true;
					channelCode = SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY);
				}
				if (rParm.isAllowOverrideVisibleFare()) {
					excludeNonLowestPublicFare = false;
					strAgent = userPrincipal.getAgentCode();
					channelCode = SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY);
					excludePublicOnewayForReturn = !rParm.isAllowViewPublicOnewayFaresForReturn();
				}
				if (rParm.isAllowOverrideAnyFare()) {
					excludeNonLowestPublicFare = false;
					strAgent = null;
					channelCode = -1;
					strAgent = null;
					excludePublicOnewayForReturn = !rParm.isAllowViewPublicOnewayFaresForReturn();
				} else {
					if (changeFareQuoteParams.getTravelAgentCode() != null
							&& !changeFareQuoteParams.getTravelAgentCode().equals("")) {
						strAgent = changeFareQuoteParams.getTravelAgentCode();
					}
				}

				if (!rParm.isAllowOverrideAgentFareOnly() && !rParm.isAllowOverrideVisibleFare()
						&& !rParm.isAllowOverrideAnyFare()) {
					excludeNonLowestPublicFare = true;
					strAgent = userPrincipal.getAgentCode();
					channelCode = SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY);
				}

				fareAvailRQ.setSalesChannelCode(channelCode);
				fareAvailRQ.getAvailPreferences().setTravelAgentCode(strAgent);
				if (AppSysParamsUtil.isOnlyGoshoFaresVisibleInFlightCutoffTime()) {
					isGoshowAgent = ModuleServiceLocator.getAirportBD().isGoshoAgent(changeFareQuoteParams.getFromAirport());
				}
				if (isGoshowAgent) {
					fareAvailRQ.getAvailPreferences().setGoshoFareAgent(true);
				} else if (ReservationUtil.isCharterAgent(request)) {
					fareAvailRQ.getAvailPreferences().setFixedFareAgent(true);
				}
				fareAvailRQ.setExcludeNonLowestPublicFares(excludeNonLowestPublicFare);
				fareAvailRQ.setExcludeOnewayPublicFaresFromReturnJourneys(excludePublicOnewayForReturn);

				/*
				 * Check the privileges to search preferred system. In case of not enough privilege gracefully cut down
				 * to match the available privileges
				 */
				SYSTEM searchSystem = deriveSearchSystem(fareAvailRQ.getAvailPreferences().getSearchSystem(),
						hasPrivInterlineSearch);

				fareAvailRQ.getAvailPreferences().setSearchSystem(searchSystem);

				fareAvailRQ.setTransactionIdentifier(resInfo.getTransactionId());
				if (showAgentCommission) {
					if (searchSystem != SYSTEM.AA) {
						showAgentCommission = false;
					}
				}

				// if (showAllBC) {
				// clearClassOfServiceSelection(fareAvailRQ.getOriginDestinationInformationList());
				// }
				fareAvailRQ.getAvailPreferences().setSearchAllBC(showAllBC);

				if (AppSysParamsUtil.isModifyFQOnInitialSegmentFQDate()) {
					if (bookingShoppingCart.getPriceInfoTO() != null
							&& bookingShoppingCart.getPriceInfoTO().getLastFareQuotedDate() == null) {
						fareAvailRQ.getAvailPreferences().setSkipValidityFQ(true);
					}
				}

				if (fareAvailRQ.getTravelPreferences().isOpenReturn() && allowHalfRetFareForOpenReturn) {
					fareAvailRQ.getAvailPreferences().setHalfReturnFareQuote(true);
				}

				FareAvailRS fareAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchFares(fareAvailRQ,
						getTrackInfo());

				boolean modifyBookingToLowCos = true;

				if (isRequote || modifyBooking) {
					modifyBookingToLowCos = BasicRH.hasPrivilege(request,
							PriviledgeConstants.PRIVI_ALLOW_MODIFY_BOOKINGS_TO_LOW_PRIORITY_COS);
				}

				for (SegmentInvFareTO segInvFare : fareAvailRS.getSegmentInvFares()) {
					fareQuotedFltRefList.add(segInvFare.getFlightRefNumber());
				}

				List<String> allowedReturnFareCOSList = getAllowedModifyReturnFareCOSList(modifyBookingToLowCos);

				boolean allowOverrideOnHold = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_OVERRIDE_ONHOLD);

				Set<Integer> commissionApplicableFareIds = null;
				if (bookingShoppingCart.getAgentCommissions() != null) {
					commissionApplicableFareIds = bookingShoppingCart.getAgentCommissions().getCommissionApplicableFareIds();
				}

				boolean showFlownFltClosedBC = true;
				if (isRequote && AppSysParamsUtil.isCheckPrivilegeForFlownFlightsClosedBC()) {
					showFlownFltClosedBC = BasicRH.hasPrivilege(request, PriviledgeConstants.SHOW_FLOWN_FLIGHTS_CLS_BC);
				}

				boolean hideCommisionInfro = bookingShoppingCart.isRestrictCommissionVisibility();
				allFaresInfoMap = ReservationUtil.getAllFaresInformationDTO(fareAvailRS, selectedSegmentRefNumber,
						allowOverrideOnHold, modifyBookingToLowCos, allowedReturnFareCOSList, commissionApplicableFareIds,
						hideCommisionInfro, showFlownFltClosedBC);
				allFaresInfo = allFaresInfoMap.get(selectedSegmentRefNumber);
				allFaresInfoMap.remove(selectedSegmentRefNumber);

				if (showNestedAvailableSeats && allFaresInfo != null) {
					int numberOfPax = fareAvailRQ.getTravelerInfoSummary().getPayablePaxCount();
					ReservationUtil.checkInvSuffiencyInAllSegments(allFaresInfoMap, allFaresInfo, numberOfPax);
				}

			} else if (mode != null && mode.equals(MODE_CHANGE_FARES)) {
				if (changedFaresInfo != null && !"".equals(changedFaresInfo)) {

					ChangeFareRQ changeFareRQ = ReservationBeanUtil.createChangeFareRQ(changeFareQuoteParams, isRequote,
							multicitySearch);

					changeFareRQ.setCarrierWiseOnd(createCarrierWiseOndMap());

					SegmentsFareDTO segmentsFareDTO;
					FareSummaryDTO fareSummaryDTO;

					List<SegmentsFareDTO> changedFareSegmentList = new ArrayList<SegmentsFareDTO>();
					FareType fareType = FareType.STANDARD;

					changeFareRQ.setTransactionIdentifier(resInfo.getTransactionId());
					@SuppressWarnings("unchecked")
					List<ChangeFareInfoFETO> changeInfoList = JSONFETOParser.parseCollection(ArrayList.class,
							ChangeFareInfoFETO.class, changedFaresInfo);

					boolean allowOverbook = false;

					if (BookingClass.BookingClassType.OVERBOOK.equals(changeFareRQ.getTravelPreferences().getBookingType())) {
						allowOverbook = true;
					}

					IPaxCountAssembler paxAssm = new PaxCountAssembler(
							changeFareRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());

					// List<Integer> existingFltSegIds = changeFareRQ.getExistingFlightSegIds();
					for (int i = 0; i < changeInfoList.size(); i++) {
						ChangeFareInfoFETO chgFareInfo = changeInfoList.get(i);

						FareRuleDTO fareRule = chgFareInfo.getFareRule();
						InvAllocationDTO seatInfo = chgFareInfo.getSeatInfo();
						FlightInfoFETO fltInfo = chgFareInfo.getFlightInfo();

						// This validation is to handle temporarily when inventory is not their won't allow the user to
						// confirm the selected fare. This value may not accurate because this is without considering
						// the curtailed, overbook those factors. Still when click the book chances of inventory
						// failure.
						if (changeFareRQ.getOriginDestinationInformation(fltInfo.getOndSequence()) != null) {
							if (BookingClass.BookingClassType.OVERBOOK.equals(changeFareRQ
									.getOriginDestinationInformation(fltInfo.getOndSequence()).getPreferredBookingType())) {
								allowOverbook = true;
							} else {
								allowOverbook = false;
							}
						}

						segmentsFareDTO = new SegmentsFareDTO();
						fareSummaryDTO = new FareSummaryDTO();

						fareSummaryDTO.setFareId(fareRule.getFareId());
						segmentsFareDTO.setFareType(chgFareInfo.getFareType());
						segmentsFareDTO.setFixedQuotaSeats(seatInfo.isFixedQuota());
						segmentsFareDTO.setFareSummaryDTO(fareSummaryDTO);
						segmentsFareDTO.setCabinClassCode(seatInfo.getCcCode());
						segmentsFareDTO.setLogicalCabinClassCode(seatInfo.getLogicalCabinClass());
						segmentsFareDTO.setOndSequence(fltInfo.getOndSequence());
						segmentsFareDTO.setRequestedOndSequence(fltInfo.getRequestedOndSequence());

						boolean returnFlight = false;
						int effectiveOndSequence = fltInfo.getOndSequence();
						if (fltInfo.getRequestedOndSequence() != -1) {
							effectiveOndSequence = fltInfo.getRequestedOndSequence();
						}
						if (isRequote || multicitySearch) {
							returnFlight = fltInfo.isReturnFlag();
						} else if (!isRequote && !multicitySearch && effectiveOndSequence == OndSequence.IN_BOUND) {
							returnFlight = true;
						}

						if (FareTypes.RETURN_FARE == chgFareInfo.getFareType()
								|| FareTypes.HALF_RETURN_FARE == chgFareInfo.getFareType()) {
							segmentsFareDTO.setOndCode(fltInfo.getOndCode());
						} else {
							segmentsFareDTO.setOndCode(fareRule.getOrignNDest());
						}

						if (i == 0) { // Take the selected fare quota type of first flight segment
							fareType = seatInfo.isFixedQuota() ? FareType.FIXED : FareType.STANDARD;
						}

						int seatsNo = changeFareQuoteParams.getAdultCount().intValue()
								+ changeFareQuoteParams.getChildCount().intValue();

						SeatDistribution seatDistribution = new SeatDistribution();
						seatDistribution.setBookingClassCode(seatInfo.getBookingCode());
						seatDistribution.setNoOfSeats(seatsNo);
						seatDistribution.setOnholdRestricted(seatInfo.hasOnholdRestricted());
						seatDistribution.setBookingClassType(seatInfo.getBcType());
						seatDistribution.setBookedFlightCabinBeingSearched(seatInfo.isBookedFlightCabinBeingSearched());
						seatDistribution.setSameBookingClassAsExisting(seatInfo.isSameBookingClassBeingSearch());

						if (FareTypes.RETURN_FARE == chgFareInfo.getFareType() || FareTypes.OND_FARE == chgFareInfo.getFareType()
								|| FareTypes.HALF_RETURN_FARE == chgFareInfo.getFareType()) {
							setSelectedFlightNestedSeatDis(chgFareInfo.getFareType(), seatInfo, fltInfo.getFltSegRefNo(), seatsNo,
									seatDistribution);
						}

						boolean isOverbook = false;
						boolean allowNesting = false;
						int totalAdult = paxAssm.getAdultCount() + paxAssm.getChildCount();

						if (!allowOverbook && showNestedAvailableSeats && (totalAdult > seatInfo.getAvailableMaxSeats())
								&& (seatInfo.getAvailableNestedSeats() >= totalAdult) && !seatInfo.isFlownOnd()
								&& !seatInfo.isSameBookingClassBeingSearch()) {
							allowNesting = true;
						}

						if (!allowNesting && seatInfo.getAvailableMaxSeats() < totalAdult) {

							if (!allowOverbook && !seatInfo.isFlownOnd() && !seatInfo.isSameBookingClassBeingSearch()) {
								throw new ModuleException("airinventory.segment.inventory.unavailable",
										AirinventoryCustomConstants.INV_MODULE_CODE);
							} else {
								isOverbook = true;
							}
						}

						seatDistribution.setOverbook(isOverbook);
						seatDistribution
								.setFlownOnd(isFlownSegment(changeFareQuoteParams.getOndList(), fltInfo.getFltSegRefNo()));

						if (seatInfo.isOpenRTSegment()) {
							seatDistribution.setNoOfSeats(totalAdult);
						} else if (seatInfo.getAvailableSeats() < totalAdult && allowNesting) {
							int availableSeats = seatInfo.getAvailableSeats();
							if (FareTypes.RETURN_FARE == chgFareInfo.getFareType() && i > 0) {
								availableSeats = seatDistribution.getNoOfSeats();
							}
							int requiredSeats = totalAdult - availableSeats;
							seatDistribution.setNoOfSeats(availableSeats);

							ArrayList<SeatDistribution> nestedSeatDistributionList = getNestedSeatDistributionList(
									chgFareInfo.getFareType(), seatInfo.getBookingCode(), requiredSeats, seatInfo.getNestRank(),
									seatDistribution.isFlownOnd(), fltInfo.getFltSegRefNo(), seatInfo.getCcCode());
							segmentsFareDTO.getSeatDistributions().addAll(nestedSeatDistributionList);
						}

						segmentsFareDTO.addSeatDistributions(seatDistribution);

						for (int x = i; x < changeInfoList.size(); x++) {
							ChangeFareInfoFETO innerChgFareInfo = changeInfoList.get(x);
							FlightInfoFETO innerFltInfo = innerChgFareInfo.getFlightInfo();

							if (fltInfo.getOndGroup().equals(innerFltInfo.getOndGroup())
									&& fltInfo.isReturnFlag() == innerFltInfo.isReturnFlag()) {
								if (!StringUtil.isNullOrEmpty(fltInfo.getFltSegRefNo())
										&& fltInfo.getFltSegRefNo().equals(innerFltInfo.getFltSegRefNo())) {
									segmentsFareDTO.addSegment(
											FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltInfo.getFltSegRefNo()),
											fltInfo.getFltSegRefNo());
									i = x;
								}
							} else {
								break;
							}
						}

						segmentsFareDTO.setInbound(fltInfo.isReturnFlag());

						changedFareSegmentList.add(segmentsFareDTO);
					}

					if (modifyBooking) {
						ReservationBeanUtil.setExistingSegInfo(changeFareRQ, request.getParameter("resSegments"),
								request.getParameter("modifySegment"));
						changeFareRQ.getAvailPreferences().setModifyBooking(true);
					}

					if (!isRequote && !multicitySearch) {
						setOndOptions(changeFareRQ, selectedFlightSegments, changeInfoList);
					}
				
					ReservationApiUtils.replaceOnDInformationForFareQuoteAfterCityBaseSearch(changeFareRQ);
					changeFareRQ.setPnr(pnr);

					String groupPNR = request.getParameter("groupPNR");
					if (isValidString(groupPNR)) {
						changeFareRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
					}

					OndRebuildCriteria ondRebuildCriteria = null;
					if (changeFareRQ.getAvailPreferences().getSearchSystem() != SYSTEM.INT
							&& bookingShoppingCart.getPriceInfoTO() != null) {
						ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(changeFareRQ,
								bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
					}
					if (AppSysParamsUtil.isModifyFQOnInitialSegmentFQDate()) {
						if (bookingShoppingCart.getPriceInfoTO() != null
								&& bookingShoppingCart.getPriceInfoTO().getLastFareQuotedDate() == null) {
							changeFareRQ.getAvailPreferences().setSkipValidityFQ(true);
						}
					}

					changeFareRQ.setPosAirport(userPrincipal.getAgentStation());
					changeFareRQ.setChangedFareSegments(changedFareSegmentList);
					changeFareRQ.setSelectedFareType(fareType);
					changeFareRQ.setOndRebuildCriteria(ondRebuildCriteria);

					FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD()
							.changeFlightPrice(changeFareRQ, getTrackInfo());

					// Collection<AvailableFlightInfo> outboundFlights = ReservationBeanUtil.createAvailFlightInfoList(
					// flightPriceRS, changeFareQuoteParams, hasPrivViewAvailability, viewFlightsWithLesserSeats,
					// "depature", false, false, null);
					// Collection<AvailableFlightInfo> returnFlights =
					// ReservationBeanUtil.createAvailFlightInfoList(flightPriceRS,
					// changeFareQuoteParams, hasPrivViewAvailability, viewFlightsWithLesserSeats, "return", false,
					// false,
					// null);

					isOpenReturn = (flightPriceRS.getOpenReturnOptionsTO() != null);
					if (isOpenReturn) {
						openReturnInfo = new OpenReturnInfo(flightPriceRS.getOpenReturnOptionsTO(),
								changeFareQuoteParams.getFromAirport(), changeFareQuoteParams.getToAirport());
					}

					showFareDiscountCodes = AppSysParamsUtil.enableFareDiscountCodes();
					if (showFareDiscountCodes) {
						fareDiscountCodeList = SelectListGenerator.createFareDiscountCodeList();
					}

					PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();
					Collection<EXTERNAL_CHARGES> resettingExtChargee = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
					resettingExtChargee.add(EXTERNAL_CHARGES.CREDIT_CARD);
					resettingExtChargee.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
					bookingShoppingCart.resetSelectedExternalChargeAmount(resettingExtChargee);

					/*
					 * boolean blnFixedFareOverridden = false; boolean blnStdFareOverridden = false; if
					 * (priceInfoTO.hasFare(FareType.FIXED)) { blnFixedFareOverridden = true; } else {
					 * blnStdFareOverridden = true; }
					 */

					// Re-storing the fare from session, if not overridden
					if (priceInfoTO.getFareTypeTO() == null && bookingShoppingCart.getPriceInfoTO().getFareTypeTO() != null) {
						priceInfoTO.setFareTypeTO(bookingShoppingCart.getPriceInfoTO().getFareTypeTO());
					}
					ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
					ondWiseTotalFlexiAvailable = ReservationBeanUtil
							.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());

					if (priceInfoTO.getFareSegChargeTO() == null
							&& bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO() != null) {
						priceInfoTO.setFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
					}

					bookingShoppingCart.setPriceInfoTO(priceInfoTO);

					if (priceInfoTO != null) {
						ArrayList<String> domesticSegmentCodeList = new ArrayList<String>();
						ArrayList<String> internationalSegmentCodeList = new ArrayList<String>();
						// handling charge is not considered for modifications
						if (isCalculateHandlingFee()) {
							this.calculateTotalHandlingFee();
						}

						flightInfo = ReservationBeanUtil.createSelecteFlightInfo(flightPriceRS, domesticSegmentCodeList,
								internationalSegmentCodeList, null, false);

						flightInfo = SortUtil.sortFlightSegBySubJourneyGroup(new ArrayList<FlightInfoTO>(flightInfo));

						BigDecimal fareDiscountAmount = BigDecimal.ZERO;

						ApplicablePromotionDetailsTO promotionTO = (priceInfoTO.getFareTypeTO() != null)
								? priceInfoTO.getFareTypeTO().getPromotionTO()
								: null;

						if (promotionTO != null && !isRequote) {
							// Promo Code related discounts
							bookingShoppingCart
									.setFareDiscount(changeFareRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
							ReservationUtil.calculateDiscount(bookingShoppingCart, changeFareRQ,
									flightPriceRS.getTransactionIdentifier(), getTrackInfo());

							fareDiscountAmount = bookingShoppingCart.getTotalFareDiscount(true);

							if (fareDiscountAmount == null || fareDiscountAmount.doubleValue() == 0) {
								BigDecimal actualDiscount = bookingShoppingCart.getTotalFareDiscount(false);
								if (actualDiscount != null) {
									dummyCreditDiscountAmount = actualDiscount.toPlainString();
								}
							}

							appliedFareDiscount = bookingShoppingCart.getFareDiscount();
						} else {
							if (ReservationBeanUtil.hasIntAndDomFlights(domesticSegmentCodeList, internationalSegmentCodeList)) {
								bookingShoppingCart.setDomesticFareDiscount(
										changeFareRQ.getTravelerInfoSummary().getPassengerTypeQuantityList(),
										domesticSegmentCodeList);
								DiscountedFareDetails discountedFareDetails = bookingShoppingCart.getFareDiscount();
								if (discountedFareDetails.isDomesticDiscountApplied()) {
									ReservationUtil.calculateDiscount(bookingShoppingCart, changeFareRQ,
											flightPriceRS.getTransactionIdentifier(), getTrackInfo());
									fareDiscountAmount = bookingShoppingCart.getTotalFareDiscount(true);
									if (fareDiscountAmount.doubleValue() > 0) {
										domFareDiscountApplied = true;
									}
								}

							}

							if (!domFareDiscountApplied) {
								boolean hasAddFareDiscPrev = BasicRH.hasPrivilege(request,
										PriviledgeConstants.ALLOW_ADD_FARE_DISCOUNTS);
								boolean hasOverrideFareDiscPrev = BasicRH.hasPrivilege(request,
										PriviledgeConstants.OVERRIDE_FARE_DISCOUNT);

								ReservationUtil.applyFareDiscount(pnr, false, priceInfoTO.getFareTypeTO().getFareDiscountInfo(),
										bookingShoppingCart, changeFareRQ.getTravelerInfoSummary().getPassengerTypeQuantityList(),
										hasAddFareDiscPrev, hasOverrideFareDiscPrev, changeFareRQ,
										flightPriceRS.getTransactionIdentifier());

								fareDiscountAmount = bookingShoppingCart.getTotalFareDiscount(true);
							}

							if (fareDiscountAmount.doubleValue() > 0) {
								appliedFareDiscount = bookingShoppingCart.getFareDiscount();
							}
						}

						BigDecimal handlingCharge = BigDecimal.ZERO;
						ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

						handlingCharge = bookingShoppingCart.getHandlingCharge();
						handlingCharge = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, handlingCharge);
						// changeFareRQ.getChangedFareSegments() SegmentsFareDTO
						Map<String, String> changedFlights = new HashMap<String, String>();
						for (ChangeFareInfoFETO flight : changeInfoList) {
							changedFlights.put(flight.getFlightInfo().getOrignNDest(), flight.getFlightInfo().getOrignNDest());
						}

						availableFare = ReservationUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(),
								flightPriceRS.getOriginDestinationInformationList(), handlingCharge, changeFareQuoteParams, rParm,
								fareDiscountAmount, exchangeRateProxy, priceInfoTO.getFareSegChargeTO(), changeFareRQ,
								getTrackInfo(), null);
						// availableFare.setOverriddenFare(blnFareOverridden);
						bookingShoppingCart.setAgentCommissions(availableFare.getApplicableAgentCommissions());
						bookingShoppingCart.setTotAgentCommission(availableFare.getTotAgentCommission());

						String serviceTax = availableFare.getFareQuoteSummaryTO().getTotalServiceTax();
						request.getSession().setAttribute("serviceTax", serviceTax);

						FareQuoteSummaryTO fareQuoteSummaryTo = availableFare.getFareQuoteSummaryTO();
						BigDecimal administrationFee = AccelAeroCalculator.getDefaultBigDecimalZero();

						if (!isRequote) {
							administrationFee =	ReservationUtil.calclulateAdministrationFee(bookingShoppingCart, request, false,
									resInfo.getTransactionId(), getTrackInfo(), changeFareQuoteParams,
									flightPriceRS.getAllFlightSegments(), null, null, false);
						}
						fareQuoteSummaryTo.setAdminFee(administrationFee.toString());
						// converting Admin Fee to selected Currency and adding it to total price.
						String selectedCurrency = changeFareQuoteParams.getSelectedCurrency();
						
						if(multicitySearch) {
							if(changeFareQuoteParams.getOndListString()!=null) {
								setOndListString(changeFareQuoteParams.getOndListString());
							}
						}
						
						CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(
								CalendarUtil.getCurrentSystemTimeInZulu()).getCurrencyExchangeRate(selectedCurrency);
						Currency currency = currencyExchangeRate.getCurrency();
						fareQuoteSummaryTo.setSelectedEXRate(currencyExchangeRate.getExrateBaseToCurNumber().toPlainString());
						BigDecimal total = new BigDecimal(fareQuoteSummaryTo.getTotalPrice());
						BigDecimal selectedTotPrice = AccelAeroCalculator.add(total, administrationFee);
						fareQuoteSummaryTo.setSelectedtotalPrice(AccelAeroCalculator.formatAsDecimal(
								AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
										selectedTotPrice, currency.getBoundryValue(), currency.getBreakPoint())));
						// to display admin fee in selected currency
						fareQuoteSummaryTo.setSelectCurrAdminFee(AccelAeroCalculator.formatAsDecimal(
								AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
										administrationFee, currency.getBoundryValue(), currency.getBreakPoint())));
						// Adding AdminFee to total price.
						fareQuoteSummaryTo.setTotalPrice(
								new BigDecimal(fareQuoteSummaryTo.getTotalPrice()).add(administrationFee).toString());

						List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
						Collections.sort(allLogicalCCList, new Comparator<OndClassOfServiceSummeryTO>() {
							public int compare(OndClassOfServiceSummeryTO p1, OndClassOfServiceSummeryTO p2) {
								return new Long(p1.getSequence()).compareTo(new Long(p2.getSequence()));
							}
						});

						ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);

						setClassOfServiceDesc(ReservationBeanUtil.getClassOfServiceDescription(flightInfo));
					}

				}

			}

		} catch (Exception e) {
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private boolean isValidString(String str) {
		if (str != null && !"".equals(str.trim()) && !"undefined".equals(str.trim()) && !"null".equals(str)) {
			return true;
		}
		return false;
	}

	private void clearValues() {
		changedFaresInfo = null;
		outFlightRPHList = null;
		retFlightRPHList = null;
		availableFare = null;
		flightInfo = null;
		allFaresInfoStr = null;
	}

	private void setOndOptions(BaseAvailRQ baseAvailRQ, String flightSegments, List<ChangeFareInfoFETO> changeInfoList)
			throws Exception {
		Map<Integer, List<FlightSegmentTO>> fltSegMap = getOndSeqWiseSegments(flightSegments);

		int ondSequence = 0;
		for (OriginDestinationInformationTO ondInfo : baseAvailRQ.getOrderedOriginDestinationInformationList()) {
			List<FlightSegmentTO> ondFlights = fltSegMap.get(ondSequence);
			if (ondInfo.getOrignDestinationOptions().isEmpty()) {
				ondInfo.getOrignDestinationOptions().add(new OriginDestinationOptionTO());
			}

			if (changeInfoList != null && changeInfoList.size() > 0) {
				String prefCabinClass = null;
				String prefLogicalCabinClass = null;
				String prefBookingType = null;
				String preBookingClass = null;
				for (ChangeFareInfoFETO changeFareInfoFETO : changeInfoList) {
					if (changeFareInfoFETO.getFlightInfo() != null
							&& changeFareInfoFETO.getFlightInfo().getOndSequence() == ondSequence) {
						prefCabinClass = changeFareInfoFETO.getSeatInfo().getCcCode();
						prefLogicalCabinClass = changeFareInfoFETO.getSeatInfo().getLogicalCabinClass();
						preBookingClass = changeFareInfoFETO.getSeatInfo().getBookingCode();
						prefBookingType = changeFareInfoFETO.getSeatInfo().getBcType();
						break;
					}
				}

				ondInfo.setPreferredClassOfService(prefCabinClass);
				ondInfo.setPreferredLogicalCabin(prefLogicalCabinClass);
				ondInfo.setPreferredBookingClass(preBookingClass);
				ondInfo.setPreferredBookingType(prefBookingType);

				if (ondFlights != null) {
					for (FlightSegmentTO flightSegmentTO : ondFlights) {
						flightSegmentTO.setCabinClassCode(prefCabinClass);
						flightSegmentTO.setLogicalCabinClassCode(prefLogicalCabinClass);
					}
				}

			}

			if (ondInfo.getOrignDestinationOptions().get(0).getFlightSegmentList() != null) {
				ondInfo.getOrignDestinationOptions().get(0).getFlightSegmentList().clear();
			}
			ondInfo.getOrignDestinationOptions().get(0).getFlightSegmentList().addAll(ondFlights);
			ondSequence++;
		}
	}

	private Map<Integer, List<FlightSegmentTO>> getOndSeqWiseSegments(String flightSegments) {
		Map<Integer, List<FlightSegmentTO>> fltSegMap = new HashMap<Integer, List<FlightSegmentTO>>();

		if (!showAllBC) {
			ArrayList<FlightInfoTO> result = JSONFETOParser.parseCollection(ArrayList.class, FlightInfoTO.class, flightSegments);
			for (FlightInfoTO fltInfo : result) {
				FlightSegmentTO segment = new FlightSegmentTO();
				segment.setSegmentCode(fltInfo.getOrignNDest());
				segment.setFlightId(Integer.valueOf(fltInfo.getFlightId()));
				segment.setFlightNumber(fltInfo.getFlightNo());
				segment.setDepartureDateTime(new java.util.Date(Long.valueOf(fltInfo.getDepartureDateLong())));
				segment.setArrivalDateTime(new java.util.Date(Long.valueOf(fltInfo.getArrivalDateLong())));
				segment.setDepartureDateTimeZulu(new java.util.Date(Long.valueOf(fltInfo.getDepartureTimeZuluLong())));
				segment.setArrivalDateTimeZulu(new java.util.Date(Long.valueOf(fltInfo.getArrivalTimeZuluLong())));
				segment.setFlightRefNumber(fltInfo.getFlightRefNumber());
				segment.setRequestedOndSequence(fltInfo.getRequestedOndSequence());

				int ondSequence = fltInfo.getRequestedOndSequence();

				if (ondSequence == -1) {
					ondSequence = fltInfo.getOndSequence();
				}

				if (!fltSegMap.containsKey(ondSequence)) {
					fltSegMap.put(ondSequence, new ArrayList<FlightSegmentTO>());
				}

				fltSegMap.get(ondSequence).add(segment);

			}
		} else {
			ArrayList<FlightInfoFETO> result = JSONFETOParser.parseCollection(ArrayList.class, FlightInfoFETO.class,
					flightSegments);
			for (FlightInfoFETO fltInfo : result) {
				FlightSegmentTO segment = new FlightSegmentTO();
				segment.setFlightNumber(fltInfo.getFlightNo());
				segment.setDepartureDateTimeZulu(fltInfo.getDepartureDateTimeZulu());
				segment.setArrivalDateTimeZulu(fltInfo.getArrivalDateTimeZulu());
				segment.setFlightRefNumber(fltInfo.getFltSegRefNo());
				segment.setRequestedOndSequence(fltInfo.getRequestedOndSequence());
				int ondSequence = fltInfo.getRequestedOndSequence();

				if (ondSequence == -1) {
					ondSequence = fltInfo.getOndSequence();
				}
				if (!fltSegMap.containsKey(ondSequence)) {
					fltSegMap.put(ondSequence, new ArrayList<FlightSegmentTO>());
				}

				fltSegMap.get(ondSequence).add(segment);
			}
		}

		return fltSegMap;
	}

	public FlightSearchDTO getChangeFareQuoteParams() {
		return changeFareQuoteParams;
	}

	public List<AllFaresInformationDTO> getAllFaresInfo() {
		return this.allFaresInfo;
	}

	public void setChangeFareQuoteParams(FlightSearchDTO changeFareQuoteParams) {
		this.changeFareQuoteParams = changeFareQuoteParams;
	}

	public void setSelectedFlightSegments(String selectedFlightSegments) {
		this.selectedFlightSegments = selectedFlightSegments;
	}

	public void setSelectedSegmentRefNumber(String selectedSegmentRefNumber) {
		if (selectedSegmentRefNumber != null && selectedSegmentRefNumber.indexOf("#") != -1) {
			selectedSegmentRefNumber = selectedSegmentRefNumber.split("#")[0];
		}
		this.selectedSegmentRefNumber = selectedSegmentRefNumber;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public void setChangedFaresInfo(String changedFaresInfo) {
		this.changedFaresInfo = changedFaresInfo;
	}

	/**
	 * @param outFlightRPHList
	 *            the outFlightRPHList to set
	 */
	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	/**
	 * @param retFlightRPHList
	 *            the retFlightRPHList to set
	 */
	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	/**
	 * @return the availableFare
	 */
	public FareTypeInfoTO getAvailableFare() {
		return availableFare;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public Collection<FlightInfoTO> getFlightInfo() {
		return flightInfo;
	}

	/**
	 * @param flightPriceRQ
	 */
	public void addInbound(FlightPriceRQ flightPriceRQ) {
		OriginDestinationInformationTO returnOnD = flightPriceRQ.addNewOriginDestinationInformation();
		if (changeFareQuoteParams != null) {
			returnOnD.setOrigin(changeFareQuoteParams.getToAirport());
			returnOnD.setDestination(changeFareQuoteParams.getFromAirport());
		}
		returnOnD.setReturnFlag(true);
	}

	private SYSTEM deriveSearchSystem(SYSTEM searchSystem, boolean hasPrivInterlineSearch) throws ParseException {

		Set<String> ownAirlineCodes = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes();
		boolean nonOwnCarriersExists = false;
		boolean ownCarriersExists = false;

		if (isValidString(this.carrierWiseSeg)) {
			JSONArray jsonCarrierWiseSegs = (JSONArray) new JSONParser().parse(this.carrierWiseSeg);
			for (int i = 0; i < jsonCarrierWiseSegs.size(); i++) {
				JSONObject carrierObj = (JSONObject) jsonCarrierWiseSegs.get(i);
				for (String carrier : (Set<String>) carrierObj.keySet()) {
					if (!ownAirlineCodes.contains(carrier)) {
						nonOwnCarriersExists = true;
					} else {
						ownCarriersExists = true;
					}
				}
			}
		}

		if (ownCarriersExists && nonOwnCarriersExists) {
			searchSystem = SYSTEM.INT;
		} else if (ownCarriersExists) {
			searchSystem = SYSTEM.AA;
		}

		if (searchSystem == SYSTEM.ALL || searchSystem == SYSTEM.COND) {
			if (!hasPrivInterlineSearch) {
				searchSystem = SYSTEM.AA;
			}
		} else if (searchSystem == SYSTEM.INT && !hasPrivInterlineSearch) {
			searchSystem = SYSTEM.NONE;
		}

		return searchSystem;

	}

	private Map<String, List<String>> createCarrierWiseOndMap() throws ParseException {
		Map<String, List<String>> carrierWiseOnd = new HashMap<String, List<String>>();
		JSONArray jsonCarrierWiseOnd = (JSONArray) new JSONParser().parse(this.carrierWiseOnd);
		try {
			Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();

			for (int i = 0; i < jsonCarrierWiseOnd.size(); i++) {
				JSONObject carrierObj = (JSONObject) jsonCarrierWiseOnd.get(i);
				for (String key : (Set<String>) carrierObj.keySet()) {
					List<String> ondList = null;
					String carrierCode = key;
					if (busAirCarrierCodes.containsKey(key)) {
						carrierCode = busAirCarrierCodes.get(key);
					}
					if (carrierWiseOnd.containsKey(carrierCode)) {
						ondList = carrierWiseOnd.get(carrierCode);
					} else {
						ondList = new ArrayList<String>();
					}

					ondList.add((String) carrierObj.get(key));
					carrierWiseOnd.put(carrierCode, ondList);
				}
			}
		} catch (Exception ex) {
			log.error("Getting Carrier Codes Failed", ex);
		}

		return carrierWiseOnd;
	}

	private Map<String, List<String>> createCarrierWiseSegMap() throws ParseException {
		Map<String, List<String>> carrierWiseSegMap = new HashMap<String, List<String>>();
		JSONArray jsonCarrierWiseSeg = (JSONArray) new JSONParser().parse(this.carrierWiseSeg);

		try {
			Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();
			for (int i = 0; i < jsonCarrierWiseSeg.size(); i++) {
				JSONObject carrierObj = (JSONObject) jsonCarrierWiseSeg.get(i);
				for (String key : (Set<String>) carrierObj.keySet()) {
					List<String> strList = null;
					String carrierCode = key;
					if (busAirCarrierCodes.containsKey(key)) {
						carrierCode = busAirCarrierCodes.get(key);
					}

					if (carrierWiseSegMap.containsKey(carrierCode)) {
						strList = carrierWiseSegMap.get(carrierCode);
					} else {
						strList = new ArrayList<String>();
					}
					strList.add((String) carrierObj.get(key));
					carrierWiseSegMap.put(carrierCode, strList);
				}
			}
		} catch (Exception ex) {
			log.error("Getting Carrier Codes Failed", ex);
		}

		return carrierWiseSegMap;
	}

	private boolean isCalculateHandlingFee() {
		return (!modifyBooking && !changeFareQuoteParams.isOpenReturnConfirm());
	}

	private void calculateTotalHandlingFee() throws ModuleException {

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		PaxSet paxSet = new PaxSet(changeFareQuoteParams.getAdultCount(), changeFareQuoteParams.getChildCount(),
				changeFareQuoteParams.getInfantCount());
		ExternalChargeUtil.calculateExternalChargeAmount(paxSet, bookingShoppingCart.getPriceInfoTO(),
				bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.HANDLING_CHARGE),
				changeFareQuoteParams.isReturnFlag());

		bookingShoppingCart.addSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE);
	}

	/**
	 * @return the airLine
	 */
	@JSON(serialize = false)
	public String getAirLine() {
		return airLine;
	}

	/**
	 * @param airLine
	 *            the airLine to set
	 */
	public void setAirLine(String airLine) {
		this.airLine = airLine;
	}

	/**
	 * @return the carrierWiseOnd
	 */
	@JSON(serialize = false)
	public String getCarrierWiseOnd() {
		return carrierWiseOnd;
	}

	/**
	 * @param carrierWiseOnd
	 *            the carrierWiseOnd to set
	 */
	public void setCarrierWiseOnd(String carrierWiseOnd) {
		this.carrierWiseOnd = carrierWiseOnd;
	}

	/**
	 * @return the carrierWiseSeg
	 */
	@JSON(serialize = false)
	public String getCarrierWiseSeg() {
		return carrierWiseSeg;
	}

	/**
	 * @param carrierWiseSeg
	 *            the carrierWiseSeg to set
	 */
	public void setCarrierWiseSeg(String carrierWiseSeg) {
		this.carrierWiseSeg = carrierWiseSeg;
	}

	/**
	 * @return
	 */
	public boolean isModifyBooking() {
		return modifyBooking;
	}

	/**
	 * @param modifyBooking
	 */
	public void setModifyBooking(boolean modifyBooking) {
		this.modifyBooking = modifyBooking;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}

	/**
	 * @return the openReturnInfo
	 */
	public OpenReturnInfo getOpenReturnInfo() {
		return openReturnInfo;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	public void setOndLogicalCCList(List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList) {
		this.ondLogicalCCList = ondLogicalCCList;
	}

	public void setLogicalCCSelection(String logicalCCSelection) {
		this.logicalCCSelection = logicalCCSelection;
	}

	public void setShowAllBC(boolean showAllBC) {
		this.showAllBC = showAllBC;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public boolean isMulticitySearch() {
		return multicitySearch;
	}

	public void setMulticitySearch(boolean multicitySearch) {
		this.multicitySearch = multicitySearch;
	}

	public boolean isShowFareDiscountCodes() {
		return showFareDiscountCodes;
	}

	public void setShowFareDiscountCodes(boolean showFareDiscountCodes) {
		this.showFareDiscountCodes = showFareDiscountCodes;
	}

	public List<String[]> getFareDiscountCodeList() {
		return fareDiscountCodeList;
	}

	public void setFareDiscountCodeList(List<String[]> fareDiscountCodeList) {
		this.fareDiscountCodeList = fareDiscountCodeList;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isDomFareDiscountApplied() {
		return domFareDiscountApplied;
	}

	public void setDomFareDiscountApplied(boolean domFareDiscountApplied) {
		this.domFareDiscountApplied = domFareDiscountApplied;
	}

	public DiscountedFareDetails getAppliedFareDiscount() {
		return appliedFareDiscount;
	}

	public void setAppliedFareDiscount(DiscountedFareDetails appliedFareDiscount) {
		this.appliedFareDiscount = appliedFareDiscount;
	}

	private boolean isFlownSegment(List<ONDSearchDTO> ondSearchList, String fltSegRefNo) {
		if (ondSearchList != null && ondSearchList.size() > 0 && !StringUtil.isNullOrEmpty(fltSegRefNo)) {
			Iterator<ONDSearchDTO> ondSearchItr = ondSearchList.iterator();
			while (ondSearchItr.hasNext()) {
				ONDSearchDTO ondSearchDTO = ondSearchItr.next();
				List<String> flightRphList = ondSearchDTO.getFlightRPHList();
				if (flightRphList != null && flightRphList.size() > 0) {
					Iterator<String> flightRphItr = flightRphList.iterator();
					while (flightRphItr.hasNext()) {
						String fltRph = flightRphItr.next();
						if (fltSegRefNo.equals(fltRph)) {
							if (ondSearchDTO.isFlownOnd()) {
								return true;
							}

							return false;
						}

					}

				}
			}
		}
		return false;
	}

	public String getCabinClassRanking() {
		return cabinClassRanking;
	}

	public void setCabinClassRanking(String cabinClassRanking) {
		this.cabinClassRanking = cabinClassRanking;

	}

	public String getDummyCreditDiscountAmount() {
		return dummyCreditDiscountAmount;
	}

	private List<String> getAllowedModifyReturnFareCOSList(boolean modifyBookingToLowCos) {
		List<String> allowedReturnFareCOSList = new ArrayList<String>();

		HashMap<String, String> availableCOSRankMap = null;
		if (cabinClassRanking != null) {
			availableCOSRankMap = JSONFETOParser.parseMap(HashMap.class, String.class, cabinClassRanking);
			if (availableCOSRankMap == null) {
				availableCOSRankMap = new HashMap<String, String>();
			}
		}
		List<ONDSearchDTO> ondSearchList = changeFareQuoteParams.getOndList();

		if (ondSearchList != null && ondSearchList.size() > 0) {
			Iterator<ONDSearchDTO> ondSearchItr = ondSearchList.iterator();
			String prevCos = null;
			String highestRank = null;
			while (ondSearchItr.hasNext()) {
				ONDSearchDTO ondSearchDTO = ondSearchItr.next();
				String COS = ondSearchDTO.getClassOfService();

				int cabinClassRank = Integer.parseInt(availableCOSRankMap.get(COS));
				highestRank = COS;
				if (prevCos != null) {
					int prevCabinClassRank = Integer.parseInt(availableCOSRankMap.get(prevCos));
					if (prevCabinClassRank <= cabinClassRank) {
						highestRank = prevCos;
					}
				}

				allowedReturnFareCOSList.add(COS);

				prevCos = COS;
			}

			if (!modifyBookingToLowCos) {
				allowedReturnFareCOSList = new ArrayList<String>();
				allowedReturnFareCOSList.add(highestRank);
			}

		}
		return allowedReturnFareCOSList;
	}

	public boolean isShowNestedAvailableSeats() {
		return showNestedAvailableSeats;
	}

	public void setShowNestedAvailableSeats(boolean showNestedAvailableSeats) {
		this.showNestedAvailableSeats = showNestedAvailableSeats;
	}

	public boolean isShowAgentCommission() {
		return showAgentCommission;
	}

	public String getAllFaresInfoStr() {
		return allFaresInfoStr;
	}

	public void setAllFaresInfoStr(String allFaresInfoStr) {
		this.allFaresInfoStr = allFaresInfoStr;
	}

	/**
	 * @return the allFaresInfoMap
	 */
	public HashMap<String, List<AllFaresInformationDTO>> getAllFaresInfoMap() {
		return allFaresInfoMap;
	}

	/**
	 * @param allFaresInfoMap
	 *            the allFaresInfoMap to set
	 */
	public void setAllFaresInfoMap(HashMap<String, List<AllFaresInformationDTO>> allFaresInfoMap) {
		this.allFaresInfoMap = allFaresInfoMap;
	}

	/**
	 * If we select RT fare in change fare, we duplicate the same seat distribution to inbound segment in js level. But
	 * when we allow return nesting, we need to get actual seat distribution. Also if inventory go from same flight same
	 * booking class, no need to update the inventory again.
	 * 
	 */
	private void setSelectedFlightNestedSeatDis(int fareType, InvAllocationDTO seatInfo, String selectedFltRefNo, int seatCount,
			SeatDistribution seatDistribution) throws JSONException {

		int noOfSeats = seatCount;
		if (!StringUtil.isNullOrEmpty(allFaresInfoStr)) {
			@SuppressWarnings("unchecked")
			HashMap<String, ArrayList<AllFaresInformationDTO>> faresMapByFltSegRef = JSONFETOParser.parseMap(HashMap.class,
					ArrayList.class, allFaresInfoStr);

			for (String fltSegRef : faresMapByFltSegRef.keySet()) {
				if ((fltSegRef.equals(selectedFltRefNo) || fltSegRef.equals(getRPH(selectedFltRefNo)))
						&& faresMapByFltSegRef.get(fltSegRef) != null) {
					@SuppressWarnings("unchecked")
					List<AllFaresInformationDTO> allFaresInfoList = JSONFETOParser.parseCollection(ArrayList.class,
							AllFaresInformationDTO.class, JSONUtil.serialize(faresMapByFltSegRef.get(fltSegRef)));

					if (allFaresInfoList != null && allFaresInfoList.size() > 0) {
						Iterator<AllFaresInformationDTO> allFaresInfoItr = allFaresInfoList.iterator();
						while (allFaresInfoItr.hasNext()) {
							AllFaresInformationDTO faresInfo = allFaresInfoItr.next();
							if (fareType == Integer.parseInt(faresInfo.getFareType())) {
								List<InvAllocationDTO> invAllocationInfoList = faresInfo.getInvAllocationInfo();

								if (invAllocationInfoList != null && invAllocationInfoList.size() > 0) {
									boolean seatFound = false;
									Iterator<InvAllocationDTO> invAllocationInfoItr = invAllocationInfoList.iterator();
									while (invAllocationInfoItr.hasNext()) {
										InvAllocationDTO invAllocationDTO = invAllocationInfoItr.next();
										if (invAllocationDTO.getBookingCode().equals(seatInfo.getBookingCode())) {
											if (seatCount > invAllocationDTO.getAvailableSeats()) {
												noOfSeats = invAllocationDTO.getAvailableSeats();
											}
											seatDistribution.setSameBookingClassAsExisting(
													invAllocationDTO.isSameBookingClassBeingSearch());
											seatInfo.setSameBookingClassBeingSearch(
													invAllocationDTO.isSameBookingClassBeingSearch());
											seatInfo.setAvailableSeats(invAllocationDTO.getAvailableSeats());
											seatInfo.setAvailableNestedSeats(invAllocationDTO.getAvailableNestedSeats());
											seatFound = true;
											break;
										}
									}
									if (!seatFound) {
										noOfSeats = 0;
										seatInfo.setAvailableSeats(noOfSeats);
									}
								}
							}
						}
					}
				}
			}
		}
		if (!seatInfo.isSameBookingClassBeingSearch()) {
			seatDistribution.setNoOfSeats(noOfSeats);
		}
	}

	private String getRPH(String selectedFltRefNo) {
		String fltRPH = null;
		if (selectedFltRefNo.indexOf("#") != -1) {
			String arr[] = selectedFltRefNo.split("#");
			fltRPH = arr[0];
		} else {
			fltRPH = selectedFltRefNo;
		}
		return fltRPH;
	}

	private ArrayList<SeatDistribution> getNestedSeatDistributionList(int fareType, String bookingCode, int requiredSeats,
			int nestRank, boolean isFlownOnd, String selectedFltRefNo, String cabinClassCode) throws JSONException {

		ArrayList<SeatDistribution> seatDistributionList = new ArrayList<SeatDistribution>();
		Set<String> bcSet = new HashSet<String>();

		if (!StringUtil.isNullOrEmpty(allFaresInfoStr)) {
			@SuppressWarnings("unchecked")
			HashMap<String, ArrayList<AllFaresInformationDTO>> faresMapByFltSegRef = JSONFETOParser.parseMap(HashMap.class,
					ArrayList.class, allFaresInfoStr);

			for (String fltSegRef : faresMapByFltSegRef.keySet()) {
				if ((fltSegRef.equals(selectedFltRefNo) || fltSegRef.equals(getRPH(selectedFltRefNo)))
						&& faresMapByFltSegRef.get(fltSegRef) != null) {
					@SuppressWarnings("unchecked")
					List<AllFaresInformationDTO> allFaresInfoList = JSONFETOParser.parseCollection(ArrayList.class,
							AllFaresInformationDTO.class, JSONUtil.serialize(faresMapByFltSegRef.get(fltSegRef)));

					if (allFaresInfoList != null && allFaresInfoList.size() > 0) {
						Iterator<AllFaresInformationDTO> allFaresInfoItr = allFaresInfoList.iterator();
						while (allFaresInfoItr.hasNext()) {
							AllFaresInformationDTO faresInfo = allFaresInfoItr.next();
							if (fareType == Integer.parseInt(faresInfo.getFareType())) {
								List<InvAllocationDTO> invAllocationInfoList = faresInfo.getInvAllocationInfo();

								if (invAllocationInfoList != null && invAllocationInfoList.size() > 0) {

									Iterator<InvAllocationDTO> invAllocationInfoItr = invAllocationInfoList.iterator();
									while (invAllocationInfoItr.hasNext()) {
										InvAllocationDTO invAllocationDTO = invAllocationInfoItr.next();
										if (invAllocationDTO.getNestRank() != null && invAllocationDTO.getNestRank() > 0
												&& invAllocationDTO.getNestRank() < nestRank
												&& !bcSet.contains(invAllocationDTO.getBookingCode())
												&& invAllocationDTO.getCcCode().equals(cabinClassCode)) {
											int noOfSeats = 0;
											if (invAllocationDTO.getAvailableSeats() > 0
													&& invAllocationDTO.getAvailableSeats() >= requiredSeats) {
												noOfSeats = requiredSeats;

											} else if (invAllocationDTO.getAvailableSeats() > 0
													&& invAllocationDTO.getAvailableSeats() < requiredSeats) {
												noOfSeats = invAllocationDTO.getAvailableSeats();
											}
											requiredSeats = requiredSeats - noOfSeats;

											if (noOfSeats > 0) {
												SeatDistribution seatDistribution = new SeatDistribution();
												seatDistribution.setBookingClassCode(invAllocationDTO.getBookingCode());

												seatDistribution.setNoOfSeats(noOfSeats);
												seatDistribution.setOnholdRestricted(invAllocationDTO.hasOnholdRestricted());
												seatDistribution.setBookingClassType(invAllocationDTO.getBcType());
												seatDistribution.setBookedFlightCabinBeingSearched(
														invAllocationDTO.isBookedFlightCabinBeingSearched());
												seatDistribution.setSameBookingClassAsExisting(
														invAllocationDTO.isSameBookingClassBeingSearch());
												seatDistribution.setOverbook(false);
												seatDistribution.setFlownOnd(isFlownOnd);
												seatDistributionList.add(seatDistribution);
												bcSet.add(invAllocationDTO.getBookingCode());
											}
										}

										if (requiredSeats == 0) {
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return seatDistributionList;
	}

	public boolean isAllowHalfRetFareForOpenReturn() {
		return allowHalfRetFareForOpenReturn;
	}

	public void setAllowHalfRetFareForOpenReturn(boolean allowHalfRetFareForOpenReturn) {
		this.allowHalfRetFareForOpenReturn = allowHalfRetFareForOpenReturn;
	}

	public List<String> getFareQuotedFltRefList() {
		return fareQuotedFltRefList;
	}

	public void setFareQuotedFltRefList(List<String> fareQuotedFltRefList) {
		this.fareQuotedFltRefList = fareQuotedFltRefList;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public boolean isSplitOperationForBusInvoked() {
		return splitOperationForBusInvoked;
	}

	public void setSplitOperationForBusInvoked(boolean splitOperationForBusInvoked) {
		this.splitOperationForBusInvoked = splitOperationForBusInvoked;
	}

	private boolean isOndSplitInvokedForBusFareQuote(PriceInfoTO priceInfoTO) {
		if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote() && priceInfoTO != null && priceInfoTO.getFareSegChargeTO() != null
				&& priceInfoTO.getFareSegChargeTO().getOndFareSegChargeTOs() != null) {
			Collection<OndFareSegChargeTO> ondFareSegChargeTOs = priceInfoTO.getFareSegChargeTO().getOndFareSegChargeTOs();
			for (OndFareSegChargeTO ondFareSegChargeTO : ondFareSegChargeTOs) {
				if (ondFareSegChargeTO.isSplitOperationForBusInvoked())
					return true;

			}
		}
		return false;
	}

	public boolean isDisplayBasedOnTemplates() {
		return displayBasedOnTemplates;
	}

	/**
	 * @return the ondListString
	 */
	public String getOndListString() {
		return ondListString;
	}

	/**
	 * @param ondListString the ondListString to set
	 */
	public void setOndListString(String ondListString) {
		this.ondListString = ondListString;
	}
	
	

}
