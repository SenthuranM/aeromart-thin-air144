package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Collection;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.SystemParams;
import com.isa.thinair.xbe.api.dto.v2.ModifySegmentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Load Transfer Segment page with Availability Search Parameters
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_TRANSFER_SEGMENT),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class LoadTransferSegmentAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(LoadTransferSegmentAction.class);

	private String fltSegment;

	private String resSegments;

	private String groupPNRNo;

	private String selectedAlertSegment;

	private String transferAction;

	private boolean gdsPnr;
	
	private String resAlerts;

	public String execute() {

		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNRNo);
		try {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			boolean isInterlineCarrierReservation = ReservationUtil.isInterlineCarrierReservation(colsegs);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rparam = new ReservationProcessParams(request, resInfo.getReservationStatus(), colsegs,
					resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);
			if (isGroupPNR) {
				rparam.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}
			request.setAttribute("initialParams", JSONUtil.serialize(rparam));

			boolean isInterline = isInterlineCarrierReservation && isGroupPNR;
			Collection<LCCClientReservationSegment> colTransferSegs = ReservationUtil.getTransferSegments(colsegs, fltSegment,
					isInterline);
			validateFlownSegments(colTransferSegs, selectedAlertSegment);
			boolean isAlertSegmentTransfer = isAlertSegmentTransfer();
			if (isAlertSegmentTransfer && selectedAlertSegment != null) {
				if (selectedAlertSegment.split(",").length > 1) {
					throw new ModuleException("airreservations.transfersegment.segment.notmatch");
				}
				colTransferSegs = ReservationUtil.getAlertTransferSegment(colsegs, selectedAlertSegment);
			} else {
				colTransferSegs = ReservationUtil.getTransferSegments(colsegs, fltSegment, isInterline);
			}
			String transferedSegmentOpCarrier = null;
			TreeMap<Integer, LCCClientReservationSegment> flightSeqMap = new TreeMap<Integer, LCCClientReservationSegment>();
			for (LCCClientReservationSegment seg : colTransferSegs) {
				flightSeqMap.put(seg.getSegmentSeq(), seg);
				// get the operating carrier of the segment being transfered.
				// this is later used to restrict LCC availability search to search only this carrier.
				if (transferedSegmentOpCarrier == null) {
					transferedSegmentOpCarrier = getTransferedSegmentOperatingCarrier(fltSegment, seg);
				}
			}
			// we cannot limit availability search airline if this is an interline scenario.
			if (ReservationUtil.isInterlineCarrierReservation(colTransferSegs)) {
				transferedSegmentOpCarrier = null;
			}

			LCCClientReservationSegment departureFlight = flightSeqMap.get(flightSeqMap.firstKey());
			LCCClientReservationSegment arrivalFlight = flightSeqMap.get(flightSeqMap.lastKey());

			String depAirport = departureFlight.getSegmentCode().split("/")[0];
			String[] arrSegmentCodes = arrivalFlight.getSegmentCode().split("/");
			String arrAirport = arrSegmentCodes[arrSegmentCodes.length - 1];
			String strDepdate = DateFormatUtils.format(departureFlight.getDepartureDate(), "dd/MM/yyyy");

			ModifySegmentTO transferSegment = new ModifySegmentTO();
			transferSegment.setDepartureDate(strDepdate);
			transferSegment.setFrom(depAirport);
			transferSegment.setTo(arrAirport);
			transferSegment.setModifyingSegments(request.getParameter("fltSegment"));
			transferSegment.setMode(request.getParameter("pgMode"));
			transferSegment.setOhdReleaseTime(request.getParameter("ohdReleaseTime")); 
			transferSegment.setPNR(request.getParameter("pnrNo"));
			transferSegment.setGroupPNR(request.getParameter("groupPNRNo"));
			transferSegment.setBookingclass(departureFlight.getCabinClassCode());
			transferSegment.setBookingType(request.getParameter("bookingType"));
			transferSegment.setNoOfAdults(new Integer(request.getParameter("resNoOfAdults")));
			transferSegment.setNoOfInfants(new Integer(request.getParameter("resNoOfInfants")));
			transferSegment.setNoOfChildren(new Integer(request.getParameter("resNoOfchildren")));
			transferSegment.setVersion(request.getParameter("version"));
			transferSegment.setModifyingSegmentOperatingCarrier(transferedSegmentOpCarrier);
			transferSegment.setInterline(isInterline);
			transferSegment.setAlertSegmentTransfer(isAlertSegmentTransfer);
			transferSegment.setAlertSegmentID(selectedAlertSegment);
			transferSegment.setStatus(resInfo.getReservationStatus());
			transferSegment.setGdsPnr(isGdsPnr());

			request.setAttribute("resSegments", resSegments);
			request.setAttribute("transferSegment", JSONUtil.serialize(transferSegment));

			SystemParams sysParams = new SystemParams();
			sysParams.setMinimumTransitionTimeInMillis(AppSysParamsUtil.getMinimumReturnTransitTimeInMillis());
			sysParams.setDefaultLanguage(XBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));
			sysParams.setDynamicOndListPopulationEnabled(AppSysParamsUtil.isDynamicOndListPopulationEnabled());
			sysParams.setUserDefineTransitTimeEnabled(AppSysParamsUtil.isEnableUserDefineTransitTime());

			request.setAttribute("systemParams", JSONUtil.serialize(sysParams));
			request.setAttribute("excludableCharges", JSONUtil.serialize(ReservationUtil.getExcludableChargesMap()));
			request.setAttribute("carrierCode", AppSysParamsUtil.getDefaultCarrierCode());
			if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
				request.setAttribute("sysOndSource", AppSysParamsUtil.getDynamicOndListUrl());
			}
			request.setAttribute("resAlerts", resAlerts);

		} catch (Exception e) {
			log.error("ERROR in Transfering segments", e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	private boolean isAlertSegmentTransfer() {
		boolean isAlertSegmentTransfer = false;

		if ("tranSelAltSeg".equalsIgnoreCase(transferAction)) {
			isAlertSegmentTransfer = true;
		}

		return isAlertSegmentTransfer;
	}

	private String getTransferedSegmentOperatingCarrier(String transferingSegmentId, LCCClientReservationSegment comparedSegment) {
		String transferedSegmentOperatingCarrier = null;
		if (StringUtils.isNotEmpty(transferingSegmentId) && comparedSegment != null) {
			String[] selectedSegs = transferingSegmentId.split(",");
			StringBuffer bookingRefNumbers = new StringBuffer();
			for (int i = 0; i < selectedSegs.length; i++) {
				String[] selectedCNFSegs = selectedSegs[i].split("\\$");
				bookingRefNumbers.append(selectedCNFSegs[0]).append(",");
			}

			if (bookingRefNumbers.toString().contains(comparedSegment.getBookingFlightSegmentRefNumber())) {
				transferedSegmentOperatingCarrier = AppSysParamsUtil.extractCarrierCode(comparedSegment.getFlightNo());
			}
		}
		return transferedSegmentOperatingCarrier;
	}
	
	private void validateFlownSegments(Collection<LCCClientReservationSegment> colsegs, String selAlertSegment) throws ModuleException{
		for(LCCClientReservationSegment seg : colsegs){
			if (seg.isFlownSegment() && selAlertSegment.equalsIgnoreCase(seg.getBookingFlightSegmentRefNumber())) {
				throw new ModuleException("airreservations.transfersegment.flown.segment");
			}
		}
	}

	public String getFltSegment() {
		return fltSegment;
	}

	public void setFltSegment(String fltSegment) {
		this.fltSegment = fltSegment;
	}

	public void setResSegments(String resSegments) {
		this.resSegments = resSegments;
	}

	public void setGroupPNRNo(String groupPNRNo) {
		this.groupPNRNo = groupPNRNo;
	}

	public String getSelectedAlertSegment() {
		return selectedAlertSegment;
	}

	public void setSelectedAlertSegment(String selectedAlertSegment) {
		this.selectedAlertSegment = selectedAlertSegment;
	}

	public void setTransferAction(String transferAction) {
		this.transferAction = transferAction;
	}

	public boolean isGdsPnr() {
		return gdsPnr;
	}

	public void setGdsPnr(boolean gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	public String getResAlerts() {
		return resAlerts;
	}

	public void setResAlerts(String resAlerts) {
		this.resAlerts = resAlerts;
	}
}
