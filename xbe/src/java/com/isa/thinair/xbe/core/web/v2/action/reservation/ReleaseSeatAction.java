package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ReleaseSeatAction extends BaseRequestAwareAction {

	private static final Log log = LogFactory.getLog(ReleaseSeatAction.class);

	private String selectedAncillaries;

	private String success;

	public String getSelectedAncillaries() {
		return selectedAncillaries;
	}

	public void setSelectedAncillaries(String selectedAncillaries) {
		this.selectedAncillaries = selectedAncillaries;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String execute() {

		try {
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			String strTxnIdntifier = resInfo.getTransactionId();
			SYSTEM system = resInfo.getSelectedSystem();

			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}
			List<LCCSegmentSeatDTO> blockSeatDTOs = extractLCCBlockSeatDTOJSonString(selectedAncillaries);

			boolean blockSeatsStatus = ModuleServiceLocator.getAirproxyAncillaryBD().releaseSeats(blockSeatDTOs, strTxnIdntifier,
					system, TrackInfoUtil.getBasicTrackInfo(request));

			success = Boolean.toString(blockSeatsStatus);
		} catch (Exception e) {
			log.error("Generic exception caught", e);
			success = Boolean.toString(false);
		}
		return S2Constants.Result.SUCCESS;
	}

	private List<LCCSegmentSeatDTO> extractLCCBlockSeatDTOJSonString(String jSonString) throws ParseException {
		List<LCCSegmentSeatDTO> jsonList = new ArrayList<LCCSegmentSeatDTO>();

		if (jSonString != null && !jSonString.equals("")) {
			JSONParser parser = new JSONParser();

			JSONArray jsonArray = (JSONArray) parser.parse(jSonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();

				String flightRefNumber = (String) jObject.get("flightRefNumber");

				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
				flightSegmentTO.setFlightRefNumber(flightRefNumber);

				LCCSegmentSeatDTO blockSeatDTO = new LCCSegmentSeatDTO();
				blockSeatDTO.setFlightSegmentTO(flightSegmentTO);

				List<LCCAirSeatDTO> airSeatDTOs = new ArrayList<LCCAirSeatDTO>();

				JSONArray pax = (JSONArray) jObject.get("pax");
				JSONArray paxArray = (JSONArray) parser.parse(pax.toJSONString());
				Iterator<?> paxArrayIterator = paxArray.iterator();
				while (paxArrayIterator.hasNext()) {
					JSONObject paxJObject = (JSONObject) paxArrayIterator.next();
					JSONObject seat = (JSONObject) paxJObject.get("seat");

					String seatNumber = (String) seat.get("seatNumber");
					String seatCharge = (String) seat.get("seatCharge");

					LCCAirSeatDTO airSeatDTO = new LCCAirSeatDTO();
					airSeatDTO.setSeatNumber(seatNumber);
					airSeatDTO.setSeatCharge(new BigDecimal(seatCharge));

					airSeatDTOs.add(airSeatDTO);
				}

				blockSeatDTO.setAirSeatDTOs(airSeatDTOs);

				jsonList.add(blockSeatDTO);
			}
		}
		return jsonList;
	}
}
