package com.isa.thinair.xbe.core.web.handler.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ICommonsException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.config.XBEModuleConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.exception.XBEException;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public abstract class BasicRH {

	private static Log log = LogFactory.getLog(BasicRH.class);

	public static final String MSG_TYPE_ERROR = "error";

	public static final String MSG_TYPE_CONFIRMATION = "confirmation";

	/*
	 * This method is used to show the error, success, exceptional messages to the client/ user. To call this method,
	 * all the <Custom>RequestHandler classes should extend this abstract class.
	 */
	public static void saveMessage(HttpServletRequest request, String userMessage, String msgType) {
		// set the message
		request.setAttribute(WebConstants.REQ_MESSAGE, userMessage);

		// set the type of message. It could be Error, Warning, Confirmation
		request.setAttribute(WebConstants.MSG_TYPE, msgType);
	}

	/**
	 * 
	 * @param request
	 * @param strClientErrors
	 */
	public static void saveClientErrors(HttpServletRequest request, String strClientErrors) {
		if (strClientErrors == null) {
			strClientErrors = "";
		}

		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param page
	 * @throws ServletException
	 * @throws IOException
	 */
	public static void forward(ServletRequest request, ServletResponse response, String page) throws ServletException,
			IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		String userLanguage = (String) ((HttpServletRequest) request).getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (rd != null) {
			rd.forward(request, response);
		} else {
			log.error("Cannot get RequestDispatcher for " + page);
			response.getWriter().println(XBEConfig.getServerMessage("default.server.error", userLanguage, null));
		}
	}

	/**
	 * 
	 * @param response
	 * @param responseType
	 * @param responseData
	 * @throws IOException
	 */
	public static void sendMessage(ServletResponse response, String responseType, String responseData) throws IOException {
		StringBuffer strData = new StringBuffer();
		strData.append("arrMsg = new Array();");
		strData.append("arrMsg[0]='" + responseType + "';");
		strData.append("arrMsg[1]='" + responseData + "';");

		response.getWriter().println(strData.toString());
	}

	/**
	 * 
	 * @param ex
	 * @return
	 */
	public static String getErrorMessage(Exception ex,String language) {
		String errMsg = null;

		if (ex instanceof XBEException) {
			XBEException xe = (XBEException) ex;
			errMsg = XBEConfig.getServerMessage(xe.getExceptionCode(), language, null);
		} else if (ex instanceof XBERuntimeException) {
			XBERuntimeException xre = (XBERuntimeException) ex;
			errMsg = XBEConfig.getServerMessage(xre.getExceptionCode(), language, null);
		} else if (ex instanceof ModuleException) {
			ModuleException me = (ModuleException) ex;
			while (me.getCause() != null && me.getCause() instanceof ModuleException) {
				me = (ModuleException) me.getCause();
			}
			if (me.getExceptionCode().equals(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR)) {
				String errorCode = "";
				String errorMsg = "";
				try {
					Map<String, String> errorMap = (Map<String, String>) me.getExceptionDetails();
					Iterator<String> itErrorMap = errorMap.keySet().iterator();
					if (itErrorMap.hasNext()) {
						errorCode = (String) itErrorMap.next();
						errorMsg = (String) errorMap.get(errorCode);
					}
					if (errorMsg.contains("Excessive refund Amount is atttempted")) {
						errMsg = XBEConfig.getServerMessage(WebConstants.REQ_PAYMENT_GATEWAY_ERROR + "13", language, errorMsg);
					} else if (errorMsg.equals(PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED)) {
						errMsg = XBEConfig.getServerMessage(
								PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED, language, errorMsg);
					} else {
						errMsg = XBEConfig.getServerMessage(WebConstants.REQ_PAYMENT_GATEWAY_ERROR + errorCode, language,
								errorMsg);
					}

					if (errMsg.equals("") || errMsg == null) {
						errMsg = XBEConfig.getServerMessage(WebConstants.REQ_PAYMENT_GATEWAY_ERROR_DEFAULT, language, errorMsg);
					}
				} catch (Exception ex1) {

					errMsg = XBEConfig.getServerMessage(WebConstants.REQ_PAYMENT_GATEWAY_ERROR_DEFAULT, language, null);

				}
			} else if (me.getExceptionCode().equals(ResponseCodes.DUPLICATE_NAMES_IN_FLIGHT_SEGMENT)) {
				errMsg = XBEConfig.getServerMessage(WebConstants.DUP_NAMES_IN_FLIGHT_SEGMENT, language, null);
			} else if (me.getExceptionCode().equals(WebConstants.REQ_LOAD_REP_AGENT_PNR_ERROR)) {
				errMsg = XBEConfig.getServerMessage(WebConstants.KEY_UNAUTHORIZED_OPERATION, language, null);
			}else {
				errMsg = me.getMessageString();
			}
		} else if (ex instanceof ModuleRuntimeException) {
			ModuleRuntimeException mre = (ModuleRuntimeException) ex;
			errMsg = mre.getMessageString();
		} else {
			errMsg = XBEConfig.getServerMessage(XBEConfig.DEFULT_SERVER_ERROR_CODE, language, null);
			log.error(errMsg, ex);
		}

		if (errMsg == null || "null".equals(errMsg)) {
			errMsg = XBEConfig.getServerMessage(XBEConfig.DEFULT_SERVER_ERROR_CODE, language, null);
		}

		if (ICommonsException.class.isAssignableFrom(ex.getClass()) && ((ICommonsException) ex).getMessageParameters() != null) {
			List<String> messageParameters = ((ICommonsException) ex).getMessageParameters();
			for (int i = 0; i < messageParameters.size(); i++) {
				errMsg = errMsg.replace("#" + (i + 1), messageParameters.get(i));
			}
		}

		errMsg = StringUtils.replace(errMsg, "'", "`");
		errMsg = StringUtils.replace(errMsg, "\"", "`");

		return errMsg;
	}

	/**
	 * 
	 * @param request
	 * @param privilegeId
	 */
	public static void checkPrivilege(HttpServletRequest request, String privilegeId) {

		if (!hasPrivilege(request, privilegeId)) {
			log.error("Unauthorized operation:" + request.getUserPrincipal().getName() + ":" + privilegeId);
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		}

	}

	public static boolean hasPrivilege(HttpServletRequest request, String privilegeId) {
		Map<String, String> mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges == null) {
			return false;
		}
		return (mapPrivileges.get(privilegeId) != null) ? true : false;
	}

	public static Collection<String> getPrivileges(HttpServletRequest request) {
		Map<String, String> mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		return mapPrivileges.keySet();
	}

	public static void setJS(HttpServletRequest request, String js) {
		request.setAttribute(WebConstants.REQ_JS_ARRAY, js);
	}

	public static void audit(HttpServletRequest request, String taskCode) throws ModuleException {
		AuditorBD auditorBD = ModuleServiceLocator.getAuditorBD();
		XBEModuleConfig config = XBEModuleUtils.getConfig();
		String strAudit = config.getAuditEnable();
		boolean blnAuditable = false;
		if (strAudit != null && strAudit.trim().equals("true")) {
			blnAuditable = true;
		}

		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);

		if (user != null && blnAuditable) {
			Audit audit = new Audit();
			String userId = user.getUserId();
			LinkedHashMap mapContents = new LinkedHashMap();

			audit.setUserId(userId);
			audit.setTimestamp(new Date());
			audit.setAppCode(WebConstants.APP_CODE);
			audit.setTaskCode(taskCode);

			auditorBD.audit(audit, mapContents);
		}

	}

	public static void audit(HttpServletRequest request, String taskCode, LinkedHashMap mapContents) throws ModuleException {
		AuditorBD auditorBD = ModuleServiceLocator.getAuditorBD();
		XBEModuleConfig config = XBEModuleUtils.getConfig();
		String strAudit = config.getAuditEnable();
		boolean blnAuditable = false;
		if (strAudit != null && strAudit.trim().equals("true")) {
			blnAuditable = true;
		}

		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);

		if (user != null && blnAuditable) {
			Audit audit = new Audit();
			String userId = user.getUserId();
			audit.setUserId(userId);
			audit.setTimestamp(new Date());
			audit.setAppCode(WebConstants.APP_CODE);
			audit.setTaskCode(taskCode);

			auditorBD.audit(audit, mapContents);
		}

	}

	public static void audit(HttpServletRequest request, String taskCode, Collection data) throws ModuleException {
		LinkedHashMap contents = new LinkedHashMap();
		StringBuilder codes = new StringBuilder();
		for (Iterator iter = data.iterator(); iter.hasNext();) {
			codes.append((String) iter.next()).append(",");
		}

		if (codes.length() < 1990) {
			contents.put("codes", codes);
			audit(request, taskCode, contents);
		} else {
			int codeLen = 0;
			while (codeLen <= codes.length()) {
				contents.put("codes", codes.substring(codeLen, codeLen + 1990));
				audit(request, taskCode, contents);
				codeLen += 1990;
			}
		}
	}

	/**
	 * gets all commands
	 * 
	 * @param request
	 * @return
	 */
	protected static Collection<String> getCommands(HttpServletRequest request) {
		String hdnCommnds = request.getParameter(WebConstants.PARAM_HDN_COMMANDS);

		String[] arrCommands = StringUtils.split(hdnCommnds, ",");
		Collection<String> colCommands = new ArrayList<String>();

		for (int i = 0; i < arrCommands.length; i++) {
			colCommands.add(arrCommands[i]);
		}

		return colCommands;
	}

	// Menaka
	// Setting and retrieving attributes in Request
	public static void setAttribInRequest(HttpServletRequest request, String attributeName, Object value) {
		request.setAttribute(attributeName, value);
	}

	public static Object getAttribInRequest(HttpServletRequest request, String attributeName) {
		Object objReturn = null;
		try {
			objReturn = request.getAttribute(attributeName);
		} catch (Exception e) {
		}
		return objReturn;
	}

	/**
	 * retrive the non secure url
	 * 
	 * @param request
	 * @return
	 */
	public static String getNonSecureUrl(HttpServletRequest request) {

		StringBuffer sbRedirect = new StringBuffer("");
		XBEModuleConfig config = XBEModuleUtils.getConfig();
		String strRedirect = null;
		String pageUrl = request.getRequestURI();
		String server = request.getServerName();

		sbRedirect.append(config.getNonSecureProtocol() + "://");
		String httpsPort = config.getPort();

		sbRedirect.append(server);

		if (httpsPort != null && !"".equals(httpsPort)) {
			sbRedirect.append(":" + httpsPort);
		}

		sbRedirect.append(pageUrl);
		strRedirect = sbRedirect.toString();
		return strRedirect;
	}

	/**
	 * retrive the secure url
	 * 
	 * @param request
	 * @return
	 */
	public static String getSecureUrl(HttpServletRequest request) {

		StringBuffer sbRedirect = new StringBuffer("");
		XBEModuleConfig config = XBEModuleUtils.getConfig();
		String strRedirect = null;
		String pageUrl = request.getRequestURI();
		String server = request.getServerName();

		String port = null;
		if (config.isUseSecureLogin()) {
			sbRedirect.append(config.getSecureProtocol() + "://");
			port = config.getSslPort();
		} else {
			sbRedirect.append(config.getNonSecureProtocol() + "://");
			port = config.getPort();
		}

		sbRedirect.append(server);

		if (port != null && !"".equals(port)) {
			sbRedirect.append(":" + port);
		}
		sbRedirect.append(pageUrl);
		strRedirect = sbRedirect.toString();

		return strRedirect;
	}

	public static String getSecureContextPath(HttpServletRequest request) {

		StringBuffer sbRedirect = new StringBuffer("");
		XBEModuleConfig config = XBEModuleUtils.getConfig();
		String server = request.getServerName();
		String port = config.getSslPort();
		String secureContextPath = null;

		if (!config.getPort().equals(config.getSslPort())) {
			sbRedirect.append(config.getSecureProtocol() + "://");
		} else {
			sbRedirect.append(config.getNonSecureProtocol() + "://");
		}

		sbRedirect.append(server);

		if (port != null && !"".equals(port)) {
			sbRedirect.append(":" + port);
		}

		sbRedirect.append(request.getContextPath());

		secureContextPath = sbRedirect.toString();

		return secureContextPath;

	}

	public static String getContextPath(HttpServletRequest request) {
		StringBuffer sbRedirect = new StringBuffer("");
		XBEModuleConfig config = XBEModuleUtils.getConfig();
		String server = request.getServerName();
		String port = config.getPort();
		String contextPath = null;

		sbRedirect.append(config.getNonSecureProtocol() + "://");

		sbRedirect.append(server);

		if (port != null && !"".equals(port)) {
			sbRedirect.append(":" + port);
		}

		sbRedirect.append(request.getContextPath());

		contextPath = sbRedirect.toString();

		return contextPath;
	}

	public static boolean isProtocolStatusEqual() {
		XBEModuleConfig config = XBEModuleUtils.getConfig();
		return config.getNonSecureProtocol().equals(config.getSecureProtocol());

	}

	/**
	 * Returns the searchable origin sales channels
	 * 
	 * @param request
	 * @param isListReservations TODO
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Integer> getSearchableOriginSalesChannels(HttpServletRequest request, boolean isListReservations) throws ModuleException {
		Collection<Integer> colSalesChannels = null;
		if (!JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_ALL_RES)) {
			colSalesChannels = new ArrayList<Integer>();

			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_XBE_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT);
			}
			if ((JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_IBE_RES))
					|| (isListReservations && JavaScriptGenerator.checkAccess(request,
							PriviledgeConstants.WEB_ORIGINATED_OWNED_PNR_SEARCH))) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.WEB);
			}
			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_HOLIDAY_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.AA_HOLIDAYS);
			}
			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_DNATA_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.DNATA);
			}

			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_INTERLINED_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.INTERLINED);
			}

			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.LCC);
			}
			
			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.LCC);
			}
			
			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_IOS_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.IOS);
			}

			if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_ANDROID_RES)) {
				colSalesChannels.add(ReservationInternalConstants.SalesChannel.ANDROID);
			}

			colSalesChannels.add(ReservationInternalConstants.SalesChannel.AM_PAY);

			if (colSalesChannels != null && colSalesChannels.size() == 0) {
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		}

		return colSalesChannels;
	}

	public static Collection<String> getUserCarrierCodes(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		if (user.getCarriers() == null || user.getCarriers().size() < 1) {
			throw new ModuleException("module.user.carrier");
		}
		return user.getCarriers();
	}

	public static boolean isUserTypeGSAHasAccess(HttpServletRequest request, String Privilege, String agentTypeCode) {
		if (hasPrivilege(request, Privilege) && (!agentTypeCode.equals(WebConstants.AGENT_TYPE_GSA))) {
			return true;
		} else {
			return false;
		}
	}

}
