package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency, countryCurrExchangeRate\\.currency" })
public class ApplyPaxCreditAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ApplyPaxCreditAction.class);

	private boolean success = true;

	private boolean blnApply = true;

	private String messageTxt;

	private String pnr;

	private String groupPNR;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private boolean modifyAnci = false;

	private boolean requote = false;
	
	private boolean csPnr = false;

	private CurrencyExchangeRate currencyExchangeRate;

	private CurrencyExchangeRate countryCurrExchangeRate;

	public String execute() throws ModuleException {
		String strSelectedCurrency = request.getParameter("selCurrency");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShopingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			PaymentSummaryTO oldPysummary = ReservationUtil.transformJsonPaymentSummary(request.getParameter("resPaySumm"));
			currencyExchangeRate = bookingShopingCart.getCurrencyExchangeRate();

			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);

			if (strSelectedCurrency == null) {
				strSelectedCurrency = oldPysummary.getSelectedCurrency();
			}

			if (strSelectedCurrency == null || strSelectedCurrency.equals("")) {
				strSelectedCurrency = AppSysParamsUtil.getBaseCurrency();
			}

			LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO = bookingShopingCart.getReservationAlterModesTO();
			boolean blnModify = true;
			boolean blnModifySegment = false;
			boolean isGroupPnr = false;

			if ((pnr == null || pnr.trim().equals("")) && (groupPNR == null || groupPNR.trim().equals(""))) {
				blnModify = false;
			} else if (lCCClientResAlterQueryModesTO != null) {
				isGroupPnr = ReservationUtil.isGroupPnr(groupPNR);
				if (isGroupPnr) {
					this.pnr = groupPNR;
				}
				blnModifySegment = true;
			} else if (requote) {
				blnModifySegment = true;
			}
			Object[] arrObj = new Object[3];

			if (this.blnApply) {
				arrObj = PaymentUtil.applyPaxCreditToPassenger(request.getParameter("paxPayments"),
						request.getParameter("selectedCredit"), request.getParameter("paxId"), blnModify, blnModifySegment);
			} else {
				arrObj = PaymentUtil.removePaxCreditToPassenger(request.getParameter("paxPayments"),
						request.getParameter("paxId"), blnModify, blnModifySegment);
			}
			passengerPayment = (Collection<PaymentPassengerTO>) arrObj[0];
			String pendingCommissions = CommonUtil.getPendingAgentCommissions(RequestHandlerUtil.getAgentCode(request), groupPNR);

			if ((blnModify && !blnModifySegment) || modifyAnci) {
				strSelectedCurrency = bookingShopingCart.getCurrencyExchangeRate().getCurrency().getCurrencyCode();
				if (modifyAnci) {
					paymentSummaryTO = ReservationUtil.createPaymentSummary(AppSysParamsUtil.getBaseCurrency(),
							AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
									new BigDecimal(oldPysummary.getBalanceToPay()), bookingShopingCart.getUsedPaxCredit(),
									((BigDecimal) arrObj[2]).negate())), oldPysummary.getHandlingCharges(), oldPysummary
									.getModificationCharges(), AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
									(BigDecimal) arrObj[1], bookingShopingCart.getUsedPaxCredit().negate())), oldPysummary
									.getTicketPrice(), oldPysummary.getTotalFare(), oldPysummary.getTotalTaxSurCharges(),
							strSelectedCurrency, oldPysummary.getTotalDiscount(), pendingCommissions, null, null);
				} else {
					paymentSummaryTO = ReservationUtil.createPaymentSummary(AppSysParamsUtil.getBaseCurrency(),
							AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
									new BigDecimal(oldPysummary.getBalanceToPay()), ((BigDecimal) arrObj[2]).negate())),
							oldPysummary.getHandlingCharges(), oldPysummary.getModificationCharges(), AccelAeroCalculator
									.formatAsDecimal(AccelAeroCalculator.add((BigDecimal) arrObj[1], bookingShopingCart
											.getUsedPaxCredit().negate())), oldPysummary.getTicketPrice(), oldPysummary
									.getTotalFare(), oldPysummary.getTotalTaxSurCharges(), strSelectedCurrency, oldPysummary
									.getTotalDiscount(), pendingCommissions, null, null);
				}

				if (!this.blnApply) {
					arrObj[2] = AccelAeroCalculator.add(bookingShopingCart.getUsedPaxCredit(), (BigDecimal) arrObj[2]);
				}
				bookingShopingCart.setUsedPaxCredit((BigDecimal) arrObj[2]);
			} else {
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				if (blnModifySegment) {
					if (this.blnApply) {
						bookingShopingCart.setUsedPaxCredit((BigDecimal) arrObj[2]);
						paymentSummaryTO = PaymentUtil.createPaymentSummary(bookingShopingCart, strSelectedCurrency,
								exchangeRateProxy, pendingCommissions, RequestHandlerUtil.getAgentCode(request));

					} else {
						strSelectedCurrency = bookingShopingCart.getCurrencyExchangeRate().getCurrency().getCurrencyCode();
						paymentSummaryTO = ReservationUtil.createPaymentSummary(
								AppSysParamsUtil.getBaseCurrency(),
								AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
										new BigDecimal(oldPysummary.getBalanceToPay()), ((BigDecimal) arrObj[2]).negate())),
								oldPysummary.getHandlingCharges(), oldPysummary.getModificationCharges(),
								AccelAeroCalculator.formatAsDecimal((BigDecimal) arrObj[1]), oldPysummary.getTicketPrice(),
								oldPysummary.getTotalFare(), oldPysummary.getTotalTaxSurCharges(), strSelectedCurrency,
								oldPysummary.getTotalDiscount(), pendingCommissions, null, null);
						arrObj[2] = AccelAeroCalculator.add(bookingShopingCart.getUsedPaxCredit(), (BigDecimal) arrObj[2]);
						bookingShopingCart.setUsedPaxCredit((BigDecimal) arrObj[2]);
					}

				} else {
					bookingShopingCart.setUsedPaxCredit((BigDecimal) arrObj[1]);
					paymentSummaryTO = PaymentUtil.createPaymentSummary(bookingShopingCart, strSelectedCurrency,
							exchangeRateProxy, pendingCommissions, RequestHandlerUtil.getAgentCode(request));
				}

			}
			
			if (this.countryCurrExchangeRate != null && this.paymentSummaryTO != null) {
				this.paymentSummaryTO.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
			}
			
			request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShopingCart);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the passengerPayment
	 */
	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	/**
	 * @return the paymentSummaryTO
	 */
	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public boolean isBlnApply() {
		return blnApply;
	}

	public void setBlnApply(boolean blnApply) {
		this.blnApply = blnApply;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	public void setRequote(boolean requote) {
		this.requote = requote;
	}

	public boolean isCsPnr() {
		return csPnr;
	}

	public void setCsPnr(boolean csPnr) {
		this.csPnr = csPnr;
	}

}
