package com.isa.thinair.xbe.core.web.action.travagent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;


import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.PRINT_MCO),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class PrintMCOAction extends BaseRequestResponseAwareAction {

	private Log log = LogFactory.getLog(PrintMCOAction.class);

	private String mcoNumber;

	public String execute() throws ModuleException {
		String forward = S2Constants.Result.SUCCESS;
		String mcoPrintDetails = "";
		try {
			BasicRH.checkPrivilege(request, PrivilegesKeys.MCOPrivilegesKeys.ALLOW_PRINT_MCO);
			User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);

			if (BasicRH.hasPrivilege(request, PrivilegesKeys.MCOPrivilegesKeys.ALLOW_SEARCH_ANY)) {
				mcoPrintDetails = ModuleServiceLocator.getChargeBD().getPrintMCOText(mcoNumber, null);
			} else if (BasicRH.hasPrivilege(request, PrivilegesKeys.MCOPrivilegesKeys.ALLOW_SEARCH_OWN)) {
				mcoPrintDetails = ModuleServiceLocator.getChargeBD().getPrintMCOText(mcoNumber, user.getAgentCode());
			} else {
				throw new ModuleException("airpricing.empty.mco.results");
			}

			request.setAttribute("mcoPrintDetails", mcoPrintDetails);

		} catch (ModuleException me) {
			forward = S2Constants.Result.ERROR;
			throw new ModuleException(me.getExceptionCode());
		}
		return forward;
	}

	public String getMcoNumber() {
		return mcoNumber;
	}

	public void setMcoNumber(String mcoNumber) {
		this.mcoNumber = mcoNumber;
	}


}
