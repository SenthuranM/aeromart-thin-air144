package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.config.Action;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.api.dto.v2.InfantPaxTO;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.util.PassengerDetailParsingUtil;

/**
 * The result is a direct JSP file. This is necessary to bypass save file dialog resulting in iframe fall back strategy
 * used in the jQuery frame plugin. It is possible to use the recommended method to get around this limitation if
 * struts2 json plugin is included in the libraries. However it is not compatible with the current version of struts 2
 * being used. So falling back to this. For more Info refer
 * com.isa.thinair.airadmin.core.web.v2.action.system.ShowUploadAction}
 * 
 * @author Thihara
 * 
 */
@Action(name = "PassengerDetailsParse")
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Jsp.Reservation.PASSENGER_DETAILS_UPLOAD_RESULT_JSP, value = S2Constants.Jsp.Reservation.PASSENGER_DETAILS_UPLOAD_RESULT_JSP)
public class PassengerDetailsParseAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(PassengerDetailsParseAction.class);

	/*
	 * File to be uploaded from the front end.
	 */
	private File csvPaxFile;

	private boolean success = true;

	/*
	 * List of adults and infants to be passed on to the front end. adultList contains AD and CH types while infantList
	 * contains IN type.
	 */
	private List<AdultPaxTO> adultList;
	private List<InfantPaxTO> infantList;

	/*
	 * Count of passenger types parsed from the file.
	 */
	private int adultCount;
	private int childCount;
	private int infantCount;

	/*
	 * If parsing fails the record number which failed.
	 */
	private int unparsableRecordCount;

	private String messageTxt;

	private PassengerDetailParsingUtil paxDetailsParser;

	public PassengerDetailsParseAction() {
		paxDetailsParser = new PassengerDetailParsingUtil();
	}

	@SuppressWarnings("unchecked")
	public String execute() {
		try {

			/*
			 * When loading a fresh JSP there's no way we can know if its a fresh load or not. So if the file is null we
			 * are setting the result to null and return. From the js all processing is done if result!=null || 'null'.
			 * File size validation, empty file submit validation have to be removed due to this reason.
			 */
			if (csvPaxFile == null) {
				request.setAttribute("paxDetailsFileUploadResultData", null);
				return S2Constants.Jsp.Reservation.PASSENGER_DETAILS_UPLOAD_RESULT_JSP;
			}

			/*
			 * Checking if user has the necessary privilege.
			 */
			if (!BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_CSV_DETAIL_UPLOAD)) {
				throw new SecurityException("You are not authorized to upload passenger details with a .csv file!");
			}

			/*
			 * Gets the internal MultiPartRequestWrapper and checks for file type. Currently checks for just one file
			 * hence the array index [0]. We need the request wrapper because the uploaded file name on server system is
			 * of extension .tmp
			 */
			MultiPartRequestWrapper requestWrapper = ((MultiPartRequestWrapper) ServletActionContext.getRequest());
			if (!FilenameUtils.isExtension(requestWrapper.getFileNames("csvPaxFile")[0], "csv")) {
				throw new IllegalArgumentException("File type must be of .csv type.");
			}

			Map<String, Object> resultMap = paxDetailsParser.parsePassengerDetails(csvPaxFile);
			if (resultMap.get(PassengerDetailParsingUtil.PARSE_FAILING_LINE) != null) {
				unparsableRecordCount = (Integer) resultMap.get(PassengerDetailParsingUtil.PARSE_FAILING_LINE);
				messageTxt = (String) resultMap.get(PassengerDetailParsingUtil.PARSE_FAILING_REASON);
				success = false;
			} else {
				adultList = (List<AdultPaxTO>) resultMap.get(PassengerDetailParsingUtil.ADULT_LIST);
				adultCount = adultList.size();

				List<AdultPaxTO> childList = (List<AdultPaxTO>) resultMap.get(PassengerDetailParsingUtil.CHILDREN_LIST);
				childCount = childList.size();

				infantList = (List<InfantPaxTO>) resultMap.get(PassengerDetailParsingUtil.INFANT_LIST);
				setInfantCount(infantList.size());

				// CH is also considered adults in the front end UI parsing.
				adultList.addAll(childList);
			}

		} catch (Exception e) {
			messageTxt = "Unable to parse the file. Reason : " + e.getMessage();
			handleException(e);
		}
		request.setAttribute("paxDetailsFileUploadResultData", getJSResultString());
		return S2Constants.Jsp.Reservation.PASSENGER_DETAILS_UPLOAD_RESULT_JSP;
	}

	private void handleException(Exception x) {
		success = false;
		log.error(messageTxt, x);
	}

	/**
	 * Converts the result into JSON. This was necessary to get pass the save file dialog in older browsers including
	 * IE.
	 * 
	 * @return Manually hacked together json result string.
	 */
	private String getJSResultString() {
		String resultString = "";
		resultString += "{\"success\":" + success;
		try {
			resultString += ",\"adultList\":" + JSONUtil.serialize(adultList);
			resultString += ",\"infantList\":" + JSONUtil.serialize(infantList);
		} catch (JSONException e) {
			log.debug("Error converting to JSON :", e);
		}

		resultString += ",\"adultCount\":" + adultCount;
		resultString += ",\"childCount\":" + childCount;
		resultString += ",\"infantCount\":" + infantCount;
		resultString += ",\"unparsableRecordCount\":" + unparsableRecordCount;
		resultString += ",\"messageText\":\"" + messageTxt + "\"}";
		return resultString;
	}

	public void setCsvPaxFile(File csvPaxFile) {
		this.csvPaxFile = csvPaxFile;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setAdultList(List<AdultPaxTO> adultList) {
		this.adultList = adultList;
	}

	public List<AdultPaxTO> getAdultList() {
		return adultList;
	}

	public void setInfantList(List<InfantPaxTO> infantList) {
		this.infantList = infantList;
	}

	public List<InfantPaxTO> getInfantList() {
		return infantList;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setUnparsableRecordCount(int unparsableRecordCount) {
		this.unparsableRecordCount = unparsableRecordCount;
	}

	public int getUnparsableRecordCount() {
		return unparsableRecordCount;
	}
}
