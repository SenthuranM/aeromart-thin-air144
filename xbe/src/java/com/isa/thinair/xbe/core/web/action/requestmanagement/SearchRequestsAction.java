/**
 * 
 */
package com.isa.thinair.xbe.core.web.action.requestmanagement;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.alerting.api.dto.QueueRequestDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.dto.RequestHistoryDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author suneth
 *
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class SearchRequestsAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(SearchRequestsAction.class);
	
	private String queueId;
	
	private String applicablePageID;
	
	private ArrayList<Map<String, Object>> requestsList;
	
	private ArrayList<Map<String, Object>> requestHistoryList;
	
	private int page;

	private int records;

	private int total;

	private int requestId;
	
	private String status;
	
	private static String sidx;
	
	private static String sord;
	
	private String fromDate;
	
	private String toDate;
	
	private String pnr;
	
	private static boolean lastTenRequests;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String execute(){
		
		String success = S2Constants.Result.SUCCESS;
		try{
			
			QueueRequestDetailsDTO searchCriteria = new QueueRequestDetailsDTO();
			searchCriteria.setUserId(request.getUserPrincipal().getName());
			searchCriteria.setQueueId(queueId);
			searchCriteria.setApplicablePageID(applicablePageID);
			searchCriteria.setFromDate(fromDate);
			searchCriteria.setToDate(toDate);
			searchCriteria.setPnr(pnr);
			searchCriteria.setStatusCode(status);
			
			int start = (page - 1) * 20;
			Page requestsPage = ModuleServiceLocator.getAirproxyAlertingBD().retrieveSubmittedRequests(searchCriteria);
			setRequestsList(getQueueRequestDTOList((ArrayList<QueueRequestDTO>) requestsPage.getPageData() , start , 20));
			this.records = requestsPage.getTotalNoOfRecords();
			this.total = requestsPage.getTotalNoOfRecords() / 20;
			int mod = requestsPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			
		}catch (ModuleException e) {
			log.error("SearchRequestsAction ==> ", e);
		}
		
		
		return success;
	}
	
	
	/**
	 * This will create a list of queueReqeustDTO mapped with string name of the 'queueRequestDTO'
	 * 
	 * @param requestList list of queueReqeustDTOs
	 * @return  list of queueReqeustDTO mapped with string name of the 'queueRequestDTO'
	 */
	public static ArrayList<Map<String, Object>> getQueueRequestDTOList(List<QueueRequestDTO> requestList , int start , int pageSize) {
		
		int index = 0;
		ArrayList<Map<String, Object>> queueRequestDTOList = new ArrayList<Map<String, Object>>();
		QueueRequestDTO queueRequestDTO = null;
		int i = 0;
		
		if(lastTenRequests){
			requestList = requestList.subList(0, 10);
		}
		
		if(sidx.equals("createdDate")){
			requestList = sortRequestListByCreatedDate(requestList);
		}else if(sidx.equals("modifiedDate")){
			requestList = sortRequestListByModifiedDate(requestList);
		}
		
		if(sord.equals("desc") && (sidx.equals("createdDate") || sidx.equals("modifiedDate"))){
			Collections.reverse(requestList); 
		}
		
		Iterator<QueueRequestDTO> iterator = requestList.iterator();
		while (iterator.hasNext()) {
			queueRequestDTO = iterator.next();

			if(index >= start && index < (pageSize + start)){
				
				Map<String, Object> row = new HashMap<String, Object>();
				row.put("queueRequestDTO", queueRequestDTO);
				row.put("id", i++);
				queueRequestDTOList.add(row);
				
			}
			
			index++;
			
		}

		return queueRequestDTOList;
		
	}
	
	
	/**
	 * This is can be used to retrieve the History of a given request by Id.
	 * 
	 * @param 
	 * @return  list of queueReqeustDTO mapped with string name of the 'queueRequestDTO'
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	public String searchRequestHistory() {
		
		String success = S2Constants.Result.SUCCESS;
		
		try{
			
			int start = (page - 1) * 20;
			Page requestHistoryPage = ModuleServiceLocator.getAirproxyAlertingBD().getRequestHistory(requestId);
			setRequestHistoryList(getRequestHistoryDTOList((ArrayList<RequestHistoryDTO>) requestHistoryPage.getPageData() , start , 20));
			this.records = requestHistoryPage.getTotalNoOfRecords();
			this.total = requestHistoryPage.getTotalNoOfRecords() / 20;
			int mod = requestHistoryPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			
		}catch (ModuleException e) {
			log.error("SearchRequestsAction ==> ", e);
		}
		
		return success;
		
	}
	
	/**
	 * This will create a list of queueReqeustDTO mapped with string name of the 'queueRequestDTO'
	 * 
	 * @param requestList list of queueReqeustDTOs
	 * @return  list of queueReqeustDTO mapped with string name of the 'queueRequestDTO'
	 */
	public static ArrayList<Map<String, Object>> getRequestHistoryDTOList(List<RequestHistoryDTO> requestList , int start , int pageSize) {
		
		int index = 0;
		ArrayList<Map<String, Object>> requestHistoryDTOList = new ArrayList<Map<String, Object>>();
		RequestHistoryDTO requestHistoryDTO = null;
		int i = 0;
		Iterator<RequestHistoryDTO> iterator = requestList.iterator();
		while (iterator.hasNext()) {
			requestHistoryDTO = iterator.next();
			
			if(index >= start && index < (pageSize + start)){
				
		
				Map<String, Object> row = new HashMap<String, Object>();
				row.put("requestHistoryDTO", requestHistoryDTO);
				row.put("id", i++);
				requestHistoryDTOList.add(row);
				
			}
			
			index++;
				
		}

		return requestHistoryDTOList;
		
	}
	
	/**
	 * This is can be used to search request for the users who can do actions to requests.
	 * 
	 * @param 
	 * @return  list of queueReqeustDTO mapped with string name of the 'queueRequestDTO'
	 */
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String searchRequestsForActioning(){
		
		String success = S2Constants.Result.SUCCESS;
		try{
			
			QueueRequestDetailsDTO searchCriteria = new QueueRequestDetailsDTO();
			searchCriteria.setUserId(request.getUserPrincipal().getName());
			searchCriteria.setQueueId(queueId);
			searchCriteria.setStatusCode(status);
			
			int start = (page - 1) * 20;
			Page requestsPage = ModuleServiceLocator.getAirproxyAlertingBD().retrieveSubmittedRequestsForAction(request.getUserPrincipal().getName() , queueId, status);
			setRequestsList(getQueueRequestDTOList((ArrayList<QueueRequestDTO>) requestsPage.getPageData() , start , 20));
			this.records = requestsPage.getTotalNoOfRecords();
			this.total = requestsPage.getTotalNoOfRecords() / 20;
			int mod = requestsPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			
		}catch (ModuleException e) {
			log.error("SearchRequestsAction ==> ", e);
		}
		
		
		return success;
	}
	
	// sort the request list according to the request created date
	private static List<QueueRequestDTO> sortRequestListByCreatedDate(List<QueueRequestDTO> requestList){
		
		Collections.sort(requestList, new Comparator<QueueRequestDTO>() {
			@Override
			public int compare(QueueRequestDTO o1, QueueRequestDTO o2) {
				return o1.getCreatedDate().compareTo(o2.getCreatedDate());
			}
		});
		
		return requestList;
	}

	// sort the request list according to the request modified date
	private static List<QueueRequestDTO> sortRequestListByModifiedDate(List<QueueRequestDTO> requestList){
		
		Collections.sort(requestList, new Comparator<QueueRequestDTO>() {
			@Override
			public int compare(QueueRequestDTO o1, QueueRequestDTO o2) {
				return o1.getCreatedDate().compareTo(o2.getCreatedDate());
			}
		});
		
		return requestList;
	}
	/**
	 * @return the queueType
	 */
	public String getQueueId() {
		return queueId;
	}

	/**
	 * @param queueType the queueType to set
	 */
	public void setQueueId(String queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the applicablePageID
	 */
	public String getApplicablePageID() {
		return applicablePageID;
	}

	/**
	 * @param applicablePageID the applicablePageID to set
	 */
	public void setApplicablePageID(String applicablePageID) {
		this.applicablePageID = applicablePageID;
	}

	/**
	 * @return the requestsList
	 */
	public ArrayList<Map<String, Object>> getRequestsList() {
		return requestsList;
	}

	/**
	 * @param requestsList the requestsList to set
	 */
	public void setRequestsList(ArrayList<Map<String, Object>> requestsList) {
		this.requestsList = requestsList;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the requestHistoryList
	 */
	public ArrayList<Map<String, Object>> getRequestHistoryList() {
		return requestHistoryList;
	}

	/**
	 * @param requestHistoryList the requestHistoryList to set
	 */
	public void setRequestHistoryList(
			ArrayList<Map<String, Object>> requestHistoryList) {
		this.requestHistoryList = requestHistoryList;
	}

	/**
	 * @return the requestId
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param sidx the sidx to set
	 */
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	/**
	 * @param sord the sord to set
	 */
	public void setSord(String sord) {
		this.sord = sord;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @param lastTenRequests the lastTenRequests to set
	 */
	public void setLastTenRequests(boolean lastTenRequests) {
		this.lastTenRequests = lastTenRequests;
	}
			
}
