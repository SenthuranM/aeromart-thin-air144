package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.ForceConfirmedPayStatus;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * 
 * @author rajitha
 *
 */
public class ForceConfirmedBookingReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(ForceConfirmedBookingReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execue(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		try {
			setDisplayData(request);
		} catch (ModuleException ex) {
			log.error("ERROR :: ForceConfirmedBookingReport ", ex.getCause());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
			}
		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ERROR :: ForceConfirmedBookingReport " + ex.getMessage());
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setOriginAndDestinations(request);
		setPayStatus(request);
		setReportingPeriod(request);
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setOriginAndDestinations(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	private static void setPayStatus(HttpServletRequest request) throws ModuleException {

		String strHtml = SelectListGenerator.createForceConfirmedPayStatuTypes();
		request.setAttribute(WebConstants.REQ_FORCE_CONFIRMED_PAY_TYPES, strHtml);
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		ReportsSearchCriteria reportsSearchCriteria = new ReportsSearchCriteria();

		String bookingFromDate = request.getParameter("txtBkngFromDate");
		String bookingToDate = request.getParameter("txtBkngToDate");
		String value = request.getParameter("radReportOption");

		reportsSearchCriteria.setDateRangeFrom(ReportsHTMLGenerator.convertDate(bookingFromDate));
		reportsSearchCriteria.setDateRangeTo(ReportsHTMLGenerator.convertDate(bookingToDate));

		if (!StringUtil.isNullOrEmpty(request.getParameter("txtDeptFromDate"))
				&& !StringUtil.isNullOrEmpty(request.getParameter("txtDeptToDate"))) {

			String departureFromDate = request.getParameter("txtDeptFromDate");
			String departureToDate = request.getParameter("txtDeptToDate");
			reportsSearchCriteria.setDateRange2From(ReportsHTMLGenerator.convertDate(departureFromDate));
			reportsSearchCriteria.setDateRange2To(ReportsHTMLGenerator.convertDate(departureToDate));
		}

		if (!StringUtil.isNullOrEmpty(request.getParameter("selDeparture"))
				&& !StringUtil.isNullOrEmpty(request.getParameter("selArrival"))) {

			String from = request.getParameter("selDeparture");
			String to = request.getParameter("selArrival");
			reportsSearchCriteria.setFrom(from);
			reportsSearchCriteria.setTo(to);
		}

		if (!StringUtil.isNullOrEmpty(request.getParameter("payStatus"))) {
			String payStatus = request.getParameter("payStatus");
			reportsSearchCriteria.setOnlyTotalAmountToPay(payStatus.equals(ForceConfirmedPayStatus.NOT_PAID.getStatus()));
			reportsSearchCriteria.setIncludePaidStatusFilter(true);
		}

		try {

			String reportTemplate = "ForceConfirmedPNRReport.jasper";
			Map<String, Object> parameters = new HashMap<String, Object>();
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			parameters.put("FROM_DATE", bookingFromDate);
			parameters.put("TO_DATE", bookingToDate);
			parameters.put("CURRENCY", AppSysParamsUtil.getBaseCurrency());
			String locale = null;
			Object localeObject = request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
			if (localeObject == null) {
				locale = WebConstants.DEFAULT_LANGUAGE;
			} else {
				locale = localeObject.toString();
			}
			parameters.put("language", locale);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getForceConfirmedBookingReport(reportsSearchCriteria);

			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("IMG", reportsRootDir);
			String reportName = "ForceConfirmBookingReport";

			if (value.trim().equals("HTML")) {
				response.reset();
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (value.trim().equals("PDF")) {
				response.reset();
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("CSV")) {
				response.reset();
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}

		} catch (Exception ex) {
			log.error("ERROR :: Error genrating Force Confirm Booking Report " + ex.getCause());
		}

	}
	
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
			request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

}
