/**
 * 
 */
package com.isa.thinair.xbe.core.web.handler.customer;


import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author suneth
 *
 */
public class AgentFrequentCustomerHandler extends BasicRH{

	public static String execute(HttpServletRequest request){
		
		String forward = S2Constants.Result.SUCCESS;
		
		try {
			setDisplayData(request);
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
			e.printStackTrace();
		}
		
		return forward;
		
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException{
		setNationalityList(request);
	}
	
	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setNationalityList(HttpServletRequest request) throws ModuleException {
		String strNationality = SelectListGenerator.createNationalityList();
		request.setAttribute(WebConstants.REQ_HTML_NATIONALITY_LIST, strNationality);
	}

}
