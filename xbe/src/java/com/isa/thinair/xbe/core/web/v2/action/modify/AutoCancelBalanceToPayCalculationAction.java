package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class to calculate balance to pay amount if auto cancellation executes on the given PNR
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AutoCancelBalanceToPayCalculationAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(AutoCancelBalanceToPayCalculationAction.class);

	private boolean success = true;

	private String messageTxt;

	private String pnr;

	private String groupPNR;

	private String version;

	private String resSegments;

	private String resPax;

	private String strBalanceToPay;

	public String execute() {
		String strForward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);

		try {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(resSegments);
			Collection<LCCClientReservationSegment> autoCancelingSegments = new ArrayList<LCCClientReservationSegment>();
			for (LCCClientReservationSegment lccClientReservationSegment : colsegs) {
				if (lccClientReservationSegment.isAlertAutoCancellation()) {
					autoCancelingSegments.add(lccClientReservationSegment);
				}
			}

			Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPax);
			List<String> paxInfantList = new ArrayList<String>();
			for (LCCClientReservationPax resPax : colpaxs) {
				if (resPax.isAlertAutoCancellation() && PaxTypeTO.INFANT.equals(resPax.getPaxType())) {
					paxInfantList.add(resPax.getTravelerRefNumber());
				}
			}

			LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO.composeCancelSegmentRequest(pnr,
					autoCancelingSegments, colsegs, null, null, null, version, null);
			lccClientResAlterModesTO.setGroupPnr(isGroupPNR);
			lccClientResAlterModesTO.setPaxInfantList(paxInfantList);

			BigDecimal balance = ModuleServiceLocator.getAirproxyReservationBD().calculateBalanceToPayAfterAutoCancellation(
					lccClientResAlterModesTO, getTrackInfo());

			strBalanceToPay = (balance == null) ? "0.00" : balance.toPlainString();
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}

		return strForward;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setResSegments(String resSegments) {
		this.resSegments = resSegments;
	}

	public void setResPax(String resPax) {
		this.resPax = resPax;
	}

	public String getStrBalanceToPay() {
		return strBalanceToPay;
	}

}
