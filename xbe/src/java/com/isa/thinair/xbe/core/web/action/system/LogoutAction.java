package com.isa.thinair.xbe.core.web.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.system.LogoutRH;

@Namespace(S2Constants.Namespace.PUBLIC)
@Result(name = S2Constants.Result.GET_XBE_DATA, value = S2Constants.Jsp.Search.GET_XBE_DATA)
public class LogoutAction extends BaseRequestAwareAction {

	public String execute() {
		return LogoutRH.execute(request);
	}
}
