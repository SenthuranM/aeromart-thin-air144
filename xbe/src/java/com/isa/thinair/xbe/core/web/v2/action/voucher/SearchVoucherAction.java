package com.isa.thinair.xbe.core.web.v2.action.voucher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.to.VoucherSearchRequest;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class SearchVoucherAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(SearchVoucherAction.class);

	private VoucherSearchRequest voucherSearchRequest;

	private int pageSize = 20;

	private int page;

	private int records;

	private int total;

	private ArrayList<Map<String, Object>> voucherList;

	public String execute() {
		String success = S2Constants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();

			try {
				int start = (page - 1) * 20;
				Page<VoucherDTO> voucherPage = voucherDelegate.searchVoucher(start, pageSize, voucherSearchRequest,
						PromotionCriteriaConstants.VoucherType.VOUCHER);
				setVoucherList(getVoucherDTOList((List<VoucherDTO>) voucherPage.getPageData()));
				this.setRecords(voucherPage.getTotalNoOfRecords());
				this.total = voucherPage.getTotalNoOfRecordsInSystem() / 20;
				int mod = voucherPage.getTotalNoOfRecordsInSystem() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}
			} catch (ModuleException e) {
				log.error("SearchCustomerAction ==> ", e);
			}
		}
		return success;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the voucherList
	 */
	public ArrayList<Map<String, Object>> getVoucherList() {
		return voucherList;
	}

	/**
	 * @param voucherList
	 *            the voucherList to set
	 */
	public void setVoucherList(ArrayList<Map<String, Object>> voucherList) {
		this.voucherList = voucherList;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	public static ArrayList<Map<String, Object>> getVoucherDTOList(List<VoucherDTO> voucherList) {
		ArrayList<Map<String, Object>> voucherDTOList = new ArrayList<Map<String, Object>>();
		VoucherDTO voucherDTO = null;
		int i = 0;
		Iterator<VoucherDTO> iterator = voucherList.iterator();
		while (iterator.hasNext()) {
			voucherDTO = iterator.next();
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("voucherDTO", voucherDTO);
			row.put("id", i++);
			voucherDTOList.add(row);
		}

		return voucherDTOList;
	}

	public VoucherSearchRequest getVoucherSearchRequest() {
		return voucherSearchRequest;
	}

	public void setVoucherSearchRequest(VoucherSearchRequest voucherSearchRequest) {
		this.voucherSearchRequest = voucherSearchRequest;
	}

}

