package com.isa.thinair.xbe.core.web.generator.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.v2.AreaCodeTO;
import com.isa.thinair.xbe.api.dto.v2.CountryPhoneTO;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class JSONGenerator {
	private static String EMPTY_ARRAY = "[]";
	private static String EMPTY_OBJECT = "{}";

	public static String createCreditCardTypesForAllPGW() {
		try {
			return JSONUtil.serialize(SelectListGenerator.createCreditCardTypesForAllPGW());
		} catch (ModuleException e) {
			return EMPTY_OBJECT;
		} catch (JSONException e) {
			return EMPTY_OBJECT;
		}
	}

	public static String createActualPaymentMethodList() {
		try {
			return JSONUtil.serialize(SelectListGenerator.createActualPaymentMethodList());
		} catch (ModuleException e) {
			return EMPTY_OBJECT;
		} catch (JSONException e) {
			return EMPTY_OBJECT;
		}
	}

	public static String createCountryPhoneList() {
		Collection<CountryPhoneTO> colPhone = new ArrayList<CountryPhoneTO>();
		Collection<String[]> countryList = SelectListGenerator.getCountryTeles();
		Collection<AreaCodeTO> areaList = new ArrayList<AreaCodeTO>();
		CountryPhoneTO countryPhone = null;
		String countryCode = "";
		for (String[] country : countryList) {
			if (country[7].equals(countryCode)) {
				if (country[4] != null) {
					AreaCodeTO areacode = new AreaCodeTO();
					areacode.setAreaCode(country[4]);
					areacode.setType(country[5]);
					areacode.setStatus(country[6]);
					areaList.add(areacode);
				}
			} else {
				if (countryPhone != null) {
					countryPhone.setAreaCodes(areaList);
					colPhone.add(countryPhone);
				}
				countryPhone = new CountryPhoneTO();
				areaList = new ArrayList<AreaCodeTO>();
				countryCode = country[7];
				countryPhone.setCountryCode(country[7]);
				countryPhone.setCountryPhoneCode(country[0]);
				countryPhone.setMaxlength(country[3]);
				countryPhone.setMinLength(country[2]);
				countryPhone.setValidateAreaCode((country[1].equals("Y")));
				if (country[4] != null) {
					AreaCodeTO areacode = new AreaCodeTO();
					areacode.setAreaCode(country[4]);
					areacode.setType(country[5]);
					areacode.setStatus(country[6]);
					areaList.add(areacode);
				}
			}
		}
		try {
			return JSONUtil.serialize(colPhone);
		} catch (JSONException e) {
			return EMPTY_ARRAY;
		}
	}

	public static String createCarrierAirlineCodeMap(Map<String, String> carrierAirlineCodeMap) {
		try {
			return JSONUtil.serialize(carrierAirlineCodeMap);
		} catch (JSONException je) {
			return EMPTY_OBJECT;
		}
	}

	public static String createCabinClassRankingMap(Map<String, Integer> cabinClassRankingMap) {
		try {
			return JSONUtil.serialize(cabinClassRankingMap);
		} catch (JSONException e) {
			return EMPTY_OBJECT;
		}
	}
}
