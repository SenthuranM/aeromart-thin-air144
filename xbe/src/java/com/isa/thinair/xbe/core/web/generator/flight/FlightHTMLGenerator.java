package com.isa.thinair.xbe.core.web.generator.flight;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentApplicableOND;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.util.FlightUtils;

/**
 * @author Thushara
 * 
 */
public class FlightHTMLGenerator {
	private static Log log = LogFactory.getLog(FlightHTMLGenerator.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static final String defaultFormat // get the default date format
	= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);
	// get the default schedule status to search
	private String dayOffSet = globalConfig.getBizParam(SystemParamKeys.OFFSET_FROM_THE_STANDED_FIRST_DAY_OF_WEEK);
	private static String clientErrors;

	/**
	 * Gets the Flight Grid Data Row
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Flight Grid Rows
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getFlightRowHtml(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		String strMode = request.getParameter("hdnMode");
		
		Page page = null;
		int recNo = 1;
		int totRec = 0;
		Collection flightCol = null;
		Collection territoryCollection = new ArrayList<String>();
		List<String> applicableOnds = null;
		boolean localtime = true;
		boolean isFlightManifest = true;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (request.getParameter("hdnRecNo") == null) {
			recNo = 1;
		} else if (!("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}
		recNo = recNo - 1;

		String strStartDate = null;
		String strStopDate = null;
		String strFlightNo = null;
		String strFlightStartNo = null;
		String strFrom = null;
		String strTo = null;
		String strOperationtype = null;
		String strStatus = null;
		String strCheckinStatus = null;
		String strStartTime = null;
		String strEndTime = null;
		String strSunday = null;
		String strMonday = null;
		String strTuesday = null;
		String strWednesday = null;
		String strThursday = null;
		String strFriday = null;
		String strSaturday = null;
		String flightSearchCarrierCode = null;

		boolean viewAnyMF = false;
		boolean isInterline = false;

		// Default search values
		Date dStartDate = null;
		Date dStopDate = null;
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		String strDefaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		// get the default operation type to search
		String defaultOperationType = globalConfig.getBizParam(SystemParamKeys.DEFAULT_OPERATION_TYPE);
		// get the default schedule status to search
		String defaultStatus = globalConfig.getBizParam(SystemParamKeys.DEFAULT_FLIGHT_STATUS);
		// get the default minimum time allowed for amendments
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_OPERATIONTYPE, defaultOperationType);
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_FLIGHTSTATUS, defaultStatus);
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_CRRIERCODE, strDefaultCarrierCode);
		request.setAttribute(WebConstants.REQ_DAY_OFFSET, dayOffSet);

		if (strMode != null && (strMode.equals("SEARCH") || strMode.equals("LOAD") || strMode.equals("CHECKIN"))) {

			if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_VIEW_ANY_FLIGHT_MANIFEST)) {
				viewAnyMF = true;
			}

			// Called from flight - search button click
			strStartDate = request.getParameter("txtStartDateSearch");
			strStopDate = request.getParameter("txtStopDateSearch");
			strFlightNo = request.getParameter("txtFlightNoSearch");
			strFlightStartNo = request.getParameter("txtFlightNoStartSearch");
			strOperationtype = request.getParameter("selOperationTypeSearch");
			strFrom = request.getParameter("selFromStn6");
			strTo = request.getParameter("selToStn6");
			strStatus = request.getParameter("selStatusSearch");
			strCheckinStatus = request.getParameter("selCheckinStatusSearch");
			strStartTime = request.getParameter("txtStartTime");
			strEndTime = request.getParameter("txtEndTime");
			strSunday = request.getParameter("chkSunday");
			strMonday = request.getParameter("chkMonday");
			strTuesday = request.getParameter("chkTuesday");
			strWednesday = request.getParameter("chkWednesday");
			strThursday = request.getParameter("chkThursday");
			strFriday = request.getParameter("chkFriday");
			strSaturday = request.getParameter("chkSaturday");

			if (strFlightStartNo != null && strFlightStartNo.length() > 0) {
				flightSearchCarrierCode = strFlightStartNo.trim();
				if (flightSearchCarrierCode.equals(strDefaultCarrierCode)) {
					isInterline = false;
				} else {
					Set<String> interlineCarriers = getInterlineCarriers();
					if (AppSysParamsUtil.isLCCConnectivityEnabled() && interlineCarriers.contains(flightSearchCarrierCode)) {
						isInterline = true;
					} else {
						isInterline = false;
					}
				}
			}

			if (!viewAnyMF && !isInterline) {

				Agent ag = ModuleServiceLocator.getTravelAgentBD().getAgent(ModuleServiceLocator.getSecurityBD()
						.getUserBasicDetails(request.getUserPrincipal().getName()).getAgentCode());

				if (ag != null) {

					if (AgentType.GSA.equals(ag.getAgentTypeCode())) {
						territoryCollection = ModuleServiceLocator.getLocationServiceBD().getStationDetails(
								ag.getTerritoryCode(), true);
					} else {
						territoryCollection = ModuleServiceLocator.getLocationServiceBD().getStationDetails(ag.getStationCode(),
								false);
					}
					
					if (AppSysParamsUtil.isEnableRouteSelectionForAgents()
							&& AppSysParamsUtil.isEnableFlightSelectionInManifestForAgentAllowedRoutes()) {
						List<String> applicableRoutes = ag.getApplicableOndList().stream().map(AgentApplicableOND::getOndCOde)
								.collect(Collectors.toList());
						applicableOnds = applicableRoutes.stream()
								.map(ond -> ond.substring(0, 3) + ond.substring(ond.length() - 4)).collect(Collectors.toList());
						
					}
				}

			}

			try {

				dStartDate = formatter.parse(strStartDate);
				dStopDate = formatter.parse(strStopDate);
				Calendar cal = Calendar.getInstance();
				cal.setTime(dStartDate);
				if (strStartTime != null && !strStartTime.trim().equals("")) {
					String[] stAr = strStartTime.split(":");
					int intMins = new Integer(stAr[1]).intValue();
					int inthours = new Integer(stAr[0]).intValue();
					cal.add(Calendar.MINUTE, intMins);
					cal.add(Calendar.HOUR, inthours);
				} else {
					cal.add(Calendar.MINUTE, 0);
					cal.add(Calendar.HOUR, 0);
				}
				dStartDate = cal.getTime(); // Set Start date
				cal.setTime(dStopDate);
				if (strEndTime != null && !strEndTime.trim().equals("")) {
					String[] edAr = strEndTime.split(":");
					int intMins = new Integer(edAr[1]).intValue();
					int inthours = new Integer(edAr[0]).intValue();
					cal.add(Calendar.MINUTE, intMins);
					cal.add(Calendar.HOUR, inthours);
				} else if (strMode.equals("CHECKIN")) {
					cal.add(Calendar.MINUTE, 59);
					cal.add(Calendar.HOUR, 23);
				}

				dStopDate = cal.getTime(); // Set Stop date

			} catch (Exception e) {
				log.error("Error in getFlightRowHtml(HttpServletRequest request)" + " - Fromat Setted Date" + e.getMessage());
			}
			if (strFlightNo != null) {
				strFlightNo = strFlightNo.trim();
			}

		}

		// setting the criteria list to search
		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();

		if (strMode != null && (strMode.equals("SEARCH") || strMode.equals("LOAD"))) {
			// setting the start date as criteria
			if (dStartDate != null) {
				ModuleCriterion moduleCriterionStartD = new ModuleCriterion();
				moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
				List<Date> valueStartDate = new ArrayList<Date>();
				valueStartDate.add(dStartDate);
				moduleCriterionStartD.setValue(valueStartDate);
				critrian.add(moduleCriterionStartD);
			}

			// setting the stop date as criteria
			if (dStopDate != null) {
				ModuleCriterion moduleCriterionStopD = new ModuleCriterion();
				moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
				List<Date> valueStopDate = new ArrayList<Date>();
				valueStopDate.add(dStopDate);
				moduleCriterionStopD.setValue(valueStopDate);
				critrian.add(moduleCriterionStopD);
			}

			// setting the operation type as criteria
			if (strOperationtype != null && !strOperationtype.trim().equals("All")) {
				ModuleCriterion moduleCriterionOptType = new ModuleCriterion();
				moduleCriterionOptType.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionOptType.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.OPERATION_TYPE_ID);
				List<Integer> valueOptType = new ArrayList<Integer>();
				valueOptType.add(new Integer(strOperationtype));
				moduleCriterionOptType.setValue(valueOptType);
				critrian.add(moduleCriterionOptType);
			}

			// setting the flight status as criteria
			if (strStatus != null && !strStatus.trim().equals("All")) {
				ModuleCriterion moduleCriterionStatus = new ModuleCriterion();
				moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.STATUS);
				List<String> valueStatus = new ArrayList<String>();
				valueStatus.add(strStatus);
				moduleCriterionStatus.setValue(valueStatus);
				critrian.add(moduleCriterionStatus);
			}

			// setting the flight checkin status as criteria
			if (strCheckinStatus != null && !strCheckinStatus.trim().equals("All")) {
				ModuleCriterion moduleCriterionStatus = new ModuleCriterion();
				moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.CHECKIN_STATUS);
				List<String> valueStatus = new ArrayList<String>();
				valueStatus.add(strCheckinStatus);
				moduleCriterionStatus.setValue(valueStatus);
				critrian.add(moduleCriterionStatus);
			}

			// setting the flight number as criteria
			if (strFlightNo != null && !strFlightNo.trim().equals("")) {
				ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();
				moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER);
				List<String> valueFlightNo = new ArrayList<String>();
				String flightNumber = strFlightStartNo + strFlightNo;
				valueFlightNo.add(flightNumber != null ? flightNumber.toUpperCase() : "");
				moduleCriterionFlightNo.setValue(valueFlightNo);
				critrian.add(moduleCriterionFlightNo);
			}

			// setting the departure airport code as criteria
			if (strFrom != null && !strFrom.trim().equals("")) {
				ModuleCriterion moduleCriterionFromStn = new ModuleCriterion();
				moduleCriterionFromStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionFromStn.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE);
				List<String> valueFromStn = new ArrayList<String>();
				valueFromStn.add(strFrom);
				moduleCriterionFromStn.setValue(valueFromStn);
				critrian.add(moduleCriterionFromStn);
			}

			// setting the arrival airport code as criteria
			if (strTo != null && !strTo.trim().equals("")) {
				ModuleCriterion moduleCriterionToStn = new ModuleCriterion();
				moduleCriterionToStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
				// arrival airport code of the schedule
				moduleCriterionToStn.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE);
				List<String> valueToStn = new ArrayList<String>();
				valueToStn.add(strTo);
				moduleCriterionToStn.setValue(valueToStn);
				critrian.add(moduleCriterionToStn);
			}

			// AARESAA-1950
			if (!viewAnyMF && !isInterline) {

				if (territoryCollection != null && !territoryCollection.isEmpty()) {

					ModuleCriterion moduleCriterionTDFilter = new ModuleCriterion();
					moduleCriterionTDFilter.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE);
					moduleCriterionTDFilter.setCondition(ModuleCriterion.CONDITION_IN);
					moduleCriterionTDFilter.setValue(new ArrayList(territoryCollection));
					critrian.add(moduleCriterionTDFilter);

					BasicRH.saveMessage(request, XBEConfig.getServerMessage("common.flightmanifest.nopriv", userLanguage, null),
							BasicRH.MSG_TYPE_ERROR);
				}
			}

			Collection<DayOfWeek> daysCol = new ArrayList<DayOfWeek>();
			if (strSunday != null && strSunday.equals(WebConstants.VAL_CHECKBOX_TRUE)) {
				daysCol.add(DayOfWeek.SUNDAY);
			}
			if (strMonday != null && strMonday.equals(WebConstants.VAL_CHECKBOX_TRUE)) {
				daysCol.add(DayOfWeek.MONDAY);
			}

			if (strTuesday != null && strTuesday.equals(WebConstants.VAL_CHECKBOX_TRUE)) {
				daysCol.add(DayOfWeek.TUESDAY);
			}

			if (strWednesday != null && strWednesday.equals(WebConstants.VAL_CHECKBOX_TRUE)) {
				daysCol.add(DayOfWeek.WEDNESDAY);
			}

			if (strThursday != null && strThursday.equals(WebConstants.VAL_CHECKBOX_TRUE)) {
				daysCol.add(DayOfWeek.THURSDAY);
			}

			if (strFriday != null && strFriday.equals(WebConstants.VAL_CHECKBOX_TRUE)) {
				daysCol.add(DayOfWeek.FRIDAY);
			}

			if (strSaturday != null && strSaturday.equals(WebConstants.VAL_CHECKBOX_TRUE)) {
				daysCol.add(DayOfWeek.SATURDAY);
			}

			if (daysCol != null && daysCol.size() > 0) {
				ModuleCriterion moduleCriterionFrequency = new ModuleCriterion();
				moduleCriterionFrequency.setCondition(ModuleCriterion.CONDITION_IN);
				// arrival airport code of the schedule
				moduleCriterionFrequency.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DAY_NUMBER);

				List<Integer> valueFrequency = new ArrayList<Integer>();
				Iterator days = daysCol.iterator();
				while (days.hasNext()) {

					DayOfWeek day = (DayOfWeek) days.next();
					Integer dayNumber = (Integer) CalendarUtil.getDayNumber(day);
					if (dayNumber == 0) {
						valueFrequency.add(dayNumber);
					}
					if (dayNumber == 1) {
						valueFrequency.add(dayNumber);
					}
					if (dayNumber == 2) {
						valueFrequency.add(dayNumber);
					}
					if (dayNumber == 3) {
						valueFrequency.add(dayNumber);
					}
					if (dayNumber == 4) {
						valueFrequency.add(dayNumber);
					}
					if (dayNumber == 5) {
						valueFrequency.add(dayNumber);
					}
					if (dayNumber == 6) {
						valueFrequency.add(dayNumber);
					}

				}
				moduleCriterionFrequency.setValue(valueFrequency);
				critrian.add(moduleCriterionFrequency);
			}
		}

		if (strMode != null && (strMode.equals("SEARCH"))) {
			FlightManifestSearchDTO searchDTO = new FlightManifestSearchDTO();

			searchDTO.setCriteria(critrian);
			searchDTO.setStartIndex(recNo);
			searchDTO.setPageSize(20);
			searchDTO.setDatesInLocal(localtime);
			searchDTO.setFlightCriteria(null);
			searchDTO.setFlightManifest(isFlightManifest);
			searchDTO.setInterline(isInterline);
			searchDTO.setCarrierCode(flightSearchCarrierCode);
			searchDTO.setAgentApplicableOnds(applicableOnds);
			
			String dataSource = request.getParameter(WebConstants.REP_HDN_LIVE);
			if (dataSource != null) {
				if (dataSource.equals(WebConstants.REP_LIVE)) {
					searchDTO.setDataFromReporting(false);
				} else if (dataSource.equals(WebConstants.REP_OFFLINE)) {
					searchDTO.setDataFromReporting(true);
				}
			}
			
			if (isInterline) {
				if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_VIEW_OTHER_FLIGHT_MANIFEST)) {

					UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

					Collection allowedCarriers = userPrincipal.getCarrierCodes();

					if (allowedCarriers != null && allowedCarriers.contains(flightSearchCarrierCode)) {
						page = ModuleServiceLocator.getAirProxyFlightServiceBD().searchFlightsForDisplay(searchDTO,
								TrackInfoUtil.getBasicTrackInfo(request));
					}

				}
			} else {
				page = ModuleServiceLocator.getAirProxyFlightServiceBD().searchFlightsForDisplay(searchDTO,
						TrackInfoUtil.getBasicTrackInfo(request));
			}
			// page =
			// ModuleServiceLocator.getFlightServiceBD().searchFlightsForDisplay(critrian,
			// recNo, 20,localtime, null, isFlightManifest);
		}
		if (strMode != null && strMode.equals("LOAD")) {

			if (dStopDate != null && dStartDate != null) {
				if (dStopDate.getTime() - dStartDate.getTime() > 24 * 60 * 60 * 1000) {
					throw new ModuleException("reporting.date.exceeds.max.duration");
				}
			}

			// TODO needs backekend methods
			// ResultSet resultSet = null;
			List<FlightLoadReportDataDTO> resultList;
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			String strConTime = request.getParameter("txtConnLimit");
			Integer tottime = null;
			if (strConTime != null && !strConTime.trim().equals("")) {
				String[] arrTime = strConTime.split(":");
				tottime = (new Integer(arrTime[0]) * 60) + new Integer(arrTime[1]);
			} else {
				tottime = new Integer(2160);
			}
			search.setConTime(tottime.toString());
			if (strOperationtype != null && !strOperationtype.trim().equals("All")) {
				search.setOperationType(strOperationtype);
			}
			if (strStatus != null && !strStatus.trim().equals("All")) {
				search.setStatus(strStatus);
			}
			if (strFlightNo != null && !strFlightNo.trim().equals("")) {
				search.setFlightNumber(strFlightStartNo + strFlightNo);
			}
			if (strFrom != null && !strFrom.trim().equals("")) {
				search.setFrom(strFrom);
			}
			if (strTo != null && !strTo.trim().equals("")) {
				search.setTo(strTo);
			}
			search.setCarrierCode(strFlightStartNo);
			try {
				if (strStartTime != null && !strStartTime.trim().equals("")) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(strStartDate) + " " + strStartTime + ":00");
				} else {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(strStartDate) + "00:00:00");
				}

				if (strEndTime != null && !strEndTime.trim().equals("")) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(strStopDate) + " " + strEndTime + ":59");
				} else {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(strStopDate) + " 23:59:59");
				}

				// resultSet =
				// ModuleServiceLocator.getDataExtractionBD().getFlightLoad(search);

			} catch (Exception e) {
				log.error("Error Generating Flight Manifest");
			}
			resultList = ModuleServiceLocator.getAirProxyFlightServiceBD().getFlightLoadReportData(search,
					TrackInfoUtil.getBasicTrackInfo(request));

			createLoadData(request, response, resultList);
		}

		if (strMode != null && (strMode.equals("CHECKIN"))) {
			isFlightManifest = false;
			FlightFlightSegmentSearchCriteria criteria = new FlightFlightSegmentSearchCriteria();
			criteria.setFlightNumber(strFlightStartNo.trim() + strFlightNo.trim().toUpperCase());
			criteria.setSegmentCode(strFrom.trim() + "%");
			criteria.setEstStartTimeZulu(dStartDate);
			criteria.setEstEndTimeZulu(dStopDate);
			if (strCheckinStatus != null && !strCheckinStatus.equals("All")) {
				criteria.setCheckinStatus(strCheckinStatus);
			}
			if (strStatus != null && !strStatus.equals("All")) {
				criteria.setStatus(strStatus);
			}

			FlightManifestSearchDTO searchDTO = new FlightManifestSearchDTO();
			searchDTO.setCriteria(critrian);
			searchDTO.setStartIndex(recNo);
			searchDTO.setPageSize(20);
			searchDTO.setDatesInLocal(localtime);
			searchDTO.setFlightCriteria(criteria);
			searchDTO.setFlightManifest(isFlightManifest);
			searchDTO.setInterline(isInterline);

			if (isInterline) {
				if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_VIEW_OTHER_FLIGHT_MANIFEST)) {

					UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

					Collection allowedCarriers = userPrincipal.getCarrierCodes();

					if (allowedCarriers != null && allowedCarriers.contains(flightSearchCarrierCode)) {
						page = ModuleServiceLocator.getAirProxyFlightServiceBD().searchFlightsForDisplay(searchDTO,
								TrackInfoUtil.getBasicTrackInfo(request));
					}
				}
			} else {
				page = ModuleServiceLocator.getAirProxyFlightServiceBD().searchFlightsForDisplay(searchDTO,
						TrackInfoUtil.getBasicTrackInfo(request));
			}

			// page =
			// ModuleServiceLocator.getFlightServiceBD().searchFlightsForDisplay(critrian,
			// recNo, 20,localtime, criteria, isFlightManifest);
		}

		// get the page data
		if (page != null) {
			flightCol = page.getPageData();
			totRec = page.getTotalNoOfRecords();
		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		log.debug("inside ScheduleHTMLGenerator.getScheduleRowHtml search values");
		return createFlightRowHTML(flightCol, totRec, recNo, isFlightManifest, strFrom);
	}

	/**
	 * Creates the Flight Grid Array From a Collection of Flights
	 * 
	 * @param flights
	 *            the Collection of Flights
	 * @return String the Array containing the Grid Rows
	 */
	private String createFlightRowHTML(Collection flights, Integer totRec, Integer recStNo, boolean isFlightManifest,
			String airportCode) {

		Iterator iter = null;
		DisplayFlightDTO flightDTO = null;
		StringBuffer sb = new StringBuffer();
		int i = 0;
		recStNo += 1;
		sb.append("var flightData = new Array();");
		if (flights != null) {
			iter = flights.iterator();
		}

		if (iter != null) {
			while (iter.hasNext()) {

				flightDTO = (DisplayFlightDTO) iter.next();

				Flight flight = flightDTO.getFlight();
				String strStart = "";
				String strStartZulu = "";

				Set segSet = flight.getFlightSegements();
				Collection legCol = flight.getFlightLegs();
				FlightLeg firstLeg = FlightUtils.getFirstFlightLeg(legCol);
				FlightLeg lastLeg = FlightUtils.getLastFlightLeg(legCol);

				strStart = FlightUtils.formatDate(flight.getDepartureDateLocal(), "dd/MM/yyyy");
				strStartZulu = FlightUtils.formatDate(flight.getDepartureDate(), "dd/MM/yyyy");

				sb.append("flightData[" + i + "] = new Array();");
				sb.append("flightData[" + i + "][1] = '" + flight.getFlightNumber() + "';");
				sb.append("flightData[" + i + "][2] = '" + strStartZulu + "';");
				sb.append("flightData[" + i + "][22] = '" + strStart + "';");
				sb.append("flightData[" + i + "][3] = '" + flight.getOriginAptCode() + "';");
				sb.append("flightData[" + i + "][4] = '" + flight.getDestinationAptCode() + "';");
				sb.append("flightData[" + i + "][5] = '" + FlightUtils.getSegment(segSet) + "';");

				if (firstLeg != null) {
					sb.append("flightData[" + i + "][6] = '"
							+ FlightUtils.formatDate(firstLeg.getEstDepartureTimeZulu(), defaultFormat) + "';"); // etd
																													// -
																													// zulu
					sb.append("flightData[" + i + "][7] = '"
							+ FlightUtils.formatDate(lastLeg.getEstArrivalTimeZulu(), defaultFormat) + "';");// eta
					sb.append("flightData[" + i + "][24] =  '"
							+ FlightUtils.formatDate(firstLeg.getEstDepartureTimeLocal(), defaultFormat) + "';"); // etd
																													// -
																													// local
				} else {
					sb.append("flightData[" + i + "][6] = '&nbsp';");
					sb.append("flightData[" + i + "][7] = '&nbsp';");
				}

				if (!isFlightManifest) {
					try {
						sb.append("flightData[" + i + "][8] = '" + flight.getModelNumber() + "';");
						sb.append("flightData[" + i + "][9] = '" + flightDTO.getAllocated() + "';");
						sb.append("flightData[" + i + "][10] = '" + flightDTO.getSeatsSold() + "';"); // no of
																										// seat sold
						sb.append("flightData[" + i + "][11] = '" + flightDTO.getOnHold() + "';"); // no of on
																									// hold
						sb.append("flightData[" + i + "][12] = '" + flightDTO.getStndBy() + "';"); // no of stand
																									// by
						sb.append("flightData[" + i + "][13] = '" + flightDTO.getCheckedIn() + "';"); // no of
																										// checked
																										// in pax
						sb.append("flightData["
								+ i
								+ "][14] = '"
								+ ((flightDTO.getSeatsSold() + flightDTO.getOnHold() + flightDTO.getStndBy()) - flightDTO
										.getCheckedIn()) + "';"); // no of pax
																	// to be
																	// checked
																	// in
						sb.append("flightData[" + i + "][15] = '" + flight.getStatus() + "';");
						sb.append("flightData[" + i + "][16] = '" + FlightUtils.getSegmentCheckinStatus(segSet, airportCode)
								+ "';");
						sb.append("flightData[" + i + "][17] = '" + flight.getFlightId() + "';");
						sb.append("flightData[" + i + "][18] = '"
								+ ModuleServiceLocator.getAirportServiceBD().getAirport(airportCode).getManualCheckin() + "';");
						sb.append("flightData[" + i + "][19] = '" + FlightUtils.getSegmentId(segSet, airportCode) + "';");

					} catch (ModuleException moduleException) {
						log.error("Exception in FlightHTMLGenerator.createFlightRowHTML - " + moduleException.getModuleDesc()
								+ "]", moduleException);
					}
				} else {

					sb.append("flightData[" + i + "][8] = '" + flight.getOperationTypeId() + "';");

					if (flight.getOverlapingFlightId() != null) {
						sb.append("flightData[" + i + "][9] = '" + flight.getOverlapingFlightId().intValue() + "';");
					} else {
						sb.append("flightData[" + i + "][9] = '&nbsp';");
					}

					sb.append("flightData[" + i + "][10] = '" + flightDTO.getAllocated() + "';");
					sb.append("flightData[" + i + "][11] = '" + flightDTO.getSeatsSold() + "';"); // no of seat
																									// sold
					sb.append("flightData[" + i + "][12] = '" + flightDTO.getOnHold() + "';"); // no of on hold
					sb.append("flightData[" + i + "][13] = '" + flight.getStatus() + "';");

					if (flight.getAvailableSeatKilometers() != null) {
						sb.append("flightData[" + i + "][14] = '" + flight.getAvailableSeatKilometers().intValue() / 1000 + "';");
					} else {
						sb.append("flightData[" + i + "][14] = '&nbsp';");
					}

					if (flight.getScheduleId() != null) {
						sb.append("flightData[" + i + "][15] = '" + flight.getScheduleId() + "';");
					} else {
						sb.append("flightData[" + i + "][15] = '&nbsp';");
					}
					sb.append("flightData[" + i + "][16] = '" + flight.getFlightId() + "';");
				}
				i++;
			}
			sb.append(" totalRecNo =" + totRec + ";");
			sb.append(" recstNo = " + recStNo + ";");
		}
		return sb.toString();
	}

	/**
	 * Creates the Client validations for Flight Search in MANIFEST
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("cc_flight_startdate_null", "startdate");
			moduleErrs.setProperty("cc_flight_stopdate_null", "stopdate");
			moduleErrs.setProperty("cc_flight_startdate_greater", "stdategreat");
			moduleErrs.setProperty("cc_flight_stopdate_greater", "stopdategreat");
			moduleErrs.setProperty("cc_flight_optype_null", "optypenull");
			moduleErrs.setProperty("cc_flight_status_null", "statusnull");
			moduleErrs.setProperty("cc_flight_invalidate", "invalidDate");
			moduleErrs.setProperty("cc_flight_invalidSearch", "invalidSearch");
			moduleErrs.setProperty("cc_flight_invalidFlightSearch", "invalidFlightSearch");
			moduleErrs.setProperty("cc_flight_invalidOriginSearch", "invalidOriginSearch");
			moduleErrs.setProperty("cc_flight_connectionLimit_invalid", "invalidConnectionLimit");
			moduleErrs.setProperty("cc_flight_maxconnectiontime", "maxtimeexceed");
			moduleErrs.setProperty("cc_flight_minconnectiontime", "lessthanMinTime");
			moduleErrs.setProperty("cc_flight_ctiontimeempty", "connectionlimitEmpty");
			moduleErrs.setProperty("cc_flight_invalidStartTime", "invalidStartTime");
			moduleErrs.setProperty("cc_flight_invalidEndTime", "invalidEndTime");
			moduleErrs.setProperty("cc_flight_endtimeless", "endtimeless");
			moduleErrs.setProperty("um.agent.form.invalid.email", "emailInvalid");
			moduleErrs.setProperty("um.problem.email.format", "XBE-ERR-04");
			moduleErrs.setProperty("um.problem.countryCode.mismatch", "countryCodeMismatch");
			moduleErrs.setProperty("um.problem.email.null", "emailEmpty");
			moduleErrs.setProperty("cc_email_success", "emailsuccess");
			moduleErrs.setProperty("cc_email_failure", "emailfailure");
			moduleErrs.setProperty("cc_flight_carrier_code", "invalidCarrierCode");
			moduleErrs.setProperty("cc_flight_Date_range_exceeds", "dateRangeExceeds");
			moduleErrs.setProperty("um.report.ancillary.check.required", "ancillaryCheckedStatus");
			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private static void createLoadData(HttpServletRequest request, HttpServletResponse response,
			List<FlightLoadReportDataDTO> resultList) {

		String id = "UC_REPM_035";
		String reportTemplate = "FlightLoad.jasper";

		try {

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			Map<String, Object> parameters = new HashMap<String, Object>();

			parameters.put("REPORT_ID", id);
			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=FlightLoad.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultList, response);

		} catch (Exception e) {
			log.error(e);
		}

	}

	private static Set<String> getInterlineCarriers() {
		Set<String> interlinedCarriers = new HashSet<String>();
		String[] carrierList = AppSysParamsUtil.getAdjustmentCarrierList();
		if (carrierList != null && carrierList.length > 0) {
			for (String carrierCode : carrierList) {
				if (!AppSysParamsUtil.getDefaultCarrierCode().equals(carrierCode)) {
					interlinedCarriers.add(carrierCode);
				}
			}
		}

		return interlinedCarriers;
	}

	public static void setDisplayOptionsHTML(HttpServletRequest request) throws ModuleException {

		if (AppSysParamsUtil.isShowPSPTFilter()) {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_PSPT_FLITER, "var showPSPTFilter = true;");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_PSPT_FLITER, "var showPSPTFilter = false;");
		}

		if (AppSysParamsUtil.isShowDOCSFilter()) {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_DOCS_FLITER, "var showDOCSFilter = true;");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_DOCS_FLITER, "var showDOCSFilter = false;");
		}

	}
}
