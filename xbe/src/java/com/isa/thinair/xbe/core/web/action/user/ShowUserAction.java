package com.isa.thinair.xbe.core.web.action.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.user.UserRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.User.USER_ADMIN),
		@Result(name = S2Constants.Result.SHOW_DUMMY, value = S2Constants.Jsp.User.DUMMY_USER),
		@Result(name = S2Constants.Result.SHOW_LANDING, value = S2Constants.Jsp.User.DUMMY_LAND),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT),
		@Result(name = S2Constants.Result.GET_XBE_DATA, type = JSONResult.class, value = "") })
public class ShowUserAction extends BaseRequestAwareAction {

	private String station;

	private String agentOptions = "";

	private static Log log = LogFactory.getLog(UserRequestHandler.class);

	public String execute() {
		return UserRequestHandler.execute(request);
	}

	public String loadAgents() {
		String forward = S2Constants.Result.GET_XBE_DATA;
		try {
			agentOptions = UserRequestHandler.loadAgentList(request, station);
		} catch (ModuleException e) {
			log.error("Exception in ShowUserAction.loadAgents() [origin module=" + e.getModuleDesc() + "]", e);
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getAgentOptions() {
		return agentOptions;
	}

}
