package com.isa.thinair.xbe.core.web.v2.generator;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class IncentiveSchemeHG {

	private static String clientErrors;

	/**
	 * Create Client Validations for Incentive Scheme Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.report.todate.futureDate", "toDtShdFutrDt");
			moduleErrs.setProperty("um.report.fromdate.futureDate", "fromDtShdFurDt");
			moduleErrs.setProperty("um.report.fromdate.exeeedsToDate", "fromDtExceed");
			moduleErrs.setProperty("um.report.fromdate.empty", "fromDtEmpty");
			moduleErrs.setProperty("um.report.fromdate.invalid", "fromDtInvalid");
			moduleErrs.setProperty("um.report.todate.empty", "toDtEmpty");
			moduleErrs.setProperty("um.report.todate.invalid", "toDtinvalid");
			moduleErrs.setProperty("um.incentive.scheme.name.required", "schmNameRqrd");
			moduleErrs.setProperty("um.incentive.to.date.should.future", "toDtshdFutrDt");
			moduleErrs.setProperty("um.incentive.from.date.should.future", "fromDtshdFutrDt");
			moduleErrs.setProperty("um.incentive.scheme.least.slab", "leastOneSlab");
			moduleErrs.setProperty("um.incentive.stale.data", "staleData");
			moduleErrs.setProperty("um.incentive.duplicate", "duplicateIncen");
			moduleErrs.setProperty("um.incentive.fare.should.checked", "fareNtChekd");
			moduleErrs.setProperty("um.incentive.basis.should.checked", "basisNtChekd");
			moduleErrs.setProperty("um.incentive.slab.start.cannot.empty", "slabStrtEmpty");
			moduleErrs.setProperty("um.incentive.slab.end.cannot.empty", "slabEndEmpty");
			moduleErrs.setProperty("um.incentive.slab.percentg.cannot.empty", "slabPrcntgEmpty");
			moduleErrs.setProperty("um.incentive.record.not.selected", "recordNtSelectd");
			moduleErrs.setProperty("cc.airadmin.delete.success", "deleteRecoredSucces");
			moduleErrs.setProperty("um.incentive.select.slab", "selectSlab");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.incentive.slab.vlaue.overlap", "slabRngOvrLap");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	private static String createAgentGSAMultiSelectStation() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		String strAssignGSAHtml = "";

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";

		agents = SelectListGenerator.createStationCodes();

		Iterator ite = agents.iterator();
		sb.append("var stns = new Array(); ");

		sb.append(" var agentsArr = new Array();");
		int tempCat = 0;
		int tinc = 0;
		Collection col = null;
		Map keyValues = null;
		Set keys = null;
		Iterator keyIterator = null;
		String agentTpes[] = new String[1];
		String agentType = "";

		while (ite.hasNext()) {

			keyValues = (Map) ite.next();
			keys = keyValues.keySet();
			keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {

				agentType = (String) keyValues.get(keyIterator.next());

				// Populate the Group Array
				sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
				agentTpes[0] = agentType;
				col = SelectListGenerator.createAgentsByStation(agentTpes);
				if (col != null) {
					sb.append("var agents_" + tempCat + " = new Array(); ");
				}
				if (col.size() == 0) {
					sb.append("new Array();");
				} else {
					Iterator iteAgents = col.iterator();
					tinc = 0;
					while (iteAgents.hasNext()) {
						String str[] = (String[]) iteAgents.next();
						sb.append("agents_" + tempCat + "[" + tinc + "] = ");
						sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
						tinc++;
					}
				}
			}
			sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
			tempCat++;
		}
		strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

		+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '200px'; ls.width  = '200px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");

		return sb.toString();
	}

	public static String setAgentsMultiSelect() throws ModuleException {
		String AgentMultiSelect = createAgentGSAMultiSelectStation();
		return AgentMultiSelect;
	}

	public static String createIncentiveSchemeList(HttpServletRequest request) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Collection<AgentIncentive> colIncentive = ModuleServiceLocator.getTravelAgentBD().getActiveAgentIncentive();
		if (colIncentive != null) {
			for (AgentIncentive incentive : colIncentive) {
				if (incentive.getStatus().equals(AgentIncentive.Status.ACTIVE)) {
					sb.append("<option value='");
					sb.append(incentive.getAgentIncSchemeID());
					sb.append("'>");
					sb.append(incentive.getSchemeName());
					sb.append("</option>");
				}

			}
		}
		return sb.toString();
	}
}
