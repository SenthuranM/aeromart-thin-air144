package com.isa.thinair.xbe.core.web.action.reservation;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reservation.LoadDataRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.XBE_DATA),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Reservation.XBE_DATA) })
public class ShowLoadDataAction extends BaseRequestAwareAction {

	public String execute() {
		return LoadDataRequestHandler.execute(request);
	}
}
