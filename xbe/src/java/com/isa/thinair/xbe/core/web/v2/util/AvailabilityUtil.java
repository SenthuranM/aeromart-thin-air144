package com.isa.thinair.xbe.core.web.v2.util;

import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class AvailabilityUtil {

	public static SYSTEM getSearchSystem(List<OriginDestinationInformationTO> ondInfoTOList, TrackInfoDTO trackInfo, String pnr,
			boolean groupPnr,
			boolean hasPrivInterlineSearch) throws ModuleException {

		if (pnr != null && groupPnr) {
			return SYSTEM.INT;
		}

		return ModuleServiceLocator.getCommonServiceBD().getSearchSystem(ondInfoTOList, trackInfo.getCarrierCode());
	}

}
