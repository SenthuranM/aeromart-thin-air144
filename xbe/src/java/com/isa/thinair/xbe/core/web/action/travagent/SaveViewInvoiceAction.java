package com.isa.thinair.xbe.core.web.action.travagent;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.travagent.InvoiceRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.VIEW_EMAIL_INVOICE)
public class SaveViewInvoiceAction extends BaseRequestResponseAwareAction {

	public String execute() throws ModuleException {
		return InvoiceRequestHandler.execute(request, response);
	}
}
