package com.isa.thinair.xbe.core.web.handler.GroupBooking;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class GroupBookingRH extends BasicRH {
	private static Log log = LogFactory.getLog(GroupBookingRH.class);

	/**
	 * Execute Method to Display the Manifest
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);

		setBookingRequestData(request, user.getAirlineCode(), user.getAgentCode());

		// getUserCarrierCodes(request);
		// UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		return forward;
	}

	private static void setBookingRequestData(HttpServletRequest request, String agentCode, String station) {
		String strReturnData = "returnData";
		String strStation = null;
		if (station.length() > 6) {
			strStation = station.substring(3, 6);
		}
		request.setAttribute(strReturnData, "Agent -" + agentCode + "\t Station -" + strStation);

	}

	private static void setRollForwardPage(HttpServletRequest request, int reqID) {

		request.setAttribute("reqID", reqID);
	}

}
