/**
 * 
 */
package com.isa.thinair.xbe.core.web.handler.travagent;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.DateUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.generator.travagent.EmailOutStanBalHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author noshani
 * 
 */
public class EmailOutStanBalanceRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(EmailOutStanBalanceRequestHandler.class);

	private static XBEConfig xBEConfig = new XBEConfig();

	private static GlobalConfig globalConfig = XBEModuleUtils.getGlobalConfig();

	private static final String ACTION_INDIVIDUAL = "INDIVIDUAL";

	private static final String ACTION_ALL = "All";

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		String strbillingEmail = request.getParameter("hdnBillingEmail");
		String fileName = "OutstandingBalanceReport";

		String strYear = request.getParameter("selYear1");
		String strMonth = request.getParameter("selMonth1");
		String strBilingPeriod = request.getParameter("selBPStart");
		String strAgent = request.getParameter("hdnSelectedAgents");
		String strAll = request.getParameter("chkAll");
		String strZero = request.getParameter("chkZero1");

		String strYearTo = request.getParameter("selYear2");
		String strMonthTo = request.getParameter("selMonth2");
		String strBilingPeriodTo = request.getParameter("selBPEnd");
		String strIndividual = request.getParameter("chkIndiAgent");
		String strAllAgent = request.getParameter("chkAllAgent");

		SearchInvoicesDTO srcinvDto = new SearchInvoicesDTO();
		Collection<String> agentcol = new ArrayList<String>();

		if (strYear != null && !strYear.trim().equals("")) {
			srcinvDto.setYear(Integer.parseInt(strYear));
		}
		if (strYearTo != null && !strYearTo.trim().equals("")) {
			srcinvDto.setYearTo(Integer.parseInt(strYearTo));
		}

		if (strMonth != null && !strMonth.trim().equals("")) {
			srcinvDto.setMonth(Integer.parseInt(strMonth));
		}
		if (strMonthTo != null && !strMonthTo.trim().equals("")) {
			srcinvDto.setMonthTo(Integer.parseInt(strMonthTo));
		}

		if (strBilingPeriod != null) {
			srcinvDto.setInvoicePeriod(Integer.parseInt(strBilingPeriod));
		}
		if (strBilingPeriodTo != null) {
			srcinvDto.setInvoicePeriodTo(Integer.parseInt(strBilingPeriodTo));
		}

		if (strAgent != null && !strAgent.equals("")) {
			String[] agentList = strAgent.split(",");
			for (int j = 0; j < agentList.length; j++) {
				agentcol.add(agentList[j]);
			}

			srcinvDto.setAgentCodes(agentcol);
		}
		if (strAll != null && strAll.equals("ALL")) {
			srcinvDto.setAgentCodes(null);
		}
		if (strZero != null && strZero.equals("ZERO")) {
			srcinvDto.setFilterZeroInvoices(true);
		}
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EMAIL)) { // email the invoices

				if (strIndividual != null && strIndividual.equals(ACTION_INDIVIDUAL)) {
					ModuleServiceLocator.getInvoicingBD().sendInvoiceMail(srcinvDto);
					saveMessage(request, xBEConfig.getMessage(WebConstants.KEY_SEND_EMAIL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);

				} else if (strAllAgent != null && strAllAgent.equals(ACTION_ALL)) {

					if (strbillingEmail != null && !strbillingEmail.equals("")) {
						String[] billingEmailList = strbillingEmail.split(",");
						for (int j = 0; j < billingEmailList.length; j++) {
							sendEmail(request, billingEmailList[j], fileName, response);

						}
						saveMessage(request, xBEConfig.getMessage(WebConstants.KEY_SEND_EMAIL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);
					}

				}
			}
			if (strHdnMode != null && strHdnMode.equals("EMAIL2")) {
				sendEmail(request, request.getParameter("txtEmail"), fileName, response);
				saveMessage(request, xBEConfig.getMessage(WebConstants.KEY_SEND_EMAIL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);

			}

		} catch (ModuleException e) {
			log.error("EmailOutStanBalanceRequestHandler EMAIL() Falied", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		try {

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setDetailReportView(request, response);

				log.debug("EmailOutStanBalanceRequestHandler setDetailReportView Success");

			} else {
				log.debug("EmailOutStanBalanceRequestHandler setDetailReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("EmailOutStanBalanceRequestHandler setDetailReportView Failed " + e.getMessageString());
		}

		try {
			setDisplayData(request);
			log.debug("EmailOutStanBalanceRequestHandler SETDISPLAYDATA() SUCCESS");

		} catch (ModuleException e) {
			log.error("EmailOutStanBalanceRequestHandler SETDISPLAYDATA() Falied", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		return forward;
	}

	/**
	 * Sets the Display Data for Invoice Transfer Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setInvoiceYear(request);
		setInvoicePeriod(request);
		setInvoiceRowHtml(request);
	}

	/**
	 * Sets the Client Validations For Invoice Tranfer Page
	 * 
	 * @param request
	 *            the Request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = EmailOutStanBalHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

	}

	/**
	 * Method to get invoice period details.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setInvoicePeriod(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		sb.append("<option value='1' selected>1</option>");
		sb.append("<option value='2'>2</option>");

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			sb.append("<option value='3'>3</option>");
			sb.append("<option value='4'>4</option>");
			sb.append("<option value='5'>5</option>");
		}

		request.setAttribute(WebConstants.REQ_IS_WEEKLYINVOICE_ENABLED, AppSysParamsUtil.isWeeklyInvoiceEnabled());
		request.setAttribute(WebConstants.REQ_INVOICEPERIOD, sb.toString());
	}

	/**
	 * Method get current year and previous year & set to the Request.
	 * 
	 * @param request
	 *            the ModuleException
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setInvoiceYear(HttpServletRequest request) throws ModuleException {
		String currentYear = null;
		String previousYear = null;
		String selectYear = null;

		StringBuffer sb = new StringBuffer();

		Calendar year = new GregorianCalendar();
		currentYear = Integer.toString(year.get(Calendar.YEAR));
		previousYear = Integer.toString(year.get(Calendar.YEAR) - 1);
		String s[] = new String[4];
		s[0] = currentYear;
		s[1] = currentYear;
		s[2] = previousYear;
		s[3] = previousYear;

		sb.append("<option value='" + s[0] + "' selected>" + s[1] + "</option>");
		sb.append("<option value='" + s[2] + "'>" + s[3] + "</option>");

		selectYear = sb.toString();

		request.setAttribute(WebConstants.REQ_CURRENT_YEAR, currentYear);
		request.setAttribute(WebConstants.REQ_INVOICEYEAR, selectYear);
	}

	/**
	 * Sets the Agents Type List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the Agent List to the Requst by Station
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		String strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * Sets the Invoice Transfer Grid Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setInvoiceRowHtml(HttpServletRequest request) throws ModuleException {
		EmailOutStanBalHTMLGenerator htmlGen = new EmailOutStanBalHTMLGenerator();
		String strHtml = htmlGen.getInvoiceTransRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_INVOIVEGEN_DETAILS, strHtml);

		String strTotal = htmlGen.getTotalInvoiceTransRowHtml(request);
		request.setAttribute("reqTotalInvoiceGenDetails", strTotal);

	}

	private static void setDetailReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		Collection<String> agentcol = new ArrayList<String>();

		String strAgent = request.getParameter("hdnSelectedAgents");
		String fromDate = request.getParameter("hdnFromDate");
		String toDate = request.getParameter("hdnToDate");

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			Date from = getFromDate(request);
			Date to = getToDate(request);

			fromDate = DateUtil.formatDate(from, DateUtil.DEFAULT_DATE_INPUT_FORMAT);
			toDate = DateUtil.formatDate(to, DateUtil.DEFAULT_DATE_INPUT_FORMAT);
		}

		String strZero = request.getParameter("chkZero1");

		String id = " SC_FACM_007";
		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strZero != null && strZero.equals("ZERO")) {
				search.setExcludeZeroBalance(true);
			}

			if (strAgent != null && !strAgent.equals("")) {
				String[] agentList = strAgent.split(",");
				for (int j = 0; j < agentList.length; j++) {
					agentcol.add(agentList[j]);
				}

				search.setAgents(agentcol);
			}

			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));

			reportTemplate = "EmailOutstandingBalanceDetail.jasper";

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getEmailOutstandingBalanceDetailData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

		} catch (Exception e) {
			log.error("EmailOutStanBalanceRequestHandler setDetailReportView" + e);
		}
	}

	private static void setReportAttachment(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		Collection<String> agentcol = new ArrayList<String>();

		String strAgent = request.getParameter("hdnSelectedAgents");
		String fromDate = request.getParameter("hdnFromDate");
		String toDate = request.getParameter("hdnToDate");

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			Date from = getFromDate(request);
			Date to = getToDate(request);

			fromDate = DateUtil.formatDate(from, DateUtil.DEFAULT_DATE_INPUT_FORMAT);
			toDate = DateUtil.formatDate(to, DateUtil.DEFAULT_DATE_INPUT_FORMAT);
		}

		String strZero = request.getParameter("chkZero1");

		String fileName = (String) request.getAttribute("fileNameDetail");

		String id = " SC_FACM_007";
		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true);;
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strAgent != null && !strAgent.equals("")) {
				String[] agentList = strAgent.split(",");
				for (int j = 0; j < agentList.length; j++) {
					agentcol.add(agentList[j]);
				}

				search.setAgents(agentcol);
			}

			if (strZero != null && strZero.equals("ZERO")) {
				search.setExcludeZeroBalance(true);
			}

			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));

			reportTemplate = "EmailOutstandingBalanceDetail.jasper";

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getEmailOutstandingBalanceDetailData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);

			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			byte[] byteStream = ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters,
					resultSet);

			FTPServerProperty serverProperty = new FTPServerProperty();
			FTPUtil ftpUtil = new FTPUtil();

			// Code to FTP
			serverProperty.setServerName(globalConfig.getFtpServerName());
			if (globalConfig.getFtpServerUserName() == null) {
				serverProperty.setUserName("");
			} else {
				serverProperty.setUserName(globalConfig.getFtpServerUserName());
			}
			serverProperty.setUserName(globalConfig.getFtpServerUserName());
			if (globalConfig.getFtpServerPassword() == null) {
				serverProperty.setPassword("");
			} else {
				serverProperty.setPassword(globalConfig.getFtpServerPassword());
			}
			ftpUtil.setPassiveMode(globalConfig.isFtpEnablePassiveMode());
			ftpUtil.uploadAttachment(fileName, byteStream, serverProperty);

		} catch (Exception e) {
			log.error("EmailOutStanBalanceRequestHandler setDetailReportView" + e);
		}

	}

	private static Date getFromDate(HttpServletRequest request) {
		SearchInvoicesDTO searchInvoicesFrom = new SearchInvoicesDTO();

		searchInvoicesFrom.setYear(Integer.valueOf(request.getParameter("selYear1")));
		searchInvoicesFrom.setMonth(Integer.valueOf(request.getParameter("selMonth1")));
		searchInvoicesFrom.setInvoicePeriod(Integer.valueOf(request.getParameter("selBPStart")));

		return BLUtil.getFromDate(searchInvoicesFrom);
	}

	private static Date getToDate(HttpServletRequest request) {
		SearchInvoicesDTO searchInvoicesTo = new SearchInvoicesDTO();

		searchInvoicesTo.setYear(Integer.valueOf(request.getParameter("selYear2")));
		searchInvoicesTo.setMonth(Integer.valueOf(request.getParameter("selMonth2")));
		searchInvoicesTo.setInvoicePeriod(Integer.valueOf(request.getParameter("selBPEnd")));

		return BLUtil.getToDate(searchInvoicesTo);
	}

	private static void sendEmail(HttpServletRequest request, String emailAddress, String fileName, HttpServletResponse response) {
		UserMessaging usermsg = null;
		List<UserMessaging> messageList = null;
		MessageProfile msgProfile = null;
		HashMap<String, UserMessaging> map = null;
		Topic topic = null;

		fileName = fileName + ".pdf";
		String path = PlatformConstants.getAbsAttachmentsPath() + "/" + fileName;

		usermsg = new UserMessaging();
		messageList = new ArrayList<UserMessaging>();
		msgProfile = new MessageProfile();
		map = new HashMap<String, UserMessaging>();
		topic = new Topic();
		usermsg.setToAddres(emailAddress);

		usermsg.setFirstName("All");
		usermsg.setLastName("");
		messageList.add(usermsg);
		msgProfile.setUserMessagings(messageList);
		map.put("user", usermsg);

		try {
			request.setAttribute("fileNameDetail", fileName);
			setReportAttachment(request, response);
			File attachment = new File(path);
			if (attachment.exists()) {
				topic.addAttachmentFileName(fileName);
			}

			topic.setTopicParams(map);
			topic.setTopicName("outstanding_balance_rpt_email");
			msgProfile.setTopic(topic);
			log.debug("sending invoice summary to " + emailAddress);
			ModuleServiceLocator.getMessagingBD().sendMessage(msgProfile);
		} catch (ModuleException e) {
			log.error("Error in generating and sending email" + e.getMessage());
		}

	}

}
