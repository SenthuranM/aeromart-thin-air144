package com.isa.thinair.xbe.core.web.v2.action.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.v2.generator.reservation.BlacklistedPaxResHG;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.BLACKLISTED_PAX_RES),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowBlackListedPaxReservationAction extends BaseRequestAwareAction {
	/**
	 * @author subash
	 */
	private static Log log = LogFactory.getLog(ShowBlackListedPaxReservationAction.class);

	public String execute() {
		
		try {
			String strNationalityList;
			strNationalityList = SelectListGenerator.createNationalityList();
			request.setAttribute(WebConstants.REQ_HTML_NATIONALITY_LIST, strNationalityList);
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, BlacklistedPaxResHG.getTemplateClientErrors(request));
			
			return S2Constants.Result.SUCCESS;
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error occor : "+ e.getMessage());
			return S2Constants.Result.ERROR;
		}
		
		
	}
}
