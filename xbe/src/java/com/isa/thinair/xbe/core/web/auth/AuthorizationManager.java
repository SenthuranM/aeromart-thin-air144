package com.isa.thinair.xbe.core.web.auth;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.config.XBEConfig;

public class AuthorizationManager {

	private static Log log = LogFactory.getLog(AuthorizationManager.class);

	public static final boolean isUserAuthorized(User user, String url) {
		boolean auth = false;
		String priviledge = XBEConfig.getUrlPriviledge(url);

		if (priviledge == null) {
			auth = true;
		} else {
			Collection privColl = null;
			try {
				privColl = user.getPrivitedgeIDs();
			} catch (ModuleException e) {
				// TODO: handle exception
				log.error(e);
			}
			if (privColl != null) {
				auth = privColl.contains(priviledge);
			}
		}

		return auth;
	}

}
