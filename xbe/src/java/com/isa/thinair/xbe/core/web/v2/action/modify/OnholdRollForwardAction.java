package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.AuthorizationsUtil;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class OnholdRollForwardAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(OnholdRollForwardAction.class);

	private boolean success = true;

	private String messageTxt;

	private String pnr;

	private String jsonResSegs;

	private String jsonRollForwardFlights;

	private Integer adultCount;

	private Integer childCount;

	private Integer infantCount;

	private String system;

	private String strGeneratedPnrs;

	private boolean overrideBufferTime;

	private int bufferDays;

	private String bufferTime;
	
	private boolean overBook;

	@SuppressWarnings("unchecked")
	public String execute() {
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		try {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(jsonResSegs);
			Map<String, RollForwardFlightRQ> rollForwardingFlights = ModifyReservationJSONUtil
					.populateRollForwardRequest(jsonRollForwardFlights);

			Map<String, String> mapPrivileges = (Map<String, String>) request.getSession().getAttribute(
					WebConstants.SES_PRIVILEGE_IDS);
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			// Set onhold release times for roll forwarding bookings
			BookingUtil.setOnholdRealeaseTimes(rollForwardingFlights, mapPrivileges, userPrincipal.getAgentCode(),
					overrideBufferTime, bufferDays, bufferTime);
			boolean isDuplicateNameSkip = AuthorizationsUtil.hasPrivilege(mapPrivileges.keySet(), "xbe.res.make.dupli.name.skip");
			ServiceResponce serviceResponce = ModuleServiceLocator.getAirproxyReservationBD().rollForwardOnholdBooking(pnr,
					colsegs, adultCount, childCount, infantCount, rollForwardingFlights, SYSTEM.getEnum(system), getTrackInfo(),
					isDuplicateNameSkip, overBook);

			if (serviceResponce.isSuccess()) {
				Collection<String> generatedPnrs = (Collection<String>) serviceResponce
						.getResponseParam(CommandParamNames.PNR_LIST);
				strGeneratedPnrs = CommonUtil.getCollectionElementsStr(generatedPnrs);
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	@JSON(serialize = false)
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	@JSON(serialize = false)
	public String getJsonResSegs() {
		return jsonResSegs;
	}

	public void setJsonResSegs(String jsonResSegs) {
		this.jsonResSegs = jsonResSegs;
	}

	@JSON(serialize = false)
	public String getJsonRollForwardFlights() {
		return jsonRollForwardFlights;
	}

	public void setJsonRollForwardFlights(String jsonRollForwardFlights) {
		this.jsonRollForwardFlights = jsonRollForwardFlights;
	}

	@JSON(serialize = false)
	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	@JSON(serialize = false)
	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	@JSON(serialize = false)
	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	@JSON(serialize = false)
	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getStrGeneratedPnrs() {
		return strGeneratedPnrs;
	}

	public void setStrGeneratedPnrs(String strGeneratedPnrs) {
		this.strGeneratedPnrs = strGeneratedPnrs;
	}

	public void setOverrideBufferTime(boolean overrideBufferTime) {
		this.overrideBufferTime = overrideBufferTime;
	}

	public void setBufferDays(int bufferDays) {
		this.bufferDays = bufferDays;
	}

	public void setBufferTime(String bufferTime) {
		this.bufferTime = bufferTime;
	}

	public boolean isOverBook() {
		return overBook;
	}

	public void setOverBook(boolean overBook) {
		this.overBook = overBook;
	}

}
