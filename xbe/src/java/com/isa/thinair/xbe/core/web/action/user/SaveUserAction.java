package com.isa.thinair.xbe.core.web.action.user;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.user.UserRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.User.USER_ADMIN),
		@Result(name = S2Constants.Result.SHOW_DUMMY, value = S2Constants.Jsp.User.DUMMY_USER),
		@Result(name = S2Constants.Result.SHOW_LANDING, value = S2Constants.Jsp.User.DUMMY_LAND),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class SaveUserAction extends BaseRequestAwareAction {

	public String execute() {
		return UserRequestHandler.execute(request);
	}

}
