package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentHolder;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.NameChangeUtil;
import com.isa.thinair.airproxy.api.utils.PNRModificationAuthorizationUtils;
import com.isa.thinair.airproxy.api.utils.ReservationAssemblerConvertUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.v2.reservation.PaxFareTO;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.api.dto.v2.AncillaryPaxTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.InfantPaxTO;
import com.isa.thinair.xbe.api.dto.v2.NoShowPassenger;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountPaymentsTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.TaxReversalDTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationContactInfoDTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.api.dto.v2.XBESessionDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.exception.XBEException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.NameChangeParameters;
import com.isa.thinair.xbe.core.web.v2.util.PaxAccountUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency" })
public class LoadReservationShowDataAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadReservationShowDataAction.class);

	private boolean success = true;

	private String messageTxt;

	private String groupPNR;

	private String pnr;

	private String marketingAirlineCode;

	private String airlineCode;

	private Long interlineAgreementId;

	private ContactInfoTO contactInfo;

	private Collection<AdultPaxTO> paxAdults;

	private Collection<InfantPaxTO> paxInfants;

	private Collection<PaxFareTO> paxPrice;

	private boolean isVoidReservation;

	private Collection<FlightInfoTO> flightDetails;

	private Collection<AncillaryPaxTO> paxWiseAnci;

	private BookingTO bookingInfo;

	private String userNotes;

	private PaymentSummaryTO paxPaymentSummary;

	private Collection<PaymentPassengerTO> paxPaymentTO;

	private PaymentSummaryTO paymentSummary;

	private String firstFlightDepartDate;

	private String lastFlightArrivalDate;

	private String firstFlightDepartDateTime;

	private String lastFlightDepartDate;

	private List<LCCClientAlertInfoTO> alertInfo;

	private List<LCCClientReservationInsurance> insurances;

	private ReservationProcessParams rParam;

	private String carrierGroupingOptions;

	private XBESessionDTO xbeData;

	private String forwardmessage;

	private String ohdAmount;

	private boolean loadAudit;

	private List<LCCClientAlertInfoTO> flexiAlertInfo;

	private CurrencyExchangeRate currencyExchangeRate;

	private String originSalesChanel;

	private String system;

	private boolean printEmailIndItinerary = false;

	private int noOfCnfSegments;

	private String endorsementTxt;

	private String actualOrigin;

	private String actualDestination;

	private String ohdRollForwardStartDate;

	private boolean allowOnholdRollFoward = true;

	private String rollForwardErrorCode;

	private Map<String, Collection<RefundableChargeDetailDTO>> refundableChargeDetails;

	private boolean allowVoidReservation = false;

	private boolean gdsPnr = false;

	private boolean gdsAllowAncillary = false;
	
	private boolean csPnr = false;

	private Map<String, PaxAccountPaymentsTO> perPaxPaymentDetails;

	private Map<String, String> perPaxCreditDetails;

	private Set<Integer> applicablBINs;

	private boolean allowCSVDetailUpload;

	private boolean allowCreateUserAlert;

	private String agentBalance;

	private boolean isGroupBookingRequest = false;

	private CurrencyExchangeRate countryCurrExchangeRate;

	private Map<String, BundledFareDTO> segWiseBindleAlertInfo;

	private boolean allowNameChange;

	private Integer gdsId;

	private boolean csOCPnr = false;

	private boolean allowExtendedNameChange = false;

	private boolean allowRequoteOverride;

	private boolean editFFID = false;

	private boolean addFFID = false;

	private boolean inEditFFIDBuffer;

	private boolean hasUnSegment;
	
	private boolean infantPaymentSeparated = false;

	private TaxReversalDTO taxReversalDto;
	
	private List<TaxInvoice> taxInvoices;

	private String serviceTaxApplicableCountries;
	
	private boolean serviceTaxApplied = false;
	
	private boolean handlingFeeApplicableForAdj = false;
	
	private List<FlightSegmentTO> flightSegmentToList;

	private boolean isUserDefinedSSR = false;
	
	private Map<String, String> paxWiseUserDefinedChgMap = null;

	private static final String USER_DEFINED_SSR_TOTAL = "USER_DEFINED_SSR_TOTAL";

	private String originCountryOfCall;


	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShoppingCart = new BookingShoppingCart(ChargeRateOperationType.MODIFY_ONLY);
			XBEReservationInfoDTO resInfo = new XBEReservationInfoDTO();
			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			String strCurrency = up.getAgentCurrencyCode();
			String agentCode = up.getAgentCode();
			if (strCurrency == null || strCurrency.equals("")) {
				strCurrency = AppSysParamsUtil.getBaseCurrency();
			}

			boolean applyExtendedNameModification = AppSysParamsUtil.applyExtendedNameModificationFunctionality();
			currencyExchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getCurrencyExchangeRate(strCurrency, ApplicationEngine.XBE);
			bookingShoppingCart.setCurrencyExchangeRate(currencyExchangeRate);

			if (isGroupPNR || (StringUtils.isNotEmpty(marketingAirlineCode) && StringUtils.isNotEmpty(airlineCode))) {
				system = SYSTEM.INT.toString();
			} else {
				system = SYSTEM.AA.toString();
			}

			LCCClientReservation reservation = ReservationUtil.loadFullLccReservation(pnr, isGroupPNR, null, bookingShoppingCart,
					request, loadAudit, marketingAirlineCode, airlineCode, interlineAgreementId,
					AppSysParamsUtil.isPromoCodeEnabled(), getTrackInfo());
			enableFFIDAddEdit(reservation);

			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					&& reservation.getZuluReleaseTimeStamp().before(CalendarUtil.getCurrentZuluDateTime())
					&& AppSysParamsUtil.isCancelExpiredOhdResEnable()) {
				log.debug("Cancel expired reservation before loading : " + reservation.getPNR());
				LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO
						.composeCancelReservationRequest(reservation.getPNR(), null, null, null, reservation.getVersion(), "Cancelled by scheduler user");
				lccClientResAlterModesTO.setGroupPnr(reservation.isGroupPNR());
				ModuleServiceLocator.getAirproxyReservationBD().cancelReservation(lccClientResAlterModesTO, getClientInfoDTO(),
						getTrackInfo(), false, true);
				reservation = ReservationUtil.loadFullLccReservation(pnr, isGroupPNR, null, bookingShoppingCart, request,
						loadAudit, marketingAirlineCode, airlineCode, interlineAgreementId, AppSysParamsUtil.isPromoCodeEnabled(),
						getTrackInfo());
				log.debug("Load calceled expired reservation : " + reservation.getPNR() + " new status :"
						+ reservation.getStatus());
			}
			
			// only GDS reservations will be allowed When AA is not the Validating carrier 
			if (AppSysParamsUtil.isGDSIntergrationEnabled() && reservation.getGdsId() != null
					&& !ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())
					&& !((GDSStatusTO) ReservationModuleUtils.getGlobalConfig().getActiveGdsMap()
							.get(reservation.getGdsId().toString())).isAaValidatingCarrier()) {

				ReservationUtil.initiateDisplayForModifiedReservation(reservation);
			}
			
			// TODO implement getGDSstatus in lcc flow temp fix
			if (reservation.getGdsId() != null
					&& !AppSysParamsUtil.getDefaultCarrierCode().equals(reservation.getMarketingCarrier())) {
				throw new XBEException("gds.display.rstrct.dry.usrs");
			}

			reservation = setFlagToDisplayOnXBE(reservation);
			if (!isGroupPNR && reservation.isGroupPNR()) {
				system = SYSTEM.INT.toString();
				isGroupPNR = true;
				groupPNR = pnr;
				reservation = ReservationUtil.loadFullLccReservation(pnr, isGroupPNR, null, bookingShoppingCart, request,
						loadAudit, marketingAirlineCode, airlineCode, interlineAgreementId, AppSysParamsUtil.isPromoCodeEnabled(),
						getTrackInfo());
			}
			validateLoadReservation(reservation.getBookingCategory(), reservation.getAdminInfo().getOwnerAgentCode(), up.getAgentCode(), up.getAgentTypeCode());

			bookingShoppingCart.addNewServiceTaxes(reservation.getApplicableServiceTaxes());
			if (!isGroupPNR) {
				strCurrency = reservation.getLastCurrencyCode();

				Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();

				if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
					bookingShoppingCart.setSelectedCurrencyTimestamp(reservation.getLastModificationTimestamp());
					if (reservation.getLastModificationTimestamp() != null) {
						exchangeRateByDate = reservation.getLastModificationTimestamp();
					}
				}

				currencyExchangeRate = new ExchangeRateProxy(exchangeRateByDate).getCurrencyExchangeRate(strCurrency,
						ApplicationEngine.XBE);

				bookingShoppingCart.setCurrencyExchangeRate(currencyExchangeRate);
			}

			resInfo.setOriginAgentCode(reservation.getAdminInfo().getOriginAgentCode());
			resInfo.setAvailableBalance(reservation.getTotalAvailableBalance());
			resInfo.setReservationStatus(reservation.getStatus());
			resInfo.setReservationLastModDate(reservation.getLastModificationTimestamp());
			resInfo.setModifiableReservation(reservation.isModifiableReservation());
			resInfo.setPaxCreditMap(ReservationUtil.getPaxCreditMap(reservation.getPassengers()));
			resInfo.setResBaggageSummary(AncillaryCommonUtil.getResBaggageSummary(reservation));
			resInfo.setBundledFareDTOs(reservation.getBundledFareDTOs());
			resInfo.setExistingReleaseTimestamp(reservation.getZuluReleaseTimeStamp());
			resInfo.setLccPromotionInfoTO(reservation.getLccPromotionInfoTO());
			resInfo.setOriginChannelId(
					Integer.valueOf(cleanedChannelId(reservation.getAdminInfo().getOriginChannelId())).intValue());
			resInfo.setMarketingAirlineCode(reservation.getMarketingCarrier());
			resInfo.setInfantPaymentSeparated(reservation.isInfantPaymentSeparated());
			resInfo.setContactInfoDTO(XBEReservationContactInfoDTO.getContactInfoDTO(reservation.getContactInfo()));

			String ownerChannelId = cleanedChannelId(reservation.getAdminInfo().getOwnerChannelId());
			if (!StringUtil.isNullOrEmpty(ownerChannelId)) {
				resInfo.setOwnerChannelId(ownerChannelId);

			}

			request.getSession().setAttribute(S2Constants.Session_Data.XBE_SES_RESDATA, resInfo);

			String ticketPrice = AccelAeroCalculator.formatAsDecimal(reservation.getTotalTicketPrice());
			String totalDiscount = setDiscountAmount(reservation);
			String totalTaxSur = AccelAeroCalculator.formatAsDecimal(
					AccelAeroCalculator.add(reservation.getTotalTicketTaxCharge(), reservation.getTotalTicketSurCharge()));
			
			String userDefinedChargeAmount = "0.00";
			if (isUserDefinedSSREnabled(reservation)){
				isUserDefinedSSR = true;
				userDefinedChargeAmount = getUserDefinedSSRCharge(reservation).get(USER_DEFINED_SSR_TOTAL);
			} else {
				isUserDefinedSSR = false;
				userDefinedChargeAmount = AccelAeroCalculator.formatAsDecimal(reservation.getTotalGOQUOAmount());
			}
			paxWiseUserDefinedChgMap = getUserDefinedSSRCharge(reservation);
			
			if (reservation.getGdsId() != null) {
				resInfo.setGdsReservationModAllowed(((GDSStatusTO) ReservationModuleUtils.getGlobalConfig().getActiveGdsMap()
						.get(reservation.getGdsId().toString())).isAirlineResModifiable());
			}
			if (reservation.getGroupBookingRequestID() > 0) {
				isGroupBookingRequest = true;
			}

			boolean isGoquo = false;
			if (resInfo.getOriginChannelId() == SalesChannelsUtil.SALES_CHANNEL_GOQUO) {
				isGoquo = true;
			}

			rParam = new ReservationProcessParams(request, reservation.getStatus(), reservation.getSegments(),
					resInfo.isModifiableReservation(), resInfo.isGdsReservationModAllowed(), reservation.getLccPromotionInfoTO(),
					reservation.getAdminInfo().getOwnerAgentCode(), false, isGoquo);
			rParam.setNotTransfered(false);

			if (system.equals(SYSTEM.INT.toString())) {
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_ACCESSED, Boolean.TRUE);
				resInfo.setInterlineModificationParams(reservation.getInterlineModificationParams());
				rParam.enableInterlineModificationParams(reservation.getInterlineModificationParams(), reservation.getStatus(),
						reservation.getSegments(), reservation.isModifiableReservation(), reservation.getLccPromotionInfoTO());
				rParam.addInterlineChargeAdjustmentPrivileges(reservation.getInterlineChargeAdjustmentPrivileges());
			}
			
			boolean isSegmentModifiableAsPerETicketStatus = PNRModificationAuthorizationUtils.isSegmentModifiableAsPerETicketStatus(reservation, null);
			if (rParam.isCancelReservation()) {
				rParam.setCancelReservation(isSegmentModifiableAsPerETicketStatus);				
			}
			// ReservationProcessParams is initialized in many places, by adding
			// this to session can eliminate the
			// Unnecessary call.
			// TODO ReservationProcessParams usages must be Re-factored to load
			// from the session.
			request.getSession().setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, rParam);

			String strStation = ((UserPrincipal) request.getUserPrincipal()).getAgentStation();
			Collection colUserDST = ((UserPrincipal) request.getUserPrincipal()).getColUserDST();

			NameChangeParameters nameModificationParams = getNameModificationParams(reservation);

			Object[] object = ReservationUtil.loadPaxList(reservation.getPassengers(), rParam, isGroupPNR,
					reservation.getStatus(), nameModificationParams, reservation.isInfantPaymentSeparated());

			allowCSVDetailUpload = rParam.isAllowPassengerDetailCSVUpload();

			String ownerAgent = null;
			if (ReservationUtil.getWebBookingChannel(reservation.getAdminInfo().getOwnerChannelId()) == SalesChannelsUtil.SALES_CHANNEL_WEB) {
				ownerAgent = "WEB-USER";
			} else if (ReservationUtil.getWebBookingChannel(reservation.getAdminInfo().getOwnerChannelId()) == SalesChannelsUtil.SALES_CHANNEL_IOS) {
				ownerAgent = "IOS-USER";
			} else if (ReservationUtil.getWebBookingChannel(reservation.getAdminInfo().getOwnerChannelId()) == SalesChannelsUtil.SALES_CHANNEL_ANDROID) {
				ownerAgent = "ANDROID-USER";
			} else {
				ownerAgent = reservation.getAdminInfo().getOwnerAgentContactInfo().getAgentName();
			}

			if (BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_GROUP_REFUND)
					&& !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
				@SuppressWarnings({ "unchecked", "rawtypes" })
				List<LCCClientReservationPax> passengers = new ArrayList(reservation.getPassengers());
				Collections.sort(passengers);
				perPaxPaymentDetails = buildPerPaxPaymentDetails(passengers, reservation.isInfantPaymentSeparated());
				perPaxCreditDetails = buildPerPaxCreditDetails(passengers, reservation.isInfantPaymentSeparated());
			}

			xbeData = new XBESessionDTO();

			xbeData.setPnrPaxs(reservation.getPassengers());
			xbeData.setPassengerTktCoupons(ReservationUtil.getPassengerETicketCoupons(reservation.getPassengers()));
			xbeData.setPnrSegments(reservation.getSegments());
			xbeData.setPnrOndGroups(reservation.getCarrierOndGrouping());
			xbeData.setPnrAlerts(reservation.getAlertInfoTOs());
			xbeData.setNoOfAdults(reservation.getTotalPaxAdultCount());
			xbeData.setNoOfClidren(reservation.getTotalPaxChildCount());
			xbeData.setNoOfInfants(reservation.getTotalPaxInfantCount());
			String marketingOwnerAgentCode = PaxTypeUtils.getMarketingOwnerAgentCode(reservation.getMarketingCarrier(),
					reservation.getAdminInfo().getOwnerAgentCode());
			if (marketingOwnerAgentCode == null) {
				xbeData.setOwnerAgentCode(reservation.getAdminInfo().getOwnerAgentCode());
			} else {
				xbeData.setOwnerAgentCode(marketingOwnerAgentCode);
			}
			xbeData.setContactInfo(reservation.getContactInfo());
			xbeData.setFlexiPnrAlerts(reservation.getFlexiAlertInfoTOs());
			xbeData.setOpenReturnSegInfo(reservation.getOpenReturnSegmentInfoTOs());
			if (reservation.getSegmentInfoTOs() == null) {
				reservation.setSegmentInfoTOs(ReservationUtil.getSegmentAlertInfo(reservation.getSegments()));
			}
			xbeData.setAdditionalSegInfo(reservation.getSegmentInfoTOs());

			AutoCancellationInfo autoCancellationInfo = reservation.getAutoCancellationInfo();
			if (autoCancellationInfo != null) {
				LCCClientAlertTO lccClientAlertTO = new LCCClientAlertTO();
				lccClientAlertTO.setAlertId(autoCancellationInfo.getAutoCancellationId());
				String expireTimeInLocal = DateUtil.getAgentLocalTime(autoCancellationInfo.getExpireOn(), colUserDST, strStation,
						"dd MMM yyyy - HH:mm");
				lccClientAlertTO.setContent(expireTimeInLocal);

				// Set Auto cancellation alert info
				xbeData.setAutoCancellationAlert(lccClientAlertTO);
			}

			xbeData.setPromotionAlerts(ReservationAssemblerConvertUtil.generatePromotionInfoAlerts(reservation));

			// Set Applicable Bank Identification numbers if promotion exists
			if (reservation.getLccPromotionInfoTO() != null) {
				applicablBINs = reservation.getLccPromotionInfoTO().getApplicableBINs();
				xbeData.setCreditPromotion(PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT
						.equals(reservation.getLccPromotionInfoTO().getDiscountAs()));
			}

			contactInfo = ReservationUtil.loadContactInfo(reservation.getContactInfo());
			paxAdults = (Collection<AdultPaxTO>) object[0];
			paxInfants = (Collection<InfantPaxTO>) object[1];
			paxPrice = (Collection<PaxFareTO>) object[2];

			// sets true if the reservation is marked as void, this is to
			// disable the UI controls which allow
			// modification
			// of the reservation
			setVoidReservation(false);
			if (resInfo != null && resInfo.getReservationStatus() != null) {
				if (ReservationInternalConstants.ReservationStatus.VOID.equals(resInfo.getReservationStatus())) {
					setVoidReservation(true);
				}
			}

			// CC charge as a fixed value , update payable pax count.
			bookingShoppingCart.setPayablePaxCount(ReservationUtil.getNoOfPayablePaxCount(reservation.getPassengers()));
			// update no of CNF segment count
			noOfCnfSegments = ReservationUtil.getNoOfCnfSegments(reservation.getSegments());
			hasUnSegment = hasUncancelledUnSegment(reservation.getSegments());
			enableFFIDAddEdit(reservation);
			/* segment modification criteria comes with the flight details */
			flightDetails = ReservationUtil.loadFlightSegments(reservation, rParam);
			flightSegmentToList = ReservationUtil.loadFlightSegmentToList(reservation);
			userNotes = reservation.getLastUserNote();
			paxPaymentSummary = ReservationUtil.loadPaxPriceSummary(strCurrency, ticketPrice, totalDiscount,  userDefinedChargeAmount);
			Object[] arrObj = ReservationUtil.loadPaxPriceSummary(reservation.getPassengers(), reservation.isInfantPaymentSeparated());
			String pendingAgentCommissions = CommonUtil.getPendingAgentCommissions(agentCode, pnr);
			paxPaymentTO = (Collection<PaymentPassengerTO>) arrObj[0];
			// FIXME :Must pass handling charges if any
			paymentSummary = ReservationUtil.createPaymentSummary(AppSysParamsUtil.getBaseCurrency(),
					AccelAeroCalculator.formatAsDecimal((BigDecimal) arrObj[1]),
					AccelAeroCalculator.getDefaultBigDecimalZero().toString(),
					AccelAeroCalculator.formatAsDecimal(reservation.getTotalTicketModificationCharge()),
					AccelAeroCalculator.formatAsDecimal(reservation.getTotalPaidAmount()), ticketPrice,
					AccelAeroCalculator.formatAsDecimal(reservation.getTotalTicketFare()), totalTaxSur, strCurrency,
					totalDiscount, pendingAgentCommissions, userDefinedChargeAmount, null);

			String strStatus = reservation.getStatus();
			String strDisplayStatus = "";
			ohdAmount = "";
			if (strStatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				// if (!isWaitListedSegmentExists(reservation.getSegments())) {
				String localTime = DateUtil.getAgentLocalTime(reservation.getZuluReleaseTimeStamp(), colUserDST, strStation,
						"dd-MM-yyyy HH:mm (EEEE)");
				strDisplayStatus = "Release on Hold : " + localTime + " LOCAL";
				// } else {
				// strDisplayStatus = "Wait Listed";
				// }

				if (!isGroupPNR && !reservation.getLastCurrencyCode().equals(AppSysParamsUtil.getBaseCurrency())) {
					if (currencyExchangeRate != null) {
						BigDecimal ohdPaycurrencyAmount = AccelAeroCalculator
								.multiply(currencyExchangeRate.getMultiplyingExchangeRate(), reservation.getTotalTicketPrice());
						ohdAmount = "[ " + AccelAeroCalculator.formatAsDecimal(ohdPaycurrencyAmount) + " ] "
								+ reservation.getLastCurrencyCode();
					}
				}
				// update charge rate type to make since onhold booking is not
				// paid yet.
				bookingShoppingCart.setExternalChargesType(ChargeRateOperationType.MAKE_ONLY);
				Collection<EXTERNAL_CHARGES> externalCharges = new ArrayList<EXTERNAL_CHARGES>();
				externalCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
				bookingShoppingCart.updateExternalCharges(externalCharges);

			} else {
				if (new BigDecimal(paymentSummary.getBalanceToPay()).compareTo(BigDecimal.ZERO) > 0
						&& strStatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
					strDisplayStatus = strStatus + "(forced)";
				} else {
					// strDisplayStatus = strStatus;
					strDisplayStatus = reservation.getDisplayStatus();
				}
			}
			bookingInfo = ReservationUtil.loadBookingInfo(pnr, strStatus, ownerAgent,
					DateUtil.formatDate(reservation.getZuluBookingTimestamp(), CalendarUtil.PATTERN11),
					reservation.getVersion(), reservation.getTotalPaxAdultCount(), reservation.getTotalPaxChildCount(),
					reservation.getTotalPaxInfantCount(),
					(reservation.getMarketingCarrier().compareTo(AppSysParamsUtil.getDefaultCarrierCode()) == 0),
					isGroupPNR, reservation.getBookingCategory(), reservation.getOriginCountryOfCall(),
					reservation.getItineraryFareMask(),
					DateUtil.formatDate(reservation.getTicketValidTill(), CalendarUtil.PATTERN11),
					DateUtil.formatDate(reservation.getTicketValidFrom(), CalendarUtil.PATTERN11));
			bookingInfo.setDisplayStatus(strDisplayStatus);
			bookingInfo.setReleaseTime(DateUtil.formatDate(reservation.getZuluReleaseTimeStamp(), CalendarUtil.PATTERN11));
			bookingInfo.setUseEtsForReservation(reservation.isUseEtsForReservation());
			
			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);
			if (this.countryCurrExchangeRate != null) {
				paymentSummary.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
			}
			xbeData.setPaymentSummary(paymentSummary);

			boolean isPaymentCarrierSame = resInfo.getMarketingAirlineCode() == null ? true : resInfo.getMarketingAirlineCode()
					.equals(AppSysParamsUtil.getDefaultCarrierCode());
			boolean isReservationOnHold = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(resInfo
					.getReservationStatus());
			if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
					.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode()))
					&& !isPaymentCarrierSame
					&& isReservationOnHold) {
				PaymentSummaryTO tempPaymentSummary = xbeData.getPaymentSummary();
				BigDecimal totalPaymentAmount = new BigDecimal(tempPaymentSummary.getBalanceToPay());
				BigDecimal totalPaymentAmountExcludingCCCharge = new BigDecimal(tempPaymentSummary.getBalanceToPay());
				BigDecimal ccChargeAmount = getCCChargeAmount(totalPaymentAmountExcludingCCCharge);
				// need to fix
				// ccChargeAmount = ReservationUtil.addTaxToServiceCharge(bookingShopingCart, ccChargeAmount);
				totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, ccChargeAmount);
				tempPaymentSummary.setBalanceToPay(totalPaymentAmount.toString());
				xbeData.setPaymentSummary(tempPaymentSummary);
			}

			paxWiseAnci = ReservationUtil.createPaxWiseAnci(reservation.getPassengers(), rParam.isGoquoValid());

			if (strStatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
				reconcileFailedInsurances(reservation);
			}

			this.insurances = ReservationUtil.getInsurance(reservation.getReservationInsurances());
			String departureDates[] = ReservationUtil.getFirstLastFlightDepartureDates(reservation.getSegments());
			firstFlightDepartDate = departureDates[0];
			lastFlightDepartDate = departureDates[1];
			firstFlightDepartDateTime = departureDates[2];
			lastFlightArrivalDate = ReservationUtil.getFlightLastArrivalDate(reservation.getSegments());
			alertInfo = ReservationUtil.getSortedAlertInfo(reservation.getAlertInfoTOs());
			carrierGroupingOptions = ReservationUtil.createCarrierCodeOptions(reservation.getCarrierOndGrouping(), rParam);
			flexiAlertInfo = reservation.getFlexiAlertInfoTOs();
			originSalesChanel = reservation.getAdminInfo().getOriginChannelId();
			segWiseBindleAlertInfo = getSegmentWiseBundledFareInfo(reservation.getBundledFareDTOs(), reservation.getSegments());

			refundableChargeDetails = reservation.getRefundableChargeDetails();

			// If reservation is OHD reservation then calculate roll forward
			// start date
			if (strStatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD) && rParam.isAllowOnholdRollForward()) {
				isReservationRollForwardEnabled(reservation.getSegments());
				if (this.allowOnholdRollFoward) {
					calculateOnholdRollForwardStartDate(reservation);
				}
			}

			// Hide Stop Over Airport
			if (AppSysParamsUtil.isHideStopOverEnabled()) {
				hideStopOverAirport(flightDetails);
			}
			endorsementTxt = reservation.getEndorsementNote();
			if (AppSysParamsUtil.isCountryPaxConfigEnabled()) {
				String[] originDests = ReservationUtil.populateActualOriginAndDestination(reservation);
				actualOrigin = originDests[0];
				actualDestination = originDests[1];
			}
			if ((rParam.isAllowVoidReservation() || rParam.isAllowVoidOwnReservation()) && reservation.isAllowVoidReservation()) {
				allowVoidReservation = true;
			}

			boolean isAllExternalTicketsExchanged = isAllExternalTicketsExchanged(reservation);
			if (rParam.isAllowExternalTicketExchange()) {
				if (!isAllExternalTicketsExchanged) {
					rParam.setCancelReservation(false);
					rParam.setRequoteEnabled(false);
					rParam.setVoidReservation(false);
					allowVoidReservation = false;
					// rParam.setAllowCouponControlRequest(true);
				} else {
					rParam.setAllowExternalTicketExchange(false);
				}
			}
			
			if (reservation.getGdsId() != null) {
				gdsPnr = true;
			}

			gdsAllowAncillary = ReservationApiUtils.allowAddModifyAncillaryForGDS(reservation.getGdsId());

			if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
				allowNameChange = false;
				allowRequoteOverride = false;
			} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
				allowNameChange = isAllowOnholdNameChange(rParam);
				allowRequoteOverride = isAllowRequoteNameChange(rParam);
			} else {
				allowNameChange = isAllowNameChange(rParam, reservation.getNameChangeCount());
				allowRequoteOverride = isAllowRequoteNameChange(rParam);
				allowExtendedNameChange = rParam.isAllowExtendedNameChange();
			}
			setCodeShareInfo(reservation);

			infantPaymentSeparated = reservation.isInfantPaymentSeparated();
			serviceTaxApplied = isServiceTaxApplied(reservation.getSegments());
			handlingFeeApplicableForAdj = isHandlingFeeApplicableForChargeAdjustment();

			allowCreateUserAlert = rParam.isAllowCreateUserAlert();
			agentBalance = CommonUtil.getLoggedInAgentBalance(request);
			gdsId = reservation.getGdsId();
			if (gdsId != null) {
				csOCPnr = ReservationApiUtils.isCodeShareReservation(gdsId);
			}
			
			populateNoShowPassengerDetails(strCurrency, reservation.getPassengers());
			
			taxInvoices = reservation.getTaxInvoicesList();

			serviceTaxApplicableCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries();

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, String preferredLanguage,
			HttpServletRequest request, boolean recordAudit, String marketingAirlineCode, String airlineCode,
			Long interlineAgreementId, boolean skipPromoAdjustment) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
		} else {
			pnrModesDTO.setPnr(pnr);
		}

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadLocalTimes(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true); // baggage
		pnrModesDTO.setAppIndicator(ApplicationEngine.XBE);
		pnrModesDTO.setPreferredLanguage(preferredLanguage);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		pnrModesDTO.setLoadGOQUOAmounts(true);
		if (request != null) {
			pnrModesDTO.setMapPrivilegeIds((Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS));
		}
		pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
		pnrModesDTO.setInterlineAgreementId(interlineAgreementId);
		pnrModesDTO.setAirlineCode(airlineCode);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setSkipPromoAdjustment(skipPromoAdjustment);

		if (request != null) {
			pnrModesDTO.setLoadRefundableTaxInfo(
					BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_NOSHOW_REFUNDABLE_TAX_CALC));
			pnrModesDTO.setLoadClassifyUN(BasicRH.hasPrivilege(request, PriviledgeConstants.CLASSIFY_USER_NOTE));
		}
		return pnrModesDTO;

	}

	private Map<String, String> getUserDefinedSSRCharge(LCCClientReservation reservation){
		Map<String, String> paxWiseUserDefinedSSRChgMap = new HashMap<String,String>();
		Set<LCCClientReservationPax> paxSet = reservation.getPassengers();
		BigDecimal userDefinedSSRCharge = BigDecimal.ZERO;
		for (LCCClientReservationPax pax : paxSet) {
			List<LCCSelectedSegmentAncillaryDTO> selectedAnciList = pax.getSelectedAncillaries();
		    for (LCCSelectedSegmentAncillaryDTO anci : selectedAnciList) {
		    	for (LCCSpecialServiceRequestDTO ssr : anci.getSpecialServiceRequestDTOs()) {
		    		if (ssr.isUserDefinedCharge()){
		    			paxWiseUserDefinedSSRChgMap.put(pax.getTravelerRefNumber(), ssr.getCharge().toString());
		    			userDefinedSSRCharge = AccelAeroCalculator.add(userDefinedSSRCharge, ssr.getCharge());
		    		}
		    	}
		    }
		}
		paxWiseUserDefinedSSRChgMap.put(USER_DEFINED_SSR_TOTAL, userDefinedSSRCharge.toString());
		return paxWiseUserDefinedSSRChgMap;
	}

	private boolean isUserDefinedSSREnabled(LCCClientReservation reservation){
		Set<LCCClientReservationPax> paxSet = reservation.getPassengers();
		for (LCCClientReservationPax pax : paxSet) {
			List<LCCSelectedSegmentAncillaryDTO> selectedAnciList = pax.getSelectedAncillaries();
			for (LCCSelectedSegmentAncillaryDTO anci : selectedAnciList) {
				for (LCCSpecialServiceRequestDTO ssr : anci.getSpecialServiceRequestDTOs()) {

					if (ssr.isUserDefinedCharge()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void enableFFIDAddEdit(LCCClientReservation reservation) {
		List<LCCClientReservationSegment> segmentList = new ArrayList<>(reservation.getSegments());
		Collections.sort(segmentList);
		for (LCCClientReservationSegment segment : segmentList) {
			if (!ReservationSegmentSubStatus.EXCHANGED.equals(segment.getSubStatus())) {
				addFFID = segment.getModifyTillBufferDateTime().after(new Date()) && !segment.isFlownSegment() && !hasUnSegment;
				inEditFFIDBuffer = segment.getModifyTillBufferDateTime().after(new Date()) && !segment.isFlownSegment();
				if (!addFFID) {
					break;
				}
			}
		}
		if (!segmentList.isEmpty()) {
			LCCClientReservationSegment firstSegment = segmentList.get(0);
			if (firstSegment.getModifyTillBufferDateTime().after(new Date())) {
				addFFID = true;
				editFFID = true;
			}
		}
	}

	private String cleanedChannelId(String originString) {
		String cleanedId = null;
		if (!StringUtil.isNullOrEmpty(originString)) {
			String[] arr = originString.split("\\|");
			if (originString != null && originString.length() > 0) {
				cleanedId = arr[arr.length - 1];
			}

		}
		return cleanedId;
	}

	private static boolean hasUncancelledUnSegment(Set<LCCClientReservationSegment> segments) {
		boolean hasUnSegment = false;
		for (LCCClientReservationSegment segment : segments) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())
					&& segment.isUnSegment())
				hasUnSegment = true;

		}
		return hasUnSegment;

	}

	private void reconcileFailedInsurances(LCCClientReservation reservation) throws ModuleException {

		List<LCCClientReservationInsurance> clientInsurances = reservation.getReservationInsurances();
		List<Integer> failedInsuranceReferences = new ArrayList<Integer>();

		if (clientInsurances != null && !clientInsurances.isEmpty()) {
			for (LCCClientReservationInsurance clientInsurance : clientInsurances) {
				if (clientInsurance.getPolicyCode() == null && clientInsurance.getState() != null
						&& isPreviouslySoldInsurance(clientInsurance.getState())) {
					failedInsuranceReferences.add(Integer.parseInt(clientInsurance.getInsuranceQuoteRefNumber()));
				}
			}

			if (!failedInsuranceReferences.isEmpty()) {
				SYSTEM system = (reservation.isGroupPNR() ? SYSTEM.INT : SYSTEM.AA);
				clientInsurances = ModuleServiceLocator.getAirproxyReservationBD().resellInsurance(system, reservation.getPNR(),
						failedInsuranceReferences);
				reservation.setReservationInsurances(clientInsurances);
			}
		}
	}

	private void setCodeShareInfo(LCCClientReservation reservation) {
		for (LCCClientReservationSegment lccClientReservationSegment : reservation.getSegments()) {
			if (lccClientReservationSegment.getCsOcCarrierCode() != null) {
				csPnr = true;
				break;
			}
		}
	}

	private boolean isPreviouslySoldInsurance(String state) {
		boolean previouslySold = false;
		previouslySold = ReservationInternalConstants.INSURANCE_STATES.SC.toString().equals(state)
				|| ReservationInternalConstants.INSURANCE_STATES.FL.toString().equals(state)
				|| ReservationInternalConstants.INSURANCE_STATES.RQ.toString().equals(state)
				|| ReservationInternalConstants.INSURANCE_STATES.TO.toString().equals(state);

		return previouslySold;
	}

	private Map<String, BundledFareDTO> getSegmentWiseBundledFareInfo(List<BundledFareDTO> bundledFareDtos,
			Set<LCCClientReservationSegment> resSegments) {
		Map<String, BundledFareDTO> segmentWiseBundledFares = new HashMap<String, BundledFareDTO>();
		for (LCCClientReservationSegment lccSegment : resSegments) {
			if (bundledFareDtos != null) {
				for (BundledFareDTO bundledFareDTO : bundledFareDtos) {
					if (bundledFareDTO != null) {
						if (bundledFareDTO.getBundledFarePeriodId().equals(lccSegment.getBundledServicePeriodId())) {
							segmentWiseBundledFares.put(lccSegment.getPnrSegID().toString(), bundledFareDTO);
						}
					}
				}
			}
		}
		return segmentWiseBundledFares;
	}

	private NameChangeParameters getNameModificationParams(LCCClientReservation lccReservation) throws ModuleException {

		boolean isBeforeBufferTime = false, isWithinBufferTime = false, creditUtilizedPnr = false;

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String agentStation = userPrincipal.getAgentStation();
		long nameChangeCutoffTime = AppSysParamsUtil.getPassenegerNameModificationCutoffTimeInMillis();
		String agentStationCutoff = NameChangeUtil.populateStationWiseNameChangeInfo(ModuleServiceLocator.getLocationServiceBD(),
				agentStation, lccReservation);

		long nameChangeCutoffPerStation = AppSysParamsUtil.getTimeInMillis(agentStationCutoff);

		if (nameChangeCutoffTime > nameChangeCutoffPerStation) {
			nameChangeCutoffTime = nameChangeCutoffPerStation;
		}

		for (LCCClientReservationSegment segment : SortUtil.sortByDepDate(lccReservation.getSegments())) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus())) {
				long timeDiff = segment.getDepartureDateZulu().getTime() - CalendarUtil.getCurrentZuluDateTime().getTime();
				if (timeDiff > nameChangeCutoffPerStation) {
					isBeforeBufferTime = true;
				} else if (nameChangeCutoffPerStation >= timeDiff && timeDiff > nameChangeCutoffTime) {
					isWithinBufferTime = true;
				}
				break;
			}
		}

		for (LCCClientReservationPax pax : lccReservation.getPassengers()) {
			LCCClientPaymentHolder paymentHolder = pax.getLccClientPaymentHolder();
			Collection<LCCClientPaymentInfo> payments = paymentHolder.getPayments();
			for (LCCClientPaymentInfo payment : payments) {
				if (payment instanceof LCCClientPaxCreditPaymentInfo) {
					LCCClientPaxCreditPaymentInfo payInfo = (LCCClientPaxCreditPaymentInfo) payment;
					if (ReservationTnxNominalCode.getDescription(6).equals(payInfo.getDescription())) {
						creditUtilizedPnr = true;
					}
				}
			}
		}

		NameChangeParameters params = new NameChangeParameters();
		params.isBeforeBufferTime = isBeforeBufferTime;
		params.isWithinBufferTime = isWithinBufferTime;
		params.isCreditUtilizedBooking = creditUtilizedPnr;
		params.isFlownSegmentExists = rParam.isFlownSegmentExists();

		return params;
	}

	/**
	 * Builds a passenger wise payment details map.
	 * 
	 * @param paxList
	 *            List of passengers whose payment details need to be sorted.
	 * @return a Map<String,LCCCLientPaymentHolder> with key -> travellerRefNumber value -> passenger payment details
	 */
	@SuppressWarnings("unchecked")
	private Map<String, PaxAccountPaymentsTO> buildPerPaxPaymentDetails(Collection<LCCClientReservationPax> paxList, boolean isInfantPaymentSeparated)
			throws ModuleException {
		Map<String, PaxAccountPaymentsTO> resultMap = new TreeMap<String, PaxAccountPaymentsTO>();

		Map<String, String> mapAllAgentCodeDesc = XBEModuleUtils.getGlobalConfig().getAgentsByAgentCode();
		for (LCCClientReservationPax pax : paxList) {
			if (!isInfantPaymentSeparated && PaxTypeTO.INFANT.equalsIgnoreCase(pax.getPaxType())) {
				continue;
			}

			resultMap.put(pax.getTravelerRefNumber() + "-" + pax.getTitle() + " " + pax.getFirstName() + " " + pax.getLastName(),
					PaxAccountUtil.getPaymentDetails(pax, pax.getStatus(), mapAllAgentCodeDesc, true));
		}
		return resultMap;
	}

	private Map<String, String> buildPerPaxCreditDetails(Collection<LCCClientReservationPax> paxList, boolean isInfantPaymentSeparated) throws ModuleException {
		Map<String, String> resultMap = new HashMap<String, String>();
		for (LCCClientReservationPax pax : paxList) {
			if (!isInfantPaymentSeparated && PaxTypeTO.INFANT.equalsIgnoreCase(pax.getPaxType())) {
				continue;
			}

			resultMap.put(pax.getTravelerRefNumber() + "-" + pax.getTitle() + " " + pax.getFirstName() + " " + pax.getLastName(),

					(pax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) < 0)
							? AccelAeroCalculator.formatAsDecimal(pax.getTotalAvailableBalance().negate())
							: "-");
		}
		return resultMap;
	}

	private void calculateOnholdRollForwardStartDate(LCCClientReservation reservation) throws ModuleException {

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

		LCCClientReservationSegment firstSegment = ReservationUtil.getFirstFlightSeg(reservation.getSegments());

		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		onHoldReleaseTimeDTO.setBookingDate(new Date());
		onHoldReleaseTimeDTO.setOndCode(firstSegment.getSegmentCode());
		onHoldReleaseTimeDTO.setFlightDepartureDate(firstSegment.getDepartureDateZulu());
		onHoldReleaseTimeDTO.setDomestic(firstSegment.isDomesticFlight());
		onHoldReleaseTimeDTO.setCabinClass(firstSegment.getCabinClassCode());
		onHoldReleaseTimeDTO.setBookingClass(firstSegment.getFareTO().getBookingClassCode());
		onHoldReleaseTimeDTO.setModuleCode(AppIndicatorEnum.APP_XBE.toString());
		onHoldReleaseTimeDTO.setAgentCode(userPrincipal.getAgentCode());
		onHoldReleaseTimeDTO.setHasBuffertimeOhd(rParam.isBufferTimeOnHold());
		onHoldReleaseTimeDTO.setFareRuleId(firstSegment.getFareTO().getFareRuleID());

		long ohdBufferInMillis = ReleaseTimeUtil.getOnholdBufferTime(userPrincipal.getAgentCode(),
				AppIndicatorEnum.APP_XBE.toString(), onHoldReleaseTimeDTO);

		Date currentDate = DateUtil.getCurrentZuluDateTime();
		long currentDateInMillis = currentDate.getTime();

		Date firstDepartDate = firstSegment.getDepartureDateZulu();
		Date minAllowed = CalendarUtil.add(firstDepartDate, 0, 0, 1, 0, 0, 0);
		long minAllowedInMillis = minAllowed.getTime();

		Calendar cal = Calendar.getInstance();

		if ((minAllowedInMillis - currentDateInMillis) >= ohdBufferInMillis) {
			cal.setTimeInMillis(minAllowedInMillis);
		} else {
			cal.setTimeInMillis(currentDateInMillis + ohdBufferInMillis);
		}

		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd/MM/yyyy");
		this.ohdRollForwardStartDate = smpdtDate.format(cal.getTime());
	}

	private void hideStopOverAirport(Collection<FlightInfoTO> flightDetails) {
		for (FlightInfoTO flightInfoTo : flightDetails) {
			String flightOnd = flightInfoTo.getOrignNDest();
			if (flightOnd != null && flightOnd.split("/").length > 2) {
				flightInfoTo.setOrignNDest(ReservationApiUtils.hideStopOverSeg(flightOnd));
			}
		}
	}

	/**
	 * If reservation has any canceled segment or segments from multiple cabin classes, system won't allow to roll
	 * forward such reservations.
	 * 
	 * @param resSegments
	 */
	private void isReservationRollForwardEnabled(Set<LCCClientReservationSegment> resSegments) {
		Set<String> cabinClassSet = new HashSet<String>();
		for (LCCClientReservationSegment lccSeg : resSegments) {
			cabinClassSet.add(lccSeg.getCabinClassCode());
			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(lccSeg.getStatus())) {
				rollForwardErrorCode = "XBE-ERR-82";
				allowOnholdRollFoward = false;
				break;
			}
		}

		if (cabinClassSet.size() > 1) {
			rollForwardErrorCode = "XBE-ERR-83";
			allowOnholdRollFoward = false;
		}
	}

	private boolean isWaitListedSegmentExists(Set<LCCClientReservationSegment> resSegments) {
		boolean isWaitListed = false;
		for (LCCClientReservationSegment lccSeg : resSegments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING.equals(lccSeg.getStatus())) {
				isWaitListed = true;
				break;
			}
		}

		return isWaitListed;
	}

	private String setDiscountAmount(LCCClientReservation reservation) {
		String totalDiscount;
		if (reservation.getLccPromotionInfoTO() != null
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT
						.equals(reservation.getLccPromotionInfoTO().getDiscountAs())
				&& reservation.getLccPromotionInfoTO().getCreditDiscountAmount() != null) {
			totalDiscount = AccelAeroCalculator.formatAsDecimal(reservation.getLccPromotionInfoTO().getCreditDiscountAmount());
		} else {
			totalDiscount = AccelAeroCalculator.formatAsDecimal(reservation.getTotalDiscount());
		}
		return totalDiscount;
	}
	
	private void populateNoShowPassengerDetails(String strCurrency, Set<LCCClientReservationPax> passengers) throws ModuleException {

		TaxReversalDTO taxReversalDTO = new TaxReversalDTO();
		taxReversalDTO.setNoShowPaxDetails(new ArrayList<NoShowPassenger>());
		NoShowPassenger noShowPassenger;
		BigDecimal paxTaxTotal;
		Map<String, NoShowPassenger> noShowPaxMAp = new HashMap<String, NoShowPassenger>();

		for (LCCClientReservationPax lccClientReservationPax : passengers) {
			noShowPassenger = null;
			//paxTaxTotal = calculateNoShowRefundableAmounts(lccClientReservationPax);
			paxTaxTotal = lccClientReservationPax.getNoShowRefundableAmount() == null ? AccelAeroCalculator.getDefaultBigDecimalZero() : lccClientReservationPax.getNoShowRefundableAmount();
			if (!PaxTypeTO.INFANT.equals(lccClientReservationPax.getPaxType())) {
				if (!noShowPaxMAp.containsKey(lccClientReservationPax.getTravelerRefNumber())) {
					noShowPaxMAp.put(lccClientReservationPax.getTravelerRefNumber(), new NoShowPassenger());
				} else {
					paxTaxTotal = AccelAeroCalculator
							.add(paxTaxTotal,
									noShowPaxMAp.get(lccClientReservationPax.getTravelerRefNumber()).getRefundableTaxTotal() != null ? noShowPaxMAp
											.get(lccClientReservationPax.getTravelerRefNumber()).getRefundableTaxTotal()
											: AccelAeroCalculator.getDefaultBigDecimalZero());
				}
				noShowPassenger = noShowPaxMAp.get(lccClientReservationPax.getTravelerRefNumber());
				noShowPassenger.setPaxType(lccClientReservationPax.getPaxType());
				noShowPassenger.setTravellerRef(lccClientReservationPax.getTravelerRefNumber());
				noShowPassenger.setPassengerName(lccClientReservationPax.getTitle() == null ? "" : lccClientReservationPax
						.getTitle()
						+ " "
						+ lccClientReservationPax.getFirstName()
						+ " "
						+ lccClientReservationPax.getLastName());
				noShowPassenger.setRefundableTaxTotal(paxTaxTotal);
				noShowPassenger
						.setRefundable(noShowPassenger.isRefundable() || lccClientReservationPax.isNoShowRefundable());

			} else {
				if (lccClientReservationPax.getParent() != null
						&& lccClientReservationPax.getParent().getTravelerRefNumber() != null) {
					if (!noShowPaxMAp.containsKey(lccClientReservationPax.getParent().getTravelerRefNumber())) {
						noShowPaxMAp.put(lccClientReservationPax.getParent().getTravelerRefNumber(), new NoShowPassenger());
					}
					noShowPassenger = noShowPaxMAp.get(lccClientReservationPax.getParent().getTravelerRefNumber());
					if (AccelAeroCalculator.getDefaultBigDecimalZero().compareTo(paxTaxTotal) < 0) {
						noShowPassenger.setRefundableTaxTotal(AccelAeroCalculator.add(
								noShowPassenger.getRefundableTaxTotal() != null ? noShowPassenger.getRefundableTaxTotal()
										: AccelAeroCalculator.getDefaultBigDecimalZero(), paxTaxTotal));
						noShowPassenger.setRefundable(lccClientReservationPax.isNoShowRefundable());
						noShowPassenger.setInfantName(lccClientReservationPax.getFirstName() + " "
								+ lccClientReservationPax.getLastName());
					}
				}
			}
		}

		for (NoShowPassenger noShowPassengerRef : noShowPaxMAp.values()) {
			taxReversalDTO.getNoShowPaxDetails().add(noShowPassengerRef);
		}

		taxReversalDTO.setCurrencyCode(strCurrency);
		taxReversalDTO.setGroupPNR(ReservationUtil.isGroupPnr(groupPNR));
		taxReversalDTO.setPnr(pnr);
		taxReversalDTO.setHasRefundablePax(isNoShowPaxExistWithRefundables(taxReversalDTO.getNoShowPaxDetails()));

		setTaxReversalDto(taxReversalDTO);
	}
	
	private boolean isNoShowPaxExistWithRefundables(List<NoShowPassenger> lstNoShowPassenger) {
		boolean noShowPaxExist = false;
		for (NoShowPassenger noShowPassenger : lstNoShowPassenger) {
			if (noShowPassenger.isRefundable() && noShowPassenger.getRefundableTaxTotal() != null
					&& noShowPassenger.getRefundableTaxTotal().doubleValue() > 0) {
				noShowPaxExist = true;
				break;
			}
		}
		return noShowPaxExist;
	}

	private boolean isServiceTaxApplied(Set<LCCClientReservationSegment> resSegments){
		
		if(resSegments != null){
			
			String originCountry = "";
			LCCClientReservationSegment firstSegment = ReservationUtil.getFirstFlightSeg(resSegments);
			String[] countryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			String originCode = "";
			
			if (firstSegment != null && firstSegment.getSegmentCode() != null && firstSegment.getSegmentCode().length() > 2) {
				originCode = firstSegment.getSegmentCode().split("/")[0];
			}
			
			if(originCode.isEmpty()){
				return false;
			}
			
			if (SYSTEM.INT.toString().equals(system)) {
				try {
					Map<String, CachedAirportDTO> mapAirports = ModuleServiceLocator.getAirportBD()
							.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(originCode)));
					originCountry = mapAirports.get(originCode) != null ? mapAirports.get(originCode).getCountryCode() : "";

				} catch (ModuleException e) {
					log.error("ERROR in loading country code for the origin airport", e);
					return false;
				}
			} else {
				Object[] airportInfo = XBEModuleUtils.getGlobalConfig().retrieveAirportInfo(originCode);
				if (airportInfo != null) {
					originCountry = (String) airportInfo[3];
				}
			}
			
			for(String countryCode : countryCodes){
				if(originCountry.equals(countryCode)){
					return true;
				}
			}

		}
		
		return false;
		
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Collection<AdultPaxTO> getPaxAdults() {
		return paxAdults;
	}

	public void setPaxAdults(Collection<AdultPaxTO> paxAdults) {
		this.paxAdults = paxAdults;
	}

	public Collection<InfantPaxTO> getPaxInfants() {
		return paxInfants;
	}

	public void setPaxInfants(Collection<InfantPaxTO> paxInfants) {
		this.paxInfants = paxInfants;
	}

	public Collection<PaxFareTO> getPaxPrice() {
		return paxPrice;
	}

	public void setPaxPrice(Collection<PaxFareTO> paxPrice) {
		this.paxPrice = paxPrice;
	}

	public Collection<FlightInfoTO> getFlightDetails() {
		return flightDetails;
	}

	public void setFlightDetails(Collection<FlightInfoTO> flightDetails) {
		this.flightDetails = flightDetails;
	}

	public BookingTO getBookingInfo() {
		return bookingInfo;
	}

	public void setBookingInfo(BookingTO bookingInfo) {
		this.bookingInfo = bookingInfo;
	}

	public String getUserNotes() {
		return userNotes;
	}

	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	public PaymentSummaryTO getPaxPaymentSummary() {
		return paxPaymentSummary;
	}

	public void setPaxPaymentSummary(PaymentSummaryTO paxPaymentSummary) {
		this.paxPaymentSummary = paxPaymentSummary;
	}

	public Collection<PaymentPassengerTO> getPaxPaymentTO() {
		return paxPaymentTO;
	}

	public void setPaxPaymentTO(Collection<PaymentPassengerTO> paxPaymentTO) {
		this.paxPaymentTO = paxPaymentTO;
	}

	public PaymentSummaryTO getPaymentSummary() {
		return paymentSummary;
	}

	public void setPaymentSummary(PaymentSummaryTO paymentSummary) {
		this.paymentSummary = paymentSummary;
	}

	public Collection<AncillaryPaxTO> getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(Collection<AncillaryPaxTO> paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public String getFirstFlightDepartDate() {
		return firstFlightDepartDate;
	}

	public void setFirstFlightDepartDate(String firstFlightDepartDate) {
		this.firstFlightDepartDate = firstFlightDepartDate;
	}

	public List<LCCClientAlertInfoTO> getAlertInfo() {
		return alertInfo;
	}

	public void setAlertInfo(List<LCCClientAlertInfoTO> alertInfo) {
		this.alertInfo = alertInfo;
	}

	public List<LCCClientReservationInsurance> getInsurances() {
		return insurances;
	}

	public void setInsurances(List<LCCClientReservationInsurance> insurances) {
		this.insurances = insurances;
	}

	public ReservationProcessParams getrParam() {
		return rParam;
	}

	public void setrParam(ReservationProcessParams rParam) {
		this.rParam = rParam;
	}

	public String getCarrierGroupingOptions() {
		return carrierGroupingOptions;
	}

	public void setCarrierGroupingOptions(String carrierGroupingOptions) {
		this.carrierGroupingOptions = carrierGroupingOptions;
	}

	public XBESessionDTO getXbeData() {
		return xbeData;
	}

	public void setXbeData(XBESessionDTO xbeData) {
		this.xbeData = xbeData;
	}

	public String getForwardmessage() {
		return forwardmessage;
	}

	public void setForwardmessage(String forwardmessage) {
		this.forwardmessage = forwardmessage;
	}

	public boolean isLoadAudit() {
		return loadAudit;
	}

	public void setLoadAudit(boolean loadAudit) {
		this.loadAudit = loadAudit;
	}

	public List<LCCClientAlertInfoTO> getFlexiAlertInfo() {
		return flexiAlertInfo;
	}

	public void setFlexiAlertInfo(List<LCCClientAlertInfoTO> flexiAlertInfo) {
		this.flexiAlertInfo = flexiAlertInfo;
	}

	public String getOhdAmount() {
		return ohdAmount;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public String getOriginSalesChanel() {
		return originSalesChanel;
	}

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		if (marketingAirlineCode != null && !marketingAirlineCode.trim().equals("null")
				&& !marketingAirlineCode.trim().equals("undefined")) {
			this.marketingAirlineCode = marketingAirlineCode;
		}
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		if (airlineCode != null && !airlineCode.trim().equals("null") && !airlineCode.trim().equals("undefined")) {
			this.airlineCode = airlineCode;
		}
	}

	public Long getInterlineAgreementId() {
		return interlineAgreementId;
	}

	public void setInterlineAgreementId(Long interlineAgreementId) {
		this.interlineAgreementId = interlineAgreementId;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public boolean isPrintEmailIndItinerary() {
		return printEmailIndItinerary;
	}

	public void setPrintEmailIndItinerary(boolean printEmailIndItinerary) {
		this.printEmailIndItinerary = printEmailIndItinerary;
	}

	public int getNoOfCnfSegments() {
		return noOfCnfSegments;
	}

	public void setNoOfCnfSegments(int noOfCnfSegments) {
		this.noOfCnfSegments = noOfCnfSegments;
	}

	public String getEndorsementTxt() {
		return endorsementTxt;
	}

	public void setEndorsementTxt(String endorsementTxt) {
		this.endorsementTxt = endorsementTxt;
	}

	/**
	 * @return the gdsPnr
	 */
	public boolean isGdsPnr() {
		return gdsPnr;
	}

	/**
	 * @param gdsPnr
	 *            the gdsPnr to set
	 */
	public void setGdsPnr(boolean gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	
	public boolean isGdsAllowAncillary() {
		return gdsAllowAncillary;
	}

	public void setGdsAllowAncillary(boolean gdsAllowAncillary) {
		this.gdsAllowAncillary = gdsAllowAncillary;
	}

	public String getActualOrigin() {
		return actualOrigin;
	}

	public void setActualOrigin(String actualOrigin) {
		this.actualOrigin = actualOrigin;
	}

	public String getActualDestination() {
		return actualDestination;
	}

	public void setActualDestination(String actualDestination) {
		this.actualDestination = actualDestination;
	}

	public void setVoidReservation(boolean isVoidReservation) {
		this.isVoidReservation = isVoidReservation;
	}

	public boolean isVoidReservation() {
		return isVoidReservation;
	}

	public String getOhdRollForwardStartDate() {
		return ohdRollForwardStartDate;
	}

	public void setOhdRollForwardStartDate(String ohdRollForwardStartDate) {
		this.ohdRollForwardStartDate = ohdRollForwardStartDate;
	}

	public boolean isAllowOnholdRollFoward() {
		return allowOnholdRollFoward;
	}

	public void setAllowOnholdRollFoward(boolean allowOnholdRollFoward) {
		this.allowOnholdRollFoward = allowOnholdRollFoward;
	}

	public String getRollForwardErrorCode() {
		return rollForwardErrorCode;
	}

	public void setRollForwardErrorCode(String rollForwardErrorCode) {
		this.rollForwardErrorCode = rollForwardErrorCode;
	}

	public String getLastFlightDepartDate() {
		return lastFlightDepartDate;
	}

	public String getRefundableChargeDetails() {
		try {
			return JSONUtil.serialize(refundableChargeDetails);
		} catch (JSONException e) {
			return null;
		}
	}

	public boolean isAllowVoidReservation() {
		return allowVoidReservation;
	}

	public void setAllowVoidReservation(boolean allowVoidReservation) {
		this.allowVoidReservation = allowVoidReservation;
	}

	public Map<String, PaxAccountPaymentsTO> getPerPaxPaymentDetails() {
		return perPaxPaymentDetails;
	}

	public Map<String, String> getPerPaxCreditDetails() {
		return perPaxCreditDetails;
	}

	public Set<Integer> getApplicablBINs() {
		return applicablBINs;
	}

	public String getLastFlightArrivalDate() {
		return lastFlightArrivalDate;
	}

	public void setLastFlightArrivalDate(String lastFlightArrivalDate) {
		this.lastFlightArrivalDate = lastFlightArrivalDate;
	}

	public boolean isAllowCSVDetailUpload() {
		return allowCSVDetailUpload;
	}

	public void setAllowCSVDetailUpload(boolean allowCSVDetailUpload) {
		this.allowCSVDetailUpload = allowCSVDetailUpload;
	}

	public boolean isAllowCreateUserAlert() {
		return allowCreateUserAlert;
	}

	public void setAllowCreateUserAlert(boolean allowCreateUserAlert) {
		this.allowCreateUserAlert = allowCreateUserAlert;
	}

	public String getAgentBalance() {
		return agentBalance;
	}

	public void setAgentBalance(String agentBalance) {
		this.agentBalance = agentBalance;
	}

	public String getFirstFlightDepartDateTime() {
		return firstFlightDepartDateTime;
	}

	public boolean isGroupBookingRequest() {
		return isGroupBookingRequest;
	}

	public void setGroupBookingRequest(boolean isGroupBookingRequest) {
		this.isGroupBookingRequest = isGroupBookingRequest;
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	private boolean isAllowNameChange(ReservationProcessParams rParam, int nameChangeCount) {
		boolean enableNameChange = false;
		boolean avoidNameChangeCountCheck = true;
		boolean allowExtendedNameChange = rParam.isExtendedNameChangeEnabled();
		if (allowExtendedNameChange) {
			avoidNameChangeCountCheck = true;
		} else if (isAllowRequoteNameChange(rParam)) {
			avoidNameChangeCountCheck = true;
		} else {
			avoidNameChangeCountCheck = nameChangeCount < rParam.getNameChangeMaxCount();
		}

		if (rParam != null && rParam.isNameChange() && rParam.isEnableFareRuleNCC() && avoidNameChangeCountCheck) {
			if (allowExtendedNameChange) {
				enableNameChange = true;
			} else if (rParam.isFlownSegmentExists()) {
				if (rParam.isEnableFareRuleNCCForFlown()) {
					enableNameChange = true;
				}
			} else {
				enableNameChange = true;
			}
		}
		return enableNameChange;
	}

	private boolean isAllowRequoteNameChange(ReservationProcessParams rParam) {
		return rParam.isEnableFareRuleNCC() && rParam.isAllowOverrideRequoteNameChange();
	}

	private boolean isAllowOnholdNameChange(ReservationProcessParams rParam) {
		return rParam.isOnholdNameChange() && rParam.isEnableFareRuleNCC();
	}

	public boolean isAllowNameChange() {
		return allowNameChange;
	}

	public void setAllowNameChange(boolean allowNameChange) {
		this.allowNameChange = allowNameChange;
	}

	public boolean isAllowExtendedNameChange() {
		return allowExtendedNameChange;
	}

	public void setAllowExtendedNameChange(boolean allowExtendedNameChange) {
		this.allowExtendedNameChange = allowExtendedNameChange;
	}

	public Map<String, BundledFareDTO> getSegWiseBindleAlertInfo() {
		return segWiseBindleAlertInfo;
	}

	public void setSegWiseBindleAlertInfo(Map<String, BundledFareDTO> segWiseBindleAlertInfo) {
		this.segWiseBindleAlertInfo = segWiseBindleAlertInfo;
	}

	private boolean isAllExternalTicketsExchanged(LCCClientReservation reservation) {
		boolean hasCouponControl;
		GDSStatusTO gdsStatusTO;
		if (reservation.getGdsId() != null && ReservationApiUtils.allowExternalTicketExchange(reservation.getGdsId())
				&& reservation.getPassengers() != null && !reservation.getPassengers().isEmpty()) {

			gdsStatusTO = CommonsServices.getGlobalConfig().getActiveGdsMap().get(String.valueOf(reservation.getGdsId()));

			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				for (LccClientPassengerEticketInfoTO paxEticket : pax.geteTickets()) {

					hasCouponControl = paxEticket.getExternalPaxETicketNo() != null && paxEticket.getExternalCouponNo() != null
							&& ((gdsStatusTO.isAaValidatingCarrier() && paxEticket.getExternalCouponControl()
									.equals(ReservationPaxFareSegmentETicket.ExternalCouponControl.VALIDATING_CARRIER))
									|| (!gdsStatusTO.isAaValidatingCarrier() && paxEticket.getExternalCouponControl()
											.equals(ReservationPaxFareSegmentETicket.ExternalCouponControl.OPERATING_CARRIER)));

					if (!EticketStatus.FLOWN.code().equals(paxEticket.getPaxETicketStatus())
							&& !ReservationSegmentStatus.CANCEL.equals(paxEticket.getSegmentStatus()) && hasCouponControl) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private BigDecimal getCCChargeAmount(BigDecimal totalPaymentAmountExcludingCCCharge) throws ModuleException {
		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
				S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		ExternalChgDTO externalChgCCDTO = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD);
		externalChgCCDTO = (ExternalChgDTO) externalChgCCDTO.clone();

		if (externalChgCCDTO.getAmount() == null || BigDecimal.ZERO.equals(externalChgCCDTO.getAmount())) {
			if (externalChgCCDTO.isRatioValueInPercentage()) {
				externalChgCCDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
			}
		}

		return ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, externalChgCCDTO.getAmount());
	}

	public boolean isCsPnr() {
		return csPnr;
	}

	public void setCsPnr(boolean csPnr) {
		this.csPnr = csPnr;
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public boolean isCsOCPnr() {
		return csOCPnr;
	}

	public void setCsOCPnr(boolean csOCPnr) {
		this.csOCPnr = csOCPnr;
	}

	public boolean isAllowRequoteOverride() {
		return allowRequoteOverride;
	}

	public void setAllowRequoteOverride(boolean allowRequoteOverride) {
		this.allowRequoteOverride = allowRequoteOverride;
	}

	public boolean isEditFFID() {
		return inEditFFIDBuffer && !hasUnSegment
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_EDIT_FFID);
	}

	public boolean isAddFFID() {
		return addFFID;
	}

	public void setEditFFID(boolean editFFID) {
		this.editFFID = editFFID;
	}

	public void setAddFFID(boolean addFFID) {
		this.addFFID = addFFID;
	}

	public boolean isInEditFFIDBuffer() {
		return inEditFFIDBuffer;
	}

	public void setInEditFFIDBuffer(boolean inEditFFIDBuffer) {
		this.inEditFFIDBuffer = inEditFFIDBuffer;
	}

	public boolean getInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}
	
	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}

	public TaxReversalDTO getTaxReversalDto() {
		return taxReversalDto;
	}
	

	public void setTaxReversalDto(TaxReversalDTO taxReversalDto) {
		this.taxReversalDto = taxReversalDto;
	}

	public List<TaxInvoice> getTaxInvoices() {
		return taxInvoices;
	}

	public void setTaxInvoices(List<TaxInvoice> taxInvoices) {
		this.taxInvoices = taxInvoices;
	}
	
	public String getServiceTaxApplicableCountries() {
		return serviceTaxApplicableCountries;
	}

	public void setServiceTaxApplicableCountries(String serviceTaxApplicableCountries) {
		this.serviceTaxApplicableCountries = serviceTaxApplicableCountries;
	}
	public boolean isServiceTaxApplied() {
		return serviceTaxApplied;
	}

	public void setServiceTaxApplied(boolean serviceTaxApplied) {
		this.serviceTaxApplied = serviceTaxApplied;
	}

	public List<FlightSegmentTO> getFlightSegmentToList() {
		return flightSegmentToList;
	}

	public void setFlightSegmentToList(List<FlightSegmentTO> flightSegmentToList) {
		this.flightSegmentToList = flightSegmentToList;
	}

	public LCCClientReservation setFlagToDisplayOnXBE(LCCClientReservation reservation) {
		List<LCCClientAlertInfoTO> alertInfoTOs = reservation.getAlertInfoTOs();
		if (alertInfoTOs != null) {
			List<LCCClientAlertInfoTO> alertInfoTOsList = new ArrayList<LCCClientAlertInfoTO>();
			List<LCCClientAlertTO> LCCClientAlertTOList = new ArrayList<LCCClientAlertTO>();

			for (LCCClientAlertInfoTO lCCClientAlertInfoTO : alertInfoTOs) {
				LCCClientAlertTOList = lCCClientAlertInfoTO.getAlertTO();
				if (LCCClientAlertTOList.size() > 0) {
					LCCClientAlertTO lCCClientAlertTO = LCCClientAlertTOList.get(LCCClientAlertTOList.size() - 1);
					if (!("Y").equals(lCCClientAlertTO.getIbeVisibleOnly())) {
						lCCClientAlertInfoTO.setAlertActionOnXBE(true);
					} else {
						lCCClientAlertInfoTO.setAlertActionOnXBE(false);
					}
				} else {
					lCCClientAlertInfoTO.setAlertActionOnXBE(false);
				}
				alertInfoTOsList.add(lCCClientAlertInfoTO);
			}
			reservation.setAlertInfoTOs(alertInfoTOsList);
			return reservation;
		} else {
			return reservation;
		}
	}

	private boolean isHandlingFeeApplicableForChargeAdjustment() throws ModuleException {
		if (AppSysParamsUtil.applyHandlingFeeOnModification()) {
			int operationMode = CommonsConstants.ModifyOperation.CHARGE_ADJ.getOperation();
			ExternalChgDTO handlingChgDTO = ModuleServiceLocator.getReservationBD().getHandlingFeeForModification(operationMode);

			if (handlingChgDTO != null && handlingChgDTO.getAmount().doubleValue() > 0) {
				return true;
			}
		}
		return false;
	}

	public boolean isHandlingFeeApplicableForAdj() {
		return handlingFeeApplicableForAdj;
	}

	public void setHandlingFeeApplicableForAdj(boolean handlingFeeApplicableForAdj) {
		this.handlingFeeApplicableForAdj = handlingFeeApplicableForAdj;
	}

	public boolean isUserDefinedSSR() {
		return isUserDefinedSSR;
	}

	public void setUserDefinedSSR(boolean isUserDefinedSSR) {
		this.isUserDefinedSSR = isUserDefinedSSR;
	}

	public Map<String, String> getPaxWiseUserDefinedChgMap() {
		return paxWiseUserDefinedChgMap;
	}

	public void setPaxWiseUserDefinedChgMap(Map<String, String> paxWiseUserDefinedChgMap) {
		this.paxWiseUserDefinedChgMap = paxWiseUserDefinedChgMap;
	}

	private void validateLoadReservation(String bookingCategory, String reservationOwnerAgent, String userAgent, String userAgentTypeCode) throws ModuleException {
		Collection<String> agents = null;
		//for interline(Marketing carrier|agent,Operating carrier|agent) and dry(Operating carrier|agent) reservation's owner agent code
		if (!StringUtil.isNullOrEmpty(reservationOwnerAgent) && reservationOwnerAgent.contains("|")) {
			reservationOwnerAgent = reservationOwnerAgent.substring(reservationOwnerAgent.lastIndexOf("|") + 1);
		}
		if (!BasicRH.hasPrivilege(request, PriviledgeConstants.ANY_PNR_SEARCH)) {
			if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_SEARCH_ANY_HR_BOOKING)
					&& CommonsConstants.BookingCategory.HR.getCode().equals(bookingCategory)) {
				return;
			} else if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_SEARCH_ANY_CHARTER_BOOKING)
					&& CommonsConstants.BookingCategory.CHARTER.getCode().equals(bookingCategory)) {
				return;
			} else if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_SEARCH_ANY_STANDARD_BOOKING)
					&& CommonsConstants.BookingCategory.STANDARD.getCode().equals(bookingCategory)) {
				return;
			}
			if (!StringUtil.isNullOrEmpty(reservationOwnerAgent)) {
				if (BasicRH.hasPrivilege(request, PriviledgeConstants.ALL_REPORTING_AGENT_PNR_SEARCH)) {
					agents = ModuleServiceLocator.getSecurityBD().getAllReportingAgentCodes(userAgent, true);
				} else if (BasicRH.hasPrivilege(request, PriviledgeConstants.REPORTING_AGENT_PNR_SEARCH)) {
					agents = ModuleServiceLocator.getSecurityBD().getAllReportingAgentCodes(userAgent, false);
				} else if (BasicRH.hasPrivilege(request, PriviledgeConstants.ANY_GSA_PNR_SEARCH)
						&& AgentType.GSA.equals(userAgentTypeCode)) {
					agents = ModuleServiceLocator.getSecurityBD().getAllReportingAgentCodes(userAgent, false);
				}
				if (!reservationOwnerAgent.equals(userAgent) && (agents == null || !agents
						.contains(reservationOwnerAgent))) {
					throw new ModuleException(WebConstants.REQ_LOAD_REP_AGENT_PNR_ERROR);
				}
			}
		}

	}

	public String getOriginCountryOfCall() {
		return originCountryOfCall;
	}

	public void setOriginCountryOfCall(String originCountryOfCall) {
		this.originCountryOfCall = originCountryOfCall;
	}
}
