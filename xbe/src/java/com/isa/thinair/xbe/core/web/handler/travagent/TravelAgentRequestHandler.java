package com.isa.thinair.xbe.core.web.handler.travagent;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentApplicableOND;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.util.LookupUtils;
import com.isa.thinair.xbe.api.dto.UserAgentInfoTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.config.XBEModuleConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.DateUtil;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.travagent.TravelAgentHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Shakir
 * 
 *         FIXME CLEAN THIS UP
 */
public final class TravelAgentRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(TravelAgentRequestHandler.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static XBEConfig xbeConfig = new XBEConfig();

	private static XBEModuleConfig config = XBEModuleUtils.getConfig();

	private static final String PARAM_STATION_CODE = "hdnStationCode";
	private static final String PARAM_AGENTCODE = "hdnCode";
	private static final String PARAM_TYPE1 = "selAgentType";
	private static final String PARAM_NAME1 = "txtName";
	private static final String PARAM_ADDRESS1 = "txtAdd1";
	private static final String PARAM_ADDRESS2 = "txtAdd2";
	private static final String PARAM_CITY = "txtCity";
	private static final String PARAM_TERRITORY_CODE = "hdnTerritoryCode";
	private static final String PARAM_REPORTING_TO_GSA = "chkGSA";
	private static final String PARAM_REPORTING_GSA = "selGSA";
	private static final String PARAM_AUTOINVOICE = "chkAutoInvoice";
	private static final String PARAM_ADVANCEBANK = "txtAdvnace";
	private static final String PARAM_CREDITLIMIT = "txtCredit";
	private static final String PARAM_PAYMENTMODE = "hdnPaymentmode";
	private static final String PARAM_STATE = "txtState";
	private static final String PARAM_STATUS = "hdnSelStatus";
	private static final String PARAM_POSTALCODE = "txtPostal";
	private static final String PARAM_TELEPHONE = "txtTelNo";
	private static final String PARAM_FAX = "txtFaxNo";
	private static final String PARAM_EMAIL = "txtEmail";
	private static final String PARAM_IATA = "txtIATA";
	private static final String PARAM_ACCOUNTCODE = "txtAccount";
	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_NEWCREDITLIMIT = "txtNLimit";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_BILLING_EMAIL = "txtBillingEmail";
	private static final String PARAM_HANDLING_CHARGE = "hdnHandling";
	private static final String PARAM_CURRENTCREDIT = "hdnCurrectCreditLimit";
	private static final String PARAM_FORCECANCEL = "ForceCancel";
	private static final String PARAM_CURRENCY_CODE = "selCurrency";
	private static final String PARAM_NOTES = "txtNotes";
	private static final String PARAM_CONTACT_PERSON = "txtContactPerson";
	private static final String PARAM_LICENSE = "txtLicense";
	private static final String PARAM_ON_HOLD = "chkOnHold";
	private static final String PARAM_AGENT_COMMISSION = "hdnAgentCommition";
	private static final String PARAM_CREDIT_LOCAL = "txtLocCredit";
	private static final String PARAM_ADVANCE_LOCAL = "txtLocAdvnace";
	private static final String PARAM_CURRENCY_IN = "selCurrIn";
	private static final String PARAM_CURENTCREDIT_LOCAL = "hdnLocCurrectCreditLimit";
	private static final String PARAM_BANKGURRENT_LOCAL = "hdnLocBankGuarantee";
	private static final String PARAM_PREF_LANGUAGE = "hdnPrefLanguage";
	private static final String PAY_CURR_CODE_CREDIT = "selCurrCode";
	private static final String PARAM_LOCATION_URL = "txtLocationURL";
	private static final String PARAM_PREF_RPT_FORMAT = "hdnPrefRprFormat";
	private static final String PARAM_SERVICE_CHANNEL = "selServiceChannel";
	private static final String PARAM_CAPTURE_EXT_PAY = "chkCapExtPay";
	private static final String PARAM_EXT_PAY_MANDATORY = "chkExtPayMand";
	private static final String PARAM_PUBLISH_TO_WEB = "chkPublish";
	private static final String PARAM_CONTACT_PERSON_DESIGNATION = "selContactPersonDes";
	private static final String PARAM_MOBILE_NUMBER = "txtMobile";
	private static final String PARAM_SERV_CH_JS = "reqServChJS";
	private static final String PARAM_AGENT_INACTIVE_DATE = "hdnInactiveDate";
	private static final String PARAM_EMAILS_TO_NOTIFY = "txtEmailsToNotify";
	private static final String PARAM_AGENT_COUNTRY = "hdnCountry";
	private static final String PARAM_MAX_ADULT_ALLOWED = "maxAdultAllowed";
	private static final String PARAM_FIXED_CREDIT = "chkFixedCredit";
	

	// handling fee changes
	private static final String PARAM_HANDLING_FEE_APPLIES_TO = "selAppliesto";
	private static final String PARAM_HANDLING_FEE_TYPE = "selType";
	private static final String PARAM_HANDLING_FEE_OW_AMOUNT = "txtOWAmount";
	private static final String PARAM_HANDLING_FEE_RT_AMOUNT = "txtRTAmount";
	private static final String PARAM_HANDLING_FEE_MIN_AMOUNT = "txtMinAmount";
	private static final String PARAM_HANDLING_FEE_MAX_AMOUNT = "txtMaxAmount";
	private static final String PARAM_HANDLING_FEE_ENABLE = "chkHFActive";
	public static final String CURRENCY_SYS_DATE = "reqSysDate";
	
	//HMF
	
	private static final String PARAM_HANLING_FEE_MOD = "hdnHandlingFeeModStr";
	private static final String PARAM_HANLING_FEE_MOD_ACT= "hdnHandlingFeeModAct";
	private static final String PARAM_HANLING_FEE_MOD_APPLY = "hdnHandlingFeeModApp";

	// BSP
	private static final String PARAM_BSP_GUATANTEE = "txtBSPGuarantee";
	private static final String PARAM_BSP_GUATANTEE_LOCAL = "txtLocBSPGuarantee";

	// Agent ticketing info
	private static final String PARAM_TKT_SEQ_ENABLE = "chkEnableAgentLevelEticket";

	// Agent fare mask
	private static final String PARAM_FARE_MASK_ENABLE = "chkEnableAgentLevelFareMask";

	private static final String PARAM_BANK_GURANTEE_EXPIRY_DATE = "txtAdvnaceExpiryDate";

	// Charter booking
	private static final String PARAM_CHARTER_BOOKING = "chkCharterAgent";

	private static final String PARAM_MODEL = "hdnModel";

	private static final String PARAM_OND_LIST = "hdnOndList";

	// hdnModel strings
	private static final String PARAM_MODEL_ADD = "ADD";
	private static final String PARAM_MODEL_EDIT = "EDIT";
	private static UserAgentInfoTO userInfoTO = null;

	private static final String PARAM_HIDE_GDS_ONHOLD = "hideGDSOnholdOption";
	private static final String PARAM_VISIBLE_TERRITORIES_STATIONS = "hdnVisibleTerritoryStationValues";
	
	private static final String YES = "Y";
	
	private static boolean isGSAV2Enabled = AppSysParamsUtil.isGSAStructureVersion2Enabled();
	/**
	 * Main Execute Method for Travel Agent & Credit History
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */

	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		boolean isExceptionOccured = false;
		String strHdnMode = request.getParameter("hdnMode");
		String strCreditMode = request.getParameter("hdnCMode");

		String saveSuccess = "var isSaveTransactionSuccessful = false;";
		String isSuccessfulSaveMessageDisplayEnabledJS = "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";";
		boolean isSaveTransactionSuccessful = false;
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, isSuccessfulSaveMessageDisplayEnabledJS);
		request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, saveSuccess);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);

		// Setting whether the use has privileges to change agent status Replace with actual checking later :-D
		request.setAttribute(WebConstants.REQ_HAS_STATUS_CHANGE_PRIVILEGE,
				hasPrivilege(request, WebConstants.TA_MAINT_STATUS_EDIT));
		String userAgentCode = null;
		// UserAgentInfoTO userInfoTO = null; //************
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		userAgentCode = user.getAgentCode();
		StringBuffer scBuff = new StringBuffer();
		if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_SERVCH_VIEW)) {
			scBuff.append("isServCh = true;");
		}
		if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_SERVCH_EDIT)) {
			scBuff.append("isServChEditable = true;");
		}
		setAttribInRequest(request, PARAM_SERV_CH_JS, scBuff.toString());
		setAttribInRequest(request, PARAM_HIDE_GDS_ONHOLD, config.isHideGDSOnHoldOption());
		try {
			userInfoTO = getUserType(userAgentCode);
			userInfoTO.setPrivilegedUser(isPrivilegedUser(request, userInfoTO.getAgentCode()));
		} catch (ModuleException e1) {
			log.error("Error ", e1);
			forward = S2Constants.Result.ERROR;
		}

		try {

			if (strHdnMode != null) {

				// show the Credit History for Button Click
				if (strHdnMode.equals("showCredit")) {
					checkPrivilege(request, WebConstants.PRIV_TA_MAINT_CREDIT);
					audit(request, TasksUtil.TA_VIEW_TRAVEL_AGENT_CREDIT_LIMIT);
					return showCreditAct(request, userInfoTO);
				}

				// saves the Travel Agent
				if (strHdnMode.equals(WebConstants.ACTION_SAVE)) {
					isSaveTransactionSuccessful = true;
					if (userInfoTO.isPrivilegedUser()) {
						isExceptionOccured = saveData(request, isExceptionOccured, userInfoTO.isGSA());
						if (isExceptionOccured)
							isSaveTransactionSuccessful = false;
					} else {
						log.debug("User does not have sufficient privilages");
						throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
					}

				}

				// Delete the Travel Agent
				if (strHdnMode.equalsIgnoreCase(WebConstants.ACTION_DELETE)) {
					checkPrivilege(request, WebConstants.PRIV_TA_MAINT_DELETE);
					deleteData(request);
					log.debug("\nTRAVEL AGENT REQUEST HANDLER deleteData() SUCCESS");
				}
			}
			// saves the Creit history
			if (strCreditMode != null && strCreditMode.equals("SAVECREDIT")) {
				try {
					checkPrivilege(request, WebConstants.PRIV_TA_MAINT_CREDIT_UPDATE);
					isSaveTransactionSuccessful = true;
					saveCreditLimit(request);
					saveMessage(request, xbeConfig.getMessage("cc.airadmin.add.success", userLanguage), WebConstants.MSG_SUCCESS);
					saveSuccess = "var isSaveTransactionSuccessful = true;";
					request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, saveSuccess);
					return showCreditAct(request, userInfoTO);

				} catch (ModuleException e) {
					isSaveTransactionSuccessful = false;
					isExceptionOccured = true;
					log.error("EXCEPTION IN saveCreditLimit " + e.getMessageString());
					saveMessage(request,xbeConfig.getMessage(e.getExceptionCode(),userLanguage), WebConstants.MSG_ERROR);

				} finally {
					try {
						setCreditHistoryRowHtml(request);
					} catch (ModuleException e) {
						log.error("EXCEPTION IN FINALLY saveCreditLimit " + e.getExceptionCode());
					}
				}
			}
		} catch (ModuleException e) {
			isExceptionOccured = true;
			isSaveTransactionSuccessful = false;
			String strFormData = getErrorForm(getProperty(request));
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {

				saveMessage(request, xbeConfig.getMessage("cc.agent.form.code.defined", userLanguage), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception e) {
			log.error("\nTRAVEL AGENT REQUEST HANDLER SETDISPLAYDATA() FAILED ", e);
			forward = S2Constants.Result.ERROR;
		}

		try {

			audit(request, TasksUtil.TA_VIEW_TRAVEL_AGENTS);
			setDisplayData(request, isExceptionOccured, userInfoTO);
		} catch (ModuleException e) {
			isExceptionOccured = true;

			String strFormData = getErrorForm(getProperty(request));
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception e) {
			log.error("\nTRAVEL AGENT REQUEST HANDLER SETDISPLAYDATA() FAILED ", e);
			forward = "error";

			String msg = BasicRH.getErrorMessage(e,userLanguage);
			log.error(e.getMessage() + ":" + msg);
			request.setAttribute(WebConstants.REQ_ERROR_SERVER_MESSAGES, msg);
		}

		if (isSaveTransactionSuccessful) {
			saveSuccess = "var isSaveTransactionSuccessful = true;";
		} else {
			saveSuccess = "var isSaveTransactionSuccessful = false;";
		}
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, isSuccessfulSaveMessageDisplayEnabledJS);
		request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, saveSuccess);
		request.setAttribute(WebConstants.REQ_USERPRINCIPLE_CARRIER,
				globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE));
		request.setAttribute(WebConstants.REQ_TRAVEL_AGENT_INCENTIVE_ENABLED, AppSysParamsUtil.isTravelAgentIncentiveAnabled());
		request.setAttribute(WebConstants.REQ_TRAVEL_AGENT_PAY_REF_ENABLED, AppSysParamsUtil.isAllowCapturePayRef());
		request.setAttribute(WebConstants.REQ_AGENT_MAINTAIN_BSP_CRED_LIM_ENABLED,
				AppSysParamsUtil.isMaintainAgentBspCreditLimit());
		return forward;
	}

	/**
	 * Saves the Travel Agent Add/Edit
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param isExceptionOccured
	 *            the status
	 * @return boolean true-success else not
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static boolean saveData(HttpServletRequest request, boolean isExceptionOccured, boolean isUserGSA)
			throws ModuleException {
		Agent agent = new Agent();
		Properties agp = getProperty(request);
		String agtStatus = "";
		int validity = 0;
		Currency curr = null;
		Station station = null;
		Territory territory = null;
		Agent gsaV2Agent = null;

		boolean isEnableMultipleGSAsForAStation = AppSysParamsUtil.enableMultipleGSAsForAStation();
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (agp.getProperty(PARAM_VERSION).equals("")) {

			boolean isAgentIATACodeExists = false;
			station = ModuleServiceLocator.getLocationServiceBD().getStation(agp.getProperty(PARAM_STATION_CODE));
			territory = ModuleServiceLocator.getLocationServiceBD().getTerritory(agp.getProperty(PARAM_TERRITORY_CODE));
			if (!agp.getProperty(PARAM_CURRENCY_CODE).equals(WebConstants.REQ_HTML_COUNTTRY_CURR)) {
				curr = ModuleServiceLocator.getCommonServiceBD().getCurrency(agp.getProperty(PARAM_CURRENCY_CODE));
			}

			if (!agp.getProperty(PARAM_IATA).equals("")) {
				Agent iataAgent = ModuleServiceLocator.getTravelAgentBD().getAgentByIATANumber(agp.getProperty(PARAM_IATA));

				if (iataAgent != null) {
					isAgentIATACodeExists = true;
				}
			}

			if (curr != null && curr.getStatus().equals(Currency.STATUS_INACTIVE)) {
				validity = 1;
			} else if (station != null && station.getStatus().equals(Station.STATUS_INACTIVE)) {
				validity = 2;
			} else if (territory != null && territory.getStatus().equals(Territory.STATUS_INACTIVE)) {
				validity = 3;
			} else if (isAgentIATACodeExists) {
				validity = 14;
			}

			if (!agp.getProperty(PARAM_STATUS).equals(Agent.STATUS_ACTIVE) && isUserGSA) {
				log.debug("GSA User can only save agents with active status");
				validity = 3;
			}

			if (!validatePayCurrType(agp.getProperty(PARAM_CURRENCY_IN), agp.getProperty(PARAM_CURRENCY_CODE))) {
				log.debug("###### currency code did not match with curr specified in"); // do
				// not
				// validate
			}

		} else {
			Agent agentTemp = ModuleServiceLocator.getTravelAgentBD().getAgent(agp.getProperty(PARAM_AGENTCODE));
			agent.setServiceChannel(agentTemp.getServiceChannel());
			agent.setLccPublishStatus(agentTemp.getLccPublishStatus());
			if (agentTemp != null && agentTemp.getAirlineCode() != null) {
				agent.setAirlineCode(agentTemp.getAirlineCode());
			}
			agtStatus = agentTemp.getStatus();
			// agtCpaturePaymentRef = agentTemp.getCapturePaymentRef();

			if (agentTemp != null && !agentTemp.getCurrencyCode().equals(agp.getProperty(PARAM_CURRENCY_CODE))) {

				if (!agp.getProperty(PARAM_CURRENCY_CODE).equals(WebConstants.REQ_HTML_COUNTTRY_CURR)) {
					curr = ModuleServiceLocator.getCommonServiceBD().getCurrency(agp.getProperty(PARAM_CURRENCY_CODE));
				}
				if (curr != null && curr.getStatus().equals(Currency.STATUS_INACTIVE)) {
					/* if the bank guarantee hasn't change skip the validity check */
					if (agentTemp.getBankGuaranteeCashAdvance() != agent.getBankGuaranteeCashAdvance()) {
						validity = 1;
					}
				}
			}

			if (agp.getProperty(PARAM_TERRITORY_CODE).equals("")) {
				agp.setProperty(PARAM_TERRITORY_CODE, agentTemp.getTerritoryCode());
			}

			if (!hasPrivilege(request, WebConstants.PRIV_TA_MAINT_REL_BLK_AGENT)) {
				if ((agentTemp.getStatus().equals(Agent.STATUS_BLOCKED) && !agp.getProperty(PARAM_STATUS).equals(
						Agent.STATUS_BLOCKED))
						&& isUserGSA) {
					validity = 10;
				}
			} else {
				log.debug("User has privilege to release black listed agents");
			}

			if (!(agp.getProperty(PARAM_STATUS).equals(Agent.STATUS_ACTIVE) || agp.getProperty(PARAM_STATUS).equals(
					Agent.STATUS_BLOCKED))) {
				if (agp.getProperty(PARAM_STATUS).equals(Agent.STATUS_NEW)) {
					validity = 4;
				}
				if ((agentTemp.getStatus().equals(Agent.STATUS_BLOCKED) || agentTemp.getStatus().equals(Agent.STATUS_INACTIVE) || agentTemp
						.getStatus().equals(Agent.STATUS_ACTIVE)) && agp.getProperty(PARAM_STATUS).equals(Agent.STATUS_NEW)) {
					validity = 4;
				}
			}
			boolean isCurrChange = !agentTemp.getCurrencyCode().equalsIgnoreCase(agp.getProperty(PARAM_CURRENCY_CODE));
			boolean isCurrSpecifiedIn = !agentTemp.getCurrencySpecifiedIn().equalsIgnoreCase(agp.getProperty(PARAM_CURRENCY_IN));
			if (isCurrChange || isCurrSpecifiedIn) {
				if (DatabaseUtil.getHasTxn(agentTemp.getAgentCode())) {
					if (isCurrChange && isCurrSpecifiedIn) {
						validity = 7;
					} else if (isCurrSpecifiedIn) {
						validity = 6;
					} else {
						validity = 5;
					}
				}
			}

			boolean isAgentIATACodeAlreadyExists = false;

			if (!agp.getProperty(PARAM_IATA).equals("")) {
				Agent iataAgent = ModuleServiceLocator.getTravelAgentBD().getAgentByIATANumber(agp.getProperty(PARAM_IATA));

				if (iataAgent != null && !iataAgent.getAgentCode().equals(agentTemp.getAgentCode())) {
					isAgentIATACodeAlreadyExists = true;
				}
			}

			if (isAgentIATACodeAlreadyExists) {
				validity = 14;
			}

			// check BSP ballance exceeed
			if (AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
				BigDecimal minCreditAdjusment = AccelAeroCalculator.subtract(agentTemp.getBspGuarantee(), agentTemp
						.getAgentSummary().getAvailableBSPCredit());

				BigDecimal specifiedBspCreditInBaseCurr = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (agp.getProperty(PARAM_CURRENCY_IN).equalsIgnoreCase("L")) {
					if (!agp.getProperty(PARAM_BSP_GUATANTEE_LOCAL).isEmpty() && !agp.getProperty(PARAM_CURRENCY_CODE).isEmpty()) {
						ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
						BigDecimal amountInLocal = AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
								.getProperty(PARAM_BSP_GUATANTEE_LOCAL));
						specifiedBspCreditInBaseCurr = BLUtil.getAmountInBase(amountInLocal,
								agp.getProperty(PARAM_CURRENCY_CODE), exchangeRateProxy);
					} else {
						validity = 15;
					}
				} else {
					if (!agp.getProperty(PARAM_BSP_GUATANTEE).isEmpty()) {
						specifiedBspCreditInBaseCurr = AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
								.getProperty(PARAM_BSP_GUATANTEE));
					}
				}

				if (AccelAeroCalculator.isLessThan(specifiedBspCreditInBaseCurr, minCreditAdjusment)) {
					validity = 13;
				}
			}

			if (isGSAV2Enabled) {
				if ((agp.getProperty(PARAM_REPORTING_GSA) != null
						&& !agp.getProperty(PARAM_REPORTING_GSA).equals(StringUtil.getNotNullString(agentTemp.getGsaCode())))
						|| (agentTemp.getGsaCode() != null
								&& !agentTemp.getGsaCode().equals(agp.getProperty(PARAM_REPORTING_GSA)))) {
					validity = 23;
				}

				String fixedCredit = agp.getProperty(PARAM_FIXED_CREDIT).equals(WebConstants.VAL_CHECKBOX_ON) ? "Y" : "N";
				if (!fixedCredit.equals(agentTemp.getHasCreditLimit())) {
					validity = 24;
				}

				BigDecimal existingCreditLimit = agentTemp.getCreditLimit();
				BigDecimal creatingAgentCredit = null;

				if (!agp.getProperty(PARAM_CREDITLIMIT).trim().equals("")) {
					creatingAgentCredit = AccelAeroCalculator
							.getTwoScaledBigDecimalFromString(agp.getProperty(PARAM_CREDITLIMIT));
				}

				if (AccelAeroCalculator.scaleValueDefault(creatingAgentCredit)
						.compareTo(AccelAeroCalculator.scaleValueDefault(existingCreditLimit)) != 0) {
					validity = 25;
				}

			}			
			if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(agent.getAgentTypeCode())
					&& !agentTemp.getAgentTypeCode().equals(agent.getAgentTypeCode())) {
				validity = 26;
			} else {
				agent.setAgentTypeCode(agentTemp.getAgentTypeCode());
			}

		}

		String gsaCode = agp.getProperty(PARAM_REPORTING_GSA);
		if (agp.getProperty(PARAM_REPORTING_TO_GSA).equals(WebConstants.VAL_CHECKBOX_ON)) {
			if (!isEnableMultipleGSAsForAStation) {
				if (station == null) {
					station = ModuleServiceLocator.getLocationServiceBD().getStation(agp.getProperty(PARAM_STATION_CODE));
				}
				Agent gsaAgent = ModuleServiceLocator.getTravelAgentBD().getGSAForTerritory(station.getTerritoryCode());
				if (gsaAgent != null) {
					gsaCode = gsaAgent.getAgentCode();
				}
				if (agp.getProperty(PARAM_PAYMENTMODE) != null && gsaAgent != null) {
					String[] strPaymentModeArr = agp.getProperty(PARAM_PAYMENTMODE).split(",");
					if (!validatePaymentMethods(gsaAgent, strPaymentModeArr)) {
						validity = 16;
					}
				}
			} else {
				Agent gsa = ModuleServiceLocator.getTravelAgentBD().getAgent(agp.getProperty(PARAM_REPORTING_GSA));
				if (agp.getProperty(PARAM_PAYMENTMODE) != null && gsa != null) {
					String[] strPaymentModeArr = agp.getProperty(PARAM_PAYMENTMODE).split(",");
					if (!validatePaymentMethods(gsa, strPaymentModeArr)) {
						validity = 16;
					}
				}
			}
		}

		if (gsaCode != null && !gsaCode.equals("")) {
			Agent reportToAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(gsaCode);
			AgentType reportToAgentType = ModuleServiceLocator.getTravelAgentBD().getAgentType(reportToAgent.getAgentTypeCode());
			if (!reportToAgentType.getSubAgentTypeCodes().contains(agp.getProperty(PARAM_TYPE1))) {
				validity = 28;
			}
		}

		if (agtStatus != null) {
			if (agtStatus.equalsIgnoreCase("") || agp.getProperty(PARAM_STATUS).equalsIgnoreCase("ACT"))

			{

				if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
					if (agp.getProperty(PARAM_REPORTING_TO_GSA).equals(WebConstants.VAL_CHECKBOX_ON)) {
						if (!validateTaCurrency(gsaCode, agp.getProperty(PARAM_CURRENCY_CODE))) {
							validity = 9;
						}
						if (!validateTaCurrencySpecified(gsaCode, agp.getProperty(PARAM_CURRENCY_IN))) {
							validity = 12;
						}
					}

				}
			}

		}

		if (isGSAV2Enabled && agp.getProperty(PARAM_FIXED_CREDIT) != null && !agp.getProperty(PARAM_FIXED_CREDIT).equals("")
				&& agp.getProperty(PARAM_FIXED_CREDIT).equals(WebConstants.VAL_CHECKBOX_ON)
				&& agp.getProperty(PARAM_REPORTING_TO_GSA).equals(WebConstants.VAL_CHECKBOX_ON) && gsaCode != null) {

			Agent reportingAgent = null;
			BigDecimal allowableCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal creatingAgentCredit = null;
			ArrayList<Agent> agentsList = new ArrayList<Agent>();

			if (!isEnableMultipleGSAsForAStation) {
				if (station == null) {
					station = ModuleServiceLocator.getLocationServiceBD().getStation(agp.getProperty(PARAM_STATION_CODE));
				}
				reportingAgent = ModuleServiceLocator.getTravelAgentBD().getGSAForTerritory(station.getTerritoryCode());
			} else {
				reportingAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(agp.getProperty(PARAM_REPORTING_GSA));
			}

			if (reportingAgent != null) {
				gsaV2Agent = getFixedCreditAgent(reportingAgent);

				if (gsaV2Agent != null && agp.getProperty(PARAM_VERSION).equals("")) {
					allowableCreditAmount = gsaV2Agent.getAgentSummary().getAvailableCredit();
					if (AccelAeroCalculator.isGreaterThan(allowableCreditAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						if (!agp.getProperty(PARAM_CREDITLIMIT).trim().equals("")) {
							creatingAgentCredit = AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
									.getProperty(PARAM_CREDITLIMIT));
						}

						if(creatingAgentCredit != null && AccelAeroCalculator.isGreaterThan(creatingAgentCredit, allowableCreditAmount)){
							validity = 17;
						}
					} else {
						validity = 18;
					}
				}
			}
		}
		
		// check to validate assigned station belongs to its own or visible territories
		if (isGSAV2Enabled && agp.getProperty(PARAM_REPORTING_TO_GSA).equals(WebConstants.VAL_CHECKBOX_ON)
				&& agp.getProperty(PARAM_REPORTING_GSA) != null && !agp.getProperty(PARAM_REPORTING_GSA).isEmpty()) {

			station = ModuleServiceLocator.getLocationServiceBD().getStation(agp.getProperty(PARAM_STATION_CODE));
			boolean foundGsa = false;
			Agent gsaAgent = null;
			String parentAgent = agp.getProperty(PARAM_REPORTING_GSA);

			if (!isEnableMultipleGSAsForAStation) {
				if (station == null) {
					station = ModuleServiceLocator.getLocationServiceBD().getStation(agp.getProperty(PARAM_STATION_CODE));
				}
				gsaAgent = ModuleServiceLocator.getTravelAgentBD().getGSAForTerritory(station.getTerritoryCode());
			} else {
				do {
					gsaAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(parentAgent);
					if (gsaAgent != null) {
						if (gsaAgent.getGsaCode() != null && !gsaAgent.getGsaCode().isEmpty()) {
							parentAgent = gsaAgent.getGsaCode();
						} else {
							foundGsa = true;
						}
					} else {
						foundGsa = true;
					}
				} while (!foundGsa);
			}

			if (gsaAgent != null && gsaAgent.getTerritoryCode() != null && !gsaAgent.getTerritoryCode().isEmpty()) {
				if (!(gsaAgent.getTerritoryCode().equals(station.getTerritoryCode()) || (gsaAgent.getVisibleTerritories() != null
						&& !gsaAgent.getVisibleTerritories().isEmpty() && gsaAgent.getVisibleTerritories().contains(
						station.getTerritoryCode())))) {
					validity = 19;
				}
			}
		}
		
		if (isGSAV2Enabled) {
			String fixedCreditAgent = StringUtil.getNotNullString(request.getParameter(PARAM_FIXED_CREDIT));
			if (com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(fixedCreditAgent)
					|| !WebConstants.VAL_CHECKBOX_ON.equals(fixedCreditAgent)) {
				// validation for shared credit basis restricted flow
				String agentType = agp.getProperty(PARAM_TYPE1);
				if (AgentType.GSA.equals(agp.getProperty(PARAM_TYPE1))) {
					validity = 20;
				} else if (AppSysParamsUtil.getCarrierAgent().equals(agentType) || AgentType.CO.equals(agentType)
						|| AgentType.SO.equals(agentType)) {
					validity = 22;
				}

				String reportToAgentCode = agp.getProperty(PARAM_REPORTING_GSA);
				if (!WebConstants.VAL_CHECKBOX_ON.equalsIgnoreCase(agp.getProperty(PARAM_REPORTING_TO_GSA))
						|| com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(reportToAgentCode)) {
					validity = 22;
				}

			} else if (WebConstants.VAL_CHECKBOX_ON.equals(fixedCreditAgent) && userInfoTO.isSharedCreditBasis()
					&& !hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
				validity = 27;
			}

		}

		// check to validate visible stations
		if (isGSAV2Enabled && agp.getProperty(PARAM_REPORTING_TO_GSA).equals(WebConstants.VAL_CHECKBOX_ON)
				&& agp.getProperty(PARAM_REPORTING_GSA) != null && !agp.getProperty(PARAM_REPORTING_GSA).isEmpty()
				&& agp.getProperty(PARAM_VISIBLE_TERRITORIES_STATIONS) != null
				&& !agp.getProperty(PARAM_VISIBLE_TERRITORIES_STATIONS).isEmpty()) {
			String[] stationsArray = agp.getProperty(PARAM_VISIBLE_TERRITORIES_STATIONS).split(",");
			Collection stationCodes = Arrays.asList(stationsArray);
			Collection<String> territories = ModuleServiceLocator.getLocationServiceBD().getTerritoriesForStations(stationCodes);
			boolean foundGsa = false;
			Agent gsaAgent = null;
			String parentAgent = agp.getProperty(PARAM_REPORTING_GSA);

			if (!isEnableMultipleGSAsForAStation) {
				if (station == null) {
					station = ModuleServiceLocator.getLocationServiceBD().getStation(agp.getProperty(PARAM_STATION_CODE));
				}
				gsaAgent = ModuleServiceLocator.getTravelAgentBD().getGSAForTerritory(station.getTerritoryCode());
			} else {
				do {
					gsaAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(parentAgent);
					if (gsaAgent != null) {
						if (gsaAgent.getGsaCode() != null && !gsaAgent.getGsaCode().isEmpty()) {
							parentAgent = gsaAgent.getGsaCode();
						} else {
							foundGsa = true;
						}
					} else {
						foundGsa = true;
					}
				} while (!foundGsa);
			}

			if (gsaAgent != null && gsaAgent.getTerritoryCode() != null && !gsaAgent.getTerritoryCode().isEmpty()) {
				if ((gsaAgent.getVisibleTerritories() != null && !gsaAgent.getVisibleTerritories().isEmpty())) {
					for (String territoryCode : territories) {
						if (!gsaAgent.getVisibleTerritories().contains(territoryCode)) {
							validity = 21;
							break;
						}
					}
				}
			}
		}

		if (!validatePayCurrType(agp.getProperty(PARAM_CURRENCY_IN), agp.getProperty(PARAM_CURRENCY_CODE))) {
			log.debug("###### currency code did not match with curr specified in"); // do
			// not
			// validate
		}

		switch (validity) {
		case 1:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.country.form.currency.inactive", userLanguage), WebConstants.MSG_ERROR);
			break;

		case 2:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.station.inactive", userLanguage), WebConstants.MSG_ERROR);
			break;

		case 3:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.territory.inactive", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 4:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.statusInvalid", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 5:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.CurrencyInvalid", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 6:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.CurrSpecInvalid", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 7:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.CurrSpecCurrInv", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 8:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.CurrSpecNotAll", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 9:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.GsaCurrNotMatch", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 10:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.statusBlkAct", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 11:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.payCurrMismatch", userLanguage), WebConstants.MSG_ERROR);
			break;

		case 12:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.GsaSpecifiedNotMatch", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 13:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.bsp.credit.invalid.amount", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 14:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.iataCodeExists", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 15:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.invalidCurrencySelection", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 16:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.invalidPaymentMethodSelection", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 17:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.fixedCreditLimitExceeds", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 18:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.fixedCreditLimitFinish", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 19:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.station.invalid", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 20:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.credit.basis.invalid", userLanguage), WebConstants.MSG_ERROR);
			break;
		case 21:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.visible.station.invalid", userLanguage),
					WebConstants.MSG_ERROR);
			break;
		case 22:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.credit.basis.invalid.for.selected.type", userLanguage),
					WebConstants.MSG_ERROR);
			break;
		case 23:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.invalid.reporting.to.agent", userLanguage),
					WebConstants.MSG_ERROR);
			break;
		case 24:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.credit.type.change.restricted", userLanguage),
					WebConstants.MSG_ERROR);
			break;
		case 25:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.credit.limit.change.restricted", userLanguage),
					WebConstants.MSG_ERROR);
			break;
		case 26:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.type.change.restricted", userLanguage),
					WebConstants.MSG_ERROR);
			break;	
		case 27:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.shared.agent.limit.priv", userLanguage),
					WebConstants.MSG_ERROR);
			break;
		case 28:
			isExceptionOccured = true;
			saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.invalid.reporting.agent.type", userLanguage),
					WebConstants.MSG_ERROR);
			break;
		}

		if (isExceptionOccured && validity != 0) {

			String strFormData = getErrorForm(agp);
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		} else {

			if (!agp.getProperty(PARAM_VERSION).equals("")) {
				agent.setVersion(Long.parseLong(agp.getProperty(PARAM_VERSION)));
				agent.setAgentCode(agp.getProperty(PARAM_AGENTCODE));
			} else {
				agent.setAgentCode(null);

				AgentSummary agentSummary = new AgentSummary();
				if (!agp.getProperty(PARAM_CREDITLIMIT).trim().equals("")) {
					agentSummary.setAvailableCredit(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
							.getProperty(PARAM_CREDITLIMIT)));
				}

				agentSummary.setAvailableAdvance(AccelAeroCalculator.getDefaultBigDecimalZero());
				agentSummary.setAvailableFundsForInv(AccelAeroCalculator.getDefaultBigDecimalZero());
				agent.setAgentSummary(agentSummary);
			}

			agent.setAgentTypeCode(agp.getProperty(PARAM_TYPE1));
			agent.setAgentName(agp.getProperty(PARAM_NAME1));
			agent.setAddressLine1(agp.getProperty(PARAM_ADDRESS1));
			agent.setAddressLine2(agp.getProperty(PARAM_ADDRESS2));
			agent.setAddressCity(agp.getProperty(PARAM_CITY));
			agent.setTerritoryCode(agp.getProperty(PARAM_TERRITORY_CODE));
			agent.setStationCode(agp.getProperty(PARAM_STATION_CODE));
			agent.setLocationUrl(agp.getProperty(PARAM_LOCATION_URL)); // JIRA:
			agent.setMaxAdultAllowed(agp.getProperty(PARAM_MAX_ADULT_ALLOWED).equals("")? null :  new Integer(agp.getProperty(PARAM_MAX_ADULT_ALLOWED)));

			if (agp.getProperty(PARAM_MODEL).equals(WebConstants.ACTION_ADD)) {
				agent.setLccPublishStatus(Util.LCC_TOBE_PUBLISHED);
			} else if (agp.getProperty(PARAM_MODEL).equals(WebConstants.ACTION_EDIT)) {
				agent.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);
			}

			Date inactiveDate = null;
			if (!("").equals(agp.getProperty(PARAM_AGENT_INACTIVE_DATE))) {
				String pattern = "dd/MM/yyyy HH:mm";
				try {
					SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
					inactiveDate = dateFormat.parse(agp.getProperty(PARAM_AGENT_INACTIVE_DATE));
				} catch (Exception e) {
					log.error("error in parsing system date" + e.getMessage());
				}
			}

			agent.setInactiveFromDate(inactiveDate);

			// 3031
			// -
			// Lalanthi
			// Date
			// :
			// 08/10/2009

			if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
				if (agp.getProperty(PARAM_REPORTING_TO_GSA).equals(WebConstants.VAL_CHECKBOX_ON)) {
					agent.setReportingToGSA("Y");
					if (isEnableMultipleGSAsForAStation) {
						agent.setGsaCode(agp.getProperty(PARAM_REPORTING_GSA));
						Agent gsa = ModuleServiceLocator.getTravelAgentBD().getAgent(agp.getProperty(PARAM_REPORTING_GSA));
						if (gsa != null) {
							agent.setTerritoryCode(gsa.getTerritoryCode());
						}
					} else {
						agent.setGsaCode(gsaCode);
					}
				} else {
					agent.setReportingToGSA("N");
				}
			} else if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_GSA)) {
				agent.setReportingToGSA("Y");
				if (isEnableMultipleGSAsForAStation) {
					agent.setGsaCode(agp.getProperty(PARAM_REPORTING_GSA));
					Agent gsa = ModuleServiceLocator.getTravelAgentBD().getAgent(agp.getProperty(PARAM_REPORTING_GSA));
					if (gsa != null) {
						agent.setTerritoryCode(gsa.getTerritoryCode());
					}
				} else {
					if (station == null) {
						station = ModuleServiceLocator.getLocationServiceBD().getStation(agp.getProperty(PARAM_STATION_CODE));
					}
					Agent gsaAgent = ModuleServiceLocator.getTravelAgentBD().getGSAForTerritory(station.getTerritoryCode());
					if (gsaAgent != null) {
						agent.setGsaCode(gsaAgent.getAgentCode());
						agent.setTerritoryCode(gsaAgent.getTerritoryCode());
					}
				}
			}
			if (AppSysParamsUtil.isEnableRouteSelectionForAgents()) {
				if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ROUTE_SELECTION)) {
					Set<AgentApplicableOND> agentOndList = new HashSet<AgentApplicableOND>();
					if (!request.getParameter(PARAM_OND_LIST).equals("")) {
						String[] ondList = request.getParameter(PARAM_OND_LIST).split(",");
						if (ondList.length > 0) {
							for (int i = 0; i < ondList.length; i++) {
								AgentApplicableOND agentAppOnd = new AgentApplicableOND();
								agentAppOnd.setAgent(agent);
								agentAppOnd.setOndCOde(ondList[i]);
								agentOndList.add(agentAppOnd);
							}
							agent.setApplicableOndList(agentOndList);
						}
					}

				}
			} else {
				Set<AgentApplicableOND> agentOndList = new HashSet<AgentApplicableOND>();
				AgentApplicableOND agentAppOnd = new AgentApplicableOND();
				agentAppOnd.setAgent(agent);
				agentAppOnd.setOndCOde("***/***");
				agentOndList.add(agentAppOnd);
				agent.setApplicableOndList(agentOndList);
			}

			if (agp.getProperty(PARAM_AUTOINVOICE).equals(WebConstants.VAL_CHECKBOX_ON)) {
				agent.setAutoInvoice("Y");

			} else {
				agent.setAutoInvoice("N");

			}
			agent.setStatus(agp.getProperty(PARAM_STATUS));

			Set<String> set1 = new HashSet<String>();
			if (agp.getProperty(PARAM_PAYMENTMODE) != null) {
				String[] strPaymentModeArr = agp.getProperty(PARAM_PAYMENTMODE).split(",");
				/** JIRA -AARESAA:2553 (Lalanthi) */
				set1 = setPaymentMethods(strPaymentModeArr, hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY), isUserGSA);
				/*
				 * if (strPaymentModeArr[0].equals("true")) { set1.add("OA"); } //if(hasPrivilege(request,
				 * WebConstants.PRIV_TA_MAINT_ADD_ANY)) { if (strPaymentModeArr[1].equals("true")) { set1.add("CA"); }
				 * if (strPaymentModeArr[2].equals("true")) { set1.add("CC"); } //} }
				 */
			}
			agent.setPaymentMethod(set1);

			agent.setAddressStateProvince(agp.getProperty(PARAM_STATE));
			agent.setPostalCode(agp.getProperty(PARAM_POSTALCODE));
			agent.setTelephone(agp.getProperty(PARAM_TELEPHONE));
			agent.setFax(agp.getProperty(PARAM_FAX));
			agent.setEmailId(agp.getProperty(PARAM_EMAIL));
			agent.setAgentIATANumber(agp.getProperty(PARAM_IATA));
			agent.setAccountCode(agp.getProperty(PARAM_ACCOUNTCODE));
			agent.setBillingEmail(agp.getProperty(PARAM_BILLING_EMAIL));
			if (agp.getProperty(PARAM_HANDLING_CHARGE).trim().equals(""))
				agp.setProperty(PARAM_HANDLING_CHARGE, "0.0");
			agent.setHandlingChargAmount(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
					.getProperty(PARAM_HANDLING_CHARGE)));
			agent.setCurrencyCode(agp.getProperty(PARAM_CURRENCY_CODE));
			agent.setNotes(agp.getProperty(PARAM_NOTES));
			agent.setContactPersonName(agp.getProperty(PARAM_CONTACT_PERSON));
			agent.setLicenceNumber(agp.getProperty(PARAM_LICENSE));
			agent.setMobile(agp.getProperty(PARAM_MOBILE_NUMBER));
			agent.setDesignation(agp.getProperty(PARAM_CONTACT_PERSON_DESIGNATION));
			agent.setEmailsToNotify(agp.getProperty(PARAM_EMAILS_TO_NOTIFY));
			if (agp.getProperty(PARAM_ON_HOLD).equals(WebConstants.VAL_CHECKBOX_ON)) {
				agent.setOnHold(Agent.ONHOLD_YES);
			} else {
				agent.setOnHold(Agent.ONHOLD_NO);
			}
			agent.setAgentCommissionCode(agp.getProperty(PARAM_AGENT_COMMISSION));

			agent.setLanguageCode(agp.getProperty(PARAM_PREF_LANGUAGE));

			agent.setCurrencySpecifiedIn(agp.getProperty(PARAM_CURRENCY_IN));

			if (agp.getProperty(PARAM_CURRENCY_IN).equalsIgnoreCase("L")) {
				if (!agp.getProperty(PARAM_CREDIT_LOCAL).equals(""))
					agent.setCreditLimitLocal(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
							.getProperty(PARAM_CREDIT_LOCAL)));
				if (!agp.getProperty(PARAM_ADVANCE_LOCAL).equals(""))
					agent.setBankGuaranteeCashAdvanceLocal(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
							.getProperty(PARAM_ADVANCE_LOCAL)));
				if (!agp.getProperty(PARAM_BSP_GUATANTEE_LOCAL).equals(""))
					agent.setBspGuaranteeLocal(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
							.getProperty(PARAM_BSP_GUATANTEE_LOCAL)));
			} else {
				if (!agp.getProperty(PARAM_CREDITLIMIT).trim().equals("")) {
					agent.setCreditLimit(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp.getProperty(PARAM_CREDITLIMIT)));
					agent.setTotalCreditLimit(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp.getProperty(PARAM_CREDITLIMIT)));
				}if(!agp.getProperty(PARAM_ADVANCEBANK).equals("")){
					agent.setBankGuaranteeCashAdvance(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
							.getProperty(PARAM_ADVANCEBANK)));
				}				
				if (!agp.getProperty(PARAM_BSP_GUATANTEE).equals(""))
					agent.setBspGuarantee(AccelAeroCalculator.getTwoScaledBigDecimalFromString(agp
							.getProperty(PARAM_BSP_GUATANTEE)));
			}

			if ((agent.getBankGuaranteeCashAdvance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0 || agent
					.getBankGuaranteeCashAdvanceLocal().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0)
					&& !agp.getProperty(PARAM_BANK_GURANTEE_EXPIRY_DATE).equals("")) {
				try {
					agent.setBankGuranteeExpiryDate(DateUtil.parseDate(agp.getProperty(PARAM_BANK_GURANTEE_EXPIRY_DATE).trim()));
				} catch (ParseException e) {
					if (log.isErrorEnabled()) {
						log.error("Error while converting stirng to Date", e);
					}
					throw new ModuleException(e, e.getMessage());
				}
			}
			/*
			 * if(agtCpaturePaymentRef == null || agtCpaturePaymentRef.equals("")){ agent.setCapturePaymentRef("N");
			 * }else{ agent.setCapturePaymentRef(agtCpaturePaymentRef); }
			 */

			if (agp.getProperty(PARAM_CAPTURE_EXT_PAY) != null && !agp.getProperty(PARAM_CAPTURE_EXT_PAY).equals("")) {
				if (agp.getProperty(PARAM_CAPTURE_EXT_PAY).equals(WebConstants.VAL_CHECKBOX_ON)) {
					agent.setCapturePaymentRef("Y");
				} else {
					agent.setCapturePaymentRef("N");
				}
			} else {
				agent.setCapturePaymentRef("N");
			}

			if (agp.getProperty(PARAM_EXT_PAY_MANDATORY) != null && !agp.getProperty(PARAM_EXT_PAY_MANDATORY).equals("")) {
				if (agp.getProperty(PARAM_EXT_PAY_MANDATORY).equals(WebConstants.VAL_CHECKBOX_ON)) {
					agent.setPayRefMandatory("Y");
				} else {
					agent.setPayRefMandatory("N");
				}
			} else {
				agent.setPayRefMandatory("N");
			}

			/** JIRA - AARESSAA:4553 (Lalanthi) */
			if (agp.getProperty(PARAM_PUBLISH_TO_WEB) != null && !agp.getProperty(PARAM_PUBLISH_TO_WEB).equals("")) {
				if (agp.getProperty(PARAM_PUBLISH_TO_WEB).equals(WebConstants.VAL_CHECKBOX_ON)) {
					agent.setPublishToWeb("Y");
				} else {
					agent.setPublishToWeb("N");
				}
			} else {
				agent.setPublishToWeb("N");
			}

			// FIXME : At the moment this is
			// set in the DB, as per
			// client's
			// requirements.(Lalanthi
			// 25/11/2009)

			agent.setPrefferedRptFormat(agp.getProperty(PARAM_PREF_RPT_FORMAT));
			String strServChannel = agp.getProperty(PARAM_SERVICE_CHANNEL);
			if (hasPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_SERVCH_EDIT) && strServChannel != null
					&& !strServChannel.equals("")) {
				agent.setServiceChannel(strServChannel);
			}
			if (agent.getAirlineCode() == null) {
				agent.setAirlineCode(AppSysParamsUtil.getDefaultAirlineIdentifierCode());
			}

			// update agent handling fee details
			if (agp.getProperty(PARAM_HANDLING_FEE_APPLIES_TO) != null
					&& !agp.getProperty(PARAM_HANDLING_FEE_APPLIES_TO).isEmpty()) {
				agent.setHandlingFeeAppliesTo(Integer.valueOf(agp.getProperty(PARAM_HANDLING_FEE_APPLIES_TO)));
			}

			if (agp.getProperty(PARAM_HANDLING_FEE_TYPE) != null && !agp.getProperty(PARAM_HANDLING_FEE_TYPE).isEmpty()) {
				agent.setHandlingFeeChargeBasisCode(agp.getProperty(PARAM_HANDLING_FEE_TYPE));
			}

			if (agp.getProperty(PARAM_HANDLING_FEE_OW_AMOUNT) != null && !agp.getProperty(PARAM_HANDLING_FEE_OW_AMOUNT).isEmpty()) {
				agent.setHandlingFeeOnewayAmount(new BigDecimal(agp.getProperty(PARAM_HANDLING_FEE_OW_AMOUNT)));
			} else {
				agent.setHandlingFeeOnewayAmount(null);
			}

			if (agp.getProperty(PARAM_HANDLING_FEE_RT_AMOUNT) != null && !agp.getProperty(PARAM_HANDLING_FEE_RT_AMOUNT).isEmpty()) {
				agent.setHandlingFeeReturnAmount(new BigDecimal(agp.getProperty(PARAM_HANDLING_FEE_RT_AMOUNT)));
			} else {
				agent.setHandlingFeeReturnAmount(null);
			}

			if (agp.getProperty(PARAM_HANDLING_FEE_MIN_AMOUNT) != null
					&& !agp.getProperty(PARAM_HANDLING_FEE_MIN_AMOUNT).isEmpty()) {
				agent.setHandlingFeeMinAmount(new BigDecimal(agp.getProperty(PARAM_HANDLING_FEE_MIN_AMOUNT)));
			} else {
				agent.setHandlingFeeMinAmount(null);
			}

			if (agp.getProperty(PARAM_HANDLING_FEE_MAX_AMOUNT) != null
					&& !agp.getProperty(PARAM_HANDLING_FEE_MAX_AMOUNT).isEmpty()) {
				agent.setHandlingFeeMaxAmount(new BigDecimal(agp.getProperty(PARAM_HANDLING_FEE_MAX_AMOUNT)));
			} else {
				agent.setHandlingFeeMaxAmount(null);
			}

			if (agp.getProperty(PARAM_HANDLING_FEE_ENABLE) != null && !agp.getProperty(PARAM_HANDLING_FEE_ENABLE).isEmpty()) {
				agent.setHandlingFeeEnable("Y");
			} else {
				agent.setHandlingFeeEnable("N");
			}

			// agent level e ticket sequence enable
			if (agp.getProperty(PARAM_TKT_SEQ_ENABLE) != null && !agp.getProperty(PARAM_TKT_SEQ_ENABLE).isEmpty()) {
				agent.setAgentWiseTicketEnable("Y");
			} else {
				agent.setAgentWiseTicketEnable("N");
			}

			// charter agent
			if (agp.getProperty(PARAM_CHARTER_BOOKING) != null && !agp.getProperty(PARAM_CHARTER_BOOKING).isEmpty()) {
				agent.setCharterAgent(Agent.CHARTER_AGENT_YES);
			} else {
				agent.setCharterAgent(Agent.CHARTER_AGENT_NO);
			}

			// fare mask enable
			if (agp.getProperty(PARAM_FARE_MASK_ENABLE) != null && !agp.getProperty(PARAM_FARE_MASK_ENABLE).isEmpty()) {
				agent.setAgentWiseFareMaskEnable(Agent.CHARTER_AGENT_YES);
			} else {
				agent.setAgentWiseFareMaskEnable(Agent.CHARTER_AGENT_NO);
			}

			if (isGSAV2Enabled) {
				if (agp.getProperty(PARAM_VISIBLE_TERRITORIES_STATIONS) != null
						&& !agp.getProperty(PARAM_VISIBLE_TERRITORIES_STATIONS).isEmpty()) {

					String[] assignedTerritoryStations = agp.getProperty(PARAM_VISIBLE_TERRITORIES_STATIONS).split(",");

					Set<String> stationTerritories = null;
					if (AgentType.GSA.equals(agent.getAgentTypeCode())) {
						List<String[]> territoryList = LookupUtils.LookupDAO().getTwoColumnMap("territory");
						if (territoryList != null && !territoryList.isEmpty()) {
							stationTerritories = getAssignableTerritoriesStations(assignedTerritoryStations,
									territoryList);

							if (!stationTerritories.isEmpty()) {
								agent.setVisibleTerritories(stationTerritories);
							} else {
								// Error!!! Non-assignable territories assigned
								isExceptionOccured = true;
								saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.nonAssignableTerritories", userLanguage),
										WebConstants.MSG_ERROR);
							}
						} else {
							// Error!!! No territories are visible
							isExceptionOccured = true;
							saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.noVisibleTerritories", userLanguage),
									WebConstants.MSG_ERROR);
						}

					} else if (AgentType.TA.equals(agent.getAgentTypeCode())) {
						List<String[]> stationList = getVisibleStationList();
						if (stationList != null && !stationList.isEmpty()) {
							stationTerritories = getAssignableTerritoriesStations(assignedTerritoryStations,
									stationList);

							if (!stationTerritories.isEmpty()) {
								agent.setVisibleStations(stationTerritories);
							} else {
								// Error!!! Non-assignable territories assigned
								isExceptionOccured = true;
								saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.nonAssignableStations", userLanguage),
										WebConstants.MSG_ERROR);
							}
						} else {
							// Error!!! No territories are visible
							isExceptionOccured = true;
							saveMessage(request, xbeConfig.getMessage("cc.user.form.agent.nonVisibleStations", userLanguage),
									WebConstants.MSG_ERROR);
						}
					}

				}

				if (agp.getProperty(PARAM_FIXED_CREDIT) != null && !agp.getProperty(PARAM_FIXED_CREDIT).equals("")
						&& agp.getProperty(PARAM_FIXED_CREDIT).equals(WebConstants.VAL_CHECKBOX_ON)) {
					agent.setHasCreditLimit(CommonsConstants.YES);
				} else {
					agent.setHasCreditLimit(CommonsConstants.NO);
				}
			} else {
				agent.setHasCreditLimit(CommonsConstants.YES);
			}
			
			Set<AgentHandlingFeeModification> agentHandlingFeesForMod = new HashSet<>();

			String handlingFeeModStr = request.getParameter(PARAM_HANLING_FEE_MOD);
			if (handlingFeeModStr != null && !handlingFeeModStr.isEmpty()) {
				String[] arrHandlingFeeAgent = handlingFeeModStr.split("~");
				String[] arrHandlingFees = null;
				for (int i = 0; i < arrHandlingFeeAgent.length; ++i) {
					if (arrHandlingFeeAgent[i] != null && !arrHandlingFeeAgent[i].isEmpty()) {
						arrHandlingFees = arrHandlingFeeAgent[i].split(",");
						AgentHandlingFeeModification agentModHandlingFees = new AgentHandlingFeeModification();
						agentModHandlingFees.setAgent(agent);
						agentModHandlingFees.setOperation(new Integer(arrHandlingFees[0]));
						agentModHandlingFees.setAmount(new BigDecimal(arrHandlingFees[1]));
						agentModHandlingFees.setChargeBasisCode(arrHandlingFees[2]);
						agentModHandlingFees.setStatus(arrHandlingFees[3]);
/*						if(!new Integer(0).equals(new Integer(arrHandlingFees[4]))){
							agentModHandlingFees.setAgentModificationHandlingFeeId(new Integer(arrHandlingFees[4]));
						}*/
						agentHandlingFeesForMod.add(agentModHandlingFees);
					}
				}

			}

			agent.setHandlingFeeModificationApllieTo(new Integer(request.getParameter(PARAM_HANLING_FEE_MOD_APPLY)));
			agent.setHandlingFeeModificationEnabled(
					Agent.STATUS_HANDLING_FEE_MOD_Y.equals(request.getParameter(PARAM_HANLING_FEE_MOD_ACT)) ? "Y" : "N");
			agent.setModificationHandlingFees(agentHandlingFeesForMod);
			
			Agent newAgent = ModuleServiceLocator.getTravelAgentBD().saveAgent(agent);

			if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
				try {
					ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(newAgent);
				} catch (ModuleException ex) {
					log.error("Failed while publishing Agent Info into LCC");
				}
			}

			notifyAgentModifications(request, newAgent);
			agent = null;
			newAgent = null;
		}

		// sets the save Message
		if (!isExceptionOccured) {
			if (agp.getProperty(PARAM_MODEL).equals(WebConstants.ACTION_ADD)) {
				saveMessage(request, xbeConfig.getMessage("cc.airadmin.add.success", null), WebConstants.MSG_SUCCESS);
			} else if (agp.getProperty(PARAM_MODEL).equals(WebConstants.ACTION_EDIT)) {
				saveMessage(request, xbeConfig.getMessage("cc.airadmin.edit.success", null), WebConstants.MSG_SUCCESS);
			} else {
				saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS, null), WebConstants.MSG_SUCCESS);
			}
		}
		return isExceptionOccured;
	}
	
	
	
	
	private static Set<String> getAssignableTerritoriesStations(String[] assignedTerritoryStations,
			List<String[]> visibleTerritoryStationList) {

		Set<String> assignableList = new HashSet<String>();
		for (String territoryStation : assignedTerritoryStations) {
			boolean allAssignedVisible = false;
			for (String[] visibleTerritoryStation : visibleTerritoryStationList) {
				if (visibleTerritoryStation[0].equals(territoryStation)) {
					allAssignedVisible = true;
					break;
				}
			}
			if (allAssignedVisible) {
				assignableList.add(territoryStation);
				continue;
			} else {
				assignableList = new HashSet<String>();
				break;
			}
		}

		return assignableList;
	}
	
	
	private static List<String[]> getVisibleStationList() {
		String[] arg = new String[1];
		int[] argType = new int[1];
		arg[0] = userInfoTO.getAgentCode();
		argType[0] = java.sql.Types.VARCHAR;

		Agent agent = null;
		try {
			agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userInfoTO.getAgentCode());
		} catch (ModuleException e) {
			log.error("Error while retrieving agent", e);
		}

		List<String[]> stationList = null;
		if (AppSysParamsUtil.getCarrierAgent().equals(userInfoTO.getAgentType())) {
			stationList = LookupUtils.LookupDAO().getTwoColumnMap("stationsWithActiveStatus");
		} else if (AgentType.GSA.equals(userInfoTO.getAgentType())) {
			String[] argsForGSA = new String[2];
			argsForGSA[0] = arg[0];
			argsForGSA[1] = arg[0];
			//update agent type
			argType = new int[2];
			argType[0] = java.sql.Types.VARCHAR;
			argType[1] = java.sql.Types.VARCHAR;
			stationList = LookupUtils.LookupDAO().getTwoColumnMap("visibleStationsForGSA", argsForGSA, argType);
		} else if (AppSysParamsUtil.isGSAStructureVersion2Enabled() && agent != null && agent.getGsaCode() == null
				&& AgentType.SGSA.equals(userInfoTO.getAgentType())) {
			stationList = LookupUtils.LookupDAO().getTwoColumnMap("stationsWithActiveStatus");
		} else if (AgentType.SGSA.equals(userInfoTO.getAgentType())) {
			stationList = LookupUtils.LookupDAO().getTwoColumnMap("visibleStationsForSGSA", arg, argType);
		}

		return stationList;
	}

	/**
	 * 
	 * @param currType
	 * @param currCode
	 * @return
	 */
	private static boolean validatePayCurrType(String currType, String currCode) {
		String baseCurr = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		if (currCode.equalsIgnoreCase(baseCurr) && (currType.equals("B"))) {
			return true;
		} else if (!currCode.equalsIgnoreCase(baseCurr) && (currType.equals("L"))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Delete the Travel Agent
	 * 
	 * @param request
	 */
	private static void deleteData(HttpServletRequest request) {

		Agent gsaV2Agent = null;
		Agent gsaV2DelAgent = null;
		
		String strCode = request.getParameter("hdnCode");
		String strVersion = request.getParameter("hdnVersion");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (strCode != null) {
				Agent delAgent = new Agent();
				delAgent.setAgentCode(strCode);
				delAgent.setVersion(new Long(strVersion).longValue());
				// update parent agents available credit
				if (isGSAV2Enabled) {
					gsaV2DelAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(strCode);
					if (gsaV2DelAgent != null && YES.equals(gsaV2DelAgent.getHasCreditLimit())
							&& gsaV2DelAgent.getGsaCode() != null && !"".equals(gsaV2DelAgent.getGsaCode())) {
						gsaV2Agent = getFixedCreditAgent(ModuleServiceLocator.getTravelAgentBD().getAgent(
								gsaV2DelAgent.getGsaCode()));
						if (gsaV2Agent != null && gsaV2Agent.getAgentSummary() != null) {
							gsaV2Agent.getAgentSummary().setAvailableCredit(
									AccelAeroCalculator.add(gsaV2Agent.getAgentSummary().getAvailableCredit(),
											gsaV2DelAgent.getTotalCreditLimit()));
						}
					}
				}
				ModuleServiceLocator.getTravelAgentBD().removeAgent(delAgent);
				if(isGSAV2Enabled && gsaV2Agent != null && gsaV2Agent != null){
					ModuleServiceLocator.getTravelAgentBD().saveAgent(gsaV2Agent);
				}
				saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("DELETE FAILED +" + e.getMessage());
		}
	}

	/**
	 * Send Notification Emails for the mentioned email address regarding agent modifications
	 * @param request
	 * @param agent
	 * */
	private static boolean notifyAgentModifications(HttpServletRequest request, Agent agent){
		String emailsList = request.getParameter(PARAM_EMAILS_TO_NOTIFY);
		String hdnModel = request.getParameter(PARAM_MODEL);
		String agentCountry = request.getParameter(PARAM_AGENT_COUNTRY);

		if (!emailsList.equals("")) {

			MessagingServiceBD messagingServiceBD = ModuleServiceLocator.getMessagingBD();
			String[] listOfEmails = emailsList.split(",");
			HashMap<String, Object> map = new HashMap<String, Object>();
			MessageProfile msgProfile = new MessageProfile();
			UserMessaging usermsg = new UserMessaging();
			List<UserMessaging> messages = new ArrayList<UserMessaging>();
			Calendar cal = Calendar.getInstance();
			
			map.put("agentID", agent.getAgentCode());
			map.put("agentTypeCode", agent.getAgentTypeCode());
			map.put("agentName", agent.getAgentName());
			map.put("agentDisplayName", agent.getAgentDisplayName());
			map.put("station", agent.getStationCode());
			//map.put("country", agent.get)
			map.put("currency", agent.getCurrencyCode());
			map.put("paymentMethod", agent.getPaymentMethod());
			
			if(agent.getAddressLine1().equals("deleted") || agent.getAddressLine1() == null){
				map.put("addressLine1", "");
			}else{
				map.put("addressLine1", agent.getAddressLine1());
			}
			
			if(agent.getAddressLine2().equals("deleted") || agent.getAddressLine2() == null){
				map.put("addressLine2", "");
			}else{
				map.put("addressLine2", agent.getAddressLine2());
			}
			
			if(agent.getAddressStateProvince().equals("deleted") || agent.getAddressStateProvince() == null){
				map.put("getAddressStateProvince", "");
			}else{
				map.put("getAddressStateProvince", agent.getAddressStateProvince());
			}
			
			map.put("addressCity", agent.getAddressCity());
			map.put("telephone", agent.getTelephone());
			map.put("dayOfWeek", new SimpleDateFormat("EEEE").format(cal.getTime()));  // the day of the week spelled out completely 
			map.put("month", new SimpleDateFormat("MMMM").format(cal.getTime()));
		    map.put("dayOfMonth", String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
		    map.put("year", String.valueOf(cal.get(Calendar.YEAR)));
		    map.put("country", agentCountry);
		    map.put("carrierName", AppSysParamsUtil.getDefaultCarrierName());
		    
			Topic topic = new Topic();
			topic.setTopicName(Agent.NOTIFY_AGENT_MODIFICATIONS_EMAIL);

			if (hdnModel.equals(PARAM_MODEL_ADD)) {
				map.put("subject","New " + agent.getAgentTypeCode() + " Created");				
			} else if (hdnModel.equals(PARAM_MODEL_EDIT)) {				
				map.put("subject","Modification of " + agent.getAgentTypeCode());
			}

			topic.setTopicParams(map);
			messages.add(usermsg);
			
			msgProfile.setTopic(topic);
			msgProfile.setUserMessagings(messages);
			
			for(String address : listOfEmails){
				usermsg.setToAddres(address);
				messagingServiceBD.sendMessage(msgProfile);
			}
			
		} else {
			log.debug("\nEmpty List of Email addresses to notify : notifyAgentModifications()");
		}

		return true;
	}

	/**
	 * Saves the Agents Credit History
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void saveCreditLimit(HttpServletRequest request) throws ModuleException {

		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		boolean blnForceCancel;

		String strAgentCode = StringUtil.getNotNullString(request.getParameter(PARAM_AGENTCODE));
		String strNewLimit = StringUtil.getNotNullString(request.getParameter(PARAM_NEWCREDITLIMIT));
		String strRemarks = StringUtil.getNotNullString(request.getParameter(PARAM_REMARKS));
		String strCurrentCredit = StringUtil.getNotNullString(request.getParameter(PARAM_CURRENTCREDIT));
		String strForceCancel = StringUtil.getNotNullString(request.getParameter(PARAM_FORCECANCEL));

		String payCurrCode = StringUtil.getNotNullString(request.getParameter(PAY_CURR_CODE_CREDIT));

		String strCurrentCreditLocal = StringUtil.getNotNullString(request.getParameter(PARAM_CURENTCREDIT_LOCAL));
		String strGurrenteeLoc = StringUtil.getNotNullString(request.getParameter(PARAM_BANKGURRENT_LOCAL));

		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(strAgentCode);

		String agentName = agent.getAgentDisplayName();
		BigDecimal bankGuarantee = agent.getBankGuaranteeCashAdvance();
		BigDecimal creditLimit = agent.getCreditLimit();
		BigDecimal locCredit = agent.getCreditLimitLocal();
		String strSelCurrIn = agent.getCurrencySpecifiedIn();
		String agentTypeCode = agent.getAgentTypeCode();

		String agCurr = agent.getCurrencyCode();
		strCurrentCredit = decimalFormat.format(creditLimit);
		strCurrentCreditLocal = decimalFormat.format(locCredit);
		// strGurrenteeLoc = decimalFormat.format(bankGuarantee);

		String sltdCirr = agentCurrencyType(strSelCurrIn, agCurr);

		String strFormData = "var arrFormData = new Array();";
		strFormData += "arrFormData[0] = '" + strAgentCode + "';";
		strFormData += "arrFormData[1] = '" + strCurrentCredit + "';";
		strFormData += "arrFormData[2] = '" + strNewLimit + "';";
		strFormData += "arrFormData[3] = '" + getAgentRemarksHTML(strRemarks) + "';";
		strFormData += "arrFormData[4] = '" + strForceCancel + "';";
		strFormData += "arrFormData[5] = '" + agentName + "';";
		strFormData += "arrFormData[6] = '" + bankGuarantee + "';";
		strFormData += "arrFormData[7] = '" + strCurrentCreditLocal + "';";
		strFormData += "arrFormData[8] = '" + strGurrenteeLoc + "';";
		strFormData += "arrFormData[9] = '" + sltdCirr + "';";
		strFormData += "arrFormData[10] = '" + strSelCurrIn + "';";
		strFormData += "arrFormData[11] = '" + agentTypeCode + "';";

		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		BigDecimal fltNewLimit = new BigDecimal(strNewLimit);
		BigDecimal fltCurrentLimit = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (Agent.CURRENCY_LOCAL.equals(strSelCurrIn)
				&& !payCurrCode.trim().equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY))) {
			fltCurrentLimit = AccelAeroCalculator.getTwoScaledBigDecimalFromString(strCurrentCreditLocal);
		} else {
			fltCurrentLimit = AccelAeroCalculator.getTwoScaledBigDecimalFromString(strCurrentCredit);
		}

		if (strForceCancel.equals("true")) {
			blnForceCancel = true;
		} else {
			blnForceCancel = false;
		}

		ModuleServiceLocator.getTravelAgentBD().adjustCreditLimit(strAgentCode, fltCurrentLimit, fltNewLimit, strRemarks,
				blnForceCancel, strSelCurrIn);
	}

	/**
	 * JIRA :AARESAA-3026 (Lalanthi - Date :02/10/2009) Formats the remark text to ignore carriage returns/ new lines
	 * 
	 * @param remark
	 * @return
	 */
	private static String getAgentRemarksHTML(String remark) {
		String strRem = "";
		if (remark != null && remark.length() > 0) {
			StringTokenizer st = new StringTokenizer(remark, "\n");
			while (st.hasMoreTokens()) {
				strRem += st.nextToken();
			}
			strRem = strRem.replaceAll("\r", " ");
		}
		return strRem;
	}

	/**
	 * Sets the Display Data for Travel Agent Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param isExceptionOccured
	 *            the Exception status
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request, boolean isExceptionOccured, UserAgentInfoTO agentInfoTO)
			throws Exception {
		String agentCode = null;
		String gsaCode = "";
		if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
			if (!AppSysParamsUtil.getCarrierAgent().equals(userInfoTO.getAgentType())) {
				agentCode = agentInfoTO.getAgentCode();
				log.debug("setDisplayData USER AGENT CODE " + agentCode);
			}
		} else if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_GSA)) {
			agentCode = agentInfoTO.getAgentCode();
			log.debug("setDisplayData USER AGENT CODE " + agentCode);
		} else {
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		}
		if (isGSAV2Enabled && agentCode != null && !agentCode.isEmpty()) {
			String parentAgent = agentCode;
			boolean foundGsa = false;
			do {
				Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(parentAgent);
				if (agent != null) {
					if (agent.getGsaCode() != null && !agent.getGsaCode().isEmpty()) {
						parentAgent = agent.getGsaCode();
					} else {
						foundGsa = true;
					}
				} else {
					foundGsa = true;
				}
			} while (!foundGsa);
			if(!(AgentType.TA.equals(userInfoTO.getAgentType()) || AgentType.STA.equals(userInfoTO.getAgentType()))){
				gsaCode = parentAgent;
			}		
		}
		
		setClientErrors(request);
		TravelAgentHTMLGenerator.setTravelAgentListHtml(request);
		setServiceChannelsHtml(request);
		setHandlingFeeAppliesToHtml(request);
		setChargeFareBasisToHtml(request);
		setAgentStatusListHtml(request);
		setAgentComboList(request, agentCode, agentInfoTO.getAirlineCode());
		setGSAComboList(request);
		setTerritoryComboList(request, agentCode);
		setOwnTerritoryComboList(request, agentCode);
		setStationComboList(request, agentCode, gsaCode);
		setStationTerritoryList(request, agentCode);
		setCurrencyComboList(request, agentCode);
		setCurrencyExchangeList(request);
		setReportingAgentList(request, agentInfoTO);
		//setGSAList(request, AgentTypeTO.GSA.equals(agentInfoTO.getAgentType()) ? agentInfoTO.getAgentCode() : null);
		
		setEnableMultipleGSAsForAStation(request);

		setHandlingChargesTimeBizParam(request);
		setStationCountry(request, agentCode);
		setStationCurrency(request);
		setAgentTypeCreditApplicableData(request);
		setDefaultBaseCurrency(request);
		setPaymentTypeList(request);
		setAccountCode(request);
		setCarrierAgent(request);
		setAgentCommiListHtml(request);
		setAddingPrivilege(request);
		setAirlineCodeList(request);
		setTravelAgentRowHtml(request, agentCode);
		setLanguageHtml(request, agentInfoTO);
		setAllowLocalCreditLimit(request, agentInfoTO);
		userAgentType(request, agentInfoTO.isGSA());
		setAllowLocalCreditDef(request);
		setAgentCurrType(request, agentInfoTO.getAgentsCurrType());
		creditApplicability(request, agentInfoTO.getUserPaymentInfo());
		setCarrierPrefixStatus(request);
		setCurrentDate(request);
		showFareMaskOption(request);
		showCharterAgentOption(request);
		setAirportList(request);

		//HFM
		
		setHandlingModificationOps(request);
		setHandlingModificationOpStatus(request);
		
		setEnableGSAV2(request,isGSAV2Enabled);
		if(isGSAV2Enabled) {
			setVisibleTerritoriesAndStations(request);
			setAgentCreditSummary(request);
		}
		
		if (!isExceptionOccured) {
			String strCreditMode = request.getParameter("hdnCMode");
			if (strCreditMode != null && strCreditMode.equals("SAVEADJPAAYMENT")) {

			} else {
				String strFormData = "var arrFormData = new Array();";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			}
		}
		request.setAttribute(WebConstants.REQ_DEFAULT_AIRLINE_CODE, AppSysParamsUtil.getDefaultAirlineIdentifierCode());
		UserPrincipal principal = (UserPrincipal) request.getUserPrincipal();
		if (principal != null) {
			request.setAttribute(WebConstants.REQ_IS_REMOTE_USER, "false");
		}
		// set handling fee privilege
		request.setAttribute(WebConstants.REQ_AGENT_HANDLING_FEE_ENABLE,
				hasPrivilege(request, WebConstants.PRIV_TA_MAINT_HANDLING_FEE));
		request.setAttribute(WebConstants.REQ_AGENT_LEVEL_ETICKETS, AppSysParamsUtil.isAgentWiseTicketEnable());
		
		request.setAttribute(WebConstants.REQ_AGENT_ROUTE_SELECTIONS,
				hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ROUTE_SELECTION));
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportsWithStatusList();
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, strHtml);
	}

	private static void setCarrierPrefixStatus(HttpServletRequest request) {
		boolean isAllowAirlineCarrierPrefixForAgentsAndUsers = AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers();
		request.setAttribute(WebConstants.REQ_ALLOW_CARRIER_PREFIX, isAllowAirlineCarrierPrefixForAgentsAndUsers);
	}

	private static void showFareMaskOption(HttpServletRequest request) {
		boolean showFareMaskOption = AppSysParamsUtil.showFareMaskOptionInXBE();
		request.setAttribute(WebConstants.REQ_SHOW_FARE_MASK_OPTION, showFareMaskOption);
	}

	private static void showCharterAgentOption(HttpServletRequest request) {
		boolean showCharterAgentOption = AppSysParamsUtil.showCharterAgentInXBE();
		request.setAttribute(WebConstants.REQ_SHOW_CHARTER_AGENT_OPTION, showCharterAgentOption);
	}

	private static void setCurrencyExchangeList(HttpServletRequest request) {
		StringBuffer xchangeList = new StringBuffer("var arrExchangeRate= new Array();");
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap("currencyExchangeRate");
		Iterator<String[]> iter = l.iterator();
		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			xchangeList.append("arrExchangeRate[" + i + "] = new Array('" + s[0] + "','" + s[1] + "'); ");
			i++;
		}
		request.setAttribute(WebConstants.REQ_CURRENCY_EXCHANGE_LIST, xchangeList.toString());
	}

	// FIXME This should be moved to HTML generator
	private static void setLanguageHtml(HttpServletRequest request, UserAgentInfoTO agentInfoTO) {
		Map mapSupportedLanguages = config.getSupportedLanguages();
		Set colKey = mapSupportedLanguages.keySet();
		StringBuffer sb = new StringBuffer();
		sb.append("<select id='selLanguage'>");
		for (Iterator iter = colKey.iterator(); iter.hasNext();) {

			String key = (String) iter.next();
			if (!agentInfoTO.isGSA() && key.equals("en")) {
				sb.append("<option value='");
				sb.append(key);
				sb.append("' selected='selected'");
				sb.append(">");
				sb.append(mapSupportedLanguages.get(key));
				sb.append("</option>");
			} else if (!agentInfoTO.isGSA()) {
				sb.append("<option value='");
				sb.append(key);
				sb.append("'");
				sb.append(">");
				sb.append(mapSupportedLanguages.get(key));
				sb.append("</option>");
			} else if (agentInfoTO.isGSA() && key.equals(agentInfoTO.getAgentLanguage())) {
				sb.append("<option value='");
				sb.append(key);
				sb.append("' selected='selected'");
				sb.append(">");
				sb.append(mapSupportedLanguages.get(key));
				sb.append("</option>");
			} else {
				sb.append("<option value='");
				sb.append(key);
				sb.append("'");
				sb.append(">");
				sb.append(mapSupportedLanguages.get(key));
				sb.append("</option>");
			}

		}
		sb.append("</select>");
		request.setAttribute(WebConstants.REQ_LANGUAGE_HTML, sb.toString());
	}

	private static void creditApplicability(HttpServletRequest request, Set paymentInfo) {
		Iterator<String> iter = paymentInfo.iterator();
		StringBuilder strPayOp = new StringBuilder();
		strPayOp.append("");
		while (iter.hasNext()) {
			String strPM = iter.next();
			strPayOp.append(strPM).append(",");
		}
		request.setAttribute(WebConstants.REQ_AGENT_PAYMENT_OPTIONS, strPayOp.toString());
	}

	private static boolean isPrivilegedUser(HttpServletRequest request, String agentCode) {
		if ((hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_GSA) || hasPrivilege(request,
				WebConstants.PRIV_TA_MAINT_ADD_ANY)) && (!agentCode.equals(WebConstants.AGENT_TYPE_TRAVEL_AGENT))) {
			return true;
		} else {
			return false;
		}

	}

	private static void setAddingPrivilege(HttpServletRequest request) throws ModuleException {
		if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
			request.setAttribute(WebConstants.REQ_SET_PRIVILEGE, "blnsearchall = true;");
		}
	}

	private static void setCreditHistoryRowHtml(HttpServletRequest request) throws ModuleException {
		TravelAgentHTMLGenerator htmlGen = new TravelAgentHTMLGenerator();
		String strHtml = htmlGen.getCreditHistoryRowHtml(request);
		request.setAttribute(WebConstants.REQ_CREDIT_HISTORY_ROWS, strHtml);
	}

	private static void setAgentCurrType(HttpServletRequest request, String currCode) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_USER_CURR, currCode);
	}

	/**
	 * Sets the Agent Status Type List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentStatusListHtml(HttpServletRequest request) throws ModuleException {
		String strAgentList = SelectListGenerator.createAgentStatusList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTSTATUS_LIST, strAgentList);
	}

	/**
	 * Set the ServiceChannels to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuelException
	 */
	private static void setServiceChannelsHtml(HttpServletRequest request) throws ModuleException {
		String strSrvChs = SelectListGenerator.createServiceChannels();
		request.setAttribute(WebConstants.REQ_HTML_SERVICE_CHANNELS, strSrvChs);
	}

	/**
	 * Set the HandlingFeeAppliesTo to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuelException
	 */
	private static void setHandlingFeeAppliesToHtml(HttpServletRequest request) throws ModuleException {
		String strSrvChs = SelectListGenerator.createHandlingFeeAppliesToDisplayList();
		request.setAttribute(WebConstants.REQ_CHARGE_APPLIES_TO_LIST, strSrvChs);
	}

	/**
	 * Set the GSA List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuelException
	 */
	private static void setGSAList(HttpServletRequest request, String agentCode) throws ModuleException {
		String strGSAList = SelectListGenerator.createGSAList(agentCode);
				
		request.setAttribute(WebConstants.REQ_GSA_LIST, strGSAList);
	}
	
	private static void setReportingAgentList(HttpServletRequest request, UserAgentInfoTO agentInfo) throws ModuleException {
		String strReportingAgentList = SelectListGenerator.createReportingAgentList(agentInfo.getAgentCode(), agentInfo.getAgentType());		
		request.setAttribute(WebConstants.REQ_GSA_LIST, strReportingAgentList);
	}


	/**
	 * Set the property to enable multiple GSAs for a station to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuelException
	 */
	private static void setEnableMultipleGSAsForAStation(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_MULTIPLE_GSAS_FOR_A_STATION, AppSysParamsUtil.enableMultipleGSAsForAStation());
	}
	
	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setEnableGSAV2(HttpServletRequest request, boolean enabled) throws ModuleException {
		request.setAttribute(WebConstants.REQ_GSA_V2, enabled);
	}

	/**
	 * Set the HandlingFeeAppliesTo to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuelException
	 */
	private static void setChargeFareBasisToHtml(HttpServletRequest request) throws ModuleException {
		String strSrvChs = SelectListGenerator.createFareBasisToDisplayList();
		request.setAttribute(WebConstants.REQ_CHARGE_FAREBASIS_TO_LIST, strSrvChs);
	}

	/**
	 * Sets the Airline code list
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setAirlineCodeList(HttpServletRequest request) throws ModuleException {

		String strAirLineList = "";
		strAirLineList = SelectListGenerator.getAirlineCodeList();
		request.setAttribute(WebConstants.REQ_AIRLINE_SELECT_LIST, strAirLineList);
	}

	private static void setTravelAgentRowHtml(HttpServletRequest request, String gsaCode) throws ModuleException {
		TravelAgentHTMLGenerator htmlGen = new TravelAgentHTMLGenerator();
		String terrotitryCode = null;
		String loggedInUsersGSA = null;

		if (gsaCode != null) {
			terrotitryCode = SelectListGenerator.getAgentTerrotiry(gsaCode);
		}

		if (AppSysParamsUtil.enableMultipleGSAsForAStation()) {
			loggedInUsersGSA = gsaCode;
		}
		
		boolean isAirlineAgent = AppSysParamsUtil.getCarrierAgent().equals(userInfoTO.getAgentType());
		String strHtml = htmlGen.getTravelAgentRowHtml(request, terrotitryCode, loggedInUsersGSA, isAirlineAgent);
		request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
	}

	/**
	 * Sets the Agents List to the Request to be Used in the Combo Box
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentComboList(HttpServletRequest request, String agentCode, String airlineCode)
			throws ModuleException {

		String strAgentComboList = "";
		if (agentCode != null) {
			strAgentComboList = SelectListGenerator.createAgentListWOTag(agentCode, airlineCode);
		} else {
			strAgentComboList = SelectListGenerator.createAgentListWOTag();
		}

		request.setAttribute(WebConstants.REQ_HTML_AGENT_COMBO, strAgentComboList);
	}

	/**
	 * Sets the GSA list to the Request to be used in the Combo Box
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setGSAComboList(HttpServletRequest request) throws ModuleException {

		String strGSAComboList = SelectListGenerator.createGSAListWOTag();
		request.setAttribute(WebConstants.REQ_HTML_GSA_COMBO, strGSAComboList);
	}

	/**
	 * Sets the Station List to be used in the Combo Box
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStationComboList(HttpServletRequest request, String agentCode, String gsaCode) throws ModuleException {
		String strStationComboList = "";
		if (agentCode != null) {
			strStationComboList = SelectListGenerator.createStationComboListWOTag(agentCode, gsaCode);
		} else {
			strStationComboList = SelectListGenerator.createStationComboListWOTag();
		}
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, strStationComboList);
	}

	/**
	 * Sets the Territory List to the Request to be Used in the Combo Box
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOwnTerritoryComboList(HttpServletRequest request, String gsaCode) throws ModuleException {
		String strTerritoryComboList = "";
		if (gsaCode != null) {
			strTerritoryComboList = SelectListGenerator.createOwnTerritoryComboListWOTag(gsaCode);
		} else {
			strTerritoryComboList = SelectListGenerator.createOwnAirlineTerritoryComboListWOTag();
		}
		request.setAttribute(WebConstants.REQ_HTML_OWN_TERRITORY_COMBO, strTerritoryComboList);
	}

	/**
	 * Sets the Territory List to the Request to be Used in the Combo Box
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setTerritoryComboList(HttpServletRequest request, String gsaCode) throws ModuleException {
		String strTerritoryComboList = "";
		if (gsaCode != null) {
			strTerritoryComboList = SelectListGenerator.createTerritoryComboListWOTag(gsaCode);
		} else {
			strTerritoryComboList = SelectListGenerator.createTerritoryComboListWOTag();
		}
		request.setAttribute(WebConstants.REQ_HTML_TERRITORY_COMBO, strTerritoryComboList);
	}

	/**
	 * Sets the Airport Territory list to the Request
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @throws ModuleExceptionModuleException
	 */
	private static void setStationTerritoryList(HttpServletRequest request, String gsaCode) throws ModuleException {
		String strStationTerritory = "";
		if (gsaCode != null) {
			strStationTerritory = SelectListGenerator.gsaTerritoryStationList(gsaCode);
		} else {
			strStationTerritory = SelectListGenerator.createAirportTerritoryList();
		}
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_TERRITORY_LIST, strStationTerritory);
	}

	/**
	 * Sets the Currency List to be Used in the combo Box
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCurrencyComboList(HttpServletRequest request, String gsaCode) throws ModuleException {
		String strCurrencyComboList = "";
		if (gsaCode != null) {
			StringBuilder ssb = new StringBuilder();
			String strcur = SelectListGenerator.getAgentCurrency(gsaCode);
			ssb.append("arrCurrency[0] = new Array('" + strcur + "','" + strcur + "'); ");
			// if(!defaultBaseCurrency.equalsIgnoreCase(strcur)){
			// ssb.append("arrCurrency[1] = new Array('" + defaultBaseCurrency +
			// "','" + defaultBaseCurrency + "'); ");
			// }
			strCurrencyComboList = ssb.toString();
		} else {
			strCurrencyComboList = SelectListGenerator.createCurrencyCodeListWOTag();
		}
		request.setAttribute(WebConstants.REQ_HTML_CURRENCY_COMBO, strCurrencyComboList);
	}

	/**
	 * Sets the Station Against Country to the Request
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStationCountry(HttpServletRequest request, String gsaCode) throws ModuleException {
		String strStationCountry = "";
		if (gsaCode != null) {
			strStationCountry = SelectListGenerator.createStationCountryListWOTag(gsaCode);
		} else {
			strStationCountry = SelectListGenerator.createStationCountryListWOTag();
		}
		request.setAttribute(WebConstants.REQ_HTML_STATION_COUNTRY, strStationCountry);
	}

	/**
	 * Sets the Station Against Currency to the Request
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStationCurrency(HttpServletRequest request) throws ModuleException {
		String strStationCurrency = "";
		strStationCurrency = SelectListGenerator.createStationCurrencyListWOTag();
		request.setAttribute(WebConstants.REQ_HTML_STATION_CURRENCY, strStationCurrency);
	}

	/**
	 * Sets the Agent Types Credit Applicability to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentTypeCreditApplicableData(HttpServletRequest request) throws ModuleException {
		TravelAgentHTMLGenerator htmlGen = new TravelAgentHTMLGenerator();
		String strHtml = htmlGen.getAgentTypeCreditApplicableRows(request);
		request.setAttribute(WebConstants.REQ_AGENT_TYPE_CREDIT_APPLICABLE, strHtml);
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = TravelAgentHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null)
			strClientErrors = "";

		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Default handling Charges to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setHandlingChargesTimeBizParam(HttpServletRequest request) {
		String defaultHandlingCharges = globalConfig.getBizParam(SystemParamKeys.HANDLING_CHARGES);
		request.setAttribute(WebConstants.REQ_HANDLING_CHARGES, defaultHandlingCharges);
	}

	/**
	 * Sets Default base Currency to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setDefaultBaseCurrency(HttpServletRequest request) {
		String defaultBaseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		request.setAttribute(WebConstants.REQ_BASE_CURRENCY, defaultBaseCurrency);

	}

	/**
	 * Sets the Agent Collection Types to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setPaymentTypeList(HttpServletRequest request) throws ModuleException {
		String strList = SelectListGenerator.createCollectionTypeList();
		request.setAttribute(WebConstants.REQ_HTML_PAYMENTTYPE_LIST, strList);
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (xbeConfig.getMessage("cc.airadmin.savemessage.display", null).equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	private static boolean isHandlingFeeDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (xbeConfig.getMessage("cc.airadmin.savemessage.display", null).equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * Sets Accoount Code for Carrier
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAccountCode(HttpServletRequest request) throws ModuleException {
		String strACCCode = globalConfig.getBizParam(SystemParamKeys.ACCOUNT_CODE);
		request.setAttribute(WebConstants.REQ_ACCOUNT_CODE, strACCCode);
	}

	/**
	 * Sets the Default Agent Type for Carrier like AA
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCarrierAgent(HttpServletRequest request) throws ModuleException {
		String strCarrierAgent = AppSysParamsUtil.getCarrierAgent();
		request.setAttribute(WebConstants.REQ_CARRIER_AGENT, strCarrierAgent);
	}

	/**
	 * Sets the Agent Commission Type List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentCommiListHtml(HttpServletRequest request) throws ModuleException {
		String strCommiTypeList = SelectListGenerator.createAgentCommiListHtml();
		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.AGENT_COMMISSION))) {
			request.setAttribute(WebConstants.REQ_HTML_COMMITION_TYPE_LIST, strCommiTypeList);
		} else {
			request.setAttribute(WebConstants.REQ_HTML_COMMITION_TYPE_LIST, "INA");
		}
	}

	private static void userAgentType(HttpServletRequest request, boolean isGSA) throws ModuleException {
		if (isGSA) {
			request.setAttribute(WebConstants.REQ_HTML_USER_AGENT_TYPE, WebConstants.AGENT_TYPE_GSA);
		} else {
			request.setAttribute(WebConstants.REQ_HTML_USER_AGENT_TYPE, "");
		}
		request.setAttribute(WebConstants.REQ_HTML_AIRLINE_USER_AGENT_TYPE, AppSysParamsUtil.getCarrierAgent());
		request.setAttribute(WebConstants.REQ_HTML_ACTUAL_USER_AGENT_TYPE,
				((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode());
	}

	private static void setAllowLocalCreditDef(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_ALLOW_LOCAL_CR, AppSysParamsUtil.isAllowLocalCreditLimitDefinition());
	}

	/**
	 * Allow to specify credit limit in Local
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setAllowLocalCreditLimit(HttpServletRequest request, UserAgentInfoTO userAgentInfoTO)
			throws ModuleException {
		String defaultBaseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		StringBuilder strAllowLocalCredit = new StringBuilder();
		if (!userAgentInfoTO.isGSA()) {
			strAllowLocalCredit.append("<option value='" + defaultBaseCurrency + "'>" + defaultBaseCurrency + "</option>");
			if (AppSysParamsUtil.isAllowLocalCreditLimitDefinition()) {

			}
		} else {
			if ((userAgentInfoTO.getAgentsCurrType() != null && userAgentInfoTO.getAgentsCurrType().equals(Agent.CURRENCY_LOCAL))
					&& AppSysParamsUtil.isAllowLocalCreditLimitDefinition()) {
				strAllowLocalCredit.append("<option value='" + userAgentInfoTO.getAgentsCurrCode() + "'>"
						+ userAgentInfoTO.getAgentsCurrCode() + "</option>");
			} else {
				strAllowLocalCredit.append("<option value='" + defaultBaseCurrency + "'>" + defaultBaseCurrency + "</option>");
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_ALLOW_LOCAL_AGENT, strAllowLocalCredit.toString());
	}

	private static boolean validateTaCurrency(String gsaCode, String agentCurrency) throws ModuleException {

		Agent gsa = ModuleServiceLocator.getTravelAgentBD().getAgent(gsaCode);
		boolean blnValidCurrency = false;
		if (gsa != null) {
			if (gsa.getStatus().equals(Agent.STATUS_ACTIVE) && (gsa.getAgentTypeCode().equals(AgentType.GSA) || gsa.getAgentTypeCode().equals(AgentType.SGSA) || gsa.getAgentTypeCode().equals(AgentType.TA))) {
				if (gsa.getCurrencyCode().equals(agentCurrency)) {
					blnValidCurrency = true;
					return blnValidCurrency;
				}
			}
		}
		return blnValidCurrency;
	}

	/**
	 * validate gsa currencySpecified in
	 * 
	 * @param territory
	 * @param agentCurrency
	 * @return
	 * @throws ModuleException
	 */
	private static boolean validateTaCurrencySpecified(String gsaCode, String agentCurrencySpecified) throws ModuleException {

		Agent gsa = ModuleServiceLocator.getTravelAgentBD().getAgent(gsaCode);
		boolean blnValidCurrency = false;
		if (gsa != null) {
			if (gsa.getStatus().equals(Agent.STATUS_ACTIVE) && (gsa.getAgentTypeCode().equals(AgentType.GSA) || gsa.getAgentTypeCode().equals(AgentType.SGSA)) || gsa.getAgentTypeCode().equals(AgentType.TA)) {
				if (gsa.getCurrencySpecifiedIn().equals(agentCurrencySpecified)) {
					blnValidCurrency = true;
					return blnValidCurrency;
				}
			}
		}
		return blnValidCurrency;
	}

	/**
	 * Validates agent payment methods against user, if the user is GSA, then allow payment methods available only for
	 * the GSA. For non-GSA users, if user has 'Add ANY Travel Agent' privilege, then allow all payment methods, else
	 * allow payment methods available only for the user.
	 * 
	 * @param paymentMethods
	 * @param isIndependentUser
	 * @return Set<String> -paymentMethods
	 * @author lalanthi -Date : 25/11/2009 (JIRA: AARESAA-2553)
	 */
	private static Set<String> setPaymentMethods(String[] paymentMethods, boolean hasPrivilageToAddAny, boolean isUserGSA) {
		Set<String> set1 = new HashSet<String>();
		Set<String> userPaymentInfo = userInfoTO.getUserPaymentInfo();
		if (isUserGSA) {// A GSA is creating this agent, so only allow the
			// payment methods available for GSA
			if (paymentMethods[0].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_ONACCOUNT)) {
				set1.add(Agent.PAYMENT_MODE_ONACCOUNT);
			}
			if (paymentMethods[1].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_CASH)) {
				set1.add(Agent.PAYMENT_MODE_CASH);
			}
			if (paymentMethods[2].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_CREDITCARD)) {
				set1.add(Agent.PAYMENT_MODE_CREDITCARD);
			}
			if (paymentMethods[3].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_BSP)) {
				set1.add(Agent.PAYMENT_MODE_BSP);
			}
			if (paymentMethods[4].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_VOUCHER)) {
				set1.add(Agent.PAYMENT_MODE_VOUCHER);
			}
			if (paymentMethods[5].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_OFFLINE)) {
				set1.add(Agent.PAYMENT_MODE_OFFLINE);
			}
		} else { // Non-GSA user is creating the agent
			if (hasPrivilageToAddAny) {// Allow any the payment method.
				if (paymentMethods[0].equals("true")) {
					set1.add(Agent.PAYMENT_MODE_ONACCOUNT);
				}
				if (paymentMethods[1].equals("true")) {
					set1.add(Agent.PAYMENT_MODE_CASH);
				}
				if (paymentMethods[2].equals("true")) {
					set1.add(Agent.PAYMENT_MODE_CREDITCARD);
				}
				if (paymentMethods[3].equals("true")) {
					set1.add(Agent.PAYMENT_MODE_BSP);
				}
				if (paymentMethods[4].equals("true")) {
					set1.add(Agent.PAYMENT_MODE_VOUCHER);
				}
				if (paymentMethods[5].equals("true")) {
					set1.add(Agent.PAYMENT_MODE_OFFLINE);
				}
			} else {// Allow only the payment methods available for the user.
				if (paymentMethods[0].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_ONACCOUNT)) {
					set1.add(Agent.PAYMENT_MODE_ONACCOUNT);
				}
				if (paymentMethods[1].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_CASH)) {
					set1.add(Agent.PAYMENT_MODE_CASH);
				}
				if (paymentMethods[2].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_CREDITCARD)) {
					set1.add(Agent.PAYMENT_MODE_CREDITCARD);
				}
				if (paymentMethods[3].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_BSP)) {
					set1.add(Agent.PAYMENT_MODE_BSP);
				}
				if (paymentMethods[4].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_VOUCHER)) {
					set1.add(Agent.PAYMENT_MODE_VOUCHER);
				}
				if (paymentMethods[5].equals("true") && userPaymentInfo.contains(Agent.PAYMENT_MODE_OFFLINE)) {
					set1.add(Agent.PAYMENT_MODE_OFFLINE);
				}
			}
		}
		return set1;
	}

	/**
	 * Call From Execte to Display the Credit Limit
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action to pass to execute
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String showCreditAct(HttpServletRequest request, UserAgentInfoTO userAgentInfoTO) throws ModuleException {

		String strHdnCode = request.getParameter("hdnCode");
		String strCredit = request.getParameter("hdnCredit");
		String strSelCurrIn = null;// request.getParameter("selCurrIn");
		String strCurrCode = null; // request.getParameter("hdnCurr");

		DecimalFormat numberFormat = new DecimalFormat("#");

		String strFormData = "var arrFormData = new Array();";
		strFormData += "arrFormData[0] = '" + strHdnCode + "';";
		strFormData += "arrFormData[1] = '" + strCredit + "';";
		strFormData += "arrFormData[2] = '';";
		strFormData += "arrFormData[3] = '';";
		// strFormData += "arrFormData[4] = '';";
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(strHdnCode);
		String agentName = null;
		BigDecimal bankGuarantee = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal locCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal locbankGuarantee = AccelAeroCalculator.getDefaultBigDecimalZero();
		String agencyTypeCode = null;
		if (agent != null) {
			agentName = agent.getAgentDisplayName();
			bankGuarantee = agent.getBankGuaranteeCashAdvance();
			locCredit = agent.getCreditLimitLocal();
			locbankGuarantee = agent.getBankGuaranteeCashAdvanceLocal();

			strCurrCode = agent.getCurrencyCode();
			strSelCurrIn = agent.getCurrencySpecifiedIn();
			agencyTypeCode = agent.getAgentTypeCode();

		}

		strCurrCode = agentCurrencyType(strSelCurrIn, strCurrCode);

		strFormData += "arrFormData[5] = '" + (agentName == null ? "" : agentName) + "';";
		strFormData += "arrFormData[6] = '" + ((bankGuarantee == null) ? "0" : numberFormat.format(bankGuarantee)) + "';";
		strFormData += "arrFormData[7] = '" + ((locCredit == null) ? "0" : numberFormat.format(locCredit)) + "';";
		strFormData += "arrFormData[8] = '" + ((locbankGuarantee == null) ? "0" : numberFormat.format(locbankGuarantee)) + "';";
		strFormData += "arrFormData[9] = '" + (strCurrCode == null ? "" : strCurrCode) + "';";
		strFormData += "arrFormData[10] = '" + strSelCurrIn + "';";
		strFormData += "arrFormData[11] = '" + agencyTypeCode + "';";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setCreditHistoryRowHtml(request);
		setClientErrors(request);
		setDefaultBaseCurrency(request);
		setAllowLocalCreditLimit(request, userAgentInfoTO);
		setAllowLocalCreditDef(request);
		setAgentTypeCreditApplicableData(request);
		// setCurrencyComboList(request, userAgentInfoTO.getAgentCode());
		return S2Constants.Result.CREDIT_LIMIT;

	}

	/**
	 * User is a GSA USER
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	private static UserAgentInfoTO getUserType(String agentCode) throws ModuleException {
		Agent userAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(agentCode);
		if (userAgent != null) {
			UserAgentInfoTO agentInfoTO = new UserAgentInfoTO();
			agentInfoTO.setAgentType(userAgent.getAgentTypeCode());
			agentInfoTO.setAgentLanguage(userAgent.getLanguageCode());
			agentInfoTO.setAgentCode(agentCode);
			agentInfoTO.setAgentsCurrCode(userAgent.getCurrencyCode());
			agentInfoTO.setAgentsCurrType(userAgent.getCurrencySpecifiedIn());
			agentInfoTO.setUserPaymentInfo(userAgent.getPaymentMethod());
			agentInfoTO.setAirlineCode(userAgent.getAirlineCode());
			agentInfoTO.setSharedCreditBasis(userAgent.getHasCreditLimit());
			return agentInfoTO;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @param CurrIn
	 * @param currency
	 * @return
	 */
	private static String agentCurrencyType(String CurrIn, String currency) {
		if (CurrIn.equalsIgnoreCase(Agent.CURRENCY_LOCAL)) {
			return currency;
		} else {
			return globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		}

	}

	/**
	 * Creates a Property file with Agent Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the Property file with Agent Data
	 */
	private static Properties getProperty(HttpServletRequest request) {
		Properties prop = new Properties();
		prop.setProperty(PARAM_AGENTCODE, StringUtil.getNotNullString(request.getParameter(PARAM_AGENTCODE)));
		prop.setProperty(PARAM_TYPE1, StringUtil.getNotNullString(request.getParameter(PARAM_TYPE1)));
		prop.setProperty(PARAM_NAME1, StringUtil.getNotNullString(request.getParameter(PARAM_NAME1)));
		prop.setProperty(PARAM_ADDRESS1, StringUtil.getNotNullString(request.getParameter(PARAM_ADDRESS1)));
		prop.setProperty(PARAM_ADDRESS2, StringUtil.getNotNullString(request.getParameter(PARAM_ADDRESS2)));
		prop.setProperty(PARAM_CITY, StringUtil.getNotNullString(request.getParameter(PARAM_CITY)));
		prop.setProperty(PARAM_TERRITORY_CODE, StringUtil.getNotNullString(request.getParameter(PARAM_TERRITORY_CODE)));
		prop.setProperty(PARAM_CREDITLIMIT, StringUtil.getNotNullString(request.getParameter(PARAM_CREDITLIMIT)));
		prop.setProperty(PARAM_PAYMENTMODE, StringUtil.getNotNullString(request.getParameter(PARAM_PAYMENTMODE)));
		prop.setProperty(PARAM_REPORTING_TO_GSA, StringUtil.getNotNullString(request.getParameter(PARAM_REPORTING_TO_GSA)));
		prop.setProperty(PARAM_REPORTING_GSA, StringUtil.getNotNullString(request.getParameter(PARAM_REPORTING_GSA)));
		prop.setProperty(PARAM_ADVANCEBANK,
				request.getParameter(PARAM_ADVANCEBANK) == null ? "0.0" : request.getParameter(PARAM_ADVANCEBANK));
		prop.setProperty(PARAM_STATUS, StringUtil.getNotNullString(request.getParameter(PARAM_STATUS)));
		prop.setProperty(PARAM_STATE, StringUtil.getNotNullString(request.getParameter(PARAM_STATE)));
		prop.setProperty(PARAM_POSTALCODE, StringUtil.getNotNullString(request.getParameter(PARAM_POSTALCODE)));
		prop.setProperty(PARAM_TELEPHONE, StringUtil.getNotNullString(request.getParameter(PARAM_TELEPHONE)));
		prop.setProperty(PARAM_FAX, StringUtil.getNotNullString(request.getParameter(PARAM_FAX)));
		prop.setProperty(PARAM_EMAIL, StringUtil.getNotNullString(request.getParameter(PARAM_EMAIL)));
		prop.setProperty(PARAM_IATA, StringUtil.getNotNullString(request.getParameter(PARAM_IATA)));
		prop.setProperty(PARAM_ACCOUNTCODE, StringUtil.getNotNullString(request.getParameter(PARAM_ACCOUNTCODE)));
		prop.setProperty(PARAM_VERSION, StringUtil.getNotNullString(request.getParameter(PARAM_VERSION)));
		prop.setProperty(PARAM_BILLING_EMAIL, StringUtil.getNotNullString(request.getParameter(PARAM_BILLING_EMAIL)));
		prop.setProperty(PARAM_HANDLING_CHARGE,
				request.getParameter(PARAM_HANDLING_CHARGE) == null ? "0.0" : request.getParameter(PARAM_HANDLING_CHARGE));
		prop.setProperty(PARAM_CURRENCY_CODE, StringUtil.getNotNullString(request.getParameter(PARAM_CURRENCY_CODE)));
		prop.setProperty(PARAM_NOTES, StringUtil.getNotNullString(request.getParameter(PARAM_NOTES)));
		prop.setProperty(PARAM_STATION_CODE, StringUtil.getNotNullString(request.getParameter(PARAM_STATION_CODE)));
		prop.setProperty(PARAM_MODEL, StringUtil.getNotNullString(request.getParameter(PARAM_MODEL)));
		prop.setProperty(PARAM_CONTACT_PERSON, StringUtil.getNotNullString(request.getParameter(PARAM_CONTACT_PERSON)));
		prop.setProperty(PARAM_LICENSE, StringUtil.getNotNullString(request.getParameter(PARAM_LICENSE)));
		prop.setProperty(PARAM_ON_HOLD, StringUtil.getNotNullString(request.getParameter(PARAM_ON_HOLD)));
		prop.setProperty(PARAM_AGENT_COMMISSION, StringUtil.getNotNullString(request.getParameter(PARAM_AGENT_COMMISSION)));
		prop.setProperty(PARAM_CREDIT_LOCAL,
				request.getParameter(PARAM_CREDIT_LOCAL) == null ? "0" : request.getParameter(PARAM_CREDIT_LOCAL));
		prop.setProperty(PARAM_ADVANCE_LOCAL,
				request.getParameter(PARAM_ADVANCE_LOCAL) == null ? "0" : request.getParameter(PARAM_ADVANCE_LOCAL));
		prop.setProperty(PARAM_CURRENCY_IN, StringUtil.getNotNullString(request.getParameter(PARAM_CURRENCY_IN)));
		prop.setProperty(PARAM_AUTOINVOICE, StringUtil.getNotNullString(request.getParameter(PARAM_AUTOINVOICE)));
		prop.setProperty(PARAM_PREF_LANGUAGE, StringUtil.getNotNullString(request.getParameter(PARAM_PREF_LANGUAGE)));
		prop.setProperty(PARAM_LOCATION_URL, StringUtil.getNotNullString(request.getParameter(PARAM_LOCATION_URL)));
		prop.setProperty(PARAM_PREF_RPT_FORMAT, StringUtil.getNotNullString(request.getParameter(PARAM_PREF_RPT_FORMAT)));

		prop.setProperty(PARAM_SERVICE_CHANNEL, request.getParameter(PARAM_SERVICE_CHANNEL) == null
				? Constants.INTERNAL_CHANNEL
				: request.getParameter(PARAM_SERVICE_CHANNEL));

		prop.setProperty(PARAM_CAPTURE_EXT_PAY, StringUtil.getNotNullString(request.getParameter(PARAM_CAPTURE_EXT_PAY)));
		prop.setProperty(PARAM_EXT_PAY_MANDATORY, StringUtil.getNotNullString(request.getParameter(PARAM_EXT_PAY_MANDATORY)));
		prop.setProperty(PARAM_PUBLISH_TO_WEB, StringUtil.getNotNullString(request.getParameter(PARAM_PUBLISH_TO_WEB)));
		prop.setProperty(PARAM_CONTACT_PERSON_DESIGNATION,
				StringUtil.getNotNullString(request.getParameter(PARAM_CONTACT_PERSON_DESIGNATION)));
		prop.setProperty(PARAM_MOBILE_NUMBER, StringUtil.getNotNullString(request.getParameter(PARAM_MOBILE_NUMBER)));

		// handling fee change
		prop.setProperty(PARAM_HANDLING_FEE_APPLIES_TO,
				StringUtil.getNotNullString(request.getParameter(PARAM_HANDLING_FEE_APPLIES_TO)));
		prop.setProperty(PARAM_HANDLING_FEE_TYPE, StringUtil.getNotNullString(request.getParameter(PARAM_HANDLING_FEE_TYPE)));
		prop.setProperty(PARAM_HANDLING_FEE_OW_AMOUNT,
				StringUtil.getNotNullString(request.getParameter(PARAM_HANDLING_FEE_OW_AMOUNT)));
		prop.setProperty(PARAM_HANDLING_FEE_RT_AMOUNT,
				StringUtil.getNotNullString(request.getParameter(PARAM_HANDLING_FEE_RT_AMOUNT)));
		prop.setProperty(PARAM_HANDLING_FEE_MIN_AMOUNT,
				StringUtil.getNotNullString(request.getParameter(PARAM_HANDLING_FEE_MIN_AMOUNT)));
		prop.setProperty(PARAM_HANDLING_FEE_MAX_AMOUNT,
				StringUtil.getNotNullString(request.getParameter(PARAM_HANDLING_FEE_MAX_AMOUNT)));
		prop.setProperty(PARAM_HANDLING_FEE_ENABLE, StringUtil.getNotNullString(request.getParameter(PARAM_HANDLING_FEE_ENABLE)));

		prop.setProperty(PARAM_BSP_GUATANTEE,
				request.getParameter(PARAM_BSP_GUATANTEE) == null ? "0.0" : request.getParameter(PARAM_BSP_GUATANTEE));
		prop.setProperty(PARAM_BSP_GUATANTEE_LOCAL,
				request.getParameter(PARAM_BSP_GUATANTEE_LOCAL) == null ? "0" : request.getParameter(PARAM_BSP_GUATANTEE_LOCAL));

		// ticketing info
		prop.setProperty(PARAM_TKT_SEQ_ENABLE, StringUtil.getNotNullString(request.getParameter(PARAM_TKT_SEQ_ENABLE)));

		// fare mask info
		prop.setProperty(PARAM_FARE_MASK_ENABLE, StringUtil.getNotNullString(request.getParameter(PARAM_FARE_MASK_ENABLE)));

		// charter agent
		prop.setProperty(PARAM_CHARTER_BOOKING, StringUtil.getNotNullString(request.getParameter(PARAM_CHARTER_BOOKING)));

		prop.setProperty(PARAM_BANK_GURANTEE_EXPIRY_DATE,
				StringUtil.getNotNullString(request.getParameter(PARAM_BANK_GURANTEE_EXPIRY_DATE)));
		prop.setProperty(PARAM_AGENT_INACTIVE_DATE, request.getParameter(PARAM_AGENT_INACTIVE_DATE));
		prop.setProperty(PARAM_EMAILS_TO_NOTIFY, StringUtil.getNotNullString(request.getParameter(PARAM_EMAILS_TO_NOTIFY)));
		prop.setProperty(PARAM_AGENT_COUNTRY, StringUtil.getNotNullString(request.getParameter(PARAM_AGENT_COUNTRY)));
		prop.setProperty(PARAM_OND_LIST, StringUtil.getNotNullString(request.getParameter(PARAM_OND_LIST)));
		prop.setProperty(PARAM_VISIBLE_TERRITORIES_STATIONS, request.getParameter(PARAM_VISIBLE_TERRITORIES_STATIONS));
		prop.setProperty(PARAM_FIXED_CREDIT, StringUtil.getNotNullString(request.getParameter(PARAM_FIXED_CREDIT)));

		prop.setProperty(PARAM_MAX_ADULT_ALLOWED, StringUtil.getNotNullString(request.getParameter(PARAM_MAX_ADULT_ALLOWED)));		
		
		return prop;

	}

	/**
	 * Creates the Form Data from the Property File
	 * 
	 * @param erp
	 *            the Property File with Agent data
	 * @return String the created Form Array
	 */
	private static String getErrorForm(Properties erp) {

		StringBuffer esb = new StringBuffer("var arrFormData = new Array();");
		String strReportingGSA = "";

		esb.append("arrFormData[0] = new Array();");
		esb.append("arrFormData[0][0] = '" + erp.getProperty(PARAM_MODEL) + "';");
		esb.append("arrFormData[0][1] = '" + erp.getProperty(PARAM_AGENTCODE) + "';");
		esb.append("arrFormData[0][2] = '" + erp.getProperty(PARAM_NAME1) + "';");
		esb.append("arrFormData[0][3] = '" + erp.getProperty(PARAM_TERRITORY_CODE) + "';");
		esb.append("arrFormData[0][4] = '';");
		esb.append("arrFormData[0][5] = '" + erp.getProperty(PARAM_CREDITLIMIT) + "';");

		if (erp.getProperty(PARAM_REPORTING_TO_GSA).equals(WebConstants.VAL_CHECKBOX_ON)) {
			strReportingGSA = "Y";
			esb.append("arrFormData[0][66] = '" + erp.getProperty(PARAM_REPORTING_GSA) + "';");
		} else {
			strReportingGSA = "N";
			esb.append("arrFormData[0][66] = '';");
		}
		esb.append("arrFormData[0][6] = '" + strReportingGSA + "';");
		esb.append("arrFormData[0][7] = '" + erp.getProperty(PARAM_STATUS) + "';");
		esb.append("arrFormData[0][8] = '" + erp.getProperty(PARAM_ADDRESS1) + "';");
		esb.append("arrFormData[0][9] = '" + erp.getProperty(PARAM_ADDRESS2) + "';");
		esb.append("arrFormData[0][10] = '" + erp.getProperty(PARAM_STATE) + "';");
		esb.append("arrFormData[0][11] = '" + erp.getProperty(PARAM_POSTALCODE) + "';");
		esb.append("arrFormData[0][12] = '" + erp.getProperty(PARAM_TELEPHONE) + "';");
		esb.append("arrFormData[0][13] = '" + erp.getProperty(PARAM_FAX) + "';");
		esb.append("arrFormData[0][14] = '" + erp.getProperty(PARAM_EMAIL) + "';");
		esb.append("arrFormData[0][15] = '" + erp.getProperty(PARAM_IATA) + "';");
		esb.append("arrFormData[0][16] = '" + erp.getProperty(PARAM_ACCOUNTCODE) + "';");
		esb.append("arrFormData[0][17] = '" + erp.getProperty(PARAM_ADVANCEBANK) + "';");
		esb.append("arrFormData[0][18] = '" + erp.getProperty(PARAM_TYPE1) + "';");
		esb.append("arrFormData[0][19] = '" + erp.getProperty(PARAM_VERSION) + "';");

		String[] strPaymentModeArr = erp.getProperty(PARAM_PAYMENTMODE).split(",");
		if (strPaymentModeArr[0].equals("true")) {
			esb.append("arrFormData[0][20] = 'OA';");
		}
		if (strPaymentModeArr.length > 1 && strPaymentModeArr[1].equals("true")) {
			esb.append("arrFormData[0][21] = 'CA';");
		}
		if (strPaymentModeArr.length > 2 && strPaymentModeArr[2].equals("true")) {
			esb.append("arrFormData[0][22] = 'CC';");
		}

		if (strPaymentModeArr.length > 3 && strPaymentModeArr[3].equals("true")) {
			esb.append("arrFormData[0][63] = 'BSP';");
		}
		if (strPaymentModeArr.length > 4 && strPaymentModeArr[4].equals("true")) {
			esb.append("arrFormData[0][78] = 'VO';");
		}

		esb.append("arrFormData[0][23] = '" + erp.getProperty(PARAM_CITY) + "';");
		esb.append("arrFormData[0][24] = '" + erp.getProperty(PARAM_BILLING_EMAIL) + "';");
		esb.append("arrFormData[0][25] = '" + erp.getProperty(PARAM_HANDLING_CHARGE) + "';");
		esb.append("arrFormData[0][26] = '';");
		esb.append("arrFormData[0][27] = '" + erp.getProperty(PARAM_CURRENCY_CODE) + "';");
		esb.append("arrFormData[0][28] = '" + erp.getProperty(PARAM_NOTES) + "';");
		esb.append("arrFormData[0][29] = '" + erp.getProperty(PARAM_STATION_CODE) + "';");
		
		String strStatTer = "";
		if (("GSA").equals( erp.getProperty(PARAM_TYPE1))) {			
			strStatTer = erp.getProperty(PARAM_TERRITORY_CODE) + " / " + erp.getProperty(PARAM_STATION_CODE);
		} else {
			strStatTer = erp.getProperty(PARAM_STATION_CODE);
		}
		
		esb.append("arrFormData[0][30] = '" + strStatTer + "';");
		// Sticking to the grid values
		esb.append("arrFormData[0][33] = '" + erp.getProperty(PARAM_CONTACT_PERSON) + "';");
		esb.append("arrFormData[0][34] = '" + erp.getProperty(PARAM_LICENSE) + "';");
		esb.append("arrFormData[0][35] = '" + erp.getProperty(PARAM_ON_HOLD) + "';");
		esb.append("arrFormData[0][38] = '" + erp.getProperty(PARAM_ADVANCE_LOCAL) + "';");
		esb.append("arrFormData[0][39] = '" + erp.getProperty(PARAM_CREDIT_LOCAL) + "';");
		esb.append("arrFormData[0][40] = '" + erp.getProperty(PARAM_CURRENCY_IN) + "';");
		esb.append("arrFormData[0][41] = '" + erp.getProperty(PARAM_LOCATION_URL) + "';");// Lalanthi-
		// JIRA:3031(Issue
		// 3)
		esb.append("arrFormData[0][42] = '" + erp.getProperty(PARAM_CONTACT_PERSON) + "';");
		esb.append("arrFormData[0][43] = '" + erp.getProperty(PARAM_PUBLISH_TO_WEB) + "';");
		esb.append("arrFormData[0][44] = '" + erp.getProperty(PARAM_PREF_RPT_FORMAT) + "';");
		esb.append("arrFormData[0][45] = '" + erp.getProperty(PARAM_SERVICE_CHANNEL) + "';");
		String autoInvoiceStr = "";
		if (erp.getProperty(PARAM_AUTOINVOICE).equals(WebConstants.VAL_CHECKBOX_ON)) {
			autoInvoiceStr = "Y";
		} else {
			autoInvoiceStr = "N";
		}
		esb.append("arrFormData[0][37] = '" + autoInvoiceStr + "';");

		String capExtPaymentRefStr = "";
		String extPaymentMandStr = "";
		String publishToWebStr = "";
		String handlingFeeActStr = "";
		String enableAgentLevEticketStr = "";
		String enableAgentLevelFareMask = "";

		if (erp.getProperty(PARAM_CAPTURE_EXT_PAY).equals(WebConstants.VAL_CHECKBOX_ON)) {
			capExtPaymentRefStr = "Y";
		} else {
			capExtPaymentRefStr = "N";
		}

		esb.append("arrFormData[0][46] = '" + capExtPaymentRefStr + "';");

		if (erp.getProperty(PARAM_EXT_PAY_MANDATORY).equals(WebConstants.VAL_CHECKBOX_ON)) {
			extPaymentMandStr = "Y";
		} else {
			extPaymentMandStr = "N";
		}
		esb.append("arrFormData[0][47] = '" + extPaymentMandStr + "';");

		if (erp.getProperty(PARAM_PUBLISH_TO_WEB).equals(WebConstants.VAL_CHECKBOX_ON)) {
			publishToWebStr = "Y";
		} else {
			publishToWebStr = "N";
		}
		esb.append("arrFormData[0][49] = '" + publishToWebStr + "';");

		esb.append("arrFormData[0][50] = '" + erp.getProperty(PARAM_MOBILE_NUMBER) + "';");
		esb.append("arrFormData[0][51] = '';");
		esb.append("arrFormData[0][52] = '" + erp.getProperty(PARAM_HANDLING_FEE_APPLIES_TO) + "';");
		esb.append("arrFormData[0][53] = '" + erp.getProperty(PARAM_HANDLING_FEE_TYPE) + "';");
		esb.append("arrFormData[0][54] = '" + erp.getProperty(PARAM_HANDLING_FEE_OW_AMOUNT) + "';");
		esb.append("arrFormData[0][55] = '" + erp.getProperty(PARAM_HANDLING_FEE_RT_AMOUNT) + "';");
		esb.append("arrFormData[0][56] = '" + erp.getProperty(PARAM_HANDLING_FEE_MIN_AMOUNT) + "';");
		esb.append("arrFormData[0][57] = '" + erp.getProperty(PARAM_HANDLING_FEE_MAX_AMOUNT) + "';");

		if (erp.getProperty(PARAM_HANDLING_FEE_ENABLE).equals(WebConstants.VAL_CHECKBOX_ON)) {
			handlingFeeActStr = "Y";
		} else {
			handlingFeeActStr = "N";
		}

		esb.append("arrFormData[0][58] = '" + handlingFeeActStr + "';");

		if (erp.getProperty(PARAM_TKT_SEQ_ENABLE).equals(WebConstants.VAL_CHECKBOX_ON)) {
			enableAgentLevEticketStr = "Y";
		} else {
			enableAgentLevEticketStr = "N";
		}
		esb.append("arrFormData[0][60] = '" + erp.getProperty(PARAM_BSP_GUATANTEE) + "';");
		esb.append("arrFormData[0][61] = '" + erp.getProperty(PARAM_BSP_GUATANTEE_LOCAL) + "';");

		esb.append("arrFormData[0][62] = '" + enableAgentLevEticketStr + "';");

		if (erp.getProperty(PARAM_FARE_MASK_ENABLE).equals(WebConstants.VAL_CHECKBOX_ON)) {
			enableAgentLevelFareMask = "Y";
		} else {
			enableAgentLevelFareMask = "N";
		}
		
		String grntExpDate = "";
		try {
			if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(erp.getProperty(PARAM_BANK_GURANTEE_EXPIRY_DATE))) {
				grntExpDate = DateUtil.formatDate(DateUtil.parseDate(erp.getProperty(PARAM_BANK_GURANTEE_EXPIRY_DATE).trim()),
						DateUtil.DEFAULT_DATE_INPUT_FORMAT);
			}
		} catch (ParseException e) {
			if (log.isErrorEnabled()) {
				log.error("Error while converting stirng to Date", e);
			}
		}
		esb.append("arrFormData[0][65] = '" + grntExpDate + "';");
		String charterAgent = "N";
		
		if (erp.getProperty(PARAM_CHARTER_BOOKING) != null && !erp.getProperty(PARAM_CHARTER_BOOKING).isEmpty()) {
			charterAgent = "Y";
		}
		esb.append("arrFormData[0][67] = '"+charterAgent+"';");
		esb.append("arrFormData[0][68] = '" + enableAgentLevelFareMask + "';");
		esb.append("arrFormData[0][69] = '" + erp.getProperty(PARAM_AGENT_INACTIVE_DATE) + "';");
		esb.append("arrFormData[0][70] = '" + erp.getProperty(PARAM_EMAILS_TO_NOTIFY) + "';");
		esb.append("arrFormData[0][71] = '';");
		
		if (isGSAV2Enabled
				&& (AgentType.GSA.equals(erp.getProperty(PARAM_TYPE1)) || AgentType.TA.equals(erp.getProperty(PARAM_TYPE1)))) {
			esb.append("arrFormData[0][72] = '" + erp.getProperty(PARAM_VISIBLE_TERRITORIES_STATIONS) + "';");
		}else{
			esb.append("arrFormData[0][72] = '';");
		}		
		
		String hasFixCredit = "Y";
		if (isGSAV2Enabled && !WebConstants.VAL_CHECKBOX_ON.equals(erp.getProperty(PARAM_FIXED_CREDIT))) {
			hasFixCredit = "N";
		}
		esb.append("arrFormData[0][73] = '" + hasFixCredit + "';");

		return esb.toString();
	}

	private static void setCurrentDate(HttpServletRequest request) throws ModuleException {
		String strHTML = "";
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			strHTML = sdf.format(date);
		} catch (Exception e) {
			log.error(e);
			// may not need to throw the exception
		}
		request.setAttribute(CURRENCY_SYS_DATE, strHTML);
	}
	
	private static boolean validatePaymentMethods(Agent gsaAgent, String[] selectedPaymentMethods) {
		Set<String> reportingGSAPaymentMethods = gsaAgent.getPaymentMethod();
		List<String> selectedPaymentMethodList = new ArrayList<String>();
		if (reportingGSAPaymentMethods == null || reportingGSAPaymentMethods.isEmpty()) {
			return false;
		} else {

			if (selectedPaymentMethods[0].equals("true")) {
				selectedPaymentMethodList.add(Agent.PAYMENT_MODE_ONACCOUNT);
			}
			if (selectedPaymentMethods[1].equals("true")) {
				selectedPaymentMethodList.add(Agent.PAYMENT_MODE_CASH);
			}
			if (selectedPaymentMethods[2].equals("true")) {
				selectedPaymentMethodList.add(Agent.PAYMENT_MODE_CREDITCARD);
			}
			if (selectedPaymentMethods[3].equals("true")) {
				selectedPaymentMethodList.add(Agent.PAYMENT_MODE_BSP);
			}
			if (selectedPaymentMethods[4].equals("true")) {
				selectedPaymentMethodList.add(Agent.PAYMENT_MODE_VOUCHER);
			}
			for (String paymentMethod : selectedPaymentMethodList) {
				if (!reportingGSAPaymentMethods.contains(paymentMethod)) {
					return false;
				}
			}
		}
		return true;
	}

	private static void setVisibleTerritoriesAndStations(HttpServletRequest request) {

		StringBuffer javascriptBuilder = new StringBuffer("var arrVisibleTerritories = new Array();");
		javascriptBuilder.append("var arrVisibleStations = new Array();");
		javascriptBuilder.append("var arrVisibleTerritories = new Array();");
		
		if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ASSIGN_VISIBLE_TERRITORIES)
				&& AppSysParamsUtil.getCarrierAgent().equals(userInfoTO.getAgentType())) {
			javascriptBuilder.append("var assignVisibleTerritories = true;");
			List<String[]> territoryList = LookupUtils.LookupDAO().getTwoColumnMap("territory");
			int i = 0;
			for(String[] territory:territoryList) {
				javascriptBuilder.append("arrVisibleTerritories[" + i + "] = new Array('" + territory[1] +"-"+ territory[0]+"','" + territory[0]
						+ "'); ");
				i++;
			}
		} else {
			javascriptBuilder.append("var assignVisibleTerritories = false;");
		}

		if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ASSIGN_VISIBLE_TERRITORIES)) {
			javascriptBuilder.append("var assignVisibleStations = true;");

			// AA,CO,SO - All Stations
			// GSA - Stations of all visible territories
			// SGSA - Stations of default territory

			List<String[]> stationList = getVisibleStationList();
			if (stationList != null && !stationList.isEmpty()) {
				int i = 0;	
				for(String[] station:stationList) {
					javascriptBuilder.append("arrVisibleStations[" + i + "] = new Array('" + station[1] +"-"+ station[0]+"','" + station[0]
							+ "'); ");
					i++;
				}			
			}
		} else {
			javascriptBuilder.append("var assignVisibleStations = false;");

		}
		request.setAttribute(WebConstants.REQ_VISIBLE_TERRITORIES_STATIONS, javascriptBuilder.toString());
	}
	
	private static Agent getFixedCreditAgent(Agent gsaAgent) throws ModuleException{
		
		if("Y".equals(gsaAgent.getHasCreditLimit())){
			return gsaAgent;
		} else {
			gsaAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(gsaAgent.getGsaCode());
			return getFixedCreditAgent(gsaAgent);
		}
	}
	
	private static void setAgentCreditSummary(HttpServletRequest request) throws ModuleException {

		Agent agent;
		boolean showCreditSummary = false;
		BigDecimal distributedCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		StringBuffer javascriptBuilder = new StringBuffer("");

		if (userInfoTO != null && !AppSysParamsUtil.getCarrierAgent().equals(userInfoTO.getAgentType())) {
			agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userInfoTO.getAgentCode());
			if (agent.getAgentSummary() != null) {
				ArrayList<Agent> childAgents = (ArrayList<Agent>) ModuleServiceLocator.getTravelAgentBD().getAgentsForGSA(
						userInfoTO.getAgentCode());
				if (childAgents != null && !childAgents.isEmpty()) {
					for (Agent refAgent : childAgents) {
						if (Agent.HAS_CREDIT_LIMIT_YES.equals(refAgent.getHasCreditLimit())) {
							distributedCredit = AccelAeroCalculator.add(distributedCredit, refAgent.getTotalCreditLimit());
						}
					}
				}

				javascriptBuilder.append("var agentAvailbleCredit = "
						+ ModuleServiceLocator.getTravelAgentBD().getCreditSharingAgent(agent.getAgentCode()).getAgentSummary()
								.getAvailableCredit() + ";");
				javascriptBuilder.append("var agentDistributedCredit = " + distributedCredit + ";");
				javascriptBuilder.append("var agentCreditLimit = " + agent.getAgentSummary().getAvailableAdvance() + ";");
				showCreditSummary = true;
			}
		}
		request.setAttribute(WebConstants.REQ_SHOW_AGENT_CREDIT_SUMMARY, showCreditSummary);
		request.setAttribute(WebConstants.REQ_AGENT_CREDIT_SUMMARY, javascriptBuilder.toString());

	}
	
	private static void setHandlingModificationOps(HttpServletRequest request) throws ModuleException {
		String strHFMOps = SelectListGenerator.createApplicableHandlingFeeModificationOps();
		request.setAttribute(WebConstants.REQ_HANDLING_FEE_APPLY_OPS, strHFMOps);
	}
	
	private static void setHandlingModificationOpStatus(HttpServletRequest request) throws ModuleException {
		String strHFMStatus = SelectListGenerator.createHandlingFeeModificationStatus();
		request.setAttribute(WebConstants.REQ_HANDLING_FEE_OP_STATUS, strHFMStatus);
	}
}
