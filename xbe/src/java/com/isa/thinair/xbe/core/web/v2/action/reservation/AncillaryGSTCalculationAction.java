
package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.util.Constants.ReservationFlow;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AncillaryUtil;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AncillaryGSTCalculationAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AncillaryGSTCalculationAction.class);

	private boolean success = true;

	private String messageTxt;

	private String paxWiseAnci;

	private String insurances;

	private String insurableFltRefNumbers;

	private String selectedFlightList;

	private String confExistingFlightSegs;

	private FlightSearchDTO searchParams;

	private String isOnd;

	private boolean isRequote = false;

	private String modifyingFltRefNumbers;

	private String resPaxInfo;

	private String ondwiseFlexiSelected;

	private Map<Integer, Boolean> ondSelectedFlexi = new HashMap<Integer, Boolean>();;

	private String pnr;

	private PaymentTO payment;

	private boolean alertMedicalSsrForAllSegments = false;

	private boolean isCancellationOnly = false;

	private String groupPnr;

	private String paxState;

	private String paxCountryCode;

	private boolean paxTaxRegistered;

	private BigDecimal totalServiceTaxAmountForAncillary = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalServiceTaxAmountForFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal adminFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String selectedCurrencyAdminFee;

	@SuppressWarnings("unchecked")
	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			String strTxnIdntifier = resInfo.getTransactionId();

			List<FlightSegmentTO> flightSegmentTOs = null;

			if (isRequote) {
				flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
			} else {
				flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
			}

			List<FlightSegmentTO> newAllFltSegmentTOs = new ArrayList<FlightSegmentTO>();
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				newAllFltSegmentTOs.add(flightSegmentTO);
			}

			if (confExistingFlightSegs != null) {
				if (isRequote) {
					newAllFltSegmentTOs.addAll(ReservationUtil.populateFlightSegmentList(confExistingFlightSegs, searchParams));
				} else {
					FltSegBuilder fltSegBuilder = new FltSegBuilder(confExistingFlightSegs, true, false, true);
					newAllFltSegmentTOs.addAll(fltSegBuilder.getSelectedFlightSegments());
				}
			}

			List<LCCInsuranceQuotationDTO> insuranceQuotations = AncillaryJSONUtil.getSelectedInsuranceQuotations(insurances,
					newAllFltSegmentTOs, insurableFltRefNumbers);

			if (insuranceQuotations != null && !insuranceQuotations.isEmpty()) {

				BigDecimal insuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCInsuranceQuotationDTO insuranceQuotation : insuranceQuotations) {
					insuranceAmount = AccelAeroCalculator.add(insuranceAmount, insuranceQuotation.getQuotedTotalPremiumAmount());
				}
				ExternalChgDTO insChgDTO = new ExternalChgDTO();
				insChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.INSURANCE);
				insChgDTO.setAmount(insuranceAmount);
				bookingShoppingCart.addSelectedExternalCharge(insChgDTO);
				bookingShoppingCart.setInsuranceQuotes(insuranceQuotations);
			} else {
				bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.INSURANCE);
				bookingShoppingCart.setInsuranceQuotes(null);
			}

			List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotations,
					ApplicationEngine.XBE, newAllFltSegmentTOs, isOnd, null);

			if (!isRequote) {
				SSRServicesUtil.checkMedicalSsrInAllSegments(paxList, getNonCancelledFltSegsWithoutBusSegs(flightSegmentTOs),
						ReservationInternalConstants.SsrTypes.MEDA);
			} else {

				LCCClientReservation reservation = null;

				if (StringUtil.isNullOrEmpty(groupPnr) && getSearchParams().getSearchSystem().equals(SYSTEM.INT.toString())) {
					reservation = ReservationUtil.loadFullLccReservation(pnr, false, null, bookingShoppingCart, request, false,
							null, null, null, false, getTrackInfo());
				} else {
					reservation = ReservationUtil.loadFullLccReservation(pnr,
							getSearchParams().getSearchSystem().equals(SYSTEM.AA.name()) ? false : true, null,
							bookingShoppingCart, request, false, null, null, null, false, getTrackInfo());
				}
				isCancellationOnly = isCancellationOnly(getNonCancelledFltSegsWithoutBusSegs(reservation.getSegments()),
						newAllFltSegmentTOs);

				if (!isCancellationOnly) {
					SSRServicesUtil.alertMedicalSsrForAllSegments(reservation,
							getNonCancelledFltSegsWithoutBusSegs(reservation.getSegments()), paxList,
							ReservationInternalConstants.SsrTypes.MEDA, newAllFltSegmentTOs);
				}

			}

			bookingShoppingCart.setPaxList(paxList);

			if (newAllFltSegmentTOs != null && !newAllFltSegmentTOs.isEmpty()) {
				bookingShoppingCart.getPriceInfoTO().getFareTypeTO().setOndFlexiSelection(ondSelectedFlexi);
				BookingUtil.applyFlexiCharges(paxList, bookingShoppingCart, newAllFltSegmentTOs);
			}

			if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
				if (!isRequote) {
					Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModSegAnciTotal = null;
					if (bookingShoppingCart.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI) && modifyingFltRefNumbers != null
							&& !"".equals(modifyingFltRefNumbers)
							&& !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(resInfo.getReservationStatus())) {
						List<String> modFltList = JSONFETOParser.parseCollection(List.class, String.class,
								modifyingFltRefNumbers);
						if (paxWiseAnci != null && !"".equals(paxWiseAnci)) {
							Collection<LCCClientReservationPax> colPax = AncillaryUtil.extractLccReservationPax(resPaxInfo);
							paxWiseModSegAnciTotal = ExternalChargeUtil.getPaxWiseModifyingSegmentAnciTotal(flightSegmentTOs,
									modFltList, colPax);
						}
					}
					BookingUtil.applyAncillaryTax(bookingShoppingCart, flightSegmentTOs.get(0), paxWiseModSegAnciTotal);
				}
			}

			quotePaxServiceTaxAndApplyPaxWise(bookingShoppingCart, flightSegmentTOs, strTxnIdntifier);
			// calculate Administration Fee based on ancillaries selected
			adminFee = ReservationUtil.calclulateAdministrationFee(bookingShoppingCart, request, isRequote, strTxnIdntifier,
					getTrackInfo(), searchParams, flightSegmentTOs, paxState, paxCountryCode, paxTaxRegistered);

			// converting Admin Fee to selected Currency based on ancillaries selected.
			String selectedCurrency = searchParams.getSelectedCurrency();
			CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getCurrencyExchangeRate(selectedCurrency);
			Currency currency = currencyExchangeRate.getCurrency();
			setSelectedCurrencyAdminFee(AccelAeroCalculator
					.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
							adminFee, currency.getBoundryValue(), currency.getBreakPoint())));

		} catch (ModuleException e) {
			log.error("ERROR :: Service tax calculation", e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	private List<FlightSegmentTO> getNonCancelledFltSegsWithoutBusSegs(List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		List<FlightSegmentTO> actualFlightSegmentList = new ArrayList<FlightSegmentTO>();
		for (Iterator<FlightSegmentTO> flightSegmentListIterator = flightSegmentTOs.iterator(); flightSegmentListIterator
				.hasNext();) {
			FlightSegmentTO flightSegment = flightSegmentListIterator.next();
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegment.getSegmentCode())) {
				if (flightSegment.getFlightSegmentStatus() != null) {
					if (!flightSegment.getFlightSegmentStatus().equals("CNX")) {
						actualFlightSegmentList.add(flightSegment);
					}
				} else {
					actualFlightSegmentList.add(flightSegment);
				}
			}
		}
		return actualFlightSegmentList;
	}

	private Set<LCCClientReservationSegment>
			getNonCancelledFltSegsWithoutBusSegs(Set<LCCClientReservationSegment> flightSegmentTOs) throws ModuleException {
		Set<LCCClientReservationSegment> actualFlightSegmentList = new HashSet<LCCClientReservationSegment>();
		for (Iterator<LCCClientReservationSegment> flightSegmentListIterator = flightSegmentTOs
				.iterator(); flightSegmentListIterator.hasNext();) {
			LCCClientReservationSegment flightSegment = flightSegmentListIterator.next();
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegment.getSegmentCode())) {
				if (flightSegment.getStatus() != null) {
					if (!flightSegment.getStatus().equals("CNX")) {
						actualFlightSegmentList.add(flightSegment);
					}
				} else {
					actualFlightSegmentList.add(flightSegment);
				}
			}
		}
		return actualFlightSegmentList;
	}

	private boolean isCancellationOnly(Set<LCCClientReservationSegment> lccSegments, List<FlightSegmentTO> newAllFltSegmentTOs) {
		boolean isCancellationOnly = false;

		List<String> reservationSegRPHList = getSegRPHList(lccSegments);
		List<String> newAllFltSegRPHList = getSegRPHList(newAllFltSegmentTOs);
		boolean isNewAllFltSubset = reservationSegRPHList.containsAll(newAllFltSegRPHList);

		if (isNewAllFltSubset) {
			isCancellationOnly = true;
		}

		return isCancellationOnly;
	}

	private List<String> getSegRPHList(Set<LCCClientReservationSegment> lccSegments) {
		List<String> segRPHList = new ArrayList<String>();

		for (Iterator<LCCClientReservationSegment> segIterator = lccSegments.iterator(); segIterator.hasNext();) {
			LCCClientReservationSegment segment = segIterator.next();
			segRPHList.add(segment.getFlightSegmentRefNumber());
		}

		return segRPHList;

	}

	private List<String> getSegRPHList(List<FlightSegmentTO> newAllFltSegmentTOs) {
		List<String> segRPHList = new ArrayList<String>();

		for (Iterator<FlightSegmentTO> segIterator = newAllFltSegmentTOs.iterator(); segIterator.hasNext();) {
			FlightSegmentTO segment = segIterator.next();
			segRPHList.add(segment.getFlightRefNumber());
		}

		return segRPHList;

	}

	private void quotePaxServiceTaxAndApplyPaxWise(BookingShoppingCart bookingShoppingCart,
			List<FlightSegmentTO> flightSegmentTOs, String txnIdntifier) throws ParseException, ModuleException {
		if (bookingShoppingCart.getPriceInfoTO() != null && !isRequote) {
			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);

			ExternalChgDTO handlingChargeDTO = bookingShoppingCart.getSelectedExternalCharges()
					.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
			BigDecimal handlingFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (handlingChargeDTO != null) {
				handlingFeeAmount = bookingShoppingCart.getHandlingCharge();
			}

			BigDecimal[] paxWiseHandlingFee = AccelAeroCalculator.roundAndSplit(handlingFeeAmount,
					bookingShoppingCart.getPaxList().size());

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
			serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(flightAvailRQ);
			serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
			serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
			serviceTaxQuoteCriteriaDTO.setPaxState(paxState);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(paxCountryCode);
			serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(paxTaxRegistered);

			ServiceTaxCalculatorUtil.addPaxExternalChargesToServiceTaxQuoteRQ(serviceTaxQuoteCriteriaDTO,
					bookingShoppingCart.getPaxList(), flightSegmentTOs, null, null, handlingChargeDTO, paxWiseHandlingFee, true);

			serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(txnIdntifier);

			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
					.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfo());

			Map<String, BigDecimal> serviceTaxAmountMap = ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTaxForDisplay(
					bookingShoppingCart.getPaxList(), serviceTaxQuoteRS, ReservationFlow.CREATE);

			setTotalServiceTaxAmountForAncillary(
					serviceTaxAmountMap.get(ReservationInternalConstants.ServiceTaxIdentificationConstant.ANCI));
			setTotalServiceTaxAmountForFare(
					serviceTaxAmountMap.get(ReservationInternalConstants.ServiceTaxIdentificationConstant.FARE));

			BigDecimal serviceTax = AccelAeroCalculator.add(
					serviceTaxAmountMap.get(ReservationInternalConstants.ServiceTaxIdentificationConstant.ANCI),
					serviceTaxAmountMap.get(ReservationInternalConstants.ServiceTaxIdentificationConstant.FARE));
			request.getSession().setAttribute("serviceTax", serviceTax.toString());

		}

	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public String getInsurances() {
		return insurances;
	}

	public void setInsurances(String insurances) {
		this.insurances = insurances;
	}

	public String getInsurableFltRefNumbers() {
		return insurableFltRefNumbers;
	}

	public void setInsurableFltRefNumbers(String insurableFltRefNumbers) {
		this.insurableFltRefNumbers = insurableFltRefNumbers;
	}

	public String getSelectedFlightList() {
		return selectedFlightList;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public String getConfExistingFlightSegs() {
		return confExistingFlightSegs;
	}

	public void setConfExistingFlightSegs(String confExistingFlightSegs) {
		this.confExistingFlightSegs = confExistingFlightSegs;
	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public String getIsOnd() {
		return isOnd;
	}

	public void setIsOnd(String isOnd) {
		this.isOnd = isOnd;
	}

	public boolean isRequote() {
		return isRequote;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public String getModifyingFltRefNumbers() {
		return modifyingFltRefNumbers;
	}

	public void setModifyingFltRefNumbers(String modifyingFltRefNumbers) {
		this.modifyingFltRefNumbers = modifyingFltRefNumbers;
	}

	public String getResPaxInfo() {
		return resPaxInfo;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public String getOndwiseFlexiSelected() {
		return ondwiseFlexiSelected;
	}

	public void setOndwiseFlexiSelected(String ondwiseFlexiSelected) {
		this.ondwiseFlexiSelected = ondwiseFlexiSelected;
		if (this.ondwiseFlexiSelected != null && !"".equals(this.ondwiseFlexiSelected)) {
			setOndSelectedFlexi(
					WebplatformUtil.convertMap(JSONFETOParser.parseMap(HashMap.class, String.class, this.ondwiseFlexiSelected)));
		}
	}

	public Map<Integer, Boolean> getOndSelectedFlexi() {
		return ondSelectedFlexi;
	}

	public void setOndSelectedFlexi(Map<Integer, Boolean> ondSelectedFlexi) {
		this.ondSelectedFlexi = ondSelectedFlexi;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public boolean isAlertMedicalSsrForAllSegments() {
		return alertMedicalSsrForAllSegments;
	}

	public void setAlertMedicalSsrForAllSegments(boolean alertMedicalSsrForAllSegments) {
		this.alertMedicalSsrForAllSegments = alertMedicalSsrForAllSegments;
	}

	public boolean isCancellationOnly() {
		return isCancellationOnly;
	}

	public void setCancellationOnly(boolean isCancellationOnly) {
		this.isCancellationOnly = isCancellationOnly;
	}

	public String getGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	public String getPaxState() {
		return paxState;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public String getPaxCountryCode() {
		return paxCountryCode;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public boolean isPaxTaxRegistered() {
		return paxTaxRegistered;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public BigDecimal getTotalServiceTaxAmountForAncillary() {
		return totalServiceTaxAmountForAncillary;
	}

	public void setTotalServiceTaxAmountForAncillary(BigDecimal totalServiceTaxAmountForAncillary) {
		this.totalServiceTaxAmountForAncillary = totalServiceTaxAmountForAncillary;
	}

	public BigDecimal getTotalServiceTaxAmountForFare() {
		return totalServiceTaxAmountForFare;
	}

	public void setTotalServiceTaxAmountForFare(BigDecimal totalServiceTaxAmountForFare) {
		this.totalServiceTaxAmountForFare = totalServiceTaxAmountForFare;
	}

	public BigDecimal getAdminFee() {
		return adminFee;
	}

	public void setAdminFee(BigDecimal totalAdministrationFee) {
		this.adminFee = totalAdministrationFee;
	}

	public String getSelectedCurrencyAdminFee() {
		return selectedCurrencyAdminFee;
	}

	public void setSelectedCurrencyAdminFee(String selectedCurrencyAdminFee) {
		this.selectedCurrencyAdminFee = selectedCurrencyAdminFee;
	}

}
