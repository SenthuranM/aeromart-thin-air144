package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.text.ParseException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.OndConvertAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.OpenReturnInfo;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AvailabilityUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class FlightSearchAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AvailabilitySearchAction.class);

	private FlightSearchDTO searchParams;

	private boolean success = true;

	private Collection<AvailableFlightInfo> outboundFlights;

	private String messageTxt;

	private OpenReturnInfo openReturnInfo;

	private boolean isOpenReturn = false;

	private boolean currencyRound = false;

	private boolean modifyBooking = false;

	private boolean addSegment = false;

	private String airportMessage;

	private boolean transferSegment = false;

	private String groupPNR;

	private boolean addGroundSegment = false;

	private String selectedFltSeg;

	private ReservationProcessParams rpParams;

	private Set<String> busCarrierCodes;

	private String classOfServiceDesc;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {			
			boolean hasPrivViewAvailability = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			// TODO charith : should be able to remove
			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			boolean hasPrivInterlineSearch = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)
					&& AppSysParamsUtil.isLCCConnectivityEnabled();
			boolean isAllowDomesticOpenReturnBooking = BasicRH.hasPrivilege(request,
					PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_MAKE_OPEN_RETURN_BOOKINGS_FOR_DOMESTIC_FLIGHTS);
			boolean isDomesticRoute = true;
			boolean isAllowFlightSearchAfterCutoffTime = BasicRH.hasPrivilege(request,
					PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);
			boolean isAllowOverbookAfterCutoffTime = BasicRH.hasPrivilege(request,
					PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			ReservationProcessParams resParm = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
			
			FlightAvailRS flightAvailRS = new FlightAvailRS();
			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
			flightAvailRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(isAllowOverbookAfterCutoffTime);
			flightAvailRQ.getAvailPreferences().setRestrictionLevel(
					AvailabilityConvertUtil.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats,
							searchParams.getBookingType()));
			flightAvailRQ.getAvailPreferences().setAllowFlightSearchAfterCutOffTime(isAllowFlightSearchAfterCutoffTime);

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (isGroupPNR) {
				flightAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
				searchParams.setSearchSystem(SYSTEM.INT.toString());
			}

			if (transferSegment) {
				ReservationUtil.updateTransferSegmentData(flightAvailRQ, isGroupPNR,
						request.getParameter("modifyingSegmentOperatingCarrier"));
			}

			/*
			 * Check the privileges to search preferred system. In case of not enough privilege gracefully cut down to
			 * match the available privileges
			 */
			SYSTEM searchSystem = flightAvailRQ.getAvailPreferences().getSearchSystem();
			boolean isValidBusSearch = true;
			if (searchSystem == SYSTEM.ALL || searchSystem == SYSTEM.COND) {
				if (!hasPrivInterlineSearch) {
					searchSystem = SYSTEM.AA;
				}
			} else if (searchSystem == SYSTEM.INT && !hasPrivInterlineSearch) {
				searchSystem = SYSTEM.NONE;
			}
			flightAvailRQ.getAvailPreferences().setSearchSystem(searchSystem);

			// Set default stay over for half return variance search
			setDefaultStayOverDaysForReturnFareSearch(searchParams, flightAvailRQ);
			String pnr = request.getParameter("pnr");
			if (pnr != null && !"".equals(pnr)) {
				flightAvailRQ.getAvailPreferences().setModifyBooking(true);
				ReservationBeanUtil.setExistingSegInfo(flightAvailRQ, request.getParameter("resSegments"),
						request.getParameter("modifySegment"));
				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
						.getParameter("resSegments"));
				
				// if(!isValidRouteModification(colsegs)){
				// success = false;
				// messageTxt = XBEConfig.getClientMessage("cc_resmod_modify_book_selected_route_not_allowed",
				// userLanguage);
				// return S2Constants.Result.SUCCESS;
				// }
				
				// FIXME
				// THIS block of code is a contain duplicate set of operations carried out by
				// ReservationBeanUtil.setExistingSegInfo
				// SHOULD BE CLEANED UP
				if ((!addSegment) || searchParams.isOpenReturnConfirm()) {
					Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs,
							request.getParameter("modifySegment"), resParm, true);
					Integer oldFareID = null;
					Integer oldFareType = null;
					BigDecimal oldFareAmount = BigDecimal.ZERO;
					String carrierCode = null;
					for (LCCClientReservationSegment seg : colModSegs) {
						oldFareID = seg.getFareTO().getFareId();
						oldFareType = seg.getFareTO().getFareRuleID();
						oldFareAmount = seg.getFareTO().getAmount();
						carrierCode = seg.getFareTO().getCarrierCode();
						break;
					}
					flightAvailRQ.getAvailPreferences().setOldFareID(oldFareID);
					flightAvailRQ.getAvailPreferences().setOldFareType(oldFareType);
					flightAvailRQ.getAvailPreferences().setOldFareAmount(oldFareAmount);
					flightAvailRQ.getAvailPreferences().setOldFareCarrierCode(carrierCode);
				}

				if (!this.validateAddGroundSegment(searchParams,userLanguage)) {
					return S2Constants.Result.SUCCESS;
				}

				if (isGroupPNR) {
					// No longer required as use can do seemless modifications
					// flightAvailRQ.getAvailPreferences().setParticipatingCarriers(ReservationUtil.getParticipatingCarriers(request,
					// colsegs,
					// request.getParameter("modifySegment")));

					if (addSegment && addGroundSegment) {
						if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
							List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
							OriginDestinationInformationTO ondInfoTO = flightAvailRQ.getOriginDestinationInformationList()
									.iterator().next();
							isValidBusSearch = groundSegmentInfoWithConnectionValidationsForDry(colsegs, ondInfoTO);
							originDestinationInformationList.add(ondInfoTO);
							flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
						}
					}

				} else {

					Date firstDepatureDate = null;
					Date lastArrivalDate = null;
					for (LCCClientReservationSegment seg : colsegs) {
						if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
							if (firstDepatureDate == null || seg.getDepartureDateZulu().before(firstDepatureDate)) {
								firstDepatureDate = seg.getDepartureDateZulu();
							}
							if (lastArrivalDate == null || seg.getArrivalDateZulu().after(lastArrivalDate)) {
								lastArrivalDate = seg.getDepartureDateZulu();
							}
						}
					}
					flightAvailRQ.getAvailPreferences().setFirstDepatureDate(firstDepatureDate);
					flightAvailRQ.getAvailPreferences().setLastArrivalDate(lastArrivalDate);

					// This validation is need to be done for dry bus segments also
					// Re calculate arrival, departure start/end times when adding bus segments - DONE
					// TODO dry validation "groundSegmentInfoWithConnectionValidationsForDry" should be refactored
					// when all cases (own/dry) O & D validation calculated by common validation method
					if (AppSysParamsUtil.isGroundServiceEnabled() && addSegment && addGroundSegment) {
						if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
							List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
							OriginDestinationInformationTO ondInfoTO = flightAvailRQ.getOriginDestinationInformationList()
									.iterator().next();
							isValidBusSearch = originDestinationInfoWithConnectionValidations(colsegs, ondInfoTO);
							originDestinationInformationList.add(ondInfoTO);
							flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
						}
					}
				}

				flightAvailRQ.getAvailPreferences().setSearchSystem(
						AvailabilityUtil.getSearchSystem(flightAvailRQ.getOriginDestinationInformationList(), getTrackInfo(), pnr,
								isGroupPNR, hasPrivInterlineSearch));

				if (flightAvailRQ.getTravelPreferences().isOpenReturnConfirm()) {
					ReservationUtil.setOpenReturnConfirmInfo(flightAvailRQ, pnr, getTrackInfo());
				}

			}

			if (addSegment && addGroundSegment && !isValidBusSearch) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.surfacesegment.modify.book.invalid", userLanguage);
				return S2Constants.Result.SUCCESS;
			}

			if (((!modifyBooking && !addGroundSegment) || (!addGroundSegment && addSegment))
					|| (modifyBooking && !addGroundSegment)) {
				if (!AddModifySurfaceSegmentValidations.isValidSearchForFreshBooking(searchParams)) {
					success = false;
					messageTxt = XBEConfig.getClientMessage("um.surfacesegment.add.searchonly.invalid", userLanguage);
					return S2Constants.Result.SUCCESS;
				}
			}

			if ((modifyBooking || addSegment) && AddModifySurfaceSegmentValidations.isSubStationMissing(searchParams)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.surfacesegment.modify.search.invalid", userLanguage);
				return S2Constants.Result.SUCCESS;
			}

			if (flightAvailRQ.getTravelPreferences().isOpenReturn()) {
				flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlightsWithAllFares(
						flightAvailRQ, getTrackInfo());
			} else {
				flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
						getTrackInfo(), false);
			}

			/* Convert response to UI friendly DTO's */
			if (searchSystem == SYSTEM.INT) {
				if (flightAvailRS != null && flightAvailRS.getReservationParms() != null) {
					resParm.enableInterlineModificationParams(flightAvailRS.getReservationParms(),
							resInfo != null ? resInfo.getReservationStatus() : null, null, true, null);
				}
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_ACCESSED, Boolean.TRUE);
			}

			busCarrierCodes = new HashSet<String>();
			outboundFlights = ReservationBeanUtil
					.createAvailFlightInfoList(flightAvailRS, searchParams, hasPrivViewAvailability, (flightAvailRQ
							.getAvailPreferences().getRestrictionLevel() == AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION),
							"depature", false, false, busCarrierCodes);
			busCarrierCodes.addAll(SelectListGenerator.getBusCarrierCodes());

			isOpenReturn = (flightAvailRS.getOpenReturnOptionsTO() != null);
			if (isOpenReturn) {
				for (AvailableFlightInfo availableFlight : outboundFlights) {
					for (Boolean isDomesticFlight : availableFlight.getDomesticFlightList()) {
						if (!isDomesticFlight) {
							isDomesticRoute = false;
							break;
						}
					}
				}

				if ((isAllowDomesticOpenReturnBooking && isDomesticRoute) || (!isDomesticRoute)) {
					openReturnInfo = new OpenReturnInfo(flightAvailRS.getOpenReturnOptionsTO(), searchParams.getFromAirport(),
							searchParams.getToAirport());
				} else {
					throw new Exception("error.domestic.openrt.bookings.not.allowed");
				}
			}

			PriceInfoTO priceInfoTO = flightAvailRS.getSelectedPriceFlightInfo();

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			if (bookingShoppingCart == null) {
				bookingShoppingCart = new BookingShoppingCart(modifyBooking ? ChargeRateOperationType.MODIFY_ONLY
						: ChargeRateOperationType.MAKE_ONLY);
			}
			bookingShoppingCart.setPriceInfoTO(flightAvailRS.getSelectedPriceFlightInfo());
			bookingShoppingCart.setPayablePaxCount(searchParams.getAdultCount() + searchParams.getChildCount());

			resInfo.setTransactionId(flightAvailRS.getTransactionIdentifier());
			resInfo.setBookingType(flightAvailRQ.getTravelPreferences().getBookingType());
			bookingShoppingCart.setPriceInfoTO(priceInfoTO);
			currencyRound = CommonUtil.enableCurrencyRounding(searchParams.getSelectedCurrency());

			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightAvailRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, null);

			// ReservationProcessParams is initialized in many places, by adding this to session can eliminate the
			// Unnecessary call.
			// TODO ReservationProcessParams usages must be Re-factored to load from the session.
			request.getSession().setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, resParm);

			rpParams = resParm;
		} catch (Exception e) {
			clearValues();
			success = false;
			if (e instanceof ModuleException) {
				if ("error.openrt.confirm.before.outbound.notallowed".equals(((ModuleException) e).getExceptionCode())) {
					messageTxt = XBEConfig.getClientMessage("error.openrt.confirm.before.outbound.notallowed", userLanguage);
				} else {
					messageTxt = BasicRH.getErrorMessage(e,userLanguage);
				}
			} else {
				if (("error.domestic.openrt.bookings.not.allowed").equals(e.getMessage())) {
					messageTxt = XBEConfig.getClientMessage(e.getMessage(), userLanguage);
				} else {
					messageTxt = BasicRH.getErrorMessage(e,userLanguage);
				}
			}
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private boolean validateAddGroundSegment(FlightSearchDTO searchDTO, String language) throws ModuleException {
		if (this.addGroundSegment) {
			if (!AddModifySurfaceSegmentValidations.isValidAddGroundSegment(searchDTO)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.surfacesegment.add.search.invalid", language);
				return false;
			}
		}
		return true;
	}

	private boolean originDestinationInfoWithConnectionValidations(Collection<LCCClientReservationSegment> colsegs,
			OriginDestinationInformationTO originDestinationInformation) throws ModuleException {

		boolean isValidBusSearch = false;
		
		if (!StringUtil.isNullOrEmpty(selectedFltSeg)) {
			
			String matchingFlightSegmentRPH = getMatchingFlightSegmentRPH(selectedFltSeg, originDestinationInformation);
			
			if (!StringUtil.isNullOrEmpty(matchingFlightSegmentRPH)) {
				ArrayList<Integer> arrFlightSegId = new ArrayList<Integer>();
				for (LCCClientReservationSegment resSeg : colsegs) {
					if (resSeg.getFlightSegmentRefNumber().equals(matchingFlightSegmentRPH)) {
						arrFlightSegId.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));
						break;
					}
				}

				if (arrFlightSegId != null && arrFlightSegId.size() > 0) {
					Collection<FlightSegmentDTO> flightSegments = ModuleServiceLocator.getFlightServiceBD().getFlightSegments(
							arrFlightSegId);
					Iterator<FlightSegmentDTO> segIter = flightSegments.iterator();
					FlightSegmentDTO flightSegment = null;
					String[] minMaxTransitDurations = null;
					Calendar valiedTimeFrom = Calendar.getInstance();
					Calendar valiedTimeTo = Calendar.getInstance();
					boolean busForDeparture = false;

					if (segIter.hasNext()) {
						flightSegment = segIter.next();
					}

					String depBusSegAirport = originDestinationInformation.getOrigin();
					String arrBusSegAirport = originDestinationInformation.getDestination();

					Set<String> busConnectingAirports = CommonsServices.getGlobalConfig().getBusConnectingAirports();

					if (flightSegment != null && flightSegment.getSegmentCode() != null) {

						String depAirSegAirport = flightSegment.getSegmentCode().substring(0, 3);
						String arrAirSegAirport = flightSegment.getSegmentCode().substring(
								flightSegment.getSegmentCode().length() - 3);

						String airlineCarrier = flightSegment.getFlightNumber().substring(0, 2);

						String busCarrier = SelectListGenerator.getDefaultBusCarrierCode(airlineCarrier);

						// Departure airport of air segment is connecting to bus segments
						if (arrBusSegAirport.equalsIgnoreCase(depAirSegAirport)
								&& busConnectingAirports.contains(depAirSegAirport)) {
							busForDeparture = true;
							isValidBusSearch = true;
							minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
									flightSegment.getSegmentCode().substring(0, 3), busCarrier, airlineCarrier);

							String[] minDurationHHMM = minMaxTransitDurations[1].split(":");
							valiedTimeFrom.setTime(flightSegment.getDepartureDateTime());
							valiedTimeFrom.add(Calendar.HOUR, -(Integer.parseInt(minDurationHHMM[0])));
							valiedTimeFrom.add(Calendar.MINUTE, -(Integer.parseInt(minDurationHHMM[1])));

							String[] maxDurationHHMM = minMaxTransitDurations[0].split(":");
							valiedTimeTo.setTime(flightSegment.getDepartureDateTime());
							valiedTimeTo.add(Calendar.HOUR, -(Integer.parseInt(maxDurationHHMM[0])));
							valiedTimeTo.add(Calendar.MINUTE, -(Integer.parseInt(maxDurationHHMM[1])));

							if (originDestinationInformation != null) {
								originDestinationInformation.setArrivalDateTimeStart(valiedTimeFrom.getTime());
								originDestinationInformation.setArrivalDateTimeEnd(valiedTimeTo.getTime());
							}

						} else if (depBusSegAirport.equalsIgnoreCase(arrAirSegAirport)
								&& busConnectingAirports.contains(arrAirSegAirport)) {
							isValidBusSearch = true;
							minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
									flightSegment.getSegmentCode().substring(flightSegment.getSegmentCode().length() - 3),
									airlineCarrier, busCarrier);

							String[] minDurationHHMM = minMaxTransitDurations[0].split(":");
							valiedTimeFrom.setTime(flightSegment.getArrivalDateTime());
							valiedTimeFrom.add(Calendar.HOUR, (Integer.parseInt(minDurationHHMM[0])));
							valiedTimeFrom.add(Calendar.MINUTE, (Integer.parseInt(minDurationHHMM[1])));

							String[] maxDurationHHMM = minMaxTransitDurations[1].split(":");
							valiedTimeTo.setTime(flightSegment.getArrivalDateTime());
							valiedTimeTo.add(Calendar.HOUR, (Integer.parseInt(maxDurationHHMM[0])));
							valiedTimeTo.add(Calendar.MINUTE, (Integer.parseInt(maxDurationHHMM[1])));

							if (originDestinationInformation != null) {
								Calendar dptStart = Calendar.getInstance();
								dptStart.setTime(originDestinationInformation.getDepartureDateTimeStart());
								Calendar dptEnd = Calendar.getInstance();
								dptEnd.setTime(originDestinationInformation.getDepartureDateTimeEnd());

								// Valied times should be within the search date.
								List<Calendar> valiedPeriod = CommonUtil.valiedPeriod(dptStart, dptEnd, valiedTimeFrom,
										valiedTimeTo);

								if (valiedPeriod.size() == 2) {
									originDestinationInformation.setDepartureDateTimeStart(valiedPeriod.get(0).getTime());
									originDestinationInformation.setDepartureDateTimeEnd(valiedPeriod.get(1).getTime());
								}
							}
						}
					}

					// Set required parameters for Next/Previous navigations
					searchParams.setBusForDeparture(busForDeparture);
					searchParams.setBusConValiedTimeFrom(String.valueOf(valiedTimeFrom.getTimeInMillis()));
					searchParams.setBusConValiedTimeTo(String.valueOf(valiedTimeTo.getTimeInMillis()));
				}

			}
		}

		return isValidBusSearch;
	}

	private String getMatchingFlightSegmentRPH(String selectedFltSeg, OriginDestinationInformationTO originDestinationInformation) {
		
		String origin = originDestinationInformation.getOrigin();
		String destination = originDestinationInformation.getDestination();		
		String[] selectedSegs = selectedFltSeg.split(",");
		String matchingSegmentRPH = "";
		
		for (String segmentRPH : selectedSegs) {
			
			if (segmentRPH.contains(origin) || segmentRPH.contains(destination)) {
				matchingSegmentRPH = segmentRPH;
				break;
			}
		}		
		
		return matchingSegmentRPH;
	}

	/*
	 * groundSegmentInfoWithConnectionValidationsForDry - this validation should be applied when search for a ground
	 * segment - "AddBusSegment"
	 */
	private boolean groundSegmentInfoWithConnectionValidationsForDry(Collection<LCCClientReservationSegment> colsegs,
			OriginDestinationInformationTO originDestinationInformation) throws ModuleException {

		boolean isValidBusSearch = false;
		if (!StringUtil.isNullOrEmpty(selectedFltSeg)) {
			String[] selectedSegs = selectedFltSeg.split(",");

			LCCClientReservationSegment selectedSegment = null;

			if (!StringUtil.isNullOrEmpty(selectedSegs[0])) {
				for (LCCClientReservationSegment resSeg : colsegs) {
					if (selectedSegs[0].startsWith(resSeg.getFlightSegmentRefNumber())) {
						selectedSegment = resSeg;
						break;
					}
				}

				if (selectedSegment != null) {
					Calendar valiedTimeFrom = Calendar.getInstance();
					Calendar valiedTimeTo = Calendar.getInstance();
					boolean busForDeparture = false;

					String arriAirportMinConTime = selectedSegment.getArriAirportMinConTime();
					String arriAirportMaxConTime = selectedSegment.getArriAirportMaxConTime();
					String depAirportMinConTime = selectedSegment.getDepAirportMinConTime();
					String depAirportMaxConTime = selectedSegment.getDepAirportMaxConTime();

					String depAirport = originDestinationInformation.getOrigin();
					String arrivalAirport = originDestinationInformation.getDestination();

					String segmentCode = selectedSegment.getSegmentCode();
					String[] airportCodes = null;

					if (!StringUtil.isNullOrEmpty(segmentCode)) {
						airportCodes = segmentCode.split("/");

						if (airportCodes[0].equalsIgnoreCase(arrivalAirport)
								&& (depAirportMinConTime != null && depAirportMaxConTime != null)) {
							// Departure Time
							busForDeparture = true;
							isValidBusSearch = true;
							String[] minDurationHHMM = depAirportMaxConTime.split(":");
							valiedTimeFrom.setTime(selectedSegment.getDepartureDate());
							valiedTimeFrom.add(Calendar.HOUR, -(Integer.parseInt(minDurationHHMM[0])));
							valiedTimeFrom.add(Calendar.MINUTE, -(Integer.parseInt(minDurationHHMM[1])));

							String[] maxDurationHHMM = depAirportMinConTime.split(":");
							valiedTimeTo.setTime(selectedSegment.getDepartureDate());
							valiedTimeTo.add(Calendar.HOUR, -(Integer.parseInt(maxDurationHHMM[0])));
							valiedTimeTo.add(Calendar.MINUTE, -(Integer.parseInt(maxDurationHHMM[1])));

							if (originDestinationInformation != null) {
								originDestinationInformation.setArrivalDateTimeStart(valiedTimeFrom.getTime());
								originDestinationInformation.setArrivalDateTimeEnd(valiedTimeTo.getTime());
							}

						} else if (airportCodes[1].equalsIgnoreCase(depAirport)
								&& (arriAirportMinConTime != null && arriAirportMaxConTime != null)) {
							// Arrival Time
							isValidBusSearch = true;
							String[] minDurationHHMM = arriAirportMinConTime.split(":");
							valiedTimeFrom.setTime(selectedSegment.getArrivalDate());
							valiedTimeFrom.add(Calendar.HOUR, (Integer.parseInt(minDurationHHMM[0])));
							valiedTimeFrom.add(Calendar.MINUTE, (Integer.parseInt(minDurationHHMM[1])));

							String[] maxDurationHHMM = arriAirportMaxConTime.split(":");
							valiedTimeTo.setTime(selectedSegment.getArrivalDate());
							valiedTimeTo.add(Calendar.HOUR, (Integer.parseInt(maxDurationHHMM[0])));
							valiedTimeTo.add(Calendar.MINUTE, (Integer.parseInt(maxDurationHHMM[1])));

							if (originDestinationInformation != null) {
								Calendar dptStart = Calendar.getInstance();
								dptStart.setTime(originDestinationInformation.getDepartureDateTimeStart());
								Calendar dptEnd = Calendar.getInstance();
								dptEnd.setTime(originDestinationInformation.getDepartureDateTimeEnd());

								// Valied times should be within the search date.
								List<Calendar> valiedPeriod = CommonUtil.valiedPeriod(dptStart, dptEnd, valiedTimeFrom,
										valiedTimeTo);

								if (valiedPeriod.size() == 2) {
									originDestinationInformation.setDepartureDateTimeStart(valiedPeriod.get(0).getTime());
									originDestinationInformation.setDepartureDateTimeEnd(valiedPeriod.get(1).getTime());
								}
							}

						}
					}

					// Set required parameters for Next/Previous navigations
					searchParams.setBusForDeparture(busForDeparture);
					searchParams.setBusConValiedTimeFrom(String.valueOf(valiedTimeFrom.getTimeInMillis()));
					searchParams.setBusConValiedTimeTo(String.valueOf(valiedTimeTo.getTimeInMillis()));
				}

			}
		}

		return isValidBusSearch;
	}

	private void setDefaultStayOverDaysForReturnFareSearch(FlightSearchDTO searchParams, FlightAvailRQ flightAvailRQ) {
		OndConvertAssembler ondAssm = new OndConvertAssembler(flightAvailRQ.getOriginDestinationInformationList());
		if (ondAssm.isReturnFlight() && !flightAvailRQ.getTravelPreferences().isOpenReturn()) {
			long stayOverTimeBetweenRequestedDates = ondAssm.getSelectedInboundDateTimeEnd().getTime()
					- ondAssm.getSelectedOutboundDateTimeStart().getTime();
			flightAvailRQ.getAvailPreferences().setStayOverTimeInMillis(stayOverTimeBetweenRequestedDates);
			searchParams.setStayOverTimeInMillis(stayOverTimeBetweenRequestedDates);
		}
	}

	private void clearValues() {
		outboundFlights = null;
		airportMessage = null;
	}

	private boolean isValidRouteModification(Collection<LCCClientReservationSegment> colsegs) throws ParseException{
		
		boolean validRouteModification = true;
		boolean isSelectedSegmentFirst = false;
		int segmentCount = 0;
		
		if (!StringUtil.isNullOrEmpty(selectedFltSeg)) {
			
			String[] selectedSegs = selectedFltSeg.split(",");

			if (!StringUtil.isNullOrEmpty(selectedSegs[0])) {
				
				LCCClientReservationSegment selectedSegment = null;

				for (LCCClientReservationSegment resSeg : colsegs) {
					
					if(!ReservationInternalConstants.ReservationStatus.CANCEL.equals(resSeg.getStatus())){
						segmentCount++;
					}
					
					if (selectedSegs[0].startsWith(resSeg.getFlightSegmentRefNumber())) {
						if(segmentCount == 1){
							isSelectedSegmentFirst = true;
						}
						selectedSegment = resSeg;
						break;
					}
					
				}
				
				String originAirport = selectedSegment.getSegmentCode().split("/")[0];
				
				if(!originAirport.equals(searchParams.getFromAirport()) && isSelectedSegmentFirst){
					GlobalConfig globalConfig  = XBEModuleUtils.getGlobalConfig();
					String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
					String newOriginCountryCode = (String)globalConfig.retrieveAirportInfo(searchParams.getFromAirport())[3];
					for(String countryCode : taxRegNoEnabledCountries){
						if(newOriginCountryCode.equals(countryCode)){
							validRouteModification = false;
							break;
						}
					}
				}
				
			}
			
		}else{
			
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date newSegDepatureDate = format.parse(searchParams.getDepartureDate());
			
			if(colsegs.size() == 1 && (((LCCClientReservationSegment)colsegs.toArray()[0]).getDepartureDate().compareTo(newSegDepatureDate) > 0)){
				GlobalConfig globalConfig  = XBEModuleUtils.getGlobalConfig();
				String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
				String newOriginCountryCode = (String)globalConfig.retrieveAirportInfo(searchParams.getFromAirport())[3];
				for(String countryCode : taxRegNoEnabledCountries){
					if(newOriginCountryCode.equals(countryCode)){
						validRouteModification = false;
						break;
					}
				}
			}
			
		}
		
		return validRouteModification;
		
	}
	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public Collection<AvailableFlightInfo> getOutboundFlights() {
		return outboundFlights;
	}

	// setters
	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public static Log getLog() {
		return log;
	}

	public boolean isModifyBooking() {
		return modifyBooking;
	}

	public void setModifyBooking(boolean modifyBooking) {
		this.modifyBooking = modifyBooking;
	}

	/**
	 * @return the openReturnInfo
	 */
	public OpenReturnInfo getOpenReturnInfo() {
		return openReturnInfo;
	}

	/**
	 * @param openReturnInfo
	 *            the openReturnInfo to set
	 */
	public void setOpenReturnInfo(OpenReturnInfo openReturnInfo) {
		this.openReturnInfo = openReturnInfo;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public void setTransferSegment(boolean transferSegment) {
		this.transferSegment = transferSegment;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public ReservationProcessParams getRpParams() {
		return rpParams;
	}

	public void setRpParams(ReservationProcessParams rpParams) {
		this.rpParams = rpParams;
	}

	public Set<String> getBusCarrierCodes() {
		return busCarrierCodes;
	}

	public void setSelectedFltSeg(String selectedFltSeg) {
		this.selectedFltSeg = selectedFltSeg;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}
}
