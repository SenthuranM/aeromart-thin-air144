package com.isa.thinair.xbe.core.web.util;

import java.util.List;

import com.isa.aeromart.services.endpoint.constant.PaymentConsts;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.xbe.api.dto.CurrencyInfoTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.TravelAgentUtil;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

import org.springframework.util.CollectionUtils;

public class PaymentRHUtil {

	private static Log log = LogFactory.getLog(PaymentRHUtil.class);

	public static CurrencyInfoTO getCurrencyInfo(String currencyCode, ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		CurrencyInfoTO currencyInfoTO = new CurrencyInfoTO();
		currencyInfoTO.setCurrencyCode(currencyCode);
		currencyInfoTO.setExchangeRateTimestamp(exchangeRateProxy.getExecutionDate());

		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy
				.getCurrencyExchangeRate(currencyCode, ApplicationEngine.XBE);

		if (currencyExchangeRate != null) {
			Currency currency = currencyExchangeRate.getCurrency();
			currencyInfoTO.setCurrencyDesc(currency.getCurrencyDescriprion());

			if (currency.getStatus().equals(WebConstants.RECORD_STATUS_ACTIVE) && currency.getXBEVisibility() == 1) {
				currencyInfoTO.setBoundryValue(currency.getBoundryValue());
				currencyInfoTO.setBreakPointValue(currency.getBreakPoint());
				currencyInfoTO.setDefaultIpgId(currency.getDefaultXbePGId());
				if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultXbePGId() != null) {
					currencyInfoTO.setIsCardPaymentEnabled(true);
				} else {
					currencyInfoTO.setIsCardPaymentEnabled(false);
				}
				currencyInfoTO.setCurrencyExchangeRate(currencyExchangeRate.getMultiplyingExchangeRate());
				boolean blnRoundAppParam = AppSysParamsUtil.isCurrencyRoundupEnabled();

				if (blnRoundAppParam && currencyInfoTO.getBoundryValue() != null
						&& currencyInfoTO.getBoundryValue().doubleValue() != 0 && currencyInfoTO.getBreakPointValue() != null) {
					currencyInfoTO.setIsApplyRounding(true);
				} else {
					currencyInfoTO.setIsApplyRounding(false);
				}
			}
		}

		return currencyInfoTO;
	}

	public static CurrencyInfoTO getCardPaymentCurrencyInfo(CurrencyInfoTO selectedCurrencyInfoTO,
			ExchangeRateProxy exchangeRateProxy, Integer ipgId, boolean isOfflinePayment) throws ModuleException {
		CurrencyInfoTO cardPayCurrencyTO = null;

		if ((ipgId != null && selectedCurrencyInfoTO != null && selectedCurrencyInfoTO.getIsCardPaymentEnabled() != null
				&& selectedCurrencyInfoTO.getIsCardPaymentEnabled() && ModuleServiceLocator.getPaymentBrokerBD()
				.isPaymentGatewaySupportsCurrency(ipgId, selectedCurrencyInfoTO.getCurrencyCode()))) {

			cardPayCurrencyTO = selectedCurrencyInfoTO;
		} else {
			if (ipgId != null) {
				IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
				ipgPaymentOptionDTO.setSelCurrency(selectedCurrencyInfoTO.getCurrencyCode());
				ipgPaymentOptionDTO.setPaymentGateway(ipgId);
				ipgPaymentOptionDTO.setModuleCode("XBE");
				ipgPaymentOptionDTO.setOfflinePayment(isOfflinePayment);
				List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getPaymentGateways(
						ipgPaymentOptionDTO);
				// Below if condition was added to resolve bug AEROMART-3223
				if (!isOfflinePayment && CollectionUtils.isEmpty(pgwList)) {
					ipgPaymentOptionDTO.setOfflinePayment(true);
					List<IPGPaymentOptionDTO> pgwListWithOffline = ModuleServiceLocator.getPaymentBrokerBD().getPaymentGateways(
							ipgPaymentOptionDTO);
					if (!CollectionUtils.isEmpty(pgwListWithOffline) && (
							PaymentConsts.PAY_FORT_OFFLINE.equals(pgwListWithOffline.get(0).getProviderName())
									|| PaymentConsts.EFAWATEER_OFFLINE
									.equals(pgwListWithOffline.get(0).getProviderName()))) {
						pgwList = pgwListWithOffline;
					}
				}
				String ipgCurrencyCode = pgwList.get(0).getSelCurrency();
				cardPayCurrencyTO = getCurrencyInfo(ipgCurrencyCode, exchangeRateProxy);

			} else {
				// Choosing default card payment currency (RES_69)
				String defaultCardPayCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
				cardPayCurrencyTO = getCurrencyInfo(defaultCardPayCurrencyCode, exchangeRateProxy);

				if (AppSysParamsUtil.isXBECreditCardPaymentsEnabled()) {
					if (cardPayCurrencyTO == null || cardPayCurrencyTO.getIsCardPaymentEnabled() == null
							|| !cardPayCurrencyTO.getIsCardPaymentEnabled()) {
						throw new XBERuntimeException("error.default.cardpay.currency.invalid");
					}
				}
			}
		}

		return cardPayCurrencyTO;
	}

	/**
	 * Returns CurrencyInfoTO for agency payment. If agentCodeForOnAccPayment is not set, it defaults the payment
	 * currency to agent currency.
	 * 
	 * @param agentCodeForOnAccPayment
	 * @param applicableExchageRateTimestamp
	 * @param selectedCurrencyInfoTO
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public static CurrencyInfoTO getAgentPaymentCurrencyInfoTO(String agentCodeForOnAccPayment, String loggedInAgencyCode,
			ExchangeRateProxy exchangeRateProxy, CurrencyInfoTO selectedCurrencyInfoTO) throws ModuleException {
		CurrencyInfoTO payCurrencyInfoTO;
		Agent agent = null;
		String currencyCodeForOnAccPayment = null;

		if (agentCodeForOnAccPayment == null || "".equals(agentCodeForOnAccPayment)) {
			agentCodeForOnAccPayment = loggedInAgencyCode;
		}

		agent = TravelAgentUtil.loadTravelAgent(agentCodeForOnAccPayment);
		currencyCodeForOnAccPayment = agent.getCurrencyCode();

		if (currencyCodeForOnAccPayment.equals(selectedCurrencyInfoTO.getCurrencyCode())) {
			payCurrencyInfoTO = selectedCurrencyInfoTO;
		} else {
			payCurrencyInfoTO = getCurrencyInfo(currencyCodeForOnAccPayment, exchangeRateProxy);
		}
		return payCurrencyInfoTO;
	}

	public static IPGIdentificationParamsDTO validateAndPrepareIPGIdentificationParamsDTO(CurrencyInfoTO cardPayCurrencyInfoTO,
			Integer ipgId) throws ModuleException {
		if (ipgId == null)
			ipgId = cardPayCurrencyInfoTO.getDefaultIpgId();
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
				cardPayCurrencyInfoTO.getCurrencyCode());

		boolean isPGExists = ModuleServiceLocator.getPaymentBrokerBD().checkForIPG(ipgIdentificationParamsDTO);

		if (AppSysParamsUtil.isXBECreditCardPaymentsEnabled() && !isPGExists) {
			throw new XBERuntimeException("error.cardpay.ipgconfig.notfound");
		}

		return ipgIdentificationParamsDTO;
	}
}
