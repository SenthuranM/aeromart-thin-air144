package com.isa.thinair.xbe.core.web.action.gds;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.gds.GdsMessagesRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Gds.GDS_MESSAGES_ADMIN),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowGdsMessagesAction extends BaseRequestAwareAction {

	public String execute() {
		return GdsMessagesRequestHandler.execute(request);
	}
}
