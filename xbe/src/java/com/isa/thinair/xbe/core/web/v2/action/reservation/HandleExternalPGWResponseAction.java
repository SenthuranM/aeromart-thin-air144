package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.xbe.api.dto.v2.ReservationPostPaymentDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Payment.EXTERNAL_PGW_FEEDBACK),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class HandleExternalPGWResponseAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(HandleExternalPGWResponseAction.class);

	public String execute() {

		ReservationPostPaymentDTO postPayData = (ReservationPostPaymentDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.POST_PAY_DATA);
		if (postPayData != null) {
			IPGResponseDTO externalPGWResponseDTO = postPayData.getIpgResponseDTO();
			Map<String, String> receiptyMap;
			try {
				externalPGWResponseDTO.setRequestTimsStamp(ModuleServiceLocator.getReservationBD().getPaymentRequestTime(
						postPayData.getIpgResponseDTO().getTemporyPaymentId()));
			} catch (ModuleException e) {
				log.error("Error in retrieving paymentRequestTime : " + e);
			}

			externalPGWResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
			externalPGWResponseDTO.setPaymentBrokerRefNo(postPayData.getPaymentBrokerRefNo());

			try {
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil
						.validateAndPrepareIPGConfigurationParamsDTO(postPayData.getIpgId(), postPayData.getCurrencyCode());
				receiptyMap = getReceiptMap(request);
				externalPGWResponseDTO = ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap,
						externalPGWResponseDTO, ipgIdentificationParamsDTO);
				request.getSession().setAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE,
						externalPGWResponseDTO);
			} catch (NumberFormatException e) {
				log.error("Error in retrieving paymentResponseData : " + e);
			} catch (ModuleException e) {
				log.error("Error in retrieving paymentResponseData : " + e);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	private static Map<String, String> getReceiptMap(HttpServletRequest request) {

		Map<String, String> fields = new LinkedHashMap<>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}
}
