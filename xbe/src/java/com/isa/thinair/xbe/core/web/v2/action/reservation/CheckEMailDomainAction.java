package com.isa.thinair.xbe.core.web.v2.action.reservation;

import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.EmailDomainUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * POJO to validate the email domain name
 * 
 * @author Jagath
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CheckEMailDomainAction extends BaseRequestAwareAction {

	private boolean exists = true;

	private boolean success = true;

	private String messageTxt;

	private static Log log = LogFactory.getLog(CheckEMailDomainAction.class);

	public String execute() throws ModuleException, NamingException {
		String result = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			String emailDomain = request.getParameter("domain");
			if (EmailDomainUtil.isDomainWhiteListed(emailDomain)) {
				exists = true;
			} else {
				exists = false;
			}

		} catch (NameNotFoundException exp) {
			exists = false;
		} catch (Exception e) {
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return result;
	}

	public boolean getExists() {
		return exists;
	}

	public boolean getSuccess() {
		return success;
	}
}