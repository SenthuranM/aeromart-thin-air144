package com.isa.thinair.xbe.core.web.handler.travagent;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.AgentTicketStock;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public final class StockRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(StockRequestHandler.class);

	private static final String PARAM_SRS_QTY = "txtSrsQty";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_AGENT_CODE = "hdnAgentCode";
	private static final String PARAM_MODE = "hdnMode";

	/**
	 * Main Execute Method for Travel Agent & Credit History
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = "success";

		String strMode = request.getParameter(PARAM_MODE);
		if (strMode != null && strMode.equals("SAVE")) {
			try {
				saveStock(request);
			} catch (ModuleException me) {
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			} catch (Exception e) {
				log.error(" Stock REQUEST HANDLER SETDISPLAYDATA() FAILED ", e);
				forward = S2Constants.Result.ERROR;
			}
		}

		try {
			setDisplayData(request);
		} catch (ModuleException me) {
			saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error(" Stock AGENT REQUEST HANDLER SETDISPLAYDATA() FAILED ", e);
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		String agentCode = request.getParameter(PARAM_AGENT_CODE);
		String strEticket = ModuleServiceLocator.getReservationBD().getCurrentEticketNo(agentCode);
		String strIATANumber = ModuleServiceLocator.getTravelAgentBD().getAgent(agentCode).getAgentIATANumber();

		if (strIATANumber == null || strIATANumber.equals("")) {
			strIATANumber = "";
		}
		setStockData(request);
		setRunninsequence(request, strIATANumber + strEticket);
		setActualLastEticket(request, agentCode);
	}

	private static void setRunninsequence(HttpServletRequest request, String strEticket) throws ModuleException {

		request.setAttribute("reqETktNo", "runNo = '" + strEticket + "';");

	}

	private static void setActualLastEticket(HttpServletRequest request, String agentCode) throws ModuleException {
		String strEticket = ModuleServiceLocator.getReservationBD().getLastActualEticket(agentCode);
		request.setAttribute("reqLastETktNo", "lastEticket = '" + strEticket + "';");
	}

	/**
	 * Doing it here if this is permanent move to HG
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setStockData(HttpServletRequest request) throws ModuleException {
		String agentCode = request.getParameter(PARAM_AGENT_CODE);
		Collection<AgentTicketStock> colStock = ModuleServiceLocator.getTravelAgentBD().getAgentTicketStock(agentCode);
		StringBuilder sb = new StringBuilder();
		int count = 0;
		User user = null;
		SimpleDateFormat sDf = new SimpleDateFormat("dd/MM/yyyy");
		for (AgentTicketStock agentStock : colStock) {
			sb.append("arrStockData[" + count + "] = new Array();");
			sb.append("arrStockData[" + count + "][0]= '" + count + "';");
			sb.append("arrStockData[" + count + "][1]= '" + sDf.format(agentStock.getAddedDate()) + "';");
			sb.append("arrStockData[" + count + "][2]= '" + agentStock.getCreatedBy() + "';");
			user = ModuleServiceLocator.getSecurityBD().getUserBasicDetails(agentStock.getCreatedBy());
			sb.append("arrStockData[" + count + "][3]= '" + user.getDisplayName() + "';");
			sb.append("arrStockData[" + count + "][4]= '" + agentStock.getSeriesFrom() + "';");
			sb.append("arrStockData[" + count + "][5]= '" + agentStock.getSeriesTo() + "';");

			long lngTo = new Long(agentStock.getSeriesTo()).longValue();
			long dif = lngTo - Long.parseLong(ModuleServiceLocator.getReservationBD().getCurrentTicketNoOnly(agentCode));

			sb.append("arrStockData[" + count + "][6]= '" + dif + "';");
			sb.append("arrStockData[" + count + "][7]= '" + agentStock.getRemarks() + "';");

			count++;
		}

		request.setAttribute("reqStockData", sb.toString());
	}

	private static void saveStock(HttpServletRequest request) throws ModuleException {
		AgentTicketStock agentTicketStock = new AgentTicketStock();
		String strSrsQty = StringUtil.getNotNullString(request.getParameter(PARAM_SRS_QTY));
		String strAgentCode = StringUtil.getNotNullString(request.getParameter(PARAM_AGENT_CODE));
		String strRemarks = StringUtil.getNotNullString(request.getParameter(PARAM_REMARKS));
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

		agentTicketStock.setAddedDate(new Date());
		agentTicketStock.setAgentCode(strAgentCode);
		agentTicketStock.setCreatedBy(userPrincipal.getUserId());
		agentTicketStock.setStockQty(Long.parseLong(strSrsQty));
		agentTicketStock.setVersion(-1l);
		agentTicketStock.setRemarks(strRemarks);

		ModuleServiceLocator.getTravelAgentBD().saveAgentStock(agentTicketStock);
	}

}
