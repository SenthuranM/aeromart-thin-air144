package com.isa.thinair.xbe.core.web.handler.system;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.utils.AirSecurityUtil;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.system.PasswordChangeHG;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class ShowPasswordChangeRH {

	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SHOW_PASSWORD_CHANGE;

		setDisplayData(request);

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) {
		setClientErrors(request);
		setRequestHtml(request);
		setReseted(request);
		setPasswordExpire(request);
	}

	private static void setRequestHtml(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();

		sb.append("request['contextPath']='" + BasicRH.getContextPath(request) + "';");
		sb.append("request['secureUrl']='" + BasicRH.getSecureUrl(request) + "';");
		sb.append("request['userId']='" + request.getRemoteUser() + "';");
		sb.append("request['protocolStatus']='" + BasicRH.isProtocolStatusEqual() + "';");
		sb.append("request['useSecureLogin']=" + XBEModuleUtils.getConfig().isUseSecureLogin() + ";");

		request.setAttribute(WebConstants.REQ_HTML_REQUEST, sb.toString());

	}

	private static void setClientErrors(HttpServletRequest request) {
		String strClientErrors = PasswordChangeHG.getClientErrors(request);
		BasicRH.saveClientErrors(request, strClientErrors);
	}

	private static void setReseted(HttpServletRequest request) {
		String reset = "";
		reset = request.getParameter("reset");
		if (reset != null && reset.equals("true")) {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_RESET_MSG, "true");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_RESET_MSG, "false");
		}
	}

	private static void setPasswordExpire(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		boolean isPasswordExpired = AirSecurityUtil.isPasswordExpire(user.getPasswordExpiryDate());
		if (isPasswordExpired) {
			request.setAttribute(WebConstants.REQ_HTML_PWD_EXPIRED, "true");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_PWD_EXPIRED, "false");
		}
	}

}
