package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadRBDAllFaresAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(LoadRBDAllFaresAction.class);

	private FlightSearchDTO searchParams;

	private boolean success = true;

	private String messageTxt;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

		} catch (Exception e) {
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private Map<String, List<String>> createCarrierWiseSegMap() {
		// TODO Auto-generated method stub
		return null;
	}

	private HashMap<String, Collection<FlightSegmentDTO>> transformJsonFlightSegments(String str) {
		return null;
	}

	private void clearValues() {

	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}
}
