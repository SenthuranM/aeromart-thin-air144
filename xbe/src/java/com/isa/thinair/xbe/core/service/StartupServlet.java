package com.isa.thinair.xbe.core.service;

import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.xbe.core.config.XBEModuleConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

/**
 * Initilizes application framework and initiliazes application scoper parameters
 */
public class StartupServlet extends HttpServlet {

	private static final long serialVersionUID = 4975249435359114995L;

	private static GlobalConfig globalConfig = null;

	private static XBEModuleConfig xbeConfig = null;

	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		ModuleFramework.startup();

		globalConfig = ModuleServiceLocator.getGlobalConfig();
		xbeConfig = XBEModuleUtils.getConfig();

		setGlobals();
	}

	public void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, java.io.IOException {
	}

	private void setGlobals() {
		setCarrierName();
		setdefaultCarrierCode();
		setReleaseVersion();
		setNonEncryptedURLs();
		setGoogleAnalyticCode();
	}

	private void setdefaultCarrierCode() {
		String strCarrCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		getServletContext().setAttribute(WebConstants.APP_CARRIER_CODE, strCarrCode);

	}

	private void setNonEncryptedURLs() {
		getServletContext().setAttribute(WebConstants.APP_MAP_NON_ENCRYPTED_URLS, xbeConfig.getNonEncryptedURLs());
	}

	private void setCarrierName() {
		String strCarrName = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME);
		getServletContext().setAttribute(WebConstants.APP_CARRIER_NAME, strCarrName);
	}

	private void setReleaseVersion() {
		if (SystemPropertyUtil.isCreateNewBuildIdWithEveryRestart()) {
			getServletContext().setAttribute(WebConstants.APP_RELEASE_VERSION,
					WebConstants.APP_RELEASE_VERSION_URL_KEY + "=" + new Date().getTime());
		} else {
			getServletContext().setAttribute(WebConstants.APP_RELEASE_VERSION,
					WebConstants.APP_RELEASE_VERSION_URL_KEY + "=" + globalConfig.getLastBuildVersion());
		}
	}
	
	private void setGoogleAnalyticCode() {
		StringBuffer sb = new StringBuffer();
		if (AppSysParamsUtil.isXBETrackingEnable()) {
			sb.append("<script type='text/javascript'>");
			sb.append("try{");
			sb.append("var _gaq = _gaq || [];");
			sb.append("_gaq.push(['_setAccount', '" + AppSysParamsUtil.getGoogleTrackingId() + "']);");
			sb.append("_gaq.push(['_setDomainName', '." + AppSysParamsUtil.getAirlineDomainName() + "']);");
			sb.append("_gaq.push(['_trackPageview']);");
			sb.append("(function() {");
			sb.append("var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;");
			sb.append("ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';");
			sb.append("var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);");
			sb.append("})();");
			sb.append("}catch(e){}");
			sb.append("</script>");
		}
		getServletContext().setAttribute(WebConstants.XBE_ANALYTIC_JS, sb.toString());
	}

}
