package com.isa.thinair.xbe.core.web.v2.action.schedrept;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.CommonsException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webplatform.core.schedrept.ReportInputs;
import com.isa.thinair.webplatform.core.schedrept.SRPComposer;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ScheduleAReportAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ScheduleAReportAction.class);

	// scheduling cron type
	private static String DAILY = "DAILY";
	private static String EVERY_WEEK_START = "EVERY_WEEK_START";
	private static String EVERY_WEEK_END = "EVERY_WEEK_END";
	private static String EVERY_MONTH_END = "EVERY_MONTH_END";
	private static String EVERY_MONTH_MIDDLE_N_END = "EVERY_MONTH_MIDDLE_N_END";

	private static String UI_DATE_FORMAT = "dd/MM/yyyy";

	private String errorMessage;
	private boolean success;

	private String composerName;
	private String cronType;
	private String reportPeriodCriteria;
	private String scheduledTime;
	private String sendReportTo;
	private String startDate;
	private String endDate;

	public String execute() throws Exception {

		success = true;

		try {
			if (isScheduleTimeInvalid(scheduledTime, startDate, endDate)) {
				success = false;
				errorMessage = "Current time Cannot be less than Zulu time";
				return S2Constants.Result.SUCCESS;
			}
			SRPComposer composer = WPModuleUtils.getSRPComposer(composerName);
			ReportInputs reportInputs = composer.composeReportInputs(request);

			String cronExperession = this.generateCronExpression();

			XStream xstream = new XStream(new DomDriver());
			String criteriaTemplete = xstream.toXML(reportInputs.getCriteria());
			String paramTemplete = xstream.toXML(reportInputs.getReportParams());

			ScheduledReport scheduledReport = new ScheduledReport();

			scheduledReport.setCreatedTime(new Date(System.currentTimeMillis()));
			scheduledReport.setCronExpression(cronExperession);
			scheduledReport.setParamTemplete(paramTemplete);
			scheduledReport.setCriteriaTemplete(criteriaTemplete);
			scheduledReport.setReportName(reportInputs.getReportName());
			scheduledReport.setScheduledBy(request.getUserPrincipal().getName());
			scheduledReport.setSendReportTo(sendReportTo);
			scheduledReport.setReportFormat(reportInputs.getReportFormat());
			scheduledReport.setReportTempleteRelativePath(reportInputs.getReportTempleteRelativePath());
			scheduledReport.setStatus(ScheduledReport.STATUS_NOT_STARTED);
			scheduledReport.setDateCalculationMechanism(reportPeriodCriteria);

			SimpleDateFormat format = new SimpleDateFormat(UI_DATE_FORMAT + " HH:mm:ss");
			scheduledReport.setStartDate(format.parse(startDate + " 00:00:00"));
			scheduledReport.setEndDate(format.parse(endDate + " 23:59:59"));

			ModuleServiceLocator.getScheduledReportBD().scheduleAReport(scheduledReport);

		} catch (Exception e) {
			success = false;
			if (e instanceof CommonsException) {
				errorMessage = ((CommonsException) e).getMessageString();
			} else {
				errorMessage = e.getMessage();
			}
			
			errorMessage = BeanUtils.nullHandler(errorMessage);
			if (errorMessage.length() == 0) {
				errorMessage = "Scheduled Error, please report!";
			}
			log.error(errorMessage, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private boolean isScheduleTimeInvalid(String time, String fromDate, String toDate) {// FIXME
		if (fromDate != null && toDate != null && !fromDate.equals(toDate)) {
			return false;
		}
		Date currDate = CalendarUtil.getCurrentSystemTimeInZulu();
		if (!fromDate.equals(CalendarUtil.getDateInFormattedString(UI_DATE_FORMAT, currDate))) {
			return false;
		}
		int minute = new Integer(time.substring(time.indexOf(":") + 1));
		int hour = new Integer(time.substring(0, time.indexOf(":")));

		String currDateAsStr = CalendarUtil.getDateInFormattedString("HH mm", currDate);
		String[] splitedCurrentDate = currDateAsStr.split(" ");

		if (hour < new Integer(splitedCurrentDate[0])) {
			if (minute < new Integer(splitedCurrentDate[0])) {
				return true;
			}
			return false;
		}
		return false;
	}

	private String generateCronExpression() {

		String cronExpression = null;
		// scheduledTime is expected in hh:mm format
		int minute = new Integer(scheduledTime.substring(scheduledTime.indexOf(":") + 1));
		int hour = new Integer(scheduledTime.substring(0, scheduledTime.indexOf(":")));

		if (cronType.equals(DAILY)) {
			cronExpression = "0 " + minute + " " + hour + " * * ?";
		} else if (cronType.equals(EVERY_WEEK_START)) {
			cronExpression = "0 " + minute + " " + hour + " ? * SUN";
		} else if (cronType.equals(EVERY_WEEK_END)) {
			cronExpression = "0 " + minute + " " + hour + " ? * SAT";
		} else if (cronType.equals(EVERY_MONTH_END)) {
			cronExpression = "0 " + minute + " " + hour + " L * ?";
		} else if (cronType.equals(EVERY_MONTH_MIDDLE_N_END)) {
			cronExpression = "0 " + minute + " " + hour + " 15,L * ?";
		} else {
			// TODO testing only, should remove later
			cronExpression = "0 0/5 * * * ?";
		}

		return cronExpression;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getReportPeriodCriteria() {
		return reportPeriodCriteria;
	}

	public void setReportPeriodCriteria(String reportPeriodCriteria) {
		this.reportPeriodCriteria = reportPeriodCriteria;
	}

	public String getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public String getSendReportTo() {
		return sendReportTo;
	}

	public void setSendReportTo(String sendReportTo) {
		this.sendReportTo = sendReportTo;
	}

	public String getCronType() {
		return cronType;
	}

	public void setCronType(String cronType) {
		this.cronType = cronType;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getComposerName() {
		return composerName;
	}

	public void setComposerName(String composerName) {
		this.composerName = composerName;
	}

}
