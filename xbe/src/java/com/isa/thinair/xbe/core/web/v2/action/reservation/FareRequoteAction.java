package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.xbe.api.dto.v2.XBEReservationContactInfoDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.OpenReturnInfo;
import com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.FareTypeInfoTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class FareRequoteAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(FareRequoteAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private Collection<FlightInfoTO> flightInfo;

	private FareTypeInfoTO availableFare;

	private boolean currencyRound = false;

	private String airportMessage;

	private boolean resetAll = false;

	private String groupPNR;

	private ReservationProcessParams rpParams;

	private boolean addSegment = false;

	private boolean transferSegment = false;

	private boolean modifyBooking = false;

	private String classOfServiceDesc;

	private OpenReturnInfo openReturnInfo;

	private boolean isOpenReturn = false;

	private boolean showFareDiscountCodes = false;

	private List<String[]> fareDiscountCodeList = null;

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private boolean domFareDiscountApplied = false;

	private String pnr;

	private DiscountedFareDetails appliedFareDiscount = null;

	private boolean overrideFareDiscount = false;

	private HashMap<String, List<String>> subJourneyGroupMap;

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();

	private Set<String> busCarrierCodes;

	private String operatingCarrierCodesList;
	
	private HashMap<String, List<String>> segmentsMapPerSegType;

	private boolean taxInvoicesExist;
	
	private boolean displayBasedOnTemplates = false;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			int confirmedSegsInReservation = 0;

			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil
					.transformJsonSegments(request.getParameter("resSegments"));
			LCCClientReservationSegment firstCNFSegment = null;
			for (LCCClientReservationSegment seg : colsegs) {
				if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(seg.getStatus())) {
					firstCNFSegment = seg;
					break;
				}
			}

			String newFromAirportCode = fareQuoteParams.getOndList().get(0).getFromAirport();
			boolean isGroupPnr = ReservationUtil.isGroupPnr(groupPNR);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
			XBEReservationContactInfoDTO contactInfoDTO = resInfo.getContactInfoDTO();

			if (!ReservationBeanUtil
					.isRequoteValidWithServiceTax(firstCNFSegment.getSegmentCode().split("/")[0], newFromAirportCode, isGroupPnr,
							taxInvoicesExist)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("cc_resmod_modify_book_selected_route_not_allowed", userLanguage);
				return S2Constants.Result.SUCCESS;
			}

			if (!ReservationBeanUtil.isContactInfoValidForRouteModWithServiceTax(newFromAirportCode, contactInfoDTO.getCountry(),
					contactInfoDTO.getState(), StringUtils.defaultString(contactInfoDTO.getStreetAddress1()) + StringUtils
							.defaultString(contactInfoDTO.getStreetAddress2()), isGroupPnr)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("cc_resmod_modify_state_and_address_required", userLanguage);
				return S2Constants.Result.SUCCESS;
			}

			boolean hasPrivViewAvailability = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);

			boolean isAllowFlightSearchAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);
			boolean isAllowOverbookAfterCutoffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME);

			// boolean isAllowOverrideSameOrHigherFare = BasicRH.hasPrivilege(request,
			// PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALLOW_OVERRIDE_SAME_OR_HIGHER_FARE);

			overrideFareDiscount = BasicRH.hasPrivilege(request, PriviledgeConstants.OVERRIDE_FARE_DISCOUNT);
			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, AppIndicatorEnum.APP_XBE);

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			boolean isGoshowAgent = false;
			if (AppSysParamsUtil.isOnlyGoshoFaresVisibleInFlightCutoffTime()) {
				isGoshowAgent = ModuleServiceLocator.getAirportBD().isGoshoAgent(fareQuoteParams.getFromAirport());
			}
			if (isGoshowAgent && isAllowFlightSearchAfterCutOffTime) {
				fareQuoteParams.setBookingType(BookingClass.BookingClassType.OVERBOOK);
			}

			flightPriceRQ.getAvailPreferences().setRequoteFlightSearch(true);
			flightPriceRQ.getAvailPreferences().setModifyBooking(true);
			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());
			flightPriceRQ.getAvailPreferences().setRestrictionLevel(AvailabilityConvertUtil
					.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats, fareQuoteParams.getBookingType()));
			flightPriceRQ.getAvailPreferences().setQuoteFares(true);
			flightPriceRQ.getAvailPreferences()
					.setAllowOverrideSameOrHigherFareMod(fareQuoteParams.getAllowOverrideSameOrHigherFare());
			if (isGoshowAgent) {
				flightPriceRQ.getAvailPreferences().setGoshoFareAgent(true);
			} else if (ReservationUtil.isCharterAgent(request)) {
				flightPriceRQ.getAvailPreferences().setFixedFareAgent(true);
			}

			if (operatingCarrierCodesList != null && operatingCarrierCodesList != "") {
				flightPriceRQ.getAvailPreferences().setSearchSystem(deriveSearchSystem(operatingCarrierCodesList));
			}

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (isGroupPNR) {
				flightPriceRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}

			if (log.isDebugEnabled()) {
				if (flightPriceRQ.getTransactionIdentifier() != null) {
					log.debug("Fare quote" + flightPriceRQ.getTransactionIdentifier());
				}
			}

			if (pnr != null) {
				flightPriceRQ.getAvailPreferences().setPnr(pnr);
			}

			AnalyticsLogger.logRequoteSearch(AnalyticSource.XBE_REQUOTE, flightPriceRQ, request, pnr, getTrackInfo());
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(isAllowOverbookAfterCutoffTime);
			flightPriceRQ.getAvailPreferences().setAllowFlightSearchAfterCutOffTime(isAllowFlightSearchAfterCutOffTime);

			FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ,
					getTrackInfo());

			currencyRound = CommonUtil.enableCurrencyRounding(fareQuoteParams.getSelectedCurrency());
			showFareDiscountCodes = AppSysParamsUtil.enableFareDiscountCodes();
			if (showFareDiscountCodes) {
				fareDiscountCodeList = SelectListGenerator.createFareDiscountCodeList();
			}
			if (resInfo.getTransactionId() == null && flightPriceRS.getTransactionIdentifier() != null) {
				resInfo.setTransactionId(flightPriceRS.getTransactionIdentifier());
				flightPriceRQ.setTransactionIdentifier(flightPriceRS.getTransactionIdentifier());
			}
			// TODO check scenarios where fare quote might not have valid selected price flight info
			PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();
			bookingShoppingCart.setPriceInfoTO(priceInfoTO);
			bookingShoppingCart.addNewServiceTaxes(flightPriceRS.getApplicableServiceTaxes());

			isOpenReturn = (flightPriceRS.getOpenReturnOptionsTO() != null);
			if (isOpenReturn) {
				openReturnInfo = new OpenReturnInfo(flightPriceRS.getOpenReturnOptionsTO(), fareQuoteParams.getFromAirport(),
						fareQuoteParams.getToAirport());
			}

			if (priceInfoTO != null) {
				// handling charge is not considered for modifications
				if (isCalculateHandlingFee()) {
					this.calculateTotalHandlingFee();
				}
				ArrayList<String> domesticSegmentCodeList = new ArrayList<String>();
				ArrayList<String> internationalSegmentCodeList = new ArrayList<String>();

				busCarrierCodes = new HashSet<String>();
				flightInfo = ReservationBeanUtil.createSelecteFlightInfo(flightPriceRS, domesticSegmentCodeList,
						internationalSegmentCodeList, busCarrierCodes, false);
				busCarrierCodes.addAll(SelectListGenerator.getBusCarrierCodes());

				flightInfo = SortUtil.sortFlightSegBySubJourneyGroup(new ArrayList<FlightInfoTO>(flightInfo));

				segmentsMapPerSegType = ReservationBeanUtil.getSegmentsMap(flightInfo);
				
				BigDecimal fareDiscountAmount = BigDecimal.ZERO;

				if (ReservationBeanUtil.hasIntAndDomFlights(domesticSegmentCodeList, internationalSegmentCodeList)) {
					bookingShoppingCart.setDomesticFareDiscount(
							flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList(), domesticSegmentCodeList);
					DiscountedFareDetails discountedFareDetails = bookingShoppingCart.getFareDiscount();
					if (discountedFareDetails.isDomesticDiscountApplied()) {
						ReservationUtil.calculateDiscount(bookingShoppingCart, flightPriceRQ,
								flightPriceRS.getTransactionIdentifier(), getTrackInfo());

						fareDiscountAmount = bookingShoppingCart.getTotalFareDiscount(true);
						if (fareDiscountAmount.doubleValue() > 0) {
							domFareDiscountApplied = true;
						}
					}

				}
				
				displayBasedOnTemplates = AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed();

				if (!domFareDiscountApplied) {
					boolean hasAddFareDiscPrev = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_ADD_FARE_DISCOUNTS);
					boolean hasOverrideFareDiscPrev = BasicRH.hasPrivilege(request, PriviledgeConstants.OVERRIDE_FARE_DISCOUNT);
					ReservationUtil.applyFareDiscount(pnr, isGroupPnr, priceInfoTO.getFareTypeTO().getFareDiscountInfo(),
							bookingShoppingCart, flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList(),
							hasAddFareDiscPrev, hasOverrideFareDiscPrev, flightPriceRQ, flightPriceRS.getTransactionIdentifier());

					fareDiscountAmount = bookingShoppingCart.getTotalFareDiscount(true);
				}

				if (fareDiscountAmount.doubleValue() > 0) {
					appliedFareDiscount = bookingShoppingCart.getFareDiscount();
				}

				ReservationProcessParams rParm = new ReservationProcessParams(request,
						resInfo != null ? resInfo.getReservationStatus() : null, null, true, false,
						resInfo.getLccPromotionInfoTO(), null, false, false);
				BigDecimal handlingCharge = BigDecimal.ZERO;
				handlingCharge = bookingShoppingCart.getHandlingCharge();
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

				availableFare = ReservationUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(),
						flightPriceRS.getOriginDestinationInformationList(), handlingCharge, fareQuoteParams, rParm,
						fareDiscountAmount, exchangeRateProxy, priceInfoTO.getFareSegChargeTO(), flightPriceRQ, getTrackInfo(), null);

				setClassOfServiceDesc(ReservationBeanUtil.getClassOfServiceDescription(flightInfo));

				List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
				ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);

				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());

			}

			if (priceInfoTO == null && flightPriceRS.getSubJourneyGroupMap() != null
					&& flightPriceRS.getSubJourneyGroupMap().size() > 0) {
				this.subJourneyGroupMap = flightPriceRS.getSubJourneyGroupMap();
			}
			
			// Hide Stop Over Airport
			if (AppSysParamsUtil.isHideStopOverEnabled()) {
				if (availableFare != null) {
					getHideStopOverAirportForFareQuote(availableFare);
				}

				if (flightInfo != null) {
					for (FlightInfoTO flightInfoTo : flightInfo) {
						String originDest = flightInfoTo.getOrignNDest();
						if (originDest != null && originDest.split("/").length > 2) {
							flightInfoTo.setOrignNDest(ReservationApiUtils.hideStopOverSeg(originDest));
						}
					}
				}
			}

			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightPriceRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, null);

			rpParams = (ReservationProcessParams) request.getSession()
					.getAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS);

		} catch (ModuleException me) {
			log.error(me.getMessage(), me);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(me, userLanguage);
			if (me.getExceptionCode().equals("airinventory.arg.farequote.flightclosed")) {
				resetAll = true;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	private void getHideStopOverAirportForFareQuote(FareTypeInfoTO fareType) {
		Collection<FareQuoteTO> FareQuoteTOs = fareType.getFareQuoteTO();
		for (FareQuoteTO fareQuoteTo : FareQuoteTOs) {
			String ondCode = fareQuoteTo.getOndCode();
			if (ondCode != null && ondCode.split("/").length > 2) {
				fareQuoteTo.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode), false);
			}
			Collection<SegmentFareTO> segmentWiseFares = fareQuoteTo.getSegmentWise();
			for (SegmentFareTO segmentFareTo : segmentWiseFares) {
				String fareSegment = segmentFareTo.getSegmentCode();
				segmentFareTo.setSegmentCode(ReservationApiUtils.hideStopOverSeg(fareSegment));
			}
		}

		/*
		 * FareRulesInformationDTO fareRuleInfoDto = fareType.getFareRulesInfo(); List<FareRuleDTO> fareRules =
		 * fareRuleInfoDto.getFareRules(); for(FareRuleDTO fareRule : fareRules){ String fareRuleSeg =
		 * fareRule.getOrignNDest(); fareRule.setOrignNDest(ReservationApiUtils.hideStopOverSeg(fareRuleSeg)); }
		 * 
		 * FlexiFareRulesInformationDTO flexiFareRulesInfoDTO = fareType.getFlexiFareRulesInfo(); List<FareRuleDTO>
		 * flexiRules = flexiFareRulesInfoDTO.getFareRules(); for(FareRuleDTO flexiFareRule : flexiRules){ String
		 * fareRuleSeg = flexiFareRule.getOrignNDest();
		 * flexiFareRule.setOrignNDest(ReservationApiUtils.hideStopOverSeg(fareRuleSeg)); }
		 */
	}

	private void clearValues() {
		flightInfo = null;
		currencyRound = false;
		// outFlightRPHList = null;
		// retFlightRPHList = null;
		flightInfo = null;
		availableFare = null;
		// outboundLogicalCCList = null;
		// inboundLogicalCCList = null;
	}

	private boolean isCalculateHandlingFee() {
		return false;
	}

	private void calculateTotalHandlingFee() throws ModuleException {

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		PaxSet paxSet = new PaxSet(fareQuoteParams.getAdultCount(), fareQuoteParams.getChildCount(),
				fareQuoteParams.getInfantCount());
		ExternalChargeUtil.calculateExternalChargeAmount(paxSet, bookingShoppingCart.getPriceInfoTO(),
				bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.HANDLING_CHARGE), false); // FIXME
																												// pass
																												// the
																												// return
																												// jouney
																												// flag

		bookingShoppingCart.addSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE);
	}

	private SYSTEM deriveSearchSystem(String operatingCarrierCodes) {

		String[] operatingCarrierCodesArray = operatingCarrierCodes.split(",");

		Set<String> ownAirlineCodes = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes();
		boolean nonOwnCarriersExists = false;
		boolean ownCarriersExists = false;
		SYSTEM searchSystem = SYSTEM.AA;

		for (String carrierCode : operatingCarrierCodesArray) {
			if (!ownAirlineCodes.contains(carrierCode)) {
				nonOwnCarriersExists = true;
			} else {
				ownCarriersExists = true;
			}
		}

		if (nonOwnCarriersExists) {
			searchSystem = SYSTEM.INT;
		} else if (ownCarriersExists) {
			searchSystem = SYSTEM.AA;
		}
		return searchSystem;
	}

	private boolean isValidSgementCalcellation(int noOfSegmentsInReservation) {

		boolean validSegmentCancellation = true;

		if (noOfSegmentsInReservation > fareQuoteParams.getOndList().size()) {
			String newResOrigin = fareQuoteParams.getOndList().get(0).getFromAirport();
			String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			String newOriginCountryCode = (String) XBEModuleUtils.getGlobalConfig().retrieveAirportInfo(newResOrigin)[3];
			for (String countryCode : taxRegNoEnabledCountries) {
				if (newOriginCountryCode.equals(countryCode)) {
					validSegmentCancellation = false;
					break;
				}
			}
		}

		return validSegmentCancellation;

	}

	@JSON(serialize = false)
	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public Collection<FlightInfoTO> getFlightInfo() {
		return flightInfo;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public FareTypeInfoTO getAvailableFare() {
		return availableFare;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public boolean isResetAll() {
		return resetAll;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public ReservationProcessParams getRpParams() {
		return rpParams;
	}

	public void setRpParams(ReservationProcessParams rpParams) {
		this.rpParams = rpParams;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public boolean isTransferSegment() {
		return transferSegment;
	}

	public void setTransferSegment(boolean transferSegment) {
		this.transferSegment = transferSegment;
	}

	public boolean isModifyBooking() {
		return modifyBooking;
	}

	public void setModifyBooking(boolean modifyBooking) {
		this.modifyBooking = modifyBooking;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}

	/**
	 * @return the openReturnInfo
	 */
	public OpenReturnInfo getOpenReturnInfo() {
		return openReturnInfo;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	public boolean isShowFareDiscountCodes() {
		return showFareDiscountCodes;
	}

	public void setShowFareDiscountCodes(boolean showFareDiscountCodes) {
		this.showFareDiscountCodes = showFareDiscountCodes;
	}

	public List<String[]> getFareDiscountCodeList() {
		return fareDiscountCodeList;
	}

	public void setFareDiscountCodeList(List<String[]> fareDiscountCodeList) {
		this.fareDiscountCodeList = fareDiscountCodeList;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	public boolean isDomFareDiscountApplied() {
		return domFareDiscountApplied;
	}

	public void setDomFareDiscountApplied(boolean domFareDiscountApplied) {
		this.domFareDiscountApplied = domFareDiscountApplied;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public DiscountedFareDetails getAppliedFareDiscount() {
		return appliedFareDiscount;
	}

	public void setAppliedFareDiscount(DiscountedFareDetails appliedFareDiscount) {
		this.appliedFareDiscount = appliedFareDiscount;
	}

	public boolean isOverrideFareDiscount() {
		return overrideFareDiscount;
	}

	public void setOverrideFareDiscount(boolean overrideFareDiscount) {
		this.overrideFareDiscount = overrideFareDiscount;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public HashMap<String, List<String>> getSubJourneyGroupMap() {
		return subJourneyGroupMap;
	}

	public void setSubJourneyGroupMap(HashMap<String, List<String>> subJourneyGroupMap) {
		this.subJourneyGroupMap = subJourneyGroupMap;
	}

	public Set<String> getBusCarrierCodes() {
		return busCarrierCodes;
	}

	public String getOperatingCarrierCodesList() {
		return operatingCarrierCodesList;
	}

	public void setOperatingCarrierCodesList(String operatingCarrierCodesList) {
		this.operatingCarrierCodesList = operatingCarrierCodesList;
	}

	public HashMap<String, List<String>> getSegmentsMapPerSegType() {
		return segmentsMapPerSegType;
	}

	public void setSegmentsMapPerSegType(HashMap<String, List<String>> segmentsMapPerSegType) {
		this.segmentsMapPerSegType = segmentsMapPerSegType;
	}
	
	public boolean isTaxInvoicesExist() {
		return taxInvoicesExist;
	}

	public void setTaxInvoicesExist(boolean taxInvoicesExist) {
		this.taxInvoicesExist = taxInvoicesExist;
	}

	public boolean isDisplayBasedOnTemplates() {
		return displayBasedOnTemplates;
	}


}
