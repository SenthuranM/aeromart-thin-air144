package com.isa.thinair.xbe.core.web.handler.palcal;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.messaging.core.config.MailServerConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.palcal.SendPALCALHG;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class SendPALCALRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(SendPALCALRequestHandler.class);

	private static XBEConfig XBEConfig = new XBEConfig();

	private static final String PARAM_PAL_TIMESTAMP = "hdnPALTimestamp";

	private static final String PARAM_CAL_TIMESTAMP = "hdnCALTimestamp";

	private static final String PARAM_PALCAL_STATUS = "hdnTransmitStatus";

	private static final String PARAM_ASSIGN_SITA_ADDRESSESS = "hdnSITAValues";

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_AIRPORT_CODE = "selAirport";

	private static final String PARAM_SEARCH_FLIGHTNO = "txtFlightNo";

	private static final String PARAM_SEARCH_FLIGHTDATE = "txtDate";

	private static final String PARAM_MAIL_SERVER = "selMailServer";
	private static final String PARAM_SELECTED_SITA = "hdnSITAValues";

	private static final String PARAM_FLIGHT_ID = "hdnFlightID";

	private static final String PARAM_TS_SELECTED = "hdnTS";

	private static final String PARAM_SITA = "hdnRowSITA";

	private static final String PARAM_MSG_TYPE = "hdnmsgType";

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setPALCALText(HttpServletRequest request, String text) {
		setAttribInRequest(request, "pnlAdlText", new String(text));
	}

	private static String getPNLADLText(HttpServletRequest request) {
		String returnValue = (String) getAttribInRequest(request, "pnlAdlText");
		if (returnValue != null)
			return returnValue;
		else
			return null;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * @param request
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = S2Constants.Result.SUCCESS;
		String strUIModeJS = "var isSearchMode = false; var pnlAdlText =''";
		setExceptionOccured(request, false);
		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";

		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if (strHdnMode != null
					&& (strHdnMode.equals(WebConstants.ACTION_NEWPAL) || strHdnMode.equals(WebConstants.ACTION_NEWCAL)
							|| strHdnMode.equals(WebConstants.ACTION_RESENDPAL)
							|| strHdnMode.equals(WebConstants.ACTION_RESENDCAL)
							|| strHdnMode.equals(WebConstants.ACTION_REPRINT_CAL)
							|| strHdnMode.equals(WebConstants.ACTION_REPRINT_PAL)
							|| strHdnMode.equals(WebConstants.ACTION_PRINT_NEW_CAL)
							|| strHdnMode.equals(WebConstants.ACTION_PRINT_NEW_PAL) || strHdnMode
								.equals(WebConstants.ACTION_PRINT))) {
				if (sendPNLADLData(request)) {

					if (!isExceptionOccured(request)) {

						strFormData = " var saveSuccess = " + getIntSuccess(request) + ";";
						strFormData += " var arrFormData = new Array();";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					}
				}
			}

			setDisplayData(request);

		} catch (Exception exception) {

			if (exception instanceof RuntimeException) {

				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static boolean sendPNLADLData(HttpServletRequest request) throws Exception {
		String strFlightNo = null;
		String strAirport = null;
		String strPALTimestamp = null;
		String strCALTimestamp = null;
		String strDateOfFlight = null;
		String strSITAAddress = null;
		String strMailServer = null;
		String strPALCALStatus = null;
		Date timeStampPALDate = null;
		Date timeStampCALDate = null;
		Date timeStampSel = null;
		Date dateOfFlight = null;
		String strSelectedSita = null;

		String strPrintText = null;
		String flightId = null;
		String selectedTS = null;
		String selectedSITA = null;
		String messageType = null;
		String strHdnMode = "";
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			strHdnMode = request.getParameter(PARAM_MODE);
			strFlightNo = request.getParameter(PARAM_SEARCH_FLIGHTNO) == null ? "" : request.getParameter(PARAM_SEARCH_FLIGHTNO);
			strAirport = request.getParameter(PARAM_AIRPORT_CODE) == null ? "" : request.getParameter(PARAM_AIRPORT_CODE);
			strPALTimestamp = request.getParameter(PARAM_PAL_TIMESTAMP) == null ? "" : request.getParameter(PARAM_PAL_TIMESTAMP);
			strCALTimestamp = request.getParameter(PARAM_CAL_TIMESTAMP) == null ? "" : request.getParameter(PARAM_CAL_TIMESTAMP);
			strDateOfFlight = request.getParameter(PARAM_SEARCH_FLIGHTDATE) == null ? "" : request
					.getParameter(PARAM_SEARCH_FLIGHTDATE);
			strSITAAddress = request.getParameter(PARAM_ASSIGN_SITA_ADDRESSESS) == null ? "" : request
					.getParameter(PARAM_ASSIGN_SITA_ADDRESSESS);
			strMailServer = request.getParameter(PARAM_MAIL_SERVER) == null ? "" : request.getParameter(PARAM_MAIL_SERVER);
			strPALCALStatus = request.getParameter(PARAM_PALCAL_STATUS) == null ? "" : request.getParameter(PARAM_PALCAL_STATUS);
			strSelectedSita = request.getParameter(PARAM_SELECTED_SITA) == null ? "" : request.getParameter(PARAM_SELECTED_SITA);
			flightId = request.getParameter(PARAM_FLIGHT_ID) == null ? "" : request.getParameter(PARAM_FLIGHT_ID);
			selectedTS = request.getParameter(PARAM_TS_SELECTED) == null ? "" : request.getParameter(PARAM_TS_SELECTED);
			selectedSITA = request.getParameter(PARAM_SITA) == null ? "" : request.getParameter(PARAM_SITA);
			messageType = request.getParameter(PARAM_MSG_TYPE) == null ? "" : request.getParameter(PARAM_MSG_TYPE);

			boolean allowManuallySendPnl = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_MANUALLY_SEND_PAL_CAL);
			SimpleDateFormat dateFormat = null;

			if (!strPALTimestamp.equals("")) {
				if (strPALTimestamp.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strPALTimestamp.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}

				timeStampPALDate = dateFormat.parse(strPALTimestamp);
			}

			if (!strCALTimestamp.equals("")) {
				if (strCALTimestamp.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strCALTimestamp.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}

				timeStampCALDate = dateFormat.parse(strCALTimestamp);
			}

			if (!selectedTS.equals("")) {
				if (selectedTS.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (selectedTS.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}

				timeStampSel = dateFormat.parse(selectedTS);
			}

			if (!strDateOfFlight.equals("")) {
				if (strDateOfFlight.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strDateOfFlight.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strDateOfFlight.indexOf(' ') != -1) {
					strDateOfFlight = strDateOfFlight.substring(0, strDateOfFlight.indexOf(' '));
				}

				dateOfFlight = dateFormat.parse(strDateOfFlight);
			}

			Date currentDate = new Date();
			Timestamp timeStampPAL = null;
			Timestamp timeStampCAL = null;
			Timestamp selectedTimeStamp = null;
			Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
			if (timeStampPALDate != null)
				timeStampPAL = new Timestamp(timeStampPALDate.getTime());
			if (timeStampCALDate != null)
				timeStampCAL = new Timestamp(timeStampCALDate.getTime());
			if (selectedTS != null && !selectedTS.equals("")) {
				selectedTimeStamp = new Timestamp(timeStampSel.getTime());
			}

			String arrSITAAddress[] = strSITAAddress.split(",");

			if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_NEWPAL) && allowManuallySendPnl) {

				ModuleServiceLocator.getReservationAuxilliaryBD().sendPALMessage(strFlightNo, strAirport, dateOfFlight,
						arrSITAAddress, PNLConstants.SendingMethod.MANUALLY);

				setIntSuccess(request, 1);

				saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_SENDNEW_PAL_SUCCESS, userLanguage),
						WebConstants.MSG_SUCCESS);
			} else if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_NEWCAL) && allowManuallySendPnl) {

				ModuleServiceLocator.getReservationAuxilliaryBD().sendCALAuxiliary(strFlightNo, strAirport, dateOfFlight,
						arrSITAAddress, PNLConstants.SendingMethod.MANUALLY);

				setIntSuccess(request, 2);

				saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_SENDNEW_CAL_SUCCESS, userLanguage),
						WebConstants.MSG_SUCCESS);
			} else if (!strHdnMode.equals("")
					&& (strHdnMode.equals(WebConstants.ACTION_RESENDPAL) || strHdnMode.equals(WebConstants.ACTION_RESENDCAL))) {

				ModuleServiceLocator.getReservationAuxilliaryBD().resendExistingManualPALCAL(Integer.parseInt(flightId),
						selectedTimeStamp, selectedSITA, strAirport, messageType, strPALCALStatus, arrSITAAddress, strMailServer,
						strFlightNo, dateOfFlight);

				if (messageType.equals("PAL")) {
					setIntSuccess(request, 3);
					saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_RESEND_PAL_SUCCESS, userLanguage),
							WebConstants.MSG_SUCCESS);
				} else {
					setIntSuccess(request, 4);
					saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_RESEND_CAL_SUCCESS, userLanguage),
							WebConstants.MSG_SUCCESS);
				}

			} else if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_PRINT)) {
				strPrintText = ModuleServiceLocator.getReservationAuxilliaryBD().getSavedPalCal(Integer.parseInt(flightId),
						selectedTimeStamp, selectedSITA, strAirport, messageType, strPALCALStatus);

				if (strPrintText != null) {

					setPALCALText(request, strPrintText);
				} else {
					setPALCALText(request, "");
				}
			} else if (!strHdnMode.equals("")
					&& (strHdnMode.equals(WebConstants.ACTION_NEWPAL) || strHdnMode.equals(WebConstants.ACTION_NEWCAL))
					&& !allowManuallySendPnl) {
				setExceptionOccured(request, true);
				setIntSuccess(request, 7);
				saveMessage(request, "Insufficient privilege to send PAL/CAL", WebConstants.MSG_ERROR);
			}

			setExceptionOccured(request, false);
			SimpleDateFormat dateFormataudit = new SimpleDateFormat("yyyy-MM-dd");

			if (!strHdnMode.equals("") && !strHdnMode.equals(WebConstants.ACTION_PRINT)) {
				LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
				contents.put("Action", strHdnMode);
				contents.put("flight id", flightId);
				contents.put("flight No", strFlightNo);
				contents.put("dep date", dateFormataudit.format(dateOfFlight));
				contents.put("Airport", strAirport);
				contents.put("Sita Address", strSITAAddress);
				audit(request, TasksUtil.SITA_TRANSFER_PAL_CAL, contents);
			}

			if (getPNLADLText(request) != null) {
				request.setAttribute(WebConstants.REQ_HTML_PNLADL_TEXT_DATA, getPNLADLText(request));
			}

		} catch (ModuleException moduleException) {
			log.error("SendPALCALRequestHandler.SendPALCAL() method is failed :" + moduleException.getMessageString(),
					moduleException);
			setExceptionOccured(request, true);
			setIntSuccess(request, 7);
			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strSelectedSita + "';";
			strFormData += "arrFormData[0][2] = '" + strMailServer + "';";

			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			strPrintText = "";

			setPALCALText(request, strPrintText);
			if (!strHdnMode.equals("") && !strHdnMode.equals(WebConstants.ACTION_PRINT)) {
				SimpleDateFormat dateFormataudit = new SimpleDateFormat("yyyy-MM-dd");
				LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
				contents.put("Action", strHdnMode);
				contents.put("flight id", flightId);
				contents.put("flight No", strFlightNo);
				contents.put("dep date", dateFormataudit.format(dateOfFlight));
				contents.put("Airport", strAirport);
				contents.put("Sita Address", strSITAAddress);
				contents.put("Status", moduleException.getMessageString());
				audit(request, TasksUtil.SITA_TRANSFER_PAL_CAL, contents);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			setIntSuccess(request, 7);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}

		return true;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {
			setSendPNLADLRowHtml(request);
		}
		setReqParams(request);
		setAirportList(request);
		if (request.getParameter(PARAM_AIRPORT_CODE) != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {
			setSITAHtml(request);

			setMailServerConfigurations(request);
		}
		setClientErrors(request);
		request.setAttribute(WebConstants.REQ_IS_EXCEPTION, "var isException='" + isExceptionOccured(request) + "';");
		request.setAttribute(WebConstants.REQ_HAS_MANUALLY_SEND_PAL,
				BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_MANUALLY_SEND_PAL_CAL));

		if (!isExceptionOccured(request)) {

			setPrintData(request);

			String strFormData = "var arrFormData = new Array();";

			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

	}

	private static void setPrintData(HttpServletRequest request) throws ModuleException {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String strPNLADLText = "";
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_PRINT)) {

			if (getPNLADLText(request) != null)
				strPNLADLText = getPNLADLText(request).replaceAll("\n", "<br>");
		}
		String regVar = " var pnlAdlText = '" + strPNLADLText + "';";

		request.setAttribute(WebConstants.REQ_HTML_PNLADL_TEXT_DATA, regVar);

	}

	private static void setSITAHtml(HttpServletRequest request) {
		try {
			String carrierCode = AppSysParamsUtil.extractCarrierCode(request.getParameter(PARAM_SEARCH_FLIGHTNO));
			String flightNumber = request.getParameter(PARAM_SEARCH_FLIGHTNO) == null ? "" : request.getParameter(PARAM_SEARCH_FLIGHTNO);
			String strDateOfFlight = request.getParameter(PARAM_SEARCH_FLIGHTDATE) == null ? "" : request.getParameter(PARAM_SEARCH_FLIGHTDATE);
			String strSITAList = JavaScriptGenerator.createActiveSITAHtml(request.getParameter(PARAM_AIRPORT_CODE), flightNumber, carrierCode , DCS_PAX_MSG_GROUP_TYPE.PAL_CAL, strDateOfFlight);
			request.setAttribute(WebConstants.REQ_HTML_SITA_DATA, strSITAList);
		} catch (ModuleException e) {
			log.error("SendPNLADLRequestHandler.setSITAHtml() method is failed :" + e.getMessageString(), e);
		} 

	}

	private static void setAirportList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createAirportsList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Set Mail Server Configurations
	 * 
	 * @param request
	 */
	private static void setMailServerConfigurations(HttpServletRequest request) {
		try {
			MailServerConfig defaultMailServerConfig = ModuleServiceLocator.getMessagingBD().getDefaultMailServer();
			Collection colMailServerConfig = ModuleServiceLocator.getMessagingBD().getMailServerConfigs(
					MessagingCustomConstants.MAIL_SERVER_CONFIGS_CODE_PAL_CAL_TRANSMISSION);

			if (defaultMailServerConfig == null) {
				String defaultMailServerJS = "var defaultMailServer = '';";
				request.setAttribute(WebConstants.REQ_HTML_DEFAULT_MAIL_SERVER, defaultMailServerJS);
			} else {
				String defaultMailServerJS = "var defaultMailServer = '" + defaultMailServerConfig.getHostAddress() + "';";
				request.setAttribute(WebConstants.REQ_HTML_DEFAULT_MAIL_SERVER, defaultMailServerJS);
				colMailServerConfig.add(defaultMailServerConfig);
			}

			String strHtml = SelectListGenerator.createMailServerList(colMailServerConfig);
			request.setAttribute(WebConstants.REQ_HTML_MAILSERVER_LIST, strHtml);
		} catch (ModuleException moduleException) {

			log.error("SendPNLADLRequestHandler.setMailServerList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = SendPALCALHG.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setReqParams(HttpServletRequest request) {
		String jsStr = " isDCSWS = false; ";
		if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
			jsStr = " isDCSWS = false; ";
		}
		request.setAttribute(WebConstants.REQ_PARAMS, jsStr);
	}

	private static void setSendPNLADLRowHtml(HttpServletRequest request) throws Exception {

		try {
			SendPALCALHG htmlGen = new SendPALCALHG();
			String strHtml = htmlGen.getSendPNLADLRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLHandler.setSendPNLADLRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

}
