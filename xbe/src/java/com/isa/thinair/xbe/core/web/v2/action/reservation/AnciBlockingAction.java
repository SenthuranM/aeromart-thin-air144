package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AncillaryUtil;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciBlockingAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(AnciBlockingAction.class);

	private boolean success = true;

	private String messageTxt;

	private String selectedSeats;

	private String paxWiseAnci;

	private String insurances;

	private String insurableFltRefNumbers;

	private boolean blockSeats;

	private String selectedFlightList;

	private String confExistingFlightSegs;

	private FlightSearchDTO searchParams;

	private String isOnd;

	private boolean isRequote = false;

	private String modifyingFltRefNumbers;

	private String resPaxInfo;

	private String ondwiseFlexiSelected;

	private Map<Integer, Boolean> ondSelectedFlexi = new HashMap<Integer, Boolean>();

	private String pnr;

	private PaymentTO payment;

	private boolean alertMedicalSsrForAllSegments = false;

	private boolean isCancellationOnly = false;

	private String groupPnr;

	private String paxState;

	private String paxCountryCode;

	private boolean paxTaxRegistered;

	@SuppressWarnings("unchecked")
	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			String strTxnIdntifier = resInfo.getTransactionId();
			SYSTEM system = resInfo.getSelectedSystem();

			if (blockSeats) {
				List<LCCSegmentSeatDTO> blockSeatDTOs = AncillaryJSONUtil.getBlockSeatDTOs(selectedSeats);

				if (pnr != null && !pnr.isEmpty()) {
					if (pnr != null && !pnr.isEmpty()) {

						boolean isGroupPnr = false; // AEROMART-2893

						if (getSearchParams().getSearchSystem().equals(SYSTEM.INT.name())
								&& !StringUtil.isNullOrEmpty(groupPnr)) {
							isGroupPnr = true;
						}
						blockSeatDTOs = ReservationUtil.filteredBlockSeats(blockSeatDTOs, pnr, isGroupPnr, request,
								getTrackInfo());
					}
				}

				try {
					success = ModuleServiceLocator.getAirproxyAncillaryBD().blockSeats(blockSeatDTOs, strTxnIdntifier, system,
							TrackInfoUtil.getBasicTrackInfo(request));
				} catch (ModuleException me) {
					log.error("Seat blocking failed", me);
					success = false;
				}
			}

			if (success) {

				List<FlightSegmentTO> flightSegmentTOs = null;

				if (isRequote) {
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}

				List<FlightSegmentTO> newAllFltSegmentTOs = new ArrayList<FlightSegmentTO>();
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					newAllFltSegmentTOs.add(flightSegmentTO);
				}

				if (confExistingFlightSegs != null) {
					if (isRequote) {
						newAllFltSegmentTOs
								.addAll(ReservationUtil.populateFlightSegmentList(confExistingFlightSegs, searchParams));
					} else {
						FltSegBuilder fltSegBuilder = new FltSegBuilder(confExistingFlightSegs, true, false, true);
						newAllFltSegmentTOs.addAll(fltSegBuilder.getSelectedFlightSegments());
					}
				}

				List<LCCInsuranceQuotationDTO> insuranceQuotations = AncillaryJSONUtil.getSelectedInsuranceQuotations(insurances,
						newAllFltSegmentTOs, insurableFltRefNumbers);

				if (insuranceQuotations != null && !insuranceQuotations.isEmpty()) {

					BigDecimal insuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					for (LCCInsuranceQuotationDTO insuranceQuotation : insuranceQuotations) {
						insuranceAmount = AccelAeroCalculator.add(insuranceAmount,
								insuranceQuotation.getQuotedTotalPremiumAmount());
					}
					/* adding insurance as external charges to the booking shopping cart */
					ExternalChgDTO insChgDTO = new ExternalChgDTO();
					insChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.INSURANCE);
					insChgDTO.setAmount(insuranceAmount);
					bookingShoppingCart.addSelectedExternalCharge(insChgDTO);
					bookingShoppingCart.setInsuranceQuotes(insuranceQuotations);
				} else {
					bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.INSURANCE);
					bookingShoppingCart.setInsuranceQuotes(null);
				}

				List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotations,
						ApplicationEngine.XBE, newAllFltSegmentTOs, isOnd, null);

				if (!isRequote) {
					SSRServicesUtil.checkMedicalSsrInAllSegments(paxList, getNonCancelledFltSegsWithoutBusSegs(flightSegmentTOs),
							ReservationInternalConstants.SsrTypes.MEDA);
				} else {

					LCCClientReservation reservation = null;

					if (StringUtil.isNullOrEmpty(groupPnr) && getSearchParams().getSearchSystem().equals(SYSTEM.INT.toString())) {
						reservation = ReservationUtil.loadFullLccReservation(pnr, false, null, bookingShoppingCart, request,
								false, null, null, null, false, getTrackInfo());
					} else {
						reservation = ReservationUtil.loadFullLccReservation(pnr,
								getSearchParams().getSearchSystem().equals(SYSTEM.AA.name()) ? false : true, null,
								bookingShoppingCart, request, false, null, null, null, false, getTrackInfo());
					}
					isCancellationOnly = isCancellationOnly(getNonCancelledFltSegsWithoutBusSegs(reservation.getSegments()),
							newAllFltSegmentTOs);

					if (!isCancellationOnly) {
						SSRServicesUtil.alertMedicalSsrForAllSegments(reservation,
								getNonCancelledFltSegsWithoutBusSegs(reservation.getSegments()), paxList,
								ReservationInternalConstants.SsrTypes.MEDA, newAllFltSegmentTOs);
					}

				}

				bookingShoppingCart.setPaxList(paxList);

				if (newAllFltSegmentTOs != null && !newAllFltSegmentTOs.isEmpty()) {
					bookingShoppingCart.getPriceInfoTO().getFareTypeTO().setOndFlexiSelection(ondSelectedFlexi);
					BookingUtil.applyFlexiCharges(paxList, bookingShoppingCart, newAllFltSegmentTOs);
				}

				if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
					if (!isRequote) {
						Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModSegAnciTotal = null;
						if (bookingShoppingCart.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI) && modifyingFltRefNumbers != null
								&& !"".equals(modifyingFltRefNumbers) && !ReservationInternalConstants.ReservationStatus.ON_HOLD
										.equals(resInfo.getReservationStatus())) {
							List<String> modFltList = JSONFETOParser.parseCollection(List.class, String.class,
									modifyingFltRefNumbers);
							if (paxWiseAnci != null && !"".equals(paxWiseAnci)) {
								Collection<LCCClientReservationPax> colPax = AncillaryUtil.extractLccReservationPax(resPaxInfo);
								paxWiseModSegAnciTotal = ExternalChargeUtil.getPaxWiseModifyingSegmentAnciTotal(flightSegmentTOs,
										modFltList, colPax);
							}
						}
						BookingUtil.applyAncillaryTax(bookingShoppingCart, flightSegmentTOs.get(0), paxWiseModSegAnciTotal);
					}
				}

				quotePaxServiceTaxAndApplyPaxWise(bookingShoppingCart, flightSegmentTOs, strTxnIdntifier);

				// Apply cc charge + jn when adminfee is enabled
				if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
						.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, RequestHandlerUtil.getAgentCode(request)))
						&& !isRequote) {

					BigDecimal totalPaymentAmountExcludingCCCharge = bookingShoppingCart.getTotalPayable(
							EXTERNAL_CHARGES.HANDLING_CHARGE, EXTERNAL_CHARGES.CREDIT_CARD, EXTERNAL_CHARGES.JN_ANCI);

					// handling fee calculation
					BigDecimal handlingCharge = bookingShoppingCart.getHandlingCharge();
					handlingCharge = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, handlingCharge);
					totalPaymentAmountExcludingCCCharge = AccelAeroCalculator.add(totalPaymentAmountExcludingCCCharge,
							handlingCharge);

					// cc fee calculation
					ExternalChgDTO externalCcChgDTO = bookingShoppingCart.getAllExternalChargesMap()
							.get(EXTERNAL_CHARGES.CREDIT_CARD);
					if (externalCcChgDTO.isRatioValueInPercentage()) {
						externalCcChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
					} else {
						externalCcChgDTO.calculateAmount(bookingShoppingCart.getPayablePaxCount(), flightSegmentTOs.size());
					}
					bookingShoppingCart.addSelectedExternalCharge(externalCcChgDTO);

					// calculate CC & handling charge JN tax
					ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

					// apply service tax
					applyServiceTaxForCCFee(bookingShoppingCart, externalCcChgDTO.getAmount(), resInfo.getTransactionId());
				}

			} else {
				messageTxt = XBEConfig.getServerMessage(WebConstants.ANCI_BLOCKING_FAILED, userLanguage, null);
			}
		} catch (ModuleException e) {
			log.error("Reservation Load Fails To Filter Already Block Seats.", e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setInsurableFltRefNumbers(String insurableFltRefNumbers) {
		this.insurableFltRefNumbers = insurableFltRefNumbers;
	}

	public void setInsurances(String insurances) {
		this.insurances = insurances;
	}

	public void setBlockSeats(boolean blockSeats) {
		this.blockSeats = blockSeats;
	}

	/**
	 * @param paxWiseAnci
	 *            the paxWiseAnci to set
	 */
	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setSelectedSeats(String selectedSeats) {
		this.selectedSeats = selectedSeats;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public void setModifyingFltRefNumbers(String modifyingFltRefNumbers) {
		this.modifyingFltRefNumbers = modifyingFltRefNumbers;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public String getOndwiseFlexiSelected() {
		return ondwiseFlexiSelected;
	}

	public void setOndwiseFlexiSelected(String ondwiseFlexiSelected) {
		this.ondwiseFlexiSelected = ondwiseFlexiSelected;
		if (this.ondwiseFlexiSelected != null && !"".equals(this.ondwiseFlexiSelected)) {
			setOndSelectedFlexi(
					WebplatformUtil.convertMap(JSONFETOParser.parseMap(HashMap.class, String.class, this.ondwiseFlexiSelected)));
		}
	}

	public void setOndSelectedFlexi(Map<Integer, Boolean> ondSelectedFlexi) {
		this.ondSelectedFlexi = ondSelectedFlexi;
	}

	@JSON(serialize = false)
	public Map<Integer, Boolean> getOndSelectedFlexi() {
		return ondSelectedFlexi;
	}

	public void setConfExistingFlightSegs(String confExistingFlightSegs) {
		this.confExistingFlightSegs = confExistingFlightSegs;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	/*
	 * returns actual flight segments (without bus segments)
	 */
	private List<FlightSegmentTO> getNonCancelledFltSegsWithoutBusSegs(List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		List<FlightSegmentTO> actualFlightSegmentList = new ArrayList<FlightSegmentTO>();
		for (Iterator<FlightSegmentTO> flightSegmentListIterator = flightSegmentTOs.iterator(); flightSegmentListIterator
				.hasNext();) {
			FlightSegmentTO flightSegment = flightSegmentListIterator.next();
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegment.getSegmentCode())) {
				if (flightSegment.getFlightSegmentStatus() != null) {
					if (!flightSegment.getFlightSegmentStatus().equals("CNX")) {
						actualFlightSegmentList.add(flightSegment);
					}
				} else {
					actualFlightSegmentList.add(flightSegment);
				}
			}
		}
		return actualFlightSegmentList;
	}

	/*
	 * returns actual flight segments (without bus segments)
	 */
	private Set<LCCClientReservationSegment>
			getNonCancelledFltSegsWithoutBusSegs(Set<LCCClientReservationSegment> flightSegmentTOs) throws ModuleException {
		Set<LCCClientReservationSegment> actualFlightSegmentList = new HashSet<LCCClientReservationSegment>();
		for (Iterator<LCCClientReservationSegment> flightSegmentListIterator = flightSegmentTOs
				.iterator(); flightSegmentListIterator.hasNext();) {
			LCCClientReservationSegment flightSegment = flightSegmentListIterator.next();
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegment.getSegmentCode())) {
				if (flightSegment.getStatus() != null) {
					if (!flightSegment.getStatus().equals("CNX")) {
						actualFlightSegmentList.add(flightSegment);
					}
				} else {
					actualFlightSegmentList.add(flightSegment);
				}
			}
		}
		return actualFlightSegmentList;
	}

	private boolean isCancellationOnly(Set<LCCClientReservationSegment> lccSegments, List<FlightSegmentTO> newAllFltSegmentTOs) {
		boolean isCancellationOnly = false;

		List<String> reservationSegRPHList = getSegRPHList(lccSegments);
		List<String> newAllFltSegRPHList = getSegRPHList(newAllFltSegmentTOs);

		// check newAllFltSegRPHList is exact subset of reservationSegRPHList
		boolean isNewAllFltSubset = reservationSegRPHList.containsAll(newAllFltSegRPHList);

		if (isNewAllFltSubset) {
			isCancellationOnly = true;
		}

		return isCancellationOnly;
	}

	private List<String> getSegRPHList(Set<LCCClientReservationSegment> lccSegments) {
		List<String> segRPHList = new ArrayList<String>();

		for (Iterator<LCCClientReservationSegment> segIterator = lccSegments.iterator(); segIterator.hasNext();) {
			LCCClientReservationSegment segment = segIterator.next();
			segRPHList.add(segment.getFlightSegmentRefNumber());
		}

		return segRPHList;

	}

	private List<String> getSegRPHList(List<FlightSegmentTO> newAllFltSegmentTOs) {
		List<String> segRPHList = new ArrayList<String>();

		for (Iterator<FlightSegmentTO> segIterator = newAllFltSegmentTOs.iterator(); segIterator.hasNext();) {
			FlightSegmentTO segment = segIterator.next();
			segRPHList.add(segment.getFlightRefNumber());
		}

		return segRPHList;

	}

	public String getGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	private void quotePaxServiceTaxAndApplyPaxWise(BookingShoppingCart bookingShoppingCart,
			List<FlightSegmentTO> flightSegmentTOs, String txnIdntifier) throws ParseException, ModuleException {
		if (bookingShoppingCart.getPriceInfoTO() != null && !isRequote) {
			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);

			ExternalChgDTO handlingChargeDTO = bookingShoppingCart.getSelectedExternalCharges()
					.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
			BigDecimal handlingFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (handlingChargeDTO != null) {
				handlingFeeAmount = bookingShoppingCart.getHandlingCharge();
			}

			BigDecimal[] paxWiseHandlingFee = AccelAeroCalculator.roundAndSplit(handlingFeeAmount,
					bookingShoppingCart.getPaxList().size());

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
			serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(flightAvailRQ);
			serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
			serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
			serviceTaxQuoteCriteriaDTO.setPaxState(paxState);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(paxCountryCode);
			serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(paxTaxRegistered);

			ServiceTaxCalculatorUtil.addPaxExternalChargesToServiceTaxQuoteRQ(serviceTaxQuoteCriteriaDTO,
					bookingShoppingCart.getPaxList(), flightSegmentTOs, null, null, handlingChargeDTO, paxWiseHandlingFee, true);

			serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(txnIdntifier);

			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
					.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfo());
			ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(bookingShoppingCart.getPaxList(), serviceTaxQuoteRS, false,
					ApplicationEngine.XBE, false);

		}

	}

	private BigDecimal applyServiceTaxForCCFee(BookingShoppingCart bookingShoppingCart, BigDecimal ccChargeAmount,
			String strTxnIdntifier) throws ParseException, ModuleException, org.json.simple.parser.ParseException {

		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (bookingShoppingCart != null && ccChargeAmount != null && searchParams != null && selectedFlightList != null) {
			ExternalChgDTO creditCardChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD);

			BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
			AvailPreferencesTO availPref = baseAvailRQ.getAvailPreferences();
			availPref.setAppIndicator(ApplicationEngine.XBE);
			if (ReservationUtil.isGroupPnr(pnr)
					|| (searchParams != null && !SYSTEM.AA.toString().equals(searchParams.getSearchSystem()))) {
				availPref.setSearchSystem(SYSTEM.INT);
			} else {
				availPref.setSearchSystem(SYSTEM.AA);
			}

			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList,
					searchParams);

			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
			ServiceTaxCalculatorUtil.populatePaxWiseCreditCardCharge(bookingShoppingCart.getPaxList(), creditCardChgDTO,
					ccChargeAmount, paxWiseExternalCharges, paxWisePaxTypes, flightSegmentTOs, null, null);

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
			serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
			serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(null);
			serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
			serviceTaxQuoteCriteriaDTO.setPaxState(paxState);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(paxCountryCode);
			serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(paxTaxRegistered);
			serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
			serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
			serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(strTxnIdntifier);

			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
					.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfo());
			totalServiceTaxAmount = ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(bookingShoppingCart.getPaxList(),
					serviceTaxQuoteRS, true, ApplicationEngine.XBE, false);
		}
		return totalServiceTaxAmount;

	}

}