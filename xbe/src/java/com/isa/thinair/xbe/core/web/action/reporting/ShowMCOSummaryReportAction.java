package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.MCOSummaryReportRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.MCO_SUMMARY_REPORT)
public class ShowMCOSummaryReportAction extends BaseRequestResponseAwareAction {

	public String execute() {
		return MCOSummaryReportRH.execute(request, response);
	}
}
