package com.isa.thinair.xbe.core.web.handler.travagent;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.travagent.PostPaymentHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class PostPaymentsRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(PostPaymentsRequestHandler.class);

	private static XBEConfig xbeConfig = new XBEConfig();

	private static final String PARAM_DATE = "txtDStart";
	private static final String PARAM_AMOUNT = "txtAmount";
	private static final String PARAM_COLPURP = "selColP";
	private static final String PARAM_COLTYPE = "selColT";
	private static final String PARAM_CHQNO = "txtChqNo";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_AGENTCODE = "AgentCode";
	private static final String PARAM_CREDITLIMIT = "CreditLimit";
	private static final String PARAM_AVAILABLECREDIT = "AvailableCredit";
	private static final String PARAM_FORCECANCEL = "ForceCancel";

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	private static String isSuccessfulSaveMessageDisplayEnabledJS = "isSuccessfulSaveMessageDisplayEnabled = "
			+ isSuccessfulSaveMessageDisplayEnabled() + ";";

	public static String execute(HttpServletRequest request) {

		String forward = S2Constants.Result.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (strHdnMode != null) {

			boolean isSaveTransactionSuccessful = false;
			String saveSuccess = "isSaveTransactionSuccessful = false;";

			try {
				if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
					checkPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_PAYMENT);
					isSaveTransactionSuccessful = true;
					saveData(request);
					saveMessage(request, xbeConfig.getMessage("cc.airadmin.add.success", userLanguage), WebConstants.MSG_SUCCESS);
					log.debug("\nPostPaymentsRequestHandler SAVEDATA() SUCCESS");
				} else {
					log.debug("\nPostPaymentsRequestHandler SAVEDATA() NOT ENTERED");
				}
			} catch (ModuleException e) {

				saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
				log.error("\nPostPaymentsRequestHandler SAVEDATA() FAILED IN TRY CATCH BLOCK:" + e.getExceptionCode());
				isSaveTransactionSuccessful = false;
			}

			try {
				setDisplayData(request);
				log.debug("PostPaymentsRequestHandler SETDISPLAYDATA() SUCCESS");
			} catch (Exception e) {
				log.error("PostPaymentsRequestHandler SETDISPLAYDATA() FAILED " + e.getMessage());
			}

			if (isSaveTransactionSuccessful) {
				saveSuccess = "isSaveTransactionSuccessful = true;";
			} else {
				saveSuccess = "isSaveTransactionSuccessful = false;";
			}

			request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, isSuccessfulSaveMessageDisplayEnabledJS);
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, saveSuccess);
		} else {
			// do nothing will show the empty page
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {

		if (!request.getParameter("hdnAgentCode").equals("") && request.getParameter("hdnAgentCode") != null) {
			setInvoiceSettlementData(request);

			LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
			contents.put("agentCode", request.getParameter("hdnAgentCode"));
			audit(request, TasksUtil.TA_VIEW_TRAVEL_AGENT_RECIPTS, contents);
		}
		setClientErrors(request);
	}

	private static void saveData(HttpServletRequest request) throws ModuleException {
		String strAgentCode = StringUtil.getNotNullString(request.getParameter(PARAM_AGENTCODE));
		String strAmount = StringUtil.getNotNullString(request.getParameter(PARAM_AMOUNT));
		String strChqNo = StringUtil.getNotNullString(request.getParameter(PARAM_CHQNO));
		String strColPurp = StringUtil.getNotNullString(request.getParameter(PARAM_COLPURP));
		String strColType = StringUtil.getNotNullString(request.getParameter(PARAM_COLTYPE));
		String strDate = StringUtil.getNotNullString(request.getParameter(PARAM_DATE));
		String strRemarks = StringUtil.getNotNullString(request.getParameter(PARAM_REMARKS));
		String strCreditLimit = StringUtil.getNotNullString(request.getParameter(PARAM_CREDITLIMIT));
		String strAvailableCredit = StringUtil.getNotNullString(request.getParameter(PARAM_AVAILABLECREDIT));
		String strForceCancel = StringUtil.getNotNullString(request.getParameter(PARAM_FORCECANCEL));

		Date date = null;
		try {
			if (strDate != null) {
				date = CalendarUtil.getParsedTime(strDate, DATE_FORMAT_DDMMYYYY);
			}
		} catch (ParseException e) {
			log.error("saveData() Invalid Date - Parse Exception" + e.getMessage());
		}

		boolean strBoo = false;
		if (strForceCancel.equals("true")) {
			strBoo = true;
		}

		AgentCollection agentCol = new AgentCollection();
		agentCol.setAgentCode(strAgentCode);
		agentCol.setChequeNumber(strChqNo);
		agentCol.setDateOfPayment(date);
		agentCol.setRemarks(strRemarks);
		agentCol.setPaymentAmount(AccelAeroCalculator.getTwoScaledBigDecimalFromString(strAmount));
		agentCol.setCpCode(strColPurp);
		agentCol.setCmCode(strColType);

		ModuleServiceLocator.getTravelAgentFinanceBD().postAgentCollection(strAgentCode, AccelAeroCalculator.getTwoScaledBigDecimalFromString(strCreditLimit),
				AccelAeroCalculator.getTwoScaledBigDecimalFromString(strAvailableCredit), agentCol, strBoo);
		agentCol = null;
	}

	private static void setInvoiceSettlementData(HttpServletRequest request) throws ModuleException {
		PostPaymentHTMLGenerator htmlGen = new PostPaymentHTMLGenerator();
		String strHtml = htmlGen.getInvoiceDetailsRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_INVOICE_ARRAY, strHtml);
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = PostPaymentHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (xbeConfig.getMessage("cc.airadmin.savemessage.display", null).equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}
}