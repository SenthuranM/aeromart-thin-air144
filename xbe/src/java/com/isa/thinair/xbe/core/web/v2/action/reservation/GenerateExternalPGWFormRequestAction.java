package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.googlecode.jsonplugin.JSONResult;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.ReservationPostPaymentDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class GenerateExternalPGWFormRequestAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(GenerateExternalPGWFormRequestAction.class);

	private String pgwId;

	private String selectedCurrency;

	private String pgwBrokerType;

	private boolean success = true;

	private String messageTxt;

	private String amount;

	private ContactInfoTO contactInfo;

	private String redirectionString;

	private String redirectionMechanism;
	
	private String pnr;

	private static final String RETURN_URL = AppSysParamsUtil.getAirlineReservationURL()
			+ "/xbe/private/handleExternalPGWResponse.action";
	public static final String POST_INPUT_DATA_FORM_HTML = "postInputDataFormHtml";
	private static final String REDIRECTION_MECHANISM = "REDIRECTION_MECHANISM";
	private static final String BROKER_TYPE = "BROKER_TYPE";

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO;
		try {
			ipgIdentificationParamsDTO = WebplatformUtil.prepareIPGConfigurationParamsDTOPerPGWId(new Integer(pgwId));
			TrackInfoDTO trackInfoDTO = this.getTrackInfo();
			ReservationContactInfo contactInfoTO = getContactInfoTO();
			Collection<PaymentInfo> colPaymentInfo = getPaymentInfo();
			Integer tempPayId = null;

			Collection<Integer> colTnxIds = ModuleServiceLocator.getReservationBD().makeTemporyPaymentEntry(null, contactInfoTO,
					colPaymentInfo, trackInfoDTO, true, false);

			IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
			ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
			ipgRequestDTO.setAmount(getPayAmount(ipgIdentificationParamsDTO).toString());
			ipgRequestDTO.setReturnUrl(RETURN_URL);
			ipgRequestDTO.setApplicationIndicator(AppIndicatorEnum.APP_XBE);
			ipgRequestDTO.setSessionID(request.getSession().getId());
			if (colTnxIds != null && colTnxIds.size() > 0) {
				tempPayId = colTnxIds.iterator().next();
				ipgRequestDTO.setApplicationTransactionId(colTnxIds.iterator().next());
			}

			IPGRequestResultsDTO ipgRequestResultsDTO = ModuleServiceLocator.getPaymentBrokerBD().getRequestData(ipgRequestDTO);
			this.redirectionString = ipgRequestResultsDTO.getRequestData();
			this.redirectionMechanism = (String) ipgRequestResultsDTO.getPostDataMap().get(REDIRECTION_MECHANISM);

			ReservationPostPaymentDTO postPayData = new ReservationPostPaymentDTO();
			postPayData.setPaymentBrokerRefNo(ipgRequestResultsDTO.getPaymentBrokerRefNo());
			postPayData.setIpgId(new Integer(pgwId));
			postPayData.setCurrencyCode(ipgIdentificationParamsDTO.getPaymentCurrencyCode());

			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			ipgResponseDTO.setBrokerType((String) ipgRequestResultsDTO.getPostDataMap().get(BROKER_TYPE));
			ipgResponseDTO.setTemporyPaymentId(tempPayId);
			ipgResponseDTO.setPaymentBrokerRefNo(ipgRequestResultsDTO.getPaymentBrokerRefNo());
			postPayData.setIpgResponseDTO(ipgResponseDTO);
			request.getSession().setAttribute(S2Constants.Session_Data.POST_PAY_DATA, postPayData);
			return S2Constants.Result.SUCCESS;
		} catch (NumberFormatException | ModuleException e) {
			log.error("Error while retrieving payment gateway type : " + e);
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			success = false;
			return S2Constants.Result.ERROR;
		} catch (Exception e) {
			log.error("Error while retrieving payment gateway type : " + e);
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			success = false;
			return S2Constants.Result.ERROR;
		}
	}

	private BigDecimal getPayAmount(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		CurrencyExchangeRate payCurrencyExchangeRate = exchangeRateProxy
				.getCurrencyExchangeRate(ipgIdentificationParamsDTO.getPaymentCurrencyCode());
		Currency payCurrency = payCurrencyExchangeRate.getCurrency();

		CurrencyExchangeRate selectedCurrencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);
		Currency selectedCurrency = selectedCurrencyExchangeRate.getCurrency();

		BigDecimal baseCurrencyAmount = AccelAeroRounderPolicy.convertAndRound(new BigDecimal(amount),
				selectedCurrencyExchangeRate.getExrateBaseToCurNumber(), selectedCurrency.getBoundryValue(),
				selectedCurrency.getBreakPoint());

		BigDecimal payCurrencyAmount = AccelAeroRounderPolicy.convertAndRound(baseCurrencyAmount,
				payCurrencyExchangeRate.getMultiplyingExchangeRate(), payCurrency.getBoundryValue(), payCurrency.getBreakPoint());

		return payCurrencyAmount;

	}

	private ReservationContactInfo getContactInfoTO() throws Exception {
		
		ReservationContactInfo contactInfoTO = new ReservationContactInfo();
		if (contactInfo != null) {
			contactInfoTO.setFirstName(contactInfo.getFirstName());
			contactInfoTO.setLastName(contactInfo.getLastName());
			contactInfoTO.setEmail(contactInfo.getEmail());
			contactInfoTO.setPhoneNo(contactInfo.getPhoneNo());
			contactInfoTO.setMobileNo(contactInfo.getMobileNo());
		} else {
			String resContactInfo = request.getParameter("resContactInfo");
			CommonReservationContactInfo resContactInfoTO = ReservationUtil.transformJsonContactInfo(resContactInfo);
			contactInfoTO.setFirstName(resContactInfoTO.getFirstName());
			contactInfoTO.setLastName(resContactInfoTO.getLastName());
			contactInfoTO.setEmail(resContactInfoTO.getEmail());
			contactInfoTO.setPhoneNo(resContactInfoTO.getPhoneNo());
			contactInfoTO.setMobileNo(resContactInfoTO.getMobileNo());
		}
		return contactInfoTO;
	}

	private PayCurrencyDTO getPayCurrencyDTO() throws ModuleException {
		PayCurrencyDTO payCurrencyDTO = null;
		Currency currency = null;
		String baseCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency, ApplicationEngine.XBE);

		if (exchangeRate != null) {
			currency = ModuleServiceLocator.getCommonServiceBD().getCurrency(selectedCurrency);
			if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultIbePGId() != null) {
				payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, exchangeRate.getMultiplyingExchangeRate(),
						currency.getBoundryValue(), currency.getBreakPoint());
			}
		}

		if (payCurrencyDTO == null) {
			if (log.isDebugEnabled()) {
				log.debug("Interline paymentgateway checking for selected currency does not have pg getting the default ");
			}
			currency = ModuleServiceLocator.getCommonServiceBD().getCurrency(baseCurrencyCode);
			payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, new BigDecimal(1), currency.getBoundryValue(),
					currency.getBreakPoint());
		}
		payCurrencyDTO.setTotalPayCurrencyAmount(new BigDecimal(amount));
		return payCurrencyDTO;
	}

	private Collection<PaymentInfo> getPaymentInfo() throws ModuleException {
		PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO();
		Collection<PaymentInfo> colPaymentInfo = new ArrayList<PaymentInfo>();
		CardPaymentInfo cardPaymentInfo = new CardPaymentInfo();
		cardPaymentInfo.setTotalAmount(new BigDecimal(this.amount));
		cardPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		cardPaymentInfo.setPnr(this.pnr);
		colPaymentInfo.add(cardPaymentInfo);
		return colPaymentInfo;
	}

	public String getPgwId() {
		return pgwId;
	}

	public void setPgwId(String pgwId) {
		this.pgwId = pgwId;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getPgwBrokerType() {
		return pgwBrokerType;
	}

	public void setPgwBrokerType(String pgwBrokerType) {
		this.pgwBrokerType = pgwBrokerType;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getRedirectionString() {
		return redirectionString;
	}

	public void setRedirectionString(String redirectionString) {
		this.redirectionString = redirectionString;
	}

	public String getRedirectionMechanism() {
		return redirectionMechanism;
	}

	public void setRedirectionMechanism(String redirectionMechanism) {
		this.redirectionMechanism = redirectionMechanism;
	}

	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
