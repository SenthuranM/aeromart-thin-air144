package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightAncillaryDetailsDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class FlightAncillaryDetailsReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(FlightAncillaryDetailsReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("FlightAncillaryDetailsReportRH setReportView Success");
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("FlightAncillaryRequestHandler setReportView Failed " + e.getMessageString());
			forward = S2Constants.Result.ERROR;
		}

		return forward;
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		boolean isMealSelected = false;
		boolean isSeatSelected = false;
		boolean isBaggageSelected = false;
		boolean isInsuranceSeleted = false;
		boolean isFlexiSelected = false;
		boolean isHalaSelected = false;
		boolean isBusSelected = false;
		String flightDate = request.getParameter("hdnFltDate");
		String flightNo = request.getParameter("hdnFlightNo");
		String value = request.getParameter("hdnView");
		isMealSelected = Boolean.parseBoolean(request.getParameter("chkMealSelected"));
		isSeatSelected = Boolean.parseBoolean(request.getParameter("chkSeatSelected"));
		isBaggageSelected = Boolean.parseBoolean(request.getParameter("chkBaggageSelected"));
		isInsuranceSeleted = Boolean.parseBoolean(request.getParameter("chkInsuranceSelected"));
		isFlexiSelected = Boolean.parseBoolean(request.getParameter("chkFlexiSelected"));
		isHalaSelected = Boolean.parseBoolean(request.getParameter("chkHalaSelected"));
		isBusSelected = Boolean.parseBoolean(request.getParameter("chkBusSelected"));

		String id = "UC_REPM_090";
		String templateFlightAncillaryDetail = "FlightAncillaryDetails.jasper";

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (flightDate != null && !"".equals(flightDate.trim())) {
				search.setDepTimeLocal(ReportsHTMLGenerator.convertDate(flightDate));
			}
			if (flightNo != null && !"".equals(flightNo.trim())) {
				search.setFlightNumber(flightNo.toUpperCase());
			}
			search.setMealSelected(isMealSelected);
			search.setSeatSelected(isSeatSelected);
			search.setBaggageSeleted(isBaggageSelected);
			search.setInsuranceSelected(isInsuranceSeleted);
			search.setFlexiSelected(isFlexiSelected);
			search.setHalaSerivceSelected(isHalaSelected);
			search.setBusSelected(isBusSelected);

			List<FlightAncillaryDetailsDTO> reportItems = new ArrayList<FlightAncillaryDetailsDTO>();

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(templateFlightAncillaryDetail));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getFlightAncillaryDetailsReport(search);
			reportItems = createFlightAncillaryDetailDTOList(resultSet,search);

			parameters.put("DEPARTURE_DATE", flightDate);
			parameters.put("FLIGHT_NO", flightNo.toUpperCase());
			parameters.put("ID", id);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (value.trim().equals("Html")) {
				response.reset();
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, reportItems,
						null, reportsRootDir, response);
			} else if (value.trim().equals("Pdf")) {
				response.reset();
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, reportItems,
						response);
			} else if (value.trim().equals("Excel")) {
				response.reset();
				response.setHeader("Content-Disposition", "attachment;filename=FlightAncillaryDetailsReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, reportItems,
						response);
			} else if (value.trim().equals("Csv")) {
				response.reset();
				response.setHeader("Content-Disposition", "attachment;filename=FlightAncillaryDetailsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, reportItems,
						response);
			}

		} catch (Exception e) {
			log.error(e.getStackTrace());
			log.error("setReportView() method is failed:");
		}
	}

	private static List<FlightAncillaryDetailsDTO> createFlightAncillaryDetailDTOList(
			ResultSet flightAncillaryDetailsSet,ReportsSearchCriteria search) {
		List<FlightAncillaryDetailsDTO> flightAncillaryDetailsDTOs = new ArrayList<FlightAncillaryDetailsDTO>();

		try {
			while (flightAncillaryDetailsSet.next()) {
				FlightAncillaryDetailsDTO flightAncillaryDetailsDTO = new FlightAncillaryDetailsDTO();

				flightAncillaryDetailsDTO.setPnr(flightAncillaryDetailsSet.getString("pnr"));
				flightAncillaryDetailsDTO.setPaxName(flightAncillaryDetailsSet.getString("paxName"));
				flightAncillaryDetailsDTO.setContactPerson(flightAncillaryDetailsSet.getString("contactPerson"));
				flightAncillaryDetailsDTO.setMobileNumber(BeanUtils.nullHandler(flightAncillaryDetailsSet.getString("mobileNumber")));
				flightAncillaryDetailsDTO.setPhoneNumber(BeanUtils.nullHandler(flightAncillaryDetailsSet.getString("phoneNumber")));
				flightAncillaryDetailsDTO.seteMail(BeanUtils.nullHandler(flightAncillaryDetailsSet.getString("eMail")));
				flightAncillaryDetailsDTO.setSegmentCode(flightAncillaryDetailsSet.getString("segmentCode"));
				flightAncillaryDetailsDTO.setDepartureTimeLocal(flightAncillaryDetailsSet.getString("departureTimeLocal"));
				if(search.isInsuranceSelected()) {
					flightAncillaryDetailsDTO
					.setInsuranceSelected(flightAncillaryDetailsSet.getString("insuranceSelected"));
				}
				if(search.isMealSelected()) {
					flightAncillaryDetailsDTO.setMealSelected(flightAncillaryDetailsSet.getString("mealSelected"));
				}
				if(search.isSeatSelected()) {
					flightAncillaryDetailsDTO.setSeatSelected(flightAncillaryDetailsSet.getString("seatSelected"));
				}
				if(search.isBaggageSeleted()) {
					flightAncillaryDetailsDTO.setBaggageSelected(flightAncillaryDetailsSet.getString("baggageSelected"));
				}
				if(search.isHalaSerivceSelected()) {
					flightAncillaryDetailsDTO.setSsrSelected(flightAncillaryDetailsSet.getString("halaSelected"));
				}
				if(search.isFlexiSelected()) {
					flightAncillaryDetailsDTO.setFlexiSelected(flightAncillaryDetailsSet.getString("flexiSelected"));
				}
				if(search.isBusSelected()) {
					flightAncillaryDetailsDTO.setBusSelected(flightAncillaryDetailsSet.getString("busSelected"));
				}
				
				flightAncillaryDetailsDTOs.add(flightAncillaryDetailsDTO);
			}
		} catch (SQLException sqlEx) {
			log.error("Error occurred while iterating the flight Ancillary details list :" + sqlEx);
		}

		return flightAncillaryDetailsDTOs;
	}

}
