package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;

public class PaymentHolder {
	private Map<String, RawPayment> paymentMap = new HashMap<String, RawPayment>();
	private List<String> keyOrder = new ArrayList<String>();

	private Map<String, BigDecimal> consumedRef = new HashMap<String, BigDecimal>();
	private Map<String, BigDecimal> consumedExtChargesRef = new HashMap<String, BigDecimal>();
	private ArrayList<VoucherDTO> voucherDTOList = new ArrayList<VoucherDTO>();
	private Queue<String> consumeOrder = new LinkedList<String>();
	private boolean partialPaxPaymentAllowed = false;

	public void consumePayment(BigDecimal excluingExtChargesAmount, BigDecimal includingExtChargesAmount) {
		consumedRef = new HashMap<String, BigDecimal>();
		consumeOrder = new LinkedList<String>();
		for (String key : keyOrder) {
			RawPayment rPay = paymentMap.get(key);
			ConsumedData consumedData = rPay.consume(excluingExtChargesAmount, includingExtChargesAmount, isZeroPaymentAllowed());
			if (consumedData.isConsumed()) {
				BigDecimal consumedExtCharge = AccelAeroCalculator.add(includingExtChargesAmount,
						excluingExtChargesAmount.negate());
				excluingExtChargesAmount = AccelAeroCalculator.add(excluingExtChargesAmount, consumedData.getAmount().negate());
				includingExtChargesAmount = excluingExtChargesAmount;

				consumedRef.put(rPay.getKey(), consumedData.getAmount());
				consumeOrder.add(key);
				if (consumedExtCharge.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
					consumedExtChargesRef.put(key, consumedExtCharge);
				}
				// sortConsumerByPaymentAsc();
			}
		}

	}

	private boolean isZeroPaymentAllowed() {
		return !isPartialPaxPaymentAllowed() && keyOrder.size() == 1;
	}

	public void sortConsumerByPaymentAsc() {
		if (consumeOrder.size() > 1) {
			List<String> list = new ArrayList<String>();
			String prevkeyWithExternalCharges = null;
			while (!consumeOrder.isEmpty()) {
				String key = consumeOrder.poll();
				if (prevkeyWithExternalCharges == null) {
					prevkeyWithExternalCharges = key;
				}
				list.add(key);
			}
			Collections.sort(list, new Comparator<String>() {

				@Override
				public int compare(String first, String second) {
					BigDecimal amountFirst = consumedRef.get(first);
					if (consumedExtChargesRef.containsKey(first)) {
						amountFirst = AccelAeroCalculator.add(amountFirst, consumedExtChargesRef.get(first));
					}
					BigDecimal amountSecond = consumedRef.get(second);
					if (consumedExtChargesRef.containsKey(second)) {
						amountSecond = AccelAeroCalculator.add(amountSecond, consumedExtChargesRef.get(second));
					}
					return amountFirst.compareTo(amountSecond);
				}

			});
			for (String key : list) {
				if (prevkeyWithExternalCharges != null && prevkeyWithExternalCharges.equals(key)
						&& consumedExtChargesRef.containsKey(prevkeyWithExternalCharges)) {
					consumedRef.put(key,
							AccelAeroCalculator.add(consumedRef.get(key), consumedExtChargesRef.get(prevkeyWithExternalCharges)));
				}
				consumeOrder.add(key);
			}
			String newKeyWithExternalCharges = consumeOrder.peek();
			if (newKeyWithExternalCharges != null && prevkeyWithExternalCharges != null
					&& consumedExtChargesRef.containsKey(prevkeyWithExternalCharges)) {
				consumedRef.put(newKeyWithExternalCharges, AccelAeroCalculator.add(consumedRef.get(newKeyWithExternalCharges),
						consumedExtChargesRef.get(prevkeyWithExternalCharges).negate()));

			}
		}
	}

	public BigDecimal getTotalPaymentAmount() {
		BigDecimal totalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (String key : paymentMap.keySet()) {
			RawPayment rPay = paymentMap.get(key);
			totalPayment = AccelAeroCalculator.add(totalPayment, rPay.getTotalPaymentAmount());
		}
		return totalPayment;
	}

	public BigDecimal getConsumedPaymentAmount(String ref) {
		return consumedRef.get(ref);
	}

	public String getNextConsumedPaymentRef() {
		if (consumeOrder.size() > 0) {
			return consumeOrder.poll();
		}
		return null;
	}

	public boolean hasNonRecordedConsumedPayments() {
		return (consumeOrder.size() > 0);
	}

	public PaymentTO getPayment(String reference) {
		if (keyOrder.contains(reference)) {
			return paymentMap.get(reference).getPaymentTO();
		}
		return null;
	}

	public VoucherDTO getVoucherPayment(String reference) {
		if (keyOrder.contains(reference)) {
			return paymentMap.get(reference).getVoucherDTO();
		}
		return null;
	}

	public CreditCardTO getCreditCardTO(String reference) {
		if (keyOrder.contains(reference)) {
			return paymentMap.get(reference).getCreditCardTO();
		}
		return null;
	}

	public void addCardPayment(PaymentTO paymentTO, CreditCardTO creditCardTO) {
		RawPayment rawPayment = new RawPayment(paymentTO, creditCardTO);
		addPayment(rawPayment);
	}

	public void addPayment(PaymentTO paymentTO) {
		RawPayment rawPayment = new RawPayment(paymentTO);
		addPayment(rawPayment);
	}

	public void addVoucherPayment(ArrayList<VoucherDTO> voucherDTOList) {
		this.voucherDTOList = voucherDTOList;
		for (VoucherDTO voucherDTO : voucherDTOList) {
			RawPayment rawPayment = new RawPayment(voucherDTO);
			addPayment(rawPayment);
		}
	}

	private void addPayment(RawPayment payment) {
		paymentMap.put(payment.getKey(), payment);
		keyOrder.add(payment.getKey());
	}

	public boolean isPartialPaxPaymentAllowed() {
		return partialPaxPaymentAllowed;
	}

	public void setPartialPaxPaymentAllowed(boolean partialPaxPaymentAllowed) {
		this.partialPaxPaymentAllowed = partialPaxPaymentAllowed;
	}

	public ArrayList<VoucherDTO> getVoucherDTOList() {
		return voucherDTOList;
	}

	public void setVoucherDTOList(ArrayList<VoucherDTO> voucherDTOList) {
		this.voucherDTOList = voucherDTOList;
	}
	
	public PaymentTO getDummyPaymentMethod() {
		if (paymentMap != null && !paymentMap.isEmpty()) {
			for (String key : paymentMap.keySet()) {
				RawPayment payment = paymentMap.get(key);
				if (payment.getPaymentTO() != null) {
					return payment.getPaymentTO();
				}
			}
		}
		return null;
	}

	public CreditCardTO getDummyCreditCardTO() {
		if (paymentMap != null && !paymentMap.isEmpty()) {
			for (String key : paymentMap.keySet()) {
				RawPayment payment = paymentMap.get(key);
				if (payment.getCreditCardTO() != null) {
					return payment.getCreditCardTO();
				}
			}
		}
		return null;
	}
}

class ConsumedData {
	boolean consumed;
	BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	void setAmount(BigDecimal amount) {
		this.amount = amount;
		consumed = true;
	}

	BigDecimal getAmount() {
		return amount;
	}

	boolean isConsumed() {
		return consumed;
	}
}

class RawPayment {
	private String key;
	private BigDecimal balance = null;
	private PaymentTO paymentTO;
	private CreditCardTO creditCardTO;
	private VoucherDTO voucherDTO;
	private BigDecimal totalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();

	public RawPayment(PaymentTO paymentTO, CreditCardTO creditCardTO) {
		this.paymentTO = paymentTO;
		this.creditCardTO = creditCardTO;
		if (paymentTO.getAmount() != null) {
			this.balance = new BigDecimal(paymentTO.getAmount().trim());
		}
		setKey(paymentTO.getType(), null, creditCardTO.getCardNo());
	}

	public ConsumedData consume(BigDecimal amountWithoutExtCharges, BigDecimal totalPayAmountWithExtCharges,
			boolean allowZeroPayment) {
		ConsumedData data = new ConsumedData();
		if (totalPayAmountWithExtCharges.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			if (balance == null) {
				totalPayment = AccelAeroCalculator.add(totalPayment, totalPayAmountWithExtCharges);
				data.setAmount(amountWithoutExtCharges);
			} else if (balance.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				if (balance.compareTo(totalPayAmountWithExtCharges) > 0) {
					totalPayment = AccelAeroCalculator.add(totalPayment, totalPayAmountWithExtCharges);
					balance = AccelAeroCalculator.add(balance, totalPayAmountWithExtCharges.negate());
					data.setAmount(amountWithoutExtCharges);
				} else {
					BigDecimal balanceAmtWithoutExtCharges = AccelAeroCalculator.add(balance, totalPayAmountWithExtCharges.negate(),
							amountWithoutExtCharges);
					totalPayment = AccelAeroCalculator.add(totalPayment, balance);
					balance = AccelAeroCalculator.getDefaultBigDecimalZero();
					data.setAmount(balanceAmtWithoutExtCharges);
				}
			}
		}

		// required to record the payment records when their is a 0 payment.
		if (allowZeroPayment && !data.isConsumed()
				&& totalPayAmountWithExtCharges.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
			data.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		return data;
	}

	public BigDecimal getTotalPaymentAmount() {
		return totalPayment;
	}

	public RawPayment(PaymentTO paymentTO) {
		this.paymentTO = paymentTO;

		setKey(paymentTO.getType(), paymentTO.getAgent(), null);
		if (paymentTO.getAmount() != null && !"".equals(paymentTO.getAmount().trim())) {
			this.balance = new BigDecimal(paymentTO.getAmount().trim());
		}
	}

	public RawPayment(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
		setKey(PaymentMethod.VOUCHER.getCode(), voucherDTO.getVoucherId(), null);
		if (voucherDTO.getAmountInBase() != null && !"".equals(voucherDTO.getAmountInBase().trim())) {
			this.balance = new BigDecimal(voucherDTO.getAmountInBase());
		}
	}

	private void setKey(String type, String agentCode, String cardNo) {
		key = type + ":" + agentCode + ":" + cardNo;
	}

	public CreditCardTO getCreditCardTO() {
		return creditCardTO;
	}

	public PaymentTO getPaymentTO() {
		return paymentTO;
	}

	public String getKey() {
		return key;
	}

	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}
}
