package com.isa.thinair.xbe.core.web.constants;

public class S2Constants {

	public interface Namespace {

		public static final String PRIVATE = "/private";

		public static final String PUBLIC = "/public";
	}

	public interface Jsp {

		public interface TravAgent {

			public static final String CREDIT_LIMIT_GRID = "/WEB-INF/jsp/airTravelAgents/CreditLimitGrid.jsp";

			public static final String TRAVEL_AGENT = "/WEB-INF/jsp/airTravelAgents/TravelAgent.jsp";

			public static final String EMAIL_OUTSTANDING_BALANCE = "/WEB-INF/jsp/emailoutstandingbalance/EmailOutstandingBalance.jsp";

			public static final String INVOICE_SETTLE = "/WEB-INF/jsp/payments/InvoiceSettle.jsp";

			public static final String MANAGE_PAYMENT = "/WEB-INF/jsp/payments/ManagePayment.jsp";

			public static final String VIEW_EMAIL_INVOICE = "/WEB-INF/jsp/payments/ViewEmailInvoice.jsp";

			public static final String ADJUSTMENT_PAYMENT_GRID = "/WEB-INF/jsp/payments/AdjustmentpaymentGrid.jsp";

			public static final String INVOICE_DETAIL = "/WEB-INF/jsp/invoicetransfer/InvoiceDetail.jsp";

			public static final String AGENT_INCENTIVE = "/WEB-INF/jsp/airTravelAgents/AgentIncentive.jsp";

			public static final String V2_INCENTIVE_SCHEME = "/WEB-INF/jsp/v2/travagent/IncentiveScheme.jsp";

			public static final String TICKET_STOCK_GRID = "/WEB-INF/jsp/airTravelAgents/StockTicketGrid.jsp";
			
			public static final String MANAGE_MCO = "/WEB-INF/jsp/payments/ManageMCO.jsp";

			public static final String PRINT_MCO = "/WEB-INF/jsp/payments/PrintMCO.jsp";
			
			public static final String V2_CREDIT_MANAGEMENT = "/WEB-INF/jsp/v2/travagent/CreditManagement.jsp";

			public static final String SELF_TOPUP = "/WEB-INF/jsp/payments/AgentSelfTopup.jsp";

		}

		public interface Alert {

			public static final String MANAGE_ALERT = "/WEB-INF/jsp/alert/ManageAlert.jsp";
			public static final String MANAGE_FLIGHT_PNR_NOTIFICATION = "/WEB-INF/jsp/alert/SendFlightPnrNotification.jsp";
			public static final String SHOW_FLIGHT_PNR_NOTIFICATION = "/WEB-INF/jsp/alert/ViewFlightPnrNotification.jsp";
			public static final String SHOW_FLIGHT_PNR_NOTIFICATION_MESSAGE = "/WEB-INF/jsp/alert/ShowFlightPnrNotificationMessage.jsp";
			public static final String SHOW_FLIGHT_PNR_NOTIFICATION_CONTACT_DETAILS_MESSAGE = "/WEB-INF/jsp/alert/ViewFlightPnrNotificationUpdateContact.jsp";
			public static final String SHOW_PENDING_EMAIL_SMS_DETAILS_MESSAGE = "/WEB-INF/jsp/alert/ViewPendingEmailSMSDetails.jsp";
		}

		public interface Gds {

			public static final String GDS_MESSAGES_ADMIN = "/WEB-INF/jsp/gdsMessages/GdsMessagesAdmin.jsp";

			public static final String GDS_MESSAGE_DETAIL = "/WEB-INF/jsp/gdsMessages/GdsMessageDetails.jsp";
		}

		public interface Flight {

			public static final String FLIGHT_REPORT = "/WEB-INF/jsp/flight/FlightReport.jsp";

			public static final String FLIGHT_REPORT_DETAIL = "/WEB-INF/jsp/flight/FlightReportDetail.jsp";

			public static final String FLIGHT_REPORT_GRID = "/WEB-INF/jsp/flight/FlightReportGrid.jsp";

		}

		public interface Reservation {

			public static final String XBE_DATA = "/WEB-INF/jsp/reservation/XBEData.jsp";

			public static final String FARE_CONDS = "/WEB-INF/jsp/reservation/FareConds.jsp";

			public static final String SEARCH = "/WEB-INF/jsp/reservation/Search.jsp";

			public static final String DISPLAY_MEALS = "/WEB-INF/jsp/reservation/DisplayMeals.jsp";

			public static final String V2_MAKE_RESERVATION = "/WEB-INF/jsp/v2/reservation/makeReservation.jsp";
			public static final String V2_CHANGE_FARE = "/WEB-INF/jsp/v2/reservation/changeFare.jsp";
			public static final String V2_CREDIT = "/WEB-INF/jsp/v2/reservation/credit.jsp";
			public static final String V2_SHOW_MEALS = "/WEB-INF/jsp/v2/reservation/displayMeals.jsp";
			public static final String V2_DISPLAY_FARE_RULES = "/WEB-INF/jsp/v2/reservation/displayFareRules.jsp";
			public static final String V2_PRINT_TAX_INVOICE = "/WEB-INF/jsp/v2/reservation/printTaxInvoice.jsp";
			public static final String V2_PRINTITINERARY = "/WEB-INF/jsp/v2/reservation/printItinerary.jsp";
			public static final String V2_PRINTHISTORY = "/WEB-INF/jsp/v2/reservation/printHistory.jsp";
			public static final String V2_PRINTRECIEPT = "/WEB-INF/jsp/v2/reservation/printReciept.jsp";
			public static final String V2_DISPLAY_TAXES_SURCHARGES = "/WEB-INF/jsp/v2/reservation/displayTaxesSurcharges.jsp";

			public static final String V2_SEARCH_RESERVATION = "/WEB-INF/jsp/v2/modify/searchReservation.jsp";
			public static final String V2_RESERVATION = "/WEB-INF/jsp/v2/modify/reservation.jsp";
			public static final String V2_USERNOTES = "/WEB-INF/jsp/v2/modify/userNotes.jsp";

			public static final String V2_ACCOUNTS = "/WEB-INF/jsp/v2/modify/paxAccountDetails.jsp";
			public static final String V2_CONFIRMUPDATE = "/WEB-INF/jsp/v2/modify/confirmUpdate.jsp";
			public static final String V2_PAX_DETAILS = "/WEB-INF/jsp/v2/modify/paxDetails.jsp";
			public static final String V2_INTERNATIONAL_FLIGHT_DETAILS = "/WEB-INF/jsp/v2/modify/InternationalFlightDetails.jsp";
			public static final String V2_ADVANCED_PASSENGER_SELECTION_FOR_SPLIT="/WEB-INF/jsp/v2/modify/AdvancedPassengerSelection.jsp";
			public static final String V2_E_TICKET_MASK = "/WEB-INF/jsp/v2/modify/paxETicketMask.jsp";
			public static final String V2_AGENT_TICKET_STOCK = "/WEB-INF/jsp/v2/travagent/AgentTicketStock.jsp";
			public static final String V2_CREATE_ALERT = "/WEB-INF/jsp/v2/modify/createAlert.jsp";
			public static final String V2_PAX_NAMES_IN_OTHER_LANGUAGES = "/WEB-INF/jsp/v2/modify/paxNames.jsp";
			public static final String V2_GROUP_PAX_STATUS_UPDATE = "/WEB-INF/jsp/v2/modify/groupPaxStatusUpdate.jsp";

			public static final String V2_MODIFY_SEGMENT = "/WEB-INF/jsp/v2/modify/modifySegment.jsp";
			public static final String V2_MODIFY_ANCILLARY = "/WEB-INF/jsp/v2/modify/modifyAncillary.jsp";
			public static final String V2_TRANSFER_SEGMENT = "/WEB-INF/jsp/v2/modify/transferSegment.jsp";
			public static final String V2_ADD_SEGMENT = "/WEB-INF/jsp/v2/modify/addSegment.jsp";
			public static final String MODIFY_RES_REQUOTE = "/WEB-INF/jsp/v2/modify/modifyResRequote.jsp";

			public static final String V2_SEARCH_SHEDULES = "/WEB-INF/jsp/v2/search/flightScheduleSearch.jsp";

			public static final String TAXES_SURCHARGES = "/WEB-INF/jsp/reservation/TaxSurcharge.jsp";
			
			public static final String BLACKLISTED_PAX_RES = "/WEB-INF/jsp/v2/reservation/blackListedPaxReservation.jsp";

			// Demo
			public static final String V2_MULTYCITY_SEARCH = "/WEB-INF/jsp/v2/demo/advancedSearch.jsp";
			public static final String RESERVATION2 = "/WEB-INF/jsp/v2/demo/availability.jsp";
			public static final String RESERVATION3 = "/WEB-INF/jsp/v2/demo/availability-multi.jsp";

			public static final String PASSENGER_DETAILS_UPLOAD_RESULT_JSP = "/WEB-INF/jsp/v2/reservation/PassengerDetailsParseResult.jsp";

			public static final String V2_MCO_DETAILS = "/WEB-INF/jsp/v2/modify/paxMCODetails.jsp";
		}

		public interface Common {

			public static final String ERROR_REDIRECT = "/WEB-INF/jsp/common/ErrorRedirect.jsp";

			public static final String LOAD_MSG = "/WEB-INF/jsp/common/LoadMsg.jsp";

			public static final String LOADAJAX_JSP = "/WEB-INF/jsp/common/loadAJAX.jsp";

			public static final String DASHBOARD_UTIL_POPUP = "/WEB-INF/jsp/v2/common/uitil_popup.jsp";
			
			public static final String BLANK_JSP = "/WEB-INF/jsp/common/Blank.jsp";

		}

		public interface System {

			public static final String LOGIN = "/WEB-INF/jsp/login/Login.jsp";

			public static final String PASSWORD_CHANGE = "/WEB-INF/jsp/passwordchange/PasswordChange.jsp";

			public static final String MENU = "/WEB-INF/jsp/NMenu/Menu.jsp";

			public static final String REPORT_PROBLEM = "/WEB-INF/jsp/problem/ReportProblem.jsp";

			public static final String ATTACH_FILE = "/WEB-INF/jsp/problem/AttachFile.jsp";

			public static final String SHOWTRACKER = "/WEB-INF/jsp/v2/common/tracking.jsp";
		}

		public interface GroupBooking {
			public static final String GRP_BOOKING_URL = "/WEB-INF/jsp/GroupBooking/GroupBooking.jsp";
			public static final String GRP_ROLLFORWARD_BOOKING_URL = "/WEB-INF/jsp/GroupBooking/GroupBookingRollForward.jsp";
		}

		public interface Reporting {

			public static final String ON_HOLD_REPORT = "/WEB-INF/jsp/reporting/OnHoldReport.jsp";

			public static final String ON_HOLD_IBE_REPORT = "/WEB-INF/jsp/reporting/IBEOnHoldReport.jsp";

			public static final String AGENT_TRANSACTION_DETAIL_REPORT = "/WEB-INF/jsp/reporting/AgentTransactionDetailsReport.jsp";

			public static final String COMPANY_PAYMENT_REPORT = "/WEB-INF/jsp/reporting/CompanyPaymentReport.jsp";

			public static final String FAIR_DISCOUNTS_REPORT = "/WEB-INF/jsp/reporting/FareDiscountsReport.jsp";

			public static final String PAX_CONTACT_DETAIL = "/WEB-INF/jsp/reporting/PaxContactDetails.jsp";

			public static final String PAX_STATUS_REPORT = "/WEB-INF/jsp/reporting/PAXStatusReport.jsp";

			public static final String SECTOR_CONTRIBUTION_BY_AGENT = "/WEB-INF/jsp/reporting/SectorContributionByAgent.jsp";

			public static final String VIEW_SEAT_INVENTORY_COLLECTION = "/WEB-INF/jsp/reporting/ViewSeatInventoryCollections.jsp";

			public static final String AGENT_GSA_DETAIL_REPORT = "/WEB-INF/jsp/reporting/GSAAgentDetailsReport.jsp";

			public static final String FLOWN_PASSENGER_LIST_REPORT = "/WEB-INF/jsp/reporting/FlownPassengerListReport.jsp";

			public static final String AGENT_WISE_FORWARD_SALES_REPORT = "/WEB-INF/jsp/reporting/AgentWiseForwardSalesReport.jsp";

			public static final String AGENT_MEAL_DETAILS_REPORT = "/WEB-INF/jsp/reporting/AgentMealDetailsReport.jsp";

			public static final String SCHEDULE_DETAIL_REPORT = "/WEB-INF/jsp/reporting/SeheduleDetailsReport.jsp";

			public static final String HALA_SERVICES_REPORT = "/WEB-INF/jsp/reporting/HalaServicesReport.jsp";

			public static final String BOOKED_SSR_DETAILS_REPORT = "/WEB-INF/jsp/reporting/BookedSSRDetailsReport.jsp";

			public static final String REPORTING_FARE_DETAILS_JSP = "/WEB-INF/jsp/reporting/FareDetailsReport.jsp";

			public static final String REPORTING_TAX_DETAILS_JSP = "/WEB-INF/jsp/reporting/TaxDetailsReport.jsp";

			public static final String RPTS_LOYALTY_POINTS_JSP = "/WEB-INF/jsp/reporting/LoyaltyPointsReport.jsp";

			public static final String RPTS_AGENT_TRANSACTION_JSP = "/WEB-INF/jsp/graph/Graph.jsp";

			public static final String AGENT_HANDLING_FEE_REPORT = "/WEB-INF/jsp/reporting/AgentHandlingFeeReport.jsp";

			public static final String CHARGE_ADJUSTMENTS_REPORT = "/WEB-INF/jsp/reporting/ChargeAdjustmentsReport.jsp";

			public static final String AGENT_SALES_REFUND_SUMMARY_REPORT = "/WEB-INF/jsp/reporting/AgentSalesRefundSummaryReport.jsp";

			public static final String USER_INCENTIVE_DETAILS_REPORT = "/WEB-INF/jsp/reporting/UserIncentiveReport.jsp";

			public static final String AGENT_SALE_REF_RPT_RESULT = "/WEB-INF/jsp/reporting/AgentSalesRefundResult.jsp";

			public static final String FLIGHT_ANCILLARY_DETAILS_REPORT = "/WEB-INF/jsp/reporting/FlightAncillaryDetailsReport.jsp";

			public static final String GDS_RESERVATIONS_REPORT = "/WEB-INF/jsp/reporting/GDSReservationsReport.jsp";

			public static final String ANCILLARY_DETAILS_FLIGHT_REPORT = "/WEB-INF/jsp/reporting/AncillaryDetailsFlightReport.jsp";

			public static final String ANCILLARY_DETAILS_FLIGHT_REPORT_GRID = "/WEB-INF/jsp/reporting/AncillaryDetailsFlightReportGrid.jsp";
			
			public static final String INTERNATIONAL_FLIGHT_DETAILS_REPORT = "/WEB-INF/jsp/reporting/InternationalFlightDetailsReport.jsp";
			
			public static final String NIL_TRANSACTIONS_AGENTS_LIST_REPORT = "/WEB-INF/jsp/reporting/NILTransactionsAgentsListReport.jsp";
		
			public static final String REPORTING_MONITOR_PERFOMANCE_SALES_STAFF_JSP = "/WEB-INF/jsp/reporting/MonitorPerformanceOfSalesStaff.jsp";

			public static final String ISSUED_VOUCHER_DETAIL_REPORT = "/WEB-INF/jsp/reporting/IssuedVoucherDetailsReport.jsp";
			
			public static final String REDEEMED_VOUCHER_DETAIL_REPORT = "/WEB-INF/jsp/reporting/RedeemedVoucherDetailsReport.jsp";

			public static final String MCO_DETAIL_REPORT = "/WEB-INF/jsp/reporting/MCODetailReport.jsp";
			
			public static final String MCO_SUMMARY_REPORT = "/WEB-INF/jsp/reporting/MCOSummaryReport.jsp";
			
			public static final String FORCE_CONFIRMED_BOOKING_REPORT = "/WEB-INF/jsp/reporting/ForceConfirmedBookingReport.jsp";
			
			public static final String FLOWN_PASSENGER_REPORT = "/WEB-INF/jsp/reporting/FlownPassengerReport.jsp";
			
			public static final String RPTS_LMS_BLOCKED_CREDIT_JSP = "/WEB-INF/jsp/reporting/LMSBlockedCreditReport.jsp";
		}

		public interface Search {

			public static final String GET_XBE_DATA = "/WEB-INF/jsp/search/GetXBEData.jsp";

			public static final String V2_CLEAR_ALERT = "/WEB-INF/jsp/v2/search/clearAlert.jsp";
		}

		public interface PnlAdl {

			public static final String PNL_ADL_GRID = "/WEB-INF/jsp/throughcheck/PnlAdlGrid.jsp";

			public static final String SEND_PNL_ADL = "/WEB-INF/jsp/scheduledservices/SendPNLADL.jsp";

			public static final String PRINT_PNL_ADL = "/WEB-INF/jsp/scheduledservices/PrintPNLADL.jsp";
		}

		public interface PalCal {

			public static final String PAL_CAL_TIMING_CONFIG = "/WEB-INF/jsp/palCalMessage/PalCalTimings.jsp";

			public static final String SEND_PAL_CAL = "/WEB-INF/jsp/scheduledservices/SendPALCAL.jsp";

			public static final String PRINT_PAL_CAL = "/WEB-INF/jsp/scheduledservices/PrintPNLADL.jsp";
		}

		public interface User {

			public static final String USER_ADMIN = "/WEB-INF/jsp/user/UserAdmin.jsp";

			public static final String DUMMY_USER = "/WEB-INF/jsp/user/DummyUser.jsp";

			public static final String DUMMY_LAND = "/WEB-INF/jsp/user/DummyLand.jsp";

			public static final String USER_ADMIN_GRID = "/WEB-INF/jsp/user/UserAdminGrid.jsp";

			public static final String USER_ADD_NOTE_JSP = "/WEB-INF/jsp/user/AddUserNote.jsp";

			public static final String USER_VIEW_NOTE_JSP = "/WEB-INF/jsp/user/ViewUserNote.jsp";
		}
		
		public interface Customer{
			
			public static final String CUSTOMER = "/WEB-INF/jsp/customer/Customer.jsp";
			public static final String FREQUENT_CUSTOMER = "/WEB-INF/jsp/customer/AgentFrequentCustomerProfile.jsp";

			
		}

		public interface Test {

			public static final String INTERLINED_TRANSFER = "/WEB-INF/jsp/testing/InterlinedTransfer.jsp";

			public static final String COMMON_TESTING_UI = "/WEB-INF/jsp/testing/CommonTestingUI.jsp";
		}

		public interface Checkin {

			public static final String CHECKIN_JSP = "/WEB-INF/jsp/checkin/Checkin.jsp";

			public static final String CHECKIN_ADD_NOTE_JSP = "/WEB-INF/jsp/checkin/AddCheckinNote.jsp";

			public static final String CHECKIN_VIEW_NOTE_JSP = "/WEB-INF/jsp/checkin/ViewCheckinNote.jsp";

			public static final String PASSENGER_CHECKIN_JSP = "/WEB-INF/jsp/checkin/PassengerCheckin.jsp";

			public static final String CHECKIN_PFSDETAILS_JSP = "/WEB-INF/jsp/checkin/PFSDetails.jsp";
		}
		
		public interface RequestManagement{
			
			public static final String REQUEST_MANAGEMENT = "/WEB-INF/jsp/requestmanagement/RequestManagement.jsp";
			public static final String REQUEST_HISTORY = "/WEB-INF/jsp/requestmanagement/RequestHistory.jsp";
			public static final String REQUEST_SUBMIT_POPUP = "/WEB-INF/jsp/requestmanagement/RequestSubmitPopup.jsp";
		}
	
		public interface TypeB {

			public static final String TYPEB_MESSAGES = "/WEB-INF/jsp/v2/typeB/TypeBMessageDetails.jsp";
		}
		
		public interface Payment {

			public static final String POST_PGW_DATA = "/WEB-INF/jsp/payment/PostPGWData.jsp";
			public static final String EXTERNAL_PGW_FEEDBACK = "/WEB-INF/jsp/payment/HandleEPGResponse.jsp";
			public static final String CCTOPUP_PGW_FEEDBACK = "/WEB-INF/jsp/payment/HandleCCTopupEPGResponse.jsp";
		}

		public interface Voucher {

			public static final String VOUCHER = "/WEB-INF/jsp/voucher/voucher.jsp";

			public static final String GIFT_VOUCHER = "/WEB-INF/jsp/voucher/giftVoucher.jsp";
			
			public static final String PRINT_VOUCHER = "/WEB-INF/jsp/voucher/printVoucher.jsp";
			
			public static final String ISSUE_VOUCHER_FOR_REPROTECTED_PAX = "/WEB-INF/jsp/voucher/issueVoucherForReprotectedPax.jsp";

		}

	}

	public interface Result {

		public static final String SUCCESS = "success";

		public static final String REQUOTE = "requote";

		public static final String ERROR = "error";

		public static final String GET_XBE_DATA = "getXBEData";

		public static final String CREDIT_LIMIT = "creditLimit";

		public static final String SHOW_LOGIN = "showLogin";

		public static final String SHOW_MAIN = "showMain";

		public static final String SHOW_PASSWORD_CHANGE = "showPasswordChange";

		public static final String ADJUSTED_PAYMENT = "adjustedpayment";

		public static final String SHOW_LANDING = "showLanding";

		public static final String SHOW_DUMMY = "showDummy";

		public static final String VIEW_USER_NOTE = "viewUserNote";

		public static final String ACTION_FORWARD_SUCCESS_AJAX = "successAjax";

		public static final String RES_SUCCESS = "successres";

		public static final String SHOW_TRACKER = "showTracker";

	}

	public interface Actions {
		public static final String SHOW_PASSWORD_CHANGE_ACTION = "/private/showPasswordChange.action";
	}

	public interface Session_Data {
		public static final String XBE_SES_RESDATA = "XBE_SES_RESDATA";
		public static final String LCC_SES_BOOKING_CART = "LCC_SES_BOOKING_CART";
		// to be compatible with the V1 flow
		public static final String LCC_SESSION_BLOCK_IDS = WebConstants.REQ_SESSION_BLOCK_SEAT_IDS;
		public static final String LCC_ACCESSED = "LCC_ACCESSED";
		public static final String RESER_PROCESS_PARAMS = "RESER_PROCESS_PARAMS";
		public static final String POST_PAY_DATA = "POST_PAY_DATA";
		public static final String EXTERNAL_PAYMENT_GATEWAY_RESPONSE = "EXT_PGW_RESPONSE";
		public static final String EXTERNAL_PGW_RESPONSE_STRING = "EXT_PGW_RESPONSE_STRING";
		public static final String SELF_TOPUP_AMOUNT = "SELF_TOPUP_AMOUNT";
		public static final String SELF_TOPUP_REMARKS = "SELF_TOPUP_REMARKS";
		public static final String SELF_TOPUP_MSGTXT = "SELF_TOPUP_MSGTXT";
		public static final String SELF_TOPUP_SEL_CURR = "SELF_TOPUP_SEL_CURR";
	}

}
