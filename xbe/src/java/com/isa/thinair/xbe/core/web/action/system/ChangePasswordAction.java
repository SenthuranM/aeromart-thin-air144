package com.isa.thinair.xbe.core.web.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.system.ChangePasswordRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SHOW_MAIN, value = S2Constants.Jsp.System.MENU),
		@Result(name = S2Constants.Result.SHOW_PASSWORD_CHANGE, value = S2Constants.Jsp.System.PASSWORD_CHANGE),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ChangePasswordAction extends BaseRequestAwareAction {

	public String execute() {
		return ChangePasswordRH.execute(request);
	}
}
