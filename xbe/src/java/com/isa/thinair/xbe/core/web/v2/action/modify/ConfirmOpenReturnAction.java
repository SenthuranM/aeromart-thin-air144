package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
/**
 * @author lalanthi
 * Action class for confirming open return segments.
 */
public class ConfirmOpenReturnAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ConfirmOpenReturnAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private String flightRPHList;

	private String cabinClass;

	private String pnr;

	private String groupPNR;

	private String version;

	private String fltSegment;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;
	private String flexiSelected;

	@SuppressWarnings("unchecked")
	/**
	 * This action handles open return segments confirmation before the expiry date. The selected inbound segment should be attached to the reservation and the open return segment should be removed.
	 * There won't be any payments involved as original return fare is already applied to the booking.
	 * Booking should be updated with the new flight segment details
	 */
	public String execute() {
		
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			ReservationProcessParams rpParams = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), false, null, null, false, false);
			if (isGroupPNR) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), null);
			}
			/** These are the segments belong to original reservation */
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			List<String> confirmingSegList = new ArrayList<String>();
			if (fltSegment != null && fltSegment.length() > 0) {
				String[] segRPHArr = fltSegment.split(",");
				for (String segRPH : segRPHArr) {
					confirmingSegList.add(segRPH.substring(0, segRPH.indexOf("$")));
				}
			}
			if (fareQuoteParams == null) {
				fareQuoteParams = new FlightSearchDTO();
				fareQuoteParams.setClassOfService(cabinClass);
			}

			/** These are the new segment details */
			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(flightRPHList, fareQuoteParams);
			if (!isGroupPNR) { // No open return segments for Interline

				LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
				for (IFlightSegment segTO : flightSegmentTOs) {
					String depDate = DateUtil.formatDate(segTO.getDepartureDateTime(), "dd/MM/yyyy");
					fareQuoteParams.setDepartureDate(depDate);
				}

				Collection<TempSegBcAlloc> blockSeatIds = (Collection<TempSegBcAlloc>) request.getSession().getAttribute(
						S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS);
				FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, outFlightRPHList,
						retFlightRPHList);
				String pnr = request.getParameter("pnr");
				String modifySegment = "";
				if (pnr != null && !"".equals(pnr)) {
					ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"),
							request.getParameter("modifySegment"));
					modifySegment = request.getParameter("modifySegment");
				}

				BookingUtil.addConfirmOprtSegments(lccClientSegmentAssembler, flightSegmentTOs,
						BookingUtil.getNextSegmentSequnce(colsegs));
				lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
				setLastFareQuotedDate(lccClientSegmentAssembler);
				lccClientSegmentAssembler.setLccTransactionIdentifier(resInfo.getTransactionId());
				lccClientSegmentAssembler.setSelectedFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
				lccClientSegmentAssembler.setFlexiSelected(ReservationUtil.isFlexiSelected(flexiSelected));
				BookingUtil.populateInsurance(bookingShoppingCart.getInsuranceQuotes(), lccClientSegmentAssembler,
						getAdultCount(flightPriceRQ));
				Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs, modifySegment,
						rpParams, true);
				LCCClientResAlterModesTO lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeModifySegmentRequest(
						pnr, colModSegs, null, lccClientSegmentAssembler, null, null, null, version,
						resInfo.getReservationStatus());
				lCCClientResAlterQueryModesTO.setGroupPnr(isGroupPNR);
				lCCClientResAlterQueryModesTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
				Collection<Integer> oldSegIds = new ArrayList<Integer>();
				for (LCCClientReservationSegment fSeg : colsegs) {
					if (confirmingSegList.contains(fSeg.getBookingFlightSegmentRefNumber())) {
						oldSegIds.add(new Integer(fSeg.getBookingFlightSegmentRefNumber()));
					}
				}

				ModuleServiceLocator.getAirproxySegmentBD().confirmOpenReturnSegments(pnr, lCCClientResAlterQueryModesTO,
						oldSegIds, blockSeatIds, getTrackInfo());

				// clearing the block seats after modification success.
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, null);

			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;

	}

	private void setLastFareQuotedDate(LCCClientSegmentAssembler lccClientSegmentAssembler) {
		AvailPreferencesTO availPreferencesTO = lccClientSegmentAssembler.getFlightPriceRQ().getAvailPreferences();
		if (availPreferencesTO.getModifiedSegmentsList() != null && availPreferencesTO.getModifiedSegmentsList().size() > 0) {
			Date maxValidDate = null;
			Date lastFQDate = null;
			for (FlightSegmentDTO segment : availPreferencesTO.getModifiedSegmentsList()) {
				lastFQDate = segment.getLastFareQuotedDate();
				maxValidDate = segment.getTicketValidTill();
				break;
			}
			if (maxValidDate == null) {
				maxValidDate = AvailabilityConvertUtil.getMaxValidDate(availPreferencesTO);
			}
			if (maxValidDate != null && maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())) {
				lccClientSegmentAssembler.setLastFareQuotedDate(lastFQDate);
			}
		}
	}

	private int getAdultCount(FlightPriceRQ flightPriceRQ) {
		int i = 0;
		for (PassengerTypeQuantityTO paxQty : flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList()) {
			if (!paxQty.getPassengerType().equals(PaxTypeTO.INFANT)) {
				i++;
			}
		}
		return i;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getFltSegment() {
		return fltSegment;
	}

	public void setFltSegment(String fltSegment) {
		this.fltSegment = fltSegment;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public String getFlexiSelected() {
		return flexiSelected;
	}

	public void setFlexiSelected(String flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

}
