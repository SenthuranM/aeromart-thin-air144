package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import org.json.JSONObject;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.webplatform.core.constants.MessageCodes;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AncillaryUtil;
import com.isa.thinair.xbe.core.web.v2.util.AutoCheckinUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Struts action to handle the ancillary availability.
 * 
 * @author Abheek Das Gupta
 * @author Dilan Anuruddha
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AncillaryAvailabilityAction extends BaseRequestAwareAction {

	private static final Log log = LogFactory.getLog(AncillaryAvailabilityAction.class);
	
	private static final String PAX_VALIDATION = "paxValidation";
	private static final String ADULT_AGE_CUT_OVER_YEARS = "adultAgeCutOverYears";
	private static final String CHILD_AGE_CUT_OVER_YEARS = "childAgeCutOverYears";
	private static final String INFANT_AGE_CUT_OVER_YEARS = "infantAgeCutOverYears";

	private boolean success = true;

	private FlightSearchDTO searchParams;

	private boolean seatMapAvailable = false;

	private boolean anciPreferenceMsgEnabled = false;

	private boolean mealsAvailable = false;

	private boolean ssrAvailable = false;

	private boolean insuranceAvailable = false;

	private boolean airportServiceAvailable = false;

	private boolean airportTransferAvailable = false;

	private boolean mulipleMealsSelectionEnabled = false;

	private boolean onHoldBooking;

	private boolean waitListingBooking;

	private String messageTxt;

	private String flightRPHList;

	private String insFltSegments;

	private String modifyingSegmentsIds;

	private List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO;

	private String selectedSystem;

	private boolean modifyAncillary = false;

	private static boolean hasPrivAddSeatsInCutover = false;

	private boolean modifySegment = false;

	private boolean addSegment = false;

	private boolean hasPrivEditSeats = false;

	private boolean hasPrivSSRChange = false;

	private boolean hasPrivModifySSRWithinBuffer = false;

	private String pnr;

	private boolean autoCheckinAvailable = false;

	@Deprecated
	private boolean allowInsModify = true;

	@Deprecated
	private boolean insuranceViewOnly = false;

	private String resPaxInfo;

	private String foidIds = "";

	private String foidCode = "";

	private Integer[] segmentList;

	SYSTEM system = null;

	private boolean focMealEnabled = false;
	
	private boolean flexiSelectionEnabledInAnciPage = false;

	private boolean baggageAvailable = false;

	private boolean baggageMandatory = false;

	private boolean baggageChargeOverride = false;

	private String paxWiseAnci;

	private Map<String, Boolean> carrierWiseSSRInvCheck = new HashMap<String, Boolean>();

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private Map<String, LogicalCabinClassDTO> lccMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();

	private boolean isRequote = false;

	private boolean gdsPnr = false;

	private String isOpenReturnReservation;

	private String jsonOnds;
	
	private boolean isSsrRefundableInModification;
	
	private boolean isBundleFarePopupEnabled = false;
	
	private String autoCheckinPaxConfig;

	public String execute() {
		String result = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		JSONObject paxValidation;
		JSONObject segWisePaxConfigJson;
		try {

			ReservationProcessParams resParm = (ReservationProcessParams) request.getSession().getAttribute(
					S2Constants.Session_Data.RESER_PROCESS_PARAMS);

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			String strTxnIdntifier = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			List<FlightSegmentTO> fltSegApplicableToIns = null;
			boolean skipInsurance = false;
			boolean isRakInsurance = AppSysParamsUtil.isRakEnabled();

			int adultCutOffAge = globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears();
			int childCutOffAge = globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears();
			int infantCutOffAge = globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getCutOffAgeInYears();
			int infantAgeLowerBoundaryInDays = globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getAgeLowerBoundaryInDays();
			boolean stanby = false;
			LCCReservationBaggageSummaryTo resBaggageSummary;
			
			LCCAncillaryAvailabilityInDTO inAncillaryAvailabilityDTO = new LCCAncillaryAvailabilityInDTO();
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(flightRPHList, false, true, false);
				system = fltSegBuilder.getSystem();
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
				fltSegApplicableToIns = flightSegmentTOs;
				stanby = isStandby(flightSegmentTOs);
				// removeStandbySegments(flightSegmentTOs);

				if (this.insuranceAvailable) {
					skipInsurance = true;
				}

				inAncillaryAvailabilityDTO.setAddEditAnci(true);
				if (paxWiseAnci != null && !"".equals(paxWiseAnci)) {
					Collection<LCCClientReservationPax> colPax = AncillaryUtil.extractLccReservationPax(paxWiseAnci);
					inAncillaryAvailabilityDTO.setAirportServiceCountMap(AncillaryUtil
							.extractAiportServiceCountFromJsonObj(colPax));
					inAncillaryAvailabilityDTO.setFlightRefWiseSelectedMeals(AncillaryUtil
							.getFlightReferenceWiseSelectedMeals(colPax));
					inAncillaryAvailabilityDTO.setFlightRefWiseSelectedSSR(AncillaryUtil
							.getFlightReferenceWiseSelectedSSRs(colPax));
				}

				resBaggageSummary = resInfo.getResBaggageSummary();
				if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
					onHoldBooking = true;
				}

			} else {
				stanby = isStandby(searchParams);

				// If reservation is on-hold & promotion is applied, skip applying promotion
				if (bookingShoppingCart.getFareDiscount() != null
						&& bookingShoppingCart.getFareDiscount().getPromotionId() != null) {
					if (onHoldBooking || waitListingBooking) {
						bookingShoppingCart.setSkipPromotionForOnHold(true);
					} else {
						bookingShoppingCart.setSkipPromotionForOnHold(false);
					}
				}

				strTxnIdntifier = resInfo.getTransactionId();
				resInfo.setSelectedSystem(SYSTEM.getEnum(selectedSystem));
				system = resInfo.getSelectedSystem();
				if (isRequote) {
					inAncillaryAvailabilityDTO.setRequote(true);
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(flightRPHList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(flightRPHList, searchParams);
				}

				if (insFltSegments == null) {
					fltSegApplicableToIns = flightSegmentTOs;
				} else {
					if (isRequote) {
						if (flightSegmentTOs != null && !flightSegmentTOs.isEmpty()) {
							fltSegApplicableToIns = ReservationUtil.populateFlightSegmentList(insFltSegments, searchParams);
						} else {
							fltSegApplicableToIns = new ArrayList<FlightSegmentTO>();
						}
					} else {
						FltSegBuilder fltSegBuilder = new FltSegBuilder(insFltSegments, true, false, true);
						fltSegApplicableToIns = fltSegBuilder.getSelectedFlightSegments();
					}
					fltSegApplicableToIns.addAll(flightSegmentTOs);
				}

				if (this.insuranceAvailable) {
					skipInsurance = true;
				}

				Map<Integer, Set<String>> ondSeqRPHMap = new HashMap<Integer, Set<String>>();
				for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
					if (!ondSeqRPHMap.containsKey(flightSegmentTo.getOndSequence())) {
						ondSeqRPHMap.put(flightSegmentTo.getOndSequence(), new HashSet<String>());
					}
					ondSeqRPHMap.get(flightSegmentTo.getOndSequence()).add(flightSegmentTo.getFlightRefNumber());
				}

				// if the ondSequence is wrong we need to validate it from fare-quote it self. following code will not
				// work properly when same segment code duplicates
				// Multicity Search/Re-Quote
				// Ex: 1.CMB/SHJ/DOH 2.SHJ/DOH in above case first ondSequence & second ondSequence might get updated
				// incorrectly

				// for (FlightSegmentTO flightSegment : flightSegmentTOs) {
				// for (FareRuleDTO fareRule :
				// bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getApplicableFareRules()) {
				// if (fareRule.getOrignNDest().contains(flightSegment.getSegmentCode())) {
				// fareRule.setOndSequence(flightSegment.getOndSequence());
				// }
				// }
				// }

				resBaggageSummary = new LCCReservationBaggageSummaryTo();
				Map<String, String> bookingClasses = new HashMap<String, String>();
				Map<String, String> classesOfService = new HashMap<String, String>();
				for (FareRuleDTO fareRuleDTO : bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getApplicableFareRules()) {
					bookingClasses.put(fareRuleDTO.getOrignNDest(), fareRuleDTO.getBookingClassCode());
					Set<String> fltRefs = ondSeqRPHMap.get(fareRuleDTO.getOndSequence());
					if (fltRefs != null && !fltRefs.isEmpty()) {
						for (String fltRef : fltRefs) {
							classesOfService.put(fltRef, fareRuleDTO.getCabinClassCode());
						}
					}
				}
				resBaggageSummary.setBookingClasses(bookingClasses);
				resBaggageSummary.setClassesOfService(classesOfService);
				resInfo.setResBaggageSummary(resBaggageSummary);
			}
			
			if (resBaggageSummary != null) {
				resBaggageSummary.setAllowTillFinalCutOver(BasicRH.hasPrivilege(request,
						PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER));
			}
			InsuranceFltSegBuilder insFltSegBldr = new InsuranceFltSegBuilder(fltSegApplicableToIns);
			boolean insFltSegIncluded = false;

			if (!stanby) {
				if (resPaxInfo != null && !resPaxInfo.equals("")) {
					Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPaxInfo);
					if (foidIds == null || foidIds.equals("")) {
						for (LCCClientReservationPax colPax : colpaxs) {
							if (colPax.getLccClientAdditionPax() != null
									&& !colPax.getLccClientAdditionPax().getPassportNo().equals("")) {
								String passport = colPax.getLccClientAdditionPax().getPassportNo();
								Date expDate = colPax.getLccClientAdditionPax().getPassportExpiry();
								if (passport != null) {
									if (passport != null && !passport.equals("")) {
										foidIds += passport + "^";
									}
									if (expDate != null) {

										// Check Passport Expiry Date
										for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
											Date depDateTime = flightSegmentTo.getDepartureDateTime();
											if (depDateTime.getTime() >= expDate.getTime()) {
												throw new XBERuntimeException(WebConstants.PAX_PASSPORT_EXPIRE);
											}
										}
									}
								}
							}
						}
						foidCode = "PSPT";
						String modSegIds[] = null;
						if (modifyingSegmentsIds != null && modifyingSegmentsIds.length() > 0) {
							modSegIds = modifyingSegmentsIds.split(",");
						}
						List<String> modSegList = null;
						if (modSegIds != null && modSegIds.length > 0) {
							modSegList = Arrays.asList(modSegIds);
						}
						segmentList = new Integer[flightSegmentTOs.size()];
						int index = 0;
						for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
							// This is to bypass the passport duplication check in modifying segments
							if (modSegList != null && !modSegList.contains(flightSegmentTO.getFlightSegId())) {
								segmentList[index] = flightSegmentTO.getFlightSegId();
								index++;
							}
						}
					}

					// Validate DOB
					for (LCCClientReservationPax pax : colpaxs) {
						Date paxDOB = pax.getDateOfBirth();
						if (paxDOB != null && pax.getPaxType().equals(PaxTypeTO.ADULT)) {
							int maxAgeLimit = adultCutOffAge;
							int lowerAgeLimit = childCutOffAge;

							for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
								Date depDateTime = flightSegmentTo.getDepartureDateTime();

								if (!Util.compareAge(paxDOB, depDateTime, maxAgeLimit)) {
									throw new ModuleException(MessageCodes.PAX_DOB_RANGE_CONFLICT);
									// PAX_DOB_RANGE_CONFLICT
								}

								if (Util.compareAge(paxDOB, depDateTime, lowerAgeLimit)) {
									throw new ModuleException(MessageCodes.PAX_DOB_RANGE_CONFLICT);
								}
							}

						} else if (paxDOB != null && pax.getPaxType().equals(PaxTypeTO.CHILD)) {
							int maxAgeLimit = childCutOffAge;
							int lowerAgeLimit = infantCutOffAge;
							for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
								Date depDateTime = flightSegmentTo.getDepartureDateTime();

								if (!Util.compareAge(paxDOB, depDateTime, maxAgeLimit)) {
									throw new ModuleException(MessageCodes.PAX_DOB_RANGE_CONFLICT);
								}

								if (Util.compareAge(paxDOB, depDateTime, lowerAgeLimit)) {
									throw new ModuleException(MessageCodes.PAX_DOB_RANGE_CONFLICT);
								}
							}

						} else if (paxDOB != null && pax.getPaxType().equals(PaxTypeTO.INFANT)) {
							int maxAgeLimit = infantCutOffAge;
							for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
								Date depDateTime = flightSegmentTo.getDepartureDateTime();

								if (!Util.compareAge(paxDOB, depDateTime, maxAgeLimit)) {
									throw new ModuleException(MessageCodes.PAX_DOB_RANGE_CONFLICT);
									// PAX_DOB_RANGE_CONFLICT
								}

								// Check for Infant Min Days Limit Check
								if (!Util.checkInfantMinAgeInDays(paxDOB, depDateTime, infantAgeLowerBoundaryInDays)) {
									throw new ModuleException(MessageCodes.PAX_INFANT_DOB_MIN_RANGE);
								}
							}
						}
					}

					if (addSegment || modifySegment) {
						inAncillaryAvailabilityDTO.setAirportServiceCountMap(SSRServicesUtil
								.extractAiportServiceCountFromJsonObj(colpaxs));
					}
				}

				if (foidIds.length() > 0 && foidIds != null && segmentList.length > 0) {
					String returnData = ReservationUtil.getUniqueFoidData(foidIds, foidCode, segmentList, log, null);
					if (returnData != "true") {
						throw new XBERuntimeException(WebConstants.DUPLICATE_PASSPORT_FOR_SEGMENT);
					}
				}

				if (addSegment && isRakInsurance) {
					skipInsurance = true;
				}

				boolean insCarrierIncluded = false;// FIX : temporary hack for insurance in add dry/interline segment by
				// MD
				if (insFltSegBldr.getFlightSegment() != null && fltSegApplicableToIns != null && !fltSegApplicableToIns.isEmpty()) {
					for (FlightSegmentTO flightSegmentTO : fltSegApplicableToIns) {
						if (flightSegmentTO.getOperatingAirline().equals(insFltSegBldr.getFlightSegment().getOperatingAirline())) {
							insCarrierIncluded = true;
							break;
						}
					}
				}

				if (!insCarrierIncluded) {
					skipInsurance = true;
				}

				if (log.isDebugEnabled()) {
					log.debug("Transaction identifier : " + strTxnIdntifier);
				}

				if (!skipInsurance && insFltSegBldr.getInsurableFlightSegments().size() > flightSegmentTOs.size()) {
					insFltSegIncluded = true;
				}

				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.SEAT_MAP);

				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.MEALS);

				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.AIRPORT_SERVICE);

				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.AIRPORT_TRANSFER);

				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.FLEXI);

				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.AUTOMATIC_CHECKIN);

				if (!skipInsurance && insFltSegBldr.hasFlightSegments()
						&& hasPrivilege(request, system, LCCAncillaryType.INSURANCE)) {
					inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.INSURANCE);
					inAncillaryAvailabilityDTO.setOnHold(isOnHoldBooking());
					inAncillaryAvailabilityDTO.setInsuranceOrigin(insFltSegBldr.getOriginAirport());
				}

			} else {

				disableAllAncilaryAvailability();

			}

			/**
			 * Adding baggaes
			 */
			if ((AppSysParamsUtil.isEnableBaggageForStandby() && stanby) || !stanby) {
				baggageAvailable = true;
			}

			if (hasPrivilege(request, system, LCCAncillaryType.BAGGAGE) && baggageAvailable) {
				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.BAGGAGE);
				inAncillaryAvailabilityDTO.setBaggageSummaryTo(resBaggageSummary);
			}

			/**
			 * Adding SSR
			 */

			if ((AppSysParamsUtil.isEnableSSRForStandby() && stanby) || !stanby) {
				ssrAvailable = true;
			}

			if (hasPrivilege(request, system, LCCAncillaryType.SSR) && ssrAvailable) {
				inAncillaryAvailabilityDTO.addAncillaryType(LCCAncillaryType.SSR);
			}

			if (insFltSegIncluded) {
				inAncillaryAvailabilityDTO.addAllFlightSegmentTOs(insFltSegBldr.getInsurableFlightSegments());
			} else {
				inAncillaryAvailabilityDTO.addAllFlightSegmentTOs(flightSegmentTOs);
			}
			inAncillaryAvailabilityDTO.setTransactionIdentifier(strTxnIdntifier);
			inAncillaryAvailabilityDTO.setQueryingSystem(system);

			inAncillaryAvailabilityDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
			inAncillaryAvailabilityDTO.setModifyAncillary(modifyAncillary);

			List<FlightSegmentTO> fltSegForServiceTax = null;
			if (isRequote) {
				fltSegForServiceTax = fltSegApplicableToIns;
			} else {
				fltSegForServiceTax = flightSegmentTOs;
			}

			String departureSegmentCode = com.isa.thinair.webplatform.api.util.ReservationUtil
					.getDepartureSegmentCode(fltSegForServiceTax);
			inAncillaryAvailabilityDTO.setDepartureSegmentCode(departureSegmentCode);

			if (system == SYSTEM.AA) {
				hasPrivAddSeatsInCutover = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ADD_SEATS_IN_CUTOVER);
				hasPrivEditSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_SEAT_UPDATE);
				hasPrivSSRChange = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_SSR_CHANGE);
				hasPrivModifySSRWithinBuffer = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_MODIFY_SSR_WITHIN_BUFFER);
			}
			inAncillaryAvailabilityDTO.setEnableModifySSRWithinBuffer(hasPrivModifySSRWithinBuffer);

			if (log.isDebugEnabled()) {
				StringBuilder ancillariesToQuery = new StringBuilder();
				for (LCCAncillaryType ancillaryType : inAncillaryAvailabilityDTO.getAncillaryTypes()) {
					ancillariesToQuery.append(ancillaryType.getAncillaryType());
					ancillariesToQuery.append(",\t");
				}

				log.debug("Checking for the following ancillaries on participating airlines : " + ancillariesToQuery.toString());
			}

			Collections.sort(inAncillaryAvailabilityDTO.getFlightDetails());

			if (inAncillaryAvailabilityDTO.getAncillaryTypes().size() > 0
					&& inAncillaryAvailabilityDTO.getFlightDetails() != null
					&& inAncillaryAvailabilityDTO.getFlightDetails().size() > 0) {
				AnciAvailabilityRS anciAvailabilityRS = ModuleServiceLocator.getAirproxyAncillaryBD().getAncillaryAvailability(
						inAncillaryAvailabilityDTO, TrackInfoUtil.getBasicTrackInfo(request));
				availabilityOutDTO = anciAvailabilityRS.getLccAncillaryAvailabilityOutDTOs();

				bookingShoppingCart.addServiceTax(anciAvailabilityRS.isJnTaxApplicable(), anciAvailabilityRS.getTaxRatio(),
						EXTERNAL_CHARGES.JN_ANCI);

				if (system == SYSTEM.AA)
					availabilityOutDTO = checkPrivilegesAndBufferTimes(availabilityOutDTO, modifyAncillary, stanby);
			} else {
				availabilityOutDTO = composeEmptyResponse(inAncillaryAvailabilityDTO);
			}

		
			if (!skipInsurance && insFltSegBldr.hasFlightSegments()) {
				insuranceAvailable = checkInsuranceAvailability(availabilityOutDTO, insFltSegBldr.getOriginFltRefNo());
			} else {
				insuranceAvailable = false;
			}
			if (insFltSegIncluded) {
				availabilityOutDTO = stripInsuranceFltSegs(availabilityOutDTO, flightSegmentTOs);
			}

			// Disable ancillaries for ground segments
			disableAnciForGroundSegments(availabilityOutDTO);

			seatMapAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.SEAT_MAP);

			mealsAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.MEALS);

			airportServiceAvailable = checkAirportLevelAvailability(availabilityOutDTO, flightSegmentTOs,
					LCCAncillaryType.AIRPORT_SERVICE);

			airportTransferAvailable = checkAirportLevelAvailability(availabilityOutDTO, flightSegmentTOs,
					LCCAncillaryType.AIRPORT_TRANSFER);

			ssrAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.SSR);

			if (AppSysParamsUtil.isONDBaggaeEnabled()) {
				baggageAvailable = isOndBaggageActive(availabilityOutDTO, LCCAncillaryType.BAGGAGE);
			} else {
				baggageAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.BAGGAGE);
			}

			anciPreferenceMsgEnabled = AppSysParamsUtil.isAncillaryPreferenceMessageEnabled();

			autoCheckinAvailable = anciActiveInLeastOneSeg(availabilityOutDTO, LCCAncillaryType.AUTOMATIC_CHECKIN);
			
			if (autoCheckinAvailable) {
				String paxType = globalConfig.getpaxConfig(AppIndicatorEnum.APP_XBE.toString()).stream().findFirst().get().getPaxCatCode();
				segWisePaxConfigJson = AutoCheckinUtil.getPaxWiseAutoCheckinConfig(flightSegmentTOs,
						TrackInfoUtil.getBasicTrackInfo(request), getTrackInfo(), paxType, system.name());
				paxValidation = new JSONObject();
				paxValidation.put(ADULT_AGE_CUT_OVER_YEARS, adultCutOffAge);
				paxValidation.put(CHILD_AGE_CUT_OVER_YEARS, childCutOffAge);
				paxValidation.put(INFANT_AGE_CUT_OVER_YEARS, infantCutOffAge);
				segWisePaxConfigJson.put(PAX_VALIDATION, paxValidation);
				setAutoCheckinPaxConfig(segWisePaxConfigJson.toString());
			}

			// Only ssr are allowed to modify
			if (gdsPnr) {
				insuranceAvailable = false;
				seatMapAvailable = false;
				mealsAvailable = false;
				airportServiceAvailable = false;
				airportTransferAvailable = false;
				baggageAvailable = false;
				anciPreferenceMsgEnabled = false;
				autoCheckinAvailable = false;
			}

			/*if (isOpenReturnReservation()) {
				airportTransferAvailable = false;
			}*/

			baggageMandatory = resParm != null ? resParm.isBaggageMandatory() : AppSysParamsUtil.isBaggageMandatory();

			baggageChargeOverride = hasBaggageChargeOverridePrivilege(request);

			focMealEnabled = AppSysParamsUtil.isFOCMealSelectionEnabled();
			
			isBundleFarePopupEnabled = AppSysParamsUtil.isBundleFarePopupEnabled(ApplicationEngine.XBE);
			
			flexiSelectionEnabledInAnciPage = AppSysParamsUtil.isFlexiEnabledInAnci();

			populateCarrierWiseSSRInvCheckMap(availabilityOutDTO);
			
			Charge ssrCharge = ModuleServiceLocator.getChargeBD().getCharge("SSR");
			if (ssrCharge.getRefundableChargeFlag() == 0 && ssrCharge.getRefundableOnlyforModifications().equals("N")){
				isSsrRefundableInModification = false;
			} else {
				isSsrRefundableInModification = true;
			}
		} catch (Exception e) {
			if (e instanceof XBERuntimeException) {
				if (((XBERuntimeException) e).getExceptionCode().equals(WebConstants.NO_OUTBOUND_FOUND)) {
					disableAllAncilaryAvailability();
					log.error(e);
				} else {
					success = false;
					messageTxt = BasicRH.getErrorMessage(e,userLanguage);
					log.error(messageTxt, e);
				}
			} else {
				success = false;
				messageTxt = BasicRH.getErrorMessage(e,userLanguage);
				log.error(messageTxt, e);
			}
		}
		return result;
	}

	private void populateCarrierWiseSSRInvCheckMap(List<LCCAncillaryAvailabilityOutDTO> availDTOList) {
		if (availDTOList != null && availDTOList.size() > 0) {
			for (LCCAncillaryAvailabilityOutDTO availDTO : availDTOList) {
				FlightSegmentTO flightSegmentTO = availDTO.getFlightSegmentTO();
				if (!carrierWiseSSRInvCheck.containsKey(flightSegmentTO.getOperatingAirline())) {
					carrierWiseSSRInvCheck.put(flightSegmentTO.getOperatingAirline(), flightSegmentTO.isSsrInventoryCheck());
				}
			}
		}
	}

	private void disableAnciForGroundSegments(List<LCCAncillaryAvailabilityOutDTO> availDTO) {
		if (availDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO anciAvail : availDTO) {
				if (anciAvail.getFlightSegmentTO() != null) {
					try {
						if (ModuleServiceLocator.getAirportBD().isBusSegment(anciAvail.getFlightSegmentTO().getSegmentCode())) {
							List<LCCAncillaryStatus> ancillaryStatusList = anciAvail.getAncillaryStatusList();
							if (ancillaryStatusList != null) {
								for (LCCAncillaryStatus lccAncillaryStatus : ancillaryStatusList) {
									if (!LCCAncillaryType.INSURANCE.equals(lccAncillaryStatus.getAncillaryType()
											.getAncillaryType())) {
										lccAncillaryStatus.setAvailable(false);
									}
								}
							}
						}
					} catch (ModuleException e) {
						log.error("Error in disabling Anci for ground seg", e);
					}
				}
			}
		}
	}

	private void disableAllAncilaryAvailability() {
		availabilityOutDTO = composeEmptyResponse();
		success = true;
		insuranceAvailable = false;
		seatMapAvailable = false;
		baggageAvailable = false;
		ssrAvailable = false;
		focMealEnabled = false;
		mulipleMealsSelectionEnabled = false;
		baggageMandatory = false;
		flexiSelectionEnabledInAnciPage = false;
		autoCheckinAvailable = false;
	}

	private void removeStandbySegments(Collection<FlightSegmentTO> flightSegmentTOs) {
		Collection<FlightSegmentTO> withoutStandBy = new ArrayList<FlightSegmentTO>();
		if (flightSegmentTOs != null) {
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (!BookingClass.BookingClassType.STANDBY.equals(flightSegmentTO.getBookingType())) {
					withoutStandBy.add(flightSegmentTO);
				}
			}
		}
		flightSegmentTOs.clear();
		flightSegmentTOs.addAll(withoutStandBy);
	}

	private boolean isStandby(FlightSearchDTO searchParams) {
		boolean isStandBy = false;
		if (searchParams != null) {
			if (BookingClass.BookingClassType.STANDBY.equals(searchParams.getBookingType())) {
				isStandBy = true;
			}
		}
		return isStandBy;
	}

	private boolean isStandby(Collection<FlightSegmentTO> flightSegmentTOs) {
		boolean isStandBy = true;
		if (flightSegmentTOs != null) {
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (!BookingClass.BookingClassType.STANDBY.equals(flightSegmentTO.getBookingType())) {
					isStandBy = false;
					break;
				}
			}
		}
		return isStandBy;
	}

	private boolean hasPrivilege(HttpServletRequest request, SYSTEM system, String anciType) {
		if (system == SYSTEM.AA) {
			if (anciType.equals(LCCAncillaryType.SEAT_MAP)) {
				return true;
			} else if (anciType.equals(LCCAncillaryType.MEALS)) {
				return true;
			} else if (anciType.equals(LCCAncillaryType.BAGGAGE)) {
				// return true;
				return BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_BAGGAGE_SHOW);
			} else if (anciType.equals(LCCAncillaryType.INSURANCE)) {
				boolean hasInsurancePrivilege = false;
				if (isOnHoldBooking()) {
					hasInsurancePrivilege = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_INSURANCE_OHD);
				} else {
					hasInsurancePrivilege = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_INSURANCE);
				}
				return hasInsurancePrivilege;
			} else if (anciType.equals(LCCAncillaryType.SSR)) {
				return BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_SSR_ADD);
			}
		} else {
			return true;
		}
		return false;
	}

	private boolean hasBaggageChargeOverridePrivilege(HttpServletRequest request) {
		return BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_BAGGAGE_CHARGE_OVERRIDE);
	}

	private List<LCCAncillaryAvailabilityOutDTO> stripInsuranceFltSegs(List<LCCAncillaryAvailabilityOutDTO> availOut,
			List<FlightSegmentTO> flightSegmentTOs) {
		List<LCCAncillaryAvailabilityOutDTO> availNewOut = new ArrayList<LCCAncillaryAvailabilityOutDTO>();
		for (LCCAncillaryAvailabilityOutDTO avail : availOut) {
			for (FlightSegmentTO fltSeg : flightSegmentTOs) {
				if (fltSeg.getFlightRefNumber().equals(avail.getFlightSegmentTO().getFlightRefNumber())
						&& fltSeg.getSegmentSequence() == avail.getFlightSegmentTO().getSegmentSequence()) {
					availNewOut.add(avail);
				}
			}
		}
		return availNewOut;
	}

	private List<LCCAncillaryAvailabilityOutDTO> composeEmptyResponse(LCCAncillaryAvailabilityInDTO inAncillaryAvailabilityDTO) {
		List<LCCAncillaryAvailabilityOutDTO> outDTOs = new ArrayList<LCCAncillaryAvailabilityOutDTO>();
		if (inAncillaryAvailabilityDTO != null) {
			for (FlightSegmentTO fltSegment : inAncillaryAvailabilityDTO.getFlightDetails()) {
				LCCAncillaryAvailabilityOutDTO outDTO = new LCCAncillaryAvailabilityOutDTO();
				outDTO.setFlightSegmentTO(fltSegment);
				outDTO.setAncillaryStatusList(new ArrayList<LCCAncillaryStatus>());
			}
		}
		return outDTOs;
	}

	private List<LCCAncillaryAvailabilityOutDTO> composeEmptyResponse() {
		List<LCCAncillaryAvailabilityOutDTO> outDTOs = new ArrayList<LCCAncillaryAvailabilityOutDTO>();

		LCCAncillaryAvailabilityOutDTO outDTO = new LCCAncillaryAvailabilityOutDTO();
		outDTO.setAncillaryStatusList(new ArrayList<LCCAncillaryStatus>());

		return outDTOs;
	}

	// FIXME remove this block and refactor once the modify segment ancillary is ready for interline
	private boolean addAncillary(SYSTEM system, boolean modifySegment) {
		if (system == SYSTEM.INT && modifySegment) {
			return false;
		} else {
			return true;
		}
	}

	private List<LCCAncillaryAvailabilityOutDTO> checkPrivilegesAndBufferTimes(
			List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, boolean edit, boolean isStandBy) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				Date departureDate = FlightRefNumberUtil.getDepartureDateFromFlightRPH(ancillaryAvailabilityOutDTO
						.getFlightSegmentTO().getFlightRefNumber());
				String airportCode = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getSegmentCode().split("/")[0];
				boolean isDomesticFlight = ancillaryAvailabilityOutDTO.getFlightSegmentTO().isDomesticFlight();
				Date depatureDateZulu = null;
				try {
					depatureDateZulu = DateUtil.getZuluDateTime(departureDate, airportCode);
				} catch (ModuleException e) {
					depatureDateZulu = departureDate;
				}
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					// In airport services cut-over time is already checked @ the time of querying available services
					// In Automatic checkin services cut-over time is already checked @ the time of querying available
					// services
					String ancilaryType = ancillaryStatus.getAncillaryType().getAncillaryType();
					if (!(LCCAncillaryType.AIRPORT_SERVICE.equals(ancilaryType))
							&& !(LCCAncillaryType.AUTOMATIC_CHECKIN.equals(ancilaryType))) {
						ancillaryStatus.setAvailable(ancillaryStatus.isAvailable() && isEnableService(
										ancillaryStatus.getAncillaryType().getAncillaryType(), depatureDateZulu, edit,
										isStandBy
												? isStandBy
												: BookingClass.BookingClassType.STANDBY.equals(
														ancillaryAvailabilityOutDTO.getFlightSegmentTO().getBookingType()),
										isDomesticFlight, ancillaryStatus.isAvailable()));
					}

				}
			}
		}
		return availabilityOutDTO;
	}

	private List<SpecialServiceRequestDTO> getApplicableSSRDTOs(List<SpecialServiceRequestDTO> ssrDTOs, boolean isInEditMode,
			String anciType) {
		if (isInEditMode && anciType.equals(LCCAncillaryType.SSR) && hasPrivSSRChange) {
			List<SpecialServiceRequestDTO> ssrList = new ArrayList<SpecialServiceRequestDTO>();
			if (ssrDTOs != null) {
				long diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();
				for (SpecialServiceRequestDTO ssr : ssrDTOs) {
					if (ssr.getCutOffTime() != null) {
						ssrList.add(ssr);
					}
				}
			}
			return ssrList;
		}
		return null;
	}

	// isMaintainSSRWiseCutoffTime

	private boolean anciActiveInLeastOneSeg(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					if ((ancillaryStatus.isAvailable())
							&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean isOndBaggageActive(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		if (availabilityOutDTO != null) {
			Map<String, Boolean> ondGrpStatus = new HashMap<String, Boolean>();
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {

				// Surface / Ground / Bus segments are not considered for OND baggages.
				if (ancillaryAvailabilityOutDTO.getFlightSegmentTO().isSurfaceSegment()) {
					continue;
				}

				String ondGroupId = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getBaggageONDGroupId();
				boolean status = false;
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					if ((ancillaryStatus.isAvailable())
							&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))) {
						status = true;
						break;
					}
				}
				if (!ondGrpStatus.containsKey(ondGroupId)) {
					ondGrpStatus.put(ondGroupId, status);
				} else {
					if (ondGroupId == null) {
						if (ondGrpStatus.containsKey(ondGroupId) && !ondGrpStatus.get(ondGroupId) && status) {
							ondGrpStatus.put(ondGroupId, status);
						}
					} else {
						ondGrpStatus.put(ondGroupId, ondGrpStatus.get(ondGroupId) && status);
					}
				}
			}
			for (String groupId : ondGrpStatus.keySet()) {
				if (ondGrpStatus.get(groupId)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkInsuranceAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String fltRefNo) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (FlightRefNumberUtil.getSegmentIdFromFlightRPH(
						ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber()).intValue() == FlightRefNumberUtil
						.getSegmentIdFromFlightRPH(fltRefNo).intValue()) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(LCCAncillaryType.INSURANCE))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
					break;
				}
			}
		}
		return false;
	}

	@Deprecated
	private InsuranceFltSegBuilder removeGroundSegmentFromInsurance(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			InsuranceFltSegBuilder insFltSegBldr, boolean isGroundServiceEnabled) throws ModuleException {

		if (isGroundServiceEnabled) {
			List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
			Iterator<FlightSegmentTO> flightSegItr = insFltSegBldr.getInsurableFlightSegments().iterator();
			int surfaceSegmentCount = 0;
			while (flightSegItr.hasNext()) {

				FlightSegmentTO flightSegmentTO = flightSegItr.next();

				if (availabilityOutDTO != null && flightSegmentTO != null) {
					for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
						if (FlightRefNumberUtil.getSegmentIdFromFlightRPH(
								ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber()).intValue() == FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(flightSegmentTO.getFlightRefNumber()).intValue()) {

							if (ancillaryAvailabilityOutDTO.getFlightSegmentTO().isSurfaceSegment()) {
								surfaceSegmentCount++;
							} else {
								flightSegments.add(flightSegmentTO);
							}

						}
					}
				}
			}

			if (surfaceSegmentCount == availabilityOutDTO.size()) {
				insFltSegBldr = new InsuranceFltSegBuilder(flightSegments);
			}

		}
		return insFltSegBldr;
	}

	private boolean checkAirportLevelAvailability(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO,
			List<FlightSegmentTO> flightSegmentTOs, String ancilaryType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				if (isAvailableForSelecetedSegment(ancillaryAvailabilityOutDTO.getFlightSegmentTO().getFlightRefNumber(),
						flightSegmentTOs)) {
					for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
						if ((ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(ancilaryType))
								&& ancillaryStatus.isAvailable()) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	private boolean isAvailableForSelecetedSegment(String fltRefNo, List<FlightSegmentTO> flightSegmentTOs) {
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getFlightRefNumber().equals(fltRefNo)) {
				return true;
			}
		}

		return false;
	}

	private boolean isSSREnabled(String anciType,boolean isInEditMode){
		if (isInEditMode && anciType.equals(LCCAncillaryType.SSR) && !hasPrivSSRChange) {
			return false;
		}
		return true;
	}
	
	private boolean isEnableService(String anciType, Date departDate, boolean isInEditMode, boolean isStandBy,
			boolean isDomesticFlight, boolean isServiceEnabled) {

		// FIXME: App parameter check for cutovers is wrong here. Operating carrier behaviour will be overriden
		// if marketing carrier cutovers are taken from app params.

		if (isInEditMode && anciType.equals(LCCAncillaryType.SEAT_MAP) && !hasPrivEditSeats) {
			return false;
		}
		if (isInEditMode && anciType.equals(LCCAncillaryType.SSR) && !hasPrivSSRChange) {
			return false;
		}
		if(anciType.equals(LCCAncillaryType.SSR) && AppSysParamsUtil.isMaintainSSRWiseCutoffTime()){
			return isServiceEnabled;
		}
		
		// no cutover or privileges limiting insurance add/modification
		if (anciType.equals(LCCAncillaryType.INSURANCE)) {
			return true;
		}

		// We don't have a meal change privilege as of now. Only privilege level check should be validated here.
		// Checking app parameters for cutovers, should be done in the backend, which is working for meals.
		// Hence it should always return true.
		if (anciType.equals(LCCAncillaryType.MEALS)) {
			if (!isStandBy) {
				return true;
			} else {
				return false;
			}
		}

		long diffMils = 3600000;
		long lngBufferTime = 0;
		Calendar currentTime = Calendar.getInstance();
		long modificationStopBuffer = AppSysParamsUtil.getInternationalBufferDurationInMillis();

		if (anciType.equals(LCCAncillaryType.SEAT_MAP)) {
			diffMils = AppSysParamsUtil.getXBESeatmapStopCutoverInMillis();
		} else if (anciType.equals(LCCAncillaryType.MEALS)) {
			diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
		} else if (anciType.equals(LCCAncillaryType.BAGGAGE)) {
			diffMils = BaggageTimeWatcher.getBaggageCutOverTimeForXBEInMillis(isInEditMode,
					BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER),
					isStandBy);
		} else if (anciType.equals(LCCAncillaryType.SSR)) {
			diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();
		} else if (anciType.equals(LCCAncillaryType.INSURANCE)) {
			diffMils = lngBufferTime;
		} else if (anciType.equals(LCCAncillaryType.AIRPORT_TRANSFER)) {
			if (isDomesticFlight) {
				diffMils = AppSysParamsUtil.getAirportTransferStopCutoverForDomesticInMillis();
			} else {
				diffMils = AppSysParamsUtil.getAirportTransferStopCutoverForInternationalInMillis();
			}
		}

		if (isInEditMode) {
			if (anciType.equals(LCCAncillaryType.BAGGAGE) || anciType.equals(LCCAncillaryType.AIRPORT_TRANSFER)) {
				if (departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
					return false;
				} else {
					return true;
				}
			} else if ((departDate.getTime() - modificationStopBuffer) < currentTime.getTimeInMillis()) {
				if (anciType.equals(LCCAncillaryType.SSR) && hasPrivModifySSRWithinBuffer) {
					return true;
				} else if (anciType.equals(LCCAncillaryType.SEAT_MAP)) {
					if(departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
						if(hasPrivAddSeatsInCutover && departDate.getTime() > currentTime.getTimeInMillis()){
							return true;
						} else {
							return false;
						}
					} else {
						return true;
					}	
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else if (departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
			if (anciType.equals(LCCAncillaryType.SEAT_MAP) && hasPrivAddSeatsInCutover) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}


	private List<FlightSegmentTO> removeDuplicateflightSegments(List<FlightSegmentTO> insFlightSegmnts) {
		List<FlightSegmentTO> flightSegmentTOs = null;
		if (insFlightSegmnts != null) {
			flightSegmentTOs = new ArrayList<FlightSegmentTO>();
			for (FlightSegmentTO flightSegmentTO : insFlightSegmnts) {
				boolean fltExists = false;
				for (FlightSegmentTO innerFltSeg : flightSegmentTOs) {
					if (innerFltSeg.getFlightRefNumber().equals(flightSegmentTO.getFlightRefNumber())) {
						fltExists = true;
						break;
					}
				}

				if (!fltExists) {
					flightSegmentTOs.add(flightSegmentTO);
				}
			}
		}

		return flightSegmentTOs;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @return the gdsPnr
	 */
	public boolean isGdsPnr() {
		return gdsPnr;
	}

	/**
	 * @param gdsPnr
	 *            the gdsPnr to set
	 */
	public void setGdsPnr(boolean gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public boolean isSeatMapAvailable() {
		return seatMapAvailable;
	}

	public boolean isMealsAvailable() {
		return mealsAvailable;
	}

	public boolean isSsrAvailable() {
		return ssrAvailable;
	}

	public void setInsuranceAvailable(boolean insuranceAvailable) {
		this.insuranceAvailable = insuranceAvailable;
	}

	public boolean isInsuranceAvailable() {
		return insuranceAvailable;
	}

	public boolean isAirportServiceAvailable() {
		return airportServiceAvailable;
	}

	public boolean isAirportTransferAvailable() {
		return airportTransferAvailable;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public List<LCCAncillaryAvailabilityOutDTO> getAvailabilityOutDTO() {
		return availabilityOutDTO;
	}

	public boolean isMulipleMealsSelectionEnabled() {
		return mulipleMealsSelectionEnabled;
	}

	public void setMulipleMealsSelectionEnabled(boolean mulipleMealsSelectionEnabled) {
		this.mulipleMealsSelectionEnabled = mulipleMealsSelectionEnabled;
	}

	public boolean isOnHoldBooking() {
		return onHoldBooking;
	}

	public void setOnHoldBooking(boolean onHoldBooking) {
		this.onHoldBooking = onHoldBooking;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setSelectedSystem(String selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setFoidIds(String foidIds) {
		this.foidIds = foidIds;
	}

	public void setFoidCode(String foidCode) {
		this.foidCode = foidCode;
	}

	public void setSegmentList(Integer[] segmentList) {
		this.segmentList = segmentList;
	}

	public void setInsFltSegments(String insFltSegments) {
		this.insFltSegments = insFltSegments;
	}

	public boolean isInsuranceViewOnly() {
		return insuranceViewOnly;
	}

	public void setInsuranceViewOnly(boolean insuranceViewOnly) {
		this.insuranceViewOnly = insuranceViewOnly;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public String getResPaxInfo() {
		return resPaxInfo;
	}

	/**
	 * @return the anciPreferenceMsgEnabled
	 */
	public boolean isAnciPreferenceMsgEnabled() {
		return anciPreferenceMsgEnabled;
	}

	/**
	 * @param anciPreferenceMsgEnabled
	 *            the anciPreferenceMsgEnabled to set
	 */
	public void setAnciPreferenceMsgEnabled(boolean anciPreferenceMsgEnabled) {
		this.anciPreferenceMsgEnabled = anciPreferenceMsgEnabled;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public boolean isFocMealEnabled() {
		return focMealEnabled;
	}

	public void setFocMealEnabled(boolean focMealEnabled) {
		this.focMealEnabled = focMealEnabled;
	}

	/**
	 * @return the baggageAvailable
	 */
	public boolean isBaggageAvailable() {
		return baggageAvailable;
	}

	/**
	 * @param baggageAvailable
	 *            the baggageAvailable to set
	 */
	public void setBaggageAvailable(boolean baggageAvailable) {
		this.baggageAvailable = baggageAvailable;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public String getModifyingSegmentsIds() {
		return modifyingSegmentsIds;
	}

	public void setModifyingSegmentsIds(String modifyingSegmentsIds) {
		this.modifyingSegmentsIds = modifyingSegmentsIds;
	}

	public boolean isBaggageMandatory() {
		return baggageMandatory;
	}

	public void setBaggageMandatory(boolean baggageMandatory) {
		this.baggageMandatory = baggageMandatory;
	}

	public boolean isBaggageChargeOverride() {
		return baggageChargeOverride;
	}

	public void setBaggageChargeOverride(boolean baggageChargeOverride) {
		this.baggageChargeOverride = baggageChargeOverride;
	}

	public Map<String, Boolean> getCarrierWiseSSRInvCheck() {
		return carrierWiseSSRInvCheck;
	}

	public void setCarrierWiseSSRInvCheck(Map<String, Boolean> carrierWiseSSRInvCheck) {
		this.carrierWiseSSRInvCheck = carrierWiseSSRInvCheck;
	}

	/**
	 * @param lccMap
	 *            the lccMap to set
	 */
	public void setLccMap(Map<String, LogicalCabinClassDTO> lccMap) {
		this.lccMap = lccMap;
	}

	/**
	 * @return the lccMap
	 */
	public Map<String, LogicalCabinClassDTO> getLccMap() {
		return lccMap;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public boolean isWaitListingBooking() {
		return waitListingBooking;
	}

	public void setWaitListingBooking(boolean waitListingBooking) {
		this.waitListingBooking = waitListingBooking;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public boolean isFlexiSelectionEnabledInAnciPage() {
		return flexiSelectionEnabledInAnciPage;
	}

	public void setFlexiSelectionEnabledInAnciPage(boolean flexiSelectionEnabledInAnciPage) {
		this.flexiSelectionEnabledInAnciPage = flexiSelectionEnabledInAnciPage;
	}

	public boolean isOpenReturnReservation() {
		return new Boolean(isOpenReturnReservation);
	}

	public void setIsOpenReturnReservation(String isOpenReturnReservation) {
		this.isOpenReturnReservation = isOpenReturnReservation;
	}
	
	/**
	 * @return the isSsrRefundableInModification
	 */
	public boolean isSsrRefundableInModification() {
		return isSsrRefundableInModification;
	}

	/**
	 * @param isSsrRefundableInModification the isSsrRefundableInModification to set
	 */
	public void setSsrRefundableInModification(boolean isSsrRefundableInModification) {
		this.isSsrRefundableInModification = isSsrRefundableInModification;
	}

	/**
	 * @return the isBundleFarePopupEnabled
	 */
	public boolean isBundleFarePopupEnabled() {
		return isBundleFarePopupEnabled;
	}

	/**
	 * @param isBundleFarePopupEnabled the isBundleFarePopupEnabled to set
	 */
	public void setBundleFarePopupEnabled(boolean isBundleFarePopupEnabled) {
		this.isBundleFarePopupEnabled = isBundleFarePopupEnabled;
	}

	/**
	 * @return the autoCheckinAvailable
	 */
	public boolean isAutoCheckinAvailable() {
		return autoCheckinAvailable;
	}

	/**
	 * @param autoCheckinAvailable the autoCheckinAvailable to set
	 */
	public void setAutoCheckinAvailable(boolean autoCheckinAvailable) {
		this.autoCheckinAvailable = autoCheckinAvailable;
	}

	/**
	 * @return the autoCheckinPaxConfig
	 */
	public String getAutoCheckinPaxConfig() {
		return autoCheckinPaxConfig;
	}

	/**
	 * @param autoCheckinPaxConfig
	 *            the autoCheckinPaxConfig to set
	 */
	public void setAutoCheckinPaxConfig(String autoCheckinPaxConfig) {
		this.autoCheckinPaxConfig = autoCheckinPaxConfig;
	}

}
