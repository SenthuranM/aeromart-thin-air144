package com.isa.thinair.xbe.core.web.handler.reporting;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.system.GraphHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class AgentTransactionRH extends BasicRH {

	private static Log log = LogFactory.getLog(AgentTransactionRH.class);

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AgentTransactionRH execute Failed " + e.getMessageString());
			forward = S2Constants.Result.ERROR;
		} catch (Exception e) {
			log.error("Error in AgentTransactionRH execute()" + e.getMessage());
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {

		GraphHTMLGenerator.setGraphContent(request);

	}

}
