package com.isa.thinair.xbe.core.web.action.travagent;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.travagent.EmailOutStanBalanceRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.EMAIL_OUTSTANDING_BALANCE)
public class ShowEmailOutstandingBalanceAction extends BaseRequestResponseAwareAction {

	public String execute() {
		return EmailOutStanBalanceRequestHandler.execute(request, response);
	}

}
