package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.googlecode.jsonplugin.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class DerivePGWConfigAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(DerivePGWConfigAction.class);

	private String pgwId;

	private String selectedCurrency;

	private String pgwBrokerType;

	private boolean success = true;

	private String messageTxt;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO;
		Properties ipgProps;
		String brokerType = null;

		try {
			ipgIdentificationParamsDTO = WebplatformUtil.prepareIPGConfigurationParamsDTOPerPGWId(new Integer(pgwId));
			ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
			brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
		} catch (NumberFormatException | ModuleException e) {
			log.error("Error while retrieving payment gateway type : " + e);
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			success = false;
		}
		if (brokerType != null) {
			pgwBrokerType = brokerType;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String getPgwId() {
		return pgwId;
	}

	public void setPgwId(String pgwId) {
		this.pgwId = pgwId;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getPgwBrokerType() {
		return pgwBrokerType;
	}

	public void setPgwBrokerType(String pgwBrokerType) {
		this.pgwBrokerType = pgwBrokerType;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}
}
