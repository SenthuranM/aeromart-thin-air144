package com.isa.thinair.xbe.core.web.v2.generator.reservation;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class BlacklistedPaxResHG {

	private static String templateclientErrors;

	public static String getTemplateClientErrors(HttpServletRequest request) throws ModuleException {

		if (templateclientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("cc.airadmin.edit.success", "updateSuccess");
			moduleErrs.setProperty("cc_common", "updateError");
			moduleErrs.setProperty("cc_flight_status_null", "actionReq");
			moduleErrs.setProperty("um.blacklist.pax.res.data.Reason.req", "reasonReqForWL");
			moduleErrs.setProperty("cc_resmod_select_pax", "rowReq");
			moduleErrs.setProperty("cc_fromdate_exeeeds_ToDate", "fromGrtTo");
			moduleErrs.setProperty("um.report.fromdate.empty", "fromDateReq");
			moduleErrs.setProperty("um.report.todate.empty", "toDateReq");
						
			templateclientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return templateclientErrors;
	}

}
