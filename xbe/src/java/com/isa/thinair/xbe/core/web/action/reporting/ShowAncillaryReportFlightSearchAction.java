package com.isa.thinair.xbe.core.web.action.reporting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.flight.FlightHTMLGenerator;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.ANCILLARY_DETAILS_FLIGHT_REPORT),
		@Result(name = S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX, value = S2Constants.Jsp.Common.LOADAJAX_JSP),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowAncillaryReportFlightSearchAction extends BaseRequestResponseAwareAction {

	private static String PARAM_TIMEMODE = "";
	private static Log log = LogFactory.getLog(ShowAncillaryReportFlightSearchAction.class);

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String strMode = request.getParameter("hdnMode");

		if (strMode != null && strMode.equals("SEARCH")) {
			// if SEARCH, ajax cal will proceed
			forward = S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
		}
		try {
			setDisplayData(request, response);
		} catch (ModuleException moduleException) {
			log.error("Exception in ShowAncillaryFlightSearchAction.execute - setDisplayData(request) [origin module="
					+ moduleException.getModuleDesc() + "]", moduleException);

			if (moduleException.getExceptionCode().equals("reporting.date.exceeds.max.duration")) {
				forward = S2Constants.Result.SUCCESS;
			}

		} catch (Exception e) {
			log.error("Exception in ShowAncillaryFlightSearchAction.execute - setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		setClientErrors(request);
		setStatusHtml(request);
		setFlightRowHtml(request, response);
		setOnlineAirportHtml(request);
		setInterlineCarrierCodeHtml(request);
		setOperationTypeHtml(request);
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = FlightHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static void setStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightStatus();
		request.getSession().setAttribute(WebConstants.SES_HTML_STATUS_DATA, strHtml);
	}

	private static void setFlightRowHtml(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
		String strReturnData = htmlGen.getFlightRowHtml(request, response);
		request.setAttribute("returnData", strReturnData);
	}

	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {
		Map<String, String> airports = null;
		airports = SelectListGenerator.createAirportsWithoutSubstations(false, null, null);

		StringBuffer sb = new StringBuffer();
		List<String> list = new ArrayList<String>(airports.keySet());
		Collections.sort(list);
		for (String airportCode : list) {
			sb.append("<option value='" + airportCode + "'>" + airportCode + "</option>");
		}

		request.getSession().setAttribute(WebConstants.SES_HTML_AIRPORT_DATA, sb.toString());
	}

	private static void setInterlineCarrierCodeHtml(HttpServletRequest request) {
		String strHtml = SelectListGenerator.createInterlineCarrierSelectListOptions(null);
		request.setAttribute(WebConstants.REQ_CARRIER_CODE_LIST_OPTIONS, strHtml);
	}

	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationType();
		request.getSession().setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);
	}

}
