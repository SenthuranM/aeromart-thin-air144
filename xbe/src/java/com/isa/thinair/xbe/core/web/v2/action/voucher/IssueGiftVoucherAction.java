package com.isa.thinair.xbe.core.web.v2.action.voucher;

import java.util.List;

import com.isa.thinair.airreservation.api.utils.GiftVoucherUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author chethiya
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class IssueGiftVoucherAction extends BaseRequestAwareAction {
	private Log log = LogFactory.getLog(IssueGiftVoucherAction.class);

	private VoucherDTO voucherDTO;
	
	private List<VoucherDTO> voucherList; 

	private PaymentTO paymentTO;

	private String paymentMethod;

	private String ipgId;
	
	private int quantity;

	private boolean success = true;

	private String paymentError;
	
	private String fulAmountInBase;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (AppSysParamsUtil.isVoucherEnabled()) {
			UserPrincipal userPrincipal = (UserPrincipal) this.request.getUserPrincipal();			
			try {
				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();

				if (PromotionCriteriaConstants.VoucherPaymentType.CREDIT_CARD_PAYMENT.equals(paymentMethod)) {
					GiftVoucherUtils.updateCCFeeForGiftVouchers(voucherList);
				}

				for (VoucherDTO voucherDTO : voucherList) {
					voucherDTO.setIssuedUserId(userPrincipal.getUserId());
					VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();
					if (paymentMethod.equals(PromotionCriteriaConstants.VoucherPaymentType.CREDIT_CARD_PAYMENT)) {
						voucherPaymentDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
						voucherPaymentDTO.setTnxModeCode(TnxModeEnum.MAIL_TP_ORDER.getCode());
						voucherPaymentDTO.setCardTxnFeeLocal(voucherDTO.getVoucherPaymentDTO().getCardTxnFeeLocal());
					}
					if (voucherPaymentDTO.getAgentCode() == null || voucherPaymentDTO.getAgentCode().equals("")) {
						voucherPaymentDTO.setAgentCode(userPrincipal.getAgentCode());
					}
					voucherPaymentDTO.setUserIp(this.getClientInfoDTO().getIpAddress());
				}

				// Validation should come here
				voucherDelegate.issueGiftVouchersFromXBE(voucherList, this.getCredentials(),paymentMethod,
						GiftVoucherUtils.getTotalPaymentAmountForVouchersInBase(voucherList).toString());
			} catch (ModuleException me) {
				log.error("IssueGiftVoucherAction ==> execute() generating voucher failed", me);
				if (me.getExceptionCode().equals("promotions.voucher.amount.local.null")) {
					forward = S2Constants.Result.ERROR;
				}
				if (me.getExceptionCode().equals("airreservations.arg.cc.error")) {
					success = false;
					this.setPaymentError(BasicRH.getErrorMessage((Exception) me.getExceptionDetails(), userLanguage));
				}
			}
		}
		return forward;
	}

	/**
	 * @return the voucher
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO
	 *            the voucher to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

	/**
	 * @return the paymentTO
	 */
	public PaymentTO getPaymentTO() {
		return paymentTO;
	}

	/**
	 * @param paymentTO
	 *            the paymentTO to set
	 */
	public void setPaymentTO(PaymentTO paymentTO) {
		this.paymentTO = paymentTO;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentType
	 *            the paymentType to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the ipgId
	 */
	public String getIpgId() {
		return ipgId;
	}

	/**
	 * @param ipgId
	 *            the ipgId to set
	 */
	public void setIpgId(String ipgId) {
		this.ipgId = ipgId;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	private CredentialsDTO getCredentials() {
		TrackInfoDTO trackInfoDTO = this.getTrackInfo();
		UserPrincipal userPrincipal = (UserPrincipal) this.request.getUserPrincipal();
		CredentialsDTO credentialsDTO = null;
		try {
			credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
		} catch (ModuleException me) {
			log.error("IssueGiftVoucherAction ==> getCredentials() generating voucher failed", me);
		}

		return credentialsDTO;

	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the paymentError
	 */
	public String getPaymentError() {
		return paymentError;
	}

	/**
	 * @param paymentError
	 *            the paymentError to set
	 */
	public void setPaymentError(String paymentError) {
		this.paymentError = paymentError;
	}

	public List<VoucherDTO> getVoucherList() {
		return voucherList;
	}

	public void setVoucherList(List<VoucherDTO> voucherList) {
		this.voucherList = voucherList;
	}

	public String getFulAmountInBase() {
		return fulAmountInBase;
	}

	public void setFulAmountInBase(String fulAmountInBase) {
		this.fulAmountInBase = fulAmountInBase;
	}

}
