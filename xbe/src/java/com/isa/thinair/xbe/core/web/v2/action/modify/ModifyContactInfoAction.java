package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Locale;

import com.isa.thinair.xbe.api.dto.v2.XBEReservationContactInfoDTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

/**
 * Action to handle the modifying an interline reservation's contact details
 * 
 * @author Zaki Saimeh
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ModifyContactInfoAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ModifyContactInfoAction.class);

	private String pnr;
	private boolean success = true;
	private String messageTxt;
	private String groupPNR;
	private String version;
	private String oldContactInfoStr;

	private ContactInfoTO contactInfo;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		String strForward = S2Constants.Result.SUCCESS;
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);

		try {

			if (!isGroupPNR) {
				groupPNR = pnr;
			}

			CommonReservationContactInfo oldContactInfo = ReservationUtil.transformJsonContactInfo(oldContactInfoStr);
			String[] taxRegNoEnabledCountries = ModuleServiceLocator.getGlobalConfig()
					.getBizParam(SystemParamKeys.TAX_REGISTRATION_NUMBER_VISIBLE_COUNTRIES).split(",");

			String city = contactInfo.getCity();
			String countryCode = StringUtil
					.getNotNullString(StringUtils.defaultIfBlank(contactInfo.getCountry(), oldContactInfo.getCountryCode()));
			String zipCode = contactInfo.getZipCode();
			String email = contactInfo.getEmail();
			String fax = StringUtil.getNotNullString(contactInfo.getFaxCountry()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getFaxArea()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getFaxNo());
			String firstName = contactInfo.getFirstName();
			String lastName = contactInfo.getLastName();
			String mobileNo = StringUtil.getNotNullString(contactInfo.getMobileCountry()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getMobileArea()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getMobileNo());
			String phoneNo = StringUtil.getNotNullString(contactInfo.getPhoneCountry()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getPhoneArea()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getPhoneNo());
			String streetAddress1 = contactInfo.getStreet();
			String streetAddress2 = contactInfo.getAddress();
			String title = contactInfo.getTitle();
			String nationality = contactInfo.getNationality();
			String preferredLangauge = StringUtil.getNotNullString(contactInfo.getPreferredLang());
			String taxRegNo = StringUtil
					.getNotNullString(StringUtils.defaultIfBlank(contactInfo.getTaxRegNo(), oldContactInfo.getTaxRegNo()));
			String state = StringUtil
					.getNotNullString(StringUtils.defaultIfBlank(contactInfo.getState(), oldContactInfo.getState()));

			// get emergency contact details
			String emgnTitle = contactInfo.getEmgnTitle();
			String emgnFirstName = contactInfo.getEmgnFirstName();
			String emgnLastName = contactInfo.getEmgnLastName();
			String emgnPhoneNo = StringUtil.getNotNullString(contactInfo.getEmgnPhoneCountry()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getEmgnPhoneArea()) + "-"
					+ StringUtil.getNotNullString(contactInfo.getEmgnPhoneNo());
			String emgnEmail = contactInfo.getEmgnEmail();

			CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();
			lccClientReservationContactInfo.setCity(StringUtil.getNotNullString(city));
			lccClientReservationContactInfo.setCountryCode(StringUtil.getNotNullString(countryCode));
			if (nationality != null && (!nationality.equals("")) && !nationality.equalsIgnoreCase("null")) {
				lccClientReservationContactInfo.setNationalityCode(Integer.parseInt(nationality));
			}

			lccClientReservationContactInfo.setZipCode(StringUtil.getNotNullString(zipCode));
			lccClientReservationContactInfo.setEmail(StringUtil.getNotNullString(email));
			lccClientReservationContactInfo.setFax(StringUtil.getNotNullString(fax));
			lccClientReservationContactInfo.setFirstName(StringUtil.getNotNullString(firstName));
			lccClientReservationContactInfo.setLastName(StringUtil.getNotNullString(lastName));
			lccClientReservationContactInfo.setMobileNo(StringUtil.getNotNullString(mobileNo));
			lccClientReservationContactInfo.setPhoneNo(StringUtil.getNotNullString(phoneNo));
			lccClientReservationContactInfo.setStreetAddress1(StringUtil.getNotNullString(streetAddress1));
			lccClientReservationContactInfo.setStreetAddress2(StringUtil.getNotNullString(streetAddress2));
			lccClientReservationContactInfo.setTitle(StringUtil.getNotNullString(title));
			lccClientReservationContactInfo.setPreferredLanguage((preferredLangauge == "")
					? Locale.ENGLISH.toString() : preferredLangauge);

			lccClientReservationContactInfo.setCountryCode(countryCode);

			if (ArrayUtils.contains(taxRegNoEnabledCountries, countryCode)) {
				lccClientReservationContactInfo.setTaxRegNo(taxRegNo);
				lccClientReservationContactInfo.setState(state);
			} else {
				lccClientReservationContactInfo.setTaxRegNo("");
				lccClientReservationContactInfo.setState("");
			}


			// set emergency contact information
			lccClientReservationContactInfo.setEmgnTitle(StringUtil.getNotNullString(emgnTitle));
			lccClientReservationContactInfo.setEmgnFirstName(StringUtil.getNotNullString(emgnFirstName));
			lccClientReservationContactInfo.setEmgnLastName(StringUtil.getNotNullString(emgnLastName));
			lccClientReservationContactInfo.setEmgnPhoneNo(StringUtil.getNotNullString(emgnPhoneNo));
			lccClientReservationContactInfo.setEmgnEmail(StringUtil.getNotNullString(emgnEmail));

			// Update contact info
			ModuleServiceLocator.getAirproxyReservationBD().modifyContactInfo(groupPNR, version, lccClientReservationContactInfo,
					oldContactInfo, isGroupPNR, AppIndicatorEnum.APP_XBE.toString(), getTrackInfoDTO());

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
			resInfo.setContactInfoDTO(XBEReservationContactInfoDTO.getContactInfoDTO(lccClientReservationContactInfo));
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return strForward;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the contactInfo
	 */
	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public TrackInfoDTO getTrackInfoDTO() {
		TrackInfoDTO trackInfo = super.getTrackInfo();
		trackInfo.setAppIndicator(AppIndicatorEnum.APP_XBE);
		return trackInfo;
	}

	@JSON(serialize = false)
	public String getOldContactInfoStr() {
		return oldContactInfoStr;
	}

	public void setOldContactInfoStr(String oldContactInfoStr) {
		this.oldContactInfoStr = oldContactInfoStr;
	}

}