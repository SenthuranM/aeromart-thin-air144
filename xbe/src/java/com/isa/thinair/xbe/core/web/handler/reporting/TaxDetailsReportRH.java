package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Tax Detail Report action request handler
 * 
 * @author asiri
 * 
 */

public class TaxDetailsReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(TaxDetailsReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");

		try {
			setDisplayData(request);

		} catch (Exception e) {
			log.error("TaxDetailsReportRH function setDisplayData failed " + e.getMessage());

		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			} else {
				log.error("TaxDetailsReportRH setReportView not selected");
			}
		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			log.error("TaxDetailReportRH function setReportView failed" + ex.getMessageString());
		}

		return forward;
	}

	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String strReportId = "UC_REPM_091";
		String reportTemplate = "TaxDetailsReport.jasper";
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../images/" + AppSysParamsUtil.getReportLogo(true);
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String reportNameStr = "TaxDetailsReport";
		String reportFormDate = "";
		String reportToDate = "";

		String selChargeCode = request.getParameter("selChargeCode");
		String selGroup = request.getParameter("selGroup");
		String selCategory = request.getParameter("selCategory");
		String selStatus = request.getParameter("selStatus");
		String frmDate = request.getParameter("txtDateFrom");
		String toDate = request.getParameter("txtDateTo");
		String sortByOrder = request.getParameter("selSortByOrder");
		String raetCat = request.getParameter("radRateCat");

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			if (selChargeCode != null && !selChargeCode.equals("")) {
				search.setChargeCode(selChargeCode);
			}
			if (selGroup != null && !selGroup.equals("")) {
				search.setChargeGrooup(selGroup);
			}
			if (selCategory != null && !selCategory.equals("")) {
				search.setCategory(selCategory);
			}
			if (selStatus != null && !selStatus.equals("")) {
				search.setStatus(selStatus);
			}
			if (frmDate != null && !frmDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(frmDate));
				reportFormDate = frmDate;
			}
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
				reportToDate = toDate;
			}
			if (raetCat != null) {
				if (raetCat.equals("NoCharges")) {
					search.setOnlyChargesWithoutRate(true);
				} else if (raetCat.equals("WithDateRange")) {
					search.setOnlyChargesWithoutRate(false);
				}
			}

			search.setSortByOrder(sortByOrder);

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getTaxHistoryData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORT_ID", strReportId);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("COS", "COS1");
			parameters.put("BC_TYPE", "BC1");
			parameters.put("ALLOCATION_TYPE", "ALLOC1");
			parameters.put("STATUS", "ACT");
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("FROM_DATE", reportFormDate);
			parameters.put("TO_DATE", reportToDate);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.PDF_FORMAT, reportNameStr));
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportNameStr));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=TaxDetailsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			}

		} catch (Exception e) {
			log.error("setReportView Exception " + e.toString());
		}

	}

	/**
	 * Sets the Display data for CHARGE Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		try {
			setAirportList(request);
			setChargeCodeList(request);
			setGroupList(request);
			setCategoryList(request);
			ReportsHTMLGenerator.createPreferedReportOptions(request);

		} catch (Exception e) {
			log.error("Error", e);
		}
	}

	/**
	 * Sets the Chrage Code List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setChargeCodeList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargeCodeList();
		request.setAttribute(WebConstants.REQ_CHARGE_CODE_LIST, strHtml);
	}

	/**
	 * Sets the Category list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCategoryList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargeCategoryList();
		request.setAttribute(WebConstants.REQ_CATEGORY_LIST, strHtml);
	}

	/**
	 * Sets the Charge Group List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setGroupList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargeGroupList();
		request.setAttribute(WebConstants.REQ_GROUP_LIST, strHtml);
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

}
