package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class IssuedVoucherReportRH extends BasicRH {
	private static Log log = LogFactory.getLog(IssuedVoucherReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	
	private static final String REPORT_TEMPLATE = "IssuedVoucherDetailsReport.jasper";
	
	private static final String REPORT_NAME ="IssuedVoucherDetailsReport";
	
	private static final String REPORT_ID = "UC_REPM_020";

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = S2Constants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {

			try {
				setDisplayData(request);
				log.debug("IssuedVoucherReportRH success");
			} catch (Exception e) {
				log.error("IssuedVoucherReportRH execute()" + e.getMessage());
			}

			try {
				if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
					setReportView(request, response);
					log.error("IssuedVoucherReportRH setReportView Success");
					return null;
				} else {
					log.error("IssuedVoucherReportRH setReportView not selected");
				}

			} catch (ModuleException e) {
				saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
				log.error("IssuedVoucherReportRH setReportView Failed " + e.getMessageString());
			}
		}
		return forward;
	}
	
	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setReportingPeriod(request);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String voucherName = request.getParameter("voucherNo");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String value = request.getParameter("radReportOption");
		String status = request.getParameter("status");
		
		String reportTemplate = REPORT_TEMPLATE;
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strReportId = REPORT_ID;
		String reportName = REPORT_NAME;
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			
			search.setVoucherName(voucherName);
			search.setFirstName(firstName);
			search.setLastName(lastName);	
			search.setVoucherStatus(status);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getIssuedVoucherDetails(search);	

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("REPORT_ID", strReportId);
			parameters.put("TIME_ZONE", WebConstants.ZULUTIME);
			parameters.put("ZULU_TIME", getZuluTime (request));			
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("LOCALE", "US");
			parameters.put("TO_DATE", toDate);
			parameters.put("CURRENCY", AppSysParamsUtil.getBaseCurrency());			
			
			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", String.format("attachment;filename=%s.xls", reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	private static String getZuluTime (HttpServletRequest request) {

		String zuluTimePart = "";
		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTimestamp);
		zuluTimePart = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
					+ calendar.get(Calendar.SECOND);
		
		return zuluTimePart;
	}
}
