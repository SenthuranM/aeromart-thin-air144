package com.isa.thinair.xbe.core.web.v2.action.voucher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author chanaka
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Voucher.PRINT_VOUCHER),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class PrintVoucherAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(PrintVoucherAction.class);

	private String validFrom;
	private String validTo;
	private String amountInLocal;
	private String currencyCode;
	private String paxFirstName;
	private String paxLastName;
	private String voucherId;
	private String templateId;
	private String languageCode;
	private VoucherDTO voucherDTO = new VoucherDTO();

	public String execute() {

		String forward = S2Constants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			UserPrincipal userPrincipal = (UserPrincipal) this.request.getUserPrincipal();
			voucherDTO.setIssuedUserId(userPrincipal.getUserId());
			voucherDTO.setValidFrom(validFrom);
			voucherDTO.setValidTo(validTo);
			voucherDTO.setAmountInLocal(amountInLocal);
			voucherDTO.setCurrencyCode(currencyCode);
			voucherDTO.setPaxFirstName(paxFirstName);
			voucherDTO.setPaxLastName(paxLastName);
			voucherDTO.setVoucherId(voucherId);
			voucherDTO.setTemplateId(Integer.parseInt(templateId));
			voucherDTO.setPrintLanguage(languageCode);

			String printHtmlContent = "";

			try {
				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				printHtmlContent += voucherDelegate.printVoucher(voucherDTO);

				request.setAttribute("printVoucherBodyHtml", printHtmlContent);

			} catch (ModuleException me) {
				log.error("PrintVoucherAction ==> execute() printing voucher failed", me);
				if (me.getExceptionCode().equals("promotions.voucher.amount.local.null")) {
					forward = S2Constants.Result.ERROR;
				}
			}
		}
		return forward;
	}

	/**
	 * @return the voucher
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO
	 *            the voucher to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

	/**
	 * @return the validFrom
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the amountInLocal
	 */
	public String getAmountInLocal() {
		return amountInLocal;
	}

	/**
	 * @param amountInLocal
	 *            the amountInLocal to set
	 */
	public void setAmountInLocal(String amountInLocal) {
		this.amountInLocal = amountInLocal;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the paxFirstName
	 */
	public String getPaxFirstName() {
		return paxFirstName;
	}

	/**
	 * @param paxFirstName
	 *            the paxFirstName to set
	 */
	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}

	/**
	 * @return the paxLastName
	 */
	public String getPaxLastName() {
		return paxLastName;
	}

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * @param paxLastName
	 *            the paxLastName to set
	 */
	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}

	/**
	 * @return the voucherId
	 */
	public String getVoucherId() {
		return voucherId;
	}

	/**
	 * @param voucherId
	 *            the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @param languageCode the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
}
