package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.xbe.api.dto.v2.PaxCreditListTO;
import com.isa.thinair.xbe.api.dto.v2.PaxCreditTO;
import com.isa.thinair.xbe.api.dto.v2.PaxUsedCreditInfoTO;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class PaxCreditUtil {

	/**
	 * 
	 * @param collection
	 * @return
	 * @throws ParseException
	 */
	public static Collection<PaxCreditListTO> createPaxCredit(Collection<ReservationPaxDTO> collection, String strAdults,
			Collection<PaxUsedCreditInfoTO> selectedCredit) throws ParseException, ModuleException {
		Collection<PaxCreditListTO> collectionPaxCredits = new ArrayList<PaxCreditListTO>();

		String strDateFormatDDMMYYYY = "dd/MM/yyyy";
		String strTimeFormatHHmm = "HH:mm";
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");

		// Take the previously used credits and sets the object according to that
		JSONArray jsonArray = (JSONArray) new JSONParser().parse(strAdults);
		Collection<PaxUsedCreditInfoTO> creditCol = new ArrayList<PaxUsedCreditInfoTO>();

		// Set<?> arrSet = jsonArray.keySet();
		Iterator<?> jiter = jsonArray.iterator();

		while (jiter.hasNext()) {
			JSONObject jObject = (JSONObject) jiter.next();
			JSONArray paxCrditInfo = (JSONArray) jObject.get("paxCreditInfo");

			// Set<?> creditSet = paxCrditInfo.keySet();
			Iterator<?> criter = paxCrditInfo.iterator();

			while (criter.hasNext()) {
				JSONObject creditObject = (JSONObject) criter.next();
				PaxUsedCreditInfoTO paxCredit = new PaxUsedCreditInfoTO();
				paxCredit.setPaxCreditId((String) creditObject.get("paxCreditId"));
				paxCredit.setUsedAmount((String) creditObject.get("usedAmount"));
				creditCol.add(paxCredit);
			}
		}

		// Filter both selected & used credit
		boolean prevoiusFound = false;
		for (PaxUsedCreditInfoTO selected : selectedCredit) {
			prevoiusFound = false;
			for (PaxUsedCreditInfoTO prevCredit : creditCol) {
				if (selected.getPaxCreditId().equals(prevCredit.getPaxCreditId())) {
					prevoiusFound = false;
					break;
				}
			}

			if (!prevoiusFound) {
				creditCol.add(selected);
			}
		}

		// Filling the paxcredit collection
		if (collection != null && collection.size() > 0) {
			String strBuyersInfo = "";
			String strPaxCount = "";
			String strSegemnt = "";
			Iterator<ReservationPaxDTO> iterator = collection.iterator();
			while (iterator.hasNext()) {
				ReservationPaxDTO reservationPaxDTO = iterator.next();

				strBuyersInfo = "";
				strPaxCount = "";

				ReservationContactInfo reservationContactInfo = reservationPaxDTO.getReservationContactInfo();
				strBuyersInfo = JavaScriptGenerator.nullConvertToString(reservationContactInfo.getFirstName()) + ", "
						+ JavaScriptGenerator.nullConvertToString(reservationContactInfo.getLastName());
				strPaxCount = reservationPaxDTO.getAdultCount().toString() + "/" + reservationPaxDTO.getChildCount().toString()
						+ "/" + reservationPaxDTO.getInfantCount().toString();

				PaxCreditListTO paxCreditListTO = new PaxCreditListTO();

				paxCreditListTO.setPnrNo(reservationPaxDTO.getPnr());
				paxCreditListTO.setPaxName(strBuyersInfo);
				paxCreditListTO.setCarrier(reservationPaxDTO.getCarrierCode());
				paxCreditListTO.setPaxCount(strPaxCount);
				paxCreditListTO.setPnrStatus(reservationPaxDTO.getStatus());

				// Flight Informations
				Collection<ReservationSegmentDTO> collSegments = reservationPaxDTO.getSegments();
				Collection<ReservationExternalSegmentTO> collExtSegments = reservationPaxDTO.getColExternalSegments();

				List<FlightInfoTO> listFlightInfo = new ArrayList<FlightInfoTO>();
				for (ReservationSegmentDTO reservationSegmentDTO : collSegments) {

					FlightInfoTO flightInfoTO = new FlightInfoTO();

					strSegemnt = JavaScriptGenerator.nullConvertToString(reservationSegmentDTO.getSegmentCode());
					if (AppSysParamsUtil.isHideStopOverEnabled() && strSegemnt != null && strSegemnt.split("/").length > 2)
						strSegemnt = ReservationApiUtils.hideStopOverSeg(strSegemnt);

					flightInfoTO.setFlightNo(reservationSegmentDTO.getFlightNo());
					flightInfoTO.setAirLine(reservationSegmentDTO.getCarrierCode());
					flightInfoTO.setDepartureDate(reservationSegmentDTO.getDepartureStringDate(strDateFormatDDMMYYYY));
					flightInfoTO.setDepartureTime(reservationSegmentDTO.getDepartureStringDate(strTimeFormatHHmm));
					flightInfoTO.setDepartureDateLong(reservationSegmentDTO.getZuluDepartureDate().getTime());
					flightInfoTO.setArrivalDate(reservationSegmentDTO.getArrivalStringDate(strDateFormatDDMMYYYY));
					flightInfoTO.setArrivalTime(reservationSegmentDTO.getArrivalStringDate(strTimeFormatHHmm));
					flightInfoTO.setOrignNDest(strSegemnt);

					listFlightInfo.add(flightInfoTO);

				}
				if (collExtSegments != null) {
					for (ReservationExternalSegmentTO reservationExternalSegmentTO : collExtSegments) {
						String segmentCode = reservationExternalSegmentTO.getSegmentCode();

						if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
							segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
						}

						FlightInfoTO flightInfoTO = new FlightInfoTO();
						flightInfoTO.setFlightNo(reservationExternalSegmentTO.getFlightNo());
						flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(reservationExternalSegmentTO.getFlightNo()));
						flightInfoTO.setDepartureDate(smpdtDate.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureTime(smpdtTime.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureDateLong(reservationExternalSegmentTO.getDepartureDate().getTime());
						flightInfoTO.setArrivalDate(smpdtDate.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setArrivalTime(smpdtTime.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setOrignNDest(segmentCode);
						flightInfoTO.setPaxCount(strPaxCount);
						flightInfoTO.setStatus(reservationExternalSegmentTO.getStatus());
						listFlightInfo.add(flightInfoTO);
					}
				}
				Collections.sort(listFlightInfo);
				paxCreditListTO.setFlightInfo(listFlightInfo);

				// Credits
				String strPsng = "";
				Collection<ReservationPaxDetailsDTO> collCredits = reservationPaxDTO.getPassengers();
				Collection<PaxCreditTO> collectionCreditInfo = new ArrayList<PaxCreditTO>();
				for (ReservationPaxDetailsDTO reservationPaxDetailsDTO : collCredits) {

					BigDecimal credit = reservationPaxDetailsDTO.getCredit();

					if (!JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getTitle()).equals("")) {
						reservationPaxDetailsDTO.setTitle(reservationPaxDetailsDTO.getTitle().concat("."));
					}

					if (reservationPaxDetailsDTO.getPaxType().equals(PaxTypeTO.CHILD)) {
						strPsng = "Child. " + JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getTitle())
								+ JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getFirstName()) + ", "
								+ JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getLastName());

					} else {
						strPsng = JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getTitle())
								+ JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getFirstName()) + ", "
								+ JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getLastName());
					}

					// calculate the used credit

					if (reservationPaxDetailsDTO.getCredit() == null) {
						credit = AccelAeroCalculator.getDefaultBigDecimalZero();
					}

					if (credit == null) {
						credit = AccelAeroCalculator.getDefaultBigDecimalZero();
					}

					PaxCreditTO paxCreditTO = new PaxCreditTO();
					BigDecimal utilized = BigDecimal.ZERO;
					BigDecimal originalCredit = credit;

					Iterator<PaxUsedCreditInfoTO> usdIter = creditCol.iterator();
					while (usdIter.hasNext()) {
						PaxUsedCreditInfoTO usedCredit = (PaxUsedCreditInfoTO) usdIter.next();
						if (Integer.toString(reservationPaxDetailsDTO.getPnrPaxId()).equals(usedCredit.getPaxCreditId())) {
							utilized = new BigDecimal(usedCredit.getUsedAmount());
							credit = AccelAeroCalculator.add(credit, utilized.negate());
						}
					}
					paxCreditTO.setPaxID(Integer.toString(reservationPaxDetailsDTO.getPnrPaxId()));
					paxCreditTO.setPaxName(strPsng);
					paxCreditTO.setCarrier(reservationPaxDTO.getCarrierCode()); // all the pax in a given res belongs to
																				// the same carrier
					paxCreditTO.setPnr(reservationPaxDTO.getPnr());
					if (credit.compareTo(BigDecimal.ZERO) < 0) {
						credit = BigDecimal.ZERO;
					}
					paxCreditTO.setAvailableCredit(AccelAeroCalculator.formatAsDecimal(credit));
					paxCreditTO.setUtilizedCredit(AccelAeroCalculator.formatAsDecimal(utilized));
					paxCreditTO.setOriginalCredit(AccelAeroCalculator.formatAsDecimal(originalCredit));
					paxCreditTO.setResidingCarrierAmount(AccelAeroCalculator.formatAsDecimal(reservationPaxDetailsDTO
							.getResidingCarrierAmount()));
					collectionCreditInfo.add(paxCreditTO);
				}
				paxCreditListTO.setPaxCredits(collectionCreditInfo);
				collectionPaxCredits.add(paxCreditListTO);
			}
		}
		return collectionPaxCredits;
	}

	public static BigDecimal getSelectedPaxCreditAmount(Collection<PaxUsedCreditInfoTO> selectedCredit) {
		BigDecimal paxUsedCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (PaxUsedCreditInfoTO selected : selectedCredit) {
			if (selected != null && selected.getUsedAmount() != null && !selected.getUsedAmount().equals("")) {
				paxUsedCredit = AccelAeroCalculator.add(paxUsedCredit, new BigDecimal(selected.getUsedAmount()));
			}

		}
		return paxUsedCredit;
	}

}
