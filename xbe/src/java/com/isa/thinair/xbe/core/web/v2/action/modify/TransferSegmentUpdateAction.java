package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO.SegmentDetails;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Alert Transfer functionality
 * 
 * @author Pradeep Karunanayake
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class TransferSegmentUpdateAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(TransferSegmentUpdateAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private String flightRPHList;

	private String cabinClass;

	private String pnr;

	private String groupPNR;

	private String version;

	private String fltSegment;

	private boolean alertSegmentTransfer;

	private String alertSegmentID;
	
	private boolean gdsPnr; 
	
	private Collection<LCCClientAlertInfoTO> pnrAlerts;

	@SuppressWarnings("unchecked")
	public String execute() {

		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);

		ReservationProcessParams rparam = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, gdsPnr, false);
		if (isGroupPNR) {
			rparam.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
					resInfo != null ? resInfo.getReservationStatus() : null, null,
					resInfo != null ? resInfo.isModifiableReservation() : true, resInfo.getLccPromotionInfoTO());
		}
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			LCCClientTransferSegmentTO transferSegmentTO = new LCCClientTransferSegmentTO();
			/*
			 * There are two scenarios are handling 1. Interline - Only Reservation Transfer 2. Own Airline - Connecting
			 * segments can be changed These two scenarios handling
			 */

			if (rparam.isTransferAlert()) {
				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
						.getParameter("resSegments"));
				boolean isInterlineCarrierReservation = ReservationUtil.isInterlineCarrierReservation(colsegs);
				boolean isInterLine = isInterlineCarrierReservation && isGroupPNR;
				Collection<LCCClientReservationSegment> colTransferSegs = null;
				if (alertSegmentTransfer) {
					colTransferSegs = ReservationUtil.getAlertTransferSegment(colsegs, alertSegmentID);
				} else {
					colTransferSegs = getReprotectedTransferSegments(colsegs, fltSegment);
					alertSegmentID = getTransferSegmentsIDs(fltSegment);
				}
				List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(flightRPHList,
						fareQuoteParams);

				if (isGroupPNR) {
					/*
					 * ######### Interline Create Old Segment Details
					 */
					Map<String, SegmentDetails> oldSegmentDetails = new HashMap<String, SegmentDetails>();
					if (colTransferSegs != null) {

						for (LCCClientReservationSegment clientReservationSegment : colTransferSegs) {
							SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
							segmentDetails.setDepartureDate(clientReservationSegment.getDepartureDate());
							segmentDetails.setDepartureDateZulu(clientReservationSegment.getDepartureDateZulu());
							segmentDetails.setArrivalDate(clientReservationSegment.getArrivalDate());
							segmentDetails.setArrivalDateZulu(clientReservationSegment.getArrivalDateZulu());

							segmentDetails.setPnrSegId(new Integer(clientReservationSegment.getBookingFlightSegmentRefNumber()));
							segmentDetails.setSegmentCode(clientReservationSegment.getSegmentCode());
							segmentDetails
									.setCarrier(AppSysParamsUtil.extractCarrierCode(clientReservationSegment.getFlightNo()));
							segmentDetails.setCabinClass(clientReservationSegment.getCabinClassCode());
							segmentDetails.setLogicalCabinClass(clientReservationSegment.getLogicalCabinClass());
							segmentDetails.setFlightNumber(clientReservationSegment.getFlightNo());
							segmentDetails.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
							segmentDetails.setSegmentSeq(clientReservationSegment.getSegmentSeq());
							oldSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

						}
					}
					transferSegmentTO.setOldSegmentDetails(oldSegmentDetails);
					/* Create New Segment Details */
					Map<String, SegmentDetails> newSegmentDetails = new HashMap<String, SegmentDetails>();
					if (flightSegmentTOs != null) {
						for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
							SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
							segmentDetails.setDepartureDate(flightSegmentTO.getDepartureDateTime());
							segmentDetails.setDepartureDateZulu(flightSegmentTO.getDepartureDateTimeZulu());
							segmentDetails.setArrivalDate(flightSegmentTO.getArrivalDateTime());
							segmentDetails.setArrivalDateZulu(flightSegmentTO.getArrivalDateTimeZulu());
							segmentDetails.setFlightSegId(flightSegmentTO.getFlightSegId());
							segmentDetails.setSegmentCode(flightSegmentTO.getSegmentCode());
							segmentDetails.setCarrier(AppSysParamsUtil.extractCarrierCode(flightSegmentTO.getFlightNumber()));
							segmentDetails.setCabinClass(flightSegmentTO.getCabinClassCode());
							segmentDetails.setLogicalCabinClass(flightSegmentTO.getLogicalCabinClassCode());
							segmentDetails.setFlightNumber(flightSegmentTO.getFlightNumber());
							segmentDetails.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
							segmentDetails.setSegmentSeq(flightSegmentTO.getSegmentSequence());
							newSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);
						}
					}
					transferSegmentTO.setNewSegmentDetails(newSegmentDetails);

					if (oldSegmentDetails.size() != newSegmentDetails.size()) {
						throw new ModuleException("airreservations.transfersegment.segment.notmatch");
					}
				} else {

					/*
					 * Validate alerts segment size vs flights segments
					 */
					if (colTransferSegs.size() != flightSegmentTOs.size()) {
						throw new ModuleException("airreservations.transfersegment.segment.notmatch");
					}
					/*
					 * As requested by AA - removed ond transfer restrictions Users can misuse the functionality.
					 */

					Map<String, Integer> oldNewFlightSegId = new HashMap<String, Integer>();
					if (colsegs != null) {
						int flightCount = 0;
						Set<LCCClientReservationSegment> segmentsSet = new HashSet(colTransferSegs);
						for (LCCClientReservationSegment segment : SortUtil.sort(segmentsSet)) {
							if (flightCount < flightSegmentTOs.size()) {
								oldNewFlightSegId.put(segment.getFlightSegmentRefNumber(), flightSegmentTOs.get(flightCount)
										.getFlightSegId());
								flightCount++;
							}
						}
						transferSegmentTO.setOldNewFlightSegId(oldNewFlightSegId);

					}

				}
				if (isGroupPNR) {
					pnr = groupPNR;
				}
				transferSegmentTO.setPnr(pnr);
				transferSegmentTO.setGroupPNR(isGroupPNR);
				transferSegmentTO.setReservationVersion(version);
				transferSegmentTO.setAlert(true);

				ModuleServiceLocator.getAirproxySegmentBD().transferSegments(transferSegmentTO,
						TrackInfoUtil.getBasicTrackInfo(request), null);
			} else {
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private String getTransferSegmentsIDs(String segId) {
		String[] selectedSegs = segId.split(",");
		String newAlertSegIDs = "";
		for (int i = 0; i < selectedSegs.length; i++) {
			String[] selectedCNFSegs = selectedSegs[i].split("\\$");
			String strBookingRef = selectedCNFSegs[0];
			if (newAlertSegIDs!=null && !newAlertSegIDs.isEmpty()){
				newAlertSegIDs = newAlertSegIDs + "," + strBookingRef;
			}
			else{
				newAlertSegIDs = strBookingRef;
			}
		}
		return newAlertSegIDs;
	}
	
	/**
	 * Get applicable transfer segments
	 * 
	 * @param colResSegs
	 * @param segId
	 * @return
	 */
	private static Collection<LCCClientReservationSegment> getReprotectedTransferSegments(
			Collection<LCCClientReservationSegment> colResSegs, String segId) {
		Collection<LCCClientReservationSegment> colSegs = new ArrayList<LCCClientReservationSegment>();
		String[] selectedSegs = segId.split(",");

		for (LCCClientReservationSegment resSeg : colResSegs) {
			for (int i = 0; i < selectedSegs.length; i++) {
				String[] selectedCNFSegs = selectedSegs[i].split("\\$");
				String strBookingRef = selectedCNFSegs[0];
				String strInterlingGroupKey = selectedCNFSegs[1];

				if (resSeg.getBookingFlightSegmentRefNumber().equals(strBookingRef)
						&& resSeg.getInterlineGroupKey().equals(strInterlingGroupKey)) {
					colSegs.add(resSeg);
					break;
				}
			}
		}
		return colSegs;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * 
	 * @return the messageTxt
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * 
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setFltSegment(String fltSegment) {
		this.fltSegment = fltSegment;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getAlertSegmentID() {
		return alertSegmentID;
	}

	public void setAlertSegmentID(String alertSegmentID) {
		this.alertSegmentID = alertSegmentID;
	}

	public boolean isAlertSegmentTransfer() {
		return alertSegmentTransfer;
	}

	public void setAlertSegmentTransfer(boolean alertSegmentTransfer) {
		this.alertSegmentTransfer = alertSegmentTransfer;
	}

	public boolean isGdsPnr() {
		return gdsPnr;
	}

	public void setGdsPnr(boolean gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	public Collection<LCCClientAlertInfoTO> getPnrAlerts() {
		return pnrAlerts;
	}

	public void setPnrAlerts(Collection<LCCClientAlertInfoTO> pnrAlerts) {
		this.pnrAlerts = pnrAlerts;
	}

}
