/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Dhanushka
 *
 */

package com.isa.thinair.xbe.core.web.handler.gds;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.gds.GdsMessagesHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Dhanushka
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style -
 *         Code Templates
 */

public final class GdsMessagesRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(GdsMessagesRequestHandler.class);

	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_SRC_GDS_NAME = "selGDSType";
	private static final String PARAM_SRC_MSG_TYPE = "selMSGType";
	private static final String PARAM_SRC_PROCESS_STATUS = "selProcessStatus";
	private static final String PARAM_SRC_COM_REF = "txtCommReference";
	private static final String PARAM_SRC_FROM_DATE = "txtFromDate";
	private static final String PARAM_SRC_TO_DATE = "txtToDate";
	private static final String PARAM_SRC_REQ_XML = "txtReqXml";
	private static final String PARAM_SRC_RES_XML = "txtResXml";
	private static final String PARAM_SRC_GDS_PNR = "txtGDSPnr";
	private static final String PARAM_SRC_MSG_BROKER = "txtMsgBrokerRef";
	private static final String PARAM_GDS_MSG_ID = "hdnGdsMsgID";

	/**
	 * @param request
	 */
	public static String execute(HttpServletRequest request) {

		Properties properties = getProperty(request);

		String forward = S2Constants.Result.SUCCESS;
		String strUIModeJS = "var isSearchMode = false;";
		setExceptionOccured(request, false);
		setIntSuccess(request, 0);

		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if (properties.getProperty(PARAM_UI_MODE).equals(WebConstants.ACTION_VIEW_REQ_XML)) {
				showGDSRequest(request, properties);
			} else if (properties.getProperty(PARAM_UI_MODE).equals(WebConstants.ACTION_VIEW_RES_XML)) {
				showGDSResponse(request, properties);
			} else {
				setDisplayData(request, properties);
			}
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static void setMessageTypeList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createGDSMessageTypeList();
			request.setAttribute(WebConstants.REQ_HTML_GDS_MSGTYPE_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("GdsMessagesRequestHandler.setMessageTypeList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setGDSNameList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createGDSNamesList();
			request.setAttribute(WebConstants.REQ_HTML_GDS_NAMES_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("GdsMessagesRequestHandler.setGDSNameList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setProcessStatusList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createGDSProcessStatusList();
			request.setAttribute(WebConstants.REQ_HTML_GDS_PROCESS_STATUS_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("GdsMessagesRequestHandler.setProcessStatusList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void showGDSResponse(HttpServletRequest request, Properties properties) throws Exception {
		try {
			GdsMessagesHTMLGenerator htmlGen = new GdsMessagesHTMLGenerator();
			String strHtml = htmlGen.getGDSResponseHtml(request, properties);
			request.setAttribute(WebConstants.REQ_HTML_GDS_MSG_DATA, strHtml);

			setClientErrors(request);
			request.setAttribute(WebConstants.REQ_IS_EXCEPTION, "var isException='" + isExceptionOccured(request) + "';");

		} catch (ModuleException moduleException) {
			log.error("GdsMessagesRequestHandler.showGDSResponse() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void showGDSRequest(HttpServletRequest request, Properties properties) {
		try {
			GdsMessagesHTMLGenerator htmlGen = new GdsMessagesHTMLGenerator();
			String strHtml = htmlGen.getGDSRequestHtml(request, properties);
			request.setAttribute(WebConstants.REQ_HTML_GDS_MSG_DATA, strHtml);

			setClientErrors(request);
			request.setAttribute(WebConstants.REQ_IS_EXCEPTION, "var isException='" + isExceptionOccured(request) + "';");

		} catch (ModuleException moduleException) {
			log.error("GdsMessagesRequestHandler.showGDSResponse() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static void setDisplayData(HttpServletRequest request, Properties properties) throws Exception {

		String strUIMode = properties.getProperty(PARAM_UI_MODE);

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {
			setGDSMessagesRowHtml(request, properties);
		}

		setMessageTypeList(request);
		setGDSNameList(request);
		setProcessStatusList(request);

		setClientErrors(request);
		request.setAttribute(WebConstants.REQ_IS_EXCEPTION, "var isException='" + isExceptionOccured(request) + "';");
	}

	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = GdsMessagesHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			// saveMessage(request, moduleException.getExceptionCode() + " ~ " + moduleException.getMessageString(),
			// Constants.MSG_ERROR);
		}
	}

	private static void setGDSMessagesRowHtml(HttpServletRequest request, Properties properties) throws Exception {

		try {
			GdsMessagesHTMLGenerator htmlGen = new GdsMessagesHTMLGenerator();
			String strHtml = htmlGen.getGDSMessagesRowHtml(request, properties);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error("GdsMessagesRequestHandler.setGDSMessagesRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Request Data to a Property File
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties
	 */
	private static Properties getProperty(HttpServletRequest request) {
		Properties prop = new Properties();

		prop.setProperty(PARAM_MODE, StringUtil.getNotNullString(request.getParameter(PARAM_MODE)));
		prop.setProperty(PARAM_UI_MODE, StringUtil.getNotNullString(request.getParameter(PARAM_UI_MODE)));
		prop.setProperty(PARAM_SRC_GDS_NAME, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_GDS_NAME)));
		prop.setProperty(PARAM_SRC_MSG_TYPE, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_MSG_TYPE)));
		prop.setProperty(PARAM_SRC_PROCESS_STATUS, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_PROCESS_STATUS)));
		prop.setProperty(PARAM_SRC_COM_REF, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_COM_REF)));
		prop.setProperty(PARAM_SRC_FROM_DATE, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_FROM_DATE)));
		prop.setProperty(PARAM_SRC_TO_DATE, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_TO_DATE)));
		prop.setProperty(PARAM_SRC_REQ_XML, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_REQ_XML)));
		prop.setProperty(PARAM_SRC_RES_XML, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_RES_XML)));
		prop.setProperty(PARAM_GDS_MSG_ID, StringUtil.getNotNullString(request.getParameter(PARAM_GDS_MSG_ID)));
		prop.setProperty(PARAM_SRC_GDS_PNR, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_GDS_PNR)));
		prop.setProperty(PARAM_SRC_MSG_BROKER, StringUtil.getNotNullString(request.getParameter(PARAM_SRC_MSG_BROKER)));

		return prop;
	}

}
