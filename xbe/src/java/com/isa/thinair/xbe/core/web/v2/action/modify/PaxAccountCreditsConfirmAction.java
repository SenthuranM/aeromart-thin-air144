package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.dto.PaxCreditReinstateTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountChargesTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountCreditTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountPaymentsTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaxAccountUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class PaxAccountCreditsConfirmAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(PaxAccountCreditsConfirmAction.class);

	private boolean success = true;

	private String messageTxt;
	private String txtCreditAmount;
	private String txtExpiryDate;
	private String txtCreditUN;
	private String txtCreditId;
	private String txtTNXId;
	private String mode;
	private String selPax;
	private String groupPNR;
	private String pnr;
	private String status;
	private PaxAccountCreditTO paxAccountCreditTO;
	private PaxAccountChargesTO paxAccountChargesTO;
	private PaxAccountPaymentsTO paxAccountPaymentsTO;
	private String txtCarrierCode;
	private String ocCreditAmount;

	@SuppressWarnings("unchecked")
	public String execute() throws ModuleException {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);

			if (!isGroupPNR) {
				groupPNR = pnr;
			}
			Integer intPnrPaxId = PaxAccountUtil.getPnrPaxID(request.getParameter("paxID"), txtCarrierCode);
			LCCClientReservationPax pax = null;
			String[] sdate = txtExpiryDate.split("/");
			// saftey check
			if (sdate.length < 3) {
				sdate = txtExpiryDate.split("-");
			}
			Calendar oCal = Calendar.getInstance();

			oCal.set(Integer.parseInt(sdate[2]), Integer.parseInt(sdate[1]) - 1, Integer.parseInt(sdate[0]));

			// TODO bind the variables from the jsp itself
			PaxCreditReinstateTO paxCreditReinstateTO = new PaxCreditReinstateTO();
			paxCreditReinstateTO.setBlnGroupPnr(isGroupPNR);
			paxCreditReinstateTO.setCreditAmount(new BigDecimal(txtCreditAmount));
			paxCreditReinstateTO.setCreditId(Long.parseLong(txtCreditId));
			paxCreditReinstateTO.setExtendingDate(oCal.getTime());
			paxCreditReinstateTO.setMode(mode);
			paxCreditReinstateTO.setPaxID(intPnrPaxId);
			paxCreditReinstateTO.setPnr(groupPNR);
			paxCreditReinstateTO.setTransactionId(Integer.parseInt(txtTNXId));
			paxCreditReinstateTO.setUserNote(txtCreditUN);
			paxCreditReinstateTO.setCarrierCode(txtCarrierCode);
			paxCreditReinstateTO.setOcCreditAmount(new BigDecimal(ocCreditAmount));

			TrackInfoDTO trackInfoDTO = getTrackInfo();
			ModuleServiceLocator.getAirproxyPassengerBD().reInstatePaxCredit(paxCreditReinstateTO, trackInfoDTO);
			pax = ModuleServiceLocator.getAirproxyPassengerBD().loadPassenger(groupPNR, intPnrPaxId, trackInfoDTO, isGroupPNR);

			paxAccountCreditTO = PaxAccountUtil.getCreditDetails(pax);

			List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(request.getParameter("resONDs"));

			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));

			String strCurrency = AppSysParamsUtil.getBaseCurrency();
			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			String agentCurrency = up.getAgentCurrencyCode();

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			paxAccountChargesTO = PaxAccountUtil.getChargesDetails(pax, strCurrency, colONDs, isGroupPNR, colsegs, agentCurrency,
					status, resInfo.getReservationLastModDate(), Boolean.parseBoolean(request.getParameter("isInfantPaymentSeparated")));

			Map<String, String> mapAllAgentCodeDesc = XBEModuleUtils.getGlobalConfig().getAgentsByAgentCode();

			paxAccountPaymentsTO = PaxAccountUtil.getPaymentDetails(pax, status, mapAllAgentCodeDesc, false);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setTxtCreditAmount(String txtCreditAmount) {
		this.txtCreditAmount = txtCreditAmount;
	}

	public void setTxtExpiryDate(String txtExpiryDate) {
		this.txtExpiryDate = txtExpiryDate;
	}

	public void setTxtCreditUN(String txtCreditUN) {
		this.txtCreditUN = txtCreditUN;
	}

	public void setTxtCreditId(String txtCreditId) {
		this.txtCreditId = txtCreditId;
	}

	public void setTxtTNXId(String txtTNXId) {
		this.txtTNXId = txtTNXId;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getSelPax() {
		return selPax;
	}

	public void setSelPax(String selPax) {
		this.selPax = selPax;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public PaxAccountCreditTO getPaxAccountCreditTO() {
		return paxAccountCreditTO;
	}

	public PaxAccountChargesTO getPaxAccountChargesTO() {
		return paxAccountChargesTO;
	}

	public PaxAccountPaymentsTO getPaxAccountPaymentsTO() {
		return paxAccountPaymentsTO;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTxtCarrierCode(String txtCarrierCode) {
		this.txtCarrierCode = txtCarrierCode;
	}

	public void setOcCreditAmount(String ocCreditAmount) {
		this.ocCreditAmount = ocCreditAmount;
	}

}
