package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONUtil;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.SystemParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_MODIFY_ANCILLARY),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ModifyAncillaryAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ModifyAncillaryAction.class);

	public String execute() {
		try {

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rpParams = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), false, null, null, false, false);
			if (resInfo.getInterlineModificationParams() != null && resInfo.getInterlineModificationParams().size() > 0) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), null, resInfo.isModifiableReservation(), null);
			}
			removeBundledFareDescriptionTemplate(resInfo.getBundledFareDTOs());
			request.setAttribute("initialParams", JSONUtil.serialize(rpParams));

			request.setAttribute("resPaySummary", request.getParameter("resPaySummary"));
			request.setAttribute("resPaxWisePayments", request.getParameter("resPaxWisePayments"));
			request.setAttribute("anciAvail", request.getParameter("anciAvail"));
			FareQuoteSummaryTO fQ = new FareQuoteSummaryTO();
			fQ.setCurrency(AppSysParamsUtil.getBaseCurrency());
			request.setAttribute("fareQuote", JSONUtil.serialize(fQ));
			request.setAttribute("flightRPHList", request.getParameter("resSegments"));
			request.setAttribute("resPaxs", request.getParameter("resPax").replaceAll("'", "\\\\'"));
			request.setAttribute("reqPayAgents", ReservationUtil.getPaymentAgentOptions(request));
			request.setAttribute("resContactInfo", request.getParameter("resContactInfo"));
			request.setAttribute("pnrNo", request.getParameter("pnrNo"));
			request.setAttribute("groupPNRNo", request.getParameter("groupPNRNo"));
			request.setAttribute("version", request.getParameter("version"));
			request.setAttribute("carrierCode", AppSysParamsUtil.getDefaultCarrierCode());
			request.setAttribute("selCurrency", rpParams.getDefaultCurrencyCode());
			request.setAttribute("ownerAgentCode", request.getParameter("resOwnerAgent"));
			request.setAttribute("reservationStatus", resInfo.getReservationStatus());
			request.setAttribute("jsonOnds", request.getParameter("jsonOnds"));
			
			
			request.setAttribute("bundleFareList", JSONUtil.serialize(resInfo.getBundledFareDTOs()));
			request.setAttribute("serviceTaxLabel", AppSysParamsUtil.getServiceTaxLabel());

			SystemParams sysParams = new SystemParams();
			sysParams.setMinimumTransitionTimeInMillis(AppSysParamsUtil.getMinimumReturnTransitTimeInMillis());
			sysParams.setDefaultLanguage(XBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));
			sysParams.setDynamicOndListPopulationEnabled(AppSysParamsUtil.isDynamicOndListPopulationEnabled());
			sysParams.setUserDefineTransitTimeEnabled(AppSysParamsUtil.isEnableUserDefineTransitTime());

			request.setAttribute("systemParams", JSONUtil.serialize(sysParams));

		} catch (Exception e) {
			log.error("ERROR in Ancillary MOdifying", e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	private void removeBundledFareDescriptionTemplate(List<BundledFareDTO> bundledFareDTOs) {
		
		if (!CollectionUtils.isEmpty(bundledFareDTOs)) {
			
			for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
				
				if (bundledFareDTO != null) {					
					bundledFareDTO.setBundleFareDescriptionTemplateDTO(null);
				}
			}
		}
	}
}
