package com.isa.thinair.xbe.core.web.generator.travagent;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class PostPaymentHTMLGenerator {

	private static Log log = LogFactory.getLog(AgentCollectionHTMLGenerator.class);

	private static String clientErrors;

	public String getInvoiceDetailsRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("PostPaymentHTMLGenerator getInvoiceDetailsRowHtml");
		StringBuffer sb = new StringBuffer();
		SimpleDateFormat SDF2 = new SimpleDateFormat("dd/MM/yyyy");
		String strAgentType = request.getParameter("AgentCode");

		if (!"".equals(request.getParameter("hdnAgentCode")) && request.getParameter("hdnAgentCode") != null) {
			strAgentType = request.getParameter("hdnAgentCode");
		}

		Collection colData = null;
		colData = ModuleServiceLocator.getInvoicingBD().getUnsettledInvoicesForAgent(strAgentType);

		DecimalFormat formatter = new DecimalFormat("#.00");
		if (colData != null) {
			Iterator iter = colData.iterator();
			Invoice invoice = null;
			int count = 0;
			while (iter.hasNext()) {
				invoice = (Invoice) iter.next();
				sb.append("arrInvoice[" + count + "] = new Array();");
				sb.append("arrInvoice[" + count + "][1] = '" + SDF2.format(invoice.getInvoiceDate()) + "';");
				if (invoice.getInvoiceNumber() != null) {
					sb.append("arrInvoice[" + count + "][2] = '" + invoice.getInvoiceNumber() + "';");
				} else {
					sb.append("arrInvoice[" + count + "][2] = '0';");
				}
				if (invoice.getInvoiceAmount() != null) {
					sb.append("arrInvoice[" + count + "][3] = '" + formatter.format(invoice.getInvoiceAmount()) + "';");
				} else {
					sb.append("arrInvoice[" + count + "][3] = '00.00';");
				}

				if (invoice.getSettledAmount() != null) {
					sb.append("arrInvoice[" + count + "][4] = '" + formatter.format(invoice.getSettledAmount()) + "';");
				} else {
					sb.append("arrInvoice[" + count + "][4] = '00.00';");
				}
				sb.append("arrInvoice[" + count + "][5] = '"
						+ formatter.format(AccelAeroCalculator.subtract(invoice.getInvoiceAmount(), invoice.getSettledAmount()))
						+ "';");
				sb.append("arrInvoice[" + count + "][6]='0';");

				if (invoice.getCommissionAmount() != null) {
					sb.append("arrInvoice[" + count + "][7] = '" + formatter.format(invoice.getCommissionAmount()) + "';");
				} else {
					sb.append("arrInvoice[" + count + "][7] = '00.00';");
				}
				
				if (invoice.getEntity().getEntityName() != null) {
					sb.append("arrInvoice[" + count + "][8] = '" + invoice.getEntity().getEntityName() + "';");
				} else {
					sb.append("arrInvoice[" + count + "][8] = '';");
				}
				count++;
			}
		}

		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.PostPayment.Date.Null", "DateEmpty");
			moduleErrs.setProperty("um.PostPayment.FutureDate", "FutureDate");
			moduleErrs.setProperty("um.PostPayment.Amount.null", "AmountEmpty");
			moduleErrs.setProperty("um.PostPayment.CollectPurpose.null", "PurposeEmpty");
			moduleErrs.setProperty("um.PostPayment.CollectType.Null", "TypeEmpty");
			moduleErrs.setProperty("um.PostPayment.ChqNo.null", "ChequeEmpty");
			moduleErrs.setProperty("um.ReversePayment.date.null", "DateFieldsEmpty");
			moduleErrs.setProperty("um.ReversePayment.no.updates", "NoUpdations");
			moduleErrs.setProperty("um.ReversePayment.reverse.field.blank", "ReverseFieldsBlank");
			moduleErrs.setProperty("um.ReversePayment.reverse.no.match", "NoMatch");
			moduleErrs.setProperty("um.InvoiceSettlement.Agent.null", "ISAgentNull");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.null", "ISAmountNull");
			moduleErrs.setProperty("um.ReversePayment.Agent.null", "RPAgentNull");
			moduleErrs.setProperty("um.ReversePayment.ReverseAmount.null", "RPAmountNull");
			moduleErrs.setProperty("um.ReversePayment.TotalAmountReversed.null", "RPTotalAmountReversedNull");
			moduleErrs.setProperty("um.ReversePayment.TotalAmountReversed.Exceed", "RPTotalAmountReversedExceeds");
			moduleErrs.setProperty("um.ReversePayment.ReasonForReversal.Null", "RPReasonForReversalNull");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "DeleteConfirm");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}
}
