package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.PaxAccountUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")

public class LoadPaxMCODetailsAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(LoadPaxMCODetailsAction.class);

	List<HashMap<String, Object>> paxMCOs;

	public String execute() throws Exception {
		Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(request.getParameter("respaxs"));
		LCCClientReservationPax pax = PaxAccountUtil.getSelectedPax(request.getParameter("paxID"), colpaxs);
		Integer pnrPaxID = Integer.valueOf(pax.getTravelerRefNumber().split("\\$")[1]);
		paxMCOs = ModuleServiceLocator.getChargeBD().getMCORecords(null, pnrPaxID);
		return S2Constants.Result.SUCCESS;
	}

	public List<HashMap<String, Object>> getPaxMCOs() {
		return paxMCOs;
	}

	public void setPaxMCOs(List<HashMap<String, Object>> paxMCOs) {
		this.paxMCOs = paxMCOs;
	}

}
