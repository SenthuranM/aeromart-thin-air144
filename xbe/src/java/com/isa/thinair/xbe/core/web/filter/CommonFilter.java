package com.isa.thinair.xbe.core.web.filter;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.util.PrivilegeUtil;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class CommonFilter extends BasicFilter {

	private static Log log = LogFactory.getLog(CommonFilter.class);

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String nonSecureUrl = BasicRH.getNonSecureUrl(request);
		String strStatus = (String) request.getSession().getAttribute("HTTP_STATUS");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (strStatus == null) {

			request.getSession().setAttribute("HTTP_STATUS", "DONE");
			response.sendRedirect(response.encodeRedirectURL(nonSecureUrl));
			return;

		} else if (request.getSession().getAttribute(WebConstants.SES_CURRENT_USER) == null) {

			String userId = request.getRemoteUser();
			request.getSession().setAttribute("HTTP_STATUS", null);
			try {
				User user = ModuleServiceLocator.getSecurityBD().getUser(userId);
				request.getSession().setAttribute(WebConstants.SES_CURRENT_USER, user);
				UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
				Map<String, String> mapPrivilegeIds = PrivilegeUtil.getXBEAndAdminUserPrivileges(user, userPrincipal);
				request.getSession().setAttribute(WebConstants.SES_PRIVILEGE_IDS, mapPrivilegeIds);

				LinkedHashMap<String, String> mapContents = new LinkedHashMap<String, String>();
				mapContents.put("UserID", user.getUserId());
				mapContents.put("RemoteAddr", getNullSafeValue(getRemoteAddr(request)));
				mapContents.put("RemoteHost", getNullSafeValue(getRemoteHost(request)));
				mapContents.put("X-Forwarded-For", getNullSafeValue(getHTTPHeaderValue(request, "X-Forwarded-For")));
				mapContents.put("User-Agent", getNullSafeValue(getHTTPHeaderValue(request, "User-Agent")));
				mapContents.put("Host", getNullSafeValue(getHTTPHeaderValue(request, "Host")));
				mapContents.put("SessionID", getNullSafeValue(request.getSession().getId()));

				BasicRH.audit(request, TasksUtil.SECURITY_LOG_IN, mapContents);

			} catch (ModuleException e) {
				String msg = BasicRH.getErrorMessage(e,userLanguage);
				JavaScriptGenerator.setServerError(request, XBEConfig.getServerMessage("default.server.error", userLanguage, null), "", "");

				log.error(msg, e);
				BasicRH.forward(req, res, errorPage);
				return;
			} catch (RuntimeException re) {
				String msg = BasicRH.getErrorMessage(re,userLanguage);

				log.error(msg, re);
				BasicRH.forward(req, res, errorPage);
				return;
			}

		}

		filterChain.doFilter(req, res);

	}

	private String getRemoteAddr(HttpServletRequest request) {
		String ipAddress = "";
		try {
			ipAddress = request.getRemoteAddr().toString();
		} catch (Exception ex) {
			ipAddress = "-";
		}
		return ipAddress;
	}

	private String getRemoteHost(HttpServletRequest request) {
		String remoteHost = "";
		try {
			remoteHost = request.getRemoteHost().toString();
		} catch (Exception ex) {
			remoteHost = "-";
		}
		return remoteHost;
	}

	private String getHTTPHeaderValue(HttpServletRequest request, String headerName) {
		String headerParamValue = "";
		try {
			headerParamValue = request.getHeader(headerName);
		} catch (Exception e) {
			headerParamValue = "-";
		}
		return headerParamValue;
	}

	private String getNullSafeValue(String value) {
		return StringUtils.trimToEmpty(value);
	}
}
