package com.isa.thinair.xbe.core.web.v2.action.voucher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.voucher.VoucherRequestHandler;

/**
 * @author chethiya
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Voucher.VOUCHER),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowVoucherAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(ShowVoucherAction.class);
	public String execute() {
		try {
			request.setAttribute(WebConstants.REQ_LANGUAGE_HTML, SelectListGenerator.createLanguageList());
		} catch (ModuleException e) {
			log.error("Failed Loading Language List", e);
		}
		return VoucherRequestHandler.execute(request);
	}
}
