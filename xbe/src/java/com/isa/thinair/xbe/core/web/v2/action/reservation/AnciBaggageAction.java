package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * @author mano
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciBaggageAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AnciBaggageAction.class);

	private boolean success = true;

	private String messageTxt;

	private String selectedFlightList;

	private LCCReservationBaggageSummaryTo resBaggageSummary;

	private FlightSearchDTO searchParams;

	private LCCBaggageResponseDTO baggageResponseDTO;

	private boolean modifyAncillary = false;

	private boolean isRequote = false;

	private String jsonOnds;

	private String pnr;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String strTxnIdntifier = null;
			SYSTEM system = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			String classOfService = null;
			List<BundledFareDTO> bundledFareDTOs = null;

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(selectedFlightList);
				system = fltSegBuilder.getSystem();
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
				classOfService = fltSegBuilder.getClassOfService();
				bundledFareDTOs = resInfo.getBundledFareDTOs();
				updateClassOfServiceInBaggageSummary(flightSegmentTOs, resInfo);
			} else {
				strTxnIdntifier = resInfo.getTransactionId();
				system = resInfo.getSelectedSystem();
				if (isRequote) {
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}
				classOfService = searchParams.getClassOfService();

				BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
						S2Constants.Session_Data.LCC_SES_BOOKING_CART);
				bundledFareDTOs = bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getOndBundledFareDTOs();

			}

			List<LCCBaggageRequestDTO> lccBaggageRequestDTOs = composeBaggageRequest(flightSegmentTOs, classOfService,
					strTxnIdntifier);

			baggageResponseDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableBaggages(lccBaggageRequestDTOs,
					strTxnIdntifier, system, "en", modifyAncillary,
					BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER),
					isRequote, ApplicationEngine.XBE, resInfo.getResBaggageSummary(), bundledFareDTOs, pnr,
					TrackInfoUtil.getBasicTrackInfo(request));

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("BAGGAGE REQ ERROR", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private void updateClassOfServiceInBaggageSummary(List<FlightSegmentTO> flightSegmentTOs, XBEReservationInfoDTO resInfo) {
		Map<String, String> classesOfService = new HashMap<String, String>();
		for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
			classesOfService.put(flightSegmentTo.getFlightRefNumber(), flightSegmentTo.getLogicalCabinClassCode());
		}

		LCCReservationBaggageSummaryTo resBaggageSummary = resInfo.getResBaggageSummary();
		if(resBaggageSummary !=null){
			resBaggageSummary.setClassesOfService(classesOfService);
		}
	}

	private List<LCCBaggageRequestDTO> composeBaggageRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCBaggageRequestDTO> bL = new ArrayList<LCCBaggageRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCBaggageRequestDTO brd = new LCCBaggageRequestDTO();
			brd.setTransactionIdentifier(txnId);
			if (cabinClass != null) {
				brd.setCabinClass(fst.getCabinClassCode()); // For make res
			} else {
				brd.setCabinClass(fst.getCabinClassCode()); // Modify Res or add anci
			}
			brd.setFlightSegment(fst);
			bL.add(brd);
		}
		return bL;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	@JSON(serialize = false)
	public LCCReservationBaggageSummaryTo getResBaggageSummary() {
		return resBaggageSummary;
	}

	public void setResBaggageSummary(LCCReservationBaggageSummaryTo resBaggageSummary) {
		this.resBaggageSummary = resBaggageSummary;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	/**
	 * @return the baggageResponseDTO
	 */
	public LCCBaggageResponseDTO getBaggageResponseDTO() {
		return baggageResponseDTO;
	}

	/**
	 * @param baggageResponseDTO
	 *            the baggageResponseDTO to set
	 */
	public void setBaggageResponseDTO(LCCBaggageResponseDTO baggageResponseDTO) {
		this.baggageResponseDTO = baggageResponseDTO;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
