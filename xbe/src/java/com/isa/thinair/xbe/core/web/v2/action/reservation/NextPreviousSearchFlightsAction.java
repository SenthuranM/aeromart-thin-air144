package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.OpenReturnInfo;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AvailabilityUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the availability search - Grid Navigation Next day or Previous Day
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class NextPreviousSearchFlightsAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(NextPreviousSearchFlightsAction.class);

	private FlightSearchDTO searchNavParams;

	private OpenReturnInfo openReturnInfo;

	private boolean isOpenReturn = false;

	private boolean success = true;

	private String messageTxt;

	private String direction = "depature";

	private String airportMessage;

	private Collection<AvailableFlightInfo> outboundFlights;

	private boolean transferSegment = false;

	private String groupPNR;

	private Set<String> busCarrierCodes;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			boolean hasPrivViewAvailability = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			boolean hasPrivInterlineSearch = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)
					&& AppSysParamsUtil.isLCCConnectivityEnabled();

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			String pnr = request.getParameter("pnr");

			// create the initial availableSearchDTO form user parameters
			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchNavParams);
			if ("return".equalsIgnoreCase(direction)) {
				ReservationBeanUtil.updateOnDCitySearchInfoForNextPrev(flightAvailRQ, searchNavParams.getOndWiseCitySearch());
			}			
			flightAvailRQ.setTransactionIdentifier(resInfo.getTransactionId());
			flightAvailRQ.getAvailPreferences().setRestrictionLevel(
					AvailabilityConvertUtil.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats,
							searchNavParams.getBookingType()));
			flightAvailRQ.getAvailPreferences().setQuoteFares(false);

			if (isGroupPNR && !transferSegment) {
				// Collection<LCCClientReservationSegment> colsegs =
				// ModifyReservationJSONUtil.transformJsonSegments(request.getParameter("resSegments"));
				// flightAvailRQ.getAvailPreferences().setParticipatingCarriers(ReservationUtil.getParticipatingCarriers(request,
				// colsegs,
				// request.getParameter("modifySegment")));
			}

			if (searchNavParams.getHubTimeDetailJSON() != null && !searchNavParams.getHubTimeDetailJSON().equals("")
					&& direction.equalsIgnoreCase("return")) {
				ReservationBeanUtil.setHubTimeDetails(flightAvailRQ.getOriginDestinationInformation(0),
						searchNavParams.getHubTimeDetailJSON(), false);
			}

			if (transferSegment) {
				ReservationUtil.updateTransferSegmentData(flightAvailRQ, isGroupPNR,
						request.getParameter("modifyingSegmentOperatingCarrier"));
			}

			SYSTEM searchSystem = flightAvailRQ.getAvailPreferences().getSearchSystem();
			ReservationProcessParams rParm = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
			if (searchSystem == SYSTEM.ALL || searchSystem == SYSTEM.COND) {
				if (!hasPrivInterlineSearch) {
					searchSystem = SYSTEM.AA;
				}
			} else if (searchSystem == SYSTEM.INT && !hasPrivInterlineSearch) {
				searchSystem = SYSTEM.NONE;
			}

			flightAvailRQ.getAvailPreferences().setSearchSystem(searchSystem);
			// flightAvailRQ.getAvailPreferences().setForceHalfReturnSearch(true);
			// This is really bad. we should be able to use only one boolean.
			boolean isHalfReturnQuote = flightAvailRQ.getAvailPreferences().getInverseSegmentsList() == null ? false
					: !flightAvailRQ.getAvailPreferences().getInverseSegmentsList().isEmpty();
			flightAvailRQ.getAvailPreferences().setForceHalfReturnSearch(isHalfReturnQuote);
			flightAvailRQ.getAvailPreferences().setInboundFareModified(direction.equalsIgnoreCase("return"));
			if (pnr != null && !"".equals(pnr)) {
				flightAvailRQ.getAvailPreferences().setModifyBooking(true);
				ReservationBeanUtil.setExistingSegInfo(flightAvailRQ, request.getParameter("resSegments"),
						request.getParameter("modifySegment"));

				if (!isGroupPNR) {

					Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
							.getParameter("resSegments"));

					// FIXME CLEAN UP
					// Duplicates with ReservationBeanUtil.setExistingSegInfo
					if (searchNavParams.isOpenReturnConfirm()) {
						Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs,
								request.getParameter("modifySegment"), rParm, true);
						Integer oldFareID = 0;
						Integer oldFareType = 0;
						BigDecimal oldFareAmount = BigDecimal.ZERO;
						for (LCCClientReservationSegment seg : colModSegs) {
							oldFareID = seg.getFareTO().getFareId();
							oldFareType = seg.getFareTO().getFareRuleID();
							oldFareAmount = seg.getFareTO().getAmount();
						}
						flightAvailRQ.getAvailPreferences().setOldFareID(oldFareID);
						flightAvailRQ.getAvailPreferences().setOldFareType(oldFareType);
						flightAvailRQ.getAvailPreferences().setOldFareAmount(oldFareAmount);
					}
					Date firstDepatureDate = null;
					Date lastArrivalDate = null;
					for (LCCClientReservationSegment seg : colsegs) {
						if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
							if (firstDepatureDate == null || seg.getDepartureDateZulu().before(firstDepatureDate)) {
								firstDepatureDate = seg.getDepartureDateZulu();
							}
							if (lastArrivalDate == null || seg.getArrivalDateZulu().after(lastArrivalDate)) {
								lastArrivalDate = seg.getDepartureDateZulu();
							}
						}
					}
					flightAvailRQ.getAvailPreferences().setFirstDepatureDate(firstDepatureDate);
					flightAvailRQ.getAvailPreferences().setLastArrivalDate(lastArrivalDate);

					if (rParm.isGroundServiceEnabled() && request.getParameter("addSegment") != null
							&& request.getParameter("addSegment").equals("true")
							&& request.getParameter("addGroundSegment") != null
							&& request.getParameter("addGroundSegment").equals("true")) {
						if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
							List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
							originDestinationInformationList.add(originDestinationInfoWithConnectionValidations(flightAvailRQ
									.getOriginDestinationInformationList().iterator().next()));
							flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
						}
					}

				} else {
					if (request.getParameter("addSegment") != null && request.getParameter("addSegment").equals("true")
							&& request.getParameter("addGroundSegment") != null
							&& request.getParameter("addGroundSegment").equals("true")) {
						if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
							List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
							originDestinationInformationList.add(originDestinationInfoWithConnectionValidations(flightAvailRQ
									.getOriginDestinationInformationList().iterator().next()));
							flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
						}
					}
				}

			}

			flightAvailRQ.getAvailPreferences().setSearchSystem(
					AvailabilityUtil.getSearchSystem(flightAvailRQ.getOriginDestinationInformationList(), getTrackInfo(), pnr,
							isGroupPNR, hasPrivInterlineSearch));

			if (flightAvailRQ.getTravelPreferences().isOpenReturnConfirm()) {
				ReservationUtil.setOpenReturnConfirmInfo(flightAvailRQ, pnr, getTrackInfo());
			}

			if (pnr != null && !"".equals(pnr)) {
				AnalyticsLogger.logModSearch(AnalyticSource.XBE_MOD_NXTPREV, flightAvailRQ, request, pnr, getTrackInfo());
			} else {
				AnalyticsLogger.logAvlSearch(AnalyticSource.XBE_CRE_NXTPREV, flightAvailRQ, request, getTrackInfo());
			}

			FlightAvailRS flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(
					flightAvailRQ, getTrackInfo(), false);

			boolean enforceSeatAvailable = false;
			if (flightAvailRQ.getAvailPreferences().getStayOverTimeInMillis() > 0
					&& !flightAvailRQ.getTravelPreferences().isOpenReturn()
					&& !BookingClass.BookingClassType.STANDBY.equals(flightAvailRQ.getTravelPreferences().getBookingType())) {
				// Next prev for any one segment of a return booking
				flightAvailRQ.getAvailPreferences().setHalfReturnFareQuote(
						AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());

				enforceSeatAvailable = flightAvailRQ.getAvailPreferences().isHalfReturnFareQuote() ? false : true;
				// if half return search enabled for return booking, consider the actual seat availability, else
				// forcefully return true
			}
			busCarrierCodes = new HashSet<String>();

			outboundFlights = ReservationBeanUtil
					.createAvailFlightInfoList(flightAvailRS, searchNavParams, hasPrivViewAvailability, (flightAvailRQ
							.getAvailPreferences().getRestrictionLevel() == AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION),
							direction, true, enforceSeatAvailable, busCarrierCodes);

			busCarrierCodes.addAll(SelectListGenerator.getBusCarrierCodes());

			isOpenReturn = (flightAvailRS.getOpenReturnOptionsTO() != null);
			if (isOpenReturn) {
				openReturnInfo = new OpenReturnInfo(flightAvailRS.getOpenReturnOptionsTO(), searchNavParams.getFromAirport(),
						searchNavParams.getToAirport());
			}

			// Hide Stop Over Airport
			if (AppSysParamsUtil.isHideStopOverEnabled()) {
				hideStopOverAirport(outboundFlights);
			}

			// Load Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightAvailRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, null);

			if (searchSystem == SYSTEM.INT) {
				if (flightAvailRS != null && flightAvailRS.getReservationParms() != null) {
					rParm.enableInterlineModificationParams(flightAvailRS.getReservationParms(),
							resInfo != null ? resInfo.getReservationStatus() : null, null, true, null);
				}
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_ACCESSED, Boolean.TRUE);
			}

			// ReservationProcessParams again updated here when new FlightAvailRS retreived
			// important when no segment on first search and loading via next/previous then SESSION
			// ReservationProcessParams should be updated
			// TODO ReservationProcessParams usages must be Re-factored to load from the session.
			request.getSession().setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, rParm);

		} catch (Exception e) {

			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private void hideStopOverAirport(Collection<AvailableFlightInfo> availableFlightInfo) {
		for (AvailableFlightInfo outboundFlight : availableFlightInfo) {
			String ondCode = outboundFlight.getOndCode();
			outboundFlight.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode));
			List<String> segCodeList = outboundFlight.getSegmentCodeList();
			for (int index = 0; index < segCodeList.size(); index++) {
				segCodeList.set(index, ReservationApiUtils.hideStopOverSeg(segCodeList.get(index)));
			}
		}
	}

	private OriginDestinationInformationTO originDestinationInfoWithConnectionValidations(
			OriginDestinationInformationTO originDestinationInformation) throws ModuleException {

		Calendar valiedTimeFrom = Calendar.getInstance();
		valiedTimeFrom.setTimeInMillis(Long.valueOf(searchNavParams.getBusConValiedTimeFrom()));
		Calendar valiedTimeTo = Calendar.getInstance();
		valiedTimeTo.setTimeInMillis(Long.valueOf(searchNavParams.getBusConValiedTimeTo()));

		if (searchNavParams.isBusForDeparture()) {
			originDestinationInformation.setArrivalDateTimeStart(valiedTimeFrom.getTime());
			originDestinationInformation.setArrivalDateTimeEnd(valiedTimeTo.getTime());
		} else {
			Calendar dptStart = Calendar.getInstance();
			dptStart.setTime(originDestinationInformation.getDepartureDateTimeStart());
			Calendar dptEnd = Calendar.getInstance();
			dptEnd.setTime(originDestinationInformation.getDepartureDateTimeEnd());

			// Valied times should be within the search date.
			List<Calendar> valiedPeriod = CommonUtil.valiedPeriod(dptStart, dptEnd, valiedTimeFrom, valiedTimeTo);
			if (valiedPeriod.size() == 2) {
				originDestinationInformation.setDepartureDateTimeStart(valiedPeriod.get(0).getTime());
				originDestinationInformation.setDepartureDateTimeEnd(valiedPeriod.get(1).getTime());
			}
		}

		return originDestinationInformation;
	}

	private void clearValues() {

		outboundFlights = null;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	// setters
	public void setSearchNavParams(FlightSearchDTO searchNavParams) {
		this.searchNavParams = searchNavParams;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchNavParams() {
		return searchNavParams;
	}

	public Collection<AvailableFlightInfo> getOutboundFlights() {
		return outboundFlights;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	/**
	 * @return the airportMessage
	 */
	public String getAirportMessage() {
		return airportMessage;
	}

	public OpenReturnInfo getOpenReturnInfo() {
		return openReturnInfo;
	}

	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	public void setTransferSegment(boolean transferSegment) {
		this.transferSegment = transferSegment;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public Set<String> getBusCarrierCodes() {
		return busCarrierCodes;
	}
}
