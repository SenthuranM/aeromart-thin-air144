package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys.MakeResPrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.assembler.OndConvertAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.OpenReturnInfo;
import com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.FareTypeInfoTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AvailabilityUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the availability search
 * 
 * @author Lasantha Pambagoda
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AvailabilitySearchAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AvailabilitySearchAction.class);

	private FlightSearchDTO searchParams;

	private boolean success = true;

	private Collection<AvailableFlightInfo> outboundFlights;

	private Collection<AvailableFlightInfo> returnFlights;

	private Collection<FlightInfoTO> flightInfo;

	private String messageTxt;

	private OpenReturnInfo openReturnInfo;

	private FareTypeInfoTO availableFare;

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private boolean isOpenReturn = false;

	private boolean currencyRound = false;

	private boolean showFareDiscountCodes = false;

	private List<String[]> fareDiscountCodeList = null;

	private boolean modifyBooking = false;

	private boolean addSegment = false;

	private String airportMessage;

	private boolean transferSegment = false;

	private String groupPNR;

	private boolean addGroundSegment = false;

	private String selectedFltSeg;

	private ReservationProcessParams rpParams;

	private Set<String> busCarrierCodes;

	private String classOfServiceDesc;

	private DiscountedFareDetails appliedFareDiscount = null;

	private String dummyCreditDiscountAmount = "0.00";

	private boolean showAgentCommission = false;

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();

	private boolean serviceTaxApplicable = false;

	private boolean isFlexiEnableInAnci = true;

	private HashMap<String, List<String>> segmentsMapPerSegType;

	private BaggageRatesDTO baggageRatesDTO;
	
	private boolean displayBasedOnTemplates = false;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			boolean hasPrivViewAvailability = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			// TODO charith : should be able to remove
			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			boolean hasPrivInterlineSearch = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)
					&& AppSysParamsUtil.isLCCConnectivityEnabled();
			boolean isAllowFlightSearchAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);
			boolean isAllowDomesticOpenReturnBooking = BasicRH.hasPrivilege(request,
					PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_MAKE_OPEN_RETURN_BOOKINGS_FOR_DOMESTIC_FLIGHTS);
			boolean hasPrivAddSeatsInCutover = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ADD_SEATS_IN_CUTOVER);
			boolean isAllowTillFinalCutOver = BasicRH.hasPrivilege(request,
					MakeResPrivilegesKeys.ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER);
			boolean bypassMaxPaxLimit = BasicRH.hasPrivilege(request,
					PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_BYPASS_MAX_PAX_LIMIT);
			boolean isDomesticRoute = true;

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			ReservationProcessParams resParm = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);

			FlightAvailRS flightAvailRS = new FlightAvailRS();
			
			
			int maxAdultAllowed = WebplatformUtil.getMaxAdultCount(((UserPrincipal) request.getUserPrincipal()).getUserId(), "XBE");
			
			if (!bypassMaxPaxLimit && maxAdultAllowed < (searchParams.getAdultCount())) {
				throw new ModuleException("error.invalid.pax.count");
			}

			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
			flightAvailRQ.getAvailPreferences().setRestrictionLevel(AvailabilityConvertUtil
					.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats, searchParams.getBookingType()));

			flightAvailRQ.getAvailPreferences().setQuoteFares(true);
			flightAvailRQ.getAvailPreferences().setAllowFlightSearchAfterCutOffTime(isAllowFlightSearchAfterCutOffTime);
			flightAvailRQ.getAvailPreferences().setHasPrivAddSeatsInCutover(hasPrivAddSeatsInCutover);
			flightAvailRQ.getAvailPreferences().setAllowTillFinalCutOver(isAllowTillFinalCutOver);
			if (ReservationUtil.isCharterAgent(request)) {
				flightAvailRQ.getAvailPreferences().setFixedFareAgent(true);
			}

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (isGroupPNR) {
				flightAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}

			if (transferSegment) {
				ReservationUtil.updateTransferSegmentData(flightAvailRQ, isGroupPNR,
						request.getParameter("modifyingSegmentOperatingCarrier"));
			}

			/*
			 * Check the privileges to search preferred system. In case of not enough privilege gracefully cut down to
			 * match the available privileges
			 */
			SYSTEM searchSystem = flightAvailRQ.getAvailPreferences().getSearchSystem();

			if (searchSystem == SYSTEM.ALL || searchSystem == SYSTEM.COND) {
				if (!hasPrivInterlineSearch) {
					searchSystem = SYSTEM.AA;
				}
			} else if (searchSystem == SYSTEM.INT && !hasPrivInterlineSearch) {
				searchSystem = SYSTEM.NONE;
			}
			flightAvailRQ.getAvailPreferences().setSearchSystem(searchSystem);

			// Set default stay over for half return variance search
			setDefaultStayOverDaysForReturnFareSearch(searchParams, flightAvailRQ);
			String pnr = request.getParameter("pnr");
			if (pnr != null && !"".equals(pnr)) {
				flightAvailRQ.getAvailPreferences().setModifyBooking(true);
				flightAvailRQ.getAvailPreferences().setAddSegment(addSegment);
				ReservationBeanUtil.setExistingSegInfo(flightAvailRQ, request.getParameter("resSegments"),
						request.getParameter("modifySegment"));
				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil
						.transformJsonSegments(request.getParameter("resSegments"));
				// FIXME
				// THIS block of code is a contain duplicate set of operations carried out by
				// ReservationBeanUtil.setExistingSegInfo
				// SHOULD BE CLEANED UP
				if ((!addSegment) || searchParams.isOpenReturnConfirm()) {
					Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs,
							request.getParameter("modifySegment"), resParm, true);
					Integer oldFareID = null;
					Integer oldFareType = null;
					BigDecimal oldFareAmount = BigDecimal.ZERO;
					String carrierCode = null;
					for (LCCClientReservationSegment seg : colModSegs) {
						oldFareID = seg.getFareTO().getFareId();
						oldFareType = seg.getFareTO().getFareRuleID();
						oldFareAmount = seg.getFareTO().getAmount();
						carrierCode = seg.getFareTO().getCarrierCode();
						break;
					}
					flightAvailRQ.getAvailPreferences().setOldFareID(oldFareID);
					flightAvailRQ.getAvailPreferences().setOldFareType(oldFareType);
					flightAvailRQ.getAvailPreferences().setOldFareAmount(oldFareAmount);
					flightAvailRQ.getAvailPreferences().setOldFareCarrierCode(carrierCode);
				}

				if (!this.validateAddGroundSegment(searchParams, userLanguage)) {
					return S2Constants.Result.SUCCESS;
				}
				if (isGroupPNR) {
					// No longer required as use can do seemless modifications
					// flightAvailRQ.getAvailPreferences().setParticipatingCarriers(ReservationUtil.getParticipatingCarriers(request,
					// colsegs,
					// request.getParameter("modifySegment")));

					if (addSegment && addGroundSegment) {
						if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
							List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
							originDestinationInformationList.add(groundSegmentInfoWithConnectionValidationsForDry(colsegs,
									flightAvailRQ.getOriginDestinationInformationList().iterator().next()));
							flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
						}
					}

				} else {

					Date firstDepatureDate = null;
					Date lastArrivalDate = null;
					for (LCCClientReservationSegment seg : colsegs) {
						if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
							if (firstDepatureDate == null || seg.getDepartureDateZulu().before(firstDepatureDate)) {
								firstDepatureDate = seg.getDepartureDateZulu();
							}
							if (lastArrivalDate == null || seg.getArrivalDateZulu().after(lastArrivalDate)) {
								lastArrivalDate = seg.getDepartureDateZulu();
							}
						}
					}
					flightAvailRQ.getAvailPreferences().setFirstDepatureDate(firstDepatureDate);
					flightAvailRQ.getAvailPreferences().setLastArrivalDate(lastArrivalDate);

					// This validation is need to be done for dry bus segments also
					// Re calculate arrival, departure start/end times when adding bus segments - DONE
					// TODO dry validation "groundSegmentInfoWithConnectionValidationsForDry" should be refactored
					// when all cases (own/dry) O & D validation calculated by common validation method
					if (AppSysParamsUtil.isGroundServiceEnabled() && addSegment && addGroundSegment) {
						flightAvailRQ.getAvailPreferences().setAddGroundSegment(true);
						if (flightAvailRQ.getOriginDestinationInformationList().iterator().hasNext()) {
							List<OriginDestinationInformationTO> originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
							originDestinationInformationList.add(originDestinationInfoWithConnectionValidations(colsegs,
									flightAvailRQ.getOriginDestinationInformationList().iterator().next()));
							flightAvailRQ.setOriginDestinationInformationList(originDestinationInformationList);
						}
					}
				}

				flightAvailRQ.getAvailPreferences()
						.setSearchSystem(AvailabilityUtil.getSearchSystem(flightAvailRQ.getOriginDestinationInformationList(),
								getTrackInfo(), pnr, isGroupPNR, hasPrivInterlineSearch));

				if (flightAvailRQ.getTravelPreferences().isOpenReturnConfirm()) {
					ReservationUtil.setOpenReturnConfirmInfo(flightAvailRQ, pnr, getTrackInfo());
				}

			}

			flightAvailRQ.getAvailPreferences().setAllowDoOverbookBeforeCutOffTime(resParm.isAllowOverBook());
			flightAvailRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(resParm.isAllowOverBookAfterCutoffTime());
			if (((!modifyBooking && !addGroundSegment) || (!addGroundSegment && addSegment))
					|| (modifyBooking && !addGroundSegment)) {
				if (!AddModifySurfaceSegmentValidations.isValidSearchForFreshBooking(searchParams)) {
					success = false;
					messageTxt = XBEConfig.getClientMessage("um.surfacesegment.add.searchonly.invalid", userLanguage);
					return S2Constants.Result.SUCCESS;
				}
			}

			if ((modifyBooking || addSegment) && AddModifySurfaceSegmentValidations.isSubStationMissing(searchParams)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.surfacesegment.modify.search.invalid", userLanguage);
				return S2Constants.Result.SUCCESS;
			}

			AnalyticsLogger.logAvlSearch(AnalyticSource.XBE_CRE_AVL, flightAvailRQ, request, getTrackInfo());

			flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlightsWithAllFares(flightAvailRQ,
					getTrackInfo());
			
			
			/* Convert response to UI friendly DTO's */
			if (searchSystem == SYSTEM.INT) {
				if (flightAvailRS != null && flightAvailRS.getReservationParms() != null) {
					resParm.enableInterlineModificationParams(flightAvailRS.getReservationParms(),
							resInfo != null ? resInfo.getReservationStatus() : null, null, true, null);
				}
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_ACCESSED, Boolean.TRUE);
			}

			busCarrierCodes = new HashSet<String>();
			outboundFlights = ReservationBeanUtil.createAvailFlightInfoList(flightAvailRS, searchParams, hasPrivViewAvailability,
					(flightAvailRQ.getAvailPreferences()
							.getRestrictionLevel() == AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION),
					"depature", false, false, busCarrierCodes);
			returnFlights = ReservationBeanUtil.createAvailFlightInfoList(flightAvailRS, searchParams, hasPrivViewAvailability,
					(flightAvailRQ.getAvailPreferences()
							.getRestrictionLevel() == AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION),
					"return", false, false, busCarrierCodes);

			isOpenReturn = (flightAvailRS.getOpenReturnOptionsTO() != null);

			if (isOpenReturn) {
				for (AvailableFlightInfo availableFlight : returnFlights) {
					for (Boolean isDomesticFlight : availableFlight.getDomesticFlightList()) {
						if (!isDomesticFlight) {
							isDomesticRoute = false;
							break;
						}
					}
				}

				if ((isAllowDomesticOpenReturnBooking && isDomesticRoute) || (!isDomesticRoute)) {
					openReturnInfo = new OpenReturnInfo(flightAvailRS.getOpenReturnOptionsTO(), searchParams.getFromAirport(),
							searchParams.getToAirport());
				} else {
					throw new Exception("error.domestic.openrt.bookings.not.allowed");
				}
			}

			PriceInfoTO priceInfoTO = flightAvailRS.getSelectedPriceFlightInfo();

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			if (bookingShoppingCart == null) {
				bookingShoppingCart = new BookingShoppingCart(
						modifyBooking ? ChargeRateOperationType.MODIFY_ONLY : ChargeRateOperationType.MAKE_ONLY);
			}
			bookingShoppingCart.setPriceInfoTO(priceInfoTO);
			bookingShoppingCart.setPayablePaxCount(searchParams.getAdultCount() + searchParams.getChildCount());
			bookingShoppingCart.addNewServiceTaxes(flightAvailRS.getApplicableServiceTaxes());

			Collection<EXTERNAL_CHARGES> resettingExtChargee = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
			resettingExtChargee.add(EXTERNAL_CHARGES.CREDIT_CARD);
			resettingExtChargee.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
			bookingShoppingCart.resetSelectedExternalChargeAmount(resettingExtChargee);

			resInfo.setTransactionId(flightAvailRS.getTransactionIdentifier());
			resInfo.setBookingType(flightAvailRQ.getTravelPreferences().getBookingType());
			currencyRound = CommonUtil.enableCurrencyRounding(searchParams.getSelectedCurrency());
			showFareDiscountCodes = AppSysParamsUtil.enableFareDiscountCodes();
			if (showFareDiscountCodes) {
				fareDiscountCodeList = SelectListGenerator.createFareDiscountCodeList();
			}
			showAgentCommission = AppSysParamsUtil.isAgentCommmissionEnabled();

			boolean restrictCommissionVisibility = false;
			if (modifyBooking && !addSegment) {
				restrictCommissionVisibility = true;
			}

			if (priceInfoTO != null) {
				// handling charge is not considered for modifications
				if (isCalculateHandlingFee()) {
					this.calculateTotalHandlingFee();
				}
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				BigDecimal handlingCharge = BigDecimal.ZERO;

				bookingShoppingCart.setFareDiscount(flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());

				if (bookingShoppingCart.getFareDiscount() != null
						&& !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE
								.equals(bookingShoppingCart.getFareDiscount().getPromotionType())) {
					ReservationUtil.calculateDiscountForReservation(bookingShoppingCart, null, flightAvailRQ, getTrackInfo(),
							true, bookingShoppingCart.getFareDiscount(), priceInfoTO, flightAvailRS.getTransactionIdentifier());
				}

				if (!searchParams.isOpenReturnConfirm()) {
					handlingCharge = bookingShoppingCart.getHandlingCharge();
					handlingCharge = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, handlingCharge);

					BigDecimal promoDiscountAmt = null;
					promoDiscountAmt = bookingShoppingCart.getTotalFareDiscount(true);
					appliedFareDiscount = bookingShoppingCart.getFareDiscount();

					if (promoDiscountAmt == null || promoDiscountAmt.doubleValue() == 0) {
						BigDecimal actualDiscount = bookingShoppingCart.getTotalFareDiscount(false);
						if (actualDiscount != null) {
							dummyCreditDiscountAmount = actualDiscount.toPlainString();
						}
					}
					flightAvailRQ.setTransactionIdentifier(resInfo.getTransactionId());
					availableFare = ReservationUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(),
							flightAvailRS.getOriginDestinationInformationList(), handlingCharge, searchParams, resParm,
							promoDiscountAmt, exchangeRateProxy, priceInfoTO.getFareSegChargeTO(), flightAvailRQ, getTrackInfo(),
							null);
					bookingShoppingCart.setAgentCommissions(availableFare.getApplicableAgentCommissions());
					bookingShoppingCart.setTotAgentCommission(availableFare.getTotAgentCommission());
					bookingShoppingCart.setRestrictCommissionVisibility(restrictCommissionVisibility);
					
					String serviceTax= availableFare.getFareQuoteSummaryTO().getTotalServiceTax();
					request.getSession().setAttribute("serviceTax", serviceTax);
				
				}
				
				boolean overideSubJourneyId = true; //TODO improve logic temp fix
				
				flightInfo = ReservationBeanUtil.createSelecteFlightInfo(flightAvailRS, null, null, null, overideSubJourneyId);
				segmentsMapPerSegType = ReservationBeanUtil.getSegmentsMap(flightInfo);
				// populateFlightRefNumber(); //TODO Check and remove if it's not required
				setClassOfServiceDesc(ReservationBeanUtil.getClassOfServiceDescription(flightInfo));

				List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
				ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);
				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());
								
				if (!AppSysParamsUtil.isFlexiEnabledInAnci()) {
					this.setFlexiEnableInAnci(false);
				}
			}

			displayBasedOnTemplates= AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed();
			
			if (modifyBooking && !(searchParams.isQuoteOutboundFlexi() && searchParams.isQuoteInboundFlexi())) {
				// set the flexi details only in the modify flow
				request.setAttribute("flexiAlertInfo", request.getParameter("flexiAlertInfo"));
			}

			// Hide Stop Over Airport
			if (AppSysParamsUtil.isHideStopOverEnabled()) {
				hideStopOverAirport(outboundFlights);
				hideStopOverAirport(returnFlights);

				getHideStopOverAirportForFareQuote(availableFare);

				if (flightInfo != null) {
					for (FlightInfoTO flightInfoTo : flightInfo) {
						String originDest = flightInfoTo.getOrignNDest();
						if (originDest != null && originDest.split("/").length > 2) {
							flightInfoTo.setOrignNDest(ReservationApiUtils.hideStopOverSeg(originDest));
						}
					}
				}
			}

			// CalculateAdminFee
			// in Availabilty page pax details are not availiable,
			// we took dummy values for paxState,paxCountryCode and paxTaxRegistered.
			if (availableFare != null && AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE,
							((UserPrincipal) request.getUserPrincipal()).getAgentCode())) {
				FareQuoteSummaryTO fareQuoteSummaryTo = availableFare.getFareQuoteSummaryTO();
				BigDecimal administrationFee = ReservationUtil.calclulateAdministrationFee(bookingShoppingCart, request, false,
						resInfo.getTransactionId(), getTrackInfo(), searchParams, flightAvailRS.getAllFlightSegments(), null,
						null, false);
				fareQuoteSummaryTo.setAdminFee(administrationFee.toString());
				// converting Admin Fee to selected Currency and adding it to total price.
				String selectedCurrency = searchParams.getSelectedCurrency();

				CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
						.getCurrencyExchangeRate(selectedCurrency);
				Currency currency = currencyExchangeRate.getCurrency();
				fareQuoteSummaryTo.setSelectedEXRate(currencyExchangeRate.getExrateBaseToCurNumber().toPlainString());
				BigDecimal total = new BigDecimal(fareQuoteSummaryTo.getTotalPrice());
				BigDecimal selectedTotPrice = AccelAeroCalculator.add(total, administrationFee);
				fareQuoteSummaryTo.setSelectedtotalPrice(AccelAeroCalculator
						.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
								selectedTotPrice, currency.getBoundryValue(), currency.getBreakPoint())));
				// to display admin fee in selected currency
				fareQuoteSummaryTo.setSelectCurrAdminFee(AccelAeroCalculator
						.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
								administrationFee, currency.getBoundryValue(), currency.getBreakPoint())));
				// Adding AdminFee to total price.
				fareQuoteSummaryTo
						.setTotalPrice(new BigDecimal(fareQuoteSummaryTo.getTotalPrice()).add(administrationFee).toString());
			}

			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightAvailRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, null);

			// ReservationProcessParams is initialized in many places, by adding this to session can eliminate the
			// Unnecessary call.
			// TODO ReservationProcessParams usages must be Re-factored to load from the session.
			request.getSession().setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, resParm);
			this.setBaggageRatesDTO(
					ModuleServiceLocator.getAirproxyAncillaryBD().getBaggageRates(getTrackInfo(), flightAvailRS,
					searchParams.getClassOfService(), searchParams.getClassOfService(), flightAvailRQ.getAvailPreferences().getSearchSystem().toString(),
							ApplicationEngine.XBE, userLanguage));
			rpParams = resParm;
			setServiceTaxApplicable(isServiceTaxApplicableForOnd());
			
		} catch (Exception e) {
			clearValues();
			success = false;
			if (e instanceof ModuleException) {
				if ("error.openrt.confirm.before.outbound.notallowed".equals(((ModuleException) e).getExceptionCode())) {
					messageTxt = XBEConfig.getClientMessage("error.openrt.confirm.before.outbound.notallowed", userLanguage);
				} else {
					messageTxt = BasicRH.getErrorMessage(e,userLanguage);
				}
				
				if ("error.invalid.pax.count".equals(((ModuleException) e).getExceptionCode())) {
					messageTxt = XBEConfig.getClientMessage("error.invalid.pax.count", userLanguage);
				} else {
					messageTxt = BasicRH.getErrorMessage(e, userLanguage);
				}
			} else {
				if (("error.domestic.openrt.bookings.not.allowed").equals(e.getMessage())) {
					messageTxt = XBEConfig.getClientMessage(e.getMessage(), userLanguage);
				} else {
					messageTxt = BasicRH.getErrorMessage(e, userLanguage);
				}
			}
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}
		
	private void hideStopOverAirport(Collection<AvailableFlightInfo> availableFlightInfo) {
		for (AvailableFlightInfo outboundFlight : availableFlightInfo) {
			String ondCode = outboundFlight.getOndCode();
			outboundFlight.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode));
			List<String> segCodeList = outboundFlight.getSegmentCodeList();
			for (int index = 0; index < segCodeList.size(); index++) {
				segCodeList.set(index, ReservationApiUtils.hideStopOverSeg(segCodeList.get(index)));
			}
		}
	}

	private void getHideStopOverAirportForFareQuote(FareTypeInfoTO fareType) {
		if (fareType != null) {
			Collection<FareQuoteTO> FareQuoteTOs = fareType.getFareQuoteTO();
			for (FareQuoteTO fareQuoteTo : FareQuoteTOs) {
				String ondCode = fareQuoteTo.getOndCode();
				if (ondCode != null && ondCode.split("/").length > 2) {
					fareQuoteTo.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode), false);
				}
				Collection<SegmentFareTO> segmentWiseFares = fareQuoteTo.getSegmentWise();
				for (SegmentFareTO segmentFareTo : segmentWiseFares) {
					String fareSegment = segmentFareTo.getSegmentCode();
					segmentFareTo.setSegmentCode(ReservationApiUtils.hideStopOverSeg(fareSegment));
				}
			}
		}
	}

	private boolean validateAddGroundSegment(FlightSearchDTO searchDTO, String lang) throws ModuleException {
		if (this.addGroundSegment) {
			if (!AddModifySurfaceSegmentValidations.isValidAddGroundSegment(searchDTO)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.surfacesegment.add.search.invalid", lang);
				return false;
			}
		}
		return true;
	}

	private OriginDestinationInformationTO originDestinationInfoWithConnectionValidations(
			Collection<LCCClientReservationSegment> colsegs, OriginDestinationInformationTO originDestinationInformation)
			throws ModuleException {

		String[] selectedSegs = selectedFltSeg.split(",");
		String[] selectedSeg = selectedSegs[0].split("\\$");

		ArrayList<Integer> arrFlightSegId = new ArrayList<Integer>();
		for (LCCClientReservationSegment resSeg : colsegs) {
			if (resSeg.getBookingFlightSegmentRefNumber().equals(selectedSeg[0])) {
				arrFlightSegId.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));
				break;
			}
		}

		Collection<FlightSegmentDTO> flightSegments = ModuleServiceLocator.getFlightServiceBD().getFlightSegments(arrFlightSegId);
		Iterator<FlightSegmentDTO> segIter = flightSegments.iterator();
		FlightSegmentDTO flightSegment = null;
		String[] minMaxTransitDurations = null;
		Calendar valiedTimeFrom = Calendar.getInstance();
		Calendar valiedTimeTo = Calendar.getInstance();
		boolean busForDeparture = false;

		if (segIter.hasNext()) {
			flightSegment = segIter.next();
		}

		String depBusSegAirport = originDestinationInformation.getOrigin();
		String arrBusSegAirport = originDestinationInformation.getDestination();

		Set<String> busConnectingAirports = CommonsServices.getGlobalConfig().getBusConnectingAirports();
		if (flightSegment != null && flightSegment.getSegmentCode() != null) {

			String depAirSegAirport = flightSegment.getSegmentCode().substring(0, 3);
			String arrAirSegAirport = flightSegment.getSegmentCode().substring(flightSegment.getSegmentCode().length() - 3);

			String airlineCarrier = flightSegment.getFlightNumber().substring(0, 2);

			String busCarrier = SelectListGenerator.getDefaultBusCarrierCode(airlineCarrier);

			// Departure airport of air segment is connecting to bus segments
			if (arrBusSegAirport.equalsIgnoreCase(depAirSegAirport) && busConnectingAirports.contains(depAirSegAirport)) {
				busForDeparture = true;
				minMaxTransitDurations = CommonsServices.getGlobalConfig()
						.getMixMaxTransitDurations(flightSegment.getSegmentCode().substring(0, 3), busCarrier, airlineCarrier);

				String[] minDurationHHMM = minMaxTransitDurations[1].split(":");
				valiedTimeFrom.setTime(flightSegment.getDepartureDateTime());
				valiedTimeFrom.add(Calendar.HOUR, -(Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, -(Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = minMaxTransitDurations[0].split(":");
				valiedTimeTo.setTime(flightSegment.getDepartureDateTime());
				valiedTimeTo.add(Calendar.HOUR, -(Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, -(Integer.parseInt(maxDurationHHMM[1])));

				if (originDestinationInformation != null) {
					originDestinationInformation.setArrivalDateTimeStart(valiedTimeFrom.getTime());
					originDestinationInformation.setArrivalDateTimeEnd(valiedTimeTo.getTime());
				}

			} else if (depBusSegAirport.equalsIgnoreCase(arrAirSegAirport) && busConnectingAirports.contains(arrAirSegAirport)) {
				minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
						flightSegment.getSegmentCode().substring(flightSegment.getSegmentCode().length() - 3), airlineCarrier,
						busCarrier);

				String[] minDurationHHMM = minMaxTransitDurations[0].split(":");
				valiedTimeFrom.setTime(flightSegment.getArrivalDateTime());
				valiedTimeFrom.add(Calendar.HOUR, (Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, (Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = minMaxTransitDurations[1].split(":");
				valiedTimeTo.setTime(flightSegment.getArrivalDateTime());
				valiedTimeTo.add(Calendar.HOUR, (Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, (Integer.parseInt(maxDurationHHMM[1])));

				if (originDestinationInformation != null) {
					Calendar dptStart = Calendar.getInstance();
					dptStart.setTime(originDestinationInformation.getDepartureDateTimeStart());
					Calendar dptEnd = Calendar.getInstance();
					dptEnd.setTime(originDestinationInformation.getDepartureDateTimeEnd());

					// Valied times should be within the search date.
					List<Calendar> valiedPeriod = CommonUtil.valiedPeriod(dptStart, dptEnd, valiedTimeFrom, valiedTimeTo);

					if (valiedPeriod.size() == 2) {
						originDestinationInformation.setDepartureDateTimeStart(valiedPeriod.get(0).getTime());
						originDestinationInformation.setDepartureDateTimeEnd(valiedPeriod.get(1).getTime());
					}
				}
			}
		}
		// Set required parameters for Next/Previous navigations
		searchParams.setBusForDeparture(busForDeparture);
		searchParams.setBusConValiedTimeFrom(String.valueOf(valiedTimeFrom.getTimeInMillis()));
		searchParams.setBusConValiedTimeTo(String.valueOf(valiedTimeTo.getTimeInMillis()));

		return originDestinationInformation;
	}

	/*
	 * groundSegmentInfoWithConnectionValidationsForDry - this validation should be applied when search for a ground
	 * segment - "AddBusSegment"
	 */
	private OriginDestinationInformationTO groundSegmentInfoWithConnectionValidationsForDry(
			Collection<LCCClientReservationSegment> colsegs, OriginDestinationInformationTO originDestinationInformation)
			throws ModuleException {

		String[] selectedSegs = selectedFltSeg.split(",");
		String[] selectedSeg = selectedSegs[0].split("\\$");

		LCCClientReservationSegment selectedSegment = null;

		for (LCCClientReservationSegment resSeg : colsegs) {
			if (resSeg.getBookingFlightSegmentRefNumber().equals(selectedSeg[0])) {
				selectedSegment = resSeg;
				break;
			}
		}

		Calendar valiedTimeFrom = Calendar.getInstance();
		Calendar valiedTimeTo = Calendar.getInstance();
		boolean busForDeparture = false;

		String arriAirportMinConTime = selectedSegment.getArriAirportMinConTime();
		String arriAirportMaxConTime = selectedSegment.getArriAirportMaxConTime();
		String depAirportMinConTime = selectedSegment.getDepAirportMinConTime();
		String depAirportMaxConTime = selectedSegment.getDepAirportMaxConTime();

		String depAirport = originDestinationInformation.getOrigin();
		String arrivalAirport = originDestinationInformation.getDestination();

		String segmentCode = selectedSegment.getSegmentCode();
		String[] airportCodes = null;

		if (!StringUtil.isNullOrEmpty(segmentCode)) {
			airportCodes = segmentCode.split("/");

			if (airportCodes[0].equalsIgnoreCase(arrivalAirport)
					&& (depAirportMinConTime != null && depAirportMaxConTime != null)) {
				// Departure Time
				busForDeparture = true;
				String[] minDurationHHMM = depAirportMaxConTime.split(":");
				valiedTimeFrom.setTime(selectedSegment.getDepartureDate());
				valiedTimeFrom.add(Calendar.HOUR, -(Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, -(Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = depAirportMinConTime.split(":");
				valiedTimeTo.setTime(selectedSegment.getDepartureDate());
				valiedTimeTo.add(Calendar.HOUR, -(Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, -(Integer.parseInt(maxDurationHHMM[1])));

				if (originDestinationInformation != null) {
					originDestinationInformation.setArrivalDateTimeStart(valiedTimeFrom.getTime());
					originDestinationInformation.setArrivalDateTimeEnd(valiedTimeTo.getTime());
				}

			} else if (airportCodes[1].equalsIgnoreCase(depAirport)
					&& (arriAirportMinConTime != null && arriAirportMaxConTime != null)) {
				// Arrival Time

				String[] minDurationHHMM = arriAirportMinConTime.split(":");
				valiedTimeFrom.setTime(selectedSegment.getArrivalDate());
				valiedTimeFrom.add(Calendar.HOUR, (Integer.parseInt(minDurationHHMM[0])));
				valiedTimeFrom.add(Calendar.MINUTE, (Integer.parseInt(minDurationHHMM[1])));

				String[] maxDurationHHMM = arriAirportMaxConTime.split(":");
				valiedTimeTo.setTime(selectedSegment.getArrivalDate());
				valiedTimeTo.add(Calendar.HOUR, (Integer.parseInt(maxDurationHHMM[0])));
				valiedTimeTo.add(Calendar.MINUTE, (Integer.parseInt(maxDurationHHMM[1])));

				if (originDestinationInformation != null) {
					Calendar dptStart = Calendar.getInstance();
					dptStart.setTime(originDestinationInformation.getDepartureDateTimeStart());
					Calendar dptEnd = Calendar.getInstance();
					dptEnd.setTime(originDestinationInformation.getDepartureDateTimeEnd());

					// Valied times should be within the search date.
					List<Calendar> valiedPeriod = CommonUtil.valiedPeriod(dptStart, dptEnd, valiedTimeFrom, valiedTimeTo);

					if (valiedPeriod.size() == 2) {
						originDestinationInformation.setDepartureDateTimeStart(valiedPeriod.get(0).getTime());
						originDestinationInformation.setDepartureDateTimeEnd(valiedPeriod.get(1).getTime());
					}
				}

			}
		}

		// Set required parameters for Next/Previous navigations
		searchParams.setBusForDeparture(busForDeparture);
		searchParams.setBusConValiedTimeFrom(String.valueOf(valiedTimeFrom.getTimeInMillis()));
		searchParams.setBusConValiedTimeTo(String.valueOf(valiedTimeTo.getTimeInMillis()));

		return originDestinationInformation;
	}

	private void setDefaultStayOverDaysForReturnFareSearch(FlightSearchDTO searchParams, FlightAvailRQ flightAvailRQ) {
		OndConvertAssembler ondAssm = new OndConvertAssembler(flightAvailRQ.getOriginDestinationInformationList());
		if (ondAssm.isReturnFlight() && !flightAvailRQ.getTravelPreferences().isOpenReturn()) {
			long stayOverTimeBetweenRequestedDates = ondAssm.getSelectedInboundDateTimeEnd().getTime()
					- ondAssm.getSelectedOutboundDateTimeStart().getTime();
			flightAvailRQ.getAvailPreferences().setStayOverTimeInMillis(stayOverTimeBetweenRequestedDates);
			searchParams.setStayOverTimeInMillis(stayOverTimeBetweenRequestedDates);
		}
	}

	private void clearValues() {

		outboundFlights = null;
		returnFlights = null;
		availableFare = null;
		ondLogicalCCList = null;
		flightInfo = null;
		airportMessage = null;
	}

	private boolean isCalculateHandlingFee() {
		return (!modifyBooking && !addGroundSegment && !transferSegment && !addSegment && !searchParams.isOpenReturnConfirm());
	}

	private void calculateTotalHandlingFee() throws ModuleException {

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		PaxSet paxSet = new PaxSet(searchParams.getAdultCount(), searchParams.getChildCount(), searchParams.getInfantCount());
		ExternalChargeUtil.calculateExternalChargeAmount(paxSet, bookingShoppingCart.getPriceInfoTO(),
				bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.HANDLING_CHARGE),
				(returnFlights != null && returnFlights.size() > 0));

		bookingShoppingCart.addSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE);
		// bookingShoppingCart.addSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD);
		// bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE).setAmount(new
		// BigDecimal(555));
		// bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD).setTotalAmount(new
		// BigDecimal(14.15));
	}

	public boolean isServiceTaxApplicableForOnd(){

		if (searchParams != null) {

			String originCountry = "";
			String originCode = searchParams.getFromAirport();
			String[] countryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");

			if (SYSTEM.getEnum(searchParams.getSearchSystem()).equals(SYSTEM.INT)) {
				try {
					Map<String, CachedAirportDTO> mapAirports = ModuleServiceLocator.getAirportBD()
							.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(originCode)));
					originCountry = mapAirports.get(originCode) != null ? mapAirports.get(originCode).getCountryCode() : "";
				} catch (ModuleException e) {
					log.error("ERROR in loading country code for the origin airport", e);
					return false;
				}
			} else {
				Object[] airportInfo = ModuleServiceLocator.getGlobalConfig().retrieveAirportInfo(originCode);
				if (airportInfo != null && airportInfo.length > 2) {
					originCountry = (String) airportInfo[3];
				}
			}

			for (String countryCode : countryCodes) {
				if (countryCode.equalsIgnoreCase(originCountry)) {
					return true;
				}
			}

		}
		return false;

	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public Collection<AvailableFlightInfo> getOutboundFlights() {
		return outboundFlights;
	}

	public Collection<AvailableFlightInfo> getReturnFlights() {
		return returnFlights;
	}

	// setters
	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	/**
	 * @return the flightInfo
	 */
	public Collection<FlightInfoTO> getFlightInfo() {
		return flightInfo;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public static Log getLog() {
		return log;
	}

	public boolean isModifyBooking() {
		return modifyBooking;
	}

	public void setModifyBooking(boolean modifyBooking) {
		this.modifyBooking = modifyBooking;
	}

	/**
	 * @return the openReturnInfo
	 */
	public OpenReturnInfo getOpenReturnInfo() {
		return openReturnInfo;
	}

	/**
	 * @param openReturnInfo
	 *            the openReturnInfo to set
	 */
	public void setOpenReturnInfo(OpenReturnInfo openReturnInfo) {
		this.openReturnInfo = openReturnInfo;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public FareTypeInfoTO getAvailableFare() {
		return availableFare;
	}

	public void setAvailableFare(FareTypeInfoTO availableFare) {
		this.availableFare = availableFare;
	}

	public void setTransferSegment(boolean transferSegment) {
		this.transferSegment = transferSegment;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public ReservationProcessParams getRpParams() {
		return rpParams;
	}

	public void setRpParams(ReservationProcessParams rpParams) {
		this.rpParams = rpParams;
	}

	public Set<String> getBusCarrierCodes() {
		return busCarrierCodes;
	}

	public void setSelectedFltSeg(String selectedFltSeg) {
		this.selectedFltSeg = selectedFltSeg;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	public void setOndLogicalCCList(List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList) {
		this.ondLogicalCCList = ondLogicalCCList;
	}

	public boolean isShowFareDiscountCodes() {
		return showFareDiscountCodes;
	}

	public void setShowFareDiscountCodes(boolean showFareDiscountCodes) {
		this.showFareDiscountCodes = showFareDiscountCodes;
	}

	public List<String[]> getFareDiscountCodeList() {
		return fareDiscountCodeList;
	}

	public void setFareDiscountCodeList(List<String[]> fareDiscountCodeList) {
		this.fareDiscountCodeList = fareDiscountCodeList;
	}

	public DiscountedFareDetails getAppliedFareDiscount() {
		return appliedFareDiscount;
	}

	public String getDummyCreditDiscountAmount() {
		return dummyCreditDiscountAmount;
	}

	public boolean isShowAgentCommission() {
		return showAgentCommission;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public void setOndWiseTotalFlexiCharge(List<String> ondWiseTotalFlexiCharge) {
		this.ondWiseTotalFlexiCharge = ondWiseTotalFlexiCharge;
	}

	public boolean isFlexiEnableInAnci() {
		return isFlexiEnableInAnci;
	}

	public void setFlexiEnableInAnci(boolean isFlexiEnableInAnci) {
		this.isFlexiEnableInAnci = isFlexiEnableInAnci;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public void setOndWiseTotalFlexiAvailable(List<Boolean> ondWiseTotalFlexiAvailable) {
		this.ondWiseTotalFlexiAvailable = ondWiseTotalFlexiAvailable;
	}

	public HashMap<String, List<String>> getSegmentsMapPerSegType() {
		return segmentsMapPerSegType;
	}

	public void setSegmentsMapPerSegType(HashMap<String, List<String>> segmentsMapPerSegType) {
		this.segmentsMapPerSegType = segmentsMapPerSegType;
	}

	public boolean isServiceTaxApplicable() {
		return serviceTaxApplicable;
	}

	public void setServiceTaxApplicable(boolean serviceTaxApplicable) {
		this.serviceTaxApplicable = serviceTaxApplicable;
	}

	public BaggageRatesDTO getBaggageRatesDTO() {
		return baggageRatesDTO;
	}

	public void setBaggageRatesDTO(BaggageRatesDTO baggageRatesDTO) {
		this.baggageRatesDTO = baggageRatesDTO;
	}

	public boolean isDisplayBasedOnTemplates() {
		return displayBasedOnTemplates;
	}


	
}
