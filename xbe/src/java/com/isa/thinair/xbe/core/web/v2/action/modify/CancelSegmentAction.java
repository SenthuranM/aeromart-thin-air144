package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.RouteTypes;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdateOverrideChargesTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class CancelSegmentAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(CancelSegmentAction.class);

	private boolean success = true;

	private String messageTxt;

	private BookingTO bookingInfo;

	private String groupPNR;

	private String version;

	private String adultCnxCharge;

	private String childCnxCharge;

	private String infantCnxCharge;

	private String routeType;

	private ConfirmUpdateOverrideChargesTO adultOverideCharges;

	private ConfirmUpdateOverrideChargesTO childOverideCharges;

	private ConfirmUpdateOverrideChargesTO infantOverideCharges;

	private String defaultAdultCnxCharge;

	private String defaultChildCnxCharge;

	private String defaultInfantCnxCharge;

	private String resContactInfo;

	private String userNote;

	public String execute() {

		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String pnr = bookingInfo.getPNR();
			if (isGroupPNR) {
				pnr = groupPNR;
			}
			String strSegments = request.getParameter("segmentRefNo");
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rparam = new ReservationProcessParams(request, resInfo != null
					? resInfo.getReservationStatus()
					: null, colsegs, resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);
			if (isGroupPNR) {
				rparam.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}
			Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs, strSegments,
					rparam, false);
			LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO.composeCancelSegmentRequest(pnr,
					colModSegs, colsegs, null, null, null, version, resInfo.getReservationStatus());
			lccClientResAlterModesTO.setGroupPnr(isGroupPNR);

			if (this.adultOverideCharges != null) {
				lccClientResAlterModesTO.setAdultCustomChargeTO(this.createPNRDetailTO(adultOverideCharges));
			}
			if (this.infantOverideCharges != null) {
				lccClientResAlterModesTO.setInfantCustomChargeTO(this.createPNRDetailTO(infantOverideCharges));
			}
			if (this.childOverideCharges != null) {
				lccClientResAlterModesTO.setChildCustomChargeTO(this.createPNRDetailTO(childOverideCharges));
			}

			// Default cnx charge state no route value will be set
			if (this.routeType != null && !this.routeType.equals("")) {
				BigDecimal customAdultCharge = null;
				BigDecimal customChildCharge = null;
				BigDecimal customInfantCharge = null;
				if (adultCnxCharge != null && !adultCnxCharge.equals("") && !adultCnxCharge.equals("undefined")) {
					customAdultCharge = new BigDecimal(adultCnxCharge);
					if (this.adultOverideCharges == null
							|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
									this.adultOverideCharges)) {
						lccClientResAlterModesTO.setSendingAbosoluteCharge(true);
					} // if not modifying using percentage
				}

				if (childCnxCharge != null && !childCnxCharge.equals("") && !childCnxCharge.equals("undefined")) {
					customChildCharge = new BigDecimal(childCnxCharge);
					if (this.childOverideCharges == null
							|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
									this.childOverideCharges)) {
						lccClientResAlterModesTO.setSendingAbosoluteCharge(true);
					}
				}

				if (infantCnxCharge != null && !infantCnxCharge.equals("") && !infantCnxCharge.equals("undefined")) {
					customInfantCharge = new BigDecimal(infantCnxCharge);
					if (this.infantOverideCharges == null
							|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
									this.infantOverideCharges)) {
						lccClientResAlterModesTO.setSendingAbosoluteCharge(true);
					}
				}

				lccClientResAlterModesTO.setCustomAdultCharge(customAdultCharge);
				lccClientResAlterModesTO.setCustomChildCharge(customChildCharge);
				lccClientResAlterModesTO.setCustomInfantCharge(customInfantCharge);

			}

			if (this.defaultAdultCnxCharge != null && !this.defaultAdultCnxCharge.isEmpty()) {
				lccClientResAlterModesTO.setDefaultCustomAdultCharge(new BigDecimal(defaultAdultCnxCharge));
			}
			if (this.defaultChildCnxCharge != null && !this.defaultChildCnxCharge.isEmpty()) {
				lccClientResAlterModesTO.setDefaultCustomChildCharge(new BigDecimal(defaultChildCnxCharge));
			}
			if (this.defaultInfantCnxCharge != null) {
				lccClientResAlterModesTO.setDefaultCustomInfantCharge(new BigDecimal(defaultInfantCnxCharge));
			}
			lccClientResAlterModesTO.setUserNotes(userNote);
			// when it comes to actually CNX the reservation we can always assume the rout type as total as the value
			// in the override box will represent the total override charge even if the initial criteria is a per_ond
			lccClientResAlterModesTO.setRouteType(RouteTypes.TOTAL_ROUTE.getRouteTypes());

			// adding reservation contact info to the request which is required for shuffle credit if required when
			// cancel segment
			LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
			lccClientSegmentAssembler.setContactInfo(ReservationUtil.transformJsonContactInfo(resContactInfo));
			lccClientResAlterModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);

			ModuleServiceLocator.getAirproxySegmentBD().cancelSegments(lccClientResAlterModesTO, getClientInfoDTO(),
					getTrackInfo());

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private PnrChargeDetailTO createPNRDetailTO(ConfirmUpdateOverrideChargesTO chargesTO) {
		PnrChargeDetailTO chargeDetailTO = new PnrChargeDetailTO();
		chargeDetailTO.setCancellationChargeType(chargesTO.getChargeType());
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMin())) {
			chargeDetailTO.setMinCancellationAmount(new BigDecimal(chargesTO.getMin()));
		}
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMax())) {
			chargeDetailTO.setMaximumCancellationAmount(new BigDecimal(chargesTO.getMax()));
		}

		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getPrecentageVal())) {
			chargeDetailTO.setChargePercentage(new BigDecimal(chargesTO.getPrecentageVal()));
		}
		return chargeDetailTO;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the bookingInfo
	 */
	public BookingTO getBookingInfo() {
		return bookingInfo;
	}

	/**
	 * @param bookingInfo
	 *            the bookingInfo to set
	 */
	public void setBookingInfo(BookingTO bookingInfo) {
		this.bookingInfo = bookingInfo;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setAdultCnxCharge(String adultCnxCharge) {
		this.adultCnxCharge = adultCnxCharge;
	}

	public void setChildCnxCharge(String childCnxCharge) {
		this.childCnxCharge = childCnxCharge;
	}

	public void setInfantCnxCharge(String infantCnxCharge) {
		this.infantCnxCharge = infantCnxCharge;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public ConfirmUpdateOverrideChargesTO getAdultOverideCharges() {
		return adultOverideCharges;
	}

	public void setAdultOverideCharges(ConfirmUpdateOverrideChargesTO adultOverideCharges) {
		this.adultOverideCharges = adultOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getChildOverideCharges() {
		return childOverideCharges;
	}

	public void setChildOverideCharges(ConfirmUpdateOverrideChargesTO childOverideCharges) {
		this.childOverideCharges = childOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getInfantOverideCharges() {
		return infantOverideCharges;
	}

	public void setInfantOverideCharges(ConfirmUpdateOverrideChargesTO infantOverideCharges) {
		this.infantOverideCharges = infantOverideCharges;
	}

	public String getAdultCnxCharge() {
		return adultCnxCharge;
	}

	public String getChildCnxCharge() {
		return childCnxCharge;
	}

	public String getInfantCnxCharge() {
		return infantCnxCharge;
	}

	public String getDefaultAdultCnxCharge() {
		return defaultAdultCnxCharge;
	}

	public void setDefaultAdultCnxCharge(String defaultAdultCnxCharge) {
		this.defaultAdultCnxCharge = defaultAdultCnxCharge;
	}

	public String getDefaultChildCnxCharge() {
		return defaultChildCnxCharge;
	}

	public void setDefaultChildCnxCharge(String defaultChildCnxCharge) {
		this.defaultChildCnxCharge = defaultChildCnxCharge;
	}

	public String getDefaultInfantCnxCharge() {
		return defaultInfantCnxCharge;
	}

	public void setDefaultInfantCnxCharge(String defaultInfantCnxCharge) {
		this.defaultInfantCnxCharge = defaultInfantCnxCharge;
	}

	/**
	 * @return the resContactInfo
	 */
	public String getResContactInfo() {
		return resContactInfo;
	}

	/**
	 * @param resContactInfo
	 *            the resContactInfo to set
	 */
	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

}
