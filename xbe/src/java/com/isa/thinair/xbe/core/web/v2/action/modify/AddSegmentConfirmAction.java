package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ConfirmUpdateUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Add Segment Confirmation
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency,countryCurrExchangeRate\\.currency" })
public class AddSegmentConfirmAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AddSegmentConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private String selCurrency;

	private String pnr;

	private String groupPNR;

	private boolean blnNoPay = false;

	private FlightSearchDTO fareQuoteParams;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private String flightRPHList;

	private String version;

	private String airportMessage;

	private String flexiSelected;

	private CurrencyExchangeRate currencyExchangeRate;

	private String selectedPnrSeg;

	private String resContactInfo;

	private String ownerAgentCode;

	private CurrencyExchangeRate countryCurrExchangeRate;

	private String agentBalance;

	private String agentBalanceInAgentCurrency;
	
	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		if (!isGroupPNR && SYSTEM.getEnum(fareQuoteParams.getSearchSystem()) == SYSTEM.INT) {
			isGroupPNR = true;
			groupPNR = pnr;
		}
		try {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			// set tracking info
			TrackInfoDTO trackInfoDTO = this.getTrackInfo();

			// currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();
			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			String agentCurrency = up.getAgentCurrencyCode();
			if (agentCurrency == null || agentCurrency.equals("")) {
				agentCurrency = AppSysParamsUtil.getBaseCurrency();
			}
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			if ((selCurrency != null && !selCurrency.equals("")) && !selCurrency.equals(agentCurrency)) {
				currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency, ApplicationEngine.XBE);
			} else {
				currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(agentCurrency, ApplicationEngine.XBE);
			}
			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);

			BigDecimal customAdultCancelCharge = null;
			BigDecimal customInfantCancelCharge = null;
			BigDecimal customChildCancelCharge = null;
			List<FlightSegmentTO> allCnfSegments = null;

			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			Collection<LCCClientReservationPax> colpaxs = ReservationUtil
					.transformJsonPassengers(request.getParameter("resPaxs"));
			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(flightRPHList, fareQuoteParams);
			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo.getReservationStatus(), colsegs,
					resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);
			if (isGroupPNR) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}

			// BigDecimal totalAnci = bookingShoppingCart.getAncillaryTotalExternalCharge(true);
			Map<Integer, ReservationPaxTO> paxMap = new HashMap<Integer, ReservationPaxTO>();
			if (bookingShoppingCart.getPaxList() != null) {
				for (ReservationPaxTO pax : bookingShoppingCart.getPaxList()) {
					paxMap.put(pax.getSeqNumber(), pax);
				}
			}

			if (!AddModifySurfaceSegmentValidations.isValidSegment(colsegs, flightSegmentTOs, selectedPnrSeg)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.surfacesegment.modify.book.invalid", userLanguage);
				return S2Constants.Result.SUCCESS;
			}

			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, outFlightRPHList,
					retFlightRPHList);

			String pendingCommissions = CommonUtil.getPendingAgentCommissions(up.getAgentCode(), (isGroupPNR ? groupPNR : pnr));

			if (isGroupPNR) {
				Collection<LCCClientReservationSegment> existingAllSegs = null;
				if (pnr != null && !"".equals(pnr)) {
					ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"), null);
					existingAllSegs = ModifyReservationJSONUtil.transformJsonSegments(request.getParameter("resSegments"));
				}

				LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
				ExternalChargeUtil.injectOndBaggageGroupId(bookingShoppingCart.getPaxList(), flightSegmentTOs);
				BookingUtil.addSegments(lccClientSegmentAssembler, flightSegmentTOs, BookingUtil.getNextSegmentSequnce(colsegs));
				lccClientSegmentAssembler.setPassengerExtChargeMap(ReservationUtil.getPassengerExtChgMap(bookingShoppingCart));
				lccClientSegmentAssembler.setContactInfo(ReservationUtil.transformJsonContactInfo(resContactInfo));
				// lccClientSegmentAssembler.setExternalChargesMap(getExternalChargesMap(bookingShoppingCart));
				lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
				lccClientSegmentAssembler.setLccTransactionIdentifier(resInfo.getTransactionId());
				lccClientSegmentAssembler.setSelectedFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
				lccClientSegmentAssembler.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
				lccClientSegmentAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));
				lccClientSegmentAssembler.setOwnerAgent(ownerAgentCode);
				BookingUtil.populateInsurance(bookingShoppingCart.getInsuranceQuotes(), lccClientSegmentAssembler,
						getAdultCount(fareQuoteParams)); // adding reservation insurance

				if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
						|| resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
					Map<String, String> mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
					allCnfSegments = ReservationBeanUtil.getAllCNFSegsForModify(colsegs, flightSegmentTOs);
					Date existingRelTimestamp = null;
					if (pnr != null && !"".equals(pnr) && colpaxs.iterator().next().getZuluReleaseTimeStamp() != null) {
						existingRelTimestamp = colpaxs.iterator().next().getZuluReleaseTimeStamp();
					}

					Date releaseTimestamp = BookingUtil.getReleaseTimestamp(allCnfSegments, mapPrivileges, up.getAgentCode(),
							bookingShoppingCart.getPriceInfoTO().getFareTypeTO(), existingAllSegs, existingRelTimestamp);
					lccClientSegmentAssembler.setPnrZuluReleaseTimeStamp(releaseTimestamp);
				}

				lccClientSegmentAssembler.setFlexiSelected(ReservationUtil.isFlexiSelected(flexiSelected));
				Collection<LCCClientReservationSegment> colOldSegs = null;
				LCCClientResAlterModesTO lCCClientResAlterQueryModesTO = null;

				lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeAddSegmentRequest(pnr, colOldSegs,
						existingAllSegs, lccClientSegmentAssembler, customAdultCancelCharge, customChildCancelCharge,
						customInfantCancelCharge, version, this.getSelectedPnrSeg(), resInfo.getReservationStatus());
				lCCClientResAlterQueryModesTO.setPassengers(new HashSet<LCCClientReservationPax>(colpaxs));
				lCCClientResAlterQueryModesTO.setGroupPnr(isGroupPNR);
				lCCClientResAlterQueryModesTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
				lCCClientResAlterQueryModesTO.setVersion(ReservationCommonUtil
						.getCorrectInterlineVersion(lCCClientResAlterQueryModesTO.getVersion()));
				ReservationBalanceTO lCCClientReservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
						.getResAlterationBalanceSummary(lCCClientResAlterQueryModesTO, trackInfoDTO);

				BigDecimal totalAmountDue = lCCClientReservationBalanceTO.getTotalAmountDue();
				if (lCCClientReservationBalanceTO.getVersion() != null) {
					lCCClientResAlterQueryModesTO.setVersion(lCCClientReservationBalanceTO.getVersion());
				}
				lCCClientResAlterQueryModesTO.setHasMadePayments(lCCClientReservationBalanceTO.getTotalPaidAmount().compareTo(
						BigDecimal.ZERO) > 0);

				if (totalAmountDue.compareTo(BigDecimal.ZERO) <= 0) {
					List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) lCCClientReservationBalanceTO
							.getPassengerSummaryList();
					for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
						if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
							LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
							paymentAssembler.setPaxType(paxSummaryTo.getPaxType());
							Date paymentTimestamp = new Date();
							paymentAssembler.addCashPayment(BigDecimal.ZERO, null, paymentTimestamp, null, null, null, null);
							lccClientSegmentAssembler.addPassengerPayments(
									PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()), paymentAssembler);
						}
					}
					lCCClientResAlterQueryModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);
					LCCClientReservation lccClientReservation = ModuleServiceLocator.getAirproxySegmentBD().addSegment(
							lCCClientResAlterQueryModesTO, getClientInfoDTO(),
							AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), trackInfoDTO);
					this.blnNoPay = true;
					// clearing the block seats after modification success.
					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, null);
					// Update release timestamp of the dummy booking
					if (lccClientReservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
							&& (lccClientReservation.getZuluReleaseTimeStamp().compareTo(
									lccClientSegmentAssembler.getPnrZuluReleaseTimeStamp()) != 0)) {
						ReservationUtil.updateReleaseTimeForDummyBooking(lccClientReservation.getPNR(),
								lccClientReservation.getZuluReleaseTimeStamp(), trackInfoDTO);
					}
				} else {
					bookingShoppingCart.setReservationBalance(lCCClientReservationBalanceTO);
					bookingShoppingCart.setReservationAlterModesTO(lCCClientResAlterQueryModesTO);
					bookingShoppingCart.setPaidAmount(lCCClientReservationBalanceTO.getTotalPaidAmount());
					paymentSummaryTO = PaymentUtil.createPaymentSummaryAddSegment(bookingShoppingCart, selCurrency, colpaxs,
							exchangeRateProxy, pendingCommissions, up.getAgentCode());
					passengerPayment = ConfirmUpdateUtil.getPaxPaySummaryListAddSegment(lCCClientReservationBalanceTO, paxMap,
							colpaxs, false, resInfo.getSelectedSystem());
					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShoppingCart);
				}

			} else {
				Collection<LCCClientReservationSegment> existingAllSegs = null;
				if (pnr != null && !"".equals(pnr)) {
					ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"), null);
					existingAllSegs = ModifyReservationJSONUtil.transformJsonSegments(request.getParameter("resSegments"));
				}

				List<FlightSegmentTO> allFlightSegmentsList = AirProxyReservationUtil.populateFlightSegmentTO(existingAllSegs);
				SortUtil.sortFlightSegByDepDate(allFlightSegmentsList);

				LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
				ExternalChargeUtil.injectOndBaggageGroupId(bookingShoppingCart.getPaxList(), flightSegmentTOs);
				BookingUtil.addSegments(lccClientSegmentAssembler, flightSegmentTOs, BookingUtil.getNextSegmentSequnce(colsegs));

				SSRServicesUtil.setSegmentSeqForSSR((List<ReservationPaxTO>) bookingShoppingCart.getPaxList(), flightSegmentTOs,
						allFlightSegmentsList.size());

				lccClientSegmentAssembler.setPassengerExtChargeMap(ReservationUtil.getPassengerExtChgMap(bookingShoppingCart));
				lccClientSegmentAssembler.setContactInfo(ReservationUtil.transformJsonContactInfo(resContactInfo));
				lccClientSegmentAssembler.setExternalChargesMap(getExternalChargesMap(bookingShoppingCart));
				lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
				lccClientSegmentAssembler.setLccTransactionIdentifier(resInfo.getTransactionId());
				lccClientSegmentAssembler.setSelectedFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
				lccClientSegmentAssembler.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
				lccClientSegmentAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));
				lccClientSegmentAssembler.setApplicableAgentCommissions(bookingShoppingCart.getAgentCommissions());

				if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
						|| resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
					Map<String, String> mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
					allCnfSegments = ReservationBeanUtil.getAllCNFSegsForModify(colsegs, flightSegmentTOs);

					Date existingRelTimestamp = null;
					if (pnr != null && !"".equals(pnr) && colpaxs.iterator().next().getZuluReleaseTimeStamp() != null) {
						existingRelTimestamp = colpaxs.iterator().next().getZuluReleaseTimeStamp();
					}

					Date releaseTimestamp = BookingUtil.getReleaseTimestamp(allCnfSegments, mapPrivileges, up.getAgentCode(),
							bookingShoppingCart.getPriceInfoTO().getFareTypeTO(), existingAllSegs, existingRelTimestamp);
					lccClientSegmentAssembler.setPnrZuluReleaseTimeStamp(releaseTimestamp);
				}

				lccClientSegmentAssembler.setFlexiSelected(ReservationUtil.isFlexiSelected(flexiSelected));
				Collection<LCCClientReservationSegment> colOldSegs = null;
				LCCClientResAlterModesTO lCCClientResAlterQueryModesTO = null;

				lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeAddSegmentRequest(pnr, colOldSegs,
						existingAllSegs, lccClientSegmentAssembler, customAdultCancelCharge, customChildCancelCharge,
						customInfantCancelCharge, version, this.getSelectedPnrSeg(), resInfo.getReservationStatus());
				lCCClientResAlterQueryModesTO.setGroupPnr(isGroupPNR);
				lCCClientResAlterQueryModesTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
				ReservationBalanceTO lCCClientReservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
						.getResAlterationBalanceSummary(lCCClientResAlterQueryModesTO, trackInfoDTO);

				BigDecimal totalAmountDue = lCCClientReservationBalanceTO.getTotalAmountDue();
				// totalAmountDue = AccelAeroCalculator.add(totalAmountDue, totalAnci);

				if (totalAmountDue.compareTo(BigDecimal.ZERO) <= 0
						&& !resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
					Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = lccClientSegmentAssembler
							.getPassengerExtChargeMap();
					List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) lCCClientReservationBalanceTO
							.getPassengerSummaryList();
					for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
						if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
							LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
							Date paymentTimestamp = new Date();
							paymentAssembler.addCashPayment(BigDecimal.ZERO, null, paymentTimestamp, null, null, null, null);
							if (paxExtChgMap != null && paxExtChgMap.size() > 0) {
								Integer paxSeq = PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber());
								List<LCCClientExternalChgDTO> paxExternalCharge = paxExtChgMap.get(paxSeq);
								// mark the external chargers already consume as we do not need any external payment for
								// this case
								for (LCCClientExternalChgDTO chgDTO : paxExternalCharge) {
									chgDTO.setAmountConsumedForPayment(true);
								}
								paymentAssembler.getPerPaxExternalCharges().addAll(paxExternalCharge);
							}
							lccClientSegmentAssembler.addPassengerPayments(
									PaxTypeUtils.getPnrPaxId(paxSummaryTo.getTravelerRefNumber()), paymentAssembler);
						}
					}
					lCCClientResAlterQueryModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);
					lCCClientResAlterQueryModesTO
							.setPaymentTypes(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT);
					lCCClientResAlterQueryModesTO.setActualPayment(false);
					ModuleServiceLocator.getAirproxySegmentBD().addSegment(lCCClientResAlterQueryModesTO, getClientInfoDTO(),
							AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), trackInfoDTO);
					this.blnNoPay = true;
					// clearing the block seats after modification success.
					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, null);
				} else {
					bookingShoppingCart.setReservationBalance(lCCClientReservationBalanceTO);
					bookingShoppingCart.setReservationAlterModesTO(lCCClientResAlterQueryModesTO);
					bookingShoppingCart.setPaidAmount(lCCClientReservationBalanceTO.getTotalPaidAmount());
					paymentSummaryTO = PaymentUtil.createPaymentSummaryAddSegment(bookingShoppingCart, selCurrency, colpaxs,
							exchangeRateProxy, pendingCommissions, up.getAgentCode());
					passengerPayment = ConfirmUpdateUtil.getPaxPaySummaryListAddSegment(lCCClientReservationBalanceTO, paxMap,
							colpaxs, false, resInfo.getSelectedSystem());
					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShoppingCart);
				}
			}

			if (this.countryCurrExchangeRate != null && this.paymentSummaryTO != null) {
				this.paymentSummaryTO.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
			}

			// Load Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessagesForFlightSegmentWise(flightSegmentTOs,
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.CARD_PAYMENT, null);
			agentBalance = CommonUtil.getLoggedInAgentBalance(request);
			agentBalanceInAgentCurrency = CommonUtil.getLoggedInAgentBalanceInAgentCurrency(request);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private int getAdultCount(FlightSearchDTO fltSearchDTO) {
		return (fltSearchDTO.getAdultCount() + fltSearchDTO.getChildCount());
	}

	private Map getExternalChargesMap(BookingShoppingCart bookingShoppingCart) throws ModuleException {
		Collection<ReservationPaxTO> paxList = bookingShoppingCart.getPaxList();
		Map selectedChargers = bookingShoppingCart.getSelectedExternalCharges();

		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		if (paxList != null) {
			for (ReservationPaxTO pax : paxList) {
				for (LCCClientExternalChgDTO extChg : pax.getExternalCharges()) {
					colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
				}

			}
		}
		if (selectedChargers != null) {
			Set<EXTERNAL_CHARGES> tmpKeySet = selectedChargers.keySet();
			for (EXTERNAL_CHARGES key : tmpKeySet) {
				colEXTERNAL_CHARGES.add(key);
			}
		}

		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_OTHER);

		Map extExternalChgDTOMap = null;
		if (colEXTERNAL_CHARGES.size() > 0) {
			extExternalChgDTOMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
					ChargeRateOperationType.MODIFY_ONLY);
		}
		return extExternalChgDTOMap;
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	public void setSelCurrency(String selCurrency) {
		this.selCurrency = selCurrency;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isBlnNoPay() {
		return blnNoPay;
	}

	public void setBlnNoPay(boolean blnNoPay) {
		this.blnNoPay = blnNoPay;
	}

	public boolean isSuccess() {
		return success;
	}

	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setPassengerPayment(Collection<PaymentPassengerTO> passengerPayment) {
		this.passengerPayment = passengerPayment;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	/**
	 * Gets the flexi selected.
	 * 
	 * @return the flexi selected
	 */
	public String getFlexiSelected() {
		return flexiSelected;
	}

	/**
	 * Sets the flexi selected.
	 * 
	 * @param flexiSelected
	 *            the new flexi selected
	 */
	public void setFlexiSelected(String flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public String getSelectedPnrSeg() {
		return selectedPnrSeg;
	}

	public void setSelectedPnrSeg(String selectedPnrSeg) {
		this.selectedPnrSeg = selectedPnrSeg;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		if (ownerAgentCode != null && !"".equals(ownerAgentCode.trim()) && !"undefined".equals(ownerAgentCode.trim())
				&& !"null".equals(ownerAgentCode.trim())) {
			this.ownerAgentCode = ownerAgentCode;
		}
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	public String getAgentBalance() {
		return agentBalance;
	}
	
	public String getAgentBalanceInAgentCurrency() {
		return agentBalanceInAgentCurrency;
	}

	public void setAgentBalanceInAgentCurrency(String agentBalanceInAgentCurrency) {
		this.agentBalanceInAgentCurrency = agentBalanceInAgentCurrency;
	}


}
