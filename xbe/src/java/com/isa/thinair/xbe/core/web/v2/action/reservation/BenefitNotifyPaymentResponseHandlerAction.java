package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * This is specific servlet related to benefit payment gateway This servlet response will be redirected to handler
 * action as normal behavior
 * 
 */
@Namespace(S2Constants.Namespace.PUBLIC)
public class BenefitNotifyPaymentResponseHandlerAction extends BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(BenefitNotifyPaymentResponseHandlerAction.class);

	private static final String RETURN_URL = AppSysParamsUtil.getAirlineReservationURL()
			+ "/xbe/private/handleExternalPGWResponse.action";

	private static final String RESULTSTATUS_CAPTURED = "CAPTURED";

	public String execute() {
		ForceLoginInvoker.defaultLogin();
		if (log.isDebugEnabled())
			log.debug("###Start.." + request.getRequestedSessionId());

		response.setContentType("text/html;charset=UTF-8");
		// if(!request.getParameterMap().isEmpty()){

		String paymentId = request.getParameter("paymentid");
		String result = request.getParameter("result");
		String trackid = request.getParameter("trackid");
		Map receiptyMap = getReceiptMap(request);
		String resCode = request.getParameter("responsecode");
		try {
			PrintWriter out = response.getWriter();
			// Get message details sent from Gateway
			String ErrorNo = request.getParameter("Error");
			String udf1 = request.getParameter("udf1");
			String udf2 = request.getParameter("udf2");
			String udf3 = request.getParameter("udf3");
			String udf4 = request.getParameter("udf4");
			String udf5 = request.getParameter("udf5");
			String ErrorText = request.getParameter("ErrorText");
			String postdate = request.getParameter("postdate");
			String tranid = request.getParameter("tranid");
			String auth = request.getParameter("auth");
			String ref = request.getParameter("ref");

			StringBuilder strLog = new StringBuilder();
			strLog.append("paymentid:").append(paymentId).append(",");
			strLog.append("ErrorNo:").append(ErrorNo).append(",");
			strLog.append("udf1:").append(udf1).append(",");
			strLog.append("udf2:").append(udf2).append(",");
			strLog.append("udf3:").append(udf3).append(",");
			strLog.append("udf4:").append(udf4).append(",");
			strLog.append("udf5:").append(udf5).append(",");
			strLog.append("ErrorText:").append(ErrorText).append(",");
			strLog.append("postdate:").append(postdate).append(",");
			strLog.append("tranid:").append(tranid).append(",");
			strLog.append("trackid:").append(trackid).append(",");
			strLog.append("ref:").append(ref).append(",");
			strLog.append("result:").append(result).append(",");
			strLog.append("auth:").append(auth).append(",");
			strLog.append("responsecode:").append(resCode).append(",");
			strLog.append("SessionID:").append(request.getRequestedSessionId()).append(",");
			log.info(strLog.toString());

			StringBuffer responseURL = new StringBuffer();
			responseURL.append("REDIRECT=").append(RETURN_URL).append("?");
			responseURL.append("paymentid=").append(paymentId).append("&");
			if (!StringUtil.isNullOrEmpty(result))
				responseURL.append("result=").append(result).append("&");
			if (!StringUtil.isNullOrEmpty(auth))
				responseURL.append("auth=").append(auth).append("&");
			if (!StringUtil.isNullOrEmpty(ref))
				responseURL.append("ref=").append(ref).append("&");
			// if (!StringUtil.isNullOrEmpty(postdate))
			// responseURL.append("postdate=").append(postdate).append("&");
			if (!StringUtil.isNullOrEmpty(trackid))
				responseURL.append("trackid=").append(trackid).append("&");
			if (!StringUtil.isNullOrEmpty(tranid))
				responseURL.append("tranid=").append(tranid).append("&");
			if (!StringUtil.isNullOrEmpty(udf1))
				responseURL.append("udf1=").append(udf1).append("&");
			// if (!StringUtil.isNullOrEmpty(udf2))
			// responseURL.append("udf2=").append(udf2).append("&");
			if (!StringUtil.isNullOrEmpty(udf3))
				responseURL.append("udf3=").append(udf3).append("&");
			if (!StringUtil.isNullOrEmpty(udf4))
				responseURL.append("udf4=").append(udf4).append("&");
			if (!StringUtil.isNullOrEmpty(udf5))
				responseURL.append("udf5=").append(udf5).append("&");
			if (!StringUtil.isNullOrEmpty(resCode) && !result.equals(RESULTSTATUS_CAPTURED))
				responseURL.append("responsecode=").append(resCode);
			// Save Intermediate response
			// This action is executed by payment gateway repeatedly
			// TODO revirew

			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			int paymentBrokerRefNo = Integer.parseInt(udf4);
			int tempPayId = Integer.parseInt(getTempID(trackid));
			Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(tempPayId);

			ipgResponseDTO.setRequestTimsStamp(requestTime);
			ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
			ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
			ipgResponseDTO.setTemporyPaymentId(tempPayId);

			String[] udf3Arr = udf3.split("_");
			String strPayCurCode = udf3Arr[1];
			int ipgId = Integer.parseInt(udf3Arr[0]);
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					new Integer(ipgId), strPayCurCode);
			// Update receipt map for as intermediate response
			receiptyMap.put("responseType", "INTERMEDIATE");
			ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);

			out.print(responseURL.toString());
			out.flush();
			out.close();

			if (log.isDebugEnabled())
				log.debug("###END.." + request.getRequestedSessionId());

		} catch (Exception ex) {
			log.error("###Error...##" + request.getRequestedSessionId() + "paymentid" + paymentId + "Result" + result + trackid,
					ex);
			try {
				PrintWriter out = response.getWriter();
				StringBuffer responseURL = new StringBuffer();
				responseURL.append("REDIRECT=").append(RETURN_URL).append("?");
				responseURL.append("paymentid=").append(paymentId).append("&");
				responseURL.append("result=").append(result).append("&");
				responseURL.append("trackid=").append(trackid).append("&");
				responseURL.append("responsecode=").append(resCode);
				out.print(responseURL.toString());
				out.flush();
				out.close();
			} catch (Exception exp) {
				log.error("###Error on data##" + request.getRequestedSessionId(), exp);
			}
		} finally {
			ForceLoginInvoker.close();
		}
		// }
		return null;
	}

	private Map<String, String> getReceiptMap(HttpServletRequest request) {
		Map<String, String> fields = new LinkedHashMap<String, String>();
		for (Enumeration<String> enumeration = request.getParameterNames(); enumeration.hasMoreElements();) {
			String fieldName = (String) enumeration.nextElement();
			String fieldValue = request.getParameter(fieldName);

			if (fieldValue != null && fieldValue.length() > 0) {
				fields.put(fieldName, fieldValue);
			}
		}
		fields.put(PaymentConstants.IPG_SESSION_ID, request.getSession().getId());
		return fields;
	}

	private String getTempID(String trackID) {
		String tempPayID = "";
		if (trackID != null) {
			tempPayID = trackID.substring(1);
		}
		return tempPayID;
	}

}
