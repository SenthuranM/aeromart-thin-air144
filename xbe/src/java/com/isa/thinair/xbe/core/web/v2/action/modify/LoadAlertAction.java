package com.isa.thinair.xbe.core.web.v2.action.modify;

import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

import java.util.ArrayList;
import java.util.List;

/**
 * Load Alert Page functionality
 * 
 * @author Pradeep Karunanayake
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Search.V2_CLEAR_ALERT),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class LoadAlertAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadAlertAction.class);

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		String strForward = S2Constants.Result.SUCCESS;
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		boolean isGdsPnr = new Boolean(request.getParameter("isGdsPnr"));

        boolean isTransferable = true;

		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo.isModifiableReservation(), false, null, null, isGdsPnr, false);

        if(request.getParameter("isTransferable")!= null) {
            isTransferable = new Boolean(request.getParameter("isTransferable"));
            List<String> list = new ArrayList<>(1);
            list.add(ModifcationParamTypes.TRANSFER_ALERT);
            resInfo.setInterlineModificationParams(list);
            rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(), resInfo.getReservationStatus(),
                    null, resInfo.isModifiableReservation(), null, isTransferable);
        } else {
            if (resInfo.getInterlineModificationParams() != null && resInfo.getInterlineModificationParams().size() > 0) {
                rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(), resInfo.getReservationStatus(),
                        null, resInfo.isModifiableReservation(), null);
            }
        }
		try {
			request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
			request.setAttribute(WebConstants.REQ_HTML_ACTION_LIST, SelectListGenerator.createClearAlertActionList());
		} catch (Exception e) {
			strForward = S2Constants.Result.ERROR;
			log.error(BasicRH.getErrorMessage(e,userLanguage), e);
		}
		return strForward;
	}

}
