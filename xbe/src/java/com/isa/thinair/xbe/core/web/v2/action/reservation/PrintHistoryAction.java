package com.isa.thinair.xbe.core.web.v2.action.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_PRINTHISTORY),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class PrintHistoryAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(PrintHistoryAction.class);

	private String pnr;
	private String historyLanguage;

	public String execute() {

		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String history = ModuleServiceLocator.getAirproxyReservationBD().getHistoryForPrint(getPnr(), getTrackInfo());
			request.setAttribute("reqHistoryBodyHtml", history);

		} catch (Exception e) {
			String msg = BasicRH.getErrorMessage(e,userLanguage);
			forward = S2Constants.Result.ERROR;
			JavaScriptGenerator.setServerError(request, msg, "top", "");
			log.error(msg, e);
		}
		return forward;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the historyLanguage
	 */
	public String getHistoryLanguage() {
		return historyLanguage;
	}

	/**
	 * @param historyLanguage
	 *            the historyLanguage to set
	 */
	public void setHistoryLanguage(String historyLanguage) {
		this.historyLanguage = historyLanguage;
	}

}