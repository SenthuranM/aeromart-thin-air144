/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.xbe.core.web.generator.system;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

/**
 * @author Thushara
 * 
 */
public class ReportProblemHG {

	public final static String getClientErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.problem.name.null", "namenull");
		moduleErrs.setProperty("um.problem.email.null", "emailnull");
		moduleErrs.setProperty("um.problem.summary.null", "summarynull");
		moduleErrs.setProperty("um.problem.problem.null", "problemnull");
		moduleErrs.setProperty("um.problem.email.format", "invalidformat");
		return JavaScriptGenerator.createClientErrors(moduleErrs);
	}
}
