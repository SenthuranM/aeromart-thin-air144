package com.isa.thinair.xbe.core.web.handler.reporting;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Request handler for Agent Sales,Refund & Summary Report
 * 
 * @author M.Rikaz
 * @since 12/12/2011
 * 
 */
public class AgentSalesRefundReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(AgentSalesRefundReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		String timestamp = DateUtil.formatDate(DateUtil.getZuluTime(), "yyMMddHHmmss");

		try {
			setDisplayData(request);
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AgentSalesRefundReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			forward = S2Constants.Result.ERROR;
			log.error("Error in AgentSalesRefundReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {

				setReportViewAll(request, response, timestamp);
				forward = S2Constants.Result.RES_SUCCESS;

				setDownloadURL(request, timestamp);

			} else {
				log.error("AgentSalesRefundReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AgentSalesRefundReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("AgentSalesRefundReportRH setReportView Failed ", e);
		}

		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setReportingPeriod(request);
		setStationList(request);
		setCountrySelectList(request);

		setAirportList(request);
		setDisplayAgencyMode(request);
		setJourneyTypeComboList(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setFlightTypesHtml(request);
		setClassOfService(request);
		setBasisCodeList(request);
		setAgentCurrencyList(request);
		setAgentAddressCityList(request);
		setTerritoryList(request);
		setPaxTypesList(request);
		setLiveStatus(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
		setAllowRouteWiseAgentSelection(request);
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_HDN_LIVE, strLive);
	}

	/**
	 * Setting the initial reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	protected static void setReportViewAll(HttpServletRequest request, HttpServletResponse response, String timestamp)
			throws ModuleException {

		try {

			String fromDate = request.getParameter("txtFromDate");
			String toDate = request.getParameter("txtToDate");

			String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

			String reportType = request.getParameter("selReportType");

			String strReportFormat = request.getParameter("radRptNumFormat");

			String flighNo = request.getParameter("txtFlightNumber");
			String flightType = request.getParameter("selFlightType");
			String COS = request.getParameter("selCOS");
			String journeyType = request.getParameter("selJourneyType");
			String depAirport = request.getParameter("selDept");
			String arrAirport = request.getParameter("selArival");
			String fareBasisCode = request.getParameter("selBasisCode");
			String stations = request.getParameter("selStations");
			String country = request.getParameter("selCountry");
			String paidCurrency = request.getParameter("selPaidCurrency");

			String agentCity = request.getParameter("selAgentCity");
			String agentTerritory = request.getParameter("selTerritory");
			String paxTypeCode = request.getParameter("selPaxType");

			String strUsrerId = request.getUserPrincipal().getName();

			if (timestamp != null) {
				strUsrerId += "_" + timestamp;
			}

			String agents = "";

			if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
				agents = (String) getAttribInRequest(request, "currentAgentCode");
			} else {
				agents = request.getParameter("hdnAgents");
			}

			String agentArr[] = agents.split(",");
			ArrayList<String> agentCol = new ArrayList<String>();
			for (int r = 0; r < agentArr.length; r++) {

				if (!StringUtil.isNullOrEmpty(agentArr[r]))
					agentCol.add(agentArr[r]);
			}

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (!StringUtil.isNullOrEmpty(toDate)) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}

			if (agentCol != null && agentCol.size() == 1) {
				search.setAgentCode(agents);
			}

			if (!StringUtil.isNullOrEmpty(strReportFormat)) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}

			if (!StringUtil.isNullOrEmpty(flighNo) && !flighNo.equalsIgnoreCase("All")) {
				search.setFlightNumber(flighNo);
			}

			if (!StringUtil.isNullOrEmpty(flightType) && !flightType.equalsIgnoreCase("All")) {
				search.setSearchFlightType(flightType);
			}

			if (!StringUtil.isNullOrEmpty(COS) && !COS.equalsIgnoreCase("All")) {
				search.setCos(COS);
			}

			if (!StringUtil.isNullOrEmpty(journeyType) && !journeyType.equals("0")) {
				if (journeyType.equals("1")) {
					search.setJourneyType("N");
				} else {
					search.setJourneyType("Y");
				}
			}

			if (!StringUtil.isNullOrEmpty(depAirport) && !depAirport.equalsIgnoreCase("All")) {
				search.setOriginAirport(depAirport);
			}

			if (!StringUtil.isNullOrEmpty(arrAirport) && !arrAirport.equalsIgnoreCase("All")) {
				search.setDestinationAirport(arrAirport);
			}

			if (!StringUtil.isNullOrEmpty(fareBasisCode) && !fareBasisCode.equalsIgnoreCase("All")) {
				search.setFareBasisCode(fareBasisCode);
			}

			if (!StringUtil.isNullOrEmpty(stations) && !stations.equalsIgnoreCase("All")) {
				search.setStation(stations);
			}

			if (!StringUtil.isNullOrEmpty(country) && !country.equalsIgnoreCase("All")) {
				search.setCountryCode(country);
			}

			if (!StringUtil.isNullOrEmpty(paidCurrency) && !paidCurrency.equalsIgnoreCase("All")) {
				search.setCurrencies(paidCurrency);
			}

			if (!StringUtil.isNullOrEmpty(agentCity) && !agentCity.equalsIgnoreCase("All")) {
				search.setAddressCity(agentCity);
			}

			if (!StringUtil.isNullOrEmpty(agentTerritory) && !agentTerritory.equalsIgnoreCase("All")) {
				search.setTerritory(agentTerritory);
			}

			if (!StringUtil.isNullOrEmpty(paxTypeCode) && !paxTypeCode.equalsIgnoreCase("All")) {
				search.setPaxCategory(paxTypeCode);
			}

			if (!StringUtil.isNullOrEmpty(reportType) && !reportType.equalsIgnoreCase("All")) {
				search.setReportType(reportType);
			}

			if (!StringUtil.isNullOrEmpty(fromDate) && !StringUtil.isNullOrEmpty(toDate)) {
				ModuleServiceLocator.getDataExtractionBD().getAgentSalesModifyRefundReports(search);
			}

		} catch (Exception e) {
			log.error("download method is failed :", e);
			throw new ModuleException("Error in report data retrieval");
		}
	}

	protected static void setAllAgents(HttpServletRequest request) throws ModuleException {
		String strList = SelectListGenerator.createAgentsListByName();
		request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strList);
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	private static void setStationList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	private static void setCountrySelectList(HttpServletRequest request) throws ModuleException {
		String strCountry = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strCountry);
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	private static void setJourneyTypeComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createJourneyTypeList();
		request.setAttribute(WebConstants.REQ_JOURNEY_TYPE_LIST, strStation);
	}

	private static void setFlightTypesHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightTypeList(false);
		setAttribInRequest(request, WebConstants.REQ_HTML_FLIGHT_TYPES, strHtml);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.ta.sal.refund.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.ta.sal.refund.gsa") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.ta.sal.refund") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
	}

	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);

	}

	private static void setClassOfService(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_COS_LIST, strHtml);
	}

	private static void setBasisCodeList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createBasisCodeList();
		request.setAttribute(WebConstants.REQ_HTML_BASIS_LIST, strHtml);

	}

	private static void setAgentCurrencyList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAgentCurrencyCodeList();
		request.setAttribute(WebConstants.REQ_HTML_CURRENCY_LIST, strHtml);
	}

	private static void setAgentAddressCityList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAgentAddressCityList();
		request.setAttribute(WebConstants.REQ_HTML_AGENT_CITY_LIST, strHtml);
	}

	private static void setTerritoryList(HttpServletRequest request) throws ModuleException {
		String types = SelectListGenerator.createTerritoryList();
		request.setAttribute(WebConstants.REQ_TERRITORY, types);
	}

	private static void setPaxTypesList(HttpServletRequest request) throws ModuleException {
		String paxTypeStr = SelectListGenerator.createPaxTypeList();
		request.setAttribute(WebConstants.PAX_TYPE, paxTypeStr);
	}

	private static void setDownloadURL(HttpServletRequest request, String timestamp) throws Exception {

		String strUsrerId = request.getUserPrincipal().getName();

		if (timestamp != null) {
			strUsrerId += "_" + timestamp;
		}

		String strurl = globalConfig.getBizParam(SystemParamKeys.REPORT_URL_PREFIX) + "agent_sales_rept";
		String strReportType = request.getParameter("selReportType");

		request.setAttribute("selReportType", strReportType);

		strurl = strurl + "_" + strUsrerId + ".zip";
		request.setAttribute("reqReptURl", strurl);

	}

	private static void setAllowRouteWiseAgentSelection(HttpServletRequest request) {
		boolean allowRouteWiseAgentSelection = AppSysParamsUtil.isEnableRouteSelectionForAgents();
		request.setAttribute("agentWiseRouteSelection", allowRouteWiseAgentSelection);
	}

}
