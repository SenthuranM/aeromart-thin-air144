package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.PaxCreditListTO;
import com.isa.thinair.xbe.api.dto.v2.PaxUsedCreditInfoTO;
import com.isa.thinair.xbe.api.dto.v2.SearchReservationTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaxCreditUtil;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadPaxCreditAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadPaxCreditAction.class);

	private SearchReservationTO searchParams;

	private boolean success = true;

	private String messageTxt;

	private String paxID;

	private String balanceToPay;

	private String paxAppliedCredit;

	private Collection<PaxCreditListTO> paxCreditList;
	
	private String pnr;
	
	private String paxFullName;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			Date departureDate = new Date();
			Date returnDate = new Date();
			Collection<ReservationPaxDTO> collection = null;
			Collection<ReservationPaxDTO> collectionOtherCarrier = null;

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			String strAgentCode = userPrincipal.getAgentCode();

			String strExactMatch = "";
			// if (searchParams.getExactMatch() == null){strExactMatch = "*";}
			String strSelectedCredit = request.getParameter("selPaxCredit");

			boolean isListReservations = false;
			
			if ((searchParams.getCreditCardNo().length() == 0)) {
				ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();

				if (!JavaScriptGenerator.checkAccess(request, PriviledgeConstants.ANY_PNR_SEARCH)) {
					reservationSearchDTO.setOwnerAgentCode(strAgentCode);
					reservationSearchDTO.setSearchGSABookings(JavaScriptGenerator.checkAccess(request,
							PriviledgeConstants.ANY_GSA_PNR_SEARCH));
					reservationSearchDTO.setBookingCategories(this.getBookingCategories());
				}

				if (searchParams.getPnr().trim().length() > 0) {
					reservationSearchDTO.setPnr(searchParams.getPnr().trim());
				}
				if (searchParams.getFirstName().trim().length() > 0) {
					reservationSearchDTO.setFirstName(searchParams.getFirstName().trim() + strExactMatch);
				}
				if (searchParams.getLastName().trim().length() > 0) {
					reservationSearchDTO.setLastName(searchParams.getLastName().trim() + strExactMatch);
				}
				if (searchParams.getFromAirport().trim().length() > 0) {
					reservationSearchDTO.setFromAirport(searchParams.getFromAirport().trim());
				}
				if (searchParams.getToAirport().trim().length() > 0) {
					reservationSearchDTO.setToAirport(searchParams.getToAirport().trim());
				}

				if (searchParams.getPhoneNo().trim().length() > 0) {
					reservationSearchDTO.setTelephoneNo(searchParams.getPhoneNo().trim());
				}

				if (searchParams.getDepatureDate().trim().length() > 0) {
					departureDate = new SimpleDateFormat("dd/MM/yyyy").parse(searchParams.getDepatureDate().trim());
					reservationSearchDTO.setDepartureDate(departureDate);
				}
				if (searchParams.getReturnDate().trim().length() > 0) {
					returnDate = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(searchParams.getReturnDate().trim() + " 23:59");
					reservationSearchDTO.setReturnDate(returnDate);
				}

				if (searchParams.getLoadZeroBalance() == null) {
					reservationSearchDTO.setFilterZeroCredits(true);
				}

				if (searchParams.getFlightNo().trim().length() > 0) {
					reservationSearchDTO.setFlightNo(searchParams.getFlightNo().trim());
				}
				reservationSearchDTO.setOriginChannelIds(BasicRH.getSearchableOriginSalesChannels(request,
						isListReservations));

				collection = ModuleServiceLocator.getReservationQueryBD().getPnrPassengerSumCredit(reservationSearchDTO);
				// searching other carriers for pax credit as well
				if (searchParams.getSearchOtheCarrier() != null && AppSysParamsUtil.isLCCConnectivityEnabled()) {
					collectionOtherCarrier = ModuleServiceLocator.getLCCPaxCreditBD().getPaxCreditAccrossCarriers(
							reservationSearchDTO, getTrackInfo());
					collection.addAll(collectionOtherCarrier);
				}
				eliminateSamePnrPaxCredit(collection);				
			} else {
				ReservationPaymentDTO reservationPaymentDTO = new ReservationPaymentDTO();
				reservationPaymentDTO.setOwnerAgentCode(strAgentCode);
				if (searchParams.getCreditCardNo().trim().length() > 0) {
					reservationPaymentDTO.setCreditCardNo(searchParams.getCreditCardNo().trim());
				}
				if (searchParams.getCardExpiry().trim().length() > 0) {
					reservationPaymentDTO.setEDate(searchParams.getCardExpiry().trim());
				}
				reservationPaymentDTO.setOriginChannelIds(BasicRH.getSearchableOriginSalesChannels(request,
						isListReservations));

				collection = ModuleServiceLocator.getReservationQueryBD().getReservationsForCreditCardInfo(reservationPaymentDTO);

				// searching other carriers for pax credit as well
				if (searchParams.getSearchOtheCarrier() != null && AppSysParamsUtil.isLCCConnectivityEnabled()) {
					collectionOtherCarrier = ModuleServiceLocator.getLCCPaxCreditBD().getPaxCreditAccrossCarriers(
							reservationPaymentDTO, getTrackInfo());
					collection.addAll(collectionOtherCarrier);
				}
			}

			List<PaxUsedCreditInfoTO> extractedList = extractSelectedCredit(strSelectedCredit);

			paxCreditList = PaxCreditUtil.createPaxCredit(collection, request.getParameter("paxPayments"), extractedList);
			BigDecimal appliedCredit = PaxCreditUtil.getSelectedPaxCreditAmount(extractedList);
			balanceToPay = AccelAeroCalculator.formatAsDecimal(new BigDecimal(balanceToPay));
			paxAppliedCredit = AccelAeroCalculator.formatAsDecimal(appliedCredit);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private List<PaxUsedCreditInfoTO> extractSelectedCredit(String strPaxCredits) throws Exception {

		List<PaxUsedCreditInfoTO> selectedCredit = new ArrayList<PaxUsedCreditInfoTO>();
		if (strPaxCredits != null && !strPaxCredits.equals("null")) {
			JSONArray jsonCreitArray = (JSONArray) new JSONParser().parse(strPaxCredits);
			Iterator<?> creditIter = jsonCreitArray.iterator();
			while (creditIter.hasNext()) {
				JSONObject jObject = (JSONObject) creditIter.next();
				if (jObject != null) {
					PaxUsedCreditInfoTO usedCredit = new PaxUsedCreditInfoTO();
					usedCredit.setPaxCreditId((String) jObject.get("paxID"));
					usedCredit.setUsedAmount((String) jObject.get("utilizedCredit"));
					usedCredit.setPaxSequence(paxID);
					usedCredit.setCarrier((String) jObject.get("carrier"));
					usedCredit.setPnr((String) jObject.get("pnr"));
					usedCredit.setResidingCarrierAmount((String) jObject.get("residingCarrierAmount"));
					selectedCredit.add(usedCredit);
				}
			}
		}
		return selectedCredit;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the searchParams
	 */
	public SearchReservationTO getSearchParams() {
		return searchParams;
	}

	/**
	 * @param searchParams
	 *            the searchParams to set
	 */
	public void setSearchParams(SearchReservationTO searchParams) {
		this.searchParams = searchParams;
	}

	/**
	 * @return the paxCreditList
	 */
	public Collection<PaxCreditListTO> getPaxCreditList() {
		return paxCreditList;
	}

	public String getPaxID() {
		return paxID;
	}

	public void setPaxID(String paxID) {
		this.paxID = paxID;
	}

	public String getBalanceToPay() {
		return balanceToPay;
	}

	public void setBalanceToPay(String balanceToPay) {
		this.balanceToPay = balanceToPay;
	}

	public String getPaxAppliedCredit() {
		return paxAppliedCredit;
	}

	/**
	 * @return
	 * @throws ModuleException
	 */
	private Collection<String> getBookingCategories() throws ModuleException {
		Collection<String> privilegeBookingCategories = new ArrayList<String>();
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_STANDARD_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.STANDARD.getCode());
		}
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_CHARTER_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.CHARTER.getCode());
		}
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_HR_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.HR.getCode());
		}
		return privilegeBookingCategories;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaxFullName() {
		return paxFullName;
	}

	public void setPaxFullName(String paxFullName) {
		this.paxFullName = paxFullName;
	}
	
	
	/**
	 * function to control over same PAX credit getting applied twice. since available credit of PAX is already consumed
	 * when calculating balance summary.
	 * */
	private void eliminateSamePnrPaxCredit(Collection<ReservationPaxDTO> collection) {
		if (collection != null && !collection.isEmpty()) {
			String replaceChars = "[\\s\\.]";
			Iterator<ReservationPaxDTO> pnrPaxDTOItr = collection.iterator();
			while (pnrPaxDTOItr.hasNext()) {
				ReservationPaxDTO pnrPaxDTO = pnrPaxDTOItr.next();

				if (!StringUtil.isNullOrEmpty(pnr) && pnr.equalsIgnoreCase(pnrPaxDTO.getPnr())) {
					Iterator<ReservationPaxDetailsDTO> passengerItr = pnrPaxDTO.getPassengers().iterator();
					while (passengerItr.hasNext()) {
						ReservationPaxDetailsDTO PaxDetailsDTO = passengerItr.next();
						StringBuilder sb = new StringBuilder();
						if(PaxDetailsDTO.isChild()){
							sb.append(PaxDetailsDTO.getPaxType());
						}
						sb.append(PaxDetailsDTO.getTitle()).append(".").append(PaxDetailsDTO.getFirstName())
								.append(PaxDetailsDTO.getLastName());

						sb = new StringBuilder(sb.toString().replaceAll(replaceChars, ""));
						if (!StringUtil.isNullOrEmpty(paxFullName)) {
							paxFullName = paxFullName.replaceAll(replaceChars, "");

							if (paxFullName.equalsIgnoreCase(sb.toString())) {
								passengerItr.remove();
							}
						}

					}

					if (pnrPaxDTO.getPassengers() == null || pnrPaxDTO.getPassengers().isEmpty()) {
						pnrPaxDTOItr.remove();
					}

				}

			}
		}
	}

}
