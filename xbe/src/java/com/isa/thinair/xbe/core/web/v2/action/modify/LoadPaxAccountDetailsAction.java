package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.CarrierwiseChargeAdjustments;
import com.isa.thinair.xbe.api.dto.v2.ExternalPaymentsTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountChargesTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountCreditTO;
import com.isa.thinair.xbe.api.dto.v2.PaxAccountPaymentsTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaxAccountUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadPaxAccountDetailsAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadPaxAccountDetailsAction.class);

	private boolean success = true;

	private String messageTxt;

	private PaxAccountChargesTO paxAccountChargesTO;

	private PaxAccountPaymentsTO paxAccountPaymentsTO;

	private Collection<ExternalPaymentsTO> paxAccountExternalPaymentTO;

	private PaxAccountCreditTO paxAccountCreditTO;

	private Collection<CarrierwiseChargeAdjustments> chargeAdjustmentTypes;

	private String segAdjustOption;

	private String status;

	private String pnr;

	private String groupPNR;

	@SuppressWarnings("unchecked")
	public String execute() {
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			boolean isGroupPnr = ReservationUtil.isGroupPnr(groupPNR);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo.getReservationStatus(), colsegs,
					resInfo.isModifiableReservation(), false, null, null, false, false);
			if (isGroupPnr) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(), null);
			}
			request.setAttribute("reqPaxID", request.getParameter("paxID"));
			String strCurrency = AppSysParamsUtil.getBaseCurrency();

			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			String agentCurrency = up.getAgentCurrencyCode();

			paxAccountExternalPaymentTO = PaxAccountUtil.getExternalPayments();
			LCCClientReservationPax pax = null;

			Map<String, String> mapAllAgentCodeDesc = XBEModuleUtils.getGlobalConfig().getAgentsByAgentCode();
			Collection<LCCClientReservationPax> colpaxs = ReservationUtil
					.transformJsonPassengers(request.getParameter("respaxs"));
			pax = PaxAccountUtil.getSelectedPax(request.getParameter("paxID"), colpaxs);

			paxAccountPaymentsTO = PaxAccountUtil.getPaymentDetails(pax, status, mapAllAgentCodeDesc, false);

			List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(request.getParameter("resONDs"));

			paxAccountChargesTO = PaxAccountUtil.getChargesDetails(pax, strCurrency, colONDs, isGroupPnr, colsegs, agentCurrency,
					status, resInfo.getReservationLastModDate(), Boolean.parseBoolean(request.getParameter("isInfantPaymentSeparated")));

			this.segAdjustOption = ReservationUtil.createAjdustementSegmentOptions(colONDs, rpParams);

			this.chargeAdjustmentTypes = PaxAccountUtil.getChargeAdjustmentTypes(isGroupPnr, getCarrierList(colsegs),
					getTrackInfo());

			paxAccountCreditTO = PaxAccountUtil.getCreditDetails(pax);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * Method to load just the necessary details for group adjustment
	 */
	public String loadGroupAdjustDetails() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			boolean isGroupPnr = ReservationUtil.isGroupPnr(groupPNR);
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			this.chargeAdjustmentTypes = PaxAccountUtil.getChargeAdjustmentTypes(isGroupPnr, getCarrierList(colsegs),
					getTrackInfo());

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	// Gets the set of carrier codes involved in the reservation
	private Set<String> getCarrierList(Collection<LCCClientReservationSegment> colsegs) {
		Set<String> carrierCodeSet = new HashSet<String>();
		for (LCCClientReservationSegment segment : colsegs) {
			carrierCodeSet.add(segment.getCarrierCode());
		}
		return carrierCodeSet;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the paxAccountChargesTO
	 */
	public PaxAccountChargesTO getPaxAccountChargesTO() {
		return paxAccountChargesTO;
	}

	/**
	 * @return the paxAccountPaymentsTO
	 */
	public PaxAccountPaymentsTO getPaxAccountPaymentsTO() {
		return paxAccountPaymentsTO;
	}

	/**
	 * @return the paxAccountExternalPaymentTO
	 */
	public Collection<ExternalPaymentsTO> getPaxAccountExternalPaymentTO() {
		return paxAccountExternalPaymentTO;
	}

	/**
	 * @return the paxAccountCreditTO
	 */
	public PaxAccountCreditTO getPaxAccountCreditTO() {
		return paxAccountCreditTO;
	}

	public String getSegAdjustOption() {
		return segAdjustOption;
	}

	public void setSegAdjustOption(String segAdjustOption) {
		this.segAdjustOption = segAdjustOption;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setChargeAdjustmentTypes(Collection<CarrierwiseChargeAdjustments> chargeAdjustmentTypes) {
		this.chargeAdjustmentTypes = chargeAdjustmentTypes;
	}

	public Collection<CarrierwiseChargeAdjustments> getChargeAdjustmentTypes() {
		return chargeAdjustmentTypes;
	}
}
