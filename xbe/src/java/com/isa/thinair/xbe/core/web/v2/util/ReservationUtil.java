package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.DepAPFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.UserNoteHistoryTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ClassOfServicePreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DisplayTickerRqDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareDiscountTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRulesInformationDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiFareRulesInformationDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareAllocTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareAllocTO.BCType;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SegmentInvFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOtherAirlineSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentHolder;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPaxNamesTranslation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.FlightStopOverDisplayUtil;
import com.isa.thinair.airproxy.api.utils.PNRModificationAuthorizationUtils;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.FareCategoryTO.FareCategoryStatusCodes;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.Constants.ReservationFlow;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorTransactionFeeUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.FlightSegmentFETO;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxFareTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxPriceTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.reservation.ServiceTaxCCParamsDTO;
import com.isa.thinair.webplatform.api.v2.reservation.SurchargeFETO;
import com.isa.thinair.webplatform.api.v2.reservation.TaxFETO;
import com.isa.thinair.webplatform.api.v2.util.BigDecimalWrapper;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.api.dto.v2.AllFaresInformationDTO;
import com.isa.thinair.xbe.api.dto.v2.AncillaryPaxTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.FareTypeInfoTO;
import com.isa.thinair.xbe.api.dto.v2.IDisplayPax;
import com.isa.thinair.xbe.api.dto.v2.InfantPaxTO;
import com.isa.thinair.xbe.api.dto.v2.InvAllocationDTO;
import com.isa.thinair.xbe.api.dto.v2.PaxUsedCreditInfoTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.generator.reservation.JavaScriptGenerator;

/**
 * @author pkarunanayake
 * 
 */
public class ReservationUtil {

	private static final Log log = LogFactory.getLog(ReservationUtil.class);

	/**
	 * 
	 * @param strPNRs
	 * @param strStatus
	 * @param strAgent
	 * @param strBookingDate
	 * @param loadedByMarketing
	 *            Specifies whether this booking was loaded by the marketing carrier
	 * @return
	 */
	public static BookingTO loadBookingInfo(String strPNR, String strStatus, String strAgent, String strBookingDate,
			String version, int adults, int child, int infant, boolean loadedByMarketing, boolean groupPNR,
			String bookingCategory, String originCountry, String itineraryFareMask, String ticketValidTill,
			String ticketValidFrom) {
		BookingTO bookingTO = new BookingTO();

		bookingTO.setPNR(strPNR);
		bookingTO.setBookingDate(strBookingDate);
		bookingTO.setStatus(strStatus);
		bookingTO.setAgent(strAgent);
		bookingTO.setVersion(version);
		bookingTO.setGroupPNR(groupPNR);
		bookingTO.setTotalPaxAdultCount(Integer.toString(adults));
		bookingTO.setTotalPaxChildCount(Integer.toString(child));
		bookingTO.setTotalPaxInfantCount(Integer.toString(infant));
		bookingTO.setBookingCategory(bookingCategory);
		bookingTO.setOriginCountryOfCall(originCountry);
		bookingTO.setItineraryFareMask(itineraryFareMask);

		if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(ticketValidTill)) {
			bookingTO.setTicketExpiryEnabled(true);
			bookingTO.setTicketExpiryDate(ticketValidTill);

			if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(ticketValidFrom)) {
				bookingTO.setTicketValidFromDate(ticketValidFrom);
			}
		}

		bookingTO.setLoadedByMarketing(loadedByMarketing ? "true" : "false");

		return bookingTO;
	}

	/**
	 * 
	 * @param lccClientReservationContactInfo
	 * @return
	 */
	public static ContactInfoTO loadContactInfo(CommonReservationContactInfo lccClientReservationContactInfo) {
		ContactInfoTO contactInfoTO = new ContactInfoTO();

		if (lccClientReservationContactInfo != null) {
			contactInfoTO.setTitle(lccClientReservationContactInfo.getTitle());
			contactInfoTO.setFirstName(lccClientReservationContactInfo.getFirstName());
			contactInfoTO.setLastName(lccClientReservationContactInfo.getLastName());
			contactInfoTO.setStreet(BeanUtils.nullHandler(lccClientReservationContactInfo.getStreetAddress1()));
			contactInfoTO.setAddress(BeanUtils.nullHandler(lccClientReservationContactInfo.getStreetAddress2()));
			contactInfoTO.setCity(BeanUtils.nullHandler(lccClientReservationContactInfo.getCity()));
			contactInfoTO.setCountry(BeanUtils.nullHandler(lccClientReservationContactInfo.getCountryCode()));
			contactInfoTO.setZipCode(BeanUtils.nullHandler(lccClientReservationContactInfo.getZipCode()));
			contactInfoTO.setNationality(BeanUtils.nullHandler(lccClientReservationContactInfo.getNationalityCode()));

			String[] arrMobile = new String[3];
			arrMobile[0] = "";
			arrMobile[1] = "";
			arrMobile[2] = "";

			String[] arrPhone = new String[3];
			arrPhone[0] = "";
			arrPhone[1] = "";
			arrPhone[2] = "";

			String[] arrFax = new String[3];
			arrFax[0] = "";
			arrFax[1] = "";
			arrFax[2] = "";

			String[] mobileNo = BeanUtils.nullHandler(lccClientReservationContactInfo.getMobileNo()).split("-");
			for (int i = 0; i < mobileNo.length; i++) {
				arrMobile[i] = mobileNo[i];
			}

			String[] phoneNo = BeanUtils.nullHandler(lccClientReservationContactInfo.getPhoneNo()).split("-");
			for (int i = 0; i < phoneNo.length; i++) {
				arrPhone[i] = phoneNo[i];
			}

			String[] faxNo = BeanUtils.nullHandler(lccClientReservationContactInfo.getFax()).split("-");
			for (int i = 0; i < faxNo.length; i++) {
				arrFax[i] = faxNo[i];
			}

			contactInfoTO.setMobileCountry(arrMobile[0]);
			contactInfoTO.setMobileArea(arrMobile[1]);
			contactInfoTO.setMobileNo(arrMobile[2]);

			contactInfoTO.setPhoneCountry(arrPhone[0]);
			contactInfoTO.setPhoneArea(arrPhone[1]);
			contactInfoTO.setPhoneNo(arrPhone[2]);

			contactInfoTO.setFaxCountry(arrFax[0]);
			contactInfoTO.setFaxArea(arrFax[1]);
			contactInfoTO.setFaxNo(arrFax[2]);

			contactInfoTO.setEmail(BeanUtils.nullHandler(lccClientReservationContactInfo.getEmail()));
			contactInfoTO.setPreferredLang(BeanUtils.nullHandler(lccClientReservationContactInfo.getPreferredLanguage()));
			contactInfoTO.setTaxRegNo(BeanUtils.nullHandler(lccClientReservationContactInfo.getTaxRegNo()));
			contactInfoTO.setState(BeanUtils.nullHandler(lccClientReservationContactInfo.getState()));

			// set Emergency contact information
			contactInfoTO.setEmgnTitle(BeanUtils.nullHandler(lccClientReservationContactInfo.getEmgnTitle()));
			contactInfoTO.setEmgnFirstName(BeanUtils.nullHandler(lccClientReservationContactInfo.getEmgnFirstName()));
			contactInfoTO.setEmgnLastName(BeanUtils.nullHandler(lccClientReservationContactInfo.getEmgnLastName()));

			String[] arrEmgnPhone = new String[3];
			arrEmgnPhone[0] = "";
			arrEmgnPhone[1] = "";
			arrEmgnPhone[2] = "";

			String[] emgnPhoneNo = BeanUtils.nullHandler(lccClientReservationContactInfo.getEmgnPhoneNo()).split("-");
			for (int i = 0; i < emgnPhoneNo.length; i++) {
				arrEmgnPhone[i] = emgnPhoneNo[i];
			}

			contactInfoTO.setEmgnPhoneCountry(BeanUtils.nullHandler(arrEmgnPhone[0]));
			contactInfoTO.setEmgnPhoneArea(BeanUtils.nullHandler(arrEmgnPhone[1]));
			contactInfoTO.setEmgnPhoneNo(BeanUtils.nullHandler(arrEmgnPhone[2]));

			contactInfoTO.setEmgnEmail(BeanUtils.nullHandler(lccClientReservationContactInfo.getEmgnEmail()));
		}

		return contactInfoTO;
	}

	/**
	 * @param passengers
	 *            - get the payable passenger count.
	 * @return
	 */
	public static int getNoOfPayablePaxCount(Collection<PaymentPassengerTO> passengers) {
		int payablePaxCount = 0;
		for (PaymentPassengerTO pax : passengers) {
			if (pax.getDisplayToPay() != null && !pax.getDisplayToPay().isEmpty() && Double.valueOf(pax.getDisplayToPay()) > 0) {
				payablePaxCount++;
			}
		}
		return payablePaxCount;
	}

	/**
	 * @param passengers
	 *            - get the payable passenger count .
	 * @return
	 */
	public static int getNoOfPayablePaxCount(Set<LCCClientReservationPax> passengers) {
		int payablePaxCount = 0;
		for (LCCClientReservationPax pax : passengers) {
			if (pax.getTotalAvailableBalance().doubleValue() > 0) {
				payablePaxCount++;
			}
		}
		return payablePaxCount;
	}

	public static Object[] loadPaxList(Set<LCCClientReservationPax> setClientReservationPaxs, ReservationProcessParams rParam,
			boolean isGroupPnr, String reservationStatus, NameChangeParameters nameModificationParams,
			boolean isInfantPaymentSeparated) throws ModuleException {
		Object[] objects = new Object[3];

		Collection<AdultPaxTO> collection = new ArrayList<AdultPaxTO>();
		Collection<InfantPaxTO> collectionInfant = new ArrayList<InfantPaxTO>();
		Collection<PaxFareTO> paxFareCollection = new ArrayList<PaxFareTO>();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		if (setClientReservationPaxs != null) {
			int intPaxCount = 0;
			int intInf = 0;
			int intChild = 0;

			BigDecimalWrapper totalPrice = new BigDecimalWrapper();
			BigDecimalWrapper ppAdultFare = new BigDecimalWrapper();
			BigDecimalWrapper ppAdultSurcharge = new BigDecimalWrapper();
			BigDecimalWrapper ppAdultTax = new BigDecimalWrapper();
			BigDecimalWrapper ppAdultOthe = new BigDecimalWrapper();

			BigDecimalWrapper ppChildFare = new BigDecimalWrapper();
			BigDecimalWrapper ppChildSurcharge = new BigDecimalWrapper();
			BigDecimalWrapper ppChildTax = new BigDecimalWrapper();
			BigDecimalWrapper ppChildOther = new BigDecimalWrapper();

			BigDecimalWrapper ppInfantFare = new BigDecimalWrapper();
			BigDecimalWrapper ppInfantSurcharge = new BigDecimalWrapper();
			BigDecimalWrapper ppInfantTax = new BigDecimalWrapper();
			BigDecimalWrapper ppInfantOther = new BigDecimalWrapper();

			boolean blnAdltFilled = false;
			boolean blnChildFilled = false;
			boolean blnInfantFilled = false;

			boolean showPaxEticket = AppSysParamsUtil.isShowPaxETKT()
					&& (reservationStatus != null && !reservationStatus.equals(ReservationStatus.ON_HOLD));

			boolean allowViewMCO = rParam.isAllowViewMCO(); // && AppSysParamsUtil.enableElectronicMCO()

			for (LCCClientReservationPax lccClientReservationPax : SortUtil.sortPax(setClientReservationPaxs)) {

				IDisplayPax pax = null;
				int zeroIndexPaxSequence = 0;
				int orderSequence = 0;
				int displaySequence = 0;

				Integer pnrPaxID = getCarrierPnrPaxId(lccClientReservationPax.getTravelerRefNumber());
				boolean hasMCO = ModuleServiceLocator.getChargeBD().hasMCORecords(pnrPaxID);

				if (lccClientReservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
					zeroIndexPaxSequence = intInf++;
					orderSequence = zeroIndexPaxSequence + 1;
					displaySequence = orderSequence;
					InfantPaxTO infantPax = InfantPaxTO.getInfantDisplayInstance(zeroIndexPaxSequence, showPaxEticket,
							allowViewMCO, hasMCO, rParam.isViewPassengerAccountAllowed(), lccClientReservationPax,
							isInfantPaymentSeparated);
					collectionInfant.add(infantPax);
					pax = infantPax;

					if (!blnInfantFilled || !isGroupPnr) {
						ppInfantFare.add(lccClientReservationPax.getTotalFare());
						ppInfantSurcharge.add(lccClientReservationPax.getTotalSurCharge());
						ppInfantTax.add(lccClientReservationPax.getTotalTaxCharge());
						ppInfantOther.add(lccClientReservationPax.getTotalAdjustmentCharge());
						ppInfantOther.add(lccClientReservationPax.getTotalCancelCharge());
						ppInfantOther.add(lccClientReservationPax.getTotalModificationCharge());
						blnInfantFilled = true;
					}
				} else if (lccClientReservationPax.getPaxType().equals(PaxTypeTO.CHILD)) {
					intChild++;
					zeroIndexPaxSequence = intPaxCount++;
					orderSequence = zeroIndexPaxSequence + 1;
					displaySequence = lccClientReservationPax.getPaxSequence();

					AdultPaxTO childPaxTO = AdultPaxTO.getChildDisplayInstance(lccClientReservationPax, zeroIndexPaxSequence,
							rParam.isViewPassengerAccountAllowed(), showPaxEticket, allowViewMCO, hasMCO);
					collection.add(childPaxTO);
					pax = childPaxTO;

					if (!blnChildFilled || !isGroupPnr) {
						ppChildFare.add(lccClientReservationPax.getTotalFare());
						ppChildSurcharge.add(AccelAeroCalculator.subtract(lccClientReservationPax.getTotalSurCharge(),
								getPaxUserDefinedSSRTotalAmount(lccClientReservationPax)));
						ppChildTax.add(lccClientReservationPax.getTotalTaxCharge());
						ppChildOther.add(lccClientReservationPax.getTotalAdjustmentCharge());
						ppChildOther.add(lccClientReservationPax.getTotalCancelCharge());
						ppChildOther.add(lccClientReservationPax.getTotalModificationCharge());
						blnChildFilled = true;
					}

				} else if (lccClientReservationPax.getPaxType().equals(PaxTypeTO.ADULT)) {
					zeroIndexPaxSequence = intPaxCount++;
					orderSequence = zeroIndexPaxSequence + 1;
					displaySequence = lccClientReservationPax.getPaxSequence();

					AdultPaxTO adultPaxTO = AdultPaxTO.getAdultDisplayInstance(lccClientReservationPax, zeroIndexPaxSequence,
							rParam.isViewPassengerAccountAllowed(), showPaxEticket, allowViewMCO, hasMCO);
					collection.add(adultPaxTO);
					pax = adultPaxTO;

					if (!blnAdltFilled || !isGroupPnr) {
						ppAdultFare.add(lccClientReservationPax.getTotalFare());
						ppAdultSurcharge.add(AccelAeroCalculator.subtract(lccClientReservationPax.getTotalSurCharge(),
								getPaxUserDefinedSSRTotalAmount(lccClientReservationPax)));
						ppAdultTax.add(lccClientReservationPax.getTotalTaxCharge());
						ppAdultOthe.add(lccClientReservationPax.getTotalAdjustmentCharge());
						ppAdultOthe.add(lccClientReservationPax.getTotalCancelCharge());
						ppAdultOthe.add(lccClientReservationPax.getTotalModificationCharge());
						blnAdltFilled = true;
					}

				}
				populatePaxDisplayInfo(pax, lccClientReservationPax, reservationStatus, rParam, nameModificationParams,
						orderSequence, displaySequence);

			}

			String strCurrnecy = AppSysParamsUtil.getBaseCurrency();
			if (intPaxCount - intChild > 0) {
				PaxFareTO adultFareTO = ReservationBeanUtil.createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_ADULT_DISPLAY,
						intPaxCount - intChild, totalPrice, ppAdultFare, ppAdultSurcharge, ppAdultTax, ppAdultOthe, strCurrnecy,
						isGroupPnr, exchangeRateProxy);
				paxFareCollection.add(adultFareTO);
			}

			if (intChild > 0) {
				PaxFareTO childFareTO = ReservationBeanUtil.createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_CHILD_DISPLAY,
						intChild, totalPrice, ppChildFare, ppChildSurcharge, ppChildTax, ppChildOther, strCurrnecy, isGroupPnr,
						exchangeRateProxy);
				paxFareCollection.add(childFareTO);
			}

			if (intInf > 0) {
				PaxFareTO intantFareTO = ReservationBeanUtil.createPaxFareTOWhileAddingTotalPrice(
						PaxTypeTO.PAX_TYPE_INFANT_DISPLAY, intInf, totalPrice, ppInfantFare, ppInfantSurcharge, ppInfantTax,
						ppInfantOther, strCurrnecy, isGroupPnr, exchangeRateProxy);
				paxFareCollection.add(intantFareTO);
			}
		}

		objects[0] = collection;
		objects[1] = collectionInfant;
		objects[2] = paxFareCollection;

		return objects;

	}

	private static BigDecimal getPaxUserDefinedSSRTotalAmount(LCCClientReservationPax lccClientReservationPax) {
		List<LCCSelectedSegmentAncillaryDTO> selectedAnciList = lccClientReservationPax.getSelectedAncillaries();
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (selectedAnciList != null && !selectedAnciList.isEmpty()) {
			for (LCCSelectedSegmentAncillaryDTO anci : selectedAnciList) {
				for (LCCSpecialServiceRequestDTO ssr : anci.getSpecialServiceRequestDTOs()) {
					if (ssr.isUserDefinedCharge()) {
						amount = AccelAeroCalculator.add(amount, ssr.getCharge());
					}
				}
			}
		}

		return amount;
	}

	private static void populatePaxDisplayInfo(IDisplayPax pax, LCCClientReservationPax lccClientReservationPax,
			String reservationStatus, ReservationProcessParams rParam, NameChangeParameters nameModificationParams,
			int displayOrderPaxSequence, int displayPaxSequence) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat time = new SimpleDateFormat("HH:mm");

		pax.setDisplayPaxTravelReference(lccClientReservationPax.getTravelerRefNumber());
		pax.setDisplayPaxTitle(lccClientReservationPax.getTitle());

		if (AppSysParamsUtil.isLoadPaxNamesInBlockLetters()) {
			pax.setDisplayPaxFirstName(lccClientReservationPax.getFirstName().toUpperCase());
			pax.setDisplayPaxLastName(lccClientReservationPax.getLastName().toUpperCase());
		} else {
			pax.setDisplayPaxFirstName(lccClientReservationPax.getFirstName());
			pax.setDisplayPaxLastName(lccClientReservationPax.getLastName());
		}
		if (lccClientReservationPax.getLccClientPaxNameTranslations() != null) {
			String firstNameInOtherLanguage = lccClientReservationPax.getLccClientPaxNameTranslations().getFirstNameOl();
			String lastNameInOtherLanguage = lccClientReservationPax.getLccClientPaxNameTranslations().getLastNameOl();
			String infantTitleInOtherLanguage = lccClientReservationPax.getLccClientPaxNameTranslations().getTitleOl();
			pax.setDisplayPaxFirstNameOl((firstNameInOtherLanguage != null)
					? com.isa.thinair.commons.core.util.StringUtil.getUnicode(firstNameInOtherLanguage)
					: "");
			pax.setDisplayPaxLastNameOl((lastNameInOtherLanguage != null)
					? (com.isa.thinair.commons.core.util.StringUtil.getUnicode(lastNameInOtherLanguage))
					: "");
			pax.setDisplayNameTranslationLanguage(
					StringUtil.getNotNullString(lccClientReservationPax.getLccClientPaxNameTranslations().getLanguageCode()));
			pax.setDisplayPaxTitleOl((infantTitleInOtherLanguage != null)
					? com.isa.thinair.commons.core.util.StringUtil.getUnicode(infantTitleInOtherLanguage)
					: "");
		}
		pax.setDisplayPaxNationality(StringUtil.getNotNullString(lccClientReservationPax.getNationalityCode()));
		pax.setDisplayPaxDOB(dateToStringFormat(lccClientReservationPax.getDateOfBirth()));
		pax.setDisplayPaxCategory(lccClientReservationPax.getPaxCategory());

		if (lccClientReservationPax.getLccClientAdditionPax() != null) {
			String psptNo = lccClientReservationPax.getLccClientAdditionPax().getPassportNo();
			Date psptExpiryDt = lccClientReservationPax.getLccClientAdditionPax().getPassportExpiry();
			String psptIssuedCntry = lccClientReservationPax.getLccClientAdditionPax().getPassportIssuedCntry();
			String arrivalIntlFltNo = lccClientReservationPax.getLccClientAdditionPax().getArrivalIntlFltNo();
			Date intlFltArrivalDate = lccClientReservationPax.getLccClientAdditionPax().getIntlFltArrivalDate();
			String departureIntlFltNo = lccClientReservationPax.getLccClientAdditionPax().getDepartureIntlFltNo();
			Date intlFltDepartureDate = lccClientReservationPax.getLccClientAdditionPax().getIntlFltDepartureDate();
			String pnrPaxGroupId = lccClientReservationPax.getLccClientAdditionPax().getPnrPaxGroupId();
			String ffid = lccClientReservationPax.getLccClientAdditionPax().getFfid();

			if (psptExpiryDt != null) {
				pax.setDisplayPnrPaxCatFOIDExpiry(sdf.format(psptExpiryDt));
			} else {
				pax.setDisplayPnrPaxCatFOIDExpiry("");
			}

			pax.setDisplayPnrPaxCatFOIDPlace(BeanUtils.nullHandler(psptIssuedCntry));
			pax.setDisplayPnrPaxPlaceOfBirth(lccClientReservationPax.getLccClientAdditionPax().getPlaceOfBirth());
			pax.setDisplayTravelDocType(lccClientReservationPax.getLccClientAdditionPax().getTravelDocumentType());
			pax.setDisplayVisaDocNumber(lccClientReservationPax.getLccClientAdditionPax().getVisaDocNumber());
			pax.setDisplayVisaDocPlaceOfIssue(lccClientReservationPax.getLccClientAdditionPax().getVisaDocPlaceOfIssue());
			pax.setDisplayVisaApplicableCountry(lccClientReservationPax.getLccClientAdditionPax().getVisaApplicableCountry());
			pax.setDisplayFFID(ffid);

			if (lccClientReservationPax.getLccClientAdditionPax().getNationalIDNo() != null) {
				pax.setDisplayNationalIDNo(lccClientReservationPax.getLccClientAdditionPax().getNationalIDNo());
			} else {
				pax.setDisplayNationalIDNo("");
			}
			if (lccClientReservationPax.getLccClientAdditionPax().getVisaDocIssueDate() != null) {
				pax.setDisplayVisaDocIssueDate(
						sdf.format(lccClientReservationPax.getLccClientAdditionPax().getVisaDocIssueDate()));
			} else {
				pax.setDisplayVisaDocIssueDate("");
			}

			if (psptNo != null) {
				/*
				 * split FOID number to get PSPT #, expiry date & issued location This is to support for old records
				 * with comma separated values
				 */

				String[] foidArr = psptNo.split(ReservationInternalConstants.PassengerConst.FOID_SEPARATOR);

				if (foidArr.length == 0) {
					pax.setDisplayPnrPaxCatFOIDNumber("");
				} else if (foidArr.length == 1) {
					pax.setDisplayPnrPaxCatFOIDNumber(BeanUtils.nullHandler(foidArr[0]));
				} else if (foidArr.length == 2) {
					pax.setDisplayPnrPaxCatFOIDNumber(BeanUtils.nullHandler(foidArr[0]));
					if (psptExpiryDt == null) {
						pax.setDisplayPnrPaxCatFOIDExpiry(BeanUtils.nullHandler(foidArr[1]));
					}
				} else if (foidArr.length == 3) {
					pax.setDisplayPnrPaxCatFOIDNumber(BeanUtils.nullHandler(foidArr[0]));
					if (psptExpiryDt == null) {
						pax.setDisplayPnrPaxCatFOIDExpiry(BeanUtils.nullHandler(foidArr[1]));
					}
					if (psptExpiryDt == null) {
						pax.setDisplayPnrPaxCatFOIDPlace(BeanUtils.nullHandler(foidArr[2]));
					}
				}
			} else {
				pax.setDisplayPnrPaxCatFOIDNumber("");
			}

			if (intlFltArrivalDate != null) {
				pax.setDisplaypnrPaxFltArrivalDate(sdf.format(intlFltArrivalDate));
				pax.setDisplaypnrPaxArrivalTime(time.format(intlFltArrivalDate));
			} else {
				pax.setDisplaypnrPaxFltArrivalDate("");
				pax.setDisplaypnrPaxArrivalTime("");
			}

			if (intlFltDepartureDate != null) {
				pax.setDisplaypnrPaxFltDepartureDate(sdf.format(intlFltDepartureDate));
				pax.setDisplaypnrPaxDepartureTime(time.format(intlFltDepartureDate));
			} else {
				pax.setDisplaypnrPaxFltDepartureDate("");
				pax.setDisplaypnrPaxDepartureTime("");
			}

			if (arrivalIntlFltNo != null) {
				pax.setDisplaypnrPaxArrivalFlightNumber(arrivalIntlFltNo);
			} else {
				pax.setDisplaypnrPaxArrivalFlightNumber("");
			}

			if (departureIntlFltNo != null) {
				pax.setDisplaypnrPaxDepartureFlightNumber(departureIntlFltNo);
			} else {
				pax.setDisplaypnrPaxDepartureFlightNumber("");
			}

			if (pnrPaxGroupId != null) {
				pax.setDisplayPnrPaxGroupId(pnrPaxGroupId);
			} else {
				pax.setDisplayPnrPaxGroupId("");
			}
		} else {
			pax.setDisplayPnrPaxCatFOIDNumber("");
			pax.setDisplayPnrPaxCatFOIDExpiry("");
			pax.setDisplayPnrPaxCatFOIDPlace("");
			pax.setDisplayPnrPaxPlaceOfBirth("");
			// infantPaxTO.setDisplayTravelDocType("");
			pax.setDisplayVisaDocNumber("");
			pax.setDisplayVisaDocPlaceOfIssue("");
			pax.setDisplayVisaApplicableCountry("");
			pax.setDisplayVisaDocIssueDate("");
		}

		pax.setDisplayPaxSequence(Integer.toString(displayPaxSequence));
		pax.setDisplayOrdePaxSequence(Integer.toString(displayOrderPaxSequence));

		if (lccClientReservationPax.getParent() != null) {
			pax.setDisplayPaxTravellingWith(lccClientReservationPax.getParent().getPaxSequence().toString());
		} else {
			pax.setDisplayPaxTravellingWith("");
		}

		pax.setNameEditable(false);

		if (!rParam.isPostDepature()) {
			if (AppSysParamsUtil.applyExtendedNameModificationFunctionality()) {
				if ((nameModificationParams.isBeforeBufferTime && rParam.isAllowExtendedPassengerNameModification())
						|| (nameModificationParams.isWithinBufferTime
								&& rParam.isAllowExtendedPassengerNameModificationWithinBuffer())
						|| (!rParam.isInBufferTime())) {

					if (lccClientReservationPax.getFirstName()
							.equals(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER)
							&& lccClientReservationPax.getLastName()
									.equals(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER)) {
						if (rParam.isTbaNameChange()) {
							pax.setNameEditable(true);
						}
					} else if (rParam.isNameChange()) {
						if (!nameModificationParams.isCreditUtilizedBooking || (nameModificationParams.isCreditUtilizedBooking
								&& rParam.isAllowCreditUtilizedPassengerNameModification())) {
							if (!nameModificationParams.isFlownSegmentExists || (nameModificationParams.isFlownSegmentExists
									&& rParam.isAllowPassengerNameModificationHavingDepatedSegments())) {
								pax.setNameEditable(true);
							}
						}
					}
				}

			} else {
				if (!rParam.isInBufferTime()) {
					if (lccClientReservationPax.getFirstName()
							.equals(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER)
							&& lccClientReservationPax.getLastName()
									.equals(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER)) {
						if (rParam.isTbaNameChange()) {
							pax.setNameEditable(true);
						}
					} else if (rParam.isNameChange()) {
						pax.setNameEditable(true);
					}
				} else if (rParam.isInBufferTime() && rParam.isBufferTimeNameChange()) {
					pax.setNameEditable(true);
				}
			}
		} else {
			if (rParam.isAllowPassengerNameModificationHavingDepatedSegments()) {
				pax.setNameEditable(true);
			}
		}

		if (rParam.isCancelledReservation() && rParam.isModifyCnxResAllowed()) {
			pax.setNameEditable(true);
		}

		// If reservation is On Hold then name change allowed no
		// matter what.
		if (reservationStatus.equals(ReservationStatus.ON_HOLD) && rParam.isOnholdNameChange()) {
			pax.setNameEditable(true);
		} else if (reservationStatus.equals(ReservationStatus.ON_HOLD) && !rParam.isOnholdNameChange()
				&& ((lccClientReservationPax.getFirstName().equals(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER)
						&& lccClientReservationPax.getLastName()
								.equals(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER))
						&& !rParam.isTbaNameChange())) {
			pax.setNameEditable(false);
		}
		pax.setAlertAutoCancellation(lccClientReservationPax.isAlertAutoCancellation());

	}

	/**
	 * 
	 * @param reservation
	 * @param lccClientReservationSegment
	 * @return
	 */
	public static Collection<FlightInfoTO> loadFlightSegments(LCCClientReservation reservation, ReservationProcessParams rParam) {
		Collection<FlightInfoTO> collection = new ArrayList<FlightInfoTO>();

		Set<LCCClientReservationSegment> setReservationSegments = reservation.getSegments();

		SimpleDateFormat smpdtDate = new SimpleDateFormat("EEE dd/MM/yyyy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
		String strNextDay = "";
		long segModifyBuffStart = 0l;
		long segCancelBuffStart = 0l;
		long segBuffEnd = 0l;
		long sysTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		Map<String, LogicalCabinClassDTO> logicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
		Map<Integer, LCCClientReservationSegment> clientSegMap = createMapedLCCReservationSegments(setReservationSegments);
		Map<String, String> busAirCarrierCodes = null;
		try {
			busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();
		} catch (ModuleException e) {
			log.info(e);
		}
		if (setReservationSegments != null && setReservationSegments.size() > 0) {
			LCCClientReservationSegment[] resSegs = SortUtil.sort(setReservationSegments);
			int finalJourneyOnd = resSegs[resSegs.length - 1].getJourneySequence();
			for (LCCClientReservationSegment lccClientReservationSegment : resSegs) {
				strNextDay = "";
				segModifyBuffStart = 0l;
				segCancelBuffStart = 0l;
				segBuffEnd = 0l;
				String formatedArrivalDate = lccClientReservationSegment.getArrivalDate() != null
						? smpdtDate.format(lccClientReservationSegment.getArrivalDate())
						: "";
				String formatedArrivalTime = lccClientReservationSegment.getArrivalDate() != null
						? smpdtTime.format(lccClientReservationSegment.getArrivalDate())
						: "";

				if (!smpdtDate.format(lccClientReservationSegment.getDepartureDate()).equals(formatedArrivalDate)) {
					strNextDay = " (+1)";
				}

				FlightInfoTO flightInfoTO = new FlightInfoTO();
				flightInfoTO.setOrignNDest(lccClientReservationSegment.getSegmentCode());
				flightInfoTO.setFlightNo(lccClientReservationSegment.getFlightNo());
				flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(lccClientReservationSegment.getFlightNo()));
				if (busAirCarrierCodes != null && busAirCarrierCodes.get(lccClientReservationSegment.getCarrierCode()) != null) {
					flightInfoTO.setFilghtOperatingCarrier(busAirCarrierCodes.get(lccClientReservationSegment.getCarrierCode()));
				} else {
					flightInfoTO.setFilghtOperatingCarrier(
							AppSysParamsUtil.extractCarrierCode(lccClientReservationSegment.getFlightNo()));
				}
				flightInfoTO.setDepartureDate(smpdtDate.format(lccClientReservationSegment.getDepartureDate()));
				flightInfoTO.setDepartureDateLong(lccClientReservationSegment.getDepartureDate().getTime());
				flightInfoTO.setDepartureText("DEP");
				flightInfoTO.setDeparture("");
				flightInfoTO.setDepartureTime(smpdtTime.format(lccClientReservationSegment.getDepartureDate()));
				flightInfoTO.setArrivalText("ARR");
				flightInfoTO.setFlightRefNumber(lccClientReservationSegment.getFlightSegmentRefNumber());
				flightInfoTO.setArrivalDate(formatedArrivalDate);
				flightInfoTO.setArrival("");
				flightInfoTO.setArrivalTime(formatedArrivalTime + strNextDay);
				flightInfoTO.setArrivalDateLong(lccClientReservationSegment.getArrivalDate().getTime());
				flightInfoTO.setCsOcCarrierCode(lccClientReservationSegment.getCsOcCarrierCode());

				String fltDepartureTerminalName = lccClientReservationSegment.getDepartureTerminalName();
				String fltArrivalTerminalName = lccClientReservationSegment.getArrivalTerminalName();

				fltDepartureTerminalName = FlightStopOverDisplayUtil.formatNullOrEmpty(fltDepartureTerminalName);
				fltArrivalTerminalName = FlightStopOverDisplayUtil.formatNullOrEmpty(fltArrivalTerminalName);

				flightInfoTO.setDepartureTerminal(fltDepartureTerminalName);
				flightInfoTO.setArrivalTerminal(fltArrivalTerminalName);

				boolean skipCabinClassDisp = false;
				if (lccClientReservationSegment.getBundledServicePeriodId() != null) {
					String bundledFareName = getBundledFareName(reservation,
							lccClientReservationSegment.getBundledServicePeriodId());
					if (bundledFareName != null && !"".equals(bundledFareName)) {
						flightInfoTO.setCabinClass(bundledFareName);
						skipCabinClassDisp = true;
					}
				}

				if (!skipCabinClassDisp) {
					String fareTypeName = "";
					if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
						String logicalCC = lccClientReservationSegment.getLogicalCabinClass();
						fareTypeName = getFareTypeName(logicalCC, lccClientReservationSegment.getFlexiRuleID());
					} else {
						String cabinClass = lccClientReservationSegment.getCabinClassCode();
						fareTypeName = getFareTypeName(cabinClass, lccClientReservationSegment.getFlexiRuleID());
					}

					flightInfoTO.setBkgClass(fareTypeName);
					flightInfoTO.setCabinClass(fareTypeName);
				}

				flightInfoTO.setType(
						lccClientReservationSegment.getBookingType() == null ? "" : lccClientReservationSegment.getBookingType());
				flightInfoTO.setStatus(lccClientReservationSegment.getStatus());
				flightInfoTO.setSubStatus(lccClientReservationSegment.getSubStatus());
				flightInfoTO.setDisplayStatus(BeanUtils.nullHandler(lccClientReservationSegment.getDisplayStatus()));
				flightInfoTO.setFlightSegmentRefNumber(lccClientReservationSegment.getBookingFlightSegmentRefNumber());
				flightInfoTO.setInterLineGroupKey(lccClientReservationSegment.getInterlineGroupKey());
				flightInfoTO.setSurfaceStation(lccClientReservationSegment.getSubStationShortName());
				flightInfoTO.setPnrSegID(PlatformUtiltiies.nullHandler(lccClientReservationSegment.getPnrSegID()));

				String fltDuration = lccClientReservationSegment.getFlightDuration();
				String fltModelDescription = lccClientReservationSegment.getFlightModelDescription();
				String fltModelNo = lccClientReservationSegment.getFlightModelNumber();
				String fltRemarks = lccClientReservationSegment.getRemarks();

				fltDuration = FlightStopOverDisplayUtil.formatNullOrEmpty(fltDuration);
				fltModelDescription = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelDescription);
				fltModelNo = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelNo);
				fltRemarks = FlightStopOverDisplayUtil.formatNullOrEmpty(fltRemarks);

				flightInfoTO.setFlightDuration(fltDuration);
				flightInfoTO.setFlightModelDescription(fltModelDescription);
				flightInfoTO.setFlightModelNumber(fltModelNo);
				flightInfoTO.setRemarks(fltRemarks);

				String stopOverDuration = FlightStopOverDisplayUtil.getStopOverDuration(setReservationSegments,
						lccClientReservationSegment);

				stopOverDuration = FlightStopOverDisplayUtil.formatNullOrEmpty(stopOverDuration);

				flightInfoTO.setFlightStopOverDuration(stopOverDuration);

				flightInfoTO.setNoOfStops(
						FlightStopOverDisplayUtil.getStopOverStations(lccClientReservationSegment.getSegmentCode()));

				if (lccClientReservationSegment.getReturnFlag() != null
						&& lccClientReservationSegment.getReturnFlag().equalsIgnoreCase("Y")) {
					flightInfoTO.setReturnFlag(true);
				}

				flightInfoTO.setOpenReturnSegment(lccClientReservationSegment.isOpenReturnSegment());
				// Setting modifiable according to privilege
				segModifyBuffStart = lccClientReservationSegment.getModifyTillBufferDateTime().getTime();
				segCancelBuffStart = lccClientReservationSegment.getCancelTillBufferDateTime().getTime();
				segBuffEnd = lccClientReservationSegment.getModifyTillFlightClosureDateTime().getTime();
				if (!lccClientReservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
						&& rParam != null) {
					/* segment modification */
					if (sysTime > segModifyBuffStart) {
						if (sysTime < segBuffEnd) {
							if (rParam.isBuffertimeModification() || rParam.isFareRuleBufferTimeModification()) {
								flightInfoTO.setIsAllowModifyByDate(
										reservation.isModifiableReservation() && lccClientReservationSegment.isModifyByDate()
												&& lccClientReservationSegment.isModifible());
								flightInfoTO.setIsAllowModifyByRoute(
										reservation.isModifiableReservation() && lccClientReservationSegment.isModifyByRoute()
												&& lccClientReservationSegment.isModifible());
							}
						} else {
							// JIRA AARESAA-6580 Fix
							if ((rParam.isModifyFlown() || lccClientReservationSegment.isModifible())
									&& reservation.isModifiableReservation()) {
								flightInfoTO.setIsAllowModifyByDate(true);
								flightInfoTO.setIsAllowModifyByRoute(true);
							}
						}
					} else {
						if (rParam.getModifySegmentByDate()) {
							flightInfoTO.setIsAllowModifyByDate(
									lccClientReservationSegment.isModifyByDate() && reservation.isModifiableReservation());
						}
						if (rParam.getModifySegmentByRoute()) {
							flightInfoTO.setIsAllowModifyByRoute(
									lccClientReservationSegment.isModifyByRoute() && reservation.isModifiableReservation());
						}
					}

					if (sysTime > segCancelBuffStart) {
						if (sysTime < segBuffEnd) {
							if (rParam.isBuffertimeModification() && reservation.isModifiableReservation()) {
								flightInfoTO.setCancellable(true);
								lccClientReservationSegment.setCancellable(true);
							}
						} else {
							// JIRA AARESAA-6580 Fix
							if ((rParam.isModifyFlown() || lccClientReservationSegment.isModifible())
									&& reservation.isModifiableReservation()) {
								flightInfoTO.setCancellable(true);
								lccClientReservationSegment.setCancellable(true);
							}
						}
					} else {
						if ((reservation.isModifiableReservation() || rParam.isGdsReservationModAllowed())
								&& rParam.isCancelSegment()) {
							flightInfoTO.setCancellable(true);
							lccClientReservationSegment.setCancellable(true);
						}
					}

					if (finalJourneyOnd == lccClientReservationSegment.getJourneySequence()
							&& lccClientReservationSegment.getTicketValidTill() != null) {
						flightInfoTO.setTicketValidTill(
								BeanUtils.parseDateFormat(lccClientReservationSegment.getTicketValidTill(), "dd MMM yyyy"));
					}

				} else {
					if (rParam != null && rParam.isModifyCnxResAllowed() && reservation.isModifiableReservation()) {
						flightInfoTO.setCancellable(true);
						lccClientReservationSegment.setCancellable(true);
						flightInfoTO.setIsAllowModifyByDate(true);
						flightInfoTO.setIsAllowModifyByRoute(true);
					}
				}
				if (lccClientReservationSegment.isCancellable()) {
					lccClientReservationSegment.setCancellable(PNRModificationAuthorizationUtils
							.isSegmentModifiableAsPerETicketStatus(reservation, lccClientReservationSegment.getPnrSegID()));
				}

				if (rParam != null && rParam.isGroundServiceEnabled()) {
					if (lccClientReservationSegment.getSubStationShortName() == null) {
						boolean addGroundSegmentEnableForUser = isAddGroundSegmentEnableForUser(rParam.isBuffertimeModification(),
								sysTime, segModifyBuffStart, segBuffEnd);
						if (lccClientReservationSegment.getGroundStationPnrSegmentID() != null) {
							LCCClientReservationSegment actualGroundSeg = clientSegMap
									.get(lccClientReservationSegment.getGroundStationPnrSegmentID());
							if (actualGroundSeg != null && ReservationSegmentStatus.CANCEL.equals(actualGroundSeg.getStatus())
									&& addGroundSegmentEnableForUser) {
								flightInfoTO.setGroundStationAddAllowed(Boolean.TRUE);
							}
						} else if (addGroundSegmentEnableForUser) {
							flightInfoTO.setGroundStationAddAllowed(Boolean.TRUE);
						}
					}
				}
				if (lccClientReservationSegment.getStatus().equals(ReservationInternalConstants.ReservationType.WL.toString())) {
					// WL segment priority
					try {
						List<Integer> wlprs = new ArrayList<Integer>();
						List<ReservationWLPriority> wlPriorities = ModuleServiceLocator.getReservationBD()
								.getWaitListedPrioritiesForSegment(lccClientReservationSegment.getPnrSegID());
						for (ReservationWLPriority priority : wlPriorities) {
							if (priority.getPnr().equals(reservation.getPNR())) {
								flightInfoTO.setWaitListedPriority(priority.getPriority());
								flightInfoTO.setFlightSegId(priority.getFlightSegId());
							}

							wlprs.add(priority.getPriority());
						}
						Collections.sort(wlprs);
						flightInfoTO.setSegmentWaitListedPriorities(wlprs);

					} catch (Exception e) {
						if (log.isDebugEnabled()) {
							log.debug("Error while loading wait listed priorities");
						}
					}
				}
				collection.add(flightInfoTO);
			}
		}
		if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
			collection.addAll(loadOtherAirlineFlightSegments(reservation, rParam));
		}
		return sortByInterlineGroupKey(SortUtil.sortByDepatureDate(new ArrayList<FlightInfoTO>(collection)));
	}

	public static List<FlightSegmentTO> loadFlightSegmentToList(LCCClientReservation reservation) {
		List<FlightSegmentTO> flightSegmentToList = new ArrayList<FlightSegmentTO>();
		Set<LCCClientReservationSegment> resSegments = reservation.getSegments();

		for (LCCClientReservationSegment resSeg : resSegments) {

			String flightRPH = resSeg.getFlightSegmentRefNumber();
			String routeRPH = null;
			if (flightRPH != null && flightRPH.indexOf("#") != -1) {
				String[] flightRouteRPH = flightRPH.split("#");
				flightRPH = flightRouteRPH[0];
				routeRPH = flightRouteRPH[1];
			}
			String segCode = resSeg.getSegmentCode();
			String flightNumber = resSeg.getFlightNo();
			String operatingAirline = FlightRefNumberUtil.getOperatingAirline(flightRPH);

			boolean returnFlag = Boolean.parseBoolean(BeanUtils.nullHandler(resSeg.getReturnFlag()));
			boolean isDomestic = resSeg.isDomesticFlight();
			// boolean waitListed = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("waitListed")).toString());
			// String ondSequence = BeanUtils.nullHandler(jObj.get("ondSequence")).toString();
			// String subStationShortName = null;
			// if (AddModifySurfaceSegmentValidations.isMatchingSegmentCodeForFromSubStation(segCode,
			// searchParams.getFromAirportSubStation(), returnFlag)) {
			// subStationShortName = searchParams.getFromAirportSubStation();
			// } else if (AddModifySurfaceSegmentValidations.isMatchingSegmentCodeForToSubStation(segCode,
			// searchParams.getToAirportSubStation(), returnFlag)) {
			// subStationShortName = searchParams.getToAirportSubStation();
			// }
			FlightSegmentTO flightSegTO = new FlightSegmentTO();
			flightSegTO.setFlightRefNumber(flightRPH);
			flightSegTO.setRouteRefNumber(routeRPH);
			flightSegTO.setSegmentCode(segCode);
			flightSegTO.setArrivalDateTime(resSeg.getArrivalDate());
			flightSegTO.setDepartureDateTime(resSeg.getDepartureDate());
			flightSegTO.setArrivalDateTimeZulu(resSeg.getArrivalDateZulu());
			flightSegTO.setDepartureDateTimeZulu(resSeg.getDepartureDateZulu());
			flightSegTO.setOperatingAirline(operatingAirline);
			flightSegTO.setReturnFlag(returnFlag);
			flightSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRPH));
			// flightSegTO.setSubStationShortName(subStationShortName);
			flightSegTO.setDomesticFlight(isDomestic);
			// flightSegTO.setBookingType(searchParams.getBookingType());
			// flightSegTO.setWaitListed(waitListed);
			flightSegTO.setFlightNumber(flightNumber);

			flightSegmentToList.add(flightSegTO);

		}
		return flightSegmentToList;
	}

	private static String getFareTypeName(String logicalCabinClass, Integer flexiRuleId) {
		String fareTypeName = "";
		Map<String, LogicalCabinClassDTO> logicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();

		if (logicalCabinClass != null && !"".equals(logicalCabinClass)) {
			LogicalCabinClassDTO logicalCabinClassDTO = logicalCCMap.get(logicalCabinClass);

			if (flexiRuleId == null) {
				fareTypeName = logicalCabinClassDTO.getDescription();
			} else {
				fareTypeName = logicalCabinClassDTO.getFlexiDescription();
			}

		}

		return fareTypeName;
	}

	private static List<FlightInfoTO> sortByInterlineGroupKey(List<FlightInfoTO> flightInfoList) {
		LinkedList<FlightInfoTO> sortedFlights = new LinkedList<FlightInfoTO>();
		LinkedHashMap<String, LinkedList<FlightInfoTO>> flightInfoMap = new LinkedHashMap<String, LinkedList<FlightInfoTO>>();
		for (FlightInfoTO flightInfoTO : flightInfoList) {
			String groupKey = flightInfoTO.getInterLineGroupKey();
			if (!flightInfoMap.containsKey(groupKey)) {
				LinkedList<FlightInfoTO> flightInfo = new LinkedList<FlightInfoTO>();
				flightInfo.add(flightInfoTO);
				flightInfoMap.put(groupKey, flightInfo);
			} else {
				flightInfoMap.get(groupKey).add(flightInfoTO);
			}
		}

		for (String intlGroupKey : flightInfoMap.keySet()) {
			sortedFlights.addAll(flightInfoMap.get(intlGroupKey));
		}
		return sortedFlights;
	}

	public static List<LCCClientAlertInfoTO> getSegmentAlertInfo(Set<LCCClientReservationSegment> segments)
			throws ModuleException {

		List<LCCClientAlertInfoTO> segmentAlertList = new ArrayList<LCCClientAlertInfoTO>();

		for (LCCClientReservationSegment segDto : segments) {
			// show common service alert
			if (!segDto.getStatus().equals("CNX") && segDto.getSubStationShortName() != null
					&& !segDto.getSubStationShortName().trim().equals("")) {
				List<LCCClientAlertTO> groundserviceAlertList = new ArrayList<LCCClientAlertTO>();
				LCCClientAlertTO lcc = new LCCClientAlertTO();
				lcc.setAlertId(0);
				Collection<String> colAirPortCodes = new ArrayList<String>();
				colAirPortCodes.add(segDto.getSubStationShortName());
				Map<String, CachedAirportDTO> mapAirports = new HashMap<String, CachedAirportDTO>();
				if (colAirPortCodes.size() != mapAirports.size()) {
					mapAirports = ModuleServiceLocator.getAirportBD().getCachedAllAirportMap(colAirPortCodes);
				}
				String stationName = mapAirports.get(segDto.getSubStationShortName()) != null
						? mapAirports.get(segDto.getSubStationShortName()).getAirportName()
						: "";
				lcc.setContent(stationName);
				groundserviceAlertList.add(lcc);

				LCCClientAlertInfoTO alertInfo = new LCCClientAlertInfoTO();
				alertInfo.setAlertTO(groundserviceAlertList);
				alertInfo.setFlightSegmantRefNumber(segDto.getBookingFlightSegmentRefNumber().toString());
				alertInfo.setAlertCategory(LCCClientAlertInfoTO.alertType.GROUND_SERVICE);
				segmentAlertList.add(alertInfo);
			}
		}
		return segmentAlertList;
	}

	/**
	 * @param passengers
	 * @return
	 */
	public static Collection<LccClientPassengerEticketInfoTO>
			getPassengerETicketCoupons(Set<LCCClientReservationPax> passengers) {
		Collection<LccClientPassengerEticketInfoTO> eticketCouponColl = new ArrayList<LccClientPassengerEticketInfoTO>();

		for (LCCClientReservationPax lccPassenger : passengers) {
			eticketCouponColl.addAll(lccPassenger.geteTickets());
		}
		return eticketCouponColl;
	}

	/*
	 * 
	 * @param strCurrency
	 * 
	 * @param strTotalFare
	 * 
	 * @param strTotalTax
	 * 
	 * @param strTotalPrice
	 * 
	 * @return
	 */
	public static PaymentSummaryTO loadPaxPriceSummary(String strCurrency, String strTicketPrice, String strTotalFareDiscount,
			String goquoAmount) {
		PaymentSummaryTO paymentSummaryTO = new PaymentSummaryTO();

		paymentSummaryTO.setCurrency(strCurrency);
		paymentSummaryTO.setTicketPrice(strTicketPrice);
		paymentSummaryTO.setTotalDiscount(strTotalFareDiscount);
		paymentSummaryTO.setTotalGOQUOAmount(goquoAmount);

		return paymentSummaryTO;
	}

	/**
	 * 
	 * @param setClientReservationPaxs
	 * @param isInfantPaymentSeparated
	 * @return
	 */
	public static Object[] loadPaxPriceSummary(Set<LCCClientReservationPax> setClientReservationPaxs,
			boolean isInfantPaymentSeparated) {

		Collection<PaymentPassengerTO> collection = new ArrayList<PaymentPassengerTO>();
		BigDecimal totalToPay = BigDecimal.ZERO;
		if (setClientReservationPaxs != null) {

			for (LCCClientReservationPax resPax : SortUtil.sortPax(setClientReservationPaxs)) {
				if (isInfantPaymentSeparated) {

					PaymentPassengerTO payPax = new PaymentPassengerTO();
					int intPaxId = resPax.getPaxSequence().intValue();
					String strCh = resPax.getPaxType().equals(PaxTypeTO.CHILD) ? "CH. " : "";
					String strInf = resPax.getInfants() != null && resPax.getInfants().size() > 0 ? "/INF" : "";

					payPax.setDisplayPaxID(Integer.toString(intPaxId));
					payPax.setDisplayPassengerName(strCh + (isPaxTitleEmpty(resPax) ? "" : resPax.getTitle() + ". ")
							+ StringUtil.toInitCap(resPax.getFirstName()) + " " + StringUtil.toInitCap(resPax.getLastName())
							+ strInf);
					payPax.setDisplayFirstName(resPax.getFirstName());
					payPax.setDisplayLastName(resPax.getLastName());
					payPax.setDisplayPaid(AccelAeroCalculator.formatAsDecimal(resPax.getTotalPaidAmount()));
					payPax.setDisplayOriginalPaid(AccelAeroCalculator.formatAsDecimal(resPax.getTotalPaidAmount()));
					if (resPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {
						totalToPay = AccelAeroCalculator.add(totalToPay, resPax.getTotalAvailableBalance());
					}
					payPax.setDisplayToPay(AccelAeroCalculator.formatAsDecimal(resPax.getTotalAvailableBalance()));
					payPax.setDisplayOriginalTobePaid(AccelAeroCalculator.formatAsDecimal(resPax.getTotalAvailableBalance()));
					payPax.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(resPax.getTotalPrice()));
					payPax.setDisplayUsedCredit("0.00");
					payPax.setPaxSequence(resPax.getPaxSequence() + "");
					payPax.setPaxType(resPax.getPaxType());
					// payPax.setDisplayCreditButton("<input type='button' id='btnCredit"
					// + intPaxId +
					// "' name='btnCredit' value='Credit' class='ui-state-default ui-corner-all'
					// onclick='UI_tabPayment.btnCreditOnClick({row:"
					// + (intPaxId -1) + "})'>");
					// payPax.setDisplayRemoveButton("<input type='button' id='btnRemove"
					// + intPaxId +
					// "' name='btnRemove' value='Remove' class='ui-state-default ui-corner-all'
					// onclick='UI_tabPayment.btnRemoveOnClick({row:"
					// + (intPaxId -1) + "})'>");
					payPax.setPaxCreditInfo(new ArrayList<PaxUsedCreditInfoTO>());
					collection.add(payPax);

				} else {

					if (!resPax.getPaxType().equals(PaxTypeTO.INFANT)) {
						PaymentPassengerTO payPax = new PaymentPassengerTO();
						int intPaxId = resPax.getPaxSequence().intValue();
						String strCh = resPax.getPaxType().equals(PaxTypeTO.CHILD) ? "CH. " : "";
						String strInf = resPax.getInfants() != null && resPax.getInfants().size() > 0 ? "/INF" : "";

						payPax.setDisplayPaxID(Integer.toString(intPaxId));
						payPax.setDisplayPassengerName(strCh + (isPaxTitleEmpty(resPax) ? "" : resPax.getTitle() + ". ")
								+ StringUtil.toInitCap(resPax.getFirstName()) + " " + StringUtil.toInitCap(resPax.getLastName())
								+ strInf);
						payPax.setDisplayFirstName(resPax.getFirstName());
						payPax.setDisplayLastName(resPax.getLastName());
						payPax.setDisplayPaid(AccelAeroCalculator.formatAsDecimal(resPax.getTotalPaidAmount()));
						payPax.setDisplayOriginalPaid(AccelAeroCalculator.formatAsDecimal(resPax.getTotalPaidAmount()));
						if (resPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {
							totalToPay = AccelAeroCalculator.add(totalToPay, resPax.getTotalAvailableBalance());
						}
						payPax.setDisplayToPay(AccelAeroCalculator.formatAsDecimal(resPax.getTotalAvailableBalance()));
						payPax.setDisplayOriginalTobePaid(AccelAeroCalculator.formatAsDecimal(resPax.getTotalAvailableBalance()));
						payPax.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(resPax.getTotalPrice()));
						payPax.setDisplayUsedCredit("0.00");
						payPax.setPaxCreditInfo(new ArrayList<PaxUsedCreditInfoTO>());
						payPax.setPaxSequence(resPax.getPaxSequence() + "");
						payPax.setPaxType(resPax.getPaxType());
						collection.add(payPax);
					}

				}
			}
		}

		Object[] arrObj = new Object[2];
		arrObj[0] = collection;
		arrObj[1] = totalToPay;
		return arrObj;
	}

	private static boolean isPaxTitleEmpty(LCCClientReservationPax resPax) {
		return resPax.getTitle() == null || "".equals(resPax.getTitle());
	}

	/**
	 * 
	 * @param strCurrency
	 * @param strBalanceToPay
	 * @param strHandlingCharges
	 * @param strModificationCharges
	 * @param strPaid
	 * @param strTicketPrice
	 * @param strTotalFare
	 * @param strTotalFareDiscount
	 *            TODO
	 * @param totalGOQUOAmount
	 *            TODO
	 * @param strTotalTax
	 * @return
	 * @throws ParseException
	 */
	public static PaymentSummaryTO createPaymentSummary(String strCurrency, String strBalanceToPay, String strHandlingCharges,
			String strModificationCharges, String strPaid, String strTicketPrice, String strTotalFare, String strTotalTaxChagre,
			String selectedCurrency, String strTotalFareDiscount, String pendingCommissions, String totalGOQUOAmount,
			String strTotalServiceTax) throws ParseException {

		PaymentSummaryTO paymentSummaryTO = new PaymentSummaryTO();

		paymentSummaryTO.setBalanceToPay(strBalanceToPay);
		paymentSummaryTO.setCurrency(strCurrency);
		paymentSummaryTO.setHandlingCharges(strHandlingCharges);
		paymentSummaryTO.setModificationCharges(strModificationCharges);
		paymentSummaryTO.setPaid(strPaid);
		paymentSummaryTO.setTicketPrice(strTicketPrice);
		paymentSummaryTO.setTotalGOQUOAmount(totalGOQUOAmount);
		paymentSummaryTO.setTotalFare(strTotalFare);
		paymentSummaryTO.setTotalTaxSurCharges(strTotalTaxChagre);
		paymentSummaryTO.setSelectedCurrency(selectedCurrency);
		paymentSummaryTO.setTotalDiscount(strTotalFareDiscount);
		paymentSummaryTO.setTotalAgentCommission("0.00");
		paymentSummaryTO.setPendingAgentCommission(pendingCommissions);
		paymentSummaryTO.setTotalServiceTax(strTotalServiceTax);

		return paymentSummaryTO;
	}

	/**
	 * 
	 * @param dtDate
	 * @return
	 */
	private static String dateToStringFormat(Date dtDate) {
		String strDate = "";
		if (dtDate != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			strDate = simpleDateFormat.format(dtDate);
		}
		return strDate;
	}

	public static List<LCCClientReservationInsurance> getInsurance(List<LCCClientReservationInsurance> insurances)
			throws ModuleException {
		if (insurances != null && !insurances.isEmpty()) {
			Iterator<LCCClientReservationInsurance> insIterator = insurances.iterator();
			while (insIterator.hasNext()) {
				LCCClientReservationInsurance insurance = insIterator.next();
				String state = insurance.getState();
				if (state != null) {
					if (state.equals(ReservationInternalConstants.INSURANCE_STATES.SC.toString())) {
						insurance.setState("Success");
					} else if (state.equals(ReservationInternalConstants.INSURANCE_STATES.OH.toString())) {
						insurance.setState("On Hold");
					} else if (state.equals(ReservationInternalConstants.INSURANCE_STATES.FL.toString())) {
						insurance.setState("Failed");
					} else {
						insurance.setState("Processing");
					}
				}
			}
		}

		return insurances;
	}

	/**
	 * Return the valid list of modified segments <br />
	 * Following checks are performed
	 * <ol>
	 * <li>Modifying/Cancelling segments are not canceled.</li>
	 * <li>If Modifying/Cancelling segments are within modification buffer time, check whether user have buffer time
	 * modification privilege.</li>
	 * <li>If Modifying/Cancelling segments are already flown, check whether user have flown segment modification
	 * privilege</li>
	 * <li>If Operation is cancel segment check whether user has that privilege</li>
	 * <li>If Operation is modify segment check whether user has that privilege (modify segment by route/date)</li>
	 * </ol>
	 * 
	 * @param colResSegs
	 * @param segId
	 * @param rParam
	 * @param blnModify
	 * @return
	 */
	public static Collection<LCCClientReservationSegment> getModifyingSegments(Collection<LCCClientReservationSegment> colResSegs,
			String segId, ReservationProcessParams rParam, boolean blnModify) {
		Collection<LCCClientReservationSegment> colSegs = new ArrayList<LCCClientReservationSegment>();

		String[] selectedSegs = segId.split(",");
		long sysTime = new Date().getTime();
		boolean singleCarrier = isSingleCarrier(colResSegs);
		for (LCCClientReservationSegment resSeg : colResSegs) {
			for (int i = 0; i < selectedSegs.length; i++) {
				String[] selectedCNFSegs = selectedSegs[i].split("\\$");
				String strBookingRef = selectedCNFSegs[0];
				String strInterlingGroupKey = selectedCNFSegs[1];

				long segBuffStart = 0l;
				long segBuffEnd = 0l;

				if (resSeg.getBookingFlightSegmentRefNumber().equals(strBookingRef)
						&& (singleCarrier || resSeg.getInterlineGroupKey().equals(strInterlingGroupKey))) {
					// Setting modifiable according to privilege
					if (blnModify) {
						segBuffStart = resSeg.getModifyTillBufferDateTime().getTime();
					} else {
						segBuffStart = resSeg.getCancelTillBufferDateTime().getTime();
					}
					segBuffEnd = resSeg.getModifyTillFlightClosureDateTime().getTime();
					if (!resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
						if ((sysTime > segBuffStart) && !resSeg.isOpenReturnSegment()) {
							if (sysTime < segBuffEnd) {
								if (!rParam.isBuffertimeModification() && !rParam.isFareRuleBufferTimeModification()) {
									throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
								}
								// JIRA AARESAA-6580 Fix
							} else if (!rParam.isModifyFlown() && !resSeg.isModifible()) {
								throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
							}
						} else {
							if (blnModify) {
								if (!rParam.getModifySegmentByDate() && !rParam.getModifySegmentByRoute()) {
									// Fix me : above condition needs to be
									// validated seperately later
									throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
								}
							} else {
								if (!rParam.isCancelSegment()) {
									throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
								}
							}

						}
					}
					colSegs.add(resSeg);
					break;
				}
			}

		}
		return colSegs;

	}

	private static boolean isSingleCarrier(Collection<LCCClientReservationSegment> colResSegs) {
		String carrier = null;
		boolean singleCarrier = true;
		for (LCCClientReservationSegment resSeg : colResSegs) {
			if (carrier == null) {
				carrier = resSeg.getCarrierCode();
			} else if (!carrier.equals(resSeg.getCarrierCode())) {
				singleCarrier = false;
				break;
			}
		}
		return singleCarrier;
	}

	/**
	 * Transfer Segments
	 * 
	 * @param reservation
	 * @param segId
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Collection<LCCClientReservationSegment> getTransferSegments(Collection<LCCClientReservationSegment> colResSegs,
			String segId, boolean isInterline) throws ModuleException {
		List<LCCClientReservationSegment> transferSegments = null;
		Map<Integer, List<LCCClientReservationSegment>> ondFlightsMap = new HashMap<Integer, List<LCCClientReservationSegment>>();
		Map<String, List<LCCClientReservationSegment>> interlineSegMap = new HashMap<String, List<LCCClientReservationSegment>>();
		Map<Integer, Boolean> ondTransferMap = new HashMap<Integer, Boolean>();
		List tempList = null;
		String transferInterlineGroupKey = null;

		String[] selectedSegs = segId.split(",");
		Set<LCCClientReservationSegment> segmentsSet = new HashSet(colResSegs);
		StringBuffer bookingRefNumbers = new StringBuffer();
		for (int i = 0; i < selectedSegs.length; i++) {
			String[] selectedCNFSegs = selectedSegs[i].split("\\$");
			bookingRefNumbers.append(selectedCNFSegs[0]).append(",");
		}

		for (LCCClientReservationSegment resSeg : SortUtil.sort(segmentsSet)) {

			if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {

				// Own Airline Flights
				if (!isInterline && !AppSysParamsUtil.transferSementsOnFareOnd()) {
					if (!ondFlightsMap.containsKey(resSeg.getJourneySequence())) {
						ondFlightsMap.put(resSeg.getJourneySequence(), new ArrayList<LCCClientReservationSegment>());
					}

					ondFlightsMap.get(resSeg.getJourneySequence()).add(resSeg);
					if (bookingRefNumbers.toString().contains(resSeg.getBookingFlightSegmentRefNumber())) {
						ondTransferMap.put(resSeg.getJourneySequence(), true);
					}

				} else {
					if (interlineSegMap.containsKey(resSeg.getInterlineGroupKey())) {
						tempList = interlineSegMap.get(resSeg.getInterlineGroupKey());
						tempList.add(resSeg);
						interlineSegMap.put(resSeg.getInterlineGroupKey(), tempList);
					} else {
						tempList = new ArrayList<LCCClientReservationSegment>();
						tempList.add(resSeg);
						interlineSegMap.put(resSeg.getInterlineGroupKey(), tempList);
					}

					if (transferInterlineGroupKey == null
							&& bookingRefNumbers.toString().contains(resSeg.getBookingFlightSegmentRefNumber())) {
						transferInterlineGroupKey = resSeg.getInterlineGroupKey();
					}

				}
			}
		}

		if (isInterline || AppSysParamsUtil.transferSementsOnFareOnd()) {
			transferSegments = interlineSegMap.get(transferInterlineGroupKey);
		} else {
			for (Entry<Integer, Boolean> transferEntry : ondTransferMap.entrySet()) {
				Boolean isTransfer = transferEntry.getValue();
				if (isTransfer != null && isTransfer.booleanValue()) {
					transferSegments = ondFlightsMap.get(transferEntry.getKey());
				}
			}
		}

		if (transferSegments == null) {
			throw new ModuleException("airreservations.transfersegment.segment.notmatch");
		}

		return transferSegments;
	}

	public static Collection<LCCClientReservationSegment>
			getAlertTransferSegment(Collection<LCCClientReservationSegment> colResSegs, String alertPnrSegmentID) {
		List<LCCClientReservationSegment> transferSegments = new ArrayList<LCCClientReservationSegment>();
		if (colResSegs != null && alertPnrSegmentID != null) {
			for (LCCClientReservationSegment resSeg : colResSegs) {
				if (resSeg.getBookingFlightSegmentRefNumber() != null
						&& resSeg.getBookingFlightSegmentRefNumber().equals(alertPnrSegmentID)) {
					transferSegments.add(resSeg);
					break;
				}
			}
		}
		return transferSegments;
	}

	public static boolean isInterlineCarrierReservation(Collection<LCCClientReservationSegment> colSegments) {
		boolean isInterlineCarrierReservation = false;
		Set<String> carrierCodeSet = new HashSet<String>();
		// Interline Carrier codes
		String[] carrierList = AppSysParamsUtil.getAdjustmentCarrierList();
		if (colSegments != null) {
			for (LCCClientReservationSegment segment : colSegments) {
				carrierCodeSet.add(segment.getCarrierCode());
			}
		}

		if (carrierCodeSet.size() > 1 && carrierList != null) {
			int i = 0;
			for (String interlineCarrierCode : carrierList) {
				if (carrierCodeSet.contains(interlineCarrierCode)) {
					i++;
					if (i == 2) {
						isInterlineCarrierReservation = true;
						break;
					}
				}
			}
		}

		return isInterlineCarrierReservation;
	}

	public static Collection<LCCClientReservationSegment>
			getReprotctedTransferSegments(Collection<LCCClientReservationSegment> colResSegs, String segId) {
		Collection<LCCClientReservationSegment> colSegs = new ArrayList<LCCClientReservationSegment>();

		String[] selectedSegs = segId.split(",");
		for (LCCClientReservationSegment resSeg : colResSegs) {
			for (int i = 0; i < selectedSegs.length; i++) {
				String[] selectedCNFSegs = selectedSegs[i].split("\\$");
				String strBookingRef = selectedCNFSegs[0];
				String strInterlingGroupKey = selectedCNFSegs[1];

				if (resSeg.getBookingFlightSegmentRefNumber().equals(strBookingRef)
						&& resSeg.getInterlineGroupKey().equals(strInterlingGroupKey)) {
					colSegs.add(resSeg);
					break;
				}
			}

		}
		return colSegs;

	}

	/**
	 * Gets userNotes history collection
	 * 
	 * @param reservation
	 * @return
	 */
	public static Collection<UserNoteHistoryTO> getUserNoteHistory(List<UserNoteTO> userNotesHistoryList) {
		Collection<UserNoteHistoryTO> userNotesCol = new ArrayList<UserNoteHistoryTO>();
		for (UserNoteTO userNote : userNotesHistoryList) {
			UserNoteHistoryTO userNoteHistoryTO = new UserNoteHistoryTO();
			userNoteHistoryTO.setDisplayAction(userNote.getAction());
			if (userNote.getModifiedDate() != null) {
				userNoteHistoryTO.setDisplayModifyDate(DateFormatUtils.format(userNote.getModifiedDate(), "dd-MM-yyyy HH:mm"));
			}
			userNoteHistoryTO.setDisplayUserName(userNote.getUserName());
			userNoteHistoryTO.setDisplayUserNote(StringUtil.replaceNewLineForJSON(userNote.getUserText()));
			userNoteHistoryTO.setDisplayCarrierCode(userNote.getCarrierCode());
			userNotesCol.add(userNoteHistoryTO);
		}

		return userNotesCol;
	}

	/**
	 * Gets reservation history collection
	 * 
	 * @param reservation
	 * @return
	 */
	public static Collection<UserNoteHistoryTO> getReservationHistory(List<UserNoteTO> reservationHistoryList) {
		Collection<UserNoteHistoryTO> historyCol = new ArrayList<UserNoteHistoryTO>();
		for (UserNoteTO userNote : reservationHistoryList) {
			UserNoteHistoryTO historyTO = new UserNoteHistoryTO();
			historyTO.setDisplayAction(userNote.getAction());
			if (userNote.getModifiedDate() != null) {
				historyTO.setDisplayModifyDate(DateFormatUtils.format(userNote.getModifiedDate(), "dd-MM-yyyy HH:mm"));
			}
			historyTO.setDisplaySystemNote(userNote.getSystemNote());
			historyTO.setDisplayUserName(userNote.getUserName());
			historyTO.setDisplayUserNote(userNote.getUserText());
			historyTO.setDisplayCarrierCode(userNote.getCarrierCode());
			historyCol.add(historyTO);
		}
		return historyCol;
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, String preferredLanguage,
			HttpServletRequest request, boolean recordAudit, String marketingAirlineCode, String airlineCode,
			Long interlineAgreementId, boolean skipPromoAdjustment) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
		} else {
			pnrModesDTO.setPnr(pnr);
		}

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadLocalTimes(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true); // baggage
		pnrModesDTO.setLoadAutoCheckinInfo(true);// Auto Checkin
		pnrModesDTO.setAppIndicator(ApplicationEngine.XBE);
		pnrModesDTO.setPreferredLanguage(preferredLanguage);
		pnrModesDTO.setLoadGOQUOAmounts(true);
		if (request != null) {
			pnrModesDTO.setMapPrivilegeIds((Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS));
		}
		pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
		pnrModesDTO.setInterlineAgreementId(interlineAgreementId);
		pnrModesDTO.setAirlineCode(airlineCode);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setSkipPromoAdjustment(skipPromoAdjustment);

		if (request != null) {
			pnrModesDTO.setLoadRefundableTaxInfo(
					BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_NOSHOW_REFUNDABLE_TAX_CALC));
			pnrModesDTO.setLoadClassifyUN(BasicRH.hasPrivilege(request, PriviledgeConstants.CLASSIFY_USER_NOTE));
		}
		return pnrModesDTO;

	}

	public static LCCClientReservation loadFullLccReservation(String pnr, boolean isGroupPNR, String preferredLanguage,
			BookingShoppingCart bookingShopingCart, HttpServletRequest request, boolean recordAudit, String marketingAirlineCode,
			String airlineCode, Long interlineAgreementId, boolean skipPromoAdjustment, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		// REQUEST OBJECT
		LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, isGroupPNR, preferredLanguage, request, recordAudit,
				marketingAirlineCode, airlineCode, interlineAgreementId, skipPromoAdjustment);
		pnrModesDTO.setLoadRefundableTaxInfo(true);

		// String clientIp = request.getHeader("X-Forwarded-For");
		// if (clientIp == null || clientIp.equals("")){
		// clientIp = request.getRemoteAddr();
		// }
		// TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		// trackInfoDTO.setIpAddress(clientIp);
		// trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
		paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_XBE);

		if (log.isDebugEnabled())
			log.debug(" Search reservation initiated for loadFullLccReservation. " + pnrModesDTO.toString());
		LCCClientReservation reservation = ModuleServiceLocator.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO,
				paramRQInfo, trackInfoDTO);
		if (bookingShopingCart != null) {
			bookingShopingCart.setUsedPaxCredit(BigDecimal.ZERO);
			request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShopingCart);
		}
		return reservation;

	}

	public static int getWebBookingChannel(String bookingChannel) {
		String originBokingChannel = "";
		StringTokenizer st = new StringTokenizer(bookingChannel, ",");
		while (st.hasMoreTokens()) {
			originBokingChannel = st.nextToken();
			if (originBokingChannel.indexOf("|") > -1) {
				originBokingChannel = originBokingChannel.substring(originBokingChannel.indexOf("|") + 1);
				break;
			}

		}
		if (originBokingChannel != null && !originBokingChannel.equals("")) {
			return Integer.valueOf(originBokingChannel);
		}

		return 0;
	}

	public static String[] getFirstLastFlightDepartureDates(Set<LCCClientReservationSegment> setReservationSegments) {
		String departureDates[] = new String[3];
		LCCClientReservationSegment[] segArray = SortUtil.sort(setReservationSegments);
		LCCClientReservationSegment firstSeg = null;
		LCCClientReservationSegment lastSeg = null;
		if (segArray != null && segArray.length != 0) {
			int i = 0;
			for (LCCClientReservationSegment lCCClientReservationSegment : segArray) {
				if (lCCClientReservationSegment != null && ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
						.equals(lCCClientReservationSegment.getStatus())) {
					if (i == 0) {
						firstSeg = lCCClientReservationSegment;
					} else {
						lastSeg = lCCClientReservationSegment;
					}
					i++;
				}
			}
		}
		if (firstSeg == null) {
			departureDates[0] = DateUtil.formatDate(new Date(), "dd/MM/yyyy");
			departureDates[2] = DateUtil.formatDate(new Date(), "dd-MM-yyyy HH:mm:ss");
		} else {
			departureDates[0] = DateUtil.formatDate(firstSeg.getDepartureDate(), "dd/MM/yyyy");
			departureDates[2] = DateUtil.formatDate(firstSeg.getDepartureDate(), "dd-MM-yyyy HH:mm:ss");
		}

		if (lastSeg == null) {
			lastSeg = firstSeg;
		}

		if (lastSeg == null) {
			departureDates[1] = DateUtil.formatDate(new Date(), "dd/MM/yyyy");
		} else {
			departureDates[1] = DateUtil.formatDate(lastSeg.getDepartureDate(), "dd/MM/yyyy");
		}

		return departureDates;
	}

	public static String getFlightLastArrivalDate(Set<LCCClientReservationSegment> setReservationSegments) {
		LCCClientReservationSegment[] segArray = SortUtil.sort(setReservationSegments);
		LCCClientReservationSegment seg = segArray[segArray.length - 1];
		return DateUtil.formatDate(seg.getDepartureDate(), "dd/MM/yyyy");
	}

	public static LCCClientReservationSegment getFirstFlightSeg(Set<LCCClientReservationSegment> setReservationSegments) {
		LCCClientReservationSegment[] segArray = SortUtil.sort(setReservationSegments);
		return segArray[0];
	}

	public static FlightSegmentTO getFirstFlightSeg(List<FlightSegmentTO> flightSegmentTOs) {
		FlightSegmentTO[] sortedArray = SortUtil.sortFlightSegmentTOs(flightSegmentTOs);
		return sortedArray[0];
	}

	public static Collection<AncillaryPaxTO> createPaxWiseAnci(Set<LCCClientReservationPax> passengers, boolean goquoValid) {
		List<AncillaryPaxTO> paxWiseAnci = new ArrayList<AncillaryPaxTO>();
		for (LCCClientReservationPax pax : passengers) {
			AncillaryPaxTO anciPax = new AncillaryPaxTO();
			anciPax.setFirstName(pax.getFirstName());
			anciPax.setLastName(pax.getLastName());
			anciPax.setDateOfBirth(pax.getDateOfBirth());
			anciPax.setNationalityCode(pax.getNationalityCode());
			anciPax.setPaxType(pax.getPaxType());
			anciPax.setTitle(pax.getTitle());
			anciPax.setPaxSequence(pax.getPaxSequence());
			anciPax.setSelectedAncillaries(filterUserDefinedSSR(pax.getSelectedAncillaries(), goquoValid));

			Set<LCCClientReservationPax> infants = pax.getInfants();
			if (infants != null && !infants.isEmpty()) {
				anciPax.setIsParent(true);
				setInfantWith(passengers, anciPax, infants);
			}

			paxWiseAnci.add(anciPax);

			// if (infants != null && !infants.isEmpty()) {
			// paxWiseAnci.addAll(createPaxWiseAnciForInfants(infants, goquoValid));
			// }
		}
		if (paxWiseAnci.size() > 0) {
			Collections.sort(paxWiseAnci);
		}
		return paxWiseAnci;
	}

	// private static Collection<AncillaryPaxTO> createPaxWiseAnciForInfants(Set<LCCClientReservationPax> infants,
	// boolean goquoValid) {
	// List<AncillaryPaxTO> paxWiseAnci = new ArrayList<AncillaryPaxTO>();
	// for (LCCClientReservationPax infant : infants) {
	// AncillaryPaxTO anciPax = new AncillaryPaxTO();
	// anciPax.setFirstName(infant.getFirstName());
	// anciPax.setLastName(infant.getLastName());
	// anciPax.setDateOfBirth(infant.getDateOfBirth());
	// anciPax.setNationalityCode(infant.getNationalityCode());
	// anciPax.setPaxType(infant.getPaxType());
	// anciPax.setTitle(infant.getTitle());
	// anciPax.setPaxSequence(infant.getPaxSequence());
	// anciPax.setSelectedAncillaries(filterUserDefinedSSR(infant.getSelectedAncillaries(), goquoValid));
	//
	// paxWiseAnci.add(anciPax);
	// }
	// if (paxWiseAnci.size() > 0) {
	// Collections.sort(paxWiseAnci);
	// }
	// return paxWiseAnci;
	// }

	private static List<LCCSelectedSegmentAncillaryDTO>
			filterUserDefinedSSR(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries, boolean goquoValid) {
		if (selectedAncillaries != null && !selectedAncillaries.isEmpty()) {
			for (LCCSelectedSegmentAncillaryDTO anci : selectedAncillaries) {
				for (LCCSpecialServiceRequestDTO ssr : anci.getSpecialServiceRequestDTOs()) {
					if (ssr.isUserDefinedCharge() && !goquoValid) {
						ssr.setShowSsrValue(false);
					}
				}
			}
		}
		return selectedAncillaries;
	}

	private static void setInfantWith(Set<LCCClientReservationPax> passengers, AncillaryPaxTO anciPax,
			Set<LCCClientReservationPax> infants) {
		int adultChildCount = getAdultChildCount(passengers);
		LCCClientReservationPax[] infantArray = infants.toArray(new LCCClientReservationPax[0]);
		for (int i = 0; i < infantArray.length; i++) {
			int infantSequence = infantArray[i].getPaxSequence();
			// since one infant for one adult
			anciPax.setInfantWith(infantSequence - adultChildCount);
		}
	}

	private static int getAdultChildCount(Set<LCCClientReservationPax> passengers) {
		int adultChildCount = 0;
		for (LCCClientReservationPax pax : passengers) {
			if (PaxTypeTO.ADULT.equals(pax.getPaxType()) || PaxTypeTO.CHILD.equals(pax.getPaxType())) {
				adultChildCount++;
			}
		}
		return adultChildCount;
	}

	/**
	 * *
	 * 
	 * @param lCCClientAlertInfoTO
	 * @return List<LCCClientAlertInfoTO>
	 */
	public static List<LCCClientAlertInfoTO> getSortedAlertInfo(List<LCCClientAlertInfoTO> lCCClientAlertInfoTO) {
		for (LCCClientAlertInfoTO lCCClientAlertInfo : lCCClientAlertInfoTO) {
			lCCClientAlertInfo.setAlertTO(SortUtil.sort(lCCClientAlertInfo.getAlertTO()));
		}
		return lCCClientAlertInfoTO;
	}

	/**
	 * @param pnrSegmentRefNumber
	 * @param lCCClientAlertInfoTOList
	 * @return String (Alert Content)
	 */
	public static String getSegmentAlertContent(String pnrSegmentRefNumber, List<LCCClientAlertInfoTO> lCCClientAlertInfoTOList) {
		StringBuffer alertContentBuffer = new StringBuffer();
		for (LCCClientAlertInfoTO lCCClientAlertInfoTO : lCCClientAlertInfoTOList) {
			if (lCCClientAlertInfoTO.getFlightSegmantRefNumber().equals(pnrSegmentRefNumber)) {
				List<LCCClientAlertTO> alertList = SortUtil.sort(lCCClientAlertInfoTO.getAlertTO());
				for (LCCClientAlertTO alertTO : alertList) {
					alertContentBuffer.append(alertTO.getContent());
					alertContentBuffer.append("<br/>");
				}
				break;
			}
		}

		return alertContentBuffer.toString();
	}

	public static FareTypeInfoTO buildFareTypeInfo(FareTypeTO fareTypeTO, Map<String, Long> segmentDepartureTimeMap,
			BigDecimal handlingCharge, FlightSearchDTO searchParams, List<String> ondCodeList,
			List<Set<String>> selectedOndSegCodes, ReservationProcessParams rParm, BigDecimal fareDiscountAmount,
			ExchangeRateProxy exchangeRateProxy, FareSegChargeTO fareSegChargeTO, List<FlightSegmentTO> flightSegmentTOs,
			BaseAvailRQ baseAvailRQ, TrackInfoDTO trackInfo) throws ParseException, ModuleException {
		FareRulesInformationDTO fareRulesInfo = ReservationUtil.getFareRulesInformationDTO(fareTypeTO, segmentDepartureTimeMap,
				searchParams.getSelectedCurrency(), exchangeRateProxy);
		// Fare quote Details for Search Tab
		// boolean isReturnSearch =
		// ((!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(searchParams.getReturnDate())
		// || searchParams
		// .getOpenReturn()));

		List<FareQuoteTO> fareQuoteTO = new ArrayList<FareQuoteTO>();
		List<PaxFareTO> paxFareTOs = new ArrayList<PaxFareTO>();
		List<PaxPriceTO> paxPriceTOs = new ArrayList<PaxPriceTO>();
		Collection<SurchargeTO> surcharges = new ArrayList<SurchargeTO>();
		Collection<TaxTO> taxes = new ArrayList<TaxTO>();
		List<Collection<ExternalChargeTO>> ondExternalCharges = new ArrayList<Collection<ExternalChargeTO>>();

		// fill fare details
		ReservationBeanUtil.fillFareQuote(fareQuoteTO, paxFareTOs, paxPriceTOs, surcharges, taxes, ondExternalCharges, fareTypeTO,
				ondCodeList, selectedOndSegCodes, searchParams.getSelectedCurrency(), exchangeRateProxy);

		// Setting it here for future use

		// selected currency is changed.
		String strCurrnecy = AppSysParamsUtil.getBaseCurrency();

		boolean inBufferTime = false;
		inBufferTime = ReservationBeanUtil.calculateBufferTime(fareTypeTO);
		if (inBufferTime) {
			if (!rParm.isBufferTimeOnHold()) {
				fareTypeTO.setOnHoldRestricted(true);
			}
		}

		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS;
		BigDecimal totalSericeTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		String originCode = getFirstFlightSeg(flightSegmentTOs).getSegmentCode().split("\\/")[0];
		if (isServiceTaxApplicable(originCode, baseAvailRQ)) {

			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
			BigDecimal adultTotalSericeTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal childTotalSericeTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantTotalSericeTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
			serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
			serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(fareSegChargeTO);
			serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
			serviceTaxQuoteCriteriaDTO.setPaxState(null);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(null);
			serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(false);

			serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(baseAvailRQ.getTransactionIdentifier());

			for (PerPaxPriceInfoTO paxPriceInfoTO : fareTypeTO.getPerPaxPriceInfo()) {
				String trvelerRefNo = paxPriceInfoTO.getTravelerRefNumber().substring(0, 1);
				paxWisePaxTypes.put(Integer.valueOf(trvelerRefNo), paxPriceInfoTO.getPassengerType());
				// paxWiseExternalCharges.put(Integer.valueOf(paxPriceInfoTO.getTravelerRefNumber()), new
				// ArrayList<LCCClientExternalChgDTO>());
			}

			serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
			serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);

			serviceTaxQuoteRS = ModuleServiceLocator.getAirproxyReservationBD()
					.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfo);

			for (ServiceTaxQuoteForTicketingRevenueResTO ticketingRevenueResTO : serviceTaxQuoteRS.values()) {

				if (ticketingRevenueResTO.getPaxTypeWiseServiceTaxes() != null) {
					if (ticketingRevenueResTO.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.ADULT) != null) {
						for (ServiceTaxTO serviceTaxTO : ticketingRevenueResTO.getPaxTypeWiseServiceTaxes()
								.get(PaxTypeTO.ADULT)) {
							taxes.add(ReservationBeanUtil.convertToTaxTO(serviceTaxTO, PaxTypeTO.ADULT));
							adultTotalSericeTaxAmount = AccelAeroCalculator.add(adultTotalSericeTaxAmount,
									serviceTaxTO.getAmount());
						}
					}

					if (ticketingRevenueResTO.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.CHILD) != null) {
						for (ServiceTaxTO serviceTaxTO : ticketingRevenueResTO.getPaxTypeWiseServiceTaxes()
								.get(PaxTypeTO.CHILD)) {
							taxes.add(ReservationBeanUtil.convertToTaxTO(serviceTaxTO, PaxTypeTO.CHILD));
							childTotalSericeTaxAmount = AccelAeroCalculator.add(childTotalSericeTaxAmount,
									serviceTaxTO.getAmount());
						}
					}

					if (ticketingRevenueResTO.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT) != null) {
						for (ServiceTaxTO serviceTaxTO : ticketingRevenueResTO.getPaxTypeWiseServiceTaxes()
								.get(PaxTypeTO.INFANT)) {
							taxes.add(ReservationBeanUtil.convertToTaxTO(serviceTaxTO, PaxTypeTO.INFANT));
							infantTotalSericeTaxAmount = AccelAeroCalculator.add(infantTotalSericeTaxAmount,
									serviceTaxTO.getAmount());
						}
					}

					totalSericeTaxAmount = updateFareQuoteTOs(fareQuoteTO, ticketingRevenueResTO, exchangeRateProxy,
							searchParams.getSelectedCurrency());

				}

			}

			updatePaxFareTOs(paxFareTOs, adultTotalSericeTaxAmount, PaxTypeTO.PAX_TYPE_ADULT_DISPLAY, exchangeRateProxy,
					searchParams.getSelectedCurrency());
			updatePaxFareTOs(paxFareTOs, childTotalSericeTaxAmount, PaxTypeTO.PAX_TYPE_CHILD_DISPLAY, exchangeRateProxy,
					searchParams.getSelectedCurrency());
			updatePaxFareTOs(paxFareTOs, infantTotalSericeTaxAmount, PaxTypeTO.PAX_TYPE_INFANT_DISPLAY, exchangeRateProxy,
					searchParams.getSelectedCurrency());

			updatePaxPriceTOs(paxPriceTOs, adultTotalSericeTaxAmount, PaxTypeTO.ADULT, exchangeRateProxy,
					searchParams.getSelectedCurrency());
			updatePaxPriceTOs(paxPriceTOs, childTotalSericeTaxAmount, PaxTypeTO.CHILD, exchangeRateProxy,
					searchParams.getSelectedCurrency());
			updatePaxPriceTOs(paxPriceTOs, infantTotalSericeTaxAmount, PaxTypeTO.INFANT, exchangeRateProxy,
					searchParams.getSelectedCurrency());

		}

		FareQuoteSummaryTO fareQuoteSummaryTO = ReservationBeanUtil.createFareQuoteSummary(fareTypeTO,
				fareTypeTO.isOnHoldRestricted(), strCurrnecy, searchParams.getSelectedCurrency(), fareQuoteTO, handlingCharge,
				fareDiscountAmount);

		if (!totalSericeTaxAmount.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
			updateFareQuoteSummaryTO(fareQuoteSummaryTO, totalSericeTaxAmount, exchangeRateProxy,
					searchParams.getSelectedCurrency());
		}

		// Passenger Tab Informations
		if (searchParams.getAdultCount() == null)
			searchParams.setAdultCount(0);
		if (searchParams.getChildCount() == null)
			searchParams.setChildCount(0);
		if (searchParams.getInfantCount() == null)
			searchParams.setInfantCount(0);
		Collection<AdultPaxTO> paxAdults = PassengerUtil.createAdultList(searchParams.getAdultCount(),
				searchParams.getChildCount(), paxPriceTOs);
		List<InfantPaxTO> paxInfants = PassengerUtil.createInfantList(searchParams.getInfantCount());

		Collection<SurchargeFETO> surchargeTOs = ReservationBeanUtil.convertToSurchargeFETO(surcharges,
				searchParams.getSelectedCurrency(), exchangeRateProxy);
		Collection<TaxFETO> taxTOs = ReservationBeanUtil.convertToTaxFETO(taxes, searchParams.getSelectedCurrency(),
				exchangeRateProxy);

		Collection<SurchargeFETO> externalChargeTOs = new ArrayList<SurchargeFETO>();
		Collection<ExternalChargeTO> flexiCharges = new ArrayList<ExternalChargeTO>();
		for (Collection<ExternalChargeTO> ondExtObj : ondExternalCharges) {
			externalChargeTOs.addAll(ReservationBeanUtil.convertExternalChargeTOToSurchargeFETO(ondExtObj,
					searchParams.getSelectedCurrency(), exchangeRateProxy));
			flexiCharges.addAll(ondExtObj);
		}

		FlexiFareRulesInformationDTO flexiFareRulesInfo = ReservationUtil.getFlexiFareRulesInformationDTO(flexiCharges,
				fareQuoteTO, segmentDepartureTimeMap, searchParams.getSelectedCurrency(), exchangeRateProxy,
				getSegmentwiseTotalPrices(fareTypeTO));

		// Collection<FareCategoryTO> colFareCategoryTO =
		// globalConfig.getFareCategories().values();
		boolean showFareRules = false;
		GlobalConfig globalConfig = new GlobalConfig();
		Map<String, FareCategoryTO> sortedFareCategoryTOMap = globalConfig.getFareCategories();
		List<FareRuleDTO> fareRuleDTOs = fareRulesInfo.getFareRules();
		for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
			FareCategoryTO fareCategoryTO = sortedFareCategoryTOMap.get(fareRuleDTO.getFareCategoryCode());

			if (fareCategoryTO != null && fareCategoryTO.getStatus().equals(FareCategoryStatusCodes.ACT)) {
				showFareRules = fareCategoryTO.getShowComments();
				break;
			}
		}

		FareTypeInfoTO fareTypeInfo = new FareTypeInfoTO();

		fareTypeInfo.setShowFareRules(showFareRules);
		fareTypeInfo.setFlexiFareRulesInfo(flexiFareRulesInfo);

		fareTypeInfo.setSurchargeTOs(surchargeTOs);
		fareTypeInfo.setTaxTOs(taxTOs);

		fareTypeInfo.setFareQuoteSummaryTO(fareQuoteSummaryTO);
		fareTypeInfo.setFareQuoteTO(fareQuoteTO);
		fareTypeInfo.setFareRulesInfo(fareRulesInfo);

		fareTypeInfo.setPaxAdults(paxAdults);
		fareTypeInfo.setPaxInfants(paxInfants);

		fareTypeInfo.setPaxFareTOs(paxFareTOs);
		fareTypeInfo.setPaxPriceTOs(paxPriceTOs);
		fareTypeInfo.setFareDiscountInfo(fareTypeTO.getFareDiscountInfo());

		fareTypeInfo.setApplicableAgentCommissions(fareTypeTO.getApplicableAgentCommissions());
		if (fareTypeTO.getTotAgentCommission() != null) {
			fareTypeInfo.setTotAgentCommission(AccelAeroCalculator.formatAsDecimal(fareTypeTO.getTotAgentCommission()));
		}

		return fareTypeInfo;
	}

	public static FareTypeInfoTO buildFareTypeInfo(FareTypeTO fareTypeTO, List<OriginDestinationInformationTO> ondList,
			BigDecimal handlingCharge, FlightSearchDTO searchParams, ReservationProcessParams rParm,
			BigDecimal fareDiscountAmount, ExchangeRateProxy exchangeRateProxy, FareSegChargeTO fareSegChargeTO,
			BaseAvailRQ baseAvailRQ, TrackInfoDTO trackInfo, Map<String, String> selectedSegMap)
			throws ParseException, ModuleException {

		int ondSequence = 0;
		List<String> selectedOndCodes = new ArrayList<String>();
		List<Set<String>> selectedSegCodes = new ArrayList<Set<String>>();
		Map<String, Long> segmentDeptTimeMap = new HashMap<String, Long>();
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
		FlightSegmentTO aulteredFlightSegmentTO;

		for (OriginDestinationInformationTO ondInfo : ondList) {
			for (OriginDestinationOptionTO ondOpt : ondInfo.getOrignDestinationOptions()) {
				if (ondOpt.isSelected()) {
					Set<String> selectedONDSegCodes = new HashSet<String>();
					for (FlightSegmentTO flightSegmentTO : ondOpt.getFlightSegmentList()) {
						aulteredFlightSegmentTO = flightSegmentTO.clone();
						aulteredFlightSegmentTO.setOndSequence(ondSequence);
						flightSegments.add(aulteredFlightSegmentTO);
						if (selectedSegMap != null && selectedSegMap.get(flightSegmentTO.getSegmentCode()) != null) {
							selectedONDSegCodes.add(flightSegmentTO.getSegmentCode());
							segmentDeptTimeMap.put(flightSegmentTO.getSegmentCode(),
									flightSegmentTO.getDepartureDateTime().getTime());
						} else {
							selectedONDSegCodes.add(flightSegmentTO.getSegmentCode());
							segmentDeptTimeMap.put(flightSegmentTO.getSegmentCode(),
									flightSegmentTO.getDepartureDateTime().getTime());
						}
					}

					selectedOndCodes.add(ondSequence, ondOpt.getOndCode());
					selectedSegCodes.add(ondSequence, selectedONDSegCodes);
					break;
				}
			}
			if (selectedOndCodes.size() - 1 != ondSequence) {
				selectedOndCodes.add(ondSequence, null);
				selectedSegCodes.add(ondSequence, new HashSet<String>());
			}
			ondSequence++;
		}

		return buildFareTypeInfo(fareTypeTO, segmentDeptTimeMap, handlingCharge, searchParams, selectedOndCodes, selectedSegCodes,
				rParm, fareDiscountAmount, exchangeRateProxy, fareSegChargeTO, flightSegments, baseAvailRQ, trackInfo);
	}

	private static FareRulesInformationDTO getFareRulesInformationDTO(FareTypeTO fareTypeTO,
			Map<String, Long> segmentDepartureTimeMap, String selectedCurrency, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {
		FareRulesInformationDTO fareInfoDTO = new FareRulesInformationDTO();
		if (fareTypeTO != null) {
			List<FareRuleDTO> fareRuleDTOs = com.isa.thinair.webplatform.api.util.ReservationUtil
					.getMergedFareRuleDTOs(fareTypeTO.getApplicableFareRules());
			com.isa.thinair.webplatform.api.util.ReservationUtil.injectDepartureDateToFareRules(fareRuleDTOs,
					segmentDepartureTimeMap);
			Set<String> paxTypes = new HashSet<String>();
			for (BaseFareTO baseFareTO : fareTypeTO.getBaseFares()) {
				paxTypes.addAll(baseFareTO.getApplicablePassengerTypeCode());
			}

			if (paxTypes.contains("AD")) {
				fareInfoDTO.setAdultApplicable(true);
			}

			if (paxTypes.contains("CH")) {
				fareInfoDTO.setChildApplicable(true);
			}

			if (paxTypes.contains("IN") || paxTypes.contains("INF")) {
				fareInfoDTO.setInfantApplicable(true);
			}

			for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
				if (fareRuleDTO.getComments() != null) {
					fareInfoDTO.setHasComment(true);
					break;
				}
			}

			for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
				if (fareRuleDTO.getAgentInstructions() != null) {
					fareInfoDTO.setHasAgentInstructions(true);
					break;
				}
			}

			if (!AppSysParamsUtil.getBaseCurrency().equals(selectedCurrency)) {
				CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);
				Currency currency = currencyExchangeRate.getCurrency();
				for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
					double adultFare = new Double(AccelAeroCalculator.formatAsDecimal(
							AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
									fareRuleDTO.getAdultFareAmount(), currency.getBoundryValue(), currency.getBreakPoint())))
											.doubleValue();
					double childFare = new Double(AccelAeroCalculator.formatAsDecimal(
							AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
									fareRuleDTO.getChildFareAmount(), currency.getBoundryValue(), currency.getBreakPoint())))
											.doubleValue();
					double infantFare = new Double(AccelAeroCalculator.formatAsDecimal(
							AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
									fareRuleDTO.getInfantFareAmount(), currency.getBoundryValue(), currency.getBreakPoint())))
											.doubleValue();
					DepAPFareDTO selCurFareDTO = new DepAPFareDTO();
					selCurFareDTO.setAdultFare(adultFare);
					selCurFareDTO.setChildFare(childFare);
					selCurFareDTO.setInfantFare(infantFare);
					fareRuleDTO.setSelectedCurrFareDTO(selCurFareDTO);
				}
			}

			fareInfoDTO.setFareRules(fareRuleDTOs);
		}
		return fareInfoDTO;
	}

	private static FlexiFareRulesInformationDTO getFlexiFareRulesInformationDTO(Collection<ExternalChargeTO> flexiCharges,
			List<FareQuoteTO> fareQuoteTO, Map<String, Long> segmentDepartureTimeMap, String selectedCurrency,
			ExchangeRateProxy exchangeRateProxy, Map<String, BigDecimal> segmentTotalMap) throws ModuleException {

		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);
		Currency currency = currencyExchangeRate.getCurrency();

		Map<String, FareRuleDTO> fareRuleDTOMap = new LinkedHashMap<String, FareRuleDTO>();
		FlexiFareRulesInformationDTO flexiFareInfoDTO = new FlexiFareRulesInformationDTO();
		for (ExternalChargeTO flexiCharge : flexiCharges) {
			boolean found = true;
			FareRuleDTO fareRuleDTO = fareRuleDTOMap.get(flexiCharge.getSegmentCode());
			if (fareRuleDTO == null) {
				fareRuleDTO = new FareRuleDTO();
				found = false;
				fareRuleDTOMap.put(flexiCharge.getSegmentCode(), fareRuleDTO);

				String formattedTotal = AccelAeroCalculator
						.formatAsDecimal(segmentTotalMap.get(flexiCharge.getOndSequence() + flexiCharge.getSegmentCode()));
				BigDecimal totalPriceInSelCurr = AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(formattedTotal),
						currency.getBoundryValue(), currency.getBreakPoint());
				flexiFareInfoDTO.getTotalSegmentTicketPrice().add(flexiCharge.getSegmentCode() + "-" + formattedTotal);
				flexiFareInfoDTO.getTotalSegmentTicketPriceInSelCurr()
						.add(flexiCharge.getSegmentCode() + "-" + totalPriceInSelCurr.toString());

			}

			if (fareRuleDTO.getSelectedCurrFareDTO() == null) {
				fareRuleDTO.setSelectedCurrFareDTO(new DepAPFareDTO());
			}

			if (flexiCharge.getApplicablePassengerTypeCode().contains("AD")) {
				flexiFareInfoDTO.setAdultApplicable(true);
				fareRuleDTO.setAdultFareAmount(flexiCharge.getAmount());
				BigDecimal adultFlexiInSelCurr = AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), flexiCharge.getAmount(), currency.getBoundryValue(),
						currency.getBreakPoint());
				fareRuleDTO.getSelectedCurrFareDTO().setAdultFare(adultFlexiInSelCurr.doubleValue());
				fareRuleDTO.setAdultFareApplicable(true);
			} else if (flexiCharge.getApplicablePassengerTypeCode().contains("CH")) {
				flexiFareInfoDTO.setChildApplicable(true);
				fareRuleDTO.setChildFareAmount(flexiCharge.getAmount());
				BigDecimal childFlexiInSelCurr = AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), flexiCharge.getAmount(), currency.getBoundryValue(),
						currency.getBreakPoint());
				fareRuleDTO.getSelectedCurrFareDTO().setChildFare(childFlexiInSelCurr.doubleValue());
				fareRuleDTO.setChildFareApplicable(true);
			} else if (flexiCharge.getApplicablePassengerTypeCode().contains("IN")
					|| flexiCharge.getApplicablePassengerTypeCode().contains("INF")) {
				flexiFareInfoDTO.setInfantApplicable(true);
				fareRuleDTO.setInfantFareAmount(flexiCharge.getAmount());
				BigDecimal infantFlexiInSelCurr = AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), flexiCharge.getAmount(), currency.getBoundryValue(),
						currency.getBreakPoint());
				fareRuleDTO.getSelectedCurrFareDTO().setInfantFare(infantFlexiInSelCurr.doubleValue());
				fareRuleDTO.setInfantFareApplicable(true);
			}
			if (!found) {
				fareRuleDTO.setOrignNDest(flexiCharge.getSegmentCode());
				String flexiMessage = "#1 Modification(s)#3 up to #2 hours before departure";
				if (flexiCharge.getAdditionalDetails() != null) {
					for (FlexiInfoTO flexiInfoTO : flexiCharge.getAdditionalDetails()) {
						if (flexiInfoTO.getFlexibilityTypeId() == 1) {// Modification
							flexiMessage = flexiMessage.replace("#1", String.valueOf(flexiInfoTO.getAvailableCount()));
							flexiMessage = flexiMessage.replace("#2",
									String.valueOf((int) flexiInfoTO.getCutOverBufferInMins() / 60));
						} else if (flexiInfoTO.getFlexibilityTypeId() == 2) {// Cancellation
							flexiMessage = flexiMessage.replace("#3", ", Cancellation");
						}
					}
					if (flexiMessage.contains("#3")) {
						flexiMessage = flexiMessage.replace("#3", "");
					}
				}

				if (segmentDepartureTimeMap.containsKey(fareRuleDTO.getOrignNDest())) {
					fareRuleDTO.setDepartureDateLong(segmentDepartureTimeMap.get(fareRuleDTO.getOrignNDest()));
				}

				fareRuleDTO.setDescription(flexiMessage);
				fareRuleDTO.setComments(flexiCharge.getComments());
				flexiFareInfoDTO.setHasComment(flexiCharge.getComments() != null);
			}
		}
		List<FareRuleDTO> flexiFareRuleDTOs = new ArrayList<FareRuleDTO>(fareRuleDTOMap.values());
		Collections.sort(flexiFareRuleDTOs);
		flexiFareInfoDTO.setFareRules(flexiFareRuleDTOs);
		return flexiFareInfoDTO;
	}

	private static Map<String, BigDecimal> getSegmentwiseTotalPrices(FareTypeTO fareTypeTo) {

		Map<String, BigDecimal> segmentwiseTotals = new HashMap<String, BigDecimal>();

		for (BaseFareTO baseFare : fareTypeTo.getBaseFares()) {

			String key = baseFare.getOndSequence() + baseFare.getSegmentCode();

			if (segmentwiseTotals.containsKey(key)) {
				segmentwiseTotals.put(key, AccelAeroCalculator.add(segmentwiseTotals.get(key), baseFare.getAmount()));
			} else {
				segmentwiseTotals.put(key, baseFare.getAmount());
			}
		}

		for (TaxTO tax : fareTypeTo.getTaxes()) {

			for (String key : segmentwiseTotals.keySet()) {
				if (key.contains(tax.getSegmentCode())) {
					segmentwiseTotals.put(key, AccelAeroCalculator.add(segmentwiseTotals.get(key), tax.getAmount()));
					break;
				}
			}
		}

		for (SurchargeTO surcharge : fareTypeTo.getSurcharges()) {

			for (String key : segmentwiseTotals.keySet()) {
				if (key.contains(surcharge.getSegmentCode())) {
					segmentwiseTotals.put(key, AccelAeroCalculator.add(segmentwiseTotals.get(key), surcharge.getAmount()));
					break;
				}
			}
		}

		for (FeeTO fee : fareTypeTo.getFees()) {

			for (String key : segmentwiseTotals.keySet()) {
				if (key.contains(fee.getSegmentCode())) {
					segmentwiseTotals.put(key, AccelAeroCalculator.add(segmentwiseTotals.get(key), fee.getAmount()));
					break;
				}
			}
		}

		return segmentwiseTotals;
	}

	public static String createCarrierCodeOptions(List<LCCClientCarrierOndGroup> carrierGrouping,
			ReservationProcessParams rpParams) {

		StringBuilder carrierGroupingOptions = new StringBuilder();
		if (carrierGrouping != null && carrierGrouping.size() > 0) {
			carrierGroupingOptions.append("<option value='LCC'>LCC</option>"); // LCCHistory
			Map<String, String> carrierCodeMap = new HashMap<String, String>();

			for (LCCClientCarrierOndGroup carrierOndGroup : carrierGrouping) {
				boolean insert = false;
				if (AppSysParamsUtil.getDefaultCarrierCode().equals(carrierOndGroup.getCarrierCode())) {
					insert = true;
				} else if (!AppSysParamsUtil.getDefaultCarrierCode().equals(carrierOndGroup.getCarrierCode())
						&& rpParams.isViewReservationHistoryForOtherCarriers()) {// User
																					// has
																					// the
																					// privilege
																					// to
																					// view
																					// other
																					// caarier's
																					// history
					insert = true;
				}

				if (insert && (!carrierCodeMap.containsKey(carrierOndGroup.getCarrierCode()))) {// To check whether the
																								// carrier code is
																								// already there.
					carrierCodeMap.put(carrierOndGroup.getCarrierCode(), "");
					carrierGroupingOptions.append("<option value='" + carrierOndGroup.getCarrierCode() + "'>"
							+ carrierOndGroup.getCarrierCode() + "</option>");
				}
			}
		}

		return carrierGroupingOptions.toString();
	}

	public static Collection<LCCClientReservationPax> transformJsonPassengers(String resPaxss) throws Exception {
		Collection<LCCClientReservationPax> colpaxs = new ArrayList<LCCClientReservationPax>();
		JSONArray jsonPaxArray = (JSONArray) new JSONParser().parse(resPaxss);

		Iterator<?> itIter = jsonPaxArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			if (jPPObj != null) {
				colpaxs.add(transformPax(jPPObj));
			}
		}
		return colpaxs;
	}

	private static LCCClientReservationPax transformPax(JSONObject jPPObj) throws Exception {
		String strDateformat = "yyyy-MM-dd'T'HH:mm:ss";
		LCCClientReservationPax pax = new LCCClientReservationPax();
		LCCClientPaymentHolder lccClientPaymentHolder = new LCCClientPaymentHolder();

		List<TaxTO> taxes = new ArrayList<TaxTO>();
		List<FeeTO> fees = new ArrayList<FeeTO>();
		List<SurchargeTO> surcharges = new ArrayList<SurchargeTO>();
		List<FareTO> fares = new ArrayList<FareTO>();
		Collection<LccClientPassengerEticketInfoTO> eTickets = new ArrayList<LccClientPassengerEticketInfoTO>();

		List<LCCSelectedSegmentAncillaryDTO> anciSegList = new ArrayList<LCCSelectedSegmentAncillaryDTO>();

		if (jPPObj.get("fares") != null) {
			JSONArray jsonFareArray = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("fares")));
			transformJsonFare(fares, jsonFareArray, strDateformat);
		}

		if (jPPObj.get("taxes") != null) {
			JSONArray jsonTaxArray = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("taxes")));
			transformJsonTax(taxes, jsonTaxArray, strDateformat);
		}

		if (jPPObj.get("fees") != null) {
			JSONArray jsonFeeArray = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("fees")));
			transformJsonFee(fees, jsonFeeArray, strDateformat);
		}

		if (jPPObj.get("surcharges") != null) {
			JSONArray jsonSurchargeArray = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("surcharges")));
			transformJsonSurcharge(surcharges, jsonSurchargeArray, strDateformat);
		}

		if (jPPObj.get("lccClientPaymentHolder") != null) {
			JSONObject jsonPayHolder = (JSONObject) new JSONParser()
					.parse(BeanUtils.nullHandler(jPPObj.get("lccClientPaymentHolder")));
			transformPayments(lccClientPaymentHolder, jsonPayHolder, strDateformat);
			transformCredits(lccClientPaymentHolder, jsonPayHolder, strDateformat);
		}

		if (jPPObj.get("selectedAncillaries") != null) {
			JSONArray jsonAnciArray = (JSONArray) new JSONParser()
					.parse(BeanUtils.nullHandler(jPPObj.get("selectedAncillaries")));
			transformAnciList(anciSegList, jsonAnciArray);
		}

		if (jPPObj.get("eTickets") != null) {
			JSONArray jsonEtickets = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("eTickets")));
			transformEticketList(eTickets, jsonEtickets);
		}

		pax.setFares(fares);
		pax.setFees(fees);
		pax.setFirstName(BeanUtils.nullHandler(jPPObj.get("firstName")));
		pax.setLastName(BeanUtils.nullHandler(jPPObj.get("lastName")));
		pax.setLccClientPaymentHolder(lccClientPaymentHolder);
		pax.setPaxSequence(new Integer(BeanUtils.nullHandler((jPPObj.get("paxSequence")))));
		pax.setPaxType(BeanUtils.nullHandler(jPPObj.get("paxType")));
		pax.setStatus(BeanUtils.nullHandler(jPPObj.get("status")));
		pax.setSurcharges(surcharges);
		pax.setTaxes(taxes);
		pax.setTitle(BeanUtils.nullHandler(jPPObj.get("title")));
		pax.getInfants().addAll(transformJsonPassengers(BeanUtils.nullHandler(jPPObj.get("infants"))));
		pax.setSelectedAncillaries(anciSegList);
		pax.seteTickets(eTickets);

		if (jPPObj.get("dateOfBirth") != null) {
			pax.setDateOfBirth(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("dateOfBirth")), strDateformat));
		}
		if (jPPObj.get("totalAdjustmentCharge") != null) {
			pax.setTotalAdjustmentCharge(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAdjustmentCharge"))));
		}
		if (jPPObj.get("totalAvailableBalance") != null) {
			pax.setTotalAvailableBalance(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAvailableBalance"))));
		}
		if (jPPObj.get("totalActualCredit") != null) {
			pax.setTotalActualCredit(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalActualCredit"))));
		}
		if (jPPObj.get("totalCancelCharge") != null) {
			pax.setTotalCancelCharge(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalCancelCharge"))));
		}

		if (jPPObj.get("totalFare") != null) {
			pax.setTotalFare(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalFare"))));
		}

		if (jPPObj.get("totalModificationCharge") != null) {
			pax.setTotalModificationCharge(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalModificationCharge"))));
		}

		if (jPPObj.get("totalPaidAmount") != null) {
			pax.setTotalPaidAmount(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalPaidAmount"))));
		}

		if (jPPObj.get("totalPrice") != null) {
			pax.setTotalPrice(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalPrice"))));
		}

		if (jPPObj.get("totalSurCharge") != null) {
			pax.setTotalSurCharge(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalSurCharge"))));
		}

		if (jPPObj.get("totalTaxCharge") != null) {
			pax.setTotalTaxCharge(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalTaxCharge"))));
		}

		if (jPPObj.get("parent") != null) {
			pax.setParent(transformPax((JSONObject) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("parent")))));
		}

		if (jPPObj.get("lccClientAdditionPax") != null && !jPPObj.get("lccClientAdditionPax").equals("")) {
			LCCClientReservationAdditionalPax addnPax = null;
			if (pax.getLccClientAdditionPax() != null) {
				addnPax = pax.getLccClientAdditionPax();
			} else {
				addnPax = new LCCClientReservationAdditionalPax();
				JSONObject paxInfo = (JSONObject) jPPObj.get("lccClientAdditionPax");
				if (paxInfo.get("passportExpiry") != null) {
					addnPax.setPassportExpiry(
							DateUtil.parseDate(BeanUtils.nullHandler(paxInfo.get("passportExpiry")), strDateformat));
				}
				addnPax.setPassportIssuedCntry(BeanUtils.nullHandler(paxInfo.get("passportIssuedCntry")));
				addnPax.setPassportNo(BeanUtils.nullHandler(paxInfo.get("passportNo")));
				addnPax.setFfid(BeanUtils.nullHandler(paxInfo.get("ffid")));

				addnPax.setPlaceOfBirth(BeanUtils.nullHandler(paxInfo.get("placeOfBirth")));
				if (paxInfo.get("travelDocumentType") != null && paxInfo.get("travelDocumentType").toString().length() > 0) {
					addnPax.setTravelDocumentType(BeanUtils.nullHandler(paxInfo.get("travelDocumentType")));
				}
				addnPax.setVisaDocNumber(BeanUtils.nullHandler(paxInfo.get("visaDocNumber")));
				addnPax.setVisaDocPlaceOfIssue(BeanUtils.nullHandler(paxInfo.get("visaDocPlaceOfIssue")));
				if (paxInfo.get("displayVisaDocIssueDate") != null) {
					addnPax.setVisaDocIssueDate(
							DateUtil.parseDate(BeanUtils.nullHandler(paxInfo.get("displayVisaDocIssueDate")), strDateformat));
				}
				addnPax.setVisaDocPlaceOfIssue(BeanUtils.nullHandler(paxInfo.get("visaDocPlaceOfIssue")));
			}
			pax.setLccClientAdditionPax(addnPax);
		}

		pax.setTravelerRefNumber(BeanUtils.nullHandler(jPPObj.get("travelerRefNumber")));
		pax.setAlertAutoCancellation(new Boolean(BeanUtils.nullHandler(jPPObj.get("alertAutoCancellation"))));
		pax.setZuluReleaseTimeStamp(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("zuluReleaseTimeStamp")), strDateformat));
		pax.setZuluStartTimeStamp(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("zuluStartTimeStamp")), strDateformat));

		return pax;
	}

	private static void transformEticketList(Collection<LccClientPassengerEticketInfoTO> eTickets, JSONArray jsonEtickets) {
		Iterator<?> itIter = jsonEtickets.iterator();
		while (itIter.hasNext()) {
			LccClientPassengerEticketInfoTO paxET = new LccClientPassengerEticketInfoTO();
			JSONObject innerObj = (JSONObject) itIter.next();
			if (innerObj != null) {
				String paxStatus = BeanUtils.nullHandler(innerObj.get("paxStatus"));
				String pnrSegId = BeanUtils.nullHandler(innerObj.get("pnrSegId"));
				String paxETicketStatus = BeanUtils.nullHandler(innerObj.get("paxETicketStatus"));
				String travelerRefNumber = BeanUtils.nullHandler(innerObj.get("travelerRefNumber"));
				String flightSegmentRef = BeanUtils.nullHandler(innerObj.get("flightSegmentRef"));
				paxET.setPaxStatus(paxStatus);
				paxET.setPnrSegId(pnrSegId);
				paxET.setPaxETicketStatus(paxETicketStatus);
				paxET.setTravelerRefNumber(travelerRefNumber);
				paxET.setFlightSegmentRef(flightSegmentRef);
				eTickets.add(paxET);
			}
		}

	}

	public static void transformAnciList(List<LCCSelectedSegmentAncillaryDTO> anciSegList, JSONArray anciArray)
			throws ParseException {
		Iterator<?> itIter = anciArray.iterator();
		while (itIter.hasNext()) {
			LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
			JSONObject innerObj = (JSONObject) itIter.next();
			JSONObject fltObj = (JSONObject) innerObj.get("flightSegmentTO");

			if (fltObj != null) {
				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
				flightSegmentTO.setFlightRefNumber(BeanUtils.nullHandler(fltObj.get("flightRefNumber")));
				flightSegmentTO.setSegmentCode(BeanUtils.nullHandler(fltObj.get("segmentCode")));
				flightSegmentTO.setFlightNumber(BeanUtils.nullHandler(fltObj.get("flightNumber")));
				flightSegmentTO.setOperatingAirline(BeanUtils.nullHandler(fltObj.get("operatingAirline")));
				lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(flightSegmentTO);
			}

			JSONObject seatObj = (JSONObject) innerObj.get("airSeatDTO");
			if (seatObj != null && !"".equals(BeanUtils.nullHandler(seatObj.get("seatCharge")))) {
				LCCAirSeatDTO seatDto = new LCCAirSeatDTO();
				seatDto.setSeatCharge(new BigDecimal(seatObj.get("seatCharge").toString()));
				seatDto.setSeatNumber(BeanUtils.nullHandler(seatObj.get("seatNumber")));
				lccSelectedSegmentAncillaryDTO.setAirSeatDTO(seatDto);
			}

			JSONArray mealArr = (JSONArray) innerObj.get("mealDTOs");
			if (mealArr != null && !mealArr.isEmpty()) {
				List<LCCMealDTO> mealDTOs = new ArrayList<LCCMealDTO>();

				Iterator<?> itMeal = mealArr.iterator();
				while (itMeal.hasNext()) {
					JSONObject mealObj = (JSONObject) itMeal.next();
					if (mealObj != null) {
						LCCMealDTO mealDTO = new LCCMealDTO();
						mealDTO.setMealCode(BeanUtils.nullHandler(mealObj.get("mealCode")));
						mealDTO.setSoldMeals(Integer.parseInt(mealObj.get("soldMeals").toString()));
						mealDTO.setMealCharge(new BigDecimal(mealObj.get("mealCharge").toString()));
						mealDTO.setTotalPrice(mealObj.get("totalPrice").toString());
						mealDTOs.add(mealDTO);
					}

				}

				lccSelectedSegmentAncillaryDTO.setMealDTOs(mealDTOs);
			}

			JSONArray bgArr = (JSONArray) innerObj.get("baggageDTOs");
			if (bgArr != null && !bgArr.isEmpty()) {
				List<LCCBaggageDTO> baggageList = new ArrayList<LCCBaggageDTO>();
				Iterator<?> itBg = bgArr.iterator();
				while (itBg.hasNext()) {
					JSONObject bgObj = (JSONObject) itBg.next();
					if (bgObj != null) {
						LCCBaggageDTO baggageDTO = new LCCBaggageDTO();
						baggageDTO.setBaggageCharge(new BigDecimal(bgObj.get("baggageCharge").toString()));
						baggageDTO.setSoldPieces(Integer.parseInt(bgObj.get("soldPieces").toString()));
						baggageList.add(baggageDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setBaggageDTOs(baggageList);
			}

			JSONObject insObj = (JSONObject) innerObj.get("insuranceQuotation");
			if (insObj != null && !"".equals(BeanUtils.nullHandler(insObj.get("totalPerPaxPremiumAmount")))) {
				LCCInsuranceQuotationDTO ins = new LCCInsuranceQuotationDTO();
				ins.setTotalPerPaxPremiumAmount(new BigDecimal(insObj.get("totalPerPaxPremiumAmount").toString()));
				List<LCCInsuranceQuotationDTO> quotations = new ArrayList<LCCInsuranceQuotationDTO>();
				quotations.add(ins);
				lccSelectedSegmentAncillaryDTO.setInsuranceQuotations(quotations);
			}

			JSONArray apServiceArr = (JSONArray) innerObj.get("airportServiceDTOs");

			if (apServiceArr != null && !apServiceArr.isEmpty()) {
				List<LCCAirportServiceDTO> airportServiceDTOs = new ArrayList<LCCAirportServiceDTO>();

				Iterator<?> itAps = apServiceArr.iterator();
				while (itAps.hasNext()) {
					JSONObject apsObj = (JSONObject) itAps.next();
					if (apsObj != null) {
						LCCAirportServiceDTO lccAirportServiceDTO = new LCCAirportServiceDTO();
						lccAirportServiceDTO.setSsrCode(BeanUtils.nullHandler(apsObj.get("ssrCode")));
						lccAirportServiceDTO.setSsrDescription(BeanUtils.nullHandler(apsObj.get("ssrDescription")));
						lccAirportServiceDTO.setAirportCode(BeanUtils.nullHandler(apsObj.get("airportCode")));
						lccAirportServiceDTO.setServiceCharge(new BigDecimal(BeanUtils.nullHandler(apsObj.get("serviceCharge"))));
						airportServiceDTOs.add(lccAirportServiceDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setAirportServiceDTOs(airportServiceDTOs);
			}
			JSONArray apTransferArr = (JSONArray) innerObj.get("airportTransferDTOs");

			if (apTransferArr != null && !apTransferArr.isEmpty()) {
				List<LCCAirportServiceDTO> airportTransferDTOs = new ArrayList<LCCAirportServiceDTO>();

				Iterator<?> itApt = apTransferArr.iterator();
				while (itApt.hasNext()) {
					JSONObject aptObj = (JSONObject) itApt.next();
					if (aptObj != null) {
						LCCAirportServiceDTO airportTransferDTO = new LCCAirportServiceDTO();
						airportTransferDTO.setSsrCode(BeanUtils.nullHandler(aptObj.get("ssrCode")));
						airportTransferDTO.setSsrDescription(BeanUtils.nullHandler(aptObj.get("ssrDescription")));
						airportTransferDTO.setAirportCode(BeanUtils.nullHandler(aptObj.get("airportCode")));
						airportTransferDTO.setServiceCharge(new BigDecimal(BeanUtils.nullHandler(aptObj.get("serviceCharge"))));
						airportTransferDTO.setTransferAddress(BeanUtils.nullHandler(aptObj.get("transferAddress")));
						airportTransferDTO.setTransferContact(BeanUtils.nullHandler(aptObj.get("transferContact")));
						airportTransferDTO.setTransferType(BeanUtils.nullHandler(aptObj.get("transferType")));
						airportTransferDTO.setTranferDate(BeanUtils.nullHandler(aptObj.get("tranferDate")));
						airportTransferDTOs.add(airportTransferDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setAirportTransferDTOs(airportTransferDTOs);
			}

			JSONArray autoCheckinArr = (JSONArray) innerObj.get("autoCheckinDTOs");
			if (autoCheckinArr != null && !autoCheckinArr.isEmpty()) {
				List<LCCAutomaticCheckinDTO> autoCheckinDTOs = new ArrayList<LCCAutomaticCheckinDTO>();
				Iterator<?> itAutoCheckin = autoCheckinArr.iterator();
				while (itAutoCheckin.hasNext()) {
					JSONObject autoCheckinObj = (JSONObject) itAutoCheckin.next();
					if (autoCheckinObj != null) {
						LCCAutomaticCheckinDTO autoCheckinDTO = new LCCAutomaticCheckinDTO();
						autoCheckinDTO.setAutoCheckinId(Integer.valueOf(autoCheckinObj.get("autoCheckinTemplateId").toString()));
						autoCheckinDTO
								.setAutomaticCheckinCharge(new BigDecimal(autoCheckinObj.get("autoCheckinCharge").toString()));
						autoCheckinDTO.setSeatPref(autoCheckinObj.get("seatPreference").toString());
						autoCheckinDTO.setEmail(autoCheckinObj.get("email").toString());
						autoCheckinDTO.setSeatCode(autoCheckinObj.get("seatCode").toString());
						autoCheckinDTOs.add(autoCheckinDTO);
					}

				}

				lccSelectedSegmentAncillaryDTO.setAutomaticCheckinDTOs(autoCheckinDTOs);
			}

			JSONArray ssrArr = (JSONArray) innerObj.get("specialServiceRequestDTOs");

			if (ssrArr != null && !ssrArr.isEmpty()) {
				List<LCCSpecialServiceRequestDTO> ssrDTOs = new ArrayList<LCCSpecialServiceRequestDTO>();

				Iterator<?> itAps = ssrArr.iterator();
				while (itAps.hasNext()) {
					JSONObject ssrObj = (JSONObject) itAps.next();
					if (ssrObj != null) {
						LCCSpecialServiceRequestDTO lccSSRDTO = new LCCSpecialServiceRequestDTO();
						lccSSRDTO.setSsrCode(BeanUtils.nullHandler(ssrObj.get("ssrCode")));
						lccSSRDTO.setCharge(new BigDecimal(ssrObj.get("charge").toString()));
						lccSSRDTO.setServiceQuantity(BeanUtils.nullHandler(ssrObj.get("serviceQuantity")));
						ssrDTOs.add(lccSSRDTO);
					}
				}

				lccSelectedSegmentAncillaryDTO.setSpecialServiceRequestDTOs(ssrDTOs);
			}

			anciSegList.add(lccSelectedSegmentAncillaryDTO);
		}
	}

	public static List<LCCClientCarrierOndGroup> transformJsonONDs(String resOnds) throws Exception {
		List<LCCClientCarrierOndGroup> colOnds = new ArrayList<LCCClientCarrierOndGroup>();
		JSONArray jsonOndArray = (JSONArray) new JSONParser().parse(resOnds);
		Iterator<?> itIter = jsonOndArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			LCCClientCarrierOndGroup crrOnd = new LCCClientCarrierOndGroup();
			crrOnd.setCarrierCode(BeanUtils.nullHandler(jPPObj.get("carrierCode")));
			crrOnd.setCarrierOndGroupRPH(BeanUtils.nullHandler(jPPObj.get("carrierOndGroupRPH")));
			crrOnd.setSegmentCode(BeanUtils.nullHandler(jPPObj.get("segmentCode")));
			crrOnd.setStatus(BeanUtils.nullHandler(jPPObj.get("status")));
			crrOnd.setSubStatus(BeanUtils.nullHandler(jPPObj.get("subStatus")));
			JSONArray jsonArray = (JSONArray) jPPObj.get("carrierPnrSegIds");
			if (jsonArray != null) {
				crrOnd.setCarrierPnrSegIds(new ArrayList<Integer>());
				for (int i = 0; i < jsonArray.size(); i++) {
					crrOnd.getCarrierPnrSegIds().add(((Long) jsonArray.get(i)).intValue());
				}
			}

			colOnds.add(crrOnd);
		}
		return colOnds;

	}

	public static List<FlightSegmentTO> populateFlightSegmentList(String jsonFlightList, FlightSearchDTO searchParams)
			throws ParseException, org.json.simple.parser.ParseException {
		List<FlightSegmentTO> flightSegList = new ArrayList<FlightSegmentTO>();
		if (jsonFlightList != null && !"".equals(jsonFlightList)) {
			@SuppressWarnings("unchecked")
			List<FlightSegmentFETO> fltSegFETOList = JSONFETOParser.parseCollection(ArrayList.class, FlightSegmentFETO.class,
					jsonFlightList);

			if (fltSegFETOList != null && fltSegFETOList.size() > 0) {
				for (FlightSegmentFETO flightSegmentFETO : fltSegFETOList) {
					FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
					String flightRefNumber = flightSegmentFETO.getSegmentRPH() != null
							? flightSegmentFETO.getSegmentRPH().split("#")[0]
							: null;
					flightSegmentTO.setFlightRefNumber(flightRefNumber);
					flightSegmentTO.setSegmentCode(flightSegmentFETO.getSegmentCode());
					flightSegmentTO.setFlightNumber(flightSegmentFETO.getFlightNo());
					flightSegmentTO.setOperatingAirline(flightSegmentFETO.getCarrierCode());
					flightSegmentTO.setDepartureDateTime(
							DateUtil.parseDate(flightSegmentFETO.getDepartureDate(), "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegmentTO.setDepartureDateTimeZulu(
							DateUtil.parseDate(flightSegmentFETO.getDepartureDateTimeZulu(), "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegmentTO
							.setArrivalDateTime(DateUtil.parseDate(flightSegmentFETO.getArrivalDate(), "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegmentTO.setArrivalDateTimeZulu(
							DateUtil.parseDate(flightSegmentFETO.getArrivalDateTimeZulu(), "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegmentTO.setOndSequence(flightSegmentFETO.getOndSequence());
					flightSegmentTO.setDomesticFlight(flightSegmentFETO.isDomesticFlight());
					flightSegmentTO
							.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentFETO.getSegmentRPH()));
					ReservationBeanUtil.setClassOfServiceInfo(flightSegmentTO, searchParams);
					ReservationBeanUtil.setBookingClassInfo(flightSegmentTO, searchParams);
					flightSegList.add(flightSegmentTO);
				}
			}
		}

		Collections.sort(flightSegList);
		return flightSegList;
	}

	public static List<FlightSegmentTO> getFlightSegmentFromRPHList(String rphString, FlightSearchDTO searchParams)
			throws ParseException, org.json.simple.parser.ParseException {
		List<FlightSegmentTO> flightSegList = new ArrayList<FlightSegmentTO>();
		if (rphString != null && !rphString.equals("")) {

			Map<String, String> busAirCarrierCodes = null;
			try {
				busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();
			} catch (ModuleException e) {
				log.info(e);
			}
			JSONParser parser = new JSONParser();
			JSONArray jsonRPH = (JSONArray) parser.parse(rphString);

			Iterator jItr = jsonRPH.iterator();
			while (jItr.hasNext()) {

				JSONObject jObj = (JSONObject) jItr.next();
				String flightRPH = BeanUtils.nullHandler(jObj.get("flightRPH")).toString();
				String routeRPH = null;
				if (flightRPH != null && flightRPH.indexOf("#") != -1) {
					String[] flightRouteRPH = flightRPH.split("#");
					flightRPH = flightRouteRPH[0];
					routeRPH = flightRouteRPH[1];
				}
				String segCode = BeanUtils.nullHandler(jObj.get("segmentCode")).toString();
				String flightNumber = BeanUtils.nullHandler(jObj.get("flightNo")).toString();
				String operatingAirline = FlightRefNumberUtil.getOperatingAirline(flightRPH);// jObj.get("carrierCode").toString();
				if (flightRPH == null || flightRPH.equals("")) {
					flightRPH = BeanUtils.nullHandler(jObj.get("segmentRPH")).toString();
					if (flightRPH != null && flightRPH.indexOf("#") != -1) {
						String[] flightRouteRPH = flightRPH.split("#");
						flightRPH = flightRouteRPH[0];
						routeRPH = flightRouteRPH[1];
					}
					operatingAirline = FlightRefNumberUtil.getOperatingAirline(flightRPH);
				}
				String arrivalTime = BeanUtils.nullHandler(jObj.get("arrivalTime")).toString();
				String departureTime = BeanUtils.nullHandler(jObj.get("departureTime")).toString();
				String arrivalTimeZulu = BeanUtils.nullHandler(jObj.get("arrivalTimeZulu")).toString();
				if (arrivalTimeZulu == null || arrivalTimeZulu.equals("")) {
					arrivalTimeZulu = BeanUtils.nullHandler(jObj.get("arrivalDateTimeZulu")).toString();
				}
				String departureTimeZulu = BeanUtils.nullHandler(jObj.get("departureTimeZulu")).toString();
				if (departureTimeZulu == null || departureTimeZulu.equals("")) {
					departureTimeZulu = BeanUtils.nullHandler(jObj.get("departureDateTimeZulu")).toString();
				}
				boolean returnFlag = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("returnFlag")).toString());
				boolean isDomestic = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("domesticFlight")).toString());
				boolean waitListed = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("waitListed")).toString());
				String ondSequence = BeanUtils.nullHandler(jObj.get("ondSequence")).toString();
				String subStationShortName = null;
				if (AddModifySurfaceSegmentValidations.isMatchingSegmentCodeForFromSubStation(segCode,
						searchParams.getFromAirportSubStation(), returnFlag)) {
					subStationShortName = searchParams.getFromAirportSubStation();
				} else if (AddModifySurfaceSegmentValidations.isMatchingSegmentCodeForToSubStation(segCode,
						searchParams.getToAirportSubStation(), returnFlag)) {
					subStationShortName = searchParams.getToAirportSubStation();
				}
				FlightSegmentTO flightSegTO = new FlightSegmentTO();
				flightSegTO.setFlightRefNumber(flightRPH);
				flightSegTO.setRouteRefNumber(routeRPH);
				flightSegTO.setSegmentCode(segCode);
				flightSegTO.setArrivalDateTime(DateUtil.parseDate(arrivalTime, getMatchingPattern(arrivalTime, "yyMMddHHmm")));
				flightSegTO
						.setDepartureDateTime(DateUtil.parseDate(departureTime, getMatchingPattern(departureTime, "yyMMddHHmm")));
				flightSegTO.setArrivalDateTimeZulu(
						DateUtil.parseDate(arrivalTimeZulu, getMatchingPattern(arrivalTimeZulu, "yyMMddHHmm")));
				flightSegTO.setDepartureDateTimeZulu(
						DateUtil.parseDate(departureTimeZulu, getMatchingPattern(departureTimeZulu, "yyMMddHHmm")));
				flightSegTO.setFlightNumber(flightNumber);

				if (busAirCarrierCodes != null && busAirCarrierCodes.get(operatingAirline) != null) {// AEROMART-3141
					flightSegTO.setOperatingAirline(busAirCarrierCodes.get(operatingAirline));
				} else {
					flightSegTO.setOperatingAirline(operatingAirline);
				}

				flightSegTO.setReturnFlag(returnFlag);
				flightSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRPH));
				flightSegTO.setSubStationShortName(subStationShortName);
				flightSegTO.setDomesticFlight(isDomestic);
				flightSegTO.setBookingType(searchParams.getBookingType());
				flightSegTO.setWaitListed(waitListed);
				if (!"".equals(ondSequence)) {
					flightSegTO.setOndSequence(Integer.parseInt(ondSequence));
				}
				// Set cabin class & Logical Cabin Class code
				ReservationBeanUtil.setClassOfServiceInfo(flightSegTO, searchParams);
				ReservationBeanUtil.setBookingClassInfo(flightSegTO, searchParams);
				flightSegList.add(flightSegTO);
			}

		}
		Collections.sort(flightSegList);
		return flightSegList;
	}

	private static String getMatchingPattern(String dateValue, String defaultFormat) {

		String formatPattern1 = "yyMMddHHmm";
		String formatPattern1RegEx = "\\d{10}";
		String formatPattern2 = "yyyy-MM-dd'T'HH:mm:ss";
		String formatPattern2RegEx = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}";
		String matchingPattern = null;

		if (dateValue.matches(formatPattern1RegEx)) {
			matchingPattern = formatPattern1;
		} else if (dateValue.matches(formatPattern2RegEx)) {
			matchingPattern = formatPattern2;
		} else {
			matchingPattern = defaultFormat;
		}

		return matchingPattern;

	}

	public static List<FlightSegmentTO> transformFlightSegmentTo(String rphString)
			throws ParseException, org.json.simple.parser.ParseException {
		List<FlightSegmentTO> flightSegList = new ArrayList<FlightSegmentTO>();
		if (rphString != null && !rphString.equals("")) {
			JSONParser parser = new JSONParser();
			JSONArray jsonRPH = (JSONArray) parser.parse(rphString);

			Iterator jItr = jsonRPH.iterator();
			while (jItr.hasNext()) {

				JSONObject jObj = (JSONObject) jItr.next();
				String flightRPH = BeanUtils.nullHandler(jObj.get("flightRefNumber")).toString();
				String routeRPH = null;
				if (flightRPH != null && flightRPH.indexOf("#") != -1) {
					String[] flightRouteRPH = flightRPH.split("#");
					flightRPH = flightRouteRPH[0];
					routeRPH = flightRouteRPH[1];
				}
				String segCode = BeanUtils.nullHandler(jObj.get("segmentCode")).toString();
				String flightNumber = BeanUtils.nullHandler(jObj.get("flightNumber")).toString();
				String operatingAirline = BeanUtils.nullHandler(jObj.get("operatingAirline")).toString();
				String arrivalTime = BeanUtils.nullHandler(jObj.get("arrivalDateTime")).toString();
				String departureTime = BeanUtils.nullHandler(jObj.get("departureDateTime")).toString();
				String arrivalTimeZulu = BeanUtils.nullHandler(jObj.get("arrivalDateTimeZulu")).toString();
				String departureTimeZulu = BeanUtils.nullHandler(jObj.get("departureDateTimeZulu")).toString();
				boolean returnFlag = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("returnFlag")).toString());
				boolean isDomestic = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("isDomesticFlight")).toString());
				// boolean waitListed = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("waitListed")).toString());
				// String ondSequence = BeanUtils.nullHandler(jObj.get("ondSequence")).toString();
				// String subStationShortName = null;

				FlightSegmentTO flightSegTO = new FlightSegmentTO();

				flightSegTO.setFlightRefNumber(flightRPH);
				flightSegTO.setRouteRefNumber(routeRPH);
				flightSegTO.setSegmentCode(segCode);
				flightSegTO.setArrivalDateTime(DateUtil.parseDate(arrivalTime, "yyyy-MM-dd'T'HH:mm:ss"));
				flightSegTO.setDepartureDateTime(DateUtil.parseDate(departureTime, "yyyy-MM-dd'T'HH:mm:ss"));
				flightSegTO.setArrivalDateTimeZulu(DateUtil.parseDate(arrivalTimeZulu, "yyyy-MM-dd'T'HH:mm:ss"));
				flightSegTO.setDepartureDateTimeZulu(DateUtil.parseDate(departureTimeZulu, "yyyy-MM-dd'T'HH:mm:ss"));
				flightSegTO.setFlightNumber(flightNumber);
				flightSegTO.setOperatingAirline(operatingAirline);
				flightSegTO.setReturnFlag(returnFlag);
				flightSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRPH));
				// flightSegTO.setSubStationShortName(subStationShortName);
				flightSegTO.setDomesticFlight(isDomestic);
				// flightSegTO.setBookingType(searchParams.getBookingType());
				// flightSegTO.setWaitListed(waitListed);
				// if (!"".equals(ondSequence)) {
				// flightSegTO.setOndSequence(Integer.parseInt(ondSequence));
				// }
				flightSegList.add(flightSegTO);
			}

		}
		return flightSegList;
	}

	public static List<FlightSegmentTO> transformResFlightSegment(String rphString)
			throws ParseException, org.json.simple.parser.ParseException {
		List<FlightSegmentTO> flightSegList = new ArrayList<FlightSegmentTO>();
		if (rphString != null && !rphString.equals("")) {
			JSONParser parser = new JSONParser();
			JSONArray jsonRPH = (JSONArray) parser.parse(rphString);

			Iterator jItr = jsonRPH.iterator();
			while (jItr.hasNext()) {

				JSONObject jObj = (JSONObject) jItr.next();
				String flightRPH = BeanUtils.nullHandler(jObj.get("flightSegmentRefNumber")).toString();
				String routeRPH = null;
				if (flightRPH != null && flightRPH.indexOf("#") != -1) {
					String[] flightRouteRPH = flightRPH.split("#");
					flightRPH = flightRouteRPH[0];
					routeRPH = flightRouteRPH[1];
				}
				String segCode = BeanUtils.nullHandler(jObj.get("segmentCode")).toString();
				String flightNumber = BeanUtils.nullHandler(jObj.get("flightNo")).toString();
				String operatingAirline = BeanUtils.nullHandler(jObj.get("carrierCode")).toString();
				String arrivalTime = BeanUtils.nullHandler(jObj.get("arrivalDate")).toString();
				String departureTime = BeanUtils.nullHandler(jObj.get("departureDate")).toString();
				String arrivalTimeZulu = BeanUtils.nullHandler(jObj.get("arrivalDateZulu")).toString();
				String departureTimeZulu = BeanUtils.nullHandler(jObj.get("departureDateZulu")).toString();
				boolean returnFlag = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("returnFlag")).toString());
				boolean isDomestic = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("domesticFlight")).toString());
				String status = BeanUtils.nullHandler(jObj.get("status")).toString();
				// boolean waitListed = Boolean.parseBoolean(BeanUtils.nullHandler(jObj.get("waitListed")).toString());
				// String ondSequence = BeanUtils.nullHandler(jObj.get("ondSequence")).toString();
				// String subStationShortName = null;
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(status)) {
					FlightSegmentTO flightSegTO = new FlightSegmentTO();

					flightSegTO.setFlightRefNumber(flightRPH);
					flightSegTO.setRouteRefNumber(routeRPH);
					flightSegTO.setSegmentCode(segCode);
					flightSegTO.setArrivalDateTime(DateUtil.parseDate(arrivalTime, "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegTO.setDepartureDateTime(DateUtil.parseDate(departureTime, "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegTO.setArrivalDateTimeZulu(DateUtil.parseDate(arrivalTimeZulu, "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegTO.setDepartureDateTimeZulu(DateUtil.parseDate(departureTimeZulu, "yyyy-MM-dd'T'HH:mm:ss"));
					flightSegTO.setFlightNumber(flightNumber);
					flightSegTO.setOperatingAirline(operatingAirline);
					flightSegTO.setReturnFlag(returnFlag);
					flightSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRPH));
					// flightSegTO.setSubStationShortName(subStationShortName);
					flightSegTO.setDomesticFlight(isDomestic);
					// flightSegTO.setBookingType(searchParams.getBookingType());
					// flightSegTO.setWaitListed(waitListed);
					// if (!"".equals(ondSequence)) {
					// flightSegTO.setOndSequence(Integer.parseInt(ondSequence));
					// }
					flightSegList.add(flightSegTO);
				}
			}

		}
		return flightSegList;
	}

	private static void transformJsonFare(List<FareTO> fares, JSONArray jsonFareArray, String dtFormat) throws Exception {
		Iterator<?> itIter = jsonFareArray.iterator();
		while (itIter.hasNext()) {
			FareTO fareTo = new FareTO();
			JSONObject jPPObj = (JSONObject) itIter.next();
			fareTo.setAmount(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("amount"))));
			fareTo.setApplicableDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("applicableDate")), dtFormat));
			fareTo.setDepartureDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("departureDate")), dtFormat));
			fareTo.setCarrierCode(BeanUtils.nullHandler(jPPObj.get("carrierCode")));
			fareTo.setDescription(BeanUtils.nullHandler(jPPObj.get("description")));
			fareTo.setSegmentCode(BeanUtils.nullHandler(jPPObj.get("segmentCode")));
			fares.add(fareTo);
		}
	}

	private static void transformJsonTax(List<TaxTO> taxs, JSONArray jsonTaxArray, String dtFormat) throws Exception {
		Iterator<?> itIter = jsonTaxArray.iterator();
		while (itIter.hasNext()) {
			TaxTO taxTo = new TaxTO();
			JSONObject jPPObj = (JSONObject) itIter.next();
			taxTo.setAmount(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("amount"))));
			taxTo.setApplicableTime(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("applicableTime")), dtFormat));
			taxTo.setDepartureDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("departureDate")), dtFormat));
			taxTo.setCarrierCode(BeanUtils.nullHandler(jPPObj.get("carrierCode")));
			taxTo.setApplicablePassengerTypeCode(
					transformJsonAdultVisibility(BeanUtils.nullHandler(jPPObj.get("applicablePassengerTypeCode"))));
			taxTo.setTaxCode(BeanUtils.nullHandler(jPPObj.get("taxCode")));
			taxTo.setTaxName(BeanUtils.nullHandler(jPPObj.get("taxName")));
			taxTo.setSegmentCode(BeanUtils.nullHandler(jPPObj.get("segmentCode")));
			if (jPPObj.get("chargeRateId") != null) {
				taxTo.setChargeRateId(BeanUtils.nullHandler(jPPObj.get("chargeRateId")));
			} else {
				taxTo.setChargeRateId(null);
			}

			if (jPPObj.get("chagrgeGroupCode") != null) {
				taxTo.setChagrgeGroupCode(BeanUtils.nullHandler(jPPObj.get("chagrgeGroupCode")));
			} else {
				taxTo.setChagrgeGroupCode(null);
			}
			// JIRA 6929
			if (jPPObj.get("reportingChargeGroupCode") != null) {
				taxTo.setReportingChargeGroupCode(BeanUtils.nullHandler(jPPObj.get("reportingChargeGroupCode")));
			} else {
				taxTo.setReportingChargeGroupCode(null);
			}
			taxs.add(taxTo);
		}
	}

	private static void transformJsonSurcharge(List<SurchargeTO> surcharges, JSONArray jsonSurArray, String dtFormat)
			throws Exception {
		Iterator<?> itIter = jsonSurArray.iterator();
		while (itIter.hasNext()) {
			SurchargeTO surchargeTO = new SurchargeTO();
			JSONObject jPPObj = (JSONObject) itIter.next();
			surchargeTO.setAmount(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("amount"))));
			surchargeTO.setApplicableTime(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("applicableTime")), dtFormat));
			surchargeTO.setDepartureDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("departureDate")), dtFormat));
			surchargeTO.setCarrierCode(BeanUtils.nullHandler(jPPObj.get("carrierCode")));
			surchargeTO.setApplicablePassengerTypeCode(
					transformJsonAdultVisibility(BeanUtils.nullHandler(jPPObj.get("applicablePassengerTypeCode"))));
			surchargeTO.setSurchargeCode(BeanUtils.nullHandler(jPPObj.get("surchargeCode")));
			surchargeTO.setSurchargeName(BeanUtils.nullHandler(jPPObj.get("surchargeName")));
			surchargeTO.setSegmentCode(BeanUtils.nullHandler(jPPObj.get("segmentCode")));

			if (jPPObj.get("chargeRateId") != null) {
				surchargeTO.setChargeRateId(BeanUtils.nullHandler(jPPObj.get("chargeRateId")));
			} else {
				surchargeTO.setChargeRateId(null);
			}

			if (jPPObj.get("chagrgeGroupCode") != null) {
				surchargeTO.setChagrgeGroupCode(BeanUtils.nullHandler(jPPObj.get("chagrgeGroupCode")));
			} else {
				surchargeTO.setChagrgeGroupCode(null);
			}
			// JIRA 6929
			if (jPPObj.get("reportingChargeGroupCode") != null) {
				surchargeTO.setReportingChargeGroupCode(BeanUtils.nullHandler(jPPObj.get("reportingChargeGroupCode")));
			} else {
				surchargeTO.setReportingChargeGroupCode(null);
			}
			surcharges.add(surchargeTO);
		}
	}

	private static void transformJsonFee(List<FeeTO> fees, JSONArray jsonFeeArray, String dtFormat) throws Exception {
		Iterator<?> itIter = jsonFeeArray.iterator();
		while (itIter.hasNext()) {
			FeeTO feeTO = new FeeTO();
			JSONObject jPPObj = (JSONObject) itIter.next();
			feeTO.setAmount(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("amount"))));
			feeTO.setApplicableTime(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("applicableTime")), dtFormat));
			feeTO.setDepartureDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("departureDate")), dtFormat));
			feeTO.setCarrierCode(BeanUtils.nullHandler(jPPObj.get("carrierCode")));
			feeTO.setApplicablePassengerTypeCode(
					transformJsonAdultVisibility(BeanUtils.nullHandler(jPPObj.get("applicablePassengerTypeCode"))));
			feeTO.setFeeCode(BeanUtils.nullHandler(jPPObj.get("feeCode")));
			feeTO.setFeeName(BeanUtils.nullHandler(jPPObj.get("feeName")));
			feeTO.setSegmentCode(BeanUtils.nullHandler(jPPObj.get("segmentCode")));

			if (jPPObj.get("chargeRateId") != null) {
				feeTO.setChargeRateId(BeanUtils.nullHandler(jPPObj.get("chargeRateId")));
			} else {
				feeTO.setChargeRateId(null);
			}

			if (jPPObj.get("chagrgeGroupCode") != null) {
				feeTO.setChagrgeGroupCode(BeanUtils.nullHandler(jPPObj.get("chagrgeGroupCode")));
			} else {
				feeTO.setChagrgeGroupCode(null);
			}
			// JIRA 6929
			if (jPPObj.get("reportingChargeGroupCode") != null) {
				feeTO.setReportingChargeGroupCode(BeanUtils.nullHandler(jPPObj.get("reportingChargeGroupCode")));
			} else {
				feeTO.setReportingChargeGroupCode(null);
			}
			fees.add(feeTO);
		}
	}

	private static List<String> transformJsonAdultVisibility(String strTypes) throws Exception {
		JSONArray jsonTypes = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(strTypes));
		List<String> types = new ArrayList<String>();
		Iterator<?> itIter = jsonTypes.iterator();
		while (itIter.hasNext()) {
			types.add(BeanUtils.nullHandler(itIter.next()));
		}

		return types;
	}

	public static List<LCCClientAlertInfoTO> transformJsonAlerts(String alerts) throws Exception {
		JSONArray jsonAlerts = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(alerts));
		List<LCCClientAlertInfoTO> lstAlerts = new ArrayList<LCCClientAlertInfoTO>();
		Iterator<?> itIter = jsonAlerts.iterator();
		while (itIter.hasNext()) {
			LCCClientAlertInfoTO alertInfo = new LCCClientAlertInfoTO();
			JSONObject jPPObj = (JSONObject) itIter.next();
			alertInfo.setFlightSegmantRefNumber(BeanUtils.nullHandler(jPPObj.get("flightSegmantRefNumber")));
			JSONArray jsonAlertTos = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("alertTO")));

			List<LCCClientAlertTO> lstTos = new ArrayList<LCCClientAlertTO>();
			Iterator<?> iAtIter = jsonAlertTos.iterator();
			while (iAtIter.hasNext()) {
				JSONObject jAlertPPObj = (JSONObject) iAtIter.next();
				LCCClientAlertTO alertTo = new LCCClientAlertTO();
				alertTo.setAlertId(new Integer(BeanUtils.nullHandler(jAlertPPObj.get("alertId"))));
				alertTo.setContent(BeanUtils.nullHandler(jAlertPPObj.get("content")));
				lstTos.add(alertTo);
			}
			alertInfo.setAlertTO(lstTos);
			lstAlerts.add(alertInfo);
		}
		return lstAlerts;
	}

	private static void transformPayments(LCCClientPaymentHolder lccClientPaymentHolder, JSONObject jsonPayHolder,
			String dtFormat) throws Exception {
		JSONArray jsonPayments = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jsonPayHolder.get("payments")));
		Iterator<?> itIter = jsonPayments.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();

			JSONObject jPayCurrency = (JSONObject) jPPObj.get("payCurrencyDTO");
			if (!BeanUtils.nullHandler(jPPObj.get("type")).equals("")) {
				lccClientPaymentHolder.addCardPayment(BeanUtils.nullHandler(jPPObj.get("carrierCode")),
						new Integer(BeanUtils.nullHandler(jPPObj.get("type"))).intValue(),
						BeanUtils.nullHandler(jPPObj.get("eDate")), BeanUtils.nullHandler(jPPObj.get("no")),
						BeanUtils.nullHandler(jPPObj.get("name")), BeanUtils.nullHandler(jPPObj.get("address")),
						BeanUtils.nullHandler(jPPObj.get("securityCode")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAmount"))),
						BeanUtils.nullHandler(jPPObj.get("totalAmountCurrencyCode")),
						DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("txnDateTime")), dtFormat),
						BeanUtils.nullHandler(jPPObj.get("authorizationId")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("payCurrencyAmount"))),
						BeanUtils.nullHandler(jPPObj.get("cardHoldersName")),
						BeanUtils.nullHandler(jPayCurrency.get("payCurrencyCode")), null,
						BeanUtils.nullHandler(jPPObj.get("originalPayReference")), null,
						BeanUtils.nullHandler(jPPObj.get("carrierVisePayments")),
						BeanUtils.nullHandler(jPPObj.get("lccUniqueTnxId")), BeanUtils.nullHandler(jPPObj.get("remarks")), null,
						null);
			} else if (BeanUtils.nullHandler(jPPObj.get("paymentMethod")).equals("CA")) {
				lccClientPaymentHolder.addCashPayment(BeanUtils.nullHandler(jPPObj.get("carrierCode")),
						BeanUtils.nullHandler(jPPObj.get("agentCode")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAmount"))),
						BeanUtils.nullHandler(jPPObj.get("totalAmountCurrencyCode")),
						DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("txnDateTime")), dtFormat),
						BeanUtils.nullHandler(jPPObj.get("agentName")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("payCurrencyAmount"))),
						BeanUtils.nullHandler(jPayCurrency.get("payCurrencyCode")), null,
						BeanUtils.nullHandler(jPPObj.get("originalPayReference")), null, null,
						BeanUtils.nullHandler(jPPObj.get("carrierVisePayments")),
						BeanUtils.nullHandler(jPPObj.get("lccUniqueTnxId")), BeanUtils.nullHandler(jPPObj.get("paymentMethod")),
						BeanUtils.nullHandler(jPPObj.get("remarks")), BeanUtils.nullHandler(jPPObj.get("dummyPayment")), null);
			} else if (!BeanUtils.nullHandler(jPPObj.get("agentCode")).equals("")) {
				lccClientPaymentHolder.addAgentCreditPayment(BeanUtils.nullHandler(jPPObj.get("carrierCode")),
						BeanUtils.nullHandler(jPPObj.get("agentCode")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAmount"))),
						BeanUtils.nullHandler(jPPObj.get("totalAmountCurrencyCode")),
						DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("txnDateTime")), dtFormat),
						BeanUtils.nullHandler(jPPObj.get("agentName")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("payCurrencyAmount"))),
						BeanUtils.nullHandler(jPayCurrency.get("payCurrencyCode")), null,
						BeanUtils.nullHandler(jPPObj.get("originalPayReference")), null, null,
						BeanUtils.nullHandler(jPPObj.get("carrierVisePayments")),
						BeanUtils.nullHandler(jPPObj.get("lccUniqueTnxId")), BeanUtils.nullHandler(jPPObj.get("paymentMethod")),
						BeanUtils.nullHandler(jPPObj.get("remarks")), null);
			} else if (BeanUtils.nullHandler(jPPObj.get("description")).equals("PREVIOUS CREDIT BF")
					|| BeanUtils.nullHandler(jPPObj.get("description")).equals("PREVIOUS CREDIT CF")) {
				lccClientPaymentHolder.addPaxCreditPayment(BeanUtils.nullHandler(jPPObj.get("carrierCode")), null,
						BeanUtils.nullHandler(jPPObj.get("description")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAmount"))),
						BeanUtils.nullHandler(jPPObj.get("totalAmountCurrencyCode")),
						DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("txnDateTime")), dtFormat),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("payCurrencyAmount"))),
						BeanUtils.nullHandler(jPayCurrency.get("payCurrencyCode")), null,
						BeanUtils.nullHandler(jPPObj.get("originalPayReference")), null,
						BeanUtils.nullHandler(jPPObj.get("carrierVisePayments")),
						BeanUtils.nullHandler(jPPObj.get("lccUniqueTnxId")), BeanUtils.nullHandler(jPPObj.get("remarks")), null);
			} else if (BeanUtils.nullHandler(jPPObj.get("paymentMethod")).equals("14")) {
				lccClientPaymentHolder.addLMSPayment(BeanUtils.nullHandler(jPPObj.get("carrierCode")),
						BeanUtils.nullHandler(jPPObj.get("loyaltyMemberAccountId")), null,
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAmount"))),
						BeanUtils.nullHandler(jPPObj.get("totalAmountCurrencyCode")),
						DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("txnDateTime")), dtFormat),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("payCurrencyAmount"))),
						BeanUtils.nullHandler(jPayCurrency.get("payCurrencyCode")), null,
						BeanUtils.nullHandler(jPPObj.get("originalPayReference")), null,
						BeanUtils.nullHandler(jPPObj.get("carrierVisePayments")), null,
						BeanUtils.nullHandler(jPPObj.get("remarks")), BeanUtils.nullHandler(jPPObj.get("originalPayReference")),
						BeanUtils.nullHandler(jPPObj.get("paymentMethod")),
						new Boolean(BeanUtils.nullHandler(jPPObj.get("nonRefundable"))));
			} else if (jPPObj.get("voucherDTO") != null) {
				lccClientPaymentHolder.addVoucherPayment(BeanUtils.nullHandler(jPPObj.get("carrierCode")),
						transformToVoucherDTO((JSONObject) jPPObj.get("voucherDTO")),
						BeanUtils.nullHandler(jPPObj.get("description")),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAmount"))),
						BeanUtils.nullHandler(jPPObj.get("totalAmountCurrencyCode")),
						DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("txnDateTime")), dtFormat),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("payCurrencyAmount"))),
						BeanUtils.nullHandler(jPayCurrency.get("payCurrencyCode")),
						BeanUtils.nullHandler(jPPObj.get("originalPayReference")),
						BeanUtils.nullHandler(jPPObj.get("originalPayReference")), null, null, null,
						BeanUtils.nullHandler(jPPObj.get("remarks")), BeanUtils.nullHandler(jPPObj.get("originalPayReference")));
			} else if (BeanUtils.nullHandler(jPPObj.get("description")).equals("offline")) {
				lccClientPaymentHolder.addOfflineCardPayment(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("totalAmount"))),
						new BigDecimal(BeanUtils.nullHandler(jPPObj.get("payCurrencyAmount"))),
						BeanUtils.nullHandler(jPPObj.get("totalAmountCurrencyCode")),
						BeanUtils.nullHandler(jPPObj.get("payReference")), BeanUtils.nullHandler(jPPObj.get("remarks")),
						BeanUtils.nullHandler(jPPObj.get("remarks")),
						DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("paymentTnxDateTime")), dtFormat));

			}
		}

	}

	private static void transformCredits(LCCClientPaymentHolder lccClientPaymentHolder, JSONObject jsonPayHolder, String dtFormat)
			throws Exception {
		JSONArray jsonCredits = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jsonPayHolder.get("credits")));
		Iterator<?> itIter = jsonCredits.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			String strmcCredit = "0.00";
			if (jPPObj.get("mcAmount") != null && !jPPObj.get("mcAmount").equals("null")) {
				strmcCredit = BeanUtils.nullHandler(jPPObj.get("mcAmount"));
			}
			boolean isNonRefundableCredit = false;
			if (jPPObj.get("nonRefundableCredit") != null && !jPPObj.get("nonRefundableCredit").equals("null")) {
				isNonRefundableCredit = new Boolean(BeanUtils.nullHandler(jPPObj.get("nonRefundableCredit"))).booleanValue();
			}
			lccClientPaymentHolder.addCredit(new BigDecimal(BeanUtils.nullHandler(jPPObj.get("amount"))),
					new BigDecimal(strmcCredit), new Integer(BeanUtils.nullHandler(jPPObj.get("creditId"))),
					BeanUtils.nullHandler(jPPObj.get("enumStatus")).equalsIgnoreCase(CreditInfoDTO.status.AVAILABLE.toString())
							? CreditInfoDTO.status.AVAILABLE
							: CreditInfoDTO.status.EXPIRED,
					DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("expireDate")), dtFormat),
					new Integer(BeanUtils.nullHandler(jPPObj.get("txnId"))), BeanUtils.nullHandler(jPPObj.get("userNote")),
					BeanUtils.nullHandler(jPPObj.get("carrierCode")), BeanUtils.nullHandler(jPPObj.get("currencyCode")), null,
					isNonRefundableCredit);
		}
	}

	public static boolean isGroupPnr(String groupPNR) {
		return (groupPNR != null && !groupPNR.trim().equals("") && !groupPNR.trim().equalsIgnoreCase("null"));
	}

	public static String createAjdustementSegmentOptions(List<LCCClientCarrierOndGroup> carrierGrouping,
			ReservationProcessParams rpParams) {
		String ownCarrerGroupingOptions = "<option></option>";
		String otherCarrerGroupingOptions = "";

		if (rpParams.isAdjustCharges()) {
			for (LCCClientCarrierOndGroup carrierOndGroup : carrierGrouping) {

				// Exchange Segment Adjustment will be handle by the Back end automatically, when there is a minus
				// adjustment,therefore exchanged segments will not displayed in UI drop down.
				if (ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(carrierOndGroup.getSubStatus())) {
					continue;
				}

				String segmentCode = carrierOndGroup.getSegmentCode();
				// Hide Stop Over Airport
				if (AppSysParamsUtil.isHideStopOverEnabled()) {
					if (segmentCode != null && segmentCode.split("/").length > 2) {
						segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
					}
				}

				// if user can adjust his own charges
				if (carrierOndGroup.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					ownCarrerGroupingOptions = ownCarrerGroupingOptions + "<option value='"
							+ carrierOndGroup.getCarrierOndGroupRPH() + "'>" + carrierOndGroup.getCarrierCode() + " - "
							+ segmentCode + " - " + carrierOndGroup.getStatus() + "</option>";

					// if user can adjust other's charges
				} else if (rpParams.isAdjustOtherCarrierCharges()) {
					otherCarrerGroupingOptions = otherCarrerGroupingOptions + "<option value='"
							+ carrierOndGroup.getCarrierOndGroupRPH() + "'>" + carrierOndGroup.getCarrierCode() + " - "
							+ segmentCode + " - " + carrierOndGroup.getStatus() + "</option>";

				}
			}
		}
		return ownCarrerGroupingOptions + otherCarrerGroupingOptions;
	}

	public static String getPaymentAgentOptions(HttpServletRequest request) throws ModuleException {
		String strAgOption = JavaScriptGenerator.createTAsWithCreditHtml(request);
		return strAgOption;
	}

	public static boolean isCharterAgent(HttpServletRequest request) throws ModuleException {
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userPrincipal.getAgentCode());

		return (agent.getCharterAgent() != null && Agent.CHARTER_AGENT_YES.equals(agent.getCharterAgent()));
	}

	public static boolean isCharterAgent(String agentCode) throws ModuleException {
		if (agentCode != null && !"".equals(agentCode)) {
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(agentCode);
			return (agent.getCharterAgent() != null && Agent.CHARTER_AGENT_YES.equals(agent.getCharterAgent()));
		} else {
			return false;
		}
	}

	public static Object getRefundAgentOptions(HttpServletRequest request) throws ModuleException {
		return JavaScriptGenerator.createRefundTAWithCreditHtml(request);
	}

	public static Object[] getDepArrZulutimes(Set<LCCClientReservationSegment> colsegs) {
		Object[] arr = new Object[2];
		Date tempDepZulu = null;
		Date tempArrZulu = null;

		for (LCCClientReservationSegment segment : SortUtil.sort(colsegs)) {
			if (segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				if (tempDepZulu == null || tempDepZulu.after(segment.getDepartureDate())) {
					tempDepZulu = segment.getDepartureDate();
				}
				if (tempArrZulu == null || tempArrZulu.before(segment.getArrivalDate())) {
					tempArrZulu = segment.getArrivalDate();
				}
			}
		}
		arr[0] = tempDepZulu;
		arr[1] = tempArrZulu;

		return arr;

	}

	public static PaymentSummaryTO transformJsonPaymentSummary(String resPaySummary) throws Exception {
		PaymentSummaryTO paySummary = new PaymentSummaryTO();

		if (resPaySummary != null) {
			JSONObject jsonPaySumObject = (JSONObject) new JSONParser().parse(resPaySummary);

			paySummary.setBalanceToPay(BeanUtils.nullHandler(jsonPaySumObject.get("balanceToPay")));
			if (jsonPaySumObject.get("balanceToPaySelcur") != null) {
				paySummary.setBalanceToPaySelcur(BeanUtils.nullHandler(jsonPaySumObject.get("balanceToPaySelcur")));
			} else {
				paySummary.setBalanceToPaySelcur(null);
			}

			paySummary.setCurrency(BeanUtils.nullHandler(jsonPaySumObject.get("currency")));
			paySummary.setHandlingCharges(BeanUtils.nullHandler(jsonPaySumObject.get("handlingCharges")));
			paySummary.setModificationCharges(BeanUtils.nullHandler(jsonPaySumObject.get("modificationCharges")));
			paySummary.setPaid(BeanUtils.nullHandler(jsonPaySumObject.get("paid")));
			if (jsonPaySumObject.get("selectedCurrency") != null) {
				paySummary.setSelectedCurrency(BeanUtils.nullHandler(jsonPaySumObject.get("selectedCurrency")));
			} else {
				paySummary.setSelectedCurrency(null);
			}
			paySummary.setTicketPrice(BeanUtils.nullHandler(jsonPaySumObject.get("ticketPrice")));
			paySummary.setTotalFare(BeanUtils.nullHandler(jsonPaySumObject.get("totalFare")));
			paySummary.setTotalTaxSurCharges(BeanUtils.nullHandler(jsonPaySumObject.get("totalTaxSurCharges")));
		}
		return paySummary;
	}

	public static CommonReservationContactInfo transformJsonContactInfo(String resContactInfo) throws Exception {
		CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
		JSONObject jsonPaySumObject = (JSONObject) new JSONParser().parse(resContactInfo);

		contactInfo.setTitle(BeanUtils.nullHandler(jsonPaySumObject.get("title")));
		contactInfo.setFirstName(BeanUtils.nullHandler(jsonPaySumObject.get("firstName")));
		contactInfo.setLastName(BeanUtils.nullHandler(jsonPaySumObject.get("lastName")));

		contactInfo.setCity(BeanUtils.nullHandler(jsonPaySumObject.get("city")));

		if (jsonPaySumObject.get("countryCode") != null) {
			contactInfo.setCountryCode(BeanUtils.nullHandler(jsonPaySumObject.get("countryCode")));
		} else if (jsonPaySumObject.get("country") != null) {
			contactInfo.setCountryCode(BeanUtils.nullHandler(jsonPaySumObject.get("country")));
		} else {
			contactInfo.setCountryCode("");
		}

		if (jsonPaySumObject.get("customerId") != null) {
			contactInfo.setCustomerId(new Integer(BeanUtils.nullHandler(jsonPaySumObject.get("customerId"))));
		} else {
			contactInfo.setCustomerId(null);
		}

		if (jsonPaySumObject.get("email") != null) {
			contactInfo.setEmail(BeanUtils.nullHandler(jsonPaySumObject.get("email")));
		} else {
			contactInfo.setEmail(null);
		}

		String faxNo = BeanUtils.nullHandler(jsonPaySumObject.get("fax"));

		if (faxNo.equals("")) {
			faxNo = BeanUtils.nullHandler(jsonPaySumObject.get("faxNo"));
			if (!faxNo.equals("")) {
				contactInfo.setFax(BeanUtils.nullHandler(jsonPaySumObject.get("faxCountry")) + "-"
						+ BeanUtils.nullHandler(jsonPaySumObject.get("faxArea")) + "-" + faxNo);
			} else {
				contactInfo.setFax("");
			}
		} else {
			contactInfo.setFax(faxNo);
		}

		String mobileNo = BeanUtils.nullHandler(jsonPaySumObject.get("mobileNo"));

		if (mobileNo.equals("")) {
			contactInfo.setMobileNo("");
		} else {
			if (!BeanUtils.nullHandler(jsonPaySumObject.get("mobileCountry")).equals("")
					&& !BeanUtils.nullHandler(jsonPaySumObject.get("mobileArea")).equals("")) {
				contactInfo.setMobileNo(BeanUtils.nullHandler(jsonPaySumObject.get("mobileCountry")) + "-"
						+ BeanUtils.nullHandler(jsonPaySumObject.get("mobileArea")) + "-" + mobileNo);
			} else {
				contactInfo.setMobileNo(mobileNo);
			}
		}

		String phoneNo = BeanUtils.nullHandler(jsonPaySumObject.get("phoneNo"));

		if (phoneNo.equals("")) {
			contactInfo.setPhoneNo("");
		} else {
			if (!BeanUtils.nullHandler(jsonPaySumObject.get("phoneCountry")).equals("")
					&& !BeanUtils.nullHandler(jsonPaySumObject.get("phoneArea")).equals("")) {
				contactInfo.setPhoneNo(BeanUtils.nullHandler(jsonPaySumObject.get("phoneCountry")) + "-"
						+ BeanUtils.nullHandler(jsonPaySumObject.get("phoneArea")) + "-" + phoneNo);
			} else {
				contactInfo.setPhoneNo(phoneNo);
			}
		}

		if (!BeanUtils.nullHandler(jsonPaySumObject.get("nationality")).equals("")) {
			contactInfo.setNationality(BeanUtils.nullHandler(jsonPaySumObject.get("nationality")));
			contactInfo.setNationalityCode(Integer.parseInt(BeanUtils.nullHandler(jsonPaySumObject.get("nationality"))));
		} else {
			contactInfo.setNationality(null);
		}

		if (jsonPaySumObject.get("nationalityCode") != null) {
			contactInfo.setNationalityCode(new Integer(BeanUtils.nullHandler(jsonPaySumObject.get("nationalityCode"))));
		} else {
			contactInfo.setNationalityCode(null);
		}

		contactInfo.setState(BeanUtils.nullHandler(jsonPaySumObject.get("state")));

		if (jsonPaySumObject.get("streetAddress1") != null) {
			contactInfo.setStreetAddress1(BeanUtils.nullHandler(jsonPaySumObject.get("streetAddress1")));
		} else if (jsonPaySumObject.get("street") != null) {
			contactInfo.setStreetAddress1(BeanUtils.nullHandler(jsonPaySumObject.get("street")));
		} else {
			contactInfo.setStreetAddress1("");
		}

		if (jsonPaySumObject.get("streetAddress2") != null) {
			contactInfo.setStreetAddress2(BeanUtils.nullHandler(jsonPaySumObject.get("streetAddress2")));
		} else if (jsonPaySumObject.get("address") != null) {
			contactInfo.setStreetAddress2(BeanUtils.nullHandler(jsonPaySumObject.get("address")));
		} else {
			contactInfo.setStreetAddress2("");
		}

		contactInfo.setZipCode(BeanUtils.nullHandler(jsonPaySumObject.get("zipCode")));

		contactInfo.setPreferredLanguage(BeanUtils.nullHandler(jsonPaySumObject.get("preferredLang")));

		contactInfo.setTaxRegNo(BeanUtils.nullHandler(jsonPaySumObject.get("taxRegNo")));

		// Set Emergency Contact
		contactInfo.setEmgnTitle(BeanUtils.nullHandler(jsonPaySumObject.get("emgnTitle")));
		contactInfo.setEmgnFirstName(BeanUtils.nullHandler(jsonPaySumObject.get("emgnFirstName")));
		contactInfo.setEmgnLastName(BeanUtils.nullHandler(jsonPaySumObject.get("emgnLastName")));

		String emgnPhoneNo = BeanUtils.nullHandler(jsonPaySumObject.get("emgnPhoneNo"));
		if (emgnPhoneNo.equals("")) {
			contactInfo.setEmgnPhoneNo("");
		} else {
			contactInfo.setEmgnPhoneNo(BeanUtils.nullHandler(jsonPaySumObject.get("emgnPhoneCountry")) + "-"
					+ BeanUtils.nullHandler(jsonPaySumObject.get("emgnPhoneArea")) + "-" + emgnPhoneNo);
		}

		contactInfo.setEmgnEmail(BeanUtils.nullHandler(jsonPaySumObject.get("emgnEmail")));

		return contactInfo;
	}

	public static Map<Integer, BigDecimal> getPaxCreditMap(Set<LCCClientReservationPax> passengers) {
		Map<Integer, BigDecimal> paxCreditMap = new HashMap<Integer, BigDecimal>();

		for (LCCClientReservationPax pax : passengers) {
			paxCreditMap.put(pax.getPaxSequence(), pax.getTotalAvailableBalance().negate());
		}

		return paxCreditMap;
	}

	public static boolean setFlexiInformation(String action, String fltSegId, StringBuffer flexiInfo, String flexiAlerts)
			throws Exception {
		if (flexiAlerts != null && !flexiAlerts.equals("") && !flexiAlerts.equals("[]")) {
			JSONArray jsonFlexiArray = (JSONArray) new JSONParser().parse(flexiAlerts);
			if (action.equals(WebConstants.ACTION_CANCEL_SEGMENT) || action.equals(WebConstants.ACTION_RES_CANCEL)
					|| action.equals(WebConstants.ACTION_REMOVE_PAX)) {
				String FLEXI_MSG = "";
				if (action.equals(WebConstants.ACTION_CANCEL_SEGMENT)) {
					JSONArray jsonFlexiAlertsArray = getFlightSegmentFlexiAlerts(jsonFlexiArray, fltSegId);
					if (jsonFlexiAlertsArray != null) {
						FLEXI_MSG = "No more flexibilities available for the cancelled segment";
					} else {
						return false;
					}
				} else if (action.equals(WebConstants.ACTION_RES_CANCEL)) {
					FLEXI_MSG = "No more flexibilities available for the cancelled reservation";
				} else if (action.equals(WebConstants.ACTION_REMOVE_PAX)) {
					FLEXI_MSG = "No more flexibilities available for the removed pax";
				}

				flexiInfo.append(FLEXI_MSG);
				return true;// No need the remaining flexibilities. Just return
							// true
			}

			if (action.equals(WebConstants.ACTION_MODIFY_SEGMENT)) {
				if (jsonFlexiArray != null) {
					JSONArray jsonFlexiAlertsArray = getFlightSegmentFlexiAlerts(jsonFlexiArray, fltSegId);
					if (jsonFlexiAlertsArray != null) {
						for (Iterator<?> alertsIterator = jsonFlexiAlertsArray.iterator(); alertsIterator.hasNext();) {
							JSONObject jsonAlertObject = (JSONObject) alertsIterator.next();
							if (BeanUtils.nullHandler(jsonAlertObject.get("alertId")).equals("1")) {
								// Modification
								int remaining = new Integer(BeanUtils.nullHandler(jsonAlertObject.get("content"))).intValue() - 1;
								if (remaining > 0) {
									flexiInfo.append(remaining + " Modification(s), ");
								}
							} else if (BeanUtils.nullHandler(jsonAlertObject.get("alertId")).equals("2")) {
								flexiInfo.append(BeanUtils.nullHandler(jsonAlertObject.get("content")) + " Cancellation");
							}
						}
						return true;
					}
				}
			}
		}
		return false;
	}

	private static JSONArray getFlightSegmentFlexiAlerts(JSONArray jsonFlexiArray, String fltSegId)
			throws org.json.simple.parser.ParseException {

		if (jsonFlexiArray != null) {
			for (Iterator<?> iterator = jsonFlexiArray.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				if ((fltSegId.split("\\$")[0]).equals(BeanUtils.nullHandler(jsonObject.get("flightSegmantRefNumber")))) {
					return (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jsonObject.get("alertTO")));
				}
			}
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public static boolean isRAKInsuranceModificationAllowed(LCCClientReservation reservation,
			List<FlightSegmentTO> flightSegmentTOs, LCCInsuredJourneyDTO insJrnyDto) throws ModuleException {
		Collection<FlightInfoTO> flightDepDetails = loadFlightSegments(reservation, null);
		if (flightDepDetails != null) {

			if (isAllowedInsPurchaseOnline(flightDepDetails, flightSegmentTOs, insJrnyDto)) {
				for (FlightInfoTO flightSeg : flightDepDetails) {
					int modificationCutover = Integer.parseInt(AppSysParamsUtil.getRakModificationCutoverTime());
					GregorianCalendar calendar2 = new GregorianCalendar();
					calendar2.setTime(new Date(flightSeg.getDepartureDateLong()));
					calendar2.add(GregorianCalendar.MINUTE, -modificationCutover);
					if (calendar2.getTimeInMillis() >= new Date().getTime()) {
						return true;
					}
				}
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * Helper method to determine the duration of the trip
	 * 
	 * @param flightDepDetails
	 * @return
	 */
	private static boolean isAllowedInsPurchaseOnline(Collection<FlightInfoTO> flightDepDetails,
			List<FlightSegmentTO> flightSegmentTOs, LCCInsuredJourneyDTO insJrnyDto) {
		boolean isAllowed = false;
		FlightInfoTO startSeg = null;
		FlightInfoTO endSeg = null;
		boolean isRoundTrip = false;
		for (FlightInfoTO flightSeg : flightDepDetails) {
			if (flightSeg.isReturnFlag()) {
				isRoundTrip = true;
			}
		}
		if (isRoundTrip) {
			// Set starting segment from the loaded reservation's segments
			for (FlightInfoTO flightSeg : flightDepDetails) {
				if (startSeg == null || startSeg.getDepartureDateLong() > flightSeg.getDepartureDateLong()) {
					startSeg = flightSeg;
				}
			}
			// Set the ending segment from the modified segments
			if (flightSegmentTOs != null) {
				for (FlightSegmentTO flightSeg : flightSegmentTOs) {
					if (endSeg == null || endSeg.getDepartureDateLong() < flightSeg.getDepartureDateTime().getTime()) {
						FlightInfoTO endSegInfo = new FlightInfoTO();
						endSegInfo.setDepartureDateLong(flightSeg.getDepartureDateTime().getTime());
						endSeg = endSegInfo;
					}
				}
			} else {
				FlightInfoTO endSegInfo = new FlightInfoTO();
				endSegInfo.setDepartureDateLong(insJrnyDto.getJourneyEndDate().getTime());
				endSeg = endSegInfo;
			}
			long dateDifferenceInMillis = endSeg.getDepartureDateLong() - startSeg.getDepartureDateLong();
			long diffrenceInDays = (dateDifferenceInMillis / (1000 * 60 * 60 * 24)) + 1;
			isAllowed = diffrenceInDays < 91;
		} else {
			isAllowed = true;
		}
		return isAllowed;
	}

	public static HashMap<String, List<AllFaresInformationDTO>> getAllFaresInformationDTO(FareAvailRS fareAvailRS,
			String selectedFltRefNo, boolean allowOverrideOnHold, boolean modifyBookingToLowCos,
			List<String> allowedReturnFareCOSList, Set<Integer> commissionApplicableFareIds, boolean hideCommisionInfro,
			boolean showFlownFltClosedBC) {
		HashMap<String, List<AllFaresInformationDTO>> allFaresInfoDTOsMap = new HashMap<String, List<AllFaresInformationDTO>>();
		for (SegmentInvFareTO segInvFare : fareAvailRS.getSegmentInvFares()) {
			List<AllFaresInformationDTO> allFareInfoDTOs = new ArrayList<AllFaresInformationDTO>();

			for (InvFareTypeTO invFareType : segInvFare.getInvetoryFareTypes()) {
				AllFaresInformationDTO allFaresInfoDTO = new AllFaresInformationDTO();
				List<InvAllocationDTO> invAllocInfo = new ArrayList<InvAllocationDTO>();
				List<FareRuleDTO> fareRuleInfo = new ArrayList<FareRuleDTO>();
				HashSet<String> bcSet = new HashSet<String>();
				boolean isReturnFare = false;
				String strONDCode = invFareType.getFareOndCode();
				// FIXME display specific behaviour should not be in JAVA!!
				String strONDCodeForDisplay = "";
				if (invFareType.getFareType() == FareTypes.HALF_RETURN_FARE) {
					strONDCodeForDisplay = "/PRORATED RETURN";
				} else if (invFareType.getFareType() == FareTypes.RETURN_FARE) {
					strONDCodeForDisplay = "/RETURN";
					isReturnFare = true;
				}
				if (AppSysParamsUtil.isHideStopOverEnabled() && strONDCode != null && strONDCode.split("/").length > 2)
					strONDCode = ReservationApiUtils.hideStopOverSeg(strONDCode);

				allFaresInfoDTO.setDisplayCodeForFareType(strONDCode + strONDCodeForDisplay);
				allFaresInfoDTO.setFareType(invFareType.getFareType().toString());
				for (InvFareAllocTO invFareAlloc : invFareType.getInvFareAllocations()) {
					// FIXME display specific behaviour should not be in JAVA!!
					String strFlag = "Non Std.";
					if (invFareAlloc.getBcType() == BCType.FIXED) {
						strFlag = "Fixed";
					}
					if (invFareAlloc.getBcType() == BCType.STD) {
						strFlag = "Std.";
					}
					InvAllocationDTO invAllocationDTO = new InvAllocationDTO();
					invAllocationDTO.setBookingCode(invFareAlloc.getBookingCode());
					invAllocationDTO.setCcCode(invFareAlloc.getCcCode());

					// user required additional prev to modify to lower rank
					// return fare
					if (isReturnFare && !modifyBookingToLowCos && (allowedReturnFareCOSList.size() > 0
							&& !allowedReturnFareCOSList.contains(invFareAlloc.getCcCode()))) {
						continue;
					}

					if (invFareAlloc.isFlownOnd()
							&& ReservationInternalConstants.BookingClassAllocStatus.CLOSE.equals(invFareAlloc.getBcInvStatus())
							&& !showFlownFltClosedBC) {
						continue;
					}

					invAllocationDTO.setLogicalCabinClass(invFareAlloc.getLogicalCabinClass());
					invAllocationDTO.setBcType(strFlag);
					invAllocationDTO.setBcInvStatus(invFareAlloc.getBcInvStatus());
					invAllocationDTO.setAllocatedSeats(invFareAlloc.getAllocatedSeats());
					invAllocationDTO.setSoldSeats(invFareAlloc.getSoldSeats());
					invAllocationDTO.setOnholdSeats(invFareAlloc.getOnholdSeats());
					invAllocationDTO.setAvailableMaxSeats(invFareAlloc.getAvailableMaxSeats());
					invAllocationDTO.setAvailableSeats(invFareAlloc.getAvailableSeats());
					invAllocationDTO.setOnholdRestricted(allowOverrideOnHold ? "N" : invFareAlloc.getOnholdRestricted());
					invAllocationDTO.setSameBookingClassBeingSearch(invFareAlloc.isSameBookingClassBeingSearch());
					invAllocationDTO.setBookedFlightCabinBeingSearched(invFareAlloc.isBookedFlightCabinBeingSearched());
					invAllocationDTO.setFlownOnd(invFareAlloc.isFlownOnd());
					invAllocationDTO.setAvailableNestedSeats(invFareAlloc.getAvailableNestedSeats());
					invAllocationDTO.setCommonAllSegments(invFareAlloc.isCommonAllSegments());
					invAllocationDTO.setNestRank(invFareAlloc.getNestRank());
					invAllocationDTO.setActualSeatsSold(invFareAlloc.getActualSeatsSold());
					invAllocationDTO.setActualSeatsOnHold(invFareAlloc.getActualSeatsOnHold());

					for (FareRuleFareDTO fareRule : invFareAlloc.getFareRuleFares()) {
						if (segInvFare.getFlightRefNumber().equals(selectedFltRefNo)
								|| !bcSet.contains(fareRule.getBookingClassCode())) {
							bcSet.add(invFareAlloc.getBookingCode());
							FareRuleDTO fareRuleDTO = new FareRuleDTO();
							fareRuleInfo.add(fareRuleDTO);
							invAllocInfo.add(invAllocationDTO);

							fareRuleDTO.setOrignNDest(strONDCode);
							fareRuleDTO.setFareRuleCode(fareRule.getFareRuleCode());
							fareRuleDTO.setFareBasisCode(fareRule.getFareBasisCode());
							fareRuleDTO.setBookingClassCode(fareRule.getBookingClassCode());
							fareRuleDTO.setCabinClassCode(invFareAlloc.getCcCode());
							fareRuleDTO.setAdultFareAmount(fareRule.getAdultFareAmount());
							fareRuleDTO.setChildFareAmount(fareRule.getChildFareAmount());
							fareRuleDTO.setInfantFareAmount(fareRule.getInfantFareAmount());
							fareRuleDTO.setAdultFareApplicable(fareRule.isAdultFareApplicable());
							fareRuleDTO.setChildFareApplicable(fareRule.isChildFareApplicable());
							fareRuleDTO.setInfantFareApplicable(fareRule.isInfantFareApplicable());
							fareRuleDTO.setFareId(fareRule.getFareId());
							fareRuleDTO.setFareRuleId(fareRule.getFareRuleId());

							Integer fareId = fareRule.getFareId();
							if (!hideCommisionInfro || fareId != null && commissionApplicableFareIds != null
									&& !commissionApplicableFareIds.isEmpty() && commissionApplicableFareIds.contains(fareId)) {
								fareRuleDTO.setApplicableAgentCommission(fareRule.getApplicableAgentCommission());
							} else {
								fareRuleDTO.setApplicableAgentCommission("0.00");
							}

							// FIXME display specific behaviour should not be in
							// JAVA!!
							String strVisibleAgents = "";
							if (fareRule.hasVisibleAgents()) {
								for (String agent : fareRule.getVisibleAgents()) {
									if (!strVisibleAgents.equals("")) {
										strVisibleAgents += "<br>";
									}
									strVisibleAgents += agent;
								}
							} else {
								strVisibleAgents = "&nbsp;";
							}
							fareRuleDTO.setVisibleAgents(strVisibleAgents);

							String strVisibleChannels = "";
							if (fareRule.getVisibleChannelNames() != null) {
								for (String channel : fareRule.getVisibleChannelNames()) {
									if (!strVisibleChannels.equals("")) {
										strVisibleChannels += "<br>";
									}
									strVisibleChannels += channel;
								}
							} else {
								strVisibleChannels = "&nbsp;";
							}
							fareRuleDTO.setVisibleChannelNames(strVisibleChannels);
						}
					}
				}
				allFaresInfoDTO.setInvAllocationInfo(invAllocInfo);
				allFaresInfoDTO.setFareRuleInfo(fareRuleInfo);

				if (invAllocInfo != null && invAllocInfo.size() > 0)
					allFareInfoDTOs.add(allFaresInfoDTO);

			}
			allFaresInfoDTOsMap.put(segInvFare.getFlightRefNumber(), allFareInfoDTOs);
		}
		return allFaresInfoDTOsMap;
	}

	public static void checkInvSuffiencyInAllSegments(HashMap<String, List<AllFaresInformationDTO>> allFaresInfoDTOsMap,
			List<AllFaresInformationDTO> selectedFltFaresInfo, int numberOfSeats) {

		Map<String, List<InvAllocationDTO>> invAllocationDTOsMap = new HashMap<String, List<InvAllocationDTO>>();
		for (Entry<String, List<AllFaresInformationDTO>> allFaresInformationDTOs : allFaresInfoDTOsMap.entrySet()) {
			for (AllFaresInformationDTO allFaresInformationDTO : allFaresInformationDTOs.getValue()) {
				if (Integer.parseInt(allFaresInformationDTO.getFareType()) == FareTypes.RETURN_FARE) {
					invAllocationDTOsMap.put(allFaresInformationDTOs.getKey(), allFaresInformationDTO.getInvAllocationInfo());
				}
			}
		}
		for (AllFaresInformationDTO selectedFltFaresInformationDTO : selectedFltFaresInfo) {
			if (Integer.parseInt(selectedFltFaresInformationDTO.getFareType()) == FareTypes.RETURN_FARE) {
				for (InvAllocationDTO selectedFltInvAllocationDTO : selectedFltFaresInformationDTO.getInvAllocationInfo()) {
					for (List<InvAllocationDTO> otherFltInvAllocationDTOs : invAllocationDTOsMap.values()) {
						for (InvAllocationDTO otherFltInvAllocationDTO : otherFltInvAllocationDTOs) {
							if (selectedFltInvAllocationDTO.getBookingCode().equals(otherFltInvAllocationDTO.getBookingCode())) {
								if (otherFltInvAllocationDTO.getAvailableNestedSeats() < numberOfSeats) {
									selectedFltInvAllocationDTO.setCommonAllSegments(false);
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Check weather foid Id is unique for the Segments.
	 * 
	 * @return foid id which are duplicate if foid id is unique for the segment.
	 * @throws ModuleException
	 */
	public static String getUniqueFoidData(String foidIds, String foidCode, Integer[] segmentList, Log log, String pnr) {

		String strReturnData = "true";
		try {
			if (foidIds.length() > 0 && foidIds != null && segmentList.length > 0 && segmentList[0] != null) {
				String[] idListStr = foidIds.split("\\^");
				Integer[] idList = null;
				if (idListStr != null && idListStr.length > 0) {
					idList = new Integer[idList.length];
					int i = 0;
					for (String str : idListStr) {
						idList[i] = Integer.parseInt(idListStr[i]);
						i++;
					}
				}
				Integer dept = segmentList[0];
				Integer arr = 0;
				if (segmentList.length == 2) {
					arr = segmentList[1];
				}
				Collection<Integer[]> duplicateFoidIds = null;
				if (idList != null && idList.length > 0 && foidCode != null) {
					duplicateFoidIds = SelectListGenerator.getDuplicateFoidIdList(foidCode, dept, arr, idList, pnr);
					if (duplicateFoidIds != null && !duplicateFoidIds.isEmpty()) {
						strReturnData = "";
						for (Iterator iter = duplicateFoidIds.iterator(); iter.hasNext();) {
							String str[] = (String[]) iter.next();
							for (int i = 0; i < str.length; i++) {
								strReturnData += str[i] + " ";
							}

						}
					}
				} else {
					throw new Exception("Error retriving unique foid ids inside xbe");
				}

			}
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug("Error while validating unique FOID Id : ");
			}
		}
		return strReturnData;
	}

	/**
	 * This method is used only for get the operating carrier of dry reservation. So, getting first segment carrier code
	 * is enough for that. This should not use for interline bookings.
	 * 
	 * @param colResSegs
	 * @return
	 */
	public static String getDryOperatingCarrier(Collection<LCCClientReservationSegment> colResSegs) {
		String carrier = null;
		for (LCCClientReservationSegment resSeg : colResSegs) {
			carrier = AppSysParamsUtil.extractCarrierCode(resSeg.getFlightNo());
			return carrier;
		}
		return carrier;
	}

	public static boolean isFlexiSelected(String jsonFlexiSelection) {
		if (jsonFlexiSelection != null && (!"".equals(jsonFlexiSelection) || !"null".equals(jsonFlexiSelection))) {
			Map<Integer, Boolean> ondFlexiSelection = WebplatformUtil
					.convertMap(JSONFETOParser.parseMap(HashMap.class, String.class, jsonFlexiSelection));
			for (Entry<Integer, Boolean> flexiEntry : ondFlexiSelection.entrySet()) {
				if (flexiEntry.getValue()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Create a map of LCCClientReservationSegments for easy access
	 * 
	 * @param setReservationSegments
	 * @return
	 */
	private static Map<Integer, LCCClientReservationSegment>
			createMapedLCCReservationSegments(Set<LCCClientReservationSegment> setReservationSegments) {
		Map<Integer, LCCClientReservationSegment> mappedSegs = new HashMap<Integer, LCCClientReservationSegment>();
		for (Iterator<LCCClientReservationSegment> iterator = setReservationSegments.iterator(); iterator.hasNext();) {
			LCCClientReservationSegment clientReservationSegment = iterator.next();
			mappedSegs.put(clientReservationSegment.getPnrSegID(), clientReservationSegment);
		}
		return mappedSegs;
	}

	/**
	 * This method is responsible for following if the own booking use other carrier pax credits 1. Creating the group
	 * reservation in the LCC 2. Creating the Dummy reservation(s) in the respective carriers (For payable) 3. Track at
	 * interline payment transactions
	 * 
	 * @param pnr
	 * @param trackInfo
	 *            TODO
	 * @throws ModuleException
	 */
	public static void reconcileDummyCarrierReservation(String pnr, TrackInfoDTO trackInfo) throws ModuleException {

		String defaultCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, true, null, null, false, defaultCarrierCode, defaultCarrierCode,
				null, false);
		ModuleServiceLocator.getAirproxyReservationBD().reconcileDummyCarrierReservation(pnrModesDTO, trackInfo);
	}

	public static void updateReleaseTimeForDummyBooking(String pnr, Date releaseTimeStamp, TrackInfoDTO trackInfo)
			throws ModuleException {
		ModuleServiceLocator.getAirproxyReservationBD().updateReleaseTimeForDummyReservation(pnr, releaseTimeStamp);
	}

	public static void updateTransferSegmentData(FlightAvailRQ flightAvailRQ, boolean isGroupPNR, String modifyingSegOpCarrier) {
		if (!isGroupPNR) {
			flightAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.AA);
		}
		// we should only do the search in the same carrier for dry reservation
		// transfer segment.
		// boolean isDryReservation =
		// "true".equals(request.getParameter("isDryReservation"));
		if (StringUtils.isNotEmpty(modifyingSegOpCarrier) && !"null".equals(modifyingSegOpCarrier)) {
			List<String> participatingAirlines = new ArrayList<String>();
			participatingAirlines.add(modifyingSegOpCarrier);
			flightAvailRQ.getAvailPreferences().setParticipatingCarriers(participatingAirlines);
		}
	}

	public static String[] populateActualOriginAndDestination(LCCClientReservation lccReservation) {
		// assumes only confirmed bookings and not considering complex
		// combinations, ideally all the segments should be
		// considered to find the actual origin and destination
		List<LCCClientReservationSegment> outBoundResSegs = new ArrayList<LCCClientReservationSegment>();
		for (LCCClientReservationSegment segment : lccReservation.getSegments()) {
			if (segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
					&& segment.getReturnFlag().equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE)) {
				outBoundResSegs.add(segment);
			}
		}
		Collections.sort(outBoundResSegs);
		StringBuilder actualOrigins = new StringBuilder();
		StringBuilder actualDestinations = new StringBuilder();
		if (outBoundResSegs.size() > 0) {
			for (LCCClientReservationSegment outBoundResSeg : outBoundResSegs) {
				actualOrigins.append(outBoundResSeg.getSegmentCode().split("/")[0]);
				actualOrigins.append(",");
				actualDestinations.append(outBoundResSeg.getSegmentCode().split("/")[1]);
				actualDestinations.append(",");
			}
		}
		return new String[] { actualOrigins.toString(), actualDestinations.toString() };
	}

	public static void addLccSegments(LCCClientReservation lccClientReservation,
			Collection<LCCClientReservationSegment> lccSegments) {
		for (LCCClientReservationSegment lccSegment : lccSegments) {
			lccClientReservation.addSegment(lccSegment);
		}
	}

	/**
	 * @param reservationSegments
	 * @return
	 */
	public static int getNoOfCnfSegments(Collection<LCCClientReservationSegment> reservationSegments) {
		int noOfSegments = 0;
		if (reservationSegments != null) {
			for (LCCClientReservationSegment segment : reservationSegments) {
				if (!segment.getStatus().equalsIgnoreCase(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					noOfSegments++;
				}
			}
		}

		return noOfSegments;

	}

	/**
	 * When confirming an open return segment(s) it will be confirmed to the out bound booking class / fare rule
	 * combination.
	 * 
	 * @param availRQ
	 * @param pnr
	 * @throws ModuleException
	 */
	public static void setOpenReturnConfirmInfo(BaseAvailRQ availRQ, String pnr, TrackInfoDTO trackInfo) throws ModuleException {

		if (pnr != null && pnr.length() > 0) {
			Collection<FlightSegmentDTO> modifyingSegList = availRQ.getAvailPreferences().getModifiedSegmentsList();
			Collection<String> modifyingSegIds = new ArrayList<String>();
			for (FlightSegmentDTO fSegDTO : modifyingSegList) {
				modifyingSegIds.add(String.valueOf(fSegDTO.getSegmentId()));
			}
			Map<String, Map<String, List<Integer>>> mapBookingCodeAndCabinClass = ModuleServiceLocator.getAirproxyReservationBD()
					.getBookingClassDataForOpenReturn(modifyingSegIds, pnr, trackInfo);
			availRQ.getTravelPreferences().setBookingClassCode(BeanUtils.getFirstElement(mapBookingCodeAndCabinClass.keySet()));
			availRQ.getOriginDestinationInformation(OndSequence.OUT_BOUND)
					.setPreferredBookingClass(BeanUtils.getFirstElement(mapBookingCodeAndCabinClass.keySet()));
			ClassOfServicePreferencesTO cosPreferenceTO = new ClassOfServicePreferencesTO();
			cosPreferenceTO.setCabinClassSelection(BeanUtils.getFirstElement(mapBookingCodeAndCabinClass.values()));

			Date lastArrivalOfOB = null;
			for (FlightSegmentDTO fltSeg : availRQ.getAvailPreferences().getInverseSegmentsList()) {
				if (lastArrivalOfOB == null || lastArrivalOfOB.after(fltSeg.getArrivalDateTime())) {
					lastArrivalOfOB = fltSeg.getArrivalDateTime();
				}
			}

			if (availRQ.getOriginDestinationInformation(0).getDepartureDateTimeEnd().compareTo(lastArrivalOfOB) <= 0) {
				throw new ModuleException("error.openrt.confirm.before.outbound.notallowed");
			}
			lastArrivalOfOB = new Date(lastArrivalOfOB.getTime() + AppSysParamsUtil.getMinimumReturnTransitTimeInMillis());

			if (availRQ.getOriginDestinationInformation(0).getDepartureDateTimeStart().before(lastArrivalOfOB)) {
				availRQ.getOriginDestinationInformation(0).setDepartureDateTimeStart(lastArrivalOfOB);
			}
		}

	}

	public static List<LCCClientReservationPax> populatePaxAdults(List<AdultPaxTO> adultPaxTOList) throws ParseException {
		List<LCCClientReservationPax> paxAdults = new ArrayList<LCCClientReservationPax>();
		for (AdultPaxTO adultPaxTO : adultPaxTOList) {
			LCCClientReservationPax adultLccPax = new LCCClientReservationPax();

			adultLccPax.setTravelerRefNumber(adultPaxTO.getDisplayPaxTravelReference());
			adultLccPax.setPaxSequence(PaxTypeUtils.getPaxSeq(adultPaxTO.getDisplayPaxTravelReference()));
			adultLccPax.setPaxType(adultPaxTO.getDisplayAdultType());
			if (adultPaxTO.getDisplayAdultTitle() != null && !adultPaxTO.getDisplayAdultTitle().equals("")
					&& !adultPaxTO.getDisplayAdultTitle().equals("null")) {
				adultLccPax.setTitle(adultPaxTO.getDisplayAdultTitle());
			} else {
				adultLccPax.setTitle("");
			}
			adultLccPax.setFirstName(adultPaxTO.getDisplayAdultFirstName());
			adultLccPax.setLastName(adultPaxTO.getDisplayAdultLastName());
			if (adultPaxTO.getDisplayAdultNationality() != null && !adultPaxTO.getDisplayAdultNationality().equals("")) {
				adultLccPax.setNationalityCode(Integer.valueOf(adultPaxTO.getDisplayAdultNationality()));
			}

			LCCClientReservationAdditionalPax addnInfo = new LCCClientReservationAdditionalPax();

			String foidNum = "";
			Date foidExpiry = null;
			String foidPlace = "";
			String arrivalIntlFlightNo = "";
			String intlFlightArrivalDate = "";
			String intlFlightArrivalTime = "";
			String departureIntlFlightNo = "";
			String intlFlightDepartureDate = "";
			String intlFlightDepartureTime = "";
			Date internationalFlightArrivalDate = null;
			Date internationalFlightDepartureDate = null;
			String pnrPaxGroupId = "";
			String ffid = "";
			String nationalIDNo = "";

			if (adultPaxTO.getDisplayPnrPaxCatFOIDNumber() != null && !adultPaxTO.getDisplayPnrPaxCatFOIDNumber().equals("null")
					&& !adultPaxTO.getDisplayPnrPaxCatFOIDNumber().equals("undefined")
					&& !adultPaxTO.getDisplayPnrPaxCatFOIDNumber().equals("")) {
				foidNum = adultPaxTO.getDisplayPnrPaxCatFOIDNumber();
			}

			if (adultPaxTO.getDisplayPnrPaxCatFOIDExpiry() != null && !adultPaxTO.getDisplayPnrPaxCatFOIDExpiry().equals("null")
					&& !adultPaxTO.getDisplayPnrPaxCatFOIDExpiry().equals("undefined")
					&& !adultPaxTO.getDisplayPnrPaxCatFOIDExpiry().equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				foidExpiry = sdf.parse(adultPaxTO.getDisplayPnrPaxCatFOIDExpiry());
			}

			if (adultPaxTO.getDisplayPnrPaxCatFOIDPlace() != null && !adultPaxTO.getDisplayPnrPaxCatFOIDPlace().equals("null")
					&& !adultPaxTO.getDisplayPnrPaxCatFOIDPlace().equals("undefined")
					&& !adultPaxTO.getDisplayPnrPaxCatFOIDPlace().equals("")) {
				foidPlace = adultPaxTO.getDisplayPnrPaxCatFOIDPlace();
			}

			addnInfo.setPassportNo(foidNum);
			addnInfo.setPassportExpiry(foidExpiry);
			addnInfo.setPassportIssuedCntry(foidPlace);

			if (adultPaxTO.getDisplayNationalIDNo() != null && !adultPaxTO.getDisplayNationalIDNo().equals("null")
					&& !adultPaxTO.getDisplayNationalIDNo().equals("undefined")
					&& !adultPaxTO.getDisplayNationalIDNo().equals("")) {
				nationalIDNo = adultPaxTO.getDisplayNationalIDNo();
			}

			if (adultPaxTO.getDisplaypnrPaxArrivalFlightNumber() != null
					&& !adultPaxTO.getDisplaypnrPaxArrivalFlightNumber().equals("null")
					&& !adultPaxTO.getDisplaypnrPaxArrivalFlightNumber().equals("")) {
				arrivalIntlFlightNo = adultPaxTO.getDisplaypnrPaxArrivalFlightNumber();
			}

			if (adultPaxTO.getDisplaypnrPaxFltArrivalDate() != null && !adultPaxTO.getDisplaypnrPaxFltArrivalDate().equals("null")
					&& !adultPaxTO.getDisplaypnrPaxFltArrivalDate().equals("")) {
				intlFlightArrivalDate = adultPaxTO.getDisplaypnrPaxFltArrivalDate();
			}

			if (adultPaxTO.getDisplaypnrPaxArrivalTime() != null && !adultPaxTO.getDisplaypnrPaxArrivalTime().equals("null")
					&& !adultPaxTO.getDisplaypnrPaxArrivalTime().equals("")) {
				intlFlightArrivalTime = adultPaxTO.getDisplaypnrPaxArrivalTime();
			}

			if (adultPaxTO.getDisplaypnrPaxDepartureFlightNumber() != null
					&& !adultPaxTO.getDisplaypnrPaxDepartureFlightNumber().equals("null")
					&& !adultPaxTO.getDisplaypnrPaxDepartureFlightNumber().equals("")) {
				departureIntlFlightNo = adultPaxTO.getDisplaypnrPaxDepartureFlightNumber();
			}

			if (adultPaxTO.getDisplaypnrPaxFltDepartureDate() != null
					&& !adultPaxTO.getDisplaypnrPaxFltDepartureDate().equals("null")
					&& !adultPaxTO.getDisplaypnrPaxFltDepartureDate().equals("")) {
				intlFlightDepartureDate = adultPaxTO.getDisplaypnrPaxFltDepartureDate();
			}

			if (adultPaxTO.getDisplaypnrPaxDepartureTime() != null && !adultPaxTO.getDisplaypnrPaxDepartureTime().equals("null")
					&& !adultPaxTO.getDisplaypnrPaxDepartureTime().equals("")) {
				intlFlightDepartureTime = adultPaxTO.getDisplaypnrPaxDepartureTime();
			}

			if (adultPaxTO.getDisplayPnrPaxGroupId() != null && !adultPaxTO.getDisplayPnrPaxGroupId().equals("null")
					&& !adultPaxTO.getDisplayPnrPaxGroupId().equals("")) {
				pnrPaxGroupId = adultPaxTO.getDisplayPnrPaxGroupId();
			}

			if (adultPaxTO.getDisplayFFID() != null && !adultPaxTO.getDisplayFFID().equals("null")
					&& !adultPaxTO.getDisplayFFID().equals("")) {
				ffid = adultPaxTO.getDisplayFFID();
			}

			internationalFlightArrivalDate = getTimeStampObj(intlFlightArrivalDate, intlFlightArrivalTime);
			internationalFlightDepartureDate = getTimeStampObj(intlFlightDepartureDate, intlFlightDepartureTime);
			addnInfo.setPassportNo(foidNum);
			addnInfo.setPassportExpiry(foidExpiry);
			addnInfo.setPassportIssuedCntry(foidPlace);
			addnInfo.setNationalIDNo(nationalIDNo);

			if (adultPaxTO.getDisplayPnrPaxPlaceOfBirth() != null && !adultPaxTO.getDisplayPnrPaxPlaceOfBirth().equals("null")
					&& !adultPaxTO.getDisplayPnrPaxPlaceOfBirth().equals("undefined")
					&& !adultPaxTO.getDisplayPnrPaxPlaceOfBirth().equals("")) {
				addnInfo.setPlaceOfBirth(adultPaxTO.getDisplayPnrPaxPlaceOfBirth());
			}
			addnInfo.setTravelDocumentType(adultPaxTO.getDisplayTravelDocType());

			if (adultPaxTO.getDisplayVisaDocNumber() != null && !adultPaxTO.getDisplayVisaDocNumber().equals("null")
					&& !adultPaxTO.getDisplayVisaDocNumber().equals("undefined")
					&& !adultPaxTO.getDisplayVisaDocNumber().equals("")) {
				addnInfo.setVisaDocNumber(adultPaxTO.getDisplayVisaDocNumber());
			}
			if (adultPaxTO.getDisplayVisaDocPlaceOfIssue() != null && !adultPaxTO.getDisplayVisaDocPlaceOfIssue().equals("null")
					&& !adultPaxTO.getDisplayVisaDocPlaceOfIssue().equals("undefined")
					&& !adultPaxTO.getDisplayVisaDocPlaceOfIssue().equals("")) {
				addnInfo.setVisaDocPlaceOfIssue(adultPaxTO.getDisplayVisaDocPlaceOfIssue());
			}

			if (adultPaxTO.getDisplayVisaDocIssueDate() != null && !adultPaxTO.getDisplayVisaDocIssueDate().equals("null")
					&& !adultPaxTO.getDisplayVisaDocIssueDate().equals("undefined")
					&& !adultPaxTO.getDisplayVisaDocIssueDate().equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				addnInfo.setVisaDocIssueDate(sdf.parse(adultPaxTO.getDisplayVisaDocIssueDate()));
			}

			if (adultPaxTO.getDisplayVisaApplicableCountry() != null
					&& !adultPaxTO.getDisplayVisaApplicableCountry().equals("null")
					&& !adultPaxTO.getDisplayVisaApplicableCountry().equals("undefined")
					&& !adultPaxTO.getDisplayVisaApplicableCountry().equals("")) {
				addnInfo.setVisaApplicableCountry(adultPaxTO.getDisplayVisaApplicableCountry());
			}
			addnInfo.setArrivalIntlFltNo(arrivalIntlFlightNo);
			addnInfo.setIntlFltArrivalDate(internationalFlightArrivalDate);
			addnInfo.setDepartureIntlFltNo(departureIntlFlightNo);
			addnInfo.setIntlFltDepartureDate(internationalFlightDepartureDate);
			addnInfo.setPnrPaxGroupId(pnrPaxGroupId);
			addnInfo.setFfid(ffid);

			adultLccPax.setLccClientAdditionPax(addnInfo);
			String firstName = adultPaxTO.getDisplayAdultFirstNameOl();
			String lastName = adultPaxTO.getDisplayAdultLastNameOl();
			String translationLanguage = adultPaxTO.getDisplayNameTranslationLanguage();
			if (isValid(firstName) && isValid(lastName) && isValid(translationLanguage)) {
				LCCClientReservationPaxNamesTranslation paxNamesTranslations = new LCCClientReservationPaxNamesTranslation();
				paxNamesTranslations.setFirstNameOl(firstName);
				paxNamesTranslations.setLastNameOl(lastName);
				paxNamesTranslations.setLanguageCode(translationLanguage);
				paxNamesTranslations.setTitleOl(adultPaxTO.getDisplayAdultTitleOl());
				adultLccPax.setLccClientPaxNameTranslations(paxNamesTranslations);
			}

			String adultDOB = adultPaxTO.getDisplayAdultDOB();
			adultLccPax
					.setDateOfBirth(adultDOB != null && adultDOB.trim().length() > 0 ? BookingUtil.stringToDate(adultDOB) : null);

			paxAdults.add(adultLccPax);
		}
		return paxAdults;
	}

	public static Map<Integer, List<LCCClientExternalChgDTO>> getPassengerExtChgMap(BookingShoppingCart bookingShoppingCart) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		for (ReservationPaxTO pax : bookingShoppingCart.getPaxList()) {
			List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
			extChgList.addAll(pax.getExternalCharges());
			paxMap.put(pax.getSeqNumber(), extChgList);
		}

		return paxMap;
	}

	public static Map<String, List<LCCClientExternalChgDTO>>
			getPaxRefNumberWiseExtChgMap(BookingShoppingCart bookingShoppingCart) {
		Map<String, List<LCCClientExternalChgDTO>> paxMap = new HashMap<String, List<LCCClientExternalChgDTO>>();
		for (ReservationPaxTO pax : bookingShoppingCart.getPaxList()) {
			List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
			extChgList.addAll(pax.getExternalCharges());
			paxMap.put(pax.getTravelerRefNumber(), extChgList);
		}

		return paxMap;
	}

	public static Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap(BookingShoppingCart bookingShoppingCart)
			throws ModuleException {
		Collection<ReservationPaxTO> paxList = bookingShoppingCart.getPaxList();
		Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedChargers = bookingShoppingCart.getSelectedExternalCharges();

		Set<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		if (paxList != null) {
			for (ReservationPaxTO pax : paxList) {
				for (LCCClientExternalChgDTO extChg : pax.getExternalCharges()) {
					colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
				}

			}
		}
		if (selectedChargers != null) {
			Set<EXTERNAL_CHARGES> tmpKeySet = selectedChargers.keySet();
			for (EXTERNAL_CHARGES key : tmpKeySet) {
				colEXTERNAL_CHARGES.add(key);
			}
		}
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SERVICE_TAX);

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		if (colEXTERNAL_CHARGES.size() > 0) {
			extExternalChgDTOMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
					ChargeRateOperationType.MODIFY_ONLY);
		}
		return extExternalChgDTOMap;
	}

	private static boolean isValid(String value) {
		if (value != null && !value.trim().equals("") && !value.trim().equals("null") && !value.trim().equals("undefined")) {
			return true;
		}
		return false;
	}

	/**
	 * checkes whether user has Buffer Time Mod - Seg & Pax privillege if segment within the buffer time
	 * 
	 * @param privilBuffertimeMod
	 * @param sysTime
	 * @param segModifyBuffStart
	 * @param segBuffEnd
	 */
	private static boolean isAddGroundSegmentEnableForUser(boolean privilBuffertimeMod, long sysTime, long segModifyBuffStart,
			long segBuffEnd) {
		if (sysTime > segModifyBuffStart) {
			if (sysTime < segBuffEnd) {
				if (!privilBuffertimeMod) {
					return false;
				}
			}
		}
		return true;
	}

	public static LCCClientReservation loadMinimumReservation(String pnr, boolean isGroupPNR, TrackInfoDTO trackInfo)
			throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			pnrModesDTO.setLoadExternalPaxPayments(true);
			pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		} else {
			pnrModesDTO.setPnr(pnr);
		}

		pnrModesDTO.setAppIndicator(ApplicationEngine.XBE);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);

		ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
		paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_XBE);

		if (log.isDebugEnabled())
			log.debug("Loading minimum reservation. " + pnrModesDTO.toString());
		LCCClientReservation reservation = ModuleServiceLocator.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO,
				paramRQInfo, trackInfo);
		return reservation;
	}

	public static void applyFareDiscount(String pnr, boolean isGroupPNR, FareDiscountTO fareDiscountTO,
			BookingShoppingCart bookingShoppingCart, List<PassengerTypeQuantityTO> passengerTypeQuantityList,
			boolean hasAddFareDiscPrev, boolean hasOverrideFareDiscPrev, BaseAvailRQ baseAvailRQ, String transactionId)
			throws ModuleException {

		Integer oldFareDiscount = ModuleServiceLocator.getAirproxySearchAndQuoteBD().getAppliedFareDiscountPercentage(pnr,
				isGroupPNR);

		if (hasAddFareDiscPrev && oldFareDiscount > 0 && fareDiscountTO != null && bookingShoppingCart != null
				&& passengerTypeQuantityList != null && passengerTypeQuantityList.size() > 0
				&& fareDiscountTO.getFareDiscountMax() != null && fareDiscountTO.getFareDiscountMin() != null) {
			if (fareDiscountTO != null && fareDiscountTO.isFareDiscountAvailable()
					&& oldFareDiscount >= fareDiscountTO.getFareDiscountMin()
					&& fareDiscountTO.getFareDiscountMax() >= oldFareDiscount) {
				bookingShoppingCart.setFareDiscount(oldFareDiscount, "Fare Discount - " + oldFareDiscount, "",
						passengerTypeQuantityList, true);

				ReservationUtil.calculateDiscountForReservation(bookingShoppingCart, bookingShoppingCart.getPaxList(),
						baseAvailRQ, null, true, bookingShoppingCart.getFareDiscount(), bookingShoppingCart.getPriceInfoTO(),
						transactionId);

			} else if (hasOverrideFareDiscPrev && (oldFareDiscount >= 0 && oldFareDiscount <= 100)
					&& (oldFareDiscount > fareDiscountTO.getFareDiscountMax()
							|| oldFareDiscount < fareDiscountTO.getFareDiscountMin())) {
				bookingShoppingCart.setFareDiscount(oldFareDiscount, "Fare Discount - " + oldFareDiscount, "",
						passengerTypeQuantityList, true);
				ReservationUtil.calculateDiscountForReservation(bookingShoppingCart, bookingShoppingCart.getPaxList(),
						baseAvailRQ, null, true, bookingShoppingCart.getFareDiscount(), bookingShoppingCart.getPriceInfoTO(),
						transactionId);
			}

		}
	}

	public static void updateDepartureDateInfoForAnciFlights(Collection<LCCClientReservationSegment> segments,
			Collection<ReservationPaxTO> paxList) {
		for (ReservationPaxTO reservationPaxTO : paxList) {
			List<LCCSelectedSegmentAncillaryDTO> lstSelAnci = reservationPaxTO.getSelectedAncillaries();
			if (lstSelAnci != null) {
				for (LCCSelectedSegmentAncillaryDTO selAnci : lstSelAnci) {
					FlightSegmentTO flightSegmentTO = selAnci.getFlightSegmentTO();
					for (LCCClientReservationSegment resSeg : segments) {
						if (flightSegmentTO.getFlightRefNumber().equals(resSeg.getFlightSegmentRefNumber())
								&& flightSegmentTO.getSegmentSequence() == resSeg.getSegmentSeq()) {
							flightSegmentTO.setDepartureDateTime(resSeg.getDepartureDate());
							flightSegmentTO.setDepartureDateTimeZulu(resSeg.getDepartureDateZulu());
							break;
						}
					}
				}
			}
		}
	}

	public static Map<String, String> getExcludableChargesMap() throws ModuleException {
		Map<String, String> excludableChargeMap = new HashMap<String, String>();
		List<Charge> excludableCharges = ModuleServiceLocator.getChargeBD().getExcludableCharges();

		if (excludableCharges != null && !excludableCharges.isEmpty()) {
			for (Charge charge : excludableCharges) {
				excludableChargeMap.put(charge.getChargeCode(), charge.getChargeDescription());
			}
		}

		return excludableChargeMap;
	}

	public static BigDecimal addTaxToServiceCharge(BookingShoppingCart bookingShoppingCart, BigDecimal serviceCharge) {
		BigDecimal serviceTax = calculatedServiceTax(bookingShoppingCart, serviceCharge);
		return AccelAeroCalculator.add(serviceCharge, serviceTax);
	}

	private static BigDecimal calculatedServiceTax(BookingShoppingCart bookingShoppingCart, BigDecimal serviceCharge) {
		BigDecimal serviceChargeTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (bookingShoppingCart.isTaxApplicable(EXTERNAL_CHARGES.JN_OTHER)) {
			if (AccelAeroCalculator.isGreaterThan(serviceCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				serviceChargeTax = AccelAeroCalculator.multiplyDefaultScale(serviceCharge,
						bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_OTHER));
			}
		}

		return serviceChargeTax;
	}

	public static BigDecimal getApplicableServiceTax(BookingShoppingCart bookingShoppingCart, EXTERNAL_CHARGES taxChargeCode) {

		BigDecimal totalJnTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		ServiceTaxContainer serviceTaxObj = bookingShoppingCart.getServiceTax(taxChargeCode);

		if (serviceTaxObj != null && serviceTaxObj.isTaxApplicable()) {
			for (EXTERNAL_CHARGES taxableService : serviceTaxObj.getTaxableExternalCharges()) {
				BigDecimal serviceCharge = bookingShoppingCart.getServiceCharge(taxableService);
				if (AccelAeroCalculator.isGreaterThan(serviceCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
					BigDecimal serviceJNTax = AccelAeroCalculator.multiplyDefaultScale(serviceCharge,
							bookingShoppingCart.getServiceTaxRatio(taxChargeCode));

					totalJnTax = AccelAeroCalculator.add(totalJnTax, serviceJNTax);
				}
			}

			ExternalChgDTO externalChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(taxChargeCode);
			externalChgDTO.setTotalAmount(AccelAeroCalculator.parseBigDecimal(totalJnTax.doubleValue()));
			((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(serviceTaxObj.getTaxApplyingFlightRefNumber());
			bookingShoppingCart.addSelectedExternalCharge(taxChargeCode);
			bookingShoppingCart.setTotalExternalChangeAmount(taxChargeCode);
		}

		return totalJnTax;
	}

	public static BigDecimal getApplicableServiceTaxForCCCharge(BookingShoppingCart bookingShoppingCart,
			EXTERNAL_CHARGES taxChargeCode) {

		BigDecimal totalJnTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		ServiceTaxContainer serviceTaxObj = bookingShoppingCart.getServiceTax(taxChargeCode);

		if (serviceTaxObj != null && serviceTaxObj.isTaxApplicable()) {
			for (EXTERNAL_CHARGES taxableService : serviceTaxObj.getTaxableExternalCharges()) {
				if (taxableService.equals(EXTERNAL_CHARGES.CREDIT_CARD)) {
					BigDecimal serviceCharge = bookingShoppingCart.getServiceCharge(taxableService);
					if (AccelAeroCalculator.isGreaterThan(serviceCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						BigDecimal serviceJNTax = AccelAeroCalculator.multiplyDefaultScale(serviceCharge,
								bookingShoppingCart.getServiceTaxRatio(taxChargeCode));

						totalJnTax = AccelAeroCalculator.add(totalJnTax, serviceJNTax);
					}
				}
			}
		}

		return totalJnTax;
	}

	private static Date getTimeStampObj(String strDate, String strTime) {
		try {
			Calendar validDate = null;
			Date dtReturn = null;
			if (!strDate.equals("")) {
				int date = Integer.parseInt(strDate.substring(0, 2));
				int month = Integer.parseInt(strDate.substring(3, 5));
				int year = Integer.parseInt(strDate.substring(6, 10));

				validDate = new GregorianCalendar(year, month - 1, date);

			}
			if (!strTime.equals("") && validDate != null) {
				Date tmpDate = validDate.getTime();
				Calendar tmpCalendar = Calendar.getInstance();
				tmpCalendar.setTime(tmpDate);
				if (strTime.length() == 4) {
					int hours = Integer.parseInt(strTime.substring(0, 1));
					int minutes = Integer.parseInt(strTime.substring(2, 4));
					tmpCalendar.add(Calendar.HOUR, hours);
					tmpCalendar.add(Calendar.MINUTE, minutes);
					validDate = tmpCalendar;
				} else {
					int hours = Integer.parseInt(strTime.substring(0, 2));
					int minutes = Integer.parseInt(strTime.substring(3, 5));
					tmpCalendar.add(Calendar.HOUR, hours);
					tmpCalendar.add(Calendar.MINUTE, minutes);
					validDate = tmpCalendar;
				}

			}
			if (validDate != null) {
				dtReturn = validDate.getTime();
			}
			return dtReturn;

		} catch (Exception e) {
			return null;
		}
	}

	private static String getBundledFareName(LCCClientReservation reservation, Integer bundledFarePeriodId) {
		List<BundledFareDTO> bundledFareDTOs = reservation.getBundledFareDTOs();
		if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty() && bundledFarePeriodId != null) {
			for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
				if (bundledFareDTO != null) {
					if (bundledFareDTO.getBundledFarePeriodId().equals(bundledFarePeriodId)) {
						return bundledFareDTO.getBundledFareName();
					}
				}
			}
		}
		return null;
	}

	private static VoucherDTO transformToVoucherDTO(JSONObject ob) {
		VoucherDTO voucherDTO = new VoucherDTO();
		voucherDTO.setVoucherId(BeanUtils.nullHandler(ob.get("voucherId")));
		voucherDTO.setRedeemdAmount(BeanUtils.nullHandler(ob.get("redeemdAmount")));
		return voucherDTO;
	}

	public static ReservationDiscountDTO calculateDiscountForReservation(BookingShoppingCart resInfo,
			Collection<ReservationPaxTO> paxList, BaseAvailRQ baseAvailRQ, TrackInfoDTO trackInfoDTO, boolean calculateTotalOnly,
			DiscountedFareDetails discountInfo, PriceInfoTO priceInfoTO, String transactionId) throws ModuleException {
		ReservationDiscountDTO resDiscountDTO = null;

		if (discountInfo != null && priceInfoTO != null && baseAvailRQ != null) {

			DiscountRQ promotionRQ = ReservationBeanUtil.getPromotionCalculatorRQ(discountInfo,
					ReservationBeanUtil.transformPaxList(paxList),
					baseAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
			promotionRQ.setCalculateTotalOnly(calculateTotalOnly);

			if (promotionRQ != null) {
				resDiscountDTO = ReservationApiUtils.getApplicableDiscount(promotionRQ, priceInfoTO.getFareSegChargeTO(),
						baseAvailRQ, baseAvailRQ.getAvailPreferences().getSearchSystem(), transactionId, trackInfoDTO);
				if (resInfo != null && resDiscountDTO.isDiscountExist()) {
					resInfo.setFareDiscountAmount(resDiscountDTO.getTotalDiscount());
					if (DiscountRQ.DISCOUNT_METHOD.DOM_FARE_DISCOUNT == promotionRQ.getActionType()) {
						resInfo.getFareDiscount().setDomFareDiscountPercentage((int) resDiscountDTO.getDiscountPercentage());
					}

				}
			}

		}

		return resDiscountDTO;
	}

	public static void calculateDiscount(BookingShoppingCart bookingShoppingCart, BaseAvailRQ flightPriceRQ, String txnIdentifier,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		if (bookingShoppingCart != null && flightPriceRQ != null && bookingShoppingCart.getPriceInfoTO() != null
				&& bookingShoppingCart.getFareDiscount() != null && !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE
						.equals(bookingShoppingCart.getFareDiscount().getPromotionType())) {
			ReservationUtil.calculateDiscountForReservation(bookingShoppingCart, bookingShoppingCart.getPaxList(), flightPriceRQ,
					trackInfoDTO, true, bookingShoppingCart.getFareDiscount(), bookingShoppingCart.getPriceInfoTO(),
					txnIdentifier);
		}
	}

	public static Collection<FlightInfoTO> loadOtherAirlineFlightSegments(LCCClientReservation reservation,
			ReservationProcessParams rParam) {
		Collection<FlightInfoTO> collection = new ArrayList<FlightInfoTO>();
		Set<LCCClientOtherAirlineSegment> otherAirlineSegments = reservation.getOthertAirlineSegments();

		SimpleDateFormat smpdtDate = new SimpleDateFormat("EEE dd/MM/yyyy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
		String strNextDay = "";

		List<LCCClientOtherAirlineSegment> otherAirlineSegmentList = new ArrayList<LCCClientOtherAirlineSegment>(
				otherAirlineSegments);
		Collections.sort(otherAirlineSegmentList);
		for (LCCClientOtherAirlineSegment lccOtherAirlineSegment : otherAirlineSegmentList) {

			String formatedArrivalDate = lccOtherAirlineSegment.getArrivalDateTimeLocal() != null
					? smpdtDate.format(lccOtherAirlineSegment.getArrivalDateTimeLocal())
					: "";
			String formatedArrivalTime = lccOtherAirlineSegment.getArrivalDateTimeLocal() != null
					? smpdtTime.format(lccOtherAirlineSegment.getArrivalDateTimeLocal())
					: "00:00";

			String formatedDepartureDate = lccOtherAirlineSegment.getDepartureDateTimeLocal() != null
					? smpdtDate.format(lccOtherAirlineSegment.getDepartureDateTimeLocal())
					: "";
			String formatedDepartureTime = lccOtherAirlineSegment.getDepartureDateTimeLocal() != null
					? smpdtTime.format(lccOtherAirlineSegment.getDepartureDateTimeLocal())
					: "00:00";

			if (!smpdtDate.format(lccOtherAirlineSegment.getDepartureDateTimeLocal()).equals(formatedArrivalDate)) {
				strNextDay = " (+1)";
			}
			String status = ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED
					.equals(lccOtherAirlineSegment.getStatus())
							? ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
							: (ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CANCEL
									.equals(lccOtherAirlineSegment.getStatus())
											? ReservationInternalConstants.ReservationSegmentStatus.CANCEL
											: "");
			String cabinClass = getFareTypeName(lccOtherAirlineSegment.getCabinClassCode(), null);

			FlightInfoTO flightInfoTO = new FlightInfoTO();
			flightInfoTO.setOrignNDest(lccOtherAirlineSegment.getSegmentCode());
			flightInfoTO.setFlightNo(lccOtherAirlineSegment.getFlightNumber());
			flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(lccOtherAirlineSegment.getFlightNumber()));
			flightInfoTO.setDepartureDate(formatedDepartureDate);
			flightInfoTO.setDepartureDateLong(lccOtherAirlineSegment.getDepartureDateTimeLocal().getTime());
			flightInfoTO.setDepartureText("DEP");
			flightInfoTO.setDeparture("");
			flightInfoTO.setDepartureTime(formatedDepartureTime);
			flightInfoTO.setInterLineGroupKey(lccOtherAirlineSegment.getInterlineGroupKey());

			flightInfoTO.setArrivalText("ARR");
			flightInfoTO.setArrivalDate(formatedArrivalDate);
			flightInfoTO.setArrivalDateLong(lccOtherAirlineSegment.getArrivalDateTimeLocal().getTime());
			flightInfoTO.setArrival("");
			flightInfoTO.setArrivalTime(formatedArrivalTime + strNextDay);
			flightInfoTO.setCabinClass(cabinClass);
			flightInfoTO.setType(BookingClass.BookingClassType.NORMAL);
			flightInfoTO.setStatus(status);
			flightInfoTO.setDisplayStatus(status);

			collection.add(flightInfoTO);
		}

		return collection;
	}

	// method to filter the reservations seats from modifying seats and avoid unnecessary actions like release or block
	// on the reservation seats
	public static List<LCCSegmentSeatDTO> filteredBlockSeats(List<LCCSegmentSeatDTO> blockedSeatDTOs, String pnr,
			boolean isGroupPnr, HttpServletRequest request, TrackInfoDTO trackInfoDTO) throws ModuleException {

		List<LCCSegmentSeatDTO> filteredBlockSeatDTOs = new ArrayList<LCCSegmentSeatDTO>();
		Map<LCCSegmentSeatDTO, List<LCCAirSeatDTO>> existingSeatMaps = new HashMap<LCCSegmentSeatDTO, List<LCCAirSeatDTO>>();

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		if (pnr != null && !pnr.isEmpty()) {
			// filter already blocked seats requires reservation load
			LCCClientReservation reservation = ReservationUtil.loadFullLccReservation(pnr, isGroupPnr, null, bookingShoppingCart,
					request, false, null, null, null, false, trackInfoDTO);

			if (reservation.getPassengers() != null && !reservation.getPassengers().isEmpty()) {
				for (LCCSegmentSeatDTO lccSegmentSeatDTO : blockedSeatDTOs) {
					for (LCCAirSeatDTO lccAirSeatDTO : lccSegmentSeatDTO.getAirSeatDTOs()) {
						for (LCCClientReservationPax reservationPaxTO : reservation.getPassengers()) {
							for (LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO : reservationPaxTO
									.getSelectedAncillaries()) {
								if (lccSegmentSeatDTO.getFlightSegmentTO().getFlightRefNumber()
										.equals(lccSelectedSegmentAncillaryDTO.getFlightSegmentTO().getFlightRefNumber())
										&& lccSelectedSegmentAncillaryDTO.getAirSeatDTO() != null) {
									if (lccAirSeatDTO.getSeatNumber()
											.equals(lccSelectedSegmentAncillaryDTO.getAirSeatDTO().getSeatNumber())) {
										if (!existingSeatMaps.containsKey(lccSegmentSeatDTO)) {
											existingSeatMaps.put(lccSegmentSeatDTO, new ArrayList<LCCAirSeatDTO>());
										}
										existingSeatMaps.get(lccSegmentSeatDTO).add(lccAirSeatDTO);
									}
								}
							}
						}
					}
				}
				if (!existingSeatMaps.isEmpty()) {
					for (LCCSegmentSeatDTO lccSegmentSeatDTO : blockedSeatDTOs) {
						if (existingSeatMaps.containsKey(lccSegmentSeatDTO)) {
							LCCSegmentSeatDTO filteredLCCSegmentSeatDTO = new LCCSegmentSeatDTO();
							filteredLCCSegmentSeatDTO.setFlightSegmentTO(lccSegmentSeatDTO.getFlightSegmentTO());
							filteredLCCSegmentSeatDTO.setAirSeatDTOs(new ArrayList<LCCAirSeatDTO>());

							for (LCCAirSeatDTO lccAirSeatDTO : lccSegmentSeatDTO.getAirSeatDTOs()) {
								if (!existingSeatMaps.get(lccSegmentSeatDTO).contains(lccAirSeatDTO)) {
									filteredLCCSegmentSeatDTO.getAirSeatDTOs().add(lccAirSeatDTO);
								}
							}

							if (!filteredLCCSegmentSeatDTO.getAirSeatDTOs().isEmpty()) {
								filteredBlockSeatDTOs.add(filteredLCCSegmentSeatDTO);
							}
						} else {
							filteredBlockSeatDTOs.add(lccSegmentSeatDTO);
						}
					}
				} else {
					filteredBlockSeatDTOs = blockedSeatDTOs;
				}
			} else {
				filteredBlockSeatDTOs = blockedSeatDTOs;
			}
		} else {
			filteredBlockSeatDTOs = blockedSeatDTOs;
		}

		return filteredBlockSeatDTOs;
	}

	private static Integer getCarrierPnrPaxId(String travelerRefNumber) {
		travelerRefNumber = PaxTypeUtils.getCarrierTravelerRefFromLCCTravelerRef(AppSysParamsUtil.getDefaultCarrierCode(),
				travelerRefNumber);
		Integer pnrPaxID = null;
		if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(travelerRefNumber)) {
			pnrPaxID = Integer.valueOf(travelerRefNumber.split("\\$")[1]);
		}

		return pnrPaxID;
	}

	public static List<FlightSegmentTO> convertToFlightSegmentTOs(Collection<LCCClientReservationSegment> colsegs) {

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<>();

		for (Iterator iterator = colsegs.iterator(); iterator.hasNext();) {

			FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
			LCCClientReservationSegment reservationSegment = (LCCClientReservationSegment) iterator.next();

			flightSegmentTO.setDepartureDateTimeZulu(reservationSegment.getDepartureDateZulu());
			flightSegmentTO.setArrivalDateTimeZulu(reservationSegment.getArrivalDateZulu());
			flightSegmentTO.setLogicalCabinClassCode(reservationSegment.getLogicalCabinClass());
			flightSegmentTO.setCabinClassCode(reservationSegment.getCabinClassCode());
			flightSegmentTO.setOndSequence(reservationSegment.getJourneySequence());
			flightSegmentTO.setSegmentCode(reservationSegment.getSegmentCode());
			flightSegmentTO.setSegmentSequence(reservationSegment.getSegmentSeq());
			flightSegmentTO.setReturnFlag(reservationSegment.getReturnFlag().equals("Y") ? true : false);
			flightSegmentTO.setOperatingAirline(reservationSegment.getCarrierCode());
			flightSegmentTO.setFlightRefNumber(reservationSegment.getFlightSegmentRefNumber());

			flightSegmentTOs.add(flightSegmentTO);
		}

		return flightSegmentTOs;

	}

	/*
	 * public static boolean isInfantPaymentSeparated(BookingShoppingCart bookingShoppingCart) {
	 * 
	 * boolean isInfantPaymentSeparated = true;
	 * 
	 * if (bookingShoppingCart.getReservationBalance() != null &&
	 * !bookingShoppingCart.getReservationBalance().isInfantPaymentSeparated()) { isInfantPaymentSeparated = false; }
	 * 
	 * return isInfantPaymentSeparated; }
	 */

	public static BigDecimal calculateServiceTaxForTransactionFee(BookingShoppingCart bookingShoppingCart,
			BigDecimal ccChargeAmount, String strTxnIdntifier, ServiceTaxCCParamsDTO serviceTaxCCParamsDTO,
			TrackInfoDTO trackInfoDTO, EXTERNAL_CHARGES externalChargeType, boolean skipPaxServiceTaxUpdate)
			throws ParseException, ModuleException, Exception {

		return calculateServiceTaxForTransactionFee(bookingShoppingCart, ccChargeAmount, strTxnIdntifier, serviceTaxCCParamsDTO,
				trackInfoDTO, externalChargeType, 0, 0, new HashSet<Integer>(), skipPaxServiceTaxUpdate);

	}

	public static BigDecimal calculateServiceTaxForTransactionFee(BookingShoppingCart bookingShoppingCart,
			BigDecimal ccChargeAmount, String strTxnIdntifier, ServiceTaxCCParamsDTO serviceTaxCCParamsDTO,
			TrackInfoDTO trackInfoDTO, EXTERNAL_CHARGES externalChargeType, int payableAdultChildCount, int payableInfantCount,
			Set<Integer> payablePaxSequence, boolean skipPaxServiceTaxUpdate) throws ParseException, ModuleException, Exception {

		ExternalChgDTO externalChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(externalChargeType);
		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		// boolean isInfantPaymentSeparated = isInfantPaymentSeparated(bookingShoppingCart);

		// params to apply service tax
		FlightSearchDTO searchParams = serviceTaxCCParamsDTO.getSearchParams();
		String paxWiseAnci = serviceTaxCCParamsDTO.getPaxWiseAnci();
		String resContactInfo = serviceTaxCCParamsDTO.getResContactInfo();
		String paxState = serviceTaxCCParamsDTO.getPaxState();
		String paxCountryCode = serviceTaxCCParamsDTO.getPaxCountryCode();
		String resPaxs = serviceTaxCCParamsDTO.getResPaxs();
		boolean paxTaxRegistered = serviceTaxCCParamsDTO.isPaxTaxRegistered();
		String groupPNR = serviceTaxCCParamsDTO.getGroupPNR();

		if (bookingShoppingCart.getPaxList() == null || bookingShoppingCart.getPaxList().isEmpty()) {
			if (paxWiseAnci != null && !paxWiseAnci.equals("")) {
				List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPaxBasicInfo(paxWiseAnci,
						ApplicationEngine.XBE);
				if (paxList != null && !paxList.isEmpty()) {
					bookingShoppingCart.setPaxList(paxList);
				}
			} else if (resPaxs != null && !resPaxs.equals("")) {
				Collection<LCCClientReservationPax> lccResPaxList = ReservationUtil
						.transformJsonPassengers(resPaxs.replaceAll("'", "\\\\'"));

				List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();

				for (LCCClientReservationPax lccResPax : lccResPaxList) {
					ReservationPaxTO resPaxTO = new ReservationPaxTO();
					resPaxTO.setPaxType(lccResPax.getPaxType());
					resPaxTO.setSeqNumber(lccResPax.getPaxSequence());
					resPaxTO.setIsParent((lccResPax.getInfants() != null && !lccResPax.getInfants().isEmpty()));
					resPaxTO.setTravelerRefNumber(lccResPax.getTravelerRefNumber());
					resPaxTO.setInfantWith(getInfantWith(lccResPax));
					paxList.add(resPaxTO);
				}

				if (lccResPaxList != null && !lccResPaxList.isEmpty()) {
					bookingShoppingCart.setPaxList(paxList);
				}
			}
		}

		if (bookingShoppingCart != null && ccChargeAmount != null) {

			BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
			AvailPreferencesTO availPref = baseAvailRQ.getAvailPreferences();
			availPref.setAppIndicator(ApplicationEngine.XBE);

			if (ReservationUtil.isGroupPnr(groupPNR)
					|| (searchParams != null && !SYSTEM.AA.toString().equals(searchParams.getSearchSystem()))) {
				availPref.setSearchSystem(SYSTEM.INT);
			} else {
				availPref.setSearchSystem(SYSTEM.AA);
			}

			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();

			List<FlightSegmentTO> flightSegmentTOs = populateFlightSegmentTO(serviceTaxCCParamsDTO);

			Collection<LCCClientPassengerSummaryTO> lccClientPassengerSummaryTO = null;
			if (bookingShoppingCart.getReservationBalance() != null
					&& bookingShoppingCart.getReservationBalance().getPassengerSummaryList() != null
					&& !bookingShoppingCart.getReservationBalance().getPassengerSummaryList().isEmpty()) {
				lccClientPassengerSummaryTO = bookingShoppingCart.getReservationBalance().getPassengerSummaryList();
			}

			if (flightSegmentTOs != null) {
				if (EXTERNAL_CHARGES.BSP_FEE.equals(externalChargeType)) {
					ServiceTaxCalculatorTransactionFeeUtil.populatePaxWiseCreditCardCharge(bookingShoppingCart.getPaxList(),
							externalChgDTO, ccChargeAmount, paxWiseExternalCharges, paxWisePaxTypes, flightSegmentTOs,
							payableAdultChildCount, payableInfantCount, payablePaxSequence);
				} else {
					ServiceTaxCalculatorUtil.populatePaxWiseCreditCardCharge(bookingShoppingCart.getPaxList(), externalChgDTO,
							ccChargeAmount, paxWiseExternalCharges, paxWisePaxTypes, flightSegmentTOs,
							lccClientPassengerSummaryTO, payablePaxSequence);
				}

				ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
				serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
				serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(null);
				serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
				serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
				serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
				serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(strTxnIdntifier);
				// required on lcc flow
				serviceTaxQuoteCriteriaDTO.setCalculateOnlyForExternalCharges(true);

				if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(resContactInfo)) {
					CommonReservationContactInfo contactInfo = ReservationUtil.transformJsonContactInfo(resContactInfo);
					serviceTaxQuoteCriteriaDTO.setPaxState(contactInfo.getState());
					serviceTaxQuoteCriteriaDTO.setPaxCountryCode(contactInfo.getCountryCode());
					serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(isPaxTaxRegistered(contactInfo.getTaxRegNo()));
				} else {
					serviceTaxQuoteCriteriaDTO.setPaxState(paxState);
					serviceTaxQuoteCriteriaDTO.setPaxCountryCode(paxCountryCode);
					serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(paxTaxRegistered);
				}

				Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
						.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfoDTO);
				totalServiceTaxAmount = ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(bookingShoppingCart.getPaxList(),
						serviceTaxQuoteRS, true, ApplicationEngine.XBE, skipPaxServiceTaxUpdate);

			}
		}
		return totalServiceTaxAmount;

	}

	private static Integer getInfantWith(LCCClientReservationPax lccResPax) {
		if (lccResPax.getPaxType().equals("IN") && lccResPax.getParent() != null) {
			return lccResPax.getParent().getPaxSequence();
		}
		return null;
	}

	private static List<FlightSegmentTO> populateFlightSegmentTO(ServiceTaxCCParamsDTO serviceTaxCCParamsDTO)
			throws ParseException, org.json.simple.parser.ParseException {
		FlightSearchDTO searchParams = serviceTaxCCParamsDTO.getSearchParams();
		FlightSearchDTO fareQuoteParams = serviceTaxCCParamsDTO.getFareQuoteParams();
		String flightRPHList = serviceTaxCCParamsDTO.getFlightRPHList();
		String selectedFlightList = serviceTaxCCParamsDTO.getSelectedFlightList();
		String flightSegmentToList = serviceTaxCCParamsDTO.getFlightSegmentToList();
		String reservationFlightSegmentList = serviceTaxCCParamsDTO.getNameChangeFlightSegmentList();

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		if (serviceTaxCCParamsDTO.isAddSegment()) {
			List<FlightSegmentTO> oldFlightSegmentTOs = ReservationUtil.transformResFlightSegment(reservationFlightSegmentList);
			List<FlightSegmentTO> newFlightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList,
					searchParams);
			Set<String> rphSet = new HashSet<>();
			for (ONDSearchDTO ondSearch : fareQuoteParams.getOndList()) {
				rphSet.addAll(cleanRPH(ondSearch.getFlightRPHList()));
			}
			flightSegmentTOs.addAll(oldFlightSegmentTOs);
			flightSegmentTOs.addAll(newFlightSegmentTOs);

			Iterator<FlightSegmentTO> itr = flightSegmentTOs.iterator();
			while (itr.hasNext()) {
				FlightSegmentTO flightSegment = itr.next();
				if (!rphSet.contains(flightSegment.getFlightRefNumber())) {
					itr.remove();
				}
			}

			SortUtil.sortFlightSegByDepDate(flightSegmentTOs);
		} else {
			if (searchParams != null && selectedFlightList != null && !"[]".equals(selectedFlightList)) {
				flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
			} else if (fareQuoteParams != null && flightRPHList != null && !"[]".equals(flightRPHList)) {
				flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(flightRPHList, fareQuoteParams);
			} else if (flightRPHList != null && !"[]".equals(flightRPHList)) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(flightRPHList);
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				SortUtil.sortFlightSegByDepDate(flightSegmentTOs);
			} else if (flightSegmentToList != null && !"[]".equals(flightSegmentToList)) {
				flightSegmentTOs = new ArrayList<FlightSegmentTO>(ReservationUtil.transformFlightSegmentTo(flightSegmentToList));
				SortUtil.sortFlightSegByDepDate(flightSegmentTOs);
			} else if (reservationFlightSegmentList != null && !"[]".equals(reservationFlightSegmentList)) {
				flightSegmentTOs = new ArrayList<FlightSegmentTO>(
						ReservationUtil.transformResFlightSegment(reservationFlightSegmentList));
				SortUtil.sortFlightSegByDepDate(flightSegmentTOs);
			}
		}

		return flightSegmentTOs;
	}

	private static List<String> cleanRPH(List<String> existingFlightRPHList) {
		List<String> list = new ArrayList<>();
		for (String flightRPH : existingFlightRPHList) {
			if (flightRPH != null && flightRPH.indexOf("#") != -1) {
				String[] flightRouteRPH = flightRPH.split("#");
				flightRPH = flightRouteRPH[0];
			}
			list.add(flightRPH);
		}

		return list;
	}

	private static boolean isPaxTaxRegistered(String taxRegNo) {
		boolean isPaxTaxRegistered = false;
		if (taxRegNo != null && !taxRegNo.equals("")) {
			isPaxTaxRegistered = true;
		}
		return isPaxTaxRegistered;
	}

	private static boolean isServiceTaxApplicable(String originCode, BaseAvailRQ baseAvailRQ) {

		String originCountry = "";

		String[] countryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");

		if (originCode == null || originCode.isEmpty() || baseAvailRQ == null) {
			return false;
		}

		if (baseAvailRQ.getAvailPreferences().getSearchSystem().equals(SYSTEM.INT)) {
			originCountry = getCountryCodeFromCachedAirportMap(originCode);
		} else {
			Object[] airportInfo = ModuleServiceLocator.getGlobalConfig().retrieveAirportInfo(originCode);
			if (airportInfo != null) {
				originCountry = (String) airportInfo[3];
			}
		}

		for (String countryCode : countryCodes) {
			if (originCountry.equals(countryCode)) {
				return true;
			}
		}

		return false;

	}

	private static BigDecimal updateFareQuoteTOs(List<FareQuoteTO> fareQuoteTOs,
			ServiceTaxQuoteForTicketingRevenueResTO ticketingRevenueResTO, ExchangeRateProxy exchangeRateProxy,
			String selCurrency) throws ModuleException {

		BigDecimal serviceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		List<ServiceTaxTO> adultServiceTaxes = SortUtil
				.sortServiceTaxByFlightRefDepartureDate(ticketingRevenueResTO.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.ADULT));
		List<ServiceTaxTO> childServiceTaxes = SortUtil
				.sortServiceTaxByFlightRefDepartureDate(ticketingRevenueResTO.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.CHILD));
		List<ServiceTaxTO> infantServiceTaxes = SortUtil
				.sortServiceTaxByFlightRefDepartureDate(ticketingRevenueResTO.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT));
		List<ServiceTaxTO> serviceTaxTOsToBeRemoved;

		String flightRefNo;

		for (FareQuoteTO fareQuoteTO : fareQuoteTOs) {

			BigDecimal totalServiceTaxAmountPerFareQuote = AccelAeroCalculator.getDefaultBigDecimalZero();

			flightRefNo = null;
			serviceTaxTOsToBeRemoved = new ArrayList<>();

			if (adultServiceTaxes != null) {
				for (ServiceTaxTO serviceTaxTO : adultServiceTaxes) {
					if (isServiceTaxApplicableToFareQuoteTO(fareQuoteTO.getFullOndCode(), serviceTaxTO.getFlightRefNumber(),
							flightRefNo)) {
						flightRefNo = serviceTaxTO.getFlightRefNumber();
						serviceTaxAmount = updatePaxFareTOs(fareQuoteTO.getPaxWise(), serviceTaxTO.getAmount(),
								PaxTypeTO.PAX_TYPE_ADULT_DISPLAY, exchangeRateProxy, selCurrency);
						totalServiceTaxAmountPerFareQuote = AccelAeroCalculator.add(totalServiceTaxAmountPerFareQuote,
								serviceTaxAmount);
						serviceTaxTOsToBeRemoved.add(serviceTaxTO);
					}
				}
				adultServiceTaxes.removeAll(serviceTaxTOsToBeRemoved);
			}

			flightRefNo = null;
			serviceTaxTOsToBeRemoved = new ArrayList<>();

			if (childServiceTaxes != null) {
				for (ServiceTaxTO serviceTaxTO : childServiceTaxes) {
					if (isServiceTaxApplicableToFareQuoteTO(fareQuoteTO.getFullOndCode(), serviceTaxTO.getFlightRefNumber(),
							flightRefNo)) {
						flightRefNo = serviceTaxTO.getFlightRefNumber();
						serviceTaxAmount = updatePaxFareTOs(fareQuoteTO.getPaxWise(), serviceTaxTO.getAmount(),
								PaxTypeTO.PAX_TYPE_CHILD_DISPLAY, exchangeRateProxy, selCurrency);
						totalServiceTaxAmountPerFareQuote = AccelAeroCalculator.add(totalServiceTaxAmountPerFareQuote,
								serviceTaxAmount);
						serviceTaxTOsToBeRemoved.add(serviceTaxTO);
					}
				}
				childServiceTaxes.removeAll(serviceTaxTOsToBeRemoved);
			}

			flightRefNo = null;
			serviceTaxTOsToBeRemoved = new ArrayList<>();

			if (infantServiceTaxes != null) {
				for (ServiceTaxTO serviceTaxTO : infantServiceTaxes) {
					if (isServiceTaxApplicableToFareQuoteTO(fareQuoteTO.getFullOndCode(), serviceTaxTO.getFlightRefNumber(),
							flightRefNo)) {
						flightRefNo = serviceTaxTO.getFlightRefNumber();
						serviceTaxAmount = updatePaxFareTOs(fareQuoteTO.getPaxWise(), serviceTaxTO.getAmount(),
								PaxTypeTO.PAX_TYPE_INFANT_DISPLAY, exchangeRateProxy, selCurrency);
						totalServiceTaxAmountPerFareQuote = AccelAeroCalculator.add(totalServiceTaxAmountPerFareQuote,
								serviceTaxAmount);
						serviceTaxTOsToBeRemoved.add(serviceTaxTO);
					}
				}
				infantServiceTaxes.removeAll(serviceTaxTOsToBeRemoved);
			}

			totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount, totalServiceTaxAmountPerFareQuote);

			fareQuoteTO.setTotalPrice(AccelAeroCalculator.formatAsDecimal(
					AccelAeroCalculator.add(new BigDecimal(fareQuoteTO.getTotalPrice()), totalServiceTaxAmountPerFareQuote)));

		}

		return totalServiceTaxAmount;

	}

	private static void updatePaxPriceTOs(List<PaxPriceTO> paxPriceTOs, BigDecimal serviceTaxAmount, String paxType,
			ExchangeRateProxy exchangeRateProxy, String selCurrency) throws ModuleException {
		for (PaxPriceTO paxPriceTO : paxPriceTOs) {
			if (paxPriceTO.getPaxType().equals(paxType)) {
				paxPriceTO.setPaxTotalPrice(AccelAeroCalculator.formatAsDecimal(
						AccelAeroCalculator.add(new BigDecimal(paxPriceTO.getPaxTotalPrice()), serviceTaxAmount)));
				CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency);
				Currency currency = currencyExchangeRate.getCurrency();

				paxPriceTO.setPaxTotalPriceInSelectedCurr(
						AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
								currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(paxPriceTO.getPaxTotalPrice()),
								currency.getBoundryValue(), currency.getBreakPoint())));
			}
		}
	}

	private static void updateFareQuoteSummaryTO(FareQuoteSummaryTO fareQuoteSummaryTO, BigDecimal totalServiceTaxAmount,
			ExchangeRateProxy exchangeRateProxy, String selCurrency) throws ModuleException {

		String totalPrice = AccelAeroCalculator.formatAsDecimal(
				AccelAeroCalculator.add(new BigDecimal(fareQuoteSummaryTO.getTotalPrice()), totalServiceTaxAmount));
		String totalTaxes = AccelAeroCalculator.formatAsDecimal(
				AccelAeroCalculator.add(new BigDecimal(fareQuoteSummaryTO.getTotalTaxes()), totalServiceTaxAmount));
		String totalWithoutHandlingCharge = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
				.add(new BigDecimal(fareQuoteSummaryTO.getTotalWithoutHandlingCharge()), totalServiceTaxAmount));

		fareQuoteSummaryTO.setTotalServiceTax(AccelAeroCalculator.formatAsDecimal(totalServiceTaxAmount));

		fareQuoteSummaryTO.setTotalPrice(totalPrice);
		fareQuoteSummaryTO.setTotalTaxes(totalTaxes);
		fareQuoteSummaryTO.setTotalWithoutHandlingCharge(totalWithoutHandlingCharge);

		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency);
		Currency currency = currencyExchangeRate.getCurrency();

		fareQuoteSummaryTO.setSelectedtotalPrice(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
				currencyExchangeRate.getMultiplyingExchangeRate(), new BigDecimal(fareQuoteSummaryTO.getTotalPrice()),
				currency.getBoundryValue(), currency.getBreakPoint())));

	}

	private static BigDecimal updatePaxFareTOs(Collection<PaxFareTO> paxFareTOs, BigDecimal serviceTaxAmount, String paxType,
			ExchangeRateProxy exchangeRateProxy, String selCurrency) throws ModuleException {

		BigDecimal totalServiceTaxForAllPax = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (PaxFareTO paxFareTO : paxFareTOs) {
			if (paxFareTO.getPaxType().equals(paxType)) {

				CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency);
				Currency currency = currencyExchangeRate.getCurrency();

				totalServiceTaxForAllPax = AccelAeroCalculator.multiply(serviceTaxAmount, new BigDecimal(paxFareTO.getNoPax()));
				paxFareTO.setTotal(AccelAeroCalculator.formatAsDecimal(
						AccelAeroCalculator.add(new BigDecimal(paxFareTO.getTotal()), totalServiceTaxForAllPax)));
				paxFareTO.setPerPax(AccelAeroCalculator
						.formatAsDecimal(AccelAeroCalculator.add(new BigDecimal(paxFareTO.getPerPax()), serviceTaxAmount)));
				paxFareTO.setTax(AccelAeroCalculator
						.formatAsDecimal(AccelAeroCalculator.add(new BigDecimal(paxFareTO.getTax()), serviceTaxAmount)));

				paxFareTO.setSelCurtotal(AccelAeroCalculator
						.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
								new BigDecimal(paxFareTO.getTotal()), currency.getBoundryValue(), currency.getBreakPoint())));
			}
		}

		return totalServiceTaxForAllPax;

	}

	private static boolean isServiceTaxApplicableToFareQuoteTO(String fareQouteFullONDCode, String serviceTaxFlightRefNo,
			String previousFlightRefNo) {
		if (fareQouteFullONDCode.contains(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(serviceTaxFlightRefNo))
				&& (previousFlightRefNo == null || previousFlightRefNo.equals(serviceTaxFlightRefNo) || fareQouteFullONDCode
						.substring(4).equals(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(serviceTaxFlightRefNo)))) {
			return true;
		}

		return false;
	}

	public static boolean isSegmentModifiableAsPerETicketStatus(LCCClientReservation reservation, Integer pnrSegId) {
		if (AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				for (LccClientPassengerEticketInfoTO eTicket : pax.geteTickets()) {
					if (pnrSegId != null) {
						if (Integer.parseInt(eTicket.getPnrSegId()) == pnrSegId.intValue()) {
							if (EticketStatus.CHECKEDIN.code().equals(eTicket.getPaxETicketStatus())
									|| EticketStatus.BOARDED.code().equals(eTicket.getPaxETicketStatus())) {
								return false;
							}
						}
					} else {
						if (EticketStatus.CHECKEDIN.code().equals(eTicket.getPaxETicketStatus())
								|| EticketStatus.BOARDED.code().equals(eTicket.getPaxETicketStatus())) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public static void initiateDisplayForModifiedReservation(LCCClientReservation reservation) throws ModuleException {

		DisplayTickerRqDTO displayTicketRq = new DisplayTickerRqDTO();
		displayTicketRq.setPnr(reservation.getPNR());
		displayTicketRq.setGdsId(reservation.getGdsId());
		displayTicketRq.setReservationPaxSet(reservation.getPassengers());

		ModuleServiceLocator.getGDSNotifyBD().handleDisplayForModifiedReservation(displayTicketRq);

	}

	public static ReservationFlow getReservationFlow(XBEReservationInfoDTO resInfo) {

		ReservationFlow reservationFlow = ReservationFlow.MODIFY;

		if (resInfo.getReservationStatus() == null) {
			reservationFlow = ReservationFlow.CREATE;
		}

		return reservationFlow;
	}

	public static String getCountryCodeFromCachedAirportMap(String airportCode) {
		String originCountry = "";
		try {
			Map<String, CachedAirportDTO> mapAirports = new HashMap<String, CachedAirportDTO>();
			mapAirports = ModuleServiceLocator.getAirportBD().getCachedAllAirportMap(new ArrayList<>(Arrays.asList(airportCode)));
			originCountry = mapAirports.get(airportCode) != null ? mapAirports.get(airportCode).getCountryCode() : "";

		} catch (ModuleException e) {
			originCountry = "";
			log.error("ERROR in loading country code for the origin airport", e);
		}
		return originCountry;
	}

	public static List<ReservationPaxTO> createPaxList(Collection<ReservationPax> resPaxList) {
		List<ReservationPaxTO> paxList = new ArrayList<>();
		String strCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (ReservationPax resPax : resPaxList) {
			ReservationPaxTO pax = new ReservationPaxTO();
			pax.setSeqNumber(resPax.getPaxSequence());
			String travellerRef = strCarrierCode + "|"
					+ com.isa.thinair.airproxy.api.utils.PaxTypeUtils.travelerReference(resPax);
			pax.setTravelerRefNumber(travellerRef);
			pax.setPaxType(resPax.getPaxType());
			pax.setIsParent(isParent(resPax.getPaxSequence(), resPaxList));
			pax.setFirstName(resPax.getFirstName());
			pax.setLastName(resPax.getLastName());
			if (resPax.getInfants() != null && resPax.getInfants().size() > 0) {
				pax.setInfantWith(resPax.getInfants().iterator().next().getPaxSequence());
			}
			paxList.add(pax);
		}
		return paxList;
	}

	private static boolean isParent(Integer paxSequence, Collection<ReservationPax> paxList) {
		boolean isParent = false;
		for (ReservationPax pax : paxList) {
			if (PaxTypeTO.INFANT.equals(pax.getPaxType()) && pax.getParent() != null
					&& paxSequence.equals(pax.getParent().getPaxSequence())) {
				isParent = true;
				break;
			}
		}
		return isParent;
	}

	// Method for Calculation of Administration fee
	public static BigDecimal calclulateAdministrationFee(BookingShoppingCart bookingShoppingCart, HttpServletRequest request,
			boolean isRequote, String strTxnIdntifier, TrackInfoDTO trackInfo, FlightSearchDTO searchParams,
			List<FlightSegmentTO> flightSegmentTOs, String paxState, String paxCountryCode, boolean paxTaxRegistered)
			throws ModuleException, ParseException, org.json.simple.parser.ParseException {

		BigDecimal totalAdministrationFee = AccelAeroCalculator.getDefaultBigDecimalZero();

		if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
				.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, RequestHandlerUtil.getAgentCode(request)))
				&& !isRequote) {

			BigDecimal totalPaymentAmountExcludingCCCharge = bookingShoppingCart.getTotalPayable(EXTERNAL_CHARGES.HANDLING_CHARGE,
					EXTERNAL_CHARGES.CREDIT_CARD, EXTERNAL_CHARGES.JN_ANCI);
			BigDecimal handlingCharge = bookingShoppingCart.getHandlingCharge();
			totalPaymentAmountExcludingCCCharge = AccelAeroCalculator.add(totalPaymentAmountExcludingCCCharge, handlingCharge);

			String serviceTaxObj = request.getSession().getAttribute("serviceTax").toString();
			if (serviceTaxObj != "") {
				BigDecimal serviveTax = new BigDecimal(serviceTaxObj);
				totalPaymentAmountExcludingCCCharge = AccelAeroCalculator.add(totalPaymentAmountExcludingCCCharge, serviveTax);
			}
			// cc fee calculation
			ExternalChgDTO externalCcChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD);
			if (externalCcChgDTO.isRatioValueInPercentage()) {
				externalCcChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
			} else {
				externalCcChgDTO.calculateAmount(bookingShoppingCart.getPayablePaxCount(), flightSegmentTOs.size());
			}
			bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD);
			bookingShoppingCart.addSelectedExternalCharge(externalCcChgDTO);

			// calculate CC & handling charge JN tax
			BigDecimal totalJNTax = ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

			/*
			 * // apply service tax for CCFee
			 */

			BigDecimal totalServiceTaxforCcFee = applyServiceTaxForCCFee(bookingShoppingCart, externalCcChgDTO.getAmount(),
					strTxnIdntifier, trackInfo, searchParams, flightSegmentTOs, paxState, paxCountryCode, paxTaxRegistered, true);

			BigDecimal basicAdminFee = externalCcChgDTO.getAmount();

			BigDecimal totalAdminFee = AccelAeroCalculator.add(basicAdminFee, totalJNTax, totalServiceTaxforCcFee);

			totalAdministrationFee = totalAdministrationFee.add(totalAdminFee);

		}

		return totalAdministrationFee;

	}

	private static BigDecimal applyServiceTaxForCCFee(BookingShoppingCart bookingShoppingCart, BigDecimal ccChargeAmount,
			String strTxnIdntifier, TrackInfoDTO trackInfo, FlightSearchDTO searchParams, List<FlightSegmentTO> flightSegmentTOs,
			String paxState, String paxCountryCode, boolean paxTaxRegistered, boolean skipPaxServiceTaxUpdate)
			throws ParseException, ModuleException {

		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// FlightAvailRS flightAvailRS;

		if (bookingShoppingCart != null && ccChargeAmount != null && searchParams != null) {

			ExternalChgDTO creditCardChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD);

			BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
			AvailPreferencesTO availPref = baseAvailRQ.getAvailPreferences();
			availPref.setAppIndicator(ApplicationEngine.XBE);

			if ((searchParams != null && !SYSTEM.AA.toString().equals(searchParams.getSearchSystem()))) {
				availPref.setSearchSystem(SYSTEM.INT);
			} else {
				availPref.setSearchSystem(SYSTEM.AA);
			}

			/*
			 * List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList,
			 * searchParams);
			 */

			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
			ServiceTaxCalculatorUtil.populatePaxWiseCreditCardCharge(bookingShoppingCart.getPaxList(), creditCardChgDTO,
					ccChargeAmount, paxWiseExternalCharges, paxWisePaxTypes, flightSegmentTOs, null, null);

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
			serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
			serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(null);
			serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
			serviceTaxQuoteCriteriaDTO.setPaxState(paxState);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(paxCountryCode);
			serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(paxTaxRegistered);
			serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
			serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
			serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(strTxnIdntifier);

			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
					.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfo);
			totalServiceTaxAmount = ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(bookingShoppingCart.getPaxList(),
					serviceTaxQuoteRS, false, ApplicationEngine.XBE, skipPaxServiceTaxUpdate);
		}
		return totalServiceTaxAmount;
	}
}