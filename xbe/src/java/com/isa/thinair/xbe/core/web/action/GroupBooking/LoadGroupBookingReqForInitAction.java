package com.isa.thinair.xbe.core.web.action.GroupBooking;

import java.util.Collection;
import java.util.Map;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class LoadGroupBookingReqForInitAction extends BaseRequestResponseAwareAction {

	private Map<String, String> airports;
	private String baseCurrency;
	private String agents;
	private Collection <String[]> agentNames;


	public String execute() {
		try {
			
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			String agentCode = user.getAgentCode();
			String [] params = new String[]{agentCode,agentCode,agentCode,agentCode};
			airports = SelectListGenerator.createActiveAirportCodeMap();
			baseCurrency = AppSysParamsUtil.getBaseCurrency();
			agents = SelectListGenerator.createAgentsNameList(params);
			agentNames = SelectListGenerator.createAgentsList();

		} catch (Exception e) {

		}
		return S2Constants.Result.SUCCESS;

	}
		
	public String getAgents() {
		return agents;
	}

	public void setAgents(String agents) {
		this.agents = agents;
	}
	
	public Map<String, String> getAirports() {
		return airports;
	}

	public void setAirports(Map<String, String> airports) {
		this.airports = airports;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	

	public Collection<String[]> getAgentNames() {
		return agentNames;
	}

	public void setAgentNames(Collection<String[]> agentNames) {
		this.agentNames = agentNames;
	}

}
