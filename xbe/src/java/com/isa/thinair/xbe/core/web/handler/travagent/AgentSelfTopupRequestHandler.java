package com.isa.thinair.xbe.core.web.handler.travagent;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.MessageCodes;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.travagent.AgentCollectionHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class AgentSelfTopupRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(AgentSelfTopupRequestHandler.class);

	private static XBEConfig xbeConfig = new XBEConfig();
	private static final String PARAM_HDNMODE = "hdnMode";
	private static final String LOCAL_CURRENCY = "L";
	private static final String BASE_CURRENCY = "B";
	private static final String SAVESUCCESS = "SAVESUCCESS";
	private static AgentDAO agentDAO = AirTravelagentsModuleUtils.getAgentDAO();

	public static String execute(HttpServletRequest request) throws ModuleException {
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		Agent agent = agentDAO.getAgent(userPrincipal.getAgentCode());
		if (!agent.getPaymentMethod().stream()
				.anyMatch(method -> method.equals(ReservationInternalConstants.PaymentMode.ONACCOUNT))) {
			log.debug("User does not have onAccount payment method for self topup");
			throw new ModuleException(MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN);
		}
		String strHdnMode = request.getParameter(PARAM_HDNMODE);
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (strHdnMode != null && strHdnMode.equals("saveSuccess")) {
			String saveStatus = (String) request.getSession().getAttribute(S2Constants.Session_Data.SELF_TOPUP_MSGTXT);
			if (SAVESUCCESS.equals(saveStatus)) {
				saveMessage(request, xbeConfig.getMessage("cc.airadmin.add.success", userLanguage), WebConstants.MSG_SUCCESS);				
				request.setAttribute(WebConstants.REQ_POPUPMESSAGE,
						xbeConfig.getMessage(WebConstants.KEY_ADD_SUCCESS, userLanguage));
			} else {
				request.setAttribute(WebConstants.REQ_POPUPMESSAGE, xbeConfig.getMessage("pay.error.default", userLanguage));
			}
			request.getSession().removeAttribute(S2Constants.Session_Data.SELF_TOPUP_MSGTXT);
		}

		try {
			setClientErrors(request);
			setApplicationBaseCurr(request);
			setPGWList(request);
			String strFormData = "var arrFormData = new Array();";
			strFormData += getCCTopupFormData(request);
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			request.setAttribute(WebConstants.REQ_DEFAULT_AIRLINE_CODE,
					AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode());
		} catch (Exception e) {
			log.error("\nAgentSelfTopupRequestHandler SELF TOPUP pageload FAILED " + e.getMessage());
		}
		return forward;
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = AgentCollectionHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static void setApplicationBaseCurr(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_BASE_CURRENCY, AppSysParamsUtil.getBaseCurrency());
	}

	private static void setPGWList(HttpServletRequest request) throws ModuleException {
		String strList = SelectListGenerator.createTopupPGWList();
		if (hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_CC_PAYMENT)) {
			strList = SelectListGenerator.createTopupPGWList();
		}
		request.setAttribute(WebConstants.REQ_HTML_PGW_LIST, strList);
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (xbeConfig.getMessage("cc.airadmin.savemessage.display", null).equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * Creates Agent Self Topup page's Form Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Created Form Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String getCCTopupFormData(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		String strAgentName = "";
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		String strAgentId = user.getAgentCode();
		sb.append("arrFormData[0] = '" + strAgentId + "';");

		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(strAgentId);
		Currency agentCurrency = null;
		BigDecimal balanceLocal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal crdLimit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal avlBalanceForsettlement = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal avlBspBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal balanceBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		boolean displayInAgentCurrency = false;

		if (agent != null) {
			strAgentName = agent.getAgentName();
			if (agent.getAgentSummary() != null) {
				balanceBase = agent.getAgentSummary().getAvailableCredit();
				crdLimit = agent.getCreditLimit();
				avlBspBalance = agent.getAgentSummary().getAvailableBSPCredit();
				avlBalanceForsettlement = agent.getAgentSummary().getAvailableFundsForInv();
				amountDue = AccelAeroCalculator.subtract(crdLimit, balanceBase);
			}
		}
		agentCurrency = ModuleServiceLocator.getCommonServiceBD().getCurrency(agent.getCurrencyCode());
		sb.append("arrFormData[1] = '" + (strAgentName == null ? "" : strAgentName) + "';");
		sb.append("arrFormData[2] = '" + decimalFormat.format(balanceBase) + "';");
		sb.append("arrFormData[3] = '" + decimalFormat.format(amountDue) + "';");
		sb.append("arrFormData[4] = '" + decimalFormat.format(avlBalanceForsettlement) + "';");
		sb.append("arrFormData[5] = '" + decimalFormat.format(crdLimit) + "';");
		if (!AppSysParamsUtil.getBaseCurrency().equals(agent.getCurrencyCode())) {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			balanceLocal = BLUtil.getAmountInLocal(balanceBase, agent.getCurrencyCode(), exchangeRateProxy);
			sb.append("arrFormData[6] = '" + LOCAL_CURRENCY + "';");
			displayInAgentCurrency = true;
		} else {
			sb.append("arrFormData[6] = '" + BASE_CURRENCY + "';");
		}

		sb.append("arrFormData[7] = '" + agent.getCurrencyCode() + "';");
		sb.append("arrFormData[8] = '" + decimalFormat.format(avlBspBalance) + "';");
		sb.append("arrFormData[9] = '" + agent.getEmailId() + "';");
		sb.append("arrFormData[10] = '" + agent.getTelephone() + "';");
		sb.append("arrFormData[11] = '" + agent.getMobile() + "';");

		sb.append("arrFormData[12] = '" + decimalFormat.format(balanceLocal) + "';");
		sb.append("arrFormData[13] = '" + agent.getEmailId() + "';");
		sb.append("arrFormData[14] = '" + agent.getTelephone() + "';");
		sb.append("arrFormData[15] = '" + agent.getMobile() + "';");
		sb.append("arrFormData[16] = '" + agentCurrency.getDecimalPlaces() + "';");

		Currency baseCurrency = ModuleServiceLocator.getCommonServiceBD().getCurrency(AppSysParamsUtil.getBaseCurrency());
		sb.append("arrFormData[17] = '" + baseCurrency.getDecimalPlaces() + "';");
		sb.append("arrFormData[18] = '" + convertTopupMinAmountToDisplayCurrency(displayInAgentCurrency, agent.getCurrencyCode())
				+ "';");

		return sb.toString();
	}

	private static String convertTopupMinAmountToDisplayCurrency(boolean displayInAgentCurrency, String agentCurrency)
			throws ModuleException {

		BigDecimal minimumAmout = AccelAeroCalculator.getDefaultBigDecimalZero();

		String strAmount = AppSysParamsUtil.getAgentTopupMinAmount();
		if (strAmount != null && !strAmount.trim().isEmpty()) {
			minimumAmout = new BigDecimal(strAmount);
			if (displayInAgentCurrency) {
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				minimumAmout = exchangeRateProxy.convert(agentCurrency, minimumAmout);
			}
		}

		return AccelAeroCalculator.formatAsDecimal(minimumAmout);
	}

}