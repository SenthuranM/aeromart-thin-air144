package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.FareTypeInfoTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class MultiCityFareQuoteAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(MultiCityFareQuoteAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private Collection<FlightInfoTO> flightInfo;

	private FareTypeInfoTO availableFare;

	private boolean currencyRound = false;

	private String airportMessage;

	private boolean resetAll = false;

	private String groupPNR;

	private ReservationProcessParams rpParams;

	private String classOfServiceDesc;

	private boolean showFareDiscountCodes = false;

	private List<String[]> fareDiscountCodeList = null;

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private boolean domFareDiscountApplied = false;

	private DiscountedFareDetails appliedFareDiscount = null;

	private HashMap<String, List<String>> subJourneyGroupMap;

	private String dummyCreditDiscountAmount = "0.00";

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();
	
	private HashMap<String, List<String>> segmentsMapPerSegType;
	
	private boolean displayBasedOnTemplates = false;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			boolean isAllowFlightSearchAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);

			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, AppIndicatorEnum.APP_XBE);

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			boolean isGoshowAgent = false;

			if (AppSysParamsUtil.isOnlyGoshoFaresVisibleInFlightCutoffTime()) {
				isGoshowAgent = ModuleServiceLocator.getAirportBD().isGoshoAgent(fareQuoteParams.getFromAirport());
			}
			if (isGoshowAgent && isAllowFlightSearchAfterCutOffTime) {
				fareQuoteParams.setBookingType(BookingClass.BookingClassType.OVERBOOK);
			}
			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());
			flightPriceRQ.getAvailPreferences().setRestrictionLevel(
					AvailabilityConvertUtil.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats,
							fareQuoteParams.getBookingType()));
			flightPriceRQ.getAvailPreferences().setQuoteFares(true);
			flightPriceRQ.getAvailPreferences().setTravelAgentCode(fareQuoteParams.getTravelAgentCode());

			if (isGoshowAgent) {
				flightPriceRQ.getAvailPreferences().setGoshoFareAgent(true);
			} else if (ReservationUtil.isCharterAgent(request)) {
				flightPriceRQ.getAvailPreferences().setFixedFareAgent(true);
			}

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (isGroupPNR) {
				flightPriceRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}

			if (log.isDebugEnabled()) {
				if (flightPriceRQ.getTransactionIdentifier() != null) {
					log.debug("MultiCity Fare Quote " + flightPriceRQ.getTransactionIdentifier());
				}
			}
			flightPriceRQ.getAvailPreferences().setMultiCitySearch(true);

			AnalyticsLogger.logAvlSearch(AnalyticSource.XBE_MSC_FQ, flightPriceRQ, request, getTrackInfo());
			
			ReservationApiUtils.replaceOnDInformationForFareQuoteAfterCityBaseSearch(flightPriceRQ);

			FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ,
					getTrackInfo());

			currencyRound = CommonUtil.enableCurrencyRounding(fareQuoteParams.getSelectedCurrency());
			showFareDiscountCodes = AppSysParamsUtil.enableFareDiscountCodes();
			if (showFareDiscountCodes) {
				fareDiscountCodeList = SelectListGenerator.createFareDiscountCodeList();
			}

			// TODO check scenarios where fare quote might not have valid selected price flight info
			PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();
			bookingShoppingCart.setPriceInfoTO(priceInfoTO);
			bookingShoppingCart.addNewServiceTaxes(flightPriceRS.getApplicableServiceTaxes());

			if (priceInfoTO != null) {
				// handling charge is not considered for modifications

				this.calculateTotalHandlingFee();

				ReservationProcessParams rParm = new ReservationProcessParams(request,
						resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
				BigDecimal handlingCharge = BigDecimal.ZERO;
				handlingCharge = bookingShoppingCart.getHandlingCharge();
				handlingCharge = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, handlingCharge);

				ArrayList<String> domesticSegmentCodeList = new ArrayList<String>();
				ArrayList<String> internationalSegmentCodeList = new ArrayList<String>();

				flightInfo = ReservationBeanUtil.createSelecteFlightInfo(flightPriceRS, domesticSegmentCodeList,
						internationalSegmentCodeList, null, false);
				
				segmentsMapPerSegType = ReservationBeanUtil.getSegmentsMap(flightInfo);
				
				flightInfo = SortUtil.sortFlightSegBySubJourneyGroup(new ArrayList<FlightInfoTO>(flightInfo));

				BigDecimal fareDiscountAmount = BigDecimal.ZERO;

				if (ReservationBeanUtil.hasIntAndDomFlights(domesticSegmentCodeList, internationalSegmentCodeList)) {
					bookingShoppingCart.setDomesticFareDiscount(flightPriceRQ.getTravelerInfoSummary()
							.getPassengerTypeQuantityList(), domesticSegmentCodeList);
					DiscountedFareDetails discountedFareDetails = bookingShoppingCart.getFareDiscount();

					if (discountedFareDetails.isDomesticDiscountApplied()) {
						ReservationUtil.calculateDiscount(bookingShoppingCart, flightPriceRQ,
								flightPriceRS.getTransactionIdentifier(), getTrackInfo());

						fareDiscountAmount = bookingShoppingCart.getTotalFareDiscount(true);
						if (fareDiscountAmount.doubleValue() > 0) {
							domFareDiscountApplied = true;
						}
					}

				}

				if (fareDiscountAmount.doubleValue() > 0) {
					appliedFareDiscount = bookingShoppingCart.getFareDiscount();
				} else {
					bookingShoppingCart.setFareDiscount(flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());					
					ReservationUtil.calculateDiscount( bookingShoppingCart,  flightPriceRQ, flightPriceRS.getTransactionIdentifier(),getTrackInfo());
					
					BigDecimal promoDiscountAmt = null;
					promoDiscountAmt = bookingShoppingCart.getTotalFareDiscount(true);
					appliedFareDiscount = bookingShoppingCart.getFareDiscount();

					if (promoDiscountAmt == null || promoDiscountAmt.doubleValue() == 0) {
						BigDecimal actualDiscount = bookingShoppingCart.getTotalFareDiscount(false);
						if (actualDiscount != null) {
							dummyCreditDiscountAmount = actualDiscount.toPlainString();
						}
					}

					fareDiscountAmount = promoDiscountAmt;
				}

				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				availableFare = ReservationUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(),
						flightPriceRS.getOriginDestinationInformationList(), handlingCharge, fareQuoteParams, rParm,
						fareDiscountAmount, exchangeRateProxy, priceInfoTO.getFareSegChargeTO(), flightPriceRQ, getTrackInfo(), null);
				availableFare.setAllRequestedOndsHasFares(priceInfoTO.isAllRequestedOndsHasFares());
				
				String serviceTax= availableFare.getFareQuoteSummaryTO().getTotalServiceTax();
				request.getSession().setAttribute("serviceTax", serviceTax);

				setClassOfServiceDesc(ReservationBeanUtil.getClassOfServiceDescription(flightInfo));

				List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
				ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);

				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());

			}

			if (priceInfoTO != null && flightPriceRS.getSubJourneyGroupMap() != null
					&& flightPriceRS.getSubJourneyGroupMap().size() > 0) {
				this.subJourneyGroupMap = flightPriceRS.getSubJourneyGroupMap();
			}

			// Hide Stop Over Airport
			if (AppSysParamsUtil.isHideStopOverEnabled()) {
				if (availableFare != null) {
					getHideStopOverAirportForFareQuote(availableFare);
				}

				if (flightInfo != null) {
					for (FlightInfoTO flightInfoTo : flightInfo) {
						String originDest = flightInfoTo.getOrignNDest();
						if (originDest != null && originDest.split("/").length > 2) {
							flightInfoTo.setOrignNDest(ReservationApiUtils.hideStopOverSeg(originDest));
						}
					}
				}
			}
			
			displayBasedOnTemplates= AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed();

			if (availableFare != null) {
				FareQuoteSummaryTO fareQuoteSummaryTo = availableFare.getFareQuoteSummaryTO();
				BigDecimal administrationFee = ReservationUtil.calclulateAdministrationFee(bookingShoppingCart, request, false,
						resInfo.getTransactionId(), getTrackInfo(), fareQuoteParams, flightPriceRS.getAllFlightSegments(), null,
						null, false);

				fareQuoteSummaryTo.setAdminFee(administrationFee.toString());
				// converting Admin Fee to selected Currency
				String selectedCurrency = fareQuoteParams.getSelectedCurrency();

				CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
						.getCurrencyExchangeRate(selectedCurrency);
				Currency currency = currencyExchangeRate.getCurrency();
				fareQuoteSummaryTo.setSelectedEXRate(currencyExchangeRate.getExrateBaseToCurNumber().toPlainString());
				BigDecimal total = new BigDecimal(fareQuoteSummaryTo.getTotalPrice());
				BigDecimal selectedTotPrice = AccelAeroCalculator.add(total, administrationFee);
				fareQuoteSummaryTo.setSelectedtotalPrice(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), selectedTotPrice, currency.getBoundryValue(),
						currency.getBreakPoint())));
				// to display admin fee in selected currency
				fareQuoteSummaryTo.setSelectCurrAdminFee(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(
						currencyExchangeRate.getMultiplyingExchangeRate(), administrationFee, currency.getBoundryValue(),
						currency.getBreakPoint())));

				// Adding AdminFee to total price.
				fareQuoteSummaryTo
						.setTotalPrice(new BigDecimal(fareQuoteSummaryTo.getTotalPrice()).add(administrationFee).toString());
			}

			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightPriceRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, null);

			rpParams = (ReservationProcessParams) request.getSession()
					.getAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS);

		} catch (ModuleException me) {
			log.error(me.getMessage(), me);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(me,userLanguage);
			if (me.getExceptionCode().equals("airinventory.arg.farequote.flightclosed")) {
				resetAll = true;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	private void getHideStopOverAirportForFareQuote(FareTypeInfoTO fareType) {
		Collection<FareQuoteTO> FareQuoteTOs = fareType.getFareQuoteTO();
		for (FareQuoteTO fareQuoteTo : FareQuoteTOs) {
			String ondCode = fareQuoteTo.getOndCode();
			if (ondCode != null && ondCode.split("/").length > 2) {
				fareQuoteTo.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode), false);
			}
			Collection<SegmentFareTO> segmentWiseFares = fareQuoteTo.getSegmentWise();
			for (SegmentFareTO segmentFareTo : segmentWiseFares) {
				String fareSegment = segmentFareTo.getSegmentCode();
				segmentFareTo.setSegmentCode(ReservationApiUtils.hideStopOverSeg(fareSegment));
			}
		}
	}

	private void clearValues() {
		flightInfo = null;
		currencyRound = false;
		flightInfo = null;
		availableFare = null;

	}

	private void calculateTotalHandlingFee() throws ModuleException {

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
				S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		PaxSet paxSet = new PaxSet(fareQuoteParams.getAdultCount(), fareQuoteParams.getChildCount(),
				fareQuoteParams.getInfantCount());
		ExternalChargeUtil.calculateExternalChargeAmount(paxSet, bookingShoppingCart.getPriceInfoTO(), bookingShoppingCart
				.getAllExternalChargesMap().get(EXTERNAL_CHARGES.HANDLING_CHARGE), false); // FIXME pass the return
																							// jouney flag

		bookingShoppingCart.addSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE);
	}

	@JSON(serialize = false)
	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public Collection<FlightInfoTO> getFlightInfo() {
		return flightInfo;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public FareTypeInfoTO getAvailableFare() {
		return availableFare;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public boolean isResetAll() {
		return resetAll;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public ReservationProcessParams getRpParams() {
		return rpParams;
	}

	public void setRpParams(ReservationProcessParams rpParams) {
		this.rpParams = rpParams;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}

	public boolean isShowFareDiscountCodes() {
		return showFareDiscountCodes;
	}

	public void setShowFareDiscountCodes(boolean showFareDiscountCodes) {
		this.showFareDiscountCodes = showFareDiscountCodes;
	}

	public boolean isDisplayBasedOnTemplates() {
		return displayBasedOnTemplates;
	}

	public List<String[]> getFareDiscountCodeList() {
		return fareDiscountCodeList;
	}

	public void setFareDiscountCodeList(List<String[]> fareDiscountCodeList) {
		this.fareDiscountCodeList = fareDiscountCodeList;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	public boolean isDomFareDiscountApplied() {
		return domFareDiscountApplied;
	}

	public void setDomFareDiscountApplied(boolean domFareDiscountApplied) {
		this.domFareDiscountApplied = domFareDiscountApplied;
	}

	public DiscountedFareDetails getAppliedFareDiscount() {
		return appliedFareDiscount;
	}

	public void setAppliedFareDiscount(DiscountedFareDetails appliedFareDiscount) {
		this.appliedFareDiscount = appliedFareDiscount;
	}

	public String getDummyCreditDiscountAmount() {
		return dummyCreditDiscountAmount;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public HashMap<String, List<String>> getSubJourneyGroupMap() {
		return subJourneyGroupMap;
	}

	public void setSubJourneyGroupMap(HashMap<String, List<String>> subJourneyGroupMap) {
		this.subJourneyGroupMap = subJourneyGroupMap;
	}
	
	public HashMap<String, List<String>> getSegmentsMapPerSegType() {
		return segmentsMapPerSegType;
	}

	public void setSegmentsMapPerSegType(HashMap<String, List<String>> segmentsMapPerSegType) {
		this.segmentsMapPerSegType = segmentsMapPerSegType;
	}
}
