package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class for Meals
 * 
 * @author Dilan Anuruddha
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class DuplicateCheckAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(DuplicateCheckAction.class);

	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO searchParams;

	private String selectedFlightList;

	private String paxInfo;
	
	private String ffid;
	
	private boolean ffidAvailability;

	private boolean hasDuplicates = false;

	private String system;

	private String firstName;

	private String lastName;

	private char isEmailConfirmed;


	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil
					.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
			List<LCCClientReservationPax> paxList = AncillaryJSONUtil.extractPaxOnly(paxInfo, ApplicationEngine.XBE);
			if (hasReservationDuplicates(paxList)) {
				this.hasDuplicates = true;
			} else {
				DuplicateValidatorAssembler dva = new DuplicateValidatorAssembler();
				dva.setTargetSystem(SYSTEM.getEnum(this.system));
				dva.setFlightSegments(flightSegmentTOs);
				dva.setPaxList(paxList);
				this.hasDuplicates = ModuleServiceLocator.getAirproxyReservationBD().checkForDuplicates(dva, null ,getTrackInfo());
			}
			resInfo.setDuplicates(hasDuplicates);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("MEAL REQ ERROR", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public boolean hasReservationDuplicates(List<LCCClientReservationPax> paxList) {
		List<String> adultNameList = new ArrayList<String>();
		for (LCCClientReservationPax pax : paxList) {
			if (PaxTypeTO.ADULT.equals(pax.getPaxType()) || PaxTypeTO.CHILD.equals(pax.getPaxType())) {
				String fullName = BeanUtils.nullHandler(pax.getTitle()) + ReservationInternalConstants.DLIM
						+ BeanUtils.nullHandler(pax.getFirstName()) + ReservationInternalConstants.DLIM
						+ BeanUtils.nullHandler(pax.getLastName());
				fullName = fullName.toUpperCase();
				if (fullName.length() > 0 && !fullName.contains(com.isa.thinair.webplatform.api.util.ReservationUtil.TBA_USER)) {
					adultNameList.add(fullName);
				}
			}
		}

		// Validate the duplicate names in the current setup
		Set<String> currentNameSet = new HashSet<String>();
		currentNameSet.addAll(adultNameList);

		if (currentNameSet.size() < adultNameList.size()) {
			return true;
		}
		return false;
	}

	public String ffidCheck(){
		LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
		LmsMemberServiceBD lmsMemberServiceBD = ModuleServiceLocator.getLmsMemberServiceBD();
		ffidAvailability = true;
		try {
			LoyaltyMemberCoreDTO member = loyaltyManagementBD.getLoyaltyMemberCoreDetails(ffid);
			Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(ffid);
			if ((customer != null && customer.getLMSMemberDetails() != null)
					&& LmsCommonUtil.availableForCarrier(ffid, lmsMemberServiceBD)) {
				if (member != null && member.getMemberFirstName() != null && member.getMemberLastName() != null
						&& !member.getMemberFirstName().trim().isEmpty() && !member.getMemberLastName().trim().isEmpty()) {
					setFirstName(member.getMemberFirstName());
					setLastName(member.getMemberLastName());
				} else {
					setFirstName(customer.getLMSMemberDetails().getFirstName());
					setLastName(customer.getLMSMemberDetails().getLastName());
				}
				setIsEmailConfirmed(customer.getLMSMemberDetails().getEmailConfirmed());
			} else {
				ffidAvailability = false;
			}

		} catch (ModuleException me) {
			log.error("DuplicateCheckAction ==>", me);
			ffidAvailability = false;
		}
		return S2Constants.Result.SUCCESS;
	}
	
	/**
	 * getters and setters
	 */
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setPaxInfo(String paxInfo) {
		this.paxInfo = paxInfo;
	}

	public boolean isHasDuplicates() {
		return hasDuplicates;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public boolean isFfidAvailability() {
		return ffidAvailability;
	}

	public void setFfidAvailability(boolean ffidAvailability) {
		this.ffidAvailability = ffidAvailability;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public char getIsEmailConfirmed() {
		return isEmailConfirmed;
	}

	public void setIsEmailConfirmed(char isEmailConfirmed) {
		this.isEmailConfirmed = isEmailConfirmed;
	}

}
