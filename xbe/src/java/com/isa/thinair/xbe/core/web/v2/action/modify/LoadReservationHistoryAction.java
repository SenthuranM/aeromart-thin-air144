package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airproxy.api.dto.UserNoteHistoryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadReservationHistoryAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadReservationHistoryAction.class);

	private boolean success = true;
	private String messageTxt;
	private Collection<UserNoteHistoryTO> reservationAuditList;
	private String pnr;
	private String carrierCode;
	private String groupPNR;
	private String originSalesChanel;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		String strForward = S2Constants.Result.SUCCESS;

		try {
			boolean isGroupPNR = false;

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
					? resInfo.getReservationStatus()
					: null, null, resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);
			if (groupPNR != null && !groupPNR.trim().equals("") && !groupPNR.trim().equalsIgnoreCase("null")) {
				isGroupPNR = true;
			} else {
				groupPNR = pnr;
				carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			}
			if (carrierCode != null && carrierCode.equalsIgnoreCase("LCC")) {
				carrierCode = null;
			}
			if (isGroupPNR) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), null, resInfo.isModifiableReservation(), resInfo.getLccPromotionInfoTO());
			}
			List<UserNoteTO> reservationHistoryList = ModuleServiceLocator.getAirproxyReservationBD().searchReservationAudit(
					groupPNR, carrierCode, isGroupPNR, originSalesChanel, getTrackInfo());
			this.reservationAuditList = ReservationUtil.getReservationHistory(reservationHistoryList);

			request.setAttribute("initialParams", JSONUtil.serialize(rpParams));

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}
		return strForward;
	}

	public Collection<UserNoteHistoryTO> getReservationAuditList() {
		return reservationAuditList;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setOriginSalesChanel(String originSalesChanel) {
		this.originSalesChanel = originSalesChanel;
	}

}
