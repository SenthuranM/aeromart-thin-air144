package com.isa.thinair.xbe.core.web.v2.action.voucher;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.VoucherTemplateBD;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.generator.voucher.VoucherHTMLGenerator;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * @author chethiya
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowGiftVoucherDetailAction extends BaseRequestAwareAction {

	Log log = LogFactory.getLog(ShowGiftVoucherDetailAction.class);

	private String errorList;

	private String reqPayAgents;

	private ArrayList<VoucherTemplateTo> voucherTemplateList;

	private ArrayList<VoucherTemplateTo> salesPeriodTemplateList;

	public String execute() {
		if (AppSysParamsUtil.isVoucherEnabled()) {
			VoucherTemplateBD voucherTemplateDelegate = ModuleServiceLocator.getVoucherTemplateBD();
			try {
				setErrorList(VoucherHTMLGenerator.getGiftVoucherClientErrors());
			} catch (ModuleException me) {
				log.error("ShowGiftVoucherDetailAction ==> execute()", me);
			}
			try {
				setVoucherTemplateList(voucherTemplateDelegate.getVouchers());
				setSalesPeriodTemplateList(voucherTemplateDelegate.getVoucherInSalesPeriod());
				reqPayAgents = ReservationUtil.getPaymentAgentOptions(request);
			} catch (Exception e) {
				log.error("ShowGiftVoucherDetailAction ==> execute()", e);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String getErrorList() {
		return errorList;
	}

	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}

	public ArrayList<VoucherTemplateTo> getVoucherTemplateList() {
		return voucherTemplateList;
	}

	public void setVoucherTemplateList(ArrayList<VoucherTemplateTo> voucherTemplateList) {
		this.voucherTemplateList = voucherTemplateList;
	}

	public String getReqPayAgents() {
		return reqPayAgents;
	}

	public void setReqPayAgents(String reqPayAgents) {
		this.reqPayAgents = reqPayAgents;
	}

	public ArrayList<VoucherTemplateTo> getSalesPeriodTemplateList() {
		return salesPeriodTemplateList;
	}

	public void setSalesPeriodTemplateList(ArrayList<VoucherTemplateTo> salesPeriodTemplateList) {
		this.salesPeriodTemplateList = salesPeriodTemplateList;
	}
}

