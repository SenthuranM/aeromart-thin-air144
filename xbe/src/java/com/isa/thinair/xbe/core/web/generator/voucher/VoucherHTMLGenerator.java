package com.isa.thinair.xbe.core.web.generator.voucher;

import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

/**
 * @author chethiya
 *
 */
public class VoucherHTMLGenerator {

	Log log = LogFactory.getLog(VoucherHTMLGenerator.class);

	private static String clientErrors;

	private static String currencyList;
	
	private static String rePaxClientErrors;

	/**
	 * @return
	 * @throws ModuleException
	 */
	public static String getVoucherClientErrors() throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("voucher.details.fname.empty", "fnameEmpty");
			moduleErrs.setProperty("voucher.details.fname.invalid", "fnameInvalid");
			moduleErrs.setProperty("voucher.details.lname.empty", "lnameEmpty");
			moduleErrs.setProperty("voucher.details.lname.invalid", "lnameInvalid");
			moduleErrs.setProperty("voucher.details.email.invalid", "emailInvalid");
			moduleErrs.setProperty("voucher.details.email.empty", "emailEmpty");
			moduleErrs.setProperty("voucher.details.fromdate.empty", "fromDateEmpty");
			moduleErrs.setProperty("voucher.details.todate.empty", "toDateEmpty");
			moduleErrs.setProperty("voucher.details.sales.period.invalid", "periodInvalid");
			moduleErrs.setProperty("voucher.details.amount.invalid", "amountInvalid");
			moduleErrs.setProperty("voucher.details.amount.empty", "amountEmpty");
			moduleErrs.setProperty("cc.airadmin.save.success", "saveSuccess");
			moduleErrs.setProperty("voucher.details.status.invalid", "statusInvalid");
			moduleErrs.setProperty("voucher.details.cancelremarks.empty", "emptyCancelRemarks");
			moduleErrs.setProperty("voucher.email.success", "emailSuccess");
			moduleErrs.setProperty("voucher.print.success", "printSuccess");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;

	}
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static String getCurrencyList(HttpServletRequest request) throws Exception {
		Currency agentCurrency = RequestHandlerUtil.getAgentCurrency(request);
		Entry baseCurrency = DatabaseUtil.getBaseCurrency();
		currencyList = JavaScriptGenerator.createCurrencyHtml(request, baseCurrency, agentCurrency);
		return currencyList;
	}

	/**
	 * @return
	 * @throws ModuleException
	 */
	public static String getGiftVoucherClientErrors() throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("gift.voucher.details.fname.empty", "fnameEmpty");
			moduleErrs.setProperty("gift.voucher.details.fname.invalid", "fnameInvalid");
			moduleErrs.setProperty("gift.voucher.details.lname.empty", "lnameEmpty");
			moduleErrs.setProperty("gift.voucher.details.lname.invalid", "lnameInvalid");
			moduleErrs.setProperty("gift.voucher.details.email.invalid", "emailInvalid");
			moduleErrs.setProperty("gift.voucher.details.email.empty", "emailEmpty");
			moduleErrs.setProperty("gift.voucher.details.name.empty", "nameEmpty");
			moduleErrs.setProperty("gift.voucher.details.payment.type.empty", "paymentMethodEmpty");
			moduleErrs.setProperty("cc.airadmin.save.success", "saveSuccess");
			moduleErrs.setProperty("gift.voucher.details.cancelremarks.empty", "emptyCancelRemarks");
			moduleErrs.setProperty("gift.voucher.email.success", "emailSuccess");
			moduleErrs.setProperty("gift.voucher.print.success", "printSuccess");
			moduleErrs.setProperty("gift.voucher.cancel.success", "cancelSuccess");
			moduleErrs.setProperty("gift.voucher.payment.cc.card", "ccTypeEmpty");
			moduleErrs.setProperty("gift.voucher.payment.cc.number.empty", "ccNumberEmpty");
			moduleErrs.setProperty("gift.voucher.payment.cc.number.invalid", "ccNumberInvalid");
			moduleErrs.setProperty("gift.voucher.payment.cc.name.empty", "ccNameEmpty");
			moduleErrs.setProperty("gift.voucher.payment.cc.name.invalid", "ccNameInvalid");
			moduleErrs.setProperty("gift.voucher.payment.cc.expiry.empty", "ccExpiryEmpty");
			moduleErrs.setProperty("gift.voucher.payment.cc.expiry.invalid", "ccExpiryInvalid");
			moduleErrs.setProperty("gift.voucher.payment.cc.cvv.empty", "ccCvvEmpty");
			moduleErrs.setProperty("gift.voucher.payment.cc.cvv.invalid", "ccCvvInvalid");
			moduleErrs.setProperty("gift.voucher.payment.oa.agent", "oaAgentEmpty");
			moduleErrs.setProperty("gift.voucher.payment.oa.confirm", "oaConfirm");
			moduleErrs.setProperty("gift.voucher.quantity.empty", "noQuantity");
			moduleErrs.setProperty("gift.voucher.select.empty", "noSelect");
			moduleErrs.setProperty("gift.voucher.req.incomplete", "errorReq");
			moduleErrs.setProperty("gift.voucher.mnumber.empty", "mobileNumberEmpty");
			moduleErrs.setProperty("gift.voucher.remarks.too.long", "remarksTooLong");
			
			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;

	}
	
	public static String getRePaxErrors() throws ModuleException{
		
		if (rePaxClientErrors == null) {
			Properties moduleErrs = new Properties();			
			moduleErrs.setProperty("repax.voucher.fltnum.empty", "fltNumInvalid");
			moduleErrs.setProperty("repax.voucher.select.pax", "selectPax");
			moduleErrs.setProperty("repax.voucher.frmdate.empty", "fromDateEmpty");
			moduleErrs.setProperty("repax.voucher.todate.empty", "toDateInvalid");
			moduleErrs.setProperty("repax.voucher.period.invalid", "periodInvalid");
			moduleErrs.setProperty("repax.voucher.amount.empty", "amountEmpty");
			moduleErrs.setProperty("repax.voucher.amount.invalid", "amountInvalid");	
			moduleErrs.setProperty("repax.voucher.issue.success", "issueSuccess");
			moduleErrs.setProperty("repax.voucher.pf.invalid", "percentageInvalid");	
			moduleErrs.setProperty("repax.voucher.srchdate.empty", "srchDateEmpty");	
			moduleErrs.setProperty("repax.voucher.srchdate.invalid", "srchDateInvalid");
			    
			rePaxClientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}		
		return rePaxClientErrors;
	}

}
