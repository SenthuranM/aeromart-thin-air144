package com.isa.thinair.xbe.core.web.v2.action.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * POJO to handle the Passenger Tab
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class PassengerTabAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(PassengerTabAction.class);

	private boolean success = true;

	private String messageTxt;

	public String execute() throws ModuleException {
		return S2Constants.Result.SUCCESS;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}
}
