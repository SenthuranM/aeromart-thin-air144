package com.isa.thinair.xbe.core.config;

import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class XBEModuleUtils {

	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	public static XBEModuleConfig getConfig() {
		return (XBEModuleConfig) LookupServiceFactory.getInstance().getBean("isa:base://modules/xbe?id=xbeConfig");
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}
}
