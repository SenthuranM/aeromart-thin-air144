//TODO include license header.

package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.FareDetailsReportRH;

/**
 * The Struts Action class for Fare Details Report.
 * 
 * @author M.Rikaz
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({
		@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.REPORTING_FARE_DETAILS_JSP),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowFareDetailsReportAction extends BaseRequestResponseAwareAction {
	public String execute() throws Exception {
		return FareDetailsReportRH.execute(request, response);
	}
}
