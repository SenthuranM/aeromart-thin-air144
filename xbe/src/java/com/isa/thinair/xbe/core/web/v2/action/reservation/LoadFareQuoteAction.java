package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.OpenReturnInfo;
import com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.FareTypeInfoTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Load Fare Quote
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadFareQuoteAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(LoadFareQuoteAction.class);

	private FlightSearchDTO fareQuoteParams;

	private boolean success = true;

	private String messageTxt;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private Collection<FlightInfoTO> flightInfo;

	private FareTypeInfoTO availableFare;

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private boolean currencyRound = false;

	private String airportMessage;

	private boolean resetAll = false;

	private String groupPNR;

	private ReservationProcessParams rpParams;

	private boolean addSegment = false;

	private boolean transferSegment = false;

	private boolean modifyBooking = false;

	private String classOfServiceDesc;

	private OpenReturnInfo openReturnInfo;

	private boolean isOpenReturn = false;

	private boolean showFareDiscountCodes = false;

	private boolean showAgentCommission = false;

	private List<String[]> fareDiscountCodeList = null;

	private DiscountedFareDetails appliedFareDiscount = null;

	private String dummyCreditDiscountAmount = "0.00";

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();

	private HashMap<String, List<String>> segmentsMapPerSegType;

	private BaggageRatesDTO baggageRatesDTO;
	
	private boolean displayBasedOnTemplates = false;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			boolean hasPrivViewAvailability = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			boolean isAllowFlightSearchAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);
			boolean isAllowOverBookAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME);
			boolean isAllowOverBookBeforeCutOffTime = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_OVER_BOOKING);
			String pnr = request.getParameter("pnr");

			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, outFlightRPHList,
					retFlightRPHList);

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			flightPriceRQ.getAvailPreferences().setAllowFlightSearchAfterCutOffTime(isAllowFlightSearchAfterCutOffTime);
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookBeforeCutOffTime(isAllowOverBookBeforeCutOffTime);
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(isAllowOverBookAfterCutOffTime);

			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());
			flightPriceRQ.getAvailPreferences().setRestrictionLevel(AvailabilityConvertUtil
					.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats, fareQuoteParams.getBookingType()));
			flightPriceRQ.getAvailPreferences().setQuoteFares(true);

			if (pnr != null && !"".equals(pnr)) {
				flightPriceRQ.getAvailPreferences().setModifyBooking(true);
				flightPriceRQ.getAvailPreferences().setAddSegment(addSegment);
				ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"),
						request.getParameter("modifySegment"));
			}

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (isGroupPNR) {
				flightPriceRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}

			if (log.isDebugEnabled()) {
				if (flightPriceRQ.getTransactionIdentifier() != null) {
					log.debug("Fare quote" + flightPriceRQ.getTransactionIdentifier());
				}
			}

			flightPriceRQ.getAvailPreferences().setTravelAgentCode(fareQuoteParams.getTravelAgentCode());

			if (ReservationUtil.isCharterAgent(request)) {
				flightPriceRQ.getAvailPreferences().setFixedFareAgent(true);
			}
			if (flightPriceRQ.getTravelPreferences().isOpenReturnConfirm()) {
				ReservationUtil.setOpenReturnConfirmInfo(flightPriceRQ, pnr, getTrackInfo());
			}

			if (pnr != null && !"".equals(pnr)) {
				AnalyticsLogger.logAvlSearch(AnalyticSource.XBE_MOD_FQ, flightPriceRQ, request, getTrackInfo());
			} else {
				AnalyticsLogger.logAvlSearch(AnalyticSource.XBE_CRE_FQ, flightPriceRQ, request, getTrackInfo());

			}
			
			ReservationApiUtils.replaceOnDInformationForFareQuoteAfterCityBaseSearch(flightPriceRQ);
			
			displayBasedOnTemplates = AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed();
			
			FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ,
					getTrackInfo());

			// Collection<AvailableFlightInfo> outboundFlights =
			// ReservationBeanUtil.createAvailFlightInfoList(flightPriceRS,
			// fareQuoteParams, hasPrivViewAvailability, viewFlightsWithLesserSeats, "depature", false, false, null);
			// Collection<AvailableFlightInfo> returnFlights =
			// ReservationBeanUtil.createAvailFlightInfoList(flightPriceRS,
			// fareQuoteParams, hasPrivViewAvailability, viewFlightsWithLesserSeats, "return", false, false, null);

			currencyRound = CommonUtil.enableCurrencyRounding(fareQuoteParams.getSelectedCurrency());

			showFareDiscountCodes = AppSysParamsUtil.enableFareDiscountCodes();
			if (showFareDiscountCodes) {
				fareDiscountCodeList = SelectListGenerator.createFareDiscountCodeList();
			}

			showAgentCommission = AppSysParamsUtil.isAgentCommmissionEnabled(); // handle on modifications.
			// TODO check scenarios where fare quote might not have valid selected price flight info
			PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();
			bookingShoppingCart.setPriceInfoTO(priceInfoTO);

			isOpenReturn = (flightPriceRS.getOpenReturnOptionsTO() != null);
			if (isOpenReturn) {
				openReturnInfo = new OpenReturnInfo(flightPriceRS.getOpenReturnOptionsTO(), fareQuoteParams.getFromAirport(),
						fareQuoteParams.getToAirport());
			}
			bookingShoppingCart.addNewServiceTaxes(flightPriceRS.getApplicableServiceTaxes());

			Collection<EXTERNAL_CHARGES> resettingExtChargee = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
			resettingExtChargee.add(EXTERNAL_CHARGES.CREDIT_CARD);
			resettingExtChargee.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
			bookingShoppingCart.resetSelectedExternalChargeAmount(resettingExtChargee);

			if (priceInfoTO != null) {
				// handling charge is not considered for modifications
				if (isCalculateHandlingFee()) {
					this.calculateTotalHandlingFee();
				}

				ReservationProcessParams rParm = new ReservationProcessParams(request,
						resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
				BigDecimal handlingCharge = BigDecimal.ZERO;
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				handlingCharge = bookingShoppingCart.getHandlingCharge();
				handlingCharge = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, handlingCharge);

				bookingShoppingCart.setFareDiscount(flightPriceRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
				if (bookingShoppingCart.getFareDiscount() != null
						&& !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE
								.equals(bookingShoppingCart.getFareDiscount().getPromotionType())) {
					ReservationUtil.calculateDiscountForReservation(bookingShoppingCart, bookingShoppingCart.getPaxList(),
							flightPriceRQ, getTrackInfo(), true, bookingShoppingCart.getFareDiscount(), priceInfoTO,
							flightPriceRS.getTransactionIdentifier());
				}

				BigDecimal promoDiscountAmt = null;
				promoDiscountAmt = bookingShoppingCart.getTotalFareDiscount(true);
				appliedFareDiscount = bookingShoppingCart.getFareDiscount();

				if (promoDiscountAmt == null || promoDiscountAmt.doubleValue() == 0) {
					BigDecimal actualDiscount = bookingShoppingCart.getTotalFareDiscount(false);
					if (actualDiscount != null) {
						dummyCreditDiscountAmount = actualDiscount.toPlainString();
					}
				}

				availableFare = ReservationUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(),
						flightPriceRS.getOriginDestinationInformationList(), handlingCharge, fareQuoteParams, rParm,
						promoDiscountAmt, exchangeRateProxy, priceInfoTO.getFareSegChargeTO(), flightPriceRQ, getTrackInfo(),
						null);
				bookingShoppingCart.setAgentCommissions(availableFare.getApplicableAgentCommissions());
				bookingShoppingCart.setTotAgentCommission(availableFare.getTotAgentCommission());
				
				String serviceTax= availableFare.getFareQuoteSummaryTO().getTotalServiceTax();
				request.getSession().setAttribute("serviceTax", serviceTax);

				flightInfo = ReservationBeanUtil.createSelecteFlightInfo(flightPriceRS, null, null, null, false);
				setClassOfServiceDesc(ReservationBeanUtil.getClassOfServiceDescription(flightInfo));
				segmentsMapPerSegType = ReservationBeanUtil.getSegmentsMap(flightInfo);
				List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();

				ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);
				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());
			}

			// Hide Stop Over Airport
			if (AppSysParamsUtil.isHideStopOverEnabled()) {
				if (availableFare != null) {
					getHideStopOverAirportForFareQuote(availableFare);
				}

				if (flightInfo != null) {
					for (FlightInfoTO flightInfoTo : flightInfo) {
						String originDest = flightInfoTo.getOrignNDest();
						if (originDest != null && originDest.split("/").length > 2) {
							flightInfoTo.setOrignNDest(ReservationApiUtils.hideStopOverSeg(originDest));
						}
					}
				}
			}

			// CalculateAdminFee
			// in Availabilty page pax details are not availiable,
			// we took dummy values for paxState,paxCountryCode and paxTaxRegistered.

			this.setBaggageRatesDTO(ModuleServiceLocator.getAirproxyAncillaryBD().getBaggageRates(getTrackInfo(),
					flightPriceRS, fareQuoteParams.getClassOfService(), fareQuoteParams.getClassOfService(),
					flightPriceRQ.getAvailPreferences().getSearchSystem().toString(), ApplicationEngine.XBE,
					userLanguage));
			if (availableFare != null && availableFare.getFareQuoteSummaryTO() != null) {
				FareQuoteSummaryTO fareQuoteSummaryTo = availableFare.getFareQuoteSummaryTO();
				BigDecimal administrationFee = ReservationUtil.calclulateAdministrationFee(bookingShoppingCart, request, false,
						resInfo.getTransactionId(), getTrackInfo(), fareQuoteParams, flightPriceRS.getAllFlightSegments(), null,
						null, false);
				fareQuoteSummaryTo.setAdminFee(administrationFee.toString());
				// converting Admin Fee to selected Currency
				String selectedCurrency = fareQuoteParams.getSelectedCurrency();
				CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
						.getCurrencyExchangeRate(selectedCurrency);
				Currency currency = currencyExchangeRate.getCurrency();
				fareQuoteSummaryTo.setSelectedEXRate(currencyExchangeRate.getExrateBaseToCurNumber().toPlainString());
				BigDecimal total = new BigDecimal(fareQuoteSummaryTo.getTotalPrice());
				BigDecimal selectedTotPrice = AccelAeroCalculator.add(total, administrationFee);
				fareQuoteSummaryTo.setSelectedtotalPrice(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy
						.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), selectedTotPrice,
								currency.getBoundryValue(), currency.getBreakPoint())));
				// to display admin fee in selected currency
				fareQuoteSummaryTo.setSelectCurrAdminFee(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy
						.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), administrationFee,
								currency.getBoundryValue(), currency.getBreakPoint())));

				// Adding AdminFee to total price.
				fareQuoteSummaryTo.setTotalPrice(new BigDecimal(fareQuoteSummaryTo.getTotalPrice()).add(administrationFee)
						.toString());
			}
			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightPriceRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, null);

			// rpParams = new ReservationProcessParams(request, resInfo != null ? resInfo.getReservationStatus() : null,
			// null, true);

			// if (flightPriceRQ.getAvailPreferences().getSearchSystem() == SYSTEM.INT) {
			// if (flightPriceRS.getReservationParms() != null) {
			// rpParams.enableInterlineModificationParams(flightPriceRS.getReservationParms(),
			// resInfo != null ? resInfo.getReservationStatus() : null, null, true);
			// }
			// }

			rpParams = (ReservationProcessParams) request.getSession()
					.getAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS);

		} catch (ModuleException me) {
			log.error(me.getMessage(), me);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(me, userLanguage);
			if (me.getExceptionCode().equals("airinventory.arg.farequote.flightclosed")) {
				resetAll = true;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	private void getHideStopOverAirportForFareQuote(FareTypeInfoTO fareType) {
		Collection<FareQuoteTO> FareQuoteTOs = fareType.getFareQuoteTO();
		for (FareQuoteTO fareQuoteTo : FareQuoteTOs) {
			String ondCode = fareQuoteTo.getOndCode();
			if (ondCode != null && ondCode.split("/").length > 2) {
				fareQuoteTo.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode), false);
			}
			Collection<SegmentFareTO> segmentWiseFares = fareQuoteTo.getSegmentWise();
			for (SegmentFareTO segmentFareTo : segmentWiseFares) {
				String fareSegment = segmentFareTo.getSegmentCode();
				segmentFareTo.setSegmentCode(ReservationApiUtils.hideStopOverSeg(fareSegment));
			}
		}

		/*
		 * FareRulesInformationDTO fareRuleInfoDto = fareType.getFareRulesInfo(); List<FareRuleDTO> fareRules =
		 * fareRuleInfoDto.getFareRules(); for(FareRuleDTO fareRule : fareRules){ String fareRuleSeg =
		 * fareRule.getOrignNDest(); fareRule.setOrignNDest(ReservationApiUtils.hideStopOverSeg(fareRuleSeg)); }
		 * 
		 * FlexiFareRulesInformationDTO flexiFareRulesInfoDTO = fareType.getFlexiFareRulesInfo(); List<FareRuleDTO>
		 * flexiRules = flexiFareRulesInfoDTO.getFareRules(); for(FareRuleDTO flexiFareRule : flexiRules){ String
		 * fareRuleSeg = flexiFareRule.getOrignNDest();
		 * flexiFareRule.setOrignNDest(ReservationApiUtils.hideStopOverSeg(fareRuleSeg)); }
		 */
	}

	private void clearValues() {
		flightInfo = null;
		currencyRound = false;
		outFlightRPHList = null;
		retFlightRPHList = null;
		flightInfo = null;
		availableFare = null;
		ondLogicalCCList = null;
	}

	private boolean isCalculateHandlingFee() {
		return (!modifyBooking && !addSegment && !transferSegment && !fareQuoteParams.isOpenReturnConfirm());
	}

	private void calculateTotalHandlingFee() throws ModuleException {

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		PaxSet paxSet = new PaxSet(fareQuoteParams.getAdultCount(), fareQuoteParams.getChildCount(),
				fareQuoteParams.getInfantCount());
		ExternalChargeUtil.calculateExternalChargeAmount(paxSet, bookingShoppingCart.getPriceInfoTO(),
				bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.HANDLING_CHARGE),
				(retFlightRPHList != null && retFlightRPHList.size() > 0));

		bookingShoppingCart.addSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE);
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public Collection<FlightInfoTO> getFlightInfo() {
		return flightInfo;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public FareTypeInfoTO getAvailableFare() {
		return availableFare;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public boolean isResetAll() {
		return resetAll;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public ReservationProcessParams getRpParams() {
		return rpParams;
	}

	public void setRpParams(ReservationProcessParams rpParams) {
		this.rpParams = rpParams;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public boolean isTransferSegment() {
		return transferSegment;
	}

	public void setTransferSegment(boolean transferSegment) {
		this.transferSegment = transferSegment;
	}

	public boolean isModifyBooking() {
		return modifyBooking;
	}

	public void setModifyBooking(boolean modifyBooking) {
		this.modifyBooking = modifyBooking;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	/**
	 * @return the openReturnInfo
	 */
	public OpenReturnInfo getOpenReturnInfo() {
		return openReturnInfo;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	public boolean isShowFareDiscountCodes() {
		return showFareDiscountCodes;
	}

	public void setShowFareDiscountCodes(boolean showFareDiscountCodes) {
		this.showFareDiscountCodes = showFareDiscountCodes;
	}

	public List<String[]> getFareDiscountCodeList() {
		return fareDiscountCodeList;
	}

	public void setFareDiscountCodeList(List<String[]> fareDiscountCodeList) {
		this.fareDiscountCodeList = fareDiscountCodeList;
	}

	public DiscountedFareDetails getAppliedFareDiscount() {
		return appliedFareDiscount;
	}

	public String getDummyCreditDiscountAmount() {
		return dummyCreditDiscountAmount;
	}

	public boolean isShowAgentCommission() {
		return showAgentCommission;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public void setOndWiseTotalFlexiCharge(List<String> ondWiseTotalFlexiCharge) {
		this.ondWiseTotalFlexiCharge = ondWiseTotalFlexiCharge;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public void setOndWiseTotalFlexiAvailable(List<Boolean> ondWiseTotalFlexiAvailable) {
		this.ondWiseTotalFlexiAvailable = ondWiseTotalFlexiAvailable;
	}

	public HashMap<String, List<String>> getSegmentsMapPerSegType() {
		return segmentsMapPerSegType;
	}

	public void setSegmentsMapPerSegType(HashMap<String, List<String>> segmentsMapPerSegType) {
		this.segmentsMapPerSegType = segmentsMapPerSegType;
	}

	public BaggageRatesDTO getBaggageRatesDTO() {
		return baggageRatesDTO;
	}

	public void setBaggageRatesDTO(BaggageRatesDTO baggageRatesDTO) {
		this.baggageRatesDTO = baggageRatesDTO;
	}

	public boolean isDisplayBasedOnTemplates() {
		return displayBasedOnTemplates;
	}


}