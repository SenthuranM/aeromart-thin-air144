package com.isa.thinair.xbe.core.web.action.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

import javax.sql.DataSource;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Test.COMMON_TESTING_UI),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Test.COMMON_TESTING_UI) })
public class EmailTestAction extends BaseRequestAwareAction {

	/**
	 * Execute Process
	 */
	public String execute() {
		final ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
		DataSource ds = getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		String sql = " SELECT pnr from t_reservation";
		final Object count = null;

		try {
			template.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					while (rs.next()) {
						String pnr = rs.getString("pnr");
						ItineraryLayoutModesDTO itineraryLayoutModesDTO = new ItineraryLayoutModesDTO();
						itineraryLayoutModesDTO.setPnr(pnr);
						itineraryLayoutModesDTO.setLocale(new Locale("en"));
						itineraryLayoutModesDTO.setIncludePassengerPrices(false);
						try {
							reservationBD.emailItinerary(itineraryLayoutModesDTO, null, null, false);
						} catch (Exception e) {
							System.out.println("an exception occured");
						}
					}
					return count;

				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}

		return S2Constants.Result.SUCCESS;
	}

	private static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

}
