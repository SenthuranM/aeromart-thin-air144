package com.isa.thinair.xbe.core.web.handler.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.utils.AirSecurityUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.v2.LanguagesTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.util.TravelAgentUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JSONGenerator;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.reservation.ReservationHG;
import com.isa.thinair.xbe.core.web.generator.system.DashboardHTMLGenerator;
import com.isa.thinair.xbe.core.web.generator.system.MenuHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AirportDDListGenerator;
import com.isa.thinair.xbe.core.web.v2.util.ItineraryUtil;

public class ShowMenuRH extends BasicRH {

	private static Log log = LogFactory.getLog(ShowMenuRH.class);

	private static GlobalConfig globalConfig = XBEModuleUtils.getGlobalConfig();

	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if(userLanguage == null || userLanguage.isEmpty()){
			request.getSession().setAttribute(WebConstants.REQ_LANGUAGE, WebConstants.DEFAULT_LANGUAGE);
			userLanguage = WebConstants.DEFAULT_LANGUAGE;
		}
		
		
		try {
			setDisplayData(request);
		} catch (Exception ex) {
			String msg = BasicRH.getErrorMessage(ex,userLanguage);
			forward = S2Constants.Result.ERROR;
			log.error(msg, ex);
			JavaScriptGenerator.setServerError(request, msg, "top", "");
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		
		Entry baseCurency = DatabaseUtil.getBaseCurrency();
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		Currency agentCurrency = RequestHandlerUtil.getAgentCurrency(request);
		boolean isPasswordReset = RequestHandlerUtil.isPasswordReset(user.getUserId());
		if (AirSecurityUtil.isPasswordExpire(user.getPasswordExpiryDate())) {
			// TODO use different variable to propagate the expiry date 
			isPasswordReset = true;
		}		
		
		setPrivileges(request);
		setAriportOwnList(request);
		setAirportsHtml(request);
		setOndMapAsPerAgent(request);
		setAirportsHtmlV2(request);
		setCurrencyHtml(request, baseCurency, agentCurrency);
		setTitleHtml(request);
		setPaxTypesHtml(request);
		setTitleVisibleHtml(request);
		setInfantTitleHtml(request);
		setSalesPointsHtml(request);
		setSalesPointsWithDryHtml(request);
		setAllTravelAgentsHtml(request);
		setCountryHtml(request);
		setNationalityHtml(request);
		setCardTypeHtml(request);
		setLogicalCabinClassEnableParam(request);
		setCOSHtml(request);
		setCOSWithLogicalHtml(request);
		setYearHtml(request);
		setTermsConditionsHtml(request);
		setMenuHtml(request, isPasswordReset,userLanguage );
		setDefaultData(request, agentCurrency);
		setSSRCodes(request);
		setStandbyhtml(request);
		setSearchOptionhtml(request);
		setReservationSearchOptionsHtml(request);
		setBookingClassCodesHtml(request);
		setBookingCategoryHtml(request);
		setAllBookingCategoryList(request);
		setItineraryFareMaskHtml(request);
		setAllCouponStatusList(request);
		setAllPaxStatusList(request);
		setSelectedLanguage(request);

//		setAllTravelAgentsCollection(request);
		setOwnTravelAgentsHtml(request);
		setAllIPGsHtml(request);
		setAllOfflineIPGsHtml(request);
		setModificationEnabledOfflineIPGs(request);
		setItineraryLanguages(request);
		setClientErrors(request);
		setRequestHtml(request);
		setCarrierName(request);
		setCarrierCode(request);
		setCarrierAirlineCodeMap(request);
		setChildVisibility(request);
		setPaxCategory(request);
		setPaxCategoryFOID(request);
		setFareCategories(request);
		setTitleHtmlWithChild(request);
		setFareCategoriesAll(request);
		setCountryPhoneNos(request);
		setDupNameCheckEnable(request);
		setCreditCardTypes(request);
		setCreditCardMsgsNErrrs(request);

		setAgentCurrencyDetails(request, agentCurrency);
		changePassword(request, isPasswordReset);
		setReturnValidity(request);
		setBookingClassType(request);
		setRedirectLink(request);
		setActualPaymentMethodData(request);
		DashboardHTMLGenerator.setDashboardContent(request);
		setItinerarySetting(request);
		setReportSchedulerAppParam(request);
		setWindowHeigth(request);
		setXBETracking(request);
		setAutomaticSeatAssignment(request);
		setAgentFareMaskStatus(request, user);
		setCabinClassRankingMap(request);
		setSessionKeepAlive(request);
		setIsRequestNationalIDForReservationsHavingDomesticSegments(request);
		setCountryStateMap(request);
		setHubCountryMap(request);
		setAgentTaxRegNo(request, user);
		setGSTMessageEnabledAirports(request);
		setGSTMessage(request);

		setShowAgentCreditInAgentCurrency(request, agentCurrency);
		
		
		globalConfig.resetLoadedAgents();
	}
	
	private static void setIsRequestNationalIDForReservationsHavingDomesticSegments(HttpServletRequest request){
		request.setAttribute(WebConstants.REQ_NIC_MANDATORY_FOR_DOMESTIC_SEGMENTS, AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments());
	}

	private static void setSessionKeepAlive(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_SESSION_KEEP_ALIVE_IN_MINS, XBEModuleUtils.getConfig()
				.getSessionKeepAliveIntervalInMinutes());
	}

	private static void setAutomaticSeatAssignment(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_AUTO_SEAT_ASSIGNMENT_ENABLED, JavaScriptGenerator.createAutoSeatAssignmentValue());
		request.setAttribute(WebConstants.REQ_AUTO_SEAT_ASSIGNMENT_START_ROW,
				JavaScriptGenerator.createAutoSeatAsssignemntStartingRowNo());
	}

	private static void setXBETracking(HttpServletRequest request) {
		try {
			request.setAttribute("reqXBETrackingEnabled", AppSysParamsUtil.isXBETrackingEnable());
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static void setWindowHeigth(HttpServletRequest request) {
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		int myHeight = 0;
		if (userPrincipal.getScreenHeight() != null && !userPrincipal.getScreenHeight().equals("")) {
			myHeight = Integer.parseInt(userPrincipal.getScreenHeight().trim());
			myHeight = myHeight - 80;
		} else {
			myHeight = 478;
		}
		request.setAttribute("ViewPortHeight", myHeight);
	}

	private static void setCreditCardMsgsNErrrs(HttpServletRequest request) {
		String ccMsg = "";
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if ("G9".equals(AppSysParamsUtil.getDefaultCarrierCode())) {
			ccMsg = XBEConfig.getClientMessage("msg.fltSearch.spMsgCC", userLanguage);
		} else if ("3O".equals(AppSysParamsUtil.getDefaultCarrierCode())) {
			ccMsg = XBEConfig.getClientMessage("msg.fltSearch.spMsgCC2", userLanguage);
		}
		request.setAttribute("spMsgCC", ccMsg);
	}

	private static void setCreditCardTypes(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_HTML_CC_TYPES_FOR_PGW, JSONGenerator.createCreditCardTypesForAllPGW());
	}

	private static void setDupNameCheckEnable(HttpServletRequest request) {
		request.setAttribute("reqDupNameCheck", AppSysParamsUtil.getDuplicateNameCheckEnabled());

	}

	private static void setRequestHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		String userId = user.getUserId();
		String userName = user.getDisplayName();
		String agentCode = user.getAgentCode();
		Agent agent = TravelAgentUtil.loadTravelAgent(agentCode);
		String strAgentName = JavaScriptGenerator.nullConvertToString(agent.getAgentDisplayName());
		String secureContextPath = BasicRH.getSecureContextPath(request);
		String strFFPNumber = (user.getFfpNumber() != null) ? user.getFfpNumber() : "";

		sb.append("var request=new Array();");
		sb.append("request['secureLoginUrl']='" + secureContextPath + "';");
		sb.append("request['userId']='" + userId + "';");
		sb.append("request['userName']='" + userName + "';");
		sb.append("request['agentCode']='" + agentCode + "';");
		sb.append("request['agentTypeCode']='" + agent.getAgentTypeCode() + "';");
		sb.append("request['secureContextPath']='" + secureContextPath + "';");
		sb.append("request['debugFrontend']='" + SystemPropertyUtil.isDebugFrontend() + "';");
		sb.append("request['cosLegend']='"
				+ ReservationApiUtils
						.getCabinClassesLegend(CommonsServices.getGlobalConfig().getActiveCabinClassesMap().size() == 1 ? false
								: true) + "';");

		request.setAttribute(WebConstants.REQ_HTML_REQUEST_DATA, sb.toString());
		if (AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers()) {
			if (userId.length() > 3) {
				userId = userId.substring(3);
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_LOGIN_UID, userId);
		request.setAttribute(WebConstants.REQ_HTML_AGENT_CODE, agentCode);
		request.setAttribute(WebConstants.REQ_HTML_AGENT_NAME, strAgentName);
		request.setAttribute(WebConstants.REQ_FFP_NUMBER, strFFPNumber);
	}

	private static void setDefaultData(HttpServletRequest request, Currency agentCurrency) throws ModuleException {
		request.setAttribute(WebConstants.PARAM_SES_AGENT_BALANCE, ReservationHG.getUsersAvailableCredit(request));
		request.setAttribute(WebConstants.PARAM_SYSTEM_DETAILS,
				JavaScriptGenerator.createDefaultValuesHtml(request, agentCurrency));
	}

	private static void setAirportsHtml(HttpServletRequest request) throws ModuleException {
		// if (AppSysParamsUtil.isLCCConnectivityEnabled() && hasPrivilege(request,
		// PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
		// request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST_V2,
		// JavaScriptGenerator.createAirportsHtmlV2(request,true));
		// }else {
		// request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST_V2,
		// JavaScriptGenerator.createAirportsHtmlV2(request,false));
		// }
		boolean isSubStationsAllowed = AppSysParamsUtil.isGroundServiceEnabled();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST_V2,
				AirportDDListGenerator.prepareAirportsHtml(request, null, "arrAirportsV2", isSubStationsAllowed));
	}
	
	private static void setOndMapAsPerAgent(HttpServletRequest request) throws ModuleException{
		if(AppSysParamsUtil.isEnableRouteSelectionForAgents()){
			boolean isSubStationsAllowed = AppSysParamsUtil.isGroundServiceEnabled();
			request.setAttribute(WebConstants.REQ_HTML_AGENT_WISE_ONDS,
					AirportDDListGenerator.prepareOndMapAsPerAgent(request, null, "arrOndMap", isSubStationsAllowed));
		}
	}

	private static void setAirportsHtmlV2(HttpServletRequest request) throws ModuleException {
		if (AppSysParamsUtil.isLCCConnectivityEnabled() && hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, JavaScriptGenerator.createAirportsHtml(request, true));
		} else {
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, JavaScriptGenerator.createAirportsHtml(request, false));
		}

	}

	private static void setAriportOwnList(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_OWNER_LIST, JavaScriptGenerator.createAirportOwnerList(request));
	}

	private static void setCurrencyHtml(HttpServletRequest request, Entry baseCurrency, Currency agentCurrency)
			throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_CURRENCY_LIST,
				JavaScriptGenerator.createCurrencyHtml(request, baseCurrency, agentCurrency));
	}

	private static void setTitleHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_TITLE_LIST, JavaScriptGenerator.createTitelHtmlForAdult(request));
		// request.setAttribute(Constants.REQ_HTML_TITLE_LIST_WITH_CHILD,
		// JavaScriptGenerator.createTitelHtmlWithChild(request));
	}

	private static void setTitleHtmlWithChild(HttpServletRequest request) throws ModuleException {
		// request.setAttribute(Constants.REQ_HTML_TITLE_LIST, JavaScriptGenerator.createTitelHtml(request));
		request.setAttribute(WebConstants.REQ_HTML_TITLE_LIST_WITH_CHILD, JavaScriptGenerator.createTitelHtmlWithChild(request));
	}

	private static void setPaxTypesHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_PAX_TYPES_LIST, JavaScriptGenerator.createPaxTypesHtml(request));
	}

	private static void setTitleVisibleHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_TITLE_VISIBLE_LIST, JavaScriptGenerator.createTiteVisibilityHtml(request));
	}

	private static void setInfantTitleHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_INFANT_TITLE_LIST, JavaScriptGenerator.createInfantTitelHtml(request));
	}

	private static void setSalesPointsHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_SALES_POINT_LIST, JavaScriptGenerator.createTAsWithCreditHtml(request));
	}

	private static void setSalesPointsWithDryHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_SALES_POINT_WITH_DRY_LIST,
				JavaScriptGenerator.createRefundTAWithCreditHtml(request));
	}

	private static void setAllTravelAgentsHtml(HttpServletRequest request) throws ModuleException {
		Collection colAgents = SelectListGenerator.getAllActiveTAsCollection();

		request.setAttribute(WebConstants.REQ_HTML_ALL_TRAVEL_AGENT_LIST,
				JavaScriptGenerator.createAllTravelAgentsHtml("arrAllTravelAgents", request, colAgents));

		request.setAttribute(WebConstants.REQ_HTML_ALL_TRANSFER_TRAVEL_AGENT_LIST,
				JavaScriptGenerator.createAllTravelAgentsHtml("arrAllTransferTravelAgents", request, colAgents));
	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	private static void setBookingCategoryHtml(HttpServletRequest request) throws ModuleException {

		Collection<String[]> bookingCategories = new ArrayList<String[]>();
		bookingCategories = SelectListGenerator.getAllBookingCategories();
		Collection<String[]> unprivilegedbookingCategories = new ArrayList<String[]>();

		for (String[] bookingType : bookingCategories) {
			if (!hasPrivilege(request, PriviledgeConstants.PRIVI_CREATE_STANDARD_BOOKING)
					&& bookingType[2].equalsIgnoreCase(PriviledgeConstants.PRIVI_CREATE_STANDARD_BOOKING)) {
				unprivilegedbookingCategories.add(bookingType);

			}
			if (!hasPrivilege(request, PriviledgeConstants.PRIVI_CREATE_CHARTER_BOOKING)
					&& bookingType[2].equalsIgnoreCase(PriviledgeConstants.PRIVI_CREATE_CHARTER_BOOKING)) {
				unprivilegedbookingCategories.add(bookingType);
			}

			if (!hasPrivilege(request, PriviledgeConstants.PRIVI_CREATE_HR_BOOKING)
					&& bookingType[2].equalsIgnoreCase(PriviledgeConstants.PRIVI_CREATE_HR_BOOKING)) {
				unprivilegedbookingCategories.add(bookingType);
			}
		}
		bookingCategories.removeAll(unprivilegedbookingCategories);
		request.setAttribute(WebConstants.REQ_BOOKING_CATEGORY_LIST,
				SelectListGenerator.getJSArray("arrBookingCategories", bookingCategories));

	}

	private static void setAllBookingCategoryList(HttpServletRequest request) throws ModuleException {
		Collection<String[]> bookingCategories = new ArrayList<String[]>();
		bookingCategories = SelectListGenerator.getAllBookingCategories();
		request.setAttribute(WebConstants.REQ_ALL_BOOKING_CATEGORY_LIST,
				SelectListGenerator.getJSArray("arrAllBookingCategoriesList", bookingCategories));
	}

	private static void setAllCouponStatusList(HttpServletRequest request) throws ModuleException {
		Collection<String[]> couponStatus = new ArrayList<String[]>();
		couponStatus = SelectListGenerator.getAllCouponStatusList();
		request.setAttribute(WebConstants.REQ_ALL_COUPON_STATUS_LIST,
				SelectListGenerator.getJSArray("arrAllCouponStatus", couponStatus));
	}

	private static void setAllPaxStatusList(HttpServletRequest request) throws ModuleException {
		Collection<String[]> paxStatus = new ArrayList<String[]>();
		paxStatus = SelectListGenerator.getAllPaxStatusList();
		request.setAttribute(WebConstants.REQ_ALL_PAX_STATUS_LIST, SelectListGenerator.getJSArray("arrAllPaxStatus", paxStatus));
	}

	private static void setItineraryFareMaskHtml(HttpServletRequest request) throws ModuleException {

		Collection<String[]> itineraryFareMaskOptions = new ArrayList<String[]>();

		itineraryFareMaskOptions.add(new String[] { "Y", "Yes" });
		itineraryFareMaskOptions.add(new String[] { "N", "No" });

		request.setAttribute(WebConstants.REQ_ITINERARY_FARE_MASK_OPTIONS,
				SelectListGenerator.getJSArray("arrItineraryFareMaskOptions", itineraryFareMaskOptions));

	}

	private static void setOwnTravelAgentsHtml(HttpServletRequest request) throws ModuleException {

		Collection colAgents = new ArrayList();

		colAgents = SelectListGenerator.getAllActiveOwnTAsCollection(AppSysParamsUtil.getDefaultAirlineIdentifierCode());

		request.setAttribute(WebConstants.REQ_HTML_OWN_TRAVEL_AGENT_LIST,
				JavaScriptGenerator.createAllTravelAgentsHtml("arrOwnTravelAgents", request, colAgents));
	}

	private static void setAllIPGsHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_ALL_IPGS_LIST, SelectListGenerator.createAllIPGsForXBE("arrIPGs"));
	}
	
	private static void setAllOfflineIPGsHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_ALL_OFFLINE_IPGS_LIST,
				SelectListGenerator.createAllOfflineIPGsForXBE("arrOfflineIPGs"));
	}

	private static void setModificationEnabledOfflineIPGs(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_MODIFCATION_ENABLED_OFFLINE_IPGS_LIST,
				SelectListGenerator.createModificationEnabledIPGsForXBE("arrOfflineIPGsModifyEnabled"));
	}

	private static void setItineraryLanguages(HttpServletRequest request) throws ModuleException {
		Collection<LanguagesTO> itineraryLangs = ItineraryUtil.getSupportedLanguages();
		String json = "var jsonItnLang = ";
		try {
			json += JSONUtil.serialize(itineraryLangs);
		} catch (JSONException e) {
			json += "[]";
		}
		json += ";";
		request.setAttribute(WebConstants.REQ_HTML_ITINERARY_LANG_LIST, json);
	}

//	private static void setAllTravelAgentsCollection(HttpServletRequest request) throws ModuleException {
//		Collection<String[]> colAgents = SelectListGenerator.getAllTAsCollection();
//		Map<String, String> mapAllAgentCodeDesc = getAgentsMap(colAgents);
//
//		request.getSession().setAttribute(WebConstants.SES_MAP_ALL_TRAVELS, mapAllAgentCodeDesc);
//	}

	private static Map<String, String> getAgentsMap(Collection<String[]> colAgents) {
		Map<String, String> mapAgents = new HashMap<String, String>();

		if (colAgents != null && !colAgents.isEmpty()) {
			for (Iterator<String[]> iter = colAgents.iterator(); iter.hasNext();) {
				String[] arrAgent = (String[]) iter.next();
				mapAgents.put(arrAgent[0], arrAgent[1]);
			}
		}

		return mapAgents;
	}

	private static void setCountryHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_SALES_COUNTRY_LIST, JavaScriptGenerator.createCountryHtml(request));
	}

	private static void setNationalityHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_NATIONALITY_LIST, JavaScriptGenerator.createNationalityHtml(request));
	}

	private static void setCardTypeHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_CARD_TYPE_LIST, JavaScriptGenerator.createCardTypeHtml(request));
	}

	private static void setYearHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.SES_HTML_YEAR_DATA, JavaScriptGenerator.createYearHtml(request));

	}

	private static void setTermsConditionsHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_TERMS_N_CONDITIONS, MenuHTMLGenerator.createTermsConditionsHtml(request));
	}

	private static void setCOSHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.SES_HTML_COS_DATA, JavaScriptGenerator.createCosHtml(request));
	}

	private static void setCOSWithLogicalHtml(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_COS_WITH_LOGICAL_DATA, JavaScriptGenerator.createCosWithLogicalHtml(request));
	}

	private static void setMenuHtml(HttpServletRequest request, boolean isPasswordReset, String language) throws ModuleException {
		String menuFile = request.getSession().getServletContext().getRealPath("/WEB-INF/MenuData.xml");
		HashMap hashMapPrevi = (HashMap) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (isPasswordReset) {
			hashMapPrevi.clear();
			request.setAttribute(WebConstants.REQ_HTML_MENU_DATA, MenuHTMLGenerator.createMenuHtml(menuFile, hashMapPrevi, language));
		} else {
			request.setAttribute(WebConstants.REQ_HTML_MENU_DATA, MenuHTMLGenerator.createMenuHtml(menuFile, hashMapPrevi, language));
		}
	}

	private static void setSSRCodes(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_SSRCODE_LIST, JavaScriptGenerator.createSSRHtml(request));
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = MenuHTMLGenerator.getClientErrors(request);
		BasicRH.saveClientErrors(request, strClientErrors);
	}

	private static void setPrivileges(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();

		Map mapPrivilegeIds = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		Set colKeys = removePrivilegeAcordingToAppParameter(mapPrivilegeIds.keySet());
		boolean isTrue = false;
		for (Iterator iter = colKeys.iterator(); iter.hasNext();) {
			String privilegeId = (String) iter.next();
			sb.append("arrPrivi['" + privilegeId + "']=1;");
		}

		request.setAttribute(WebConstants.REQ_PRIVILEGES, sb.toString());
	}

	private static Set removePrivilegeAcordingToAppParameter(Set colKeys) {
		if (!AppSysParamsUtil.isEnableElectronicMCO()) {
			colKeys.remove("xbe.tool.mco.display");
			colKeys.remove("rpt.res.mco.detail.view");
			colKeys.remove("rpt.res.mco.summary.view");			
		}
		if (!AppSysParamsUtil.isEnableRouteSelectionForAgents()) {
			colKeys.remove("ta.maint.route.selection");
		}
		
		if (!AppSysParamsUtil.isOnHoldEnable(OnHold.AGENTS)) {
			colKeys.remove("xbe.res.make.pay.offline.payment");
		}
		return colKeys;
	}

	private static void setStandbyhtml(HttpServletRequest request) throws ModuleException {
		StringBuffer standBF = new StringBuffer(" var arrBookingTypes = new Array(); ");
		standBF.append("arrBookingTypes[0] = new Array('" + BookingClass.BookingClassType.NORMAL + "','"
				+ BookingClass.BookingClassType.NORMAL + "');");
		int arrayIndex = 1;
		if (BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_STANDBY_BOOKING)) {
			standBF.append("arrBookingTypes[" + arrayIndex + "] = new Array('" + BookingClass.BookingClassType.STANDBY + "','"
					+ BookingClass.BookingClassType.STANDBY + "');");
			arrayIndex++;
		}
		if (BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_OVER_BOOKING)) {
			standBF.append("arrBookingTypes[" + arrayIndex + "] = new Array('" + BookingClass.BookingClassType.OVERBOOK + "','"
					+ BookingClass.BookingClassType.OVERBOOK + "');");
			arrayIndex++;
		}
		if (BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_WAITLISTING_BOOKING)) {
			standBF.append("arrBookingTypes[" + arrayIndex + "] = new Array('"
					+ ReservationInternalConstants.ReservationType.WL.getDescription() + "','"
					+ ReservationInternalConstants.ReservationType.WL.getDescription() + "');");
		}
		request.setAttribute(WebConstants.REQ_HTML_STANDBY_LIST, standBF.toString());
	}

	private static void setSearchOptionhtml(HttpServletRequest request) throws ModuleException {
		StringBuffer sf = new StringBuffer();
		String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		if (AppSysParamsUtil.isLCCConnectivityEnabled() && hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
			sf.append("arrSearchOptions[0] = new Array('" + SYSTEM.AA + "','Search " + defaultCarrier + " ');");
			// sf.append("arrSearchOptions[1] = new Array('"+SYSTEM.COND+"','Search Interline If No Results');");
			// sf.append("arrSearchOptions[2] = new Array('"+SYSTEM.INT+"','Search Interline');");
			// sf.append("arrSearchOptions[3] = new Array('"+SYSTEM.ALL+"','Search All');");
			sf.append("arrSearchOptions[1] = new Array('" + SYSTEM.INT + "','Search All');");
		} else {
			sf.append("arrSearchOptions[0] = new Array('" + SYSTEM.AA + "','Search " + defaultCarrier + " ');");
		}
		request.setAttribute(WebConstants.REQ_HTML_SEARCH_OPTION_LIST, sf.toString());
	}

	/*
	 * Method used to set the reservation search options - own airline or other systems. Only one privilege is checked
	 * as reservation search is not controlled airline wise
	 */
	private static void setReservationSearchOptionsHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer sf = new StringBuffer();
		String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		/**
		 * TODO : && hasPrivilege(request, PriviledgeConstants.PRIVILEGE) privilege is for dry bookings search
		 **/
		if (AppSysParamsUtil.isLCCConnectivityEnabled() && hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
			sf.append("arrReservationSearchOptions[0] = new Array('" + SYSTEM.AA + "','Search " + defaultCarrier + " ');");
			// sf.append("arrReservationSearchOptions[1] = new Array('"+SYSTEM.ALL+"','Search All');");
			sf.append("arrReservationSearchOptions[1] = new Array('" + SYSTEM.INT + "','Search All');");
		} else {
			sf.append("arrReservationSearchOptions[0] = new Array('" + SYSTEM.AA + "','Search " + defaultCarrier + " ');");
		}
		request.setAttribute(WebConstants.REQ_HTML_RESERVATION_SEARCH_OPTION_LIST, sf.toString());
	}

	private static void setChildVisibility(HttpServletRequest request) {
		String strVisibility = globalConfig.getBizParam(SystemParamKeys.CHILD_VISIBILITY);
		request.setAttribute("reqChildVisibility", strVisibility);
	}

	private static void setCarrierName(HttpServletRequest request) {
		String strCarrName = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME);
		request.setAttribute("reqCarrName", strCarrName);
	}

	/**
	 * Set Carrier Code
	 * 
	 * @param request
	 */
	private static void setCarrierCode(HttpServletRequest request) {
		String strCarrCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		request.setAttribute("reqCarrCode", strCarrCode.toUpperCase());
	}

	private static void setCarrierAirlineCodeMap(HttpServletRequest request) throws ModuleException {
		Map<String, String> carrierAirlineCodeMap = globalConfig.getCarrierAirlineCodeMap();
		request.setAttribute("reqCarrierAirlineCodeMap", JSONGenerator.createCarrierAirlineCodeMap(carrierAirlineCodeMap));
	}

	/**
	 * Method to retrieve the pax category.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setPaxCategory(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_CUS_TYPE_LIST,
				JavaScriptGenerator.createPaxCategory(request, WebConstants.RECORD_STATUS_ACTIVE));
	}

	/**
	 * Method to retrieve the pax category foid.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setPaxCategoryFOID(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_PAX_CATEGORY_FOID_LIST,
				JavaScriptGenerator.createPaxCategoryFOID(request, WebConstants.RECORD_STATUS_ACTIVE));

	}

	/**
	 * Method to retrieve the fare category.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setFareCategories(HttpServletRequest request) throws ModuleException {
		String html = JavaScriptGenerator.createActiveFareCategories("arrFareCat");
		request.setAttribute(WebConstants.REQ_HTML_FARE_CAT_LIST, html);
	}

	/**
	 * Method to retrieve the fare category. different array name required.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setFareCategoriesAll(HttpServletRequest request) throws ModuleException {
		String html = JavaScriptGenerator.createActiveFareCategories("arrFareCatAll");
		request.setAttribute(WebConstants.REQ_HTML_FARE_CAT_LIST_ALL, html);
	}

	private static void setCountryPhoneNos(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createTelephoneArray();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_PHONE_LIST, strHtml);
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_PHONE_JSON, JSONGenerator.createCountryPhoneList());

	}

	private static void setAgentCurrencyDetails(HttpServletRequest request, Currency agentCurrency) throws ModuleException {

		String html = JavaScriptGenerator.setAgentCurrencyDetails(agentCurrency);
		request.setAttribute(WebConstants.REQ_AGENT_CURR_DET, html);
	}

	private static void changePassword(HttpServletRequest request, boolean isPasswordReset) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_PWD_RESETED, isPasswordReset);
	}

	private static void setReturnValidity(HttpServletRequest request) throws ModuleException {
		String html = JavaScriptGenerator.createReturnValidities("arrRetValidity");
		request.setAttribute(WebConstants.REQ_HTML_RETURN_VALIDITY, html);
	}

	private static void setBookingClassType(HttpServletRequest request) throws ModuleException {
		String html = JavaScriptGenerator.createBCTypes("arrBCType");
		request.setAttribute(WebConstants.REQ_HTML_BC_TYPE, html);
	}

	private static void setRedirectLink(HttpServletRequest request) {
		String menuLink = request.getParameter("menuLink");
		request.setAttribute("menuLink", menuLink);
	}

	private static void setActualPaymentMethodData(HttpServletRequest request) throws ModuleException {

		request.setAttribute(WebConstants.REQ_HTML_ACTUAL_PAYMENT_METHOD_DATA, JSONGenerator.createActualPaymentMethodList());
	}

	private static void setItinerarySetting(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.TICKET_PRINT_EMAIL_PAX_WISE_FORMAT, AppSysParamsUtil.isPrintEmailIndItinerary());
	}

	private static void setBookingClassCodesHtml(HttpServletRequest request) throws ModuleException {
		String html = JavaScriptGenerator.createBookingClassCodesList(request);
		request.setAttribute(WebConstants.BOOKING_CLASS_CODES, html);
	}

	private static void setReportSchedulerAppParam(HttpServletRequest request) throws ModuleException {
		Boolean isReportSchedulingEnabled = AppSysParamsUtil.isScheduledReportsAllowed();
		request.setAttribute(WebConstants.REQ_HTML_REPORT_SCHEDULER_APP_PARAM, isReportSchedulingEnabled.toString());
	}

	private static void setLogicalCabinClassEnableParam(HttpServletRequest request) {
		String strParam = "var isLogicalCCEnabled = " + AppSysParamsUtil.isLogicalCabinClassEnabled() + "; ";
		request.setAttribute(WebConstants.REQ_LOGICAL_CABIN_CLASS_ENABLED, strParam);
	}
	
	private static void setLCCConnectivityEnableParam(HttpServletRequest request) {
		String strParam = "var isLCCConnectivityEnabled = " + AppSysParamsUtil.isLCCConnectivityEnabled() + "; ";
		request.setAttribute(WebConstants.REQ_LOGICAL_CABIN_CLASS_ENABLED, strParam);
	}

	private static void setAgentFareMaskStatus(HttpServletRequest request, User user) {
		String param = JavaScriptGenerator.getAgentFareMaskStatus(user.getAgentCode());
		request.setAttribute(WebConstants.REQ_AGENT_FAREMASK_STATUS, param);
	}

	private static void setCabinClassRankingMap(HttpServletRequest request) throws ModuleException {
		Map<String, Integer> cabinClassRankingMap = JavaScriptGenerator.createCabinClassRankingMap(request);
		request.setAttribute(WebConstants.REQ_CABINCLASS_RANKING_MAP,
				JSONGenerator.createCabinClassRankingMap(cabinClassRankingMap));
	}
	
	private static void setShowAgentCreditInAgentCurrency(HttpServletRequest request, Currency agentCurrency)
			throws ModuleException {

		String strAgentCurrency = agentCurrency.getCurrencyCode();
		String baseCurency = AppSysParamsUtil.getBaseCurrency();

		if (AppSysParamsUtil.showAgentCreditInAgentCurrency() && !baseCurency.equals(strAgentCurrency)) {

			request.setAttribute(WebConstants.PARAM_SES_SHOW_AGENT_BALANCE_IN_AGENT_CURRENCY, "true");
			request.setAttribute(WebConstants.PARAM_SES_AGENT_BALANCE_IN_AGENT_CURRENCY,
					ReservationHG.getUserAvailableCreditInUserCurrency(request, baseCurency));

		} else {

			request.setAttribute(WebConstants.PARAM_SES_SHOW_AGENT_BALANCE_IN_AGENT_CURRENCY, "false");
			request.setAttribute(WebConstants.PARAM_SES_AGENT_BALANCE_IN_AGENT_CURRENCY,
					ReservationHG.getUsersAvailableCredit(request));
		}
	}

	private static void setCountryStateMap(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_COUNTRY_STATE_MAP, SelectListGenerator.createCountryState());
	}
	
	private static void setAgentTaxRegNo(HttpServletRequest request,User user) throws ModuleException {
		String coAgentTaxRegNo = RequestHandlerUtil.getCOAgentTaxRegNo(user.getAgentCode());
		request.setAttribute(WebConstants.REQ_AGENT_TAX_REG_NO, coAgentTaxRegNo);
	}
	
	private static void setGSTMessageEnabledAirports(HttpServletRequest request){
		String gstMessageEnabledAirports = SelectListGenerator.getGSTMessageEnabledAirports();
		request.setAttribute(WebConstants.REQ_GST_MESSAGE_ENABLED_AIRPORTS, gstMessageEnabledAirports);
	}
	
	private static void setGSTMessage(HttpServletRequest request){
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		String gstMessage = XBEConfig.getClientMessage("msg.fltSearch.gstMsg", userLanguage);
		request.setAttribute(WebConstants.REQ_GST_MESSAGE, gstMessage);
	}
	
	private static void setHubCountryMap(HttpServletRequest request) throws ModuleException {
		Collection<String[]> hubCountries = new ArrayList<String[]>();
		hubCountries = SelectListGenerator.createHubCountry();
		request.setAttribute(WebConstants.REQ_HUB_COUNTRIES, SelectListGenerator.getJSArray("hubCountryArry", hubCountries));
	}
	
	private static void setSelectedLanguage(HttpServletRequest request) throws ModuleException {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		request.setAttribute(WebConstants.REQ_SELECTED_LANGUAGE, userLanguage);
	}

}
