package com.isa.thinair.xbe.core.web.generator.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.DateUtil;
import com.isa.thinair.xbe.core.util.TravelAgentUtil;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class JavaScriptGenerator {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String createAirportOwnerList(HttpServletRequest request) {
		String html = SelectListGenerator.careateAirportOwnerList();
		return html;
	}

	public static String createAirportsHtml(HttpServletRequest request, boolean includeInterline) throws ModuleException {

		String html = SelectListGenerator.createXBEAirportsListWOTag(includeInterline);

		return html;
	}

	public static String createAirportsHtmlV2(HttpServletRequest request, boolean includeInterline, List<String[]> filterList,
			String arrName, boolean isSubStationAllowed) throws ModuleException {
		// String html = SelectListGenerator.createXBEV2AirportsListWOTag(includeInterline);
		String html = SelectListGenerator.createXBEV2AirportsListWOTagWSub(includeInterline, filterList, arrName,
				isSubStationAllowed);

		return html;
	}
	
	public static String createOndMapAsPerAgent(HttpServletRequest request, boolean includeInterline, List<String[]> filterList,
			String arrName, boolean isSubStationAllowed) throws ModuleException {
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String agentCode = userPrincipal.getAgentCode();
		String html = SelectListGenerator.createOndMapAsPerAgent(includeInterline, filterList, arrName,
				isSubStationAllowed, agentCode);

		return html;
	}


	public static String createCurrencyHtml(HttpServletRequest request, Entry baseCurrency, Currency agentCurrency)
			throws ModuleException {
		StringBuffer strbData = new StringBuffer();

		if (agentCurrency != null) {
			if (agentCurrency.getStatus().equals(WebConstants.RECORD_STATUS_INACTIVE) || agentCurrency.getXBEVisibility() == 0) {
				agentCurrency = null;
			}
		}

		/*
		 * if (AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled()) { if (agentCurrency == null) {
		 * strbData.append("arrCurrency[0] = new Array('" + baseCurrency.getKey() + "','" + baseCurrency.getValue() +
		 * "');"); } else { strbData.append("arrCurrency[0] = new Array('" + agentCurrency.getCurrencyCode() + "','" +
		 * agentCurrency.getCurrencyDescriprion() + "');"); } } else { if (checkAccess(request,
		 * PriviledgeConstants.CHANGE_CURRENCY)) {
		 * strbData.append(SelectListGenerator.createXBECurrencyActiveDescListWOTag()); } else {
		 * strbData.append("arrCurrency[0] = new Array('" + baseCurrency.getKey() + "','" + baseCurrency.getValue() +
		 * "');");
		 * 
		 * if (agentCurrency != null && !baseCurrency.getKey().equals(agentCurrency.getCurrencyCode())) {
		 * strbData.append("arrCurrency[1] = new Array('" + agentCurrency.getCurrencyCode() + "','" +
		 * agentCurrency.getCurrencyDescriprion() + "');"); } } }
		 */
		if (checkAccess(request, PriviledgeConstants.CHANGE_CURRENCY)) {
			strbData.append(SelectListGenerator.createXBECurrencyActiveDescListWOTag());
		} else {
			strbData.append("arrCurrency[0] = new Array('" + baseCurrency.getKey() + "','" + baseCurrency.getValue() + "');");

			if (agentCurrency != null && !baseCurrency.getKey().equals(agentCurrency.getCurrencyCode())) {
				strbData.append("arrCurrency[1] = new Array('" + agentCurrency.getCurrencyCode() + "','"
						+ agentCurrency.getCurrencyDescriprion() + "');");
			}
		}

		return strbData.toString();
	}

	// Menaka
	public final static String createActiveSITAHtml(String airportCode, String flightNumber,String carrierCode, DCS_PAX_MSG_GROUP_TYPE msgType, String strDateOfFlight) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection sitaCollecAll = null;
		Collection sitaCollec = new ArrayList<SITAAddress>();
		String strHtml = "var arrGroup1 = new Array();";
		strHtml += "arrGroup1[0] = 'SITA Address List';";
		strHtml += "arrGroup1[1] = 'AIRPORT MAIL Address List';";

		sb.append(strHtml);
		String strSITAHtml = "";
		String strMailHtml = "";

		strSITAHtml += "var arrData = new Array();";
		strSITAHtml += "var arrData1 = new Array();";
		strSITAHtml += "var arrData2 = new Array();";
		
		Date date = convertStringToDate(strDateOfFlight);
		int flightID = ModuleServiceLocator.getFlightServiceBD().getFlightID(flightNumber, date, airportCode);
		Map<String, String> legNumberWiseOndMap = ModuleServiceLocator.getFlightServiceBD().getFlightLegs(flightID);
		String ond = getOndFromDepartureAirport(airportCode, legNumberWiseOndMap);
		
		sitaCollecAll = ModuleServiceLocator.getAirportBD().getActiveSITAAddresses(airportCode, carrierCode, msgType);
		for (Iterator iterator = sitaCollecAll.iterator(); iterator.hasNext();) {
			SITAAddress sitaAddress = (SITAAddress) iterator.next();
			if (sitaAddress.getFlightNumbers() != null && !sitaAddress.getFlightNumbers().isEmpty()
					&& !sitaAddress.getFlightNumbers().contains(flightNumber)) {
				continue;
			} else if (sitaAddress.getOndCodes() != null && !sitaAddress.getOndCodes().isEmpty()
					&& !sitaAddress.getOndCodes().contains(ond)) {
				continue;
			}
			sitaCollec.add((SITAAddress) sitaAddress);
		}
		
		int tempInc = 0;
		int tempMailInc = 0;
		boolean hasSita = false;
		boolean hasEmail = false;
		SITAAddress sitaAddress = null;
		Iterator sitaIter = sitaCollec.iterator();
		if (!sitaCollec.isEmpty()) {
			while (sitaIter.hasNext()) {
				sitaAddress = (SITAAddress) sitaIter.next();
				if (sitaAddress.getSitaAddress() != null) {

					strSITAHtml += "arrData1[" + tempInc + "] = new Array('" + sitaAddress.getSitaAddress() + "','"
							+ sitaAddress.getSitaAddress() + "'), ";
					tempInc++;
					hasSita = true;
				}
				if (sitaAddress.getEmailId() != null) {

					String[] array = sitaAddress.getEmailId().split(",");
					strMailHtml += splitEmail(array, tempMailInc);
					tempMailInc += array.length;
					hasEmail = true;
				}

			}
		}

		if (strSITAHtml.length() > 2 && hasSita) {
			strSITAHtml = strSITAHtml.substring(0, strSITAHtml.length() - 2);
			sb.append(strSITAHtml + ";");
		} else {
			sb.append(strSITAHtml);
		}
		if (strMailHtml.length() > 2 && hasEmail) {
			strMailHtml = strMailHtml.substring(0, strMailHtml.length() - 2);
			sb.append(strMailHtml + ";");
		}

		sb.append("arrData[0]= arrData1;");
		sb.append("arrData[1]= arrData2;");
		sb.append("var arrGroup2 = new Array();");
		sb.append("var arrData2 = new Array();arrData2[0] = new Array();");
		String strSITAlistHtml = "var ls = new Listbox('lstSITAAddresses', 'lstAssignedSITAAdresses', 'spn1');"
				+ "ls.group1 = arrGroup1;ls.group2=arrGroup2;ls.list1=arrData;ls.list2=arrData2;" + "ls.width = '200px';"
				+ "ls.height = '150px';" + "ls.headingLeft = '&nbsp;&nbsp;All SITA & MAIL Addresses';"
				+ "ls.headingRight = '&nbsp;&nbsp;Selected SITA Addresses';" + "ls.drawListBox();";

		sb.append(strSITAlistHtml);
		return sb.toString();
	}
	
	private static Date convertStringToDate(String strDateOfFlight){
		SimpleDateFormat dateFormat = null;
		Date dateOfFlight = null;
		
		if (!strDateOfFlight.equals("")) {
			if (strDateOfFlight.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yy");
			}
			if (strDateOfFlight.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yy");
			}
			if (strDateOfFlight.indexOf(' ') != -1) {
				strDateOfFlight = strDateOfFlight.substring(0, strDateOfFlight.indexOf(' '));
			}

			try {
				dateOfFlight = dateFormat.parse(strDateOfFlight);
			} catch (ParseException e) {
				
			}
		}
		
		return dateOfFlight;
	}
	
	private static String getOndFromDepartureAirport(String departureAirport, Map<String, String> legNumberWiseOndMap){
		String ondFromDepartureAirport = "";
		Iterator it = legNumberWiseOndMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        String ond = (String) pair.getValue();
	        if (departureAirport.equals(ond.substring(0, 3))){
	        	ondFromDepartureAirport = ond;
	        	break;
	        }
	    }
	    return ondFromDepartureAirport;
	}

	private static String splitEmail(String[] array, int tempInt) {

		String strMailHtml = "";
		for (int a = 0; a < array.length; a++) {
			strMailHtml += "arrData2[" + tempInt + "] =new Array('" + array[a] + "','" + array[a] + "'), ";
			tempInt += 1;
		}
		return strMailHtml;
	}

	public static String createDefaultValuesHtml(HttpServletRequest request, Currency agentCurrency) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		java.util.Date dtCurrent = new java.util.Date();
		SimpleDateFormat smpDtFormat = new SimpleDateFormat(DateUtil.DEFAULT_DATE_INPUT_FORMAT);

		String strAgentCurrency = null;
		boolean restrictCurr = true;

		if (agentCurrency == null) {
			strAgentCurrency = "";
		} else {
			strAgentCurrency = agentCurrency.getCurrencyCode();
			if (agentCurrency.getStatus().equals(WebConstants.RECORD_STATUS_ACTIVE) && agentCurrency.getXBEVisibility() == 1) {
				restrictCurr = false;
			}
		}

		String childBookingAllovedFlag = "N";
		if (AppSysParamsUtil.isChildOnlyBookingsAllowed()) {
			childBookingAllovedFlag = "Y";
		} else {
			if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.MAKE_RES_CHILDONLY)) {
				childBookingAllovedFlag = "Y";
			}
		}
		
		int maxAdultAllowed = WebplatformUtil.getMaxAdultCount(((UserPrincipal) request.getUserPrincipal()).getUserId(), "XBE");

		strbData.append("arrParams[0] = '" + smpDtFormat.format(dtCurrent) + "';");
		strbData.append("arrParams[1] = '" + maxAdultAllowed + "';");
		strbData.append("arrParams[2] = '" + globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_IBE) + "';");
		strbData.append("arrParams[3] = '" + globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM) + "';");
		strbData.append("arrParams[4] = '" + globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY) + "';");
		strbData.append("arrParams[5] = '" + ReservationInternalConstants.PassengerType.ADULT + "';");
		strbData.append("arrParams[6] = '" + ReservationInternalConstants.PassengerType.INFANT + "';");
		strbData.append("arrParams[7] = '" + globalConfig.getBizParam(SystemParamKeys.INSURCHG) + "';");
		strbData.append("arrParams[8] = '" + globalConfig.getBizParam(SystemParamKeys.DEPT_DATE) + "';");
		strbData.append("arrParams[9] = '" + globalConfig.getBizParam(SystemParamKeys.RETU_DATE) + "';");
		strbData.append("arrParams[10] = '" + globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getCutOffAgeInYears() + "';");
		strbData.append("arrParams[11] = '" + globalConfig.getBizParam(SystemParamKeys.DECIMALS) + "';");
		strbData.append("arrParams[12] = '" + globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_XBE) + "';");
		strbData.append("arrParams[13] = '" + globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_DEF_IBE) + "';");
		strbData.append("arrParams[14] = '" + globalConfig.getBizParam(SystemParamKeys.FLT_SEARCH_VARIANCE_DEF_XBE) + "';");
		strbData.append("arrParams[15] = '" + globalConfig.getBizParam(SystemParamKeys.CLASS_OF_SERVICE) + "';");
		strbData.append("arrParams[16] = '" + PaxTypeTO.CHILD + "';");
		strbData.append("arrParams[17] = '" + globalConfig.getBizParam(SystemParamKeys.MAX_PAX_AGENT) + "';");
		strbData.append("arrParams[18] = '" + strAgentCurrency + "';");
		// strbData.append("arrParams[19] = '" + strExRate + "';");
		strbData.append("arrParams[20] = '" + globalConfig.getBizParam(SystemParamKeys.PAGE_TIME_OUT_MILISECONDS) + "';");
		strbData.append("arrParams[21] = '" + WebConstants.HTML_STR_INFANT + "';");
		strbData.append("arrParams[22] = '" + childBookingAllovedFlag + "';");
		strbData.append("arrParams[23] = '" + AccelAeroCalculator.DEFAULT_NUMBER_OF_DECIMAL_POINTS + "';");
		strbData.append("arrParams[24] = '" + ReservationInternalConstants.PassengerType.CHILD + "';");
		strbData.append("arrParams[25] = '" + globalConfig.getBizParam(SystemParamKeys.SHOW_SEAT_MAP) + "';");
		strbData.append("arrParams[26] = '" + globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears() + "';");
		strbData.append("arrParams[27] = '" + globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears() + "';");

		strbData.append("arrParams[28] = '';");
		strbData.append("arrParams[29] = '';");
		strbData.append("arrParams[30] = '';");
		strbData.append("arrParams[31] = " + AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled() + ";");
		strbData.append("arrParams[32] = " + restrictCurr + ";");
		strbData.append("arrParams[33] = '" + AppSysParamsUtil.getCarrierAgent() + "';");
		if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_INSURANCE))
			strbData.append("arrParams[34] = '" + globalConfig.getBizParam(SystemParamKeys.SHOW_TRAVEL_INSURANCE) + "';");
		else
			strbData.append("arrParams[34] = 'N';");

		strbData.append("arrParams[35] = " + AppSysParamsUtil.isOpenEndedReturnEnabled() + ";");

		strbData.append("arrParams[36] = '" + globalConfig.getBizParam(SystemParamKeys.SHOW_MEAL) + "';");
		strbData.append("arrParams[37] = '" + globalConfig.getBizParam(SystemParamKeys.MEAL_IMG_PATH) + "';");
		/** JIRA : 2675 - Lalanthi */
		strbData.append("arrParams[38] = '" + globalConfig.getBizParam(SystemParamKeys.SHOW_AIRPORT_SERVICES) + "';");

		/** JIRA -AARESAA:2715 (Lalanthi) */
		strbData.append("arrParams[39] = '" + globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getAgeLowerBoundaryInDays()
				+ "';");
		strbData.append("arrParams[40] = '" + globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getAgeLowerBoundaryInMonths()
				+ "';");
		strbData.append("arrParams[41] = '" + globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getAgeLowerBoundaryInMonths()
				+ "';");

		/*
		 * Hard coding v1 enabling param due to re-factoring of unused v1 action classes
		 */
		// strbData.append("arrParams[42] = " + XBEModuleUtils.getConfig().isV2Enable() + ";");
		strbData.append("arrParams[42] = true;");
		strbData.append("arrParams[43] = " + AppSysParamsUtil.isEmailDomainValidationEnabled() + ";");
		strbData.append("arrParams[44] = " + AppSysParamsUtil.addMobileAreaCodePrefix() + ";");
		strbData.append("arrParams[45] = " + AppSysParamsUtil.displayFFPNoInXBEToolBar() + ";");
		if (BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_INSURANCE_OHD))
			strbData.append("insuranceOHD = true;");
		else
			strbData.append("insuranceOHD = false;");
		return strbData.toString();
	}

	public static String createTitelHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleArray());
		return strbData.toString();
	}

	public static String createTitelHtmlForAdult(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleArrayForAdult());
		return strbData.toString();
	}

	public static String createTitelHtmlWithChild(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleArrayWithChild());
		return strbData.toString();
	}

	public static String createPaxTypesHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTypesAssociativeArray());
		return strbData.toString();
	}

	public static String createTiteVisibilityHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleVisibleArray());
		return strbData.toString();
	}

	public static String createSSRHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createSSRCodeArray(AppIndicatorEnum.APP_XBE, "arrSSR"));
		return strbData.toString();
	}

	public static String createInfantTitelHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();

		strbData.append("arrInfantTitle[0] = new Array('Baby','Baby');");
		return strbData.toString();
	}

	public static String createCountryHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createCountryListWOTag());
		return strbData.toString();
	}

	public static String createNationalityHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createNationalityListWOTag());
		return strbData.toString();
	}

	public static String createCardTypeHtml(HttpServletRequest request) throws ModuleException {
		String html = SelectListGenerator.createCreditCardTypesArray();
		return html;
	}

	public static String createCosHtml(HttpServletRequest request) throws ModuleException {
		String str = SelectListGenerator.createCabinClassCodeArray();
		return str;
	}

	public static String createCosWithLogicalHtml(HttpServletRequest request) throws ModuleException {
		String str = SelectListGenerator.createCosWithLogicalCabinClassList();
		return str;
	}

	public static Map<String, Integer> createCabinClassRankingMap(HttpServletRequest request) throws ModuleException {
		Map<String, Integer> str = SelectListGenerator.createCabinClassRankingMap();
		return str;
	}

	public static String createClientErrors(Properties moduleErrs) {
		StringBuffer strbData = new StringBuffer();
		Set keySet = moduleErrs.keySet();
		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			strbData.append("arrError['" + moduleErrs.getProperty(key) + "'] = '" + XBEConfig.getClientMessage(key, null) + "';");
		}

		return strbData.toString();
	}
	
	public static String createClientErrorsTempMethod(Properties moduleErrs, String userLang) {
		StringBuffer strbData = new StringBuffer();
		Set keySet = moduleErrs.keySet();
		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			strbData.append("arrError['" + moduleErrs.getProperty(key) + "'] = '" + XBEConfig.getClientMessage(key, userLang) + "';");
		}

		return strbData.toString();
	}

	public static void setServerError(HttpServletRequest request, String strErrorMsg, String strFrame, String strReturnPage) {
		request.setAttribute(WebConstants.REQ_ERROR_REDIRECT, strReturnPage);
		request.setAttribute(WebConstants.REQ_ERROR_FRAME, strFrame);
		request.setAttribute(WebConstants.REQ_ERROR_SERVER_MESSAGES, strErrorMsg);
	}

	public static String createTAsWithCreditHtml(HttpServletRequest request) throws ModuleException {
		int columnCount = 3;
		String defaultAirlineCode = "";
		String ownAirlineCode = null;
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		Collection<String[]> colStrAgents = new ArrayList<String[]>();
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges != null) {
			defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
			ownAirlineCode = userPrincipal.getAirlineCode();

			if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_PAYMNETS_ANY) != null) {
				colStrAgents = SelectListGenerator.createAllTAsWithCredit(ownAirlineCode, columnCount);
			} else if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_PAYMNETS) != null) {
				User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
				String agentCode = user.getAgentCode();
				if (agentCode != null && mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_PAYMNETS_REPORTING) != null) {
					colStrAgents = SelectListGenerator.createReportingTAsWithCredit(ownAirlineCode, agentCode, columnCount);
				}
			}
		}

		// appending the carrier code to the agent name
		if (colStrAgents != null && !colStrAgents.isEmpty()) {
			for (String[] raw : colStrAgents) {
				if ("".equals(defaultAirlineCode)) {
					raw[0] = raw[0];
				} else {
					raw[0] = raw[0] + " - " + raw[1].substring(0, 3);
				}
			}
		}
		return SelectListGenerator.getJSArray("arrTravelAgents", colStrAgents);
	}

	/**
	 * V1 list This method is used to generate refund agent list which include remote agents as well as own agents For
	 * Normal Users 1) Allow ANY On Account Refund - all the agents in the carrier will be added to the list 2) Allow
	 * Reporting On Account Refund - all the reporting agents plus the own account will be added 3) Allow On Account
	 * Refund on operating carrier For Dry Users 1) Allow ANY On Account Refund - all the marketing carrier agents will
	 * be added to the list 2) Allow Reporting On Account Refund - all the marketing carrier reporting agents + own
	 * agent will be added to the list 3) Allow On Account Refund - marketing carrier own agent will be added to the
	 * list. 4) Allow ANY On Account Refund on Operating and marketing Carrier - will include all the agents in the
	 * operating carrier to the agent list.
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public static String createRefundTAWithCreditHtml(HttpServletRequest request) throws ModuleException {
		List<String[]> results = new ArrayList<String[]>();
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		int columnCount = 3;
		String defaultAirlineCode = "";
		String ownAirlineCode = null;
		if (mapPrivileges != null) {
			Collection<String[]> localAgentList = new ArrayList<String[]>();

			defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
			ownAirlineCode = userPrincipal.getAirlineCode();

			if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_REFUND_ANY) != null) {
				localAgentList = SelectListGenerator.createAllTAsWithCredit(ownAirlineCode, columnCount);
			} else if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_REFUND) != null) {
				User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
				String agentCode = user.getAgentCode();
				if (agentCode != null && mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_REFUND_REPORTING) != null) {
					localAgentList = SelectListGenerator.createReportingTAsWithCredit(ownAirlineCode, agentCode, columnCount);
				}
			}
			results.addAll(localAgentList);
		}

		// appending the carrier code to the agent name
		if (results != null && !results.isEmpty()) {
			for (String[] raw : results) {
				if ("".equals(defaultAirlineCode)) {
					raw[0] = raw[0];
				} else {
					raw[0] = raw[0] + " - " + raw[1].substring(0, 3);
				}
			}
		}

		return SelectListGenerator.createJSArray("arrTravelAgentsWithDry", results);
	}

	public static String createYearHtml(HttpServletRequest request) throws ModuleException {
		StringBuffer sb = new StringBuffer();

		Calendar cal = new GregorianCalendar();

		int iYear = cal.get(Calendar.YEAR);

		int arrSize = 4;
		for (int i = 0; i < arrSize; i++) {
			String sYear = "" + iYear;
			sb.append("arrYear[" + i + "]=new Array('" + sYear + "','" + sYear + "');");
			iYear++;
		}

		return sb.toString();
	}

	public static String nullConvertToString(Object obj) throws ModuleException {
		String strReturn = "";

		if (obj == null) {
			strReturn = "";
		} else {
			strReturn = obj.toString();
		}
		return strReturn;
	}

	public static boolean checkAccess(HttpServletRequest request, String strPrivilegeID) throws ModuleException {
		boolean blnReturn = false;
		HashMap hashMapPrevi = (HashMap) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (hashMapPrevi != null && hashMapPrevi.containsKey(strPrivilegeID)) {
			blnReturn = true;
		}
		return blnReturn;
	}

	public static String createAllTravelAgentsHtml(String jsArryName, HttpServletRequest request, Collection colAgents)
			throws ModuleException {
		String html = SelectListGenerator.getJSArray(jsArryName, colAgents);
		return html;
	}

	/**
	 * Method to retrieve the pax category list.
	 * 
	 * @param request
	 * @param status
	 * @return
	 * @throws ModuleException
	 */
	public static String createPaxCategory(HttpServletRequest request, String status) throws ModuleException {
		return SelectListGenerator.createPaxCategory(status);
	}

	/**
	 * Method to retrieve the pax category foid list.
	 * 
	 * @param request
	 * @param status
	 * @return
	 * @throws ModuleException
	 */
	public static String createPaxCategoryFOID(HttpServletRequest request, String status) throws ModuleException {
		return SelectListGenerator.createPaxCategoryFoid(status, AppIndicatorEnum.APP_XBE.toString());
	}

	/**
	 * Method to retrieve the active fare category list.
	 * 
	 * @param request
	 * @param status
	 * @return
	 * @throws ModuleException
	 */
	public static String createActiveFareCategories(String arrName) throws ModuleException {
		return SelectListGenerator.createActiveFareCategories(arrName);
	}

	/**
	 * Creates Roles Multi Select According to User
	 * 
	 * @param paramString
	 *            the User
	 * @return String the array of Roles Multi select Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createRoleByAgent(String paramString) throws ModuleException {

		String[] agentCode = { paramString };
		Collection<String[]> rolesCollec = null;

		Collection<String[]> roleIdCollec = null;

		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append("arrGroup1[0] = '' ;");
		jsb.append("var arrData = new Array();");
		jsb.append("var roles = new Array();");

		rolesCollec = SelectListGenerator.createRolesByAgentType(agentCode);

		int tempInt = 0;
		String[] role = null;
		String[] roleAgentType = null;

		if (rolesCollec != null) {
			Iterator<String[]> rolesIter = rolesCollec.iterator();

			while (rolesIter.hasNext()) {
				role = (String[]) rolesIter.next();
				Map roleIdMap = new HashMap();
				String agentRole = "";
				if (role != null) {
					if (!role[0].equals("0")) {
						if (!role[0].equals("-1")) {
							String[] roleId = { role[0] };
							roleIdCollec = SelectListGenerator.createAgentTypeByRole(roleId);

							if (roleIdCollec != null) {
								Iterator itr = roleIdCollec.iterator();
								while (itr.hasNext()) {
									roleAgentType = (String[]) itr.next();
									if (roleAgentType != null) {
										agentRole = agentRole + roleAgentType[1];
										if (itr.hasNext()) {
											agentRole += ",";
										}
										roleIdMap.put(roleAgentType[0], agentRole);

									}
								}
							}

							if (Role.EXCLUDE_RLOE.equals(role[4])) {
								role[3] = Role.STATUS_INACTIVE;
							}

							jsb.append("roles [" + tempInt + "] = new Array('" + role[1] + "(" + roleIdMap.get(role[0]) + ")"
									+ "','" + role[0] + "','" + role[2] + "','" + role[3] + "','" + role[4] + "'); ");
							tempInt++;
						}
					}
				}

			}
		}

		jsb.append("arrData[0] = roles; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var ls = new Listbox2('lstRoles', 'lstAssignedRoles', 'spn1');");
		jsb.append("checkMoveValidity= true; ");
		jsb.append("blnRoleValidate= true; ");
		jsb.append("ls.group1 = arrData[0];" + "ls.group2 = arrGroup2;");
		jsb.append("ls.height = '165px'; ls.width = '170px';");
		jsb.append("ls.headingLeft = '&nbsp;&nbsp;Roles';");
		jsb.append("ls.headingRight = '&nbsp;&nbsp;Assigned Roles';");
		jsb.append("if(document.getElementById('spn1'))");
		jsb.append("ls.drawListBox();");

		return jsb.toString();
	}

	public final static String createTAsOfGSAMultiSelect(String gsaCode) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		List gsas = null;
		String strRoleslistHtml = "";
		String strAssignGSAHtml = "";
		String strAgenciesHtml = "";

		strAgenciesHtml = "var arrData = new Array();arrData[0] = new Array(";
		strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.height = '200px';"
				+ "ls.width  = '350px';" + "ls.headingLeft = '&nbsp;&nbsp;All Agents';"
				+ "ls.headingRight = '&nbsp;&nbsp;Selected Agents';" + "ls.drawListBox();";
		if (gsaCode != null && !gsaCode.equals("") && !gsaCode.equals("All")) {

			String[] agentType = new String[1];
			agentType[0] = gsaCode;
			String[] gsaAraay = null;

			gsas = (List) SelectListGenerator.createTasFromGSA(agentType);
			if (!gsas.isEmpty()) {
				strAgenciesHtml = "var arrGroup1 = new Array();";
				strRoleslistHtml += "var privArray = new Array();";

				Iterator gsaIter = gsas.iterator();
				int i = 0;
				while (gsaIter.hasNext()) {
					gsaAraay = (String[]) gsaIter.next();
					strAgenciesHtml += " arrGroup1[" + i + "] = new Array('" + gsaAraay[3] + "','" + gsaAraay[2] + "'); ";
					i++;
				}
				sb.append(strAgenciesHtml);
				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"
						+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = arrGroup1;"
						+ "ls.group2 = arrGroup2;" + "ls.height = '200px';" + "ls.width = '350px';"
						+ "ls.headingLeft = '&nbsp;&nbsp;All Agents';" + "ls.headingRight = '&nbsp;&nbsp;Selected Agents';"
						+ "ls.drawListBox();";
			}
		}
		sb.append(strAssignGSAHtml);
		return sb.toString();
	}

	public static String setAgentCurrencyDetails(Currency agentCurrency) throws ModuleException {
		StringBuilder sb = new StringBuilder();

		if (agentCurrency != null && !agentCurrency.getStatus().equals(WebConstants.RECORD_STATUS_INACTIVE)
				&& agentCurrency.getXBEVisibility() != 0) {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate exchRate = exchangeRateProxy.getCurrencyExchangeRate(agentCurrency.getCurrencyCode(),
					ApplicationEngine.XBE);
			sb.append("agentExchgRate = '" + exchRate.getExrateBaseToCurNumber() + "';");// JIRA : AARESAA-3087
			sb.append("agentCurrBound = '" + agentCurrency.getBoundryValue() + "';");
			sb.append("agentCurrBreakPoint = '" + agentCurrency.getBreakPoint() + "';");
		}
		return sb.toString();
	}

	public static String createReturnValidities(String arrName) throws ModuleException {
		return SelectListGenerator.createReturnValidityPeriods(arrName, ReservationInternalConstants.SalesChannel.TRAVEL_AGENT);
	}

	public static String createBCTypes(String arrName) throws ModuleException {
		return BookingClassUtil.createBCTypes(arrName);
	}

	public static String createBookingClassCodesList(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createBookingClassCodesArray());
		return strbData.toString();
	}

	/**
	 * Creates a multi Select Box for all countries
	 * 
	 * @return String the Station multy select box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCountryHtml() throws ModuleException {

		List<Map<String, String>> countries = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var countries = new Array();");

		countries = SelectListGenerator.createCountryListWithCode();
		int tempInt = 0;
		String countryName = null;
		String countryCode = null;

		Iterator<Map<String, String>> countryItr = countries.iterator();
		while (countryItr.hasNext()) {

			Map<String, String> keyValues = countryItr.next();
			Set<String> keySet = keyValues.keySet();
			Iterator<String> keySetItr = keySet.iterator();

			if (keySetItr.hasNext()) {
				countryCode = keyValues.get(keySetItr.next());
				countryName = keyValues.get(keySetItr.next());
				jsb.append("countries[" + tempInt + "] = new Array('" + countryName + "','" + countryName + "'); ");
				tempInt++;
			}

		}
		jsb.append("arrData[0] = countries;");
		jsb.append(" var countryListBox = new Listbox('countries', 'selectedCountries', 'spnCountrList', 'countryListBox');");
		jsb.append("countryListBox.group1 = arrData[0];");
		jsb.append("countryListBox.height = '100px'; countryListBox.width = '170px';");
		jsb.append("countryListBox.headingLeft = '&nbsp;&nbsp;Countries';");
		jsb.append("countryListBox.headingRight = '&nbsp;&nbsp;Selected Countries';");
		return jsb.toString();
	}

	/**
	 * Creates a multi Select Box for Active Stations
	 * 
	 * @return String the Station multy select box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createStationHtml() throws ModuleException {

		List<Map<String, String>> Stations = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var methods = new Array();");

		Stations = SelectListGenerator.createStationsListWithStatus();
		int tempInt = 0;
		String stationName = null;
		String stationCode = null;
		// String stations[] = new String[2];

		Iterator<Map<String, String>> stationsItr = Stations.iterator();
		while (stationsItr.hasNext()) {

			Map<String, String> keyValues = stationsItr.next();
			Set<String> keySet = keyValues.keySet();
			Iterator<String> keySetItr = keySet.iterator();

			if (keySetItr.hasNext()) {
				stationCode = keyValues.get(keySetItr.next());
				stationName = keyValues.get(keySetItr.next());
				// if(status != null && status.equals("ACT")) {
				jsb.append("methods[" + tempInt + "] = new Array('" + stationCode + "  -  " + stationName + "','" + stationCode
						+ "'); ");
				tempInt++;
			}

		}

		jsb.append("arrData[0] = methods;");
		jsb.append(" var lssta = new Listbox('lstStations', 'lstAssignedStations', 'spnSta', 'lssta');");
		jsb.append("lssta.group1 = arrData[0];");
		jsb.append("lssta.height = '100px'; lssta.width = '170px';");
		jsb.append("lssta.headingLeft = '&nbsp;&nbsp;Stations';");
		jsb.append("lssta.headingRight = '&nbsp;&nbsp;Selected Stations';");
		// jsb.append("lssta.drawListBox();");
		return jsb.toString();
	}

	public static String createAutoSeatAssignmentValue() {
		StringBuffer sb = new StringBuffer();
		sb.append(" var isAutoSeatAssignmentEnabled = ");

		if (AppSysParamsUtil.isAutomaticSeatAssignmentEnabled()) {
			sb.append("true; ");
		} else {
			sb.append("false; ");
		}

		return sb.toString();
	}

	public static String createAutoSeatAsssignemntStartingRowNo() {
		StringBuffer sb = new StringBuffer();
		sb.append(" var autoSeatAssignmentStartingRowNumber = '");
		sb.append(AppSysParamsUtil.getAutoSeatAssignmentStartingRowNumberForXBE());
		sb.append("'; ");
		return sb.toString();
	}

	public static String getAgentFareMaskStatus(String agentCode) {
		String param = "var agentFareMaskStatus = '" + SelectListGenerator.getAgentFareMaskStatus(agentCode) + "'; ";
		return param;

	}
	
	public final static String createAgentsForMultiSelect(String reportingAgentCode, String searchAgentType,
			boolean displayAllAgents, boolean blnWithTAs, boolean blnWithCOs, String agentType)
			throws ModuleException {
		StringBuffer sb = new StringBuffer();
		List gsas = null;
		String strRoleslistHtml = "";
		String strAssignGSAHtml = "";
		String strAgenciesHtml = "";

		strAgenciesHtml = "var arrData = new Array();arrData[0] = new Array(";
		strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.height = '200px';"
				+ "ls.width  = '350px';" + "ls.headingLeft = '&nbsp;&nbsp;All Agents';"
				+ "ls.headingRight = '&nbsp;&nbsp;Selected Agents';" + "ls.drawListBox();";
		
		if (((!StringUtil.isNullOrEmpty(reportingAgentCode) && !displayAllAgents) || displayAllAgents)) {
			String[] gsaAraay = null;

			gsas = (List) SelectListGenerator.createAgentsOfReporting(reportingAgentCode, displayAllAgents, searchAgentType,
					blnWithTAs, blnWithCOs, agentType);
			if (!gsas.isEmpty()) {
				strAgenciesHtml = "var arrGroup1 = new Array();";
				strRoleslistHtml += "var privArray = new Array();";

				Iterator gsaIter = gsas.iterator();
				int i = 0;
				while (gsaIter.hasNext()) {
					gsaAraay = (String[]) gsaIter.next();
					strAgenciesHtml += " arrGroup1[" + i + "] = new Array('" + gsaAraay[3] + "','" + gsaAraay[2] + "'); ";
					i++;
				}
				sb.append(strAgenciesHtml);
				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"
						+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = arrGroup1;"
						+ "ls.group2 = arrGroup2;" + "ls.height = '200px';" + "ls.width = '350px';"
						+ "ls.headingLeft = '&nbsp;&nbsp;All Agents';" + "ls.headingRight = '&nbsp;&nbsp;Selected Agents';"
						+ "ls.drawListBox();";
			}
		}
		sb.append(strAssignGSAHtml);
		return sb.toString();
	}
	
	public static boolean checkWebOrMobileAccess(HttpServletRequest request) throws ModuleException {
		boolean blnReturn = false;
		if (checkAccess(request, PriviledgeConstants.PRIVI_ACC_IBE_RES)
				|| checkAccess(request, PriviledgeConstants.PRIVI_ACC_IOS_RES)
				|| checkAccess(request, PriviledgeConstants.PRIVI_ACC_ANDROID_RES)) {
			blnReturn = true;
		}
		return blnReturn;
	}

}
