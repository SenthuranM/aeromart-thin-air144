package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.RecallTicketCouponControlRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.RecallTicketCouponControlRs;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * 
 * @author M.Rikaz
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class CouponControlRequestAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(CouponControlRequestAction.class);

	private boolean success = true;

	private String messageTxt;

	private String resSegRPHList;

	public String execute() {
		RecallTicketCouponControlRs couponControlRs = null;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			RecallTicketCouponControlRq couponControlReq = new RecallTicketCouponControlRq();
			Collection<Integer> paxSegETktIds = null;
			boolean validRequest = false;
			if (!StringUtil.isNullOrEmpty(resSegRPHList)) {
				Collection<Integer> pnrSegIds = new ArrayList<Integer>();
				String tsr[] = resSegRPHList.split(",");
				if (tsr != null && tsr.length > 0) {
					for (String pnrSegRPHStr : tsr) {

						pnrSegIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(pnrSegRPHStr));
					}

					if (!pnrSegIds.isEmpty()) {
						paxSegETktIds = ModuleServiceLocator.getReservationBD().getPaxFareSegmentETicketIDs(pnrSegIds);
					}
				}

				if (paxSegETktIds != null && !paxSegETktIds.isEmpty()) {
					validRequest = true;
					couponControlReq.setPpfsETicketIds(new ArrayList<Integer>(paxSegETktIds));
					couponControlRs = ModuleServiceLocator.getGdsRequiredBD().recallTicketCouponControl(couponControlReq);

					if (!couponControlRs.isSuccess()
							|| (couponControlRs.getRejectedPpfsEticketIds() != null && !couponControlRs
									.getRejectedPpfsEticketIds().isEmpty())) {
						success = false;
						messageTxt = XBEConfig.getClientMessage("um.gds.coupon.ctrl.request.failed", userLanguage);

					}
				}

			}
			
			
			if(!validRequest){
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.gds.coupon.ctrl.request.invalid", userLanguage);
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);

			log.error("Exception @ CouponControlRequest : ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * getters and setters
	 */
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getResSegRPHList() {
		return resSegRPHList;
	}

	public void setResSegRPHList(String resSegRPHList) {
		this.resSegRPHList = resSegRPHList;
	}

}
