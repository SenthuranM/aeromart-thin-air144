package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.assembler.OndConvertAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.TravelAgentUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * 
 * @author M.Rikaz
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class MultiCitySearchAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(MultiCitySearchAction.class);

	private FlightSearchDTO searchParams;

	private boolean success = true;

	private List<Collection<AvailableFlightInfo>> ondSeqFlightsList;

	private String messageTxt;

	private boolean currencyRound = false;

	private boolean addSegment = false;

	private String airportMessage;

	private String groupPNR;

	private String selectedFltSeg;

	private ReservationProcessParams rpParams;

	private String classOfServiceDesc;

	private List<String> actLogCabClassList;

	private Set<String> busCarrierCodes;

	private List<String> ondWiseAvailLogCOSInfoList;
	
	private boolean serviceTaxApplicable = false;
	
	private boolean displayBasedOnTemplates = false;
	
	private String fromAirports = "";

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			fromAirports = searchParams.getFromAirport();
			boolean hasPrivViewAvailability = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			// TODO charith : should be able to remove
			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
			boolean hasPrivInterlineSearch = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)
					&& AppSysParamsUtil.isLCCConnectivityEnabled();
			boolean isAllowFlightSearchAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			ReservationProcessParams resParm = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);

			FlightAvailRS flightAvailRS = new FlightAvailRS();

			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createMultiCityAvailSearchRQ(searchParams, AppIndicatorEnum.APP_XBE);
			
			int maxAdultAllowed = WebplatformUtil.getMaxAdultCount(((UserPrincipal) request.getUserPrincipal()).getUserId(), "XBE");
			
			if (maxAdultAllowed < (searchParams.getAdultCount())) {
				throw new ModuleException("error.invalid.pax.count");
			}

			flightAvailRQ.getAvailPreferences().setRestrictionLevel(
					AvailabilityConvertUtil.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats,
							searchParams.getBookingType()));

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (isGroupPNR) {
				flightAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
			}

			/*
			 * Check the privileges to search preferred system. In case of not enough privilege gracefully cut down to
			 * match the available privileges
			 */
			SYSTEM searchSystem = flightAvailRQ.getAvailPreferences().getSearchSystem();

			if (searchSystem == SYSTEM.ALL || searchSystem == SYSTEM.COND) {
				if (!hasPrivInterlineSearch) {
					searchSystem = SYSTEM.AA;
				}
			} else if (searchSystem == SYSTEM.INT && !hasPrivInterlineSearch) {
				searchSystem = SYSTEM.NONE;
			}
			flightAvailRQ.getAvailPreferences().setSearchSystem(searchSystem);

			// Set default stay over for half return variance search
			setDefaultStayOverDaysForReturnFareSearch(searchParams, flightAvailRQ);

			// cannot search bus segment alone,
			if (resParm.isGroundServiceEnabled() && AddModifySurfaceSegmentValidations.isGroundSegmentOnlyExist(searchParams)) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("um.surfacesegment.add.searchonly.invalid", userLanguage);
				return S2Constants.Result.SUCCESS;
			}

			flightAvailRQ.getAvailPreferences().setQuoteFares(false);
			flightAvailRQ.getAvailPreferences().setMultiCitySearch(true);
			flightAvailRQ.getAvailPreferences().setAllowFlightSearchAfterCutOffTime(isAllowFlightSearchAfterCutOffTime);

			AnalyticsLogger.logAvlSearch(AnalyticSource.XBE_MCS_AVL, flightAvailRQ, request, getTrackInfo());

			flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
					getTrackInfo(), false);

			/* Convert response to UI friendly DTO's */
			if (searchSystem == SYSTEM.INT) {
				if (flightAvailRS != null && flightAvailRS.getReservationParms() != null) {
					resParm.enableInterlineModificationParams(flightAvailRS.getReservationParms(),
							resInfo != null ? resInfo.getReservationStatus() : null, null, true, null);
				}
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_ACCESSED, Boolean.TRUE);
			}
			
			displayBasedOnTemplates= AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed();

			actLogCabClassList = SelectListGenerator.getActiveLogicalCabinClassList();
			busCarrierCodes = new HashSet<String>();

			List<List<String>> ondWiseAvailableCOSList = new ArrayList<List<String>>();
			ondSeqFlightsList = ReservationBeanUtil.createAvailFlightInfoListMultiCity(flightAvailRS, searchParams,
					hasPrivViewAvailability, viewFlightsWithLesserSeats, false, busCarrierCodes, actLogCabClassList,
					ondWiseAvailableCOSList);

			ondWiseAvailLogCOSInfoList = ReservationBeanUtil.generateCOSAvailabilityStringList(ondWiseAvailableCOSList);

			PriceInfoTO priceInfoTO = flightAvailRS.getSelectedPriceFlightInfo();

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			if (bookingShoppingCart == null) {
				bookingShoppingCart = new BookingShoppingCart(ChargeRateOperationType.MAKE_ONLY);
			}

			bookingShoppingCart.setPriceInfoTO(flightAvailRS.getSelectedPriceFlightInfo());
			bookingShoppingCart.setPayablePaxCount(searchParams.getAdultCount() + searchParams.getChildCount());

			resInfo.setTransactionId(flightAvailRS.getTransactionIdentifier());
			resInfo.setBookingType(flightAvailRQ.getTravelPreferences().getBookingType());
			bookingShoppingCart.setPriceInfoTO(priceInfoTO);
			currencyRound = CommonUtil.enableCurrencyRounding(searchParams.getSelectedCurrency());

			// LOAD Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightAvailRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, null);

			// ReservationProcessParams is initialized in many places, by adding
			// this to session can eliminate the
			// Unnecessary call.
			// TODO ReservationProcessParams usages must be Re-factored to load
			// from the session.
			request.getSession().setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, resParm);

			rpParams = resParm;
		} catch (Exception e) {
			clearValues();
			success = false;
			if (e instanceof ModuleException) {
				if ("error.openrt.confirm.before.outbound.notallowed".equals(((ModuleException) e).getExceptionCode())) {
					messageTxt = XBEConfig.getClientMessage("error.openrt.confirm.before.outbound.notallowed", userLanguage);
				} else {
					messageTxt = BasicRH.getErrorMessage(e,userLanguage);
				}
				
				if ("error.invalid.pax.count".equals(((ModuleException) e).getExceptionCode())) {
					messageTxt = XBEConfig.getClientMessage("error.invalid.pax.count", userLanguage);
				} else {
					messageTxt = BasicRH.getErrorMessage(e, userLanguage);
				}
				
			} else {
				messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			}
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private void setDefaultStayOverDaysForReturnFareSearch(FlightSearchDTO searchParams, FlightAvailRQ flightAvailRQ) {
		OndConvertAssembler ondAssm = new OndConvertAssembler(flightAvailRQ.getOriginDestinationInformationList());
		if (ondAssm.isReturnFlight() && !flightAvailRQ.getTravelPreferences().isOpenReturn()) {
			long stayOverTimeBetweenRequestedDates = ondAssm.getSelectedInboundDateTimeEnd().getTime()
					- ondAssm.getSelectedOutboundDateTimeStart().getTime();
			flightAvailRQ.getAvailPreferences().setStayOverTimeInMillis(stayOverTimeBetweenRequestedDates);
			searchParams.setStayOverTimeInMillis(stayOverTimeBetweenRequestedDates);
		}
	}

	public boolean isServiceTaxApplicable(){

		if (searchParams != null && !fromAirports.isEmpty()) {
			
			String originCountry = "";
			String originCode = ((fromAirports.split(",")[0]).split("_")[0]).trim();
			String[] countryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			
			if (SYSTEM.getEnum(searchParams.getSearchSystem()).equals(SYSTEM.INT)){
				
				originCountry = ReservationUtil.getCountryCodeFromCachedAirportMap(originCode);
				
			}else{
					Object[] airportInfo = ModuleServiceLocator.getGlobalConfig().retrieveAirportInfo(originCode);
					if (airportInfo != null && airportInfo.length > 2) {
						originCountry = (String) airportInfo[3];
					}
			}
			
			for (String countryCode : countryCodes) {
				if (countryCode.equalsIgnoreCase(originCountry)) {
					return true;
				}
			}
			
		}
		return false;
		
	}
	
	private void clearValues() {
		airportMessage = null;
		ondSeqFlightsList = null;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	// setters
	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public static Log getLog() {
		return log;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public ReservationProcessParams getRpParams() {
		return rpParams;
	}

	public void setRpParams(ReservationProcessParams rpParams) {
		this.rpParams = rpParams;
	}

	public void setSelectedFltSeg(String selectedFltSeg) {
		this.selectedFltSeg = selectedFltSeg;
	}

	public String getClassOfServiceDesc() {
		return classOfServiceDesc;
	}

	public void setClassOfServiceDesc(String classOfServiceDesc) {
		this.classOfServiceDesc = classOfServiceDesc;
	}

	public List<Collection<AvailableFlightInfo>> getOndSeqFlightsList() {
		return ondSeqFlightsList;
	}

	public List<String> getActLogCabClassList() {
		return actLogCabClassList;
	}

	public Set<String> getBusCarrierCodes() {
		return busCarrierCodes;
	}

	public List<String> getOndWiseAvailLogCOSInfoList() {
		return ondWiseAvailLogCOSInfoList;
	}

	public boolean isDisplayBasedOnTemplates() {
		return displayBasedOnTemplates;
	}
}
