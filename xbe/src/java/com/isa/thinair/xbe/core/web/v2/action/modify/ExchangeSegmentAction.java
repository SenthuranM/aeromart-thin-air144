package com.isa.thinair.xbe.core.web.v2.action.modify;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ExchangeSegmentAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ExchangeSegmentAction.class);

	private boolean success = true;
	private String messageTxt;
	private String groupPNR;
	private String pnr;

	public String execute() {
		log.info("Inside ExchangeSegmentAction");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (!StringUtil.isNullOrEmpty(pnr)) {
				boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);

				TrackInfoDTO trackInfoDTO = this.getTrackInfo();
				ModuleServiceLocator.getAirproxySegmentBD().exchangeSegment(pnr, isGroupPNR, trackInfoDTO);
			}

		} catch (Exception e) {

			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}
		log.info("Exit ExchangeSegmentAction");
		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
