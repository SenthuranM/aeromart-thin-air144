package com.isa.thinair.xbe.core.service;

import java.util.Date;

import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class OnlineCheckInReminderNotification {

	public void sendReminderNotification() throws ModuleException {
		// ModuleServiceLocator.getReservationBD().sendAncillaryReminderNotification(date);

		OnlineCheckInReminderDTO onlineCheckInReminderDTO = new OnlineCheckInReminderDTO();
		onlineCheckInReminderDTO.setFlightNo("G90121");
		onlineCheckInReminderDTO.setFltSegId(514124);
		onlineCheckInReminderDTO.setFirstName("Test");
		onlineCheckInReminderDTO.setLastName("Last name");
		onlineCheckInReminderDTO.setEmail("email@isa.ae");
		onlineCheckInReminderDTO.setOrigin("SHJ");
		onlineCheckInReminderDTO.setPreferedLanguage("en");
		onlineCheckInReminderDTO.setPnr("1111111");
		onlineCheckInReminderDTO.setDepartureDate(new Date());

		ModuleServiceLocator.getReservationBD().sendOnlineCheckInReminder(onlineCheckInReminderDTO);

	}
}
