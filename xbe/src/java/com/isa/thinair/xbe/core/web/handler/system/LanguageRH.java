package com.isa.thinair.xbe.core.web.handler.system;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
/**
 * 
 * @author rajitha
 *
 */
public class LanguageRH extends BasicRH {
	
	private static Log log = LogFactory.getLog(LanguageRH.class);
	
	public static String execute(HttpServletRequest request) {
		String userLanguage = "en"; //(String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		String forward = S2Constants.Result.SUCCESS;

		try {

			setData(request);

		} catch (RuntimeException re) {

			forward = S2Constants.Result.ERROR;
			String msg = BasicRH.getErrorMessage(re,userLanguage);
			log.error(msg, re);
			JavaScriptGenerator.setServerError(request, msg, "top", "");
		}

		return forward;
	}
	
	private static void setData(HttpServletRequest request) {
		 
		if(request.getParameter(WebConstants.REQ_LANGUAGE) != null){
			String lang = request.getParameter(WebConstants.REQ_LANGUAGE);
			request.getSession().setAttribute(WebConstants.REQ_LANGUAGE, lang);
		}else{
			request.getSession().setAttribute(WebConstants.REQ_LANGUAGE, WebConstants.DEFAULT_LANGUAGE);
		}
		
	}

}
