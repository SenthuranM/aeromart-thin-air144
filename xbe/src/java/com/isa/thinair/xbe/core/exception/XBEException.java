/**
 * 
 */
package com.isa.thinair.xbe.core.exception;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Sudheera
 * 
 */
public class XBEException extends ModuleException {

	/**
     * 
     */
	private static final long serialVersionUID = -698497571153487198L;

	/**
	 * @param exceptionCode
	 */
	public XBEException(String exceptionCode) {
		super(exceptionCode);
		// TODO Auto-generated constructor stub
	}

}
