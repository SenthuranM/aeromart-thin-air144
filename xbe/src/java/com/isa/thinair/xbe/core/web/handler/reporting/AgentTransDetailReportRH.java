package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class AgentTransDetailReportRH extends BasicRH {
	private static Log log = LogFactory.getLog(AgentTransDetailReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("AgentGSADetailReportRH success");
		} catch (Exception e) {
			forward = "error";
			log.error("AgentGSADetailReportRH execute()" + e.getMessage(), e);
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				if (log.isDebugEnabled()) {
					log.debug("AgentGSADetailReportRH setReportView Success");
				}
				return null;
			} else {
				log.error("AgentGSADetailReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AgentGSADetailReportRH setReportView Failed " + e.getMessageString(), e);
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setDisplayAgencyMode(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setClientErrors(request);
		setSearchFullPrivilege(request);
		setGSAVersion(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}
		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs); // true for
																											// cos
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.ta.atx.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.ta.atx.rpt") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.ta.atx") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
		// request.setAttribute(WebConstants.REQ_HTML_DETAILS, "displayAgencyMode = " + getAttribInRequest(request,
		// "displayAgencyMode") + ";");
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;

		String reportType = request.getParameter("hdnRptType");
		String agents = request.getParameter("hdnAgents");
		String agentCode = request.getParameter("hdnAgentCode");
		String value = request.getParameter("radReportOption");
		String agentName = request.getParameter("hdnAgentName");
		String strTrnsTypes = request.getParameter("hdnTransTypes");
		String strDetFromDate = request.getParameter("hdnFromDate");
		String strDetToDate = request.getParameter("hdnToDate");

		String strTransDateFrom = request.getParameter("txtTransFrom");
		String strtransDateTo = request.getParameter("txtTransTo");
		String chkPayTypeR = request.getParameter("chkTypeR");
		String chkPayTypeC = request.getParameter("chkTypeC");
		String chkPayTypeD = request.getParameter("chkTypeD");
		String chkPayTypeRR = request.getParameter("chkTypeRR");
		String chkPayTypeA = request.getParameter("chkTypeA");
		String chkPayTypeQ = request.getParameter("chkTypeQ");
		String chkPayTypeRV = request.getParameter("chkTypeRV");
		String chkTypeCG = request.getParameter("chkTypeCG");
		String chkTypeCR = request.getParameter("chkTypeCR");
		String chkTypeAO = request.getParameter("chkTypeAO");

		// String userId = request.getUserPrincipal().getName();

		if (strTrnsTypes == null) {
			strTrnsTypes = "";
			if (getNotNull(chkPayTypeR))
				strTrnsTypes += AgentTransactionNominalCode.ACC_SALE.toString() + ","; // strTrnsTypes += "1,";
			if (getNotNull(chkPayTypeC))
				strTrnsTypes += AgentTransactionNominalCode.ACC_CREDIT.toString() + ","; // strTrnsTypes += "8,";
			if (getNotNull(chkPayTypeD))
				strTrnsTypes += AgentTransactionNominalCode.ACC_DEBIT.toString() + ","; // strTrnsTypes += "6,";
			if (getNotNull(chkPayTypeRR))
				strTrnsTypes += AgentTransactionNominalCode.ACC_REFUND.toString() + ","; // strTrnsTypes += "7,";
			if (getNotNull(chkPayTypeA))
				strTrnsTypes += AgentTransactionNominalCode.PAY_INV_CASH.toString() + ","; // strTrnsTypes += "2,";
			if (getNotNull(chkPayTypeQ))
				strTrnsTypes += AgentTransactionNominalCode.PAY_INV_CHQ.toString() + ","; // strTrnsTypes += "3,";
			if (getNotNull(chkPayTypeRV))
				strTrnsTypes += AgentTransactionNominalCode.ACC_PAY_REVERSE.toString() + ","; // strTrnsTypes += "9,";
			if (getNotNull(chkTypeCG))
				strTrnsTypes += AgentTransactionNominalCode.ACC_COMMISSION_GRANT.toString() + ",";
			if (getNotNull(chkTypeCR))
				strTrnsTypes += AgentTransactionNominalCode.ACC_COMMISSION_REMOVE.toString() + ",";
			if (getNotNull(chkTypeAO))
				strTrnsTypes += AgentTransactionNominalCode.SUB_AGENT_OPERATIONS.toString() + ","; // strTrnsTypes +=
																									// "23,";
		}

		String id = "UC_REPM_025";
		String reportTemplateSummary = "AgentTransactionSummaryReport.jasper";
		String reportTemplateDetails = "AgentTransactionDetailsReport.jasper";
		String reportTemplateAgentDetails = "AgentTransactionAgentDetailsReport.jasper";

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (reportType.equals("Summary")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));
				search.setReportType(ReportsSearchCriteria.AGENT_TRANS_SUMMARY);
				ArrayList<String> agentCol = new ArrayList<String>();
				String agentArr[] = agents.split(",");
				for (int r = 0; r < agentArr.length; r++) {
					agentCol.add(agentArr[r].trim());
				}

				search.setAgents(agentCol);

				if (strTransDateFrom != null && !strTransDateFrom.trim().equals("")) {
					strTransDateFrom = ReportsHTMLGenerator.convertDate(strTransDateFrom);
				}

				if (strtransDateTo != null && !strtransDateTo.trim().equals("")) {
					strtransDateTo = ReportsHTMLGenerator.convertDate(strtransDateTo);
				}

			} else if (reportType.equals("AgentDetail")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateAgentDetails));
				search.setReportType(ReportsSearchCriteria.AGENT_TRANS_AGENT_DETAIL);

				ArrayList<String> agentCol = new ArrayList<String>();
				String agentArr[] = agents.split(",");
				for (int r = 0; r < agentArr.length; r++) {
					agentCol.add(agentArr[r]);
				}
				search.setAgents(agentCol);

				if (strTransDateFrom != null && !strTransDateFrom.trim().equals("")) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(strTransDateFrom));
				}
				if (strtransDateTo != null && !strtransDateTo.trim().equals("")) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(strtransDateTo));
				}
				ArrayList<String> paymentCol = new ArrayList<String>();

				if (strTrnsTypes != null && !strTrnsTypes.trim().equals("")) {
					String payArr[] = strTrnsTypes.split(",");
					for (int r = 0; r < payArr.length; r++) {
						paymentCol.add(payArr[r]);
					}
				}
				search.setPaymentTypes(paymentCol);

			} else {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));
				search.setReportType(ReportsSearchCriteria.AGENT_TRANS_DETAILS);
				search.setAgentCode(agentCode);
				if (strDetFromDate != null)
					search.setDateRangeFrom(strDetFromDate);
				if (strDetToDate != null)
					search.setDateRangeTo(strDetToDate);
				ArrayList<String> paymentCol = new ArrayList<String>();

				if (strTrnsTypes != null && !strTrnsTypes.trim().equals("")) {
					String payArr[] = strTrnsTypes.split(",");
					for (int r = 0; r < payArr.length; r++) {
						paymentCol.add(payArr[r]);
					}
				}
				search.setPaymentTypes(paymentCol);

			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getAgentTransactionData(search);
			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true); 
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("ID", id);
			parameters.put("OPTION", value);
			parameters.put("AGENT_NAME", agentName);
			parameters.put("PAYMENT_TYPE", strTrnsTypes);
			parameters.put("FROM_DATE", strTransDateFrom);
			parameters.put("TO_DATE", strtransDateTo);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AgentTransactionsReport.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AgentTransactionsReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AgentTransactionsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error("Error occured", e);
		}
	}

	private static boolean getNotNull(String str) {
		return ((str != null) && !(str.trim().equals(""))) ? true : false;
	}

	private static void setSearchFullPrivilege(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_AGENT_TRANSACTION_SEARCH_FULL,
				hasPrivilege(request, "rpt.agenttransactionrpt.search.full"));
	}

	private static void setGSAVersion(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_GSA_V2, AppSysParamsUtil.isGSAStructureVersion2Enabled());
	}
}
