/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Chamindap
 * 
 */
public class SectorContributionByAgentRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(SectorContributionByAgentRequestHandler.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public SectorContributionByAgentRequestHandler() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String value = request.getParameter("radSegmentFlight");
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("SectorContributionByAgentRequestHandler success");
		} catch (Exception e) {
			forward = S2Constants.Result.ERROR;
			log.error("SectorContributionByAgentRequestHandler execute()" + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {

				if (value.trim().equals("POS")) {
					setReportViewPOS(request, response);
				} else if (value.trim().equals("Country")) {
					setReportViewCountry(request, response);
				}

				log.error("SectorContributionByAgentRH setReportView Success");
				return null;
			} else {
				log.error("SectorContributionByAgentRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("SectorContributionByAgentRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
		}
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCountryList(request);
		setPOSList(request);
		setAirportList(request);
		setOperationTypeHtml(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationType();
		request.setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);
	}

	/**
	 * Sets the Airport With Status to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportsWithStatusList();
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, strHtml);
	}

	/**
	 * Sets the Country list
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void setCountryList(HttpServletRequest request) throws ModuleException {
		String strCountryHtml = JavaScriptGenerator.createCountryHtml();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strCountryHtml);
	}

	/**
	 * Sets POS List for reports
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setPOSList(HttpServletRequest request) throws ModuleException {
		String strPOSHtml = JavaScriptGenerator.createStationHtml();
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_STATION_SELECT_LIST, strPOSHtml);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportViewPOS(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String flightFromDate = request.getParameter("txtFlightFromDate");
		String flightToDate = request.getParameter("txtFlightToDate");
		String value = request.getParameter("radReportOption");
		String flightNo = request.getParameter("txtFlightNo");
		String strPOSList = request.getParameter("hdnPOSs");
		String strOperationType = request.getParameter("selOperationType");
		List<String> posList = new ArrayList<String>();
		if (strPOSList != null && strPOSList != "") {
			posList = Arrays.asList(strPOSList.split(","));
		}
		String strSegmentList = request.getParameter("hdnSegments");
		List<String> segmentList = new ArrayList<String>();
		if (strSegmentList != null && strSegmentList != "") {
			segmentList = Arrays.asList(strSegmentList.split(","));
		}
		String id = "UC_REPM_014";
		String reportTemplate = "SectorContributionByAgents.jasper";

		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (flightFromDate != null && !flightFromDate.equals("")) {
				search.setFlightDateRangeFrom(ReportsHTMLGenerator.convertDate(flightFromDate));
			}
			if (flightToDate != null && !flightToDate.equals("")) {
				search.setFlightDateRangeTo(ReportsHTMLGenerator.convertDate(flightToDate));
			}
			if (posList != null && posList.size() > 0) {
				search.setSelectedPOSs(posList);
			}
			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo.trim());
			}

			if (strOperationType != null) {
				search.setOperationType(strOperationType);
			}
			if (segmentList != null && segmentList.size() > 0) {
				search.setSelectedSegments(segmentList);
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getSegmentContributionByAgentsByPOSData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("FLI_FROM_DATE", flightFromDate);
			parameters.put("FLI_TO_DATE", flightToDate);
			parameters.put("ID", id);
			parameters.put("SEGMENTS", strSegmentList);
			parameters.put("POS", strPOSList);
			parameters.put("OPTYPE", strOperationType);
			parameters.put("FLIGHTNO", flightNo);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");

			if (AppSysParamsUtil.isPromoCodeEnabled()) {
				parameters.put("SHW_DISCOUNT", true);
			} else {
				parameters.put("SHW_DISCOUNT", false);
			}

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=SectorContributionByAgents.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=SectorContributionByAgents.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=SectorContributionByAgents.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportViewCountry(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String flightFromDate = request.getParameter("txtFlightFromDate");
		String flightToDate = request.getParameter("txtFlightToDate");
		String value = request.getParameter("radReportOption");
		String flightNo = request.getParameter("txtFlightNo");
		String strCountryList = request.getParameter("hdnCountries");
		List<String> countryList = new ArrayList<String>();
		if (strCountryList != null && strCountryList != "") {
			countryList = Arrays.asList(strCountryList.split(","));
		}
		String strSegmentList = request.getParameter("hdnSegments");
		List<String> segmentList = new ArrayList<String>();

		if (strSegmentList != null && strSegmentList != "") {
			segmentList = Arrays.asList(strSegmentList.split(","));
		}

		String id = "UC_REPM_014";
		String reportTemplate = "CountryContributionForFlight.jasper";
		String image = "../images/".concat(AppSysParamsUtil.getReportLogo(true));
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strOperationType = request.getParameter("selOperationType");

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (flightFromDate != null && !flightFromDate.equals("")) {
				search.setFlightDateRangeFrom(ReportsHTMLGenerator.convertDate(flightFromDate));
			}
			if (flightToDate != null && !flightToDate.equals("")) {
				search.setFlightDateRangeTo(ReportsHTMLGenerator.convertDate(flightToDate));
			}
			if (countryList != null && countryList.size() > 0) {
				search.setSelectedCountries(countryList);
			}
			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo.trim());
			}

			if (strOperationType != null) {
				search.setOperationType(strOperationType);
			}
			if (segmentList != null && segmentList.size() > 0) {
				search.setSelectedSegments(segmentList);
			}
			resultSet = ModuleServiceLocator.getDataExtractionBD().getCountryContributionPerFlightSegmentData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("FLI_FROM_DATE", flightFromDate);
			parameters.put("FLI_TO_DATE", flightToDate);
			parameters.put("COUNTRY", strCountryList);
			parameters.put("ID", id);
			parameters.put("FLIGHTNO", flightNo);
			parameters.put("SEGMENTS", strSegmentList);
			parameters.put("OPTYPE", strOperationType);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");

			if (AppSysParamsUtil.isPromoCodeEnabled()) {
				parameters.put("SHW_DISCOUNT", true);
			} else {
				parameters.put("SHW_DISCOUNT", false);
			}

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=CountryContributionForFlight.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(AppSysParamsUtil.getReportLogo(false));
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CountryContributionForFlight.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CountryContributionForFlight.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}
