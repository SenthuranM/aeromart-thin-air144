package com.isa.thinair.xbe.core.util;

public class StringUtil {

	/**
	 * Returns not null string
	 * 
	 * @param str
	 *            The String to check
	 * @return String the Not Null String
	 */
	public static String getNotNullString(String str) {
		return (str == null) ? "" : str.trim();
	}

}
