package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationPreferrenceInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentEmailUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.dto.FlexiDTO;
import com.isa.thinair.webplatform.api.dto.ReservationConstants;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.ItineraryAnciAvailability;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.FlightListTO;
import com.isa.thinair.xbe.api.dto.v2.ItineraryPaxTO;
import com.isa.thinair.xbe.api.dto.v2.ItineraryPaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ItineraryUtil;
import com.isa.thinair.xbe.core.web.v2.util.ParameterUtil;
import com.isa.thinair.xbe.core.web.v2.util.PassengerUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Passenger Tab
 * 
 * @author Rilwan A. Latiff
 */
/**
 * @author tfernando
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ConfirmationTabAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ConfirmationTabAction.class);

	private static final String SELECTED_ORIGIN_COUNTRY = "selectedOriginCountry";

	/* outbound params */
	private boolean success = true;

	private String messageTxt;

	private String agentBalance;

	private List<LCCClientReservationInsurance> insurances;

	private ItineraryAnciAvailability ancillaryAvailability;

	private List<FlexiDTO> flexiRuleDetails;

	private BookingTO bookingTO;

	private BookingTO splitBookingTO;

	private Collection<FlightListTO> flightDepDetails;

	private Collection<FlightListTO> flightRetDetails;

	private Collection<ItineraryPaxTO> passengerDetails;

	private Collection<ItineraryPaxTO> splitPassengerDetails;

	private List<TaxInvoice> taxInvoices;

	private ContactInfoTO contactInfoTO;

	private ItineraryPaymentSummaryTO itineraryPaymentSummaryTO;

	/* inbound params */
	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private ContactInfoTO contactInfo;

	private PaymentTO payment;

	private String groupBookingReference;

	private CreditCardTO creditInfo;

	private boolean onHoldBooking = false;

	private boolean offlinePayment = false;

	private boolean waitListingBooking = false;

	private boolean forceConfirm = false;

	private String hdnDestination;

	private String hdnSelCurrency;

	private String selectedFlightList;

	private FlightSearchDTO searchParams;

	private String flexiSelected;

	private boolean blnSplitBooking = false;

	private boolean blnPrintItinerary = true;

	private String enableRecieptGeneration;

	private boolean customerProfileUpdate = false;

	private String selectedBookingCategory;

	private String itineraryFareMaskOption;

	private boolean actualPayment;

	private boolean creditDiscount;

	private BigDecimal totalAmount;

	private GroupBookingRequestTO groupBookingRequest;

	private String agentBalanceInAgentCurrency;

	private String paxState;

	private String paxCountryCode;

	private String paymentGatewayBillNumber;

	private String paymentGatewayBillNumberLabel;

	private boolean paxTaxRegistered;

	private String fatouratiRef;
	
	private String selectedOriginCountry;


	@SuppressWarnings("unchecked")
	public String execute() {
		HttpSession session = request.getSession();

		Integer ipgId = null;

		enableRecieptGeneration = "N";

		boolean partialPaxPaymentAllowed = false;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
			IPGResponseDTO externalPGWResponseDTO = (IPGResponseDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE);
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) session
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			// tracking info
			TrackInfoDTO trackInfoDTO = this.getTrackInfo();

			if (request.getParameter(WebConstants.REQ_HDN_IPG_ID) != null
					&& !"".equals(request.getParameter(WebConstants.REQ_HDN_IPG_ID))) {
				ipgId = new Integer(request.getParameter(WebConstants.REQ_HDN_IPG_ID));
			}

			// update handling fee according selected fare type
			bookingShoppingCart.setTotalExternalChangeAmount(EXTERNAL_CHARGES.HANDLING_CHARGE);

			String strTxnIdntifier = resInfo.getTransactionId();

			// get preferred language from contact info
			String preferredLang = contactInfo.getPreferredLang();

			// set the email itinerary language
			String emailLanguage = (request.getParameter("selLang") == null) ? preferredLang : request.getParameter("selLang");

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			if (log.isDebugEnabled()) {
				log.debug(" Transaction Identifier " + strTxnIdntifier);
				log.debug(" Preferred Language " + preferredLang);
				log.debug(" Email Language " + emailLanguage);
				log.debug(" +++++bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getTotalBaseFare "
						+ bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getTotalBaseFare() + "+++++++");
			}

			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(searchParams, outFlightRPHList, retFlightRPHList);
			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList,
					searchParams);
			SortUtil.sortFlightSegByDepDate(flightSegmentTOs);
			ReservationProcessParams resProcessParams = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, resInfo.getLccPromotionInfoTO(),
					null, false, false);

			if (hasTBAPax(bookingShoppingCart.getPaxList()) && !resProcessParams.isTbaBookings()) {
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}

			// if the quote is done and wait for some time check this
			PriceInfoTO priceInfoTO = bookingShoppingCart.getPriceInfoTO();
			FareTypeTO fare = priceInfoTO.getFareTypeTO();

			if (onHoldBooking) {
				if (log.isDebugEnabled()) {
					log.debug(" On hold times - start time: " + fare.getOnHoldStartTime() + ", end time: "
							+ fare.getOnHoldStopTime());
				}
				long bufferStartTime = fare.getOnHoldStartTime().getTime();
				long bufferEndTime = fare.getOnHoldStopTime().getTime();
				long currentTime = new Date().getTime();
				if (currentTime > bufferEndTime) {
					throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
				}

				if (currentTime > bufferStartTime && !resProcessParams.isBufferTimeOnHold()) {
					throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
				}

			} else {
				// check Reciept Generation is enabled or Not
				enableRecieptGeneration = isReceptGenerationEnable();
			}

			BigDecimal payableAmt = new BigDecimal(payment.getAmount().trim());

			if (!onHoldBooking && payableAmt.compareTo(BigDecimal.ZERO) == 1) {
				PaymentMethod paymentMethod = payment.getPaymentMethod();
				if ((payment.getType() == null)
						|| (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode()) && !resProcessParams.isCashPayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())
								&& !resProcessParams.isOfflinePayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())
								&& !resProcessParams.isOnAccountPayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode()) && !resProcessParams.isCreditPayments())
						|| (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode()) && !resProcessParams.isBspPayments())) {
					throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
				}
				if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
					payment.setAmount(totalAmount.toString());
				}
				if (groupBookingReference != null && groupBookingReference.length() > 0) {
					partialPaxPaymentAllowed = true;
					if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
						// bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD);
						payment.setAmount(totalAmount.toString());
					}
					if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
						payment.setAmount(totalAmount.toString());
					}
					if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
						payment.setAmount(totalAmount.toString());
					}
				}
				this.validate(flightSegmentTOs);
			}
			if (payment.getPaymentMethod().getCode().equals(PaymentMethod.OFFLINE.getCode())) {
				offlinePayment = true;
			}
			// Override selected currency with BSP currency code if different
			if (payment.getPaymentMethod().getCode().equals(PaymentMethod.BSP.getCode())) {
				hdnSelCurrency = ModuleServiceLocator.getCommonServiceBD()
						.getCurrencyCodeForStation(userPrincipal.getAgentStation());
			}

			checkTransferOwnershipCompliance(payment, resProcessParams, userPrincipal);

			SSRServicesUtil.setSegmentSeqForSSR((List<ReservationPaxTO>) bookingShoppingCart.getPaxList(), flightSegmentTOs,
					null);
			// TODO set the partial pax payment based on the group booking count and
			// TODO add groupBookingValidations

			CommonReservationAssembler commonReservationAssembler = BookingUtil.createBookingDetails(
					request.getParameter("paxInfants"), contactInfo, payment, creditInfo, ipgId, bookingShoppingCart,
					onHoldBooking, forceConfirm, hdnSelCurrency, request.getParameter("paxPayments"), partialPaxPaymentAllowed,
					externalPGWResponseDTO, userPrincipal);
			ExternalChargeUtil.injectOndBaggageGroupId(bookingShoppingCart.getPaxList(), flightSegmentTOs);
			commonReservationAssembler.setWaitListingBooking(waitListingBooking);
			BookingUtil.addSegments(commonReservationAssembler, flightSegmentTOs);// request.getSession().getAttribute("sesPrivilegeIds")
			boolean isAllowBookingAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME);
			boolean isAllowOverBookAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME);
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(isAllowOverBookAfterCutOffTime);
			commonReservationAssembler.setAllowFlightSearchAfterCutOffTime(isAllowBookingAfterCutOffTime);
			commonReservationAssembler.setBookingCategory(selectedBookingCategory);

			String  reasonForAllowBlPaxRes =request.getParameter(WebConstants.REQ_REASON_FOR_ALLOW);
			if(reasonForAllowBlPaxRes!=null && !reasonForAllowBlPaxRes.equals("")){
				commonReservationAssembler.setReasonForAllowBlPaxRes(reasonForAllowBlPaxRes);
			}
			
			if (!resProcessParams.isItineraryFareMaskPrivilege()) {
				itineraryFareMaskOption = "N";
			}

			commonReservationAssembler.setActualPayment(actualPayment);

			boolean skipDuplicationCheck = false;
			if (resInfo.hasDuplicates() && resProcessParams.isDuplicateNameSkipPrivilege()) {
				skipDuplicationCheck = true;
			}
			selectedOriginCountry = request.getParameter(SELECTED_ORIGIN_COUNTRY);

			commonReservationAssembler.setBookingType(resInfo.getBookingType());
			commonReservationAssembler.setItineraryFareMask(itineraryFareMaskOption);
			commonReservationAssembler.setOriginCountryOfCall(selectedOriginCountry);
			commonReservationAssembler.setAppIndicator(AppIndicatorEnum.APP_XBE);
			commonReservationAssembler.setSelectedFare(fare);
			commonReservationAssembler.setSelectedFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
			boolean isFlexiQuote = ReservationUtil.isFlexiSelected(flexiSelected);
			commonReservationAssembler.setFlexiQuote(isFlexiQuote);
			if (!bookingShoppingCart.isSkipPromotionForOnHold()) {
				commonReservationAssembler.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
				commonReservationAssembler.setDiscountAmount(bookingShoppingCart.getTotalFareDiscount(true));
			}
			commonReservationAssembler.setLccTransactionIdentifier(strTxnIdntifier);
			commonReservationAssembler.setTargetSystem(resInfo.getSelectedSystem());
			// TODO check whether exchange rate exits for this currency and default to base
			commonReservationAssembler.setSelectedCurrency(searchParams.getSelectedCurrency());
			commonReservationAssembler.setSkipDuplicateCheck(skipDuplicationCheck);
			commonReservationAssembler.setAgentCommissions(bookingShoppingCart.getAgentCommissions());

			commonReservationAssembler.setChargesApplicability(bookingShoppingCart.getExternalChargesType());
			commonReservationAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));

			if (onHoldBooking || commonReservationAssembler.isSplitBooking()) {
				Map<String, String> mapPrivileges = (Map<String, String>) request.getSession()
						.getAttribute(WebConstants.SES_PRIVILEGE_IDS);
				Date releaseTimestamp = BookingUtil.getReleaseTimestamp(flightSegmentTOs, mapPrivileges,
						userPrincipal.getAgentCode(), fare, null, null);
				if (releaseTimestamp == null) {
					throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
				}

				commonReservationAssembler.setPnrZuluReleaseTimeStamp(releaseTimestamp);
			}
			AncillaryDTOUtil.populateResPaxWithSelAnciDtls(bookingShoppingCart.getPaxList(), commonReservationAssembler);
			AncillaryDTOUtil.populateInsurance(bookingShoppingCart.getInsuranceQuotes(), commonReservationAssembler);
			commonReservationAssembler.setFlightPriceRQ(flightPriceRQ);

			commonReservationAssembler.setFlightSegmentTOs(flightSegmentTOs);

			CommonReservationPreferrenceInfo preferrenceInfo = new CommonReservationPreferrenceInfo();
			preferrenceInfo.setPreferredLanguage(emailLanguage);
			commonReservationAssembler.getLccreservation().setPreferrenceInfo(preferrenceInfo);
			if (log.isDebugEnabled()) {
				log.debug("Calling Airproxy Reservation service");
			}

			if (commonReservationAssembler.isSplitBooking()) {
				if (!resProcessParams.isPartialPayments()) {
					throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
				}
			}

			if (flightPriceRQ.getTravelPreferences().isOpenReturn()) {
				setFlightSegIdForOpenReturnFlexiCharge(commonReservationAssembler);
			}
			ServiceResponce responceGroupBooking = null;
			if (groupBookingReference != null && groupBookingReference.length() > 0) {
				String passengerList = searchParams.getAdultCount() + "|" + searchParams.getChildCount() + "|"
						+ searchParams.getInfantCount();
				responceGroupBooking = ModuleServiceLocator.getGroupBookingBD().updateBookingStatus(
						Long.parseLong(groupBookingReference), totalAmount, passengerList, outFlightRPHList, retFlightRPHList);
				commonReservationAssembler.setGroupBookingRequestID(Long.parseLong(groupBookingReference));
			}
			// check permission for user to add private user note
			commonReservationAssembler.setAllowClassifyUN(resProcessParams.isAllowClassifyUserNotes());
			if (groupBookingReference == null || groupBookingReference.equals("")
					|| (responceGroupBooking != null && responceGroupBooking.isSuccess())) {
				ServiceResponce responce = ModuleServiceLocator.getAirproxyReservationBD()
						.makeReservation(commonReservationAssembler, trackInfoDTO);
				
				

				LCCClientReservation lccClientReservation = (LCCClientReservation) responce
						.getResponseParam(CommandParamNames.RESERVATION);
				Object fatouratiRef = responce.getResponseParam(CommandParamNames.CMI_FATOURATI_REF);
				if (fatouratiRef != null) {
					this.setFatouratiRef((String) fatouratiRef);
				}
				log.info("Fatourati ref==============******"+this.getFatouratiRef());
				boolean isOtherCarrierCreditUsed = (Boolean) responce
						.getResponseParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED);

				if (StringUtils.isNoneEmpty((String) responce.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER))) {
					paymentGatewayBillNumber = (String) responce.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER);
					paymentGatewayBillNumberLabel = (String) responce
							.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY);
				}
				// Reconcile dummy carrier reservation if other pax credit used
				if (isOtherCarrierCreditUsed) {
					ReservationUtil.reconcileDummyCarrierReservation(lccClientReservation.getPNR(), trackInfoDTO);
				}

				// Update release timestamp of the dummy booking if not own airline involved reservation
				if (onHoldBooking && (lccClientReservation.getZuluReleaseTimeStamp()
						.compareTo(commonReservationAssembler.getPnrZuluReleaseTimeStamp()) != 0)) {
					ReservationUtil.updateReleaseTimeForDummyBooking(lccClientReservation.getPNR(),
							lccClientReservation.getZuluReleaseTimeStamp(), trackInfoDTO);
				}

				if (log.isDebugEnabled()) {
					log.debug("Reservation Successful, PNR [" + lccClientReservation.getPNR() + "] isGroupPNR ["
							+ lccClientReservation.isGroupPNR() + "]");
				}

				// Update customer profile if checked
				if (customerProfileUpdate) {
					AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
					Customer oldCustomer = customerDelegate.getCustomer(contactInfo.getEmail());

					if (oldCustomer != null) {
						Customer newCust = PassengerUtil.createCustomerProfile(contactInfo, oldCustomer);
						customerDelegate.saveOrUpdate(newCust);
					}
				}

				AirProxyReservationUtil.skipPromotionAdjustments(lccClientReservation);

				// Itinerary Details
				String strStation = userPrincipal.getAgentStation();
				Collection<UserDST> colUserDST = userPrincipal.getColUserDST();
				bookingTO = ItineraryUtil.createBookingDetails(lccClientReservation, colUserDST, strStation);
				Object[] fltObj = ItineraryUtil.createSelecteFlightInfo(lccClientReservation);
				flightDepDetails = (Collection<FlightListTO>) fltObj[0];
				flightRetDetails = (Collection<FlightListTO>) fltObj[1];
				passengerDetails = ItineraryUtil.createPassengerList(lccClientReservation);
				ancillaryAvailability = ItineraryUtil.getAncillaryAvailability(passengerDetails);
				contactInfoTO = ItineraryUtil.createContactInfo(lccClientReservation);

				insurances = lccClientReservation.getReservationInsurances();
				flexiRuleDetails = ItineraryUtil.createFlexiRules(lccClientReservation, preferredLang);

				String selectedCurrencyCode = StringUtil.getNotNullString(hdnSelCurrency);

				if (selectedCurrencyCode.length() == 0) {
					selectedCurrencyCode = AppSysParamsUtil.getBaseCurrency();
				}

				// for split reservations
				LCCClientReservation ohdLccClientReservation = null;
				if (commonReservationAssembler.isSplitBooking()) {
					ohdLccClientReservation = (LCCClientReservation) responce.getResponseParam(CommandParamNames.OHD_RESERVATION);
					splitPassengerDetails = ItineraryUtil.createPassengerList(ohdLccClientReservation);
					splitBookingTO = ItineraryUtil.createBookingDetails(ohdLccClientReservation, colUserDST, strStation);
					blnSplitBooking = true;
					if (!resProcessParams.isPrintItineraryForPartialPayment())
						blnPrintItinerary = false;

				}

				itineraryPaymentSummaryTO = ItineraryUtil.createPaymentSummary(lccClientReservation, selectedCurrencyCode);

				if (lccClientReservation.getLccPromotionInfoTO() != null && PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT
						.equals(lccClientReservation.getLccPromotionInfoTO().getDiscountAs())) {
					creditDiscount = true;
				}

				// When merging refactor this method using the existing method
				// agentBalance = ReservationHG.getUsersAvailableCredit(request);
				agentBalance = CommonUtil.getLoggedInAgentBalance(request);
				agentBalanceInAgentCurrency = CommonUtil.getLoggedInAgentBalanceInAgentCurrency(request);
				// Email Itinerary
				try {
					if (lccClientReservation.getStatus() != null && (ReservationInternalConstants.ReservationStatus.CONFIRMED
							.equals(lccClientReservation.getStatus()) || isFatouratiExists()) ) {
						if (BeanUtils.nullHandler(request.getParameter("chkEmail")).compareTo("on") == 0
								&& contactInfo.getEmail() != null && !BeanUtils.nullHandler(contactInfo.getFirstName()).isEmpty()
								&& !BeanUtils.nullHandler(contactInfo.getLastName()).isEmpty()
								&& !BeanUtils.nullHandler(contactInfo.getEmail()).isEmpty()) {

							String agentStation = ((UserPrincipal) request.getUserPrincipal()).getAgentStation();
							boolean includePaxFinancials = true;
							if (!BeanUtils.nullHandler(request.getParameter("chkPayPaxChg")).trim().equals("")) {
								includePaxFinancials = BeanUtils.nullHandler(request.getParameter("chkPayPaxChg")).equals("on");
							}
							LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(lccClientReservation.getPNR(),
									lccClientReservation.isGroupPNR(), null, request, false, null, null, null, false);
							CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
							commonItineraryParam.setItineraryLanguage(emailLanguage);
							commonItineraryParam.setIncludePaxFinancials(includePaxFinancials);
							commonItineraryParam.setIncludePaymentDetails(false);
							commonItineraryParam.setIncludeTicketCharges(false);
							commonItineraryParam.setStation(agentStation);
							commonItineraryParam.setAppIndicator(ApplicationEngine.XBE);
							commonItineraryParam.setIncludeTermsAndConditions(true);
							commonItineraryParam
									.setAirportMap(SelectListGenerator.getAirportsList(lccClientReservation.isGroupPNR()));
							commonItineraryParam.setIncludePaxContactDetails(true);
							commonItineraryParam.setIncludeStationContactDetails(true);

							if (isCustomizedItinerary(request) && !isHeadOfficeAgent(request)) {
								commonItineraryParam.setIncludePaxFinancials(false);
								commonItineraryParam.setIncludePaymentDetails(false);
								commonItineraryParam.setIncludeTicketCharges(false);
							}

							String selectedPax = "";
							if (AppSysParamsUtil.isPrintEmailIndItinerary()) {
								for (int i = 1; i < (commonReservationAssembler.getAdultCount()
										+ commonReservationAssembler.getChildCount() + 1); i++)
									selectedPax += i + ",";

								String paxs[] = selectedPax.split(",");
								for (String pax : paxs) {
									if (pax != null || !pax.equals("")) {
										commonItineraryParam.setSelectedPaxDetails(pax);
										/*Send itinerary on confirmed status only*/
										if(ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())){
										ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO,
												commonItineraryParam, trackInfoDTO);
										}
									}
								}
							} else if(ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())){
								ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO,
										commonItineraryParam, trackInfoDTO);
								}else{
									/*Fatourati payment. Send Fatourati to client email*/
									lccClientReservation.setFatourati(this.getFatouratiRef());
									PaymentEmailUtil.sendFatouratiEmail(lccClientReservation, commonItineraryParam, trackInfoDTO);
								}
						}
					}

				} catch (Exception e) {
					log.error("Error while trying to send itinerary by email after payment", e);
				}

				if (lccClientReservation.getStatus() != null
						&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())) {
					// to send medical ssr email
					SSRServicesUtil.sendMedicalSsrEmail(lccClientReservation);
				}

				// Hide Stop Over Airport
				if (AppSysParamsUtil.isHideStopOverEnabled()) {
					hideStopOverAirport(flightDepDetails);
					hideStopOverAirport(flightRetDetails);
				}

				// Adding this part to clear the session Data;
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART,
						new BookingShoppingCart(ChargeRateOperationType.MAKE_ONLY));
				request.getSession().setAttribute(S2Constants.Session_Data.XBE_SES_RESDATA, new XBEReservationInfoDTO());
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, null);
				request.getSession().setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, null);
				request.getSession().setAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE, null);
				if (lccClientReservation.getTaxInvoicesList() != null) {
					taxInvoices = lccClientReservation.getTaxInvoicesList();
				}

			} else {
				success = false;
				messageTxt = "Booking confirmation failed.Please try again later";
				log.error(messageTxt);
			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		} finally {
			request.getSession().setAttribute(S2Constants.Session_Data.EXTERNAL_PAYMENT_GATEWAY_RESPONSE, null);
		}
		return S2Constants.Result.SUCCESS;
	}

	private boolean isFatouratiExists() {
		return this.getFatouratiRef() != null && !this.getFatouratiRef().isEmpty();
	}

	private boolean hasTBAPax(Collection<ReservationPaxTO> paxList) {
		for (ReservationPaxTO pax : paxList) {
			if (pax.isTBA()) {
				return true;
			}
		}
		return false;
	}

	public String groupBookingValidate() {
		long requestID = Long.parseLong(groupBookingReference);
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String passengerList = searchParams.getAdultCount() + "|" + searchParams.getChildCount() + "|"
					+ searchParams.getInfantCount();
			groupBookingRequest = ModuleServiceLocator.getGroupBookingBD().validateGroupBookingRequest(requestID, totalAmount,
					passengerList, outFlightRPHList, retFlightRPHList, user.getAgentCode());

			// If the agreed fare is set as a percentage then calculate it accordingly
			if (groupBookingRequest != null && groupBookingRequest.isAgreedFarePercentage()) {
				groupBookingRequest.setMinPaymentRequired(groupBookingRequest.getAgreedFare().divide(new BigDecimal("100"))
						.multiply(groupBookingRequest.getMinPaymentRequired()));
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	// In KAM air it was reported that some users are performing a transfer ownership with out having the transfer
	// ownership privileges. We should never ever trust the frontends (jsp/html/js) and the respective sanity checks
	// should be done in the action level before calling the Business logics. This issue motivated to add this
	// validation.
	// Nili Jan 18, 2012
	private void checkTransferOwnershipCompliance(PaymentTO paymentTO, ReservationProcessParams resProcessParams,
			UserPrincipal userPrincipal) {
		if (resProcessParams.isOnHold() && !userPrincipal.getAgentCode().equals(paymentTO.getAgent())) {
			if (resProcessParams.isTransferOwnership() || resProcessParams.isTransferOwnershipToAnyCarrier()) {
				// only having the transfer ownership and transfer ownership to any carrier is allowed to proceed.
			} else {
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		}
	}

	// This method assigns dummy flight segment's ID for flexi external charge in open return booking
	private void setFlightSegIdForOpenReturnFlexiCharge(CommonReservationAssembler commonReservationAssembler) {
		FareSegChargeTO fareSegChargeTO = commonReservationAssembler.getSelectedFareSegChargeTO();
		LCCClientReservation lccRes = commonReservationAssembler.getLccreservation();

		OndFareSegChargeTO openRTSegChgTO = null;
		for (OndFareSegChargeTO segChgTo : fareSegChargeTO.getOndFareSegChargeTOs()) {
			if (segChgTo.getOndSequence() == OndSequence.IN_BOUND) {
				openRTSegChgTO = segChgTo;
			}
		}

		if (openRTSegChgTO != null) {
			Set<Integer> flightSegIds = openRTSegChgTO.getFsegBCAlloc().keySet();
			Set<LCCClientReservationPax> lccPaxs = lccRes.getPassengers();
			for (LCCClientReservationPax lccPax : lccPaxs) {
				Iterator<Integer> fltSegIterator = flightSegIds.iterator();
				LCCClientPaymentAssembler lccClientPaymentAssembler = lccPax.getLccClientPaymentAssembler();
				if (lccClientPaymentAssembler != null) {
					Collection<LCCClientExternalChgDTO> perPaxExternalCharges = lccClientPaymentAssembler
							.getPerPaxExternalCharges();
					if (perPaxExternalCharges != null) {
						for (LCCClientExternalChgDTO extChg : perPaxExternalCharges) {
							if (extChg.getFlightRefNumber() == null || "".equals(extChg.getFlightRefNumber())) {
								if (fltSegIterator.hasNext()) {
									extChg.setFlightRefNumber(fltSegIterator.next().toString());
								}
							}
						}
					}
				}
			}
		}

	}

	/**
	 * @param fltSegs
	 * @throws ModuleException
	 */
	private void validate(Collection<FlightSegmentTO> fltSegs) throws ModuleException {

		PaymentMethod paymentMethod = payment.getPaymentMethod();
		if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {

			if (fltSegs.size() > ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT) {
				XBERuntimeException ex = new XBERuntimeException("bsp.payment.segment.count.exceeded");
				List<String> messageParameters = new ArrayList<String>();
				messageParameters.add(ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT + "");
				ex.setMessageParameters(messageParameters);
				throw ex;
			}
		}
	}

	private void hideStopOverAirport(Collection<FlightListTO> flightDetailsTo) {
		for (FlightListTO flightTo : flightDetailsTo) {
			for (com.isa.thinair.airproxy.api.dto.FlightInfoTO flightInfo : flightTo.getFlights()) {
				String flightOnd = flightInfo.getOrignNDest();
				if (flightOnd != null && flightOnd.split("/").length > 2) {
					flightInfo.setOrignNDest(ReservationApiUtils.hideStopOverSeg(flightOnd));
				}
			}
		}
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(
				payment != null ? payment.getAgent() : ((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the contactInfo
	 */
	@JSON(serialize = false)
	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the payment
	 */
	@JSON(serialize = false)
	public PaymentTO getPayment() {
		return payment;
	}

	/**
	 * @param payment
	 *            the payment to set
	 */
	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	/**
	 * @return the flightDetails
	 */
	public Collection<FlightListTO> getFlightDepDetails() {
		return flightDepDetails;
	}

	/**
	 * @return the flightDetails
	 */
	public Collection<FlightListTO> getFlightRetDetails() {
		return flightRetDetails;
	}

	/**
	 * @return the contactInfoTO
	 */
	public ContactInfoTO getContactInfoTO() {
		return contactInfoTO;
	}

	/**
	 * @return the passengerDetails
	 */
	public Collection<ItineraryPaxTO> getPassengerDetails() {
		return passengerDetails;
	}

	/**
	 * @return the bookingTO
	 */
	public BookingTO getBookingTO() {
		return bookingTO;
	}

	/**
	 * @return the itineraryPaymentSummaryTO
	 */
	public ItineraryPaymentSummaryTO getItineraryPaymentSummaryTO() {
		return itineraryPaymentSummaryTO;
	}

	@JSON(serialize = false)
	public CreditCardTO getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(CreditCardTO creditInfo) {
		this.creditInfo = creditInfo;
	}

	/**
	 * @param onHoldBooking
	 *            the onHoldBooking to set
	 */
	public void setOnHoldBooking(boolean onHoldBooking) {
		this.onHoldBooking = onHoldBooking;
	}

	public boolean isWaitListingBooking() {
		return waitListingBooking;
	}

	public void setWaitListingBooking(boolean waitListingBooking) {
		this.waitListingBooking = waitListingBooking;
	}

	public void setSelectedBookingCategory(String selectedBookingCategory) {
		this.selectedBookingCategory = selectedBookingCategory;
	}

	public void setItineraryFareMaskOption(String itineraryFareMaskOption) {
		this.itineraryFareMaskOption = itineraryFareMaskOption;
	}

	public void setHdnDestination(String hdnDestination) {
		this.hdnDestination = hdnDestination;
	}

	public void setHdnSelCurrency(String hdnSelCurrency) {
		this.hdnSelCurrency = hdnSelCurrency;
	}

	public String getAgentBalance() {
		return agentBalance;
	}

	public List<LCCClientReservationInsurance> getInsurances() {
		return insurances;
	}

	public ItineraryAnciAvailability getAncillaryAvailability() {
		return ancillaryAvailability;
	}

	/**
	 * @return the outFlightRPHList
	 */
	@JSON(serialize = false)
	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	/**
	 * @param outFlightRPHList
	 *            the outFlightRPHList to set
	 */
	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	/**
	 * @return the retFlightRPHList
	 */
	@JSON(serialize = false)
	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	/**
	 * @param retFlightRPHList
	 *            the retFlightRPHList to set
	 */
	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setFlexiSelected(String flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	public void setForceConfirm(boolean forceConfirm) {
		this.forceConfirm = forceConfirm;
	}

	public String getEnableRecieptGeneration() {
		return enableRecieptGeneration;
	}

	public void setEnableRecieptGeneration(String enableRecieptGeneration) {
		this.enableRecieptGeneration = enableRecieptGeneration;
	}

	public boolean isBlnSplitBooking() {
		return blnSplitBooking;
	}

	public boolean isBlnPrintItinerary() {
		return blnPrintItinerary;
	}

	public Collection<ItineraryPaxTO> getSplitPassengerDetails() {
		return splitPassengerDetails;
	}

	public BookingTO getSplitBookingTO() {
		return splitBookingTO;
	}

	public boolean isCustomerProfileUpdate() {
		return customerProfileUpdate;
	}

	public void setCustomerProfileUpdate(boolean customerProfileUpdate) {
		this.customerProfileUpdate = customerProfileUpdate;
	}

	public String getSelectedOriginCountry() {
		return selectedOriginCountry;
	}

	public void setSelectedOriginCountry(String selectedOriginCountry) {
		this.selectedOriginCountry = selectedOriginCountry;
	}

	private static boolean isCustomizedItinerary(HttpServletRequest request) {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams processParams = new ReservationProcessParams(request,
				resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
		return processParams.isCustomizedItineraryView();
	}

	private static boolean isHeadOfficeAgent(HttpServletRequest request) {
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		boolean headOfficeAgent = false;
		if (userPrincipal != null) {
			String agentTypeCode = userPrincipal.getAgentTypeCode();
			if (agentTypeCode.equals(AppSysParamsUtil.getCarrierAgent())) {
				headOfficeAgent = true;
			}
		}
		return headOfficeAgent;
	}

	private String isReceptGenerationEnable() throws ModuleException {

		String paymentMode = null;
		String showRecieptEnable;

		PaymentMethod paymentType = payment.getPaymentMethod();

		if (paymentType.getCode().equals(PaymentMethod.CASH.getCode())) {
			paymentMode = Agent.PAYMENT_MODE_CASH;
		} else if (paymentType.getCode().equals(PaymentMethod.ACCO.getCode())) {
			paymentMode = Agent.PAYMENT_MODE_ONACCOUNT;
		} else if (paymentType.getCode().equals(PaymentMethod.CRED.getCode())) {
			paymentMode = Agent.PAYMENT_MODE_CREDITCARD;
		} else if (paymentType.getCode().equals(PaymentMethod.BSP.getCode())) {
			paymentMode = Agent.PAYMENT_MODE_BSP;
		} else if (paymentType.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
			paymentMode = Agent.PAYMENT_MODE_OFFLINE;
		}

		boolean recieptGeneration = ReservationBeanUtil.isRecieptGenerationEnable(paymentMode);

		if (recieptGeneration && (paymentType.getCode().equals(PaymentMethod.CASH.getCode())
				|| paymentType.getCode().equals(PaymentMethod.ACCO.getCode())
				|| paymentType.getCode().equals(PaymentMethod.BSP.getCode()))) {
			showRecieptEnable = "Y";
		} else {
			showRecieptEnable = "N";
		}

		return showRecieptEnable;
	}

	public List<FlexiDTO> getFlexiRuleDetails() {
		return flexiRuleDetails;
	}

	public void setFlexiRuleDetails(List<FlexiDTO> flexiRuleDetails) {
		this.flexiRuleDetails = flexiRuleDetails;
	}

	public boolean isActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public boolean isCreditDiscount() {
		return creditDiscount;
	}

	public void setGroupBookingReference(String groupBookingReference) {
		this.groupBookingReference = groupBookingReference;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public GroupBookingRequestTO getGroupBookingRequest() {
		return groupBookingRequest;
	}

	public void setGroupBookingRequest(GroupBookingRequestTO groupBookingRequest) {
		this.groupBookingRequest = groupBookingRequest;
	}

	public boolean isOfflinePayment() {
		return offlinePayment;
	}

	public void setOfflinePayment(boolean offlinePayment) {
		this.offlinePayment = offlinePayment;
	}

	public String getAgentBalanceInAgentCurrency() {
		return agentBalanceInAgentCurrency;
	}

	public void setAgentBalanceInAgentCurrency(String agentBalanceInAgentCurrency) {
		this.agentBalanceInAgentCurrency = agentBalanceInAgentCurrency;
	}

	private ExternalChgDTO getExternalCharge(BookingShoppingCart bookingShoppingCart, EXTERNAL_CHARGES externalChargeType) {
		return bookingShoppingCart.getSelectedExternalCharges().get(externalChargeType);
	}

	private void reCalculateAndApplyServiceTax(BookingShoppingCart bookingShoppingCart, BaseAvailRQ baseAvailRQ,
			List<FlightSegmentTO> flightSegmentTOs, String strTxnIdntifier) throws ModuleException {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

		if (bookingShoppingCart.getPriceInfoTO() != null && bookingShoppingCart.getPaxList().size() > 0) {

			ExternalChgDTO ccTransactionFeeDTO = null;
			ExternalChgDTO bspTransactionFeeDTO = null;
			BigDecimal ccTotalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal bspTotalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

			PaymentMethod paymentType = payment.getPaymentMethod();

			if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil.isAdminFeeChannelWiseConfigured(
					ApplicationEngine.XBE, RequestHandlerUtil.getAgentCode(request)))
					|| PaymentMethod.CRED.getCode().equals(paymentType.getCode())) {
				ccTransactionFeeDTO = getExternalCharge(bookingShoppingCart, EXTERNAL_CHARGES.CREDIT_CARD);

				if (ccTransactionFeeDTO != null && ccTransactionFeeDTO.getAmount().doubleValue() > 0) {
					ccTotalTransactionFee = ccTransactionFeeDTO.getAmount();
				}
			}

			if (PaymentMethod.BSP.getCode().equals(paymentType.getCode())) {
				bspTransactionFeeDTO = getExternalCharge(bookingShoppingCart, EXTERNAL_CHARGES.BSP_FEE);

				if (bspTransactionFeeDTO != null && bspTransactionFeeDTO.getAmount().doubleValue() > 0) {
					bspTotalTransactionFee = bspTransactionFeeDTO.getAmount();
				}
			}

			ExternalChgDTO handlingChargeDTO = bookingShoppingCart.getSelectedExternalCharges()
					.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
			BigDecimal handlingFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (handlingChargeDTO != null) {
				handlingFeeAmount = bookingShoppingCart.getHandlingCharge();
			}
			BigDecimal[] paxWiseHandlingFee = AccelAeroCalculator.roundAndSplit(handlingFeeAmount,
					bookingShoppingCart.getPaxList().size());

			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
			serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
			serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
			serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
			serviceTaxQuoteCriteriaDTO.setPaxState(paxState);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(paxCountryCode);
			serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(paxTaxRegistered);

			ServiceTaxCalculatorUtil.addPaxExternalChargesToServiceTaxQuoteRQ(serviceTaxQuoteCriteriaDTO,
					bookingShoppingCart.getPaxList(), flightSegmentTOs, ccTransactionFeeDTO, ccTotalTransactionFee,
					handlingChargeDTO, paxWiseHandlingFee, true, bspTransactionFeeDTO, bspTotalTransactionFee);

			serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(strTxnIdntifier);
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
					.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfo());

			ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(bookingShoppingCart.getPaxList(), serviceTaxQuoteRS, false,
					ApplicationEngine.XBE, false);

		}
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public List<TaxInvoice> getTaxInvoices() {
		return taxInvoices;
	}

	public void setTaxInvoices(List<TaxInvoice> taxInvoices) {
		this.taxInvoices = taxInvoices;
	}

	public String getFatouratiRef() {
		return fatouratiRef;
	}

	public void setFatouratiRef(String fatouratiRef) {
		this.fatouratiRef = fatouratiRef;
	}

	public String getPaymentGatewayBillNumber() {
		return paymentGatewayBillNumber;
	}

	public void setPaymentGatewayBillNumber(String paymentGatewayBillNumber) {
		this.paymentGatewayBillNumber = paymentGatewayBillNumber;
	}

	public String getPaymentGatewayBillNumberLabel() {
		return paymentGatewayBillNumberLabel;
	}

	public void setPaymentGatewayBillNumberLabel(String paymentGatewayBillNumberLabel) {
		this.paymentGatewayBillNumberLabel = paymentGatewayBillNumberLabel;
	}
}