package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.IBEOnHoldPassengersDTO;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class IBEOnHoldPassengersReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(IBEOnHoldPassengersReportRH.class);

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("IBEOnHoldPassengersReportRH setReportView Success");
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("IBEOnHoldPassengersReportRH setReportView Failed " + e);
		} catch (Exception e) {
			log.error(e);
		}
		return forward;
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		// TODO Auto-generated method stub
		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String timeOption = request.getParameter("radTimeOption");
		String releaseTimeOption = request.getParameter("radReleaseTimeOption");
		String flightNo = request.getParameter("txtFlightNumber");
		String depFromDate = request.getParameter("txtFromDepDate");
		String depToDate = request.getParameter("txtToDepDate");
		String rptOption = request.getParameter("radOption");
		String bookingFromDate = request.getParameter("txtBookFromDate");
		String bookingToDate = request.getParameter("txtBookToDate");

		String consideredTimePeriod = request.getParameter("txtReleaseHours");

		String bookCurrentStatus = request.getParameter("selBookCurStatus");
		String id = "UC_REPM_006";
		String reportTemplate = "IBEOnHoldPassengerReportForFlight.jasper";
		String agents = "";

		ReportsSearchCriteria search = new ReportsSearchCriteria();
		if (rptOption.equals("DATE_RANGE")) {
			if (releaseTimeOption.equals("LOCAL")) {// Mon Apr 01 04:30:00 IST 2013
				LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(ModuleServiceLocator.getAirportBD());
				String agentStation = ModuleServiceLocator.getDataExtractionBD().getAgentStationCode();
				Date localFromDate = timeAdder.getLocalDateTime(agentStation, new Date(ReportsHTMLGenerator.convertDate(fromDate)
						+ "  00:00:00"));
				Date localToDate = timeAdder.getLocalDateTime(agentStation, new Date(ReportsHTMLGenerator.convertDate(toDate)
						+ "  23:59:59"));

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					search.setDateRangeFrom(sdf.format(localFromDate));
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					search.setDateRangeTo(sdf.format(localToDate));
				}
			} else {
				if (!StringUtil.isNullOrEmpty(fromDate)) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + "  00:00:00");
				}
				if (!StringUtil.isNullOrEmpty(toDate)) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59");
				}
			}
		} else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
			search.setDateRangeFrom(dateFormat.format(new Date()));
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, Integer.valueOf(consideredTimePeriod));
			search.setDateRangeTo(dateFormat.format(cal.getTime()));
		}

		if (!StringUtil.isNullOrEmpty(depToDate) && !StringUtil.isNullOrEmpty(depFromDate)) {
			search.setDepartureDateRangeTo(ReportsHTMLGenerator.convertDate(depToDate));
			search.setDepartureDateRangeFrom(ReportsHTMLGenerator.convertDate(depFromDate));
		}

		if (!StringUtil.isNullOrEmpty(bookingFromDate) && !StringUtil.isNullOrEmpty(bookingToDate)) {
			search.setDateRange2From(ReportsHTMLGenerator.convertDate(bookingFromDate));
			search.setDateRange2To(ReportsHTMLGenerator.convertDate(bookingToDate));
		}
		if (!StringUtil.isNullOrEmpty(consideredTimePeriod)) {
			search.setConTime(consideredTimePeriod);
		}

		search.setReportType(rptOption);
		search.setFlightNumber(flightNo);
		search.setAgentCode(agents);
		search.setStatus(bookCurrentStatus);

		if (timeOption.trim().equals("LOCAL")) {
			search.setShowInLocalTime(true);
		} else if (timeOption.trim().equals("ZULU")) {
			search.setShowInLocalTime(false);
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		resultSet = ModuleServiceLocator.getDataExtractionBD().getIBEOnholdPassengersData(search);

		List<IBEOnHoldPassengersDTO> colReportData = createIBEOnHoldPassengersDTOList(resultSet,
				((UserPrincipal) request.getUserPrincipal()), search.isShowInLocalTime());

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("FROM_DATE", search.getDateRangeFrom());
		parameters.put("TO_DATE", search.getDateRangeTo());
		parameters.put("FLIGHT_NO", flightNo.toUpperCase());
		parameters.put("ID", id);

		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.CARRIER_NAME)
				+ " Reservation System";
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

		if (value.trim().equals("HTML")) {
			parameters.put("IMG", reportsRootDir);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, colReportData, null, null,
					response);
		} else if (value.trim().equals("PDF")) {
			response.reset();
			response.addHeader("Content-Disposition", "filename=OnHoldPassengerReportForFlight.pdf");
			reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo); // "AA145.jpg"
			parameters.put("IMG", reportsRootDir);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, colReportData, response);
		} else if (value.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=OnHoldPassengerReportForFlight.xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, colReportData, response);
		} else if (value.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=OnHoldPassengerReportForFlight.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, colReportData, response);
		}

	}

	private static void setDisplayData(HttpServletRequest request) {
		setClientErrors(request);
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());

		}
	}

	private static List<IBEOnHoldPassengersDTO> createIBEOnHoldPassengersDTOList(ResultSet ibeOnHoldPaxDetailsResults,
			UserPrincipal userPrinciple, boolean showInLocal) {

		List<IBEOnHoldPassengersDTO> ibeOnHoldPassengerList = new ArrayList<IBEOnHoldPassengersDTO>();
		try {

			if (showInLocal) {
				while (ibeOnHoldPaxDetailsResults.next()) {
					IBEOnHoldPassengersDTO ibeOnholdPassenger = new IBEOnHoldPassengersDTO();
					try {

						ibeOnholdPassenger.setReservationDate(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("RESERVATION_DATE"), "dd/MM/yy"),
								userPrinciple.getAgentStation()), "dd/MM/yy"));
						ibeOnholdPassenger.setReleaseDate(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("RELEASE_DATE"), "dd/MM/yy HH:mm:ss"),
								userPrinciple.getAgentStation()), "dd/MM/yy HH:mm:ss"));
						ibeOnholdPassenger.setFlightDate(ibeOnHoldPaxDetailsResults.getString("FLIGHT_DATE"));
						ibeOnholdPassenger.setFlightNumber(ibeOnHoldPaxDetailsResults.getString("FLIGHT_NUMBER"));
						ibeOnholdPassenger.setPnr(ibeOnHoldPaxDetailsResults.getString("PNR"));
						ibeOnholdPassenger.setNoOfPaxOnHold(String.valueOf(ibeOnHoldPaxDetailsResults
								.getString("NO_OF_PAX_ONHOLD")));
						ibeOnholdPassenger.setContactName(ibeOnHoldPaxDetailsResults.getString("CONTACT_NAME"));
						ibeOnholdPassenger.setSector(ibeOnHoldPaxDetailsResults.getString("SECTOR"));
						ibeOnholdPassenger.setResTel(ibeOnHoldPaxDetailsResults.getString("RES_TEL"));
						ibeOnholdPassenger.setMobile(ibeOnHoldPaxDetailsResults.getString("MOBILE"));
						ibeOnholdPassenger.setSegStatus(ibeOnHoldPaxDetailsResults.getString("SEG_STATUS"));
						ibeOnholdPassenger.setMxTime(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("MX_TIME"), "dd/MM/yy HH:mm"),
								userPrinciple.getAgentStation()), "dd/MM/yy HH:mm"));
						ibeOnholdPassenger.setModDate(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("MOD_DATE"), "dd/MM/yy HH:mm"),
								userPrinciple.getAgentStation()), "dd/MM/yy HH:mm"));

						ibeOnHoldPassengerList.add(ibeOnholdPassenger);
					} catch (ParseException e) {
						log.error(e);
					} catch (ModuleException e1) {
						log.error(e1);
					}
				}
			} else {
				while (ibeOnHoldPaxDetailsResults.next()) {
					IBEOnHoldPassengersDTO ibeOnholdPassenger = new IBEOnHoldPassengersDTO();
					ibeOnholdPassenger.setReservationDate(ibeOnHoldPaxDetailsResults.getString("RESERVATION_DATE"));
					ibeOnholdPassenger.setReleaseDate(ibeOnHoldPaxDetailsResults.getString("RELEASE_DATE"));
					ibeOnholdPassenger.setFlightDate(ibeOnHoldPaxDetailsResults.getString("FLIGHT_DATE"));
					ibeOnholdPassenger.setFlightNumber(ibeOnHoldPaxDetailsResults.getString("FLIGHT_NUMBER"));
					ibeOnholdPassenger.setPnr(ibeOnHoldPaxDetailsResults.getString("PNR"));
					ibeOnholdPassenger.setNoOfPaxOnHold(String.valueOf(ibeOnHoldPaxDetailsResults.getString("NO_OF_PAX_ONHOLD")));
					ibeOnholdPassenger.setContactName(ibeOnHoldPaxDetailsResults.getString("CONTACT_NAME"));
					ibeOnholdPassenger.setSector(ibeOnHoldPaxDetailsResults.getString("SECTOR"));
					ibeOnholdPassenger.setResTel(ibeOnHoldPaxDetailsResults.getString("RES_TEL"));
					ibeOnholdPassenger.setMobile(ibeOnHoldPaxDetailsResults.getString("MOBILE"));
					ibeOnholdPassenger.setSegStatus(ibeOnHoldPaxDetailsResults.getString("SEG_STATUS"));
					ibeOnholdPassenger.setMxTime(ibeOnHoldPaxDetailsResults.getString("MX_TIME"));
					ibeOnholdPassenger.setModDate(ibeOnHoldPaxDetailsResults.getString("MOD_DATE"));
					ibeOnHoldPassengerList.add(ibeOnholdPassenger);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ibeOnHoldPassengerList;
	}
}
