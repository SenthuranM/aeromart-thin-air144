package com.isa.thinair.xbe.core.web.v2.util;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

/**
 * Helper Class to Store Airport Drop Down List Creation Common Data
 * 
 * @author Navod Ediriweera
 * @since 04 Oct 2010
 */
public class AirportDDListGenerator {

	/**
	 * Prepares Airport List As a Javascript 2D array
	 * 
	 * @param request
	 *            - The httpRequest with privilige data
	 * @param filterList
	 *            - The list if present will use only the codes in filterList.get(i)[0] position
	 * @param arrName
	 *            - The Javascript arrayName to be created
	 * @return the Javascript Array of Airports in 2D format of {code,desc, code+desc}
	 * @throws ModuleException
	 */
	public static String prepareAirportsHtml(HttpServletRequest request, List<String[]> filterList, String arrName,
			boolean isSubStationAllowed) throws ModuleException {
		if (AppSysParamsUtil.isLCCConnectivityEnabled() && hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
			return JavaScriptGenerator.createAirportsHtmlV2(request, true, filterList, arrName, isSubStationAllowed);
		} else {
			return JavaScriptGenerator.createAirportsHtmlV2(request, false, filterList, arrName, isSubStationAllowed);
		}
	}

	public static String prepareOndMapAsPerAgent(HttpServletRequest request, List<String[]> filterList, String arrName,
			boolean isSubStationAllowed) throws ModuleException {
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			// TODO : Implement for LCC flow
			return null;
		} else {
			return JavaScriptGenerator.createOndMapAsPerAgent(request, false, filterList, arrName, isSubStationAllowed);
		}
	}

	/**
	 * Checks for Privilieges
	 * 
	 * @param request
	 * @param privilegeId
	 * @return
	 */
	private static boolean hasPrivilege(HttpServletRequest request, String privilegeId) {
		Map<String, String> mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		return (mapPrivileges.get(privilegeId) != null) ? true : false;
	}

}
