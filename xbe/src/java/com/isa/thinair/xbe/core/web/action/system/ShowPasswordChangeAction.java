package com.isa.thinair.xbe.core.web.action.system;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.system.ShowPasswordChangeRH;
import com.isa.thinair.xbe.core.web.v2.util.I18nUtil;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SHOW_PASSWORD_CHANGE, value = S2Constants.Jsp.System.PASSWORD_CHANGE)
public class ShowPasswordChangeAction extends BaseRequestAwareAction {
	
	final String CHANGE_PASSWORD_PAGE_ID = "cngepwd";

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		request.setAttribute("languageData", I18nUtil.getXbeMesseges(userLanguage, new String[] { CHANGE_PASSWORD_PAGE_ID }));
		return ShowPasswordChangeRH.execute(request);
	}

}
