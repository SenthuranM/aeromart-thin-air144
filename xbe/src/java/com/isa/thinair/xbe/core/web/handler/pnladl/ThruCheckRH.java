package com.isa.thinair.xbe.core.web.handler.pnladl;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.pnladl.ThruCheckHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Thushara
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style -
 *         Code Templates
 */

public final class ThruCheckRH extends BasicRH {

	private static Log log = LogFactory.getLog(ThruCheckRH.class);

	/**
	 * @param request
	 * @throws ModuleException
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		String strMode = request.getParameter("hdnMode");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (strMode != null && (strMode.equals("SAVE") || strMode.equals("DELETE"))) {
				saveData(request);
				if (strMode.equals("SAVE")) {
					saveMessage(request, XBEConfig.getServerMessage("thrucheck.record.save.success", userLanguage, null), WebConstants.MSG_SUCCESS);
				} else {
					saveMessage(request, XBEConfig.getServerMessage("thrucheck.record.cancel.success", userLanguage, null), WebConstants.MSG_SUCCESS);
				}

			}

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in ThruCheckRH.execute " + "- setDisplayData(request) [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error("Exception in ThruCheckRH.execute " + "- setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}

		try {
			setDisplayData(request);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in ThruCheckRH.execute " + "- setDisplayData(request) [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error("Exception in ThruCheckRH.execute " + "- setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	private static void saveData(HttpServletRequest request) throws ModuleException {

		String strAirport = request.getParameter("selAirport");
		String strFlightNo = request.getParameter("txtFlightNo");
		String strADLTime = request.getParameter("txtADLTime");
		String strPNLTime = request.getParameter("txtPNLTime");
		String strStartDate = request.getParameter("txtFromDate");
		String strStopDate = request.getParameter("txtToDate");
		String strstatus = request.getParameter("chkStatus");
		String strAllFlights = request.getParameter("chkAllFlts");
		String strVersion = request.getParameter("hdnVersion");
		SimpleDateFormat smtd = new SimpleDateFormat("dd/MM/yyyy");
		PnlAdlTiming pnladlTiming = new PnlAdlTiming();
		String strMode = request.getParameter("hdnMode");
		String strThruID = request.getParameter("hdnThruID");
		String strLastAdl = request.getParameter("txtLstADL");
		String strADLTimeAfterCutOffTime = request.getParameter("txtADLTimeAfterCutoffTime");

		if (strAirport != null && !strAirport.equals("")) {
			pnladlTiming.setAirportCode(strAirport.trim());
		}
		if (strFlightNo != null && !strFlightNo.equals("")) {
			pnladlTiming.setFlightNumber(strFlightNo.trim());
		}
		if (strPNLTime != null && !strPNLTime.equals("")) {

			pnladlTiming.setPnlDepGap(parseTimeGapString(strPNLTime.trim()));
		}
		if (strADLTime != null && !strADLTime.equals("")) {
			// converted DD:HH:MM to mins and save
			pnladlTiming.setAdlRepIntv(parseTimeGapString(strADLTime.trim()));
		}
		if (strADLTimeAfterCutOffTime != null && !strADLTimeAfterCutOffTime.equals("")) {
			pnladlTiming.setAdlRepIntvAfterCutoff(parseTimeGapString(strADLTimeAfterCutOffTime.trim()));
		}
		try {
			if (strStartDate != null && !strStartDate.equals("")) {
				pnladlTiming.setStartingZuluDate(smtd.parse(strStartDate.trim()));
			}
			if (strStopDate != null && !strStopDate.equals("")) {
				pnladlTiming.setEndingZuluDate(smtd.parse(strStopDate.trim()));
			}
		} catch (ParseException pse) {
			System.out.println("parse exception" + pse);
		}
		if (strstatus != null && strstatus.equals("on")) {
			pnladlTiming.setStatus(PNLConstants.PnlAdlTimings.ACTIVE);
		} else {
			pnladlTiming.setStatus(PNLConstants.PnlAdlTimings.INACTIVE);
		}
		if (strThruID != null && !strThruID.trim().equals("")) {
			pnladlTiming.setId(new Integer(strThruID));
		}
		if (strAllFlights != null && strAllFlights.equals("on")) {
			pnladlTiming.setApplyToAllFlights(PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES);
		} else {
			pnladlTiming.setApplyToAllFlights(PNLConstants.PnlAdlTimings.APPLY_TO_ALL_NO);
		}
		if (strVersion != null && !strVersion.equals("")) {
			pnladlTiming.setVersion(Long.parseLong(strVersion));
		}

		if (strLastAdl != null && !strLastAdl.equals("")) {
			pnladlTiming.setLastAdlGap(parseTimeGapString(strLastAdl));
		}

		if (strMode != null && strMode.equals("DELETE")) {
			pnladlTiming.setStatus(PNLConstants.PnlAdlTimings.INACTIVE);
		}

		ModuleServiceLocator.getReservationAuxilliaryBD().savePnlAdlTiming(pnladlTiming);

	}

	/**
	 * set data to show in the jsp file
	 * 
	 * @param request
	 * @param daf
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
		setOnlineAirportHtml(request);
		setThruCheckRowHtml(request);

	}

	/**
	 * sets Online Airport list
	 * 
	 * @param request
	 */
	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createActiveAirportCodeList();
		request.getSession().setAttribute(WebConstants.SES_HTML_AIRPORT_DATA, strHtml);
	}

	/**
	 * sets ThrucheckGrid
	 * 
	 * @param request
	 */
	private static void setThruCheckRowHtml(HttpServletRequest request) throws ModuleException {

		ThruCheckHTMLGenerator htmlGen = new ThruCheckHTMLGenerator();
		String strHtml = htmlGen.getThruCheckRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_THRUCHK_DATA, strHtml);

	}

	/**
	 * sets the client errors
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = ThruCheckHTMLGenerator.getClientErrors(request);

		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Parses time given in DD:HH:MM format to minutes
	 * 
	 * @param timeGapString
	 * @return
	 * @throws ModuleException
	 */

	private static int parseTimeGapString(String timeGapString) throws ModuleException {

		// Data string format to be parsed "DD:HH:MM"
		String[] dataArray = timeGapString.split(":");

		// We parse only the data in correct format
		if (dataArray != null && dataArray.length == 3) {

			int days = Integer.parseInt(dataArray[0].trim());
			int hours = Integer.parseInt(dataArray[1].trim());
			int mins = Integer.parseInt(dataArray[2].trim());

			return (days * 1440 + hours * 60 + mins);

		} else {

			throw new ModuleException("timegap.parse.failed");
		}
	}

}
