package com.isa.thinair.xbe.core.web.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SHOW_TRACKER, value = S2Constants.Jsp.System.SHOWTRACKER),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowTrackerPrivateAction extends BaseRequestAwareAction {
	
	public String execute() {
		return S2Constants.Result.SHOW_TRACKER;
	}

}
