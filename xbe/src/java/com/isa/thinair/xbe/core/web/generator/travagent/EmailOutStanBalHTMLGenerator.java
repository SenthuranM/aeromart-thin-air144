/**
 * 
 */
package com.isa.thinair.xbe.core.web.generator.travagent;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.util.GeneratedInvoicesDetailsUtil;

/**
 * @author Noshani Perera
 * 
 */
public class EmailOutStanBalHTMLGenerator {

	private static Log log = LogFactory.getLog(EmailOutStanBalHTMLGenerator.class);

	private static String clientErrors;

	/**
	 * Gets the Invoice Transfer Grid Row for the Search critiriea
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containg Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getInvoiceTransRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside InoiceHTMLGenerator.getGenerated invoices search ");
		return GeneratedInvoicesDetailsUtil.getInvoiceDetails(request);

	}

	/**
	 * Gets the total invoices
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public final String getTotalInvoiceTransRowHtml(HttpServletRequest request) throws ModuleException {
		return GeneratedInvoicesDetailsUtil.getTotalInvoiceDetails(request);
	}

	/**
	 * Sets the Client Validations for the Invice Generate Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.invoicetrans.year.null", "yearEmpty");
			moduleErrs.setProperty("um.invoicetrans.month.null", "monthEmpty");
			moduleErrs.setProperty("um.invoicetrans.agentType.null", "agentTypeRqrd");
			moduleErrs.setProperty("um.invoicetrans.agent.null", "agentsRqrd");
			moduleErrs.setProperty("um.invoicetrans.month.greater", "monthGreater");
			moduleErrs.setProperty("um.invoicetrans.fromdate.greater", "fromDateGreater");
			moduleErrs.setProperty("um.agent.viewInvoice.email.blank", "EMailisEmpty");
			moduleErrs.setProperty("um.agent.viewInvoice.email.invalidmailformat", "InvalidEmail");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
