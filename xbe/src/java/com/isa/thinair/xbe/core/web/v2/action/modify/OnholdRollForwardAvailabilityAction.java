package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.DateUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class OnholdRollForwardAvailabilityAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(OnholdRollForwardAvailabilityAction.class);

	private boolean success = true;

	private String messageTxt;

	private String jsonResSegs;

	private String paxWiseAnci;

	private String startDate;

	private String endDate;

	private Integer adultCount;

	private Integer childCount;

	private Integer infantCount;

	private String system;

	private boolean openReturn;

	private List<RollForwardFlightAvailRS> availFlightRS;

	private boolean duplicateNameExist;
	
	private boolean overBook;

	private String pnr;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(jsonResSegs);
			Collection<Integer> fltSegmentIds = new ArrayList<Integer>();
			Set<ReservationPax> resPaxs = null;
			String agentCode = null;
			
			if(request.getUserPrincipal()!=null){
				UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();				
				agentCode = userPrincipal.getAgentCode();
			}
			availFlightRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlightForRollForward(colsegs,
					DateUtil.parseDate(startDate), DateUtil.parseDate(endDate), adultCount, childCount, infantCount,
					SYSTEM.getEnum(system), overBook, agentCode);
			for (RollForwardFlightAvailRS flightAvail : availFlightRS) {
				for (RollForwardFlightDTO rollForwardFlight : flightAvail.getRollForwardFlights()) {
					fltSegmentIds.add(rollForwardFlight.getFlightSegId());
				}
			}
			duplicateNameExist = ModuleServiceLocator.getAirproxySearchAndQuoteBD().isDuplicateNameExist(fltSegmentIds, pnr);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	@JSON(serialize = false)
	public String getJsonResSegs() {
		return jsonResSegs;
	}

	public void setJsonResSegs(String jsonResSegs) {
		this.jsonResSegs = jsonResSegs;
	}

	@JSON(serialize = false)
	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	@JSON(serialize = false)
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@JSON(serialize = false)
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@JSON(serialize = false)
	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	@JSON(serialize = false)
	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	@JSON(serialize = false)
	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	@JSON(serialize = false)
	public boolean isOpenReturn() {
		return openReturn;
	}

	public void setOpenReturn(boolean openReturn) {
		this.openReturn = openReturn;
	}

	@JSON(serialize = false)
	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public List<RollForwardFlightAvailRS> getAvailFlightRS() {
		return availFlightRS;
	}

	public void setAvailFlightRS(List<RollForwardFlightAvailRS> availFlightRS) {
		this.availFlightRS = availFlightRS;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isDuplicateNameExist() {
		return duplicateNameExist;
	}

	public void setDuplicateNameExist(boolean duplicateNameExist) {
		this.duplicateNameExist = duplicateNameExist;
	}

	public boolean isOverBook() {
		return overBook;
	}

	public void setOverBook(boolean overBook) {
		this.overBook = overBook;
	}
}
