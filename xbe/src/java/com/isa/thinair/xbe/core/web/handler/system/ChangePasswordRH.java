package com.isa.thinair.xbe.core.web.handler.system;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class ChangePasswordRH {

	private static Log log = LogFactory.getLog(ChangePasswordRH.class);

	private static final String PARAM_OLD_PWD = "pwdOld";

	private static final String PARAM_NEW_PWD = "pwdNew";

	public static String execute(HttpServletRequest request) {

		String forward = S2Constants.Result.SHOW_MAIN;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String userId = request.getRemoteUser();
			String oldPassword = request.getParameter(PARAM_OLD_PWD);
			
			if (authenticateUser(userId, oldPassword)) {
				String newPassword = request.getParameter(PARAM_NEW_PWD);
				if (AppSysParamsUtil.isEnablePasswordHistory() && AirSecurityUtil.getSecurityBD().isPasswordUsed(userId, newPassword)) {
					forward = S2Constants.Result.SHOW_PASSWORD_CHANGE;
					BasicRH.saveMessage(request, XBEConfig.getServerMessage("passwordchange.used.password", userLanguage, null), BasicRH.MSG_TYPE_ERROR);
				} else {
					changePassword(userId, newPassword);
					request.getSession().invalidate();
					ForceLoginInvoker.close();
					BasicRH.saveMessage(request, XBEConfig.getServerMessage("passwordchange.operation.success", userLanguage, null),
							BasicRH.MSG_TYPE_CONFIRMATION);
					BasicRH.setJS(request, "request['hdnAction']='RELOGIN'");
			   }
			} else {
				forward = S2Constants.Result.SHOW_PASSWORD_CHANGE;
				BasicRH.saveMessage(request, XBEConfig.getServerMessage("passwordchange.invalid.user", userLanguage, null), BasicRH.MSG_TYPE_ERROR);
			}
		} catch (Exception ex) {
			forward = S2Constants.Result.ERROR;
			String msg = BasicRH.getErrorMessage(ex, userLanguage);

			log.error(msg);

			BasicRH.saveMessage(request, msg, BasicRH.MSG_TYPE_ERROR);
		}

		return forward;
	}

	private static void changePassword(String userId, String newPassword) throws ModuleException {
		User user = ModuleServiceLocator.getSecurityBD().getUser(userId);

		user.setPassword(newPassword);
		user.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);

		ModuleServiceLocator.getSecurityBD().saveUser(user);
		if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
			User savedUser = ModuleServiceLocator.getSecurityBD().getUser(user.getUserId());
			ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(savedUser);
		}
	}

	private static boolean authenticateUser(String userId, String password) throws ModuleException {
		User user = ModuleServiceLocator.getSecurityBD().authenticate(userId, password);
		return (user != null);
	}
}
