package com.isa.thinair.xbe.core.web.v2.action.system;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class DashboardBaggageAvailabilityAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(DashboardBaggageAvailabilityAction.class);

	private boolean success = true;
	private String origin;
	private String destination;
	private String departureDate;
	private String searchOption;
	private String classOfService;
	private String logicalCabinClass;
	private String messageTxt;
	private List<LCCBaggageResponseDTO> lccBaggageResponseDTOs = new ArrayList<LCCBaggageResponseDTO>();
	private Map<String, List<LCCBaggageDTO>> baggageDisplayResponse = new TreeMap<String, List<LCCBaggageDTO>>();
	private List<LCCBaggageDTO> baggages = new ArrayList<LCCBaggageDTO>();

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			FlightSearchDTO searchParams = new FlightSearchDTO();
			searchParams.setFromAirport(origin);
			searchParams.setToAirport(destination);
			searchParams.setDepartureDate(departureDate);
			searchParams.setSearchSystem(searchOption);
			searchParams.setClassOfService(classOfService);
			searchParams.setLogicalCabinClass(logicalCabinClass);
			FlightAvailRQ flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
			FlightAvailRS flightAvailRS = new FlightAvailRS();
			flightAvailRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
					getTrackInfo(), true);

			for (OriginDestinationInformationTO ondInfoTO : flightAvailRS.getOriginDestinationInformationList()) {
				for (OriginDestinationOptionTO ondOptionTo : ondInfoTO.getOrignDestinationOptions()) {
					LCCBaggageResponseDTO lccBaggageResponseDTO;
					List<FlightSegmentTO> flightSegmentTOs = ondOptionTo.getFlightSegmentList();
					LCCReservationBaggageSummaryTo lCCReservationBaggageSummaryTo = new LCCReservationBaggageSummaryTo();
					Map<String, String> bookingClasses = new HashMap<String, String>();
					Map<String, String> classesOfServices = new HashMap<String, String>();

					for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
						classesOfServices.put(flightSegmentTo.getFlightRefNumber(), classOfService);
					}
					
					/**
					 * Dummy values is added beacause booking class is skipped for dashboard baggage availability.
					 * Baggege templates configured for specific booking classes will not be shown in dashboard.
					 * 
					 */
					
					bookingClasses.put("DummyVal", "DummyBC");					
					lCCReservationBaggageSummaryTo.setBookingClasses(bookingClasses);
					lCCReservationBaggageSummaryTo.setClassesOfService(classesOfServices);
					String strTxnIdntifier = null;
					boolean modifyAncillary = false;
					boolean isRequote = false;
					List<BundledFareDTO> bundledFareDTOs = null;
					String pnr = null;
					SYSTEM system = SYSTEM.getEnum(searchParams.getSearchSystem());
					List<LCCBaggageRequestDTO> lccBaggageRequestDTOs = composeBaggageRequest(flightSegmentTOs,
							searchParams.getClassOfService(), strTxnIdntifier);
					lccBaggageResponseDTO = ModuleServiceLocator.getAirproxyAncillaryBD()
							.getAvailableBaggages(
									lccBaggageRequestDTOs,
									strTxnIdntifier,
									system,
									"en",
									modifyAncillary,
									BasicRH.hasPrivilege(request,
											PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER), isRequote,
									ApplicationEngine.XBE, lCCReservationBaggageSummaryTo, bundledFareDTOs, pnr,
									TrackInfoUtil.getBasicTrackInfo(request));
					lccBaggageResponseDTOs.add(lccBaggageResponseDTO);
				}
			}
			constructBaggageResponse(lccBaggageResponseDTOs);

			if (baggageDisplayResponse.isEmpty()) {
				throw new Exception("um.dashboard.baggage.not.avail");
			}

		} catch (Exception e) {
			success = false;
			if ("um.dashboard.baggage.not.avail".equals(e.getMessage())) {
				messageTxt = XBEConfig.getClientMessage(e.getMessage(), userLanguage);
			} else {
				messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			}
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;

	}

	private void constructBaggageResponse(List<LCCBaggageResponseDTO> lccBaggageResponseDTOs) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {
			for (LCCBaggageResponseDTO lccBaggageResponseDTO : lccBaggageResponseDTOs) {

				Map<String, List<LCCBaggageDTO>> ondGroupIDBaggagesMap = new HashMap<String, List<LCCBaggageDTO>>();
				Map<String, List<FlightSegmentTO>> ondGroupIDSegmentsKeyMap = new HashMap<String, List<FlightSegmentTO>>();
				String baggageONDGroupId = null;
				for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : lccBaggageResponseDTO.getFlightSegmentBaggages()) {
					baggageONDGroupId = lccFlightSegmentBaggagesDTO.getFlightSegmentTO().getBaggageONDGroupId();

					if (baggageONDGroupId != null && !baggageONDGroupId.isEmpty()) {

						if (!ondGroupIDBaggagesMap.containsKey(baggageONDGroupId)) {
							ondGroupIDBaggagesMap.put(baggageONDGroupId, lccFlightSegmentBaggagesDTO.getBaggages());
							List<FlightSegmentTO> flightSegmentTos = new ArrayList<FlightSegmentTO>();
							flightSegmentTos.add(lccFlightSegmentBaggagesDTO.getFlightSegmentTO());
							ondGroupIDSegmentsKeyMap.put(baggageONDGroupId, flightSegmentTos);
						} else {
							ondGroupIDSegmentsKeyMap.get(baggageONDGroupId).add(lccFlightSegmentBaggagesDTO.getFlightSegmentTO());
						}
					}
				}
				constructDisplayResponse(ondGroupIDBaggagesMap, ondGroupIDSegmentsKeyMap);
			}
		} else {

			for (LCCBaggageResponseDTO lccBaggageResponseDTO : lccBaggageResponseDTOs) {
				for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : lccBaggageResponseDTO.getFlightSegmentBaggages()) {
					FlightSegmentTO flightSegmentTO = lccFlightSegmentBaggagesDTO.getFlightSegmentTO();
					String departureDate = df.format(flightSegmentTO.getDepartureDateTime());
					
					if(lccFlightSegmentBaggagesDTO.getBaggages() != null && !lccFlightSegmentBaggagesDTO.getBaggages().isEmpty()){
						baggageDisplayResponse.put(
								departureDate + " " + flightSegmentTO.getSegmentCode() + " " + flightSegmentTO.getFlightNumber(),
								lccFlightSegmentBaggagesDTO.getBaggages());
					}
					
				}
			}
		}
	}

	private void constructDisplayResponse(Map<String, List<LCCBaggageDTO>> ondGroupIDBaggagesMap,
			Map<String, List<FlightSegmentTO>> ondGroupIDSegmentsKeyMap) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");

		for (String key : ondGroupIDBaggagesMap.keySet()) {
			String segmentCode = "";
			String departureDateString = "";
			String flightNumberString = "";
			int i = 0;
			for (FlightSegmentTO flightSegmentTO : ondGroupIDSegmentsKeyMap.get(key)) {
				if (i == 0) {
					
					departureDateString = format.format(flightSegmentTO.getDepartureDateTime());
					flightNumberString = flightSegmentTO.getFlightNumber();
					segmentCode = flightSegmentTO.getSegmentCode();
				} else {
					flightNumberString += "/" + flightSegmentTO.getFlightNumber();
					segmentCode += "/" + flightSegmentTO.getSegmentCode();
				}
				i++;
			}
			String segementKey = departureDateString + " " + flightNumberString + " " + getMergedOndCode(segmentCode);
			baggageDisplayResponse.put(segementKey, ondGroupIDBaggagesMap.get(key));
		}
	}

	private String getMergedOndCode(String segmentCodes) {

		String[] onds = segmentCodes.split("/");
		Map<String, String> uniqueOnds = new LinkedHashMap<String, String>();
		String mergerdSegmentCode = "";
		for (String s : onds) {
			uniqueOnds.put(s, s);
		}
		Iterator<String> it = uniqueOnds.keySet().iterator();
		mergerdSegmentCode = it.next();
		while (it.hasNext()) {
			mergerdSegmentCode += ("/" + it.next());
		}
		return mergerdSegmentCode;
	}

	private List<LCCBaggageRequestDTO> composeBaggageRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCBaggageRequestDTO> bL = new ArrayList<LCCBaggageRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCBaggageRequestDTO brd = new LCCBaggageRequestDTO();
			brd.setTransactionIdentifier(txnId);
			brd.setCabinClass(fst.getCabinClassCode());
			brd.setFlightSegment(fst);
			bL.add(brd);
		}
		return bL;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public List<LCCBaggageDTO> getBaggages() {
		return baggages;
	}

	public void setBaggages(List<LCCBaggageDTO> baggages) {
		this.baggages = baggages;
	}

	public Map<String, List<LCCBaggageDTO>> getBaggageDisplayResponse() {
		return baggageDisplayResponse;
	}

	public void setBaggageDisplayResponse(Map<String, List<LCCBaggageDTO>> baggageDisplayResponse) {
		this.baggageDisplayResponse = baggageDisplayResponse;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

}
