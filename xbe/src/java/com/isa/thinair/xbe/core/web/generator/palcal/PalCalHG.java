package com.isa.thinair.xbe.core.web.generator.palcal;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class PalCalHG {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static String clientErrors;

	public static void setHtmlComp(HttpServletRequest request) throws ModuleException {

		String strPALMinimum = globalConfig.getBizParam(SystemParamKeys.PAL_DEPATURE_GAP);
		String strCALMinimum = globalConfig.getBizParam(SystemParamKeys.CAL_INTERVAL);
		String min_cal_clausure_time = globalConfig.getBizParam(SystemParamKeys.LAST_CAL);

		String min_adl_time_after_cutoff = globalConfig.getBizParam(SystemParamKeys.CAL_INTERVAL_AFTER_CUTOFF_TIME);

		String maintain_min_adl_time_after_cutoff = globalConfig
				.getBizParam(SystemParamKeys.MAINTAIN_SEPERATE_CAL_TRANSMISSION_REPEAT_INTERVAL_AFTER_CUTOFF_TIME);

		if (strPALMinimum != null) {
			request.setAttribute("min_pnl_time", strPALMinimum);
		}

		if (strCALMinimum != null) {
			request.setAttribute("min_adl_time", strCALMinimum);
		}
		
		if(min_adl_time_after_cutoff != null){
			request.setAttribute("min_adl_time_after_cutoff", min_adl_time_after_cutoff);
		}
		
		if(maintain_min_adl_time_after_cutoff != null && maintain_min_adl_time_after_cutoff.equals("Y")){
			request.setAttribute("maintain_min_adl_time_after_cutoff", Boolean.TRUE);
		}
		
		if(min_cal_clausure_time != null){
			request.setAttribute("min_cal_clausure_time", min_cal_clausure_time);
		}

		request.getSession().setAttribute(WebConstants.SES_HTML_AIRPORT_DATA, SelectListGenerator.createActiveAirportCodeList());

	}

	public static String getClientErrors() throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("cc_palcal_airport_null", "airportnull");
			moduleErrs.setProperty("cc_palcal_caltime_null", "nocaltime");
			moduleErrs.setProperty("cc_palcal_paltime_null", "nopaltime");
			moduleErrs.setProperty("cc_palcal_startdate_null", "startdate");
			moduleErrs.setProperty("cc_palcal_stopdate_null", "enddate");
			moduleErrs.setProperty("cc_palcal_date_invalid", "invaliddate");
			moduleErrs.setProperty("cc_palcal_time_invalid_format", "invalidtimegap");
			moduleErrs.setProperty("cc_palcal_startdate_startdategreat", "startdategreat");
			moduleErrs.setProperty("cc_palcal_stopdate_stopdategreat", "stopdategreat");
			moduleErrs.setProperty("cc_palcal_pal_lesspal", "lessminpal");
			moduleErrs.setProperty("cc_palcal_cal_lessadl", "lessmincal");
			moduleErrs.setProperty("cc_palcal_cal_greatpnl", "greatmaxpal");
			moduleErrs.setProperty("cc_palcal_cal_greatadl", "greatmaxcal");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}
}
