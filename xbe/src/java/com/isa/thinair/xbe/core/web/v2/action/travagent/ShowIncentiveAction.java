package com.isa.thinair.xbe.core.web.v2.action.travagent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airtravelagents.api.dto.AgentIncDTO;
import com.isa.thinair.airtravelagents.api.dto.SaveIncentiveDTO;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowIncentiveAction {

	private static Log log = LogFactory.getLog(ShowIncentiveAction.class);
	private static final int PAGE_LENGTH = 20;
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;
	private String hdnMode;
	private SaveIncentiveDTO incentive;
	private final String DDMMYYYY = "dd/MM/yyyy";
	private String schemeName;
	private String status;
	private String searchFrom;
	private String searchTo;
	private boolean success;
	private String comments;

	public String execute() {

/**
 * 
 */
		if (this.getIncentive() != null) {
			try {
				ModuleServiceLocator.getTravelAgentBD().saveIncentive(this.getIncentive());
				succesMsg = XBEConfig.getClientMessage(WebConstants.KEY_SAVE_SUCCESS, null);
				msgType = WebConstants.MSG_SUCCESS;
				success = true;

			} catch (ModuleException e) {
				success = false;
				succesMsg = XBEConfig.getClientMessage(e.getExceptionCode(), null);
				msgType = WebConstants.MSG_ERROR;
			} catch (Exception e) {
				succesMsg = XBEConfig.getClientMessage(WebConstants.KEY_EXT_UPDATE_FAILED, null);
				msgType = WebConstants.MSG_ERROR;
				success = false;
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteIncentive() {
		String strIncID = this.getIncentive().getAgentIncSchemeID();

		if (strIncID != null && !"".equals(strIncID)) {
			Integer incentiveId = new Integer(strIncID);
			try {
				ModuleServiceLocator.getTravelAgentBD().removeIncentive(incentiveId,
						Long.valueOf(this.getIncentive().getVersion()).longValue());
				succesMsg = XBEConfig.getClientMessage(WebConstants.KEY_DELETE_SUCCESS, null);
				msgType = WebConstants.MSG_SUCCESS;
				success = true;
			} catch (ModuleException me) {
				succesMsg = XBEConfig.getClientMessage(me.getExceptionCode(), null);
				msgType = WebConstants.MSG_ERROR;
				success = false;
			} catch (Exception e) {
				succesMsg = XBEConfig.getClientMessage(WebConstants.KEY_EXT_UPDATE_FAILED, null);
				msgType = WebConstants.MSG_ERROR;
				success = false;
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchIncentives() {
		if (this.page <= 0) {
			this.page = 1;
		}
		int startIndex = (page - 1) * PAGE_LENGTH;

		Page pageObj = null;
		try {
			AgentIncDTO scrhIncenDTO = new AgentIncDTO();
			scrhIncenDTO.setSchemeName(this.schemeName);
			scrhIncenDTO.setStatus(this.status);
			scrhIncenDTO.setStrEffectiveFrom(this.searchFrom);
			scrhIncenDTO.setStrEffectiveTo(this.searchTo);
			pageObj = ModuleServiceLocator.getTravelAgentBD().getAgentIncentive(scrhIncenDTO, startIndex, PAGE_LENGTH);

			this.records = pageObj.getTotalNoOfRecords();
			this.total = pageObj.getTotalNoOfRecords() / 20;
			int mod = pageObj.getTotalNoOfRecords() % 20;
			if (mod > 0)
				this.total = this.total + 1;
			if (this.page <= 0)
				this.page = 1;
			rows = (Collection<Map<String, Object>>) DataUtil.createGridData(pageObj.getStartPosition(), "incentive",
					pageObj.getPageData(), this.decorateIncentiveRow());

		} catch (ModuleException me) {
			succesMsg = XBEConfig.getServerMessage(me.getExceptionCode(), null, null);
			msgType = WebConstants.MSG_ERROR;
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	/* ********************************************* */
	/* 												 */
	/* ROW DECORATERS */
	/* 												 */
	/* ********************************************* */
	private RowDecorator<AgentIncentive> decorateIncentiveRow() {
		RowDecorator<AgentIncentive> incentiveDecorator = new RowDecorator<AgentIncentive>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, AgentIncentive record) {
				if (record.getEffectiveFrom() != null && record.getEffectiveTo() != null) {
					row.put("incentive.period", convertDateToString(new Date(record.getEffectiveFrom().getTime())) + "- "
							+ convertDateToString(new Date(record.getEffectiveTo().getTime())));
					row.put("incentive.schemePrdFrom", convertDateToString(new Date(record.getEffectiveFrom().getTime())));
					row.put("incentive.schemePrdTo", convertDateToString(new Date(record.getEffectiveTo().getTime())));
				}
				if (record.getCreatedDate() != null) {
					row.put("incentive.date", record.getCreatedDate());
				}
				if (record.getAgentIncentiveSlabs() != null) {
					Collection slabs = record.getAgentIncentiveSlabs();
					Iterator iter = slabs.iterator();
					Integer slabMinVal = new Integer(0);
					Integer slabMaxVal = new Integer(0);
					int cnt = 0;
					while (iter.hasNext()) {
						AgentIncentiveSlab slab = (AgentIncentiveSlab) iter.next();
						Integer tmpMin = slab.getRateStartValue();
						Integer tmpMax = slab.getRateEndValue();
						if (slabMinVal == 0) {
							slabMinVal = tmpMin;
						} else if (tmpMin < slabMinVal) {
							slabMinVal = tmpMin;
						}
						if (slabMaxVal == 0) {
							slabMaxVal = tmpMax;
						} else if (tmpMax > slabMaxVal) {
							slabMaxVal = tmpMax;
						}

						cnt++;
					}
					row.put("incentive.range", slabMinVal + "- " + slabMaxVal);
				}
				row.put("incentive.version", record.getVersion());

			}
		};
		return incentiveDecorator;
	}

	public String convertDateToString(Date date) {
		DateFormat formatter = new SimpleDateFormat(DDMMYYYY);
		String strDate = formatter.format(date);
		return strDate;

	}

	/* ********************************************* */
	/* 												 */
	/* Getters and Setters */
	/* 												 */
	/* ********************************************* */

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public SaveIncentiveDTO getIncentive() {
		return incentive;
	}

	public void setIncentive(SaveIncentiveDTO incentive) {
		this.incentive = incentive;
	}

	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(String searchFrom) {
		this.searchFrom = searchFrom;
	}

	public String getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(String searchTo) {
		this.searchTo = searchTo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
