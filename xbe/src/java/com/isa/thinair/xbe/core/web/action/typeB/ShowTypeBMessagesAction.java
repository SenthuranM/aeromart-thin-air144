package com.isa.thinair.xbe.core.web.action.typeB;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageDTO;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageSearchDTO;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowTypeBMessagesAction extends BaseRequestAwareAction {

	public static Log log = LogFactory.getLog(ShowTypeBMessagesAction.class);
	public String PARAM_RECORD_LOCATOR = "recordLocator";

	public Collection<TypeBMessageDTO> typeBMessages;

	public String execute() {
		String strForward = S2Constants.Result.SUCCESS;
		String recordLocator = request.getParameter(PARAM_RECORD_LOCATOR);

		List<String> messageTypes = new ArrayList<String>();
		messageTypes.add(MessageComposerConstants.Message_Identifier.RES.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.BKG.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.AMD.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.CAN.toString());

		TypeBMessageSearchDTO messageSearchDTO = new TypeBMessageSearchDTO();
		messageSearchDTO.setPnr(recordLocator);
		try {
			typeBMessages = ModuleServiceLocator.getMessageBrokerServiceBD().searchTypeBMessageByPnr(messageSearchDTO,
					messageTypes);
		} catch (ModuleException e) {
			log.error("ShowTypeBMessagesAction execute method failed : " + e.getMessage());
		}

		return strForward;
	}

	public Collection<TypeBMessageDTO> getTypeBMessages() {
		return typeBMessages;
	}

	public void setTypeBMessages(Collection<TypeBMessageDTO> typeBMessages) {
		this.typeBMessages = typeBMessages;
	}
}
