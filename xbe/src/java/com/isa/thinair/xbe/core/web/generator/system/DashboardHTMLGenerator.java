package com.isa.thinair.xbe.core.web.generator.system;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Generates the Dashboard HTML(s)
 * 
 * @author Nilindra Fernando
 * @Modified by Baladewa
 */
public class DashboardHTMLGenerator {

	private static Log log = LogFactory.getLog(DashboardHTMLGenerator.class);

	/*
	 * private static String createMessageContectJosn(Map<Integer,
	 * Collection<DashboardMessageTO>> mapDashboardMessages) { String
	 * EMPTY_ARRAY = "[]"; Collection<DashboardMessageTO> dashboardMessageList =
	 * new ArrayList<DashboardMessageTO>(); try { for
	 * (Collection<DashboardMessageTO> colDashboardMessageTO :
	 * mapDashboardMessages.values()) { if (colDashboardMessageTO != null &&
	 * colDashboardMessageTO.size() > 0) { for (DashboardMessageTO
	 * dashboardMessageTO : colDashboardMessageTO) {
	 * dashboardMessageList.add(dashboardMessageTO); } } } return
	 * JSONUtil.serialize(dashboardMessageList); } catch (JSONException e) {
	 * return EMPTY_ARRAY; } }
	 */

	public static void setDashboardContent(HttpServletRequest request) {
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String userId = userPrincipal.getUserId();

		// log.debug("Dashboard content User Id :" + userId);
		// DashbrdUserDataRetrivalTO dataRetrivalTO = new
		// DashbrdUserDataRetrivalTO();
		// dataRetrivalTO.setUserID(userId);
		// dataRetrivalTO.setAgentCode(userPrincipal.getAgentCode());
		// dataRetrivalTO.setAgentType(userPrincipal.getAgentTypeCode());
		// dataRetrivalTO.setPosCode(userPrincipal.getAgentStation());

		// Map<Integer, Collection<DashboardMessageTO>> mapDashboardMessages =
		// LookupUtils.getWebServiceDAO().getDashboardContent(
		// new Date(), dataRetrivalTO);

		// if (mapDashboardMessages == null || mapDashboardMessages.size() == 0)
		// {
		// request.setAttribute(WebConstants.DASHBOARD_CONTENT, "");
		// request.getSession().setAttribute(WebConstants.DASHBOARD_ENABLED,
		// "N");
		// } else {
		// StringBuilder strbData = new StringBuilder();
		// strbData.append("<div id='sideBar'>");
		// strbData.append("<div id='sideBarContents' style='display:none;'>");
		// strbData.append("<div id='sideBarMsg'>");
		// strbData.append("<h2>AccelAero Dashboard Messages</h2>");

		// Collection<DashboardMessageTO> colDashboardMessageTO;

		// for (Integer priorityId : mapDashboardMessages.keySet()) {
		// colDashboardMessageTO = mapDashboardMessages.get(priorityId);
		// strbData.append(createMessageTypeContent(colDashboardMessageTO));
		// }
		// strbData.append("</div>");
		// strbData.append("</div>");
		// strbData.append("<a href='#' id='sideBarTab'><img src='"
		// +
		// StaticFileNameUtil.getCorrected("../images/dashboard/slide-button-left.gif")
		// + "' "
		// + "alt='Dashboard' title='Dashboard' /></a>");
		// strbData.append("</div>");

		// request.setAttribute(WebConstants.DASHBOARD_CONTENT,
		// createMessageContectJosn(mapDashboardMessages));
		// request.getSession().setAttribute(WebConstants.DASHBOARD_ENABLED,
		// "Y");
		// }

		request.setAttribute(WebConstants.DASHBOARD_TOOLS, BasicRH.hasPrivilege(request,
				PrivilegesKeys.XBESystemPrivilageKeys.PRIVI_SYSTEM_DBTOOLS_DSIPLAY));
		request.setAttribute(WebConstants.DASHBOARD_ANCI, BasicRH.hasPrivilege(request,
				PrivilegesKeys.XBESystemPrivilageKeys.PRIVI_SYSTEM_DBTOOLS_ANCI));
		request.setAttribute(WebConstants.DEMO_DISPLAY, BasicRH.hasPrivilege(request,
				PrivilegesKeys.XBESystemPrivilageKeys.PRIVI_SYSTEM_DEMO_DSIPLAY));
		request.setAttribute(WebConstants.DASHBOARD_MSG_SYNC, AppSysParamsUtil.isDBMessageSynchronise());
		request.setAttribute(WebConstants.DASHBOARD_MSG_SYNC_TIME, AppSysParamsUtil.getDBMessageSyncTime());
		User user;
		try {
			user = ModuleServiceLocator.getSecurityBD().getUser(userId);
			if (user.getDashboardPreference() != null) {
				request.setAttribute(WebConstants.REQ_DASHBOARD_PREF_JSON, user.getDashboardPreference());
			} else {
				request.setAttribute(WebConstants.REQ_DASHBOARD_PREF_JSON, "{}");
			}
		} catch (ModuleException e) {
			request.setAttribute(WebConstants.REQ_DASHBOARD_PREF_JSON, "{}");
			log.error("Error occurred while loading user dash board pref for userId:" + userId, e);
		}
	}
}
