/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.xbe.core.web.handler.travagent;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.MessageCodes;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.generator.travagent.InvoiceHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class InvoiceRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(InvoiceRequestHandler.class);

	public static String execute(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		boolean isByTransaction = AppSysParamsUtil.isTransactInvoiceEnable();
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		if (userPrincipal.getAgentTypeCode().equals(WebConstants.AGENT_TYPE_TRAVEL_AGENT)) {
			log.debug("User does not have sufficient privilages");
			throw new ModuleException(MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN);
		}

		String forward = S2Constants.Result.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");
		String agentName = request.getParameter("hdnAgentName");
		String fileName = agentName;
		String fileNameSummary = agentName + "_Summary";

		try {

			if (strHdnMode != null) {
				if (strHdnMode.equals("EMAIL1")) {
					sendEmail(request, request.getParameter("agentEmail"), fileName, fileNameSummary, response);
					setInvoiceSummaryRowHTML(request);
					return null;
				} else if (strHdnMode.equals("EMAIL2")) {
					sendEmail(request, request.getParameter("txtEmail"), fileName, fileNameSummary, response);
					setInvoiceSummaryRowHTML(request);
					return null;
				}

				if (strHdnMode.equalsIgnoreCase(WebConstants.ACTION_SEARCH)) {
					if (isByTransaction) {
						setInvoiceSummaryGridHTML(request);
					} else {
						setInvoiceSummaryRowHTML(request);
					}
					log.error("InvoiceRequestHandler setInvoiceSummaryRowHTML SUCCESS");
				}
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("InvoiceRequestHandler setInvoiceSummaryRowHTML FAILED: " + e.getMessageString());
		}

		try {

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				getReportView(request, response);
				log.error("InvoiceRequestHandler setReportView Success");
				return null;
			}
		} catch (Exception e) {
			log.error("InvoiceRequestHandler SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			setDisplayData(request, userPrincipal.getAgentTypeCode());
			log.debug("InvoiceRequestHandler SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("InvoiceRequestHandler SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request, String agentTypeCode) throws Exception {

		boolean isAllowedToViewAnyInv = isUserTypeGSAHasAccess(request, WebConstants.PRIV_TA_INVOICE_VIEW_ANY, agentTypeCode);
		setClientErrors(request);
		setAgentGSAList(request, isAllowedToViewAnyInv);
		setInvoiceYear(request);
		setCurrentMonth(request);
		setInvoicePeriod(request);
		setAppParamSales(request);
		setInvoiceEntities(request);
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = InvoiceHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static void setInvoiceSummaryRowHTML(HttpServletRequest request) throws ModuleException {
		InvoiceHTMLGenerator htmlGen = new InvoiceHTMLGenerator();
		String strHtml = htmlGen.getInvoiceSummaryRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
	}

	private static void setInvoiceSummaryGridHTML(HttpServletRequest request) throws ModuleException {
		InvoiceHTMLGenerator htmlGen = new InvoiceHTMLGenerator();
		String strHtml = null;
		String invoiceNo = request.getParameter("hdnInvoice");
		if (invoiceNo != null && !"".equals(invoiceNo)) {
			setInvoiceSummaryRowHTML(request);
		} else {
			strHtml = htmlGen.setInvoiceSummaryGridHTML(request);
		}
		request.setAttribute(WebConstants.REQ_INOICE_SUMM, strHtml);
	}

	private static void setAgentGSAList(HttpServletRequest request, boolean isAllowedToViewAnyInv) throws ModuleException {
		String strHtml = "";
		if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			List<String[]> gsas = null;
			if (AppSysParamsUtil.getCarrierAgent().equals(userPrincipal.getAgentTypeCode()) && isAllowedToViewAnyInv) {
				gsas = (List<String[]>) SelectListGenerator.createAgentsOfReporting(null, true, null, false, false,
						userPrincipal.getAgentTypeCode());
			} else if (isAllowedToViewAnyInv) {
				gsas = (List<String[]>) SelectListGenerator.createAgentForLoggedUserLevelOfRights(userPrincipal.getAgentCode(),
						2);
			} else if (hasPrivilege(request, WebConstants.PRIV_TA_INVOICE_VIEW_GSA)) {
				gsas = (List<String[]>) SelectListGenerator.createAgentForLoggedUserLevelOfRights(userPrincipal.getAgentCode(),
						3);
			}

			String[] gsaAgents;

			StringBuilder gsb = new StringBuilder();

			if (gsas != null && !gsas.isEmpty()) {
				Iterator<String[]> iter = gsas.iterator();
				while (iter.hasNext()) {
					gsaAgents = (String[]) iter.next();
					if (gsaAgents != null) {
						gsb.append("<option value='" + gsaAgents[2] + "'>" + gsaAgents[3] + "</option>");
					}
				}
			}
			strHtml = gsb.toString();

		} else {
			if (isAllowedToViewAnyInv) {
				strHtml = SelectListGenerator.createOwnAirlineAgentGSAListWOIntegrationAgency();
			} else if (hasPrivilege(request, WebConstants.PRIV_TA_INVOICE_VIEW_GSA)) {
				User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
				String agentCode = user.getAgentCode();
				String[] agents = new String[1];
				agents[0] = agentCode;
				String[] gsaAgents;
				List<String[]> gsas = null;
				StringBuilder gsb = new StringBuilder();

				gsas = (List<String[]>) SelectListGenerator.createTasFromGSA(agents);
				if (gsas != null && !gsas.isEmpty()) {
					Iterator<String[]> iter = gsas.iterator();
					while (iter.hasNext()) {
						gsaAgents = (String[]) iter.next();
						if (gsaAgents != null) {
							gsb.append("<option value='" + gsaAgents[2] + "'>" + gsaAgents[3] + "</option>");
						}
					}
				}
				strHtml = gsb.toString();
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strHtml);
	}

	/**
	 * Method get current year and previous year.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setInvoiceYear(HttpServletRequest request) throws ModuleException {
		String currentYear = null;
		String previousYear = null;
		String selectYear = null;

		StringBuffer sb = new StringBuffer();

		Calendar year = new GregorianCalendar();
		currentYear = Integer.toString(year.get(Calendar.YEAR));
		previousYear = Integer.toString(year.get(Calendar.YEAR) - 1);
		String s[] = new String[4];
		s[0] = currentYear;
		s[1] = currentYear;
		s[2] = previousYear;
		s[3] = previousYear;

		sb.append("<option value='" + s[0] + "' selected>" + s[1] + "</option>");
		sb.append("<option value='" + s[2] + "'>" + s[3] + "</option>");

		selectYear = sb.toString();

		request.setAttribute(WebConstants.REQ_CURRENT_YEAR, currentYear);
		request.setAttribute(WebConstants.REQ_INVOICEYEAR, selectYear);
	}

	/**
	 * Method to get invoice period details.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setInvoicePeriod(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		sb.append("<option value='1' selected>1</option>");
		sb.append("<option value='2'>2</option>");

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			sb.append("<option value='3'>3</option>");
			sb.append("<option value='4'>4</option>");
			sb.append("<option value='5'>5</option>");
		}

		request.setAttribute(WebConstants.REQ_INVOICEPERIOD, sb.toString());
	}

	/**
	 * Method to get the current month.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setCurrentMonth(HttpServletRequest request) throws ModuleException {
		String currentMonth = null;

		Calendar cal = new GregorianCalendar();
		currentMonth = Integer.toString(cal.get(Calendar.MONTH));

		request.setAttribute(WebConstants.REQ_CURRENT_MONTH, currentMonth);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	private static void getReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		setReportView(request, response);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String reportTemplateDetails = null;
		String year = request.getParameter("selYear");
		String month = request.getParameter("selMonth");
		String bilingPeriod = request.getParameter("selBP");
		String value = request.getParameter("radReportOption");
		String agent = request.getParameter("hdnAgents");
		String agentName = request.getParameter("hdnAgentName");
		String invoiceNo = request.getParameter("hdnInvoice");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strSource = request.getParameter("selPaySource");
		String strBase = request.getParameter("chkBase");
		String strAgentCurr = request.getParameter("hdnAgentCurr");
		boolean blnBase = (strBase != null && strBase.equals("on")) ? true : false;
		String strView = request.getParameter("chkNewview");
		String baseCurrencyCode = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY);
		String entity = request.getParameter("selEntity");
		String entitytext = request.getParameter("hdnEntityText");
		
		boolean isNewView = (strView != null && strView.equalsIgnoreCase("on")) ? true : false;

		String id = "UC_REPM_008";
		reportTemplateDetails = "InvoiceDetailReportTemplat.jasper";

		String showPaymentBreakDown = "N";
		String showPaymentCurrency = "N";
		String showPaymentCurrWithBreakDown = "N";
		String showPetrofacReference = "N";
		String showExternalAmout = "N";

		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		Agent logonAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
		String prefReportFormat = logonAgent.getPrefferedRptFormat();

		String strCarrier = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.CARRIER_NAME)
				+ " Reservation System";
		String agnBaseCurrency = baseCurrencyCode;
		String period = "";
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			// AIRLINE_37
			String paymentBreakDown = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
			// AIRLINE_33
			String paymentInAgCur = ModuleServiceLocator.getGlobalConfig().getBizParam(
					SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY);
			// AIRLINE_34
			String invoiceInAgCur = ModuleServiceLocator.getGlobalConfig()
					.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY);
			boolean isShowPetrofac = ModuleServiceLocator.getGlobalConfig().getPetroFacAgentsMap().containsKey(agent);

			if (paymentBreakDown.equalsIgnoreCase("Y")) {
				showPaymentBreakDown = "Y";
			}
			if (blnBase == false && (paymentInAgCur.equalsIgnoreCase("Y") && invoiceInAgCur.equalsIgnoreCase("Y"))) {
				showPaymentCurrency = "Y";
			}
			if (showPaymentCurrency.equalsIgnoreCase("Y") && showPaymentBreakDown.equalsIgnoreCase("Y")) {
				showPaymentCurrWithBreakDown = "Y";
				showPaymentBreakDown = "N";
			}
			if (isShowPetrofac) {
				showPetrofacReference = "Y";
			}
			showExternalAmout = "Y";

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));
			search.setAgentCode(agent);
			search.setYear(year);
			search.setMonth(month);
			search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_DETAILS);
			search.setReportViewNew(isNewView);
			parameters.put("AGENT_CODE", agent);
			parameters.put("AGENT_NAME", agentName);
			parameters.put("INVOICE_NO", invoiceNo);
			parameters.put("ID", id);
			parameters.put("AMT_1", agnBaseCurrency);
			parameters.put("AMT_2", strAgentCurr);

			// FIXME Check this Source Value on DEBuG
			if (strSource == null) {
				strSource = "INTERNAL";
			}
			parameters.put("SOURCE", strSource);

			parameters.put("LOCALE", prefReportFormat);
			if (blnBase) {
				parameters.put("CURRENCY", agnBaseCurrency);
			} else
				parameters.put("CURRENCY", strAgentCurr);

			String sameBaseCur = "N";
			if (strAgentCurr != null && agnBaseCurrency != null) {
				if (strAgentCurr.equalsIgnoreCase(agnBaseCurrency)) {
					sameBaseCur = "Y";
				}
			}
			parameters.put("SAME_PAYMENT_CUR", sameBaseCur);

			search.setPaymentSource(strSource);
			if (bilingPeriod != null && !bilingPeriod.equals("")) {
				search.setBillingPeriod(Integer.parseInt(bilingPeriod));
				period += bilingPeriod;
			}

			// Show 'Holiday' External Payment Data
			search.setShowExternal("Y");
			
			search.setEntity(entity);
			parameters.put("INVOICE_PERIOD", period);
			parameters.put("OPTION", value);

			parameters.put("SHOW_BREAKDOWN", showPaymentBreakDown);
			parameters.put("SHOW_PAY_CUR", showPaymentCurrency);
			parameters.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
			parameters.put("SHOW_PAYREF", showPetrofacReference);
			parameters.put("SHOW_EXTERNAL", showExternalAmout);
			parameters.put("BASE_CURRENCY", baseCurrencyCode);
			parameters.put("ENTITY", entitytext);
			
			resultSet = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryDataByCurrency(search);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + strLogo;

			if (value.trim().equals("HTML")) {
				reportsRootDir = "../images/" + StaticFileNameUtil.getCorrected(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * 
	 * @param request
	 * @param emailAddress
	 */
	private static void sendEmail(HttpServletRequest request, String emailAddress, String fileName, String summaryFileName,
			HttpServletResponse response) {
		UserMessaging usermsg = null;
		List<UserMessaging> messageList = null;
		MessageProfile msgProfile = null;
		HashMap<String, Object> map = null;
		map = new HashMap<String, Object>();
		Topic topic = null;

		Date currentDate = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String formattedDate = dateFormat.format(currentDate);
		String extFileName = fileName + "_Holidays_" + formattedDate + ".pdf";

		fileName = fileName + "_" + formattedDate + ".pdf";
		summaryFileName = summaryFileName + "_" + formattedDate + ".pdf";

		String path = PlatformConstants.getAbsAttachmentsPath() + "/" + fileName;
		String summaryPath = PlatformConstants.getAbsAttachmentsPath() + "/" + summaryFileName;
		String extPath = PlatformConstants.getAbsAttachmentsPath() + "/" + extFileName;
		String agentName = request.getParameter("hdnAgentName");

		if (!"".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.INVOICE_EMAIL_REPLY_ADDRESS))) {
			String invoiceEmails = getInvoiceEmailString(CommonsServices.getGlobalConfig().getBizParam(
					SystemParamKeys.INVOICE_EMAIL_REPLY_ADDRESS));

			map.put("notice", " ----------------------------------------------- <br> "
					+ " This E-mail is system generated. <br> " + " Please do not reply to the sender's address <br> "
					+ " For any queries please reply to : <br>" + invoiceEmails + "<br> "
					+ " -----------------------------------------------<br>");
		}

		usermsg = new UserMessaging();
		messageList = new ArrayList<UserMessaging>();
		msgProfile = new MessageProfile();

		topic = new Topic();
		usermsg.setToAddres(emailAddress);

		usermsg.setFirstName(agentName);
		usermsg.setLastName("");
		messageList.add(usermsg);
		msgProfile.setUserMessagings(messageList);
		map.put("user", usermsg);

		try {
			request.setAttribute("fileNameDetail", fileName);
			request.setAttribute("fileNameSummary", summaryFileName);
			if (AppSysParamsUtil.isTransactInvoiceEnable()) {
				setTransactionReportAttachment(request, response, extFileName);
			} else {
				setReportAttachment(request, response, extFileName);
			}
			File attachment = new File(path);
			if (attachment.exists()) {
				topic.addAttachmentFileName(fileName);
			}

			File attachmentSummary = new File(summaryPath);
			if (attachmentSummary.exists()) {
				topic.addAttachmentFileName(summaryFileName);
			}

			// external
			File attachmentExt = new File(extPath);
			if (attachmentExt.exists()) {
				topic.addAttachmentFileName(extFileName);
			}

			topic.setTopicParams(map);
			topic.setTopicName("invoice");
			msgProfile.setTopic(topic);
			log.debug("sending invoice summary to " + emailAddress);
			ModuleServiceLocator.getMessagingBD().sendMessage(msgProfile);
		} catch (ModuleException e) {
			log.error("Error in generating and sending email" + e.getMessage());
		}

	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportAttachment(HttpServletRequest request, HttpServletResponse response, String extFileName)
			throws ModuleException {

		Date firstday = null;
		Date lastday = null;
		ResultSet resultSet = null;
		ResultSet resultSetSummary = null;
		ResultSet resultSetForExrenal = null;
		String year = request.getParameter("selYear");
		String month = request.getParameter("selMonth");
		String bilingPeriod = request.getParameter("selBP");
		String value = request.getParameter("radReportOption");
		String agent = request.getParameter("hdnAgents");
		String agentName = request.getParameter("hdnAgentName");
		String addStatement = AppSysParamsUtil.getAdditionalSentenceOnInvoice();
		String invoiceNo = request.getParameter("hdnInvoice");
		String strView = request.getParameter("chkNewview");
		boolean isNewView = (strView != null && strView.equalsIgnoreCase("on")) ? true : false;

		// String reportTemplateDetails = "InvoiceDetailViewReport.jasper";
		String id = "UC_REPM_008";
		String reportTemplateDetails = "InvoiceDetailReportTemplat.jasper";
		String summaryAttachment = "InvoiceSummaryAttachmentByCurrency.jasper";
		String strBaseChk = request.getParameter("chkBase");
		boolean blnBase = (strBaseChk != null && strBaseChk.equals("on")) ? true : false;

		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

		String paymentTerms = request.getParameter("paymentTerms");
		String accountNumber = request.getParameter("accountNumber");
		String accountName = request.getParameter("accountName");
		String bankName = request.getParameter("bankName");
		String bankAddress = request.getParameter("bankAddress");
		String swiftCode = request.getParameter("swiftCode");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String bankBeneficiaryName = request.getParameter("bankBeneficiaryName");
		String bankIBANCode = request.getParameter("bankIBANCode");
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strAgentCurr = request.getParameter("hdnAgentCurr");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strEntity = request.getParameter("hdnEntityText");
		String entityCode = request.getParameter("selEntity");

		String showPaymentBreakDown = "N";
		String showPaymentCurrency = "N";
		String showPaymentCurrWithBreakDown = "N";
		String showPetrofacReference = "N";
		String showExternalAmout = "N";

		String period = "";
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			Map<String, Object> parameterSummary = new HashMap<String, Object>();
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			ReportsSearchCriteria searchSummary = new ReportsSearchCriteria();
			Map parametersForInvoiceExternal = new HashMap();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
					searchSummary.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
					searchSummary.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			// AIRLINE_37
			String paymentBreakDown = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
			// AIRLINE_33
			String paymentInAgCur = ModuleServiceLocator.getGlobalConfig().getBizParam(
					SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY);
			// AIRLINE_34
			String invoiceInAgCur = ModuleServiceLocator.getGlobalConfig()
					.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY);
			boolean isShowPetrofac = ModuleServiceLocator.getGlobalConfig().getPetroFacAgentsMap().containsKey(agent);

			if (paymentBreakDown.equalsIgnoreCase("Y")) {
				showPaymentBreakDown = "Y";
			}
			if (!blnBase && (paymentInAgCur.equalsIgnoreCase("Y") && invoiceInAgCur.equalsIgnoreCase("Y"))) {
				showPaymentCurrency = "Y";
			}
			if (showPaymentCurrency.equalsIgnoreCase("Y") && showPaymentBreakDown.equalsIgnoreCase("Y")) {
				showPaymentCurrWithBreakDown = "Y";
				showPaymentBreakDown = "N";
			}
			if (isShowPetrofac) {
				showPetrofacReference = "Y";
			}
			showExternalAmout = "Y";

			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			Agent logonAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
			String prefReportFormat = logonAgent.getPrefferedRptFormat();

			search.setAgentCode(agent);
			search.setYear(year);
			search.setMonth(month);
			searchSummary.setYear(year);
			searchSummary.setMonth(month);
			search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_DETAILS);
			search.setReportViewNew(isNewView);
			search.setEntity(entityCode);
			searchSummary.setReportViewNew(isNewView);
			parameters.put("AGENT_CODE", agent);
			parameters.put("AGENT_NAME", agentName);
			parameters.put("INVOICE_NO", invoiceNo);
			parameters.put("ID", id);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("AMT_2", strAgentCurr);
			parameters.put("ENTITY", strEntity);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (bilingPeriod != null && !bilingPeriod.equals("")) {
				search.setBillingPeriod(Integer.parseInt(bilingPeriod));
				searchSummary.setBillingPeriod(Integer.parseInt(bilingPeriod));
				period = year + "/" + month + " Period ";
				if (bilingPeriod.equals("1")) {
					period += "1";
				} else {
					period += "2";
				}
			}
			GregorianCalendar gc = new GregorianCalendar();

			gc.set(Calendar.YEAR, Integer.parseInt(year));
			gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
			gc.set(Calendar.DATE, 1);
			firstday = gc.getTime();
			gc.add(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
			lastday = gc.getTime();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

			searchSummary.setAgentCode(agent);
			searchSummary.setDateRangeFrom(formatter.format(firstday));
			searchSummary.setDateRangeTo(formatter.format(lastday));
			searchSummary.setEntity(entityCode);

			// TO Display External Holoday Data
			search.setShowExternal("Y");


			// Detail Report
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));
			resultSet = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryDataByCurrency(search);

			// Attachment Summary Report
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport2",
					ReportsHTMLGenerator.getReportTemplate(summaryAttachment));
			resultSetSummary = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryAttachmentByCurrency(searchSummary);

			String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + strLogo;

			String sameBaseCur = "N";
			if (strBase != null && strAgentCurr != null) {
				if (strBase.equalsIgnoreCase(strAgentCurr)) {
					sameBaseCur = "Y";
				}
			}

			parameters.put("SAME_PAYMENT_CUR", sameBaseCur);
			parameters.put("INVOICE_PERIOD", period);
			parameters.put("OPTION", value);
			parameters.put("LOCALE", prefReportFormat);

			parameters.put("SHOW_BREAKDOWN", showPaymentBreakDown);
			parameters.put("SHOW_PAY_CUR", showPaymentCurrency);
			parameters.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
			parameters.put("SHOW_PAYREF", showPetrofacReference);
			parameters.put("SHOW_EXTERNAL", showExternalAmout);
			parameters.put("BASE_CURRENCY", strBase);

			if (value.trim().equals("PDF")) {
				parameters.put("IMG", reportsRootDir);
				String fileName = (String) request.getAttribute("fileNameDetail");

				byte[] byteStream = ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters,
						resultSet);

				FTPServerProperty serverProperty = new FTPServerProperty();
				FTPUtil ftpUtil = new FTPUtil();

				// Code to FTP
				serverProperty.setServerName(globalConfig.getFtpServerName());
				if (globalConfig.getFtpServerUserName() == null) {
					serverProperty.setUserName("");
				} else {
					serverProperty.setUserName(globalConfig.getFtpServerUserName());
				}
				serverProperty.setUserName(globalConfig.getFtpServerUserName());
				if (globalConfig.getFtpServerPassword() == null) {
					serverProperty.setPassword("");
				} else {
					serverProperty.setPassword(globalConfig.getFtpServerPassword());
				}

				ftpUtil.setPassiveMode(globalConfig.isFtpEnablePassiveMode());
				ftpUtil.uploadAttachment(fileName, byteStream, serverProperty);
				// End Code to FTP

				parameters.put("IMG", reportsRootDir);
				String fileNameSummary = (String) request.getAttribute("fileNameSummary");

				parameterSummary.put("SHOW_BREAKDOWN", "Y");
				parameterSummary.put("PAYMENT_TERM", paymentTerms);
				parameterSummary.put("ACCOUNT_NO", accountNumber);
				parameterSummary.put("BANK_NAME", bankName);
				parameterSummary.put("BANK_ADDRESS", bankAddress);
				parameterSummary.put("SWIFT_CODE", swiftCode);
				parameterSummary.put("IBAN_NO", bankIBANCode);
				parameterSummary.put("BENEFICIARY_NAME", bankBeneficiaryName);
				parameterSummary.put("IMG", reportsRootDir);
				parameterSummary.put(WebConstants.REP_CARRIER_NAME, strCarrier);
				parameterSummary.put("AGENT_CODE", agent);
				parameterSummary.put("AMT_1", "(" + strBase + ")");
				parameterSummary.put("AMT_2", strAgentCurr);
				parameterSummary.put("INCLUDE_SENTENCE", addStatement);
				parameterSummary.put("INVOICE_ENTITY", strEntity);

				sameBaseCur = "N";
				if (strBase != null && strAgentCurr != null) {
					if (strBase.equalsIgnoreCase(strAgentCurr)) {
						sameBaseCur = "Y";
					}
				}
				parameterSummary.put("SAME_PAYMENT_CUR", sameBaseCur);

				parameterSummary.put("SHOW_BREAKDOWN", showPaymentBreakDown);
				parameterSummary.put("SHOW_PAY_CUR", showPaymentCurrency);
				parameterSummary.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
				parameterSummary.put("SHOW_PAYREF", showPetrofacReference);
				parameterSummary.put("SHOW_EXTERNAL", showExternalAmout);

				String[] details = globalConfig.getBizParam(SystemParamKeys.INVOICE_ADDITIONAL_DETAILS).split(" \\| ");
				String invDetails = "";
				for (String detail : details) {
					invDetails += detail + "\n";
				}
				parameterSummary.put("ADDITIONAL_DETAILS", invDetails);
				parameterSummary.put("LOCALE", prefReportFormat);

				byte[] byteStreamSummary = ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport2",
						parameterSummary, resultSetSummary);

				// Code to FTP
				ftpUtil.uploadAttachment(fileNameSummary, byteStreamSummary, serverProperty);

				// adding code to external payment
				ReportsSearchCriteria extSummary = new ReportsSearchCriteria();
				extSummary.setAgentCode(agent);
				extSummary.setYear(year);
				extSummary.setMonth(month);
				extSummary.setPaymentSource("EXTERNAL");
				extSummary.setBillingPeriod(Integer.parseInt(bilingPeriod));
				extSummary.setReportType(ReportsSearchCriteria.AGENT_INVOICE_EXT_DETAILS);
				extSummary.setEntity(entityCode);

				if (showPaymentCurrency.equals("y") || showPaymentCurrWithBreakDown.equals("Y")) {
					resultSetForExrenal = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryDataByCurrency(extSummary);
				} else {
					resultSetForExrenal = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryData(extSummary);
				}

				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport3",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));

				if (resultSetForExrenal != null) {
					if (resultSetForExrenal.last()) {
						resultSetForExrenal.beforeFirst();
						parametersForInvoiceExternal.put("AGENT_CODE", agent);
						parametersForInvoiceExternal.put("AGENT_NAME", agentName);
						parametersForInvoiceExternal.put("INVOICE_NO", invoiceNo);
						parametersForInvoiceExternal.put("ID", id);
						parametersForInvoiceExternal.put("CARRIER", strCarrier);
						parametersForInvoiceExternal.put("SHOW_BREAKDOWN",
								ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BREAKDOWN));
						parametersForInvoiceExternal.put("INVOICE_PERIOD", period);
						parametersForInvoiceExternal.put("OPTION", value);
						parametersForInvoiceExternal.put("AMT_1", "(" + strBase + ")");
						parametersForInvoiceExternal.put("AMT_2", strAgentCurr);
						parametersForInvoiceExternal.put("LOCALE", prefReportFormat);

						parametersForInvoiceExternal.put("SHOW_BREAKDOWN", showPaymentBreakDown);
						parametersForInvoiceExternal.put("SHOW_PAY_CUR", showPaymentCurrency);
						parametersForInvoiceExternal.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
						parametersForInvoiceExternal.put("SHOW_PAYREF", showPetrofacReference);
						parametersForInvoiceExternal.put("SHOW_EXTERNAL", showExternalAmout);
						parametersForInvoiceExternal.put("ENTITY", strEntity);

						sameBaseCur = "N";
						if (strBase != null && strAgentCurr != null) {
							if (strBase.equalsIgnoreCase(strAgentCurr)) {
								sameBaseCur = "Y";
							}
						}
						parametersForInvoiceExternal.put("SAME_PAYMENT_CUR", sameBaseCur);
						parametersForInvoiceExternal.put("BASE_CURRENCY", strBase);

						byte[] byteStreamExternal = ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport3",
								parametersForInvoiceExternal, resultSetForExrenal);
						// Code to FTP
						ftpUtil.uploadAttachment(extFileName, byteStreamExternal, serverProperty);
						// End Code to FTP
					}
				}
			}
		} catch (Exception e) {
			log.error("Error in creating invoice summary and detail report" + e.getMessage());
			throw new ModuleException("Error in creating invoice summary and detail report" + e.getMessage());

		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setTransactionReportAttachment(HttpServletRequest request, HttpServletResponse response,
			String extFileName) throws ModuleException {

		ResultSet resultSetSummary = null;
		String value = request.getParameter("radReportOption");
		String agent = request.getParameter("hdnAgents");
		String agentName = request.getParameter("hdnAgentName");
		String invoiceNo = request.getParameter("hdnInvoice");
		String pnr = request.getParameter("hdnPNR");
		String id = "UC_REPM_008";
		String reportTemplateSummary = "InvoiceSummaryReceiptAttachment.jasper";
		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
		String paymentTerms = request.getParameter("paymentTerms");
		String accountNumber = request.getParameter("accountNumber");
		String bankName = request.getParameter("bankName");
		String bankAddress = request.getParameter("bankAddress");
		String swiftCode = request.getParameter("swiftCode");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strAgentCurr = request.getParameter("hdnAgentCurr");
		String agnBaseCurrency = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strEntity = request.getParameter("hdnEntityText");
		String entityCode = request.getParameter("selEntity");

		try {
			Map<String, Object> parameterSummary = new HashMap<String, Object>();
			ReportsSearchCriteria searchSummary = new ReportsSearchCriteria();
			FTPServerProperty serverProperty = new FTPServerProperty();
			FTPUtil ftpUtil = new FTPUtil();

			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			Agent logonAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
			String prefReportFormat = logonAgent.getPrefferedRptFormat();

			NumberFormat numFormat = NumberFormat.getInstance(new Locale(prefReportFormat));
			numFormat.setMinimumFractionDigits(2);

			// Code to FTP
			serverProperty.setServerName(globalConfig.getFtpServerName());
			if (globalConfig.getFtpServerUserName() == null) {
				serverProperty.setUserName("");
			} else {
				serverProperty.setUserName(globalConfig.getFtpServerUserName());
			}
			serverProperty.setUserName(globalConfig.getFtpServerUserName());
			if (globalConfig.getFtpServerPassword() == null) {
				serverProperty.setPassword("");
			} else {
				serverProperty.setPassword(globalConfig.getFtpServerPassword());
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					searchSummary.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					searchSummary.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport2",
					ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));

			searchSummary.setAgentCode(agent);
			searchSummary.setInvoiceNumber(invoiceNo);
			searchSummary.setEntity(entityCode);
			resultSetSummary = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryAttachmentByTransaction(searchSummary);

			boolean paymentBreakdown = false;
			if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BREAKDOWN))) {
				paymentBreakdown = true;
			}

			BigDecimal invFare = BigDecimal.ZERO;
			BigDecimal invTax = BigDecimal.ZERO;
			BigDecimal invModify = BigDecimal.ZERO;
			BigDecimal invSurCharge = BigDecimal.ZERO;
			String strBreakdown = "N";
			if (paymentBreakdown) {
				strBreakdown = "Y";
				InvoiceSummaryDTO invoiceSummaryDTO = null;
				invoiceSummaryDTO = ModuleServiceLocator.getInvoicingBD().searchInvoiceForAgent(agent, "", "", 1,
						paymentBreakdown, invoiceNo, pnr.length() == 0 ? null : pnr,null);// no need to send year,month &
																						// billing period

				if (invoiceSummaryDTO != null) {
					invFare = (invoiceSummaryDTO.getFareAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO.getFareAmount();
					invTax = (invoiceSummaryDTO.getTaxAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO.getTaxAmount();
					invModify = (invoiceSummaryDTO.getModifyAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO
							.getModifyAmount();
					invSurCharge = (invoiceSummaryDTO.getSurchrageAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO
							.getSurchrageAmount();
				}
			}

			String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + strLogo;
			String fileNameSummary = (String) request.getAttribute("fileNameSummary");
			if (value.trim().equals("PDF")) {
				parameterSummary.put("IMG", reportsRootDir);
				parameterSummary.put("PAYMENT_TERM", paymentTerms);
				parameterSummary.put("ACCOUNT_NO", accountNumber);
				parameterSummary.put("BANK_NAME", bankName);
				parameterSummary.put("BANK_ADDRESS", bankAddress);
				parameterSummary.put("SWIFT_CODE", swiftCode);
				parameterSummary.put("AMT_1", "(" + agnBaseCurrency + ")");
				parameterSummary.put(WebConstants.REP_CARRIER_NAME, strCarrier);
				parameterSummary.put("AGENT_CODE", agent);
				parameterSummary.put("OPTION", value);
				parameterSummary.put("LOCALE", prefReportFormat);
				parameterSummary.put("ADDITIONAL_DETAILS", globalConfig.getBizParam(SystemParamKeys.INVOICE_ADDITIONAL_DETAILS));
				parameterSummary.put("SHOW_BREAKDOWN", strBreakdown);
				parameterSummary.put("INV_FARE", numFormat.format(invFare).toString());
				parameterSummary.put("INV_TAX", numFormat.format(invTax).toString());
				parameterSummary.put("INV_MOD", numFormat.format(invModify).toString());
				parameterSummary.put("INV_SURCHARGE", numFormat.format(invSurCharge).toString());
				parameterSummary.put("INVOICE_ENTITY", strEntity);

			}
			byte[] byteStreamSummary = ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport2",
					parameterSummary, resultSetSummary);

			ftpUtil.setPassiveMode(globalConfig.isFtpEnablePassiveMode());
			ftpUtil.uploadAttachment(fileNameSummary, byteStreamSummary, serverProperty);

		} catch (Exception e) {
			log.error("Error in creating invoice summary transaction report" + e.getMessage());
			throw new ModuleException("Error in creating invoice summary transaction report" + e.getMessage());

		}

	}

	/**
	 * get invoice reply email Address
	 * 
	 * @param replyEmailAddrs
	 * @return
	 */
	private static String getInvoiceEmailString(String replyEmailAddrs) {

		String[] sArr = replyEmailAddrs.split(",");
		String str = "";
		if (sArr != null && sArr.length > 1) {
			str = sArr[0] + " or " + sArr[1];
		} else {
			str = sArr[0];
		}

		return str;
	}

	private static void setAppParamSales(HttpServletRequest request) {
		boolean blnIsTrans = AppSysParamsUtil.isTransactInvoiceEnable();
		request.setAttribute("IsTransEnable", blnIsTrans);
	}
	
	private static void setInvoiceEntities(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_ENTITIES, SelectListGenerator.createEntityListByName());
	}
}
