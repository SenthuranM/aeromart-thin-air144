package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxValildationTO;
import com.isa.thinair.commons.api.dto.XBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.PassengerUtil;

/**
 * Load contact configuration
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadPaxContactConfigAction extends BaseRequestAwareAction {
	private static final Log log = LogFactory.getLog(LoadPaxContactConfigAction.class);

	private String system;

	private String carriers;

	private String paxType;

	private String alternativeEmailForXBE;

	private Map<String, XBEContactConfigDTO> contactConfig = new HashMap<String, XBEContactConfigDTO>();

	private Map<String, List<String>> validationGroup = new HashMap<String, List<String>>();

	private Map<String, XBEPaxConfigDTO> paxConfigAD = new HashMap<String, XBEPaxConfigDTO>();

	private Map<String, XBEPaxConfigDTO> paxConfigCH = new HashMap<String, XBEPaxConfigDTO>();

	private Map<String, XBEPaxConfigDTO> paxConfigIN = new HashMap<String, XBEPaxConfigDTO>();

	private Map<String, List<String>> uniqGroupAD = new HashMap<String, List<String>>();

	private Map<String, List<String>> uniqGroupCH = new HashMap<String, List<String>>();

	private Map<String, List<String>> uniqGroupIN = new HashMap<String, List<String>>();

	private PaxValildationTO paxValidation;

	private boolean success;

	public String execute() {

		try {
			List<String> carrierList = createCarrierList(carriers);

			PaxContactConfigDTO paxContactDTO = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxContactConfig(system,
					AppIndicatorEnum.APP_XBE.toString(), carrierList, TrackInfoUtil.getBasicTrackInfo(request));

			PassengerUtil.populateContactConfig(paxContactDTO.getContactConfigList(), contactConfig, validationGroup);

			PassengerUtil.populatePaxCatConfig(paxContactDTO.getPaxConfigList(), paxType, paxConfigAD, paxConfigCH, paxConfigIN,
					uniqGroupAD, uniqGroupCH, uniqGroupIN);

			this.paxValidation = paxContactDTO.getPaxValidation();
			this.alternativeEmailForXBE = AppSysParamsUtil.getAlternativeEmailForXBE();

			success = true;
		} catch (ModuleException e) {
			log.error("ERROR in loading pax & contact config", e);
			success = false;
		}
		return S2Constants.Result.SUCCESS;
	}

	private List<String> createCarrierList(String carriers) {
		List<String> cList = new ArrayList<String>();

		String[] cArr = carriers.split(",");

		for (String c : cArr) {
			cList.add(c);
		}

		return cList;
	}

	/**
	 * @return the system
	 */
	@JSON(serialize = false)
	public String getSystem() {
		return system;
	}

	/**
	 * @param system
	 *            the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * @return the paxType
	 */
	@JSON(serialize = false)
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the contactConfig
	 */
	public Map<String, XBEContactConfigDTO> getContactConfig() {
		return contactConfig;
	}

	/**
	 * @param contactConfig
	 *            the contactConfig to set
	 */
	public void setContactConfig(Map<String, XBEContactConfigDTO> contactConfig) {
		this.contactConfig = contactConfig;
	}

	/**
	 * @return the validationGroup
	 */
	public Map<String, List<String>> getValidationGroup() {
		return validationGroup;
	}

	/**
	 * @param validationGroup
	 *            the validationGroup to set
	 */
	public void setValidationGroup(Map<String, List<String>> validationGroup) {
		this.validationGroup = validationGroup;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the carriers
	 */
	@JSON(serialize = false)
	public String getCarriers() {
		return carriers;
	}

	/**
	 * @param carriers
	 *            the carriers to set
	 */
	public void setCarriers(String carriers) {
		this.carriers = carriers;
	}

	/**
	 * @return the paxConfigAD
	 */
	public Map<String, XBEPaxConfigDTO> getPaxConfigAD() {
		return paxConfigAD;
	}

	/**
	 * @param paxConfigAD
	 *            the paxConfigAD to set
	 */
	public void setPaxConfigAD(Map<String, XBEPaxConfigDTO> paxConfigAD) {
		this.paxConfigAD = paxConfigAD;
	}

	/**
	 * @return the paxConfigCH
	 */
	public Map<String, XBEPaxConfigDTO> getPaxConfigCH() {
		return paxConfigCH;
	}

	/**
	 * @param paxConfigCH
	 *            the paxConfigCH to set
	 */
	public void setPaxConfigCH(Map<String, XBEPaxConfigDTO> paxConfigCH) {
		this.paxConfigCH = paxConfigCH;
	}

	/**
	 * @return the paxConfigIN
	 */
	public Map<String, XBEPaxConfigDTO> getPaxConfigIN() {
		return paxConfigIN;
	}

	/**
	 * @param paxConfigIN
	 *            the paxConfigIN to set
	 */
	public void setPaxConfigIN(Map<String, XBEPaxConfigDTO> paxConfigIN) {
		this.paxConfigIN = paxConfigIN;
	}

	/**
	 * @return the uniqGroupAD
	 */
	public Map<String, List<String>> getUniqGroupAD() {
		return uniqGroupAD;
	}

	/**
	 * @param uniqGroupAD
	 *            the uniqGroupAD to set
	 */
	public void setUniqGroupAD(Map<String, List<String>> uniqGroupAD) {
		this.uniqGroupAD = uniqGroupAD;
	}

	/**
	 * @return the uniqGroupCH
	 */
	public Map<String, List<String>> getUniqGroupCH() {
		return uniqGroupCH;
	}

	/**
	 * @param uniqGroupCH
	 *            the uniqGroupCH to set
	 */
	public void setUniqGroupCH(Map<String, List<String>> uniqGroupCH) {
		this.uniqGroupCH = uniqGroupCH;
	}

	/**
	 * @return the uniqGroupIN
	 */
	public Map<String, List<String>> getUniqGroupIN() {
		return uniqGroupIN;
	}

	/**
	 * @param uniqGroupIN
	 *            the uniqGroupIN to set
	 */
	public void setUniqGroupIN(Map<String, List<String>> uniqGroupIN) {
		this.uniqGroupIN = uniqGroupIN;
	}

	public PaxValildationTO getPaxValidation() {
		return paxValidation;
	}

	public void setPaxValidation(PaxValildationTO paxValidation) {
		this.paxValidation = paxValidation;
	}

	public void setAlternativeEmailForXBE(String alternativeEmailForXBE) {
		this.alternativeEmailForXBE = alternativeEmailForXBE;
	}

	/**
	 * @return the alternative email to be set if email in blank in XBE.
	 */
	public String getAlternativeEmailForXBE() {
		return alternativeEmailForXBE;
	}

}
