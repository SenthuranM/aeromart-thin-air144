package com.isa.thinair.xbe.core.web.generator.travagent;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.AccelAeroClients;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class InvoiceHTMLGenerator {

	private static String clientErrors;
	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public String getInvoiceSummaryRowHtml(HttpServletRequest request) throws ModuleException {

		Invoice invoice = null;
		InvoiceSummaryDTO invoiceSummaryDTO = null;
		StringBuffer strMyData = new StringBuffer();

		SimpleDateFormat SMD = new SimpleDateFormat("dd/MM/yyyy");

		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
		String prefferedReportFormat = agent.getPrefferedRptFormat();

		NumberFormat numFormat = NumberFormat.getInstance(new Locale(prefferedReportFormat));
		numFormat.setMinimumFractionDigits(2);

		String strAgentCode = request.getParameter("selAgent");
		String strYear = request.getParameter("selYear");
		String strMonth = request.getParameter("selMonth");
		String invoiceNo = request.getParameter("hdnInvoice");
		String strPnr = PlatformUtiltiies.nullHandler(request.getParameter("hdnPNR"));
		String billingPeriod = request.getParameter("selBP");
		String entity = request.getParameter("selEntity");

		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		ResultSet resultSet = null;

		int intBP = 1;
		if (billingPeriod != null)
			intBP = Integer.parseInt(billingPeriod);

		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
		boolean inPayCurr = false;
		boolean isByTransaction = AppSysParamsUtil.isTransactInvoiceEnable();
		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY))
				&& "Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY))) {
			inPayCurr = true;
		}

		boolean paymentBreakdown = false;
		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BREAKDOWN))) {
			paymentBreakdown = true;
		}

		if (request.getParameter("chkBase") != null) {
			if (request.getParameter("chkBase").equals("on")) {
				strMyData.append("blnBaseCurr = true;");
			}
		}

		invoiceSummaryDTO = ModuleServiceLocator.getInvoicingBD().searchInvoiceForAgent(strAgentCode, strYear, strMonth, intBP,
				paymentBreakdown, invoiceNo, strPnr.length() == 0 ? null : strPnr,entity);

		String invoiceDate = null;
		String invoicePeriodFrom = null;
		String invoicePeriodTo = null;
		String invNo = "";
		BigDecimal invAmo = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal invAmoLocal = BigDecimal.ZERO;
		BigDecimal invCommission = AccelAeroCalculator.getDefaultBigDecimalZero();
		String accName = "";
		String agAdd = "";
		String agentEmail = "";
		String paymentTerms = "";
		BigDecimal resAount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal ExtAount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<String, BigDecimal> externalProductAmounts = new HashMap<String, BigDecimal>();
		BigDecimal invFare = BigDecimal.ZERO;
		BigDecimal invTax = BigDecimal.ZERO;
		BigDecimal invModify = BigDecimal.ZERO;
		BigDecimal invSurCharge = BigDecimal.ZERO;
		BigDecimal invFareLocal = BigDecimal.ZERO;
		BigDecimal invTaxLocal = BigDecimal.ZERO;
		BigDecimal invModifyLocal = BigDecimal.ZERO;
		BigDecimal invSurChargeLocal = BigDecimal.ZERO;
		BigDecimal resAountLocal = BigDecimal.ZERO;
		BigDecimal ExtAountLocal = BigDecimal.ZERO;

		if (invoiceSummaryDTO != null) {
			invoice = invoiceSummaryDTO.getInvoice();
			if (invoice != null) {
				invoiceDate = SMD.format(invoice.getInvoiceDate());
				invoicePeriodFrom = SMD.format(invoice.getInviocePeriodFrom());
				invoicePeriodTo = SMD.format(invoice.getInvoicePeriodTo());
				invNo = ((invoice.getInvoiceNumber() == null) ? "" : invoice.getInvoiceNumber());
				invAmo = ((invoice.getInvoiceAmount() == null) ? invAmo : invoice.getInvoiceAmount());
				invAmoLocal = (invoice.getInvoiceAmountLocal() == null) ? BigDecimal.ZERO : invoice.getInvoiceAmountLocal();
				resAount = ((invoiceSummaryDTO.getInvoiceResrvationAmount() == null) ? resAount : invoiceSummaryDTO
						.getInvoiceResrvationAmount());//TODO is this needed
				ExtAount = ((invoiceSummaryDTO.getInvoiceExternalAmount() == null) ? ExtAount : invoiceSummaryDTO
						.getInvoiceExternalAmount());//TODO is this needed
				externalProductAmounts = ((invoiceSummaryDTO.getInvoiceExternalProductAmounts() == null) ? externalProductAmounts
						: invoiceSummaryDTO.getInvoiceExternalProductAmounts());

				accName = ((invoiceSummaryDTO.getAccountName() == null) ? "" : invoiceSummaryDTO.getAccountName());
				agentEmail = ((invoiceSummaryDTO.getAgentEmail() == null) ? "" : invoiceSummaryDTO.getAgentEmail());
				agAdd = ((invoiceSummaryDTO.getAgentAddress() == null) ? "" : invoiceSummaryDTO.getAgentAddress());

				invFare = (invoiceSummaryDTO.getFareAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO.getFareAmount();
				invTax = (invoiceSummaryDTO.getTaxAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO.getTaxAmount();
				invModify = (invoiceSummaryDTO.getModifyAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO.getModifyAmount();
				invSurCharge = (invoiceSummaryDTO.getSurchrageAmount() == null) ? BigDecimal.ZERO : invoiceSummaryDTO
						.getSurchrageAmount();
				invCommission = (invoice.getCommissionAmount() == null) ? BigDecimal.ZERO : invoice.getCommissionAmount();

				if (inPayCurr) {
					strMyData.append("strData[12] = 'true';");
					invFareLocal = (invoiceSummaryDTO.getFareAmountLocal() == null) ? BigDecimal.ZERO : invoiceSummaryDTO
							.getFareAmountLocal();
					invTaxLocal = (invoiceSummaryDTO.getTaxAmountLocal() == null) ? BigDecimal.ZERO : invoiceSummaryDTO
							.getTaxAmountLocal();
					invModifyLocal = (invoiceSummaryDTO.getModifyAmountLocal() == null) ? BigDecimal.ZERO : invoiceSummaryDTO
							.getModifyAmountLocal();
					invSurChargeLocal = (invoiceSummaryDTO.getSurchrageAmountLocal() == null) ? BigDecimal.ZERO
							: invoiceSummaryDTO.getSurchrageAmountLocal();
				} else {
					strMyData.append("strData[12] = 'false';");
				}

				strMyData.append("strData[0] = '" + invNo + "';");
				strMyData.append("strData[1] = '" + invoiceDate + "';");
				strMyData.append("strData[3] = '" + agAdd + "';");
				strMyData.append("strData[4] = '" + invoicePeriodFrom + "';");
				strMyData.append("strData[5] = '" + invoicePeriodTo + "';");
				strMyData.append("strData[6] = '" + accName + "';");
				strMyData.append("strData[7] = '" + agentEmail + "';");
				strMyData.append("strData[11] = '" + invoiceSummaryDTO.getCurrencyCode() + "';");

				strMyData.append("strData[13] = '" + numFormat.format(invFareLocal) + "';");
				strMyData.append("strData[14] = '" + numFormat.format(invTaxLocal) + "';");
				strMyData.append("strData[15] = '" + numFormat.format(invModifyLocal) + "';");
				strMyData.append("strData[16] = '" + numFormat.format(invSurChargeLocal) + "';");
				strMyData.append("strData[17] = '" + numFormat.format(invFare) + "';");
				strMyData.append("strData[18] = '" + numFormat.format(invTax) + "';");
				strMyData.append("strData[19] = '" + numFormat.format(invModify) + "';");
				strMyData.append("strData[20] = '" + numFormat.format(invSurCharge) + "';");

				if (AccelAeroClients.AIR_ARABIA.equals(CommonsServices.getGlobalConfig().getBizParam(
						SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
						|| AccelAeroClients.AIR_ARABIA_GROUP.equals(CommonsServices.getGlobalConfig().getBizParam(
								SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))) {
					strMyData.append("strData[8] = '" + numFormat.format(resAount) + "';");
					strMyData.append("strData[9] = '" + numFormat.format(ExtAount) + "';");
				
					@SuppressWarnings("rawtypes")
					Iterator entries = externalProductAmounts.entrySet().iterator();
					Map<String, String> externalProductAmountsInString = new HashMap<String, String>();
					while (entries.hasNext()) {
						@SuppressWarnings("rawtypes")
						Entry extProdAmountPair = (Entry) entries.next();
						BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (extProdAmountPair.getValue() != null) {
							amount = (BigDecimal) extProdAmountPair.getValue();
						}
						externalProductAmountsInString.put((String) extProdAmountPair.getKey(), numFormat.format(amount));

					}

					String jsonStringOfExternalProdAmts = "";
					try {
						jsonStringOfExternalProdAmts = JSONUtil.serialize(externalProductAmountsInString);
					} catch (JSONException e) {
						log.error(e);
					}

					strMyData.append("strData[31] = '" + jsonStringOfExternalProdAmts + "';");
					resAountLocal = (resAountLocal == null) ? BigDecimal.ZERO : resAountLocal;
					ExtAountLocal = (ExtAountLocal == null) ? BigDecimal.ZERO : ExtAountLocal;
					strMyData.append("strData[21] = '" + numFormat.format(resAountLocal) + "';");
					strMyData.append("strData[22] = '" + numFormat.format(ExtAountLocal) + "';");
					strMyData.append("strData[2] = '" + numFormat.format(AccelAeroCalculator.add(invAmo, resAountLocal)) + "';");
					strMyData.append("strData[10] = '" + numFormat.format(invAmoLocal.add(ExtAountLocal)) + "';");
				} else {
					strMyData.append("strData[2] = '" + numFormat.format(invAmo) + "';");
					strMyData.append("strData[10] = '" + numFormat.format(invAmoLocal) + "';");
				}

				if (AppSysParamsUtil.isAgentCommmissionEnabled()) {
					strMyData.append("strData[29] = 'true';");
					strMyData.append("strData[30] = '" + numFormat.format(invCommission) + "';");
				} else {
					strMyData.append("strData[29] = 'false';");
				}

				if (paymentBreakdown) {
					strMyData.append("strData[23] = 'true';"); // indicate payment breakdown
				} else {
					strMyData.append("strData[23] = 'false';");
				}

				if (isByTransaction) {
					ReportsSearchCriteria searchSummary = new ReportsSearchCriteria();
					if (strlive != null) {
						if (strlive.equals(WebConstants.REP_LIVE)) {
							searchSummary.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
						} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
							searchSummary.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
						}
					}
					searchSummary.setAgentCode(strAgentCode);
					searchSummary.setInvoiceNumber(invoiceNo);

					resultSet = ModuleServiceLocator.getDataExtractionBD()
							.getInvoiceSummaryAttachmentByTransaction(searchSummary);

					String passName = "";
					String route = "";
					String bookedDate = "";

					try {
						if (resultSet != null) {
							if (resultSet.next()) {
								passName = resultSet.getString("res_name");
								route = resultSet.getString("route");
								bookedDate = resultSet.getString("booked_date");
							}
						}
					} catch (SQLException ex) {
						throw new ModuleException("Error in getting per transaction details");
					}

					strMyData.append("strData[24] = 'true';"); // indicate per transaction
					strMyData.append("strData[25] = '" + strPnr + "';");
					strMyData.append("strData[26] = '" + passName + "';");
					strMyData.append("strData[27] = '" + route + "';");
					strMyData.append("strData[28] = '" + bookedDate + "';");
				} else {
					strMyData.append("strData[24] = 'false';");
				}

				if (AccelAeroClients.MIHIN_LANKA.equals(CommonsServices.getGlobalConfig().getBizParam(
						SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
						&& invoiceSummaryDTO.getReportToGSA().equals("Y")) {
					paymentTerms = "Please contact your GSA for payment terms";
				} else {
					Agent invoiceAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(strAgentCode);
					Territory territory = ModuleServiceLocator.getLocationServiceBD().getTerritory(
							invoiceAgent.getTerritoryCode());
					paymentTerms = globalConfig.getBizParam(SystemParamKeys.PAYMENT_TERMS);
					String accountNumber = AppSysParamsUtil.getSettlementBankAccountNumber();
					String bankName = AppSysParamsUtil.getSettlementBankName();
					String bankAddress = AppSysParamsUtil.getSettlementBankAddress();
					String baseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
					String swiftCode = AppSysParamsUtil.getSettlementBankSwiftCode();
					String IBANCode = AppSysParamsUtil.getSettlementBankIBANNo();
					String beneficialName = AppSysParamsUtil.getSettlementBankBeneficiaryName();

					request.setAttribute(WebConstants.REQ_ACCOUNT_NO,
							getFirstValueIfNotNullAndNotEmpty(territory.getAccountNumber(), accountNumber));

					request.setAttribute(WebConstants.REQ_BANK_NAME,
							getFirstValueIfNotNullAndNotEmpty(territory.getBank(), bankName));
					request.setAttribute(WebConstants.REQ_BANK_ADDRESS,
							getFirstValueIfNotNullAndNotEmpty(territory.getBankAddress(), bankAddress));
					request.setAttribute(WebConstants.REQ_BASE_CURRENCY, baseCurrency);
					request.setAttribute(WebConstants.REQ_SWIFT_CODE,
							getFirstValueIfNotNullAndNotEmpty(territory.getSwiftCode(), swiftCode));
					request.setAttribute(WebConstants.REQ_BANK_IBAN_CODE,
							getFirstValueIfNotNullAndNotEmpty(territory.getIBANCode(), IBANCode));
					request.setAttribute(WebConstants.REQ_BANK_BENEFICIARY_NAME,
							getFirstValueIfNotNullAndNotEmpty(territory.getBeneficiaryName(), beneficialName));
				}
				request.setAttribute(WebConstants.REQ_PAYMENT_TERMS, paymentTerms);
			}

		}

		return strMyData.toString();
	}

	public String setInvoiceSummaryGridHTML(HttpServletRequest request) throws ModuleException {
		String strAgentCode = request.getParameter("selAgent");
		String strInvDate = request.getParameter("txtInvoiceDate");
		String dateFrom = null;
		String dateTo = null;
		StringBuffer sbGridData = new StringBuffer("var arrInvData = new Array();");
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		Collection<GeneratedInvoiceDTO> colGeneratedInvDto = new ArrayList();
		GeneratedInvoiceDTO genInvoiceDto = null;
		if (strInvDate != null && !"".equals(strInvDate)) {
			dateFrom = "'" + strInvDate + " 00:00:00' , 'DD/MM/YYYY HH24:mi:ss'";
			dateTo = "'" + strInvDate + " 23:59:59' , 'DD/MM/YYYY HH24:mi:ss'";
		}

		colGeneratedInvDto = ModuleServiceLocator.getInvoicingBD()
				.searchGeneratedInvoicesForAgent(strAgentCode, dateFrom, dateTo);

		if (colGeneratedInvDto != null) {
			Object[] lstArray = colGeneratedInvDto.toArray();
			for (int i = 0; i < lstArray.length; i++) {
				sbGridData.append("arrInvData[" + i + "] = new Array();");
				genInvoiceDto = (GeneratedInvoiceDTO) lstArray[i];
				String strDate = format.format(genInvoiceDto.getInvoiceDate());
				sbGridData.append("arrInvData[" + i + "][0] = '" + genInvoiceDto.getInvoiceNumber() + "';");
				sbGridData.append("arrInvData[" + i + "][1] = '" + strDate + "';");
				sbGridData.append("arrInvData[" + i + "][2] = '" + genInvoiceDto.getPnr() + "';");
				sbGridData.append("arrInvData[" + i + "][3] = '" + genInvoiceDto.getInvoiceAmount() + "';");
			}
		}

		return sbGridData.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.agent.viewInvoice.empty", "noInvoice");
			moduleErrs.setProperty("um.agent.viewInvoice.futureDate", "futureDate");
			moduleErrs.setProperty("um.agent.viewInvoice.agentName.empty", "agentEmpty");
			moduleErrs.setProperty("um.agent.viewInvoice.not.available", "invoiceEmpty");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "DeleteConfirm");
			moduleErrs.setProperty("um.agent.viewInvoice.email.blank", "EMailisEmpty");
			moduleErrs.setProperty("um.agent.viewInvoice.email.invalidmailformat", "InvalidEmail");
			moduleErrs.setProperty("um.agent.viewInvoice.agent.email", "agentEmail");
			moduleErrs.setProperty("um.agent.viewInvoice.given.email", "givenEmail");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private String getFirstValueIfNotNullAndNotEmpty(String first, String second) {
		if (first == null || first.isEmpty()) {
			return second;
		}
		return first;
	}

}