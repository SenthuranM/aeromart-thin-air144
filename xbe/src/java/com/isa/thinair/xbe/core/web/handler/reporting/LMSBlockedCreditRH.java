package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class LMSBlockedCreditRH extends BasicRH {

	private static Log log = LogFactory.getLog(LMSBlockedCreditRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("LMSBlockedCreditRH setReportView Failed " + e.getMessageString());
			forward = S2Constants.Result.ERROR;
		} catch (Exception e) {
			log.error("Error in LMSBlockedCreditRH execute()" + e.getMessage());
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;

		String reportTemplate = "BlockedLmsPointsReport.jasper";

		String id = "UC_REPM_071";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String pnr = request.getParameter("pnr");
		String lmsAccountId = request.getParameter("lmsAccountId");
		String usageFromDate = request.getParameter("txtUsegeFrom");
		String usageToDate = request.getParameter("txtUsegeTo");
		String value = request.getParameter("radReportOption");

		if (isNotEmptyOrNull(usageFromDate) && isNotEmptyOrNull(usageToDate)) {
			String fromDate = "'" + usageFromDate + " 00:00:00 ','dd/mm/yyyy HH24:mi:ss' ";
			String toDate = "'" + usageToDate + " 23:59:59 ','dd/mm/yyyy HH24:mi:ss' ";

			search.setDateRangeFrom(fromDate);
			search.setDateRangeTo(toDate);

			if (isNotEmptyOrNull(pnr)) {
				search.setPnr(pnr);
			}
			if (isNotEmptyOrNull(lmsAccountId)) {
				search.setAccountNumber(lmsAccountId);
			}

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getBlockedLMSPointsData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();

			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=BlockedLmsPointsReport.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=BlockedLmsPointsReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=BlockedLmsPointsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		}
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}
}
