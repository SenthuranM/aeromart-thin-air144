package com.isa.thinair.xbe.core.web.generator.customer;

import java.util.Properties;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class CustomerHTMLGenerator {
	
	private static String clientErrors;

	public String getClientErrors() throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("customer.details.email.required", "emailRequired");
			moduleErrs.setProperty("customer.details.email.invalid", "emailInvalid");
			moduleErrs.setProperty("customer.details.email.exists", "emailExists");
			moduleErrs.setProperty("customer.details.title.required", "titleRequired");
			moduleErrs.setProperty("customer.details.fname.required", "fnameRequired");
			moduleErrs.setProperty("customer.details.lname.required", "lnameRequired");
			moduleErrs.setProperty("customer.details.mobile.required", "mobileRequired");
			moduleErrs.setProperty("customer.details.mobile.invalid", "mobileInvalid");
			moduleErrs.setProperty("customer.details.phone.invalid", "phoneInvalid");
			moduleErrs.setProperty("customer.details.fax.invalid", "faxInvalid");
			moduleErrs.setProperty("customer.details.natonality.required", "nationalityRequired");
			moduleErrs.setProperty("customer.details.country.required", "countryRequired");
			moduleErrs.setProperty("customer.details.mobile.country.required", "mcountryRequired");
			moduleErrs.setProperty("customer.details.phone.country.required", "pcountryRequired");
			moduleErrs.setProperty("customer.details.fax.country.required", "fcountryRequired");
			moduleErrs.setProperty("customer.details.lms.dob.required", "dobRequired");
			moduleErrs.setProperty("customer.details.lms.dob.invalid", "dobInvalid");
			moduleErrs.setProperty("customer.details.lms.hemail.required", "familyHeadRequired");
			moduleErrs.setProperty("customer.details.lms.hemail.invalid", "familyHeadInvalid");
			moduleErrs.setProperty("customer.details.lms.refmail.invalid", "refemailInvalid");
			moduleErrs.setProperty("customer.details.lms.refmail.doesnt.exist", "refEmailDoesntExist");
			moduleErrs.setProperty("customer.details.lms.hemail.doesnt.exist", "familyHeadDoesntExist");
			moduleErrs.setProperty("customer.registration.successfull", "registerSuccessfull");
			moduleErrs.setProperty("customer.modification.successfull", "modificationSuccessfull");
			moduleErrs.setProperty("customer.modification.lms.enroll.not.allow", "lmsJoinNotAllowed");
			


			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}


}
