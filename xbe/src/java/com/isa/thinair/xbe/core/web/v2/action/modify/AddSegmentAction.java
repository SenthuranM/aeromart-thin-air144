package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.SystemParams;
import com.isa.thinair.xbe.api.dto.v2.ModifySegmentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.AirportDDListGenerator;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Add Segment
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_ADD_SEGMENT),
		@Result(name = S2Constants.Result.REQUOTE, value = S2Constants.Jsp.Reservation.MODIFY_RES_REQUOTE),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class AddSegmentAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AddSegmentAction.class);

	private String pgMode;

	private String pnrNo;

	private boolean groupPNRNo;

	private String resSegments;

	private String resPax;

	private int resNoOfAdults;

	private int resNoOfInfants;

	private int resNoOfchildren;

	private String resPaySummary;

	private String resContactInfo;

	private String version;

	private boolean addGroundSegment;

	private String selectedOnd;

	private String flightDetails;

	private String selectedPnrSeg;

	private String resOwnerAgent;
	
	private String reasonForAllowBlPax;

	private String taxInvoicesExist;

	@SuppressWarnings("unchecked")
	public String execute() {
		try {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));

			if (AppSysParamsUtil.isGroundServiceEnabled()) {
				request.setAttribute("existingBusCarrierCodes", JSONUtil.serialize(loadExistingBusCarriers(colsegs)));
			} else {
				request.setAttribute("existingBusCarrierCodes", "[]");
			}

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rparam = new ReservationProcessParams(request, resInfo != null
					? resInfo.getReservationStatus()
					: null, colsegs, resInfo.isModifiableReservation(), resInfo.isGdsReservationModAllowed(),
					resInfo.getLccPromotionInfoTO(), null, false, false);
			if (groupPNRNo) {
				rparam.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}
			request.setAttribute("initialParams", JSONUtil.serialize(rparam));

			Set colSegSet = new HashSet(colsegs);
			Object[] arrDepArrZulu = ReservationUtil.getDepArrZulutimes(colSegSet);

			ModifySegmentTO reservationInfo = new ModifySegmentTO();
			reservationInfo.setRequote(true);
			reservationInfo.setMode(pgMode);
			reservationInfo.setPNR(pnrNo);
			if (groupPNRNo) {
				reservationInfo.setGroupPNR(pnrNo);
				reservationInfo.setInterline(true);
			} else {
				reservationInfo.setGroupPNR("");
			}
			reservationInfo.setNoOfAdults(resNoOfAdults);
			reservationInfo.setNoOfInfants(resNoOfInfants);
			reservationInfo.setNoOfChildren(resNoOfchildren);
			reservationInfo.setOwnerAgentCode(resOwnerAgent);
			reservationInfo.setVersion(version);
			reservationInfo.setStatus(resInfo.getReservationStatus());

			reservationInfo.setTicketExpiryEnabled(new Boolean(request.getParameter("ticketExpiryEnabled")));
			reservationInfo.setTicketExpiryDate(request.getParameter("ticketExpiryDate"));
			reservationInfo.setTicketValidFromDate(request.getParameter("ticketValidFrom"));
			reservationInfo.setIsTaxInvoicesAvailable(Boolean.TRUE.toString().equals(taxInvoicesExist));

			// Set First Departure And Last Arrival
			if (arrDepArrZulu != null) {
				if (arrDepArrZulu[0] != null) {
					// reservationInfo.setFirstDepature(((Date)arrDepArrZulu[0]).getTime());
					reservationInfo.setFirstDepature(DateFormatUtils.format((Date) arrDepArrZulu[0], "yyyy-MM-dd HH:mm:ss.S"));
				}
				if (arrDepArrZulu[1] != null) {
					reservationInfo.setFirstDepature(DateFormatUtils.format((Date) arrDepArrZulu[1], "yyyy-MM-dd HH:mm:ss.S"));
					// reservationInfo.setLastArrival(((Date)arrDepArrZulu[1]).getTime());
				}
			}

			request.setAttribute("reservationInfo", JSONUtil.serialize(reservationInfo));
			request.setAttribute("resSegments", resSegments);
			request.setAttribute("resPaxs", resPax.replaceAll("'", "\\\\'"));
			request.setAttribute("reqPayAgents", ReservationUtil.getPaymentAgentOptions(request));
			request.setAttribute("resPaySummary", resPaySummary);
			request.setAttribute("resContactInfo", resContactInfo);
			request.setAttribute("resHasInsurance", request.getParameter("resHasInsurance"));
			request.setAttribute("carrierCode", AppSysParamsUtil.getDefaultCarrierCode());
			request.setAttribute("flexiAlertInfo", request.getParameter("flexiAlertInfo"));
			request.setAttribute("blnIsFromNameChange", request.getParameter("blnIsFromNameChange"));
			request.setAttribute("paxNameDetails", request.getParameter("paxNameDetails"));
			request.setAttribute("modifyUNSegments", AppSysParamsUtil.isEnableSegmentModificationForCancelledFlightSegments());
			request.setAttribute("reasonForAllowBlPax", request.getParameter("reasonForAllowBlPax"));

			SystemParams sysParams = new SystemParams();
			sysParams.setMinimumTransitionTimeInMillis(AppSysParamsUtil.getMinimumReturnTransitTimeInMillis());
			sysParams.setDefaultLanguage(XBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));
			sysParams.setDynamicOndListPopulationEnabled(AppSysParamsUtil.isDynamicOndListPopulationEnabled());
			sysParams.setUserDefineTransitTimeEnabled(AppSysParamsUtil.isEnableUserDefineTransitTime());

			request.setAttribute("systemParams", JSONUtil.serialize(sysParams));
			request.setAttribute("isAddSegment", true);
			boolean addGroundSegment = this.getAddGroundSegment(rparam.isGroundServiceEnabled());
			request.setAttribute("addGroundSegment", addGroundSegment);
			String operatingCarrierCode = ReservationUtil.getDryOperatingCarrier(colsegs);
			String strOnd = this.selectedOnd;
			if (addGroundSegment) {
				Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs,
						this.getSelectedPnrSeg(), rparam, true);
				operatingCarrierCode = ReservationUtil.getDryOperatingCarrier(colModSegs);

				String depSegment = null;
				String arrSegment = null;
				Date arrivalDate = null;
				Date departureDateZulu = null;
				for (LCCClientReservationSegment seg : colsegs) {
					for (LCCClientReservationSegment intModSeg : colModSegs) {
						if (intModSeg.getBookingFlightSegmentRefNumber().equals(seg.getBookingFlightSegmentRefNumber())) {
							if (departureDateZulu == null || departureDateZulu.after(seg.getDepartureDateZulu())) {
								depSegment = seg.getSegmentCode();
								departureDateZulu = seg.getDepartureDateZulu();
							}
							if (arrivalDate == null || arrivalDate.before(seg.getArrivalDateZulu())) {
								arrivalDate = seg.getArrivalDateZulu();
								arrSegment = seg.getSegmentCode();
							}
						}
					}
				}
				String depAirport = depSegment.split("/")[0];
				String[] arrSegmentCodes = arrSegment.split("/");
				String arrAirport = arrSegmentCodes[arrSegmentCodes.length - 1];
				strOnd = depAirport + "/" + arrAirport;
			}

			request.setAttribute("allowedGroundStations",
					this.prepareGroundSegmentData(groupPNRNo, strOnd, addGroundSegment, operatingCarrierCode));
			request.setAttribute("flightDetails", this.getFlightDetails() == null ? "" : this.getFlightDetails());
			request.setAttribute("selectedPnrSeg", this.getSelectedPnrSeg());
			request.setAttribute("excludableCharges", JSONUtil.serialize(ReservationUtil.getExcludableChargesMap()));
			request.setAttribute("skipNonModifiedSegFareONDs", AppSysParamsUtil.isSkipNonModifiedSegFareONDs());

			if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
				request.setAttribute("sysOndSource", AppSysParamsUtil.getDynamicOndListUrl());
			}

		} catch (Exception e) {
			log.error("Error In AddSegmentAction", e);
			return S2Constants.Result.ERROR;
		}
		if (AppSysParamsUtil.isRequoteEnabled()) {
			return S2Constants.Result.REQUOTE;
		} else {
			return S2Constants.Result.SUCCESS;
		}
	}

	private String prepareGroundSegmentData(boolean groupPNRNo, String strOnd, boolean isSubStationAllowed,
			String operatingCarrier) throws JSONException, ModuleException {
		StringBuilder newGroundStations = new StringBuilder();

		GroundSegmentTO groundSegmentTO = new GroundSegmentTO();
		groundSegmentTO.setAddGroundSegment(isSubStationAllowed);
		groundSegmentTO.setGroupPNRNo(groupPNRNo);
		groundSegmentTO.setSelectedOnd(strOnd);
		groundSegmentTO.setOperatingCarrier(operatingCarrier);
		List[] arrList = ModuleServiceLocator.getAirproxySegmentBD().getGroundSegment(groundSegmentTO,
				TrackInfoUtil.getBasicTrackInfo(request));

		newGroundStations.append(AirportDDListGenerator.prepareAirportsHtml(request, !isSubStationAllowed ? null : arrList[0],
				"arrGroundSegmentParentFrom", isSubStationAllowed));
		newGroundStations.append(AirportDDListGenerator.prepareAirportsHtml(request, !isSubStationAllowed ? null : arrList[1],
				"arrGroundSegmentParentTo", isSubStationAllowed));

		return newGroundStations.toString();
	}

	private Set<String> loadExistingBusCarriers(Collection<LCCClientReservationSegment> reservationSegments)
			throws ModuleException {
		Set<String> existingBusCarriers = new HashSet<String>();
		Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();

		if (busAirCarrierCodes != null && !busAirCarrierCodes.isEmpty()) {
			for (LCCClientReservationSegment reservationSegment : reservationSegments) {
				if (busAirCarrierCodes.containsKey(FlightRefNumberUtil.getOperatingAirline(reservationSegment
						.getFlightSegmentRefNumber()))) {
					existingBusCarriers.add(FlightRefNumberUtil.getOperatingAirline(reservationSegment
							.getFlightSegmentRefNumber()));
				}
			}
		}
		return existingBusCarriers;

	}

	public void setPgMode(String pgMode) {
		this.pgMode = pgMode;
	}

	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}

	public void setGroupPNRNo(boolean groupPNRNo) {
		this.groupPNRNo = groupPNRNo;
	}

	public void setResSegments(String resSegments) {
		this.resSegments = resSegments;
	}

	public void setResPax(String resPax) {
		this.resPax = resPax;
	}

	public void setResNoOfAdults(int resNoOfAdults) {
		this.resNoOfAdults = resNoOfAdults;
	}

	public void setResNoOfInfants(int resNoOfInfants) {
		this.resNoOfInfants = resNoOfInfants;
	}

	public void setResNoOfchildren(int resNoOfchildren) {
		this.resNoOfchildren = resNoOfchildren;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getResPax() {
		return resPax;
	}

	public void setResPaySummary(String resPaySummary) {
		this.resPaySummary = resPaySummary;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public boolean getAddGroundSegment() {
		if (!AppSysParamsUtil.isGroundServiceEnabled()) {
			return false;
		}
		return addGroundSegment;
	}

	public boolean getAddGroundSegment(boolean groundServiceEnabled) {
		if (!groundServiceEnabled) {
			return false;
		}
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean groundSegmentAllowed) {
		this.addGroundSegment = groundSegmentAllowed;
	}

	public String getSelectedOnd() {
		return selectedOnd;
	}

	public void setSelectedOnd(String selectedOnd) {
		this.selectedOnd = selectedOnd;
	}

	public String getFlightDetails() {
		return flightDetails;
	}

	public void setFlightDetails(String flightDetails) {
		this.flightDetails = flightDetails;
	}

	public String getSelectedPnrSeg() {
		return selectedPnrSeg;
	}

	public void setSelectedPnrSeg(String selectedPnrSeg) {
		this.selectedPnrSeg = selectedPnrSeg;
	}

	public void setResOwnerAgent(String resOwnerAgent) {
		this.resOwnerAgent = resOwnerAgent;
	}

	public void setTaxInvoicesExist(String taxInvoicesExist) {
		this.taxInvoicesExist = taxInvoicesExist;
	}

	public String getReasonForAllowBlPax() {
		return reasonForAllowBlPax;
	}

	public void setReasonForAllowBlPax(String reasonForAllowBlPax) {
		this.reasonForAllowBlPax = reasonForAllowBlPax;
	}
}
