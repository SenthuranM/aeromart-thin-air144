package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.UserIncentiveDetailsReportRH;

/**
 * Action class for User Incentive Details Report
 * 
 * @author Harshana
 * @since 22-02-2012
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({
	@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.USER_INCENTIVE_DETAILS_REPORT),
	@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) 
})
public class ShowUserIncentiveDetailsReportAction extends BaseRequestResponseAwareAction {

	public String execute() {
		return UserIncentiveDetailsReportRH.execute(request, response);
	}

}
