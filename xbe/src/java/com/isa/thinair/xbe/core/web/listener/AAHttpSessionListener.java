package com.isa.thinair.xbe.core.web.listener;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AAHttpSessionListener implements HttpSessionListener {

	private static Log log = LogFactory.getLog(AAHttpSessionListener.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");

	@Override
	public void sessionCreated(HttpSessionEvent hse) {
		try {
			if (hse != null) {
				HttpSession session = hse.getSession();
				if (session != null) {
					log.info("SessionCreated : " + session.getId() + ":" + dateFormat.format(session.getCreationTime()) + ":"
							+ dateFormat.format(session.getLastAccessedTime()) + ":" + session.getMaxInactiveInterval());
				} else {
					log.error("SessionCreated event :: session is null");
				}
			}
		} catch (Exception ex) {
			log.error("SessionCreated event :: Error in logging session created event", ex);
		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent hse) {
		try {
			if (hse != null) {
				HttpSession session = hse.getSession();
				if (session != null) {
					log.info("SessionDestroyed : " + session.getId() + ":" + dateFormat.format(session.getCreationTime()) + ":"
							+ dateFormat.format(session.getLastAccessedTime()) + ":" + session.getMaxInactiveInterval());
				} else {
					log.error("SessionDestroyed event :: session is null");
				}
			}
		} catch (Exception ex) {
			log.error("SessionDestroyed event :: Error in logging session destroyed event", ex);
		}

	}

}
