package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class BlackListedPaxReservationAction {
	/**
	 * @author subash
	 */
	private static Log log = LogFactory.getLog(BlackListedPaxReservationAction.class);

	private BlacklisPaxReservationTO blPaxResToSearchReq = new BlacklisPaxReservationTO();
	private BlacklisPaxReservationTO blacklistResTOSaveReq;

	private String message;
	private String msgType;

	private ArrayList<Map<String, Object>> blPaxResList;

	private int pageSize = 20;

	private int page;

	private int records;

	private int total;

	public String search() {
		try {
			int start = (page - 1) * pageSize;
			Page<BlacklisPaxReservationTO> blPaxResage = ModuleServiceLocator.getBlacklisPAXBD().searchBlacklistPaxResTO(
					blPaxResToSearchReq, start, pageSize);

			blPaxResList = getBlPaxResList((List<BlacklisPaxReservationTO>) blPaxResage.getPageData());

			records = blPaxResage.getTotalNoOfRecords();
			total = blPaxResage.getTotalNoOfRecords() / pageSize;
			int mod = blPaxResage.getTotalNoOfRecords() % pageSize;

			if (mod > 0) {
				total = this.total + 1;
			}

			msgType = S2Constants.Result.SUCCESS;
		} catch (Exception e) {
			log.error("error occured : " + e.getCause());
			msgType = S2Constants.Result.ERROR;
			message = e.getMessage();

		}
		return S2Constants.Result.SUCCESS;
	}

	public String update() {
		try {
			if (blacklistResTOSaveReq.getBlacklistReservationId() != null
					&& !blacklistResTOSaveReq.getBlacklistReservationId().equals("")) {
				ModuleServiceLocator.getBlacklisPAXBD().updateBlacklistReservation(blacklistResTOSaveReq);
				msgType = S2Constants.Result.SUCCESS;
			}

		} catch (Exception e) {
			msgType = S2Constants.Result.ERROR;
			log.error("error occured : " + e.getCause());
			message = e.getMessage();
		}
		return S2Constants.Result.SUCCESS;
	}

	private ArrayList<Map<String, Object>> getBlPaxResList(List<BlacklisPaxReservationTO> blPaxlist) {
		ArrayList<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
		BlacklisPaxReservationTO blPaxResTo = null;

		int i = 0;
		Iterator<BlacklisPaxReservationTO> iterator = blPaxlist.iterator();
		while (iterator.hasNext()) {
			blPaxResTo = iterator.next();
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("blacklisPaxReservationTO", blPaxResTo);
			row.put("id", i++);
			rows.add(row);
		}

		return rows;
	}

	public BlacklisPaxReservationTO getBlPaxResToSearchReq() {
		return blPaxResToSearchReq;
	}

	public void setBlPaxResToSearchReq(BlacklisPaxReservationTO blPaxResToSearchReq) {
		this.blPaxResToSearchReq = blPaxResToSearchReq;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<Map<String, Object>> getBlPaxResList() {
		return blPaxResList;
	}

	public void setBlPaxResList(ArrayList<Map<String, Object>> blPaxResList) {
		this.blPaxResList = blPaxResList;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public BlacklisPaxReservationTO getBlacklistResTOSaveReq() {
		return blacklistResTOSaveReq;
	}

	public void setBlacklistResTOSaveReq(BlacklisPaxReservationTO blacklistResTOSaveReq) {
		this.blacklistResTOSaveReq = blacklistResTOSaveReq;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
