package com.isa.thinair.xbe.core.web.v2.generator.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

public class JavaScriptGenerator {

	/**
	 * This method is used to generate refund agent list which include remote agents as well as own agents For Normal
	 * Users 1) Allow ANY On Account payment - all the agents in the carrier will be added to the list 2) Allow
	 * Reporting On Account payment - all the reporting agents plus the own account will be added 3) Allow On Account
	 * payment on operating carrier For Dry Users 1) Allow ANY On Account payment - all the marketing carrier agents
	 * will be added to the list 2) Allow Reporting On Account payment - all the marketing carrier reporting agents +
	 * own agent will be added to the list 3) Allow On Account payment - marketing carrier own agent will be added to
	 * the list. 4) Allow ANY On Account payment on Operating and marketing Carrier - will include all the agents in the
	 * operating carrier to the agent list.
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public static String createTAsWithCreditHtml(HttpServletRequest request) throws ModuleException {
		Collection<String[]> colStrAgents = null;
		int columnCount = 2;

		StringBuilder sb = new StringBuilder();
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String defaultAirlineCode = "";
		String refCode = null;
		String ownAirlineCode = null;

		if (mapPrivileges != null) {

			defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
			ownAirlineCode = userPrincipal.getAirlineCode();
			if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_PAYMNETS_ANY) != null) {
				if (AppSysParamsUtil.isGSAStructureVersion2Enabled()
						&& !AppSysParamsUtil.getCarrierAgent().equals(userPrincipal.getAgentTypeCode())) {
					User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
					colStrAgents = SelectListGenerator.createAllSubAgentsWithCredit(ownAirlineCode, user.getAgentCode(),
							columnCount);
				} else {
					colStrAgents = SelectListGenerator.createAllTAsWithCredit(ownAirlineCode, columnCount);
				}

			} else if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_PAYMNETS) != null) {
				User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
				String agentCode = user.getAgentCode();

				if (agentCode != null && mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_PAYMNETS_REPORTING) != null) {
					colStrAgents = SelectListGenerator.createReportingTAsWithCredit(ownAirlineCode, agentCode, columnCount);
				}
			}
		}

		if (colStrAgents != null) {
			for (String[] strAgent : colStrAgents) {
				if ("".equals(defaultAirlineCode)) {
					refCode = strAgent[0];
				} else {
					refCode = strAgent[0] + " - " + strAgent[1].substring(0, 3);
				}
				sb.append("<option value='" + strAgent[1] + "'>" + refCode + "</option>");
			}
		}
		return sb.toString();
	}

	/**
	 * This method is used to generate refund agent list which include remote agents as well as own agents For Normal
	 * Users 1) Allow ANY On Account Refund - all the agents in the carrier will be added to the list 2) Allow Reporting
	 * On Account Refund - all the reporting agents plus the own account will be added 3) Allow On Account Refund on
	 * operating carrier For Dry Users 1) Allow ANY On Account Refund - all the marketing carrier agents will be added
	 * to the list 2) Allow Reporting On Account Refund - all the marketing carrier reporting agents + own agent will be
	 * added to the list 3) Allow On Account Refund - marketing carrier own agent will be added to the list. 4) Allow
	 * ANY On Account Refund on Operating and marketing Carrier - will include all the agents in the operating carrier
	 * to the agent list.
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public static String createRefundTAWithCreditHtml(HttpServletRequest request) throws ModuleException {

		int columnCount = 3;
		StringBuilder sb = new StringBuilder();

		List<String[]> results = new ArrayList<String[]>();
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String defaultAirlineCode = AppSysParamsUtil.getDefaultAirlineIdentifierCode();
		String refCode = null;
		if (mapPrivileges != null) {
			String ownAirlineCode = userPrincipal.getAirlineCode();

			Collection<String[]> localAgentList = new ArrayList<String[]>();
			if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_REFUND_ANY) != null) {
				if (AppSysParamsUtil.isGSAStructureVersion2Enabled()
						&& !AppSysParamsUtil.getCarrierAgent().equals(userPrincipal.getAgentTypeCode())) {
					User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
					localAgentList = SelectListGenerator.createAllSubAgentsWithCredit(ownAirlineCode, user.getAgentCode(),
							columnCount);
				} else {
					localAgentList = SelectListGenerator.createAllTAsWithCredit(ownAirlineCode, 3);
				}
			} else if (mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_REFUND) != null) {
				User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
				String agentCode = user.getAgentCode();
				if (agentCode != null && mapPrivileges.get(PriviledgeConstants.ACCEPT_ONACCOUNT_REFUND_REPORTING) != null) {
					localAgentList = SelectListGenerator.createReportingTAsWithCredit(ownAirlineCode, agentCode, columnCount);
				}
			}
			results.addAll(localAgentList);
		}

		if (results != null) {
			for (String[] strAgent : results) {
				if ("".equals(defaultAirlineCode)) {
					refCode = strAgent[0];
				} else {
					refCode = strAgent[0] + " - " + strAgent[1].substring(0, 3);
				}
				sb.append("<option value='" + strAgent[1] + "'>" + refCode + "</option>");
			}
		}
		return sb.toString();
	}
}
