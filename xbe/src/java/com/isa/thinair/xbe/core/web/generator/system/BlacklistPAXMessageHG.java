package com.isa.thinair.xbe.core.web.generator.system;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class BlacklistPAXMessageHG {

	private static String templateclientErrors;

	public static String getTemplateClientErrors(HttpServletRequest request) throws ModuleException {

		if (templateclientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.blacklist.pax.rule.label.required", "ruleLabelRequired");
			moduleErrs.setProperty("um.blacklist.pax.rule.data.element.required", "ruleDataEleRequired");
			moduleErrs.setProperty("um.blacklist.pax.rule.data.element.duplicate", "duplicateRule");

			templateclientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return templateclientErrors;
	}

}
