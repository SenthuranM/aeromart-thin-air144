package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airproxy.api.model.reservation.commons.BasicChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;

/**
 * Pax Wise Charge Holder to fetch the reflective charge dates
 * 
 * @author Manoj Dhanushka
 * 
 */
public class PnrPaxChargeHolder {

	private Map<String, Map<BigDecimal, Set<BasicChargeTO>>> chargeMap = new HashMap<String, Map<BigDecimal, Set<BasicChargeTO>>>();

	public void addCharge(BasicChargeTO chargeTO) {
		String chargeGroupCode = getChargeGroupCode(chargeTO);
		if (!chargeMap.containsKey(chargeGroupCode)) {
			chargeMap.put(chargeGroupCode, new HashMap<BigDecimal, Set<BasicChargeTO>>());
		}
		if (!chargeMap.get(chargeGroupCode).containsKey(chargeTO.getAmount())) {
			chargeMap.get(chargeGroupCode).put(chargeTO.getAmount(), new HashSet<BasicChargeTO>());
		}

		chargeMap.get(chargeGroupCode).get(chargeTO.getAmount()).add(chargeTO);
	}

	/**
	 * Based on the charge group code and reflective charge value (opposite sign value) of the ReservationPaxOndCharge
	 * passed the method, will return the the charge date of the already added matching charge if any. Else it will
	 * return the passed charge date
	 * 
	 * Reason for using this is to remove the effect of exchange rate changes in between charge date and reservation
	 * loading date.
	 * 
	 * @param reservationPaxOndCharge
	 * @return
	 */
	public Date getZuluChargeDate(BasicChargeTO chargeTO) {
		String chargeGroupCode = getChargeGroupCode(chargeTO);
		if (chargeTO.getAmount().doubleValue() < 0) {
			if (chargeMap.containsKey(chargeGroupCode)
					&& chargeMap.get(chargeGroupCode).containsKey(chargeTO.getAmount().negate())) {
				Set<BasicChargeTO> chargeSet = chargeMap.get(chargeGroupCode).get(chargeTO.getAmount().negate());
				if (chargeSet.size() > 0) {
					BasicChargeTO chg = chargeSet.iterator().next();
					chargeSet.remove(chg);
					return getApplicableZuluChargeDate(chg);
				} else {
					return getApplicableZuluChargeDate(chargeTO);
				}
			}
		}
		return getApplicableZuluChargeDate(chargeTO);
	}

	private String getChargeGroupCode(BasicChargeTO chargeTO) {
		String chargeGroupCode;
		if (chargeTO instanceof FareTO) {
			chargeGroupCode = ChargeGroups.FARE;
		} else if (chargeTO instanceof FeeTO) {
			chargeGroupCode = ChargeGroups.MODIFICATION_CHARGE;
		} else if (chargeTO instanceof SurchargeTO) {
			chargeGroupCode = ChargeGroups.SURCHARGE;
		} else {
			chargeGroupCode = ChargeGroups.TAX;
		}
		return chargeGroupCode;
	}

	private Date getApplicableZuluChargeDate(BasicChargeTO chargeTO) {
		if (chargeTO instanceof FareTO) {
			return ((FareTO) chargeTO).getApplicableDate();
		} else if (chargeTO instanceof FeeTO) {
			return ((FeeTO) chargeTO).getApplicableTime();
		} else if (chargeTO instanceof SurchargeTO) {
			return ((SurchargeTO) chargeTO).getApplicableTime();
		} else {
			return ((TaxTO) chargeTO).getApplicableTime();
		}
	}

}
