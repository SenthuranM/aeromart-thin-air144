package com.isa.thinair.xbe.core.web.handler.customer;


import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.handler.reporting.AgentGSADetailReportRH;

/**
 * @author suneth
 *
 */

public class CustomerRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(AgentGSADetailReportRH.class);
	
	/**
	 * Main Execute Method for Customer Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = S2Constants.Result.SUCCESS;
		
		try {
			setDisplayData(request);
			
		} catch (Exception e) {
			forward = "error";
			
		}
		
		return forward;
	}
	
	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {

		setCountryList(request);
		setNationalityList(request);
		setTitlesVisible(request);

	}

	private static void setCountryList(HttpServletRequest request) throws ModuleException {
		String country = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, country);
	}
	
	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setNationalityList(HttpServletRequest request) throws ModuleException {
		String strNationality = SelectListGenerator.createNationalityList();
		request.setAttribute(WebConstants.REQ_HTML_NATIONALITY_LIST, strNationality);
	}
	
	protected static void setTitlesVisible(HttpServletRequest request)throws ModuleException{
		String strTitlesVisible = SelectListGenerator.createPaxTitleVisibleArray();
		request.setAttribute(WebConstants.PAX_TITLES_VISIBLE, strTitlesVisible);		
	}

}



