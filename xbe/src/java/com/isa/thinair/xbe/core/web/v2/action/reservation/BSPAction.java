package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationPaxUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.reservation.ServiceTaxCCParamsDTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class BSPAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(BSPAction.class);

	private String bspFee;
	private String totalPayAmountWithBSPFee;
	private String totalPayAmountWithBSPFeeSelcur;
	private String agentCurrencyCode;
	private PaymentTO payment;
	private boolean success = true;
	private String messageTxt;
	private String groupPNR;
	private String paxPayments;
	private String action;
	

	/** params for apply service tax **/
	private FlightSearchDTO searchParams;
	private String selectedFlightList;
	private String pnr;
	private String paxState;
	private String paxCountryCode;
	private boolean paxTaxRegistered;
	private FlightSearchDTO fareQuoteParams;
	private String resContactInfo;
	private String flightRPHList;
	private String flightSegmentToList;
	private String paxWiseAnci;
	private String nameChangeFlightSegmentList;
	private String resPaxs;
	private boolean addSegment;
	
	enum actions {CREATE, MODIFY, SEGMENT_MODIFY, ADD_SEGMENT, REQUOTE, ADD_MODIFY_ANCI };

	public String calculateBSPFee() {

		String strReturn = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		BigDecimal bspFeeDecimal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPayable = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalServiceTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalBSPFeeWithTotalServiceTax = AccelAeroCalculator.getDefaultBigDecimalZero();	
		

		try {
			logData("BSP==>Action:", action);
			BookingShoppingCart bookingShopingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);		
			clearExternalCharges(bookingShopingCart);
			extractData(bookingShopingCart);
			totalPayable = PaymentUtil.getTotalPayableAmount(request, payment);			
					
			int payableAdultChildPaxCount = getPayableAdultChildCount(bookingShopingCart);
			int payableInfantPaxCount =  getPayableInfantCount(bookingShopingCart, payableAdultChildPaxCount);		
			int totalPayablePaxCount =  payableAdultChildPaxCount + payableInfantPaxCount;	
			
			ExternalChgDTO bspExternalChgDTO = bookingShopingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.BSP_FEE);
			bspExternalChgDTO.calculateAmount(totalPayablePaxCount);
			bspFeeDecimal = bspExternalChgDTO.getAmount();
			bspExternalChgDTO.setAmount(bspFeeDecimal);			
			bookingShopingCart.addSelectedExternalCharge(bspExternalChgDTO);
			totalServiceTax = getTotalServiceTax(bookingShopingCart, bspFeeDecimal, payableAdultChildPaxCount, payableInfantPaxCount);

			totalPayable = AccelAeroCalculator.add(totalPayable, bspFeeDecimal, totalServiceTax);
			setAgentCurrencyCode(bookingShopingCart);			
			totalBSPFeeWithTotalServiceTax = AccelAeroCalculator.add(bspFeeDecimal, totalServiceTax);
			
			bspFee = AccelAeroCalculator.formatAsDecimal(totalBSPFeeWithTotalServiceTax);
			totalPayAmountWithBSPFee = AccelAeroCalculator.formatAsDecimal(totalPayable);
			totalPayAmountWithBSPFeeSelcur = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
					bookingShopingCart.getCurrencyExchangeRate().getMultiplyingExchangeRate(), totalPayable));


		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}
		return strReturn;
	}
	
	private void logData(String message,Object data) {		
		if (log.isDebugEnabled()) {
			log.debug(message + data);
		}		
	}
	
	private int getTotalPayablePaxCount() throws ParseException {
		
		int totalPayablePaxCount = 0;
		JSONArray jsonPaxPayArray =  (JSONArray) new JSONParser().parse(paxPayments);		
		Iterator<?> itIter = jsonPaxPayArray.iterator();
		
		while (itIter.hasNext()) {
			
			JSONObject jPPObj = (JSONObject) itIter.next();
			
			if (new BigDecimal((String) jPPObj.get("displayToPay")).compareTo(BigDecimal.ZERO) > 0) {				
					totalPayablePaxCount++;			
			}
		}
		
		logData("Total Payable Pax Count :" , totalPayablePaxCount);		
		
		return totalPayablePaxCount;
		
	} 
	
	
	private boolean paxHasPayment(ReservationPaxTO pax, Map<Integer, BigDecimal> paxCreditMap) {
		BigDecimal paxCredit = null;
		if (paxCreditMap != null) {
			paxCredit = paxCreditMap.get(pax.getSeqNumber());
		}
		if (paxCredit == null) {
			paxCredit = BigDecimal.ZERO;
		}
		BigDecimal totalPaxPayment = BigDecimal.ZERO;
		totalPaxPayment = AccelAeroCalculator.add(pax.getAncillaryTotal(true), paxCredit.negate());

		if (totalPaxPayment.compareTo(BigDecimal.ZERO) <= 0) {
			return false;
		} else {
			return true;
		}
	}
	
	private int getPayableAdultChildCount(BookingShoppingCart bookingShopingCart) {
		
		ReservationBalanceTO reservationBalance = bookingShopingCart.getReservationBalance();		
		int payableAdultChildPaxCount = 0;
		
		if (action.equals(actions.CREATE.name())) {
			payableAdultChildPaxCount = bookingShopingCart.getPayablePaxCount();
		} else {
		
			if (reservationBalance != null && 	reservationBalance.getPassengerSummaryList() != null) {
				payableAdultChildPaxCount = ReservationPaxUtil.getPayablePaxCount(reservationBalance.getPassengerSummaryList());
			}  else if (!bookingShopingCart.getPaxList().isEmpty()) {
				// Crete flow, Adding ancilary, Add infant
				XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
						S2Constants.Session_Data.XBE_SES_RESDATA);			
				Map<Integer, BigDecimal> passengerPayments = resInfo.getPaxCreditMap();	
				
				int paxWithPaymentCount = 0;
				for (ReservationPaxTO pax : bookingShopingCart.getPaxList()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType()) && paxHasPayment(pax, passengerPayments)) {
						paxWithPaymentCount++;
					}
				}
				
				payableAdultChildPaxCount = paxWithPaymentCount;
			}
		}
		
		logData("PayableAdultChildPaxCount :" , payableAdultChildPaxCount);		
		
		return payableAdultChildPaxCount;
		
	}
	
	/*
	private Set<Integer> getPayablePassengerSequence() {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);	
		Set<Integer> paxSequences = new HashSet();
		Map<Integer, BigDecimal> passengerPayments = resInfo.getPaxCreditMap();
		
		if (passengerPayments != null) {
			passengerPayments.forEach((key,value) -> {
					if (AccelAeroCalculator.isLessThan(value, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						paxSequences.add(key);
					}
			});		
			
		}	
		
		return paxSequences;
	} */
	
	private Set<Integer> getPayablePassengerSequenceFromRequestData() throws ParseException {
			
		Set<Integer> paxSequences = new HashSet();
		JSONArray jsonPaxPayArray =  (JSONArray) new JSONParser().parse(paxPayments);	
		Iterator<?> itIter = jsonPaxPayArray.iterator();
		
		while (itIter.hasNext()) {
			
			JSONObject jPPObj = (JSONObject) itIter.next();	
			
			if (new BigDecimal((String) jPPObj.get("displayToPay")).compareTo(BigDecimal.ZERO) == 1) {
				paxSequences.add(new Integer(BeanUtils.nullHandler(jPPObj.get("displayPaxID"))));
			}			
			
		}		
		
		logData("Pax sequence data from request :", paxSequences );
		
		return paxSequences;
	}
	
	
	private int getPayableInfantCount(BookingShoppingCart bookingShopingCart, int totalPayableAdultChildCount) throws ParseException {
		
		ReservationBalanceTO reservationBalance = bookingShopingCart.getReservationBalance();		
		int payableInfantPaxCount = 0;
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);	
		
		if (action.equals(actions.CREATE.name())) {
			payableInfantPaxCount = bookingShopingCart.getPayableInfantPaxCount();
		}  else  {
		
			if (reservationBalance != null &&
					reservationBalance.getPassengerSummaryList() != null) {
				payableInfantPaxCount = ReservationPaxUtil.getPayableInfantCount(reservationBalance.getPassengerSummaryList(), 
						resInfo.isInfantPaymentSeparated());
			} else if (!bookingShopingCart.getPaxList().isEmpty()) {				
				    //Add infant							
					Map<Integer, BigDecimal> passengerPayments = resInfo.getPaxCreditMap();	
					
					int paxWithPaymentCount = 0;
					for (ReservationPaxTO pax : bookingShopingCart.getPaxList()) {
						if (PaxTypeTO.INFANT.equals(pax.getPaxType()) && paxHasPayment(pax, passengerPayments)) {
							paxWithPaymentCount++;
						}
					}		
					
					if (actions.ADD_MODIFY_ANCI.name().equals(action)) {
						// Add/modify Ancilary
						if (paxWithPaymentCount == 0 && passengerPayments != null) {						
							int totalInfantCount = passengerPayments.size() - totalPayableAdultChildCount;
							
							for (int infantCount = 1; infantCount <= totalInfantCount; infantCount++  ) {
								BigDecimal amountDue = passengerPayments.get(totalPayableAdultChildCount + infantCount);
								
								if(amountDue != null && AccelAeroCalculator.isGreaterThan(amountDue.negate(), AccelAeroCalculator.getDefaultBigDecimalZero())) {
									paxWithPaymentCount++;
								}
							}
						}
					}
					
					payableInfantPaxCount = paxWithPaymentCount;
				
			}
		}
		
		logData("PayableInfantPaxCount :" , payableInfantPaxCount);		
		
		return payableInfantPaxCount;
		
	} 
	
	private void clearExternalCharges(BookingShoppingCart bookingShopingCart) throws Exception {			
		bookingShopingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.BSP_FEE);	
		/*if (paxWiseAnci != null && !paxWiseAnci.equals("")) {
			List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPaxBasicInfo(paxWiseAnci,
					ApplicationEngine.XBE);
			if (paxList != null && !paxList.isEmpty()) {
				bookingShopingCart.setPaxList(paxList);
			}
		} */
	}
	
	private static Integer getInfantWith(LCCClientReservationPax lccResPax) {
		if (lccResPax.getPaxType().equals("IN") && lccResPax.getParent() != null) {
			return lccResPax.getParent().getPaxSequence();
		}
		return null;
	}
	
	private void extractData(BookingShoppingCart bookingShopingCart) throws Exception {
		
		if (actions.MODIFY.name().equals(action) && resPaxs != null) {
			
			Collection<LCCClientReservationPax> lccResPaxList = ReservationUtil.transformJsonPassengers(resPaxs.replaceAll(
					"'", "\\\\'"));

			List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();

			for (LCCClientReservationPax lccResPax : lccResPaxList) {
				ReservationPaxTO resPaxTO = new ReservationPaxTO();
				resPaxTO.setPaxType(lccResPax.getPaxType());
				resPaxTO.setSeqNumber(lccResPax.getPaxSequence());
				resPaxTO.setIsParent((lccResPax.getInfants() != null && !lccResPax.getInfants().isEmpty()));
				resPaxTO.setTravelerRefNumber(lccResPax.getTravelerRefNumber());
				resPaxTO.setInfantWith(getInfantWith(lccResPax));
				paxList.add(resPaxTO);
			}

			if (lccResPaxList != null && !lccResPaxList.isEmpty()) {
				bookingShopingCart.setPaxList(paxList);
			}
		} else if (paxWiseAnci != null && !paxWiseAnci.equals("")) {
			List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPaxBasicInfo(paxWiseAnci,
					ApplicationEngine.XBE);
			if (paxList != null && !paxList.isEmpty()) {
				bookingShopingCart.setPaxList(paxList);
			}
		} 
		
	}
	
	
	

	private BigDecimal getTotalServiceTax(BookingShoppingCart bookingShopingCart, BigDecimal bspFee, int payableAdultPaxCount,int payableInfantPaxCount ) throws Exception {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);

		ServiceTaxCCParamsDTO serviceTaxCCParamsDTO = new ServiceTaxCCParamsDTO(searchParams, fareQuoteParams, paxWiseAnci, resPaxs,
				resContactInfo, paxState, paxCountryCode, paxTaxRegistered, flightRPHList, selectedFlightList,
				flightSegmentToList, nameChangeFlightSegmentList,addSegment, groupPNR);

		return ReservationUtil.calculateServiceTaxForTransactionFee(bookingShopingCart, bspFee, resInfo.getTransactionId(),
				serviceTaxCCParamsDTO, getTrackInfo(), EXTERNAL_CHARGES.BSP_FEE, payableAdultPaxCount, 
				payableInfantPaxCount, getPayablePassengerSequenceFromRequestData(), false);
	}


	private void setAgentCurrencyCode(BookingShoppingCart bookingShopingCart) {
		agentCurrencyCode = "";
		if (bookingShopingCart.getCurrencyExchangeRate() != null) {
			agentCurrencyCode = bookingShopingCart.getCurrencyExchangeRate().getCurrency().getCurrencyCode();
		}
	}


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getBspFee() {
		return bspFee;
	}

	public void setBspFee(String bspFee) {
		this.bspFee = bspFee;
	}

	public String getTotalPayAmountWithBSPFee() {
		return totalPayAmountWithBSPFee;
	}

	public void setTotalPayAmountWithBSPFee(String totalPayAmountWithBSPFee) {
		this.totalPayAmountWithBSPFee = totalPayAmountWithBSPFee;
	}

	public String getTotalPayAmountWithBSPFeeSelcur() {
		return totalPayAmountWithBSPFeeSelcur;
	}

	public void setTotalPayAmountWithBSPFeeSelcur(String totalPayAmountWithBSPFeeSelcur) {
		this.totalPayAmountWithBSPFeeSelcur = totalPayAmountWithBSPFeeSelcur;
	}

	public String getAgentCurrencyCode() {
		return agentCurrencyCode;
	}

	public void setAgentCurrencyCode(String agentCurrencyCode) {
		this.agentCurrencyCode = agentCurrencyCode;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public String getSelectedFlightList() {
		return selectedFlightList;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaxState() {
		return paxState;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public String getPaxCountryCode() {
		return paxCountryCode;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public boolean isPaxTaxRegistered() {
		return paxTaxRegistered;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public String getResContactInfo() {
		return resContactInfo;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public String getFlightSegmentToList() {
		return flightSegmentToList;
	}

	public void setFlightSegmentToList(String flightSegmentToList) {
		this.flightSegmentToList = flightSegmentToList;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}


	public String getPaxPayments() {
		return paxPayments;
	}


	public void setPaxPayments(String paxPayments) {
		this.paxPayments = paxPayments;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getNameChangeFlightSegmentList() {
		return nameChangeFlightSegmentList;
	}

	public void setNameChangeFlightSegmentList(String nameChangeFlightSegmentList) {
		this.nameChangeFlightSegmentList = nameChangeFlightSegmentList;
	}

	public String getResPaxs() {
		return resPaxs;
	}

	public void setResPaxs(String resPaxs) {
		this.resPaxs = resPaxs;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}		

}
