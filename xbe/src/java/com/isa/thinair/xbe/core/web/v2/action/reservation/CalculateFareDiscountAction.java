package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareDiscountTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.FareTypeInfoTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AvailabilityUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Calculater Fare Discount
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class CalculateFareDiscountAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(CalculateFareDiscountAction.class);

	private FlightSearchDTO fareQuoteParams;

	private boolean success = true;

	private String messageTxt;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private FareTypeInfoTO selectedFare;

	private boolean currencyRound = false;

	private String groupPNR;

	private ReservationProcessParams rpParams;

	private Integer fareDiscountPercentage;

	private String fareDiscountNotes;

	private String fareDiscountCode;

	private DiscountedFareDetails appliedFareDiscount = null;

	private boolean overrideFareDiscount = false;

	public String execute() {

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			boolean viewFlightsWithLesserSeats = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_ADD_FARE_DISCOUNTS);
			overrideFareDiscount = BasicRH.hasPrivilege(request, PriviledgeConstants.OVERRIDE_FARE_DISCOUNT);
			FlightPriceRQ flightPriceRQ = null;
			boolean requoteNewFlow = false;
			if (fareQuoteParams.getOndList() != null && fareQuoteParams.getOndList().size() > 0) {
				flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, AppIndicatorEnum.APP_XBE);
				requoteNewFlow = true;
			} else {
				flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, outFlightRPHList, retFlightRPHList);
			}

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());
			flightPriceRQ.getAvailPreferences().setRestrictionLevel(
					AvailabilityConvertUtil.getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats,
							fareQuoteParams.getBookingType()));

			String pnr = request.getParameter("pnr");
			if (pnr != null && !"".equals(pnr)) {
				flightPriceRQ.getAvailPreferences().setModifyBooking(true);
				ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"),
						request.getParameter("modifySegment"));
			}

			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			flightPriceRQ.getAvailPreferences().setSearchSystem(
					AvailabilityUtil.getSearchSystem(flightPriceRQ.getOriginDestinationInformationList(), getTrackInfo(), pnr,
							isGroupPNR, true));

			if (log.isDebugEnabled()) {
				if (flightPriceRQ.getTransactionIdentifier() != null) {
					log.debug("Fare quote" + flightPriceRQ.getTransactionIdentifier());
				}
			}

			flightPriceRQ.getAvailPreferences().setTravelAgentCode(fareQuoteParams.getTravelAgentCode());

			currencyRound = CommonUtil.enableCurrencyRounding(fareQuoteParams.getSelectedCurrency());

			if (fareDiscountPercentage != null) {

				PriceInfoTO priceInfoTO = bookingShoppingCart.getPriceInfoTO();

				if (priceInfoTO.getFareTypeTO() != null && priceInfoTO.getFareTypeTO().getFareDiscountInfo() != null) {
					FareDiscountTO fareDiscountTO = priceInfoTO.getFareTypeTO().getFareDiscountInfo();

					if (fareDiscountTO.isFareDiscountAvailable()) {
						BasicRH.checkPrivilege(request, PriviledgeConstants.ALLOW_ADD_FARE_DISCOUNTS);

						if ((fareDiscountPercentage >= 0 && fareDiscountPercentage <= 100)
								&& fareDiscountPercentage > fareDiscountTO.getFareDiscountMax()
								|| fareDiscountPercentage < fareDiscountTO.getFareDiscountMin()) {
							BasicRH.checkPrivilege(request, PriviledgeConstants.OVERRIDE_FARE_DISCOUNT);
						}
					} else {
						throw new ModuleRuntimeException("Invalid Fare Discount");
					}

				}

				bookingShoppingCart.setFareDiscount(fareDiscountPercentage, fareDiscountNotes, fareDiscountCode, flightPriceRQ
						.getTravelerInfoSummary().getPassengerTypeQuantityList(), false);
				if(fareDiscountPercentage==0){
					//remove fare discount
					bookingShoppingCart.removeFareDiscount();
				}else if (bookingShoppingCart.getFareDiscount() != null
						&& !PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(bookingShoppingCart
								.getFareDiscount().getPromotionType())) {
					ReservationUtil.calculateDiscountForReservation(bookingShoppingCart, bookingShoppingCart.getPaxList(),
							flightPriceRQ, getTrackInfo(), true, bookingShoppingCart.getFareDiscount(), priceInfoTO,
							flightPriceRQ.getTransactionIdentifier());
				}
				

				if (priceInfoTO != null) {
					// handling charge is not considered for modifications
					ReservationProcessParams resParm = new ReservationProcessParams(request,
							resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
					BigDecimal handlingCharge = BigDecimal.ZERO;
					List<String> ondCodeList = new ArrayList<String>();
					List<Set<String>> ondSegCodes = new ArrayList<Set<String>>();
					Map<String, Long> segmentDeptTimeMap = new HashMap<String, Long>();
					BigDecimal fareDiscountAmt = null;
					fareDiscountAmt = bookingShoppingCart.getTotalFareDiscount(true);

					if (fareDiscountAmt.doubleValue() > 0) {
						appliedFareDiscount = bookingShoppingCart.getFareDiscount();
					}

					handlingCharge = bookingShoppingCart.getHandlingCharge();
					if (requoteNewFlow) {
						int ondSequence = 0;
						for (OriginDestinationInformationTO ondInfoTO : flightPriceRQ.getOriginDestinationInformationList()) {
							for (OriginDestinationOptionTO ondOptionTO : ondInfoTO.getOrignDestinationOptions()) {
								ondCodeList.add(ondSequence, ondOptionTO.getOndCode());
								for (FlightSegmentTO fltSeg : ondOptionTO.getFlightSegmentList()) {
									if (ondSegCodes.size() == ondSequence) {
										ondSegCodes.add(ondSequence, new HashSet<String>());
									}
									ondSegCodes.get(ondSequence).add(fltSeg.getSegmentCode());
									segmentDeptTimeMap.put(fltSeg.getSegmentCode(), FlightRefNumberUtil
											.getDepartureDateFromFlightRPHString(fltSeg.getFlightRefNumber()).getTime());

								}
								break; // only one ond option could come
							}
							ondSequence++;
						}

						if (fareQuoteParams.getOpenReturn() && ondCodeList.size() == 1) {
							String arr[] = ondCodeList.get(0).split("/");
							StringBuilder sbInbound = new StringBuilder();
							for (int i = arr.length - 1; i >= 0; i--) {
								if (sbInbound.length() > 0) {
									sbInbound.append("/");
								}
								sbInbound.append(arr[i]);
							}
							ondCodeList.add(ondSequence, sbInbound.toString());
							Set<String> inboundSegs = new HashSet<String>();
							inboundSegs.add(sbInbound.toString());
							ondSegCodes.add(ondSequence, inboundSegs);
						}

					} else {
						Object[] resArr = getSelectedSegmentInfo(outFlightRPHList, retFlightRPHList);
						segmentDeptTimeMap = (Map<String, Long>) resArr[0];
						Set<String> selectedOBSegCodes = (Set<String>) resArr[1];
						Set<String> selectedIBSegCodes = (Set<String>) resArr[2];
						String selectedOBOndCode = (String) resArr[3];
						String selectedIBOndCode = (String) resArr[4];

						if (fareQuoteParams.getOpenReturn() && selectedIBOndCode == null) {
							String arr[] = selectedOBOndCode.split("/");
							StringBuilder sb = new StringBuilder();
							for (int i = arr.length - 1; i >= 0; i--) {
								if (sb.length() > 0) {
									sb.append("/");
								}
								sb.append(arr[i]);
							}
							selectedIBOndCode = sb.toString();
						}

						ondCodeList.add(selectedOBOndCode);
						ondSegCodes.add(selectedOBSegCodes);
						if (selectedIBOndCode != null) {
							ondCodeList.add(selectedIBOndCode);
							ondSegCodes.add(selectedIBSegCodes);
						}

					}
					ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
					selectedFare = ReservationUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(), segmentDeptTimeMap,
							handlingCharge, fareQuoteParams, ondCodeList, ondSegCodes, resParm, fareDiscountAmt,
							exchangeRateProxy, null, null, null, null);

				}

				// Hide Stop Over Airport
				if (AppSysParamsUtil.isHideStopOverEnabled()) {
					if (selectedFare != null) {
						getHideStopOverAirportForFareQuote(selectedFare);
					}
				}
			} else {
				messageTxt = "Invalid fare discount";
				success = false;
			}

		} catch (ModuleException me) {
			log.error(me.getMessage(), me);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(me,userLanguage);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			clearValues();
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	private Object[] getSelectedSegmentInfo(List<String> outFlightRPHList, List<String> inFlightRPHList) {
		Set<String> obSegCodes = new HashSet<String>();
		Set<String> ibSegCodes = new HashSet<String>();
		String obOndCode = null;
		String ibOndCode = null;
		String lastAirportCode = null;
		Map<String, Long> segmentDeptTimeMap = new HashMap<String, Long>();
		if (outFlightRPHList != null) {
			StringBuilder sb = new StringBuilder();
			for (String flightRefNumber : outFlightRPHList) {
				String fltRPH = null;
				if (flightRefNumber.indexOf("#") != -1) {
					String arr[] = flightRefNumber.split("#");
					fltRPH = arr[0];
				} else {
					fltRPH = flightRefNumber;
				}
				String segCode = FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRPH);
				obSegCodes.add(segCode);
				segmentDeptTimeMap.put(segCode, FlightRefNumberUtil.getDepartureDateFromFlightRPHString(fltRPH).getTime());

				if (sb.length() == 0) {
					sb.append(segCode);
				} else {
					if (lastAirportCode != null
							&& lastAirportCode.compareToIgnoreCase(segCode.substring(0, segCode.indexOf("/"))) == 0) {
						sb.append("/" + segCode.substring(segCode.indexOf("/") + 1));
					} else {
						sb.append("/" + segCode);
					}

				}
				lastAirportCode = segCode.substring(segCode.lastIndexOf("/") + 1);
				obOndCode = sb.toString();
			}
		}

		if (inFlightRPHList != null) {
			StringBuilder sb = new StringBuilder();
			for (String flightRefNumber : inFlightRPHList) {
				String fltRPH = null;
				if (flightRefNumber.indexOf("#") != -1) {
					String arr[] = flightRefNumber.split("#");
					fltRPH = arr[0];
				} else {
					fltRPH = flightRefNumber;
				}
				String segCode = FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRPH);
				ibSegCodes.add(segCode);
				segmentDeptTimeMap.put(segCode, FlightRefNumberUtil.getDepartureDateFromFlightRPHString(fltRPH).getTime());

				if (sb.length() == 0) {
					sb.append(segCode);
				} else {
					if (lastAirportCode != null
							&& lastAirportCode.compareToIgnoreCase(segCode.substring(0, segCode.indexOf("/"))) == 0) {
						sb.append("/" + segCode.substring(segCode.indexOf("/") + 1));
					} else {
						sb.append("/" + segCode);
					}

				}
				lastAirportCode = segCode.substring(segCode.lastIndexOf("/") + 1);
				ibOndCode = sb.toString();
			}
		}
		return new Object[] { segmentDeptTimeMap, obSegCodes, ibSegCodes, obOndCode, ibOndCode };
	}

	private void getHideStopOverAirportForFareQuote(FareTypeInfoTO fareType) {
		Collection<FareQuoteTO> FareQuoteTOs = fareType.getFareQuoteTO();
		for (FareQuoteTO fareQuoteTo : FareQuoteTOs) {
			String ondCode = fareQuoteTo.getOndCode();
			if (ondCode != null && ondCode.split("/").length > 2) {
				fareQuoteTo.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode), false);
			}
			Collection<SegmentFareTO> segmentWiseFares = fareQuoteTo.getSegmentWise();
			for (SegmentFareTO segmentFareTo : segmentWiseFares) {
				String fareSegment = segmentFareTo.getSegmentCode();
				segmentFareTo.setSegmentCode(ReservationApiUtils.hideStopOverSeg(fareSegment));
			}
		}

	}

	private void clearValues() {
		currencyRound = false;
		outFlightRPHList = null;
		retFlightRPHList = null;
		selectedFare = null;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public boolean isCurrencyRound() {
		return currencyRound;
	}

	public FareTypeInfoTO getSelectedFare() {
		return selectedFare;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public ReservationProcessParams getRpParams() {
		return rpParams;
	}

	public void setRpParams(ReservationProcessParams rpParams) {
		this.rpParams = rpParams;
	}

	public void setFareDiscountPercentage(Integer fareDiscountPercentage) {
		this.fareDiscountPercentage = fareDiscountPercentage;
	}

	public void setFareDiscountNotes(String fareDiscountNotes) {
		this.fareDiscountNotes = fareDiscountNotes;
	}

	public void setFareDiscountCode(String fareDiscountCode) {
		this.fareDiscountCode = fareDiscountCode;
	}

	public DiscountedFareDetails getAppliedFareDiscount() {
		return appliedFareDiscount;
	}

	public void setAppliedFareDiscount(DiscountedFareDetails appliedFareDiscount) {
		this.appliedFareDiscount = appliedFareDiscount;
	}

	public void setOverrideFareDiscount(boolean overrideFareDiscount) {
		this.overrideFareDiscount = overrideFareDiscount;
	}

	public boolean isOverrideFareDiscount() {
		return overrideFareDiscount;
	}
}