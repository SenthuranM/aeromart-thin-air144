//$Id$
package com.isa.thinair.xbe.core.web.generator.system;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class PasswordChangeHG {

	private static String clientErrors;

	public static String getClientErrors(HttpServletRequest request) {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			// error messages

			moduleErrs.setProperty("cc_null", "XBE-ERR-01");
			moduleErrs.setProperty("cc_no_child_only_resrvations", "TAIR-90144");
			moduleErrs.setProperty("cc_compare", "XBE-ERR-02");
			moduleErrs.setProperty("cc_min_length", "XBE-ERR-13");
			moduleErrs.setProperty("cc_max_length", "XBE-ERR-70"); // password maximum length constraint
			moduleErrs.setProperty("cc_not_equal", "XBE-ERR-14");
			moduleErrs.setProperty("cc_sys_down", "XBE-ERR-33");
			moduleErrs.setProperty("cc_paaaword_change_restart", "XBE-ERR-35");
			moduleErrs.setProperty("cc_paaaword_invalidComb", "XBE-ERR-50");

			// Info messages
			moduleErrs.setProperty("cc_user_password_reset", "TAIR-90179");
			moduleErrs.setProperty("cc_user_password_expire", "TAIR-90180");

			// confirmation messages
			moduleErrs.setProperty("cc_success_msg", "XBE-CONF-01");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

}
