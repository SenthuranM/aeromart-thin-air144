/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Dhanushka
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.xbe.core.web.generator.gds;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.criteria.GDSMessageSearchCriteria;
import com.isa.thinair.gdsservices.api.model.GDSMessage;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class GdsMessagesHTMLGenerator {

	private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat formatterFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private static String clientErrors;

	// searching parameters
	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_SRC_GDS_NAME = "selGDSType";
	private static final String PARAM_SRC_MSG_TYPE = "selMSGType";
	private static final String PARAM_SRC_PROCESS_STATUS = "selProcessStatus";
	private static final String PARAM_SRC_COM_REF = "txtCommReference";
	private static final String PARAM_SRC_FROM_DATE = "txtFromDate";
	private static final String PARAM_SRC_TO_DATE = "txtToDate";
	private static final String PARAM_SRC_REQ_XML = "txtReqXml";
	private static final String PARAM_SRC_RES_XML = "txtResXml";
	private static final String PARAM_SRC_GDS_PNR = "txtGDSPnr";
	private static final String PARAM_SRC_MSG_BROKER = "txtMsgBrokerRef";
	private static final String PARAM_GDS_MSG_ID = "hdnGdsMsgID";

	private String strFormFieldsVariablesJS = "";

	public final String getGDSMessagesRowHtml(HttpServletRequest request, Properties properties) throws ModuleException,
			Exception {

		String strUIModeJS = "var isSearchMode = false;";
		Page page = null;
		int recNo = 0;
		int totRec = 0;
		Collection thruChkCol = null;

		StringBuilder searchData = new StringBuilder("var arrSearchData = new Array();");

		if (properties.getProperty(PARAM_UI_MODE).equals(WebConstants.ACTION_SEARCH)) {

			strUIModeJS = "var isSearchMode = true;";

			searchData
					.append("arrSearchData['" + PARAM_SRC_GDS_NAME + "']='" + properties.getProperty(PARAM_SRC_GDS_NAME) + "';");
			searchData
					.append("arrSearchData['" + PARAM_SRC_MSG_TYPE + "']='" + properties.getProperty(PARAM_SRC_MSG_TYPE) + "';");
			searchData.append("arrSearchData['" + PARAM_SRC_PROCESS_STATUS + "']='"
					+ properties.getProperty(PARAM_SRC_PROCESS_STATUS) + "';");
			searchData.append("arrSearchData['" + PARAM_SRC_COM_REF + "']='" + properties.getProperty(PARAM_SRC_COM_REF) + "';");
			searchData.append("arrSearchData['" + PARAM_SRC_FROM_DATE + "']='" + properties.getProperty(PARAM_SRC_FROM_DATE)
					+ "';");
			searchData.append("arrSearchData['" + PARAM_SRC_TO_DATE + "']='" + properties.getProperty(PARAM_SRC_TO_DATE) + "';");
			searchData.append("arrSearchData['" + PARAM_SRC_REQ_XML + "']='" + properties.getProperty(PARAM_SRC_REQ_XML) + "';");
			searchData.append("arrSearchData['" + PARAM_SRC_RES_XML + "']='" + properties.getProperty(PARAM_SRC_RES_XML) + "';");
			searchData.append("arrSearchData['" + PARAM_SRC_GDS_PNR + "']='" + properties.getProperty(PARAM_SRC_GDS_PNR) + "';");
			searchData.append("arrSearchData['" + PARAM_SRC_MSG_BROKER + "']='" + properties.getProperty(PARAM_SRC_MSG_BROKER)
					+ "';");

			GDSMessageSearchCriteria criteria = new GDSMessageSearchCriteria();

			if (!"".equals(properties.getProperty(PARAM_SRC_COM_REF))) {
				criteria.setCommunicationReference(properties.getProperty(PARAM_SRC_COM_REF));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_GDS_NAME))) {
				criteria.setGds(properties.getProperty(PARAM_SRC_GDS_NAME));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_FROM_DATE))) {
				criteria.setMessageReceivedDateFrom(formatter.parse(properties.getProperty(PARAM_SRC_FROM_DATE)));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_TO_DATE))) {
				criteria.setMessageReceivedDateTo(formatter.parse(properties.getProperty(PARAM_SRC_TO_DATE)));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_MSG_TYPE))) {
				criteria.setMessageType(properties.getProperty(PARAM_SRC_MSG_TYPE));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_PROCESS_STATUS))) {
				criteria.setProcessStatus(properties.getProperty(PARAM_SRC_PROCESS_STATUS));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_REQ_XML))) {
				criteria.setRequestXML(properties.getProperty(PARAM_SRC_REQ_XML));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_RES_XML))) {
				criteria.setResponseXML(properties.getProperty(PARAM_SRC_RES_XML));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_GDS_PNR))) {
				criteria.setGdsPnr(properties.getProperty(PARAM_SRC_GDS_PNR));
			}

			if (!"".equals(properties.getProperty(PARAM_SRC_MSG_BROKER))) {
				criteria.setMessageBrokerReferenceId(properties.getProperty(PARAM_SRC_MSG_BROKER));
			}

			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			if (request.getParameter("hdnRecNo") != null && !"".equals(request.getParameter("hdnRecNo"))) {
				recNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;
			}

			page = ModuleServiceLocator.getGDSServicesBD().searchGDSMessages(criteria, recNo, 20);

			// get the page data
			if (page != null) {
				thruChkCol = page.getPageData();
				totRec = page.getTotalNoOfRecords();

			}
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		}

		setFormFieldValues(searchData.toString());
		return createGDSMessagesRowHTML(thruChkCol);
	}

	private String createGDSMessagesRowHTML(Collection listGDSMessages) throws ModuleException {

		List list = (List) listGDSMessages;

		Object[] listArr = list.toArray();
		StringBuilder sb = new StringBuilder("var arrGDSData = new Array();");
		GDSMessage gdsMessage = null;

		for (int i = 0; i < listArr.length; i++) {
			gdsMessage = (GDSMessage) listArr[i];

			sb.append("arrGDSData[" + i + "] = new Array();");
			sb.append("arrGDSData[" + i + "][0] = " + i + ";");
			sb.append("arrGDSData[" + i + "][1] = '" + (gdsMessage.getMessageType() == null ? "" : gdsMessage.getMessageType())
					+ "';");
			sb.append("arrGDSData[" + i + "][2] = '" + (gdsMessage.getGds() == null ? "" : gdsMessage.getGds()) + "';");

			if (gdsMessage.getMessageReceivedDate() != null) {

				sb.append("arrGDSData[" + i + "][3] = '" + formatterFull.format(gdsMessage.getMessageReceivedDate()) + "';");
			} else {
				sb.append("arrGDSData[" + i + "][3] = '';");
			}

			sb.append("arrGDSData[" + i + "][4] = '"
					+ (gdsMessage.getCommunicationReference() == null ? "" : gdsMessage.getCommunicationReference()) + "';");
			sb.append("arrGDSData[" + i + "][5] = '"
					+ (gdsMessage.getProcessStatus() == null ? "" : gdsMessage.getProcessStatus()) + "';");
			sb.append("arrGDSData[" + i + "][6] = '"
					+ (gdsMessage.getOriginatorAddress() == null ? "" : gdsMessage.getOriginatorAddress()) + "';");
			sb.append("arrGDSData[" + i + "][7] = '"
					+ (gdsMessage.getResponderAddress() == null ? "" : gdsMessage.getResponderAddress()) + "';");
			sb.append("arrGDSData[" + i + "][8] = '"
					+ (gdsMessage.getUniqueAccelAeroRequestId() == null ? "" : gdsMessage.getUniqueAccelAeroRequestId()) + "';");
			sb.append("arrGDSData[" + i + "][9] = '"
					+ (gdsMessage.getProcessStatusHistory() == null ? "" : gdsMessage.getProcessStatusHistory()) + "';");
			sb.append("arrGDSData[" + i + "][10] = '"
					+ (gdsMessage.getProcessDescription() == null ? "" : gdsMessage.getProcessDescription()) + "';");
			sb.append("arrGDSData[" + i + "][11] = " + gdsMessage.getMessageId() + ";");
			sb.append("arrGDSData[" + i + "][12] = " + gdsMessage.getVersion() + ";");
			sb.append("arrGDSData[" + i + "][13] = '" + (gdsMessage.getGdsPnr() == null ? "" : gdsMessage.getGdsPnr()) + "';");
			sb.append("arrGDSData[" + i + "][14] = '"
					+ (gdsMessage.getGdsAgentCode() == null ? "" : gdsMessage.getGdsAgentCode()) + "';");
			sb.append("arrGDSData[" + i + "][15] = '" + (gdsMessage.getGdsUserId() == null ? "" : gdsMessage.getGdsUserId())
					+ "';");
			sb.append("arrGDSData[" + i + "][16] = '"
					+ (gdsMessage.getMessageBrokerReferenceId() == null ? "" : gdsMessage.getMessageBrokerReferenceId()) + "';");
		}

		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("cc.resend.pnl.sendpnladl.confirmation", "reSendPNLRecoredCfrm");
			moduleErrs.setProperty("cc.send.new.pnl.sendpnladl.confirmation", "sendnewPNLRecoredCfrm");
			moduleErrs.setProperty("cc.resend.adl.sendpnladl.confirmation", "reSendADLRecoredCfrm");
			moduleErrs.setProperty("cc.send.new.adl.sendpnladl.confirmation", "sendnewADLRecoredCfrm");
			moduleErrs.setProperty("cc.reprint.pnl.sendpnladl.confirmation", "rePrintPNLRecoredCfrm");
			moduleErrs.setProperty("cc.reprint.adl.sendpnladl.confirmation", "rePrintADLRecoredCfrm");

			moduleErrs.setProperty("cc_flight_invalidate", "invalidDate");

			moduleErrs.setProperty("cc.see.log", "seeLogEntry");

			moduleErrs.setProperty("cc.sendpnladl.form.flight.date.required", "flightDateRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.flight.date.invalid", "flightDateInvalid");
			moduleErrs.setProperty("cc.sendpnladl.form.flight.number.required", "flightNumberRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.airport.required", "airportRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.messagetype.required", "msgTypeRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.mailserver.required", "mailServerRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.sita.address.required", "sitaAddressRqrd");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

	public String getGDSResponseHtml(HttpServletRequest request, Properties properties) throws ModuleException {
		GDSMessage message = null;
		String messXML = null;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		StringBuilder sb = new StringBuilder("var headerMsg = '" + XBEConfig.getClientMessage("um.gds.message.head.response", userLanguage)
				+ "';");

		if (!"".equals(properties.getProperty(PARAM_GDS_MSG_ID))) {
			message = ModuleServiceLocator.getGDSServicesBD().getGDSMessage(
					Long.parseLong(properties.getProperty(PARAM_GDS_MSG_ID)));

			if (message != null) {
				messXML = message.getResponseXML();
			}

			if (messXML != null) {
				messXML = messXML.replace("/", "\\/");
				messXML = messXML.replace('\n', '~');
				messXML = messXML.replace('\r', ' ');
				sb.append("var MsgData = '" + messXML + "';");
			} else {
				sb.append("var MsgData = '';");
			}
		} else {
			sb.append("var MsgData = '';");
		}

		return sb.toString();
	}

	public String getGDSRequestHtml(HttpServletRequest request, Properties properties) throws ModuleException {
		GDSMessage message = null;
		String messXML = null;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		StringBuilder sb = new StringBuilder("var headerMsg = '" + XBEConfig.getClientMessage("um.gds.message.head.request", userLanguage)
				+ "';");

		if (!"".equals(properties.getProperty(PARAM_GDS_MSG_ID))) {
			message = ModuleServiceLocator.getGDSServicesBD().getGDSMessage(
					Long.parseLong(properties.getProperty(PARAM_GDS_MSG_ID)));

			if (message != null) {
				messXML = message.getRequestXML();
			}

			if (messXML != null) {
				messXML = messXML.replace("/", "\\/");
				messXML = messXML.replace('\n', '~');
				messXML = messXML.replace('\r', ' ');
				sb.append("var MsgData = '" + messXML + "';");
			} else {
				sb.append("var MsgData = '';");
			}
		} else {
			sb.append("var MsgData = '';");
		}

		return sb.toString();
	}

}
