package com.isa.thinair.xbe.core.web.action.travagent;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.travagent.AgentCollectionRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.MANAGE_PAYMENT)
public class ShowManagePaymentAction extends BaseRequestAwareAction {

	public String execute() throws ModuleException {
		return AgentCollectionRequestHandler.execute(request);
	}
}
