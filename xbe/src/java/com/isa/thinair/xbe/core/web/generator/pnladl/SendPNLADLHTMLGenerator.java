/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author srikantha
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.xbe.core.web.generator.pnladl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airreservation.api.model.AirportPassengerMessageTxHsitory;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class SendPNLADLHTMLGenerator {

	private static String clientErrors;

	// searching parameters
	private static final String PARAM_SEARCH_AIRPORTCODE = "selAirport";

	private static final String PARAM_SEARCH_FLIGHTDATE = "txtDate";

	private static final String PARAM_SEARCH_FLIGHTNO = "txtFlightNo";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private String strFormFieldsVariablesJS = "";

	public final String getSendPNLADLRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);
		String strUIModeJS = "var isSearchMode = false;";
		String strAirportCode = "";
		String strFromDate = "";
		String strFlightNo = "";
		Date fromDate = null;
		Collection col = null;

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {

			strUIModeJS = "var isSearchMode = true;";

			strAirportCode = request.getParameter(PARAM_SEARCH_AIRPORTCODE);
			strFormFieldsVariablesJS = "var airportCode ='" + strAirportCode + "';";

			strFromDate = request.getParameter(PARAM_SEARCH_FLIGHTDATE);
			strFormFieldsVariablesJS += "var flightDate ='" + strFromDate + "';";

			strFlightNo = request.getParameter(PARAM_SEARCH_FLIGHTNO).trim();
			strFormFieldsVariablesJS += "var flightNo ='" + strFlightNo + "';";

			SimpleDateFormat dateFormat = null;

			if (!strFromDate.equals("")) {
				if (strFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
				}
				fromDate = dateFormat.parse(strFromDate);
			}
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
			col = ModuleServiceLocator.getReservationAuxilliaryBD().getPnlAdlHistory(strFlightNo, fromDate, strAirportCode);
		} else {
			strUIModeJS = "var isSearchMode = false;";
			strFormFieldsVariablesJS = "var airportCode ='';";
			strFormFieldsVariablesJS += "var flightDate ='';";
			strFormFieldsVariablesJS += "var flightNo ='';";
		}

		setFormFieldValues(strFormFieldsVariablesJS);
		return createSendPNLADLRowHTML(col);
	}

	private String covertZuluToLocal(Timestamp timestamp, String strAirport) throws ModuleException {

		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportBD());
		GregorianCalendar calendar = new GregorianCalendar();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		calendar.setTime(timestamp);
		Date date = null;
		date = helper.getLocalDateTime(strAirport, calendar.getTime());

		return formatter.format(date);
	}

	private String createSendPNLADLRowHTML(Collection listPNLADL) throws ModuleException {

		List list = (List) listPNLADL;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrPNLADLData = new Array();");
		AirportPassengerMessageTxHsitory pnlADLHistory = null;

		Timestamp strLastPNLTimestamp = null;
		Timestamp strLastADLTimestamp = null;
		String strPrevMsgType = null;
		String strPNLStatusYes = null;
		String strADLStatusYes = null;

		for (int i = 0; i < listArr.length; i++) {
			pnlADLHistory = (AirportPassengerMessageTxHsitory) listArr[i];

			if (pnlADLHistory.getTransmissionStatus() != null) {
				if ((pnlADLHistory.getTransmissionStatus().equals("Y") || pnlADLHistory.getTransmissionStatus().equals("P"))
						&& pnlADLHistory.getMessageType().equals("PNL")) {
					strPNLStatusYes = "Y";
				}
				if (pnlADLHistory.getTransmissionStatus().equals("Y") && pnlADLHistory.getMessageType().equals("ADL")) {
					strADLStatusYes = "Y";
				}
			}
			if (strPrevMsgType != null && strLastPNLTimestamp != null
					&& strLastPNLTimestamp.getTime() < pnlADLHistory.getTransmissionTimeStamp().getTime()
					&& pnlADLHistory.getMessageType().equals(strPrevMsgType)) {

				if (strLastPNLTimestamp != null && pnlADLHistory.getMessageType().equals("PNL")) {

					strLastPNLTimestamp = pnlADLHistory.getTransmissionTimeStamp();

				} else if (strLastADLTimestamp != null && pnlADLHistory.getMessageType().equals("ADL")) {

					strLastADLTimestamp = pnlADLHistory.getTransmissionTimeStamp();

				}
			} else if (strLastPNLTimestamp == null && pnlADLHistory.getMessageType().equals("PNL")) {

				strLastPNLTimestamp = pnlADLHistory.getTransmissionTimeStamp();

			} else if (strLastADLTimestamp == null && pnlADLHistory.getMessageType().equals("ADL")) {

				strLastADLTimestamp = pnlADLHistory.getTransmissionTimeStamp();

			}

			sb.append("arrPNLADLData[" + i + "] = new Array();");
			if (pnlADLHistory.getAirportCode() != null) {
				sb.append("arrPNLADLData[" + i + "][1] = '" + pnlADLHistory.getAirportCode() + "';");
			} else {
				sb.append("arrPNLADLData[" + i + "][1] = '';");
			}
			if (pnlADLHistory.getMessageType() != null) {
				sb.append("arrPNLADLData[" + i + "][2] = '" + pnlADLHistory.getMessageType() + "';");
				strPrevMsgType = pnlADLHistory.getMessageType();
			} else {
				sb.append("arrPNLADLData[" + i + "][2] = '';");
			}

			if (pnlADLHistory.getTransmissionTimeStamp() != null) {
				sb.append("arrPNLADLData[" + i + "][3] = '" + changeTimestampFormat(pnlADLHistory.getTransmissionTimeStamp())
						+ "';");
			} else {
				sb.append("arrPNLADLData[" + i + "][3] = '';");
			}

			if (pnlADLHistory.getTransmissionStatus() != null) {
				sb.append("arrPNLADLData[" + i + "][4] = '" + pnlADLHistory.getTransmissionStatus() + "';");
			} else {
				sb.append("arrPNLADLData[" + i + "][4] = '';");
			}

			if (pnlADLHistory.getSitaAddress() != null) {
				sb.append("arrPNLADLData[" + i + "][5] = '" + pnlADLHistory.getSitaAddress() + "';");
			} else if (pnlADLHistory.getEmail() != null) {
				sb.append("arrPNLADLData[" + i + "][5] = '" + pnlADLHistory.getEmail() + "';");
			} else {
				sb.append("arrPNLADLData[" + i + "][5] = '';");
			}

			if (strLastADLTimestamp != null) {
				sb.append("arrPNLADLData[" + i + "][6] = '" + strLastADLTimestamp + "';");
			} else {
				sb.append("arrPNLADLData[" + i + "][6] = '';");
			}

			if (strLastPNLTimestamp != null) {
				sb.append("arrPNLADLData[" + i + "][7] = '" + strLastPNLTimestamp + "';");
			} else {
				sb.append("arrPNLADLData[" + i + "][7] = '';");
			}

			if (pnlADLHistory.getTransmissionStatus() != null) {
				if (pnlADLHistory.getTransmissionStatus().equals("N")) {

					sb.append("arrPNLADLData[" + i + "][8] = 'Failed';");
				}

				if (pnlADLHistory.getTransmissionStatus().equals("Y")) {
					sb.append("arrPNLADLData[" + i + "][8] = 'Successful';");
				}

				if (pnlADLHistory.getTransmissionStatus().equals("P")) {
					sb.append("arrPNLADLData[" + i + "][8] = 'Successful';");
				}

				if (pnlADLHistory.getTransmissionStatus().equals("E")) {
					sb.append("arrPNLADLData[" + i + "][8] = 'Empty';");
				}
			} else {
				sb.append("arrPNLADLData[" + i + "][8] = '';");
			}

			sb.append("arrPNLADLData[" + i + "][9] = '" + pnlADLHistory.getFlightNumber() + "';");

			if (pnlADLHistory.getTransmissionTimeStamp() != null) {
				sb.append("arrPNLADLData[" + i + "][10] = '"
						+ covertZuluToLocal(pnlADLHistory.getTransmissionTimeStamp(), pnlADLHistory.getAirportCode()) + "';");
			}
		}

		if (strLastPNLTimestamp != null)
			sb.append("var strLastPNLTimestamp = '" + changeTimestampFormat(strLastPNLTimestamp) + "';");
		else
			sb.append("var strLastPNLTimestamp = '';");
		if (strLastADLTimestamp != null)
			sb.append("var strLastADLTimestamp = '" + changeTimestampFormat(strLastADLTimestamp) + "';");
		else
			sb.append("var strLastADLTimestamp = '';");
		if (strPNLStatusYes != null)
			sb.append("var strPNLStatusYes = '" + strPNLStatusYes + "';");
		else
			sb.append("var strPNLStatusYes = '';");
		if (strADLStatusYes != null)
			sb.append("var strADLStatusYes = '" + strADLStatusYes + "';");
		else
			sb.append("var strADLStatusYes = '';");

		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("cc.resend.pnl.sendpnladl.confirmation", "reSendPNLRecoredCfrm");
			moduleErrs.setProperty("cc.send.new.pnl.sendpnladl.confirmation", "sendnewPNLRecoredCfrm");
			moduleErrs.setProperty("cc.resend.adl.sendpnladl.confirmation", "reSendADLRecoredCfrm");
			moduleErrs.setProperty("cc.send.new.adl.sendpnladl.confirmation", "sendnewADLRecoredCfrm");
			moduleErrs.setProperty("cc.reprint.pnl.sendpnladl.confirmation", "rePrintPNLRecoredCfrm");
			moduleErrs.setProperty("cc.reprint.adl.sendpnladl.confirmation", "rePrintADLRecoredCfrm");

			moduleErrs.setProperty("cc.see.log", "seeLogEntry");

			moduleErrs.setProperty("cc.sendpnladl.form.flight.date.required", "flightDateRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.flight.date.invalid", "flightDateInvalid");
			moduleErrs.setProperty("cc.sendpnladl.form.flight.number.required", "flightNumberRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.airport.required", "airportRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.messagetype.required", "msgTypeRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.mailserver.required", "mailServerRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.sita.address.required", "sitaAddressRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.user.priv", "insufficientPnlAdlPriv");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

	private String changeTimestampFormat(Timestamp time) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dtfmt.format(time);
	}
}
