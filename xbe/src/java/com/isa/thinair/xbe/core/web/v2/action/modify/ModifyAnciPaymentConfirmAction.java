package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.utils.AuthorizationsUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.dto.ReservationConstants;
import com.isa.thinair.webplatform.api.util.ReservationPaxUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentHolder;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * 
 * @author Dilan Anuruddha
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ModifyAnciPaymentConfirmAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ModifyAnciPaymentConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private PaymentTO payment;

	private CreditCardTO creditInfo;

	private String reservationResponse;

	private String agentBalance;

	private String version;

	private String pnr;

	private String groupPNR;

	private String paxPayments;

	/** User selected currency */
	private String hdnSelCurrency;

	private boolean convertedToGroupReservation;

	private String ownerAgentCode;

	private BigDecimal totalAmount;

	private GroupBookingRequestTO groupBookingRequest;

	private String groupBookingReference;

	private boolean forceConfirm;

	private boolean isOhdResExpired;

	private String agentBalanceInAgentCurrency;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					&& resInfo.getExistingReleaseTimestamp().before(CalendarUtil.getCurrentZuluDateTime())
					&& AppSysParamsUtil.isCancelExpiredOhdResEnable()) {
				this.success = false;
				this.messageTxt = "Your on hold reservation has expired. Please reload";
				this.isOhdResExpired = true;
				return S2Constants.Result.SUCCESS;
			}

			CommonReservationContactInfo contactInfo = ReservationUtil
					.transformJsonContactInfo(request.getParameter("resContactInfo"));

			// transform the pnr segments
			Collection<LCCClientReservationSegment> segments = ModifyReservationJSONUtil
					.transformJsonSegments(request.getParameter("pnrSegments"));
			// transform passengers
			Collection<LCCClientReservationPax> colpaxs = ReservationUtil
					.transformJsonPassengers(request.getParameter("resPaxs"));

			this.validate(segments);

			// set tracking info
			TrackInfoDTO trackingfoDTO = this.getTrackInfo();

			PaymentMethod paymentMethod = payment.getPaymentMethod();

			boolean isGroupPnr = ReservationUtil.isGroupPnr(groupPNR);
			SYSTEM system = SYSTEM.INT;
			if (!isGroupPnr) {
				system = SYSTEM.AA;
				groupPNR = pnr;
			}

			Collection<ReservationPaxTO> paxList = bookingShoppingCart.getPaxList();
			Map<Integer, BigDecimal> paxResCreditMap = resInfo.getPaxCreditMap();

			ReservationUtil.updateDepartureDateInfoForAnciFlights(segments, paxList);

			Map<String, BigDecimal> agentAmountMap = new ConcurrentHashMap<String, BigDecimal>();
			if (BookingUtil.isDoubleAgentPay(payment)) {
				agentAmountMap.put(payment.getAgent(), new BigDecimal(payment.getAgentOneAmount()));
				agentAmountMap.put(payment.getAgentTwo(), new BigDecimal(payment.getAgentTwoAmount()));
			}

			int paxWithPaymentCount = 0;
			for (ReservationPaxTO pax : paxList) {
				if (!PaxTypeTO.INFANT.equals(pax.getPaxType()) && paxHasPayment(pax, paxResCreditMap)) {
					paxWithPaymentCount++;
				}
			}

			boolean isForceConfirmed = payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE");
			boolean isCCPayment = (!isForceConfirmed && paymentMethod.getCode().equals(PaymentMethod.CRED.getCode()));

			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {
				isCCPayment = true;
			}

			ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
					bookingShoppingCart.getSelectedExternalCharges(), isCCPayment, false);
			externalChargesMediator.setCalculateJNTaxForCCCharge(true);

			if (!isForceConfirmed && paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
				externalChargesMediator.setBSPPayment(true);
			}

			ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

			LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(paxWithPaymentCount, 0);

			Integer ipgId = null;
			if (request.getParameter(WebConstants.REQ_HDN_IPG_ID) != null
					&& !"".equals(request.getParameter(WebConstants.REQ_HDN_IPG_ID))) {
				ipgId = new Integer(request.getParameter(WebConstants.REQ_HDN_IPG_ID));
			}

			// CommonReservationContactInfo contactInfo
			// =ReservationUtil.transformJsonContactInfo(request.getParameter("resContactInfo"));

			JSONArray jsonPaxPay = (JSONArray) new JSONParser().parse(paxPayments);
			Map<Integer, JSONArray> paxUtilizedCreditMap = new HashMap<Integer, JSONArray>();

			Iterator<?> itIter = jsonPaxPay.iterator();

			while (itIter.hasNext()) {
				JSONObject jPPObj = (JSONObject) itIter.next();
				Integer jPaxSeqNo = new Integer(jPPObj.get("displayPaxID").toString());
				paxUtilizedCreditMap.put(jPaxSeqNo, (JSONArray) jPPObj.get("paxCreditInfo"));

			}
			long requestID = -1;
			if (getGroupBookingReference() != null && !getGroupBookingReference().equals("")) {
				requestID = Long.parseLong(getGroupBookingReference());
				if (requestID > 0) {
					if (totalAmount != null) {
						payment.setAmount(totalAmount.toString());

					} else {
						payment.setAmount(payment.getCcAmount());
					}
				}
			}

			PaymentHolder paymentHolder = new PaymentHolder();
			if (BookingUtil.isDoubleAgentPay(payment)) {
				if (payment.getAgentOneAmount() != null && !"".equals(payment.getAgentOneAmount().trim())) {
					payment.setAmount(payment.getAgentOneAmount());

				}

				PaymentTO second = payment.clone();
				second.setAgent(payment.getAgentTwo());
				second.setAmount(payment.getAgentTwoAmount());

				paymentHolder.addPayment(payment);
				paymentHolder.addPayment(second);

			} else if (creditInfo != null) {
				paymentHolder.addCardPayment(payment, creditInfo);
			} else {
				paymentHolder.addPayment(payment);
			}

			if (bookingShoppingCart.getPayByVoucherInfo() != null) {
				paymentHolder.addVoucherPayment(bookingShoppingCart.getPayByVoucherInfo().getVoucherDTOList());
			}

			CommonAncillaryModifyAssembler anciAssembler = new CommonAncillaryModifyAssembler();
			anciAssembler.setPnr(groupPNR);
			anciAssembler.setVersion(version);
			anciAssembler.setTargetSystem(system);
			anciAssembler.setTransactionIdentifier(null);
			anciAssembler.setTemporyPaymentMap(null);
			anciAssembler.setOwnerAgentCode(ownerAgentCode);
			anciAssembler.setReservationStatus(resInfo.getReservationStatus());
			anciAssembler.setLccSegments(segments);
			anciAssembler.setLccPassengers(colpaxs);
			anciAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));
			anciAssembler.setForceConfirm(forceConfirm);

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			// BigDecimal amountSpend = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (ReservationPaxTO reservationPax : paxList) {
				List<LCCClientExternalChgDTO> extCharges = new ArrayList<LCCClientExternalChgDTO>();
				if (!reservationPax.getPaxType().equals(PaxTypeTO.INFANT) && paxHasPayment(reservationPax, paxResCreditMap)) {
					extCharges = BookingUtil.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
				}
				extCharges.addAll(reservationPax.getExternalCharges());

				BigDecimal addAnciTotal = BookingUtil.getTotalExtCharge(extCharges);
				BigDecimal removeAnciTotal = reservationPax.getToRemoveAncillaryTotal();
				BigDecimal updateAnciTotal = reservationPax.getToUpdateAncillaryTotal();
				BigDecimal paxCredit = paxResCreditMap.get(reservationPax.getSeqNumber());
				if (paxCredit == null) {
					paxCredit = BigDecimal.ZERO;
				}
				BigDecimal amountDue = AccelAeroCalculator.add(addAnciTotal, updateAnciTotal, removeAnciTotal.negate(),
						paxCredit.negate());
				if (amountDue.compareTo(BigDecimal.ZERO) < 0) {
					amountDue = BigDecimal.ZERO;
				}

				LCCClientPaymentAssembler paymentAssembler = createPaymentAssembler(extCharges, reservationPax,
						paxUtilizedCreditMap, amountDue, addAnciTotal, exchangeRateProxy, anciAssembler, requestID, ipgId,
						paymentMethod, paymentHolder);

				if (anciAssembler.getTargetSystem() == SYSTEM.INT) {
					for (LCCClientPaymentInfo clientPaymentInfo : paymentAssembler.getPayments()) {
						clientPaymentInfo.setLccUniqueTnxId(anciAssembler.getLccPaymentRefNumber());
					}
				}
				// Remove due to Modify reservation failed while adding ancillary.
				// CurrencyExchangeRate currencyExchangeRate = bookingShopingCart.getCurrencyExchangeRate();
				// PayCurrencyDTO currencyDTO = new PayCurrencyDTO(currencyExchangeRate.getCurrency().getCurrencyCode(),
				// currencyExchangeRate.getMultiplyingExchangeRate());
				// paymentAssembler.setPayCurrencyDTO(currencyDTO);
				anciAssembler.addPassengerPayment(reservationPax.getSeqNumber(), paymentAssembler);

				// there can be situations where no payments exits but ancillary is added (e.g. no charge ssr)
				List<LCCSelectedSegmentAncillaryDTO> selectedAnci = reservationPax.getSelectedAncillaries();
				anciAssembler.addAncillary(reservationPax.getSeqNumber(), selectedAnci);
				anciAssembler.removeAncillary(reservationPax.getSeqNumber(), reservationPax.getAncillariesToRemove());
				anciAssembler.updateAncillary(reservationPax.getSeqNumber(), reservationPax.getAncillariesToUpdate());

			}

			boolean autoCnxEnabled = false;
			boolean hasBufferTimeAutoCnx = false;
			Map<String, String> mapPrivileges = new HashMap<String, String>();
			if (AppSysParamsUtil.isAutoCancellationEnabled()
					&& (payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE"))
					&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(resInfo.getReservationStatus())) {
				autoCnxEnabled = true;
				mapPrivileges = (Map<String, String>) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
				hasBufferTimeAutoCnx = AuthorizationsUtil.hasPrivilege(mapPrivileges.keySet(),
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALT_RES_AUTO_CNX);
			}

			anciAssembler.setAutoCnxEnabled(autoCnxEnabled);
			anciAssembler.setHasBufferTimeAutoCnx(hasBufferTimeAutoCnx);

			Map<String, Set<String>> fltRefWiseSelectedMeal = AncillaryDTOUtil
					.getFlightReferenceWiseSelectedMeals((List<ReservationPaxTO>) paxList);

			// anciAssembler.
			boolean dummyCarrierReconcileNeeded = ModuleServiceLocator.getAirproxyReservationBD().updateAncillary(anciAssembler,
					AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), contactInfo, true,
					fltRefWiseSelectedMeal, trackingfoDTO);

			// Creating the group pnr and dummy carriers since other carrier pax credit is utilized for balance payment
			if (dummyCarrierReconcileNeeded) {
				ReservationUtil.reconcileDummyCarrierReservation(anciAssembler.getPnr(), trackingfoDTO);
				convertedToGroupReservation = true;
				groupPNR = anciAssembler.getPnr(); // setting the group pnr as we need to load reservation through lcc
			}
			if (requestID > 0) {
				ModuleServiceLocator.getGroupBookingBD().updateGroupBookingStatus(requestID, totalAmount, pnr);

			}

			// to send medical ssr email
			Map<Integer, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<>();
			for (Map.Entry<Integer, ReservationPaxTO> entry : bookingShoppingCart.getPassengerMap().entrySet()) {
				externalCharges.put(entry.getKey(), new ArrayList<>(entry.getValue().getExternalCharges()));
			}
			boolean hasBalanceToPay = payment.getMode().equals("FORCE");
			SSRServicesUtil.sendMedicalSsrEmail(pnr,
					ReservationPaxUtil.toLccClientReservationPax(bookingShoppingCart.getPaxList()), externalCharges, contactInfo,
					hasBalanceToPay);

			// updated agent credit
			agentBalance = CommonUtil.getLoggedInAgentBalance(request);
			agentBalanceInAgentCurrency = CommonUtil.getLoggedInAgentBalanceInAgentCurrency(request);

		} catch (Exception e) {
			success = false;
			reservationResponse = "";
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private LCCClientPaymentAssembler createPaymentAssembler(Collection<LCCClientExternalChgDTO> extCharges,
			ReservationPaxTO reservationPax, Map<Integer, JSONArray> paxUtilizedCreditMap, BigDecimal amountDue,
			BigDecimal addAnciTotal, ExchangeRateProxy exchangeRateProxy, CommonAncillaryModifyAssembler anciAssembler,
			long requestID, Integer ipgId, PaymentMethod paymentMethod, PaymentHolder paymentHolder) throws ModuleException {
		PayCurrencyDTO payCurrencyDTO = null;
		LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
		paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);
		paymentAssembler.setPaxType(com.isa.thinair.webplatform.api.util.CommonUtil.getActualPaxType(reservationPax));

		if (!(payment.getMode().equals("CONFIRM") || payment.getMode().equals("FORCE"))) {
			JSONArray jPCInfo = paxUtilizedCreditMap.get(reservationPax.getSeqNumber());
			boolean paxCreditPaymentExists = false;
			if (jPCInfo != null) {
				Iterator<?> uIter = jPCInfo.iterator();
				while (uIter.hasNext()) {
					JSONObject jUObj = (JSONObject) uIter.next();
					String carrierCode = (String) jUObj.get("carrier");
					Integer debitPaxRefNo = new Integer((String) jUObj.get("paxCreditId"));
					BigDecimal cr = new BigDecimal((String) jUObj.get("usedAmount"));
					amountDue = AccelAeroCalculator.add(amountDue, cr.negate());
					BigDecimal residingCarrierAmount = new BigDecimal((String) jUObj.get("residingCarrierAmount"));
					String pnr = (String) jUObj.get("pnr");
					paymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, cr, exchangeRateProxy.getExecutionDate(),
							residingCarrierAmount, null, null, reservationPax.getSeqNumber(), null, pnr, null);
					paxCreditPaymentExists = true;
				}
			}
			BigDecimal amountToPay = AccelAeroCalculator.add(amountDue, addAnciTotal.negate());
			if (requestID > 0) {
				anciAssembler.setPartialPayment(true);

			}

			if (PaymentUtil.isValidPaymentExist(paxCreditPaymentExists, amountDue, anciAssembler.getReservationStatus())) {

				paymentHolder.consumePayment(amountToPay, amountDue);

				while (paymentHolder.hasNonRecordedConsumedPayments()) {
					String consumedPayRef = paymentHolder.getNextConsumedPaymentRef();
					BigDecimal consumedPayAmount = paymentHolder.getConsumedPaymentAmount(consumedPayRef);
					PaymentTO payment = paymentHolder.getPayment(consumedPayRef);
					CreditCardTO creditInfo = paymentHolder.getCreditCardTO(consumedPayRef);
					VoucherDTO voucherDTO = paymentHolder.getVoucherPayment(consumedPayRef);

					if (payment != null) {
						paymentMethod = payment.getPaymentMethod();
						if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
							payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(getHdnSelCurrency(), exchangeRateProxy);
							paymentAssembler.addCashPayment(consumedPayAmount, payCurrencyDTO,
									exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
									payment.getActualPaymentMethod(), null, null);

						} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
							String loggedInAgenctCode = RequestHandlerUtil.getAgentCode(request);
							payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(getHdnSelCurrency(), loggedInAgenctCode,
									payment.getAgent(), exchangeRateProxy);

							paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount, payCurrencyDTO,
									exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
									payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);

						} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {

							// Override the selected currency with Agent country currency code for BSP
							String loggedInAgenctCode = RequestHandlerUtil.getAgentCode(request);
							// Override the selected currency with Agent country currency code for BSP
							String bspCurrency = ModuleServiceLocator.getCommonServiceBD()
									.getCurrencyCodeForStation(RequestHandlerUtil.getAgentStationCode(request));
							payCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgenctCode, payment.getAgent(),
									exchangeRateProxy);

							paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount, payCurrencyDTO,
									exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
									payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_BSP, null);

						} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
							payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(getHdnSelCurrency(), ipgId,
									exchangeRateProxy, false);
							IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
									payCurrencyDTO.getPayCurrencyCode());

							UserInputDTO userInputDTO = new UserInputDTO();
							userInputDTO.setBuyerAddress1(creditInfo.getBillingAddress1());
							userInputDTO.setBuyerAddress2(creditInfo.getBillingAddress2());
							userInputDTO.setBuyerCity(creditInfo.getCity());
							userInputDTO.setPostalCode(creditInfo.getPostalCode());
							userInputDTO.setBuyerState(creditInfo.getState());

							paymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType().trim()).intValue(),
									creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
									creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(),
									creditInfo.getCardCVV().trim(), consumedPayAmount, AppIndicatorEnum.APP_XBE,
									TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, payCurrencyDTO,
									exchangeRateProxy.getExecutionDate(), creditInfo.getCardHoldersName(),
									payment.getPaymentReference(), payment.getActualPaymentMethod(), null, userInputDTO);
						} else if (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
							payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(getHdnSelCurrency(), ipgId,
									exchangeRateProxy, false);
							IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
									payCurrencyDTO.getPayCurrencyCode());

							Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(payment.getAgent());
							Set<String> agentcolMethod = null;
							if (agent != null) {
								agentcolMethod = agent.getPaymentMethod();
							}

							if (validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_OFFLINE)) {
								paymentAssembler.addOfflineCardPayment(consumedPayAmount, TnxModeEnum.MAIL_TP_ORDER,
										ipgIdentificationParamsDTO, payment.getPaymentReference(),
										payment.getActualPaymentMethod(), payCurrencyDTO);
							}
						}
					} else if (voucherDTO != null) {
						voucherDTO.setRedeemdAmount(consumedPayAmount.toString());
						paymentAssembler.addVoucherPayment(voucherDTO, payCurrencyDTO, exchangeRateProxy.getExecutionDate());
					}
				}

			}
		}
		return paymentAssembler;
	}

	public String groupBookingValidate() {
		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			long requestID = Long.parseLong(getGroupBookingReference());
			Collection<LCCClientReservationPax> colPaxes = ReservationUtil
					.transformJsonPassengers(request.getParameter("resPaxs"));
			Collection<LCCClientReservationSegment> segments = ModifyReservationJSONUtil
					.transformJsonSegments(request.getParameter("pnrSegments"));
			int adultCount = 0, childCount = 0, infantCount = 0;
			for (LCCClientReservationPax reservationPax : colPaxes) {

				if (reservationPax.getPaxType().equals("AD")) {
					adultCount++;
				} else if (reservationPax.getPaxType().equals("CH")) {
					childCount++;
				} else {
					infantCount++;
				}
			}
			String paxCountStr = adultCount + "|" + childCount + "|" + infantCount;
			ArrayList<String> flightList = new ArrayList<String>();
			for (LCCClientReservationSegment reservationSeg : segments) {
				flightList.add(reservationSeg.getFlightSegmentRefNumber());
			}

			setGroupBookingRequest(ModuleServiceLocator.getGroupBookingBD().validateModifyResGroupBookingRequest(requestID,
					getTotalAmount(), paxCountStr, flightList, user.getAgentCode()));

			// If the agreed fare is set as a percentage then calculate it accordingly
			if (groupBookingRequest != null && groupBookingRequest.isAgreedFarePercentage()) {
				groupBookingRequest.setMinPaymentRequired(groupBookingRequest.getAgreedFare().divide(new BigDecimal("100"))
						.multiply(groupBookingRequest.getMinPaymentRequired()));
			}
		} catch (Exception e) {
			log.error("Error validating the group booking ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @param bookingShopingCart
	 * @return
	 */
	private void validate(Collection<LCCClientReservationSegment> segments) throws ModuleException {

		PaymentMethod paymentMethod = payment.getPaymentMethod();
		if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
			if (ReservationUtil.getNoOfCnfSegments(segments) > ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT) {
				XBERuntimeException ex = new XBERuntimeException("bsp.payment.segment.count.exceeded");
				List<String> messageParameters = new ArrayList<String>();
				messageParameters.add(ReservationConstants.MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT + "");
				ex.setMessageParameters(messageParameters);
				throw ex;
			}
		}
	}

	private boolean paxHasPayment(ReservationPaxTO pax, Map<Integer, BigDecimal> paxCreditMap) {
		BigDecimal paxCredit = null;
		if (paxCreditMap != null) {
			paxCredit = paxCreditMap.get(pax.getSeqNumber());
		}
		if (paxCredit == null) {
			paxCredit = BigDecimal.ZERO;
		}
		BigDecimal totalPaxPayment = BigDecimal.ZERO;
		totalPaxPayment = AccelAeroCalculator.add(pax.getAncillaryTotal(true), paxCredit.negate());

		if (totalPaxPayment.compareTo(BigDecimal.ZERO) <= 0) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean validatePayMethod(Set<String> agentcolMethod, String payMethod) {
		boolean validated = false;
		if (agentcolMethod == null || payMethod == null) {
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		} else {
			for (String agPay : agentcolMethod) {
				if (agPay.equals(payMethod)) {
					validated = true;
					break;
				}
			}
		}
		if (!validated) {
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		}
		return validated;

	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(
				payment != null ? payment.getAgent() : ((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public CreditCardTO getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(CreditCardTO creditInfo) {
		this.creditInfo = creditInfo;
	}

	public String getReservationResponse() {
		return reservationResponse;
	}

	public void setReservationResponse(String reservationResponse) {
		this.reservationResponse = reservationResponse;
	}

	public String getAgentBalance() {
		return agentBalance;
	}

	/**
	 * @return the hdnSelCurrency
	 */
	public String getHdnSelCurrency() {
		return hdnSelCurrency;
	}

	/**
	 * @param hdnSelCurrency
	 *            the hdnSelCurrency to set
	 */
	public void setHdnSelCurrency(String hdnSelCurrency) {
		this.hdnSelCurrency = hdnSelCurrency;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPaxPayments() {
		return paxPayments;
	}

	public void setPaxPayments(String paxPayments) {
		this.paxPayments = paxPayments;
	}

	/**
	 * @return the convertedToGroupReservation
	 */
	public boolean isConvertedToGroupReservation() {
		return convertedToGroupReservation;
	}

	/**
	 * @param convertedToGroupReservation
	 *            the convertedToGroupReservation to set
	 */
	public void setConvertedToGroupReservation(boolean convertedToGroupReservation) {
		this.convertedToGroupReservation = convertedToGroupReservation;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public GroupBookingRequestTO getGroupBookingRequest() {
		return groupBookingRequest;
	}

	public void setGroupBookingRequest(GroupBookingRequestTO groupBookingRequest) {
		this.groupBookingRequest = groupBookingRequest;
	}

	public String getGroupBookingReference() {
		return groupBookingReference;
	}

	public void setGroupBookingReference(String groupBookingReference) {
		this.groupBookingReference = groupBookingReference;
	}

	public boolean isForceConfirm() {
		return forceConfirm;
	}

	public void setForceConfirm(boolean forceConfirm) {
		this.forceConfirm = forceConfirm;
	}

	public boolean isOhdResExpired() {
		return isOhdResExpired;
	}

	public void setOhdResExpired(boolean isOhdResExpired) {
		this.isOhdResExpired = isOhdResExpired;
	}

	public String getPnr() {
		return pnr;
	}

	public String getAgentBalanceInAgentCurrency() {
		return agentBalanceInAgentCurrency;
	}

	public void setAgentBalanceInAgentCurrency(String agentBalanceInAgentCurrency) {
		this.agentBalanceInAgentCurrency = agentBalanceInAgentCurrency;
	}
}