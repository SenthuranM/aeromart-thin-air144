package com.isa.thinair.xbe.core.web.util;

import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.com.bytecode.opencsv.CSVReader;

import com.google.common.base.CharMatcher;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.api.dto.v2.InfantPaxTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;

public class PassengerDetailParsingUtil {
	private static Log log = LogFactory.getLog(PassengerDetailParsingUtil.class);

	/**
	 * the key holding the value for parse failing line in the result map.
	 */
	public static final String PARSE_FAILING_LINE = "parse_failing_line";

	/**
	 * the key holding the value for parse failing reason in the result map.
	 */
	public static final String PARSE_FAILING_REASON = "parse_failing_reason";

	/**
	 * the key holding the adult list in the result map.
	 */
	public static final String ADULT_LIST = "infant_list";

	/**
	 * the key holding the children list in the result map.
	 */
	public static final String CHILDREN_LIST = "children_list";

	/**
	 * the key holding the infant list the result map.
	 */
	public static final String INFANT_LIST = "adult_list";

	private static final String FORMAT_TYPE_DATE = "Date";

	private static final String FORMAT_TYPE_String = "String";
	
	private static final String FORMAT_TYPE_TIME = "Time";

	private char seperator = ',';
	private char quoteCharacter = '\"';

	// Starting line is 0
	private int dataStartingLine = 1;

	// Default format for adults
	private String adultFormat[] = { "displayAdultType", "displayAdultTitle", "displayAdultFirstName", "displayAdultLastName",
			"displayAdultNationality", "displayAdultDOB", "displayPnrPaxCatFOIDNumber", "displayPnrPaxCatFOIDExpiry",
			"displayPnrPaxCatFOIDPlace", "displaypnrPaxArrivalFlightNumber", "displaypnrPaxFltArrivalDate",
			"displaypnrPaxArrivalTime", "displaypnrPaxDepartureFlightNumber", "displaypnrPaxFltDepartureDate",
			"displaypnrPaxDepartureTime", "displayPnrPaxGroupId" };

	// default format for infants
	private String infantFormat[] = { "displayInfantType", "displayInfantFirstName", "displayInfantLastName",
			"displayInfantNationality", "displayInfantDOB", "displayInfantTravellingWith", "displayPnrPaxCatFOIDNumber",
			"displayPnrPaxCatFOIDExpiry", "displayPnrPaxCatFOIDPlace", "displaypnrPaxArrivalFlightNumber",
			"displaypnrPaxFltArrivalDate", "displaypnrPaxArrivalTime", "displaypnrPaxDepartureFlightNumber",
			"displaypnrPaxFltDepartureDate", "displaypnrPaxDepartureTime", "displayPnrPaxGroupId" };

	private final Map<String, String> adultConfig;
	private final Map<String, String> infantConfig;

	private final SimpleDateFormat dateFormat;
	
	private final String TIME24HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";

	public PassengerDetailParsingUtil() {
		/*
		 * Override defaults with configurations if they exist.
		 */
		adultConfig = XBEModuleUtils.getConfig().getAdultPaxDetailsFormat();
		infantConfig = XBEModuleUtils.getConfig().getInfantPaxDetailsFormat();

		if (adultConfig != null) {
			adultFormat = adultConfig.keySet().toArray(adultFormat);
		}
		if (infantConfig != null) {
			infantFormat = infantConfig.keySet().toArray(infantFormat);
		}
		String sformat = "dd/MM/yyyy";
		dateFormat = new SimpleDateFormat(sformat);
		
	}

	/**
	 * Parses the file and returns a Map with results
	 * 
	 * @param file
	 *            The csv file with passenger details to be parsed.
	 * @return A Map containing results of parsing. If parsing succeeds without any errors it will return a Map with
	 *         adultList<AdultPaxTO> (ADULT_LIST),childList<AdultPaxTO>(CHILDREN_LIST) and infantList<InfantPaxTO>
	 *         (INFANT_LIST). If parsing fails the Map returned will contain the following information only line the
	 *         parsing failed(PARSE_FAILING_LINE), the reason parsing failed or the general
	 *         exception(PARSE_FAILING_REASON). These two results are mutually exclusive, if one others will not be
	 *         present in the result map.
	 */
	public Map<String, Object> parsePassengerDetails(File file) {
		Map<String, Object> parseResultMap = new HashMap<String, Object>();
		List<AdultPaxTO> adultList = new ArrayList<AdultPaxTO>();
		List<AdultPaxTO> childList = new ArrayList<AdultPaxTO>();
		List<InfantPaxTO> infantList = new ArrayList<InfantPaxTO>();

		int currentLineNumber = dataStartingLine;

		try {
			CSVReader reader = new CSVReader(new FileReader(file), seperator, quoteCharacter, dataStartingLine);
			String[] line;
			while ((line = reader.readNext()) != null) {
				currentLineNumber++;
				checkForNonAsciiCharacters(line);

				if (isOfPaxType(PaxTypeTO.ADULT, line)) {
					adultList.add(bindBean(line, adultFormat, AdultPaxTO.class));
				} else if (isOfPaxType(PaxTypeTO.CHILD, line)) {
					childList.add(bindBean(line, adultFormat, AdultPaxTO.class));
				} else if (isOfPaxType(PaxTypeTO.INFANT, line)) {
					infantList.add(bindBean(line, infantFormat, InfantPaxTO.class));
				} else {
					throw new IllegalArgumentException("Invalid pax type detected, allowed type are AD,CH,IN");
				}
			}
			parseResultMap.put(ADULT_LIST, adultList);
			parseResultMap.put(CHILDREN_LIST, childList);
			parseResultMap.put(INFANT_LIST, infantList);
		} catch (Exception e) {
			parseResultMap.put(PARSE_FAILING_LINE, currentLineNumber);
			parseResultMap.put(PARSE_FAILING_REASON, e.getMessage());
			log.error(e);
		}

		return parseResultMap;
	}

	/*
	 * Checks for non ascii characters in the passed strings, using Guava library. If check fails throws
	 * IllegalArgumentException.
	 */
	private void checkForNonAsciiCharacters(String[] line) throws IllegalArgumentException {
		for (String entry : line) {
			if (!CharMatcher.ASCII.matchesAllOf(entry)) {
				throw new IllegalArgumentException("File contains non ascii characters. Only ascii characters are allowed.");
			}
		}
	}

	/*
	 * Goes through type configuration if enabled in the config file and validate non String values.
	 */
	private void checkForFormattingErrors(String[] line) throws ParseException {
		if (adultConfig != null && (isOfPaxType(PaxTypeTO.ADULT, line) || isOfPaxType(PaxTypeTO.CHILD, line))) {
			for (int i = 0; i < adultFormat.length; i++) {
				if (!FORMAT_TYPE_String.equalsIgnoreCase(adultConfig.get(adultFormat[i]))) {
					validateFormat(line[i], adultConfig.get(adultFormat[i]));
				}
			}
		}
		if (infantConfig != null && isOfPaxType(PaxTypeTO.INFANT, line)) {
			for (int i = 0; i < infantFormat.length; i++) {
				if (!FORMAT_TYPE_String.equalsIgnoreCase(infantConfig.get(infantFormat[i]))) {
					validateFormat(line[i], infantConfig.get(infantFormat[i]));
				}
			}
		}
	}

	/*
	 * Validates a given value according to a format. Currently only supports Date.
	 */
	private void validateFormat(String value, String format) throws ParseException {
		if (value == null || value.isEmpty()) {
			return;
		}
		if (FORMAT_TYPE_DATE.equalsIgnoreCase(format)) {
			try {
				dateFormat.parse(value);
			} catch (ParseException e) {
				throw new ParseException(e.getLocalizedMessage() + "Proper date format : " + dateFormat.toLocalizedPattern(),
						e.getErrorOffset());
			}
		}
		if (FORMAT_TYPE_TIME.equalsIgnoreCase(format)) {
			
				Pattern pattern = Pattern.compile(TIME24HOURS_PATTERN);
				Matcher matcher = pattern.matcher(value);
				if (matcher.matches()) {
					return;
				} else {
					throw new ParseException("Proper time format : HH:mm" , 0);
				}
			
		}
	}

	/*
	 * Checks if the line passed belongs to the pax specified paxType. Checks if the first element of the array is equal
	 * to the paxType.
	 */
	private boolean isOfPaxType(String paxType, String[] line) {
		if (paxType.equalsIgnoreCase(line[0])) {
			return true;
		}
		return false;
	}

	/**
	 * Binds a bean of the specified class with the given data (with Apache Commons BeanUtils help).
	 * 
	 * @param <T>
	 *            Type of the Bean to be binded (populated with the given data.)
	 * @param line
	 *            Array of data to be binded to the bean in order of method names passed.
	 * @param format
	 *            Name of the methods in the order of data passed.
	 * @param cls
	 *            Class of the bean to be binded
	 * @return A new instance of the class specified populated with the given data in the given format.
	 * @throws Exception
	 *             If binding fails for any reason. (Also parse exception may be thrown when checking for formatting
	 *             errors internally)
	 */
	@SuppressWarnings("unchecked")
	private <T> T bindBean(String[] line, String[] format, Class<T> cls) throws Exception {
		if (format.length != line.length) {
			throw new IllegalArgumentException("The line contains only " + line.length + " fields while format expects "
					+ format.length + " number of fields.");
		}

		checkForFormattingErrors(line);

		T adult = cls.newInstance();
		Map<String, Object> properties = BeanUtils.describe(adult);
		for (int i = 0; i < format.length; i++) {
			properties.put(format[i], line[i]);
		}
		BeanUtils.populate(adult, properties);

		return adult;
	}

	/**
	 * Separator to be used to distinguish seperate entries in the file. Default is ;
	 * 
	 * @param seperator
	 */
	public void setSeperator(char seperator) {
		this.seperator = seperator;
	}

	public char getSeperator() {
		return seperator;
	}

	/**
	 * Quote character to be used to ignore separators etc. Default is "
	 * 
	 * @param quoteCharacter
	 */
	public void setQuoteCharacter(char quoteCharacter) {
		this.quoteCharacter = quoteCharacter;
	}

	public char getQuoteCharacter() {
		return quoteCharacter;
	}

	/**
	 * The starting line of data. Lines start from 0. Default is 1 (Will start parsing at the 2nd line).
	 * 
	 * @param dataStartingLine
	 */
	public void setDataStartingLine(int dataStartingLine) {
		this.dataStartingLine = dataStartingLine;
	}

	public int getDataStartingLine() {
		return dataStartingLine;
	}
}