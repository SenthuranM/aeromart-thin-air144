package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class CreateAlertAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(CreateAlertAction.class);

	private String pnr;
	private String jsonGroupPnr;
	private String pnrSegIds;
	private String description;
	private String messageText;
	private boolean success;
	private static XBEConfig xbeConfig = new XBEConfig();

	public String execute() {
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			Collection<Integer> colPnrSegIds = composePnrSegIds();
			// Hard coding the alert type as 2. To differenciate between the pnr generated alert and user generated
			// alert.
			// This will be useful in the future.
			boolean isGroupPNR = ReservationUtil.isGroupPnr(jsonGroupPnr);
			BookingShoppingCart bookingShoppingCart = new BookingShoppingCart(ChargeRateOperationType.MODIFY_ONLY);
			LCCClientReservation lccReservation = null;
			if (isGroupPNR) {
				pnr = jsonGroupPnr;
				lccReservation = ReservationUtil.loadFullLccReservation(pnr, true, null, bookingShoppingCart, request,
						true, "", "", null, AppSysParamsUtil.isPromoCodeEnabled(), getTrackInfo());
			}

			ServiceResponce sr = ModuleServiceLocator.getResSegmentBD().createAlert(pnr, colPnrSegIds, description,
					lccReservation, getTrackInfo());

			messageText = xbeConfig.getMessage(WebConstants.KEY_ADD_SUCCESS, userLanguage);
			success = true;
		} catch (Exception e) {
			success = false;
			messageText = BasicRH.getErrorMessage(e, userLanguage);
			log.error(e.getMessage());
		}
		return S2Constants.Result.SUCCESS;
	}

	private Collection<Integer> composePnrSegIds() {
		String rowPnrSegIds = BeanUtils.nullHandler(getPnrSegIds());
		Collection<Integer> composePnrSegIds = new ArrayList<Integer>();

		if (rowPnrSegIds.length() > 0) {
			String[] selectedSegs = rowPnrSegIds.split(",");
			for (int i = 0; i < selectedSegs.length; i++) {
				String exactPnrSegId = selectedSegs[i].split("\\$")[0];
				composePnrSegIds.add(new Integer(exactPnrSegId));
			}
		}

		return composePnrSegIds;
	}

	/**
	 * @return the pnrSegIds
	 */
	public String getPnrSegIds() {
		return pnrSegIds;
	}

	/**
	 * @param pnrSegIds
	 *            the pnrSegIds to set
	 */
	public void setPnrSegIds(String pnrSegIds) {
		this.pnrSegIds = pnrSegIds;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the messageText
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * @param messageText
	 *            the messageText to set
	 */
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getJsonGroupPnr() {
		return jsonGroupPnr;
	}

	public void setJsonGroupPnr(String jsonGroupPnr) {
		this.jsonGroupPnr = jsonGroupPnr;
	}

}
