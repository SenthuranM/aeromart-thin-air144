package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.RefundInfoTO;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class to handle group passenger refunds.
 * 
 * @author Thihara (Some of the functionality is copied from PaxAccountPaymentConfirmAction.java)
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class GroupPaxRefundAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(PaxAccountPaymentConfirmAction.class);

	/** Operation success status indicator */
	private boolean success = true;

	/** Success/Failure message for UI display */
	private String messageTxt;

	/** Passenger refund amount */
	private String txtPayAmount;

	/** Selected pax's payments tnx id sTo be used for refund */
	private List<String> payRef;

	/** User notes for refund */
	private String txtPayUN;

	private String selAgent; // {CarrierCode-AgentCode} Empty when refund to own
								// account

	/** Mode of refund */
	private String radPaymentType;

	/** Credit card type */
	private String selCardType;

	/** Credit card holder name */
	private String cardHoldersName;

	/** Credit card number */
	private String cardNo;

	/** Credit card expiry date */
	private String cardExpiryDate;

	/** Credit card security code */
	private String cardCVV;

	private String cccdId;

	private PaymentTO payment;

	private CreditCardTO creditInfo;

	private String agentBalance;

	private String groupPNR;

	private String pnr;

	private boolean convertedToGroupReservation;

	/** User selected currency */
	private String hdnSelCurrency;

	/** Payment Gateway ID */
	private String hdnIPGId;

	private String tempPayId;

	private String version;

	private String lastUserNote;

	private String resONDs;

	public String execute() {
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			// Privilege check
			if (!BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_GROUP_REFUND)) {
				throw new SecurityException("Insufficient Privileges To Commence Group Refund");
			}

			XBEReservationInfoDTO sesDto = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			validate(sesDto.getAvailableBalance(), sesDto.getOriginAgentCode());

			LCCClientReservation reservation = ReservationUtil.loadMinimumReservation(pnr, isGroupPNR, getTrackInfo());
			CommonReservationContactInfo contactInfo = reservation.getContactInfo();

			// set tracking info
			TrackInfoDTO trackInfoDTO = this.getTrackInfo();

			if (!isGroupPNR) {
				this.groupPNR = pnr;
			}

			List<LCCClientReservationPax> reservationPaxList = getReservationPaxForSelectedPax(reservation);
			if (!reservationPaxList.isEmpty()) {
				BigDecimal refundAmount = new BigDecimal(txtPayAmount);
				CommonReservationAssembler reservationAssembler = getReservationAssembler(groupPNR, reservationPaxList,
						refundAmount);
				reservationAssembler.getLccreservation().setVersion(version);
				reservationAssembler.addContactInfo(lastUserNote, contactInfo, null);

				boolean removeAgentCommission = true; // TODO: populate from F/E

				success = ModuleServiceLocator.getAirproxyPassengerBD().groupPassengerRefund(reservationAssembler, txtPayUN,
						isGroupPNR, trackInfoDTO, null, null, removeAgentCommission, true);

				if (success) {
					messageTxt = "Refund Successful";
				} else {
					messageTxt = "Refund Failed";
				}
			} else {
				success = false;
				messageTxt = "Selected passenger does not belong to the reservation.";
			}

		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
		}

		return S2Constants.Result.SUCCESS;
	}

	private void validate(BigDecimal resBalance, String strOriginAgentCode) throws Exception {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams processParams = new ReservationProcessParams(request,
				resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), false, null,
				null, false, false);
		if (resInfo.getInterlineModificationParams() != null && resInfo.getInterlineModificationParams().size() > 0) {
			processParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
					resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), null);
		}
		if (resBalance.compareTo(new BigDecimal(0)) >= 0) {
			if (!processParams.isAllowRefundNoCredit()) {
				throw new ModuleException("airreservation.modify.refund.no.credits");
			}
		}

		// Temporally disable refund for multiple carrier and allow to only one
		// opCarrier reservations only
		List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(resONDs);
		Set<String> opCarrierList = new HashSet<String>();
		for (LCCClientCarrierOndGroup ondGroup : colONDs) {
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(ondGroup.getSegmentCode())) {
				opCarrierList.add(ondGroup.getCarrierCode());
			}
		}

		if (opCarrierList.size() > 1) {
			throw new ModuleException("airreservation.modify.refund.multiple.carrier.not.allowed");
		}

		PaymentMethod paymentMethod = PaymentMethod.getPaymentType(radPaymentType);

		if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
			if (!processParams.isAllowOnAccountRefund()) {
				throw new ModuleException("airreservation.modify.refund.onaccount.not.allowed");
			} else {
				if (isRefundToOthreAgent(selAgent) && !(processParams.isAllowAnyOnAccountRefund()
						|| processParams.isAllowOnAccountRptRefund() || processParams.isAllowAnyOpCarrierOnAccountRefund())) {
					throw new ModuleException("airreservation.modify.refund.other.onaccount.not.allowed");
				}
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
			if (!processParams.isAllowCashRefund()) {
				throw new ModuleException("airreservation.modify.refund.cash.not.allowed");
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
			if (!processParams.isAllowCreditCardRefund()) {
				throw new ModuleException("airreservation.modify.refund.creditcard.not.allowed");
			} else {

				// TODO: Add logic to identify the Credit Card originally paid
				// with, and if the selected card is not same as the one
				// originally used to pay then check the privilege :
				// processParams.isAllowAnyCreditCardRefund();
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
			if (!processParams.isAllowBspRefund()) {
				throw new ModuleException("airreservation.modify.refund.bsp.not.allowed");
			}
		}

	}

	private CommonReservationAssembler getReservationAssembler(String groupPNR, List<LCCClientReservationPax> reservationPaxList,
			BigDecimal refundAmount) throws ModuleException {

		CommonReservationAssembler reservationAssembler = new CommonReservationAssembler();
		reservationAssembler.getLccreservation().setPNR(groupPNR);

		for (LCCClientReservationPax reservationPax : reservationPaxList) {
			LCCClientPaymentAssembler paymentAssembler = getPaymentAssembler(refundAmount, reservationPax);
			boolean isParent = reservationPax.getInfants().size() > 0 ? true : false;

			validatePaymentWiseCreditAvailableForRefund(reservationPax, refundAmount);

			PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
			String arrivalIntlFltNo = "";
			Date intlFltArrivalDate = null;
			String departureIntlFltNo = "";
			Date intlFltDepartureDate = null;
			String pnrPaxGroupId = "";
			if (reservationPax.getLccClientAdditionPax() != null) {
				paxAdditionalInfoDTO.setPassportNo(reservationPax.getLccClientAdditionPax().getPassportNo());
				paxAdditionalInfoDTO.setPassportExpiry(reservationPax.getLccClientAdditionPax().getPassportExpiry());
				paxAdditionalInfoDTO.setPassportIssuedCntry(reservationPax.getLccClientAdditionPax().getPassportIssuedCntry());
				paxAdditionalInfoDTO.setEmployeeId(reservationPax.getLccClientAdditionPax().getEmployeeId());
				paxAdditionalInfoDTO.setDateOfJoin(reservationPax.getLccClientAdditionPax().getDateOfJoin());
				paxAdditionalInfoDTO.setIdCategory(reservationPax.getLccClientAdditionPax().getIdCategory());
				arrivalIntlFltNo = reservationPax.getLccClientAdditionPax().getArrivalIntlFltNo();
				intlFltArrivalDate = reservationPax.getLccClientAdditionPax().getIntlFltArrivalDate();
				departureIntlFltNo = reservationPax.getLccClientAdditionPax().getDepartureIntlFltNo();
				intlFltDepartureDate = reservationPax.getLccClientAdditionPax().getIntlFltDepartureDate();
				pnrPaxGroupId = reservationPax.getLccClientAdditionPax().getPnrPaxGroupId();

			}
			if (isParent) {

				// Passenger with a Infant - Parent
				reservationAssembler.addParent(reservationPax.getFirstName(), reservationPax.getLastName(),
						reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(),
						reservationPax.getPaxSequence(), reservationPax.getAttachedPaxId(), reservationPax.getTravelerRefNumber(),
						paxAdditionalInfoDTO, reservationPax.getPaxCategory(), paymentAssembler, null, null, null, null,
						arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
			} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxType())) {
				reservationAssembler.addChild(reservationPax.getFirstName(), reservationPax.getLastName(),
						reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(),
						reservationPax.getPaxSequence(), reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO,
						reservationPax.getPaxCategory(), paymentAssembler, null, null, null, null, null, arrivalIntlFltNo,
						intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
			} else {
				// Passenger without a Infant - Single
				reservationAssembler.addSingle(reservationPax.getFirstName(), reservationPax.getLastName(),
						reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(),
						reservationPax.getPaxSequence(), reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO,
						reservationPax.getPaxCategory(), paymentAssembler, null, null, null, null, null, arrivalIntlFltNo,
						intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
			}
		}
		return reservationAssembler;
	}

	private LCCClientPaymentAssembler getPaymentAssembler(BigDecimal refundAmount, LCCClientReservationPax reservationPax)
			throws ModuleException {

		LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();

		Set<String> agentcolMethod = null;
		String selAgentStationCode = null;
		PaymentMethod paymentMethod = PaymentMethod.getPaymentType(radPaymentType);
		if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())
				|| paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(getSelAgent(selAgent));
			if (hdnSelCurrency == null) {
				hdnSelCurrency = agent.getCurrencyCode();
			}
			if (agent != null) {
				agentcolMethod = agent.getPaymentMethod();
				selAgentStationCode = agent.getStationCode();
			}
		} else if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())
				|| paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userPrincipal.getAgentCode());
			if (agent != null) {
				agentcolMethod = agent.getPaymentMethod();
			}
		}

		String loggedInAgentCode = ((UserPrincipal) request.getUserPrincipal()).getAgentCode();
		String loggedInAgentCurrencyCode = ((UserPrincipal) request.getUserPrincipal()).getAgentCurrencyCode();

		for (LCCClientPaymentInfo payment : reservationPax.getLccClientPaymentHolder().getPayments()) {

			RefundInfoTO refundInfo = new RefundInfoTO(payment.getOriginalPayReference(), payment.getLccUniqueTnxId(),
					payment.getTxnDateTime(), payment.getCarrierVisePayments(), payment.getCarrierCode());

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(payment.getTxnDateTime());
			if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
				if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_ONACCOUNT)) {
					PayCurrencyDTO payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(hdnSelCurrency, loggedInAgentCode,
							getSelAgent(selAgent), exchangeRateProxy);
					paymentAssembler.addAgentCreditPayment(getSelAgent(selAgent), refundAmount, payCurrencyDTO, new Date(), null,
							null, Agent.PAYMENT_MODE_ONACCOUNT, refundInfo);
				}

			} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
				if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_BSP)) {
					// Override the selected currency with Agent country
					// currency code for BSP
					if (selAgentStationCode == null) {
						selAgentStationCode = RequestHandlerUtil.getAgentStationCode(request);
					}
					String bspCurrency = ModuleServiceLocator.getCommonServiceBD().getCurrencyCodeForStation(selAgentStationCode);
					PayCurrencyDTO payCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgentCode,
							getSelAgent(selAgent), exchangeRateProxy);
					paymentAssembler.addAgentCreditPayment(getSelAgent(selAgent), refundAmount, payCurrencyDTO, new Date(), null,
							null, Agent.PAYMENT_MODE_BSP, refundInfo);
				}
			} else if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
				if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_CASH)) {
					PayCurrencyDTO payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(loggedInAgentCurrencyCode,
							loggedInAgentCode, loggedInAgentCode, exchangeRateProxy);
					paymentAssembler.addCashPayment(refundAmount, payCurrencyDTO, new Date(), null, null, null, refundInfo);
				}
			} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
				if (BookingUtil.validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_CREDITCARD)) {
					Integer ipgId = 0;
					Integer intCCdId = null;
					if (!BeanUtils.nullHandler(hdnIPGId).equals("")) {
						ipgId = new Integer(hdnIPGId);
					}
					if (!BeanUtils.nullHandler(cccdId).equals("")) {
						intCCdId = new Integer(cccdId);
					}

					PayCurrencyDTO payCurrencyDTO = PaymentUtil
							.getPaymentCurrencyForCardPay(BeanUtils.nullHandler(hdnSelCurrency), ipgId, exchangeRateProxy, false);
					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, hdnSelCurrency);
					ipgIdentificationParamsDTO.setFQIPGConfigurationName(ipgId + "_" + hdnSelCurrency);

					paymentAssembler.addInternalCardPayment(new Integer(BeanUtils.nullHandler(selCardType)),
							BeanUtils.nullHandler(cardExpiryDate), BeanUtils.nullHandler(cardNo),
							BeanUtils.nullHandler(cardHoldersName), null, BeanUtils.nullHandler(cardCVV), refundAmount,
							AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, payCurrencyDTO,
							new Date(), BeanUtils.nullHandler(cardHoldersName), new Integer(BeanUtils.nullHandler(tempPayId)),
							tempPayId, intCCdId, null, null, refundInfo);

				}
			}
		}

		return paymentAssembler;
	}

	/**
	 * Removes the irrelevant details form the passenger list and their payment holders. If a passengers transaction IDs
	 * are not included in the group refund request then that passenger is removed. If some transaction IDs of a
	 * passenger is included then other relevant payment details (transactions) are removed from that passenger
	 * 
	 * @param reservation
	 *            the currently loaded reservation object.
	 * @return List<LCCClientReservationPax> of passengers after irrelevant detail removal.
	 */
	private List<LCCClientReservationPax> getReservationPaxForSelectedPax(LCCClientReservation reservation)
			throws ModuleException {

		String ownCarrier = AppSysParamsUtil.getDefaultCarrierCode();

		for (Iterator<LCCClientReservationPax> reservationPaxIterator = reservation.getPassengers()
				.iterator(); reservationPaxIterator.hasNext();) {

			LCCClientReservationPax reservationPax = reservationPaxIterator.next();
			boolean hasAtLeastOneRelevantPayment = false;
			for (Iterator<LCCClientPaymentInfo> paymentIterator = reservationPax.getLccClientPaymentHolder().getPayments()
					.iterator(); paymentIterator.hasNext();) {
				LCCClientPaymentInfo payment = paymentIterator.next();

				// If payment transaction is not needed remove.
				if (!payRef.contains(payment.getOriginalPayReference())) {
					paymentIterator.remove();
				} else {
					if (!payment.getCarrierCode().equals(ownCarrier)) {
						throw new ModuleException("airreservation.modify.refund.other.carrier.not.allowed");
					}
					hasAtLeastOneRelevantPayment = true;
				}

			}

			/*
			 * If the passenger don't have at least one payment transaction in the selected list then remove the
			 * passenger
			 */
			if (!hasAtLeastOneRelevantPayment) {
				reservationPaxIterator.remove();
			}
		}

		return new ArrayList<LCCClientReservationPax>(reservation.getPassengers());
	}

	/**
	 * get the selected refund agent
	 * 
	 * @param selAgnet
	 * @return
	 */
	private String getSelAgent(String selAgent) {
		if (selAgent != null && !selAgent.isEmpty()) {
			return selAgent;
		} else {
			return ((UserPrincipal) request.getUserPrincipal()).getAgentCode();
		}
	}

	private boolean isRefundToOthreAgent(String selAgnet) {
		if (selAgnet == null || selAgnet.isEmpty()) {
			return false;
		} else {
			return !selAgnet.equals(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		}
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(
				payment != null ? payment.getAgent() : ((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public CreditCardTO getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(CreditCardTO creditInfo) {
		this.creditInfo = creditInfo;
	}

	public String getAgentBalance() {
		return agentBalance;
	}

	// Setter methods for OGNL
	public void setTxtPayAmount(String txtPayAmount) {
		this.txtPayAmount = txtPayAmount;
	}

	public void setTxtPayUN(String txtPayUN) {
		this.txtPayUN = txtPayUN;
	}

	public void setRadPaymentType(String radPaymentType) {
		this.radPaymentType = radPaymentType;
	}

	public void setSelAgent(String selAgent) {
		this.selAgent = selAgent;
	}

	public void setSelCardType(String selCardType) {
		this.selCardType = selCardType;
	}

	public void setCardHoldersName(String cardHoldersName) {
		this.cardHoldersName = cardHoldersName;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}

	public void setCardCVV(String cardCVV) {
		this.cardCVV = cardCVV;
	}

	public String getHdnSelCurrency() {
		return hdnSelCurrency;
	}

	public void setHdnSelCurrency(String hdnSelCurrency) {
		this.hdnSelCurrency = hdnSelCurrency;
	}

	public void setHdnIPGId(String hdnIPGId) {
		this.hdnIPGId = hdnIPGId;
	}

	public void setTempPayId(String tempPayId) {
		this.tempPayId = tempPayId;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setLastUserNote(String lastUserNote) {
		this.lastUserNote = lastUserNote;
	}

	public String getCccdId() {
		return cccdId;
	}

	public void setCccdId(String cccdId) {
		this.cccdId = cccdId;
	}

	/**
	 * @return the convertedToGroupReservation
	 */
	public boolean isConvertedToGroupReservation() {
		return convertedToGroupReservation;
	}

	/**
	 * @param convertedToGroupReservation
	 *            the convertedToGroupReservation to set
	 */
	public void setConvertedToGroupReservation(boolean convertedToGroupReservation) {
		this.convertedToGroupReservation = convertedToGroupReservation;
	}

	@SuppressWarnings("unchecked")
	public void setPayRef(String payRef) throws JSONException, ParseException {

		this.payRef = (List<String>) JSONUtil.deserialize(payRef);
	}

	public void setResONDs(String resONDs) {
		this.resONDs = resONDs;
	}

	private void validatePaymentWiseCreditAvailableForRefund(LCCClientReservationPax reservationPax, BigDecimal refundAmount)
			throws ModuleException {

		if (this.payRef != null && this.payRef.size() > 0 && reservationPax != null
				&& reservationPax.getLccClientPaymentHolder() != null) {

			BigDecimal actualCredit = reservationPax.getTotalActualCredit();
			if (actualCredit != null && actualCredit.doubleValue() <= 0
					&& Math.ceil(refundAmount.doubleValue()) > Math.ceil(actualCredit.abs().doubleValue())) {
				throw new ModuleException("airreservation.modify.refund.exceeds.actual.credit");
			}

			if (reservationPax.getLccClientPaymentHolder().getCredits() == null
					|| reservationPax.getLccClientPaymentHolder().getPayments() == null
					|| reservationPax.getLccClientPaymentHolder().getCredits().size() == 0
					|| reservationPax.getLccClientPaymentHolder().getPayments().size() == 0) {
				throw new ModuleException("airreservation.modify.refund.insuf.credits");
			} else {
				Collection<LCCClientPaymentInfo> payments = reservationPax.getLccClientPaymentHolder().getPayments();
				List<String> paxSelectedPayments = new ArrayList<String>();
				for (LCCClientPaymentInfo payment : payments) {

					for (String payRefStr : payRef) {
						if (payRefStr.equalsIgnoreCase(payment.getOriginalPayReference())) {
							if (!StringUtil.isNullOrEmpty(payment.getLccUniqueTnxId())) {
								paxSelectedPayments.add(payment.getLccUniqueTnxId());
							} else {
								paxSelectedPayments.add(payRefStr);
							}

						}
					}

				}

				if (paxSelectedPayments.size() > 0) {
					Collection<CreditInfoDTO> creditInfoColl = reservationPax.getLccClientPaymentHolder().getCredits();
					BigDecimal availableTotalCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

					boolean creditFound = false;
					for (CreditInfoDTO creditInfoDTO : creditInfoColl) {

						if (CreditInfoDTO.status.AVAILABLE.equals(creditInfoDTO.getEnumStatus())) {
							availableTotalCredit = AccelAeroCalculator.add(availableTotalCredit, creditInfoDTO.getMcAmount());
						}
					}

					BigDecimal requiredTotalCredit = AccelAeroCalculator.multiply(refundAmount, paxSelectedPayments.size());

					// we may have a scenario that other carrier payments might
					// lead do decimal point differences
					// therefore check the ceil value.
					if (Math.ceil(requiredTotalCredit.doubleValue()) <= Math.ceil(availableTotalCredit.doubleValue())) {
						creditFound = true;
					}

					if (!creditFound) {
						throw new ModuleException("airreservation.modify.refund.insuf.credits");
					}

				} else {
					throw new ModuleException("airreservation.modify.refund.insuf.credits");
				}

			}

		}

	}

}