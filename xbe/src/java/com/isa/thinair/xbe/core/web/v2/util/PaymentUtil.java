package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.xbe.api.dto.CurrencyInfoTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaxUsedCreditInfoTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.util.PaymentRHUtil;

public class PaymentUtil {

	private static Log log = LogFactory.getLog(PaymentUtil.class);

	private static BigDecimal getTotalExternalCharges(Collection<ExternalChgDTO> extChgDTOs) {
		BigDecimal total = BigDecimal.ZERO;
		for (ExternalChgDTO chg : extChgDTOs) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	public static Collection<PaymentPassengerTO> createPaymentPaxList(BookingShoppingCart bookingShoppingCart,
			ReservationDiscountDTO resDiscountDTO, String loggedInAgentCode) {

		ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

		Collection<PaymentPassengerTO> paxCol = new ArrayList<PaymentPassengerTO>();

		ExternalChargesMediator externalChargesMediator;

		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, loggedInAgentCode)) {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true, true);
		} else {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false, true);
		}

		LinkedList perPaxExternalCharges = externalChargesMediator
				.getExternalChargesForAFreshPayment(bookingShoppingCart.getPaxList().size(), 0);

		PriceInfoTO priceInfoTO = bookingShoppingCart.getPriceInfoTO();
		DiscountedFareDetails discountedFareDetails = bookingShoppingCart.getFareDiscount();

		boolean isFareDiscountApplied = false;
		boolean isPromoDiscountApplied = false;

		if (discountedFareDetails != null) {
			if (discountedFareDetails.getPromotionId() == null) {
				isFareDiscountApplied = true;
			} else if (!bookingShoppingCart.isSkipPromotionForOnHold()) {
				isPromoDiscountApplied = true;
			}
		}

		for (ReservationPaxTO reservationPax : bookingShoppingCart.getPaxList()) {
			String strInf = "";
			String strChild = "";

			BigDecimal paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal paymentINFAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			Collection extChargesCol = (Collection) perPaxExternalCharges.pop();
			extChargesCol.addAll(getFlexiCharges(reservationPax));
			BigDecimal extCharges = getTotalExternalCharges(extChargesCol);

			paymentAmount = priceInfoTO.getPerPaxPriceInfoTO(reservationPax.getPaxType()).getPassengerPrice().getTotalPrice();
			BigDecimal paxDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (resDiscountDTO != null) {
				PaxDiscountDetailTO paxDiscountDetailTO = resDiscountDTO.getPaxDiscountDetail(reservationPax.getSeqNumber());
				if (paxDiscountDetailTO != null) {
					paxDiscountAmount = paxDiscountDetailTO.getTotalDiscount();
				}
			}

			if (reservationPax.getIsParent()) {
				strInf = "/INF";
				paymentINFAmount = priceInfoTO.getPerPaxPriceInfoTO(PaxTypeTO.INFANT).getPassengerPrice().getTotalPrice();
			}

			BigDecimal totalAmount = AccelAeroCalculator.add(paymentAmount, paymentINFAmount, extCharges);
			BigDecimal totalToPay = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (isFareDiscountApplied) {
				totalToPay = AccelAeroCalculator.add(totalAmount, paxDiscountAmount.negate());
			} else if (isPromoDiscountApplied) {

				if (PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discountedFareDetails.getDiscountAs())) {
					totalToPay = totalAmount;
				} else {
					totalToPay = (paxDiscountAmount != null)
							? AccelAeroCalculator.add(totalAmount, paxDiscountAmount.negate())
							: totalAmount;
				}
			} else {
				totalToPay = totalAmount;
			}

			if (PaxTypeTO.CHILD.equals(reservationPax.getPaxType())) {
				strChild = "Child. ";
			}

			PaymentPassengerTO paymentPassengerTO = new PaymentPassengerTO();
			paymentPassengerTO.setDisplayPaxID(reservationPax.getSeqNumber() + "");
			paymentPassengerTO.setDisplayPassengerName(strChild
					+ ((reservationPax.getTitle() != null && !"".equals(reservationPax.getTitle().trim()))
							? reservationPax.getTitle() + ". "
							: "")
					+ StringUtil.toInitCap(reservationPax.getFirstName()) + " "
					+ StringUtil.toInitCap(reservationPax.getLastName()) + strInf);
			paymentPassengerTO.setDisplayFirstName(reservationPax.getFirstName());
			paymentPassengerTO.setDisplayLastName(reservationPax.getLastName());

			paymentPassengerTO.setDisplayPaid("0.00");
			paymentPassengerTO.setDisplayOriginalPaid("0.00");

			paymentPassengerTO.setDisplayToPay(AccelAeroCalculator.formatAsDecimal(totalToPay));
			paymentPassengerTO.setDisplayOriginalTobePaid(AccelAeroCalculator.formatAsDecimal(totalToPay));
			paymentPassengerTO.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(totalAmount));
			paymentPassengerTO.setDisplayUsedCredit("0.00");
			paymentPassengerTO.setPaxType(reservationPax.getPaxType());

			paymentPassengerTO.setPaxCreditInfo(new ArrayList<PaxUsedCreditInfoTO>());

			paxCol.add(paymentPassengerTO);
		}
		return paxCol;
	}

	/**
	 * TODO
	 * 
	 * @return
	 * @throws ParseException
	 * @throws ModuleException
	 */
	public static PaymentSummaryTO createPaymentSummary(BookingShoppingCart bookingShoppingCart, String selCurrency,
			ExchangeRateProxy exchangeRateProxy, String pendingCommission, String loggedInAgentCode)
			throws ParseException, ModuleException {

		BigDecimal handlingCharge = bookingShoppingCart.getHandlingCharge();
		handlingCharge = ReservationUtil.addTaxToServiceCharge(bookingShoppingCart, handlingCharge);

		// Ancillary total for the create booking. For the modifications if balance calculation is done this will be
		// zero
		BigDecimal anciTotal = bookingShoppingCart.getAncillaryTotalExternalCharge(true);
		BigDecimal serviceTaxTotal = bookingShoppingCart.getTotalServiceTaxExternalCharge(false);
		BigDecimal totalBookingPrice = BigDecimal.ZERO;

		PaymentSummaryTO paymentSummaryTO = new PaymentSummaryTO();

		BigDecimal balTOPay = BigDecimal.ZERO;

		// Included CC charges to balToPay
		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, loggedInAgentCode)) {
			if (bookingShoppingCart.getReservationBalance() == null) {
				balTOPay = bookingShoppingCart.getTotalPayableIncludingCCCharges();
				totalBookingPrice = bookingShoppingCart.getTotalPriceIncludingCCCharges();
			} else {
				balTOPay = bookingShoppingCart.getTotalPayable();
				totalBookingPrice = bookingShoppingCart.getTotalPrice();
			}
		} else {
			balTOPay = bookingShoppingCart.getTotalPayable();
			totalBookingPrice = bookingShoppingCart.getTotalPrice();
		}

		paymentSummaryTO.setBalanceToPay(AccelAeroCalculator.formatAsDecimal(balTOPay));
		paymentSummaryTO.setCurrency(AppSysParamsUtil.getBaseCurrency());
		paymentSummaryTO.setHandlingCharges(AccelAeroCalculator.formatAsDecimal(handlingCharge));
		paymentSummaryTO
				.setModificationCharges(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getModifyOrCancellationAmount()));

		paymentSummaryTO.setPaid(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getTotalPaidAmount().negate()));
		paymentSummaryTO.setTicketPrice(AccelAeroCalculator.formatAsDecimal(totalBookingPrice));
		paymentSummaryTO.setTotalFare(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getTotalBaseFare()));
		paymentSummaryTO.setTotalAgentCommission(bookingShoppingCart.getTotAgentCommission());
		paymentSummaryTO.setPendingAgentCommission(pendingCommission);

		BigDecimal discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal discountExcludeCredit = bookingShoppingCart.getTotalFareDiscount(true);

		if (discountExcludeCredit == null || discountExcludeCredit.doubleValue() == 0) {
			BigDecimal creditDiscount = bookingShoppingCart.getTotalFareDiscount(false);
			if (creditDiscount != null && creditDiscount.doubleValue() > 0) {
				discountAmount = creditDiscount;
				paymentSummaryTO.setCreditDiscount(true);
			}
		} else {
			discountAmount = discountExcludeCredit;
		}

		paymentSummaryTO.setTotalDiscount(AccelAeroCalculator.formatAsDecimal(discountAmount));

		BigDecimal nonrefunds = BigDecimal.ZERO;
		if (bookingShoppingCart.getReservationBalance() != null
				&& bookingShoppingCart.getReservationBalance().getSegmentSummary() != null) {
			nonrefunds = bookingShoppingCart.getReservationBalance().getSegmentSummary().getCurrentNonRefunds();
		}

		// Included CC charge to TotalTaxSurCharges and JN_OTHER (if there)
		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, loggedInAgentCode)) {
			paymentSummaryTO.setTotalTaxSurCharges(
					AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(anciTotal, bookingShoppingCart.getTotalTax(),
							bookingShoppingCart.getTotalSurcharges(), nonrefunds, bookingShoppingCart.getExtFeeAmount(),
							bookingShoppingCart.getSelectedExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD).getAmount(),
							ReservationUtil.getApplicableServiceTaxForCCCharge(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER))));

		} else {
			paymentSummaryTO.setTotalTaxSurCharges(
					AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(anciTotal, bookingShoppingCart.getTotalTax(),
							bookingShoppingCart.getTotalSurcharges(), nonrefunds, bookingShoppingCart.getExtFeeAmount())));
		}
		paymentSummaryTO.setTotalServiceTax(AccelAeroCalculator.formatAsDecimal(serviceTaxTotal));
		paymentSummaryTO.setSelectedCurrency(selCurrency);
		CurrencyExchangeRate currencyExchangeRate = null;
		if (bookingShoppingCart.getSelectedCurrencyTimestamp() == null || bookingShoppingCart.getCurrencyExchangeRate() == null) {
			currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency, ApplicationEngine.XBE);
			bookingShoppingCart.setCurrencyExchangeRate(currencyExchangeRate);
		} else {
			currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();
		}
		Currency currency = currencyExchangeRate.getCurrency();

		paymentSummaryTO.setBalanceToPaySelcur(AccelAeroCalculator
				.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
						balTOPay, currency.getBoundryValue(), currency.getBreakPoint())));

		return paymentSummaryTO;
	}

	/**
	 * TODO
	 * 
	 * @return
	 * @throws ParseException
	 * @throws ModuleException
	 */
	public static PaymentSummaryTO createPaymentSummaryAddSegment(BookingShoppingCart bookingShoppingCart, String selCurrency,
			Collection<LCCClientReservationPax> colpaxs, ExchangeRateProxy exchangeRateProxy, String pendingCommission,
			String loggedInAgentCode) throws ParseException, ModuleException {

		BigDecimal handlingCharge = bookingShoppingCart.getHandlingCharge();

		// Ancillary total for the create booking. For the modifications if balance calculation is done this will be
		// zero
		BigDecimal anciTotal = bookingShoppingCart.getAncillaryTotalExternalCharge(true);

		BigDecimal totalBookingPrice = bookingShoppingCart.getTotalPrice();

		PaymentSummaryTO paymentSummaryTO = new PaymentSummaryTO();

		BigDecimal balTOPay = BigDecimal.ZERO;

		// Included CC charges to balToPay
		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, loggedInAgentCode)) {
			balTOPay = bookingShoppingCart.getTotalPayableIncludingCCCharges();
		} else {
			balTOPay = bookingShoppingCart.getTotalPayable();
		}

		paymentSummaryTO.setBalanceToPay(AccelAeroCalculator.formatAsDecimal(balTOPay));
		paymentSummaryTO.setCurrency(AppSysParamsUtil.getBaseCurrency());
		paymentSummaryTO.setHandlingCharges(AccelAeroCalculator.formatAsDecimal(handlingCharge));
		paymentSummaryTO
				.setModificationCharges(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getModifyOrCancellationAmount()));

		paymentSummaryTO.setPaid(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getTotalPaidAmount().negate()));
		paymentSummaryTO.setTicketPrice(AccelAeroCalculator.formatAsDecimal(totalBookingPrice));

		BigDecimal currentTotalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotalTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotalSurCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (LCCClientReservationPax pax : colpaxs) {
			currentTotalFare = AccelAeroCalculator.add(currentTotalFare, pax.getTotalFare());
			currentTotalTax = AccelAeroCalculator.add(currentTotalTax, pax.getTotalTaxCharge());
			currentTotalSurCharges = AccelAeroCalculator.add(currentTotalSurCharges, pax.getTotalSurCharge());
		}

		paymentSummaryTO.setTotalFare(AccelAeroCalculator
				.formatAsDecimal(AccelAeroCalculator.add(bookingShoppingCart.getTotalBaseFare(), currentTotalFare)));
		paymentSummaryTO.setTotalDiscount(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getTotalFareDiscount(true)));

		paymentSummaryTO.setTotalTaxSurCharges(AccelAeroCalculator.formatAsDecimal(
				AccelAeroCalculator.add(anciTotal, bookingShoppingCart.getTotalTax(), bookingShoppingCart.getTotalSurcharges(),
						currentTotalTax, currentTotalSurCharges, bookingShoppingCart.getExtFeeAmount())));

		paymentSummaryTO.setSelectedCurrency(selCurrency);
		paymentSummaryTO.setTotalAgentCommission(bookingShoppingCart.getTotAgentCommission());
		paymentSummaryTO.setPendingAgentCommission(pendingCommission);

		CurrencyExchangeRate currencyExchangeRate = null;
		if (bookingShoppingCart.getSelectedCurrencyTimestamp() == null || bookingShoppingCart.getCurrencyExchangeRate() == null) {
			currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency, ApplicationEngine.XBE);
			bookingShoppingCart.setCurrencyExchangeRate(currencyExchangeRate);
		} else {
			currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();
		}

		paymentSummaryTO.setBalanceToPaySelcur(AccelAeroCalculator.formatAsDecimal(
				AccelAeroCalculator.multiply(currencyExchangeRate.getMultiplyingExchangeRate(), totalBookingPrice)));

		return paymentSummaryTO;
	}

	/**
	 * TODO
	 * 
	 * @param strPaxCredits
	 * @throws ParseException
	 */
	public static Object[] applyPaxCreditToPassenger(String paxPayObj, String strPaxCredits, String paxId, boolean blnModify,
			boolean blnModSegs) throws Exception {

		Collection<PaymentPassengerTO> collection = new ArrayList<PaymentPassengerTO>();

		JSONArray jsonPaxPayArray = (JSONArray) new JSONParser().parse(paxPayObj);

		Iterator<?> itIter = jsonPaxPayArray.iterator();

		JSONArray jsonCreitArray = (JSONArray) new JSONParser().parse(strPaxCredits);
		Iterator<?> creditIter = jsonCreitArray.iterator();
		BigDecimal totalPaidAmount = BigDecimal.ZERO;
		BigDecimal creditAmount = BigDecimal.ZERO;

		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			PaymentPassengerTO paymentPassengerTO = new PaymentPassengerTO();
			String intPaxCount = (String) jPPObj.get("displayPaxID");

			paymentPassengerTO.setDisplayPaxID(intPaxCount);

			paymentPassengerTO.setDisplayPassengerName((String) jPPObj.get("displayPassengerName"));
			paymentPassengerTO.setDisplayFirstName((String) jPPObj.get("displayFirstName"));
			paymentPassengerTO.setDisplayLastName((String) jPPObj.get("displayLastName"));
			paymentPassengerTO.setDisplayOriginalPaid((String) jPPObj.get("displayOriginalPaid"));
			paymentPassengerTO.setDisplayOriginalTobePaid((String) jPPObj.get("displayOriginalTobePaid"));
			if (jPPObj.get("displayTravelerRefNo") != null) {
				paymentPassengerTO.setDisplayTravelerRefNo((String) jPPObj.get("displayTravelerRefNo"));
			}

			BigDecimal chargeAmount = new BigDecimal((String) jPPObj.get("displayOriginalTobePaid"));
			BigDecimal paidAmount = new BigDecimal((String) jPPObj.get("displayPaid"));
			BigDecimal modPaidAmount = new BigDecimal((String) jPPObj.get("displayPaid"));

			BigDecimal originalPaidAmount = new BigDecimal((String) jPPObj.get("displayOriginalPaid"));
			BigDecimal originalTobePaidAmount = new BigDecimal((String) jPPObj.get("displayOriginalTobePaid"));
			BigDecimal paxUsedFullCredit = BigDecimal.ZERO;

			ArrayList<PaxUsedCreditInfoTO> arrPCInfo = new ArrayList<PaxUsedCreditInfoTO>();
			JSONArray jPCInfo = (JSONArray) jPPObj.get("paxCreditInfo");
			paidAmount = BigDecimal.ZERO;
			if (paxId.trim().equals(intPaxCount)) {

				arrPCInfo = new ArrayList<PaxUsedCreditInfoTO>();
				while (creditIter.hasNext()) {
					JSONObject jObject = (JSONObject) creditIter.next();
					if (jObject != null) {
						PaxUsedCreditInfoTO usedCredit = new PaxUsedCreditInfoTO();
						usedCredit.setPaxCreditId(((String) jObject.get("paxID")));
						usedCredit.setCarrier(((String) jObject.get("carrier")));
						usedCredit.setPnr(((String) jObject.get("pnr")));
						BigDecimal utilizedAmount = new BigDecimal((String) jObject.get("utilizedCredit"));
						usedCredit.setResidingCarrierAmount((String) jObject.get("residingCarrierAmount"));
						if (utilizedAmount.doubleValue() > chargeAmount.doubleValue()) {
							utilizedAmount = chargeAmount;
						}

						chargeAmount = AccelAeroCalculator.add(chargeAmount, utilizedAmount.negate());
						paidAmount = AccelAeroCalculator.add(paidAmount, utilizedAmount);
						usedCredit.setUsedAmount(AccelAeroCalculator.formatAsDecimal(utilizedAmount));
						creditAmount = AccelAeroCalculator.add(creditAmount, utilizedAmount);
						arrPCInfo.add(usedCredit);
						paxUsedFullCredit = AccelAeroCalculator.add(paxUsedFullCredit, utilizedAmount);
					}

				}
				if (blnModSegs || blnModify) {
					totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, modPaidAmount);
				}
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, paidAmount);
			} else {
				chargeAmount = new BigDecimal((String) jPPObj.get("displayToPay"));
				Iterator<?> uIter = jPCInfo.iterator();
				while (uIter.hasNext()) {
					JSONObject jUObj = (JSONObject) uIter.next();
					PaxUsedCreditInfoTO paxUsedCr = new PaxUsedCreditInfoTO();
					totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, new BigDecimal((String) jUObj.get("usedAmount")));
					paxUsedCr.setPaxCreditId((String) jUObj.get("paxCreditId"));
					paxUsedCr.setCarrier((String) jUObj.get("carrier"));
					paxUsedCr.setPnr((String) jUObj.get("pnr"));
					paxUsedCr.setUsedAmount((String) jUObj.get("usedAmount"));
					BigDecimal utilizedAmount = new BigDecimal((String) jUObj.get("usedAmount"));
					paxUsedCr.setResidingCarrierAmount((String) jUObj.get("residingCarrierAmount"));
					creditAmount = AccelAeroCalculator.add(creditAmount, utilizedAmount);
					paxUsedFullCredit = AccelAeroCalculator.add(paxUsedFullCredit, utilizedAmount);
					arrPCInfo.add(paxUsedCr);
					paidAmount = paxUsedFullCredit;
				}
				if (blnModSegs || blnModify) {
					totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, modPaidAmount);
				}
				// totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, paidAmount);
			}
			paymentPassengerTO.setDisplayPaid(
					AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(originalPaidAmount, paxUsedFullCredit)));
			paymentPassengerTO.setDisplayToPay(AccelAeroCalculator
					.formatAsDecimal(AccelAeroCalculator.add(originalTobePaidAmount, paxUsedFullCredit.negate())));
			paymentPassengerTO.setDisplayTotal((String) jPPObj.get("displayTotal"));
			paymentPassengerTO.setDisplayUsedCredit(AccelAeroCalculator.formatAsDecimal(paidAmount));
			paymentPassengerTO.setPaxCreditInfo(arrPCInfo);
			collection.add(paymentPassengerTO);
		}

		Object[] arraObj = new Object[3];
		arraObj[0] = collection;
		arraObj[1] = totalPaidAmount;
		arraObj[2] = creditAmount;
		return arraObj;
	}

	public static Object[] removePaxCreditToPassenger(String paxPayObj, String paxId, boolean blnModify, boolean blnModSegs)
			throws Exception {

		Collection<PaymentPassengerTO> collection = new ArrayList<PaymentPassengerTO>();
		JSONArray jsonPaxPayArray = (JSONArray) new JSONParser().parse(paxPayObj);

		Iterator<?> itIter = jsonPaxPayArray.iterator();
		BigDecimal totalPaidAmount = BigDecimal.ZERO;
		BigDecimal creditAmount = BigDecimal.ZERO;

		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			PaymentPassengerTO paymentPassengerTO = new PaymentPassengerTO();
			String intPaxCount = (String) jPPObj.get("displayPaxID");
			paymentPassengerTO.setDisplayPaxID(intPaxCount);

			paymentPassengerTO.setDisplayPassengerName((String) jPPObj.get("displayPassengerName"));
			paymentPassengerTO.setDisplayFirstName((String) jPPObj.get("displayFirstName"));
			paymentPassengerTO.setDisplayLastName((String) jPPObj.get("displayLastName"));
			paymentPassengerTO.setDisplayOriginalPaid((String) jPPObj.get("displayOriginalPaid"));
			paymentPassengerTO.setDisplayOriginalTobePaid((String) jPPObj.get("displayOriginalTobePaid"));

			BigDecimal paidAmount = new BigDecimal((String) jPPObj.get("displayPaid"));
			BigDecimal modPaidAmount = new BigDecimal((String) jPPObj.get("displayPaid"));

			BigDecimal originalPaidAmount = new BigDecimal((String) jPPObj.get("displayOriginalPaid"));
			BigDecimal originalTobePaidAmount = new BigDecimal((String) jPPObj.get("displayOriginalTobePaid"));
			BigDecimal paxUsedFullCredit = BigDecimal.ZERO;

			ArrayList<PaxUsedCreditInfoTO> arrPCInfo = new ArrayList<PaxUsedCreditInfoTO>();
			JSONArray jPCInfo = (JSONArray) jPPObj.get("paxCreditInfo");
			totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount,
					new BigDecimal((String) jPPObj.get("displayOriginalPaid")));
			if (paxId.trim().equals(intPaxCount)) {
				arrPCInfo = new ArrayList<PaxUsedCreditInfoTO>();
				paidAmount = BigDecimal.ZERO;
				paxUsedFullCredit = BigDecimal.ZERO;
				if (blnModSegs || blnModify) {
					Iterator<?> uIter = jPCInfo.iterator();
					while (uIter.hasNext()) {
						JSONObject jUObj = (JSONObject) uIter.next();
						BigDecimal usedAmt = new BigDecimal((String) jUObj.get("usedAmount"));
						modPaidAmount = AccelAeroCalculator.add(modPaidAmount, usedAmt.negate());
						creditAmount = AccelAeroCalculator.add(creditAmount, usedAmt.negate());
					}
				}
			} else {
				paidAmount = BigDecimal.ZERO;
				Iterator<?> uIter = jPCInfo.iterator();
				while (uIter.hasNext()) {
					JSONObject jUObj = (JSONObject) uIter.next();
					PaxUsedCreditInfoTO paxUsedCr = new PaxUsedCreditInfoTO();
					totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, new BigDecimal((String) jUObj.get("usedAmount")));
					paxUsedCr.setPaxCreditId((String) jUObj.get("paxCreditId"));
					paxUsedCr.setUsedAmount((String) jUObj.get("usedAmount"));
					paxUsedCr.setCarrier((String) jUObj.get("carrier"));
					paxUsedCr.setPnr((String) jUObj.get("pnr"));
					paxUsedCr.setResidingCarrierAmount((String) jUObj.get("residingCarrierAmount"));
					BigDecimal utilizedAmount = new BigDecimal((String) jUObj.get("usedAmount"));
					// creditAmount = AccelAeroCalculator.add(creditAmount, utilizedAmount);
					paxUsedFullCredit = AccelAeroCalculator.add(paxUsedFullCredit, utilizedAmount);
					arrPCInfo.add(paxUsedCr);
					paidAmount = paxUsedFullCredit;
				}
			}

			paymentPassengerTO.setDisplayPaid(
					AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(originalPaidAmount, paxUsedFullCredit)));
			paymentPassengerTO.setDisplayToPay(AccelAeroCalculator
					.formatAsDecimal(AccelAeroCalculator.add(originalTobePaidAmount, paxUsedFullCredit.negate())));
			paymentPassengerTO.setDisplayTotal((String) jPPObj.get("displayTotal"));
			paymentPassengerTO.setDisplayUsedCredit(AccelAeroCalculator.formatAsDecimal(paidAmount));
			paymentPassengerTO.setPaxCreditInfo(arrPCInfo);
			collection.add(paymentPassengerTO);
		}

		Object[] arraObj = new Object[3];
		arraObj[0] = collection;
		arraObj[1] = totalPaidAmount;
		arraObj[2] = creditAmount;

		return arraObj;
	}

	public static CurrencyInfoTO getCurrencyInfo(String currencyCode, Date applicableExRateTimestamp) throws ModuleException {

		CurrencyInfoTO currencyInfoTO = new CurrencyInfoTO();
		currencyInfoTO.setCurrencyCode(currencyCode);
		currencyInfoTO.setExchangeRateTimestamp(applicableExRateTimestamp);

		CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(applicableExRateTimestamp)
				.getCurrencyExchangeRate(currencyCode, ApplicationEngine.XBE);

		if (currencyExchangeRate != null) {
			Currency currency = currencyExchangeRate.getCurrency();
			currencyInfoTO.setCurrencyDesc(currency.getCurrencyDescriprion());

			if (currency.getStatus().equals(WebConstants.RECORD_STATUS_ACTIVE) && currency.getXBEVisibility() == 1) {
				currencyInfoTO.setBoundryValue(currency.getBoundryValue());
				currencyInfoTO.setBreakPointValue(currency.getBreakPoint());
				currencyInfoTO.setDefaultIpgId(currency.getDefaultXbePGId());
				if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultXbePGId() != null) {
					currencyInfoTO.setIsCardPaymentEnabled(true);
				} else {
					currencyInfoTO.setIsCardPaymentEnabled(false);
				}
				currencyInfoTO.setCurrencyExchangeRate(currencyExchangeRate.getMultiplyingExchangeRate());
				boolean blnRoundAppParam = AppSysParamsUtil.isCurrencyRoundupEnabled();

				if (blnRoundAppParam && currencyInfoTO.getBoundryValue() != null
						&& currencyInfoTO.getBoundryValue().doubleValue() != 0 && currencyInfoTO.getBreakPointValue() != null) {
					currencyInfoTO.setIsApplyRounding(true);
				} else {
					currencyInfoTO.setIsApplyRounding(false);
				}
			}
		}

		return currencyInfoTO;
	}

	/**
	 * Returns PayCurrencyDTO for on account payment.
	 * 
	 * @param agentCodeForOnAccPayment
	 * @param paymentTimestamp
	 * @return
	 * @throws ModuleException
	 */
	public static PayCurrencyDTO getOnAccPayCurrencyDTO(String selectedCurrencyCode, String loggedInAgencyCode,
			String agentCodeForOnAccPayment, ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		CurrencyInfoTO selectedCurrencyInfoTO = PaymentRHUtil.getCurrencyInfo(selectedCurrencyCode, exchangeRateProxy);
		CurrencyInfoTO paymentCurrencyInfoTO = PaymentRHUtil.getAgentPaymentCurrencyInfoTO(agentCodeForOnAccPayment,
				loggedInAgencyCode, exchangeRateProxy, selectedCurrencyInfoTO);
		if (paymentCurrencyInfoTO.getCurrencyExchangeRate() == null) {
			log.error("Currency [" + paymentCurrencyInfoTO.getCurrencyCode() + "] exRate is not define in the system");
			throw new XBERuntimeException("xbe.exchangerate.undefined");
		}
		return new PayCurrencyDTO(paymentCurrencyInfoTO.getCurrencyCode(), paymentCurrencyInfoTO.getCurrencyExchangeRate(),
				paymentCurrencyInfoTO.getBoundryValue(), paymentCurrencyInfoTO.getBreakPointValue());
	}

	/**
	 * Returns PayCurrencyDTO for on account payment.
	 * 
	 * @param agentCodeForOnAccPayment
	 * @param paymentTimestamp
	 * @return
	 * @throws ModuleException
	 */
	public static PayCurrencyDTO getBSPPayCurrencyDTO(String selectedCurrencyCode, String loggedInAgencyCode,
			String agentCodeForOnAccPayment, ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		CurrencyInfoTO paymentCurrencyInfoTO = PaymentRHUtil.getCurrencyInfo(selectedCurrencyCode, exchangeRateProxy);
		if (paymentCurrencyInfoTO.getCurrencyExchangeRate() == null) {
			log.error("Currency [" + paymentCurrencyInfoTO.getCurrencyCode() + "] exRate is not define in the system");
			throw new XBERuntimeException("xbe.exchangerate.undefined");
		}
		return new PayCurrencyDTO(paymentCurrencyInfoTO.getCurrencyCode(), paymentCurrencyInfoTO.getCurrencyExchangeRate(),
				paymentCurrencyInfoTO.getBoundryValue(), paymentCurrencyInfoTO.getBreakPointValue());
	}

	/**
	 * Returns PayCurrencyDTO for credit card payment.
	 * 
	 * @param ipgId
	 * @param isOfflinePayment
	 *            TODO
	 * @param paymentTimestamp
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static PayCurrencyDTO getPaymentCurrencyForCardPay(String selectedCurrencyCode, Integer ipgId,
			ExchangeRateProxy exchangeRateProxy, boolean isOfflinePayment) throws ModuleException {
		CurrencyInfoTO selectedCurrencyInfoTO = PaymentRHUtil.getCurrencyInfo(selectedCurrencyCode, exchangeRateProxy);
		CurrencyInfoTO paymentCurrencyInfoTO = PaymentRHUtil.getCardPaymentCurrencyInfo(selectedCurrencyInfoTO, exchangeRateProxy,
				ipgId, isOfflinePayment);

		return new PayCurrencyDTO(paymentCurrencyInfoTO.getCurrencyCode(), paymentCurrencyInfoTO.getCurrencyExchangeRate(),
				paymentCurrencyInfoTO.getBoundryValue(), paymentCurrencyInfoTO.getBreakPointValue());
	}

	public static PayCurrencyDTO getPaymentCurrencyForCashPay(String selectedCurrencyCode, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {
		CurrencyInfoTO paymentCurrencyInfoTO = PaymentRHUtil.getCurrencyInfo(selectedCurrencyCode, exchangeRateProxy);
		return new PayCurrencyDTO(paymentCurrencyInfoTO.getCurrencyCode(), paymentCurrencyInfoTO.getCurrencyExchangeRate(),
				paymentCurrencyInfoTO.getBoundryValue(), paymentCurrencyInfoTO.getBreakPointValue());
	}

	/**
	 * @return all the flexiCharges
	 */
	private static Collection<ExternalChgDTO> getFlexiCharges(ReservationPaxTO reservationPax) {
		Collection<ExternalChgDTO> externalCharges = new ArrayList<ExternalChgDTO>();
		if (reservationPax.getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : reservationPax.getExternalCharges()) {
				if (charge.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)) {
					ExternalChgDTO externalChgDTO = new ExternalChgDTO();
					externalChgDTO.setAmount(charge.getAmount());
					externalChgDTO.setAmountConsumedForPayment(false);
					externalChgDTO.setChargeCode(charge.getCode());
					externalChgDTO.setExternalChargesEnum(charge.getExternalCharges());
					externalCharges.add(externalChgDTO);
				}
			}
		}
		return externalCharges;
	}

	/**
	 * Crap method for interline modification must re factor
	 * 
	 * @param bookingShoppingCart
	 * @param selCurrency
	 * @return
	 * @throws ParseException
	 * @throws ModuleException
	 */
	public static PaymentSummaryTO createInterlinePaymentSummary(BookingShoppingCart bookingShoppingCart, String selCurrency,
			ExchangeRateProxy exchangeRateProxy) throws ParseException, ModuleException {
		return createInterlinePaymentSummary(bookingShoppingCart, selCurrency, exchangeRateProxy, null);
	}

	public static PaymentSummaryTO createInterlinePaymentSummary(BookingShoppingCart bookingShoppingCart, String selCurrency,
			ExchangeRateProxy exchangeRateProxy, String loggedInAgentCode) throws ParseException, ModuleException {

		BigDecimal handlingCharge = getTotalHandlingFeeForModification(bookingShoppingCart.getPaxList());
		// Ancillary total for the create booking. For the modifications if balance calculation is done this will be
		// zero
		BigDecimal anciTotal = bookingShoppingCart.getAncillaryTotalExternalCharge(true);

		BigDecimal totalBookingPrice = bookingShoppingCart.getTotalPrice();

		PaymentSummaryTO paymentSummaryTO = new PaymentSummaryTO();

		BigDecimal balTOPay = bookingShoppingCart.getTotalPayable();

		paymentSummaryTO.setBalanceToPay(AccelAeroCalculator.formatAsDecimal(balTOPay));
		paymentSummaryTO.setCurrency(AppSysParamsUtil.getBaseCurrency());
		paymentSummaryTO.setHandlingCharges(AccelAeroCalculator.formatAsDecimal(handlingCharge));
		paymentSummaryTO
				.setModificationCharges(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getModifyOrCancellationAmount()));

		paymentSummaryTO.setPaid(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getTotalPaidAmount().negate()));
		paymentSummaryTO.setTicketPrice(AccelAeroCalculator.formatAsDecimal(totalBookingPrice));
		paymentSummaryTO.setTotalFare(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getTotalBaseFare()));
		paymentSummaryTO.setTotalDiscount(AccelAeroCalculator.formatAsDecimal(bookingShoppingCart.getTotalFareDiscount(true)));

		BigDecimal nonrefunds = BigDecimal.ZERO;
		if (bookingShoppingCart.getReservationBalance() != null
				&& bookingShoppingCart.getReservationBalance().getSegmentSummary() != null) {
			nonrefunds = bookingShoppingCart.getReservationBalance().getSegmentSummary().getCurrentNonRefunds();
		}

		BigDecimal surchargeWithoutHandlingCharge = AccelAeroCalculator.subtract(bookingShoppingCart.getTotalSurcharges(),
				handlingCharge);
		paymentSummaryTO.setTotalTaxSurCharges(
				AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(anciTotal, bookingShoppingCart.getTotalTax(),
						surchargeWithoutHandlingCharge, nonrefunds, bookingShoppingCart.getExtFeeAmount())));

		paymentSummaryTO.setSelectedCurrency(selCurrency);

		CurrencyExchangeRate currencyExchangeRate = null;
		if (bookingShoppingCart.getSelectedCurrencyTimestamp() == null || bookingShoppingCart.getCurrencyExchangeRate() == null) {
			currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency, ApplicationEngine.XBE);
			bookingShoppingCart.setCurrencyExchangeRate(currencyExchangeRate);
		} else {
			currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();
		}

		paymentSummaryTO.setBalanceToPaySelcur(AccelAeroCalculator
				.formatAsDecimal(AccelAeroCalculator.multiply(currencyExchangeRate.getMultiplyingExchangeRate(), balTOPay)));

		return paymentSummaryTO;
	}

	public static CurrencyExchangeRate getBSPCountryCurrencyExchgRate(HttpServletRequest request) throws ModuleException {
		if (AppSysParamsUtil.isBspPaymentsAcceptedForMCCreateReservation()) {
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userPrincipal.getAgentCode());

			Set<String> allowedPaymentMethods = agent.getPaymentMethod();
			if (allowedPaymentMethods != null && allowedPaymentMethods.contains(Agent.PAYMENT_MODE_BSP)) {
				String currencyCode = ModuleServiceLocator.getCommonServiceBD()
						.getCurrencyCodeForStation(userPrincipal.getAgentStation());
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				return exchangeRateProxy.getCurrencyExchangeRate(currencyCode);
			}
		}
		return null;
	}

	public static boolean isValidPaymentExist(boolean creditFlag, BigDecimal amountDue, String reservationStatus)
			throws ModuleException {
		reservationStatus = BeanUtils.nullHandler(reservationStatus);

		if (reservationStatus.length() == 0) {
			throw new ModuleException("airreservation.internal.status.empty");
		}

		// This is like Panaroma's Box. Don't you dare change it with out all the scenarios
		// This amountDue can be zero, + and - too.
		// in top of that genuine zero payments should be tracked too.
		// Nili March 8, 2012
		if (!(creditFlag && amountDue.compareTo(BigDecimal.ZERO) == 0) && (amountDue.compareTo(BigDecimal.ZERO) >= 0)) {
			if (amountDue.compareTo(BigDecimal.ZERO) > 0 || !(amountDue.compareTo(BigDecimal.ZERO) == 0
					&& reservationStatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED))) {
				return true;
			}
		}

		return false;
	}

	public static BigDecimal getTotalPaymentAmountWithoutTransactionFee(HttpServletRequest request, PaymentTO payment) {
		BigDecimal totalPaymentAmount = BigDecimal.ZERO;
		if ("FORCE".equals(payment.getMode())) {
			totalPaymentAmount = new BigDecimal(payment.getAmount());
		} else {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			if (bookingShoppingCart.getTotalPayable().compareTo(BigDecimal.ZERO) == 0) {
				totalPaymentAmount = new BigDecimal(payment.getAmount());
			} else if (bookingShoppingCart.getTotalPayable().compareTo(BigDecimal.ZERO) < 0) {
				totalPaymentAmount = new BigDecimal(payment.getAmount());
			} else if (bookingShoppingCart.getTotalPayable().compareTo(new BigDecimal(payment.getAmount())) > 0) {
				totalPaymentAmount = new BigDecimal(payment.getAmount());
			} else {
				totalPaymentAmount = bookingShoppingCart.getTotalPayable();
			}

			if (bookingShoppingCart.getPayByVoucherInfo() != null) {
				BigDecimal toPay = bookingShoppingCart.getPayByVoucherInfo().getRedeemedTotal();
				if (totalPaymentAmount.compareTo(toPay) > -1) {
					totalPaymentAmount = AccelAeroCalculator.subtract(totalPaymentAmount, toPay);
				} else {
					totalPaymentAmount = BigDecimal.ZERO;
				}
			}

		}
		return totalPaymentAmount;
	}

	public static BigDecimal getTotalPayableAmount(HttpServletRequest request, PaymentTO payment) {

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String agentCode = userPrincipal.getAgentCode();
		BigDecimal totalPayableAmount;

		if (ParameterUtil.isAdminFeeEnabledForXBE(agentCode)
				&& bookingShoppingCart.getTotalPayableIncludingCCCharges().compareTo(BigDecimal.ZERO) > 0) {

			if (bookingShoppingCart.getReservationBalance() == null) {
				totalPayableAmount = bookingShoppingCart.getTotalPayableIncludingCCCharges();
			} else {
				totalPayableAmount = bookingShoppingCart.getTotalPayable();
			}

		} else {
			totalPayableAmount = getTotalPaymentAmountWithoutTransactionFee(request, payment);
		}

		return totalPayableAmount;

	}

	public static BigDecimal getTotalHandlingFeeForModification(Collection<ReservationPaxTO> paxList) {
		BigDecimal totalHandlingCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (AppSysParamsUtil.applyHandlingFeeOnModification()) {
			for (ReservationPaxTO pax : paxList) {
				totalHandlingCharges = totalHandlingCharges
						.add(pax.getExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE).getAmount());
			}
		}
		return totalHandlingCharges;
	}
}
