package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRS;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleRouteInfo;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfo;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.FlightScheduleDTO;
import com.isa.thinair.xbe.api.dto.v2.FlightScheduleSegInfoDTO;
import com.isa.thinair.xbe.api.dto.v2.FltSchRouteInfoDTO;
import com.isa.thinair.xbe.api.dto.v2.SearchFlightScheduleTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author eric
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchFlightScheduleAction extends BaseRequestAwareAction {

	private static final Log log = LogFactory.getLog(SearchFlightScheduleAction.class);
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat timeFormatter = new SimpleDateFormat(CommonsServices.getGlobalConfig().getBizParam(
			SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS));

	private boolean success = true;

	private String messageTxt;

	private SearchFlightScheduleTO searchFlightScheduleTO;

	private Collection<FltSchRouteInfoDTO> outBoundFltRouteScheduleInfo;

	private Collection<FltSchRouteInfoDTO> inBoundFltRouteScheduleInfo;

	public String execute() {
		try {
			ScheduleSearchRS schedulerSearchRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().getSchedulesList(
					createScheduleSearchRQ());

			this.buildFlightSchedules(schedulerSearchRS);

		} catch (ModuleException me) {
			this.success = false;
			this.messageTxt = me.getMessage();
			if (log.isErrorEnabled()) {
				log.error(me.getMessage());
			}
		} catch (Exception e) {
			this.success = false;
			this.messageTxt = e.getMessage();
			if (log.isErrorEnabled()) {
				log.error(e.getMessage());
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public Collection<FltSchRouteInfoDTO> getOutBoundFltRouteScheduleInfo() {
		return outBoundFltRouteScheduleInfo;
	}

	public Collection<FltSchRouteInfoDTO> getInBoundFltRouteScheduleInfo() {
		return inBoundFltRouteScheduleInfo;
	}

	public SearchFlightScheduleTO getSearchFlightScheduleTO() {
		return searchFlightScheduleTO;
	}

	public void setSearchFlightScheduleTO(SearchFlightScheduleTO searchFlightScheduleTO) {
		this.searchFlightScheduleTO = searchFlightScheduleTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	private void buildFlightSchedules(ScheduleSearchRS schedulerSearchRS) throws Exception {
		this.outBoundFltRouteScheduleInfo = new ArrayList<FltSchRouteInfoDTO>();
		this.inBoundFltRouteScheduleInfo = new ArrayList<FltSchRouteInfoDTO>();

		if (!schedulerSearchRS.getOutboundScheduleRouteInfo().isEmpty()) {
			for (FlightScheduleRouteInfo fltScheduleRouteInfo : schedulerSearchRS.getOutboundScheduleRouteInfo()) {
				if (!fltScheduleRouteInfo.getFlightScheduleSegmentInfo().isEmpty()) {
					FltSchRouteInfoDTO fltSchRouteInfoDTO = new FltSchRouteInfoDTO();
					fltSchRouteInfoDTO.setRoute(fltScheduleRouteInfo.getRoute());
					for (FlightScheduleSegmentInfo fltScheduleSegmentInfo : fltScheduleRouteInfo.getFlightScheduleSegmentInfo()) {
						FlightScheduleSegInfoDTO fltSchSegInfoDTO = new FlightScheduleSegInfoDTO();
						fltSchSegInfoDTO.setSegment(fltScheduleSegmentInfo.getSegemntCode());
						int scheduleIndex = 1;
						for (FlightScheduleInfoDTO fltSchInfo : fltScheduleSegmentInfo.getFlightSchedules()) {
							FlightScheduleDTO fltSch = transformFlightScheduleDTO(fltSchInfo);
							fltSch.setId(scheduleIndex);
							fltSchSegInfoDTO.getFlightSchedules().add(fltSch);
							scheduleIndex++;
						}
						fltSchRouteInfoDTO.getFltScheSegInfo().add(fltSchSegInfoDTO);
					}
					outBoundFltRouteScheduleInfo.add(fltSchRouteInfoDTO);
				}
			}
		}

		if (!schedulerSearchRS.getInboundScheduleRouteInfo().isEmpty()) {
			for (FlightScheduleRouteInfo fltScheduleRouteInfo : schedulerSearchRS.getInboundScheduleRouteInfo()) {
				if (!fltScheduleRouteInfo.getFlightScheduleSegmentInfo().isEmpty()) {
					FltSchRouteInfoDTO fltSchRouteInfoDTO = new FltSchRouteInfoDTO();
					fltSchRouteInfoDTO.setRoute(fltScheduleRouteInfo.getRoute());
					for (FlightScheduleSegmentInfo fltScheduleSegmentInfo : fltScheduleRouteInfo.getFlightScheduleSegmentInfo()) {
						FlightScheduleSegInfoDTO fltSchSegInfoDTO = new FlightScheduleSegInfoDTO();
						fltSchSegInfoDTO.setSegment(fltScheduleSegmentInfo.getSegemntCode());
						int scheduleIndex = 1;
						for (FlightScheduleInfoDTO fltSchInfo : fltScheduleSegmentInfo.getFlightSchedules()) {
							FlightScheduleDTO fltSch = transformFlightScheduleDTO(fltSchInfo);
							fltSch.setId(scheduleIndex);
							fltSchSegInfoDTO.getFlightSchedules().add(fltSch);
							scheduleIndex++;
						}
						fltSchRouteInfoDTO.getFltScheSegInfo().add(fltSchSegInfoDTO);
					}
					inBoundFltRouteScheduleInfo.add(fltSchRouteInfoDTO);
				}
			}
		}
	}

	/**
	 * @param flightScheduleInfoDTO
	 * @return
	 * @throws Exception
	 */
	private FlightScheduleDTO transformFlightScheduleDTO(FlightScheduleInfoDTO flightScheduleInfoDTO) throws Exception {

		FlightScheduleDTO flightScheduleDTO = new FlightScheduleDTO();

		List<FlightScheduleLegInfoDTO> fltLegInfoList = new ArrayList<FlightScheduleLegInfoDTO>(
				flightScheduleInfoDTO.getFlightScheduleLegs());
		Collections.sort(fltLegInfoList);

		flightScheduleDTO.setFlightNo(flightScheduleInfoDTO.getFlightNumber());
		flightScheduleDTO.setDepartureTime(timeFormatter.format(fltLegInfoList.get(0).getEstDepartureTimeLocal()));
		flightScheduleDTO.setArrivalTime(timeFormatter.format(fltLegInfoList.get(fltLegInfoList.size() - 1)
				.getEstArrivalTimeLocal()));
		flightScheduleDTO.setFromDate(dateFormatter.format(flightScheduleInfoDTO.getStartDate()));
		flightScheduleDTO.setToDate(dateFormatter.format(flightScheduleInfoDTO.getStopDate()));
		flightScheduleDTO.setDaysOfOperation(createDaysFrequncey(flightScheduleInfoDTO.getFrequencyLocal()));
		flightScheduleDTO.setNoOfStopovers(String.valueOf(flightScheduleInfoDTO.getFlightScheduleLegs().size() - 1));
		flightScheduleDTO.setFlightModel(flightScheduleInfoDTO.getModelNumber());
		flightScheduleDTO.setTotalFlyingTime(this.getTotalFlyingTime(fltLegInfoList));
		flightScheduleDTO.setTransitDuration(this.getTransitTime(fltLegInfoList));

		return flightScheduleDTO;

	}

	/**
	 * @param fltLegInfo
	 * @return
	 */
	private String getTotalFlyingTime(List<FlightScheduleLegInfoDTO> fltLegInfo) {
		String flyingTimeStr = "";
		int totalflyingTime = 0;
		for (FlightScheduleLegInfoDTO fltScheduleLegInfoDTO : fltLegInfo) {
			totalflyingTime += fltScheduleLegInfoDTO.getDuration();
		}

		if (totalflyingTime / 60 > 0) {
			flyingTimeStr += (totalflyingTime / 60) + " Hrs ";
		}
		if (totalflyingTime % 60 > 0) {
			flyingTimeStr += (totalflyingTime % 60) + " Mins";
		}
		return flyingTimeStr;
	}

	/**
	 * @param fltLegInfo
	 * @return
	 */
	private String getTransitTime(List<FlightScheduleLegInfoDTO> fltLegInfo) {
		String transitTimeStr = "";

		int transitTime = 0;
		if (fltLegInfo.size() > 0) {
			for (int legIndex = 1; legIndex < fltLegInfo.size(); legIndex++) {

				transitTime += CalendarUtil.getTimeDifferenceInMinutes(fltLegInfo.get(legIndex - 1).getEstArrivalTimeZulu(),
						fltLegInfo.get(legIndex).getEstDepartureTimeZulu());
			}
		}
		if (transitTime / 60 > 0) {
			transitTimeStr += (transitTime / 60) + " Hrs ";
		}
		if (transitTime % 60 > 0) {
			transitTimeStr += (transitTime % 60) + " Mins";
		}
		return transitTimeStr;

	}

	private ScheduleSearchRQ createScheduleSearchRQ() throws Exception {
		ScheduleSearchRQ scheduleSearchRQ = new ScheduleSearchRQ();

		scheduleSearchRQ.setFromAirport(getSearchFlightScheduleTO().getFromAirport());
		scheduleSearchRQ.setToAirport(getSearchFlightScheduleTO().getToAirport());
		scheduleSearchRQ.setFromDate(dateFormatter.parse(getSearchFlightScheduleTO().getFromDate()));
		scheduleSearchRQ.setToDate(dateFormatter.parse(getSearchFlightScheduleTO().getToDate()));
		scheduleSearchRQ.setRoundTrip(getSearchFlightScheduleTO().getRoundTrip() != null);
		UserPrincipal user = (UserPrincipal)request.getUserPrincipal();
		scheduleSearchRQ.setAgentCode(user.getAgentCode());
		return scheduleSearchRQ;

	}

	/**
	 * Method to get the Frequency segment
	 * 
	 * @param freq
	 *            the Frequency
	 * @return String the string frequency Su_TuWd__Sa
	 */

	private String createDaysFrequncey(Frequency freq) {

		HashMap<DayOfWeek, Boolean> daysMap = createDayOfWeekMap(freq);

		// create frequency string using daysMap
		String strFrq = "";
		String[] dayweek = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };

		int intoffset = CalendarUtil.getOffsetFromStanderdDayNumber();
		intoffset = intoffset % 7;
		for (int i = 0; i < 7; i++) {
			if (intoffset == 7)
				intoffset = 0;
			strFrq += (((Boolean) daysMap.get(CalendarUtil.getDayFromDayNumber(i))).booleanValue()) ? ((strFrq.length() > 0 ? ", "
					: "") + dayweek[intoffset])
					: "";

			intoffset++;
		}
		if (!strFrq.isEmpty() && strFrq.split(",").length == 7) {
			strFrq = "Daily";
		}
		return strFrq;
	}

	/**
	 * Creates a Day of Week Map
	 * 
	 * @param freq
	 *            the Frequency
	 * @return HashMap containing Day of Week
	 */
	private HashMap<DayOfWeek, Boolean> createDayOfWeekMap(Frequency freq) {

		// initialize hashmap
		HashMap<DayOfWeek, Boolean> daysMap = new HashMap<DayOfWeek, Boolean>();

		daysMap.put(DayOfWeek.SUNDAY, new Boolean(false));
		daysMap.put(DayOfWeek.MONDAY, new Boolean(false));
		daysMap.put(DayOfWeek.TUESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.WEDNESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.THURSDAY, new Boolean(false));
		daysMap.put(DayOfWeek.FRIDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SATURDAY, new Boolean(false));

		Collection days = CalendarUtil.getDaysFromFrequency(freq);
		Iterator itDays = days.iterator();

		while (itDays.hasNext()) {
			DayOfWeek day = (DayOfWeek) itDays.next();

			// reset map with selected days
			daysMap.put(day, new Boolean(true));
		}

		return daysMap;
	}
}
