package com.isa.thinair.xbe.core.web.generator.travagent;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airtravelagents.api.dto.AgentTransactionsDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class AgentCollectionHTMLGenerator {

	private static Log log = LogFactory.getLog(AgentCollectionHTMLGenerator.class);

	private static String clientErrors;

	public String getAgentCollectionRowHtml(HttpServletRequest request, String trtryCode, String airlineCode)
			throws ModuleException {

		DecimalFormat formatter = new DecimalFormat("#.00");
		Page page = null;
		Collection<Agent> colData = null;
		String strCurr = request.getParameter("hdnCurrency");
		if (strCurr == null) {
			strCurr = "B";
		}
		request.setAttribute(WebConstants.REQ_SEARCH_CURR, "strCurrency = '" + strCurr + "';");

		String optAgentType = request.getParameter("selAgentType");

		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();

		ModuleCriterion MCagentType = new ModuleCriterion();
		ModuleCriterion MCagentName = new ModuleCriterion();
		ModuleCriterion MCagentCode = new ModuleCriterion();
		ModuleCriterion isGsaCode = new ModuleCriterion();
		ModuleCriterion MCGsaCode = new ModuleCriterion();
		ModuleCriterion MCagentIataCode = new ModuleCriterion();

		ModuleCriterion MCreptogsa = new ModuleCriterion();
		MCreptogsa.setFieldName("reportingToGSA");

		ModuleCriterion MCterritory = new ModuleCriterion();
		MCterritory.setCondition(ModuleCriterion.CONDITION_EQUALS);
		MCterritory.setFieldName("territoryCode");

		ModuleCriterion MCairline = new ModuleCriterion();
		MCairline.setCondition(ModuleCriterion.CONDITION_EQUALS);
		MCairline.setFieldName("airlineCode");

		MCagentName.setCondition(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE);
		MCagentName.setFieldName("agentName");

		MCagentCode.setCondition(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE);
		MCagentCode.setFieldName("agentCode");

		MCagentIataCode.setCondition(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE);
		MCagentIataCode.setFieldName("agentIATANumber");

		if (optAgentType != null && !(optAgentType.equals(""))) {
			List<String> valueType = new ArrayList<String>();

			MCagentType.setCondition(ModuleCriterion.CONDITION_EQUALS);
			MCagentType.setFieldName("agentTypeCode");
			valueType.add(optAgentType);
			MCagentType.setValue(valueType);
			critrian.add(MCagentType);
		}

		if (request.getParameter("hdnAgentName") != null && !(request.getParameter("hdnAgentName").equals(""))) {
			List<String> valueName = new ArrayList<String>();
			valueName.add("%" + request.getParameter("hdnAgentName") + "%");
			MCagentName.setValue(valueName);
			critrian.add(MCagentName);
		}

		if (request.getParameter("hdnAgCode") != null && !(request.getParameter("hdnAgCode").equals(""))) {
			List<String> valueCode = new ArrayList<String>();
			valueCode.add("%" + request.getParameter("hdnAgCode") + "%");
			MCagentCode.setValue(valueCode);
			critrian.add(MCagentCode);
		}

		if (request.getParameter("hdnAgentIataCode") != null && !(request.getParameter("hdnAgentIataCode").equals(""))) {
			List<String> valueIataCode = new ArrayList<String>();
			valueIataCode.add("%" + request.getParameter("hdnAgentIataCode") + "%");
			MCagentIataCode.setValue(valueIataCode);
			critrian.add(MCagentIataCode);
		}

		if (airlineCode != null) {
			List<String> valueAirline = new ArrayList<String>();
			valueAirline.add(airlineCode);
			MCairline.setValue(valueAirline);
			critrian.add(MCairline);
		}

		StringBuffer sb = new StringBuffer();
		int RecNo;
		if (request.getParameter("hdnRecNo") == null) {
			RecNo = 1;
		} else {
			RecNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		if (BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_ANY)) {
			if (AppSysParamsUtil.isGSAStructureVersion2Enabled()
					&& !AppSysParamsUtil.getCarrierAgent()
							.equals(((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode())) {
				List<String> gsaCode = getReportingAgentCodes(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
				MCGsaCode.setFieldName("gsaCode");
				MCGsaCode.setCondition(ModuleCriterion.CONDITION_IN);
				MCGsaCode.setValue(gsaCode);
				critrian.add(MCGsaCode);
			}
			page = ModuleServiceLocator.getTravelAgentBD().searchAgents(critrian, RecNo - 1, 10);
		} else if (optAgentType.trim().equals("GSA") && isGSA(request)) {

			critrian.remove(MCagentCode);
			critrian.remove(MCagentName);
			critrian.remove(MCagentIataCode);
			isGsaCode.setCondition(ModuleCriterion.CONDITION_EQUALS);
			isGsaCode.setFieldName("agentCode");
			List<String> valueCode = new ArrayList<String>();
			valueCode.add(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
			isGsaCode.setValue(valueCode);
			critrian.add(isGsaCode);
			page = ModuleServiceLocator.getTravelAgentBD().searchAgents(critrian, RecNo - 1, 10);

		} else if (BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_GSA)) {
			if (!AppSysParamsUtil.isGSAStructureVersion2Enabled()
					|| (AppSysParamsUtil.isGSAStructureVersion2Enabled() && ("TA".equals(optAgentType) || "STA"
							.equals(optAgentType)))) {
				List<String> valuergsa = new ArrayList<String>();
				MCreptogsa.setCondition(ModuleCriterion.CONDITION_EQUALS);
				valuergsa.add("Y");
				MCreptogsa.setValue(valuergsa);
				critrian.add(MCreptogsa);
			}

			List<String> gsaCode = new ArrayList<String>();
			MCGsaCode.setFieldName("gsaCode");
			MCGsaCode.setCondition(ModuleCriterion.CONDITION_EQUALS);
			gsaCode.add(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
			MCGsaCode.setValue(gsaCode);
			critrian.add(MCGsaCode);

			page = ModuleServiceLocator.getTravelAgentBD().searchAgents(critrian, RecNo - 1, 10);
		}

		int totRec = 0;
		if (page != null) {
			totRec = page.getTotalNoOfRecords();
			colData = page.getPageData();
		}

		String strJavascriptTotalNoOfRecs = "var totalRecords = " + totRec + ";";
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

		sb.append("var arrData = new Array();");

		if (colData != null) {
			Iterator<Agent> iter = colData.iterator();
			Agent agent = null;
			AgentSummary agentSummary = null;
			int count = 0;
			while (iter.hasNext()) {
				agent = (Agent) iter.next();
				Agent creditSharedAgent = null;
				boolean hasAgentCredit = true;

				if (Agent.HAS_CREDIT_LIMIT_NO.equals(agent.getHasCreditLimit())) {
					hasAgentCredit = false;
					creditSharedAgent = ModuleServiceLocator.getTravelAgentBD().getCreditSharingAgent(agent.getAgentCode());
				}

				sb.append("arrData[" + count + "] = new Array();");
				sb.append("arrData[" + count + "][1] = '" + ((agent.getAgentCode() == null) ? "" : agent.getAgentCode()) + "';");
				sb.append("arrData[" + count + "][2] = '"
						+ ((agent.getAgentDisplayName() == null) ? "" : agent.getAgentDisplayName()) + "';");

				Agent creditLimitAgent = agent;

				if (creditLimitAgent.getCreditLimit() != null) {

					sb.append("arrData[" + count + "][3] = '" + formatter.format(creditLimitAgent.getCreditLimit()) + "';");
				} else {
					sb.append("arrData[" + count + "][3] = '00.00';");
				}
				if (agent.getAgentIATANumber() != null) {
					sb.append("arrData[" + count + "][10] = '" + agent.getAgentIATANumber() + "';");
				} else {
					sb.append("arrData[" + count + "][10] = '';");
				}

				agentSummary = agent.getAgentSummary();

				BigDecimal amountDueFromReportingAgents = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (agentSummary != null) {

					BigDecimal ownDueAmount = agentSummary.getOwnDueAmount();
					BigDecimal distributedCreditCollection = agentSummary.getDistributedCreditCollection();
					BigDecimal totalDueAmount = AccelAeroCalculator.add(ownDueAmount, distributedCreditCollection);

					if (hasAgentCredit) {
						if (agentSummary.getAvailableCredit() != null) {
							sb.append(
									"arrData[" + count + "][4] = '" + formatter.format(agentSummary.getAvailableCredit()) + "';");
						} else {
							sb.append("arrData[" + count + "][4] = '00.00';");
						}

					} else {
						sb.append("arrData[" + count + "][4] = '"
								+ formatter.format(creditSharedAgent.getAgentSummary().getAvailableCredit()) + "';");
					}

					if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
						sb.append("arrData[" + count + "][5] = '" + formatter.format(ownDueAmount) + "';");
					} else {
						BigDecimal amountDue = AccelAeroCalculator.subtract(agent.getCreditLimit(),
								agentSummary.getAvailableCredit());

						sb.append("arrData[" + count + "][5] = '" + formatter.format(amountDue) + "';");
					}

					if (agentSummary.getAvailableFundsForInv() != null) {
						sb.append("arrData[" + count + "][6] = '" + formatter.format(agentSummary.getAvailableFundsForInv())
								+ "';");
					} else {
						sb.append("arrData[" + count + "][6] = '00.00';");
					}
					if (agentSummary.getAvailableAdvance() != null) {
						sb.append("arrData[" + count + "][7] = '" + formatter.format(agentSummary.getAvailableAdvance()) + "';");
					} else {
						sb.append("arrData[" + count + "][7] = '00.00';");
					}

					// FIXME : amountDueFromReportingAgents need to be fetched the due from the reporting agents
					BigDecimal totalSharedCreditDue = AccelAeroCalculator.subtract(agent.getCreditLimit(),
							agentSummary.getAvailableCredit());
					amountDueFromReportingAgents = AccelAeroCalculator.subtract(totalSharedCreditDue, ownDueAmount);

					if (agentSummary.getAvailableBSPCredit() != null) {
						sb.append(
								"arrData[" + count + "][11] ='" + formatter.format(agentSummary.getAvailableBSPCredit()) + "';");
					} else {
						sb.append("arrData[" + count + "][11] = '00.00';");
					}

				} else {
					sb.append("arrData[" + count + "][4] = '';");
					sb.append("arrData[" + count + "][5] = '';");
					sb.append("arrData[" + count + "][6] = '';");
					sb.append("arrData[" + count + "][7] = '';");
					sb.append("arrData[" + count + "][11] = '';");
				}
				if (strCurr != null && strCurr.equalsIgnoreCase("L")) {
					sb.append("arrData[" + count + "][8] = '"
							+ ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY) + "';");
				} else {
					sb.append("arrData[" + count + "][8] = '" + agent.getCurrencyCode() + "';");
				}

				sb.append("arrData[" + count + "][9] = '" + getCreditLimitInSpecifiedCurrency(creditLimitAgent) + "';");

				if (AccelAeroCalculator.isGreaterThan(amountDueFromReportingAgents,
						AccelAeroCalculator.getDefaultBigDecimalZero())) {
					sb.append("arrData[" + count + "][10] = '" + formatter.format(amountDueFromReportingAgents) + "';");
				} else {
					sb.append("arrData[" + count + "][10] = '00.00';");
				}
				 
				count++;
			}
		}
		return sb.toString();
	}

	private List<String> getReportingAgentCodes(String agentCode) throws ModuleException {
		List<String> reporingAgentCodes = new ArrayList<String>();
		reporingAgentCodes.add(agentCode);
		Collection<Agent> reportingAgents = ModuleServiceLocator.getTravelAgentBD().getAgentsForGSA(agentCode);
		if (reportingAgents != null) {
			for (Agent reportingAgent : reportingAgents) {
				reporingAgentCodes.addAll(getReportingAgentCodes(reportingAgent.getAgentCode()));
			}
		}
		return reporingAgentCodes;
	}

	/**
	 * Check Weather the current logged user is GSA.
	 * 
	 * @param request
	 * @return
	 */
	private static boolean isGSA(HttpServletRequest request) {

		boolean isGsa = false;
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String agentType = userPrincipal.getAgentTypeCode();
		if (agentType != null && AgentType.GSA.equals(agentType)) {
			isGsa = true;
		}
		return isGsa;

	}

	public final String getAdjustpaymentRowHtml(HttpServletRequest request) throws ModuleException {
		log.debug("***********inside AgentCollectionHTMLGenerator.getAdjustpaymentRowHtml()");

		String strHdnCode = request.getParameter("hdnAgentID");
		String strDate = request.getParameter("txtDStart");
		String strStopDate = request.getParameter("txtDStop");
		String chkPayTypeR = request.getParameter("chkTypeR");
		String chkPayTypeC = request.getParameter("chkTypeC");
		String chkPayTypeD = request.getParameter("chkTypeD");
		String chkPayTypeRR = request.getParameter("chkTypeRR");
		String chkPayTypeA = request.getParameter("chkTypeA");
		String chkPayTypeQ = request.getParameter("chkTypeQ");
		String chkPayTypeRV = request.getParameter("chkTypeRV");
		String chkPayTypeBSP = request.getParameter("chkTypeBSP");
		String chkTypeCG = request.getParameter("chkTypeCG");
		String chkTypeCR = request.getParameter("chkTypeCR");
		String chkTypeAO = request.getParameter("chkTypeAO");
		String chkTypeCCD = request.getParameter("chkTypeCCD");

		String selectedCarriers = request.getParameter("hdnSelectedCarriers");
		SimpleDateFormat smpDateFmt = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat smpDateSql = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		Date stDate = null;
		Date stopDate = null;
		StringBuffer sb = new StringBuffer();
		sb.append("var arrAdjData = new Array();");
		Collection<AgentTransactionNominalCode> trnsColl = new HashSet<AgentTransactionNominalCode>();
		AgentTransactionsDTO agentTransDTO = new AgentTransactionsDTO();
		if (getNotNull(strHdnCode)) {
			agentTransDTO.setAgentCode(strHdnCode);
		}
		if (getNotNull(strDate) && getNotNull(strStopDate)) {

			try {
				strDate = strDate + " 00:00:00";
				strStopDate = strStopDate + " 23:59:59";
				stDate = smpDateSql.parse(strDate);
				stopDate = smpDateSql.parse(strStopDate);
				agentTransDTO.setTnxFromDate(stDate);
				agentTransDTO.setTnxToDate(stopDate);
			} catch (ParseException prse) {
				log.error("EXCEPTION IN getAdjustpaymentRowHtml methhod in HTMLGEN " + prse);
			}

		}

		if (selectedCarriers != null && !selectedCarriers.isEmpty()) {
			List<String> carrierList = Arrays.asList(selectedCarriers.split(","));
			if (!carrierList.contains("ALL")) {
				agentTransDTO.getAdjustmentCarriers().addAll(carrierList);
			}
		}

		if (getNotNull(chkPayTypeR)) {
			trnsColl.add(AgentTransactionNominalCode.ACC_SALE);
			trnsColl.add(AgentTransactionNominalCode.BSP_ACC_SALE);
			trnsColl.add(AgentTransactionNominalCode.EXT_PROD_ACC_SALE);
			trnsColl.add(AgentTransactionNominalCode.EXT_PROD_BSP_ACC_SALE);
		}
		if (getNotNull(chkPayTypeC))
			trnsColl.add(AgentTransactionNominalCode.ACC_CREDIT);
		if (getNotNull(chkPayTypeD))
			trnsColl.add(AgentTransactionNominalCode.ACC_DEBIT);
		if (getNotNull(chkPayTypeRR)) {
			trnsColl.add(AgentTransactionNominalCode.ACC_REFUND);
			trnsColl.add(AgentTransactionNominalCode.BSP_REFUND);
			trnsColl.add(AgentTransactionNominalCode.EXT_PROD_ACC_REFUND);
			trnsColl.add(AgentTransactionNominalCode.EXT_PROD_BSP_ACC_REFUND);
		}
		if (getNotNull(chkPayTypeA))
			trnsColl.add(AgentTransactionNominalCode.PAY_INV_CASH);
		if (getNotNull(chkPayTypeQ))
			trnsColl.add(AgentTransactionNominalCode.PAY_INV_CHQ);
		if (getNotNull(chkPayTypeRV))
			trnsColl.add(AgentTransactionNominalCode.ACC_PAY_REVERSE);
		if (getNotNull(chkPayTypeBSP))
			trnsColl.add(AgentTransactionNominalCode.BSP_SETTLEMENT);
		if (getNotNull(chkTypeCG))
			trnsColl.add(AgentTransactionNominalCode.ACC_COMMISSION_GRANT);
		if (getNotNull(chkTypeCR))
			trnsColl.add(AgentTransactionNominalCode.ACC_COMMISSION_REMOVE);
		if (getNotNull(chkTypeAO)) {
			trnsColl.add(AgentTransactionNominalCode.SUB_AGENT_OPERATIONS);
		}
		if (getNotNull(chkTypeCCD))
			trnsColl.add(AgentTransactionNominalCode.PAY_INV_CC);

		agentTransDTO.setAgentTransactionNominalCodes(trnsColl);

		AgentTransaction agentTransaction = null;
		Map<String, String> nominalCodes = ModuleServiceLocator.getTravelAgentBD().getAgentTransationNominalCodes();
		Collection colPayment = ModuleServiceLocator.getTravelAgentFinanceBD().getAgentTransactions(agentTransDTO);
		StringBuffer notes = null;
		if (colPayment != null && colPayment.size() > 0) {
			Object[] arrPayments = colPayment.toArray();
			for (int i = 0; i < arrPayments.length; i++) {
				agentTransaction = (AgentTransaction) arrPayments[i];
				sb.append("arrAdjData[" + i + "] = new Array();");
				sb.append("arrAdjData[" + i + "][1] = '" + smpDateFmt.format(agentTransaction.getTransctionDate()) + "';");
				if (agentTransaction.getDrOrcr().equals(WebConstants.TRANS_DEBIT)) { // set what need to be set
					sb.append("arrAdjData[" + i + "][2] = '" + decimalFormat.format(agentTransaction.getAmount()) + "';");
					sb.append("arrAdjData[" + i + "][3] = '&nbsp;';");
				} else if (agentTransaction.getDrOrcr().equals(WebConstants.TRANS_CREDIT)) {
					sb.append("arrAdjData[" + i + "][2] = '&nbsp;';");
					sb.append("arrAdjData[" + i + "][3] = '"
							+ decimalFormat.format(agentTransaction.getAmount().doubleValue() * -1) + "';");
				}
				notes = new StringBuffer();
				notes.append(agentTransaction.getNotes());
				if (agentTransaction.getReceiptNumber() != null && agentTransaction.getReceiptNumber().length() > 0) {
					notes.append(", ReceiptNo: ").append(agentTransaction.getReceiptNumber());
				}
				if (AppSysParamsUtil.isHideTypeOfPaymentInAgentSettlement() && (nominalCodes != null
						&& nominalCodes.get(String.valueOf(agentTransaction.getNominalCode())) != null)) {
					notes.append(", Type of payment: ")
							.append(nominalCodes.get(String.valueOf(agentTransaction.getNominalCode())));
				}

				sb.append("arrAdjData[" + i + "][4] = '" + removeInvalidChars(notes.toString()) + "';");
				// sb.append("arrAdjData["+i+"][6] = '"+agentTransaction.getTnxId()+"';");
				sb.append("arrAdjData[" + i + "][5] = '" + getTransactionType(agentTransaction.getNominalCode()) + " "
						+ agentTransaction.getTnxId() + "';");
				sb.append("arrAdjData[" + i + "][6] = '" + agentTransaction.getUserId() + "';");
				sb.append("arrAdjData[" + i + "][7] = '" + BeanUtils.nullHandler(agentTransaction.getAdjustmentCarrierCode())
						+ "';");
				if (agentTransaction.getReceiptNumber() != null && agentTransaction.getReceiptNumber().length() > 0) {
					sb.append("arrAdjData[" + i + "][8] = '" + agentTransaction.getReceiptNumber() + "';");
				} else {
					sb.append("arrAdjData[" + i + "][8] = '" + " " + "';");
				}
				if (agentTransaction.getCreditReversed() != null && agentTransaction.getCreditReversed() == 'Y') {
					sb.append("arrAdjData[" + i + "][9] = '" + true + "';");
				} else {
					sb.append("arrAdjData[" + i + "][9] = '" + false + "';");
				}
				if (agentTransaction.getDrOrcr() != null && agentTransaction.getNominalCode() != 14) {
					sb.append("arrAdjData[" + i + "][10] = '" + agentTransaction.getDrOrcr() + "';");
				} else {
					sb.append("arrAdjData[" + i + "][10] = '" + "" + "';");
				}
			}
		}

		return sb.toString();
	}

	private static String removeInvalidChars(String str) {
		String regex = "[^a-zA-Z0-9-_.@$!#%^&*()+/]";
		String strNew = str.replaceAll(regex, " ");
		return strNew;
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.PostPayment.Date.Null", "DateEmpty");
			moduleErrs.setProperty("um.PostPayment.FutureDate", "FutureDate");
			moduleErrs.setProperty("um.PostPayment.InvalidDate", "InvalidDate");
			moduleErrs.setProperty("um.PostPayment.Amount.null", "AmountEmpty");
			moduleErrs.setProperty("um.PostPayment.CollectPurpose.null", "PurposeEmpty");
			moduleErrs.setProperty("um.PostPayment.CollectType.Null", "TypeEmpty");
			moduleErrs.setProperty("um.PostPayment.ChqNo.null", "ChequeEmpty");
			moduleErrs.setProperty("um.ReversePayment.date.null", "DateFieldsEmpty");
			moduleErrs.setProperty("um.ReversePayment.no.updates", "NoUpdations");
			moduleErrs.setProperty("um.ReversePayment.reverse.field.blank", "ReverseFieldsBlank");
			moduleErrs.setProperty("um.ReversePayment.reverse.no.match", "NoMatch");
			moduleErrs.setProperty("um.InvoiceSettlement.Agent.null", "ISAgentNull");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.null", "ISAmountNull");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.notNegative", "ISAmountNegative");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.exceed", "ISAmountExceed");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.Zero", "InvoiceAmountZero");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.exceed.total", "AmountExceedTotal");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.not.valid", "AmountNotValid");
			moduleErrs.setProperty("um.InvoiceSettlement.Amount.Exceed.Advance", "AmountExceedAdvance");
			moduleErrs.setProperty("um.ReversePayment.Agent.null", "RPAgentNull");
			moduleErrs.setProperty("um.ReversePayment.ReverseAmount.null", "RPAmountNull");
			moduleErrs.setProperty("um.ReversePayment.TotalAmountReversed.null", "RPTotalAmountReversedNull");
			moduleErrs.setProperty("um.ReversePayment.TotalAmountReversed.Exceed", "RPTotalAmountReversedExceeds");
			moduleErrs.setProperty("um.ReversePayment.ReasonForReversal.Null", "RPReasonForReversalNull");
			moduleErrs.setProperty("um.managePayment.form.travelagent.notexist", "TravelAgentNotExist");
			moduleErrs.setProperty("um.managePayment.form.gsa.notexist", "GSANotExist");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "DeleteConfirm");

			// new messages
			moduleErrs.setProperty("um.airadmin.adjpayment.stdaynull", "startnull");
			moduleErrs.setProperty("um.airadmin.adjpayment.stpnull", "stopnull");
			moduleErrs.setProperty("um.airadmin.adjpayment.invaliddate", "invaliddate");
			moduleErrs.setProperty("um.airadmin.adjpayment.typenull", "typenull");
			moduleErrs.setProperty("um.airadmin.adjpayment.methodnull", "methodnull");
			moduleErrs.setProperty("um.airadmin.adjpayment.amountnull", "amountnull");
			moduleErrs.setProperty("um.airadmin.adjpayment.remarksnull", "remarksnull");
			moduleErrs.setProperty("um.airadmin.adjpayment.checkthebox", "checkthebox");
			moduleErrs.setProperty("um.airadmin.adjpayment.shldbegreater", "shldbegreater");
			moduleErrs.setProperty("um.airadmin.adjpayment.exceedspool", "exceedspool");
			moduleErrs.setProperty("um.airadmin.adjpayment.maintain.bsp.credit.disabled", "bspCredDisabled");
			moduleErrs.setProperty("um.xbe.topup.empty.payment.method", "emptyPaymentMethod");
			moduleErrs.setProperty("um.airadmin.adjpayment.reversed", "reversed");
			moduleErrs.setProperty("um.airadmin.adjpayment.select.credit", "selectCredit");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private boolean getNotNull(String str) {
		return ((str != null) && !(str.trim().equals(""))) ? true : false;
	}

	private String getTransactionType(int nmCode) {
		String strTrntype = "";
		switch (nmCode) {
		case 1:
			strTrntype = "R";
			break;
		case 2:
			strTrntype = "CH";
			break;
		case 3:
			strTrntype = "CQ";
			break;
		case 6:
			strTrntype = "D";
			break;
		case 7:
			strTrntype = "RR";
			break;
		case 8:
			strTrntype = "C";
			break;
		case 9:
			strTrntype = "A";
			break;
		case 14:
			strTrntype = "BSP";
			break;
		case 15:
			strTrntype = "CG";
			break;
		case 16:
			strTrntype = "CR";
			break;
		case 12:
			strTrntype = "R BSP";
			break;
		case 13:
			strTrntype = "RR BSP";
			break;
		case 23:
			strTrntype = "AO";
			break;
		default:
			break;
		}
		return strTrntype;
	}

	private String getCreditLimitInSpecifiedCurrency(Agent agent) {
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		StringBuffer currencySb = new StringBuffer();
		if ("L".equals(agent.getCurrencySpecifiedIn())) {
			currencySb.append(decimalFormat.format(agent.getCreditLimitLocal() != null ? agent.getCreditLimitLocal() : 0));
			currencySb.append(" ");
			currencySb.append(agent.getCurrencyCode());
		} else {
			currencySb.append(decimalFormat.format(agent.getCreditLimit() != null ? agent.getCreditLimit() : 0));
			currencySb.append(" ");
			currencySb.append(agent.getCurrencyCode());
		}
		return currencySb.toString();
	}
}
