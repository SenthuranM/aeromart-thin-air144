package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InterlineBlockSeatRQInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBEException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.xbe.core.web.v2.util.SessionClearUtill;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;

/**
 * Action to block seat for privileged users.
 * 
 * @author mekanayake
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class BlockSeatAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(BlockSeatAction.class);

	private FlightSearchDTO fareQuoteParams;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private String messageTxt;

	private String flexiSelected;

	private String selectedSystem;

	private boolean success = true;

	private String groupPNR;

	private boolean allowBlockSeats = false;

	private boolean allowBufferTimeOnHold = true;

	private boolean requote;

	private boolean requoteOverbook = false;
	
	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			ReservationProcessParams rpParams = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);

			// Onhold validation
			PriceInfoTO priceInfoTO = bookingShoppingCart.getPriceInfoTO();
			FareTypeTO fare = priceInfoTO.getFareTypeTO();
			if (log.isDebugEnabled()) {
				log.debug(" On hold times - start time: " + fare.getOnHoldStartTime() + ", end time: " + fare.getOnHoldStopTime());
			}
			long bufferStartTime = fare.getOnHoldStartTime().getTime();
			long bufferEndTime = fare.getOnHoldStopTime().getTime();
			long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
			if (currentTime > bufferEndTime) {
				allowBufferTimeOnHold = false;
			}
			if (currentTime > bufferStartTime && !rpParams.isBufferTimeOnHold()) {
				allowBufferTimeOnHold = false;
			}
			if (ReservationUtil.isGroupPnr(groupPNR)) {
				selectedSystem = SYSTEM.INT.toString();
			}

			if ((groupPNR == null || "".equals(groupPNR.trim())) && !SYSTEM.getEnum(selectedSystem).equals(SYSTEM.INT)) {
				// temporarily skip block seats for requote
				if (BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_SEATBLOCKING)
						&& BookingClass.BookingClassType.NORMAL.equals(fareQuoteParams.getBookingType()) && !requoteOverbook) {
					if (log.isDebugEnabled()) {
						log.debug("Block seat for the priviladge user");
					}

					if (log.isDebugEnabled()) {
						log.debug("releasing the previously added blosk seats if exist");
					}
					SessionClearUtill.releaseBlocSeat(request);// Audit action is set. Release existing block
																// seats

					resInfo.getSelectedSystem();
					if (fareQuoteParams.isOpenReturnConfirm()) {
						fareQuoteParams.setDepartureDate(fareQuoteParams.getReturnDate());
					}
					FlightPriceRQ flightPriceRQ = null;
					if (outFlightRPHList == null && retFlightRPHList == null) {
						flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, AppIndicatorEnum.APP_XBE);
					} else {
						flightPriceRQ = ReservationBeanUtil
								.createFareQuoteRQ(fareQuoteParams, outFlightRPHList, retFlightRPHList);
					}
					flightPriceRQ.getAvailPreferences()
							.setAllowDoOverbookAfterCutOffTime(
									BasicRH.hasPrivilege(request,
											PriviledgeConstants.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME));
					FareSegChargeTO fareSegChargeTO = bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO();
					boolean isFlexiQuote = ReservationUtil.isFlexiSelected(flexiSelected);
					Collection blockSeatIds = ModuleServiceLocator.getAirproxySearchAndQuoteBD().priviledgeUserBlockSeat(
							flightPriceRQ, fareSegChargeTO);
					if (log.isDebugEnabled()) {
						log.debug("BlockSeatUID generated [blockSeatUID = " + blockSeatIds + "]");
					}

					// adding the block seats to the session
					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, blockSeatIds);
					fareSegChargeTO.setBlockSeatIds(blockSeatIds);
					if (!requote && (blockSeatIds == null || blockSeatIds.size() == 0)) {
						throw new XBEException(WebConstants.KEY_BLOCK_SEATS_FAILED);
					}
				} else {
					success = true;
				}
			} else if (SYSTEM.getEnum(selectedSystem).equals(SYSTEM.INT)) {
				if (allowBlockSeats == true) {
					String trxID = resInfo.getTransactionId();

					InterlineBlockSeatRQInfoTO infoTO = new InterlineBlockSeatRQInfoTO();
					infoTO.setOutFlightRPHList(outFlightRPHList);
					infoTO.setRetFlightRPHList(retFlightRPHList);
					infoTO.setTransactionIdentifier(trxID);

					boolean isSuccess = ModuleServiceLocator.getLCCSearchAndQuoteBD().priviledgeUserBlockSeat(infoTO,
							getTrackInfo());
					if (!isSuccess) {
						throw new XBEException(WebConstants.KEY_BLOCK_SEATS_FAILED);
					}
				} else {
					success = true;
				}
			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public void setRequote(boolean requote) {
		this.requote = requote;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	@JSON(serialize = false)
	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	@JSON(serialize = false)
	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setFlexiSelected(String flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	/**
	 * @param selectedSystem
	 *            the selectedSystem to set
	 */
	public void setSelectedSystem(String selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAllowBlockSeats() {
		return allowBlockSeats;
	}

	public void setAllowBlockSeats(boolean allowBlockSeats) {
		this.allowBlockSeats = allowBlockSeats;
	}

	public boolean isAllowBufferTimeOnHold() {
		return allowBufferTimeOnHold;
	}

	public void setAllowBufferTimeOnHold(boolean allowBufferTimeOnHold) {
		this.allowBufferTimeOnHold = allowBufferTimeOnHold;
	}

	public void setRequoteOverbook(boolean requoteOverbook) {
		this.requoteOverbook = requoteOverbook;
	}

}
