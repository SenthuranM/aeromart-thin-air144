/**
 * 
 */
package com.isa.thinair.xbe.core.web.handler.requestmanagement;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author suneth
 *
 */
public class RequestSubmitPopupHandler extends BasicRH{
	
	/**
	 * Main Execute Method for RequestSubmitPopup Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = S2Constants.Result.SUCCESS;
		
		try {
			
			setDisplayData(request);
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			forward = "error";

		}
		
		return forward;
	}
	
	private static void setDisplayData(HttpServletRequest request) throws ModuleException{
		
		String pageId = request.getParameter("pageId");
		String queuesForPage;
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT Q.QUEUE_ID ,");
		sb.append(" Q.QUEUE_NAME ");
		sb.append("FROM T_QUEUE Q ");
		sb.append("INNER JOIN T_QUEUE_PAGE QP ");
		sb.append("ON Q.QUEUE_ID  = QP.QUEUE_ID ");
		sb.append("WHERE STATUS   ='ACT' ");		
		sb.append("AND QP.PAGE_ID = '"+ pageId +"' ");
		sb.append("ORDER BY QUEUE_NAME");
		
		String query = sb.toString();
	
		queuesForPage = SelectListGenerator.createQueueListForPage(query);
		request.setAttribute(WebConstants.REQ_HTML_QUEUES_FOR_PAGE, queuesForPage);

	}

}
