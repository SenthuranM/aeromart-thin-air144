package com.isa.thinair.xbe.core.web.generator.system;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class LoginHTMLGenerator {
	private static String clientErrors;

	public static String getClientErrors(HttpServletRequest request) {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("cc_null", "XBE-ERR-01");
			moduleErrs.setProperty("cc_invalid", "XBE-ERR-04");
			moduleErrs.setProperty("cc_no_child_only_resrvations", "TAIR-90144");
			moduleErrs.setProperty("cc_inactive_user", "XBE-ERR-94");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
