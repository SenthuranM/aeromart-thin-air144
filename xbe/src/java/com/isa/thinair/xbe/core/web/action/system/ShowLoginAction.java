package com.isa.thinair.xbe.core.web.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.system.ShowLoginRH;

@Namespace(S2Constants.Namespace.PUBLIC)
@Results({ @Result(name = S2Constants.Result.SHOW_LOGIN, value = S2Constants.Jsp.System.LOGIN),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowLoginAction extends BaseRequestAwareAction {

	public String execute() {
		return ShowLoginRH.execute(request);
	}
}
