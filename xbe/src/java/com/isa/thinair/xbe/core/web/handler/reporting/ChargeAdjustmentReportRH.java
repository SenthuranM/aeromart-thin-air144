package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Request handler for the charge adjustments report.
 * 
 * @author sanjaya
 * 
 */
public class ChargeAdjustmentReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(ChargeAdjustmentReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");

		String currentTimestamp = DateUtil.formatDate(new Date(), "dd/MM/yyyy");
		setAttribInRequest(request, "systemDate", currentTimestamp);

		String offlineReportParams = AppSysParamsUtil.getEnableOfflineReportParams();

		setAttribInRequest(request, "offlineReportParams", offlineReportParams);

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);

				if (log.isDebugEnabled()) {
					log.debug("ChargeAdjustmentReportRH setReportView Success");
				}
				return null;
			} else {
				log.error("ChargeAdjustmentReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ChargeAdjustmentReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		String id = "UC_REPM_028";
		String reportTemplate = "ChargeAdjustmentsReport.jasper";

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");

		String agents;
		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}

		String reportType = request.getParameter("radReportOption");
		String reportNumFormat = request.getParameter("radRptNumFormat");

		List<String> selectedAgents = new ArrayList<String>();
		if (!agents.trim().isEmpty()) {
			String agentList[] = agents.split(",");
			for (String agent : agentList) {
				selectedAgents.add(agent);
			}
		}

		String selectedChargeCodes = request.getParameter("hdnChargeAdjustmentChargeCodes");

		List<String> selectedChargeAdjustmentChargeCodes = new ArrayList<String>();
		if (StringUtils.isNotEmpty(selectedChargeCodes)) {
			String chargeCodeList[] = selectedChargeCodes.split(",");
			for (String chargeCode : chargeCodeList) {
				selectedChargeAdjustmentChargeCodes.add(chargeCode);
			}
		}

		try {
			ReportsSearchCriteria searchCriteria = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (fromDate != null && !fromDate.equals("")) {
				searchCriteria.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}
			if (toDate != null && !toDate.equals("")) {
				searchCriteria.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59");
			}

			searchCriteria.setAgents(selectedAgents);

			// TODO take this from the user. === AKA introduce a multi select box.
			searchCriteria.setSelectedChargeCodes(selectedChargeAdjustmentChargeCodes);

			String offlineReportParams = AppSysParamsUtil.getEnableOfflineReportParams();
			String[] reportParams = offlineReportParams.split(",");
			if (reportParams[0].equals("Y")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
				Date reportFromDate = DateUtil.parseDate(searchCriteria.getDateRangeFrom(), "dd-MMM-yyyy");
				int noOfLiveReportDays = Integer.parseInt(reportParams[1]);

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_YEAR, -1 * noOfLiveReportDays);
				Date validReportFromDate = dateFormat.parse(dateFormat.format(cal.getTime()));
				if (validReportFromDate.compareTo(reportFromDate) > 0) {
					searchCriteria.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				} else {
					searchCriteria.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				}
			}

			ResultSet resultSet = null;

			resultSet = ModuleServiceLocator.getDataExtractionBD().getChargeAdjustmentsData(searchCriteria);

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);

			// To provide Report Format Options
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (reportType.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (reportType.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=ChargeAdjustmentsReport.pdf");
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=ChargeAdjustmentsReport.xls");
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=ChargeAdjustmentsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}

		} catch (Exception e) {
			log.error("setReportView() method is failed :" + e.getMessage());
		}

	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setDisplayAgencyMode(request);
		setAgentTypes(request);
		setClientErrors(request);
		setGSAMultiSelectList(request);
		setCustomChargeAdjustmentChargeCodesMultiSelectList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.ta.comp.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.ta.comp.rpt") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.ta.comp") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setCustomChargeAdjustmentChargeCodesMultiSelectList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createChargeAdjustmentTypesHtml();
		request.setAttribute(WebConstants.REQ_HTML_CUSTOM_CHARGE_ADJUSTMENT_CHARGE_CODES_LIST, strList);
	}

	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}
		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}
}
