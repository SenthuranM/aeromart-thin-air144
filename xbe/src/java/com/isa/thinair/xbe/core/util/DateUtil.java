package com.isa.thinair.xbe.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.xbe.core.config.XBEModuleConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.sun.tools.ws.processor.model.ModelException;

public final class DateUtil {

	private static Log log = LogFactory.getLog(DateUtil.class);

	private static XBEModuleConfig config = XBEModuleUtils.getConfig();

	public static String DEFAULT_DATE_INPUT_FORMAT = "dd/MM/yyyy";

	/**
	 * formats the date using the default format
	 * 
	 * @param date
	 * @return String
	 */
	public static String formatDate(Date date) {
		String formatedDate = "";

		if (date != null) {
			formatedDate = formatDate(date, config.getDefaultDateFormat());
		}

		return formatedDate;
	}

	/**
	 * Formats specified date with specified format
	 * 
	 * @return String the formated date
	 */
	public static String formatDate(Date utilDate, String fmt) {
		String formatedDate = "";

		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}

	public static Date parseDate(String strDate) throws ParseException {
		SimpleDateFormat sdFmt = new SimpleDateFormat(DEFAULT_DATE_INPUT_FORMAT);
		return sdFmt.parse(strDate);
	}
	
	/**
	 * 
	 * @param strDate
	 * @return
	 * @throws ParseException
	 * @author nafly
	 */
	public static Calendar convertStringToCalendar(String strDate) throws ModelException {
		SimpleDateFormat sdFmt = new SimpleDateFormat(DEFAULT_DATE_INPUT_FORMAT);
		Calendar calendar = Calendar.getInstance();

		try {
			Date date = sdFmt.parse(strDate);
			calendar.setTime(date);
		} catch (ParseException e) {
			if (log.isErrorEnabled()) {
				log.error("Error while converting String Date to Calendar", e);
			}
			throw new ModelException(e.getMessage(), e);
		}

		return calendar;
	}

}
