package com.isa.thinair.xbe.core.web.handler.checkin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class PFSProcessingRH extends BasicRH {

	private static Log log = LogFactory.getLog(PFSProcessingRH.class);
	private static XBEConfig xbeConfig = new XBEConfig();
	private static String clientErrors;

	private static final String PFS_MANNUAL_CHECKIN_METHOD = "MANNUAL_CHECKIN";

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = S2Constants.Result.SUCCESS;
		String hdnMode;
		String paxTitle;
		String paxType;
		String paxFirstName;
		String paxLastName;
		String paxStatus;
		String paxDestination;
		String paxCOS;
		String paxCategory;
		Integer flightId;
		Integer flightSegId;
		String flightNumber;
		String fromAirport;
		String paxPnr;
		String parentPaxType;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			flightSegId = new Integer(request.getParameter(WebConstants.REQ_SEGMENT_ID));
			flightId = new Integer(request.getParameter(WebConstants.REQ_FLIGHT_ID));
			paxPnr = request.getParameter(WebConstants.REQ_PAX_PNR);
			parentPaxType = request.getParameter(WebConstants.REQ_PAX_PNR_TYPE);

			ArrayList<Integer> flightSegIds = new ArrayList<Integer>();
			flightSegIds.add(flightSegId);
			FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) ModuleServiceLocator.getFlightServiceBD()
					.getFlightSegments(flightSegIds).toArray()[0];
			Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);

			fromAirport = flight.getOriginAptCode();
			flightNumber = flight.getFlightNumber();

			// Create PFS if not present
			Date flightDate = flightSegmentDTO.getDepartureDateTime();
			int presentPfsiD = ModuleServiceLocator.getReservationBD().getPFS(fromAirport, flightNumber, flightDate);
			Pfs presentPfs = ModuleServiceLocator.getReservationBD().getPFS(presentPfsiD);

			hdnMode = request.getParameter(WebConstants.PFS_HDN_MODE);
			setExceptionOccured(request, false);

			if (hdnMode != null && !hdnMode.equals("") && hdnMode.equals("SAVE")) {
				paxTitle = request.getParameter(WebConstants.PAX_TITLE);
				paxType = request.getParameter(WebConstants.PAX_TYPE);
				paxFirstName = request.getParameter(WebConstants.PAX_FIRST_NAME);
				paxLastName = request.getParameter(WebConstants.PAX_LAST_NAME);
				paxStatus = request.getParameter(WebConstants.PAX_STATUS);
				paxDestination = request.getParameter(WebConstants.PAX_DESTINATION);
				paxCOS = request.getParameter(WebConstants.PAX_COS);
				paxCategory = request.getParameter(WebConstants.PAX_CATEGORY);

				if (presentPfs == null) {
					// Error situation. Pfs should be created in the previous state.
					setIntSuccess(request, 1);
					saveMessage(request, xbeConfig.getMessage(WebConstants.ERROR_SYSTEM_ERROR, userLanguage), WebConstants.MSG_ERROR);
					setExceptionOccured(request, true);
					setDisplayData(request);
					return forward;
				}

				// Create PFS parsed data
				PfsPaxEntry pfsPaxEntry = new PfsPaxEntry();
				pfsPaxEntry.setArrivalAirport(paxDestination);
				pfsPaxEntry.setCabinClassCode(paxCOS);
				pfsPaxEntry.setDepartureAirport(fromAirport);
				pfsPaxEntry.setEntryStatus(paxStatus);
				pfsPaxEntry.setFirstName(paxFirstName);
				pfsPaxEntry.setLastName(paxLastName);
				pfsPaxEntry.setFlightDate(flightDate);
				pfsPaxEntry.setFlightNumber(flightNumber);
				pfsPaxEntry.setPaxType(paxType);
				pfsPaxEntry.setPaxCategoryCode(paxCategory);
				pfsPaxEntry.setTitle(paxTitle);
				pfsPaxEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED);
				pfsPaxEntry.setPfsId(presentPfs.getPpId());
				pfsPaxEntry.setReceivedDate(new Date());

				if (pfsPaxEntry.getPaxType() != null && pfsPaxEntry.getPaxType().equals("IN")) {
					if (paxPnr == null || paxPnr.equals("") || parentPaxType == null || !parentPaxType.equals("AD")) {
						// throw new ModuleException("airreservations.add.infant.Invalid");
						throw new XBERuntimeException("airreservations.add.infant.Invalid");
					}
					pfsPaxEntry.setPnr(paxPnr);

					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(paxPnr);
					pnrModesDTO.setLoadFares(true);
					pnrModesDTO.setLoadSegView(true);
					pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
					Reservation infParentReservation = ReservationProxy.getReservation(pnrModesDTO);

					if (infParentReservation.getTotalPaxInfantCount() >= 1) {
						// throw new ModuleException("airreservations.adult.exceeds.infant");
						throw new XBERuntimeException("airreservations.add.infant.Invalid");
					}
				}

				ModuleServiceLocator.getReservationBD().savePfsParseEntry(presentPfs, pfsPaxEntry);
				// PfsParsedEntry saved successfully.

				ServiceResponce serviceResponce = ModuleServiceLocator.getReservationBD().reconcileReservations(
						presentPfs.getPpId(), true, false, PFS_MANNUAL_CHECKIN_METHOD,
						AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
				if (serviceResponce != null && serviceResponce.isSuccess()) {
					// Handle success
					setIntSuccess(request, 0);
					saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_ADD_SUCCESS, null), WebConstants.MSG_SUCCESS);
					setExceptionOccured(request, false);
				} else {
					// Handle error
					setIntSuccess(request, 1);
					saveMessage(request, serviceResponce.getResponseCode(), WebConstants.MSG_ERROR);
					setExceptionOccured(request, true);
					// Load again for deletion
					presentPfs = ModuleServiceLocator.getReservationBD().getPFS(presentPfs.getPpId());
					Collection pfsParsedEntries = ModuleServiceLocator.getReservationBD()
							.getPfsParseEntries(presentPfs.getPpId());
					for (Iterator<PfsPaxEntry> iterator = pfsParsedEntries.iterator(); iterator.hasNext();) {
						PfsPaxEntry paxEntry = iterator.next();
						// Delete the record by name.
						if (isSamePFSParsedEntry(pfsPaxEntry, paxEntry)) {
							ModuleServiceLocator.getReservationBD().deletePfsParseEntry(presentPfs, paxEntry);
						}
					}
				}
			} else {
				if (presentPfs == null) {
					presentPfs = new Pfs();
					presentPfs.setDateDownloaded(new Date());// TODO get from jsp file
					presentPfs.setDepartureDate(flightDate);
					presentPfs.setFlightNumber(flightNumber);
					presentPfs.setFromAirport(fromAirport);
					presentPfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);
					ModuleServiceLocator.getReservationBD().savePfs(presentPfs);
				}
			}

		} catch (Exception exception) {
			log.error("PFSProcessingRH.execute() method is failed :" + exception.getMessage(), exception);
			saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			setExceptionOccured(request, true);
		} finally {
			try {
				setDisplayData(request);
			} catch (Exception innerException) {
				log.error("PFSProcessingRH.execute() method is failed :" + innerException.getMessage(), innerException);
				saveMessage(request, innerException.getMessage(), WebConstants.MSG_ERROR);
				setExceptionOccured(request, true);
			}
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setTitleList(request);
		setPaxTypesList(request);
		setCabinClassHtml(request);
		setPaxCategory(request);
		setDestinationAirportList(request);
		setClientErrors(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	private static void setTitleList(HttpServletRequest request) throws ModuleException {
		String arrTitles = SelectListGenerator.createPaxTitleArray();
		request.setAttribute(WebConstants.REQ_PAX_TITLES, arrTitles);
	}

	private static void setPaxTypesList(HttpServletRequest request) throws ModuleException {
		String arrPaxTypes = SelectListGenerator.createPaxTypesArray();
		request.setAttribute(WebConstants.REQ_PAX_TYPE, arrPaxTypes);
	}

	private static void setCabinClassHtml(HttpServletRequest request) throws ModuleException {
		String strCabinClassList = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strCabinClassList);
	}

	private static void setPaxCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createPaxCatagoryList();
		request.setAttribute(WebConstants.REQ_PAXCAT_LIST, strHtml);
	}

	private static void setDestinationAirportList(HttpServletRequest request) throws Exception {

		Integer flightSegId = new Integer(request.getParameter(WebConstants.REQ_SEGMENT_ID));
		Integer flightId = new Integer(request.getParameter(WebConstants.REQ_FLIGHT_ID));

		ArrayList<Integer> flightSegIds = new ArrayList<Integer>();
		flightSegIds.add(flightSegId);

		try {
			StringBuffer sb = new StringBuffer();
			FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) ModuleServiceLocator.getFlightServiceBD()
					.getFlightSegments(flightSegIds).toArray()[0];
			Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);
			List<String> destinationsList = ModuleServiceLocator.getFlightServiceBD().getPossibleDestinations(
					flight.getFlightNumber(), flightSegmentDTO.getDepartureDateTime(), flightSegmentDTO.getFromAirport());
			;
			Iterator<String> iterator = destinationsList.iterator();
			while (iterator.hasNext()) {
				String destination = iterator.next();
				sb.append("<option value='" + destination + "'>" + destination + "</option>");
			}
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, sb.toString());

		} catch (ModuleException moduleException) {
			log.error("PFSProcessingRH.setDestinationAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception exception) {
			log.error("PFSProcessingRH.setDestinationAirportList() method is failed :" + exception.getMessage(), exception);
			saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
		}
	}

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	/**
	 * Sucess Values: 0 - Saved Successfuly -1 - Error occurred - No releavant field identified -2 - Error occurred -
	 * Pax Title -3 - Error occurred - Pax First Name -4 - Error occurred - Pax Last Name -5 - Error occurred -
	 * Destination -6 - Error occurred - Status
	 */
	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static boolean isSamePFSParsedEntry(PfsPaxEntry oldEntry, PfsPaxEntry newEntry) {
		if (oldEntry.getArrivalAirport().equals(newEntry.getArrivalAirport())
				&& oldEntry.getCabinClassCode().equals(newEntry.getCabinClassCode())
				&& oldEntry.getDepartureAirport().equals(newEntry.getDepartureAirport())
				&& oldEntry.getFirstName().equals(newEntry.getFirstName())
				&& oldEntry.getLastName().equals(newEntry.getLastName())
				&& oldEntry.getPaxCategoryCode().equals(newEntry.getPaxCategoryCode())
				&& oldEntry.getPaxType().equals(newEntry.getPaxType()) && oldEntry.getTitle().equals(newEntry.getTitle())
				&& newEntry.getPnr() == null) {
			return true;
		}
		return false;
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("PFSProcessingRH.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.checkin.pax.title.required", "paxTitleRequired");
			moduleErrs.setProperty("um.checkin.pax.firstname.required", "paxFirstNameRequired");
			moduleErrs.setProperty("um.checkin.pax.lastname.required", "paxLastNameRequired");
			moduleErrs.setProperty("um.checkin.pax.destination.required", "destinationRequired");
			moduleErrs.setProperty("airreservations.add.infant.Invalid", "infantWithoutAdult");
			moduleErrs.setProperty("airreservations.adult.exceeds.infant", "infantCountExceeds");
			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
