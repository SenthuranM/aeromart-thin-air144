package com.isa.thinair.xbe.core.config;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.v2.util.I18nUtil;
import com.isa.thinair.xbe.core.web.v2.util.XbeI18nKeys;

public class XBEConfig {

	/**
	 * @author Latiff Keeps configurations used in xbe module
	 */
	private static Log log = LogFactory.getLog(XBEConfig.class);

	private static final String URL_PRIVILEDGES_RESOURCE_BUNDLE_NAME = "resources/authorization/UrlPriviledgesMappings";
	private static final String DEFULT_CLIENT_MESSAGE_CODE = "default.client.messsage";
	private static final String DEFULT_SERVER_MESSAGE_CODE = "default.server.messsage";
	public static final String DEFULT_SERVER_ERROR_CODE = "default.server.error";

	private static final ResourceBundle urlPriviledgesMappings = loadResourceBundle(URL_PRIVILEDGES_RESOURCE_BUNDLE_NAME);
	
	// Menaka
	private static final String DEFULT_USER_MESSAGE_CODE = "cc.module.usermessage.code.empty";
	// ResourceBundle userMessagesCodes;
	// Menaka

	public static final String getServerMessage(String messageCode,String usrLanguage, String errorMsg) {
		if (usrLanguage == null) {
			usrLanguage = WebConstants.DEFAULT_LANGUAGE;
		}

		if (messageCode == null || messageCode.equals("")) {
			return I18nUtil.getMessage(DEFULT_SERVER_MESSAGE_CODE, usrLanguage, XbeI18nKeys.SERVER_MESSAGES, errorMsg);
		} else {
			return I18nUtil.getMessage(messageCode, usrLanguage, XbeI18nKeys.SERVER_MESSAGES, errorMsg);
		}

	}

	// Menaka
	public String getMessage(String messageCode, String userLanguage) {

		if (userLanguage == null) {
			userLanguage = WebConstants.DEFAULT_LANGUAGE;
		}

		if (messageCode == null || messageCode.equals(""))

			return I18nUtil.getMessage(DEFULT_USER_MESSAGE_CODE, userLanguage, XbeI18nKeys.CLIENT_MESSAGES, null);

		if (messageCode.startsWith("cc.")) {

			String message = I18nUtil.getMessage(messageCode, userLanguage, XbeI18nKeys.CLIENT_MESSAGES, null);

			if (message != null)
				return message;

		}

		return I18nUtil.getMessage(DEFULT_USER_MESSAGE_CODE, userLanguage, XbeI18nKeys.CLIENT_MESSAGES, null);
	}

	// Menaka
	public static final String getClientMessage(String messageCode, String userLanguage) {

		if (userLanguage == null) {
			userLanguage = WebConstants.DEFAULT_LANGUAGE;
		}
		if (messageCode == null || messageCode.equals("")) {
			return I18nUtil.getMessage(DEFULT_CLIENT_MESSAGE_CODE, userLanguage,XbeI18nKeys.CLIENT_MESSAGES, null);

		} else {
			return I18nUtil.getMessage(messageCode, userLanguage,XbeI18nKeys.CLIENT_MESSAGES, null);
		}
	}

	private static final ResourceBundle loadResourceBundle(String bundleName) {
		ResourceBundle messageCodes = null;
		Locale locale = new Locale("en");

		try {
			messageCodes = ResourceBundle.getBundle(bundleName, locale);

		} catch (RuntimeException re) {
			log.fatal("Cannot load the resouce bundle:" + bundleName + ":" + re.getMessage());
			;
			throw re;
		}

		return messageCodes;
	}

	public static final String getUrlPriviledge(String url) {
		String priviledge = null;

		try {
			priviledge = urlPriviledgesMappings.getString(url);
		} catch (RuntimeException e) {
			if (!(e instanceof MissingResourceException)) {
				throw e;
			}
		}

		return priviledge;
	}
}

