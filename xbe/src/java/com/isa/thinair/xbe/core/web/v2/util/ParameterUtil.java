package com.isa.thinair.xbe.core.web.v2.util;

import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class ParameterUtil {
	
	public static boolean isAdminFeeEnabledForXBE(String agentCode) {
		return AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, agentCode);
	}

}
