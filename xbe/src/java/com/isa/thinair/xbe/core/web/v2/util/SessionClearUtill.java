package com.isa.thinair.xbe.core.web.v2.util;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * used to clear the session info when exiting from the reservation flow
 * 
 * @author mekanayake
 * 
 */
public class SessionClearUtill {

	private static Log log = LogFactory.getLog(SessionClearUtill.class);

	/**
	 * cleaning the session when exiting form the reservation flow.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void releaseBlocSeatAndCleanUpSession(HttpServletRequest request) throws ModuleException {

		HttpSession httpSession = request.getSession();

		// release block seats
		releaseBlocSeat(request);

		// clean the booking shopping cart
		if (httpSession.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART) != null) {
			if (log.isDebugEnabled()) {
				log.debug("Clearing the session setting new BookingShoppingCart");
			}
			httpSession.setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, new BookingShoppingCart(
					ChargeRateOperationType.MAKE_ONLY));
		}

		// clearing the resInfo
		if (httpSession.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA) != null) {
			if (log.isDebugEnabled()) {
				log.debug("Clearing the session setting new XBEReservationInfoDTO");
			}
			httpSession.setAttribute(S2Constants.Session_Data.XBE_SES_RESDATA, new XBEReservationInfoDTO());
		}

		// clearing the ReservationProcessParams
		if (httpSession.getAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS) != null) {
			if (log.isDebugEnabled()) {
				log.debug("Clearing the session setting ReservationProcessParams");
			}
			httpSession.setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, null);
		}
	}

	public static void releaseBlocSeat(HttpServletRequest request) throws ModuleException {
		if (BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_SEATBLOCKING)) {
			HttpSession httpSession = request.getSession();
			SYSTEM system = SYSTEM.getEnum(request.getParameter("system"));
			if (log.isDebugEnabled()) {
				log.debug("RELEASING BLOCK SEATS FOR system = [" + system + "]");
			}
			if (system != SYSTEM.INT && httpSession.getAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS) != null) {
				Collection collBlockIDs = (Collection) httpSession.getAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS);
				if (collBlockIDs != null) {
					if (log.isDebugEnabled()) {
						log.debug("Releasing block seats size = " + collBlockIDs.size());
					}
					ModuleServiceLocator.getReservationBD().releaseBlockedSeats(collBlockIDs);
					collBlockIDs = null;
					httpSession.setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, collBlockIDs);
				}
			}
			if (system == SYSTEM.INT && httpSession.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA) != null) {
				String transactionID = ((XBEReservationInfoDTO) request.getSession().getAttribute(
						S2Constants.Session_Data.XBE_SES_RESDATA)).getTransactionId();
				if (transactionID != null) {
					ModuleServiceLocator.getLCCSearchAndQuoteBD().removeBlockSeats(transactionID,
							TrackInfoUtil.getBasicTrackInfo(request));
				}
			}
		}
	}

}
