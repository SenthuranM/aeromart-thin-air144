package com.isa.thinair.xbe.core.web.action.travagent;

import java.math.BigDecimal;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.googlecode.jsonplugin.JSONResult;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.ReservationPostPaymentDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class GenerateCCTopupEPGWFormRequestAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(GenerateCCTopupEPGWFormRequestAction.class);

	private String pgwId;

	private String selectedCurrency;

	private String pgwBrokerType;

	private boolean success = true;

	private String messageTxt;

	private String amount;
	
	private String remarks;

	private ContactInfoTO contactInfo;

	private String redirectionString;

	private String redirectionMechanism;

	private String minimumTopupAmount;

	private static final String RETURN_URL = AppSysParamsUtil.getAirlineReservationURL()
			+ "/xbe/private/handleCCTopupEPGWResponse.action";
	private static final String REDIRECTION_MECHANISM = "REDIRECTION_MECHANISM";
	private static final String BROKER_TYPE = "BROKER_TYPE";

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO;
		Properties ipgProps;
		String brokerType = null;
		try {

			validateParameters();

			checkMinimumAgentTopupConstraints();

			ipgIdentificationParamsDTO = WebplatformUtil.prepareIPGConfigurationParamsDTOPerPGWId(new Integer(pgwId));

			ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
			brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
			if (brokerType != null) {
				pgwBrokerType = brokerType;
			}

			TrackInfoDTO trackInfoDTO = this.getTrackInfo();
			ReservationContactInfo contactInfoTO = getContactInfoTO();
			PaymentInfo paymentInfo = getPaymentInfo(ipgIdentificationParamsDTO);
			Integer tempPayId = null;
			tempPayId = ModuleServiceLocator.getTravelAgentFinanceBD().makeTemporyPaymentEntry(contactInfoTO, paymentInfo,
					trackInfoDTO, true, false);
			IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
			ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
			ipgRequestDTO.setAmount(getPayAmount(ipgIdentificationParamsDTO).toString());
			ipgRequestDTO.setReturnUrl(RETURN_URL);
			ipgRequestDTO.setApplicationIndicator(AppIndicatorEnum.APP_XBE);
			ipgRequestDTO.setSessionID(request.getSession().getId());
			ipgRequestDTO.setApplicationTransactionId(tempPayId);

			ipgRequestDTO.setContactFirstName(contactInfoTO.getFirstName());
			ipgRequestDTO.setEmail(contactInfoTO.getEmail());
			if (contactInfoTO.getMobileNo() != null) {
				ipgRequestDTO.setContactMobileNumber(contactInfoTO.getMobileNo());
			} else if (contactInfoTO.getPhoneNo() != null) {
				ipgRequestDTO.setContactMobileNumber(contactInfoTO.getPhoneNo());
				ipgRequestDTO.setContactPhoneNumber(contactInfoTO.getPhoneNo());
			}
			IPGRequestResultsDTO ipgRequestResultsDTO = ModuleServiceLocator.getPaymentBrokerBD().getRequestData(ipgRequestDTO);
			this.redirectionString = ipgRequestResultsDTO.getRequestData();
			this.redirectionMechanism = (String) ipgRequestResultsDTO.getPostDataMap().get(REDIRECTION_MECHANISM);

			ReservationPostPaymentDTO postPayData = new ReservationPostPaymentDTO();
			postPayData.setPaymentBrokerRefNo(ipgRequestResultsDTO.getPaymentBrokerRefNo());
			postPayData.setIpgId(new Integer(pgwId));
			postPayData.setCurrencyCode(ipgIdentificationParamsDTO.getPaymentCurrencyCode());

			IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
			ipgResponseDTO.setBrokerType((String) ipgRequestResultsDTO.getPostDataMap().get(BROKER_TYPE));
			ipgResponseDTO.setTemporyPaymentId(tempPayId);
			ipgResponseDTO.setPaymentBrokerRefNo(ipgRequestResultsDTO.getPaymentBrokerRefNo());
			postPayData.setIpgResponseDTO(ipgResponseDTO);
			request.getSession().setAttribute(S2Constants.Session_Data.POST_PAY_DATA, postPayData);
			request.getSession().setAttribute(S2Constants.Session_Data.SELF_TOPUP_AMOUNT, this.getAmount());
			request.getSession().setAttribute(S2Constants.Session_Data.SELF_TOPUP_REMARKS, this.getRemarks());
			request.getSession().setAttribute(S2Constants.Session_Data.SELF_TOPUP_SEL_CURR,this.getSelectedCurrency());

		} catch (ModuleException me) {
			log.error("Module exception while processing : " + me.getExceptionCode());
			messageTxt = BasicRH.getErrorMessage(me, userLanguage);
			success = false;

		} catch (Exception e) {
			log.error("Error generating external PGW response : " + e);
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			success = false;

		}

		return S2Constants.Result.SUCCESS;
	}

	private boolean isNullSafe(String value) {
		if (value == null || value.trim().isEmpty()) {
			return false;
		}
		return true;
	}

	private void validateParameters() throws ModuleException {
		if (!isNullSafe(pgwId) || !isNullSafe(amount) || !isNullSafe(selectedCurrency)) {
			throw new ModuleException("common.invalid.input.parameters");
		}

	}

	private void checkMinimumAgentTopupConstraints() throws ModuleException {
		minimumTopupAmount = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.getDefaultBigDecimalZero());
		String strAmount = AppSysParamsUtil.getAgentTopupMinAmount();
		if (isNullSafe(strAmount)) {
			BigDecimal treshold = new BigDecimal(strAmount);
			boolean displayInAgentCurrency = !selectedCurrency.equals(AppSysParamsUtil.getBaseCurrency());
			if (displayInAgentCurrency) {
				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				treshold = exchangeRateProxy.convert(selectedCurrency, treshold);
			}
			minimumTopupAmount = AccelAeroCalculator.formatAsDecimal(treshold);
    		boolean topupAmountIsLessThanMinimum = new BigDecimal(amount).compareTo(new BigDecimal(minimumTopupAmount)) == -1;
			if (topupAmountIsLessThanMinimum) {
				throw new ModuleException("xbe.agent.not.exceeding.topup.minimum.amount");
			}
		}
	}

	private ReservationContactInfo getContactInfoTO() throws Exception {

		ReservationContactInfo contactInfoTO = new ReservationContactInfo();
		if (contactInfo != null) {
			contactInfoTO.setFirstName(contactInfo.getFirstName());
			contactInfoTO.setLastName(contactInfo.getLastName());
			contactInfoTO.setEmail(contactInfo.getEmail());
			contactInfoTO.setPhoneNo(contactInfo.getPhoneNo());
			contactInfoTO.setMobileNo(contactInfo.getMobileNo());
		}
		return contactInfoTO;
	}

	private PaymentInfo getPaymentInfo(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO(ipgIdentificationParamsDTO);
		CardPaymentInfo cardPaymentInfo = new CardPaymentInfo();
		cardPaymentInfo.setTotalAmount(getPayAmount(ipgIdentificationParamsDTO));
		cardPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		cardPaymentInfo.setPnr("");
		return cardPaymentInfo;
	}

	private PayCurrencyDTO getPayCurrencyDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PayCurrencyDTO payCurrencyDTO = null;
		Currency currency = null;
		String baseCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency, ApplicationEngine.XBE);

		if (exchangeRate != null) {
			currency = ModuleServiceLocator.getCommonServiceBD().getCurrency(selectedCurrency);
			if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultXbePGId() != null) {
				payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, exchangeRate.getMultiplyingExchangeRate(),
						currency.getBoundryValue(), currency.getBreakPoint());
			}
		}

		if (payCurrencyDTO == null) {
			if (log.isDebugEnabled()) {
				log.debug("Interline paymentgateway checking for selected currency does not have pg getting the default ");
			}
			currency = ModuleServiceLocator.getCommonServiceBD().getCurrency(baseCurrencyCode);
			payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, new BigDecimal(1), currency.getBoundryValue(),
					currency.getBreakPoint());
		}
		payCurrencyDTO.setTotalPayCurrencyAmount(getPayAmount(ipgIdentificationParamsDTO));
		return payCurrencyDTO;
	}

	private BigDecimal getPayAmount(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		CurrencyExchangeRate payCurrencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(ipgIdentificationParamsDTO
				.getPaymentCurrencyCode());
		Currency payCurrency = payCurrencyExchangeRate.getCurrency();

		CurrencyExchangeRate selectedCurrencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);
		Currency selectedCurrency = selectedCurrencyExchangeRate.getCurrency();

		BigDecimal baseCurrencyAmount = AccelAeroRounderPolicy.convertAndRound(new BigDecimal(amount),
				selectedCurrencyExchangeRate.getExrateBaseToCurNumber(), selectedCurrency.getBoundryValue(),
				selectedCurrency.getBreakPoint());

		BigDecimal payCurrencyAmount = AccelAeroRounderPolicy.convertAndRound(baseCurrencyAmount,
				payCurrencyExchangeRate.getMultiplyingExchangeRate(), payCurrency.getBoundryValue(), payCurrency.getBreakPoint());

		return payCurrencyAmount;

	}

	public String getPgwId() {
		return pgwId;
	}

	public void setPgwId(String pgwId) {
		this.pgwId = pgwId;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getPgwBrokerType() {
		return pgwBrokerType;
	}

	public void setPgwBrokerType(String pgwBrokerType) {
		this.pgwBrokerType = pgwBrokerType;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getRedirectionString() {
		return redirectionString;
	}

	public void setRedirectionString(String redirectionString) {
		this.redirectionString = redirectionString;
	}

	public String getRedirectionMechanism() {
		return redirectionMechanism;
	}

	public void setRedirectionMechanism(String redirectionMechanism) {
		this.redirectionMechanism = redirectionMechanism;
	}

	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	public String getMinimumTopupAmount() {
		return minimumTopupAmount;
	}

	public void setMinimumTopupAmount(String minimumTopupAmount) {
		this.minimumTopupAmount = minimumTopupAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
