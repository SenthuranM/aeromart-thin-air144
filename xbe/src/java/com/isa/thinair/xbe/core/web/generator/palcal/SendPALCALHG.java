package com.isa.thinair.xbe.core.web.generator.palcal;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airreservation.api.model.AirportPassengerMessageTxHsitory;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class SendPALCALHG {


	private static String clientErrors;

	// searching parameters
	private static final String PARAM_SEARCH_AIRPORTCODE = "selAirport";

	private static final String PARAM_SEARCH_FLIGHTDATE = "txtDate";

	private static final String PARAM_SEARCH_FLIGHTNO = "txtFlightNo";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private String strFormFieldsVariablesJS = "";

	public final String getSendPNLADLRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);
		String strUIModeJS = "var isSearchMode = false;";
		String strAirportCode = "";
		String strFromDate = "";
		String strFlightNo = "";
		Date fromDate = null;
		Collection col = null;

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {

			strUIModeJS = "var isSearchMode = true;";

			strAirportCode = request.getParameter(PARAM_SEARCH_AIRPORTCODE);
			strFormFieldsVariablesJS = "var airportCode ='" + strAirportCode + "';";

			strFromDate = request.getParameter(PARAM_SEARCH_FLIGHTDATE);
			strFormFieldsVariablesJS += "var flightDate ='" + strFromDate + "';";

			strFlightNo = request.getParameter(PARAM_SEARCH_FLIGHTNO).trim();
			strFormFieldsVariablesJS += "var flightNo ='" + strFlightNo + "';";

			SimpleDateFormat dateFormat = null;

			if (!strFromDate.equals("")) {
				if (strFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
				}
				fromDate = dateFormat.parse(strFromDate);
			}
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
			col = ModuleServiceLocator.getReservationAuxilliaryBD().getPalCalHistory(strFlightNo, fromDate, strAirportCode);
		} else {
			strUIModeJS = "var isSearchMode = false;";
			strFormFieldsVariablesJS = "var airportCode ='';";
			strFormFieldsVariablesJS += "var flightDate ='';";
			strFormFieldsVariablesJS += "var flightNo ='';";
		}

		setFormFieldValues(strFormFieldsVariablesJS);
		return createSendPNLADLRowHTML(col);
	}

	private String covertZuluToLocal(Timestamp timestamp, String strAirport) throws ModuleException {

		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportBD());
		GregorianCalendar calendar = new GregorianCalendar();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		calendar.setTime(timestamp);
		Date date = null;
		date = helper.getLocalDateTime(strAirport, calendar.getTime());

		return formatter.format(date);
	}

	private String createSendPNLADLRowHTML(Collection listPNLADL) throws ModuleException {

		List list = (List) listPNLADL;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrPALCALData = new Array();");
		AirportPassengerMessageTxHsitory pnlADLHistory = null;

		Timestamp strLastPALTimestamp = null;
		Timestamp strLastCALTimestamp = null;
		String strPrevMsgType = null;
		String strPALStatusYes = null;
		String strCALStatusYes = null;

		for (int i = 0; i < listArr.length; i++) {
			pnlADLHistory = (AirportPassengerMessageTxHsitory) listArr[i];

			if (pnlADLHistory.getTransmissionStatus() != null) {
				if ((pnlADLHistory.getTransmissionStatus().equals("Y") || pnlADLHistory.getTransmissionStatus().equals("P"))
						&& pnlADLHistory.getMessageType().equals(PNLConstants.MessageTypes.PAL)) {
					strPALStatusYes = "Y";
				}
				if (pnlADLHistory.getTransmissionStatus().equals("Y") && pnlADLHistory.getMessageType().equals(PNLConstants.MessageTypes.CAL)) {
					strCALStatusYes = "Y";
				}
			}
			if (strPrevMsgType != null && strLastPALTimestamp != null
					&& strLastPALTimestamp.getTime() < pnlADLHistory.getTransmissionTimeStamp().getTime()
					&& pnlADLHistory.getMessageType().equals(strPrevMsgType)) {

				if (strLastPALTimestamp != null && pnlADLHistory.getMessageType().equals(PNLConstants.MessageTypes.PAL)) {

					strLastPALTimestamp = pnlADLHistory.getTransmissionTimeStamp();

				} else if (strLastCALTimestamp != null && pnlADLHistory.getMessageType().equals(PNLConstants.MessageTypes.CAL)) {

					strLastCALTimestamp = pnlADLHistory.getTransmissionTimeStamp();

				}
			} else if (strLastPALTimestamp == null && pnlADLHistory.getMessageType().equals("PNL")) {

				strLastPALTimestamp = pnlADLHistory.getTransmissionTimeStamp();

			} else if (strLastCALTimestamp == null && pnlADLHistory.getMessageType().equals("ADL")) {

				strLastCALTimestamp = pnlADLHistory.getTransmissionTimeStamp();

			}

			sb.append("arrPALCALData[" + i + "] = new Array();");
			if (pnlADLHistory.getAirportCode() != null) {
				sb.append("arrPALCALData[" + i + "][1] = '" + pnlADLHistory.getAirportCode() + "';");
			} else {
				sb.append("arrPALCALData[" + i + "][1] = '';");
			}
			if (pnlADLHistory.getMessageType() != null) {
				sb.append("arrPALCALData[" + i + "][2] = '" + pnlADLHistory.getMessageType() + "';");
				strPrevMsgType = pnlADLHistory.getMessageType();
			} else {
				sb.append("arrPALCALData[" + i + "][2] = '';");
			}

			if (pnlADLHistory.getTransmissionTimeStamp() != null) {
				sb.append("arrPALCALData[" + i + "][3] = '" + changeTimestampFormat(pnlADLHistory.getTransmissionTimeStamp())
						+ "';");
			} else {
				sb.append("arrPALCALData[" + i + "][3] = '';");
			}

			if (pnlADLHistory.getTransmissionStatus() != null) {
				sb.append("arrPALCALData[" + i + "][4] = '" + pnlADLHistory.getTransmissionStatus() + "';");
			} else {
				sb.append("arrPALCALData[" + i + "][4] = '';");
			}

			if (pnlADLHistory.getSitaAddress() != null) {
				sb.append("arrPALCALData[" + i + "][5] = '" + pnlADLHistory.getSitaAddress() + "';");
			} else if (pnlADLHistory.getEmail() != null) {
				sb.append("arrPALCALData[" + i + "][5] = '" + pnlADLHistory.getEmail() + "';");
			} else {
				sb.append("arrPALCALData[" + i + "][5] = '';");
			}

			if (strLastCALTimestamp != null) {
				sb.append("arrPALCALData[" + i + "][6] = '" + strLastCALTimestamp + "';");
			} else {
				sb.append("arrPALCALData[" + i + "][6] = '';");
			}

			if (strLastPALTimestamp != null) {
				sb.append("arrPALCALData[" + i + "][7] = '" + strLastPALTimestamp + "';");
			} else {
				sb.append("arrPALCALData[" + i + "][7] = '';");
			}

			if (pnlADLHistory.getTransmissionStatus() != null) {
				if (pnlADLHistory.getTransmissionStatus().equals("N")) {

					sb.append("arrPALCALData[" + i + "][8] = 'Failed';");
				}

				if (pnlADLHistory.getTransmissionStatus().equals("Y")) {
					sb.append("arrPALCALData[" + i + "][8] = 'Successful';");
				}

				if (pnlADLHistory.getTransmissionStatus().equals("P")) {
					sb.append("arrPALCALData[" + i + "][8] = 'Successful';");
				}

				if (pnlADLHistory.getTransmissionStatus().equals("E")) {
					sb.append("arrPALCALData[" + i + "][8] = 'Empty';");
				}
			} else {
				sb.append("arrPALCALData[" + i + "][8] = '';");
			}

			sb.append("arrPALCALData[" + i + "][9] = '" + pnlADLHistory.getFlightNumber() + "';");

			if (pnlADLHistory.getTransmissionTimeStamp() != null) {
				sb.append("arrPALCALData[" + i + "][10] = '"
						+ covertZuluToLocal(pnlADLHistory.getTransmissionTimeStamp(), pnlADLHistory.getAirportCode()) + "';");
			}
		}

		if (strLastPALTimestamp != null)
			sb.append("var strLastPALTimestamp = '" + changeTimestampFormat(strLastPALTimestamp) + "';");
		else
			sb.append("var strLastPALTimestamp = '';");
		if (strLastCALTimestamp != null)
			sb.append("var strLastCALTimestamp = '" + changeTimestampFormat(strLastCALTimestamp) + "';");
		else
			sb.append("var strLastCALTimestamp = '';");
		if (strPALStatusYes != null)
			sb.append("var strPALStatusYes = '" + strPALStatusYes + "';");
		else
			sb.append("var strPALStatusYes = '';");
		if (strCALStatusYes != null)
			sb.append("var strCALStatusYes = '" + strCALStatusYes + "';");
		else
			sb.append("var strCALStatusYes = '';");

		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("cc.resend.pal.sendpalcal.confirmation", "reSendPALRecoredCfrm");
			moduleErrs.setProperty("cc.send.new.pal.sendpalcal.confirmation", "sendnewPALRecoredCfrm");
			moduleErrs.setProperty("cc.resend.cal.sendpalcal.confirmation", "reSendCALRecoredCfrm");
			moduleErrs.setProperty("cc.send.new.cal.sendpalcal.confirmation", "sendnewCALRecoredCfrm");
			moduleErrs.setProperty("cc.reprint.pal.sendpalcal.confirmation", "rePrintPALRecoredCfrm");
			moduleErrs.setProperty("cc.reprint.cal.sendpalcal.confirmation", "rePrintCALRecoredCfrm");

			moduleErrs.setProperty("cc.see.log", "seeLogEntry");

			moduleErrs.setProperty("cc.sendpnladl.form.flight.date.required", "flightDateRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.flight.date.invalid", "flightDateInvalid");
			moduleErrs.setProperty("cc.sendpnladl.form.flight.number.required", "flightNumberRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.airport.required", "airportRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.messagetype.required", "msgTypeRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.mailserver.required", "mailServerRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.sita.address.required", "sitaAddressRqrd");
			moduleErrs.setProperty("cc.sendpnladl.form.user.priv", "insufficientPalCalPriv");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

	private String changeTimestampFormat(Timestamp time) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dtfmt.format(time);
	}


}
