package com.isa.thinair.xbe.core.test.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.invoicing.api.dto.GenerateInvoiceOption;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

/**
 * Tests the InvoiceDetailsposterJob.
 * 
 * @author sanjaya
 * 
 */
public class TestInvoiceDetailsposterJob {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(TestInvoiceDetailsposterJob.class);

	public void execute(Date invoiceDate) throws ModuleException {

		SimpleDateFormat dateFormatDay = new SimpleDateFormat("dd");
		String dayNum = dateFormatDay.format(invoiceDate);

		if (Integer.valueOf(dayNum).intValue() == 16 || Integer.valueOf(dayNum).intValue() == 01) {

			int period = 0;

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(invoiceDate);
			int year = gc.get(GregorianCalendar.YEAR);
			int month = gc.get(GregorianCalendar.MONTH);
			if (Integer.valueOf(dayNum).intValue() == 16) {
				period = 1;
				month = month + 1;
			} else {
				period = 2;
			}
			SearchInvoicesDTO searchInvoicesDTO = new SearchInvoicesDTO();
			searchInvoicesDTO.setFilterZeroInvoices(true);
			searchInvoicesDTO.setMonth(month);
			searchInvoicesDTO.setYear(year);
			searchInvoicesDTO.setInvoicePeriod(period);
			searchInvoicesDTO.setAgentCodes(null);

			if (log.isDebugEnabled()) {
				log.debug("######################## WILL START INVOICE GENERATION");
			}

			ScheduledservicesUtils.getInvoicingBD().generateInvoices(searchInvoicesDTO,
					GenerateInvoiceOption.IF_EXISTS_DO_NOT_PROCEED);
		}
	}

}
