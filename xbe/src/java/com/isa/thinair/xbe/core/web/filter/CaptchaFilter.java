package com.isa.thinair.xbe.core.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class CaptchaFilter extends BasicFilter {

	private static Log log = LogFactory.getLog(CaptchaFilter.class);

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		String userIP = getIpAddress(request);

		try {
			if (AppSysParamsUtil.isCaptchaEnabledForXbeLogin()
					&& ModuleServiceLocator.getSecurityBD().isCaptchaEnableForUser(userIP)) {

				try {
					boolean isCaptchaValidated = true;
					isCaptchaValidated = (boolean) request.getSession().getAttribute(WebConstants.XBE_LOGIN_CAPTCHA);

					if (!isCaptchaValidated) {
						BasicRH.forward(req, res, errorPage);
						return;
					}
				} catch (NullPointerException e) {
					log.error(e);
				}

			}
		} catch (Exception e) {
			String msg = BasicRH.getErrorMessage(e, userLanguage);
			JavaScriptGenerator.setServerError(request, XBEConfig.getServerMessage("default.server.error", userLanguage, null),
					"", "");
			log.error(msg, e);
			BasicRH.forward(req, res, errorPage);
			return;
		}

		filterChain.doFilter(req, res);

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

	@Override
	public void destroy() {

	}

	private String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		
		if (ip != null && !ip.trim().isEmpty()) {
			String[] clientIpArray = ip.split(Constants.COMMA_SEPARATOR);
			if (clientIpArray.length > 0) {
				for (String ipAddress : clientIpArray) {
					if (ipAddress != null && !ipAddress.trim().isEmpty()) {
						ip = ipAddress.trim();
						break;
					}
				}
			}
		}
		
		return ip;
	}
}
