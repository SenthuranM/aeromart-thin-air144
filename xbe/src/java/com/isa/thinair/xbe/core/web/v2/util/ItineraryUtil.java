package com.isa.thinair.xbe.core.web.v2.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightStopOverDisplayUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.util.I18NMessagingUtil;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.dto.FlexiDTO;
import com.isa.thinair.webplatform.api.v2.ancillary.ItineraryAnciAvailability;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.FlightListTO;
import com.isa.thinair.xbe.api.dto.v2.ItineraryPaxTO;
import com.isa.thinair.xbe.api.dto.v2.ItineraryPaymentListTO;
import com.isa.thinair.xbe.api.dto.v2.ItineraryPaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.LanguagesTO;
import com.isa.thinair.xbe.api.dto.v2.PaxTO;
import com.isa.thinair.xbe.core.config.XBEModuleConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class ItineraryUtil {

	/**
	 * TODO
	 * 
	 * @return
	 */

	private static Log log = LogFactory.getLog(ItineraryUtil.class);

	private static XBEModuleConfig config = XBEModuleUtils.getConfig();

	public static BookingTO createBookingDetails(LCCClientReservation lccClientReservation, Collection colUserDST,
			String strStation) {
		BookingTO bookingTO = new BookingTO();

		bookingTO.setBookingDate(BeanUtils.parseDateFormat(lccClientReservation.getZuluBookingTimestamp(), "dd-MM-yyyy HH:mm"));
		bookingTO.setPNR(lccClientReservation.getPNR());
		bookingTO.setGroupPNR(lccClientReservation.isGroupPNR());
		bookingTO.setBookingCategory(lccClientReservation.getBookingCategory());
		bookingTO.setOriginCountryOfCall(lccClientReservation.getOriginCountryOfCall());
		if (hasStandbySegments(lccClientReservation)) {
			bookingTO.setStatus("STANDBY");
		} else if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())) {
			if (lccClientReservation.getTotalAvailableBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 0) {
				bookingTO.setStatus("CONFIRMED");
			} else {
				bookingTO.setStatus("CONFIRMED-FORCED");
			}
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(lccClientReservation.getStatus())) {
			bookingTO.setStatus("CANCELLED");
		} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(lccClientReservation.getStatus())) {
			bookingTO.setStatus("ONHOLD");
			try {
				String strReleaseTime = DateUtil.getAgentLocalTime(lccClientReservation.getZuluReleaseTimeStamp(), colUserDST,
						strStation, "dd-MM-yyyy HH:mm (EEEE)");
				bookingTO.setReleaseTime(strReleaseTime);
			} catch (Exception e) {
				// TODO: handle exception
				if (log.isErrorEnabled()) {
					log.error("error in parsing localtime for onhold " + e);
				}

			}

		}

		return bookingTO;
	}

	private static boolean hasStandbySegments(LCCClientReservation reservation) {
		for (LCCClientReservationSegment seg : reservation.getSegments()) {
			if (seg.getBookingType().equals("STANDBY")) {
				return true;
			}
		}
		return false;
	}

	public static List<FlexiDTO> createFlexiRules(LCCClientReservation lccClientReservation, String language) {
		List<FlexiDTO> flexiInfo = new ArrayList<FlexiDTO>();
		for (LCCClientReservationSegment segment : SortUtil.sort(lccClientReservation.getSegments())) {
			for (LCCClientAlertInfoTO flexiAlert : lccClientReservation.getFlexiAlertInfoTOs()) {
				if (flexiAlert.getFlightSegmantRefNumber().equals(segment.getPnrSegID().toString())) {
					String flexiMessage = I18NMessagingUtil.getMessage("itinerary_lblFlexiMessage", new Locale(language));
					String flexiCnxMessage = I18NMessagingUtil.getMessage("itinerary_lblFlexiCnxMessage", new Locale(language));
					String flexiModCnxMessage = I18NMessagingUtil.getMessage("itinerary_lblFlexiModCnxMessage", new Locale(
							language));
					List<LCCClientAlertTO> alertsList = flexiAlert.getAlertTO();
					/*
					 * Alert id 1 means modification and number of modifications contain in the alert content Alert id 2
					 * means cancellation and we dont need the number of cancellations because it is only one always
					 * Alert id 0 means buffer time in hours. We can push this to cancellation content but if will fail
					 * when a system doesnt have cancellation flexibily. Eg 3O
					 */
					for (LCCClientAlertTO alertTO : alertsList) {
						if (alertTO.getAlertId() == 1) {
							flexiMessage = flexiMessage.replace("#1", alertTO.getContent());
						} else if (alertTO.getAlertId() == 2) {
							flexiMessage = flexiMessage.replace("#3", flexiModCnxMessage);
						} else if (alertTO.getAlertId() == 0) {
							flexiMessage = flexiMessage.replace("#2", alertTO.getContent());
							flexiCnxMessage = flexiCnxMessage.replace("#2", alertTO.getContent());
						}
					}
					// All modifications consumed
					if (flexiMessage.contains("#1")) {
						flexiMessage = flexiCnxMessage;
					}
					// Flexi rule has no cancellations
					if (flexiMessage.contains("#3")) {
						flexiMessage = flexiMessage.replace("#3", "");
					}
					FlexiDTO flexiDTO = new FlexiDTO();
					flexiDTO.setOriginDestination(segment.getSegmentCode());
					flexiDTO.setFlexiDetails(flexiMessage);
					flexiInfo.add(flexiDTO);
				}
			}
		}
		return flexiInfo;
	}

	/**
	 * 
	 * @return
	 */
	public static Object[] createSelecteFlightInfo(LCCClientReservation lccClientReservation) {

		FlightListTO flightListTO = new FlightListTO();
		flightListTO.setFlights(new ArrayList<FlightInfoTO>());
		flightListTO.setType("Departing Flight(s)");

		FlightListTO retflightListTO = new FlightListTO();
		retflightListTO.setFlights(new ArrayList<FlightInfoTO>());
		retflightListTO.setType("Returning Flight(s)");

		LCCClientReservationSegment[] flightsegs = SortUtil.sort(lccClientReservation.getSegments());

		boolean blnReturn;

		// Departing Flights
		for (int i = 0; i < flightsegs.length; i++) {
			FlightInfoTO flightInfoTO = new FlightInfoTO();

			LCCClientReservationSegment lccClientReservationSegment = (LCCClientReservationSegment) flightsegs[i];

			blnReturn = lccClientReservationSegment.getReturnFlag().equalsIgnoreCase(
					ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE);

			flightInfoTO.setDepartureText("DEP");
			flightInfoTO.setArrivalText("ARR");
			flightInfoTO.setStatus(lccClientReservationSegment.getBookingType());
			flightInfoTO.setDepartureTerminal(lccClientReservationSegment.getDepartureTerminalName());
			flightInfoTO.setArrivalTerminal(lccClientReservationSegment.getArrivalTerminalName());
			flightInfoTO.setCsOcCarrierCode(lccClientReservationSegment.getCsOcCarrierCode());

			if (lccClientReservationSegment.getBookingType().equals(BookingClass.BookingClassType.OPEN_RETURN)) {
				// This is kind of a hack to display open return details as earlier, TODO do we need to change this?
				blnReturn = true;
				flightInfoTO.setFlightNo("");
				flightInfoTO.setDepartureDate("");
				flightInfoTO.setDepartureDateLong(0);

				flightInfoTO.setDeparture("");
				flightInfoTO.setDepartureTime("");
				flightInfoTO.setArrivalDate("");

				flightInfoTO.setArrival("");
				flightInfoTO.setArrivalTime("");
			} else {
				flightInfoTO.setFlightNo(lccClientReservationSegment.getFlightNo());
				flightInfoTO.setDepartureDate(BeanUtils.parseDateFormat(lccClientReservationSegment.getDepartureDate(),
						"EEE dd-MM-yyyy"));
				flightInfoTO.setDepartureDateLong(lccClientReservationSegment.getDepartureDate().getTime());

				flightInfoTO.setDeparture(lccClientReservationSegment.getSegmentCode().split("/")[0]);
				flightInfoTO.setDepartureTime(BeanUtils.parseDateFormat(lccClientReservationSegment.getDepartureDate(), "HH:mm"));
				flightInfoTO.setArrivalDate(BeanUtils.parseDateFormat(lccClientReservationSegment.getArrivalDate(),
						"EEE dd-MM-yyyy"));

				flightInfoTO.setArrival(lccClientReservationSegment.getSegmentCode().split("/")[1]);
				flightInfoTO.setArrivalTime(BeanUtils.parseDateFormat(lccClientReservationSegment.getArrivalDate(), "HH:mm"));

				String fltDuration = lccClientReservationSegment.getFlightDuration();
				String fltModelDescription = lccClientReservationSegment.getFlightModelDescription();
				String fltModelNo = lccClientReservationSegment.getFlightModelNumber();
				String fltRemarks = lccClientReservationSegment.getRemarks();
				String fltDepartureTerminalName = lccClientReservationSegment.getDepartureTerminalName();
				String fltArrivalTerminalName = lccClientReservationSegment.getArrivalTerminalName();

				fltDuration = FlightStopOverDisplayUtil.formatNullOrEmpty(fltDuration);
				fltModelDescription = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelDescription);
				fltModelNo = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelNo);
				fltRemarks = FlightStopOverDisplayUtil.formatNullOrEmpty(fltRemarks);
				fltDepartureTerminalName = FlightStopOverDisplayUtil.formatNullOrEmpty(fltDepartureTerminalName);
				fltArrivalTerminalName = FlightStopOverDisplayUtil.formatNullOrEmpty(fltArrivalTerminalName);

				flightInfoTO.setDepartureTerminal(fltDepartureTerminalName);
				flightInfoTO.setArrivalTerminal(fltArrivalTerminalName);
				flightInfoTO.setFlightDuration(fltDuration);
				flightInfoTO.setFlightModelDescription(fltModelDescription);
				flightInfoTO.setFlightModelNumber(fltModelNo);
				flightInfoTO.setRemarks(fltRemarks);

				String stopOverDuration = FlightStopOverDisplayUtil.getStopOverDuration(lccClientReservation.getSegments(),
						lccClientReservationSegment);
				
				stopOverDuration = FlightStopOverDisplayUtil.formatNullOrEmpty(stopOverDuration);

				flightInfoTO.setFlightStopOverDuration(stopOverDuration);
				flightInfoTO.setNoOfStops(FlightStopOverDisplayUtil.getStopOverStations(lccClientReservationSegment
						.getSegmentCode()));

			}

			flightInfoTO.setOrignNDest(lccClientReservationSegment.getSegmentCode());
			flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(lccClientReservationSegment.getFlightNo()));

			if (blnReturn) {
				retflightListTO.getFlights().add(flightInfoTO);
			} else {
				flightListTO.getFlights().add(flightInfoTO);
			}

		}

		Collection<FlightListTO> colFlightListTO = new ArrayList<FlightListTO>();
		colFlightListTO.add(flightListTO);

		Collection<FlightListTO> colRetFlightListTO = new ArrayList<FlightListTO>();
		if (retflightListTO.getFlights().size() > 0)
			colRetFlightListTO.add(retflightListTO);

		// TODO Integrating the return flights
		Object[] colObj = new Object[2];
		colObj[0] = colFlightListTO;
		colObj[1] = colRetFlightListTO;
		return colObj;
	}

	private static String composeName(String title, String firstName, String lastName) {
		title = BeanUtils.nullHandler(title);
		firstName = StringUtil.toInitCap(BeanUtils.nullHandler(firstName));
		lastName = StringUtil.toInitCap(BeanUtils.nullHandler(lastName));

		StringBuilder name = new StringBuilder();

		if (title.length() > 0) {
			name.append(title).append(" ");
		}

		if (firstName.length() > 0) {
			name.append(firstName).append(" ");
		}

		if (lastName.length() > 0) {
			name.append(lastName).append(" ");
		}

		return name.toString().trim();
	}

	/**
	 * TODO
	 * 
	 * @return
	 */
	public static Collection<ItineraryPaxTO> createPassengerList(LCCClientReservation lccClientReservation) {

		Collection<ItineraryPaxTO> collection = new ArrayList<ItineraryPaxTO>();

		Collection<PaxTO> colAdultCollection = new ArrayList<PaxTO>();
		Collection<PaxTO> colChildCollection = new ArrayList<PaxTO>();
		Collection<PaxTO> colInfantCollection = new ArrayList<PaxTO>();

		for (LCCClientReservationPax lccClientReservationPax : SortUtil.sortPax(lccClientReservation.getPassengers())) {
			PaxTO paxTO = new PaxTO();
			paxTO.setItnSeqNo(lccClientReservationPax.getPaxSequence() + "");
			paxTO.setItnPaxName(composeName(lccClientReservationPax.getTitle(), lccClientReservationPax.getFirstName(),
					lccClientReservationPax.getLastName()));

			paxTO.setSelectedAncillaries(lccClientReservationPax.getSelectedAncillaries());
			paxTO.setItnMeal("");
			paxTO.setItnSeat("");

			if (ReservationInternalConstants.PassengerType.ADULT.equals(lccClientReservationPax.getPaxType())) {
				colAdultCollection.add(paxTO);
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(lccClientReservationPax.getPaxType())) {
				colChildCollection.add(paxTO);
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(lccClientReservationPax.getPaxType())) {
				colInfantCollection.add(paxTO);
			}
		}

		// Add Adults
		if (colAdultCollection.size() > 0) {
			ItineraryPaxTO itineraryAdultPaxTO = new ItineraryPaxTO();
			itineraryAdultPaxTO.setType("Adult(s)");
			itineraryAdultPaxTO.setPaxCollection(colAdultCollection);
			collection.add(itineraryAdultPaxTO);
		}

		// Add Children
		if (colChildCollection.size() > 0) {
			ItineraryPaxTO itineraryChildPaxTO = new ItineraryPaxTO();
			itineraryChildPaxTO.setType("Children");
			itineraryChildPaxTO.setPaxCollection(colChildCollection);
			collection.add(itineraryChildPaxTO);
		}

		// Add Infants
		if (colInfantCollection.size() > 0) {
			ItineraryPaxTO itineraryInfantPaxTO = new ItineraryPaxTO();
			itineraryInfantPaxTO.setType("Infant(s)");
			itineraryInfantPaxTO.setPaxCollection(colInfantCollection);
			collection.add(itineraryInfantPaxTO);
		}

		return collection;
	}

	public static ItineraryAnciAvailability getAncillaryAvailability(Collection<ItineraryPaxTO> paxTOCollection) {
		ItineraryAnciAvailability availability = new ItineraryAnciAvailability();
		outermost: for (ItineraryPaxTO paxTO : paxTOCollection) {
			for (PaxTO paxTypeTO : paxTO.getPaxCollection()) {
				for (LCCSelectedSegmentAncillaryDTO anci : paxTypeTO.getSelectedAncillaries()) {
					LCCAirSeatDTO seat = anci.getAirSeatDTO();
					if (seat != null && seat.getSeatNumber() != null && !seat.getSeatNumber().equals("")) {
						availability.setSeatAvailable(true);
					}
					List<LCCMealDTO> meals = anci.getMealDTOs();
					if (meals != null && meals.size() > 0) {
						availability.setMealAvailable(true);
					}
					List<LCCBaggageDTO> baggages = anci.getBaggageDTOs();
					if (baggages != null && baggages.size() > 0) {
						availability.setBaggageAvailable(true);
					}
					List<LCCSpecialServiceRequestDTO> ssrs = anci.getSpecialServiceRequestDTOs();
					if (ssrs != null && ssrs.size() > 0) {
						availability.setSsrAvailable(true);
					}
					List<LCCAirportServiceDTO> apss = anci.getAirportServiceDTOs();
					if (apss != null && apss.size() > 0) {
						availability.setAirportServiceAvailable(true);
					}
					
					List<LCCAirportServiceDTO> apts = anci.getAirportTransferDTOs();
					if (apts != null && apts.size() > 0) {
						availability.setAirportTransferAvailable(true);
					}
					
					List<LCCAutomaticCheckinDTO> autoCheckin = anci.getAutomaticCheckinDTOs();
					if (autoCheckin != null && autoCheckin.size() > 0) {
						availability.setAutoCheckinAvailable(true);
					}
					
					
					if (availability.isAllAvailable()) {
						break outermost;
					}
				}
			}
		}
		return availability;
	}

	/**
	 * TODO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static ContactInfoTO createContactInfo(LCCClientReservation lccClientReservation) throws ModuleException {
		ContactInfoTO contactInfoTO = new ContactInfoTO();
		contactInfoTO.setTitle(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getTitle()));
		contactInfoTO.setFirstName(StringUtil.toInitCap(BeanUtils.nullHandler(lccClientReservation.getContactInfo()
				.getFirstName())));
		contactInfoTO
				.setLastName(StringUtil.toInitCap(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getLastName())));
		contactInfoTO.setStreet(StringUtil.toInitCap(BeanUtils.nullHandler(lccClientReservation.getContactInfo()
				.getStreetAddress1())));
		contactInfoTO.setAddress(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getStreetAddress2()));
		contactInfoTO.setCity(StringUtil.toInitCap(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getCity())));
		contactInfoTO.setZipCode(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getZipCode()));
		// TODO : Find faster way than hitting on the DB

		String countryCode = BeanUtils.nullHandler(lccClientReservation.getContactInfo().getCountryCode());
		if (countryCode != null && (!"".equals(countryCode))) {
			String countryName = ModuleServiceLocator.getLocationServiceBD().getCountry(countryCode).getCountryName();
			contactInfoTO.setCountry(countryName);
		}

		contactInfoTO.setMobileNo(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getMobileNo()));
		contactInfoTO.setPhoneNo(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getPhoneNo()));
		contactInfoTO.setFaxNo(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getFax()));
		contactInfoTO.setEmail(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getEmail()));
		contactInfoTO.setPreferredLang(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getPreferredLanguage()));
		contactInfoTO.setTaxRegNo(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getTaxRegNo()));
		contactInfoTO.setState(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getState()));
		
		String stateCode = lccClientReservation.getContactInfo().getState();
		
		if(stateCode != null && !stateCode.isEmpty()){
			State state = ModuleServiceLocator.getCommonServiceBD().getState(stateCode);
			contactInfoTO.setState(state != null?state.getStateName():"");
		}
		
		// set emergency contact information
		contactInfoTO.setEmgnTitle(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getEmgnTitle()));
		contactInfoTO.setEmgnFirstName(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getEmgnFirstName()));
		contactInfoTO.setEmgnLastName(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getEmgnLastName()));
		contactInfoTO.setEmgnEmail(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getEmgnEmail()));
		contactInfoTO.setEmgnPhoneNo(BeanUtils.nullHandler(lccClientReservation.getContactInfo().getEmgnPhoneNo()));

		return contactInfoTO;
	}

	/**
	 * TODO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static ItineraryPaymentSummaryTO createPaymentSummary(LCCClientReservation lccClientReservation, String selCurrency)
			throws ModuleException {
		ItineraryPaymentSummaryTO itineraryPaymentSummaryTO = new ItineraryPaymentSummaryTO();

		Collection<ItineraryPaymentListTO> payDetails = new ArrayList<ItineraryPaymentListTO>();
		ItineraryPaymentListTO itineraryPaymentListTO = new ItineraryPaymentListTO();
		itineraryPaymentListTO.setDescription("Total Airfare");
		itineraryPaymentListTO.setTotal(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketFare()));
		payDetails.add(itineraryPaymentListTO);

		itineraryPaymentListTO = new ItineraryPaymentListTO();
		itineraryPaymentListTO.setDescription("Total Tax");
		itineraryPaymentListTO.setTotal(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketTaxCharge()));
		payDetails.add(itineraryPaymentListTO);

		itineraryPaymentListTO = new ItineraryPaymentListTO();
		itineraryPaymentListTO.setDescription("Total Surcharge");
		itineraryPaymentListTO.setTotal(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketSurCharge()));
		payDetails.add(itineraryPaymentListTO);

		itineraryPaymentListTO = new ItineraryPaymentListTO();
		itineraryPaymentListTO.setDescription("Total Fee");
		itineraryPaymentListTO.setTotal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
				lccClientReservation.getTotalTicketModificationCharge(), lccClientReservation.getTotalTicketCancelCharge(),
				lccClientReservation.getTotalTicketAdjustmentCharge())));
		payDetails.add(itineraryPaymentListTO);

		itineraryPaymentSummaryTO.setCurrency(AppSysParamsUtil.getBaseCurrency());
		itineraryPaymentSummaryTO.setPayDetails(payDetails);
		itineraryPaymentSummaryTO.setTotalAmount(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketPrice()));

		CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(lccClientReservation.getZuluBookingTimestamp())
				.getCurrencyExchangeRate(selCurrency, ApplicationEngine.XBE);
		Currency currency = currencyExchangeRate.getCurrency();

		itineraryPaymentSummaryTO.setTotalCurrencyAmount(AccelAeroCalculator.formatAsDecimal(AccelAeroRounderPolicy
				.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), lccClientReservation.getTotalTicketPrice(),
						currency.getBoundryValue(), currency.getBreakPoint())));
		itineraryPaymentSummaryTO.setSelCurrency(selCurrency);

		// set total fare discounts for the reservation
		itineraryPaymentSummaryTO.setTotalDiscount(setDiscountAmount(lccClientReservation));

		return itineraryPaymentSummaryTO;
	}

	public static Collection<LanguagesTO> getSupportedLanguages() {
		Collection<LanguagesTO> languagesTOs = new ArrayList<LanguagesTO>();
		Map<String, String> supportedLang = config.getSupportedLanguages();
		for (Iterator itr = supportedLang.keySet().iterator(); itr.hasNext();) {
			String id = (String) itr.next();
			String desc = supportedLang.get(id);
			LanguagesTO languagesTO = new LanguagesTO();
			languagesTO.setId(id);
			languagesTO.setDesc(desc);

			languagesTOs.add(languagesTO);

		}
		return languagesTOs;
	}
	
	private static String setDiscountAmount(LCCClientReservation reservation) {
		String totalDiscount;
		if (reservation.getLccPromotionInfoTO() != null
				&& reservation.getLccPromotionInfoTO().getDiscountAs() != null
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(reservation.getLccPromotionInfoTO()
						.getDiscountAs()) && reservation.getLccPromotionInfoTO().getCreditDiscountAmount() != null) {
			totalDiscount = AccelAeroCalculator
					.formatAsDecimal(reservation.getLccPromotionInfoTO().getCreditDiscountAmount());
		} else {
			totalDiscount = AccelAeroCalculator.formatAsDecimal(reservation.getTotalDiscount());
		}
		return totalDiscount;
	}
}
