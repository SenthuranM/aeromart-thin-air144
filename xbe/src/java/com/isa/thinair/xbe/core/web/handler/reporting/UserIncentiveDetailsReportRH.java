package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Request handler for User Incentive Details Report
 * 
 * @author Harshana
 * @since 22-02-2012
 * 
 */
public class UserIncentiveDetailsReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(UserIncentiveDetailsReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static String EMPTY_STRING = "";

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportViewAll(request, response);
				return null;
			}
		} catch (ModuleException e) {
			JavaScriptGenerator.setServerError(request, e.getMessageString(), "", "");
			forward = S2Constants.Result.ERROR;
			log.error("UserIncentiveDetailsReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			forward = S2Constants.Result.ERROR;
			log.error("Error in UserIncentiveDetailsReportRH execute()" + e.getMessage());
		}
		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setStationComboList(request);
		setReportingPeriod(request);
		setServiceCategory(request);
		setBookingClassCodesHtml(request);
		setFlightTypesHtml(request);
		setBcHtml(request);
		setPaxStatusHtml(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setServiceCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createSsrCategoryList();
		request.setAttribute(WebConstants.REQ_SERVICE_CATEGORY_LIST, strHtml);
	}

	/**
	 * Sets the values list to populate from and to airports lists
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createAirportsList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the initial reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	private static void setBookingClassCodesHtml(HttpServletRequest request) throws ModuleException {
		String html = SelectListGenerator.createBookingCodeList();
		request.setAttribute(WebConstants.BOOKING_CLASS_CODES, html);
	}

	private static void setFlightTypesHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightTypeList(false);
		setAttribInRequest(request, WebConstants.REQ_HTML_FLIGHT_TYPES, strHtml);
	}

	public static void setBcHtml(HttpServletRequest request) throws ModuleException {
		String strBcList = ReportsHTMLGenerator.createBookingCodeHtml();
		request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_LIST, strBcList);
	}

	private static void setPaxStatusHtml(HttpServletRequest request) throws ModuleException {
		String strBcList = ReportsHTMLGenerator.createPaxStatusHtml();
		request.setAttribute(WebConstants.REQ_HTML_PAX_STATUS_LIST, strBcList);
	}

	protected static void setReportViewAll(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		try {

			String fromDate = request.getParameter("txtFromDate");
			String toDate = request.getParameter("txtToDate");

			String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

			String segments = request.getParameter("hdnSegments");

			String userID = request.getParameter("txtUserID");

			String ffpNumber = request.getParameter("txtFfpNumber");

			String numberOfSegments = request.getParameter("txtSegmentCount");

			String segmentQuantifier = request.getParameter("selSegQuantifier");

			String bookingClass = request.getParameter("selBookingClass");

			String strReportFormat = request.getParameter("radRptNumFormat");

			String bookingClasses = request.getParameter("hdnBookingClasses");

			String flightType = request.getParameter("selFlightType");

			String depFromDate = request.getParameter("txtDepFromDate");

			String depToDate = request.getParameter("txtDepToDate");

			String paxStatus = request.getParameter("hdnPaxStatus");

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !EMPTY_STRING.equals(fromDate)) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}
			if (toDate != null && !EMPTY_STRING.equals(toDate)) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}
			if (depFromDate != null && !EMPTY_STRING.equals(depFromDate)) {
				search.setDepartureDateRangeFrom(ReportsHTMLGenerator.convertDate(depFromDate) + " 00:00:00");
			}
			if (depToDate != null && !EMPTY_STRING.equals(depToDate)) {
				search.setDepartureDateRangeTo(ReportsHTMLGenerator.convertDate(depToDate) + " 23:59:59");
			}

			if (userID != null && !EMPTY_STRING.equals(userID)) {
				search.setUserId(userID);
			}

			if (ffpNumber != null && !EMPTY_STRING.equals(ffpNumber)) {
				search.setFfpNumber(ffpNumber);
			}

			if (numberOfSegments != null && !EMPTY_STRING.equals(numberOfSegments)) {
				search.setCount(Integer.parseInt(numberOfSegments));
			}

			if (segmentQuantifier != null && !EMPTY_STRING.equals(segmentQuantifier)) {
				search.setQuantifier(segmentQuantifier);
			}

			if (bookingClass != null && !EMPTY_STRING.equals(bookingClass)) {
				search.setBookinClass(bookingClass);
			}

			if (flightType != null && !EMPTY_STRING.equals(flightType)) {
				search.setFlightType(flightType);
			}

			if (strReportFormat != null && !EMPTY_STRING.equals(strReportFormat)) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat(EMPTY_STRING);
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			// AAN/ALA,AIR/ADE/ATZ/AMS
			if (segments != null && !EMPTY_STRING.equals(segments)) {
				String segmentsArr[] = segments.split(",");
				ArrayList<String> segmentsCol = new ArrayList<String>();
				for (String seg : segmentsArr) {
					segmentsCol.add(seg);
				}
				search.setSegmentCodes(segmentsCol);
			}

			if (bookingClasses != null && !EMPTY_STRING.equals(bookingClasses)) {
				String bookingCodesArr[] = bookingClasses.split(",");
				List<String> bookingClsCol = Arrays.asList(bookingCodesArr);
				search.setBookingCodes(bookingClsCol);
			}

			if (paxStatus != null && !EMPTY_STRING.equals(paxStatus)) {
				String paxStatusArr[] = paxStatus.split(",");
				List<String> paxStatusCol = Arrays.asList(paxStatusArr);
				search.getPaxStatus().addAll(paxStatusCol);
			}

			ResultSet rs = null;
			rs = ModuleServiceLocator.getDataExtractionBD().getUserIncentiveDetailsReport(search);

			viewJasperReport(request, response, rs, search, fromDate, toDate, segments, depFromDate, depToDate);

		} catch (Exception e) {
			log.error("download method is failed :", e);
			throw new ModuleException("Error in report data retrieval");
		}
	}

	private static void viewJasperReport(HttpServletRequest request, HttpServletResponse response, ResultSet resultSet,
			ReportsSearchCriteria search, String fromDate, String toDate, String ondCode, String depFromDate, String depToDate) throws ModuleException {
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		String strReportId = "UC_REPM_082";
		String reportTemplate = null;
		String reportName = null;

		reportTemplate = "UserIncentiveDetails.jasper";

		reportName = "UserIncentiveDetails";

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_ID", strReportId);
		parameters.put("FROM_DATE", fromDate);
		parameters.put("TO_DATE", toDate);
		parameters.put("DEP_FROM_DATE", depFromDate);
		parameters.put("DEP_TO_DATE", depToDate);
		parameters.put("OND_CODE", ondCode);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals(WebConstants.REPORT_HTML)) {
			parameters.put("IMG", strPath);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet, null,
					null, response);
		} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
			strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", strPath);
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.pdf", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.csv", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.xls", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		}
	}

}
