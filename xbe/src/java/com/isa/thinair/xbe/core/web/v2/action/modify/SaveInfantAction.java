package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.InfantInfoTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.AuthorizationsUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.util.ReservationPaxUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ConfirmUpdateUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency, countryCurrExchangeRate\\.currency" })
public class SaveInfantAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(SaveInfantAction.class);

	private boolean success = true;

	private boolean paysuccess = false;

	private String messageTxt;

	private BookingTO bookingInfo;

	private String groupPNR;

	private boolean infantFare = true;

	private InfantInfoTO infantTo;

	private String saveMode;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private PaymentTO payment;

	private CreditCardTO creditInfo;

	private String hdnSelCurrency;

	private CurrencyExchangeRate currencyExchangeRate;

	private CurrencyExchangeRate countryCurrExchangeRate;

	public String execute() {

		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams resParam = new ReservationProcessParams(request,
				resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(),
				resInfo.isGdsReservationModAllowed(), null, null, false, false);
		if (isGroupPNR) {
			resParam.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
					resInfo != null ? resInfo.getReservationStatus() : null, null,
					resInfo != null ? resInfo.isModifiableReservation() : true, null);
		}
		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		// set tracking info
		TrackInfoDTO trackInfoDTO = this.getTrackInfo();

		List<String> adultSeqId = new ArrayList<String>();
		Map<Integer, ReservationPaxTO> paxMap = bookingShoppingCart.getPassengerMap();
		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT;
		currencyExchangeRate = bookingShoppingCart.getCurrencyExchangeRate();

		if (hdnSelCurrency == null || "".equals(hdnSelCurrency)) {
			hdnSelCurrency = currencyExchangeRate.getCurrency().getCurrencyCode();
		}

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);
			if (log.isDebugEnabled()) {
				log.debug("Retrieved reservation for add infant operation");
				log.debug("Existing infant count ");
			}

			if (!isGroupPNR) {
				groupPNR = bookingInfo.getPNR();
			}

			Collection<LCCClientReservationPax> colpaxs = ReservationUtil
					.transformJsonPassengers(request.getParameter("respaxs"));

			// transform the pnr segments
			Collection<LCCClientReservationSegment> lccSegments = ModifyReservationJSONUtil
					.transformJsonSegments(request.getParameter("pnrSegments"));

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String infFName = request.getParameter("infFName");
			String infLName = request.getParameter("infLName");
			String infNationality = request.getParameter("infNationality");
			String infDOB = request.getParameter("infDOB");
			String infTravellingAdult = request.getParameter("infTravellingAdult");
			String infTitle = request.getParameter("infTitle");
			String infNIC = request.getParameter("infNationalID");
			String infPnrPaxCatFOIDExpiry = request.getParameter("infPnrPaxCatFOIDExpiry");
			String infPnrPaxCatFOIDNumber = request.getParameter("infPnrPaxCatFOIDNumber");
			String infPnrPaxCatFOIDPlace = request.getParameter("infPnrPaxCatFOIDPlace");
			String infPnrPaxPlaceOfBirth = request.getParameter("infPnrPaxPlaceOfBirth");
			String infVisaApplicableCountry = request.getParameter("infVisaApplicableCountry");
			String infVisaDocIssueDate = request.getParameter("infVisaDocIssueDate");
			String infVisaDocNumber = request.getParameter("infVisaDocNumber");
			String infVisaDocPlaceOfIssue = request.getParameter("infVisaDocPlaceOfIssue");

			if (log.isDebugEnabled()) {
				log.debug("New infant name : " + infFName + " " + infLName + "/ parent " + infTravellingAdult);
			}

			LCCClientReservation lccClientReservation = new LCCClientReservation();
			LCCClientReservationPax lccClientReservationPax = new LCCClientReservationPax();
			lccClientReservation.setPNR(groupPNR);
			lccClientReservation.setVersion(bookingInfo.getVersion());
			lccClientReservation.setStatus(bookingInfo.getStatus());
			CommonReservationContactInfo contactInfo = ReservationUtil
					.transformJsonContactInfo(request.getParameter("contactInfo"));
			lccClientReservation.setContactInfo(contactInfo);
			ReservationUtil.addLccSegments(lccClientReservation, lccSegments);

			LCCClientReservationPax parent = null;
			Integer maxPaxSeq = 0;
			Integer selectedSeq = PaxTypeUtils.getPaxSeq(infTravellingAdult);
			for (LCCClientReservationPax adultPax : colpaxs) {
				Integer adultPaxSeq = PaxTypeUtils.getPaxSeq(adultPax.getTravelerRefNumber());

				if (adultPaxSeq.compareTo(selectedSeq) == 0) {
					parent = adultPax;
					adultSeqId.add(adultPaxSeq.toString());
				}
				// fix to get max sequence to add to infant
				if (adultPaxSeq.compareTo(Integer.valueOf(maxPaxSeq)) > 0) {
					maxPaxSeq = adultPaxSeq;
				}
			}

			if (parent != null) {
				lccClientReservationPax.setAttachedPaxId(parent.getPaxSequence());
				lccClientReservationPax.setFirstName(infFName);
				lccClientReservationPax.setLastName(infLName);
				if (infTitle != null) {
					lccClientReservationPax.setTitle(infTitle);
				}

				if (infNationality != null && !infNationality.trim().equals("")) {
					lccClientReservationPax.setNationalityCode(Integer.valueOf(infNationality));
				}

				lccClientReservationPax
						.setDateOfBirth(infDOB != null && infDOB.trim().length() > 0 ? BookingUtil.stringToDate(infDOB) : null);
				lccClientReservationPax.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
				int totalPaxCount = maxPaxSeq;
				lccClientReservationPax.setPaxSequence(totalPaxCount + 1);
				lccClientReservationPax.setTravelerRefNumber(PaxTypeUtils.travelerReference(lccClientReservationPax));
				lccClientReservationPax.setParent(parent);

				LCCClientReservationAdditionalPax addInfo = new LCCClientReservationAdditionalPax();
				addInfo.setNationalIDNo(infNIC);
				addInfo.setPassportNo(infPnrPaxCatFOIDNumber);
				addInfo.setVisaDocPlaceOfIssue(infPnrPaxCatFOIDPlace);
				addInfo.setPlaceOfBirth(infPnrPaxPlaceOfBirth);
				addInfo.setVisaApplicableCountry(infVisaApplicableCountry);
				addInfo.setVisaDocIssueDate(
						(infVisaDocIssueDate == null || infVisaDocIssueDate.isEmpty()) ? null : sdf.parse(infVisaDocIssueDate));
				addInfo.setVisaDocNumber(infVisaDocNumber);
				addInfo.setVisaDocPlaceOfIssue(infVisaDocPlaceOfIssue);
				addInfo.setPassportExpiry((infPnrPaxCatFOIDExpiry == null || infPnrPaxCatFOIDExpiry.isEmpty())
						? null
						: sdf.parse(infPnrPaxCatFOIDExpiry));
				addInfo.setPassportIssuedCntry(infPnrPaxCatFOIDPlace);

				lccClientReservationPax.setLccClientAdditionPax(addInfo);
				parent.getInfants().add(lccClientReservationPax);
				// Add parent
				lccClientReservation.addPassenger(parent);
				// Add Infant
				lccClientReservation.addPassenger(lccClientReservationPax);
			}

			ReservationBalanceTO lCCClientReservationBalanceTO = null;

			if (saveMode.equals("ADDINF")) {
				LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO = null;
				lCCClientResAlterQueryModesTO = LCCClientResAlterQueryModesTO.composeAddInfantQueryRequest(groupPNR,
						bookingInfo.getVersion(), isGroupPNR, lccClientReservationPax);
				lCCClientResAlterQueryModesTO.setPaxAdultsList(adultSeqId);

				List<LCCClientExternalChgDTO> handlingFeeChgs = getHandlingFeeChargeList(bookingShoppingCart);
				if (handlingFeeChgs != null && !handlingFeeChgs.isEmpty()) {
					LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
					lccClientSegmentAssembler.getPassengerExtChargeMap().put(parent.getPaxSequence(), handlingFeeChgs);
					lCCClientResAlterQueryModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);
				}

				lCCClientReservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
						.getResAlterationBalanceSummary(lCCClientResAlterQueryModesTO, trackInfoDTO);

				if (isGroupPNR) {
					BigDecimal infCharge = lCCClientReservationBalanceTO.getInfantCharge();
					this.infantTo = new InfantInfoTO();
					this.infantTo.setInfantCharge(infCharge);
				} else {
					this.infantTo = ModuleServiceLocator.getAirproxyPassengerBD().getInfantCharge(groupPNR, true);
				}

				if (AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
						.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, RequestHandlerUtil.getAgentCode(request))) {

					ExternalChgDTO externalChgDTO = bookingShoppingCart.getAllExternalChargesMap()
							.get(EXTERNAL_CHARGES.CREDIT_CARD);
					externalChgDTO.calculateAmount(
							getTotalEligibleForCCCharge(parent, lCCClientReservationBalanceTO.getPassengerSummaryList()));
					bookingShoppingCart.addSelectedExternalCharge(externalChgDTO);
					// Calculate CC & Handling charge JN tax
					ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

					BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
					AvailPreferencesTO availPref = baseAvailRQ.getAvailPreferences();
					availPref.setAppIndicator(ApplicationEngine.XBE);
					if (isGroupPNR) {
						availPref.setSearchSystem(SYSTEM.INT);
					} else {
						availPref.setSearchSystem(SYSTEM.AA);
					}
					List<FlightSegmentTO> flightSegmentTOs = ServiceTaxConverterUtil.adaptFlightSegments(lccSegments);
					Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
					Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
					paxWisePaxTypes.put(parent.getPaxSequence(), PaxTypeTO.INFANT);

					List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
					chgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegmentTOs,
							externalChgDTO.getAmount(), externalChgDTO, false));
					paxWiseExternalCharges.put(parent.getPaxSequence(), chgDTOs);

					ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
					serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
					serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
					serviceTaxQuoteCriteriaDTO.setPaxState(contactInfo.getState());
					serviceTaxQuoteCriteriaDTO.setPaxCountryCode(contactInfo.getCountryCode());
					if (contactInfo.getTaxRegNo() != null && contactInfo.getTaxRegNo().length() > 0) {
						serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(true);
					} else {
						serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(false);
					}
					serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
					serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
					serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(resInfo.getTransactionId());
					serviceTaxQuoteCriteriaDTO.setCalculateOnlyForExternalCharges(true);


					Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator
							.getAirproxyReservationBD()
							.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfo());
					List<LCCClientExternalChgDTO> groupedServiceTaxes = ServiceTaxCalculatorUtil
							.groupServiceTax(serviceTaxQuoteRS, PaxTypeTO.INFANT, parent.getPaxSequence());

					BigDecimal totalServiceTax = AccelAeroCalculator.getDefaultBigDecimalZero();
					for (LCCClientExternalChgDTO groupedServiceTax : groupedServiceTaxes) {
						totalServiceTax = totalServiceTax.add(groupedServiceTax.getAmount());
					}

					if (!resParam.isPartialPaymentModify()) {
						BigDecimal totalAmountDue = lCCClientReservationBalanceTO.getTotalAmountDue();
						lCCClientReservationBalanceTO.setTotalAmountDue(totalAmountDue
								.add(bookingShoppingCart.getTotalSelctedExternalChargesAmount()).add(totalServiceTax));

						for (LCCClientPassengerSummaryTO paxSummaryTo : lCCClientReservationBalanceTO.getPassengerSummaryList()) {
							if (selectedSeq == PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()).intValue()) {
								paxSummaryTo.setTotalAmountDue(AccelAeroCalculator.add(paxSummaryTo.getTotalAmountDue(),
										bookingShoppingCart.getTotalSelctedExternalChargesAmount(), totalServiceTax));
								paxSummaryTo.getSegmentSummary()
										.setNewExtraFeeAmount(AccelAeroCalculator.add(
												paxSummaryTo.getSegmentSummary().getNewExtraFeeAmount(),
												bookingShoppingCart.getTotalSelctedExternalChargesAmount(), totalServiceTax));
							}
						}
					}
				}
				bookingShoppingCart.setReservationBalance(lCCClientReservationBalanceTO);
				bookingShoppingCart.setReservationAlterModesTO(lCCClientResAlterQueryModesTO);
				bookingShoppingCart.setPaidAmount(lCCClientReservationBalanceTO.getTotalPaidAmount());
				bookingShoppingCart.setPayablePaxCount(
						ReservationPaxUtil.getPayablePaxCount(lCCClientReservationBalanceTO.getPassengerSummaryList()));

				String pendingAgentCommissions = CommonUtil.getPendingAgentCommissions(RequestHandlerUtil.getAgentCode(request),
						groupPNR);
				PaymentSummaryTO oldPysummary = ReservationUtil.transformJsonPaymentSummary(request.getParameter("resPaySumm"));
				this.paymentSummaryTO = ReservationUtil.createPaymentSummary(AppSysParamsUtil.getBaseCurrency(),
						AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getTotalAmountDue()),
						AccelAeroCalculator.formatAsDecimal(BigDecimal.ZERO), oldPysummary.getModificationCharges(),
						oldPysummary.getPaid(),
						AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getTotalPrice()),
						oldPysummary.getTotalFare(),
						AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
								.add(new BigDecimal(oldPysummary.getTotalTaxSurCharges()), infantTo.getInfantCharge())),
						"", "", pendingAgentCommissions, null, "10.00");

				if (this.countryCurrExchangeRate != null) {
					this.paymentSummaryTO.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
				}
				Object[] paySumObj = ConfirmUpdateUtil.getPaxPaySummaryList(lCCClientReservationBalanceTO, colpaxs, true);
				this.passengerPayment = (Collection<PaymentPassengerTO>) paySumObj[0];

				// If total due is Zero then there's no payment to be made.
				if (BigDecimal.ZERO.compareTo(lCCClientReservationBalanceTO.getTotalAmountDue()) == 0) {
					paysuccess = true;
				}

				request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShoppingCart);
			}

			if (saveMode.equals("SAVEINF")
					|| resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					|| resParam.isPartialPaymentModify() || (lCCClientReservationBalanceTO != null
							&& lCCClientReservationBalanceTO.getTotalAmountDue().compareTo(BigDecimal.ZERO) <= 0)) {
				Integer ipgId = null;
				if (request.getParameter(WebConstants.REQ_HDN_IPG_ID) != null
						&& !"".equals(request.getParameter(WebConstants.REQ_HDN_IPG_ID))) {
					ipgId = new Integer(request.getParameter(WebConstants.REQ_HDN_IPG_ID));
				}
				if (!saveMode.equals("SAVEINF")) {
					payment.setType(PaymentMethod.NOPAY.getCode());
					if (!isGroupPNR && (lCCClientReservationBalanceTO.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0
							|| resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD))) {
						intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;
					}
				} else {
					if (payment.getMode().equals("FORCE")) {
						intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED;
					}
				}
				Map<Integer, LCCClientPaymentAssembler> payMap = null;

				payMap = getAlterModesTo(payment, creditInfo, ipgId, request.getParameter("paxPayments"), bookingShoppingCart,
						isGroupPNR, selectedSeq);

				boolean autoCnxEnabled = false;
				boolean hasBufferTimeAutoCnx = false;
				Map<String, String> mapPrivileges = new HashMap<String, String>();
				if (AppSysParamsUtil.isAutoCancellationEnabled()
						&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(resInfo.getReservationStatus())
						&& (resParam.isPartialPaymentModify() || payment.getMode().equals("FORCE"))
						&& ((lCCClientReservationBalanceTO != null
								&& lCCClientReservationBalanceTO.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0))) {
					autoCnxEnabled = true;
					mapPrivileges = (Map<String, String>) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
					hasBufferTimeAutoCnx = AuthorizationsUtil.hasPrivilege(mapPrivileges.keySet(),
							PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALT_RES_AUTO_CNX);
				}
				boolean mcETGenerationEligible = checkMCSideETGenerationEligibility(isGroupPNR, lCCClientReservationBalanceTO,
						parent, resParam.isPartialPaymentModify());

				ModuleServiceLocator.getAirproxyPassengerBD().addInfant(lccClientReservation, intPaymentMode, isGroupPNR, payMap,
						AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), autoCnxEnabled,
						hasBufferTimeAutoCnx, mapPrivileges, trackInfoDTO, mcETGenerationEligible,
						bookingShoppingCart.getAllExternalChargesMap());

				if (saveMode.equals("SAVEINF")) {
					paysuccess = true;
				}
				this.infantFare = false;
			}

		} catch (Exception e) {
			success = false;

			if (e instanceof ModuleException) {
				String errorCode = ((ModuleException) e).getExceptionCode();
				if ("airinventory.logic.blockseats.segavailability.failed".equals(errorCode)) {
					((ModuleException) e).setExceptionCode("airinventory.logic.blockseats.segavailability.addInfant.failed");
				}
			}

			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private boolean checkMCSideETGenerationEligibility(boolean isGroupPNR, ReservationBalanceTO lCCClientReservationBalanceTO,
			LCCClientReservationPax parent, boolean confirmPartialpaymentModify) {
		if (isGroupPNR && lCCClientReservationBalanceTO != null
				&& lCCClientReservationBalanceTO.getPassengerSummaryList() != null) {
			for (LCCClientPassengerSummaryTO lCCClientPassengerSummaryTO : lCCClientReservationBalanceTO
					.getPassengerSummaryList()) {
				if (parent.getTravelerRefNumber().equals(lCCClientPassengerSummaryTO.getTravelerRefNumber())
						&& lCCClientPassengerSummaryTO.getTotalAmountDue().compareTo(BigDecimal.ZERO) == 0) {
					return true;
				}
			}
		}
		if (!confirmPartialpaymentModify) {
			return true;
		}
		return false;
	}

	private BigDecimal getTotalEligibleForCCCharge(LCCClientReservationPax parent,
			Collection<LCCClientPassengerSummaryTO> paxSummaryList) {
		BigDecimal totalEligibleForCCCharge = BigDecimal.ZERO;

		for (LCCClientPassengerSummaryTO paxSummary : paxSummaryList) {
			if (parent.getPaxSequence().equals(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber()))) {
				totalEligibleForCCCharge = paxSummary.getTotalAmountDue().subtract(parent.getTotalAvailableBalance());
			}
		}
		return totalEligibleForCCCharge;
	}

	private Map<Integer, LCCClientPaymentAssembler> getAlterModesTo(PaymentTO payment, CreditCardTO creditInfo, Integer ipgId,
			String paxCredit, BookingShoppingCart bookingShoppingCart, boolean isGroupPNR, int selectedSeq) throws Exception {

		Map<Integer, LCCClientPaymentAssembler> paxPayMap = new HashMap<Integer, LCCClientPaymentAssembler>();
		PayCurrencyDTO cardPayCurrencyDTO = null;
		PayCurrencyDTO onAccPayCurrencyDTO = null;
		PayCurrencyDTO bspPayCurrencyDTO = null;
		PayCurrencyDTO cashPayCurrencyDTO = null;
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		BigDecimal totalPaxCredit = BigDecimal.ZERO;

		PaymentMethod paymentMethod = payment.getPaymentMethod();
		ReservationBalanceTO lCCClientReservationBalanceTO = bookingShoppingCart.getReservationBalance();

		JSONArray jsonPaxPayArray = (JSONArray) new JSONParser().parse(paxCredit);

		List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) lCCClientReservationBalanceTO
				.getPassengerSummaryList();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false);
		boolean isBSPPayment = paymentMethod.getCode().equals(PaymentMethod.BSP.getCode());
		boolean isCreditCardPayment = paymentMethod.getCode().equals(PaymentMethod.CRED.getCode());

		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
				BigDecimal amountDue = AccelAeroCalculator.add(paxSummaryTo.getTotalCreditAmount().negate(),
						paxSummaryTo.getTotalAmountDue());
				LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();

				if (selectedSeq == PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()).intValue()) {

					if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
							.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, RequestHandlerUtil.getAgentCode(request)))
							|| isBSPPayment || isCreditCardPayment) {

						paymentAssembler.addExternalCharges(bookingShoppingCart.getSelectedExternalCharges().values());
						Collection<ExternalChgDTO> externalInfantServiceTaxCharges = externalChargesMediator
								.getPaxServiceTaxExternalCharge(selectedSeq);
						paymentAssembler.addExternalCharges(externalInfantServiceTaxCharges);
					}

				}
				LCCClientExternalChgDTO handlingFeeChgDTO = null;
				if (selectedSeq == PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()).intValue()) {
					handlingFeeChgDTO = calculateHandlingFeeForAddInfantOperation();
					if (handlingFeeChgDTO != null && handlingFeeChgDTO.getAmount() != null
							&& handlingFeeChgDTO.getAmount().doubleValue() > 0) {
						paymentAssembler.addExternalCharges(handlingFeeChgDTO);
					}
				}

				Iterator<?> itIter = jsonPaxPayArray.iterator();
				JSONArray jPCInfo = null;

				while (itIter.hasNext()) {
					JSONObject jPPObj = (JSONObject) itIter.next();
					String jtrevelRefNo = BeanUtils.nullHandler(jPPObj.get("displayTravelerRefNo"));

					if (jtrevelRefNo.equalsIgnoreCase(paxSummaryTo.getTravelerRefNumber())) {
						jPCInfo = (JSONArray) jPPObj.get("paxCreditInfo");
					}
				}
				if (jPCInfo != null) {
					Iterator<?> uIter = jPCInfo.iterator();
					while (uIter.hasNext()) {
						JSONObject jUObj = (JSONObject) uIter.next();
						String carrierCode = (String) jUObj.get("carrier");
						Integer debitPaxRefNo = new Integer((String) jUObj.get("paxCreditId"));
						BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
						amountDue = AccelAeroCalculator.add(amountDue, amount.negate());
						BigDecimal residingCarrierAmount = new BigDecimal((String) jUObj.get("residingCarrierAmount"));
						String pnr = (String) jUObj.get("pnr");
						paymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, amount,
								exchangeRateProxy.getExecutionDate(), residingCarrierAmount, null, null,
								PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()), null, pnr, null);
						totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, amount);
					}
				}

				if (amountDue.compareTo(BigDecimal.ZERO) >= 0) {

					if (handlingFeeChgDTO != null && handlingFeeChgDTO.getAmount().doubleValue() > 0
							&& !AccelAeroCalculator.isLessThan(amountDue, handlingFeeChgDTO.getAmount())) {
						amountDue = AccelAeroCalculator.subtract(amountDue, handlingFeeChgDTO.getAmount());
					}

					if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
						if (cashPayCurrencyDTO == null) {
							cashPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(getHdnSelCurrency(), exchangeRateProxy);
						}
						paymentAssembler.addCashPayment(amountDue, cashPayCurrencyDTO, exchangeRateProxy.getExecutionDate(),
								payment.getPaymentReference(), payment.getActualPaymentMethod(), null, null);
					} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {

						if (onAccPayCurrencyDTO == null) {
							String loggedInAgencyCode = RequestHandlerUtil.getAgentCode(request);
							onAccPayCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(getHdnSelCurrency(), loggedInAgencyCode,
									payment.getAgent(), exchangeRateProxy);
						}
						paymentAssembler.addAgentCreditPayment(payment.getAgent(), amountDue, onAccPayCurrencyDTO,
								exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
								payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);
					} else if (isBSPPayment) {

						if (bspPayCurrencyDTO == null) {
							String loggedInAgencyCode = RequestHandlerUtil.getAgentCode(request);
							String bspCurrency = ModuleServiceLocator.getCommonServiceBD()
									.getCurrencyCodeForStation(RequestHandlerUtil.getAgentStationCode(request));
							bspPayCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgencyCode,
									payment.getAgent(), exchangeRateProxy);
						}
						paymentAssembler.addAgentCreditPayment(payment.getAgent(), amountDue, bspPayCurrencyDTO,
								exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
								payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_BSP, null);
					} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
						if (cardPayCurrencyDTO == null) {
							cardPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(getHdnSelCurrency(), ipgId,
									exchangeRateProxy, false);
							ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
									cardPayCurrencyDTO.getPayCurrencyCode());
						}

						paymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType().trim()).intValue(),
								creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
								creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(), creditInfo.getCardCVV().trim(),
								amountDue, AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO,
								cardPayCurrencyDTO, exchangeRateProxy.getExecutionDate(), creditInfo.getCardHoldersName(),
								payment.getPaymentReference(), payment.getActualPaymentMethod(), null, null);

					}
				} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
					if (cardPayCurrencyDTO == null) {
						cardPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(getHdnSelCurrency(), ipgId,
								exchangeRateProxy, false);
						ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
								cardPayCurrencyDTO.getPayCurrencyCode());
					}

					paymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType().trim()).intValue(),
							creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
							creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(), creditInfo.getCardCVV().trim(),
							amountDue, AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO,
							cardPayCurrencyDTO, exchangeRateProxy.getExecutionDate(), creditInfo.getCardHoldersName(),
							payment.getPaymentReference(), payment.getActualPaymentMethod(), null, null);

				}
				// paxPayMap.put(PaxTypeUtils.getPnrPaxId(paxSummaryTo.getTravelerRefNumber()), paymentAssembler);
				if (isGroupPNR && paymentAssembler.getTotalPayAmount().doubleValue() <= 0) {
					if (isAnyValidExternalChargesApplicable(handlingFeeChgDTO)
							&& paymentMethod.getCode().equals(PaymentMethod.NOPAY.getCode())
							&& paymentAssembler.getPerPaxExternalCharges() != null
							&& !paymentAssembler.getPerPaxExternalCharges().isEmpty()) {

						paxPayMap.put(PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()), paymentAssembler);
					}

					continue;
				}
				paxPayMap.put(PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()), paymentAssembler);
			}

		}

		return paxPayMap;
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(
				payment != null ? payment.getAgent() : ((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	// getters

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the bookingInfo
	 */
	public BookingTO getBookingInfo() {
		return bookingInfo;
	}

	/**
	 * @param bookingInfo
	 *            the bookingInfo to set
	 */
	public void setBookingInfo(BookingTO bookingInfo) {
		this.bookingInfo = bookingInfo;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isInfantFare() {
		return infantFare;
	}

	public void setInfantFare(boolean infantFare) {
		this.infantFare = infantFare;
	}

	public InfantInfoTO getInfantTo() {
		return infantTo;
	}

	public void setInfantTo(InfantInfoTO infantTo) {
		this.infantTo = infantTo;
	}

	public String getSaveMode() {
		return saveMode;
	}

	public void setSaveMode(String saveMode) {
		this.saveMode = saveMode;
	}

	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public PaymentTO getPayment() {
		return payment;
	}

	public void setPayment(PaymentTO payment) {
		this.payment = payment;
	}

	public CreditCardTO getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(CreditCardTO creditInfo) {
		this.creditInfo = creditInfo;
	}

	public String getHdnSelCurrency() {
		return hdnSelCurrency;
	}

	public void setHdnSelCurrency(String hdnSelCurrency) {
		this.hdnSelCurrency = hdnSelCurrency;
	}

	public boolean isPaysuccess() {
		return paysuccess;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	private List<LCCClientExternalChgDTO> getHandlingFeeChargeList(BookingShoppingCart bookingShoppingCart)
			throws ModuleException, Exception {
		List<LCCClientExternalChgDTO> listExteCHs = new ArrayList<>();

		LCCClientExternalChgDTO chg = calculateHandlingFeeForAddInfantOperation();

		if (chg != null)
			listExteCHs.add(chg);

		return listExteCHs;
	}

	private LCCClientExternalChgDTO calculateHandlingFeeForAddInfantOperation() throws ModuleException, Exception {

		int operationMode = CommonsConstants.ModifyOperation.ADD_INFANT.getOperation();
		ExternalChgDTO handlingChgDTO = ModuleServiceLocator.getReservationBD().getHandlingFeeForModification(operationMode);
		LCCClientExternalChgDTO chg = null;
		if (handlingChgDTO != null && handlingChgDTO.getAmount() != null && handlingChgDTO.getAmount().doubleValue() > 0) {
			String flightRef = getFirstFlightReference();
			chg = new LCCClientExternalChgDTO();
			chg.setAmount(handlingChgDTO.getAmount());
			chg.setExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
			chg.setCode(handlingChgDTO.getChargeCode());
			chg.setFlightRefNumber(flightRef);
			chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightRef));
			chg.setOperationMode(operationMode);
		}

		return chg;
	}

	private String getFirstFlightReference() throws Exception {
		Collection<LCCClientReservationSegment> lccSegments = ModifyReservationJSONUtil
				.transformJsonSegments(request.getParameter("pnrSegments"));

		if (lccSegments != null && !lccSegments.isEmpty()) {
			for (LCCClientReservationSegment segment : lccSegments) {
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())
						&& !segment.isFlownSegment()) {
					if (!StringUtil.isNullOrEmpty(segment.getFlightSegmentRefNumber())
							&& !StringUtil.isNullOrEmpty(segment.getBookingFlightSegmentRefNumber()))
						return FlightRefNumberUtil.composePnrSegRPHFromFlightRPH(segment.getFlightSegmentRefNumber(),
								segment.getBookingFlightSegmentRefNumber());
				}
			}
		}

		return null;
	}

	private boolean isAnyValidExternalChargesApplicable(LCCClientExternalChgDTO handlingFeeChgDTO) {
		if ((AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
				.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, RequestHandlerUtil.getAgentCode(request)))
				|| (handlingFeeChgDTO != null && handlingFeeChgDTO.getAmount().doubleValue() > 0)) {
			return true;
		}
		return false;
	}

}
