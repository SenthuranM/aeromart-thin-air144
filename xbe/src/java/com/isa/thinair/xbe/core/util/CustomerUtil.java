package com.isa.thinair.xbe.core.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerQuestionaire;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.utils.LmsMemberGenderEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.dto.CustomerQuestionaireDTO;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.xbe.api.dto.v2.CustomerDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class CustomerUtil {

	private static Log log = LogFactory.getLog(CustomerUtil.class);

	public static ArrayList<Map<String, Object>> getCustomerDTOList(List<Customer> customerList) {
		ArrayList<Map<String, Object>> customerDTOList = new ArrayList<Map<String, Object>>();
		CustomerDTO customerDTO = null;
		Customer customer = null;
		int i = 0;
		Iterator<Customer> iterator = customerList.iterator();
		while (iterator.hasNext()) {
			customer = iterator.next();
			customerDTO = customerModelToDto(customer);
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("customerDTO", customerDTO);
			row.put("id", i++);
			customerDTOList.add(row);
		}

		return customerDTOList;
	}

	public static Customer customerDtoToModel(CustomerDTO customerDTO) {
		Customer customer = null;
		if (customerDTO.getCustomerID() != 0) {
			try {
				customer = getCustomer(customerDTO.getCustomerID());
			} catch (ModuleException e) {
				log.error("CustomerUtil ==> customerDtoToModel(Customer)", e);
			}
		} else {
			customer = new Customer();
		}
		customer.setEmailId(customerDTO.getEmailId());
		customer.setTitle(customerDTO.getTitle());
		customer.setFirstName(customerDTO.getFirstName());
		customer.setLastName(customerDTO.getLastName());
		customer.setCountryCode(customerDTO.getCountryCode());
		customer.setGender(customerDTO.getGender());
		customer.setNationalityCode("".equals(customerDTO.getNationalityCode()) ? null : (new Integer(customerDTO
				.getNationalityCode())));
		String birthDay = customerDTO.getDateOfBirth();
		SimpleDateFormat dateFormat = null;

		if(!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(customerDTO.getZipCode())){
			customer.setZipCode(customerDTO.getZipCode());
		}
		
		if (!birthDay.equals("")) {

			if (birthDay.indexOf(' ') != -1) {
				birthDay = birthDay.substring(0, birthDay.indexOf(' '));
			}

			if (birthDay.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			}
			if (birthDay.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			}

			Date dateOfBirth = null;
			try {
				dateOfBirth = dateFormat.parse(birthDay);
			} catch (Exception ex) {
			}
			customer.setDateOfBirth(dateOfBirth);
		}

		customer.setTelephone(customerDTO.getTelephone());
		customer.setFax(customerDTO.getFax());
		customer.setMobile(customerDTO.getMobile());
		customer.setOfficeTelephone(customerDTO.getOfficeTelephone());
		customer.setPassword(customerDTO.getPassword());
		customer.setIsLmsMember('N');
		customer.setSendPromoEmail(customerDTO.getSendPromoEmail());
		customer.setAddressStreet(customerDTO.getAddressStreet());
		customer.setAddressLine(customerDTO.getAddressLine());

		//emergency contact details
		customer.setEmgnEmail(customerDTO.getEmgnEmail());
		customer.setEmgnTitle(customerDTO.getEmgnTitle());
		customer.setEmgnFirstName(customerDTO.getEmgnFirstName());
		customer.setEmgnLastName(customerDTO.getEmgnLastName());
		customer.setEmgnPhoneNo(customerDTO.getEmgnPhoneNo());
		
		//questionaire
		Set<CustomerQuestionaire> setQuestions = customer.getCustomerQuestionaire();
		List<CustomerQuestionaireDTO> customerQuestionaireDTOs = customerDTO.getCustomerQuestionaireDTO();
		// Update Customer
		if (setQuestions != null) {
			Iterator iterSet = setQuestions.iterator();
			CustomerQuestionaire customerQuestionaire;
			while (iterSet.hasNext()) {
				customerQuestionaire = (CustomerQuestionaire) iterSet.next();
				if (customerQuestionaireDTOs != null) {
					for (CustomerQuestionaireDTO customerQuestionaireDTO : customerQuestionaireDTOs) {
						if (customerQuestionaireDTO != null
								&& Integer.parseInt(customerQuestionaireDTO.getQuestionKey()) == customerQuestionaire
										.getQuestionKey()) {
							if (customerQuestionaireDTO.getQuestionAnswer() != null) {
								customerQuestionaire.setQuestionAnswer(customerQuestionaireDTO.getQuestionAnswer().trim());
								setQuestions.add(customerQuestionaire);
							}
							break;
						}
					}
				}
			}
		} else { // New Customer
			setQuestions = new HashSet<CustomerQuestionaire>();
			CustomerQuestionaire customerQuestionaire;
			if (customerQuestionaireDTOs != null) {
				for (CustomerQuestionaireDTO customerQuestionaireDTO : customerQuestionaireDTOs) {
					customerQuestionaire = new CustomerQuestionaire();
					if (customerQuestionaireDTO != null) {
						customerQuestionaire.setQuestionAnswer(customerQuestionaireDTO.getQuestionAnswer().trim());
						customerQuestionaire.setQuestionKey(Integer.parseInt(customerQuestionaireDTO.getQuestionKey()));
						setQuestions.add(customerQuestionaire);
					}
				}
			}
		}
		customer.setCustomerQuestionaire(setQuestions);
		
		return customer;
	}

	public static CustomerDTO customerModelToDto(Customer customer) {
		CustomerDTO customerDTO = null;
		SimpleDateFormat dateFormat = null;
		String dob = "";
		if (customer != null) {
			dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			customerDTO = new CustomerDTO();
			customerDTO.setZipCode(StringUtil.getNotNullString(customer.getZipCode()));
			customerDTO.setCountryCode(StringUtil.getNotNullString(customer.getCountryCode()));
			customerDTO.setNationalityCode(StringUtil.getNotNullString(new Integer(customer.getNationalityCode()).toString()));
			if (customer.getDateOfBirth() != null) {
				dob = dateFormat.format(customer.getDateOfBirth());
			}
			customerDTO.setDateOfBirth(dob);
			customerDTO.setEmailId(StringUtil.getNotNullString(customer.getEmailId()));
			customerDTO.setFax(StringUtil.getNotNullString(customer.getFax()));
			customerDTO.setFirstName(StringUtil.getNotNullString(customer.getFirstName()));
			customerDTO.setGender(StringUtil.getNotNullString(customer.getGender()));
			customerDTO.setLastName(StringUtil.getNotNullString(customer.getLastName()));
			customerDTO.setMobile(StringUtil.getNotNullString(customer.getMobile()));
			customerDTO.setOfficeTelephone(StringUtil.getNotNullString(customer.getOfficeTelephone()));
			customerDTO.setTelephone(StringUtil.getNotNullString(customer.getTelephone()));
			customerDTO.setTitle(StringUtil.getNotNullString(customer.getTitle()));
			if (customer.getLMSMemberDetails() != null) {
				customerDTO.setLmsMemberDTO(lmsMemberModelToDto(customer.getLMSMemberDetails()));
			}
			customerDTO.setIsLmsMember(customer.getIsLmsMember());
			customerDTO.setEmgnEmail(StringUtil.getNotNullString(customer.getEmgnEmail()));
			customerDTO.setEmgnFirstName(StringUtil.getNotNullString(customer.getEmgnFirstName()));
			customerDTO.setEmgnLastName(StringUtil.getNotNullString(customer.getEmgnLastName()));
			customerDTO.setEmgnPhoneNo(StringUtil.getNotNullString(customer.getEmgnPhoneNo()));
			customerDTO.setEmgnTitle(StringUtil.getNotNullString(customer.getEmgnTitle()));
			customerDTO.setAddressLine(StringUtil.getNotNullString(customer.getAddressLine()));
			customerDTO.setAddressStreet(StringUtil.getNotNullString(customer.getAddressStreet()));
			List<CustomerQuestionaireDTO> setCusQusDTO = new ArrayList<CustomerQuestionaireDTO>();
			CustomerQuestionaire customerQuestionaire;
			CustomerQuestionaireDTO customerQuestionaireDTO;
			Set<CustomerQuestionaire> set = customer.getCustomerQuestionaire();
			Iterator<CustomerQuestionaire> iterSet = set.iterator();
			while (iterSet.hasNext()) {
				customerQuestionaire = (CustomerQuestionaire) iterSet.next();
				customerQuestionaireDTO = new CustomerQuestionaireDTO();
				customerQuestionaireDTO.setQuestionKey(StringUtil.getNotNullString(customerQuestionaire.getQuestionKey()
						.toString()));
				customerQuestionaireDTO.setQuestionAnswer(StringUtil.getNotNullString(customerQuestionaire.getQuestionAnswer()));
				setCusQusDTO.add(customerQuestionaireDTO);
			}
			customerDTO.setCustomerQuestionaireDTO(setCusQusDTO);
			customerDTO.setSendPromoEmail(customer.getSendPromoEmail());
		}
		return customerDTO;
	}

	public static LmsMember lmsMemberDtoToModel(LmsMemberDTO lmsMemberDTO) throws ModuleException {
		LmsMember lmsMember = null;
		try {
			lmsMember = getLmsMember(lmsMemberDTO.getEmailId());
		} catch (ModuleException e) {
			log.error("CustomerUtil ==> getLmsMember(String)", e);
		}
		return lmsMember;
	}

	public static LmsMemberDTO lmsMemberModelToDto(LmsMember lmsMember) {
		LmsMemberDTO lmsMemberDTO = new LmsMemberDTO();

		lmsMemberDTO.setEmailId(lmsMember.getFfid());
		lmsMemberDTO.setFirstName(lmsMember.getFirstName());
		lmsMemberDTO.setMiddleName(StringUtil.getNotNullString(lmsMember.getMiddleName()));
		lmsMemberDTO.setLastName(lmsMember.getLastName());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String dateOfBirth = dateFormat.format(lmsMember.getDateOfBirth());
		lmsMemberDTO.setDateOfBirth(dateOfBirth);

		lmsMemberDTO.setPassportNum(StringUtil.getNotNullString(lmsMember.getPassportNum()));
		lmsMemberDTO.setLanguage(StringUtil.getNotNullString(lmsMember.getLanguage()));
		lmsMemberDTO.setNationalityCode(StringUtil.getNotNullString(lmsMember.getNationalityCode()));
		lmsMemberDTO.setResidencyCode(StringUtil.getNotNullString(lmsMember.getResidencyCode()));
		lmsMemberDTO.setHeadOFEmailId(StringUtil.getNotNullString(lmsMember.getHeadOFEmailId()));
		lmsMemberDTO.setRefferedEmail(StringUtil.getNotNullString(lmsMember.getRefferedEmail()));
		lmsMemberDTO.setEmailStatus(lmsMember.getEmailConfirmed() == 'Y' ? "Y" : "N");
		if (lmsMember.getEmailConfirmed() == 'Y') {
			LoyaltyManagementBD lmsManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			try {
				LoyaltyPointDetailsDTO existingMember = lmsManagementBD.getLoyaltyMemberPointBalances(lmsMember.getFfid(), lmsMember.getMemberExternalId());
				lmsMemberDTO.setAvailablePoints(String.valueOf(existingMember.getMemberPointsAvailable()));
			} catch (ModuleException e) {
				log.error("CustomerUtil ==> lmsMemberModelToDto(LmsMember)", e);
			}
		}
		return lmsMemberDTO;
	}

	public static Customer getCustomer(int customerId) throws ModuleException {
		Customer customer = null;
		try {
			customer = ModuleServiceLocator.getCustomerBD().getCustomer(customerId);
		} catch (ModuleException e) {
			log.error("CustomerUtil ==> getLmsMember(String)", e);
		}
		
		return customer;
	}
	
	public static LmsMember getLmsMember(String ffid) throws ModuleException {
		LmsMember lmsMember = null;
		try {
			lmsMember = ModuleServiceLocator.getLmsMemberServiceBD().getLmsMember(ffid);
		} catch (ModuleException e) {
			log.error("CustomerUtil ==> getLmsMember(String)", e);
		}
		return lmsMember;
	}
	
	public static LmsMember getModelLmsMember(LmsMemberDTO lmsDetails, int customerId) {
		
		LmsMember lmsMember = new LmsMember();
		
	    lmsMember.setFfid(lmsDetails.getEmailId());		
		lmsMember.setFirstName(lmsDetails.getFirstName());
		lmsMember.setMiddleName("".equals(lmsDetails.getMiddleName()) ? null : lmsDetails.getMiddleName());
	    lmsMember.setLastName(lmsDetails.getLastName());
	    lmsMember.setCustomerId(customerId);
	    String birthDay = lmsDetails.getDateOfBirth();
		SimpleDateFormat dateFormat = null;

		if (!birthDay.equals("")) {

			if (birthDay.indexOf(' ') != -1) {
				birthDay = birthDay.substring(0, birthDay.indexOf(' '));
			}

			if (birthDay.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			}
			if (birthDay.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			}

			Date dateOfBirth = null;
			try {
				dateOfBirth = dateFormat.parse(birthDay);
			} catch (Exception ex) {
			}
			lmsMember.setDateOfBirth(dateOfBirth);
		}
	    
		lmsMember.setMobileNumber(LmsCommonUtil.getValidPhoneNumber(lmsDetails.getMobileNumber()));
		lmsMember.setPhoneNumber("".equals(lmsDetails.getPhoneNumber()) ? null : LmsCommonUtil.getValidPhoneNumber(lmsDetails
				.getPhoneNumber()));
		lmsMember.setRegDate(new Date());
	    lmsMember.setEmailConfSent('N');
	    lmsMember.setEmailConfirmed('N');
	    
		Map<String, String> locationRefMap = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyLocationExtReferences();
		Map<String, String> enrollingChannelMap = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyEnrollingChannelExtReferences();

		String enrollmentLocExtRef = lmsDetails.getAppCode();
		
		if (enrollmentLocExtRef.equals("APP_IBE")){
			lmsMember.setEnrollmentLocExtRef(locationRefMap.get(AppIndicatorEnum.APP_IBE.toString()));
			lmsMember.setEnrollingChannelExtRef(enrollingChannelMap.get(AppIndicatorEnum.APP_IBE.toString()));
		}
		else if (enrollmentLocExtRef.equals("APP_XBE")){
			lmsMember.setEnrollmentLocExtRef(locationRefMap.get(AppIndicatorEnum.APP_XBE.toString()));
			lmsMember.setEnrollingChannelExtRef(enrollingChannelMap.get(AppIndicatorEnum.APP_XBE.toString()));
		}

	    lmsMember.setPassportNum("".equals(lmsDetails.getPassportNum()) ? null :lmsDetails.getPassportNum());
	    lmsMember.setLanguage("".equals(lmsDetails.getLanguage()) ? null :lmsDetails.getLanguage().toLowerCase());
	    lmsMember.setNationalityCode("".equals(lmsDetails.getNationalityCode()) ? null : lmsDetails.getNationalityCode());
	    lmsMember.setResidencyCode("".equals(lmsDetails.getResidencyCode()) ? null :lmsDetails.getResidencyCode());	    
	    lmsMember.setHeadOFEmailId("".equals(lmsDetails.getHeadOFEmailId()) ? null : lmsDetails.getHeadOFEmailId());
	    lmsMember.setRefferedEmail("".equals(lmsDetails.getRefferedEmail()) ? null : lmsDetails.getRefferedEmail());
	    lmsMember.setEnrollingCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		
		String gender = lmsDetails.getGenderCode();
		
		if(gender.equalsIgnoreCase("MR")){
			lmsMember.setGenderTypeId(LmsMemberGenderEnum.MALE.getCode());
		} else if (gender.equalsIgnoreCase("MS")) {
			lmsMember.setGenderTypeId(LmsMemberGenderEnum.FEMALE.getCode());
		} else {
			lmsMember.setGenderTypeId(LmsMemberGenderEnum.UNDEFINED.getCode());
		}					
		
		String password = UUID.randomUUID().toString().replace("-", "").substring(0,20);
		
		lmsMember.setPassword(password.toUpperCase());
		
		return lmsMember;
	}

	public static boolean isExistingLMSNameMatches(LmsMemberDTO lmsDetails) throws ModuleException {
		LoyaltyMemberCoreDTO existingMember = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyMemberCoreDetails(
				lmsDetails.getEmailId());
	
		if (existingMember != null
				&& existingMember.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK
				&& (!existingMember.getMemberFirstName().equalsIgnoreCase(lmsDetails.getFirstName()) || !existingMember
						.getMemberLastName().equalsIgnoreCase(lmsDetails.getLastName()))) {
			return false;
		}
	
		return true;
	}

}
