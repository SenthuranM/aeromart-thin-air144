package com.isa.thinair.xbe.core.web.v2.action.modify;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * @author Zaki Saimeh
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class TransferAgentAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(TransferAgentAction.class);

	private boolean success = true;
	private String messageTxt;

	private String groupPNR;
	private String pnr;
	private String transfereeAgent;
	private String version;

	public String execute() {

		String strForward = S2Constants.Result.SUCCESS;
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			if (!isGroupPNR) {
				groupPNR = pnr;
			}

			ModuleServiceLocator.getAirproxyReservationBD().transferOwnership(groupPNR, transfereeAgent, version, isGroupPNR,
					getTrackInfo());

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		} finally {
			groupPNR = "";
			transfereeAgent = "";
		}

		return strForward;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the transfereeAgent
	 */
	public String getTransfereeAgent() {
		return transfereeAgent;
	}

	/**
	 * @param transfereeAgent
	 *            the transfereeAgent to set
	 */
	public void setTransfereeAgent(String transfereeAgent) {
		this.transfereeAgent = transfereeAgent;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}