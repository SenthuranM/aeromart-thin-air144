package com.isa.thinair.xbe.core.web.v2.action.system;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * Common currency exchange rates calculator
 * 
 * @author Baladewa
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ExtRateConvertorAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ExtRateConvertorAction.class);

	private boolean success = true;
	private String messageTxt = "";
	private String convetedAmountWithRuond = null;
	private String convetedAmountWithOutRuond = null;
	private String amount;
	private String fromCurrency;
	private String toCurrency;
	private String exchageRateWithRound;
	private String exchageRateWithOutRound;

	public String getConvetedAmountWithRuond() {
		return convetedAmountWithRuond;
	}

	public void setConvetedAmountWithRuond(String convetedAmountWithRuond) {
		this.convetedAmountWithRuond = convetedAmountWithRuond;
	}

	public String getConvetedAmountWithOutRuond() {
		return convetedAmountWithOutRuond;
	}

	public void setConvetedAmountWithOutRuond(String convetedAmountWithOutRuond) {
		this.convetedAmountWithOutRuond = convetedAmountWithOutRuond;
	}

	public String getExchageRateWithRound() {
		return exchageRateWithRound;
	}

	public void setExchageRateWithRound(String exchageRateWithRound) {
		this.exchageRateWithRound = exchageRateWithRound;
	}

	public String getExchageRateWithOutRound() {
		return exchageRateWithOutRound;
	}

	public void setExchageRateWithOutRound(String exchageRateWithOutRound) {
		this.exchageRateWithOutRound = exchageRateWithOutRound;
	}

	public String getFromCurrency() {
		return fromCurrency;
	}

	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public String getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;

		BigDecimal currMultiExRate = null;
		BigDecimal conAmt = null;
		BigDecimal amt = new BigDecimal(amount);
		try {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(toCurrency, ApplicationEngine.XBE);
			currMultiExRate = exchangeRateProxy.getExchangeRate(fromCurrency, toCurrency);
			Currency currency = currExRate.getCurrency();
			conAmt = AccelAeroRounderPolicy.convertAndRound(amt, currMultiExRate, currency.getBoundryValue(),
					currency.getBreakPoint());
			exchageRateWithOutRound = currMultiExRate.toPlainString();
			exchageRateWithRound = AccelAeroRounderPolicy.convertAndRound(BigDecimal.ONE, currMultiExRate,
					currency.getBoundryValue(), currency.getBreakPoint()).toPlainString();
			convetedAmountWithOutRuond = AccelAeroCalculator.multiply(amt, currMultiExRate).toPlainString();
			convetedAmountWithRuond = conAmt.toPlainString();
		} catch (Exception e) {

			success = false;
			if (e instanceof ModuleException
					&& "airmaster.exchangerate.undefined".equals(((ModuleException) e).getExceptionCode())) {
				messageTxt = "No effective exchange rate exists";
			} else {

				messageTxt = "Error occurred in exchange rate calculator ";
			}

			log.error(messageTxt, e);
		}
		return forward;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmount() {
		return amount;
	}
}
