/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.xbe.core.web.filter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.HttpUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class GlobalFilter implements Filter {

	private static Log log = LogFactory.getLog(GlobalFilter.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * Filter server cookie
	 */
	public void doFilter(ServletRequest sRequest, ServletResponse sResponse, FilterChain chain)
			throws ServletException, IOException {

		if (globalConfig.isGenerateAppCookie()) {
			if (log.isDebugEnabled()) {
				HttpServletRequest request = (HttpServletRequest) sRequest;
				Cookie[] reqCookies = request.getCookies();

				StringBuilder message = new StringBuilder();
				message.append(request.getRequestURI());
				if (reqCookies != null && reqCookies.length > 0) {
					for (int i = 0; i < reqCookies.length; ++i) {
						Cookie cookie = reqCookies[i];
						message.append(":").append(cookie.getName()).append("=").append(cookie.getValue());
					}
				}
				message.append("::").append(request.getRequestedSessionId());
				log.debug(message.toString());
			}

			HttpServletResponse response = (HttpServletResponse) sResponse;
			String cookieName = globalConfig.getCookieName();
			InetAddress host = null;
			String ip = null;
			String cookieValue = null;
			try {
				host = InetAddress.getLocalHost();
				ip = host.getHostAddress();
				Map cookieValueMap = globalConfig.getCookieValueMap();
				if (cookieValueMap != null) {
					cookieValue = (String) cookieValueMap.get(ip);
				}
				if ((cookieName == null) || (cookieName.trim().equals("")) || (cookieValue == null)
						|| (cookieValue.trim().equals(""))) {
					throw new Exception("No cookie value");
				}
			} catch (UnknownHostException uhe) {
				log.error("Error in doFilter - server name could not be obtained to get the cookie value - ["
						+ uhe.getMessage() + "]", uhe);
				return;
			} catch (Exception e) {
				log.error("Error in doFilter - [" + e.getMessage() + "]", e);
				return;
			}

			Cookie cookie = new Cookie(cookieName, cookieValue);
			response.addCookie(cookie);
		}

		if (globalConfig.isControlXBECachingHeaders()) {
			String requestURI = ((HttpServletRequest) sRequest).getRequestURI();
			if (requestURI != null) {
				try {
					Map expirationDurationsMap = globalConfig.getCacheControlProperties();
					Iterator expirationDurationsMapIt = expirationDurationsMap.keySet().iterator();
					String path;
					while (expirationDurationsMapIt.hasNext()) {
						path = (String) expirationDurationsMapIt.next();
						if (requestURI.indexOf(path) != -1) {
							((HttpServletResponse) sResponse).addHeader("Cache-Control",
									"max-age=" + expirationDurationsMap.get(path));
							break;
						}
					}
				} catch (Exception e) {
					log.error("Error in doFilter - [" + e.getMessage() + "]", e);
					return;
				}
			}
		}

		if (globalConfig.isControlXBESecureHeaders()) {
			((HttpServletResponse) sResponse).addHeader("Cache-Control", "1");
			((HttpServletResponse) sResponse).addHeader("X-Content-Type-Options", "nosniff");
			if (globalConfig.isSecureHeadersIframeDeny()) {
				((HttpServletResponse) sResponse).addHeader("X-Frame-Options", "deny");
			} else if (globalConfig.isSecureHeadersIframeSameOrigin()) {
				((HttpServletResponse) sResponse).addHeader("X-Frame-Options", "sameorigin");
			}
		}

		if (globalConfig.isLogXBEAccessInfo()) {
			try {
				HttpServletRequest httpReq = (HttpServletRequest) sRequest;
				String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());
				if (isLogAccessIncluded(PlatformUtiltiies.nullHandler(requestURI))) {
					String timestamp = CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date());
					UserPrincipal userPrincipal = (UserPrincipal) httpReq.getUserPrincipal();

					String userId = null;
					if (userPrincipal != null) {
						userId = userPrincipal.getUserId();
					}

					log.info("AccessInfo" + "|" + timestamp + "|" + httpReq.getRequestedSessionId() + "|" + userId + "|"
							+ requestURI + "|"
							+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "X-Forwarded-For")) + "|"
							+ httpReq.getRemoteAddr() + "|["
							+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "User-Agent")) + "]" + "|"
							+ getNullSafeValue(HttpUtil.getHTTPHeaderValue(httpReq, "Referer"))
							+ ((globalConfig.isLogXBEReqParams()) ? getAllRequestParams(httpReq) : ""));
				}
			} catch (Exception ex) {
				log.error("Error in logging access info", ex);
			}
		}

		chain.doFilter(sRequest, sResponse);
	}

	public void init(FilterConfig filterConfig) throws ServletException {

	}

	public void destroy() {

	}

	private static String getAllRequestParams(HttpServletRequest httpReq) {
		StringBuilder sb = new StringBuilder("|<REQPARAMS>");

		Enumeration pnames = httpReq.getParameterNames();
		String pvalues[];
		String pname;
		while (pnames.hasMoreElements()) {
			pname = (String) pnames.nextElement();
			if (!isExcludeParameter(pname)) {
				pvalues = httpReq.getParameterValues(pname);
				sb.append('|').append(pname);
				sb.append('|');
				for (int i = 0; i < pvalues.length; i++) {
					if (i > 0)
						sb.append(',');
					sb.append(pvalues[i]);
				}
			}
		}
		sb.append("|</REQPARAMS>");
		return sb.toString();
	}

	private String getNullSafeValue(String value) {
		return StringUtils.trimToEmpty(value);
	}

	private boolean isLogAccessIncluded(String requestURI) {
		boolean includes = false;
		if (globalConfig.getLogAccessInfoIncludes() != null) {
			for (String include : globalConfig.getLogAccessInfoIncludes()) {
				if (requestURI.indexOf(include) > -1) {
					includes = true;
					break;
				}
			}
		}
		return includes;
	}

	private static boolean isExcludeParameter(String variableName) {
		boolean isExclude = false;
		String[] excludeParms = { "creditInfo.cardNo", "creditInfo.expiryDate", "creditInfo.cardCVV",
				"creditInfo.cardHoldersName", "pwdOld", "pwdNew" };

		for (String param : excludeParms) {
			if (param.equals(variableName)) {
				isExclude = true;
				break;
			}
		}

		return isExclude;
	}
}
