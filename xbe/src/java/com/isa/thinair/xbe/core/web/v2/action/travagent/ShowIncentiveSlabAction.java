package com.isa.thinair.xbe.core.web.v2.action.travagent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.DynamicRowDecorator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowIncentiveSlabAction {

	private static Log log = LogFactory.getLog(ShowIncentiveSlabAction.class);
	private static final int PAGE_LENGTH = 20;
	private Collection<Map<String, Object>> rowsSlb;
	private int pageSlb;
	private int totalSlb;
	private int recordsSlb;
	private String succesMsg;
	private String msgType;
	private String hdnMode;
	private final String MMDDYYYY = "MM/dd/yyyy";
	private String searchSchemeID;

	public String execute() {
		return "";
	}

	public String searchIncSlabs() {
		if (this.pageSlb <= 0) {
			this.pageSlb = 1;
		}
		int startIndex = (pageSlb - 1) * PAGE_LENGTH;

		Page pageObj = null;
		try {
			if (this.getSearchSchemeID() != null && !"".equals(this.getSearchSchemeID())) {
				pageObj = ModuleServiceLocator.getTravelAgentBD().getAgentIncentiveSlabs(new Integer(this.getSearchSchemeID()),
						startIndex, PAGE_LENGTH);
				this.recordsSlb = pageObj.getTotalNoOfRecords();
				this.totalSlb = pageObj.getTotalNoOfRecords() / 20;
				int mod = pageObj.getTotalNoOfRecords() % 20;
				if (mod > 0)
					this.totalSlb = this.totalSlb + 1;
				if (this.pageSlb <= 0)
					this.pageSlb = 1;

				rowsSlb = (Collection<Map<String, Object>>) DataUtil.createDynamicGridDataDecorateOnly(
						pageObj.getStartPosition(), "incentive", pageObj.getPageData(), this.decorateIncentiveRow());
				succesMsg = "";
			}
		} catch (ModuleException me) {
			succesMsg = XBEConfig.getServerMessage(me.getExceptionCode(), null, null);
			msgType = WebConstants.MSG_ERROR;
		}

		return S2Constants.Result.SUCCESS;
	}

	/* ********************************************* */
	/* 												 */
	/* ROW DECORATERS */
	/* 												 */
	/* ********************************************* */
	private DynamicRowDecorator<AgentIncentiveSlab> decorateIncentiveRow() {
		DynamicRowDecorator<AgentIncentiveSlab> incentiveSlabDecorator = new DynamicRowDecorator<AgentIncentiveSlab>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, AgentIncentiveSlab record, int i) {
				row.put("incentive.version", record.getVersion());
				int slbNo = i + 1;
				String lable = "SLAB " + slbNo;
				row.put("incentive.slabLable", lable);

			}
		};
		return incentiveSlabDecorator;
	}

	public String convertDateToString(Date date) {
		DateFormat formatter = new SimpleDateFormat(MMDDYYYY);
		String strDate = formatter.format(date);
		return strDate;

	}

	/* ********************************************* */
	/* 												 */
	/* Getters and Setters */
	/* 												 */
	/* ********************************************* */

	public Collection<Map<String, Object>> getRowsSlb() {
		return rowsSlb;
	}

	public void setRowsSlb(Collection<Map<String, Object>> rowsSlb) {
		this.rowsSlb = rowsSlb;
	}

	public int getPageSlb() {
		return pageSlb;
	}

	public void setPageSlb(int pageSlb) {
		this.pageSlb = pageSlb;
	}

	public int getTotalSlb() {
		return totalSlb;
	}

	public void setTotalSlb(int totalSlb) {
		this.totalSlb = totalSlb;
	}

	public int getRecordsSlb() {
		return recordsSlb;
	}

	public void setRecordsSlb(int recordsSlb) {
		this.recordsSlb = recordsSlb;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getSearchSchemeID() {
		return searchSchemeID;
	}

	public void setSearchSchemeID(String searchSchemeID) {
		this.searchSchemeID = searchSchemeID;
	}

}
