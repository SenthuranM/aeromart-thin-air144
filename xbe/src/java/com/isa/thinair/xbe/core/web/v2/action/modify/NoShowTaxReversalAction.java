package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PaxAccountUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class NoShowTaxReversalAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(NoShowTaxReversalAction.class);

	private boolean success = true;
	private String messageTxt;
	private String groupPNR;
	private String pnr;
	private String pnrPaxIds;
	private String version;

	public String execute() {
		log.info("Inside NoShowTaxReversalAction");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			if (!StringUtil.isNullOrEmpty(pnrPaxIds)) {
				String[] pnrPaxAr = pnrPaxIds.split(",");

				List<String> pnrPaxIds = Arrays.asList(pnrPaxAr);

				if (!StringUtil.isNullOrEmpty(pnr)) {				

					boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);

					List<LCCClientChargeReverse> chargeReverseList = getChargeReverseList(pnrPaxIds);

					TrackInfoDTO trackInfoDTO = this.getTrackInfo();
					ModuleServiceLocator.getAirproxySegmentBD().noShowTaxChargesReversal(pnr, isGroupPNR, null, version,
							chargeReverseList, getClientInfoDTO(), trackInfoDTO);

				}

			}

		} catch (Exception e) {

			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}
		log.info("Exit NoShowTaxReversalAction");
		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPnrPaxIds() {
		return pnrPaxIds;
	}

	public void setPnrPaxIds(String pnrPaxIds) {
		this.pnrPaxIds = pnrPaxIds;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	private List<LCCClientChargeReverse> getChargeReverseList(List<String> pnrPaxIds) throws Exception {

		List<LCCClientChargeReverse> chargeReverseList = new ArrayList<>();
		List<LCCClientCarrierOndGroup> colONDs = ReservationUtil.transformJsonONDs(request.getParameter("resONDs"));
		
		//FIXME :send only valid pax segments noShowSegmentsByPax
		Map<String, Collection<String>> noShowSegmentsByPax = paxWiseNoShowSegments();
		if (colONDs != null && !colONDs.isEmpty()) {		
			
			for (LCCClientCarrierOndGroup carrierONDGroup : colONDs) {

				List<String> carrierPnrPaxIds = new ArrayList<>();
				for (String passenger : pnrPaxIds) {
					if (PaxAccountUtil.isPaxOfCarrier(passenger, carrierONDGroup.getCarrierCode())) {
						carrierPnrPaxIds.add(passenger);
					}
				}

				if (!carrierPnrPaxIds.isEmpty()) {
					LCCClientChargeReverse chargeReverse = new LCCClientChargeReverse();
					chargeReverse.setCarrierCode(carrierONDGroup.getCarrierCode());
					chargeReverse.setCarrierOndGroupRPH(carrierONDGroup.getCarrierOndGroupRPH());
					chargeReverse.setSegmentCode(carrierONDGroup.getSegmentCode());
					chargeReverse.setTravelerRefNumbersList(carrierPnrPaxIds);
					chargeReverse.setUserNote("NoShow Tax Reverse");

					chargeReverseList.add(chargeReverse);
				}

			}
		}

		return chargeReverseList;
	}

	private Map<String, Collection<String>> paxWiseNoShowSegments() throws Exception {
		Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(request.getParameter("respaxs"));

		Map<String, Collection<String>> noShowSegmentsByPax = new HashMap<>();
		if (colpaxs != null && !colpaxs.isEmpty()) {
			for (LCCClientReservationPax reservationPax : colpaxs) {
				Collection<LccClientPassengerEticketInfoTO> eTickets = reservationPax.geteTickets();
				Collection<String> noShowSegmentList = new ArrayList<>();
				if (eTickets != null && !eTickets.isEmpty()) {
					for (LccClientPassengerEticketInfoTO eTicket : eTickets) {
						if (ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE.equals(eTicket.getPaxStatus())) {
							noShowSegmentList.add(eTicket.getPnrSegId());
						}
					}
				}

				noShowSegmentsByPax.put(reservationPax.getTravelerRefNumber(), noShowSegmentList);

			}
		}

		return noShowSegmentsByPax;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

}
