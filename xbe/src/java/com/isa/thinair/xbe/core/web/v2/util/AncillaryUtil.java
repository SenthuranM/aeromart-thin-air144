package com.isa.thinair.xbe.core.web.v2.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class AncillaryUtil {

	public static List<FlightSegmentTO> extractFlightSegmentTOListFromJSonObject(String jSonString, String jsonFlightInfo)
			throws ParseException {
		List<FlightSegmentTO> jsonList = new ArrayList<FlightSegmentTO>();

		if (jSonString != null && !jSonString.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jSonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();

				String flightRefNumber = BeanUtils.nullHandler(jObject.get("flightRPH"));

				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
				flightSegmentTO.setSegmentCode(flightRefNumber.split("\\$")[1]);
				flightSegmentTO.setFlightRefNumber(flightRefNumber);

				jsonList.add(flightSegmentTO);
			}
		}
		if (jsonFlightInfo != null && !jsonFlightInfo.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jsonFlightInfo);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();

				String segCode = BeanUtils.nullHandler(jObject.get("orignNDest"));
				String flightNumber = BeanUtils.nullHandler(jObject.get("flightNo"));
				String cabinClass = BeanUtils.nullHandler(jObject.get("bkgClass"));
				for (FlightSegmentTO fst : jsonList) {
					if (fst.getSegmentCode().equalsIgnoreCase(segCode)) {
						fst.setCabinClassCode(cabinClass);
						fst.setFlightNumber(flightNumber);
						break;
					}
				}

			}
		}
		return jsonList;
	}

	public static Collection<LCCClientReservationPax> extractLccReservationPax(String jsonString) throws ParseException, java.text.ParseException {
		Collection<LCCClientReservationPax> paxList = new ArrayList<LCCClientReservationPax>();

		if (jsonString != null && !jsonString.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jsonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jPPObj = (JSONObject) iterator.next();
				LCCClientReservationPax pax = new LCCClientReservationPax();
				pax.setFirstName(BeanUtils.nullHandler(jPPObj.get("firstName")));
				pax.setLastName(BeanUtils.nullHandler(jPPObj.get("lastName")));
				pax.setPaxSequence(new Integer(BeanUtils.nullHandler((jPPObj.get("paxSequence")))));
				pax.setPaxType(BeanUtils.nullHandler(jPPObj.get("paxType")));
				pax.setTitle(BeanUtils.nullHandler(jPPObj.get("title")));

				if (jPPObj.get("selectedAncillaries") != null) {
					List<LCCSelectedSegmentAncillaryDTO> anciSegList = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
					pax.setSelectedAncillaries(anciSegList);
					JSONArray jsonAnciArray = (JSONArray) new JSONParser().parse(BeanUtils.nullHandler(jPPObj
							.get("selectedAncillaries")));
					ReservationUtil.transformAnciList(anciSegList, jsonAnciArray);
				}

				paxList.add(pax);
			}
		}

		return paxList;
	}

	public static Map<String, Integer> extractAiportServiceCountFromJsonObj(Collection<LCCClientReservationPax> colpaxs)
			throws ParseException {
		Map<String, Integer> apServiceCount = new HashMap<String, Integer>();

		for (LCCClientReservationPax lccPax : colpaxs) {
			List<LCCSelectedSegmentAncillaryDTO> selAnnciList = lccPax.getSelectedAncillaries();
			if (selAnnciList != null && selAnnciList.size() > 0) {
				for (LCCSelectedSegmentAncillaryDTO selAnci : selAnnciList) {
					String fltRef = selAnci.getFlightSegmentTO().getFlightRefNumber();

					List<LCCAirportServiceDTO> serviceList = selAnci.getAirportServiceDTOs();

					if (serviceList != null && !serviceList.isEmpty()) {
						if (apServiceCount.get(fltRef) == null) {
							apServiceCount.put(fltRef, 1);
						} else {
							int c = apServiceCount.get(fltRef);
							apServiceCount.put(fltRef, ++c);
						}
					} else {
						if (apServiceCount.get(fltRef) == null) {
							apServiceCount.put(fltRef, 0);
						}
					}
				}
			}
		}

		return apServiceCount;
	}

	public static Map<String, Integer> extractAiportServiceCountFromJsonObj(String jsonString) throws ParseException {
		Map<String, Integer> apServiceCount = new HashMap<String, Integer>();

		if (jsonString != null && !jsonString.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jsonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();
				Object anciObj = jObject.get("selectedAncillaries");

				if (anciObj != null && anciObj instanceof JSONArray) {
					JSONArray anciArr = (JSONArray) anciObj;
					Iterator<?> anciIt = anciArr.iterator();
					while (anciIt.hasNext()) {
						JSONObject innerObj = (JSONObject) anciIt.next();
						JSONObject fltObj = (JSONObject) innerObj.get("flightSegmentTO");

						String fltRef = (String) fltObj.get("flightRefNumber");

						JSONArray serviceArr = (JSONArray) innerObj.get("airportServiceDTOs");

						if (serviceArr != null && !serviceArr.isEmpty()) {
							if (apServiceCount.get(fltRef) == null) {
								apServiceCount.put(fltRef, 1);
							} else {
								int c = apServiceCount.get(fltRef);
								apServiceCount.put(fltRef, ++c);
							}
						} else {
							if (apServiceCount.get(fltRef) == null) {
								apServiceCount.put(fltRef, 0);
							}
						}

					}
				}
			}
		}

		return apServiceCount;
	}

	public static Map<String, Set<String>> getFlightReferenceWiseSelectedMeals(Collection<LCCClientReservationPax> colpaxs) {
		Map<String, Set<String>> fltRefWiseSelectedMeal = new HashMap<String, Set<String>>();

		for (LCCClientReservationPax pax : colpaxs) {
			if (pax.getSelectedAncillaries() != null) {
				for (LCCSelectedSegmentAncillaryDTO selAnci : pax.getSelectedAncillaries()) {
					if (selAnci.getMealDTOs() != null) {
						for (LCCMealDTO meal : selAnci.getMealDTOs()) {
							if (fltRefWiseSelectedMeal.get(selAnci.getFlightSegmentTO().getFlightRefNumber()) == null) {
								fltRefWiseSelectedMeal.put(selAnci.getFlightSegmentTO().getFlightRefNumber(),
										new HashSet<String>());
							}

							fltRefWiseSelectedMeal.get(selAnci.getFlightSegmentTO().getFlightRefNumber()).add(meal.getMealCode());
						}
					}
				}
			}
		}

		return fltRefWiseSelectedMeal;
	}

	public static Map<String, Set<String>> getFlightReferenceWiseSelectedSSRs(Collection<LCCClientReservationPax> colpaxs) {
		Map<String, Set<String>> fltRefWiseSelectedSSR = new HashMap<String, Set<String>>();

		for (LCCClientReservationPax pax : colpaxs) {
			if (pax.getSelectedAncillaries() != null) {
				for (LCCSelectedSegmentAncillaryDTO selAnci : pax.getSelectedAncillaries()) {
					if (selAnci.getSpecialServiceRequestDTOs() != null) {
						for (LCCSpecialServiceRequestDTO ssr : selAnci.getSpecialServiceRequestDTOs()) {
							if (fltRefWiseSelectedSSR.get(selAnci.getFlightSegmentTO().getFlightRefNumber()) == null) {
								fltRefWiseSelectedSSR.put(selAnci.getFlightSegmentTO().getFlightRefNumber(),
										new HashSet<String>());
							}

							fltRefWiseSelectedSSR.get(selAnci.getFlightSegmentTO().getFlightRefNumber()).add(ssr.getSsrCode());
						}
					}
				}
			}
		}

		return fltRefWiseSelectedSSR;
	}

}
