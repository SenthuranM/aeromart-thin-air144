package com.isa.thinair.xbe.core.web.action.reservation;

import java.io.InputStream;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.config.Action;
import org.apache.struts2.config.Result;
import org.apache.struts2.dispatcher.StreamResult;

import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.opensymphony.xwork2.ActionSupport;

@Action(name = "SampleFileDownload", namespace = S2Constants.Namespace.PRIVATE)
@Result(value = S2Constants.Result.SUCCESS, type = StreamResult.class, params = { "contentType", "application/octet-stream",
		"inputName", "fileInputStream", "contentDisposition", "attachment;filename=sample.csv" })
public class SampleFileDownloadAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private InputStream fileInputStream;

	public InputStream getFileInputStream() {
		ServletActionContext.getServletContext();
		return fileInputStream;
	}

	public String execute() throws Exception {
		fileInputStream = ServletActionContext.getServletContext().getResourceAsStream("/publicfiles/sample.csv");
		return S2Constants.Result.SUCCESS;
	}
}
