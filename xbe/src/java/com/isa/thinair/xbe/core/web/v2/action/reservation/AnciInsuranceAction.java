package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAddressDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class for Insurance
 * 
 * @author Dilan Anuruddha
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciInsuranceAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(AnciMealAction.class);

	private boolean success = true;

	private String messageTxt;

	private String selectedFlightList;

	private String insFltSegments;

	private String paxAdults;

	private String paxInfants;

	private ContactInfoTO contactInfo;

	private List<LCCInsuranceQuotationDTO> insuranceDTO;

	private String termsNCondLink;
	
	private String insContent;

	private FlightSearchDTO searchParams;

	private boolean modifyAncillary;

	private String pnr;

	private List<String> insurableFltRefNumbers;

	private boolean isRequote = false;

	private boolean allowInfantInsurance = false;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			String strTxnIdntifier = null;
			SYSTEM system = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			boolean isReturnForModify = false;

			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(selectedFlightList);
				system = fltSegBuilder.getSystem();
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				isReturnForModify = fltSegBuilder.isReturn();
			} else {
				XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
						S2Constants.Session_Data.XBE_SES_RESDATA);
				strTxnIdntifier = resInfo.getTransactionId();
				system = resInfo.getSelectedSystem();

				if (isRequote) {
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}

				if (insFltSegments != null) {
					if (isRequote) {
						flightSegmentTOs.addAll(ReservationUtil.populateFlightSegmentList(insFltSegments, searchParams));
					} else {
						FltSegBuilder fltSegBuilder = new FltSegBuilder(insFltSegments, true, false, true);
						flightSegmentTOs.addAll(fltSegBuilder.getSelectedFlightSegments());
					}

				}

			}

			ReservationProcessParams resParm = (ReservationProcessParams) request.getSession().getAttribute(
					S2Constants.Session_Data.RESER_PROCESS_PARAMS);

			if (resParm.isGroundServiceEnabled() && flightSegmentTOs.size() > 0) {
				removeGroundSegmentsForInsuranceQuote(flightSegmentTOs);
			}
			InsuranceFltSegBuilder insFltSegBldr = new InsuranceFltSegBuilder(flightSegmentTOs);
			// this.pnr = request.getParameter("pnrNo");
			LCCInsuredJourneyDTO insuredJourneyDTO = null;

			if (modifyAncillary) {
				insuredJourneyDTO = AncillaryDTOUtil.getInsuranceJourneyDetails(insFltSegBldr.getInsurableFlightSegments(),
						isReturnForModify, SalesChannelsUtil.SALES_CHANNEL_AGENT);
			} else {
				insuredJourneyDTO = AncillaryDTOUtil.getInsuranceJourneyDetails(insFltSegBldr.getInsurableFlightSegments(),
						insFltSegBldr.isReturnJourney(), SalesChannelsUtil.SALES_CHANNEL_AGENT);
			}

			List<LCCInsuredPassengerDTO> insPax = extractPaxFromJson(paxAdults, contactInfo);

			if (AppSysParamsUtil.allowAddInsurnaceForInfants()) {
				insPax.addAll(extractPaxFromJson(paxInfants, contactInfo));
			}

			LCCInsuredContactInfoDTO insuranceContactInfo = getLCCInsuranceContactInfo(contactInfo);
			insuranceDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getInsuranceQuotation(
					insFltSegBldr.getInsurableFlightSegments(), insPax, insuredJourneyDTO, strTxnIdntifier, system, pnr,
					getClientInfoDTO(), null, TrackInfoUtil.getBasicTrackInfo(request), ApplicationEngine.XBE,
					insuranceContactInfo);

			if (insuranceDTO != null && !insuranceDTO.isEmpty()) {
				if (insuranceDTO.get(0).getInsuranceRefNumber() != null) {
					for (LCCInsuranceQuotationDTO insuranceDTObjet : insuranceDTO) {
						insuranceDTObjet.setInsuredJourney(insuredJourneyDTO);
					}
				}
				messageTxt = insuranceDTO.get(0).getReponseMessage();
			}
			insurableFltRefNumbers = insFltSegBldr.getInsurableFlightRefNumbers();
			termsNCondLink = AppSysParamsUtil.getInsurnaceLink();
			
			insContent = XBEConfig.getClientMessage("cc_insurance_content", null);
			allowInfantInsurance = AppSysParamsUtil.allowAddInsurnaceForInfants();

		} catch (Exception e) {
			log.error("INSURANCE REQ ERROR", e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
		}
		return S2Constants.Result.SUCCESS;
	}

	private void removeGroundSegmentsForInsuranceQuote(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {

		Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightServiceBD().getBusAirCarrierCodes();

		Iterator<FlightSegmentTO> flightSegIterator = flightSegmentTOs.iterator();
		while (flightSegIterator.hasNext()) {
			FlightSegmentTO flightSegmentTO = flightSegIterator.next();
			if (busAirCarrierCodes.containsKey(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()))) {
				flightSegIterator.remove();
			}
		}

	}

	/**
	 * Compile List of LCCInsuredPassengerDTO from json and contact info
	 * 
	 * @param jsonString
	 * @param contactInfo
	 * @return
	 * @throws ParseException
	 */

	private List<LCCInsuredPassengerDTO> extractPaxFromJson(String jsonString, ContactInfoTO contactInfo) throws ParseException {
		List<LCCInsuredPassengerDTO> paxList = new ArrayList<LCCInsuredPassengerDTO>();

		if (jsonString != null && !jsonString.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jsonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();

				String strBday = BeanUtils.nullHandler(jObject.get("displayAdultDOB"));
				if ((strBday == null || strBday.equals("")) && AppSysParamsUtil.allowAddInsurnaceForInfants()) {
					strBday = BeanUtils.nullHandler(jObject.get("displayInfantDOB"));
				}
				Date dateOfBirth = null;
				if (strBday.contains("/")) {
					dateOfBirth = BookingUtil.stringToDate(strBday);
				} else if (strBday.contains("-")) {
					dateOfBirth = BookingUtil._stringToDate(strBday);
				}
				String fn = BeanUtils.nullHandler(jObject.get("fn"));
				String lastName = BeanUtils.nullHandler(jObject.get("ln"));
				String title = BeanUtils.nullHandler(jObject.get("displayAdultTitle"));
				String nationalityCode = BeanUtils.nullHandler(jObject.get("displayAdultNationality"));

				NameDTO nameDto = new NameDTO();
				nameDto.setFirstName(fn);
				nameDto.setLastName(lastName);
				nameDto.setPaxTitle(title);

				LCCAddressDTO address = new LCCAddressDTO();
				address.setCellPhoneNo(contactInfo.getMobileCountry() + "-" + contactInfo.getMobileArea() + "-"
						+ contactInfo.getMobileNo());
				address.setCityName(contactInfo.getCity());
				address.setCountryName(contactInfo.getCountry());
				address.setEmailAddr(contactInfo.getEmail());
				address.setHomePhoneNo(contactInfo.getPhoneCountry() + "-" + contactInfo.getPhoneArea() + "-"
						+ contactInfo.getPhoneNo());
				address.setStateName("");

				LCCInsuredPassengerDTO pax = new LCCInsuredPassengerDTO();
				pax.setBirthDate(dateOfBirth);
				pax.setAddressDTO(address);
				pax.setEmail(contactInfo.getEmail());
				pax.setHomePhoneNumber(address.getHomePhoneNo());
				pax.setNationality(nationalityCode);
				pax.setNameDTO(nameDto);
				paxList.add(pax);
			}
		}
		return paxList;
	}

	private LCCInsuredContactInfoDTO getLCCInsuranceContactInfo(ContactInfoTO contactInfo) {
		LCCInsuredContactInfoDTO lccInsuranceContactInfo = new LCCInsuredContactInfoDTO();
		lccInsuranceContactInfo.setContactPerson(contactInfo.getFirstName() + " " + contactInfo.getLastName());
		lccInsuranceContactInfo.setAddress1(contactInfo.getAddress() + contactInfo.getStreet());
		lccInsuranceContactInfo.setAddress2(contactInfo.getCity());
		lccInsuranceContactInfo.setHomePhoneNum(contactInfo.getPhoneNo());
		lccInsuranceContactInfo.setMobilePhoneNum(contactInfo.getMobileNo());
		lccInsuranceContactInfo.setCity(contactInfo.getCity());
		lccInsuranceContactInfo.setCountry(contactInfo.getCountry());
		lccInsuranceContactInfo.setEmailAddress(contactInfo.getEmail());
		lccInsuranceContactInfo.setPrefLanguage(contactInfo.getPreferredLang());

		return lccInsuranceContactInfo;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setPaxAdults(String paxAdults) {
		this.paxAdults = paxAdults;
	}

	public List<LCCInsuranceQuotationDTO> getInsuranceDTO() {
		return insuranceDTO;
	}

	public void setInsuranceDTO(List<LCCInsuranceQuotationDTO> insuranceDTO) {
		this.insuranceDTO = insuranceDTO;
	}

	@JSON(serialize = false)
	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getTermsNCondLink() {
		return termsNCondLink;
	}

	public String getInsContent() {
		return insContent;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<String> getInsurableFltRefNumbers() {
		return insurableFltRefNumbers;
	}

	public void setInsFltSegments(String insFltSegments) {
		this.insFltSegments = insFltSegments;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public String getPaxInfants() {
		return paxInfants;
	}

	public void setPaxInfants(String paxInfants) {
		this.paxInfants = paxInfants;
	}

	public boolean isAllowInfantInsurance() {
		return allowInfantInsurance;
	}

	public void setAllowInfantInsurance(boolean allowInfantInsurance) {
		this.allowInfantInsurance = allowInfantInsurance;
	}

}
