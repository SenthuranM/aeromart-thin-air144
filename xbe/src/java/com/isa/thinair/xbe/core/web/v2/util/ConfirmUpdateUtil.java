package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesListTO;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.BigDecimalWrapper;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdateOverrideChargesTO;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdatePaxSummaryListTO;
import com.isa.thinair.xbe.api.dto.v2.PaxUsedCreditInfoTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;

public class ConfirmUpdateUtil {

	/**
	 * 
	 * @param bookingShoppingCart
	 * 
	 * @return
	 * @throws ParseException
	 */
	public static ConfirmUpdateChargesTO getChargesDetails(ReservationBalanceTO lCCClientReservationBalanceTO,
			BookingShoppingCart bookingShoppingCart) throws ParseException {
		ConfirmUpdateChargesTO confirmUpdateChargesTO = new ConfirmUpdateChargesTO();
		if (lCCClientReservationBalanceTO != null) {

			Collection<ConfirmUpdateChargesListTO> collection = new ArrayList<ConfirmUpdateChargesListTO>();
			LCCClientSegmentSummaryTO lCCClientSegmentSummaryTO = lCCClientReservationBalanceTO.getSegmentSummary();

			ConfirmUpdateChargesListTO confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("FARE");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentFareAmount()));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getNewFareAmount()));
			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("TAX + SUR");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentTaxAmount(), lCCClientSegmentSummaryTO.getCurrentSurchargeAmount())));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getNewTaxAmount(), lCCClientSegmentSummaryTO.getNewSurchargeAmount(),
					lCCClientSegmentSummaryTO.getNewExtraFeeAmount())));
			collection.add(confirmUpdateChargesListTO);

			if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
				confirmUpdateChargesListTO.setDisplayDesc("SEAT");
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentSeatAmount()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getNewSeatAmount()));
				collection.add(confirmUpdateChargesListTO);

				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
				confirmUpdateChargesListTO.setDisplayDesc("MEAL");
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentMealAmount()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getNewMealAmount()));
				collection.add(confirmUpdateChargesListTO);

				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
				confirmUpdateChargesListTO.setDisplayDesc("BAGGAGE");
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentBaggageAmount()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getNewBaggageAmount()));
				collection.add(confirmUpdateChargesListTO);

				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
				confirmUpdateChargesListTO.setDisplayDesc("INSURANCE");
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentInsuranceAmount()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getNewInsuranceAmount()));
				collection.add(confirmUpdateChargesListTO);

				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
				confirmUpdateChargesListTO.setDisplayDesc("SSR");
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentSSRAmount()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getNewSSRAmount()));
				collection.add(confirmUpdateChargesListTO);
			}

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("CNX Charges");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentCnxAmount()));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getNewCnxAmount()));
			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("MOD Charges");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentModAmount()));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getNewModAmount()));
			collection.add(confirmUpdateChargesListTO);
			BigDecimal modificationPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (lCCClientSegmentSummaryTO.getModificationPenalty() != null) {
				confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
				confirmUpdateChargesListTO.setDisplayDesc("Penalties");
				// confirmUpdateChargesListTO.setDisplayOldCharges("0.00");
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentModificationPenatly()));
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getModificationPenalty()));
				modificationPenalty = lCCClientSegmentSummaryTO.getModificationPenalty();
				collection.add(confirmUpdateChargesListTO);
			}

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("Adjustments");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentAdjAmount()));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getNewAdjAmount()));
			collection.add(confirmUpdateChargesListTO);

			// Total Current + New Charges
			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("Total Charges");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getCurrentFareAmount(), lCCClientSegmentSummaryTO.getCurrentTaxAmount(),
					lCCClientSegmentSummaryTO.getCurrentSurchargeAmount(), lCCClientSegmentSummaryTO.getCurrentCnxAmount(),
					lCCClientSegmentSummaryTO.getCurrentModAmount(), lCCClientSegmentSummaryTO.getCurrentAdjAmount(),
					lCCClientSegmentSummaryTO.getCurrentModificationPenatly(),
					lCCClientSegmentSummaryTO.getCurrentBaggageAmount(), lCCClientSegmentSummaryTO.getCurrentInsuranceAmount(),
					lCCClientSegmentSummaryTO.getCurrentMealAmount(), lCCClientSegmentSummaryTO.getCurrentSeatAmount(),
					lCCClientSegmentSummaryTO.getCurrentSSRAmount())));
			confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					lCCClientSegmentSummaryTO.getNewFareAmount(), lCCClientSegmentSummaryTO.getNewTaxAmount(),
					lCCClientSegmentSummaryTO.getNewSurchargeAmount(), lCCClientSegmentSummaryTO.getNewCnxAmount(),
					lCCClientSegmentSummaryTO.getNewModAmount(), lCCClientSegmentSummaryTO.getNewAdjAmount(),
					modificationPenalty, lCCClientSegmentSummaryTO.getNewBaggageAmount(),
					lCCClientSegmentSummaryTO.getNewInsuranceAmount(), lCCClientSegmentSummaryTO.getNewMealAmount(),
					lCCClientSegmentSummaryTO.getNewSeatAmount(), lCCClientSegmentSummaryTO.getNewSSRAmount(),
					lCCClientSegmentSummaryTO.getNewExtraFeeAmount())));
			collection.add(confirmUpdateChargesListTO);

			// total discount portion
			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("Total Discounts");
			if (lCCClientSegmentSummaryTO.getCurrentDiscount() != null) {
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getCurrentDiscount().abs()));
			} else {
				confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
						.getDefaultBigDecimalZero()));
			}

			if (lCCClientSegmentSummaryTO.getNewDiscount() != null) {
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
						.getNewDiscount().abs()));
			} else {
				confirmUpdateChargesListTO.setDisplayNewCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
						.getDefaultBigDecimalZero()));
			}

			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("Non Refundable Amount");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentNonRefunds()));
			confirmUpdateChargesListTO.setDisplayNewCharges(new BigDecimalWrapper().getDisplayValue());
			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesListTO = new ConfirmUpdateChargesListTO();
			confirmUpdateChargesListTO.setDisplayDesc("Refundable Amount");
			confirmUpdateChargesListTO.setDisplayOldCharges(AccelAeroCalculator.formatAsDecimal(lCCClientSegmentSummaryTO
					.getCurrentRefunds()));
			confirmUpdateChargesListTO.setDisplayNewCharges(new BigDecimalWrapper().getDisplayValue());
			collection.add(confirmUpdateChargesListTO);

			confirmUpdateChargesTO.setDisplayCredit(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO
					.getTotalCreditAmount()));
			confirmUpdateChargesTO.setDisplayDue(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO
					.getTotalAmountDue()));

			confirmUpdateChargesTO.setChargesList(collection);
		}

		return confirmUpdateChargesTO;
	}

	/**
	 * TODO need to modify this to handle cnx/ mod changes seperately
	 * @param isInfantPaymentSeparated TODO
	 * @param collection2
	 * @param bookingShoppingCart
	 * 
	 * @return
	 * @throws ParseException
	 */
	public static Collection<ConfirmUpdatePaxSummaryListTO> getPaxSummaryList(ReservationBalanceTO reservationBalanceTO,
			Collection<NameDTO> updatedPaxNames, boolean isInfantPaymentSeparated) throws ParseException {
		Collection<ConfirmUpdatePaxSummaryListTO> collection = new ArrayList<ConfirmUpdatePaxSummaryListTO>();
		List<LCCClientPassengerSummaryTO> colPassengerSummary = new ArrayList<LCCClientPassengerSummaryTO>(
				reservationBalanceTO.getPassengerSummaryList());
		ConfirmUpdatePaxSummaryListTO confirmUpdatePaxSummaryListTO = null;

		LCCClientSegmentSummaryTO lccClientSegmentSummaryTO = null;
		LCCClientSegmentSummaryTO lccinfSegmentSummaryTO = null;
		Collections.sort(colPassengerSummary);

		if (updatedPaxNames != null) {
			updatePaxNamesWhenNameChange(colPassengerSummary, updatedPaxNames);
		}

		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
			BigDecimal infantcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantNewcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();

			// Removing infant check code as ConfirmUpdateScreen is being used for Remove PAX as well.
			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT) || isInfantPaymentSeparated) {
				lccClientSegmentSummaryTO = paxSummaryTo.getSegmentSummary();
				confirmUpdatePaxSummaryListTO = new ConfirmUpdatePaxSummaryListTO();
				confirmUpdatePaxSummaryListTO.setDisplayPaxName(paxSummaryTo.getPaxName());
				if (paxSummaryTo.hasInfant() && !isInfantPaymentSeparated) {
					confirmUpdatePaxSummaryListTO
							.setDisplayPaxName(paxSummaryTo.getPaxName() + " [ " + paxSummaryTo.getInfantName() + " ]");
					// add infant charges
					LCCClientPassengerSummaryTO infummaryTo = null;
					if (paxSummaryTo.getAccompaniedTravellerRef() != null
							&& !"".equals(paxSummaryTo.getAccompaniedTravellerRef())) {
						infummaryTo = reservationBalanceTO.getPassengerSummary(paxSummaryTo.getAccompaniedTravellerRef());

					}

					if (infummaryTo == null) {
						for (LCCClientPassengerSummaryTO paxSumTo : colPassengerSummary) {
							if (paxSumTo.getPaxType().equals(PaxTypeTO.INFANT)) {

								if (paxSummaryTo.getAccompaniedTravellerRef() != null
										&& !"".equals(paxSummaryTo.getAccompaniedTravellerRef())
										&& paxSummaryTo.getAccompaniedTravellerRef().equals(paxSumTo)) {
									infummaryTo = paxSumTo;
									break;
								}

								String paxName = paxSumTo.getPaxName();
								if (paxName.trim().equals(paxSummaryTo.getInfantName().trim())) {
									infummaryTo = paxSumTo;
									break;
								}
							}

						}
					}
					lccinfSegmentSummaryTO = infummaryTo.getSegmentSummary();
					infantcharge = lccinfSegmentSummaryTO.getCurrentTotalPrice();
					infantRefunds = lccinfSegmentSummaryTO.getCurrentRefunds();
					infantNewcharge = AccelAeroCalculator.add(lccinfSegmentSummaryTO.getNewAdjAmount(),
							lccinfSegmentSummaryTO.getNewFareAmount(), lccinfSegmentSummaryTO.getNewSurchargeAmount(),
							lccinfSegmentSummaryTO.getNewTaxAmount(), lccinfSegmentSummaryTO.getNewCnxAmount(),
							lccinfSegmentSummaryTO.getNewModAmount(), lccinfSegmentSummaryTO.getNewExtraFeeAmount());
					if (lccinfSegmentSummaryTO.getModificationPenalty() != null) {
						infantNewcharge = AccelAeroCalculator.add(lccinfSegmentSummaryTO.getModificationPenalty(),
								infantNewcharge);
					}

				} else {
					confirmUpdatePaxSummaryListTO.setDisplayPaxName(paxSummaryTo.getPaxName());
				}

				BigDecimal currentPaidAmount = BigDecimal.ZERO;
				currentPaidAmount = paxSummaryTo.getTotalPaidAmount();
				BigDecimal excludedSegAmount = BigDecimal.ZERO;
				excludedSegAmount = paxSummaryTo.getTotalExcludedSegAmount();
				BigDecimal paxCarriedFWD = BigDecimal.ZERO;
				paxCarriedFWD = AccelAeroCalculator.add(lccClientSegmentSummaryTO.getCurrentRefunds(), excludedSegAmount,
						infantRefunds);
				if (paxCarriedFWD.compareTo(currentPaidAmount) > 0) {
					paxCarriedFWD = AccelAeroCalculator.add(currentPaidAmount,
							lccClientSegmentSummaryTO.getCurrentNonRefunds().negate());
				}

				confirmUpdatePaxSummaryListTO
						.setDisplayTotalChargesCurrent(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
								.add(lccClientSegmentSummaryTO.getCurrentTotalPrice(), excludedSegAmount, infantcharge)));
				BigDecimal modificationPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (lccClientSegmentSummaryTO.getModificationPenalty() != null) {
					modificationPenalty = lccClientSegmentSummaryTO.getModificationPenalty();
				}
				confirmUpdatePaxSummaryListTO.setDisplayTotalChargesNew(AccelAeroCalculator.formatAsDecimal(
						AccelAeroCalculator.scaleValueDefault(AccelAeroCalculator.add(lccClientSegmentSummaryTO.getNewAdjAmount(),
								lccClientSegmentSummaryTO.getNewCnxAmount(), lccClientSegmentSummaryTO.getNewFareAmount(),
								lccClientSegmentSummaryTO.getNewModAmount(), lccClientSegmentSummaryTO.getNewSurchargeAmount(),
								lccClientSegmentSummaryTO.getCurrentNonRefunds(), lccClientSegmentSummaryTO.getNewTaxAmount(),
								infantNewcharge, modificationPenalty, lccClientSegmentSummaryTO.getNewDiscount(),
								excludedSegAmount, lccClientSegmentSummaryTO.getNewExtraFeeAmount()))));
				confirmUpdatePaxSummaryListTO
						.setDisplayTotalPaymentsCurrent(AccelAeroCalculator.formatAsDecimal(currentPaidAmount));
				confirmUpdatePaxSummaryListTO.setDisplayTotalCFNew(AccelAeroCalculator.formatAsDecimal(paxCarriedFWD));
				BigDecimal balance = paxSummaryTo.getBalance();
				confirmUpdatePaxSummaryListTO.setDisplayBalance(AccelAeroCalculator.formatAsDecimal(balance));
				collection.add(confirmUpdatePaxSummaryListTO);
			}
		}
		return collection;
	}

	private static void updatePaxNamesWhenNameChange(List<LCCClientPassengerSummaryTO> colPassengerSummary,
			Collection<NameDTO> updatedPaxNames) {
		for (NameDTO nameDto : updatedPaxNames) {
			for (LCCClientPassengerSummaryTO lccClientPassengerSummaryTO : colPassengerSummary) {
				if (nameDto.getPaxReference().equals(lccClientPassengerSummaryTO.getTravelerRefNumber())) {
					lccClientPassengerSummaryTO.setPaxFirstName(nameDto.getFirstname());
					lccClientPassengerSummaryTO.setPaxLastName(nameDto.getLastName());
					lccClientPassengerSummaryTO.setPaxName(ReservationApiUtils.getPassengerName("", nameDto.getTitle(),
							nameDto.getFirstname(), nameDto.getLastName(), false));
					break;
				} else if (nameDto.getPaxReference().equals(lccClientPassengerSummaryTO.getAccompaniedTravellerRef())) {
					lccClientPassengerSummaryTO.setInfantName(ReservationApiUtils.getPassengerName("", nameDto.getTitle(),
							nameDto.getFirstname(), nameDto.getLastName(), false));
				}
			}
		}

	}

	/**
	 * TODO need to modify this to handle cnx/ mod changes seperately
	 * 
	 * @param lCCClientReservationBalanceTO
	 * @param lCCClientResAlterQueryModesTO
	 * @param isModify
	 * @return
	 * @throws ParseException
	 */
	public static Collection<ConfirmUpdateOverrideChargesTO>
			getOverrideList(ReservationBalanceTO lCCClientReservationBalanceTO,
					LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO, boolean isModify,
					boolean rpPramsblnShowChargeTypeOveride) throws ParseException {
		List<ConfirmUpdateOverrideChargesTO> collection = new ArrayList<ConfirmUpdateOverrideChargesTO>();
		Collection<LCCClientPassengerSummaryTO> colPassengerSummary = lCCClientReservationBalanceTO.getPassengerSummaryList();
		ConfirmUpdateOverrideChargesTO confirmUpdatePaxSummaryListTO = new ConfirmUpdateOverrideChargesTO();
		boolean blnAdultFilled = false;
		boolean blnChildFilled = false;
		boolean blnInfantFilled = false;
		Boolean blnShowChargeTypeOveride = Boolean.FALSE;
		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
			blnShowChargeTypeOveride = new Boolean(Boolean.TRUE);// Charge type override is shown in all cases.

			if (skipPaxSummaryRecord(colPassengerSummary, paxSummaryTo)) {
				continue;
			}

			if (paxSummaryTo.getPaxType().equals(PaxTypeTO.ADULT) && !blnAdultFilled) {
				confirmUpdatePaxSummaryListTO = new ConfirmUpdateOverrideChargesTO();
				if (isModify) {
					confirmUpdatePaxSummaryListTO.setDescription("Adult MOD Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalModCharge()));
					for (LCCClientPassengerSummaryTO infSummaryTo : colPassengerSummary) {
						if (infSummaryTo.getPaxType().equals(PaxTypeTO.INFANT) && infSummaryTo.getTotalModCharge() != null
								&& paxSummaryTo.getTotalModCharge().doubleValue() > 0) {
							BigDecimal adultCharge = AccelAeroCalculator.subtract(paxSummaryTo.getTotalModCharge(),
									infSummaryTo.getTotalModCharge());
							confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(adultCharge));
							break;
						}
					}

					PnrChargeDetailTO modChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTo.getModChargeDetailTO() != null) {
						modChargeDetailTO = paxSummaryTo.getModChargeDetailTO();
					} else if (lCCClientResAlterQueryModesTO.getAdultCustomChargeTO() != null) {
						// For lcc charge override. Getting the values form the front end.
						modChargeDetailTO = lCCClientResAlterQueryModesTO.getAdultCustomChargeTO();
					}
					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							modChargeDetailTO.getModificationChargeType(), modChargeDetailTO.getMaximumModificationAmount(),
							modChargeDetailTO.getMinModificationAmount(), modChargeDetailTO.getChargePercentage());

				} else {
					confirmUpdatePaxSummaryListTO.setDescription("Adult CNX Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalCnxCharge()));

					String infTraveRef = PaxTypeUtils.getAssociatedInfTravRef(colPassengerSummary,
							paxSummaryTo.getTravelerRefNumber());

					if (infTraveRef != null) {
						for (LCCClientPassengerSummaryTO infSummaryTo : colPassengerSummary) {
							if (infSummaryTo.getPaxType().equals(PaxTypeTO.INFANT) && infSummaryTo.getTotalCnxCharge() != null
									&& paxSummaryTo.getTotalCnxCharge().doubleValue() > 0
									&& (infTraveRef.equals(infSummaryTo.getTravelerRefNumber()))) {
								BigDecimal adultCharge = AccelAeroCalculator.subtract(paxSummaryTo.getTotalCnxCharge(),
										infSummaryTo.getTotalCnxCharge());
								confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(adultCharge));
								break;
							}
						}
					}

					PnrChargeDetailTO cnxChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTo.getCnxChargeDetailTO() != null) {
						cnxChargeDetailTO = paxSummaryTo.getCnxChargeDetailTO();
					} else if (lCCClientResAlterQueryModesTO.getAdultCustomChargeTO() != null) {
						cnxChargeDetailTO = lCCClientResAlterQueryModesTO.getAdultCustomChargeTO();
					}
					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							cnxChargeDetailTO.getCancellationChargeType(), cnxChargeDetailTO.getMaximumCancellationAmount(),
							cnxChargeDetailTO.getMinCancellationAmount(), cnxChargeDetailTO.getChargePercentage());

				}
				confirmUpdatePaxSummaryListTO.setPaxType(PaxTypeTO.ADULT);
				confirmUpdatePaxSummaryListTO.setId("txtCNXAdt");
				confirmUpdatePaxSummaryListTO.setType("textBox"); // This will use to generate the html control
				confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(blnShowChargeTypeOveride);
				if (!rpPramsblnShowChargeTypeOveride) {
					confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(false);
				}
				collection.add(confirmUpdatePaxSummaryListTO);
				blnAdultFilled = true;
			}
			if (paxSummaryTo.getPaxType().equals(PaxTypeTO.CHILD) && !blnChildFilled) {
				confirmUpdatePaxSummaryListTO = new ConfirmUpdateOverrideChargesTO();

				if (isModify) {
					confirmUpdatePaxSummaryListTO.setDescription("Child MOD Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalModCharge()));

					PnrChargeDetailTO modChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTo.getModChargeDetailTO() != null) {
						modChargeDetailTO = paxSummaryTo.getModChargeDetailTO();
					} else if (lCCClientResAlterQueryModesTO.getChildCustomChargeTO() != null) {
						modChargeDetailTO = lCCClientResAlterQueryModesTO.getChildCustomChargeTO();
					}
					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							modChargeDetailTO.getModificationChargeType(), modChargeDetailTO.getMaximumModificationAmount(),
							modChargeDetailTO.getMinModificationAmount(), modChargeDetailTO.getChargePercentage());

				} else {
					confirmUpdatePaxSummaryListTO.setDescription("Child CNX Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalCnxCharge()));

					PnrChargeDetailTO cnxChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTo.getCnxChargeDetailTO() != null) {
						cnxChargeDetailTO = paxSummaryTo.getCnxChargeDetailTO();
					} else if (lCCClientResAlterQueryModesTO.getChildCustomChargeTO() != null) {
						cnxChargeDetailTO = lCCClientResAlterQueryModesTO.getChildCustomChargeTO();
					}
					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							cnxChargeDetailTO.getCancellationChargeType(), cnxChargeDetailTO.getMaximumCancellationAmount(),
							cnxChargeDetailTO.getMinCancellationAmount(), cnxChargeDetailTO.getChargePercentage());
				}
				confirmUpdatePaxSummaryListTO.setPaxType(PaxTypeTO.CHILD);
				confirmUpdatePaxSummaryListTO.setId("txtCNXChld");
				confirmUpdatePaxSummaryListTO.setType("textBox"); // This will use to generate the html control
				confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(blnShowChargeTypeOveride);
				if (!rpPramsblnShowChargeTypeOveride) {
					confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(false);
				}
				collection.add(confirmUpdatePaxSummaryListTO);
				blnChildFilled = true;
			}
			if (paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT) && !blnInfantFilled) {
				confirmUpdatePaxSummaryListTO = new ConfirmUpdateOverrideChargesTO();
				if (isModify) {
					confirmUpdatePaxSummaryListTO.setDescription("Infant MOD Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalModCharge()));
					PnrChargeDetailTO modChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTo.getModChargeDetailTO() != null) {
						modChargeDetailTO = paxSummaryTo.getModChargeDetailTO();
					} else if (lCCClientResAlterQueryModesTO.getInfantCustomChargeTO() != null) {
						modChargeDetailTO = lCCClientResAlterQueryModesTO.getInfantCustomChargeTO();
					}
					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							modChargeDetailTO.getModificationChargeType(), modChargeDetailTO.getMaximumModificationAmount(),
							modChargeDetailTO.getMinModificationAmount(), modChargeDetailTO.getChargePercentage());

				} else {
					confirmUpdatePaxSummaryListTO.setDescription("Infant CNX Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalCnxCharge()));

					PnrChargeDetailTO cnxChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTo.getCnxChargeDetailTO() != null) {
						cnxChargeDetailTO = paxSummaryTo.getCnxChargeDetailTO();
					} else if (lCCClientResAlterQueryModesTO.getInfantCustomChargeTO() != null) {
						cnxChargeDetailTO = lCCClientResAlterQueryModesTO.getInfantCustomChargeTO();
					}
					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							cnxChargeDetailTO.getCancellationChargeType(), cnxChargeDetailTO.getMaximumCancellationAmount(),
							cnxChargeDetailTO.getMinCancellationAmount(), cnxChargeDetailTO.getChargePercentage());

				}
				confirmUpdatePaxSummaryListTO.setPaxType(PaxTypeTO.INFANT);
				confirmUpdatePaxSummaryListTO.setId("txtCNXInf");
				confirmUpdatePaxSummaryListTO.setType("textBox"); // This will use to generate the html control
				confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(blnShowChargeTypeOveride);
				if (!rpPramsblnShowChargeTypeOveride) {
					confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(false);
				}
				collection.add(confirmUpdatePaxSummaryListTO);
				blnInfantFilled = true;
			}
		}
		Collections.sort(collection);
		return collection;
	}

	public static Object[] getPaxPaySummaryList(ReservationBalanceTO lCCClientReservationBalanceTOs,
			Collection<LCCClientReservationPax> setClientReservationPaxs, boolean addInfant) throws ParseException {
		Collection<PaymentPassengerTO> collection = new ArrayList<PaymentPassengerTO>();
		List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) lCCClientReservationBalanceTOs
				.getPassengerSummaryList();
		PaymentPassengerTO paymentPassengerTO = null;
		LCCClientSegmentSummaryTO lccinfSegmentSummaryTO = null;

		LCCClientSegmentSummaryTO lccClientSegmentSummaryTO = null;
		BigDecimal creditUsedAnciAmt = BigDecimal.ZERO;
		Collections.sort(colPassengerSummary);
		int intPaxCount = 0;
		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
			BigDecimal infantNewcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			Integer paxSeqNo = PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber());

			paymentPassengerTO = new PaymentPassengerTO();
			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {

				for (LCCClientReservationPax pax : setClientReservationPaxs) {
					if (PaxTypeUtils.getPaxSeq(pax.getTravelerRefNumber()).intValue() == PaxTypeUtils.getPaxSeq(
							paxSummaryTo.getTravelerRefNumber()).intValue()) {
						paymentPassengerTO.setDisplayFirstName(pax.getFirstName());
						paymentPassengerTO.setDisplayLastName(pax.getLastName());
						break;
					}
				}

				lccClientSegmentSummaryTO = paxSummaryTo.getSegmentSummary();

				if (paxSummaryTo.getInfantName() != null && !"".equals(paxSummaryTo.getInfantName())) {
					String infName = paxSummaryTo.getInfantName();
					paymentPassengerTO.setDisplayPassengerName(paxSummaryTo.getPaxName() + " [ " + paxSummaryTo.getInfantName()
							+ " ]");
					for (LCCClientPassengerSummaryTO infummaryTo : colPassengerSummary) {
						if (infummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
							String paxName = infummaryTo.getPaxName();
							if (paxName.trim().equals(infName.trim())) {
								lccinfSegmentSummaryTO = infummaryTo.getSegmentSummary();
								infantNewcharge = AccelAeroCalculator.add(lccinfSegmentSummaryTO.getNewAdjAmount(),
										lccinfSegmentSummaryTO.getNewFareAmount(),
										lccinfSegmentSummaryTO.getNewSurchargeAmount(), lccinfSegmentSummaryTO.getNewTaxAmount(),
										lccClientSegmentSummaryTO.getNewExtraFeeAmount());
								break;
							}
						}

					}
				} else {
					paymentPassengerTO.setDisplayPassengerName(paxSummaryTo.getPaxName());
				}

				paymentPassengerTO.setDisplayPaxID(Integer.toString(paxSeqNo));
				paymentPassengerTO
						.setDisplayPaid(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalPaidAmount().negate()));
				paymentPassengerTO.setDisplayOriginalPaid(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalPaidAmount()));
				if (addInfant) {
					paymentPassengerTO.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalPrice()));
				} else {
					paymentPassengerTO.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
							.scaleValueDefault(AccelAeroCalculator.add(lccClientSegmentSummaryTO.getNewAdjAmount(),
									lccClientSegmentSummaryTO.getNewCnxAmount(), lccClientSegmentSummaryTO.getNewFareAmount(),
									lccClientSegmentSummaryTO.getNewModAmount(),
									lccClientSegmentSummaryTO.getCurrentNonRefunds(),
									lccClientSegmentSummaryTO.getNewSurchargeAmount(),
									lccClientSegmentSummaryTO.getNewTaxAmount(), infantNewcharge,
									lccClientSegmentSummaryTO.getNewExtraFeeAmount()))));
				}
				paymentPassengerTO.setDisplayUsedCredit("0.00");
				paymentPassengerTO.setDisplayToPay(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalAmountDue()));
				paymentPassengerTO.setDisplayOriginalTobePaid(AccelAeroCalculator.formatAsDecimal(paxSummaryTo
						.getTotalAmountDue()));
				paymentPassengerTO.setPaxCreditInfo(new ArrayList<PaxUsedCreditInfoTO>());
				paymentPassengerTO.setDisplayTravelerRefNo(paxSummaryTo.getTravelerRefNumber());
				intPaxCount++;
				collection.add(paymentPassengerTO);
			}
		}
		Object[] arrSumObj = new Object[2];
		arrSumObj[0] = collection;
		arrSumObj[1] = creditUsedAnciAmt;
		return arrSumObj;
	}

	public static Collection<PaymentPassengerTO> getPaxPaymentSummaryList(ReservationBalanceTO reservationBalanceTO,
			Collection<NameDTO> updatedPaxNames, boolean isInfantPaymentSeparated) throws ParseException {
		Collection<PaymentPassengerTO> paymentPassengerList = new ArrayList<PaymentPassengerTO>();
		List<LCCClientPassengerSummaryTO> passegnerSummaryList = new ArrayList<LCCClientPassengerSummaryTO>(
				reservationBalanceTO.getPassengerSummaryList());
		PaymentPassengerTO paymentPassenger = null;
		// LCCClientSegmentSummaryTO infSegmentSummary = null;
		// LCCClientSegmentSummaryTO segmentSummary = null;
		// BigDecimal creditUsedAnciAmt = BigDecimal.ZERO;
		Collections.sort(passegnerSummaryList);

		if (updatedPaxNames != null) {
			updatePaxNamesWhenNameChange(passegnerSummaryList, updatedPaxNames);
		}
		// int intPaxCount = 0;
		for (LCCClientPassengerSummaryTO passengerSummary : passegnerSummaryList) {
			// BigDecimal infantNewcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantDueAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			Integer paxSeqNo = PaxTypeUtils.getPaxSeq(passengerSummary.getTravelerRefNumber());

			paymentPassenger = new PaymentPassengerTO();
			if (!passengerSummary.getPaxType().equals(PaxTypeTO.INFANT) || isInfantPaymentSeparated) {

				// for (LCCClientReservationPax pax : setClientReservationPaxs) {
				// if (PaxTypeUtils.getPaxSeq(pax.getTravelerRefNumber()).intValue() == PaxTypeUtils.getPaxSeq(
				// passengerSummary.getTravelerRefNumber()).intValue()) {
				// paymentPassengerTO.setDisplayFirstName(pax.getFirstName());
				// paymentPassengerTO.setDisplayLastName(pax.getLastName());
				// break;
				// }
				// }
				paymentPassenger.setDisplayFirstName(passengerSummary.getPaxFirstName());
				paymentPassenger.setDisplayLastName(passengerSummary.getPaxLastName());

				// segmentSummary = passengerSummary.getLccClientSegmentSummaryTO();

				if (passengerSummary.hasInfant() && !isInfantPaymentSeparated) {
					paymentPassenger.setDisplayPassengerName(passengerSummary.getPaxName() + " [ "
							+ passengerSummary.getInfantName() + " ]");
					// LCCClientPassengerSummaryTO infSummary =
					// reservationBalanceTO.getPassengerSummary(passengerSummary
					// .getAccompaniedTravellerRef());
					// infSegmentSummary = infSummary.getLccClientSegmentSummaryTO();
					// infantNewcharge = AccelAeroCalculator.add(infSegmentSummary.getNewAdjAmount(),
					// infSegmentSummary.getNewFareAmount(), infSegmentSummary.getNewSurchargeAmount(),
					// infSegmentSummary.getNewTaxAmount());
					// infantTotalPrice = infSummary.getTotalPrice();
					// infantDueAmount = infSummary.getTotalAmountDue();
				} else {
					paymentPassenger.setDisplayPassengerName(passengerSummary.getPaxName());
				}

				paymentPassenger.setDisplayPaxID(Integer.toString(paxSeqNo));
				paymentPassenger.setDisplayPaid(AccelAeroCalculator.formatAsDecimal(passengerSummary.getTotalPaidAmount()
						.negate()));
				paymentPassenger
						.setDisplayOriginalPaid(AccelAeroCalculator.formatAsDecimal(passengerSummary.getTotalPaidAmount()));
				// if (addInfant) {
				paymentPassenger.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
						passengerSummary.getTotalPrice(), infantTotalPrice)));
				// } else {
				// paymentPassenger.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
				// .scaleValueDefault(AccelAeroCalculator.add(segmentSummary.getNewAdjAmount(),
				// segmentSummary.getNewCnxAmount(), segmentSummary.getNewFareAmount(),
				// segmentSummary.getNewModAmount(), segmentSummary.getCurrentNonRefunds(),
				// segmentSummary.getNewSurchargeAmount(), segmentSummary.getNewTaxAmount(), infantNewcharge))));
				// }
				String totalDueAmount = AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
						passengerSummary.getTotalAmountDue(), infantDueAmount));
				paymentPassenger.setDisplayUsedCredit("0.00");
				paymentPassenger.setDisplayToPay(totalDueAmount);
				paymentPassenger.setDisplayOriginalTobePaid(totalDueAmount);
				paymentPassenger.setPaxCreditInfo(new ArrayList<PaxUsedCreditInfoTO>());
				paymentPassenger.setDisplayTravelerRefNo(passengerSummary.getTravelerRefNumber());
				// intPaxCount++;
				paymentPassengerList.add(paymentPassenger);
			}
		}
		return paymentPassengerList;
	}

	public static Collection<PaymentPassengerTO> getPaxPaySummaryListAddSegment(
			ReservationBalanceTO lCCClientReservationBalanceTOs, Map<Integer, ReservationPaxTO> paxMap,
			Collection<LCCClientReservationPax> setClientReservationPaxs, boolean addInfant, SYSTEM selectedSystem)
			throws ParseException {
		Collection<PaymentPassengerTO> collection = new ArrayList<PaymentPassengerTO>();
		List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) lCCClientReservationBalanceTOs
				.getPassengerSummaryList();
		PaymentPassengerTO paymentPassengerTO = null;
		LCCClientSegmentSummaryTO lccinfSegmentSummaryTO = null;

		LCCClientSegmentSummaryTO lccClientSegmentSummaryTO = null;
		Collections.sort(colPassengerSummary);
		int intPaxCount = 0;
		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
			BigDecimal infantNewcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
			Integer paxSeqNo = PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber());
			// ReservationPaxTO resPax = paxMap.get(paxSeqNo);

			paymentPassengerTO = new PaymentPassengerTO();
			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {

				for (LCCClientReservationPax pax : setClientReservationPaxs) {
					if (pax.getTravelerRefNumber().equals(paxSummaryTo.getTravelerRefNumber())) {
						paymentPassengerTO.setDisplayFirstName(pax.getFirstName());
						paymentPassengerTO.setDisplayLastName(pax.getLastName());
						break;
					}
				}

				lccClientSegmentSummaryTO = paxSummaryTo.getSegmentSummary();

				if (paxSummaryTo.getInfantName() != null && !"".equals(paxSummaryTo.getInfantName())) {
					String infName = paxSummaryTo.getInfantName();
					paymentPassengerTO.setDisplayPassengerName(paxSummaryTo.getPaxName() + " [ " + paxSummaryTo.getInfantName()
							+ " ]");
					for (LCCClientPassengerSummaryTO infummaryTo : colPassengerSummary) {
						if (infummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
							String paxName = infummaryTo.getPaxName();
							if (paxName.trim().equals(infName.trim())) {
								lccinfSegmentSummaryTO = infummaryTo.getSegmentSummary();
								infantNewcharge = AccelAeroCalculator.add(lccinfSegmentSummaryTO.getNewAdjAmount(),
										lccinfSegmentSummaryTO.getNewCnxAmount(), lccinfSegmentSummaryTO.getNewFareAmount(),
										lccinfSegmentSummaryTO.getNewModAmount(), lccinfSegmentSummaryTO.getNewSurchargeAmount(),
										lccinfSegmentSummaryTO.getNewTaxAmount(),
										lccClientSegmentSummaryTO.getNewExtraFeeAmount());
								break;
							}
						}

					}
				} else {
					paymentPassengerTO.setDisplayPassengerName(paxSummaryTo.getPaxName());
				}

				paymentPassengerTO.setDisplayPaxID(Integer.toString(intPaxCount));
				paymentPassengerTO
						.setDisplayPaid(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalPaidAmount().negate()));
				paymentPassengerTO.setDisplayOriginalPaid(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalPaidAmount()));
				if (addInfant) {
					paymentPassengerTO.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalPrice()));
				} else {
					if (selectedSystem == SYSTEM.INT) {
						paymentPassengerTO.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator
								.scaleValueDefault(AccelAeroCalculator.add(lccClientSegmentSummaryTO.getNewAdjAmount(),
										lccClientSegmentSummaryTO.getNewCnxAmount(),
										lccClientSegmentSummaryTO.getNewFareAmount(),
										lccClientSegmentSummaryTO.getNewModAmount(),
										lccClientSegmentSummaryTO.getCurrentNonRefunds(),
										lccClientSegmentSummaryTO.getNewSurchargeAmount(),
										lccClientSegmentSummaryTO.getNewTaxAmount(), infantNewcharge, /* anciTotal, */
										paxSummaryTo.getTotalPaidAmount(), lccClientSegmentSummaryTO.getNewExtraFeeAmount()))));
					} else {
						paymentPassengerTO.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(paxSummaryTo.getTotalPrice()));
					}
				}
				BigDecimal totalDue = paxSummaryTo.getTotalAmountDue();
				paymentPassengerTO.setDisplayUsedCredit("0.00");
				paymentPassengerTO.setDisplayToPay(AccelAeroCalculator.formatAsDecimal(totalDue));
				paymentPassengerTO.setDisplayOriginalTobePaid(AccelAeroCalculator.formatAsDecimal(totalDue));

				paymentPassengerTO.setPaxCreditInfo(new ArrayList<PaxUsedCreditInfoTO>());
				paymentPassengerTO.setDisplayTravelerRefNo(paxSummaryTo.getTravelerRefNumber());
				intPaxCount++;
				collection.add(paymentPassengerTO);
			}
		}
		return collection;
	}

	private static void populatePaxSummaryWithChargeOverride(ConfirmUpdateOverrideChargesTO confirmUpdatePaxSummaryListTO,
			String ChargeType, BigDecimal maximumAmount, BigDecimal minAmount, BigDecimal chargePercentage) {

		confirmUpdatePaxSummaryListTO.setChargeType(ChargeType);
		confirmUpdatePaxSummaryListTO.setMax(AccelAeroCalculator.formatAsDecimal(maximumAmount));
		confirmUpdatePaxSummaryListTO.setMin(AccelAeroCalculator.formatAsDecimal(minAmount));
		confirmUpdatePaxSummaryListTO.setPrecentageVal(AccelAeroCalculator.formatAsDecimal(chargePercentage));

	}

	/**
	 * following validation required when first PAX has a no show segment then it skip CNX charge and it consider all
	 * other remaining PAX type CNX charge as zero to overcome following validation implemented
	 * 
	 * @param colPassengerSummary
	 * @param currentPaxSummaryTO
	 * @return
	 */
	private static boolean skipPaxSummaryRecord(Collection<LCCClientPassengerSummaryTO> colPassengerSummary,
			LCCClientPassengerSummaryTO currentPaxSummaryTO) {

		if (colPassengerSummary != null && currentPaxSummaryTO != null) {
			for (LCCClientPassengerSummaryTO paxSummaryTO : colPassengerSummary) {
				if (paxSummaryTO != null) {
					if ((currentPaxSummaryTO.getTotalCnxCharge() != null && !(currentPaxSummaryTO.getTotalCnxCharge()
							.doubleValue() > 0))
							&& (currentPaxSummaryTO.getPaxType().equals(paxSummaryTO.getPaxType()))
							&& (paxSummaryTO.getTotalCnxCharge() != null && paxSummaryTO.getTotalCnxCharge().doubleValue() > 0)) {
						return true;

					}
				}

			}
		}

		return false;
	}

	public static Collection<ConfirmUpdateOverrideChargesTO> getOverrideList(ReservationBalanceTO reservationBalanceTO,
			RequoteBalanceQueryRQ balanceQueryRQ, boolean modifiableReservation, boolean rpParamsblnShowChargeTypeOveride) {
		List<ConfirmUpdateOverrideChargesTO> collection = new ArrayList<ConfirmUpdateOverrideChargesTO>();
		Collection<LCCClientPassengerSummaryTO> colPassengerSummary = reservationBalanceTO.getPassengerSummaryList();
		ConfirmUpdateOverrideChargesTO confirmUpdatePaxSummaryListTO = new ConfirmUpdateOverrideChargesTO();

		Set<String> processedPaxTypes = new HashSet<String>();
		Boolean blnShowChargeTypeOveride = Boolean.FALSE;
		for (LCCClientPassengerSummaryTO paxSummaryTO : colPassengerSummary) {
			blnShowChargeTypeOveride = new Boolean(Boolean.TRUE);// Charge type override is shown in all cases.

			if (skipPaxSummaryRecord(colPassengerSummary, paxSummaryTO)) {
				continue;
			}
			if (!processedPaxTypes.contains(paxSummaryTO.getPaxType())) {
				processedPaxTypes.add(paxSummaryTO.getPaxType());
				String pref = "";
				if (paxSummaryTO.getPaxType().equals(PaxTypeTO.ADULT)) {
					pref = "Adult";
				} else if (paxSummaryTO.getPaxType().equals(PaxTypeTO.CHILD)) {
					pref = "Child";
				} else if (paxSummaryTO.getPaxType().equals(PaxTypeTO.INFANT)) {
					pref = "Infant";
				}
				confirmUpdatePaxSummaryListTO = new ConfirmUpdateOverrideChargesTO();
				if (modifiableReservation && reservationBalanceTO.isShowModCharge()) {
					confirmUpdatePaxSummaryListTO.setDescription(pref + " MOD Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTO.getTotalModCharge()));

					PnrChargeDetailTO modChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTO.getModChargeDetailTO() != null) {
						modChargeDetailTO = paxSummaryTO.getModChargeDetailTO();
					} else if (balanceQueryRQ.hasCustomCharge(paxSummaryTO.getPaxType())) {
						// For lcc charge override. Getting the values form the front end.
						modChargeDetailTO = balanceQueryRQ.getCustomCharge(paxSummaryTO.getPaxType());
					}

					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							modChargeDetailTO.getModificationChargeType(), modChargeDetailTO.getMaximumModificationAmount(),
							modChargeDetailTO.getMinModificationAmount(), modChargeDetailTO.getChargePercentage());

				} else if (reservationBalanceTO.isShowCnxCharge()) {
					confirmUpdatePaxSummaryListTO.setDescription(pref + " CNX Charges");
					confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(paxSummaryTO.getTotalCnxCharge()));

					String infTraveRef = PaxTypeUtils.getAssociatedInfTravRef(colPassengerSummary,
							paxSummaryTO.getTravelerRefNumber());

					if (infTraveRef != null) {
						for (LCCClientPassengerSummaryTO infSummaryTo : colPassengerSummary) {
							if (infSummaryTo.getPaxType().equals(PaxTypeTO.INFANT) && infSummaryTo.getTotalCnxCharge() != null
									&& paxSummaryTO.getTotalCnxCharge().doubleValue() > 0
									&& (infTraveRef.equals(infSummaryTo.getTravelerRefNumber()))) {
								BigDecimal adultCharge = AccelAeroCalculator.subtract(paxSummaryTO.getTotalCnxCharge(),
										infSummaryTo.getTotalCnxCharge());
								confirmUpdatePaxSummaryListTO.setValue(AccelAeroCalculator.formatAsDecimal(adultCharge));
								break;
							}
						}
					}

					PnrChargeDetailTO cnxChargeDetailTO = new PnrChargeDetailTO();
					if (paxSummaryTO.getCnxChargeDetailTO() != null) {
						cnxChargeDetailTO = paxSummaryTO.getCnxChargeDetailTO();
					} else if (balanceQueryRQ.hasCustomCharge(paxSummaryTO.getPaxType())) {
						cnxChargeDetailTO = balanceQueryRQ.getCustomCharge(paxSummaryTO.getPaxType());
					}
					populatePaxSummaryWithChargeOverride(confirmUpdatePaxSummaryListTO,
							cnxChargeDetailTO.getCancellationChargeType(), cnxChargeDetailTO.getMaximumCancellationAmount(),
							cnxChargeDetailTO.getMinCancellationAmount(), cnxChargeDetailTO.getChargePercentage());

				} else {
					confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(false);
					confirmUpdatePaxSummaryListTO.setDescription("");
				}
				confirmUpdatePaxSummaryListTO.setPaxType(paxSummaryTO.getPaxType());
				// FIXME getting rid of these HTML
				if (paxSummaryTO.getPaxType().equals(PaxTypeTO.ADULT)) {
					confirmUpdatePaxSummaryListTO.setId("txtCNXAdt");
				} else if (paxSummaryTO.getPaxType().equals(PaxTypeTO.CHILD)) {
					confirmUpdatePaxSummaryListTO.setId("txtCNXChld");
				} else if (paxSummaryTO.getPaxType().equals(PaxTypeTO.INFANT)) {
					confirmUpdatePaxSummaryListTO.setId("txtCNXInf");
				}
				confirmUpdatePaxSummaryListTO.setType("textBox"); // This will use to generate the html control
				confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(blnShowChargeTypeOveride);
				if (!rpParamsblnShowChargeTypeOveride) {
					confirmUpdatePaxSummaryListTO.setBlnShowChargeTypeOveride(false);
				}
				collection.add(confirmUpdatePaxSummaryListTO);
			}
		}
		Collections.sort(collection);
		return collection;
	}
}