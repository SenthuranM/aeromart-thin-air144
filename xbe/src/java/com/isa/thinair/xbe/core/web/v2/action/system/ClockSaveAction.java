package com.isa.thinair.xbe.core.web.v2.action.system;

/**
 * Common currency exchange rates calculator
 * 
 * @author Baladewa
 * 
 */
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ClockSaveAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ClockSaveAction.class);
	private boolean success = true;
	private String messageTxt = "";
	private String dashboardPref;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (getDashboardPref() != null) {
				UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
				ModuleServiceLocator.getSecurityBD().saveDashboardPref(up.getUserId(), getDashboardPref());
			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("MEAL REQ ERROR", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	private String getDashboardPref() {
		return dashboardPref;
	}

	public void setDashboardPref(String dashboardPref) {
		this.dashboardPref = dashboardPref;
	}

}
