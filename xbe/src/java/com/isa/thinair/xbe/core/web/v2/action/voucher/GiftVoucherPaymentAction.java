package com.isa.thinair.xbe.core.web.v2.action.voucher;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.utils.GiftVoucherUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author chethiya
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class GiftVoucherPaymentAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(GiftVoucherPaymentAction.class);

	private String paymentAmount;

	private String ccTxnFee;

	private String fullPayment;

	private String currencyCode;

	private String agentCode;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		return forward;
	}

	public String ccPayment() {
		String forward = S2Constants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			ExternalChgDTO externalChgDTO = GiftVoucherUtils.getCCFeeExternalChargeDTOFORVoucherPurchase();
			externalChgDTO.calculateAmount(new BigDecimal(paymentAmount));
			ccTxnFee = AccelAeroCalculator.scaleValueDefault(externalChgDTO.getAmount()).toString();
			fullPayment = AccelAeroCalculator
					.scaleValueDefault(AccelAeroCalculator.add(new BigDecimal(paymentAmount), externalChgDTO.getAmount()))
					.toString();
			currencyCode = AppSysParamsUtil.getBaseCurrency();
		}
		return forward;
	}

	public String oaPayment() {
		String forward = S2Constants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			try {
				TravelAgentBD travelAgentBD = (TravelAgentBD) ModuleServiceLocator.getTravelAgentBD();
				Agent travelAgent = travelAgentBD.getAgent(this.getAgentCode());
				String payCurrencyCode = travelAgent.getCurrencyCode();
				currencyCode = AppSysParamsUtil.getBaseCurrency();
				if (!payCurrencyCode.equals(currencyCode) && paymentAmount != null) {
					BigDecimal exchangeRateCC = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
							.getPurchaseExchangeRateAgainstBase(currencyCode);
					BigDecimal exchangeRatePC = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
							.getPurchaseExchangeRateAgainstBase(payCurrencyCode);
					BigDecimal exchangeRate = AccelAeroCalculator.divide(exchangeRateCC, exchangeRatePC);
					setFullPayment(AccelAeroRounderPolicy
							.convertAndRound(new BigDecimal(paymentAmount), exchangeRate, null, null).toString());
				} else {
					setFullPayment(new BigDecimal(paymentAmount).toString());
				}
				currencyCode = payCurrencyCode;
			} catch (ModuleException ex) {
				log.error("VoucherCCPaymentAction ==> execute()", ex);
			}
		}
		return forward;
	}

	public String getCcTxnFee() {
		return ccTxnFee;
	}

	public void setCcTxnFee(String ccTxnFee) {
		this.ccTxnFee = ccTxnFee;
	}

	public String getFullPayment() {
		return fullPayment;
	}

	public void setFullPayment(String fullPayment) {
		this.fullPayment = fullPayment;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @param paymentAmount
	 *            the paymentAmount to set
	 */
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

}
