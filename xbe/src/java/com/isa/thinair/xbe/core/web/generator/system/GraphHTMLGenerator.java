/**
 * 
 */
package com.isa.thinair.xbe.core.web.generator.system;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.core.util.LookupUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

/**
 * @author Noshani Perera
 * 
 */
public class GraphHTMLGenerator {

	public static void setGraphContent(HttpServletRequest request) throws ModuleException {

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String agentCode = userPrincipal.getAgentCode();
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(agentCode);

		StringBuilder sb = new StringBuilder();
		StringBuilder sb1 = new StringBuilder();

		GregorianCalendar gc = new GregorianCalendar();
		Calendar curDateCal = Calendar.getInstance();
		curDateCal.setTime(curDateCal.getTime());
		int date = curDateCal.get(Calendar.DAY_OF_MONTH);

		Date firstday = null;
		Date lastday = null;

		if (date >= 1 && date <= 15) {
			gc.set(Calendar.DATE, 1);
			firstday = gc.getTime();
			gc.set(Calendar.DATE, 15);
			lastday = gc.getTime();
		} else {
			gc.set(Calendar.DATE, 16);
			firstday = gc.getTime();
			gc.set(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
			lastday = gc.getTime();
		}

		SimpleDateFormat formater = new SimpleDateFormat("dd-MMM-yyyy");

		Map<Integer, String> mapGraphData = LookupUtils.getWebServiceDAO().getGraphContent(agentCode, formater.format(firstday),
				formater.format(lastday));

		int i = 0;
		sb1.append("[");

		if (mapGraphData != null && mapGraphData.size() > 0) {

			sb.append(" var options = {");
			sb.append(" 'IECanvasHTC': '/plotkit/iecanvas.htc',");
			sb.append(" 'colorScheme': PlotKit.Base.palette(PlotKit.Base.baseColors()[6].lighterColorWithLevel(0.8)),");
			sb.append(" 'padding': {left: 0, right: 0, top: 10, bottom: 10},");
			sb.append(" 'axisLabelFontSize': 13,");
			sb.append(" 'axisLabelWidth': 200,");
			sb.append(" 'backgroundColor':Color.whiteColor(),");
			sb.append(" 'xTicks': [");

			for (Iterator itr = mapGraphData.keySet().iterator(); itr.hasNext();) {
				Integer pnrCount = (Integer) itr.next();
				String userName = mapGraphData.get(pnrCount);

				sb.append("{v:" + i + ", label:'" + userName + "[" + pnrCount + "]'}");
				sb1.append("[" + i + ", " + pnrCount + "] ");

				if (itr.hasNext()) {
					sb.append(", ");
					sb1.append(", ");
				}

				i++;
			}
			sb.append(" ],");
			sb1.append("]);");

			sb.append(" 'drawYAxis': false");
			sb.append(" };");
			sb.append("  var layout = new Layout('pie', options);");
			sb.append(" layout.addDataset('sqrt', " + sb1.toString() + " ");
			sb.append("  layout.evaluate();");
			sb.append(" var chart = new SweetCanvasRenderer($('chart'), layout, options);");
			sb.append(" chart.render();");

		}

		request.setAttribute("graphContent", sb.toString());
		request.setAttribute("graphTitle", "Agent user transactions for Agent: " + agent.getAgentDisplayName() + " from: "
				+ formater.format(firstday) + " to: " + formater.format(lastday));
	}

}
