package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * @author Zaki Saimeh
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ExtendOnHoldAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ExtendOnHoldAction.class);

	private boolean success = true;
	private String messageTxt;

	private String groupPNR;
	private String pnr;
	private Integer hourOfDay;
	private Integer minutesOfHour;
	private String extendDate;
	private String version;

	public String execute() {

		String strForward = S2Constants.Result.SUCCESS;
		boolean isGroupPNR = false;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("M/d/y H mm");
			formatter.setLenient(false);
			Date extendDateTime = formatter.parse(extendDate + " " + hourOfDay + " " + minutesOfHour);

			String stationAirportCode = ((UserPrincipal) request.getUserPrincipal()).getAgentStation();
			Date extendDateTimeZulu = DateUtil.getZuluDateTime(extendDateTime, stationAirportCode);

			// Check to see that extendDate is not in the past
			// (Checking should never be done using JS, as the clock on the user's computer might be screwed up)
			if (extendDateTimeZulu.compareTo(new Date()) < 0) {
				success = false;
				messageTxt = XBEConfig.getClientMessage("cc_resmod_select_time_lower", userLanguage);
				return strForward;
			}
			isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (!isGroupPNR) {
				groupPNR = pnr;
			}

			ModuleServiceLocator.getAirproxyReservationBD().extendOnHold(groupPNR, extendDateTimeZulu, version, isGroupPNR,
					getClientInfoDTO(), getTrackInfo());

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		} finally {
			groupPNR = "";
			hourOfDay = null;
			extendDate = "";
		}

		return strForward;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the hourOfDay
	 */
	public Integer getHourOfDay() {
		return hourOfDay;
	}

	/**
	 * @param hourOfDay
	 *            the hourOfDay to set
	 */
	public void setHourOfDay(Integer hourOfDay) {
		this.hourOfDay = hourOfDay;
	}

	/**
	 * @return the extendDate
	 */
	public String getExtendDate() {
		return extendDate;
	}

	/**
	 * @param extendDate
	 *            the extendDate to set
	 */
	public void setExtendDate(String extendDate) {
		this.extendDate = extendDate;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getMinutesOfHour() {
		return minutesOfHour;
	}

	public void setMinutesOfHour(Integer minutesOfHour) {
		this.minutesOfHour = minutesOfHour;
	}

}