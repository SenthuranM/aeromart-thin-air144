package com.isa.thinair.xbe.core.web.action.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.quartz.JobExecutionException;

import com.isa.thinair.airpricing.api.service.AnciChargeLocalCurrBD;
import com.isa.thinair.airmaster.api.model.Event;
import com.isa.thinair.airmaster.api.model.EventProfile;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.OndPublishToCachingBL;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.invoicing.api.model.bsp.ReportFileTypes;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.scheduledservices.core.client.AutoCheckinSeatMapJob;
import com.isa.thinair.scheduledservices.core.client.AutomaticCheckinDetailsSender;
import com.isa.thinair.scheduledservices.core.client.BSPSales;
import com.isa.thinair.scheduledservices.core.client.BlackListAgents;
import com.isa.thinair.scheduledservices.core.client.EmailBookedHalaServices;
import com.isa.thinair.scheduledservices.core.client.EmailInvoicesJob;
import com.isa.thinair.scheduledservices.core.client.FlightClosurer;
import com.isa.thinair.scheduledservices.core.client.IBEOnholdReservationEmailNotifier;
import com.isa.thinair.scheduledservices.core.client.OHDPaymentReminderJob;
import com.isa.thinair.scheduledservices.core.client.OndPublishInvoker;
import com.isa.thinair.scheduledservices.core.client.OnlineCheckInReminder;
import com.isa.thinair.scheduledservices.core.client.PALCALTxSchedularJob;
import com.isa.thinair.scheduledservices.core.client.PalCalResender;
import com.isa.thinair.scheduledservices.core.client.PalCalSenderViaSitaTex;
import com.isa.thinair.scheduledservices.core.client.PnlAdlSenderViaSitaTex;
import com.isa.thinair.scheduledservices.core.client.RapidReportFiles.ReportFileGeneration;
import com.isa.thinair.scheduledservices.core.client.ReleaseBlockedSeatsFromSeatMap;
import com.isa.thinair.scheduledservices.core.client.ResolvePartialPayments;
import com.isa.thinair.scheduledservices.core.client.TotalPaymentNotification;
import com.isa.thinair.scheduledservices.core.client.UpdateAgentCredit;
import com.isa.thinair.scheduledservices.core.client.UserDashboardMessageStatusUpdate;
import com.isa.thinair.scheduledservices.core.client.rak.InsuranceDataPublisherServices;
import com.isa.thinair.scheduledservices.core.client.schedrep.ScheduledReportGenerationBL;
import com.isa.thinair.scheduledservices.core.client.ssmasm.ReleaseLockInMessageProcessingQueue;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.AncillaryReminderNotification;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.service.OnlineCheckInReminderNotification;
import com.isa.thinair.xbe.core.test.scheduler.TestInvoiceDetailsposterJob;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Test.COMMON_TESTING_UI),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Test.COMMON_TESTING_UI) })
public class CommonSchedularJobsTestAction extends BaseRequestAwareAction {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(CommonSchedularJobsTestAction.class);

	/** Holds the execution lock */
	private static Object lock = new Object();

	/**
	 * Execute Process
	 */
	public String execute() {

		String command = PlatformUtiltiies.nullHandler(request.getParameter("hdnMode"));
		String inputVal = PlatformUtiltiies.nullHandler(request.getParameter("hdnVal"));
		String inputVal1 = PlatformUtiltiies.nullHandler(request.getParameter("inMsgText"));
		if (command != null && command.length() > 0) {
			// if (command.length() < 6) {
			processRequest(request, command.charAt(0), command, inputVal, inputVal1);
			// } else {
			// processRequest(request, 'A', command);
			// }
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * Process the request
	 * 
	 * @param request
	 */
	private void processRequest(HttpServletRequest request, char command, String method, String inputVal, String inputVal1) {
		synchronized (lock) {
			String strSchedularJob = "";
			boolean blncommand = false;
			try {

				Date date = new Date();

				switch (command) {
				case 'A': // FIXME : have to test
					if (method.equals("AUTOCNX")) {
						log.debug("########## AUTOCNX job Started ");
						ModuleServiceLocator.getReservationBD().executeAutoCancellation(CalendarUtil.getCurrentSystemTimeInZulu(),
								null, null, null);
						log.debug("########## AUTOCNX job Ended ");
					} else if (method.equals("AVI")) {
						ModuleServiceLocator.getDataExtractionBD().generateAviatorFile();
					} else if (method.equals("ABM")) {
						ModuleServiceLocator.getTravelAgentFinanceBD().sendBSPCreditLimitUtilizationEmail();
					} else if (method.equals("AGENTTOPUP")) {

						try {
							String array1[] = inputVal.split("/");
							if (array1.length == 3) {
								int year = Integer.valueOf(array1[0]);
								int month = Integer.valueOf(array1[1]) - 1;
								int day = Integer.valueOf(array1[2]);

								log.info("########## Agent TopUp tranfer Scheduler Started...");
								Date daten = CalendarUtil.getDate(year, month, day);
								log.info("**********************--------------- date: " + daten.toString());
								ScheduledservicesUtils.getReservationAuxilliaryBD()
										.transferAgentTopUp(CalendarUtil.formatForSQL(daten, "dd-MM-yyyy"));
								log.info("########## Agent TopUp tranfer Scheduler Finished");
							} else {
								log.debug("!!!!!!!!!!! Agent TopUp tranfer ERROR: Date Invalied");
							}

						} catch (Exception e) {
							log.debug("!!!!!!!!!!! Agent TopUp tranfer ERROR: " + e);
						}

					}// Automatic Checkin Details Sender Scheduler
					else if (method.equals("AUTOCHK")) {
						AutomaticCheckinDetailsSender automaticCheckinDetailsSender = new AutomaticCheckinDetailsSender();
						automaticCheckinDetailsSender.executeAction(inputVal);
					} else if (method.equals("AUTOCHK_SEAT")) {
						AutoCheckinSeatMapJob autoCheckinSeatMapJob = new AutoCheckinSeatMapJob();
						autoCheckinSeatMapJob.executeAction(inputVal);
					} else {
						method(method, request); // invoke method
					}
					break;
				case 'B':
					if (method.equals("BC_STATUS")) {
						ModuleServiceLocator.getGDSServicesBD().publishAvailabilityMessages();
					} else if (method.equals("BLK")) {
						BlackListAgents blackListAgents = new BlackListAgents();
						blackListAgents.execute(date);
						request.setAttribute(WebConstants.REQ_MESSAGE, "BlackListAgents job executed");
					} else if (method.equals("BSP")) {
						log.debug("########## BSPLink job Started ");

						Date startDate = date;
						Date endDate = date;
						String countryCode = null;
						String agentCode = null;
						if (inputVal != null && !inputVal.isEmpty()) {
							SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
							String[] bspDataArray = inputVal.split("-");
							if (bspDataArray.length > 0) {
								try {
									startDate = fmt.parse(bspDataArray[0]);
								} catch (ParseException e) {
									log.error("Invaild Date");
								}

							}

							if (bspDataArray.length >= 2) {
								try {
									endDate = fmt.parse(bspDataArray[1]);
								} catch (ParseException e) {
									log.error("Invaild Date");
								}

							}

							if (bspDataArray.length >= 3) {
								if (bspDataArray[2] != null) {
									countryCode = bspDataArray[2].trim();
								}
							}
							if (bspDataArray.length >= 4) {
								if (bspDataArray[3] != null) {
									agentCode = bspDataArray[3].trim();
								}
							}
						}

						if (endDate.before(startDate)) {
							request.setAttribute(WebConstants.REQ_MESSAGE, "End date should be after start date");
						} else {
							ModuleServiceLocator.getInvoicingBD().publishDailySalesToBSP(startDate, endDate, false, countryCode,
									agentCode);
							ModuleServiceLocator.getInvoicingBD().uploadPendingRETFiles();
							request.setAttribute(WebConstants.REQ_MESSAGE, "BSPLink job executed");
						}

						log.debug("########## BSPLink job Ended");
						request.setAttribute(WebConstants.REQ_MESSAGE, "BSPLink job executed");

					} else if (method.equals("BSPTRANSFER")) { // transfer BSP sales
						BSPSales.transferBSPSales();
					}

					break;

				case 'C':
					if (method.equals("CSV")) {
						ModuleServiceLocator.getReconcileCsvBD().reconcileCSVReservations();

						request.setAttribute(WebConstants.REQ_MESSAGE, "Reconcile Booking from CSV executed");
					} else if (method.equals("CSVDE")) {
						ModuleServiceLocator.getReconcileCsvBD().processDepDateText();

						request.setAttribute(WebConstants.REQ_MESSAGE, "Update CSV Departure Date executed");

					} else if (method.equals("CFWS")) {
						ModuleServiceLocator.getReservationAuxilliaryBD().sendFlightLoadInfo();

						request.setAttribute(WebConstants.REQ_MESSAGE, "Send flight load info to Cargo Flash executed");

					} else if (method.equals("CFFU")) {

						Date startDate = null;
						Date endDate = null;

						if (inputVal != null && !inputVal.isEmpty()) {
							SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
							String[] dateArray = inputVal.split("-");
							if (dateArray.length > 0) {
								try {
									startDate = fmt.parse(dateArray[0]);
									endDate = fmt.parse(dateArray[1]);
								} catch (ParseException e) {
									startDate = null;
									endDate = null;
									log.error("Invaild Dates");
								}

							}
						}

						ModuleServiceLocator.getReservationAuxilliaryBD().publishPendingFlights(startDate, endDate);
						request.setAttribute(WebConstants.REQ_MESSAGE, "Send flight Update info to Cargo Flash executed");
					} else if (method.equals("CHIP")) {
						CommonsServices.getAerospikeCachingService().syncIpToCountryData();
						request.setAttribute(WebConstants.REQ_MESSAGE, "Export ip to country to caching DB executed");
					} else if (method.equals("CHAPALL")) {
						CommonsServices.getAerospikeCachingService().syncAllAppParamsToCache();
						request.setAttribute(WebConstants.REQ_MESSAGE, "Export app parameters to caching DB executed");
					} else if (method.equals("CHAP")) {
						String[] valueArray = null;
						if (inputVal != null && !inputVal.isEmpty()) {
							valueArray = inputVal.split("-");
						}
						CommonsServices.getAerospikeCachingService().syncAppParameterToCache(valueArray[0], valueArray[1], "W5");
						request.setAttribute(WebConstants.REQ_MESSAGE, "Export app parameters to caching DB executed");
					} else if (method.equals("CSPFS")) {
						ModuleServiceLocator.getMessageBrokerServiceBD().sendPFSMessages();
					} else if (method.equals("CSETL")) {
						ModuleServiceLocator.getMessageBrokerServiceBD().sendETLMessages();
					} else if (method.equals("CASHSALES")) {

						try {
							String array1[] = inputVal.split("/");
							if (array1.length == 3) {
								int year = Integer.valueOf(array1[0]);
								int month = Integer.valueOf(array1[1]) - 1;
								int day = Integer.valueOf(array1[2]);

								log.info("########## transferCashSales Scheduler Started...");
								Date daten = CalendarUtil.getDate(year, month, day);
								log.info("**********************--------------- date: " + daten.toString());
								ScheduledservicesUtils.getReservationAuxilliaryBD()
										.transferCashSales(CalendarUtil.formatForSQL(daten, "dd-MM-yyyy"));
								log.info("########## transferCashSales Scheduler Finished");
							} else {
								log.debug("!!!!!!!!!!! transferCashSales ERROR: Date Invalied");
							}

						} catch (Exception e) {
							log.debug("!!!!!!!!!!! transferCashSales ERROR: " + e);
						}

					} else if (method.equals("CREDITSALES")) {

						try {
							String array1[] = inputVal.split("/");
							if (array1.length == 3) {
								int year = Integer.valueOf(array1[0]);
								int month = Integer.valueOf(array1[1]) - 1;
								int day = Integer.valueOf(array1[2]);

								log.info("########## transferCreditCardSales Scheduler Started...");
								Date daten = CalendarUtil.getDate(year, month, day);
								log.info("**********************--------------- date: " + daten.toString());
								ScheduledservicesUtils.getReservationAuxilliaryBD()
										.transferCreditCardSales(CalendarUtil.formatForSQL(daten, "dd-MM-yyyy"));
								log.info("########## transferCreditCardSales Scheduler Finished");
							} else {
								log.debug("!!!!!!!!!!! transferCreditCardSales ERROR: Date Invalied");
							}

						} catch (Exception e) {
							log.debug("!!!!!!!!!!! transferCreditCardSales ERROR: " + e);
						}

					} else if (method.equals("CAMP")) {
						if (inputVal != null && !inputVal.isEmpty()) {
							ModuleServiceLocator.getPromotionManagementBD()
									.sendEmailsForScheduledCampaigns(Integer.valueOf(inputVal));
						}
					} else if (method.equals("CSPFS")) {
						ModuleServiceLocator.getMessageBrokerServiceBD().sendPFSMessages();
					} else if (method.equals("CSETL")) {
						ModuleServiceLocator.getMessageBrokerServiceBD().sendETLMessages();
					}

					break;
				case 'D':
					if ("DTA".equals(method)) {
						ModuleServiceLocator.getTravelAgentBD().deactivateTravelAgents();
						request.setAttribute(WebConstants.REQ_MESSAGE, "Deactivate Travel Agents job executed");
					} else {
						log.debug("########## AmadeusDSUPublisherJob Started ");
						ModuleServiceLocator.getGDServicesBD().publishDailyScheduleUpdateMessages();
						log.debug("########## AmadeusDSUPublisherJob Ended");
						request.setAttribute(WebConstants.REQ_MESSAGE, "BSPLink job executed");
					}
					break;

				case 'E':
					if (method.equals("EMAIL")) {
						EmailInvoicesJob emailInvoices = new EmailInvoicesJob();
						// emailInvoices..execute(date);
						request.setAttribute(WebConstants.REQ_MESSAGE, "Email Invoices job executed");
					}
					// CDJ
					if (method.equals("EXC")) {
						ModuleServiceLocator.getCommonServiceBD().updateExchangeRate();
					}

					// Email the invoices.
					if ("EINV".equals(method)) {
						ModuleServiceLocator.getInvoicingBD().autoEmailInvoices();
					}
					// ETL processing
					if ("ETL".equals(method)) {
						log.debug("########## ETL processing Started ");
						ModuleServiceLocator.getETLBD().processETLMessage();
						log.debug("########## ETL processing Stopped ");
					}
					// Expiring passenger credit
					if ("EPC".equals(method)) {
						Date dateT = new Date();
						Date[] dateArray = getInputDates(inputVal);
						if (dateArray != null && dateArray.length > 0 && dateArray[0] != null) {
							dateT = dateArray[0];
						}
						ModuleServiceLocator.getResPassengerBD().expirePassengerCredits(dateT);
					}
					// Expiring vouchers
					if ("EVOU".equals(method)) {
						ModuleServiceLocator.getVoucherBD().expireVouchers();
					}
					
					if ("EVENT".equals(method)) {
						EventProfile eventProfile = new EventProfile();
						Event event = new Event(); 
						event.setEventCategory("bookingClass");
						event.setEventName("InventoryChange");
						
						Map<String, String> eventData =  new HashMap<>();
						eventData.put("origin", "SHJ");
						eventData.put("destination", "CMB");
						eventData.put("effectiveFrom", "20181005");
						eventData.put("effectiveTo", "20181010");
						
						event.setEventData(eventData);
						
						eventProfile.setEvent(event);
						eventProfile.setTopicName("myChannel");						
						
						ModuleServiceLocator.getEventServiceBD().publishEvent(eventProfile);
					}

					break;
				case 'F':
					if ("FUP".equals(method)) {
						FareBD fareBD = ModuleServiceLocator.getFareBD();
						ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
						AnciChargeLocalCurrBD anciChargeLocalCurrBD = ModuleServiceLocator.getAnciChargeLocalCurrBD();
						// ScheduledservicesUtils.getAnciChargeLocalCurrBD().baseCurrAmountUpdateOnLocalCurrExchangeRtUpdate();

						log.info("######### Fare Update on Exchange Rate Change Automation Job started at " + new Date());
						fareBD.fareUpdateOnExchangeRateChange();
						log.info("######### Fare Update on Exchange Rate Change Automation Job ended at " + new Date());

						log.info("######### Charge Update on Exchange Rate Change Automation Job started at " + new Date());
						chargeBD.chargeUpdateOnExchangeRateChange();
						log.info("######### Charge Update on Exchange Rate Change Automation Job ended at " + new Date());

						log.info("######### ANCI Charge Update on Exchange Rate Change Automation Job started at " + new Date());
						anciChargeLocalCurrBD.baseCurrAmountUpdateOnLocalCurrExchangeRtUpdate();
						log.info("######### ANCI Charge Update on Exchange Rate Change Automation Job ended at " + new Date());

						// FareUpdateOnExchangeRateChangeAutomation fareUpdateOnExchangeRateChangeAutomation = new
						// FareUpdateOnExchangeRateChangeAutomation();
						// fareUpdateOnExchangeRateChangeAutomation.execute();
						request.setAttribute(WebConstants.REQ_MESSAGE,
								"Automated Fare Update upon Exchange Rate Change job executed. ");
					} else if (method.startsWith("FLT_CLS")) {
						String[] arguments = method.split(",");
						String flightNum = arguments[1];
						String depAirPort = arguments[2];
						String flightID = arguments[3];
						FlightClosurer flightclosure = new FlightClosurer();
						flightclosure.executeAction(flightID, depAirPort, flightNum, null);
					} else if (method.startsWith("FLT_FLOWN")) {
						String[] arguments = method.split(",");
						String depAirPort = arguments[1];
						String flightID = arguments[2];
						ModuleServiceLocator.getReservationBD().updatePassengerCoupons(new Integer(flightID), depAirPort);
					}
					break;
				case 'G':
					if (method.equals("GUR")) {
						ModuleServiceLocator.getTravelAgentFinanceBD().sendBankGuaranteeNotificationEmail();
					} else if (method.equals("GDS_AVS")) {
						ModuleServiceLocator.getGDSServicesBD().publishAvailabilityMessages();
					} else if (method.equals("GETAP")) {
						CommonsServices.getAerospikeCachingService().getAppParamValueByParamKey(inputVal);
						request.setAttribute(WebConstants.REQ_MESSAGE, "Get app parameters from caching DB executed");
					} else if (method.equals("GST")) {
						ModuleServiceLocator.getReservationBD().invokeTestInvoiceCall(inputVal);
					}

					break;
				case 'H':
					if (method.equals("HALA")) {
						EmailBookedHalaServices emailBookedHalaServices = new EmailBookedHalaServices();
						emailBookedHalaServices.execute();

						request.setAttribute(WebConstants.REQ_MESSAGE, "Email booked hala services job executed");
					}
					break;
				case 'I': // Rak InsurancePublish
					if (method.equals("INS")) {
						InsuranceDataPublisherServices insuranceDataPublisherServices = new InsuranceDataPublisherServices();
						insuranceDataPublisherServices.execute();
						request.setAttribute(WebConstants.REQ_MESSAGE, "Rak insurance publishing job executed");
					} else if ("INV".equals(method)) {
						TestInvoiceDetailsposterJob invoicePosterJobTester = new TestInvoiceDetailsposterJob();
						SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
						Date invoiceDate = null;
						try {
							invoiceDate = fmt.parse(inputVal);
							invoicePosterJobTester.execute(invoiceDate);
						} catch (ParseException e) {
							log.error("Invaild Date");
						}
						request.setAttribute(WebConstants.REQ_MESSAGE, "Invoice Details posting job executed");
					} else if ("ION".equals(method)) {
						request.setAttribute(WebConstants.REQ_MESSAGE, "IBE Onhold Notification job execution Started");
						IBEOnholdReservationEmailNotifier notifier = new IBEOnholdReservationEmailNotifier(5, 10);
						notifier.sendEmailNotification();
						request.setAttribute(WebConstants.REQ_MESSAGE, "Invoice Details posting job execution Stopped");
					} else if ("INSR".equals(method)) {

						int searchGapInMinutes = 60;
						if (inputVal != null && !inputVal.isEmpty()) {
							searchGapInMinutes = Integer.parseInt(inputVal);
						}

						GregorianCalendar gc = new GregorianCalendar();
						gc.setTime(new Date());
						gc.add(GregorianCalendar.MINUTE, -searchGapInMinutes);
						request.setAttribute(WebConstants.REQ_MESSAGE, "Reconcile Insurances For Failures job execution Started");
						ModuleServiceLocator.getReservationAuxilliaryBD().reconcileInsurancesForFailures(gc.getTime());
						request.setAttribute(WebConstants.REQ_MESSAGE, "Reconcile Insurances For Failures job execution Stopped");
					}
					break;
				case 'Q':
					if (method.equals("QRET")) {
						ReportFileGeneration retFileGen = new ReportFileGeneration();
						retFileGen.execute(1, ReportFileTypes.FileTypes.RET);
						request.setAttribute(WebConstants.REQ_MESSAGE, "Ret File publishing job executed");
					} else if (method.equals("QHOT")) {
						ReportFileGeneration retFileGen = new ReportFileGeneration();
						retFileGen.execute(1, ReportFileTypes.FileTypes.HOT);
						request.setAttribute(WebConstants.REQ_MESSAGE, "Hot File publishing job executed");
					} else if (method.equals("QETLR")) {
						ReportFileGeneration retFileGen = new ReportFileGeneration();
						retFileGen.execute(1, ReportFileTypes.FileTypes.ETLR);
						request.setAttribute(WebConstants.REQ_MESSAGE, "ETLR File publishing job executed");
					} else if ("ION".equals(method)) {
						request.setAttribute(WebConstants.REQ_MESSAGE, "IBE Onhold Notification job execution Started");
						IBEOnholdReservationEmailNotifier notifier = new IBEOnholdReservationEmailNotifier(5, 10);
						notifier.sendEmailNotification();
						request.setAttribute(WebConstants.REQ_MESSAGE, "Invoice Details posting job execution Stopped");
					}
					break;
				case 'N':
					if (method.equals("NSR")) {
						log.debug("NOSHOW Auto Refund process Started Successfully");
						ModuleServiceLocator.getReservationBD().processNSAutoRefundPNRs();
						log.debug("NOSHOW Auto Refund process executed Successfully");
						request.setAttribute(WebConstants.REQ_MESSAGE, " NOSHOW Auto refund job executed");
					} else {
						AncillaryReminderNotification notification = new AncillaryReminderNotification();
						notification.sendReminderNotification(new Date());
					}
					break;

				case 'M':
					if ("MSG_SEND".equals(method)) {
						ModuleServiceLocator.getMessageBrokerServiceBD().sendMessages();
					}
					if ("MSG_RECV".equals(method)) {
						ModuleServiceLocator.getMessageBrokerServiceBD().receiveMessage(true);
					}
					if ("MSG_QUEUE".equals(method)) {
						ModuleServiceLocator.getMessageBrokerServiceBD().clearInMsgProcessingQueue();
					}
					if ("MSG_LOCK".equals(method)) {
						ReleaseLockInMessageProcessingQueue.executeJob();
					}
				case 'O':

					if (method.equals("ONDJS")) {
						OndPublishInvoker ondPublishInvoker = new OndPublishInvoker();
						try {
							ondPublishInvoker.execute();
							request.setAttribute(WebConstants.REQ_MESSAGE, " ONDJS Publisher executed");
						} catch (Exception ex) {
							throw new JobExecutionException(ex);
						}
					}
					if (method.equals("ONDCACHE")) {
						// OndPublishedTO output = ScheduledservicesUtils.getCommonMasterBD().getOndListForSystem();
						OndPublishedTO output = ModuleServiceLocator.getCommonServiceBD().getOndListForSystem();
						OndPublishToCachingBL casheBL = new OndPublishToCachingBL();
						casheBL.prepareListForCaching(output);

						// boolean b = ModuleServiceLocator.getCachingServiceBD().isLCCRequired("SHJ", "DEL", "G9");
						// HBEJED
						// boolean b2 = ModuleServiceLocator.getCachingServiceBD().isLCCRequired("SHJ", "CMB", "E5");
						// HBEAMN

						request.setAttribute(WebConstants.REQ_MESSAGE, " Ond combinations publish to cache job executed");
					}
					if (method.equals("ORR")) {
						log.debug("########## Onhold Release Started ");
						ModuleServiceLocator.getReservationBD().expireOnholdReservations(new Date(), null, null, null);
						log.debug("########## Onhold Release Stopped ");
					}
					if (method.equals("OGSBC")) {
						boolean isValid = true;
						if (isValid) {
							// GoshowBCOpenerSchedulerJob goShowBCOpenerSchedulerJob = new GoshowBCOpenerSchedulerJob();
							// goShowBCOpenerSchedulerJob.execute();
						} else {
							// ArrayList<FlightSegmentDTO> flightSegList = ScheduledservicesUtils.getFlightBD()
							// .getGoshowBCOpeningFilghtList(new Date());
							// ModuleServiceLocator.getFlightInventoryBD().openGoshowBcInventory(flightSegList.get(0));
						}
					}

					if (method.equals("ONC")) {
						OnlineCheckInReminder onlineCheckInReminder = new OnlineCheckInReminder();
						try {
							onlineCheckInReminder.execute();
						} catch (Exception e) {
							log.error("Error in executing online checking reminder job", e);
							throw new JobExecutionException(e);
						}
					}
					break;
				case 'P': // ResolvePartialPaymentsJob
					if (method.equals("PAL")) {
						PALCALTxSchedularJob palCalTxJob = new PALCALTxSchedularJob();
						palCalTxJob.executeInternal(null);
						request.setAttribute(WebConstants.REQ_MESSAGE, "PAL CAL TX JOB SUCCESSFULLY EXECUTED");
						int flightId = Integer.parseInt(inputVal);
						String destination = inputVal1;
						ModuleServiceLocator.getReservationAuxilliaryBD().sendPALMessage(flightId, destination);
						request.setAttribute(WebConstants.REQ_MESSAGE, "PAL SUCCESSFULLY Sent");
					}
					if (method.equals("PCAL")) {
						// PALCALTxSchedularJob palCalTxJob = new PALCALTxSchedularJob();
						// palCalTxJob.executeInternal(null);
						// request.setAttribute(WebConstants.REQ_MESSAGE, "PAL CAL TX JOB SUCCESSFULLY EXECUTED");
						int flightId = Integer.parseInt(inputVal);
						String destination = inputVal1;
						ModuleServiceLocator.getReservationAuxilliaryBD().sendCALMessage(flightId, destination);
						request.setAttribute(WebConstants.REQ_MESSAGE, "PAL SUCCESSFULLY Sent");
					}
					if (method.equals("PAL_RESEND")) {
						// PnlAdlResender.checkAndResend();
						PalCalResender.checkAndResend();
					}

					if (method.equals("PAY")) {
						ResolvePartialPayments payments = new ResolvePartialPayments();
						payments.resolvePartialPayments();
						request.setAttribute(WebConstants.REQ_MESSAGE, " Resolve PartialPayments job executed");
					}
					if (method.equals("PFS")) {
						log.debug("Automatic Reconciliation Started Successfully");
						ModuleServiceLocator.getReservationBD().processAutomaticReconcilation();
						log.debug("Automatic Reconciliation Ended Successfully");
						request.setAttribute(WebConstants.REQ_MESSAGE, " PFS Processing job executed");
					}
					if (method.equals("PFSX")) {
						log.debug("Automatic Reconciliation Started Successfully");
						String dateStr = inputVal;// ddMMyyyy
						DateFormat df = new SimpleDateFormat("ddMMyyyy");
						Date day = df.parse(dateStr);
						ModuleServiceLocator.getPfsXmlBD().processPFSXml(day);
						log.debug("Automatic Reconciliation Ended Successfully");
						request.setAttribute(WebConstants.REQ_MESSAGE, " PFS Processing job executed");
					}
					// PRL processing
					if ("PRL".equals(method)) {
						log.debug("########## PRL processing Started ");
						ModuleServiceLocator.getPRLBD().processPRLMessage();
						log.debug("########## PRL processing Stopped ");
					}

					/**
					 * Note: if using following PUB_ALL_SSM & PUB_ALL_ASM methods all required gds ids should be sent,
					 * in case of missing an gds id for an already published SSM/ASM considered as unpublished from the
					 * respective gds.
					 * 
					 * Usage : SCHEDULE_ID=1345 already published to GDS=2, if u want to publish all the schedule to
					 * GDS=1
					 * 
					 * CMD="PUB_ALL_SSM" input="1,2" => will publish to 1 only no action to GDS 2
					 * 
					 * CMD="PUB_ALL_SSM" input="1" => will publish to 1 and unpublished from 2
					 */
					/*
					 * if ("PUB_ALL_SSM".equals(method)) {
					 * 
					 * List<Integer> gdsIds = new ArrayList<Integer>();
					 * 
					 * if(inputVal.length() > 0 && inputVal.length()<2){ gdsIds.add(Integer.parseInt(inputVal)); } else{
					 * // Set GDS ID as 1 gdsIds.add(1); }
					 * 
					 * ModuleServiceLocator.getScheduleServiceBD().publishAllSchedulesToGDS(gdsIds); } if
					 * ("PUB_ALL_ASM".equals(method)) {
					 * 
					 * List<Integer> gdsIds = new ArrayList<Integer>();
					 * 
					 * if(inputVal.length()>0 && inputVal.length()<2){ gdsIds.add(Integer.parseInt(inputVal)); } else{
					 * // Set GDS ID as 1 gdsIds.add(1); }
					 * ModuleServiceLocator.getFlightServiceBD().publishAllAdhocFlightsToGDS(gdsIds); }
					 */
					if ("PUB_PNL_ADL".equals(method)) {
						// Set GDS ID as 1
						PnlAdlSenderViaSitaTex pnlAdlSender = new PnlAdlSenderViaSitaTex();
						pnlAdlSender.execute();
					}
					if ("PUB_PAL_CAL".equals(method)) {
						// Set GDS ID as 1
						PalCalSenderViaSitaTex palCalSender = new PalCalSenderViaSitaTex();
						palCalSender.execute();
					}
					if (method.equals("PNRGOV")) {
						if (inputVal != null && !inputVal.trim().isEmpty()) {
							String[] govArr = inputVal.split(",");
							int flightSegmentId = Integer.parseInt(govArr[0]);
							String contryCode = govArr[1];
							String airportCode = govArr[2];
							int timePeriod = Integer.parseInt(govArr[3]);
							String inboundOutbound = govArr[4];
							log.info("Flight seg id:" + flightSegmentId + ", contryCode:" + "" + contryCode + ", airportCode:"
									+ airportCode + ", timePeriod:" + timePeriod + ", inboundOutbound:" + inboundOutbound);
							ModuleServiceLocator.getReservationBD().publishPNRGOV(flightSegmentId, contryCode, airportCode,
									timePeriod, inboundOutbound);
							log.info("Successfully published the messages to flight segment id : " + flightSegmentId);
						}
					}
					if (method.contains("PUB_ACKNAC")) {
						ScheduledservicesUtils.getMessageBrokerServiceBD().sendACKNACMessages();
					}

					break;
				case 'R':
					if (method.equals("RBS")) {
						ReleaseBlockedSeatsFromSeatMap releaseBlockedSeatsFromSeatMap = new ReleaseBlockedSeatsFromSeatMap();
						releaseBlockedSeatsFromSeatMap.releaseBlockedSeatsFromSeatMap();
						request.setAttribute(WebConstants.REQ_MESSAGE, "ReleaseBlockedSeatsFromSeatMap job executed");
					} else if (method.equals("ROP")) {
						OHDPaymentReminderJob ohdPaymentReminderJob = new OHDPaymentReminderJob();
						ohdPaymentReminderJob.executeInternal(null);
					}
					break;
				case 'S': // Scheduled reports testing
					if (method.contains("Sc")) {
						int seperetorIndex = method.indexOf(":");

						if (seperetorIndex != -1) {

							Integer scheduledReportId = new Integer(method.substring(seperetorIndex + 1));

							// Execute Report Generation logic
							ScheduledReportGenerationBL reportGenerator = new ScheduledReportGenerationBL();
							reportGenerator.generateReport(scheduledReportId);

						}
						request.setAttribute(WebConstants.REQ_MESSAGE, " Resolve PartialPayments job executed");
					} else if (method.contains("SSM_CNL")) {
						ModuleServiceLocator.getScheduleServiceBD().resumeSsmAsmWaitingCancelActions();
					} else if (method.contains("SSM_PUB")) {
						ModuleServiceLocator.getSSMASMServiceBD().publishSSMASMMessages();
					} else if (method.contains("SSM_RECAP_EXT")) {
						if (inputVal != null && !inputVal.trim().isEmpty()) {
							String[] govArr = inputVal.split(",");
							String email = govArr[0];
							String sita = "";
							if (govArr.length > 1) {
								sita = govArr[1];
							}
							Calendar calendar = Calendar.getInstance();
							Date today = calendar.getTime();
							calendar.add(Calendar.MONTH, 2);
							Date tomorrow = calendar.getTime();
							ModuleServiceLocator.getScheduleServiceBD().publishSchedulesToExternal(today, tomorrow, true, email,
									sita);
						}
					} else if (method.contains("SSM_RECAP")) {
						if (inputVal != null && !inputVal.trim().isEmpty()) {
							String[] govArr = inputVal.split(",");
							int gdsId = Integer.parseInt(govArr[0]);
							Calendar calendar = Calendar.getInstance();
							Date today = calendar.getTime();
							calendar.add(Calendar.MONTH, 2);
							Date tomorrow = calendar.getTime();
							ModuleServiceLocator.getScheduleServiceBD().publishSchedulesToGDS(gdsId, today, tomorrow, true,
									"chanu619@yahoo.com");
						}
					} else if (method.contains("SEQ_PROC")) {
						ModuleServiceLocator.getMessageBrokerServiceBD().processMessagesSequentially();
					}

					if ("SITA_ETL".equals(method)) {
						ModuleServiceLocator.getMessageBrokerServiceBD().receiveMessage(false);
					}
					// SSIM processing
					if ("SSIM".equals(method)) {
						log.debug("########## SSIM processing Started ");
						ModuleServiceLocator.getSSIMBD().processScheduleMessage();
						log.debug("########## SSIM processing Stopped ");
					}

					break;
				case 'T': // Type B testing
					if (method.contains("TTY")) {
						TypeBRQ typeBRQ = new TypeBRQ();
						typeBRQ.setTypeBMessage(inputVal1);
						ModuleServiceLocator.getTypeBServiceBD().processTypeBMessage(typeBRQ);
					} else if (method.contains("TWS")) {
						TypeBRQ typeBRQ = new TypeBRQ();
						typeBRQ.setTypeBMessage(inputVal1);
						typeBRQ.setWsServerUrl("http://10.20.11.138:8180/");
						ModuleServiceLocator.getWSClientBD().processTypeBResMessage(typeBRQ);
					} else if (method.contains("TYPE-A")) {
						if (inputVal.equals("IN")) {

						}
						if (inputVal.equals("OUT")) {
							ModuleServiceLocator.getTypeAServiceBD().sendMessageToGDS(null, inputVal1);
						}
					}
					break;
				case 'U':
					if (method.equals("UAC")) {
						UpdateAgentCredit update = new UpdateAgentCredit();
						update.execute(null); // GMT get system date
						request.setAttribute(WebConstants.REQ_MESSAGE, "UpdateAgentCredit job executed");
					} else if ("UBVOU".equals(method)) {
						// Unblocking vouchers
						ModuleServiceLocator.getVoucherBD().unblockVouchers();
					}
					break;
				case 'Z':
					if (method.equals("ZAP")) {
						ModuleServiceLocator.getCommonServiceBD().publishMasterDataUpdates();
						request.setAttribute(WebConstants.REQ_MESSAGE, "lccMasterDataPublisher job executed");
					} else if (method.equals("ZUI")) {
						ModuleServiceLocator.getCommonServiceBD().publishUserInfoUpdates();
						request.setAttribute(WebConstants.REQ_MESSAGE, "lccUserInfoPublisher job executed");
					}
					break;
				case 'X':
					if (method.equals("XBEDB")) {
						UserDashboardMessageStatusUpdate xbeDB = new UserDashboardMessageStatusUpdate();
						xbeDB.execute();
						request.setAttribute(WebConstants.REQ_MESSAGE, "XBE dashboard synchronize job executed");
					}
					break;

				case 'L':
					if (method.equals("LNE")) {
						ModuleServiceLocator.getTravelAgentFinanceBD().sendCreditLimitUtilizationEmail();
					} else if (method.equals("LMS")) {
						UserDashboardMessageStatusUpdate messageUpdater = new UserDashboardMessageStatusUpdate();
						messageUpdater.execute();
					} else if (method.equals("LFLW")) {
						log.debug("########## BSPLink job Started ");
						String reportDate = PlatformUtiltiies.nullHandler(request.getParameter("inMsgText"));

						// date = new Date();
						SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
						// date = fmt.parse(fmt.format(date)); // Convert the date to yyyy/MM/dd 00:00

						date = fmt.parse(reportDate);
						// toDate = yyyy/MM/(dd-1) 11:59
						Date toDate = new Date(date.getTime() - TimeUnit.SECONDS.toMillis(1));

						// fromDate = (yyyy/MM/(dd-salesFrequncyInDays) 00:00)
						Date fromDate = new Date(date.getTime() - TimeUnit.DAYS.toMillis(1));

						ModuleServiceLocator.getLoyaltyManagementBD().generateFlownFile(fromDate, toDate, false);
						ModuleServiceLocator.getLoyaltyManagementBD().updatePendingFiles(FILE_TYPE.FLOWN_FILE);
						request.setAttribute(WebConstants.REQ_MESSAGE, "LMS flown file generate job executed");

						log.debug("########## LMS flown file generate job Ended");
						request.setAttribute(WebConstants.REQ_MESSAGE, "LMS flown file generate job executed");
					} else if (method.equals("LPRD")) {
						ModuleServiceLocator.getLoyaltyManagementBD().generateProductFile();
						ModuleServiceLocator.getLoyaltyManagementBD().updatePendingFiles(FILE_TYPE.PRODUCT_FILE);
						request.setAttribute(WebConstants.REQ_MESSAGE, "LMS product file generate job executed");

						log.debug("########## LMS product file generate job Ended");
						request.setAttribute(WebConstants.REQ_MESSAGE, "LMS product file generate job executed");
					} else if (method.equals("LMS_CNL")) {
						log.debug("########## EXPIRED LMS BLOCKED CREDIT PAYMENT TNX STARTED");
						ModuleServiceLocator.getReservationBD().cancelFailedLMSBlockedCreditPayments();
						log.debug("########## EXPIRED LMS BLOCKED CREDIT PAYMENT TNX JOB EXECUTED");
					} else if (method.equals("LMS_CNX_ISSUED")) {
						log.debug("########## EXPIRED LMS ISSUED UNCONSUMED CREDIT TNX JOB STARTED");
						ModuleServiceLocator.getReservationBD().clearIssuedUnConsumedLMSCreditPayments();
						log.debug("########## EXPIRED LMS ISSUED UNCONSUMED CREDIT TNX JOB EXECUTED");
					}
					break;
				case 'J':
					OnlineCheckInReminderNotification notificationOnl = new OnlineCheckInReminderNotification();
					notificationOnl.sendReminderNotification();
					break;

				case 'K':
					TotalPaymentNotification notificationTp = new TotalPaymentNotification();
					notificationTp.execute();
					break;
				case 'o':

				default:
					request.setAttribute(WebConstants.REQ_MESSAGE, "Command Not Found ");
					blncommand = true;
					break;
				}

			} catch (ModuleException moduleException) {

				log.error("error ", moduleException);

				request.setAttribute(WebConstants.REQ_MESSAGE, moduleException.getExceptionCode());
				StringBuilder strBuilder = new StringBuilder();
				for (int a = 0; a < moduleException.getStackTrace().length; a++) {
					strBuilder.append(moduleException.getStackTrace()[a]);
				}

				request.setAttribute("reqDetailException", strBuilder.toString());
				strSchedularJob = moduleException.getExceptionCode();
			} catch (Exception exception) {
				log.error("error", exception);
			} finally {
				if (!blncommand) {
					StringBuilder strShedularJob = new StringBuilder();
					strShedularJob.append("Finished executing schedular Job");
					if (!strSchedularJob.isEmpty()) {
						strShedularJob.append(" With Result : ");
						strShedularJob.append(" ( " + strSchedularJob + ")");
					}
					request.setAttribute(WebConstants.REQ_MESSAGE, strShedularJob.toString());
				}
			}
		}
	}

	private Date[] getInputDates(String inputVal) {
		if (inputVal != null && !inputVal.isEmpty()) {
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
			String[] dateStringList = inputVal.split("-");
			if (dateStringList.length > 0) {
				try {
					Date[] dateArr = new Date[dateStringList.length];
					for (int i = 0; i < dateStringList.length; i++) {
						dateArr[i] = fmt.parse(dateStringList[i]);
					}
					return dateArr;
				} catch (ParseException e) {
					log.error("Invaild Dates");
				}
			}
		}
		return null;
	}

	private static Object[] method(String command, HttpServletRequest request) {
		Object[] objArray = new Object[3];
		String[] spitCommand = command.split("-");
		String[] params = spitCommand[1].split(",");
		objArray[0] = spitCommand[0];
		objArray[1] = construtParams(params);
		String[] values = spitCommand[2].split(",");
		construtValues(values, construtParams(params));
		return objArray;
	}

	private static Class[] construtParams(String[] params) {
		Class[] aa = new Class[params.length];
		for (int a = 0; a <= params.length; a++) {
			Class classT = getClassForType(params[a]);
			aa[a] = classT;
		}

		return aa;
	}

	private static Object[] construtValues(String[] values, Class[] params) {
		Object[] aa = new Object[values.length];
		for (int a = 0; a <= values.length; a++) {
			Object obj = new Object();
			try {
				obj = getObjectForType(values, params);
			} catch (SecurityException e) {
				log.error(e);
			}
			aa[a] = obj;
		}

		return aa;
	}

	private static Class getClassForType(String type) {
		if (type.equalsIgnoreCase("date")) {
			return Date.class;
		}
		if (type.equalsIgnoreCase("string")) {
			return String.class;
		}
		if (type.equalsIgnoreCase("int")) {
			return Integer.class;
		}
		if (type.equalsIgnoreCase("double")) {
			return Double.class;
		}
		if (type.equalsIgnoreCase("float")) {
			return Float.class;
		}
		if (type.equalsIgnoreCase("BigDecimal")) {
			return BigDecimal.class;
		}
		return null;
	}

	private static Object[] getObjectForType(String[] val, Class param[]) {

		Object[] objs = new Object[val.length];

		for (int a = 0; a <= val.length; a++) {
			if (param[a].equals(Date.class)) {
				// This is a date
			} else {
				Class c = param[a];
				Constructor con = null;
				try {
					con = c.getConstructor(String.class);
				} catch (SecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NoSuchMethodException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					objs[a] = con.newInstance(val[a]);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return objs;
	}

}
