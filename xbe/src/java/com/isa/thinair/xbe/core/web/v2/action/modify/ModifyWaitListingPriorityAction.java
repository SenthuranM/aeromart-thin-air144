package com.isa.thinair.xbe.core.web.v2.action.modify;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ModifyWaitListingPriorityAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ExtendOnHoldAction.class);

	private boolean success = true;
	private String messageTxt;

	private String groupPNR;
	private String pnr;
	private Integer priority;
	private Integer fltSegId;	
	private String version;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		String strForward = S2Constants.Result.SUCCESS;
		boolean isGroupPNR = false;

		try {		
			isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			if (!isGroupPNR) {
				groupPNR = pnr;
			}

			ModuleServiceLocator.getAirproxyReservationBD().modifyWaitListedPriority(groupPNR, priority, fltSegId, version, isGroupPNR,
					getClientInfoDTO(), getTrackInfo());

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		} finally {
			groupPNR = "";
			priority = null;
			fltSegId = null;
		}

		return strForward;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getFltSegId() {
		return fltSegId;
	}

	public void setFltSegId(Integer fltSegId) {
		this.fltSegId = fltSegId;
	}

}