package com.isa.thinair.xbe.core.web.action.alerts;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.alerts.FlightPnrNotificationRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Alert.SHOW_FLIGHT_PNR_NOTIFICATION_MESSAGE),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowFlightPnrNotificationMessageAction extends BaseRequestAwareAction {

	public String execute() {
		return FlightPnrNotificationRequestHandler.execute(request);
	}

}
