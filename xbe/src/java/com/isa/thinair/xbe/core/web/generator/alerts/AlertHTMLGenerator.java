package com.isa.thinair.xbe.core.web.generator.alerts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class AlertHTMLGenerator {

	private static Log log = LogFactory.getLog(AlertHTMLGenerator.class);

	private static String clientErrors;

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	private static final String ALERT_ID_SEPERATOR = "-";

	/**
	 * returns the alert detail data
	 * 
	 * @return String
	 */
	public final String getAlertRowHtml(HttpServletRequest request) throws ModuleException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		Iterator it = null;
		String[] arrAlertInfo = null;
		String alertInfo = null;
		String alertFromDate = null;
		String alertToDate = null;
		String flightNo = null;
		String depDate = null;
		String oriDepDate = null;
		Date fromDate = null;
		Date toDate = null;
		Date flgDate = null;
		Date originalDepDate = null;
		String searchAirline = null;
		String anciReprotectStatus = null;
		String flightStatus = null;
		StringBuffer sbData = new StringBuffer();
		String arrName = "arrData";
		int totalRecords = 0;
		int count = 0;
		String originAirport = null;
		boolean checkForRescheduled = false;

		HashMap userPrivileges = (HashMap) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

		boolean blnViewAnyAlerts = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_ANY_ALERT);
		boolean blnViewRepAgentsAlerts = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_REP_AGENT_ALERT);
		boolean blnViewOwnAlerts = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_OWN_ALERT);
		boolean searchAnyAirlineAlerts = BasicRH.hasPrivilege(request, PriviledgeConstants.ALLOW_VIEW_ANY_AIRLINE_ALERT);

		sbData.append("var " + arrName + " = new Array();");
		alertInfo = request.getParameter(WebConstants.PARAM_ALERT_INFO);

		if (alertInfo != null && (blnViewAnyAlerts || blnViewOwnAlerts || blnViewRepAgentsAlerts)) {

			arrAlertInfo = alertInfo.split(",");

			if (airlineSearchAuthorised(arrAlertInfo[9], searchAnyAirlineAlerts)) {

				if ((arrAlertInfo != null) && (arrAlertInfo.length > 0)) {
					alertFromDate = (arrAlertInfo[1].trim().equalsIgnoreCase("") ? null : arrAlertInfo[1].trim());
					alertToDate = (arrAlertInfo[2].trim().equalsIgnoreCase("") ? null : arrAlertInfo[2].trim());
					flightNo = (arrAlertInfo[3].trim().equalsIgnoreCase("") ? null : arrAlertInfo[3].trim().toUpperCase());
					depDate = (arrAlertInfo[4].trim().equalsIgnoreCase("") ? null : arrAlertInfo[4].trim());

					totalRecords = 100;
					try {
						totalRecords = Integer.parseInt(arrAlertInfo[5]);
					} catch (Exception e) {
					}

					try {
						originAirport = (arrAlertInfo[7].trim().equalsIgnoreCase("") ? null : arrAlertInfo[7].trim());
						oriDepDate = (arrAlertInfo[8].trim().equalsIgnoreCase("") ? null : arrAlertInfo[8].trim());

						searchAirline = (arrAlertInfo[9].trim().equalsIgnoreCase("") ? null : arrAlertInfo[9].trim());

						anciReprotectStatus = (arrAlertInfo[10].trim().equalsIgnoreCase("") ? null : arrAlertInfo[10].trim());

						flightStatus = (arrAlertInfo[11].trim().equalsIgnoreCase("") ? null : arrAlertInfo[11].trim());

						if ("RESCHEDULED".equals(flightStatus)) {
							flightStatus = null;
							checkForRescheduled = true;
						}

					} catch (ArrayIndexOutOfBoundsException e) {
					}

					try {
						if (alertFromDate != null) {
							fromDate = CalendarUtil.getParsedTime(alertFromDate, DATE_FORMAT_DDMMYYYY);
						}
						if (alertToDate != null) {
							toDate = CalendarUtil.getParsedTime(alertToDate, DATE_FORMAT_DDMMYYYY);
						}
						if (depDate != null) {
							flgDate = CalendarUtil.getParsedTime(depDate, DATE_FORMAT_DDMMYYYY);
						}
						if (oriDepDate != null) {
							originalDepDate = CalendarUtil.getParsedTime(oriDepDate, DATE_FORMAT_DDMMYYYY);
						}
					} catch (ParseException e) {
						log.error("getAlertRowHtml() Invalid Date - Parse Exception" + e.getMessage());
					}

					AlertDetailDTO searchCriteria = new AlertDetailDTO();
					searchCriteria.setFromDate(fromDate);
					searchCriteria.setToDate(toDate);
					searchCriteria.setFlightNumber(flightNo);
					searchCriteria.setDepDate(flgDate);
					searchCriteria.setOriginAirport(originAirport);
					searchCriteria.setOriginDepDate(originalDepDate);
					searchCriteria.setAnciReprotectStatus(anciReprotectStatus);
					searchCriteria.setFlightStatus(flightStatus);
					searchCriteria.setCheckForRescheduled(checkForRescheduled);

					if (blnViewAnyAlerts) {
						searchCriteria.setViewAnyAlert(true);
					} else {
						if (blnViewOwnAlerts) {

							searchCriteria.setAgentCode(userPrincipal.getAgentCode());
							searchCriteria.setViewOwnAlert(true);
						} else if (blnViewRepAgentsAlerts) {

							searchCriteria.setAgentCode(userPrincipal.getAgentCode());
							searchCriteria.setViewReptAgentAlert(true);
						}
					}

					int recordNo = 1;
					if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals(""))
						recordNo = Integer.parseInt(request.getParameter("hdnRecNo"));

					int endRec = recordNo + 19;

					searchCriteria.setStartRec(recordNo);
					searchCriteria.setEndRec(endRec);

					if (StringUtils.isNotEmpty(searchAirline) && !"null".equals(searchAirline)) {
						searchCriteria.setSearchAirline(searchAirline);
					}

					Page alertDetailsPage = ModuleServiceLocator.getAirproxyAlertingBD().retrieveAlertsForSearchCriteria(
							searchCriteria, TrackInfoUtil.getBasicTrackInfo(request));

					Collection alertDetails = alertDetailsPage.getPageData();
					// int totNoOfAlerts = alertDetailsPage.getTotalNoOfRecords();
					sbData.append("var recstNo = " + alertDetailsPage.getStartPosition() + ";");
					sbData.append("var totNoOfAlerts = " + alertDetailsPage.getTotalNoOfRecords() + ";");
					sbData.append("var allAlerts = " + alertDetailsPage.getTotalNoOfRecordsInSystem() + ";");
					// messagingServiceBD.retrieveAlertsForSearchCriteria(
					// searchCriteria);
					if ((alertDetails != null) && (!alertDetails.isEmpty())) {
						it = alertDetails.iterator();
						while (it.hasNext()) {
							AlertDetailDTO alertDetailDTO = (AlertDetailDTO) it.next();
							if (alertDetailDTO != null) {

								sbData.append(arrName + "[" + count + "] = new Array();");
								sbData.append(arrName + "[" + count + "][0] = '" + count + "';");
								if (alertDetailDTO.getAlertDate() == null) {
									sbData.append(arrName + "[" + count + "][1] = '" + "" + "';");
								} else {
									sbData.append(arrName + "[" + count + "][1] = '"
											+ dateFormat.format(alertDetailDTO.getAlertDate()) + "';");
								}
								sbData.append(arrName + "[" + count + "][2] = '"
										+ ((alertDetailDTO.getFlightNumber() == null) ? "" : alertDetailDTO.getFlightNumber())
										+ "';");
								if (alertDetailDTO.getDepDate() == null) {
									sbData.append(arrName + "[" + count + "][3] = '" + "" + "';");
								} else {
									sbData.append(arrName + "[" + count + "][3] = '"
											+ dateFormat.format(alertDetailDTO.getDepDate()) + "';");
								}
								sbData.append(arrName + "[" + count + "][4] = '"
										+ ((alertDetailDTO.getPnr() == null) ? "" : alertDetailDTO.getPnr()) + "';");
								sbData.append(arrName + "[" + count + "][5] = '"
										+ ((alertDetailDTO.getContent() == null) ? "" : alertDetailDTO.getContent()) + "';");

								sbData.append(arrName + "[" + count + "][6] = '" + getAlertID(alertDetailDTO) + "';");

								sbData.append(arrName + "[" + count + "][7] = '" + alertDetailDTO.getPrivilege() + "';");
								if ((userPrivileges != null) && (!userPrivileges.isEmpty())
										&& (userPrivileges.containsKey(alertDetailDTO.getPrivilege()))) {
									sbData.append(arrName + "[" + count + "][8] = 'enable';");
								} else {
									sbData.append(arrName + "[" + count + "][8] = 'disable';");
								}
								if (alertDetailDTO.getOriginatorPnr() != null) {
									sbData.append(arrName + "[" + count + "][9] = 'true';");
								} else {
									sbData.append(arrName + "[" + count + "][9] = 'false';");
								}

								if (StringUtils.isNotEmpty(alertDetailDTO.getSearchAirline())) {
									sbData.append(arrName + "[" + count + "][10] = '" + alertDetailDTO.getSearchAirline() + "';");
								} else {
									sbData.append(arrName + "[" + count + "][10] = '" + AppSysParamsUtil.getDefaultCarrierCode()
											+ "';");
								}
								count++;
							}
						}
					}
				}

			}
		}
		sbData.append(getClientErrors(request));
		return sbData.toString();
	}

	// Appends the Carrier code to the alert id.
	private String getAlertID(AlertDetailDTO detail) {
		String alertID = detail.getAlertId().toString();
		alertID = alertID + ALERT_ID_SEPERATOR;
		if (StringUtils.isNotEmpty(detail.getSearchAirline())) {
			alertID = alertID + detail.getSearchAirline();
		} else {
			alertID = alertID + AppSysParamsUtil.getDefaultCarrierCode();
		}

		return alertID;
	}

	private final String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("cc_confirm_delete", "deleteConfirmation");
			clientErrors = "var arrError = new Array();";
			clientErrors += JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	private boolean airlineSearchAuthorised(String searchedAirline, boolean hasAnyAirlinesearchPrivilege) {
		boolean airlineSearchAuthorised = false;
		if (StringUtils.isEmpty(searchedAirline)) {
			// will be searching the own airline
			airlineSearchAuthorised = true;
		} else {
			if (AppSysParamsUtil.getDefaultCarrierCode().equals(searchedAirline)) {
				// will be searching the own airline
				airlineSearchAuthorised = true;
			} else {
				// other airline search. authorization based on privilege.
				airlineSearchAuthorised = hasAnyAirlinesearchPrivilege;
			}
		}

		return airlineSearchAuthorised;
	}
}
