package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdateOverrideChargesTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ConfirmUpdateUtil;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency, countryCurrExchangeRate\\.currency" })
public class ModifyConfirmAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ModifyConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private String selCurrency;

	private String pnr;

	private String groupPNR;

	private boolean blnNoPay = false;

	private String modifySegment;

	private FlightSearchDTO fareQuoteParams;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private String flightRPHList;

	private String version;

	private String adultCnxCharge;

	private String childCnxCharge;

	private String infantCnxCharge;

	private String defaultAdultCnxCharge;

	private String defaultChildCnxCharge;

	private String defaultInfantCnxCharge;
	private String airportMessage;

	private String flexiSelected;

	private CurrencyExchangeRate currencyExchangeRate;

	private ConfirmUpdateOverrideChargesTO adultOverideCharges;

	private ConfirmUpdateOverrideChargesTO childOverideCharges;

	private ConfirmUpdateOverrideChargesTO infantOverideCharges;

	private String resContactInfo;

	private String routeType;

	private String ownerAgentCode;

	private CurrencyExchangeRate countryCurrExchangeRate;
	private String userNote;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		if (!isGroupPNR && SYSTEM.getEnum(fareQuoteParams.getSearchSystem()) == SYSTEM.INT) {
			isGroupPNR = true;
			groupPNR = pnr;
		}

		try {
			List<FlightSegmentTO> allCnfSegments = null;

			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			// set tracking info
			TrackInfoDTO trackInfoDTO = this.getTrackInfo();

			String agentCurrency = userPrincipal.getAgentCurrencyCode();
			if (agentCurrency == null || agentCurrency.equals("")) {
				agentCurrency = AppSysParamsUtil.getBaseCurrency();
			}

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			if ((selCurrency != null && !selCurrency.equals("")) && !selCurrency.equals(agentCurrency)) {
				currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency, ApplicationEngine.XBE);
			} else {
				currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(agentCurrency, ApplicationEngine.XBE);
			}

			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);

			BigDecimal customAdultCancelCharge = null; // override these when over ride applies
			BigDecimal customInfantCancelCharge = null;
			BigDecimal customChildCancelCharge = null;
			BigDecimal defaultCustomAdultCancelCharge = null; // override these when over ride applies
			BigDecimal defaultCustomInfantCancelCharge = null;
			BigDecimal defaultCustomChildCancelCharge = null;
			boolean sendingAbsoluteCharge = false;

			// Default cnx charge state no route value will be set
			if (this.routeType != null && !this.routeType.equals("")) {
				if (adultCnxCharge != null && !adultCnxCharge.equals("") && !adultCnxCharge.equals("undefined")) {
					customAdultCancelCharge = new BigDecimal(adultCnxCharge);
					if (this.adultOverideCharges == null
							|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
									this.adultOverideCharges)) {
						sendingAbsoluteCharge = true;
					} // if not modifying using percentage
				}

				if (childCnxCharge != null && !childCnxCharge.equals("") && !childCnxCharge.equals("undefined")) {
					customChildCancelCharge = new BigDecimal(childCnxCharge);
					if (this.childOverideCharges == null
							|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
									this.childOverideCharges)) {
						sendingAbsoluteCharge = true;
					}
				}

				if (infantCnxCharge != null && !infantCnxCharge.equals("") && !infantCnxCharge.equals("undefined")) {
					customInfantCancelCharge = new BigDecimal(infantCnxCharge);
					if (this.infantOverideCharges == null
							|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
									this.infantOverideCharges)) {
						sendingAbsoluteCharge = true;
					}
				}
				if (defaultAdultCnxCharge != null && !defaultAdultCnxCharge.equals("")
						&& !defaultAdultCnxCharge.equals("undefined")) {
					defaultCustomAdultCancelCharge = new BigDecimal(defaultAdultCnxCharge);
				}
				if (defaultChildCnxCharge != null && !defaultChildCnxCharge.equals("")
						&& !defaultChildCnxCharge.equals("undefined")) {
					defaultCustomChildCancelCharge = new BigDecimal(defaultChildCnxCharge);
				}
				if (defaultInfantCnxCharge != null && !defaultInfantCnxCharge.equals("")
						&& !defaultInfantCnxCharge.equals("undefined")) {
					defaultCustomInfantCancelCharge = new BigDecimal(defaultInfantCnxCharge);
				}
			}

			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			Collection<LCCClientReservationPax> colpaxs = ReservationUtil
					.transformJsonPassengers(request.getParameter("resPaxs"));
			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(flightRPHList, fareQuoteParams);
			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo.getReservationStatus(), colsegs,
					resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);
			if (isGroupPNR) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}

			// BigDecimal totalAnci = bookingShoppingCart.getAncillaryTotalExternalCharge(true);
			Map<Integer, ReservationPaxTO> paxMap = new HashMap<Integer, ReservationPaxTO>();
			if (bookingShoppingCart.getPaxList() != null) {
				for (ReservationPaxTO pax : bookingShoppingCart.getPaxList()) {
					paxMap.put(pax.getSeqNumber(), pax);
				}
			}

			if (isGroupPNR) {

				LCCClientSegmentAssembler segmentAssembler = new LCCClientSegmentAssembler();
				segmentAssembler.setLccTransactionIdentifier(resInfo.getTransactionId());
				ExternalChargeUtil.injectOndBaggageGroupId(bookingShoppingCart.getPaxList(), flightSegmentTOs);
				BookingUtil.addSegments(segmentAssembler, flightSegmentTOs, BookingUtil.getNextSegmentSequnce(colsegs));
				segmentAssembler.setPassengerExtChargeMap(ReservationUtil.getPassengerExtChgMap(bookingShoppingCart));
				segmentAssembler.setContactInfo(ReservationUtil.transformJsonContactInfo(resContactInfo));
				// segmentAssembler.setExternalChargesMap(getExternalChargesMap(bookingShoppingCart));
				segmentAssembler.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
				segmentAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));
				segmentAssembler.setOwnerAgent(ownerAgentCode);
				BookingUtil.populateInsurance(bookingShoppingCart.getInsuranceQuotes(), segmentAssembler,
						getAdultCount(fareQuoteParams)); // adding reservation insurance

				Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs, modifySegment,
						rpParams, true);
				LCCClientResAlterModesTO resAlterQueryModesTO = LCCClientResAlterModesTO.composeModifySegmentRequest(groupPNR,
						colModSegs, colsegs, segmentAssembler, customAdultCancelCharge, customChildCancelCharge,
						customInfantCancelCharge, version, resInfo.getReservationStatus());
				resAlterQueryModesTO.setGroupPnr(isGroupPNR);
				resAlterQueryModesTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
				resAlterQueryModesTO.setSendingAbosoluteCharge(sendingAbsoluteCharge);
				resAlterQueryModesTO.setPassengers(new HashSet<LCCClientReservationPax>(colpaxs));
				resAlterQueryModesTO.setReservationStatus(resInfo.getReservationStatus());
				resAlterQueryModesTO.setUserNotes(userNote);
				resAlterQueryModesTO.setDefaultCustomAdultCharge(defaultCustomAdultCancelCharge);
				resAlterQueryModesTO.setDefaultCustomChildCharge(defaultCustomChildCancelCharge);
				resAlterQueryModesTO.setDefaultCustomInfantCharge(defaultCustomInfantCancelCharge);

				// For On hold bookings set the release timestamp
				if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
					Map<String, String> mapPrivileges = (Map<String, String>) request.getSession().getAttribute(
							WebConstants.SES_PRIVILEGE_IDS);
					Collection<LCCClientReservationSegment> allUnmodifiedLccSegments = new ArrayList<LCCClientReservationSegment>(
							colsegs);
					allUnmodifiedLccSegments.removeAll(colModSegs);
					allCnfSegments = ReservationBeanUtil.getAllCNFSegsForModify(allUnmodifiedLccSegments, flightSegmentTOs);
					Date existingRelTimestamp = null;
					if(pnr != null && !"".equals(pnr) && colpaxs.iterator().next().getZuluReleaseTimeStamp() != null){
						existingRelTimestamp = colpaxs.iterator().next().getZuluReleaseTimeStamp();
					}					
					Date releaseTimestamp = BookingUtil.getReleaseTimestamp(allCnfSegments, mapPrivileges,
							userPrincipal.getAgentCode(), bookingShoppingCart.getPriceInfoTO().getFareTypeTO(), colsegs,
							existingRelTimestamp);
					segmentAssembler.setPnrZuluReleaseTimeStamp(releaseTimestamp);
				}

				// This only applys to Cancel reservation and Modify Rerservations flows ONLY
				if (this.adultOverideCharges != null) {
					resAlterQueryModesTO.setAdultCustomChargeTO(this.createPNRDetailTO(adultOverideCharges));
				}
				if (this.infantOverideCharges != null) {
					resAlterQueryModesTO.setInfantCustomChargeTO(this.createPNRDetailTO(infantOverideCharges));
				}
				if (this.childOverideCharges != null) {
					resAlterQueryModesTO.setChildCustomChargeTO(this.createPNRDetailTO(childOverideCharges));
				}

				resAlterQueryModesTO.setRouteType(this.routeType);

				resAlterQueryModesTO.setVersion(ReservationCommonUtil.getCorrectInterlineVersion(resAlterQueryModesTO
						.getVersion()));

				ReservationBalanceTO reservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
						.getResAlterationBalanceSummary(resAlterQueryModesTO, trackInfoDTO);
				// BookingUtil.injectExtChgToBalanceTO(lCCClientReservationBalanceTO, bookingShoppingCart);
				BigDecimal totalAmountDue = reservationBalanceTO.getTotalAmountDue();
				if (reservationBalanceTO.getVersion() != null) {
					resAlterQueryModesTO.setVersion(reservationBalanceTO.getVersion());
				}
				// BigDecimal totalAmountDue = BookingUtil.getTotalAmoutDue(lCCClientReservationBalanceTO,
				// bookingShoppingCart);
				// totalAmountDue = AccelAeroCalculator.add(totalAmountDue, totalAnci);
				// if(totalAmountDue.compareTo(BigDecimal.ZERO) == 1 && !rpParams.isPartialPaymentModify()){ //TODO
				// check why this privilege is checked
				if (totalAmountDue.compareTo(BigDecimal.ZERO) == 1) {
					bookingShoppingCart.setReservationBalance(reservationBalanceTO);
					bookingShoppingCart.setReservationAlterModesTO(resAlterQueryModesTO);
					bookingShoppingCart.setPaidAmount(reservationBalanceTO.getTotalPaidAmount());
					Object[] paySumObj = ConfirmUpdateUtil.getPaxPaySummaryList(reservationBalanceTO, colpaxs, false);
					passengerPayment = (Collection<PaymentPassengerTO>) paySumObj[0];
					// Add the credit used here so that payment sum only have the balance to pay
					bookingShoppingCart.setUsedPaxCredit((BigDecimal) paySumObj[1]);
					paymentSummaryTO = PaymentUtil
							.createInterlinePaymentSummary(bookingShoppingCart, selCurrency, exchangeRateProxy,
									userPrincipal.getAgentCode());

					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShoppingCart);
				} else {
					addExtChargesWithDummyPayments(reservationBalanceTO, segmentAssembler, isGroupPNR);
					resAlterQueryModesTO.setActualPayment(false);
					ModuleServiceLocator.getAirproxySegmentBD().modifySegments(resAlterQueryModesTO,
							AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), getClientInfoDTO(),
							trackInfoDTO);
					this.blnNoPay = true;
				}

			} else {
				// Create the price Quote Request
				FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, outFlightRPHList,
						retFlightRPHList);

				// Inject the existing segment informationpaxMap
				String pnr = request.getParameter("pnr");
				if (pnr != null && !"".equals(pnr)) {
					ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"),
							request.getParameter("modifySegment"));
				}

				// Prepare the Segment Assembler
				LCCClientSegmentAssembler segmentAssembler = new LCCClientSegmentAssembler();

				ExternalChargeUtil.injectOndBaggageGroupId(bookingShoppingCart.getPaxList(), flightSegmentTOs);
				BookingUtil.addSegments(segmentAssembler, flightSegmentTOs, BookingUtil.getNextSegmentSequnce(colsegs)); // adding
																															// flight
																															// segments

				List<FlightSegmentTO> allFlightSegmentsList = AirProxyReservationUtil.populateFlightSegmentTO(colsegs);

				SSRServicesUtil.setSegmentSeqForSSR((List<ReservationPaxTO>) bookingShoppingCart.getPaxList(), flightSegmentTOs,
						allFlightSegmentsList.size());

				segmentAssembler.setPassengerExtChargeMap(ReservationUtil.getPassengerExtChgMap(bookingShoppingCart));
				segmentAssembler.setContactInfo(ReservationUtil.transformJsonContactInfo(resContactInfo));
				// External charges DTO map(containing charge id,etc) -not the currentbooking ext charge values
				segmentAssembler.setExternalChargesMap(getExternalChargesMap(bookingShoppingCart));
				segmentAssembler.setFlightPriceRQ(flightPriceRQ); // flightPriceRQ
				// tnx id initiated for this flow
				segmentAssembler.setLccTransactionIdentifier(resInfo.getTransactionId());
				// selected fare seg charge to (required only for the own airline)
				segmentAssembler.setSelectedFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
				segmentAssembler.setLastFareQuotedDate(bookingShoppingCart.getPriceInfoTO().getLastFareQuotedDate());
				segmentAssembler.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
				segmentAssembler.setFlexiSelected(ReservationUtil.isFlexiSelected(flexiSelected)); // is flexi selected
				BookingUtil.populateInsurance(bookingShoppingCart.getInsuranceQuotes(), segmentAssembler,
						getAdultCount(fareQuoteParams)); // adding reservation insurance

				// Get the valid modifying segments
				Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs, modifySegment,
						rpParams, true);

				// For On hold bookings set the release timestamp
				if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
					Map<String, String> mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
					colsegs.removeAll(colModSegs);
					allCnfSegments = ReservationBeanUtil.getAllCNFSegsForModify(colsegs, flightSegmentTOs);
					Date existingRelTimestamp = null;
					if(pnr != null && !"".equals(pnr) && colpaxs.iterator().next().getZuluReleaseTimeStamp() != null){
						existingRelTimestamp = colpaxs.iterator().next().getZuluReleaseTimeStamp();
					}
					Date releaseTimestamp = BookingUtil.getReleaseTimestamp(allCnfSegments, mapPrivileges,
							userPrincipal.getAgentCode(), bookingShoppingCart.getPriceInfoTO().getFareTypeTO(), colsegs,
							existingRelTimestamp);
					segmentAssembler.setPnrZuluReleaseTimeStamp(releaseTimestamp);
				}

				// Prepare the Modify segment request
				LCCClientResAlterModesTO resAlterModesTO = LCCClientResAlterModesTO.composeModifySegmentRequest(pnr, colModSegs,
						null, segmentAssembler, customAdultCancelCharge, customChildCancelCharge, customInfantCancelCharge,
						version, resInfo.getReservationStatus());

				resAlterModesTO.setGroupPnr(isGroupPNR);
				resAlterModesTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
				resAlterModesTO.setSendingAbosoluteCharge(sendingAbsoluteCharge);
				resAlterModesTO.setUserNotes(userNote);
				resAlterModesTO.setDefaultCustomAdultCharge(defaultCustomAdultCancelCharge);
				resAlterModesTO.setDefaultCustomChildCharge(defaultCustomChildCancelCharge);
				resAlterModesTO.setDefaultCustomInfantCharge(defaultCustomInfantCancelCharge);

				// This only applys to Cancel reservation and Modify Rerservations flows ONLY
				if (this.adultOverideCharges != null) {
					resAlterModesTO.setAdultCustomChargeTO(this.createPNRDetailTO(adultOverideCharges));
				}
				if (this.infantOverideCharges != null) {
					resAlterModesTO.setInfantCustomChargeTO(this.createPNRDetailTO(infantOverideCharges));
				}
				if (this.childOverideCharges != null) {
					resAlterModesTO.setChildCustomChargeTO(this.createPNRDetailTO(childOverideCharges));
				}

				resAlterModesTO.setRouteType(this.routeType);
				// TODO have the selected ancillary in the RQ and perform the balance calculation at the back end

				// Retrive the balance to pay
				ReservationBalanceTO reservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
						.getResAlterationBalanceSummary(resAlterModesTO, trackInfoDTO);

				// Hack to inject the ancillary charges to balance pay -- TODO clean up and include the ancillary
				// balances in the back end
				// BigDecimal totalAmountDue = BookingUtil.getTotalAmoutDue(reservationBalanceTO, bookingShoppingCart);
				BigDecimal totalAmountDue = reservationBalanceTO.getTotalAmountDue();
				if (totalAmountDue.compareTo(BigDecimal.ZERO) == 0
						&& !resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
					// If no balance to pay
					Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;
					resAlterModesTO.setPaymentTypes(intPaymentMode);
					resAlterModesTO.getLccClientSegmentAssembler().setPnrZuluReleaseTimeStamp(null);
					resAlterModesTO.setActualPayment(false);
					addExtChargesWithDummyPayments(reservationBalanceTO, segmentAssembler, isGroupPNR);
					ModuleServiceLocator.getAirproxySegmentBD().modifySegments(resAlterModesTO,
							AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), getClientInfoDTO(),
							trackInfoDTO);
					this.blnNoPay = true;
					// clearing the block seats after modification success.
					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SESSION_BLOCK_IDS, null);
				} else { // has a balance to pay
					// set the balance calculation related RQ/RS data to the session
					bookingShoppingCart.setReservationBalance(reservationBalanceTO);
					bookingShoppingCart.setReservationAlterModesTO(resAlterModesTO);
					bookingShoppingCart.setPaidAmount(reservationBalanceTO.getTotalPaidAmount());

					Object[] paySumObj = ConfirmUpdateUtil.getPaxPaySummaryList(reservationBalanceTO, colpaxs, false);
					passengerPayment = (Collection<PaymentPassengerTO>) paySumObj[0];
					// Add the credit used here so that payment sum only have the balance to pay
					bookingShoppingCart.setUsedPaxCredit((BigDecimal) paySumObj[1]);
					String pendingCommission = CommonUtil.getPendingAgentCommissions(userPrincipal.getAgentCode(), pnr);
					paymentSummaryTO = PaymentUtil.createPaymentSummary(bookingShoppingCart, selCurrency, exchangeRateProxy,
							pendingCommission, userPrincipal.getAgentCode());

					request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART, bookingShoppingCart);
				}
			}

			if (this.countryCurrExchangeRate != null && this.paymentSummaryTO != null) {
				this.paymentSummaryTO.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
			}

			// LOAD Airport Messages;
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessagesForFlightSegmentWise(flightSegmentTOs,
					ReservationInternalConstants.AirportMessageSalesChannel.XBE,
					ReservationInternalConstants.AirportMessageStages.CARD_PAYMENT, null);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("Generic exception caught.", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private void addExtChargesWithDummyPayments(ReservationBalanceTO reservationBalanceTO, LCCClientSegmentAssembler segmentAsm,
			boolean isGroupPNR) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = segmentAsm.getPassengerExtChargeMap();
		if (paxExtChgMap != null && paxExtChgMap.size() > 0) {
			for (LCCClientPassengerSummaryTO pax : reservationBalanceTO.getPassengerSummaryList()) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(pax.getTravelerRefNumber());
				if (paxExtChgMap.containsKey(paxSeq)) {
					LCCClientPaymentAssembler payAsm = new LCCClientPaymentAssembler();

					List<LCCClientExternalChgDTO> paxExternalCharge = paxExtChgMap.get(paxSeq);
					// mark the external chargers already consume as we do not need any external payment for this case
					for (LCCClientExternalChgDTO chgDTO : paxExternalCharge) {
						chgDTO.setAmountConsumedForPayment(true);
					}
					payAsm.getPerPaxExternalCharges().addAll(paxExternalCharge);

					if (PaxTypeTO.ADULT.equals(pax.getPaxType())
							&& (pax.getInfantName() != null && !"".equals(pax.getInfantName()))) {
						payAsm.setPaxType(PaxTypeTO.PARENT);
					} else {
						payAsm.setPaxType(pax.getPaxType());
					}

					/**
					 * In order to record the payment breakdown for refundable we need to have a payment. in this case a
					 * zero payment coz all the chargers will be payed by refundables itself. No external payment is
					 * required
					 */
					payAsm.addCashPayment(BigDecimal.ZERO, null, new Date(), null, null, null, null);
					if (isGroupPNR) {
						segmentAsm.addPassengerPayments(paxSeq, payAsm);
					} else {
						segmentAsm.addPassengerPayments(PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber()), payAsm);
					}
				}
			}
		}
	}

	private int getAdultCount(FlightSearchDTO fltSearchDTO) {
		return (fltSearchDTO.getAdultCount() + fltSearchDTO.getChildCount());
	}

	private PnrChargeDetailTO createPNRDetailTO(ConfirmUpdateOverrideChargesTO chargesTO) {
		PnrChargeDetailTO chargeDetailTO = new PnrChargeDetailTO();
		if (chargesTO != null) {
			chargeDetailTO.setModificationChargeType(chargesTO.getChargeType());
			if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMin())) {
				chargeDetailTO.setMinModificationAmount(new BigDecimal(chargesTO.getMin()));
			}
			if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMax())) {
				chargeDetailTO.setMaximumModificationAmount(new BigDecimal(chargesTO.getMax()));
			}
		} else {
			return null;
		}
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getPrecentageVal())) {
			chargeDetailTO.setChargePercentage(new BigDecimal(chargesTO.getPrecentageVal()));
		}
		return chargeDetailTO;
	}

	private Map getExternalChargesMap(BookingShoppingCart bookingShoppingCart) throws ModuleException {
		Collection<ReservationPaxTO> paxList = bookingShoppingCart.getPaxList();
		Map selectedChargers = bookingShoppingCart.getSelectedExternalCharges();

		Set<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		if (paxList != null) {
			for (ReservationPaxTO pax : paxList) {
				for (LCCClientExternalChgDTO extChg : pax.getExternalCharges()) {
					colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
				}

			}
		}
		if (selectedChargers != null) {
			Set<EXTERNAL_CHARGES> tmpKeySet = selectedChargers.keySet();
			for (EXTERNAL_CHARGES key : tmpKeySet) {
				colEXTERNAL_CHARGES.add(key);
			}
		}
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_OTHER);

		Map extExternalChgDTOMap = new HashMap();
		if (colEXTERNAL_CHARGES.size() > 0) {
			extExternalChgDTOMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
					ChargeRateOperationType.MODIFY_ONLY);
		}
		return extExternalChgDTOMap;
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public void setSelCurrency(String selCurrency) {
		this.selCurrency = selCurrency;
	}

	public boolean isBlnNoPay() {
		return blnNoPay;
	}

	public String getPnr() {
		return pnr;
	}

	public String getModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(String modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setAdultCnxCharge(String adultCnxCharge) {
		this.adultCnxCharge = adultCnxCharge;
	}

	public void setChildCnxCharge(String childCnxCharge) {
		this.childCnxCharge = childCnxCharge;
	}

	public void setInfantCnxCharge(String infantCnxCharge) {
		this.infantCnxCharge = infantCnxCharge;
	}

	public String getDefaultAdultCnxCharge() {
		return defaultAdultCnxCharge;
	}

	public void setDefaultAdultCnxCharge(String defaultAdultCnxCharge) {
		this.defaultAdultCnxCharge = defaultAdultCnxCharge;
	}

	public String getDefaultChildCnxCharge() {
		return defaultChildCnxCharge;
	}

	public void setDefaultChildCnxCharge(String defaultChildCnxCharge) {
		this.defaultChildCnxCharge = defaultChildCnxCharge;
	}

	public String getDefaultInfantCnxCharge() {
		return defaultInfantCnxCharge;
	}

	public void setDefaultInfantCnxCharge(String defaultInfantCnxCharge) {
		this.defaultInfantCnxCharge = defaultInfantCnxCharge;
	}
	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public String getFlexiSelected() {
		return flexiSelected;
	}

	public void setFlexiSelected(String flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public ConfirmUpdateOverrideChargesTO getAdultOverideCharges() {
		return adultOverideCharges;
	}

	public void setAdultOverideCharges(ConfirmUpdateOverrideChargesTO adultOverideCharges) {
		this.adultOverideCharges = adultOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getChildOverideCharges() {
		return childOverideCharges;
	}

	public void setChildOverideCharges(ConfirmUpdateOverrideChargesTO childOverideCharges) {
		this.childOverideCharges = childOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getInfantOverideCharges() {
		return infantOverideCharges;
	}

	public void setInfantOverideCharges(ConfirmUpdateOverrideChargesTO infantOverideCharges) {
		this.infantOverideCharges = infantOverideCharges;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public String getAdultCnxCharge() {
		return adultCnxCharge;
	}

	public String getChildCnxCharge() {
		return childCnxCharge;
	}

	public String getInfantCnxCharge() {
		return infantCnxCharge;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		if (ownerAgentCode != null && !"".equals(ownerAgentCode.trim()) && !"undefined".equals(ownerAgentCode.trim())
				&& !"null".equals(ownerAgentCode.trim())) {
			this.ownerAgentCode = ownerAgentCode;
		}
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

}
