package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action for duplicate name check in name change
 * 
 * @author rumesh
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class DuplicateNameCheckForModifyAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(DuplicateNameCheckForModifyAction.class);

	private boolean success = true;

	private String messageTxt;

	private String pnr;

	private String resSegments;

	private List<AdultPaxTO> adultPaxTOList;

	private boolean hasDuplicates = false;

	private String system;

	public String execute() {
		
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			List<FlightSegmentTO> flightSegmentTOs = AirProxyReservationUtil.populateFlightSegmentTO(colsegs);

			List<LCCClientReservationPax> lstAdultPax = ReservationUtil.populatePaxAdults(adultPaxTOList);

			DuplicateValidatorAssembler dva = new DuplicateValidatorAssembler();
			dva.setTargetSystem(SYSTEM.getEnum(this.system));
			dva.setFlightSegments(flightSegmentTOs);
			dva.setPaxList(lstAdultPax);
			dva.setPnr(this.pnr);
			this.hasDuplicates = ModuleServiceLocator.getAirproxyReservationBD().checkForDuplicates(dva, null , getTrackInfo());
			resInfo.setDuplicates(hasDuplicates);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Duplicate Name Check Error", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getResSegments() {
		return resSegments;
	}

	public void setResSegments(String resSegments) {
		this.resSegments = resSegments;
	}

	public List<AdultPaxTO> getAdultPaxTOList() {
		return adultPaxTOList;
	}

	public void setAdultPaxTOList(List<AdultPaxTO> adultPaxTOList) {
		this.adultPaxTOList = adultPaxTOList;
	}

	public boolean isHasDuplicates() {
		return hasDuplicates;
	}

	public void setHasDuplicates(boolean hasDuplicates) {
		this.hasDuplicates = hasDuplicates;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}
}
