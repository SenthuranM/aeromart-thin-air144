/**
 * 
 */
package com.isa.thinair.xbe.core.web.action.customer;




import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;
import org.json.JSONArray;
import org.json.JSONObject;

import com.ibm.icu.text.SimpleDateFormat;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.LoyaltyNameValidator;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CustomerQuestionaireDTO;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.xbe.api.dto.v2.CustomerDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.CustomerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author suneth
 *
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class RegisterCustomerAction extends BaseRequestAwareAction{
	
	private static Log log = LogFactory.getLog(RegisterCustomerAction.class);

	private CustomerDTO customer;
	
	private LmsMemberDTO lmsDetails = null;

	private String optLMS = "N";
	
	private String carrierCode = "";

	private Map<String, String> errorInfo;
	
	private boolean registration = false;

	private boolean accountExists = false;

	private boolean lmsNameMismatch = false;

	private boolean skipNameMatching = false;
	
	private boolean lmsJoinNotAllowed = false;

	private List<CustomerQuestionaireDTO> questionaire = null;

	private String jsonQuestionaire;

	private String ffid;

	private String success;

	private String emailId;
	
	public String execute(){
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		success = S2Constants.Result.SUCCESS;
		String generatedPass = UUID.randomUUID().toString().substring(0, 8);
		
		try {

			customer.setPassword(generatedPass);
			
			questionaire = getQuestionaireFromJson(jsonQuestionaire);
			
			if (questionaire != null) {
				customer.setCustomerQuestionaireDTO(questionaire);
			}
			
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			Customer modelCustomer = CustomerUtil.customerDtoToModel(customer);
			LoyaltyCustomerProfile loyaltyProfile = null;

			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			
			modelCustomer.setIsXBECustomer('Y');
			customerDelegate.registerCustomer(modelCustomer , carrierCode , loyaltyProfile, false);

			int customerId = customerDelegate.getCustomer(modelCustomer.getEmailId()).getCustomerId();
			
			if (AppSysParamsUtil.isLMSEnabled()){
				if (optLMS.equals("Y")) {

					lmsDetails.setAppCode("APP_XBE");
					
					LmsMember lmsMember = CustomerUtil.getModelLmsMember(lmsDetails , customerId);
					setEnrollingChannelIntRef(userPrincipal, lmsMember);
					lmsMember.setEnrollingCarrier(AppSysParamsUtil.getDefaultCarrierCode());
					LmsCommonUtil.lmsEnroll(lmsMember, carrierCode, loyaltyManagementBD, lmsMemberDelegate,
							new Locale(lmsMember.getLanguage()), false);
					lmsDetails.setEmailStatus("N");
					
				}
			}

			this.customer = null;
			
		} catch (Exception e) {
			// TODO: handle exception
			success = S2Constants.Result.ERROR;
			log.error("RegisterCustomerActionError-->  " + e);

		}
		
		return success;
		
	}
	
	public String validateHeadReffered() throws ModuleException{
		
		if (AppSysParamsUtil.isLMSEnabled() && optLMS.equals("Y")){
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
			LmsMemberServiceBD lmsMemberServiceBD = ModuleServiceLocator.getLmsMemberServiceBD();
			
			if(lmsDetails.getRefferedEmail()!=null && !lmsDetails.getRefferedEmail().equals("")){
				if(!LmsCommonUtil.isNonMandAccountAvailable(lmsDetails.getRefferedEmail(), loyaltyManagementBD)){
					lmsDetails.setRefferedEmail("N");
				}
			}
			if(lmsDetails.getHeadOFEmailId()!=null && !lmsDetails.getHeadOFEmailId().equals("")){
				LoyaltyMemberCoreDTO member = loyaltyManagementBD.getLoyaltyMemberCoreDetails(lmsDetails.getHeadOFEmailId());
				if(!LmsCommonUtil.isValidFamilyHeadAccount(lmsDetails.getHeadOFEmailId(), member)){
					lmsDetails.setHeadOFEmailId("N");
				}
			}
			if(lmsDetails.getEmailId()!=null && !lmsDetails.getEmailId().equals("")){
	
				Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(lmsDetails.getEmailId());
	
				if (registration) {
					if (customer != null) {
						accountExists = true;
						return S2Constants.Result.SUCCESS;
					}
				}
	
				if (customer != null) {
					if (lmsDetails.getFirstName() == null || "".equals(lmsDetails.getFirstName())) {
						lmsDetails.setFirstName(customer.getFirstName());
					}
					if (lmsDetails.getLastName() == null || "".equals(lmsDetails.getLastName())) {
						lmsDetails.setLastName(customer.getLastName());
					}
					if (BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_NAME_EDIT)) {
						return S2Constants.Result.SUCCESS;
					}
				}
	
				if (!skipNameMatching && !CustomerUtil.isExistingLMSNameMatches(lmsDetails)) {
					
					lmsNameMismatch = true;
					
					if (!registration) {
						
						lmsJoinNotAllowed = true;
						
					}
					
					return S2Constants.Result.SUCCESS;
					
				}
	
				if(!LmsCommonUtil.availableForCarrier(lmsDetails.getEmailId(), lmsMemberServiceBD)){
					if(LmsCommonUtil.isNonMandAccountAvailable(lmsDetails.getEmailId(), loyaltyManagementBD)){
						lmsDetails.setEmailId("Y");
					}
				}
			}
		}else{
			
			Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(lmsDetails.getEmailId());
			
			if (registration) {
				if (customer != null) {
					accountExists = true;
					return S2Constants.Result.SUCCESS;
				}
			}
			
		}
		
		return S2Constants.Result.SUCCESS;
		
	}
	
	public String updateCustomer() throws ModuleException{

		String forward = S2Constants.Result.SUCCESS;
		success = S2Constants.Result.SUCCESS;
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
			LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();

			Customer tempCustomer = ModuleServiceLocator.getCustomerBD().getCustomer(customer.getEmailId());
			
			questionaire = getQuestionaireFromJson(jsonQuestionaire);
			customer.setCustomerQuestionaireDTO(questionaire);
			customer.setCustomerID(tempCustomer.getCustomerId());
			customer.setPassword(tempCustomer.getPassword());

			Customer updatedCustomer = CustomerUtil.customerDtoToModel(customer);

			boolean isNameUpdated = false;
			
			if (AppSysParamsUtil.isLMSEnabled() && optLMS.equals("Y")){
				if (tempCustomer.getLMSMemberDetails() != null) {
					
					LoyaltyMemberCoreDTO loyaltyMemberCoreDTO = loyaltyManagementBD.getLoyaltyMemberCoreDetails(tempCustomer
							.getLMSMemberDetails().getFfid());
					
					LmsMember lmsMember = lmsMemberDelegate.getLmsMember(customer.getEmailId());
					lmsMember.setResidencyCode(customer.getCountryCode());
					lmsMember.setNationalityCode(customer.getNationalityCode());
					lmsMember.setMobileNumber(LmsCommonUtil.getValidPhoneNumber(customer.getMobile()));
					lmsMember.setPhoneNumber(LmsCommonUtil.getValidPhoneNumber(customer.getTelephone()));
					lmsMember.setLanguage(lmsDetails.getLanguage());
					lmsMember.setHeadOFEmailId(lmsDetails.getHeadOFEmailId());
					lmsMember.setRefferedEmail(lmsDetails.getRefferedEmail());
					lmsMember.setPassportNum(lmsDetails.getPassportNum());

					if ((!updatedCustomer.getFirstName().equalsIgnoreCase(loyaltyMemberCoreDTO.getMemberFirstName()) || !updatedCustomer
							.getLastName().equalsIgnoreCase(loyaltyMemberCoreDTO.getMemberLastName()))
							&& BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_NAME_EDIT)) {
						lmsMember.setFirstName(updatedCustomer.getFirstName());
						lmsMember.setLastName(updatedCustomer.getLastName());
						isNameUpdated = true;
					}
	
					// 10/01/1983
					if (lmsDetails.getDateOfBirth() != null && !"".equals(lmsDetails.getDateOfBirth().trim())) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						lmsMember.setDateOfBirth(sdf.parse(lmsDetails.getDateOfBirth()));
					}
					
					if(lmsDetails.getHeadOFEmailId() != null && !"".equals(lmsDetails.getHeadOFEmailId().trim())){
						LmsMember headOfEmailFFID = lmsMemberDelegate.getLmsMember(lmsDetails.getHeadOFEmailId());
						if(headOfEmailFFID == null){
							success = S2Constants.Result.ERROR;
						}
					}
	
					if (lmsDetails.getRefferedEmail() != null && !"".equals(lmsDetails.getRefferedEmail().trim())) {
						lmsMember.setRefferedEmail(lmsDetails.getRefferedEmail());
					}
	
					if(success.equals(S2Constants.Result.SUCCESS)){
						Set<LmsMember> lmsMembers = new HashSet<LmsMember>();
						lmsMembers.add(lmsMember);
						updatedCustomer.setCustomerLmsDetails(lmsMembers);
						updatedCustomer.setIsLmsMember('Y');
						loyaltyManagementBD.updateMemberProfile(updatedCustomer);
					}
				}else{
					
					lmsDetails.setAppCode("APP_XBE");
					
					LmsMember lmsMember = CustomerUtil.getModelLmsMember(lmsDetails , tempCustomer.getCustomerId());
					setEnrollingChannelIntRef(userPrincipal, lmsMember);
					lmsMember.setEnrollingCarrier(AppSysParamsUtil.getDefaultCarrierCode());
					LmsCommonUtil.lmsEnroll(lmsMember, carrierCode, loyaltyManagementBD, lmsMemberDelegate, new Locale(
							lmsMember.getLanguage()), false);
					lmsDetails.setEmailStatus("N");
					
				}
			}

			if (success.equals(S2Constants.Result.SUCCESS)) {

				if (isNameUpdated) {
					customerDelegate.saveOrUpdateWithName(tempCustomer, updatedCustomer, userPrincipal.getUserId());
					if (!LoyaltyNameValidator.compareNames(updatedCustomer.getFirstName(), updatedCustomer.getLastName(),
							tempCustomer.getFirstName(), tempCustomer.getLastName())) {
						ModuleServiceLocator.getReservationBD().removeFFIDFromUnflownPNRs(updatedCustomer.getEmailId(),
								getTrackInfo());
					}
					if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
						String ffid = updatedCustomer.getLMSMemberDetails().getFfid();
						String title = updatedCustomer.getTitle();
						String firstName = updatedCustomer.getFirstName();
						String lastName = updatedCustomer.getLastName();
						loyaltyManagementBD.customerNameUpdate(ffid, getTrackInfo(), title, firstName, lastName);
					} else if (!LoyaltyNameValidator.compareNames(updatedCustomer.getFirstName(), updatedCustomer.getLastName(),
							tempCustomer.getFirstName(), tempCustomer.getLastName())) {
						ModuleServiceLocator.getReservationBD().removeFFIDFromUnflownPNRs(forward, getTrackInfo());
					}
				} else {
					customerDelegate.saveOrUpdate(updatedCustomer);
				}
			}

		} catch (ModuleException me) {
			if(!me.getExceptionCode().equals("lms.data.pass")){
				success = S2Constants.Result.ERROR;
			}
			log.error("CustomerProfileUpdateAction==>", me);
		} catch (Exception ex) {
			success = S2Constants.Result.ERROR;
			log.error("CustomerProfileUpdateAction==>", ex);
		}
		return forward;
		
	}

	private void setEnrollingChannelIntRef(UserPrincipal userPrincipal, LmsMember lmsMember) {
		
		String enrollingChannelIntRef = userPrincipal.getAgentCode()+"/"+userPrincipal.getUserId();
		
		lmsMember.setEnrollingChannelIntRef(enrollingChannelIntRef);
	}
	
	public List<CustomerQuestionaireDTO> getQuestionaireFromJson(String jsonQuestionaire){
		
		List<CustomerQuestionaireDTO> questionaire = null;
		
		if(!jsonQuestionaire.equals("")){
			
			try{
				
				questionaire = new ArrayList<CustomerQuestionaireDTO>();
				
				JSONArray jsonArray = new JSONArray(jsonQuestionaire); // JSONArray is from the json.org library
				
				for (int i = 0; i < jsonArray.length(); i++) {
					
				    JSONObject innerJsonObject = (JSONObject) jsonArray.get(i);

				    CustomerQuestionaireDTO customerQuerstionaireDTO = new CustomerQuestionaireDTO();
				    
				    customerQuerstionaireDTO.setQuestionAnswer(innerJsonObject.getString("questionAnswer"));
				    customerQuerstionaireDTO.setQuestionKey(String.valueOf(i + 1));
				    
				    questionaire.add(customerQuerstionaireDTO);			    
				    
				}
				
			}catch(Exception e){
				
				
				
			}
			
			
		}
		
		return questionaire;
	}

	public String sendlmsConfEmail() {
		String success = S2Constants.Result.SUCCESS;
		LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
		LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
		try {
			LmsMember lmsMember = lmsMemberDelegate.getLmsMember(ffid);
			String validationString = lmsMemberDelegate.getVerificationString(lmsMember.getFfid());
			LmsCommonUtil.resendLmsConfEmail(lmsMember, carrierCode, validationString, lmsMemberDelegate, loyaltyManagementBD,
					new Locale(lmsMember.getLanguage()), false);
		} catch (ModuleException me) {
			log.error("RegisterCustomerAction==>", me);
		}
		return success;
	}

	public String resendProfileDetailsMail() {
		String success = S2Constants.Result.SUCCESS;
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		try {
			customerDelegate.resendProfileDetailsMail(emailId, carrierCode);
		} catch (ModuleException me) {
			log.error("RegisterCustomerAction==>", me);
		}
		return success;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public void setOptLMS(String optLMS) {
		this.optLMS = optLMS;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(Map<String, String> errorInfo) {
		this.errorInfo = errorInfo;
	}

	public boolean isLmsNameMismatch() {
		return lmsNameMismatch;
	}

	public void setLmsNameMismatch(boolean lmsNameMismatch) {
		this.lmsNameMismatch = lmsNameMismatch;
	}

	public void setRegistration(boolean registration) {
		this.registration = registration;
	}

	public boolean isAccountExists() {
		return accountExists;
	}

	public void setAccountExists(boolean accountExists) {
		this.accountExists = accountExists;
	}

	public void setSkipNameMatching(boolean skipNameMatching) {
		this.skipNameMatching = skipNameMatching;
	}
	
	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public void setQuestionaire(List<CustomerQuestionaireDTO> questionaire) {
		this.questionaire = questionaire;
	}
	
	public void setJsonQuestionaire(String jsonQuestionaire) {
		this.jsonQuestionaire = jsonQuestionaire;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public boolean isLmsJoinNotAllowed() {
		return lmsJoinNotAllowed;
	}

	public void setLmsJoinNotAllowed(boolean lmsJoinNotAllowed) {
		this.lmsJoinNotAllowed = lmsJoinNotAllowed;
	}
	
}
