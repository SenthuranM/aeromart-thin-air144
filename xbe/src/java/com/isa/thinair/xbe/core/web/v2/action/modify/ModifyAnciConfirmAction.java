package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.ssr.MedicalSsrDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.HandlingFeeCalculationUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationPaxUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.reservation.ServiceTaxCCParamsDTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.PaxCreditFETO;
import com.isa.thinair.xbe.api.dto.v2.PaxUsedCreditInfoTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.xbe.core.web.v2.util.StringUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "", params = { "excludeProperties",
		"currencyExchangeRate\\.currency, countryCurrExchangeRate\\.currency" })
public class ModifyAnciConfirmAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ModifyAnciConfirmAction.class);

	private boolean success = true;

	private String messageTxt;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private String selCurrency;

	private String pnr;

	private String groupPNR;

	private String insurableFltRefNumbers;

	private boolean blnNoPay = false;

	private String modifySegment;

	private FlightSearchDTO fareQuoteParams;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private String flightRPHList;

	private String version;

	private String resPaySummary;

	private String paxWiseAnci;

	private String insurances;

	private String airportMessage;

	private CurrencyExchangeRate currencyExchangeRate;

	private String modifedSegmentsCount;

	private CurrencyExchangeRate countryCurrExchangeRate;

	private String resPaxPayments;

	private boolean csPnr = false;

	public String execute() {

		@SuppressWarnings("unused")
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			SYSTEM system = SYSTEM.INT;
			if (!isGroupPNR) {
				system = SYSTEM.AA;
				groupPNR = pnr;
			}

			PaymentSummaryTO paySummary = getPaymentSummaryDTO(resPaySummary);
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);

			CommonReservationContactInfo contactInfo = ReservationUtil
					.transformJsonContactInfo(request.getParameter("resContactInfo"));
			BigDecimal serviceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			// tracking info
			TrackInfoDTO trackingfoDTO = this.getTrackInfo();

			UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
			String strCurrency = up.getAgentCurrencyCode();
			if (strCurrency == null || strCurrency.equals("")) {
				strCurrency = AppSysParamsUtil.getBaseCurrency();
			}
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(strCurrency, ApplicationEngine.XBE);
			this.countryCurrExchangeRate = PaymentUtil.getBSPCountryCurrencyExchgRate(request);

			FltSegBuilder fltSegBuilder = new FltSegBuilder(flightRPHList);
			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			SortUtil.sortFlightSegByDepDate(flightSegmentTOs);

			List<LCCInsuranceQuotationDTO> insuranceQuotations = AncillaryJSONUtil.getSelectedInsuranceQuotations(insurances,
					flightSegmentTOs, insurableFltRefNumbers);

			if (insuranceQuotations != null && !insuranceQuotations.isEmpty()) {

				BigDecimal insuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCInsuranceQuotationDTO insuranceQuotation : insuranceQuotations) {
					insuranceAmount = AccelAeroCalculator.add(insuranceAmount, insuranceQuotation.getQuotedTotalPremiumAmount());
				}

				ExternalChgDTO insChgDTO = new ExternalChgDTO();
				insChgDTO.setAmount(insuranceAmount);
				insChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.INSURANCE);

				bookingShoppingCart.addSelectedExternalCharge(insChgDTO);
				bookingShoppingCart.setInsuranceQuotes(insuranceQuotations);

			} else {
				bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.INSURANCE);
				bookingShoppingCart.setInsuranceQuotes(null);
			}

			List<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotations,
					ApplicationEngine.XBE, flightSegmentTOs, null, null);

			SSRServicesUtil.checkMedicalSsrInAllSegments(paxList, getFlightSegmentsWithoutBusSegments(flightSegmentTOs),
					ReservationInternalConstants.SsrTypes.MEDA);

			SSRServicesUtil.setSegmentSeqForSSR(paxList, flightSegmentTOs, null);
			bookingShoppingCart.setPaxList(paxList);

			// calculate & apply handling fee for passengers
			calculateAndApplyHandlingFeeForPax(bookingShoppingCart, flightSegmentTOs);

			BookingUtil.applyAncillaryTax(bookingShoppingCart, flightSegmentTOs.get(0), null);

			serviceTaxAmount = BookingUtil.applyServiceTax(bookingShoppingCart.getPaxList(), flightSegmentTOs, contactInfo,
					getTrackInfo(), system);

			Map<Integer, BigDecimal> paxResCreditMap = resInfo.getPaxCreditMap();

			Map<Integer, PaymentPassengerTO> payPaxMap = paxPayExtract(paxWiseAnci);
			int adultChildCount = 0;
			for (ReservationPaxTO pax : paxList) {
				if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					adultChildCount++;
				}
			}
			boolean isCCPayment = false;

			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {
				isCCPayment = true;
			} else {
				isCCPayment = false;
			}

			ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList,
					bookingShoppingCart.getSelectedExternalCharges(), isCCPayment, false);
			LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(adultChildCount, 0);

			this.passengerPayment = new ArrayList<PaymentPassengerTO>();
			BigDecimal totalToPay = BigDecimal.ZERO;
			BigDecimal totalAddedExtChgs = BigDecimal.ZERO;

			CommonAncillaryModifyAssembler anciAssembler = new CommonAncillaryModifyAssembler();
			anciAssembler.setPnr(groupPNR);
			anciAssembler.setVersion(version);
			anciAssembler.setTargetSystem(system);
			anciAssembler.setTransactionIdentifier(null);
			anciAssembler.setTemporyPaymentMap(null);
			anciAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));

			List<String> selectedSegemnts = new ArrayList<String>();
			Map<Integer, Boolean> paxHasPaymentMap = new HashMap<>();
			List<PaymentPassengerTO> paxWisePayments = JSONFETOParser.parseCollection(ArrayList.class, PaymentPassengerTO.class,
					resPaxPayments);

			for (ReservationPaxTO pax : paxList) {
				if (pax.getPaxType().equals(PaxTypeTO.INFANT)) {
					continue;
				}
				List<LCCClientExternalChgDTO> extCharges = BookingUtil
						.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
				extCharges.addAll(pax.getExternalCharges());

				if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
						&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {

					removeExistingCCCharge(extCharges);
				}

				LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
				paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);

				paymentAssembler.setPaxType(CommonUtil.getActualPaxType(pax));

				BigDecimal addAnciTotal = BookingUtil.getTotalExtCharge(extCharges);
				BigDecimal removeAnciTotal = pax.getToRemoveAncillaryTotal();
				BigDecimal updateAnciTotal = pax.getToUpdateAncillaryTotal();
				BigDecimal paxCreditOrDueAmount = paxResCreditMap.get(pax.getSeqNumber());
				BigDecimal paxTotalPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (paxCreditOrDueAmount == null) {
					paxCreditOrDueAmount = BigDecimal.ZERO;
				}

				totalAddedExtChgs = AccelAeroCalculator.add(totalAddedExtChgs, addAnciTotal, updateAnciTotal,
						removeAnciTotal.negate());
				BigDecimal paxToPay = AccelAeroCalculator.add(addAnciTotal, updateAnciTotal, removeAnciTotal.negate());
				BigDecimal totalWOCreditDeduct = new BigDecimal(paxToPay.toString());
				if (paxCreditOrDueAmount.compareTo(BigDecimal.ZERO) > 0) { // has pax credit
					paxToPay = AccelAeroCalculator.add(paxToPay, paxCreditOrDueAmount.negate());
				} else { // has no pax credit only a outstanding balance to pay

					paxTotalPaymentAmount = AccelAeroCalculator.add(paxTotalPaymentAmount, paxCreditOrDueAmount.negate());

					// If ancillary modification created outstanding credit, deduct the new credit amount
					// from the existing outstanding balance to pay
					if (AccelAeroCalculator.isGreaterThan(paxTotalPaymentAmount, AccelAeroCalculator.getDefaultBigDecimalZero())
							&& paxToPay.compareTo(BigDecimal.ZERO) < 0) {
						paxTotalPaymentAmount = AccelAeroCalculator.add(paxTotalPaymentAmount, paxToPay);
					}
				}

				if (paxToPay.compareTo(BigDecimal.ZERO) < 0) {
					paxToPay = BigDecimal.ZERO;
				}

				paxTotalPaymentAmount = AccelAeroCalculator.add(paxTotalPaymentAmount, paxToPay);

				PaymentPassengerTO payPaxTO = composePayPaxDTO(payPaxMap.get(pax.getSeqNumber()), pax, totalWOCreditDeduct);
				this.passengerPayment.add(payPaxTO);

				anciAssembler.addPassengerPayment(pax.getSeqNumber(), paymentAssembler);

				// there can be situations where no payments exits but ancillary is added (e.g. no charge ssr)
				anciAssembler.addAncillary(pax.getSeqNumber(), pax.getSelectedAncillaries());
				anciAssembler.removeAncillary(pax.getSeqNumber(), pax.getAncillariesToRemove());
				anciAssembler.updateAncillary(pax.getSeqNumber(), pax.getAncillariesToUpdate());
				anciAssembler.setReservationStatus(resInfo.getReservationStatus());

				if (paxTotalPaymentAmount.compareTo(BigDecimal.ZERO) > 0) {
					totalToPay = AccelAeroCalculator.add(totalToPay, paxTotalPaymentAmount);
				}

				if (totalToPay.compareTo(BigDecimal.ZERO) > 0) {
					pax.updateAnciModifiedSegments(selectedSegemnts);
				}
				paxHasPaymentMap.put(pax.getSeqNumber(), paxTotalPaymentAmount.compareTo(BigDecimal.ZERO) > 0);
			}

			if (paxWisePayments != null && paxWisePayments.size() > 0) {
				for (PaymentPassengerTO paxTO : paxWisePayments) {
					Integer paxSeq = Integer.parseInt(paxTO.getPaxSequence());
					BigDecimal amountToPay = new BigDecimal(paxTO.getDisplayToPay());
					if (!paxHasPaymentMap.containsKey(paxSeq) && amountToPay != null
							&& amountToPay.compareTo(BigDecimal.ZERO) > 0) {
						totalToPay = AccelAeroCalculator.add(totalToPay, amountToPay);
						this.passengerPayment.add(paxTO);
						bookingShoppingCart.getPaxList().add(convertPax(paxTO));
						paxHasPaymentMap.put(paxSeq, Boolean.TRUE);
					}
				}
			}
			// update total payable pax count
			bookingShoppingCart.setPayablePaxCount(ReservationUtil.getNoOfPayablePaxCount(this.passengerPayment));
			// update total segment count pax count
			this.modifedSegmentsCount = getModifiedSegmentCount(selectedSegemnts, flightSegmentTOs);

			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {

				boolean isReservationOnHold = ReservationInternalConstants.ReservationStatus.ON_HOLD
						.equals(resInfo.getReservationStatus());
				boolean isPaymentCarrierSame = resInfo.getMarketingAirlineCode() == null
						? true
						: resInfo.getMarketingAirlineCode().equals(AppSysParamsUtil.getDefaultCarrierCode());

				ExternalChgDTO externalChgDTO = bookingShoppingCart.getAllExternalChargesMap().get(EXTERNAL_CHARGES.CREDIT_CARD);
				BigDecimal totalPaymentAmountExcludingCCCharge = ((resInfo.getAvailableBalance().compareTo(BigDecimal.ZERO) <= 0)
						|| (isReservationOnHold && !isPaymentCarrierSame))
								? totalToPay
								: totalToPay.subtract(resInfo.getAvailableBalance());

				if (totalPaymentAmountExcludingCCCharge.compareTo(BigDecimal.ZERO) <= 0) {
					totalPaymentAmountExcludingCCCharge = BigDecimal.ZERO;
				}
				externalChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
				bookingShoppingCart.addSelectedExternalCharge(externalChgDTO);
				BigDecimal adminCCChargeAmount = externalChgDTO.getAmount();
				BigDecimal jnTaxForCC = ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);
				BigDecimal adminCCChargeAmountWithJN = AccelAeroCalculator.add(jnTaxForCC, adminCCChargeAmount);

				// apply service tax
				BigDecimal serviceTaxAmountForCC = BigDecimal.ZERO;
				ServiceTaxCCParamsDTO serviceTaxCCParamsDTO = new ServiceTaxCCParamsDTO(fareQuoteParams, paxWiseAnci,
						request.getParameter("resContactInfo"), flightRPHList, (isGroupPNR ? groupPNR : ""));
				serviceTaxAmountForCC = ReservationUtil.calculateServiceTaxForTransactionFee(bookingShoppingCart,
						externalChgDTO.getAmount(), resInfo.getTransactionId(), serviceTaxCCParamsDTO, getTrackInfo(),
						EXTERNAL_CHARGES.CREDIT_CARD, 0, 0, getPayablePaxSequence(paxHasPaymentMap), false);
				// add service tax also
				adminCCChargeAmountWithJN = AccelAeroCalculator.add(adminCCChargeAmountWithJN, serviceTaxAmountForCC);

				totalToPay = AccelAeroCalculator.add(totalToPay, adminCCChargeAmountWithJN);

				// need fix for payable pax count
				int payablePaxCount = getPayablePaxSequence(paxHasPaymentMap).size();
				BigDecimal[] paxWisePayment = null;
				if (payablePaxCount > 0) {
					paxWisePayment = AccelAeroCalculator.roundAndSplit(adminCCChargeAmountWithJN, payablePaxCount);
				}
				int index = 0;
				for (ReservationPaxTO pax : paxList) {
					if (pax.getPaxType().equals(PaxTypeTO.INFANT) || !paxHasPaymentMap.get(pax.getSeqNumber())) {
						continue;
					}
					updateDisplayPaymentValues(paxWisePayment[index++], pax.getSeqNumber().toString());
				}
			}

			Map<String, Set<String>> fltRefWiseSelectedMeal = AncillaryDTOUtil.getFlightReferenceWiseSelectedMeals(paxList);

			if (totalToPay.compareTo(BigDecimal.ZERO) <= 0) {
				ModuleServiceLocator.getAirproxyReservationBD().updateAncillary(anciAssembler,
						AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), contactInfo, false,
						fltRefWiseSelectedMeal, trackingfoDTO);
				blnNoPay = true;

				// to send medical ssr email
				boolean hasBalanceToPay = false;
				Map<Integer, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<>();
				for (Map.Entry<Integer, LCCClientPaymentAssembler> entry : anciAssembler.getPassengerPaymentMap().entrySet()) {
					externalCharges.put(entry.getKey(), new ArrayList<>(entry.getValue().getPerPaxExternalCharges()));
				}
				MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(anciAssembler.getPnr(),
						ReservationPaxUtil.toLccClientReservationPax(bookingShoppingCart.getPaxList()), externalCharges,
						contactInfo, hasBalanceToPay);
				medicalSsrDTO.sendEmail();
			} else {
				ReservationBalanceTO reservationBalance = createBalanceTO(totalToPay);
				bookingShoppingCart.setReservationBalance(reservationBalance);
				BigDecimal btp = BigDecimal.ZERO;
				if (paySummary.getBalanceToPay() != null) {
					btp = new BigDecimal(paySummary.getBalanceToPay());
				}
				BigDecimal tp = BigDecimal.ZERO;
				if (paySummary.getTicketPrice() != null) {
					tp = new BigDecimal(paySummary.getTicketPrice());
				}
				BigDecimal nbtp = AccelAeroCalculator.add(btp, totalToPay);
				BigDecimal ntp = AccelAeroCalculator.add(tp, totalAddedExtChgs);
				paySummary.setHandlingCharges(
						PaymentUtil.getTotalHandlingFeeForModification(bookingShoppingCart.getPaxList()).toString());
				paySummary.setBalanceToPay(AccelAeroCalculator.formatAsDecimal(nbtp));
				paySummary.setTicketPrice(AccelAeroCalculator.formatAsDecimal(ntp));
				paySummary.setTotalServiceTax(AccelAeroCalculator.formatAsDecimal(serviceTaxAmount));
				// TODO handle the selected currency pay scenarios....
				this.paymentSummaryTO = paySummary;

				if (this.countryCurrExchangeRate != null && this.paymentSummaryTO != null) {
					this.paymentSummaryTO.setCountryCurrencyCode(countryCurrExchangeRate.getCurrency().getCurrencyCode());
				}
			}

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (flightSegmentTO.getCsOcCarrierCode() != null
						&& ReservationApiUtils.isCodeShareCarrier(flightSegmentTO.getCsOcCarrierCode())) {
					csPnr = true;
					break;
				}
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private ReservationPaxTO convertPax(PaymentPassengerTO paxTO) {
		ReservationPaxTO pax = new ReservationPaxTO();
		pax.setFirstName(paxTO.getDisplayFirstName());
		pax.setLastName(paxTO.getDisplayLastName());
		pax.setSeqNumber(Integer.parseInt(paxTO.getPaxSequence()));
		pax.setPaxType(paxTO.getPaxType());
		return pax;
	}

	private void removeExistingCCCharge(List<LCCClientExternalChgDTO> extCharges) {
		Iterator<LCCClientExternalChgDTO> extChrgItr = extCharges.iterator();
		while (extChrgItr.hasNext()) {
			LCCClientExternalChgDTO extChrg = extChrgItr.next();
			if (EXTERNAL_CHARGES.CREDIT_CARD.equals(extChrg.getExternalCharges())
					|| EXTERNAL_CHARGES.JN_OTHER.equals(extChrg.getExternalCharges())) {
				extChrgItr.remove();
			}
		}
	}

	private BigDecimal getExternalCgrhAmountToUpdate(List<LCCClientExternalChgDTO> extCCCharges) {
		BigDecimal totalExtChrg = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO extChrg : extCCCharges) {
			totalExtChrg = AccelAeroCalculator.add(totalExtChrg, extChrg.getAmount());
		}
		return totalExtChrg;
	}

	private void updateDisplayPaymentValues(BigDecimal adminFee, String paxSeq) {
		for (PaymentPassengerTO paxPaymet : this.passengerPayment) {
			if (paxPaymet.getPaxSequence().equals(paxSeq)) {
				paxPaymet.setDisplayToPay(
						AccelAeroCalculator.add(new BigDecimal(paxPaymet.getDisplayToPay()), adminFee).toString());
				paxPaymet.setDisplayOriginalTobePaid(
						AccelAeroCalculator.add(new BigDecimal(paxPaymet.getDisplayOriginalTobePaid()), adminFee).toString());
				paxPaymet.setDisplayTotal(
						AccelAeroCalculator.add(new BigDecimal(paxPaymet.getDisplayTotal()), adminFee).toString());
			}
		}
	}

	/*
	 * returns actual flight segments (without bus segments)
	 */
	private List<FlightSegmentTO> getFlightSegmentsWithoutBusSegments(List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		List<FlightSegmentTO> actualFlightSegmentList = new ArrayList<>();
		for (Iterator<FlightSegmentTO> flightSegmentListIterator = flightSegmentTOs.iterator(); flightSegmentListIterator
				.hasNext();) {
			FlightSegmentTO flightSegment = flightSegmentListIterator.next();
			if (!ModuleServiceLocator.getAirportBD().isBusSegment(flightSegment.getSegmentCode())) {
				actualFlightSegmentList.add(flightSegment);
			}
		}
		return actualFlightSegmentList;
	}

	private String getModifiedSegmentCount(List<String> selectedSegemnts, List<FlightSegmentTO> flightSegmentTOs) {
		// where ancillary modified segments are available
		int segmentCount = 0;
		if (!selectedSegemnts.isEmpty()) {
			segmentCount = selectedSegemnts.size();
		} else { // where user continue with existing ancillary
			segmentCount = flightSegmentTOs.size();
		}

		return String.valueOf(segmentCount);
	}

	private ReservationBalanceTO createBalanceTO(BigDecimal totalRemToPay) {
		ReservationBalanceTO reservationBalance = new ReservationBalanceTO();
		reservationBalance.setTotalAmountDue(totalRemToPay);
		reservationBalance.setTotalCnxCharge(BigDecimal.ZERO);
		reservationBalance.setTotalCreditAmount(BigDecimal.ZERO);
		reservationBalance.setTotalModCharge(BigDecimal.ZERO);
		reservationBalance.setTotalPaidAmount(BigDecimal.ZERO);
		reservationBalance.setTotalPrice(totalRemToPay);
		return reservationBalance;
	}

	private PaymentPassengerTO composePayPaxDTO(PaymentPassengerTO payPaxTO, ReservationPaxTO pax, BigDecimal paxToPay) {
		String strChild = "";
		String strInf = "";
		if (pax.getPaxType().equals(PaxTypeTO.CHILD)) {
			strChild = "CH. ";
		}
		if (pax.getIsParent()) {
			strInf = " /INF";
		}
		payPaxTO.setDisplayFirstName(pax.getFirstName());
		payPaxTO.setDisplayLastName(pax.getLastName());
		payPaxTO.setDisplayPassengerName(
				strChild + ((pax.getTitle() != null && !"".equals(pax.getTitle().trim())) ? pax.getTitle() + ". " : "")
						+ StringUtil.toInitCap(pax.getFirstName()) + " " + StringUtil.toInitCap(pax.getLastName()) + strInf);
		payPaxTO.setDisplayPaxID(pax.getSeqNumber() + "");

		BigDecimal toPay = AccelAeroCalculator.add(new BigDecimal(payPaxTO.getDisplayToPay()), paxToPay);
		if (toPay.compareTo(BigDecimal.ZERO) < 0) {
			toPay = BigDecimal.ZERO;
		}

		BigDecimal total = AccelAeroCalculator.add(paxToPay, new BigDecimal(payPaxTO.getDisplayTotal()));
		if (total.compareTo(BigDecimal.ZERO) < 0) {
			total = BigDecimal.ZERO;
		}

		payPaxTO.setDisplayToPay(AccelAeroCalculator.formatAsDecimal(toPay));
		payPaxTO.setDisplayOriginalTobePaid(AccelAeroCalculator
				.formatAsDecimal(AccelAeroCalculator.add(new BigDecimal(payPaxTO.getDisplayOriginalTobePaid()), paxToPay)));
		payPaxTO.setDisplayTotal(AccelAeroCalculator.formatAsDecimal(total));
		payPaxTO.setDisplayUsedCredit("0.00");
		payPaxTO.setPaxSequence(pax.getSeqNumber().toString());
		return payPaxTO;
	}

	private Map<Integer, PaymentPassengerTO> paxPayExtract(String paxJson) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONArray jsonArray = (JSONArray) parser.parse(paxJson);
		Iterator<?> iterator = jsonArray.iterator();
		Map<Integer, PaymentPassengerTO> paxMap = new HashMap<Integer, PaymentPassengerTO>();
		while (iterator.hasNext()) {
			PaymentPassengerTO payPax = new PaymentPassengerTO();
			JSONObject jObject = (JSONObject) iterator.next();
			payPax.setDisplayPaid(jObject.get("totalPaidAmount").toString());
			payPax.setDisplayOriginalPaid(jObject.get("totalPaidAmount").toString());
			payPax.setDisplayToPay(jObject.get("totalAvailableBalance").toString());
			payPax.setDisplayOriginalTobePaid(jObject.get("totalAvailableBalance").toString());
			payPax.setDisplayTotal(jObject.get("totalPrice").toString());
			payPax.setDisplayUsedCredit("0.00");

			Integer seqId = new Integer(jObject.get("seqNumber").toString());

			payPax.setPaxCreditInfo(new ArrayList<PaxUsedCreditInfoTO>());

			paxMap.put(seqId, payPax);
		}
		return paxMap;
	}

	private PaymentSummaryTO getPaymentSummaryDTO(String jsonPaySummary) throws ParseException {
		PaymentSummaryTO paySum = new PaymentSummaryTO();

		JSONParser parser = new JSONParser();
		JSONObject jObj = (JSONObject) parser.parse(jsonPaySummary);
		paySum.setHandlingCharges(jObj.get("handlingCharges").toString());
		paySum.setModificationCharges(jObj.get("modificationCharges").toString());
		paySum.setPaid(jObj.get("paid").toString());
		paySum.setTicketPrice(jObj.get("ticketPrice").toString());
		paySum.setTotalFare(jObj.get("totalFare").toString());
		paySum.setTotalTaxSurCharges(jObj.get("totalTaxSurCharges").toString());
		paySum.setCurrency(jObj.get("currency").toString());
		if (selCurrency != null && !selCurrency.equalsIgnoreCase(paySum.getCurrency())) {
			// paySum.setSelectedCurrency(jObj.get("selectedCurrency").toString());
			paySum.setBalanceToPaySelcur(
					jObj.get("balanceToPaySelCur") != null ? jObj.get("balanceToPaySelCur").toString() : null);
			paySum.setSelectedCurrency(selCurrency);
		}

		return paySum;
	}

	@Override
	protected TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = super.getTrackInfo();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		trackInfoDTO.setPaymentAgent(((UserPrincipal) request.getUserPrincipal()).getAgentCode());
		return trackInfoDTO;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public void setSelCurrency(String selCurrency) {
		this.selCurrency = selCurrency;
	}

	public boolean isBlnNoPay() {
		return blnNoPay;
	}

	public String getPnr() {
		return pnr;
	}

	public String getModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(String modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setInsurableFltRefNumbers(String insurableFltRefNumbers) {
		this.insurableFltRefNumbers = insurableFltRefNumbers;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setResPaySummary(String resPaySummary) {
		this.resPaySummary = resPaySummary;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public void setInsurances(String insurances) {
		this.insurances = insurances;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(CurrencyExchangeRate currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public String getModifedSegmentsCount() {
		return modifedSegmentsCount;
	}

	public void setModifedSegmentsCount(String modifedSegmentsCount) {
		this.modifedSegmentsCount = modifedSegmentsCount;
	}

	public CurrencyExchangeRate getCountryCurrExchangeRate() {
		return countryCurrExchangeRate;
	}

	public void setCountryCurrExchangeRate(CurrencyExchangeRate countryCurrExchangeRate) {
		this.countryCurrExchangeRate = countryCurrExchangeRate;
	}

	public boolean getCsPnr() {
		return csPnr;
	}

	public void setCsPnr(boolean csPnr) {
		this.csPnr = csPnr;
	}

	private void calculateAndApplyHandlingFeeForPax(BookingShoppingCart bookingShoppingCart,
			List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {
		HandlingFeeCalculationUtil.applyPaxWiseHandlingFeeForModification(bookingShoppingCart.getPaxList(),
				getFirstFlightReference(flightSegmentTOs), CommonsConstants.ModifyOperation.MODIFY_ANCI.getOperation());

	}

	private String getFirstFlightReference(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {
		String flightRef = null;
		if (flightSegmentTOs != null && !flightSegmentTOs.isEmpty()) {
			List<FlightSegmentTO> actualFlightSegmentList = getFlightSegmentsWithoutBusSegments(flightSegmentTOs);
			FlightSegmentTO FlightSegmentTO = BeanUtils.getFirstElement(actualFlightSegmentList);

			if (FlightSegmentTO != null)
				flightRef = FlightSegmentTO.getFlightRefNumber();
		}

		return flightRef;

	}

	public void setResPaxPayments(String resPaxPayments) {
		this.resPaxPayments = resPaxPayments;
	}

	private Set<Integer> getPayablePaxSequence(Map<Integer, Boolean> paxHasPaymentMap) {
		Set<Integer> payablePaxSequence = new HashSet<>();
		if (paxHasPaymentMap != null && !paxHasPaymentMap.isEmpty()) {
			for (Integer paxSeq : paxHasPaymentMap.keySet()) {
				if (paxHasPaymentMap.get(paxSeq) != null && paxHasPaymentMap.get(paxSeq)) {
					payablePaxSequence.add(paxSeq);
				}
			}
		}

		return payablePaxSequence;

	}

}
