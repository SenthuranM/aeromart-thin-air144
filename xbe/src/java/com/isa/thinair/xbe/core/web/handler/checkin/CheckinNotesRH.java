package com.isa.thinair.xbe.core.web.handler.checkin;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.UserNotes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.checkin.CheckinNotesHG;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class CheckinNotesRH extends BasicRH {

	private static Log log = LogFactory.getLog(CheckinNotesRH.class);

	private static XBEConfig xbeConfig = new XBEConfig();

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_FLIGHT_SEG_ID = "hdnFlightSegId";

	/**
	 * Main execute method for User notes
	 * 
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		String strMode = request.getParameter(PARAM_MODE);
		String strFlightSegId = request.getParameter(PARAM_FLIGHT_SEG_ID);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		if (strMode != null && strMode.equals("SAVE")) {
			try {
				
				checkPrivilege(request, WebConstants.PRIV_CHECKIN_FLIGHT_AGENT);
				saveCheckinNotes(request);
				saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);

			} catch (ModuleException me) {
				if (log.isErrorEnabled()) {
					log.error("Error in saving user Notes " + me.getMessage());
				}
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Error in saving user Notes " + e.getMessage());
				}
				if (e instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
				} else {
					saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}

		if (strMode != null && strMode.equals("VIEW")) {
			try {
				checkPrivilege(request, WebConstants.PRIV_CHECKIN_FLIGHT_AGENT);
				setCheckinNotes(request);
			} catch (ModuleException me) {
				if (log.isErrorEnabled()) {
					log.error("Error in geting checkin Notes ", me);
				}
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Error in geting checkin Notes ", e);
				}
				if (e instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
				} else {
					saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
				}
			}

			forward = S2Constants.Result.VIEW_USER_NOTE;
		}
		request.setAttribute(WebConstants.PARAM_FLIGHT_SEG_ID, strFlightSegId);
		return forward;
	}

	/**
	 * SETS THE USER CHECKIN NOTES TO THE REQUEST
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setCheckinNotes(HttpServletRequest request) throws ModuleException {
		String strHtml = CheckinNotesHG.getCheckinNotesDetails(request);
		request.setAttribute(WebConstants.REQ_HTML_CHECKIN_NOTES, strHtml);
	}

	/**
	 * saves the checkin notes
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void saveCheckinNotes(HttpServletRequest request) throws ModuleException {

		String strUserId = ((UserPrincipal) request.getUserPrincipal()).getUserId();
		String flightSegId = request.getParameter("hdnFlightSegId");
		String strUserNote = request.getParameter("txtUsetNotes");
		UserNotes uNotes = null;
		if (strUserId != null && !strUserId.equals("")) {
			uNotes = new UserNotes();
			uNotes.setUserNotesTypeCode(WebConstants.USER_NOTES_FOR_MANUAL_CHECKIN);
			uNotes.setIdentificationCode(flightSegId);
			uNotes.setContent(strUserNote);
			uNotes.setUserId(strUserId);
			ModuleServiceLocator.getSecurityBD().saveUserNotes(uNotes);
		}
	}

}
