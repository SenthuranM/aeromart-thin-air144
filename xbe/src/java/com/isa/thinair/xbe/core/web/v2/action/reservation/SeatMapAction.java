package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the seat map
 * 
 * @author Lasantha Pambagoda
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SeatMapAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(SeatMapAction.class);

	private boolean success = true;

	private String messageTxt;

	private LCCSeatMapDTO seatMapDTO;

	private String selectedFlightList;

	private FlightSearchDTO searchParams;

	private boolean modifyAncillary = false;

	private boolean isRequote = false;

	private String jsonOnds;

	private String pnr;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			String strTxnIdntifier = null;
			SYSTEM system = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			List<BundledFareDTO> bundledFareDTOs = null;

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(selectedFlightList);
				system = fltSegBuilder.getSystem();
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
				bundledFareDTOs = resInfo.getBundledFareDTOs();
			} else {
				strTxnIdntifier = resInfo.getTransactionId();
				system = resInfo.getSelectedSystem();
				if (isRequote) {
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}

				BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
						S2Constants.Session_Data.LCC_SES_BOOKING_CART);
				bundledFareDTOs = bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getOndBundledFareDTOs();
			}

			LCCSeatMapDTO inSeatMapDTO = new LCCSeatMapDTO();
			inSeatMapDTO.setFlightDetails(flightSegmentTOs);
			inSeatMapDTO.setTransactionIdentifier(strTxnIdntifier);
			inSeatMapDTO.setQueryingSystem(system);
			inSeatMapDTO.setBundledFareDTOs(bundledFareDTOs);
			inSeatMapDTO.setPnr(pnr);

			seatMapDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getSeatMap(inSeatMapDTO, null,
					TrackInfoUtil.getBasicTrackInfo(request), ApplicationEngine.XBE);

			// setSeatMapDTO(seatMapDTO);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("messageTxt", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public LCCSeatMapDTO getSeatMapDTO() {
		return seatMapDTO;
	}

	public void setSeatMapDTO(LCCSeatMapDTO seatMapDTO) {
		this.seatMapDTO = seatMapDTO;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
