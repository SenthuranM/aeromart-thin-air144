package com.isa.thinair.xbe.core.web.v2.action.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.SystemParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.v2.util.CommonUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_MAKE_RESERVATION),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class MakeReservationAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(MakeReservationAction.class);

	private FlightSearchDTO externalSearchParams;
	
	private String oldAdultPaxInfo;
	
	private String oldInfantPaxInfo;
	
	private String oldContactInfo;
	
	private int oldNoOfAdults;
	
	private int oldNoOfChildren;
	
	private int oldNoOfInfants;
	
	public String execute() throws JSONException {

		try {
			ReservationProcessParams rpParams = new ReservationProcessParams(request, null, null, true, false, null, null, false, false);
			if (externalSearchParams != null) {
				rpParams.setTriggerExternalSearch(true);
			}

			rpParams.setPromoCodeEnabled(AppSysParamsUtil.isPromoCodeEnabled());

			SystemParams sysParams = new SystemParams();
			sysParams.setMinimumTransitionTimeInMillis(AppSysParamsUtil.getMinimumReturnTransitTimeInMillis());
			sysParams.setDefaultLanguage(XBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));
			sysParams.setDynamicOndListPopulationEnabled(AppSysParamsUtil.isDynamicOndListPopulationEnabled());
			sysParams.setAllowedMaxNoONDMulticitySearch(AppSysParamsUtil.getMulticitySearchMaximumNoONDXBE());
			sysParams.setUserDefineTransitTimeEnabled(AppSysParamsUtil.isEnableUserDefineTransitTime());

			request.setAttribute("externalSearchParams", JSONUtil.serialize(externalSearchParams));
			request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
			request.setAttribute("systemParams", JSONUtil.serialize(sysParams));
			request.setAttribute("carrierCode", AppSysParamsUtil.getDefaultCarrierCode());
			
			if (oldAdultPaxInfo != null && !oldAdultPaxInfo.isEmpty()) {
				request.setAttribute("oldResAdultPax", oldAdultPaxInfo);
				request.setAttribute("oldResInfantPax", oldInfantPaxInfo);
				request.setAttribute("oldResContactInfo", oldContactInfo);
				request.setAttribute("oldResNoOfAdults", oldNoOfAdults);
				request.setAttribute("oldResNoOfChildren", oldNoOfChildren);
				request.setAttribute("oldResNoOfInfants", oldNoOfInfants);
			}

			request.setAttribute("isXBEMinimumFareEnabled", AppSysParamsUtil.isXBEMinimumFareEnabled());
			/*
			 * String isMultiCitySearch = request.getParameter("criteria") != null ? request.getParameter("mc") :
			 * "false";
			 * 
			 * request.setAttribute("isMultiCity", Boolean.parseBoolean(isMultiCitySearch));
			 */
			if (request.getParameter("criteria") != null) {
				if ("minimumFare".equals(request.getParameter("criteria")) && AppSysParamsUtil.isXBEMinimumFareEnabled()) {
					request.setAttribute("isMinimumFare", "true");
					request.setAttribute("isMultiCity", "false");
				} else if ("multiCity".equals(request.getParameter("criteria").toString())) {
					request.setAttribute("isMinimumFare", "false");
					request.setAttribute("isMultiCity", "true");
				} else {
					request.setAttribute("isMinimumFare", "false");
					request.setAttribute("isMultiCity", "false");
				}
			} else {
				request.setAttribute("isMinimumFare", "false");
				request.setAttribute("isMultiCity", "false");
			}

			if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
				request.setAttribute("sysOndSource", AppSysParamsUtil.getDynamicOndListUrl());
			}

			request.getSession().setAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART,
					new BookingShoppingCart(ChargeRateOperationType.MAKE_ONLY));
			request.getSession().setAttribute(S2Constants.Session_Data.XBE_SES_RESDATA, new XBEReservationInfoDTO());
			request.setAttribute("reqPayAgents", ReservationUtil.getPaymentAgentOptions(request));
			request.setAttribute("serviceTaxLabel", AppSysParamsUtil.getServiceTaxLabel());
			request.setAttribute("taxRegistrationNumberLabel", AppSysParamsUtil.getTaxRegistrationNumberLabel());
			request.getSession().setAttribute(S2Constants.Session_Data.RESER_PROCESS_PARAMS, null);
			request.setAttribute("excludableCharges", JSONUtil.serialize(ReservationUtil.getExcludableChargesMap()));
			request.setAttribute(WebConstants.PARAM_SES_AGENT_BALANCE, CommonUtil.getLoggedInAgentBalance(request));



		} catch (ModuleException me) {
			// This should not break the flow
			log.error(me.getExceptionDetails(), me);
		} catch (Exception e) {
			log.error("Generic error occured in make reservation", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public FlightSearchDTO getExternalSearchParams() {
		return externalSearchParams;
	}

	public void setExternalSearchParams(FlightSearchDTO externalSearchParams) {
		this.externalSearchParams = externalSearchParams;
	}

	public static void setLog(Log log) {
		MakeReservationAction.log = log;
	}

	public String getOldAdultPaxInfo() {
		return oldAdultPaxInfo;
	}

	public void setOldAdultPaxInfo(String oldAdultPaxInfo) {
		this.oldAdultPaxInfo = oldAdultPaxInfo;
	}

	public String getOldInfantPaxInfo() {
		return oldInfantPaxInfo;
	}

	public void setOldInfantPaxInfo(String oldInfantPaxInfo) {
		this.oldInfantPaxInfo = oldInfantPaxInfo;
	}

	public String getOldContactInfo() {
		return oldContactInfo;
	}

	public void setOldContactInfo(String oldContactInfo) {
		this.oldContactInfo = oldContactInfo;
	}

	public int getOldNoOfAdults() {
		return oldNoOfAdults;
	}

	public void setOldNoOfAdults(int oldNoOfAdults) {
		this.oldNoOfAdults = oldNoOfAdults;
	}

	public int getOldNoOfChildren() {
		return oldNoOfChildren;
	}

	public void setOldNoOfChildren(int oldNoOfChildren) {
		this.oldNoOfChildren = oldNoOfChildren;
	}

	public int getOldNoOfInfants() {
		return oldNoOfInfants;
	}

	public void setOldNoOfInfants(int oldNoOfInfants) {
		this.oldNoOfInfants = oldNoOfInfants;
	}
}
