package com.isa.thinair.xbe.core.util;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

import javax.servlet.http.HttpServletRequest;

public class AuthUtils {

    public static String getAgentCodeOfCurrentUser(HttpServletRequest request) {
        String agentCode = null;
        User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
        if (user != null) {
            agentCode = user.getAgentCode();
        }
        return agentCode;
    }
}
