package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.CustomizedItineraryUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.taxInvoice.TaxInvoiceDTO;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_PRINT_TAX_INVOICE),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class PrintTaxInvoiceAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(PrintTaxInvoiceAction.class);

	private static String ACTION_PRINT = "PRINT";

	private String hdnPNRNo;
	private String invoiceLanguage;
	private boolean groupPNR = false;
	private String selectedTaxInvoices;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(hdnPNRNo, groupPNR, "en", request, false, null, null, null, false);
			ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
			paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_XBE);
			LCCClientReservation reservation = ModuleServiceLocator.getAirproxyReservationBD().searchReservationByPNR(
					pnrModesDTO, paramRQInfo, getTrackInfo());

			List<TaxInvoice> taxInvoicesList = new ArrayList<TaxInvoice>();
			taxInvoicesList = getTaxInvoiceByPnr(hdnPNRNo, selectedTaxInvoices, reservation);
			getTaxInvoiceInfo(taxInvoicesList, reservation);

		} catch (Exception e) {
			String msg = BasicRH.getErrorMessage(e, userLanguage);

			forward = S2Constants.Result.ERROR;

			JavaScriptGenerator.setServerError(request, msg, "top", "");
			log.error(msg, e);
		}

		return forward;
	}

	private void getTaxInvoiceInfo(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation) throws ModuleException {
		String taxInvoice = "";
		taxInvoice = getTaxInvoice(taxInvoicesList, reservation);
		request.setAttribute("reqTaxInvoiceBodyHtml", taxInvoice);
	}

	public String getHdnPNRNo() {
		return hdnPNRNo;
	}

	public void setHdnPNRNo(String hdnPNRNo) {
		this.hdnPNRNo = hdnPNRNo;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}
	
	private List<TaxInvoice> getTaxInvoices(String taxInvoiceNumbers) throws ModuleException {
		List<Integer> invoiceNumbersInt = new ArrayList<>();
		List<String> invoiceNumbersStrList = Arrays.asList(taxInvoiceNumbers.split("\\s*,\\s*"));
		return ModuleServiceLocator.getReservationBD().getTaxInvoiceFor(invoiceNumbersStrList);
	}

	public String getTaxInvoice(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation) throws ModuleException {
		String taxInvoice = "";
		taxInvoice += ModuleServiceLocator.getReservationBD().getTaxInvoiceForPrint(taxInvoicesList, reservation);
		return taxInvoice;
	}

	public List<TaxInvoice> getTaxInvoiceByPnr(String pnr, String taxInvoiceNumbers, LCCClientReservation reservation)
			throws ModuleException {

		List<String> selectedInvoiceNumbersStrList = Arrays.asList(taxInvoiceNumbers.split("\\s*,\\s*"));

		List<TaxInvoice> selectedTaxInvoicesList = getSelectedTaxInvoicesList(selectedInvoiceNumbersStrList,
				reservation.getTaxInvoicesList());

		return selectedTaxInvoicesList;
	}

	private List<TaxInvoice> getSelectedTaxInvoicesList(List<String> selectedInvoiceNumbersStrList,
			List<TaxInvoice> allTaxInvoicesList) {
		List<TaxInvoice> selectedTaxInvoices = new ArrayList<>();
		for (String taxInvoiceNum : selectedInvoiceNumbersStrList) {
			for (TaxInvoice taxInvoice : allTaxInvoicesList) {
				if (taxInvoiceNum.equals(taxInvoice.getTaxInvoiceId().toString())) {
					selectedTaxInvoices.add(taxInvoice);
				}
			}
		}
		return selectedTaxInvoices;
	}

	public String getSelectedTaxInvoices() {
		return selectedTaxInvoices;
	}

	public void setSelectedTaxInvoices(String selectedTaxInvoices) {
		this.selectedTaxInvoices = selectedTaxInvoices;
	}

	public String getInvoiceLanguage() {
		return invoiceLanguage;
	}

	public void setInvoiceLanguage(String invoiceLanguage) {
		this.invoiceLanguage = invoiceLanguage;
	}

	private static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, String preferredLanguage,
			HttpServletRequest request, boolean recordAudit, String marketingAirlineCode, String airlineCode,
			Long interlineAgreementId, boolean skipPromoAdjustment) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
		} else {
			pnrModesDTO.setPnr(pnr);
		}

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadLocalTimes(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true); // baggage
		pnrModesDTO.setAppIndicator(ApplicationEngine.XBE);
		pnrModesDTO.setPreferredLanguage(preferredLanguage);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		pnrModesDTO.setLoadGOQUOAmounts(true);
		if (request != null) {
			pnrModesDTO.setMapPrivilegeIds((Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS));
		}
		pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
		pnrModesDTO.setInterlineAgreementId(interlineAgreementId);
		pnrModesDTO.setAirlineCode(airlineCode);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setSkipPromoAdjustment(skipPromoAdjustment);

		if (request != null) {
			pnrModesDTO.setLoadRefundableTaxInfo(BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_NOSHOW_REFUNDABLE_TAX_CALC));
			pnrModesDTO.setLoadClassifyUN(BasicRH.hasPrivilege(request, PriviledgeConstants.CLASSIFY_USER_NOTE));
		}
		return pnrModesDTO;

	}

}