package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.BookingTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SplitPaxAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(SplitPaxAction.class);

	private boolean success = true;

	private String messageTxt;

	private String newPNRNoMsg;

	private String newPNRNo;

	private BookingTO bookingInfo;

	private String groupPNR;

	public String execute() {
		if (log.isDebugEnabled()) {
			log.debug("Inside SplitPaxAction.execute");
		}

		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (isGroupPNR) {
				bookingInfo.setPNR(groupPNR);
			}
			String paxInfants = request.getParameter("paxInfants");
			String paxAdults = request.getParameter("paxAdults");
			Collection<LCCClientReservationPax> colpaxs = ReservationUtil
					.transformJsonPassengers(request.getParameter("respaxs"));

			List<String> paxInfantList = extractJSonObject(paxInfants);
			List<String> paxAdultsList = extractJSonObject(paxAdults);

			List<String> pnrPaxIds = getPaxIdsForSplit(colpaxs, paxAdultsList, paxInfantList);

			// Check Split constraints
			this.checkSplitConstraints(colpaxs, pnrPaxIds);

			if (log.isDebugEnabled()) {
				log.debug("PAX to split : adults = " + paxAdults + ", children = " + paxInfants);
			}

			LCCClientReservation lccClientReservationForSplit = new LCCClientReservation();
			{
				lccClientReservationForSplit.setPNR(bookingInfo.getPNR());
				lccClientReservationForSplit.setVersion(bookingInfo.getVersion());
				lccClientReservationForSplit.setStatus(bookingInfo.getStatus());
				lccClientReservationForSplit.setBookingCategory(bookingInfo.getBookingCategory());
				lccClientReservationForSplit.setOriginCountryOfCall(bookingInfo.getOriginCountryOfCall());
				for (LCCClientReservationPax lccClientReservationPax : colpaxs) {

					if (pnrPaxIds.contains(lccClientReservationPax.getTravelerRefNumber())) {

						lccClientReservationForSplit.addPassenger(lccClientReservationPax);

						if (log.isDebugEnabled()) {
							log.debug("Requesting to Split : " + lccClientReservationPax.getFirstName() + " "
									+ lccClientReservationPax.getLastName());
							log.debug("Traveler passenger number : " + lccClientReservationPax.getTravelerRefNumber());
						}
					}
				}

				if (log.isDebugEnabled()) {
					log.debug("Group PNR to be split : " + bookingInfo.getPNR());
					log.debug("Version : " + lccClientReservationForSplit.getVersion());
				}
			}

			if (log.isDebugEnabled()) {
				log.debug("Calling split reservation");
			}

			LCCClientReservation newReservation = ModuleServiceLocator.getAirproxyReservationBD().splitReservation(
					lccClientReservationForSplit, isGroupPNR, getTrackInfo());
			this.newPNRNoMsg = "Reservation Split Successfully : New PNR Number is " + newReservation.getPNR();
			this.newPNRNo = newReservation.getPNR();
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
		}

		if (log.isDebugEnabled()) {
			log.debug("Exitting SplitPaxAction.execute");
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * Check split constraints
	 * 
	 * @param reservation
	 * @param pnrPaxIds
	 * @param version
	 * @throws ModuleException
	 */
	private void checkSplitConstraints(Collection<LCCClientReservationPax> paxes, Collection<String> pnrPaxIds)
			throws ModuleException {

		int paxSize = paxes.size();

		Collection<String> currentPaxIds = new ArrayList<String>();

		for (LCCClientReservationPax reservationPax : paxes) {
			currentPaxIds.add(reservationPax.getTravelerRefNumber());
		}

		// Passenger count can not be less than or equal to the split passenger count
		if (paxSize == pnrPaxIds.size()) {
			throw new ModuleException("airreservations.arg.invalidSplit.all.pax.selected");
		}

		// If invalid passenger(s) exist from the input
		if (!currentPaxIds.containsAll(pnrPaxIds) || paxSize < pnrPaxIds.size()) {
			throw new ModuleException("airreservations.arg.invalidSplit");
		}
	}

	private List<String> getPaxIdsForSplit(Collection<LCCClientReservationPax> paxes, List<String> paxAdultsList,
			List<String> paxInfantList) {
		List<String> splitPaxIds = new ArrayList<String>();

		splitPaxIds.addAll(paxAdultsList);
		splitPaxIds.addAll(paxInfantList);

		for (LCCClientReservationPax pax : paxes) {

			// Check if the passenger has been chosen for split
			if (splitPaxIds.contains(pax.getTravelerRefNumber())) {

				// If the passenger is of type adult or child
				if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)
						|| pax.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
					Set<LCCClientReservationPax> infants = pax.getInfants();

					// If there are infants associated with the passenger and
					// have not been chosen for split with their parents, then
					// include them.
					if (infants != null && infants.size() > 0) {

						for (LCCClientReservationPax infantPax : infants) {
							if (!splitPaxIds.contains(infantPax.getTravelerRefNumber())) {
								splitPaxIds.add(infantPax.getTravelerRefNumber());
							}
						}
					}
				}
				// if the passenger is of infant type
				else if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {

					// If the infants parents have not been chosen for split,
					// then remove the infant.
					if (!splitPaxIds.contains(pax.getParent().getTravelerRefNumber())) {
						splitPaxIds.remove(pax.getTravelerRefNumber());
					}
				}
			}
		}
		return splitPaxIds;
	}

	private List<String> extractJSonObject(String jSonString) throws ParseException {
		List<String> jsonList = new ArrayList<String>();
		if (jSonString != null && !jSonString.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jSonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();
				String paxId = BeanUtils.nullHandler(jObject.get("paxID"));
				jsonList.add(paxId);
			}
		}
		return jsonList;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the newPNRNoMsg
	 */
	public String getNewPNRNoMsg() {
		return newPNRNoMsg;
	}

	/**
	 * @param newPNRNoMsg
	 *            the newPNRNoMsg to set
	 */
	public void setNewPNRNoMsg(String newPNRNoMsg) {
		this.newPNRNoMsg = newPNRNoMsg;
	}

	/**
	 * @return the bookingInfo
	 */
	public BookingTO getBookingInfo() {
		return bookingInfo;
	}

	/**
	 * @param bookingInfo
	 *            the bookingInfo to set
	 */
	public void setBookingInfo(BookingTO bookingInfo) {
		this.bookingInfo = bookingInfo;
	}

	public String getNewPNRNo() {
		return newPNRNo;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

}
