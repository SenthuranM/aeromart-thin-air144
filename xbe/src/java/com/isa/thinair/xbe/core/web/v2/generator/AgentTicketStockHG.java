package com.isa.thinair.xbe.core.web.v2.generator;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

/**
 * @author eric
 * 
 */
public class AgentTicketStockHG {

	private static String clientErrors;

	/**
	 * Create Client Validations for Agent Ticket Stock Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.ticket.stock.minBound.required", "msgTStockLowerBoundEmpty");
			moduleErrs.setProperty("um.ticket.stock.maxBound.required", "msgTStockUpperBoundEmpty");
			moduleErrs.setProperty("um.ticket.stock.status.required", "msgTktStockStatusEmpty");
			moduleErrs.setProperty("um.ticket.stock.maxBound.lessthan.previousMax", "msgTktStockUpperNewInvalid");
			moduleErrs.setProperty("cc.airadmin.save.success", "saveRecoredSucces");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

}
