package com.isa.thinair.xbe.core.util;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class TravelAgentUtil {

	/**
	 * loads a travel agent
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static Agent loadTravelAgent(String agentCode) throws ModuleException {
		Collection agentCodes = new ArrayList();
		agentCodes.add(agentCode);
		Collection colAgents = ModuleServiceLocator.getTravelAgentBD().getAgents(agentCodes);
		return (Agent) BeanUtils.getFirstElement(colAgents);
	}
	
}
