package com.isa.thinair.xbe.core.web.handler.travagent;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.invoicing.api.dto.GenerateInvoiceOption;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.service.InvoicingBD;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.MessageCodes;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.generator.travagent.InvoiceTransHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class InvoiceTransRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(InvoiceTransRequestHandler.class);
	private static XBEConfig xbeConfig = new XBEConfig();

	/**
	 * Main Execute Method for Invoice Transfer Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 * @throws ModuleException
	 * 
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		InvoicingBD invoicingDelegate = ModuleServiceLocator.getInvoicingBD();
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
			boolean isReportingAgentsExist = true;
			if (!AppSysParamsUtil.getCarrierAgent().equals(userPrincipal.getAgentTypeCode())) {
				List<String[]> gsas = (List<String[]>) SelectListGenerator
						.createAgentForLoggedUserLevelOfRights(userPrincipal.getAgentCode(), 2);
				if (gsas == null || gsas.isEmpty()) {
					isReportingAgentsExist = false;
				}
			}
			if (!isReportingAgentsExist || AgentType.STA.equals(userPrincipal.getAgentTypeCode())) {
				throw new ModuleException(MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN);
			}
		} else {
			if (userPrincipal.getAgentTypeCode().equals(WebConstants.AGENT_TYPE_TRAVEL_AGENT)) {
				throw new ModuleException(MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN);
			}
		}

		// audit(request,TasksUtil.TA_EMAIL_INVOICE);
		boolean isByTransaction = AppSysParamsUtil.isTransactInvoiceEnable();
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		if (strHdnMode != null && strHdnMode.equals("REPORT") && AppSysParamsUtil.isDownLoadCSVCashSalesReport()) {
			String selectedInvoices = request.getParameter("hdnSelectedInvoices");
			if (selectedInvoices != null && !selectedInvoices.equals("")) {
				String[] invoiceList = selectedInvoices.split(",");
				Collection<String> invoiceListCol = new ArrayList<String>();
				invoiceListCol = Arrays.asList(invoiceList);
				setReportView(response, invoiceListCol, "CSV");
			}
			return null;
		}
		if (strHdnMode != null && strHdnMode.equals("PRINT") && AppSysParamsUtil.isDownLoadCSVCashSalesReport()) {
			String selectedInvoices = request.getParameter("hdnSelectedInvoices");
			if (selectedInvoices != null && !selectedInvoices.equals("")) {
				String[] invoiceList = selectedInvoices.split(",");
				Collection<String> invoiceListCol = new ArrayList<String>();
				invoiceListCol = Arrays.asList(invoiceList);
				setReportView(response, invoiceListCol, "PDF");
			}
			return null;
		}
		String strYear = request.getParameter("selYear");
		String strMonth = request.getParameter("selMonth");
		String strBilingPeriod = request.getParameter("selBP");
		String strAgent = request.getParameter("hdnSelectedAgents");
		if (StringUtil.isNullOrEmpty(strAgent) && !StringUtil.isNullOrEmpty(request.getParameter("hdnAgents"))) {
			strAgent = request.getParameter("hdnAgents");
		}	
		String strAll = request.getParameter("chkAll");
		String strZero = request.getParameter("chkZero");
		// For Transaction wise invoices
		String strInvoiceFrm = request.getParameter("txtInvoiceFrom");
		String strInvoiceTo = request.getParameter("txtInvoiceTo");
		String strSlectedInvs = request.getParameter("hdnSelectedInvoices");
		String strEntity = request.getParameter("selEntity");

		SearchInvoicesDTO srcinvDto = new SearchInvoicesDTO();
		Collection<String> agentcol = new ArrayList<String>();
		Collection<String> invoicecol = new ArrayList<String>();

		if (strYear != null && !strYear.trim().equals("")) {
			srcinvDto.setYear(Integer.parseInt(strYear));
		}
		if (strMonth != null && !strMonth.trim().equals("")) {
			srcinvDto.setMonth(Integer.parseInt(strMonth));
		}
		if (strBilingPeriod != null) {
			srcinvDto.setInvoicePeriod(Integer.parseInt(strBilingPeriod));
		}
		// comment after codes implement
		if (strAgent != null && !strAgent.equals("")) {
			String[] agentList = strAgent.split(",");
			for (int j = 0; j < agentList.length; j++) {
				agentcol.add(agentList[j]);
			}
			srcinvDto.setAgentCodes(agentcol);
		}
		if (strSlectedInvs != null && !strSlectedInvs.equals("")) {
			String[] invoiceList = strSlectedInvs.split(",");
			for (String invoiceNo : invoiceList) {
				invoicecol.add(invoiceNo);
			}
			srcinvDto.setInoviceNumbers(invoicecol);
		}

		if (strAll != null && strAll.equals("ALL")) {
			srcinvDto.setAgentCodes(null);
		}
		if (strZero != null && strZero.equals("ZERO")) {
			srcinvDto.setFilterZeroInvoices(true);
		}
		if (strInvoiceFrm != null && !strInvoiceFrm.trim().equals("")) {
			String[] strDate = strInvoiceFrm.split("/");
			Date frmDate = new GregorianCalendar(new Integer(strDate[2]).intValue(), new Integer(strDate[1]).intValue() - 1,
					new Integer(strDate[0]).intValue(), 0, 0, 0).getTime();
			srcinvDto.setInvoiceDateFrom(frmDate);
		}
		if (strInvoiceTo != null && !strInvoiceFrm.trim().equals("")) {
			String[] strDate = strInvoiceTo.split("/");
			Date toDate = new GregorianCalendar(new Integer(strDate[2]).intValue(), new Integer(strDate[1]).intValue() - 1,
					new Integer(strDate[0]).intValue(), 23, 59, 59).getTime();
			srcinvDto.setYear(Integer.parseInt(strDate[2]));
			srcinvDto.setMonth(Integer.parseInt(strDate[1]) - 1);
			srcinvDto.setInvoiceDateTo(toDate);
		}
		srcinvDto.setByTrasaction(isByTransaction);
		
		
		if(strEntity != null && !strEntity.equals("")){
			srcinvDto.setEntity(strEntity);
		}
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GENERATE)) { // generate the invoices
				checkPrivilege(request, WebConstants.PRIV_TA_INVOICE_GENERATE);
//				srcinvDto.setAgentCodes(null);
				ServiceResponce responce = invoicingDelegate.generateInvoices(srcinvDto,
						GenerateInvoiceOption.IF_EXISTS_DO_NOT_PROCEED);
				if (responce.isSuccess())
					saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_GENERATE_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);

			}

		} catch (ModuleException e) {
			log.error("InvoiceTransRequestHandler GENERATE() Falied", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_TRANSFER)) { // transfer the invoices
				checkPrivilege(request, WebConstants.PRIV_TA_INVOICE_TRANSFER);
				srcinvDto.setAgentCodes(null);
				invoicingDelegate.transferToInterfaceTables(srcinvDto);
				saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_TRANSFER_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);
			}

		} catch (ModuleException e) {
			log.error("InvoiceTransRequestHandler Transfer() Falied", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EMAIL)) { // email the invoices
				checkPrivilege(request, WebConstants.PRIV_TA_INVOICE_EMAIL);
				audit(request, TasksUtil.TA_EMAIL_INVOICE, srcinvDto.getAgentCodes());
				invoicingDelegate.sendInvoiceMail(srcinvDto);
				saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_SEND_EMAIL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);

			}

		} catch (ModuleException e) {
			log.error("InvoiceTransRequestHandler EMAIL() Falied", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		try {
			setDisplayData(request, userPrincipal.getAgentTypeCode(), userPrincipal.getAgentCode());
			log.debug("InvoiceTransRequestHandler SETDISPLAYDATA() SUCCESS");

		} catch (ModuleException e) {
			log.error("InvoiceTransRequestHandler SETDISPLAYDATA() Falied", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error(e);
		}
		return forward;
	}

	/**
	 * Sets the Display Data for Invoice Transfer Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request, String agentTypeCode, String agentCode) throws ModuleException {

		if (agentCode == null) {
			User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
			agentCode = user.getAgentCode();
		}

		boolean isAllowedToViewAnyInv = isUserTypeGSAHasAccess(request, WebConstants.PRIV_TA_INVOICE_VIEW_ANY, agentTypeCode);
		setClientErrors(request);
		setAgentTypes(request, isAllowedToViewAnyInv);
		setGSAMultiSelectList(request, agentCode, isAllowedToViewAnyInv);
		setInvoiceYear(request);
		setInvoiceRowHtml(request);
		setShowTotal(request, agentCode);
		setInvoicePeriod(request);
		setAppParamSales(request);
		setInvoiceExportStatus(request);
		setEntities(request);
	}

	private static void setShowTotal(HttpServletRequest request, String agentCode) {
		request.setAttribute(WebConstants.REQ_SHOW_TOTAL, agentCode);
	}

	/**
	 * Sets the Client Validations For Invoice Tranfer Page
	 * 
	 * @param request
	 *            the Request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = InvoiceTransHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

	}

	/**
	 * Method to get invoice period details.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setInvoicePeriod(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		sb.append("<option value='1' selected>1</option>");
		sb.append("<option value='2'>2</option>");

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			sb.append("<option value='3'>3</option>");
			sb.append("<option value='4'>4</option>");
			sb.append("<option value='5'>5</option>");
		}

		request.setAttribute(WebConstants.REQ_IS_WEEKLYINVOICE_ENABLED, AppSysParamsUtil.isWeeklyInvoiceEnabled());
		request.setAttribute(WebConstants.REQ_INVOICEPERIOD, sb.toString());
	}

	/**
	 * Method get current year and previous year & set to the Request.
	 * 
	 * @param request
	 *            the ModuleException
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setInvoiceYear(HttpServletRequest request) throws ModuleException {
		String currentYear = null;
		String previousYear = null;
		String selectYear = null;

		StringBuffer sb = new StringBuffer();

		Calendar year = new GregorianCalendar();
		currentYear = Integer.toString(year.get(Calendar.YEAR));
		previousYear = Integer.toString(year.get(Calendar.YEAR) - 1);
		String s[] = new String[4];
		s[0] = currentYear;
		s[1] = currentYear;
		s[2] = previousYear;
		s[3] = previousYear;

		sb.append("<option value='" + s[0] + "' selected>" + s[1] + "</option>");
		sb.append("<option value='" + s[2] + "'>" + s[3] + "</option>");

		selectYear = sb.toString();

		request.setAttribute(WebConstants.REQ_CURRENT_YEAR, currentYear);
		request.setAttribute(WebConstants.REQ_INVOICEYEAR, selectYear);
	}

	/**
	 * Sets the Agents Type List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentTypes(HttpServletRequest request, boolean isAllowedToViewAnyInvoice) throws ModuleException {
		String strATList = "";
		if (isAllowedToViewAnyInvoice) {
			strATList = "<option value='All'>All</option>";
			strATList += SelectListGenerator.createAgentTypeList_SG();
		} else if (hasPrivilege(request, WebConstants.PRIV_TA_INVOICE_VIEW_GSA)) {
			strATList = "<option value='TA'>TA</option>"; // needs to change if required -Thushara
		} else {
			strATList = "<option value='TA'> </option>";
		}
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the Agent List to the Requst by Station
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setGSAMultiSelectList(HttpServletRequest request, String agentCode, boolean isAllowedToViewAnyInvoice)
			throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		
		String agentType = ((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode();

		if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
			if (!strAgentType.isEmpty()) {
				if ("All".equalsIgnoreCase(strAgentType)) {
					strList = JavaScriptGenerator.createAgentsForMultiSelect(null, strAgentType, true, blnWithTAs, blnWithCOs,
							agentType);
				} else if (!strAgentType.isEmpty()) {
					strList = JavaScriptGenerator.createAgentsForMultiSelect(agentCode, strAgentType, false, blnWithTAs,
							blnWithCOs, agentType);
				} else if (hasPrivilege(request, WebConstants.PRIV_TA_INVOICE_VIEW_GSA)) {
					strList = JavaScriptGenerator.createAgentsForMultiSelect(agentCode, strAgentType, false, blnWithTAs,
							blnWithCOs, agentType);
				} else {
					throw new ModuleException(MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN);
				}
			}
		} else {
			if (isAllowedToViewAnyInvoice) {

				strList = ReportsHTMLGenerator.createAgentGSAMultiSelectStation(strAgentType, blnWithTAs, blnWithCOs);
			} else if (hasPrivilege(request, WebConstants.PRIV_TA_INVOICE_VIEW_GSA)) {
				strList = JavaScriptGenerator.createTAsOfGSAMultiSelect(agentCode);
			} else {
				throw new ModuleException(MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * Sets the Invoice Transfer Grid Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setInvoiceRowHtml(HttpServletRequest request) throws ModuleException {
		InvoiceTransHTMLGenerator htmlGen = new InvoiceTransHTMLGenerator();
		String strHtml = htmlGen.getInvoiceTransRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_INVOIVEGEN_DETAILS, strHtml);
	}

	private static void setAppParamSales(HttpServletRequest request) {
		boolean blnIsTrans = AppSysParamsUtil.isTransactInvoiceEnable();
		request.setAttribute("IsTransEnable", blnIsTrans);
	}

	private static void setInvoiceExportStatus(HttpServletRequest request) {
		String strCashSalesStatus = "var isShowReport = " + AppSysParamsUtil.isDownLoadCSVCashSalesReport() + ";";
		request.setAttribute(WebConstants.REQ_VIEW_REPORT_STATUS, strCashSalesStatus);
	}

	private static void setReportView(HttpServletResponse response, Collection<String> invoiceIDList, String printMode)
			throws ModuleException {
		InvoicingBD invoicingDelegate = ModuleServiceLocator.getInvoicingBD();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			String reportTemplate = "invoiceExport.jasper";
			ResultSet resultSet = invoicingDelegate.getInvoiceReportDataSet(invoiceIDList);
			if (AppSysParamsUtil.isAllowCapturePayRef()) {
				parameters.put("IS_ACTUAL_PAYMENT_MODE", "Y");
			} else {
				parameters.put("IS_ACTUAL_PAYMENT_MODE", "N");
			}
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			if ("CSV".equalsIgnoreCase(printMode)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=invoiceView.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			} else if ("PDF".equalsIgnoreCase(printMode)) {
				String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
				String strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			}

		} catch (Exception e) {
			log.error(e);
		}

	}
	
	private static void setEntities(HttpServletRequest request){
		String entityList = "<option></option>";
		try {
			entityList = SelectListGenerator.createEntityListByName();
		} catch (ModuleException e) {			
			e.printStackTrace();
		}
		request.setAttribute(WebConstants.REQ_ENITITY, entityList);
		
	}
}
