package com.isa.thinair.xbe.core.web.action.travagent;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.travagent.InvoiceTransRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.INVOICE_DETAIL),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowInvoiceTransAction extends BaseRequestResponseAwareAction {

	public String execute() throws ModuleException {
		return InvoiceTransRequestHandler.execute(request, response);
	}

}
