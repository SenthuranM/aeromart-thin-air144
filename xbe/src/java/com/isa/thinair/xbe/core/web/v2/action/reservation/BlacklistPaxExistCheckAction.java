package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.utils.BlacklistPAXUtil;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class BlacklistPaxExistCheckAction extends BaseRequestAwareAction {

	/**
	 * @author subash
	 */
	private static Log log = LogFactory.getLog(BlacklistPaxExistCheckAction.class);

	private List<BlacklistPAX> blackListedPaxList;
	private String msgType;
	private String message;
	private String paxInfo;
	private String system;
	private String flow;
	private FlightSearchDTO searchParams;
	private String selectedFlightList;
	private final String CREATE = "C";
	private List<AdultPaxTO> adultPaxTOList;

	private String resSegments;

	public String checkBlackListPaxIncluding() {
		try {

			List<LCCClientReservationPax> paxList = AncillaryJSONUtil.extractPaxDetails(paxInfo, ApplicationEngine.XBE);
			if(paxList != null){
				removeTBAnames(paxList);
			}
			List<String> participatingOperatingCarriers = new ArrayList<String>();

			if (paxList!=null && paxList.size() > 0) {
				if (SYSTEM.INT.equals(SYSTEM.getEnum(this.system))) {
					getParticipatingOpertingCarriers(participatingOperatingCarriers);
				}
				blackListedPaxList = ModuleServiceLocator.getAirproxyReservationBD().getBalcklistedPaxReservation(paxList,
						SYSTEM.getEnum(this.system), true, participatingOperatingCarriers, getTrackInfo());
			} else {
				blackListedPaxList = new ArrayList<BlacklistPAX>();
			}

		} catch (Exception e) {
			msgType = S2Constants.Result.ERROR;
			log.error("error occured : " + e.getCause());
			message = e.getMessage();
		}
		return S2Constants.Result.SUCCESS;
	}
	
	public String checkBlackListPaxIncludingAtPaxInfoUpdate(){
		try {
			List<LCCClientReservationPax> paxList = ReservationUtil.populatePaxAdults(adultPaxTOList);
			removeTBAnames(paxList);
			
			List<String> participatingOperatingCarriers = new ArrayList<String>();

			if (paxList.size() > 0) {
				if (SYSTEM.INT.equals(SYSTEM.getEnum(this.system))) {
					getParticipatingOpertingCarriers(participatingOperatingCarriers);
				}
				blackListedPaxList = ModuleServiceLocator.getAirproxyReservationBD().getBalcklistedPaxReservation(paxList,
						SYSTEM.getEnum(this.system), false, participatingOperatingCarriers, getTrackInfo());
			} else {
				blackListedPaxList = new ArrayList<BlacklistPAX>();
			}		
			//remove already blacklisted pax
			removeAlreadyBlacklistedPax();			
			
		} catch (Exception e) {
			msgType = S2Constants.Result.ERROR;
			log.error("error occured : " + e.getCause());
			message = e.getMessage();
		}
		return S2Constants.Result.SUCCESS;
	}
	
	private void removeAlreadyBlacklistedPax(){
		if(!blackListedPaxList.isEmpty()){
			for(AdultPaxTO aPax: adultPaxTOList){
				if(aPax.isBlacklisted()){
					
					Iterator<BlacklistPAX> itr = blackListedPaxList.iterator();
					while (itr.hasNext()) {
						BlacklistPAX pax = itr.next();
						if (pax.getFullName().equalsIgnoreCase(aPax.getDisplayAdultFirstName()+BlacklistPAXUtil.PAX_NAME_SEPARATOR+aPax.getDisplayAdultLastName()) &&
							pax.getNationality() == Integer.parseInt(aPax.getDisplayAdultNationality())) {
							itr.remove();
						}
					}
										
				}
			}			
		}		
	}
	
	private void getParticipatingOpertingCarriers(List<String> participatingOperatingCarriers) throws Exception {
		try {
			List<FlightSegmentTO> flightSegmentTOs;

			if (CREATE.equalsIgnoreCase(flow)) {
				flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
			} else {
				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(resSegments);
				flightSegmentTOs = AirProxyReservationUtil.populateFlightSegmentTO(colsegs);
			}

			if (flightSegmentTOs != null) {
				for (FlightSegmentTO flightSeg : flightSegmentTOs) {
					if (!participatingOperatingCarriers.contains(flightSeg.getOperatingAirline())) {
						participatingOperatingCarriers.add(flightSeg.getOperatingAirline());
					}
				}
			}

		} catch (ParseException e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		} catch (org.json.simple.parser.ParseException e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		} catch (Exception e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		}

	}

	private void removeTBAnames(List<LCCClientReservationPax> paxList) {

		Iterator<LCCClientReservationPax> itr = paxList.iterator();
		while (itr.hasNext()) {
			LCCClientReservationPax pax = itr.next();
			if (pax.getFirstName().equals("T B A") && pax.getLastName().equals("T B A")) {
				itr.remove();
			}
		}
	}
	
	public List<BlacklistPAX> getBlackListedPaxList() {
		return blackListedPaxList;
	}

	public void setBlackListedPaxList(List<BlacklistPAX> blackListedPaxList) {
		this.blackListedPaxList = blackListedPaxList;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPaxInfo() {
		return paxInfo;
	}

	public void setPaxInfo(String paxInfo) {
		this.paxInfo = paxInfo;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getFlow() {
		return flow;
	}

	public void setFlow(String flow) {
		this.flow = flow;
	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public String getSelectedFlightList() {
		return selectedFlightList;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public String getResSegments() {
		return resSegments;
	}

	public void setResSegments(String resSegments) {
		this.resSegments = resSegments;
	}

	public List<AdultPaxTO> getAdultPaxTOList() {
		return adultPaxTOList;
	}

	public void setAdultPaxTOList(List<AdultPaxTO> adultPaxTOList) {
		this.adultPaxTOList = adultPaxTOList;
	}

}
