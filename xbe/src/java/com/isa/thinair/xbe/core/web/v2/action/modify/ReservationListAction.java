package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.SearchReservationTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ReservationListAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ReservationListAction.class);

	private boolean success = true;

	private String messageTxt;

	private String rSearch;

	private SearchReservationTO searchParams;

	private Collection<ReservationListTO> reservations;

	private int pageNo;

	private int recordsPerPage;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			Date departureDate = new Date();
			Date returnDate = new Date();
			Collection<ReservationDTO> collection = null;

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			String strAgentCode = userPrincipal.getAgentCode();
			recordsPerPage = AppSysParamsUtil.getRecordsPerReservationSearchPage();	
			Collection<String> carrierCodes = userPrincipal.getCarrierCodes();

			String strExactMatch = "";
			boolean isInterlineSearched = false;
			boolean isListReservations = true;
			boolean isWebOrMobileVisible = JavaScriptGenerator.checkWebOrMobileAccess(request);
			
			if ((searchParams.getCreditCardNo().length() == 0) && (searchParams.getAuthCode().length() == 0)) {
				ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();

				if (searchParams.getExactMatch() == null) {
					strExactMatch = "*";
					reservationSearchDTO.setExactMatch(false);
				} else {
					reservationSearchDTO.setExactMatch(true);
				}

				if (searchParams.getFirstName().length() > 0) {
					reservationSearchDTO.setFirstName(searchParams.getFirstName().trim() + strExactMatch);
				}
				if (searchParams.getLastName().length() > 0) {
					reservationSearchDTO.setLastName(searchParams.getLastName().trim() + strExactMatch);
				}
				if (searchParams.getFromAirport() != null && searchParams.getFromAirport().length() > 0) {
					reservationSearchDTO.setFromAirport(searchParams.getFromAirport());
				}
				if (searchParams.getToAirport() != null && searchParams.getToAirport().length() > 0) {
					reservationSearchDTO.setToAirport(searchParams.getToAirport());
				}

				if (searchParams.getPhoneCntry() != null && searchParams.getPhoneCntry().length() > 0) {
					reservationSearchDTO.setTelephoneNo(searchParams.getPhoneCntry().trim() + "-"
							+ searchParams.getPhoneArea().trim() + "-" + searchParams.getPhoneNo().trim());
				}

				if (searchParams.getPhoneNo() != null && searchParams.getPhoneNo().length() > 0) {
					reservationSearchDTO.setTelephoneNoForSQL(BeanUtils.getTelephoneNoForSQL(searchParams.getPhoneCntry(),
							searchParams.getPhoneArea(), searchParams.getPhoneNo()));
				}

				if (searchParams.getDepatureDate() != null && searchParams.getDepatureDate().length() > 0) {
					departureDate = new SimpleDateFormat("dd/MM/yyyy").parse(searchParams.getDepatureDate().trim());
					reservationSearchDTO.setDepartureDate(departureDate);
				}
				if (searchParams.getReturnDate() != null && searchParams.getReturnDate().length() > 0) {
					returnDate = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(searchParams.getReturnDate().trim() + " 23:59");
					reservationSearchDTO.setReturnDate(returnDate);
				}

				if (searchParams.getFlightNo() != null && searchParams.getFlightNo().length() > 0) {
					reservationSearchDTO.setFlightNo(searchParams.getFlightNo().trim());
				}
				if (searchParams.getBookingType() != null && searchParams.getBookingType().length() > 0) {
					reservationSearchDTO.setBookingClassType(searchParams.getBookingType());
				}
				if (searchParams.getPassport() != null && searchParams.getPassport().length() > 0) {
					reservationSearchDTO.setPassport(searchParams.getPassport().trim());
				}
				
				reservationSearchDTO.setOriginChannelIds(BasicRH.getSearchableOriginSalesChannels(request,
						isListReservations));

				if (AppSysParamsUtil.isShowPaxETKT()) {
					if (rSearch != null && rSearch.equals("eticket")) {
						if (searchParams.getPnr().length() > 0) {
							reservationSearchDTO.setETicket(searchParams.getPnr().trim() + strExactMatch);
						}
					} else if (rSearch != null && rSearch.equals("externalEticket")) {
						if (searchParams.getPnr().length() > 0) {
							reservationSearchDTO.setExternalETicket(searchParams.getPnr().trim() + strExactMatch);
						}
					} else if ("invoiceNumber".equalsIgnoreCase(rSearch)) {
						if (StringUtils.isNoneBlank(searchParams.getPnr())) {
							reservationSearchDTO.setInvoiceNumber(searchParams.getPnr().trim() + strExactMatch);
						}
					} else if (rSearch != null && rSearch.equals("gdsRecLocator")) {
						if (searchParams.getPnr().length() > 0) {
							reservationSearchDTO.setExternalRecordLocator(searchParams.getPnr().trim());
						}
					} else {
						if (searchParams.getPnr().length() > 0) {
							reservationSearchDTO.setPnr(searchParams.getPnr().trim());
						}
					}
				} else {
					if (rSearch != null && rSearch.equals("gdsRecLocator") && searchParams.getPnr().length() > 0) {
						reservationSearchDTO.setExternalRecordLocator(searchParams.getPnr().trim());
					} else if (searchParams.getPnr().length() > 0) {
						reservationSearchDTO.setPnr(searchParams.getPnr().trim());
					}
				}
				if (pageNo >= 1) {
					reservationSearchDTO.setPageNo(pageNo);
				} else {
					reservationSearchDTO.setPageNo(1);
				}
				if (searchParams.getSearchSystem() != null && searchParams.getSearchSystem().equals(SYSTEM.INT.toString())
						&& AppSysParamsUtil.isLCCConnectivityEnabled()) {
					reservationSearchDTO.setSearchAll(true);
					isInterlineSearched = true;
					reservationSearchDTO.setSearchableCarriers(carrierCodes);
				}
				
				
				if (!JavaScriptGenerator.checkAccess(request, PriviledgeConstants.ANY_PNR_SEARCH)) {
					if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.ALL_REPORTING_AGENT_PNR_SEARCH)) {
						reservationSearchDTO.setSearchAllReportingAgentBookings(JavaScriptGenerator
								.checkAccess(request, PriviledgeConstants.ALL_REPORTING_AGENT_PNR_SEARCH));
						reservationSearchDTO.setOwnerAgentCode(strAgentCode);
					} else {
						reservationSearchDTO.setSearchReportingAgentBookings(JavaScriptGenerator
								.checkAccess(request, PriviledgeConstants.REPORTING_AGENT_PNR_SEARCH));
						reservationSearchDTO.setOwnerAgentCode(strAgentCode);
					}
					if(!(reservationSearchDTO.getExternalRecordLocator() != null && !reservationSearchDTO.getExternalRecordLocator().isEmpty() 
							&& JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_ACC_GDS_RES))) {
						reservationSearchDTO.setSearchGSABookings(JavaScriptGenerator.checkAccess(request,
								PriviledgeConstants.ANY_GSA_PNR_SEARCH));
						reservationSearchDTO.setSearchIBEBookings(isWebOrMobileVisible);
						reservationSearchDTO.setOwnerAgentCode(strAgentCode);
						reservationSearchDTO.setBookingCategories(this.getBookingCategories());
					}
					
				}

				reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(reservationSearchDTO,
						userPrincipal, TrackInfoUtil.getBasicTrackInfo(userPrincipal));
			} else {
				ReservationPaymentDTO reservationPaymentDTO = new ReservationPaymentDTO();
				if (searchParams.getCreditCardNo() != null && searchParams.getCreditCardNo().length() != 0) {
					if (!JavaScriptGenerator.checkAccess(request, PriviledgeConstants.ANY_PNR_SEARCH)) {
						reservationPaymentDTO.setOwnerAgentCode(strAgentCode);
						reservationPaymentDTO.setSearchGSABookings(JavaScriptGenerator.checkAccess(request,
								PriviledgeConstants.ANY_GSA_PNR_SEARCH));
						reservationPaymentDTO.setSearchIBEBookings(isWebOrMobileVisible);
						reservationPaymentDTO.setBookingCategories(this.getBookingCategories());
						if (JavaScriptGenerator
								.checkAccess(request, PriviledgeConstants.ALL_REPORTING_AGENT_PNR_SEARCH)) {
							reservationPaymentDTO.setSearchAllReportingAgentBookings(JavaScriptGenerator
									.checkAccess(request, PriviledgeConstants.ALL_REPORTING_AGENT_PNR_SEARCH));
						} else {
							reservationPaymentDTO.setSearchReportingAgentBookings(JavaScriptGenerator
									.checkAccess(request, PriviledgeConstants.REPORTING_AGENT_PNR_SEARCH));
						}
					}

					if (searchParams.getExactMatch() == null) {
						strExactMatch = "*";
						reservationPaymentDTO.setExactMatch(false);
					} else {
						reservationPaymentDTO.setExactMatch(true);
					}

					if (searchParams.getCreditCardNo() != null && searchParams.getCreditCardNo().length() > 0) {
						reservationPaymentDTO.setCreditCardNo(searchParams.getCreditCardNo().trim());
					}
					if (searchParams.getCardExpiry() != null && searchParams.getCardExpiry().length() > 0) {
						if (searchParams.getCardExpiry() != null) {
							String cardExpiry = searchParams.getCardExpiry().trim();
							String[] dateList = cardExpiry.split("/");
							if (dateList != null && dateList.length >= 2) {
								cardExpiry = dateList[1] + dateList[0];
							}
							reservationPaymentDTO.setEDate(cardExpiry);
						}
					}
					if (searchParams.getBookingType() != null && searchParams.getBookingType().length() > 0) {
						reservationPaymentDTO.setBookingClassType(searchParams.getBookingType());
					}
				}
				if (searchParams.getAuthCode() != null && searchParams.getAuthCode().length() > 0) {
					reservationPaymentDTO.setCcAuthorCode(searchParams.getAuthCode().trim());
				}
				reservationPaymentDTO.setOriginChannelIds(BasicRH.getSearchableOriginSalesChannels(request,
						isListReservations));
				if (pageNo >= 1) {
					reservationPaymentDTO.setPageNo(pageNo);
				} else {
					reservationPaymentDTO.setPageNo(1);
				}
				if (searchParams.getSearchSystem() != null && searchParams.getSearchSystem().equals(SYSTEM.INT.toString())
						&& AppSysParamsUtil.isLCCConnectivityEnabled()) {
					reservationPaymentDTO.setSearchAll(true);
					isInterlineSearched = true;
					reservationPaymentDTO.setSearchableCarriers(carrierCodes);
				}
				reservations = ModuleServiceLocator.getAirproxyReservationBD().searchReservations(reservationPaymentDTO,
						userPrincipal, getTrackInfo());
			}

			if (isInterlineSearched) {
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_ACCESSED, Boolean.TRUE);
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @return the searchParams
	 */
	public SearchReservationTO getSearchParams() {
		return searchParams;
	}

	/**
	 * @param searchParams
	 *            the searchParams to set
	 */
	public void setSearchParams(SearchReservationTO searchParams) {
		this.searchParams = searchParams;
	}

	/**
	 * @return the reservations
	 */
	public Collection<ReservationListTO> getReservations() {
		return reservations;
	}

	public void setrSearch(String rSearch) {
		this.rSearch = rSearch;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getRecordsPerPage() {
		return recordsPerPage;
	}

	public void setRecordsPerPage(int recordsPerPage) {
		this.recordsPerPage = recordsPerPage;
	}

	/**
	 * @return
	 * @throws ModuleException
	 */
	private Collection<String> getBookingCategories() throws ModuleException {
		Collection<String> privilegeBookingCategories = new ArrayList<String>();
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_STANDARD_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.STANDARD.getCode());
		}
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_CHARTER_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.CHARTER.getCode());
		}
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_HR_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.HR.getCode());
		}
		return privilegeBookingCategories;
	}
}
