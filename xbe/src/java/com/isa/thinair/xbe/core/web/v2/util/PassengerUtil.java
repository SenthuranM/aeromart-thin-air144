package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.XBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.v2.reservation.PaxPriceTO;
import com.isa.thinair.xbe.api.dto.v2.AdultPaxTO;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.InfantPaxTO;
import com.isa.thinair.xbe.api.dto.v2.PaxValidationTO;
import com.isa.thinair.xbe.api.dto.v2.ProfileInfoTO;

public class PassengerUtil {

	private static final String AUTO_CHECKIN_VISIBILITY = "_autoCheckinVisibility";
	private static final String AUTO_CHECKIN_MANDATORY = "_autoCheckinMandatory";

	/**
	 * 
	 * @param customer
	 * @return
	 * @throws ParseException
	 */
	public static ProfileInfoTO createProfileInfo(Customer customer) throws ParseException {
		ProfileInfoTO profileInfoTO = new ProfileInfoTO();

		String[] arrMobile = new String[3];
		arrMobile[0] = "";
		arrMobile[1] = "";
		arrMobile[2] = "";

		String[] arrPhone = new String[3];
		arrPhone[0] = "";
		arrPhone[1] = "";
		arrPhone[2] = "";

		String[] arrFax = new String[3];
		arrFax[0] = "";
		arrFax[1] = "";
		arrFax[2] = "";

		if ((customer.getMobile() != null) && (!customer.getMobile().equals("--"))) {
			arrMobile = StringUtils.split(customer.getMobile(), "-");
		}
		if ((customer.getTelephone() != null) && (!customer.getTelephone().equals("--"))) {
			arrPhone = StringUtils.split(customer.getTelephone(), "-");
		}
		if ((customer.getFax() != null) && (!customer.getFax().equals("--"))) {
			arrFax = StringUtils.split(customer.getFax(), "-");
		}

		profileInfoTO.setTitle(customer.getTitle());
		profileInfoTO.setFirstName(customer.getFirstName());
		profileInfoTO.setLastName(customer.getLastName());
		profileInfoTO.setCity(StringUtil.getNotNullString(customer.getCity()));
		profileInfoTO.setAddressStreet(StringUtil.getNotNullString(customer.getAddressStreet()));
		profileInfoTO.setAddressLine(StringUtil.getNotNullString(customer.getAddressLine()));
		profileInfoTO.setZipCode(StringUtil.getNotNullString(customer.getZipCode()));
		profileInfoTO.setCountry(customer.getCountryCode());
		if (arrMobile.length > 0) {
			profileInfoTO.setMobileCountry(arrMobile[0]);
		} else {
			profileInfoTO.setMobileCountry("");
		}
		if (arrMobile.length > 1) {
			profileInfoTO.setMobileArea(arrMobile[1]);
		} else {
			profileInfoTO.setMobileArea("");
		}
		if (arrMobile.length > 2) {
			profileInfoTO.setMobileNo(arrMobile[2]);
		} else {
			profileInfoTO.setMobileNo("");
		}

		if (arrPhone.length > 0) {
			profileInfoTO.setPhoneCountry(arrPhone[0]);
		} else {
			profileInfoTO.setPhoneCountry(arrPhone[0]);
		}

		if (arrPhone.length > 1) {
			profileInfoTO.setPhoneArea(arrPhone[1]);
		} else {
			profileInfoTO.setPhoneArea("");
		}

		if (arrPhone.length > 2) {
			profileInfoTO.setPhoneNo(arrPhone[2]);
		} else {
			profileInfoTO.setPhoneNo("");
		}
		if (arrFax.length > 0) {
			profileInfoTO.setFaxCountry(arrFax[0]);
		} else {
			profileInfoTO.setFaxCountry("");
		}

		if (arrFax.length > 1) {
			profileInfoTO.setFaxArea(arrFax[1]);
		} else {
			profileInfoTO.setFaxArea("");
		}

		if (arrFax.length > 2) {
			profileInfoTO.setFaxNo(arrFax[2]);
		} else {
			profileInfoTO.setFaxNo("");
		}

		profileInfoTO.setEmail(customer.getEmailId());

		// set emergency contact details
		profileInfoTO.setEmgnTitle(customer.getEmgnTitle());
		profileInfoTO.setEmgnFirstName(customer.getEmgnFirstName());
		profileInfoTO.setEmgnLastName(customer.getEmgnLastName());
		profileInfoTO.setEmgnEmail(customer.getEmgnEmail());

		String[] arrEmgnPhone = new String[3];
		arrEmgnPhone[0] = "";
		arrEmgnPhone[1] = "";
		arrEmgnPhone[2] = "";

		if (customer.getEmgnPhoneNo() != null && (!customer.getEmgnPhoneNo().equals("--"))) {
			arrEmgnPhone = StringUtils.split(customer.getEmgnPhoneNo(), "-");
		}

		if (arrEmgnPhone.length > 0) {
			profileInfoTO.setEmgnPhoneCountry(arrEmgnPhone[0]);
		}

		if (arrEmgnPhone.length > 1) {
			profileInfoTO.setEmgnPhoneArea(arrEmgnPhone[1]);
		}

		if (arrEmgnPhone.length > 2) {
			profileInfoTO.setEmgnPhoneNo(arrEmgnPhone[2]);
		}

		return profileInfoTO;
	}

	/**
	 * 
	 * @param customer
	 * @return
	 * @throws ParseException
	 */
	public static ProfileInfoTO createProfileInfo(ReservationContactInfo rescontinf) throws ParseException {
		ProfileInfoTO profileInfoTO = new ProfileInfoTO();

		String[] arrMobile = new String[3];
		arrMobile[0] = "";
		arrMobile[1] = "";
		arrMobile[2] = "";

		String[] arrPhone = new String[3];
		arrPhone[0] = "";
		arrPhone[1] = "";
		arrPhone[2] = "";

		String[] arrFax = new String[3];
		arrFax[0] = "";
		arrFax[1] = "";
		arrFax[2] = "";

		if ((rescontinf.getMobileNo() != null) && (!rescontinf.getMobileNo().equals("--"))) {
			arrMobile = StringUtils.split(rescontinf.getMobileNo(), "-");
		}
		if ((rescontinf.getPhoneNo() != null) && (!rescontinf.getPhoneNo().equals("--"))) {
			arrPhone = StringUtils.split(rescontinf.getPhoneNo(), "-");
		}
		if ((rescontinf.getFax() != null) && (!rescontinf.getFax().equals("--"))) {
			arrFax = StringUtils.split(rescontinf.getFax(), "-");
		}

		profileInfoTO.setTitle(rescontinf.getTitle());
		profileInfoTO.setFirstName(rescontinf.getFirstName());
		profileInfoTO.setLastName(rescontinf.getLastName());
		profileInfoTO.setCity(rescontinf.getCity());
		profileInfoTO.setCountry(rescontinf.getCountryCode());
		profileInfoTO.setAddressStreet(rescontinf.getStreetAddress1());
		profileInfoTO.setAddressLine(rescontinf.getStreetAddress2());
		profileInfoTO.setZipCode(rescontinf.getZipCode());
		profileInfoTO.setNationality(rescontinf.getNationalityCode());
		if (arrMobile.length > 0) {
			profileInfoTO.setMobileCountry(arrMobile[0]);
		} else {
			profileInfoTO.setMobileCountry("");
		}
		if (arrMobile.length > 1) {
			profileInfoTO.setMobileArea(arrMobile[1]);
		} else {
			profileInfoTO.setMobileArea("");
		}
		if (arrMobile.length > 2) {
			profileInfoTO.setMobileNo(arrMobile[2]);
		} else {
			profileInfoTO.setMobileNo("");
		}

		if (arrPhone.length > 0) {
			profileInfoTO.setPhoneCountry(arrPhone[0]);
		} else {
			profileInfoTO.setPhoneCountry("");
		}

		if (arrPhone.length > 1) {
			profileInfoTO.setPhoneArea(arrPhone[1]);
		} else {
			profileInfoTO.setPhoneArea("");
		}

		if (arrPhone.length > 2) {
			profileInfoTO.setPhoneNo(arrPhone[2]);
		} else {
			profileInfoTO.setPhoneNo("");
		}

		if (arrFax.length > 0) {
			profileInfoTO.setFaxCountry(arrFax[0]);
		} else {
			profileInfoTO.setFaxCountry(arrPhone[0]);
		}

		if (arrFax.length > 1) {
			profileInfoTO.setFaxArea(arrFax[1]);
		} else {
			profileInfoTO.setFaxArea("");
		}

		if (arrFax.length > 2) {
			profileInfoTO.setFaxNo(arrFax[2]);
		} else {
			profileInfoTO.setFaxNo("");
		}

		profileInfoTO.setEmail(rescontinf.getEmail());

		// Set Emergency contact details
		profileInfoTO.setEmgnTitle(rescontinf.getEmgnTitle());
		profileInfoTO.setEmgnFirstName(rescontinf.getEmgnFirstName());
		profileInfoTO.setEmgnLastName(rescontinf.getEmgnLastName());
		profileInfoTO.setEmgnEmail(rescontinf.getEmgnEmail());

		String[] arrEmgnPhone = new String[3];
		arrEmgnPhone[0] = "";
		arrEmgnPhone[1] = "";
		arrEmgnPhone[2] = "";

		if ((rescontinf.getEmgnPhoneNo() != null) && (!rescontinf.getEmgnPhoneNo().equals("--"))) {
			arrEmgnPhone = StringUtils.split(rescontinf.getEmgnPhoneNo(), "-");
		}

		if (arrEmgnPhone.length > 0) {
			profileInfoTO.setEmgnPhoneCountry(arrEmgnPhone[0]);
		} else {
			profileInfoTO.setEmgnPhoneCountry("");
		}

		if (arrEmgnPhone.length > 1) {
			profileInfoTO.setEmgnPhoneArea(arrEmgnPhone[1]);
		} else {
			profileInfoTO.setEmgnPhoneArea("");
		}

		if (arrEmgnPhone.length > 2) {
			profileInfoTO.setEmgnPhoneNo(arrEmgnPhone[2]);
		} else {
			profileInfoTO.setEmgnPhoneNo("");
		}

		return profileInfoTO;
	}

	/**
	 * TODO
	 * 
	 * @param intAdults
	 * @param intChild
	 * @return
	 * @throws ParseException
	 */
	protected static Collection<AdultPaxTO> createAdultList(int intAdults, int intChild, Collection<PaxPriceTO> paxPriceTOs)
			throws ParseException {

		Collection<AdultPaxTO> collection = new ArrayList<AdultPaxTO>();

		BigDecimal totalAdultPrice = getPerPaxPrice(paxPriceTOs, PaxTypeTO.ADULT);
		BigDecimal totalInfantPrice = getPerPaxPrice(paxPriceTOs, PaxTypeTO.INFANT);
		BigDecimal totalAdultPriceWithInfant = AccelAeroCalculator.add(totalAdultPrice, totalInfantPrice);

		int intPaxCount = 0;

		for (int i = 0; i < intAdults; i++) {
			AdultPaxTO adultPaxTO = new AdultPaxTO();
			adultPaxTO.setDisplayAdultCharg(AccelAeroCalculator.formatAsDecimal(totalAdultPrice));

			adultPaxTO.setDisplayAdultInfantCharg(AccelAeroCalculator.formatAsDecimal(totalAdultPriceWithInfant));
			adultPaxTO.setDisplayAdultDOB("");
			adultPaxTO.setDisplayAdultFirstName("");
			adultPaxTO.setDisplayAdultLastName("");
			adultPaxTO.setDisplayAdultNationality("");
			adultPaxTO.setDisplayAdultSSRCode("");
			adultPaxTO.setDisplayAdultSSRInformation("");
			adultPaxTO.setDisplayAdultTitle("");
			adultPaxTO.setDisplayAdultType("AD");
			adultPaxTO.setDisplayPnrPaxCatFOIDNumber("");
			adultPaxTO
					.setDisplayAdultSSR("<input type='button' id='btndisplayAdultSSR"
							+ intPaxCount
							+ "' name='btndisplayAdultSSR' value='SSR' class='ui-state-default ui-corner-all' onclick='UI_tabPassenger.btnSSROnClick({row:"
							+ intPaxCount + ", type:\"ADT\"})'>");
			collection.add(adultPaxTO);

			intPaxCount++;
		}

		if (intChild > 0) {
			for (int i = 0; i < intChild; i++) {
				BigDecimal totalChildPrice = getPerPaxPrice(paxPriceTOs, PaxTypeTO.CHILD);

				AdultPaxTO adultPaxTO = new AdultPaxTO();
				adultPaxTO.setDisplayAdultCharg(AccelAeroCalculator.formatAsDecimal(totalChildPrice));

				adultPaxTO.setDisplayAdultDOB("");
				adultPaxTO.setDisplayAdultFirstName("");
				adultPaxTO.setDisplayAdultLastName("");
				adultPaxTO.setDisplayAdultNationality("");
				adultPaxTO.setDisplayAdultSSRCode("");
				adultPaxTO.setDisplayAdultSSRInformation("");
				adultPaxTO.setDisplayAdultTitle("");
				adultPaxTO.setDisplayAdultType("CH");
				adultPaxTO.setDisplayPnrPaxCatFOIDNumber("");
				adultPaxTO
						.setDisplayAdultSSR("<input type='button' id='btndisplayAdultSSR"
								+ intPaxCount
								+ "' name='btndisplayAdultSSR' value='SSR' class='ui-state-default ui-corner-all' onclick='UI_tabPassenger.btnSSROnClick({row:"
								+ intPaxCount + ", type:\"ADT\"})'>");
				collection.add(adultPaxTO);

				intPaxCount++;
			}
		}
		return collection;
	}

	private static BigDecimal getPerPaxPrice(Collection<PaxPriceTO> paxPriceTOs, String paxType) {
		for (PaxPriceTO paxPriceTO : paxPriceTOs) {
			if (paxPriceTO.getPaxType().equals(paxType)) {
				return new BigDecimal(paxPriceTO.getPaxTotalPrice());
			}
		}

		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	/**
	 * TODO
	 * 
	 * @param intAdults
	 * @param intChild
	 * @return
	 * @throws ParseException
	 */
	protected static List<InfantPaxTO> createInfantList(int infants) throws ParseException {
		List<InfantPaxTO> list = null;

		if (infants > 0) {
			list = new ArrayList<InfantPaxTO>();
			for (int i = 0; i < infants; i++) {
				InfantPaxTO infantPaxTO = new InfantPaxTO();
				infantPaxTO.setDisplayPaxDOB("");
				infantPaxTO.setDisplayPaxFirstName("");
				infantPaxTO.setDisplayPaxLastName("");
				infantPaxTO.setDisplayPaxNationality("");
				infantPaxTO.setDisplayInfantSSRCode("");
				infantPaxTO.setDisplayInfantSSRInformation("");
				infantPaxTO.setDisplayInfantTravellingWith("");
				infantPaxTO.setDisplayPaxType("BABY");
				infantPaxTO
						.setDisplayInfantSSR("<input type='button' id='btndisplayInfantSSRI"
								+ i
								+ "' name='btndisplayInfantSSR' value='SSR' class='ui-state-default ui-corner-all' onclick='UI_tabPassenger.btnSSROnClick({row:"
								+ i + ", type:\"INF\"})'>");
				list.add(infantPaxTO);
			}
		}

		return list;
	}

	public static PaxValidationTO fillPaxValidation() {
		PaxValidationTO paxValidationTO = new PaxValidationTO();
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

		paxValidationTO.setChildAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears());
		paxValidationTO.setAdultAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears());
		paxValidationTO.setInfantAgeCutOverYears(globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getCutOffAgeInYears());

		return paxValidationTO;
	}

	public static Customer createCustomerProfile(ContactInfoTO contactInfo, Customer oldCustObj) {
		Customer customer = new Customer();
		customer.setCustomerId(oldCustObj.getCustomerId());
		customer.setPassword(oldCustObj.getPassword());
		customer.setGender(oldCustObj.getGender());
		customer.setAlternativeEmailId(oldCustObj.getAlternativeEmailId());
		customer.setOfficeTelephone(oldCustObj.getOfficeTelephone());
		customer.setDateOfBirth(oldCustObj.getDateOfBirth());
		customer.setStatus(oldCustObj.getStatus());
		customer.setSecretQuestion(oldCustObj.getSecretQuestion());
		customer.setSecretAnswer(oldCustObj.getSecretAnswer());
		customer.setRegistration_date(oldCustObj.getRegistration_date());
		customer.setConfirmation_date(customer.getConfirmation_date());
		customer.setEmailId(contactInfo.getEmail());

		if (contactInfo.getTitle() != null && (!contactInfo.getTitle().equals(""))) {
			customer.setTitle(contactInfo.getTitle());
		} else {
			customer.setTitle(oldCustObj.getTitle());
		}

		customer.setFirstName(contactInfo.getFirstName());
		customer.setLastName(contactInfo.getLastName());
		customer.setTelephone(contactInfo.getPhoneCountry() + "-" + contactInfo.getPhoneArea() + "-" + contactInfo.getPhoneNo());
		customer.setMobile(contactInfo.getMobileCountry() + "-" + contactInfo.getMobileArea() + "-" + contactInfo.getMobileNo());
		customer.setFax(contactInfo.getFaxCountry() + "-" + contactInfo.getFaxArea() + "-" + contactInfo.getFaxNo());
		customer.setAddressStreet(contactInfo.getStreet());
		customer.setAddressLine(contactInfo.getAddress());
		customer.setZipCode(contactInfo.getZipCode());

		if (contactInfo.getCountry() != null && (!contactInfo.getCountry().equals(""))) {
			customer.setCountryCode(contactInfo.getCountry());
		} else {
			customer.setCountryCode(oldCustObj.getCountryCode());
		}

		if (contactInfo.getNationality() != null && (!contactInfo.getNationality().equals(""))
				&& !contactInfo.getNationality().equalsIgnoreCase("null")) {
			customer.setNationalityCode(Integer.parseInt(contactInfo.getNationality()));
		} else {
			customer.setNationalityCode(oldCustObj.getNationalityCode());
		}

		customer.setCity(contactInfo.getCity());
		customer.setEmgnTitle(contactInfo.getEmgnTitle());
		customer.setEmgnFirstName(contactInfo.getEmgnFirstName());
		customer.setEmgnLastName(contactInfo.getEmgnLastName());
		customer.setEmgnPhoneNo(contactInfo.getEmgnPhoneCountry() + "-" + contactInfo.getEmgnPhoneArea() + "-"
				+ contactInfo.getEmgnPhoneNo());
		customer.setEmgnEmail(contactInfo.getEmgnEmail());
		customer.setSocialCustomer(oldCustObj.getSocialCustomer());
		
		customer.setVersion(oldCustObj.getVersion());
		return customer;
	}

	public static void populatePaxCatConfig(List allConfigList, String paxType, Map<String, XBEPaxConfigDTO> configAD,
			Map<String, XBEPaxConfigDTO> configCH, Map<String, XBEPaxConfigDTO> configIN, Map<String, List<String>> uniqGroupAD,
			Map<String, List<String>> uniqGroupCH, Map<String, List<String>> uniqGroupIN) {
		for (Object obj : allConfigList) {
			if (obj instanceof XBEPaxConfigDTO) {
				XBEPaxConfigDTO config = (XBEPaxConfigDTO) obj;
				if (config.getPaxCatCode().equals(paxType)) {
					if (ReservationInternalConstants.PassengerType.ADULT.equals(config.getPaxTypeCode())) {
						configAD.put(config.getFieldName(), config);
						createUniqPaxGroupMap(config, uniqGroupAD);
					} else if (ReservationInternalConstants.PassengerType.CHILD.equals(config.getPaxTypeCode())) {
						configCH.put(config.getFieldName(), config);
						createUniqPaxGroupMap(config, uniqGroupCH);
					} else if (ReservationInternalConstants.PassengerType.INFANT.equals(config.getPaxTypeCode())) {
						configIN.put(config.getFieldName(), config);
						createUniqPaxGroupMap(config, uniqGroupIN);
					}
				}
			}
		}
	}

	public static void populateCountryWisePaxConfigs(Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs,
			Map<String, Boolean> countryPaxConfigAD, Map<String, Boolean> countryPaxConfigCH,
			Map<String, Boolean> countryPaxConfigIN, String paxType) {

		for (List<PaxCountryConfigDTO> paxCountryConfigs : countryWiseConfigs.values()) {
			for (PaxCountryConfigDTO paxCountryConfig : paxCountryConfigs) {
				if (paxCountryConfig.getPaxCatCode().equals(paxType)) {
					if (ReservationInternalConstants.PassengerType.ADULT.equals(paxCountryConfig.getPaxTypeCode())) {
						fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigAD);
					} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxCountryConfig.getPaxTypeCode())) {
						fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigCH);
					} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxCountryConfig.getPaxTypeCode())) {
						fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigIN);
					}
				}
			}
		}
	}

	private static void fillPaxCountryConfigMap(PaxCountryConfigDTO paxCountryConfig, Map<String, Boolean> countryPaxConfig) {
		String fieldName = paxCountryConfig.getFieldName();
		if (countryPaxConfig.get(fieldName) == null) {
			countryPaxConfig.put(fieldName, paxCountryConfig.isMandatory());
			// This is only for autocheckin ancillary page
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_VISIBILITY, paxCountryConfig.getAutoCheckinVisibility());
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_MANDATORY, paxCountryConfig.getAutoCheckinMandatory());
		} else {
			// applying the most restricted rule
			Boolean currentValue = countryPaxConfig.get(fieldName);
			countryPaxConfig.put(fieldName, (currentValue || paxCountryConfig.isMandatory()));
			// This is only for autocheckin ancillary page
			Boolean autoCheckinVisibility = countryPaxConfig.get(fieldName + AUTO_CHECKIN_VISIBILITY);
			Boolean autoCheckinMandatory = countryPaxConfig.get(fieldName + AUTO_CHECKIN_MANDATORY);
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_VISIBILITY,
					(autoCheckinVisibility || paxCountryConfig.getAutoCheckinVisibility()));
			countryPaxConfig.put(fieldName + AUTO_CHECKIN_MANDATORY,
					(autoCheckinMandatory || paxCountryConfig.getAutoCheckinMandatory()));
		}
	}

	public static void populateContactConfig(List configList, Map<String, XBEContactConfigDTO> configMap,
			Map<String, List<String>> validationGroup) {
		for (Object obj : configList) {
			if (obj instanceof XBEContactConfigDTO) {
				XBEContactConfigDTO xbeConfig = (XBEContactConfigDTO) obj;
				configMap.put(xbeConfig.getFieldName() + xbeConfig.getGroupId(), xbeConfig);
				createContactGroupMap(xbeConfig, validationGroup);

			}
		}
	}

	private static void createContactGroupMap(XBEContactConfigDTO config, Map<String, List<String>> map) {
		List<String> fieldList = new ArrayList<String>();
		if (config.getValidationGroup() != null && !config.getValidationGroup().equals("")) {
			if (map.containsKey(config.getValidationGroup())) {
				fieldList = map.get(config.getValidationGroup());
			}

			fieldList.add(config.getFieldName() + config.getGroupId());
			map.put(config.getValidationGroup(), fieldList);
		}
	}

	/*
	 * Create Uniqueness group map
	 */
	private static void createUniqPaxGroupMap(XBEPaxConfigDTO config, Map<String, List<String>> map) {
		List<String> fieldList = new ArrayList<String>();
		if (config.getUniquenessGroup() != null && !config.getUniquenessGroup().equals("")) {
			if (map.containsKey(config.getUniquenessGroup())) {
				fieldList = map.get(config.getUniquenessGroup());
			}

			fieldList.add(config.getFieldName());
			map.put(config.getUniquenessGroup(), fieldList);
		}
	}

}
