package com.isa.thinair.xbe.core.service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/***
 * This is a scheduler to Notify Passengers about Ancillary
 ***/

public class AncillaryReminderNotification {

	public void sendReminderNotification(Date date) throws ModuleException {
		// ModuleServiceLocator.getReservationBD().sendAncillaryReminderNotification(date);

		AncillaryReminderDetailDTO reminderDetailDTO = new AncillaryReminderDetailDTO();
		reminderDetailDTO.setFlightId(2056359);
		// 12838
		reminderDetailDTO.setFlightSegId(2084137);
		// 13478
		reminderDetailDTO.setAirportOnlineCheckin("Y");
		reminderDetailDTO.setAnciNotificationStartCutOverTime(7200);

		Calendar tempcalendar = new GregorianCalendar();
		/*
		 * tempcalendar.set(Calendar.DATE, 20); tempcalendar.set(Calendar.MONTH, 7-1); tempcalendar.set(Calendar.YEAR,
		 * 2010); tempcalendar.set(Calendar.HOUR, 8); tempcalendar.set(Calendar.MINUTE, 3);
		 * tempcalendar.set(Calendar.SECOND, 07);
		 */
		tempcalendar.set(Calendar.DATE, 20);
		tempcalendar.set(Calendar.MONTH, 7 - 1);
		tempcalendar.set(Calendar.YEAR, 2010);
		tempcalendar.set(Calendar.HOUR, 8);
		tempcalendar.set(Calendar.MINUTE, 3);
		tempcalendar.set(Calendar.SECOND, 07);
		Timestamp tempDeptTimeStamp = new Timestamp(tempcalendar.getTimeInMillis());

		reminderDetailDTO.setDeparturetimeLocal(tempDeptTimeStamp);
		reminderDetailDTO.setDepartureTimeZulu(tempDeptTimeStamp);

		ModuleServiceLocator.getReservationBD().sendAncillaryReminderNotification(reminderDetailDTO);

	}

}
