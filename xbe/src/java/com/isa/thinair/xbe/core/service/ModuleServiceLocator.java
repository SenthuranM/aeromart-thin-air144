package com.isa.thinair.xbe.core.service;

import com.isa.thinair.gdsservices.api.service.TypeAServiceBD;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.utils.AircustomerConstants;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.AutomaticCheckinBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.EventServiceBD;
import com.isa.thinair.airmaster.api.service.GdsBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.AnciChargeLocalCurrBD;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airproxy.api.service.AirProxyFlightServiceBD;
import com.isa.thinair.airproxy.api.service.AirproxyAlertingBD;
import com.isa.thinair.airproxy.api.service.AirproxyAncillaryBD;
import com.isa.thinair.airproxy.api.service.AirproxyPassengerBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.service.AirproxySegmentBD;
import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;
import com.isa.thinair.airreservation.api.service.GroupBookingServiceBD;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.service.GdsRequiredBD;
import com.isa.thinair.gdsservices.api.service.TypeAServiceBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.invoicing.api.service.InvoicingBD;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.lccclient.api.service.LCCPaxCreditBD;
import com.isa.thinair.lccclient.api.service.LCCSearchAndQuoteBD;
import com.isa.thinair.lccclient.api.service.LCCUserAgentSeviceBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messagepasser.api.service.ETLBD;
import com.isa.thinair.messagepasser.api.service.PFSXmlBD;
import com.isa.thinair.messagepasser.api.service.PRLBD;
import com.isa.thinair.messagepasser.api.service.ReconcileCsvBD;
import com.isa.thinair.messagepasser.api.service.SSIMBD;
import com.isa.thinair.messagepasser.api.utils.MessagepasserConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.msgbroker.api.service.MessageBrokerServiceBD;
import com.isa.thinair.msgbroker.api.service.SSMASMServiceBD;
import com.isa.thinair.msgbroker.api.service.TypeBServiceBD;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.service.BunldedFareBD;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.service.VoucherTemplateBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.service.ScheduledReportBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;

public class ModuleServiceLocator {

	private static final Log log = LogFactory.getLog(ModuleServiceLocator.class);

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, XBEModuleUtils.getConfig(), "xbe",
				"config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, XBEModuleUtils.getConfig(), "xbe",
				"config.dependencymap.invalid");
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public final static CommonMasterBD getCommonServiceBD() {

		try {
			return (CommonMasterBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
					CommonMasterBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating CommonMasterBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirmasterConstants.MODULE_NAME);
		}
	}

	public final static SecurityBD getSecurityBD() {
		try {
			return (SecurityBD) ModuleServiceLocator.lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating SecurityBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirsecurityConstants.MODULE_NAME);
		}
	}

	public final static ReservationBD getReservationBD() {
		try {
			return (ReservationBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
					ReservationBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating ReservationBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirreservationConstants.MODULE_NAME);
		}

	}

	public final static MessageBrokerServiceBD getMessageBrokerServiceBD() {
		try {
			return (MessageBrokerServiceBD) ModuleServiceLocator.lookupEJB3Service(MsgbrokerConstants.MODULE_NAME,
					MessageBrokerServiceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating ReservationBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MsgbrokerConstants.MODULE_NAME);
		}
	}

	public final static ReservationQueryBD getReservationQueryBD() {

		try {
			return (ReservationQueryBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
					ReservationQueryBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating ReservationQueryBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirreservationConstants.MODULE_NAME);
		}
	}

	public final static PassengerBD getResPassengerBD() {
		try {
			return (PassengerBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
					PassengerBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating PassengerBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirreservationConstants.MODULE_NAME);
		}
	}

	public final static MessagingServiceBD getMessagingBD() {
		try {
			return (MessagingServiceBD) ModuleServiceLocator.lookupEJB3Service(MessagingConstants.MODULE_NAME,
					MessagingServiceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getMessagingServiceBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MessagingConstants.MODULE_NAME);
		}
	}
	
	public final static EventServiceBD getEventServiceBD() {
		try {
			return (EventServiceBD) ModuleServiceLocator.lookupEJB3Service(MessagingConstants.MODULE_NAME,
					EventServiceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating EventServiceBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MessagingConstants.MODULE_NAME);
		}
	}

	public final static AlertingBD getAlertingBD() {
		try {
			return (AlertingBD) ModuleServiceLocator.lookupServiceBD(AlertingConstants.MODULE_NAME,
					AlertingConstants.BDKeys.ALERTING_SERVICE);
		} catch (Exception e) {
			log.error("Error locating getAlertingBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AlertingConstants.MODULE_NAME);
		}
	}

	public final static TravelAgentBD getTravelAgentBD() {
		try {
			return (TravelAgentBD) ModuleServiceLocator.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
					TravelAgentBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getTravelAgentBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirtravelagentsConstants.MODULE_NAME);
		}
	}
	
	public final static GDSServicesBD getGDServicesBD() {
		try {
			return (GDSServicesBD) ModuleServiceLocator.lookupEJB3Service(GdsservicesConstants.MODULE_NAME,
					GDSServicesBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getTravelAgentBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", GdsservicesConstants.MODULE_NAME);
		}
	}

	public static AirCustomerServiceBD getCustomerBD() {
		try {
			return (AirCustomerServiceBD) ModuleServiceLocator.lookupEJB3Service(AircustomerConstants.MODULE_NAME,
					AirCustomerServiceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getCustomerBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AircustomerConstants.MODULE_NAME);
		}
	}

	public static ChargeBD getChargeBD() {
		try {
			return (ChargeBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getChargeBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirpricingConstants.MODULE_NAME);
		}
	}
	
	public static FareBD getFareBD() {
		try {
			return (FareBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getChargeBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirpricingConstants.MODULE_NAME);
		}
	}
	
	public static AnciChargeLocalCurrBD getAnciChargeLocalCurrBD(){
		try{
			return (AnciChargeLocalCurrBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME, AnciChargeLocalCurrBD.SERVICE_NAME);
		}catch(Exception e){
			log.error("Error locating getAnciChargeLocalCurrBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirpricingConstants.MODULE_NAME);
		}
	}

	/**
	 * gets the auditor BD
	 * 
	 * @return
	 */
	public static AuditorBD getAuditorBD() {
		try {
			return (AuditorBD) ModuleServiceLocator.lookupServiceBD(AuditorConstants.MODULE_NAME,
					AuditorConstants.BDKeys.AUDITOR_SERVICE);
		} catch (Exception e) {
			log.error("Error locating getAuditorBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AuditorConstants.MODULE_NAME);
		}
	}

	public final static FlightBD getFlightServiceBD() {

		FlightBD flightDelegate = null;
		try {
			flightDelegate = (FlightBD) ModuleServiceLocator.lookupEJB3Service(AirschedulesConstants.MODULE_NAME,
					FlightBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating FlightServiceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirschedulesConstants.MODULE_NAME);
		}
		return flightDelegate;
	}
	
	public final static FlightInventoryBD getFlightInventoryBD() {

		FlightInventoryBD flightInventoryBD = null;
		try {
			flightInventoryBD = (FlightInventoryBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
					FlightInventoryBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating FlightServiceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirinventoryConstants.MODULE_NAME);
		}
		return flightInventoryBD;
	}

	public final static AirportBD getAirportBD() {

		AirportBD airportDelegate = null;
		try {
			airportDelegate = (AirportBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
					AirportBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirportBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirmasterConstants.MODULE_NAME);
		}
		return airportDelegate;
	}

	public final static ReservationAuxilliaryBD getReservationAuxilliaryBD() {

		ReservationAuxilliaryBD reservationDelegate = null;
		try {
			reservationDelegate = (ReservationAuxilliaryBD) ModuleServiceLocator.lookupEJB3Service(
					AirreservationConstants.MODULE_NAME, ReservationAuxilliaryBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getReservationAuxilliaryBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirreservationConstants.MODULE_NAME);
		}
		return reservationDelegate;
	}

	/**
	 * Added By ChamindaP Gets the reporting frame work.
	 * 
	 * @return
	 */
	public final static ReportingFrameworkBD getReportingFrameworkBD() {
		ReportingFrameworkBD reportingFrameworkDelegate = null;

		try {
			reportingFrameworkDelegate = (ReportingFrameworkBD) ModuleServiceLocator.lookupServiceBD(
					ReportingframeworkConstants.MODULE_NAME, ReportingframeworkConstants.BDKeys.REPORTINGFRAMEWORK_SERVICE);

		} catch (Exception e) {
			log.error("Error locating ReportingFrameworkBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", ReportingConstants.MODULE_NAME);
		}
		return reportingFrameworkDelegate;
	}

	public final static DataExtractionBD getDataExtractionBD() {

		DataExtractionBD reprotingDelegate = null;
		try {
			reprotingDelegate = (DataExtractionBD) ModuleServiceLocator.lookupEJB3Service(ReportingConstants.MODULE_NAME,
					DataExtractionBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating DataExtractionBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", ReportingConstants.MODULE_NAME);
		}
		return reprotingDelegate;
	}

	public final static PaymentBrokerBD getPaymentBrokerBD() {

		try {
			return (PaymentBrokerBD) ModuleServiceLocator.lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME,
					PaymentBrokerBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating PaymentBrokerBD", e);
			throw new XBERuntimeException(e, "common.paymentbrokerbd.loading.error", PaymentbrokerConstants.MODULE_NAME);
		}
	}

	/**
	 * Gets an Airport Service from Air master
	 * 
	 * @return AirportBD the Airport delegate
	 */
	public final static AirportBD getAirportServiceBD() {

		AirportBD airportDelegate = null;
		try {
			airportDelegate = (AirportBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
					AirportBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirportServiceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirmasterConstants.MODULE_NAME);
		}
		return airportDelegate;
	}

	/**
	 * Gets an GDS Service from GDSServices
	 * 
	 * @return GDSServicesBD the GDS Services delegate
	 */
	public final static GDSServicesBD getGDSServicesBD() {
		GDSServicesBD gdsServicesBD = null;
		try {
			gdsServicesBD = (GDSServicesBD) ModuleServiceLocator.lookupEJB3Service(GdsservicesConstants.MODULE_NAME,
					GDSServicesBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating GDSServicesBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", GdsservicesConstants.MODULE_NAME);
		}
		return gdsServicesBD;
	}

	public final static GdsBD getGDSBD() {
		GdsBD gdsBD = null;
		try {
			gdsBD = (GdsBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, GdsBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating GDSBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirmasterConstants.MODULE_NAME);
		}
		return gdsBD;
	}

	public final static SSMASMServiceBD getSSMASMServiceBD() {
		SSMASMServiceBD sSMASMServiceBD = null;
		try {
			sSMASMServiceBD = (SSMASMServiceBD) ModuleServiceLocator.lookupEJB3Service(MsgbrokerConstants.MODULE_NAME,
					SSMASMServiceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating GDSServicesBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MsgbrokerConstants.MODULE_NAME);
		}
		return sSMASMServiceBD;
	}

	public final static TypeBServiceBD getTypeBServiceBD() {
		return (TypeBServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, TypeBServiceBD.SERVICE_NAME);
	}

	public final static TypeAServiceBD getTypeAServiceBD() {
		return (TypeAServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, TypeAServiceBD.SERVICE_NAME);
	}

	/**
	 * Gets the Location Service from Air Master
	 * 
	 * @return LocationBD the Location delegate
	 */
	public final static LocationBD getLocationServiceBD() {

		LocationBD locationDelegate = null;
		try {
			locationDelegate = (LocationBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
					LocationBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating LocationServiceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirmasterConstants.MODULE_NAME);
		}
		return locationDelegate;
	}

	/**
	 * Gets Travel Agent Finance Service from Air Travel Agent
	 * 
	 * @return TravelAgentFinanceBD the Travel Agent Finance delegate
	 */
	public final static TravelAgentFinanceBD getTravelAgentFinanceBD() {

		TravelAgentFinanceBD financeDelegate = null;
		try {
			financeDelegate = (TravelAgentFinanceBD) ModuleServiceLocator.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
					TravelAgentFinanceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getTravelAgentFinanceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirtravelagentsConstants.MODULE_NAME);
		}
		return financeDelegate;
	}

	public final static InvoicingBD getInvoicingBD() {
		InvoicingBD invoicingDelegate = null;
		try {
			invoicingDelegate = (InvoicingBD) ModuleServiceLocator.lookupEJB3Service(InvoicingConstants.MODULE_NAME,
					InvoicingBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getInvoicingBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", InvoicingConstants.MODULE_NAME);
		}
		return invoicingDelegate;
	}

	public static CryptoServiceBD getCryptoServiceBD() {
		CryptoServiceBD cryptoServiceBD = null;
		try {
			cryptoServiceBD = (CryptoServiceBD) lookupServiceBD(CryptoConstants.MODULE_NAME,
					CryptoConstants.BDKeys.CRYPTO_SERVICE);
		} catch (Exception e) {
			log.error("Error locating CryptoServiceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", CryptoConstants.MODULE_NAME);
		}
		return cryptoServiceBD;
	}

	/**
	 * gets the lcc client biz delegate
	 * 
	 * @return LCCSearchAndQuoteBD
	 */
	public final static LCCSearchAndQuoteBD getLCCSearchAndQuoteBD() {
		LCCSearchAndQuoteBD lccSearchAndQuoteBD = null;
		try {
			lccSearchAndQuoteBD = (LCCSearchAndQuoteBD) ModuleServiceLocator.lookupEJB3Service(LccclientConstants.MODULE_NAME,
					LCCSearchAndQuoteBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getLCCSearchAndQuoteBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", LccclientConstants.MODULE_NAME);
		}
		return lccSearchAndQuoteBD;
	}

	public final static LCCUserAgentSeviceBD getLCCUserAgentSeviceBD() {
		LCCUserAgentSeviceBD lccUserAgentSeviceBD = null;
		try {
			lccUserAgentSeviceBD = (LCCUserAgentSeviceBD) ModuleServiceLocator.lookupEJB3Service(LccclientConstants.MODULE_NAME,
					LCCUserAgentSeviceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error location LCCUserAgentSeviceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", LccclientConstants.MODULE_NAME);
		}
		return lccUserAgentSeviceBD;
	}

	public final static LCCPaxCreditBD getLCCPaxCreditBD() {
		LCCPaxCreditBD lccPaxCreditBD = null;
		try {
			lccPaxCreditBD = (LCCPaxCreditBD) ModuleServiceLocator.lookupEJB3Service(LccclientConstants.MODULE_NAME,
					LCCPaxCreditBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error location LCCPaxCreditBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", LccclientConstants.MODULE_NAME);
		}
		return lccPaxCreditBD;
	}

	/**
	 * Gets an Aircraft Service fro Air Master
	 * 
	 * @return AircraftBD the Aircraft delegate
	 */
	public final static AircraftBD getAircraftServiceBD() {

		AircraftBD aircraftDelegate = null;
		try {
			aircraftDelegate = (AircraftBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
					AircraftBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AircraftServiceBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirmasterConstants.MODULE_NAME);
		}
		return aircraftDelegate;
	}

	/**
	 * Get AirproxyReservationQuery BD
	 * 
	 * @return AirproxySearchAndQuoteBD the Proxy Search and Quote Delegate
	 */
	public final static AirproxyReservationQueryBD getAirproxySearchAndQuoteBD() {
		AirproxyReservationQueryBD airproxySearchAndQuoteBD = null;
		try {
			airproxySearchAndQuoteBD = (AirproxyReservationQueryBD) ModuleServiceLocator.lookupEJB3Service(
					AirproxyConstants.MODULE_NAME, AirproxyReservationQueryBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxySearchAndQuoteBD :", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirproxyConstants.MODULE_NAME);
		}
		return airproxySearchAndQuoteBD;
	}

	/**
	 * Get AirproxyAncillary BD
	 * 
	 * @return AirproxyAncillaryBD the Proxy Ancillary Delegate
	 */
	public final static AirproxyAncillaryBD getAirproxyAncillaryBD() {
		AirproxyAncillaryBD airproxyAncillary = null;
		try {
			airproxyAncillary = (AirproxyAncillaryBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyAncillaryBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyAncillaryBD :", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirproxyConstants.MODULE_NAME);
		}
		return airproxyAncillary;
	}

	/**
	 * Get AirproxyAncillary BD
	 * 
	 * @return AirproxyAncillaryBD the Proxy Reservation Delegate
	 */
	public final static AirproxyReservationBD getAirproxyReservationBD() {
		AirproxyReservationBD airproxyReservation = null;
		try {
			airproxyReservation = (AirproxyReservationBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyReservationBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyReservationBD :", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirproxyConstants.MODULE_NAME);
		}
		return airproxyReservation;
	}

	/**
	 * Get AirproxyPassengerBD
	 * 
	 * @return AirproxyPassengerBD the Proxy Reservation Delegate
	 */
	public final static AirproxyPassengerBD getAirproxyPassengerBD() {
		AirproxyPassengerBD airproxyPassenger = null;
		try {
			airproxyPassenger = (AirproxyPassengerBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyPassengerBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyPassengerBD :", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirproxyConstants.MODULE_NAME);
		}
		return airproxyPassenger;
	}

	/**
	 * Get AirproxySegmentBD
	 * 
	 * @return AirproxySegmentBD the Proxy Reservation Delegate
	 */
	public final static AirproxySegmentBD getAirproxySegmentBD() {
		AirproxySegmentBD airproxySegment = null;
		try {
			airproxySegment = (AirproxySegmentBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxySegmentBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxySegmentBD :", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirproxyConstants.MODULE_NAME);
		}
		return airproxySegment;
	}

	/**
	 * @return The {@link AirproxyAlertingBD} The Proxy Alerting Delegate.
	 */
	public final static AirproxyAlertingBD getAirproxyAlertingBD() {
		AirproxyAlertingBD airproxyAlertingBD = null;
		try {
			airproxyAlertingBD = (AirproxyAlertingBD) ModuleServiceLocator.lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyAlertingBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyAlertingBD :", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirproxyConstants.MODULE_NAME);
		}
		return airproxyAlertingBD;
	}

	public final static AirProxyFlightServiceBD getAirProxyFlightServiceBD() {
		AirProxyFlightServiceBD airProxyFlightServiceBD = null;
		try {
			airProxyFlightServiceBD = (AirProxyFlightServiceBD) ModuleServiceLocator.lookupEJB3Service(
					AirproxyConstants.MODULE_NAME, AirProxyFlightServiceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyAlertingBD :", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirproxyConstants.MODULE_NAME);
		}
		return airProxyFlightServiceBD;
	}

	public final static ReconcileCsvBD getReconcileCsvBD() {

		ReconcileCsvBD airportDelegate = null;
		try {
			airportDelegate = (ReconcileCsvBD) ModuleServiceLocator.lookupEJB3Service(MessagepasserConstants.MODULE_NAME,
					ReconcileCsvBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getReconcileCsvBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MessagepasserConstants.MODULE_NAME);
		}
		return airportDelegate;
	}

	public final static ETLBD getETLBD() {
		try {
			return (ETLBD) ModuleServiceLocator.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, ETLBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating ETLBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MessagepasserConstants.MODULE_NAME);
		}

	}

	public final static PRLBD getPRLBD() {
		try {
			return (PRLBD) ModuleServiceLocator.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, PRLBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating PRLBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MessagepasserConstants.MODULE_NAME);
		}

	}

	public final static ScheduledReportBD getScheduledReportBD() {
		return (ScheduledReportBD) ModuleServiceLocator.lookupEJB3Service(ReportingConstants.MODULE_NAME,
				ScheduledReportBD.SERVICE_NAME);
	}

	/**
	 * Gets Schedule Service(Flight) BD
	 * 
	 * @return ScheduleBD the Flight schedule delegate
	 */
	public final static ScheduleBD getScheduleServiceBD() {
		return (ScheduleBD) ModuleServiceLocator.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	public static SegmentBD getResSegmentBD() {
		return (SegmentBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static GroupBookingServiceBD getGroupBookingBD() {
		return (GroupBookingServiceBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				GroupBookingServiceBD.SERVICE_NAME);
	}
	
	public static PFSXmlBD getPfsXmlBD() {
		return (PFSXmlBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, PFSXmlBD.SERVICE_NAME);
	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, LoyaltyManagementBD.SERVICE_NAME);
	}
	
	public static final BunldedFareBD getBundledFareBD() {
		return (BunldedFareBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME, BunldedFareBD.SERVICE_NAME);
	}
	
	public final static LmsMemberServiceBD getLmsMemberServiceBD() {
		return (LmsMemberServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME,
				LmsMemberServiceBD.SERVICE_NAME);

	}

	public final static GdsRequiredBD getGdsRequiredBD() {
		try {
			return (GdsRequiredBD) ModuleServiceLocator.lookupEJB3Service(GdsservicesConstants.MODULE_NAME,
					GdsRequiredBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getGdsRequiredBD: ", e);
			throw new XBERuntimeException(e, "module.lookup.failed", GdsservicesConstants.MODULE_NAME);
		}
	}

	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}
	
	public final static SSIMBD getSSIMBD() {
		try {
			return (SSIMBD) ModuleServiceLocator.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, SSIMBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating SSIMBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", MessagepasserConstants.MODULE_NAME);
		}

	}

	public final static VoucherBD getVoucherBD() {
		VoucherBD voucherDelegate = null;
		try {
			voucherDelegate = (VoucherBD) lookupEJB3Service(PromotionConstants.MODULE_NAME,
					VoucherBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating VoucherBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", PromotionConstants.MODULE_NAME);
		}
		return voucherDelegate;
	}

	public final static AirproxyVoucherBD getAirproxyVoucherBD() {
		AirproxyVoucherBD airproxyVoucherBD = null;
		try {
			airproxyVoucherBD = (AirproxyVoucherBD) lookupEJB3Service(PromotionConstants.MODULE_NAME,
					AirproxyVoucherBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyVoucherBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", PromotionConstants.MODULE_NAME);
		}
		return airproxyVoucherBD;
	}

	public final static VoucherTemplateBD getVoucherTemplateBD() {
		VoucherTemplateBD voucherTemplateDelegate = null;
		try {
			voucherTemplateDelegate = (VoucherTemplateBD) lookupEJB3Service(PromotionConstants.MODULE_NAME,
					VoucherTemplateBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating VoucherBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", PromotionConstants.MODULE_NAME);
		}
		return voucherTemplateDelegate;
	}
	
	public final static GDSNotifyBD getGDSNotifyBD() {
		GDSNotifyBD gdsNotifyBD = null;
		try {
			gdsNotifyBD = (GDSNotifyBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, GDSNotifyBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating GDSNotifyBD", e);
			throw new XBERuntimeException(e, "module.lookup.failed", AirmasterConstants.MODULE_NAME);
		}
		return gdsNotifyBD;
	}
	
	public final static BlacklistPAXBD getBlacklisPAXBD() {
		return (BlacklistPAXBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				BlacklistPAXBD.SERVICE_NAME);
	}
	
	public final static FlightInventoryResBD getFlightInventoryReservationBD() {
		return (FlightInventoryResBD) ModuleServiceLocator.lookupEJB3Service("airinventory",
				FlightInventoryResBD.SERVICE_NAME);
	}
	
	public final static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionManagementBD.SERVICE_NAME);
	}	
	
	public final static AutomaticCheckinBD getAutomaticCheckinBD() {
		return (AutomaticCheckinBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				AutomaticCheckinBD.SERVICE_NAME);
	}

}
