package com.isa.thinair.xbe.core.web.handler.flight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.flight.FlightHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Thushara
 * 
 */
public final class FlightRequestHandler extends BasicRH {

	private static String PARAM_TIMEMODE = "";
	private static Log log = LogFactory.getLog(FlightRequestHandler.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * Execute Method for Flight Search for MANIFEST Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = S2Constants.Result.SUCCESS;
		String strMode = request.getParameter("hdnMode");
		if (strMode != null && strMode.equals("LOAD")) {
			// if LOAD, jasper report will be shown, no need for forward
			forward = "";
		}
		if (strMode != null && strMode.equals("SEARCH")) {
			// if SEARCH, ajax cal will proceed
			forward = S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX;
		}
		try {
			setDisplayData(request, response);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in FlightRequestHandler.execute - setDisplayData(request) [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

			if (moduleException.getExceptionCode().equals("reporting.date.exceeds.max.duration")) {
				forward = S2Constants.Result.SUCCESS;
			}

		} catch (Exception e) {
			log.error("Exception in FlightRequestHandler.execute - setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Sets the Display Data for the MANIFEST Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		setClientErrors(request);
		setOperationTypeHtml(request);
		setLiveStatus(request);
		setStatusHtml(request);
		setFlightRowHtml(request, response);
		setTimeInModeHtml(request);
		setOnlineAirportHtml(request);
		setMaxmiumConnectionLimit(request);
		setMinimiumConnectionLimit(request);
		setInterlineCarrierCodeHtml(request);
		setDisplayOptions(request);
	}

	/**
	 * Set Maximum Connection Time to the Session
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setMaxmiumConnectionLimit(HttpServletRequest request) throws ModuleException {
		String strHtml = globalConfig.getBizParam(SystemParamKeys.MAX_TRANSIT_TIME);
		if (strHtml != null)
			request.getSession().setAttribute(WebConstants.REQ_CONN_LIMIT, "connLimit='" + strHtml + "'");
	}

	/**
	 * Sets Online & Active Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {
		Map<String, String> airports = null;
		if (AppSysParamsUtil.isLCCConnectivityEnabled()
				&& BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_VIEW_OTHER_FLIGHT_MANIFEST)) {

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			if (userPrincipal.getCarrierCodes() != null) {
				airports = SelectListGenerator.createAirportsWithoutSubstations(true, null, userPrincipal.getCarrierCodes());
			} else {
				airports = SelectListGenerator.createAirportsWithoutSubstations(false, null, null);
			}

		} else {
			airports = SelectListGenerator.createAirportsWithoutSubstations(false, null, null);
		}

		StringBuffer sb = new StringBuffer();
		List<String> list = new ArrayList<String>(airports.keySet());
		Collections.sort(list);
		for (String airportCode : list) {
			sb.append("<option value='" + airportCode + "'>" + airportCode + "</option>");
		}

		request.getSession().setAttribute(WebConstants.SES_HTML_AIRPORT_DATA, sb.toString());
	}

	/**
	 * Sets the Operation Types of Flight to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationType();
		request.getSession().setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);
	}

	/**
	 * Sets the Status of the Fligt to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightStatus();
		request.getSession().setAttribute(WebConstants.SES_HTML_STATUS_DATA, strHtml);
	}

	/**
	 * Sets the Flight Grid Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightRowHtml(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
		String strReturnData = htmlGen.getFlightRowHtml(request, response);
		request.setAttribute("returnData", strReturnData);
	}
	
	/**
	 * Sets the Flight manifest display options 
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayOptions(HttpServletRequest request) throws ModuleException {
		FlightHTMLGenerator.setDisplayOptionsHTML(request);		
	}
	

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = FlightHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Time Mode to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setTimeInModeHtml(HttpServletRequest request) throws ModuleException {

		String strTimeinMode = request.getParameter(PARAM_TIMEMODE) == null ? "" : request.getParameter(PARAM_TIMEMODE);
		String strHtml = "";
		if (strTimeinMode.equals(WebConstants.LOCALTIME))
			strHtml = WebConstants.LOCALTIME;
		else
			strHtml = WebConstants.ZULUTIME;

		request.setAttribute(WebConstants.REQ_HTML_TIMEIN, strHtml);
	}

	/**
	 * Sets the Minimum Connection Time to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setMinimiumConnectionLimit(HttpServletRequest request) throws ModuleException {
		String strHtml = globalConfig.getBizParam(SystemParamKeys.MIN_TRANSIT_TIME);
		if (strHtml != null)
			request.getSession().setAttribute(WebConstants.REQ_MIN_CONN_LIMIT, "minConnLimit='" + strHtml + "'");

	}

	private static void setInterlineCarrierCodeHtml(HttpServletRequest request) {
		String strHtml = SelectListGenerator.createInterlineCarrierSelectListOptions(null);
		request.setAttribute(WebConstants.REQ_CARRIER_CODE_LIST_OPTIONS, strHtml);
	}
	
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_HDN_LIVE, strLive);
	}

}
