package com.isa.thinair.xbe.core.web.v2.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCCPaymentMetaInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.AuthorizationsUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.Constants.ReservationFlow;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.PaxSummaryCreditFETO;
import com.isa.thinair.xbe.api.dto.v2.PaymentTO;
import com.isa.thinair.xbe.api.dto.v2.PnrCreditFETO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBEException;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

public class BookingUtil {

	public static CommonReservationAssembler createBookingDetails(String strInfants, ContactInfoTO contactInfo, PaymentTO payment,
			CreditCardTO creditInfo, Integer ipgId, BookingShoppingCart bookingShoppingCart, boolean onHoldBooking,
			boolean forceConfirm, String selectedCurrencyCode, String paxCredit, boolean partialPaxPaymentAllowed,
			IPGResponseDTO externalPGWResponse, UserPrincipal userPrincipal) throws ModuleException, ParseException {

		CommonReservationAssembler lcclientReservationAssembler = new CommonReservationAssembler();
		lcclientReservationAssembler.setOnHoldBooking(onHoldBooking);
		lcclientReservationAssembler.setForceConfirm(forceConfirm || partialPaxPaymentAllowed);
		lcclientReservationAssembler.setAgentCode(payment.getAgent());

		PaymentMethod paymentMethod = payment.getPaymentMethod();

		PaymentHolder paymentHolder = new PaymentHolder();

		if (bookingShoppingCart.getPayByVoucherInfo() != null) {
			paymentHolder.addVoucherPayment(bookingShoppingCart.getPayByVoucherInfo().getVoucherDTOList());
		}

		if (BookingUtil.isDoubleAgentPay(payment)) {
			if (payment.getAgentOneAmount() != null && !"".equals(payment.getAgentOneAmount().trim())) {
				payment.setAmount(payment.getAgentOneAmount());
			}

			PaymentTO second = payment.clone();
			second.setAgent(payment.getAgentTwo());
			second.setAmount(payment.getAgentTwoAmount());

			paymentHolder.addPayment(payment);
			paymentHolder.addPayment(second);
		} else if (creditInfo != null) {
			paymentHolder.addCardPayment(payment, creditInfo);
		} else {
			paymentHolder.addPayment(payment);
		}

		paymentHolder.setPartialPaxPaymentAllowed(partialPaxPaymentAllowed);

		CreditNSummaryHolder creditSummaryHolder = new CreditNSummaryHolder(
				JSONFETOParser.parseCollection(ArrayList.class, PaxSummaryCreditFETO.class, paxCredit));

		int noOfAdults = bookingShoppingCart.getPaxList().size();

		ExternalChargesMediator externalChargesMediator;
		// ExternalChargesMediator creditExtChargesMediator;

		boolean isAdminFeeEnabled = AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, userPrincipal.getAgentCode());

		if (isAdminFeeEnabled) {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
		} else {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart,
					PaymentMethod.CRED.getCode().equals(paymentMethod.getCode()));
		}
		externalChargesMediator.setCalculateJNTaxForCCCharge(true);

		if (!lcclientReservationAssembler.isForceConfirm() && paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
			externalChargesMediator.setBSPPayment(true);
		} else {
			bookingShoppingCart.removeSelectedExternalCharge(EXTERNAL_CHARGES.BSP_FEE);
		}

		// Calculate CC & Handling charge JN tax
		com.isa.thinair.xbe.core.web.v2.util.ReservationUtil.getApplicableServiceTax(bookingShoppingCart,
				EXTERNAL_CHARGES.JN_OTHER);

		LinkedList perPaxExternalCharges = null;
		if (isAdminFeeEnabled) {
			perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(noOfAdults,
					bookingShoppingCart.getPayableInfantPaxCount());
		} else {
			if (creditSummaryHolder.getTotalPaxWithoutFullCreditPayment(PaxTypeTO.ADULT, PaxTypeTO.CHILD,
					PaxTypeTO.INFANT) == 0) {
				perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(noOfAdults,
						bookingShoppingCart.getPayableInfantPaxCount());
			} else {
				perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(
						creditSummaryHolder.getTotalPaxWithoutFullCreditPayment(PaxTypeTO.ADULT, PaxTypeTO.CHILD),
						bookingShoppingCart.getPayableInfantPaxCount());
			}
		}

		int intSeq = 1;

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		PayCurrencyDTO payCurrencyDTO = null;
		PayCurrencyDTO cardPayCurrencyDTO = null;
		PayCurrencyDTO onAccPayCurrencyDTO = null;
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		DiscountedFareDetails discountedFareDetails = bookingShoppingCart.getFareDiscount();
		String discountApplyAs = null;
		Map<Integer, BigDecimal> paxWiseDiscount = null;

		if (discountedFareDetails != null) {
			if (discountedFareDetails.getPromotionId() != null && !bookingShoppingCart.isSkipPromotionForOnHold()) {
				discountApplyAs = discountedFareDetails.getDiscountAs();
				paxWiseDiscount = com.isa.thinair.xbe.core.web.v2.util.CommonUtil.getPaxWisePromoDiscount(bookingShoppingCart);
			}
		}
		ReservationDiscountDTO resDiscountDTO = bookingShoppingCart.getReservationDiscountDTO();

		Map<Integer, LCCClientPaymentAssembler> adultInfantPaymentAssemblerMap = new HashMap<Integer, LCCClientPaymentAssembler>();
		Map<Integer, List<LCCClientExternalChgDTO>> infantExternalChargesByAdultPaxSeq = new HashMap<>();
		for (ReservationPaxTO reservationPax : bookingShoppingCart.getPaxList()) {
			String paxType = reservationPax.getPaxType();
			Integer paxsequence = reservationPax.getSeqNumber();
			boolean isParent = reservationPax.getIsParent();
			// boolean blnCreditPax = false;
			Integer nationality = reservationPax.getNationality();

			LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();
			List<LCCClientExternalChgDTO> infantExternalCharges = null;
			lccClientPaymentAssembler.setPaxType(CommonUtil.getActualPaxType(reservationPax));

			// skip adding transaction fee if it is a credit card payment - for admin fee regulation

			if (perPaxExternalCharges.size() > 0) {
				if (isAdminFeeEnabled || !creditSummaryHolder.isPaxFullyPaidByCredit(paxsequence)) {
					lccClientPaymentAssembler.addExternalCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
				}
			}

			lccClientPaymentAssembler.getPerPaxExternalCharges().addAll(reservationPax.getExternalCharges());
			if (isParent) {
				infantExternalCharges = reservationPax.getInfantExternalCharges();
				if (infantExternalCharges == null) {
					infantExternalCharges = new ArrayList<>();
				}
				if (perPaxExternalCharges.size() > 0) {
					infantExternalCharges
							.addAll(converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop()));
				}
				infantExternalChargesByAdultPaxSeq.put(paxsequence, infantExternalCharges);
			}

			if (!lcclientReservationAssembler.isOnHoldBooking()
					|| paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {

				if (creditSummaryHolder.hasPayment(reservationPax.getSeqNumber())) {

					if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
						if (onAccPayCurrencyDTO == null) {
							String loggedInAgencyCode = null;
							onAccPayCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(selectedCurrencyCode, loggedInAgencyCode,
									payment.getAgent(), exchangeRateProxy);
						}
						payCurrencyDTO = onAccPayCurrencyDTO;
					} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
						if (onAccPayCurrencyDTO == null) {
							String loggedInAgencyCode = null;
							onAccPayCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(selectedCurrencyCode, loggedInAgencyCode,
									payment.getAgent(), exchangeRateProxy);
						}
						payCurrencyDTO = onAccPayCurrencyDTO;
					} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
						if (externalPGWResponse != null) {
							String payCurrency = null;
							if (externalPGWResponse.getIpgTransactionResultDTO() != null) {
								payCurrency = externalPGWResponse.getIpgTransactionResultDTO().getCurrency();
							}
							if (payCurrency == null || ("").equals(payCurrency)) {
								payCurrency = selectedCurrencyCode;
							}
							cardPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(payCurrency, ipgId, exchangeRateProxy,
									false);
							ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
									cardPayCurrencyDTO.getPayCurrencyCode());
							ipgIdentificationParamsDTO
									.setFQIPGConfigurationName(ipgId + "_" + cardPayCurrencyDTO.getPayCurrencyCode());
							ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
						}
						if (cardPayCurrencyDTO == null) {
							cardPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(selectedCurrencyCode, ipgId,
									exchangeRateProxy, false);
							ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
									cardPayCurrencyDTO.getPayCurrencyCode());
							ipgIdentificationParamsDTO
									.setFQIPGConfigurationName(ipgId + "_" + cardPayCurrencyDTO.getPayCurrencyCode());
							ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
						}
						payCurrencyDTO = cardPayCurrencyDTO;
					} else if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
						payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(selectedCurrencyCode, exchangeRateProxy);
					} else if (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
						if (cardPayCurrencyDTO == null) {
							cardPayCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(selectedCurrencyCode, ipgId,
									exchangeRateProxy, true);
							ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
									cardPayCurrencyDTO.getPayCurrencyCode());
							ipgIdentificationParamsDTO
									.setFQIPGConfigurationName(ipgId + "_" + cardPayCurrencyDTO.getPayCurrencyCode());
							ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);
						}
						payCurrencyDTO = cardPayCurrencyDTO;
					}

					BigDecimal discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal infantFareDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal paxDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

					if (resDiscountDTO != null) {
						PaxDiscountDetailTO paxDiscountDetailTO = resDiscountDTO
								.getPaxDiscountDetail(reservationPax.getSeqNumber());
						if (paxDiscountDetailTO != null) {
							paxDiscountAmount = paxDiscountDetailTO.getPaxTotalDiscountAmount();
							if (isParent) {
								infantFareDiscountAmount = paxDiscountDetailTO.getInfantTotalDiscountAmount();
							}
						}
					}

					discountAmount = paxDiscountAmount;

					lccClientPaymentAssembler = composeClientPaymentAssembler(lccClientPaymentAssembler, paxType,
							bookingShoppingCart.getPriceInfoTO(), isParent, ipgIdentificationParamsDTO, contactInfo,
							payCurrencyDTO, exchangeRateProxy.getExecutionDate(),
							creditSummaryHolder.getPaxCreditInfo(paxsequence), paxsequence, paymentHolder, discountAmount,
							discountApplyAs, infantFareDiscountAmount, adultInfantPaymentAssemblerMap, infantExternalCharges,
							externalPGWResponse);
				} else if (!lcclientReservationAssembler.isForceConfirm()) {
					lcclientReservationAssembler.setSplitBooking(true);
				}

			}

			if (isParent) {
				int intInfantWith = reservationPax.getInfantWith();

				// Passenger with a Infant - Parent
				lcclientReservationAssembler.addParent(reservationPax.getFirstName().trim(), reservationPax.getLastName().trim(),
						reservationPax.getTitle(), reservationPax.getDateOfBirth(), nationality, intSeq, intInfantWith, null,
						reservationPax.getPaxAdditionalInfo(), reservationPax.getPaxCategory(), lccClientPaymentAssembler,
						reservationPax.getFirstNameInOtherLanguage(), reservationPax.getLastNameInOtherLanguage(),
						reservationPax.getTitleInOtherLanguage(), reservationPax.getNameTranslationLanguage(),
						reservationPax.getPnrPaxArrivalIntlFltNumber(), reservationPax.getPnrPaxIntlFltArrivalDate(),
						reservationPax.getPnrPaxDepartureIntlFltNumber(), reservationPax.getPnrPaxIntlFltDepartureDate(),
						reservationPax.getPnrPaxGroupId());
				lcclientReservationAssembler.addAdultCount();
			} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxType())) {
				lcclientReservationAssembler.addChild(reservationPax.getFirstName().trim(), reservationPax.getLastName().trim(),
						reservationPax.getTitle(), reservationPax.getDateOfBirth(), nationality, intSeq, null,
						reservationPax.getPaxAdditionalInfo(), reservationPax.getPaxCategory(), lccClientPaymentAssembler, null,
						reservationPax.getFirstNameInOtherLanguage(), reservationPax.getLastNameInOtherLanguage(),
						reservationPax.getTitleInOtherLanguage(), reservationPax.getNameTranslationLanguage(),
						reservationPax.getPnrPaxArrivalIntlFltNumber(), reservationPax.getPnrPaxIntlFltArrivalDate(),
						reservationPax.getPnrPaxDepartureIntlFltNumber(), reservationPax.getPnrPaxIntlFltDepartureDate(),
						reservationPax.getPnrPaxGroupId());
				lcclientReservationAssembler.addChildCount();
			} else {
				// Passenger without a Infant - Single
				lcclientReservationAssembler.addSingle(reservationPax.getFirstName().trim(), reservationPax.getLastName().trim(),
						reservationPax.getTitle(), reservationPax.getDateOfBirth(), nationality, intSeq, null,
						reservationPax.getPaxAdditionalInfo(), reservationPax.getPaxCategory(), lccClientPaymentAssembler, null,
						reservationPax.getFirstNameInOtherLanguage(), reservationPax.getLastNameInOtherLanguage(),
						reservationPax.getTitleInOtherLanguage(), reservationPax.getNameTranslationLanguage(),
						reservationPax.getPnrPaxArrivalIntlFltNumber(), reservationPax.getPnrPaxIntlFltArrivalDate(),
						reservationPax.getPnrPaxDepartureIntlFltNumber(), reservationPax.getPnrPaxIntlFltDepartureDate(),
						reservationPax.getPnrPaxGroupId());
				lcclientReservationAssembler.addAdultCount();
			}
			intSeq++;
		}

		// Infants
		if (strInfants.length() > 0) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(strInfants);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();
				Integer nationality = null;
				if (jObject.get("displayInfantNationality").toString() != null
						&& !"".equals(jObject.get("displayInfantNationality").toString().trim())) {
					nationality = Integer.parseInt(jObject.get("displayInfantNationality").toString());
				}

				PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
				paxAdditionalInfoDTO.setPassportNo(BeanUtils.nullHandler(jObject.get("displayPnrPaxCatFOIDNumber")));
				paxAdditionalInfoDTO
						.setPassportExpiry(stringToDate(BeanUtils.nullHandler(jObject.get("displayPnrPaxCatFOIDExpiry"))));
				paxAdditionalInfoDTO.setPassportIssuedCntry(BeanUtils.nullHandler(jObject.get("displayPnrPaxCatFOIDPlace")));
				paxAdditionalInfoDTO.setPlaceOfBirth(BeanUtils.nullHandler(jObject.get("displayPnrPaxPlaceOfBirth")));
				paxAdditionalInfoDTO.setTravelDocumentType(BeanUtils.nullHandler(jObject.get("displayTravelDocType")));
				paxAdditionalInfoDTO.setVisaDocNumber(BeanUtils.nullHandler(jObject.get("displayVisaDocNumber")));
				paxAdditionalInfoDTO.setVisaDocPlaceOfIssue(BeanUtils.nullHandler(jObject.get("displayVisaDocPlaceOfIssue")));
				paxAdditionalInfoDTO
						.setVisaDocIssueDate(stringToDate(BeanUtils.nullHandler(jObject.get("displayVisaDocIssueDate"))));
				paxAdditionalInfoDTO.setVisaApplicableCountry(BeanUtils.nullHandler(jObject.get("displayVisaApplicableCountry")));
				paxAdditionalInfoDTO.setFfid(BeanUtils.nullHandler(jObject.get("ffid")).isEmpty()
						? BeanUtils.nullHandler(jObject.get("displayFFID")).toUpperCase()
						: BeanUtils.nullHandler(jObject.get("ffid")).toUpperCase());
				paxAdditionalInfoDTO.setNationalIDNo(BeanUtils.nullHandler(jObject.get("displayNationalIDNo")));

				String infCategory = BeanUtils.nullHandler(jObject.get("displayInfantCategory"));
				String title = jObject.get("displayInfantTitle") != null ? jObject.get("displayInfantTitle").toString() : null;
				String arrivalIntlFltNo = BeanUtils.nullHandler(jObject.get("displaypnrPaxArrivalFlightNumber"));
				String intlFltArrivalDate = BeanUtils.nullHandler(jObject.get("displaypnrPaxFltArrivalDate"));
				String intlFltArrivalTime = BeanUtils.nullHandler(jObject.get("displaypnrPaxArrivalTime"));
				String departureIntlFltNo = BeanUtils.nullHandler(jObject.get("displaypnrPaxDepartureFlightNumber"));
				String intlFltDepartureDate = BeanUtils.nullHandler(jObject.get("displaypnrPaxFltDepartureDate"));
				String intlFltDepartureTime = BeanUtils.nullHandler(jObject.get("displaypnrPaxDepartureTime"));
				String pnrPaxGroupId = BeanUtils.nullHandler(jObject.get("displayPnrPaxGroupId"));

				int infantTravelWith = Integer.parseInt(jObject.get("displayInfantTravellingWith").toString());
				LCCClientPaymentAssembler infantPaymentAssembler = adultInfantPaymentAssemblerMap.get(infantTravelWith);
				if (infantPaymentAssembler == null) {
					infantPaymentAssembler = new LCCClientPaymentAssembler();
					infantPaymentAssembler.setPaxType(PaxTypeTO.INFANT);
					if (infantPaymentAssembler.getPerPaxExternalCharges() == null
							|| infantPaymentAssembler.getPerPaxExternalCharges().isEmpty()) {

						List<LCCClientExternalChgDTO> infantExternalCharges = infantExternalChargesByAdultPaxSeq
								.get(infantTravelWith);
						if (infantExternalCharges != null && !infantExternalCharges.isEmpty()) {
							infantPaymentAssembler.addAllExternalCharges(infantExternalCharges);
						}

					}

				}

				lcclientReservationAssembler.addInfant(jObject.get("displayInfantFirstName").toString().trim(),
						jObject.get("displayInfantLastName").toString().trim(), title,
						stringToDate(jObject.get("displayInfantDOB").toString()), nationality, intSeq, infantTravelWith, null,
						paxAdditionalInfoDTO, infCategory, null,
						StringUtil.convertToHex(BeanUtils.nullHandler(jObject.get("displayInfantFirstNameOl"))),
						StringUtil.convertToHex(BeanUtils.nullHandler(jObject.get("displayInfantLastNameOl"))),
						BeanUtils.nullHandler(jObject.get("displayNameTranslationLanguage")), arrivalIntlFltNo,
						stringToTimeStamp(intlFltArrivalDate, intlFltArrivalTime), departureIntlFltNo,
						stringToTimeStamp(intlFltDepartureDate, intlFltDepartureTime), pnrPaxGroupId, infantPaymentAssembler);
				intSeq++;
			}
		}

		/* Properly set Parent/INF Sequence id */
		Set<LCCClientReservationPax> reservationPaxs = lcclientReservationAssembler.getLccreservation().getPassengers();

		for (LCCClientReservationPax lccClientReservationPax : reservationPaxs) {

			if (PaxTypeTO.INFANT.equals(lccClientReservationPax.getPaxType())) {

				Integer parentSeqId = lccClientReservationPax.getAttachedPaxId();

				// Now iterate through the set; find and update the parent
				for (LCCClientReservationPax pax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
					// Find the correct parent using sequence id
					if (PaxTypeTO.ADULT.equals(pax.getPaxType()) && parentSeqId.equals(pax.getPaxSequence())) {
						// Update the parent record
						pax.setAttachedPaxId(lccClientReservationPax.getPaxSequence());
						// pax.getInfants().add(lccClientReservationPax);
						// lccClientReservationPax.setParent(pax);
						break;
					}

				}
			}

		}

		if (bookingShoppingCart.getPayByVoucherInfo() != null) {
			lcclientReservationAssembler.setPayByVoucherInfo(bookingShoppingCart.getPayByVoucherInfo());
		}

		CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();
		lccClientReservationContactInfo.setTitle(contactInfo.getTitle());
		lccClientReservationContactInfo.setFirstName(contactInfo.getFirstName().trim());
		lccClientReservationContactInfo.setLastName(contactInfo.getLastName().trim());
		lccClientReservationContactInfo.setStreetAddress1(contactInfo.getStreet());
		lccClientReservationContactInfo.setStreetAddress2(contactInfo.getAddress());
		lccClientReservationContactInfo.setCity(contactInfo.getCity());
		lccClientReservationContactInfo.setCountryCode(contactInfo.getCountry());
		lccClientReservationContactInfo.setZipCode(contactInfo.getZipCode());
		lccClientReservationContactInfo.setMobileNo(
				contactInfo.getMobileCountry() + "-" + contactInfo.getMobileArea() + "-" + contactInfo.getMobileNo());
		lccClientReservationContactInfo
				.setPhoneNo(contactInfo.getPhoneCountry() + "-" + contactInfo.getPhoneArea() + "-" + contactInfo.getPhoneNo());
		lccClientReservationContactInfo
				.setFax(contactInfo.getFaxCountry() + "-" + contactInfo.getFaxArea() + "-" + contactInfo.getFaxNo());
		lccClientReservationContactInfo.setEmail(contactInfo.getEmail());
		// lccClientReservationContactInfo.setNationality(contactInfo.getNationality());
		if (contactInfo.getNationality() != null && !contactInfo.getNationality().equals("")
				&& !contactInfo.getNationality().equalsIgnoreCase("null")) {
			lccClientReservationContactInfo.setNationalityCode(new Integer(contactInfo.getNationality()));
		}
		lccClientReservationContactInfo.setUserNotes(contactInfo.getUserNotes());
		lccClientReservationContactInfo.setUserNoteType(contactInfo.getUserNoteType());
		lccClientReservationContactInfo.setPreferredLanguage(contactInfo.getPreferredLang());
		lccClientReservationContactInfo.setEmgnTitle(contactInfo.getEmgnTitle());
		lccClientReservationContactInfo.setEmgnFirstName(contactInfo.getEmgnFirstName());
		lccClientReservationContactInfo.setEmgnLastName(contactInfo.getEmgnLastName());
		lccClientReservationContactInfo.setEmgnPhoneNo(
				contactInfo.getEmgnPhoneCountry() + "-" + contactInfo.getEmgnPhoneArea() + "-" + contactInfo.getEmgnPhoneNo());
		lccClientReservationContactInfo.setEmgnEmail(contactInfo.getEmgnEmail());
		lccClientReservationContactInfo.setTaxRegNo(contactInfo.getTaxRegNo());
		lccClientReservationContactInfo.setState(contactInfo.getState());
		lcclientReservationAssembler.addContactInfo(contactInfo.getUserNotes(), lccClientReservationContactInfo,
				contactInfo.getUserNoteType());

		// TODO refactor this out and tae the flight avail rs with filght segment id's etc
		// addSegments(lcclientReservationAssembler, bookingShoppingCart.getFlightAvailRS());
		if (externalPGWResponse != null) {
			lcclientReservationAssembler.setTemporaryPaymentMap(getTemporyPaymentMap(externalPGWResponse));
			if (PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL
					.equals(externalPGWResponse.getBrokerType())) {
				lcclientReservationAssembler.setCreateResNPayInitInDifferentFlows(true);
			}
		}

		return lcclientReservationAssembler;
	}

	public static Map<Integer, CommonCreditCardPaymentInfo> getTemporyPaymentMap(IPGResponseDTO externalPGWResponse) {
		Map<Integer, CommonCreditCardPaymentInfo> mapCCPaymentInfo = new HashMap<Integer, CommonCreditCardPaymentInfo>();
		if (externalPGWResponse != null) {
			CommonCreditCardPaymentInfo cardPayment = new CommonCreditCardPaymentInfo();
			cardPayment.setPaymentBrokerRefNo(externalPGWResponse.getPaymentBrokerRefNo());
			cardPayment.setPaymentBrokerId(externalPGWResponse.getPaymentBrokerRefNo());
			cardPayment.setTemporyPaymentId(externalPGWResponse.getTemporyPaymentId());
			cardPayment.setPaymentSuccess(true);
			// if(externalPGWResponse.getStatus().a)
			mapCCPaymentInfo.put(externalPGWResponse.getTemporyPaymentId(), cardPayment);
		}
		return mapCCPaymentInfo;
	}

	private static LCCClientPaymentAssembler composeClientPaymentAssembler(LCCClientPaymentAssembler lccClientPaymentAssembler,
			String paxType, PriceInfoTO priceInfoTO, boolean isParent, IPGIdentificationParamsDTO ipgIdentificationParamsDTO,
			ContactInfoTO contactInfo, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, List<PnrCreditFETO> paxCredits,
			Integer paxSequence, PaymentHolder paymentHolder, BigDecimal discountAmount, String discountAs,
			BigDecimal infantFareDiscountAmount, Map<Integer, LCCClientPaymentAssembler> adultInfantPaymentAssemblerMap,
			List<LCCClientExternalChgDTO> infantExternalCharges, IPGResponseDTO externalPGWResponse)
			throws ModuleException, ParseException {

		BigDecimal paymentAmount = priceInfoTO.getPerPaxPriceInfoTO(paxType).getPassengerPrice().getTotalPrice();
		BigDecimal totalPayAmountBeforeDiscount = AccelAeroCalculator.add(
				priceInfoTO.getPerPaxPriceInfoTO(paxType).getPassengerPrice().getTotalPrice(),
				lccClientPaymentAssembler.calculateTotalExternalChargesAmount());

		BigDecimal paymentINFAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paymentINFAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paymentINFAmountBeforeDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		LCCClientPaymentAssembler infantPaymentAssembler = null;
		boolean isHavingCreditPayments = false;
		boolean isPaxDiscountApplied = false;
		boolean isInfantDiscountApplied = false;
		if (isParent) {
			infantPaymentAssembler = new LCCClientPaymentAssembler();
			infantPaymentAssembler.setPaxType(PaxTypeTO.INFANT);
			if (infantExternalCharges != null && !infantExternalCharges.isEmpty()) {
				infantPaymentAssembler.getPerPaxExternalCharges().addAll(infantExternalCharges);
			}
			paymentINFAmount = priceInfoTO.getPerPaxPriceInfoTO(PaxTypeTO.INFANT).getPassengerPrice().getTotalPrice();
			paymentINFAmountBeforeDiscount = AccelAeroCalculator.add(
					priceInfoTO.getPerPaxPriceInfoTO(PaxTypeTO.INFANT).getPassengerPrice().getTotalPrice(),
					infantPaymentAssembler.calculateTotalExternalChargesAmount());
			if (!PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discountAs)) {
				paymentINFAmount = AccelAeroCalculator.subtract(paymentINFAmount, infantFareDiscountAmount);
				isInfantDiscountApplied = AccelAeroCalculator.isGreaterThan(infantFareDiscountAmount,
						AccelAeroCalculator.getDefaultBigDecimalZero());
			}
			paymentINFAmountWithServiceCharges = AccelAeroCalculator.add(paymentINFAmount,
					infantPaymentAssembler.calculateTotalExternalChargesAmount());
		}

		if (!PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discountAs)) {
			paymentAmount = AccelAeroCalculator.subtract(paymentAmount, discountAmount);
			isPaxDiscountApplied = AccelAeroCalculator.isGreaterThan(discountAmount,
					AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		Set<String> agentcolMethod = null;
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(paymentAmount,
				lccClientPaymentAssembler.calculateTotalExternalChargesAmount());
		if (paxCredits != null) {
			Iterator<PnrCreditFETO> creditItr = paxCredits.iterator();
			while (creditItr.hasNext()) {
				PnrCreditFETO paxCredit = creditItr.next();
				String carrierCode = paxCredit.getCarrier();
				Integer debitPaxRefNo = paxCredit.getPaxCreditId();
				BigDecimal amount = paxCredit.getUsedAmount();
				BigDecimal residingCarrierAmount = paxCredit.getResidingCarrierAmount();
				String pnr = paxCredit.getPnr();
				// TODO IMPROVE
				if (isParent && AccelAeroCalculator.isLessThan(
						AccelAeroCalculator.add(totalPayAmountWithServiceCharges, amount.negate()),
						AccelAeroCalculator.getDefaultBigDecimalZero())) {
					lccClientPaymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, totalPayAmountWithServiceCharges,
							paymentTimestamp, residingCarrierAmount, null, null, paxSequence, null, pnr, null);
					infantPaymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo,
							AccelAeroCalculator.isGreaterThan(
									AccelAeroCalculator.add(amount, totalPayAmountWithServiceCharges.negate()),
									paymentINFAmountWithServiceCharges)
											? paymentINFAmountWithServiceCharges
											: AccelAeroCalculator.add(amount, totalPayAmountWithServiceCharges.negate()),
							paymentTimestamp, residingCarrierAmount, null, null, paxSequence, null, pnr, null);
					if (AccelAeroCalculator.isLessThan(paymentINFAmountWithServiceCharges,
							AccelAeroCalculator.add(totalPayAmountWithServiceCharges, amount.negate()).negate())) {
						paymentINFAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {
						paymentINFAmountWithServiceCharges = AccelAeroCalculator.subtract(paymentINFAmountWithServiceCharges,
								AccelAeroCalculator.add(totalPayAmountWithServiceCharges, amount.negate()).negate());
					}
					paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					totalPayAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
					paymentINFAmount = AccelAeroCalculator.subtract(paymentINFAmountWithServiceCharges,
							infantPaymentAssembler.calculateTotalExternalChargesAmount());
				} else if (AccelAeroCalculator.isGreaterThan(amount, totalPayAmountWithServiceCharges)) {
					lccClientPaymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, totalPayAmountWithServiceCharges,
							paymentTimestamp, residingCarrierAmount, null, null, paxSequence, null, pnr, null);
					paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					totalPayAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
				} else {
					paymentAmount = AccelAeroCalculator.add(paymentAmount, amount.negate());
					lccClientPaymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, amount, paymentTimestamp,
							residingCarrierAmount, null, null, paxSequence, null, pnr, null);
					totalPayAmountWithServiceCharges = AccelAeroCalculator.add(paymentAmount,
							lccClientPaymentAssembler.calculateTotalExternalChargesAmount());

				}
				isHavingCreditPayments = true;
			}
		}

		BigDecimal paymentAmountWithInfant = AccelAeroCalculator.add(paymentAmount, paymentINFAmountWithServiceCharges);

		if (!(isHavingCreditPayments && totalPayAmountWithServiceCharges.compareTo(BigDecimal.ZERO) == 0)
				|| (isParent && paymentINFAmountWithServiceCharges.compareTo(BigDecimal.ZERO) > 0)) {
			paymentHolder.consumePayment(paymentAmount, totalPayAmountWithServiceCharges);

			boolean isPaymentRecordExist = false;
			boolean isInfantPaymentRecordExist = false;
			BigDecimal totalConsumed = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal consumedInfantPayAmount = paymentINFAmountWithServiceCharges;
			CreditCardTO creditInfo = null;
			while (paymentHolder.hasNonRecordedConsumedPayments()) {
				String consumedPayRef = paymentHolder.getNextConsumedPaymentRef();
				BigDecimal consumedPayAmount = paymentHolder.getConsumedPaymentAmount(consumedPayRef);
				PaymentTO payment = paymentHolder.getPayment(consumedPayRef);
				creditInfo = paymentHolder.getCreditCardTO(consumedPayRef);
				VoucherDTO voucherDTO = paymentHolder.getVoucherPayment(consumedPayRef);

				if (payment != null) {
					// TODO IMPROVE
					PaymentMethod paymentMethod = payment.getPaymentMethod();
					if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())
							|| paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())
							|| paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())
							|| paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
						Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(payment.getAgent());
						if (agent != null) {
							agentcolMethod = agent.getPaymentMethod();
						}
					}

					if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
						if (validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_CASH)) {
							if (isValidPaymentOption(consumedPayAmount, totalPayAmountWithServiceCharges)) {
								lccClientPaymentAssembler.addCashPayment(consumedPayAmount, payCurrencyDTO, paymentTimestamp,
										payment.getPaymentReference(), payment.getActualPaymentMethod(), null, null);

								isPaymentRecordExist = true;
							}
							if (isParent) {
								consumedInfantPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							}
						}
					} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
						if (isValidPaymentOption(consumedPayAmount, totalPayAmountWithServiceCharges)) {
							lccClientPaymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount, payCurrencyDTO,
									paymentTimestamp, payment.getPaymentReference(), payment.getActualPaymentMethod(),
									Agent.PAYMENT_MODE_ONACCOUNT, null);

							isPaymentRecordExist = true;
						}

						if (isParent) {
							// will be taken care after adult payments processed
							consumedInfantPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
					} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
						if (validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_BSP)) {
							if (isValidPaymentOption(consumedPayAmount, totalPayAmountWithServiceCharges)) {
								lccClientPaymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount,
										payCurrencyDTO, paymentTimestamp, payment.getPaymentReference(),
										payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_BSP, null);

								isPaymentRecordExist = true;
							}
						}
						if (isParent) {
							consumedInfantPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
					} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
						UserInputDTO userInputDTO = getUserInputDTO(creditInfo, contactInfo);

						if (isValidPaymentOption(consumedPayAmount, totalPayAmountWithServiceCharges)) {
							if (externalPGWResponse != null
									&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL
											.equals(externalPGWResponse.getBrokerType())) {
								LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo = new LCCClientCCPaymentMetaInfo();
								lccClientCCPaymentMetaInfo.setTemporyPaymentId(externalPGWResponse.getTemporyPaymentId());
								lccClientCCPaymentMetaInfo.setPaymentBrokerId(externalPGWResponse.getPaymentBrokerRefNo());
								lccClientPaymentAssembler.addExternalCardPayment(PaymentType.CARD_GENERIC.getTypeValue(), "",
										consumedPayAmount, AppIndicatorEnum.APP_XBE, TnxModeEnum.SECURE_3D,
										lccClientCCPaymentMetaInfo, ipgIdentificationParamsDTO, payCurrencyDTO, new Date(), null,
										null, false, externalPGWResponse.getTemporyPaymentId().toString());
							} else {
								lccClientPaymentAssembler.addInternalCardPayment(
										new Integer(creditInfo.getCardType().trim()).intValue(),
										creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
										creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(),
										creditInfo.getCardCVV().trim(), consumedPayAmount, AppIndicatorEnum.APP_XBE,
										TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, payCurrencyDTO, paymentTimestamp,
										creditInfo.getCardHoldersName(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), null, userInputDTO);
							}
							isPaymentRecordExist = true;
						}
						if (isParent) {
							consumedInfantPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
					} else if (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
						if (validatePayMethod(agentcolMethod, Agent.PAYMENT_MODE_OFFLINE)) {
							lccClientPaymentAssembler.addOfflineCardPayment(consumedPayAmount, TnxModeEnum.MAIL_TP_ORDER,
									ipgIdentificationParamsDTO, payment.getPaymentReference(), payment.getActualPaymentMethod(),
									payCurrencyDTO);
						}
						if (isParent) {
							consumedInfantPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
					}
				} else if (voucherDTO != null) {
					voucherDTO.setRedeemdAmount(consumedPayAmount.toString());
					lccClientPaymentAssembler.addVoucherPayment(voucherDTO, payCurrencyDTO, paymentTimestamp);
					if (isParent) {
						consumedInfantPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					}
				}
				totalConsumed = AccelAeroCalculator.add(totalConsumed, consumedPayAmount, consumedInfantPayAmount);
			}

			// ON-ACC multiple agent infant separation
			if (isParent) {
				paymentHolder.consumePayment(paymentINFAmount, paymentINFAmountWithServiceCharges);
				while (paymentHolder.hasNonRecordedConsumedPayments()) {
					String consumedPayRef = paymentHolder.getNextConsumedPaymentRef();
					BigDecimal consumedPayAmount = paymentHolder.getConsumedPaymentAmount(consumedPayRef);
					PaymentTO payment = paymentHolder.getPayment(consumedPayRef);
					creditInfo = paymentHolder.getCreditCardTO(consumedPayRef);
					VoucherDTO infantVoucherDTO = paymentHolder.getVoucherPayment(consumedPayRef);
					if (payment != null) {
						PaymentMethod paymentMethod = payment.getPaymentMethod();

						if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
							if (isValidPaymentOptionForInfant(isParent, paymentINFAmountWithServiceCharges, consumedPayAmount)) {
								infantPaymentAssembler.addCashPayment(consumedPayAmount, payCurrencyDTO, paymentTimestamp,
										payment.getPaymentReference(), payment.getActualPaymentMethod(), null, null);

								isInfantPaymentRecordExist = true;
							}
						} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
							if (isValidPaymentOptionForInfantAllowZeroPayments(isParent, paymentINFAmountWithServiceCharges,
									consumedPayAmount)) {

								infantPaymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount,
										payCurrencyDTO, paymentTimestamp, payment.getPaymentReference(),
										payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);

								isInfantPaymentRecordExist = true;
							}
						} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {
							if (isValidPaymentOptionForInfant(isParent, paymentINFAmountWithServiceCharges, consumedPayAmount)) {
								infantPaymentAssembler.addAgentCreditPayment(payment.getAgent(), paymentINFAmount, payCurrencyDTO,
										paymentTimestamp, payment.getPaymentReference(), payment.getActualPaymentMethod(),
										Agent.PAYMENT_MODE_BSP, null);

								isInfantPaymentRecordExist = true;
							}
						} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {
							if (isValidPaymentOptionForInfant(isParent, paymentINFAmountWithServiceCharges, consumedPayAmount)) {
								UserInputDTO userInputDTO = getUserInputDTO(creditInfo, contactInfo);
								infantPaymentAssembler.addInternalCardPayment(
										new Integer(creditInfo.getCardType().trim()).intValue(),
										creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
										creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(),
										creditInfo.getCardCVV().trim(), paymentINFAmount, AppIndicatorEnum.APP_XBE,
										TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, payCurrencyDTO, paymentTimestamp,
										creditInfo.getCardHoldersName(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), null, userInputDTO);

								isInfantPaymentRecordExist = true;
							}
						} else if (paymentMethod.getCode().equals(PaymentMethod.OFFLINE.getCode())) {
							if (isValidPaymentOptionForInfant(isParent, paymentINFAmountWithServiceCharges, consumedPayAmount)) {
								infantPaymentAssembler.addOfflineCardPayment(paymentINFAmount, TnxModeEnum.MAIL_TP_ORDER,
										ipgIdentificationParamsDTO, payment.getPaymentReference(),
										payment.getActualPaymentMethod(), payCurrencyDTO);
								isInfantPaymentRecordExist = true;
							}
						}
					} else if (infantVoucherDTO != null) {
						if (isValidPaymentOptionForInfant(isParent, paymentINFAmountWithServiceCharges, consumedPayAmount)) {
							infantVoucherDTO.setRedeemdAmount(consumedPayAmount.toString());
							infantPaymentAssembler.addVoucherPayment(infantVoucherDTO, payCurrencyDTO, paymentTimestamp);
						}
						isInfantPaymentRecordExist = true;
					}
					totalConsumed = AccelAeroCalculator.add(totalConsumed, consumedPayAmount);
				}
			}

			// Records a dummy payment entry when full discount/promotion is applied for Passenger
			recordZeroPaymentEntryForFullDiscount(isPaymentRecordExist, isPaxDiscountApplied, totalPayAmountBeforeDiscount,
					discountAmount, lccClientPaymentAssembler, paymentHolder, payCurrencyDTO, paymentTimestamp, contactInfo,
					ipgIdentificationParamsDTO);

			if (isParent && !isInfantPaymentRecordExist) {
				recordZeroPaymentEntryForFullDiscount(isInfantPaymentRecordExist, isInfantDiscountApplied,
						paymentINFAmountBeforeDiscount, infantFareDiscountAmount, infantPaymentAssembler, paymentHolder,
						payCurrencyDTO, paymentTimestamp, contactInfo, ipgIdentificationParamsDTO);
			}
		}
		if (isParent) {
			adultInfantPaymentAssemblerMap.put(paxSequence, infantPaymentAssembler);
		}
		return lccClientPaymentAssembler;
	}

	public static boolean validatePayMethod(Set<String> agentcolMethod, String payMethod) {
		boolean validated = false;
		if (agentcolMethod == null || payMethod == null) {
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		} else {
			for (String agPay : agentcolMethod) {
				if (agPay.equals(payMethod)) {
					validated = true;
					break;
				}
			}
		}
		if (!validated) {
			throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		}
		return validated;

	}

	public static void addSegments(CommonReservationAssembler lcclientReservationAssembler, Collection<FlightSegmentTO> fltSegs) {
		int segmentSequnce = 1;
		// sort segments by departure date
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>(fltSegs);
		SortUtil.sortFlightSegByDepDate(flightSegments);
		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			if (!flightSegmentTO.isReturnFlag()) {
				lcclientReservationAssembler.addOutgoingSegment(segmentSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightRefNumber(),
						flightSegmentTO.getFlightNumber(), flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getBaggageONDGroupId(), flightSegmentTO.isWaitListed(),
						flightSegmentTO.getRouteRefNumber());
			} else {
				lcclientReservationAssembler.addReturnSegment(segmentSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightRefNumber(),
						flightSegmentTO.getFlightNumber(), flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getBaggageONDGroupId(), flightSegmentTO.isWaitListed(),
						flightSegmentTO.getRouteRefNumber());

			}

		}
	}

	private static String getFlightRefNumber(String flightRefNumber) {
		flightRefNumber = flightRefNumber.substring(flightRefNumber.indexOf("$") + 1, flightRefNumber.length());
		flightRefNumber = flightRefNumber.substring(flightRefNumber.indexOf("$") + 1, flightRefNumber.length());
		return flightRefNumber.substring(0, flightRefNumber.indexOf("$"));
	}

	public static Date stringToDate(String strDate) {
		Date dtReturn = null;
		if (!strDate.equals("")) {
			int date = Integer.parseInt(strDate.substring(0, 2));
			int month = Integer.parseInt(strDate.substring(3, 5));
			int year = Integer.parseInt(strDate.substring(6, 10));

			Calendar validDate = new GregorianCalendar(year, month - 1, date);
			dtReturn = new Date(validDate.getTime().getTime());
		}
		return dtReturn;
	}

	public static Date _stringToDate(String strDate) {
		Date dtReturn = null;
		if (!strDate.equals("")) {
			int year = Integer.parseInt(strDate.substring(0, 4));
			int month = Integer.parseInt(strDate.substring(5, 7));
			int date = Integer.parseInt(strDate.substring(8, 10));

			Calendar validDate = new GregorianCalendar(year, month - 1, date);
			dtReturn = new Date(validDate.getTime().getTime());
		}
		return dtReturn;
	}

	private static Date stringToTimeStamp(String strDate, String strTime) {
		try {
			Calendar validDate = null;
			Date dtReturn = null;
			if (!strDate.equals("")) {
				int date = Integer.parseInt(strDate.substring(0, 2));
				int month = Integer.parseInt(strDate.substring(3, 5));
				int year = Integer.parseInt(strDate.substring(6, 10));

				validDate = new GregorianCalendar(year, month - 1, date);

			}
			if (!strTime.equals("") && validDate != null) {
				Date tmpDate = validDate.getTime();
				Calendar tmpCalendar = Calendar.getInstance();
				tmpCalendar.setTime(tmpDate);
				int hours = Integer.parseInt(strTime.substring(0, 2));
				int minutes = Integer.parseInt(strTime.substring(3, 5));
				tmpCalendar.add(Calendar.HOUR, hours);
				tmpCalendar.add(Calendar.MINUTE, minutes);
				validDate = tmpCalendar;
			}
			if (validDate != null) {
				dtReturn = validDate.getTime();
			}
			return dtReturn;

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param lCCClientSegmentAssembler
	 * @param fltSegs
	 * @param startingSegmentSequnce
	 */
	public static void addSegments(LCCClientSegmentAssembler lCCClientSegmentAssembler, Collection<FlightSegmentTO> fltSegs,
			int startingSegmentSequnce) {
		// sort the flight segments by departure date
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>(fltSegs);
		SortUtil.sortFlightSegByDepDate(flightSegments);
		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			if (!flightSegmentTO.isReturnFlag()) {
				lCCClientSegmentAssembler.addOutgoingSegment(startingSegmentSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightNumber(),
						flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getOperatingAirline(), flightSegmentTO.getBaggageONDGroupId(), null,
						flightSegmentTO.getRouteRefNumber());

			} else {
				lCCClientSegmentAssembler.addReturnSegment(startingSegmentSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightNumber(),
						flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getOperatingAirline(), flightSegmentTO.getBaggageONDGroupId(), null,
						flightSegmentTO.getRouteRefNumber());

			}
		}
	}

	/**
	 * get last segment sequence
	 * 
	 * @param pnrSegments
	 * @return
	 */
	public static int getNextSegmentSequnce(Collection<LCCClientReservationSegment> pnrSegments) {
		int segmentSequnce = 0;
		if (pnrSegments != null) {
			for (LCCClientReservationSegment segemnt : pnrSegments) {
				if (segemnt.getSegmentSeq() > segmentSequnce) {
					segmentSequnce = segemnt.getSegmentSeq().intValue();
				}
			}
		}
		return ++segmentSequnce;
	}

	public static void addConfirmOprtSegments(LCCClientSegmentAssembler lCCClientSegmentAssembler,
			Collection<FlightSegmentTO> fltSegs, int startingSegmentSequnce) {
		// sort the flight segments by departure date
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>(fltSegs);
		SortUtil.sortFlightSegByDepDate(flightSegments);
		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			lCCClientSegmentAssembler.addReturnSegment(startingSegmentSequnce++,
					getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightNumber(),
					flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
					flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
					flightSegmentTO.getArrivalDateTime(), null, flightSegmentTO.getDepartureDateTimeZulu(),
					flightSegmentTO.getArrivalDateTimeZulu(), flightSegmentTO.getOperatingAirline(), null, null, null);
		}
	}

	public static List<LCCClientExternalChgDTO> converToProxyExtCharges(Collection<ExternalChgDTO> extChgs) {
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
		if (extChgs != null && extChgs.size() > 0) {
			for (ExternalChgDTO chg : extChgs) {
				extChgList.add(new LCCClientExternalChgDTO(chg));
			}
		}
		return extChgList;
	}

	public static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}
	
	public static BigDecimal getTotalExtChargeExcludingSelected(List<LCCClientExternalChgDTO> extChgList,
			List<EXTERNAL_CHARGES> extChargesToSkip) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			if (!extChargesToSkip.contains(chg.getExternalCharges())) {
				total = AccelAeroCalculator.add(total, chg.getAmount());
			}
		}
		return total;
	}
	
	public static BigDecimal getTotalExtChargeSelected(List<LCCClientExternalChgDTO> extChgList,
			List<EXTERNAL_CHARGES> extChargesToSkip) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			if (extChargesToSkip.contains(chg.getExternalCharges())) {
				total = AccelAeroCalculator.add(total, chg.getAmount());
			}
		}
		return total;
	}

	public static void applyFlexiCharges(List<ReservationPaxTO> paxList, BookingShoppingCart bookingShoppingCart,
			boolean[] isFlexiSelected, List<FlightSegmentTO> flightSegmentTOs) {
		for (ReservationPaxTO reservationPaxTO : paxList) {
			for (PerPaxPriceInfoTO paxPriceInfoTO : bookingShoppingCart.getPriceInfoTO().getPerPaxPriceInfo()) {
				if (paxPriceInfoTO.getPassengerType().equals(reservationPaxTO.getPaxType())) {
					int ondSequence = 0;
					for (boolean ondFlexi : isFlexiSelected) {
						if (ondFlexi) {
							reservationPaxTO.addExternalCharges(ReservationUtil.transform(
									paxPriceInfoTO.getPassengerPrice().getOndExternalCharge(ondSequence).getExternalCharges(),
									flightSegmentTOs));
						}
						ondSequence++;
					}
					break;
				}
			}
		}
	}

	public static void applyFlexiCharges(List<ReservationPaxTO> paxList, BookingShoppingCart bookingShoppingCart,
			List<FlightSegmentTO> flightSegmentTOs) {
		for (ReservationPaxTO reservationPaxTO : paxList) {
			PriceInfoTO priceInfoTO = bookingShoppingCart.getPriceInfoTO();
			Map<Integer, Boolean> ondFlexiSelection = priceInfoTO.getFareTypeTO().getOndFlexiSelection();
			for (PerPaxPriceInfoTO paxPriceInfoTO : priceInfoTO.getPerPaxPriceInfo()) {
				if (paxPriceInfoTO.getPassengerType().equals(reservationPaxTO.getPaxType())) {
					for (Entry<Integer, Boolean> entry : ondFlexiSelection.entrySet()) {
						if (entry.getValue()) {
							reservationPaxTO.addExternalCharges(ReservationUtil.transform(
									paxPriceInfoTO.getPassengerPrice().getOndExternalCharge(entry.getKey()).getExternalCharges(),
									flightSegmentTOs));
						}
					}
					break;
				}
			}
		}
	}

	public static void populateInsurance(List<LCCInsuranceQuotationDTO> lccInsQuoteDTOs,
			LCCClientSegmentAssembler lcclientSegmentAssembler, int adultCount) {

		LCCClientReservationInsurance insurance = new LCCClientReservationInsurance();
		if (lccInsQuoteDTOs != null && !lccInsQuoteDTOs.isEmpty()) {

			for (LCCInsuranceQuotationDTO lccInsQuoteDTO : lccInsQuoteDTOs) {
				LCCInsuredJourneyDTO insuredJourneyDTO = new LCCInsuredJourneyDTO();
				insuredJourneyDTO.setJourneyEndDate(lccInsQuoteDTO.getInsuredJourney().getJourneyEndDate());
				insuredJourneyDTO.setJourneyStartDate(lccInsQuoteDTO.getInsuredJourney().getJourneyStartDate());
				insuredJourneyDTO.setJourneyStartAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyStartAirportCode());
				insuredJourneyDTO.setJourneyEndAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyEndAirportCode());
				insuredJourneyDTO.setRoundTrip(lccInsQuoteDTO.getInsuredJourney().isRoundTrip());
				insurance.setInsuranceQuoteRefNumber(lccInsQuoteDTO.getInsuranceRefNumber());
				insurance.setInsuredJourneyDTO(insuredJourneyDTO);
				insurance.setQuotedTotalPremium(lccInsQuoteDTO.getQuotedTotalPremiumAmount());
				insurance.setApplicablePaxCount(adultCount);
				insurance.setOperatingAirline(lccInsQuoteDTO.getOperatingAirline());
				insurance.setPlanCode(lccInsQuoteDTO.getPlanCode());
				insurance.setSsrFeeCode(lccInsQuoteDTO.getSsrFeeCode());

				lcclientSegmentAssembler.addResInsurance(insurance);
			}
		}
	}

	/**
	 * 
	 * @param postPayDTO
	 * @param lcclientSegmentAssembler
	 * @throws ModuleException
	 */
	public static void populateInsurance(List<LCCInsuranceQuotationDTO> lccInsQuoteDTOs, RequoteModifyRQ requoteRQ,
			int adultCount, TrackInfoDTO trackDTO) throws ModuleException {

		List<LCCClientReservationInsurance> lccInsurances = new ArrayList<LCCClientReservationInsurance>();

		if (lccInsQuoteDTOs != null && !lccInsQuoteDTOs.isEmpty()) {
			for (LCCInsuranceQuotationDTO lccInsQuoteDTO : lccInsQuoteDTOs) {
				LCCClientReservationInsurance lccInsurance = new LCCClientReservationInsurance();
				LCCInsuredJourneyDTO insuredJourneyDTO = new LCCInsuredJourneyDTO();
				insuredJourneyDTO.setJourneyEndDate(lccInsQuoteDTO.getInsuredJourney().getJourneyEndDate());
				insuredJourneyDTO.setJourneyStartDate(lccInsQuoteDTO.getInsuredJourney().getJourneyStartDate());
				insuredJourneyDTO.setJourneyStartAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyStartAirportCode());
				insuredJourneyDTO.setJourneyEndAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyEndAirportCode());
				insuredJourneyDTO.setRoundTrip(lccInsQuoteDTO.getInsuredJourney().isRoundTrip());
				lccInsurance.setInsuranceQuoteRefNumber(lccInsQuoteDTO.getInsuranceRefNumber());
				lccInsurance.setInsuredJourneyDTO(insuredJourneyDTO);
				lccInsurance.setQuotedTotalPremium(lccInsQuoteDTO.getQuotedTotalPremiumAmount());
				lccInsurance.setApplicablePaxCount(adultCount);
				lccInsurance.setOperatingAirline(lccInsQuoteDTO.getOperatingAirline());
				lccInsurance.setSsrFeeCode(lccInsQuoteDTO.getSsrFeeCode());
				lccInsurance.setPlanCode(lccInsQuoteDTO.getPlanCode());
				lccInsurances.add(lccInsurance);
			}
		}

		IInsuranceRequest insurance = null;
		Reservation reservation = null;

		if (lccInsurances != null && !lccInsurances.isEmpty()) {
			LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
			modes.setPnr(requoteRQ.getPnr());

			reservation = ModuleServiceLocator.getReservationBD().getReservation(modes, trackDTO);

			for (LCCClientReservationInsurance lccInsurance : lccInsurances) {
				insurance = new InsuranceRequestAssembler();

				InsureSegment insSegment = new InsureSegment();
				LCCInsuredJourneyDTO insuredJourneyDTO = lccInsurance.getInsuredJourneyDTO();
				insSegment.setArrivalDate(insuredJourneyDTO.getJourneyEndDate());
				insSegment.setDepartureDate(insuredJourneyDTO.getJourneyStartDate());
				insSegment.setFromAirportCode(insuredJourneyDTO.getJourneyStartAirportCode());
				insSegment.setToAirportCode(insuredJourneyDTO.getJourneyEndAirportCode());
				insSegment.setRoundTrip(insuredJourneyDTO.isRoundTrip());
				insSegment.setSalesChanelCode(insuredJourneyDTO.getSalesChannel());

				insurance.setInsuranceId(new Integer(lccInsurance.getInsuranceQuoteRefNumber()));
				insurance.addFlightSegment(insSegment);
				insurance.setQuotedTotalPremiumAmount(lccInsurance.getQuotedTotalPremium());
				insurance.setInsuranceType(lccInsurance.getInsuranceType());
				insurance.setAllDomastic(lccInsQuoteDTOs.get(0).isAllDomastic());
				insurance.setOperatingAirline(lccInsQuoteDTOs.get(0).getOperatingAirline());
				insurance.setSsrFeeCode(lccInsurance.getSsrFeeCode());
				insurance.setPlanCode(lccInsurance.getPlanCode());

				BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(lccInsurance.getQuotedTotalPremium(),
						reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount());
				int i = 0;
				for (ReservationPax pax : reservation.getPassengers()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType())
							|| (PaxTypeTO.INFANT.equals(pax.getPaxType()) && AppSysParamsUtil.allowAddInsurnaceForInfants())) {
						InsurePassenger insurePassenger = new InsurePassenger();

						// adding AIG Pax info
						insurePassenger.setFirstName(pax.getFirstName());
						insurePassenger.setLastName(pax.getLastName());
						insurePassenger.setDateOfBirth(pax.getDateOfBirth());

						ReservationContactInfo contactInfo = reservation.getContactInfo();
						// Adding AIG Contact Info
						insurePassenger.setAddressLn1(contactInfo.getStreetAddress1());
						insurePassenger.setAddressLn2(contactInfo.getStreetAddress2());
						insurePassenger.setCity(contactInfo.getCity());
						insurePassenger.setEmail(contactInfo.getEmail());
						insurePassenger.setCountryOfAddress(contactInfo.getCountryCode());
						insurePassenger.setHomePhoneNumber(contactInfo.getPhoneNo());

						insurance.addPassenger(insurePassenger);
						if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
							insurance.addInsuranceCharge(pax.getPaxSequence(), BigDecimal.ZERO);
						} else {
							insurance.addInsuranceCharge(pax.getPaxSequence(), insPaxWise[i++]);
						}
					}
				}
				requoteRQ.addInsurance(insurance);
			}
		}
	}

	public static Date getReleaseTimestamp(List<FlightSegmentTO> flightSegmentTOs, Map<String, String> mapPrivileges,
			String agentCode, FareTypeTO fare, Collection<LCCClientReservationSegment> existingSegs, Date existingRelTimestamp)
			throws ModuleException {
		FlightSegmentTO firstDeptFltSeg = null;
		List<FlightSegmentTO> firstDepFlightSegments = new ArrayList<FlightSegmentTO>();
		for (FlightSegmentTO seg : flightSegmentTOs) {
			String segmentCode = seg.getSegmentCode();
			// Nili You don't need to eliminate the bus segment because it's needs for onhold release time.
			// otherwise there will be inconsistent with the release time calculations
			if (segmentCode != null /* && !WPModuleUtils.getAirportBD().isBusSegment(segmentCode) */) {
				if (firstDeptFltSeg == null) {
					firstDeptFltSeg = seg;
				} else {
					if (firstDeptFltSeg.getDepartureDateTimeZulu().after(seg.getDepartureDateTimeZulu())) {
						firstDeptFltSeg = seg;
					}
				}
			}
		}
		if (firstDeptFltSeg != null) {
			GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
			String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			firstDepFlightSegments.add(firstDeptFltSeg);
			OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = new OnHoldReleaseTimeDTO();
			boolean freshFirstDepSeg = false;
			if (firstDeptFltSeg.getOperatingAirline().equals(defaultCarrierCode)) {
				if (fare != null) {
					List<FareRuleDTO> fareRuleDTOs = fare.getApplicableFareRules();
					if (fareRuleDTOs != null) {
						for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
							if (fareRuleDTO != null && fareRuleDTO.getOrignNDest() != null) {
								if (firstDeptFltSeg.getSegmentCode().equalsIgnoreCase(fareRuleDTO.getOrignNDest())) {
									onHoldReleaseTimeDTO = ReservationBeanUtil.getOnHoldReleaseTimeDTO(firstDepFlightSegments,
											fare);
									onHoldReleaseTimeDTO.setModuleCode(AppIndicatorEnum.APP_XBE.toString());
									freshFirstDepSeg = true;
									break;
								}
							}
						}
					}
				}

				// In requote disable flow first dept seg may differ with this fareSegment
				if (!freshFirstDepSeg && existingSegs != null && existingRelTimestamp != null) {
					for (LCCClientReservationSegment lccClientReservationSegment : existingSegs) {
						if (lccClientReservationSegment.getSegmentCode().equals(firstDeptFltSeg.getSegmentCode())
								&& lccClientReservationSegment.getDepartureDateZulu()
										.compareTo(firstDeptFltSeg.getDepartureDateTimeZulu()) == 0
								&& lccClientReservationSegment.getStatus()
										.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
							return existingRelTimestamp;
						}
					}
				} else if (freshFirstDepSeg && existingRelTimestamp != null && firstDeptFltSeg.getFlightSegmentStatus()
						.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
					return existingRelTimestamp;
				}

				onHoldReleaseTimeDTO.setDomestic(firstDeptFltSeg.isDomesticFlight());
			}

			// If the first departing carrier is different or Fare is not matching with segment
			if (onHoldReleaseTimeDTO.getModuleCode() == null) {
				onHoldReleaseTimeDTO.setModuleCode(AppIndicatorEnum.APP_XBE.toString());
				onHoldReleaseTimeDTO.setOndCode(firstDeptFltSeg.getSegmentCode());
				onHoldReleaseTimeDTO.setBookingDate(new Date());
				onHoldReleaseTimeDTO.setFlightDepartureDate(firstDeptFltSeg.getDepartureDateTimeZulu());
			}

			if (agentCode != null) {
				onHoldReleaseTimeDTO.setAgentCode(agentCode);
			}

			// TODO Remove Agent Code from getReleaseTime method
			return ReleaseTimeUtil.getReleaseTime(mapPrivileges.keySet(), firstDeptFltSeg.getDepartureDateTimeZulu(), true,
					agentCode, AppIndicatorEnum.APP_XBE.toString(), onHoldReleaseTimeDTO);
		}
		return null;
	}

	public static void setOnholdRealeaseTimes(Map<String, RollForwardFlightRQ> rollForwardingFlightsMap,
			Map<String, String> mapPrivileges, String agentCode, boolean overrideBufferTime, int bufferDays, String bufferTime)
			throws ModuleException {

		int hours = 0;
		int mins = 0;
		int secs = 0;
		if (overrideBufferTime) {
			String arr[] = bufferTime.split(":");
			hours = Integer.parseInt(arr[0]);
			mins = Integer.parseInt(arr[1]);
			secs = Integer.parseInt(arr[2]);
		}
		for (Entry<String, RollForwardFlightRQ> entry : rollForwardingFlightsMap.entrySet()) {
			RollForwardFlightRQ rollForwardFlightRQ = entry.getValue();
			List<RollForwardFlightDTO> rollForwardFlts = rollForwardFlightRQ.getRollForwardFlights();

			if (rollForwardFlts != null && rollForwardFlts.size() > 0) {
				RollForwardFlightDTO firstDeptSeg = null;
				String segmentCode = null;
				Collections.sort(rollForwardFlts);
				for (RollForwardFlightDTO seg : rollForwardFlts) {
					segmentCode = seg.getSegmentCode();
					if (segmentCode != null && !WPModuleUtils.getAirportBD().isBusSegment(segmentCode)) {
						firstDeptSeg = seg;
						break;
					}
				}

				if (firstDeptSeg != null) {

					if (!overrideBufferTime) {
						OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = new OnHoldReleaseTimeDTO();
						onHoldReleaseTimeDTO.setBookingDate(new Date());
						onHoldReleaseTimeDTO.setOndCode(firstDeptSeg.getSegmentCode());
						onHoldReleaseTimeDTO.setFlightDepartureDate(firstDeptSeg.getDepartureDateTimeZulu());
						onHoldReleaseTimeDTO.setDomestic(firstDeptSeg.isDomesticFlight());
						onHoldReleaseTimeDTO.setCabinClass(firstDeptSeg.getCabinClass());
						onHoldReleaseTimeDTO.setBookingClass(firstDeptSeg.getBookingClass());
						onHoldReleaseTimeDTO.setHasBuffertimeOhd(
								AuthorizationsUtil.hasPrivilege(mapPrivileges.keySet(), "xbe.res.make.onhold.buffer"));
						onHoldReleaseTimeDTO.setModuleCode(AppIndicatorEnum.APP_XBE.toString());
						onHoldReleaseTimeDTO.setAgentCode(agentCode);
						onHoldReleaseTimeDTO.setFareRuleId(firstDeptSeg.getFareRuleId());

						Date onholdReleaseTime = ReleaseTimeUtil.getReleaseTime(firstDeptSeg.getDepartureDateTimeZulu(), true,
								agentCode, AppIndicatorEnum.APP_XBE.toString(), onHoldReleaseTimeDTO);

						rollForwardFlightRQ.setOnholdReleaseTime(onholdReleaseTime);

					} else {
						Calendar calReleaseDate = Calendar.getInstance();
						calReleaseDate
								.setTime(CalendarUtil.addDateVarience(firstDeptSeg.getDepartureDateTimeZulu(), -bufferDays));
						calReleaseDate.setTime(CalendarUtil.getStartTimeOfDate(calReleaseDate.getTime()));
						calReleaseDate.add(Calendar.HOUR, hours);
						calReleaseDate.add(Calendar.MINUTE, mins);
						calReleaseDate.add(Calendar.SECOND, secs);
						Date onholdReleaseTime = calReleaseDate.getTime();
						Date maximumAllowedReleaseTime = ReleaseTimeUtil
								.calculateClosureReleaseTime(firstDeptSeg.getDepartureDateTimeZulu());
						if (onholdReleaseTime.after(maximumAllowedReleaseTime)
								|| onholdReleaseTime.before(CalendarUtil.getCurrentSystemTimeInZulu())) {
							throw new ModuleException("airreservations.ohd.rollfoward.invalidreleasetime",
									"Invalid onhold releasetime");
						}
						rollForwardFlightRQ.setOnholdReleaseTime(onholdReleaseTime);
					}
				}
			}

		}

	}

	/**
	 * Util method related to modification flow
	 * 
	 * @param actualPayment
	 */

	public static LCCClientResAlterQueryModesTO getAlterModesToAddSeg(BookingShoppingCart bookingShoppingCart,
			PaymentTO paymentTO, CreditCardTO creditInfoTO, Integer ipgId, String OwnerAgentCode, String paxCredit,
			boolean isGroupPNR, CommonReservationContactInfo contactInfo, String selCurrency, String loggedInAgenctCode,
			String loggedInAgentStation, Map<String, String> mapPrivileges, boolean actualPayment, long requestID)
			throws Exception {

		LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO = bookingShoppingCart.getReservationAlterModesTO();
		ReservationBalanceTO lCCClientReservationBalanceTO = bookingShoppingCart.getReservationBalance();
		Collection<ReservationPaxTO> paxList = bookingShoppingCart.getPaxList();

		PaymentMethod paymentMethod = paymentTO.getPaymentMethod();

		JSONArray jsonPaxPayArray = (JSONArray) new JSONParser().parse(paxCredit);
		int noofCreditPax = 0;
		Iterator<?> itIter = jsonPaxPayArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			if (new BigDecimal((String) jPPObj.get("displayToPay")).compareTo(BigDecimal.ZERO) > 0) {
				noofCreditPax++;
			}
		}

		Map<Integer, ReservationPaxTO> resPaxMap = new HashMap<Integer, ReservationPaxTO>();
		int noOfAdults = 0;
		for (ReservationPaxTO resPax : paxList) {
			if (!resPax.getPaxType().equals(PaxTypeTO.INFANT)) {
				noOfAdults++;
			}
			resPaxMap.put(resPax.getSeqNumber(), resPax);
		}

		ExternalChargesMediator creditExtChargesMediator;
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false);
		if (new BigDecimal(paymentTO.getAmount().trim()).compareTo(BigDecimal.ZERO) <= 0) {
			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, loggedInAgenctCode)) {
				creditExtChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
			} else {
				creditExtChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false);
			}
		} else {
			creditExtChargesMediator = new ExternalChargesMediator(bookingShoppingCart,
					paymentMethod.getCode().equals(PaymentMethod.CRED.getCode()));
		}

		com.isa.thinair.xbe.core.web.v2.util.ReservationUtil.getApplicableServiceTax(bookingShoppingCart,
				EXTERNAL_CHARGES.JN_OTHER);

		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(noOfAdults, 0);
		LinkedList perPaxCreditExternalCharges = null;
		if (!paymentTO.getMode().equals("CONFIRM")) {
			creditExtChargesMediator.setCalculateJNTaxForCCCharge(true);
			perPaxCreditExternalCharges = creditExtChargesMediator.getExternalChargesForABalancePayment(noofCreditPax, 0);
		}

		LCCClientSegmentAssembler segmentAssembler = lCCClientResAlterQueryModesTO.getLccClientSegmentAssembler();
		segmentAssembler.setOwnerAgent(OwnerAgentCode);
		segmentAssembler.setContactInfo(contactInfo);
		segmentAssembler.setLastCurrencyCode(selCurrency);
		BookingUtil.populateInsurance(bookingShoppingCart, segmentAssembler, noOfAdults);

		PaymentHolder paymentHolder = new PaymentHolder();
		if (BookingUtil.isDoubleAgentPay(paymentTO)) {
			if (paymentTO.getAgentOneAmount() != null && !"".equals(paymentTO.getAgentOneAmount().trim())) {
				paymentTO.setAmount(paymentTO.getAgentOneAmount());

			}

			PaymentTO second = paymentTO.clone();
			second.setAgent(paymentTO.getAgentTwo());
			second.setAmount(paymentTO.getAgentTwoAmount());

			paymentHolder.addPayment(paymentTO);
			paymentHolder.addPayment(second);

		} else if (creditInfoTO != null) {
			paymentHolder.addCardPayment(paymentTO, creditInfoTO);
		} else {
			paymentHolder.addPayment(paymentTO);
		}
		PayCurrencyDTO payCurrencyDTO = null;
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		BigDecimal totalPaxCredit = BigDecimal.ZERO;

		List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) lCCClientReservationBalanceTO
				.getPassengerSummaryList();
		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
				BigDecimal amountDueWithExt = AccelAeroCalculator.add(paxSummaryTo.getTotalCreditAmount().negate(),
						paxSummaryTo.getTotalAmountDue());
				LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
				ReservationPaxTO resPax = resPaxMap.get(PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()));
				BigDecimal amountDueWithoutExt = amountDueWithExt;
				if (perPaxExternalCharges.size() > 0) {
					List<LCCClientExternalChgDTO> extCharges = BookingUtil
							.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
					extCharges.addAll(resPax.getExternalCharges());
					paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);
					BigDecimal extChgTotal = BookingUtil.getTotalExtCharge(extCharges);
					amountDueWithoutExt = AccelAeroCalculator.subtract(amountDueWithExt, extChgTotal);
				}

				boolean blnCreditPax = false;
				boolean creditFlag = false;

				if (!paymentTO.getMode().equals("CONFIRM")) {
					segmentAssembler.setPnrZuluReleaseTimeStamp(null);

					itIter = jsonPaxPayArray.iterator();
					JSONArray jPCInfo = null;

					while (itIter.hasNext()) {
						JSONObject jPPObj = (JSONObject) itIter.next();
						String jtrevelRefNo = BeanUtils.nullHandler(jPPObj.get("displayTravelerRefNo"));

						if (jtrevelRefNo.equalsIgnoreCase(paxSummaryTo.getTravelerRefNumber())) {
							jPCInfo = (JSONArray) jPPObj.get("paxCreditInfo");
							if (new BigDecimal((String) jPPObj.get("displayToPay")).compareTo(BigDecimal.ZERO) > 0) {
								blnCreditPax = true;
							}
						}
					}

					if (blnCreditPax && perPaxCreditExternalCharges.size() > 0) {
						List<LCCClientExternalChgDTO> ccChgs = BookingUtil
								.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxCreditExternalCharges.pop());
						BigDecimal ccChgTotal = BookingUtil.getTotalExtCharge(ccChgs);
						amountDueWithExt = AccelAeroCalculator.add(amountDueWithExt, ccChgTotal);
						paymentAssembler.getPerPaxExternalCharges().addAll(ccChgs);
					}

					if (jPCInfo != null) {
						Iterator<?> uIter = jPCInfo.iterator();
						while (uIter.hasNext()) {
							JSONObject jUObj = (JSONObject) uIter.next();
							BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
							amountDueWithoutExt = AccelAeroCalculator.add(amountDueWithoutExt, amount.negate());
							amountDueWithExt = AccelAeroCalculator.add(amountDueWithExt, amount.negate());
							creditFlag = true;
						}
					}

					if (PaymentUtil.isValidPaymentExist(creditFlag, amountDueWithExt,
							lCCClientResAlterQueryModesTO.getReservationStatus())) {

						paymentHolder.consumePayment(amountDueWithoutExt, amountDueWithExt);

						while (paymentHolder.hasNonRecordedConsumedPayments()) {
							String consumedPayRef = paymentHolder.getNextConsumedPaymentRef();
							BigDecimal consumedPayAmount = paymentHolder.getConsumedPaymentAmount(consumedPayRef);
							PaymentTO payment = paymentHolder.getPayment(consumedPayRef);
							CreditCardTO creditInfo = paymentHolder.getCreditCardTO(consumedPayRef);

							paymentMethod = payment.getPaymentMethod();

							if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
								payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(selCurrency, exchangeRateProxy);
								paymentAssembler.addCashPayment(consumedPayAmount, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), null, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
								payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(selCurrency, loggedInAgenctCode,
										payment.getAgent(), exchangeRateProxy);

								paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {

								// Override the selected currency with Agent country currency code for BSP
								String bspCurrency = ModuleServiceLocator.getCommonServiceBD()
										.getCurrencyCodeForStation(loggedInAgentStation);
								payCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgenctCode,
										payment.getAgent(), exchangeRateProxy);

								paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_BSP, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {

								payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(selCurrency, ipgId, exchangeRateProxy,
										false);
								ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
										payCurrencyDTO.getPayCurrencyCode());
								ipgIdentificationParamsDTO
										.setFQIPGConfigurationName(ipgId + "_" + payCurrencyDTO.getPayCurrencyCode());
								ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);

								UserInputDTO userInputDTO = new UserInputDTO();
								userInputDTO.setBuyerAddress1(creditInfo.getBillingAddress1());
								userInputDTO.setBuyerAddress2(creditInfo.getBillingAddress2());
								userInputDTO.setBuyerCity(creditInfo.getCity());
								userInputDTO.setPostalCode(creditInfo.getPostalCode());
								userInputDTO.setBuyerState(creditInfo.getState());
								// userInputDTO.setPayerCountry(contactInfo.getCountry());
								userInputDTO.setBuyerFirstName(contactInfo.getFirstName());
								userInputDTO.setBuyerLastName(contactInfo.getLastName());

								paymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType().trim()).intValue(),
										creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
										creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(),
										creditInfo.getCardCVV().trim(), consumedPayAmount, AppIndicatorEnum.APP_XBE,
										TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), creditInfo.getCardHoldersName(),
										payment.getPaymentReference(), payment.getActualPaymentMethod(), null, userInputDTO);

							}

						}

					}
					if (jPCInfo != null) {
						Iterator<?> uIter = jPCInfo.iterator();
						while (uIter.hasNext()) {
							JSONObject jUObj = (JSONObject) uIter.next();
							String carrierCode = (String) jUObj.get("carrier");
							Integer debitPaxRefNo = new Integer((String) jUObj.get("paxCreditId"));
							BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
							BigDecimal residingCarrierAmount = new BigDecimal((String) jUObj.get("residingCarrierAmount"));
							String pnr = (String) jUObj.get("pnr");
							paymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, amount,
									exchangeRateProxy.getExecutionDate(), residingCarrierAmount, null, null,
									resPax.getSeqNumber(), null, pnr, null);
							totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, amount);
						}
					}
				}

				if (PaxTypeTO.ADULT.equals(paxSummaryTo.getPaxType()) && paxSummaryTo.getInfantName() != null
						&& !paxSummaryTo.getInfantName().equals("")) {
					paymentAssembler.setPaxType(PaxTypeTO.PARENT);
				} else {
					paymentAssembler.setPaxType(paxSummaryTo.getPaxType());
				}

				if (isGroupPNR || lCCClientResAlterQueryModesTO.isGroupPnr()) {
					segmentAssembler.addPassengerPayments(PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()),
							paymentAssembler);
				} else {
					segmentAssembler.addPassengerPayments(PaxTypeUtils.getPnrPaxId(paxSummaryTo.getTravelerRefNumber()),
							paymentAssembler);
				}

			}

		}

		// Add auto cancellation information
		if (AppSysParamsUtil.isAutoCancellationEnabled() && paymentTO.getMode().equals("CONFIRM")
				&& ReservationInternalConstants.ReservationStatus.CONFIRMED
						.equals(lCCClientResAlterQueryModesTO.getReservationStatus())) {
			segmentAssembler.setAutoCancellationEnabled(true);
			segmentAssembler.setHasBufferTimeAutoCnxPrivilege(AuthorizationsUtil.hasPrivilege(mapPrivileges.keySet(),
					PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALT_RES_AUTO_CNX));
		}

		segmentAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));

		lCCClientResAlterQueryModesTO.setActualPayment(actualPayment);

		return lCCClientResAlterQueryModesTO;
	}

	public static LCCClientResAlterQueryModesTO getAlterModesToForModifySeg(BookingShoppingCart bookingShoppingCart,
			PaymentTO paymentTO, CreditCardTO creditInfoTO, Integer ipgId, String OwnerAgentCode, String paxCredit,
			boolean isGroupPNR, CommonReservationContactInfo contactInfo, String selCurrency, String loggedInAgenctCode,
			String loggedInAgentStation, Map<String, String> mapPrivileges, XBEReservationInfoDTO resInfo, boolean actualPayment)
			throws Exception {
		LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO = bookingShoppingCart.getReservationAlterModesTO();
		ReservationBalanceTO reservationBalanceTO = bookingShoppingCart.getReservationBalance();
		Collection<ReservationPaxTO> paxList = bookingShoppingCart.getPaxList();

		JSONArray jsonPaxPayArray = (JSONArray) new JSONParser().parse(paxCredit);

		PaymentMethod paymentMethod = paymentTO.getPaymentMethod();

		int noofCreditPax = 0;
		Iterator<?> itIter = jsonPaxPayArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			if (new BigDecimal((String) jPPObj.get("displayToPay")).compareTo(BigDecimal.ZERO) > 0) {
				noofCreditPax++;
			}
		}

		Map<Integer, ReservationPaxTO> resPaxMap = new HashMap<Integer, ReservationPaxTO>();
		int noOfAdults = 0;
		for (ReservationPaxTO resPax : paxList) {
			if (!resPax.getPaxType().equals(PaxTypeTO.INFANT)) {
				noOfAdults++;
			}
			resPaxMap.put(resPax.getSeqNumber(), resPax);
		}

		ExternalChargesMediator creditExtChargesMediator;

		ExternalChargesMediator externalChargesMediator;

		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, loggedInAgenctCode)) {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
		} else {
			externalChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false);
		}

		if (new BigDecimal(paymentTO.getAmount().trim()).compareTo(BigDecimal.ZERO) <= 0) {
			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, loggedInAgenctCode)) {
				creditExtChargesMediator = new ExternalChargesMediator(bookingShoppingCart, true);
			} else {
				creditExtChargesMediator = new ExternalChargesMediator(bookingShoppingCart, false);
			}
		} else {
			creditExtChargesMediator = new ExternalChargesMediator(bookingShoppingCart,
					paymentMethod.getCode().equals(PaymentMethod.CRED.getCode()));
		}

		com.isa.thinair.xbe.core.web.v2.util.ReservationUtil.getApplicableServiceTax(bookingShoppingCart,
				EXTERNAL_CHARGES.JN_OTHER);

		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(noOfAdults, 0);
		LinkedList perPaxCreditExternalCharges = null;
		if (!paymentTO.getMode().equals("CONFIRM")) {
			creditExtChargesMediator.setCalculateJNTaxForCCCharge(true);
			perPaxCreditExternalCharges = creditExtChargesMediator.getExternalChargesForABalancePayment(noofCreditPax, 0);
		}

		LCCClientSegmentAssembler segmentAssembler = lCCClientResAlterQueryModesTO.getLccClientSegmentAssembler();
		segmentAssembler.setOwnerAgent(OwnerAgentCode);
		segmentAssembler.setApplicableAgentCommissions(bookingShoppingCart.getAgentCommissions());
		segmentAssembler.setContactInfo(contactInfo);
		BookingUtil.populateInsurance(bookingShoppingCart, segmentAssembler, noOfAdults);

		PaymentHolder paymentHolder = new PaymentHolder();
		if (BookingUtil.isDoubleAgentPay(paymentTO)) {
			if (paymentTO.getAgentOneAmount() != null && !"".equals(paymentTO.getAgentOneAmount().trim())) {
				paymentTO.setAmount(paymentTO.getAgentOneAmount());

			}

			PaymentTO second = paymentTO.clone();
			second.setAgent(paymentTO.getAgentTwo());
			second.setAmount(paymentTO.getAgentTwoAmount());

			paymentHolder.addPayment(paymentTO);
			paymentHolder.addPayment(second);

		} else if (creditInfoTO != null) {
			paymentHolder.addCardPayment(paymentTO, creditInfoTO);
		} else {
			paymentHolder.addPayment(paymentTO);
		}

		PayCurrencyDTO payCurrencyDTO = null;
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		BigDecimal totalPaxCredit = BigDecimal.ZERO;

		List<LCCClientPassengerSummaryTO> colPassengerSummary = (List<LCCClientPassengerSummaryTO>) reservationBalanceTO
				.getPassengerSummaryList();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		for (LCCClientPassengerSummaryTO paxSummaryTo : colPassengerSummary) {
			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
				BigDecimal amountDueWithExt = AccelAeroCalculator.add(paxSummaryTo.getTotalCreditAmount().negate(),
						paxSummaryTo.getTotalAmountDue());
				LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
				ReservationPaxTO resPax = resPaxMap.get(PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()));

				// BigDecimal amountDueWithExt = amountDue;
				BigDecimal amountWithoutExt = amountDueWithExt;
				if (perPaxExternalCharges.size() > 0) {
					List<LCCClientExternalChgDTO> extCharges = BookingUtil
							.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
					extCharges.addAll(resPax.getExternalCharges());
					paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);
					BigDecimal extChgTotal = BookingUtil.getTotalExtCharge(extCharges);
					amountWithoutExt = AccelAeroCalculator.subtract(amountDueWithExt, extChgTotal);
				}

				boolean blnCreditPax = false;
				boolean creditFlag = false;

				if (!paymentTO.getMode().equals("CONFIRM")) {
					// segmentAssembler.setPnrZuluReleaseTimeStamp(null);
					itIter = jsonPaxPayArray.iterator();
					JSONArray jPCInfo = null;

					while (itIter.hasNext()) {
						JSONObject jPPObj = (JSONObject) itIter.next();
						String jtrevelRefNo = BeanUtils.nullHandler(jPPObj.get("displayTravelerRefNo"));

						if (jtrevelRefNo.equalsIgnoreCase(paxSummaryTo.getTravelerRefNumber())) {
							jPCInfo = (JSONArray) jPPObj.get("paxCreditInfo");
							if (new BigDecimal((String) jPPObj.get("displayToPay")).compareTo(BigDecimal.ZERO) > 0) {
								blnCreditPax = true;
							}
						}
					}
					if (blnCreditPax && perPaxCreditExternalCharges.size() > 0) {
						List<LCCClientExternalChgDTO> ccChgs = BookingUtil
								.converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxCreditExternalCharges.pop());
						BigDecimal ccChgTotal = BookingUtil.getTotalExtCharge(ccChgs);
						amountDueWithExt = AccelAeroCalculator.add(amountDueWithExt, ccChgTotal);
						paymentAssembler.getPerPaxExternalCharges().addAll(ccChgs);
					}

					if (jPCInfo != null) {
						Iterator<?> uIter = jPCInfo.iterator();
						while (uIter.hasNext()) {
							JSONObject jUObj = (JSONObject) uIter.next();
							BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
							amountWithoutExt = AccelAeroCalculator.add(amountWithoutExt, amount.negate());
							amountDueWithExt = AccelAeroCalculator.add(amountDueWithExt, amount.negate());
							creditFlag = true;
						}
					}

					if (PaymentUtil.isValidPaymentExist(creditFlag, amountDueWithExt,
							lCCClientResAlterQueryModesTO.getReservationStatus())) {

						paymentHolder.consumePayment(amountWithoutExt, amountDueWithExt);

						while (paymentHolder.hasNonRecordedConsumedPayments()) {
							String consumedPayRef = paymentHolder.getNextConsumedPaymentRef();
							BigDecimal consumedPayAmount = paymentHolder.getConsumedPaymentAmount(consumedPayRef);
							PaymentTO payment = paymentHolder.getPayment(consumedPayRef);
							CreditCardTO creditInfo = paymentHolder.getCreditCardTO(consumedPayRef);

							paymentMethod = payment.getPaymentMethod();

							if (paymentMethod.getCode().equals(PaymentMethod.CASH.getCode())) {
								payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCashPay(selCurrency, exchangeRateProxy);
								paymentAssembler.addCashPayment(consumedPayAmount, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), null, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.ACCO.getCode())) {
								payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(selCurrency, loggedInAgenctCode,
										payment.getAgent(), exchangeRateProxy);

								paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.BSP.getCode())) {

								// Override the selected currency with Agent country currency code for BSP
								String bspCurrency = ModuleServiceLocator.getCommonServiceBD()
										.getCurrencyCodeForStation(loggedInAgentStation);
								payCurrencyDTO = PaymentUtil.getBSPPayCurrencyDTO(bspCurrency, loggedInAgenctCode,
										payment.getAgent(), exchangeRateProxy);

								paymentAssembler.addAgentCreditPayment(payment.getAgent(), consumedPayAmount, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), payment.getPaymentReference(),
										payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_BSP, null);

							} else if (paymentMethod.getCode().equals(PaymentMethod.CRED.getCode())) {

								payCurrencyDTO = PaymentUtil.getPaymentCurrencyForCardPay(selCurrency, ipgId, exchangeRateProxy,
										false);
								ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
										payCurrencyDTO.getPayCurrencyCode());
								ipgIdentificationParamsDTO
										.setFQIPGConfigurationName(ipgId + "_" + payCurrencyDTO.getPayCurrencyCode());
								ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);

								UserInputDTO userInputDTO = new UserInputDTO();
								userInputDTO.setBuyerAddress1(creditInfo.getBillingAddress1());
								userInputDTO.setBuyerAddress2(creditInfo.getBillingAddress2());
								userInputDTO.setBuyerCity(creditInfo.getCity());
								userInputDTO.setPostalCode(creditInfo.getPostalCode());
								userInputDTO.setBuyerState(creditInfo.getState());
								// userInputDTO.setPayerCountry(contactInfo.getCountry());
								userInputDTO.setBuyerFirstName(contactInfo.getFirstName());
								userInputDTO.setBuyerLastName(contactInfo.getLastName());

								paymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType().trim()).intValue(),
										creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
										creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(),
										creditInfo.getCardCVV().trim(), consumedPayAmount, AppIndicatorEnum.APP_XBE,
										TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, payCurrencyDTO,
										exchangeRateProxy.getExecutionDate(), creditInfo.getCardHoldersName(),
										payment.getPaymentReference(), payment.getActualPaymentMethod(), null, userInputDTO);

							}

						}

					}
					if (jPCInfo != null) {
						Iterator<?> uIter = jPCInfo.iterator();
						while (uIter.hasNext()) {
							JSONObject jUObj = (JSONObject) uIter.next();
							String carrierCode = (String) jUObj.get("carrier");
							Integer debitPaxRefNo = new Integer((String) jUObj.get("paxCreditId"));
							BigDecimal amount = new BigDecimal((String) jUObj.get("usedAmount"));
							BigDecimal residingCarrierAmount = new BigDecimal((String) jUObj.get("residingCarrierAmount"));
							String pnr = (String) jUObj.get("pnr");
							paymentAssembler.addPaxCreditPayment(carrierCode, debitPaxRefNo, amount,
									exchangeRateProxy.getExecutionDate(), residingCarrierAmount, null, null,
									resPax.getSeqNumber(), null, pnr, null);
							totalPaxCredit = AccelAeroCalculator.add(totalPaxCredit, amount);
						}
					}
				} else {
					if (resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
						if (segmentAssembler.getPnrZuluReleaseTimeStamp() == null) {
							throw new XBEException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
						}
					}
				}

				if (PaxTypeTO.ADULT.equals(paxSummaryTo.getPaxType()) && paxSummaryTo.getInfantName() != null
						&& !paxSummaryTo.getInfantName().equals("")) {
					paymentAssembler.setPaxType(PaxTypeTO.PARENT);
				} else {
					paymentAssembler.setPaxType(paxSummaryTo.getPaxType());
				}

				if (lCCClientResAlterQueryModesTO.isGroupPnr()) {
					segmentAssembler.addPassengerPayments(PaxTypeUtils.getPaxSeq(paxSummaryTo.getTravelerRefNumber()),
							paymentAssembler);
				} else {
					segmentAssembler.addPassengerPayments(PaxTypeUtils.getPnrPaxId(paxSummaryTo.getTravelerRefNumber()),
							paymentAssembler);
				}

			}

		}

		// Add auto cancellation information
		if (AppSysParamsUtil.isAutoCancellationEnabled() && paymentTO.getMode().equals("CONFIRM")
				&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(resInfo.getReservationStatus())) {
			segmentAssembler.setAutoCancellationEnabled(true);
			segmentAssembler.setHasBufferTimeAutoCnxPrivilege(AuthorizationsUtil.hasPrivilege(mapPrivileges.keySet(),
					PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALT_RES_AUTO_CNX));
		}

		segmentAssembler.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));

		lCCClientResAlterQueryModesTO.setActualPayment(actualPayment);

		return lCCClientResAlterQueryModesTO;
	}

	private static void populateInsurance(BookingShoppingCart bookingShoppingCart,
			LCCClientSegmentAssembler lcclientSegmentAssembler, int adultCount) {

		List<LCCInsuranceQuotationDTO> lccInsQuoteDTOs = bookingShoppingCart.getInsuranceQuotes();
		LCCClientReservationInsurance insurance = new LCCClientReservationInsurance();
		if (lccInsQuoteDTOs != null && !lccInsQuoteDTOs.isEmpty()) {
			for (LCCInsuranceQuotationDTO lccInsQuoteDTO : lccInsQuoteDTOs) {
				LCCInsuredJourneyDTO insuredJourneyDTO = new LCCInsuredJourneyDTO();
				insuredJourneyDTO.setJourneyEndDate(lccInsQuoteDTO.getInsuredJourney().getJourneyEndDate());
				insuredJourneyDTO.setJourneyStartDate(lccInsQuoteDTO.getInsuredJourney().getJourneyStartDate());
				insuredJourneyDTO.setJourneyStartAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyStartAirportCode());
				insuredJourneyDTO.setJourneyEndAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyEndAirportCode());
				insuredJourneyDTO.setRoundTrip(lccInsQuoteDTO.getInsuredJourney().isRoundTrip());
				insurance.setInsuranceQuoteRefNumber(lccInsQuoteDTO.getInsuranceRefNumber());
				insurance.setInsuredJourneyDTO(insuredJourneyDTO);
				insurance.setQuotedTotalPremium(lccInsQuoteDTO.getQuotedTotalPremiumAmount());
				insurance.setApplicablePaxCount(adultCount);
				insurance.setOperatingAirline(lccInsQuoteDTO.getOperatingAirline());
				insurance.setPlanCode(lccInsQuoteDTO.getPlanCode());
				insurance.setSsrFeeCode(lccInsQuoteDTO.getSsrFeeCode());

				lcclientSegmentAssembler.addResInsurance(insurance);
			}
		}
	}

	public static boolean isDoubleAgentPay(PaymentTO payment) {

		if (!StringUtil.isNullOrEmpty(payment.getAgent()) && !StringUtil.isNullOrEmpty(payment.getAgentTwo())
				&& !StringUtil.isNullOrEmpty(payment.getAgentOneAmount())
				&& !StringUtil.isNullOrEmpty(payment.getAgentTwoAmount())) {
			return true;
		}
		return false;
	}

	public static void applyAncillaryTax(BookingShoppingCart bookingShoppingCart, FlightSegmentTO flightSegmentTO,
			Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModifyingSegAnciTotal) {
		if (bookingShoppingCart.isTaxApplicable(EXTERNAL_CHARGES.JN_ANCI)) {
			for (ReservationPaxTO resPaxTO : bookingShoppingCart.getPaxList()) {
				ExternalChargeUtil.setPaxEffectiveExternalChargeTaxAmount(resPaxTO,
						bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI), flightSegmentTO.getFlightRefNumber(),
						paxWiseModifyingSegAnciTotal);
			}

		}
	}

	public static BigDecimal applyServiceTax(Collection<ReservationPaxTO> paxList, List<FlightSegmentTO> flightSegmentTOs,
			CommonReservationContactInfo contactInfo, TrackInfoDTO trackInfoDTO, SYSTEM system) throws ModuleException {
		Map<ReservationPaxTO, List<LCCClientExternalChgDTO>> paxChgMap = new HashMap<>();
		for (ReservationPaxTO resPaxTO : paxList) {
			Map<String, LCCClientExternalChgDTO> addedChgs = getChgCodeFltRefWiseAddedChargeDTOs(resPaxTO);
			Map<String, BigDecimal> removedChgAmts = getChgCodeFltRefWiseRemovedChgAmts(resPaxTO);
			setEffectiveChgs(addedChgs, removedChgAmts);
			List<LCCClientExternalChgDTO> charges = new ArrayList<>();
			for (LCCClientExternalChgDTO extChg : addedChgs.values()) {
				charges.add(extChg);
			}
			paxChgMap.put(resPaxTO, charges);
		}

		ServiceTaxQuoteCriteriaForTktDTO request = createRequest(paxChgMap, flightSegmentTOs, contactInfo, system);
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator.getAirproxyReservationBD()
				.quoteServiceTaxForTicketingRevenue(request, trackInfoDTO);
		return ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTax(paxList, serviceTaxQuoteRS, false, ApplicationEngine.XBE,
				false);

	}

	private static ServiceTaxQuoteCriteriaForTktDTO createRequest(Map<ReservationPaxTO, List<LCCClientExternalChgDTO>> paxChgMap,
			List<FlightSegmentTO> flightSegmentTOs, CommonReservationContactInfo contactInfo, SYSTEM system) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<>();
		for (ReservationPaxTO paxTO : paxChgMap.keySet()) {
			paxWiseExternalCharges.put(paxTO.getSeqNumber(), paxChgMap.get(paxTO));
			paxWisePaxTypes.put(paxTO.getSeqNumber(), paxTO.getPaxType());
		}

		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		baseAvailRQ.getAvailPreferences().setSearchSystem(system);
		baseAvailRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.XBE);
		String paxState = contactInfo.getState();
		String paxCountryCode = contactInfo.getCountryCode();
		boolean paxTaxRegistered = contactInfo.getTaxRegNo() != null && !"".equals(contactInfo.getTaxRegNo());

		ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
		serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
		// serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO());
		serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
		serviceTaxQuoteCriteriaDTO.setPaxState(paxState);
		serviceTaxQuoteCriteriaDTO.setPaxCountryCode(paxCountryCode);
		serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered(paxTaxRegistered);
		serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
		serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
		serviceTaxQuoteCriteriaDTO.setCalculateOnlyForExternalCharges(true);
		serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(null);

		return serviceTaxQuoteCriteriaDTO;
	}

	private static void setEffectiveChgs(Map<String, LCCClientExternalChgDTO> addedChgs, Map<String, BigDecimal> removedChgAmts) {
		Iterator<Entry<String, LCCClientExternalChgDTO>> itr = addedChgs.entrySet().iterator();
		while (itr.hasNext()) {
			String key = itr.next().getKey();
			if (removedChgAmts.containsKey(key)) {
				LCCClientExternalChgDTO chg = addedChgs.get(key);
				if (chg.getAmount().compareTo(removedChgAmts.get(key)) <= 0) {
					itr.remove();
				} else {
					chg.setAmount(AccelAeroCalculator.subtract(chg.getAmount(), removedChgAmts.get(key)));
				}
			}
		}
	}

	private static Map<String, BigDecimal> getChgCodeFltRefWiseRemovedChgAmts(ReservationPaxTO resPaxTO) {
		Map<String, BigDecimal> removedChgAmts = new HashMap<>();
		for (LCCSelectedSegmentAncillaryDTO segAnci : resPaxTO.getAncillariesToRemove()) {
			if (segAnci.getAirSeatDTO() != null && segAnci.getAirSeatDTO().getSeatNumber() != null
					&& !segAnci.getAirSeatDTO().getSeatNumber().trim().equals("")) {
				BigDecimal charge = segAnci.getAirSeatDTO().getSeatCharge();
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.SEAT_MAP.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getMealDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCMealDTO meal : segAnci.getMealDTOs()) {
					charge = AccelAeroCalculator.add(charge,
							AccelAeroCalculator.multiply(meal.getMealCharge(), new BigDecimal(meal.getAllocatedMeals())));

				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.MEAL.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getBaggageDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCBaggageDTO baggage : segAnci.getBaggageDTOs()) {
					charge = AccelAeroCalculator.add(charge, baggage.getBaggageCharge());
				}

				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|" + EXTERNAL_CHARGES.BAGGAGE.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getSpecialServiceRequestDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
					charge = AccelAeroCalculator.add(charge, ssr.getCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAirportServiceDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAirportServiceDTO airportService : segAnci.getAirportServiceDTOs()) {
					charge = AccelAeroCalculator.add(charge, airportService.getServiceCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AIRPORT_SERVICE.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
			if (segAnci.getAirportTransferDTOs() != null) {
				BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCAirportServiceDTO airportTrasnsfer : segAnci.getAirportTransferDTOs()) {
					charge = AccelAeroCalculator.add(charge, airportTrasnsfer.getServiceCharge());
				}
				String key = segAnci.getFlightSegmentTO().getFlightRefNumber() + "|"
						+ EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString();
				if (!removedChgAmts.containsKey(key)) {
					removedChgAmts.put(key, charge);
				} else {
					removedChgAmts.put(key, AccelAeroCalculator.add(removedChgAmts.get(key), charge));
				}
			}
		}
		return removedChgAmts;
	}

	private static Map<String, LCCClientExternalChgDTO> getChgCodeFltRefWiseAddedChargeDTOs(ReservationPaxTO resPaxTO) {
		Map<String, LCCClientExternalChgDTO> addedChgs = new HashMap<>();
		for (LCCClientExternalChgDTO extChg : resPaxTO.getExternalCharges()) {
			String key = extChg.getFlightRefNumber() + "|" + extChg.getExternalCharges().toString();

			if (!addedChgs.containsKey(key)) {
				addedChgs.put(key, extChg.clone());
			} else {
				LCCClientExternalChgDTO existinChg = addedChgs.get(key);
				existinChg.setAmount(AccelAeroCalculator.add(extChg.getAmount(), existinChg.getAmount()));
			}
		}
		return addedChgs;
	}

	private static UserInputDTO getUserInputDTO(CreditCardTO creditInfo, ContactInfoTO contactInfo) {
		UserInputDTO userInputDTO = null;
		if (creditInfo != null && contactInfo != null) {
			userInputDTO = new UserInputDTO();
			userInputDTO.setBuyerAddress1(creditInfo.getBillingAddress1());
			userInputDTO.setBuyerAddress2(creditInfo.getBillingAddress2());
			userInputDTO.setBuyerCity(creditInfo.getCity());
			userInputDTO.setPostalCode(creditInfo.getPostalCode());
			userInputDTO.setBuyerState(creditInfo.getState());
			userInputDTO.setPayerCountry(contactInfo.getCountry());
			userInputDTO.setBuyerFirstName(contactInfo.getFirstName());
			userInputDTO.setBuyerLastName(contactInfo.getLastName());
		}

		return userInputDTO;
	}

	private static boolean isValidPaymentOption(BigDecimal consumedPayAmount, BigDecimal totalPayAmountWithServiceCharges) {
		if (AccelAeroCalculator.isGreaterThan(consumedPayAmount, AccelAeroCalculator.getDefaultBigDecimalZero())
				|| AccelAeroCalculator.isGreaterThan(totalPayAmountWithServiceCharges,
						AccelAeroCalculator.getDefaultBigDecimalZero())) {
			return true;
		}
		return false;
	}

	private static boolean isValidPaymentOptionForInfant(boolean isParent, BigDecimal paymentINFAmountWithServiceCharges,
			BigDecimal consumedPayAmount) {
		if (isParent && (AccelAeroCalculator.isGreaterThan(paymentINFAmountWithServiceCharges,
				AccelAeroCalculator.getDefaultBigDecimalZero())
				|| (AccelAeroCalculator.isGreaterThan(consumedPayAmount, AccelAeroCalculator.getDefaultBigDecimalZero())
						&& AccelAeroCalculator.isEqual(paymentINFAmountWithServiceCharges,
								AccelAeroCalculator.getDefaultBigDecimalZero())))) {
			return true;
		}
		return false;
	}

	private static boolean isValidPaymentOptionForInfantAllowZeroPayments(boolean isParent,
			BigDecimal paymentINFAmountWithServiceCharges, BigDecimal consumedPayAmount) {
		if (isParent && (AccelAeroCalculator.isGreaterThan(paymentINFAmountWithServiceCharges,
				AccelAeroCalculator.getDefaultBigDecimalZero())
				|| ((AccelAeroCalculator.isGreaterThan(consumedPayAmount, AccelAeroCalculator.getDefaultBigDecimalZero())
						|| AccelAeroCalculator.isEqual(consumedPayAmount, AccelAeroCalculator.getDefaultBigDecimalZero()))
						&& AccelAeroCalculator.isEqual(paymentINFAmountWithServiceCharges,
								AccelAeroCalculator.getDefaultBigDecimalZero())))) {
			return true;
		}
		return false;
	}

	/**
	 * Records a ZERO payment entry when full discount/promotion is applied for Passenger charges
	 * 
	 */
	private static void recordZeroPaymentEntryForFullDiscount(boolean isPaymentRecordExist, boolean isPaxDiscountApplied,
			BigDecimal totalPayAmountBeforeDiscount, BigDecimal discountAmount,
			LCCClientPaymentAssembler lccClientPaymentAssembler, PaymentHolder paymentHolder, PayCurrencyDTO payCurrencyDTO,
			Date paymentTimestamp, ContactInfoTO contactInfo, IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {

		if (!isPaymentRecordExist && isPaxDiscountApplied
				&& AccelAeroCalculator.subtract(totalPayAmountBeforeDiscount, discountAmount)
						.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 0) {

			PaymentTO payment = paymentHolder.getDummyPaymentMethod();
			PaymentMethod paymentMethod = payment.getPaymentMethod();
			CreditCardTO creditInfo = paymentHolder.getDummyCreditCardTO();

			if (PaymentMethod.CASH.getCode().equals(paymentMethod.getCode())) {

				lccClientPaymentAssembler.addCashPayment(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrencyDTO,
						paymentTimestamp, payment.getPaymentReference(), payment.getActualPaymentMethod(), null, null);

			} else if (PaymentMethod.ACCO.getCode().equals(paymentMethod.getCode())) {

				lccClientPaymentAssembler.addAgentCreditPayment(payment.getAgent(),
						AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrencyDTO, paymentTimestamp,
						payment.getPaymentReference(), payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_ONACCOUNT, null);

			} else if (PaymentMethod.BSP.getCode().equals(paymentMethod.getCode())) {

				lccClientPaymentAssembler.addAgentCreditPayment(payment.getAgent(),
						AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrencyDTO, paymentTimestamp,
						payment.getPaymentReference(), payment.getActualPaymentMethod(), Agent.PAYMENT_MODE_BSP, null);

			} else if (PaymentMethod.CRED.getCode().equals(paymentMethod.getCode()) && creditInfo != null) {

				UserInputDTO userInputDTO = getUserInputDTO(creditInfo, contactInfo);

				lccClientPaymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType().trim()).intValue(),
						creditInfo.getExpiryDateYYMMFormat().trim(), creditInfo.getCardNo().trim(),
						creditInfo.getCardHoldersName().trim(), creditInfo.getAddress(), creditInfo.getCardCVV().trim(),
						AccelAeroCalculator.getDefaultBigDecimalZero(), AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER,
						ipgIdentificationParamsDTO, payCurrencyDTO, paymentTimestamp, creditInfo.getCardHoldersName(),
						payment.getPaymentReference(), payment.getActualPaymentMethod(), null, userInputDTO);

			}

		}

	}

	public static Map<String, BigDecimal> applyServiceTaxAnciModifyDisplay(Collection<ReservationPaxTO> paxList,
			List<FlightSegmentTO> flightSegmentTOs, CommonReservationContactInfo contactInfo, TrackInfoDTO trackInfoDTO,
			SYSTEM system, ReservationFlow reservationFlow) throws ModuleException {
		Map<ReservationPaxTO, List<LCCClientExternalChgDTO>> paxChgMap = new HashMap<>();
		for (ReservationPaxTO resPaxTO : paxList) {
			Map<String, LCCClientExternalChgDTO> addedChgs = getChgCodeFltRefWiseAddedChargeDTOs(resPaxTO);
			Map<String, BigDecimal> removedChgAmts = getChgCodeFltRefWiseRemovedChgAmts(resPaxTO);
			setEffectiveChgs(addedChgs, removedChgAmts);
			List<LCCClientExternalChgDTO> charges = new ArrayList<>();
			for (LCCClientExternalChgDTO extChg : addedChgs.values()) {
				charges.add(extChg);
			}
			paxChgMap.put(resPaxTO, charges);
		}

		ServiceTaxQuoteCriteriaForTktDTO request = createRequest(paxChgMap, flightSegmentTOs, contactInfo, system);
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = ModuleServiceLocator.getAirproxyReservationBD()
				.quoteServiceTaxForTicketingRevenue(request, trackInfoDTO);
		return ServiceTaxCalculatorUtil.addPaxWiseApplicableServiceTaxForDisplay(paxList, serviceTaxQuoteRS, reservationFlow);
	}

}
