package com.isa.thinair.xbe.core.web.util;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;

/**
 * @author Thushara
 * 
 */
public class FlightUtils {

	/**
	 * Formats a Date to a Given String Format
	 * 
	 * @param utilDate
	 *            the Date
	 * @param fmt
	 *            the String Format dd/MM/YY
	 * @return String the Formatted Date 22/06/06
	 */
	public static String formatDate(Date utilDate, String fmt) {
		String strDate = "";
		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			strDate = sdFmt.format(utilDate);
		}
		return strDate;
	}

	/**
	 * Gets the Segment String From a Segment Set
	 * 
	 * @param set
	 *            the Set Containing all the Segments
	 * @return String the Segment String CMB/SHJ/DOH
	 */
	public static String getSegment(Set set) {
		String strSeg = "";
		String strSegArray = "";
		int prevLen = 0;
		FlightSegement seg = null;
		if (set != null) {
			Iterator ite = set.iterator();

			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightSegement) ite.next();
					strSeg = seg.getSegmentCode();
					if (prevLen < strSeg.length()) {
						strSegArray = strSeg;
						prevLen = strSeg.length();
					}
				}
			}
		}
		return strSegArray;
	}

	/**
	 * Gets the first leg of the given Flight leg list
	 * 
	 * @param legList
	 *            the Collection of Legs
	 * @return FlightLeg the first flight leg
	 */
	public static FlightLeg getFirstFlightLeg(Collection legList) {
		Iterator it = legList.iterator();
		FlightLeg leg = null;
		while (it.hasNext()) {
			leg = (FlightLeg) it.next();
			if (leg.getLegNumber() == 1)
				break;
		}
		return leg;
	}

	/**
	 * Get last leg of the given Flight leg list
	 * 
	 * @param legList
	 *            the Collection of Flight Legs
	 * @return FlightLeg the Last Flight Leg
	 */
	public static FlightLeg getLastFlightLeg(Collection legList) {
		Iterator it = legList.iterator();
		FlightLeg leg = null;
		int legnum = 0;
		while (it.hasNext()) {
			FlightLeg currentLeg = (FlightLeg) it.next();
			if (legnum < currentLeg.getLegNumber()) {
				legnum = currentLeg.getLegNumber();
				leg = currentLeg;
			}
		}
		return leg;
	}

	/**
	 * Gets the given Segment Checkin Status String From a Segment Set
	 * 
	 * @param set
	 *            the Set Containing all the Segments
	 * @return String the Segment String CMB/SHJ/DOH
	 */
	public static String getSegmentCheckinStatus(Set set, String airportCode) {
		String strSeg = "";
		FlightSegement seg = null;
		if (set != null) {
			Iterator ite = set.iterator();

			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightSegement) ite.next();
					strSeg = seg.getSegmentCode().split("/")[0];
					if (strSeg.equals(airportCode)) {
						return seg.getCheckinStatus();
					}
				}
			}
		}
		return "";
	}

	/**
	 * Gets the flight segment id for given starting airport
	 * 
	 * @param set
	 *            the Set Containing all the Segments
	 * @return flight segment id
	 */
	public static int getSegmentId(Set set, String airportCode) {
		String strSeg = "";
		FlightSegement seg = null;
		if (set != null) {
			Iterator ite = set.iterator();

			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightSegement) ite.next();
					strSeg = seg.getSegmentCode().split("/")[0];
					if (strSeg.equals(airportCode)) {
						return seg.getFltSegId();
					}
				}
			}
		}
		return -1;
	}
}
