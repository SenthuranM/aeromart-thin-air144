package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.alerting.api.dto.PnrRequestStatusDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * POJO to handle the Viewing the statuses and history of requests on a PNR
 * 
 * @author Sashrika Waidyarathna
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ViewQueueRequestStatusesAction {
	private static Log log = LogFactory.getLog(ViewQueueRequestStatusesAction.class);
	private List<Map<String,Object>> row;
	private String pnrId;
	private int page;
	private int total;
	private int records;	
		
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getPnrId() {
		return pnrId;
	}

	public void setPnrId(String pnrId) {
		this.pnrId = pnrId;
	}

	public List<Map<String, Object>> getRow() {
		return row;
	}

	public void setRow(List<Map<String, Object>> row) {
		this.row = row;
	}

	public String execute() {
		return S2Constants.Result.SUCCESS;
	}
	
	public String loadRequestsOnPnr() {
		Page<PnrRequestStatusDTO> pnrStatuses=null;
		try {
			pnrStatuses = ModuleServiceLocator.getAirproxyAlertingBD().getRequestStatusForPnr((this.page - 1) * 5, 5, pnrId);
		} catch (Exception e) {
			e.printStackTrace();  // handle exception
		}
		total = pnrStatuses.getTotalNoOfRecords() / 5;
		records = pnrStatuses.getTotalNoOfRecords();
		int mod = pnrStatuses.getTotalNoOfRecords() % 5;

		if (mod > 0) {
			this.total = this.total + 1;
		}
		setRow(getRequestDTOList((List<PnrRequestStatusDTO>) pnrStatuses
				.getPageData()));		
		return S2Constants.Result.SUCCESS;
	}
	
	public ArrayList<Map<String, Object>> getRequestDTOList(
			List<PnrRequestStatusDTO> requestList) {
		ArrayList<Map<String, Object>> requestStatusesDTOList = new ArrayList<Map<String, Object>>();
		PnrRequestStatusDTO pnrRequestStatus = null;
		Iterator<PnrRequestStatusDTO> iterator = requestList.iterator();
		while (iterator.hasNext()) {
			pnrRequestStatus = iterator.next();
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("pnrRequestStatus", pnrRequestStatus);
			requestStatusesDTOList.add(row);
		}
		return requestStatusesDTOList;
	}
}
