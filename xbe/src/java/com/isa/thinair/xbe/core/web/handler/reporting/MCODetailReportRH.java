package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class MCODetailReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(MCODetailReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static final String REPORT_PAGE_CODES_MAPPINGS_RESOURCE_BUNDLE = "resources/i18n/XBETranslationData";

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		try {
			setDisplayData(request);
			log.error("MCODetailReportRH SETDISPLAYDATA() SUCCESS ");
		} catch (ModuleException ex) {
			log.error("MCODetailReportRH SETDISPLAYDATA() FAILED ", ex);
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("MCODetailReportRH setReportView Success");
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("MCODetailReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setDisplayAgencyMode(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get(PrivilegesKeys.MCOPrivilegesKeys.ALLOW_SEARCH_ANY_MCO) != null) {
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("xbe.res.mco.search.own") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
	}

	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			
			String agentStatus = "All";
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs,
					getResourceBundle(request, REPORT_PAGE_CODES_MAPPINGS_RESOURCE_BUNDLE),agentStatus);
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	private static ResourceBundle getResourceBundle(HttpServletRequest request, String path) {
		String lang = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (lang == null) {
			lang = WebConstants.DEFAULT_LANGUAGE;
		}
		Locale locale = new Locale(lang);
		ResourceBundle rb = ResourceBundle.getBundle(path, locale);
		return rb;
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String depDate = request.getParameter("txtDepDate");
		String flightNo = request.getParameter("txtFlightNo");
		String agents = "";
		String value = request.getParameter("radReportOption");
		String reportTemplate = "MCODetailReport.jasper";
		boolean isSelectedAllAgents = false;
		if (request.getParameter("hdnSelectedAllAgents") != null && request.getParameter("hdnSelectedAllAgents").equals("true")) {
			isSelectedAllAgents = true;
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}
		ArrayList<String> agentCol = new ArrayList<String>();

		String id = "UC_REPM_096";
		if (!isSelectedAllAgents && !agents.equals("")) {
			String agentArr[] = agents.split(",");
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
		}

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (depDate != null && !depDate.equals("")) {
				search.setFlightDateRangeFrom(ReportsHTMLGenerator.convertDate(depDate));
			}
			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo);
			}

			search.setSelectedAllAgents(isSelectedAllAgents);
			search.setAgents(agentCol);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getMCODetailReport(search);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("AGENT_CODE", agents);

			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			// To provide Report Translation
			String locale = null;
			Object localeObject = request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
			if (localeObject == null) {
				locale = WebConstants.DEFAULT_LANGUAGE;
			} else {
				locale = localeObject.toString();
			}
			parameters.put("language", locale);

			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			String reportName = "MCODetailReport";

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (value.trim().equals("PDF")) {
				response.reset();
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error("setReportView Failed : " + e);
		}
	}
}
