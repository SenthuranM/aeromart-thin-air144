package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.Collection;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.PrintRecieptUtil;
import com.isa.thinair.airreservation.api.dto.RecieptLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reservation.V2_PRINTRECIEPT),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class PrintRecieptAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(PrintItineraryAction.class);

	private String hdnPNRNo;
	private String recieptLanguage;
	private boolean groupPNR;
	private boolean includePaxFinancials = true;
	private boolean includePaymentDetails = true;
	private boolean includeTicketCharges = true;
	private String selectedPax;
	private String hdnPayId;
	private String recieptNo;
	private String hdnPayRef;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			getRecieptInfo();
		} catch (Exception e) {
			String msg = BasicRH.getErrorMessage(e,userLanguage);

			forward = S2Constants.Result.ERROR;

			JavaScriptGenerator.setServerError(request, msg, "top", "");
			log.error(msg, e);
		}

		return forward;
	}

	/**
	 * TODO Generate the Itinerary Body
	 * 
	 * @throws ModuleException
	 */
	private void getRecieptInfo() throws ModuleException {

		boolean isCheckIndividual = false;

		LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(hdnPNRNo, groupPNR, recieptLanguage, request, false,
				null, null, null, false);
		hdnPayId = request.getParameter(WebConstants.PARAM_HDN_PAY_ID);

		String reciept = null;

		recieptLanguage = "en";
		Locale locale = new Locale(recieptLanguage);

		RecieptLayoutModesDTO recieptLayoutModesDTO = new RecieptLayoutModesDTO();
		recieptLayoutModesDTO.setPnr(hdnPNRNo);
		recieptLayoutModesDTO.setLocale(locale);

		recieptLayoutModesDTO.setIncludePassengerPrices(includeTicketCharges);
		recieptLayoutModesDTO.setIncludeDetailPayments(includePaymentDetails);
		recieptLayoutModesDTO.setIncludeDetailCharges(includePaxFinancials);
		recieptLayoutModesDTO.setPayId(hdnPayRef);

		if (hdnPayId != null) {
			recieptLayoutModesDTO.setPayId(hdnPayId);
		}

		Collection<Integer> selectedPNRPaxIdCol = null;
		Reservation reservation = PrintRecieptUtil.getReservation(pnrModesDTO, getTrackInfoDto());
		selectedPNRPaxIdCol = PrintRecieptUtil.getPnrPaxList(reservation);

		reciept = ModuleServiceLocator.getAirproxyReservationBD().getRecieptPrint(recieptLayoutModesDTO, selectedPNRPaxIdCol,
				isCheckIndividual, getTrackInfoDto());

		request.setAttribute(WebConstants.REQ_RECIEPT_BODY_HTML, reciept);

	}

	public String getHdnPNRNo() {
		return hdnPNRNo;
	}

	public void setHdnPNRNo(String hdnPNRNo) {
		this.hdnPNRNo = hdnPNRNo;
	}

	public String getRecieptLanguage() {
		return recieptLanguage;
	}

	public void setRecieptLanguage(String recieptLanguage) {
		this.recieptLanguage = recieptLanguage;
	}

	public boolean isIncludePaxFinancials() {
		return includePaxFinancials;
	}

	public void setIncludePaxFinancials(boolean includePaxFinancials) {
		this.includePaxFinancials = includePaxFinancials;
	}

	public boolean isIncludePaymentDetails() {
		return includePaymentDetails;
	}

	public void setIncludePaymentDetails(boolean includePaymentDetails) {
		this.includePaymentDetails = includePaymentDetails;
	}

	public boolean isIncludeTicketCharges() {
		return includeTicketCharges;
	}

	public void setIncludeTicketCharges(boolean includeTicketCharges) {
		this.includeTicketCharges = includeTicketCharges;
	}

	public String getSelectedPax() {
		return selectedPax;
	}

	public void setSelectedPax(String selectedPax) {
		this.selectedPax = selectedPax;
	}

	public String getHdnPayId() {
		return hdnPayId;
	}

	public void setHdnPayId(String hdnPayId) {
		this.hdnPayId = hdnPayId;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getRecieptNo() {
		return recieptNo;
	}

	public void setRecieptNo(String recieptNo) {
		this.recieptNo = recieptNo;
	}

	public String getHdnPayRef() {
		return hdnPayRef;
	}

	public void setHdnPayRef(String hdnPayRef) {
		this.hdnPayRef = hdnPayRef;
	}

	private TrackInfoDTO getTrackInfoDto() {
		return super.getTrackInfo();
	}

}
