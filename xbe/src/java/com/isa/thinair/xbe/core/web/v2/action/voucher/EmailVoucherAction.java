package com.isa.thinair.xbe.core.web.v2.action.voucher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author chanaka
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class EmailVoucherAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(EmailVoucherAction.class);

	private VoucherDTO voucherDTO;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		if (AppSysParamsUtil.isVoucherEnabled()) {
			UserPrincipal userPrincipal = (UserPrincipal) this.request.getUserPrincipal();
			voucherDTO.setIssuedUserId(userPrincipal.getUserId());
			try {
				VoucherBD voucherDelegate = ModuleServiceLocator.getVoucherBD();
				voucherDelegate.emailVoucher(voucherDTO);
			} catch (ModuleException me) {
				log.error("EmailVoucherAction ==> execute() emailing voucher failed", me);
				if (me.getExceptionCode().equals("promotions.voucher.amount.local.null")) {
					forward = S2Constants.Result.ERROR;
				}
			}
		}
		return forward;
	}

	/**
	 * @return the voucher
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO
	 *            the voucher to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}
}
