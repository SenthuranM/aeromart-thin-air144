package com.isa.thinair.xbe.core.web.v2.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class AutoCheckinUtil {

	private static final Log log = LogFactory.getLog(AutoCheckinUtil.class);
	private static final String AUTO_CHECKIN_VISIBILITY = "_autoCheckinVisibility";
	private static final String AUTO_CHECKIN_MANDATORY = "_autoCheckinMandatory";
	private static final String VISIBILITY = "visibility";
	private static final String MANDATORY = "mandatory";

	/**
	 * This method will override country config over basic pax config
	 * 
	 * @param paxConfig
	 * @param countryPaxConfig
	 * @return
	 */
	public static JSONObject overrideWithCountryConfigs(Map<String, XBEPaxConfigDTO> paxConfig,
			Map<String, Boolean> countryPaxConfig) {
		JSONObject paxConfigJson = new JSONObject();
		JSONObject paxField;
		for (Entry<String, XBEPaxConfigDTO> paxConfigDTO : paxConfig.entrySet()) {
			String fieldName = paxConfigDTO.getKey();
			if (fieldName.contains("_"))
				continue;
			paxField = new JSONObject();
			if (countryPaxConfig.get(fieldName) != null) {
				try {
					paxField.put(VISIBILITY, countryPaxConfig.get(fieldName + AUTO_CHECKIN_VISIBILITY));
					paxField.put(MANDATORY, countryPaxConfig.get(fieldName + AUTO_CHECKIN_MANDATORY));
					paxConfigJson.put(fieldName, paxField);
				} catch (JSONException e) {
					log.error("Error on override with country configs");
				}

			} else {
				try {
					paxField.put(VISIBILITY, paxConfigDTO.getValue().getAutoCheckinVisibility());
					paxField.put(MANDATORY, paxConfigDTO.getValue().getAutoCheckinMandatory());
					paxConfigJson.put(fieldName, paxField);
				} catch (JSONException e) {
					log.error("Error on getting basic pax configs");
				}
			}
		}

		return paxConfigJson;
	}

	/**
	 * This method will return pax details configurations for auto checkin ancillary page
	 * 
	 * @param flightSegmentTOs
	 * @param tracInfo
	 * @param trackInfoDTO
	 * @param paxType
	 * @param system
	 * @return
	 * @throws ModuleException
	 * @throws JSONException
	 */
	public static JSONObject getPaxWiseAutoCheckinConfig(List<FlightSegmentTO> flightSegmentTOs, BasicTrackInfo tracInfo,
			TrackInfoDTO trackInfoDTO, String paxType, String system) throws ModuleException, JSONException {

		Map<String, XBEPaxConfigDTO> paxConfigAD = new HashMap<String, XBEPaxConfigDTO>();

		Map<String, XBEPaxConfigDTO> paxConfigCH = new HashMap<String, XBEPaxConfigDTO>();

		Map<String, XBEPaxConfigDTO> paxConfigIN = new HashMap<String, XBEPaxConfigDTO>();

		Map<String, List<String>> uniqGroupAD = new HashMap<String, List<String>>();

		Map<String, List<String>> uniqGroupCH = new HashMap<String, List<String>>();

		Map<String, List<String>> uniqGroupIN = new HashMap<String, List<String>>();

		Map<String, Boolean> countryAutoCheckinPaxConfigAD;

		Map<String, Boolean> countryAutoCheckinPaxConfigCH;

		Map<String, Boolean> countryAutoCheckinPaxConfigIN;

		JSONObject airportWisePaxConfig = new JSONObject();

		JSONObject paxTypeWiseConfig;

		PaxContactConfigDTO paxContactDTO;

		Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs;

		List<String> airportList;

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			countryAutoCheckinPaxConfigAD = new HashMap<String, Boolean>();
			countryAutoCheckinPaxConfigCH = new HashMap<String, Boolean>();
			countryAutoCheckinPaxConfigIN = new HashMap<String, Boolean>();
			airportList = new ArrayList<String>();
			airportList.add(flightSegmentTO.getSegmentCode().split("/")[0]);
			paxTypeWiseConfig = new JSONObject();

			paxContactDTO = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxContactConfig(system,
					AppIndicatorEnum.APP_XBE.toString(), null, tracInfo);
			PassengerUtil.populatePaxCatConfig(paxContactDTO.getPaxConfigList(), paxType, paxConfigAD, paxConfigCH, paxConfigIN,
					uniqGroupAD, uniqGroupCH, uniqGroupIN);

			// loading country wise configs for above origin and destination
			countryWiseConfigs = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxCountryConfig(system,
					AppIndicatorEnum.APP_XBE.toString(), airportList, trackInfoDTO);

			if (countryWiseConfigs != null && countryWiseConfigs.size() > 0) {
				// generating pax category type configs for Autocheckin ancillary
				PassengerUtil.populateCountryWisePaxConfigs(countryWiseConfigs, countryAutoCheckinPaxConfigAD,
						countryAutoCheckinPaxConfigCH, countryAutoCheckinPaxConfigIN, paxType);

				if (countryAutoCheckinPaxConfigAD.size() > 0 && countryAutoCheckinPaxConfigCH.size() > 0
						&& countryAutoCheckinPaxConfigIN.size() > 0) {
					paxTypeWiseConfig.put(ReservationInternalConstants.PassengerType.ADULT,
							overrideWithCountryConfigs(paxConfigAD, countryAutoCheckinPaxConfigAD));
					paxTypeWiseConfig.put(ReservationInternalConstants.PassengerType.CHILD,
							overrideWithCountryConfigs(paxConfigCH, countryAutoCheckinPaxConfigCH));
					paxTypeWiseConfig.put(ReservationInternalConstants.PassengerType.INFANT,
							overrideWithCountryConfigs(paxConfigIN, countryAutoCheckinPaxConfigIN));
					airportWisePaxConfig.put(airportList.get(0), paxTypeWiseConfig);
				}
			}
		}
		return airportWisePaxConfig;
	}
}
