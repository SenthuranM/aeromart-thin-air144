/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Chamindap, RumeshW
 * 
 */
public class ViewSeatInventoryCollectionsRH extends BasicRH {

	private static Log log = LogFactory.getLog(ViewSeatInventoryCollectionsRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public ViewSeatInventoryCollectionsRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("ViewSeatInventoryCollectionsRH success");
		} catch (Exception e) {
			forward = S2Constants.Result.ERROR;
			log.error("ViewSeatInventoryCollectionsRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("ViewSeatInventoryCollectionsRH setReportView Success");
				return null;
			} else {
				log.error("ViewSeatInventoryCollectionsRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ViewSeatInventoryCollectionsRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setReportTypeList(request);
		setAirportComboList(request);
		setOperationTypeHtml(request);
		setCarrierCodeList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	private static void setCarrierCodeList(HttpServletRequest request) throws ModuleException {

		Collection userCarrierCodes = getUserCarrierCodes(request);

		String carrierCodes = ReportsHTMLGenerator.createCarrierCodeWithDefaultHtml(userCarrierCodes);
		request.setAttribute(WebConstants.REQ_HTML_CARRIER_CODE_LIST, carrierCodes);
	}

	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationType();
		request.setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);
	}

	/**
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportTypeList(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		/* Need to check if selReportType is already selected */

		String strReportType = request.getParameter("selReportType");

		if (strReportType != null && strReportType.equalsIgnoreCase(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_FLIGHT)) {
			sb.append("<option selected='selected' value='BY_FLIGHT'>By Flight</option>");
			sb.append("<option value='BY_STATION'>By Station</option>");
			sb.append("<option value='BY_AGENT'>By Agent</option>");
		} else if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_STATION)) {
			sb.append("<option value='BY_FLIGHT'>By Flight</option>");
			sb.append("<option selected='selected' value='BY_STATION'>By Station</option>");
			sb.append("<option value='RTREOPRT_AGENT_SUMMARY'>By Agent</option>");

		} else if (strReportType != null && strReportType.equalsIgnoreCase(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_AGENT)) {
			sb.append("<option value='BY_FLIGHT'>By Flight</option>");
			sb.append("<option value='BY_STATION'>By Station</option>");
			sb.append("<option selected='selected' value='BY_AGENT'>By Agent</option>");

		} else {
			sb.append("<option value='BY_FLIGHT'>By Flight</option>");
			sb.append("<option value='BY_STATION'>By Station</option>");
			sb.append("<option value='BY_AGENT'>By Agent</option>");

		}

		String strHtml = sb.toString();

		request.setAttribute("reqReportTypeList", strHtml);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setAirportComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String strReportType = request.getParameter("selReportType");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String bookedFromDate = request.getParameter("txtBookedFromDate");
		String bookedToDate = request.getParameter("txtBookedToDate");
		String value = request.getParameter("radReportOption");
		String from = request.getParameter("selDept");
		String to = request.getParameter("selArival");
		String flightNo = request.getParameter("txtFlightNo");
		String exactFlightNo = request.getParameter("chkExactFN");
		String selVia1 = request.getParameter("selVia1");
		String selVia2 = request.getParameter("selVia2");
		String selVia3 = request.getParameter("selVia3");
		String selVia4 = request.getParameter("selVia4");
		String strOperationType = request.getParameter("selOperationType");
		String strFlightStatus = request.getParameter("selFlightStatus");
		String carrierCode = request.getParameter("hdnCarrierCode");

		boolean showRevenueColumn = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_SHOW_REVENUE_SEAT_INVENT_RPT);

		boolean showFullColumns = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_VIEW_FULL_SEAT_INVENT_RPT);

		boolean showAditionalColumns = AppSysParamsUtil.isShowFwdBookingReportAdditionalInfo();

		boolean isExactFlightNumber = (exactFlightNo != null && exactFlightNo.equalsIgnoreCase("on")) ? true : false;

		List<String> viaList = new ArrayList<String>();
		if (selVia1 != null && !selVia1.equals("")) {
			viaList.add(selVia1);
		}
		if (selVia2 != null && !selVia2.equals("")) {
			viaList.add(selVia2);
		}
		if (selVia3 != null && !selVia3.equals("")) {
			viaList.add(selVia3);
		}
		if (selVia4 != null && !selVia4.equals("")) {
			viaList.add(selVia4);
		}
		String id = "UC_REPM_015";
		String reportTemplate = "";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		boolean logicalCabinClassEnable = AppSysParamsUtil.isLogicalCabinClassEnabled();
		String logicalCCEnable = "" + logicalCabinClassEnable + "";
		Collection<String> carrierCodesCol = new ArrayList<String>();
		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59");
			}
			if (bookedFromDate != null && !bookedFromDate.equals("")) {
				search.setDateRange2From(ReportsHTMLGenerator.convertDate(bookedFromDate) + " 00:00:00");
			}
			if (bookedToDate != null && !bookedToDate.equals("")) {
				search.setDateRange2To(ReportsHTMLGenerator.convertDate(bookedToDate) + "  23:59:59");
			}
			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo.trim());
				search.setExactFlightNumber(isExactFlightNumber);
			}
			if (to != null && !to.equals("")) {
				search.setSectorTo(to);
			}
			if (from != null && !from.equals("")) {
				search.setSectorFrom(from);
			}

			if (strOperationType != null && !strOperationType.equals("")) {
				search.setOperationType(strOperationType);
			}

			if (strFlightStatus != null && !strFlightStatus.equals("")) {
				search.setFlightStatus(strFlightStatus);
			}

			String carrierCodeArr[] = null;
			if (!carrierCode.trim().equals("")) {
				if (carrierCode != null && carrierCode.indexOf(",") != -1) {
					carrierCodeArr = carrierCode.split(",");
				} else {
					carrierCodeArr = new String[1];
					carrierCodeArr[0] = carrierCode;
				}
			}
			if (carrierCodeArr != null && carrierCodeArr.length > 0) {
				for (String element : carrierCodeArr) {
					carrierCodesCol.add(element);
				}
			}

			search.setCarrierCodes(carrierCodesCol);
			search.setViaPoints(viaList);
			search.setReportOption(ReportsSearchCriteria.FORWARD_BOOKING);

			Map<String, Object> parameters = new HashMap<String, Object>();
			if (strReportType.equals(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_FLIGHT)) {
				search.setByFlight(true);
				parameters.put("SHOW_FLIGHT", "Y");
				parameters.put("SHOW_STATION", "N");
				reportTemplate = "ViewSeatInvFareMvmntTot.jasper";
			} else if (strReportType.equals(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_STATION)) {
				search.setByStation(true);
				parameters.put("SHOW_STATION", "Y");
				reportTemplate = "ViewSeatInvFareMvmntTot.jasper";
			} else if (strReportType.equals(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_AGENT)) {
				search.setByAgent(true);
				parameters.put("SHOW_AGENT", "Y");
				reportTemplate = "ViewSeatInvFareMvmntAgt.jasper";
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getViewSeatInventoryFareMovements(search);

			parameters.put("LOGICAL_CABIN_CLASS_ENABLE", logicalCCEnable);
			parameters.put("SHOW_FULL_COLUMNS", String.valueOf(showFullColumns));
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("BOOKED_FROM_DATE", BeanUtils.nullHandler(bookedFromDate));
			parameters.put("BOOKED_TO_DATE", BeanUtils.nullHandler(bookedToDate));
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			parameters.put("SHOW_TOTAL_COLUMNS", "TRUE");
			parameters.put("SHOW_REVENUE_DATA", String.valueOf(showRevenueColumn));
			parameters.put("SHOW_ADDITIONAL_COLUMNS", String.valueOf(showAditionalColumns));

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			
			if(AppSysParamsUtil.isLogicalCabinClassEnabled()){
				parameters.put("CABINCLASS", "Y");
			}
			String reportName = "SeatInventoryAndCollectionsReport";
			
			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				parameters.put("SHOW_TOTAL_COLUMNS", "FALSE");
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				parameters.put("SHOW_TOTAL_COLUMNS", "FALSE");
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=SeatInventoryAndCollectionsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}
