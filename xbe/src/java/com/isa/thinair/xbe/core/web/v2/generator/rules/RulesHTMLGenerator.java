package com.isa.thinair.xbe.core.web.v2.generator.rules;

import com.isa.thinair.commons.api.exception.ModuleException;

public class RulesHTMLGenerator {

	public static final String DATA_ELE_FIRST_NAME = "First Name";
	public static final String DATA_ELE_LAST_NAME = "Last Name";
	public static final String DATA_ELE_DOB = "Date Of Birth";
	public static final String DATA_ELE_PASSPORT = "Passport No";
	public static final String DATA_ELE_NATIONALITY = "Nationality";

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static String createRulesDataElementList() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		//sb.append("var showDataElementCombo = true;");
		sb.append("var arrData = new Array();");
		sb.append("var methods = new Array();");
		sb.append("methods[0] = new Array('" + DATA_ELE_FIRST_NAME + "','" + DATA_ELE_FIRST_NAME + "'); ");
		sb.append("methods[1] = new Array('" + DATA_ELE_LAST_NAME + "','" + DATA_ELE_LAST_NAME + "'); ");
		sb.append("methods[2] = new Array('" + DATA_ELE_DOB + "','" + DATA_ELE_DOB + "'); ");
		sb.append("methods[3] = new Array('" + DATA_ELE_PASSPORT + "','" + DATA_ELE_PASSPORT + "'); ");
		sb.append("methods[4] = new Array('" + DATA_ELE_NATIONALITY + "','" + DATA_ELE_NATIONALITY + "'); ");
		sb.append("arrData[0] = methods;");
		sb.append("var dataElements = new Listbox('lstDataElements', 'lstAssignedDataElements', 'spn3', 'dataElements');");
		sb.append("dataElements.group1 = arrData[0];");
		sb.append("dataElements.height = '75px'; dataElements.width = '170px';");
		sb.append("dataElements.headingLeft = '&nbsp;&nbsp;All Data Elements';");
		sb.append("dataElements.headingRight = '&nbsp;&nbsp;Selected Data Elements';");
		sb.append("dataElements.drawListBox();");

		return sb.toString();
	}

}
