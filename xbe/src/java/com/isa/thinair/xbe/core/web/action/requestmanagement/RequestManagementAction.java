/**
 * 
 */
package com.isa.thinair.xbe.core.web.action.requestmanagement;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;

/**
 * @author suneth
 *
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class RequestManagementAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(SearchRequestsAction.class);
	
	private static final String OPEN = "OPEN";
	private static final String CLOSE = "CLSE";
	private static final String REOPEN = "ROPN";
	private static final String EDIT = "EDIT";
	
	private int queueId;
	
	private String pnr;
	
	private String description;
	
	private int requestId;
	
	private String remark;
	
	private String status;
	
	private String queuesForPage;
	
	private String pageID;
	
	public String execute(){
		
		String success = S2Constants.Result.SUCCESS;
		
		QueueRequest queueRequest = new QueueRequest((UserPrincipal) request.getUserPrincipal());
		
		queueRequest.setQueueId(queueId);
		queueRequest.setPnr(pnr);
		queueRequest.setDescription(description);
		queueRequest.setStatus(OPEN);
		
		try {
			
			ModuleServiceLocator.getAirproxyAlertingBD().submitOrEditRequest(queueRequest);
			
		} catch (ModuleException e) {
			
			success = "error";
			e.printStackTrace();
			log.error("Submitting Request Error ==> ", e);
			
		}
		
		return success;
		
	}
	
	public String editRequest(){
		
		String success = S2Constants.Result.SUCCESS;
		
		QueueRequest queueRequest = null;
		
		try {
			
			queueRequest = ModuleServiceLocator.getAirproxyAlertingBD().getQueueRequest(requestId);
		
		} catch (ModuleException e) {
			success = "error";
			e.printStackTrace();
			log.error("Getting the Request by request ID Error ==> ", e);
			return success;
		}
		
		queueRequest.setUserDetails((UserPrincipal) request.getUserPrincipal());
		queueRequest.setQueueId(queueId);
		queueRequest.setPnr(pnr);
		queueRequest.setDescription(description);
		
		try {
			
			RequestHistory requestHistory = createRequestHistoryModel("Request Edited", OPEN, OPEN , EDIT);
			ModuleServiceLocator.getAirproxyAlertingBD().submitOrEditRequest(queueRequest);
			ModuleServiceLocator.getAirproxyAlertingBD().addRequestHistory(requestHistory);
			
		} catch (ModuleException e) {
			
			success = "error";
			e.printStackTrace();
			log.error("Request Modification Error ==> ", e);
		}

		return success;
		
	}
	
	public String reopenRequest(){
		
		String success = S2Constants.Result.SUCCESS;
		QueueRequest queueRequest = null;
		
		try {
			
			queueRequest = ModuleServiceLocator.getAirproxyAlertingBD().getQueueRequest(requestId);
		
		} catch (ModuleException e) {
			success = "error";
			e.printStackTrace();
			log.error("Getting the Request by request ID Error ==> ", e);
			return success;
		}
		
		if(queueRequest.getStatus().equals(CLOSE)){
			
			queueRequest.setStatus(OPEN);
			queueRequest.setUserDetails((UserPrincipal) request.getUserPrincipal());
			
			try {
				
				RequestHistory requestHistory = createRequestHistoryModel(remark, CLOSE, OPEN , REOPEN); 
				ModuleServiceLocator.getAirproxyAlertingBD().submitOrEditRequest(queueRequest);
				ModuleServiceLocator.getAirproxyAlertingBD().addRequestHistory(requestHistory);
				
			} catch (ModuleException e) {

				success = "error";
				e.printStackTrace();
				log.error("Request Modification Error when reopening ==> ", e);
				
			}
			
		}else{
			
			success = "error";
			
		}
		
		return success;
		
	}
	
	public String deleteRequest(){
		
		String success = S2Constants.Result.SUCCESS;
		
		try {
			
			ModuleServiceLocator.getAirproxyAlertingBD().deleteRequest(requestId);
			
		} catch (ModuleException e) {

			success = "error";
			e.printStackTrace();
			log.error("Request Delete Error ==> ", e);
			
		}
		
		return success;
		
	}
	
	
	public String actionRequest(){
		
		String success = S2Constants.Result.SUCCESS;
		
		QueueRequest queueRequest = null;
		String previousStatus;
		
		try {
			
			queueRequest = ModuleServiceLocator.getAirproxyAlertingBD().getQueueRequest(requestId);
		
		} catch (ModuleException e) {
			success = "error";
			e.printStackTrace();
			log.error("Getting the Request by request ID Error ==> ", e);
			return success;
		}
		
		queueRequest.setUserDetails((UserPrincipal) request.getUserPrincipal());
		previousStatus = queueRequest.getStatus();
		queueRequest.setStatus(status);
		
		if(previousStatus.equals(OPEN)){
			
			queueRequest.setAssignedTo(request.getUserPrincipal().getName());
			queueRequest.setAssignedOn(new Date());
			
		}
		
		try {
			
			RequestHistory requestHistory = createRequestHistoryModel(remark, previousStatus, status, status);
			ModuleServiceLocator.getAirproxyAlertingBD().submitOrEditRequest(queueRequest);
			ModuleServiceLocator.getAirproxyAlertingBD().addRequestHistory(requestHistory);
			
		} catch (ModuleException e) {
			
			success = "error";
			e.printStackTrace();
			log.error("Request Modification Error ==> ", e);
		}

		return success;
		
	}

	private RequestHistory createRequestHistoryModel(String remark , String fromStatus , String toStatus , String action){
		
		RequestHistory requestHistory = new RequestHistory();
		
		requestHistory.setRequestId(requestId);
		requestHistory.setRemark(remark);
		requestHistory.setTimestamp(new Timestamp(Calendar.getInstance().getTime().getTime()));
		requestHistory.setUserId(request.getUserPrincipal().getName());
		requestHistory.setAction(action);
		requestHistory.setStatusChange(fromStatus +"->" + toStatus);
		
		return requestHistory;
	}
	
	public String getQueueNames(){
		
		String success = S2Constants.Result.SUCCESS;
		
		//String pageId = request.getParameter("pageId");
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("SELECT Q.QUEUE_ID ,");
		sb.append(" Q.QUEUE_NAME ");
		sb.append("FROM T_QUEUE Q ");
		sb.append("INNER JOIN T_QUEUE_PAGE QP ");
		sb.append("ON Q.QUEUE_ID  = QP.QUEUE_ID ");
		sb.append("WHERE STATUS   ='ACT' ");		
		sb.append("AND QP.PAGE_ID = '"+ pageID +"' ");
		sb.append("ORDER BY QUEUE_NAME");
		
		String query = sb.toString();
	
		try {
			queuesForPage = SelectListGenerator.createQueueListForPage(query);
		} catch (ModuleException e) {
			success = S2Constants.Result.ERROR;
			e.printStackTrace();
		}
//		request.setAttribute(WebConstants.REQ_HTML_QUEUES_FOR_PAGE, queuesForPage);

		return success;
	}
	/**
	 * @return the queueId
	 */
	public int getQueueId() {
		return queueId;
	}

	/**
	 * @param queueId the queueId to set
	 */
	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the requestId
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the queuesForPage
	 */
	public String getQueuesForPage() {
		return queuesForPage;
	}

	/**
	 * @param pageID the pageID to set
	 */
	public void setPageID(String pageID) {
		this.pageID = pageID;
	}
	
}
