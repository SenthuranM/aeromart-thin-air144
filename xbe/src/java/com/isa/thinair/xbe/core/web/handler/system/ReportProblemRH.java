package com.isa.thinair.xbe.core.web.handler.system;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.xbe.core.config.XBEModuleConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.system.ReportProblemHG;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class ReportProblemRH extends BasicRH {

	private static Log log = LogFactory.getLog(ReportProblemRH.class);

	private static GlobalConfig globalConfig = XBEModuleUtils.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request) throws Exception {

		String contentType = request.getContentType();
		String forward = S2Constants.Result.SUCCESS;
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (isMultipart) {
			uploadAttachFile(request, contentType);
		}
		try {
			setDisplayData(request);
		} catch (Exception e) {
			forward = S2Constants.Result.ERROR;
			log.error("ReportProblemRH execute()" + e.getMessage());
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
	}

	private static void setClientErrors(HttpServletRequest request) throws Exception {

		String strClientErrors = ReportProblemHG.getClientErrors(request);

		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static void sendProblemMail(String name, String strName, String strAddress, String strProblem, String strSummary)
			throws ModuleException {

		String path = PlatformConstants.getAbsAttachmentsPath() + "/" + name;
		// String strAdmin = ModuleServiceLocator.getGlobalConfig()
		// .getBizParam(SystemParamKeys.ADMIN_EMAIL_ID);
		XBEModuleConfig xbeConfig = XBEModuleUtils.getConfig();

		File flAttachment = new File(path);
		String emailAddress = null;

		Properties props = xbeConfig.getEmailProblemTo();
		Enumeration enumeration = props.elements();

		while (enumeration.hasMoreElements()) {
			emailAddress = (String) enumeration.nextElement();
			MessageProfile msgProfile = new MessageProfile();
			UserMessaging usermsg = new UserMessaging();
			strName = StringUtils.capitalize(strName);
			usermsg.setFirstName(strName);
			usermsg.setLastName("");
			usermsg.setToAddres(emailAddress);

			List<UserMessaging> messageList = new ArrayList<UserMessaging>();
			HashMap<String, Object> map = new HashMap<String, Object>();

			EmailMessage emailmsg = new EmailMessage();
			emailmsg.setFromAddress(strAddress);
			emailmsg.setBody(strProblem);
			emailmsg.setSubject(strSummary);

			map.put("emailMessage", emailmsg);
			map.put("userMessaging", usermsg);

			messageList.add(usermsg);
			msgProfile.setUserMessagings(messageList);

			Topic topic = new Topic();
			topic.setTopicParams(map);
			topic.setTopicName("report_problem");
			if (!name.equals("") && flAttachment.exists()) {
				topic.addAttachmentFileName(name);
			}
			msgProfile.setTopic(topic);

			ModuleServiceLocator.getMessagingBD().sendMessage(msgProfile);
		}
	}

	private static void uploadAttachFile(HttpServletRequest request, String contentType) {

		String strName = request.getParameter("txtName");
		String strAddress = request.getParameter("txtEMail");
		String strProblem = request.getParameter("txtProblem");
		String strSummary = request.getParameter("txtSummary");
		String name = "";
		String fileName = "";

		try {
			FileItemFactory factory = new DiskFileItemFactory();

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Parse the request
			List items = upload.parseRequest(request);

			Iterator iter = items.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				name = item.getFieldName();

				if (item.isFormField()) {
					if (name.equals("txtName"))
						strName = item.getString();
					if (name.equals("txtEMail"))
						strAddress = item.getString();
					if (name.equals("txtProblem"))
						strProblem = item.getString();
					if (name.equals("txtSummary"))
						strSummary = item.getString();

				} else {
					byte fileByte[] = item.get();
					fileName = item.getName();
					if (fileName != null && !fileName.trim().equals(""))
						uploadAttachment(fileName, fileByte);
				}

			}

			sendProblemMail(fileName, strName, strAddress, strProblem, strSummary);

		} catch (Exception e) {
			log.error(e);
		}

	}

	private static void uploadAttachment(String name, byte[] fileByte) {

		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		// Code to FTP
		serverProperty.setServerName(globalConfig.getFtpServerName());
		if (globalConfig.getFtpServerUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(globalConfig.getFtpServerUserName());
		}
		serverProperty.setUserName(globalConfig.getFtpServerUserName());
		if (globalConfig.getFtpServerPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(globalConfig.getFtpServerPassword());
		}

		ftpUtil.setPassiveMode(globalConfig.isFtpEnablePassiveMode());
		ftpUtil.uploadAttachment(name, fileByte, serverProperty);
		// End Code to FTP */
	}
}
