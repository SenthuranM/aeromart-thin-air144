package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AncillaryUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class for Meals
 * 
 * @author Dilan Anuruddha
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciMealAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AnciMealAction.class);

	private boolean success = true;

	private String messageTxt;

	private String selectedFlightList;

	private FlightSearchDTO searchParams;

	private LCCMealResponceDTO mealResponceDTO;

	private boolean modifyAncillary = false;

	private String resPaxInfo;

	private boolean isRequote = false;

	private String jsonOnds;

	private String pnr;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String strTxnIdntifier = null;
			SYSTEM system = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			String classOfService = null;
			Map<String, Set<String>> fltRefWiseSelectedMeal = null;
			List<BundledFareDTO> bundledFareDTOs = null;

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(selectedFlightList);
				system = fltSegBuilder.getSystem();
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
				classOfService = fltSegBuilder.getClassOfService();

				if (resPaxInfo != null && !resPaxInfo.equals("")) {
					Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPaxInfo);
					fltRefWiseSelectedMeal = AncillaryUtil.getFlightReferenceWiseSelectedMeals(colpaxs);
				}
				bundledFareDTOs = resInfo.getBundledFareDTOs();

			} else {
				strTxnIdntifier = resInfo.getTransactionId();
				system = resInfo.getSelectedSystem();
				if (isRequote) {
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}
				classOfService = searchParams.getClassOfService();

				BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
						S2Constants.Session_Data.LCC_SES_BOOKING_CART);
				bundledFareDTOs = bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getOndBundledFareDTOs();

			}

			List<LCCMealRequestDTO> lccMealRequestDTOs = composeMealRequest(flightSegmentTOs, classOfService, strTxnIdntifier);

			mealResponceDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableMeals(lccMealRequestDTOs,
					TrackInfoUtil.getBasicTrackInfo(request), fltRefWiseSelectedMeal, bundledFareDTOs, system, strTxnIdntifier,
					ApplicationEngine.XBE, pnr);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("MEAL REQ ERROR", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * Compose the meal request list from the flight segment list
	 * 
	 * @param flightSegTO
	 * @param txnId
	 * @return List<LCCMealRequestDTO>
	 */
	private List<LCCMealRequestDTO> composeMealRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCMealRequestDTO> mL = new ArrayList<LCCMealRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCMealRequestDTO mrd = new LCCMealRequestDTO();
			mrd.setTransactionIdentifier(txnId);
			if (cabinClass != null && !modifyAncillary) {
				mrd.setCabinClass(cabinClass); // For make res
			} else {
				mrd.setCabinClass(fst.getCabinClassCode()); // Modify Res or add anci
			}
			mrd.getFlightSegment().add(fst);
			mL.add(mrd);
		}
		return mL;
	}

	/**
	 * getters and setters
	 */
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public LCCMealResponceDTO getMealResponceDTO() {
		return mealResponceDTO;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
