package com.isa.thinair.xbe.core.web.v2.action.travagent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.v2.generator.IncentiveSchemeHG;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.V2_INCENTIVE_SCHEME),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class LoadIncentiveAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(LoadIncentiveAction.class);

	public String execute() {
		try {
			if (AppSysParamsUtil.isTravelAgentIncentiveAnabled()) {
				String schemeList = IncentiveSchemeHG.createIncentiveSchemeList(request);
				String agentMulitiSelList = IncentiveSchemeHG.setAgentsMultiSelect();
				request.setAttribute("reqSchemeList", schemeList);
				request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, agentMulitiSelList);
				request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, IncentiveSchemeHG.getClientErrors(request));
			} else {
				return S2Constants.Result.ERROR;
			}

		} catch (ModuleException me) {
			log.error("Error in IncentiveSchemeAction" + me.getExceptionCode());
		}
		return S2Constants.Result.SUCCESS;
	}
}
