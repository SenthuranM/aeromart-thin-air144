package com.isa.thinair.xbe.core.web.generator.travagent;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentApplicableOND;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.DateUtil;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Shakir
 * 
 */
public class TravelAgentHTMLGenerator extends BasicRH {

	private static Log log = LogFactory.getLog(TravelAgentHTMLGenerator.class);

	private static String clientErrors;

	/**
	 * Gets the Travel Agent Grid Data According to Seach critiriea
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param gsaCode
	 *            TODO
	 * @param isAirlineUser
	 * @return String the Array of Agents
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getTravelAgentRowHtml(HttpServletRequest request, String trtryCode, String gsaCode, boolean isAirlineUser)
			throws ModuleException {

		log.debug("***********inside TravelAgentHTMLGenerator.getTravelAgentRowHTML()");

		Page page = null;
		Collection<Agent> colAgents = null;

		int RecNo;
		if (request.getParameter("hdnRecNo") == null) {
			RecNo = 1;
		} else {
			RecNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();

		ModuleCriterion MCagentType = new ModuleCriterion();

		ModuleCriterion MCStation = new ModuleCriterion();
		MCStation.setCondition(ModuleCriterion.CONDITION_EQUALS);
		MCStation.setFieldName("stationCode");

		ModuleCriterion MCterritory = new ModuleCriterion();
		MCterritory.setCondition(ModuleCriterion.CONDITION_EQUALS);
		MCterritory.setFieldName("territoryCode");

		ModuleCriterion MCname = new ModuleCriterion();
		MCname.setCondition(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE);
		MCname.setFieldName("agentName");

		ModuleCriterion MCcode = new ModuleCriterion();
		MCcode.setCondition(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE);
		MCcode.setFieldName("agentCode");

		ModuleCriterion MCIataCode = new ModuleCriterion();
		MCIataCode.setCondition(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE);
		MCIataCode.setFieldName("agentIATANumber");

		ModuleCriterion MCStatus = new ModuleCriterion();
		MCStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
		MCStatus.setFieldName("status");

		// for gsa change
		ModuleCriterion MCreptogsa = new ModuleCriterion();
		MCreptogsa.setFieldName("reportingToGSA");

		ModuleCriterion MCgsa = new ModuleCriterion();
		MCgsa.setCondition(ModuleCriterion.CONDITION_EQUALS);
		MCgsa.setFieldName("gsaCode");

		if (request.getParameter("chkGroup1") != null && !(request.getParameter("chkGroup1").equals(""))) {
			List<String> valueType = new ArrayList<String>();
			if (request.getParameter("chkGroup1").trim().equals("GSA")) {
				if (BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
					MCagentType.setCondition(ModuleCriterion.CONDITION_EQUALS);
					valueType.add(request.getParameter("chkGroup1"));
				} else {
					MCagentType.setCondition(ModuleCriterion.CONDITION_NOT_IN);
					valueType.add("GSA");
					valueType.add("BSP");
				}
			} else {
				MCagentType.setCondition(ModuleCriterion.CONDITION_NOT_IN);
				valueType.add("GSA");
				valueType.add("BSP");
			}
			MCagentType.setFieldName("agentTypeCode");
			MCagentType.setValue(valueType);
			critrian.add(MCagentType);
		}

		if (request.getParameter("hdnStationCMB") != null && !(request.getParameter("hdnStationCMB").equals(""))) {
			List<String> valueStation = new ArrayList<String>();
			valueStation.add(request.getParameter("hdnStationCMB"));
			MCStation.setValue(valueStation);
			critrian.add(MCStation);
		}

		if (request.getParameter("hdnTerritoryCMB") != null && !(request.getParameter("hdnTerritoryCMB").equals(""))) {
			List<String> valueTerritory = new ArrayList<String>();
			valueTerritory.add(request.getParameter("hdnTerritoryCMB"));
			MCterritory.setValue(valueTerritory);
			critrian.add(MCterritory);
		}

		if (request.getParameter("hdnAgentCMB") != null && !(request.getParameter("hdnAgentCMB").equals(""))) {
			List<String> valueName = new ArrayList<String>();
			valueName.add("%" + request.getParameter("hdnAgentCMB") + "%");
			MCname.setValue(valueName);
			critrian.add(MCname);
		}

		if (request.getParameter("hdnSearchCode") != null && !(request.getParameter("hdnSearchCode").equals(""))
				&& request.getParameter("hdnSearchAirline").trim().toUpperCase().equals("ALL")) {
			List<String> valueCode = new ArrayList<String>();
			valueCode.add("%" + request.getParameter("hdnSearchCode") + "%");
			MCcode.setValue(valueCode);
			critrian.add(MCcode);

		} else if (request.getParameter("hdnSearchCode") != null && !(request.getParameter("hdnSearchCode").equals(""))
				&& !request.getParameter("hdnSearchAirline").trim().toUpperCase().equals("ALL")) {
			List<String> valueCode = new ArrayList<String>();
			valueCode.add(request.getParameter("hdnSearchAirline").trim().toUpperCase() + "%"
					+ request.getParameter("hdnSearchCode") + "%");
			MCcode.setValue(valueCode);
			critrian.add(MCcode);

		} else if (request.getParameter("hdnSearchCode") != null && request.getParameter("hdnSearchCode").equals("")
				&& !request.getParameter("hdnSearchAirline").trim().toUpperCase().equals("ALL")) {
			List<String> valueCode = new ArrayList<String>();
			valueCode.add(request.getParameter("hdnSearchAirline").trim().toUpperCase() + "%");
			MCcode.setValue(valueCode);
			critrian.add(MCcode);
		}

		if (request.getParameter("hdnSearchIataCode") != null && !request.getParameter("hdnSearchIataCode").equals("")) {
			List<String> valueIataCode = new ArrayList<String>();
			valueIataCode.add("%" + request.getParameter("hdnSearchIataCode") + "%");
			MCIataCode.setValue(valueIataCode);
			critrian.add(MCIataCode);
		}
		if (request.getParameter("hdnSearchStatus") != null && !(request.getParameter("hdnSearchStatus").equals(""))
				&& !(request.getParameter("hdnSearchStatus").equals("ALL"))) {
			List<String> valueCode = new ArrayList<String>();
			valueCode.add(request.getParameter("hdnSearchStatus"));
			MCStatus.setValue(valueCode);
			critrian.add(MCStatus);
		}

		if (request.getParameter("hdnMode") != null) {

			// This need s check and back end change
			if(BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_MAINT)){
				
				if (!isAirlineUser && AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
					String agentStatus = request.getParameter("hdnSelStatus");
					if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(agentStatus)) {

						if (Agent.STATUS_ACTIVE.equalsIgnoreCase(agentStatus)
								|| Agent.STATUS_INACTIVE.equalsIgnoreCase(agentStatus)) {
							agentStatus = agentStatus.toUpperCase();
						} else {
							agentStatus = null;
						}
					}

					Collection<String> agentColl = ModuleServiceLocator.getSecurityBD()
							.getReportingAgentCodesIncludeSubAgentLevels(gsaCode, agentStatus, false);
					agentColl.add(gsaCode);
					ModuleCriterion agentCodeMC = new ModuleCriterion();
					agentCodeMC.setCondition(ModuleCriterion.CONDITION_IN);
					agentCodeMC.setFieldName("agentCode");
					List<String> valueGSA = new ArrayList<String>();
					valueGSA.addAll(agentColl);
					agentCodeMC.setValue(valueGSA);
					critrian.add(agentCodeMC);

				}
				
				if (BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY) || 
							BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_MAINT_SEARCH_ANY)) {
					page = ModuleServiceLocator.getTravelAgentBD().searchAgents(critrian, RecNo - 1, 10);
				} else if (BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_GSA)) {
					List<String> valuergsa = new ArrayList<String>();
					MCreptogsa.setCondition(ModuleCriterion.CONDITION_EQUALS);
					valuergsa.add("Y");
					MCreptogsa.setValue(valuergsa);
					critrian.add(MCreptogsa);

					List<String> valueTerritory = new ArrayList<String>();
					valueTerritory.add(trtryCode);
					MCterritory.setValue(valueTerritory);
					critrian.add(MCterritory);

					if (gsaCode != null) {
						List<String> valueGSA = new ArrayList<String>();
						valueGSA.add(gsaCode);
						MCgsa.setValue(valueGSA);
						critrian.add(MCgsa);
					}

					page = ModuleServiceLocator.getTravelAgentBD().searchAgents(critrian, RecNo - 1, 10);
				}

				int totRec = 1;
				if (page != null) {
					totRec = page.getTotalNoOfRecords();
					colAgents = page.getPageData();
				}
				String strJavascriptTotalNoOfRecs = new Integer(totRec).toString();
				request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
				
			}
			
		}

		return createTravleAgents(colAgents);
	}

	/**
	 * Creates the Travel Agent Grid from a Collection of Agents
	 * 
	 * @param colAgents
	 *            the Collection of Agents
	 * @return String the Created Agent Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private final String createTravleAgents(Collection<Agent> colAgents) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		if (colAgents != null && !colAgents.isEmpty()) {
			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			DecimalFormat numberFormat = new DecimalFormat("#");
			Agent agent = null;
			int count = 0;
			String strCountryName = null;
			String gsaname = null;

			Collection<Agent> gsaTerritory = ModuleServiceLocator.getTravelAgentBD().getGSAsForTerritories();
			Iterator<Agent> iterTravelAgent = colAgents.iterator();
			List<String[]> stncntry = (List<String[]>) SelectListGenerator.createStationCountryName();
			while (iterTravelAgent.hasNext()) {
				agent = null;
				strCountryName = null;
				gsaname = null;

				agent = iterTravelAgent.next();
				sb.append("arrData[" + count + "] = new Array();");

				sb.append("arrData[" + count + "][1] = '" + agent.getAgentCode() + "';");
				sb.append("arrData[" + count + "][2] = '" + agent.getAgentName() + "';");

				if (agent.getAgentIATANumber() != null) {
					sb.append("arrData[" + count + "][71] = '" + agent.getAgentIATANumber() + "';");
				} else {
					sb.append("arrData[" + count + "][71] = '';");
				}

				sb.append("arrData[" + count + "][3] = '" + StringUtil.getNotNullString(agent.getTerritoryCode()) + "';");

				if (!agent.getAgentTypeCode().equals("GSA")) {
					sb.append("arrData[" + count + "][3] = '" + StringUtil.getNotNullString(agent.getStationCode()) + "';");
				}

				strCountryName = getCountryName(stncntry, agent.getStationCode());
				sb.append("arrData[" + count + "][4] = '" + StringUtil.getNotNullString(strCountryName) + "';");

				sb.append("arrData["
						+ count
						+ "][5] = '"
						+ ((agent.getCreditLimit() == null) ? decimalFormat.format(0) : decimalFormat.format(agent
								.getCreditLimit())) + "';");
				sb.append("arrData[" + count + "][6] = '" + StringUtil.getNotNullString(agent.getReportingToGSA()) + "';");

				String strStatus = "";
				if (agent.getStatus() != null) {
					if (Agent.STATUS_ACTIVE.equals(agent.getStatus())) {
						strStatus = "Active";
					} else if (Agent.STATUS_INACTIVE.equals(agent.getStatus())) {
						strStatus = "In-Active";
					} else if (Agent.STATUS_NEW.equals(agent.getStatus())) {
						strStatus = "New";
					} else if (Agent.STATUS_BLOCKED.equals(agent.getStatus())) {
						strStatus = "Black Listed";
					}
				} else {
					strStatus = "";
				}

				sb.append("arrData[" + count + "][7] = '" + strStatus + "';");
				sb.append("arrData[" + count + "][8] = '" + StringUtil.getNotNullString(agent.getAddressLine1()) + "';");
				sb.append("arrData[" + count + "][9] = '" + StringUtil.getNotNullString(agent.getAddressLine2()) + "';");
				sb.append("arrData[" + count + "][10] = '" + StringUtil.getNotNullString(agent.getAddressStateProvince()) + "';");
				sb.append("arrData[" + count + "][11] = '" + StringUtil.getNotNullString(agent.getPostalCode()) + "';");
				sb.append("arrData[" + count + "][12] = '" + StringUtil.getNotNullString(agent.getTelephone()) + "';");
				sb.append("arrData[" + count + "][13] = '" + StringUtil.getNotNullString(agent.getFax()) + "';");
				sb.append("arrData[" + count + "][14] = '" + StringUtil.getNotNullString(agent.getEmailId()) + "';");
				sb.append("arrData[" + count + "][15] = '" + StringUtil.getNotNullString(agent.getAgentIATANumber()) + "';");
				sb.append("arrData[" + count + "][16] = '" + StringUtil.getNotNullString(agent.getAccountCode()) + "';");
				sb.append("arrData["
						+ count
						+ "][17] = '"
						+ ((agent.getBankGuaranteeCashAdvance() == null) ? "0" : numberFormat.format(agent
								.getBankGuaranteeCashAdvance())) + "';");
				sb.append("arrData[" + count + "][18] = '" + StringUtil.getNotNullString(agent.getAgentTypeCode()) + "';");
				sb.append("arrData[" + count + "][19] = '" + agent.getVersion() + "';");

				Set<String> set1 = agent.getPaymentMethod();
				if (set1 != null) {
					Iterator<String> iter = set1.iterator();
					while (iter.hasNext()) {
						String strPM = iter.next();
						if (strPM.equals(Agent.PAYMENT_MODE_ONACCOUNT)) {
							sb.append("arrData[" + count + "][20] = '" + strPM + "';");
						}
						if (strPM.equals(Agent.PAYMENT_MODE_CASH)) {
							sb.append("arrData[" + count + "][21] = '" + strPM + "';");
						}
						if (strPM.equals(Agent.PAYMENT_MODE_CREDITCARD)) {
							sb.append("arrData[" + count + "][22] = '" + strPM + "';");
						}
						if (strPM.equals(Agent.PAYMENT_MODE_BSP)) {
							sb.append("arrData[" + count + "][63] = '" + strPM + "';");
						}
						if (strPM.equals(Agent.PAYMENT_MODE_VOUCHER)) {
							sb.append("arrData[" + count + "][78] = '" + strPM + "';");
						}
						if (strPM.equals(Agent.PAYMENT_MODE_OFFLINE)) {
							sb.append("arrData[" + count + "][74] = '" + strPM + "';");
						}
					}
				}
				sb.append("arrData[" + count + "][23] = '" + StringUtil.getNotNullString(agent.getAddressCity()) + "';");

				sb.append("arrData[" + count + "][24] = '" + StringUtil.getNotNullString(agent.getBillingEmail()) + "';");

				sb.append("arrData["
						+ count
						+ "][25] = '"
						+ ((agent.getHandlingChargAmount() == null) ? decimalFormat.format(0) : decimalFormat.format(agent
								.getHandlingChargAmount())) + "';");

				if (agent.getReportingToGSA() != null && agent.getReportingToGSA().equalsIgnoreCase("Y")) {
					gsaname = getGSAName(gsaTerritory, agent);
					if(gsaname.isEmpty() && agent.getGsaCode() != null && !agent.getGsaCode().isEmpty()){
						gsaname = ModuleServiceLocator.getTravelAgentBD().getAgent(agent.getGsaCode()).getAgentName();
					}
				}

				sb.append("arrData[" + count + "][26] = '" + StringUtil.getNotNullString(gsaname) + "';");

				sb.append("arrData[" + count + "][27] = '" + StringUtil.getNotNullString(agent.getCurrencyCode()) + "';");

				sb.append("arrData[" + count + "][28] = '" + StringUtil.getNotNullString(agent.getNotes()) + "';");

				sb.append("arrData[" + count + "][29] = '" + StringUtil.getNotNullString(agent.getStationCode()) + "';");

				String strStatTer = "";
				if (agent.getAgentTypeCode().equals("GSA")) {
					strStatTer = agent.getTerritoryCode() + " / " + agent.getStationCode();
				} else {
					strStatTer = agent.getStationCode();
				}
				sb.append("arrData[" + count + "][30] = '" + strStatTer + "';");

				BigDecimal amountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
				if(AppSysParamsUtil.isGSAStructureVersion2Enabled()){
					Agent creditSharingAgent = ModuleServiceLocator.getTravelAgentBD().getCreditSharingAgent(agent.getAgentCode());
					if (creditSharingAgent.getAgentSummary() == null) {
						sb.append("arrData[" + count + "][31] = '" + new Double(decimalFormat.format(0)) + "';");
					} else {
						sb.append("arrData["
								+ count
								+ "][31] = '"
								+ ((creditSharingAgent.getAgentSummary().getAvailableCredit() == null) ? decimalFormat.format(0) : decimalFormat
										.format(creditSharingAgent.getAgentSummary().getAvailableCredit())) + "';");
						amountDue = agent.getAgentSummary().getOwnDueAmount();
					}
				} else {					
					if (agent.getAgentSummary() == null) {
						sb.append("arrData[" + count + "][31] = '" + new Double(decimalFormat.format(0)) + "';");
					} else {
						sb.append("arrData["
								+ count
								+ "][31] = '"
								+ ((agent.getAgentSummary().getAvailableCredit() == null) ? decimalFormat.format(0) : decimalFormat
										.format(agent.getAgentSummary().getAvailableCredit())) + "';");
						amountDue = AccelAeroCalculator
								.subtract(agent.getCreditLimit(), agent.getAgentSummary().getAvailableCredit());
					}
				}
				

				sb.append("arrData[" + count + "][32] = '"
						+ ((amountDue == null) ? decimalFormat.format(0) : decimalFormat.format(amountDue)) + "';");

				String agentCommCode = StringUtil.getNotNullString(agent.getAgentCommissionCode());
				sb.append("arrData[" + count + "][33] = '" + agentCommCode + "';");
				sb.append("arrData[" + count + "][34] = '" + (agent.getLicenceNumber() == null ? "" : agent.getLicenceNumber())
						+ "';");
				sb.append("arrData[" + count + "][35] = '" + (agent.getOnHold() == null ? "N" : agent.getOnHold()) + "';");

				sb.append("arrData[" + count + "][37] = '" + StringUtil.getNotNullString(agent.getAutoInvoice()) + "';");
				sb.append("arrData["
						+ count
						+ "][38] = '"
						+ numberFormat.format((agent.getBankGuaranteeCashAdvanceLocal() == null) ? 0 : agent
								.getBankGuaranteeCashAdvanceLocal()) + "';");
				sb.append("arrData[" + count + "][39] = '"
						+ decimalFormat.format(agent.getCreditLimitLocal() != null ? agent.getCreditLimitLocal() : 0) + "';");
				sb.append("arrData[" + count + "][40] = '" + agent.getCurrencySpecifiedIn() + "';");

				sb.append("arrData[" + count + "][41] = '" + StringUtil.getNotNullString(agent.getLanguageCode()) + "';");

				sb.append("arrData[" + count + "][42] = '" + StringUtil.getNotNullString(agent.getContactPersonName()) + "';");
				sb.append("arrData[" + count + "][43] = '" + StringUtil.getNotNullString(agent.getLocationUrl()) + "';");
				sb.append("arrData[" + count + "][44] = '" + StringUtil.getNotNullString(agent.getPrefferedRptFormat()) + "';");
				sb.append("arrData[" + count + "][45] = '" + StringUtil.getNotNullString(agent.getServiceChannel()) + "';");
				sb.append("arrData[" + count + "][46] = '" + StringUtil.getNotNullString(agent.getCapturePaymentRef()) + "';");
				sb.append("arrData[" + count + "][47] = '" + StringUtil.getNotNullString(agent.getPayRefMandatory()) + "';");
				// MUEWUI BRANCH MERGE
				sb.append("arrData[" + count + "][48] = '" + StringUtil.getNotNullString(agent.getManualTicketStatus()) + "';");
				// TODO CHECK MERGED CODE
				sb.append("arrData[" + count + "][49] = '" + StringUtil.getNotNullString(agent.getPublishToWeb()) + "';");
				sb.append("arrData[" + count + "][50] = '" + StringUtil.getNotNullString(agent.getMobile()) + "';");
				sb.append("arrData[" + count + "][51] = '" + StringUtil.getNotNullString(agent.getDesignation()) + "';");

				// agent handling fee details

				sb.append("arrData[" + count + "][52] = '"
						+ StringUtil.getNotNullString(String.valueOf(agent.getHandlingFeeAppliesTo())) + "';");
				sb.append("arrData[" + count + "][53] = '" + StringUtil.getNotNullString(agent.getHandlingFeeChargeBasisCode())
						+ "';");
				sb.append("arrData[" + count + "][54] = '"
						+ (agent.getHandlingFeeOnewayAmount() != null ? String.valueOf(agent.getHandlingFeeOnewayAmount()) : "")
						+ "';");
				sb.append("arrData[" + count + "][55] = '"
						+ (agent.getHandlingFeeReturnAmount() != null ? String.valueOf(agent.getHandlingFeeReturnAmount()) : "")
						+ "';");
				sb.append("arrData[" + count + "][56] = '"
						+ (agent.getHandlingFeeMinAmount() != null ? String.valueOf(agent.getHandlingFeeMinAmount()) : "") + "';");
				sb.append("arrData[" + count + "][57] = '"
						+ (agent.getHandlingFeeMaxAmount() != null ? String.valueOf(agent.getHandlingFeeMaxAmount()) : "") + "';");
				sb.append("arrData[" + count + "][58] = '" + StringUtil.getNotNullString(agent.getHandlingFeeEnable()) + "';");
				sb.append("arrData[" + count + "][59] = '" + getCreditLimitInSpecifiedCurrency(agent) + "';");
				sb.append("arrData[" + count + "][60] = '"
						+ decimalFormat.format(agent.getBspGuarantee() == null ? 0 : agent.getBspGuarantee()) + "';");

				sb.append("arrData[" + count + "][61] = '"
						+ decimalFormat.format(agent.getBspGuaranteeLocal() == null ? 0 : agent.getBspGuaranteeLocal()) + "';");

				sb.append("arrData[" + count + "][62] = '" + StringUtil.getNotNullString(agent.getAgentWiseTicketEnable()) + "';");

				if (agent.getAgentSummary() == null) {
					sb.append("arrData[" + count + "][64] = '" + new Double(decimalFormat.format(0)) + "';");
				} else {
					sb.append("arrData["
							+ count
							+ "][64] = '"
							+ ((agent.getAgentSummary().getAvailableBSPCredit() == null) ? decimalFormat.format(0)
									: decimalFormat.format(agent.getAgentSummary().getAvailableBSPCredit())) + "';");
				}

				if (agent.getBankGuranteeExpiryDate() != null) {
					sb.append("arrData[" + count + "][65] = '"
							+ DateUtil.formatDate(agent.getBankGuranteeExpiryDate(), DateUtil.DEFAULT_DATE_INPUT_FORMAT) + "';");
				} else {
					sb.append("arrData[" + count + "][65] = '';");

				}

				sb.append("arrData[" + count + "][66] = '" + StringUtil.getNotNullString(agent.getGsaCode()) + "';");
				sb.append("arrData[" + count + "][67] = '" + (agent.getCharterAgent() == null ? "N" : agent.getCharterAgent())
						+ "';");

				sb.append("arrData[" + count + "][68] = '"
						+ (agent.getCharterAgent() == null ? "N" : agent.getAgentWiseFareMaskEnable()) + "';");
				sb.append("arrData[" + count + "][69] = '" + DateUtil.formatDate(agent.getInactiveFromDate(), "dd/MM/yyyy HH:mm")
						+ "';");
				sb.append("arrData[" + count + "][70] = '" + StringUtil.getNotNullString(agent.getEmailsToNotify()) + "';");
				String ondList = "";
				for (AgentApplicableOND ond : agent.getApplicableOndList()) {
					ondList += ond.getOndCOde() + ",";
				}
				sb.append("arrData[" + count + "][72] = '" + ondList + "';");
								
				//Set visible stations/territories
				
				Set<String> visibleStationsTerritories = null;
				if(AgentType.GSA.equals(agent.getAgentTypeCode())) {
					visibleStationsTerritories = agent.getVisibleTerritories();
				} else if (AgentType.TA.equals(agent.getAgentTypeCode())){
					visibleStationsTerritories = agent.getVisibleStations();
				}
				
				String strAssigned = "";
				
				if(visibleStationsTerritories != null && !visibleStationsTerritories.isEmpty()) {
					for(String visibleStationTerritory:visibleStationsTerritories){
						strAssigned += visibleStationTerritory + ",";
					}
					
					if (!("".equals(strAssigned))) {
						strAssigned = strAssigned.substring(0, strAssigned.lastIndexOf(","));
					}
				}
				
				sb.append("arrData[" + count + "][72] = '" + strAssigned + "';");	
				sb.append("arrData[" + count + "][73] = '" + StringUtil.getNotNullString(agent.getHasCreditLimit()) + "';");
				sb.append("arrData[" + count + "][74] = '" + ((agent.getMaxAdultAllowed() == null) ? "" : agent.getMaxAdultAllowed()) + "';");
				
				if (agent.getModificationHandlingFees() != null && !agent.getModificationHandlingFees().isEmpty()) {
					
					int indx = 0;
					sb.append("arrData[" + count + "][75] = " + "new Array()" + ";");
					for(AgentHandlingFeeModification modFee : agent.getModificationHandlingFees()){
						sb.append("arrData[" + count + "][75]["+indx+"] = " + "new Array(5)" + ";");
						sb.append("arrData[" + count + "][75]["+indx+"][0] = '" + modFee.getAgentModificationHandlingFeeId() + "';");
						sb.append("arrData[" + count + "][75]["+indx+"][1] = '" + modFee.getAmount() + "';");
						sb.append("arrData[" + count + "][75]["+indx+"][2] = '" + modFee.getOperation() + "';");
						sb.append("arrData[" + count + "][75]["+indx+"][3] = '" + modFee.getStatus() + "';");
						sb.append("arrData[" + count + "][75]["+indx+"][4] = '" + modFee.getChargeBasisCode() + "';");
						indx++;
					}
					
				}else{
					sb.append("arrData[" + count + "][75] = " + "new Array()" + ";");
				}
				
				if(Agent.STATUS_HANDLING_FEE_MOD_Y.equals(agent.getHandlingFeeModificationEnabled())){
					sb.append("arrData[" + count + "][76] = '" + "Y" + "';");
				}else{
					sb.append("arrData[" + count + "][76] = '" + "N" + "';");
				}
				
				count++;
			}
		}
		return sb.toString();
	}

	/**
	 * Gets the History Grid for a Travel Agent
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the History Gird
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getCreditHistoryRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("***********inside TravelAgentHTMLGenerator.getCreditHistoryRowHtml()");
		String strHdnCode = request.getParameter("hdnCode");
		Collection<AgentCreditHistory> colCredit = null;
		Agent agent = null;
		colCredit = ModuleServiceLocator.getTravelAgentBD().getCreditHistoryForAgent(strHdnCode);
		agent = ModuleServiceLocator.getTravelAgentBD().getAgent(strHdnCode);
		return createCreditHistory(colCredit, agent, strHdnCode);
	}

	/**
	 * Create the Credit History Grid Data from a Coolection of History & Agent
	 * 
	 * @param colCredit
	 *            the Collection of Agent Credit History
	 * @param agent
	 *            the Agent
	 * @param agCode
	 *            the Agent Code
	 * @return String the Grid Rows
	 */
	private final String createCreditHistory(Collection<AgentCreditHistory> colCredit, Agent agent, String agCode) {

		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		DecimalFormat numberFormat = new DecimalFormat("#");
		SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
		StringBuffer chsb = new StringBuffer("var arrChildData = new Array();");

		if (colCredit != null && !colCredit.isEmpty()) {

			Iterator<AgentCreditHistory> iter = colCredit.iterator();
			AgentCreditHistory ACH = null;
			User user = null;
			int count = 0;
			String agentName = null;
			BigDecimal bankGuarantee = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal bankGuaranteeloc = AccelAeroCalculator.getDefaultBigDecimalZero();

			while (iter.hasNext()) {
				ACH = iter.next();

				chsb.append("arrChildData[" + count + "] = new Array();");
				if (count == 0) {
					if (agent != null) {
						agentName = agent.getAgentDisplayName();
						bankGuarantee = agent.getBankGuaranteeCashAdvance();
						bankGuaranteeloc = agent.getBankGuaranteeCashAdvanceLocal();
					}
				}

				String updDate = "";
				try {
					updDate = SDF.format(ACH.getUpdateDate());
				} catch (Exception e) {

				}

				chsb.append("arrChildData[" + count + "][1] = '" + updDate + "';");
				chsb.append("arrChildData[" + count + "][2] = '" + ((ACH.getCreatedBy() == null) ? "" : ACH.getCreatedBy())
						+ "';");
				try {
					user = ModuleServiceLocator.getSecurityBD().getUserBasicDetails(ACH.getCreatedBy());
					chsb.append("arrChildData[" + count + "][3] = '" + user.getDisplayName() + "';");

				} catch (Exception e) {
					chsb.append("arrChildData[" + count + "][3] = '';");
				}
				chsb.append("arrChildData["
						+ count
						+ "][4] = '"
						+ ((ACH.getCreditLimitamount() == null) ? decimalFormat.format(0) : decimalFormat.format(ACH
								.getCreditLimitamount())) + "';");
				// chsb.append("arrChildData[" + count + "][5] = '"
				// + ((ACH.getRemarks() == null) ? "" : ACH.getRemarks()) +
				// "';");
				// JIRA :AARESAA-3026 (Lalanthi - Date :02/10/2009)
				chsb.append("arrChildData[" + count + "][5] = '" + getAgentRemarksHTML(ACH.getRemarks()) + "';");

				chsb.append("arrChildData[" + count + "][6] = '" + agCode + "';");
				chsb.append("arrChildData[" + count + "][7] = '" + (agentName == null ? "" : agentName) + "';");
				chsb.append("arrChildData[" + count + "][8] = '"
						+ ((bankGuarantee == null) ? "0" : numberFormat.format(bankGuarantee)) + "';");
				// TODO change to local currency
				chsb.append("arrChildData["
						+ count
						+ "][9] = '"
						+ ((ACH.getCreditLimitamountLocal() == null) ? decimalFormat.format(0) : decimalFormat.format(ACH
								.getCreditLimitamountLocal())) + " "
						+ ((ACH.getSpecifiedCurrency() == null) ? "" : ACH.getSpecifiedCurrency()) + "';");

				chsb.append("arrChildData[" + count + "][10] = '"
						+ ((bankGuaranteeloc == null) ? "0" : numberFormat.format(bankGuaranteeloc)) + "';");
				count++;
			}

		}
		return chsb.toString();
	}

	/**
	 * JIRA :AARESAA-3026 (Lalanthi - Date :02/10/2009)
	 * 
	 * @param remark
	 * @return
	 */
	private String getAgentRemarksHTML(String remark) {
		String strRem = "";
		if (remark != null && remark.length() > 0) {
			// strRem = remark.replaceAll("\n", "<br>");
			StringTokenizer st = new StringTokenizer(remark, "\n");
			while (st.hasMoreTokens()) {
				strRem += st.nextToken();
			}
			strRem = strRem.replaceAll("\r", " ");
		}
		return strRem;
	}

	/**
	 * Gets the Credit Applicable or Not for Agent Type
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public final String getAgentTypeCreditApplicableRows(HttpServletRequest request) throws ModuleException {

		AgentType agentType = null;
		int count = 0;

		StringBuffer sb = new StringBuffer("var arrAgentCA = new Array();");
		Collection<AgentType> agentTypes = ModuleServiceLocator.getTravelAgentBD().getAgentTypes();
		boolean hasAddCreditPrivilege = hasPrivilege(request, WebConstants.PRIV_TA_MAINT_CREDIT_ADD);
		if (agentTypes != null) {
			Iterator<AgentType> itAgentTypes = agentTypes.iterator();
			while (itAgentTypes.hasNext()) {
				agentType = itAgentTypes.next();
				if (agentType != null && agentType.getAgentTypeCode() != null) {
					boolean creditApplicable = false;
					sb.append("arrAgentCA[" + count + "] = new Array();");
					sb.append("arrAgentCA[" + count + "][0] = '");
					sb.append(agentType.getAgentTypeCode() + "';");
					if (agentType.isCreditApplicable().equalsIgnoreCase("Y")) {
						if (hasAddCreditPrivilege) {
							creditApplicable = true;
						} else if (!agentType.getAgentTypeCode().equals(WebConstants.AGENT_TYPE_TRAVEL_AGENT)) {
							creditApplicable = true;
						} else {
							creditApplicable = false;
						}
					} else {
						creditApplicable = false;
					}

					sb.append("arrAgentCA[" + count + "][1] = '" + creditApplicable + "';");
					count++;
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Creates the Client Validations for Travel Agent & Credit Limit history
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.agent.form.select.agent.toedit", "SelectAgentToEdit");
			moduleErrs.setProperty("um.agent.form.select.agent.todelete", "SelectAgentToDelete");
			moduleErrs.setProperty("um.agent.form.agent.type.empty", "AgentTypeisempty");
			moduleErrs.setProperty("um.agent.form.station.empty", "StationCannotbeblank");
			moduleErrs.setProperty("um.agent.form.territory.empty", "TerritoryCannotbeblank");
			moduleErrs.setProperty("um.agent.form.name.empty", "NameisEmpty");
			moduleErrs.setProperty("um.agent.form.hmf.operation.added", "operationAdded");
			moduleErrs.setProperty("um.agent.form.hmf.amount.invalid", "invalidOpAmount");
			moduleErrs.setProperty("um.agent.form.hmf.operation.empty", "operationNotAdded");
			moduleErrs.setProperty("um.agent.form.payment.mode.empty", "PaymentModeisEmpty");
			moduleErrs.setProperty("um.agent.form.gsa.empty", "SelectGSA");
			moduleErrs.setProperty("um.agent.form.city.empty", "CityisEmpty");
			moduleErrs.setProperty("um.agent.form.address.empty", "AddressisEmpty");
			moduleErrs.setProperty("um.agent.form.acccode.empty", "AccountCodeisEmpty");
			moduleErrs.setProperty("um.agent.form.email.empty", "EMailisEmpty");
			moduleErrs.setProperty("um.agent.form.telephone.empty", "TelephoneNumberisEmpty");
			moduleErrs.setProperty("um.agent.form.advance.empty", "AdvanceisEmpty");
			moduleErrs.setProperty("um.agent.form.credit.limit.empty", "CreditLimitisEmpty");
			moduleErrs.setProperty("um.agent.form.select.agent.assign.user", "SelectAgenttoAssignUsers");
			moduleErrs.setProperty("um.agent.form.invalid.email", "InvalidEmail");
			moduleErrs.setProperty("um.agent.form.billing.email.empty", "BillingEmail");
			moduleErrs.setProperty("um.agent.form.invalid.billing.email", "InvalidBillingEmail");
			moduleErrs.setProperty("um.agent.form.invalid.same.billing.email", "SameBillingEmail");
			moduleErrs.setProperty("um.agent.form.station.notexist", "StationNotExist");
			moduleErrs.setProperty("um.agent.form.territory.notexist", "TerritoryNotExist");
			moduleErrs.setProperty("um.agent.form.travelagent.notexist", "TravelAgentNotExist");
			moduleErrs.setProperty("um.agent.form.gsa.notexist", "GSANotExist");
			moduleErrs.setProperty("um.agent.form.station.notbelongto.territory", "StationNotBelongToTerritory");
			moduleErrs.setProperty("um.agentCreditLimit.form.credit.limit.empty", "CreditLimitisEmpty");
			moduleErrs.setProperty("um.agentCreditLimit.form.reasonForChange.empty", "ReasonForChangeEmpty");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "DeleteConfirm");
			moduleErrs.setProperty("um.airadmin.accountCode.invalid", "accountCodeInvalid");
			moduleErrs.setProperty("um.airadmin.usernote.toolong", "notetoolong");
			moduleErrs.setProperty("um.airadmin.travelagent.invalidchange", "invalidchange");
			moduleErrs.setProperty("um.agent.form.iata.empty", "IATANumberEmpty");
			moduleErrs.setProperty("um.agent.form.iata.invalid", "InvalidIATANumber");
			moduleErrs.setProperty("um.airadmin.travelagent.change.currnull", "Amountblank");
			moduleErrs.setProperty("um.airadmin.travelagent.local.crlimitnull", "CreditLocalLimitisEmpty");
			moduleErrs.setProperty("um.airadmin.travelagent.local.advancenull", "LocalAdvanceisEmpty");
			moduleErrs.setProperty("um.agent.form.currency.empty", "currencyCannotbeblank");
			moduleErrs.setProperty("um.agentCreditLimit.form.reasonForChange.invalidchar", "invalidCharacter");
			moduleErrs.setProperty("um.agent.form.mapurl.invalid", "invalidMapURL");
			moduleErrs.setProperty("um.agent.form.currency.station.mismatch", "stationCurrencyDoNotMatch");
			moduleErrs.setProperty("um.agent.form.bank.guarantee.expiry.empty", "expiryDateEmpty");
			moduleErrs.setProperty("um.agent.form.bank.guarantee.expiry.invalid", "expiryDateInvalid");
			moduleErrs.setProperty("um.agent.form.bank.guarantee.expiry.past", "expiryDatePast");
			moduleErrs.setProperty("um.agent.form.inactive.date.past", "inactiveDatePast");
			moduleErrs.setProperty("um.agent.form.invalid.notifyEmails", "invalidListOfEmails");			
			moduleErrs.setProperty("um.agent.form.invalid.agent","invalidAgentName");
			moduleErrs.setProperty("um.agent.form.invalid.address1","invalidAddressLine1");
			moduleErrs.setProperty("um.agent.form.invalid.address2","invalidAddressLine2");
			moduleErrs.setProperty("um.agent.form.invalid.city","invalidCity");
			moduleErrs.setProperty("um.agent.form.invalid.state","invalidState");
			moduleErrs.setProperty("um.agent.form.invalid.postalcode","invalidPostalCode");
			moduleErrs.setProperty("um.agent.form.invalid.faxnumber","invalidFaxNumber");
			moduleErrs.setProperty("um.agent.form.invalid.mobile","invalidMobileNumber");
			moduleErrs.setProperty("um.agent.form.invalid.contacperson","invalidContactPerson");
			moduleErrs.setProperty("um.agent.form.invalid.license","invalidLicense");
			moduleErrs.setProperty("um.agent.form.invalid.note","invalidNote");
			moduleErrs.setProperty("um.agent.form.invalid.telephone","invalidTelephoneNumber");
			moduleErrs.setProperty("um.agent.form.invalid.iatanumber","invalidIataNumber");
			moduleErrs.setProperty("um.agent.form.invalid.creditlimit","invalidCrediLimit");
			moduleErrs.setProperty("um.agent.form.invalid.crditlimit.local","invalidCrediLimit");
			moduleErrs.setProperty("um.agent.form.invalid.bank.guarantee","invalidBankGuarantee");
			moduleErrs.setProperty("um.agent.form.invalid.bank.guarantee.local","invalidBankGuaranteeLocal");
			moduleErrs.setProperty("um.agent.form.invalid.bsp.bank.guarantee","invalidBspGuarantee");
			moduleErrs.setProperty("um.agent.form.invalid.bsp.bank.guarantee.local","invalidBspGuaranteeLocal");
			moduleErrs.setProperty("um.agent.form.invalid.oneway.amount","invalidOneWayAmount");
			moduleErrs.setProperty("um.agent.form.invalid.return.amount","invalidReturnAmount");
			moduleErrs.setProperty("um.agent.form.invalid.minimum.amount","invalidMinimumAmount");
			moduleErrs.setProperty("um.agent.form.invalid.maximu.amount","invalidMaxAmount");
			moduleErrs.setProperty("um.agent.form.invalid.inactive.date","invalidInactiveDate");
			moduleErrs.setProperty("um.agent.form.invalid.inactive.time","invalidInactiveTime");
			moduleErrs.setProperty("cc.user.form.agent.credit.basis.invalid","invalidCreditBasis");
			moduleErrs.setProperty("cc.user.form.agent.credit.basis.invalid.for.selected.type", "invalidCreditBasisForType");
			moduleErrs.setProperty("cc.user.form.agent.credit.basis.shared.select.reporting", "selectReportingCreditSharing");
			moduleErrs.setProperty("um.agent.form.select.reporting.agent", "selectReportTo");
			moduleErrs.setProperty("um.report.arrivalDepature.same", "depatureArriavlSame");
			moduleErrs.setProperty("um.report.ond.departure.required", "depatureRqrd");
			moduleErrs.setProperty("um.report.departure.inactive", "departureInactive");
			moduleErrs.setProperty("um.report.ond.arrival.required", "arrivalRqrd");
			moduleErrs.setProperty("um.report.arrival.inactive", "arrivalInactive");
			moduleErrs.setProperty("um.report.arrivalDepature.same", "depatureArriavlSame");
			moduleErrs.setProperty("um.report.arrivalVia.same", "arriavlViaSame");
			moduleErrs.setProperty("um.report.depatureVia.same", "depatureViaSame");
			moduleErrs.setProperty("um.report.via.sequence", "vianotinSequence");
			moduleErrs.setProperty("um.report.via.inactine", "viaInactive");
			moduleErrs.setProperty("um.report.ond.required", "ondRequired");
			moduleErrs.setProperty("um.charges.via.equal", "viaEqual");			
			
			
			
			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	/**
	 * Returns the GSA Name for a given Agent
	 * 
	 * @param gsas
	 *            the GSAs Collection
	 * @param agent
	 *            the agent to find the GSA
	 * @return String the GSA name
	 */
	private final String getGSAName(Collection<Agent> gsas, Agent agent) {
		String strGsa = "";
		Iterator<Agent> gsaite = null;
		if (gsas != null && agent != null) {
			for (Agent gsa : gsas) {
				if (gsa != null && (gsa.getAgentCode().equals(agent.getGsaCode())) && (gsa.getStatus().equals("ACT"))) {
					strGsa = gsa.getAgentName();
					return strGsa;
				}
			}
		}
		return strGsa;
	}

	/**
	 * Returns the Country Name for a Given Station Code
	 * 
	 * @param lst
	 *            the List Having the couty names
	 * @param stationCode
	 *            the Station Code
	 * @return String the Country name
	 */
	private final String getCountryName(List<String[]> lst, String stationCode) {
		String countryName = "";
		String[] cuntryarray;
		if (lst != null && stationCode != null) {
			for (int len = 0; len < lst.size(); len++) {
				cuntryarray = lst.get(len);
				if (cuntryarray[0].equals(stationCode))
					return cuntryarray[1];
			}
		}
		return countryName;
	}

	private String getCreditLimitInSpecifiedCurrency(Agent agent) {
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		StringBuffer currencySb = new StringBuffer();
		if ("L".equals(agent.getCurrencySpecifiedIn())) {
			currencySb.append(decimalFormat.format(agent.getCreditLimitLocal() != null ? agent.getCreditLimitLocal() : 0));
			currencySb.append(" ");
			currencySb.append(agent.getCurrencyCode());
		} else {
			currencySb.append(decimalFormat.format(agent.getCreditLimit() != null ? agent.getCreditLimit() : 0));
			currencySb.append(" ");
			currencySb.append(AppSysParamsUtil.getBaseCurrency());

		}
		return currencySb.toString();
	}

	/**
	 * Sets the Travel Agent Type List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setTravelAgentListHtml(HttpServletRequest request) throws ModuleException {

		StringBuilder sb = new StringBuilder();
		String agentType = ((UserPrincipal) request.getUserPrincipal()).getAgentTypeCode();
		if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
			// PRIV check - ta.maint.add.subagent
			String subAgentList = SelectListGenerator.createSubAgentTypes(agentType);

			String carrierAgent = AppSysParamsUtil.getCarrierAgent();
			if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
				// including same agent type & all sub agent types allowed
				if (carrierAgent.equals(agentType)) {
					sb.append("<option value='" + carrierAgent + "'>" + carrierAgent + "</option>");
				}
				sb.append(subAgentList);
			} else if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_SUB_AGENTS)) {
				// all sub agent types allowed
				sb.append(subAgentList);
			} else if (!AgentType.STA.equals(agentType) && hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_TRAVEL_AGENT)) {
				// only TA/STA can be added
				if (!AgentType.TA.equals(agentType)) {
					sb.append("<option value='TA'>TA</option>");
				}
				sb.append("<option value='STA'>STA</option>");
			}
		} else if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_ANY)) {
			sb.append(SelectListGenerator.createAgentTypeList_SG());
		} else if (hasPrivilege(request, WebConstants.PRIV_TA_MAINT_ADD_GSA)) {
			sb.append("<option value='TA'>TA</option>");
		}

		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, sb.toString());
	}
	
	
	public static void setReportingAgentTypeLists(HttpServletRequest request) throws ModuleException {
		String strAllReportingAgentList = SelectListGenerator.createAllSubAgentTypes();
		request.setAttribute(WebConstants.REQ_ALL_REP_AGENT_LISTS, strAllReportingAgentList);
	}
}