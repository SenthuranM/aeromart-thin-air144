package com.isa.thinair.xbe.core.web.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.web.auth.AuthorizationManager;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class AuthorizationFilter extends BasicFilter {
	private static Log log = LogFactory.getLog(AuthorizationFilter.class);

	private static final List<String> ajaxActions = setAjaxActions();

	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain) throws ServletException, IOException {

		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpSession session = request.getSession(false);
		User user = (User) session.getAttribute(WebConstants.SES_CURRENT_USER);
		
		String userLanguage = (String) session.getAttribute(WebConstants.REQ_LANGUAGE);
		// FIXME map the privileges correctly in property file

		// Get relevant URI.
		String url = request.getServletPath();

		// if (log.isDebugEnabled()) log.debug("url: " + url);
		// Invoke AuthorizationManager method to see if user can
		// access resource.
		boolean authorized = AuthorizationManager.isUserAuthorized(user, url);

		if (authorized) {
			chain.doFilter(arg0, arg1);
		} else {
			String msg = XBEConfig.getServerMessage(WebConstants.KEY_AUTHORIZATION_FAILED, userLanguage, null);
			msg = StringUtils.replace(msg, "#1", url);
			log.error("Authorization failed:" + user.getUserId() + ":" + url);

			if (ajaxActions.contains(url)) {
				BasicRH.sendMessage(arg1, WebConstants.MSG_ERROR, errorPage);
			} else {
				JavaScriptGenerator.setServerError(request, msg, "", "");
				BasicRH.forward(arg0, arg1, errorPage);
			}
			return;

		}
	}

	private static final List<String> setAjaxActions() {
		List<String> ajaxActions = new ArrayList<String>();
		// TODO need to test authorization.
		return ajaxActions;
	}

}
