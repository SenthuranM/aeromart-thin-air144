package com.isa.thinair.xbe.core.web.generator.alerts;

import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by hasika on 6/11/18.
 */
public class FlightPnrViewNotificationHTMLGenerator {

    private static Log log = LogFactory.getLog(FlightPnrNotificationHTMLGenerator.class);

    private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

    private static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";

    private static final String PARAM_PNR_LOAD_CRITERIA = "hdnLoadPnrCriteria";

    private static final String PARAM_PNR_VIEW_CRITERIA = "hdnViewPnrCriteria";

    private static final String PARAM_FLIGHT_ID = "hdnFlightId";

    private static final String PARAM_FLIGHT_PNR_DTO = "flightPnrDetailDTO";

	private static final String PARAM_DEP_DATE = "hdnDepDate";

    /**
     * returns the PNR detail data
     *
     * @return String
     */
    public final String getPnrRowHtml(HttpServletRequest request) throws ModuleException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
        Iterator it = null;
        String pnrSearchInfo = null;
        String[] arrPnrSearchInfo = null;

        String flightNo = null;
        String depDate = null;
        String pnr = null;
        String searchMobileOption = null;
        String searchEmailOption = null;
        String searchLandPhoneOption = null;
        String notificationCode = null;
        String mobileNumberPattern = null;
        String strAlert = null;
        Date fltDepDate = null;
        String prefLanguage = null;
        String langOpt = null;
        String anciReprotectStatus = null;
        String sortBy = null;
        String sortByOrder = null;
        String alertFromDateString = null;
        String alertToDateString = null;
        Date alertFromDate = null;
        Date alertToDate = null;
        int noOfAdults = -1;
        int noOfChildren = -1;
        int noOfInfants = -1;
        String segmentFrom = null;
        String segmentTo = null;

        StringBuffer sbData = new StringBuffer();
        String arrName = "arrData";

        int count = 0;

        sbData.append("var " + arrName + " = new Array();");
        pnrSearchInfo = request.getParameter(PARAM_PNR_LOAD_CRITERIA);

        if (pnrSearchInfo != null) {
            arrPnrSearchInfo = pnrSearchInfo.split(",");
            if ((arrPnrSearchInfo != null) && (arrPnrSearchInfo.length > 0)) {
                flightNo = (arrPnrSearchInfo[1].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[1].trim().toUpperCase());
                depDate = (arrPnrSearchInfo[2].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[2].trim();
                try {
                    if (depDate != null)
                        fltDepDate = CalendarUtil.getParsedTime(depDate, DATE_FORMAT_DDMMYYYY);
                } catch (ParseException e) {
                    log.error("getPnrRowHtml() Invalid Date - Parse Exception", e);
                }
                pnr = (arrPnrSearchInfo[3].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[3].trim());
                searchMobileOption = (arrPnrSearchInfo[4].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[4].trim()
                        .toUpperCase());
                searchEmailOption = (arrPnrSearchInfo[5].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[5].trim()
                        .toUpperCase());
                searchLandPhoneOption = (arrPnrSearchInfo[6].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[6].trim()
                        .toUpperCase());
                notificationCode = (arrPnrSearchInfo[8].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[8].trim()
                        .toUpperCase());
                mobileNumberPattern = (arrPnrSearchInfo[7].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[7].trim());
                strAlert = (arrPnrSearchInfo[9] != null) ? arrPnrSearchInfo[9].trim() : null;

                prefLanguage = (arrPnrSearchInfo[10] != null) ? arrPnrSearchInfo[10].trim() : "";
                langOpt = (arrPnrSearchInfo[11] != null) ? arrPnrSearchInfo[11].trim() : "";

                prefLanguage = (arrPnrSearchInfo[10] != null) ? arrPnrSearchInfo[10].trim() : "";
                langOpt = (arrPnrSearchInfo[11] != null) ? arrPnrSearchInfo[11].trim() : "";

                anciReprotectStatus = (arrPnrSearchInfo[12] != null) ? arrPnrSearchInfo[12].trim() : "";

                sortBy = (arrPnrSearchInfo[18] != null) ? arrPnrSearchInfo[18].trim() : "";
                sortByOrder = (arrPnrSearchInfo[19] != null) ? arrPnrSearchInfo[19].trim() : "";

                noOfAdults = (arrPnrSearchInfo[13].trim().equals("")) ? noOfAdults : Integer.valueOf(arrPnrSearchInfo[13].trim());
                noOfChildren = (arrPnrSearchInfo[14].trim().equals("")) ? noOfChildren : Integer.valueOf(arrPnrSearchInfo[14].trim());
                noOfInfants = (arrPnrSearchInfo[15].trim().equals("")) ? noOfInfants : Integer.valueOf(arrPnrSearchInfo[15].trim());

                alertFromDateString = (arrPnrSearchInfo[16].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[16].trim();
                try {
                    if (alertFromDateString != null)
                        alertFromDate = CalendarUtil.getParsedTime(alertFromDateString, DATE_FORMAT_DDMMYYYY);
                } catch (ParseException e) {
                    log.error("getPnrRowHtml() Invalid Alert Created From Date - Parse Exception", e);
                }

                alertToDateString = (arrPnrSearchInfo[17].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[17].trim();
                try {
                    if (alertToDateString != null)
                        alertToDate = CalendarUtil.getParsedTime(alertToDateString, DATE_FORMAT_DDMMYYYY);
                } catch (ParseException e) {
                    log.error("getPnrRowHtml() Invalid Alert Created To Date - Parse Exception", e);
                }

                segmentFrom = (arrPnrSearchInfo[20].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[20].trim());
                segmentTo = (arrPnrSearchInfo[21].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[21].trim());

                if (mobileNumberPattern != null) {
                    if (mobileNumberPattern.indexOf(" ") != -1)
                        mobileNumberPattern = mobileNumberPattern.replaceAll(" ", "");
                    if (mobileNumberPattern.indexOf("-") != -1)
                        mobileNumberPattern = mobileNumberPattern.replaceAll("-", "");
                }


                FlightPnrNotificationDTO searchCriteria = new FlightPnrNotificationDTO();
                searchCriteria.setFlightNumber(flightNo);
                searchCriteria.setDepDate(fltDepDate);
                searchCriteria.setPnr(pnr);
                searchCriteria.setSearchMobileOption(searchMobileOption);
                searchCriteria.setSearchEmailOption(searchEmailOption);
                searchCriteria.setSearchLandPhoneOption(searchLandPhoneOption);
                searchCriteria.setNotificationCode(notificationCode);
                searchCriteria.setMobileNumberPattern(mobileNumberPattern);
                searchCriteria.setAlert(strAlert);
                searchCriteria.setPreferredLanguage(prefLanguage);
                searchCriteria.setLanguageOption(langOpt);
                searchCriteria.setSearchReprotectAnciOption(anciReprotectStatus);
                searchCriteria.setSortBy(sortBy);
                searchCriteria.setSortByOrder(sortByOrder);
                searchCriteria.setNoOfAdults(noOfAdults);
                searchCriteria.setNoOfChildren(noOfChildren);
                searchCriteria.setNoOfInfants(noOfInfants);
                searchCriteria.setAlertCreatedFromDate(alertFromDate);
                searchCriteria.setAlertCreatedToDate(alertToDate);
                searchCriteria.setSegmentFrom(segmentFrom);
                searchCriteria.setSegmentTo(segmentTo);


                Page pnrDetailsPage = ModuleServiceLocator.getAlertingBD().retrieveFlightPnrDetailsForSearchCriteria(
                        searchCriteria);
                Collection pnrDetails = pnrDetailsPage.getPageData();

                HashMap<Integer, FlightPnrNotificationDTO> hmPnrDetails = new HashMap<Integer, FlightPnrNotificationDTO>();
                int flightId = -1;

                sbData.append("var " + arrName + " = new Array();");

                sbData.append("var totNoOfPnr = " + pnrDetailsPage.getTotalNoOfRecords() + ";");

                if ((pnrDetails != null) && (!pnrDetails.isEmpty())) {
                    it = pnrDetails.iterator();
                    while (it.hasNext()) {
                        FlightPnrNotificationDTO pnrDetailDTO = (FlightPnrNotificationDTO) it.next();
                        if (pnrDetailDTO != null) {
                            sbData.append(arrName + "[" + count + "] = new Array();");
                            sbData.append(arrName + "[" + count + "][1] = '"
                                    + ((pnrDetailDTO.getFlightNumber() == null) ? "" : pnrDetailDTO.getFlightNumber()) + "';");
                            if (pnrDetailDTO.getDepDate() == null) {
                                sbData.append(arrName + "[" + count + "][2] = '" + "" + "';");
                            } else {
                                sbData.append(arrName + "[" + count + "][2] = '" + dateFormat.format(pnrDetailDTO.getDepDate())
                                        + "';");
                            }
                            if (pnrDetailDTO.getAlert().equals("1"))
                                sbData.append(arrName + "[" + count + "][3] = '"
                                        + ((pnrDetailDTO.getPnr() == null) ? "" : pnrDetailDTO.getPnr())
                                        + "<font class=\"Mandatory\">&nbsp;*</font>';");
                            else
                                sbData.append(arrName + "[" + count + "][3] = '"
                                        + ((pnrDetailDTO.getPnr() == null) ? "" : pnrDetailDTO.getPnr()) + "';");
                            if (pnr == null)
                                sbData.append(arrName + "[" + count + "][4] = '"
                                        + ((pnrDetailDTO.getRoute() == null) ? "" : pnrDetailDTO.getRoute()) + "';");
                            else {
                                sbData.append(arrName
                                        + "["
                                        + count
                                        + "][4] = '"
                                        + ((pnrDetailDTO.getRoute() == null) ? "" : pnrDetailDTO.getRoute())
                                        + " ["
                                        + ((pnrDetailDTO.getFlightNumber() == null) ? "" : pnrDetailDTO.getFlightNumber())
                                        + "-"
                                        + ((pnrDetailDTO.getDepDate() == null) ? ""
                                        : dateFormat.format(pnrDetailDTO.getDepDate())) + "]" + "';");
                            }

                            sbData.append(arrName + "[" + count + "][5] = '" + getMobNumber(pnrDetailDTO.getMobile()) + "';");
                            sbData.append(arrName + "[" + count + "][6] = '"
                                    + ((pnrDetailDTO.getEmail() == null) ? "" : pnrDetailDTO.getEmail()) + "';");
                            sbData.append(arrName + "[" + count + "][7] = '"
                                    + ((pnrDetailDTO.getLandPhone() == null) ? "" : pnrDetailDTO.getLandPhone()) + "';");
                            sbData.append(arrName + "[" + count + "][8] = '"
                                    + ((pnrDetailDTO.getFlightId() <= 0) ? "" : pnrDetailDTO.getFlightId()) + "';");
                            sbData.append(arrName + "[" + count + "][9] = '"
                                    + ((pnrDetailDTO.getPnrSegmentId() <= 0) ? "" : pnrDetailDTO.getPnrSegmentId()) + "';");
                            sbData.append(arrName + "[" + count + "][10] = new Array();");
                            if (pnrDetailDTO.getNotifications() != null) {
                                ArrayList aList = pnrDetailDTO.getNotifications();
                                for (int k = 0; k < aList.size(); k++) {
                                    FlightPnrNotification fltNotification = (FlightPnrNotification) aList.get(k);
                                    sbData.append(arrName + "[" + count + "][10][" + k + "] = new Array();");
                                    sbData.append(arrName
                                            + "["
                                            + count
                                            + "][10]["
                                            + k
                                            + "][1] = '"
                                            + ((fltNotification.getFlightNotificationId() <= 0) ? "" : fltNotification
                                            .getFlightNotificationId()) + "';");
                                    sbData.append(arrName
                                            + "["
                                            + count
                                            + "][10]["
                                            + k
                                            + "][2] = '"
                                            + ((fltNotification.getNotificationChannel() == null) ? "" : fltNotification
                                            .getNotificationChannel()) + "';");
                                }
                            }
                            sbData.append(arrName + "[" + count + "][11]='" + pnrDetailDTO.getTotalPax() + "';");
                            sbData.append(arrName + "[" + count + "][12]='" + pnrDetailDTO.getNoOfAdults() + "';");
                            sbData.append(arrName + "[" + count + "][13]='" + pnrDetailDTO.getNoOfChildren() + "';");
                            sbData.append(arrName + "[" + count + "][14]='" + pnrDetailDTO.getNoOfInfants() + "';");
                            if (pnrDetailDTO.getAlertCreatedActualDate() == null) {
                                sbData.append(arrName + "[" + count + "][15]='';");
                            } else {
                                sbData.append(arrName + "[" + count + "][15]='" + dateFormat.format(pnrDetailDTO.getAlertCreatedActualDate()) + "';");
                            }
                            if (pnrDetailDTO.getInBoundFlightNo() != null) {
                                sbData.append(arrName
                                        + "["
                                        + count
                                        + "][16]='"
                                        + pnrDetailDTO.getInBoundFlightNo()
                                        + ":"
                                        + ((pnrDetailDTO.getInBoundDepartureStation() == null) ? "" : pnrDetailDTO.getInBoundDepartureStation())
                                        + ":"
                                        + ((pnrDetailDTO.getInBoundDepartureDate() == null) ? "" : pnrDetailDTO.getInBoundDepartureDate()) + "';");
                            } else {
                                sbData.append(arrName + "[" + count + "][16]='';");
                            }
                            count++;

                            hmPnrDetails.put(new Integer(pnrDetailDTO.getPnrSegmentId()), pnrDetailDTO);
                            flightId = pnrDetailDTO.getFlightId();

                        }
                    }
                }
                // request.getSession().setAttribute("flightPnrDetailDTO",hmPnrDetails);
                request.setAttribute(PARAM_FLIGHT_PNR_DTO, hmPnrDetails);
                request.setAttribute(PARAM_FLIGHT_ID, String.valueOf(flightId));

            }

        }
        return sbData.toString();
    }

    /**
     * returns the Notification Summary data
     *
     * @return String
     */
    public final String getNotificationSummaryRowHtml(HttpServletRequest request) throws ModuleException {

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);

        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATE_TIME_FORMAT);

        Iterator it = null;

        String pnrSearchInfo = null;

        String[] arrPnrSearchInfo = null;

        String flightNo = null;

        String pnr = null;

        String strFromDate = null;

        String strToDate = null;

        Date fromDate = null;

        Date toDate = null;

        StringBuffer sbData = new StringBuffer();

        String arrName = "arrSummaryData";

        int count = 0;

        pnrSearchInfo = request.getParameter(PARAM_PNR_VIEW_CRITERIA);
        if (pnrSearchInfo != null) {
            arrPnrSearchInfo = pnrSearchInfo.split(",");
            if ((arrPnrSearchInfo != null) && (arrPnrSearchInfo.length > 0)) {
                flightNo = (arrPnrSearchInfo[1].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[1].trim().toUpperCase());
                strFromDate = (arrPnrSearchInfo[3].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[3].trim();
                strToDate = (arrPnrSearchInfo[4].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[4].trim();
                try {
                    if (strFromDate != null)
                        fromDate = CalendarUtil.getParsedTime(strFromDate, DATE_FORMAT_DDMMYYYY);
                    if (strToDate != null)
                        toDate = CalendarUtil.getParsedTime(strToDate, DATE_FORMAT_DDMMYYYY);

                } catch (ParseException e) {
                    log.error("getPnrRowHtml() Invalid Date - Parse Exception", e);
                }
                pnr = (arrPnrSearchInfo[2].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[2].trim());

                FlightPnrNotificationDTO searchCriteria = new FlightPnrNotificationDTO();
                searchCriteria.setFlightNumber(flightNo);
                searchCriteria.setPnr(pnr);
                searchCriteria.setDateFrom(fromDate);
                searchCriteria.setDateTo(toDate);

                Page pnrDetailsPage = ModuleServiceLocator.getAlertingBD().retrieveFlightPnrDetailsForViewSearchCriteria(
                        searchCriteria);
                Collection pnrDetails = pnrDetailsPage.getPageData();

                List<Integer> flightId = new ArrayList<>(1);
                String strPnr = searchCriteria.getPnr();

                if (flightNo != null) {
                    flightId = ModuleServiceLocator.getAlertingBD().getFlightIDList(flightNo, fromDate, toDate);
                }

                sbData.append("var " + arrName + " = new Array();");

                if ((pnrDetails != null) && (!pnrDetails.isEmpty())) {
                    it = pnrDetails.iterator();
                    while (it.hasNext()) {
                        FlightPnrNotificationDTO pnrDetailDTO = (FlightPnrNotificationDTO) it.next();
                        if (pnrDetailDTO != null) {
                            sbData.append(arrName + "[" + count + "] = new Array();");
                            sbData.append(arrName + "[" + count + "][0] = " + count + ";");
                            sbData.append(arrName + "[" + count + "][1] = '"
                                    + ((pnrDetailDTO.getFlightNumber() == null) ? "" : pnrDetailDTO.getFlightNumber()) + "';");
                            if (pnrDetailDTO.getDepDate() == null) {
                                sbData.append(arrName + "[" + count + "][2] = '" + "" + "';");
                            } else {
                                sbData.append(arrName + "[" + count + "][2] = '" + dateFormat.format(pnrDetailDTO.getDepDate())
                                        + "';");
                            }
                            sbData.append(arrName + "[" + count + "][3] = '" + ((strPnr == null) ? "" : strPnr) + "';");
                            if (pnrDetailDTO.getNotificationtDate() == null) {
                                sbData.append(arrName + "[" + count + "][4] = '" + "" + "';");
                            } else {
                                sbData.append(arrName + "[" + count + "][4] = '"
                                        + dateTimeFormat.format(pnrDetailDTO.getNotificationtDate()) + "';");
                            }
                            sbData.append(arrName + "[" + count + "][5] = '"
                                    + ((pnrDetailDTO.getNotificationCode() == null) ? "" : pnrDetailDTO.getNotificationCode())
                                    + "';");
                            String msg = StringUtil.escapeStr(pnrDetailDTO.getNotificationMsg(), "'", "\\\\'");
                            msg = StringUtil.escapeStr(msg, "\n|\r|\r\n", " ");
                            sbData.append(arrName + "[" + count + "][6] = '"
                                    + ((pnrDetailDTO.getNotificationMsg() == null) ? "" : msg) + "';");
                            sbData.append(arrName + "[" + count + "][7] = '" + pnrDetailDTO.getTotalSms() + "';");
                            sbData.append(arrName + "[" + count + "][8] = '" + pnrDetailDTO.getTotalSmsDelivered() + "';");
                            sbData.append(arrName + "[" + count + "][9] = '" + pnrDetailDTO.getTotalSmsFailed() + "';");
                            sbData.append(arrName + "[" + count + "][10] = '" + pnrDetailDTO.getTotalEmail() + "';");
                            sbData.append(arrName + "[" + count + "][11] = '" + pnrDetailDTO.getTotalEmailDelivered() + "';");
                            sbData.append(arrName + "[" + count + "][12] = '" + pnrDetailDTO.getTotalEmailFailed() + "';");
                            sbData.append(arrName + "[" + count + "][13] = '" + pnrDetailDTO.getFlightNotificationId() + "';");
                            sbData.append(arrName + "[" + count + "][14] = '" + pnrDetailDTO.getNotificationChannel()
                                    .stream().map(Object::toString).collect(Collectors.joining(",")) + "';");

                            count++;

                        }
                    }
                }
                request.setAttribute(PARAM_FLIGHT_ID, flightId.stream().map(Object::toString)
                        .collect(Collectors.joining(", ")));

            }

        }
        return sbData.toString();
    }

    /**
     * returns the Notification Details data
     *
     * @return String
     */
    public final String getNotificationDetailRowHtml(HttpServletRequest request) throws ModuleException {

        HashMap<String, Integer> countyCount = new HashMap<>(1);

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
        String strParam = request.getParameter("hdnFlightNotificationId");
        if (strParam == null || "".equalsIgnoreCase(strParam))
            return "";

        String[] arrParams = strParam.split(",");
        String hdnFlightNotificationId = (arrParams[0] != null && !"".equals(arrParams[0])) ? arrParams[0] : "-1";
        String hdnNotificationDate = (arrParams[1].trim().equalsIgnoreCase("")) ? null : arrParams[1].trim();
        String hdnPnr = StringUtil.isNullOrEmpty(request.getParameter("hdnPnr")) ? "" : request.getParameter("hdnPnr");
        Date notifyDate = null;

        try {
            if (hdnNotificationDate != null)
                notifyDate = CalendarUtil.getParsedTime(hdnNotificationDate, DATE_TIME_FORMAT);

        } catch (ParseException e) {
            log.error("getNotificationDetailRowHtml() Invalid Date - Parse Exception", e);
        }

        Page pnrDetailsPage = ModuleServiceLocator.getAlertingBD().retrieveDetailedNotificationList(
                Integer.parseInt(hdnFlightNotificationId), notifyDate, hdnPnr);
        Collection pnrDetails = pnrDetailsPage.getPageData();

        Iterator it = null;
        StringBuffer sbData = new StringBuffer();
        String arrName = "arrDetailData";
        String smsDeliveryStatus = "";
        String emailDeliveryStatus = "";

        int count = 0;

        sbData.append("var " + arrName + " = new Array();");

        if ((pnrDetails != null) && (!pnrDetails.isEmpty())) {

            it = pnrDetails.iterator();
            while (it.hasNext()) {
                FlightPnrNotificationDTO pnrDetailDTO = (FlightPnrNotificationDTO) it.next();
                if (pnrDetailDTO != null) {
                    getOutBoundConnectionList(pnrDetailDTO);

					String ph = StringUtil.getNotNullString(pnrDetailDTO.getMobile());
					if (ph.split("-").length > 0) {
						CountryDetailsDTO country = ModuleServiceLocator.getLocationServiceBD().getCountryByPhoneCode(ph.split("-")[0]);
						if (country != null) {
	                        String key = country.getCountryName() + "-" + country.getPhoneCode();
	                        if (countyCount.containsKey(key)) {
	                            countyCount.put(key, countyCount.get(key) + 1);
	                        } else {
	                            countyCount.put(key, 1);
	                        }
	                    }
					}                   

                    HashMap hmap = pnrDetailDTO.getDeliveryStatus();
                    smsDeliveryStatus = (String) hmap.get("SMS");
                    emailDeliveryStatus = (String) hmap.get("EMAIL");

                    sbData.append(arrName + "[" + count + "] = new Array();");
                    if (pnrDetailDTO.getNotificationtDate() == null) {
                        sbData.append(arrName + "[" + count + "][1] = '" + "" + "';");
                    } else {
                        sbData.append(arrName + "[" + count + "][1] = '" + dateFormat.format(pnrDetailDTO.getNotificationtDate())
                                + "';");
                    }
                    sbData.append(arrName + "[" + count + "][2] = '"
                            + ((pnrDetailDTO.getNotificationCode() == null) ? "" : pnrDetailDTO.getNotificationCode()) + "';");
                    sbData.append(arrName + "[" + count + "][3] = '"
                            + ((pnrDetailDTO.getPnr() == null) ? "" : pnrDetailDTO.getPnr()) + "';");
					sbData.append(arrName + "[" + count + "][4] = '" + (getMobNumber(pnrDetailDTO.getMobile())) + "';");
                    sbData.append(arrName
                            + "["
                            + count
                            + "][5] = '"
                            + ((smsDeliveryStatus == null) ? "" : (smsDeliveryStatus.equals("D") ? "Delivered"
                            : (smsDeliveryStatus.equals("F") ? "Failed" : "Sent"))) + "';");
                    sbData.append(arrName + "[" + count + "][6] = '"
                            + ((pnrDetailDTO.getEmail() == null) ? "" : pnrDetailDTO.getEmail()) + "';");
                    sbData.append(arrName
                            + "["
                            + count
                            + "][7] = '"
                            + ((emailDeliveryStatus == null) ? "" : (emailDeliveryStatus.equals("D") ? "Delivered"
                            : (emailDeliveryStatus.equals("F") ? "Failed" : "Sent"))) + "';");
					sbData.append(arrName + "[" + count + "][8] = '" + (getMobNumber(pnrDetailDTO.getLandPhone())) + "';");
                    sbData.append(arrName + "[" + count + "][9] = '"
                            + ((pnrDetailDTO.getRoute() == null) ? "" : pnrDetailDTO.getRoute()) + "';");
                    sbData.append(arrName + "[" + count + "][10] = '" + pnrDetailDTO.getFlightNotificationId() + "';");

                    if (pnrDetailDTO.getInBoundFlightNo() != null) {
                        sbData.append(arrName + "[" + count + "][11] = '" + pnrDetailDTO.getInBoundFlightNo() + "';");
                        sbData.append(arrName + "[" + count + "][12] = '" + pnrDetailDTO.getInBoundDepartureDate() + "';");
                    } else {
                        sbData.append(arrName + "[" + count + "][11] = '" + "" + "';");
                        sbData.append(arrName + "[" + count + "][12] = '" + "" + "';");
                    }
					sbData.append(arrName + "[" + count + "][13] = '" + pnrDetailDTO.getNoOfAdults() + "';");

                    sbData.append(arrName + "[" + count + "][15] = '" + getCountryCountList(countyCount) + "';");

                    if (pnrDetailDTO.getOutBoundFlightNo() != null) {
                        sbData.append(arrName + "[" + count + "][16] = '" + pnrDetailDTO.getOutBoundFlightNo() + "';");
                        sbData.append(arrName + "[" + count + "][17] = '" + pnrDetailDTO.getOutBoundDepartureDate() + "';");
                    } else {
                        sbData.append(arrName + "[" + count + "][16] = '" + "" + "';");
                        sbData.append(arrName + "[" + count + "][17] = '" + "" + "';");
                    }
                    sbData.append(arrName + "[" + count + "][18] = '" + pnrDetailDTO.getFlightId() + "';");
                    sbData.append(arrName + "[" + count + "][19] = '" + pnrDetailDTO.getPnrSegmentId() + "';");
                    sbData.append(arrName + "[" + count + "][20] = '" + pnrDetailDTO.getDepDate() + "';");
                    sbData.append(arrName + "[" + count + "][21] = '" + pnrDetailDTO.getNoOfChildren() + "';");
                    sbData.append(arrName + "[" + count + "][22] = '" + pnrDetailDTO.getNoOfInfants() + "';");
					sbData.append(arrName + "[" + count + "][23] = '" + ((getMobNumber(pnrDetailDTO.getMobile()).equals("")) ? ""
							: getMobNumber(pnrDetailDTO.getMobile()).replaceAll("-", "")) + "';");
					sbData.append(arrName + "[" + count + "][24] = '" + ((getMobNumber(pnrDetailDTO.getLandPhone()).equals("")) ? ""
									: getMobNumber(pnrDetailDTO.getLandPhone()).replaceAll("-", "")) + "';");

					if (pnrDetailDTO.getDepDate() == null) {
						sbData.append(arrName + "[" + count + "][25] = '" + "" + "';");
					} else {
						sbData.append(arrName + "[" + count + "][25] = '" + dateFormat.format(pnrDetailDTO.getDepDate()) + "';");
					}

                    count++;

                }
            }
        }

        return sbData.toString();
    }

    public void updateReservationContactData(HttpServletRequest request) {
        String pnr = request.getParameter("pnr");
        String land = request.getParameter("land");
        String mobile = request.getParameter("mobile");
        String email = request.getParameter("email");
        Reservation reservation = ModuleServiceLocator.getReservationBD().getReservation(pnr, false);
        reservation.getContactInfo().setPhoneNo(land);
        reservation.getContactInfo().setMobileNo(mobile);
        reservation.getContactInfo().setEmail(email);
        reservation.getContactInfo().setPnr(pnr);
        ModuleServiceLocator.getReservationBD().updateReservationContactData(reservation.getContactInfo());
    }

	public String getMessageListRowHtml(HttpServletRequest request) throws ModuleException {
		Iterator it = null;
		String depDate = null;
		StringBuffer sbData = new StringBuffer();
		String arrName = "arrMsgListData";

		Date fltDepDate = null;
		int count = 0;

		sbData.append("var " + arrName + " = new Array();");
		depDate = request.getParameter(PARAM_DEP_DATE);

		if (depDate != null) {
			try {
				if (depDate != null)
					fltDepDate = CalendarUtil.getParsedTime(depDate, DATE_FORMAT_DDMMYYYY);
			} catch (ParseException e) {
				log.error("getMessageListRowHtml() Invalid Date - Parse Exception", e);
			}

			Page msgDetailsPage = ModuleServiceLocator.getAlertingBD().retrieveMessageListForLoadCriteria(fltDepDate);
			Collection msgDetails = msgDetailsPage.getPageData();

			sbData.append("var totNoOfRecord = " + msgDetailsPage.getTotalNoOfRecords() + ";");

			if ((msgDetails != null) && (!msgDetails.isEmpty())) {
				it = msgDetails.iterator();
				while (it.hasNext()) {
					FlightPnrNotificationDTO msgDetailDTO = (FlightPnrNotificationDTO) it.next();
					if (msgDetailDTO != null) {
						sbData.append(arrName + "[" + count + "] = new Array();");

						sbData.append(arrName + "[" + count + "][1] = '"
								+ ((msgDetailDTO.getFlightNumber() == null) ? "" : msgDetailDTO.getFlightNumber()) + "';");

						sbData.append(arrName + "[" + count + "][2] = '"
								+ (msgDetailDTO.getTotalEmail() + msgDetailDTO.getTotalSms()) + "';");
						sbData.append(arrName + "[" + count + "][3] = '" + msgDetailDTO.getTotalEmail() + "';");
						sbData.append(arrName + "[" + count + "][4] = '" + msgDetailDTO.getTotalSms() + "';");
						sbData.append(arrName + "[" + count + "][5] = '" + msgDetailDTO.getTotalEmailDelivered() + "';");
						sbData.append(arrName + "[" + count + "][6] = '" + msgDetailDTO.getTotalSmsDelivered() + "';");
						sbData.append(arrName + "[" + count + "][7] = '"
								+ (msgDetailDTO.getTotalEmailDelivered() + msgDetailDTO.getTotalSmsDelivered()) + "';");
						sbData.append(arrName + "[" + count + "][8] = '" + msgDetailDTO.getTotalEmailFailed() + "';");
						sbData.append(arrName + "[" + count + "][9] = '" + msgDetailDTO.getTotalSmsFailed() + "';");
						sbData.append(arrName + "[" + count + "][10] = '"
								+ (msgDetailDTO.getTotalEmailFailed() + msgDetailDTO.getTotalSmsFailed()) + "';");

						if (msgDetailDTO.getFailedEmailList() == null) {
							sbData.append(arrName + "[" + count + "][11]='';");
						} else {
							sbData.append(arrName + "[" + count + "][11] = '"
									+ removeDuplicateRecords(msgDetailDTO.getFailedEmailList()) + "';");
						}

						if (msgDetailDTO.getFailedSmsList() == null) {
							sbData.append(arrName + "[" + count + "][12]='';");
						} else {
							sbData.append(arrName + "[" + count + "][12] = '"
									+ removeDuplicateRecords(msgDetailDTO.getFailedSmsList()) + "';");
						}

						sbData.append(arrName + "[" + count + "][13] = '"
								+ ((msgDetailDTO.getSegmentCode() == null) ? "" : msgDetailDTO.getSegmentCode()) + "';");

						count++;
					}
				}
			}

		}
		return sbData.toString();
	}

    private String getCountryCountList(HashMap<String, Integer> countyCount) {
        Set<String> keys = countyCount.keySet();
        String list = "";
        for (String key : keys) {
            if ("".equalsIgnoreCase(list)) {
                list = list + key + "-" + countyCount.get(key);
            } else {
                list = list + "," + key + "-" + countyCount.get(key);
            }
        }

        return list;
    }

    private String getMobNumber(String str) {
        String strMob = "";
        if (str != null) {
            if (str.trim().length() > 5) {
                strMob = str;
            }
            for (int i = 0; i < strMob.length(); i++) {
    			if (strMob.startsWith("0")) {
    				strMob = strMob.substring(1, strMob.length());
    			} else {
    				return strMob;
    			}
    		}
        }
        return strMob;
    }

    private void getOutBoundConnectionList(FlightPnrNotificationDTO flpnDTO) throws ModuleException {
        Iterator it;
        int flightId = flpnDTO.getFlightId();
        String departureStation = flpnDTO.getRoute().substring(0, 3);
        String[] pnrArray = flpnDTO.getPnr().split(",");
        for (String pnr : pnrArray) {
            Collection outBound = ModuleServiceLocator.getReservationAuxilliaryBD().getAllOutBoundSequences(pnr, 0,
                    departureStation, flightId);
            if (outBound != null) {
                it = outBound.iterator();
                while (it.hasNext()) {
                    OnWardConnectionDTO onward = (OnWardConnectionDTO) it.next();
                    if (flpnDTO.getOutBoundFlightNo() != null) {
                        flpnDTO.setOutBoundFlightNo(flpnDTO.getOutBoundFlightNo() + "," + onward.getFlight());
                        flpnDTO.setOutBoundDepartureDate(flpnDTO.getOutBoundDepartureDate() + "," + onward.getDepartureDate().toString());
                    } else {
                        flpnDTO.setOutBoundFlightNo(onward.getFlight());
                        flpnDTO.setOutBoundDepartureDate(onward.getDepartureDate().toString());
                    }
                }
            }

        }
    }

	private String removeDuplicateRecords(String recepientsList) {
		String[] arrRecepients = recepientsList.split(",");
		Set<String> recepientsSet = new HashSet<>();
		if (arrRecepients.length > 0) {
			for (String recepient : arrRecepients) {
				recepientsSet.add(recepient);
			}

		}
		return recepientsSet.toString().replaceAll("\\[|\\]", "");
	}
}
