package com.isa.thinair.xbe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalanderUtil;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;

public class MainTest {

    /**
     * @param args
     * @throws ModuleException 
     */
    public static void main(String[] args) throws com.isa.thinair.commons.api.exception.ModuleException {
        //testSplit();
        //testGetAgentCode();
        //testDates();
        testDates2();
    }

    private static void testDates2() {
        System.out.println("Date = " + new Date());
        System.out.println("Calendar = " + Calendar.getInstance());
    }

    private static void testDates() {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        Calendar cal3 = Calendar.getInstance();
        
        long mlCurentTime = CalanderUtil.getCurrentSystemTimeInZulu().getTime();
        
        cal1.setTimeZone(TimeZone.getTimeZone("GMT"));
        cal3.setTimeInMillis(mlCurentTime);
        
        System.out.println(cal1.getTime());
        System.out.println(cal2.getTime());
        System.out.println(cal3.getTime());
        
    }

    private static void testGetAgentCode() throws ModuleException {
        String userId = "JANAKI";
        String agentCode = DatabaseUtil.getAgentCode(userId);
        
    }

    private static void testSplit() {
        String str1 = "aaaa,bbb,";
        String str2 = ",xxxx,yyyy";
        String str3 = "1111,,2222";

        String[] arr1 = StringUtils.split(str1, ",");
        String[] arr2 = StringUtils.split(str2, ",");
        String[] arr3 = StringUtils.split(str3, ",");

        showArray(arr1, "arr1");
        showArray(arr2, "arr2");
        showArray(arr3, "arr3");

    }

    private static void showArray(String[] arr, String string) {
        System.out.println("\nshow array: " + string);
        String str = "";
        for (int i = 0; i < arr.length; i++) {
            if("".equals(str)){
                str=arr[i];
            }else{
                str+="||"+arr[i];
            }
        }
        
        System.out.println(str);
    }
    
    public void testname() throws Exception {
        List lst = new ArrayList();
    
    }
    

}
