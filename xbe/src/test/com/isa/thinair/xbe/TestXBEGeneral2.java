package com.isa.thinair.xbe;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class TestXBEGeneral2 extends PlatformTestCase {

      reservationQDelegate;

      reservationDelegate;

      customerDelegate;

      commonMasterBD;

    protected void setUp() throws Exception {
        super.setUp();
        reservationQDelegate = ModuleServiceLocator.getReservationQueryBD();
        reservationDelegate = ModuleServiceLocator.getReservationBD();
        customerDelegate = ModuleServiceLocator.getCustomerBD();
        commonMasterBD = ModuleServiceLocator.getCommonServiceBD();

    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCreateAirportsListDesc() throws ModuleException {

        System.out.println("testing page Starting ..............................................");

        try {
            Date date = new SimpleDateFormat("MM/dd/yy").parse("05/18/05");
            System.out.println(date);
        } catch (Exception ex) {

        }

        String str = SelectListGenerator.createSSRCodeArray();
        System.out.println(str);

        /*
         * Currency currency = commonMasterBD.getCurrency("AED");
         * System.out.println("Ex Rate" + currency.getBaseExchangeRate());
         */

        /*
         * // Customer Credit StringBuffer strbData = new StringBuffer();
         * ReservationSearchDTO reservationSearchDTO = new
         * ReservationSearchDTO(); reservationSearchDTO.setPnr("QWERTY");
         * //reservationSearchDTO.setFirstName("N First Name");
         * //reservationSearchDTO.setLastName("N Last Name");
         * //reservationSearchDTO.setTelephoneNo("");
         * //reservationSearchDTO.setEmail("");
         * //reservationSearchDTO.setReturnDate();
         * //reservationSearchDTO.setDepartureDate();
         * 
         * Collection coll =
         * reservationQDelegate.getPnrPassengerSumCredit(reservationSearchDTO);
         * assertNotNull("Not Null" + coll);
         * 
         * Iterator iterReservation = coll.iterator(); int intCount = 0 ; int
         * intPsngCount = 0 ; int intSegmentCount = 0; String strBuyersInfo =
         * ""; String strPaxCount = ""; String strFlightNo = ""; String
         * strDeptDate = ""; String strRetuDate = ""; String strSegemnt = "";
         * String strPsng = "";
         * 
         * 
         * while (iterReservation.hasNext()) { ReservationPaxDTO
         * reservationPaxDTO = (ReservationPaxDTO)iterReservation.next();
         * System.out.println("PNR No : " + reservationPaxDTO.getPnr());
         * 
         * Collection collPassengers = reservationPaxDTO.getPassengers(); if
         * (collPassengers != null){ System.out.println("not
         * nulllllllllllllllllllllllllllll"); //----------------------- //
         * Contact Information //----------------------- ReservationContactInfo
         * reservationContactInfo =
         * reservationPaxDTO.getReservationContactInfo(); strBuyersInfo =
         * JavaScriptGenerator.nullConvertToString(reservationContactInfo.getFirstName()) + ", " +
         * JavaScriptGenerator.nullConvertToString(reservationContactInfo.getLastName());
         * strPaxCount = reservationPaxDTO.getAdultCount().toString() + "/" +
         * reservationPaxDTO.getInfantCount().toString();
         * 
         * strbData.append("arrCredits[" + intCount + "] = new Array();");
         * strbData.append("arrCredits[" + intCount + "][0] = '" +
         * JavaScriptGenerator.nullConvertToString(reservationPaxDTO.getPnr()) +
         * "';"); strbData.append("arrCredits[" + intCount + "][1] = '" +
         * strBuyersInfo + "';"); strbData.append("arrCredits[" + intCount +
         * "][2] = new Array();");
         * 
         * //----------------------- // Segment Information
         * //----------------------- Collection collSegments =
         * reservationPaxDTO.getSegments(); Iterator iterSegments =
         * collSegments.iterator(); intSegmentCount = 0 ; while
         * (iterSegments.hasNext()){ ReservationSegmentDTO reservationSegmentDTO =
         * (ReservationSegmentDTO)iterSegments.next();
         * 
         * strFlightNo =
         * JavaScriptGenerator.nullConvertToString(reservationSegmentDTO.getFlightNo());
         * if (reservationSegmentDTO.getDepartureDate() != null){ strDeptDate =
         * new
         * SimpleDateFormat("dd/MM/yyyy").format(reservationSegmentDTO.getDepartureDate()) + " " +
         * new
         * SimpleDateFormat("HH:mm").format(reservationSegmentDTO.getDepartureDate()); }
         * 
         * if (reservationSegmentDTO.getArrivalDate() != null){ strRetuDate =
         * new
         * SimpleDateFormat("dd/MM/yyyy").format(reservationSegmentDTO.getArrivalDate()) + " " +
         * new
         * SimpleDateFormat("HH:mm").format(reservationSegmentDTO.getArrivalDate()); }
         * strSegemnt =
         * JavaScriptGenerator.nullConvertToString(reservationSegmentDTO.getSegmentCode());
         * 
         * strbData.append("arrCredits[" + intCount + "][2][" + intSegmentCount + "] =
         * new Array('" + strFlightNo + "'," + "'" + strDeptDate + "'," + "'" +
         * strRetuDate + "'," + "'" + strSegemnt + "');"); intSegmentCount++; } //
         * ---------------------- End of segment array
         * 
         * strbData.append("arrCredits[" + intCount + "][3] = '" + strPaxCount +
         * "';"); strbData.append("arrCredits[" + intCount + "][4] = '" +
         * JavaScriptGenerator.nullConvertToString(reservationPaxDTO.getStatus()) +
         * "';"); strbData.append("arrCredits[" + intCount + "][5] = new
         * Array();");
         * 
         * 
         * //----------------------- // Segment Information
         * //----------------------- intPsngCount = 0 ; Iterator iterPassegners =
         * collPassengers.iterator(); while(iterPassegners.hasNext()){
         * ReservationPaxDetailsDTO reservationPaxDetailsDTO =
         * (ReservationPaxDetailsDTO)iterPassegners.next();
         * 
         * strPsng =
         * JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getTitle()) +
         * "." +
         * JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getFirstName()) + ", " +
         * JavaScriptGenerator.nullConvertToString(reservationPaxDetailsDTO.getLastName());
         * 
         * 
         * strbData.append("arrCredits[" + intCount + "][5][" + intPsngCount + "] =
         * new Array('" + reservationPaxDetailsDTO.getPnrPaxId() + "'," + "'" +
         * strPsng + "'," + "'" +
         * Double.toString(reservationPaxDetailsDTO.getCredit()) + "'," +
         * "'0');");
         * 
         * intPsngCount++; } // ---------------------- End of Passenger array
         * 
         * intCount++; }else{ System.out.println("hello world"); } }
         * 
         * System.out.println(strbData.toString());
         */

        /*
         * // Search Reservations
         * 
         * StringBuffer strbData = new StringBuffer(); Date dtFrom = new Date();
         * Date dtTo = new Date(); try{ dtFrom = new
         * SimpleDateFormat("MM/dd/yyyy").parse("01/16/2006"); //dtTo = new
         * SimpleDateFormat("MM/dd/yyyy").parse(arrHdnData[6].trim()); }catch
         * (Exception ex){}
         * 
         * ReservationSearchDTO reservationSearchDTO = new
         * ReservationSearchDTO(); //reservationSearchDTO.setPnr("QWERTY");
         * //reservationSearchDTO.setFirstName("N First Name");
         * reservationSearchDTO.setFirstName("N*");
         * reservationSearchDTO.setFromAirport("DEL");
         * reservationSearchDTO.setToAirport("CMBL");
         * 
         * //reservationSearchDTO.setLastName("N Last Name");
         * //reservationSearchDTO.setTelephoneNo("2300770211232");
         * //reservationSearchDTO.setEmail("");
         * //reservationSearchDTO.setReturnDate();
         * //reservationSearchDTO.setDepartureDate(dtFrom);
         * 
         * Collection coll =
         * reservationQDelegate.getReservations(reservationSearchDTO);
         * System.out.println("COLLECTION ULL " + coll == null);
         * assertNotNull("Not Null" + coll);
         * 
         * Iterator iterReservation = coll.iterator(); SimpleDateFormat
         * smpdtDate = new SimpleDateFormat("dd/MM/yyyy"); SimpleDateFormat
         * smpdtTime = new SimpleDateFormat("HH:mm");
         * 
         * int intCount = 0 ; int intSegmentCount = 0; String strDeptDate = "";
         * String strRetuDate = ""; String strPaxCount = ""; String strStatus =
         * "";
         * 
         * while (iterReservation.hasNext()){ ReservationDTO reservationDTO =
         * (ReservationDTO)iterReservation.next(); Collection collSegments =
         * reservationDTO.getSegments(); Iterator iterSegments =
         * collSegments.iterator();
         * 
         * strbData.append("arrData[" + intCount + "] = new Array();");
         * strbData.append("arrData[" + intCount + "][0] = '" +
         * JavaScriptGenerator.nullConvertToString(reservationDTO.getPnr()) +
         * "';"); strbData.append("arrData[" + intCount + "][1] = '';");
         * strbData.append("arrData[" + intCount + "][2]= new Array();");
         * strPaxCount = reservationDTO.getAdultCount().toString() + "/" +
         * reservationDTO.getInfantCount(); strStatus =
         * JavaScriptGenerator.nullConvertToString(reservationDTO.getStatus());
         * intSegmentCount = 0;
         * 
         * while (iterSegments.hasNext()) {
         * 
         * ReservationSegmentDTO reservationSegmentDTO =
         * (ReservationSegmentDTO)iterSegments.next(); if
         * (reservationSegmentDTO.getDepartureDate() != null &&
         * reservationSegmentDTO.getArrivalDate() != null){
         * System.out.println(reservationSegmentDTO.getDepartureDate());
         * 
         * strDeptDate =
         * smpdtDate.format(reservationSegmentDTO.getDepartureDate()) + " " +
         * smpdtTime.format(reservationSegmentDTO.getDepartureDate());
         * strRetuDate =
         * smpdtDate.format(reservationSegmentDTO.getArrivalDate()) + " " +
         * smpdtTime.format(reservationSegmentDTO.getArrivalDate());
         * 
         * 
         * strbData.append("arrData[" + intCount + "][2][" + intSegmentCount + "] =
         * new Array('" + reservationSegmentDTO.getFlightNo() + "'," + "'" +
         * strDeptDate + "'," + "'" + strRetuDate + "'," + "'" + strPaxCount +
         * "'," + "'" + strStatus + "');"); intSegmentCount++; } }
         * 
         * intCount++; } System.out.println(strbData.toString());
         */

        /*
         * 
         * //--------------------------------------------------- // Customer
         * Info Customer cust =
         * customerDelegate.getCustomer("janakik@jkcsworld.com");
         * assertNotNull("custo is null", cust); System.out.println("Email
         * Address " + cust.getFirstName()); System.out.println("arrProfile =
         * new Array('" + cust.getTitle() + "'," + "'" + cust.getFirstName() +
         * "'," + "'" + cust.getLastName() + "'," + "''," + "''," + "'" +
         * cust.getOfficeTelephone() + "'," + "'" + cust.getMobile() + "'," +
         * "''," + "'" + cust.getFax() + "'," + "'" + cust.getStatus() + "'," +
         * "'" + cust.getEmailId() + "'," + "'" + cust.getNationalityCode() +
         * "');)"); //---------------------------------------------------
         */
        System.out.println("end -------------------------------------");
    }
}
