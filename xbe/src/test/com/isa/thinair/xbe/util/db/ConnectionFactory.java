package com.isa.thinair.xbe.util.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    private static String dbUser = "AA_USER";

    private static String dbPassword = "AA_USER";

    /**
     * Obtain a connection to the Oracle database.
     * 
     * @throws java.sql.SQLException
     */
    public static Connection getConnection() throws SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {

        String driver_class = "oracle.jdbc.driver.OracleDriver";
        String connectionURL = null;
        Connection conn = null;

        try {
            Class.forName(driver_class).newInstance();
            connectionURL = "jdbc:oracle:thin:@//10.2.240.119:1521/AADB";

            conn = DriverManager.getConnection(connectionURL, dbUser, dbPassword);
            conn.setAutoCommit(false);
            System.out.println("Connected.\n");
        } catch (IllegalAccessException e) {
            System.out.println("Illegal Access Exception: (Open Connection).");
            e.printStackTrace();
            throw e;
        } catch (InstantiationException e) {
            System.out.println("Instantiation Exception: (Open Connection).");
            e.printStackTrace();
            throw e;
        } catch (ClassNotFoundException e) {
            System.out.println("Class Not Found Exception: (Open Connection).");
            e.printStackTrace();
            throw e;
        } catch (SQLException e) {
            System.out.println("Caught SQL Exception: (Open Connection).");
            e.printStackTrace();
            throw e;
        }

        return conn;
    }
}
