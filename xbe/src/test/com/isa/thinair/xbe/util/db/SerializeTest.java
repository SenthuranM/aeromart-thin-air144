package com.isa.thinair.xbe.util.db;

import java.sql.*;
import java.io.*;
import java.util.Vector;

import oracle.jdbc.OracleResultSet;

public class SerializeTest {

    private static ByteArrayOutputStream getByteArrayOutputStream(Object obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(baos);
        oout.writeObject(obj);
        oout.close();

        return baos;
    }

    public static Object getObject(byte[] buf) throws SQLException, IOException, ClassNotFoundException {
        if (buf != null) {
            ObjectInputStream objectIn = new ObjectInputStream(new ByteArrayInputStream(buf));
            return objectIn.readObject();
        }
        return null;
    }

    public static void save(Object obj) throws Exception {
        Connection con = ConnectionFactory.getConnection();
        String sqlInsert = "INSERT INTO T_MESSAGES_AUDIT (ID, MSG_PARAMS, MSG_CONTENT) VALUES(?,?,EMPTY_BLOB())";
        String sqlUpdate = "SELECT MSG_CONTENT FROM T_MESSAGES_AUDIT WHERE  id=? FOR UPDATE";
        PreparedStatement psInsert = con.prepareStatement(sqlInsert);
        PreparedStatement psUpdate = con.prepareStatement(sqlUpdate);
        ResultSet rs = null;
        ByteArrayOutputStream baos = getByteArrayOutputStream(obj);

        psInsert.setInt(1, 1);
        psInsert.setString(2, "params.......");
        psInsert.executeUpdate();
        psInsert.close();
        
        psUpdate.setInt(1, 1);
        rs = psUpdate.executeQuery();
        
        if(! rs.next()){
            
        }
       
        //image = ((OracleResultSet) rset).getBLOB("MSG_CONTENT");
        con.close();
    }

    public static void showAll() throws Exception {
        Connection con = ConnectionFactory.getConnection();
        Statement st = con.createStatement();

        ResultSet rs = st.executeQuery("SELECT ID,MSG_PARAMS,MSG_CONTENT FROM T_MESSAGES_AUDIT");

        while (rs.next()) {
            System.out.println(rs.getString("ID"));
            System.out.println(rs.getString("MSG_PARAMS"));
            System.out.println(getObject(rs.getBytes("MSG_CONTENT")));
        }

        rs.close();
        st.close();
        con.close();

    }

    public static void main(String[] args) throws Exception {

        Vector vecTest = new Vector();

        vecTest.add("Hello");
        vecTest.add("World");
        save(vecTest);

        showAll();

    }
}
