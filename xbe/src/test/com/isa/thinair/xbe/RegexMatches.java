package com.isa.thinair.xbe;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class RegexMatches {
	private static final String REGEX1 = "(SHJ/)([A-Z]{3}){0,1}";
	private static final String REGEX2 = "([A-Z]{3}){0,1}(/SHJ)";
	private static final String REGEX3 = "([A-Z]{3}/){0,1}(SHJ)(/[A-Z]{3}){0,1}";
	// private static final String INPUT = "SHJ/CMB";
	private static final String INPUT = "CMB/SHJ";
	// private static final String INPUT = "SHJ/CMB";

	public static void main(String args[]) {
		//test1();
		test2();
	}

	private static void test2() {
		//String ondPattern = "SHJ/***";
		//String ondPattern = "***/SHJ";
		String ondPattern = "***/SHJ/***";
		
		String regEx = createONDPatternRegExpresion(ondPattern);
		
		System.out.println(regEx);
		
	}

	private static void test1() {
		Pattern p = Pattern.compile(REGEX3);
		Matcher m = p.matcher(INPUT); // get a matcher object
		int count = 0;

		while (m.find()) {
			count++;
			System.out.println("Match number " + count);
			System.out.println("start(): " + m.start());
			System.out.println("end(): " + m.end());
		}

	}

	private static String createONDPatternRegExpresion(String ondPattern) {
		String ondPatternRegEx = null;

		String TAIL_MATCH = "/***";
		String HEAD_MATCH = "***/";

		if (ondPattern.indexOf(TAIL_MATCH) != -1) {
			if (ondPattern.indexOf("***/") != -1) {
				ondPatternRegEx = StringUtils.replace(ondPattern, HEAD_MATCH, "([A-Z]{3}/){0,1}(");
				ondPatternRegEx = StringUtils.replace(ondPatternRegEx, TAIL_MATCH, ")(/[A-Z]{3}){0,1}");
			} else {
				ondPatternRegEx = "(" + StringUtils.replace(ondPattern, TAIL_MATCH, "/)([A-Z]{3}){0,1}");
			}
		} else {
			ondPatternRegEx = StringUtils.replace(ondPattern, HEAD_MATCH, "([A-Z]{3}){0,1}(/") + ")";
		}

		return ondPatternRegEx;
	}
}
