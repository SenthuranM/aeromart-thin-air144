package com.isa.thinair.xbe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airreservation.api.dto.PaxAccountDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

/**
 * @author Chamindap
 * 
 */
public class TestXBEGeneral extends PlatformTestCase {

      securityBD;

      reservationBD;

      reservationQueryBD;

      resSegmentBD;

      fareBD;

    /**
     * 
     * @throws Exception .
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        // securityBD = ModuleServiceLocator.getSecurityBD();
        // reservationBD = ModuleServiceLocator.getReservationBD();

        // reservationQueryBD = ModuleServiceLocator.getReservationQueryBD();
        // resSegmentBD = ModuleServiceLocator.getResSegmentBD();
         fareBD = ModuleServiceLocator.getFareBD();

    }

    /**
     * 
     * @throws Exception .
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    // public void testReservation() throws ModuleException {
    // String pnrNo = "TR1000";
    //
    // Reservation reservation = reservationBD.getReservation(pnrNo, true,
    // true);
    //
    // assertNotNull(reservation);
    //
    // }

    //
    // public void testGetRefundHtml() throws ModuleException {
    // String pnrPaxId = "1";
    // ShowPaxAccountRH.getRefundHtml(pnrPaxId);
    //
    // }

    // public void testXBEConfig() throws ModuleException {
    // String sss = XBEConfig.getDefaultLanguage();
    //        
    // System.out.println("def Lan: " + sss);
    //
    // }

    public void testGetFare() throws ModuleException {
        int fareId = 1;

        FareDTO fareDTO = fareBD.getFare(fareId);

        assertNotNull(fareDTO);
        assertNotNull(fareDTO.getFareRule());
        System.out.println("ModificationChargeAmount: " + fareDTO.getFareRule().getModificationChargeAmount());
    }

    // public void testGetFareQuote() throws ModuleException {
    // int fareId = 1;
    //        
    // SelectedFlightDTO selectedFlightDTO =
    // reservationQueryBD.getFareQuote(null,
    // null);
    //        
    //
    //        
    // assertNotNull(selectedFlightDTO);
    // assertNotNull(selectedFlightDTO);
    //        
    // }

    // public void testLoadUser() throws ModuleException {
    // String userId = "Janaki";
    //    
    // User user = securityBD.getUser(userId);
    //    
    // assertNotNull(user);
    // assertNotNull(user.getPrivitedgeIDs());
    // System.out.println("\n\nPrevilages: " + user.getPrivitedgeIDs());
    // }

    // public void testCancelSegment() throws ModuleException {
    // SegmentBD resSegmentBD = ModuleServiceLocator.getResSegmentBD();
    // String pnrNo = "TR1000";
    // Collection colPNRSegIds = new ArrayList();
    // double cnxCharge = 150;
    //
    // colPNRSegIds.add(new Integer(2));
    // resSegmentBD.cancelSegments(pnrNo, colPNRSegIds, cnxCharge);
    // }

    // public void testCancelregment() throws ModuleException {
    // String pnrNo = "SUBB7M";
    // Collection colPNRSegIds = new ArrayList();
    // double cnxCharge = 150;
    // int pnrSegId1 = 67;
    //
    // colPNRSegIds.add(new Integer(pnrSegId1));
    //
    // resSegmentBD.cancelSegments(pnrNo, colPNRSegIds, cnxCharge);
    //
    // }

    // public void testGetPaxAccount() throws ModuleException {
    // String pnrPaxId = "75";
    //
    // PaxAccountDTO paxAccountDTO = reservationQueryBD.getPaxAccount(pnrPaxId);
    //
    // assertNotNull(paxAccountDTO);
    //
    // showPaxAccountDTO(paxAccountDTO);
    //
    // }

    // private void showPaxAccountDTO(PaxAccountDTO paxAccountDTO) {
    // Collection colDebits = paxAccountDTO.getDebits();
    // Collection colCredits = paxAccountDTO.getCredits();
    //
    // Iterator iterDr = (colDebits == null) ? null : colDebits.iterator();
    // Iterator iterCr = (colCredits == null) ? null : colCredits.iterator();
    //
    // int idx = 0;
    // String drNominalDesc = "";
    // double drAmount = 0.0;
    // Date drDate = null;
    // String crNominalDesc = "";
    // double crAmount = 0.0;
    // Date crDate = null;
    // String payType = null;
    //
    // while ((iterDr != null && iterDr.hasNext()) || (iterCr != null &&
    // iterCr.hasNext())) {
    //
    // if (iterDr != null && iterDr.hasNext()) {
    // ReservationTnx dr = (ReservationTnx) iterDr.next();
    //
    // drNominalDesc = "" + dr.getNominalCode();
    // drAmount = dr.getAmount();
    // drDate = dr.getDateTime();
    // } else {
    // drNominalDesc = "";
    // drAmount = 0.0;
    // drDate = null;
    // }
    //
    // if (iterCr != null && iterCr.hasNext()) {
    // ReservationTnx cr = (ReservationTnx) iterCr.next();
    //
    // drNominalDesc = "" + cr.getNominalCode();
    // drAmount = cr.getAmount();
    // drDate = cr.getDateTime();
    // payType = cr.getPaymentType();
    // } else {
    // crNominalDesc = "";
    // crAmount = 0.0;
    // crDate = null;
    // payType = "";
    // }
    //
    // System.out.println("aRfnd[" + idx + "][0]='" + idx + "';");
    // System.out.println("aRfnd[" + idx + "][1]='" +
    // StringUtil.toString(drNominalDesc) + "';");
    // System.out.println("aRfnd[" + idx + "][2]='" + drAmount + "';");
    // System.out.println("aRfnd[" + idx + "][3]='" +
    // StringUtil.toString(drDate) + "';");
    // System.out.println("aRfnd[" + idx + "][4]='" +
    // StringUtil.toString(crNominalDesc) + "';");
    // System.out.println("aRfnd[" + idx + "][5]='" + crAmount + "';");
    // System.out.println("aRfnd[" + idx + "][6]='" +
    // StringUtil.toString(crDate) + "';");
    // System.out.println("aRfnd[" + idx + "][7]='" +
    // StringUtil.toString(payType) + "';");
    //
    // }
    //
    // }

    // public void testAdjustCredit() throws ModuleException {
    // PassengerBD resPaxBD = ModuleServiceLocator.getResPassengerBD();
    // String pnrNo = "SUBB7M";
    // String pnrPaxId = "75";
    // String txtAdjust = "";
    // String txtaNote = "testing: " + pnrNo + ": " + pnrPaxId;
    //
    // resPaxBD.adjustCreditManual(pnrNo, pnrPaxId, new Double(txtAdjust),
    // txtaNote);
    //        
    // testGetPaxAccount();
    //
    // }

    // public void testGetAgentCode() throws ModuleException {
    // String userId = "Janaki";
    //
    // String agentCode = DatabaseUtil.getAgentCode(userId);
    //
    // assertNotNull(agentCode);
    // System.out.println("agentCode:" + agentCode);
    //
    // }

}
