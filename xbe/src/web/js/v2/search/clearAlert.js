/**
	*********************************************************
		Description		: XBE Clear Alert and Transfer process
		Author			: Pradeep Karunanayake		
	*********************************************************	
*/
function UI_clearAlert(){}	

/*
 * Document ready
 */		
$(function() { 	
	UI_clearAlert.ready();  
});

UI_clearAlert.ready = function() {
	$("#btnConfirm").decorateButton();
	$("#btnTrnsfr").decorateButton();
	$("#btnExit").decorateButton();
	$("#btnTrnsfrConfirm").decorateButton();
	$("#btnConfirm").click(function(){UI_clearAlert.ActionClick(1);});
	$("#btnTrnsfr").click(function(){UI_clearAlert.checkAlertsSegments();});
	$("#btnTrnsfrConfirm").click(function(){UI_clearAlert.transferConfirmClick();});
	$("#btnExit").click(function(){UI_clearAlert.ActionClick(2);});
	$("#selAction").change(function(){UI_message.hideMessage();});
	$("#chkTrnsfr").change(function(){UI_clearAlert.chkChange();});
	$("#btnTrnsfrConfirm").hide();
    if (DATA_ResPro.initialParams.transferAlert == false) {
        $("#btnTrnsfr").disableButton();
    }
//	if(DATA_ResPro.initialParams.clearAlertAfterTransfer== true) {	
//		$("#btnConfirm").disableButton();	
//	}
}

UI_clearAlert.checkAlertsSegments =  function() {
	
	if ((opener.jsonGroupPNR == null || opener.jsonGroupPNR == "") && opener.UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef != null &&
			opener.UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef.split(",").length > 1) {
		/**
		 * Display Transfer segment prompt
		 * Get informations from the user
		 */
		$("#btnConfirm").hide();
		$("#btnTrnsfr").hide();
		$("#clearAlertPannel").hide();
		$("#btnTrnsfrConfirm").show();
		$("#transferSegmentPannel").show();		
	} else {
		opener.UI_tabBookingInfo.setTransferSegmentAdditionalData("tranSelAltSeg");
		UI_clearAlert.ActionClick(3);
	}
}

UI_clearAlert.transferConfirmClick = function() {
	var $transferAction = $("#selTransfer").val();
	if ($transferAction ==  null || $transferAction == "") {
		UI_message.showErrorMessage({messageText:"Please select transfer performed"}); 
		} else {
		opener.UI_tabBookingInfo.setTransferSegmentAdditionalData($transferAction);
		UI_clearAlert.ActionClick(3);
	}
}


/*
 * Button functionality - clear alert, transfer segment,close
 */
UI_clearAlert.ActionClick = function(strID){
	switch (strID){
		case 1:			
			if (UI_clearAlert.pageValidate()){
				UI_clearAlert.disableButtons();				
				opener.UI_tabBookingInfo.clearAlertConfirm({"comment":$("#txtaComment").val(),"selAction":$("#selAction").val()});
				UI_clearAlert.enableButtons();
			}			
			break;
		case 2 :
			window.close();
			break;
		case 3:			
			if (opener.UI_tabBookingInfo.transferSegment()) {
				window.close();
			} else {
				window.focus();
			}
			break;
	}
}
/*
 * Validate data
 */
UI_clearAlert.pageValidate = function(){
	UI_message.initializeMessage(); 
	
	if ($("#txtaComment").val().length > 250){
		UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-18","250","Additional information")}); 
		$("#txtaComment").focus();		
		return false;
	}
	if ($.trim(getValue("txtaComment")) == "") {			
		UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-01","Additional information")}); 
		$("#txtaComment").focus();
		return false;		
	}
	if (!isAlphaNumericWhiteSpace($.trim($("#txtaComment").val()))){		
		UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-12","Additional information")}); 
		$("#txtaComment").focus();
		return false;
	}		
	return true;
}
/*
 * Check box on change
 */
UI_clearAlert.chkChange = function() {
	if(DATA_ResPro.initialParams.clearAlert== true && DATA_ResPro.initialParams.clearAlertAfterTransfer== false) {		
		if ($('#chkTrnsfr').is(':checked')){			
			$("#btnConfirm").enableButton();			
		}else{			
			$("#btnConfirm").disableButton();	
		}
	}
}
/*
 * Disable all buttons
 */
UI_clearAlert.disableButtons = function() {
	if (DATA_ResPro.initialParams.clearAlert == 1){
		$("#btnConfirm").disableButton();	
	}
	
	if (DATA_ResPro.initialParams.transferAlert==1) {		
		$("#btnTrnsfr").disableButton();	
	}
	$("#btnExit").disableButton();	
}
/*
 * Enable all buttons
 */
UI_clearAlert.enableButtons = function() {	
	if(DATA_ResPro.initialParams.clearAlert== true && DATA_ResPro.initialParams.clearAlertAfterTransfer== false) {		
		if ($('#chkTrnsfr').is(':checked')){			
			$("#btnConfirm").enableButton();			
		}else{			
			$("#btnConfirm").disableButton();	
		}
	}
	if (DATA_ResPro.initialParams.transferAlert==1) {		
		$("#btnTrnsfr").enableButton();	
	}
	$("#btnExit").enableButton();	
}