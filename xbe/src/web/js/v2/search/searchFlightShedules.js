/**
 * 
 */

function UI_searchFlightSchedules(){}

/*
	 * Search Flight schedules Tab Page On Load
	 */
$(document).ready(function(){
	
	$("#btnSearch").decorateButton();
	$("#btnReset").decorateButton();
	$("#btnClose").decorateButton();

	$("#departureDate").datepicker({ minDate: new Date(), showOn: "button", buttonImage: "../images/Calendar_no_cache.gif", buttonImageOnly: true, maxDate:'+1Y', yearRange:'-1:+1', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#returnDate").datepicker({ minDate: new Date(), showOn: "button", buttonImage: "../images/Calendar_no_cache.gif", buttonImageOnly: true, maxDate:'+1Y', yearRange:'-1:+1', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	
	$("#departureDate").blur(function() { UI_searchFlightSchedules.dateOnBlur({id:0});});
	$("#returnDate").blur(function() { UI_searchFlightSchedules.dateOnBlur({id:1});});
	
	$('#btnSearch').click(function() { UI_searchFlightSchedules.searchFlightSchedules(); });
	$('#btnReset').click(function() { UI_searchFlightSchedules.resetOnClick();});
	$('#btnClose').click(function() { UI_searchFlightSchedules.closeOnClick();});
	
	if(agentWiseRouteSelection) {
		UI_searchFlightSchedules.fillDropDownDataAgentWise();
	} else {
		UI_searchFlightSchedules.fillDropDownData(top.arrAirportsV2, top.arrAirportsV2); 
	}
	
	if(viewAvailableCredit) {
		UI_commonSystem.showAgentCredit();
		UI_commonSystem.showAgentCreditInAgentCurrency();
	} else {
		$("#divCreditPnl").hide();
	}
	
	$("#divSrchResults").hide();

	
});

/*
 * Auto Date generation
 */
UI_searchFlightSchedules.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 :
				if($.trim($("#departureDate").val()) != ''){					
					dateChk("departureDate");
				}else{
					$("#departureDate").val('');
				}
				break;
			case 1 : 
				if($.trim($("#returnDate").val()) != ''){					
					dateChk("returnDate");
				}else{
					$("#returnDate").val('');
				}
				break;
		}
}

/*
 * Reset Button On Click
 */
UI_searchFlightSchedules.resetOnClick = function() {
	UI_commonSystem.initializeMessage();
	$("#frmSrchFlightSchedules").reset();
	$("#divSrchResults").hide();
	DivWrite("outBoundSchTable" ,"");
	DivWrite("inBoundSchTable","");
}

/*
 * Close Button On Click
 */
UI_searchFlightSchedules.closeOnClick = function() {
	top.LoadHome();
}



/*
 * Construct Grid
 */
UI_searchFlightSchedules.constructGrid = function(inParams){
	// construct grid
	jsPaymentGrid = $(inParams.id).jqGrid({
		datatype: "local",		
		height: 140,
		width: 920,// 960
		colNames:['&nbsp;','Flight','Departure','Arrival','Days of Operation','From Date','To Date' ,'Stopovers' ,'Aircraft Model' ,'Total Flying Time', 'Transit Duration'], 
		colModel:[
				    {name:'id', width:5, jsonmap:'id' },  
					{name:'flightNo',		index: 'flightNo', 		width:30, align:"left"},	
					{name:'departureTime', 	index: 'departureTime', 		width:30, align:"left" },
					{name:'arrivalTime', 	index: 'arrivalTime', 		width:30, align:"left" },
					{name:'daysOfOperation',	index: 'daysOfOperation', 	width:60, align:"left"},
					{name:'fromDate',	index: 'fromDate', 	width:30, align:"left"},	
					{name:'toDate', 		index: 'toDate',			width:30, align:"left"},	
					{name:'noOfStopovers', 			index: 'noOfStopovers',			width:30, align:"left"},					
					{name:'flightModel', 		index: 'flightModel',			width:40, align:"left"},
					{name:'totalFlyingTime', 		index: 'totalFlyingTime',			width:50, align:"left"}	,	
					{name:'transitDuration', 		index: 'transitDuration',			width:60, align:"left"}		
			],
		imgpath: UI_commonSystem.strImagePath,
		multiselect: false,
		viewrecords: true,	
	});
}

UI_searchFlightSchedules.getRouteHtml= function(inParams)
{
	var routeRowHtmltext = "<tr id='"+inParams.rowId+"'> "+
			"<td class='ui-widget ui-widget-content ui-corner-all'>"+
			"<table width='100%' border='0' cellpadding='1' cellspacing='0'>" +
			"	<tr> "+
			"		<td class='paneHD'> "+
			"			<span class='txtWhite txtBold' id='"+inParams.headerId+"'></span> "+
			"		</td> "+
			"	</tr> "+
			"	<tr> "+
			"		<td > "+
			" 			<div style='margin-top:10px;' id='"+inParams.gridHolder+"'></div>";		
			"		</td> "+
			"	</tr>"+												
			"</table>"+													
			"</td>"+
			"</tr>";
			
			return routeRowHtmltext;
	
}

UI_searchFlightSchedules.setFlightSchedules= function(response)
{
	$("#divOutboundSchResults").hide();
	$("#divInboundSchResults").hide();
	
	if (response.outBoundFltRouteScheduleInfo != null && response.outBoundFltRouteScheduleInfo.length >0 )
	{
		var routeIndex = 0;
		var segmentIndex =0;
		var objRouteData = null;
		var objScheduleData = null;
		
		var gridHolderIdPrefix ="flt_sch_out_gridHolder_";
		var rowIdPrefix ="flt_sch_out_Row_";
		var rowHeaderIdPrefix ="flt_sch_out_RowHeader_";
		var gridIdPrefix = "flt_sch_out_gridId_";
		
		$.each(response.outBoundFltRouteScheduleInfo, function(){
			objRouteData = response.outBoundFltRouteScheduleInfo[routeIndex];
			segmentIndex =0;
			var rowId =rowIdPrefix +routeIndex;
			var rowHeaderId = rowHeaderIdPrefix+routeIndex;
			var gridHolderId = gridHolderIdPrefix+routeIndex;
			var routeRowhtml = UI_searchFlightSchedules.getRouteHtml({rowId:rowId,headerId:rowHeaderId,gridHolder:gridHolderId});
			
			$("#outBoundSchTable").append(routeRowhtml);
			DivWrite(rowHeaderId, " Route : " + objRouteData.route );
			$.each(objRouteData.fltScheSegInfo, function(){
				objScheduleData =objRouteData.fltScheSegInfo[segmentIndex];
				var gridId =gridIdPrefix+routeIndex+segmentIndex;
				
				var htmlSchGrid ="<div style='margin-top:5px;'><table id='"+gridId+"'></table></div>";
				$("#"+gridHolderId).append(htmlSchGrid);
				
				UI_searchFlightSchedules.constructGrid({id:"#"+gridId});
				
				UI_commonSystem.fillGridData({id:"#"+gridId, data: objScheduleData.flightSchedules});			
				jQuery("#"+gridId).setCaption(objScheduleData.segment);		
							
				segmentIndex++;
			});
			routeIndex++;
		});
		$("#divOutboundSchResults").show();
	}
	
	if (response.inBoundFltRouteScheduleInfo != null && response.inBoundFltRouteScheduleInfo.length >0 )
	{
		var routeIndex = 0;
		var segmentIndex =0;
		var objRouteData = null;
		var objScheduleData = null;		
		
		var rowIdPrefix ="flt_sch_In_Row_";
		var rowHeaderIdPrefix ="flt_sch_In_RowHeader_";
		var gridHolderIdPrefix ="flt_sch_In_gridHolder_";
		var gridIdPrefix = "flt_sch_In_gridId_";
		
		$.each(response.inBoundFltRouteScheduleInfo, function(){
			objRouteData = response.inBoundFltRouteScheduleInfo[routeIndex];
			segmentIndex =0;
			var rowId =rowIdPrefix + routeIndex;
			var rowHeaderId = rowHeaderIdPrefix + routeIndex;
			var gridHolderId = gridHolderIdPrefix + routeIndex;
			var routeRowhtml = UI_searchFlightSchedules.getRouteHtml({rowId:rowId,headerId:rowHeaderId,gridHolder:gridHolderId});
			
			$("#inBoundSchTable").append(routeRowhtml);
			DivWrite(rowHeaderId, " Route : " + objRouteData.route );
			$.each(objRouteData.fltScheSegInfo, function(){
				objScheduleData =objRouteData.fltScheSegInfo[segmentIndex];
				var gridId =gridIdPrefix+routeIndex+segmentIndex;
				
				var htmlSchGrid ="<div style='margin-top:5px;'><table id='"+gridId+"'></table></div>";
				
				$("#"+gridHolderId).append(htmlSchGrid);
				
				UI_searchFlightSchedules.constructGrid({id:"#"+gridId});
				
				UI_commonSystem.fillGridData({id:"#"+gridId, data: objScheduleData.flightSchedules});			
				jQuery("#"+gridId).setCaption(objScheduleData.segment);		
							
				segmentIndex++;
			});
			routeIndex++;
		});
		$("#divInboundSchResults").show();
	}
}


/*
 * Search Click
 */
UI_searchFlightSchedules.searchFlightSchedules = function (){	
	var data = {};
	$("#frmSrchFlightSchedules").ajaxSubmit({ dataType: 'json', data:data,
									beforeSubmit:UI_searchFlightSchedules.validateSearch,	
									success: UI_searchFlightSchedules.returnCallSearchFlightSchedules,
									error:UI_commonSystem.setErrorStatus});
	return false;
	
}

UI_searchFlightSchedules.validateSearch = function (){
	
	if($("#fromAirport").val()=="" || $("#fromAirport").val()==null || $('#fromAirport').val().toUpperCase() == 'PLEASE SELECT A VALUE'){
		showERRMessage("Departure location can not be empty");
		$("#fromAirport").focus();			
		return false;
	}
	
	if($("#toAirport").val()=="" || $("#toAirport").val()==null || $('#toAirport').val().toUpperCase() == 'PLEASE SELECT A VALUE'){
		showERRMessage("Arrival location can not be empty");
		$("#toAirport").focus();			
		return false;
	}

	if ($("#fromAirport").val() == $("#toAirport").val()){
		showERRMessage("Departure location and Arrival location can not be same");
		$("#fromAirport").focus();			
		return false;
	}
	
	if ($("#departureDate").val()=="" || $("#departureDate").val()==null){
		showERRMessage("From date can not be empty");
		$("#departureDate").focus();		
		return false;
	}
	
	if ($("#returnDate").val()=="" || $("#returnDate").val()==null){
		showERRMessage("To date can not be empty");
		$("#returnDate").focus();		
		return false;
	}
	
	UI_commonSystem.showProgress();
	return true;
}

/*
 * Search Click success
 */
UI_searchFlightSchedules.returnCallSearchFlightSchedules = function(response){
	var roundTrip = $('#chkreturn').attr('checked');
	UI_commonSystem.hideProgress();
	
	DivWrite("outBoundSchTable" ,"");
	DivWrite("inBoundSchTable","");
	
	$("#divSrchResults").show();
	if(response!=null && response.success && (response.outBoundFltRouteScheduleInfo.length != 0 || response.inBoundFltRouteScheduleInfo .length != 0 ) ){
		UI_searchFlightSchedules.setFlightSchedules(response);		
	}else{
		if(response.success ){		
			showERRMessage('No matching flight schedules found');			
		}else{
			showERRMessage(response.messageTxt);
		}		
	}	
}


/*
 * Fill Drop Down Data for the Page
 */
UI_searchFlightSchedules.fillDropDownData = function(toData, fromData){
	
	var fromStore = new Ext.data.ArrayStore({
	    fields: ['code', 'x', 'name'],
	    data : fromData
	});
 
	var toStore = new Ext.data.ArrayStore({
	    fields: ['code', 'x', 'name'],
	    data : toData
	}); 
	
	UI_searchFlightSchedules.fromCombo = new Ext.form.ComboBox({
	    store: fromStore,
	    id: 'extFromAirport',
	    displayField:'name',
	    typeAhead: false,
	    mode: 'local',
	    triggerAction: 'all',
	    emptyText:'Please select a value',
	    selectOnFocus:true,
	    applyTo: 'fAirport',
 	    anyMatch: true,
        caseSensitive: false,
        forceSelection: true,
        valueField:'code',
        hiddenName:'searchFlightScheduleTO.fromAirport', // 'searchParams.fromAirport',
        hiddenId:'fromAirport',
        listeners: {
            'blur': function(f,new_val,oldVal) {
            	UI_searchFlightSchedules.onExtComboOnChange(f,new_val,oldVal)
            }
        }
	});
	
	UI_searchFlightSchedules.toCombo = new Ext.form.ComboBox({
	    store: toStore,
	    id: 'extToAirport',
	    displayField:'name',
	    typeAhead: false,
	    mode: 'local',
	    triggerAction: 'all',
	    emptyText:'Please select a value',
	    selectOnFocus:true,
	    applyTo: 'tAirport',
 	    anyMatch: true,
        caseSensitive: false,
        valueField:'code',
        hiddenName: 'searchFlightScheduleTO.toAirport', // 'searchParams.toAirport',
        hiddenId:'toAirport',
        listeners: {
            'blur': function(f,new_val,oldVal) {
            	UI_searchFlightSchedules.onExtComboOnChange(f,new_val,oldVal)
            }
        }
	});	
}	
	
UI_searchFlightSchedules.onExtComboOnChange = function (combo,record,index){
		var newVal = combo.getValue();		
		if (!UI_searchFlightSchedules.isAvailableAnList(combo, newVal)){
			combo.reset();
			combo.select();
		}
	}
	
UI_searchFlightSchedules.isAvailableAnList = function (combo, val){
		var dataObj = combo.store.data.items;
		for (var i = 0; i < dataObj.length; i++){
			if (val == dataObj[i].data.code){
				return true;
				break;
			}
		}
		return false;
	}

UI_searchFlightSchedules.fillDropDownDataAgentWise = function(){
	var toData = top.arrAirportsV2;
	var fromData = top.arrAirportsV2;
	var isAllOriginAirports = false;
	var isAllOriginDestAirports = false;
	var allOnds = top.arrOndMap;
	var selectedDestinations = new Array();
	var newToData = new Array();
	var newFromData = new Array();
	var ondMap = null;
	
	if(allOnds != null) {
		if(typeof(allOnds['***']) != "undefined"){
			isAllOriginAirports = true;
			if(allOnds['***'].indexOf('***') > -1){
				isAllOriginDestAirports = true;									
			} else {
				var destinationsString = allOnds['***'].replace(/^'+/i, '');
				var destinationArray = destinationsString.split("|");
				for(var key in destinationArray){
					if(destinationArray.hasOwnProperty(key)){
						if(destinationArray[key].indexOf('***') == -1){
							selectedDestinations.push(destinationArray[key].split(",")[0]);
						}
					}					
				}				
			}
		} else {
			ondMap = new Object();
			for(var ond in allOnds){
				if(allOnds.hasOwnProperty(ond)) {
					ondMap[ond] = allOnds[ond].replace(/^'+/i, '').split("|");
				}				
			}
		}
	}
	
	if(isAllOriginAirports && isAllOriginDestAirports) {
		UI_searchFlightSchedules.fillDropDownData(toData, fromData);
	} else if (isAllOriginAirports && selectedDestinations.length > 0) {
		for(var select in selectedDestinations) {
			if(selectedDestinations.hasOwnProperty(select)) {
				for(var to in toData) {
					if(toData.hasOwnProperty(to)) {
						if(selectedDestinations[select] == toData[to][0].replace(/^'+/i, '')) {
							newToData.push(toData[to]);
						}
					}					
				}
			}			
		}
		UI_searchFlightSchedules.fillDropDownData(newToData, fromData);		
	} else if (ondMap != null) {
		for (ond in ondMap) {
			if(ondMap.hasOwnProperty(ond)) {
				for (var from in fromData) {
					if(fromData.hasOwnProperty(from)) {
						if(ond == fromData[from][0]) {
							newFromData.push(fromData[from]);
						}
					}
				}
			}			
		}
		
		if(newFromData.length > 0) {
			var fromStore = new Ext.data.ArrayStore({
			    fields: ['code', 'x', 'name'],
			    data : newFromData
			});	
			
			UI_searchFlightSchedules.fromCombo = new Ext.form.ComboBox({
			    store: fromStore,
			    id: 'extFromAirport',
			    displayField:'name',
			    typeAhead: false,
			    mode: 'local',
			    triggerAction: 'all',
			    emptyText:'Please select a value',
			    selectOnFocus:true,
			    applyTo: 'fAirport',
		 	    anyMatch: true,
		        caseSensitive: false,
		        forceSelection: true,
		        valueField:'code',
		        hiddenName:'searchFlightScheduleTO.fromAirport', // 'searchParams.fromAirport',
		        hiddenId:'fromAirport',
		        listeners: {
		        	'blur': function(f,new_val,oldVal) {
			            	UI_searchFlightSchedules.onExtComboOnChange(f,new_val,oldVal)
			            }
		        }
			});
		}
	
		UI_searchFlightSchedules.fromCombo.on('change', function(){
			UI_searchFlightSchedules.onExtComboOnChangeFiltered();
		});
		
		UI_searchFlightSchedules.fromCombo.on('select', function(){
			
			if(UI_searchFlightSchedules.toCombo != undefined) {
				UI_searchFlightSchedules.toCombo.clearValue();
				UI_searchFlightSchedules.toCombo.applyEmptyText();
				UI_searchFlightSchedules.toCombo.reset();
			}			
		});
		
		UI_searchFlightSchedules.onExtComboOnChangeFiltered = function (){
			var newVal = UI_searchFlightSchedules.fromCombo.getValue();	
			var toData = top.arrAirportsV2;
			var newToData = new Array();
			var foundAnyDestination = false;
			if(ondMap[newVal] != null && ondMap[newVal] != undefined) {
				for (var dest in ondMap[newVal]) {
					if(ondMap[newVal].hasOwnProperty(dest)) {
						if(ondMap[newVal][dest].indexOf('***') > -1) {
							foundAnyDestination = true;
							break;
						} else {
							for(var to in toData) {
								if(toData.hasOwnProperty(to)) {
									if(ondMap[newVal][dest].split(",")[0] == toData[to][0].replace(/^'+/i, '')) {
										newToData.push(toData[to]);
									}
								}								
							}
						}
					}
					
				}
			}
			
			if(!foundAnyDestination) {
				toData = newToData;
			}
			
			var toStore = new Ext.data.ArrayStore({
			    fields: ['code', 'x', 'name'],
			    data : toData
			}); 
			
			
			if(UI_searchFlightSchedules.toCombo != undefined){
				UI_searchFlightSchedules.toCombo.clearValue();
				UI_searchFlightSchedules.toCombo.applyEmptyText();
				UI_searchFlightSchedules.toCombo.reset();
				Ext.getCmp("extToAirport").bindStore(toStore);
			} else {
				UI_searchFlightSchedules.toCombo = new Ext.form.ComboBox({
				    store: toStore,
				    id: 'extToAirport',
				    displayField:'name',
				    typeAhead: false,
				    mode: 'local',
				    triggerAction: 'all',
				    emptyText:'Please select a value',
				    selectOnFocus:true,
				    applyTo: 'tAirport',
			 	    anyMatch: true,
			        caseSensitive: false,
			        valueField:'code',
			        hiddenName: 'searchFlightScheduleTO.toAirport', // 'searchParams.toAirport',
			        hiddenId:'toAirport',
			        listeners: {
			            'blur': function(f,new_val,oldVal) {
			            	UI_searchFlightSchedules.onExtComboOnChange(f,new_val,oldVal)
			            }
			        }
			        
				});	
			}	  
			
			
			
		}
	}		
}

