UI_AgentTicketStock.screenID = "SC_AXBE_032";
UI_AgentTicketStock.addEdit; 


/**
 * UI_AgentTicketStock for AgentTicket sUI related functionalities
 */
function UI_AgentTicketStock() {
}

/**
 * Onload function
 */
$( function() {

	$("#divResultsPanel").decoratePanelWithAlign("Agent Ticket Stocks");	
	$("#divDisplayAgentTktStock").decoratePanelWithAlign("Add/Edit Ticket Stock");
	
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();	
	$('#btnSave').decorateButton();
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();	
	
	$("#tblAgentTktStock").jqGrid({ 
		url:'showAdjustAgentTicketStock.action?agentCode='+strAgentCode,
		datatype: "json",	
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,			 
			  id: "0"				  
			},			
		colNames:['&nbsp;','ID','Sequence Name','Sequence UpperBound', 'Sequence LowerBound', 'Stock Size', 'Cycle',
		          'Status','Version'], 
		colModel:[ 	{name:'id', width:5, jsonmap:'id' },  
		           	{name:'agentTicketStockTo.agentTicketSeqId',		index: 'agentTicketSeqId', 		width:5, align:"left", hidden:true },
		           	{name:'agentTicketStockTo.sequenceName', 			index: 'sequenceName',			width:80, align:"left"},
		           	{name:'agentTicketStockTo.sequenceUpperBound', 	index: 'sequenceUpperBound', 		width:50, align:"left" },
		           	{name:'agentTicketStockTo.sequenceLowerBound', 	index: 'sequenceLowerBound', 		width:50, align:"left" },
		           	{name:'agentTicketStockTo.stockSize',	index: 'stockSize', 	width:50, align:"left"},
		           	{name:'agentTicketStockTo.cycleSequence', 			index: 'cycleSequence',			width:30, align:"left" },
		           	{name:'agentTicketStockTo.status', 			index: 'status',			width:50, align:"left"},		        	
		           	{name:'agentTicketStockTo.version', 			index: 'version',			width:30, align:"left" , hidden:true}
					], 
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#divAgentTktStockPager'),
		rowNum:10,						
		viewrecords: true,
		height:120,
		width:885,	
		onSelectRow: function(rowid){
			UI_AgentTicketStock.gridRowOnClick(rowid);			
		},
	   	loadComplete: function (e){
	   		$('#load_tblAgentTktStock').hide();					
	   	}
	}).navGrid("#divAgentTktStockPager",{refresh: true, edit: false, add: false, del: false, search: false});
	
	

	$('#btnEdit').click(function() { UI_AgentTicketStock.editOnClick(); });
	$('#btnAdd').click(function() { UI_AgentTicketStock.AddOnClick(); });
	$('#btnSave').click(function() { UI_AgentTicketStock.saveClick(); });
	$('#btnReset').click(function() { UI_AgentTicketStock.resetonClick(); });
	$('#btnClose').click(function() { UI_AgentTicketStock.closeClick(); });	
	
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();
 
    $("#frmModify").disableForm(); 

});

/*
 * Close Window
 */
UI_AgentTicketStock.closeClick = function(){	
	window.close()
}

UI_AgentTicketStock.refreshGrid = function (){	
	$("#frmModify").disableForm();	
	UI_AgentTicketStock.clearEditArea();		
	$("#tblAgentTktStock").trigger("reloadGrid");//This is an  Event
}

UI_AgentTicketStock.resetonClick = function (){	
	if($("#tblAgentTktStock").getGridParam("selrow") != null){
		UI_AgentTicketStock.gridRowOnClick($("#tblAgentTktStock").getGridParam("selrow"));
	} else {
		$("#frmModify").clearForm();
		$('#frmModify').disableForm();		
		$("#tblAgentTktStock").setGridParam({page:1,postData:{}});		
		$("#tblAgentTktStock").trigger("reloadGrid");
	}	
}

UI_AgentTicketStock.editOnClick = function (){	
	$("#btnSave").enableButton();
	$("#frmModify .frm_editable").enableFormSelective();
	$("#tktStockLowerBound").attr("readonly", true);
	$("#tktStockSequenceName").attr("readonly", true);
	$("#tktStockIsCycle").attr("disabled", true);
	
}

UI_AgentTicketStock.gridRowOnClick = function (rowid){	
	$("#tblAgentTktStock").GridToForm(rowid,"#frmModify");
	
	var selectedRow = UI_AgentTicketStock.getSelectedRow();
	if(selectedRow['agentTicketStockTo.cycleSequence'] == 'Y'){
		$('#tktStockIsCycle').attr('checked',true);
	}else
	{
		$('#tktStockIsCycle').attr('checked',false);
	}
	
	$("#frmModify").disableForm();
	$("#btnEdit").enableButton();
}

UI_AgentTicketStock.AddOnClick = function (){	
	UI_AgentTicketStock.clearEditArea();
	$("#btnSave").enableButton();
	$("#frmModify").enableForm();
	$("#tktStockSequenceName").attr("readonly", true);
	$("#tktStockLowerBound").attr("readonly", false);	
	
	$("#tblAgentTktStock").resetSelection();	
}

UI_AgentTicketStock.saveClick = function (){	
	$('#frmModify').ajaxSubmit({ dataType: 'json', 
		beforeSubmit:UI_AgentTicketStock.beforeSaveFormSubmit , 
		success: UI_AgentTicketStock.afterSave		
	});
}

UI_AgentTicketStock.clearEditArea = function (){
	$("#frmModify").clearForm();
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();	
}

UI_AgentTicketStock.beforeSaveFormSubmit = function (formData, jqForm, options){
	if(!UI_AgentTicketStock.validateSave()){
		return false;
	}	
	var $jsonString = {};
	$jsonString[formData.length] = {};
	$jsonString[formData.length].name = 'agentTicketStockTo.agentCode';
	$jsonString[formData.length].value = strAgentCode;
	$.extend(formData, $jsonString);
	
	var $jsonString = {};
	$jsonString[formData.length] = {};
	$jsonString[formData.length].name = 'agentTicketStockTo.cycleSequence';
	$jsonString[formData.length].value =( ($('#tktStockIsCycle').is(':checked')) ? 'Y':'N');
	$.extend(formData, $jsonString);	
}

UI_AgentTicketStock.afterSave = function (response){
	if(response.msgType != 'Error'){
		var postData = {};		
		$("#frmModify").clearForm();		
		postData['agentCode']= strAgentCode;	
		
		$("#tblAgentTktStock").setGridParam({page:1,postData:postData});
	 	$("#tblAgentTktStock").trigger("reloadGrid");
	 	
	 	$("#frmModify").disableForm();
	 	$("#btnSave").disableButton();
	 	UI_message.showConfirmationMessage({messageText:arrError['saveRecoredSucces']});
	 	
	}else{
	    UI_message.showErrorMessage({messageText:response.message});
	}
}

UI_AgentTicketStock.validateSave = function (){	
	
	if($("#tktStockLowerBound").isFieldEmpty()){		
		UI_message.showErrorMessage({messageText:arrError['msgTStockLowerBoundEmpty']});
		return false;
	}
	if($("#tktStockUpperBound").isFieldEmpty()){
		UI_message.showErrorMessage({messageText:arrError['msgTStockUpperBoundEmpty']});		
		return false;
	}
	if($("#tktStockStatus").isFieldEmpty()){
		UI_message.showErrorMessage({messageText:arrError['msgTktStockStatusEmpty']});				
		return false;
	}
	
	var selectedRow = UI_AgentTicketStock.getSelectedRow();
	if(selectedRow != null){	
		if (parseInt(selectedRow['agentTicketStockTo.sequenceUpperBound'])> parseInt($("#tktStockUpperBound").val()) )
		{
			UI_message.showErrorMessage({messageText:arrError['msgTktStockUpperNewInvalid']});		
			return false;
		}
	}
	return true;
}

UI_AgentTicketStock.getSelectedRow = function (){
	if($("#tblAgentTktStock").getGridParam("selrow") != null){
		return $("#tblAgentTktStock").getRowData($("#tblAgentTktStock").getGridParam("selrow"));		
	}else
		return null;	
}


UI_AgentTicketStock.onAddEditDataPopulate = function (status){
	UI_AgentTicketStock.addEdit = status;
}

UI_AgentTicketStock.positiveInt = function (objTextBox){	
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isPositiveInt(strText);
	if(!blnVal){
		objTextBox.value =  strText.substr(0,strLen-1); 
		objTextBox.focus();
	}
}