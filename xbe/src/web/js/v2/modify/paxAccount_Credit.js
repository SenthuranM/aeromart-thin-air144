	/*
	*********************************************************
		Description		: XBE - Pax Account Details - Credit
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	var jsonCreditsObj = [
					{id:"#txtCreditAmount", desc:"Amount", required:true},
					{id:"#txtExpiryDate", desc:"Expiry Date", required:true},
					{id:"#txtCreditUN", desc:"User Notes", required:true}
					];
	
	var jsCredits =  ({displayExpiredCredit:"", displayAvailableCredit:"", 
						credits :[
						{displayAmount:"", displayExpiryDate:"", displayNotes:"", displayTxnID:"", tnxStatus:""}
						]});
	
	function UI_paxAccountCredit(){}
	UI_paxAccountCredit.intCreditID = null;
	
	/*
	 * Payment ready method
	 */
	UI_paxAccountCredit.ready = function(){

		$("#btnCreditClose").decorateButton();
		$("#btnCreditClose").click(function() {UI_paxAccountCredit.closeOnclick();});
		
		$("#btnCreditReset").decorateButton();
		$("#btnCreditReset").click(function() {UI_paxAccountCredit.resetOnclick();});
		
		$("#btnCreditReInst").decorateButton();
		$("#btnCreditReInst").attr('style', 'width:auto;');
		$("#btnCreditReInst").click(function() {UI_paxAccountCredit.extendOnclick("REINSTATE");});
		
		$("#btnCreditExtend").decorateButton();
		$("#btnCreditExtend").attr('style', 'width:auto;');
		$("#btnCreditExtend").click(function() {UI_paxAccountCredit.extendOnclick("EXTEND");});
		$("#btnCreditExtend").hide();
		$("#txtCreditAmount").attr('readonly', false);
		
		$("#txtExpiryDate").datepicker({ minDate: new Date(), maxDate:'+2Y', yearRange:'-1:+2', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		$("#txtExpiryDate").blur(function() { UI_paxAccountCredit.dateOnBlur();});
		
		$('#txtCreditUN').keyup(function(){UI_paxAccountCredit.removeControlCharacters();});
		
		$("#btnCreditReInst").hide();
		$("#btnCreditExtend").hide();
		
		UI_paxAccountCredit.constructGrid({id:"#tblCredit"});
	}
	
	/*
	 * Payment Fill Data
	 */
	UI_paxAccountCredit.fillData = function(){		
		$("#divAvailCredit").html(jsCreditDetails.displayAvailableCredit +" "+ jsCreditDetails.currency);
		$("#divExpCredits").html(jsCreditDetails.displayExpiredCredit +" "+ jsCreditDetails.currency);
		$("#divActualCredit").html(jsCreditDetails.displayActualCredit +" "+ jsCreditDetails.currency);
		$("#divCreditAdjustCurr").html(jsCreditDetails.currency);
		UI_commonSystem.fillGridData({id:"#tblCredit", data:jsCreditDetails.credits});
	}
	
	/*
	 * Charges Validate
	 */
	UI_paxAccountCredit.validateEPay = function(){
		UI_message.initializeMessage();
		
		var amount;

		// get the credit amount from the selected row
		$('#tblCredit tr').each(function() {
		  
			if($(this).hasClass('ui-state-highlight')){
				
				amount = jsCreditDetails.credits[parseInt(this.id) -1].mcCreditAmount;
		    
			}
		    
		});
		
		//check if expired credit amount is lower than the re-instate amount
		if(amount < parseFloat($("#txtCreditAmount").val())){

			UI_message.showErrorMessage({messageText:"Insufficient priviledges: Re-instate allowed upto expired amount only."});
			$("#txtCreditAmount").focus();
			return false;

		}
		
		if ($("#tblCredit").getGridParam('selrow') == null){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-23"," a record")});
			return false;
		}
		
		/* Invalid Character Validations */
		if (!UI_commonSystem.validateInvalidCharWindow(jsonCreditsObj)){return false;}
		
		if (!UI_commonSystem.validateMandatoryWindow(jsonCreditsObj)){return false;}
		
		UI_paxAccountCredit.intCreditID = $("#tblCredit").getGridParam('selrow');
		return true;
	}
	
	/*
	 * User Notes Close
	 */
	UI_paxAccountCredit.closeOnclick = function(){
		UI_paxAccountDetails.close();
	}
	
	UI_paxAccountCredit.extendOnclick = function(inParams){
		UI_commonSystem.showProgress();
		if (UI_paxAccountCredit.validateEPay()){
			var data = {};
			data['groupPNR']  = opener.jsonGroupPNR;
			data['pnr']  = opener.jsonPNR;
			data['mode']  = inParams;
			data['status']  = opener.jsonBkgInfo.status;
			data['paxID'] = $("#selPax").val();
			data['resOnds']  = opener.UI_reservation.jsonONDs;
			data['resPaxs']  = opener.UI_reservation.jsonPassengers;
			data['resSegments']  = opener.UI_reservation.jsonSegs;		
			data['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
			$("#frmPaxAccount").action("paxAccountCreditsConfirm.action");
			$("#frmPaxAccount").ajaxSubmit({ dataType: 'json', processData: false, data:data,
											success: UI_paxAccountCredit.updateSuccess, error:UI_commonSystem.setErrorStatus});
		} else {
			UI_commonSystem.hideProgress();
		}
	}
	
	/*
	 * Reset On Click
	 */
	UI_paxAccountCredit.resetOnclick = function(){
		$("#frmPaxAccount").reset();
	}
	
	/*
	 * Payment Confirm Success
	 */
	UI_paxAccountCredit.updateSuccess = function(response){
		UI_commonSystem.hideProgress();
		if(response.success) {			
			jsPaxCharges = response.paxAccountChargesTO;
			jsPaymentDetails = response.paxAccountPaymentsTO;
			jsCreditDetails = response.paxAccountCreditTO;
			UI_paxAccountCharges.fillData();
			UI_paxAccountPayment.fillData();
			UI_paxAccountCredit.fillData();			
			UI_message.showConfirmationMessage({messageText:jsSuccessMsg.msgCredits});
			UI_paxAccountCharges.resetOnclick();
		} else {
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}
		$("#selPax").val(response.selPax);
		opener.UI_reservation.loadReservation("", null,false);
	}
	
	/*
	 * Auto Date generation
	 */
	UI_paxAccountCredit.dateOnBlur = function(){
		dateChk("txtExpiryDate");
	}
	
	
	UI_paxAccountCredit.fillCreditForm = function(rowid) {
		var ObjId = Number(rowid) - 1;
		$('#txtCreditAmount').val(jsCreditDetails.credits[ObjId].mcCreditAmount);
		
		/*
		 * Sets the credit extending date as one day ahead of current date. This is done 
		 * so a past date wont be set in the txtExpiryDate allowing credit to be extended to
		 * a past date.
		 */
		var expDate=new Date();
		expDate.setDate(expDate.getDate()+1);
		$('#txtExpiryDate').val($.datepicker.formatDate('dd/mm/yy', expDate)); //Format the date using jQuery date picker
		
		$('#txtCreditUN').val(jsCreditDetails.credits[ObjId].displayNotes);
		$('#txtCreditId').val(jsCreditDetails.credits[ObjId].displayCreditID);
		$('#txtTNXId').val(jsCreditDetails.credits[ObjId].displayTxnID);
		$('#txtCarrierCode').val(jsCreditDetails.credits[ObjId].carrierCode);
		$('#ocCreditAmount').val(jsCreditDetails.credits[ObjId].displayAmount.split(' ')[0]);
		if(jsCreditDetails.credits[ObjId].tnxStatus == "AVAILABLE"){
			$("#btnCreditExtend").hide();
			$("#btnCreditReInst").hide();
			if (DATA_ResPro.initialParams.allowPaxCreditExpiryDateChange) {
				$("#btnCreditExtend").show();
				$("#txtCreditAmount").attr('readonly', true);
			}
		}else {
			$("#btnCreditExtend").hide();
			$("#btnCreditReInst").hide();
			if (DATA_ResPro.initialParams.allowReinstatePassengerCredits) {
				$("#btnCreditReInst").show();
				$("#txtCreditAmount").attr('readonly', false);
			}
		}
		
	}
	
	/*
	 * Construct Grid
	 */
	UI_paxAccountCredit.constructGrid = function(inParams){
		// construct grid
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 280,
			width: 800,
			colNames:[getOpeneri18nData('PaxAccDetails_TaxId','Tax ID'),getOpeneri18nData('PaxAccDetails_Amount','Amount'),getOpeneri18nData('PaxAccDetails_ExpiryDate','Expiry Date'), getOpeneri18nData('PaxAccDetails_Status','Status'),getOpeneri18nData('PaxAccDetails_CarrierCode','Carrier Code')],
			colModel:[
				{name:'displayTxnID', index:'displayTxnID', width:80, align:"center"},
				{name:'displayAmount', index:'displayAmount', width:80, align:"right"},
				{name:'displayExpiryDate', index:'displayExpiryDate', width:100, align:"center"},
				{name:'tnxStatus',  index:'tnxStatus', width:50, align:"center"},
				{name:'carrierCode',  index:'carrierCode', width:50, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				UI_paxAccountCredit.fillCreditForm(rowid);
			}
		});
	}
	
	/**
	 * Remove Control characters because this causes problems when modifying the reservation
	 * */
	UI_paxAccountCredit.removeControlCharacters = function() {
		var str = $("#txtCreditUN").val();
		str = str.replace(/[\n\r]+/g, '');
		$("#txtCreditUN").attr("value", str);
	}
	/*
	 * ------------------------------------------------ end of File
	 * ----------------------------------------------
	 */