	function UI_MCODetails(){}
	
	/*
	 * Payment ready method
	 */
	
	UI_MCODetails.paxMCO = [];
	
	$(document).ready(function(){
		$("#btnMCOClose").decorateButton();
		$("#btnMCOClose").click(function() {UI_MCODetails.closeOnclick();});
		UI_MCODetails.onLoadCall();
		UI_MCODetails.constructGrid({id:"#tblMCODetails"});
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		$("#selPax").change(function() {UI_MCODetails.selPaxOnChange();});
		UI_MCODetails.selPaxOnChange();
	});
	
	/*
	 * On Load Call
	 */
	UI_MCODetails.onLoadCall = function(){
		$('#tblDetailsMCO').hide();
		$('#mcoDetailsHD').hide();
		UI_MCODetails.fillDropDown();
		$("#divPNR").html(opener.jsonBkgInfo.PNR);		
		$("#selPax").val(strPaxID).attr("selected", "selected");		
	}
	
	
	/*
	 * Fill Drop Down
	 */
	UI_MCODetails.fillDropDown = function(){
		var objAdtPax = opener.jsonPaxAdtObj.paxAdults;
		for (var i=0; i< objAdtPax.length; i++) {
			if(objAdtPax[i].displayAdultTitle != null){
				$("#selPax").append('<option value="' + objAdtPax[i].displayPaxTravelReference + '">' + objAdtPax[i].displayAdultTitle + '. ' + objAdtPax[i].displayAdultFirstName + ' ' + objAdtPax[i].displayAdultLastName + '</option>');
			}else{
				$("#selPax").append('<option value="' + objAdtPax[i].displayPaxTravelReference + '">' + objAdtPax[i].displayAdultFirstName + ' ' + objAdtPax[i].displayAdultLastName + '</option>');
			}
			
		}
		
		var objInfPax =  opener.jsonPaxAdtObj.paxInfants;
		for (var i=0; i< objInfPax.length; i++) {
				var infTitle  = "";
				if(objInfPax[i].displayInfantTitle != null){
					infTitle = objInfPax[i].displayInfantTitle + '. ';
				}
				$("#selPax").append('<option value="' + objInfPax[i].displayPaxTravelReference + '">' + infTitle + objInfPax[i].displayInfantFirstName + ' ' + objInfPax[i].displayInfantLastName + '</option>');						
		}
		$("#selPax").val(strPaxID).attr("selected", "selected");	
	}
	
	/*
	 * Payment Fill Data
	 */
	UI_MCODetails.fillData = function(){		
		UI_commonSystem.fillGridData({id:"#tblMCODetails", data:UI_MCODetails.paxMCO});
	}
	
	UI_MCODetails.closeOnclick = function(){
		window.close();
	}


	UI_MCODetails.fillMCODetails = function(rowid) {
		var ObjId = Number(rowid) - 1;
		$('#divService').html(UI_MCODetails.paxMCO[ObjId].service);
		$('#divMCONumber').html(UI_MCODetails.paxMCO[ObjId].mcoNumber);
		$('#divRemarks').html(UI_MCODetails.paxMCO[ObjId].remarks);
		$('#divAmount').html(UI_MCODetails.paxMCO[ObjId].amount);
		$('#divCurrencyCode').html(UI_MCODetails.paxMCO[ObjId].currencyCode);
		$('#divFlightNumber').html(UI_MCODetails.paxMCO[ObjId].flightNumber);
		$('#divStatus').html(UI_MCODetails.paxMCO[ObjId].status);
		$('#mcoDetailsHD').show();
		$('#tblDetailsMCO').show();		
	}
	
	UI_MCODetails.selPaxOnChange = function(){		
		var data = {};
		data['respaxs'] = opener.UI_reservation.jsonPassengers;
		data['paxID'] = $("#selPax").val();
		data['groupPNR'] = opener.jsonGroupPNR;
		data['status'] = opener.jsonBkgInfo.status;
		data['resSegments']  = opener.UI_reservation.jsonSegs;
		$('#tblDetailsMCO').hide();
		$('#mcoDetailsHD').hide();
		$("form").ajaxSubmit({ 
			dataType: 'json',
			processData: false,
			url: "loadPaxMCODetails.action",
			data: data,
			success: UI_MCODetails.selPaxChangeSuccess,
			error: UI_commonSystem.setErrorStatus
		});
		return false;
	}
	
	
	UI_MCODetails.selPaxChangeSuccess = function(response){
		UI_MCODetails.paxMCO = response.paxMCOs;
		UI_MCODetails.fillData();
	}
	/*
	 * Construct Grid
	 */
	UI_MCODetails.constructGrid = function(inParams){
		// construct grid
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 280,
			width: 800,
			colNames:["Service", "Electronic MCO Number", "Issued Date"],
			colModel:[
				{name:'service', index:'service', width:60, align:"center"},
				{name:'mcoNumber', index:'mcoNumber', width:120, align:"center"},
				{name:'issuedDate', index:'issuedDate', width:100, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				UI_MCODetails.fillMCODetails(rowid);
			}
		});
	}

	/*
	 * ------------------------------------------------ end of File
	 * ----------------------------------------------
	 */