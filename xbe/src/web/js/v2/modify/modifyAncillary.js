	/*
	*********************************************************
		Description		: XBE Modify Ancillary 
		Author			: Dilan Anuruddha
		Version			: 1.0v
		Created			: 05th August 2010
		Last Modified	: 05th August 2010
	*********************************************************	
	*/


	function UI_modifyAncillary(){}
	UI_modifyAncillary.tabStatus = ({tab0:true, tab1:false, tab2:false});		// tab Status
	UI_modifyAncillary.tabDataRetrive = ({tab0:false, tab1:true, tab2:false});	// Data Retrieval Status
	UI_modifyAncillary.selCurrency = "";
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapper").fadeIn("slow");
		$("#divBookingTabs").tabs();
		$('#divBookingTabs').bind('tabsbeforeactivate', function(event, ui) {
			return eval("UI_modifyAncillary.tabStatus.tab" + ui.newTab.index());
		});
		
		UI_commonSystem.strPGMode = null;//DATA_ResPro.modifyAncillary.mode; //TODO fill this up
		
		if(DATA_ResPro.initialParams.viewAvailableCredit) {
			UI_commonSystem.showAgentCredit();
			UI_commonSystem.showAgentCreditInAgentCurrency();
		} else {
			$("#divCreditPnl").hide();
		}
		var anciAvailResponse = DATA_ResPro.anciAvail;
		var fareQuoteSummary = DATA_ResPro.fareQuote;
		var contactInfo =DATA_ResPro.resContactInfo
		var paxArr = UI_modifyAncillary.transformPax(DATA_ResPro.resPaxs);
		paxArr = $.airutil.sort.quickSort(paxArr, UI_modifyAncillary.paxSorter);
		var infArr = [];
		var ninfArr = [];
		for(var i=0 ; i < paxArr.length; i++){
			if(paxArr[i].paxType == 'IN'){
				infArr[infArr.length] = paxArr[i];
			}else{
				ninfArr[ninfArr.length] = paxArr[i];
			}
		}
		anciAvailResponse.paxAdults = ninfArr;
		anciAvailResponse.paxInfants = infArr;
		anciAvailResponse.flightSegmentList = DATA_ResPro.flightRPHList;
		anciAvailResponse.contactInfo = contactInfo;
		UI_tabAnci.isAnciModify = true;
		UI_tabAnci.prepareAncillary(anciAvailResponse, fareQuoteSummary);
		UI_PageShortCuts.initilizeMetaDataForReservationAncillary();//AARESAA-4810 - Issue 13
		
		UI_commonSystem.strPGID = "SC_RESV_CC_006";  //TODO check this value is ok or not?
	});
	
	UI_modifyAncillary.paxSorter = function(first,second){
		return (first.paxId-second.paxId);
	}
	UI_modifyAncillary.transformPax = function(paxArr){
		var outArr = [];
		for(var i = 0 ; i < paxArr.length; i++){			
			var inPax = paxArr[i];
			var pax = {};
			if(inPax.infants!=null && inPax.infants.length>0){
				pax.displayInfantTravelling = 'Y';
				pax.displayInfantWith = inPax.infants[0].paxSequence - 1;
			}else{				
				pax.displayInfantTravelling = 'N';
				pax.displayInfantWith = '';
			}
			pax.displayAdultTitle = inPax.title;
			pax.displayAdultFirstName = inPax.firstName;
			pax.displayAdultLastName = inPax.lastName;
			pax.displayAdultType = inPax.paxType;
			pax.displayAdultNationality = inPax.nationalityCode;
			pax.currentAncillaries = inPax.selectedAncillaries;
			pax.displayAdultDOB = inPax.dateOfBirth;
			pax.travelerRefNumber = inPax.travelerRefNumber;
			pax.displayPnrPaxCatFOIDNumber = (inPax.pnrPaxCatFOIDNumber == undefined) ? null:inPax.pnrPaxCatFOIDNumber;
			pax.displayNationalIDNo = (inPax.nationalIDNo == undefined) ? null : inPax.nationalIDNo;
			pax.displayETicket = (inPax.eTicket == undefined) ? null:inPax.eTicket;
			pax.paxId = inPax.paxSequence-1;
			pax.seqNumber = inPax.paxSequence;
			pax.paxType = inPax.paxType;
			//pax.totalFare = inPax.totalFare;
			pax.totalPaidAmount = inPax.totalPaidAmount;
			pax.totalAvailableBalance = inPax.totalAvailableBalance;
			pax.totalPrice = inPax.totalPrice;
			
			if(inPax.lccClientAdditionPax != null && inPax.lccClientAdditionPax != undefined && inPax.lccClientAdditionPax != '' ){
				pax.displayVisaApplicableCountry = inPax.lccClientAdditionPax.visaApplicableCountry;
				pax.displayTravelDocType = inPax.lccClientAdditionPax.travelDocumentType;
				pax.displayVisaDocIssueDate = inPax.lccClientAdditionPax.visaDocIssueDate;
				pax.displayVisaDocNumber = inPax.lccClientAdditionPax.visaDocNumber;
				pax.displayVisaDocPlaceOfIssue = inPax.lccClientAdditionPax.visaDocPlaceOfIssue;
				pax.displayPnrPaxCatFOIDNumber = inPax.lccClientAdditionPax.passportNo;
				pax.displayPnrPaxCatFOIDExpiry = inPax.lccClientAdditionPax.passportExpiry;
				pax.displayPnrPaxCatFOIDPlace = inPax.lccClientAdditionPax.passportIssuedCntry;
				pax.displayPnrPaxPlaceOfBirth = inPax.lccClientAdditionPax.placeOfBirth;
			}
			//pax.totalSurcharge = inPax.totalSurcharge;
			//pax.totalTaxCharge = inPax.totalTaxCharge;
			outArr[i] = pax;
		}
		return outArr;
	}
	
	/*
	 * Open Tab from the Function Call
	 */
	UI_modifyAncillary.openTab = function(intIndex){
		$('#divBookingTabs').tabs("option", "active", intIndex); 	
	}
	
	/*
	 * Lock Tabs
	 */ 
	UI_modifyAncillary.lockTabs = function(){
		UI_modifyAncillary.tabStatus = ({tab0:true, 
										tab1:false});
		
		UI_modifyAncillary.tabDataRetrive = ({tab0:false, 
										tab1:true});
	}
	
	/*
	 * Capture Payment 
	 */
	UI_modifyAncillary.confirmAncillaryModification = function(){
		UI_commonSystem.showProgress();
		var data = {};
		data['pnr'] = DATA_ResPro.pnrNo;
		data['groupPNR'] = DATA_ResPro.groupPNRNo;
		data['selCurrency'] = DATA_ResPro.selCurrency;		
		data['flightRPHList']= $.toJSON(DATA_ResPro.flightRPHList);
//		data['resPaxs']= $.toJSON(DATA_ResPro.resPaxs);
		data['resPaxPayments'] = $.toJSON(DATA_ResPro.resPaxWisePayments);
		data['version']= DATA_ResPro.version;		
		data['selectedAncillaries']=$.toJSON(jsAnciSeg);
		data['paxWiseAnci'] = $.toJSON(UI_tabAnci.generatePaxWiseAnci());
		data['insurances'] =  $.toJSON(UI_tabAnci.anciInsurances);
		data['insurableFltRefNumbers'] = $.toJSON(UI_tabAnci.insurableFltRefNumbers);
		data['resPaySummary'] =  $.toJSON(DATA_ResPro.resPaySummary);
		data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
		//var fareQuoteData = UI_tabSearchFlights.createFareQuoteParams();					
		//data = $.airutil.dom.concatObjects(data,fareQuoteData);	
		$("#frmMakeBkg").action("modifyAnciConfirm.action");
		$("#frmMakeBkg").ajaxSubmit({ dataType: 'json', processData: false, data:data,
										success: UI_modifyAncillary.continueOnClickSuccess,
										error:UI_commonSystem.setErrorStatus});
	}
	
	UI_modifyAncillary.showConfirmError = function(resMessage){
		showERRMessage(resMessage);
	}
	
	/*
	 * Continue On Click success
	 */
	UI_modifyAncillary.continueOnClickSuccess = function(response){
		//if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		
		// prepare the next tab 
		
		UI_tabPayment.isAddModifyAnci = true;
		UI_tabAnci.isAddModifyAnci  = true;
		UI_tabAnci.modifiedSegemntCount = response.modifedSegmentsCount; 
		
		if (response != null){
			if(!response.success){
				showERRMessage(response.messageTxt);
				UI_commonSystem.hideProgress();
				return false;
			}
			if(response.blnNoPay){
				UI_modifyAncillary.loadHomePage();
				UI_commonSystem.hideProgress();
				return false;
			}
			UI_commonSystem.hideProgress();
			UI_tabPayment.ready();
			UI_tabPayment.updatePayemntTabTO(response);
			UI_modifyAncillary.tabDataRetrive.tab1 = false;
			UI_modifyAncillary.tabStatus.tab1 = true;
			UI_modifyAncillary.openTab(1);
			UI_tabPayment.paymentDetailsOnLoadCall();
		}
		UI_commonSystem.hideProgress();
	}
	
	/*
	 * Load modify segment source page
	 */
	UI_modifyAncillary.loadHomePage = function(){
		UI_modifyAncillary.selCurrency = "";
		location.replace("showNewFile!loadRes.action?pnrNO=" + DATA_ResPro.pnrNo+"&groupPNRNO="+DATA_ResPro.groupPNRNo);
	}
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	