	/*
	*********************************************************
		Description		: XBE Modify Reservation 
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/
					
	var jsonPaxTypes = "";									
	var jsonNationality = "";
	var jsonTravellingWith = "";   // Traveling with Drop Down Data	
	var jsonSystem = "";
	UI_reservation.jsonTaxReversalData =null;
	UI_reservation.jsonSegs = null;
	UI_reservation.jsonPassengers = null;
	UI_reservation.jsonPassengerTktCoupons = null;
	UI_reservation.jsonONDs = null;
	UI_reservation.jsonAlerts = null;
	UI_reservation.jsonAutoCnxAlert = null;
	UI_reservation.jsonPromotionInfo = null;
	UI_reservation.applicableBINs = null;
	UI_reservation.isCreditPromotion = false;
	UI_reservation.refreshResponse = null;
	UI_reservation.refreshAction = "";
	UI_reservation.loadDisp = false;
	UI_reservation.adultCount = 0;
	UI_reservation.childCount = 0;
	UI_reservation.infantCount = 0;
	UI_reservation.payments = null;
	UI_reservation.ownerAgent = "";
	UI_reservation.contactInfo = null;
	UI_reservation.paySummary = null;
	UI_reservation.jsonFlexiAlerts = null;
	UI_reservation.balnceTOPay = 0; // keeping seperate as paymentto keeps changing
	UI_reservation.OhdPayAmount = ""; // 
	UI_reservation.currencyExRate = null; // 
	UI_reservation.originSalesChanel = ""; // 
	UI_reservation.jsonOpenReturnSegInfo = null; // loads open return segment info
	UI_reservation.editFFID = false;
	UI_reservation.addFFID = false;
	UI_reservation.jsonAdditionalSegInfo = null; // loads addition segment info
 
	UI_reservation.allFlightDetails = null;//holds the jsonFlightDetails
	UI_reservation.selectedFlightSegRef = null;

	UI_reservation.actualOrigin = "";
	UI_reservation.actualDestination = "";// this two variable will have the values only if pax country config enabled
	
	UI_reservation.ohdRollForwardMinDate = null;
	UI_reservation.ohdRollForwardValidation = {allow: false, errorCode: ""};
	UI_reservation.jsonRefundableChargeDetails = null;
	UI_reservation.refundableChargeDetails = null;
	UI_reservation.perPaxPaymentDetails = null;
	UI_reservation.perPaxCreditDetails = null;
	UI_reservation.allowCSVDetailUpload = false;
	UI_reservation.allowCreateUserAlert = false;
	UI_reservation.isGroupBooking = false;
	UI_reservation.countryCurrExchangeRate = null;
	UI_reservation.isAllowNameChange = false;
	UI_reservation.ohdReleaseTime = "";  
	UI_reservation.gdsId = "";
	UI_reservation.isCsOCPnr = false;
	UI_reservation.isAllowRequoteOverride = false;
	UI_reservation.isAllowExtendedNameChange = false;
	UI_reservation.isInfantPaymentSeparated = false;
	
	UI_reservation.flightSegmentToList = [];
	UI_reservation.paxWiseAnci = "";
	
	/*
	 * Make Reservation related Methods
	 */
	function UI_reservation(){}
	UI_reservation.tabStatus = ({tab0:true, tab1:true, tab2:false, tab3:true, tab4:true});		// tab Status
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapper").hide();		
		UI_reservation.loadReservation("", null, true);		
		
	});
	
	UI_reservation.loadReservation = function(action, response, loadAudit) {

		if(DATA_ResPro.initialParams.restrictModForCnxFlown && action == "CNXRES") {
			UI_tabBookingInfo.cancelSuccessUpdate(response);
			UI_reservation.refreshAction = "";
			if(jsonGroupPNR != undefined && jsonGroupPNR != "" && jsonGroupPNR != "null"){
				top.blnInterLine = true;
			}
			top.loadNewModifyBooking(jsonPNR, false);
		} else {
			UI_reservation.refreshResponse = response;
			UI_reservation.refreshAction = action;
			UI_reservation.loadDisp = loadAudit;
			var data = {};
			data['groupPNR'] = jsonGroupPNR;
			data['pnr'] = jsonPNR;
			data['loadAudit'] = loadAudit;
			data['marketingAirlineCode'] = jsonMarketingAirlineCode;
			data['airlineCode'] = jsonAirlineCode;
			data['interlineAgreementId'] = jsonInterlineAgreementId;
			UI_commonSystem.showProgress();
			$("#frmTabLoad").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"loadReservationShowData.action",							
				success: UI_reservation.loadDataSuccess ,error:UI_commonSystem.setErrorStatus});
		}
		
	}
	
	UI_reservation.displayReservation = function() {
		$("#divLegacyRootWrapper").fadeIn("fast");
		$("#divBookingTabs").tabs();
		//initilizing shortcut keys for booking tab because it's first loaded
		UI_PageShortCuts.initilizeMetaDataForModoficationBookingTab();
		$('#divBookingTabs').bind('tabsbeforeactivate', UI_reservation.selectTab);
	}
	
		UI_reservation.loadDataSuccess = function(response) {
	
			if(response.success && typeof(DATA_ResPro.initialParams) != "undefined" && DATA_ResPro.initialParams.restrictModForCnxFlown 
					&& (DATA_ResPro.initialParams.reservationModificationsAllowed != response.rParam.reservationModificationsAllowed)
					&& UI_reservation.refreshAction == "SPLIT") {		
				showCommonError("CONFIRMATION", UI_reservation.refreshResponse.newPNRNoMsg);
				setTimeout(function(){ 
					top.loadNewModifyBooking(jsonPNR, false);}, 3000);
			} else if(response.success && typeof(DATA_ResPro.initialParams) != "undefined" 
					&& UI_reservation.refreshAction == "TAXREV") {
				showCommonError("CONFIRMATION","No Show Tax reversed Successfully");
				setTimeout(function(){ 
					top.loadNewModifyBooking(jsonPNR, false);}, 3000);
				
			} else {
				UI_commonSystem.hideProgress();
				if(response.success){	
					UI_tabAnciInfo.resetData();	
					DATA_ResPro["initialParams"] = response.rParam;
					isAllowVoidReservation = response.allowVoidReservation;
					jsonGroupPNR = response.groupPNR;					
					isGdsPnr = response.gdsPnr;
					isGdsAllowAncillary = response.gdsAllowAncillary;
					jsonSystem = response.system;
					jsonPaxWiseAnci	= response.paxWiseAnci;
					jsonPaxWiseAnciForMod = response.paxWiseAnci;
					jsonInsurances	= response.insurances;
					jsonContactInfo = response.contactInfo;
					jsonPaxPrice 	= response.paxPrice;
					jsonGoQuoServicesAsSSR = response.userDefinedSSR;
					jsonFltDetails 	= response.flightDetails;
					jsonBkgInfo 	= response.bookingInfo;
					jsonUserNotes 	= response.userNotes;
					jsonPaxPriceTo 	= response.paxPaymentTO;
					jsonFirstFlightDate 	= response.firstFlightDepartDate;
					jsonLastFlightArrivalDate = response.lastFlightArrivalDate;
					jsonLastFlightDate 	= response.lastFlightDepartDate;
					jsonAlertDetails		= response.alertInfo;	
					jsonFlexiDetails		= response.flexiAlertInfo;
					jsonEndorsements = response.endorsementTxt;
					jsonSegmentWiseBundledFareDetails  = response.segWiseBindleAlertInfo;
					jsonPaxPriceSummary 		= response.paxPaymentSummary;		
					jsonResPaymentSummary		= response.paymentSummary;		
					jsonPaxAdtObj.paxAdults 	= response.paxAdults;
					jsonPaxAdtObj.paxInfants 	= response.paxInfants;
					jsonCarrierOptions  = 	response.carrierGroupingOptions;
					serviceTaxApplicableCountries = response.serviceTaxApplicableCountries;
					hasServiceTaxApplied = response.taxInvoices != undefined && response.taxInvoices != null
						&& response.taxInvoices.length != 0;
					
					UI_reservation.jsonTaxReversalData = response.taxReversalDto;
					UI_reservation.firstDepDatetime = response.firstFlightDepartDateTime;
					UI_reservation.totalSegmentCount = response.noOfCnfSegments;
					UI_reservation.flightSegmentToList = response.flightSegmentToList;
					UI_reservation.paxWiseAnci = response.paxWiseAnci;
					UI_reservation.jsonSegs = $.toJSON(response.xbeData.pnrSegments);
					UI_reservation.jsonPassengers = $.toJSON(response.xbeData.pnrPaxs);
					UI_reservation.perPaxPaymentDetails = response.perPaxPaymentDetails;
					UI_reservation.perPaxCreditDetails = response.perPaxCreditDetails;
					UI_reservation.jsonPassengerTktCoupons = $.toJSON(response.xbeData.passengerTktCoupons);
					UI_reservation.jsonONDs = 	$.toJSON(response.xbeData.pnrOndGroups);
					UI_reservation.jsonAlerts = response.xbeData.pnrAlerts;	
					UI_reservation.jsonAutoCnxAlert = response.xbeData.autoCancellationAlert;
					UI_reservation.jsonPromotionInfo = response.xbeData.promotionAlerts;
					UI_reservation.applicableBINs = response.applicablBINs;
					UI_reservation.isCreditPromotion = response.xbeData.creditPromotion;
					UI_reservation.adultCount = response.xbeData.noOfAdults;
					UI_reservation.childCount = response.xbeData.noOfClidren;
					UI_reservation.infantCount = response.xbeData.noOfInfants;
					UI_reservation.payments = 	response.xbeData.paymentSummary;
					UI_reservation.ownerAgent = response.xbeData.ownerAgentCode;
					UI_reservation.taxInvoices = response.taxInvoices;
					UI_reservation.contactInfo = $.toJSON(response.xbeData.contactInfo);
					UI_reservation.paySummary = response.xbeData.paymentSummary;
					UI_reservation.jsonFlexiAlerts = response.xbeData.flexiPnrAlerts;
					UI_reservation.balnceTOPay = jsonResPaymentSummary.balanceToPay;
					UI_reservation.OhdPayAmount = response.ohdAmount;
					UI_reservation.currencyExRate = response.currencyExchangeRate;
					UI_reservation.originSalesChanel = response.originSalesChanel;	
					UI_reservation.actualOrigin = response.actualOrigin;
					UI_reservation.actualDestination = response.actualDestination;
					UI_reservation.ohdRollForwardMinDate = response.ohdRollForwardStartDate;
					
					UI_reservation.ohdRollForwardValidation.allow = response.allowOnholdRollFoward;
					UI_reservation.ohdRollForwardValidation.errorCode = response.rollForwardErrorCode;
					UI_reservation.allowCSVDetailUpload = response.allowCSVDetailUpload;
					UI_reservation.allowCreateUserAlert = response.allowCreateUserAlert;
					UI_reservation.isGroupBooking = response.groupBookingRequest;
					UI_reservation.countryCurrExchangeRate = response.countryCurrExchangeRate;
					UI_reservation.isAllowNameChange = response.allowNameChange;
					if (response.rParam.postDepature){
						if (response.rParam.allowPassengerNameModificationHavingDepatedSegments && response.allowExtendedNameChange){
							UI_reservation.isAllowExtendedNameChange = response.allowExtendedNameChange;
						}
					} else {
						UI_reservation.isAllowExtendedNameChange = response.allowExtendedNameChange;
					}
					UI_reservation.isAllowRequoteOverride = response.allowRequoteOverride;
					UI_reservation.ohdReleaseTime = jsonBkgInfo.displayStatus.substring(18,34);  
					UI_reservation.isGdsPnr = response.gdsPnr;
					isCsPnr = response.csPnr;
					UI_reservation.gdsId = response.gdsId != null ? response.gdsId : "";
					UI_reservation.isCsOCPnr = response.csOCPnr;
					UI_reservation.addFFID = response.addFFID;
					UI_reservation.editFFID = response.editFFID;
					
					/* dry reservation loading related params. could be ignored safely for all other scenarios 
					UI_reservation.isDryReservation = response.dryReservation;
					UI_reservation.marketingAirlineCode = response.marketingAirlineCode;
					UI_reservation.airlineCode = response.airlineCode;
					UI_reservation.interlineAgreementId = response.interlineAgreementId;
					*/
					
					UI_reservation.jsonAdditionalSegInfo = response.xbeData.additionalSegInfo;
					jsonAdditionalSegInfo = response.xbeData.additionalSegInfo;
		 
					UI_reservation.allFlightDetails = response.flightDetails;
					UI_reservation.jsonRefundableChargeDetails = response.refundableChargeDetails;
					UI_reservation.refundableChargeDetails = $.parseJSON(UI_reservation.jsonRefundableChargeDetails);
					jsonETicketMaskObj 	= $.toJSON(response.paxETicketMasks);
				
				if(DATA_ResPro.initialParams.viewAvailableCredit) {
					UI_commonSystem.showAgentCredit();
					UI_commonSystem.showAgentCreditInAgentCurrency();
				} else {
					$("#divCreditPnl").hide();
				}
				/** Open return alerts */
	 			UI_reservation.jsonOpenReturnSegInfo =  response.xbeData.openReturnSegInfo;
	 			jsonOpenReturnSegInfo = response.xbeData.openReturnSegInfo;
				
				if(UI_reservation.loadDisp){
					UI_reservation.displayReservation();
				}
				
				/* Booking Info */
				UI_tabBookingInfo.ready();		
				
				//parent.hideProgress();		
				UI_commonSystem.strPGID = "SC_RESV_CC_009";
				
				if(UI_reservation.refreshResponse != null){
					UI_reservation.refreshResponse.agentBalance = response.agentBalance;
				}
				if(DATA_ResPro.initialParams.cancelReservation){
					$("#btnCancel").show();
					$("#btnCancel").enable();							
				} else {
					$("#btnCancel").hide();
				}
		
				UI_reservation.isInfantPaymentSeparated = response.infantPaymentSeparated;
				UI_reservation.isServiceTaxApplied = response.serviceTaxApplied;
				UI_reservation.isHandlingFeeApplicableForAdj = response.handlingFeeApplicableForAdj;
				top.gstAppliedForReservation = response.serviceTaxApplied;
				//parent.getFieldByID("divPane").style.display = "none";
			} else {
				showERRMessage(response.messageTxt);
			}
			if(UI_reservation.refreshAction != ""){
				UI_reservation.refreshReservation(UI_reservation.refreshAction);
				UI_reservation.refreshAction = "";
			}		
		}
	}
	
	UI_reservation.allowConfirmOnNoPayOHD = function(){
		if(UI_reservation.balnceTOPay>0){
			return false;
		}
		var paxs = $.parseJSON(UI_reservation.jsonPassengers);
		for(var i=0; i < paxs.length; i++){
			if(paxs[i].status!='OHD'){
				return false;
			}
		}
		return true;
	}
	
	UI_reservation.selectTab = function(event, ui) {
		
		switch (ui.newTab.index()){
		case 0 :
			UI_PageShortCuts.initilizeMetaDataForModoficationBookingTab();
			UI_commonSystem.pageOnChange();
			UI_tabBookingInfo.ready();	
			break;
		case 1 : 
			/* Contact Info */
			UI_PageShortCuts.initilizeMetaDataForModoficationContactDetailsTab();
			UI_commonSystem.pageOnChange();
			UI_tabContactInfo.ready();					 
			break;		
		case 2 :
			/* Payment Tab */
				if(!UI_reservation.isCsOCPnr){
					if(UI_reservation.balnceTOPay > 0 || UI_reservation.allowConfirmOnNoPayOHD()) {
						UI_PageShortCuts.initilizeMetaDataForModificationPaymentTab();
						UI_commonSystem.pageOnChange();
						UI_tabPayment.ready();
						UI_tabPayment.isModify = true;
						currencyExchangeRate = UI_reservation.currencyExRate;
						countryCurrExchangeRate = UI_reservation.countryCurrExchangeRate;
						UI_tabPayment.paymentModifyDetailsOnLoadCall();
						UI_reservation.tabStatus.tab2 = true;
					      if (typeof (isCsPnr) != 'undefined') {
							if (isCsPnr) {
								$("#btnPayForce").hide();
							}
						}
					}else {
						if(UI_reservation.balnceTOPay > 0 || UI_reservation.allowConfirmOnNoPayOHD()) {
							UI_PageShortCuts.initilizeMetaDataForModificationPaymentTab();
							UI_commonSystem.pageOnChange();
							UI_tabPayment.ready();
							UI_tabPayment.isModify = true;
							currencyExchangeRate = UI_reservation.currencyExRate;
							UI_tabPayment.paymentModifyDetailsOnLoadCall();
							UI_reservation.tabStatus.tab2 = true;
						}else {
							alert(geti18nData('xbeError_NoPaymentToMake','No Payment to Make'));
							UI_reservation.tabStatus.tab2 = false;
						}
					}
				} else {
					alert("Insufficient privileges to make payments.");
					UI_reservation.tabStatus.tab3 = false;
					break;
				}
			break;	
		case 3 :
			if (DATA_ResPro.initialParams.viewReservationAuditAllowed) {
				/* History */
				UI_PageShortCuts.initilizeMetaDataForModoficationHistoryTab();
				UI_commonSystem.pageOnChange();
				UI_tabHistoryInfo.ready();	
				break;	
			} else {
				alert("Insufficient privileges to view reservation audit");
				UI_reservation.tabStatus.tab3 = false;
				break;
			}
		case 4 :
			/* Ancillary */
			var hasAnci =UI_reservation._anciAvailCheck();
			
			if(hasAnci){
				UI_PageShortCuts.initilizeMetaDataForModificationAncillary();
				UI_commonSystem.pageOnChange();
				UI_tabAnciInfo.ready();	
				UI_reservation.tabStatus.tab4 = true;
			}else{				
				//if((jsonGroupPNR!=null && $.trim(jsonGroupPNR)!='')){
				//	alert('Ancillaries are not selected');
				//}else 
				if (!DATA_ResPro.initialParams.reservationModificationsAllowed
					&& !DATA_ResPro.initialParams.gdsReservationModAllowed) {
					alert("Insufficient privileges to view Ancillary Details");
					UI_reservation.tabStatus.tab4 = false;
					break;
				}
				if(confirm('Ancillaries are not selected. Would you like to add ancillaries now?')){
					UI_tabAnciInfo.editAncillary();
				}
				UI_reservation.tabStatus.tab4 = false;
			}
			break;
		}
		return eval("UI_reservation.tabStatus.tab" + ui.newTab.index());
	}

	UI_reservation._anciAvailCheck = function(){
		var hasAnci = false;
		for(var i = 0 ; i < jsonPaxWiseAnci.length ; i++){
			if(jsonPaxWiseAnci[i].selectedAncillaries.length>0){
				hasAnci = true;
				break;
			}
		}
		if(!hasAnci && UI_reservation._hasInsurance()){
			hasAnci = true;
		}
		return hasAnci;
	}
	
	UI_reservation._hasInsurance = function () {
		var hasIns = false;
		if(jsonInsurances!=null && jsonInsurances.length > 0 ){
			$.each(jsonInsurances, function (key, insObject){
				var insRefNo = insObject.insuranceQuoteRefNumber;
				if(insRefNo != null && insRefNo != ''){
					hasIns = true;
					return;
				 }
			});
		}
		return hasIns;
	}
	/*
	 * Open Tab from the Function Call
	 */
	UI_reservation.openTab = function(intIndex){
		$('#divBookingTabs').tabs("option", "active",  intIndex); 	
	}
	
	/*
	 * Reservation Data Refresh with the loaded data
	 */
	UI_reservation.refreshReservation = function(action){
		UI_commonSystem.hideProgress();		
		switch (action) {
			case "SAVEINF":
				if(UI_reservation.refreshResponse.success && UI_reservation.refreshResponse.paysuccess){
					UI_tabPayment.isAddInfant = false;
					UI_tabPayment.cancelCROnClick();
					UI_tabPayment.ccEnableTab(true);
					UI_reservation.openTab(0);
					showCommonError("CONFIRMATION", "Infant Added Successfully");
					
				}else {
					UI_tabBookingInfo.saveInfantSuccessUpdate(UI_reservation.refreshResponse);
				}
				
				break;
			case "SPLIT":
				UI_tabBookingInfo.splitSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "TRANSFER":
				UI_tabBookingInfo.transferSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "EXTEND":
				UI_tabBookingInfo.extendSuccessUpdate(UI_reservation.refreshResponse); 
				break;
			case "MODCNT":
				UI_tabBookingInfo.modifyCNTSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "MODCONTINFO":
				UI_tabContactInfo.saveContactSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "CNXRES":
				UI_tabBookingInfo.cancelSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "REMPAX":
				UI_tabBookingInfo.removePaxSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "CNXSEG":
				UI_tabBookingInfo.cancelSegmentSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "CLRALT":
				UI_tabBookingInfo.clearAlertSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "BALPAY":
				UI_tabPayment.resetCCOnClick();
				showCommonError("CONFIRMATION", "Payment Made Successfully");
				$("#selExtend").val('');
				UI_reservation.openTab(0);
				break;
			case "NOPAY":
				UI_tabPayment.resetCCOnClick();
				showCommonError("CONFIRMATION", "Booking is successfully force confirmed");
				UI_reservation.openTab(0);
				break;	
			case "CRTALT":
				UI_tabBookingInfo.createAlertSuccessUpdate(UI_reservation.refreshResponse);
				break;
			case "PRIORITY":
				UI_tabBookingInfo.changePrioritySuccessUpdate(UI_reservation.refreshResponse); 
				break;
			default :
				break;
		}
		
		//Refresh logged in agent balance for every refresh operation.
		if(UI_reservation.refreshResponse != null && typeof UI_reservation.refreshResponse.agentBalance !== "undefined"){
			top.intAgtBalance = UI_reservation.refreshResponse.agentBalance;
		}
		if(DATA_ResPro.initialParams.viewAvailableCredit) {
			UI_commonSystem.showAgentCredit();
			UI_commonSystem.showAgentCreditInAgentCurrency();
		} else {
			$("#divCreditPnl").hide();
		}
	}
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
