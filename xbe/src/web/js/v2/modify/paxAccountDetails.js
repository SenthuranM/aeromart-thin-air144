	/*
	*********************************************************
		Description		: XBE - Pax Account Details
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 11th Jan 2010
	*********************************************************	
	*/
		
	var jsSuccessMsg = ({
							msgCharges:"Adjust Credit Success",
							msgPayment:"Payments successfully updated",
							msgExternalPay:"Record successfully updated",
							msgCredits:"Record successfully updated"
						});
	
	var jsAdjustmentTypesMap;

	/*
	 * Passenger Tab
	 */
	function UI_paxAccountDetails(){}
	
	/*
	 * Contact Info Tab Page On Load
	 */
	$(document).ready(function(){
		if(!opener.DATA_ResPro.initialParams.viewExternalPayments){
			$("#hrefExternal").disable();
			$("#divAccExternal").hide();
		}
		if(!opener.DATA_ResPro.initialParams.viewCredit){
			$("#hrefCredit").disable();
			$("#divAccCredit").hide();
		}
		
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		$("#divAccountTabs").tabs();		
		$('#divAccountTabs').bind('tabsbeforeactivate', function(event, ui) {
			UI_message.initializeMessage();
			switch (ui.newTab.index()){
				case 0 :
					break;
				case 1 :
					break;
				case 2 :
					if(opener.jsonBkgInfo.groupPNR){
						$("#divAccountTabs").tabs('disable',2);//Temporary disable External payments tab for lcc
						return false;
					}
					break;
				case 3 :
					break;
				default :
					return true;
			}
			return true;
		});
		/* Charges Tab */
		UI_paxAccountCharges.ready();
		
		/* Payment Tab */
		UI_paxAccountPayment.ready();
		
		/* External Pay Tab */
		UI_paxAccountEPay.ready();
		
		/* Credits Tab */
		UI_paxAccountCredit.ready();
		
		UI_paxAccountDetails.onLoadCall();
		$('#resONDs').val(opener.UI_reservation.jsonONDs);
		$("#selPax").change(function() {UI_paxAccountDetails.selPaxOnChange();});
		$("#selAirlineOnd").change(function() {UI_paxAccountDetails.selAirlineOndOnChange();});
		$("#selAdjustmentType").change(function() {UI_paxAccountDetails.selAdjustmentTypeOnChange();});
		
		UI_paxAccountDetails.selPaxOnChange();
		
	});
	
	/*
	 * On Load Call
	 */
	UI_paxAccountDetails.onLoadCall = function(){
		UI_paxAccountDetails.fillDropDown();
		$("#divPNR").html(opener.jsonBkgInfo.PNR);		
		$("#selPax").val(strPaxID).attr("selected", "selected");		
	}
	
	/*
	 * Pax Drop down On Change
	 */
	UI_paxAccountDetails.selPaxOnChange = function(){		
		var data = {};
		data['respaxs'] = opener.UI_reservation.jsonPassengers;
		data['paxID'] = $("#selPax").val();
		data['pnr'] = opener.jsonPNR;
		data['groupPNR'] = opener.jsonGroupPNR;
		data['status'] = opener.jsonBkgInfo.status;
		data['resSegments']  = opener.UI_reservation.jsonSegs;
		data['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
		$("#frmPaxAccount").action("loadPaxAccountDetails.action");
		$("#frmPaxAccount").ajaxSubmit({ dataType: 'json', processData: false, data:data,
										success: UI_paxAccountDetails.selPaxChangeSuccess, error:UI_commonSystem.setErrorStatus});
		return false;
	}
	
	UI_paxAccountDetails.selAirlineOndOnChange = function(){
		//remove the current options as we are changing the OND.
		$("#selAdjustmentType").children().remove();
		$("#selChargeType").children().remove();
		var selectedSegmentRPH = $("#selAirlineOnd").val();
		var jsonSegs = jQuery.parseJSON(opener.UI_reservation.jsonONDs);
		jQuery.each(jsonSegs, function() {
 			if (this.carrierOndGroupRPH == selectedSegmentRPH){
				UI_paxAccountDetails.fillAdjustmentTypeSelection(this.carrierCode);
				return false;
			}
		});
		UI_paxAccountDetails.fillRefundNonRefundOptions();
	}
	
	UI_paxAccountDetails.selAdjustmentTypeOnChange = function(){
		$("#selChargeType").children().remove();
		UI_paxAccountDetails.fillRefundNonRefundOptions();		
	}
	
	/*
	 * Ajax Return Call for passenger on change
	 */
	UI_paxAccountDetails.selPaxChangeSuccess = function(response){
		jsPaxCharges = response.paxAccountChargesTO;
		jsPaymentDetails = response.paxAccountPaymentsTO;
		jsExternalPayments = response.paxAccountExternalPaymentTO;
		jsCreditDetails = response.paxAccountCreditTO;
		$("#selAirlineOnd").children().remove();
		$("#selAirlineOnd").append(response.segAdjustOption);
		UI_paxAccountDetails.disableChargeAdjutmentInputs(response.segAdjustOption);
		
		$("#selAdjustmentType").children().remove();
		jsAdjustmentTypesMap = response.chargeAdjustmentTypes;
		
		$("#selChargeType").children().remove();
		UI_paxAccountCharges.fillData();
		UI_paxAccountPayment.fillData();
		UI_paxAccountCredit.fillData();
		
//		UI_paxAccountPayment.loadNoShowSegments($('#selPax').val());
	}
	
	/*
	 * Close Window
	 */
	UI_paxAccountDetails.close = function(){
		UI_paxAccountDetails.disableAdjutmentInputs(false);
		window.close();
	}
	// ----------------------------------------------------------------
	
	/*
	 * Fill Drop Down
	 */
	UI_paxAccountDetails.fillDropDown = function(){
		var objPax = opener.jsonPaxAdtObj.paxAdults;
		for (var i=0; i< objPax.length; i++) {
			if(objPax[i].displayAdultTitle != null){
				$("#selPax").append('<option value="' + objPax[i].displayPaxTravelReference + '">' + objPax[i].displayAdultTitle + '. ' + objPax[i].displayAdultFirstName + ' ' + objPax[i].displayAdultLastName + '</option>');
			}else{
				$("#selPax").append('<option value="' + objPax[i].displayPaxTravelReference + '">' + objPax[i].displayAdultFirstName + ' ' + objPax[i].displayAdultLastName + '</option>');
			}
			
		}	
		if(opener.UI_reservation.isInfantPaymentSeparated){
			objPax = opener.jsonPaxAdtObj.paxInfants;
		
			for (var i=0; i< objPax.length; i++) {
				if(objPax[i].displayInfantTitle != null){
					$("#selPax").append('<option value="' + objPax[i].displayPaxTravelReference + '">' + objPax[i].displayInfantTitle + '. ' + objPax[i].displayInfantFirstName + ' ' + objPax[i].displayInfantLastName + '</option>');
				}else{
					$("#selPax").append('<option value="' + objPax[i].displayPaxTravelReference + '">' + objPax[i].displayInfantFirstName + ' ' + objPax[i].displayInfantLastName + '</option>');
				}
			
			}	
		}		
		//$("#selAgents").fillDropDown( { dataArray:opener.top.arrTravelAgents , keyIndex:0 , valueIndex:2, firstEmpty:true });
	}

	
	UI_paxAccountDetails.fillAdjustmentTypeSelection = function(carrierCode){
		var adjustmentTypes;
		jsAdjustmentTypesMap
		for (var i=0; i< jsAdjustmentTypesMap.length; i++) {
			if (jsAdjustmentTypesMap[i].carrierCode == carrierCode){
				adjustmentTypes = jsAdjustmentTypesMap[i].chargeAdjustmentTypes;
				break;
			}
		}
		for (var i=0; i< adjustmentTypes.length; i++) {
			if (UI_paxAccountDetails.validateChargeAdjustmentType(adjustmentTypes[i], carrierCode) ) {
				$("#selAdjustmentType").append('<option value="' + adjustmentTypes[i].chargeAdjustmentTypeId + '">' + adjustmentTypes[i].chargeAdjustmentTypeName + '</option>');
			}					
		}
	}
	
	UI_paxAccountDetails.validateChargeAdjustmentType = function(adjustmentType, carrierCode) {
		var adjustmentPrivilege = UI_paxAccountDetails.getSelectedAdjustmentPrivileges(carrierCode, adjustmentType.chargeAdjustmentTypeId);
		var refundAllowed = false;
		var nonRefundAllowed = false;
		
		if (adjustmentType.refundableChargeCode != null &&
				adjustmentType.refundableChargeCode.length > 0 && adjustmentPrivilege.refundable == true) {				
			refundAllowed = true;
		}
		
		if (adjustmentType.nonRefundableChargeCode != null && 
				adjustmentType.nonRefundableChargeCode.length > 0 && adjustmentPrivilege.nonRefundable == true) {				
			nonRefundAllowed = true
		}	
		
		if (refundAllowed || nonRefundAllowed) {
			return true;			
		} else {
			return false;
		}
	}
	
	UI_paxAccountDetails.fillRefundNonRefundOptions = function(){
		
		var selectedCarrierCode = UI_paxAccountDetails.getSelectedSegmentCarrierCode();
		var selectedAdjustmentTypeId = $("#selAdjustmentType").val();
		
		if (selectedAdjustmentTypeId != null && selectedAdjustmentTypeId != undefined 
				&& selectedCarrierCode != null && selectedCarrierCode != undefined) {		
			var selectedAdjustmentType = UI_paxAccountDetails.getSelectedAdjustmentType(selectedCarrierCode, selectedAdjustmentTypeId);
			var selectedAdjustmentPrivileges = UI_paxAccountDetails.getSelectedAdjustmentPrivileges(selectedCarrierCode, selectedAdjustmentTypeId);		
		}
		
		if (selectedAdjustmentTypeId != null && selectedAdjustmentTypeId != undefined 
				&& selectedAdjustmentType != null && selectedAdjustmentType !=undefined
				&& selectedCarrierCode != null && selectedCarrierCode != undefined) {
			
			if (selectedAdjustmentType.refundableChargeCode != null &&
					selectedAdjustmentType.refundableChargeCode.length > 0 && selectedAdjustmentPrivileges.refundable == true) {				
				$("#selChargeType").append('<option value="true">Refundable</option>');
			}
			if (selectedAdjustmentType.nonRefundableChargeCode != null && 
					selectedAdjustmentType.nonRefundableChargeCode.length > 0 && selectedAdjustmentPrivileges.nonRefundable == true) {				
				$("#selChargeType").append('<option value="false">Non Refundable</option>');
			}						
		}				
	}
	
	UI_paxAccountDetails.getSelectedSegmentCarrierCode = function () {
		var selectedSegmentRPH = $("#selAirlineOnd").val();
		var jsonSegs = jQuery.parseJSON(opener.UI_reservation.jsonONDs);
		var selectedSegmentCarrierCode;
		jQuery.each(jsonSegs, function() {
 			if (this.carrierOndGroupRPH == selectedSegmentRPH){
 				selectedSegmentCarrierCode = this.carrierCode;
				return false;
			}
		});		
		return selectedSegmentCarrierCode;
	}
	
	UI_paxAccountDetails.getSelectedAdjustmentType = function (carrierCode, adjustmentTypeId) {
		var adjustmentTypes;
		var selectedAdjustmentType;		
		for (var i=0; i< jsAdjustmentTypesMap.length; i++) {
			if (jsAdjustmentTypesMap[i].carrierCode == carrierCode){
				adjustmentTypes = jsAdjustmentTypesMap[i].chargeAdjustmentTypes;
				break;
			}
		}
		for (var i=0; i< adjustmentTypes.length; i++) {				
			if (adjustmentTypes[i].chargeAdjustmentTypeId == adjustmentTypeId){
				selectedAdjustmentType = adjustmentTypes[i];
				break;
			}		
		}		
		return selectedAdjustmentType;
	}
	
	UI_paxAccountDetails.getSelectedAdjustmentPrivileges = function (selectedCarrierCode, selectedAdjustmentTypeId){
		
		var privs = opener.DATA_ResPro.initialParams.chargeAdjustPrivileges.carrierWiseChargeAdjustPrivileges;
		var selectedCarrierPrivileges;
		var selectedAdjustmentTypePrivileges;

		$.each(privs, function(index, value) { 
  			if (index == selectedCarrierCode) {
  				selectedCarrierPrivileges = value;
  				return false;
  			}
		});		
		
		for (var i=0; i< selectedCarrierPrivileges.length; i++) {
			if (selectedCarrierPrivileges[i].chargeAdjustmentTypeId == selectedAdjustmentTypeId) {
				selectedAdjustmentTypePrivileges = selectedCarrierPrivileges[i];
				break;
			}
		}		
		return selectedAdjustmentTypePrivileges;
	}

	//last time fix. not the most elegant. (AARESAA-5905-Issue 3)
	UI_paxAccountDetails.disableChargeAdjutmentInputs =function(options){
		if (options == '<option></option>'){
			UI_paxAccountDetails.disableAdjutmentInputs(true);
		}
	}

	UI_paxAccountDetails.disableAdjutmentInputs = function(disabled){
		$("#txtAdjustment").attr("disabled", disabled);
		$("#selChargeType").attr("disabled", disabled);
		$("#selAirlineOnd").attr("disabled", disabled);
		$("#selAdjustmentType").attr("disabled", disabled);
		$("#txtUN").attr("disabled", disabled);
		$("#btnConfirm").attr("disabled", disabled);
	}

	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	
	