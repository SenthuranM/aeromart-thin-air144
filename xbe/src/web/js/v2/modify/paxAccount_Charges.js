	/*
	*********************************************************
		Description		: XBE - Pax Account Details - Charges
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/
	
	var jsonChagesObj = [
					{id:"#txtAdjustment", desc:"Adjustment Amount", required:true},
					{id:"#selChargeType", desc:"Charge", required:true},
					{id:"#txtUN", desc:"User notes", required:true}
					];
	
	
	function UI_paxAccountCharges(){}
	
	UI_paxAccountCharges.gridCreated = false;
	/*
	 * Account Charges ready method
	 */
	UI_paxAccountCharges.ready = function(){
		 		 
		// Charges Tab
		$("#btnClose").decorateButton();
		$("#btnClose").click(function() {UI_paxAccountCharges.closeOnclick();});
		
		$("#btnConfirm").decorateButton();
		$("#btnConfirm").click(function() {UI_paxAccountCharges.confirmOnclick();});
		
		$("#btnReset").decorateButton();
		$("#btnReset").click(function() {UI_paxAccountCharges.resetOnclick();});
		$("#txtAdjustment").blur(function() {UI_paxAccountCharges.amountChange();UI_paxAccountCharges.chargeAdjustmentOnChange();});
		
		$("#btnServiceTaxAmount").decorateButton();
		$("#btnServiceTaxAmount").click(function(){UI_paxAccountCharges.getChargeAmountWithServiceTax();});
		
		$("#btnHandlingFeeAmount").decorateButton();		
		$("#btnHandlingFeeAmount").click(function() {UI_paxAccountCharges.getHandlingFeeAmount();});		
		
		$("#txtAdjustment").change(function(){UI_paxAccountCharges.chargeAdjustmentOnChange();});
		
//		UI_paxAccountCharges.constructGrid({id:"#tblCharges"});
		//UI_paxAccountCharges.fillData();

		if( !DATA_ResPro.initialParams.adjustCharges ){
			$("#btnServiceTaxAmount").hide();
			$("#chargesAdjustmentInput").hide();
			$("#btnConfirm").hide();
			$("#btnReset").hide();
		}
		
		if(opener.UI_reservation.isServiceTaxApplied){
			$("#btnConfirm").disable();
		}else{
			$("#btnServiceTaxAmount").hide();
		}
		
		$("#btnHandlingFeeAmount").hide();
	}
	
	/*
	 * Charges Validate
	 */
	UI_paxAccountCharges.validateCharges = function(){
		UI_message.initializeMessage();
		
		if ($("#selAirlineOnd").val() == ""){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-23"," the segment")});
			return false;
		}
		
		/* Invalid Character Validations */
		if (!UI_commonSystem.validateInvalidCharWindow(jsonChagesObj)){return false;}
		
		if(Number($("#txtAdjustment").val()) < 0) {
			
			if(!DATA_ResPro.initialParams.refundableChargeAdjust){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-74")});
				return false;
			}
		}
		
		if (!UI_commonSystem.validateMandatoryWindow(jsonChagesObj)){return false;}
		
		$("#txtAdjustment").val($.trim($("#txtAdjustment").val()));
		
		if ($("#txtAdjustment").val() != "" && $("#selChargeType").val() == "false" ){
			if (!UI_commonSystem.numberValidateWindow({id:"#txtAdjustment", desc:"Adjustment Amount", minValue:0, maxValue:10000000, blnDecimal:true})){return false;}
		} else if($("#txtAdjustment").val() != "" &&  $("#selChargeType").val() == "true") {
			if (!UI_commonSystem.numberValidateWindow({id:"#txtAdjustment", desc:"Adjustment Amount", maxValue:10000000, blnDecimal:true})){return false;}
		}
		
		

		if(Number($("#txtAdjustment").val()) == 0){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04","Adjustment Amount")});
			return false;
		}

		if(!currencyValidate($.trim($("#txtAdjustment").val()), 10, 2)){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-56","2")});
			return false;
		}		
		
		// Validate UserNotes Field
		if ($.trim($("#txtUN").val()).length > 250) {
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-18", "250", "User Notes")});
			$("#txtUN").focus();
			return false;
		}
		if(Number($("#txtAdjustment").val()) < 0) {
			
			if(!DATA_ResPro.initialParams.refundableChargeAdjust){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-71")});
				return false;
			}			
			var totalCharge = 0;
			var selText =  $("#selAirlineOnd option:selected").text();
			var selTrimStr = selText.split("-");
			var selTotStr = "Total ("+ trim(selTrimStr[1])+ ")";
			for(var i=0;i< jsPaxCharges.chargesList.length;i++){				
				if(trim(jsPaxCharges.chargesList[i].displayDescription) == selTotStr){
					totalCharge = Number(jsPaxCharges.chargesList[i].displayAmount);
					break;
				}
			}
			
			if(totalCharge < (Number($("#txtAdjustment").val() * -1))) {
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", "Adjustment Amount")});
				$("#txtAdjustment").focus();
				return false;				
			}
			
		}		
		
		// Validate when minus charge adjustment requested PAX has no balance to pay  
		/*if(Number($("#txtAdjustment").val()) < 0){
			var availableBalance = 0;
			var selectedPaxRef = $("#selPax").val();
			if(selectedPaxRef!=null && trim(selectedPaxRef)!=""){				
				var paxArray = $.parseJSON(opener.UI_reservation.jsonPassengers);
				for(var l=0;l<paxArray.length;l++) {	
					if(paxArray[l].travelerRefNumber == selectedPaxRef){
						availableBalance = Number(paxArray[l].totalAvailableBalance);
						if(availableBalance > 0){
							UI_message.showErrorMessage({messageText:"Sorry Minus Charge Adjustment not allowed, Identified Balance to pay"});
							$("#txtAdjustment").focus();				
							return false;	
						}
						
					}
				}
			}	
		}*/
		return true;
	}
	
	/*
	 * User Notes Close
	 */
	UI_paxAccountCharges.closeOnclick = function(){
		UI_paxAccountDetails.close();
	}
	
	/*
	 * Confirm On Click
	 */
	UI_paxAccountCharges.confirmOnclick = function() {
		if (UI_paxAccountCharges.validateCharges()) {
			//IE fix
			$("#resONDs").val(opener.UI_reservation.jsonONDs);
			$("#selChargeType").enable();
			UI_commonSystem.showProgress();
			var data = {};
			data['groupPNR']  = opener.jsonGroupPNR;
			data['pnr']  = opener.jsonPNR;
			data['version']  = opener.jsonBkgInfo.version;
			data['resOnds']  = $("#resONDs").val();			
			data['resSegments']  = opener.UI_reservation.jsonSegs;
			data['status']  = opener.jsonBkgInfo.status;
			data['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
			$("#frmPaxAccount").action("paxAccountChargesConfirm.action");
			$("#frmPaxAccount").ajaxSubmit({ dataType: 'json', processData: false, data:data,
											success: UI_paxAccountCharges.confirmChargesSuccess, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	/*
	 * Charges Confirm Success
	 */
	UI_paxAccountCharges.confirmChargesSuccess = function(response) {
		
		UI_commonSystem.hideProgress();		
		if(response.success) {
			UI_paxAccountCharges.resetOnclick();
			$("#selPax").val(response.selPax);			
			jsPaxCharges = response.paxAccountChargesTO;
			jsPaymentDetails = response.paxAccountPaymentsTO;
			jsCreditDetails = response.paxAccountCreditTO;
			UI_paxAccountCharges.fillData();
			UI_paxAccountPayment.fillData();
			UI_paxAccountCredit.fillData();			
			UI_message.showConfirmationMessage({messageText:jsSuccessMsg.msgCharges});
			
		} else {
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}
		opener.UI_reservation.loadReservation("", null,false);
	}
	
	/*
	 * Reset On Click
	 */
	UI_paxAccountCharges.resetOnclick = function(){
		$("#frmPaxAccount").reset();
		$("#amountWithSerivceTax").text('');
		if(opener.UI_reservation.isServiceTaxApplied){
			$("#btnConfirm").disable();
		}else{
			$("#btnServiceTaxAmount").hide();
		}
	}
	
	/*
	 * adjustment amount changes
	 */
	UI_paxAccountCharges.amountChange = function() {
		if(Number($("#txtAdjustment").val()) < 0) {
			$("#selChargeType").val('true');
			$("#selChargeType").disable();  
		}else {
			$("#selChargeType").val('');
			$("#selChargeType").enable();  
		}
		
	}
	
	/*
	 * Charges Fill Data
	 */
	UI_paxAccountCharges.fillData = function(){
		var showAgentCurrValues = (jsPaxCharges.agentCurrency != null || jsPaxCharges.agentCurrency != '') 
									&& jsPaxCharges.agentCurrency != jsPaxCharges.currency;
		UI_paxAccountCharges.constructGrid({id:"#tblCharges", agentCurrency: jsPaxCharges.agentCurrency, 
			showAgentCurrValues: showAgentCurrValues, currency: jsPaxCharges.currency});
		UI_commonSystem.fillGridData({id:"#tblCharges", data:jsPaxCharges.chargesList});
		$("#divChgTotal").html(jsPaxCharges.totalCharges + " " + jsPaxCharges.currency);
		$("#divChgBalance").html(jsPaxCharges.balance + " " + jsPaxCharges.currency);
		$("#divActualChgCredit").html(jsPaxCharges.actualCredit + " " + jsPaxCharges.currency);
		$("#divChjgAdjustCurr").html(jsPaxCharges.currency);
		
	}
	
	/*
	 * Construct Grid
	 */
	UI_paxAccountCharges.constructGrid = function(inParams){
		//construct grid
		if(!UI_paxAccountCharges.gridCreated){
			$(inParams.id).jqGrid({
				datatype: "local",
				height: 280,
				width: 800,
				colNames:[getOpeneri18nData('PaxAccDetails_Segment','Segment'), getOpeneri18nData('PaxAccDetails_DepartureDate','Departure Date'),getOpeneri18nData('PaxAccDetails_Date','Date'), getOpeneri18nData('PaxAccDetails_Description','Description'),'Amount ('+inParams.agentCurrency+')', 'Amount ('+inParams.currency+')',getOpeneri18nData('PaxAccDetails_Carrier','Carrier')],
				colModel:[
					{name:'displaySegment',  index:'displaySegment', width:100, align:"center",sortable:false },
					{name:'displayDepartureDate', index:'displayDepartureDate', width:100, align:"center",sortable:false },
					{name:'displayDate', index:'displayDate', width:100, align:"center",sortable:false },
					{name:'displayDescription', index:'displayDescription', width:450, align:"left",sortable:false },
					{name:'agentCurrencyAmount', index:'agentCurrencyAmount', width:100, align:"right",sortable:false, 
						hidden: !inParams.showAgentCurrValues },
					{name:'displayAmount', index:'displayAmount', width:100, align:"right",sortable:false },
					{name:'displayCarrierCode', index:'displayCarrierCode', width:50, align:"right",sortable:false }
				],
				imgpath: UI_commonSystem.strImagePath,
				multiselect: false,
				viewrecords: true,
				onSelectRow: function(rowid){
				}
			});
			UI_paxAccountCharges.gridCreated = true;
		}
	}
	
	UI_paxAccountCharges.getChargeAmountWithServiceTax = function(){
		if (UI_paxAccountCharges.validateCharges()) {
			//IE fix
			$("#selChargeType").enable();
			UI_commonSystem.showProgress();
			var data = {};
			data['groupPNR']  = opener.jsonGroupPNR;
			data['pnr']  = opener.jsonPNR;
			data['version']  = opener.jsonBkgInfo.version;
			data['resSegments']  = opener.UI_reservation.jsonSegs;
			data['status']  = opener.jsonBkgInfo.status;
			$("#frmPaxAccount").action("paxAccountChargesConfirm!chargeAmountWithServiceTax.action");
			$("#frmPaxAccount").ajaxSubmit({ dataType: 'json', processData: false, data:data,
											success: UI_paxAccountCharges.getServiceTaxSuccess, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	UI_paxAccountCharges.getServiceTaxSuccess = function(response){

		UI_commonSystem.hideProgress();	
		
		if(response.success) {
			$("#amountWithSerivceTax").text(response.serviceTaxMessage);
			$("#btnConfirm").enable();
		}else {
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}
		
	}
	
	UI_paxAccountCharges.chargeAdjustmentOnChange = function(){
		if(opener.UI_reservation.isServiceTaxApplied){
			if($("#txtAdjustment").val().startsWith("-")){
				$("#btnConfirm").enable();
				$("#btnServiceTaxAmount").hide();
			}else{
				$("#btnConfirm").disable();
				$("#btnServiceTaxAmount").show();
			}
		}		
		
		UI_paxAccountCharges.enableDisableHandlingFeeCalc();	
	}
	
	UI_paxAccountCharges.enableDisableHandlingFeeCalc = function(){		
		if(!opener.UI_reservation.isServiceTaxApplied){
			$("#amountWithSerivceTax").text('');
			if(opener.UI_reservation.isHandlingFeeApplicableForAdj){
				if($("#txtAdjustment").val().startsWith("-")){
					$("#btnConfirm").enable();
					$("#btnHandlingFeeAmount").hide();
				}else{
					$("#btnConfirm").disable();
					$("#btnHandlingFeeAmount").show();
				}
			}else{
				$("#btnConfirm").enable();
				$("#btnHandlingFeeAmount").hide();
			}
		}
	}
	
	UI_paxAccountCharges.getHandlingFeeAmount = function(){
		if (UI_paxAccountCharges.validateCharges()) {
			//IE fix
			$("#selChargeType").enable();
			UI_commonSystem.showProgress();
			var data = {};
			data['groupPNR']  = opener.jsonGroupPNR;
			data['pnr']  = opener.jsonPNR;
			data['version']  = opener.jsonBkgInfo.version;
			data['resSegments']  = opener.UI_reservation.jsonSegs;
			data['status']  = opener.jsonBkgInfo.status;
			$("#frmPaxAccount").action("paxAccountChargesConfirm!chargeAmountWithHandlingFee.action");
			$("#frmPaxAccount").ajaxSubmit({ dataType: 'json', processData: false, data:data,
											success: UI_paxAccountCharges.getHandlingFeeAmountSuccess, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	UI_paxAccountCharges.getHandlingFeeAmountSuccess = function(response){
		UI_commonSystem.hideProgress();			
		if(response.success) {
			$("#amountWithSerivceTax").text(response.serviceTaxMessage);
			$("#btnConfirm").enable();
		}else {
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}		
	}
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	