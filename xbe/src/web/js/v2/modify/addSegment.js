	/*
	*********************************************************
		Description		: XBE Add Segment 
		Author			: Pradeep Karunanayaka
		Version			: 1.0
		Created			: 5 August 2010		
	*********************************************************	
	*/

	/*
	 * Add Reservation related Methods
	 */
	function UI_addSegment(){}
	UI_addSegment.tabStatus = ({tab0:true, tab1:false, tab2:false});		// tab Status
	UI_addSegment.tabDataRetrive = ({tab0:false, tab1:true, tab2:false});	// Data Retrieval Status
	UI_addSegment.selCurrency = "";
	UI_addSegment.resPaxInfo = "";
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapper").fadeIn("slow");
		$("#divBookingTabs").tabs();
		$('#divBookingTabs').bind('tabsbeforeactivate', function(event, ui) {
			return eval("UI_addSegment.tabStatus.tab" + ui.newTab.index());
		});
		
		UI_commonSystem.strPGMode = DATA_ResPro.reservationInfo.mode;
		
		if(DATA_ResPro.initialParams.viewAvailableCredit) {
			UI_commonSystem.showAgentCredit();
			UI_commonSystem.showAgentCreditInAgentCurrency();
		} else {
			$("#divCreditPnl").hide();
		}
		
		// Search Flight Tab -----------------------------------------------------
		UI_tabSearchFlights.ready();
		UI_tabSearchFlights.fillAddSegment(DATA_ResPro.reservationInfo);
		
		//UI_tabSearchFlights.disableEnablePageControls(false);
		// TODO Review
		UI_commonSystem.strPGID = "SC_RESV_CC_005";
	});
	
	/*
	 * Open Tab from the Function Call
	 */
	UI_addSegment.openTab = function(intIndex){
		$('#divBookingTabs').tabs("option", "active", intIndex); 	
	}
	
	/*
	 * Lock Tabs
	 */ 
	UI_addSegment.lockTabs = function(){
		UI_addSegment.tabStatus = ({tab0:true, 
										tab1:false, 
										tab2:false});
		
		UI_addSegment.tabDataRetrive = ({tab0:false, 
										tab1:true, 
										tab2:true});
	}
	
	
	UI_addSegment.showConfirmError = function(resMessage){
		showERRMessage(resMessage);
	}	
	
	UI_addSegment.getInsuranceFlightSegments = function(){
		var allSegs = DATA_ResPro.resSegments;
		var out = [];
		for(var i = 0 ;i < allSegs.length; i++){
			if(allSegs[i].status != 'CNX'){
				out[out.length] = allSegs[i];
			}
		}
		return out;
	}
	
	UI_addSegment.continueToAnci = function(){
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
			UI_tabAnci.isAddSegment = true;	
			var system = null;
			var data = UI_tabSearchFlights.createSearchParams('searchParams');
			data['flightRPHList'] = $.toJSON(UI_tabSearchFlights.getFlightRPHList());
			//data['insuranceAvailable'] = DATA_ResPro.resHasInsurance;
			data['addSegment'] = true;
			data['insuranceAvailable'] = DATA_ResPro.resHasInsurance;		
			data['insFltSegments'] = $.toJSON(UI_addSegment.getInsuranceFlightSegments());
			data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();
			data['resPaxInfo'] = $.toJSON(UI_addSegment.resPaxInfo);
			data['mulipleMealsSelectionEnabled'] = DATA_ResPro.initialParams.multiMealEnabled;

			UI_commonSystem.getDummyFrm().ajaxSubmit({url:'ancillaryAvailability.action', dataType: 'json', processData: false, data:data, success: function (res){
				UI_commonSystem.hideProgress();
				if(res.success){
					var paxArr = UI_addSegment.transformPax(DATA_ResPro.resPaxs);
					paxArr = $.airutil.sort.quickSort(paxArr, UI_addSegment.paxSorter);
					var infArr = [];
					var ninfArr = [];
					for(var i=0 ; i < paxArr.length; i++){
						if(paxArr[i].paxType == 'IN'){
							infArr[infArr.length] = paxArr[i];
						}else{
							ninfArr[ninfArr.length] = paxArr[i];
						}
					}
					res.paxAdults = ninfArr;
					res.paxInfants = infArr;
					res.contactInfo = DATA_ResPro.resContactInfo;
					
					UI_tabAnci.prepareAncillary(res, jsonFareQuoteSummary);
				}else{					
					showERRMessage(res.messageTxt);
				}
			}, error:UI_commonSystem.setErrorStatus
			});	
		
	}	
	
	UI_addSegment.paxSorter = function(first,second){
		return (first.paxId-second.paxId);
	}
	
	UI_addSegment.transformPax = function(paxArr){
		var outArr = [];
		for(var i = 0 ; i < paxArr.length; i++){			
			var inPax = paxArr[i];
			var pax = {};
			if(inPax.infants!=null && inPax.infants.length>0){
				pax.displayInfantTravelling = 'Y';
				pax.displayInfantWith = inPax.infants[0].paxSequence - 1;
			}else{				
				pax.displayInfantTravelling = 'N';
				pax.displayInfantWith = '';
			}
			pax.displayAdultTitle = inPax.title;
			pax.displayAdultFirstName = inPax.firstName;
			pax.displayAdultLastName = inPax.lastName;
			pax.displayAdultType = inPax.paxType;
			pax.displayAdultNationality = inPax.nationalityCode;
			pax.currentAncillaries = inPax.selectedAncillaries;
			pax.displayAdultDOB = inPax.dateOfBirth;
			pax.travelerRefNumber = inPax.travelerRefNumber;
			pax.displayPnrPaxCatFOIDNumber = inPax.pnrPaxCatFOIDNumber;
			pax.displayNationalIDNo = inPax.nationalIDNo;
			pax.displayETicket = inPax.eTicket;
			pax.paxId = inPax.paxSequence-1;
			pax.paxType = inPax.paxType;
			pax.seqNumber = inPax.paxSequence;
			//pax.totalFare = inPax.totalFare;
			pax.totalPaidAmount = inPax.totalPaidAmount;
			pax.totalAvailableBalance = inPax.totalAvailableBalance;
			pax.totalPrice = inPax.totalPrice;
			
			
			//pax.totalSurcharge = inPax.totalSurcharge;
			//pax.totalTaxCharge = inPax.totalTaxCharge;
			outArr[i] = pax;
		}
		return outArr;
	}
	
	UI_addSegment.capturePaymentFromAnci = function(){
		var data = {};	
		data['pnr'] = DATA_ResPro.reservationInfo.PNR;
		data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;		
		data['selCurrency'] = UI_addSegment.selCurrency;
		data['resSegments']= $.toJSON(DATA_ResPro.resSegments);
		data['resPaxs']= $.toJSON(DATA_ResPro.resPaxs);
		data['version']= DATA_ResPro.reservationInfo.version;		
		data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
		data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();		
		data['flightRPHList'] = $.toJSON(UI_tabSearchFlights.getFlightRPHList());
		if(DATA_ResPro.selectedPnrSeg != null && DATA_ResPro.selectedPnrSeg != ""){
			data['selectedPnrSeg'] = DATA_ResPro.selectedPnrSeg;	
		}	
		data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
		data['ownerAgentCode'] = DATA_ResPro.reservationInfo.ownerAgentCode;
		var fareQuoteData = UI_tabSearchFlights.createFareQuoteParams();				
		data = $.airutil.dom.concatObjects(data,fareQuoteData);	
		$("#frmMakeBkg").action("addSegmentConfirm.action");
		$("#frmMakeBkg").ajaxSubmit({ dataType: 'json', processData: false, data:data,
										success: UI_addSegment.continueOnClickSuccess,
										error:UI_commonSystem.setErrorStatus});
	}
	
	
	/*
	 * Continue On Click success
	 */
	UI_addSegment.continueOnClickSuccess = function(response){
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		// prepare the next tab 
		UI_tabPayment.isAddSegment = true;
		UI_tabPayment.ready();
		UI_commonSystem.hideProgress();
		
		if (response != null){
			if(!response.success){
				showERRMessage(response.messageTxt);
				return false;
			}
			
			if(response.agentBalance != null){
				top.intAgtBalance = response.agentBalance;
				top.agentAvailableCreditInAgentCurrency = response.agentBalanceInAgentCurrency;
			}
			
			if(response.blnNoPay){
				DATA_ResPro.reservationInfo.groupPNR = response.groupPNR;
				UI_addSegment.loadHomePage();
				return false;
			}
		
			UI_tabPayment.updatePayemntTabTO(response);
		}
		UI_addSegment.tabDataRetrive.tab2 = false;
		UI_addSegment.tabStatus.tab2 = true;
		UI_addSegment.openTab(2);
		UI_tabPayment.paymentDetailsOnLoadCall();
	}
	
	/*
	 * Load add segment source page
	 */
	UI_addSegment.loadHomePage = function(){
		UI_addSegment.selCurrency = "";
		location.replace("showNewFile!loadRes.action?pnrNO=" + DATA_ResPro.reservationInfo.PNR+"&groupPNRNO="+DATA_ResPro.reservationInfo.groupPNR);
	}
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	