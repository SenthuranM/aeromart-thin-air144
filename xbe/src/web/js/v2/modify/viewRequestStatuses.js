$( document ).ready(function() {
    $("#requestHistoryView").dialog({ 
		autoOpen: false,
		modal:true,
		height: 'auto',
		width: 'auto',
		title:'Display Request Statuses and History'
	});
});
function loadStatuses(){
		reateGridforQueueResults();
		//constructInfNameGrid();


		//UI_commonSystem.fillGridData({id:"#tblPaxAdtName", data:jsonPaxAdtObj.paxAdults, editable:true});
		//UI_commonSystem.fillGridData({id:"#tblPaxInfName", data:jsonPaxAdtObj.paxInfants, editable:true});

	$("#requestHistoryView").dialog('open');
}

reateGridforQueueResults = function(){	
	
	var listFomatterToString = function(cellVal, options, rowObject){
		var treturn = "";
		if (cellVal!=null){
			treturn= cellVal.toString();
		}
		return treturn;
	}

	var listFomatterStringify = function(cellVal, options, rowObject){
		var treturn = "";
		if (cellVal!=null){
			treturn= JSON.stringify(cellVal);
		}
		return treturn;
	}
	
	var data = {};
	data['pnrId'] = $("#divPnrNo").text();
	$("#p_requestTable").empty().append('<table id="requestTable"></table><div id="requestsStatusesPages"></div>')
	var requestGrid = $("#requestTable").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'viewQueueRequestStatuses!loadRequestsOnPnr.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           type : "POST",		           
				   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	//  if (mydata.success){
			            		  $.data(requestGrid[0], "gridData", mydata.row);
			            		  requestGrid[0].addJSONData(mydata);
			            	//  }else{
			            		//  alert(mydata.message);
			            	 // }
			              }
			           }
			 });
			},
		    height: 200,
		    colNames:['RequestID','QueueId','Description','Status','AssignedTo','AssignedOn'], 
		    colModel:[ 	{name:'RequestID',width:40, align:"center", jsonmap:'pnrRequestStatus.requestId'},   
			        {name:'QueueId' ,width:40, align:"center", jsonmap:'pnrRequestStatus.queueId'},
{name:'Description' ,width:40, align:"center", jsonmap:'pnrRequestStatus.description'},
{name:'Status' ,width:40, align:"center", jsonmap:'pnrRequestStatus.status'},
{name:'AssignedTo' ,width:40, align:"center", jsonmap:'pnrRequestStatus.assignedTo'},
{name:'AssignedOn' ,width:40, align:"center", jsonmap:'pnrRequestStatus.assignedDate'}
],

		        caption: "Requests Submitted for the PNR "+$("#divPnrNo").text(),
		        rowNum : 5,
			viewRecords : true,
			width : 500,
			jsonReader: { 				
				root: "row",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				emptyrecords: "Nothing to display",
			},
			rownumbers: true,	
			pager: jQuery('#requestsStatusesPages'),
		    
	}).navGrid("#requestsPages",{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true});
	$("#requestTable").click(function(){ 
		requestsGridOnClick();					 
	});
}

requestsGridOnClick = function(){	
	var rowId =$("#requestTable").getGridParam('selrow');  
	var rowData = $("#requestTable").getRowData(rowId);
	var queueRequestId=rowData.RequestID;	
	alert(queueRequestId);

	var data2 = {};
	data2['requestId']=queueRequestId;
	$("#p_requestHistory").empty().append('<table id="requestHistoryTable"></table><div id="requestHistoryPages"></div>')
	var requestHistoryGrid = $("#requestHistoryTable").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'requestHistoryPnr!loadHistoryForRequest.action',
			           data: $.extend(postdata , data2),
			           dataType:"json",
			           type : "POST",		           
				   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	//  if (mydata.success){
			            		  $.data(requestHistoryGrid[0], "gridData", mydata.row);
			            		  requestHistoryGrid[0].addJSONData(mydata);
			            	//  }else{
			            		//  alert(mydata.message);
			            	 // }
			              }
			           }
			 });
			},
		    height: 200,
		    colNames:['HistoryId','RequestID'], 
		    colModel:[ 	{name:'HistoryId',width:40, align:"center", jsonmap:'requestHistoryDTO.requestId'},   
			        {name:'RequestID' ,width:40, align:"center", jsonmap:'requestHistoryDTO.historyId'}
],

		        caption: "Request History for Request Id "+queueRequestId,
		        rowNum : 5,
			viewRecords : true,
			width : 500,
			jsonReader: { 				
				root: "row",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				emptyrecords: "Nothing to display"
			},
			rownumbers: true,	
			pager: jQuery('#requestHistoryPages'),
		    
	}).navGrid("#requestHistoryPages",{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true});
		
}
