	/*
	*********************************************************
		Description		: XBE Make Reservation - Search
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 08th September 2008
		Last Modified	: 08th September 2008
	*********************************************************	
	*/
	
	var jsonSCObj = [
					{id:"#pnr", desc:geti18nData('Search_lbl_PNR','PNR'), required:false},
					{id:"#firstName", desc:geti18nData('Search_lbl_FirstName','First Name'), required:false},
					{id:"#lastName", desc:geti18nData('Search_lbl_LastName','Last Name'), required:false},
					{id:"#fromAirport", desc:geti18nData('Search_lbl_DepartureLocation','Departure location'), required:false},
					{id:"#depatureDate", desc:geti18nData('AdvSearch_lbl_DepartureDate','Departure Date'), required:false},
					{id:"#txtPhoneCntry", desc:geti18nData('Search_lbl_PhoneCountryCode','Phone Country Code'), required:false},
					{id:"#txtPhoneArea", desc:geti18nData('Search_lbl_PhoneAreaCode','Phone Area Code'), required:false},
					{id:"#txtPhoneNo", desc:geti18nData('AdvSearch_lbl_TelephoneNo','Phone No'), required:false},
					{id:"#toAirport", desc:geti18nData('Search_lbl_Arrival_Location','Arrival location'), required:false},
					{id:"#returnDate", desc:geti18nData('Search_lbl_ReturnDate','Return Date'), required:false},
					{id:"#flightNo", desc:geti18nData('AdvSearch_lbl_FlightNo','Flight No'), required:false},
					{id:"#creditCardNo", desc:geti18nData('AdvSearch_lbl_CreditCardNo','Credit Card No'), required:false},
					{id:"#cardExpiry", desc:('AdvSearch_lbl_ExpiryDate','Expiry Date'), required:false},
					{id:"#authCode", desc:geti18nData('AdvSearch_lbl_CreditCardAuthorizationCode','CC auth code'), required:false},
					{id:"#bookingType", desc:geti18nData('BCType','BC Type'), required:false},
					{id:"#passport", desc:geti18nData('AdvSearch_lbl_Passport','Passport'), required:false}
					];
					
	/* TODO Remove this */
	var jsonResults =  [];
	
	/*
	 * Make Reservation related Methods
	 */
	function UI_searchReservation(){}
	
	UI_searchReservation.blnAdvSearch = false;
	UI_searchReservation.intLastSelected = null;
	UI_searchReservation.strLastPNRNo = null;
	UI_searchReservation.blnExpand = false;
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divBookingTabs").getLanguage();
		$("#divLegacyRootWrapper").fadeIn("slow");
		
		$("#depatureDate").datepicker({ dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		$("#returnDate").datepicker({ dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		
		$("#depatureDate").blur(function() { UI_searchReservation.dateOnBlur({id:0});});
		$("#returnDate").blur(function() { UI_searchReservation.dateOnBlur({id:1});});
		$("#cardExpiry").blur(function() { UI_searchReservation.expiryDateChk();});
		
		$("#btnSearch").decorateButton();
		$("#btnReset").decorateButton();
		$("#btnClose").decorateButton();
		
		$("#btnSearch").click(function() { UI_searchReservation.searchOnClick();});
		$("#btnReset").click(function() { UI_searchReservation.resetOnClick();});
		$("#btnClose").click(function() { UI_searchReservation.cancelOnClick();});
		$("#rSearchPNR").click(function () {
			UI_searchReservation.switchLabelAndHideNameFields('PNR', false);
		});

		$("#rSearchETicket").click(function () {
			UI_searchReservation.switchLabelAndHideNameFields('ETICKET', false);
		});
		$("#rSearchGDSRecordLocator").click(function () {
			UI_searchReservation.switchLabelAndHideNameFields('GDS RLOC', false);
		});
		$("#rSearchExternalETicket").click(function () {
			UI_searchReservation.switchLabelAndHideNameFields('EXTERNAL ETICKET', false);
		});
		$("#rInvoiceNumber").click(function () {
			UI_searchReservation.switchLabelAndHideNameFields('INVOICE NUMBER', true);
		});

		$("#lnkAdvSrch").click(function() { UI_searchReservation.advanceSearchClick();});
		$("#tdBookMark").click(function() { UI_searchReservation.moveClick();});
		$("#divReservation").hide();
		$("#divBookMarkReservation").addClass("bookMarkInit");
		$("#divBookMarkReservation2").hide();
		$("#selSearchOptions").fillDropDown( { dataArray:top.arrReservationSearchOptions , keyIndex:0 , valueIndex:1 });
		
		if(!DATA_ResPro.searchByEticket){
			$("#spnEticket").hide();
		}
		
		if(DATA_ResPro.initialParams.viewAvailableCredit) {
			UI_commonSystem.showAgentCredit();
			UI_commonSystem.showAgentCreditInAgentCurrency();
		} else {
			$("#divCreditPnl").hide();
		}
		
		UI_searchReservation.fillDropDownData();
		UI_searchReservation.initialization();
		
		UI_searchReservation.writePNR();		

		$("#creditCardNo").numeric();
		$("#authCode").numeric();
		
		if (top.arrPrivi[PRIVI_ADVANCE_SEARCH] != 1){
			$("#divAdvSearchHD").hide();
		}
		$("#divPNR").html("PNR");
		$("#pnr").change(function() { UI_searchReservation.validatePnr();});
		$('#pnr').keyup(function() { UI_searchReservation.validatePnr();});
		$('#frmSrchBkg').bind("keypress", function(keyEvent){ UI_searchReservation.checkEnterAndSubmit(keyEvent);});
		$('#pnr').focus();
		
		if(DATA_ResPro.load == 'LOAD'){
			$("#pnr").val(top.strSearchBookingPNR);
			$("#divReservation").show();
			if(top.blnInterLine){
				$("#pnrNO").val(top.strSearchBookingPNR);
				$("#groupPNRNO").val(top.strSearchBookingPNR);
				$('#selSearchOptions').val('INT');
			}else {
				$("#pnrNO").val(top.strSearchBookingPNR);
				$("#groupPNRNO").val("");
				$('#selSearchOptions').val('AA');
			}			
			UI_searchReservation.searchOnClick();
		}else if(DATA_ResPro.load == 'ALL'){
			// in addition, will load own reservation which was never loaded via LCC
			$("#pnr").val(top.strSearchBookingPNR);
			if(top.blnInterLine){
				$('#selSearchOptions').val('INT');
			}else {
				$('#selSearchOptions').val('AA');
			}	
			UI_searchReservation.searchOnClick();
		}
		
		if(!DATA_ResPro.initialParams.displayContDetailsInPNRSummary){
			$("#spnDisplayContact").hide();
		}
		
		UI_PageShortCuts.initilizeMetaDataForModificationSearch();
	});
	
	UI_searchReservation.validatePnr = function() {
		var t= $("#pnr");
		var value = t.val();
		if (!/^[0-9a-zA-Z]*$/.test(value)) {
			value =value.replace(/[^0-9a-zA-Z]/g,"");
			t.val(value);
		}		
	}
	
	/*
 	 * Search Click
	 */
	UI_searchReservation.searchOnClick = function(){
		var data = {};
		data['pageNo'] = 1;
		$("#frmSrchBkg").ajaxSubmit({ dataType: 'json', data:data,
										beforeSubmit:UI_searchReservation.validate,	
										success: UI_searchReservation.returnCallSearchReservation,
										error:UI_commonSystem.setErrorStatus});
		return false;
	}
	
	/*
	 * Submit form on enter key press 
	 */
	
	UI_searchReservation.checkEnterAndSubmit = function(keyEvent){

          if(keyEvent.which == 13){

              UI_searchReservation.searchOnClick();
          }
     }

	
	/*
	 * Reset On Click
	 */
	UI_searchReservation.resetOnClick = function(){
		UI_searchReservation.initialization();
		$('#pnr').focus();
	}
	
	UI_searchReservation.moveClick = function(){
		if(UI_searchReservation.blnExpand){
			$("#divBookMarkReservation").removeClass("bookMarkExpand");
			$("#divBookMarkReservation").addClass("bookMarkInit");
			$("#imgPNRList").attr('src','../images/AA140_2_no_cache.jpg');
			UI_searchReservation.blnExpand = false;
			$("#spnPLRListIcon", window.top[0].document).removeClass("menuItemDown");
			$("#spnPLRListIcon", window.top[0].document).css("padding", "0px");
			$("#divBookMarkReservation2").hide();
		}else {
			$("#divBookMarkReservation").removeClass("bookMarkInit");
			$("#divBookMarkReservation").addClass("bookMarkExpand");
			$("#imgPNRList").attr('src','../images/AA140_no_cache.jpg');
			UI_searchReservation.blnExpand = true;
			$("#spnPLRListIcon", window.top[0].document).addClass("menuItemDown");
			$("#spnPLRListIcon", window.top[0].document).css("padding", "0px");
			$("#divBookMarkReservation2").show();
		}
		
	}
	
	UI_searchReservation.switchLabel = function(strLabel) {
		$("#divPNR").html(strLabel);
	};

	UI_searchReservation.switchLabelAndHideNameFields = function(strLabel, hideNameFields) {
		UI_searchReservation.switchLabel(strLabel);
		if (hideNameFields) {
			$(".name-search-fields").hide();
			$(".name-search-fields-dummy-row").show();

		} else {
			$(".name-search-fields").show();
			$(".name-search-fields-dummy-row").hide();
		}
	};
	
	UI_searchReservation.writePNR = function(){
			var strHTMLText = "";
			strHTMLText = "<table width='95%' border='0' cellpadding='2' cellspacing='0' align='center'>";
			var strData = "";
			var pnHtml = new Array();
			var pnrVal = "";
			for (var i = 1 ; i <= top[0].arrPNRHistory.length  ; i++){
				if(top[0].arrPNRHistory[top[0].arrPNRHistory.length - i] != " "){
					if (strData != ""){
						strData +=  "<tr><td><hr></td></tr>";
					}
					pnHtml = new Array();
					pnHtml = top[0].arrPNRHistory[top[0].arrPNRHistory.length - i].split("-");
					pnrVal = "";
					/*if(pnHtml.length == 2 && pnHtml[1] != 'Undefined' && trim(pnHtml[1]) != "" && trim(pnHtml[1]) == "INTERLINE"){
						pnrVal = pnHtml[0];
					}else {
						pnrVal = top[0].arrPNRHistory[top[0].arrPNRHistory.length - i];
					}*/
					if(pnHtml.length == 3 && pnHtml[2] != 'Undefined' && trim(pnHtml[2]) != "" && trim(pnHtml[2]) == "INTERLINE"){
						pnrVal = pnHtml[0] + ' - ' + pnHtml[1];
					}else {
						pnrVal = top[0].arrPNRHistory[top[0].arrPNRHistory.length - i];
					}
					strData +=  "<tr>";
					strData +=  "<td>";
					strData += "<a href='#' onclick='UI_searchReservation.CallHistoryPNR(" + '"' + top[0].arrPNRHistory[top[0].arrPNRHistory.length - i] + '"' + ")'><font style='font-weight:bold;font-size:12;'>" + pnrVal + "</font></a><br>";
					strData +=  "<\/tr>";
				}		
			}

			strHTMLText += strData + "<\/table>"

			DivWrite("spnPNRList", strHTMLText);
			
		}
		
		UI_searchReservation.CallHistoryPNR = function(strPNR){
			if (!top.loadCheck()){
				return;
			}

			UI_searchReservation.ViewSearch();
			var pnrArr = strPNR.split("-");
			var strSelectedPNR = trim(pnrArr[0])
			UI_searchReservation.moveClick();
			
			UI_searchReservation.strLastPNRNo = strSelectedPNR;
			/*if(pnrArr.length == 2 && pnrArr[1] != 'Undefined' && trim(pnrArr[1]) != "" && trim(pnrArr[1]) == "INTERLINE"){
				$("#pnrNO").val(strSelectedPNR);
				$("#groupPNRNO").val(strSelectedPNR);
			}else {
				$("#pnrNO").val(strSelectedPNR);
				$("#groupPNRNO").val("");
			}*/
			if(pnrArr.length == 3 && pnrArr[2] != 'Undefined' && trim(pnrArr[2]) != "" && trim(pnrArr[2]) == "INTERLINE"){
				$("#pnrNO").val(strSelectedPNR);
				$("#groupPNRNO").val(strSelectedPNR);
			}else {
				$("#pnrNO").val(strSelectedPNR);
				$("#groupPNRNO").val("");
			}
			$("#divReservation").show();
			$("#frmRes").attr('target', "frmReservation");
			$("#frmRes").submit();		
			
		}
	
	
	/*
	 * Cancel Click
	 */
	UI_searchReservation.cancelOnClick = function(){
		top.LoadHome();
	}
	
	/*
	 * Advance Search Click
	 */
	UI_searchReservation.advanceSearchClick = function (){
		if (!UI_searchReservation.blnAdvSearch){
			$("#divAdvSearch").show();
			UI_searchReservation.blnAdvSearch = true;
		}else{
			UI_searchReservation.blnAdvSearch = false;
			$("#divAdvSearch").hide();
		}
	}
	
	/*
	 * Validate Search Param Data
	 */
	UI_searchReservation.validate = function(){
		UI_commonSystem.initializeMessage();
		
		/* Invalid Character Validations */
		if (!UI_commonSystem.validateInvalidChar(jsonSCObj)){return false;}
		
		if (($("#pnr").val() == "") && ($("#firstName").val() == "") && ($("#lastName").val() == "") && ($("#creditCardNo").val() == "") && ($("#authCode").val() == "")){
			showERRMessage(buildError(top.arrError["XBE-ERR-11"],""));
			$("#pnr").focus();	
			return false;
		}
		
		if ($("#depatureDate").val() != "" && $("#returnDate").val() != ""){
			if (!CheckDates($("#depatureDate").val(), $("#returnDate").val())){
				showERRMessage(raiseError("XBE-ERR-03","Return date", "from date"));
				$("#returnDate").focus();
				return false;
			}	
		}
		
		if ($("#fromAirport").val() != "" && $("#toAirport").val() != ""){
			if ($("#fromAirport").val() == $("#toAirport").val()){
				showERRMessage(raiseError("XBE-ERR-02","From location","To location"));
				$("#toAirport").focus()		
				return false;
			}
		}
		
		if ($("#chkExact").attr('checked')){
			if ($("#lastName").val() != ""){
				if ($("#lastName").val().length < 2){
					showERRMessage(raiseError("XBE-ERR-16","2","Last Name"));
					$("#lastName").focus()		
					return false;
				}
			}
		}else {	
			if ($("#firstName").val() != ""){
				if ($("#firstName").val().length < 2){
					showERRMessage(raiseError("XBE-ERR-16","2","First Name"));
					$("#firstName").focus()		
					return false;
				}
			}
		}
		
		if ($("#creditCardNo").val() != ""){
			if ($("#cardExpiry").val() == ""){
				showERRMessage(raiseError("XBE-ERR-01","Expiry Date"));		
				$("#cardExpiry").focus()		
				return false;
			}
			
			if ($("#creditCardNo").val().length !== 4){
				showERRMessage(raiseError("XBE-ERR-16", "4", "Credit Card No"));	
				$("#creditCardNo").focus()		
				return false;
			}
		}
		
		$("#tblSrarch").children().remove();
		UI_commonSystem.showProgress();
		return true;
	}
	
	/*
	 * Ajax Return Call
	 */ 
	UI_searchReservation.returnCallSearchReservation = function(response){
		UI_commonSystem.hideProgress();
		
		if(response!=null && response.success && response.reservations.length >0){
			$("#divSrchResults").show();
			
			/* Initialize the Details */
			$("#tblSrarch").children().remove();
			
			jsonResults = response.reservations;
			var pageNo = response.pageNo;
			var recordsPerPage = response.recordsPerPage;
			
			if (jsonResults != null){
				var i = 0;
				var x = 0
				var intRowSpan = 0;
				var tblRow = null;
				var objD = null;
				var strColor = "";
				tblRow = document.createElement("TR");
				$.each(jsonResults, function(){
					
					objD = jsonResults[i].flightInfo;
					var exCount = 0;
					for(var ll=0; ll < objD.length; ll++){
						if(objD[ll].subStatus == 'EX'){
							exCount++;
						}
					}
					intRowSpan = objD.length - exCount;
					
					var paxName = jsonResults[i].paxName;
					var paxcontact = ' ';
					
					if(jsonResults[i].paxEmail != '' && jsonResults[i].paxEmail != 'undefined'){
						paxcontact = '<BR> Email: ' + jsonResults[i].paxEmail;
					}
					if(jsonResults[i].paxPhone != '' && jsonResults[i].paxPhone != '--'){
						paxcontact += '<BR> Phone: ' + jsonResults[i].paxPhone;
					}
					if(jsonResults[i].paxMobile != '' && jsonResults[i].paxMobile != '--'){
						paxcontact += '<BR> Mob: ' + jsonResults[i].paxMobile;
					}
					
					if(DATA_ResPro.initialParams.displayContDetailsInPNRSummary && 
							$("#chkDspContDetails").attr('checked')){
						paxName += paxcontact;
					}					
					
					x = 0;
					var strFltInfoDisplayMouseOver = "";
					var strFltInfoDisplayMouseOut = ""; 
					var first = true;
					$.each(objD, function(){
						if(objD[x].subStatus == 'EX'){
							x++;
							return ;
						}
						tblRow = document.createElement("TR");
						strColor = "";
						
						if(DATA_ResPro.initialParams.fltStopOverDurInfoToolTipEnabled){
							strFltInfoDisplayMouseOver =  UI_searchReservation.displayFlightAddInfoToolTip;
							strFltInfoDisplayMouseOut  =  UI_searchReservation.hideFlightAddInfoToolTip;
						}
						
						if (first){
							if (DATA_ResPro.initialParams.viewReservationDetailsAllowed) {
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_0_res", desc:(recordsPerPage * (pageNo - 1))+ i+1, width:"2%", align:"center", css:UI_commonSystem.strDefClass , rowSpan:intRowSpan}));
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_1_res", desc:jsonResults[i].pnrNo, width:"8%", align:"center", css:UI_commonSystem.strDefClass + " thinBorderL cursorPointer", rowSpan:intRowSpan, click:UI_searchReservation.cellClick}));
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_2_res", desc:paxName, width:"20%",  css:UI_commonSystem.strDefClass + " cursorPointer", rowSpan:intRowSpan, click:UI_searchReservation.cellClick}));
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_3_res", desc:jsonResults[i].pnrStatus, width:"8%",  align:"center", css:UI_commonSystem.strDefClass + " cursorPointer", rowSpan:intRowSpan, bold:true, textCss:UI_commonSystem.getStatusColor(jsonResults[i].pnrStatus), click:UI_searchReservation.cellClick}));		
							} else {
								tblRow.appendChild(UI_commonSystem.createCell({desc:jsonResults[i].pnrNo, width:"8%", align:"center", css:UI_commonSystem.strDefClass}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:jsonResults[i].paxName, width:"24%",  css:UI_commonSystem.strDefClass}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:jsonResults[i].pnrStatus, width:"8%",  align:"center", css:UI_commonSystem.strDefClass, bold:true, textCss:UI_commonSystem.getStatusColor(jsonResults[i].pnrStatus)}));
								
							}
							first =false;
						}
						
						if (DATA_ResPro.initialParams.viewReservationDetailsAllowed) {
							if(objD[x].openReturnSegment){
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_4_res_"+ x, desc:'OPENRT', width:"35%", colSpan:3,  align:"center", css:UI_commonSystem.strDefClass + " rowHeight cursorPointer", click:UI_searchReservation.cellClick}));
							} else {
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_4_res_"+ x, desc:objD[x].flightNo + " <b class='txtAirLineColor'>" + objD[x].airLine + "<\/b>", width:"9%",  align:"center", css:UI_commonSystem.strDefClass + " rowHeight cursorPointer", click:UI_searchReservation.cellClick,mouseOver:strFltInfoDisplayMouseOver, mouseOut:strFltInfoDisplayMouseOut}));
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_5_res_"+ x, desc:objD[x].departureDate + " " + objD[x].departureTime, width:"13%",  align:"center", css:UI_commonSystem.strDefClass + " cursorPointer", click:UI_searchReservation.cellClick}));
								tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_6_res_"+ x, desc:objD[x].arrivalDate + " " + objD[x].arrivalTime, width:"13%",  align:"center", css:UI_commonSystem.strDefClass + " cursorPointer", click:UI_searchReservation.cellClick}));
							}
							tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_7_res_"+ x, desc:objD[x].orignNDest, width:"8%",  align:"center", css:UI_commonSystem.strDefClass + " cursorPointer", click:UI_searchReservation.cellClick}));
							tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_8_res_"+ x, desc:objD[x].paxCount, width:"5%",  align:"center", css:UI_commonSystem.strDefClass + " cursorPointer", click:UI_searchReservation.cellClick}));
							tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_9_res_"+ x, desc:objD[x].cosDes, width:"9%",  align:"center", css:UI_commonSystem.strDefClass + " cursorPointer", click:UI_searchReservation.cellClick}));
							tblRow.appendChild(UI_commonSystem.createCell({id:(i + 1) + "_10_res_"+ x, desc:objD[x].displayStatus, width:"5%",  align:"center", css:UI_commonSystem.strDefClass + " cursorPointer", bold:true, textCss:UI_commonSystem.getStatusColor(objD[x].status), click:UI_searchReservation.cellClick}));
							
						} else {
							if(objD[x].openReturnSegment){
								tblRow.appendChild(UI_commonSystem.createCell({desc:'OPENRT', width:"35%", colSpan:3,  align:"center", css:UI_commonSystem.strDefClass + " rowHeight"}));
							} else {
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].flightNo + " <b class='txtAirLineColor'>" + objD[x].airLine + "<\/b>", width:"9%",  align:"center", css:UI_commonSystem.strDefClass + " rowHeight",mouseOver:strFltInfoDisplayMouseOver, mouseOut:strFltInfoDisplayMouseOut}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].departureDate + " " + objD[x].departureTime, width:"13%",  align:"center", css:UI_commonSystem.strDefClass}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].arrivalDate + " " + objD[x].arrivalTime, width:"13%",  align:"center", css:UI_commonSystem.strDefClass}));
							}
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].orignNDest, width:"8%",  align:"center", css:UI_commonSystem.strDefClass}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].paxCount, width:"5%",  align:"center", css:UI_commonSystem.strDefClass}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].cosDes, width:"9%",  align:"center", css:UI_commonSystem.strDefClass}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].displayStatus, width:"5%",  align:"center", css:UI_commonSystem.strDefClass, bold:true, textCss:UI_commonSystem.getStatusColor(objD[x].status)}));
							
						}
						
						
						x++;
						$("#tblSrarch").append(tblRow);	
					});
					i++;
				});
				if (i == recordsPerPage && pageNo > 1) {
					tblRow = document.createElement("TR");
					var html1 = '<a href="javascript:void(0)" onclick="UI_searchReservation.previousOnClick(' + pageNo + ')">&#171 Prev</a> ';
					var html2 = '<a href="javascript:void(0)" onclick="UI_searchReservation.nextOnClick(' + pageNo + ')">Next &#187;</a> ';
					UI_searchReservation.appendBlankCells(tblRow);
					tblRow.appendChild(UI_commonSystem.createCell({desc:html1, bold:true, align:"left"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:html2, bold:true, align:"left"}));
					$("#tblSrarch").append(tblRow);						
				
				} else if (i == recordsPerPage) {
					tblRow = document.createElement("TR");
					var html = '<a href="javascript:void(0)" onclick="UI_searchReservation.nextOnClick(' + pageNo + ')">Next &#187;</a> ';
					UI_searchReservation.appendBlankCells(tblRow);
					tblRow.appendChild(UI_commonSystem.createCell({desc:"", bold:true, align:"center"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:html, bold:true, align:"left"}));
					$("#tblSrarch").append(tblRow);					
				} else if (pageNo > 1) {
					tblRow = document.createElement("TR");
					var html = '<a href="javascript:void(0)" onclick="UI_searchReservation.previousOnClick(' + pageNo + ')">&#171 Prev</a> ';
					UI_searchReservation.appendBlankCells(tblRow);
					tblRow.appendChild(UI_commonSystem.createCell({desc:"", bold:true, align:"center"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:html, bold:true, align:"left"}));
					$("#tblSrarch").append(tblRow);					
				}
				if( DATA_ResPro.initialParams.viewReservationDetailsAllowed && top.blnAutoSearch){
					UI_searchReservation.cellClick();
				}
			}else{
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:top.strNOData, bold:true, align:"center"}));
				$("#tblSrarch").append(tblRow);	
			}
			
			if(DATA_ResPro.load == 'LOAD'){
				$("#frmRes").attr('target', "frmReservation");
				$("#frmRes").submit();
			}
		}else{
			if(response.success && response.reservations.length == 0){
				var pageNo = response.pageNo;
				if (pageNo > 1) {
					UI_searchReservation.searchOnClick();
				} else {
					showERRMessage('No matching reservations found');
				}
			}else{
				showERRMessage(response.messageTxt);
			}
		}
	}
	
	UI_searchReservation.appendBlankCells = function(tblRow){
		var length = 7;
		if (DATA_ResPro.initialParams.viewReservationDetailsAllowed) {
			length = 8;
		}
		for (var i = 0; i < length; i++) {
			tblRow.appendChild(UI_commonSystem.createCell({desc:"", bold:true, align:"center"}));			
		}		
	}
	
	UI_searchReservation.nextOnClick = function(pageNo){
		pageNo += 1;
		var data = {};
		data['pageNo'] = pageNo;
		$("#frmSrchBkg").ajaxSubmit({ dataType: 'json', data:data,
			beforeSubmit:UI_searchReservation.validate,	
			success: UI_searchReservation.returnCallSearchReservation,
			error:UI_commonSystem.setErrorStatus});
		return false;
	}

	UI_searchReservation.previousOnClick = function(pageNo){
		pageNo = pageNo - 1;
		var data = {};
		data['pageNo'] = pageNo;
		$("#frmSrchBkg").ajaxSubmit({ dataType: 'json', data:data,
			beforeSubmit:UI_searchReservation.validate,	
			success: UI_searchReservation.returnCallSearchReservation,
			error:UI_commonSystem.setErrorStatus});
		return false;
	}
	
	/*
	 * Cell Click 
	 */
	UI_searchReservation.cellClick = function(){
		var intRow = null;
		if (UI_searchReservation.intLastSelected != null){
			intRow = UI_searchReservation.intLastSelected.split("_")[0]
			$("#" + intRow + "_1_res").removeClass('rowSelected rowSelectedBDT rowSelectedBDB rowSelectedBDR rowSelectedBDL')
			$("#" + intRow + "_2_res").removeClass('rowSelected rowSelectedBDT rowSelectedBDB rowSelectedBDR')
			$("#" + intRow + "_3_res").removeClass('rowSelected rowSelectedBDT rowSelectedBDB rowSelectedBDR')
		
			$("#" + intRow + "_1_res").addClass('rowUnSelected thinBorderT thinBorderB thinBorderR thinBorderL')
			$("#" + intRow + "_2_res").addClass('rowUnSelected thinBorderT thinBorderB thinBorderR')
			$("#" + intRow + "_3_res").addClass('rowUnSelected thinBorderT thinBorderB thinBorderR')
		}
		
		if( DATA_ResPro.initialParams.viewReservationDetailsAllowed && top.blnAutoSearch){
			intRow = 1;
			top.blnAutoSearch = false;
		} else{
			intRow = this.id.split("_")[0];
		}
		
		
		$("#" + intRow + "_1_res").removeClass('rowUnSelected thinBorderT thinBorderB thinBorderR thinBorderL')
		$("#" + intRow + "_2_res").removeClass('rowUnSelected thinBorderT thinBorderB thinBorderR')
		$("#" + intRow + "_3_res").removeClass('rowUnSelected thinBorderT thinBorderB thinBorderR')
		
		$("#" + intRow + "_1_res").addClass('rowSelected rowSelectedBDT rowSelectedBDB rowSelectedBDR rowSelectedBDL')
		$("#" + intRow + "_2_res").addClass('rowSelected rowSelectedBDT rowSelectedBDB rowSelectedBDR')
		$("#" + intRow + "_3_res").addClass('rowSelected rowSelectedBDT rowSelectedBDB rowSelectedBDR')
		
		UI_searchReservation.intLastSelected = this.id;
		 
		$("#divReservation").show();		
		$("#pnrNO").val(jsonResults[intRow - 1].pnrNo);
		$("#groupPNRNO").val(jsonResults[intRow - 1].originatorPnr);
		$("#marketingAirlineCode").val(jsonResults[intRow - 1].marketingAirlineCode);
		$("#interlineAgreementId").val(jsonResults[intRow - 1].interlineAgreementId);
		$("#airlineCode").val(jsonResults[intRow - 1].airlineCode);
		$("#frmRes").attr('target', "frmReservation");
		$("#frmRes").submit();		
		UI_searchReservation.strLastPNRNo = jsonResults[intRow - 1].pnrNo;
		UI_PageShortCuts.initilizeMetaDataForModoficationBookingTab();
	}
	
	/*
	 * Expiry Date chk
	 */ 
	UI_searchReservation.expiryDateChk = function(){
		var strSep = "";
		if ($("#cardExpiry").val() != ""){
			var strVal = $("#cardExpiry").val();
			if (strVal.indexOf("/") != -1){strSep = "/";}
			if (strVal.indexOf("-") != -1){strSep = "-";}
			if (strVal.indexOf(".") != -1){strSep = ".";}
			$("#cardExpiry").val(dateChk("01" + strSep + strVal, "MM/YY"));
		}
	}
	
	/*
	 * Fill Drop Down Data for the Page
	 */
	UI_searchReservation.fillDropDownData = function(){
		$("#fromAirport").fillDropDown( { dataArray:top.arrAirports , keyIndex:0 , valueIndex:2, firstEmpty:true });
		$("#toAirport").fillDropDown( { dataArray:top.arrAirports , keyIndex:0 , valueIndex:2, firstEmpty:true });
		$("#bookingType").fillDropDown( { dataArray:top.arrBCType , keyIndex:0 , valueIndex:1, firstEmpty:true  });
	}
	
	/*
	 * Initialization
	 */
	UI_searchReservation.initialization = function(){
		UI_commonSystem.initializeMessage();
		
		$("#frmSrchBkg").reset();
		
		$("#chkExact").attr('checked', true);
		
		$("#divSrchResults").hide();
	}
	
	/*
	 * Auto Date generation
	 */
	UI_searchReservation.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 : dateChk("depatureDate"); break;
			case 1 : dateChk("returnDate"); break;
		}
	}
	
	UI_searchReservation.ViewSearch = function() {
		 UI_PageShortCuts.initilizeMetaDataForModificationSearch();
		$("#divReservation").hide();
	} 
	
	/*
	 * Hide Reservation
	 */
	UI_searchReservation.hideReservation = function(){
		$("#divReservation").hide();
	}
	
	  /** Flight Info tool tip display*/
	UI_searchReservation.displayFlightAddInfoToolTip = function() {
		  var sHtm  ="";  
		
		  if (this.id != "") {
			  var arrID = this.id.split("_");
			  var id = (new  Number(arrID[3])) - 0;
			  var recordId = (new  Number(arrID[0])) - 1;
			  
			  objD = jsonResults[recordId].flightInfo;
			  
			  var fltDuration = objD[id].flightDuration;
			  var fltModelDesc = objD[id].flightModelDescription;
			  var fltModel = objD[id].flightModelNumber;
			  var segmentCode = objD[id].orignNDest;
			  var flightNO = objD[id].flightNo;	
			  var specialNote = objD[id].remarks;
			  var stopOverDuration = objD[id].flightStopOverDuration;
			  var noOfStops = objD[id].noOfStops;
			  var arrivalTerminalName = objD[id].arrivalTerminal;
			  var departureTerminalName = objD[id].departureTerminal;
			  var csOcCarrierCode = objD[id].csOcCarrierCode;
			  
			  if(arrivalTerminalName == null){arrivalTerminalName = ""}
			  if(departureTerminalName == null){departureTerminalName = ""}
			  
			  var strData = "";

			  //for translation purpose
			  var strFlightNo = geti18nData('hover_Flight_No','Flight No');
			  var strSegmentCode = geti18nData('hover_Segment_Code','Segment Code');
			  var strArrivalTerminal = geti18nData('hover_Arrival_Terminal','Arrival Terminal');
			  var strDepartureTerminal = geti18nData('hover_DepartureTerminal','Departure Terminal');
			  var strFlightDuration = geti18nData('hover_Flight_Duration','Flight Duration');
			  var strNoOfStops = geti18nData('hover_No_of_Stops','No of Stops');
			  var strTransitDuration = geti18nData('hover_Transit_Duration','Transit Duration');
			  var strAircraftModel = geti18nData('hover_Aircraft_Model','Aircraft Model');
			  var strAdditionalNote = geti18nData('hover_Additional_Note','Additional Note');
			  var strOperatedBy = geti18nData('hover_Operated_By','Operated By');
				
			  	strData += "<font>"+strFlightNo+"&nbsp;:&nbsp;"+flightNO+"</font><br>";
				strData += "<font>"+strSegmentCode+"&nbsp;:&nbsp;"+segmentCode+"</font><br>";
				strData += "<font>"+strArrivalTerminal+"&nbsp;:&nbsp;"+arrivalTerminalName+"</font><br>";	
				strData += "<font>"+strDepartureTerminal+"&nbsp;:&nbsp;"+departureTerminalName+"</font><br>";	
				strData += "<font>"+strFlightDuration+"&nbsp;:&nbsp;"+fltDuration+"</font><br>";
				strData += "<font>"+strNoOfStops+"&nbsp;:&nbsp;"+noOfStops+"</font><br>";
				strData += "<font>"+strTransitDuration+"&nbsp;:&nbsp;"+stopOverDuration+"</font><br>";				
				//strData += "<font>Aircraft Model&nbsp;:&nbsp;"+fltModel+"</font><br>";
				strData += "<font>"+strAircraftModel+"&nbsp;:&nbsp;"+fltModelDesc+"</font><br>";
				strData += "<font>"+strAdditionalNote+"&nbsp;:&nbsp;"+specialNote+"</font><br>";	
				if(typeof(csOcCarrierCode) != "undefined" && csOcCarrierCode != "" && csOcCarrierCode != null){
					strData += "<font>"+strOperatedBy+"&nbsp;:&nbsp;"+csOcCarrierCode+"</font><br>";						
				}		
						
				sHtm += strData;	
		  } 
		  $("#search_flightAddInfoToolTipBody").html(sHtm);
		  
		  var pos = $("#" + this.id).offset();
		 
		  var topCor = pos.top -  ($("#search_flightAddInfoToolTipBody").find("font").length * 15 + 75);
		 
	      
	      $("#search_flightAddInfoToolTip").css({
	          left: (pos.left-100) + 'px',
	          top: topCor  + 'px'
	         
	      });      
	     $("#search_flightAddInfoToolTip").show(); 	  
		}
	  
	UI_searchReservation.hideFlightAddInfoToolTip = function() {
		  $("#search_flightAddInfoToolTip").hide();
	  }
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	