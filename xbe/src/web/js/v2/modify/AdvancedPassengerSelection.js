	/*
	 * Passenger Tab
	 */
	function UI_AdvancedPassengerSelection(){}
	
	UI_AdvancedPassengerSelection.jsonPaxAdtObj = {};
	UI_AdvancedPassengerSelection.jsonPaxIntlFltObj = {};
	UI_AdvancedPassengerSelection.arrError = [];
	
	var paxAdults = {};
	var paxInfants = {};
	
	var objPaxArrivalFltNo = new Array();
	var objPaxDeptFltNo = new Array();
	var objPnrPaxGroupId = new Array();
	
	var minDate = new Date();
	
	var adCount = 0;
	var chCount = 0;
	var inCount = 0;

	/*
	 * Contact Info Tab Page On Load
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		$("#btnEdit").decorateButton();
		$("#btnEdit").click(function() {UI_AdvancedPassengerSelection.editOnclick();});
		
		$("#btnConfirm").decorateButton();
		$("#btnConfirm").disable();
		$("#btnConfirm").click(function() {UI_AdvancedPassengerSelection.confirmOnclick();});
		
		$("#btnCancel").decorateButton();
		$("#btnCancel").click(function() {UI_AdvancedPassengerSelection.cancelOnclick();});
		
		UI_AdvancedPassengerSelection.onLoad();
		$("#1_pnrPaxArrivalFlightNumber").focus()
	});
	
	/*
	 * Pax details On Load
	 */
	UI_AdvancedPassengerSelection.onLoad = function(){
		if (DATA_ResPro.mode == 'CREATE') {
			paxAdults = opener.UI_tabPassenger.getAdultDetails();
			paxInfants = opener.UI_tabPassenger.getInfantDetails();
		}
		/***** IE Memory leak FIXEs  load opener object in the loading time****/
		UI_AdvancedPassengerSelection.jsonPaxAdtObj = opener.jsonPaxAdtObj;
		
		UI_AdvancedPassengerSelection.jsonPaxIntlFltObj = opener.jsonPaxIntlFltObj;
		
		UI_AdvancedPassengerSelection.arrError =  opener.top.arrError;
		
		$("#divHDPane1").html("Advanced Passenger Selection");
		UI_AdvancedPassengerSelection.fillPaneData();
		UI_AdvancedPassengerSelection.disableEnableControls(false);
		$("#btnEdit").hide();
		$("#btnEdit").disable();
		$("#btnConfirm").enable();
		
		
	}	


	
	UI_AdvancedPassengerSelection.fillPaneData = function(inParams){
		UI_AdvancedPassengerSelection.constructGridPane({id:"#tablPane1"});
		UI_AdvancedPassengerSelection.constructGridPaneTwo({id:"#tablPane2"});
		UI_AdvancedPassengerSelection.constructGridPaneThree({id:"#tablPane3"});
		var arrivalFlightNumbers = {};
		var uniqueArrivalflightNumbers = {};
		var departureFlightNumbers = {};
		var uniqueDepartureFlightNumbers = {};
		var pnrPaxGroupIds = {};
		var uniquePnrPaxGroupIds = {};
		var countArrival = 0;
		var countDeparture = 0;
		var countPnrPaxGroupIds = 0;
		
		
		var adultCount = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults.length;
		for(var i = 0; i < adultCount; i++) {
			if (UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber != null && UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber != "" && typeof(arrivalFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber]) == "undefined") {
				uniqueArrivalflightNumbers[countArrival] = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber;
				objPaxArrivalFltNo[countArrival] = {};
				objPaxArrivalFltNo[countArrival].arrivalFlightNumber = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber;
				countArrival++;
				arrivalFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber] = 0;
			}
			if (UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber != null && UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber != "" && typeof(departureFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber]) == "undefined") {
				uniqueDepartureFlightNumbers[countDeparture] = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber;
				objPaxDeptFltNo[countDeparture] = {};
				objPaxDeptFltNo[countDeparture].departureFlightNumber = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber;
				countDeparture ++;
				departureFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber] = 0;
			}
			if (UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId != null && UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId != "" && typeof(pnrPaxGroupIds[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId]) == "undefined") {
				uniquePnrPaxGroupIds[countPnrPaxGroupIds] = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId;
				objPnrPaxGroupId[countPnrPaxGroupIds] = {};
				objPnrPaxGroupId[countPnrPaxGroupIds].displayPnrPaxGroupId = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId;
				countPnrPaxGroupIds ++;
				pnrPaxGroupIds[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId] = 0;
			}
			
		}
		if (UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants != null) {
			var infantCount = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants.length;
			for(var j = 0; j < infantCount; j++) {
				if (UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalFlightNumber != null && UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalFlightNumber != "" && typeof(arrivalFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalFlightNumber]) == "undefined") {
					uniqueArrivalflightNumbers[countArrival] = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalFlightNumber;
					objPaxArrivalFltNo[countArrival] = {};
					objPaxArrivalFltNo[countArrival].arrivalFlightNumber = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalFlightNumber;
					countArrival++;
					arrivalFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalFlightNumber] = 0;
				}
				if (UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureFlightNumber != null && UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureFlightNumber != "" && typeof(departureFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureFlightNumber]) == "undefined") {
					uniqueDepartureFlightNumbers[countDeparture] = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureFlightNumber;
					objPaxDeptFltNo[countDeparture] = {};
					objPaxDeptFltNo[countDeparture].departureFlightNumber = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureFlightNumber;
					countDeparture ++;
					departureFlightNumbers[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureFlightNumber] = 0;
				}
				if (UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displayPnrPaxGroupId != null && UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displayPnrPaxGroupId != "" && typeof(pnrPaxGroupIds[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displayPnrPaxGroupId]) == "undefined") {
					uniquePnrPaxGroupIds[countPnrPaxGroupIds] = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displayPnrPaxGroupId;
					objPnrPaxGroupId[countPnrPaxGroupIds] = {};
					objPnrPaxGroupId[countPnrPaxGroupIds].displayPnrPaxGroupId = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displayPnrPaxGroupId;
					countPnrPaxGroupIds ++;
					pnrPaxGroupIds[UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxInfants[j].displayPnrPaxGroupId] = 0;
				}
			
			}
		}
		
		UI_commonSystem.fillGridData({id:"#tablPane1", data:objPaxArrivalFltNo});	
		UI_commonSystem.fillGridData({id:"#tablPane2", data:objPaxDeptFltNo});
		UI_commonSystem.fillGridData({id:"#tablPane3", data:objPnrPaxGroupId});
		
		UI_AdvancedPassengerSelection.populateSelectedValues();
	}
	
	UI_AdvancedPassengerSelection.populateSelectedValues = function(){
		var arrivalSelectedArr = UI_AdvancedPassengerSelection.jsonPaxIntlFltObj.arrivalFlights;
		var arrivalSelectedArrLength = UI_AdvancedPassengerSelection.jsonPaxIntlFltObj.arrivalFlights.length;
		var departureSelectedArr = UI_AdvancedPassengerSelection.jsonPaxIntlFltObj.DepartureFlights;
		var depatureSelectedArrLength = UI_AdvancedPassengerSelection.jsonPaxIntlFltObj.DepartureFlights.length;
		var pnrPaxGroupIdSelectedArr = UI_AdvancedPassengerSelection.jsonPaxIntlFltObj.pnrPaxGroupIds;
		var pnrPaxGroupIdelectedArrLength = UI_AdvancedPassengerSelection.jsonPaxIntlFltObj.pnrPaxGroupIds.length;
		var uniqueArrivalArrLength = objPaxArrivalFltNo.length;
		var uniqueDepartureArrLength = objPaxDeptFltNo.length;
		var uniquePnrPaxGruopIdArrLength = objPnrPaxGroupId.length;
		
		for (var i = 0 ; i < uniqueArrivalArrLength ; i++) {
			for (var j = 0 ; j < arrivalSelectedArrLength ; j++) {
				if (arrivalSelectedArr[j] != null && arrivalSelectedArr[j] != "" && objPaxArrivalFltNo[i].arrivalFlightNumber != null && objPaxArrivalFltNo[i].arrivalFlightNumber != "" && arrivalSelectedArr[j] == objPaxArrivalFltNo[i].arrivalFlightNumber) {
					$("#" + (i+1) + "_displaySelectArrival").attr('checked','checked');
				}
			}
		}
		
		for (var j = 0 ; j < uniqueDepartureArrLength ; j++) {
			for (var k = 0 ; k < depatureSelectedArrLength ; k++) {
				if (departureSelectedArr[k] != null && departureSelectedArr[k] != "" && objPaxDeptFltNo[j].departureFlightNumber != null && objPaxDeptFltNo[j].departureFlightNumber != "" && departureSelectedArr[k] == objPaxDeptFltNo[j].departureFlightNumber) {
					$("#" + (j+1) + "_displaySelectDeparture").attr('checked','checked');
				}
			} 
		}
		
		for (var j = 0 ; j < uniquePnrPaxGruopIdArrLength ; j++) {
			for (var k = 0 ; k < pnrPaxGroupIdelectedArrLength ; k++) { 
				if (pnrPaxGroupIdSelectedArr[k] != null && pnrPaxGroupIdSelectedArr[k] != "" && objPnrPaxGroupId[j].displayPnrPaxGroupId != null && objPnrPaxGroupId[j].displayPnrPaxGroupId != "" && pnrPaxGroupIdSelectedArr[k] == objPnrPaxGroupId[j].displayPnrPaxGroupId) {
					$("#" + (j+1) + "_displaySelectPnrPaxGroupId").attr('checked','checked');
				}
			} 
		}
	} 

	
	UI_AdvancedPassengerSelection.constructGridPane = function(inParams){
		//construct grid
		var columnNames = [];
		var columnModels = [];
		
					
			columnNames.push('');
			columnNames.push(geti18nData('FindRes_ArrivalFlightNumber','Arrival Flt No'));
			
			columnModels.push({name:'displaySelectArrival', index:'displaySelectArrival', width:18, align:"center", editable:true, edittype:"checkbox", editoptions:{className:"aa-input", value:"on"}});
			columnModels.push({name:'arrivalFlightNumber', index:'arrivalFlightNumber', width:132, align:"center"});
			

		$(inParams.id).jqGrid({
			datatype: "local",
			height: 200,
			width: 150,
			colNames:columnNames,
			colModel:columnModels,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
			}
		});
	}
	
	UI_AdvancedPassengerSelection.constructGridPaneTwo = function(inParams){
		
		
		var columnNames = ['', geti18nData('FindRes_ArrivalFlightNumber','Departure Flt No')];
		var columnModels = [
		    {name:'displaySelectDeparture', index:'displaySelectDeparture', width:18, align:"center", editable:true, edittype:"checkbox", editoptions:{className:"aa-input", value:"on"}},
			{name:'departureFlightNumber', index:'departureFlightNumber', width:132, align:"center"}
			];

		$(inParams.id).jqGrid({
			datatype: "local",
			height: 200,
			width: 150,
			colNames:columnNames,
			colModel:columnModels,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				
			}
		});
	}
	
	UI_AdvancedPassengerSelection.constructGridPaneThree = function(inParams){
		
		
		var columnNames = ['', geti18nData('FindRes_GroupID','Group Id')];
		var columnModels = [
		    {name:'displaySelectPnrPaxGroupId', index:'displaySelectPnrPaxGroupId', width:18, align:"center", editable:true, edittype:"checkbox", editoptions:{className:"aa-input", value:"on"}},
			{name:'displayPnrPaxGroupId', index:'displayPnrPaxGroupId', width:132, align:"center"}
			];

		$(inParams.id).jqGrid({
			datatype: "local",
			height: 200,
			width: 150,
			colNames:columnNames,
			colModel:columnModels,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				
			}
		});
	}
	
	
	/*
	 * Edit
	 */
	UI_AdvancedPassengerSelection.editOnclick = function(){
		UI_AdvancedPassengerSelection.disableEnableControls(false);		
		$("#btnEdit").disable();
		$("#btnConfirm").enable();
	}
	
	/*
	 * Disable Page Controls
	 */ 
	UI_AdvancedPassengerSelection.disableEnableControls = function(blnStatus){
		var adultCount = UI_AdvancedPassengerSelection.jsonPaxAdtObj.paxAdults.length;
		
	}
	
	/*
	 * Save
	 */
	UI_AdvancedPassengerSelection.confirmOnclick = function(){		
		var data = {};
		
		UI_commonSystem.showProgress();		
		
		var arrivalIndex = 0;
		var departureIndex = 0;
		var pnrPaxGroupIdIndex = 0;
		var arrivalFltLength = objPaxArrivalFltNo.length;
		var departFltLenght = objPaxDeptFltNo.length;
		var pnrPaxGroupIdLength = objPnrPaxGroupId.length;
		var selectedArrivalFlights = [];
		var selectedDepartureFlights = [];
		var selectedPnrPaxGroupIds = [];
		

		for ( var i = 0; i < arrivalFltLength; i++) {
			if ($("#" + (i+1) + "_displaySelectArrival").attr('checked')) {
				selectedArrivalFlights[arrivalIndex] = objPaxArrivalFltNo[i].arrivalFlightNumber;
				arrivalIndex++;
			}
		}
		
		for ( var j = 0; j < departFltLenght; j++) {
			if ($("#" + (j+1) + "_displaySelectDeparture").attr('checked')) {
				
				selectedDepartureFlights[departureIndex] = objPaxDeptFltNo[j].departureFlightNumber;
				departureIndex++;
			}
		}
		
		for ( var k = 0; k < pnrPaxGroupIdLength; k++) {
			if ($("#" + (k+1) + "_displaySelectPnrPaxGroupId").attr('checked')) {
				selectedPnrPaxGroupIds[pnrPaxGroupIdIndex] = objPnrPaxGroupId[k].displayPnrPaxGroupId;
				pnrPaxGroupIdIndex++;
			}
		}
		
		UI_AdvancedPassengerSelection.jsonPaxIntlFltObj["arrivalFlights"] = selectedArrivalFlights;
		UI_AdvancedPassengerSelection.jsonPaxIntlFltObj["DepartureFlights"] = selectedDepartureFlights;
		UI_AdvancedPassengerSelection.jsonPaxIntlFltObj["pnrPaxGroupIds"] = selectedPnrPaxGroupIds;
		opener.UI_tabBookingInfo.filterAdvancedPassengerSelection(UI_AdvancedPassengerSelection.jsonPaxIntlFltObj);
	
		UI_AdvancedPassengerSelection.disableEnableControls(true);
		UI_commonSystem.hideProgress();
		
		window.close();
	}
	
	
	/*
	 * Cancel
	 */
	UI_AdvancedPassengerSelection.cancelOnclick = function(){
		window.close();
	}
	
