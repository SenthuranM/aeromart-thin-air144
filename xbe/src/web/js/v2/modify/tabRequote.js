/*
 * *********************************************************************************************************
 * Description : XBE Requote
 * Author : Dilan Anuruddha
 * Version : 1.0
 ***********************************************************************************************************/
/*
 * Requote related methods
 */
function UI_requote() {

	var intialized = false;
	
	// var newFlightList = [];
	var removedFlightList = [];
	var newOndList = [];
	var removedOndList = [];
	var originalJourney = "";
	
	var blnShowChargeTypeOveride = false;
	var isSameFlightmodification = false;
	var skipUntuchedSegFareONDs = false;
	var skipFareReQuote = false;
	var addGroundSegment = false;
	var selectedFltSeg = null;
	var groundFltSegByFltSegId = {};
	UI_requote.overlappingOND = false;

	var chargeTypes = {
	    v : "V",
	    pfs : "PFS",
	    pf : "PF"
	}

	var ondStatus = {
	    NEW : "NEW",
	    CNF : "CNF",
	    OPENRT : "OPENRT",
	    OVERBOOK: 'OVERBOOK',
	    UN:'UN',
	    WL: 'WL'
	}

	var jsonOverride = [];

	var applyCustomCharges = true;
	var customChargesApplied = false;

	var classHidePaxAd = "hideAdPax";

	var modifySegmentClicked = false;
	var addSegmentClicked = false;
	var selectedOndIndex = -1;
	
	
	var strSegmentSummary = geti18nData('tabRequote_SegmentSummary','Segment Summary');
	var strOverrideChargesPerPaxSegment = geti18nData('tabRequote_OverideChargesPerPaxSegment','Override Charges - per PAX/Segment');
	var strPaxSummary = geti18nData('tabRequote_PaxSummary','Pax Summary');


	var jsPageConf = ({
		defaultMode : {
		    pane1 : strSegmentSummary,
		    pane2 : strOverrideChargesPerPaxSegment,
		    pane3 : strPaxSummary
		}
	});

	var jsPaneColumnHD = ({
		defaultMode : {
		    pane1 : {
		        column1 : "",
		        column2 : "Current OND<br />Charges",
		        column3 : "New OND<br />Charges"
		    },
		    pane3 : {
		        column1 : "Pax Name",
		        column2 : "Total Charges<br />Current",
		        column3 : "Total Charges<br />New",
		        column4 : "Total Payments<br />Current",
		        column5 : "Total C/F<br />New",
		        column6 : "Balance"
		    }
		}
	});

	var overiedFareMetaData = {
	    precentage : "_precentage",
	    chargeType : "_type",
	    min : "_prec_min",
	    max : "_prec_max",
	    overideOnlyCssTag : "confOverideOnly",
	    adultObj : {
		    isPresent : function() {
			    return false;
		    }
	    },
	    childObj : {
		    isPresent : function() {
			    return false;
		    }
	    },
	    infantObj : {
		    isPresent : function() {
			    return false;
		    }
	    },

	    setIDNames : function(sentID, type) {
		    // sets the dynamic id values
		    var temp_precentage = "" + sentID + this.precentage;
		    var temp_type = "selDefineCanType";
		    var temp_route = "selDefineRouteType";
		    var temp_min = "" + sentID + this.min;
		    var temp_max = "" + sentID + this.max;
		    if (type == "AD") {
			    this.adultObj = new OverrideObj(temp_type, temp_route,
			            temp_precentage, temp_min, temp_max, "AD");
			    return this.adultObj;
		    } else if (type == "CH") {
			    this.childObj = new OverrideObj(temp_type, temp_route,
			            temp_precentage, temp_min, temp_max, "CH");
			    return this.childObj;
		    } else if (type == "IN") {
			    this.infantObj = new OverrideObj(temp_type, temp_route,
			            temp_precentage, temp_min, temp_max, "IN");
			    return this.infantObj;
		    }
		    return null;
	    }

	};

	function OverrideObj(chargeType, routeType, percetageVal, min, max, type) {
		// maps to ConfirmUpdateOverrideChargesTO
		this.chargeType = chargeType;
		this.routeType = routeType;
		this.percentageVal = percetageVal;
		this.min = min;
		this.max = max;
		this.type = type;

		function isValid(val) {
			if (typeof (val) == "undefined" || val == null) {
				return false;
			}
		}

		this.isPresent = function() {
			if (isValid(this.chargeType) && isValid(this.routeType)
			        && isValid(this.percentageVal) && isValid(this.min)
			        && isValid(this.max) && isValid(this.type)) {
				return true;
			}
			return false;
		}
		function getChargeType() {
			return $('#' + this.chargeType).val();
		}
		function getPrecentage() {
			return $('#' + this.percentageVal).val();
		}
		function getMax() {
			return $('#' + this.max).val()
		}
		function getMin() {
			return $('#' + this.min).val()
		}
		this.setDataToJsonObj = function(objName) {
			var dataObj = {};
			dataObj[objName + '.chargeType'] = getChargeType();
			dataObj[objName + '.routeType'] = routeType;
			dataObj[objName + '.precentageVal'] = getPrecentage();
			dataObj[objName + '.min'] = getMin();
			dataObj[objName + '.max'] = getMax();
			return dataObj;
		};

		this.getCustomChargeJson = function() {
			var dataObj = {};
			dataObj['modificationChargeType'] = getChargeType();
			dataObj['cancellationChargeType'] = getChargeType();
			// dataObj['routeType'] = routeType;
			dataObj['chargePercentage'] = getPrecentage();
			dataObj['minCancellationAmount'] = getMin();
			dataObj['minModificationAmount'] = getMin();
			dataObj['maximumModificationAmount'] = getMax();
			dataObj['maximumCancellationAmount'] = getMax();
			return dataObj;
		};

		this.getChargeTypeValue = function() {
			return getChargeType();
		}

		this.validate = function() {
			if ($('#' + precentageVal).val() == "") {
				// showCommonError("WARNING",raiseError('XBE-ERR-77'));
				// showERRMessage(raiseError("XBE-ERR-01", "Departure
				// location"));
				showERRMessage(raiseError("XBE-ERR-01", "Percentage Value"));
				$('#' + precentageVal).focus();
				return false;
			}
			if (Number($('#' + precentageVal).val()) < 0
			        || Number($('#' + precentageVal).val()) > 100) {
				showERRMessage(raiseError("XBE-ERR-04", "Percentage Value"));
				$('#' + precentageVal).focus();
				return false;
			}
			if ($('#' + min).val() == "") {
				showERRMessage(raiseError("XBE-ERR-01", "Minimum Value"));
				$('#' + min).focus();
				return false;
			}
			if ($('#' + max).val() == "") {
				showERRMessage(raiseError("XBE-ERR-01", "Minimum Value"));
				$('#' + max).focus();
				return false;
			}
			if (Number($('#' + max).val()) < Number($('#' + min).val())) {
				showERRMessage(raiseError("XBE-ERR-05", "Minimum Value",
				        "Maximum Value"));
				$('#' + max).focus();
				return false;
			}
			return true;
		};

	}
	;

	function clearDataStructs() {
		// newFlightList = [];
		newOndList = [];
		removedOndList = [];
		populateExistingONDs();
		selectedFltSeg = null;
		groundFltSegByFltSegId = {};
	}

	this.initialize  = function() {
		if (!intialized) {
			intialized = true;
			// this.constructGrid({id:"#tblFlights"});

			$("#divLegacyRootWrapperPopUp").fadeIn("slow");

			// Charges Tab
			$("#btnRQCancel").decorateButton();			
			$("#btnRQCancel").unbind("click").bind("click", function() {
				UI_tabAnci.resetAnciData();				
				UI_requote.resetOverrideCharges();
				if(DATA_ResPro.blnIsFromNameChange){
					UI_modifyResRequote.loadHomePage();
				} else {
					UI_modifyResRequote.openTab(0);
				}
			});

			$("#btnRQReset").decorateButton();			
			$("#btnRQReset").unbind("click").bind("click", function() {
				resetOnclick();
			});

			$("#btnOverride").decorateButton();
			$("#btnOverride").attr('style', 'width:auto;');

			// TODO enable for override privilege
			if (!DATA_ResPro.initialParams.hasOverrideCnxModChargePrivilege) {
				$("#btnOverride").hide();
			}

			$("#btnOverride").click(function() {
				overrideOnClick();
			});

			$("#btnApply").decorateButton();
			$("#btnApply").disable();
			$("#btnApply").click(function() {
				applyOnClick();
			});
			$("#flexiInfo").hide();

			$("#btnCalculateNew").decorateButton();
			$("#btnCalculateNew").click(function() {
				// UI_confirmUpdate.onCalculateNewClick();
			});
			$('#divCalculateNewCharge').hide();
			$("#txtCNXAdt").numeric({
				allowDecimal : true
			});

			$("#selDefineCanType").change(function(targetObj) {
				chargeTypeOnChange(targetObj);
			});
			$("#selDefineRouteType").change(function() {
				routeTypeOnChange(this);
			});
			$("#btnRQConfirm").decorateButton();
			$("#btnRQConfirm").unbind("click").bind("click", function() {
				callRequoteConfirm();
			});

			$('#selDefineRouteType').disable();		
			
		}
		UI_requote.showHideUserNoteClassification();
		UI_requote.createFlightSegments();
		UI_requote.resetOverrideCharges();
		UI_tabAnci.resetAnciData();
		
	}

	this.ready = function() {
		this.initialize();
		this.continueToAnci();
	};
	
	this.resetOverrideCharges = function() {
		applyCustomCharges = false;
		customChargesApplied = false;
	};
	
	this.continueToAnci = function(){
		UI_commonSystem.showProgress();
		
		var data = UI_tabSearchFlights.createSearchParams('searchParams');
		data['requote'] = true;
		data['flightRPHList'] = $.toJSON(this.populateFlightSegmentList());
		data['flexiAlertInfo'] = DATA_ResPro.jsonFlexiAlerts;
		data['insuranceAvailable'] = DATA_ResPro.resHasInsurance;
		data['insFltSegments'] = $.toJSON(this.getConfirmedExistingFlightSegments());
		data['modifyingSegmentsIds'] = UI_requote.getModifyingFlightSegmentsIds();
		data['pnr'] = DATA_ResPro.reservationInfo.PNR;
		data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();
		data['resPaxInfo']= $.toJSON(DATA_ResPro.resPaxs);
		data['mulipleMealsSelectionEnabled'] = DATA_ResPro.initialParams.multiMealEnabled;		
		data['onHoldBooking'] = (DATA_ResPro.resPaxs[0].status == "OHD") ? true : false;
		
		var isOpenRTBooking = (jsonOpenRetInfo != null) ? true : false;
		if(!isOpenRTBooking){			
			isOpenRTBooking = UI_tabSearchFlights.createSearchParams('fareQuoteParams')['fareQuoteParams.openReturn'];
		}	
		data['isOpenReturnReservation'] =  isOpenRTBooking;
		UI_commonSystem.getDummyFrm().ajaxSubmit({url:'ancillaryAvailability.action', 
				dataType: 'json', 
				processData: false, 
				data:data, 
				success: function (res){
					UI_commonSystem.hideProgress();
					if(res.success){
						//TODO:RW remove this, use common method
						//var paxArr = UI_requote.transformPax(DATA_ResPro.resPaxs);
						//paxArr = $.airutil.sort.quickSort(paxArr, UI_requote.paxSorter);
						var paxArr = UI_tabAnci.transformPax(DATA_ResPro.resPaxs);
						paxArr = $.airutil.sort.quickSort(paxArr, UI_tabAnci.paxSorter);
						var infArr = [];
						var ninfArr = [];
						for(var i=0 ; i < paxArr.length; i++){
							if(paxArr[i].paxType == 'IN'){
								infArr[infArr.length] = paxArr[i];
							}else{
								ninfArr[ninfArr.length] = paxArr[i];
							}
						}
						res.paxAdults = ninfArr;
						res.paxInfants = infArr;
						res.contactInfo = DATA_ResPro.resContactInfo;						
						UI_tabAnci.pnr =  DATA_ResPro.reservationInfo.PNR;
						UI_tabAnci.isRequote = true;
						UI_tabAnci.prepareAncillary(res, jsonFareQuoteSummary);
					}else{			
						showERRMessage(res.messageTxt);
					}
				}, 
				error:UI_commonSystem.setErrorStatus
		});
	}
	
	this.getAllQuotedFlights = function(){
		var flightList = [];
		var ondList = getONDList();
		var rebookSeq = 0;
		for ( var i = 0; i < ondList.length; i++) {
				var ondFlights = ondList[i].getFlights();
				if(ondFlights != null){
					for ( var j = 0; j < ondFlights.length; j++) {
						if (skipUntuchedSegFareONDs == "true") {
							ondFlights[j]['ondSequence'] = rebookSeq;
							rebookSeq++;
						} else {
							ondFlights[j]['ondSequence'] = i;	
						}
					}
				}
				flightList = $.merge(flightList, ondFlights);
		}
		
		return flightList;
	}
	
	this.populateFlightSegmentList = function(){
		var flightList = [];
		var ondList = getONDList();
		var rebookSeq = 0;
		for ( var i = 0; i < ondList.length; i++) {
			if(ondList[i].getStatus() != ondStatus.CNF 
					&& ondList[i].getBookingType() != ondStatus.OPENRT){				
				var ondFlights = ondList[i].getFlights();
				if(ondFlights != null){
					for ( var j = 0; j < ondFlights.length; j++) {
						if (skipUntuchedSegFareONDs == "true") {
							ondFlights[j]['ondSequence'] = rebookSeq;
							rebookSeq++;
						} else {
							ondFlights[j]['ondSequence'] = i;	
						}
					}
				}
				flightList = $.merge(flightList, ondFlights);
			}
		}
		
		return flightList;
	}
	
	this.getModifyingFlightSegmentsIds = function(){
		var ondList = getONDList();
		var modifyingSegIds = "";
		for ( var index = 0; index < ondList.length; index++) {
			var modifyingRPHList = ondList[index].getModifiedResSegList();			
			if(modifyingRPHList != null && modifyingRPHList != ""){
				for ( var j = 0; j < modifyingRPHList.length; j++) {
					var pnrSegId = modifyingRPHList[j].split("$")[2];
					var resSegment = UI_tabSearchFlights.getResSegment(pnrSegId);
					if(resSegment!=null){
						var fltRPH = resSegment.flightSegmentRefNumber;
						UI_requote.modifyingFltSegHash[fltRPH] = resSegment;
						modifyingSegIds += ',' + fltRPH.split("$")[2];
					}
				}
			}
		}
		
		return modifyingSegIds;
	}
	
	function resetOnclick() {
		customChargesApplied = false;
		UI_requote.balanceQuery({
			reset : true
		});
	}

	function chargeTypeOnChange(obj) {
		if (blnShowChargeTypeOveride) {
			var currVal = $('#selDefineCanType').val();
			if ($('#selDefineCanType').val() == "") {
				showOverideChargeData(false);
				$('#selDefineRouteType').hide();
				$('#lblRouteType').hide();
				$('#selDefineRouteType').val("TOT");
				$('.confValueField').disable();
				// }else if(chargeTypes.v == $('#selDefineCanType').val() &&
				// strConfirmUpdateMode == "RESCANCEL" ){
				// UI_confirmUpdate.showOverideChargeData(false);
				// $('#lblRouteType').show();
				// $('#selDefineRouteType').enable();
				// $('.confValueField').enable();
			} else if (chargeTypes.v == $('#selDefineCanType').val()) {
				showOverideChargeData(false);
				$('#selDefineRouteType').hide();
				$('#lblRouteType').hide();
				$('#selDefineRouteType').val("TOT");
				$('.confValueField').enable();
			} else {
				$('#selDefineRouteType').hide();
				$('#lblRouteType').hide();
				$('#selDefineRouteType').val("TOT");
				showOverideChargeData(true);
				$('.confValueField').disable();
			}
			$('.overideDisplay').show();
			$('#lblRouteType').show();
			$('#selDefineCanType').val(currVal);
		}
	}

	function routeTypeOnChange(obj) {
		var currVal = obj.value;
		if ($('#selDefineRouteType').val() == "") {
			showOverideChargeData(false);
			$('.confValueField').disable();
		} else {
			showOverideChargeData(false);
			$('.confValueField').enable();
		}

		if (blnShowChargeTypeOveride) {
			$('.overideDisplay').show();
			$('#lblRouteType').show();
			$('#selDefineRouteType').val(currVal);
		} else {
			$('#lblRouteType').show();
			$('#selDefineRouteType').show();
			$('#selDefineRouteType').enable();
			$('#selDefineRouteType').val(currVal);
		}

	}

	function applyOnClick() {
		applyCustomCharges = true;
		customChargesApplied = true;
		UI_commonSystem.showProgress();
		
		 $('#txtCNXHDNAdt').val($('#txtCNXAdt').val());
		 $('#txtCNXHDNChld').val($('#txtCNXChld').val());
		 $('#txtCNXHDNInf').val($('#txtCNXInf').val());		
		// UI_confirmUpdate.blnOverrideApplied = true;
		// var strJS = "[";

		// var objPaneCntrl = jsonOverride;
		// var i = 0;
		// $.each(objPaneCntrl, function() {
		// if (i != 0) {
		// strJS += ","
		// }
		// strJS += "{"
		// strJS += "\"cntrlID\":\"" + objPaneCntrl[i].id + "\",";
		// strJS += "\"" + objPaneCntrl[i].id + "\":\""
		// + $("#" + objPaneCntrl[i].id).val() + "\"";
		// strJS += "}"
		// i++;
		// });

		// loadConfirmUpdate(false);
		UI_requote.balanceQuery();
	}

	function overrideOnClick() {
		var objPaneCntrl = jsonOverride;
		var intLen = Number(objPaneCntrl.length);
		var i = 0;
		if (intLen > 0) {
			do {
				$("#" + objPaneCntrl[i].id).enable();
				i++;
			} while (--intLen);
		}
		$("#" + objPaneCntrl[0].id).focus();
		$("#btnApply").enable();
		$("#btnOverride").disable();
		showOverideChargeData(true);
		$('#selDefineCanType').trigger('change');
		if (UI_requote.defaultCnxAdultCharge == "" || typeof(UI_requote.defaultCnxAdultCharge) == 'undefined') {
			UI_requote.defaultCnxAdultCharge = $("#txtCNXAdt").val();
		}
		if (UI_requote.defaultCnxChildCharge == "" || typeof(UI_requote.defaultCnxChildCharge) == 'undefined') {
			UI_requote.defaultCnxChildCharge = $("#txtCNXChld").val();
		}
		if (UI_requote.defaultInfantCharge == "" || typeof(UI_requote.defaultInfantCharge) == 'undefined') {
			UI_requote.defaultInfantCharge = $('#txtCNXInf').val();
		}
	}

	function callRequoteConfirm() {
		if(!validateData()){
			return false;
		}
		
		UI_tabSearchFlights.disableEnablePageControls(false);
		var data = UI_tabSearchFlights.createFareQuoteParams();
		data['fareQuoteParams.ondListString'] = $.toJSON(UI_requote
		        .getONDSearchDTOList());
		data['fareQuoteParams.ticketValidTillStr'] = UI_requote
		        .getTicketValidTill();
		data['fareQuoteParams.lastFareQuoteDateStr'] = UI_requote
		        .getLastFareQuoteDate();
		delete data['fareQuoteParams.bookingClassCode'] ;
		
		data['balanceQueryData'] = $.toJSON(getBalanceQueryData());
		data['flightRPHList'] = $.toJSON(UI_requote.getAllQuotedFlights());
		
		if (DATA_ResPro.reservationInfo.status == 'OHD'
				|| DATA_ResPro.reservationInfo.status == 'CNX') {
			data['openSegments'] = $.toJSON(getAllSegmentsForOHDRelCal());
		}
		data['selCurrency'] = UI_modifyResRequote.getSelectedCurrencyCode();
		data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
		if(DATA_ResPro.blnIsFromNameChange){
			data['paxNameDetails'] = DATA_ResPro.paxNameDetails;
		}
		
		UI_commonSystem.showProgress();
		UI_commonSystem.getDummyFrm().ajaxSubmit({
		    dataType : 'json',
		    data : data,
		    url : "requoteConfirm.action",
		    success : requoteConfirmReturn,
		    error : UI_commonSystem.setErrorStatus
		});
	}

	function requoteConfirmReturn(response) {
		if (response.success) {
			if (response.noPay) {
				DATA_ResPro.reservationInfo.groupPNR = response.groupBookingReference;
				UI_modifyResRequote.loadHomePage();
			} else {
				UI_tabPayment.isRequote = true;
				UI_tabPayment.ready();
				UI_tabPayment.updatePayemntTabTO(response);
				UI_modifyResRequote.tabDataRetrive.tab3 = false;
				UI_modifyResRequote.tabStatus.tab3 = true;
				UI_modifyResRequote.openTab(3);
				UI_tabPayment.paymentDetailsOnLoadCall();
			}
		} else {
			showCommonError("ERROR", response.messageTxt);
			UI_commonSystem.readOnly(false);
		}

		UI_commonSystem.hideProgress();

	}

	function getBalanceQueryData(param) {
		
		param = $.extend({
			reset : false
		}, param || {});
		var balanceData = {
		    removedSegmentIds : getRemovedResSegmentRPHList(),
		    pnr : '',
		    groupPnr : false,
		    version : '',
		    customCharges : null,
		    sameFlightModification : isSameFlightmodification		    
		};
		balanceData.pnr = DATA_ResPro.reservationInfo.PNR;
		balanceData.groupPnr = DATA_ResPro.reservationInfo.groupPNR;
		balanceData.version = DATA_ResPro.reservationInfo.version;
		balanceData.userNotes = $('#txtUserNote').val();
		balanceData.userNoteType = $('#selClassification').val();
		
		//process existing flight-segment vs bus-segment combination
		updateGroundFltSegByFltSegMap();

		balanceData.groundFltSegByFltSegId = groundFltSegByFltSegId;
		
		if (!param.reset) {
			var customCharges = getCustomCharges();
			if (customCharges != null) {
				balanceData.customCharges = customCharges;
			}
		}

		return balanceData;

	}
	
	function updateGroundFltSegByFltSegMap(){
		var segments = UI_requote.getAllsegments();
		var existBusSegByPnrSegId ={};
		$.each(segments, function(key, flt) {
			if(flt.busSegment && flt.status == "CNF" ){
				var flightSegId = UI_requote.getFlightSegIdFromRPH(flt.segmentRPH[0]);
				var ondInfo = UI_requote.getONDSearchDTOList()[key];
				var pnrSegId = UI_requote.getFlightSegIdFromRPH(ondInfo.existingResSegRPHList[0]);
				existBusSegByPnrSegId[pnrSegId]=flightSegId;
			}			
		});
		
		
		$.each(segments, function(key, flt) {
			if(!flt.busSegment && flt.status == "CNF"){				
				var busPnrSegId =  flt.groundStationPnrSegmentID[0];				
				var flightSegId = UI_requote.getFlightSegIdFromRPH(flt.segmentRPH[0]);
				
				if(existBusSegByPnrSegId!=null && existBusSegByPnrSegId[busPnrSegId]!=null){					
					if(groundFltSegByFltSegId[flightSegId]==undefined || groundFltSegByFltSegId[flightSegId]==null){
						groundFltSegByFltSegId[flightSegId] = existBusSegByPnrSegId[busPnrSegId];	
					}
				}			

			}			
		});
	}
	
	function validateData(){
		if(DATA_ResPro.initialParams.userNotesMandatory){
			if($('#txtUserNote').val() == ""){
				showCommonError("ERROR", "User Note cannot be left blank.");
				return false;
			}
		}
		return true;
	}	

	function isNullOrEmpty(val){
		if(val == undefined || val == null 
				|| val == "undefined" || trim(val) == ""){
			return true;
		}
		return false;
	}
	
	function getCustomCharges() {
		var obj = null;
		if (applyCustomCharges || customChargesApplied) {
			obj = {};

			if(!isNullOrEmpty($('#txtCNXHDNAdt').val())) {
				obj['customAdultCCharge'] = $('#txtCNXHDNAdt').val();
				obj['customAdultMCharge'] = $('#txtCNXHDNAdt').val();
				obj['defaultCustomAdultCharge'] = UI_requote.defaultCnxAdultCharge;
			}
			
			if(!isNullOrEmpty($('#txtCNXHDNChld').val())) {
				obj['customChildCCharge'] = $('#txtCNXHDNChld').val();
				obj['customChildMCharge'] = $('#txtCNXHDNChld').val();
				obj['defaultCustomChildCharge'] = UI_requote.defaultCnxChildCharge;
			}
			
			if(!isNullOrEmpty($('#txtCNXHDNInf').val())) {
				obj['customInfantCCharge'] = $('#txtCNXHDNInf').val();
				obj['customInfantMCharge'] = $('#txtCNXHDNInf').val();
				obj['defaultCustomInfantCharge'] = UI_requote.defaultInfantCharge;
			}
			var adult = overiedFareMetaData.adultObj;
			var child = overiedFareMetaData.childObj;
			var infant = overiedFareMetaData.infantObj;
			if (adult.isPresent()) {
				obj['customAdultChargeTO'] = adult.getCustomChargeJson();
				obj['defaultCustomAdultCharge'] = UI_requote.defaultCnxAdultCharge;
				if (adult.getChargeTypeValue() == 'V') {
					obj['absoluteValuesPassed'] = true;
				}
			} else {
				obj['absoluteValuesPassed'] = true;
			}
			if (child.isPresent()) {
				obj['customChildChargeTO'] = child.getCustomChargeJson();
				if (child.getChargeTypeValue() == 'V') {
					obj['absoluteValuesPassed'] = true;
				}
			} else {
				obj['absoluteValuesPassed'] = true;
			}
			if (infant.isPresent()) {
				obj['customInfantChargeTO'] = infant.getCustomChargeJson();
				if (infant.getChargeTypeValue() == 'V') {
					obj['absoluteValuesPassed'] = true;
				}
			} else {
				obj['absoluteValuesPassed'] = true;
			}
			
			obj['routeType'] = $('#selDefineRouteType').val();			
		}

		return obj;
	}

	this.balanceQuery = function(param) {
		// TODO DILAN validation
		UI_tabSearchFlights.disableEnablePageControls(false);
		var data = UI_tabSearchFlights.createFareQuoteParams();
		data['fareQuoteParams.ondListString'] = $.toJSON(UI_requote
		        .getONDSearchDTOList());
		data['fareQuoteParams.ticketValidTillStr'] = UI_requote
		        .getTicketValidTill();
		data['fareQuoteParams.lastFareQuoteDateStr'] = UI_requote
		        .getLastFareQuoteDate();
		delete data['fareQuoteParams.bookingClassCode'] ;
		data['balanceQueryData'] = $.toJSON(getBalanceQueryData(param));
		
		data['flightRPHList'] = $.toJSON(this.getAllQuotedFlights());
		data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
		
		if(DATA_ResPro.blnIsFromNameChange){
			data['paxNameDetails'] = DATA_ResPro.paxNameDetails;
		}
		UI_commonSystem.showProgress();
		UI_commonSystem.getDummyFrm().ajaxSubmit({
		    dataType : 'json',
		    data : data,
		    url : "requoteBalance.action",
		    success : returnBalanceQuery,
		    error : UI_commonSystem.setErrorStatus
		});
	}
	

	function returnBalanceQuery(response) {
		applyCustomCharges = false;
		var pageConfig = {
		    pane1 : strSegmentSummary,
		    pane2 : strOverrideChargesPerPaxSegment,
		    pane3 : strPaxSummary
		}
		if (response.success) {
			var data = {
			    paxSummary : response.paxSummaryList,
			    chargesList : response.updateCharge.chargesList
			}
			jsonOverride = response.overrideCharges;

			$("#divHDPane1").html(pageConfig.pane1);
			$("#divHDPane2").html(pageConfig.pane2);
			$("#divHDPane3").html(pageConfig.pane3);

			$("#divCredit").html(response.updateCharge.displayCredit);
			$("#divDue").html(response.updateCharge.displayDue);

			fillPaneData({
			    type : 'defaultMode',
			    data : data
			});
			constructPane2({
				data : response.overrideCharges
			});

			//update override charges
			 $('#txtCNXHDNAdt').val($('#txtCNXAdt').val());
			 $('#txtCNXHDNChld').val($('#txtCNXChld').val());
			 $('#txtCNXHDNInf').val($('#txtCNXInf').val());		
			// is only add segment is done disable charge override option
			if (response.addSegment) {
				$("#lblRouteType").hide();
				$("#selDefineRouteType").hide();
				$("#divHDPane2").hide();
				$("#tablPane2").hide();
				$("#btnOverride").hide();
				$("#btnApply").hide();
			} else {
				$("#tablPane2").show();
				if (DATA_ResPro.initialParams.hasOverrideCnxModChargePrivilege) {
					$("#btnOverride").show();
					$("#btnApply").show();
				} else {
					$("#btnOverride").hide();
					$("#btnApply").hide();
				}
				//$("#selDefineRouteType").val('');
				if (DATA_ResPro.initialParams.hasOverrideCnxModChargePrivilege) {
					$("#btnOverride").enable();
				}
				$("#btnApply").disable();
			}
			UI_modifyResRequote.tabDataRetrive.tab2 = true;
			UI_modifyResRequote.tabStatus.tab2 = true;
			UI_modifyResRequote.openTab(2);
			if(DATA_ResPro.blnIsFromNameChange){
				$("#divBookingTabs").tabs({disabled:[0,1]});
			}
		} else {
			showCommonError("ERROR", response.messageTxt);
			UI_commonSystem.readOnly(false);
			if(DATA_ResPro.blnIsFromNameChange){
				UI_modifyResRequote.loadHomePage();
			}
		}

		UI_commonSystem.hideProgress();
	}

	function fillPaneData(inParams) {
		constructGridPane1({
		    id : "#tablPane1",
		    columnHD : eval("jsPaneColumnHD." + inParams.type)
		})
		UI_commonSystem.fillGridData({
		    id : "#tablPane1",
		    data : inParams.data.chargesList
		})

		constructGridPane3({
		    id : "#tablPane3",
		    columnHD : eval("jsPaneColumnHD." + inParams.type)
		})
		UI_commonSystem.fillGridData({
		    id : "#tablPane3",
		    data : inParams.data.paxSummary
		})
	}

	function constructGridPane1(inParams) {
		// construct grid
		
		var objHD = inParams.columnHD.pane1;
		$(inParams.id).jqGrid({
		    datatype : "local",
		    height : 200,
		    width : 400,
		    colNames : [ objHD.column1, geti18nData('tabRequote_CurrentOndCharges' ,objHD.column2), geti18nData('tabRequote_NewOndCharges' ,objHD.column3) ],
		    colModel : [ {
		        name : 'displayDesc',
		        index : 'displayDesc',
		        width : 130,
		        align : "left"
		    }, {
		        name : 'displayOldCharges',
		        index : 'displayOldCharges',
		        width : 100,
		        align : "right"
		    }, {
		        name : 'displayNewCharges',
		        index : 'displayColumn3',
		        width : 100,
		        align : "right"
		    } ],
		    imgpath : UI_commonSystem.strImagePath,
		    multiselect : false,
		    viewrecords : true,
		    onSelectRow : function(rowid) {
		    }
		});
	}
	/*
	 * Construct Pane2
	 */
	function constructPane2(inParams) {
		$("#tablPane2").find("tr").remove();
		var objPaneCntrl = inParams.data;
		var intLen = objPaneCntrl.length;

		var i = 0;
		var tblRow = null;
		var tblHEadingRow = null;
		var strHTMLText = "";
		var strHTMLTextPrecentage = "";
		var strHTMLTextMin = "";
		var strHTMLTextMax = "";
		blnShowChargeTypeOveride = DATA_ResPro.initialParams.blnShowChargeTypeOveride;
		$("#tablPane2").append(createOverideCellTop());
		if (intLen > 0) {
			do {
				strHTMLText = "";
				var paxTypeObj = overiedFareMetaData.setIDNames(
				        objPaneCntrl[i].id, objPaneCntrl[i].paxType);
				var valCls = "";

				// if(strConfirmUpdateMode == 'REMOVEPAX' && paxTypeObj.type ==
				// "AD" &&
				// (opener.UI_tabBookingInfo.jsSltdAdt == "" ||
				// opener.UI_tabBookingInfo.jsSltdAdt == null)){
				// valCls = UI_confirmUpdate.classHidePaxAd;
				// }

				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({
				    desc : objPaneCntrl[i].description + " :",
				    width : "35%",
				    classVal : valCls
				}));
				if (!blnShowChargeTypeOveride) { // if atleaset one type has
					// percentage. should show.
					blnShowChargeTypeOveride = objPaneCntrl[i].blnShowChargeTypeOveride;
				}
				
				if(blnShowChargeTypeOveride && paxTypeObj==null){
					blnShowChargeTypeOveride = false;
				}
				
				switch (objPaneCntrl[i].type) {
				case "textBox":					
					
					strHTMLText = "<input type='text' id='"
					        + objPaneCntrl[i].id
					        + "' name='"
					        + objPaneCntrl[i].id
					        + "'  class='aa-input confValueField aa-numericOnly "
					        + valCls
					        + "' style='width:50px; text-align:right;' value='"
					        + objPaneCntrl[i].value + "'>"
					if (blnShowChargeTypeOveride) {
						setPrecentageTypeDD(objPaneCntrl[i].chargeType);
						strHTMLTextPrecentage = "<input type='text' id='"
						        + paxTypeObj.precentageVal
						        + "' name='"
						        + paxTypeObj.precentageVal
						        + "'  class='aa-input aa-numericOnly "
						        + overiedFareMetaData.overideOnlyCssTag
						        + " "
						        + valCls
						        + " ' style='width:50px; text-align:right;' value='"
						        + objPaneCntrl[i].precentageVal + "'></input>"
						strHTMLTextMin = "<input type='text' id='"
						        + paxTypeObj.min
						        + "' name='"
						        + paxTypeObj.min
						        + "'  class='aa-input aa-numericOnly "
						        + overiedFareMetaData.overideOnlyCssTag
						        + " "
						        + valCls
						        + "' style='width:40px; text-align:right;' value='"
						        + objPaneCntrl[i].min + "'></input>"
						strHTMLTextMan = "<input type='text' id='"
						        + paxTypeObj.max
						        + "' name='"
						        + paxTypeObj.max
						        + "'  class='aa-input aa-numericOnly "
						        + overiedFareMetaData.overideOnlyCssTag
						        + " "
						        + valCls
						        + "' style='width:40px;text-align:right;' value='"
						        + objPaneCntrl[i].max + "'></input>"
					}
					break;
				}

				tblRow.appendChild(UI_commonSystem.createCell({
				    desc : strHTMLText,
				    width : "15%"
				}));
				if (blnShowChargeTypeOveride) {
					tblRow.appendChild(UI_commonSystem.createCell({
					    desc : strHTMLTextPrecentage,
					    width : "18%"
					}));
					tblRow.appendChild(UI_commonSystem.createCell({
					    desc : strHTMLTextMin,
					    width : "13%"
					}));
					tblRow.appendChild(UI_commonSystem.createCell({
					    desc : strHTMLTextMan,
					    width : "13%"
					}));
				}
				$("#tablPane2").append(tblRow);
				$("#" + objPaneCntrl[i].id).disable();
				i++;
			} while (--intLen);
		}
		showOverideChargeData(false);
		$("." + classHidePaxAd).hide();
		if (blnShowChargeTypeOveride) {
			$('.confValueField').disable();
		}
		applyEventToDynamicInputs();

	}

	function applyEventToDynamicInputs() {
		$('.aa-numericOnly').bind('keypress', (function() {
			JqValidations.kpLikeDecimal(this.id)
		}));
		$('.aa-numericOnly').bind('keyup', (function() {
			JqValidations.kpLikeDecimal(this.id)
		}));

	}
	/*
	 * Construct Grid
	 */
	function constructGridPane3(inParams) {
		// construct grid
		var objHD = inParams.columnHD.pane3;
		$(inParams.id).jqGrid(
		        {
		            datatype : "local",
		            height : 200,
		            width : 830,
		            colNames : [ geti18nData('tabRequote_PassengeName',objHD.column1), geti18nData('tabRequote_TotalCurrentCharges',objHD.column2), geti18nData('tabRequote_TotalNewCharges',objHD.column3),
		                         geti18nData('tabRequote_TotalCurrentPayment',objHD.column4), geti18nData('tabRequote_TotalCFNew',objHD.column5), geti18nData('tabRequote_Balance',objHD.column6)],
		            colModel : [ {
		                name : 'displayPaxName',
		                index : 'displayPaxName',
		                width : 250,
		                align : "left"
		            }, {
		                name : 'displayTotalChargesCurrent',
		                index : 'displayTotalChargesCurrent',
		                width : 110,
		                align : "right"
		            }, {
		                name : 'displayTotalChargesNew',
		                index : 'displayTotalChargesNew',
		                width : 110,
		                align : "right"
		            }, {
		                name : 'displayTotalPaymentsCurrent',
		                index : 'displayTotalPaymentsCurrent',
		                width : 110,
		                align : "right"
		            }, {
		                name : 'displayTotalCFNew',
		                index : 'displayTotalCFNew',
		                width : 110,
		                align : "right"
		            }, {
		                name : 'displayBalance',
		                index : 'displayBalance',
		                width : 110,
		                align : "right"
		            } ],
		            imgpath : UI_commonSystem.strImagePath,
		            multiselect : false,
		            viewrecords : true,
		            onSelectRow : function(rowid) {
		            }
		        });
	}

	function createOverideCellTop() {
		var tblHEadingRow = "";
		tblHEadingRow = document.createElement("TR");
		var strHTMLText1 = "<label class = 'txtBold' >&nbsp; </label>";
		var strHTMLText2 = "<label class = 'txtBold' >&nbsp; </label>"
		var strHTMLText3 = "<label class = 'txtBold "
		        + overiedFareMetaData.overideOnlyCssTag + "' >%</label>";
		var strHTMLText4 = "<label class = 'txtBold "
		        + overiedFareMetaData.overideOnlyCssTag + "' >Min</label>";
		var strHTMLText5 = "<label class = 'txtBold "
		        + overiedFareMetaData.overideOnlyCssTag + "' >Max</label>";
		tblHEadingRow.appendChild(UI_commonSystem.createCell({
		    desc : strHTMLText1,
		    width : "35%",
		    textCss : 'text-align:center;'
		}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({
		    desc : strHTMLText2,
		    width : "15%",
		    textCss : 'text-align:center;'
		}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({
		    desc : strHTMLText3,
		    width : "18%",
		    textCss : 'text-align:center;'
		}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({
		    desc : strHTMLText4,
		    width : "13%",
		    textCss : 'text-align:center;'
		}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({
		    desc : strHTMLText5,
		    width : "13%",
		    textCss : 'text-align:center;'
		}));
		return tblHEadingRow;
	}

	function setPrecentageTypeDD(strVal) {
		if ($('#selDefineCanType').val() == ""
		        || $('#selDefineCanType').val == null) {
			$('#selDefineCanType').val(strVal);
		}
	}

	function showOverideChargeData(blnHideOrShow) {
		if (blnHideOrShow && blnShowChargeTypeOveride) {
			$('.' + overiedFareMetaData.overideOnlyCssTag).show();
			$('#lblRouteType').show();
			$('.confValueField').disable();
			$('#selDefineRouteType').disable();
		} else if (blnHideOrShow && !blnShowChargeTypeOveride) {
			$('.' + overiedFareMetaData.overideOnlyCssTag).hide();
			$('#lblRouteType').show();
			$('#selDefineRouteType').show();
			$('#selDefineRouteType').enable();
			$('.confValueField').disable();
		} else {
			$('.' + overiedFareMetaData.overideOnlyCssTag).hide();
		}
	}

	function unBindGridActions() {
		$(".removeRow").unbind('click');
		$(".modifySeg").unbind('click');
	}

	this.bindGridActions = function() {
		
		this.setDisplayCancelSegmentButton();

		$(".removeRow").unbind('click').bind('click', function(e) {
			modifySegmentClicked = false;
			addSegmentClicked = false;
			addGroundSegment = false;
			resetModificationOperation();
			UI_tabSearchFlights.resetFlexiSelection();
			updateGroundFltSegByFltSegMap();
			var sName = e.target.id.split("_");
			var arrIndex = sName[1];
			removeOnd(arrIndex, false);
			UI_requote.removeGroundSegment();
			$('#tdLogicalCabins_' + (arrIndex-1)).empty();
			UI_requote.removeOpenRTSegments();
			UI_requote.resetLogicalCCRebuildInfo();
			UI_tabSearchFlights.clearAll();
			$("#tdSearchFltArea").hide('fast');
			$("#trFareSection").slideDown('fast');
			skipUntuchedSegFareONDs = skipNonModifiedSegFareONDs;
		});

		$(".modifySeg")
		        .unbind('click')
		        .bind(
		                'click',
		                function(e) {
		                	resetModificationOperation();
			                updateGroundFltSegByFltSegMap();
			                $("#btnAddOND").text('Modify OND');
			                UI_tabPayment.isRequoteAddSegment = false;
							UI_tabPayment.isRequoteSegModify  = true;
			                $("#tdSearchFltArea").show();
			                var sName = e.target.id.split("_");
			                var arrIndex = sName[1];

			                selectedOndIndex = arrIndex;
			                modifySegmentClicked = true;
			                UI_requote.addGroundSegment = false;
			                UI_commonSystem.strPGMode = DATA_ResPro.reservationInfo.mode;
			    			skipUntuchedSegFareONDs = skipNonModifiedSegFareONDs;
			    			checkOpenReturn();
			                if (arrIndex > 0) {
				                var modifyingOnd = newOndList[arrIndex - 1];
				                var origin = modifyingOnd.getOrigin()+"_N";
				                var destination = modifyingOnd.getDestination()+"_N";
				                var departureDateTime = modifyingOnd
				                        .getFirstDepartureDate();
				                var bookingType = modifyingOnd.getBookingType();
				                var cabinClass = modifyingOnd.getCabinClass();
				                var oldPerPaxFare = modifyingOnd.getOldPerPaxFare();
				                var bookingClass = modifyingOnd.getBookingClass();

				                UI_requote.selectedSegment = origin + "/" + destination;
				                UI_requote.selectedSegmentCabinclass = cabinClass;
				                UI_tabSearchFlights.fromCombo.setValue(origin);
				                UI_tabSearchFlights.toCombo.setValue(destination);
				                
				                if(!$('#selSearchOptions').is(':disabled')){			
				    				UI_tabSearchFlights.setDefaultSearchOption(origin, destination);
				    			}

				    			UI_requote.applyONDStopOverInfo(modifyingOnd);				                
				                
				                $("#departureDate")
				                        .val(
				                                getDepartureDateForCalendar(departureDateTime));
				                if (cabinClass != null && '' != cabinClass) {
					                $("#classOfService").val(cabinClass);
				                }
				                if (bookingType != null && '' != bookingType) {
					                $("#bookingType").val(bookingType);
				                }
				                if (oldPerPaxFare != null && '' != oldPerPaxFare) {
				                	$("#oldPerPaxFare").val(oldPerPaxFare);
				                } else {
				                	$("#oldPerPaxFare").val('0');
				                }

				                UI_tabSearchFlights.fillBCDropDown();							 
								
				                if (sName[0] == "modByDate") {
				                	var fromDisabledDiv = $("<div></div>").addClass('diabled-div').css({
						                "position": "absolute",
						                "z-index": "10",
						                "background-color": "white",
						                "width": "100%",
						                "height": "22px",
						                "opacity": ".1"
						                });
				                	var toDisabledDiv = $("<div></div>").addClass('diabled-div').css({
						                "position": "absolute",
						                "z-index": "10",
						                "background-color": "white",
						                "width": "100%",
						                "height": "22px",
						                "opacity": ".1"
						                });
					                if (!(DATA_ResPro.initialParams.modifySegmentByRoute && modifyingOnd.modifyByRoute)) {
						                $("#fAirport").disable();
						                $("#tAirport").disable();
						                
						                //Add Disabled Div when disabling the element
						                $("#fAirport").closest("div").css({"position":"relative"});
						                $("#fAirport").closest("div").prepend(fromDisabledDiv);
						                $("#tAirport").closest("div").css({"position":"relative"});
						                $("#tAirport").closest("div").prepend(toDisabledDiv);
					                } else {
						                $("#fAirport").enable();
						                $("#tAirport").enable();
						               
						                //Remove Disabled Div when ebableing the element
						                $("#fAirport").parent().parent().find('.diabled-div').remove();
						                $("#tAirport").parent().parent().find('.diabled-div').remove();
					                }
				                } else if (sName[0] == "modByRoute") {
					                if (!(DATA_ResPro.initialParams.modifySegmentByDate && modifyingOnd.modifyByDate)) {
						                $("#departureDate").disable();
						                $("#departureDate").datepicker(
						                        'disable');
						                $("#departureVariance").disable();
						                $("#departureVariance").val("0");
						                $("#lnkON").hide();
						                $("#lnkOP").hide();
					                } else {
						                $("#departureDate").enable();
						                $("#departureDate")
						                        .datepicker('enable');
						                $("#departureVariance").enable();
						                $("#departureVariance").val("0");
						                $("#lnkON").show();
						                $("#lnkOP").show();
					                }
				                } else {
					                $("#fAirport").enable();
					                $("#tAirport").enable();
					                $("#departureDate").enable();
				                }
			                }
			                enableDisableOpenReturnOption(false);
			                UI_requote.resetLogicalCCRebuildInfo();
		                });
	}
	
	function checkOpenReturn(){
		for ( var i = 0; i < newOndList.length; i++){
			if (newOndList[i].getBookingType()== ondStatus.OPENRT) {
				UI_tabSearchFlights.isOpenReturn = true;
			}
		}
		
	}

	this.createFlightSegments = function() {
		var segments = this.getAllsegments();
		if (segments.length > 0 && this.isRequoteRequired()) {
			$('#btnRequote').show();
			if(DATA_ResPro.initialParams.allCoSChangeFareEnabled && !UI_tabPayment.isRequoteAddSegment 
					&& !this.hasCanceledFlightExist()){
				$("#btnCAllFare").show();
			} else {
				$("#btnCAllFare").hide();
			}
		} else {
			$('#btnRequote').hide();
		}

		if (DATA_ResPro.initialParams.addSegment
		        && !DATA_ResPro.initialParams.voidReservation) {
			$("#btnAddNewOND").show();
		} else {
			$("#btnAddNewOND").hide();
		}
		
		UI_commonSystem.fillGridData({
		    id : "#tblAllONDs",
		    data : segments
		});
		this.setDisplayCancelSegmentButton();
		this.setDisplayReqouteButton(segments);
		this.setDisplayViewAllBCButton();
		this.bindGridActions();
		$("#btnFQ").hide();
		$("#tblIBOBFlights").slideUp('fast');
		$("#trAddOND").slideUp('fast');
		$("#trFareSection").slideUp('fast');
		$("#divFareQuotePane").slideUp('fast');
		
		if(DATA_ResPro.blnIsFromNameChange && UI_requote.fromFlightSearch){
			fireFQActions('NCC');
		}
	};

	function getDepartureDateForCalendar(departureDateTime) {
		var dtArr = departureDateTime.split("T");
		var dateArr = dtArr[0].split("-");
		return dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
	}

	function ONDInfo() {
		var flights = [];
		var origin = null;
		var destination = null;
		var status = null;
		var cabinClass = null;
		var logicalCabinClass = null;
		var bookingType = null;
		var bookingClass = null;
		var lastFareQuoteDate = null;
		var ticketValidTill = null;
		var modifible = true;
		var cancellable = true;
		var modifyByDate = true;
		var modifyByRoute = true;
		var modifiedResSegList = null;
		var dateChangedResSegList = null;
		var oldPerPaxFare = null;
		var inverseAutoRemove = false;
		var eligibleToSameBCMod = false;
		var sameFlightModification = false;
		var oldPerPaxFareTOList = null;
		var bundledServicePeriodId = null;
		var busSegment = false;

		function decodeJsonDate(dstr) {
			var a = dstr.split('T');
			var d = a[0].split('-');
			var t = a[1].split(':');
			// new Date(year, month, day, hours, minutes, seconds,
			// milliseconds);
			return new Date(d[0], parseInt(d[1], 10) - 1, d[2], t[0], t[1],
			        t[2]);
		}
		// stupid fmt yyMMddHHmm
		function decodeAADate(dstr) {
			var yy = parseInt(dstr.substring(0, 2), 10) + 2000;
			var MM = parseInt(dstr.substring(2, 4), 10) - 1;
			var dd = parseInt(dstr.substring(4, 6), 10);
			var hh = parseInt(dstr.substring(6, 8), 10);
			var mm = parseInt(dstr.substring(8, 10), 10);
			// new Date(year, month, day, hours, minutes, seconds,
			// milliseconds);
			return new Date(yy, MM, dd, hh, mm, 0, 0);
		}

		function converAAToJson(dstr) {
			var yy = parseInt(dstr.substring(0, 2), 10) + 2000;
			var MM = dstr.substring(2, 4);
			var dd = dstr.substring(4, 6);
			var hh = dstr.substring(6, 8);
			var mm = dstr.substring(8, 10);
			return yy + '-' + MM + '-' + dd + 'T' + hh + ':' + mm + ':00';
		}

		function sortFltSegs(fltSeg1, fltSeg2) {
			return fltSeg1.departureDTZuluMillis > fltSeg2.departureDTZuluMillis;
		}

		function cleanRPH(rph) {
			var arr = rph.split('#');
			if (arr.length > 0) {
				return arr[0];
			}
			return rph;
		}

		function convertNewFltSegments(flightList) {
			var out = [];
			var oldPaxFare = $("#oldPerPaxFare").val();
			if (oldPaxFare != null && '' != oldPaxFare) {
				oldPaxFare = $("#oldPerPaxFare").val();
            } else {
            	$("#oldPerPaxFare").val('0');
            	oldPaxFare = 0;
            }
			var busSegment = false;
			if(UI_requote.addGroundSegment){
				busSegment = true;
			}
			
			if (flightList != null) {
				for ( var i = 0; i < flightList.length; i++) {
					out[out.length] = {
					    segmentCode : flightList[i].segmentCode,
					    flightNo : flightList[i].flightNo,
					    domesticFlight : flightList[i].domesticFlight,
					    carrierCode : flightList[i].carrierCode,
					    departureDate : converAAToJson(flightList[i].departureTime),
					    arrivalDate : converAAToJson(flightList[i].arrivalTime),
					    departureTime : converAAToJson(flightList[i].departureTime),
					    arrivalTime : converAAToJson(flightList[i].arrivalTime),
					    departureDateTimeZulu : converAAToJson(flightList[i].departureTimeZulu),
					    arrivalDateTimeZulu : converAAToJson(flightList[i].arrivalTimeZulu),
					    departureDTZuluMillis : decodeAADate(
					            flightList[i].departureTimeZulu).getTime(),
					    arrivalDTZuluMillis : decodeAADate(
					            flightList[i].arrivalTimeZulu).getTime(),
					    segmentRPH : flightList[i].flightRPH,
					    resSegmentRPH : null,
					    status : ondStatus.NEW,
					    displayStatus : ondStatus.NEW,
					    bookingType : flightList[i].bookingType,
					    modifible : (busSegment?false:true),
					    cancellable : true,
					    modifyByDate : (busSegment?false:true),
					    modifyByRoute : (busSegment?false:true),
					    flownSegment : false,
					    oldPerPaxFare : oldPaxFare,
						oldPerPaxFareTO : flightList[i].fareTO,
						bundledServicePeriodId : null,
						addGroundSegment : false,
						groundStationPnrSegmentID : null,
						busSegment : busSegment
					};

				}
			}
			return out;
		}

		function convertExistingFltSegments(flightList) {
			var out = [];
			if (flightList != null) {
				for ( var i = 0; i < flightList.length; i++) {
					var flightSegmentRefNumber
					if(flightList[i].routeRefNumber != null) {
						flightSegmentRefNumber = flightList[i].flightSegmentRefNumber + "#" + flightList[i].routeRefNumber;
					} else {
						flightSegmentRefNumber = flightList[i].flightSegmentRefNumber;
					}

					var addGroundSegment = false;
					if(flightList[i].groundStationPnrSegmentID==null 
							&& flightList[i].subStationShortName==null && !flightList[i].flownSegment){
						addGroundSegment = true;
					}
					var busSegment=false;
					if(flightList[i].subStationShortName!=null && trim(flightList[i].subStationShortName)!=""){
						busSegment=true;
					}
					out[out.length] = {
					    segmentCode : flightList[i].segmentCode,
					    flightNo : flightList[i].flightNo,
					    domesticFlight : flightList[i].domesticFlight,
					    carrierCode : flightList[i].carrierCode,
					    departureDate : flightList[i].departureDate,
					    arrivalDate : flightList[i].arrivalDate,
					    departureTime : flightList[i].departureDate,
					    arrivalTime : flightList[i].arrivalDate,
					    departureDateTimeZulu : flightList[i].departureDateZulu,
					    arrivalDateTimeZulu : flightList[i].arrivalDateZulu,
					    departureDTZuluMillis : decodeJsonDate(
					            flightList[i].departureDateZulu).getTime(),
					    arrivalDTZuluMillis : decodeJsonDate(
					            flightList[i].arrivalDateZulu).getTime(),
					    segmentRPH : flightSegmentRefNumber,
					    resSegmentRPH : flightList[i].bookingFlightSegmentRefNumber,
					    routeRPH:flightList[i].routeRefNumber,
					    status : flightList[i].status,
					    displayStatus : flightList[i].displayStatus,
					    bookingType : flightList[i].bookingType,
					    modifible : (busSegment?false:flightList[i].modifible),
					    cancellable : flightList[i].cancellable,
					    modifyByDate : (busSegment?false:flightList[i].modifyByDate),
					    modifyByRoute : (busSegment?false:flightList[i].modifyByRoute),
					    flownSegment : flightList[i].flownSegment,
					    oldPerPaxFare : flightList[i].fareTO.amount,
					    oldPerPaxFareTO : flightList[i].fareTO,
					    bundledServicePeriodId : flightList[i].bundledServicePeriodId,
					    addGroundSegment : addGroundSegment,	
					    groundStationPnrSegmentID : flightList[i].groundStationPnrSegmentID, 
					    busSegment : busSegment
					};
				}
			}
			return out;
		}

		function deriveOriginDestination() {
			origin = flights[0].segmentCode.split('/')[0];
			var arr = flights[flights.length - 1].segmentCode.split('/');
			destination = arr[arr.length - 1];
		}

		function deriveModificationParams() {
			for ( var i = 0; i < flights.length; i++) {
				modifible = modifible && flights[i].modifible;
				cancellable = cancellable && flights[i].cancellable;
				modifyByDate = modifyByDate && flights[i].modifyByDate;
				modifyByRoute = modifyByRoute && flights[i].modifyByRoute;
				busSegment = flights[i].busSegment;
			}
		}

		this.addNewFlightSegments = function(fltSegments, jsonFQParams) {
			flights = convertNewFltSegments(fltSegments);
			flights = flights.sort(sortFltSegs);
			deriveOriginDestination();
			deriveModificationParams()
			bookingType = jsonFQParams.bookingType;
			bookingClass = jsonFQParams.bookingClass;
			cabinClass = jsonFQParams.cabinClass;
			logicalCabinClass = jsonFQParams.logicalCabinClass;
			status = ondStatus.NEW;
		};

		this.addFlightSegments = function(fltSegments, jsonFQParams) {
			flights = fltSegments;
			flights = flights.sort(sortFltSegs);
			deriveOriginDestination();
			deriveModificationParams()
			bookingType = jsonFQParams.bookingType;
			bookingClass = jsonFQParams.bookingClass;
			cabinClass = jsonFQParams.cabinClass;
			logicalCabinClass = jsonFQParams.logicalCabinClass;
			status = ondStatus.NEW;
		};

		this.getFirstDepartureDTZuluMillis = function() {
			return flights[0].departureDTZuluMillis;
		};

		this.getLastArrivalDTZuluMillis = function() {
			return flights[flights.length - 1].arrivalDTZuluMillis;
		};

		this.getFirstDepartureDate = function() {
			return flights[0].departureDate;
		};

		this.getStatus = function() {
			return status;
		}

		this.getOrigin = function() {
			return origin;
		};

		this.getDestination = function() {
			return destination;
		};

		this.getModifible = function() {
			return modifible;
		}
		
		this.getCancellable = function() {
			return cancellable;
		}

		this.getModifyByDate = function() {
			return modifyByDate;
		}

		this.getModifyByRoute = function() {
			return modifyByRoute;
		}

		this.getBookingType = function() {
			return bookingType;
		}
		
		this.setInverseAutoRemove = function(val){
			inverseAutoRemove = val;
		}

		this.getInverseAutoRemove = function(){
			return inverseAutoRemove;
		}
		
		this.getBookingClass = function() {
			return bookingClass;
		}
		
		this.getBundledServicePeriodId = function() {
			return bundledServicePeriodId;
		}

		this.getCabinClass = function() {
			return cabinClass;
		}
		
		this.getLogicalCabinClass = function() {
			return logicalCabinClass;
		}

		this.getLastFareQuoteDate = function() {
			return lastFareQuoteDate;
		}

		this.getTicketValidTill = function() {
			return ticketValidTill;
		}
		
		this.getBusSegment = function() {
			return busSegment;
		}

		this.addExistingFlightSegments = function(fltSegments) {
			flights = convertExistingFltSegments(fltSegments);
			flights = flights.sort(sortFltSegs);
			deriveOriginDestination();
			deriveModificationParams();
			cabinClass = fltSegments[0].cabinClassCode;
			logicalCabinClass = fltSegments[0].logicalCabinClass;
			bookingType = fltSegments[0].bookingType;
			bundledServicePeriodId = fltSegments[0].bundledServicePeriodId;
			if(DATA_ResPro.initialParams.requoteSetBCForExistingSegments && fltSegments[0].fareTO != null){
				bookingClass = fltSegments[0].fareTO.bookingClassCode;
			}			
			
			lastFareQuoteDate = fltSegments[0].lastFareQuoteDate;
			ticketValidTill = fltSegments[0].ticketValidTill;
			var tmpStatus = fltSegments[0].status;
			// If one segment is wait listed in connection, set the entire segment as WL/UN to prevent modification
			for(var ind=0;ind<fltSegments.length;ind++){
				if(fltSegments[ind].status == ondStatus.WL){
					status = ondStatus.WL;
					break;
				} else if(fltSegments[ind].displayStatus!=null && fltSegments[ind].displayStatus== ondStatus.UN){				
					status = ondStatus.UN;
					break;
				}else{
					status = tmpStatus;
				}
			}			
		};

		this.getFlights = function() {
			return flights;
		}

		this.getFlightRPHList = function() {
			var list = [];
			for ( var i = 0; i < flights.length; i++) {
				list[list.length] = flights[i].segmentRPH;
			}
			return list;
		};

		this.isFlownOnd = function() {
			for ( var i = 0; i < flights.length; i++) {
				if (flights[i].flownSegment) {
					return true;
				}
			}
			return false;
		};
		
		this.hasCanceledFlight = function() {
			for ( var i = 0; i < flights.length; i++) {
				if (flights[i].displayStatus=='UN') {
					return true;
				}
			}
			return false;
		};

		this.getExistingFlightRPHList = function() {
			var list = [];
			if (status == ondStatus.CNF) {
				for ( var i = 0; i < flights.length; i++) {
					list[list.length] = flights[i].segmentRPH;
				}
			}
			return list;
		};

		this.getResSegRPHList = function() {
			var list = [];
			for ( var i = 0; i < flights.length; i++) {
				 var resSegRPH= buildResSegRPH(flights[i]);
				 if(resSegRPH != null){
				     list[list.length] = resSegRPH;
				 }
			}
			return list;
		};
		
		function buildResSegRPH(flightObj){
		    var rph = flightObj.segmentRPH;
		    var arr = rph.split('$');
		    arr[2] = flightObj.resSegmentRPH;
		    if(arr[2] == null || $.trim(arr[2]) == ''){
		        return null;
		    } else {
		        return arr.join('$');
		    }
		}

		this.setModifiedResSegList = function(resSegList){
			this.modifiedResSegList = resSegList;
		}
		
		this.getModifiedResSegList = function(){
			return this.modifiedResSegList;
		}
		
		this.setDateChangedResSegList = function(resSegList){
			this.dateChangedResSegList = resSegList;
		}
		
		this.getDateChangedResSegList = function(){
			return this.dateChangedResSegList;
		}

		this.setSameFlightModification = function(sameFltModification){
			this.sameFlightModification = sameFltModification;
		}
		
		this.getSameFlightModification = function(){
			return this.sameFlightModification;
		}

		this.setEligibleToSameBCMod = function(eligibility) {
			this.eligibleToSameBCMod = eligibility;
		}

		this.getEligibleToSameBCMod = function() {
			return this.eligibleToSameBCMod;
		}

		this.getAllOpenSegmentsForOHDRelCal = function() {
			var list = [];
			for ( var i = 0; i < flights.length; i++) {
				list[list.length] = {
				    flightNumber : flights[i].flightNo,
				    departureDateTime : flights[i].departureTime,
				    departureDateTimeZulu : flights[i].departureDateTimeZulu,
				    operatingAirline : flights[i].carrierCode,
				    segmentCode : flights[i].segmentCode,
				    domesticFlight : flights[i].domesticFlight
				}
			}
			return list;
		}
		
		this.getOldPerPaxFare = function(index) {
			return flights[0]['oldPerPaxFare'];
		}

		this.setOldPerPaxFareTOList = function(oldPerPaxFareTOList) {
			this.oldPerPaxFareTOList = oldPerPaxFareTOList;
		}

		this.getOldPerPaxFareTOList = function() {
			return this.oldPerPaxFareTOList;
		}

		this.createData = function(index) {

			function mergeFltSegData(field) {
				var list = [];
				for ( var i = 0; i < flights.length; i++) {
					if (flights[i].bookingType != ondStatus.OPENRT) {
						list[list.length] = flights[i][field];
					}
				}
				return list;
			}

			return {
			    index : index,
			    origin : origin,
			    destination : destination,
			    ond : origin + '/' + destination,
			    flightNo : mergeFltSegData('flightNo'),
			    domesticFlight :  mergeFltSegData('domesticFlight'),
			    carrierCode : mergeFltSegData('carrierCode'),
			    departureDate : mergeFltSegData('departureDate'),
			    arrivalDate : mergeFltSegData('arrivalDate'),
			    departureTime : mergeFltSegData('departureTime'),
			    arrivalTime : mergeFltSegData('arrivalTime'),
			    departureDateTimeZulu : mergeFltSegData('departureDateTimeZulu'),
			    arrivalDateTimeZulu : mergeFltSegData('arrivalDateTimeZulu'),
			    segmentRPH : mergeFltSegData('segmentRPH'),
			    status : status,
			    bookingType : flights[0].bookingType,
			    modifible : modifible,
			    cancellable : cancellable,
			    modifyByDate : modifyByDate,
			    modifyByRoute : modifyByRoute,
			    flownSegment : mergeFltSegData('flownSegment'),
			    bookingType : bookingType,
			    bookingClass : bookingClass,
			    cabinClass : cabinClass,
			    logicalCabinClass: logicalCabinClass,
			    oldPerPaxFare : mergeFltSegData('oldPerPaxFare'),
			    bundledServicePeriodId : bundledServicePeriodId,
				addGroundSegment : mergeFltSegData('addGroundSegment'),		
			    groundStationPnrSegmentID : mergeFltSegData('groundStationPnrSegmentID'), 
			    busSegment : busSegment
			};
		};
	}

	this.removeModifyingSegmentData = function() {
		var removedOnd = null;
		if (modifySegmentClicked) {
			if (selectedOndIndex > 0) {
				removedOnd = removeOnd(selectedOndIndex, true);
				selectedOndIndex = -1;
				modifySegmentClicked = false;
			}
		}

		if (addSegmentClicked) {
			addSegmentClicked = false;
		}

		$("#tdSearchFltArea").slideUp('fast');
		return removedOnd;
	}

	function createDateTimeStr(dateTime) {
		var dateTimeArr = dateTime.split(' ');
		var formattedDate = dateTimeArr[0];
		var formattedTime = dateTimeArr[1];

		var dateArr = formattedDate.split('-');
		var timeArr = formattedTime.split(':');
		var dateStr = dateArr[2].substring(2, 4) + dateArr[1] + dateArr[0];
		var timeStr = timeArr[0] + timeArr[1];

		return dateStr + timeStr;
	}
	
	/**
	 * Will hide the Cance Segment buttons if the number of segments are one (< 2).
	 */
	this.setDisplayCancelSegmentButton=function(){
		var tableRows=$("#tblAllONDs tr").length;
		/*
		 * No need to go for a loop, if it's just one segment only we need to hide it.
		 */
		if(tableRows <= 1){
			$("#removeRow_1").hide();
		}
		
		/*
		 * when there is a flight segment with bus segment only canceling flight segment make no sense,
		 *  rather than cancel reservation
		 * */
		if(tableRows == 2 && UI_requote.isBusSegmentExist()){
			var segments = UI_requote.getAllsegments();
			var busSegmentExist = false;
			var rowId = 1;
			$.each(segments, function(key, flt) {
				if(!flt.busSegment && (flt.status == "CNF" || flt.status == "NEW") ){
					$("#removeRow_"+rowId).hide();		
				}			
				rowId++;
			});
		}
	}
	
	this.isBusSegmentExist = function(){
		var segments = UI_requote.getAllsegments();
		var busExist = false;
		if(segments!=undefined && segments!=null){
			$.each(segments, function(key, flt) {
				if(flt.busSegment && (flt.status == "CNF" || flt.status == "NEW") ){
					busExist = true;
					return;					
				}			
			});
		}
		return busExist;
	}
	
	/**
	 * Will hide requote and bc fares buttons if the number of segments are one (< 2).
	 */
	this.setDisplayReqouteButton=function(segments){
		for(var ind=0; ind<segments.length; ind++){
			if(segments[ind].status == ondStatus.WL || 
					(segments[ind].status == ondStatus.UN && modifyUNSegments == "false")){
				$("#btnCAllFare").hide();
				$("#btnRequote").hide();
				break;
			}
		}
	}
	
	this.addOndFlights = function(flightArr, jsonFQParams) {
		var ondInfo = new ONDInfo();
		ondInfo.addNewFlightSegments(flightArr, jsonFQParams);
		addNewOnd(ondInfo);
		if (UI_requote.overlappingOND) {
			UI_requote.overlappingOND = false;
			return false;
		}
		UI_requote.selectedSegmentCabinclass = jsonFQParams.cabinClass;
		if (UI_tabSearchFlights.isOpenReturn
		        || $("#openReturn").attr('checked')) {
			var openRTInfo = UI_tabSearchFlights.openReturnInfo;
			var flightList = [];
			var flightInfo = {};

			var dateTimeStr = '';
			var segmentCode = '';
			// if open return information is null, set dummy departure date
			// based on selected validity period value
			if (openRTInfo == null) {
				var deptDateArr = ($("#departureDate").val()).split('/');
				var deptDateObj = new Date(deptDateArr[2], deptDateArr[1],
				        deptDateArr[0], 23, 59);
				deptDateObj.setMonth((deptDateObj.getMonth() + 1)
				        + Number($('#validity').val()));
				var formattedDate = deptDateObj.getDate() + '-'
				        + (deptDateObj.getMonth() + 1) + '-'
				        + deptDateObj.getFullYear() + ' '
				        + deptDateObj.getHours() + ':'
				        + deptDateObj.getMinutes();

				dateTimeStr = createDateTimeStr(formattedDate);
				segmentCode = UI_tabSearchFlights.toCombo.getValue() + '/'
				        + UI_tabSearchFlights.fromCombo.getValue();
				UI_tabSearchFlights.hideChangeFareButton = true;
			} else {
				dateTimeStr = createDateTimeStr(openRTInfo.travelExpiryDate);
				segmentCode = openRTInfo.segmentCode;
				UI_tabSearchFlights.hideChangeFareButton = false;
			}

			flightInfo['segmentCode'] = segmentCode;
			flightInfo['flightNo'] = '';
			flightInfo['domesticFlight'] = '';
			flightInfo['carrierCode'] = '';
			flightInfo['departureTime'] = dateTimeStr;
			flightInfo['arrivalTime'] = dateTimeStr;
			flightInfo['departureTimeZulu'] = dateTimeStr;
			flightInfo['arrivalTimeZulu'] = dateTimeStr;
			flightInfo['flightRPH'] = '';
			flightInfo['bookingType'] = ondStatus.OPENRT;
			flightList[0] = flightInfo;

			var openRTOnd = new ONDInfo();
			var clonedJsonFQParams = $.airutil.dom.cloneObject(jsonFQParams);
			clonedJsonFQParams['bookingType'] = ondStatus.OPENRT;

			openRTOnd.addNewFlightSegments(flightList, clonedJsonFQParams);
			addNewOnd(openRTOnd);
		}

		
		if(UI_requote.addGroundSegment && ondInfo.getFlightRPHList()!=null && ondInfo.getFlightRPHList()[0]!=null){
			
			var key = UI_requote.getFlightSegIdFromRPH(UI_requote.selectedFltSeg);
			var value = UI_requote.getFlightSegIdFromRPH(ondInfo.getFlightRPHList()[0]);
			
			if(ondInfo.getFlightRPHList()!=null && ondInfo.getFlightRPHList()[0]!=null){
				groundFltSegByFltSegId[key]=value;				
			}			
		}
		
		UI_requote.createFlightSegments();
		//Add default logical cc rebuild value
		UI_requote.removeGroundSegment();
		var ondList = getONDList();
		if(ondList != null){
			for(var i=0;i<ondList.length;i++){
				UI_tabSearchFlights.ondRebuildLogicalCCAvailability[i] = true;
				if(ondList[i].getOldPerPaxFareTOList() != null){
					if(ondList[i].getOldPerPaxFareTOList()[0]!=undefined 
							&& ondList[i].getOldPerPaxFareTOList()[0].modifyToSameFare && !DATA_ResPro.initialParams.allowOverrideSameFare){
						$("#btnCAllFare").hide();
					} 
				}
			}
		}
		
		fireFQActions('ADD');

	};
	
	this.getFlightSegIdFromRPH = function (flightRPH){	
		if(flightRPH!=undefined && flightRPH!=null 
				&& flightRPH.split("$")[2]!=null){
			return flightRPH.split("$")[2];
		}
		return null;
	};	

	function fireFQActions(opt) {
		if (!DATA_ResPro.initialParams.requoteAutoFQEnabled) {
			return;
		}
		if (opt == 'ADD'
		        && !DATA_ResPro.initialParams.requoteAutoAddOndFQEnabled) {
			return;
		} else if (opt == 'CNX'
		        && !DATA_ResPro.initialParams.requoteAutoCnxOndFQEnabled) {
			return;
		} 

		var segments = UI_requote.getAllsegments();
		if (segments.length > 0) {
			UI_requote.requoteFareQuote();
		}
	}

	function addNewOnd(ondInfo) {
		function validateOnd(ond) {
			for ( var i = 0; i < newOndList.length; i++) {
				if (modifySegmentClicked && (selectedOndIndex - 1) == i) {
					continue;
				}

				if (ond.getBookingType() == ondStatus.OPENRT) {
					continue;
				}

				var error = false;
				if ((ond.getLastArrivalDTZuluMillis() <= newOndList[i]
				        .getLastArrivalDTZuluMillis())
				        && (ond.getLastArrivalDTZuluMillis() >= newOndList[i]
				                .getFirstDepartureDTZuluMillis())) {
					error = true;
				}
				if ((ond.getLastArrivalDTZuluMillis() > newOndList[i]
				        .getLastArrivalDTZuluMillis())
				        && (ond.getFirstDepartureDTZuluMillis() <= newOndList[i]
				                .getLastArrivalDTZuluMillis())) {
					error = true;
				}

				if (error) {
					showCommonError("ERROR",
					        "This OND overlapping with existing OND!");
					UI_requote.overlappingOND = true;
					return false;
				}
			}
			return true;
		}

		var valid = validateOnd(ondInfo);
		if (valid) {		
			var removedOnd = UI_requote.removeModifyingSegmentData();	
			isSameFlightmodification = false;
			ondInfo = getMatchingOndIfInRemove(ondInfo);
			ondInfo.setSameFlightModification(isSameFlightmodification);
			UI_requote.removeOpenRTSegments();
			if( removedOnd != null){
				if(removedOnd.getModifiedResSegList() != null){	
					ondInfo.setModifiedResSegList(removedOnd.getModifiedResSegList());
				} else {
					ondInfo.setModifiedResSegList(removedOnd.getResSegRPHList());
					
				}
			}
			
			if(removedOndList.length > 0){
				var oldRmOnd = getMatchingOndFromRemoveList(ondInfo);
				if(oldRmOnd != null && oldRmOnd.getModifiedResSegList() != null){
						ondInfo.setDateChangedResSegList(oldRmOnd.getModifiedResSegList());
						ondInfo.setOldPerPaxFareTOList(oldRmOnd.getOldPerPaxFareTOList());
					} else if (oldRmOnd != null){
						ondInfo.setDateChangedResSegList(oldRmOnd.getResSegRPHList());	
						var oldRmOndFlights = oldRmOnd.getFlights();						
						ondInfo.setOldPerPaxFareTOList(populateOldPerPaxFareTOList(oldRmOndFlights));
					}
			}		
				
			
			newOndList[newOndList.length] = ondInfo;
			newOndList = newOndList.sort(sortOnds);
			if(DATA_ResPro.initialParams.enforceSameOrHigherFare){			
				var currentJourney = getTotalJourney(false);
				if (originalJourney != currentJourney
						&& isEligibleToSameBCModification(originalJourney,
								currentJourney)) {
					for ( var i = 0; i < newOndList.length; i++) {
						newOndList[i].setEligibleToSameBCMod(true);
					}
				} else if (originalJourney != currentJourney 
						&& UI_tabSearchFlights.openReturnInfo == null) {
					for ( var i = 0; i < newOndList.length; i++) {
						newOndList[i].dateChangedResSegList = null;
						newOndList[i].setEligibleToSameBCMod(false);
//						newOndList[i].oldPerPaxFareTOList = null;
					}
				} else if (originalJourney == currentJourney) {
					for ( var i = 0; i < newOndList.length; i++) {
						newOndList[i].setEligibleToSameBCMod(false);
						if(newOndList[i].getOldPerPaxFareTOList() == null){
							var oldRmOnd = getMatchingOndFromRemoveList(newOndList[i]);
							if(oldRmOnd != null && oldRmOnd.getModifiedResSegList() != null){
								newOndList[i].setDateChangedResSegList(oldRmOnd.getModifiedResSegList());
								newOndList[i].setOldPerPaxFareTOList(oldRmOnd.getOldPerPaxFareTOList());
							} else if (oldRmOnd != null){
								newOndList[i].setDateChangedResSegList(oldRmOnd.getResSegRPHList());	
								var oldRmOndFlights = oldRmOnd.getFlights();						
								newOndList[i].setOldPerPaxFareTOList(populateOldPerPaxFareTOList(oldRmOndFlights));
							}
						}
					}
				}
			}
		}
		return valid;
	}
			
	function getReverseJourney(originalJourney){
		var originalSegments = originalJourney.split(',');
		var reverseSeg = "";
		var reverseJourney = "";
		for ( var i = 0; i < originalSegments.length; i++) {
			var elements = originalSegments[i].split("/");
			var count = elements.length - 1;
			reverseSeg = "";
			while (count >= 0) {
				if (reverseSeg == "") {
					reverseSeg = elements[count];
				} else {
					reverseSeg = reverseSeg + "/" + elements[count];
				}
				count--;
			}
			reverseJourney = reverseSeg + reverseJourney;
		}
		return reverseJourney;
	}

	function isEligibleToSameBCModification(originalJourney, currentJourney) {
		if (originalJourney.indexOf(currentJourney) >= 0
				|| currentJourney.indexOf(originalJourney) >= 0) {
			if (originalJourney.length >= currentJourney.length
					&& originalJourney.split(currentJourney).length >= 0) {
				var removingJourneyOnd = originalJourney.replace(currentJourney,
				'');
				if(removingJourneyOnd.replace(/,/g,'') == getReverseJourney(currentJourney)){
					return true;
				}else{
					return false;
				}				
			} else if (currentJourney.length >= originalJourney.length
					&& currentJourney.split(originalJourney).length >= 0) {
				var addingJourneyOnd = currentJourney.replace(originalJourney,
						'');
				if (addingJourneyOnd.replace(/,/g, '') == getReverseJourney(originalJourney)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function getMatchingOndIfInRemove(ondInfo) {
		var newFlts = ondInfo.getFlightRPHList();

		function ondMatch(o1, o2) {
			if (o1.getCabinClass() == o2.getCabinClass()
			        && o1.getBookingType() == o2.getBookingType()
			        && o1.getBookingClass() == o2.getBookingClass()
			        && !UI_tabSearchFlights.isOpenReturn 
			        && !$("#openReturn").attr('checked')) {
				return true;
			}
			return false;
		}
		for ( var i = 0; i < removedOndList.length; i++) {
			var rmOnd = removedOndList[i];
			var rmFlts = rmOnd.getFlightRPHList();
			if ($(newFlts).not(rmFlts).length == 0
			        && $(rmFlts).not(newFlts).length == 0
			        && ondMatch(ondInfo, rmOnd)) {
				//removedOndList.splice(i, 1);
				isSameFlightmodification = true;
				return rmOnd;
			}
		}
		return ondInfo;
	}
	
	function getMatchingOndFromRemoveList(ondInfo) {
		var newFlts = ondInfo.getFlightRPHList();

		function matchOnd(newOnd, rmvOnd) {
			if (newOnd.getCabinClass() == rmvOnd.getCabinClass()
			        && newOnd.getBookingType() == rmvOnd.getBookingType()
			        && newOnd.getOrigin() == rmvOnd.getOrigin() 
			        && newOnd.getDestination() == rmvOnd.getDestination()) {
				return true;
			}
			return false;
		}
		for ( var i = 0; i < removedOndList.length; i++) {
			var rmOnd = removedOndList[i];
			var rmFlts = rmOnd.getFlightRPHList();
			if ($(newFlts).length == rmFlts.length 
			        && matchOnd(ondInfo, rmOnd)) {				
				return rmOnd;
			}
		}
		return ondInfo;
	}
	
	function hasRmdOndInOriginalOndList(ondInfo) {
		var newFlts = ondInfo.getFlightRPHList();

		function matchOnd(newOnd, tmpOnd) {
			if (newOnd.getCabinClass() == tmpOnd.getCabinClass()
			        && newOnd.getBookingType() == tmpOnd.getBookingType()
			        && newOnd.getOrigin() == tmpOnd.getOrigin() 
			        && newOnd.getDestination() == tmpOnd.getDestination()) {
				return true;
			}
			return false;
		}
		for ( var i = 0; i < newOndList.length; i++) {
			var tmpOnd = newOndList[i];
			var tmpFlts = tmpOnd.getFlightRPHList();
			if ($(newFlts).length == tmpFlts.length 
			        && matchOnd(ondInfo, tmpOnd)) {				
				return true;
			}
		}
		return false;
	}
	
	function setRemovedOndFareIfMatchingOnd(removedOnd){
		var rmFlts = removedOnd.getFlightRPHList();

		function matchOnd(newOnd, tmpOnd) {
			if (newOnd.getCabinClass() == tmpOnd.getCabinClass()
			        && newOnd.getBookingType() == tmpOnd.getBookingType()
			        && newOnd.getOrigin() == tmpOnd.getOrigin() 
			        && newOnd.getDestination() == tmpOnd.getDestination()) {
				return true;
			}
			return false;
		}
		for ( var i = 0; i < newOndList.length; i++) {
			var tmpOnd = newOndList[i];
			var tmpFlts = tmpOnd.getFlightRPHList();
			if ($(rmFlts).length == tmpFlts.length 
			        && matchOnd(removedOnd, tmpOnd) && tmpOnd.getStatus() == ondStatus.NEW) {				
				newOndList[i].setDateChangedResSegList(removedOnd.getResSegRPHList());	
				var oldRmOndFlights = removedOnd.getFlights();						
				newOndList[i].setOldPerPaxFareTOList(populateOldPerPaxFareTOList(oldRmOndFlights))
			}
		}
	}

	function removeOnd(arrIndex, slient) {
		if (arrIndex > 0) {
			var removedOnd = newOndList[arrIndex - 1];
			if (removedOnd.getStatus() != ondStatus.NEW) {
				removedOndList[removedOndList.length] = removedOnd;
				setRemovedOndFareIfMatchingOnd(removedOnd);
			}
			if(removedOndList.length > 0 && skipNonModifiedSegFareONDs == "true"){
				var modifiedCount = 0;
				for ( var i = 0; i < newOndList.length; i++) {
					if (i != (arrIndex - 1) && newOndList[i].getStatus() != ondStatus.CNF) {
						modifiedCount++;
					}
				}
				if (modifiedCount == 0 && !modifySegmentClicked) {
					UI_commonSystem.strPGMode = "cancelSegment";
				}
			}
			newOndList.splice((arrIndex - 1), 1);
			UI_requote.createFlightSegments();
			if (!slient) {				
				fireFQActions('CNX');
			}
			if(removedOndList.length > 0){
				if (skipNonModifiedSegFareONDs == "true") {
					$("#btnBook").show();
				}
				var currentJourney = getTotalJourney(false);
				if (originalJourney != currentJourney
						&& isEligibleToSameBCModification(originalJourney,
								currentJourney)) {
					for ( var i = 0; i < newOndList.length; i++) {
						newOndList[i].setEligibleToSameBCMod(true);
					}
				}
			}
			return removedOnd;
		}
		
		return null;
	}

	this.removeOpenRTSegments = function() {
		if (newOndList != null) {
			var count = 0;

			while (count < newOndList.length) {
				var ondObj = newOndList[count];
				if (ondObj.getBookingType() == ondStatus.OPENRT) {
					ondObj.setInverseAutoRemove(true);
					removeOnd(count + 1, false);
				} else {
					count++;
				}
			}
		}
	}
	
	this.removeGroundSegment = function() {

		if(groundFltSegByFltSegId!=undefined && groundFltSegByFltSegId!=null){
			$.each(groundFltSegByFltSegId, function(key, value){
				if(!UI_requote.isSegmentExist(key)){
					
					if (newOndList != null) {						
						for (var count=0;count < newOndList.length;count++) {
							var ondObj = newOndList[count];
							var found = false;
							if(ondObj.getFlightRPHList().length == 1 && 
									UI_requote.getFlightSegIdFromRPH(ondObj.getFlightRPHList()[0])==value){
									found=true;
							}							
							
							if(found){
								ondObj.setInverseAutoRemove(true);
								removeOnd(count+1, true);
							}					

							
						}
					}
				}
			});
		}
	}
	
	this.isSegmentExist = function(fltSegId){
		var found =false;
		$.each(newOndList, function(key, obj){			
			$.each(obj.getFlightRPHList(), function(i, rph){
				if(UI_requote.getFlightSegIdFromRPH(rph)==fltSegId){
					// This is checking for single ond modifying connecion ond scenario
					if(obj.getModifiedResSegList() != null && obj.getModifiedResSegList().length == obj.getFlightRPHList().length){
						found = true; 
					} else if (obj.getModifiedResSegList() == null){
						found=true;		
					}							
				}
			});
			
		});
		return found;
	}
	
	/**
	 * when flight segment is removed bus segment also should be removed.
	 * since this gorund segment logic will be handled from back end we dont need to pass the gorund segment ID.
	 * this function will make sure there is no duplicates like that. 
	 * */
	this.removeGroundSegmentID = function() {
		if(groundFltSegByFltSegId!=undefined && groundFltSegByFltSegId!=null 
				&& removedOndList!=undefined && removedOndList!=null) {			
			$.each(groundFltSegByFltSegId, function(key, value){
				if(!UI_requote.isSegmentExist(key)){						
					for ( var i = 0; i < removedOndList.length; i++) {			
						if(!removedOndList[i].getInverseAutoRemove() && removedOndList[i].getBusSegment()){
							$.each(removedOndList[i].getFlightRPHList(), function(h, rph){
								if(UI_requote.getFlightSegIdFromRPH(rph)==value){
									removedOndList[i].setInverseAutoRemove(true);		
								}								
							});					
							
						}			
					}				

				}
			});
		}
	}
	

	this.reset = function() {
		$("#btnRQConfirm").unbind("click").bind("click", function() {
			callRequoteConfirm();
		});
		
		$("#btnRQCancel").unbind("click").bind("click", function() {
			UI_tabAnci.resetAnciData();			
			UI_requote.resetOverrideCharges();
			if(DATA_ResPro.blnIsFromNameChange){
				UI_modifyResRequote.loadHomePage();
			} else {
				UI_modifyResRequote.openTab(0);
			}
		});
		
		$("#btnRQReset").unbind("click").bind("click", function() {
			resetOnclick();
		});		
		
		clearDataStructs();
		UI_requote.createFlightSegments();
		UI_requote.resetLogicalCCRebuildInfo();
		UI_requote.showHideUserNoteClassification();
		$("#tdSearchFltArea").slideUp('fast');
		UI_requote.isRequoteRequired();
		$("#btnBook").hide();		
		showAddGroundSegment(false);
	};

	function returnCallShowFareQuoteInfo(response) {
		response = $.extend(response, {"ondListString":JSON.stringify(UI_requote.getONDSearchDTOList(), null)} );
		UI_tabSearchFlights.returnCallShowFareQuoteInfo(response);
	}
	
	// hide user note type classification when privileges not exists 
	this.showHideUserNoteClassification = function () {
		if(DATA_ResPro.initialParams.allowAddUserNotes && DATA_ResPro.initialParams.allowClassifyUserNotes){
			$("#selClassification").show();
		} else {
			$("#selClassification").hide();
		}
	}

	this.getONDSearchDTOList = function() {
		var ondList = getONDList();
		var list = [];
		for ( var i = 0; i < ondList.length; i++) {
			var ond = ondList[i];
			var bc = null;
			if (ond.getBookingType() == ondStatus.OPENRT) {
				continue;
			}
			
			//AARESAA-10867,following code commented due to, some requote flow wrong logicalcabinclass being passed,
			//all ond related info should be maintained upto date in and retreived from respective OndInfo
			
			//FIXME :following patch was applied when Zest/FG requote enable with flexi passing logicalCabinClass info is wrong
			//this has to be re investigate on above scenario and corrected with specified features enabled
			
			var ondLogicalCC = null;
			if(UI_tabSearchFlights.logicalCabinClassSelection != null
					&& UI_tabSearchFlights.logicalCabinClassSelection != {}){
				ondLogicalCC = UI_tabSearchFlights.logicalCabinClassSelection[i];
			}
			
			var searchOND = {
			    fromAirport : ond.getOrigin(),
			    toAirport : ond.getDestination(),
			    flightRPHList : ond.getFlightRPHList(),
			    flownOnd : ond.isFlownOnd(),
			    existingFlightRPHList : getExistingFlightRPHList(ond),
			    existingResSegRPHList : getExistingResSegRPHList(ond),
			    departureDate : ond.getFirstDepartureDate(),
			    classOfService : ond.getCabinClass(),
//			    logicalCabinClass :ond.getLogicalCabinClass(),
			    logicalCabinClass: ondLogicalCC, 
			    bookingType : ond.getBookingType(),
			    bookingClass : ond.getBookingClass(),
			    modifiedResSegList: ond.getModifiedResSegList(),
			    dateChangedResSegList:ond.getDateChangedResSegList(),
			    sameFlightModification:ond.getSameFlightModification(),
			    oldPerPaxFare : ond.getOldPerPaxFare(),
			    eligibleToSameBCMod : ond.getEligibleToSameBCMod(),
				oldPerPaxFareTOList : ond.getOldPerPaxFareTOList(),
				status : ond.getStatus(),
				bundledServicePeriodId : ond.getBundledServicePeriodId()
			};
			list[list.length] = searchOND;
		}
		return list;
	}
	
	this.hasOverbookOnd = function() {
		var ondList = getONDList();
		var list = [];
		for ( var i = 0; i < ondList.length; i++) {
			var ond = ondList[i];
			if (ond.getBookingType() == ondStatus.OVERBOOK) {
				return true;
			}
		}
		return false;
	}
	
	this.hasCanceledFlightExist = function() {
		var ondList = getONDList();		
		for ( var i = 0; i < ondList.length; i++) {
			var ond = ondList[i];
			if (ond.hasCanceledFlight()) {
				return true;
			}
		}
		return false;
	}	

	function firstContainAll(first, second) {
		var count = 0;
		for ( var i = 0; i < first.length; i++) {
			var found = false;
			for ( var j = 0; j < second.length; j++) {
				if (second[j] == first[i]) {
					found = true;
					count++;
					break;
				}
			}
			if (found && count == second.length) {
				return true;
			}
		}
		return false;
	}

	function getExistingResSegRPHList(ondInfo) {

		if (ondInfo.getStatus() == 'NEW') {
			var rphList = ondInfo.getFlightRPHList();
			for ( var i = 0; i < removedOndList.length; i++) {
				var existnRPHs = removedOndList[i].getFlightRPHList();
				if ((ondInfo.getCabinClass() == removedOndList[i]
				        .getCabinClass())
				        && firstContainAll(existnRPHs, rphList)) {
					return removedOndList[i].getResSegRPHList();
				}
			}
		} else if (ondInfo.getStatus() == 'CNF') {
			return ondInfo.getResSegRPHList();
		}
		return [];
	}

	function getExistingFlightRPHList(ondInfo) {
		if (ondInfo.getStatus() == 'NEW') {
			var rphList = ondInfo.getFlightRPHList();
			for ( var i = 0; i < removedOndList.length; i++) {
				var existnRPHs = removedOndList[i].getFlightRPHList();
				if ((ondInfo.getCabinClass() == removedOndList[i]
				        .getCabinClass())
				        && firstContainAll(existnRPHs, rphList)) {
					return existnRPHs;
				}
			}
		} else if (ondInfo.getStatus() == 'CNF') {
			return ondInfo.getFlightRPHList();
		}
		return [];
	}	

	this.restructureOndsChgFare = function(changedFareInfos) {

		var ondList = getONDList();

		var changedCCMap = {};
		for ( var i = 0; i < changedFareInfos.length; i++) {
			changedCCMap[changedFareInfos[i].fltRefNo] = changedFareInfos[i].ccCode;
		}

		function deriveChange(fltRphs, ccCode) {
			var changed = false;
			var newCCMap = {};
			for ( var i = 0; i < fltRphs.length; i++) {
				var cabin = ccCode;
				if (changedCCMap[fltRphs[i]] != null) {
					if (changedCCMap[fltRphs[i]] != ccCode) {
						cabin = changedCCMap[fltRphs[i]];
						changed = true;
					}
				}

				if (newCCMap[cabin] == null) {
					newCCMap[cabin] = [];
				}
				newCCMap[cabin][newCCMap[cabin].length] = fltRphs[i];
			}
			var criteria = [];
			if (changed) {
				$.each(newCCMap, function(ccCode, rphArr) {
					criteria[criteria.length] = {
					    cabinClass : ccCode,
					    rphs : rphArr
					}
				});
			}

			return {
			    changed : changed,
			    criteria : criteria
			};
		}

		var ondTBSplitted = [];
		for ( var i = 0; i < ondList.length; i++) {
			var ond = ondList[i];
			if (ond.getBookingType() == ondStatus.OPENRT || ond.isFlownOnd()) {
				continue;
			}
			// var searchOND = {
			// fromAirport : ond.getOrigin(),
			// toAirport : ond.getDestination(),
			// flightRPHList : ond.getFlightRPHList(),
			// flownOnd : ond.isFlownOnd(),
			// existingFlightRPHList : getExistingFlightRPHList(ond),
			// existingResSegRPHList : getExistingResSegRPHList(ond),
			// departureDate : ond.getFirstDepartureDate(),
			// classOfService : ond.getCabinClass(),
			// bookingType : ond.getBookingType(),
			// bookingClass : ond.getBookingClass()
			// };

			var ccChangeStat = deriveChange(ond.getFlightRPHList(), ond
			        .getCabinClass());
			if (ccChangeStat.changed) {
				ondTBSplitted[ondTBSplitted.length] = {
				    ond : ond,
				    ondIndex : i,
				    criteria : ccChangeStat.criteria
				};
			}
		}

		for ( var i = 0; i < ondTBSplitted.length; i++) {
			var ond = ondTBSplitted[i].ond;
			var flts = ond.getFlights();
			var ondTBAdded = [];
			for ( var j = 0; j < ondTBSplitted[i].criteria.length; j++) {
				var newOnd = new ONDInfo();
				var newOndFlts = [];
				for ( var k = 0; k < ondTBSplitted[i].criteria[j].rphs.length; k++) {
					for ( var p = 0; p < flts.length; p++) {
						if (ondTBSplitted[i].criteria[j].rphs[k] == flts[p].segmentRPH) {
							newOndFlts[newOndFlts.length] = flts[p];
						}
					}
				}

				newOnd.addFlightSegments(newOndFlts, {
				    bookingType : ond.getBookingType(),
				    // bookingClass = jsonFQParams.bookingClass;
				    cabinClass : ondTBSplitted[i].criteria[j].cabinClass
				});

				ondTBAdded[ondTBAdded.length] = newOnd;
			}

			removeOnd(ondTBSplitted[i].ondIndex + 1, true)
			for ( var j = 0; j < ondTBAdded.length; j++) {
				addNewOnd(ondTBAdded[j]);
			}
		}
		UI_commonSystem.fillGridData({
		    id : "#tblAllONDs",
		    data : this.getAllsegments()
		});
	};
	
	this.getJsonONDSearchDTOList = function(){
		return $.toJSON(UI_requote.getONDSearchDTOList());
	}
	

	this.paymentSubmitData = function() {
		var data = UI_tabSearchFlights.createFareQuoteParams();
		data['fareQuoteParams.ondListString'] = $.toJSON(UI_requote
		        .getONDSearchDTOList());
		data['fareQuoteParams.ticketValidTillStr'] = UI_requote
		        .getTicketValidTill();
		data['fareQuoteParams.lastFareQuoteDateStr'] = UI_requote
		        .getLastFareQuoteDate();
		data['balanceQueryData'] = $.toJSON(getBalanceQueryData());
		data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
		delete data['fareQuoteParams.bookingClassCode'] ;
		if (DATA_ResPro.reservationInfo.status == 'OHD'
				|| DATA_ResPro.reservationInfo.status == 'CNX') {
			data['openSegments'] = $.toJSON(getAllSegmentsForOHDRelCal());
		}
		return data;
	}

	this.fareDiscountSubmitData = function() {
		var data = UI_tabSearchFlights.createFareQuoteParams();
		data['fareQuoteParams.ondListString'] = $.toJSON(UI_requote
		        .getONDSearchDTOList());
		data['fareQuoteParams.ticketValidTillStr'] = UI_requote
		        .getTicketValidTill();
		data['fareQuoteParams.lastFareQuoteDateStr'] = UI_requote
		        .getLastFareQuoteDate();
		delete data['fareQuoteParams.bookingClassCode'] ;
		return data;
	}

	this.getFlexiQuoteForRequote = function(){
	    var ondSearchDTOList = UI_requote.getONDSearchDTOList();
	    return overrideFlexiQuoteForRequote(ondSearchDTOList);
	}
	
	this.showExchangedSegmentAlertForbundleChange = function() {
		if (DATA_ResPro.initialParams.allowToRemoveReprotectedAnciForOHD
				&& DATA_ResPro.reservationInfo.status == 'OHD'
				&& isLogicalCCSelected
				&& UI_tabSearchFlights.ondWiseBundleFareSelected != null
				&& !$.isEmptyObject(UI_tabSearchFlights.ondWiseBundleFareSelected)) {
			alertReprotectingAnciRemoval(UI_requote.getONDSearchDTOList());
		}
	}
	
	this.requoteFareQuote = function() {
		$("#divRQLogicalCCList").hide();
		UI_tabSearchFlights.releaseBlockSeats();
		if(UI_requote.hasCanceledFlightExist()) {
			alert('Canceled flight(UN) segment found!');
			return false;
		} else {
			if (UI_tabSearchFlights.validateFareQuote()) {
					UI_tabSearchFlights.disableEnablePageControls(false);
					var data = UI_tabSearchFlights.createFareQuoteParams();
					UI_requote.removeGroundSegment();
					var ondSearchDTOList = UI_requote.getONDSearchDTOList();
					
					overrideBundledServicesForRequote(ondSearchDTOList);
					data['operatingCarrierCodesList'] = getOperatingCarriers(ondSearchDTOList);							
					overrideSameFlightModificationForRequote(ondSearchDTOList);
					
					data['pnr'] = DATA_ResPro.reservationInfo.PNR; //UI_tabSearchFlights.PNR;
					data['taxInvoicesExist'] = DATA_ResPro.reservationInfo.taxInvoicesExist;
					data['fareQuoteParams.ondListString'] = $.toJSON(ondSearchDTOList);
					data['fareQuoteParams.ticketValidTillStr'] = UI_requote
					        .getTicketValidTill();
					data['fareQuoteParams.lastFareQuoteDateStr'] = UI_requote
					        .getLastFareQuoteDate();
					delete data['fareQuoteParams.bookingClassCode'] ;
					data['fareQuoteParams.ondQuoteFlexiStr'] = overrideFlexiQuoteForRequote(ondSearchDTOList);
					data['fareQuoteParams.ondSelectedFlexiStr'] = fillEmptyForSelectedFlexiForRequote(ondSearchDTOList);
					
					//skipInvalidOndLogicalCCRebuild(ondSearchDTOList);					
					
					//TODO: this is temp fix for hide low fare COS from user who dont have prev
					//needs to be refactored and eliminate all un-neccessary code
					if(data['fareQuoteParams.classOfService']!=null && data['fareQuoteParams.classOfService']!="" 
						&& (UI_requote.selectedSegmentCabinclass==null || UI_requote.selectedSegmentCabinclass=="")){
						UI_requote.selectedSegmentCabinclass = 	data['fareQuoteParams.classOfService'] ;
					}
					if (UI_requote.overlappingOND) {
						UI_requote.overlappingOND = false;
						return false;
					}					
					UI_commonSystem.showProgress();
					UI_commonSystem.getDummyFrm().ajaxSubmit({
					    dataType : 'json',
					    data : data,
					    url : "fareRequote.action",
					    success : returnCallShowFareQuoteInfo,
					    error : UI_commonSystem.setErrorStatus
					});
			}
		}
	};
	
	function alertReprotectingAnciRemoval(ondSearchDTOList){
		var alertReprotectingAnciRemoval = false;
		
		for (var i = 0;i < ondSearchDTOList.length; i++) {
			if (ondSearchDTOList[i].status == 'CNF' && !ondSearchDTOList[i].flownOnd) {
				var previousBundleFareId = ondSearchDTOList[i].bundledServicePeriodId;
				var newBundleFareId = UI_tabSearchFlights.ondWiseBundleFareSelected[i];
				/*
				 * Alert should be given to reprotected Ancillaries if bundle is changed to 
				 * Bundle_1 -> Basic
				 * Bundle_1 -> Bundle_2
				 * 
				 * bundleFareId will be null if it is Basic
				 */
				if (!(typeof newBundleFareId === 'undefined')
						&& (previousBundleFareId != null && newBundleFareId == null)
						|| (previousBundleFareId != null
								&& newBundleFareId != null && previousBundleFareId != newBundleFareId)) {
					alertReprotectingAnciRemoval = true;
					break;
				}
			}
		}
		
		if (alertReprotectingAnciRemoval) {
			alert('Since the bundle(s) has been changed reprotecting ancillaries will be removed for the exchanged segment(s)');
		}
	}
	
	function getOperatingCarriers(ondSearchDTOList){
		var operatingCarriers = new Array();
		for ( var i = 0; i < ondSearchDTOList.length; i++) {
			var searchObj = ondSearchDTOList[i];
			var flightRPHs = searchObj.flightRPHList;
			for ( var j = 0; j < flightRPHs.length; j++) {
				var flightRPH = flightRPHs[j];
				if(flightRPH != null && flightRPH != ''){
					var carrierCode = flightRPH.split("$")[0];
					if(UI_tabSearchFlights.busCarrierCodes != null){
						if($.inArray(carrierCode, UI_tabSearchFlights.busCarrierCodes) != -1){
							continue;
						}
					}
					
					if(DATA_ResPro["existingBusCarrierCodes"] != null){
						if($.inArray(carrierCode, DATA_ResPro["existingBusCarrierCodes"]) != -1){
							continue;
						}
					}
					
					if($.inArray(carrierCode, operatingCarriers) == -1){
						operatingCarriers.push(carrierCode);
					}
				}
			}
		}
		
		return operatingCarriers;
	}

	function skipInvalidOndLogicalCCRebuild(ondSearchDTOList){
		$.each(UI_tabSearchFlights.ondRebuildLogicalCCAvailability, function(ondSeq, val){
			if(ondSeq > (ondSearchDTOList.length-1)){
				UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSeq] = false;
			}
		});
	}
	
	function fillEmptyForSelectedFlexiForRequote(ondSearchDTOList){
	    var ondFlexiQuote= $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);
	    for ( var ondIndex = 0; ondIndex < ondSearchDTOList.length; ondIndex++) {
	         if(ondFlexiQuote[ondIndex] == null){ 
	            ondFlexiQuote[ondIndex] = false;
	        }
	    }
	    return  $.toJSON(ondFlexiQuote);
	}
	
	function overrideBundledServicesForRequote(ondSearchDTOList){
		if(isLogicalCCSelected && UI_tabSearchFlights.ondWiseBundleFareSelected != null &&
				!$.isEmptyObject(UI_tabSearchFlights.ondWiseBundleFareSelected)){			
			for ( var ondIndex = 0; ondIndex < ondSearchDTOList.length; ondIndex++) {			
				ondSearchDTOList[ondIndex].bundledServicePeriodId = UI_tabSearchFlights.ondWiseBundleFareSelected[ondIndex];
			}
		}
	}
	
	function overrideSameFlightModificationForRequote(ondSearchDTOList){
		if(DATA_ResPro.blnIsFromNameChange){
			for ( var ondIndex = 0; ondIndex < ondSearchDTOList.length; ondIndex++) {			
				ondSearchDTOList[ondIndex].sameFlightModification = true;
			}			
		}		
	}
	
	function overrideFlexiQuoteForRequote(ondSearchDTOList){
		var flexiAlerts = $.parseJSON(DATA_ResPro["flexiAlertInfo"]);		
		var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiQuote);
		
		for ( var ondIndex = 0; ondIndex < ondSearchDTOList.length; ondIndex++) {			
			if(ondFlexiQuote[ondIndex] == undefined || ondFlexiQuote[ondIndex] == null){
				ondFlexiQuote[ondIndex] = true;
			}
		}
		
		if(flexiAlerts != null && flexiAlerts != ''){		
			OUTER:
			for ( var ondIndex = 0; ondIndex < ondSearchDTOList.length; ondIndex++) {
				var searchOnd = ondSearchDTOList[ondIndex];				
				var resSegRPHList = searchOnd.modifiedResSegList;
				if(resSegRPHList == null || resSegRPHList.length == 0){					
					resSegRPHList = searchOnd.existingResSegRPHList;
				}
				if(resSegRPHList != null){
					for ( var resIndex = 0; resIndex < resSegRPHList.length; resIndex++) {
						var pnrSeg = resSegRPHList[resIndex];
						if(pnrSeg != undefined && pnrSeg != null && pnrSeg != ""){
							var pnrSegArr = pnrSeg.split("$");
							var pnrSegId = null;
							if(pnrSegArr.length > 2){
								pnrSegId = pnrSegArr[2]
							} else {
								pnrSegId = pnrSegArr[0];
							}
							
							for ( var flexiIndex in flexiAlerts) {
								var objAltDt = flexiAlerts[flexiIndex];
								var modifyFlexiBooking = false;
								if (pnrSegId == objAltDt.flightSegmantRefNumber) {
									for (var a = 0 ; a  < objAltDt.alertTO.length; a++){
										if(objAltDt.alertTO[a].alertId == 1){// Modification
											var availModifications = (new  Number(objAltDt.alertTO[a].content)) - 0;
											if(availModifications > 0){
												modifyFlexiBooking = true;
												break;
											}
										} else if(objAltDt.alertTO[a].alertId == 2){// Cancellation
											var availCancellations = (new  Number(objAltDt.alertTO[a].content)) - 0;
											if(availCancellations > 0){ 
												modifyFlexiBooking = true;
												break;
											}
										}
									}
								}
								
								if(modifyFlexiBooking){
									ondFlexiQuote[ondIndex] = false;
									continue OUTER;									
								}
								
							}
						}
					}
				}
			}			
		}
		
		return $.toJSON(ondFlexiQuote);
	}

	this.validateFareQuote = function() {
		var ondList = UI_requote.getONDSearchDTOList();
		if (ondList != null && ondList.length > 0) {
			return true;
		}
		return false;
	}

	this.createCarrierWiseOndMap = function() {
		var ondMap = '';
		for ( var i = 0; i < newOndList.length; i++) {
			if (i == 0) {
				ondMap = '[{';
			}			
			
			var carrierWiseOnd = UI_requote.buildCarrierWiseOnd(newOndList[i]);

			if (i == (newOndList.length - 1)) {
				ondMap += carrierWiseOnd + '}]';
			} else {
				ondMap += carrierWiseOnd + '},{';
			}

		}
		return ondMap;
	}
	
	this.buildCarrierWiseOnd = function (ondObj) {
		var firstFlightRef = ondObj.getFlightRPHList()[0];
		var lastFlightRef = ondObj.getFlightRPHList()[ondObj.getFlightRPHList().length-1];
		var ond = '';		
		var fltRefInfoArray = firstFlightRef.split('$');
		var carrier = fltRefInfoArray[0];
		if (firstFlightRef==lastFlightRef) {			
			var airportCodes = UI_requote.getAirportCodesFromFlightRPH(firstFlightRef);			
			ond = airportCodes[0]+ '/' + airportCodes[airportCodes.length-1];	
		} else {			
			var firstSegAirportCodes = UI_requote.getAirportCodesFromFlightRPH(firstFlightRef);
			var lastSegAirportCodes = UI_requote.getAirportCodesFromFlightRPH(lastFlightRef);			
			ond = firstSegAirportCodes[0]+ '/' + lastSegAirportCodes[lastSegAirportCodes.length-1];
		}		
		var carrierWiseOnd = '"' + carrier + '":"' + ond + '"';
		return carrierWiseOnd;
	}
	
	this.getAirportCodesFromFlightRPH = function (fltRef) {
		var airportCodes = '';
		if (fltRef!=undefined && fltRef!=null) {			
			var fltRefInfoArray = fltRef.split('$');			
			var segmentCode = fltRefInfoArray[1];
			airportCodes = segmentCode.split('/');			
		}
		return airportCodes;
	}
	
	this.setDisplayViewAllBCButton = function() {
		var ondList = getONDList();
		if(ondList != null){
			for(var i=0;i<ondList.length;i++){
				if(ondList[i].getOldPerPaxFareTOList() != null){
					if(ondList[i].getOldPerPaxFareTOList()[0]!=undefined && 
							ondList[i].getOldPerPaxFareTOList()[0].modifyToSameFare && !DATA_ResPro.initialParams.allowOverrideSameFare){
						$("#btnCAllFare").hide();
					} 
				}
			}
		}
	}

	function getRemovedResSegmentRPHList() {
		var resSegmentRPHs = [];
		UI_requote.removeGroundSegmentID();
		for ( var i = 0; i < removedOndList.length; i++) {			
			if(!removedOndList[i].getInverseAutoRemove() ){
				resSegmentRPHs = resSegmentRPHs.concat(removedOndList[i].getResSegRPHList());
			}			
		}
		
		return resSegmentRPHs;
	}

	this.getTicketValidTill = function() {
		if (removedOndList != null && removedOndList.length > 0) {
			return removedOndList[0].getTicketValidTill();
		}else if(newOndList.length > 0) {
			var isAddSeg = false;
			for ( var i = 0; i < newOndList.length; i++) {				
				if(newOndList[i].getStatus() == ondStatus.NEW){
					isAddSeg = true;
				}
			}
			if(!isAddSeg){ //If same flight modification, ticket valid till date should be same ond ticket valid date.
				for ( var j = 0; j < newOndList.length; j++) {
					var ondObj = newOndList[j];
					if(!ondObj.isFlownOnd()){
						return ondObj.getTicketValidTill();
					}
				}
			}else{
				return null;
			}
		}
		return null;
	}

	this.getLastFareQuoteDate = function() {
		if (removedOndList != null && removedOndList.length > 0) {
			return removedOndList[0].getLastFareQuoteDate();
		}else if(newOndList.length > 0) {
			var isAddSeg = false;
			for ( var i = 0; i < newOndList.length; i++) {				
				if(newOndList[i].getStatus() == ondStatus.NEW){
					isAddSeg = true;
				}
			}
			if(!isAddSeg){ //If same flight modification, last farequote date should be same ond last farequote date.
				for ( var j = 0; j < newOndList.length; j++) {
					var ondObj = newOndList[j];
					if(!ondObj.isFlownOnd()){
						return ondObj.getLastFareQuoteDate();
					}
				}
			}else{
				return null;
			}
		}
		return null;
	}
	
	function populateOldPerPaxFareTOList(ondFlights){
		if(ondFlights.length > 0){
			var oldPerPaxFareList = [];
			for ( var i = 0; i < ondFlights.length; i++) {
					oldPerPaxFareList[i] = ondFlights[i].oldPerPaxFareTO;
				}
			return oldPerPaxFareList;
		} else {
			return null;
		}
		
	}

	function populateExistingONDs() {
		function getExistingONDList() {
			var allSegs = DATA_ResPro.resSegments;
			var fareONDMap = {};
			for ( var i = 0; i < allSegs.length; i++) {
				if (allSegs[i].status != 'CNX') {
					// out[out.length] = allSegs[i];
					if (allSegs[i].interlineGroupKey != null) {
						if (fareONDMap[allSegs[i].interlineGroupKey] == null) {
							fareONDMap[allSegs[i].interlineGroupKey] = {
								flights : []
							};
						}
						var arr = fareONDMap[allSegs[i].interlineGroupKey].flights;
						arr[arr.length] = allSegs[i];
					} else {
						alert(' grp key is [' + allSegs[i].interlineGroupKey
						        + ']');
					}
				}
			}
			var out = [];
			$.each(fareONDMap, function(grpKey, obj) {
				var ondInfo = new ONDInfo();
				ondInfo.addExistingFlightSegments(obj.flights);
				var oldRmOndFlights = ondInfo.getFlights();
				ondInfo.setOldPerPaxFareTOList(populateOldPerPaxFareTOList(oldRmOndFlights));
				ondInfo.setEligibleToSameBCMod(false);
				out[out.length] = ondInfo;
			});

			return out;
		}
		newOndList = getExistingONDList();
		newOndList = newOndList.sort(sortOnds);
		originalJourney = getTotalJourney(true);
	}

	function sortOnds(ond1, ond2) {
		return ond1.getFirstDepartureDTZuluMillis() > ond2
		        .getFirstDepartureDTZuluMillis();
	}

	function getONDList() {
		return newOndList;
	}

	this.getAllsegments = function() {
		var ondList = getONDList();

		var data = [];
		for ( var i = 0; i < ondList.length; i++) {
			data[data.length] = ondList[i].createData(i);
		}
		return data;
	};
	
	this.isRequoteRequired = function() {

		if(DATA_ResPro.initialParams.reQuoteOnlyRequiredONDs) {		
			
			var ondList = getONDList();
			var data = [];
			var onlySegConnFareServed = true;
			var isAllFlownOnd = true;
			var unTouchedOndCount = 0;
			
			for ( var i = 0; i < ondList.length; i++) {
				ondInfo = ondList[i];
				if ((ondInfo.getOldPerPaxFareTOList() == null
						|| (ondStatus.CNF != ondInfo.getStatus()) || (ondInfo
						.getOldPerPaxFareTOList()[0].fareType == '1' || ondInfo
						.getOldPerPaxFareTOList()[0].fareType == '6'))) {

					onlySegConnFareServed = false;
				}

				if (ondInfo.getOldPerPaxFareTOList() != null
						&& "CNF" == ondInfo.getStatus()) {

					if (ondInfo.isFlownOnd()
							|| (!ondInfo.isFlownOnd() && (ondInfo
									.getModifiedResSegList() == "undefined" || ondInfo
									.getModifiedResSegList() == null))) {

						if (ondInfo.getExistingFlightRPHList() != "undefined"
								&& ondInfo.getExistingFlightRPHList() != null
								&& ondInfo.getExistingFlightRPHList().length > 0) {
							unTouchedOndCount++;
						}
					}
				}

				if (!ondInfo.isFlownOnd()) {
					isAllFlownOnd = false;
				}

			}

			if (unTouchedOndCount == ondList.length && removedOndList.length == 0) {
				$('#divFarePane').hide();
				UI_requote.skipFareReQuote = true;
				return false;
			} else {
				$('#divFarePane').show();
				UI_requote.skipFareReQuote = false;
				return true;
			}
			
		} else {			
			UI_requote.skipFareReQuote = false;
			return true;
		}		

	};
	

	function getAllSegmentsForOHDRelCal() {
		var ondList = getONDList();

		var data = [];
		for ( var i = 0; i < ondList.length; i++) {
			data = data.concat(ondList[i].getAllOpenSegmentsForOHDRelCal());
		}
		return data;
	}

	/*
	 * Grid Constructor
	 */
	this.constructGrid = function(inParams) {

		var COL_NAMES = {
		    INDEX : 'index',
		    OND : 'ond',
		    FLIGHT_NO : 'flightNo',
		    CARRIER_CODE : 'carrierCode',
		    DEPARTURE_DATE : 'departureDate',
		    ARRIVAL_DATE : 'arrivalDate',
		    DEPARTURE_DATETIME_ZULU : 'departureDateTimeZulu',
		    ARRIVAL_DATETIME_ZULU : 'arrivalDateTimeZulu',
		    SEGMENT_RPH : 'segmentRPH',
		    CABIN_CLASS : 'cabinClass',
		    BOOKING_TYPE : 'bookingType',
		    STATUS : 'status',
		    OLD_FARE : 'oldPerPaxFare',
		    ADD_BUS : 'addGroundSegment',
		    BUS_SEG_ID : 'groundStationPnrSegmentID'
		};

		if (DATA_ResPro.initialParams.addSegment
		        && !DATA_ResPro.initialParams.voidReservation) {
			$("#btnAddNewOND").decorateButton();
			$("#btnAddNewOND").attr('style', 'width:auto;');
			$("#btnAddNewOND").click(function() {
				//
				UI_requote.enableONDSearch();
				UI_requote.addGroundSegment = false;
				UI_requote.selectedFltSeg = null;
			});
			
			
			$("#btnAddGroundSegment").decorateButton();
			$("#btnAddGroundSegment").attr('style', 'width:auto;display: none;');
			$("#btnAddGroundSegment").click(function() {				
				UI_requote.enableONDSearch();
				UI_requote.addGroundSegment = true;
				
				if(UI_requote.selectedFltSeg==undefined|| UI_requote.selectedFltSeg==null){					
				    var selRowId = $("#tblAllONDs").getGridParam('selrow');
				    UI_requote.selectedFltSeg = jQuery("#tblAllONDs").getRowData(selRowId)['segmentRPH'];					
				}
			});
			$("#btnAddNewOND").show();
		} else {
			$("#btnAddNewOND").hide();
		}

		// construct grid
		var radioButtonFmt = function(index, options, rowObject) {
			var prefix = '';
			var checked = '';
			var disabled = '';

			return "<input type='radio' id='" + index + prefix
			        + "Selection' name='" + prefix + "Selection' " + checked
			        + disabled + "  value='" + index + "'>";
		};

		var dateFmt = function(dateTimeStr) {
			var formattedDate = '';
			var dateTimeArr = dateTimeStr.split('T');
			var time = dateTimeArr[1];
			var dateString =  dateTimeArr[0].replace(/-/g,"/");//for IE compatibility
			var parsedDate = new Date(dateString);
			var now = new Date();
			var dateObj = dtCOffsetCalculation(now.getTimezoneOffset(),parsedDate.getTime());
			var shortYear = (dateObj.getFullYear() + '').substring(2);
			var dow =getWDayFromDateObj(dateObj);

			formattedDate = dow+ " " +dateObj.getDate()
			        + autoDate_monthArr[dateObj.getMonth()] + shortYear + " "
			        + time.substring(0, 5);

			return formattedDate;
		}
		
		 var dtCOffsetCalculation = function(gmtOffset,timeInMillis){
			gmtOffset = gmtOffset*60*1000;
			var d = null;
				if(gmtOffset<0){
				  	d= new Date(timeInMillis - gmtOffset); 
				}else{
					d= new Date(timeInMillis + gmtOffset); 
				}
			return d;
	    }
	
		var arrBreakFmt = function(arr, options, rowObject) {
			var outStr = '';
			if (rowObject.bookingType != ondStatus.OPENRT) {
				var first = true;
				for ( var i = 0; i < arr.length; i++) {

					var arrVal = arr[i];
					if (options.colModel.name == COL_NAMES.DEPARTURE_DATE
					        || options.colModel.name == COL_NAMES.ARRIVAL_DATE) {
						arrVal = dateFmt(arr[i]);
					}

					if (!first)
						outStr += '<br />';
					outStr += arrVal;
					first = false;
				}
			}
			//if (rowObject.bookingType == ondStatus.OPENRT) {
			//	UI_tabSearchFlights.isOpenReturn = true;
		//	}
			return outStr;
		};

		var statusFmt = function(arr, options, rowObject) {
			if (rowObject.bookingType != ondStatus.OPENRT && rowObject.status != ondStatus.WL) {
				return rowObject.status;
			} else {
				// if open return or wait listed segment exists, hide add new OND button
				$("#btnAddNewOND").hide();
				return rowObject.status;
			}
		}

		var isFlownSegmentExists = function(flowStatusList) {
			for ( var i = 0; i < flowStatusList.length; i++) {
				if (flowStatusList[i]) {
					return true;
				}
			}

			return false;
		}
		

		var carrierCodeFmt = function (arr, options, rowObject){
			if(rowObject.busSegment!=undefined && rowObject.busSegment){				
				return '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">';								
			}
			return arrBreakFmt(arr, options, rowObject);
		}

		var removeFmt = function(arr, options, rowObject) {
			var fmtStr = "<span>";
			if (!isFlownSegmentExists(rowObject.flownSegment)) {
				if (DATA_ResPro.initialParams.cancelSegment &&
						rowObject.cancellable) {
					var strBtnCancelSegement = geti18nData('btn_CancelSegment','Cancel Segment');
					var btnCancelSeg = "<button type='button' title='Cancel Segment' style='width:110px;margin:1px;' class='removeRow btnMedium ui-state-default ui-corner-all' id='removeRow_"
					        + options.rowId + "'>"+strBtnCancelSegement+"</button>";
					fmtStr += btnCancelSeg;
				}

				if (rowObject.bookingType != ondStatus.OPENRT && rowObject.status != ondStatus.WL && 
						(modifyUNSegments == "true" || (modifyUNSegments == "false" && rowObject.status != ondStatus.UN))) {
						var strBtnModifySegement = geti18nData('btn_ModifySegment','Modify Segment');
					if (DATA_ResPro.initialParams.modifySegmentByDate
					        && DATA_ResPro.initialParams.modifySegmentByRoute
					        && rowObject.modifible && rowObject.modifyByDate
					        && rowObject.modifyByRoute) {
						var btnModSeg = "<button type='button' value='Modify Segment' title='Modify Segment' style='width:110px;margin:1px;' class='modifySeg btnMargin btnMedium ui-state-default ui-corner-all' id='modRes_"
						        + options.rowId + "'>"+strBtnModifySegement+"</button>";
						fmtStr += btnModSeg;
					} else {
						if (DATA_ResPro.initialParams.modifySegmentByDate
						        && rowObject.modifible
						        && rowObject.modifyByDate) {
							var btnModDate = "<button type='button' value='Modify Date' title='Modify By Date' style='width:110px;margin:1px;' class='modifySeg btnMargin btnMedium ui-state-default ui-corner-all' id='modByDate_"
							        + options.rowId
							        + "'>Modify By Date</button>";
							fmtStr += btnModDate;
						}

						if (DATA_ResPro.initialParams.modifySegmentByRoute
						        && rowObject.modifible
						        && rowObject.modifyByRoute) {
							var btnModRoute = "<button type='button' value='Modify Route' title='Modify By Route' style='width:110px;margin:1px;' class='modifySeg btnMargin btnMedium ui-state-default ui-corner-all' id='modByRoute_"
							        + options.rowId
							        + "'>Modify By Route</button>";
							fmtStr += btnModRoute;
						}
					}
				}
			}

			return fmtStr + "</span>";
		};

		$(inParams.id).jqGrid(
		        {
		            datatype : "local",
		            colNames : [ '&nbsp;',geti18nData('BookingInfo_FlightDetails_lbl_OriginAndDestination','OND'),geti18nData('BookingInfo_FlightDetails_lbl_Flight','Flight'), geti18nData('BookingInfo_FlightDetails_lbl_OperatingCarrier','Op'),geti18nData('BookingInfo_FlightDetails_lbl_Departure','Departure'),
		                         geti18nData('BookingInfo_FlightDetails_lbl_Arrival','Arrival'), geti18nData('BookingInfo_FlightDetails_lbl_DepartureZulu','Departure Zulu'), geti18nData('BookingInfo_FlightDetails_lbl_ArrivalZulu','Arrival Zulu'), geti18nData('BookingInfo_FlightDetails_lbl_RPH','RPH'),
		                         geti18nData('BookingInfo_FlightDetails_lbl_Cabin','Cabin'), geti18nData('BookingInfo_FlightDetails_lbl_BookingType','Booking Type'),geti18nData('Ancillary_lbl_STATUS','Status'), '', '','','' ],
		            colModel : [ {
		                name : COL_NAMES.INDEX,
		                index : COL_NAMES.INDEX,
		                width : 20,
		                align : "center",
		                formatter : radioButtonFmt,
		                hidden : true
		            }, {
		                name : COL_NAMES.OND,
		                index : COL_NAMES.OND,
		                width : 90,
		                align : "left"
		            }, {
		                name : COL_NAMES.FLIGHT_NO,
		                index : COL_NAMES.FLIGHT_NO,
		                width : 65,
		                formatter : arrBreakFmt,
		                align : "center"
		            }, {
		                name : COL_NAMES.CARRIER_CODE,
		                index : COL_NAMES.CARRIER_CODE,
		                width : 30,
		                formatter : carrierCodeFmt,
		                align : "center"
		            }, {
		                name : COL_NAMES.DEPARTURE_DATE,
		                index : COL_NAMES.DEPARTURE_DATE,
		                width : 85,
		                formatter : arrBreakFmt,
		                align : "center"
		            }, {
		                name : COL_NAMES.ARRIVAL_DATE,
		                index : COL_NAMES.ARRIVAL_DATE,
		                width : 85,
		                formatter : arrBreakFmt,
		                align : "center"
		            }, {
		                name : COL_NAMES.DEPARTURE_DATETIME_ZULU,
		                index : COL_NAMES.DEPARTURE_DATETIME_ZULU,
		                width : 85,
		                formatter : arrBreakFmt,
		                hidden : true,
		                align : "center"
		            }, {
		                name : COL_NAMES.ARRIVAL_DATETIME_ZULU,
		                index : COL_NAMES.ARRIVAL_DATETIME_ZULU,
		                width : 85,
		                formatter : arrBreakFmt,
		                hidden : true,
		                align : "center"
		            }, {
		                name : COL_NAMES.SEGMENT_RPH,
		                index : COL_NAMES.SEGMENT_RPH,
		                width : 120,
		                formatter : arrBreakFmt,
		                align : 'center',
		                hidden : true
		            }, {
		                name : COL_NAMES.CABIN_CLASS,
		                index : COL_NAMES.CABIN_CLASS,
		                width : 30,
		                align : "center"
		            }, {
		                name : COL_NAMES.BOOKING_TYPE,
		                index : COL_NAMES.BOOKING_TYPE,
		                width : 50,
		                align : "center"
		            }, {
		                name : COL_NAMES.STATUS,
		                index : COL_NAMES.STATUS,
		                width : 50,
		                formatter : statusFmt,
		                align : "center"
		            }, {
		                name : COL_NAMES.INDEX,
		                formatter : removeFmt,
		                width : 120,
		                align : "center"
		            }, {
		            	name : COL_NAMES.OLD_FARE,
		                index : COL_NAMES.OLD_FARE,
		                width : 85,
		                formatter : arrBreakFmt,
		                hidden : true,
		                align : "center"
		            } , {
		            	name : COL_NAMES.ADD_BUS,
		                index : COL_NAMES.ADD_BUS,
		                width : 85,
		                formatter : arrBreakFmt,
		                hidden : true,
		                align : "center"
		            } , {
		            	name : COL_NAMES.BUS_SEG_ID,
		                index : COL_NAMES.BUS_SEG_ID,
		                width : 85,
		                formatter : arrBreakFmt,
		                hidden : true,
		                align : "center"
		            } ],
		            imgpath : UI_commonSystem.strImagePath,
		            multiselect : false,
		            viewrecords : true,
		            width : "auto",
		            onSelectRow : function(rowid) {
		            	UI_tabSearchFlights.showLogicalCabinInfo(rowid-1);		            	
		            	UI_requote.enableBus(rowid);
			            // if(!$("#" + rowid +
			            // inParams.radioID).attr('disabled')){
			            // $("#" + rowid + inParams.radioID).attr('checked',
			            // true);
			            // UI_tabSearchFlights.hideFareQuotePane();
			            // UI_tabSearchFlights.buildFareQuoteHD();
			            // }else{
			            // $(inParams.id).resetSelection();
			            // }
		            }
		        });
		$('#trAllONDs').show();
	};
	
	this.enableONDSearch = function(){
		resetModificationOperation();
		$("#divRQLogicalCCList").hide();
		$("#btnAddOND").text('Add OND');
		UI_tabPayment.isRequoteAddSegment = true;
		UI_tabPayment.isRequoteSegModify  = false;
		// Show flight search pane
		$("#tdSearchFltArea").show();
		addSegmentClicked = true;
		modifySegmentClicked = false;
		selectedOndIndex = -1;
		$("#fAirport").enable();
		$("#tAirport").enable();
		$("#departureDate").enable();
		$("#departureDate").datepicker('enable');
		$("#departureVariance").enable();
		$("#departureVariance").val(top.strDefDV);
		$("#oldPerPaxFare").val('0');
		$("#fAirport").val('');
		$("#tAirport").val('');
		UI_tabSearchFlights.fromCombo.setValue("");
		UI_tabSearchFlights.toCombo.setValue("");
		$("#departureDate").val('');
		$("#lnkON").show();
		$("#lnkOP").show();
		
		$("#hdnBookingClassCode").val('');
		$("#hdnCabinClassCode").val('');				
		$("#selBookingClassCode").enable();
		$("#selBookingClassCode").val('');
		UI_requote.selectedSegmentCabinclass = "";
		enableDisableOpenReturnOption(true);
		UI_tabSearchFlights.clearAll();
		UI_requote.resetLogicalCCRebuildInfo();
		UI_commonSystem.strPGMode = DATA_ResPro.reservationInfo.mode;
		$("#trCheckStopOver").hide();
	}
	
	this.enableBus = function(rowid){
		var addGroundSegment = jQuery("#tblAllONDs").getRowData(rowid)['addGroundSegment'];
    	if(addGroundSegment.indexOf("true")!=-1){
    		showAddGroundSegment(true);
    		UI_requote.selectedFltSeg = jQuery("#tblAllONDs").getRowData(rowid)['segmentRPH'];
    		var selFltSegId = UI_requote.getFlightSegIdFromRPH(UI_requote.selectedFltSeg);
    		if(groundFltSegByFltSegId!=undefined && groundFltSegByFltSegId!=null){
    			$.each(groundFltSegByFltSegId, function(key, value){
    				if(key==selFltSegId){
    					showAddGroundSegment(false);
    					return;
    				}
    			});
    		}
    		
    	}else{		
    		showAddGroundSegment(false);
    		
    		var selFltSegId = UI_requote.getFlightSegIdFromRPH(UI_requote.selectedFltSeg);
    		var newGroundSegAdded = false;
    		if(groundFltSegByFltSegId!=undefined && groundFltSegByFltSegId!=null){
    			$.each(groundFltSegByFltSegId, function(key, value){
    				if(key==selFltSegId && UI_requote.isSegmentExist(key)){
    					newGroundSegAdded = true;
    					return;
    				}
    			});
    		}
    		
    		var groundPnrSegId = jQuery("#tblAllONDs").getRowData(rowid)['groundStationPnrSegmentID'];
    		if(!newGroundSegAdded && removedOndList!=null && removedOndList.length > 0){
    			$.each(removedOndList, function(key, obj){    				
    				var pnrSegID = UI_requote.getFlightSegIdFromRPH(obj.getResSegRPHList()[0]);
    				if(groundPnrSegId==pnrSegID){
    					showAddGroundSegment(true);
    					return;
    				}
    			});
    		}    		
    	}
    	
    	var isBusSegment = jQuery("#tblAllONDs").getRowData(rowid)['busSegment'];
    	if(isBusSegment){
    		var tdOndConfig = 'tdLogicalCabins_' + rowid;			
			$("#" + tdOndConfig).hide();
    	}
	}
	

	this.resetLogicalCCRebuildInfo = function(){
		$('#tdRQLogicalCCList').empty();
		$.each(UI_tabSearchFlights.ondRebuildLogicalCCAvailability, function(ondSequence, val){
			UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSequence] = true;
		});
	}
	
	this.removeRebuildeableLogicalCCInfoTables = function(){
		$.each(UI_tabSearchFlights.ondRebuildLogicalCCAvailability, function(ondSeq, rebuildeable){
			if(rebuildeable){
				$('#tdLogicalCabins_' + ondSeq).empty();
			}
		});
	}

	function enableDisableOpenReturnOption(isAddNewOnd) {
		if (DATA_ResPro.initialParams.openReturnEnabled) {
			var nonOpenRTOndCount = getNonOpenRTOndCount();
			if ((isAddNewOnd && nonOpenRTOndCount < 1) || !isAddNewOnd
			        && nonOpenRTOndCount == 1) {
				resetOpenRToptions();
				$("#divValidity").show();
				$("#validity").show();
				$("#divOpen").show();
				$("#openReturn").show();
				if(UI_tabSearchFlights.isOpenReturn){			
					$("#openReturn").attr("checked", true);
					$("#validity").enable();
				}	
			} else {
				resetOpenRToptions();
				$("#divValidity").hide();
				$("#validity").hide();
				$("#divOpen").hide();
				$("#openReturn").hide();
			}
		}
	}

	function getNonOpenRTOndCount() {
		var count = 0;
		for ( var i = 0; i < newOndList.length; i++) {
			if (newOndList[i].getBookingType() != ondStatus.OPENRT) {
				count++;
			}
		}
		return count;
	}

	function resetOpenRToptions() {
		$("#validity").val('');
		$("#openReturn").attr('checked', false);
	}

	function resetModificationOperation() {
		$("#btnBook").hide();
		$("#btnFQ").hide();
		$("#tblIBOBFlights").slideUp('fast');
		$("#trAddOND").slideUp('fast');
		$("#trFareSection").slideUp('fast');
		$("#divFareQuotePane").slideUp('fast');
		$("#trSearchParams").show();
	}	
	
	function showAddGroundSegment(status){
		if (DATA_ResPro.initialParams.groundServiceEnabled && status) {
			$("#btnAddGroundSegment").show();
		} else {
			$("#btnAddGroundSegment").hide();
		}	
	}
	
	function getTotalJourney(isOriginal){
		var totalJourney = '';
		var tmpOndList = $.extend(true,[],newOndList);
		if(isOriginal && removedOndList != null){
			for ( var i = 0; i < removedOndList.length; i++) {
				if(!hasRmdOndInOriginalOndList(removedOndList[i])){
					tmpOndList[tmpOndList.length] = removedOndList[i];
				}
			}
			tmpOndList = tmpOndList.sort(sortOnds);
		}
		for ( var i = 0; i < tmpOndList.length; i++) {
			var fltRefList = tmpOndList[i].getFlightRPHList();
			if(!tmpOndList[i].getBusSegment()){
				for(var j=0; j < fltRefList.length; j++){
					totalJourney += fltRefList[j].split('$')[1];
					totalJourney += ",";				
				}	
			}			
		}
		return totalJourney;
	} 
	
	this.applyONDStopOverInfo = function(modifyingOnd) {
		
		if(DATA_ResPro.systemParams.userDefineTransitTimeEnabled 
				&& modifyingOnd != undefined && modifyingOnd != null){
			 var fltObjArr = modifyingOnd.getFlights();	                
			 
			 if(fltObjArr != undefined && fltObjArr != null && fltObjArr.length > 1 ){
				 
				  var timeDiffInMillis = fltObjArr[fltObjArr.length-1].departureDTZuluMillis - fltObjArr[0].arrivalDTZuluMillis;
			         var stopOverDays = (timeDiffInMillis)/(1000*60*60*24);		         
			         
			         $("#outboundStopOver_0").val(Math.floor(stopOverDays));
			         UI_tabSearchFlights.applyStopOverTime()
			 }    
		}	   
       
	};
	
	this.getConfirmedExistingFlightSegments = function () {
		
		var ondList = getONDList();
		var confirmedSegments = [];
		var index = 0;
		for ( var i = 0; i < ondList.length; i++) {
			if(ondList[i].getStatus() == ondStatus.CNF){				
				var ondFlights = ondList[i].getFlights();
				if(ondFlights != null){
					for ( var j = 0; j < ondFlights.length; j++) {
						if(!ondFlights[j].busSegment) {
							confirmedSegments[index] = ondFlights[j];
							confirmedSegments[index]['ondSequence'] = i;
							index++;
						}
						
					}
				}
			}
		}
		
		return confirmedSegments;
	}

	this.getCCChargeableFltSegmentsCount = function(){
	    var ondList = getONDList();
        var insuarableSegments = [];
        var count = 0;
        for ( var i = 0; i < ondList.length; i++) {
            if(ondList[i].getStatus() != ondStatus.CNF){            
                count +=ondList[i].getFlightRPHList().length;
            }
        }
        return count;
	}
};

UI_requote = new UI_requote();
UI_requote.modifyingFltSegHash = {};
UI_requote.selectedSegmentCabinclass = "";
UI_requote.selectedSegment = "";
UI_requote.defaultCnxAdultCharge = "";
UI_requote.defaultCnxChildCharge = "";
UI_requote.defaultInfantCharge = "";
UI_requote.fromFlightSearch = false;
UI_requote.overlappingOND = false;
