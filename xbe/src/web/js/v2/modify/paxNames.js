
var jsonPaxAdtObj = {};
var paxAdults = {};
var paxInfants = {};
if (DATA_ResPro.mode == 'CREATE') {
	paxAdults = opener.UI_tabPassenger.getAdultDetails();
	paxInfants = opener.UI_tabPassenger.getInfantDetails();
}else if (DATA_ResPro.mode == 'MODIFY') {
	paxAdults = opener.UI_tabBookingInfo.getAdultDetails();
	paxInfants = opener.UI_tabBookingInfo.getInfantDetails();
}else{
	paxAdults = opener.jsonPaxAdtObj.paxAdults;
	paxInfants = opener.jsonPaxAdtObj.paxInfants;
}
var infCount = 0;
if(paxInfants != null){
	infCount = paxInfants.length;
}
var paxCount = paxAdults.length + infCount;
jQuery(document).ready(function(){
	$("#divPane").decoratePanel("Passenger Names In Other Languages");
	$("#divLanguageCode").decoratePanel("Language Code");
	$("#btnEdit").decorateButton();
	$("#btnSave").decorateButton();
	$("#btnCancel").decorateButton();
	
	$("#btnEdit").click(function(){
		enableDisableControls(false);
	});
	$("#btnCancel").click(function(){
		window.close();
	});
	$("#btnSave").click(function(){
		var count = paxAdults.length;
		var paxDetails = paxAdults;
		if($("#selLanguageCode").val() == null){
			$("#selLanguageCode").focus();
			$("#divHDPane1").html("<span style =\"color:red;\">TAIR-90176: Language Should be Specified</span>");
			return false;
		}
		$("#divHDPane1").html("");
		
		for(var i=0;i<count;i++){
			var rowNo =i+1;
			paxDetails[i].displayAdultFirstNameOl = $("#" + rowNo + "_displayAdultFirstNameOl").val();
			paxDetails[i].displayAdultLastNameOl =  $("#" + rowNo + "_displayAdultLastNameOl").val();
			paxDetails[i].displayAdultTitleOl =  $("#" + rowNo + "_displayAdultTitleOl").val();
			paxDetails[i].displayNameTranslationLanguage = $("#selLanguageCode").val();
	
			if(paxDetails[i].displayAdultFirstNameOl == "" && paxDetails[i].displayAdultLastNameOl == "" && paxDetails[i].displayAdultTitleOl == ""){
				$("#selLanguageCode").val('');
				paxDetails[i].displayNameTranslationLanguage = "";
			}
		
		}
		opener.jsonPaxAdtObj.paxAdults = paxDetails;
		
		var paxInfDetails = paxInfants;
		if(paxInfDetails != null){
			for(var i=0;i<infCount;i++){
				//paxAdults.length is because infant fields also have the adult id's. Should be fixed for proper naming format later.
				var rowNo = paxAdults.length+i+1;
				paxInfDetails[i].displayInfantFirstNameOl = $("#" + rowNo + "_displayAdultFirstNameOl").val();
				paxInfDetails[i].displayInfantLastNameOl =  $("#" + rowNo + "_displayAdultLastNameOl").val();
				paxInfDetails[i].displayInfantTitleOl =  $("#" + rowNo + "_displayAdultTitleOl").val();
				paxInfDetails[i].displayNameTranslationLanguage = $("#selLanguageCode").val();
			}
			opener.jsonPaxAdtObj.paxInfants = paxInfDetails;
		}		
		window.close();
	});
	$("#btnEdit").click(function(){	
		var count = paxAdults.length;
		var paxDetails = paxAdults;
		for(var i=0;i<count;i++){
			var rowNo =i+1;
			paxDetails[i].displayAdultFirstNameOl = $("#" + rowNo + "_displayAdultFirstNameOl").val();
			paxDetails[i].displayAdultLastNameOl =  $("#" + rowNo + "_displayAdultLastNameOl").val();
			paxDetails[i].displayAdultTitleOl =  $("#" + rowNo + "_displayAdultTitleOl").val();
			paxDetails[i].displayNameTranslationLanguage = $("#selLanguageCode").val();
			
			if(paxDetails[i].displayAdultFirstNameOl == "" && paxDetails[i].displayAdultLastNameOl == "" && paxDetails[i].displayAdultTitleOl == ""){
				$("#selLanguageCode").val('');
				paxDetails[i].displayNameTranslationLanguage = "";
			}
		}
	});
	
	constructGrid();
	fillGridData();
	fillDropDownData();	
	if(checkIfDataIsPresent()){
		enableDisableControls(true);
	}
});

function checkIfDataIsPresent()
{
	for(var i = 1;i <= paxCount;i++){
		var fName=$("#" + i + "_displayAdultFirstNameOl").val();
		var lName=$("#" + i + "_displayAdultLastNameOl").val();
		var title=$("#" + i + "_displayAdultTitleOl").val();
		if($("#tablPane1").getCell(i,'displayAdultType') == "BABY" && $("#tablPane1").getCell(i,'title') == ""){
			$("#" + i + "_displayAdultTitleOl").attr('disabled', true);
		}
		if(fName!='' || lName!='' || title!=''){
			return true;
		}
	}
	return false;
}

function enableDisableControls(mode){
	for(var rowNo = 1;rowNo <= paxCount;rowNo++){
		$("#" + rowNo + "_displayAdultFirstNameOl").attr('disabled', mode);
		$("#" + rowNo + "_displayAdultLastNameOl").attr('disabled', mode);
		if($("#tablPane1").getCell(rowNo,'displayAdultType') == "BABY" && $("#tablPane1").getCell(rowNo,'title') == ""){
			$("#" + rowNo + "_displayAdultTitleOl").attr('disabled', true);
		}else{
			$("#" + rowNo + "_displayAdultTitleOl").attr('disabled', mode);
		}
		
	}
	$("#selLanguageCode").attr('disabled',mode);
	$("#btnSave").attr('disabled', mode);
}

function fillDropDownData(){
	$("#selLanguageCode").fillDropDown({dataArray:top.jsonItnLang, keyIndex:"id", valueIndex:"desc", firstEmpty:false});
}

function fillGridData(){
	var paxDetails = new Array();
	var adultCount = 0;
	var infCount = 0;
	if(paxAdults != null){
		adultCount = paxAdults.length;
	}
	if(paxInfants != null){
		infCount = paxInfants.length;
	}	
	for(var i=0;i<adultCount;i++){
		paxDetails[i] = {};
		paxDetails[i].ppId = paxAdults[i].displayPnrPaxCatFOIDNumber;
		paxDetails[i].displayAdultType = paxAdults[i].displayAdultType;;
		paxDetails[i].title = paxAdults[i].displayAdultTitle;
		paxDetails[i].name = paxAdults[i].displayAdultFirstName + " " + paxAdults[i].displayAdultLastName;
		paxDetails[i].displayAdultFirstNameOl = paxAdults[i].displayAdultFirstNameOl;
		paxDetails[i].displayAdultLastNameOl = paxAdults[i].displayAdultLastNameOl;	
		paxDetails[i].displayAdultTitleOl = paxAdults[i].displayAdultTitleOl;
	}
	for(var i = 0;i<infCount;i++){
		paxDetails[i+adultCount] = {};
		paxDetails[i+adultCount].ppId = paxInfants[i].displayPnrPaxCatFOIDNumber;
		paxDetails[i+adultCount].displayAdultType = paxInfants[i].displayInfantType;
		paxDetails[i+adultCount].title = paxInfants[i].displayInfantTitle;
		paxDetails[i+adultCount].name = paxInfants[i].displayInfantFirstName + " " + paxInfants[i].displayInfantLastName;
		paxDetails[i+adultCount].displayAdultFirstNameOl = paxInfants[i].displayInfantFirstNameOl;
		paxDetails[i+adultCount].displayAdultLastNameOl = paxInfants[i].displayInfantLastNameOl;		
		paxDetails[i+adultCount].displayAdultTitleOl = paxInfants[i].displayInfantTitleOl;
		if($("#tablPane1").getCell(i+adultCount,'displayAdultType') == "BABY" && paxInfants[i].displayInfantTitleOl == ""){
			$("#" + rowNo + "_displayAdultTitleOl").attr('disabled', true);
		}
	}
	if(paxDetails.length > 0){
		$("#selLanguageCode").val(paxAdults[0].displayNameTranslationLanguage);	
	}	 
	jsonPaxAdtObj.paxAdults = paxDetails;
	UI_commonSystem.fillGridData({id:"#tablPane1",data:paxDetails});
}

function constructGrid(){
	jQuery("#tablPane1").jqGrid({
		dataType : "local",
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,					 
			  id: "0"				  
			},
		colNames : ['&nbsp',geti18nData('BookingInfo_PassengerInfo_lbl_Type','Type'),geti18nData('BookingInfo_PassengerInfo_lbl_Title','Title'),geti18nData('BookingInfo_PassengerInfo_lbl_Name','Name'),geti18nData('BookingInfo_PassengerInfo_lbl_Title','Title'),geti18nData('BookingInfo_PassengerInfo_lbl_FirstName','First Name'),geti18nData('BookingInfo_PassengerInfo_lbl_LastName','Last Name')],
		colModel : [{name:"id",width:5,jsonmap:"id",visible:false},
		            {name:"displayAdultType",width:10,jsonmap:"displayAdultType"},
		            {name:"title",width:10,jsonmap:"title"},
		            {name:"name",width:50,jsonmap:"name"},
		            {name:"displayAdultTitleOl",width:30,jsonmap:"displayAdultTitleOl",editable:true,
		            	editoptions:{className:"aa-input", maxlength:"30", style:"width:75px;"}},
		            {name:"displayAdultFirstNameOl",width:30,jsonmap:"displayAdultFirstNameOl",editable:true,
		            	editoptions:{className:"aa-input", maxlength:"30", style:"width:75px;"}},
		            {name:"displayAdultLastNameOl",width:30,jsonmap:"displayAdultLastNameOl",editable:true,
		            		editoptions:{className:"aa-input", maxlength:"30", style:"width:75px;"}
					}],
		viewRecords : true,
		height : 147,
		width : 570
	});
}