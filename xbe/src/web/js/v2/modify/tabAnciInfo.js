	/*
	*********************************************************
		Description		: XBE Make Reservation - Ancillary Tab Info

		Author			: Dilan Anuruddha
		Version			: 1.0
		Created			: 17th March 2010
		Last Modified	: 17th March 2010
	*********************************************************	
	*/
	
	/*
	 * Ancillary Tab
	 */

	var anciAvailability = {};
	function UI_tabAnciInfo(){}
	
	
	UI_tabAnciInfo.blnLoaded = false;
	UI_tabAnciInfo.blnDataLoaded = false;
	UI_tabAnciInfo.anciType = { SSR:'SSR',SEAT:'SEAT',MEAL:'MEAL',HALA:'HALA',INSURANCE:'INSURANCE',BAGGAGE:'BAGGAGE',APSERVICE:'APSERVICE', APTRANSFER:'AIRPORT_TRANSFER',AUTOMATICCHECKIN:"AUTOMATIC_CHECKIN"};
	UI_tabAnciInfo.calledAvailSearch = false;
	/*
	 * Ancillary Tab Page On Load
	 */
	UI_tabAnciInfo.ready = function(){
		
		if(!UI_tabAnciInfo.blnLoaded) {
			$("#btnAnciEdit").decorateButton();
			$("#btnAnciEdit").attr('style', 'width:auto;');
			//$("#btnAnciEdit").disable();
			$("#btnAnciEdit").click(function() {UI_tabAnciInfo.editAncillary();});
			//$("#btnAnciEdit").hide(); /* TODO remove when the edit ancillary is ready */
			
			UI_tabAnciInfo.constructSeatGrid({id:"#tblSeats"});
			UI_tabAnciInfo.constructMealGrid({id:"#tblMeals"});
			UI_tabAnciInfo.constructBaggageGrid({id:"#tblBaggages"});
			UI_tabAnciInfo.constructInsuranceGrid({id:"#tblInsurance"});
			UI_tabAnciInfo.constructSSRGrid({id:"#tblSSR"});
			UI_tabAnciInfo.constructAPServiceGrid({id:"#tblAPService"});
			UI_tabAnciInfo.constructAPTransferGrid({id: "#tblAPTransfer"});
			UI_tabAnciInfo.constructAutoCheckinGrid({id: "#tblAutoCheckin"});
			UI_tabAnciInfo.blnLoaded = true;
		}		
		if(!UI_tabAnciInfo.blnDataLoaded){
			UI_tabAnciInfo.blnDataLoaded = true;
			UI_tabAnciInfo.buildData();
		}		
		if (isGdsPnr && !isGdsAllowAncillary) {
			$("#btnAnciEdit").hide();
		}
		UI_tabAnciInfo.onLoadCall();
	}
	
	UI_tabAnciInfo.resetData = function(){
		UI_tabAnciInfo.blnDataLoaded = false;
	}
	
	UI_tabAnciInfo.buildData = function(){
		var retObj = UI_tabAnciInfo.processPaxData(jsonPaxWiseAnci);
		jsonPaxWiseAnci = retObj.paxList;
		anciAvailability = retObj.availability;
	}
	/*
	 * On Load Call
	 */
	UI_tabAnciInfo.onLoadCall = function(){
		UI_tabAnciInfo.fillBookingInfo();
	
		
		if(anciAvailability.ssr){
			$('#trAnciSSR').show();
			UI_commonSystem.fillGridData({id:"#tblSSR", data:UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.SSR), editable:false});
		}else{
			$('#trAnciSSR').hide();
		}
		if(anciAvailability.meal){
			$('#trAnciMeal').show();
			UI_commonSystem.fillGridData({id:"#tblMeals", data:UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.MEAL), editable:false});
		}else{
			$('#trAnciMeal').hide();
		}
		if(anciAvailability.baggage){
			$('#trAnciBaggage').show();
			UI_commonSystem.fillGridData({id:"#tblBaggages", data:UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.BAGGAGE), editable:false});
		}else{
			$('#trAnciBaggage').hide();
		}
		if(anciAvailability.seat){
			$('#trAnciSeat').show();
			UI_commonSystem.fillGridData({id:"#tblSeats", data:UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.SEAT), editable:false});
		}else{
			$('#trAnciSeat').hide();
		}
		if(anciAvailability.aps){
			$("#trAnciAPService").show();
			UI_commonSystem.fillGridData({id:"#tblAPService", data:UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.APSERVICE), editable:false});
		} else {
			$("#trAnciAPService").hide();
		}
		if(anciAvailability.apt){
			$("#trAnciAPTransfer").show();
			UI_commonSystem.fillGridData({id:"#tblAPTransfer", data:UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.APTRANSFER), editable:false});
		} else {
			$("#trAnciAPTransfer").hide();
		}
		if(anciAvailability.autoCheckin){
			$("#trAnciAutoCheckin").show();
			UI_commonSystem.fillGridData({id:"#tblAutoCheckin", data:UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.AUTOMATICCHECKIN), editable:false});
		} else {
			$("#trAnciAutoCheckin").hide();
		}
		
		
		if (jsonInsurances != null && jsonInsurances.length > 0) {
           var insuranceToDisplay = UI_tabAnciInfo.getInsurancesToDisplay();
			$('#trAnciInsurance').show();			
			UI_commonSystem.fillGridData({id:"#tblInsurance", data:[insuranceToDisplay], editable:false});
		} else {
		    $('#trAnciInsurance').hide();
		}
	}
	
	UI_tabAnciInfo.getInsurancesToDisplay = function () {
		var insuranceToDisplay = null;
		var insTotal = { CNF: 0, PEN: 0}
		var firstCNFPolicy = -1;
		
		for (var i=0; i < jsonInsurances.length; i++){ 
			var policyCode = jsonInsurances[i].policyCode;
			if( policyCode != null && policyCode != ''){
				insTotal.CNF  += parseFloat(jsonInsurances[i].quotedTotalPremium);
				if( firstCNFPolicy == -1){
					firstCNFPolicy = i;
				}	
			}else{
				insTotal.PEN  += parseFloat(jsonInsurances[i].quotedTotalPremium);
			}
		}
		
		if ( firstCNFPolicy ==  -1){
			insuranceToDisplay = AIR_Util.dom.cloneObject(jsonInsurances[0]);
			insuranceToDisplay.total = insTotal.PEN;
		}else{
			insuranceToDisplay =  AIR_Util.dom.cloneObject(jsonInsurances[firstCNFPolicy]);
			insuranceToDisplay.total = insTotal.CNF;
		}
		
		if(insuranceToDisplay.dateOfReturn!=null && insuranceToDisplay.dateOfTravel){
			var formatDate = UI_commonSystem.dateFormatter('accelaero');
			insuranceToDisplay.dateOfReturn = formatDate(UI_tabAnciInfo._getDate(insuranceToDisplay.dateOfReturn));
			insuranceToDisplay.dateOfTravel = formatDate(UI_tabAnciInfo._getDate(insuranceToDisplay.dateOfTravel));
		}
		if(insuranceToDisplay.policyCode ==null){
			insuranceToDisplay.policyCode = '';
			insuranceToDisplay.state = '<font color="red">'+insuranceToDisplay.state+'<\/font>'; 
		}
		return insuranceToDisplay;
	}
	
	UI_tabAnciInfo._getDate = function (dstr){
		if(dstr==null)	return null;
		var a = [];
		if(dstr.indexOf(' ') < 0){
			a = dstr.split('T');
		}else{
			dstr = dstr.substr(0,dstr.length-6);
			a = dstr.split(' ');
		}
		var d = a[0].split('-');
		var t = a[1].split(':');
		return new Date(d[0],parseInt(d[1],10)-1,d[2],t[0],t[1],t[2]);
	}
	
	UI_tabAnciInfo.getSegInfo = function(fltRefNo){
		for(var i = 0 ; i < jsonFltDetails.length ; i++){
			if(jsonFltDetails[i].flightRefNumber == fltRefNo && jsonFltDetails[i].status != 'CNX'){
				return jsonFltDetails[i];
			}
		}
	}
	
	UI_tabAnciInfo.processPaxData = function(paxList){
		var paxRetList = [];
		var availability = {seat:false,meal:false,ssr:false,baggage:false,aps:false,apt:false,autoCheckin:false};
		var dateFormatter = UI_commonSystem.dateFormatter('accelaero');
		for(var i = 0 ; i < paxList.length; i++){			
			
			
			var selectedAncillaries = paxList[i].selectedAncillaries;
			if(selectedAncillaries == null){				
				continue;				
			}
			for(var k=0 ; k < selectedAncillaries.length ; k++){
				var selectedAnci = selectedAncillaries[k];
				var pax =AIR_Util.dom.cloneObject(paxList[i]);
				pax.ssrDesc = '';
				pax.ssrCharge = '';
				pax.mealDesc = '';
				pax.mealCharge = '';
				pax.seatNumber = '';
				pax.seatCharge = '';
				pax.segCode = '';
				pax.baggageDesc = '';
				pax.baggageCharge = '';
				pax.paxName = pax.firstName + ' '+ pax.lastName;
				pax.seatAutoCnx = '';
				pax.mealAutoCnx = '';
				pax.baggageAutoCnx = '';
				pax.ssrAutoCnx = '';
				pax.apsAutoCnx = '';
				pax.seatPreference = '';
				pax.autoCheckinCharge = '';
				pax.email = '';
				pax.insuranceAutoCnx = '';
				var fltSegs = selectedAnci.flightSegmentTO;
				var ssrs  = selectedAnci.specialServiceRequestDTOs;
				var meals = selectedAnci.mealDTOs;
				var seat = selectedAnci.airSeatDTO;
				var baggages = selectedAnci.baggageDTOs;
				var apServices = selectedAnci.airportServiceDTOs;
				var apTransfers = selectedAnci.airportTransferDTOs; 
				var extraSeats = selectedAnci.extraSeatDTOs;
				var autoCheckins = selectedAnci.automaticCheckinDTOs;
				
				if(fltSegs!=null){
					var segInfo = UI_tabAnciInfo.getSegInfo(fltSegs.flightRefNumber);
					if(segInfo!=undefined && segInfo!=null){
						var d = UI_tabAnciInfo.getDateObj(segInfo.departureDate, segInfo.departureTime);
						pax.segCode = fltSegs.segmentCode;
						pax.flightNumber = segInfo.flightNo;
						pax.departureDate = dateFormatter(d);
						pax.arrivalDate = dateFormatter(UI_tabAnciInfo.getDateObj(segInfo.arrivalDate, segInfo.arrivalTime));
						pax.dateObj = d;
					}
				}
				
				if(ssrs!=null){
					var ssrDesc = '';
					var ssrCharge = 0;
					var autoCnxSSRs = '';
					for(var j = 0; j < ssrs.length; j++){
						if(j>0) ssrDesc+=', ';
						ssrDesc += ssrs[j].description ;
						if(ssrs[j].text!=null)
							ssrDesc += ' - ' + ssrs[j].text;
						
						if(ssrs[j].userDefinedCharge!=null && ssrs[j].userDefinedCharge){
							if (ssrs[j].showSsrValue) {
								ssrDesc += ' [' + ssrs[j].ssrCode +','+ DATA_ResPro.initialParams.baseCurrencyCode+' '+ ssrs[j].charge + '] ';
							} else {
								ssrDesc += ' [' + ssrs[j].ssrCode +'] ';
							}
							if(fltSegs!=null && (pax.flightNumber==undefined || pax.flightNumber==null)){
								UI_tabAnciInfo.updateCancelledSegmentInfo(pax,fltSegs);
							}							
						}
						
						ssrCharge += parseFloat(ssrs[j].charge);
						availability.ssr= true;
						if(ssrs[j].alertAutoCancellation){
							if(autoCnxSSRs != ''){autoCnxSSRs += ", "}
							autoCnxSSRs += ssrs[j].description;
						}
					}
					pax.ssrDesc = ssrDesc;
					pax.ssrCharge = ssrCharge;
					pax.ssrAutoCnx = autoCnxSSRs;
				}
				if(meals!=null){
					var mealDesc = '';
					var mealCharge = 0;
					var autoCnxMeals = '';
					for(var j = 0; j < meals.length; j++){
						if(j>0) mealDesc+=', ';
						mealDesc += meals[j].soldMeals + 'x ' +  meals[j].mealName ;
						mealCharge += parseFloat(meals[j].mealCharge);
						availability.meal= true;
						if(meals[j].alertAutoCancellation){
							if(autoCnxMeals != ''){autoCnxMeals += ", "}
							autoCnxMeals += meals[j].mealName;
						}
					}
					pax.mealDesc = mealDesc;
					pax.mealCharge = mealCharge;
					pax.mealAutoCnx = autoCnxMeals;
				}
				
				if(baggages!=null){
					var baggageDesc = '';
					var baggageCharge = 0;
					var autoCnxBgs = '';
					for(var j = 0; j < baggages.length; j++){
						if(j>0) baggageDesc+=', ';
						baggageDesc += baggages[j].baggageName ;
						baggageCharge += parseFloat(baggages[j].baggageCharge);
						availability.baggage= true;
						if(baggages[j].alertAutoCancellation){
							if(autoCnxBgs != ''){autoCnxBgs += ", "}
							autoCnxBgs += baggages[j].baggageName;
						}
					}
					pax.baggageDesc = baggageDesc;
					pax.baggageCharge = baggageCharge;
					pax.baggageAutoCnx = autoCnxBgs;
					
				}
				if(seat!=null && seat.seatNumber!=null && seat.seatNumber != ''){
					pax.seatNumber = seat.seatNumber;
					pax.seatCharge = seat.seatCharge;
					if(seat.alertAutoCancellation){
						pax.seatAutoCnx = seat.seatNumber;
					}
					availability.seat = true;
				} 
				if(extraSeats != null){
					for(var x=0;x<extraSeats.length;x++){
						if(extraSeats[x] != null && extraSeats[x] != ''){
							pax.seatCharge += extraSeats[x].seatCharge;
							pax.seatNumber += ' ' + extraSeats[x].seatNumber; 
						}
					}
				}
				if(apServices != null){
					var ssrDesc = '';
					var ssrCharge = 0;
					var autoCnxAPSs = '';
					for(var j = 0; j < apServices.length; j++){
						if(j>0) ssrDesc+=', ';
						ssrDesc += apServices[j].ssrName + '-' + apServices[j].airportCode;
						ssrCharge += parseFloat(apServices[j].serviceCharge);
						availability.aps= true;
						pax.airportCode = apServices[j].airportCode;
						if(apServices[j].alertAutoCancellation){
							if(autoCnxAPSs != ''){autoCnxAPSs += ", "}
							autoCnxAPSs += apServices[j].ssrName;
						}
					}
					pax.apServiceDesc = ssrDesc;
					pax.apsCharge = ssrCharge;
					pax.apsAutoCnx = autoCnxAPSs;
				}
				
				if(apTransfers != null){
					var aptDesc = '';
					var airports = '';
					var aptCharge = 0;
					for(var j = 0; j < apTransfers.length; j++){
						if(j>0) {
							aptDesc+=', ';
						}
						
						aptDesc += apTransfers[j].tranferDate + ' - ' + apTransfers[j].transferAddress;
						//aptDesc += apTransfers[j].airportCode + '-' + apTransfers[j].ssrName;
						aptCharge += parseFloat(apTransfers[j].serviceCharge);
						availability.apt= true;
						pax.airportCode = apTransfers[j].airportCode;
					}
					pax.apTransferDesc = aptDesc;
					pax.aptCharge = aptCharge;
				 }
				
				if(autoCheckins!=null){
					var automaticCheckinCharge = 0;
					var seatPreference = '';
					var autoCheckinId =0;
					var autoCnxAutoCheckins = '';
					var email='';
					for(var j = 0; j < autoCheckins.length; j++){
						if(pax.seatNumber != '' && pax.seatNumber != null){
							seatPreference = pax.seatNumber;
						}else {
							seatPreference = autoCheckins[j].seatPref;
						}
						email = autoCheckins[j].email;
						automaticCheckinCharge	=parseFloat(autoCheckins[j].automaticCheckinCharge);
						availability.autoCheckin= true;
					}
					pax.seatPreference = seatPreference;
					pax.autoCheckinCharge = automaticCheckinCharge;
					pax.email = email;
				}
				
				paxRetList[paxRetList.length] = pax;
			}
			
		}
		paxRetList = AIR_Util.sort.quickSort(paxRetList,AIR_Util.sort.anci.segmentByDateComparator);
		return {paxList:paxRetList,availability:availability};
	}
	


	UI_tabAnciInfo.getDateObj = function(strDate, strTime) {

	if (strDate.indexOf(" ") > 0) {
		// Mon 25/08/2014
		strDate = strDate.split(" ")[1];
	}

	var a = strDate.split('/');
	// var b = strTime.replace('*','').split(':');
	// * has been replaced with (+1) under AARESAA-6877
	var b = strTime.replace('(+1)', '').split(':');
	var d = new Date(a[2], parseInt(a[1], 10) - 1, a[0], b[0], b[1], 0);

	return d;
	}
	
	UI_tabAnciInfo.getDataForAnci = function(paxList,anciType){
		var paxRetList = [];
		for(var i = 0; i < paxList.length ;i++){
			if(UI_tabAnciInfo.anciType.SSR != anciType && paxList[i].paxType == 'IN'){
				continue;
			}
			if(anciType == UI_tabAnciInfo.anciType.SSR && paxList[i].ssrDesc == ''){
				continue;
			}
			if(anciType == UI_tabAnciInfo.anciType.MEAL && paxList[i].mealDesc == ''){
				continue;
			}
			if(anciType == UI_tabAnciInfo.anciType.BAGGAGE && paxList[i].baggageDesc == ''){
				continue;
			}
			if(anciType == UI_tabAnciInfo.anciType.SEAT && paxList[i].seatNumber == ''){
				continue;
			}
			if(anciType != UI_tabAnciInfo.anciType.APSERVICE && paxList[i].paxType == 'IN'){
				continue;
			}
			if(anciType == UI_tabAnciInfo.anciType.APSERVICE && paxList[i].apServiceDesc == ''){
				continue;
			}
			if(anciType == UI_tabAnciInfo.anciType.APTRANSFER && paxList[i].apTransferDesc == ''){
				continue;
			}
			if(anciType == UI_tabAnciInfo.anciType.AUTOMATICCHECKIN && paxList[i].seatPreference == ''){
				continue;
			}
			paxRetList[paxRetList.length]=paxList[i];
		}

		return paxRetList;
	}
	
	UI_tabAnciInfo.removeDplFltSegs = function(flts){
		var dup = {};
		var arr = [];
		for(var i = 0 ; i < flts.length; i++){
			if(dup[flts[i].flightSegmentRefNumber]== null){
				arr[arr.length] = flts[i];
				dup[flts[i].flightSegmentRefNumber] = true;
			}
		}
		return arr;
	}
	
	
	UI_tabAnciInfo.editAncillary = function(){
		UI_commonSystem.pageOnChange();		
		var data = {};
		data['modifyAncillary'] = true;
		var jssegObj = $.parseJSON(UI_reservation.jsonSegs);
		var fltSegList = [];
		var isOpenRTReservation = false;
		for(var i = 0; i< jssegObj.length;i++){
			if(jssegObj[i].status != 'CNX' &&
					jssegObj[i].bookingType != "OPENRT"){
				fltSegList[fltSegList.length] =  jssegObj[i];
			}
			if(jssegObj[i].status != 'CNX' &&
					jssegObj[i].bookingType == "OPENRT"){
				isOpenRTReservation = true;
			}
		}
		fltSegList = UI_tabAnciInfo.removeDplFltSegs(fltSegList);
		if(fltSegList.length ==0){
			alert('Sorry. Ancillaries are not editable for this flight');
			return;
		}
		var hasInsurance = UI_tabBookingInfo.hasInsurance();
		data['insuranceAvailable'] = hasInsurance;
		
		data['flightRPHList'] = $.toJSON(fltSegList);
		data['mulipleMealsSelectionEnabled'] = DATA_ResPro.initialParams.multiMealEnabled;
		data['paxWiseAnci'] = $.toJSON(jsonPaxWiseAnciForMod);
		data['gdsPnr'] = isGdsPnr;
		data['pnr'] = jsonPNR;
		data['jsonOnds'] = UI_reservation.jsonONDs;
		data['isOpenReturnReservation'] = isOpenRTReservation;
		
		if(UI_tabAnciInfo.calledAvailSearch){
			alert('Please wait. Page is loading..');
		}
		UI_tabAnciInfo.calledAvailSearch = true;
		UI_commonSystem.getDummyFrm().ajaxSubmit({url:'ancillaryAvailability.action', dataType: 'json', processData: false, data:data, success: function (response){
			if(response.success){
				UI_tabAnciInfo.calledAvailSearch = false;
				if(!UI_tabAnciInfo.ancillaryAvailable(response.availabilityOutDTO)){
					alert('Sorry, ancillaries are not available');
					return;
				}
				
				$('#pnrNo').val(jsonPNR);
				$('#groupPNRNo').val(jsonGroupPNR);
				$('#anciAvail').val($.toJSON(response));
				$('#version').val(jsonBkgInfo.version);
				$('#resSegments').val($.toJSON(fltSegList));
				$('#resPax').val(UI_reservation.jsonPassengers);	
				$('#resPaxWisePayments').val($.toJSON(jsonPaxPriceTo));
				$('#resNoOfAdults').val(UI_reservation.adultCount);
				$('#resNoOfchildren').val(UI_reservation.childCount);
				$('#resNoOfInfants').val(UI_reservation.infantCount);
				$('#resOwnerAgent').val(UI_reservation.ownerAgent);
				$('#resContactInfo').val(UI_reservation.contactInfo);
				$('#resPaySummary').val($.toJSON(jsonResPaymentSummary));
				$("#jsonOnds").val(UI_reservation.jsonONDs);
				$('#pgMode').val("modifyAncillary");
				$("#frmModiBkg").action("modifyAncillary.action");	
				$("#frmModiBkg").submit();
			}else{
				//TODO fix the correct error message
				showERRMessage(raiseError("XBE-ERR-00",response.message));
			}
		}, error:UI_commonSystem.setErrorStatus
		});
	}
	
	UI_tabAnciInfo.ancillaryAvailable = function(anciAvailDTO){
		for(var i = 0 ; i < anciAvailDTO.length; i++){
			for(var j = 0 ; j < anciAvailDTO[i].ancillaryStatusList.length; j++){
				if(anciAvailDTO[i].ancillaryStatusList[j].available){
					return true;
				}
			}
		}
		return false;
	}
	/*
	 * Fill Booking Info
	 */
	UI_tabAnciInfo.fillBookingInfo = function(){
		if(jsonBkgInfo.PNR == ""){
			showERRMessage("System Error ! Please Contact System Administrator.");
		}else {
			$("#divAnciPnrNo").html(jsonBkgInfo.PNR);			
			var status = jsonBkgInfo.displayStatus;
			$("#divAnciBkgStatus").html(status);
			$("#divAnciAgent").html(jsonBkgInfo.agent);
		}
		
	}
	

	/*
	 * Grid Constructor for Seat
	 */
	UI_tabAnciInfo.constructSeatGrid = function(inParams){
		var strDOBMand = "";
		
		autoCnxFormatter = function(index, options, rowObject){
			if(rowObject.seatAutoCnx != ''){				
				var imageID = "SeatImg_" + (options.rowId - 1);
				cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
				return cnxAlert;
			} else {
				return '';
			}
		}
		
		seatPopup = function(){
			$.each(UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.SEAT), function(key, item) {
				if(item.seatAutoCnx != ''){
					var imageID = "#SeatImg_" + key;
					UI_tabBookingInfo.autoCnxAlertHover(imageID);
				}
			});
		}
		
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'),geti18nData('Ancillary_lbl_Segment', 'Segment'),geti18nData('Ancillary_lbl_FlightNo', 'Flight No'),geti18nData('Ancillary_lbl_DepartureDate', 'Departure Date'), geti18nData('FindRes_Ancillary_lbl_SeatNumber', 'Seat Number'), geti18nData('BookingInfo_FlightDetails_lbl_Charge', 'Charge'), ''],
			colModel:[
			    {name:'paxName', index:'paxName', width:100, align:"left"},
			    {name:'segCode', index:'segCode', width:50, align:"center"},
			    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
			    {name:'departureDate', index:'departureDate', width:50, align:"center"},
			    {name:'seatNumber', index:'seatNumber', width:50, align:"center"},
			    {name:'seatCharge', index:'seatCharge', width:50, align:"center"},
			    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
			],
			sortname: 'paxSequence',
			sortOrder: "asc",
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true,
			gridComplete: seatPopup
		});
	}
	
	/*
	 * Grid Constructor for Meal
	 */
	UI_tabAnciInfo.constructMealGrid = function(inParams){
		var strDOBMand = "";
		
		autoCnxFormatter = function(index, options, rowObject){
			if(rowObject.mealAutoCnx != ''){				
				var imageID = "MealImg_" + (options.rowId - 1);
				cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
				return cnxAlert;
			} else {
				return '';
			}
		}
		
		mealPopup = function(){
			$.each(UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.MEAL), function(key, item) {
				if(item.mealAutoCnx != ''){
					var imageID = "#MealImg_" + key;
					UI_tabBookingInfo.autoCnxAlertHover(imageID);
				}
			});
		}
		
		if(focMealEnabled == false) {
			$(inParams.id).jqGrid({
				height: 100,
				width: 910,
				colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'),geti18nData('Ancillary_lbl_Segment', 'Segment'),geti18nData('Ancillary_lbl_FlightNo', 'Flight No'),geti18nData('Ancillary_lbl_DepartureDate', 'Departure Date'), geti18nData('Ancillary_lbl_Meal','Meal(s)'), geti18nData('BookingInfo_FlightDetails_lbl_Charge', 'Charge'), ''],
				colModel:[
				    {name:'paxName', index:'paxName', width:100, align:"left"},
				    {name:'segCode', index:'segCode', width:50, align:"center"},
				    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
				    {name:'departureDate', index:'departureDate', width:50, align:"center"},
				    {name:'mealDesc', index:'mealDesc', width:100, align:"center"},
				    {name:'mealCharge', index:'mealCharge', width:50, align:"center"},
				    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
				],
				sortname: 'paxSequence',
				sortOrder: "asc",
				imgpath: UI_commonSystem.strImagePath,
				multiselect: false,
				datatype: "local",
				viewrecords: true,
				gridComplete: mealPopup
			});
		} else {
			$(inParams.id).jqGrid({
				height: 100,
				width: 910,
				colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'),geti18nData('Ancillary_lbl_Segment', 'Segment'),geti18nData('Ancillary_lbl_FlightNo', 'Flight No'),geti18nData('Ancillary_lbl_DepartureDate', 'Departure Date'), geti18nData('Ancillary_lbl_Meal','Meal(s)'), ''],
				colModel:[
				    {name:'paxName', index:'paxName', width:100, align:"left"},
				    {name:'segCode', index:'segCode', width:50, align:"center"},
				    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
				    {name:'departureDate', index:'departureDate', width:50, align:"center"},
				    {name:'mealDesc', index:'mealDesc', width:100, align:"center"},
				    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
				],
				sortname: 'paxSequence',
				sortOrder: "asc",
				imgpath: UI_commonSystem.strImagePath,
				multiselect: false,
				datatype: "local",
				viewrecords: true,
				gridComplete: mealPopup
			});
		}
	}
	
	
	
	/*
	 * Grid Constructor for Baggage
	 */
	UI_tabAnciInfo.constructBaggageGrid = function(inParams){
		var strDOBMand = "";
		
		autoCnxFormatter = function(index, options, rowObject){
			if(rowObject.baggageAutoCnx != ''){				
				var imageID = "BaggageImg_" + (options.rowId - 1);
				cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
				return cnxAlert;
			} else {
				return '';
			}
		}
		
		baggagePopup = function(){
			$.each(UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.BAGGAGE), function(key, item) {
				if(item.baggageAutoCnx != ''){
					var imageID = "#BaggageImg_" + key;
					UI_tabBookingInfo.autoCnxAlertHover(imageID);
				}
			});
		}
		
		if(focMealEnabled == false) {
			$(inParams.id).jqGrid({
				height: 100,
				width: 910,
				colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'),geti18nData('Ancillary_lbl_Segment', 'Segment'),geti18nData('Ancillary_lbl_FlightNo', 'Flight No'),geti18nData('Ancillary_lbl_DepartureDate', 'Departure Date'), geti18nData('Ancillary_lbl_Baggages','Baggage(s)'), geti18nData('BookingInfo_FlightDetails_lbl_Charge', 'Charge'), ''],
				colModel:[
				    {name:'paxName', index:'paxName', width:100, align:"left"},
				    {name:'segCode', index:'segCode', width:50, align:"center"},
				    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
				    {name:'departureDate', index:'departureDate', width:50, align:"center"},
				    {name:'baggageDesc', index:'baggageDesc', width:100, align:"center"},
				    {name:'baggageCharge', index:'baggageCharge', width:50, align:"center"},
				    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
				],
				sortname: 'paxSequence',
				sortOrder: "asc",
				imgpath: UI_commonSystem.strImagePath,
				multiselect: false,
				datatype: "local",
				viewrecords: true,
				gridComplete: baggagePopup
			});
		} else {
			$(inParams.id).jqGrid({
				height: 100,
				width: 910,
				colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'),geti18nData('Ancillary_lbl_Segment', 'Segment'),geti18nData('Ancillary_lbl_FlightNo', 'Flight No'),geti18nData('Ancillary_lbl_DepartureDate', 'Departure Date'), geti18nData('Ancillary_lbl_Baggages','Baggage(s)'), ''],
				colModel:[
				    {name:'paxName', index:'paxName', width:100, align:"left"},
				    {name:'segCode', index:'segCode', width:50, align:"center"},
				    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
				    {name:'departureDate', index:'departureDate', width:50, align:"center"},
				    {name:'baggageDesc', index:'baggageDesc', width:100, align:"center"},
				    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
				],
				sortname: 'paxSequence',
				sortOrder: "asc",
				imgpath: UI_commonSystem.strImagePath,
				multiselect: false,
				datatype: "local",
				viewrecords: true,
				gridComplete: baggagePopup
			});
		}
	}
	
	
	
	
	
	/*
	 * Grid Constructor for Insurance
	 */
	UI_tabAnciInfo.constructInsuranceGrid = function(inParams){
		var strDOBMand = "";
		
		autoCnxFormatter = function(index, options, rowObject){
			if(rowObject.alertAutoCancellation){				
				var imageID = "InsImg_" + (options.rowId - 1);
				cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
				return cnxAlert;
			} else {
				return '';
			}
		}
		
		insurancePopup = function(){
			$.each([jsonInsurances], function(key, item) {
				if(item.alertAutoCancellation){
					var imageID = "#InsImg_" + key;
					UI_tabBookingInfo.autoCnxAlertHover(imageID);
				}
			});
		}
		
		$(inParams.id).jqGrid({
			height: 30,
			width: 910,
			colNames:[ geti18nData('Ancillary_lbl_PolicyCode','Policy Code'), geti18nData('Ancillary_lbl_Amount','Amount'), geti18nData('Ancillary_lbl_Origin','Origin'),geti18nData('Ancillary_lbl_Destination','Destination'), geti18nData('Ancillary_lbl_DepartureDate','Departure Date'), geti18nData('Ancillary_lbl_ArrivalDate','Arrival Date'),geti18nData('Ancillary_lbl_State','State'), ''],
			colModel:[
			    {name:'policyCode', index:'policyCode', width:100, align:"left"},
			    {name:'total', index:'total', width:50, align:"center"},
			    {name:'origin', index:'origin', width:50, align:"center"},
			    {name:'destination', index:'destination', width:50, align:"center"},
			    {name:'dateOfTravel', index:'dateOfTravel', width:50, align:"center"},
			    {name:'dateOfReturn', index:'dateOfReturn', width:50, align:"center"},
			    {name:'state', index:'state', width:50, align:"center"},
			    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true,
			gridComplete: insurancePopup
		});
	}
	
	/*
	 * Grid Constructor for SSR
	 */
	UI_tabAnciInfo.constructSSRGrid = function(inParams){
		var strDOBMand = "";
		
		autoCnxFormatter = function(index, options, rowObject){
			if(rowObject.ssrAutoCnx != ''){				
				var imageID = "SSRImg_" + (options.rowId - 1);
				cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';				
				return cnxAlert;
			} else {
				return '';
			}
		}
		
		ssrPopup = function(){
			$.each(UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.SSR), function(key, item) {
				if(item.ssrAutoCnx != ''){
					var imageID = "#SSRImg_" + key;
					UI_tabBookingInfo.autoCnxAlertHover(imageID);
				}
			});
		}
		
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'), geti18nData('Ancillary_lbl_Segment','Segment'), geti18nData('Ancillary_lbl_FlightNo','Flight No'), geti18nData('Ancillary_lbl_DepartureDate','Departure Date'), geti18nData('Ancillary_lbl_SpecialServiceRequestDescription','SSR Description'), ''/* 'SSR Comment', 'SSR Charge'*/],
			colModel:[
			    {name:'paxName', index:'paxName', width:100, align:"left"},
			    {name:'segCode', index:'segCode', width:50, align:"center"},
			    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
			    {name:'departureDate', index:'departureDate', width:50, align:"center"},
			    {name:'ssrDesc', index:'ssrDesc', width:100, align:"center"},
			    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
			    /*,
			    {name:'ssrComment', index:'ssrComment', width:100, align:"center"},
			    {name:'ssrCharge', index:'ssrCharge', width:50, align:"center"}*/
			],
			sortname: 'paxSequence',
			sortOrder: "asc",
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true,
			gridComplete: ssrPopup
		});
	}
	
	/*
	 * Grid Constructor for Airport Services
	 */
	UI_tabAnciInfo.constructAPServiceGrid = function(inParams){
		var strDOBMand = "";
		
		autoCnxFormatter = function(index, options, rowObject){
			if(rowObject.apsAutoCnx != ''){				
				var imageID = "APSImg_" + (options.rowId - 1);
				cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
				return cnxAlert;
			} else {
				return '';
			}
		}
		
		apsPopup = function(){
			$.each(UI_tabAnciInfo.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciInfo.anciType.APSERVICE), function(key, item) {
				if(item.apsAutoCnx != ''){
					var imageID = "#APSImg_" + key;
					UI_tabBookingInfo.autoCnxAlertHover(imageID);
				}
			});
		}
		
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'),geti18nData('Ancillary_lbl_Segment','Segment'),geti18nData('Ancillary_lbl_DepartureDate','Departure Date'), geti18nData('Ancillary_lbl_ArrivalDate','Arrival Date'), geti18nData('Ancillary_lbl_SpecialServiceRequestDescription','SSR Description'), ''],
			colModel:[
			    {name:'paxName', index:'paxName', width:100, align:"left"},
				{name:'segCode', index:'segCode', width:50, align:"center"},
			    {name:'departureDate', index:'departureDate', width:50, align:"center"},
				{name:'arrivalDate', index:'arrivalDate', width:50, align:"center"},
			    {name:'apServiceDesc', index:'apServiceDesc', width:100, align:"center"},
			    {name:'alertAutoCancellation', index: 'alertAutoCancellation', width: 30, align: 'center', formatter: autoCnxFormatter}
			],
			sortname: 'paxSequence',
			sortOrder: "asc",
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true,
			gridComplete: apsPopup
		});
	}
	
	
	UI_tabAnciInfo.constructAPTransferGrid = function(inParams){
		var strDOBMand = "";
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'),geti18nData('Ancillary_lbl_Segment','Segment'), geti18nData('Ancillary_lbl_Destination','Description'), geti18nData('Ancillary_lbl_Charge','Charge')],
			colModel:[
			    {name:'paxName',       index:'paxName', width:100, align:"left"},
				{name:'segCode',       index:'segCode', width:50, align:"center"},
				{name:'apTransferDesc',index:'apTransferDesc', width:170, align:"left"},
				{name:'aptCharge',     index:'aptCharge', width:100, align:"left"}
			],
			sortname: 'paxSequence',
			sortOrder: "asc",
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
			//gridComplete: apsPopup
		});
	}
	
	UI_tabAnciInfo.constructAutoCheckinGrid = function(inParams){
		var strDOBMand = "";

		
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[ geti18nData('Ancillary_lbl_PassengerName','Passenger Name'), geti18nData('Ancillary_lbl_Segment','Segment'), geti18nData('Ancillary_lbl_FlightNo','Flight No'), geti18nData('Ancillary_lbl_DepartureDate','Departure Date'), geti18nData('Ancillary_lbl_SeatPreference','Seat Preference'),geti18nData('Ancillary_lbl_Charge','Charge'),geti18nData('Ancillary_lbl_Email','Email')],
			colModel:[
			    {name:'paxName', index:'paxName', width:100, align:"left"},
			    {name:'segCode', index:'segCode', width:50, align:"center"},
			    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
			    {name:'departureDate', index:'departureDate', width:50, align:"center"},
			    {name:'seatPreference', index:'seatPreference', width:100, align:"center"},
				{name:'autoCheckinCharge',     index:'autoCheckinCharge', width:100, align:"left"},
				{name:'email',     index:'email', width:100, align:"left"}
			],
			sortname: 'paxSequence',
			sortOrder: "asc",
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}
	function AIR_Util(){};
	// --------------------------DOM Manipulating ----------------------------------

	
	AIR_Util.dom = function (){};
	
	AIR_Util.dom.cloneObject = function (obj) {
		if(obj==null) return null;
		var clone = (obj instanceof Array) ? [] : {};
        for(var i in obj) {
            if(typeof(obj[i])=="object")
                clone[i] = AIR_Util.dom.cloneObject(obj[i]);
            else
                clone[i] = obj[i];
        }
        return clone;
    }

	
	//-------------------------------Sorting functions --------------------------------------
	AIR_Util.sort = function () {};
	
	AIR_Util.sort.quickSort = function (inputArray,comparator){
		var less = [];
		var greater = [];
		if(inputArray.length <= 1 )
			return inputArray;
		
		var pivot = inputArray[inputArray.length-1];
		
		for(var i=0 ; i < inputArray.length-1; i++){
			if(comparator(inputArray[i],pivot)>0){
				greater[greater.length] = inputArray[i];
			}else{
				less[less.length]= inputArray[i];
			}
		}
		return ((AIR_Util.sort.quickSort(less,comparator)).concat([pivot]).concat(AIR_Util.sort.quickSort(greater,comparator)));
	}
	
	AIR_Util.sort.anci = function() {};
	
	/*
	 * Anci segment comparator for bubble sort
	 */
AIR_Util.sort.anci.segmentByDateComparator = function(first, second) {
	if (first.dateObj != undefined && first.dateObj != null
			&& second.dateObj != undefined && second.dateObj != null) {
		return (first.dateObj.getTime() - second.dateObj.getTime());
	}
	return 0;
}

UI_tabAnciInfo.updateCancelledSegmentInfo = function(pax,fltSegs){
	var segInfo = UI_tabAnciInfo.getCancelledSegInfo(fltSegs.flightRefNumber);
	if(segInfo!=undefined && segInfo!=null){
		var dateFormatter = UI_commonSystem.dateFormatter('accelaero');
		var d = UI_tabAnciInfo.getDateObj(segInfo.departureDate, segInfo.departureTime);
		pax.segCode = fltSegs.segmentCode;
		pax.flightNumber = segInfo.flightNo;
		pax.departureDate = dateFormatter(d);
		pax.arrivalDate = dateFormatter(UI_tabAnciInfo.getDateObj(segInfo.arrivalDate, segInfo.arrivalTime));
		pax.dateObj = d;
	}
}

UI_tabAnciInfo.getCancelledSegInfo = function(fltRefNo){
	for(var i = 0 ; i < jsonFltDetails.length ; i++){
		if(jsonFltDetails[i].flightRefNumber == fltRefNo && jsonFltDetails[i].status == 'CNX'){
			return jsonFltDetails[i];
		}
	}
}
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	