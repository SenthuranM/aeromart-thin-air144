	/*
	 * Passenger Tab
	 */
	function UI_paxDetails(){}
	
	UI_paxDetails.fdCode = "";
	UI_paxDetails.fdUniq = "N";
	UI_paxDetails.fdDesc = "";
	UI_paxDetails.groupId = false;
	UI_paxDetails.isError = "false";
	UI_paxDetails.infFoidCode = "";
	UI_paxDetails.fdReq = "";
	/***** IE Memory leak FIXEs  ****/
	UI_paxDetails.jsonPaxAdtObj = {};
	UI_paxDetails.fltOutDate = {};
	UI_paxDetails.paxConfigAD = {};
	UI_paxDetails.paxConfigCH = {};
	UI_paxDetails.paxConfigCH = {};
	UI_paxDetails.jsonFltDetails = {};
	UI_paxDetails.arrError = [];
	UI_paxDetails.jsonSegs = [];
	
	UI_paxDetails.foidIds = "";
	UI_paxDetails.foidCode = "";
	UI_paxDetails.segmentList = "";
	
	UI_paxDetails.foidIdsPass = "";
	UI_paxDetails.foidCodePass = "";
	UI_paxDetails.segmentListPass = "";
	/***** IE Memory leak FIXEs  ****/
	var paxAdults = {};
	var paxInfants = {};
	
	
	var flightPsptList = "";
	var globalPsptList = "";
	
	var minDate = new Date();
	
	var adCount = 0;
	var chCount = 0;
	var inCount = 0;

	/*
	 * Contact Info Tab Page On Load
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		$("#btnEdit").decorateButton();
		$("#btnEdit").click(function() {UI_paxDetails.editOnclick();});
		
		$("#btnConfirm").decorateButton();
		$("#btnConfirm").disable();
		$("#btnConfirm").click(function() {UI_paxDetails.confirmOnclick();});
		
		$("#btnCancel").decorateButton();
		$("#btnCancel").click(function() {UI_paxDetails.cancelOnclick();});
		
		UI_paxDetails.onLoad();
		$("#1_pnrPaxCatFOIDNumber").focus()
	});
	
	/*
	 * Pax details On Load
	 */
	UI_paxDetails.onLoad = function() {
	if (DATA_ResPro.mode == 'CREATE') {
		paxAdults = opener.UI_tabPassenger.getAdultDetails();
		paxInfants = opener.UI_tabPassenger.getInfantDetails();
	}
	if (DATA_ResPro.mode == 'MODIFY') {
		paxAdults = opener.UI_tabBookingInfo.getAdultDetails();
		paxInfants = opener.UI_tabBookingInfo.getInfantDetails();
	}
	/** *** IE Memory leak FIXEs load opener object in the loading time*** */
	UI_paxDetails.jsonPaxAdtObj = opener.jsonPaxAdtObj;
	UI_paxDetails.fltOutDate = opener.fltOutDate

	UI_paxDetails.flightLsttSegmentDeptDate = opener.lastSegmentDepartureDate;

	UI_paxDetails.paxConfigAD = opener.paxConfigAD;
	UI_paxDetails.paxConfigCH = opener.paxConfigCH;
	UI_paxDetails.paxConfigIN = opener.paxConfigIN;
	UI_paxDetails.jsonFltDetails = opener.jsonFltDetails;
	UI_paxDetails.arrError = opener.top.arrError;

	if (DATA_ResPro.mode == 'MODIFY') {
		UI_paxDetails.jsonSegs = opener.UI_reservation.jsonSegs;
		UI_paxDetails.foidIds = opener.UI_tabBookingInfo.foidIds;
		UI_paxDetails.foidCode = opener.UI_tabBookingInfo.foidCode;
		UI_paxDetails.segmentList = opener.UI_tabBookingInfo.segmentList;
	} else {
		UI_paxDetails.foidIdsPass = opener.UI_tabPassenger.foidIds;
		UI_paxDetails.foidCodePass = opener.UI_tabPassenger.foidCode;
		UI_paxDetails.segmentListPass = opener.UI_tabPassenger.segmentList;
	}
	/** *** IE Memory leak FIXEs *** */
	UI_paxDetails.fdCode = UI_paxDetails.paxConfigAD.passportNo.ssrCode;
	UI_paxDetails.fdUniq = UI_paxDetails.paxConfigAD.passportNo.uniqueness;
	UI_paxDetails.fdDesc = "Passport No";

	UI_paxDetails.fdADReq = UI_paxDetails.paxConfigAD.passportNo.xbeMandatory;
	UI_paxDetails.fdCHReq = UI_paxDetails.paxConfigCH.passportNo.xbeMandatory;
	UI_paxDetails.fdIFReq = UI_paxDetails.paxConfigCH.passportNo.xbeMandatory;

	UI_paxDetails.infFoidCode = UI_paxDetails.paxConfigCH.passportNo.ssrCode;

	UI_paxDetails.groupId = UI_paxDetails.paxConfigAD.groupId.xbeVisibility;

	$("#divHDPane1").html("Pax Details");
	UI_paxDetails.fillPaneData();
	UI_paxDetails.mapCalendar();
	// UI_paxDetails.disableEnableControls(true);
	if (isGdsPnr == "true") {
		UI_paxDetails.disableEnableControls(true);
		$("#btnEdit").hide();
		$("#btnEdit").disable();
		$("#btnConfirm").disable();
	} else {
		UI_paxDetails.disableEnableControls(false);
		$("#btnEdit").hide();
		$("#btnEdit").disable();
		$("#btnConfirm").enable();
	}

}	

	
	/*
	 * Fill Pane Data
	 */
	UI_paxDetails.fillPaneData = function(inParams){
		UI_paxDetails.constructGridPane({id:"#tablPane1"});
		var objPaxDt = new Array();
		var adultCount = UI_paxDetails.jsonPaxAdtObj.paxAdults.length;
		for(var i = 0; i < adultCount; i++) {
			objPaxDt[i] = {};
			objPaxDt[i].paxType 	= UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayAdultType;
			objPaxDt[i].title 	= UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayAdultTitle;
			objPaxDt[i].name 	= UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayAdultFirstName + "&nbsp;" + UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayAdultLastName;
//			if( DATA_ResPro.mode == 'MODIFY' && UI_paxDetails.groupId) {
//				objPaxDt[i].eTicket 	= UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayETicket;
//			}
			objPaxDt[i].pnrPaxCatFOIDNumber = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber;
			objPaxDt[i].nationalIDNo = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayNationalIDNo;
			objPaxDt[i].pnrPaxCatFOIDExpiry = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry;
			objPaxDt[i].pnrPaxGroupId = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId;
			if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDPlace != ''){
				objPaxDt[i].pnrPaxCatFOIDPlace = UI_paxDetails.findCountry(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDPlace);
			}
			objPaxDt[i].placeOfBirth = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayPnrPaxPlaceOfBirth;
			if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayTravelDocType != '') {
				objPaxDt[i].travelDocumentType = UI_paxDetails.findDocType(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayTravelDocType);
			}
			objPaxDt[i].visaDocNumber = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayVisaDocNumber;
			objPaxDt[i].visaDocPlaceOfIssue = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayVisaDocPlaceOfIssue;
			objPaxDt[i].visaDocIssueDate = UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayVisaDocIssueDate;
			if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayVisaApplicableCountry != ''){
				objPaxDt[i].visaApplicableCountry = UI_paxDetails.findCountry(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayVisaApplicableCountry);
			}
		}
		if (UI_paxDetails.jsonPaxAdtObj.paxInfants != null) {
			var infantCount = UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
			for(var j = 0; j < infantCount; j++) {
				objPaxDt[j + adultCount] = {};
				objPaxDt[j + adultCount].paxType 	= "IN";
				
				if(UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayInfantTitle != null){
					objPaxDt[j + adultCount].title 		= UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayInfantTitle;
				} else{
					objPaxDt[j + adultCount].title 		= "-";	
				}
				objPaxDt[j + adultCount].name 		= UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayInfantFirstName + "&nbsp;" + UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayInfantLastName;
//				if( DATA_ResPro.mode == 'MODIFY' && UI_paxDetails.groupId) {
//					objPaxDt[j + adultCount].eTicket 	= UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayETicket;					
//				}
				objPaxDt[j + adultCount].pnrPaxCatFOIDNumber = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayPnrPaxCatFOIDNumber;
				objPaxDt[j + adultCount].nationalIDNo = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayNationalIDNo;
				objPaxDt[j + adultCount].pnrPaxCatFOIDExpiry = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayPnrPaxCatFOIDExpiry;
				objPaxDt[j + adultCount].pnrPaxGroupId = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayPnrPaxGroupId;
				if(UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayPnrPaxCatFOIDPlace != ''){
					objPaxDt[j + adultCount].pnrPaxCatFOIDPlace = UI_paxDetails.findCountry(UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayPnrPaxCatFOIDPlace);
				}
				objPaxDt[j + adultCount].placeOfBirth = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayPnrPaxPlaceOfBirth;
				if (UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayTravelDocType != null) {
					objPaxDt[j + adultCount].travelDocumentType = UI_paxDetails.findDocType(UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayTravelDocType);
				}
				objPaxDt[j + adultCount].visaDocNumber = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayVisaDocNumber;
				objPaxDt[j + adultCount].visaDocPlaceOfIssue = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayVisaDocPlaceOfIssue;
				objPaxDt[j + adultCount].visaDocIssueDate = UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayVisaDocIssueDate;
				if(UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayVisaApplicableCountry != ''){
					objPaxDt[j + adultCount].visaApplicableCountry = UI_paxDetails.findCountry(UI_paxDetails.jsonPaxAdtObj.paxInfants[j].displayVisaApplicableCountry);
				}
			}
		}
		UI_commonSystem.fillGridData({id:"#tablPane1", data:objPaxDt});		
	}

	
	UI_paxDetails.constructGridPane = function(inParams){
		//construct grid
		var columnNames = [];
		var columnModels = [];
		
		var foidColumnHeading;
		var psptExpiryHeading = "Expiry Date ";
		var psptIssuedCntryHeading = "Place of Issue ";		
		var placeOfBirthHeading = "PlaceOfBirth ";
		var travelDocumentTypeHeading = "TravelDocType ";
		var visaDocNumberHeading = "Doc Number ";
		var visaDocPlaceOfIssueHeading = "DocPlaceOfIssue ";
		var visaDocIssueDateHeading = "Doc Issue Date ";
		var visaApplicableCountryHeading = "DocApplicableCountry ";
		var nationalIDHeading = "NationalID No";
		
		function updateMandatory(name){
			return UI_commonSystem.strTxtMandatory + name;
		}
		
		if(UI_paxDetails.fdADReq || UI_paxDetails.fdCHReq) {
			foidColumnHeading = updateMandatory(UI_paxDetails.fdDesc );
		} else {
			foidColumnHeading = UI_paxDetails.fdDesc;
		}
		
		if(UI_paxDetails.paxConfigAD.passportExpiry.xbeMandatory ||
				UI_paxDetails.paxConfigCH.passportExpiry.xbeMandatory){
			psptExpiryHeading = updateMandatory(psptExpiryHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.passportIssuedCntry.xbeMandatory ||
				UI_paxDetails.paxConfigCH.passportIssuedCntry.xbeMandatory){
			psptIssuedCntryHeading = updateMandatory(psptIssuedCntryHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.placeOfBirth.xbeMandatory ||
				UI_paxDetails.paxConfigCH.placeOfBirth.xbeMandatory){
			placeOfBirthHeading = updateMandatory(placeOfBirthHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.travelDocumentType.xbeMandatory ||
				UI_paxDetails.paxConfigCH.travelDocumentType.xbeMandatory){
			travelDocumentTypeHeading = updateMandatory(travelDocumentTypeHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.visaDocNumber.xbeMandatory ||
				UI_paxDetails.paxConfigCH.visaDocNumber.xbeMandatory){
			visaDocNumberHeading = updateMandatory(visaDocNumberHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.visaDocPlaceOfIssue.xbeMandatory ||
				UI_paxDetails.paxConfigCH.visaDocPlaceOfIssue.xbeMandatory){
			visaDocPlaceOfIssueHeading = updateMandatory(visaDocPlaceOfIssueHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.visaDocIssueDate.xbeMandatory ||
				UI_paxDetails.paxConfigCH.visaDocIssueDate.xbeMandatory){
			visaDocIssueDateHeading = updateMandatory(visaDocIssueDateHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.visaApplicableCountry.xbeMandatory ||
				UI_paxDetails.paxConfigCH.visaApplicableCountry.xbeMandatory){
			visaApplicableCountryHeading = updateMandatory(visaApplicableCountryHeading);
		}
		
		if(UI_paxDetails.paxConfigAD.nationalIDNo.xbeVisibility && opener.top.isRequestNICForReservationsHavingDomesticSegments == "true"){
			if(isDomesticFlightExist == "true"){
				nationalIDHeading = updateMandatory(nationalIDHeading);
			}
		} else if(UI_paxDetails.paxConfigAD.nationalIDNo.xbeMandatory || UI_paxDetails.paxConfigCH.nationalIDNo.xbeMandatory){
			nationalIDHeading = updateMandatory(nationalIDHeading);
		}
		
		//get Adult, Child & infant count
		var adultCount = UI_paxDetails.jsonPaxAdtObj.paxAdults.length;
		for(var i = 0; i < adultCount; i++) {
			if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayAdultType == "AD"){
				adCount++;
			} else if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i].displayAdultType == "CH"){
				chCount++;
			}
		}
		
		if (UI_paxDetails.jsonPaxAdtObj.paxInfants != null) {
			inCount = UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
		}
		
		//TODO : eticketNo field should be removed from the db after removing references in the code
		if((UI_paxDetails.paxConfigAD.passportNo.xbeVisibility || 
			UI_paxDetails.paxConfigCH.passportNo.xbeVisibility)
			&& (UI_paxDetails.paxConfigAD.visaDocNumber.xbeVisibility || 
			UI_paxDetails.paxConfigCH.visaDocNumber.xbeVisibility)) {
			
			columnNames.push('Type');
			columnNames.push('Title');
			columnNames.push('Name');			
			columnNames.push(foidColumnHeading);
			
			columnModels.push({name:'paxType', index:'paxType', width:40, align:"center"});
			columnModels.push({name:'title', index:'title', width:40, align:"center"});
			columnModels.push({name:'name', index:'name', width:110, align:"left"});
			columnModels.push({name:'pnrPaxCatFOIDNumber', index:'pnrPaxCatFOIDNumber', 
				width:70, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"30", style:"width:60px;"}});
			
			if((UI_paxDetails.paxConfigAD.passportExpiry.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportExpiry.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportExpiry.xbeVisibility && inCount > 0)){
				columnNames.push(psptExpiryHeading);
				columnModels.push({name:'pnrPaxCatFOIDExpiry', index:'pnrPaxCatFOIDExpiry', 
					width:90, align:"center", editable:true, 
					editoptions:{className:"aa-input", maxlength:"30", style:"width:75px;",disabled:true,
					dataEvents:[{
						type : 'change',
						fn: function(e){
							UI_paxDetails.dobOnBlur(this);
						}
					}]}
				});
			}
			
			if((UI_paxDetails.paxConfigAD.passportIssuedCntry.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportIssuedCntry.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportIssuedCntry.xbeVisibility && inCount > 0)){
				columnNames.push(psptIssuedCntryHeading);
				columnModels.push({name : 'pnrPaxCatFOIDPlace',
					 index : 'pnrPaxCatFOIDPlace',
					 width : 110,
					 align : "center",
					 editable : true,
					 edittype : "select",
					 editoptions : {
						 className : "aa-input",
						 value : UI_paxDetails.fillDropFromArray(),
						 style : "width:100px;"
						}
					});
			}
			
			if((UI_paxDetails.paxConfigAD.placeOfBirth.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.placeOfBirth.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.placeOfBirth.xbeVisibility && inCount > 0)){
				columnNames.push(placeOfBirthHeading);		
				columnModels.push({name:'placeOfBirth', index:'placeOfBirth', 
						width:70, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"30", style:"width:60px;"}});
			}
			
			if((UI_paxDetails.paxConfigAD.travelDocumentType.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.travelDocumentType.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.travelDocumentType.xbeVisibility && inCount > 0)){
				columnNames.push(travelDocumentTypeHeading);		
				columnModels.push({name : 'travelDocumentType',
					 index : 'travelDocumentType',
					 width : 100,
					 align : "center",
					 editable : true,
					 edittype : "select",
					 editoptions : {
						 className : "aa-input",
						 value : UI_paxDetails.fillDocTypeDropDown(),
						 style : "width:90px;"
						}
					});
			}
			
			columnNames.push(visaDocNumberHeading);		
			columnModels.push({name:'visaDocNumber', index:'visaDocNumber', 
						width:70, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"30", style:"width:60px;"}});

			if((UI_paxDetails.paxConfigAD.visaDocPlaceOfIssue.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocPlaceOfIssue.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocPlaceOfIssue.xbeVisibility && inCount > 0)){
				columnNames.push(visaDocPlaceOfIssueHeading);		
				columnModels.push({name:'visaDocPlaceOfIssue', index:'visaDocPlaceOfIssue', 
						width:100, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"30", style:"width:90px;"}});
			}
			
			if((UI_paxDetails.paxConfigAD.visaDocIssueDate.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocIssueDate.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocIssueDate.xbeVisibility && inCount > 0)){
				columnNames.push(visaDocIssueDateHeading);
				columnModels.push({name:'visaDocIssueDate', index:'visaDocIssueDate', 
					width:90, align:"center", editable:true, 
					editoptions:{className:"aa-input", maxlength:"30", style:"width:75px;",disabled:true,
					dataEvents:[{
						type : 'change',
						fn: function(e){
							UI_paxDetails.dobOnBlur(this);
						}
					}]}
				});
			}
			
			if((UI_paxDetails.paxConfigAD.visaApplicableCountry.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaApplicableCountry.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaApplicableCountry.xbeVisibility && inCount > 0)){
				columnNames.push(visaApplicableCountryHeading);
				columnModels.push({name : 'visaApplicableCountry',
					 index : 'visaApplicableCountry',
					 width : 120,
					 align : "center",
					 editable : true,
					 edittype : "select",
					 editoptions : {
						 className : "aa-input",
						 value : UI_paxDetails.fillDropFromArray(),
						 style : "width:110px;"
						}
					});
			}			
			
		} else if ((!UI_paxDetails.paxConfigAD.passportNo.xbeVisibility && 
					!UI_paxDetails.paxConfigCH.passportNo.xbeVisibility ) && 
					(UI_paxDetails.paxConfigAD.visaDocNumber.xbeVisibility || 
					UI_paxDetails.paxConfigCH.visaDocNumber.xbeVisibility)){
			columnNames = ['Type', 'Title', 'Name'];
			columnModels = [
						{name:'paxType', index:'paxType', width:50, align:"center"},
						{name:'title', index:'title', width:50, align:"center"},
						{name:'name', index:'name', width:110, align:"left"},
					];			
				
			if((UI_paxDetails.paxConfigAD.placeOfBirth.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.placeOfBirth.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.placeOfBirth.xbeVisibility && inCount > 0)){
				columnNames.push(placeOfBirthHeading);		
				columnModels.push({name:'placeOfBirth', index:'placeOfBirth', 
						width:70, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"30", style:"width:60px;"}});
			}
			
			if((UI_paxDetails.paxConfigAD.travelDocumentType.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.travelDocumentType.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.travelDocumentType.xbeVisibility && inCount > 0)){
				columnNames.push(travelDocumentTypeHeading);		
				columnModels.push({name : 'travelDocumentType',
					 index : 'travelDocumentType',
					 width : 40,
					 align : "center",
					 editable : true,
					 edittype : "select",
					 editoptions : {
						 className : "aa-input",
						 value : UI_paxDetails.fillDocTypeDropDown(),
						 style : "width:30px;"
						}
					});			
			}
			
			columnNames.push(visaDocNumberHeading);		
			columnModels.push({name:'visaDocNumber', index:'visaDocNumber', 
						width:70, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"30", style:"width:60px;"}});

			if((UI_paxDetails.paxConfigAD.visaDocPlaceOfIssue.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocPlaceOfIssue.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocPlaceOfIssue.xbeVisibility && inCount > 0)){
				columnNames.push(visaDocPlaceOfIssueHeading);		
				columnModels.push({name:'visaDocPlaceOfIssue', index:'visaDocPlaceOfIssue', 
						width:100, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"30", style:"width:90px;"}});
			}
			
			if((UI_paxDetails.paxConfigAD.visaDocIssueDate.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocIssueDate.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaDocIssueDate.xbeVisibility && inCount > 0)){
				columnNames.push(visaDocIssueDateHeading);
				columnModels.push({name:'visaDocIssueDate', index:'visaDocIssueDate', 
					width:90, align:"center", editable:true, 
					editoptions:{className:"aa-input", maxlength:"30", style:"width:75px;",disabled:true,
					dataEvents:[{
						type : 'change',
						fn: function(e){
							UI_paxDetails.dobOnBlur(this);
						}
					}]}
				});
			}
			
			if((UI_paxDetails.paxConfigAD.visaApplicableCountry.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaApplicableCountry.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.visaApplicableCountry.xbeVisibility && inCount > 0)){
				columnNames.push(visaApplicableCountryHeading);
				columnModels.push({name : 'visaApplicableCountry',
					 index : 'visaApplicableCountry',
					 width : 120,
					 align : "center",
					 editable : true,
					 edittype : "select",
					 editoptions : {
						 className : "aa-input",
						 value : UI_paxDetails.fillDropFromArray(),
						 style : "width:110px;"
						}
					});
			}
			
			
		} else if ((UI_paxDetails.paxConfigAD.passportNo.xbeVisibility || 
				UI_paxDetails.paxConfigCH.passportNo.xbeVisibility ) && 
				(!UI_paxDetails.paxConfigAD.visaDocNumber.xbeVisibility && 
				!UI_paxDetails.paxConfigCH.visaDocNumber.xbeVisibility)){
			
			columnNames.push('Type');
			columnNames.push('Title');
			columnNames.push('Name');
			columnNames.push(foidColumnHeading);
			
			columnModels.push({name:'paxType', index:'paxType', width:50, align:"center"});
			columnModels.push({name:'title', index:'title', width:50, align:"center"});
			columnModels.push({name:'name', index:'name', width:110, align:"left"});
			columnModels.push({name:'pnrPaxCatFOIDNumber', index:'pnrPaxCatFOIDNumber', 
				width:70, align:"center", editable:true, 
				editoptions:{className:"aa-input fontCapitalize", maxlength:"30", style:"width:60px;"}});
			
			if((UI_paxDetails.paxConfigAD.passportExpiry.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportExpiry.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportExpiry.xbeVisibility && inCount > 0)){
				columnNames.push(psptExpiryHeading);
				columnModels.push({name:'pnrPaxCatFOIDExpiry', index:'pnrPaxCatFOIDExpiry', 
							width:70, align:"center", editable:true,
							editoptions:{className:"aa-input fontCapitalize", maxlength:"30", style:"width:60px;",onchange : 'UI_paxDetails.dobOnBlur(this)',
							dataEvents:[{
								type : 'change',
								fn: function(e){
									UI_paxDetails.dobOnBlur(this);
								}
							}]}
					});
			}
			
			if((UI_paxDetails.paxConfigAD.passportIssuedCntry.xbeVisibility && adCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportIssuedCntry.xbeVisibility && chCount > 0) ||
					(UI_paxDetails.paxConfigCH.passportIssuedCntry.xbeVisibility && inCount > 0)){
				columnNames.push(psptIssuedCntryHeading);
				columnModels.push({name : 'pnrPaxCatFOIDPlace',
						 index : 'pnrPaxCatFOIDPlace',
						 width : 100,
						 align : "center",
						 editable : true,
						 edittype : "select",
						 editoptions : {
							 className : "aa-input",
							 value : UI_paxDetails.fillDropFromArray(),
							 style : "width:90px;"
							}
					});
			}
			
		} else {
			columnNames = ['Type', 'Title', 'Name'];
			columnModels = [
						{name:'paxType', index:'paxType', width:50, align:"center"},
						{name:'title', index:'title', width:50, align:"center"},
						{name:'name', index:'name', width:110, align:"left"},
					];
		}
		
		if (UI_paxDetails.groupId) {
			columnNames.push("Group ID");
			columnModels.push({name:'pnrPaxGroupId', index:'pnrPaxGroupId', 
				width:70, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"10", style:"width:60px;"}});
		}
		
		if (UI_paxDetails.paxConfigAD.nationalIDNo.xbeVisibility){
			columnNames.push(nationalIDHeading);
			columnModels.push({name : 'nationalIDNo',
					 index : 'nationalIDNo',
					 width : 100,
					 align : "center",
					 editable : true,
					 editoptions : {
						 className : "aa-input",
						 maxlength:"30",
						 style : "width:90px;"
						}
				});
		}
		
		var totalWidth = 50;
		if(columnModels.length > 0){
			for(var i = 0; i< columnModels.length; i++){
				totalWidth += (columnModels[i].width);
			}
		}else {
			totalWidth = 600;
		}

		$(inParams.id).jqGrid({
			datatype: "local",
			height: 130,
			width: totalWidth,
			colNames:columnNames,
			colModel:columnModels,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
			}
		});
	}
	
	UI_paxDetails.dobOnBlur = function(objC) {
		var strDT = dateChk(objC.id);
		strDT = strDT != true ? strDT : objC.value;
		objC.value = strDT;		
		UI_commonSystem.pageOnChange();
	}
	
	/*
	 * Edit
	 */
	UI_paxDetails.editOnclick = function(){
		UI_paxDetails.disableEnableControls(false);		
		$("#btnEdit").disable();
		$("#btnConfirm").enable();
	}
	
	/*
	 * Disable Page Controls
	 */ 
	UI_paxDetails.disableEnableControls = function(blnStatus){
		var adultCount = UI_paxDetails.jsonPaxAdtObj.paxAdults.length;
		if(UI_paxDetails.fdCode != "") {
			for(var i = 1; i <= adultCount; i++) {
				$.each(UI_paxDetails.jsonPaxAdtObj.paxAdults, function(){
					$("#"+ i + "_pnrPaxCatFOIDNumber").disableEnable(blnStatus);
					$("#"+ i + "_pnrPaxCatFOIDExpiry").disableEnable(blnStatus);
					$("#"+ i + "_pnrPaxCatFOIDPlace").disableEnable(blnStatus);
				});
			}
			
			if (UI_paxDetails.jsonPaxAdtObj.paxInfants != null) {
				var infantCount = UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
				var index = 0;
				if(UI_paxDetails.infFoidCode != ""
					&& UI_paxDetails.paxConfigCH.passportNo.xbeVisibility) {
					for(var j = 1; j <= infantCount; j++) {
						$.each(UI_paxDetails.jsonPaxAdtObj.paxInfants, function(){
							index = j + adultCount;
							$("#"+ index + "_pnrPaxCatFOIDNumber").disableEnable(blnStatus);
							$("#"+ index + "_pnrPaxCatFOIDExpiry").disableEnable(blnStatus);
							$("#"+ index + "_pnrPaxCatFOIDPlace").disableEnable(blnStatus);
						});					
					}
				} else {
					for(var j = 1; j <= infantCount; j++) {
						$.each(UI_paxDetails.jsonPaxAdtObj.paxInfants, function(){
							index = j + adultCount;
							$("#"+ index + "_pnrPaxCatFOIDNumber").disableEnable(true);
							$("#"+ index + "_pnrPaxCatFOIDExpiry").disableEnable(true);
							$("#"+ index + "_pnrPaxCatFOIDPlace").disableEnable(true);
						});					
					}
				}
			}
		}
		
		for(var i = 1; i <= adultCount; i++) {
			$.each(UI_paxDetails.jsonPaxAdtObj.paxAdults, function(){
				$("#"+ i + "_placeOfBirth").disableEnable(blnStatus);
				$("#"+ i + "_travelDocumentType").disableEnable(blnStatus);
				$("#"+ i + "_visaDocNumber").disableEnable(blnStatus);
				$("#"+ i + "_visaDocPlaceOfIssue").disableEnable(blnStatus);
				$("#"+ i + "_visaDocIssueDate").disableEnable(blnStatus);
				$("#"+ i + "_visaApplicableCountry").disableEnable(blnStatus);
				$("#"+ i + "_nationalIDNo").disableEnable(blnStatus);
			});
		}
		
		if (UI_paxDetails.jsonPaxAdtObj.paxInfants != null) {
			var infantCount = UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
			var index = 0;
			if(UI_paxDetails.infFoidCode != ""
				&& UI_paxDetails.paxConfigCH.visaDocNumber.xbeVisibility) {
				for(var j = 1; j <= infantCount; j++) {
					$.each(UI_paxDetails.jsonPaxAdtObj.paxInfants, function(){
						index = j + adultCount;
						$("#"+ index + "_placeOfBirth").disableEnable(blnStatus);
						$("#"+ index + "_travelDocumentType").disableEnable(blnStatus);
						$("#"+ index + "_visaDocNumber").disableEnable(blnStatus);
						$("#"+ index + "_visaDocPlaceOfIssue").disableEnable(blnStatus);
						$("#"+ index + "_visaDocIssueDate").disableEnable(blnStatus);
						$("#"+ index + "_visaApplicableCountry").disableEnable(blnStatus);
						$("#"+ index + "_nationalIDNo").disableEnable(blnStatus);
					});					
				}
			} else {
				for(var j = 1; j <= infantCount; j++) {
					$.each(UI_paxDetails.jsonPaxAdtObj.paxInfants, function(){
						index = j + adultCount;
						$("#"+ index + "_placeOfBirth").disableEnable(true);
						$("#"+ index + "_travelDocumentType").disableEnable(true);
						$("#"+ index + "_visaDocNumber").disableEnable(true);
						$("#"+ index + "_visaDocPlaceOfIssue").disableEnable(true);
						$("#"+ index + "_visaDocIssueDate").disableEnable(true);
						$("#"+ index + "_visaApplicableCountry").disableEnable(true);
						$("#"+ index + "_nationalIDNo").disableEnable(true);
					});					
				}
			}
		}
	}
	
	/*
	 * Save
	 */
	UI_paxDetails.confirmOnclick = function(){		
		if(!UI_paxDetails.validate()) {			
			// Validation not ok, return to page
			return;
		}

		var data = {};
		
		// PNR
		//data['groupPNR'] = opener.jsonGroupPNR;
		//data['pnr'] = opener.jsonPNR;
		//data['version'] = opener.jsonBkgInfo.version;
		UI_commonSystem.showProgress();		

		var adultCount = UI_paxDetails.jsonPaxAdtObj.paxAdults.length;
		var infantCount = 0;
		if(UI_paxDetails.jsonPaxAdtObj.paxInfants != null){
			infantCount = UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
		}
		var infantIndex = 0;
		var totalCount = adultCount+infantCount;
		for(var i = 1; i <= totalCount; i++) {
			var index = i - 1;
			var rowTR = $("#"+ i + "paxType").closest('tr');				
			
			if(i <= adultCount) {
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayPnrPaxCatFOIDNumber = $.trim($("#"+ i + "_pnrPaxCatFOIDNumber").val());
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayNationalIDNo = $("#"+ i + "_nationalIDNo").val();
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayPnrPaxCatFOIDExpiry = $("#"+ i + "_pnrPaxCatFOIDExpiry").val();
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayPnrPaxCatFOIDPlace = $("#"+ i + "_pnrPaxCatFOIDPlace").val();
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayPnrPaxGroupId = $("#"+ i + "_pnrPaxGroupId").val();
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayPnrPaxPlaceOfBirth = $.trim($("#"+ i + "_placeOfBirth").val());
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayTravelDocType = $("#"+ i + "_travelDocumentType").val();
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayVisaDocNumber = $("#"+ i + "_visaDocNumber").val();
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayVisaDocPlaceOfIssue = $.trim($("#"+ i + "_visaDocPlaceOfIssue").val());
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayVisaDocIssueDate = $("#"+ i + "_visaDocIssueDate").val();
				UI_paxDetails.jsonPaxAdtObj.paxAdults[index].displayVisaApplicableCountry = $("#"+ i + "_visaApplicableCountry").val();
			} else {		
				if(UI_paxDetails.infFoidCode != "") {
					var count = 0;
					if(infantIndex < infantCount){
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayPnrPaxCatFOIDNumber = $.trim($("#"+ i + "_pnrPaxCatFOIDNumber").val());
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayNationalIDNo = $("#"+ i + "_nationalIDNo").val();
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayPnrPaxCatFOIDExpiry = $("#"+ i + "_pnrPaxCatFOIDExpiry").val();
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayPnrPaxCatFOIDPlace = $("#"+ i + "_pnrPaxCatFOIDPlace").val();
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayPnrPaxGroupId = $("#"+ i + "_pnrPaxGroupId").val();
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayPnrPaxPlaceOfBirth = $.trim($("#"+ i + "_placeOfBirth").val());
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayTravelDocType = $("#"+ i + "_travelDocumentType").val();
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayVisaDocNumber = $("#"+ i + "_visaDocNumber").val();
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayVisaDocPlaceOfIssue = $.trim($("#"+ i + "_visaDocPlaceOfIssue").val());
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayVisaDocIssueDate = $("#"+ i + "_visaDocIssueDate").val();
						UI_paxDetails.jsonPaxAdtObj.paxInfants[infantIndex].displayVisaApplicableCountry = $("#"+ i + "_visaApplicableCountry").val();
						infantIndex++;
					}				
				}
			}
		}	
		UI_paxDetails.disableEnableControls(true);
		UI_commonSystem.hideProgress();
		
		//Value accessible from the tabPassenger.js. Specifies whether passport data is validated or not.
		opener.passportDetailsvalidated=true;
		if (opener.csvFileUpload && opener.$('#btnInternationalFlight').is(":visible") && opener.parseSuccessful) {
			opener.$('#btnInternationalFlight').trigger('click');
			opener.csvFileUpload = false;
		}
		window.close();
	}
	
UI_paxDetails.validate = function() {
		
		var foidLists  = "";
		var currFoidList  = "";
		var uniqfoidCod = "";
		var uniqfoid = "";
		var validate1 = "true";
		var paxConfig = "";
		var visaDocLists  = "";
		var currVisaDocList  = "";
		
		// Validate FOID Fields	for AD & CH
		var numPax = 0;
		numPax = UI_paxDetails.jsonPaxAdtObj.paxAdults.length;
		var strFlightLstSegmentDeptDate = "";
		if(DATA_ResPro.mode == 'MODIFY') {
			if (UI_paxDetails.fltOutDate != undefined && UI_paxDetails.fltOutDate != null) {
				strFlightLstSegmentDeptDate = UI_paxDetails.fltOutDate;
			}			
		} else {
			if (UI_paxDetails.flightLsttSegmentDeptDate != undefined && UI_paxDetails.flightLsttSegmentDeptDate != null) {
				strFlightLstSegmentDeptDate = UI_paxDetails.flightLsttSegmentDeptDate;
			}			
		}
		
		var flightLstSegmentDeptDate = UI_paxDetails.convertStringintoDateObj(strFlightLstSegmentDeptDate);		
		
		uniqfoidCod = "Passport No";
		if(numPax != 0){
			if(UI_paxDetails.jsonPaxAdtObj.paxAdults[0].displayAdultType == 'AD'){
				uniqfoid = UI_paxDetails.paxConfigAD.passportNo.ssrCode;
			} else if(UI_paxDetails.jsonPaxAdtObj.paxAdults[0].displayAdultType == 'CH') {
				uniqfoid = UI_paxDetails.paxConfigCH.passportNo.ssrCode;
			}
			
		}		
		
		for(var i = 1; i <= numPax; i++) {
			if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultType == 'AD'){			
				paxConfig = UI_paxDetails.paxConfigAD;				
				
			} else if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultType == 'CH'){		
				paxConfig = UI_paxDetails.paxConfigCH;										
			}
			
			UI_paxDetails.fdCode = paxConfig.passportNo.ssrCode;
			UI_paxDetails.fdUniq = paxConfig.passportNo.uniqueness;
			UI_paxDetails.fdDesc = "Passport No";
			UI_paxDetails.fdReq  = paxConfig.passportNo.xbeMandatory;
			
						
			//Passport(DOCS) validation	
			if ($("#"+ i + "_pnrPaxCatFOIDNumber").val()) {
				$("#"+ i + "_pnrPaxCatFOIDNumber").val(trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()));
			}
			if(UI_paxDetails.fdCode != null && UI_paxDetails.fdCode != '' && (UI_paxDetails.fdCode == 'PSPT' || UI_paxDetails.fdCode == 'DOCS')){
				/*if(!UI_paxDetails.fdReq && trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()) != ""){
					continue;
				}*/	
				
				var notEmptyValidation = false;
				if(paxConfig.passportNo.xbeVisibility && UI_paxDetails.fdReq && trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()) == ""){
					if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A'){					
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], UI_paxDetails.fdDesc)});
						$("#"+ i + "_pnrPaxCatFOIDNumber").focus();
						return false;
					}
					
					notEmptyValidation = true;
				} else if(paxConfig.passportNo.xbeVisibility &&
						trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()) != ""){
					notEmptyValidation = true;
				}
								
				if(notEmptyValidation){					
					if(!isAlphaNumeric(trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()))) {
						// TODO Give error message
						//showERRMessage(raiseError("XBE-ERR-04","FOID Number"));
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], UI_paxDetails.fdDesc)});
						$("#"+ i + "_pnrPaxCatFOIDNumber").focus();
						return false;
					}
					
					if($("#"+ i + "_pnrPaxCatFOIDNumber").val() != "" &&
							UI_paxDetails.fdUniq != "NONE"){
						if( DATA_ResPro.mode == 'MODIFY' ) {
							if(isDuplicatePassportNumberCheckOnlyWithinAdult){
								if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultType == 'AD'){
									currFoidList += trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^";
								}
							}else{
								currFoidList += trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^";
							}					
						}
						if( DATA_ResPro.mode == 'MODIFY' && UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].pnrPaxCatFOIDNumber !="" && UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].pnrPaxCatFOIDNumber != trim($("#"+ i + "_pnrPaxCatFOIDNumber").val() )) {
							if(isDuplicatePassportNumberCheckOnlyWithinAdult){
								if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultType == 'AD'){
									foidLists +=trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^" ;
								}
							}else{
								foidLists +=trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^" ;
							}
						} else if (DATA_ResPro.mode != 'MODIFY') {
							if(isDuplicatePassportNumberCheckOnlyWithinAdult){
								if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultType == 'AD'){
									foidLists +=trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^" ;
								}
							}else{
								foidLists +=trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^" ;
							}
						}
						if (uniqfoidCod ==""){uniqfoidCod = UI_paxDetails.fdDesc; }
						if (uniqfoid==""){uniqfoid = UI_paxDetails.fdCode; }
					}				
				}
			}			
			
// Check only for Adult since having national ID for child and infant is not practical. Make configurable if needed.
		if (!DATA_ResPro.initialParams.allowSkipNic) {
			if (opener.top.isRequestNICForReservationsHavingDomesticSegments == "true") {
				if (opener.$("#" + i + "_displayAdultFirstName").val() != 'T B A'|| opener.$("#" + i + "_displayAdultLastName").val() != 'T B A') {
					if ( (UI_paxDetails.jsonPaxAdtObj.paxAdults[i - 1].displayAdultType == "AD" ||
							UI_paxDetails.jsonPaxAdtObj.paxAdults[i - 1].displayAdultType == "CH")
							&& isDomesticFlightExist == "true"&& trim($("#" + i + "_nationalIDNo").val()) == "") {
						UI_message.showErrorMessage({messageText : buildError(UI_paxDetails.arrError["XBE-ERR-01"],"National ID No")});
						$("#" + i + "_nationalIDNo").focus();
						return false;
					}
				}
			} else if (paxConfig.nationalIDNo.xbeVisibility && paxConfig.nationalIDNo.xbeMandatory) {
				if (UI_paxDetails.jsonPaxAdtObj.paxAdults[i - 1].displayAdultFirstName != 'T B A'
						|| UI_paxDetails.jsonPaxAdtObj.paxAdults[i - 1].displayAdultLastName != 'T B A') {
					if (trim($("#" + i + "_nationalIDNo").val()) == "") {
						UI_message.showErrorMessage({
							messageText : buildError(UI_paxDetails.arrError["XBE-ERR-01"],"National ID No")});
						$("#" + i + "_nationalIDNo").focus();
						return false;
					}
				}
			}
		}
			if(paxConfig.nationalIDNo.xbeVisibility){
  				if(trim($("#"+ i + "_nationalIDNo").val()) != "" && !isAlphaNumericWithhyphen(trim($("#"+ i + "_nationalIDNo").val()))) {
  					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "National ID No")});
  					$("#"+ i + "_nationalIDNo").focus();
  					return false;
  				}
			}
						
			if(paxConfig.passportExpiry.xbeVisibility && 
					paxConfig.passportExpiry.xbeMandatory){
				if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A'){					
					if(trim($("#"+ i + "_pnrPaxCatFOIDExpiry").val()) == ""){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Passport Expiry Date")});
						$("#"+ i + "_pnrPaxCatFOIDExpiry").focus();
						return false;
					}
				}
				var selDateArr = $("#"+ i + "_pnrPaxCatFOIDExpiry").val().split('/');
				var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
				
				if(selDate < minDate){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Passport Expiry Date")});
					$("#"+ i + "_pnrPaxCatFOIDExpiry").focus();
					return false;
				}
				
			} else if(paxConfig.passportExpiry.xbeVisibility && 
					!paxConfig.passportExpiry.xbeMandatory){
				if(trim($("#"+ i + "_pnrPaxCatFOIDExpiry").val()) != ""){
					var selDateArr = $("#"+ i + "_pnrPaxCatFOIDExpiry").val().split('/');
					var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					
					if(selDate < minDate){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Passport Expiry Date")});
						$("#"+ i + "_pnrPaxCatFOIDExpiry").focus();
						return false;
					}
					
					if(trim($("#"+ i + "_pnrPaxCatFOIDExpiry").val()) != ""){
						var psptExpDate = UI_paxDetails.convertStringintoDateObj($("#"+ i + "_pnrPaxCatFOIDExpiry").val());
						if(flightLstSegmentDeptDate >= psptExpDate){
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Expired Passport")});
							$("#"+ i + "_pnrPaxCatFOIDExpiry").focus();							
							return false;
						} 
					}
				}
			}
			
			
			if(paxConfig.passportIssuedCntry.xbeVisibility &&
					paxConfig.passportIssuedCntry.xbeMandatory){
				if(trim($("#"+ i + "_pnrPaxCatFOIDPlace").val()) == "" &&
						(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
						UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A')){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Passport Place of Issue")});
					$("#"+ i + "_pnrPaxCatFOIDPlace").focus();
					return false;
				}
			}
			
			//Visa(DOCO) validation
			if ($("#"+ i + "_visaDocNumber").val()) {
				$("#"+ i + "_visaDocNumber").val(trim($("#"+ i + "_visaDocNumber").val()));
			}
			if(paxConfig.visaDocNumber.ssrCode != null && paxConfig.visaDocNumber.ssrCode != '' && paxConfig.visaDocNumber.ssrCode == 'DOCO'){
				// If we need to configure this, we just need to take this value from app parameter
				var isDuplicateVisaDocNumberCheckOnlyWithinAdult = false;
				var notEmptyValidation = false;
				if(paxConfig.visaDocNumber.xbeVisibility && paxConfig.visaDocNumber.xbeMandatory && trim($("#"+ i + "_visaDocNumber").val()) == ""){
					if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A'){					
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], paxConfig.visaDocNumber.fieldName)});
						$("#"+ i + "_visaDocNumber").focus();
						return false;
					}
					
					notEmptyValidation = true;
				} else if(paxConfig.visaDocNumber.xbeVisibility &&
						trim($("#"+ i + "_visaDocNumber").val()) != ""){
					notEmptyValidation = true;
				}
								
				if(notEmptyValidation){					
					if(!isNumeric(trim($("#"+ i + "_visaDocNumber").val()))) {
						if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
								UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A'){					
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], paxConfig.visaDocNumber.fieldName)});
							$("#"+ i + "_visaDocNumber").focus();
							return false;
						}
						
					}
					
					if($("#"+ i + "_visaDocNumber").val() != "" && paxConfig.visaDocNumber.uniqueness != "NONE"){
						if( DATA_ResPro.mode == 'MODIFY' ) {
							if(isDuplicateVisaDocNumberCheckOnlyWithinAdult){
								if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultType == 'AD'){
									currVisaDocList += trim($("#"+ i + "_visaDocNumber").val())+"^";
								}
							}else{
								currVisaDocList += trim($("#"+ i + "_visaDocNumber").val())+"^";
							}					
						}
						if (DATA_ResPro.mode == 'MODIFY'
							&& UI_paxDetails.jsonPaxAdtObj.paxAdults[i - 1].visaDocNumber != ""
							&& UI_paxDetails.jsonPaxAdtObj.paxAdults[i - 1].visaDocNumber != trim($(
									"#" + i + "_visaDocNumber").val())) {
							if (isDuplicateVisaDocNumberCheckOnlyWithinAdult) {
								if (UI_paxDetails.jsonPaxAdtObj.paxAdults[i - 1].displayAdultType == 'AD') {
									visaDocLists += trim($("#" + i + "_visaDocNumber").val()) + "^";
								}
							} else {
								visaDocLists += trim($("#" + i + "_visaDocNumber").val())+ "^";
							}
						} else if (DATA_ResPro.mode != 'MODIFY') {
							if(isDuplicateVisaDocNumberCheckOnlyWithinAdult){
								if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultType == 'AD'){
									visaDocLists +=trim($("#"+ i + "_visaDocNumber").val())+"^" ;
								}
							}else{
								visaDocLists +=trim($("#"+ i + "_visaDocNumber").val())+"^" ;
							}
						}
						//if (uniqfoidCod ==""){uniqfoidCod = UI_paxDetails.fdDesc; }
						//if (uniqfoid==""){uniqfoid = UI_paxDetails.fdCode; }
					}				
				}
			}			
			
			if(paxConfig.visaDocIssueDate.xbeVisibility && 
					paxConfig.visaDocIssueDate.xbeMandatory){
				if(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A'){					
					if(trim($("#"+ i + "_visaDocIssueDate").val()) == ""){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Visa Doc Issue Date")});
						$("#"+ i + "_visaDocIssueDate").focus();
						return false;
					}
				}
				
				var selDateArr = $("#"+ i + "_visaDocIssueDate").val().split('/');
				var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
				
				if(selDate > minDate){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Visa Doc Issue Date")});
					$("#"+ i + "_visaDocIssueDate").focus();
					return false;
				}
				
			} else if(paxConfig.visaDocIssueDate.xbeVisibility && 
					!paxConfig.visaDocIssueDate.xbeMandatory){
				if(trim($("#"+ i + "_visaDocIssueDate").val()) != ""){
					var selDateArr = $("#"+ i + "_visaDocIssueDate").val().split('/');
					var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					
					if(selDate > minDate){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Visa Doc Issue Date")});
						$("#"+ i + "_visaDocIssueDate").focus();
						return false;
					}
					
					if(trim($("#"+ i + "_visaDocIssueDate").val()) != ""){
						var psptExpDate = UI_paxDetails.convertStringintoDateObj($("#"+ i + "_visaDocIssueDate").val());
						if(flightLstSegmentDeptDate >= psptExpDate){
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Expired Passport")});
							$("#"+ i + "_visaDocIssueDate").focus();							
							return false;
						} 
					}					
				}
			}

			if(paxConfig.placeOfBirth.xbeVisibility){
				if(paxConfig.placeOfBirth.xbeMandatory && trim($("#"+ i + "_placeOfBirth").val()) == ""  &&
						(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
						UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A')){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Place Of Birth")});
					$("#"+ i + "_placeOfBirth").focus();
					return false;
				} else if (trim($("#"+ i + "_placeOfBirth").val()) != "" && !isAlphaNumeric(trim($("#"+ i + "_placeOfBirth").val()))) {
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], paxConfig.placeOfBirth.fieldName)});
					$("#"+ i + "_placeOfBirth").focus();
					return false;
				}
			}		
			
			if(paxConfig.travelDocumentType.xbeVisibility &&
					paxConfig.travelDocumentType.xbeMandatory){
				if(trim($("#"+ i + "_travelDocumentType").val()) == "" &&
						(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
						UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A')){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Travel Document Type")});
					$("#"+ i + "_travelDocumentType").focus();
					return false;
				}
			}			
			
			if(paxConfig.visaDocPlaceOfIssue.xbeVisibility){
				if(paxConfig.visaDocPlaceOfIssue.xbeMandatory && trim($("#"+ i + "_visaDocPlaceOfIssue").val()) == "" &&
						(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
						UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A')){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Visa Doc Place Of Issue")});
					$("#"+ i + "_visaDocPlaceOfIssue").focus();
					return false;
				} else if (trim($("#"+ i + "_visaDocPlaceOfIssue").val()) != "" && !isAlphaNumeric(trim($("#"+ i + "_visaDocPlaceOfIssue").val()))){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], paxConfig.visaDocPlaceOfIssue.fieldName)});
					$("#"+ i + "_visaDocPlaceOfIssue").focus();
					return false;					
				}
			}
			
			if(paxConfig.visaApplicableCountry.xbeVisibility &&
					paxConfig.visaApplicableCountry.xbeMandatory){
				if(trim($("#"+ i + "_visaApplicableCountry").val()) == "" &&
						(UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultFirstName != 'T B A' ||
						UI_paxDetails.jsonPaxAdtObj.paxAdults[i-1].displayAdultLastName != 'T B A')){
					UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Visa Applicable Country")});
					$("#"+ i + "_visaApplicableCountry").focus();
					return false;
				}
			}
				
		}
		
		//Validate FOID Fields	for IN
		var numInPax = UI_paxDetails.jsonPaxAdtObj.paxInfants==null?0:UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
		
		uniqfoidCod = "Passport No";
		if(numInPax != 0){
			uniqfoid = UI_paxDetails.paxConfigCH.passportNo.ssrCode;
			var totPaxCount = numPax + numInPax;
			var infIntCount = 0;
			for(var i = 1; i <= totPaxCount; i++) {
				
				//Skip AD+CH
				if(i<=numPax){
					continue;
				}
				
				paxConfig = UI_paxDetails.paxConfigIN;				
				
				UI_paxDetails.fdCode = paxConfig.passportNo.ssrCode;
				UI_paxDetails.fdUniq = paxConfig.passportNo.uniqueness;
				UI_paxDetails.fdDesc = "Passport No";
				UI_paxDetails.fdReq  = paxConfig.passportNo.xbeMandatory;
				var infantCount = UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
				
				//Passport(DOCS) validation
				if ($("#"+ i + "_pnrPaxCatFOIDNumber").val()) {
					$("#"+ i + "_pnrPaxCatFOIDNumber").val(trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()));				
				}
				if(UI_paxDetails.fdCode != null && UI_paxDetails.fdCode != '' && (UI_paxDetails.fdCode == 'PSPT' || UI_paxDetails.fdCode == 'DOCS')){
					
					var notEmptyValidation = false;
					if(paxConfig.passportNo.xbeVisibility && UI_paxDetails.fdReq && trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()) == ""){
						if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
								UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){					
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], UI_paxDetails.fdDesc)});
							$("#"+ i + "_pnrPaxCatFOIDNumber").focus();
							return false;
						}
						notEmptyValidation = true;
					} else if(paxConfig.passportNo.xbeVisibility && !UI_paxDetails.fdReq &&
							trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()) != ""){
						notEmptyValidation = true;
					}
					
					if(notEmptyValidation){						
						if(!isAlphaNumeric(trim($("#"+ i + "_pnrPaxCatFOIDNumber").val()))) {
							// TODO Give error message
							//showERRMessage(raiseError("XBE-ERR-04","FOID Number"));
							if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
									UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){					
								UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], UI_paxDetails.fdDesc)});
								$("#"+ i + "_pnrPaxCatFOIDNumber").focus();
								return false;
							}
							
						}
						
						if($("#"+ i + "_pnrPaxCatFOIDNumber").val() != "" &&
								UI_paxDetails.fdUniq != "NONE"){
							if( DATA_ResPro.mode == 'MODIFY' ) {
								if(!isDuplicatePassportNumberCheckOnlyWithinAdult){
									currFoidList += trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^";
								}
							}
							if( DATA_ResPro.mode == 'MODIFY' && UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].pnrPaxCatFOIDNumber !="" && UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].pnrPaxCatFOIDNumber != trim($("#"+ i + "_pnrPaxCatFOIDNumber").val() )) {
								if(!isDuplicatePassportNumberCheckOnlyWithinAdult){
									foidLists +=trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^" ;
								}								
							} else if (DATA_ResPro.mode != 'MODIFY') {
								if(!isDuplicatePassportNumberCheckOnlyWithinAdult){
									foidLists +=trim($("#"+ i + "_pnrPaxCatFOIDNumber").val())+"^" ;
								}
							}
							if (uniqfoidCod ==""){uniqfoidCod = UI_paxDetails.fdDesc; }
							if (uniqfoid==""){uniqfoid = UI_paxDetails.fdCode; }
						}				
					}
				}			
				
				if(paxConfig.passportExpiry.xbeVisibility && 
						paxConfig.passportExpiry.xbeMandatory){
					if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){						
						if(trim($("#"+ i + "_pnrPaxCatFOIDExpiry").val()) == ""){
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Passport Expiry Date")});
							$("#"+ i + "_pnrPaxCatFOIDExpiry").focus();
							return false;
						}
					}
					
					var selDateArr = $("#"+ i + "_pnrPaxCatFOIDExpiry").val().split('/');
					var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					
					if(selDate < minDate){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Passport Expiry Date")});
						$("#"+ i + "_pnrPaxCatFOIDExpiry").focus();
						return false;
					}
					
				} else if(paxConfig.passportExpiry.xbeVisibility && 
						!paxConfig.passportExpiry.xbeMandatory){					
					if(trim($("#"+ i + "_pnrPaxCatFOIDExpiry").val()) != ""){
						var selDateArr = $("#"+ i + "_pnrPaxCatFOIDExpiry").val().split('/');
						var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
						
						if(selDate < minDate){
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Passport Expiry Date")});
							$("#"+ i + "_pnrPaxCatFOIDExpiry").focus();
							return false;
						}
					}
				}
				
				
				if(paxConfig.passportIssuedCntry.xbeVisibility &&
						paxConfig.passportIssuedCntry.xbeMandatory){
					if(trim($("#"+ i + "_pnrPaxCatFOIDPlace").val()) == "" &&
							(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A')){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Passport Place of Issue")});
						$("#"+ i + "_pnrPaxCatFOIDPlace").focus();
						return false;
					}
				}
				
				//Visa(DOCO) validation
				if ($("#"+ i + "_visaDocNumber").val()) {
					$("#"+ i + "_visaDocNumber").val(trim($("#"+ i + "_visaDocNumber").val()));						
				}			
				if(paxConfig.visaDocNumber.ssrCode != null && paxConfig.visaDocNumber.ssrCode != '' && paxConfig.visaDocNumber.ssrCode == 'DOCO'){					
					var notEmptyValidation = false;
					if(paxConfig.visaDocNumber.xbeVisibility && paxConfig.visaDocNumber.xbeMandatory && trim($("#"+ i + "_visaDocNumber").val()) == ""){
						if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
								UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){					
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Visa Doc Number")});
							$("#"+ i + "_visaDocNumber").focus();
							return false;
						}
						notEmptyValidation = true;
					} else if(paxConfig.visaDocNumber.xbeVisibility && !paxConfig.visaDocNumber.xbeMandatory &&
							trim($("#"+ i + "_visaDocNumber").val()) != ""){
						notEmptyValidation = true;
					}
					
					if(notEmptyValidation){						
						if(!isNumeric(trim($("#"+ i + "_visaDocNumber").val()))) {
							if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
									UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){					
								UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Visa Doc Number")});
								$("#"+ i + "_visaDocNumber").focus();
								return false;
							}
							
						}
						
						if($("#"+ i + "_visaDocNumber").val() != "" &&
								paxConfig.visaDocNumber.uniqueness != "NONE"){
							if( DATA_ResPro.mode == 'MODIFY' ) {
								if(!isDuplicateVisaDocNumberCheckOnlyWithinAdult){
									currFoidList += trim($("#"+ i + "_visaDocNumber").val())+"^";
								}
							}
							if( DATA_ResPro.mode == 'MODIFY' && UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].visaDocNumber !="" && UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].visaDocNumber != trim($("#"+ i + "_visaDocNumber").val() )) {
								if(!isDuplicateVisaDocNumberCheckOnlyWithinAdult){
									foidLists +=trim($("#"+ i + "_visaDocNumber").val())+"^" ;
								}								
							} else if (DATA_ResPro.mode != 'MODIFY') {
								if(!isDuplicateVisaDocNumberCheckOnlyWithinAdult){
									foidLists +=trim($("#"+ i + "_visaDocNumber").val())+"^" ;
								}
							}
							//if (uniqfoidCod ==""){uniqfoidCod = UI_paxDetails.fdDesc; }
							//if (uniqfoid==""){uniqfoid = UI_paxDetails.fdCode; }
						}				
					}
				}			
				
				if(paxConfig.visaDocIssueDate.xbeVisibility && 
						paxConfig.visaDocIssueDate.xbeMandatory){
					if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){						
						if(trim($("#"+ i + "_visaDocIssueDate").val()) == ""){
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Visa Doc Issue Date")});
							$("#"+ i + "_visaDocIssueDate").focus();
							return false;
						}
					}
					
					var selDateArr = $("#"+ i + "_visaDocIssueDate").val().split('/');
					var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					
					if(selDate > minDate){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Visa Doc Issue Date")});
						$("#"+ i + "_visaDocIssueDate").focus();
						return false;
					}
					
				} else if(paxConfig.visaDocIssueDate.xbeVisibility && 
						!paxConfig.visaDocIssueDate.xbeMandatory){					
					if(trim($("#"+ i + "_visaDocIssueDate").val()) != ""){
						var selDateArr = $("#"+ i + "_visaDocIssueDate").val().split('/');
						var selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
						
						if(selDate > minDate){
							UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "Visa Doc Issue Date")});
							$("#"+ i + "_visaDocIssueDate").focus();
							return false;
						}
					}
				}
				
				
				if(paxConfig.placeOfBirth.xbeVisibility){
					if(paxConfig.placeOfBirth.xbeMandatory && trim($("#"+ i + "_placeOfBirth").val()) == "" &&
							(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A')){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Place Of Birth")});
						$("#"+ i + "_placeOfBirth").focus();
						return false;
					} else if (trim($("#"+ i + "_placeOfBirth").val()) != "" && !isAlphaNumeric(trim($("#"+ i + "_placeOfBirth").val()))) {
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], paxConfig.placeOfBirth.fieldName)});
						$("#"+ i + "_placeOfBirth").focus();
						return false;						
					}
				}
				
				if(paxConfig.travelDocumentType.xbeVisibility &&
						paxConfig.travelDocumentType.xbeMandatory){
					if(trim($("#"+ i + "_travelDocumentType").val()) == "" &&
							(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A')){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Travel Document Type")});
						$("#"+ i + "_travelDocumentType").focus();
						return false;
					}
				}
				
				if(paxConfig.visaDocPlaceOfIssue.xbeVisibility){
					if(paxConfig.visaDocPlaceOfIssue.xbeMandatory && trim($("#"+ i + "_visaDocPlaceOfIssue").val()) == "" &&
							(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A')){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Visa Doc Place Of Issue")});
						$("#"+ i + "_visaDocPlaceOfIssue").focus();
						return false;
					} else if (trim($("#"+ i + "_visaDocPlaceOfIssue").val()) != "" && !isAlphaNumeric(trim($("#"+ i + "_visaDocPlaceOfIssue").val()))) {
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], paxConfig.visaDocPlaceOfIssue.fieldName)});
						$("#"+ i + "_visaDocPlaceOfIssue").focus();
						return false;
					}
				}
				
				if(paxConfig.visaApplicableCountry.xbeVisibility &&
						paxConfig.visaApplicableCountry.xbeMandatory){
					if(trim($("#"+ i + "_visaApplicableCountry").val()) == "" &&
							(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
							UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A')){
						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "Visa Applicable Country")});
						$("#"+ i + "_visaApplicableCountry").focus();
						return false;
					}
				}
				
				if (!DATA_ResPro.initialParams.allowSkipNic) {
					if(opener.top.isRequestNICForReservationsHavingDomesticSegments == "true"){
						if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
								UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){					
							if(isDomesticFlightExist == "true" && trim($("#"+ i + "_nationalIDNo").val()) == ""){
								UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "National ID No")});
								$("#"+ i + "_nationalIDNo").focus();
								return false;
							}
						}
					} else if(paxConfig.nationalIDNo.xbeVisibility && paxConfig.nationalIDNo.xbeMandatory){
						if(UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantFirstName != 'T B A' ||
								UI_paxDetails.jsonPaxAdtObj.paxInfants[infIntCount].displayInfantLastName != 'T B A'){					
							if(trim($("#"+ i + "_nationalIDNo").val()) == ""){
								UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-01"], "National ID No")});
								$("#"+ i + "_nationalIDNo").focus();
								return false;
							}
						}
					}
				}
				if(paxConfig.nationalIDNo.xbeVisibility){
  					if(trim($("#"+ i + "_nationalIDNo").val()) != "" && !isAlphaNumericWithhyphen(trim($("#"+ i + "_nationalIDNo").val()))) {
  						UI_message.showErrorMessage({messageText: buildError(UI_paxDetails.arrError["XBE-ERR-04"], "National ID No")});
  						$("#"+ i + "_nationalIDNo").focus();
  						return false;
  					}
				}
				
				
				infIntCount++;
			}
		}
		
		
		
		if( DATA_ResPro.mode == 'MODIFY' && currFoidList !="") {
			var isDuplicate = UI_paxDetails.validatefoidIds(currFoidList,uniqfoidCod);
			if(isDuplicate !="" && isDuplicate == "true") {
				// TODO Give error message
				//showERRMessage(raiseError("XBE-ERR-04","FOID Number"));
				//showERRMessage(buildError(UI_paxDetails.arrError["XBE-ERR-68"], uniqfoidCod));
				UI_message.showErrorMessage({messageText: buildError(
						UI_paxDetails.arrError["XBE-ERR-68"],uniqfoidCod)});

				$("#"+ i + "_pnrPaxCatFOIDNumber").focus();
				validate1 = "false";
				return false;
						
			} else {
				if (foidLists!= "" && UI_paxDetails.fdUniq == "FLIGHT" ) {
					UI_paxDetails.foidcheck1(uniqfoid,uniqfoidCod,foidLists);
				}
			}
			
		} else if ( foidLists!= "" && uniqfoidCod!=""  ) {
			var isDuplicate = UI_paxDetails.validatefoidIds(foidLists,uniqfoidCod);
			if(isDuplicate !="" && isDuplicate == "true") {
				// TODO Give error message
				//showERRMessage(raiseError("XBE-ERR-04","FOID Number"));
				//showERRMessage(buildError(UI_paxDetails.arrError["XBE-ERR-68"], uniqfoidCod));
				UI_message.showErrorMessage({messageText: buildError(
						UI_paxDetails.arrError["XBE-ERR-68"],uniqfoidCod)});
				$("#"+ i + "_pnrPaxCatFOIDNumber").focus();
				validate1 = "false";
				return false;
						
			} else if(isDuplicate !="" && isDuplicate == "false" && UI_paxDetails.fdUniq == "FLIGHT" && validate1 != "false") {
			     UI_paxDetails.foidcheck1(uniqfoid,uniqfoidCod,foidLists);
			}
		}	
		
		if(validate1 =="true") {
			return true;
		} else {
			return false;
		}		
	}

	UI_paxDetails.createFlightList = function(){
		var returnStr = '['; 
		if(UI_paxDetails.jsonFltDetails != []){
			for(var i=0;i<UI_paxDetails.jsonFltDetails.length;i++){
				if(i == (UI_paxDetails.jsonFltDetails.length-1)){
					returnStr += '{"'+UI_paxDetails.jsonFltDetails[i].airLine+'":"'+UI_paxDetails.jsonFltDetails[i].flightId+'"}]';
				} else {
					returnStr += '{"'+UI_paxDetails.jsonFltDetails[i].airLine+'":"'+UI_paxDetails.jsonFltDetails[i].flightId+'"},';
				}
			}
		}
		
		return returnStr;
	}
	
	/*
	 * Cancel
	 */
	UI_paxDetails.cancelOnclick = function(){
		if (opener.csvFileUpload && opener.$('#btnInternationalFlight').is(":visible") && opener.parseSuccessful) {
			opener.$('#btnInternationalFlight').trigger('click');
			opener.csvFileUpload = false;
		}
		window.close();
	}
	
	//function will check foid duplicate foid ids
	UI_paxDetails.validatefoidIds = function(foidIds,uniqfoidCod) {
	
		var strErrorHTML = "";
		var arr = new Array();
		arr = foidIds.split("^");
		if (arr!="") {
			var r = new Array();
			o:for(var i = 0, n = arr.length; i < n; i++)
  			{
				for(var x = 0, y = r.length; x < y; x++)
    			{
					if(r[x]==arr[i]) continue o;
   				}
   			   r[r.length] = arr[i];
  			 }
		}
		if(arr!="" && r!="") {
			if(arr.length != r.length) {
				return "true";
			}		
		}
	 	return "false";
	}
	
	UI_paxDetails.foidcheck1 = function (foidCode,foidDesc,foidIds) {

		if (foidIds !="" && foidDesc !="" && foidCode !="" ) {
			var segmentList = [];
			if (DATA_ResPro.mode == 'MODIFY') { 
				 var arrSegmentInfo =$.parseJSON(UI_paxDetails.jsonSegs);
		         for(var sl=0;sl<arrSegmentInfo.length;sl++) {
		        	 segmentList[sl] = arrSegmentInfo[sl].bookingFlightSegmentRefNumber;
		         }
				UI_paxDetails.foidIds = foidIds;
				UI_paxDetails.foidCode = foidCode;
				UI_paxDetails.segmentList = segmentList;
	        }  
			else {	
				 for(var sl=0; sl<UI_paxDetails.jsonFltDetails.length; sl++) {
					 segmentList[sl] = 	UI_paxDetails.jsonFltDetails[sl].flightSegmentRefNumber;		
				 }
				UI_paxDetails.foidIdsPass = foidIds;
				UI_paxDetails.foidCodePass = foidCode;
				UI_paxDetails.segmentListPass = segmentList;
			 }
		}
	}
	
	UI_paxDetails.fillDropFromArray = function() {
		var intLen = opener.top.arrCountry.length;
		// Eg : jsonCountry = ":;LK:Sri Lanka;AE:United Arab Emirates";
		var jsonCountry = ":";
		if (intLen > 0) {
			i = 0;
			do {
				jsonCountry += ";" + opener.top.arrCountry[i][0] + ":"
						+ opener.top.arrCountry[i][1];
				i++;
			} while (--intLen);
		}
		
		return jsonCountry;
	}
	
	UI_paxDetails.fillDocTypeDropDown = function() {
		// Eg : jsonCountry = ":;LK:Sri Lanka;AE:United Arab Emirates";
		return jsonDocType = ":;V:Visa";
	}
	
	UI_paxDetails.findCountry = function(cCode){
		var len = opener.top.arrCountry.length;
		for(var i=0;i<len;i++){
			if(opener.top.arrCountry[i][0] == cCode){
				return opener.top.arrCountry[i][1];
			}
		}
	}
	
	UI_paxDetails.findDocType = function(cCode){
		if(cCode == 'V'){
			return 'Visa';
		}
	}
	
	/*
	 * Date On Blur
	 */
	UI_paxDetails.dobOnBlur = function(objC) {
		var strDT = dateChk(objC.id);
		strDT = strDT != true ? strDT : objC.value;
		objC.value = strDT;
	}
	
	UI_paxDetails.mapCalendar = function() {
		
		var adultCount = UI_paxDetails.jsonPaxAdtObj.paxAdults.length;
		var infantCount = 0;
		if(UI_paxDetails.jsonPaxAdtObj.paxInfants != null){
			infantCount = UI_paxDetails.jsonPaxAdtObj.paxInfants.length;
		}
		var intLen = adultCount+infantCount;
		
		if(opener.UI_tabSearchFlights != null){			
			if(opener.UI_tabSearchFlights.strRetDate != null && opener.UI_tabSearchFlights.strRetDate != ""){
				var arrtDateArr = opener.UI_tabSearchFlights.strRetDate.split('/');
				minDate = new Date(arrtDateArr[2], arrtDateArr[1]-1, arrtDateArr[0]);
				minDate.setDate(minDate.getDate()+1);
			} else if(opener.UI_tabSearchFlights.strOutDate != null && opener.UI_tabSearchFlights.strOutDate != ""){
				var deptDateArr = opener.UI_tabSearchFlights.strOutDate.split('/');
				minDate = new Date(deptDateArr[2], deptDateArr[1]-1, deptDateArr[0]);
				minDate.setDate(minDate.getDate()+1);
			} 
		} else if(opener.UI_reservation != null) {
			var arrDate;
			var lastFltDetails = opener.UI_reservation.allFlightDetails[opener.UI_reservation.allFlightDetails.length-1];
			var splitDate = lastFltDetails.arrivalDate.split(' ');
			 if(splitDate.length == 2) {
				 arrDate = splitDate[1].split('/');
			 }else{
				 arrDate = lastFltDetails.arrivalDate.split('/');
			 }
			minDate = new Date(arrDate[2], arrDate[1]-1, arrDate[0]);
			minDate.setDate(minDate.getDate()+1);
		}
		
		if (intLen > 0) {
			var i = 1;
			$("#tablPane1")
				.find("tr")
					.each(
						function() {
							$(this)
								.find("input")
									.each(
										function() {
											if (this.name == "pnrPaxCatFOIDExpiry") {
												$("#" + this.id)
														.datepicker(
																{
																	minDate : minDate,
																	dateFormat : UI_commonSystem.strDTFormat,
																	changeMonth : 'true',
																	changeYear : 'true',
																	showButtonPanel : 'true'
																});
												i++;
											} else if (this.name == "visaDocIssueDate") {
														$("#" + this.id)
														.datepicker(
																{
																	//minDate : minDate,
																	maxDate: '0',
																	dateFormat : UI_commonSystem.strDTFormat,
																	changeMonth : 'true',
																	changeYear : 'true',
																	showButtonPanel : 'true'
																});
												i++;
											}
										}
									);
					   }
				  );
		}
	}
	
	UI_paxDetails.convertStringintoDateObj = function(strDate){		
		var sD = Number(strDate.substring(0,2));
		var sM = Number(strDate.substring(3,5));
		var sY = Number(strDate.substring(6,10));
		
		var date = new Date(sY, (sM-1), sD);		
		return date;
	}
	
	
	