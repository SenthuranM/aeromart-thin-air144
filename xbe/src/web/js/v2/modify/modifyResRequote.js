/*
 *********************************************************
	Description		: Modify reservation with requote
	Author			: Dilan Anuruddha
	Version			: 1.0
	Created			: 05 March 2012
 *********************************************************	
 */

function UI_modifyResRequote() {
}

UI_modifyResRequote.tabStatus = ({
    tab0 : true,
    tab1 : false,
    tab2 : false,
    tab3 : false
}); // tab Status

UI_modifyResRequote.tabDataRetrive = ({
    tab0 : false,
    tab1 : true,
    tab2 : false,
    tab3 : false
}); // Data Retrieval Status

/*
 * Onload Function
 */
$(document).ready(function() {
	$("#divLegacyRootWrapper").fadeIn("slow");
	$("#divBookingTabs").tabs();
	$('#divBookingTabs').bind('tabsbeforeactivate', function(event, ui) {
		return eval("UI_modifyResRequote.tabStatus.tab" + ui.newTab.index());
	});

	UI_commonSystem.strPGMode = DATA_ResPro.reservationInfo.mode;

	if(DATA_ResPro.initialParams.viewAvailableCredit) {
		UI_commonSystem.showAgentCredit();
		UI_commonSystem.showAgentCreditInAgentCurrency();
	} else {
		$("#divCreditPnl").hide();
	}

	// Search Flight Tab -----------------------------------------------------
	UI_tabSearchFlights.ready();
	UI_tabSearchFlights.fillRequote(DATA_ResPro.reservationInfo);
	// UI_tabSearchFlights.fillAddSegment(DATA_ResPro.reservationInfo);

	// UI_tabSearchFlights.disableEnablePageControls(false);
	UI_commonSystem.strPGID = "SC_RESV_CC_007";
});

/*
 * Open Tab from the Function Call
 */
UI_modifyResRequote.openTab = function(intIndex) {
	$('#divBookingTabs').tabs("option", "active", intIndex);
}

/*
 * Lock Tabs
 */
UI_modifyResRequote.lockTabs = function() {
	UI_modifyResRequote.tabStatus = ({
	    tab0 : true,
	    tab1 : false,
	    tab2 : false,
	    tab3 : false
	});

	UI_modifyResRequote.tabDataRetrive = ({
	    tab0 : false,
	    tab1 : true,
	    tab2 : false,
	    tab3 : false
	});
}

UI_modifyResRequote.showConfirmError = function(resMessage) {
	showERRMessage(resMessage);
}

UI_modifyResRequote.getSelectedCurrencyCode = function() {
	var currency = $.trim($("#selectedCurrency").val());
	if (currency != null && !'' == currency) {
		return currency;
	}
	return DATA_ResPro.initialParams.defaultCurrencyCode;
}

/*
 * Load add segment source page
 */
UI_modifyResRequote.loadHomePage = function() {
	UI_modifyResRequote.selCurrency = "";
	location.replace("showNewFile!loadRes.action?pnrNO="
	        + DATA_ResPro.reservationInfo.PNR + "&groupPNRNO="
	        + DATA_ResPro.reservationInfo.groupPNR);
}
/*
 * ------------------------------------------------ end of File
 * ----------------------------------------------
 */

