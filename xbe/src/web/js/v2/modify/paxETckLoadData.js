	/*
	*********************************************************
		Description		: XBE - Pax E-Ticket Mask Load Grid Data
		Author			: M.Rikaz
		Version			: 1.0
		Created			: 07/07/2011
		Last Modified	: 07/07/2011
	*********************************************************	
	*/
	
	function UI_paxETckLoadData(){}
	
	UI_paxETckLoadData.selectedCouponObj = null;	
	var hdeCpnCntrl = true;
	

	UI_paxETckLoadData.ready = function(){	
		
		UI_paxETckLoadData.constructGrid({id:"#tblPaxETckMask"});		
	}
	
	/*
	 * Charges Fill Data
	 */
	UI_paxETckLoadData.fillData = function( jsPaxETicketMask,enableCpnCtrl ){
		if(enableCpnCtrl!=undefined && enableCpnCtrl){
			hdeCpnCntrl = false;
			//delete $('#tblPaxETckMask');
			$('#tblPaxETckMask').GridUnload('#tblPaxETckMask');
			UI_paxETckLoadData.ready();
		}
		UI_commonSystem.fillGridData({id:"#tblPaxETckMask", data:jsPaxETicketMask});			
	}
	
	/*
	 * Construct Grid
	 */
	UI_paxETckLoadData.constructGrid = function(inParams){
		
		var couponControlFmt = function (arr, options, rowObject){
			if(rowObject.couponControl== undefined 
					|| rowObject.couponControl==null || $.trim(rowObject.couponControl)==""){				
				return 'N/A';								
			}
			return rowObject.couponControl;
		}
		
		//construct grid
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 280,
			width: 800,
			colNames:[getOpeneri18nData('BookingInfo_Etick_PPFSID','ppfsId'), getOpeneri18nData('BookingInfo_Etick_ID','E-ticket-Id'),getOpeneri18nData('BookingInfo_Etick_Segment','Segment'), getOpeneri18nData('BookingInfo_Etick_SegmentStatus','Segment Status'),getOpeneri18nData('BookingInfo_Etick_FlightNumber','Flight No'),getOpeneri18nData('BookingInfo_Etick_Carrier','Carrier'),getOpeneri18nData('BookingInfo_Etick_DepartureTime','Dep. Time'),getOpeneri18nData('BookingInfo_Etick_Etick','E-Ticket'),getOpeneri18nData('BookingInfo_Etick_CouponNumber','Coupon No'),getOpeneri18nData('BookingInfo_Etick_Paxstatus','Pax Status'),getOpeneri18nData('BookingInfo_Etick_TicketStatus','Ticket Status'),getOpeneri18nData('BookingInfo_Etick_CouponCtrl','Coupon Ctrl')],
			colModel:[
			    {name:'ppfsId',  index:'ppfsId', width:90, align:"center",sortable:false ,hidden:true},
			    {name:'eticketId',  index:'eticketId', width:90, align:"center",sortable:false ,hidden:true},			    
			    {name:'segmentCode',  index:'segmentCode', width:90, align:"center",sortable:false },	
				{name:'segmentStatus', index:'segmentStatus', width:90, align:"center",sortable:false },
				{name:'flightNo',  index:'flightNo', width:90, align:"center",sortable:false },
				{name:'carrierCode',  index:'carrierCode', width:90, align:"center",sortable:false },
				{name:'departureTime',  index:'departureTime', width:90, align:"center",sortable:false },
				{name:'paxETicketNo', index:'paxETicketNo', width:100, align:"center",sortable:false },
				{name:'couponNo', index:'couponNo', width:80, align:"center",sortable:false} ,
				{name:'paxStatus', index:'paxStatus', width:80, align:"center",sortable:false} ,
				{name:'paxETicketStatus', index:'paxETicketStatus', width:90, align:"center",sortable:false },
				{name:'couponControl', index:'couponControl', width:90, align:"center", formatter : couponControlFmt,sortable:false,hidden:hdeCpnCntrl}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				UI_paxETckLoadData.selectedCouponObj = $("#tblPaxETckMask").getRowData(rowid);
				UI_paxETckMask.setSelectedCouponDetail();
			}
		});
	}
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
