/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Description		: XBE - Pax Account Details - Payments
 * Author			: Rilwan A. Latiff
 * Created			: 25th August 2008
 * 
 * ===============================================================================
 */

	var jsonPaymentObj = [
					{id:"#txtPayAmount", desc:"Amount", required:true},
					{id:"#txtPayUN", desc:"User Notes", required:true}	
			     ];
	
	var jsPaymentGrid = null;

	var jsSelCCPmtAuthId = null;
	
	var jsSelPayAmt = null;
	
	//Holds the refund type order. (TAX if tax is automatically calculated)
	var refundTypeOrder=null;
	
	//Holds the pnrPaxOndChgIDs used to differenciate between segment payments when refunding
	var pnrPaxOndChgIDs=null;
	
	var jsSelPayCarrierCode = null;
	
	var jsSelPayCurrency = null;
	
	var jsSelPayRef = null;
	
	var jsSelPayDate = null;
	
	var jsSelPayRecieptNo = null;
	
	var jsSelPrintRecieptEnabled = null;
	
	var jsSelCarrierVisePayments = null;
	
	var jsSelLccUniqueTnxId = null;
	
	var jsSelPaymentMethod = null;
	
	var isOfflinePayment = false;
	
	var ccPayData = {};
	
	UI_paxAccountPayment.pmtdate = null;
	UI_paxAccountPayment.paymentCarrier = null;
	
	
	UI_paxAccountPayment.refundableCHDetails = opener.UI_reservation.refundableChargeDetails;
	var jssupportOnlyFullRefund = false;
	
	var isNoShowRefundPossible = false;
	
	var isPaymentNonRefundable = false;

	function UI_paxAccountPayment(){}
	
	/*
	 * Payment ready method
	 */
	UI_paxAccountPayment.ready = function(){
		// Payment Tab
		$("#btnPayClose").decorateButton();
		$("#btnPayClose").click(function(){UI_paxAccountPayment.closeOnclick();});
		
		$("#btnPayConfirm").decorateButton();
		$("#btnPayConfirm").click(function(){UI_paxAccountPayment.payConfirmOnclick();});
		
		$("#btnPayReset").decorateButton();
		$("#btnPayReset").click(function(){UI_paxAccountPayment.resetOnclick();});

		$("#btnCRConfirm").decorateButton();
		$("#btnCRConfirm").click(function(){UI_paxAccountPayment.payCCConfirmOnclick();});		
			
		$("#btnCRReset").decorateButton();
		$("#btnCRReset").click(function(){UI_paxAccountPayment.resetCCOnClick();});
		
//		$("#btnCalculateTaxInfo").decorateButton();
//		$("#btnCalculateTaxInfo").click(function(){
//			UI_paxAccountPayment.loadNoShowSegments();
//			$("#taxSegmentDetails").show();
//		});
		
		$("#selectedSegment").change(function(){UI_paxAccountPayment.calculateRefundableTax();});
		$("#txtPayAmount").change(function(){
			refundTypeOrder=null; //Make tax calculations invalid since user manually changed it
			pnrPaxOndChgIDs=null; //Same for pnrPaxOngChgIds
		});
		
		$("#btnCRCancel").decorateButton();
		$("#btnCRCancel").click(function(){UI_paxAccountPayment.cancelCROnClick();});
		
		$("#radPaymentTypeC").click(function(){UI_paxAccountPayment.paymentTypeCClick();});
		$("#radPaymentTypeA").click(function(){UI_paxAccountPayment.paymentTypeAClick();});
		$("#radPaymentTypeR").click(function(){UI_paxAccountPayment.paymentTypeRClick();});
		$("#radPaymentTypeOL").click(function(){UI_paxAccountPayment.paymentTypeOLClick();});
		$("#radPaymentTypeBsp").click(function(){UI_paxAccountPayment.paymentTypeBspClick();});
		$("#selAgents").change(function(){UI_paxAccountPayment.agentselected();});
		
		$("#creditCardDet").hide();
		$("#txtPayCurrency").disable();
		$("#txtPayCurrency").hide();//to be compatible with dry refund
		$("#btnRecieptPrint").hide(); //Hide Print Reciept Button
		
		$('#txtPayAmount').keyup(function() {UI_paxAccountPayment.insertPayAmount();});
		$('#txtPayUN').keyup(function(){UI_paxAccountPayment.removeControlCharacters();});
		
		$("#btnRecieptPrint").click(function(){UI_paxAccountPayment.printReciept(opener.jsonPNR, jsSelPayRecieptNo, jsSelPayRef);});
		
		if(!cashRefundAllowed){
			$("#trCA").hide();
		}

		if(!creditCardRefundAllowed){
			$("#trCC").hide();
		}
		
		if(!onAccountRefundAllowed){
			$("#trOA").hide();
		}
		
		if(!bspRefundAllowed){			
			$("#trBSP").hide();					
		}
		
		if(!offlineRefundAllowed){
			$("#trOL").hide();
		}
		
		if(!(anyOnAccountRefundAllowed || onAccountReportingRefundAllowed || anyOpCarrierOnAccountRefundAllowed)){
			$("#selAgents").hide();
		}
		
		if(!cashRefundAllowed && !creditCardRefundAllowed && !onAccountRefundAllowed && !bspRefundAllowed && !offlineRefundAllowed){
			$("#btnPayConfirm").hide();
			$("#btnPayReset").hide();
			$("#trRefundTxt").hide();
			$("#trPayOptionInputs").hide();
			$("#trPayOptionSelections").hide();
		}
		
		//Enable lcc refund 
		if(opener.jsonGroupPNR!=null && opener.jsonGroupPNR!= '' && opener.jsonGroupPNR.length >0){
			//$("#btnPayConfirm").disable(); //disableing refund for interline for the moment
		}
		
		UI_paxAccountPayment.constructGrid({id:"#tblPayments"});
		//UI_paxAccountPayment.fillData();
		if(!opener.DATA_ResPro.initialParams.allowRefund){			
			$("#payOptions").hide();
			$("#trRefundTxt").hide();			
		}				
		if((!opener.DATA_ResPro.initialParams.allowOnAccountRefund) && (!opener.DATA_ResPro.initialParams.allowAnyOnAccountRefund)){			
			$("#trOA").hide();					
		}
		if((!opener.DATA_ResPro.initialParams.allowCreditCardRefund) && (!opener.DATA_ResPro.initialParams.allowAnyCreditCardRefund)){			
			$("#trCC").hide();					
		}
		if(!opener.DATA_ResPro.initialParams.allowOfflineRefund){
			$("#trOL").hide();
		}
		
		//display full refundable notice if the reservation has been marked as void.
		if(isVoidReservation){
			$("#voidReservationRefundableNotice").show();	
		}
		
		if(opener.DATA_ResPro.initialParams.allowKeepCommissionOnRefund){
			$("#agentCommissionInfo").show();
		}
		
//		if($('#btnCalculateTaxInfo').length > 0){
//			UI_paxAccountPayment.loadNoShowSegments();
//		}
	}
	
	/*
	 * Payment Fill Data
	 */
	UI_paxAccountPayment.fillData = function(){
		UI_commonSystem.fillGridData({id:"#tblPayments", data:jsPaymentDetails.paymentList});
		
		$("#divPayTotal").html(jsPaymentDetails.totalPayment + " " + jsPaymentDetails.currency);
		$("#divPayBalance").html(jsPaymentDetails.balance + " " + jsPaymentDetails.currency);
		$("#divActualPayCredit").html(jsPaymentDetails.actualCredit + " " + jsPaymentDetails.currency);
		//$("#divPayTotalCurr").html(jsPaymentDetails.currency);
		if ((opener.jsonBkgInfo.useEtsForReservation && !opener.jsonBkgInfo.status == "CNX")) {

		$("#btnPayConfirm").hide();
		$("#btnPayReset").hide();

	}
	}
	
	/*
	 * Mode of Payment Selection
	 */
	UI_paxAccountPayment.paymentTypeCClick = function(){			
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}				
		$("#selCardType").val("");			
		$("#selAgents").hide();
		$("#selCardType").hide();
		$("#lblAgentCurrency").html("");
		UI_paxAccountPayment.resetCCOnClick();
	}
	
	UI_paxAccountPayment.paymentTypeOLClick = function(){							  
		$("#selAgents").show();
		$("#selCardType").hide();				
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}
		UI_paxAccountPayment.resetCCOnClick();
	}
	
	UI_paxAccountPayment.paymentTypeAClick = function(){							  
		$("#selAgents").show();
		$("#selCardType").hide();				
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}
		UI_paxAccountPayment.resetCCOnClick();
	}
	
	UI_paxAccountPayment.paymentTypeRClick = function(){		
		$("#selAgents").hide();
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}
		$("#lblAgentCurrency").html("");
	}
	UI_paxAccountPayment.paymentTypeBspClick = function(){			
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}				
		$("#selCardType").val("");			
		$("#selAgents").hide();
		$("#selCardType").hide();
		$("#lblAgentCurrency").html("");
		UI_paxAccountPayment.resetCCOnClick();
	}
	
	UI_paxAccountPayment.agentselected = function() {		
		if($("#selAgents").val() != "") {
//			var selectedAgentCurrCode = "";
//			var arrAgentInfo;
//			//TODO  move this- accessing top  to a common method
//			for( var i=0; i < top.arrTravelAgents.length; i++) {
//				arrAgentInfo = top.arrTravelAgents[i];
//				if($("#selAgents").val() == arrAgentInfo[1]) {
//					selectedAgentCurrCode = arrAgentInfo[2];
//					break;
//				}				
//			}
//			if(arrAgentInfo != null && arrAgentInfo != "") {
//				$("#lblAgentCurrency").html("<b>Your payment will be processed in "+ selectedAgentCurrCode +"</b>");
//			}else {
//				$("#lblAgentCurrency").html("");
//			}			
			
		}else {
			$("#lblAgentCurrency").html("");
		}
	}
	
	/*
	 * reset on Click
	 */
	UI_paxAccountPayment.resetCCOnClick = function(){			
		$("#cardType").val("");
		$("#cardNo").val("");
		$("#cardHoldersName").val("");
		$("#expiryDate").val("");
		$("#cardCVV").val("");
	}

	/*
	 * Cancel Click
	 */
	UI_paxAccountPayment.cancelCROnClick = function(){
		UI_paxAccountPayment.resetCCOnClick();	
		$("#cardType").children().remove();
		$("#payOptions").show();
		$("#creditCardDet").hide();
		$("#payOptButtons").show();
		$("#divDisableLayer").hide();
		$("#btnPayConfirm").disableEnable(false);
		carrierCode = "";
		spMsgCC = "";
		spMsgCC2 = "";
		arrError = new Array();
		$("#btnPayConfirm").disableEnable(false);
	}
	
	/*
	 * Charges Validate
	 */
	UI_paxAccountPayment.validatePayment = function(){
		UI_message.initializeMessage();
		
		var rowId = null;
		var arrPaymentList = null; 
		var payAmount = null;
		var actualCredit = null;
		ccPayData["isFullRefundOperation"] = false;
		
		/* Invalid Character Validations */
		if (!UI_commonSystem.validateInvalidCharWindow(jsonPaymentObj)){return false;}
		
		if (!UI_commonSystem.validateMandatoryWindow(jsonPaymentObj)){return false;}
		
		/* At the moment refund can be done form only payment carrier */
		if(UI_paxAccountPayment.paymentCarrier != opener.top.carrierCode) {
			UI_message.showErrorMessage({messageText:"Select payment from own carrier."});
			return false;
		}else{
			rowId = $("#tblPayments").getGridParam('selrow');
			arrPaymentList = jsPaymentDetails.paymentList; 
			payAmount = arrPaymentList[rowId-1].hdnPayAmount;
			if(jsPaymentDetails.actualCredit!=null && jsPaymentDetails.actualCredit!=""){
				actualCredit = jsPaymentDetails.actualCredit * -1;
			}			
		}
		
		/* User should select a payment fro refund*/
		if(jsSelPayRef == null || jsSelPayRef == "" || (jsSelPayRef != null && jsSelPayAmt < 0)){
			UI_message.showErrorMessage({messageText:"Select payment that can be refunded to refund."});
			return false;
		}
		
		/* Validate non refundable payment*/
		if(isPaymentNonRefundable){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-98","")});
			return false;
		}		
		
					
		switch ($("input[name='radPaymentType']:checked").val()){
			case UI_commonSystem.strAccCode :
				if ($("#selAgents").val() == "" && anyOnAccountRefundAllowed){
					UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-01","Agent")});
					$("#selAgents").focus();
					return false;
				}
				break;
				
			case UI_commonSystem.strCCCode :
				if ($("#selCardType").val() == ""){
					UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-02","Card")});
					$("#selCardType").focus();
					return false;
				}					  
				break;
			default:
				break;
		}
		
		if(noCreditRefundAllowed){
			jsSelPayAmt = Number(jsPaymentDetails.totalPayment);
		}
	
		if ($("#txtPayAmount").val() != ""){
			if (!jssupportOnlyFullRefund && !UI_commonSystem.numberValidateWindow({id:"#txtPayAmount", desc:"Amount", minValue:0, blnDecimal:true , maxDecimal:2, maxValue:jsSelPayAmt})){return false;}
			//Refund Amount should be within Actual Credit
			if(actualCredit!=null && !UI_commonSystem.numberValidateWindow({id:"#txtPayAmount", desc:"Amount, Exceeds Actual Credit Amount", minValue:0, blnDecimal:true , maxDecimal:2, maxValue:actualCredit})){return false;}
		}
		
		if($("#txtPayUN").val() ==""){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-01","User Notes")});
			return false;
		}

		if($("#txtPayUN").val().length > 255){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-18","User Notes")});
			return false;
		}
		
		var refundMethod = $("input[name='radPaymentType']:checked").val();
		if(jsSelPaymentMethod  == "CRED" && refundMethod == "CRED"){
			if((opener.jsonPaxAdtObj.paxAdults.length > 1) && (jssupportOnlyFullRefund) ||
					($("#txtPayAmount").val() != payAmount) && (opener.jsonPaxAdtObj.paxAdults.length == 1) && (jssupportOnlyFullRefund)){
				if(confirm("Partial refunds are not allowed.Do you need to refund full amount?")){
					ccPayData["isFullRefundOperation"] = "true";
				}else{
					return false;
				}
			}	
		}else if(parseFloat($("#txtPayAmount").val()) > parseFloat(payAmount)){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-81","")});
			return false;
		}
		
		return true;
	}

	/*
	 * Credit Card Payment
	 */
	UI_paxAccountPayment.creditCardPayment = function(){	
		var data = {};
		data["selAuthId"] = jsSelCCPmtAuthId;
		data["hdnMode"] = "REFUND";
		data["selRefundAmount"] = $("#txtPayAmount").val();	
		data["carrierCode"]	 = jsSelPayCarrierCode;
		data["referenceNo"] = jsSelPayRef;
		data['pmtdate'] = UI_paxAccountPayment.pmtdate;
		data['selectedFareType'] = ''; //TODO check
		data['groupPNR']  = opener.jsonGroupPNR;
		data['paymentCarrier'] = UI_paxAccountPayment.paymentCarrier;
		data['carrierVisePayments'] = jsSelCarrierVisePayments;
		data["isFullRefundOperation"] = ccPayData["isFullRefundOperation"];
		$("#frmPaxAccount").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"creditCardPayment.action",							
							success: UI_paxAccountPayment.applyCreditCardSuccess,error:UI_commonSystem.setErrorStatus});
	}

	UI_paxAccountPayment.applyCreditCardSuccess = function(response){			
		if (response != null){
			if(response.success && response.hdnMode == "REFUND"){
				UI_commonSystem.hideProgress();
				$("#payOptions").hide();
				$("#payOptButtons").hide();
				if(ccPayData["isFullRefundOperation"] == "true"){	
					$("#creditCardDet").hide();
				}else{
				$("#creditCardDet").show();
				}
			
				$("#divDisableLayer").show();
				
				$("#selCardType").disable();
				$("#divTxtCurr").disable();
				$("#cardNo").disable();
				$("#cardHoldersName").disable();
				$("#expiryDate").disable();				
				$("#cardCVV").disable();
				$("#cardType").children().remove();
				var arr = opener.top.mapCCTypeForPgw[response.hdnIPGID];
				arr = (arr==null?[]:arr);
				$("#cardType").fillDropDown( { dataArray: arr, keyIndex:0 , valueIndex:1, firstEmpty:true});				
				ccPayData["hdnIPGId"] = response.hdnIPGID;
				ccPayData["cardNo"] = response.cardNumber;
				ccPayData["selAuthId"] = jsSelCCPmtAuthId;
				ccPayData["tempPayId"] = response.tempPayId;
				ccPayData["selCardType"] = response.cardType;
				ccPayData["hdnSelCurrency"] = response.currencyInfo.currencyCode;						
				ccPayData["cardHoldersName"] = response.cardHolderName;				
				ccPayData["hdnMode"] = "REFUND";
				ccPayData["payRef"] = jsSelPayRef;
				ccPayData["resContactInfo"] = opener.UI_reservation.contactInfo;
				ccPayData['version']  = opener.jsonBkgInfo.version;
				ccPayData['cccdId'] = response.ccdID;
				ccPayData['pnr']  = opener.jsonPNR;
				ccPayData['resPaxs']  = opener.UI_reservation.jsonPassengers;
				ccPayData['groupPNR']  = opener.jsonGroupPNR;
				ccPayData['pmtdate'] = UI_paxAccountPayment.pmtdate;
				ccPayData['ownerAgentCode'] = opener.UI_reservation.ownerAgent;
				ccPayData['carrierVisePayments'] = jsSelCarrierVisePayments;
				ccPayData['lccUniqueTnxId'] = jsSelLccUniqueTnxId;
				ccPayData['paymentCarrier'] = UI_paxAccountPayment.paymentCarrier;
				ccPayData['resOnds']  = opener.UI_reservation.jsonONDs;
				ccPayData['isFullRefundOperation'] = response.isFullRefundOperation;
				ccPayData['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
				
				$("#cardNo").val(response.cardNumber);
				$("#cardHoldersName").val(response.cardHolderName);
				$("#expiryDate").val(response.cardExpiryDate);
				$("#cardCVV").val(response.cardCVV);
				$("#cardType").val(response.cardType);
				carrierCode = response.carrierCode;					
				response.strCardErrors;
				
				// disabled card type since it is not possible to refund to other cards.
				$("#cardType").disable();
//				if(!response.allowAnyCardRefund) {
//					$("#cardType").disable();
//				}
					
				if(!response.showCardDetails){
					$("#trCardEDate").hide();
					$("#trCardCVV").hide();					
				}
				UI_paxAccountPayment.buildCreditBalanceToPay(response);
				$("#cardType").focus();
				
			}else {
				UI_commonSystem.hideProgress();
				UI_message.showErrorMessage({messageText:response.messageTxt});
				$("#btnPayConfirm").disableEnable(false);
				return false;
			}			
			
			if(ccPayData["isFullRefundOperation"] == "true"){
				UI_paxAccountPayment.payCCConfirmOnclick();
			}
			if(isOfflinePayment){
				$("#creditCardDet").hide();
				ccPayData["resContactInfo"] = opener.UI_reservation.contactInfo;
				ccPayData["payment.type"] = "OFFLINE";
				UI_paxAccountPayment.payCCConfirmOnclick();
			}
		}	
	}
	
	/*
	 * Credit Payment
	 */
	UI_paxAccountPayment.payCCConfirmOnclick = function(){
		UI_commonSystem.showProgress();		
		$("#frmPaxAccount").ajaxSubmit({dataType: 'json', processData: false, data:ccPayData, url:"paxAccountPaymentConfirm.action",							
							success: UI_paxAccountPayment.confirmPaymentSuccess,error:UI_commonSystem.setErrorStatus});
	}
	
	UI_paxAccountPayment.paymentTypeOffline = function(){
		UI_commonSystem.showProgress();	
		var data = {};
		data["selAuthId"] = jsSelCCPmtAuthId;
		data["hdnMode"] = "REFUND";
		data["selRefundAmount"] = $("#txtPayAmount").val();	
		data["carrierCode"]	 = jsSelPayCarrierCode;
		data["referenceNo"] = jsSelPayRef;
		data['pmtdate'] = UI_paxAccountPayment.pmtdate;
		data['selectedFareType'] = ''; //TODO check
		data['groupPNR']  = opener.jsonGroupPNR;
		data['paymentCarrier'] = UI_paxAccountPayment.paymentCarrier;
		data['carrierVisePayments'] = jsSelCarrierVisePayments;
		isOfflinePayment = true;
		$("#frmPaxAccount").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"creditCardPayment.action",							
							success: UI_paxAccountPayment.applyCreditCardSuccess,error:UI_commonSystem.setErrorStatus});
	}

	/*
	 * Credit Payment
	 */
	UI_paxAccountPayment.buildCreditBalanceToPay = function(response){		
		if(response.currencyCode != null && response.currencyCode != '' && response.currencyCode != response.currencyInfo.currencyCode )
			$("#divTotPay").html(response.totalPayAmountWithccCharges + " " + response.currencyCode+"(Approx. Payment Amount <font color='red'>["+ response.totalPayAmountWithccChargesSelcur +" "+ response.currencyInfo.currencyCode + " ]</font>");
		else
			$("#divTotPay").html(response.totalPayAmountWithccCharges + " " + response.currencyInfo.currencyCode);
		
		$("#divTxtCurr").html(" "+ response.currencyInfo.currencyCode);
	}
	
	/*
	 * User Notes Close
	 */
	UI_paxAccountPayment.closeOnclick = function(){
		UI_paxAccountDetails.close();
	}
	
	/*
	 * Payment confirm Click
	 */
	UI_paxAccountPayment.payConfirmOnclick = function(){
		if(UI_paxAccountPayment.validatePayment()){
			
			var balance = -1 * jsPaymentDetails.balance;
			var refundAmount = $("#txtPayAmount").val();
			var refundMethod = $("input[name='radPaymentType']:checked").val();
			
			if(!anyCarrierRefundAllowed && jsSelPayCarrierCode != currentCarrierCode){
				//alert("Insufficient priviledges: Refund allowed for own carrier payments.");
				UI_message.showErrorMessage({messageText:"Insufficient priviledges: Refund allowed for own carrier payments."});
				UI_paxAccountPayment.resetOnclick();
				return false;
			}
			
			if(!(isNoShowRefundPossible || noCreditRefundAllowed) && balance <= 0){
				//alert("Insufficient priviledges: Refund not allowed without credit.");
				UI_message.showErrorMessage({messageText:"Insufficient priviledges: Refund not allowed without credit."});
				UI_paxAccountPayment.resetOnclick();
				return false;
			} else if((!(isNoShowRefundPossible || noCreditRefundAllowed) || (!noCreditRefundAllowed && stopRefundTotalAmountWhenNoshowRulesApplied))
					&& balance > 0 && refundAmount > balance){
				//alert("Insufficient priviledges: Refund allowed upto credit amount only.");
				UI_message.showErrorMessage({messageText:"Insufficient priviledges: Refund allowed upto credit amount only."});
				UI_paxAccountPayment.resetOnclick();
				return false;
			}else if(refundMethod == null){
				UI_message.showErrorMessage({messageText:"Select refund method."});		
				return false;
			} 
			
			UI_commonSystem.showProgress();
			$("#btnPayConfirm").disableEnable(true);
			var data = {};
			data['refundTypeOrder'] = refundTypeOrder;
			data['pnrPaxOndChgIDs'] = JSON.stringify(pnrPaxOndChgIDs);
			data['taxRefundedSegment'] = $("#selectedSegment").val();
			data['doAdjustmentOnly'] = $("#chkOnlyAdjust").is(":checked");
			data['removeAgentCommission'] = $("#chkAgentCom").is(":checked");
			data["nonRefundable"] = isPaymentNonRefundable;
			
			switch (refundMethod){
			
				case UI_commonSystem.strCashCode : 
					data['groupPNR']  = opener.jsonGroupPNR;
					data['pnr']  = opener.jsonPNR;
					data['version']  = opener.jsonBkgInfo.version;
					data['resOnds']  = opener.UI_reservation.jsonONDs;
					data['resPaxs']  = opener.UI_reservation.jsonPassengers;					
					data["resContactInfo"] = opener.UI_reservation.contactInfo;
					data["lastUserNote"] = opener.jsonUserNotes;
					data['pmtdate'] = UI_paxAccountPayment.pmtdate;
					data['paymentCarrier'] = UI_paxAccountPayment.paymentCarrier;
					data['ownerAgentCode'] = opener.UI_reservation.ownerAgent;
					data['carrierVisePayments'] = jsSelCarrierVisePayments;
					data['lccUniqueTnxId'] = jsSelLccUniqueTnxId;
					data["payRef"] = jsSelPayRef;
					data["payDateTime"] = jsSelPayDate;
					data["isFullRefundOperation"] = ccPayData["isFullRefundOperation"];
					data["hdnMode"] = "REFUND";
					data['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
					$("#frmPaxAccount").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"paxAccountPaymentConfirm.action",							
							success: UI_paxAccountPayment.confirmPaymentSuccess,error:UI_commonSystem.setErrorStatus});					
					break;
					
				case UI_commonSystem.strAccCode :			
					data['groupPNR']  = opener.jsonGroupPNR;
					data['pnr']  = opener.jsonPNR;
					data['version']  = opener.jsonBkgInfo.version;
					data['resOnds']  = opener.UI_reservation.jsonONDs;
					data['resPaxs']  = opener.UI_reservation.jsonPassengers;					
					data["resContactInfo"] = opener.UI_reservation.contactInfo;
					data["lastUserNote"] = opener.jsonUserNotes;
					data['pmtdate'] = UI_paxAccountPayment.pmtdate;
					data['paymentCarrier'] = UI_paxAccountPayment.paymentCarrier;
					data['ownerAgentCode'] = opener.UI_reservation.ownerAgent;
					data['carrierVisePayments'] = jsSelCarrierVisePayments;
					data['lccUniqueTnxId'] = jsSelLccUniqueTnxId;
					data["payRef"] = jsSelPayRef;
					data["payDateTime"] = jsSelPayDate;
					data["isFullRefundOperation"] = ccPayData["isFullRefundOperation"];
					data["hdnMode"] = "REFUND";			
					data['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
					$("#frmPaxAccount").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"paxAccountPaymentConfirm.action",							
							success: UI_paxAccountPayment.confirmPaymentSuccess,error:UI_commonSystem.setErrorStatus});	
					break;
				case UI_commonSystem.strBspCode :			
					data['groupPNR']  = opener.jsonGroupPNR;
					data['pnr']  = opener.jsonPNR;
					data['version']  = opener.jsonBkgInfo.version;
					data['resOnds']  = opener.UI_reservation.jsonONDs;
					data['resPaxs']  = opener.UI_reservation.jsonPassengers;					
					data["resContactInfo"] = opener.UI_reservation.contactInfo;
					data["lastUserNote"] = opener.jsonUserNotes;
					data['pmtdate'] = UI_paxAccountPayment.pmtdate;
					data['paymentCarrier'] = UI_paxAccountPayment.paymentCarrier;
					data['ownerAgentCode'] = opener.UI_reservation.ownerAgent;
					data['carrierVisePayments'] = jsSelCarrierVisePayments;
					data['lccUniqueTnxId'] = jsSelLccUniqueTnxId;
					data["payRef"] = jsSelPayRef;
					data["payDateTime"] = jsSelPayDate;
					data["isFullRefundOperation"] = ccPayData["isFullRefundOperation"];
					data["hdnMode"] = "REFUND";		
					data['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
					$("#frmPaxAccount").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"paxAccountPaymentConfirm.action",					
							success: UI_paxAccountPayment.confirmPaymentSuccess,error:UI_commonSystem.setErrorStatus});	
					break;
				case UI_commonSystem.strCCCode :
					UI_paxAccountPayment.creditCardPayment();	
					break;
				case UI_commonSystem.strOfflineCode :
					UI_paxAccountPayment.paymentTypeOffline();
					break;
				default:
					alert("Select refund method.");				   
			}
		}
	}
	

	/*
	 * Reset On Click
	 */
	UI_paxAccountPayment.resetOnclick = function(){
		
		$("#btnPayConfirm").disableEnable(false);
		$("#payOptions").show();
		$("#payOptButtons").show();
		$("#creditCardDet").hide();
		$("#divDisableLayer").hide();
		$("#selAgents").show();
		$("#taxSegmentDetails").hide();
		
		refundTypeOrder=null;
		pnrPaxOndChgIDs=null;
		$('#txtPayAmount').removeAttr('readonly');
		$("#selectedSegment").empty();
		//To make the change event fire. Need a blank option as first option
		$("#selectedSegment").append('<option value=""></option>');
		
		//$("#tblPayments").jqGrid('resetSelection');// this is not working resetting is not working 
		$("#tblPayments").find("tr").removeClass("ui-state-highlight");
		jsSelCCPmtAuthId = null;
		jsSelPayAmt = null;
		jsSelPayRef = null;
		jsSelPayDate = null;
		jsSelCarrierVisePayments = null;
		jsSelLccUniqueTnxId = null;
		$("#frmPaxAccount").reset();
		//UI_paxAccountPayment.ready();		
	}
	
	/*
	 * Payment Confirm Success
	 */
	UI_paxAccountPayment.confirmPaymentSuccess = function(response){
		UI_paxAccountPayment.resetOnclick();
		
		if(!response.success){
			UI_message.showErrorMessage({messageText:response.messageTxt});
			UI_commonSystem.hideProgress();
			$("#selPax").val(strPaxID);			
		}else{
			UI_message.showConfirmationMessage({messageText:response.messageTxt});			
			UI_paxAccountPayment.closeOnclick();
			/* If we used other carrier pax credit in own reseravation then after the payments stage 
			 * the reservation needs to load as a group reservaion. This information will only be
			 * available after the payments is done. 
			 */
			if (response.convertedToGroupReservation){
				opener.jsonGroupPNR = response.groupPNR;  
			}
			opener.UI_reservation.loadReservation("", null,false);
		}
	}
	
	UI_paxAccountPayment.confirmPaymentSuccessUpdate = function(response){
		UI_paxAccountPayment.resetOnclick();
		var jsonPaymentDetails = response.paymentsTO;
		UI_commonSystem.fillGridData({id:"#tblPayments", data:jsonPaymentDetails.paymentList});
		$("#divPayTotal").html(jsonPaymentDetails.totalPayment + " " + jsonPaymentDetails.currency);
		$("#divPayBalance").html(jsonPaymentDetails.balance + " " + jsonPaymentDetails.currency);
		$("#divActualPayCredit").html(jsPaymentDetails.actualCredit + " " + jsPaymentDetails.currency);
		UI_commonSystem.hideProgress();		
		$("#selPax").val(strPaxID);
	}
	
	/*
	 * Construct Grid
	 */
	UI_paxAccountPayment.constructGrid = function(inParams){
		//construct grid
		jsPaymentGrid = $(inParams.id).jqGrid({
			datatype: "local",
			height: 230,
			width: 800,
			colNames:[getOpeneri18nData('PaxAccDetails_Date','Date'),getOpeneri18nData('PaxAccDetails_Amount','Amount'),getOpeneri18nData('PaxAccDetails_PayAmount','Pay Amount'),getOpeneri18nData('PaxAccDetails_Methods','Method'),'Reciept Number',getOpeneri18nData('PaxAccDetails_Notes','Notes'),getOpeneri18nData('PaxAccDetails_CarrierCode','Carrier'), 'HdnAuthId', 'HdnPayAmt', 'HdnPaidCurrency','HdnPayRef' ,'hdnPaydate','EnableRecieptPrint' , 'HdnPaidAirLineCurrency','HdnCarrierVisePayments','HdnLccUniqueTnxId', 'paymentMethod', 'supportOnlyFullRefund','nonRefundable'],
			colModel:[
					{name:'displayDate', index:'displayDate', width:100, align:"center"},
					{name:'displayAmount', index:'displayAmount', width:80, align:"right"},
					{name:'displayPayAmount', index:'displayPayAmount', width:80, align:"right"},
					{name:'displayMethod',  index:'displayMethod', width:200, align:"center"},
					{name:'recieptNumber', index:'recieptNumber', width:80, align:"left", hidden: !allowRecieptPrinting},
					{name:'displayNotes', index:'displayNotes', width:180, align:"left"},
					{name:'displayCarrierCode', index:'displayCarrierCode', width:40, align:"left"},
					{name:'hdnAuthorizationId', index:'hdnAuthorizationId', align:"left", hidden:true},
					{name:'hdnPayAmount', index:'hdnPayAmount', align:"left", hidden:true},
					{name:'hdnPaidCurrency', index:'hdnPaidCurrency', align:"left", hidden:true},
					{name:'hdnPayRef', index:'hdnPayRef', align:"left", hidden:true},
					{name:'hdnPaydate', index:'hdnPaydate', align:"left", hidden:true},
					{name:'enableRecieptPrint', index:'enableRecieptPrint', align:"left", hidden:true},
					{name:'hdnPaidAirLineCurrency', index:'hdnPaidAirLineCurrency', align:"left", hidden:true},
					{name:'hdnCarrierVisePayments', index:'hdnCarrierVisePayments', align:"left", hidden:true},
					{name:'hdnLccUniqueTnxId', index:'hdnLccUniqueTnxId', align:"left", hidden:true},
					{name:'paymentMethod', index:'paymentMethod', align:"left", hidden:true},
					{name:'supportOnlyFullRefund',index:'supportOnlyFullRefund',align:"left",hidden:true},
					{name:'nonRefundable', index:'nonRefundable', align:"left", hidden:true}
				],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowId){
				var rowData = $('#'+this.id).getRowData(rowId); 
				jsSelCCPmtAuthId = rowData['hdnAuthorizationId'];
				jsSelPayAmt = parseFloat(rowData['hdnPayAmount']);
				jsSelPayCarrierCode = rowData['displayCarrierCode'];
				jsSelPayCurrency = rowData['hdnPaidAirLineCurrency'];
				jsSelPayRef = rowData['hdnPayRef'];
				jsSelPayDate = rowData['hdnPaydate'];
				jsSelPayRecieptNo = rowData['recieptNumber'];
				jsSelPrintRecieptEnabled = rowData['enableRecieptPrint'];
				jsSelCarrierVisePayments = rowData['hdnCarrierVisePayments'];
				jsSelLccUniqueTnxId = rowData['hdnLccUniqueTnxId'];
				jsSelPaymentMethod = rowData['paymentMethod'];
				jssupportOnlyFullRefund = (rowData['supportOnlyFullRefund'] == "true");
				isPaymentNonRefundable = (rowData['nonRefundable'] == "true");
				UI_paxAccountPayment.pmtdate = rowData['displayDate'];
				UI_paxAccountPayment.paymentCarrier = rowData['displayCarrierCode'];				
				UI_paxAccountPayment.paymentRefundOnClick();
				UI_paxAccountPayment.enableRefundPaymentOptions();
			}
		});
	}
	
	UI_paxAccountPayment.paymentRefundOnClick = function(){
		
		$("#txtPayCurrency").val(jsSelPayCurrency);		
		if(allowRecieptPrinting 
				&& jsSelPayRecieptNo != null 
				&& jsSelPayRecieptNo != ""
				&& jsSelPrintRecieptEnabled == 'Y'){
			$("#btnRecieptPrint").show();
			$("#btnRecieptPrint").decorateButton();						
		} else {
			$("#btnRecieptPrint").hide();
		}
	}
	/*
	 * enable disable refund options
	 */
	UI_paxAccountPayment.enableRefundPaymentOptions = function()
	{
		// unchecked all payment options		
		$("input[name='radPaymentType']").removeAttr("checked");
		
		switch (jsSelPaymentMethod){	
			
		case UI_commonSystem.strBspCode :			
			bspRefundAllowed ? $("#trBSP").show() : $("#trBSP").hide();
			$("#trCA").hide();
			$("#trCC").hide();
			$("#trOA").hide();			
			break;
			
		default:
			cashRefundAllowed        ? $("#trCA").show() : $("#trCA").hide();
		    creditCardRefundAllowed  ? $("#trCC").show() : $("#trCC").hide();
		    onAccountRefundAllowed   ? $("#trOA").show() : $("#trOA").hide();
		    offlineRefundAllowed	 ? $("#trOL").show() : $("#trOL").hide();
		    $("#trBSP").hide();
			break;		
		}		
	}
	
	UI_paxAccountPayment.printReciept = function(pnr, recieptNo, hdnPayRef) {
		
		var recieptLanguage = "en";
		var groupPnr = false;
		
			var strUrl = "printReciept.action"
			+ "?hdnPNRNo=" + pnr
			+ "&groupPNR=" + groupPnr
			+ "&recieptNo=" + recieptNo
			+ "&hdnPayRef=" + hdnPayRef
			+ "&recieptLanguage=" + recieptLanguage;
						
			var objRecpt = window.open(strUrl,"myNewWindow",$("#popWindow").getPopWindowProp(630, 900, true));
			if (window.focus) {objRecpt.focus()}
	}
	
	/**
	 * Disables agents dropdown if CASH or CREDIT CARD payment type selected.
	 */
	UI_paxAccountPayment.paymentTypeOnClick = function(){
		//alert('Calling paymentTypeOnChange ');
		var checkedVal = $("input[name='radPaymentType']:checked").val();
		//alert('value '+checkedVal);
		if(checkedVal=='CRED'){			
			
			$("#selAgents").attr('disabled', true);
			$("#selAgents option:first-child").attr("selected","selected");
				
			$("#selCardType").val("");			
			$("#selAgents").hide();
			$("#selCardType").hide();
			UI_paxAccountPayment.resetCCOnClick();
		} else if(checkedVal =='ACCO'){			
			$("#selAgents").removeAttr('disabled'); 
			$("#selCardType").hide();
			$("#selAgents").show();
		} else {//For cash
			$("#selCardType").show();
			$("#selAgents").hide();
			$("#selCardType").hide();
			$("#lblAgentCurrency").html("");
			UI_paxAccountPayment.resetCCOnClick();
			
			$("#selAgents").hide();
			$("#selCardType").show();
		}
	}
	
	/**
	 * Calculates and sets refundable tax for noshow passenger segments
	 */
	UI_paxAccountPayment.calculateRefundableTax = function(){
		if($("#selectedSegment").val()==''){
			return;
		}
		var refundableChargeDetails=UI_paxAccountPayment.refundableCHDetails[$("#selPax").val()];
		var refundableTaxAmount=0.00;
		pnrPaxOndChgIDs=[];
		 if(refundableChargeDetails!=undefined){
			$.each(refundableChargeDetails, function() { 
				if(isOfThisRPH(this.ppfIDs, $("#selectedSegment").val().split("|"))){
					pnrPaxOndChgIDs=pnrPaxOndChgIDs.concat(this.pnrPaxOndChgIDs);
					$.each(this.chargeAmountMap, function(key, value) { 
						  refundableTaxAmount+=value;
					});
				}
			});
		 }
		 
		$('#txtPayAmount').val(refundableTaxAmount);
		refundTypeOrder="TAX"; //Make tax the first priority when refunding
		$('#txtPayAmount').attr('readonly', true);
	}
	
	isOfThisRPH = function(ppfIDList,rphList){
	  	for(var i=0;i<rphList.length;i++){
	  		if(ppfIDList.indexOf(rphList[i]) > -1 ){
	  			return true;
	  		}
	  	}
	  	return false;
	}
	
	/**
	 * Loads no show segments and set them to the combo box.
	 */
//	UI_paxAccountPayment.loadNoShowSegments = function(passengerID){
//		var pnrPaxID;
//		if(passengerID==null || passengerID=="" || passengerID=="null"){
//			pnrPaxID = strPaxID;//TODO remove after testing.
//		}else{
//			pnrPaxID = passengerID;
//		}
//
//		  var refundableChargeDetails=UI_paxAccountPayment.refundableCHDetails[pnrPaxID];
//		  UI_paxAccountPayment.resetOnclick();
//		  $("#selPax").val(pnrPaxID);
//	
//	  	if(refundableChargeDetails!=undefined){
//			  //No refundable charges(TAX) if data map entry is empty
//			  if(refundableChargeDetails.length==0 || pnrPaxID.split(',').length != 1){
//				  $("#btnCalculateTaxInfo").hide();
//				  $("#taxSegmentDetails").hide();
//				  isNoShowRefundPossible = false;
//				  return;
//			  }
//			  
//			  $.each(refundableChargeDetails,function(chargeDetailIndex,chargeDetailValue){
//				  var noShowPPFIds=chargeDetailValue.ppfIDs;
//				  $.each($.parseJSON(opener.UI_reservation.jsonONDs),function(){
//					  	var rphList=this.carrierOndGroupRPH.split("|");
//					  	for(var i=0;i<rphList.length;i++){
//					  		if(noShowPPFIds.indexOf(rphList[i]) > -1 ){
//					  			$("#selectedSegment").append('<option value="'+this.carrierOndGroupRPH+'">'+this.segmentCode+'</option>');
//					  		}
//					  	}
//					});
//			  });
//			  $("#btnCalculateTaxInfo").show();
//	          isNoShowRefundPossible = true;
//		}		  
//		  
//		  
//				  
//	}
	
	UI_paxAccountPayment.insertPayAmount = function(){
		var str = $("#txtPayAmount").val(); 
		
	    str = str.replace(/[^0-9\.]/g,'');

	    if(str.split(".")[2] != null && (str.split(".")[2]).length > 0 ){
	        str = str.substring(0, str.lastIndexOf("."));
	    }   
	    if(str.split(".")[1] != null && str.split(".")[1].length > 2){
	        var trimLen = str.split(".")[1].length - 2;
	        str = str.substring(0, str.length - trimLen);
	    }
	    
	    if(opener.jsonBkgInfo.useEtsForReservation && (str != Math.abs(jsPaymentDetails.actualCredit))){
	    	$("#btnPayConfirm").hide();
	    }else{
	    	$("#btnPayConfirm").show();
	    }
	    $("#txtPayAmount").attr("value",str);
	}
	

	UI_paxAccountPayment.removeControlCharacters = function() {
		var str = $("#txtPayUN").val();
		str = str.replace(/[\n\r]+/g, '');
		$("#txtPayUN").attr("value", str);
	}
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */