	/*
	*********************************************************
		Description		: XBE Make Reservation - Contact Info Tab
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	/*
	 * Passenger Tab
	 */
	function UI_tabContactInfo(){}
	
	UI_tabContactInfo.blnLoaded = false;
	var phoneObj = top.arrCountryPhoneJson;
	var validateEmailDomain = top.validateEmailDomain;
	
	//this will keep the mapping of validation group to
	//client error code
	var jsonGroupValidation = {"1":"XBE-ERR-26"};

	//this will keep the mapping of fieldname+groupid of t_contact_details_config
	//to html form element
	var jsonFieldToElementMapping = {"phoneNo1":"#txtPhoneNo", "mobileNo1":"#txtMobiNo"};
	
	/*
	 * Contact Info Tab Page On Load
	 */
	UI_tabContactInfo.ready = function(){

		$('#taxRegNoLabel').text(geti18nData(taxRegistrationNumberLabel, 'Tax Registration Number'));
		
		if(!UI_tabContactInfo.blnLoaded) {
			
			if(DATA_ResPro.initialParams.modifyContactDetails){	
				$("#btnEditContact").decorateButton();
				$("#btnEditContact").click(function() {UI_tabContactInfo.editOnClick();});
				
				$("#btnSaveContact").decorateButton();
				$("#btnSaveContact").click(function() {UI_tabContactInfo.saveOnClick();});
				$("#btnSaveContact").attr("disabled", true);

				$("#btnCancelSave").decorateButton();
				$("#btnCancelSave").click(function() {UI_tabContactInfo.cancelOnClick();});
				$("#btnCancelSave").attr("disabled", true);
				$("#selCountry").change(function(){UI_tabContactInfo.countryOnChange();});				
				$("#selPrefLang").change(function(){UI_tabContactInfo.prefLangOnChange();});				
			}else{
				$("#btnEditContact").hide();
				$("#btnSaveContact").hide();
				$("#btnCancelSave").hide();
			}	
			UI_tabContactInfo.showHideContactInfo();
			UI_tabContactInfo.fillDropDownData();
			UI_tabContactInfo.onLoadCall();
			UI_tabContactInfo.blnLoaded = true;				
		}else {
			UI_tabContactInfo.onLoadCall();
		}	
	}
	
	UI_tabContactInfo.showHideContactInfo = function(){

		var isContactFromSvcTaxEnabledCountry = UI_tabContactInfo.isServiceTaxApplicableCountry(serviceTaxApplicableCountries,
			jsonContactInfo.country);
		
		//Reservation contact related configurations
		if(contactConfig.title1.xbeVisibility){
			if(!contactConfig.title1.mandatory){
				$("#spnTitleMand").hide();
			}
		} else {
			$("#tblTitle").hide();
		}
		
		if(contactConfig.firstName1.xbeVisibility){
			if(!contactConfig.firstName1.mandatory){
				$("#spnFNameMand").hide();
			}
		} else {
			$("#tblFirstName").hide();
		}
		
		if(contactConfig.lastName1.xbeVisibility){
			if(!contactConfig.lastName1.mandatory){
				$("#spnLNameMand").hide();
			}
		} else {
			$("#tblLastName").hide();
		}

		if (contactConfig.address1.xbeVisibility || isContactFromSvcTaxEnabledCountry) {
			if (!contactConfig.address1.mandatory || !isContactFromSvcTaxEnabledCountry) {
				$("#spnAddrMand").hide();
			}
		} else {
			$("#tblAddr1").hide();
			$("#tblAddr2").hide();
		}
		
		if(contactConfig.nationality1.xbeVisibility){
			if(!contactConfig.nationality1.mandatory){
				$("#spnNationalityMand").hide();
			}
		} else {
			$("#tblNationality").hide();
		}
		
		if(contactConfig.city1.xbeVisibility){
			if(!contactConfig.city1.mandatory){
				$("#spnCityMand").hide();
			}
		} else {
			$("#tblCity").hide();
		}
		
		if(contactConfig.country1.xbeVisibility){
			if(!contactConfig.country1.mandatory){
				$("#spnCountryMand").hide();
			}
		} else {
			$("#tblCountry").hide();
		}
		
		if(contactConfig.zipCode1.xbeVisibility){
			if(!contactConfig.zipCode1.mandatory){
				$("#spnZipCodeMand").hide();
			}
		} else {
			$("#tblZipCode").hide();
		}
		
		if(contactConfig.mobileNo1.xbeVisibility){
			if(!contactConfig.mobileNo1.mandatory){
				$("#spnMobileNoMand").hide();
			}
		} else {
			$("#tblMobileNo").hide();
		}
		
		if(contactConfig.phoneNo1.xbeVisibility){
			if(!contactConfig.phoneNo1.mandatory){
				$("#spnPhoneNoMand").hide();
			}
		} else {
			$("#tblPhoneNo").hide();
		}
		
		if(contactConfig.fax1.xbeVisibility){
			if(!contactConfig.fax1.mandatory){
				$("#spnFaxMand").hide();
			}
		} else {
			$("#tblFax").hide();
		}
		
		if(contactConfig.email1.xbeVisibility){
			if(!contactConfig.email1.mandatory){
				$("#spnEmailMand").hide();
			}
		} else {
			$("#tblEmail").hide();
		}
		
		if(contactConfig.preferredLang1.xbeVisibility){
			if(!contactConfig.preferredLang1.mandatory){
				$("#spnPreferredLangMand").hide();
			} 
		} else {
			$("#tblPreferredLang").hide();
		}

		if (contactConfig.taxRegNo1.xbeVisibility && isContactFromSvcTaxEnabledCountry) {
			if (!contactConfig.taxRegNo1.mandatory) {
				$("#spnTaxRegNoMand").hide();
			}
		} else {
			$("#tblTaxRegNo").hide();
		}
		
		if(contactConfig.state1.xbeVisibility && isContactFromSvcTaxEnabledCountry){
			if(!contactConfig.state1.mandatory || !hasServiceTaxApplied){
				$("#spnState").hide();
			} 
		} else {
			$("#tblState").hide();
		}
		
		
		//Emergency Contact related configurations
		if(contactConfig.firstName2.xbeVisibility ||
				contactConfig.lastName2.xbeVisibility ||
				contactConfig.phoneNo2.xbeVisibility){
			
			if(contactConfig.title2.xbeVisibility){
				if(!contactConfig.title2.mandatory){
					$("#spnEmgnTitleMand").hide();
				}
			} else {
				$("#tblEmgnTitle").hide();
			}
			
			if(contactConfig.firstName2.xbeVisibility){
				if(!contactConfig.firstName2.mandatory){
					$("#spnEmgnFNameMand").hide();
				}
			} else {
				$("#tblEmgnFirstName").hide();
			}
			
			if(contactConfig.lastName2.xbeVisibility){
				if(!contactConfig.lastName2.mandatory){
					$("#spnEmgnLNameMand").hide();
				}
			} else {
				$("#tblEmgnLastName").hide();
			}
			
			if(contactConfig.phoneNo2.xbeVisibility){
				if(!contactConfig.phoneNo2.mandatory){
					$("#spnEmgnPhoneNoMand").hide();
				}
			} else {
				$("#tblEmgnPhoneNo").hide();
			}
			
			if(contactConfig.email2.xbeVisibility){
				if(!contactConfig.email2.mandatory){
					$("#spnEmgnEmailMand").hide();
				}
			} else {
				$("#tblEmgnEmail").hide();
			}		
		} else {
			$("#trEmgnContactInfo").hide();
		}
		
	}
	
	UI_tabContactInfo.countryOnChange = function(){
		UI_tabContactInfo.blnLoaded = true;
		UI_commonSystem.pageOnChange();		
		for(var cl=0;cl< phoneObj.length;cl++){
			counObj = phoneObj[cl];
			if(counObj.countryCode == $("#selCountry").val()){
				$("#txtMobCntry").val(counObj.countryPhoneCode);
				$("#txtPhoneCntry").val(counObj.countryPhoneCode);
				$("#txtFaxCntry").val(counObj.countryPhoneCode);

				if (UI_tabContactInfo.isServiceTaxApplicableCountry(serviceTaxApplicableCountries, counObj.countryCode)) {
					$("#spnAddrMand").show();
					$("#tblTaxRegNo").show();
					$("#tblAddr1").show();
					$("#tblAddr2").show();
					$("#spnTaxRegNoMand").hide();

					$('#selState').find('option').remove().end();
					if(top.countryStateArry[countryCode] != undefined ){
						$("#selState").append("<option value=''></option>");
						$("#selState").append(top.countryStateArry[countryCode]);
					}
					$("#tblState").show();
					$("#spnState").show();

				} else {
					$("#tblState").hide();
					$("#tblTaxRegNo").hide();

					if (contactConfig.address1.xbeVisibility) {
						$("#tblAddr1").show();
						$("#tblAddr2").show();
						if (!contactConfig.address1.mandatory) {
							$("#spnAddrMand").hide();
						}
					} else {
						$("#tblAddr1").hide();
						$("#tblAddr2").hide();
					}
				}

				break;
			}
		}
	}
	//@todo remove and short-circuit
//	UI_tabContactInfo.loadPhones = function(response) {
//		if(response != null && response.success){
//			phoneObj = response.phoneObj;
//			validateEmailDomain = response.validateEmailDomain;
//		}
//		UI_commonSystem.hideProgress();
//	}
	
	/*
	 * On Load Call
	 */
	UI_tabContactInfo.onLoadCall = function(){
		UI_tabContactInfo.fillBookingInfo();
		UI_tabContactInfo.disableEnableControls(true);
		UI_tabContactInfo.fillContactInfo();
		$("#btnEditContact").attr("disabled", false);
	}
	
	/*
	 * Edit Button Click
	 */
	UI_tabContactInfo.editOnClick = function() {
		UI_tabContactInfo.disableEnableControls(false);
		UI_tabContactInfo.disableEnableButtons(true);
		$("#selTitle").focus();
	}
	
	/*
	 * Save Button Click
	 */
	UI_tabContactInfo.saveOnClick = function(){

		// PNR
		var data = {};
		data['groupPNR'] = jsonGroupPNR;
		data['pnr'] = jsonPNR;
		data['version'] = jsonBkgInfo.version;	
		data['oldContactInfoStr'] = $.toJSON(jsonContactInfo);
		//UI_tabContactInfo.validate(UI_tabContactInfo.validateContactSuccess);
		if(!UI_tabContactInfo.validate()) {
			// Validation not OK, return to page
			return;
		}		
		UI_commonSystem.showProgress();
		$("#frmContact").ajaxSubmit({dataType: 'json',data:data, url:"modifyContactInfo.action",						
			success: UI_tabContactInfo.saveContactSuccess, error:UI_commonSystem.setErrorStatus});		
	}
	
	
	UI_tabContactInfo.saveContactSuccess = function(response) {		
		UI_reservation.loadReservation("MODCONTINFO", response,false);
	}
	
	UI_tabContactInfo.saveContactSuccessUpdate = function(response) {
		UI_commonSystem.hideProgress();
		if(response.success) {				
			// TODO : Improve message
			UI_tabContactInfo.disableEnableControls(true);
			UI_tabContactInfo.disableEnableButtons(false);
			UI_tabContactInfo.fillContactInfo();
			showConfirmation("Successfully updated contact info");				
			
		} else {
			// TODO : Improve message
			UI_tabContactInfo.disableEnableControls(false);
			UI_tabContactInfo.disableEnableButtons(true);
			showERRMessage("Error while updating contact info");
		}
	}
	
	/*
	 * Cancel Button Click
	 */	
	UI_tabContactInfo.cancelOnClick = function(){
		UI_tabContactInfo.onLoadCall();
		UI_tabContactInfo.disableEnableButtons(false);
		//Resets itinerary language to old lang
		UI_tabContactInfo.prefLangOnChange();
	}
	
	/*
	 * Fill Booking Info
	 */
	UI_tabContactInfo.fillBookingInfo = function(){
		$("#divPnrNoCont").html(jsonBkgInfo.PNR);
		var status = jsonBkgInfo.displayStatus;
		$("#divBkgStatusCont").html(status);
		
		$("#divAgentCont").html(jsonBkgInfo.agent);
	}
	
	/*
	 * Fill contact Info 
	 */
	UI_tabContactInfo.fillContactInfo = function(){
		if (jsonContactInfo != null){
			$("#selTitle").val(jsonContactInfo.title);
			$("#txtFirstName").val(jsonContactInfo.firstName);		
			$("#txtLastName").val(jsonContactInfo.lastName);		
			$("#txtStreet").val(jsonContactInfo.street);
			$("#txtAddress").val(jsonContactInfo.address);		
			$("#selNationality").val(jsonContactInfo.nationality);		
			$("#txtCity").val(jsonContactInfo.city);		
			$("#selCountry").val(jsonContactInfo.country);		
			$("#txtZipCode").val(jsonContactInfo.zipCode);		
			$("#txtMobCntry").val(jsonContactInfo.mobileCountry);
			$("#txtMobArea").val(jsonContactInfo.mobileArea);
			$("#txtMobiNo").val(jsonContactInfo.mobileNo);		
			$("#txtPhoneCntry").val(jsonContactInfo.phoneCountry);
			$("#txtPhoneArea").val(jsonContactInfo.phoneArea);
			$("#txtPhoneNo").val(jsonContactInfo.phoneNo);		
			$("#txtFaxCntry").val(jsonContactInfo.faxCountry);
			$("#txtFaxArea").val(jsonContactInfo.faxArea);
			$("#txtFaxNo").val(jsonContactInfo.faxNo);
			$("#txtEmail").val(jsonContactInfo.email);
			$("#txtTaxRegNo").val(jsonContactInfo.taxRegNo);
			
			if(contactConfig.state1.xbeVisibility){
				var taxRegNoEnabledCountries = DATA_ResPro.initialParams.taxRegistrationNumberEnabledCountries.split(',');
				for ( var cl = 0; cl < taxRegNoEnabledCountries.length; cl++) {
					countryCode = taxRegNoEnabledCountries[cl];
					if (countryCode == $("#selCountry").val()) {

						$('#selState').find('option').remove().end();
						if(top.countryStateArry[countryCode] != undefined ){
							$("#selState").append("<option value=''></option>");
							$("#selState").append(top.countryStateArry[countryCode]);
						}
						$("#spnAddrMand").show();
						$("#spnState").show();
						$("#tblAddr1").show();
						$("#tblAddr2").show();
						break;
					}
				}
			}
			
			$("#selState").val(jsonContactInfo.state);

			if(jsonContactInfo.preferredLang == null || 
					jsonContactInfo.preferredLang == ""){
				if(DATA_ResPro["systemParams"] != null){
					$("#selPrefLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
				}
			} else {
				$("#selPrefLang").val(jsonContactInfo.preferredLang);
			}			
			
			//Emergency Contact related configurations
			$("#selEmgnTitle").val(jsonContactInfo.emgnTitle);		
			$("#txtEmgnFirstName").val(jsonContactInfo.emgnFirstName);		
			$("#txtEmgnLastName").val(jsonContactInfo.emgnLastName);		
			$("#txtEmgnPhoneCntry").val(jsonContactInfo.emgnPhoneCountry);
			$("#txtEmgnPhoneArea").val(jsonContactInfo.emgnPhoneArea);
			$("#txtEmgnPhoneNo").val(jsonContactInfo.emgnPhoneNo);		
			$("#txtEmgnEmail").val(jsonContactInfo.emgnEmail);
		}
	}
	
	UI_tabContactInfo.isServiceTaxApplicableCountry = function(serviceTaxApplicableCountries, contactInfoCountry){
		var isServiceTaxApplicableCountry = false;
		if (serviceTaxApplicableCountries && contactInfoCountry) {
			var serviceTaxApplicableCountriesArray = serviceTaxApplicableCountries.split(',');
			for (var i = 0; i < serviceTaxApplicableCountriesArray.length; i++) {
				if (serviceTaxApplicableCountriesArray[i] == contactInfoCountry){
					isServiceTaxApplicableCountry = true;
					break;
				}
			}
		} else {
			isServiceTaxApplicableCountry = false;
		}
		return isServiceTaxApplicableCountry;
	}
	
	
	/*
	 * Fill Drop down data for the Passenger tab
	 */
	UI_tabContactInfo.fillDropDownData = function(){
		$("#selTitle").fillDropDown( { dataArray:top.arrTitle , keyIndex:0 , valueIndex:1, firstEmpty:true });	
		$("#selCountry").fillDropDown( { dataArray:top.arrCountry , keyIndex:0 , valueIndex:1, firstEmpty:true });	
		$("#selNationality").fillDropDown( { dataArray:top.arrNationality , keyIndex:0 , valueIndex:1, firstEmpty:true });	
		$("#selEmgnTitle").fillDropDown( { dataArray:top.arrPaxTitle , keyIndex:0 , valueIndex:1, firstEmpty:true });
		$("#selPrefLang").fillDropDown({dataArray:top.jsonItnLang, keyIndex:"id", valueIndex:"desc", firstEmpty:false});
	}
	
	/*
	 * Disable Page Controls
	 */ 
	UI_tabContactInfo.disableEnableControls = function(blnStatus){		
		if(contactConfig.title1.xbeVisibility){
			$("#selTitle").disableEnable(blnStatus);
		}
		
		if(contactConfig.firstName1.xbeVisibility){
			$("#txtFirstName").disableEnable(blnStatus);
		}
		
		if(contactConfig.lastName1.xbeVisibility){
			$("#txtLastName").disableEnable(blnStatus);
		}
		
		if(contactConfig.address1.xbeVisibility){
			$("#txtStreet").disableEnable(blnStatus);
			$("#txtAddress").disableEnable(blnStatus);
		}
		
		if(contactConfig.nationality1.xbeVisibility){
			$("#selNationality").disableEnable(blnStatus);
		}
		
		if(contactConfig.city1.xbeVisibility){
			$("#txtCity").disableEnable(blnStatus);
		}
		
		if(contactConfig.country1.xbeVisibility){
			$("#selCountry").disableEnable(blnStatus);
		}
		
		if(contactConfig.zipCode1.xbeVisibility){
			$("#txtZipCode").disableEnable(blnStatus);
		}
		
		if(contactConfig.mobileNo1.xbeVisibility){
			$("#txtMobCntry").disableEnable(blnStatus);
			$("#txtMobArea").disableEnable(blnStatus);
			$("#txtMobiNo").disableEnable(blnStatus);
		}
		
		if(contactConfig.phoneNo1.xbeVisibility){
			$("#txtPhoneCntry").disableEnable(blnStatus);
			$("#txtPhoneArea").disableEnable(blnStatus);
			$("#txtPhoneNo").disableEnable(blnStatus);
		}
		
		if(contactConfig.fax1.xbeVisibility){
			$("#txtFaxCntry").disableEnable(blnStatus);
			$("#txtFaxArea").disableEnable(blnStatus);
			$("#txtFaxNo").disableEnable(blnStatus);
		}
		
		if(contactConfig.email1.xbeVisibility	){
			$("#txtEmail").disableEnable(blnStatus);
		}
		
		if(contactConfig.preferredLang1.xbeVisibility){
			$("#selPrefLang").disableEnable(blnStatus);
		}	
		
		if(contactConfig.state1.xbeVisibility){
			$("#selState").disableEnable(blnStatus || hasServiceTaxApplied);
		}

		if (contactConfig.country1.xbeVisibility) {
			$("#selCountry").disableEnable(blnStatus || hasServiceTaxApplied);
		}

		if(contactConfig.taxRegNo1.xbeVisibility){
			$("#txtTaxRegNo").disableEnable(blnStatus || hasServiceTaxApplied);
		}
		
		//Emergency Contact related configurations
		if(contactConfig.firstName2.xbeVisibility ||
				contactConfig.lastName2.xbeVisibility ||
				contactConfig.phoneNo2.xbeVisibility){
			if(contactConfig.title2.xbeVisibility){
				$("#selEmgnTitle").disableEnable(blnStatus);
			}
			
			if(contactConfig.firstName2.xbeVisibility){
				$("#txtEmgnFirstName").disableEnable(blnStatus);
			}
			
			if(contactConfig.lastName2.xbeVisibility){
				$("#txtEmgnLastName").disableEnable(blnStatus);
			}
			
			if(contactConfig.phoneNo2.xbeVisibility){
				$("#txtEmgnPhoneCntry").disableEnable(blnStatus);
				$("#txtEmgnPhoneArea").disableEnable(blnStatus);
				$("#txtEmgnPhoneNo").disableEnable(blnStatus);
			}
			
			if(contactConfig.email2.xbeVisibility){
				$("#txtEmgnEmail").disableEnable(blnStatus);
			}
			
		}
	}
	
	/*
	 * Enable/Disable Buttons
	 */ 
	UI_tabContactInfo.disableEnableButtons = function(blnEditMode){
		$("#btnEditContact").attr("disabled", blnEditMode);
		$("#btnSaveContact").attr("disabled", !blnEditMode);
		$("#btnCancelSave").attr("disabled", !blnEditMode);
	}
	
	/*
	 * Validate Page
	 */ 
	UI_tabContactInfo.validate = function(){

		var isContactFromSvcTaxEnabledCountry = UI_tabContactInfo.isServiceTaxApplicableCountry(serviceTaxApplicableCountries,
			$("#selCountry").val());
		
		if(contactConfig.title1.xbeVisibility && contactConfig.title1.mandatory){
			if($.trim($("#selTitle").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Title"));
				$("#selTitle").focus();
				return false;
			}
		}
		
		if(contactConfig.firstName1.xbeVisibility && contactConfig.firstName1.mandatory){
			if($.trim($("#txtFirstName").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","First name"));
				$("#txtFirstName").focus();
				return false;
			}
			if(!isAlphaWhiteSpace($.trim($("#txtFirstName").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","First name"));
				$("#txtFirstName").focus();
				return false;
			}
		} else if(contactConfig.firstName1.xbeVisibility && !contactConfig.firstName1.mandatory && 
				$.trim($("#txtFirstName").val()) != ""){
			if(!isAlphaWhiteSpace($.trim($("#txtFirstName").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","First name"));
				$("#txtFirstName").focus();
				return false;
			}
		}
		
		if(contactConfig.lastName1.xbeVisibility && contactConfig.lastName1.mandatory){
			if($.trim($("#txtLastName").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Last name"));
				$("#txtLastName").focus();
				return false;
			}
			if(!isAlphaWhiteSpace($.trim($("#txtLastName").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Last name"));
				$("#txtLastName").focus();
				return false;
			}
		} else if(contactConfig.lastName1.xbeVisibility && !contactConfig.lastName1.mandatory && 
				$.trim($("#txtLastName").val()) != ""){
			if(!isAlphaWhiteSpace($.trim($("#txtLastName").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Last name"));
				$("#txtLastName").focus();
				return false;
			}
		}

		if(isContactFromSvcTaxEnabledCountry && $("#selState").val() == ''){
			showERRMessage(raiseError("XBE-ERR-01", "State"));
			$("#selState").focus();
			return false;
		}

		if ($.trim($("#txtTaxRegNo").val()) && $.trim($("#txtTaxRegNo").val()).length != 15) {
			showERRMessage(raiseError("XBE-ERR-16", "15", "Tax Registration Number"));
			$("#txtTaxRegNo").focus();
			return false;
		}

		if ($.trim($("#txtTaxRegNo").val()) && $.trim($("#txtTaxRegNo").val()).substring(0, 2) != $("#selState").val()) {
			showERRMessage("Tax Registration Number does not match with the state. Please check..");
			$("#txtTaxRegNo").focus();
			return false;
		}

		if ($.trim($("#txtTaxRegNo").val()) &&
			!/\d{2}[a-zA-Z]{5}\d{4}[a-zA-Z]{1}[a-zA-Z\d]{1}[a-zA-Z]{1}[a-zA-Z\d]{1}/.test($.trim($("#txtTaxRegNo").val()))) {
			showERRMessage(raiseError("XBE-ERR-04", "Tax Registration Number. Please check.."));
			$("#txtTaxRegNo").focus();
			return false;
		}
		
		if((contactConfig.address1.xbeVisibility && contactConfig.address1.mandatory) || isContactFromSvcTaxEnabledCountry){

			$("#txtStreet").val($.trim($("#txtStreet").val()));
			$("#txtAddress").val($.trim($("#txtAddress").val()));
			if ($.trim($("#txtStreet").val()) == "" && $.trim($("#txtAddress").val()) == "" &&
				(isContactFromSvcTaxEnabledCountry)) {
				showERRMessage(raiseError("XBE-ERR-01", "Street and Address"));
				$("#txtStreet").focus();
				return false;
			}
			if ($.trim($("#txtStreet").val()) == "" && !isContactFromSvcTaxEnabledCountry) {
				showERRMessage(raiseError("XBE-ERR-04", "Street address"));
				$("#txtStreet").focus();
				return false;
			}

			if ($("#txtStreet").val() && !isAlphaNumericWhiteSpaceComma($("#txtStreet").val())) {
				showERRMessage(raiseError("XBE-ERR-04", "Street address"));
				$("#txtStreet").focus();
				return false;
			}
			if ($.trim($("#txtAddress").val()) == "" && !isContactFromSvcTaxEnabledCountry) {
				showERRMessage(raiseError("XBE-ERR-04", "Street address"));
				$("#txtAddress").focus();
				return false;
			}
			if($("#txtAddress").val() && !isAlphaNumericWhiteSpaceComma($("#txtAddress").val())) {
				showERRMessage(raiseError("XBE-ERR-04","Street address"));
				$("#txtAddress").focus();
				return false;
			}
		} else if(contactConfig.address1.xbeVisibility && !contactConfig.address1.mandatory) {
			if($.trim($("#txtStreet").val()) != "") {
				$("#txtStreet").val($.trim($("#txtStreet").val()));
				if(!isAlphaNumericWhiteSpaceComma($("#txtStreet").val())) {
					showERRMessage(raiseError("XBE-ERR-04","Street address"));
					$("#txtStreet").focus();
					return false;
				}
			}
			
			if($.trim($("#txtAddress").val()) != "") {
				$("#txtAddress").val($.trim($("#txtAddress").val()));
				if(!isAlphaNumericWhiteSpaceComma($("#txtAddress").val())) {
					showERRMessage(raiseError("XBE-ERR-04","Street address"));
					$("#txtAddress").focus();
					return false;
				}
			}
		}
		
		if(contactConfig.city1.xbeVisibility && contactConfig.city1.mandatory){
			if($.trim($("#txtCity").val()) == "") {		
				showERRMessage(raiseError("XBE-ERR-01","Town / City"));
				$("#txtCity").focus();
				return false;
			}else {
					$("#txtCity").val($.trim($("#txtCity").val()));
			}
			if(!isAlphaWhiteSpace($.trim($("#txtCity").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Town / City"));
				$("#txtCity").focus();
				return false;
			}
		} else if(contactConfig.city1.xbeVisibility && !contactConfig.city1.mandatory && 
				$.trim($("#txtCity").val()) != ""){
			if(!isAlphaWhiteSpace($.trim($("#txtCity").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Town / City"));
				$("#txtCity").focus();
				return false;
			}
		}
		
		if(contactConfig.mobileNo1.xbeVisibility && contactConfig.mobileNo1.mandatory){
			if($.trim($("#txtMobCntry").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Mobile country code"));
				$("#txtMobCntry").focus();
				return false;
			}else{
				$("#txtMobCntry").val($.trim($("#txtMobCntry").val()));
			}		
			if(!validatePositiveInteger($.trim($("#txtMobCntry").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Mobile country code"));
				$("#txtMobCntry").focus();
				return false;
			}
			if($.trim($("#txtMobArea").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Mobile area code"));
				$("#txtMobArea").focus();
				return false;
			}else{
				$("#txtMobArea").val($.trim($("#txtMobArea").val()));
			}	
			if(!validatePositiveInteger($.trim($("#txtMobArea").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Mobile area code"));
				$("#txtMobArea").focus();			
				return false;
			}
			if($.trim($("#txtMobiNo").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Mobile number"));
				$("#txtMobiNo").focus();
				return false;
			}else{
				$("#txtMobiNo").val($.trim($("#txtMobiNo").val()));
			}	
			if(!validatePositiveInteger($.trim($("#txtMobiNo").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Mobile number"));
				$("#txtMobiNo").focus();
				return false;
			}
			
			if(!UI_commonSystem.validateCountryPhone($("#selCountry").val(), phoneObj, $("#txtMobCntry").val(), 
					$("#txtMobArea").val(), $("#txtMobiNo").val(),"MOBILE")){return false}
		} else if(contactConfig.mobileNo1.xbeVisibility && !contactConfig.mobileNo1.mandatory){
			if($.trim($("#txtMobCntry").val()) !== "") {
				$("#txtMobCntry").val($.trim($("#txtMobCntry").val()));
				if(!validatePositiveInteger($.trim($("#txtMobCntry").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Phone country code"));
					$("#txtMobCntry").focus();
					return false;
				}
			}
			if($.trim($("#txtMobArea").val()) !== "") {
				$("#txtMobArea").val($.trim($("#txtMobArea").val()));
				if(!validatePositiveInteger($.trim($("#txtMobArea").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Phone area code"));
					$("#txtMobArea").focus();
					return false;
				}
			}
			if($.trim($("#txtMobiNo").val()) !== "") {
				$("#txtMobiNo").val($.trim($("#txtMobiNo").val()));
				if(!validatePositiveInteger($.trim($("#txtMobiNo").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Phone number"));
					$("#txtMobiNo").focus();
					return false;
				}
			}
			
			// If a Phone field is entered, then all Phone fields must be entered
			if($.trim($("#txtMobiNo").val()) !== "") {
				var phoneFieldsFilled = 
						(($.trim($("#txtMobCntry").val()) == "") ? 0 : 1) + 
						(($.trim($("#txtMobArea").val()) == "") ? 0 : 1) +
						(($.trim($("#txtMobiNo").val()) == "") ? 0 : 1);
				if(phoneFieldsFilled != 0 && phoneFieldsFilled != 3) {
					showERRMessage(raiseError("XBE-ERR-04","number of Phone fields entered"));
					$("#txtMobCntry").focus();
					return false;
				}
				
				if($.trim($("#txtMobiNo").val()) !== "" && $.trim($("#txtMobCntry").val()) !== "" &&
						$.trim($("#txtMobArea").val()) !== "") {
					if(!UI_commonSystem.validateCountryPhone($("#selCountry").val(), phoneObj, $("#txtMobCntry").val(), 
							$("#txtMobArea").val(), $("#txtMobiNo").val(),"MOBILE")){return false}
				}
			} else {
				$("#txtMobiNo").val("");
				$("#txtMobCntry").val("");
				$("#txtMobArea").val("");
			}
		}
		
		if(contactConfig.phoneNo1.xbeVisibility && contactConfig.phoneNo1.mandatory){
			if($.trim($("#txtPhoneCntry").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Phone country code"));
				$("#txtPhoneCntry").focus();
				return false;
			} else {
				$("#txtPhoneCntry").val($.trim($("#txtPhoneCntry").val()));
			}
			
			if(!validatePositiveInteger($.trim($("#txtPhoneCntry").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Phone country code"));
				$("#txtPhoneCntry").focus();
				return false;
			}
			
			if($.trim($("#txtPhoneArea").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Phone area code"));
				$("#txtPhoneArea").focus();
				return false;
			} else {
				$("#txtPhoneArea").val($.trim($("#txtPhoneArea").val()));
			}
			
			if(!validatePositiveInteger($.trim($("#txtPhoneArea").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Phone area code"));
				$("#txtPhoneArea").focus();
				return false;
			}
			
			if($.trim($("#txtPhoneNo").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-04","Phone number"));
				$("#txtPhoneNo").focus();
				return false;
			} else {
				$("#txtPhoneNo").val($.trim($("#txtPhoneNo").val()));
			}
			
			if(!validatePositiveInteger($.trim($("#txtPhoneNo").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Phone number"));
				$("#txtPhoneNo").focus();
				return false;
			}
			
			if(!UI_commonSystem.validateCountryPhone($("#selCountry").val(), phoneObj, $("#txtPhoneCntry").val(), 
					$("#txtPhoneArea").val(), $("#txtPhoneNo").val(),"LAND")){return false}
			
		} else if(contactConfig.phoneNo1.xbeVisibility && !contactConfig.phoneNo1.mandatory){
			if($.trim($("#txtPhoneCntry").val()) !== "") {
				$("#txtPhoneCntry").val($.trim($("#txtPhoneCntry").val()));
				if(!validatePositiveInteger($.trim($("#txtPhoneCntry").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Phone country code"));
					$("#txtPhoneCntry").focus();
					return false;
				}
			}
			if($.trim($("#txtPhoneArea").val()) !== "") {
				$("#txtPhoneArea").val($.trim($("#txtPhoneArea").val()));
				if(!validatePositiveInteger($.trim($("#txtPhoneArea").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Phone area code"));
					$("#txtPhoneArea").focus();
					return false;
				}
			}
			if($.trim($("#txtPhoneNo").val()) !== "") {
				$("#txtPhoneNo").val($.trim($("#txtPhoneNo").val()));
				if(!validatePositiveInteger($.trim($("#txtPhoneNo").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Phone number"));
					$("#txtPhoneNo").focus();
					return false;
				}
			}
			
			// If a Phone field is entered, then all Phone fields must be entered
			if($.trim($("#txtPhoneNo").val()) !== "") {
				var phoneFieldsFilled = 
						(($.trim($("#txtPhoneCntry").val()) == "") ? 0 : 1) + 
						(($.trim($("#txtPhoneArea").val()) == "") ? 0 : 1) +
						(($.trim($("#txtPhoneNo").val()) == "") ? 0 : 1);
				if(phoneFieldsFilled != 0 && phoneFieldsFilled != 3) {
					showERRMessage(raiseError("XBE-ERR-04","number of Phone fields entered"));
					$("#txtPhoneCntry").focus();
					return false;
				}
				
				if($.trim($("#txtPhoneNo").val()) !== "" && $.trim($("#txtPhoneCntry").val()) !== "" &&
						$.trim($("#txtPhoneArea").val()) !== "") {
					if(!UI_commonSystem.validateCountryPhone($("#selCountry").val(), phoneObj, $("#txtPhoneCntry").val(), 
							$("#txtPhoneArea").val(), $("#txtPhoneNo").val(),"LAND")){return false}
				}
				
			} else {
				$("#txtPhoneNo").val("");
				$("#txtPhoneCntry").val("");
				$("#txtPhoneArea").val("");
			}
		}
		
		if(contactConfig.fax1.xbeVisibility && contactConfig.fax1.mandatory){
			if($.trim($("#txtFaxCntry").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Fax country code"));
				$("#txtFaxCntry").focus();
				return false;
			} else {
				$("#txtFaxCntry").val($.trim($("#txtFaxCntry").val()));
			}
			
			if(!validatePositiveInteger($.trim($("#txtFaxCntry").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Fax country code"));
				$("#txtFaxCntry").focus();
				return false;
			}
		
			if($.trim($("#txtFaxArea").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Fax area code"));
				$("#txtFaxArea").focus();
				return false;
			} else {
				$("#txtFaxArea").val($.trim($("#txtFaxArea").val()));
			}
		
			if(!validatePositiveInteger($.trim($("#txtFaxArea").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Fax area code"));
				$("#txtFaxArea").focus();
				return false;
			}
			
			if($.trim($("#txtFaxNo").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Fax number"));
				$("#txtFaxNo").focus();
				return false;
			} else {
				$("#txtFaxNo").val($.trim($("#txtFaxNo").val()));
			}
			
			if(!validatePositiveInteger($.trim($("#txtFaxNo").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Fax number"));
				$("#txtFaxNo").focus();
				return false;
			}
			
		} else if(contactConfig.fax1.xbeVisibility && !contactConfig.fax1.mandatory){
			if($.trim($("#txtFaxCntry").val()) !== "") {
				$("#txtFaxCntry").val($.trim($("#txtFaxCntry").val()));
				if(!validatePositiveInteger($.trim($("#txtFaxCntry").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Fax country code"));
					$("#txtFaxCntry").focus();
					return false;
				}
			}
			if($.trim($("#txtFaxArea").val()) !== "") {
				$("#txtFaxArea").val($.trim($("#txtFaxArea").val()));
				if(!validatePositiveInteger($.trim($("#txtFaxArea").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Fax area code"));
					$("#txtFaxArea").focus();
					return false;
				}
			}
			if($.trim($("#txtFaxNo").val()) !== "") {
				$("#txtFaxNo").val($.trim($("#txtFaxNo").val()));
				if(!validatePositiveInteger($.trim($("#txtFaxNo").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Fax number"));
					$("#txtFaxNo").focus();
					return false;
				}
			}	
			
			// If a Fax field is entered, then all Fax fields must be entered
			if($.trim($("#txtFaxNo").val()) !== "") {
				var faxFieldsFilled = 
						(($.trim($("#txtFaxCntry").val()) == "") ? 0 : 1) + 
						(($.trim($("#txtFaxArea").val()) == "") ? 0 : 1) +
						(($.trim($("#txtFaxNo").val()) == "") ? 0 : 1);
				if(faxFieldsFilled != 0 && faxFieldsFilled != 3) {
					showERRMessage(raiseError("XBE-ERR-04","number of Fax fields entered"));
					$("#txtFaxCntry").focus();
					return false;
				}
			} else {
				$("#txtFaxNo").val("");
				$("#txtFaxCntry").val("");
				$("#txtFaxArea").val("");
			}
		}
		
		if(contactConfig.nationality1.xbeVisibility && contactConfig.nationality1.mandatory){
			if($("#selNationality").val() == ""){
				showERRMessage(raiseError("XBE-ERR-01","Nationality"));
				$("#selNationality").focus();
				return false;
			}
		}
		
		if(contactConfig.email1.xbeVisibility && contactConfig.email1.mandatory){
			if($.trim($("#txtEmail").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Email address"));
				$("#txtEmail").focus();
				return false;
			} else {
				$("#txtEmail").val($.trim($("#txtEmail").val()));
			}
			
			if(!checkEmail($("#txtEmail").val())) {
				showERRMessage(raiseError("XBE-ERR-04","Email address"));
				$("#txtEmail").focus();
				return false;
			}else {
				if(validateEmailDomain){
					if(!UI_tabContactInfo.checkEmailDomain($("#txtEmail").val())){
						showERRMessage(raiseError("XBE-ERR-04","Email address"));
						$("#txtEmail").focus();
						return false;
					}
				}
			} 
			
		} else if(contactConfig.email1.xbeVisibility && !contactConfig.email1.mandatory && 
				$.trim($("#txtEmail").val()) != ""){
			if(!checkEmail($("#txtEmail").val())) {
				showERRMessage(raiseError("XBE-ERR-04","Email address"));
				$("#txtEmail").focus();
				return false;
			}else {
				if(validateEmailDomain){
					if(!UI_tabContactInfo.checkEmailDomain($("#txtEmail").val())){
						showERRMessage(raiseError("XBE-ERR-04","Email address"));
						$("#txtEmail").focus();
						return false;
					}
				}
			} 
		}
		
		if(contactConfig.country1.xbeVisibility && contactConfig.country1.mandatory){
			if($.trim($("#selCountry").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Country"));
				$("#selCountry").focus();
				return false;
			}else {
				$("#selCountry").val($.trim($("#selCountry").val()));
			}
			if(!isAlphaWhiteSpace($.trim($("#selCountry").val()))) {
				showERRMessage(raiseError("XBE-ERR-04","Country"));
				$("#selCountry").focus();
				return false;
			}
		}		
		
		if(contactConfig.zipCode1.xbeVisibility && contactConfig.zipCode1.mandatory){
			if($.trim($("#txtZipCode").val()) == ""){
				showERRMessage(raiseError("XBE-ERR-01","Zip Code"));
				$("#txtZipCode").focus();
				return false;
			} else if(!UI_tabContactInfo.validateZipCode($.trim($("#txtZipCode").val()))){
				showERRMessage(raiseError("XBE-ERR-04","Zip Code. Enter XXXXX or XXXXX-XXXX code"));
				$("#txtZipCode").focus();
				return false;
			}
		} else if(contactConfig.zipCode1.xbeVisibility && !contactConfig.zipCode1.mandatory &&
				$.trim($("#txtZipCode").val()) != "") {
			if(!UI_tabContactInfo.validateZipCode($.trim($("#txtZipCode").val()))){
				showERRMessage(raiseError("XBE-ERR-04","Zip Code. Enter XXXXX or XXXXX-XXXX code"));
				$("#txtZipCode").focus();
				return false;
			}
		}
		
		if(contactConfig.userNote1.xbeVisibility && contactConfig.userNote1.mandatory){
			if($.trim($("#txtUserNotes").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","User note"));
				$("#txtUserNotes").focus();
				return false;
			}
		}
		
		if(contactConfig.taxRegNo1.xbeVisibility && contactConfig.taxRegNo1.mandatory){
			if($.trim($("#txtTaxRegNo").val()) == "") {
				showERRMessage(raiseError("XBE-ERR-01","Tax Registration Number"));
				$("#txtTaxRegNo").focus();
				return false;
			}
		}
		
		if(!groupFieldValidation(validationGroup, jsonFieldToElementMapping, jsonGroupValidation)){
			return false;
		}
		
		//validating emergency contact information
		if(contactConfig.firstName2.xbeVisibility ||
				contactConfig.lastName2.xbeVisibility ||
				contactConfig.phoneNo2.xbeVisibility){
			
			if(contactConfig.title2.xbeVisibility && contactConfig.title2.mandatory){
				if($.trim($("#selEmgnTitle").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Emergency Contact Title"));
					$("#selEmgnTitle").focus();
					return false;
				}
			} 
			
			if(contactConfig.firstName2.xbeVisibility && contactConfig.firstName2.mandatory){
				if($.trim($("#txtEmgnFirstName").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Emergency Contact First name"));
					$("#txtEmgnFirstName").focus();
					return false;
				}
				if(!isAlphaWhiteSpace($.trim($("#txtEmgnFirstName").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact First name"));
					$("#txtEmgnFirstName").focus();
					return false;
				}
			} else if(contactConfig.firstName2.xbeVisibility && !contactConfig.firstName2.mandatory && 
					$.trim($("#txtEmgnFirstName").val()) !== ""){
				if(!isAlphaWhiteSpace($.trim($("#txtEmgnFirstName").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact First name"));
					$("#txtEmgnFirstName").focus();
					return false;
				}
			}
			
			if(contactConfig.lastName2.xbeVisibility && contactConfig.lastName2.mandatory){
				if($.trim($("#txtEmgnLastName").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Emergency Contact Last name"));
					$("#txtEmgnLastName").focus();
					return false;
				}
				if(!isAlphaWhiteSpace($.trim($("#txtEmgnLastName").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Last name"));
					$("#txtEmgnLastName").focus();
					return false;
				}
			} else if(contactConfig.lastName2.xbeVisibility && !contactConfig.lastName2.mandatory && 
					$.trim($("#txtEmgnLastName").val()) !== ""){
				if(!isAlphaWhiteSpace($.trim($("#txtEmgnLastName").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Last name"));
					$("#txtEmgnLastName").focus();
					return false;
				}
			}
			
			if(contactConfig.phoneNo2.xbeVisibility && contactConfig.phoneNo2.mandatory){
				if($.trim($("#txtEmgnPhoneCntry").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Emergency Contact Phone country code"));
					$("#txtEmgnPhoneCntry").focus();
					return false;
				} else {
					$("#txtEmgnPhoneCntry").val($.trim($("#txtEmgnPhoneCntry").val()));
				}
				
				if(!validatePositiveInteger($.trim($("#txtEmgnPhoneCntry").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Phone country code"));
					$("#txtEmgnPhoneCntry").focus();
					return false;
				}
				
				if($.trim($("#txtEmgnPhoneArea").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Emergency Contact Phone area code"));
					$("#txtEmgnPhoneArea").focus();
					return false;
				} else {
					$("#txtEmgnPhoneArea").val($.trim($("#txtEmgnPhoneArea").val()));
				}
				
				if(!validatePositiveInteger($.trim($("#txtEmgnPhoneArea").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Phone area code"));
					$("#txtEmgnPhoneArea").focus();
					return false;
				}
				
				if($.trim($("#txtEmgnPhoneNo").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Emergency Contact Phone number"));
					$("#txtEmgnPhoneNo").focus();
					return false;
				} else {
					$("#txtEmgnPhoneNo").val($.trim($("#txtEmgnPhoneNo").val()));
				}
				
				if(!validatePositiveInteger($.trim($("#txtEmgnPhoneNo").val()))) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Phone number"));
					$("#txtEmgnPhoneNo").focus();
					return false;
				}				
			} else if(contactConfig.phoneNo2.xbeVisibility && !contactConfig.phoneNo2.mandatory){
				if($.trim($("#txtEmgnPhoneCntry").val()) !== "") {
					$("#txtEmgnPhoneCntry").val($.trim($("#txtEmgnPhoneCntry").val()));
					if(!validatePositiveInteger($.trim($("#txtEmgnPhoneCntry").val()))) {
						showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Phone country code"));
						$("#txtEmgnPhoneCntry").focus();
						return false;
					}
				}
				if($.trim($("#txtEmgnPhoneArea").val()) !== "") {
					$("#txtEmgnPhoneArea").val($.trim($("#txtEmgnPhoneArea").val()));
					if(!validatePositiveInteger($.trim($("#txtEmgnPhoneArea").val()))) {
						showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Phone area code"));
						$("#txtEmgnPhoneArea").focus();
						return false;
					}
				}
				if($.trim($("#txtEmgnPhoneNo").val()) !== "") {
					$("#validatePositiveInteger").val($.trim($("#txtEmgnPhoneNo").val()));
					if(!validatePositiveInteger($.trim($("#txtEmgnPhoneNo").val()))) {
						showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Phone number"));
						$("#txtEmgnPhoneNo").focus();
						return false;
					}
				}
				
				// If a Phone field is entered, then all Phone fields must be entered
				if($.trim($("#txtEmgnPhoneNo").val()) !== "") {
					var phoneFieldsFilled = 
							(($.trim($("#txtEmgnPhoneCntry").val()) == "") ? 0 : 1) + 
							(($.trim($("#txtEmgnPhoneArea").val()) == "") ? 0 : 1) +
							(($.trim($("#txtEmgnPhoneNo").val()) == "") ? 0 : 1);
					if(phoneFieldsFilled != 0 && phoneFieldsFilled != 3) {
						showERRMessage(raiseError("XBE-ERR-04","Emergency Contact number of Phone fields entered"));
						$("#txtEmgnPhoneCntry").focus();
						return false;
					}
				} else {
					$("#txtEmgnPhoneCntry").val("");
					$("#txtEmgnPhoneArea").val("");
				}				
			}
			
			if(contactConfig.email2.xbeVisibility && contactConfig.email2.mandatory){
				if($.trim($("#txtEmgnEmail").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Emergency Contact Email address"));
					$("#txtEmgnEmail").focus();
					return false;
				} else {
					$("#txtEmgnEmail").val($.trim($("#txtEmgnEmail").val()));
				}
				
				if(!checkEmail($("#txtEmgnEmail").val())) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Email address"));
					$("#txtEmgnEmail").focus();
					return false;
				}else {
					if(validateEmailDomain){
						if(!UI_tabContactInfo.checkEmailDomain($("#txtEmgnEmail").val())){
							showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Email address"));
							$("#txtEmgnEmail").focus();
							return false;
						}
					}
				} 
				
			} else if(contactConfig.email2.xbeVisibility && 
					!contactConfig.email2.mandatory && $.trim($("#txtEmgnEmail").val()) != ""){
				if(!checkEmail($("#txtEmgnEmail").val())) {
					showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Email address"));
					$("#txtEmgnEmail").focus();
					return false;
				}else {
					if(validateEmailDomain){
						if(!UI_tabContactInfo.checkEmailDomain($("#txtEmgnEmail").val())){
							showERRMessage(raiseError("XBE-ERR-04","Emergency Contact Email address"));
							$("#txtEmgnEmail").focus();
							return false;
						}
					}
				} 
			}	
		}

		if (hasServiceTaxApplied && (!UI_tabContactInfo.isServiceTaxApplicableCountry(serviceTaxApplicableCountries, jsonContactInfo.country) &&
			UI_tabContactInfo.isServiceTaxApplicableCountry(serviceTaxApplicableCountries, $("#selCountry").val()))) {
			showERRMessage(raiseError("XBE-ERR-102"));
			$("#selCountry").focus();
			return false;
		}

		return true;
	}
	
	//validate E-mail domain
	UI_tabContactInfo.checkEmailDomain = function(emailAdd){
		var data = {};
		data['domain'] = emailAdd.substring(emailAdd.indexOf("@")+1);		
		
		var retValue = false;
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', url:"checkEMailDomain.action",async:false,
			data:data,
			success: function(response){ 
			retValue = response.exists;
			if(!retValue){
				showERRMessage(raiseError("XBE-ERR-04","Email address"));
				$("#txtEmail").focus();				
			} 		
		},
		error : UI_commonSystem.setErrorStatus
		});	
		
		return retValue;
	}

	UI_tabContactInfo.validateZipCode = function(field){
		var valid = "0123456789-";
		var hyphencount = 0;
		
		if (field.length!=5 && field.length!=10) {
			return false;
		}
		for (var i=0; i < field.length; i++) {
			var temp = "" + field.substring(i, i+1);
			if (temp == "-"){
				hyphencount++;
			}		
			if (valid.indexOf(temp) == "-1") {
				return false;
			}
			if ((hyphencount > 1) || ((field.length==10) && ""+field.charAt(5)!="-")) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Sets itinerary language in booking info tab
	 */
	UI_tabContactInfo.prefLangOnChange = function(){	
//		  $("#selITNLang").val($("#selPrefLang").val());		 
	}
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	
	