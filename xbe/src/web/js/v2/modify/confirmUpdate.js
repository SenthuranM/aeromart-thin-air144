	/*
	*********************************************************
		Description		: XBE - Confirm Update
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/
	

	var strSegmentSummary = getOpeneri18nData('tabRequote_SegmentSummary','Segment Summary');
	var strOverrideChargesPerPaxSegment = getOpeneri18nData('tabRequote_OverideChargesPerPaxSegment','Override Charges - per PAX/Segment');
	var strPaxSummary = getOpeneri18nData('tabRequote_PaxSummary','Pax Summary');


	var jsPageConf = ({
						defaultMode : {pane1:strSegmentSummary, pane2:strOverrideChargesPerPaxSegment, pane3:strPaxSummary}
						});

	var jsPaneColumnHD = ({defaultMode :
										{
											pane1:{column1:"", column2:"Update OND<br>Charges", column3:"New OND<br>Charges"},
											pane3:{column1:"Pax Name", column2:"Total Charges<br>Current", column3:"Total Charges<br>New", column4:"Total Payments<br>Current", column5:"Total C/F<br>New", column6:"Balance"}
										}
							});
						
	/*
	 * Passenger Tab
	 */
	function UI_confirmUpdate(){}
	UI_confirmUpdate.confirmUpdateMode = "defaultMode";
	UI_confirmUpdate.blnOverRide = false;
	UI_confirmUpdate.blnShowChargeTypeOveride = false;
	UI_confirmUpdate.blnOverrideApplied = false;
	UI_confirmUpdate.chargeTypes = {
		v : "V",pfs:"PFS", pf:"PF"
	}
	UI_confirmUpdate.classHidePaxAd = "hideAdPax";
	UI_confirmUpdate.defaultCnxAdultCharg == "";
	UI_confirmUpdate.defaultCnxChildCharg == "";
	UI_confirmUpdate.overideObj = {
			//maps to ConfirmUpdateOverrideChargesTO
			chargeType:"",routeType:"",precentageVal:"",
			min:"",max:"",
			init: function (type,route, precentage,min,max){
				this.isPresent = function (){return true;},
				this.chargeType = type;
				this.routeType = route;
				this.precentageVal = precentage;
				this.min = min;
				this.max= max;				
				this.getChargeType = function (){return $('#'+this.chargeType).val();};
				this.getChargeType = function (){return $('#'+this.chargeType).val();};
				this.getPrecentage = function (){return $('#'+this.precentageVal).val();};
				this.getMax = function (){return $('#'+this.max).val()};
				this.getMin = function (){return $('#'+this.min).val()};
				this.setDataToJsonObj = function (objName){
					var dataObj = {};				
					dataObj[objName+'.chargeType'] = this.getChargeType();
					dataObj[objName+'.routeType'] = this.routeType;
					dataObj[objName+'.precentageVal'] = this.getPrecentage();
					dataObj[objName+'.min'] = this.getMin();
					dataObj[objName+'.max'] = this.getMax();				
					return dataObj;
				};
				this.validate = function (){
					if(!this.isPresent ()){
						return true;
					}
					if( $('#'+this.precentageVal).val() == ""){
						UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-01"],"Percentage Value")});
						$('#'+this.precentageVal).focus();
						return false;
					}
					if( Number($('#'+this.precentageVal).val()) < 0 || Number($('#'+this.precentageVal).val()) > 100){
						UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-04"],"Precentage Value")});
						$('#'+this.precentageVal).focus();
						return false;
					}
					if( $('#'+this.min).val() == ""){
						UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-01"],"Minimum Value")});
						$('#'+this.min).focus();
						return false;
					}
					if( $('#'+this.max).val() == ""){
						UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-01"],"Maximum Value")});
						$('#'+this.max).focus();
						return false;
					}
					if( Number($('#'+this.max).val()) < Number($('#'+this.min).val())){
						UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-05"],"Minimum Value", "Maximum Value")});	
						$('#'+this.max).focus();
						return false;
					}
					return true;
				};
			},
			type: null

	}
	UI_confirmUpdate.overiedFareMetaData = {
		precentage: "_precentage",
		chargeType: "_type", min: "_prec_min", max : "_prec_max",
		overideOnlyCssTag :"confOverideOnly",		
		adultObj:{isPresent:function (){return false;}},
		childObj:{isPresent:function (){return false;}},
		infantObj : {isPresent:function (){return false;}},
		
		setIDNames : function (sentID,type){
			//sets the dynamic id values		
			var temp_precentage = ""+sentID+this.precentage;
			var temp_type = "selDefineCanType";
			var temp_route = "selDefineRouteType";
			var temp_min = ""+sentID+ this.min;
			var temp_max = ""+sentID + this.max;
			if(type == "AD"){
				this.adultObj= new UI_confirmUpdate.overideObj.init(temp_type, temp_route, temp_precentage, temp_min, temp_max);
				this.adultObj.type = "AD";	
				return this.adultObj;
			}else if(type == "CH"){
				this.childObj= new UI_confirmUpdate.overideObj.init(temp_type, temp_route, temp_precentage, temp_min, temp_max);
				this.childObj.type = "CH";				
				return this.childObj;
			}else if(type == "IN"){
				this.infantObj= new UI_confirmUpdate.overideObj.init(temp_type, temp_route, temp_precentage, temp_min, temp_max);
				this.infantObj.type = "IN";				
				return this.infantObj;
			}
			return null;
		}
		
	}
	/*
	 * Contact Info Tab Page On Load
	 */
	$(document).ready(function(){
		UI_PageShortCuts.initilizeMetaDataForComfirmUpdate();
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		// Charges Tab
		$("#btnClose").decorateButton();
		$("#btnClose").click(function() {UI_confirmUpdate.closeOnclick();});
		
		$("#btnConfirm").decorateButton();
		$("#btnConfirm").click(function() {UI_confirmUpdate.confirmOnclick();});
		
		$("#btnReset").decorateButton();
		$("#btnReset").click(function() {UI_confirmUpdate.resetOnclick();});
		
		$("#btnOverride").decorateButton();
		$("#btnOverride").attr('style', 'width:auto;');
		$("#btnOverride").disable();
		$("#btnOverride").click(function() {UI_confirmUpdate.overrideOnClick();});
		
		$("#btnApply").decorateButton();
		$("#btnApply").disable();
		$("#btnApply").click(function() {UI_confirmUpdate.applyOnClick();});
		
		if (DATA_ResPro.initialParams.hasOverrideCnxModChargePrivilege) {
			$("#btnOverride").show();
			$("#btnApply").show();
		} else {
			$("#btnOverride").hide();
			$("#btnApply").hide();
		}
		
		$("#flexiInfo").hide();
		
		$("#btnCalculateNew").decorateButton();
		$("#btnCalculateNew").click(function() {UI_confirmUpdate.onCalculateNewClick();});
		$('#divCalculateNewCharge').hide();
		$("#txtCNXAdt").numeric({allowDecimal:true});
		
		$("#selDefineCanType").change(function() {UI_confirmUpdate.chargeTypeOnChange(this);});
		$("#selDefineRouteType").change(function() {UI_confirmUpdate.routeTypeOnChange(this);});
		
		$('#selDefineRouteType').disable();
				
		UI_confirmUpdate.onLoad({type:null})
	});
	
	/*
	 * Confirm update On Load
	 */
	UI_confirmUpdate.onLoad = function(inParams){
		var strType = UI_confirmUpdate.confirmUpdateMode;
		if (inParams.type != null){strType = inParams.type;}
		UI_confirmUpdate.loadConfirmUpdate(true);
		
		
	}
	
	UI_confirmUpdate.loadConfirmUpdate = function(blnLoad) {
		if(!blnLoad && UI_confirmUpdate.blnShowChargeTypeOveride){ 
			if( !UI_confirmUpdate.validateCharges()){
				UI_commonSystem.hideProgress();
				opener.UI_commonSystem.readOnly(false);
				return false;
			}
		} else if(!blnLoad && !UI_confirmUpdate.blnShowChargeTypeOveride && $('#selDefineRouteType').val() == "" &&
				(strConfirmUpdateMode == "RESCANCEL" || strConfirmUpdateMode == "REMOVEPAX")){
			UI_commonSystem.hideProgress();			
			UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-01"],"Applicable Basis")});
			$('#selDefineRouteType').focus();
			return false;
		}
		
		//UI_confirmUpdate.chargeTypeOnChange($("#selDefineCanType"));
		opener.UI_commonSystem.readOnly(true);
		if(strConfirmUpdateMode == 'MODIFYSEGMENT'){
			var data ={};
			data['pnr'] = opener.DATA_ResPro.modifySegments.PNR;
			data['groupPNR'] = opener.DATA_ResPro.modifySegments.groupPNR;
			data['modifySegment'] = opener.DATA_ResPro.modifySegments.modifyingSegments;
			data['selCurrency'] = opener.UI_modifySegment.selCurrency;
			data['strMode'] = strConfirmUpdateMode;
			data['resSegments']= opener.UI_modifySegment.allSegments;
			data['flexiSelected'] = opener.UI_tabSearchFlights.isFlexiSelected();
			data['version'] = opener.DATA_ResPro.modifySegments.version;
			data['flexiAlertInfo'] = opener.DATA_ResPro.flexiAlertInfo;
			opener.UI_tabSearchFlights.getFlightRPHList();
			data['flightRPHList'] = opener.UI_tabSearchFlights.fltRPHInfo;
			data['selectedFareType'] = opener.UI_tabSearchFlights.selectedFareType;
			data['defaultAdultCnxCharge'] = UI_confirmUpdate.defaultCnxAdultCharg;
			data['defaultChildCnxCharge'] = UI_confirmUpdate.defaultCnxChildCharg;
			if(!blnLoad) {
				data['adultCnxCharge'] = $('#txtCNXAdt').val();
				data['childCnxCharge'] = $('#txtCNXChld').val();
				data['infantCnxCharge'] = $('#txtCNXInf').val();
				data['routeType'] = $('#selDefineRouteType').val();
				if($('#selDefineCanType').val() != "V" && $('#selDefineCanType').val() != ""){
					UI_confirmUpdate.setChargeTypeDataIfRequired(data);
				}
			} else 	{
				data['adultCnxCharge'] = 'undefined';
				data['childCnxCharge'] = 'undefined';
				data['infantCnxCharge'] = 'undefined';
				data['routeType'] = 'undefined';
			}
			//UI_confirmUpdate.setChargeTypeDataIfRequired(data);
			var fareQuoteData = opener.UI_tabSearchFlights.createFareQuoteParams();					
			data = $.airutil.dom.concatObjects(data,fareQuoteData);
			UI_commonSystem.showProgress();
			//$("#frmConfirmUpdate")
			top.UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:"confirmUpdate.action",							
					success: UI_confirmUpdate.loadConfirmsucess,error:UI_commonSystem.setErrorStatus});	
		}else {
			var data = {};
			data['pnr'] = opener.jsonPNR;
			data['modifySegment'] = opener.UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef;
			data['contactInfo'] = $.toJSON(opener.jsonContactInfo);
			data['selCurrency'] = "";
			data['strMode'] = strConfirmUpdateMode;
			data['groupPNR'] = opener.jsonGroupPNR;
			data['resSegments']=opener.UI_reservation.jsonSegs;	
			data['version']= opener.jsonBkgInfo.version;
			data['flexiAlertInfo'] = opener.$('#flexiAlertInfo').val();
			data['defaultAdultCnxCharge'] = UI_confirmUpdate.defaultCnxAdultCharg;
			data['defaultChildCnxCharge'] = UI_confirmUpdate.defaultCnxChildCharg;
			data['isInfantPaymentSeparated'] = opener.UI_reservation.isInfantPaymentSeparated;
			if(!blnLoad) {
				data['adultCnxCharge'] = $('#txtCNXAdt').val();
				data['childCnxCharge'] = $('#txtCNXChld').val();
				data['infantCnxCharge'] = $('#txtCNXInf').val();
				data['routeType'] = $('#selDefineRouteType').val();
				UI_confirmUpdate.setChargeTypeDataIfRequired(data);
			}
			UI_confirmUpdate.chargeTypeOnChange($("#selDefineCanType"));
//			data = $.airutil.dom.concatObjects(data,opener.UI_reservation.jsonSegs);
			if(strConfirmUpdateMode == 'REMOVEPAX'){
				data['resPaxs']=  $.toJSON(UI_confirmUpdate.getRemovePaxInfo());
				data['paxAdults'] = opener.UI_tabBookingInfo.jsSltdAdt;
				data['paxInfants']= opener.UI_tabBookingInfo.jsSltdInf;				
			}
			UI_commonSystem.showProgress();	
//			$("#frmConfirmUpdate")
			top.UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:"confirmUpdate.action",							
					success: UI_confirmUpdate.loadConfirmsucess,error:UI_commonSystem.setErrorStatus});	
		}
		
	}
	
	//Fill the paxifo only needed for spliting
	UI_confirmUpdate.getRemovePaxInfo = function(){
		var jPassengers = $.parseJSON(opener.UI_reservation.jsonPassengers);
		var jpData = [];
		var splitPax= {"firstName":"", "travelerRefNumber": "", "infants":"", "paxType":"", "paxSequence":""};
		var tmpSplit = {};
		for(var jpl=0;jpl < jPassengers.length;jpl++){
			tmpSplit = $.airutil.dom.cloneObject(splitPax);
			tmpSplit.firstName = jPassengers[jpl].firstName;
			tmpSplit.infants=jPassengers[jpl].infants;
			tmpSplit.paxType=jPassengers[jpl].paxType;
			tmpSplit.travelerRefNumber=jPassengers[jpl].travelerRefNumber;
			tmpSplit.paxSequence = jPassengers[jpl].paxSequence;
			jpData[jpl]= tmpSplit;
		}
		
		return jpData;
	}
	
	UI_confirmUpdate.setChargeTypeDataIfRequired = function (data){
		if(UI_confirmUpdate.blnShowChargeTypeOveride && UI_confirmUpdate.isActulChargeOverideMode()){
			var adult = UI_confirmUpdate.overiedFareMetaData.adultObj;
			var child = UI_confirmUpdate.overiedFareMetaData.childObj;
			var infant = UI_confirmUpdate.overiedFareMetaData.infantObj;
			if(adult.isPresent())
				$.extend(data,UI_confirmUpdate.overiedFareMetaData.adultObj.setDataToJsonObj('adultOverideCharges'));
			if(child.isPresent())
				$.extend(data,UI_confirmUpdate.overiedFareMetaData.childObj.setDataToJsonObj('childOverideCharges'));
			if(infant.isPresent())
				$.extend(data,UI_confirmUpdate.overiedFareMetaData.infantObj.setDataToJsonObj('infantOverideCharges'));
		}
	}
	
	UI_confirmUpdate.loadConfirmsucess = function(response) {
		var strType = UI_confirmUpdate.confirmUpdateMode;
		if(response.success) {
			jsonOverride = response.overRideCharges;
			jsonCharges = response.updateCharge;
			if(strConfirmUpdateMode == "MODIFYSEGMENT"){
				opener.DATA_ResPro.modifySegments.version = response.version;
			}
			jsonPaxSummary = response.paxSummaryList;
			UI_confirmUpdate.blnOverRide = response.blnOverride;
			UI_confirmUpdate.blnShowChargeTypeOveride = response.blnShowChargeTypeOveride;
		
			$("#hdnRouteType").val(response.routeType);
			$("#divHDPane1").html(eval("jsPageConf." + strType + ".pane1"));
			
			$("#divHDPane3").html(eval("jsPageConf." + strType + ".pane3"));
			
			$("#divCredit").html(jsonCharges.displayCredit);
			$("#divDue").html(jsonCharges.displayDue);
			
			UI_confirmUpdate.fillPaneData({type:strType});
			if (DATA_ResPro.initialParams.hasOverrideCnxModChargePrivilege) {
				$("#divHDPane2").html(eval("jsPageConf." + strType + ".pane2"));
				UI_confirmUpdate.constructPane2({data:jsonOverride});
			}		
			else{
				$("#tblOverideDisplay").hide();
			}
			
			if(response.blnOverride && strConfirmUpdateMode != "RESVOID"){
				$("#btnOverride").enable();
				
			}
			if(response.flexiReservation){
				$("#flexiInfo").show();
				if(response.flexiInfo != null && response.flexiInfo != ""){
					$("#spnFlexibilities").html(response.flexiInfo);
				}
			}
			UI_commonSystem.hideProgress();
			$("#btnApply").disable();
		}else {
			if(strConfirmUpdateMode == "MODIFYSEGMENT"){
				opener.UI_modifySegment.showConfirmError(response.messageTxt);
			}else {
				opener.UI_tabBookingInfo.showConfirmError(response.messageTxt);
			}			
			window.close();
			opener.UI_commonSystem.readOnly(false);
		}			
	}
	
	
	/*
	 * Fill Pane Data
	 */
	UI_confirmUpdate.fillPaneData = function(inParams){
		UI_confirmUpdate.constructGridPane1({id:"#tablPane1", columnHD:eval("jsPaneColumnHD." + inParams.type)})
		UI_commonSystem.fillGridData({id:"#tablPane1", data:jsonCharges.chargesList})
		
		UI_confirmUpdate.constructGridPane3({id:"#tablPane3", columnHD:eval("jsPaneColumnHD." + inParams.type)})
		UI_commonSystem.fillGridData({id:"#tablPane3", data:jsonPaxSummary})
	}
	
	/*
	 * Reset Click
	 */
	UI_confirmUpdate.resetOnclick = function(){
		$('#selDefineCanType').val("");
		$('#selDefineRouteType').val("");
		UI_confirmUpdate.loadConfirmUpdate(true);
	}
	
	/*
	 * Close Window
	 */
	UI_confirmUpdate.closeOnclick = function(){
		window.close();
		opener.UI_commonSystem.readOnly(false);
	}
	
	UI_confirmUpdate.validateData = function(){
		if(opener.DATA_ResPro.initialParams.userNotesMandatory){
			if($('#txtUserNote').val() == ""){				
				UI_message.showErrorMessage({messageText:"User Note cannot be left blank."});
				return false;
			}
		}
		return true;
	}
	
	/*
	 * Confirm Click
	 */
	UI_confirmUpdate.confirmOnclick = function(){
	
		if (!UI_confirmUpdate.validateData()){
			return false;
		}
		
		UI_commonSystem.showProgress();		
		if(strConfirmUpdateMode != "MODIFYSEGMENT"){
			var data = opener.UI_tabBookingInfo.getBookingInfo();
			opener.UI_tabBookingInfo.enableAllButtons(false);			
		} else {
			var data = {};
		}
		
		data["groupPNR"]= opener.jsonGroupPNR;
		if(opener.jsonGroupPNR!= 'undefined' && opener.jsonGroupPNR!== '' ){
			data["bookingInfo.groupPNR"] = true;
		}
		data['adultCnxCharge'] = $('#txtCNXAdt').val();
		data['defaultAdultCnxCharge'] = UI_confirmUpdate.defaultCnxAdultCharg ;
		data['defaultChildCnxCharge'] = UI_confirmUpdate.defaultCnxChildCharg;
		data['childCnxCharge'] = $('#txtCNXChld').val();
		data['infantCnxCharge'] = $('#txtCNXInf').val();
		data['routeType'] = ($('#selDefineRouteType').val() == "" || typeof $('#selDefineRouteType').val() == 'undefined') ? $("#hdnRouteType").val() : $('#selDefineRouteType').val();
		data['userNote'] = $('#txtUserNote').val();
		data['voidReservation'] = opener.$('#isVoidReservation').val();
		if($('#selDefineCanType').val() != "V" && $('#selDefineCanType').val() != ""){
			UI_confirmUpdate.setChargeTypeDataIfRequired(data);
		}

		switch (strConfirmUpdateMode){
			case "RESCANCEL" :				
				opener.UI_tabBookingInfo.cancelConfirm(data);
				break;
			case "RESVOID" :				
				opener.UI_tabBookingInfo.cancelConfirm(data);
				break;
			case "CANCELSEGMENT" :				
				opener.UI_tabBookingInfo.cancelSegmentConfirm(data);
				break;				
			case "MODIFYSEGMENT" :
				opener.UI_modifySegment.capturePayment(data);
				break;
			case "REMOVEPAX" :				
				opener.UI_tabBookingInfo.removePaxConfirm(data);
				break;
		}
	}
	
	/*
	 * Override On Click
	 */
	UI_confirmUpdate.overrideOnClick = function(){
		var objPaneCntrl = jsonOverride;
		var intLen = objPaneCntrl.length;
		var i = 0;
		if (intLen > 0){
			do{
				$("#" + objPaneCntrl[i].id).enable();
				i++;
			}while(--intLen);
		}
		$("#" + objPaneCntrl[0].id).focus();
		$("#btnApply").enable();
		$("#btnOverride").disable();
		UI_confirmUpdate.showOverideChargeData(true);
		$('#selDefineCanType').trigger('change');
		if (UI_confirmUpdate.defaultCnxAdultCharg == "" || typeof(UI_confirmUpdate.defaultCnxAdultCharg) == 'undefined') {
			UI_confirmUpdate.defaultCnxAdultCharg = $("#txtCNXAdt").val();
		}
		if (UI_confirmUpdate.defaultCnxChildCharg == "" || typeof(UI_confirmUpdate.defaultCnxChildCharg) == 'undefined') {
			UI_confirmUpdate.defaultCnxChildCharg = $("#txtCNXChld").val();
		}
	}
	
	/*
	 * Override On Click
	 */
	UI_confirmUpdate.overrideDisable = function(){
		var objPaneCntrl = jsonOverride;
		var intLen = objPaneCntrl.length;
		var i = 0;
		if (intLen > 0){
			do{
				$("#" + objPaneCntrl[i].id).disable();
				i++;
			}while(--intLen);
		}
		$("#btnApply").disable();
		UI_confirmUpdate.showOverideChargeData(false);
		
	}
	 
	
	/*
	 * Apply On Click
	 */
	UI_confirmUpdate.applyOnClick = function(){
		UI_commonSystem.showProgress();
		UI_confirmUpdate.blnOverrideApplied = true;
		var strJS = "[";
		
		var objPaneCntrl = jsonOverride;
		var i = 0;
		$.each(objPaneCntrl, function(){
			if (i != 0){strJS += "," }
			strJS += "{" 
			strJS += "\"cntrlID\":\"" + objPaneCntrl[i].id + "\",";
			strJS += "\"" + objPaneCntrl[i].id + "\":\"" + $("#" + objPaneCntrl[i].id).val() + "\"";
			strJS += "}" 
			i++;
		});
		UI_confirmUpdate.loadConfirmUpdate(false);
	}
	
	/*
	 * Apply Override success
	 */
	UI_confirmUpdate.applyOnClickRemoveSuccess = function(response){
		UI_commonSystem.hideProgress();
		
		UI_confirmUpdate.overrideDisable();
		jsonCharges = response.confirmUpdateChargesTO;
		jsonPaxSummary = response.confirmUpdatePaxSummaryListTO;
		
		$("#divCredit").html(jsonCharges.displayCredit);
		$("#divDue").html(jsonCharges.displayDue);
		
		strConfirmUpdateMode = "defaultMode";
		UI_confirmUpdate.fillPaneData({type:strConfirmUpdateMode});
		strConfirmUpdateMode = "REMOVEPAX";
	}
	
	/*
	 * Apply Override success
	 */
	UI_confirmUpdate.applyOnClickSuccess = function(response){
		UI_commonSystem.hideProgress();
		jsonCharges = response.confirmUpdateChargesTO;
		jsonPaxSummary = response.confirmUpdatePaxSummaryListTO;
		
		UI_confirmUpdate.fillPaneData({type:strConfirmUpdateMode});
	}
	
	/*
	 * Construct Grid
	 */
	UI_confirmUpdate.constructGridPane1 = function(inParams){
		//construct grid
		var objHD = inParams.columnHD.pane1;
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 200,
			width: 400,
			colNames : [ objHD.column1, getOpeneri18nData('tabRequote_CurrentOndCharges' ,objHD.column2), getOpeneri18nData('tabRequote_NewOndCharges' ,objHD.column3) ],
			colModel:[
				{name:'displayDesc', index:'displayDesc', width:130, align:"left"},
				{name:'displayOldCharges', index:'displayOldCharges', width:100, align:"right"},
				{name:'displayNewCharges', index:'displayColumn3', width:100, align:"right"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
			}
		});
	}
	
	 UI_confirmUpdate.createOverideCellTop = function (){
		var tblHEadingRow = "";		
		tblHEadingRow = document.createElement("TR");
		var strHTMLText1 = "<label class = 'txtBold' >&nbsp; </label>";
		var strHTMLText2 = "<label class = 'txtBold' >&nbsp; </label>"
		var strHTMLText3 = "<label class = 'txtBold "+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag +"' >%</label>";
		var strHTMLText4 = "<label class = 'txtBold "+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag +"' >Min</label>";
		var strHTMLText5 = "<label class = 'txtBold "+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag +"' >Max</label>";
		tblHEadingRow.appendChild(UI_commonSystem.createCell({desc:strHTMLText1, width:"35%", textCss:'text-align:center;'}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({desc:strHTMLText2, width:"15%", textCss:'text-align:center;'}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({desc:strHTMLText3, width:"18%", textCss:'text-align:center;'}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({desc:strHTMLText4, width:"13%", textCss:'text-align:center;'}));
		tblHEadingRow.appendChild(UI_commonSystem.createCell({desc:strHTMLText5, width:"13%", textCss:'text-align:center;'}));
		return tblHEadingRow;		
	 }
	 
	 UI_confirmUpdate.showOverideChargeData = function (blnHideOrShow){
		 if(blnHideOrShow && UI_confirmUpdate.blnShowChargeTypeOveride){
			 $('.'+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag).show();
			 $('#lblRouteType').show();
			 $('.confValueField').disable();
			 $('#selDefineRouteType').disable();
		 }else if(blnHideOrShow && !UI_confirmUpdate.blnShowChargeTypeOveride){
			 $('.'+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag).hide();			 
			 //$('.confValueField').enable();
			 $('#lblRouteType').show();
			 $('#selDefineRouteType').show();
			 $('#selDefineRouteType').enable();
			 $('.confValueField').disable();
			 //$('#selDefineRouteType').val("TOT");
		 }else{
			 $('.'+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag).hide();			 
			 //$('.confValueField').enable();
			 //$('#lblRouteType').show();
			 //$('#selDefineRouteType').show();			 
			 //$('#selDefineRouteType').val("TOT");
		 }
	 }
	 
	/*
	 * Construct Pane2 
	 */
	UI_confirmUpdate.constructPane2 = function(inParams){
		$("#tablPane2").find("tr").remove();
		var objPaneCntrl = inParams.data;
		var intLen = objPaneCntrl.length;
		
		var i = 0;
		var tblRow = null;
		var tblHEadingRow = null;
		var strHTMLText = "";
		var strHTMLTextPrecentage = "";
		var strHTMLTextMin = "";
		var strHTMLTextMax = "";
		UI_confirmUpdate.blnShowChargeTypeOveride = false;
		$("#tablPane2").append(UI_confirmUpdate.createOverideCellTop());
		if (intLen > 0){
			do{
				strHTMLText = "";	
				var paxTypeObj = UI_confirmUpdate.overiedFareMetaData.setIDNames(objPaneCntrl[i].id,objPaneCntrl[i].paxType);
				var valCls = "";
				
				if(strConfirmUpdateMode == 'REMOVEPAX' && paxTypeObj.type == "AD" &&
						(opener.UI_tabBookingInfo.jsSltdAdt == "" || opener.UI_tabBookingInfo.jsSltdAdt == null)){
					valCls = UI_confirmUpdate.classHidePaxAd;
				}
			
				tblRow = document.createElement("TR");				
				tblRow.appendChild(UI_commonSystem.createCell({desc:objPaneCntrl[i].description + " :", width:"35%", classVal:valCls}));
				if(!UI_confirmUpdate.blnShowChargeTypeOveride) { // if atleaset one type has percentage. should show.
					UI_confirmUpdate.blnShowChargeTypeOveride = objPaneCntrl[i].blnShowChargeTypeOveride;
				}
				switch (objPaneCntrl[i].type){
					case "textBox" :
						
						strHTMLText = "<input type='text' id='" + objPaneCntrl[i].id + "' name='" + objPaneCntrl[i].id + "'  class='aa-input confValueField aa-numericOnly " + valCls + "' style='width:50px; text-align:right;' value='" + objPaneCntrl[i].value + "'>"
						if(UI_confirmUpdate.blnShowChargeTypeOveride){
							UI_confirmUpdate.setPrecentageTypeDD(objPaneCntrl[i].chargeType);
							strHTMLTextPrecentage = "<input type='text' id='" + paxTypeObj.precentageVal + "' name='" + paxTypeObj.precentageVal + "'  class='aa-input aa-numericOnly "+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag + " " + valCls +" ' style='width:50px; text-align:right;' value='" + objPaneCntrl[i].precentageVal + "'></input>"						
							strHTMLTextMin = "<input type='text' id='" +paxTypeObj.min+ "' name='" + paxTypeObj.min+ "'  class='aa-input aa-numericOnly "+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag + " " + valCls + "' style='width:40px; text-align:right;' value='" + objPaneCntrl[i].min + "'></input>"						
							strHTMLTextMan = "<input type='text' id='" +paxTypeObj.max+ "' name='" + paxTypeObj.max+ "'  class='aa-input aa-numericOnly "+UI_confirmUpdate.overiedFareMetaData.overideOnlyCssTag + " " + valCls + "' style='width:40px;text-align:right;' value='" + objPaneCntrl[i].max + "'></input>"						
						}
						break;
				}
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:strHTMLText, width:"15%"}));
				if(UI_confirmUpdate.blnShowChargeTypeOveride){
					tblRow.appendChild(UI_commonSystem.createCell({desc:strHTMLTextPrecentage, width:"18%"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:strHTMLTextMin, width:"13%"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:strHTMLTextMan, width:"13%"}));
				}			
				$("#tablPane2").append(tblRow);
				$("#" + objPaneCntrl[i].id).disable();
				i++;
			}while(--intLen);
		}
		UI_confirmUpdate.showOverideChargeData(false);
		$("."+UI_confirmUpdate.classHidePaxAd).hide();
		if(UI_confirmUpdate.blnShowChargeTypeOveride){
			$('.confValueField').disable();
		}
		UI_confirmUpdate.applyEventToDynamicInputs();
		
	}
	 
	 UI_confirmUpdate.setPrecentageTypeDD = function (strVal){
		 if($('#selDefineCanType').val()=="" || $('#selDefineCanType').val == null){
			 $('#selDefineCanType').val(strVal);
		 }		
	 }
	 
	 UI_confirmUpdate.applyEventToDynamicInputs = function (){
		 $('.aa-numericOnly').bind('keypress',(function (){JqValidations.kpLikeDecimal(this.id)}));
		 $('.aa-numericOnly').bind('keyup',(function (){JqValidations.kpLikeDecimal(this.id)}));
	
	 }
	 
	/*
	 * Construct Grid
	 */
	UI_confirmUpdate.constructGridPane3 = function(inParams){
		//construct grid
		var objHD = inParams.columnHD.pane3;
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 200,
			width: 830,
			colNames : [ getOpeneri18nData('tabRequote_PassengeName',objHD.column1), getOpeneri18nData('tabRequote_TotalCurrentCharges',objHD.column2), getOpeneri18nData('tabRequote_TotalNewCharges',objHD.column3),
                         getOpeneri18nData('tabRequote_TotalCurrentPayment',objHD.column4), getOpeneri18nData('tabRequote_TotalCFNew',objHD.column5), getOpeneri18nData('tabRequote_Balance',objHD.column6)],
			colModel:[
				{name:'displayPaxName', index:'displayPaxName', width:250, align:"left"},
				{name:'displayTotalChargesCurrent', index:'displayTotalChargesCurrent', width:110, align:"right"},
				{name:'displayTotalChargesNew', index:'displayTotalChargesNew', width:110, align:"right"},
				{name:'displayTotalPaymentsCurrent', index:'displayTotalPaymentsCurrent', width:110, align:"right"},
				{name:'displayTotalCFNew', index:'displayTotalCFNew', width:110, align:"right"},
				{name:'displayBalance', index:'displayBalance', width:110, align:"right"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
			}
		});
	}
	 
	 UI_confirmUpdate.validateCharges = function (){		
		if ($('#selDefineCanType').val() == "") {
			UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-01"],"Override Basis")});
			$('#selDefineCanType').focus();
			return false;
		}else if($('#selDefineCanType').val() == UI_confirmUpdate.chargeTypes.v && $('#selDefineRouteType').val() == "" 
			&& (strConfirmUpdateMode == "RESCANCEL" || strConfirmUpdateMode == "REMOVEPAX")){
			UI_message.showErrorMessage({messageText: buildError(opener.top.arrError["XBE-ERR-01"],"Applicable Basis")});
			$('#selDefineRouteType').focus();
			return false;
		}		
		
		if(UI_confirmUpdate.isActulChargeOverideMode()){
			var adult = UI_confirmUpdate.overiedFareMetaData.adultObj;
			var child = UI_confirmUpdate.overiedFareMetaData.childObj;
			var infant = UI_confirmUpdate.overiedFareMetaData.infantObj;
			if (adult.isPresent() && !adult.validate()) {
				return false;
			}
			if (child.isPresent() && !child.validate()) {
				return false;
			}
			if (infant.isPresent() && !infant.validate()) {
				return false;
			}
		}
		return true;
	 }
	 
	 UI_confirmUpdate.isActulChargeOverideMode = function (){
		 if(UI_confirmUpdate.chargeTypes.v == $('#selDefineCanType').val()){
			 return false;
		 }else {
			 return true;
		 }
	 }
	 
	 UI_confirmUpdate.chargeTypeOnChange = function (obj){
		 if(UI_confirmUpdate.blnShowChargeTypeOveride){			 
			 var currVal = obj.value;
			 if($('#selDefineCanType').val() == "" ){
				 UI_confirmUpdate.showOverideChargeData(false);
				 $('#selDefineRouteType').hide();
				 $('#lblRouteType').hide();
				 $('#selDefineRouteType').val("TOT");
				 $('.confValueField').disable();
			 }else if(UI_confirmUpdate.chargeTypes.v == $('#selDefineCanType').val() && 
					 (strConfirmUpdateMode == "RESCANCEL" || strConfirmUpdateMode == "REMOVEPAX") ){
				 UI_confirmUpdate.showOverideChargeData(false);
				 $('#lblRouteType').show();
				 $('#selDefineRouteType').enable();
				 $('.confValueField').enable();
			 }else if(UI_confirmUpdate.chargeTypes.v == $('#selDefineCanType').val() && 
					 (strConfirmUpdateMode == "CANCELSEGMENT" || strConfirmUpdateMode == "MODIFYSEGMENT") ){
				 UI_confirmUpdate.showOverideChargeData(false);
				 $('#selDefineRouteType').hide();
				 $('#lblRouteType').hide();
				 $('#selDefineRouteType').val("TOT");
				 $('.confValueField').enable();
			 }else {
				 $('#selDefineRouteType').hide();
				 $('#lblRouteType').hide();
				 $('#selDefineRouteType').val("TOT");
				 UI_confirmUpdate.showOverideChargeData(true);
				 $('.confValueField').disable();
			 }		
			 $('.overideDisplay').show();
			 $('#lblRouteType').show();
			 $('#selDefineCanType').val(currVal);
		 }		
		
	 }
	 
	 UI_confirmUpdate.routeTypeOnChange = function (obj){		 	 
			 var currVal = obj.value;
			 if($('#selDefineRouteType').val() == ""){
				 UI_confirmUpdate.showOverideChargeData(false);
				 $('.confValueField').disable();
			 }	else {
				 UI_confirmUpdate.showOverideChargeData(false);
				 $('.confValueField').enable();
			 }
			 
			 if(UI_confirmUpdate.blnShowChargeTypeOveride){				 
				 $('.overideDisplay').show();
				 $('#lblRouteType').show();
				 $('#selDefineRouteType').val(currVal);	
			 }else {
				 $('#lblRouteType').show();
				 $('#selDefineRouteType').show();
				 $('#selDefineRouteType').enable();
				 $('#selDefineRouteType').val(currVal);	
			 }
		
	 }
	 
	 $(window).unload(function(){opener.UI_commonSystem.readOnly(false);})
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	
	