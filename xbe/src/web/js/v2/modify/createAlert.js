jQuery(document).ready(function(){	

	$("#btnSave").decorateButton();
	$("#btnSave").enableButton();
	$("#btnSave").attr('style', 'width:auto;');
	
	$("#btnClose").decorateButton();
	$("#btnClose").enableButton();
	$("#btnClose").attr('style', 'width:auto;');
	
	var options = {
			cache: false,	 
			beforeSubmit:  showRequest,
			success: showResponse,  
			dataType:  'json'
	}; 
	
	$("#btnSave").click(function() {
		$("#jsonGroupPnr").val(opener.jsonGroupPNR);
		$("#frmCreateAlert").ajaxSubmit(options);
	});
	
	$("#btnClose").click(function() {
		window.close();
	});
	
	$("#txtDescription").keyup(function(){
	    if($(this).val().length > 4000){
	        $(this).val($(this).val().substr(0, 4000));
	    }
	});
	    
	function showRequest(jqXHR, options) { 
		if($("#txtDescription").val() == ""){
			UI_message.showErrorMessage({messageText:"Description cannot be empty"})
			return false;
		}
	}
	
	function showResponse(responseText, statusText) {
		if(responseText.success){
			window.close();
			opener.UI_tabBookingInfo.createAlertSuccess(responseText);
		} else {
		    UI_message.showErrorMessage({messageText:responseText.messageText});
		}
	}
		
});