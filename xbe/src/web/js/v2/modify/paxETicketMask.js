	/*
	*********************************************************
		Description		: XBE - Pax E-Ticket Mask Segment Wise
		Author			: M.Rikaz
		Version			: 1.0
		Created			: 07/07/2011
		Last Modified	: 07/07/2011
	*********************************************************	
	*/

	function UI_paxETckMask(){}
	
	UI_paxETckMask.saveCouponSuccessMsg = "Coupon successfully updated";
	UI_paxETckMask.reloadBooking = false;
	var jsonPaxCouponObj = [		 					
		 					{id:"#selCouponStatus", desc:"Coupon Status", required:true},
		 					{id:"#selPaxStatus", desc:"Passenger Status", required:true},
		 					{id:"#txtUN", desc:"User notes", required:true}
		 					];
	
	/*
	 * Contact Info Tab Page On Load
	 */
	$(document).ready(function(){
		
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		UI_paxETckLoadData.ready();		
		
		$("#btnClose").decorateButton();		
		$("#btnClose").click(function() {UI_paxETckMask.closeOnclick();});	
		
		$("#selEticketStatus").fillDropDown({dataArray:opener.top.arrAllCouponStatus, keyIndex:0 , valueIndex:1, firstEmpty:true });		
		
		UI_paxETckMask.onLoadCall();		
		
		if( !DATA_ResPro.initialParams.allowChangePassengerCouponStatus ){
			$("#passengerCouponInput").hide();
			$("#btnSave").hide();
			$("#btnEdit").hide();
			
		}else {
			$("#btnEdit").decorateButton();
			$("#btnSave").decorateButton();
			
			$("#selCouponStatus").fillDropDown({dataArray:opener.top.arrAllCouponStatus, keyIndex:0 , valueIndex:1, firstEmpty:true });
			$("#selPaxStatus").fillDropDown({dataArray:opener.top.arrAllPaxStatus, keyIndex:0 , valueIndex:1, firstEmpty:true });
			
			$("#btnSave").click(function() {UI_paxETckMask.saveModifiedCoupon();});
			$("#btnEdit").click(function() {UI_paxETckMask.editOnClick();});
			
			$("#btnSave").disable();
			$("#btnEdit").disable();
			$("#frmPassengerCoupon").disableForm();
		}
		
		UI_paxETckMask.selPaxOnChange();
	});
	
	$(window).unload(function() {
		UI_paxETckMask.forceReloadReservation();
	});
	
	/*
	 * Reset On Click
	 */
	UI_paxETckMask.resetOnclick = function(){
		$("#frmPassengerCoupon").reset();
	}
	
	/*
	 * User Notes Close
	 */
	UI_paxETckMask.closeOnclick = function(){
		UI_paxETckMask.close();
	}
	
	/*
	 * On Load Call
	 */
	UI_paxETckMask.onLoadCall = function(){
		UI_paxETckMask.fillDropDown(strPaxID);				
	}
	
	/*
	 * Pax Drop down On Change
	 */
	UI_paxETckMask.selPaxOnChange = function(){	
		
		$("#selSegmentStatus").val("").attr("selected", "selected");
		$("#selEticketStatus").val("").attr("selected", "selected");		
		UI_paxETckMask.selOnChange();
	}
	
	/*
	 * Drop down On Change
	 */
	UI_paxETckMask.selOnChange = function(){		
		var data = {};		
		data['travelerRefNum'] = $("#selPax").val();		
		data['eTicketStatus'] = $("#selEticketStatus").val();
		data['segmentStatus'] = $("#selSegmentStatus").val();
		data['ticketCoupons'] = opener.UI_reservation.jsonPassengerTktCoupons;	
		
		$("#frmPaxETckMask").action("loadPaxETicketMask.action");
		$("#frmPaxETckMask").ajaxSubmit({ dataType: 'json', processData: false, data:data,
										success: UI_paxETckMask.selPaxChangeSuccess, error:UI_commonSystem.setErrorStatus});
		return false;
	}
	
	UI_paxETckMask.editOnClick = function(){		
		$("#btnSave").enable();	
		$("#frmPassengerCoupon").enableForm();
	}
	
	UI_paxETckMask.saveModifiedCoupon = function(){	
		
		if (UI_paxETckMask.validatePassengerCoupon()){	
			var data = {};
			data['pnr'] = opener.jsonPNR;;
			data['travelerRefNum'] = $("#selPax").val();		
			data['passengerName'] = $("#selPax").text();
			data['remark'] = $("#txtUN").val();
			data['modifiedCoupon.eticketId'] = UI_paxETckLoadData.selectedCouponObj['eticketId'];
			data['modifiedCoupon.carrierCode'] = UI_paxETckLoadData.selectedCouponObj['carrierCode'];
			data['modifiedCoupon.paxETicketStatus'] =$("#selCouponStatus").val();
			data['modifiedCoupon.paxStatus'] =$("#selPaxStatus").val();
			data['modifiedCoupon.ppfsId'] =UI_paxETckLoadData.selectedCouponObj['ppfsId'];
			
			data['ticketCoupons'] = opener.UI_reservation.jsonPassengerTktCoupons;	
			
			$("#frmPaxETckMask").action("loadPaxETicketMask!saveModifiedCoupon.action");
			
			$("#frmPaxETckMask").ajaxSubmit({ dataType: 'json', processData: false, data:data,
				success: UI_paxETckMask.saveModifiedCouponSuccess, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	/*
	 * Ajax Return Call for passenger coupon modify
	 */
	UI_paxETckMask.saveModifiedCouponSuccess = function(response){
		if(response.success) {
			UI_paxETckMask.reloadBooking = true;
			UI_message.showConfirmationMessage({messageText:UI_paxETckMask.saveCouponSuccessMsg});
			opener.UI_reservation.jsonPassengerTktCoupons = $.toJSON(response.updatedPassengerTicketCoupons);
			UI_paxETckMask.selOnChange();						
		}else{
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}		
	}	
	
	/*
	 * Ajax Return Call for passenger on change
	 */
	UI_paxETckMask.selPaxChangeSuccess = function(response){
		if(response.success) {
			UI_paxETckLoadData.fillData(response.passengerTicketCoupons,response.enableCpnCtrl);
			$("#divPNRPaxId").html(response.pnrPaxId);		
		}else{
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}
		UI_paxETckMask.resetOnclick();
	}
	

	UI_paxETckMask.close = function(){
		window.close();
	}

	UI_paxETckMask.forceReloadReservation = function(){
		if(UI_paxETckMask.reloadBooking){
			UI_paxETckMask.reloadBooking = false;	
			if(opener.DATA_ResPro.initialParams.restrictModForCnxFlown) {
				if(opener.jsonGroupPNR != undefined && opener.jsonGroupPNR != ""){
					opener.top.blnInterLine = true;
				}
				opener.top.loadNewModifyBooking(opener.jsonPNR, false);
			} else {
				opener.UI_reservation.loadReservation("", null, false);	
			}	
		}
	}
	
	UI_paxETckMask.setSelectedCouponDetail = function(){		
		
		if(DATA_ResPro.initialParams.allowChangePassengerCouponStatus && UI_paxETckLoadData.selectedCouponObj != null){			
			$('#txtSegmentCode').val(UI_paxETckLoadData.selectedCouponObj['segmentCode']);
			$('#txtFlightNum').val(UI_paxETckLoadData.selectedCouponObj['flightNo']);
			$('#txtTktNumber').val(UI_paxETckLoadData.selectedCouponObj['paxETicketNo'] + " / " + UI_paxETckLoadData.selectedCouponObj['couponNo']);			
			$('#selCouponStatus').val(UI_paxETckLoadData.selectedCouponObj['paxETicketStatus']);
			$('#selPaxStatus').val(UI_paxETckLoadData.selectedCouponObj['paxStatus']);	
			
			$("#btnEdit").enable();			
			$("#btnSave").disable();					
			$("#frmPassengerCoupon").disableForm();
		}		
	}
	
	UI_paxETckMask.validatePassengerCoupon = function(){
		
		/* mandatory Validations */
		if (!UI_commonSystem.validateMandatoryWindow(jsonPaxCouponObj)){return false;}
		
		/* Invalid Character Validations */
		if (!UI_commonSystem.validateInvalidCharWindow(jsonPaxCouponObj)){return false;}		 	
		
		if ($.trim($("#txtUN").val()).length > 250) {
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-18", "250", "User Notes")});
			$("#txtUN").focus();
			return false;
		}
		
		return true;		
	}
	
	/*
	 * Fill Drop Down
	 */
	UI_paxETckMask.fillDropDown = function(strPaxID){
		//append ADT and CHD 
		var objPax = opener.jsonPaxAdtObj.paxAdults;
		for (var i=0; i< objPax.length; i++) {
			if(objPax[i].displayAdultTitle != null){
				$("#selPax").append('<option value="' + objPax[i].displayPaxTravelReference + '">' + objPax[i].displayAdultTitle + '. ' + objPax[i].displayAdultFirstName + ' ' + objPax[i].displayAdultLastName + '</option>');
			}else{
				$("#selPax").append('<option value="' + objPax[i].displayPaxTravelReference + '">' + objPax[i].displayAdultFirstName + ' ' + objPax[i].displayAdultLastName + '</option>');
			}			
		}
		//append INF
		var objPax =  opener.jsonPaxAdtObj.paxInfants;
		for (var i=0; i< objPax.length; i++) {
				var infTitle  = "";
				if(objPax[i].displayInfantTitle != null){
					infTitle = objPax[i].displayInfantTitle + '. ';
				}
				$("#selPax").append('<option value="' + objPax[i].displayPaxTravelReference + '">' + infTitle + objPax[i].displayInfantFirstName + ' ' + objPax[i].displayInfantLastName + '</option>');						
		}
		$("#selPax").val(strPaxID).attr("selected", "selected");		
	}
