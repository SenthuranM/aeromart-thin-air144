	/*
	*********************************************************
		Description		: XBE Make Reservation - History Info Tab
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/


	/*
	 * History Info Tab
	 */
	function UI_tabHistoryInfo(){}
	
	UI_tabHistoryInfo.blnLoaded = false;
		
	/*
	 * Contact Info Tab Page On Load
	 */
	UI_tabHistoryInfo.ready = function(){
		
		if(!UI_tabHistoryInfo.blnLoaded) {			
			$("#selCarrierCodes").change(function() {UI_tabHistoryInfo.onCarrierCodeChange();});
			UI_tabHistoryInfo.fillBookingInfo();
			UI_tabHistoryInfo.constructGrid({id:"#tblHistory"});
			UI_tabHistoryInfo.fillCarrierCodesDropdown(jsonCarrierOptions);
		}
		$("#btnPrintHistory").decorateButton();
		$("#btnPrintHistory").attr('style', 'width:auto;');
		$("#btnPrintHistory").click(function() {UI_tabHistoryInfo.viewHistoryOnClick();});
		
		if(!DATA_ResPro.initialParams.printPNRHistoryAllow){
			$("#btnPrintHistory").hide();
		}
		UI_tabHistoryInfo.initLoad();
		
	}
	
	UI_tabHistoryInfo.viewHistoryOnClick = function() {
		UI_tabHistoryInfo.printHistory();
	}

	UI_tabHistoryInfo.printHistory = function() {
		var historyLanguage = $("#selITNLang").val();
		var strUrl = "printHistory.action" 
			+ "?pnr=" + jsonBkgInfo.PNR
			+ "&historyLanguage=" + historyLanguage;

		top.objCW = window.open(strUrl, "myWindow", $("#popWindow").getPopWindowProp(630, 900, true));
		if (top.objCW) {top.objCW.focus()}
	}
	
	UI_tabHistoryInfo.initLoad = function() {
		UI_commonSystem.showProgress();
		UI_tabHistoryInfo.fillCarrierCodesDropdown(jsonCarrierOptions);
		var data = {};	
		data['groupPNR'] = jsonGroupPNR;
		data['pnr'] = jsonPNR;
		data['carrierCode'] = $("#selCarrierCodes").val();
		data['originSalesChanel'] = UI_reservation.originSalesChanel;
		var frmUrl = "loadReservationHistory.action";
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData:false, data:data, url:frmUrl, 
			success: UI_tabHistoryInfo.selcarrierCodeSuccess, error:UI_commonSystem.setErrorStatus});
		//$("#selCarrierCodes").val(0);
		UI_tabHistoryInfo.setDefaultCarrier();
		UI_tabHistoryInfo.onCarrierCodeChange();
		return false;
	}	
	
	UI_tabHistoryInfo.setDefaultCarrier = function(){
		if(UI_reservation.jsonONDs != ""){
			var ondJson = jQuery.parseJSON(UI_reservation.jsonONDs);
			var carrier = "";
			var isSingleCarrier = true;
			for(var i=0;i<ondJson.length;i++){
				if(i != 0){
					if(carrier != ondJson[i].carrierCode){
						isSingleCarrier = false;
						break;
					}
				}
				carrier = ondJson[i].carrierCode;
			}
			
			if(isSingleCarrier){
				$("#selCarrierCodes").val(carrier);
			} else {
				$("#selCarrierCodes").val(0);
			}				
		} else {
			$("#selCarrierCodes").val(0);
		}
	}

	/*
	 * Fill Booking Info
	 */
	UI_tabHistoryInfo.fillBookingInfo = function(){
		$("#divPnrNoHstr").html(jsonBkgInfo.PNR);
		var status = jsonBkgInfo.displayStatus;
		$("#divBkgStatusHstr").html(status);
		$("#divAgentHstr").html(jsonBkgInfo.agent);
	}
	
	/*
	 * Grid Constructor
	 */
	UI_tabHistoryInfo.constructGrid = function(inParams){
		//construct grid
		$(inParams.id).jqGrid({
			datatype: "local",		
			height: 500,
			width: 880,
			colNames:[ geti18nData('History_lbl_Date','Date'),geti18nData('History_lbl_Action','Action') , geti18nData('History_lbl_SystemNote','System Note'), geti18nData('History_lbl_UserNote','User Note'), geti18nData('History_lbl_UserName','User Name')],
			colModel:[
				{name:'displayModifyDate',  index:'displayModifyDate', width:80, align:"center"},
				{name:'displayAction',  index:'displayAction', width:100, align:"left"},
				{name:'displaySystemNote', index:'displaySystemNote', width:260, align:"left"},
				{name:'displayUserNote', index:'displayUserNote', width:290, align:"left"},
				{name:'displayUserName', index:'displayUserName', width:150, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true
		});
	}
	
	UI_tabHistoryInfo.fillCarrierCodesDropdown= function(optionStr){
		$("#selCarrierCodes").children().remove();
		$("#selCarrierCodes").append(optionStr);
	}
	
	UI_tabHistoryInfo.onCarrierCodeChange= function(){
		UI_commonSystem.pageOnChange();
		UI_commonSystem.showProgress();
		var data = {};	
		data['groupPNR'] = jsonGroupPNR;
		data['pnr'] = jsonPNR;
		data['carrierCode'] = $("#selCarrierCodes").val();
		data['originSalesChanel'] = UI_reservation.originSalesChanel;
		var frmUrl = "loadReservationHistory.action";
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData:false, data:data, url:frmUrl, 
			success: UI_tabHistoryInfo.selcarrierCodeSuccess, error:UI_commonSystem.setErrorStatus});
		return false;
	}
	
	UI_tabHistoryInfo.selcarrierCodeSuccess = function(response){
		UI_commonSystem.hideProgress();
		if(response!= null && response.success) {
			if(!UI_tabHistoryInfo.blnLoaded){				
				UI_tabHistoryInfo.blnLoaded = true;
			}
			UI_commonSystem.fillGridData({id:"#tblHistory", data:response.reservationAuditList});
			UI_reservation.tabStatus.tab3 = true;
			//$("#tblHistory").iterateTemplete({templeteName:"tblHistory", data:response.reservationAuditList, dtoName:"reservationAuditList"});
		}else {
			if(!UI_tabHistoryInfo.blnLoaded){	
					UI_tabHistoryInfo.blnLoaded = true;			
			}
			showERRMessage(response.messageTxt);
		}
		if(jsonGroupPNR == ""){
			$("#selCarrierCodes").hide();
		}
		
	}
	
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	
	