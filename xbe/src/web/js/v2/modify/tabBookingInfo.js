	/*
	*********************************************************
	Description		: XBE Make Reservation - Booking Info Tab
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	var paxValidation;
	
	
	var adultAgeMand = top.isAdAgeMandXbe;
	var chaildAgeMand = top.isChAgeMandXbe ;
	
	var tempCarrierList = "";
	var tempPaxType = "";
	
	var paymentType = "";
	var infantWithWrongAdult = false;
	
	var jsonPaxIntlFltObj = ( {
		"arrivalFlights" : [],

		"DepartureFlights" : [],
		
		"pnrPaxGroupIds" :[]
	});
	var isDuplicateNameExist = false;

	var isDuplicateFFIDExist = false;
	
	var isConfirmDateExpired = false;
	
	var isConfirmDateExpired = false;
	
	//Value accessible from paxDetails.js. Used to specify whether passport details were validated or not
	var passportDetailsvalidated=true;
	
	var isTBABooking = false;

	//Value to make the close button of file upload dialog not popup the paxdetails window if parsing has failed.
	var parseSuccessful=false;
	
	var chargeAdjustments=null;
	
	//Carrier of the last OND selected. Only applicable to LCC using environments
	var lastSelectedONDCareer=null;
	var selectedONDs=[];
	var selectedPax=[];
	var chargeAdjustments=null;
	var waitListedSegmentAvailable = false;
	var csvFileUpload = false;
	var internationalFlightDetailsValidated = true;
	
	var jsonInfPaxTypes = ":;MSTR:Mstr;MISS:Miss";
	/*
	 * Passenger Tab
	 */
	function UI_tabBookingInfo(){}
	
	UI_tabBookingInfo.pnrPaxidAndTaxMap = {};
	UI_tabBookingInfo.intLastSSRRow = null;
	UI_tabBookingInfo.strLastSSRType = null;
	
	UI_tabBookingInfo.jsSltdAdt = "";
	UI_tabBookingInfo.jsSltdInf = "";

	UI_tabBookingInfo.foidIds = "";
	UI_tabBookingInfo.foidCode = "";
	UI_tabBookingInfo.segmentList = "";
	
	UI_tabBookingInfo.autoCnxAlertMap = {};
	UI_tabBookingInfo.loadAutoCnxBalanceToPay = true;
	UI_tabBookingInfo.autoCnxBalanceTOPayTot = 0;
	
	UI_tabBookingInfo.promoCodeAlertText = null;
	
	UI_tabBookingInfo.selectedFlightSeg = {
		intLastSltdFltSegRef: null,
		lastSelectedFlightSegRefNumber : null,
		lastSelectedPnrSegID:null,
		modifyByDate:null,
		modifyByRoute:null,
		clear : function (){
			this.intLastSltdFltSegRef = null;
			this.lastSelectedFlightSegRefNumber = null;
			this.lastSelectedPnrSegID = null;
			this.modifyByDate;
			this.modifyByRoute;
		}
	}
	
	UI_tabBookingInfo.blnLoaded = false;
	UI_tabBookingInfo.carrirCode = "";		
	UI_tabBookingInfo.PNRSegID = "";
	UI_tabBookingInfo.referenceID = "";
	UI_tabBookingInfo.modifyFlexiBooking = false;
	UI_tabBookingInfo.fltOutDate = "";
	UI_tabBookingInfo.FirstNonFlwnfltOutDate = "";
	UI_tabBookingInfo.flightSegmentGroup = {};
	UI_tabBookingInfo.xbeTBA = "T B A";
	UI_tabBookingInfo.grpBookingPax = null;
	
	UI_tabBookingInfo.privilageGroupItinerary = true;
	 
	UI_tabBookingInfo.booleanIsSurfaceSeg = null;
 
	UI_tabBookingInfo.addGroundSegmentSelectData = {
			selectedOnD : "",
			clear : function (){this.selectedOnD = ""}
	}
	
	UI_tabBookingInfo.ohdRFSubmitted = false;
    UI_tabBookingInfo.rollForwardAvailFlights = [];
	UI_tabBookingInfo.rfMinDate = "";
 
	UI_tabBookingInfo.WLflightSegId = "";

	var ccPayData = {};
	UI_tabBookingInfo.selectedTxnIds = [];
        UI_tabBookingInfo.allPayments = {};
	UI_tabBookingInfo.creditCardPaymentsMap = {};
	UI_tabBookingInfo.savedInfants=[];
	var isModifyResEnabled = false;

	UI_tabBookingInfo.tempPaxAdults = [];
    UI_tabBookingInfo.tempPaxInfants = [];
    
    UI_tabBookingInfo.lastFltArrivalDate = null;
    
    var gdsCsReservationModAllowed = true;
    // common group pax status change data 
    UI_tabBookingInfo.fltSegPaxMap = null; 
  
 	UI_tabBookingInfo.isGroupPnr = false;

	//Hide traditional add/modify/cancel segment buttons
	UI_tabBookingInfo.hideTraditionalModificatonButtons = function(){
		$("#btnModSegment").hide();
		$("#btnAddSegment").hide();
		$("#btnAddGroundSegment").hide();
		$("#btnCanSegment").hide();
	}
	
	/*
	 * Booking Info Tab Page On Load2
	 */
	
	UI_tabBookingInfo.ready = function(){
		if(!UI_tabBookingInfo.blnLoaded) {
			
			var data = new Array();
			if(jsonPaxAdtObj.paxAdults.length > 0){
				data['paxType'] = (jsonPaxAdtObj.paxAdults[0].displayPaxCategory == null) ? "A":jsonPaxAdtObj.paxAdults[0].displayPaxCategory;
			} else if(jsonPaxAdtObj.paxInfants.length> 0){
				data['paxType'] = (jsonPaxAdtObj.paxInfants[0].displayPaxCategory == null) ? "A":jsonPaxAdtObj.paxInfants[0].displayPaxCategory;
			}
			data['carriers'] = UI_tabBookingInfo.createCarrierList();
			data['system'] = jsonSystem;
            
			tempCarrierList = data['carriers'];
			if(!UI_tabBookingInfo.checkPaxContactConfigAvailable(data['carriers'], data['paxType'])){		
				tempPaxType = data['paxType'];
				UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, async:false, url:"loadPaxContactConfig.action",							
					success: UI_tabBookingInfo.contactConfigSuccess,error:UI_commonSystem.setErrorStatus});
			}else{
				if(DATA_ResPro.initialParams.countryPaxConfigEnabled){
					var carrierStr = top.carrierCode;
					var paxType = data['paxType'];
					top.globalPaxContactConfig[carrierStr+paxType] = top.tempGlobalPaxContactConfig[carrierStr+paxType];
				}
			}

			if(DATA_ResPro.initialParams.allowDisplayPaxNames == false){
				$('#btnPaxNames').hide();
			}
			
			if(DATA_ResPro.initialParams.countryPaxConfigEnabled){
				var submitData = {};
				submitData['system'] = jsonSystem;
				submitData['origin'] = UI_reservation.actualOrigin;
				submitData['destination'] = UI_reservation.actualDestination;
				if(jsonPaxAdtObj.paxAdults.length > 0){
					submitData['paxType'] = (jsonPaxAdtObj.paxAdults[0].displayPaxCategory == null) ? "A":jsonPaxAdtObj.paxAdults[0].displayPaxCategory;
				} else if(jsonPaxAdtObj.paxInfants.length> 0){
					submitData['paxType'] = (jsonPaxAdtObj.paxInfants[0].displayPaxCategory == null) ? "A":jsonPaxAdtObj.paxInfants[0].displayPaxCategory;
				}
				UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:submitData, async:false, url:"loadPaxContactConfigCountry.action",							
					success: UI_tabBookingInfo.contactConfigCountrySuccess,error:UI_commonSystem.setErrorStatus});
			}
			
			
			if (jsonGroupPNR == "null" || jsonGroupPNR == "") {
				$('#chkChg').show();
			} else {
				$('#chkChg').hide();
				$('#chgTxtSpan').hide();
			}
			
			isModifyResEnabled = false;
			
			if (DATA_ResPro.initialParams.addSegment && !DATA_ResPro.initialParams.voidReservation){	
				isModifyResEnabled = true;
				$("#btnAddSegment").decorateButton();
				$("#btnAddSegment").attr('style', 'width:auto;');				
				$("#btnAddSegment").click(function() {UI_tabBookingInfo.addSegmentOnClick({blnIsGroundStationAllowed:false});});				
				$("#btnAddSegment").show();
				$("#btnAddGroundSegment").decorateButton();
				$("#btnAddGroundSegment").attr('style', 'width:auto;');				
				$("#btnAddGroundSegment").click(function() {UI_tabBookingInfo.addSegmentOnClick({blnIsGroundStationAllowed:true});});				
				// $("#btnAddGroundSegment").disable();
				
				if (DATA_ResPro.initialParams.groundServiceEnabled) {
					$("#btnAddGroundSegment").show();
				} else {
					$("#btnAddGroundSegment").hide();
				}		
			}else{
				$("#btnAddSegment").hide();
				$("#btnAddGroundSegment").hide();
			}
			
						

			if (DATA_ResPro.initialParams.modifySegmentByDate || DATA_ResPro.initialParams.modifySegmentByRoute){
				isModifyResEnabled = true;
				$("#btnModSegment").decorateButton();
				$("#btnModSegment").attr('style', 'width:auto;');
				$("#btnModSegment").disable();
				$("#btnModSegment").click(function() {UI_tabBookingInfo.modifySegmentOnClick();});
				
			}else{
				$("#btnModSegment").hide();
			}		
			
			if (DATA_ResPro.initialParams.cancelSegment){
				isModifyResEnabled = true;
				$("#btnCanSegment").decorateButton();
				$("#btnCanSegment").attr('style', 'width:auto;');				
				$("#btnCanSegment").click(function() {UI_tabBookingInfo.cancelSegmentOnClick();});
			}else{
				$("#btnCanSegment").hide();
			}
			
			if ((isGdsPnr && !DATA_ResPro.initialParams.gdsReservationModAllowed)
					|| (isCsPnr && !DATA_ResPro.initialParams.reservationModificationsAllowed)) {
				gdsCsReservationModAllowed = false;
				isModifyResEnabled = false;
			}
									
			if (isModifyResEnabled && DATA_ResPro.initialParams.requoteEnabled) {
				$("#btnModifyRes").decorateButton();
				$("#btnModifyRes").attr('style', 'width:auto;');
				$("#btnModifyRes").click(function() {UI_tabBookingInfo.addSegmentOnClick({blnIsGroundStationAllowed:false});});
				UI_tabBookingInfo.hideTraditionalModificatonButtons();
				
				//Hide traditional add/modify/cancel segment buttons
				$("#btnModSegment").hide();
				$("#btnAddSegment").hide();
				$("#btnAddGroundSegment").hide();
				$("#btnCanSegment").hide();
			} else {
				
				UI_tabBookingInfo.hideTraditionalModificatonButtons();
				$("#btnModifyRes").hide();
			}
			
			if(DATA_ResPro.initialParams.createNewPnrFromOld) {
				$("#btnNewPnr").decorateButton();
				$("#btnNewPnr").attr('style', 'width:auto;');
				$("#btnNewPnr").click(function() {UI_tabBookingInfo.newPnrOnClick();});	
			} else {
				$("#btnNewPnr").hide();
			}
			
			// TODO - Temporarily commenting, current GDS scenario we are validating carrier too. Therefore
			// above check is not required. When we need this kind of feature we need to verify on this flow before
			// enable it with respective validating/ticketing carrier.
			
//			if(DATA_ResPro.initialParams.allowCouponControlRequest) {
//				$("#btnModifyRes").hide();
//				$("#btnCpnCntrlRQ").decorateButton();
//				$("#btnCpnCntrlRQ").attr('style', 'width:auto;');
//				$("#btnCpnCntrlRQ").click(function() {UI_tabBookingInfo.getCouponControl();});	
//			} else {
				$("#btnCpnCntrlRQ").hide();
//			}
			
			if (DATA_ResPro.initialParams.addInfant){
				$("#btnAddInfants").decorateButton();
				$("#btnAddInfants").attr('style', 'width:auto;');	
				$("#btnAddInfants").disable();
				$("#btnAddInfants").click(function() {
				
				if(DATA_ResPro.initialParams.allowAddInfantToPartiallyFlownBookings || !DATA_ResPro.initialParams.flownSegmentExists){
					UI_tabBookingInfo.FirstNonFlwnfltOutDate = "";
					UI_tabBookingInfo.fillFlightInfo();
					UI_tabBookingInfo.fltOutDate = UI_tabBookingInfo.FirstNonFlwnfltOutDate;
					UI_tabBookingInfo.addInfantsOnClick();
					UI_tabBookingInfo.getNewInfantDetails();
				} else {
					alert("Infant cannot be added on a partial flown segment.\nTo book an Infant, please cancel the future segment and proceed with a new booking as Adult with Infant.");
				}					
				});
			}
			
			if (DATA_ResPro.initialParams.addInfant){
				$("#btnSaveInfants").decorateButton();
				$("#btnSaveInfants").attr('style', 'width:auto;');	
				$("#btnSaveInfants").disable();
				$("#btnSaveInfants").click(function() {UI_tabBookingInfo.saveInfantsOnClick("ADDINF");});
				$("#btnSaveInfants").hide();
			}
			else{$("#btnSaveInfants").disable();}
			
			
			if (DATA_ResPro.initialParams.removePassenger){
				$("#btnRemovePax").decorateButton();
				$("#btnRemovePax").attr('style', 'width:auto;');
				// $("#btnRemovePax").disable();
				$("#btnRemovePax").click(function() {UI_tabBookingInfo.removePaxOnClick();});
				// $("#btnRemovePax").hide();
			}else{
				$("#btnRemovePax").hide();
			}
			
			
			if (!UI_reservation.isGroupBooking && DATA_ResPro.initialParams.splitPassenger){
				$("#btnSplit").decorateButton();
				$("#btnSplit").disable();
				$("#btnSplit").click(function() {UI_tabBookingInfo.splitOnClick();});
			}
			else if(UI_reservation.isGroupBooking && DATA_ResPro.initialParams.groupBookingSplitPassengers){
				$("#btnSplit").decorateButton();
				$("#btnSplit").disable();
				$("#btnSplit").click(function() {UI_tabBookingInfo.splitOnClick();});
			}
			else{
				$("#btnSplit").hide();
			}
			
			if ($('#btnSplit').is(':visible') && DATA_ResPro.initialParams.allowCaptureExternalInternationalFlightDetails) {
				$("#btnAdvPaxSel").decorateButton();
				$("#btnAdvPaxSel").disable();
				$("#btnAdvPaxSel").click(function() {UI_tabBookingInfo.advPaxSelOnClick();});
			} else {
				$("#btnSplit").disable();
				$("#btnAdvPaxSel").hide();
			}
			
			if ((DATA_ResPro.initialParams.transferOwnership || DATA_ResPro.initialParams.transferOwnershipToAnyCarrier) && !DATA_ResPro.initialParams.voidReservation){
				$("#btnTransfer").decorateButton();
				$("#btnTransfer").attr('style', 'width:auto;');
				$("#btnTransfer").click(function() {UI_tabBookingInfo.transferOnClick();});
			}else{
				$("#selTransfer").closest("td").remove();
				$("#btnTransfer").closest("td").remove();
			}
			
			$("#selWLPriority").disable();
			$("#btnWLPriority").disable();
			$("#selWLPriority").hide();
			$("#btnWLPriority").hide();
			if (DATA_ResPro.initialParams.extendOnhold){
				$("#btnExtend").decorateButton();
				$("#btnExtend").attr('style', 'width:auto;');
				
				if(jsonBkgInfo.status == 'OHD' && !waitListedSegmentAvailable) {
					$("#hdnExtendDate").datepicker({
						minDate: new Date(),
						buttonImage: '../themes/default/images/accelaero/Calendar_no_cache.gif',
				        buttonImageOnly: true,
				        changeMonth: true,
				        changeYear: true,
				        showButtonPanel: true,
				        showOn: 'both'
					});
				}

				if(!(jsonBkgInfo.status == 'OHD')) {
					$("#btnExtend").disable();
					$("#selExtend").disable();
				}

				if(waitListedSegmentAvailable){
					$("#selExtend").hide();
					$("#btnExtend").hide();
					$("#btnWLPriority").decorateButton();
					$("#selWLPriority").show();
					$("#btnWLPriority").show();
					$("#btnWLPriority").attr('style', 'width:auto;');
					$("#btnWLPriority").click(function() {UI_tabBookingInfo.changePriorityOnClick();});
				}
				$("#btnExtend").click(function() {UI_tabBookingInfo.extendOnClick();});
			}else{
				$("#selExtend").hide();
				$("#btnExtend").hide();
			}
			
			$("#btnCancel").decorateButton();
			$("#btnCancel").attr('style', 'width:auto;');
			$("#btnCancel").click(function() {UI_tabBookingInfo.cancelOnClick();});
			
			if (DATA_ResPro.initialParams.cancelReservation && !DATA_ResPro.initialParams.voidReservation){
				$("#btnCancel").show();
			}else{
				$("#btnCancel").hide();
			}
			// Changed privilege from DATA_ResPro.initialParams.addUserNotes to new privilege  DATA_ResPro.initialParams.allowAddUserNotes
			if (DATA_ResPro.initialParams.allowAddUserNotes){
				$("#btnViewNotes").decorateButton();
				$("#btnViewNotes").attr('style', 'width:auto;');
				$("#btnViewNotes").click(function() {UI_tabBookingInfo.viewNotesOnClick();});
			}else{
				$("#btnViewNotes").hide();
				$("#txtUNotes").attr('disabled','disabled');
			}
			
			if (DATA_ResPro.initialParams.endorsementsAllowed){
				$("#trEndosmentNotes").show();
			}else{
				$("#trEndosmentNotes").hide();
				$("#txtEndosment").attr('disabled','disabled');
				$("#txtUNotes").css("height", 70);
			}
			
			$('#isVoidReservation').val(false);
			if(isAllowVoidReservation && !DATA_ResPro.initialParams.voidReservation){
				$("#btnVoidReservation").decorateButton();
				$("#btnVoidReservation").attr('style','width:auto;');
				$("#btnVoidReservation").click(function(){UI_tabBookingInfo.voidOnClick();});	
			}else{
				$("#btnVoidReservation").hide();
			}
			
			if (UI_reservation.allowCSVDetailUpload ||  DATA_ResPro.initialParams.tbaGroupNameUpload) {
				isTBABooking = UI_tabBookingInfo.isTBABooking();
				if (isTBABooking) {
					$("#btnUploadFromCSV").decorateButton();
					$("#btnCSVUploadSampleFile").decorateButton();
					$("#btnUploadFromCSV").decorateButton();
					$("#btnCSVUploadSampleFile").decorateButton();
					
					$("#btnUploadFromCSV").disable();
					$("#btnCSVUploadSampleFile").disable();
					$( "#csvFileUploadDiv" ).dialog({
						modal: true,
						width:500,
						height:300,
						title: "Upload File",
						autoOpen: false,
						buttons: {
							Close: function() {
								$( this ).dialog( "close" );
								if($('#btnPaxDetails').is(":visible") && parseSuccessful){
									$('#btnPaxDetails').trigger('click');
								} else if ($('#btnInternationalFlight').is(":visible") && parseSuccessful) {
									$('#btnPaxDetails').trigger('click');
								}
							}
						}
					});

					$("#btnUploadFromCSV").click(function() {
						csvFileUpload = true;
						$("#csvFileUploadDiv").dialog('open');
					});
					
					$("#btnCSVUploadSampleFile").click(function() {
						$("#sampleFileIFrameDiv").append('<iframe width="0" height="0" frameborder="0" src="SampleFileDownload.action"></iframe>');
					})
				} else {
					$("#btnUploadFromCSV").hide();
					$("#btnCSVUploadSampleFile").hide();
				}
			} else{
				$("#btnUploadFromCSV").hide();
				$("#btnCSVUploadSampleFile").hide();
			}
			
			$("#btnViewItn").decorateButton();
			$("#btnViewItn").attr('style', 'width:auto;');
			$("#btnViewItn").click(function() {UI_tabBookingInfo.viewItineraryOnClick();});
			
			$("#btnItnViewGrp").decorateButton();
			$("#btnItnViewGrp").attr('style', 'width:auto;');
			$("#btnItnViewGrp").click(function() {UI_tabBookingInfo.viewGroupItineraryOnClick();});
			$("#btnPaxItiEmail").click(function() {UI_tabBookingInfo.emailGroupItineraryOnClick();});
			$("#btnPaxItiPrint").click(function() {UI_tabBookingInfo.printGroupItineraryOnClick();});	
			$("#btnPaxItiIndPrint").click(function() {UI_tabBookingInfo.printIndividualItineraryOnClick();});
			$("#btnPaxItiIndEmail").click(function() {UI_tabBookingInfo.emailIndividualItineraryOnClick();});			
									
			$("#btnPrintReciept").decorateButton();
			$("#btnPrintReciept").click(function() {UI_tabBookingInfo.printRecieptOnClick();});
			
									
			$("#btnEmailItn").decorateButton();
			$("#btnEmailItn").attr('style', 'width:auto;');
			$("#btnEmailItn").click(function() {UI_tabBookingInfo.emailItineraryOnClick();});
			
			$("#btnPaxDetails").decorateButton();
			$("#btnPaxDetails").disable();
			$("#btnPaxDetails").attr('style', 'width:auto;');
			$("#btnPaxDetails").click(function() {UI_tabBookingInfo.paxDetailsOnClick();});

			$("#btnInternationalFlight").decorateButton();
			$("#btnInternationalFlight").disable();
			$("#btnInternationalFlight").attr('style', 'width:auto;');
			$("#btnInternationalFlight").click(function() {
				UI_tabBookingInfo.internationalFlightDetailsOnClick();
			});
            $("#btnClearAllAlerts").enable();
			if (DATA_ResPro.initialParams.allowCreateUserAlert == true){
				$("#btnCreateAlert").decorateButton();
				$("#btnCreateAlert").enable();
				$("#btnCreateAlert").attr('style', 'width:auto;');
				$("#btnCreateAlert").show();

                $("#btnClearAllAlerts").decorateButton();
				$("#btnClearAllAlerts").enable();
				$("#btnClearAllAlerts").attr('style', 'width:auto;');
				$("#btnClearAllAlerts").show();
			} else {
				$("#btnCreateAlert").hide();

				$("#btnClearAllAlerts").hide();
			}
			$("#btnCreateAlert").click(function() {UI_tabBookingInfo.createAlertOnClick();});
			$("#btnClearAllAlerts").click(function() {UI_tabBookingInfo.clearAllAlertsOnClick();});

			if((!$.isEmptyObject(paxConfigAD) && (paxConfigAD.passportNo.xbeVisibility || paxConfigAD.groupId.xbeVisibility || paxConfigAD.visaDocNumber.xbeVisibility)) ||
			   (!$.isEmptyObject(paxConfigCH) && (paxConfigCH.passportNo.xbeVisibility || paxConfigCH.groupId.xbeVisibility || paxConfigCH.visaDocNumber.xbeVisibility)) ||
			   (!$.isEmptyObject(paxConfigIN) && (paxConfigIN.passportNo.xbeVisibility || paxConfigIN.groupId.xbeVisibility || paxConfigIN.visaDocNumber.xbeVisibility))){
				$("#btnPaxDetails").show();
			} else {
				$("#btnPaxDetails").hide();
			}
			if(!DATA_ResPro.initialParams.allowCaptureExternalInternationalFlightDetails) {
				$("#btnInternationalFlight").hide();
			} else {
				$("#btnInternationalFlight").show();
			}
			if(DATA_ResPro.initialParams.groupChargeAdjustment){
				$("#btnGroupCharge").decorateButton();
				$("#btnGroupCharge").attr('style', 'width:auto;');
				$("#btnGroupCharge").click(function() {UI_tabBookingInfo.loadGroupChargeData();});
			} else {
				$("#btnGroupCharge").hide();
			}			
			
			$("#groupChargeAdjusmentPopUp").dialog({ 
				autoOpen: false,
				modal:true,
				height: 'auto',
				width: 'auto',
				title:'Group Charge Adjustment'
			});
			
			$("#btnServiceTaxAmount").decorateButton();
			$("#btnServiceTaxAmount").attr('style', 'width:auto;');
			$("#btnServiceTaxAmount").click(function() {UI_tabBookingInfo.getChargeAmountWithServiceTax();});
			
			$("#btnGroupChargeConfirm").decorateButton();
			$("#btnGroupChargeConfirm").attr('style', 'width:auto;');
			$("#btnGroupChargeConfirm").click(function() {UI_tabBookingInfo.commenceGroupChargeAdjustment();});
			
			$("#btnGroupChargeReset").decorateButton();
			$("#btnGroupChargeReset").attr('style', 'width:auto;');
			$("#btnGroupChargeReset").click(function() {UI_tabBookingInfo.resetGroupChargeAdjustment();});
			
			$("#btnGroupChargeClose").decorateButton();
			$("#btnGroupChargeClose").attr('style', 'width:auto;');
			$("#btnGroupChargeClose").click(function() {
				$("#groupChargeAdjusmentPopUp").dialog('close');
			});

			$("#btnHandlingFeeAmount").decorateButton();
			$("#btnHandlingFeeAmount").attr('style', 'width:auto;');
			$("#btnHandlingFeeAmount").click(function() {UI_tabBookingInfo.getHandlingFeeAmount();});
			
			$("#selectDeselectSegment").click(function(){
				if ($("#listSegments li.ui-selected").length == $("#listSegments li").length) {
					$("#listSegments li").removeClass('ui-selected');
					selectedONDs=[];
					lastSelectedONDCareer=null;
					$("#selAdjustmentType").empty();
				}else{
					$("#listSegments li").addClass('ui-selected');
					selectedONDs=[];
					lastSelectedONDCareer=$.trim($("#listSegments li").text().split('-')[0]);
					$("#listSegments .ui-selected").each(function() {
						selectedONDs.push(this.id);
					});
					
					$("#selAdjustmentType").empty();
					var adjustmentTypes=[];
					for (var i=0; i< chargeAdjustments.length; i++) {
						if (chargeAdjustments[i].carrierCode == lastSelectedONDCareer){
							adjustmentTypes = chargeAdjustments[i].chargeAdjustmentTypes;
							break;
						}
					}
					for (var i=0; i< adjustmentTypes.length; i++) {
						if (UI_tabBookingInfo.validateChargeAdjustmentType(adjustmentTypes[i], lastSelectedONDCareer) ) {
							$("#selAdjustmentType").append('<option value="' + adjustmentTypes[i].chargeAdjustmentTypeId + '">' + adjustmentTypes[i].chargeAdjustmentTypeName + '</option>');
						}					
					} 
				
				}
			});
			
			$("#selectDeselectPassenger").click(function(){
				if ($("#listPassengers li.ui-selected").length == $("#listPassengers li").length) {
					$("#listPassengers li").removeClass('ui-selected');
					selectedPax=[];
				}else{
					$("#listPassengers li").addClass('ui-selected');
					selectedPax=[];
					$("#listPassengers .ui-selected").each(function() {
						selectedPax.push(this.id);
					});
				}
			});

			$("#txtAdjustment").change(function() {
				if($("#txtAdjustment").val().charAt(0)=='-'){
					$("#selChargeType").attr("checked",true);
					$("#selChargeType").attr("disabled", true);
				}else{
					$("#selChargeType").removeAttr("disabled");
				}
				UI_tabBookingInfo.chargeAdjustmentOnChange();
			});
			
			UI_tabBookingInfo.allowNameChange();
			
			//If booking is onhold no point in showing group refund button
			if(jsonBkgInfo.status != "OHD") {
				$("#btnGroupRefund").decorateButton();
				$("#btnGroupRefundConfirm").decorateButton();
				$("#btnGroupRefundReset").decorateButton();
				$("#btnGroupRefundClose").decorateButton();
				$("#selectDeselectTxn").decorateButton();
				$("#btnGroupCRConfirm").decorateButton();
				$("#btnGroupCRCancel").decorateButton();
				$("#groupCreditCardDet").hide();

				$("#btnGroupRefund").click(function() {UI_tabBookingInfo.loadGroupRefundData();});
				$("#btnGroupRefundConfirm").click(function() {UI_tabBookingInfo.commenceGroupRefund();});
				$("#btnGroupRefundReset").click(function() {UI_tabBookingInfo.resetGroupRefundData();});
				$("#btnGroupRefundClose").click(function() {UI_tabBookingInfo.closeGroupRefundDialog(true);});
				$("#btnGroupCRConfirm").click(function() {UI_tabBookingInfo.groupPayCCConfirmOnClick();});
				$("#btnGroupCRCancel").click(function() {UI_tabBookingInfo.groupPayCCCancelOnClick();});
			} else {
				$("#btnGroupRefund").hide();
			}
				
			/*
			 * Select all the transaction IDs
			 */
			$("#selectDeselectTxn").click(function() {
				if ($("#listTxnIDs li.ui-selected").length == $("#listTxnIDs li").length) {
 					$("#listTxnIDs li").removeClass('ui-selected');
 				} else {
 					$("#listTxnIDs>li").addClass('ui-selected');
 				}
			});
			
			
			if(jsonBkgInfo.status != "OHD") {
				$("#btnReverseNoShow").decorateButton();
				if(!UI_reservation.jsonTaxReversalData.hasRefundablePax){
					$("#btnReverseNoShow").prop("disabled",true);
				}				
				$("#btnReverseNoShowTaxReset").decorateButton();
				$("#btnReverseNoShowTaxClose").decorateButton();
			//	$("#selectDeselectTxn").decorateButton();
				$("#btnReverseNoShowTaxConfirm").decorateButton();

				$("#btnReverseNoShow").click(function() {UI_tabBookingInfo.loadReverseNoShowTaxData();});
				$("#btnReverseNoShowTaxConfirm").click(function() {UI_tabBookingInfo.processReverseNoShowTaxData();});
				$("#btnReverseNoShowTaxReset").click(function() {UI_tabBookingInfo.resetReverseNoShowTaxData();});
				$("#btnReverseNoShowTaxClose").click(function() {UI_tabBookingInfo.closeReverseNoShowTaxDataDialog(true);});
				$(".checkRfnd:button").click(function() {UI_tabBookingInfo.selectDeselctAllPax();});
				
				
			} else {
				$("#btnReverseNoShow").hide();
			}

			
			$("#selectDeselectRfnd").click(function() {

			$("input:checkbox").prop('checked', $(this).prop("checked"));

		});
			
			$("#groupPaxRefundPopUp").dialog({ 
				autoOpen: false,
				modal:true,
				height: 'auto',
				width: 'auto',
				title:'Group Passenger Refund',
				close:function() {UI_tabBookingInfo.closeGroupRefundDialog(false);}
			});
			
			$("#paxNameChangePopUp").dialog({ 
				autoOpen: false,
				modal:true,
				height: 'auto',
				width: 'auto',
				title:'Passenger Information',
				close:function() {UI_tabBookingInfo.closeNameChangeDialog(false);}
			});
			
			
			$("#noShowTaxRefundPopUp").dialog({ 
				autoOpen: false,
				modal:true,
				height: 'auto',
				width: 'auto',
				title:'No-show tax reverse',
				close:function() {UI_tabBookingInfo.closeReverseNoShowTaxDataDialog(false);}
			});
			
			$("#btnClose").decorateButton();
			$("#btnClose").enable();
			$("#btnClose").click(function() {UI_tabBookingInfo.closeOnclick();});
			
			$("#btnBookMark").decorateButton();
			$("#btnBookMark").attr('style', 'width:auto;');
			$("#btnBookMark").enable();
			$("#btnBookMark").click(function() {UI_tabBookingInfo.bookMarkOnClick();});
			
			$("#btnViewInvoice").decorateButton();
			$("#btnViewInvoice").attr('style', 'width:auto;');
			$("#btnViewInvoice").enable();
			
			if (UI_reservation.taxInvoices == undefined || UI_reservation.taxInvoices == null || UI_reservation.taxInvoices.length == 0){
				$("#btnViewInvoice").hide();
			}
			
			$("#btnViewInvoice").click(function() {UI_tabBookingInfo.viewInvoiceOnClick();});
			$("#btnInvoicePrint").click(function() {UI_tabBookingInfo.printTaxInvoiceOnClick();});
			
			$("#btnReset").decorateButton();
			$("#btnReset").disable();
			$("#btnReset").click(function() {
				UI_tabBookingInfo.resetOnClick();
				
				$("#btnUploadFromCSV").disable();
				$("#btnCSVUploadSampleFile").disable();
				
			});



			$("#btnPaxNames").decorateButton();
			$("#btnPaxNames").disable();
			$("#btnPaxNames").click(function(){
				UI_tabBookingInfo.paxNamesOnClick();
			});
						
			if((!$.isEmptyObject(paxConfigAD) && (paxConfigAD.passportNo.xbeVisibility || paxConfigAD.groupId.xbeVisibility || paxConfigAD.visaDocNumber.xbeVisibility)) ||
			   (!$.isEmptyObject(paxConfigCH) && (paxConfigCH.passportNo.xbeVisibility || paxConfigCH.groupId.xbeVisibility || paxConfigCH.visaDocNumber.xbeVisibility)) ||
			   (!$.isEmptyObject(paxConfigIN) && (paxConfigIN.passportNo.xbeVisibility || paxConfigIN.groupId.xbeVisibility || paxConfigIN.visaDocNumber.xbeVisibility))){
				$("#btnPaxDetails").show();
			} else {
				$("#btnPaxDetails").hide();
			}
			
			$("#btnEdit").decorateButton();
			$("#btnEdit").click(function() {UI_tabBookingInfo.editOnClick();});

			$("#btnSave").decorateButton();
			$("#btnSave").hide();
			
			$("#btnApplyChargeOk").decorateButton();
			$("#btnSave").click(function() {
				
				$("#btnUploadFromCSV").disable();
				$("#btnCSVUploadSampleFile").disable();
				
				if(DATA_ResPro.initialParams.overrideNameChangeCharge){
				  $('#divNameChangeConfirm').show();
				}else{
				  UI_tabBookingInfo.saveOnClick();
				}
			});
			
			$("#btnApplyChargeOk").click(function(){
				$('#divNameChangeConfirm').hide();
				UI_tabBookingInfo.saveOnClick();
			});
			
			
			$("#divSSRPane").hide();
			$("#btnSSRConfirm").decorateButton();
			$("#btnSSRConfirm").click(function(){UI_tabBookingInfo.ssrConfirmOnClick();});
			
			$("#btnSSRCancel").decorateButton();
			$("#btnSSRCancel").enable();
			$("#btnSSRCancel").click(function(){UI_tabBookingInfo.ssrCancelOnClick();});
			
			if(DATA_ResPro.initialParams.allowOnholdRollForward){
				$("#btnRollForward").decorateButton();
				
				if(jsonBkgInfo.status == "OHD" && !waitListedSegmentAvailable){
					$("#btnRollForward").show();
					$("#btnRollForward").attr('style', 'width:auto;');
					if(!UI_reservation.ohdRollForwardValidation.allow){
						$("#btnRollForward").attr('title', raiseError(UI_reservation.ohdRollForwardValidation.errorCode));
						$("#btnRollForward").disable();
					} else {
						$("#btnRollForward").enable();
						$("#btnRollForward").attr('title', '');
						$("#btnRollForward").click(function(){UI_tabBookingInfo.showOHDRollForward();});
					}
				} else {
					$("#btnRollForward").hide();
				}
				
				if(!DATA_ResPro.initialParams.allowOverBook){
					//$("#chkRollForwardOverBook").hide();
					$(".OverBookClass").toggle();
				}
			} else {
				$("#btnRollForward").hide();
			}
			
			$("#btnConfirmOpenRt").decorateButton();
			$("#btnConfirmOpenRt").attr('style', 'width:auto;');
			$("#btnConfirmOpenRt").hide();
			$("#btnConfirmOpenRt").click(function() {UI_tabBookingInfo.confirmReturnOnClick();});
						
			
			if(DATA_ResPro.initialParams.cancelledReservation){
				$("#btnConfirmOpenRt").hide();
			}
			
			// To limit the no. of characters entered in User notes text area.
			$("#txtUNotes").change(function(){UI_tabBookingInfo.userNoteOnChange();});	
			$("#txtUNotes").keyup(function(){UI_tabBookingInfo.userNoteOnChange();});
			
			// To limit the no. of characters entered in Endosment notes text
			// area.
			$("#txtEndosment").change(function(){UI_tabBookingInfo.endosmentNoteOnChange();});	
			$("#txtEndosment").keyup(function(){UI_tabBookingInfo.endosmentNoteOnChange();});
			
			UI_tabBookingInfo.rfMinDate = UI_tabBookingInfo.getMinDateForRollForward(UI_reservation.ohdRollForwardMinDate);
			
			$("#rollForwardStartDate").datepicker({ dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , 
					changeYear: 'true', showButtonPanel: 'true', minDate: UI_tabBookingInfo.rfMinDate, 
					beforeShow: _setZIndex});
			
			$("#rollForwardEndDate").datepicker({ dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , 
					changeYear: 'true', showButtonPanel: 'true', minDate: UI_tabBookingInfo.rfMinDate,
					beforeShow: _setZIndex});

			$("#btnOHDRollForwardSearch").click(function(){UI_tabBookingInfo.ohdRollForwardOnClick();});
			$("#btnOHDRollForwardCancel").click(function(){UI_commonSystem.readOnly(false);$('#divOHDRollForward').hide();});
			$("#btnOHDRollForwardConfirm").click(function(){UI_tabBookingInfo.ohdRollForwardConfirmOnClick();});
			
			$("#chkOhdRFAll").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(7, this);});
			$("#chkOhdRFSun").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(0, this);});
			$("#chkOhdRFMon").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(1, this);});
			$("#chkOhdRFTue").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(2, this);});
			$("#chkOhdRFWed").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(3, this);});
			$("#chkOhdRFThu").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(4, this);});
			$("#chkOhdRFFri").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(5, this);});
			$("#chkOhdRFSat").click(function(){UI_tabBookingInfo.ohdRollForwardSelection(6, this);});
			
			
			$("#chkOHDRFOvrdOHDBuffer").click(function(){
				if($(this).attr('checked')){
					$("#txtOHDRFBufferDays").enable();
					$("#txtOHDRFBufferTime").enable();
				} else {
					$("#txtOHDRFBufferDays").val("");
					$("#txtOHDRFBufferDays").disable();
					$("#txtOHDRFBufferTime").val("");
					$("#txtOHDRFBufferTime").disable();
				}
			});
			
			
			UI_tabBookingInfo.fillDropDownData();
			
			UI_tabBookingInfo.constructGridAdult({id:"#tblPaxAdt"});
			UI_tabBookingInfo.constructGridInf({id:"#tblPaxInf"});
			UI_tabBookingInfo.blnLoaded = true;
		}
		
		if(DATA_ResPro.initialParams.allowOnholdRollForward){
			if(jsonBkgInfo.status == "OHD" && !waitListedSegmentAvailable && isCsPnr == false){
				$("#btnRollForward").show();
			} else {
				$("#btnRollForward").hide();
			}
		}
		var contObj = $.parseJSON(UI_reservation.contactInfo);
		if(contObj == null || contObj.email == null || contObj.email == ''){
			$("#btnEmailItn").hide();
		}else{
			$("#btnEmailItn").show();
		}
		
		if (UI_reservation.taxInvoices != undefined && UI_reservation.taxInvoices != null && UI_reservation.taxInvoices.length > 0){
			$("#btnViewInvoice").show();
		}
		
		if(!UI_tabPayment.isAddInfant){
			UI_tabBookingInfo.resetOnClick();
		}
		
		
		if (DATA_ResPro.initialParams.allowExternalTicketExchange){
			$("#btnExchangeSegment").decorateButton();
			$("#btnExchangeSegment").attr('style', 'width:auto;');		
			$("#btnExchangeSegment").unbind().click(function() {UI_tabBookingInfo.exchangeSegmentOnClick();});
		}else{
			$("#btnExchangeSegment").hide();			
		}
		
		// Set Status Message( transfer segment success message)
		UI_tabBookingInfo.setStatusMessge();
		UI_tabBookingInfo.showHideADGrid();

		//apply name change charges
		$('#divNameChangeConfirm').hide();
		$("#chkBoxApplyCharge").attr('checked', true);			
		$('#chkOHDRFOvrdOHDBuffer').click();
		
		var atLeastOneNameEditable = false;
		var i = 0;
		while(jsonPaxAdtObj.paxAdults.length > i){
			atLeastOneNameEditable = atLeastOneNameEditable || jsonPaxAdtObj.paxAdults[i].nameEditable;
			i++;
		}
		i = 0;
		if(jsonPaxAdtObj.paxInfants.length > 0){
			while(jsonPaxAdtObj.paxInfants.length > i){
				atLeastOneNameEditable = atLeastOneNameEditable || jsonPaxAdtObj.paxInfants[i].nameEditable;
				i++;
			}
		}
		
		if (atLeastOneNameEditable && UI_reservation.isAllowNameChange){
			$('#btnNameChange').show();
		} else {
			$('#btnNameChange').hide();
		}
		
		$("#btnHandlingFeeAmount").hide();
				
		UI_tabBookingInfo.setSavedInfantOnLoad();
		
		//    Check Add and classify user Notes options
	    if(DATA_ResPro.initialParams.allowAddUserNotes){
		     $("#btnSaveUserNote").hide();
		     $("#btnAddUserNote").show();
		     $("#btnAddUserNote").enable();
		     $("#btnAddUserNote").decorateButton();
		     $("#btnAddUserNote").attr('style', 'width:auto;');
			 $("#selClassification").hide();
			 $("#btnAddUserNote").unbind().click(function() {UI_tabBookingInfo.classifyUserNote();});
		}else{
			 $("#btnAddUserNote").hide();
			 $("#btnSaveUserNote").hide();
			 $("#selClassification").hide();
			 $("#txtUNotes").attr('disabled','disabled');
		}
        
    }
	UI_tabBookingInfo.setSavedInfantOnLoad = function(){
		UI_tabBookingInfo.savedInfants=[];
		var x = 0;
		if(jsonPaxAdtObj!=null && jsonPaxAdtObj.paxInfants != null){
			var intWLen = jsonPaxAdtObj.paxInfants.length;
			while(intWLen>0){
				var travellingWith=$("#"+ (x + 1) + "_displayInfantTravellingWith").val();				 
				UI_tabBookingInfo.savedInfants.push(travellingWith);
				x++;
				intWLen--;
			} 
		}
		
	}
	
	_setZIndex = function(dateText, inst){
		if($("#ui-datepicker-div")){			
			$("#ui-datepicker-div").css('z-index', '1002');
		}
	}
	
	/*
	 * On Load Call
	 */
	UI_tabBookingInfo.onLoadCall = function(){
		UI_tabBookingInfo.fillBookingInfo();
		UI_tabBookingInfo.groupFlightSegments();
		UI_tabBookingInfo.fillFlightInfo();
		UI_tabBookingInfo.fillUserNotes();
		UI_tabBookingInfo.fillEndorsementNotes();
		
		UI_tabBookingInfo.selectedFlightSeg.clear();
		UI_tabBookingInfo.modifyFlexiBooking = false;
		
		UI_tabBookingInfo.buildPaxPriceBreakDown();
		
		UI_commonSystem.fillGridData({id:"#tblPaxAdt", data:jsonPaxAdtObj.paxAdults, editable:true})
		UI_commonSystem.fillGridData({id:"#tblPaxInf", data:jsonPaxAdtObj.paxInfants, editable:true})
		if(jsonPaxAdtObj.paxInfants.length == 0){
			$("#divInfantPane").hide();
		}
		UI_tabBookingInfo.adjustPageHeight(null, jsonPaxAdtObj.paxInfants);
		$("#btnSaveInfants").hide();
		
		if(DATA_ResPro.initialParams.addInfant){
			$("#btnAddInfants").show();
		}else {
			$("#btnAddInfants").hide();
		}		
		
		if(jsonBkgInfo.status == 'CNX'){
			$("#btnCanSegment").disable();
			$("#btnCancel").disable();		   
			$("#btnVoidReservation").disable();
			$("#btnEdit").enable();			
		}else {
			$("#btnCancel").disable();		
			$("#btnVoidReservation").disable();
			if(DATA_ResPro.initialParams.cancelReservation){
				$("#btnCancel").show()
				$("#btnCancel").enable();		
			}	
			if(isAllowVoidReservation && !DATA_ResPro.initialParams.voidReservation){
				$("#btnVoidReservation").show();
				$("#btnVoidReservation").enable();
			}
			$("#btnEdit").enable();
		}		
		if(!(jsonBkgInfo.status == 'OHD')) {
			$("#selExtend").disable();
			$("#btnExtend").disable();
		}
		
		// if void reservation
		if(jsonBkgInfo.status == 'VOD') {
			$("#btnEdit").disable();
			$("#btnVoidReservation").disable();
			$("#selTransfer").closest("td").remove();
			$("#btnTransfer").closest("td").remove();
		}
		
		// Hack to inf problem
		$('#tblPaxInf').GridUnload();
		UI_tabBookingInfo.constructGridInf({id:"#tblPaxInf"});
		UI_commonSystem.fillGridData({id:"#tblPaxInf", data:jsonPaxAdtObj.paxInfants, editable:true})
		if(jsonPaxAdtObj.paxInfants.length == 0){
			$("#divInfantPane").hide();
		}
		UI_tabBookingInfo.adjustPageHeight(jsonPaxAdtObj.paxAdults, jsonPaxAdtObj.paxInfants);
		UI_tabBookingInfo.mapCalendar();
		UI_tabBookingInfo.mailButtonEnable(true);				
		UI_tabBookingInfo.disableEnableControls(true, false, false);
		UI_tabBookingInfo.disablePaxNationality(true);
		UI_tabBookingInfo.segButtonEnable(false);
		UI_tabBookingInfo.paxButtonEnable(false);
		
		
		if(!DATA_ResPro.initialParams.viewItinerary){
			$("#btnViewItn").disable();
			$("#btnEmailItn").disable();
			$("#selITNLang").disable();
			$("#chkPax").attr('checked', false);
			$("#chkPax").disable();
			$("#chkPay").disable();
			$("#chkChg").disable();
		}else{
			$("#btnViewItn").enable();
			$("#btnEmailItn").enable();
			if(!DATA_ResPro.initialParams.viewItineraryWithPaymentInfo){
				$("#payItnTxtSpan").hide();
				$("#chkPay").hide();
				$("#chkPay").disable();
			}
			if(!DATA_ResPro.initialParams.viewItineraryWithoutPaxCharges){
				$("#paxItnTxtSpan").hide();
				$("#chkPax").hide();
				$("#chkPax").disable();	
			}
			if(!DATA_ResPro.initialParams.viewItineraryWithCharges){
				$("#chgTxtSpan").hide();
				$("#chkChg").hide();
				$("#chkChg").disable();
			}		
		}	
		
		if(DATA_ResPro.initialParams.addSegment){
			$("#btnAddSegment").enable();			
		}
		
		$("#selBookingCategory").disable();	
		$("#selItineraryFareMask").disable();
		$("#selOriginCountryOfCall").disable();
		
		$("#btnReset").disable();
		if (!gdsCsReservationModAllowed) {
			$("#btnEdit").hide();
			$("#btnPaxDetails").enable();
		} else {
			$("#btnEdit").show();
		}
		$("#btnSave").hide();		
        $("#btnClearAllAlerts").enable();
    };
	
	UI_tabBookingInfo.createCarrierList = function(){	
		var carrierList = '';
		if(jsonFltDetails != []){
			for(var i=0;i<jsonFltDetails.length;i++){
				if(!UI_tabBookingInfo.isCarrierExists(carrierList, jsonFltDetails[i].airLine)){
					if(i == (jsonFltDetails.length - 1)){
						carrierList += jsonFltDetails[i].airLine;
					} else {
						carrierList += jsonFltDetails[i].airLine + ",";
					}
				}
				if(jsonFltDetails[i].status == 'WL'){
					waitListedSegmentAvailable = true;
				}
			}
		}
		
		return carrierList;
	}
	
	UI_tabBookingInfo.isCarrierExists = function(arr, elem){
		var spltArr = arr.split(",");
		for(var i=0;i<spltArr.length;i++){
			if(spltArr[i] == elem){
				return true;		 
			}
		}
		
		return false;
	}
	
	UI_tabBookingInfo.checkPaxContactConfigAvailable = function(carrierList, paxType){
		var cArr = getArrayFromStr(carrierList);
		var carrierCombinations = getCombinations(cArr);
		
		var exists = false;
		for(var i=0;i<carrierCombinations.length;i++){
			if(typeof(top.globalPaxContactConfig[carrierCombinations[i]+paxType]) != 'undefined'){
				contactConfig = top.globalPaxContactConfig[carrierCombinations[i]+paxType].contactConfig;
				validationGroup = top.globalPaxContactConfig[carrierCombinations[i]+paxType].validationGroup;
				paxConfigAD = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxConfigAD;
				paxConfigCH = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxConfigCH;
				paxConfigIN = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxConfigIN;
				paxValidation = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxValidation;
				exists = true;
				break;
			}
		}
		
		return exists;
		
	}
	
	UI_tabBookingInfo.contactConfigSuccess = function(response){
		if(response.success){
			// Save configuration in top
			var carrierArr = getArrayFromStr(tempCarrierList);
			var carrierStr = "";
			for ( var i = 0; i < carrierArr.length; i++) {
				carrierStr += carrierArr[i];
			}
			
			top.globalPaxContactConfig[carrierStr+tempPaxType] = response;
			if(DATA_ResPro.initialParams.countryPaxConfigEnabled){
				top.tempGlobalPaxContactConfig[carrierStr+tempPaxType] = jQuery.extend(true, new Object(), response);
			}
			contactConfig = response.contactConfig;
			validationGroup = response.validationGroup;
			paxConfigAD = response.paxConfigAD;
			paxConfigCH = response.paxConfigCH;
			paxConfigIN = response.paxConfigIN;
			
			paxValidation = response.paxValidation;
		}
	}
	
	UI_tabBookingInfo.contactConfigCountrySuccess = function(response){
		var carrierStr = "";
		
		var carrierArr = getArrayFromStr(tempCarrierList);
		for ( var i = 0; i < carrierArr.length; i++) {
			carrierStr += carrierArr[i];
		}
		
		var paxType = "";
		if(jsonPaxAdtObj.paxAdults.length > 0){
			paxType = (jsonPaxAdtObj.paxAdults[0].displayPaxCategory == null) ? "A":jsonPaxAdtObj.paxAdults[0].displayPaxCategory;
		} else if(jsonPaxAdtObj.paxInfants.length> 0){
			paxType = (jsonPaxAdtObj.paxInfants[0].displayPaxCategory == null) ? "A":jsonPaxAdtObj.paxInfants[0].displayPaxCategory;
		}
		if(response.success && DATA_ResPro.initialParams.countryPaxConfigEnabled){
			var paxCountryConfigAD = response.countryPaxConfigAD;
			var paxCountryConfigCH = response.countryPaxConfigCH;
			var paxCountryConfigIN = response.countryPaxConfigIN;
			
			UI_tabBookingInfo.overridePaxConfig(paxCountryConfigAD,carrierStr,paxType,'AD');
			UI_tabBookingInfo.overridePaxConfig(paxCountryConfigCH,carrierStr,paxType,'CH');
			UI_tabBookingInfo.overridePaxConfig(paxCountryConfigIN,carrierStr,paxType,'IN');
		}else{
			top.globalPaxContactConfig[carrierStr+paxType] = top.tempGlobalPaxContactConfig[carrierStr+paxType];
		}
	}

	UI_tabBookingInfo.overridePaxConfig = function(paxCountryConfig,carrierStr,paxType,paxCatType){
		var indexStr = carrierStr + paxType;
		
		if(paxCatType == 'AD') {
			top.globalPaxContactConfig[indexStr].paxConfigAD.title.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.title.xbeMandatory || paxCountryConfig.title);
			top.globalPaxContactConfig[indexStr].paxConfigAD.firstName.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.firstName.xbeMandatory || paxCountryConfig.firstName);
			top.globalPaxContactConfig[indexStr].paxConfigAD.lastName.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.lastName.xbeMandatory || paxCountryConfig.lastName);
			top.globalPaxContactConfig[indexStr].paxConfigAD.nationality.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.nationality.xbeMandatory || paxCountryConfig.nationality);
			top.globalPaxContactConfig[indexStr].paxConfigAD.dob.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.dob.xbeMandatory || paxCountryConfig.dob);
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportNo.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.passportNo.xbeMandatory || paxCountryConfig.passportNo);
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportExpiry.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.passportExpiry.xbeMandatory || paxCountryConfig.passportExpiry);
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportIssuedCntry.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.passportIssuedCntry.xbeMandatory || paxCountryConfig.passportIssuedCntry);
			top.globalPaxContactConfig[indexStr].paxConfigAD.groupId.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.groupId.xbeMandatory || paxCountryConfig.groupId);
		} else if (paxCatType == 'CH') {
			top.globalPaxContactConfig[indexStr].paxConfigCH.title.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.title.xbeMandatory || paxCountryConfig.title);
			top.globalPaxContactConfig[indexStr].paxConfigCH.firstName.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.firstName.xbeMandatory || paxCountryConfig.firstName);
			top.globalPaxContactConfig[indexStr].paxConfigCH.lastName.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.lastName.xbeMandatory || paxCountryConfig.lastName);
			top.globalPaxContactConfig[indexStr].paxConfigCH.nationality.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.nationality.xbeMandatory || paxCountryConfig.nationality);
			top.globalPaxContactConfig[indexStr].paxConfigCH.dob.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.dob.xbeMandatory || paxCountryConfig.dob);
			top.globalPaxContactConfig[indexStr].paxConfigCH.passportNo.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.passportNo.xbeMandatory || paxCountryConfig.passportNo);
			top.globalPaxContactConfig[indexStr].paxConfigCH.passportExpiry.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.passportExpiry.xbeMandatory || paxCountryConfig.passportExpiry);
			top.globalPaxContactConfig[indexStr].paxConfigCH.passportIssuedCntry.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.passportIssuedCntry.xbeMandatory || paxCountryConfig.passportIssuedCntry);
			top.globalPaxContactConfig[indexStr].paxConfigCH.groupId.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.groupId.xbeMandatory || paxCountryConfig.groupId);
		} else if (paxCatType == 'IN') {
			top.globalPaxContactConfig[indexStr].paxConfigIN.travelWith.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.travelWith.xbeMandatory || paxCountryConfig.travelWith);
			top.globalPaxContactConfig[indexStr].paxConfigIN.firstName.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.firstName.xbeMandatory || paxCountryConfig.firstName);
			top.globalPaxContactConfig[indexStr].paxConfigIN.lastName.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.lastName.xbeMandatory || paxCountryConfig.lastName);
			top.globalPaxContactConfig[indexStr].paxConfigIN.nationality.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.nationality.xbeMandatory || paxCountryConfig.nationality);
			top.globalPaxContactConfig[indexStr].paxConfigIN.dob.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.dob.xbeMandatory || paxCountryConfig.dob);
			top.globalPaxContactConfig[indexStr].paxConfigIN.passportNo.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.passportNo.xbeMandatory || paxCountryConfig.passportNo);
			top.globalPaxContactConfig[indexStr].paxConfigIN.passportExpiry.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.passportExpiry.xbeMandatory || paxCountryConfig.passportExpiry);
			top.globalPaxContactConfig[indexStr].paxConfigIN.passportIssuedCntry.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.passportIssuedCntry.xbeMandatory || paxCountryConfig.passportIssuedCntry);
			top.globalPaxContactConfig[indexStr].paxConfigIN.groupId.xbeMandatory = 
				( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.groupId.xbeMandatory || paxCountryConfig.groupId);
		}	
	}

	// Enable flight segment buttons
	UI_tabBookingInfo.segButtonEnable = function(cond){		
		if(cond)  {
			$("#btnCanSegment").enable();
			$("#btnModSegment").enable();
			$("#btnAddSegment").enable();
			$("#btnCreateAlert").enable();
			$("#btnClearAllAlerts").enable();

			$("#btnAddGroundSegment").enable();			
		}else {
			$("#btnCanSegment").disable();
			$("#btnModSegment").disable();
			$("#btnAddSegment").disable();
			$("#btnCreateAlert").disable();
			$("#btnClearAllAlerts").disable();

			$("#btnAddGroundSegment").disable();	
		}
	};
	
	// Enable Pax grid Buttons
	UI_tabBookingInfo.paxButtonEnable = function(cond){		
		if(cond)  {			
			$("#btnRemovePax").enable();
			$("#btnSplit").enable();
			$("#btnRemovePax").enable();
			$("#btnInternationalFlight").enable();
			$("#btnAdvPaxSel").enable();
			var adultCounter = function(arr){
				var adultCount = 0;
				for(var i = 0; i < arr.length; i++){
					if(arr[i].displayAdultType == 'AD'){
						adultCount ++;
					}
				}
				return adultCount;
			}
			
			var adultCount = adultCounter(jsonPaxAdtObj.paxAdults);
			if(adultCount > jsonPaxAdtObj.paxInfants.length){
				$("#btnAddInfants").enable();
			}
			else{
				$("#btnAddInfants").disable();
			}
		}else {
			
			$("#btnRemovePax").disable();
			$("#btnSplit").disable();
			$("#btnRemovePax").disable();
			$("#btnSplit").disable();
			$("#btnAddInfants").disable();
			$("#btnInternationalFlight").disable();
			$("#btnAdvPaxSel").disable();
		}
	
	}
	
	// Enable Email Buttons
	UI_tabBookingInfo.mailButtonEnable = function(cond){		
		if(cond)  {
			$("#btnEmailItn").enable();
			$("#btnViewItn").enable();			
		}else {
			$("#btnEmailItn").disable();
			$("#btnViewItn").disable();			
		}
	}
	
	// Enable Reservation Buttons
	UI_tabBookingInfo.resButtonEnable = function(cond){		
		if(cond)  {
			$("#btnEdit").enable();
			$("#btnSave").enable();
			$("#btnReset").enable();
			$("#btnCancel").enable();
		}else {
			$("#btnEdit").disable();
			$("#btnSave").disable();
			$("#btnReset").disable();
			$("#btnCancel").disable();
		}
	}
	
	/*
	 * Split Validate
	 */
	UI_tabBookingInfo.splitValidate = function(){
		UI_commonSystem.initializeMessage();
		UI_tabBookingInfo.setSelectedPaxInfo();
		if (UI_tabBookingInfo.jsSltdAdt == ""){
			showERRMessage(raiseError("XBE-ERR-41"));
			UI_tabBookingInfo.resetOnClick();
			return false;
		} else if(!UI_tabBookingInfo.matchInfantTravelWithAdult()){
			showERRMessage('Please select adult travelling with to split infant');
//			UI_tabBookingInfo.resetOnClick();
			return false;
		}
		
		if(!UI_tabBookingInfo.allPaxSelectedValidate()){
			return false;
		}
//		if(infantWithWrongAdult){
//			if(confirm("Infant is going to be splitted with a different adult. Proceed anyway?")){
//				return true;
//			}else{
//				UI_tabBookingInfo.resetOnClick();
//				return false;
//			}
//		}
		
		return true;
	}
	
		UI_tabBookingInfo.matchInfantTravelWithAdult = function(){
		
		// Infant
		infantWithWrongAdult = false;
		var travelWithArr = new Array();
		var travelWithMatch = false;
		var objJPax = jsonPaxAdtObj.paxInfants;
		var intLen = objJPax.length;
		var i = 0;
		if (intLen > 0){
			do{
				if ($("#" + (i + 1) + "_displayInfantSelect").attr('checked')){
					travelWithArr.push(objJPax[i].displayInfantTravellingWith);
					travelWithMatch = true;
				}
				i++;
			}while(--intLen);
		}
		
		// Adult Validations if infant selected
		if(travelWithMatch){
			objJPax = jsonPaxAdtObj.paxAdults;
			intLen = objJPax.length;
			i = 0;
			var selectedAdults = {};
			do{
			    if ($("#" + (i + 1) + "_displaySelectPax").attr('checked')){
			        selectedAdults[objJPax[i].displayPaxSequence+''] = true;
                }
                i++;
            }while(--intLen);
			
			for(var count = 0; count < travelWithArr.length; count++){
			    if(!(selectedAdults[travelWithArr[count]+''] === true)){
			        travelWithMatch = false;
			        break;
			    }
			}
			
//			i = 0;
//			var x = 0;
//			if (intLen > 0){
//				do{
//				    for(var count=0;count<travelWithArr.length;count++){
//						if ($("#" + (i + 1) + "_displaySelectPax").attr('checked')){
//							var tempMatch = false;
//							if(travelWithArr[count]==objJPax[i].displayPaxSequence){
//								tempMatch = true;
//								break;
//							}
//							if(travelWithArr.length > 0){						
//								if (x == 0){
//									if(travelWithMatch && !tempMatch){
//										infantWithWrongAdult = true;
//									}
//									travelWithMatch = travelWithMatch || tempMatch;
//								} else {
//									travelWithMatch = travelWithMatch && tempMatch;
//									infantWithWrongAdult = false;
//								}
//							}					
//							x++;
//						}
//						i++;
//				    }
//				}while(--intLen);
//			}
		} else {
			objJPax = jsonPaxAdtObj.paxAdults;
			intLen = objJPax.length;
			i = 0;
			if (intLen > 0){
				do{
					if ($("#" + (i + 1) + "_displaySelectPax").attr('checked')){
						travelWithMatch = true;
						break;
					}
					i++;
				}while(--intLen);
			}
		}
		return travelWithMatch;
	}
	
	UI_tabBookingInfo.allPaxSelectedValidate = function(){
		UI_commonSystem.initializeMessage();
		UI_tabBookingInfo.setSelectedPaxInfo();
		
		var objAdt = eval(UI_tabBookingInfo.jsSltdAdt);
		var objInf = eval(UI_tabBookingInfo.jsSltdInf);
		
		var sltAdCount = 0;
		if(objAdt != null){
			sltAdCount = objAdt.length;
		}

		var sltInfCount = 0;
		if(objInf != null){
			sltInfCount = objInf.length;
		}

		var adCount = jsonPaxAdtObj.paxAdults.length;
		var infCount = jsonPaxAdtObj.paxInfants.length;

		if((infCount + adCount) == (sltAdCount + sltInfCount) || (adCount == sltAdCount)){
			alert("You cannot select all the passenger");
			UI_tabBookingInfo.resetOnClick();
			return false;
		}

		return true;
	}
	
	
	/*
	 * Remove Validate
	 */
	UI_tabBookingInfo.removeValidate = function(){
		UI_commonSystem.initializeMessage();
		UI_tabBookingInfo.setSelectedPaxInfo();
		
		if (UI_tabBookingInfo.jsSltdAdt == "" && UI_tabBookingInfo.jsSltdInf == ""){
			showERRMessage(raiseError("XBE-ERR-40"));
			return false;
		}
		
		if(!UI_tabBookingInfo.allPaxSelectedValidate()){
			return false;
		}
		
		return true;
	}
	
	/*
	 * Set Selected Pax Ids
	 */
	UI_tabBookingInfo.setSelectedPaxInfo = function(){

		UI_tabBookingInfo.jsSltdAdt = "";
		UI_tabBookingInfo.jsSltdInf = "";
		
		var strJS = "";
		
		// Adult Validations
		var objJPax = jsonPaxAdtObj.paxAdults;
		var intLen = objJPax.length;
		var i = 0;
		var x = 0;
		if (intLen > 0){
			do{
				if ($("#" + (i + 1) + "_displaySelectPax").attr('checked')){
					if (x != 0){strJS += "," }
					strJS += "{" 
					strJS += "\"paxID\":\"" + objJPax[i].displayPaxTravelReference + "\"";
					strJS += "}" 
					x++;
				}
				i++;
			}while(--intLen);
		}
		
		if (strJS != ""){UI_tabBookingInfo.jsSltdAdt = "[" + strJS + "]";}
		
		// Infant
		strJS = "";
		objJPax = jsonPaxAdtObj.paxInfants;
		intLen = objJPax.length;
		i = 0;
		x = 0;
		if (intLen > 0){
			do{
				if ($("#" + (i + 1) + "_displayInfantSelect").attr('checked')){
					if (x != 0){strJS += "," }
					strJS += "{" 
					strJS += "\"paxID\":\"" + objJPax[i].displayPaxTravelReference + "\"";
					strJS += "}" 
					x++;
				}
				i++;
			}while(--intLen);
		}
		if (strJS != ""){UI_tabBookingInfo.jsSltdInf = "[" + strJS + "]";}
	}
	
	/*
	 * Booking Informations
	 */
	UI_tabBookingInfo.getBookingInfo = function(){
		var dataBkgInfo = {};
		dataBkgInfo["bookingInfo.PNR"]		= jsonPNR;
		dataBkgInfo["bookingInfo.version"]	= jsonBkgInfo.version;	
		dataBkgInfo["bookingInfo.status"]	= jsonBkgInfo.status;		
		return dataBkgInfo;
	}
	
	
	/*
	 * Add Segment On Click
	 */
	UI_tabBookingInfo.addSegmentOnClick = function(params){
		var blnIsGroundStationAllowed = params.blnIsGroundStationAllowed;
		var blnIsFromNameChange = params.blnIsFromNameChange;		

		UI_commonSystem.pageOnChange();		
		$('#pnrNo').val(jsonPNR);
		if (jsonGroupPNR == "null" || jsonGroupPNR == "") {
			$('#groupPNRNo').val(false);	
		} else {
			$('#groupPNRNo').val(true);			
		}
		$('#version').val(jsonBkgInfo.version);
		$('#resSegments').val(UI_reservation.jsonSegs);
		$('#resPax').val(UI_reservation.jsonPassengers);				
		$('#resNoOfAdults').val(UI_reservation.adultCount);
		$('#resNoOfchildren').val(UI_reservation.childCount);
		$('#resNoOfInfants').val(UI_reservation.infantCount);
		$('#resOwnerAgent').val(UI_reservation.ownerAgent);
		$('#resContactInfo').val(UI_reservation.contactInfo);		
		$('#resPaySummary').val($.toJSON(UI_reservation.paySummary));
		$('#flexiAlertInfo').val($.toJSON(UI_reservation.jsonFlexiAlerts));
		$('#taxInvoicesExist').val(UI_reservation.taxInvoices != undefined && UI_reservation.taxInvoices != null
			&& UI_reservation.taxInvoices.length != 0);
		$('#ticketExpiryDate').val(jsonBkgInfo.ticketExpiryDate);
		$('#ticketExpiryEnabled').val(jsonBkgInfo.ticketExpiryEnabled);
		$('#ticketValidFrom').val(jsonBkgInfo.ticketValidFromDate);	
		
		var hasInsurance = UI_tabBookingInfo.hasInsurance();
		$('#resHasInsurance').val(hasInsurance);
		
		$('#pgMode').val("addSegment");
		var ond = UI_tabBookingInfo.addGroundSegmentSelectData.selectedOnD;
		if(blnIsGroundStationAllowed){
			$('#addGroundSegment').val(true);	
			$('#flightDetails').val($.toJSON(UI_reservation.allFlightDetails));	
			$('#selectedFlightSegRefNumber').val(UI_tabBookingInfo.selectedFlightSeg.lastSelectedFlightSegRefNumber);	
			$('#selectedPnrSeg').val(UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef);	
			if(ond == null || ond == ""){
				// shouldnt happend
			}			
		}else {
			$('#addGroundSegment').val(false);
		}
		$('#selectedOnd').val(ond);
		$('#blnIsFromNameChange').val(blnIsFromNameChange);
		if(blnIsFromNameChange){
			$('#paxNameDetails').val($.toJSON(params.paxNameDetails));
		}		
		
		$("#frmModiBkg").action("addSegment.action");	
		$("#frmModiBkg").submit();
		
	}
	
	UI_tabBookingInfo.hasInsurance = function () {
		var hasIns = false;
		if(jsonInsurances!=null && jsonInsurances.length > 0 ){
			$.each(jsonInsurances, function (key, insObject){
				var dateOfTravel = insObject.dateOfTravel;
				if(dateOfTravel != null && dateOfTravel != ''){
					hasIns = true;
					return;
				 }
			});
		}
		return hasIns;
	}
	
	UI_tabBookingInfo.newPnrOnClick = function() {
		$('#oldAdultPaxInfo').val($.toJSON(jsonPaxAdtObj.paxAdults));
		$('#oldInfantPaxInfo').val($.toJSON(jsonPaxAdtObj.paxInfants));
		$('#oldContactInfo').val($.toJSON(jsonContactInfo));
		$('#oldNoOfAdults').val(UI_reservation.adultCount);
		$('#oldNoOfChildren').val(UI_reservation.childCount);
		$('#oldNoOfInfants').val(UI_reservation.infantCount);
		$("#frmModiBkg").action("makeReservation.action");
		$("#frmModiBkg").submit();
	}
	 UI_tabBookingInfo.getSelectedSegmentForAddSegment = function(){
			var allSegs = DATA_ResPro.resSegments;
			var out = [];
			for(var i = 0 ;i < allSegs.length; i++){
				if(allSegs[i].status != 'CNX'){
					out[out.length] = allSegs[i];
				}
			}
			return out;
		}
	
	/*
	 * Modify Segment On Click
	 */ 
	UI_tabBookingInfo.modifySegmentOnClick = function(){
		UI_commonSystem.pageOnChange();		
		if (UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef == null){
			showERRMessage(raiseError("XBE-ERR-23","Segment"));
			return false;
		}else{
			$('#pnrNo').val(jsonPNR);
			$('#groupPNRNo').val(jsonGroupPNR);
			$('#fltSegment').val(UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef);
			$('#version').val(jsonBkgInfo.version);
			$('#resSegments').val(UI_reservation.jsonSegs);
			$('#resPax').val(UI_reservation.jsonPassengers);				
			$('#resNoOfAdults').val(UI_reservation.adultCount);
			$('#resNoOfchildren').val(UI_reservation.childCount);
			$('#resNoOfInfants').val(UI_reservation.infantCount);
			$('#resOwnerAgent').val(UI_reservation.ownerAgent);
			$('#resContactInfo').val(UI_reservation.contactInfo);
			$('#modifyStatus').val(jsonBkgInfo.status);
			$('#bookingType').val(UI_tabBookingInfo.getModifySegmentBookingType());
			$('#flexiAlertInfo').val($.toJSON(UI_reservation.jsonFlexiAlerts));
			$('#modifyFlexiBooking').val(UI_tabBookingInfo.modifyFlexiBooking);
			$('#resPaySummary').val($.toJSON(UI_reservation.paySummary));
			
			$('#ticketExpiryDate').val(jsonBkgInfo.ticketExpiryDate);
			$('#ticketExpiryEnabled').val(jsonBkgInfo.ticketExpiryEnabled);
			$('#ticketValidFrom').val(jsonBkgInfo.ticketValidFromDate);		
			
			var hasInsurance = UI_tabBookingInfo.hasInsurance();
			//if(jsonInsurance != null && jsonInsurance.dateOfTravel !=null && (jsonInsurance.state == 'Success' || jsonInsurance.state == 'Processing' || jsonInsurance.state == 'Failed') ){
			//	hasInsurance = true;
			//}
			$('#resHasInsurance').val(hasInsurance);
			$('#pgMode').val("modifySegment");
			// segment modification
			$('#allowModifyDate').val(UI_tabBookingInfo.selectedFlightSeg.modifyByDate);
			$('#allowModifyRoute').val(UI_tabBookingInfo.selectedFlightSeg.modifyByRoute);
			$("#frmModiBkg").action("modifySegment.action");	
			$("#frmModiBkg").submit();
		}
	}
	
	UI_tabBookingInfo.getModifySegmentBookingType = function(){
		var seg = UI_tabBookingInfo.getFirstModifySeg(UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef, UI_reservation.jsonSegs);		
		if(seg.length > 0 ){
			return seg[0].bookingType
		}		
		return '';
	}
	
	UI_tabBookingInfo.getFirstModifySeg = function(selectedSeg, allSegs){
		var modifyingSegIds = selectedSeg.split(',');
		var segIdHash = {};
		for(var i = 0; i < modifyingSegIds.length; i++){
			segIdHash[modifyingSegIds[i]] = true;
		}
		var out = [];
		var allparseSeg = $.parseJSON(allSegs);
		for(var i = 0; i < allparseSeg.length; i++){
			var key = allparseSeg[i].bookingFlightSegmentRefNumber+'$'+allparseSeg[i].interlineGroupKey;
			if(segIdHash[key]){
				out[out.length] = allparseSeg[i];
				break;
			}
		}
		return out;
	}
	
	/*
	 * Cancel segment On Click
	 */
	UI_tabBookingInfo.cancelSegmentOnClick = function(){
		UI_commonSystem.pageOnChange();
		if (UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef == null){
			showERRMessage(raiseError("XBE-ERR-23","Segment"));
			return false;
		}else{
			$('#flexiAlertInfo').val($.toJSON(UI_reservation.jsonFlexiAlerts));
			UI_tabBookingInfo.showConfirmUpdate("CANCELSEGMENT");
		}
	}
	
	
	/**
	 * Invokes when the user clicks on open return confirm button. Submits to
	 * modifySegment.action with pgMode = confirmOprt. So in the flightSearch
	 * screen behavior is set based on the mode ="confirmOprt" Lalanthi
	 * 05/11/2010
	 */
	UI_tabBookingInfo.confirmReturnOnClick= function(){
		UI_commonSystem.pageOnChange();	
		if (UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef == null){
			showERRMessage(raiseError("XBE-ERR-23","Segment"));
			return false;
		}else{
			$('#pnrNo').val(jsonPNR);
			$('#groupPNRNo').val(jsonGroupPNR);
			$('#fltSegment').val(UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef);
			$('#version').val(jsonBkgInfo.version);
			$('#resSegments').val(UI_reservation.jsonSegs);
			$('#resPax').val(UI_reservation.jsonPassengers);				
			$('#resNoOfAdults').val(UI_reservation.adultCount);
			$('#resNoOfchildren').val(UI_reservation.childCount);
			$('#resNoOfInfants').val(UI_reservation.infantCount);
			$('#resOwnerAgent').val(UI_reservation.ownerAgent);
			$('#resContactInfo').val(UI_reservation.contactInfo);
			$('#modifyStatus').val(jsonBkgInfo.status);
			$('#bookingType').val(UI_tabBookingInfo.getModifySegmentBookingType());
			$('#flexiAlertInfo').val($.toJSON(UI_reservation.jsonFlexiAlerts));
			$('#modifyFlexiBooking').val(UI_tabBookingInfo.modifyFlexiBooking);
			$('#resPaySummary').val($.toJSON(UI_reservation.paySummary));
			var hasInsurance = UI_tabBookingInfo.hasInsurance();
		    //if(jsonInsurance != null && jsonInsurance.dateOfTravel !=null && (jsonInsurance.statue == 'Success' || jsonInsurance.statue == 'Processing') ){
			//	hasInsurance = true;
			// }
			$('#resHasInsurance').val(hasInsurance);
			$('#pgMode').val("confirmOprt");
			$("#frmModiBkg").action("modifySegment.action");	
			$("#frmModiBkg").submit();
		}
	}
	
	/*
	 * Add Infants On Click
	 */
	UI_tabBookingInfo.addInfantsOnClick = function(){
		UI_tabBookingInfo.disableEnableControls(true,false ,false);
		UI_commonSystem.pageOnChange();		
		
		// Add a new empty row to infant grid
		var currMaxRow = $("#tblPaxInf").getGridParam("records");
		if(currMaxRow == null || currMaxRow == ""){
			currMaxRow = 0;
			$("#divInfantPane").show();
			UI_tabBookingInfo.constructGridInf({id:"#tblPaxInf"});				
		}
 
		// Doing in the front end level adding the infant saving will take care
		// of concurrent issue
		// if InfantPaxTO changed change this too.
		var emptyData = [{displayPaxTravelReference:"",displayInfantType:"BABY",displayInfantFirstName:"",displayInfantLastName:"",
							displayInfantNationality:"",displayInfantDOB:"",displayInfantTravellingWith:"",
							displayInfantSSR:"",
							displayInfantSSRCode:"",
							displayInfantSSRInformation:"",
							displayInfantSelect:"",
							displayPaxTravelReference:"",
							nameEditable:true}];
		if(currMaxRow ==0) {	
			currMaxRow++;
			$("#tblPaxInf").addRowData(currMaxRow,emptyData[0]);
			$("#tblPaxInf").editRow(currMaxRow); 
			$("#tblPaxInf").setSelection(currMaxRow,true);
		} else {	
			$("#tblPaxInf").addRowData(currMaxRow+1,emptyData[0]);
			$("#tblPaxInf").editRow(currMaxRow+1); 
			$("#tblPaxInf").setSelection(currMaxRow+1,true);
		}
		UI_tabBookingInfo.mapAddInfantCalendar();
		
		$("#btnAddInfants").disable();
		$("#btnAddInfants").hide();
		$("#btnSaveInfants").show();
		$("#btnSaveInfants").enable();
		$("#btnEdit").show();
		$("#btnSave").hide();		
	}
	
	/**
	 * Validates and saves infant data
	 */
	UI_tabBookingInfo.saveInfantsOnClick = function(saveMode){
		UI_commonSystem.pageOnChange();	
		
		var	selectedRow = $("#tblPaxInf").getGridParam("selrow");
		if(UI_tabBookingInfo.validateAddInfantData(selectedRow)){		
			var nationalityCode =  $("#"+ selectedRow + "_displayInfantNationality").val();	
			var infAcmAdtSeq = $("#"+ selectedRow + "_displayInfantTravellingWith").val();
			
			var rowTR = $("#"+ infAcmAdtSeq + "_displayAdultFirstName").closest('tr');
			
			var adtSeqNo	= rowTR.find('td:eq(1)').text();
				
			// $("#tblPaxInf").saveRow(selectedRow, false, 'clientArray'); // no
			// need of this we disable it any way
			var data = UI_tabBookingInfo.getBookingInfo();
			data["groupPNR"]	= jsonGroupPNR;
			data["infFName"]          =	$("#"+ selectedRow + "_displayInfantFirstName").val()// $("#tblPaxInf").getRowData(selectedRow)["displayInfantFirstName"];
			data["infLName"]          =	$("#"+ selectedRow + "_displayInfantLastName").val()// $("#tblPaxInf").getRowData(selectedRow)["displayInfantLastName"];
			data["infNationality" ]   = nationalityCode;
			data["infDOB"]            =	$("#"+ selectedRow + "_displayInfantDOB").val()// $("#tblPaxInf").getRowData(selectedRow)["displayInfantDOB"];
			data["infTravellingAdult"]= adtSeqNo;
			data["respaxs"] = UI_reservation.jsonPassengers;
			data["resPaySumm"] = $.toJSON(UI_reservation.paySummary);
			data["contactInfo"] = UI_reservation.contactInfo;	
			data["saveMode"] =	saveMode;
			data["infTitle"] = 	$("#"+ selectedRow + "_displayInfantTitle").val();			

			if (jsonPaxAdtObj.paxInfants.length > 0){
				data["infNationalID"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayNationalIDNo;
				data["infPnrPaxCatFOIDExpiry"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayPnrPaxCatFOIDExpiry;
				data["infPnrPaxCatFOIDNumber"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayPnrPaxCatFOIDNumber;
				data["infPnrPaxCatFOIDPlace"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayPnrPaxCatFOIDPlace;
				data["infPnrPaxPlaceOfBirth"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayPnrPaxPlaceOfBirth;
				data["infVisaApplicableCountry"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayVisaApplicableCountry;
				data["infVisaDocIssueDate"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayVisaDocIssueDate;
				data["infVisaDocNumber"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayVisaDocNumber;
				data["infVisaDocPlaceOfIssue"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayVisaDocPlaceOfIssue;
				data["infTravelDocType"] = jsonPaxAdtObj.paxInfants[selectedRow - 1].displayTravelDocType;
			}
			
			var $inputs = $('#frmPayment :input');
			$inputs.each(function() {
				data[this.name] = $(this).val();
			});	
			if(saveMode =="SAVEINF"){				
				var selectedAgent = $('#selAgents').val();
				if(!selectedAgent || trim(selectedAgent) == "") {
					// If no agent is selected, or the drop down box is
					// disabled, then use the logged in agent
					selectedAgent = top.strAgentCode;
				}	
				data['payment.agent'] = selectedAgent;
				data['payment.type'] = paymentType;
			}
			data['paxPayments'] = $.toJSON(jsonPaxPayObj);
			data["pnrSegments"] = UI_reservation.jsonSegs;
			// $("#tblPaxInf").getRowData(selectedRow)["displayInfantTravellingWith"];
			UI_commonSystem.showProgress();
			UI_tabBookingInfo.enableAllButtons(false);
			$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"saveInfant.action",							
				success: UI_tabBookingInfo.saveInfantSuccess,error:UI_tabBookingInfo.saveInfantError});
			
			return false;
		}	
	}
	
	/**
	 * Validates newly added infant data
	 */
	UI_tabBookingInfo.validateAddInfantData = function(selRow){
		UI_commonSystem.initializeMessage();
		
		var fNameReq = paxConfigIN.firstName.xbeVisibility && paxConfigIN.firstName.xbeMandatory;
		var lNameReq = paxConfigIN.lastName.xbeVisibility && paxConfigIN.lastName.xbeMandatory;
		var nationalityReq = paxConfigIN.nationality.xbeVisibility && paxConfigIN.nationality.xbeMandatory;
		var dobReq = paxConfigIN.dob.xbeVisibility && paxConfigIN.dob.xbeMandatory;
		var travelWithReq = paxConfigIN.travelWith.xbeVisibility && paxConfigIN.travelWith.xbeMandatory;
		var titleReq = paxConfigIN.title.xbeVisibility && paxConfigIN.title.xbeMandatory;
		var passportNoReq = paxConfigIN.passportNo.xbeVisibility && paxConfigIN.passportNo.xbeMandatory;
		var nationalIdReq = false;
		if(top.isRequestNICForReservationsHavingDomesticSegments == "true" && paxConfigIN.nationalIDNo.xbeVisibility) {
			if(UI_tabBookingInfo.isDomesticFlightExist() && !DATA_ResPro.initialParams.allowSkipNic){
				nationalIdReq = true;
			}
		} else if(paxConfigIN.nationalIDNo.xbeMandatory && paxConfigIN.nationalIDNo.xbeVisibility && !DATA_ResPro.initialParams.allowSkipNic){
			nationalIdReq = true;
		}
		if (!UI_commonSystem.controlValidateName({id:"#"+ selRow +"_displayInfantFirstName", desc:"Passport Number", required:passportNoReq})){return false;}
		if (!UI_commonSystem.controlValidateName({id:"#"+ selRow +"_displayInfantFirstName", desc:"First Name", required:fNameReq})){return false;}
		if (!UI_commonSystem.controlValidateName({id:"#"+ selRow + "_displayInfantLastName", desc:"Last Name", required:lNameReq})){return false;}
		if (!UI_commonSystem.controlValidate({id:"#"+ selRow + "_displayInfantNationality", desc:"Nationality", required:nationalityReq})){return false;}
		if (!UI_commonSystem.controlValidate({id:"#"+ selRow + "_displayInfantDOB", desc:"Date of Birth", required:dobReq})){return false;}
		if (!UI_commonSystem.controlValidate({id:"#"+ selRow + "_displayInfantTravellingWith", desc:"With Adult", required:travelWithReq})){return false;}
		if(titleReq){
			if (!UI_commonSystem.controlValidateName({id:"#"+ selRow +"_displayInfantTitle", desc:"Title", required:titleReq})){return false;}	
		}
		
		if (!UI_commonSystem.validateANW({id:"#"+ selRow + "_displayInfantFirstName", desc:"Infant" + UI_commonSystem.strAPS + "s First Name"})){return false;}
		if (!UI_commonSystem.validateANW({id:"#"+ selRow + "_displayInfantLastName", desc:"Infant" + UI_commonSystem.strAPS + "s Last Name"})){return false;}
	
		if(!($("#" + selRow + "_displayInfantFirstName").val() == 'T B A' ||
				$("#" + selRow + "_displayInfantLastName").val() == 'T B A')){
			if(nationalIdReq){
				if(jsonPaxAdtObj.paxInfants[selRow - 1].displayNationalIDNo == "" ||
						jsonPaxAdtObj.paxInfants[selRow - 1].displayNationalIDNo == undefined){
					showERRMessage(raiseError("XBE-ERR-01", "National ID No"));
					return false ;
				}								
			}
			if(passportNoReq){
				if(jsonPaxAdtObj.paxInfants[selRow - 1].displayPnrPaxCatFOIDNumber == "" ||
						jsonPaxAdtObj.paxInfants[selRow - 1].displayPnrPaxCatFOIDNumber == undefined){
					showERRMessage(raiseError("XBE-ERR-01", "Passport No"));
					return false ;
				}								
			}
		}
			
			/* Validate infant date of birth */
				if (!$("#"+ selRow + "_displayInfantDOB").isFieldEmpty()){
					if (!CheckDates($("#"+ selRow + "_displayInfantDOB").val(), top.arrParams[0])){
						showERRMessage(raiseError("XBE-ERR-63", "Date of birth"));
						$("#"+ selRow + "_displayInfantDOB").focus();
						return false ;		
					}

					if($.trim($("#"+ selRow + "_displayInfantDOB").val()) !== "") {
						if(!dateValidDate(($("#"+ selRow + "_displayInfantDOB").val()))) {
							showERRMessage(raiseError("XBE-ERR-04","Date of birth"));
							$("#"+ selRow + "_displayInfantDOB").focus();
							return false;
						}
					}
					/* Validates infant age */
					var intInfantAge	          = top.strDefInfantAge;
					var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;
					// Validates infant age - should be less than 2 years
					if($.trim($("#"+ selRow + "_displayInfantDOB").val()) !== "" ) {
						if(!ageCompare(($("#"+ selRow + "_displayInfantDOB").val()),jsonLastFlightDate,intInfantAge)) {
							showERRMessage(raiseError("XBE-ERR-64","Date of birth","Infant"));
							$("#"+ selRow + "_displayInfantDOB").focus();
							return false;
						}
					}
					
					// Infant age should be greater than 16 days
					if($.trim($("#"+ selRow + "_displayInfantDOB").val()) !== "" && jsonLastFlightArrivalDate != "") {
						if(!UI_tabBookingInfo.checkInfantMinAge(($("#"+ selRow + "_displayInfantDOB").val()),jsonLastFlightArrivalDate,infantAgeLowerBoundInDays))  {
							showERRMessage(raiseError("XBE-ERR-70","Date of birth",infantAgeLowerBoundInDays));
							$("#"+ selRow + "_displayInfantDOB").focus();
							return false;
						} 
					}
					
					
				}

			/* Traveling With Validations */
			if(jsonPaxAdtObj.paxInfants.length > 0 && selRow > 1){// If there
																	// are more
																	// than 1
																	// infant.
				var x = 0;
				var intWLen = jsonPaxAdtObj.paxInfants.length;
				
				while(intWLen>0){
					if(selRow != (x + 1)) {
						if ($("#"+ selRow + "_displayInfantTravellingWith").val() == $("#"+ (x + 1) + "_displayInfantTravellingWith").val()){
							showERRMessage(raiseError("XBE-ERR-09"));
							$("#"+ selRow + "_displayInfantTravellingWith").focus();
							return false ;
						}						
					}
					x++;
					intWLen--;
				}
					
				for(var i=0;i<UI_tabBookingInfo.savedInfants.length;i++){
					if($("#"+ selRow + "_displayInfantTravellingWith").val() == UI_tabBookingInfo.savedInfants[i]){
						showERRMessage(raiseError("XBE-ERR-90",x));
						$('#tblPaxInf tr:last').remove();
						$("#btnSaveInfants").disable();
						$("#btnSaveInfants").hide();
						$("#btnAddInfants").show();
						$("#btnAddInfants").enable();
						$("#btnEdit").hide();						
						$("#btnSave").show();
						return false;
					}
				}
			}


			return true;
	}
	
	
	/**
	 * Checking if Travaling with Data Change when adding new Infant
	 */
	/*
	UI_tabBookingInfo.validateOldInfantTravellingWithDataChange = function(infAcmAdtSeq) {
		
		for(var i = 0 ;i<jsonPaxAdtObj.paxInfants.length;i++){
			if(infAcmAdtSeq == jsonPaxAdtObj.paxInfants[i].displayInfantTravellingWith){
				return true;
			}
				
		}
		return false;
		
	}
	*/

	UI_tabBookingInfo.saveInfantSuccess = function(response){
		var forceConfirm = DATA_ResPro.initialParams.forceConfirm;
		$("#btnSaveInfants").disable();
		if(response.success && response.infantFare){			
			//check whether user have privilages to confirm without payment
			if(forceConfirm){
				if(confirm("Proceed to make payment?")){ 
					UI_tabBookingInfo.directToPaymentPage(response);
					
				}else {
					// clear the blocked ids
					UI_tabPayment.isAddInfant = false;
					UI_tabBookingInfo.loadAutoCnxBalanceToPay = true;
					UI_reservation.loadReservation("", null, false);	
					return false;
				}
			}else{
				UI_tabBookingInfo.directToPaymentPage(response);
			}		
		}else {
			UI_reservation.loadReservation("SAVEINF", response,false);
		}
			
	}
	
	UI_tabBookingInfo.saveInfantError= function(response){
		$("#btnSaveInfants").disable();
		UI_commonSystem.setErrorStatus(response);
	}
	
	UI_tabBookingInfo.directToPaymentPage = function(response){
		UI_commonSystem.hideProgress();
		UI_tabPayment.updatePayemntTabTO(response);
		jsonResPaymentSummary = response.paymentSummaryTO;				
		jsonPaxPayObj  =  response.passengerPayment;
		jsonPaxPriceTo = 	response.passengerPayment;
		UI_reservation.balnceTOPay =  response.paymentSummaryTO.balanceToPay;
		UI_reservation.tabStatus.tab2 = true;
		UI_tabPayment.isModify = true;
		UI_tabPayment.isAddInfant = true;
		UI_reservation.openTab(2);
		UI_tabPayment.paymentDetailsOnLoadCall()
		
	}
	
	UI_tabBookingInfo.saveInfantSuccessUpdate = function(response){
		if(response.success){
			showCommonError("CONFIRMATION", "Infant Added Successfully");
			if(!response.paysuccess){
				if(confirm("Proceed to make payment?")){				
					$("#divBookingTabs").tabs("option", "active", 2); // Redirect to
															// payments tab.
					UI_commonSystem.pageOnChange();
					UI_tabPayment.ready();
					UI_tabPayment.isModify = true;
					UI_tabPayment.isAddInfant = false;
					UI_tabPayment.paymentModifyDetailsOnLoadCall();
					UI_reservation.tabStatus.tab2 = true;
				}else {
					return false;
				}
			}else {
				UI_tabPayment.isAddInfant = false;
				UI_reservation.openTab(0);
				showCommonError("CONFIRMATION", "Infant Added Successfully");
			}
			
		}else {
			showERRMessage(response.messageTxt);			
			return false;		
		}		
	}
	
	/*
	 * Remove Pax On Click
	 */
	UI_tabBookingInfo.removePaxOnClick = function(){
		UI_commonSystem.pageOnChange();
		
		if (UI_tabBookingInfo.removeValidate()){
			if(confirm("Are you sure you want to remove these passengers?")){
				$('#flexiAlertInfo').val($.toJSON(UI_reservation.jsonFlexiAlerts));
				UI_tabBookingInfo.showConfirmUpdate("REMOVEPAX");
				return false;
			}
		}
	}
	
	/*
	 * Split Booking On Click
	 */ 
	UI_tabBookingInfo.splitOnClick = function(){
		UI_commonSystem.pageOnChange();
		if (UI_tabBookingInfo.splitValidate()){
			if(confirm("Are you sure you want to split this reservation?")){
				UI_commonSystem.showProgress();
				var data = UI_tabBookingInfo.getBookingInfo();
				data["paxAdults"] = UI_tabBookingInfo.jsSltdAdt;
				data["paxInfants"] = UI_tabBookingInfo.jsSltdInf;
				data["groupPNR"]	= jsonGroupPNR;
				data["respaxs"] = $.toJSON(UI_tabBookingInfo.getSpliPaxInfo());
				UI_tabBookingInfo.enableAllButtons(false);
				UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:"splitPax.action",							
					success: UI_tabBookingInfo.splitSuccess,error:UI_commonSystem.setErrorStatus});							
				return false;
			}else{
				UI_tabBookingInfo.resetOnClick();
				return false;
			}
		}
	}
	
	// Fill the paxifo only needed for spliting
	UI_tabBookingInfo.getSpliPaxInfo = function(){
		var jPassengers = $.parseJSON(UI_reservation.jsonPassengers);
		var jpData = [];
		var splitPax= {"firstName":"", "travelerRefNumber": "", "infants":"", "paxType":"", "paxSequence":"", "parent": ""};
		var tmpSplit = {};
		for(var jpl=0;jpl < jPassengers.length;jpl++){
			tmpSplit = $.airutil.dom.cloneObject(splitPax);
			tmpSplit.firstName = jPassengers[jpl].firstName;
			tmpSplit.infants=jPassengers[jpl].infants;
			tmpSplit.paxType=jPassengers[jpl].paxType;
			tmpSplit.travelerRefNumber=jPassengers[jpl].travelerRefNumber;
			tmpSplit.paxSequence = jPassengers[jpl].paxSequence;
			tmpSplit.parent = jPassengers[jpl].parent;
			jpData[jpl]= tmpSplit;
		}
		
		return jpData;
	}
	
	
	/*
	 * Split Success
	 */
	UI_tabBookingInfo.splitSuccess = function(response) {		
		UI_reservation.loadReservation("SPLIT", response,false);		
	}
	
	UI_tabBookingInfo.splitSuccessUpdate = function(response){
		if(response.success){
			showCommonError("CONFIRMATION", response.newPNRNoMsg);
			jsonTravellingWith = UI_tabBookingInfo.populateJsonTravellingWith(jsonPaxAdtObj);
//			$('#tblPaxInf').GridUnload();
//			UI_tabBookingInfo.constructGridInf({id:"#tblPaxInf"});
//			UI_commonSystem.fillGridData({id:"#tblPaxInf", data:jsonPaxAdtObj.paxInfants, editable:true})
			if(jsonPaxAdtObj.paxInfants.length == 0){
				$("#divInfantPane").hide();
			} else {
			    for( i = 1; i <= jsonPaxAdtObj.paxInfants.length; i++){
	                for(j=0;j < jsonPaxAdtObj.paxAdults.length; j++) {
	                    if(jsonPaxAdtObj.paxAdults[j].displayPaxSequence == jsonPaxAdtObj.paxInfants[i - 1].displayInfantTravellingWith){
	                        var adtSeq = j+1;
	                        $("#"+ i + "_displayInfantTravellingWith").val(adtSeq);
	                        break;
	                    }
	                }
	            }
			}
			UI_tabBookingInfo.adjustPageHeight(null, jsonPaxAdtObj.paxInfants);
			if(jsonGroupPNR != null && jsonGroupPNR != ""){
				// top[0].addLCCPNRHistory(response.newPNRNo);
				var jsContactInfo =$.parseJSON(UI_reservation.contactInfo);
				top[0].addLCCPNRHistory(response.newPNRNo, jsContactInfo.firstName);
			}else {
				// Fix this
				var jsContactInfo =$.parseJSON(UI_reservation.contactInfo);
				top[0].addPNRHistoryNew(response.newPNRNo, jsContactInfo.firstName);
			}
			parent.UI_searchReservation.writePNR();
			top[0].showPNRIcon();
			UI_tabBookingInfo.refreshPromotionAlert();
		}else {
			showERRMessage(response.messageTxt);
		}	
	}
	UI_tabBookingInfo.advPaxSelOnClick = function() {
		UI_commonSystem.pageOnChange();
		UI_tabBookingInfo.advancePassengerSelection();
	}
	
	
	UI_tabBookingInfo.advancePassengerSelection = function() {
		var strUrl = 'showNewFile!loadAdvancedPassengerSelectionForSplit.action?mode=MODIFY';
		
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(300, 600));
	//	top.objCW= window.open(strUrl,"myWindow",$("#popWindow").geCustomtPopWindowProp(300, 350));
		
	}
	
	/*
	 * Transfer On Click
	 */ 
	UI_tabBookingInfo.transferOnClick = function(){
		
		UI_commonSystem.pageOnChange();
		// If no agent is selected then error
		var transfereeAgent = $("#selTransfer").val();
		if(transfereeAgent == '') {
			showERRMessage(raiseError("XBE-ERR-01","Agent"));
			$("#selTransfer").focus();
			return false;
		}
		
		var data = {};
		data["groupPNR"] 		= jsonGroupPNR;
		data["transfereeAgent"] = transfereeAgent;
		data["pnr"] 	        = jsonPNR;
		$('#version').val(jsonBkgInfo.version);	
		UI_commonSystem.showProgress();
		$("#btnTransfer").disable();
		UI_tabBookingInfo.enableAllButtons(false);
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"transferAgent.action",							
			success: UI_tabBookingInfo.transferSuccess, error:UI_commonSystem.setErrorStatus});
				
	}
	
	UI_tabBookingInfo.transferSuccess = function(response){		
		UI_reservation.loadReservation("TRANSFER", response, false);		
	}
	
	UI_tabBookingInfo.transferSuccessUpdate = function(response){
		$("#btnTransfer").enable();
		if (response.success) {
			// TODO : Improve message
			showConfirmation("Successfully transferred agent");
		} else {			
			showERRMessage(response.messageTxt);
		}
	}
	
	/*
	 * Extend On Click
	 */
	UI_tabBookingInfo.extendOnClick = function(){
		
		UI_commonSystem.pageOnChange();
		// If no hour is selected then error
		
		var selectedTime = (eval(($("#selExtend").val())/2)) + "";
		var splitResult = selectedTime.split(".");
		var hourOfDay = splitResult[0];
		var minutesOfHour = "";
		if(splitResult[1]=="5"){minutesOfHour = "30"}
		else{minutesOfHour = "0"}
		
		if(hourOfDay == '' ) {
			showERRMessage(raiseError("XBE-ERR-42"));
			$("#selExtend").focus();
			return false;
		}
		
		// If no date is selected then error
		var extendDate = $("#hdnExtendDate").val();
		if(extendDate == '' ) {
			showERRMessage(raiseError("XBE-ERR-43"));
			return false;
		}
		
		var formatedExtendDate = extendDate.substring(3,5) + '/' + extendDate.substring(0,2) + '/' + extendDate.substring(6,10);
		var formatedhourOfDay= hourOfDay;
		if(formatedhourOfDay < 10){
			formatedhourOfDay = '0'+formatedhourOfDay;
		}
		var formatedminutesOfHour = minutesOfHour;
		if(formatedminutesOfHour < 10){
			formatedminutesOfHour = '0' +formatedminutesOfHour;
		}
		formatedExtendDate = formatedExtendDate + '-' + formatedhourOfDay + ':' + formatedminutesOfHour;
		var exceedDeptTime = CheckDateTimes(UI_reservation.firstDepDatetime, formatedExtendDate);
		if(exceedDeptTime){
			showERRMessage(raiseError("XBE-ERR-89"));
			return false;
		}
			
		var data = {};
		data["groupPNR"] 	= jsonGroupPNR;
		data["hourOfDay"] 	= hourOfDay;
		data["minutesOfHour"] = minutesOfHour;
		data["extendDate"] 	= extendDate;
		data["pnr"] 	    = jsonPNR;
		$('#version').val(jsonBkgInfo.version);
		UI_commonSystem.showProgress();
		$("#btnExtend").disable();
		UI_tabBookingInfo.enableAllButtons(false);
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"extendOnHold.action",							
			success: UI_tabBookingInfo.extendSuccess, error:UI_commonSystem.setErrorStatus});	
	}
	
	UI_tabBookingInfo.extendSuccess = function(response){
		UI_reservation.loadReservation("EXTEND", response,false);
	}
	
	UI_tabBookingInfo.extendSuccessUpdate = function(response){
		$("#btnExtend").enable();	
		if (response.success) {				
			// TODO set the balance to pay if exists
			UI_tabBookingInfo.updateBookingStatus(jsonBkgInfo.displayStatus, 0);			
			// TODO : Improve message
			showConfirmation("Successfully exteneded booking");
		} else {			
			showERRMessage(response.messageTxt);
		}
	}
	
	/*
	 * WL change priority On Click
	 */
	UI_tabBookingInfo.changePriorityOnClick = function(){
		
		UI_commonSystem.pageOnChange();		
		// If no value is selected then error
		var priority = $("#selWLPriority").val();
		if(priority == '' ) {
			showERRMessage(raiseError("XBE-ERR-87"));
			return false;
		}
		
		var data = {};
		data["groupPNR"] 	= jsonGroupPNR;		
		data["pnr"] 	    = jsonPNR;
		data["priority"] 	    = priority;
		data["fltSegId"] 	    = UI_tabBookingInfo.WLflightSegId;
		$('#version').val(jsonBkgInfo.version);
		UI_commonSystem.showProgress();
		$("#btnWLPriority").disable();
		UI_tabBookingInfo.enableAllButtons(false);
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"modifyWaitListingPriority.action",							
			success: UI_tabBookingInfo.changePrioritySuccess, error:UI_commonSystem.setErrorStatus});	
	}
	
	UI_tabBookingInfo.changePrioritySuccess = function(response){
		UI_reservation.loadReservation("PRIORITY", response,false);
	}
	
	UI_tabBookingInfo.changePrioritySuccessUpdate = function(response){
		$("#btnWLPriority").enable();	
		if (response.success) {			
//			UI_tabBookingInfo.updateBookingStatus(response.bookingStatus, 0);				
			showConfirmation("Successfully updated wait listed priority.");
		} else {			
			showERRMessage(response.messageTxt);
		}
	}
	/*
	 * Close Button Click
	 */
	UI_tabBookingInfo.closeOnclick = function() {
		// To Do: remove old code
		// ###########################
		/*
		 * UI_commonSystem.pageOnChange();
		 * top[0].removePNRFromHistory(jsonBkgInfo.PNR);
		 * parent.ViewSearch(false); //$("#divReservation",
		 * parent.document).hide();
		 */
		// ###################
		if (!top.loadCheck()){
			return;
		} 
		// AARESAA-13371 fix: keep the bookmarked pnrs until user sign out
		//top[0].removePNRFromHistory(jsonBkgInfo.PNR);
		top[0].showPNRIcon();
		UI_commonSystem.pageOnChange();
		if (top.strPageFrom != ""){
			if (top.strSearchCriteria != "" ) {
				UI_commonSystem.showProgress();
				top[1].location.replace(top.strPageReturn + "?hdnAlertInfo=" + top.strSearchCriteria);
			}else {
				top[1].location.replace(top.strPageReturn);
			}
		}else{
			parent.UI_searchReservation.writePNR();
			parent.UI_searchReservation.ViewSearch();			
		}
	}
	
	/*
	 * View Itinerary On Click
	 */
	UI_tabBookingInfo.printItinerary = function() {
		var loadReservation = true;
		UI_commonSystem.pageOnChange();
		var includePaxFinancials = true; // Default to true
		if($("#chkPax").length > 0) {  
			includePaxFinancials = $("#chkPax").prop("checked");
		}		
		var includePaymentDetails = true; // Default to true
		if($("#chkPay").length > 0) {
			includePaymentDetails = $("#chkPay").prop("checked");
		}
		
		var includeTCDetails = true; // Default to true

		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").is(':checked');
		}
		
		var includeTicketCharges = $("#chkChg").prop("checked");
		// var includeTicketCharges = false;
		
		var itineraryLanguage = $("#selITNLang").val();
		
		var isGroupPNR = (jsonGroupPNR!=null && jsonGroupPNR!= '' && jsonGroupPNR!="null");
		var strUrl = "printItinerary.action"
		+ "?hdnPNRNo=" + jsonBkgInfo.PNR
		+ "&groupPNR=" + UI_tabBookingInfo.isInterlineDryRes(jsonGroupPNR)
		+ "&includePaxFinancials=" + includePaxFinancials
		+ "&includePaymentDetails=" + includePaymentDetails
		+ "&includeTicketCharges=" + includeTicketCharges
		+ "&itineraryLanguage=" + itineraryLanguage 
		+ "&includeTermsAndConditions=" +includeTCDetails
		+ "&operationType=" + 'MODIFY_BOOKING'
		+ "&marketingAirlineCode=" + UI_reservation.marketingAirlineCode
		+ "&airlineCode=" + UI_reservation.airlineCode
		+ "&interlineAgreementId="+ UI_reservation.interlineAgreementId
		+ "&loadReservation="+ loadReservation;
		
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		if (top.objCW) {top.objCW.focus()}
	}
	
	UI_tabBookingInfo.isInterlineDryRes = function (groupPNR) {
		return (groupPNR != null && groupPNR != 'null' && $.trim(groupPNR) != '' && groupPNR != 'false')		
	}
	
	/*
	 * Email Itinerary On Click
	 */
	UI_tabBookingInfo.emailItinerary = function() {
		UI_commonSystem.pageOnChange();
		var includePaxFinancials = true; // Default to true
		if($("#chkPax").length > 0) {  
			includePaxFinancials = $("#chkPax").prop("checked");
		}
		var includePaymentDetails = true; // Default to true
		if($("#chkPay").length > 0) {
			includePaymentDetails = $("#chkPay").prop("checked");
		}
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}

		// TODO : Charges breakdown not part of release plan
		var includeTicketCharges = $("#chkChg").prop("checked");
		// var includeTicketCharges = false;
		
		var itineraryLanguage = $("#selITNLang").val();
	
		var data = {};		
		data["hdnPNRNo"] = jsonBkgInfo.PNR;
		data["groupPNR"]= UI_tabBookingInfo.isInterlineDryRes(jsonGroupPNR);
		data["includePaxFinancials"] = includePaxFinancials;
		data["includePaymentDetails"] = includePaymentDetails;
		data["includeTicketCharges"] = includeTicketCharges;
		data["itineraryLanguage"] = itineraryLanguage;
		data["includeTermsAndConditions"] = includeTCDetails;
		data["operationType"] = "MODIFY_BOOKING";
		
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"emailtItinerary.action",							
			success: UI_tabBookingInfo.emailSuccess, error:UI_commonSystem.setErrorStatus});
	}
	
	UI_tabBookingInfo.emailSuccess = function(response){
		if (response.success) {
			// TODO : Improve message
			showConfirmation("Successfully sent itinerary by email");
			alert("E-Mail Sent Successfully.\r\n It may take several minutes to reach the recipient.");
			$("#btnEmailItn").disable();
		} else {			
			showERRMessage(response.messageTxt);
		}
	}

	UI_tabBookingInfo.paxNamesOnClick = function() {
		UI_tabBookingInfo.showPaxNames();
	}

	UI_tabBookingInfo.showPaxNames = function(){
		var strUrl = 'showNewFile!loadPaxNamesInOtherLanguages.action?mode=MODIFY';

		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(245, 600));
	}

	
	/*
	 * Pax Details On
	 */
	UI_tabBookingInfo.paxDetailsOnClick = function(){	
		UI_tabBookingInfo.getNewInfantDetails();	
		UI_tabBookingInfo.updateInfantDetails();
		UI_commonSystem.pageOnChange();
		UI_tabBookingInfo.showPaxDetails();
	}
	
	UI_tabBookingInfo.internationalFlightDetailsOnClick = function(){
		UI_commonSystem.pageOnChange();
		UI_tabBookingInfo.internationalFlightDetails();
	}
	
	UI_tabBookingInfo.updateInfantDetails = function() {
		var infantCount = $("#tblPaxInf").getGridParam("records");
		if(infantCount != null){
			if (jsonPaxAdtObj.paxInfants.length > 0) {
				for(var i = 0;i < infantCount;i++){
					jsonPaxAdtObj.paxInfants[i].displayInfantFirstName = $("#" + (i + 1) + "_displayInfantFirstName").val();
					jsonPaxAdtObj.paxInfants[i].displayInfantLastName = $("#" + (i + 1) + "_displayInfantLastName").val();
					jsonPaxAdtObj.paxInfants[i].displayInfantTitle = $("#" + (i + 1) + "_displayInfantTitle").val();
				}
			}
		}
	}
	
	UI_tabBookingInfo.getNewInfantDetails = function() {
		var infantCount = $("#tblPaxInf").getGridParam("records");
		if(infantCount != null){
			for(var i = jsonPaxAdtObj.paxInfants.length;i < infantCount;i++){
				var infant = {
						displayInfantFirstName : $("#" + (i + 1) + "_displayInfantFirstName").val(),
						displayInfantLastName : $("#" + (i + 1) + "_displayInfantLastName").val(),
						displayNationalIDNo : "",
						displayVisaDocNumber : "",
						displayVisaDocPlaceOfIssue : "",
						displayVisaDocIssueDate : "",
						displayVisaApplicableCountry : "",
						displayTravelDocType : "",
						displayPnrPaxPlaceOfBirth : "",
						displayPnrPaxCatFOIDPlace : "",
						displayPnrPaxCatFOIDExpiry : "",
						displayPnrPaxCatFOIDNumber : "",
						displayInfantTitle : $("#" + (i + 1) + "_displayInfantTitle").val()
				};
				jsonPaxAdtObj.paxInfants[i] = infant;
			}			
		}
	}


	UI_tabBookingInfo.clearAllAlertsOnClick = function(){
        UI_commonSystem.pageOnChange();
        UI_tabBookingInfo.displayWindowForClearAllAlert(0)
	};

	UI_tabBookingInfo.createAlertOnClick = function(){
		UI_commonSystem.pageOnChange();
		UI_tabBookingInfo.createAlert();
	};
	
	UI_tabBookingInfo.createAlert =  function(){
	    var selectedPnrSegRef = UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef;
	    if(selectedPnrSegRef != null && selectedPnrSegRef != '' && selectedPnrSegRef != 'null'){
	    	
	    	if(UI_tabBookingInfo.doesAlertAlreadyExistForAllSegments(selectedPnrSegRef)){
	    		showERRMessage('An alert already exist for the selected segment. Please clear the existing alert.');
	    		return false;
	    	}
	    	
	        var strUrl = 'showNewFile!loadCreateAlert.action?pnr=' + jsonPNR + '&pnrSegIds=' + UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef;
	        if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	        top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(280, 670));
	    } else {
	        showERRMessage('Please select a segment to generate alert');
	    }

	}
	/**
	 * When booking is served with a connnection fare, we will have two segments
	 * bound together. Hence alert check should be done for all segments
	 */
	UI_tabBookingInfo.doesAlertAlreadyExistForAllSegments = function(pnrSegIDs){
		var alertExistsForAllSegments = true;
		var segIDs = pnrSegIDs.split(",");

		for(i=0;i<segIDs.length;i++) {
			alertExistsForAllSegments = alertExistsForAllSegments && UI_tabBookingInfo.doesAlertAlreadyExist(segIDs[i]);
		}
		return alertExistsForAllSegments;
	}
	
	/*
	 * Checks if the selected segment already have an alert.
	 */
	UI_tabBookingInfo.doesAlertAlreadyExist = function(pnrSegID){
		var alertExistsForSegment = false;
		var segID = pnrSegID.split("$")[0];
		$.each(UI_reservation.jsonAlerts,function(index,value){
			if(segID == value.flightSegmantRefNumber){
				alertExistsForSegment = true;
				
				//Breaking from the loop
				return false;
			}
		});
		
		return alertExistsForSegment;
	}
	
	/*
	 * Set the charge adjustment details after successfull ajax call.
	 */
	UI_tabBookingInfo.loadGroupChargeAdjustment = function(response){
		chargeAdjustments=response.chargeAdjustmentTypes;
		UI_tabBookingInfo.constructPaxListForCA();
		UI_tabBookingInfo.constructSegListForCA();
		if(UI_reservation.isServiceTaxApplied){
			$("#btnGroupChargeConfirm").disable();
		}else{
			$("#btnServiceTaxAmount").hide();
		}
		$("#groupChargeAdjusmentPopUp").dialog('open');
	}
	
	/*
	 * Load charge adjustment types
	 */
	UI_tabBookingInfo.loadGroupChargeData = function(){
		var data = {};
		data['pnr'] = jsonPNR;
		data['groupPNR'] = jsonGroupPNR;
		data['resSegments']  = UI_reservation.jsonSegs;
		UI_commonSystem.getDummyFrm().ajaxSubmit({ 
			url: 'loadPaxAccountDetails!loadGroupAdjustDetails.action',dataType: 'json',type:'post', processData: false, data:data,
			success: UI_tabBookingInfo.loadGroupChargeAdjustment, error:UI_commonSystem.setErrorStatus
		});
		return false;
	}
	
	/*
	 * Create the date view to be displayed in the group refund popup.
	 */
	UI_tabBookingInfo.loadGroupRefundData = function(){
		$("#listTxnIDs").empty();
		
		$.each(UI_reservation.perPaxPaymentDetails,function(key,value){
			var paxDetails=key.split("-");
 				
			for(var i=0;i< value.paymentList.length;i++){		
				var payment = value.paymentList[i];
				var currency = value.currency;
				var credit = 0;
				if(typeof UI_reservation.perPaxCreditDetails[key] != 'undefined' 
					&& !isNaN(UI_reservation.perPaxCreditDetails[key])){
					credit = UI_reservation.perPaxCreditDetails[key];
				}			
				
				credit = Number(credit).toFixed(2);				
				
				var refundableCreditAmt = 0;
				if(value.actualCredit != undefined && value.actualCredit!=null 
						&& !isNaN(value.actualCredit)){
					refundableCreditAmt = value.actualCredit*-1;					
				}
				refundableCreditAmt = Number(refundableCreditAmt).toFixed(2);
				payment.credit = credit + ' ' + currency;
				payment.actualCredit = refundableCreditAmt;
				payment.actualCreditDisplay = refundableCreditAmt + ' ' + currency;
				if(payment.paymentMethod == 'CRED') {
					UI_tabBookingInfo.creditCardPaymentsMap[payment.hdnPayRef] = payment;
				}
				UI_tabBookingInfo.allPayments[payment.hdnPayRef] = payment;
			}
		});
		
		//sort payments
		var sort_array = [];
		for (var key in UI_tabBookingInfo.allPayments) {
		    sort_array.push({key:key,amount:UI_tabBookingInfo.allPayments[key].displayRefundableAmount});
		}	
		
		sort_array.sort(UI_tabBookingInfo.sortPayments);
		for (var i=0;i<sort_array.length;i++) {
		    var displayPayment = UI_tabBookingInfo.allPayments[sort_array[i].key];
		    var pgReference = '';
		    if(displayPayment.paymentBrokerRefNo != null)  {
				pgReference = '/PG-REF:'+displayPayment.paymentBrokerRefNo;
			}	
			$("#listTxnIDs").append('<li class="ui-widget-content ui-selectee" id="'+displayPayment.hdnPayRef+
					'"><table border="0" width="100%"><tr><td style="width:26%;">'+displayPayment.displayPaxName +
					'</td><td style="width:14%;">'+displayPayment.displayRefundableAmount + " " + displayPayment.hdnPaidCurrency +
					'</td><td style="width:31%;">'+displayPayment.displayMethod+''+pgReference+
					'</td><td style="width:12%" align="right">' + displayPayment.actualCreditDisplay +
					'</td><td style="width:12%" align="right">' + displayPayment.credit +
					'</td><td style="width:5%" align="right">'+displayPayment.displayCarrierCode+
					'</td></tr></table></li>');
		}
		
		$("#listTxnIDs").selectable();
		
		$("#groupPaxRefundPopUp").dialog('open');
	}
	
	/*
	 * Create the date view to be displayed in the group pax status update popup.
	 */
	UI_tabBookingInfo.loadGroupPaxStatusUpdate = function(){
		
		UI_tabBookingInfo.fltSegPaxMap = {};
		var segPaxDetailsObj;
		var paxEticket;
		var paxIndex = 1;
		
		// create data model 
		$.each(eval(UI_reservation.jsonPassengers), function(key, passenger) {
		    $.each(passenger.eTickets, function(key, eTicket) {

		        if (!(UI_tabBookingInfo.fltSegPaxMap != undefined && UI_tabBookingInfo.fltSegPaxMap[eTicket.carrierCode + '|' + eTicket.pnrSegId] != undefined)) {

		            // populate segment Pax Details
		            segPaxDetailsObj = {};
		            segPaxDetailsObj.segmentCode = eTicket.segmentCode;
		            segPaxDetailsObj.segmentStatus = eTicket.segmentStatus;
		            segPaxDetailsObj.fltNumber = eTicket.flightNo;
		            segPaxDetailsObj.carrier = eTicket.carrierCode;
		            segPaxDetailsObj.depDate = eTicket.departureDate;
		            segPaxDetailsObj.depDateZulu = eTicket.departureDate;
		            segPaxDetailsObj.pnrSegId = eTicket.pnrSegId;
		            segPaxDetailsObj.paxEticketArr = [];
		            UI_tabBookingInfo.fltSegPaxMap[eTicket.carrierCode + '|' + eTicket.pnrSegId] = segPaxDetailsObj;

		        }

		        segPaxDetailsObj = UI_tabBookingInfo.fltSegPaxMap[eTicket.carrierCode + '|' + eTicket.pnrSegId];

		        paxEticket = {};
		        paxEticket.id = paxIndex;
		        paxEticket.paxType = passenger.paxType;
		        paxEticket.title = passenger.title;
		        paxEticket.firstName = passenger.firstName;
		        paxEticket.lastName = passenger.lastName;
		        paxEticket.coupanNo = eTicket.couponNo;
		        paxEticket.eticketId = eTicket.eticketId;
		        paxEticket.eticketNum = eTicket.paxETicketNo;
		        paxEticket.eticketStatus = eTicket.paxETicketStatus;
		        paxEticket.paxStatus = eTicket.paxStatus;
		        paxEticket.travelerRefNumber = eTicket.travelerRefNumber;

		        segPaxDetailsObj = UI_tabBookingInfo.fltSegPaxMap[eTicket.carrierCode + '|' + eTicket.pnrSegId];
		        segPaxDetailsObj.paxEticketArr.push(paxEticket);
		        paxIndex++;
		    });
		});	
		var strUrl = "showNewFile!loadGroupPaxStatusUpdate.action" ; 
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(600, 850));
		top.objCW.fltSegPaxMap = UI_tabBookingInfo.fltSegPaxMap;
		
	}

	UI_tabBookingInfo.sortPayments = function(a,b) {
		if (Number(a.amount) < Number(b.amount)) {
			return 1;
		}     
		if (Number(a.amount) > Number(b.amount)){
			  return -1;
		}
		return 0;
	}
	
	/*
	 * Do group refund
	 */
	UI_tabBookingInfo.commenceGroupRefund = function(){

		UI_tabBookingInfo.selectedTxnIds=[];
		
		$("#listTxnIDs .ui-selected").each(function() {
			UI_tabBookingInfo.selectedTxnIds.push(this.id);
		});
		
		if(UI_tabBookingInfo.validatePayment()){
		
			var data = {};
			if($("input[name='radPaymentType']:checked").val() == UI_commonSystem.strCCCode ){
				
				var selectedPayment = UI_tabBookingInfo.creditCardPaymentsMap[UI_tabBookingInfo.selectedTxnIds[0]];
				//get credit card info for one of the cc transactions
				data["hdnMode"] = "REFUND";
				data["selRefundAmount"] = $("#txtPayAmount").val();	
				data["referenceNo"] = selectedPayment.hdnPayRef;
				data['pmtdate'] = selectedPayment.displayDate;
				data['groupPNR']  = jsonGroupPNR;
				data['paymentCarrier'] = selectedPayment.displayCarrierCode;
				UI_commonSystem.showProgress();	
				UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:"creditCardPayment.action",							
									success: UI_tabBookingInfo.applyCreditCardSuccess,error:UI_commonSystem.setErrorStatus});				
			} else {
				var refundAmount = $("#txtPayAmount").val();
				var refundMethod = $("input[name='radPaymentType']:checked").val();				
				data['groupPNR']  = jsonGroupPNR;
				data['pnr']  = jsonPNR;
				data['version']  = jsonBkgInfo.version;
				data['resONDs']  = UI_reservation.jsonONDs;
				data["lastUserNote"] = jsonUserNotes;
				data["radPaymentType"] = refundMethod;
				data["txtPayAmount"] = refundAmount;
				data["txtPayUN"] = $("#txtPayUN").val();
				data["selAgent"] = $("#selAgent").val();
				data['ownerAgentCode'] = UI_reservation.ownerAgent;
				data["payRef"] = $.toJSON(UI_tabBookingInfo.selectedTxnIds);
				
				$("#groupPaxRefundPopUp").dialog('close');
				UI_commonSystem.showProgress();	
				UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:"groupPaxRefund.action",							
						success: groupRefundSuccessfull,error:UI_commonSystem.setErrorStatus});
			}
		
		}
	}
	
	UI_tabBookingInfo.applyCreditCardSuccess = function(response){			
		if (response != null){
			UI_commonSystem.hideProgress();
			if(response.success){
				$("#groupPayOptions").hide();
				$("#groupPayOptButtons").hide();
				if(ccPayData["isFullRefundOperation"] == "true"){	
					$("#creditCardDet").hide();
				}else{
				$("#groupCreditCardDet").show();
				}
			
				$("#divDisableLayer").show();
				
				$("#selCardType").disable();
				$("#divGroupTxtCurr").disable();
				$("#groupCardNo").disable();
				$("#groupCardHoldersName").disable();
				$("#groupExpiryDate").disable();				
				$("#groupCardCVV").disable();
				$("#groupCardType").children().remove();
				var arr = top.mapCCTypeForPgw[response.hdnIPGID];
				arr = (arr==null?[]:arr);
				$("#groupCardType").fillDropDown( { dataArray: arr, keyIndex:0 , valueIndex:1, firstEmpty:true});				


				var selectedFirstCCTxn = UI_tabBookingInfo.creditCardPaymentsMap[UI_tabBookingInfo.selectedTxnIds[0]];
				ccPayData["hdnIPGId"] = response.hdnIPGID;
				ccPayData["cardNo"] = response.cardNumber;
				ccPayData["selAuthId"] = selectedFirstCCTxn.hdnAuthorizationId;
				ccPayData["tempPayId"] = response.tempPayId;
				ccPayData["selCardType"] = response.cardType;
				ccPayData["hdnSelCurrency"] = response.currencyInfo.currencyCode;						
				ccPayData["cardHoldersName"] = response.cardHolderName;				
				ccPayData["hdnMode"] = "REFUND";
				ccPayData["payRef"] = $.toJSON(UI_tabBookingInfo.selectedTxnIds);
				ccPayData["resContactInfo"] = UI_reservation.contactInfo;
				ccPayData['version']  = jsonBkgInfo.version;
				ccPayData['cccdId'] = response.ccdID;
				ccPayData['pnr']  = jsonPNR;
				ccPayData['resPaxs']  = UI_reservation.jsonPassengers;
				ccPayData['groupPNR']  = jsonGroupPNR;
				ccPayData['pmtdate'] = selectedFirstCCTxn.displayDate;
				ccPayData['ownerAgentCode'] = UI_reservation.ownerAgent;
				ccPayData["radPaymentType"] = $("input[name='radPaymentType']:checked").val();
				ccPayData["txtPayAmount"] = $("#txtPayAmount").val();;
				ccPayData["txtPayUN"] = $("#txtPayUN").val();
				ccPayData['paymentCarrier'] = selectedFirstCCTxn.displayCarrierCode;
				ccPayData['resONDs']  = UI_reservation.jsonONDs;
				ccPayData['isFullRefundOperation'] = response.isFullRefundOperation;
				
				$("#groupCardNo").val(response.cardNumber);
				$("#groupCardHoldersName").val(response.cardHolderName);
				$("#groupExpiryDate").val(response.cardExpiryDate);
				$("#groupCardCVV").val(response.cardCVV);
				$("#groupCardType").val(response.cardType);
				carrierCode = response.carrierCode;					
				response.strCardErrors;
				
				// disabled card type since it is not possible to refund to other cards.
				$("#groupCardType").disable();
					
				if(!response.showCardDetails){
					$("#trGroupCardEDate").hide();
					$("#trGroupCVV").hide();					
				}
				
				$("#groupCardType").focus();
				if(response.currencyCode != null && response.currencyCode != '' && response.currencyCode != response.currencyInfo.currencyCode )
					$("#divGroupTotPay").html(response.totalPayAmountWithccCharges + " " + response.currencyCode+"(Approx. Payment Amount <font color='red'>["+ response.totalPayAmountWithccChargesSelcur +" "+ response.currencyInfo.currencyCode + " ]</font>");
				else
					$("#divGroupTotPay").html(response.totalPayAmountWithccCharges + " " + response.currencyInfo.currencyCode);
				
				$("#divGroupTxtCurr").html(" "+ response.currencyInfo.currencyCode);
				
			} else{
				showERRMessage(response.messageTxt);
			}					
		}	
	}
	
	

	UI_tabBookingInfo.getMaxRefundableAmount = function() {

		var maxRefundableAmount = 0;
		for(i=0;i<UI_tabBookingInfo.selectedTxnIds.length;i++){

			if (UI_tabBookingInfo.selectedTxnIds[i] != ""){
				if((i==0) || (Number(UI_tabBookingInfo.allPayments[UI_tabBookingInfo.selectedTxnIds[i]].displayRefundableAmount) < maxRefundableAmount)) {
					maxRefundableAmount = Number(UI_tabBookingInfo.allPayments[UI_tabBookingInfo.selectedTxnIds[i]].displayRefundableAmount);
				}
			}
		}
		return maxRefundableAmount;
	}
	
	UI_tabBookingInfo.getActualCreditAmount = function() {

		var actualCredit = 0;
		for(i=0;i<UI_tabBookingInfo.selectedTxnIds.length;i++){

			if (UI_tabBookingInfo.selectedTxnIds[i] != ""){
				if((i==0) || (Number(UI_tabBookingInfo.allPayments[UI_tabBookingInfo.selectedTxnIds[i]].actualCredit) < actualCredit)) {
					actualCredit = Number(UI_tabBookingInfo.allPayments[UI_tabBookingInfo.selectedTxnIds[i]].actualCredit);
				}
			}
		}
		return actualCredit;
	}
	
	/*
	 * Create the date view to be displayed in the Name Change.
	 */
	UI_tabBookingInfo.loadNameChangeData = function(reset){
		if (reset){
            $("#p_tblPaxAdtName").empty().append('<table id="tblPaxAdtName" cellpadding="0" cellspacing="0"></table>');
            $("#p_tblPaxInfName").empty().append('<table id="tblPaxInfName" cellpadding="0" cellspacing="0"></table>');
        }
		UI_tabBookingInfo.constructAdultNameGrid({id:"#tblPaxAdtName"});
		UI_tabBookingInfo.constructInfNameGrid({id:"#tblPaxInfName"});


		UI_commonSystem.fillGridData({id:"#tblPaxAdtName", data:jsonPaxAdtObj.paxAdults, editable:true});
		UI_commonSystem.fillGridData({id:"#tblPaxInfName", data:jsonPaxAdtObj.paxInfants, editable:true});
		var i = 0;
		for (i = 1; jsonPaxAdtObj.paxAdults.length>= i; i++) {
			if (jsonPaxAdtObj.paxAdults.length > 0 && jsonPaxAdtObj.paxAdults[i-1].nameEditable){			
				$("#"+i+"_display-AdultFirstName").closest("tr").find("input, select").enable();
			}
		}
		for (i = 1; jsonPaxAdtObj.paxInfants.length>= i; i++) {
			if (jsonPaxAdtObj.paxInfants.length > 0 && jsonPaxAdtObj.paxInfants[i-1].nameEditable){
				$("#"+i+"_display-InfantFirstName").closest("tr").find("input, select").enable();
			}
		}
        //cloning PaxArrays and adding chnaged flage as false
        UI_tabBookingInfo.tempPaxAdults = $.extend(true, {}, jsonPaxAdtObj.paxAdults);
        UI_tabBookingInfo.tempPaxInfants = $.extend(true, {}, jsonPaxAdtObj.paxInfants);
        $.each(UI_tabBookingInfo.tempPaxAdults, function(i, obj){
            obj.changed = false;
        });
        $.each(UI_tabBookingInfo.tempPaxInfants, function(i, obj){
            obj.changed = false;
        });
        if (!reset){
            $("#paxNameChangePopUp").dialog('open');
        }
	}
	
	UI_tabBookingInfo.commenceNameChange = function(isRequote){
		if(isDuplicateFFIDExist){
			showERRMessage("Air Rewards ID cannot be same");
			return false;
		}	
		
		if (DATA_ResPro.initialParams.duplicateNameCheckEnabled) { 			
			if (UI_tabBookingInfo.isDuplicateExistsNew()) {
				if(DATA_ResPro.initialParams.duplicateNameSkipModifyPrivilege){
					if(!confirm(raiseError("XBE-ERR-80"))){						
					return;
					}
				} else {
					showERRMessage(raiseError("XBE-ERR-79"));
					return;
				}
			}
		}

        var checkNameChangeApplied = function(list){
            var booleanNameChanged = false;
            if (list.adultNames.length>0 || list.infantNames.length>0){
                var booleanNameChanged = true;
            }
            return booleanNameChanged;
        }
	
		var isRemainUnchengedNames = function(list){
			total        =	jsonPaxAdtObj.paxAdults.length + jsonPaxAdtObj.paxInfants.length;
			totalChanged =	list.adultNames.length + list.infantNames.length;
			var booleanexistsUnchegd = true;
			if(total == totalChanged){
				booleanexistsUnchegd = false;
			}
			return booleanexistsUnchegd;	
		}
		var paxNameDetails = UI_tabBookingInfo.buildPaxNameDetails();

        if (checkNameChangeApplied(paxNameDetails)){
	        if(UI_tabBookingInfo.validateNameChangeInfo(paxNameDetails)){
	//---
	        	if (checkTBANameChangedOnly(paxNameDetails) && isRequote){
			  
					if (isRemainUnchengedNames(paxNameDetails)){	
						if(!confirm(raiseError("XBE-CONF-07")) ){
							UI_tabBookingInfo.closeNameChangeDialog(true);						
							return false;
						}
					}//---	
					UI_tabBookingInfo.addSegmentOnClick({blnIsGroundStationAllowed:false, blnIsFromNameChange:true, paxNameDetails : paxNameDetails});
	        	} else {
	        		var data = {}
	        		data['pnr']  = jsonPNR;
	                data['version'] = jsonBkgInfo.version;
	                data["paxNameDetails"] = $.toJSON(paxNameDetails);
	                data["groupPNR"] = jsonBkgInfo.groupPNR;
	
	                    //$("#groupPaxRefundPopUp").dialog('close');
	                UI_commonSystem.showProgress();
	                UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:"updatePaxNames.action",
	                	success: function(response){
	                            //showConfirmation("Name Change updated successful.");
	                		UI_tabBookingInfo.modifyCNTSuccess(response);
	                		$("#btnNameChangeConfirm").unbind();
	                		$("#btnNameCngOvrNo").unbind();	        				
	        				$("#btnNameCngOvrOk").unbind();
	        				$("#btnNameChange").unbind();
	        				$("#btnNameChangeClose").unbind();
	        				$("#btnNameChangeReset").unbind();
	                		UI_tabBookingInfo.allowNameChange();
	                        UI_commonSystem.hideProgress();
	                	}, error:UI_commonSystem.setErrorStatus
	                });
	        	}
	        }            
        }else{
            showERRMessage("Nothing to update");
        }

	}
	
	UI_tabBookingInfo.showNameChangeRequotePopup = function(){
		
		var paxNameDetails = UI_tabBookingInfo.buildPaxNameDetails();
		var isTBANameChangeOnly = checkTBANameChangedOnly(paxNameDetails);
		
		if (!isTBANameChangeOnly || jsonBkgInfo.status == "OHD") {
			UI_tabBookingInfo.commenceNameChange(isTBANameChangeOnly);
			return false;
		} 
		$("#divNameCngOvr").show();
		
    }
	
	UI_tabBookingInfo.closeNameChangeRequotePopup = function(){
		$("#divNameCngOvr").hide();
		UI_commonSystem.hideProgress();
	}
	UI_tabBookingInfo.resetNameChangeData = function(){
        UI_tabBookingInfo.loadNameChangeData(true);
	}
	
	UI_tabBookingInfo.closeNameChangeDialog = function(isCloseButton){
		if(isCloseButton) {
			$("#paxNameChangePopUp").dialog('close');
		}
	}


	UI_tabBookingInfo.validatePayment = function(){ 
			
			if(UI_tabBookingInfo.selectedTxnIds == null || UI_tabBookingInfo.selectedTxnIds.length == 0) {
				showERRMessage("Please select transactions to refund");
				return false;
			}
				

			if ($("#txtPayAmount").val() == ""){
					showERRMessage(raiseError("XBE-ERR-01","Amount"));
					$("#txtPayAmount").focus();
					return false;
			}

			if (isNaN($("#txtPayAmount").val())){
					showERRMessage(raiseError("XBE-ERR-04","Amount"));
					$("#txtPayAmount").focus();
					return false;
			}

			var maxRefundableAmount = UI_tabBookingInfo.getMaxRefundableAmount();
			if (Number($("#txtPayAmount").val()) > maxRefundableAmount){
					showERRMessage("Entered refund amount exceeds maximum refundable amount of a selected transaction!!!");
					$("#txtPayAmount").focus();
					return false;
			}

			
			var actualCreditAmount = UI_tabBookingInfo.getActualCreditAmount();
			if (Number($("#txtPayAmount").val()) > actualCreditAmount){				
					showERRMessage("Entered refund amount exceeds actual refundable amount!!!");
					$("#txtPayAmount").focus();
					return false;
			}		

			if ($("#txtPayUN").val() == ""){
					showERRMessage(raiseError("XBE-ERR-01","User note"));
					$("#txtPayUN").focus();
					return false;
			}

			if($("#txtPayUN").val().length > 255){
				showERRMessage(raiseError("XBE-ERR-18","User note"));
				return false;
			}	
			
			switch ($("input[name='radPaymentType']:checked").val()){
			case UI_commonSystem.strAccCode :
				
				if(!UI_tabBookingInfo.isSelectedRefundAllowed(UI_commonSystem.strAccCode)){
					showERRMessage("On Account Refund Is Allowed Only For On Account Payments.");
					return false;
				}else if ($("#selAgent").val() == ""){
					showERRMessage(raiseError("XBE-ERR-01", "Agent"));
					$("#selAgent").focus();
					return false;
				}
				break;

			case UI_commonSystem.strCCCode :

				var paymentBrokerRefno = null;
				for(var i=0;i<UI_tabBookingInfo.selectedTxnIds.length;i++) {

					var tempCCTnx = UI_tabBookingInfo.creditCardPaymentsMap[UI_tabBookingInfo.selectedTxnIds[i]];

					if(tempCCTnx != null || typeof(tempCCTnx) != 'undefined') {

						if(paymentBrokerRefno == null ) {
							paymentBrokerRefno = tempCCTnx.paymentBrokerRefNo;
						} else if(paymentBrokerRefno == tempCCTnx.paymentBrokerRefNo){
							continue;
						} else {
							showERRMessage("Please select Credit Card payments with the same PG Reference for refund.");
							return false;
						}
					} else {
						showERRMessage("Only credit card payments can be refunded as credit card refunds.");
						return false;
					}

				}				  
				break;
			case UI_commonSystem.strBspCode :
				
				if(!UI_tabBookingInfo.isSelectedRefundAllowed(UI_commonSystem.strBspCode)){
					showERRMessage("BSP Refund Is Allowed Only For BSP Payments.");
					return false;
				}
				break;
			
			case UI_commonSystem.strCashCode :
				
				if(!UI_tabBookingInfo.isSelectedRefundAllowed(UI_commonSystem.strCashCode)){
					showERRMessage("Cash Refund Is Allowed Only For Cash Payments.");
					return false;
				}
				break;
				
			default:
				break;
			}

			return true;
	}
	
	UI_tabBookingInfo.isSelectedRefundAllowed = function(strSelectedRefundMethod){
		
		var isallowed =false;
		
			for(var key in UI_tabBookingInfo.selectedTxnIds) {
			
				var paymentRef = UI_tabBookingInfo.selectedTxnIds[key];
				var payment = UI_tabBookingInfo.allPayments[paymentRef];
					
				if(payment != null){
					// Only if all selected payments are same as selected refund we allow refund
					if(strSelectedRefundMethod == payment.paymentMethod){
						isallowed =true;
					}else{
						return false;
					}
				}
			}
		
		return isallowed;
	}
	
	
	/*
	 * Group reufnd ajax call's call back method
	 */
	function groupRefundSuccessfull(response){
		UI_commonSystem.hideProgress();
		if(response.success){
			showConfirmation("Group refund successful.");
			UI_tabBookingInfo.groupPayCCCancelOnClick();
			UI_tabBookingInfo.resetGroupRefundData();
			//Refresh reservation data.
			UI_reservation.loadReservation("", null, false);
		}else{
			showERRMessage(response.messageTxt);
		}
	}
	
	/*
	 * Reset the popup data
	 */
	UI_tabBookingInfo.resetGroupRefundData = function(){
		$("#txtPayAmount").val("");
		$("#txtPayUN").val("");
		$("#listTxnIDs .ui-selected").removeClass('ui-selected');
	}

	
	UI_tabBookingInfo.groupPayCCConfirmOnClick = function(){
		$("#groupPaxRefundPopUp").dialog('close');
		UI_commonSystem.showProgress();		
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:ccPayData, url:"groupPaxRefund.action",							
			success: groupRefundSuccessfull,error:UI_commonSystem.setErrorStatus});
		
	}
	
	UI_tabBookingInfo.groupPayCCCancelOnClick = function(){
		$("#groupCardType").val("");
		$("#groupCardNo").val("");
		$("#groupCardHoldersName").val("");
		$("#groupExpiryDate").val("");
		$("#groupCardCVV").val("");
		$("#groupCreditCardDet").hide();
		$("#groupPayOptions").show();
		$("#groupPayOptButtons").show();	
	}
	
	UI_tabBookingInfo.closeGroupRefundDialog = function(isCloseButton){
		UI_tabBookingInfo.resetGroupRefundData();
		UI_tabBookingInfo.groupPayCCCancelOnClick();
		if(isCloseButton) {
			$("#groupPaxRefundPopUp").dialog('close');
		}
	}
	
	
	/*
	 * Saves the charge adjustments
	 */
	UI_tabBookingInfo.commenceGroupChargeAdjustment = function(){
		if(UI_tabBookingInfo.validateGroupChargeAdjustment()){
			var selPassengers="";
			var selONDs="";
			for(var i=0;i<selectedONDs.length;i++){
				if(i==0){
					selONDs=selectedONDs[i];
				}
				else{
					selONDs+=','+selectedONDs[i];
				}
			}
			for(var i=0;i<selectedPax.length;i++){
				if(i==0){
					selPassengers=selectedPax[i];
				}
				else{
					selPassengers+=','+selectedPax[i];
				}
			}
			var data = {};
			data['groupPNR']  = jsonGroupPNR;
			data['pnr']  = jsonPNR;
			data['version']  = jsonBkgInfo.version;
			data['resONDs']  = UI_reservation.jsonONDs;			
			data['resSegments']  = UI_reservation.jsonSegs;
			data['status']  = jsonBkgInfo.status;
			data['selAirlineOnd']=selONDs;
			data['selPax']=selPassengers;
			data['selChargeType']=$('#selChargeType').is(':checked');
			data['selAdjustmentType']=$('#selAdjustmentType').val();
			data['txtAdjustment']=$('#txtAdjustment').val();
			data['txtUN']=$('#txtUN').val();
			data['isInfantPaymentSeparated'] = UI_reservation.isInfantPaymentSeparated;
			$("#frmPaxAccount").action("paxAccountChargesConfirm.action");
			$("#groupChargeAdjusmentPopUp").dialog('close');
			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url: 'paxAccountChargesConfirm.action', dataType: 'json', processData: false, data:data,
											success: groupAdjustmentSuccessfull, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	/*
	 * Method dealing with group charge adjusting ajax request completing successfully
	 */
	groupAdjustmentSuccessfull = function(response){
		UI_commonSystem.hideProgress();
		$("#amountWithSerivceTax").text('');
		$("#groupChargeAdjusmentPopUp").dialog('open');
		if(response.success){
			showConfirmation("Charges successfully adjusted.");
			jsonBkgInfo.version=response.version;
			UI_tabBookingInfo.resetGroupChargeAdjustment();
			
			//Rfreshe reservation data.
			UI_reservation.loadReservation("", null, false);
		}else{
			showERRMessage(response.messageTxt);
		}
	}
	
	/*
	 * Input validation for group charge adjustment
	 */
	UI_tabBookingInfo.validateGroupChargeAdjustment = function(){
		UI_commonSystem.initializeMessage();
		
		if(selectedONDs.length < 1 || selectedPax.length < 1){
			showERRMessage("Please select at least one segment and one passenger.");
			return false;
		}
		
		if(Number($("#txtAdjustment").val()) < 1) {
			
			if(!DATA_ResPro.initialParams.refundableChargeAdjust){
				showERRMessage("Please enter the charge adjustment amount.");
				return false;
			}
		}
		
		$("#txtAdjustment").val($.trim($("#txtAdjustment").val()));
		
		if(isNaN(Number($("#txtAdjustment").val()))){
			showERRMessage("Adjustment amount must be a valid number");
			return false;
		}
		
		if(Number($("#txtAdjustment").val()) == 0){
			showERRMessage("Adjustment amount must be a number and greater than 0");
			return false;
		}	
		
		// Validate UserNotes Field
		if ($.trim($("#txtUN").val()).length < 1){
			showERRMessage("User Notes cannot be blank.");
			$("#txtUN").focus();
			return false;
		}else if ($.trim($("#txtUN").val()).length > 250) {
			showERRMessage("User Notes must be less than 250 letters.");
			$("#txtUN").focus();
			return false;
		}	

		var carrier=null;
		var passedCareerValidation=true;
		$("#listSegments .ui-selected").each(function() {
			var curCarrier=$(this).text().split('-')[0].trim();
			if(carrier==null){
				carrier=curCarrier;
			}
			if(carrier!=curCarrier){
				showERRMessage("You cannot select segments belonging to two carriers in one go");
				passedCareerValidation = false;
			}
			
		});
		
		// Validate when minus charge adjustment requested PAX has no balance to pay  
		/*if(Number($("#txtAdjustment").val()) < 0){
			var availableBalance = 0;
			var balanceToPay = false;
			for(var i=0;i<selectedPax.length;i++){
				var selectedPaxRef = selectedPax[i];
				var paxArray = $.parseJSON(UI_reservation.jsonPassengers);
				$.each(paxArray, function(key, val){
					if(paxArray[key].travelerRefNumber == selectedPaxRef){
						availableBalance = Number(paxArray[key].totalAvailableBalance);
						if(availableBalance > 0){							
							balanceToPay = true;
						}
						
					}
				});
			}	
			
			if(balanceToPay){
				showERRMessage("Sorry Minus Charge Adjustment not allowed, Identified Balance to pay");				
				$("#txtAdjustment").focus();				
				return false;	
			}
		}*/
		
		return passedCareerValidation;
	}
	
	UI_tabBookingInfo.resetGroupChargeAdjustment = function(){
		$("#listSegments .ui-selected").removeClass('ui-selected');
		$("#listPassengers .ui-selected").removeClass('ui-selected');
		$("#txtAdjustment").val("");
		$("#txtUN").val("");
		$("#selAdjustmentType").empty();
		$("#selChargeType").removeAttr("checked");
		selectedONDs=[];
		selectedPax=[];
	}
	
	/*
	 * Show Pax Details
	 */
	UI_tabBookingInfo.showPaxDetails = function(){
		var strUrl = 'showNewFile!loadPaxDetails.action?mode=MODIFY&isGdsPnr=' + isGdsPnr +'&isDomesticFlightExist=' + UI_tabBookingInfo.isDomesticFlightExist();
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(265, 625));
	}
	
	/*
	 * Show international flight details capturing window
	 * */
	UI_tabBookingInfo.internationalFlightDetails = function(){
		var strUrl = 'showNewFile!loadExternalInternationalFlightDetails.action?mode=MODIFY';
	
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(300, 800, true));
	}
	
	/*
	 * Bookmark On Click
	 */
	UI_tabBookingInfo.bookMarkOnClick = function(){
		UI_commonSystem.pageOnChange();
		if(jsonGroupPNR != null && jsonGroupPNR != ""){
			// top[0].addLCCPNRHistory(jsonGroupPNR);
			var jsContactInfo =$.parseJSON(UI_reservation.contactInfo);
			top[0].addLCCPNRHistory(jsonGroupPNR, jsContactInfo.firstName);
		}else {
			// Fix this
			var jsContactInfo =$.parseJSON(UI_reservation.contactInfo);
			top[0].addPNRHistoryNew(jsonPNR, jsContactInfo.firstName);
		}

		top[0].showPNRIcon();
		if (top.strPageFrom != ""){
			if (top.strSearchCriteria != "" ) {
				top[1].location.replace(top.strPageReturn + "?hdnAlertInfo=" + top.strSearchCriteria);
			}
			else {
				top[1].location.replace(top.strPageReturn);
			}
		}else{
			parent.UI_searchReservation.writePNR();
			parent.UI_searchReservation.ViewSearch();
			// UI_tabBookingInfo.closeOnclick();
		}
		
	}
	
	/*
	 * View Invoice On Click
	 */
	var chkBoxesArrayForPrintInvoice = new Array();
	UI_tabBookingInfo.viewInvoiceOnClick = function(){
		$('#trInvoiceListTemp').parent().find('tr').filter(function() {			
			var regex = new RegExp('iii_*');
	        return regex.test(this.id);
	    }).remove();
		var chkBoxesArray =new Array();
		   
		   $('#divInvoicesDisplay').show();
		   var firstElement = null;
		   var invoiceObj = UI_reservation.taxInvoices;
		   var invoiceSeq = 0;
		   for(var i=0; i < invoiceObj.length; i++){
				
				var tempId = 'iii_'+invoiceObj[i].taxInvoiceId;
				var chkId = 'chkInvoice_'+invoiceObj[i].taxInvoiceId;
				
				var clone = $('#trInvoiceListTemp').clone().show();
				clone.attr('id',tempId);
				clone.appendTo($('#trInvoiceListTemp').parent());  
				
				var chBox = $('#'+tempId+' input');
				chBox.attr('id',chkId);
				chBox.attr('tabIndex',9000+i);
				chBox.val(invoiceObj[i].taxInvoiceId);			
				chBox.attr('style','display:show');			
				chBox.click(function(){

				});
				
				chkBoxesArray[invoiceSeq] = chBox;
				chkBoxesArrayForPrintInvoice[invoiceSeq] = chBox;
				invoiceSeq++;
				
				if(firstElement==null){
					firstElement = chkId;
					chBox.attr('checked',true);  
				}
				
				var label = $('#'+tempId+' label');
				label.attr('id','lblInvoice_'+invoiceObj[i].taxInvoiceId);
				label.text(invoiceObj[i].taxInvoiceNumber);
				label.click(function(e){ var a = e.target.id.split('_'); $('#chkInvoice_'+a[1]).click()});				
		   }
	}
	
	UI_tabBookingInfo.printTaxInvoiceOnClick = function(){
		var atLeastOneCheckBoxSelected = false;
		var chkBoxesArray =new Array();
		var selectedInvoiceIds = "";
		chkBoxesArray = chkBoxesArrayForPrintInvoice;
		for (var i=0;i<chkBoxesArray.length;i++){
			if (chkBoxesArray[i][0].checked){
				selectedInvoiceIds +=  chkBoxesArray[i][0].value + ",";
				atLeastOneCheckBoxSelected = true;
			}
		}
		UI_commonSystem.pageOnChange();
		
		var strUrl = "printTaxInvoice.action"
				   + "?hdnPNRNo=" + jsonBkgInfo.PNR
				   + "&groupPNR=" + UI_tabBookingInfo.isInterlineDryRes(jsonGroupPNR)
				   + "&selectedTaxInvoices=" + selectedInvoiceIds
				   + "&invoiceLanguage=" + "";

		if (atLeastOneCheckBoxSelected){
			$('#divInvoicesDisplay').hide();
			top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
			$('#popup-content').clone().appendTo( top.objCW.document.body );
			top.objCW.print();
		} else {
			alert("Please select atleast one check box.")
		}
		
	}
	
	/*
	 * Reset on Click
	 */
	UI_tabBookingInfo.resetOnClick = function(){
		UI_commonSystem.pageOnChange();
		UI_tabBookingInfo.onLoadCall();		
	}
	
	/*
	 * Edit On Click
	 */
	UI_tabBookingInfo.editOnClick = function(){

		if($("#btnSaveInfants").isEnable()){
			var currMaxRow = $("#tblPaxInf").getGridParam("records");		 
			if(currMaxRow >1){
				$('#tblPaxInf tr:last').remove();
				$("#btnSaveInfants").disable();
				$("#btnSaveInfants").hide();
				$("#btnAddInfants").show();

			}
		}else{
			UI_commonSystem.pageOnChange();
			var enableControls = true;
			$("#txtUNotes").val("");
			
			//$("#selBookingCategory").enable();
			$("#selItineraryFareMask").enable();
			$("#btnPaxDetails").enable();
			$("#selItineraryFareMask").enable();
			$("#btnPaxDetails").enable();
			$("#btnPaxNames").enable();
			$("#btnCreateAlert").enable();
			$("#btnClearAllAlerts").enable();
			$("#btnInternationalFlight").enable();
			$("#btnAdvPaxSel").enable();


			var adultCounter = function(arr){
				var adultCount = 0;
				for(var i = 0; i < arr.length; i++){
					if(arr[i].displayAdultType == 'AD'){
						adultCount ++;
					}
				}
				return adultCount;
			}			
			var adultCount = adultCounter(jsonPaxAdtObj.paxAdults);
			// Enable / Disable AddInfant Button
				if(DATA_ResPro.initialParams.cancelledReservation){
					if(DATA_ResPro.initialParams.modifyCnxResAllowed && isModifyResEnabled){
						enableControls = false;
						$("#btnAddInfants").disable();					
					}
					
				} else if(DATA_ResPro.initialParams.postDepature){// If the
																	// reservation
																	// expired.
//					if(DATA_ResPro.initialParams.modifyFlown){
						enableControls = false;
						if(adultCount > jsonPaxAdtObj.paxInfants.length){
							$("#btnAddInfants").enable();
						} else{
							$("#btnAddInfants").disable();
						}
//					}else{
//						$("#btnAddInfants").disable();
//					}
				} else{ // Reservation is not expired
					if(adultCount > jsonPaxAdtObj.paxInfants.length){
						$("#btnAddInfants").enable();
					}else {
						$("#btnAddInfants").disable();
					}
					
					enableControls = false;

				}
				UI_tabBookingInfo.disableEnableControls(enableControls, true, isCsPnr);
				UI_tabBookingInfo.disablePaxNationality(false);
				if(enableControls){
					$("#btnReset").disable();
					if(!DATA_ResPro.initialParams.addUserNotes){
						$("#btnEdit").show();
						$("#btnSave").hide();
						$("#btnSave").disable();
					}else {
						$("#btnEdit").hide();
						$("#btnSave").show();
						$("#btnSave").enable();
					}				
					$("#btnSplit").enable();
					$("#btnRemovePax").disable();
				}else {
					$("#btnReset").enable();
					$("#btnEdit").hide();
					$("#btnSave").show();
					$("#btnSave").enable();
					$("#btnSplit").enable();
					$("#btnRemovePax").enable();
					
				}
				if(DATA_ResPro.initialParams.cancelledReservation){
					$("#btnRemovePax").disable();
					$("#btnSplit").disable();
				}
				if(!DATA_ResPro.initialParams.removePassenger){
					$("#btnRemovePax").disable();
				}			

				
				// allowing pax name change and user note change for departed segments
				if(DATA_ResPro.initialParams.postDepature){
					
					if(DATA_ResPro.initialParams.extendedNameChangeEnabled ){    //Name change privilege for departed segments needs to be checked only if AIRLINE_187 is enabled.
						
						if(DATA_ResPro.initialParams.allowPassengerNameModificationHavingDepatedSegments
								&& DATA_ResPro.initialParams.nameChange){
							// make names editable
							UI_tabBookingInfo.allowPaxNameChanges(false,false);
						}else{
							UI_tabBookingInfo.allowPaxNameChanges(true,false);
						}
						
					} else {
						if(DATA_ResPro.initialParams.nameChange){
							// make names editable
							UI_tabBookingInfo.allowPaxNameChanges(false,false);
						}else{
							UI_tabBookingInfo.allowPaxNameChanges(true,false);
						}
					}
					
					if(DATA_ResPro.initialParams.allowPassengerUserNoteModificationHavingDepartedSegments
							&& DATA_ResPro.initialParams.addUserNotes){						
						$("#txtUNotes").removeAttr('disabled');
					}else{
						$("#txtUNotes").attr('disabled','disabled');
					}
					
					//Disable Airwards_ID for post departured flights 
					UI_tabBookingInfo.allowPaxFFIDChanges(true);
					
				}else{
					/** Checking whether user has the privilege to edit user notes */
					if(!DATA_ResPro.initialParams.addUserNotes){
						$("#txtUNotes").attr('disabled','disabled');
					} else{					
						$("#txtUNotes").removeAttr('disabled');					
					}	
				}
				
				$("#1_displayAdultTitle").focus();
				
				//Disable first and last name editing due to the new requote name change CR.
				if(DATA_ResPro.initialParams.enableFareRuleNCC){					
					UI_tabBookingInfo.allowPaxNameChanges(true,false);					
				}
				if(UI_reservation.isAllowNameChange){
					UI_tabBookingInfo.allowPaxNameChanges(true,true);
				}
		}
		//if allowAddUserNotes privilege false then user note text and classification should be disable
		if(!DATA_ResPro.initialParams.allowAddUserNotes){
			$("#txtUNotes").attr('disabled','disabled');
			$("#selClassification").hide();
		} else{
			 if((DATA_ResPro.initialParams.allowClassifyUserNotes) && (DATA_ResPro.initialParams.allowAddUserNotes)){
				$("#selClassification").show();
				$("#selClassification").val("PUB");
				$("#selClassification").enable();
			}
		    $("#txtUNotes").removeAttr('disabled');
		}
		if(!DATA_ResPro.initialParams.postDepature && top.arrPrivi[PRIVI_MODIFY_CALL_ORIGIN_COUNTRY] == 1) {
			$("#selOriginCountryOfCall").enable();
		}		
		
	}
	
	// enable all the buttons
	UI_tabBookingInfo.enableAllButtons = function(cond) {
		UI_tabBookingInfo.resButtonEnable(cond);
		UI_tabBookingInfo.mailButtonEnable(cond);
		
		// pax and segment components are disabled for void reservations
		if(DATA_ResPro.initialParams.voidReservation){
			UI_tabBookingInfo.paxButtonEnable(false);
			UI_tabBookingInfo.segButtonEnable(false);
		}else{
		UI_tabBookingInfo.paxButtonEnable(cond);
		UI_tabBookingInfo.segButtonEnable(cond);
	}

	}

	UI_tabBookingInfo.isDuplicateExists = function(){
		var data = {};
		UI_tabBookingInfo.buildPaxAdults(data);
		data["pnr"] = jsonBkgInfo.PNR;
		data["resSegments"] = UI_reservation.jsonSegs;
		data["system"] = jsonSystem;
		var hasDuplicates = false;
		
		UI_commonSystem.showProgress();
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, async:false, 
					url:"duplicateNameCheckForModify.action",							
					success: function(response) {
						if(response != null && response.success){
							hasDuplicates = response.hasDuplicates;
						}
					}, 
					error : UI_commonSystem.setErrorStatus});
		
		UI_commonSystem.hideProgress();
		return hasDuplicates;
		
	}
	
	UI_tabBookingInfo.isDuplicateExistsNew = function(){
		var data = {};
		UI_tabBookingInfo.buildPaxAdultsNew(data);
		data["pnr"] = jsonBkgInfo.PNR;
		data["resSegments"] = UI_reservation.jsonSegs;
		data["system"] = jsonSystem;
		var hasDuplicates = false;
		
		UI_commonSystem.showProgress();
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, async:false, 
					url:"duplicateNameCheckForModify.action",							
					success: function(response) {
						if(response != null && response.success){
							hasDuplicates = response.hasDuplicates;
						}
					}, 
					error : UI_commonSystem.setErrorStatus});
		
		UI_commonSystem.hideProgress();
		return hasDuplicates;
		
	}

	/*
	 * Save On Click
	 */
	UI_tabBookingInfo.saveOnClick = function(){
		$("#btnPaxNames").disable();
		UI_commonSystem.pageOnChange();
		if(!UI_tabBookingInfo.validate()) {
			// Validation not ok, return to page
			return;
		}
		if (jsonBkgInfo.bookingCategory != $("#selBookingCategory").val() ){
			if(!confirm("You have changed booking category,Are you sure you want to change the booking category ? "))
			{
				return;
			}
		}
		
		if (DATA_ResPro.initialParams.duplicateNameCheckEnabled) { // check
																	// whether
																	// duplicate
																	// check is
																	// enabled
			if (UI_tabBookingInfo.isDuplicateExists()) {
				if(DATA_ResPro.initialParams.duplicateNameSkipModifyPrivilege){
					if(!confirm(raiseError("XBE-ERR-80"))){						
						return;
					}
				} else {
					showERRMessage(raiseError("XBE-ERR-79"));
					return;
				}
			}
		}
		
		UI_tabBookingInfo.disableEnableControls(true, false, false);
		UI_tabBookingInfo.disablePaxNationality(true);

		var data = {};
		
		// PNR
		data['groupPNR'] = jsonGroupPNR;
		data['pnr'] = jsonPNR;
		$("#version").val(jsonBkgInfo.version);

		// change the booking category
		data['selectedBookingCategory'] = $("#selBookingCategory").val();
		
		if (top.arrPrivi[PRIVI_ITINERARY_FARE_MASK_MODIFY] == 1) {
			data['itineraryFareMaskOption'] = $('#selItineraryFareMask').val();
		}else{
			data['itineraryFareMaskOption'] = $("#hdnItineraryFareMask").val(); 
		}
		if (top.arrPrivi[PRIVI_MODIFY_CALL_ORIGIN_COUNTRY] == 1) {
			data['selectedOriginCountry'] = $("#selOriginCountryOfCall").val();
		} else {
			data['selectedOriginCountry'] = '';
		}
		
		data['hdnItineraryFareMask'] = $("#hdnItineraryFareMask").val();
		
		// Endosment
		data["endorsementTxt"] = $("#txtEndosment").val();
		// User Notes
		data["userNotes"] = $("#txtUNotes").val();
		// User Note Type
		data["userNoteType"] = $("#selClassification").val();
		
		data['foidIds'] = UI_tabBookingInfo.foidIds;
		data['foidCode'] = UI_tabBookingInfo.foidCode;
		data['segmentList'] = UI_tabBookingInfo.segmentList;
		
	
		
		// Adults and Children
		UI_tabBookingInfo.buildPaxAdults(data);
		
		// Infants
		UI_tabBookingInfo.buildPaxInfants(data);
				
		$('#resPax').val(UI_reservation.jsonPassengers);
		
		data["applyNameChangeCharge"] = true;
		// override name change charge
		if (DATA_ResPro.initialParams.overrideNameChangeCharge) { 			
			data["applyNameChangeCharge"] = $("#chkBoxApplyCharge").is(':checked');			
		}	
		
		UI_tabBookingInfo.enableAllButtons(false);
		UI_commonSystem.showProgress();
		
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"modifyBookingInfo.action",							
			success: UI_tabBookingInfo.modifyCNTSuccess, error:UI_commonSystem.setErrorStatus});	    
		
	}
	
	UI_tabBookingInfo.buildPaxAdults = function(data){
		// Adults and Children
		var numAdtChd = jsonPaxAdtObj.paxAdults.length;
		for(var i = 1; i <= numAdtChd; i++) {
			var adultPaxTO = {};
			var index = i - 1;
			var rowTR = $("#"+ i + "_displayAdultFirstName").closest('tr');
			
			data['adultPaxTOList[' + index + '].displayPaxTravelReference']	= rowTR.find('td:eq(1)').text();
			data['adultPaxTOList[' + index + '].displayAdultType'] 			= rowTR.find('td:eq(2)').text();
			data['adultPaxTOList[' + index + '].displayAdultTitle'] 		= $("#"+ i + "_displayAdultTitle").val();
			data['adultPaxTOList[' + index + '].displayAdultFirstName'] 	= $("#"+ i + "_displayAdultFirstName").val();
			data['adultPaxTOList[' + index + '].displayAdultLastName']		= $("#"+ i + "_displayAdultLastName").val();
			data['adultPaxTOList[' + index + '].displayAdultNationality']	= $("#"+ i + "_displayAdultNationality").val();
			data['adultPaxTOList[' + index + '].displayAdultDOB'] 			= $("#"+ i + "_displayAdultDOB").val();
			data['adultPaxTOList[' + index + '].displayETicket'] 			= jsonPaxAdtObj.paxAdults[index].displayETicket;
			data['adultPaxTOList[' + index + '].displayPnrPaxCatFOIDNumber'] = jsonPaxAdtObj.paxAdults[index].displayPnrPaxCatFOIDNumber;
			data['adultPaxTOList[' + index + '].displayNationalIDNo'] = jsonPaxAdtObj.paxAdults[index].displayNationalIDNo;
			data['adultPaxTOList[' + index + '].displayPnrPaxCatFOIDExpiry'] = jsonPaxAdtObj.paxAdults[index].displayPnrPaxCatFOIDExpiry;
			data['adultPaxTOList[' + index + '].displayPnrPaxCatFOIDPlace'] = jsonPaxAdtObj.paxAdults[index].displayPnrPaxCatFOIDPlace;
			data['adultPaxTOList[' + index + '].displayPnrPaxPlaceOfBirth'] = jsonPaxAdtObj.paxAdults[index].displayPnrPaxPlaceOfBirth;
			data['adultPaxTOList[' + index + '].displayTravelDocType'] = jsonPaxAdtObj.paxAdults[index].displayTravelDocType;
			data['adultPaxTOList[' + index + '].displayVisaDocNumber'] = jsonPaxAdtObj.paxAdults[index].displayVisaDocNumber;
			data['adultPaxTOList[' + index + '].displayVisaDocPlaceOfIssue'] = jsonPaxAdtObj.paxAdults[index].displayVisaDocPlaceOfIssue;
			data['adultPaxTOList[' + index + '].displayVisaDocIssueDate'] = jsonPaxAdtObj.paxAdults[index].displayVisaDocIssueDate;
			data['adultPaxTOList[' + index + '].displayVisaApplicableCountry'] = jsonPaxAdtObj.paxAdults[index].displayVisaApplicableCountry;
			data['adultPaxTOList[' + index + '].displayPnrPaxCatFOIDType'] = jsonPaxAdtObj.paxAdults[index].displayPnrPaxCatFOIDType;
			data['adultPaxTOList[' + index + '].displayAdultFirstNameOl'] = jsonPaxAdtObj.paxAdults[index].displayAdultFirstNameOl;
			data['adultPaxTOList[' + index + '].displayAdultLastNameOl'] = jsonPaxAdtObj.paxAdults[index].displayAdultLastNameOl;
			data['adultPaxTOList[' + index + '].displayNameTranslationLanguage'] = jsonPaxAdtObj.paxAdults[index].displayNameTranslationLanguage;
			data['adultPaxTOList[' + index + '].displayAdultTitleOl'] = jsonPaxAdtObj.paxAdults[index].displayAdultTitleOl;
			data['adultPaxTOList[' + index + '].displaypnrPaxArrivalFlightNumber'] = jsonPaxAdtObj.paxAdults[index].displaypnrPaxArrivalFlightNumber;
			data['adultPaxTOList[' + index + '].displaypnrPaxFltArrivalDate'] = jsonPaxAdtObj.paxAdults[index].displaypnrPaxFltArrivalDate;
			data['adultPaxTOList[' + index + '].displaypnrPaxArrivalTime'] = jsonPaxAdtObj.paxAdults[index].displaypnrPaxArrivalTime;
			data['adultPaxTOList[' + index + '].displaypnrPaxDepartureFlightNumber'] = jsonPaxAdtObj.paxAdults[index].displaypnrPaxDepartureFlightNumber;
			data['adultPaxTOList[' + index + '].displaypnrPaxFltDepartureDate'] = jsonPaxAdtObj.paxAdults[index].displaypnrPaxFltDepartureDate;
			data['adultPaxTOList[' + index + '].displaypnrPaxDepartureTime'] = jsonPaxAdtObj.paxAdults[index].displaypnrPaxDepartureTime;
			data['adultPaxTOList[' + index + '].displayPnrPaxGroupId'] = jsonPaxAdtObj.paxAdults[index].displayPnrPaxGroupId; 
			data['adultPaxTOList[' + index + '].displayFFID'] = $("#"+ i + "_displayFFID").val();
		}
	}
	
	UI_tabBookingInfo.buildPaxInfants = function(data){
		var  adultseq = 0;
		if(jsonPaxAdtObj.paxInfants) {
			var numInf = jsonPaxAdtObj.paxInfants.length;
			for(var i = 1; i <= numInf; i++) {
				var infantPaxTO = {};
				var index = i - 1;
				var rowTR = $("#"+ i + "_displayInfantFirstName").closest('tr');
				
				data['infantPaxTOList[' + index + '].displayPaxTravelReference']	= rowTR.find('td:eq(1)').text();
				data['infantPaxTOList[' + index + '].displayInfantType']			= rowTR.find('td:eq(2)').text();
				data['infantPaxTOList[' + index + '].displayInfantFirstName']		= $("#"+ i + "_displayInfantFirstName").val();
				data['infantPaxTOList[' + index + '].displayInfantLastName']		= $("#"+ i + "_displayInfantLastName").val();
				data['infantPaxTOList[' + index + '].displayInfantNationality']		= $("#"+ i + "_displayInfantNationality").val();
				data['infantPaxTOList[' + index + '].displayInfantDOB']				= $("#"+ i + "_displayInfantDOB").val();
				data['infantPaxTOList[' + index + '].displayETicket'] 			= jsonPaxAdtObj.paxInfants[index].displayETicket;
				data['infantPaxTOList[' + index + '].displayPnrPaxCatFOIDNumber'] = jsonPaxAdtObj.paxInfants[index].displayPnrPaxCatFOIDNumber;
				data['infantPaxTOList[' + index + '].displayPnrPaxCatFOIDExpiry'] = jsonPaxAdtObj.paxInfants[index].displayPnrPaxCatFOIDExpiry;
				data['infantPaxTOList[' + index + '].displayPnrPaxCatFOIDPlace'] = jsonPaxAdtObj.paxInfants[index].displayPnrPaxCatFOIDPlace;				
				data['infantPaxTOList[' + index + '].displayPnrPaxPlaceOfBirth'] = jsonPaxAdtObj.paxInfants[index].displayPnrPaxPlaceOfBirth;
				data['infantPaxTOList[' + index + '].displayTravelDocType'] = jsonPaxAdtObj.paxInfants[index].displayTravelDocType;
				data['infantPaxTOList[' + index + '].displayVisaDocNumber'] = jsonPaxAdtObj.paxInfants[index].displayVisaDocNumber;
				data['infantPaxTOList[' + index + '].displayVisaDocPlaceOfIssue'] = jsonPaxAdtObj.paxInfants[index].displayVisaDocPlaceOfIssue;
				data['infantPaxTOList[' + index + '].displayVisaDocIssueDate'] = jsonPaxAdtObj.paxInfants[index].displayVisaDocIssueDate;
				data['infantPaxTOList[' + index + '].displayVisaApplicableCountry'] = jsonPaxAdtObj.paxInfants[index].displayVisaApplicableCountry;
				data['infantPaxTOList[' + index + '].displayInfantFirstNameOl'] = jsonPaxAdtObj.paxInfants[index].displayInfantFirstNameOl;
				data['infantPaxTOList[' + index + '].displayInfantLastNameOl'] = jsonPaxAdtObj.paxInfants[index].displayInfantLastNameOl;
				data['infantPaxTOList[' + index + '].displayNameTranslationLanguage'] = jsonPaxAdtObj.paxInfants[index].displayNameTranslationLanguage;
				data['infantPaxTOList[' + index + '].displaypnrPaxArrivalFlightNumber'] = jsonPaxAdtObj.paxInfants[index].displaypnrPaxArrivalFlightNumber;
				data['infantPaxTOList[' + index + '].displaypnrPaxFltArrivalDate'] = jsonPaxAdtObj.paxInfants[index].displaypnrPaxFltArrivalDate;
				data['infantPaxTOList[' + index + '].displaypnrPaxArrivalTime'] = jsonPaxAdtObj.paxInfants[index].displaypnrPaxArrivalTime;
				data['infantPaxTOList[' + index + '].displaypnrPaxDepartureFlightNumber'] = jsonPaxAdtObj.paxInfants[index].displaypnrPaxDepartureFlightNumber;
				data['infantPaxTOList[' + index + '].displaypnrPaxFltDepartureDate'] = jsonPaxAdtObj.paxInfants[index].displaypnrPaxFltDepartureDate;
				data['infantPaxTOList[' + index + '].displaypnrPaxDepartureTime'] = jsonPaxAdtObj.paxInfants[index].displaypnrPaxDepartureTime;
				data['infantPaxTOList[' + index + '].displayPnrPaxGroupId'] = jsonPaxAdtObj.paxInfants[index].displayPnrPaxGroupId;
				data['infantPaxTOList[' + index + '].displayInfantTitleOl'] = jsonPaxAdtObj.paxInfants[index].displayInfantTitleOl;
				data['infantPaxTOList[' + index + '].displayInfantTitle'] = $("#"+ i + "_displayInfantTitle").val();
				data['infantPaxTOList[' + index + '].displayNationalIDNo'] = jsonPaxAdtObj.paxInfants[index].displayNationalIDNo;
				
				var infantTravellingWithAdult = $("#"+ i + "_displayInfantTravellingWith").val() - 1;
				adultseq = jsonPaxAdtObj.paxAdults[infantTravellingWithAdult].displayPaxSequence ;
				data['infantPaxTOList[' + index + '].displayInfantTravellingWith'] = adultseq;	
			}
		}
	}
	
	function hasDuplicates(arr) {
		    var i =arr.length, j, val;

		    while (i--) {
		        val = arr[i];
		        if(val.length > 0){	        
		        	j = i;
			        while (j--) {
			            if (arr[j].toLowerCase() === val.toLowerCase()) {
			                return true;
			            }
			        }
		        }
		    }
		    return false;
		}
	
	UI_tabBookingInfo.buildPaxNameDetails = function(){
		var paxNameDetails = {};
		var adultNames = [];
		var ffids =[];
        var numAdtChd = jsonPaxAdtObj.paxAdults.length;
        for(var i = 1; i <= numAdtChd; i++) {       	
        	ffids[i-1]=$.trim($("#"+ i + "_display-FFID").val());
            var paxADT = UI_tabBookingInfo.tempPaxAdults[i-1];
            if (paxADT.changed){
                var rowTR = $("#"+ i + "_display-AdultFirstName").closest('tr');
                var adultPaxNameTO = {};
                adultPaxNameTO.displayPaxTravelReference	= rowTR.find('td:eq(1)').text();
                adultPaxNameTO.displayAdultType 			= rowTR.find('td:eq(2)').text();
                adultPaxNameTO.displayAdultTitle		= $.trim($("#"+ i + "_display-AdultTitle").val());
                adultPaxNameTO.displayAdultFirstName	= $.trim($("#"+ i + "_display-AdultFirstName").val());
                adultPaxNameTO.displayAdultLastName		= $.trim($("#"+ i + "_display-AdultLastName").val());
                adultPaxNameTO.displayFFID		= $.trim($("#"+ i + "_display-FFID").val());
                adultPaxNameTO.id						=  i;
                adultPaxNameTO.nameChangeApplied = true;
                //check ADT TBA
                if (paxADT.displayAdultFirstName == "T B A" || paxADT.displayAdultLastName == "T B A"){
                    adultPaxNameTO.nameChangeApplied = false;
                } else if((paxADT.displayAdultFirstName).toUpperCase() == (adultPaxNameTO.displayAdultFirstName).toUpperCase() && 
                		(paxADT.displayAdultLastName).toUpperCase() == (adultPaxNameTO.displayAdultLastName).toUpperCase()){
                	adultPaxNameTO.nameChangeApplied = false;
                }
                adultNames[adultNames.length] = adultPaxNameTO;
            }
		}
		paxNameDetails.adultNames = adultNames;

		isDuplicateFFIDExist = hasDuplicates(ffids);
		var  adultseq = 0;
		if(jsonPaxAdtObj.paxInfants) {
			var infantNames = [];
            var numInf = jsonPaxAdtObj.paxInfants.length;
			for(var j = 1; j <= numInf; j++) {
                var paxINF = UI_tabBookingInfo.tempPaxInfants[j-1];
                if (paxINF.changed){
                    var rowTR = $("#"+ j + "_display-InfantFirstName").closest('tr');
                    var infantPaxNameTO = {};
                    infantPaxNameTO.displayPaxTravelReference	= rowTR.find('td:eq(1)').text();
                    infantPaxNameTO.displayInfantType			= rowTR.find('td:eq(2)').text();
                    infantPaxNameTO.displayInfantFirstName		= $.trim($("#"+ j + "_display-InfantFirstName").val());
                    infantPaxNameTO.displayInfantLastName		= $.trim($("#"+ j + "_display-InfantLastName").val());
                    infantPaxNameTO.id							=  j;
                    infantPaxNameTO.nameChangeApplied = true;
                    var infantTravellingWithAdult = $.trim($("#"+ j + "_displayInfantTravellingWith").val()) - 1;
                    adultseq = jsonPaxAdtObj.paxAdults[infantTravellingWithAdult].displayPaxSequence ;
                    infantPaxNameTO.displayInfantTravellingWith = adultseq;
                    //check INF TBA
                    if (paxINF.displayInfantFirstName == "T B A" || paxINF.displayInfantLastName == "T B A"){
                        infantPaxNameTO.nameChangeApplied = false;
                    } else if((paxINF.displayInfantFirstName).toUpperCase() == (infantPaxNameTO.displayInfantFirstName).toUpperCase()
                    		&& (paxINF.displayInfantLastName).toUpperCase() == (infantPaxNameTO.displayInfantLastName).toUpperCase()){
                    	infantPaxNameTO.nameChangeApplied = false;
                    }
                    infantNames[infantNames.length] = infantPaxNameTO;
                }
			}
			paxNameDetails.infantNames = infantNames;
		}
		
		return paxNameDetails;
	}
	
	UI_tabBookingInfo.buildPaxAdultsNew = function(data){	
		var numAdtChd = jsonPaxAdtObj.paxAdults.length;
		for(var i = 1; i <= numAdtChd; i++) {
			var index = i - 1;
			var rowTR = $("#"+ i + "_display-AdultFirstName").closest('tr');
			data['adultPaxTOList[' + index + '].displayPaxTravelReference']	= rowTR.find('td:eq(1)').text();
			data['adultPaxTOList[' + index + '].displayAdultType'] 			= rowTR.find('td:eq(2)').text();
			data['adultPaxTOList[' + index + '].displayAdultTitle'] 		= $("#"+ i + "_display-AdultTitle").val();
			data['adultPaxTOList[' + index + '].displayAdultFirstName'] 	= $("#"+ i + "_display-AdultFirstName").val();
			data['adultPaxTOList[' + index + '].displayAdultLastName']		= $("#"+ i + "_display-AdultLastName").val();
			data['adultPaxTOList[' + index + '].displayFFID']		= $("#"+ i + "_display-FFID").val();
		}		
	}
	
	UI_tabBookingInfo.buildPassengerDTO = function(){
		
		// Adults and Children
		var numAdtChd = jsonPaxAdtObj.paxAdults.length;		
		for(var i = 1; i <= numAdtChd; i++) {			
			var adultPaxTO = {};
			var index = i - 1;
			var rowTR = $("#"+ i + "_displayAdultFirstName").closest('tr');
			
			jsonPaxAdtObj.paxAdults[index].displayPaxTravelReference	= rowTR.find('td:eq(0)').text();
			jsonPaxAdtObj.paxAdults[index].displayAdultType 			= rowTR.find('td:eq(1)').text();
			jsonPaxAdtObj.paxAdults[index].displayAdultTitle		= $("#"+ i + "_displayAdultTitle").val();
			jsonPaxAdtObj.paxAdults[index].displayAdultFirstName	= $("#"+ i + "_displayAdultFirstName").val();
			jsonPaxAdtObj.paxAdults[index].displayAdultLastName		= $("#"+ i + "_displayAdultLastName").val();
			jsonPaxAdtObj.paxAdults[index].displayAdultNationality	= $("#"+ i + "_displayAdultNationality").val();
			jsonPaxAdtObj.paxAdults[index].displayAdultDOB			= $("#"+ i + "_displayAdultDOB").val();
			jsonPaxAdtObj.paxAdults[index].displayFfid				= $("#"+ i + "_displayFfid").val();
		}
		
		// Infants
		if(jsonPaxAdtObj.paxInfants) {
			var numInf = jsonPaxAdtObj.paxInfants.length;
			for(var i = 1; i <= numInf; i++) {
				var infantPaxTO = {};
				var index = i - 1;
				var rowTR = $("#"+ i + "_displayInfantFirstName").closest('tr');
				
				jsonPaxAdtObj.paxInfants[index].displayPaxTravelReference	= rowTR.find('td:eq(0)').text();
				jsonPaxAdtObj.paxInfants[index].displayInfantType			= rowTR.find('td:eq(1)').text();
				jsonPaxAdtObj.paxInfants[index].displayInfantFirstName		= $("#"+ i + "_displayInfantFirstName").val();
				jsonPaxAdtObj.paxInfants[index].displayInfantLastName		= $("#"+ i + "_displayInfantLastName").val();
				jsonPaxAdtObj.paxInfants[index].displayInfantNationality		= $("#"+ i + "_displayInfantNationality").val();
				jsonPaxAdtObj.paxInfants[index].displayInfantDOB			= $("#"+ i + "_displayInfantDOB").val();
				
				var infantTravellingWithAdult = $("#"+ i + "_displayInfantTravellingWith").val() - 1;
				jsonPaxAdtObj.paxAdults[infantTravellingWithAdult].displayInfantTravelling = index;
			}
		}
	}
	
	UI_tabBookingInfo.mapAddInfantCalendar = function(){
		
		var curDateArr = top.arrParams[0].split('/');
		var deptDateArr =  UI_tabBookingInfo.fltOutDate.split('/');
		
		var curDay = (curDateArr[0] * curDateArr[0] ) / curDateArr[0];
	    var curMon = (curDateArr[1] * curDateArr[1] ) / curDateArr[1];
	    var curYer = curDateArr[2];
		
		var day = (deptDateArr[0] * deptDateArr[0] ) / deptDateArr[0];
		var mon = (deptDateArr[1] * deptDateArr[1] ) / deptDateArr[1];
		var yer =  deptDateArr[2];
		
        var FirstNflnArrivalDateArr = UI_tabBookingInfo.fltOutDate.split('/');
		
		var FirstNflnArrivalDay = FirstNflnArrivalDateArr[0];
		var FirstNflnArrivalMon = FirstNflnArrivalDateArr[1];
		var FirstNflnArrivalYear =  FirstNflnArrivalDateArr[2];
		
		var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;
		
		$("#tblPaxInf").find("tr").each(function(){
			$(this).find("input").each(function(){
				if (this.name == "displayInfantDOB"){
					// var minDate = '-'+ paxValidation.infantAgeCutOverYears +
					// 'y';
					// var minDate = day + 'd-' + mon + 'm-' +
					// (paxValidation.infantAgeCutOverYears-yer) + 'y';
					var minDate = new Date((FirstNflnArrivalYear-paxValidation.infantAgeCutOverYears), FirstNflnArrivalMon-1, FirstNflnArrivalDay);
					var maxDate = getMaximumPossibleInfantDOB(curDateArr,deptDateArr,infantAgeLowerBoundInDays);					
					var range = '-' + paxValidation.infantAgeCutOverYears + ':+' + paxValidation.infantAgeCutOverYears;
					$("#"+ this.id).datepicker({minDate : minDate, 
												maxDate : maxDate, 
												dateFormat: UI_commonSystem.strDTFormat, 
												changeMonth: 'true' , 
												changeYear: 'true',
												yearRange: range,
												showButtonPanel: 'true' });				
				}
			});
		});
		
	}
	
	UI_tabBookingInfo.modifyCNTSuccess = function(response){
		$("#paxNameChangePopUp").dialog('close');
		UI_reservation.loadReservation("MODCNT", response,false);	
	}
	
	UI_tabBookingInfo.modifyCNTSuccessUpdate = function(response){
		if(response.success) {					
			// TODO : Improve message
			showConfirmation("Reservation information successfully updated");
			
			if(UI_reservation.balnceTOPay > 0 || UI_reservation.allowConfirmOnNoPayOHD()) {
				if(confirm("Proceed to make payment?")){				
					$("#divBookingTabs").tabs("option", "active", 2);
				}				
			}
			UI_tabBookingInfo.refreshPromotionAlert();
		} else {			
			showERRMessage(response.messageTxt);
		}
	}
	
	/*
	 * Cancel on Click
	 */
	
	UI_tabBookingInfo.cancelOnClick = function(){
		UI_tabBookingInfo.cancelOrVoid();
		UI_tabBookingInfo.showConfirmUpdate("RESCANCEL");
	}
	
	UI_tabBookingInfo.voidOnClick = function(){ 
		$('#isVoidReservation').val(true);
		UI_tabBookingInfo.cancelOrVoid();
		UI_tabBookingInfo.showConfirmUpdate("RESVOID");
	} 
	
	UI_tabBookingInfo.cancelOrVoid = function(){
		UI_commonSystem.pageOnChange();
		$('#flexiAlertInfo').val($.toJSON(UI_reservation.jsonFlexiAlerts));
	}
	/*
	 * Show Confirm Update
	 */
	UI_tabBookingInfo.showConfirmUpdate = function(strMode){
		var strUrl = 'showNewFile!loadConfirmUpdate.action?confirmUpdateMode=' + strMode;
		
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900));
	}
	
	/*
	 * Cancel
	 */
	UI_tabBookingInfo.cancelConfirm = function(data){
		UI_tabBookingInfo.enableAllButtons(false);		
		$('#resSegments').val(UI_reservation.jsonSegs);
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"cancelReservation.action",							
			success: UI_tabBookingInfo.cancelSuccess, error:UI_commonSystem.setErrorStatus});										
		return false;
	}
	
	UI_tabBookingInfo.showConfirmError = function(resMessage){
		showERRMessage(resMessage);
	}
	
	UI_tabBookingInfo.overiedFareMetaData = {
			precentage: "_precentage",
			chargeType: "_type", min: "_prec_min", max : "_prec_max",
			overideOnlyCssTag :"confOverideOnly",		
			adultObj:{isPresent:function (){return false;}},
			childObj:{isPresent:function (){return false;}},
			infantObj : {isPresent:function (){return false;}},
			
			setIDNames : function (sentID,type){
				// sets the dynamic id values
				var temp_precentage = ""+sentID+this.precentage;
				var temp_type = "selDefineCanType";
				var temp_min = ""+sentID+ this.min;
				var temp_max = ""+sentID + this.max;
				if(type == "AD"){
					this.adultObj= new UI_tabBookingInfo.overideObj.init(temp_type, temp_precentage, temp_min, temp_max);
					this.adultObj.type = "AD";	
					return this.adultObj;
				}else if(type == "CH"){
					this.childObj= new UI_tabBookingInfo.overideObj.init(temp_type, temp_precentage, temp_min, temp_max);
					this.childObj.type = "CH";				
					return this.childObj;
				}else if(type == "IN"){
					this.infantObj= new UI_tabBookingInfo.overideObj.init(temp_type, temp_precentage, temp_min, temp_max);
					this.infantObj.type = "IN";				
					return this.infantObj;
				}
				return null;
			}
			
		}
	
	UI_tabBookingInfo.showConfirmError = function(resMessage){
		showERRMessage(resMessage);
	}
	
	UI_tabBookingInfo.overiedFareMetaData = {
			precentage: "_precentage",
			chargeType: "_type", min: "_prec_min", max : "_prec_max",
			overideOnlyCssTag :"confOverideOnly",		
			adultObj:{isPresent:function (){return false;}},
			childObj:{isPresent:function (){return false;}},
			infantObj : {isPresent:function (){return false;}},
			
			setIDNames : function (sentID,type){
				// sets the dynamic id values
				var temp_precentage = ""+sentID+this.precentage;
				var temp_type = "selDefineCanType";
				var temp_min = ""+sentID+ this.min;
				var temp_max = ""+sentID + this.max;
				if(type == "AD"){
					this.adultObj= new UI_tabBookingInfo.overideObj.init(temp_type, temp_precentage, temp_min, temp_max);
					this.adultObj.type = "AD";	
					return this.adultObj;
				}else if(type == "CH"){
					this.childObj= new UI_tabBookingInfo.overideObj.init(temp_type, temp_precentage, temp_min, temp_max);
					this.childObj.type = "CH";				
					return this.childObj;
				}else if(type == "IN"){
					this.infantObj= new UI_tabBookingInfo.overideObj.init(temp_type, temp_precentage, temp_min, temp_max);
					this.infantObj.type = "IN";				
					return this.infantObj;
				}
				return null;
			}
			
		}
	
	/*
	 * Cancellation Success
	 */
	UI_tabBookingInfo.cancelSuccess = function(response){
		UI_reservation.loadReservation("CNXRES", response,false);			
	}
	
	UI_tabBookingInfo.cancelSuccessUpdate = function(response){
		if(response.success){			
			showCommonError("CONFIRMATION", "Cancel Reservation Successful");
		}else {
			showERRMessage(response.messageTxt);
		}		
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	}
	
	/*
	 * Remove Pax
	 */
	UI_tabBookingInfo.removePaxConfirm = function(data){
		UI_tabBookingInfo.enableAllButtons(false);		
		data["pnr"] = jsonPNR;
		data['paxAdults'] = UI_tabBookingInfo.jsSltdAdt;
		data['paxInfants'] = UI_tabBookingInfo.jsSltdInf;		
		data['resSegments'] = UI_reservation.jsonSegs;
		data['resPaxs']= $.toJSON(UI_tabBookingInfo.getSpliPaxInfo());
		data['flexiAlertInfo']=$.toJSON(UI_reservation.jsonFlexiAlerts);
		data['version'] = jsonBkgInfo.version;
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', 
									processData: false, 
									data:data, 
									url:"removePax.action",							
									success: UI_tabBookingInfo.removePaxSuccess, 
									error:UI_commonSystem.setErrorStatus});										
		return false;
	}
	
	/*
	 * Remove Pax Success
	 */
	UI_tabBookingInfo.removePaxSuccess = function(response){
		UI_reservation.loadReservation("REMPAX", response, false);		
	}
	
	UI_tabBookingInfo.removePaxSuccessUpdate = function(response){
		if(response.success){				
			jsonTravellingWith = UI_tabBookingInfo.populateJsonTravellingWith(jsonPaxAdtObj);
		//	$('#tblPaxInf').GridUnload();
		//	UI_tabBookingInfo.constructGridInf({id:"#tblPaxInf"});
		//	UI_commonSystem.fillGridData({id:"#tblPaxInf", data:jsonPaxAdtObj.paxInfants, editable:true});
			if(jsonPaxAdtObj.paxInfants.length == 0){
				$("#divInfantPane").hide();
			} else {
			    for( i = 1; i <= jsonPaxAdtObj.paxInfants.length; i++){
                    for(j=0;j < jsonPaxAdtObj.paxAdults.length; j++) {
                        if(jsonPaxAdtObj.paxAdults[j].displayPaxSequence == jsonPaxAdtObj.paxInfants[i - 1].displayInfantTravellingWith){
                            var adtSeq = j+1;
                            $("#"+ i + "_displayInfantTravellingWith").val(adtSeq);
                            break;
                        }
                    }
                }
			}
		//	UI_tabBookingInfo.adjustPageHeight(null, jsonPaxAdtObj.paxInfants);
			showCommonError("CONFIRMATION", "Remove PAX Successful : New PNR is " + response.newPnrNo);
			
			if(jsonGroupPNR != null && jsonGroupPNR != ""){
				// top[0].addLCCPNRHistory(response.newPnrNo);
				var jsContactInfo =$.parseJSON(UI_reservation.contactInfo);
				top[0].addLCCPNRHistory(response.newPnrNo, jsContactInfo.firstName);
			}else {
				// Fix this
				var jsContactInfo =$.parseJSON(UI_reservation.contactInfo);
				top[0].addPNRHistoryNew(response.newPnrNo, jsContactInfo.firstName);
			}
			parent.UI_searchReservation.writePNR();
			top[0].showPNRIcon();
			UI_tabBookingInfo.refreshPromotionAlert();
		}else {
			showERRMessage(response.messageTxt);
		}		
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	}
	
	// -----------------
	
	/*
	 * View User Notes On Click
	 */
	UI_tabBookingInfo.viewNotesOnClick = function(){
		UI_commonSystem.pageOnChange();
		var strFileName = "showNewFile!loadUserNote.action?pnr=" + jsonPNR+"&groupPNR="+jsonGroupPNR+"&originSalesChanel="+UI_reservation.originSalesChanel;
		var inHeight = 630;
		if ($.browser.msie){inHeight = 640;}
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 900));
	}
	/*
	 * Add and classify user Notes
	 */
	UI_tabBookingInfo.classifyUserNote = function(){
		/**Checking whether user has the privilage to classify user Notes*/
        if((DATA_ResPro.initialParams.allowClassifyUserNotes) && (DATA_ResPro.initialParams.allowAddUserNotes)){
          $("#selClassification").show();
          $("#selClassification").enable();
        }else{
          $("#selClassification").hide();
        }       
        $("#btnAddUserNote").hide();
        $("#btnSaveUserNote").show();
        $("#btnSaveUserNote").enable();
        $("#txtUNotes").removeAttr('disabled');
        $("#txtUNotes").val("");
        $("#btnSaveUserNote").unbind().click(function() {UI_tabBookingInfo.saveUserNoteOnClick();});
		
	}
	/*
	 * Save classified UserNote
	 */
	UI_tabBookingInfo.saveUserNoteOnClick = function(){
		if(UI_tabBookingInfo.validateUNote()) {
			var data = {};               
			data["version"] = jsonBkgInfo.version;
			// PNR
			data["groupPNR"] = jsonGroupPNR;
			data["pnr"] = jsonPNR;
			// User Notes
			data["userNotes"] = $("#txtUNotes").val();
			// User Note Type
			data["userNoteType"] = $("#selClassification").val();
			
			$("#txtUNotes").attr('disabled','disabled');
			
			UI_tabBookingInfo.enableAllButtons(false);
			UI_commonSystem.showProgress();
			$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"userNotes.action",							
				success: UI_tabBookingInfo.addUserNote,error:UI_commonSystem.setErrorStatus});		
		}else{
			// If Validation is not ok, return to page
			
			return;
		}
	}
	UI_tabBookingInfo.addUserNote = function(response){
		 UI_tabBookingInfo.enableAllButtons(true);
		 $("#selClassification").val("PUB");
		if(response.success){	
		  UI_commonSystem.hideProgress();
		  showConfirmation(raiseError("XBE-CONF-08","255", "User Notes"));	
		  $("#txtUNotes").attr('disabled','disabled');
		  $("#selClassification").disable();
		  $("#btnSaveUserNote").hide();
		  $("#btnAddUserNote").show();
		  jsonUserNotes = $("#txtUNotes").val();
		} else{
			showERRMessage(raiseError("XBE-ERR-98","255", "User Notes"));
			$("#txtUNotes").removeAttr('disabled');
			$("#selClassification").enable();
			$("#btnSaveUserNote").show();
			$("#btnAddUserNote").hide();
		}
	}
	UI_tabBookingInfo.validateUNote = function(){
		
		var usrTxt = $.trim($("#txtUNotes").val());
		// Validate UserNotes Field
		if (usrTxt.length > 255) {
			showERRMessage(raiseError("XBE-ERR-18","255", "User Notes"));
			$("#txtUNotes").focus();
			return false ;
		} else if(!UI_commonSystem.validateInvalidChar([{id:"#txtUNotes", desc:"User Notes"}])) {
			return false;
		}else if(usrTxt == null || usrTxt == ""){
			showERRMessage(raiseError("XBE-ERR-01","User Notes"));
			$("#txtUNotes").focus();
			return false ;		
		}else {
			return true;
		}
		
	}
	/*
	 * Accounts
	 */
	UI_tabBookingInfo.btnAccoOnClick = function(inParams){
		UI_tabBookingInfo.intLastSSRRow = inParams.row;
		UI_tabBookingInfo.strLastSSRType = inParams.type;
		
		var strUrl = "showNewFile!loadPaxAccount.action?paxID=";
		
		if(inParams.type == "INF"){
			strUrl = strUrl  + jsonPaxAdtObj.paxInfants[UI_tabBookingInfo.intLastSSRRow].displayPaxTravelReference;
		}else{
			strUrl = strUrl  + jsonPaxAdtObj.paxAdults[UI_tabBookingInfo.intLastSSRRow].displayPaxTravelReference;
		}
		
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900));
	}
	
	/*
	 * MCO
	 */
	UI_tabBookingInfo.btnMCOOnClick = function(inParams){
		UI_tabBookingInfo.intLastSSRRow = inParams.row;
		UI_tabBookingInfo.strLastSSRType = inParams.type;
		
		var strUrl = "showNewFile!loadPaxMCODetails.action?paxID=" + jsonPaxAdtObj.paxAdults[UI_tabBookingInfo.intLastSSRRow].displayPaxTravelReference;
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900));
	}
	
	
	/*
	 * E-Ticket Mask
	 */
	UI_tabBookingInfo.btnETckMaskoOnClick = function(inParams){
		UI_tabBookingInfo.intLastSSRRow = inParams.row;
		UI_tabBookingInfo.strLastSSRType = inParams.type;
		
		switch (UI_tabBookingInfo.strLastSSRType){
		case "ADT" : 
			var strUrl = "showNewFile!loadETicketMask.action?paxID=" + jsonPaxAdtObj.paxAdults[UI_tabBookingInfo.intLastSSRRow].displayPaxTravelReference
							+ "&gdsId=" + UI_reservation.gdsId;
			break;
		case "INF" :
			var strUrl = "showNewFile!loadETicketMask.action?paxID=" + jsonPaxAdtObj.paxInfants[UI_tabBookingInfo.intLastSSRRow].displayPaxTravelReference
							+ "&gdsId=" + UI_reservation.gdsId;
			break;
		}		
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(465, 850));
	}
	
	/*
	 * SSR Confirmation Click
	 */
	UI_tabBookingInfo.ssrConfirmOnClick = function(){
		UI_commonSystem.pageOnChange();
		var strSSRCode = $("#selSSRCode").val();
		var strSSRInfo = $("#txtComment").val();
		
		switch (UI_tabBookingInfo.strLastSSRType){
			case UI_commonSystem.strAdtCode : 
				jsonPaxAdtObj.paxAdults[UI_tabBookingInfo.intLastSSRRow].displayAdultSSRCode = strSSRCode;
				jsonPaxAdtObj.paxAdults[UI_tabBookingInfo.intLastSSRRow].displayAdultSSRInformation = strSSRInfo;
				break;
			case UI_commonSystem.strInfCode :
				jsonPaxAdtObj.paxInfants[UI_tabBookingInfo.intLastSSRRow].displayInfantSSRCode = strSSRCode;
				jsonPaxAdtObj.paxInfants[UI_tabBookingInfo.intLastSSRRow].displayInfantSSRInformation = strSSRInfo;
				break;
		}
		
		UI_tabBookingInfo.ssrCancelOnClick();
	}
	
	/*
	 * View Endorsement Notes On Click
	 */
	UI_tabBookingInfo.viewEndorsementNotesOnClick = function(){
		UI_commonSystem.pageOnChange();
		var strFileName = "showNewFile!loadUserNote.action?pnr=" + jsonPNR+"&groupPNR="+jsonGroupPNR+"&originSalesChanel="+UI_reservation.originSalesChanel;
		var inHeight = 630;
		if ($.browser.msie){inHeight = 640;}
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		top.objCW =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 900));
	}
	
	/*
	 * Cancel
	 */
	UI_tabBookingInfo.cancelSegmentConfirm = function(data){		
		data["segmentRefNo"]    = UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef;		
		$('#resSegments').val(UI_reservation.jsonSegs);
		$("#version").val(jsonBkgInfo.version);
		$('#resContactInfo').val(UI_reservation.contactInfo);
		UI_tabBookingInfo.enableAllButtons(false);
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"cancelSegment.action",							
			success: UI_tabBookingInfo.cancelSegmentSuccess, error:UI_commonSystem.setErrorStatus});										
		return false;
	}
	
	/*
	 * Cancellation Success
	 */
	UI_tabBookingInfo.cancelSegmentSuccess = function(response){		
		UI_reservation.loadReservation("CNXSEG", response,false);		
	}
	
	UI_tabBookingInfo.cancelSegmentSuccessUpdate = function(response){
		if(response.success){			
			showCommonError("CONFIRMATION", "Cancel Segment Successful");
			if(!UI_reservation.ohdRollForwardValidation.allow){
				$("#btnRollForward").disable();
			}			
			UI_tabBookingInfo.refreshPromotionAlert();
		}else {
			showERRMessage(response.messageTxt);
		}		
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	}
	
	/*
	 * SSR Button click
	 */
	UI_tabBookingInfo.btnSSROnClick = function(inParams){
		UI_tabBookingInfo.intLastSSRRow = inParams.row;
		UI_tabBookingInfo.strLastSSRType = inParams.type;
	
		var strSSRCode = "";
		var strSSRInfo = "";
		switch (UI_tabBookingInfo.strLastSSRType){
			case UI_commonSystem.strAdtCode :
				strSSRCode = jsonPaxAdtObj.paxAdults[UI_tabBookingInfo.intLastSSRRow].displayAdultSSRCode;
				strSSRInfo = jsonPaxAdtObj.paxAdults[UI_tabBookingInfo.intLastSSRRow].displayAdultSSRInformation;
				break;
			case UI_commonSystem.strInfCode :
				strSSRCode = jsonPaxAdtObj.paxInfants[UI_tabBookingInfo.intLastSSRRow].displayInfantSSRCode;
				strSSRInfo = jsonPaxAdtObj.paxInfants[UI_tabBookingInfo.intLastSSRRow].displayInfantSSRInformation;
				break;
		}
		
		$("#selSSRCode").val(strSSRCode);
		$("#txtComment").val(strSSRInfo);
		$("#divSSRPane").show();
	}
	
	/*
	 * SSR Cancel Click
	 */
	UI_tabBookingInfo.ssrCancelOnClick = function(){
		UI_commonSystem.pageOnChange();
		$("#selSSRCode").val("");
		$("#txtComment").val("");
		$("#divSSRPane").hide();
		
		UI_tabBookingInfo.intLastSSRRow = null;
		UI_tabBookingInfo.strLastSSRType = null;
	}
	
	/*
	 * Date On Blur
	 */ 
	UI_tabBookingInfo.dobOnBlur = function(objC){
		var strDT = dateChk(objC.id);
		strDT = strDT != true ? strDT : objC.value;
		objC.value = strDT;
		UI_commonSystem.pageOnChange();
	}
	
	/*
	 * TODO If you know how to bind the calendar to jqgrid remove this method
	 */
	UI_tabBookingInfo.mapCalendar = function(){
		/* Adult */
		var intLen = jsonPaxAdtObj.paxAdults.length;		
		var curDateArr = top.arrParams[0].split('/');
		var deptDateArr =  UI_tabBookingInfo.fltOutDate.split('/');

		var curDay = (curDateArr[0] * curDateArr[0] ) / curDateArr[0];
	    var curMon = (curDateArr[1] * curDateArr[1] ) / curDateArr[1];
	    var curYer = curDateArr[2];
		
		var day = (deptDateArr[0] * deptDateArr[0] ) / deptDateArr[0];
		var mon = (deptDateArr[1] * deptDateArr[1] ) / deptDateArr[1];
		var yer =  deptDateArr[2];
		
		var lastArrivalDateArr = jsonLastFlightArrivalDate.split('/');
		
		var lastArrivalDay = lastArrivalDateArr[0];
		var lastArrivalMon = lastArrivalDateArr[1];
		var lastArrivalYear =  lastArrivalDateArr[2];
		
		var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;
		
		if (intLen > 0){
			var i = 1;
			$("#tblPaxAdt").find("tr").each(function(){
				$(this).find("input").each(function(){
					if (this.name == "displayAdultDOB"){						
						var minDate;
						var maxDate;
						var range = ''
						if(jsonPaxAdtObj.paxAdults[i - 1].displayAdultType == 'CH'){							
							minDate = new Date((yer-paxValidation.childAgeCutOverYears), lastArrivalMon-1, lastArrivalDay);
							maxDate = new Date((yer-paxValidation.infantAgeCutOverYears), lastArrivalMon-1, lastArrivalDay-1);							
							range = '-' + paxValidation.childAgeCutOverYears + ':+' + paxValidation.childAgeCutOverYears ;
							
							$("#"+ i + "_displayAdultTitle").empty();
							$("#"+ i + "_displayAdultTitle").fillDropDown( {
								dataArray : top.arrTitleChild,
								keyIndex : 0,
								valueIndex : 1,
								firstEmpty : true
							});
						}else{							
							// minDate = day + 'd-' + mon + 'm-' +
							// (paxValidation.adultAgeCutOverYears-yer) + 'y';
							// maxDate = day-1 + 'd-' + mon + 'm-' +
							// (paxValidation.childAgeCutOverYears-yer) + 'y'
							minDate = new Date((yer-paxValidation.adultAgeCutOverYears), lastArrivalMon-1, lastArrivalDay);
							maxDate = new Date((yer-paxValidation.childAgeCutOverYears), lastArrivalMon-1, lastArrivalDay-1);
							range = '-' + paxValidation.adultAgeCutOverYears + ':+' + paxValidation.adultAgeCutOverYears;
						}		
						
						$("#" + this.id).datepicker(
							{
								minDate : minDate,
								maxDate : maxDate,
								dateFormat : UI_commonSystem.strDTFormat,
								changeMonth : 'true',
								changeYear : 'true',
								yearRange: range,
								showButtonPanel : 'true'
							});
						$("#"+ i + "_displayAdultTitle").val(jsonPaxAdtObj.paxAdults[i - 1].displayAdultTitle);
						$("#"+ i + "_displayAdultNationality").val(jsonPaxAdtObj.paxAdults[i - 1].displayAdultNationality);
						i++;
					}
				});
			});
		}
		
		/* Infant */
		if (jsonPaxAdtObj.paxInfants != null){
			intLen = jsonPaxAdtObj.paxInfants.length;
			
			if (intLen > 0){
				var i = 1;
				$("#tblPaxInf").find("tr").each(function(){
					var adtSeq = 1;
					$(this).find("input").each(function(){
						if (this.name == "displayInfantDOB"){							
							// var minDate = day + 'd-' + mon + 'm-' +
							// (paxValidation.infantAgeCutOverYears-yer) + 'y';
							var minDate = new Date((lastArrivalYear-paxValidation.infantAgeCutOverYears), lastArrivalMon-1, lastArrivalDay);
							var maxDate = getMaximumPossibleInfantDOB(curDateArr,deptDateArr,infantAgeLowerBoundInDays);
							var range = '-' + paxValidation.infantAgeCutOverYears + ':+' + paxValidation.infantAgeCutOverYears;
							
							$("#"+ this.id).datepicker(
									{
										minDate : minDate,
										maxDate : maxDate,
										dateFormat : UI_commonSystem.strDTFormat,
										changeMonth : 'true',
										changeYear : 'true',
										yearRange: range,
										showButtonPanel : 'true'
									});
							for(j=0;j < jsonPaxAdtObj.paxAdults.length; j++) {
								if(jsonPaxAdtObj.paxAdults[j].displayPaxSequence == jsonPaxAdtObj.paxInfants[i - 1].displayInfantTravellingWith){
									adtSeq = j+1;
									break;
								}
							}
							if(jsonPaxAdtObj.paxInfants[i - 1].displayInfantTitle!=undefined && jsonPaxAdtObj.paxInfants[i - 1].displayInfantTitle!=null){
								$("#"+ i + "_displayInfantTitle").val(jsonPaxAdtObj.paxInfants[i - 1].displayInfantTitle.toUpperCase());	
							}
							$("#"+ i + "_displayInfantTravellingWith").val(adtSeq);
							$("#"+ i + "_displayInfantNationality").val(jsonPaxAdtObj.paxInfants[i - 1].displayInfantNationality);
							jQuery("#" + i + "_displayInfantTravellingWith").closest("td")[0].title = adtSeq;
							i++;
						}
					});
				});
			}	
		}
				
	}
	
	
	function getMaximumPossibleInfantDOB(curentDateArr, firstDepDateArr, infantAgeLowerDays){
		
		var depDay =  firstDepDateArr[0] ;
		var depMon =  firstDepDateArr[1] ;
		var depYear =  firstDepDateArr[2] ;
		
		var currDay = curentDateArr[0];
		var currMon = curentDateArr[1];
		var currYear = curentDateArr[2];
		
		var maxDate = new Date(depYear,depMon-1,depDay-infantAgeLowerDays-1) ;
		var currentDate = new Date (currYear,currMon-1,currDay);
		 
		 if (maxDate < currentDate){			 
			 return maxDate;
		 } else {
		     return currentDate ;
		 }		
	}
	
	/*
	 * Fill Booking Info
	 */
	UI_tabBookingInfo.fillBookingInfo = function(){
		if(jsonBkgInfo.PNR == ""){
			showERRMessage("System Error ! Please Contact System Administrator.");
		}else {
			$("#divPnrNo").html(jsonBkgInfo.PNR);
			UI_tabBookingInfo.updateBookingStatus(jsonBkgInfo.displayStatus, jsonResPaymentSummary.balanceToPay);
			$("#divAgent").html(jsonBkgInfo.agent);
			$("#selBookingCategory").fillDropDown({dataArray:top.arrAllBookingCategoriesList, keyIndex:0 , valueIndex:1, firstEmpty:false });
			$("#selBookingCategory").val(jsonBkgInfo.bookingCategory);
			
			if (top.arrPrivi[PRIVI_ITINERARY_FARE_MASK_MODIFY] == 1) {
				$('#selItineraryFareMask').val(jsonBkgInfo.itineraryFareMask);
			}

			$('#selOriginCountryOfCall').val(jsonBkgInfo.originCountryOfCall);
					
			$('#hdnItineraryFareMask').val(jsonBkgInfo.itineraryFareMask);
			
			$('#ticketExpiryDate').val(jsonBkgInfo.ticketExpiryDate);
			$('#ticketExpiryEnabled').val(jsonBkgInfo.ticketExpiryEnabled);		
			$('#ticketValidFrom').val(jsonBkgInfo.ticketValidFromDate);		
		}
		
	}
	
	UI_tabBookingInfo.refreshPromotionAlert = function(){
		if(UI_tabBookingInfo.promoCodeAlertText != null ||
				UI_tabBookingInfo.promoCodeAlertText != ''){				
			UI_tabBookingInfo.promoCodeAlertText = '';
		}
		UI_tabBookingInfo.updateBookingStatus(jsonBkgInfo.displayStatus, '');
	}
	
	UI_tabBookingInfo.updateBookingStatus = function(status, toPay){
		var resAlert = '';
		var autoCnxAlert = UI_tabBookingInfo.createAutoCnxBalToPayAlert('AutoCnxBalToPayImg');
		var isAutoCnx = false;
		var isPromoCode = false;
		if(autoCnxAlert != ''){
			resAlert = autoCnxAlert;
			isAutoCnx = true;
		} else {
			resAlert = UI_tabBookingInfo.createPromoCodeInfoAlert('PromoCodeAlertImg');
			isPromoCode = true;
		}
		
		var bkgStatusStr = status + resAlert;		
		$("#divBkgStatus").html(bkgStatusStr);	
		if(UI_reservation.refreshResponse){
			UI_reservation.refreshResponse.bookingStatus = bkgStatusStr;
		}
		
		if(autoCnxAlert){
			UI_tabBookingInfo.autoCnxAlertHover('#AutoCnxBalToPayImg');
		} else if(isPromoCode){
			UI_tabBookingInfo.promoCodeAlertHover("#PromoCodeAlertImg");
		}
	}
	
	UI_tabBookingInfo.autoCnxAlertHover = function(imgId){
		$(imgId).hover(UI_tabBookingInfo.getAutoCancellationBalanceToPay, UI_tabBookingInfo.hideToolTip);
	}
	
	UI_tabBookingInfo.promoCodeAlertHover = function(imgId){
		$(imgId).hover(UI_tabBookingInfo.createPromoCodeAlertInfo, UI_tabBookingInfo.hideToolTip);
	}
	
	UI_tabBookingInfo.createAutoCnxBalToPayAlert = function(imageID){
		if(jsonBkgInfo.status == "CNF" && UI_reservation.jsonAutoCnxAlert != null && 
				(typeof UI_reservation.jsonAutoCnxAlert != 'undefined')){			
			cnxAlert = '&nbsp;&nbsp;&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
			return cnxAlert;
		}
		return '';
	}
	
	UI_tabBookingInfo.createPromoCodeInfoAlert = function(imageID){
		if(jsonBkgInfo.status == "CNF" && UI_reservation.jsonPromotionInfo != null && 
				(typeof UI_reservation.jsonPromotionInfo != 'undefined')){			
			promoAlert = '&nbsp;&nbsp;&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
			return promoAlert;
		}
		return '';
	}
	
	UI_tabBookingInfo.getAutoCancellationBalanceToPay = function(){					
		var id = this.id;
		UI_tabBookingInfo.autoCnxBalanceTOPayTot = jsonResPaymentSummary.balanceToPay;
		UI_tabBookingInfo.loadAutoCnxBalanceToPay = false;
		if(UI_tabBookingInfo.loadAutoCnxBalanceToPay){
			UI_tabBookingInfo.autoCnxAlertMap[id] = null;
			UI_tabBookingInfo.displayAutoCnxBalToPayToolTip(id);
			var submitData = {};
			submitData['version'] = jsonBkgInfo.version;
			submitData['resSegments'] = UI_reservation.jsonSegs;
			submitData["groupPNR"]	= jsonGroupPNR;
			submitData["pnr"] 	    = jsonPNR;
			submitData['resPax']	= UI_reservation.jsonPassengers;
			UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', 
				data:submitData, 
				url:"autoCancelBalanceToPayCalculation.action",
				async:true,
				success: function(response){
					if(response.success){
						UI_tabBookingInfo.loadAutoCnxBalanceToPay = false;
						UI_tabBookingInfo.autoCnxBalanceTOPayTot = response.strBalanceToPay;
						UI_tabBookingInfo.toolTipTrigger(id);						
					}
				},
				error:UI_commonSystem.setErrorStatus
			});
		} else {
			UI_tabBookingInfo.toolTipTrigger(id);
		}
	}
	
	UI_tabBookingInfo.toolTipTrigger = function(id){
		var rplTxtArr = [];
		rplTxtArr[0] = DATA_ResPro["initialParams"].defaultCurrencyCode;
		var balFloatVal = parseFloat(UI_tabBookingInfo.autoCnxBalanceTOPayTot);
		var msgStr = "";
		if(balFloatVal > 0){
			msgStr = top.arrError['XBE-ALERT-09'];
			rplTxtArr[0] += " " + balFloatVal;
			rplTxtArr[1] = UI_reservation.jsonAutoCnxAlert.content;
		} else {
			msgStr = top.arrError['XBE-ALERT-10'];
			rplTxtArr[0] += " " + (-1 * balFloatVal);
			rplTxtArr[1] = UI_reservation.jsonAutoCnxAlert.content;
		}
		
		var alertText = UI_tabBookingInfo.stringReplacer(msgStr, rplTxtArr, '#');		
		UI_tabBookingInfo.autoCnxAlertMap[id] = alertText;
		UI_tabBookingInfo.displayAutoCnxBalToPayToolTip(id);
	}
	
	UI_tabBookingInfo.isAutoCancellationExists = function(pnrSegs, flightRefNum, groupKey){
		for(var i=0; i<pnrSegs.length; i++){
			var pnrSeg = pnrSegs[i];
			if(pnrSeg.status == "CNF" && pnrSeg.flightSegmentRefNumber == flightRefNum 
					&& pnrSeg.interlineGroupKey==groupKey){
				return pnrSeg.alertAutoCancellation;
			}
		}
	}
	
	UI_tabBookingInfo.createPromoCodeAlertInfo = function(){
		var toolTipId = '#PromoCodeToolTip';
		var toolTipBodyId = '#PromoCodeToolTipBody';
		if(UI_tabBookingInfo.promoCodeAlertText == null
				|| UI_tabBookingInfo.promoCodeAlertText == ''){
			UI_tabBookingInfo.promoCodeAlertText = '';
			for ( var i = 0; i < UI_reservation.jsonPromotionInfo.length; i++) {
				var elem = UI_reservation.jsonPromotionInfo[i];				
				UI_tabBookingInfo.promoCodeAlertText += elem.content + '<br/>';
			}
		}
		
		_generateReservationToolTip(UI_tabBookingInfo.promoCodeAlertText, 'PromoCodeAlertImg', 0, 20, toolTipId, toolTipBodyId);
	}
	
	/*
	 * Flight Information
	 */
	UI_tabBookingInfo.fillFlightInfo = function(){
		/* Initialize the Details */
		$("#flightTemplete").find("tr").remove();
		var objFltDt = jsonFltDetails;		
		var intLen = objFltDt.length;
		
		var i                      = 0;
		var tblRow                 = null;
		var strClass               = "";
		var strOnClick             = "";
		var strAlertMouseOver      = "";
		var strAutoCnxAlertMouseOver = ""
		var strAlertMouseOut       = "";
		var strAlertOnClick        = "";
		var strFlexiMouseOver      = "";
		var strFlexiMouseOut       = "";
		var strDisplaySegmentInfo  = "";
		var strHideSegmentInfo     = "";
		var strOpenReturnMouseOver = "";
		var strOpenReturnMouseOut  = "";
		var strFltInfoDisplayMouseOver = "";
		var strFltInfoDisplayMouseOut = "";
		var strSegReturnValidityMouseOver = "";
		var strSegReturnValidityMouseOut = "";
		
		var strFlightNo = "";
		var strDepDate  = "";
		var strDepTime  = "";
		var arrivalTime = "";
		
		var pnrSegs = $.parseJSON(UI_reservation.jsonSegs);
		
		// To do : create dynamic img or using from html component
		var alertImage = "&nbsp;<img border='0' src='../images/AA169_no_cache.gif'/>";
		
		var openRTAdded = false;
		var openRTFltGroup = [];
		
		if (intLen > 0){
			do{
				if(objFltDt[i].subStatus !=null && objFltDt[i].subStatus == "EX") {
					i++;
					continue;
				}
				
				strClass = "";
				tblRow = document.createElement("TR");
				
				strOnClick             	 = UI_tabBookingInfo.fillFlightSegmentClick;
				strAlertMouseOver        = UI_tabBookingInfo.displayToolTip;
				strAutoCnxAlertMouseOver = UI_tabBookingInfo.getAutoCancellationBalanceToPay;
				strAlertMouseOut         = UI_tabBookingInfo.hideToolTip;
				strAlertOnClick          = UI_tabBookingInfo.displayWindow;
				strFlexiMouseOver        = UI_tabBookingInfo.displayFlexiToolTip;
				strFlexiMouseOut         = UI_tabBookingInfo.hideFlexiToolTip;
				strBundledMouseOver		 = UI_tabBookingInfo.displayBundledFareToolTip;
				strBundlesMouseOut 		 = UI_tabBookingInfo.hideBundledFareToolTip,
				strDisplaySegmentInfo    = UI_tabBookingInfo.displaySegmentInfoToolTip;
				strHideSegmentInfo       = UI_tabBookingInfo.hideSegmentInfoToolTip;
				strOpenReturnMouseOver   =  UI_tabBookingInfo.displayOPRTSegmentInfoToolTip;
				strOpenReturnMouseOut    =  UI_tabBookingInfo.hideOPRTSegmentInfoToolTip;

				if(objFltDt[i].ticketValidTill!=null && objFltDt[i].ticketValidTill!="") {
					strSegReturnValidityMouseOver = UI_tabBookingInfo.displayReturnValidityInfoToolTip;
					strSegReturnValidityMouseOut = UI_tabBookingInfo.hideReturnValidityInfoToolTip;	
				}else{
					strSegReturnValidityMouseOver = "";
					strSegReturnValidityMouseOut = "";
				}
				
				if(DATA_ResPro.initialParams.fltStopOverDurInfoToolTipEnabled){
					strFltInfoDisplayMouseOver =  UI_tabBookingInfo.displayFlightAddInfoToolTip;
					strFltInfoDisplayMouseOut  =  UI_tabBookingInfo.hideFlightAddInfoToolTip;
				}				
				
				if(objFltDt[i].openReturnSegment==true){
					 strFlightNo = "";
					 strDepDate  = "";
					 strDepTime  = "";
					 arrivalTime = "";
					 strFltInfoDisplayMouseOver = "";
					 strFltInfoDisplayMouseOut = ""; 
				}
				else{
					 strFlightNo = objFltDt[i].flightNo;
					 strDepDate  = objFltDt[i].departureDate;
					 strDepTime  = objFltDt[i].departureTime;
					 arrivalTime = objFltDt[i].arrivalTime;
				}
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].orignNDest, id:"td_" + i + "_0", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", click:strOnClick}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:strFlightNo, id:"td_" + i + "_1", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick,mouseOver:strFltInfoDisplayMouseOver, mouseOut:strFltInfoDisplayMouseOut}));				
				
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].airLine, id:"td_" + i + "_2", css:UI_commonSystem.strDefClass + " txtAirLineColor", align:"center", bold:true, textCss:"", click:strOnClick}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:strDepDate, id:"td_" + i + "_3", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick,mouseOver:strSegReturnValidityMouseOver, mouseOut:strSegReturnValidityMouseOut}));
				
				for(var j=0;j<objFltDt.length;j++){

				    var dD = objFltDt[j].departureDate;
				    
					if(UI_tabBookingInfo.FirstNonFlwnfltOutDate == "" && objFltDt[j].status != 'CNX' && UI_tabBookingInfo.compareDate(dD)) {
						UI_tabBookingInfo.FirstNonFlwnfltOutDate = objFltDt[j].departureDate;
					} else if(UI_tabBookingInfo.FirstNonFlwnfltOutDate != ""
							&& UI_tabBookingInfo.convertDeptDate(UI_tabBookingInfo.FirstNonFlwnfltOutDate) > UI_tabBookingInfo.convertDeptDate(objFltDt[j].departureDate)
							&& objFltDt[j].status != 'CNX' && UI_tabBookingInfo.compareDate(dD)) {
							UI_tabBookingInfo.FirstNonFlwnfltOutDate = objFltDt[j].departureDate;
					}
					
					if(objFltDt[j].status != 'CNX' && UI_tabBookingInfo.compareDate(objFltDt[j].arrivalDate)) {
						UI_tabBookingInfo.lastFltArrivalDate = objFltDt[j].arrivalDate;
					}
				}
				
				if(UI_tabBookingInfo.fltOutDate == "" && objFltDt[i].status != 'CNX') {
					UI_tabBookingInfo.fltOutDate = objFltDt[i].departureDate;
				} else if(UI_tabBookingInfo.fltOutDate != ""
						&& UI_tabBookingInfo.convertDeptDate(UI_tabBookingInfo.fltOutDate) > UI_tabBookingInfo.convertDeptDate(objFltDt[i].departureDate)
						&& objFltDt[i].status != 'CNX') {
						UI_tabBookingInfo.fltOutDate = objFltDt[i].departureDate;
				}
				
				if(UI_tabBookingInfo.fltOutDate != "" && UI_tabBookingInfo.fltOutDate.indexOf(" ") > 0){
					UI_tabBookingInfo.fltOutDate = UI_tabBookingInfo.fltOutDate.split(" ")[1];
					
				}
				
				if(UI_tabBookingInfo.FirstNonFlwnfltOutDate !="" && UI_tabBookingInfo.FirstNonFlwnfltOutDate.indexOf(" ") > 0){
					UI_tabBookingInfo.FirstNonFlwnfltOutDate = UI_tabBookingInfo.FirstNonFlwnfltOutDate.split(" ")[1]
				}
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].departureText, id:"td_" + i + "_4", css:UI_commonSystem.strDefClass, align:"center", bold:true, click:strOnClick}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].departure, id:"td_" + i + "_5", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:strDepTime, id:"td_" + i + "_6", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick}));
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].arrivalText, id:"td_" + i + "_7", css:UI_commonSystem.strDefClass, align:"center", bold:true, click:strOnClick}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].arrival, id:"td_" + i + "_8", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:arrivalTime, id:"td_" + i + "_9", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick}));
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].type, id:"td_" + i + "_10",width : "50px", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].cabinClass, id:"td_" + i + "_11",width : "75px", css:UI_commonSystem.strDefClass, align:"center", click:strOnClick}));
					
				
				if(jsonSegmentWiseBundledFareDetails[objFltDt[i].flightSegmentRefNumber] !=null){
					if(objFltDt[i].status != 'CNX'){
						if(UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey] > 0){
							tblRow.appendChild(UI_commonSystem.createCell({desc:"" + '&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_N_no_cache.gif"/>', 
								id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status),
								rowSpan:UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey], 
								bold:true, click:strOnClick,mouseOver:strBundledMouseOver, mouseOut:strBundlesMouseOut, width:"30"}));
							
							UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey] = -1;
						}
					}else{
						tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
					}
					
				}			
				// Flexi data
				else if (jsonFlexiDetails != null && jsonFlexiDetails.length > 0) {
					for (var j = 0; j < jsonFlexiDetails.length; j++ ) {
						if(objFltDt[i].status != 'CNX'){							
							if(UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey] > 0){
								if (objFltDt[i].flightSegmentRefNumber == jsonFlexiDetails[j].flightSegmantRefNumber) {
									var imageID = "img_" + i + "_12";								
									tblRow.appendChild(UI_commonSystem.createCell({desc:"" + '&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_N_no_cache.gif"/>', 
										id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status),
										rowSpan:UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey], 
										bold:true, click:strOnClick,mouseOver:strFlexiMouseOver, mouseOut:strFlexiMouseOut, width:"30"}));
									UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey] = -1;
									break;
								// add bus image to the flexi fare
								} else if (objFltDt[i].surfaceStation != null) {
									var imageID = "img_" + i + "_12";
									tblRow.appendChild(UI_commonSystem.createCell({desc:"" + '&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_B_no_cache.gif"/>', 
										id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status),
										rowSpan:UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey], 
										bold:true, click:strOnClick,mouseOver:strDisplaySegmentInfo, mouseOut:strHideSegmentInfo, width:"30"}));
									UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey] = -1;
									break;
								} else if (jsonFlexiDetails[j].alertTO == null || jsonFlexiDetails[j].alertTO.length == 0){
									tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
									break;
								} else if(j == (jsonFlexiDetails.length -1)){
									tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
									break;
								}
							} else {
								//tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
								break;
							}
						} else {
							tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
							break;
						}
					}
				}else if (jsonAdditionalSegInfo != null && jsonAdditionalSegInfo.length > 0) {
					for (var j = 0; j < jsonAdditionalSegInfo.length; j++ ) {
						if(objFltDt[i].status != 'CNX'){
							if(UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey] > 0){
								if (objFltDt[i].flightSegmentRefNumber == jsonAdditionalSegInfo[j].flightSegmantRefNumber) {
									var imageID = "img_" + i + "_12";
									tblRow.appendChild(UI_commonSystem.createCell({desc:"" + '&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_B_no_cache.gif"/>', 
										id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status),
										rowSpan:UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey], 
										bold:true, click:strOnClick,mouseOver:strDisplaySegmentInfo, mouseOut:strHideSegmentInfo, width:"30"}));
									UI_tabBookingInfo.flightSegmentGroup[objFltDt[i].interLineGroupKey] = -1;
									break;
								} else if (jsonAdditionalSegInfo[j].alertTO == null || jsonAdditionalSegInfo[j].alertTO.length == 0) {
									tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
									break;
								} else if(j == (jsonAdditionalSegInfo.length -1)) {
									tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
									break;
								}
							} else {
								tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
								break;
							}
						} else {
							tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
							break;
						}
					}
				}else {					
					tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_12", css:UI_commonSystem.strDefClass, align:"center"}));
				}
				
				// Insert Image and url
				// To Do: Remove html
				if (jsonAlertDetails.length > 0 && !UI_reservation.isCsOCPnr) {
					for (var a = 0; a < jsonAlertDetails.length; a++ ) {						
						if (objFltDt[i].flightSegmentRefNumber == jsonAlertDetails[a].flightSegmantRefNumber && objFltDt[i].status != 'CNX') {
							var imageID = "img_" + i + "_13";
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].displayStatus + '&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_no_cache.gif"/>', id:"td_" + i + "_13", css:UI_commonSystem.strDefClass, align:"left", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status), bold:true, click:strOnClick,mouseOver:strAlertMouseOver, mouseOut:strAlertMouseOut}));
							var strClearAlert = "Action Alert";
							var urlID = "url_" + i + "_13";
							var url =  '<a href="javascript:void(0)" id="'+ urlID +'"  return false;" onclick="UI_tabBookingInfo.displayWindow(this.id)"><font class="mandatory"><u>'+ strClearAlert +'</u></font></a>';
							tblRow.appendChild(UI_commonSystem.createCell({desc:url, id:"td_" + i + "_14", css:UI_commonSystem.strDefClass, align:"center",click:strOnClick}));	
							break;
						} else if (jsonAlertDetails[a].alertTO == null || jsonAlertDetails[a].alertTO.length == 0){					
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].displayStatus, id:"td_" + i + "_13", css:UI_commonSystem.strDefClass, align:"left", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status), bold:true, click:strOnClick}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_14", css:UI_commonSystem.strDefClass, align:"center"}));	
							break;
						} else if(a == (jsonAlertDetails.length -1)){
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].displayStatus, id:"td_" + i + "_13", css:UI_commonSystem.strDefClass, align:"left", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status), bold:true, click:strOnClick}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_14", css:UI_commonSystem.strDefClass, align:"center"}));
						}
					}
				} else {
					tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].displayStatus, id:"td_" + i + "_13", css:UI_commonSystem.strDefClass, align:"left", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status), bold:true, click:strOnClick}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_14", css:UI_commonSystem.strDefClass, align:"center"}));
				}
				
				/** Tool tip for open return segment details * */
				if(jsonOpenReturnSegInfo!=null && jsonOpenReturnSegInfo.length>0){
					if(!openRTAdded){						
						openRTFltGroup = UI_tabBookingInfo.groupFlightSegmentsForOpenRTAlerts();
					}
					for (var m = 0; m < jsonOpenReturnSegInfo.length; m++ ) {
						if(objFltDt[i].status != 'CNX'){	
							if(openRTFltGroup[objFltDt[i].interLineGroupKey] > 0){
								if (objFltDt[i].flightSegmentRefNumber == jsonOpenReturnSegInfo[m].flightSegmantRefNumber) {
									openRTAdded = true;
									var imageID = "img_" + i + "_15";								
									tblRow.appendChild(UI_commonSystem.createCell({desc:"" + '&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>', 
										id:"td_" + i + "_15", css:UI_commonSystem.strDefClass, align:"center", textCss:UI_commonSystem.getStatusColor(objFltDt[i].status),
										rowSpan:openRTFltGroup[objFltDt[i].interLineGroupKey], 
										bold:true, click:strOnClick,mouseOver:strOpenReturnMouseOver, mouseOut:strOpenReturnMouseOut, width:"30"}));
									openRTFltGroup[objFltDt[i].interLineGroupKey] = -1;
									break;
								} else if (jsonOpenReturnSegInfo[m].alertTO == null || jsonOpenReturnSegInfo[m].alertTO.length == 0){
									tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_15", css:UI_commonSystem.strDefClass, align:"center"}));
									break;
								} else if(m == (jsonOpenReturnSegInfo.length -1)){
									tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_15", css:UI_commonSystem.strDefClass, align:"center"}));
									break;
								}
							}
						} else {
							tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_15", css:UI_commonSystem.strDefClass, align:"center"}));
							break;
						}
					}
				 }
				 else{
					 tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_15", css:UI_commonSystem.strDefClass, align:"center"}));
				 }
				
				var isCnxAlertExists = UI_tabBookingInfo.isAutoCancellationExists(pnrSegs, objFltDt[i].flightRefNumber, objFltDt[i].interLineGroupKey);
				var cnxAlert = "";
				if(isCnxAlertExists){
					var imageID = "segImg_" + i + "_16";
					cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
					tblRow.appendChild(UI_commonSystem.createCell({desc:cnxAlert, id:"td_" + i + "_16", 
						css:UI_commonSystem.strDefClass, align:"left", 
						textCss:UI_commonSystem.getStatusColor(objFltDt[i].status), 
						bold:true, click:strOnClick,
						mouseOver:strAutoCnxAlertMouseOver, 
						mouseOut:strAlertMouseOut}));
										
				} else {
					tblRow.appendChild(UI_commonSystem.createCell({desc:"", id:"td_" + i + "_16", css:UI_commonSystem.strDefClass, align:"center"}));
				}
								
				$("#flightTemplete").append(tblRow);
				
				i++;
			}while(--intLen);
		}
	}
	
	/*
	 * Flight Segment Click
	 */
	UI_tabBookingInfo.fillFlightSegmentClick = function(){
		var arrIDData = this.id.split("_");
		var selectedGroup = jsonFltDetails[arrIDData[1]].interLineGroupKey;
		var isCnxSegment = false;
		var isOpenRetSegment =  false;
		var segInfo = $.parseJSON(UI_reservation.jsonSegs);
		
		var segInfo = $.parseJSON(UI_reservation.jsonSegs);
		
		UI_tabBookingInfo.selectedFlightSeg.clear();
		$("#btnCanSegment").disable();
		var cancelAllowed = true;
		var modByDateAllowed = true;
		var modByRouteAllowed = true;
		$("#btnCreateAlert").enable();
		$("#btnClearAllAlerts").enable();
		for(var j =0 ;j < jsonFltDetails.length; j++) {
			if(selectedGroup == jsonFltDetails[j].interLineGroupKey) {				
				if(jsonFltDetails[j].status == 'CNX' || (!isGdsPnr && !jsonFltDetails[j].isCancellable && !jsonFltDetails[j].isAllowModifyByDate && !jsonFltDetails[j].isAllowModifyByRoute && !jsonFltDetails[j].openReturnSegment) ){
					isCnxSegment = true;
				}	
				if(jsonFltDetails[j].openReturnSegment == true ){					
					isOpenRetSegment = true;
				}
				else{
					isOpenRetSegment = false;
				}
				
				//Enforcing most restricted behaviour in enabling buttons
				cancelAllowed = cancelAllowed && jsonFltDetails[j].isCancellable;
				modByDateAllowed = modByDateAllowed && jsonFltDetails[j].isAllowModifyByDate;
				modByRouteAllowed = modByRouteAllowed && jsonFltDetails[j].isAllowModifyByRoute;
				
				if(!isCnxSegment) {
					if(UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef != null) {
						UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef += "," + jsonFltDetails[j].flightSegmentRefNumber+"$"+jsonFltDetails[j].interLineGroupKey;
						// segment modification
						UI_tabBookingInfo.selectedFlightSeg.modifyByDate = jsonFltDetails[j].isAllowModifyByDate;
						UI_tabBookingInfo.selectedFlightSeg.modifyByRoute = jsonFltDetails[j].isAllowModifyByRoute;
					} else {
						UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef = jsonFltDetails[j].flightSegmentRefNumber+"$"+jsonFltDetails[j].interLineGroupKey;
						// segment modification
						UI_tabBookingInfo.selectedFlightSeg.modifyByDate = jsonFltDetails[j].isAllowModifyByDate;
						UI_tabBookingInfo.selectedFlightSeg.modifyByRoute = jsonFltDetails[j].isAllowModifyByRoute;
					}
					
					if(isOpenRetSegment){
						$("#btnConfirmOpenRt").show();
						
						for(var k=0;k< segInfo.length;k++){
							if(segInfo[k].openRetConfirmBefore !=null && segInfo[k].flightSegmentRefNumber == jsonFltDetails[j].flightRefNumber && segInfo[k].status != 'CNX'){
								var openRetConfirmBefore = segInfo[k].openRetConfirmBefore.split(" ")[0];
								var openRetConfirmBeforeDate= stringToDate(openRetConfirmBefore);
								var currentDate = new Date();
								if(openRetConfirmBeforeDate.getTime() < currentDate.getTime()){
									isConfirmDateExpired = true;
									break;
								}
								
							}
						}
						if(isConfirmDateExpired){
							$("#btnConfirmOpenRt").disable();
						}else{
							$("#btnConfirmOpenRt").enable();
						}
						
						$("#btnModSegment").disable();
						if(jsonFltDetails[j].isCancellable){
							$("#btnCanSegment").enable();
						}	
						$("#btnAddGroundSegment").disable();
					} else {		
					
						if(cancelAllowed){
							$("#btnCanSegment").enable();
						} else {
							$("#btnCanSegment").disable();
						}						
						//segment modification enabling
						if(modByDateAllowed || modByRouteAllowed){
							$("#btnModSegment").enable();
						} else {
							$("#btnModSegment").disable();
						}
						
						if(!(jsonBkgInfo.status == 'OHD')){
							UI_tabBookingInfo.isFlexiBooking(jsonFltDetails[j].flightSegmentRefNumber);
						}
						UI_tabBookingInfo.addGroundSegmentSelectData.clear();
						if(jsonFltDetails[j].groundStationAddAllowed){
							$("#btnAddGroundSegment").enable();
							UI_tabBookingInfo.addGroundSegmentSelectData.selectedOnD = jsonFltDetails[j].orignNDest;
						}else {
							$("#btnAddGroundSegment").disable();						
						}
						UI_tabBookingInfo.isFlexiBooking(jsonFltDetails[j].flightSegmentRefNumber);
						
						$("#btnConfirmOpenRt").disable();
						$("#btnConfirmOpenRt").hide();
					}
					if(jsonFltDetails[j].status == 'WL'){
						$("#selWLPriority").enable();
						$("#btnWLPriority").enable();
						//Add Select elements
						var arrPriority = new Array();
						for (var pi =  0; pi < jsonFltDetails[j].segmentWaitListedPriorities.length; pi++) {
							arrPriority[pi] = new Array(jsonFltDetails[j].segmentWaitListedPriorities[pi], jsonFltDetails[j].segmentWaitListedPriorities[pi])
						}
						$("#selWLPriority").empty();
						$("#selWLPriority").fillDropDown({dataArray:arrPriority , keyIndex:0 , valueIndex:1, firstEmpty:true });
						$("#selWLPriority").val(jsonFltDetails[j].waitListedPriority);
						UI_tabBookingInfo.WLflightSegId = jsonFltDetails[j].flightSegId;
					}
					
				} else {
					$("#btnCanSegment").disable();
					$("#btnModSegment").disable();
					$("#btnConfirmOpenRt").disable();
					$("#btnConfirmOpenRt").hide();
					$("#btnAddGroundSegment").disable();	
				}
			} 
			if(!isCnxSegment) {
				for (var i =  0; i < 14; i++) {
					if(selectedGroup == jsonFltDetails[j].interLineGroupKey) {
						$("#td_" + j + "_" + i).removeClass('rowUnSelected');
						$("#td_" + j + "_" + i).addClass('rowSelected');
					} else {
                        $("#td_" + j + "_" + i).removeClass('rowSelected');
						$("#td_" + j + "_" + i).addClass('rowUnSelected');
					}
				}
			} else if(isGdsPnr) {
				if(jsonFltDetails[j].status != 'CNX') {
					for (var i =  0; i < 14; i++) {
						$("#td_" + j + "_" + i).removeClass('rowUnSelected');
						$("#td_" + j + "_" + i).addClass('rowSelected');					
					}					
				}
			} else {
				for (var i =  0; i < 14; i++) {
					$("#td_" + j + "_" + i).removeClass('rowSelected');
					$("#td_" + j + "_" + i).addClass('rowUnSelected');					
				}
			}			
		}		
	}
	
	/*
	 * Flight segment grouping
	 */
	UI_tabBookingInfo.groupFlightSegments = function(){
		var flightGroup = -1;		
		for(var j =0 ;j < jsonFltDetails.length; j++) {
			if(flightGroup == -1 || flightGroup != jsonFltDetails[j].interLineGroupKey) {
				flightGroup = jsonFltDetails[j].interLineGroupKey;
				UI_tabBookingInfo.flightSegmentGroup[flightGroup] = 1;
			} else{
				UI_tabBookingInfo.flightSegmentGroup[flightGroup] = UI_tabBookingInfo.flightSegmentGroup[flightGroup] + 1;
			}
		}
	}
	
	UI_tabBookingInfo.groupFlightSegmentsForOpenRTAlerts = function(){
		var fltSegGroup = [];
		var flightGroup = -1;		
		for(var j =0 ;j < jsonFltDetails.length; j++) {
			if(flightGroup == -1 || flightGroup != jsonFltDetails[j].interLineGroupKey) {
				flightGroup = jsonFltDetails[j].interLineGroupKey;
				fltSegGroup[flightGroup] = 1;
			} else{
				fltSegGroup[flightGroup] = fltSegGroup[flightGroup] + 1;
			}
		}
		
		return fltSegGroup;
	}
	
	/*
	 * Validates user notes text area's max length. Allows up to 255 characters
	 */
	UI_tabBookingInfo.userNoteOnChange = function(){	
	     var max = 255;  
         if($("#txtUNotes").val().length >= max){
        	alert("User notes character limit is exceeds");
            $("#txtUNotes").val($("#txtUNotes").val().substr(0,max));  
        }    
	}
	
	UI_tabBookingInfo.endosmentNoteOnChange = function(){	
	     var max = 500;  
        if($("#txtEndosment").val().length >= max){
       	alert("Endosment notes character limit is exceeds");
           $("#txtEndosment").val($("#txtEndosment").val().substr(0,max));  
       }    
	}
	
	/*
	 * Fill User Notes
	 */
	UI_tabBookingInfo.fillEndorsementNotes = function(){
		if (jsonEndorsements == null || jsonEndorsements == "null"){
			jsonEndorsements = "";
		}
		$("#txtEndosment").val(jsonEndorsements);
	}
	
	/*
	 * Fill Endosment
	 */
	UI_tabBookingInfo.fillUserNotes = function(){
		$("#txtUNotes").val(jsonUserNotes);
	}
	
	/*
	 * Price Break down
	 */ 
	UI_tabBookingInfo.buildPaxPriceBreakDown = function(){
		$("#trBalance").show();
		var goQuoServicesAsSSR = jsonGoQuoServicesAsSSR;
		var totalTicketAmt;
		if (goQuoServicesAsSSR) {
			totalTicketAmt = parseFloat(jsonPaxPriceSummary.ticketPrice);
		} else {
			totalTicketAmt = parseFloat(jsonPaxPriceSummary.ticketPrice)+parseFloat(jsonPaxPriceSummary.totalGOQUOAmount);
		}
		$("#trTotal").show();
		if((!(jsonBkgInfo.status == 'OHD')) && UI_reservation.OhdPayAmount == "" && DATA_ResPro.initialParams.goquoValid) {
			$("#divPBTotAmt").html(totalTicketAmt.toFixed(2));
			$("#trBalance").hide();
		} else if (DATA_ResPro.initialParams.goquoValid) {
			$("#divPBTotAmtCurr").html(UI_reservation.OhdPayAmount);
			$("#divPBTotAmtBase").html(jsonPaxPriceSummary.ticketPrice);
			$("#divPBTotAmtOhd").html(jsonPaxPriceSummary.ticketPrice);
			$("#trTotal").hide();
		} else {
			$("#divPBGrandTotAmtOhd").html(totalTicketAmt.toFixed(2));
		}
		
		var lblTotDisc = top.arrError['XBE-LABEL-01'];
		var discountSign = '-';
		if(UI_reservation.isCreditPromotion){
			discountSign = '';
			lblTotDisc = top.arrError['XBE-LABEL-02'];
			$("#trFareDiscountCredit").show();
			$("#trFareDiscount").hide();
		} else {
			$("#trFareDiscount").show();
			$("#trFareDiscountCredit").hide();
		}
		
		$(".divDiscLabel").html(lblTotDisc);
		
		if(parseFloat(jsonPaxPriceSummary.totalDiscount) > 0){			
			$(".divPBFareDiscount").html(discountSign + jsonPaxPriceSummary.totalDiscount);
		} else {
			$(".divPBFareDiscount").html(jsonPaxPriceSummary.totalDiscount);
		}
		
		/* Initialize the Details */
		$("#paxPriceTemplete").find("tr").remove();

		/* Show goquo ancillary amount */
		if (DATA_ResPro.initialParams.goquoValid && DATA_ResPro.initialParams.goquoBooking) {
			$(".divGoquoAnciAmount").html(jsonPaxPriceSummary.totalGOQUOAmount);
		} else {
			$("#trGoquoAnciAmount").hide();
		}
		
		var objPaxDt = jsonPaxPrice;
		var intLen = objPaxDt.length;
		var i = 0;
		var tblRow = null;
		var strClass = "";

		if (intLen > 0){
			do{
				strClass = "";
				tblRow = document.createElement("TR");
				var template = '';
				
				if (DATA_ResPro.initialParams.goquoValid) {
					template = "#paxPriceTemplete";
					$("#divTotal").parent().hide();
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].paxType, css:UI_commonSystem.strDefClass + " thinBorderL rowHeight"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].noPax, css:UI_commonSystem.strDefClass, align:"right"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].fare, css:UI_commonSystem.strDefClass, align:"right"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].tax, css:UI_commonSystem.strDefClass, align:"right"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].sur, css:UI_commonSystem.strDefClass, align:"right"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].other, css:UI_commonSystem.strDefClass, align:"right"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].total, css:UI_commonSystem.strDefClass, align:"right"}));
				} else {
					template = "#paxCountTemplete";
					$("#divPriceSummary").parent().hide();
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].paxType, css:UI_commonSystem.strDefClass + " thinBorderL rowHeight"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[i].noPax, css:UI_commonSystem.strDefClass, align:"right"}));
				}
				
				$(template).append(tblRow);
				i++;
			}while(--intLen);
		}
		
//		if (!DATA_ResPro.initialParams.goquoValid) {
//			$("#tdFareHD").hide();
//			$("#tdTaxHD").hide();
//			$("#tdSurChgHD").hide();
//			$("#tdOtherHD").hide();
//			$("#tdTotalHD").prop('colspan', 2);
//			$("#tdPaxNoHD").prop('colspan', 2);
//			$("#tdBalanceLbl").prop('colspan', 1);
//			$("#tdFareDiscountAmt").prop('colspan', 2);
//			$("#tdTotalAmt").prop('colspan', 2);
//			$("#tdDiscountCreditAmt").prop('colspan', 2);
//			$("#tdFareDiscountLbl").prop('colspan', 3);
//			$("#tdTotalLbl").prop('colspan', 3);
//			$("#tdDiscountCreditLbl").prop('colspan', 3);
//			$("#tdPBTotAmtBaseLbl").prop('colspan', 1);
//			$("#tdTPBTotAmtOhdLbl").prop('colspan', 1);
//			$("#trGoquoAnciAmount").hide();	
//		}
	}	
	
	/*



	 * Fill Drop down data for the Passenger tab
	 */
	
	UI_tabBookingInfo.fillDropDownData = function(){
		$("#selITNLang").fillDropDown({dataArray:top.jsonItnLang, keyIndex:"id", valueIndex:"desc", firstEmpty:false});
		// fill booking type drop down
		$("#selBookingCategory").fillDropDown({dataArray:top.arrBookingCategories, keyIndex:0 , valueIndex:1, firstEmpty:false });
		
		$("#selItineraryFareMask").fillDropDown({dataArray:top.arrItineraryFareMaskOptions, keyIndex:0 , valueIndex:1, firstEmpty:false });

		$("#selOriginCountryOfCall").fillDropDown({
			dataArray: top.arrCountry,
			keyIndex: 0,
			valueIndex: 1,
			firstEmpty: true
		});


		$("#selSSRCode").fillDropDown( { dataArray:top.arrSSR , keyIndex:0 , valueIndex:1, firstEmpty:true });
		
		if(DATA_ResPro["systemParams"] != null){
			$("#selITNLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
		} else {
			$("#selITNLang").val('en');
		}
			
		//FIXME introduce an app param
//		if(jsonContactInfo != null && jsonContactInfo.preferredLang != null && jsonContactInfo.preferredLang != ""){
//			$("#selITNLang").val(jsonContactInfo.preferredLang);
//		} else {
//			$("#selLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
//		}
			
		if (DATA_ResPro.initialParams.transferOwnershipToAnyCarrier){
			$("#selTransfer").fillDropDown({dataArray:top.arrAllTransferTravelAgents , keyIndex:0 , valueIndex:1, firstEmpty:true });			
		}else if(DATA_ResPro.initialParams.transferOwnership){
			$("#selTransfer").fillDropDown({dataArray:top.arrOwnTravelAgents , keyIndex:0 , valueIndex:1, firstEmpty:true });
		}
		
		
		if (DATA_ResPro.initialParams.extendOnhold){
			var arrTime = new Array();
			var strFormat = "";
			
			var r = 0;
			for(var i = 0 ; i < 24 ; i++){
				
				for(var j = 0; j < 2; j++ ){
					strFormat ;
					if(j == 0){
						if (i<10) {strFormat = "0" + i + ":00 Hrs";}else{strFormat = i + ":00 Hrs";}
					}else{
						if (i<10) {strFormat = "0" + i + ":30 Hrs";}else{strFormat = i + ":30 Hrs";}
					}
					arrTime[r] = new Array(r, strFormat);
					r++;
				}
				
			
			}
			$("#selExtend").fillDropDown({dataArray:arrTime, keyIndex:0, valueIndex:1, firstEmpty:true});
		}else{
			$("#selExtend").hide();
			$("#btnExtend").hide();
		}
		
		UI_tabBookingInfo.fillDropFromArray();
	}
	/*
	 * Temporary This method is introduced Once data retrevied from server as
	 * json string remove this method
	 */
	UI_tabBookingInfo.fillDropFromArray = function(){
		var i = 0;
		
		// Eg : jsonPaxTypes = ":;Mr:Mr;Ms:Ms;Mrs:Mrs;Master:Master;Miss:Miss";
		var intLen = top.arrTitle.length;
		jsonPaxTypes = ":";				
		if (intLen > 0){
			i = 0;
			do{
				jsonPaxTypes += ";" + top.arrTitle[i][0] + ":" + top.arrTitle[i][1];
				i++;
			}while(--intLen);
		}
		
		// Eg : jsonNationality = ":;LK:Sri Lankan;AE:Emirati";
		intLen = top.arrNationality.length;
		jsonNationality = ":";									
		if (intLen > 0){
			i = 0;
			do{
				jsonNationality += ";" + top.arrNationality[i][0] + ":" + top.arrNationality[i][1];
				i++;
			}while(--intLen);
		}
		jsonTravellingWith = UI_tabBookingInfo.populateJsonTravellingWith(jsonPaxAdtObj);
		
	}
	
	UI_tabBookingInfo.populateJsonTravellingWith = function(paxAdtObj){
		var intLen = paxAdtObj.paxAdults.length;
		var jsonTravellingWith = ":"; // ":;1:1;2:2";
		if (intLen > 0){
			var i = 1;
			$.each(paxAdtObj.paxAdults, function(){
				if (paxAdtObj.paxAdults[i - 1].displayAdultType == UI_commonSystem.strAdtCode){
					jsonTravellingWith += ";" + i  + ":" + i;
					i++;
				}
			});
		}
		return jsonTravellingWith;
	}
	
	UI_tabBookingInfo.showHideADGrid = function(){
		var objJPax = jsonPaxAdtObj.paxAdults;
		var paxCount = objJPax.length;
		var i = 0;
		if(paxCount > 0){
			do {
				var paxConfig = ""
				if (objJPax[i].displayAdultType == "AD") {
					paxConfig = paxConfigAD;
				} else if(objJPax[i].displayAdultType == "CH"){
					paxConfig = paxConfigCH;
				}
				
				if(paxConfig.title.xbeVisibility){
					$("#" + (i + 1) + "_displayAdultTitle").show();
				} else {
					$("#" + (i + 1) + "_displayAdultTitle").hide();
				}
				
				if(paxConfig.firstName.xbeVisibility){
					$("#" + (i + 1) + "_displayAdultFirstName").show();
				} else {
					$("#" + (i + 1) + "_displayAdultFirstName").hide();
				}
				
				if(paxConfig.lastName.xbeVisibility){
					$("#" + (i + 1) + "_displayAdultLastName").show();
				} else {
					$("#" + (i + 1) + "_displayAdultLastName").hide();
				}
				
				if(paxConfig.nationality.xbeVisibility){
					$("#" + (i + 1) + "_displayAdultNationality").show();
				} else {
					$("#" + (i + 1) + "_displayAdultNationality").hide();
				}
				
				if(paxConfig.dob.xbeVisibility){
					$("#" + (i + 1) + "_displayAdultDOB").show();
				} else {
					$("#" + (i + 1) + "_displayAdultDOB").hide();
				}
				
				if(paxConfig.dob.xbeVisibility){
					$("#" + (i + 1) + "_displayFfid").show();
				} else {
					$("#" + (i + 1) + "_displayFfid").hide();
				}				
					
			} while(paxCount--)
		}
	}
	
	/*
	 * Grid Constructor
	 */
	UI_tabBookingInfo.constructGridAdult = function(inParams){
		// construct grid
		var blnTitle = true;
		var blnFName = true;
		var blnLName = true;
		var blnNationality = true;
		var blnDOB = true;
		var blnFFID = true;
		var blnAmountsACC = true;
		var blnMCO = true;
		var ffidEditable = true;
		var strTitleMand = "";
		var strFNameMand = "";
		var strLNameMand = "";
		var strNationalityMand = "";
		var strDOBMand = "";
		
		if(!$.isEmptyObject(paxConfigAD)){			
			if(paxConfigAD.title.xbeVisibility ||
					paxConfigCH.title.xbeVisibility){
				blnTitle = false;
			}
			
			if(paxConfigAD.firstName.xbeVisibility ||
					paxConfigCH.firstName.xbeVisibility){
				blnFName = false;
			}
			
			if(paxConfigAD.lastName.xbeVisibility ||
					paxConfigCH.lastName.xbeVisibility){
				blnLName = false;
			}
			
			if(paxConfigAD.nationality.xbeVisibility ||
					paxConfigCH.nationality.xbeVisibility){
				blnNationality = false;
			}
			
			if(paxConfigAD.dob.xbeVisibility ||
					paxConfigCH.dob.xbeVisibility){
				blnDOB = false;
			}
			
			if(paxConfigAD.ffid.xbeVisibility ||
					paxConfigCH.ffid.xbeVisibility){
				blnFFID = false;
			}
						
			if(paxConfigAD.title.xbeMandatory){
				strTitleMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigAD.firstName.xbeMandatory){
				strFNameMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigAD.lastName.xbeMandatory){
				strLNameMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigAD.nationality.xbeMandatory){
				strNationalityMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigAD.dob.xbeMandatory){
				strDOBMand = UI_commonSystem.strTxtMandatory;
			}
			
			if (DATA_ResPro.initialParams.goquoValid) {
				blnAmountsACC = false;
			}
			
			if (DATA_ResPro.initialParams.allowViewMCO && !blnAmountsACC) {
				blnMCO = false ;
			}
		}
		
		if (DATA_ResPro.initialParams.enableFareRuleNCC) {
			ffidEditable = false;
		} else {
			ffidEditable = true;
		}
		
		/*
		 * if (adultAgeMand == 'true' || chaildAgeMand == 'true'){ strDOBMand =
		 * UI_commonSystem.strTxtMandatory; }
		 */
		
		$(inParams.id).jqGrid({
			height: 30,
			width: 910,
			colNames:[ '', geti18nData('non','Reference'), geti18nData('BookingInfo_PassengerInfo_lbl_Type','Type'), geti18nData('BookingInfo_PassengerInfo_lbl_Title','Title') + strTitleMand, geti18nData('BookingInfo_PassengerInfo_lbl_FirstName','First Name') + strFNameMand, geti18nData('BookingInfo_PassengerInfo_lbl_LastName','Last Name') + strLNameMand, geti18nData('BookingInfo_PassengerInfo_lbl_Nationality','Nationality') + strNationalityMand, geti18nData('BookingInfo_PassengerInfo_lbl_DateOfBirth','Date of Birth') + strDOBMand, geti18nData('BookingInfo_PassengerInfo_lbl_AirewardsID','Air Rewards ID'), geti18nData('BookingInfo_PassengerInfo_lbl_TotalCharge','Tot Chg.'), geti18nData('BookingInfo_PassengerInfo_lbl_TotalPay','Tot Pay'),geti18nData('BookingInfo_PassengerInfo_lbl_Balance','Bal.'), geti18nData('BookingInfo_PassengerInfo_lbl_Account','ACC'), geti18nData('BookingInfo_PassengerInfo_lbl_eTicket','E-TCK'), geti18nData('BookingInfo_PassengerInfo_lbl_MCO','MCO'), geti18nData('History_lbl_Date','PaxSequence'),geti18nData('BookingInfo_lbl_FOID','FOIDType'), ''],
			colModel:[
				{name:'displayOrdePaxSequence', index:'displayOrdePaxSequence', width:18, align:"center" },
				{name:'displayPaxTravelReference', index:'displayAdultPaxTravelReference', width:0, hidden:true },
				{name:'displayAdultType',  index:'displayAdultType', width:30, align:"center" },
				{name:'displayAdultTitle', index:'displayAdultTitle', width:52, align:"center", editable:true, edittype:"select", editoptions:{className:"aa-input", value:jsonPaxTypes, style:"width:50px;"}, hidden:blnTitle},
				{name:'displayAdultFirstName', index:'displayAdultFirstName', width:92, align:"center", editable:true, editoptions:{className:"aa-input fontCapitalize", maxlength:"30", style:"width:90px;"}, hidden:blnFName},
				{name:'displayAdultLastName', index:'displayAdultLastName', width:92, align:"center", editable:true, editoptions:{className:"aa-input fontCapitalize", maxlength:"30", style:"width:90px;"}, hidden:blnLName},
				{name:'displayAdultNationality', index:'displayAdultNationality', width:145, align:"center", editable:true, edittype:"select", editoptions:{className:"aa-input", value:jsonNationality, style:"width:143px;"}, hidden:blnNationality},
				{name:'displayAdultDOB', index:'displayAdultDOB', width:83, align:"center", editable:true, editoptions:{className:"aa-input", maxlength:"10", onchange:'UI_tabBookingInfo.dobOnBlur(this)', style:"width:75px;"}, hidden:blnDOB},
				{name:'displayFFID', index:'displayFFID', width:150, align:"center", hidden:blnFFID, editable:ffidEditable, editoptions:{className:"aa-input", maxlength:"255", style:"width:90px;"}},
				{name:'displayAdultCharg', index:'displayAdultCharg', width:74, align:"right", hidden:blnAmountsACC},
				{name:'displayAdultPay', index:'displayAdultPay', width:46, align:"right", hidden:blnAmountsACC},
				{name:'displayAdultBal', index:'displayAdultBal', width:46, align:"right", hidden:blnAmountsACC},
				{name:'displayAdultAcco', index:'displayAdultAcco', width:50, align:"center", hidden:blnAmountsACC},
				{name:'displayETicketMask', index:'displayETicketMask', width:55, align:"center"},
				{name:'displayAdultMCO', index:'displayAdultMCO', width:50, align:"center", hidden:blnMCO},
				{name:'displayPaxSequence', index:'displayPaxSequence', width:20, align:"center", hidden:true },
				{name:'pnrPaxCatFOIDType', index:'pnrPaxCatFOIDType', width:20, align:"center", hidden:true },
				{name:'displaySelectPax', index:'displaySelectPax', width:30, align:"center", editable:true, edittype:"checkbox", editoptions:{className:"aa-input", value:"on"}}
			],
			
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}
	
	/*
	 * Grid Constructor
	 */
	UI_tabBookingInfo.constructGridInf = function(inParams){
		var blnFName = true;
		var blnLName = true;
		var blnNationality = true;
		var blnDOB = true;
		var blnWithAdult = true;
		var blnTitle = true;
		var blnMCO = true;
		var strFNameMand = "";
		var strLNameMand = "";
		var strNationalityMand = "";
		var strDOBMand = "";
		var strWithAdult = "";
		var strLTitleMand = "";
		var blnAmountsACC = true;
		
		if(!$.isEmptyObject(paxConfigIN)){			
			if(paxConfigIN.firstName.xbeVisibility){
				blnFName = false;
			}
			
			if(paxConfigIN.lastName.xbeVisibility){
				blnLName = false;
			}
			
			if(paxConfigIN.nationality.xbeVisibility){
				blnNationality = false;
			}
			
			if(paxConfigIN.dob.xbeVisibility){
				blnDOB = false;
			}
			
			if(paxConfigIN.travelWith.xbeVisibility){
				blnWithAdult = false;
			}	
			
			if(paxConfigIN.title.xbeVisibility){
				blnTitle = false;
			}
			
			if(paxConfigIN.firstName.xbeMandatory){
				strFNameMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigIN.lastName.xbeMandatory){
				strLNameMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigIN.nationality.xbeMandatory){
				strNationalityMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigIN.dob.xbeMandatory){
				strDOBMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigIN.travelWith.xbeMandatory){
				strWithAdult = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigIN.title.xbeMandatory){
				strLTitleMand = UI_commonSystem.strTxtMandatory;
			}			
			
			if (DATA_ResPro.initialParams.allowViewMCO) {
				blnMCO = false ;
			}
			
			if (DATA_ResPro.initialParams.goquoValid) {
				blnAmountsACC = false;
			}
			
		}
				
		infAlertFormatter = function(index, options, rowObject){
			var paxSeq = rowObject.displayPaxSequence;
			if(typeof(paxSeq) == 'undefined'){
				paxSeq = '';
			}
			if(rowObject.alertAutoCancellation){				
				var imageID = "InfImg_" + paxSeq;
				cnxAlert = '<img border="0" id="'+imageID +'" src="../images/AA169_R_no_cache.gif"/>';
				return paxSeq + " " + cnxAlert;
			} else {
				return paxSeq;
			}
		}
		jsonTravellingWith = UI_tabBookingInfo.populateJsonTravellingWith(jsonPaxAdtObj);
					
		// construct grid
		$(inParams.id).jqGrid({
			height: 30,
			width: 910,
			colNames:['',  geti18nData('non','Reference'), geti18nData('BookingInfo_PassengerInfo_lbl_Type','Type'), geti18nData('BookingInfo_PassengerInfo_lbl_Title','Title') + strLTitleMand, geti18nData('BookingInfo_PassengerInfo_lbl_FirstName','First Name') + strFNameMand, geti18nData('BookingInfo_PassengerInfo_lbl_LastName','Last Name')  + strLNameMand, geti18nData('BookingInfo_PassengerInfo_lbl_Nationality','Nationality')+ strNationalityMand, geti18nData('BookingInfo_PassengerInfo_lbl_DateOfBirth','Date of Birth') + strDOBMand,geti18nData('BookingInfo_PassengerInfo_lbl_TotalCharge','Tot Chg.'), geti18nData('BookingInfo_PassengerInfo_lbl_TotalPay','Tot Pay'),geti18nData('BookingInfo_PassengerInfo_lbl_Balance','Bal.'), geti18nData('BookingInfo_PassengerInfo_lbl_Account','ACC'), geti18nData('BookingInfo_PassengerInfo_lbl_eTicket','E-TCK'), geti18nData('BookingInfo_PassengerInfo_lbl_MCO','MCO'), geti18nData('BookingInfo_PassengerInfo_lbl_withAdult','With Adult') + strWithAdult,'FOIDType', ''],
			colModel:[
				{name:'displayPaxSequence', index:'displayPaxSequence', width:20, align:"center", title:false, formatter:infAlertFormatter},
				{name:'displayPaxTravelReference', index:'displayInfantPaxTravelReference', width:0, hidden:true },
				{name:'displayInfantType',  index:'displayInfantType', width:35, align:"center" },		
				{name : 'displayInfantTitle',index : 'displayInfantTitle',width : 70,align : "center",editable : true,edittype : "select",
					editoptions : {className : "aa-input",value : jsonInfPaxTypes,style : "width:50px;font-size:11px"},
					hidden:blnTitle
				},				
				{name:'displayInfantFirstName', index:'displayInfantFirstName', width:95, align:"center", editable:true, editoptions:{className:"aa-input fontCapitalize", maxlength:"30", onchange:'UI_commonSystem.pageOnChange()', style:"width:160px;"}, hidden:blnFName},
				{name:'displayInfantLastName', index:'displayInfantLastName', width:95, align:"center", editable:true, editoptions:{className:"aa-input fontCapitalize", maxlength:"30", onchange:'UI_commonSystem.pageOnChange()', style:"width:160px;"}, hidden:blnLName},
				{name:'displayInfantNationality', index:'displayInfantNationality', width:180, align:"center", editable:true, edittype:"select", editoptions:{className:"aa-input", value:jsonNationality, style:"width:150px;", onchange:'UI_commonSystem.pageOnChange()'}, hidden:blnNationality},
				{name:'displayInfantDOB', index:'displayInfantDOB', width:100, align:"center", editable:true, editoptions:{className:"aa-input", size:"10", maxlength:"10", onchange:'UI_tabBookingInfo.dobOnBlur(this)', style:"width:75px;"}, hidden:blnDOB},
				{name:'displayInfantCharg', index:'displayInfantCharg', width:74, align:"right", hidden:blnAmountsACC},
				{name:'displayInfantPay', index:'displayInfantPay', width:46, align:"right", hidden:blnAmountsACC},
				{name:'displayInfantBal', index:'displayInfantBal', width:46, align:"right", hidden:blnAmountsACC},
				{name:'displayInfantAcco', index:'displayInfantAcco', width:50, align:"center", hidden:blnAmountsACC},
				{name:'displayETicketMask', index:'displayETicketMask', width:55, align:"center"},
				{name:'displayInfantMCO', index:'displayAdultMCO', width:50, align:"center", hidden:blnMCO},
				{name:'displayInfantTravellingWith', index:'displayInfantTravellingWith', width:80, align:"center", editable:true, edittype:"select", editoptions:{className:"aa-input", value:jsonTravellingWith, onchange:'UI_commonSystem.pageOnChange()'}, hidden:blnWithAdult},				
				{name:'pnrPaxCatFOIDType', index:'pnrPaxCatFOIDType', width:20, align:"center", hidden:true },				
				{name:'displayInfantSelect', index:'displayInfantSelect', width:50, align:"center", editable:true, edittype:"checkbox", editoptions:{className:"aa-input", value:"on"}}
			],
			
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true,
			gridComplete: function(){
				$.each(jsonPaxAdtObj.paxInfants, function(key, item) {
					var autoCnxEnabled = item.alertAutoCancellation;
					if(autoCnxEnabled){
						var paxSeq = item.displayPaxSequence;
						var imageID = "#InfImg_" + paxSeq;
						UI_tabBookingInfo.autoCnxAlertHover(imageID);
					}
				});
			}
		});
	}

    //Check Chage of pax
    UI_tabBookingInfo.changePaxNameChange = function(e, flg){
            var idArray = e.target.id.split("_"),
                index = parseInt(idArray[0], 10) - 1,
                returnBool=false,paxArray=[],gridName="";
            if (flg==="ADT"){
                paxArray = UI_tabBookingInfo.tempPaxAdults;
                gridName = "tblPaxAdtName";
            }else{
                paxArray = UI_tabBookingInfo.tempPaxInfants;
                gridName = "tblPaxInfName";
            }

            $("#"+gridName).find("#"+idArray[0]).find("input[type='text'], select").each(function(inx, obj){
                var objArray = obj.id.split("_")[1].split("-"),
                    valInsideObj = paxArray[index][objArray[0] + objArray[1]];
                if ($.trim($(obj).val()).toUpperCase()!= $.trim(valInsideObj).toUpperCase()){
                    returnBool = true;
                }
            });
            paxArray[index].changed = returnBool;
    }
	
	UI_tabBookingInfo.constructAdultNameGrid = function(inParams){
		var blnTitle = true;
		var blnFName = true;
		var blnLName = true;
		var blnFFID = true;
		var strTitleMand = "";
		var strFNameMand = "";
		var strLNameMand = "";
		var strLFFIDMand = "";
		
		if(!$.isEmptyObject(paxConfigAD)){			
			if(paxConfigAD.title.xbeVisibility ||
					paxConfigCH.title.xbeVisibility){
				blnTitle = false;
			}
			
			if(paxConfigAD.firstName.xbeVisibility ||
					paxConfigCH.firstName.xbeVisibility){
				blnFName = false;
			}
			
			if(paxConfigAD.lastName.xbeVisibility ||
					paxConfigCH.lastName.xbeVisibility){
				blnLName = false;
			}
			
			if(paxConfigAD.ffid.xbeVisibility ||
					paxConfigCH.ffid.xbeVisibility){
				blnFFID = false;
			}
				
			if(paxConfigAD.title.xbeMandatory){
				strTitleMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigAD.firstName.xbeMandatory){
				strFNameMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigAD.lastName.xbeMandatory){
				strLNameMand = UI_commonSystem.strTxtMandatory;
			}	
		}
		
		var isFFIDEditable = UI_reservation.isAllowNameChange && UI_reservation.inEditFFIDBuffer;

        //Custom formater
        var selectFormatter = function(value, options, rowObject){
            var	htmlSelect = "<select class='aa-input' disabled='disabled' id='"+ options.rowId + "_" + options.colModel.index +"' onchange='UI_tabBookingInfo.changePaxNameChange(event, \"ADT\")'>";
            jsonPaxType = jsonPaxTypes.split(";");
	       	if(rowObject.displayAdultType=="AD"){
				for (var i = 0; i<jsonPaxType.length;i++){
					var t = jsonPaxType[i].split(":"), selected="";
					if(value===t[0]){selected="selected='selected'"}
					htmlSelect += "<option value='"+t[0]+"' "+selected+">"+t[1]+"</option>";
				}
			} else if (rowObject.displayAdultType=="CH") {
	       		for (var i = 0; i<top.arrTitleChild.length;i++){
					var t = jsonPaxType[i].split(":"), selected="";
					if(value===top.arrTitleChild[i][0]){selected="selected='selected'"}
					htmlSelect += "<option value='"+top.arrTitleChild[i][0]+"' " + selected + ">"+top.arrTitleChild[i][1]+"</option>";
				}
			}
	        htmlSelect += "</select>";
	        return htmlSelect;
        };

        var inputFormatter = function(value, options, rowObject){
        	var checkedVal = isNull(value) ? "" : value;
            var htmlInput = "<input type='text' disabled='disabled' class='aa-input' id='"+ options.rowId + "_" + options.colModel.index +"' value='"+ checkedVal  +"' onchange='UI_tabBookingInfo.changePaxNameChange(event, \"ADT\")'>";
            return htmlInput;
        };

        var checkFormatter = function(value, options, rowObject){
            var htmlInput = "<input type='checkbox' class='aa-input' id='"+ options.rowId + "_" + options.colModel.index +"' value='"+ value +"' onchange='UI_tabBookingInfo.changePaxNameChange(event, \"ADT\")'>";
            return htmlInput;
        }

		
		$(inParams.id).jqGrid({
			height: 100,
			width: 850,
			colNames:[ '','Reference', 'Type', 'Title' + strTitleMand , 'First Name' + strFNameMand, 'Last Name' + strLNameMand, "Air Rewards ID", ''],
			colModel:[
				{name:'displayOrdePaxSequence', index:'display-OrdePaxSequence', width:18, align:"center" },
				{name:'displayPaxTravelReference', index:'display-AdultPaxTravelReference', width:0, hidden:true },
				{name:'displayAdultType',  index:'display-AdultType', width:30, align:"center" },
                {name:'displayAdultTitle', index:'display-AdultTitle', width:52, align:"center", hidden:blnTitle, formatter: selectFormatter},
                {name:'displayAdultFirstName', index:'display-AdultFirstName', width:92, align:"center", formatter: inputFormatter, hidden:blnFName},
                {name:'displayAdultLastName', index:'display-AdultLastName', width:92, align:"center", formatter: inputFormatter, hidden:blnLName},
                {name:'displayFFID', index:'display-FFID', width:52, align:"center", formatter: inputFormatter, hidden:blnFFID},
                {name:'displaySelectPax', index:'display-SelectPax', width:30, align:"center", formatter: checkFormatter,hidden:true }
			],
			
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}
	
	UI_tabBookingInfo.constructInfNameGrid = function(inParams){
		var blnFName = true;
		var blnLName = true;
		var strFNameMand = "";
		var strLNameMand = "";
		
		if(!$.isEmptyObject(paxConfigIN)){			
			if(paxConfigIN.firstName.xbeVisibility){
				blnFName = false;
			}
			
			if(paxConfigIN.lastName.xbeVisibility){
				blnLName = false;
			}
			
			if(paxConfigIN.firstName.xbeMandatory){
				strFNameMand = UI_commonSystem.strTxtMandatory;
			}
			
			if(paxConfigIN.lastName.xbeMandatory){
				strLNameMand = UI_commonSystem.strTxtMandatory;
			}
			
		}

        var inputFormatter = function(value, options, rowObject){
            var htmlInput = "<input type='text' disabled='disabled' class='aa-input' id='"+ options.rowId + "_" + options.colModel.index +"' value='"+ value +"' onchange='UI_tabBookingInfo.changePaxNameChange(event, \"INF\")'>";
            return htmlInput;
        };

        var checkFormatter = function(value, options, rowObject){
            var htmlInput = "<input type='checkbox' class='aa-input' id='"+ options.rowId + "_" + options.colModel.index +"' value='"+ value +"' onchange='UI_tabBookingInfo.changePaxNameChange(event, \"INF\")'>";
            return htmlInput;
        }
					
		//construct grid
		$(inParams.id).jqGrid({
			height: 60,
			width: 850,
			colNames:['', 'Reference', 'Type', 'First Name' + strFNameMand, 'Last Name' + strLNameMand, ''],
			colModel:[
				{name:'displayPaxSequence', index:'display-PaxSequence', width:20, align:"center", title:false, formatter:infAlertFormatter},
				{name:'displayPaxTravelReference', index:'display-InfantPaxTravelReference', width:0, hidden:true },
				{name:'displayInfantType',  index:'display-InfantType', width:35, align:"center" },
                {name:'displayInfantFirstName', index:'display-InfantFirstName', width:175, align:"center", formatter: inputFormatter, hidden:blnFName},
                {name:'displayInfantLastName', index:'display-InfantLastName', width:175, align:"center", formatter: inputFormatter, hidden:blnLName},
                {name:'displayInfantSelect', index:'display-InfantSelect', width:50, align:"center", formatter: checkFormatter,hidden:true }
			],
			
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}
	
	UI_tabBookingInfo.constructPaxListForCA = function(){
		$("#listPassengers").empty();
		var paxArray = $.parseJSON(UI_reservation.jsonPassengers);
		var sortedPaxArray = $.airutil.sort.quickSort(paxArray, UI_tabBookingInfo.paxListComparator);
		$.each(sortedPaxArray,function(){
			var paxTitle = ' ';
			if(this.paxType != 'IN'){
				paxTitle = this.title+'. ';
			}
			var displayText=this.paxType+' - '+paxTitle+this.firstName+' '+this.lastName+' - Total Paid : '+this.totalPaidAmount;
			$("#listPassengers").append('<li class="ui-widget-content ui-selectee" id="'+this.travelerRefNumber+'">'+displayText+'</li>');
			
		});
		$("#listPassengers").selectable({
			stop: function() {
				selectedPax=[];
				$("#listPassengers .ui-selected").each(function() {
					selectedPax.push(this.id);
				});
			}
		});
	}
	
	UI_tabBookingInfo.paxListComparator = function(pax1, pax2){
		return pax1.paxSequence - pax2.paxSequence;
	}
	
	UI_tabBookingInfo.constructSegListForCA = function(){
		$("#listSegments").empty();
		var segmentArray = $.parseJSON(UI_reservation.jsonONDs);
		var segmentList = new Array();
		var j = 0;
		for(var i = 0;i < segmentArray.length;i++){
			if(segmentArray[i].subStatus != "EX"){
				segmentList[j] = segmentArray[i];
				j++;
			}
		}
			
		$.each(segmentList,function(){
			$("#listSegments").append('<li class="ui-widget-content ui-selectee" id="'+this.carrierOndGroupRPH+'">'+this.carrierCode+' - '+
					this.segmentCode+'  '+this.departureDateTime+' - '+this.status+' - '+this.totalCharges+'</li>');
		});
		$("#listSegments").selectable({
			selecting: function(event,ui){
				var carrier=ui.selecting.firstChild.wholeText.split('-')[0].trim();
				if(carrier!=lastSelectedONDCareer && selectedONDs.length > 1){
					alert('You are not allowed to select ONDs belonging to one carrier simultaneously.');
				}else{
					lastSelectedONDCareer=carrier;
				}
			},
			stop: function(){
				selectedONDs=[];
				
				$("#listSegments .ui-selected").each(function() {
					selectedONDs.push(this.id);
				});
				
				$("#selAdjustmentType").empty();
				var adjustmentTypes=[];
				for (var i=0; i< chargeAdjustments.length; i++) {
					if (chargeAdjustments[i].carrierCode == lastSelectedONDCareer){
						adjustmentTypes = chargeAdjustments[i].chargeAdjustmentTypes;
						break;
					}
				}
				for (var i=0; i< adjustmentTypes.length; i++) {
					if (UI_tabBookingInfo.validateChargeAdjustmentType(adjustmentTypes[i], lastSelectedONDCareer) ) {
						$("#selAdjustmentType").append('<option value="' + adjustmentTypes[i].chargeAdjustmentTypeId + '">' + adjustmentTypes[i].chargeAdjustmentTypeName + '</option>');
					}					
				} 
			}
		});
	}
	UI_tabBookingInfo.validateChargeAdjustmentType = function(adjustmentType, carrierCode) {
		var adjustmentPrivilege = UI_tabBookingInfo.getSelectedAdjustmentPrivileges(carrierCode, adjustmentType.chargeAdjustmentTypeId);
		var refundAllowed = false;
		var nonRefundAllowed = false;
		
		if (adjustmentType.refundableChargeCode != null &&
				adjustmentType.refundableChargeCode.length > 0 && adjustmentPrivilege.refundable == true) {				
			refundAllowed = true;
		}
		
		if (adjustmentType.nonRefundableChargeCode != null && 
				adjustmentType.nonRefundableChargeCode.length > 0 && adjustmentPrivilege.nonRefundable == true) {				
			nonRefundAllowed = true
		}	
		
		if (refundAllowed || nonRefundAllowed) {
			return true;			
		} else {
			return false;
		}
	}
	UI_tabBookingInfo.getSelectedAdjustmentPrivileges = function (selectedCarrierCode, selectedAdjustmentTypeId){
		
		var privs = DATA_ResPro.initialParams.chargeAdjustPrivileges.carrierWiseChargeAdjustPrivileges;
		var selectedCarrierPrivileges;
		var selectedAdjustmentTypePrivileges;

		$.each(privs, function(index, value) { 
  			if (index == selectedCarrierCode) {
  				selectedCarrierPrivileges = value;
  				return false;
  			}
		});		
		
		for (var i=0; i< selectedCarrierPrivileges.length; i++) {
			if (selectedCarrierPrivileges[i].chargeAdjustmentTypeId == selectedAdjustmentTypeId) {
				selectedAdjustmentTypePrivileges = selectedCarrierPrivileges[i];
				break;
			}
		}		
		return selectedAdjustmentTypePrivileges;
	}
	/*
	 * Disable Page Controls
	 */ 
	UI_tabBookingInfo.disableEnableControls = function(blnStatus, blnFrEdit, blnIsCsOcFlightExist){
		$("#txtUNotes").disableEnable(blnStatus);
		$("#txtEndosment").disableEnable(blnStatus);
		$("#selSSRCode").disableEnable(blnStatus);
		$("#txtComment").readOnly(blnStatus);
		
		var isEnableCSVUpload = false;
		var intLen = jsonPaxAdtObj.paxAdults.length;
		if (intLen > 0){
			var i = 1;
			$.each(jsonPaxAdtObj.paxAdults, function(){
				if(!isEnableCSVUpload){
					if(UI_reservation.allowCSVDetailUpload || DATA_ResPro.initialParams.tbaGroupNameUpload){
						isEnableCSVUpload = true;
					}
				}
				
				$("#"+ i + "_displayAdultTitle").disableEnable(blnStatus);
				if(!blnStatus){
					if(this.nameEditable){
						$("#"+ i + "_displayAdultFirstName").disableEnable(blnStatus);				
						$("#"+ i + "_displayAdultLastName").disableEnable(blnStatus);
					}
				}else {
					$("#"+ i + "_displayAdultFirstName").disableEnable(blnStatus);				
					$("#"+ i + "_displayAdultLastName").disableEnable(blnStatus);
				}
								
				//$("#"+ i + "_displayAdultNationality").disableEnable(blnStatus);
				$("#"+ i + "_displayAdultDOB").disableEnable(blnStatus);
				$("#"+ i + "_displayAdultSSR").disableEnable(blnStatus);
				if(blnFrEdit) {
					$("#"+ i + "_displaySelectPax").disableEnable(false);
				}else {
					$("#"+ i + "_displaySelectPax").disableEnable(blnStatus);
				}
				if((!blnStatus && (($("#"+ i + "_displayFFID").val() == "" && UI_reservation.addFFID) || UI_reservation.editFFID)) || blnStatus){					
					$("#"+ i + "_displayFFID").disableEnable(blnStatus);
				}
				
				i++;
			});
		}
		
		intLen = jsonPaxAdtObj.paxInfants.length;
		if (intLen > 0){
			var i = 1;
			$.each(jsonPaxAdtObj.paxInfants, function(){
				if(!blnStatus){
					if(this.nameEditable){
						$("#"+ i + "_displayInfantFirstName").disableEnable(blnStatus);				
						$("#"+ i + "_displayInfantLastName").disableEnable(blnStatus);
					}
					
				}else {
					$("#"+ i + "_displayInfantFirstName").disableEnable(blnStatus);				
					$("#"+ i + "_displayInfantLastName").disableEnable(blnStatus);	
				}							
				//$("#"+ i + "_displayInfantNationality").disableEnable(blnStatus);
				$("#"+ i + "_displayInfantDOB").disableEnable(blnStatus);
				if(blnIsCsOcFlightExist){
					$("#"+ i + "_displayInfantTravellingWith").disableEnable(blnIsCsOcFlightExist);	
				} else {
					$("#"+ i + "_displayInfantTravellingWith").disableEnable(blnStatus);					
				}
				if(blnFrEdit) {
					$("#"+ i + "_displayInfantSelect").disableEnable(false);
				}else {
					$("#"+ i + "_displayInfantSelect").disableEnable(blnStatus);
				}
				$("#"+ i + "_displayInfantTitle").disableEnable(blnStatus);
				i++;
			});
		}
		
		if(blnFrEdit){
			if(!isEnableCSVUpload){
				$("#btnUploadFromCSV").disable(true);
				$("#btnCSVUploadSampleFile").disable(true);
				$("#btnUploadFromCSV").hide();
				$("#btnCSVUploadSampleFile").hide();
			} else {
				if(UI_tabBookingInfo.isTBABooking() && DATA_ResPro.initialParams.allowPassengerDetailCSVUpload){
					$("#btnUploadFromCSV").enable(true);
					$("#btnCSVUploadSampleFile").enable(true);
					$("#btnUploadFromCSV").show();
					$("#btnCSVUploadSampleFile").show();
				}
			}	
		}else if(!blnFrEdit && !UI_tabBookingInfo.isTBABooking()){
			$("#btnUploadFromCSV").disable(true);
			$("#btnCSVUploadSampleFile").disable(true);
			$("#btnUploadFromCSV").hide();
			$("#btnCSVUploadSampleFile").hide();
		}
	}
	
	/*
	 * Allow changing Pax name depending on specific privileges 
	 * 
	 * */
	
	UI_tabBookingInfo.allowPaxNameChanges = function(blnStatus, skipTitle){
		
		var intLen = jsonPaxAdtObj.paxAdults.length;
		if (intLen > 0){
			var i = 1;
			$.each(jsonPaxAdtObj.paxAdults, function(){
								
				if(!skipTitle){
					$("#"+ i + "_displayAdultTitle").disableEnable(blnStatus);
				}
				
				$("#"+ i + "_displayAdultFirstName").disableEnable(blnStatus);				
				$("#"+ i + "_displayAdultLastName").disableEnable(blnStatus);
												
				i++;
			});
		}
		
		intLen = jsonPaxAdtObj.paxInfants.length;
		if (intLen > 0){
			var i = 1;
			$.each(jsonPaxAdtObj.paxInfants, function(){
				
				$("#"+ i + "_displayInfantFirstName").disableEnable(blnStatus);				
				$("#"+ i + "_displayInfantLastName").disableEnable(blnStatus);	
				
				i++;
			});
		}
		
	}
	
	/*
	 * Allow/Deny changing passenger frequent flyer id.
	 * 
	 * */
	UI_tabBookingInfo.allowPaxFFIDChanges = function(blnStatus){
		
		var intLen = jsonPaxAdtObj.paxAdults.length;
		if (intLen > 0){
			var i = 1;
			$.each(jsonPaxAdtObj.paxAdults, function(){
				$("#"+ i + "_displayFFID").disableEnable(blnStatus);												
				i++;
			});
		}
		
	}
	
	/*
	 * Validate Page
	 */ 
	UI_tabBookingInfo.validate = function() {

		// Validate Adult/Children Fields
		var numAdtChd = jsonPaxAdtObj.paxAdults.length;
		var intAge = null;
		var intLowerAge = null;
		var strPaxDesc = "";
		var paxConfig = "";
		var tba = UI_tabBookingInfo.xbeTBA;
		var flightDate = UI_tabBookingInfo.fltOutDate.split(" ");
		var flightOutDate = (flightDate.length > 1) ? flightDate[1] : flightDate[0];
		
		var requireNationality = function(index){ 
			var fn = $.trim($("#"+ index + "_displayAdultFirstName").val());
			var ln = $.trim($("#"+ index + "_displayAdultLastName").val());
			if(fn == tba && ln == tba){
				return false;
			}
			if(index == 1) return true; else return false
		};
		var requireTitle = function(index){
			if(index == 1) return true;
			var fn = $.trim($("#"+ index + "_displayAdultFirstName").val());
			var ln = $.trim($("#"+ index + "_displayAdultLastName").val());
			if(fn == tba && ln == tba){
				return false;
			}
			return true;
		};
		
		for(var i = 1; i <= numAdtChd; i++) {
			if(jsonPaxAdtObj.paxAdults[i-1].displayAdultType == "AD"){
				intAge = paxValidation.adultAgeCutOverYears;
				intLowerAge = paxValidation.childAgeCutOverYears;
				strPaxDesc = "Adult";
				paxConfig = paxConfigAD;
			}else {
				intAge = paxValidation.childAgeCutOverYears;
				intLowerAge = paxValidation.infantAgeCutOverYears;
				strPaxDesc = "Child";
				paxConfig = paxConfigCH;
			}
			
			if(paxConfig.title.xbeVisibility && paxConfig.title.xbeMandatory){
				if(requireTitle(i) && $.trim($("#"+ i + "_displayAdultTitle").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Title"));
					$("#"+ i + "_displayAdultTitle").focus();
					return false;
				}
			}
			
			if(paxConfig.firstName.xbeVisibility && paxConfig.firstName.xbeMandatory){
				if($.trim($("#"+ i + "_displayAdultFirstName").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","First name"));
					$("#"+ i + "_displayAdultFirstName").focus();
					return false;
				}else {
					$("#"+ i + "_displayAdultFirstName").val($.trim($("#"+ i + "_displayAdultFirstName").val()));
					
				}
			}			
			
			// AARESAA-6541 Fix
			if (!UI_commonSystem.controlValidateName({id:"#"+ i +"_displayAdultFirstName", desc:"First Name", required:paxConfig.firstName.xbeMandatory})){return false;}		
			
			if($.trim($("#"+ i + "_displayAdultFirstName").val()) != "" && 
					!isAlphaWhiteSpace($("#"+ i + "_displayAdultFirstName").val())) {
				showERRMessage(raiseError("XBE-ERR-04","First name"));
				$("#"+ i + "_displayAdultFirstName").focus();
				return false;
			}
						
			// AARESAA-6541 Fix
			if (!UI_commonSystem.controlValidateName({id:"#"+ i +"_displayAdultFirstName", desc:"First Name", required:paxConfig.firstName.xbeMandatory})){return false;}		
						
			if(paxConfig.lastName.xbeVisibility && paxConfig.lastName.xbeMandatory){
				if($.trim($("#"+ i + "_displayAdultLastName").val()) == "") {
					showERRMessage(raiseError("XBE-ERR-01","Last name"));
					$("#"+ i + "_displayAdultLastName").focus();
					return false;
				}else {
					$("#"+ i + "_displayAdultLastName").val($.trim($("#"+ i + "_displayAdultLastName").val()));
					
				}
			}			
			
			// AARESAA-6541 Fix
			if (!UI_commonSystem.controlValidateName({id:"#"+ i + "_displayAdultLastName", desc:"Last Name", required:paxConfig.lastName.xbeMandatory})){return false;}
			
			if($.trim($("#"+ i + "_displayAdultLastName").val()) != "" && 
					!isAlphaWhiteSpace($("#"+ i + "_displayAdultLastName").val())) {
				showERRMessage(raiseError("XBE-ERR-04","Last name"));
				$("#"+ i + "_displayAdultLastName").focus();
				return false;
			}

			// AARESAA-6541 Fix
			if (!UI_commonSystem.controlValidateName({id:"#"+ i + "_displayAdultLastName", desc:"Last Name", required:paxConfig.lastName.xbeMandatory})){return false;}
						
			if(paxConfig.nationality.xbeVisibility && paxConfig.nationality.xbeMandatory){
				if(requireNationality(i) && $.trim($("#" + i + "_displayAdultNationality").val()) == "" ){
					showERRMessage(raiseError("XBE-ERR-01","Nationality"));
					$("#"+ i + "_displayAdultNationality").focus();
					return false;
				}
			}
			
			if(paxConfig.ffid.xbeVisibility ){
				//var ffid = jsonPaxAdtObj.paxAdults[i-1].displayFFID;
				var ffid = $.trim($("#" + i + "_displayFFID").val());
				var firstName = $.trim($("#" + i + "_displayAdultFirstName").val());
				var lastName = $.trim($("#" + i + "_displayAdultLastName").val());
				
				if (jsonPaxAdtObj.paxAdults[i-1].displayFFID != null && jsonPaxAdtObj.paxAdults[i-1].displayFFID != "" && jsonPaxAdtObj.paxAdults[i-1].displayFFID != ffid && !UI_reservation.editFFID) {
					showERRMessage(raiseError("XBE-ERR-100"));
					return false;
				}
				
				if(ffid != null && ffid != ""){
					
					//Check for T B A bookings
					if(firstName == UI_tabBookingInfo.xbeTBA || lastName == UI_tabBookingInfo.xbeTBA){
						showERRMessage(raiseError("XBE-ERR-97"));
						return false;
					}
					
					firstName = firstName.toLowerCase();
					lastName = lastName.toLowerCase();
					
					var data = {};
					data['ffid'] = ffid;
					data['firstName'] = firstName;
					data['lastName'] = lastName;
					var retValue = false;
					
					$("#frmModiBkg").ajaxSubmit({
						dataType: 'json', 
						url:"duplicateCheck!ffidCheck.action", async:false,
						data:data,
						async: false,
						success: function(response){ 
							var ffid = $.trim($("#" + i + "_displayFFID").val());
							retValue = response.ffidAvailability;
							if(!retValue){
								showERRMessage(raiseError("XBE-ERR-96", ffid));
								$("#" + i + "_displayFFID").focus();	
							} else{
								firstName2 = response.firstName.toLowerCase();
								lastName2 = response.lastName.toLowerCase();
								var nameValidation = UI_commonSystem.nameCompare(firstName, lastName, firstName2, lastName2);
								if(!nameValidation){
									retValue = false;
									showERRMessage(raiseError("XBE-ERR-95", ffid));
									$("#"+ i + "_displayAdultFirstName").focus();
								}
							}
						},
						error : UI_commonSystem.setErrorStatus
					});
					
					if(!retValue){
						return false;
					}
				} else if (UI_tabBookingInfo.ffidChanged(ffid, jsonPaxAdtObj.paxAdults[i-1].displayFFID) && !UI_reservation.editFFID) {
					showERRMessage(raiseError("XBE-ERR-101"));
				}
			}
			
			if(paxConfig.dob.xbeVisibility && paxConfig.dob.xbeMandatory){
				if($.trim($("#" + i + "_displayAdultDOB").val()) == "" ){
					showERRMessage(raiseError("XBE-ERR-01","Date Of Birth"));
					$("#"+ i + "_displayAdultDOB").focus();
					return false;
				} else {
					$("#"+ i + "_displayAdultDOB").val($.trim($("#"+ i + "_displayAdultDOB").val()));
				}
			}								
			
			
			if (!$("#" + i + "_displayAdultDOB").isFieldEmpty()) {
				if (!CheckDates($("#" + i + "_displayAdultDOB").val(), top.arrParams[0])){
					showERRMessage(raiseError("XBE-ERR-63", "Date of birth"));
					$("#" + i + "_displayAdultDOB").focus();
					return false;
				}
				
				
				if (jsonPaxAdtObj.paxAdults[i-1].displayAdultType == "AD" && UI_tabBookingInfo.fltOutDate != "") {					
					if (!ageCompare($("#" + i + "_displayAdultDOB").val(),
							jsonLastFlightArrivalDate, intAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + i  + "_displayAdultDOB").focus();
						return false;
					}
					if (ageCompare($("#" + i + "_displayAdultDOB").val(),
							jsonLastFlightArrivalDate, intLowerAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + i + "_displayAdultDOB").focus();
						return false;
					}
				}
				
				if (jsonPaxAdtObj.paxAdults[i-1].displayAdultType == "CH" && UI_tabBookingInfo.fltOutDate != "") {
					if (!ageCompare($("#" + i + "_displayAdultDOB").val(),
							jsonLastFlightArrivalDate, intAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + i + "_displayAdultDOB").focus();
						return false;
					}
					
					if (ageCompare($("#" + i + "_displayAdultDOB").val(),
							jsonLastFlightArrivalDate, intLowerAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + i  + "_displayAdultDOB").focus();
						return false;
					}
				}
			}
			
			if(paxConfig.passportNo.xbeVisibility && paxConfig.passportNo.xbeMandatory){				
				if((jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDNumber == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDNumber == "") &&
						($("#"+ i + "_displayAdultFirstName").val() != 'T B A' ||
								$("#"+ i + "_displayAdultLastName").val() != 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passport Number"));				
					return false;			
				}
			}			
			if (!DATA_ResPro.initialParams.allowSkipNic) {
				if(paxConfig.nationalIDNo.xbeVisibility && top.isRequestNICForReservationsHavingDomesticSegments == "true"){
					if($("#"+ i + "_displayAdultFirstName").val() != 'T B A' ||
							$("#"+ i + "_displayAdultLastName").val() != 'T B A'){
						if(UI_tabBookingInfo.isDomesticFlightExist() == true &&
								(jsonPaxAdtObj.paxAdults[i-1].displayNationalIDNo == "" || jsonPaxAdtObj.paxAdults[i-1].displayNationalIDNo == null)){
							showERRMessage(raiseError("XBE-ERR-01", "National ID No"));
							return false;
						}
					}
				} else if(paxConfig.nationalIDNo.xbeVisibility && paxConfig.nationalIDNo.xbeMandatory){				
					if((jsonPaxAdtObj.paxAdults[i-1].displayNationalIDNo == null || 
							jsonPaxAdtObj.paxAdults[i-1].displayNationalIDNo == "") &&
							($("#"+ i + "_displayAdultFirstName").val() != 'T B A' ||
									$("#"+ i + "_displayAdultLastName").val() != 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","National ID No"));				
						return false;			
					}
				}
			}
			
			if(paxConfig.passportExpiry.xbeVisibility && paxConfig.passportExpiry.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDExpiry == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDExpiry == "") &&
						($("#"+ i + "_displayAdultFirstName").val() != 'T B A' ||
								$("#"+ i + "_displayAdultLastName").val() != 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passport Expiry Date"));				
					return false;
				}		
				
				if(this.lastFltArrivalDate == null) {
					showERRMessage("TAIR-91157: Either all flights are flown or cancelled. Not allowing to modifiy passport information");
					return false;
				} else {
					if(jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDExpiry != null && jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDExpiry != ""){
						if(!compareDate(jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDExpiry , this.lastFltArrivalDate.substr(4))){
							showERRMessage("TAIR-91156: Pax Passport is being expire by selected travel date");
							return false;
						}
					}
				}
				
			}
			
			if(paxConfig.passportIssuedCntry.xbeVisibility && paxConfig.passportIssuedCntry.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDPlace == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxCatFOIDPlace == "") &&
						($("#"+ i + "_displayAdultFirstName").val() != 'T B A' ||
								$("#"+ i + "_displayAdultLastName").val() != 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passport Place of Issue"));				
					return false;
				}				
			}
			
			if(paxConfig.placeOfBirth.xbeVisibility && paxConfig.placeOfBirth.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxPlaceOfBirth == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayPnrPaxPlaceOfBirth == "") &&
						!($("#" + i  + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + i + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passenger Place of Birth"));				
					return false;
				}				
			}
			
			if(paxConfig.travelDocumentType.xbeVisibility && paxConfig.travelDocumentType.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayTravelDocType == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayTravelDocType == "") &&
						!($("#" + i + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + i + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Travel Document Type"));				
					return false;
				}				
			}
			
			if(paxConfig.visaDocNumber.xbeVisibility && paxConfig.visaDocNumber.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayVisaDocNumber == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayVisaDocNumber == "") &&
						!($("#" + i + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + i + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Document Number"));				
					return false;
				}				
			}
			
			if(paxConfig.visaDocPlaceOfIssue.xbeVisibility && paxConfig.visaDocPlaceOfIssue.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayVisaDocPlaceOfIssue == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayVisaDocPlaceOfIssue == "") &&
						!($("#" + i + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + i + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Document Place Of Issue"));				
					return false;
				}				
			}
			
			if(paxConfig.visaDocIssueDate.xbeVisibility && paxConfig.visaDocIssueDate.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayVisaDocIssueDate == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayVisaDocIssueDate == "") &&
						!($("#" + i + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + i + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Document Issue Date"));				
					return false;
				}				
			}
			
			if(paxConfig.visaApplicableCountry.xbeVisibility && paxConfig.visaApplicableCountry.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i-1].displayVisaApplicableCountry == null || 
						jsonPaxAdtObj.paxAdults[i-1].displayVisaApplicableCountry == "") &&
						!($("#" + i + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + i + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Applicable Country"));				
					return false;
				}				
			}
	
		}
		
		// Validate Infant Fields
		if(jsonPaxAdtObj.paxInfants) {
			var numInf = jsonPaxAdtObj.paxInfants.length;
			var withAdultStrList = "";
			for(var i = 1; i <= numInf; i++) {				
				
				if(paxConfigIN.firstName.xbeVisibility && paxConfigIN.firstName.xbeMandatory){
					if($.trim($("#"+ i + "_displayInfantFirstName").val()) == "") {
						showERRMessage(raiseError("XBE-ERR-01","First name"));
						$("#"+ i + "_displayInfantFirstName").focus();
						return false;
					} else {
						$("#"+ i + "_displayInfantFirstName").val($.trim($("#"+ i + "_displayInfantFirstName").val()));
						
					}
				}
				
				if(paxConfigIN.title.xbeVisibility && paxConfigIN.title.xbeMandatory){
					if($.trim($("#"+ i + "_displayInfantTitle").val()) == "") {
						showERRMessage(raiseError("XBE-ERR-01","Title"));
						$("#"+ i + "_displayInfantTitle").focus();
						return false;
					} else {
						$("#"+ i + "_displayInfantTitle").val($.trim($("#"+ i + "_displayInfantTitle").val()));
						
					}
				}
				
				// AARESAA-6541 Fix
				if (!UI_commonSystem.controlValidateName({id:"#"+ i + "_displayInfantFirstName", desc:"First name", required:paxConfigIN.firstName.xbeMandatory})){return false;}
				
				if($.trim($("#"+ i + "_displayInfantFirstName").val()) != "" &&
						!isAlphaWhiteSpace($("#"+ i + "_displayInfantFirstName").val())) {
					showERRMessage(raiseError("XBE-ERR-04","First name"));
					$("#"+ i + "_displayInfantFirstName").focus();
					return false;
				}
				
				if(paxConfigIN.lastName.xbeVisibility && paxConfigIN.lastName.xbeMandatory){
					if($.trim($("#"+ i + "_displayInfantLastName").val()) == "") {
						showERRMessage(raiseError("XBE-ERR-01","Last name"));
						$("#"+ i + "_displayInfantLastName").focus();
						return false;
					}else {
						$("#"+ i + "_displayInfantLastName").val($.trim($("#"+ i + "_displayInfantLastName").val()));
						
					}
				}
								
				// AARESAA-6541 Fix
				if (!UI_commonSystem.controlValidateName({id:"#"+ i + "_displayInfantLastName", desc:"Last name", required:paxConfigIN.lastName.xbeMandatory})){return false;}
				
				if($.trim($("#"+ i + "_displayInfantLastName").val()) != "" &&
						!isAlphaWhiteSpace($("#"+ i + "_displayInfantLastName").val())) {
					showERRMessage(raiseError("XBE-ERR-04","Last name"));
					$("#"+ i + "_displayInfantLastName").focus();
					return false;
				}
				
				if(paxConfigIN.nationality.xbeVisibility && paxConfigIN.nationality.xbeMandatory){
					if($.trim($("#" + i + "_displayInfantNationality").val()) == "" ){
						showERRMessage(raiseError("XBE-ERR-01","Nationality"));
						$("#"+ i + "_displayInfantNationality").focus();
						return false;
					}
				}
				
				if(paxConfigIN.dob.xbeVisibility && paxConfigIN.dob.xbeMandatory){
					if($.trim($("#" + i + "_displayInfantDOB").val()) == "" ){
						showERRMessage(raiseError("XBE-ERR-01","Date Of Birth"));
						$("#"+ i + "_displayInfantDOB").focus();
						return false;
					} else {
						$("#"+ i + "_displayInfantDOB").val($.trim($("#"+ i + "_displayInfantDOB").val()));
					}
				}
				
								
				if (!$("#" + i + "_displayInfantDOB").isFieldEmpty()) {
					if (!CheckDates($("#" + i + "_displayInfantDOB").val(), top.arrParams[0])) {
						showERRMessage(raiseError("XBE-ERR-63", "Date of birth"));
						$("#" + (i + 1) + "_displayInfantDOB").focus();
						return false;
					}
					
					if (UI_tabBookingInfo.fltOutDate != ""){
						if (!ageCompare($("#" + i + "_displayInfantDOB").val(), UI_tabBookingInfo.fltOutDate,
								top.strDefInfantAge)) {
							showERRMessage(raiseError("XBE-ERR-64", "Infant",
									"Infant"));
							$("#" + (i + 1) + "_displayInfantDOB").focus();
							return false;
						}						
					}						
     					
	
     				/* Validates infant age */
					var intInfantAge	          = top.strDefInfantAge;
					var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;
					
					// Validates infant age - should be less than 2 years
					if($.trim($("#"+ i + "_displayInfantDOB").val()) !== "") {
						if(!ageCompare(($("#"+ i + "_displayInfantDOB").val()),jsonLastFlightDate,intInfantAge)) {
							showERRMessage(raiseError("XBE-ERR-64","Date of birth","Infant"));
							$("#"+ i + "_displayInfantDOB").focus();
							return false;
						}
					}					
						// Infant age should be greater than 16 days
						if($.trim($("#" + i + "_displayInfantDOB").val()) !== "") {
							if(!UI_tabBookingInfo.checkInfantMinAge(($("#" + i + "_displayInfantDOB").val()),UI_tabBookingInfo.fltOutDate,infantAgeLowerBoundInDays))  {
								showERRMessage(raiseError("XBE-ERR-70","Date of birth",infantAgeLowerBoundInDays));
								$("#" + i + "_displayInfantDOB").focus();
								return false;
							}
						}
					
										
				}
				
				// Validate "With Adult" dropdown
				var withAdult = $.trim($("#"+ i + "_displayInfantTravellingWith").val());
				
				if(paxConfigIN.travelWith.xbeVisibility && paxConfigIN.travelWith.xbeMandatory){
					if(withAdult == "") {
						showERRMessage(raiseError("XBE-ERR-01","With Adult"));
						$("#"+ i + "_displayInfantTravellingWith").focus();
						return false;
					}
				}
				
				if(withAdult != "") {
					withAdult += "+";
					if(withAdultStrList.search(withAdult) != -1) {
						showERRMessage(raiseError("XBE-ERR-09"));
						$("#"+ i + "_displayInfantTravellingWith").focus();
						return false;
					} else {
						withAdultStrList += withAdult;
					}
				}
				
				if(paxConfigIN.passportNo.xbeVisibility && paxConfigIN.passportNo.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxCatFOIDNumber == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxCatFOIDNumber == "") &&
							($("#"+ i + "_displayInfantFirstName").val() != 'T B A' ||
									$("#"+ i + "_displayInfantLastName").val() != 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passport Number"));				
						return false;
					}				
				}
				
				if(paxConfigIN.passportExpiry.xbeVisibility && paxConfigIN.passportExpiry.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxCatFOIDExpiry == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxCatFOIDExpiry == "") &&
							($("#"+ i + "_displayInfantFirstName").val() != 'T B A' ||
									$("#"+ i + "_displayInfantLastName").val() != 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passport Expiry Date"));				
						return false;
					}				
				}
				
				if(paxConfigIN.passportIssuedCntry.xbeVisibility && paxConfigIN.passportIssuedCntry.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxCatFOIDPlace == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxCatFOIDPlace == "") &&
							($("#"+ i + "_displayInfantFirstName").val() != 'T B A' ||
									$("#"+ i + "_displayInfantLastName").val() != 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passport Place of Issue"));				
						return false;
					}				
				}
				
				if(paxConfig.placeOfBirth.xbeVisibility && paxConfig.placeOfBirth.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxPlaceOfBirth == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayPnrPaxPlaceOfBirth == "") &&
							!($("#" + i + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + i + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passenger Place of Birth"));				
						return false;
					}				
				}
				
				if(paxConfig.travelDocumentType.xbeVisibility && paxConfig.travelDocumentType.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayTravelDocType == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayTravelDocType == "") &&
							!($("#" + i + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + i + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Travel Document Type"));				
						return false;
					}				
				}
				
				if(paxConfig.visaDocNumber.xbeVisibility && paxConfig.visaDocNumber.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayVisaDocNumber == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayVisaDocNumber == "") &&
							!($("#" + i + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + i + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Document Number"));				
						return false;
					}				
				}
				
				if(paxConfig.visaDocPlaceOfIssue.xbeVisibility && paxConfig.visaDocPlaceOfIssue.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayVisaDocPlaceOfIssue == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayVisaDocPlaceOfIssue == "") &&
							!($("#" + i + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + i + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Document Place Of Issue"));				
						return false;
					}				
				}
				
				if(paxConfig.visaDocIssueDate.xbeVisibility && paxConfig.visaDocIssueDate.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayVisaDocIssueDate == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayVisaDocIssueDate == "") &&
							!($("#" + i + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + i + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Document Issue Date"));				
						return false;
					}				
				}
				
				if(paxConfig.visaApplicableCountry.xbeVisibility && paxConfig.visaApplicableCountry.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i-1].displayVisaApplicableCountry == null || 
							jsonPaxAdtObj.paxInfants[i-1].displayVisaApplicableCountry == "") &&
							!($("#" + i + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + i + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Applicable Country"));				
						return false;
					}				
				}				
			}
		}

		// Validate UserNotes Field
		if ($.trim($("#txtUNotes").val()).length > 255) {
			showERRMessage(raiseError("XBE-ERR-18","255", "User Notes"));
			$("#txtUNotes").focus();
			return false ;
		} else if(!UI_commonSystem.validateInvalidChar([{id:"#txtUNotes", desc:"User Notes"}])) {
			return false;
		}
		
		// Validate endosments Field
		if ($.trim($("#txtEndosment").val()).length > 500) {
			showERRMessage(raiseError("XBE-ERR-18","500", "Endorsments"));
			$("#txtEndosment").focus();
			return false ;
		} else if(!UI_commonSystem.validateInvalidChar([{id:"#txtEndosment", desc:"Endorsments"}])) {
			return false;
		}
		return true;
	}
	
	UI_tabBookingInfo.ffidChanged = function(oldFFID, newFFID){
		var oldFFIDEmpty = (oldFFID == null || oldFFID == "") ? true : false;
		var newFFIDEmpty = (newFFID == null || newFFID == "") ? true : false;
		var changed = false;
		
		if (oldFFIDEmpty && newFFIDEmpty) {
			changed = false;
		} else if (!oldFFIDEmpty && !newFFIDEmpty) {
			changed = newFFID.toUpperCase() != oldFFID.toUpperCase()
		} else {
			changed = true;
		}
		
		return changed;
	}
	
	UI_tabBookingInfo.displayAutoCnxToolTip = function(){
		var alertTemp = UI_tabBookingInfo.autoCnxAlertMap[this.id];
		var tooltipId = "#autoCnxToolTip";
		var tooltipBodyId = "#autoCnxToolTipBody";
		if(alertTemp == null || (typeof(alertTemp) == 'undefined')){
			alertTemp = UI_reservation.jsonAutoCnxAlert != null ? UI_reservation.jsonAutoCnxAlert.content : null;
		}		
		_generateReservationToolTip(alertTemp, this.id, 230 , 20, tooltipId, tooltipBodyId);
	}
	
	UI_tabBookingInfo.displayAutoCnxToolTipInfant = function(){
		var alertTemp = UI_tabBookingInfo.autoCnxAlertMap[this.id];
		var tooltipId = "#autoCnxToolTip";
		var tooltipBodyId = "#autoCnxToolTipBody";
		if(alertTemp == null || (typeof(alertTemp) == 'undefined')){
			alertTemp = UI_reservation.jsonAutoCnxAlert != null ? UI_reservation.jsonAutoCnxAlert.content : null;
		}
		_generateReservationToolTip(alertTemp, this.id, 0 , 20, tooltipId, tooltipBodyId);
	}
	
	UI_tabBookingInfo.displayAutoCnxToolTipAnci = function(){
		var alertTemp = UI_tabBookingInfo.autoCnxAlertMap[this.id];
		var tooltipId = "#autoCnxToolTip";
		var tooltipBodyId = "#autoCnxToolTipBody";
		if(alertTemp == null || (typeof(alertTemp) == 'undefined')){
			alertTemp = UI_reservation.jsonAutoCnxAlert != null ? UI_reservation.jsonAutoCnxAlert.content : null;
		}		
		_generateReservationToolTip(alertTemp, this.id, 260 , 20, tooltipId, tooltipBodyId);
	}
	
	UI_tabBookingInfo.displayAutoCnxBalToPayToolTip = function(elemId){
		var alertTemp = UI_tabBookingInfo.autoCnxAlertMap[elemId];
		var tooltipId = "#autoCnxToolTip";
		var tooltipBodyId = "#autoCnxToolTipBody";
		if(typeof(alertTemp) != 'undefined'){
			var left = 0;
			var top = 20;
			
			if(elemId.indexOf('td_') >= 0){
				left = 230;
			} else if(elemId.indexOf('InfImg_') >= 0 ||
					elemId.indexOf('AutoCnxBalToPayImg') >= 0){
				left = 0;
			} else {
				left = 260;
			}
			
			_generateReservationToolTip(alertTemp, elemId, left, top, tooltipId, tooltipBodyId);
		}
	}
	
	_generateReservationToolTip = function(content, elemId, left, top, tooltipId, tooltipBodyId){
		var sHtm  ="<img src='../images/wait30trans.gif' alt='' style='display:block;margin:0 auto'/>";
		if (elemId != "" && content != null) {
			sHtm = "<hr><font style='font-size:14px'>" + content + "</font></hr>";
		} 
		
		$(tooltipBodyId).html(sHtm);	 
		var pos = $("#" + elemId).offset();
		var width = $("#" + elemId).width();
		$(tooltipId).css({
			left: (pos.left) - left + 'px',
			top: pos.top + top + 'px'			  
		});      
		$(tooltipId).show();
	}

  /*
	 * Display tooltip - Alert Information
	 * 
	 */
  UI_tabBookingInfo.displayToolTip = function() {	
	  var sHtm  ="";
	  var objAltDt = jsonAlertDetails;
	  if (this.id != "") {
			var arrID = this.id.split("_");
			var id = (new  Number(arrID[1])) - 0;
			var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;			
			var strData = "";	
			if (pnrSegID != "" && objAltDt.length != 0) {
				for (var i = 0; i < objAltDt.length; i++) {
					if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
						for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++){
							if (strData != ""){ strData += "<hr>";}
							strData += "<font>" + objAltDt[i].alertTO[a].content + "</font>"							
						}						
						break;
					}
				}
				sHtm += strData;
			}					
	  } 
	  $("#toolTipBody").html(sHtm);	 
	  var pos = $("#" + this.id).offset();
      var width = $("#" +this.id).width();
      $("#toolTip").css({
          left: (pos.left) -145 + 'px',
          top: pos.top + 20 + 'px'
         
      });      
     $("#toolTip").show(); 	  
	}
  /*
	 * Hide Alert data
	 * 
	 */
  UI_tabBookingInfo.hideToolTip = function() {
	  $("#toolTip").hide();
	  $("#autoCnxToolTip").hide();
	  $("#PromoCodeToolTip").hide();
  }
  /*
	 * Load popup window
	 */
  UI_tabBookingInfo.displayWindow = function(id) {	  
	 // var arrID = this.id.split("_");
	  var arrID = id.split("_");
	  var id = arrID[1];
	  UI_tabBookingInfo.carrirCode = jsonFltDetails[id].filghtOperatingCarrier;	
	  UI_tabBookingInfo.PNRSegID = jsonFltDetails[id].flightSegmentRefNumber;     
	  UI_tabBookingInfo.referenceID = id;
	  if (jsonAlertDetails.alertID != "") {
		  try {									
				var strUrl = "loadAlert.action?isGdsPnr=" + isGdsPnr;
				if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
				top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(340, 660));
			}catch(e){
				top.objCW = null;
			}
	  }
  };

    /*
     * Load popup window for clear all
     */
    UI_tabBookingInfo.displayWindowForClearAllAlert = function (id) {

        UI_tabBookingInfo.carrirCode = jsonFltDetails[id].filghtOperatingCarrier;
        for (var i = 0; i < jsonFltDetails.length; i++) {
            if (i == 0) {
                UI_tabBookingInfo.PNRSegID = jsonFltDetails[i].flightSegmentRefNumber;
            } else {
                UI_tabBookingInfo.PNRSegID = UI_tabBookingInfo.PNRSegID + "," + jsonFltDetails[i].flightSegmentRefNumber;
  }
        }
        UI_tabBookingInfo.referenceID = id;
        if (jsonAlertDetails.length > 0) {
            try {
                var strUrl = "loadAlert.action?isGdsPnr=" + isGdsPnr+"&isTransferable=false";
                if ((top.objCW) && (!top.objCW.closed)) {
                    top.objCW.close();
                }
                top.objCW = window.open(strUrl, "myWindow", $("#popWindow").getPopWindowProp(340, 660));
            } catch (e) {
                top.objCW = null;
            }
        }
    };

  /*
	 * Execute -Clear alert
	 */
  UI_tabBookingInfo.clearAlertConfirm = function(options) {
	    var data = UI_tabBookingInfo.getBookingInfo();
	    data["clearAlert.PNR"] = jsonPNR;
		data["clearAlert.groupPNR"] = jsonGroupPNR;
		data["segmentRefNo"] = UI_tabBookingInfo.PNRSegID;
		data["carrirCode"] = UI_tabBookingInfo.carrirCode;
		data["clearAlert.actionText"] = options.comment;
		data["clearAlert.actionTaken"] = options.selAction;
		data["resAlerts"] = $.toJSON(UI_reservation.jsonAlerts);		
		UI_tabBookingInfo.enableAllButtons(false);		
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"clearAlert.action",							
			success: UI_tabBookingInfo.clearAlertSuccess,error:UI_commonSystem.setErrorStatus});										
		return false;
		
  }
  /*
	 * Clear alert success
	 */
  UI_tabBookingInfo.clearAlertSuccess = function(response) {
	 UI_reservation.loadReservation("CLRALT", response,false);	  
  } 
  
  UI_tabBookingInfo.createAlertSuccess =  function(response) {
	  UI_reservation.loadReservation("CRTALT", response,false);
  } 

  UI_tabBookingInfo.clearAlertSuccessUpdate = function(response) {
	  	if(response.success) {			  		  
			  top.objCW.close();
			  showCommonError("CONFIRMATION", "Alert Cleared Successfully");
		  } else {				 		
			  top.objCW.UI_message.showErrorMessage({messageText:response.messageTxt});  
		  }
	  }

  /*
   * Set Transfer Segment Additional data
   * wheather connnection segment or selected alert segment 
   */
  UI_tabBookingInfo.setTransferSegmentAdditionalData = function(transferAction) {
	  $('#transferAction').val(transferAction);
  }

 
  /*
	 * Transfer segment
	 */
  UI_tabBookingInfo.transferSegment = function() {	
	  if (jsonFltDetails[UI_tabBookingInfo.referenceID].status == "CNX" && jsonGroupPNR != "") {		  
		  top.objCW.UI_message.showErrorMessage({messageText:"Canceled Segment Can Not Be Transfered For Interline Booking"});
		  return false;
	  } else if (UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef == null) {   
	  	  top.objCW.UI_message.showErrorMessage({messageText:raiseError("XBE-ERR-23","Segment")});
		  return false;
	  } else {
		  var flownFound = UI_tabBookingInfo.validateFlownSegment(UI_reservation.jsonSegs, UI_tabBookingInfo.PNRSegID);
		  var segmentModifiableAsPerETicketStatus = UI_tabBookingInfo.validateSegmentPerETicketStatus(UI_reservation.jsonSegs, UI_tabBookingInfo.PNRSegID);
		  
		  if (flownFound) {
			$('#pnrNo').val(jsonPNR);
			$('#groupPNRNo').val(jsonGroupPNR);
			$('#fltSegment').val(
					UI_tabBookingInfo.selectedFlightSeg.intLastSltdFltSegRef);
			$('#version').val(jsonBkgInfo.version);
			$('#resSegments').val(UI_reservation.jsonSegs);
			$('#bookingType').val(
					UI_tabBookingInfo.getModifySegmentBookingType());
			$('#resNoOfAdults').val(UI_reservation.adultCount);
			$('#resNoOfchildren').val(UI_reservation.childCount);
			$('#resNoOfInfants').val(UI_reservation.infantCount);
			$('#pgMode').val("transferSegment");
			$('#selectedAlertSegment').val(UI_tabBookingInfo.PNRSegID);      
			$('#ohdReleaseTime').val(UI_reservation.ohdReleaseTime);                       
			$('#gdsPnr').val(isGdsPnr);			
			$('#resAlerts').val($.toJSON(UI_reservation.jsonAlerts));
			$("#frmModiBkg").action("loadTransferSegment.action");	
			$("#frmModiBkg").submit();
			return true;
		  } else if (!segmentModifiableAsPerETicketStatus) {
			top.objCW.UI_message.showErrorMessage({
				messageText : "CHECKEDIN or BOARDED Segments Cannot Be Transfered"
				});
			return false;
		  } else {
			top.objCW.UI_message.showErrorMessage({
				messageText : "Flown Segments Cannot Be Transfered"
			});
			return false;
		}
	  }
  }  
  
  UI_tabBookingInfo.validateFlownSegment = function(jsonSegs, pnrSegID){
	  var resSegs = eval(UI_reservation.jsonSegs);
	  var flownFound = true;
	  if(resSegs != undefined && resSegs != null){
		  $.each(resSegs, function(index, resSeg){
			  if(resSeg.bookingFlightSegmentRefNumber === pnrSegID && resSeg.flownSegment){
				  flownFound = false;
			  }
		   });
	  }
	  return flownFound;
  }

  UI_tabBookingInfo.validateSegmentPerETicketStatus = function(jsonSegs, pnrSegID){
	  var resSegs = eval(UI_reservation.jsonSegs);
	  var segmentModifiableAsPerETicketStatus = true;
	  if(resSegs != undefined && resSegs != null){
		  $.each(resSegs, function(index, resSeg){
			  if(resSeg.bookingFlightSegmentRefNumber === pnrSegID && !resSeg.segmentModifiableAsPerETicketStatus){
				  segmentModifiableAsPerETicketStatus = false;
			  }
		   });
	  }
	  return segmentModifiableAsPerETicketStatus;
  }
  
  UI_tabBookingInfo.checkInfantMinAge = function(dateOfBirth, strAgeDate,infantMinAgeDays) {
		var sD = Number(dateOfBirth.substring(0,2));
		var sM = Number(dateOfBirth.substring(3,5)) - 1;
		var sY = Number(dateOfBirth.substring(6,10));

		var eD = Number(strAgeDate.substring(0,2));
		var eM = Number(strAgeDate.substring(3,5)) - 1;
		var eY = Number(strAgeDate.substring(6,10));

		var dob = new Date(sY, sM, sD);
		var depDate = new Date(eY, eM, eD);
		
		var one_day=1000*60*60*24
		var dobInMilSec = dob.getTime();
		var depDateInMilSec = depDate.getTime();
		
		var noOfDays = -1;
		
		if(depDateInMilSec > dobInMilSec) {
			noOfDays = Math.ceil((depDateInMilSec - dobInMilSec)/ (one_day));
		} 
		if(noOfDays > 0 && noOfDays <=infantMinAgeDays) {
			return false;
		} else {
			return true;
		}

	} 
  
  UI_tabBookingInfo.setStatusMessge = function() {
	  if (forwardMessage != null && forwardMessage != "") {
		  if (forwardMessage == "transferSegmentSuccess") {
			  showCommonError("CONFIRMATION", "Reservation Transfered Successfully");
		  }
	  }
  }
  
  UI_tabBookingInfo.refreshPopups = function(resinfo) {
		eval(resinfo);
		UI_reservation.refreshReservation();
	}
	
  /*
	 * Display tooltip - Flexi Information
	 * 
	 */
  UI_tabBookingInfo.displayFlexiToolTip = function() {	
	  var sHtm  ="";
	  var objAltDt = jsonFlexiDetails;
	  if (this.id != "") {
			var arrID = this.id.split("_");
			var id = (new  Number(arrID[1])) - 0;
			var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;			
			var strData = "";	
			if (pnrSegID != "" && objAltDt.length != 0) {
				for (var i = 0; i < objAltDt.length; i++) {
					if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
						strData += "<font>";
						for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++){
							if(objAltDt[i].alertTO[a].alertId == 1){// Modification
								var availModifications = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
								if(availModifications > 0){ // Only display if
															// the number of
															// modifications are
															// > 0
									strData += objAltDt[i].alertTO[a].content + " Modification(s) ";
									if(!(jsonBkgInfo.status == 'OHD')){
										UI_tabBookingInfo.modifyFlexiBooking = true;
									}
								}
							} else if (objAltDt[i].alertTO[a].alertId == 2){// Cancellation
								var availCancellations = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
								if(availCancellations > 0){ // Only display if
															// the number of
															// cancellations are
															// > 0
									strData += "Cancellation ";
								}
							} else if (objAltDt[i].alertTO[a].alertId == 0){// Additional
																			// details
								strData += "up to " + objAltDt[i].alertTO[a].content + " hours before departure";
							}
						}
						strData += "</font>";
						break;
					}
				}
				sHtm += strData;
			}					
	  } 
	  $("#flexiToolTipBody").html(sHtm);	 
	  var pos = $("#" + this.id).offset();
      var width = $("#" +this.id).width();
      $("#flexiToolTip").css({
          left: (pos.left) -200 + 'px',
          top: pos.top + 20 + 'px'
         
      });      
     $("#flexiToolTip").show(); 	  
	}
  
  
  /*
	 * Display tooltip - Bundled Fare Information
	 * 
	 */
UI_tabBookingInfo.displayBundledFareToolTip = function() {
	
	var sHtm  ="";
	var objAltDt = jsonSegmentWiseBundledFareDetails;
	if (this.id != "") {
		var arrID = this.id.split("_");
		var id = (new  Number(arrID[1])) - 0;
		var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;
		
		var bundledFareInfo = jsonSegmentWiseBundledFareDetails[pnrSegID];
		
		var bundledName = bundledFareInfo.bundledFareName;
		var freeServices = bundledFareInfo.applicableServiceNames.toString().replace(/,/g, ", ");
		var bookingClasses = bundledFareInfo.bookingClasses.toString() == "" ? "ALL" : bundledFareInfo.bookingClasses.toString();
		var perPaxFee = bundledFareInfo.perPaxBundledFee;
		 
		var strData = "";
		strData += "<font>Bundled Name&nbsp;:&nbsp;"+bundledName+"</font><br>";
		strData += "<font>Free Services&nbsp;:&nbsp;"+freeServices+"</font><br>";
		strData += "<font>Booking Classes&nbsp;:&nbsp;"+bookingClasses+"</font><br>";	
		strData += "<font>Per pax Fee&nbsp;:&nbsp;"+perPaxFee+"</font><br>";	
					
		sHtm += strData;
	}
	
	//Merge Flexi Alert to bundle alert
	var objAltDt = jsonFlexiDetails;
	if (this.id != "" && objAltDt != null && objAltDt.length > 0) {
		var arrID = this.id.split("_");
		var id = (new  Number(arrID[1])) - 0;
		var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;			
		var strData = "";	
		if (pnrSegID != "" && objAltDt.length != 0) {
			var flexiHdr = "<div style='border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;' align='center'>";
			flexiHdr += "<font size='6px' style='font-weight:bold'> Flexibilities </font><img src='../images/AA171_no_cache.gif'></div><br/>";
			for (var i = 0; i < objAltDt.length; i++) {
				if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
					strData += "<font>";
					for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++){
						if(objAltDt[i].alertTO[a].alertId == 1){// Modification
							var availModifications = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
							if(availModifications > 0){ // Only display if
														// the number of
														// modifications are
														// > 0
								strData += objAltDt[i].alertTO[a].content + " Modification(s) ";
								if(!(jsonBkgInfo.status == 'OHD')){
									UI_tabBookingInfo.modifyFlexiBooking = true;
								}
							}
						} else if (objAltDt[i].alertTO[a].alertId == 2){// Cancellation
							var availCancellations = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
							if(availCancellations > 0){ // Only display if
														// the number of
														// cancellations are
														// > 0
								strData += "Cancellation ";
							}
						} else if (objAltDt[i].alertTO[a].alertId == 0){// Additional
																		// details
							strData += "up to " + objAltDt[i].alertTO[a].content + " hours before departure";
						}
					}
					strData += "</font>";
					break;
				}
			}
			
			if(strData != ''){				
				sHtm += flexiHdr;
				sHtm += strData;
			}
		}					
	} 
	
	$("#bundledFareToolTipBody").html(sHtm);	 
	var pos = $("#" + this.id).offset();
    var width = $("#" +this.id).width();
    $("#bundledFareToolTip").css({
        left: (pos.left) -200 + 'px',
        top: pos.top + 20 + 'px'
       
    });      
	$("#bundledFareToolTip").show(); 
	
}
  /**
	 * use as common
	 */
  UI_tabBookingInfo.displaySegmentInfoToolTip = function() {
	  var sHtm  ="";
	  var objAltDt = jsonAdditionalSegInfo;
	  if (this.id != "") {
			var arrID = this.id.split("_");
			var id = (new  Number(arrID[1])) - 0;
			var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;			
			var strData = "";	
			if (pnrSegID != "" && objAltDt.length != 0) {
				for (var i = 0; i < objAltDt.length; i++) {
					if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
						strData += "<font>";
						for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++) {
									strData += "Your pick up / drop off location is "+objAltDt[i].alertTO[a].content + " ";
						}
						strData += "</font>";
						break;
					}
				}
			}
				sHtm += strData;					
	  } 
	  $("#segmentInfoToolTipBody").html(sHtm);	 
	  var pos = $("#" + this.id).offset();
      var width = $("#" +this.id).width();
      $("#segmentInfoToolTip").css({
          left: (pos.left) -200 + 'px',
          top: pos.top + 20 + 'px'
         
      });      
     $("#segmentInfoToolTip").show(); 	  
	}
  
	  UI_tabBookingInfo.hideSegmentInfoToolTip = function() {
		  $("#segmentInfoToolTip").hide();
	  }
	
	
  /**
	 * use as common
	 */
  UI_tabBookingInfo.displaySegmentInfoToolTip = function() {
	  var sHtm  ="";
	  var objAltDt = jsonAdditionalSegInfo;
	  if (this.id != "") {
			var arrID = this.id.split("_");
			var id = (new  Number(arrID[1])) - 0;
			var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;			
			var strData = "";	
			if (pnrSegID != "" && objAltDt.length != 0) {
				for (var i = 0; i < objAltDt.length; i++) {
					if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
						strData += "<font>";
						for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++) {
									strData += "Your pick up / drop off location is "+objAltDt[i].alertTO[a].content + " ";
						}
						strData += "</font>";
						break;
					}
				}
			}
				sHtm += strData;					
	  } 
	  $("#segmentInfoToolTipBody").html(sHtm);	 
	  var pos = $("#" + this.id).offset();
      var width = $("#" +this.id).width();
      $("#segmentInfoToolTip").css({
          left: (pos.left) -200 + 'px',
          top: pos.top + 20 + 'px'
         
      });      
     $("#segmentInfoToolTip").show(); 	  
	}
  
	  UI_tabBookingInfo.hideSegmentInfoToolTip = function() {
		  $("#segmentInfoToolTip").hide();
	  }
	
	
  /*
	 * Hide Alert data
	 * 
	 */
  UI_tabBookingInfo.hideFlexiToolTip = function() {
	  $("#flexiToolTip").hide();
  }
  
  UI_tabBookingInfo.hideBundledFareToolTip = function() {
	  $("#bundledFareToolTip").hide();
  }
  	  
	  /** Open Return segment tool tip */
	  UI_tabBookingInfo.displayOPRTSegmentInfoToolTip = function() {
		  var sHtm  ="";
		  var objAltDt = jsonOpenReturnSegInfo;
		  if (this.id != "") {
				var arrID = this.id.split("_");
				var id = (new  Number(arrID[1])) - 0;
				var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;			
				var strData = "";	
				if (pnrSegID != "" && objAltDt.length != 0) {
					for (var i = 0; i < objAltDt.length; i++) {
						if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
							strData += "<font>";
							for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++) {
										strData +=objAltDt[i].alertTO[a].content + " ";
							}
							strData += "</font>";
							break;
						}
					}
				}
					sHtm += strData;
					if(isConfirmDateExpired){
						sHtm += "<br><font style=color:red>Open Return Confirm Date Expired<font>";
					}
		  } 
		  $("#openReturnToolTipBody").html(sHtm);	 
		  var pos = $("#" + this.id).offset();
	      var width = $("#" +this.id).width();
	      $("#openReturnToolTip").css({
	          left: (pos.left) -200 + 'px',
	          top: pos.top + 20 + 'px'
	         
	      });      
	     $("#openReturnToolTip").show(); 	  
		}
	  
	  UI_tabBookingInfo.hideOPRTSegmentInfoToolTip = function() {
		  $("#openReturnToolTip").hide();
	  }
	  
		  
	  /** Flight Info tool tip display */
	  UI_tabBookingInfo.displayFlightAddInfoToolTip = function() {
		  var sHtm  ="";
		  var objAltDt = jsonOpenReturnSegInfo;
		  if (this.id != "") {
			  var arrID = this.id.split("_");
			  var id = (new  Number(arrID[1])) - 0;
			  var fltDuration = jsonFltDetails[id].flightDuration;
			  var fltModelDesc = jsonFltDetails[id].flightModelDescription;
			  var fltModel = jsonFltDetails[id].flightModelNumber;
			  var segmentCode = jsonFltDetails[id].orignNDest;
			  var flightNO = jsonFltDetails[id].flightNo;	
			  var specialNote = jsonFltDetails[id].remarks;
			  var stopOverDuration = jsonFltDetails[id].flightStopOverDuration;
			  var noOfStops = jsonFltDetails[id].noOfStops;
			  var arrivalTerminalName = jsonFltDetails[id].arrivalTerminal;
			  var departureTerminalName = jsonFltDetails[id].departureTerminal;
			  var csOcCarrierCode = jsonFltDetails[id].csOcCarrierCode;
			
			  if(arrivalTerminalName == null){
				  arrivalTerminalName = "";}
			  
			  if(departureTerminalName == null){
				  departureTerminalName = "";}
			 
			  var strData = "";
					
			    strData += "<font>Flight No&nbsp;:&nbsp;"+flightNO+"</font><br>";
				strData += "<font>Segment Code&nbsp;:&nbsp;"+segmentCode+"</font><br>";
				strData += "<font>Arrival Terminal&nbsp;:&nbsp;"+arrivalTerminalName+"</font><br>";	
				strData += "<font>Departure Terminal&nbsp;:&nbsp;"+departureTerminalName+"</font><br>";	
				strData += "<font>Flight Duration&nbsp;:&nbsp;"+fltDuration+"</font><br>";
				strData += "<font>No of Stops&nbsp;:&nbsp;"+noOfStops+"</font><br>";
				strData += "<font>Transit Duration&nbsp;:&nbsp;"+stopOverDuration+"</font><br>";				
				// strData += "<font>Aircraft
				// Model&nbsp;:&nbsp;"+fltModel+"</font><br>";
				strData += "<font>Aircraft Model&nbsp;:&nbsp;"+fltModelDesc+"</font><br>";
				strData += "<font>Additional Note&nbsp;:&nbsp;"+specialNote+"</font><br>";
				if(typeof(csOcCarrierCode) != "undefined" && csOcCarrierCode != "" && csOcCarrierCode != null){
					strData += "<font>Operated By&nbsp;:&nbsp;"+csOcCarrierCode+"</font><br>";						
				}	
						
				sHtm += strData;	
		  } 
		  $("#flightAddInfoToolTipBody").html(sHtm);	 
		  var pos = $("#" + this.id).offset();
	      
	      $("#flightAddInfoToolTip").css({
	          left: (pos.left-100) + 'px',
	          top: pos.top + 20 + 'px'
	         
	      });      
	     $("#flightAddInfoToolTip").show(); 	  
		}
	  
	  UI_tabBookingInfo.hideFlightAddInfoToolTip = function() {
		  $("#flightAddInfoToolTip").hide();
	  }
	  	  
	  
	  /** Segment Return validity tool tip */
	  UI_tabBookingInfo.displayReturnValidityInfoToolTip = function() {
		  var sHtm  ="";
		  var objAltDt = jsonOpenReturnSegInfo;
		  if (this.id != "") {
				var arrID = this.id.split("_");
				var id = (new  Number(arrID[1])) - 0;
				var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;	
				var ticketValidTill = jsonFltDetails[id].ticketValidTill;
				
				var strData = "";	
				   strData += "<font>Valid Till/Travel Before&nbsp;:&nbsp;"+ticketValidTill+"</font><br>";
					sHtm += strData;					
		  } 
		  $("#returnValidityToolTipBody").html(sHtm);	 
		  var pos = $("#" + this.id).offset();
	      var width = $("#" +this.id).width();
	      $("#returnValidityToolTip").css({
	          left: (pos.left) -200 + 'px',
	          top: pos.top + 20 + 'px'
	         
	      });      
	     $("#returnValidityToolTip").show(); 	  
		}
	  
	  UI_tabBookingInfo.hideReturnValidityInfoToolTip = function() {
		  $("#returnValidityToolTip").hide();
	  }
  
  /*
	 * Check this is a flexi reservation
	 * 
	 */
  UI_tabBookingInfo.isFlexiBooking = function(flightSegmentRefNumber) {
	  var objAltDt = jsonFlexiDetails;
	  UI_tabBookingInfo.modifyFlexiBooking = false;
	  if (objAltDt != null && objAltDt.length > 0) {
		for (var i = 0; i < objAltDt.length; i++) {
			if (flightSegmentRefNumber == objAltDt[i].flightSegmantRefNumber) {
				for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++){
					// Display either number of modifications cancellations are
					// > 0
					if(objAltDt[i].alertTO[a].alertId == 1){// Modification
						var availModifications = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
						if(availModifications > 0){
							if(!(jsonBkgInfo.status == 'OHD')){
								UI_tabBookingInfo.modifyFlexiBooking = true;
							}
							break;
						}
					} else if(objAltDt[i].alertTO[a].alertId == 2){// Cancellation
						var availCancellations = (new  Number(objAltDt[i].alertTO[a].content)) - 0;
						if(availCancellations > 0){ 
							if(!(jsonBkgInfo.status == 'OHD')){
								UI_tabBookingInfo.modifyFlexiBooking = true;
							}
							break;
						}
					}
				}
				break;
			}
		}
	  }
	}
  
  
  UI_tabBookingInfo.viewGroupItineraryOnClick = function(iniOpt) {
		
		var chkBoxesArray =new Array();
		
		$('#btnPaxItiEmail').decorateButton();		
		$('#btnPaxItiPrint').decorateButton();
		$('#btnPaxItiCancel').decorateButton();
		$('#btnPaxItiIndPrint').decorateButton();
		$('#btnPaxItiIndEmail').decorateButton();
		
		$('#btnPaxItiEmail').disable();
		$('#btnPaxItiPrint').disable();
		$('#btnPaxItiIndPrint').disable();
		$('#btnPaxItiIndEmail').disable();
		
		
		$('#divGroupItineraryDisplay').show();				
		$('#chkPaxSelectAll').attr('checked',false);
		
		var firstElement = null;
		var paxList = jsonPaxAdtObj.paxAdults;
		$('#trPaxListTemp').parent().find('tr').filter(function() {			
			var regex = new RegExp('xxx_*');
	        return regex.test(this.id);
	    }).remove();
	
		for(var i=0; i < paxList.length; i++){
			var tempId = 'xxx_'+paxList[i].displayPaxSequence;
			var chkId = 'chkPax_'+paxList[i].displayPaxSequence;
			
			var clone = $('#trPaxListTemp').clone().show();
			clone.attr('id',tempId);
			clone.appendTo($('#trPaxListTemp').parent());  
			
			/*
			 * if(paxList[i].displayAdultType == 'IN'){ continue; }
			 */
			
			var chBox = $('#'+tempId+' input');
			chBox.attr('id',chkId);
			chBox.attr('tabIndex',9000+i);
			chBox.val(paxList[i].displayPaxSequence);			
			chBox.attr('style','display:show');			
			chBox.click(function(){
				var paxSelected = false;
				if(this.checked){
					$('#btnPaxItiEmail').enable();
					$('#btnPaxItiPrint').enable();
					$('#btnPaxItiIndPrint').enable();
					$('#btnPaxItiIndEmail').enable();
				}
				
				for(var j=0; j<chkBoxesArray.length; j++){			
					var checkbox = chkBoxesArray[j];
					if(checkbox.get(0).checked){						
						paxSelected = true;						
					}			
				}
				
				if(paxSelected){					
					$('#btnPaxItiEmail').enable();
					$('#btnPaxItiPrint').enable();
					$('#btnPaxItiIndPrint').enable();
					$('#btnPaxItiIndEmail').enable();
				} else{					
					$('#btnPaxItiEmail').disable();
					$('#btnPaxItiPrint').disable();
					$('#btnPaxItiIndPrint').disable();
					$('#btnPaxItiIndEmail').disable();
				}
			});
			
			chkBoxesArray[i] = chBox;
			
			if(firstElement==null){
				firstElement = chkId;
				chBox.attr('checked',true);  
			}
			
			var label = $('#'+tempId+' label');
			label.attr('id','lblPax_'+paxList[i].displayPaxSequence);
			var ptitle = paxList[i].displayAdultTitle;
			ptitle = (ptitle == null || ptitle == 'null')?'':ptitle;
			label.text( ptitle + ' ' + paxList[i].displayAdultFirstName + ' ' + paxList[i].displayAdultLastName);			
			label.click(function(e){ var a = e.target.id.split('_'); $('#chkPax_'+a[1]).click()});
			
			var contObj = $.parseJSON(UI_reservation.contactInfo);
			if(iniOpt == 1 || contObj == null || contObj.email == null || contObj.email == ''){
				$("#btnPaxItiEmail").hide();
				$('#btnPaxItiIndEmail').hide();
				$("#btnPaxItiPrint").show();
				$('#btnPaxItiIndPrint').show();
			}else{
				$("#btnPaxItiEmail").show();
				$('#btnPaxItiIndEmail').show();
				$("#btnPaxItiPrint").hide();
				$('#btnPaxItiIndPrint').hide();
			}
		}
		
		if(iniOpt == 1){
			$("#btnPaxItiPrint").show();
			$('#btnPaxItiIndPrint').show();
		} else {
			$("#btnPaxItiPrint").hide();
			$('#btnPaxItiIndPrint').hide();			
		}
		
		for(var j=0; j<chkBoxesArray.length; j++){			
			var checkbox = chkBoxesArray[j];			
			if(checkbox.get(0).checked){
				$('#btnPaxItiEmail').enable();
				$('#btnPaxItiPrint').enable();
				$('#btnPaxItiIndPrint').enable();
				$('#btnPaxItiIndEmail').enable();
			}			
		}		
		UI_tabBookingInfo.grpBookingPax = chkBoxesArray;		
	}
  
   UI_tabBookingInfo.comparePax = function(paxA, paxB){
  	   return parseFloat(paxA.displayPaxSequence) - parseFloat(paxB.displayPaxSequence);
  	}
    
   UI_tabBookingInfo.emailGroupItineraryOnClick = function(){		
		var grpBookingPax = UI_tabBookingInfo.grpBookingPax;
		var selectedPax = "";		
		for(var j=0; j<grpBookingPax.length; j++){			
			var checkbox = grpBookingPax[j];			
			if(checkbox.get(0).checked){
				$('#btnPaxItiEmail').enable();
				$('#btnPaxItiPrint').enable();				
				selectedPax += checkbox.get(0).value + ',';
			}			
		}		
		
		UI_commonSystem.pageOnChange();
		var includePaxFinancials = true; // Default to true
		if($("#chkPax").length > 0) {  
			includePaxFinancials = $("#chkPax").prop("checked");
		}
		var includePaymentDetails = true; // Default to true
		if($("#chkPay").length > 0) {
			includePaymentDetails = $("#chkPay").prop("checked");
		}
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}

		// TODO : Charges breakdown not part of release plan
		var includeTicketCharges = $("#chkChg").prop("checked");
		// var includeTicketCharges = false;
		
		var itineraryLanguage = $("#selITNLang").val();
		
		$('#divGroupItineraryDisplay').hide();
		var data = {};		
		data["hdnPNRNo"] = jsonBkgInfo.PNR;
		data["groupPNR"]= UI_tabBookingInfo.isInterlineDryRes(jsonGroupPNR);
		data["includePaxFinancials"] = includePaxFinancials;
		data["includePaymentDetails"] = includePaymentDetails;
		data["includeTicketCharges"] = includeTicketCharges;
		data["itineraryLanguage"] = itineraryLanguage;
		data["selectedPax"] = selectedPax;
		data["includeTermsAndConditions"] = includeTCDetails;
		data["operationType"] = "MODIFY_BOOKING"
		
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"emailtItinerary.action",							
			success: UI_tabBookingInfo.emailSuccess, error:UI_commonSystem.setErrorStatus});		
	}
	 

   UI_tabBookingInfo.printGroupItineraryOnClick = function(){	   
		var grpBookingPax = UI_tabBookingInfo.grpBookingPax;
		var selectedPax = "";		
		for(var j=0; j<grpBookingPax.length; j++){			
			var checkbox = grpBookingPax[j];			
			if(checkbox.get(0).checked){
				$('#btnPaxItiEmail').enable();
				$('#btnPaxItiPrint').enable();				
				selectedPax += checkbox.get(0).value + ',';
			}			
		}		
				
		UI_commonSystem.pageOnChange();
		var includePaxFinancials = true; // Default to true
		if($("#chkPax").length > 0) {  
			includePaxFinancials = $("#chkPax").prop("checked");
		}
		var includePaymentDetails = true; // Default to true
		if($("#chkPay").length > 0) {
			includePaymentDetails = $("#chkPay").prop("checked");
		}
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}
		
		var includeTicketCharges = $("#chkChg").prop("checked");
		// var includeTicketCharges = false;
		
		var itineraryLanguage = $("#selITNLang").val();
		$('#divGroupItineraryDisplay').hide();		
		
		var strUrl = "printItinerary.action"
		+ "?hdnPNRNo=" + jsonBkgInfo.PNR
		+ "&groupPNR=" + UI_tabBookingInfo.isInterlineDryRes(jsonGroupPNR)
		+ "&includePaxFinancials=" + includePaxFinancials
		+ "&includePaymentDetails=" + includePaymentDetails
		+ "&includeTicketCharges=" + includeTicketCharges
		+ "&itineraryLanguage=" + itineraryLanguage
		+ "&selectedPax=" + selectedPax
		+ "&includeTermsAndConditions=" +includeTCDetails
		+ "&operationType='MODIFY_BOOKING'";
		
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		if (top.objCW) {top.objCW.focus()}		
	}
   
   
   UI_tabBookingInfo.printIndividualItineraryOnClick = function(){	   
		var grpBookingPax = UI_tabBookingInfo.grpBookingPax;
		var selectedPax = "";		
		for(var j=0; j<grpBookingPax.length; j++){			
			var checkbox = grpBookingPax[j];			
			if(checkbox.get(0).checked){
				$('#btnPaxItiEmail').enable();
				$('#btnPaxItiPrint').enable();				
				selectedPax += checkbox.get(0).value + ',';
			}			
		}		
				
		UI_commonSystem.pageOnChange();
		var includePaxFinancials = true; // Default to true
		if($("#chkPax").length > 0) {  
			includePaxFinancials = $("#chkPax").prop("checked");
		}
		var includePaymentDetails = true; // Default to true
		if($("#chkPay").length > 0) {
			includePaymentDetails = $("#chkPay").prop("checked");
		}
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}
		
		var includeTicketCharges = $("#chkChg").prop("checked");
		// var includeTicketCharges = false;
		
		var itineraryLanguage = $("#selITNLang").val();
		$('#divGroupItineraryDisplay').hide();
		
		var strUrl = "printItinerary!printIndividualPaxItinerary.action"
		+ "?hdnPNRNo=" + jsonBkgInfo.PNR
		+ "&groupPNR=" + UI_tabBookingInfo.isInterlineDryRes(jsonGroupPNR)
		+ "&includePaxFinancials=" + includePaxFinancials
		+ "&includePaymentDetails=" + includePaymentDetails
		+ "&includeTicketCharges=" + includeTicketCharges
		+ "&itineraryLanguage=" + itineraryLanguage
		+ "&selectedPax=" + selectedPax
		+ "&includeTermsAndConditions=" +includeTCDetails
		+ "&operationType='MODIFY_BOOKING'";
		
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		if (top.objCW) {top.objCW.focus()}		
	}
   
   UI_tabBookingInfo.emailIndividualItineraryOnClick = function(){		
		var grpBookingPax = UI_tabBookingInfo.grpBookingPax;
		var selectedPax = "";		
		for(var j=0; j<grpBookingPax.length; j++){			
			var checkbox = grpBookingPax[j];			
			if(checkbox.get(0).checked){
				$('#btnPaxItiEmail').enable();
				$('#btnPaxItiPrint').enable();				
				selectedPax += checkbox.get(0).value + ',';
			}			
		}		
		
		UI_commonSystem.pageOnChange();
		var includePaxFinancials = true; // Default to true
		if($("#chkPax").length > 0) {  
			includePaxFinancials = $("#chkPax").prop("checked");
		}
		var includePaymentDetails = true; // Default to true
		if($("#chkPay").length > 0) {
			includePaymentDetails = $("#chkPay").prop("checked");
		}
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}

		// TODO : Charges breakdown not part of release plan
		var includeTicketCharges = $("#chkChg").prop("checked");
		// var includeTicketCharges = false;
		
		var itineraryLanguage = $("#selITNLang").val();
		
		$('#divGroupItineraryDisplay').hide();
		var data = {};		
		data["hdnPNRNo"] = jsonBkgInfo.PNR;
		data["groupPNR"]=  UI_tabBookingInfo.isInterlineDryRes(jsonGroupPNR);
		data["includePaxFinancials"] = includePaxFinancials;
		data["includePaymentDetails"] = includePaymentDetails;
		data["includeTicketCharges"] = includeTicketCharges;
		data["itineraryLanguage"] = itineraryLanguage;
		data["selectedPax"] = selectedPax;
		data["includeTermsAndConditions"] = includeTCDetails;
		data["operationType"] = "MODIFY_BOOKING";
		
		$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"emailtItinerary!printIndividualPaxItinerary.action",							
			success: UI_tabBookingInfo.emailSuccess, error:UI_commonSystem.setErrorStatus});		
	}
   
   UI_tabBookingInfo.emailItineraryOnClick = function(){
	   
	   if(((jsonPaxAdtObj.paxAdults.length > 1) &&
			   (top.arrPrivi[PRIVI_ITN_SELECTPAX] == 1)) || (top.printIndItinerary == 'true' || top.printIndItinerary == true)){
		   UI_tabBookingInfo.viewGroupItineraryOnClick(0);		   
	   } else {
		   UI_tabBookingInfo.emailItinerary();
	   }
   }
   
   UI_tabBookingInfo.viewItineraryOnClick = function(){
	   if((jsonPaxAdtObj.paxAdults.length > 1 ) &&
			   (top.arrPrivi[PRIVI_ITN_SELECTPAX] == 1)){
		   UI_tabBookingInfo.viewGroupItineraryOnClick(1);		   
	   } else {
		   UI_tabBookingInfo.printItinerary();
	   }
   }
   
  
   selectAllPaxOnClick = function(obj){	  
	   var grpBookingPax = UI_tabBookingInfo.grpBookingPax;
	   for(var i=0; i<grpBookingPax.length; i++){		   
		   var checkBox = grpBookingPax[i];		   
		   if(obj.checked == true){
				   checkBox.get(0).checked = true;		   	   
		   } else {
			   checkBox.get(0).checked = false;
		   }	   
	   }
	   
	   if(obj.checked == true){		   
		   $('#btnPaxItiEmail').enable();
		   $('#btnPaxItiPrint').enable();
		   $('#btnPaxItiIndEmail').enable();
		   $('#btnPaxItiIndPrint').enable();
	   } else {		   
		   $('#btnPaxItiEmail').disable();
		   $('#btnPaxItiPrint').disable();
		   $('#btnPaxItiIndEmail').disable();
		   $('#btnPaxItiIndPrint').disable();
	   }
   }
  
   
   UI_tabBookingInfo.printRecieptOnClick = function(){
	   var itineraryLanguage = $("#selITNLang").val();
		
	    var isGroupPNR = (jsonGroupPNR!=null && jsonGroupPNR!= '');
		var strUrl = "printReciept.action"
		+ "?hdnPNRNo=" + jsonBkgInfo.PNR
		+ "&groupPNR=" + isGroupPNR
		+ "&recieptLanguage=" + itineraryLanguage;
		
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		if (window.focus) {top.objCW.focus()}	
	}
   
   UI_tabBookingInfo.convertDeptDate = function(deptDateStr){
	   
	   if (deptDateStr.indexOf(" ") > 0) {
		   deptDateStr = deptDateStr.split(" ")[1];
		  }
	   var deptDateArr = deptDateStr.split('/');
	   return new Date(deptDateArr[1] + '/' + deptDateArr[0] + '/' + deptDateArr[2]);
   }
   
   UI_tabBookingInfo.getMinDateForRollForward = function(allowedDate){
		var minDate = new Date();
		if(allowedDate != null && allowedDate != ""){
			var dtArr = allowedDate.split('/');
			minDate = new Date(parseInt(dtArr[2], 10), parseInt(dtArr[1], 10)-1, parseInt(dtArr[0], 10));
		}
		
		return minDate;
		
	}
   
   UI_tabBookingInfo.showOHDRollForward = function(){
   		$("#rollForwardStartDate").val('');
		$("#rollForwardEndDate").val('');
		$("#trOhdRFConfirm").hide();
		
		if($("#chkOHDRFOvrdOHDBuffer").attr('checked')){
			$("#chkOHDRFOvrdOHDBuffer").attr('checked', false)
			$("#chkOHDRFOvrdOHDBuffer").click();
			$("#chkOHDRFOvrdOHDBuffer").attr('checked', false)
		}
		if($('#chkOhdRFAll').attr('checked')){
			$('#chkOhdRFAll').attr('checked', false);
			$('#chkOhdRFAll').click();
			$('#chkOhdRFAll').attr('checked', false);
		}
		
		$('#trOHDRollForwardTemp').parent().find('tr').filter(function() {			
			var regex = new RegExp('trOHDRollForwardTempd_*');
	        return regex.test(this.id);
	    }).remove();
		UI_commonSystem.readOnly(true);
		$("#divRFAvailFlights").hide();
   		$('#divOHDRollForward').show();
   }
   
   UI_tabBookingInfo.ohdRollForwardOnClick = function(){
   	if (!UI_tabBookingInfo.ohdRFSubmitted) {
		UI_tabBookingInfo.enableRollForwardButtons(false);
		$("#divRFAvailFlights").hide();
		if(!UI_tabBookingInfo.validateOhdRFSearch()){
			return false;
		}
		UI_tabBookingInfo.ohdRFSubmitted = true;
		var data = new Array();
		data['pnr'] = jsonBkgInfo.PNR
		data['jsonResSegs'] = UI_reservation.jsonSegs;
		data['startDate'] = $("#rollForwardStartDate").val();
		data['endDate'] = $("#rollForwardEndDate").val();
		if($("#chkRollForwardOverBook").attr('checked')){
			data['overBook'] = true;
		} 
		data['adultCount'] = UI_reservation.adultCount;
		data['childCount'] = UI_reservation.childCount;
		data['infantCount'] = UI_reservation.infantCount;
		data['system'] = jsonSystem;
		UI_commonSystem.getDummyFrm().ajaxSubmit({
			dataType: 'json',
			data: data,
			async: true,
			url: "onholdRollForwardAvailability.action",
			success: UI_tabBookingInfo.rollforwardSearchSuccess,
			error: UI_tabBookingInfo.ohdRollforwardError
		});
	}
   }
   
   UI_tabBookingInfo.validateOhdRFSearch = function(){
   		var startDateStr = $("#rollForwardStartDate").val();
		var endDateStr = $("#rollForwardEndDate").val();
		
		if (startDateStr=="" || startDateStr==null){
			showERRMessage(raiseError("XBE-ERR-01","From Date"));
			$("#rollForwardStartDate").focus();		
			UI_tabBookingInfo.enableRollForwardButtons(true);
			return false;
		}
		
		if (endDateStr=="" || endDateStr==null){
			showERRMessage(raiseError("XBE-ERR-01","To Date"));
			$("#rollForwardEndDate").focus();		
			UI_tabBookingInfo.enableRollForwardButtons(true);
			return false;
		}
		
		if (!CheckDates(startDateStr, endDateStr)){
			showERRMessage(raiseError("XBE-ERR-03","To date", "from date"));
			$("#rollForwardEndDate").focus();	
			UI_tabBookingInfo.enableRollForwardButtons(true);
			return false;
		}
		
		return true;		
   }
   
   UI_tabBookingInfo.rollforwardSearchSuccess = function(response){
   		UI_tabBookingInfo.enableRollForwardButtons(true);
   		UI_tabBookingInfo.ohdRFSubmitted  = false;
		
		$('#trOHDRollForwardTemp').parent().find('tr').filter(function() {			
			var regex = new RegExp('trOHDRollForwardTempd_*');
	        return regex.test(this.id);
	    }).remove();
		
		var dateInt = function(strDate){
			  var d = strDate.split('-');
			  var dd= new Date(d[0],parseInt(d[1],10)-1,d[2],0,0,0);
			  return dd.getDay();
		}
		
		if(response.success){
			isDuplicateNameExist = response.duplicateNameExist;
			if(isDuplicateNameExist && !DATA_ResPro.initialParams.duplicateNameSkipPrivilege ){ 
				showERRMessage(raiseError("XBE-ERR-71"));
			} else if(response.availFlightRS != null &&
				response.availFlightRS != ""){
					$("#divRFAvailFlights").show();
					var rollForwardConfirmEnabled = false;
					UI_tabBookingInfo.rollForwardAvailFlights = response.availFlightRS;
					
					for(var i=0;i<response.availFlightRS.length;i++){
						var deptDateTime = response.availFlightRS[i].departureDate;
						var deptDate = (deptDateTime!=null)?deptDateTime.split("T")[0]:"";
						
						var tmpId = 'trOHDRollForwardTempd_' + deptDate;
						var chId = 'chkOhdRF_' + deptDate;
						
						var clone = $('#trOHDRollForwardTemp').clone().show();
						clone.attr('id',tmpId);
						clone.appendTo($('#trOHDRollForwardTemp').parent());  
						
						var chBox = $('#'+tmpId+' input');
						chBox.attr('id',chId);		
						chBox.attr('tabIndex',9000+i);
						chBox.val(i);
											
						var label = $('#'+tmpId+' label');
						label.attr('id','lblOhdRF_'+deptDate);
						
						var dispLabel = deptDate;
						chBox.addClass('rfDate');
						if(response.availFlightRS[i].rollForwardEnabled){
							rollForwardConfirmEnabled = true;
							dispLabel += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Available";
							chBox.addClass(dateInt(deptDate)+'');
							chBox.addClass('7');
						} else {
							chBox.attr("disabled", true);
							dispLabel += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Not Available";
						}
						
						label.html(dispLabel);
						label.click(function(e){ var a = e.target.id.split('_'); $('#chkOhdRF_'+a[1]).click()});
					}
					
					if(rollForwardConfirmEnabled){
						$("#trOhdRFConfirm").show();
					} else {
						$("#trOhdRFConfirm").hide();
					}
			}
		}
   }
   
   UI_tabBookingInfo.ohdRollforwardError = function(response){
   		UI_tabBookingInfo.enableRollForwardButtons(true);
   		UI_tabBookingInfo.ohdRFSubmitted = false;
   }
   
   UI_tabBookingInfo.ohdRollForwardConfirmOnClick = function(){
	   if(!isDuplicateNameExist || UI_tabBookingInfo.isAllowDuplicateName()){
   		UI_tabBookingInfo.enableRollForwardButtons(false);
   		var checkedArr = $("#divRFAvailFlights input:checked.rfDate");
   		var count = checkedArr.length;
		
		if(count == 0){
			alert("Please select atleast one flight to roll forward");
			UI_tabBookingInfo.enableRollForwardButtons(true);
		} else {
			var submitFlights = [];
			for(var i=0;i<count;i++){
				submitFlights[i] = UI_tabBookingInfo.rollForwardAvailFlights[checkedArr[i].value];
			}
			
			var data = new Array();
			data['pnr'] = jsonBkgInfo.PNR;
			data['jsonResSegs'] = UI_reservation.jsonSegs;
			data['jsonRollForwardFlights'] = $.toJSON(submitFlights);
			data['adultCount'] = UI_reservation.adultCount;
			data['childCount'] = UI_reservation.childCount;
			data['infantCount'] = UI_reservation.infantCount;
			if($("#chkRollForwardOverBook").attr('checked')){
				data['overBook'] = true;
			}
			if($("#chkOHDRFOvrdOHDBuffer").attr('checked')){
				var days = $.trim($("#txtOHDRFBufferDays").val());
				var time = $.trim($("#txtOHDRFBufferTime").val());
				if (!UI_commonSystem.numberValidate({id:"#txtOHDRFBufferDays", desc:"Buffer Days", minValue:0})){
					UI_tabBookingInfo.enableRollForwardButtons(true);
					return false;
				}
				if( !UI_commonSystem.timeValidator($("#txtOHDRFBufferTime").val())){
					UI_tabBookingInfo.enableRollForwardButtons(true);
					return false;
				}
				if(days != '' && time != ''){
					data['overrideBufferTime'] = true;
					data['bufferDays'] = days;
					data['bufferTime'] = time;
				} else {
					alert("Valid buffer time days and time is required");
					UI_tabBookingInfo.enableRollForwardButtons(true);
					return;
				}
			} else {
				data['overrideBufferTime'] = false;
			}
			data['system'] = jsonSystem;
			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({
				dataType: 'json',
				data: data,
				async: true,
				url: "onholdRollForward.action",
				success: UI_tabBookingInfo.ohdRollForwardConfirmSuccess,
				error: UI_tabBookingInfo.ohdRollForwardConfirmError
			});
		}
	   }
   }
   
   UI_tabBookingInfo.ohdRollForwardConfirmSuccess = function(response){   		
   		UI_tabBookingInfo.enableRollForwardButtons(true);
   		
   		var bookMarkPNRs = function(strPnrs){
   			if(strPnrs != null && strPnrs != ''){
	   			var jsContactInfo =$.parseJSON(UI_reservation.contactInfo);
	   			
	   			var listPNRs = strPnrs.split(","); 
	   			if(listPNRs != null && listPNRs.length > 0){
	   				for(var i=0; i < listPNRs.length; i++){
	   					top[0].addPNRHistoryNew(listPNRs[i], jsContactInfo.firstName);
	   				}
	   			}
	   			parent.UI_searchReservation.writePNR();
				top[0].showPNRIcon();
   			}
   		}
		UI_commonSystem.hideProgress();
   		
   		if(response.success){
			$('#divOHDRollForward').hide();
	   		UI_commonSystem.readOnly(false);
			showConfirmation("Roll Forward successful. Generated PNRs:- " + response.strGeneratedPnrs.replace(/,/gi, ", "));
			bookMarkPNRs(response.strGeneratedPnrs);
		} else {
			showERRMessage(response.messageTxt);
		}
   }
   
   UI_tabBookingInfo.isAllowDuplicateName = function() {
		if(DATA_ResPro.initialParams.duplicateNameCheckEnabled && DATA_ResPro.initialParams.duplicateNameSkipPrivilege){ // check whether duplicate check is enabled
			if(isDuplicateNameExist){
				if(DATA_ResPro.initialParams.duplicateNameSkipPrivilege){ //duplicate override privilege exists
					if(confirm(raiseError("XBE-ERR-72"))){
						return true;
					} else {
						return false;
					}
				} else {
					showERRMessage(raiseError("XBE-ERR-71"));
				}
			}			
		}
   }   
   
   UI_tabBookingInfo.ohdRollForwardSelection = function(selection, target){
	   $("#divRFAvailFlights input:checkbox."+selection).attr('checked', $(target).attr('checked'));
	   if(selection == 7){
		   var list = ['chkOhdRFSun', 'chkOhdRFMon','chkOhdRFTue','chkOhdRFWed','chkOhdRFThu','chkOhdRFFri','chkOhdRFSat'];
		   $.each(list, function(index, itm){
			   $('#'+itm).attr('checked', $(target).attr('checked'));
		   });
			   
	   } else {
		   if(!$(target).attr('checked') && $('#chkOhdRFAll').attr('checked')){
			   $('#chkOhdRFAll').attr('checked', false );
		   }
	   }
   }
   

   
   UI_tabBookingInfo.ohdRollForwardConfirmError = function(response){
   		UI_tabBookingInfo.enableRollForwardButtons(true);
		UI_commonSystem.hideProgress();
   }
   
   UI_tabBookingInfo.enableRollForwardButtons = function(opt){
   		if(opt) {
			$("#btnOHDRollForwardSearch").enable();
			$("#btnOHDRollForwardCancel").enable();
			$("#btnOHDRollForwardConfirm").enable();
		} else {
			$("#btnOHDRollForwardSearch").disable();
			$("#btnOHDRollForwardCancel").disable();
			$("#btnOHDRollForwardConfirm").disable();
		}
	}
	
	   /*
    *function used to enable/disable PAX Nationality Combo, PAX Nationality should 
    *be editable even though the reservation has flown segment and user doesn't 
    *have ”xbe.res.alt.old” privilege
    * */   
	UI_tabBookingInfo.disablePaxNationality = function(blnStatus){
		
		var intLen = jsonPaxAdtObj.paxAdults.length;
		if (intLen > 0){
			var i = 1;
			$.each(jsonPaxAdtObj.paxAdults, function(){				
				$("#"+ i + "_displayAdultNationality").disableEnable(blnStatus);				
				
				i++;
			});
		}
		
		intLen = jsonPaxAdtObj.paxInfants.length;
		if (intLen > 0){
			var i = 1;
			$.each(jsonPaxAdtObj.paxInfants, function(){											
				$("#"+ i + "_displayInfantNationality").disableEnable(blnStatus);				
				
				i++;
			});
		}
	} 
	
	UI_tabBookingInfo.adjustPageHeight = function(paxAdts, infs){
		var max_Adt = 9;
		if ($("#divInfantPane").css("display") != "none"){
			if (infs.length > 2){
				$("#gview_tblPaxInf > .ui-jqgrid-bdiv").css('height',50);
			}else{
				$("#gview_tblPaxInf > .ui-jqgrid-bdiv").css("height","100%");
			}
			max_Adt = 4;
		}
		if (paxAdts !=null){
			if (paxAdts.length > max_Adt){
				$("#gview_tblPaxAdt > .ui-jqgrid-bdiv").css('height',15 * (max_Adt + 1));
			}else{
				$("#gview_tblPaxAdt > .ui-jqgrid-bdiv").css("height","100%");
			}
		}
	} 
	
	UI_tabBookingInfo.stringReplacer = function(strTemplate, strArr, placeHolder){
		for ( var i = 0; i < strArr.length; i++) {
			var element = strArr[i];
			strTemplate = strTemplate.replace(placeHolder+(i+1), element);				
		}
		
		return strTemplate;
	}
	
	validatePaxCounts = function(adultCount,childCount,infantCount){
		var infantLength=0;
		var adultLength=0;
		var childLength=0;
		
		if(jsonPaxAdtObj.paxInfants != null){
			infantLength=jsonPaxAdtObj.paxInfants.length;
		}
		
		for(var i=0;i<jsonPaxAdtObj.paxAdults.length;i++){
			if (jsonPaxAdtObj.paxAdults[i].displayAdultType == "AD") {
				adultLength++;
			} else if(jsonPaxAdtObj.paxAdults[i].displayAdultType == "CH"){
				childLength++;
			}
		}
		
		if(adultLength==adultCount && childLength==childCount && infantLength==infantCount){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Sets the passed on adult details. If the selected amount of adult passengers are lesser than the number of parsed data
	 * this function will break execution flow.
	 */
	setAdultDetails = function (adultList){
		for (var i = 0; i < adultList.length; i++) {
			var paxRec=adultList[i];			
			/*$("#" + (i + 1) + "_displayAdultTitle option").each(function() {
				if($(this).text() == paxRec.displayAdultTitle) {
					    $(this).attr('selected', 'selected');            
				}                        
			});*/			
			$("#" + (i + 1) + "_displayAdultTitle").val(paxRec.displayAdultTitle.split(".")[0].toUpperCase());
			$("#" + (i + 1) + "_displayAdultFirstName").val(paxRec.displayAdultFirstName);
			$("#" + (i + 1) + "_displayAdultLastName").val(paxRec.displayAdultLastName);
			
			$("#" + (i + 1) + "_displayAdultNationality option").each(function() {
				if($(this).text() == paxRec.displayAdultNationality) {
				    $(this).attr('selected', 'selected');            
				}                        
			});
			
			$("#" + (i + 1) + "_displayAdultDOB").val(paxRec.displayAdultDOB);
			
			jsonPaxAdtObj.paxAdults[i].displayAdultFirstName = paxRec.displayAdultFirstName;
			jsonPaxAdtObj.paxAdults[i].displayAdultLastName = paxRec.displayAdultLastName;
			jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber=paxRec.displayPnrPaxCatFOIDNumber;
			jsonPaxAdtObj.paxAdults[i].displayNationalIDNo=paxRec.displayNationalIDNo;
			jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry=paxRec.displayPnrPaxCatFOIDExpiry;
			jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDPlace=paxRec.displayPnrPaxCatFOIDPlace;
			jsonPaxAdtObj.paxAdults[i].displayPnrPaxPlaceOfBirth=paxRec.displayPnrPaxPlaceOfBirth;
			jsonPaxAdtObj.paxAdults[i].displayTravelDocType=paxRec.displayTravelDocType;
			jsonPaxAdtObj.paxAdults[i].displayVisaDocNumber=paxRec.displayVisaDocNumber;
			jsonPaxAdtObj.paxAdults[i].displayVisaDocPlaceOfIssue=paxRec.displayVisaDocPlaceOfIssue;
			jsonPaxAdtObj.paxAdults[i].displayVisaDocIssueDate=paxRec.displayVisaDocIssueDate;
			jsonPaxAdtObj.paxAdults[i].displayVisaApplicableCountry=paxRec.displayVisaApplicableCountry;
			jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber=paxRec.displaypnrPaxArrivalFlightNumber;
			jsonPaxAdtObj.paxAdults[i].displaypnrPaxFltArrivalDate=paxRec.displaypnrPaxFltArrivalDate;
			jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalTime=paxRec.displaypnrPaxArrivalTime;
			jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber=paxRec.displaypnrPaxDepartureFlightNumber;
			jsonPaxAdtObj.paxAdults[i].displaypnrPaxFltDepartureDate=paxRec.displaypnrPaxFltDepartureDate;
			jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureTime=paxRec.displaypnrPaxDepartureTime;
			jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId=paxRec.displayPnrPaxGroupId;
		}
	}

	/**
	 * Sets the passed on infant details.
	 */
	setInfantDetails = function(infantList){
		for ( var i = 0; i < infantList.length; i++) {
			
			var infantRec=infantList[i];
			
			$("#" + (i + 1) + "_displayInfantTitle option").each(function() {
				if($(this).text() == paxRec.displayInfantTitle) {
					    $(this).attr('selected', 'selected');            
				}                        
			});			
			$("#" + (i + 1) + "_displayInfantFirstName").val(infantRec.displayInfantFirstName);
			$("#" + (i + 1) + "_displayInfantLastName").val(infantRec.displayInfantLastName);
			
			$("#" + (i + 1) + "_displayInfantNationality option").each(function() {
				if($(this).text() == infantRec.displayInfantNationality) {
				    $(this).attr('selected', 'selected');            
				}                        
			});
			
			$("#" + (i + 1) + "_displayInfantDOB").val(infantRec.displayInfantDOB);
			
			$("#" + (i + 1) + "_displayInfantTravellingWith").val(infantRec.displayInfantTravellingWith);
			
			jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDNumber=infantRec.displayPnrPaxCatFOIDNumber;
			jsonPaxAdtObj.paxInfants[i].displayNationalIDNo=infantRec.displayNationalIDNo;
			jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDExpiry=infantRec.displayPnrPaxCatFOIDExpiry;
			jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDPlace=infantRec.displayPnrPaxCatFOIDPlace;			
			jsonPaxAdtObj.paxInfants[i].displayPnrPaxPlaceOfBirth=infantRec.displayPnrPaxPlaceOfBirth;
			jsonPaxAdtObj.paxInfants[i].displayTravelDocType=infantRec.displayTravelDocType;
			jsonPaxAdtObj.paxInfants[i].displayVisaDocNumber=infantRec.displayVisaDocNumber;
			jsonPaxAdtObj.paxInfants[i].displayVisaDocPlaceOfIssue=infantRec.displayVisaDocPlaceOfIssue;
			jsonPaxAdtObj.paxInfants[i].displayVisaDocIssueDate=infantRec.displayVisaDocIssueDate;
			jsonPaxAdtObj.paxInfants[i].displayVisaApplicableCountry=infantRec.displayVisaApplicableCountry;
			jsonPaxAdtObj.paxInfants[i].displaypnrPaxArrivalFlightNumber=infantRec.displaypnrPaxArrivalFlightNumber;
			jsonPaxAdtObj.paxInfants[i].displaypnrPaxFltArrivalDate=infantRec.displaypnrPaxFltArrivalDate;
			jsonPaxAdtObj.paxInfants[i].displaypnrPaxArrivalTime=infantRec.displaypnrPaxArrivalTime;
			jsonPaxAdtObj.paxInfants[i].displaypnrPaxDepartureFlightNumber=infantRec.displaypnrPaxDepartureFlightNumber;
			jsonPaxAdtObj.paxInfants[i].displaypnrPaxFltDepartureDate=infantRec.displaypnrPaxFltDepartureDate;
			jsonPaxAdtObj.paxInfants[i].displaypnrPaxDepartureTime=infantRec.displaypnrPaxDepartureTime;
			jsonPaxAdtObj.paxInfants[i].displayPnrPaxGroupId=infantRec.displayPnrPaxGroupId;
		}
	}
	
	sortByDate = function(objFltDt){
		var temp = objFltDt[0];
		for(var i=1; i<objFltDt.length; i++){			
			if(temp.departureDate > jsonFltDetails[i].departureDate){
				jsonFltDetails[i-1] = jsonFltDetails[i];
		        jsonFltDetails[i] = temp;
			}
			temp = jsonFltDetails[i];
		}
	}
	
	UI_tabBookingInfo.isTBABooking = function() { 
		for(var i=0;i<jsonPaxAdtObj.paxAdults.length;i++){
			var fn = jsonPaxAdtObj.paxAdults[i].displayAdultFirstName;
			var ln = jsonPaxAdtObj.paxAdults[i].displayAdultLastName;
			if(fn != UI_tabBookingInfo.xbeTBA && ln != UI_tabBookingInfo.xbeTBA){
				return false;
			}
		}
		return true;
	}
	
	UI_tabBookingInfo.extractFlightFirstSegOutDateObj = function() {
		var objFlightData = jsonFltDetails;	
		var segCount = objFlightData.length;
		var departureDate;
		if(segCount != null && segCount > 0){
			departureDate = objFlightData[0].departureDateLong;
			return new Date(departureDate);
		}
		return new Date();
	}
	/*
	 * This method compares two dates
	 * if date1 <= date2 this will rturns true.
	 * esle returns false.
	 */ 
	UI_tabBookingInfo.compareDate = function(date){

		if (date.indexOf(" ") > 0) {
			 date = date.split(" ")[1];
		}
		
		var CurrntDate = top.arrParams[0];
		
		var curdateD = Number(CurrntDate.substring(0,2));
		var curdateM = Number(CurrntDate.substring(3,5));
		var curdateY = Number(CurrntDate.substring(6,10));

		var dateD = Number(date.substring(0,2));
		var dateM = Number(date.substring(3,5));
		var dateY = Number(date.substring(6,10));
		
		if(curdateY != dateY){
			if(curdateY < dateY){return true;}
			else{return false;}
		}
		else if(curdateM != dateM){
			if(curdateM < dateM){return true;}
			else{return false;}
		}
		else if(curdateD != dateD){
			if(curdateD < dateD){return true;}
			else{return false;}
		}
		else{
			return true;
		}
	}
	
	UI_tabBookingInfo.isDomesticFlightExist = function() {
		var flightList = jQuery.parseJSON(UI_reservation.jsonSegs);
		for(var i = 0;i < flightList.length;i++){
			if(flightList[i].domesticFlight == true){
				return true;
			}
		}
		return false;
	}
	UI_tabBookingInfo.filterAdvancedPassengerSelection = function(flightNumberCol) {
		var objJPax = jsonPaxAdtObj.paxAdults;
		UI_tabBookingInfo.jsonPaxIntlFltObj = flightNumberCol;
		var intLen = objJPax.length;
		var arrivalFlightList = flightNumberCol.arrivalFlights;
		var departureFlightList = flightNumberCol.DepartureFlights;
		var pnrGroupIdList = flightNumberCol.pnrPaxGroupIds;
		
		for (var j = 0 ; j < intLen ; j++) {
			$("#" + (j + 1) + "_displaySelectPax").removeAttr('checked');
		}

		
		for (var j = 0 ; j < intLen ; j++) {
			var paxArrivalFlight = jsonPaxAdtObj.paxAdults[j].displaypnrPaxArrivalFlightNumber;
			var paxDepartureFlight = jsonPaxAdtObj.paxAdults[j].displaypnrPaxDepartureFlightNumber;
			var paxPnrGroupId = jsonPaxAdtObj.paxAdults[j].displayPnrPaxGroupId;
			
			var needTocheckArrivalFlight = ( arrivalFlightList.length != 0 );
			var needTocheckDepartureFlight = ( departureFlightList.length != 0 );
			var needTocheckPaxPnrGroupId = ( pnrGroupIdList.length != 0 );
			
			var matchingCriteria = true;
			var matchingCriteriaMap = {};
			
			if(needTocheckArrivalFlight){
				matchingCriteriaMap["arrivalFlight"] = isValueContain(paxArrivalFlight, arrivalFlightList);
			}
			if(needTocheckDepartureFlight){
				matchingCriteriaMap["departureFlight"] = isValueContain(paxDepartureFlight, departureFlightList);
			}
			if(needTocheckPaxPnrGroupId){
				matchingCriteriaMap["paxPnrGroupId"] = isValueContain(paxPnrGroupId, pnrGroupIdList);
			}
			
			var values = $.map(matchingCriteriaMap, function(v) { return v; });
			
			$.each(values, function( index, value ) {
				  if(!value){
					  matchingCriteria = false;
				  }
				});
			
			if(matchingCriteria && ( needTocheckArrivalFlight || needTocheckDepartureFlight || needTocheckPaxPnrGroupId)){
				$("#" + (j + 1) + "_displaySelectPax").attr('checked','checked');
			}
			
		}
			
			function isValueContain(value, list){
				if(value == null) {return false;}
				for(var i=0; i< list.length; i++){
					if(value == list[i]){
						return true;
					}
				}
				return false;
			}
	}

	UI_tabBookingInfo.disableEnablePAXName = function(blnStatus){
		if(DATA_ResPro.initialParams.enableFareRuleNCC){
			var intLen = jsonPaxAdtObj.paxAdults.length;
			if (intLen > 0){
				var i = 1;
				$.each(jsonPaxAdtObj.paxAdults, function(){				
					$("#"+ i + "_displayAdultTitle").disableEnable(blnStatus);
					if(!blnStatus){
						if(this.nameEditable){
							$("#"+ i + "_displayAdultFirstName").disableEnable(blnStatus);				
							$("#"+ i + "_displayAdultLastName").disableEnable(blnStatus);
						}
					}else {
						$("#"+ i + "_displayAdultFirstName").disableEnable(blnStatus);				
						$("#"+ i + "_displayAdultLastName").disableEnable(blnStatus);
					}				
					i++;
				});
			}
			
			intLen = jsonPaxAdtObj.paxInfants.length;
			if (intLen > 0){
				var i = 1;
				$.each(jsonPaxAdtObj.paxInfants, function(){
					if(!blnStatus){
						if(this.nameEditable){
							$("#"+ i + "_displayInfantFirstName").disableEnable(blnStatus);				
							$("#"+ i + "_displayInfantLastName").disableEnable(blnStatus);
						}
						
					}else {
						$("#"+ i + "_displayInfantFirstName").disableEnable(blnStatus);				
						$("#"+ i + "_displayInfantLastName").disableEnable(blnStatus);	
					}				
					i++;
				});
			}		
		}	
	}
	
	UI_tabBookingInfo.validateNameChangeInfo = function(paxNameDetails){
		
		if(paxNameDetails!=undefined && paxNameDetails!=null){
			if(paxNameDetails.adultNames!=undefined 
					&& paxNameDetails.adultNames!=null && paxNameDetails.adultNames.length >0){
				
				var tba = UI_tabBookingInfo.xbeTBA;
				for(var k=0;k<paxNameDetails.adultNames.length;k++){
					var adultObj =  paxNameDetails.adultNames[k];			
					if(adultObj.displayAdultType=="CH"){
						paxConfig = paxConfigCH;
					}else{
						paxConfig = paxConfigAD;
						
					}
					paxConfig = paxConfigAD
					if (!UI_commonSystem.controlValidateName({id:"#"+ adultObj.id +"_display-AdultFirstName", desc:"First Name", required:paxConfig.firstName.xbeMandatory})){return false;}
					
					if($.trim(adultObj.displayAdultFirstName) != "" && 
							!isAlphaWhiteSpace(adultObj.displayAdultFirstName)) {
						showERRMessage(raiseError("XBE-ERR-04","First name"));
						$("#"+ adultObj.id + "_displayNAdultFirstName").focus();
						return false;
					}
					
					if (!UI_commonSystem.controlValidateName({id:"#"+ adultObj.id +"_display-AdultLastName", desc:"Last Name", required:paxConfig.lastName.xbeMandatory})){return false;}
					
					if($.trim(adultObj.displayAdultLastName) != "" && 
							!isAlphaWhiteSpace(adultObj.displayAdultLastName)) {
						showERRMessage(raiseError("XBE-ERR-04","Last name"));
						$("#"+ adultObj.id + "_displayNAdultLastName").focus();
						return false;
					}
					
					if(paxConfig.ffid.xbeVisibility ){
						var j = adultObj.id;
						var ffid = $.trim($("#" + j + "_display-FFID").val());
						var firstName = $.trim($("#" + j + "_display-AdultFirstName").val());
						var lastName = $.trim($("#" + j + "_display-AdultLastName").val());
						
						if (ffid != null && ffid != "" && !checkEmail(ffid.toLowerCase())){
							showERRMessage(raiseError("XBE-ERR-04","Air Rewards ID " + ffid));
							return false;
						}

						if(ffid != null && ffid != ""){
							
							//Check for T B A bookings
							if(firstName == UI_tabBookingInfo.xbeTBA || lastName == UI_tabBookingInfo.xbeTBA){
								showERRMessage(raiseError("XBE-ERR-97"));
								return false;
							}
							
							firstName = firstName.toLowerCase();
							lastName = lastName.toLowerCase();
							ffid = ffid.toLowerCase();
							
							var data = {};
							data['ffid'] = ffid;
							data['firstName'] = firstName;
							data['lastName'] = lastName;
							var retValue = false;
							
							$("#frmModiBkg").ajaxSubmit({
								dataType: 'json', 
								url:"duplicateCheck!ffidCheck.action", async:false,
								data:data,
								async: false,
								success: function(response){ 
									var ffid = $.trim($("#" + j + "_display-FFID").val());
									retValue = response.ffidAvailability;
									if(!retValue){
										showERRMessage(raiseError("XBE-ERR-96", ffid));
										$("#" + j + "_display-FFID").focus();	
									} else{
										firstName2 = response.firstName.toLowerCase();
										lastName2 = response.lastName.toLowerCase();
										var nameValidation = UI_commonSystem.nameCompare(firstName, lastName, firstName2, lastName2);
										if(!nameValidation){
											retValue = false;
											showERRMessage(raiseError("XBE-ERR-95", ffid));
											$("#"+ j + "_display-AdultFirstName").focus();
										}
									}
								},
								error : UI_commonSystem.setErrorStatus
							});
				
							if(!retValue){
								return false;
							}
						}
						
						
					}
					
					var requireTitle = function(index){
						if(index == 1) return true;
						var fn = $.trim($("#"+ index + "_display-AdultFirstName").val());
						var ln = $.trim($("#"+ index + "_display-AdultLastName").val());
						if(fn == tba && ln == tba){
							return false;
						}
						return true;
					};
					
					if(paxConfig.title.xbeVisibility && paxConfig.title.xbeMandatory){
						if(requireTitle(adultObj.id) && $.trim($("#"+ adultObj.id + "_display-AdultTitle").val()) == "") {
							showERRMessage(raiseError("XBE-ERR-01","Title"));
							$("#"+ adultObj.id + "_display-AdultTitle").focus();
							return false;
						}
					}
				}
			}			
			
			if(paxNameDetails.infantNames!=undefined 
					&& paxNameDetails.infantNames!=null && paxNameDetails.infantNames.length >0){
				for(var k=0;k<paxNameDetails.infantNames.length;k++){
					var infantObj =  paxNameDetails.infantNames[k];	
					
					if (!UI_commonSystem.controlValidateName({id:"#"+ infantObj.id +"_display-InfantFirstName", desc:"First Name", required:paxConfigIN.firstName.xbeMandatory})){return false;}
					
					if($.trim(infantObj.displayInfantFirstName) != "" && 
							!isAlphaWhiteSpace(infantObj.displayInfantFirstName)) {
						showERRMessage(raiseError("XBE-ERR-04","First name"));
						$("#"+ infantObj.id + "_displayNInfantFirstName").focus();
						return false;
					}
					
					if (!UI_commonSystem.controlValidateName({id:"#"+ infantObj.id +"_display-InfantLastName", desc:"Last Name", required:paxConfigIN.lastName.xbeMandatory})){return false;}
					
					if($.trim(infantObj.displayInfantLastName) != "" && 
							!isAlphaWhiteSpace(infantObj.displayInfantLastName)) {
						showERRMessage(raiseError("XBE-ERR-04","Last name"));
						$("#"+ infantObj.id + "_displayNInfantLastName").focus();
						return false;
					}			
				}
			}

		}
		return true;
	}

	UI_tabBookingInfo.allowNameChange = function(){
	
		if(UI_reservation.isAllowNameChange){
			$("#btnNameChange").decorateButton();
			$("#btnNameChange").click(function() {UI_tabBookingInfo.loadNameChangeData(false);});
			$("#btnNameChangeConfirm").decorateButton();
							
			//if(UI_reservation.isAllowExtendedNameChange){
			//	$("#btnNameChangeConfirm").click(function() {UI_tabBookingInfo.commenceNameChange(false);});
			//} else 
			if (UI_reservation.isAllowRequoteOverride) {
				$("#btnNameChangeConfirm").click(function() {
					UI_tabBookingInfo.showNameChangeRequotePopup();
				});
				
				$("#btnNameCngOvrNo").click(function() {
					UI_tabBookingInfo.closeNameChangeRequotePopup();
					UI_tabBookingInfo.commenceNameChange(false);
					UI_commonSystem.showProgress();		
				});
				
				$("#btnNameCngOvrOk").click(function() {
					UI_tabBookingInfo.closeNameChangeRequotePopup();
					UI_tabBookingInfo.commenceNameChange(true);
					UI_commonSystem.showProgress();		
				});
			} else {
				$("#btnNameChangeConfirm").click(function() {UI_tabBookingInfo.commenceNameChange(true);});
			}
			$("#btnNameChangeReset").decorateButton();
			$("#btnNameChangeReset").click(function() {UI_tabBookingInfo.resetNameChangeData();});
			$("#btnNameChangeClose").decorateButton();
			$("#btnNameChangeClose").click(function() {UI_tabBookingInfo.closeNameChangeDialog(true);});	
		}else{
			$("#btnNameChange").hide();
		}
		
	}
	var checkTBANameChangedOnly = function(list){
    	
    	if(jsonBkgInfo.status != "CNF"){
    		return false;	
    	}
    	
        var booleanNameChanged = false;
        for(var i=0;i<list.adultNames.length;i++){
            if (list.adultNames[i].nameChangeApplied){
                booleanNameChanged = true;
                break;
            }
        }
        for(var j=0;j<list.infantNames.length;j++){
            if (list.infantNames[j].nameChangeApplied){
                booleanNameChanged = true;
                break;
            }
        }
        
        if(DATA_ResPro.initialParams.postDepature){
        	booleanNameChanged = false;
        }
        
        return booleanNameChanged;
    }
	
	UI_tabBookingInfo.getCouponControl = function() {		
		pnrSegs = $.parseJSON(UI_reservation.jsonSegs);		
		var pnrSegArray = new Array();
		 $.each(pnrSegs, function(index, obj){
			   if(obj.status == 'CNF' && !obj.flownSegment){
				   pnrSegArray[pnrSegArray.length] = obj.pnrSegID;
			   }
		 });
		
		
		var data = {};			
		data['resSegRPHList'] = pnrSegArray;			
		UI_commonSystem.getDummyFrm().ajaxSubmit({url:'couponControlRequest.action', 
			dataType: 'json', 
			processData: false, 
			data:data, 
			success: function (res){					
				if(res.success){						
					//load the booking
					UI_reservation.loadReservation("", null, false);					
				}else{			
					showERRMessage(res.messageTxt);
				}
			}, 
			error:UI_commonSystem.setErrorStatus
		});				

	}
	
	UI_tabBookingInfo.exchangeSegmentOnClick = function() {			
		UI_commonSystem.pageOnChange();
		var data = {};
		data["groupPNR"] 		= jsonGroupPNR;		
		data["pnr"] 	        = jsonPNR;
		
		UI_commonSystem.showProgress();		
		UI_tabBookingInfo.enableAllButtons(false);
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, async:false, url:"exchangeSegment.action",							
			success: function (res){
				UI_commonSystem.hideProgress();
				if(res.success){						
					showCommonError("CONFIRMATION", "Coupon Exchanged Successfully");
					//load the booking
					UI_reservation.loadReservation("", null, false);	
				}else{			
					showERRMessage(res.messageTxt);
				}
			},error:UI_commonSystem.setErrorStatus});
	}
	
	UI_tabBookingInfo.getAdultDetails = function() {
		var paxAdults = jsonPaxAdtObj.paxAdults;
		for ( var jl = 0; jl < paxAdults.length; jl++) {
			paxAdults[jl].displayAdultTitle = $("#" + (jl + 1) + "_displayAdultTitle")
					.val();
			paxAdults[jl].displayAdultFirstName = $(
					"#" + (jl + 1) + "_displayAdultFirstName").val();
			paxAdults[jl].displayAdultLastName = $(
					"#" + (jl + 1) + "_displayAdultLastName").val();
			paxAdults[jl].displayAdultNationality = $(
					"#" + (jl + 1) + "_displayAdultNationality").val();
			paxAdults[jl].displayAdultDOB = $("#" + (jl + 1) + "_displayAdultDOB")
					.val();
			
			paxAdults[jl].displayInfantTravelling = "N";
			paxAdults[jl].displayInfantWith = "";
		}
		return paxAdults;
	}

	UI_tabBookingInfo.getInfantDetails = function() {
		var paxInfants = jsonPaxAdtObj.paxInfants;
		if (paxInfants != null) {
			for ( var jl = 0; jl < paxInfants.length; jl++) {
				paxInfants[jl].displayInfantFirstName = $(
						"#" + (jl + 1) + "_displayInfantFirstName").val();
				paxInfants[jl].displayInfantLastName = $(
						"#" + (jl + 1) + "_displayInfantLastName").val();
				paxInfants[jl].displayInfantNationality = $(
						"#" + (jl + 1) + "_displayInfantNationality").val();
				paxInfants[jl].displayInfantDOB = $(
						"#" + (jl + 1) + "_displayInfantDOB").val();
				paxInfants[jl].displayInfantTravellingWith = $(
						"#" + (jl + 1) + "_displayInfantTravellingWith").val();
				paxInfants[jl].displayInfantTitle = $(
						"#" + (jl + 1) + "_displayInfantTitle").val();
				paxInfants[jl].displayNationaIDNo = $(
						"#" + (jl + 1) + "_displayNationaIDNo").val();

				// Parents JSON Object
				// Map the traveling with Infant
				if ($("#" + (jl + 1) + "_displayInfantTravellingWith").val() != "") {
					jsonPaxAdtObj.paxAdults[paxInfants[jl].displayInfantTravellingWith - 1].displayInfantTravelling = "Y";
					jsonPaxAdtObj.paxAdults[paxInfants[jl].displayInfantTravellingWith - 1].displayInfantWith = paxInfants[jl].displayInfantTravellingWith;
				}

			}
		}
		return paxInfants;
	}
	
/*	UI_tabBookingInfo.noShowTaxReversalOnClick = function() {			
		UI_commonSystem.pageOnChange();
		var data = {};
		data["groupPNR"] 		= jsonGroupPNR;		
		data["pnr"] 	        = jsonPNR;
		
		UI_commonSystem.showProgress();		
		UI_tabBookingInfo.enableAllButtons(false);
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, async:false, url:"noShowTaxReversal.action",							
			success: function (res){
				UI_commonSystem.hideProgress();
				if(res.success){						
					showCommonError("CONFIRMATION", "No Show Tax reversed Successfully");
					//load the booking
					UI_reservation.loadReservation("", null, false);	
				}else{			
					showERRMessage(res.messageTxt);
				}
			},error:UI_commonSystem.setErrorStatus});
	}*/

	
	UI_tabBookingInfo.loadReverseNoShowTaxData = function(){
		
		$("#listReverseNoShowTax").empty();
		$("#pnrTaxRefund").empty();
		$("#totalRefundableTax").empty();
		
		var carencyCode  = UI_reservation.jsonTaxReversalData.currencyCode;
		UI_tabBookingInfo.isGroupPnr = UI_reservation.isGroupBooking;
		
		$("#pnrTaxRefund").append(UI_reservation.jsonTaxReversalData.pnr);
		
			
			var htmlFormOpen = '<form action="">';
		    var htmlFormClose = '</form>';
		    var htmlAllStr= '';
		    
		    var totalTax = 0;
		    var hsaNoShowPax = false;
 				
			for(var i=0;i< UI_reservation.jsonTaxReversalData.noShowPaxDetails.length;i++){		
				var pnrPax = UI_reservation.jsonTaxReversalData.noShowPaxDetails[i];
				
				
				if(pnrPax.refundable){
					
					if(pnrPax.infantName !== null && pnrPax.infantName !== ""){
						pnrPax.passengerName = pnrPax.passengerName + "/" + pnrPax.infantName + "(INF)"
					}

				    var htmlStr = '<li class="ui-widget-content ui-selectee" >' + 
				  
				    '<table border="0" width="100%">' +
				    '<tr>'+
				    '<td style="width:5%" align="right"></td>' + 
				    '<td style="width:60%;">'+ pnrPax.passengerName + '</td>' +
				    '<td style="width:35%;" rowspan="2" align="center"> <input onclick=UI_tabBookingInfo.rfnd_check_onclick()  type="checkbox"  value="'+ pnrPax.travellerRef +'" class="rfnd_tax_checkbox"></td>' + 
				    '</tr><tr>' +
				    '<td style="width:5%" align="right"></td>' +
				    '<td style="width:60%;">'+ pnrPax.refundableTaxTotal +" "+carencyCode+'</td>' +
				    '<td style="width:30%;"></td>' +
				    '<td style="width:5%" align="right"></td>' + 
				    '</tr> </table> </li>';
				    
				    UI_tabBookingInfo.pnrPaxidAndTaxMap[pnrPax.travellerRef] = pnrPax.refundableTaxTotal;
				    
				    totalTax = totalTax + pnrPax.refundableTaxTotal;
				    hsaNoShowPax = true;
				    
				    htmlAllStr = htmlAllStr.concat(htmlStr);
				}			   
			}
			
		
		$("#listReverseNoShowTax").append(htmlFormOpen + htmlAllStr + htmlFormClose);
		$("#totalRefundableTax").append(totalTax+" "+ carencyCode);
		$("#noShowTaxRefundPopUp").dialog('open');		
		if(totalTax === 0){
			$("#btnReverseNoShowTaxConfirm").prop('disabled', true);
			$('.rfnd_tax_checkbox').attr("disabled", true);
			$(".checkRfnd:button").prop('disabled', true);
		}
	}
	
	UI_tabBookingInfo.rfnd_check_onclick = function() {
		
		$("#totalAmountToReverse").empty();
		
		var amoutToReverse = 0;
		var selectedPnrPaxs = $('.rfnd_tax_checkbox:checked').map(function() {
			return this.value;
		}).get();

		for (key in selectedPnrPaxs) {
			if (selectedPnrPaxs[key] in UI_tabBookingInfo.pnrPaxidAndTaxMap) {
				amoutToReverse = amoutToReverse + UI_tabBookingInfo.pnrPaxidAndTaxMap[selectedPnrPaxs[key]];
			}
		}
		
		$("#totalAmountToReverse").append(amoutToReverse+" "+ UI_reservation.jsonTaxReversalData.currencyCode);


	}
	
	UI_tabBookingInfo.onclickList = function(id){
		var paxId = id.split("noShowPaxRaw_")[1];
		if($("#noShowPaxChk_"+ paxId).prop('checked')){
			$("#noShowPaxChk_"+ paxId).prop('checked', false);
			$("#noShowPaxChk_"+ paxId).removeClass('highlight');
		} else {
			$("#noShowPaxChk_"+ paxId).prop('checked', true);
			$("#noShowPaxChk_"+ paxId).addClass('highlight');
		}		
	}
	
	UI_tabBookingInfo.closeReverseNoShowTaxDataDialog = function(isCloseButton){
		$("#totalAmountToReverse").empty();
		if(isCloseButton) {
			$("#noShowTaxRefundPopUp").dialog('close');
		}
	}
	





	UI_tabBookingInfo.processReverseNoShowTaxData = function() {

	if ($('.rfnd_tax_checkbox:input[type=checkbox]').is(':checked')) {

		var selectedPnrPaxIds = $('.rfnd_tax_checkbox:checked').map(function() {
			return this.value;
		}).get();

		UI_commonSystem.pageOnChange();
		var pnr = $("#pnrTaxRefund").text();
		var data = {};
		data["groupPNR"] = jsonGroupPNR;
		data["pnr"] = pnr;
		data["pnrPaxIds"] = selectedPnrPaxIds;
		data['version'] = jsonBkgInfo.version;
		data['resONDs'] = UI_reservation.jsonONDs;
		data['respaxs'] = UI_reservation.jsonPassengers;

		UI_commonSystem.showProgress();
		UI_tabBookingInfo.enableAllButtons(false);
		UI_commonSystem.getDummyFrm().ajaxSubmit(
				{
					dataType : 'json',
					data : data,
					async : false,
					url : "noShowTaxReversal.action",
					success : function(res) {
						UI_commonSystem.hideProgress();
						if (res.success) {
							UI_tabBookingInfo.closeReverseNoShowTaxDataDialog(true);
							UI_reservation.loadReservation("TAXREV", res, true);
						} else {
							showERRMessage(res.messageTxt);
						}
					},
					error : UI_commonSystem.setErrorStatus
				});
	} else {
		
		/*top.objCW.UI_message.showErrorMessage({
			messageText : "Please select passengers"
		});*/
		alert("Please select passengers.");
	}

 }
	

	UI_tabBookingInfo.selectDeselctAllPax = function() {

	if ($('.rfnd_tax_checkbox:input[type=checkbox]').is(':checked')) {
		$('.rfnd_tax_checkbox:input[type=checkbox]').prop('checked', false);
	} else {
		$('.rfnd_tax_checkbox:input[type=checkbox]').prop('checked', true);
	}
	
	UI_tabBookingInfo.rfnd_check_onclick();

	}
	
	UI_tabBookingInfo.getChargeAmountWithServiceTax = function() {
		if (UI_tabBookingInfo.validateGroupChargeAdjustment()) {
			
			var selPassengers="";
			var selONDs="";
			for(var i=0;i<selectedONDs.length;i++){
				if(i==0){
					selONDs=selectedONDs[i];
				}
				else{
					selONDs+=','+selectedONDs[i];
				}
			}
			for(var i=0;i<selectedPax.length;i++){
				if(i==0){
					selPassengers=selectedPax[i];
				}
				else{
					selPassengers+=','+selectedPax[i];
				}
			}
			
			UI_commonSystem.showProgress();
			var data = {};
			data['groupPNR']  = jsonGroupPNR;
			data['pnr']  = jsonPNR;
			data['version']  = jsonBkgInfo.version;
			data['resONDs']  = UI_reservation.jsonONDs;			
			data['resSegments']  = UI_reservation.jsonSegs;
			data['status']  = jsonBkgInfo.status;
			data['selAirlineOnd']=selONDs;
			data['selPax']= selPassengers;
			data['selChargeType']=$('#selChargeType').is(':checked');
			data['selAdjustmentType']=$('#selAdjustmentType').val();
			data['txtAdjustment']=$('#txtAdjustment').val();
			data['txtUN']=$('#txtUN').val();
			data['isInfantPaymentSeparated'] = UI_reservation.isInfantPaymentSeparated;

			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url: 'paxAccountChargesConfirm!chargeAmountWithServiceTax.action', dataType: 'json', processData: false, data:data,
											success: UI_tabBookingInfo.getServiceTaxSuccess, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	UI_tabBookingInfo.getServiceTaxSuccess = function(response){

		UI_commonSystem.hideProgress();	
		
		if(response.success) {
			$("#amountWithSerivceTax").text(response.serviceTaxMessage);
			$("#btnGroupChargeConfirm").enable();
		}else {
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}
		
	}
	
	UI_tabBookingInfo.chargeAdjustmentOnChange = function(){
		if(UI_reservation.isServiceTaxApplied){
			if($("#txtAdjustment").val().startsWith("-")){
				$("#btnGroupChargeConfirm").enable();
				$("#btnServiceTaxAmount").hide();
			}else{
				$("#btnGroupChargeConfirm").disable();
				$("#btnServiceTaxAmount").show();
			}
		}		
		
		UI_tabBookingInfo.enableDisableHandlingFeeCalc();
	}
	
	UI_tabBookingInfo.enableDisableHandlingFeeCalc = function(){		
		if(!UI_reservation.isServiceTaxApplied){
			$("#amountWithSerivceTax").text('');
			if(UI_reservation.isHandlingFeeApplicableForAdj){
				if($("#txtAdjustment").val().startsWith("-")){
					$("#btnConfirm").enable();
					$("#btnHandlingFeeAmount").hide();
				}else{
					$("#btnConfirm").disable();
					$("#btnHandlingFeeAmount").show();
				}
			}else{
				$("#btnConfirm").enable();
				$("#btnHandlingFeeAmount").hide();
			}
		}
	}
	
	UI_tabBookingInfo.getHandlingFeeAmount = function(){
		if (UI_tabBookingInfo.validateGroupChargeAdjustment()) {
			
			var selPassengers="";
			var selONDs="";
			for(var i=0;i<selectedONDs.length;i++){
				if(i==0){
					selONDs=selectedONDs[i];
				}
				else{
					selONDs+=','+selectedONDs[i];
				}
			}
			for(var i=0;i<selectedPax.length;i++){
				if(i==0){
					selPassengers=selectedPax[i];
				}
				else{
					selPassengers+=','+selectedPax[i];
				}
			}

			UI_commonSystem.showProgress();
			var data = {};
			data['selAirlineOnd']=selONDs;
			data['selPax']= selPassengers;
			data['selChargeType']=$('#selChargeType').is(':checked');
			data['selAdjustmentType']=$('#selAdjustmentType').val();
			data['txtAdjustment']=$('#txtAdjustment').val();
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url: 'paxAccountChargesConfirm!chargeAmountWithHandlingFee.action', dataType: 'json', processData: false, data:data,
				success: UI_tabBookingInfo.getHandlingFeeAmountSuccess, error:UI_commonSystem.setErrorStatus});
			
		}
	}
	
	UI_tabBookingInfo.getHandlingFeeAmountSuccess = function(response){
		UI_commonSystem.hideProgress();			
		if(response.success) {
			$("#amountWithSerivceTax").text(response.serviceTaxMessage);
			$("#btnConfirm").enable();
		}else {
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}		
	}
