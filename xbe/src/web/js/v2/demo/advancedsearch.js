$(function () {

	var n = 0;
	var count = 0;
	$(".showHide").click(function(){
		  if (this.id == "searc-shedule"){
			  var tem = $("#spnAdvancedSearchPane").css("display");
			  if (tem == "none")
				$("#spnAdvancedSearchPane").slideDown();
			  else
				$("#spnAdvancedSearchPane").slideUp();
				
		  }else if (this.id == "searc-flight"){
			  var temp = $("#spnSearchPane").css("display");
			  if (temp == "none")
				$("#spnSearchPane").slideDown();
				else
				$("#spnSearchPane").slideUp();
		  }else if(this.id == "backto-search"){
			  window.location.href = "demo!loadMulticiti.action";
		  }else{
			  var temp1 = $("#multicity").css("display");
			  if (temp1 == "none")
				$("#multicity").slideDown();
				else
				$("#multicity").slideUp();
		  }
	});
	
	$("#divBookingTabs").tabs();
	
	function buildSheduleSearch(){
		if ($("#origin-sche").val() != "" || $("#destination-sche").val() != "")
		{
			$("#sheduleDatetableOut").jqGrid({
			url:'gridSheduleDataOut.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['&nbsp;','Flight','Departure','Arrival','Days of Ops','From Date To'],
			colModel :[ 
			   {name:'id', width:15, align:"center"},
			   {name:'flight',width:120, label:'Flight'},
			   {name:'departure',label:'Departure', width:205},
			   {name:'arrival',label:'Arrival', width:205},
			   {name:'doOps',label:'Days of Ops', width:165},
			   {name:'fdt',label:'From Date To',width:160}
		   ],
			/*pager: jQuery('#phidengrid'),
			rowNum:10,
			rowList:[10,20,30],
			sortname: 'id',
			sortorder: "desc",*/
			viewrecords: true,
			height: "auto",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			imgpath: 'themes/basic/images',
			caption: 'Damuscus - Sabiha Gokcen Int Airport'
		 });
			
		$("#sheduleDatetableIn").jqGrid({
			url:'gridSheduleDataIn.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['&nbsp;','Flight','Departure','Arrival','Days of Ops','From Date To'],
			colModel :[ 
			   {name:'id', width:15, align:"center"},
			   {name:'flight',width:120, label:'Flight'},
			   {name:'departure',label:'Departure', width:205},
			   {name:'arrival',label:'Arrival', width:205},
			   {name:'doOps',label:'Days of Ops', width:165},
			   {name:'fdt',label:'From Date To',width:160}
		   ],
			/*pager: jQuery('#phidengrid'),
			rowNum:10,
			rowList:[10,20,30],
			sortname: 'id',
			sortorder: "desc",*/
			viewrecords: true,
			height: "auto",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			imgpath: 'themes/basic/images',
			caption: 'Sabiha Gokcen Int Airport - Damuscus'
		 });
		$("#origin").val($("#origin-sche").val());
		$("#destination").val($("#destination-sche").val());	
		}else{
			alert("Please fill the mandatory fields");
		}
	};
	
	
	function buildSearch(tableID, ind){
			ind = (ind >= 2) ? 0 : ind;
			$("#"+tableID).jqGrid({
			url:'gridData'+ind+'.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['&nbsp;','Segment','Flight','&nbsp;','Departure','Arrival','Avl.'],
			colModel :[ 
			   {name:'id', width:10, align:"center"},
			   {name:'segment',width:70, label:'Segment'},
			   {name:'flight',label:'Flight', width:60},
			   {name:'airline',label:'&nbsp;', width:25, align:"center"},
			   {name:'departure',label:'Departure', width:110},
			   {name:'arrival',label:'Arrival',width:110},
			   {name:'avlSeats',label:'Avl.',width:50}  
		   ],
			//pager: jQuery('#pager'),
			//rowNum:10,
			//rowList:[10,20,30],
			sortname: 'id',
			sortorder: "desc",
			viewrecords: true,
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			height: "auto",
			imgpath: 'themes/basic/images',
			caption: '',
			onSelectRow: function(rowid){
				$("#another-search").attr("disabled","");
				//var tk = $("#"+tableID).jqGrid('getGridParam','selrow')
				//$("#multicity-top-"+ind).find("#mSegment-"+ind).html(rowid);
			}
		 });
			//alert('../js/gridData'+ind+'.js');		
	};
	
		
		$(document).on("click","#another-search", function(){
			n = Number(n + 1);
			count = count + 1;
			$(".newcities").append(createMulticity(n));
			$("#multicity-search-"+n).css("display","block");
				$('#txtMultiDepartDate-'+Number(n)).datepicker({
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true
				});
			var inl = n - 1;
			$("#multicity-search-"+inl).hide();
			$("#multicity-top-"+inl).css("display","block");
			$("#multicity-"+inl+" span.dataTable").css("display","none");
			
			if (count >= 2)
				$("#addnother-container").hide();
			
		});
	
	$("#mainSearch").click(function(){
		var chk = $("#chkMultiCity").attr("checked");
		if (chk){
			$(".newcities").append(createMulticity(0));
			$(".multicitiStart").css("display","block");
			buildSearch("table1-0",0);
		}else{
			window.location.href = "demo!loadavailability.action?hdnAction=ADVANCEDSEARCH2";
			
		}
	});
	
	
	
	
	$(document).on("click","#btnReset", function(){
		window.location.href = "demo!loadMulticiti.action";
	});
	
	$(document).on("click", ".another", function(){
		buildSearch("table1-"+n,n);
		
	});
	$(document).on("click",".remove", function(){
		$(this).parent().parent().parent().remove();
		count = count - 1;
		if (count <= 2)
			$("#addnother-container").show();
	});
	
	$(document).on("click","#shedule-btnSearch", function(){
		showLoader();
		buildSheduleSearch();
	});
	
	$(document).on("click","#quote-search", function(){
		window.location.href = "demo!loadavailability.action?hdnAction=ADVANCEDSEARCH3";
	});
	
	$(".editFare").click(function(){
		$(this).next().find("input").val($(this).find("font").html());
		$(this).hide();
		$(this).next().show();
	});
	
	$(".faretext").keypress(function(event){
		var code = (event.keyCode ? event.keyCode : event.which);
 			if(code == 13) { 
				setTotal(this)
				$(this).parent().prev().find("font").html($(this).val());
				$(this).parent().hide();
				$(this).parent().prev().show();
			}
 	 });
	
	function setTotal(obj){
		var lastval = parseFloat($(obj).parent().prev().find("font").html());
		var newVal = parseFloat($(obj).val());
		var lastTotal = parseFloat($(obj).parent().parent().parent().find(".row").html());
		var newTotal = (lastTotal - lastval) + newVal;
		var lastGTot = parseFloat($(".tot").html());
		var newGTot = (lastGTot - lastTotal) + newTotal;
		$(obj).parent().parent().parent().find(".row").html(addDecimal(newTotal));
		$(".tot").html(addDecimal(newGTot));
	};
	
	function addDecimal(val){
		
		var temp = String(val).split(".")
		if (temp[1] == undefined){
			val = val + ".00";
		}else if (temp[1].length == 1){
			val = val + "0";
		}
		return val;
	};
	
	
	
	
	$(document).on("click",".edit", function(){
		var tk = $(this).parent().parent().prev().find(".dataTable").css("display");
		if (tk == "none"){
			//$("multicity-search-"+n).hide();
			$(this).parent().parent().prev().find(".dataTable").slideDown();
			$(this).parent().parent().prev().prev().slideDown()
			$(this).val("Done");
		}else{
			//$("multicity-search-"+n).show();
			$(this).parent().parent().prev().find(".dataTable").slideUp();
			$(this).parent().parent().prev().prev().slideUp()
			$(this).val("Edit");
		}
	});
	
	$('#txtDepartDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		minDate: 0
	});
	$('#txtReturnDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		minDate: 0
	});
	
		
	$('#chkOpenRet').click(function(){
		if ($(this).attr("checked"))
			$("#selRetValidity").attr("disabled","");
		else{
			$("#selRetValidity").val("");
			$("#selRetValidity").attr("disabled","disabled");
		}
	});
	

	function createMulticity(index){
		var str ='<div id="multicity-'+index+'" class="city">';
			if (index != 0){
			str +='<div id="multicity-search-'+Number(index)+'" style="display:none">'+
            	'<table cellspacing="0" cellpadding="2" border="0" align="center" width="98%">'+
		 	 	'<tbody>'+
                '<tr><td valign="top">'+
                       ' <table cellspacing="0" cellpadding="1" border="0" align="center" width="100%">'+
                        	'<tr><td width="5%">'+
					 	 			'<font>From :&nbsp;</font></td>'+
			 	 		  '<td width="16%">'+
							'<select id="origin" style="width:170px">'+
								'<option selected="selected"></option>'+
                                '<option>JED - King Abdul Aziz Int. Airport</option><option>DMA - Damuscus</option>'+
								'<option>IST - Istanbul</option><option>MMX - Sturup Malmo Airport</option>'+
							'</select><font class="Mandatory">&nbsp;*&nbsp;</font>'+
						  '</td>'+
                         ' <td width="3%" align="right">'+
					 	 			'<font>To :&nbsp;</font></td>'+
			 	 		  '<td width="14%">'+
							'<select id="destination" style="width:170px">'+
								'<option selected="selected"></option>'+
                                '<option>JED - King Abdul Aziz Int. Airport</option><option>DMA - Damuscus</option>'+
								'<option>IST - Istanbul</option><option>MMX - Sturup - Malmo Airport</option>'+
							'</select><font class="Mandatory">&nbsp;*&nbsp;</font>'+
						  '</td>'+
                          '<td width="12%" align="right">'+
					 	 			'<font>Departure Date :&nbsp;</font>'+
					 	 		'</td>'+
					 	 		'<td width="8%">'+
					 	 		'<input type="text" invalidtext="true" tabindex="3" id="txtMultiDepartDate-'+Number(index)+'" size="10" maxlength="10">'+
								'<font class="Mandatory">&nbsp;*</font>'+
								'</td>'+
								'<td width="11%">'+
									'<input type="text" tabindex="4" name="txtDVariance" size="2" maxlength="1" value="0" class="rightText" id="txtDVariance">'+		
									'<font>+/- Days</font>'+
								'</td>'+
                               '<td align="right" width="9%"><input type="button" tabindex="14" class="Button another" value="Search" id="'+Number(index)+'-btnSearch">'+
			 	 		 ' </tr>'+
                        '</table>'+
	 ' </td></tr>'+
			 '</tbody></table>'+
            ' </div>';
				}
			
           		str +='<table width="100%"><tr><td>'+
                '<span class="dataTable"><table id="table1-'+index+'" class="scroll"></table>'+
				'</td>'+
                '</tr><tr>'+
                '<td align="right">'+				
                '</td></tr></table>'+
				'<div id="multicity-top-'+index+'" class="citytop">'+
				'<span class="lbl">Segment : </span><span id="mSegment-'+index+'">JED - DAM </span>&nbsp;|&nbsp;<span class="lbl">Selected Flight : </span><span id="mflight-'+index+'">6Q945</span>&nbsp;|&nbsp;<span class="lbl">Departure Date/Time : </span><span id="mdeparture-'+index+'">02Aug10 13:10</span>'+
				'<span class="butnset">'+
				'<input type="button" tabindex="14" class="Button edit" value="Edit" id="edit-search-'+Number(index + 1)+'" style="width:auto">'+
				'<input type="button" tabindex="14" class="Button remove" value="Remove This City" id="remove-search-'+Number(index + 1)+'" style="width:auto">'+
				'</span>'+
				'</div>'+
	           	'</div>';
			
			return str;
	};
	

});
//jquery grid test
 