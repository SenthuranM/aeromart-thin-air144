/**
 * @author subash
 */

var arrError = new Array();
UI_BLPaxRes.rowId = null;
UI_BLPaxRes.rowData = null;

$(document).ready(function() {
	UI_BLPaxRes.ready();
});

function UI_BLPaxRes() {
}

UI_BLPaxRes.ready = function() {
	$("#btnClose").click(function() {
		top.LoadHome();
	});

	$("#btnSave").click(function() {
		UI_BLPaxRes.saveClick();
	});

	$("#blacklistType").change(function() {
		UI_BLPaxRes.dateRangeEnableDisable();
	})

	UI_BLPaxRes.createGrid();

	$("#searchBlacklistPax").click(function() {
		if (UI_BLPaxRes.searchValidate()) {
			UI_BLPaxRes.createGrid();
		}
	});
	$("#btnCancel").click(function() {
		//
		$("#pnrNO").submit("33336053");
		$("#frmRes").submit();
	});
	$("#fromDate").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "-99:+99",
		buttonImageOnly : true,
		buttonText : "Select date",
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true
	});

	$("#toDate").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "+0:+99",
		minDate : "0Y",
		buttonImageOnly : true,
		buttonText : "Select date",
		changeMonth : true,
		changeYear : true,
	});

	$("#dateOfBirth").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "-99:+99",
		buttonImageOnly : true,
		buttonText : "Select date",
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true
	});

}

UI_BLPaxRes.formatDate = function(value) {
	var hipDate = value.substr(0, 10);
	return hipDate.replace("-", "/")
}

UI_BLPaxRes.createGrid = function() {

	var blacklistedPaxDTOJsonToString = function(cellVal, options, rowObject) {
		return JSON.stringify(cellVal);
	}
	var pnrLinkFormatter = function(cellVal, options, rowObject) {
		isDummy = rowObject['blacklisPaxReservationTO'].dummyBooking;
		link = "<a href='javascript:UI_BLPaxRes.redirectToLoadPnr(\"" + cellVal
				+ "\", \""+isDummy+"\")'><Bold>" + cellVal + "</Bold></a>";
		return link;
	}

	var dateFomatter = function(cellVal, options, rowObject) {
		var treturn = "&nbsp;";
		if (cellVal != null) {
			treturn = cellVal.split("T")[0];
		}
		return treturn;
	}

	var statusFomatter = function(cellVal, options, rowObject) {
		var treturn;

		if (cellVal == null) {
			treturn = "&nbsp;";
		} else if (cellVal == "Y") {
			treturn = "WhiteListed";
		} else {
			treturn = "Blscklisted";
		}
		return treturn;
	}
	var typeFomatter = function(cellVal, options, rowObject) {
		var type;

		if (cellVal == null) {
			type = "&nbsp;";
		} else if (cellVal == "T") {
			type = "Tempory";
		} else {
			type = "Permanent";
		}
		return type;
	}

	$("#blacklistedPaxGridContainer").empty().append(
			'<table id="blacklistedPaxGrid"></table><div id="blPages"></div>')
	var data = {};

	/*txtFname = trim($("#firstName").val());
	if (txtFname != null && txtFname != "")
		data['blPaxResToSearchReq.fName'] = txtFname;

	txtLname = trim($("#lastName").val());
	if (txtLname != null && txtLname != "")
		data['blPaxResToSearchReq.lName'] = txtLname;*/

	paxName = trim($("#paxName").val());
	if (paxName != null && paxName != "")
		data['blPaxResToSearchReq.fullName'] = paxName;
	
	txtPassportNo = trim($("#passportNo").val());
	if (txtPassportNo != null && txtPassportNo != "")
		data['blPaxResToSearchReq.passportNo'] = txtPassportNo;

	txtPnr = trim($("#pnr").val());
	if (txtPnr != null && txtPnr != "")
		data['blPaxResToSearchReq.pnr'] = txtPnr;

	data['blPaxResToSearchReq.nationalityCode'] = parseInt(trim($(
			"#nationality").val()));

	txtDOb = $("#dateOfBirth").val();
	if (txtDOb != null && txtDOb != "")
		data['blPaxResToSearchReq.dateOfBirth'] = txtDOb;

	data['blPaxResToSearchReq.blacklistType'] = $("#blacklistType").val();

	txtFrmDate = $("#fromDate").val();
	if (txtFrmDate != null && txtFrmDate != "")
		data['blPaxResToSearchReq.effectiveFrom'] = txtFrmDate;

	txtToDate = $("#toDate").val();
	if (txtToDate != null && txtToDate != "")
		data['blPaxResToSearchReq.effectiveTo'] = txtToDate;

	data['blPaxResToSearchReq.isActioned'] = $("#blResStatus").val();
	
	var blPaxGrid = $("#blacklistedPaxGrid")
			.jqGrid(
					{
						datatype : function(postdata) {
							$
									.ajax({
										url : 'blackListedPaxReservation!search.action',
										data : $.extend(postdata, data),
										dataType : "json",
										beforeSend : ShowPopProgress(),
										type : "POST",
										complete : function(jsonData, stat) {
											if (stat == "success") {
												respnsData = eval("("
														+ jsonData.responseText
														+ ")");
												if (respnsData.msgType != "Error") {
													$
															.data(
																	blPaxGrid[0],
																	"gridData",
																	respnsData.blPaxResList);
													blPaxGrid[0]
															.addJSONData(respnsData);
													$("#btnSave").prop(
															'disabled', true);
													$("#btnCancel").prop(
															'disabled', true);
												} else {
													HidePopProgress();
													alert(respnsData.message);
												}
												HidePopProgress();
											}
										}
									});
						},
						height : 250,

						colNames : [ 'PNR','Passenger Name', /*'First Name', 'Last Name',*/
								'PNR Status', 'Flight No', 'Dept. Date',
								'Arr. Date', 'Stuatus', 'Type', 'Nationality',
								'Date of Birth', 'Passport No',
								'BlacklistRes Id', 'Blacklist Pax Id',
								'Pnr Pax Id', 'version', 'Reason to Allow','IsDummy',
								'ISACTIONED','Reason for whitelist' ],
						colModel : [
								{
									name : 'pnr',
									index : 'pnr',
									jsonmap : 'blacklisPaxReservationTO.pnr',
									width : 60,
									formatter : pnrLinkFormatter
								},
								/*{
									name : 'fName',
									index : 'fName',
									jsonmap : 'blacklisPaxReservationTO.fName',
									width : 90,
									sorttype : "string"
								},
								{
									name : 'lName',
									index : 'lName',
									jsonmap : 'blacklisPaxReservationTO.lName',
									width : 90,
									sorttype : "string"
								},*/
								{
									name : 'PaxName',
									index : 'paxName',
									jsonmap : 'blacklisPaxReservationTO.fullName',
									width : 170,
									sorttype : "string"
								},
								{
									name : 'pnrStatus',
									index : 'pnrStatus',
									jsonmap : 'blacklisPaxReservationTO.pnrStatus',
									width : 80
								},
								{
									name : 'flightNo',
									index : 'flightNo',
									jsonmap : 'blacklisPaxReservationTO.flightNo',
									width : 70
								},
								{
									name : 'depatureDate',
									index : 'depatureDate',
									jsonmap : 'blacklisPaxReservationTO.depatureDate',
									width : 75
								},
								{
									name : 'arrivalDate',
									index : 'arrivalDate',
									jsonmap : 'blacklisPaxReservationTO.arrivalDate',
									width : 75
								},
								{
									name : 'status',
									index : 'status',
									jsonmap : 'blacklisPaxReservationTO.isActioned',
									width : 70,
									formatter : statusFomatter
								},
								{
									name : 'blacklistType',
									index : 'blacklistType',
									jsonmap : 'blacklisPaxReservationTO.blacklistType',
									width : 65,
									formatter : typeFomatter
								},
								{
									name : 'nationality',
									index : 'nationality',
									jsonmap : 'blacklisPaxReservationTO.nationality',
									width : 80
								},
								{
									name : 'dateOfBirth',
									index : 'dateOfBirth',
									jsonmap : 'blacklisPaxReservationTO.dateOfBirth',
									width : 100,
									formatter : dateFomatter

								},
								{
									name : 'passportNo',
									index : 'passportNo',
									jsonmap : 'blacklisPaxReservationTO.passportNo',
									width : 80,
									sorttype : "string"
								},

								{
									name : 'isActioned',
									index : 'isActioned',
									jsonmap : 'blacklisPaxReservationTO.isActioned',
									hidden : true
								},
								{
									name : 'blacklistReservationId',
									index : 'blacklistReservationId',
									jsonmap : 'blacklisPaxReservationTO.blacklistReservationId',
									hidden : true
								},
								{
									name : 'blacklistPaxId',
									index : 'blacklistPaxId',
									jsonmap : 'blacklisPaxReservationTO.blacklistPaxId',
									hidden : true
								},
								{
									name : 'pnrPaxId',
									index : 'pnrPaxId',
									jsonmap : 'blacklisPaxReservationTO.pnrPaxId',
									hidden : true
								},
								{
									name : 'version',
									index : 'version',
									jsonmap : 'blacklisPaxReservationTO.version',
									hidden : true
								},
								{
									name : 'dummyBooking',
									index : 'dummyBooking',
									jsonmap : 'blacklisPaxReservationTO.dummyBooking',
									hidden : true
								},
								{
									name : 'reasonToAllow',
									index : 'reasonToAllow',
									jsonmap : 'blacklisPaxReservationTO.reasonToAllow',
									hidden : true
								},{
									name : 'reasonForWhitelisted',
									index : 'reasonForWhitelisted',
									jsonmap : 'blacklisPaxReservationTO.reasonForWhitelisted',
									hidden : true
								} ],
						caption : "",
						rowNum : 20,
						viewRecords : true,
						width : 935,
						pager : jQuery('#blPages'),
						jsonReader : {
							root : "blPaxResList",
							page : "page",
							total : "total",
							records : "records",
							repeatitems : false,
							id : "0"
						},

					}).navGrid("#blPages", {
				refresh : true,
				edit : false,
				add : false,
				del : false,
				search : false
			});

	$("#blacklistedPaxGrid").click(function() {
		UI_BLPaxRes.gridOnClick();
	});
}

UI_BLPaxRes.gridOnClick = function() {
	UI_BLPaxRes.formReset();

	UI_BLPaxRes.rowId = $("#blacklistedPaxGrid").getGridParam('selrow');
	UI_BLPaxRes.rowData = $("#blacklistedPaxGrid").getRowData(UI_BLPaxRes.rowId);

	$("#btnSave").prop('disabled', false);
	$("#btnCancel").prop('disabled', false);

	UI_BLPaxRes.populateForm(UI_BLPaxRes.rowData.isActioned, UI_BLPaxRes.rowData.reasonForWhitelisted);
}

UI_BLPaxRes.dateRangeEnableDisable = function() {
	if ($("#blacklistType").val() == "T") {
		$("#fromDate").datepicker("option", "disabled", false);
		$("#toDate").datepicker("option", "disabled", false);
	} else {
		$("#fromDate").val("");
		$("#toDate").val("");
		$("#fromDate").datepicker("option", "disabled", true);
		$("#toDate").datepicker("option", "disabled", true);
	}
}

UI_BLPaxRes.formReset = function() {
	$("#txtBlacklistResId").val();
}

UI_BLPaxRes.populateForm = function(status , reason) {
	$("#isActioned").val(status);
	$("#reasonForWhitelisted").val(reason);
}

ShowPopProgress = function() {
	setVisible("divLoadMsg", true);
	UI_BLPaxRes.disableButtons();
}

HidePopProgress = function() {
	setVisible("divLoadMsg", false);
}

UI_BLPaxRes.updateValidate = function() {
	if (UI_BLPaxRes.rowId == null || UI_BLPaxRes.rowId == "") {
		alert(arrError.rowReq);
		return false;
	} else if (UI_BLPaxRes.rowData == null || UI_BLPaxRes.rowData == "") {
		alert(arrError.rowReq);
		return false;
	} else if ($("#isActioned").val() == "" || $("#isActioned").val() == null) {
		alert(arrError.actionReq);
	} else if($("#reasonForWhitelisted").val()==null || trim($("#reasonForWhitelisted").val())==""){
		alert(arrError.reasonReqForWL)
	}else {
		return true;
	}
}

UI_BLPaxRes.searchValidate = function() {
	fromdate = trim($("#fromDate").val());
	todate = trim($("#toDate").val());

	if ((todate != null && todate != "")
			&& (fromdate == null || fromdate == "")) {
		alert(arrError.fromDateReq);
		return false;
	}
	if ((todate == null || todate == "")
			&& (fromdate != null && fromdate != "")) {
		alert(arrError.toDateReq);
		return false;
	}

	if (!CheckDates($("#fromDate").val(), $("#toDate").val())) {
		alert(arrError.fromGrtTo);
		return false;
	}

	return true;
}

UI_BLPaxRes.saveClick = function() {
	if (!UI_BLPaxRes.updateValidate()) {
		return false;
	}

	var data = {};
	data['blacklistResTOSaveReq.blacklistReservationId'] = parseInt(UI_BLPaxRes.rowData.blacklistReservationId);
	data['blacklistResTOSaveReq.pnrPaxId'] = parseInt(UI_BLPaxRes.rowData.pnrPaxId);
	data['blacklistResTOSaveReq.blacklistPaxId'] = parseInt(UI_BLPaxRes.rowData.blacklistPaxId);
	data['blacklistResTOSaveReq.reasonToAllow'] = UI_BLPaxRes.rowData.reasonToAllow;
	data['blacklistResTOSaveReq.version'] = parseInt(UI_BLPaxRes.rowData.version);
	data['blacklistResTOSaveReq.isActioned'] = $("#isActioned option:selected").val();
	data['blacklistResTOSaveReq.reasonForWhitelisted'] = $("#reasonForWhitelisted").val();

	$.ajax({
		url : 'blackListedPaxReservation!update.action',
		data : data,

		beforeSend : ShowPopProgress(),
		type : "POST",
		complete : function(jsonData, stat) {
			if (stat == "success") {
				// respnsData = eval("("+jsonData.responseText+")");
				respnsData = jsonData.responseText;
				if (respnsData.msgType != "Error") {
					HidePopProgress();
					alert(arrError.updateSuccess);
					UI_BLPaxRes.saveSuccess();
				} else {
					HidePopProgress();
					alert(respnsData.message);
				}
			}
		}
	});

}

UI_BLPaxRes.saveSuccess = function() {
	UI_BLPaxRes.createGrid();
}

UI_BLPaxRes.enableButtons = function() {
	$("#btnSave").prop('disabled', false);
	$("#btnCancel").prop('disabled', false);
}

UI_BLPaxRes.disableButtons = function() {
	$("#btnSave").prop('disabled', true);
	$("#btnCancel").prop('disabled', true);
}

UI_BLPaxRes.redirectToLoadPnr = function(pnrVal,dummyBooking) {
	
	top.strSearchBookingPNR = pnrVal;
	top.blnSearchBooking = true;
	
	var redirectUrl = "showNewFile!searchRes.action?load=";
	if(dummyBooking == "Y"){
		top.top.blnInterLine =true;
	}
	redirectUrl += "LOAD";
	top[1].location.replace(redirectUrl);
	
}
