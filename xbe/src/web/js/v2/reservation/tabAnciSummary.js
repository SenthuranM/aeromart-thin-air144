UI_tabAnciSummary = function(){};
(function(UI_tabAnciSummary){
	UI_tabAnciSummary.anciType = { SSR:'SSR',SEAT:'SEAT',MEAL:'MEAL',HALA:'HALA',INSURANCE:'INSURANCE',BAGGAGE:'BAGGAGE',APSERVICE:'AIRPORT_SERVICE', APTRANSFER:'AIRPORT_TRANSFER' };
	UI_tabAnciSummary.initialized = false; 
	UI_tabAnciSummary.focMealEnabled = false;
	UI_tabAnciSummary.init = function(){
		if(!UI_tabAnciSummary.initialized){
			UI_tabAnciSummary.constructSeatGrid({id:"#tblSummarySeats"});
			UI_tabAnciSummary.constructMealGrid({id:"#tblSummaryMeals"});
			UI_tabAnciSummary.constructBaggageGrid({id:"#tblSummaryBaggages"});
			UI_tabAnciSummary.constructInsuranceGrid({id:"#tblSummaryInsurance"});
			UI_tabAnciSummary.constructSSRGrid({id:"#tblSummarySSR"});
			UI_tabAnciSummary.constructAPServiceGrid({id: "#tblSummaryAPService"});
			UI_tabAnciSummary.constructAPTransferGrid({id: "#tblSummaryAPTransfer"});
			$('#btnAnciSummaryClose').click(function(){UI_tabAnciSummary.hide()});
			UI_tabAnciSummary.initialized = true;
		}
	}
	UI_tabAnciSummary.hide = function(){
		$('#divAnciSummary').hide();
		$("#page-mask").remove();
	}
	
	UI_tabAnciSummary.show = function(paxWiseAnci, insurance, isFOCMealEnabled){
		
		$("BODY").append('<div id="page-mask"><p/></div>');
		$("#page-mask").css({
			position: 'absolute',
			zIndex: 4,
			top: '0px',
			left: '0px',
			width: '100%',
			height: $(document).height(),
			opacity:0.1
		});
		UI_tabAnciSummary.focMealEnabled = isFOCMealEnabled;
		UI_tabAnciSummary.init();
		var retObj = UI_tabAnciSummary.processPaxData(paxWiseAnci);
		var jsonPaxWiseAnci = retObj.paxList;
		var anciAvailability = retObj.availability;
		var hasAtLeastOne = false;
		if(anciAvailability.ssr){
			hasAtLeastOne = true;
			$('#trSummaryAnciSSR').show();
			UI_commonSystem.fillGridData({id:"#tblSummarySSR", data:UI_tabAnciSummary.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciSummary.anciType.SSR), editable:false});
		}else{
			$('#trSummaryAnciSSR').hide();
		}
		if(anciAvailability.meal){
			hasAtLeastOne = true;
			$('#trSummaryAnciMeal').show();
			UI_commonSystem.fillGridData({id:"#tblSummaryMeals", data:UI_tabAnciSummary.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciSummary.anciType.MEAL), editable:false});
		}else{
			$('#trSummaryAnciMeal').hide();
		}
		if(anciAvailability.baggage){
			hasAtLeastOne = true;
			$('#trSummaryAnciBaggage').show();
			UI_commonSystem.fillGridData({id:"#tblSummaryBaggages", data:UI_tabAnciSummary.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciSummary.anciType.BAGGAGE), editable:false});
		}else{
			$('#trSummaryAnciBaggage').hide();
		}
		if(anciAvailability.seat){
			hasAtLeastOne = true;
			$('#trSummaryAnciSeat').show();
			UI_commonSystem.fillGridData({id:"#tblSummarySeats", data:UI_tabAnciSummary.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciSummary.anciType.SEAT), editable:false});
		}else{
			$('#trSummaryAnciSeat').hide();
		}
		if(anciAvailability.seat){
			hasAtLeastOne = true;
			$('#trSummaryAnciSeat').show();
			UI_commonSystem.fillGridData({id:"#tblSummarySeats", data:UI_tabAnciSummary.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciSummary.anciType.SEAT), editable:false});
		}else{
			$('#trSummaryAnciSeat').hide();
		}
		if(anciAvailability.aps){
			hasAtLeastOne = true;
			$('#trSummaryAnciAPService').show();
			UI_commonSystem.fillGridData({id:"#tblSummaryAPService", data:UI_tabAnciSummary.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciSummary.anciType.APSERVICE), editable:false});
		} else {
			$('#trSummaryAnciAPService').hide();
		}
		if(anciAvailability.apt){
			hasAtLeastOne = true;
			$('#trSummaryAnciAPTransfer').show();
			UI_commonSystem.fillGridData({id:"#tblSummaryAPTransfer", data:UI_tabAnciSummary.getDataForAnci(jsonPaxWiseAnci,UI_tabAnciSummary.anciType.APTRANSFER), editable:false});
		} else {
			$('#trSummaryAnciAPTransfer').hide();
		}
		
		
		if(insurance != null && insurance.selected){
			hasAtLeastOne = true;
			var formatDate = UI_commonSystem.dateFormatter('accelaero');
			var insuranceObj = $.airutil.dom.cloneObject(insurance);
			if(insuranceObj.dateOfReturn!=null && insuranceObj.dateOfTravel){
				insuranceObj.dateOfReturn = formatDate(insuranceObj.dateOfReturn);
				insuranceObj.dateOfTravel = formatDate(insuranceObj.dateOfTravel);
			}
			if(insuranceObj.policyCode ==null){
				insuranceObj.policyCode = '';
			}
			$('#trSummaryAnciInsurance').show();			
			UI_commonSystem.fillGridData({id:"#tblSummaryInsurance", data:[insuranceObj], editable:false});
		}else{
			$('#trSummaryAnciInsurance').hide();
		}
		if(hasAtLeastOne){
			$('#divAnciSummary').show();
		}else{
			alert('Ancillary not selected');
			UI_tabAnciSummary.hide();
		}
	}
	
	/*
	 * Grid Constructor for Seat
	 */
	UI_tabAnciSummary.constructSeatGrid = function(inParams){
		var strDOBMand = "";
		
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[ geti18nData('ancillary_PassengerName','Passenger Name'), geti18nData('ancillary_Segment','Segment'),geti18nData('ancillary_FlightNo','Flight No') , geti18nData('ancillary_DepartureDate','Departure Date'), geti18nData('ancillary_SeatNumber','Seat Number'),geti18nData('ancillary_Charge','Charge')],
			colModel:[
			    {name:'paxName', index:'paxName', width:100, align:"left"},
			    {name:'segCode', index:'segCode', width:50, align:"center"},
			    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
			    {name:'departureDate', index:'departureDate', width:50, align:"center"},
			    {name:'seatNumber', index:'seatNumber', width:50, align:"center"},
			    {name:'seatCharge', index:'seatCharge', width:50, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}
	
	/*
	 * Grid Constructor for Meal
	 */
	UI_tabAnciSummary.constructMealGrid = function(inParams){
		var strDOBMand = "";
		
		if(UI_tabAnciSummary.focMealEnabled == false){
			$(inParams.id).jqGrid({
				height: 100,
				width: 910,
				colNames:[ geti18nData('ancillary_PassengerName','Passenger Name'),geti18nData('ancillary_Segment','Segment') ,geti18nData('ancillary_FlightNo','Flight No') , geti18nData('ancillary_DepartureDate','Departure Date'), geti18nData('ancillary_Meals','Meal(s)'), geti18nData('ancillary_Charge','Charge')],
				colModel:[
				    {name:'paxName', index:'paxName', width:100, align:"left"},
				    {name:'segCode', index:'segCode', width:50, align:"center"},
				    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
				    {name:'departureDate', index:'departureDate', width:50, align:"center"},
				    {name:'mealDesc', index:'mealDesc', width:100, align:"center"},
				    {name:'mealCharge', index:'mealCharge', width:50, align:"center"}
				],
				imgpath: UI_commonSystem.strImagePath,
				multiselect: false,
				datatype: "local",
				viewrecords: true
			});
		}else{
			$(inParams.id).jqGrid({
				height: 100,
				width: 910,
				colNames:[  geti18nData('ancillary_PassengerName','Passenger Name'),geti18nData('ancillary_Segment','Segment') ,geti18nData('ancillary_FlightNo','Flight No') , geti18nData('ancillary_DepartureDate','Departure Date'), geti18nData('ancillary_Meals','Meal(s)')],
				colModel:[
				    {name:'paxName', index:'paxName', width:100, align:"left"},
				    {name:'segCode', index:'segCode', width:50, align:"center"},
				    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
				    {name:'departureDate', index:'departureDate', width:50, align:"center"},
				    {name:'mealDesc', index:'mealDesc', width:100, align:"center"}
				    
				],
				imgpath: UI_commonSystem.strImagePath,
				multiselect: false,
				datatype: "local",
				viewrecords: true
			});
		}
		
	}
	
	/*
	 * Grid Constructor for Baggage
	 */
	UI_tabAnciSummary.constructBaggageGrid = function(inParams){
		var strDOBMand = "";
		
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[ geti18nData('ancillary_PassengerName','Passenger Name'),geti18nData('ancillary_Segment','Segment') ,geti18nData('ancillary_FlightNo','Flight No') , geti18nData('ancillary_DepartureDate','Departure Date'), geti18nData('ancillary_Baggage','Baggage'), geti18nData('ancillary_Charge','Charge')],
			colModel:[
			    {name:'paxName', index:'paxName', width:100, align:"left"},
			    {name:'segCode', index:'segCode', width:50, align:"center"},
			    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
			    {name:'departureDate', index:'departureDate', width:50, align:"center"},
			    {name:'baggageName', index:'baggageName', width:100, align:"center"},
			    {name:'baggageCharge', index:'baggageCharge', width:50, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
		
		
	}
	
	
	/*
	 * Grid Constructor for Insurance
	 */
	UI_tabAnciSummary.constructInsuranceGrid = function(inParams){
		var strDOBMand = "";
		
		$(inParams.id).jqGrid({
			height: 30,
			width: 910,
			colNames:[ geti18nData('ancillary_PolicyCode','Policy Code'), geti18nData('ancillary_Amount','Amount') ,  geti18nData('ancillary_Origin','Origin'), geti18nData('ancillary_Destination','Destination'),  geti18nData('ancillary_DepartureDate','Departure Date'), geti18nData('ancillary_PolicyCode','Policy Code'), geti18nData('ancillary_State','State')],
			colModel:[
			    {name:'policyCode', index:'policyCode', width:100, align:"left",hidden:true},
			    {name:'total', index:'total', width:50, align:"center"},
			    {name:'origin', index:'origin', width:50, align:"center"},
			    {name:'destination', index:'destination', width:50, align:"center"},
			    {name:'dateOfTravel', index:'dateOfTravel', width:50, align:"center"},
			    {name:'dateOfReturn', index:'dateOfReturn', width:50, align:"center"},
			    {name:'state', index:'state', width:50, align:"center", hidden:true}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}
	
	/*
	 * Grid Constructor for SSR
	 */
	UI_tabAnciSummary.constructSSRGrid = function(inParams){
		var strDOBMand = "";
		
		$(inParams.id).jqGrid({
			height: 100,
			width: 910,
			colNames:[geti18nData('ancillary_PassengerName','Passenger Name'),geti18nData('ancillary_Segment','Segment') ,geti18nData('ancillary_FlightNo','Flight No') , geti18nData('ancillary_DepartureDate','Departure Date'), geti18nData('ancillary_SSR','SSR Description'), geti18nData('ancillary_Charge','Charge')],
			colModel:[
			    {name:'paxName', index:'paxName', width:100, align:"left"},
			    {name:'segCode', index:'segCode', width:50, align:"center"},
			    {name:'flightNumber', index:'flightNumber', width:50, align:"center"},
			    {name:'departureDate', index:'departureDate', width:50, align:"center"},
			    {name:'ssrDesc', index:'ssrDesc', width:100, align:"center"}, /*,
			    {name:'ssrComment', index:'ssrComment', width:100, align:"center"},*/
			    {name:'ssrCharge', index:'ssrCharge', width:50, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}
	
	UI_tabAnciSummary.constructAPServiceGrid = function (inParams){
		var gridHeadings =[];
		var gridColumns = [];
		
		gridHeadings =[geti18nData('ancillary_PassengerName','Passenger Name'), geti18nData('ancillary_Airport','Airport'), geti18nData('ancillary_Applicability','Applicability'), geti18nData('ancillary_Description','Description'), geti18nData('ancillary_Charge','Charge')];
		gridColumns = [
						{name:'paxName', index:'paxName', width:100, align:"left"},
						{name:'airport', index:'airport', width:100, align:"left"},
						{name:'applicability', index:'applicability', width:100, align:"left"},
		   				{name:'apsDesc', index:'apsDesc', width:170, align:"left"},
						{name:'apsCharge', index:'apsCharge', width:100, align:"left"}
					];
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 100,
			width: 910,
			colNames: gridHeadings,
			colModel: gridColumns,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true
		});
	}
	
	UI_tabAnciSummary.constructAPTransferGrid = function (inParams){
		var gridHeadings =[];
		var gridColumns = [];
		
		gridHeadings =[geti18nData('ancillary_PassengerName','Passeger Name'), geti18nData('ancillary_Segment','Segment'), geti18nData('ancillary_Description','Description'), geti18nData('ancillary_Charge','Charge')];
		gridColumns = [
						{name:'paxName', index:'paxName', width:100, align:"left"},
						{name:'segCode', index:'segCode', width:50, align:"center"},
		   				{name:'aptDesc', index:'aptDesc', width:170, align:"left"},
						{name:'aptCharge', index:'aptCharge', width:100, align:"left"}
					];
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 100,
			width: 910,
			colNames: gridHeadings,
			colModel: gridColumns,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true
		});
	}
	
	
	UI_tabAnciSummary.processPaxData = function(paxList){
		var paxRetList = [];
		var availability = {seat:false,meal:false,ssr:false,aps:false, apt:false};
		var dateFormatter = UI_commonSystem.dateFormatter('accelaero');
		for(var i = 0 ; i < paxList.length; i++){			
			
			
			var anci = paxList[i].anci;
			if(anci == null){				
				continue;				
			}
			for(var k=0 ; k < anci.length ; k++){
				var selectedAnci = anci[k];
				var pax =$.airutil.dom.cloneObject(paxList[i]);
				pax.ssrDesc = '';
				pax.ssrCharge = selectedAnci.ssr.ssrChargeTotal;
				pax.mealDesc = '';
				pax.mealCharge = selectedAnci.meal.mealChargeTotal;
				pax.baggageName = '';
				pax.baggageCharge = selectedAnci.baggage.baggageChargeTotal;
				pax.seatNumber = '';
				pax.seatCharge = selectedAnci.seat.seatCharge;
				pax.apsDesc = '';
				pax.apsCharge = selectedAnci.aps.apsChargeTotal;
				pax.aptDesc = '';
				//pax.aptCharge = selectedAnci.apt.aptChargeTotal;
				pax.segCode = '';
				pax.paxName = pax.name;
				var segInfo = selectedAnci.segInfo;
				var ssrs  = selectedAnci.ssr.ssrs;
				var meals = selectedAnci.meal.meals;
				var seat = selectedAnci.seat;
				var baggages = selectedAnci.baggage.baggages;
				var apss = selectedAnci.aps.apss;
				var apts = selectedAnci.apt.apts;
				
				//{paxType:jsonPaxAdults[j].type,refI: j, seat:{seatNumber:'',seatCharge:0},meal:{meals:[],mealChargeTotal:0,mealQtyTotal:0},ssr:{ssrs:[],ssrChargeTotal:0}};
				if(segInfo!=null){
					var d = new Date(segInfo.ddt);
					pax.segCode = segInfo.segCode;
					pax.flightNumber = segInfo.flightNumber;				
					pax.departureDate = dateFormatter(d);
					pax.dateObj = d;
				}
				
				if(ssrs!=null){
					var ssrDesc = '';
					for(var j = 0; j < ssrs.length; j++){
						if(j>0) ssrDesc+=', ';
						ssrDesc += ssrs[j].description ;
						if(ssrs[j].text!=null && ssrs[j].text != '')
							ssrDesc += ' - ' + ssrs[j].text;
						availability.ssr= true;
					}
					pax.ssrDesc = ssrDesc;
				}
				if(meals!=null){
					var mealDesc = '';
					for(var j = 0; j < meals.length; j++){
						if(j>0) mealDesc+=', ';
						mealDesc += meals[j].mealQty + 'x ' +meals[j].mealName ;
						availability.meal= true;
					}
					pax.mealDesc = mealDesc;
					
				}
				if(seat!=null && seat.seatNumber!=null && seat.seatNumber != ''){
					pax.seatNumber = seat.seatNumber;
					pax.seatCharge = seat.seatCharge;
					availability.seat = true;
				}
				if(baggages!=null && baggages.length > 0){
					var baggageName = baggages[0].baggageName;					
					availability.baggage= true;
					pax.baggageName = baggageName;
					
				}
				if(apss!=null){
					var apsDesc = '';
					for(var j = 0; j < apss.length; j++){
						if(j>0) apsDesc+=', ';
						apsDesc += apss[j].description ;
						availability.aps= true;
						pax.airport = apss[j].airport;
					}
					pax.apsDesc = apsDesc;
				}
				if(apts!=null){
					var aptDesc = '';
					var airports = '';
					var aptCharge = 0;
					for(var j = 0; j < apts.length; j++){
					  if(j>0) { 
						  aptDesc+=', ';
						  airports+=', ';
					   }
					    var airportTransfer = apts[j].airport + ": " + apts[j].text;
						aptDesc += airportTransfer;
						airports += apts[j].airport;
						aptCharge += parseFloat(apts[j].charge);
						availability.apt= true;
					}
					pax.airports = airports;
					pax.aptDesc = aptDesc;
					pax.aptCharge = aptCharge;
				}
				
				paxRetList[paxRetList.length] = pax;
			}
			
		}
		paxRetList = $.airutil.sort.quickSort(paxRetList,UI_tabAnciSummary.segmentComparator);
		return {paxList:paxRetList,availability:availability};
	}
	
	UI_tabAnciSummary.segmentComparator = function (first ,second){
		return (first.dateObj.getTime()-second.dateObj.getTime());
	
	}
	
	UI_tabAnciSummary.getDataForAnci = function(paxList,anciType){
		var paxRetList = [];
		for(var i = 0; i < paxList.length ;i++){
			if(UI_tabAnciSummary.anciType.SSR != anciType && paxList[i].paxType == 'IN'){
				continue;
			}
			if(anciType == UI_tabAnciSummary.anciType.SSR && paxList[i].ssrDesc == ''){
				continue;
			}
			if(anciType == UI_tabAnciSummary.anciType.MEAL && paxList[i].mealDesc == ''){
				continue;
			}
			if(anciType == UI_tabAnciSummary.anciType.BAGGAGE && paxList[i].baggageName == ''){
				continue;
			}
			if(anciType == UI_tabAnciSummary.anciType.SEAT && paxList[i].seatNumber == ''){
				continue;
			}
			if(anciType != UI_tabAnciSummary.anciType.APSERVICE && paxList[i].paxType == 'IN'){
				continue;
			}
			if(anciType == UI_tabAnciSummary.anciType.APSERVICE && paxList[i].apsDesc == ''){
				continue;
			}
			if(anciType != UI_tabAnciSummary.anciType.APTRANSFER && paxList[i].paxType == 'IN'){
				continue;
			}
			if(anciType == UI_tabAnciSummary.anciType.APTRANSFER && paxList[i].aptDesc == ''){
				continue;
			}
			paxRetList[paxRetList.length]=paxList[i];
		}

		return paxRetList;
	}
	
})(UI_tabAnciSummary);