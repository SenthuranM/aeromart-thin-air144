	/*
	*********************************************************
		Description		: XBE Make Reservation - Search Flight Tab
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	var jsonSCObj = [
					{id:"#fromAirport", desc:"Departure location", required:true},
					{id:"#toAirport", desc:"Arrival location", required:true},
					{id:"#departureDate", desc:"Departure Date", required:false},
					{id:"#departureVariance", desc:"Departure Variance", required:false},
					{id:"#returnDate", desc:"Return Date", required:false},
					{id:"#returnVariance", desc:"Return Variance", required:false},
					{id:"#validity", desc:"Validity", required:false},
					{id:"#adultCount", desc:"Adults", required:false},
					{id:"#childCount", desc:"Children", required:false},
					{id:"#infantCount", desc:"Infants", required:false},
					{id:"#classOfService", desc:"Class", required:true},
					{id:"#selectedCurrency", desc:"Currency", required:true},
					{id:"#bookingType", desc:"Type", required:false},
					{id:"#selBookingClassCode", desc:"Type", required:false}
				];
	
	/* Fare Quote */
	var jsonFareQuote = [ ];
	
	/* Fare Quote Summary */
	var jsonFareQuoteSummary = ({currency : "",
						 totalSurcharges : "",
						 baseFare : "",
						 totalTaxes : "",
						 totalPrice : "",
						 totalHandlingCharge : "",
						 selectedCurrency: "",
						 selectedEXRate: "",
						 selectedtotalPrice:"",
						 fareType:"",
						 totalServiceTax:"",
						 adminFee:"",
						 selectCurrAdminFee:""});
	
	var jsonOutFlts = null;
	var jsonRetFlts = null;
	var setTimeoutForBaggageToolTip = false;
	

	// holds carrier wise ond map
	var ondMap = null;
	
	var segBreakFmt = function (objValue, options, rowObject){
		var arr = objValue.split("/");
		var outStr = '';
		for(var i = 0; i < arr.length ; i++){
			if( i == 0){					
				outStr +=arr[i];  
			}else if(i%3 == 0){
				outStr+= '/<br />' +arr[i];
			}else {
				outStr+= '/' +arr[i];
			}
			
		}
		return outStr ;
	};
	
	var jsonPrice = [ ];
	
	var currencyRound = true;
	var showFareDiscountCodes = false;
	
	var flightToViewBCFares = null;
	var allFlights = [];
	var fromViewBCFare = false;
	var hideBCFare = true;
	var btnText =  "Set StopOver time {0} in days";
	var isLogicalCCSelected = false;
	var agentWiseOrigins = new Array();
	var agentWiseDestinations = new Array();
	var originArray = new Array();
	var allOnds = top.arrOndMap;
	
	/*
	 * Make Reservation related Methods
	 */
	function UI_tabSearchFlights(){}
	
	UI_tabSearchFlights.OND_SEQUENCE = {OUT_BOUND:0, IN_BOUND:1};
	
	UI_tabSearchFlights.availableFare = null;
	UI_tabSearchFlights.availableOndLogicalCCInfo = [];
	UI_tabSearchFlights.ondRebuildLogicalCCAvailability={};
	UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability={}
	UI_tabSearchFlights.logicalCabinClassSelection = null;
	UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection = null;
	UI_tabSearchFlights.segWiseLogicalCabinClassSelection = null;
	UI_tabSearchFlights.ondSegWiseBookingClassSelection = null;
	
	UI_tabSearchFlights.displayBasedOnTemplates =false;
	
	/*
	 * App param indicating whether or not to show FareRule information 
	 * in FareQuote summary table.
	*/
	UI_tabSearchFlights.showFareRuleInfoInSummary=false;
	UI_tabSearchFlights.selectedFareType = '';	
	UI_tabSearchFlights.strOutDate = "";
	UI_tabSearchFlights.strRetDate = "";
	UI_tabSearchFlights.strOutDateFmt = "";
	UI_tabSearchFlights.strRetDateFmt = "";
	UI_tabSearchFlights.selectedOutDate = "";
	UI_tabSearchFlights.selectedRetDate = "";
	UI_tabSearchFlights.strDTFormatDDMMMYYYY = "DD/MMM/YYYY";
	UI_tabSearchFlights.objResSrchParams = null;
	UI_tabSearchFlights.strBkgClassDesc = "Economy Class";
	
	UI_tabSearchFlights.isOpenReturn = false;
	UI_tabSearchFlights.isOpenReturnConfirm = false;
	UI_tabSearchFlights.openReturnInfo = null;
	UI_tabSearchFlights.busCarrierCodes = [];	
	UI_tabSearchFlights.fareDiscount = {percentage:null, notes:null, code:null};
	UI_tabSearchFlights.jsonFareDiscountInfo = null;
	UI_tabSearchFlights.fareDiscountCodes = false;
	UI_tabSearchFlights.overrideFareDiscount = false;
	
	UI_tabSearchFlights.strNavDate = "";
	UI_tabSearchFlights.strNavLast = "";
	
	UI_tabSearchFlights.strNavOP = "OP";
	UI_tabSearchFlights.strNavON = "ON";
	UI_tabSearchFlights.strNavRP = "RP";
	UI_tabSearchFlights.strNavRN = "RN";
	
	UI_tabSearchFlights.strOrigin = "";
	UI_tabSearchFlights.strDestination = "";
	
	UI_tabSearchFlights.pgObj = null;
	UI_tabSearchFlights.blnNPLoaded = false;
	
	UI_tabSearchFlights.fareRulesInfo = null;
	UI_tabSearchFlights.showFareRuleAlert = false;
	UI_tabSearchFlights.flexiFareRulesInfo = null;
	UI_tabSearchFlights.airportMessage = null;
	// UI_tabSearchFlights.flexiSelectInfo = "";
	UI_tabSearchFlights.fltRPHInfo = null;
	// to mark that quote flexi charges
	UI_tabSearchFlights.ondWiseFlexiSelected = {};
	UI_tabSearchFlights.ondWiseFlexiQuote = {};
	UI_tabSearchFlights.ondWiseFlexiAvailable = {};
	UI_tabSearchFlights.ondwiseFlexiFare = {};
	UI_tabSearchFlights.flexiEnableInAnci = true;
	UI_tabSearchFlights.ondWiseBundleFareSelected = {};
	UI_tabSearchFlights.ondWiseBookingClassSelected = {};
	UI_tabSearchFlights.resAvailableBundleFareLCClassStr = {}
	UI_tabSearchFlights.OND_SEQUENCE = {OUT_BOUND:0, IN_BOUND:1};
	UI_tabSearchFlights.segWiseBookingClassOverride = {};
		
	UI_tabSearchFlights.strPaxType = "";
	
	/* Surcharges */
	UI_tabSearchFlights.jsonBDSurcharge = [{applicableToDisplay:"", surchargeCode:"", amount:"", carrierCode:""}];
	
	/* Tax Tos */
	UI_tabSearchFlights.jsonBDTax = [{applicableToDisplay:"", taxCode:"", amount:"", carrierCode:"", taxName:""}];
	
	UI_tabSearchFlights.blockSeatsReleaseTypes = ["XBE_RST","XBE_FQE","XBE_CLS","XBE_BCL"];	
	UI_tabSearchFlights.lastBlockSeatSystem = null;
	UI_tabSearchFlights.flightInfo = null;
	UI_tabSearchFlights.jsonFlightInfo = null;
	UI_tabSearchFlights.jsonResSegments = null;
	UI_tabSearchFlights.fltSegIdByJourneyMap =[];
	UI_tabSearchFlights.segmentsMapPerSegType=[];
	
	UI_tabSearchFlights.PNR = null;
	
	UI_tabSearchFlights.modifySearch = false;
	
	UI_tabSearchFlights.fromCombo = null;
	UI_tabSearchFlights.toCombo = null;
	UI_tabSearchFlights.surfaceSegmentConfigs = {
		sufaceSegmentAppendStr : "-",
		surfSegCodeSplitter : ":",
		groundStationPrimaryIdentifier : "!"
	};
	
	UI_tabSearchFlights.ondDataMap = [];
	UI_tabSearchFlights.originList = [];
	UI_tabSearchFlights.currencyList = [];
	UI_tabSearchFlights.ticketValidityExist = false;
	UI_tabSearchFlights.ticketValidityMax = null;
	UI_tabSearchFlights.multicity = false;
	UI_tabSearchFlights.calenderMinimumFare = false;
	UI_tabSearchFlights.isDomFareDiscountApplied = false;
	UI_tabSearchFlights.appliedFareDiscountInfo = null;
	UI_tabSearchFlights.isAgentCommissionApplied = false;
	UI_tabSearchFlights.dummyCreditDiscount = null;
	UI_tabSearchFlights.isSkipDiscount = false;
	UI_tabSearchFlights.isAllowChangeFare = true;
	
	UI_tabSearchFlights.waitListedSegmentAvailable = false;
	UI_tabSearchFlights.ignoreWaitListing = false;
	UI_tabSearchFlights.flightRPHWaitListingStatus = {};
	UI_tabSearchFlights.hideChangeFareButton = false;
	UI_tabSearchFlights.hubTimeDetailJSON = [];
	UI_tabSearchFlights.agentWiseOrigins = new Array();
	UI_tabSearchFlights.agentWiseOndDataMap = {};
	UI_tabSearchFlights.paxState = null;
    UI_tabSearchFlights.paxCountryCode = null;
    UI_tabSearchFlights.paxTaxRegistered = null;
    UI_tabSearchFlights.paxRegNo = null;
    UI_tabSearchFlights.fromCitySearch = false;
    UI_tabSearchFlights.toCitySearch = false;
    UI_tabSearchFlights.ondWiseCitySearch = {};
    UI_tabSearchFlights.baggageSearchParams = {};
	UI_tabSearchFlights.availableBaggageTooltip = {};
	UI_tabSearchFlights.multiCityRequoteFightRPHList = {};
	
	UI_tabSearchFlights.promoDiscountType = {
			PERCENTAGE : "PERCENTAGE",
			VALUE : "VALUE"
	}
	
	UI_tabSearchFlights.promoDiscountAs = {
			MONEY : "Money",
			CREDIT : "Credit"
	}
	var isAllOriginAirports = false;
	var isAllDestAirports = false;
	var isAllOriginDestAirports = false;
	
	
	

	/*
	 * Search Flight Tab Page On Load
	 */
	UI_tabSearchFlights.ready = function(){
		UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = false;
		UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = false;
		UI_tabSearchFlights.ondWiseFlexiQuote[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = true;
		UI_tabSearchFlights.ondWiseFlexiQuote[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = true;
		UI_tabSearchFlights.ondRebuildLogicalCCAvailability[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = true;
		UI_tabSearchFlights.ondRebuildLogicalCCAvailability[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = true;
		UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = false;
		UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = false;
		UI_tabSearchFlights.ondWiseFlexiAvailable = {};
		
		$("#draggable, #stopOverTimePopup").draggable();
		
		UI_PageShortCuts.initilizeMetaDataForSearchFlight();
		
		$("#departureDate").datepicker({ minDate: new Date(), showOn: "button", buttonImage: "../images/Calendar_no_cache.gif", buttonImageOnly: true, maxDate:'+2Y', yearRange:'-1:+2', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
		
		if(!UI_tabSearchFlights.isRequote()){
			$("#returnDate").datepicker({ minDate: new Date(), showOn: "button", buttonImage: "../images/Calendar_no_cache.gif", buttonImageOnly: true, maxDate:'+2Y', yearRange:'-1:+2', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
		}
		
		$("#departureDate").blur(function() { UI_tabSearchFlights.dateOnBlur({id:0});});
		$("#returnDate").blur(function() { UI_tabSearchFlights.dateOnBlur({id:1});});

		$("#btnSearch").decorateButton();
		$("#btnBackToAirline1").decorateButton();
		$("#btnBackToAirline2").decorateButton();
		$("#btnShowMeInterline").decorateButton();
		$("#btnReset").decorateButton();
		$("#btnCancel").decorateButton();
		$("#btnBook").decorateButton();
		$("#btnFQ").decorateButton();
		$("#btnTaxSurB").decorateButton();
		$("#btnFareRulesB").decorateButton();
		$("#btnDiscountPop").decorateButton();
		$("#btnFareDiscountApply").decorateButton();
		$('#btnFareDiscountCancel').decorateButton();
		$("#fareDiscountPercentage").numeric({allowDecimal:false});
		$("#btnFareDiscountRemove").decorateButton();
		$("#btnAddOND").decorateButton();
		$("#btnRequote").decorateButton();
		$("#btnShowLogicalOptions").decorateButton();
		$("#btnLogicalOptionClose").decorateButton();
		$("#btnTogglePBRKDown").decorateButton();
		$(".PBRKDown").hide();
		
		if (DATA_ResPro.initialParams.allowViewAllBCFares && UI_tabSearchFlights.isRequote()) {
			 hideBCFare = false;
		}
		
		if(DATA_ResPro.initialParams.openReturnEnabled &&  UI_commonSystem.strPGMode == "modifySegment"){
			$("#divValidity").show();
			$("#validity").show();
			$("#divOpen").show();
			$("#openReturn").show();	
		} else if(!DATA_ResPro.initialParams.openReturnEnabled || (UI_commonSystem.strPGMode != "")){
			$("#divValidity").hide();
			$("#validity").hide();
			$("#divOpen").hide();
			$("#openReturn").hide();
			
		} else {
			$("#divValidity").show();
			$("#validity").show();
			$("#divOpen").show();
			$("#openReturn").show();			
		}
		
		if(!DATA_ResPro.initialParams.bCSelectionAtAvailabilitySearchEnabled){
			$("#spnBookingClass").hide();
			$("#selBookingClassCode").hide();
		}else{
			$("#spnBookingClass").show();
			$("#selBookingClassCode").show();		
		}
		
		
		// $("#btnCFare").disableEnable(true);
		// FIXME check wt this is required
		// $("#ownCarrier").html(top.carrierCode);
		
		$("#btnCFare").decorateButton();
		$("#btnCFare").click(function() { UI_tabSearchFlights.changeFareClick();});
		$("#btnCAllFare").decorateButton();
		$("#btnCAllFare").click(function() { UI_tabSearchFlights.changeAllFareClick();});
		
		if (!DATA_ResPro.initialParams.allowCOSSearch) {
			$("#classOfService").disableEnable(true);
		}
		
		$("#btnSearch").click(function() { UI_tabSearchFlights.searchOnClick();});
		$("#btnReset").click(function() { UI_tabSearchFlights.resetOnClick();});
		$("#btnCancel").click(function() { UI_tabSearchFlights.cancelOnClick();});
		$("#btnBook").click(function() { UI_tabSearchFlights.bookOnClick();});
		//$("#btnCreateRequest").click(function() { UI_tabSearchFlights.openReuestSubmitWindow();});
		
		$("#requestSubmitPopup").dialog({ 
			autoOpen: false,
			modal:true,
			height: 'auto',
			width: 'auto',
			title:'Submit Request',
			close:function() {$("#requestSubmitPopup").dialog('close');}
		});
		
		$("#btnCreateRequest").click(function() { $("#requestSubmitPopup").dialog('open');});
		
		$("#btnFQ").click(function() { UI_tabSearchFlights.loadFareQuoteOnClick();});
		$("#btnTaxSurB").click(function() { UI_tabSearchFlights.taxesSurchargesOnClick();});
		$("#btnFareRulesB").click(function() { UI_tabSearchFlights.fareRulesOnClick();});
		

		$("#lnkOP").click(function() { UI_tabSearchFlights.gridDayNavigation({id:UI_tabSearchFlights.strNavOP});});
		$("#lnkON").click(function() { UI_tabSearchFlights.gridDayNavigation({id:UI_tabSearchFlights.strNavON});});
		$("#lnkRP").click(function() { UI_tabSearchFlights.gridDayNavigation({id:UI_tabSearchFlights.strNavRP});});
		$("#lnkRN").click(function() { UI_tabSearchFlights.gridDayNavigation({id:UI_tabSearchFlights.strNavRN});});

		$("#btnShowMeInterline").click(function() { UI_tabSearchFlights.showInterlineSearch();});
		$("#btnBackToAirline1").click(function() { UI_tabSearchFlights.backToAirlineSearch();});
		$("#btnBackToAirline2").click(function() { UI_tabSearchFlights.backToAirlineSearch();});
		
		$('#btnFareDiscountApply').click(function(){UI_tabSearchFlights.applyFareDiscount('apply');});
		$('#btnDiscountPop').click(function(){UI_tabSearchFlights.applyDiscountPop();});
		$('#btnFareDiscountCancel').click(function(){UI_tabSearchFlights.closeDiscuntPop();});
		$('#btnFareDiscountRemove').click(function(){UI_tabSearchFlights.applyFareDiscount('remove');});
		$('#classOfService').change(function(){UI_tabSearchFlights.fillBCDropDown();});
		$('#btnAddOND').click(function(){UI_tabSearchFlights.addONDOnClick();});
		$('#btnRequote').click(function(){UI_tabSearchFlights.requote();});
		$("#btnShowLogicalOptions").click(function(){
			UI_commonSystem.readOnly(true);
			$("#divRQLogicalCCList").show();
		});
		$("#btnLogicalOptionClose").click(function(){
			UI_commonSystem.readOnly(false);
			$("#divRQLogicalCCList").hide();
		});
		$('#btnTogglePBRKDown').click(function(){UI_tabSearchFlights.togglePBRKDown()});
		$(".PBRKDownBody").css({
			"overflow-y":"hidden",
			"height":"auto"
		});
		if (UI_tabSearchFlights.isRequote()) {
			$("#tdResultPane").css('height', '250px');
			$("#tdSearchFltArea").slideUp('fast');
			$("#btnBook").text("Confirm");
			UI_requote.constructGrid({id: "#tblAllONDs"});  
			$(".PBRKDownBody").css({
				"overflow-y":"auto",
				"maxHeight":250
			});
		}
		
		if(DATA_ResPro.systemParams.dynamicOndListPopulationEnabled
				&& UI_tabSearchFlights.ondListLoaded()){
			UI_tabSearchFlights.initDynamicOndList();
		} else if(DATA_ResPro.initialParams.allowRouteSelectionForAgents) {
			UI_tabSearchFlights.initAgentWiseOrigins(0, false);
			UI_tabSearchFlights.initAgentWiseOndList();
		}

		UI_tabSearchFlights.fillDropDownData();
		UI_tabSearchFlights.initialization();
		// Depend on the page mode set the parent tab object
		// Reason : Same page called through Modify Segment / Make Booking
		// flows.
		if(UI_tabSearchFlights.isRequote()){
			UI_tabSearchFlights.pgObj = UI_modifyResRequote;
			UI_tabSearchFlights.disablePageControls(true);
		} else {
			if (UI_commonSystem.strPGMode == ""){
				UI_tabSearchFlights.pgObj = UI_makeReservation;
				UI_tabSearchFlights.disableEnablePageControls(true);
			} else if (UI_commonSystem.strPGMode == "transferSegment") {
				UI_tabSearchFlights.pgObj = UI_TransferSegment;			
				UI_tabSearchFlights.disablePageControls(true);
				$("#btnExcludeCharge").hide();
			} else if (UI_commonSystem.strPGMode == "addSegment") {
				UI_tabSearchFlights.pgObj = UI_addSegment;
				UI_tabSearchFlights.disablePageControls(true);
			} else{
				UI_tabSearchFlights.pgObj = UI_modifySegment;
				UI_tabSearchFlights.disablePageControls(true);
			}
		}
		
		
		$('#frmSrchFlt').bind("keypress", function(keyEvent){ UI_tabSearchFlights.checkEnterAndSubmit(keyEvent);});
		
		/* construct grid */
		UI_tabSearchFlights.constructGrid({id:"#tblOutboundFlights", radioID:"depatureSelection"});  
		UI_tabSearchFlights.constructGrid({id:"#tblInboundFlights", radioID:"returnSelection"});
		UI_tabSearchFlights.constructOpenReturnGrid();  
		if(DATA_ResPro.addGroundSegment && DATA_ResPro.addGroundSegment == "true"){
			UI_tabSearchFlights.setCalanderOnFromToChange();
		}
		
		/* On change */
		$(document).on('change',"#fromAirport",function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originChanged();});
		$(document).on('change',"#toAirport",function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originDestinationChanged();});
		$("#fAirport").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originChanged();});
		$("#tAirport").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originDestinationChanged();});		
		$("#departureDate").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#departureVariance").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#returnDate").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#returnVariance").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#validity").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#adultCount").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#childCount").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#infantCount").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#classOfService").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#selectedCurrency").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#bookingType").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.bookingTypeOnChange();});
		$("#selFareTypeOptions").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#selPaxTypeOptions").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#selBookingClassCode").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$('#openReturn').change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();UI_tabSearchFlights.openReturnOnChange();});
		$('#selSearchOptions').change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.searchOptionOnChange();});
		$("#promoCode").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		$("#bankIdNo").change(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
		UI_tabSearchFlights.blnNPLoaded = true;
		// Set Search option modifySegmnent/transfersegment
		// UI_tabSearchFlights.setSearchOption();
		UI_commonSystem.pageOnChange();
		UI_tabSearchFlights.hideAirportMessage();
		UI_tabSearchFlights.openReturnOnChange();
		
		// to be compatible with IE
		window.onbeforeunload = function (e) {
			UI_tabSearchFlights.releaseBlockSeats();
			return;
		};	
		// not allowing to add ground segment for via return flight
		if(DATA_ResPro.addGroundSegment != "false"  && 
				( DATA_ResPro.addGroundSegment || DATA_ResPro.addGroundSegment == "true") ){
			$('#returnDate').datepicker( "disable" );
			$("#returnVariance").disableEnable(true);
		}
		if((DATA_ResPro.modifySegments != null && 
				(DATA_ResPro.modifySegments.modifyingSurfaceSegment != "false" 
					&& (DATA_ResPro.modifySegments.modifyingSurfaceSegment
				|| DATA_ResPro.modifySegments.modifyingSurfaceSegment == "true")))){
			$('#returnDate').datepicker( "disable" );
			$("#returnVariance").disableEnable(true);
		}
		var setHeights = function(collapse, diff){
			var curHeight = parseInt($("#tdResultPane").css("height"), 10);
			diff = parseInt(diff,10);
			if (collapse){
				$("#tdResultPane").animate({height:curHeight+diff},500, function() {
					$("#tdResultPane").css({"overflow-y":"auto","overflow-x":"hidden"});
				});
			}else{
				$("#tdResultPane").css({height:curHeight-diff});
			}
			$("#tdResultPane").css({"overflow-y":"auto","overflow-x":"hidden"});
		};
		$("#trSearchParams").collapsePanel({
			"startLabel": geti18nData('availability_HideSeachPannel','Hide search panel'),
			"collapseLabel": geti18nData('availability_ShowSeachPannel','Show search panel'),
			"eapandLabel": geti18nData('availability_HideSeachPannel','Hide search panel'),
			"extaFun":setHeights
		});
		if(UI_tabSearchFlights.isRequote()){
			// UI_requote.constructGrid({id:"#tblAllONDs"});
			UI_requote.fromFlightSearch = true;
			UI_requote.createFlightSegments();
			$("#trFareSection").hide();
			$("#trSearchParams_div").hide();
		}
		
		//initialize exclude charges UI
		UI_tabSearchFlights.initExcludeChargesView(true);
		
		$("#iconStopOverClose").click(function() {
			$("#stopOverTimePopup").hide();
		});
		
		if(DATA_ResPro.initialParams.promoCodeEnabled) {
			$("#trPromoCode").show();
		} else {
			$("#trPromoCode").hide();
		}

        if (!UI_tabSearchFlights.isRequote()) {
            var h = 560 - $("#tdSearchFltArea").height();//560 took as max viewport high in the resolution
            $("#tdResultPane").css('height', parseInt(h,10));
        }



      // removed min fare calender options sashrika

	};
	UI_tabSearchFlights.togglePBRKDown = function(){
		$(".PBRKDown").toggle();
	};
	
	UI_tabSearchFlights.initAgentWiseOrigins = function(originIndex, dynamicOndInitialized) {
		var originId = 0;
		if(typeof(allOnds['***']) != "undefined"){
			isAllOriginAirports = true;
			if(allOnds['***'].indexOf('***') > -1){
				isAllOriginDestAirports = true;									
			}
		}
		if(dynamicOndInitialized){
			if(typeof(origins[originIndex]) != "undefined"){
				
				
				var cityCode = 'N';
				if(airports[originIndex]['city']!=undefined && airports[originIndex]['city']!=null 
						&& (airports[originIndex]['city'] || airports[originIndex]['city']=="true")){
					cityCode = 'Y';
				}
				
				
				if(isAllOriginAirports){
					var currentSize = originArray.length;
					originArray[currentSize] = [];
					originArray[currentSize][0] = airports[originIndex]['code'];
					originArray[currentSize][1] = airports[originIndex]['en'];
					originArray[currentSize][2] = airports[originIndex]['code'] + ' - ' + airports[originIndex]['en'];
					originArray[currentSize][3] = cityCode ;
					originArray[currentSize][4] = airports[originIndex]['code']+'_'+cityCode ;
				} else if(typeof(allOnds[airports[originIndex]['code']]) != "undefined"){
					var currentSize = originArray.length;
					originArray[currentSize] = [];
					originArray[currentSize][0] = airports[originIndex]['code'];
					originArray[currentSize][1] = airports[originIndex]['en'];
					originArray[currentSize][2] = airports[originIndex]['code'] + ' - ' + airports[originIndex]['en'];
					originArray[currentSize][3] = cityCode ;
					originArray[currentSize][4] = airports[originIndex]['code']+'_'+cityCode ;
				}
				
		
			}				
		} else {
			originArray = top.arrAirportsV2;
		}
		if (originArray.length > 0) {				
			originArray.sort(UI_tabSearchFlights.customSort);
			UI_tabSearchFlights.agentWiseOrigins = originArray;	
		}
	}
	
	UI_tabSearchFlights.initAgentWiseOndList = function(){		
		var originId = 0;
		
		for(var i=0;i<Object.keys(allOnds).length;i++){
			
			var agentWiseOrigin = Object.keys(allOnds)[i];			
			var agentWiseDestinations = allOnds[agentWiseOrigin].split('|');
			if(agentWiseDestinations[0] = '***'){
				isAllDestAirports = true;
			}
			var tempAgentWiseDestArray = [];
			
			if(isAllDestAirports){
				for (var destIndex=0;destIndex<originArray.length;destIndex++) {
					var destArr = originArray[destIndex];
//					var destArr = curDest.split(',');
					if(destArr[0] != null){					
						var currentSize = tempAgentWiseDestArray.length;
						tempAgentWiseDestArray[currentSize] = [];
						tempAgentWiseDestArray[currentSize][0] = destArr[0].trim();
						tempAgentWiseDestArray[currentSize][1] = destArr[0].trim();
						tempAgentWiseDestArray[currentSize][2] = destArr[0].trim() + ' - ' + destArr[1].trim();
						tempAgentWiseDestArray[currentSize][3] = 'N';
						tempAgentWiseDestArray[currentSize][4] = destArr[0].trim() + '_N';
					}				
				}
			} else {
				for (var destIndex=0;destIndex<agentWiseDestinations.length;destIndex++) {
					var curDest = agentWiseDestinations[destIndex];
					var destArr = curDest.split(',');
					if(curDest[0] != null){					
						var currentSize = tempAgentWiseDestArray.length;
						tempAgentWiseDestArray[currentSize] = [];
						tempAgentWiseDestArray[currentSize][0] = destArr[0].trim();
						tempAgentWiseDestArray[currentSize][1] = destArr[0].trim();
						tempAgentWiseDestArray[currentSize][2] = destArr[0].trim() + ' - ' + destArr[1].trim();
						tempAgentWiseDestArray[currentSize][3] = 'N';
						tempAgentWiseDestArray[currentSize][4] = destArr[0].trim() + '_N';
					}				
				}				
			}
		
			if(isAllOriginAirports){
				tempAgentWiseDestArray.sort(UI_tabSearchFlights.customSort);
				for (var i=0;i<originArray.length;i++) {
					UI_tabSearchFlights.agentWiseOndDataMap[originArray[i][0]+'_N'] = tempAgentWiseDestArray;
				}
			} else{
				if (tempAgentWiseDestArray.length > 0) {				
					tempAgentWiseDestArray.sort(UI_tabSearchFlights.customSort);
					UI_tabSearchFlights.agentWiseOndDataMap[agentWiseOrigin+'_N'] = tempAgentWiseDestArray;
				}				
			}
		}
	}
	
	UI_tabSearchFlights.initDynamicOndList = function(){
		var defaultLanguage = DATA_ResPro.systemParams.defaultLanguage;
		
		for(var originIndex=0;originIndex<origins.length;originIndex++){			
			
			if(typeof(origins[originIndex]) != "undefined"){
				if(DATA_ResPro.initialParams.allowRouteSelectionForAgents){
					UI_tabSearchFlights.initAgentWiseOrigins(originIndex, true);					
				}
				
				if(allOnds != undefined) {
					if(typeof(allOnds[airports[originIndex]['code']]) != "undefined"){
						var dests = allOnds[airports[originIndex]['code']];
						var agentWiseDestinations = dests.split('|');									
					}
				}				
				
				var tempDestArray = [];
				var tempAgentWiseDestArray = [];
				for (var destIndex=0;destIndex<origins[originIndex].length;destIndex++) {
					// check whether operating carrier exists
					var curDest = origins[originIndex][destIndex];
				
					if(curDest[0] != null){					
						var currentSize = tempDestArray.length;
						var cityCode = 'N';
						if(airports[curDest[0]]['city']!=undefined && airports[curDest[0]]['city']!=null 
								&& (airports[curDest[0]]['city'] || airports[curDest[0]]['city']=="true")){
							cityCode = 'Y';
						}
						tempDestArray[currentSize] = [];
						tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
						tempDestArray[currentSize][1] = airports[curDest[0]][defaultLanguage];
						tempDestArray[currentSize][2] = airports[curDest[0]]['code'] + ' - ' + airports[curDest[0]]['en'];
						tempDestArray[currentSize][3] = cityCode ;
						tempDestArray[currentSize][4] = airports[curDest[0]]['code']+'_'+cityCode ;								
						if(isAllOriginDestAirports){
							tempAgentWiseDestArray = tempDestArray;
						} else {
							if(typeof(agentWiseDestinations) != "undefined"){
								if(dests.indexOf('***') > -1){
									isAllDestAirports = true;									
								}
								
								if(isAllDestAirports || isAllOriginDestAirports){
									tempAgentWiseDestArray = tempDestArray;
								} else {
									for (var i=0;i<agentWiseDestinations.length;i++) {
										var agentWiseCurDest = agentWiseDestinations[i];
										var destArr = agentWiseCurDest.split(',');
										if(destArr[0] != null && airports[curDest[0]]['code'] == destArr[0].trim()){					
											var currentSize = tempAgentWiseDestArray.length;
											tempAgentWiseDestArray[currentSize] = [];
											tempAgentWiseDestArray[currentSize][0] = airports[curDest[0]]['code'];
											tempAgentWiseDestArray[currentSize][1] = airports[curDest[0]][defaultLanguage];
											tempAgentWiseDestArray[currentSize][2] = airports[curDest[0]]['code'] + ' - ' + airports[curDest[0]]['en'];
											tempAgentWiseDestArray[currentSize][3] = cityCode ;
											tempAgentWiseDestArray[currentSize][4] = airports[curDest[0]]['code']+'_'+cityCode ;
										}				
									}								
								}
							}							
						}
					}				
				}
			
				var citySearch = 'N';
				if(airports[originIndex]['city']!=undefined && airports[originIndex]['city']!=null 
						&& (airports[originIndex]['city'] || airports[originIndex]['city']=="true")){
					citySearch = 'Y';
				}
				
				if (tempDestArray.length > 0) {				
					tempDestArray.sort(UI_tabSearchFlights.customSort);
					UI_tabSearchFlights.ondDataMap[airports[originIndex]['code']+'_'+citySearch] = tempDestArray;
				}
				if (tempAgentWiseDestArray.length > 0) {				
					tempAgentWiseDestArray.sort(UI_tabSearchFlights.customSort);
					UI_tabSearchFlights.agentWiseOndDataMap[airports[originIndex]['code']+'_'+citySearch] = tempAgentWiseDestArray;
				}
				if (originArray.length > 0) {				
					originArray.sort(UI_tabSearchFlights.customSort);
					UI_tabSearchFlights.agentWiseOrigins = originArray;	
				}
			}
		}
	};
	
	UI_tabSearchFlights.initExcludeChargesView = function(populateList){
		
		if(DATA_ResPro.initialParams.allowExcludeCharges){
			UI_tabSearchFlights.populateExcludeChargeList(populateList);
			
			$("#excludeChargePopUp").dialog({ 
				autoOpen: false,
				modal:true,
				height: 'auto',
				width: 'auto',
				title:'Exclude Charges'
			});
			
			$("#btnExcludeCharge").decorateButton();
			$("#btnExcludeCharge").click(function() { $("#excludeChargePopUp").dialog('open');});		
			
			$("#btnChargeExcludeConfirm").decorateButton();
			$("#btnChargeExcludeConfirm").click(function() { 
													$("#excludeChargePopUp").dialog('close');
													UI_commonSystem.pageOnChange();
													UI_tabSearchFlights.clearAll();
												});
			
			$("#btnChargeExcludeClear").decorateButton();
			$("#btnChargeExcludeClear").click(function() { 
													$('input[name=chkExcludeCharge]').attr('checked', false);
													UI_commonSystem.pageOnChange();
													UI_tabSearchFlights.clearAll();
												});
		}
		$("#iconStopOverClose").click(function() {
			$("#stopOverTimePopup").hide();
		});
		
	}
	
	UI_tabSearchFlights.togglePBRKDown = function(){
		$(".PBRKDown").toggle();
	};
	
	/**
	 * Custom sort to sort 2 dimensional on index 0
	 */
	
	UI_tabSearchFlights.customSort = function(a,b) {

		a = a[0];
		b = b[0];
		return a == b ? 0 : (a < b ? -1 : 1);

	};
		
	UI_tabSearchFlights.bookingTypeOnChange = function(){
		if($("#bookingType").val() == "STANDBY" || $("#bookingType").val() == "WAITLISTING"){
			$("#returnDate").disableEnable(false);
			$("#returnVariance").disableEnable(false);
			$("#openReturn").attr('checked', false);
			$("#validity").val('');
			$("#openReturn").disableEnable(true);
			$("#validity").disableEnable(true);
			$('#returnDate').datepicker().datepicker('enable');
		} else {
			$("#openReturn").disableEnable(false);			
		}
	};
	
	UI_tabSearchFlights.openReturnOnChange = function(){
		if($('#openReturn').attr('checked')){
			$('#selSearchOptions').val('AA'); 
			$('#selSearchOptions').disableEnable(true);
			$("#returnDate").val('');
			$("#returnDate").disableEnable(true);
			$("#returnVariance").val('');
			$("#returnVariance").disableEnable(true);
			$("#validity").disableEnable(false);
			$('#returnDate').datepicker().datepicker('disable');
		}else{
			if (UI_commonSystem.strPGMode != "transferSegment") {
				$('#selSearchOptions').val($("#selSearchOptions option:first"));
				$('#selSearchOptions').disableEnable(false);
				$("#returnDate").disableEnable(false);
				$("#returnVariance").disableEnable(false);
				$("#validity").disableEnable(true);
				$("#validity").val('');
				$('#returnDate').datepicker().datepicker('enable');
			}
		}
	};
	UI_tabSearchFlights.resetOpenReturnControls = function(blnStatus){
		blnStatus = !blnStatus;
		$("#departureVariance").disableEnable(blnStatus);
		$("#returnDate").disableEnable(blnStatus);
		$("#returnVariance").disableEnable(blnStatus);
		$("#validity").disableEnable(blnStatus);
		$("#selSearchOptions").disableEnable(blnStatus);
		
	};
	
	UI_tabSearchFlights.searchOptionOnChange = function()
	{
		var blnStatus = false;
		if($('#selSearchOptions').val() == 'AA'){
			blnStatus = true;
		}
		else{
			blnStatus =	false;
		}
		if(!blnStatus){
			$("#divValidity").hide();
			$("#validity").hide();
			$("#divOpen").hide();
			$("#openReturn").hide();
		}else if(DATA_ResPro.initialParams.openReturnEnabled && blnStatus){
			$("#divValidity").show();
			$("#validity").show();
			$("#divOpen").show();
			$("#openReturn").show();			
		}
		else{
			$("#divValidity").hide();
			$("#validity").hide();
			$("#divOpen").hide();
			$("#openReturn").hide();
		}
	};
	/*
	 * Disable Enable Page Controls
	 */
	UI_tabSearchFlights.disableEnablePageControls = function(blnStatus){
		if (UI_commonSystem.strPGMode == "transferSegment") {
			UI_tabSearchFlights.disablePageControlsTransferSegment(blnStatus);
	    } else	if (!UI_commonSystem.strPGMode == "" && UI_commonSystem.strPGMode != "addSegment" 
	    		&& UI_commonSystem.strPGMode != "modifySegment" && UI_commonSystem.strPGMode != "confirmOprt"){
			$("#departureVariance").disableEnable(blnStatus);
			$("#returnDate").disableEnable(blnStatus);
			$("#returnVariance").disableEnable(blnStatus);
			$("#validity").disableEnable(blnStatus);			
			$("#adultCount").disableEnable(blnStatus);
			$("#childCount").disableEnable(blnStatus);
			$("#infantCount").disableEnable(blnStatus);	
			$("#fAirport").disableEnable(blnStatus);
			$("#tAirport").disableEnable(blnStatus);			
			$("#classOfService").disableEnable(blnStatus);
			$("#selectedCurrency").disableEnable(blnStatus);
			$("#bookingType").disableEnable(blnStatus);	
			$("#selSearchOptions").disableEnable(blnStatus);
			$("#selFareTypeOptions").disableEnable(blnStatus);
			$("#selPaxTypeOptions").disableEnable(blnStatus);
			$("#selBookingClassCode").disableEnable(blnStatus);
		}
		
		if (UI_commonSystem.strPGMode == "addSegment" || UI_commonSystem.strPGMode == "modifySegment") {
			$("#adultCount").disableEnable(blnStatus);
			$("#childCount").disableEnable(blnStatus);
			$("#infantCount").disableEnable(blnStatus);
			// $("#classOfService").disableEnable(blnStatus);
		} 
		
		if(UI_commonSystem.strPGMode == "confirmOprt"){
			$("#returnDate").disableEnable(false);			
			$('#returnDate').datepicker().datepicker('enable');			
			$("#departureDate").disableEnable(true);
			$('#departureDate').datepicker().datepicker('disable');			
			$("#classOfService").disableEnable(blnStatus);
			$("#adultCount").disableEnable(blnStatus);
			$("#childCount").disableEnable(blnStatus);
			$("#infantCount").disableEnable(blnStatus);
			$("#selectedCurrency").disableEnable(blnStatus);
			$("#selBookingClassCode").disableEnable(blnStatus);			
			//$("#bookingType").disableEnable(blnStatus);				
		}
		
		if (UI_commonSystem.strPGMode == "confirmOprt" 
			|| UI_commonSystem.strPGMode == "transferSegment") {
			$('#openReturn').disableEnable(true);
		}
	};
	
	UI_tabSearchFlights.isAddBusSegment = function() {
		var isAddBus = false;
		if (DATA_ResPro.addGroundSegment && DATA_ResPro.addGroundSegment == "true") {
			isAddBus =  true;
		}
		return isAddBus;
	};
	
	
	/*
	 * Transfer Segment Own Airline functionality
	 */
	UI_tabSearchFlights.disablePageControlsTransferSegment = function(blnStatus) {		
		$("#adultCount").disableEnable(blnStatus);
		$("#childCount").disableEnable(blnStatus);
		$("#infantCount").disableEnable(blnStatus);			
		$("#returnDate").disableEnable(blnStatus);
		$("#classOfService").disableEnable(blnStatus);
		$('#returnDate').datepicker().datepicker('disable');
		$("#selSearchOptions").disableEnable(blnStatus);		
		
		if(blnStatus){
			$('#bookingType').empty();
			//Removing wait listing option from transfer flow 
			var transferBookingTypeArray = new Array();
			for(var ind=0;ind<top.arrBookingTypes.length;ind++){
				if(top.arrBookingTypes[ind][0] != 'WAITLISTING'){
					transferBookingTypeArray[ind] = top.arrBookingTypes[ind];
				}
			}
			$("#bookingType").fillDropDown( { dataArray:transferBookingTypeArray , keyIndex:0 , valueIndex:1 });
			$("#bookingType").val("NORMAL");
		}
		
		if (UI_commonSystem.strPGMode == "transferSegment" && (DATA_ResPro.transferSegment.groupPNR == null || DATA_ResPro.transferSegment.groupPNR == "" )) {
				UI_tabSearchFlights.fromCombo.enable();
				UI_tabSearchFlights.toCombo.enable();			
		} else {	
				if (blnStatus) {
						// even if this is a lcc reservation we need to
						// distinguish between a dry reservation
						// and an interline reservation. This is because we need
						// to enable OnD selection for a dry reservation.
						if (!DATA_ResPro.transferSegment.isInterline){
							UI_tabSearchFlights.fromCombo.enable();
							UI_tabSearchFlights.toCombo.enable();
						} else {
							UI_tabSearchFlights.fromCombo.disable();
							UI_tabSearchFlights.toCombo.disable();
						}
				} else {
					UI_tabSearchFlights.fromCombo.enable();
					UI_tabSearchFlights.toCombo.enable();	
				}
		}
	};

	/**
	 * Checks whether the loaded reservation is a single carrier reservation We
	 * need this for dry transfer segment because for dry transfer segments we
	 * need to enable selection of OnD. But for interline this is not allowed to
	 * change.
	 */
	/** Fail with bus segment * */
	UI_tabSearchFlights.isSingleCarrierReservation = function (){
		var isSingleCarrierReservation = true; 
		if (DATA_ResPro.resSegments !=null){
			var reservationSegments = DATA_ResPro.resSegments;
			var carrierCodeArray = new Array();
			for (var i=0; i<reservationSegments.length ; i++) {
  				var segment = reservationSegments[i];	
				if ($.inArray(segment.carrierCode, carrierCodeArray) ==-1){
					carrierCodeArray.push(segment.carrierCode);			
				}			
			}

			if (carrierCodeArray.length > 1){
				isSingleCarrierReservation = false;
			}			
		}
		return isSingleCarrierReservation;
	};
	
	UI_tabSearchFlights.getResSegment = function(pnrSegId){
		var resSegment = true; 
		if (DATA_ResPro.resSegments !=null){
			var reservationSegments = DATA_ResPro.resSegments;
			for (var i=0; i<reservationSegments.length ; i++) {
  				if(reservationSegments[i].bookingFlightSegmentRefNumber == pnrSegId){
  					return reservationSegments[i];
  				}
						
			}		
		}
		return null;
	}

	/*
	 * Disable page controls
	 */
	UI_tabSearchFlights.disablePageControls = function(){
		if (!UI_commonSystem.strPGMode == ""){
			UI_tabSearchFlights.disableEnablePageControls(true);
		}
	};
	
	/*
	 * Submit form on enter key press
	 */
	
	UI_tabSearchFlights.checkEnterAndSubmit = function(keyEvent){

          if(keyEvent.which == 13){

              UI_tabSearchFlights.searchOnClick();
          }
     };

	/*
	 * Search Flights
	 */
	UI_tabSearchFlights.searchOnClick = function() {

		$("#btnSearch").blur();
		$("#departureDate").blur();
        $("#returnDate").blur();
		UI_tabSearchFlights.originDestinationOnBlur();
		UI_tabSearchFlights.releaseBlockSeats();
		UI_tabSearchFlights.updateTravelAgentCode();
		UI_tabSearchFlights.disableEnablePageControls(false);
		// Handling for transfer segment
		if (UI_commonSystem.strPGMode != "transferSegment") {			
			UI_tabAnci.resetAnciData();
			UI_tabPassenger.resetData();			
		}
		var isValidate = UI_tabSearchFlights.validateSearchFlight();
		if (isValidate) {
			//reset the flag in new search
			UI_tabSearchFlights.waitListedSegmentAvailable = false;
			UI_tabSearchFlights.ignoreWaitListing = false;
			if(UI_tabSearchFlights.isRequote()){
				$("#frmSrchFlt").action("flightSearch.action");
			} else {
				$("#frmSrchFlt").action("availabilitySearch.action");
			}
			var data = {}; // $("#frmMakeBkg").serializeArray();
			data['modifyBooking'] = false;
			if($('#selSearchOptions').attr('disabled')){
				data['searchParams.searchSystem'] = $('#selSearchOptions').val();
			}
			
			// set params on segment modification
			if($('#fromAirport').attr('disabled')){				
				data['searchParams.fromAirport'] = $('#fromAirport').val();
			}
			if($('#toAirport').attr('disabled')){				
				data['searchParams.toAirport'] = $('#toAirport').val();
			}
			if($('#departureDate').attr('disabled')){
				data['searchParams.departureDate'] = $('#departureDate').val();				
			}
			if($('#returnDate').attr('disabled')){
				data['searchParams.returnDate'] = $('#returnDate').val();				
			}						
			if($('#returnVariance').attr('disabled')){
				data['searchParams.returnVariance'] = $('#returnVariance').val();				
			}
			if($('#departureVariance').attr('disabled')){
				data['searchParams.departureVariance'] = $('#departureVariance').val();				
			}			
			
			if($('#bookingType').attr('disabled')){
				data['searchParams.bookingType'] = $('#bookingType').val();				
			}
			
			if (UI_commonSystem.strPGMode == "addSegment") {
				data['addSegment'] = true;				
			}
			if (UI_commonSystem.strPGMode == "confirmOprt") {
				data['searchParams.openReturnConfirm'] = true;
			}
			
			// If modifying segment(s) already has flexi, skip flexi quoting
			if(DATA_ResPro["modifyFlexiBooking"] != undefined &&
					DATA_ResPro["modifyFlexiBooking"]){
				var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);
				$.each(ondFlexiQuote, function(key, value){
					ondFlexiQuote[key] = false;
				});
				data['searchParams.ondQuoteFlexiStr'] = $.toJSON(ondFlexiQuote);
			} else {
				var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);
				$.each(ondFlexiQuote, function(key, value){
					ondFlexiQuote[key] = true;
				});
				data['searchParams.ondQuoteFlexiStr'] = $.toJSON(ondFlexiQuote);
			}
			
			// Set cabin class & logical cabin class based on selection
			var cos = $("#classOfService").val();
			var cosArr = cos.split("-");
			
			// If user selects logical cabin class, value patter will be
			// 'XXX-XXX' (logical_cc-cabin_class)
			if(cosArr.length > 1){
				data['searchParams.logicalCabinClass'] = cosArr[0];
				data['searchParams.classOfService'] = cosArr[1];
			} else if(cosArr.length == 1) {
				data['searchParams.classOfService'] = cosArr[0];
				data['searchParams.logicalCabinClass'] = '';
			}
			
			data['fareDiscountPercentage'] = UI_tabSearchFlights.fareDiscount.percentage;
			data['fareDiscountNotes'] = UI_tabSearchFlights.fareDiscount.notes;		
			data['fareDiscountCode'] = UI_tabSearchFlights.fareDiscount.code;	
			
			data['searchParams.ticketValidityMax'] = $('#ticketExpiryDate').val();				
			data['searchParams.ticketValidityMin'] = $('#ticketValidFrom').val();				
			data['searchParams.ticketValidityExist'] = $('#ticketExpiryEnabled').val();
			data['searchParams.hubTimeDetailJSON'] = JSON.stringify(UI_tabSearchFlights.hubTimeDetailJSON, null);			
			data['searchParams.ondWiseCitySearchStr'] = $.toJSON(UI_tabSearchFlights.ondWiseCitySearch);			
			
			UI_tabSearchFlights.setExcludeChargesSearchParams(data);
			
			if (UI_commonSystem.strPGMode == "addSegment" || UI_commonSystem.strPGMode == "transferSegment") {
				UI_tabSearchFlights.createSubmitData({data:data, mode:UI_commonSystem.strPGMode});							
			} else 	if (UI_commonSystem.strPGMode != "") {
				UI_tabSearchFlights.createSubmitData({data:data, mode:UI_commonSystem.strPGMode});
			}
			var select = document.getElementById('selPaxTypeOptions');
			UI_tabSearchFlights.strPaxType = select.options[select.selectedIndex].value;// $("#selPaxTypeOptions").val();
			
			// Set parameter to check whether logical cc availability grid
			// should rebuild or not
			$.each(UI_tabSearchFlights.ondRebuildLogicalCCAvailability, function(key, value){
				UI_tabSearchFlights.ondRebuildLogicalCCAvailability[key] = true;
			});
						
			if(UI_tabSearchFlights.isRequote()){
				$("#frmSrchFlt").action("flightSearch.action");
			}
						
			UI_tabSearchFlights.clearFareQuote();
			
			$("#frmSrchFlt").ajaxSubmit({ dataType: 'json',	data:data,										
											success: UI_tabSearchFlights.returnCallSearchFlight,
											error:UI_commonSystem.setErrorStatus});
			
		} else {			
			if(UI_commonSystem.strPGMode != "") {
				UI_tabSearchFlights.disableEnablePageControls(true);
			}	
		}		
		UI_tabSearchFlights.hideAirportMessage();		
		UI_tabSearchFlights.ondWiseFlexiSelected[0] =  false;
		UI_tabSearchFlights.ondWiseFlexiSelected[1] =  false;
		return false;
	};
	
	/**
	 * Add exclude charges info to search params
	 * 
	 * */
	UI_tabSearchFlights.setExcludeChargesSearchParams = function(data){
		
		if(DATA_ResPro.initialParams.allowExcludeCharges){
			//Set Excluding charges
			var checkedExcludedChargeArr = $("#tblExclChg input:checked.clsExclChg");
			if(checkedExcludedChargeArr.length > 0){
				for(var i=0;i<checkedExcludedChargeArr.length;i++){
					data['searchParams.excludeCharge[' + i + ']'] = checkedExcludedChargeArr[i].value;
				}
			}
		}
	}
	
	/*
	 * Apply fare discount
	 */
	UI_tabSearchFlights.applyFareDiscount = function(flg){
		// todo do the validation and recalculate the fare totals
		UI_commonSystem.showProgress();
		var perc = parseInt($.trim($('#fareDiscountPercentage').val()), 10);
		var fareDiscNotes = $.trim($('#fareDiscountNotes').val());
		var fareDiscCode = "";
		if (showFareDiscountCodes) {
			fareDiscCode = $('#selDiscountType').val();
		}
		validateDiscount = function(){
			var boolvalid = false;
			if (flg == 'remove'){
				perc = 0;
				fareDiscNotes = '';
				boolvalid = true;
			}else if ((perc >= UI_tabSearchFlights.jsonFareDiscountInfo.fareDiscountMin && 
							perc <= UI_tabSearchFlights.jsonFareDiscountInfo.fareDiscountMax) 
							|| (top.arrPrivi[PRIVI_OVERRIDE_FARE_DISCOUNT] == 1 && perc >=0 && perc <= 100)){
				
				if (fareDiscNotes == ''){
					showCommonError("ERROR",'Fare discount notes cannot be empty');
					UI_commonSystem.hideProgress();
				} else if (showFareDiscountCodes && fareDiscCode == '') {
					showCommonError("ERROR",'Fare discount code cannot be empty');
					UI_commonSystem.hideProgress();
				}else{
					boolvalid = true;
				}
			}else{
				showCommonError("ERROR",'Invalid fare discount percentage');
				UI_commonSystem.hideProgress();
			}
			return boolvalid;
		};
		
		if( validateDiscount() ){
			// if (fareDiscNotes == ''){
			// showCommonError("ERROR",'Fare disc notes cannot be empty');
			// UI_commonSystem.hideProgress();
			// return;
			// }
			UI_tabSearchFlights.fareDiscount.percentage = perc;
			UI_tabSearchFlights.fareDiscount.notes = fareDiscNotes;
			UI_tabSearchFlights.fareDiscount.code = fareDiscCode;
			UI_tabSearchFlights.disableEnablePageControls(false);
			var data = {};
			if(UI_tabSearchFlights.isRequote()){
				data = UI_requote.fareDiscountSubmitData();
			}else if(UI_tabSearchFlights.isMulticity()){
				data = UI_Multicity.fqParams;
			}else {
				data = UI_tabSearchFlights.createFareQuoteParams();
			}
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			data['fareDiscountPercentage'] = perc;
			data['fareDiscountNotes'] = fareDiscNotes;
			data['fareDiscountCode'] = fareDiscCode;
			UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', async:false, data: data , url:"calculateFareDiscount.action",
												success:function(response){
													if(response.success){														
														UI_tabSearchFlights.availableFare = response.selectedFare;
														UI_tabSearchFlights.isDomFareDiscountApplied = response.domFareDiscountApplied;
														UI_tabSearchFlights.appliedFareDiscountInfo = response.appliedFareDiscount;
														UI_tabSearchFlights.fillFareQuoteData(UI_tabSearchFlights.availableFare);
														if(response.overrideFareDiscount != null){
															UI_tabSearchFlights.overrideFareDiscount = response.overrideFareDiscount;
														}
														$("#lblDiscountApplied").text(perc);
														if (flg != 'remove'){
															$("#btnDiscountPop").text("Change");
															if(UI_tabSearchFlights.overrideFareDiscount == true){
																$("#btnFareDiscountRemove").show();	
															}															
														}else{
															UI_tabSearchFlights.clearPopulatedFareDisc();
														}
													} else {
														showCommonError("ERROR", response.messageTxt);
													}
													
													UI_tabSearchFlights.closeDiscuntPop();
													UI_commonSystem.hideProgress();
												},
												error:UI_commonSystem.setErrorStatus});
			UI_commonSystem.hideProgress();
		} 
		// else {
		// showCommonError("ERROR",'Invalid fare percentage');
		// UI_commonSystem.hideProgress();
		// return;
		// }
	};
	
	UI_tabSearchFlights.clearPopulatedFareDisc = function(){
		$('#fareDiscountPercentage').val('');
		$('#fareDiscountNotes').val('');
		$('#selDiscountType').val('');
		$('#lblDiscountApplied').text(0);
		$("#btnDiscountPop").text("Apply");
		$("#btnFareDiscountRemove").hide();
	};
	
	UI_tabSearchFlights.getFareDiscount = function(){
		return UI_tabSearchFlights.fareDiscount;
	};
	
	
	/*
	 * Reset Button On Click
	 */
	UI_tabSearchFlights.resetOnClick = function() {
		$("#trCheckStopOver").hide();
		$("#btnReset").blur();
		UI_tabSearchFlights.releaseBlockSeats();
		UI_tabSearchFlights.pgObj.lockTabs();
		$("#oldPerPaxFare").val('0');
		$("#oldPerPaxFareId").val('0');
		if (UI_commonSystem.strPGMode == "transferSegment") {
			UI_TransferSegment.initialization();
		} else if (UI_tabSearchFlights.isRequote()){
			UI_requote.reset();
			UI_tabSearchFlights.fillRequote(DATA_ResPro.reservationInfo);
		} else if (UI_commonSystem.strPGMode == "" || 
				UI_commonSystem.strPGMode == 'addSegment') { 
			UI_tabSearchFlights.initialization();
		} else {
			UI_tabSearchFlights.fillModifySegment(DATA_ResPro.modifySegments);
			$("#divResultsPane").hide();
			$("#divTaxBD").hide();
			$("#divSurBD").hide();
			$("#divBreakDownPane").hide();
		} 
		UI_tabSearchFlights.hideAirportMessage();
		isLogicalCCSelected = false;
		
		UI_tabSearchFlights.resetBundledServiceSelection();
		UI_tabSearchFlights.resetFlexiSelection();
		UI_tabSearchFlights.isOpenReturn = false;
		$("#openReturn").attr("checked", false);
	};
	
	UI_tabSearchFlights.resetBundledServiceSelection = function(){
		$.each(UI_tabSearchFlights.ondWiseBundleFareSelected, function(key, value){
			UI_tabSearchFlights.ondWiseBundleFareSelected[key] = null;
		});
	}
	
	UI_tabSearchFlights.resetFlexiSelection = function(){
		$.each(UI_tabSearchFlights.ondWiseFlexiSelected, function(key, value){
			UI_tabSearchFlights.ondWiseFlexiSelected[key] = false;
		});
	}
	
	/*
	 * Cancel Button On Click
	 */
	UI_tabSearchFlights.cancelOnClick = function() {
		$("#btnCancel").blur();
		var data = {};
		if(UI_tabSearchFlights.isRequote()){
		 	UI_modifyResRequote.loadHomePage();
		} else if (UI_commonSystem.strPGMode == ""){
			UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, url:"cleanUp!cleanUpSession.action",				
				success:top.LoadHome,error:UI_commonSystem.setErrorStatus});
	 	} else if (UI_commonSystem.strPGMode == "transferSegment") {
	 		UI_TransferSegment.loadHomePage();
	 	} else if (UI_commonSystem.strPGMode == "addSegment") {
	 		UI_addSegment.loadHomePage();
	 	} else {
	 		UI_modifySegment.loadHomePage();
	 	}
	};
	
	// -------------------------------- Book continue
	// ---------------------------------- //
	/*
	 * Make Booking On Click - retrieve data if its required
	 */
	UI_tabSearchFlights.bookOnClick = function(){
		$("#btnBook").blur();
		UI_commonSystem.showProgress();
		if(UI_tabSearchFlights.isRequote()){
    		if(!UI_requote.validateFareQuote()){
    			showERRMessage("Invalid ond fare quote");
    			UI_commonSystem.hideProgress();
    			return false;
    		} else {
    			UI_requote.showExchangedSegmentAlertForbundleChange();
    		}
		}else if (UI_tabSearchFlights.isMulticity()){
			if(!UI_Multicity.validateFareQuote()){ 
    			showERRMessage("Invalid ond fare quote");
    			UI_commonSystem.hideProgress();
    			return false;
    			}
			UI_tabSearchFlights.strPaxType = UI_Multicity.fqParams['fareQuoteParams.paxType'];
		}else if (UI_tabSearchFlights.isCalenderMinimumFare()){
			// skip all
		}else {
			/*
			 * This will required only when temporary booking is applicable if
			 * (UI_tabSearchFlights.pgObj.tabDataRetrive.tab1){
			 * $("#frmTabLoad").action("passengerTab.action?" +
			 * UI_tabSearchFlights.createBookParams());
			 * $("#frmTabLoad").ajaxSubmit({ dataType: 'json', success:
			 * UI_tabSearchFlights.bookOnClickSuccess}); }else{
			 * UI_tabSearchFlights.bookOnClickSuccess() }
			 */
			var intOutFlt  = $("#tblOutboundFlights").getGridParam('selrow') - 1;
			// Validation and call searchOnClick
			if (UI_commonSystem.strPGMode == "transferSegment" && jsonOutFlts == null) {
				UI_tabSearchFlights.searchOnClick();
				UI_commonSystem.hideProgress();
				return false;
			}
			// jsonOutFlts == null for transferSegment
			if(UI_commonSystem.strPGMode !="confirmOprt"){
				if (jsonOutFlts == null || jsonOutFlts[intOutFlt] == null || jsonOutFlts[intOutFlt].length == 0){
					showERRMessage(raiseError("XBE-ERR-06","transfer"));
					UI_commonSystem.hideProgress();
					return false;
				}
			} else if(UI_commonSystem.strPGMode =="confirmOprt") {
				if ( intOutFlt < 0 ){
					showERRMessage(raiseError("XBE-ERR-06","return"));
					UI_commonSystem.hideProgress();
					return false;
				}
				// loading the fare synchronously...(as no fare quote will happen in
				// next/prev actions
				if(jsonFareQuote == null){
					UI_tabSearchFlights.loadFareQuoteOnClick({async:false});
				}
				if(jsonFareQuote == null){
					UI_commonSystem.hideProgress();
					return false;
				}
			}
		
			if (UI_commonSystem.strPGMode == "transferSegment"){
				if ( intOutFlt < 0 ){
					showERRMessage(raiseError("XBE-ERR-06","transfer"));
					UI_commonSystem.hideProgress();
					return false;
				}
			
				if(jsonFareQuote == null){
					UI_tabSearchFlights.loadFareQuoteOnClick({async:false});
				}
				if(jsonFareQuote == null){
					UI_commonSystem.hideProgress();
					return false;
				}
			}
		
			var intRetFlt  = null;
			var lastDepartureDate;
			var firstReturndate;
			var lastDepartureTime;
			var firstReturnTime;	
		
		
			if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt"){
				intRetFlt  = $("#tblInboundFlights").getGridParam('selrow') - 1;			
			
				if (jsonRetFlts[intRetFlt].length == 0){
					showERRMessage(raiseError("XBE-ERR-06","return"));
					UI_commonSystem.hideProgress();
					return false;
				}
				var departure = jsonOutFlts[intOutFlt].arrivalTimeList;
				lastDepartureDate = departure[departure.length -1];
			
			
				var arrival = jsonRetFlts[intRetFlt].depatureTimeList;
				firstReturndate = arrival[0];
			
				// alert(strArrDate + " | " + strArrTime + "\n" + strRDepDate + " |
				// " + strRDepTime)
				if (Number(lastDepartureDate)> Number(firstReturndate)){
					showERRMessage(raiseError("XBE-ERR-27"));
					UI_commonSystem.hideProgress();
					return false;
				}			
			}		
		}
		
		if(UI_tabSearchFlights.showFareRuleAlert && UI_commonSystem.strPGMode != "transferSegment" ){
			if(!confirm('Are you sure you have selected the correct flight connections? \nPlease make sure that the Passenger is notified of the Fare rules and conditions applied.')){
				UI_commonSystem.hideProgress();
				return false;
			}
		}
		
		
		// this is usability hack, show the progress bar and have very little
		// delay.
		// removed the time out put a method to delay
		
		if(UI_commonSystem.strPGMode != "transferSegment"){
			UI_tabAnci.resetAnciData();
		}
		UI_tabSearchFlights.bookOnClickSuccess();
	};
	
	UI_tabSearchFlights.releaseBlockSeats = function(action) {
		if (DATA_ResPro.initialParams.seatBlock) {
			var data = {};
			data['system'] = UI_tabSearchFlights.lastBlockSeatSystem;
			UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data,async:false, url:"cleanUp!releaseBlockSeat.action",error:UI_commonSystem.setErrorStatus});
		}	
	};
	
	/*
	 * Create Navigate params
	 */
	UI_tabSearchFlights.createBookParams = function(){
		var strSrchParams = "";
		var strJSDTO = "bookParams";
		strSrchParams += strJSDTO + ".fromAirport=" + UI_tabSearchFlights.objResSrchParams.fromAirport;
		strSrchParams += "&" + strJSDTO + ".toAirport=" + UI_tabSearchFlights.objResSrchParams.toAirport;
		strSrchParams += "&" + strJSDTO + ".departureDate=" + UI_tabSearchFlights.strOutDate;
		strSrchParams += "&" + strJSDTO + ".returnDate=" + UI_tabSearchFlights.strRetDate;
		strSrchParams += "&" + strJSDTO + ".validity=" + UI_tabSearchFlights.objResSrchParams.validity;
		strSrchParams += "&" + strJSDTO + ".adultCount=" + UI_tabSearchFlights.objResSrchParams.adultCount;
		strSrchParams += "&" + strJSDTO + ".childCount=" + UI_tabSearchFlights.objResSrchParams.childCount;
		strSrchParams += "&" + strJSDTO + ".infantCount=" + UI_tabSearchFlights.objResSrchParams.infantCount;
		strSrchParams += "&" + strJSDTO + ".classOfService=" + UI_tabSearchFlights.objResSrchParams.classOfService;
		strSrchParams += "&" + strJSDTO + ".logicalCabinClass=" + UI_tabSearchFlights.objResSrchParams.logicalCabinClass;
		strSrchParams += "&" + strJSDTO + ".selectedCurrency=" + UI_tabSearchFlights.objResSrchParams.selectedCurrency;
		strSrchParams += "&" + strJSDTO + ".bookingType=" + UI_tabSearchFlights.objResSrchParams.bookingType;
		strSrchParams += "&" + strJSDTO + ".searchSystem=" + UI_tabSearchFlights.objResSrchParams.searchSystem;
		strSrchParams += "&" + strJSDTO + ".fromAirportSubStation=" + UI_tabSearchFlights.objResSrchParams.fromAirportSubStation;
		strSrchParams += "&" + strJSDTO + ".toAirportSubStation=" + UI_tabSearchFlights.objResSrchParams.toAirportSubStation;
		strSrchParams += "&" + strJSDTO + ".openReturnConfirm=" + UI_tabSearchFlights.objResSrchParams.openReturnConfirm;
		strSrchParams += "&" + strJSDTO + ".travelAgentCode=" + UI_tabSearchFlights.objResSrchParams.travelAgentCode==null ? '' : UI_tabSearchFlights.objResSrchParams.travelAgentCode;
		strSrchParams += "&" + strJSDTO + ".bookingClassCode=" + UI_tabSearchFlights.objResSrchParams.bookingClassCode==null ? '' : UI_tabSearchFlights.objResSrchParams.bookingClassCode;
		strSrchParams += "&" + strJSDTO + ".promoCode=" + UI_tabSearchFlights.objResSrchParams.promoCode;
		strSrchParams += "&" + strJSDTO + ".bankIdentificationNo=" + UI_tabSearchFlights.objResSrchParams.bankIdentificationNo;
		return strSrchParams;
	};
	
	/*
	 * return the selected system assumption: no cross system selection is
	 * possible
	 */
	UI_tabSearchFlights.getSelectedSystem = function() {
		if(UI_tabSearchFlights.isRequote()){
			if (DATA_ResPro.reservationInfo.groupPNR != "" && DATA_ResPro.reservationInfo.groupPNR != null){
				system = 'INT';
			} else {
				if (jsonOutFlts !== undefined && jsonOutFlts != null && jsonOutFlts.length > 0) {
					var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
					system = jsonOutFlts[intOutFlt].system;
				} else {
					system = UI_tabSearchFlights.objResSrchParams.searchSystem;
				}
			}
		}else if (UI_tabSearchFlights.isMulticity()){
		    system = UI_tabSearchFlights.objResSrchParams.searchSystem;
		} else if (UI_tabSearchFlights.isCalenderMinimumFare()){
		    system = "AA";
		} else {
    		var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
    		var system = jsonOutFlts[intOutFlt].system;
    		if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt") {
    			var intRetFlt = $("#tblInboundFlights").getGridParam('selrow') - 1;
    			var retSystem = jsonRetFlts[intRetFlt].system;
    			if(retSystem != system){
    				system = 'INT';
    			}			
    		}
		}
		
		return system;
	};
	
	UI_tabSearchFlights.getFlightRPHList = function(includeOpenReturn) {
		var rphArray = [];

		var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
		var outFlt = jsonOutFlts[intOutFlt];
		// set the flight ref numbers
		for ( var i = 0; i < outFlt.flightRPHList.length; i++) {
			var outSegWaitListed = false;
			if(!UI_tabSearchFlights.ignoreWaitListing && outFlt.waitListedList[i]){
				outSegWaitListed = true;
			}
			rphArray[rphArray.length] = {
				flightRPH : outFlt.flightRPHList[i],
				segmentCode : outFlt.segmentCodeList[i],
				flightNo : outFlt.flightNoList[i],
				arrivalTime : outFlt.arrivalTimeList[i],
				departureTime : outFlt.depatureTimeList[i],
				carrierCode : outFlt.carrierList[i],
				departureTimeZulu : outFlt.departureTimeZuluList[i],
				arrivalTimeZulu : outFlt.arrivalTimeZuluList[i],
				returnFlag : outFlt.returnFlag,
				domesticFlight : outFlt.domesticFlightList[i],
				ondSequence: UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND,
				waitListed:outSegWaitListed
			};
		}

		if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt") {
			var intRetFlt = $("#tblInboundFlights").getGridParam('selrow') - 1;
			var inFlt = jsonRetFlts[intRetFlt];
			for ( var i = 0; i < inFlt.flightRPHList.length; i++) {
				var inSegWaitListed = false;
				if(!UI_tabSearchFlights.ignoreWaitListing && inFlt.waitListedList[i]){
					inSegWaitListed = true;
				}
				rphArray[rphArray.length] = {
					flightRPH : inFlt.flightRPHList[i],
					segmentCode : inFlt.segmentCodeList[i],
					flightNo : inFlt.flightNoList[i],
					arrivalTime : inFlt.arrivalTimeList[i],
					departureTime : inFlt.depatureTimeList[i],
					carrierCode : inFlt.carrierList[i],
					departureTimeZulu : inFlt.departureTimeZuluList[i],
					arrivalTimeZulu : inFlt.arrivalTimeZuluList[i],
					returnFlag : inFlt.returnFlag,
					domesticFlight : outFlt.domesticFlightList[i],
					ondSequence: UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND
				};
			}
		}
		
		if(includeOpenReturn && UI_tabSearchFlights.isOpenReturn && UI_tabSearchFlights.openReturnInfo != null){
			var dummyOpenRTRphArray = [];
			for ( var i = 0; i < rphArray.length; i++) {
				var obj = rphArray[i];
				var obSegCode = obj.segmentCode;
				//Reverse segment code
				var elements = obSegCode.split("/");
				var reverseSeg = "";

				var count = elements.length - 1;
				while (count >= 0) {
					if (reverseSeg == "") {
						reverseSeg = elements[count];
					} else {
						reverseSeg = reverseSeg + "/" + elements[count];
					}
					count--;
				}
				
				dummyOpenRTRphArray[i] = {
					flightRPH : '',
					segmentCode : reverseSeg,
					flightNo : '',
					arrivalTime : '',
					departureTime : '',
					carrierCode : '',
					departureTimeZulu : '',
					arrivalTimeZulu : '',
					returnFlag : true,
					domesticFlight : '',
					ondSequence: UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND
				}
			}
			
			for ( var j = 0; j < dummyOpenRTRphArray.length; j++) {				
				rphArray[rphArray.length] = dummyOpenRTRphArray[j];
			}
		}
		
		UI_tabSearchFlights.fltRPHInfo = $.toJSON(rphArray);
		return rphArray;
	};
	
	UI_tabSearchFlights.isRequote = function(){
		if((UI_commonSystem.strPGMode == "addSegment" || UI_commonSystem.strPGMode == "modifySegment" || UI_commonSystem.strPGMode == "cancelSegment") 
				&&  DATA_ResPro.initialParams.requoteEnabled
				){
			return true;
		}
		return false;
	};
	
	UI_tabSearchFlights.isMulticity = function(){
		return UI_tabSearchFlights.multicity;
	};
	
	UI_tabSearchFlights.isCalenderMinimumFare = function(){
		return UI_tabSearchFlights.calenderMinimumFare;
	};
	
	/*
	 * Block seat on Success,
	 */
	UI_tabSearchFlights.bookOnClickNext = function(){
		if (UI_tabSearchFlights.isRequote()){
			if(UI_requote.skipFareReQuote){
				UI_requote.initialize();
				UI_requote.balanceQuery();
		    } else {
				UI_commonSystem.hideProgress();
				UI_requote.ready();
		    }
		} else if (UI_tabSearchFlights.isMulticity()){
			UI_tabPassenger.ready();
			UI_tabSearchFlights.pgObj.tabDataRetrive.tab1 = false;
			UI_tabSearchFlights.pgObj.tabStatus.tab1 = true;
			UI_tabSearchFlights.pgObj.openTab(1);
			UI_commonSystem.hideProgress();
    	} else {
			if (UI_commonSystem.strPGMode == ""){
				UI_tabPassenger.ready();
				UI_tabSearchFlights.pgObj.tabDataRetrive.tab1 = false;
				UI_tabSearchFlights.pgObj.tabStatus.tab1 = true;
				UI_tabSearchFlights.pgObj.openTab(1);
				UI_commonSystem.hideProgress();
			} else if (UI_commonSystem.strPGMode == "transferSegment") {
				UI_TransferSegment.saveTransferSegemnt();			
		    }else if(UI_commonSystem.strPGMode == "confirmOprt"){
		    	UI_modifySegment.confirmOpenReturn();
		    }else if (UI_commonSystem.strPGMode == "addSegment") {
		    	
		    	UI_addSegment.selCurrency = $("#selectedCurrency").val();
		    	UI_addSegment.resPaxInfo = DATA_ResPro.resPaxs;
		    	UI_addSegment.continueToAnci();	    	
		    }  else {
		    	UI_modifySegment.selCurrency = $("#selectedCurrency").val();
		    	UI_modifySegment.resPaxInfo = DATA_ResPro.resPaxs;
		    	UI_modifySegment.bookOnClickSuccess();	
				// UI_modifySegment.showConfirmUpdate($("#selectedCurrency").val());
				// UI_commonSystem.hideProgress();
			}
    	}
	};
	
	/*
	 * Block seat
	 */
	UI_tabSearchFlights.blockSeat = function() {
		if (UI_tabSearchFlights.isMulticity()){
			data = UI_Multicity.fqParams;
			data['requoteOverbook'] = UI_Multicity.hasOverbookOnd();
			data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();
			UI_tabSearchFlights.lastBlockSeatSystem = UI_tabSearchFlights.getSelectedSystem();
		}else if(UI_tabSearchFlights.isCalenderMinimumFare()){
			data = UI_MinimumFare.createFareQuoteParams();
		}else{
			data = UI_tabSearchFlights.createFareQuoteParams();
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
			data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();
			data['operation'] = UI_tabSearchFlights.getOperationType();
			data['pnr'] = UI_tabSearchFlights.PNR;
			UI_tabSearchFlights.lastBlockSeatSystem = UI_tabSearchFlights.getSelectedSystem();
			if (UI_commonSystem.strPGMode == "addSegment" || UI_tabSearchFlights.isRequote()) {
				data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;				
			} else if (UI_commonSystem.strPGMode == "transferSegment") {
				data['groupPNR'] = DATA_ResPro.transferSegment.groupPNR;
			} else 	if (UI_commonSystem.strPGMode != "") {
				data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
			}
			data['requote'] = UI_tabSearchFlights.isRequote();
			if(UI_tabSearchFlights.isRequote()){
				data['requoteOverbook'] = UI_requote.hasOverbookOnd();
			}
		}
		data['allowBlockSeats'] = DATA_ResPro.initialParams.seatBlock;
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data,async:false, url:"blockSeat.action",							
			success: UI_tabSearchFlights.blockSeatOnClickSuccess,error:UI_commonSystem.setErrorStatus});	
	};
	
	UI_tabSearchFlights.blockSeatOnClickSuccess = function(response) {
		if(response.success){
			if(typeof(UI_tabPassenger) != 'undefined'){				
				UI_tabPassenger.allowBufferTimeOnHold = response.allowBufferTimeOnHold;
			}
			UI_tabSearchFlights.bookOnClickNext();
		}else{			
			UI_tabSearchFlights.lastBlockSeatSystem = null;
			showERRMessage(response.messageTxt);
			UI_commonSystem.hideProgress();
			$("#btnBook").disable();
		}
	};
	/*
	 * Make Booking On Click success
	 */
	UI_tabSearchFlights.bookOnClickSuccess = function() {
		
		// block seats before going to the nest stage if the user have the
		// privilege to early block seat
		if (UI_commonSystem.strPGMode != "transferSegment" && (!UI_tabSearchFlights.isRequote() || (UI_tabSearchFlights.isRequote() && !UI_requote.skipFareReQuote)) && 
				(DATA_ResPro.initialParams.seatBlock || UI_commonSystem.strPGMode == "confirmOprt")) {
			UI_tabSearchFlights.blockSeat();
		}else{ // forwarding to next stage without block seat is done
			UI_tabSearchFlights.bookOnClickNext();
		}	
	};
	// todo remove this method
	UI_tabSearchFlights.loadPhones = function(response) {		
// UI_tabSearchFlights.pgObj.tabDataRetrive.tab1 = false;
// UI_tabSearchFlights.pgObj.tabStatus.tab1 = true;
// UI_tabSearchFlights.pgObj.openTab(1);
// if(response != null && response.success){
// phoneObj = response.phoneObj;
// }
// UI_tabPassenger.updatePassengerTabTOForPaxDetails(response);
// UI_commonSystem.hideProgress();
//		
	};
	// -------------------------------- end Book continue
	// ---------------------------------- //
	
	/*
	 * Search Flight Validation
	 */
	UI_tabSearchFlights.validateSearchFlight = function (){
		
		UI_commonSystem.initializeMessage();
		
		var selectedSegmentCabinclass = '';             
		if (UI_tabSearchFlights.isRequote()) {
			selectedSegmentCabinclass = UI_requote.selectedSegmentCabinclass;
		} else {
			selectedSegmentCabinclass = $("#classOfService").val();
		}
		
		if((top.arrPrivi['xbe.res.allow.modify.bookings.low.priority.cos'] != 1) 
				&& (top.arrCabinClassRanking[$("#classOfService").val().split("-")[0]]  > top.arrCabinClassRanking[selectedSegmentCabinclass])){
			showERRMessage("No privilege to modify bookings to low priority cabin classes");	
			return false;
		}
		
		/* Mandatory Validations */
		if (!UI_commonSystem.validateMandatory(jsonSCObj)){return false;}
		
		if($('#fromAirport').val().toUpperCase() == 'PLEASE SELECT A VALUE'){
			showERRMessage(raiseError("XBE-ERR-01", "Departure location"));
			$("#fromAirport").focus();			
			return false;
		}
		
		if($('#toAirport').val().toUpperCase() == 'PLEASE SELECT A VALUE'){
			showERRMessage(raiseError("XBE-ERR-01", "Arrival location"));
			$("#toAirport").focus();			
			return false;
		}
		
		
		if ($("#fromAirport").val() == $("#toAirport").val()){
			showERRMessage(raiseError("XBE-ERR-02","Departure location","Arrival location"));
			$("#fromAirport").focus();			
			return false;
		}
		if(UI_commonSystem.strPGMode != "confirmOprt"){
			
			if ($("#departureDate").val() == "" || ($("#departureDate").val().indexOf("/") == -1)){
				showERRMessage("Wrong date format of Departure Date");
				$("#departureDate").focus();		
				return false;
			}
			
			if (!CheckDates(top.strSysDate, $("#departureDate").val())){
				showERRMessage(raiseError("XBE-ERR-03","From date", top.strSysDate));
				$("#departureDate").focus();		
				return false;
			}
			if($("#bookingType").val()=="" || $("#bookingType").val()==null){
				showERRMessage(raiseError("XBE-ERR-06","Booking Type"));
				return false;
			}
		}
		
		if(UI_commonSystem.strPGMode == "confirmOprt"){
			if ($("#returnDate").val()=="" || $("#returnDate").val()==null){
				showERRMessage(raiseError("XBE-ERR-06","Return Date"));
				$("#returnDate").focus();		
				return false;
			}
			if ($("#returnVariance").val() != "") {
				var trvlExp = DATA_ResPro.modifySegments.lastArrival;
				var trvlExpArr = trvlExp.split("-");
				var retDateArr = $("#returnDate").val().split("/");
				
				var expiryDate = new Date(trvlExpArr[0],(trvlExpArr[1] -1),trvlExpArr[2].substring(0,2));
				var retDate = new Date(retDateArr[2],(retDateArr[1] -1),retDateArr[0]);
				if(expiryDate.getTime() < retDate.getTime()){
					showERRMessage(raiseError("XBE-ERR-05","Return Date ", " travel expiry date"));
					$("#returnDate").focus();		
					return false;
				}
			}
		   
			var reservationPaxs = DATA_ResPro.resPaxs;
			var retDateArr = $("#returnDate").val().split("/");
			var retDate = new Date(retDateArr[2],(retDateArr[1] -1),retDateArr[0]);
						  
			for(var i=0; i < reservationPaxs.length; i++){								
				var passportExpiryDateStr = reservationPaxs[i].lccClientAdditionPax.passportExpiry;				
				if(passportExpiryDateStr != null && passportExpiryDateStr != ''){					
					var passportExpiryDate = decodeJsonDate(passportExpiryDateStr);														
					if (passportExpiryDate < retDate){
						var paxName = reservationPaxs[i].firstName + " " + reservationPaxs[i].lastName;
						showERRMessage("Passport expiry date of passenger : " + paxName + ", is less than the return date.");
						$("#returnDate").focus();
						return false;
					} 
				}				
			}
		
		}
		
		if ($("#departureVariance").val() != ""){
			if($("#departureVariance").val() > top.strDefMaxV){
				showERRMessage(raiseError("XBE-ERR-07","Departure Variance"));
				$("#departureVariance").focus();	
				return false;
			}
			if (!UI_commonSystem.numberValidate({id:"#departureVariance", desc:"Departure Variance", minValue:0, maxValue : top.strDefMaxV})){return false;}
		}else {
			$("#departureVariance").val(0);
		}
		
		if ($("#returnDate").val() != ""){
			
			if ($("#returnDate").val().indexOf("/") == -1){
				showERRMessage("Wrong date format of Return Date");
				$("#departureDate").focus();		
				return false;
			}

			if(UI_commonSystem.strPGMode != "confirmOprt"){
				if (!CheckDates($("#departureDate").val(), $("#returnDate").val())){
					showERRMessage(raiseError("XBE-ERR-03","Return date", "from date"));
					$("#returnDate").focus();	
					return false;
				}
			}
			
			if ($("#returnVariance").val() != "") {
				if($("#returnVariance").val() > top.strDefMaxV){
					showERRMessage(raiseError("XBE-ERR-07","Return Variance"));
					$("#returnVariance").focus();	
					return false;
				}
				if (!UI_commonSystem.numberValidate({id:"#returnVariance", desc:"Return Variance", minValue:0, maxValue : top.strDefMaxV})){return false;}
			}else {
				$("#returnVariance").val(0);
			}
		}
		
		if(UI_commonSystem.strPGMode != "transferSegment" && UI_commonSystem.strPGMode != "confirmOprt"){
			if(false){
// if($("#ticketExpiryEnabled").val()!="" && $("#ticketExpiryEnabled").val()){
				
				var ticketExpiry = $("#ticketExpiryDate").val();
				var ticketExpiryDate;
				var ticketExpDateFrmtStr;			
				if(ticketExpiry!=null && ticketExpiry!=""){
					var ticketExpArr = ticketExpiry.split("-");
					ticketExpiryDate = new Date(ticketExpArr[2].substring(0,4),(ticketExpArr[1] -1),ticketExpArr[0]);
					ticketExpDateFrmtStr = ticketExpArr[0]+"/"+ticketExpArr[1]+"/"+ticketExpArr[2].substring(0,4);
				}			
				
				var validFrom = $("#ticketValidFrom").val();			
				var validFromDate;
				var validFromDateFrmtStr;			
				if (validFrom!=null && validFrom!=""){			
					var validFromArr = validFrom.split("-");
					validFromDate = new Date(validFromArr[2].substring(0,4),(validFromArr[1] -1),validFromArr[0]);
					validFromDateFrmtStr = validFromArr[0]+"/"+validFromArr[1]+"/"+validFromArr[2].substring(0,4);
				}				
				
				if (ticketExpiry!="" && !CheckDates($("#departureDate").val(),$("#ticketExpiryDate").val())){
					showERRMessage(raiseError("XBE-ERR-05","Departure Date", ticketExpDateFrmtStr));
					$("#departureDate").focus();		
					return false;
				}
				
				if (validFrom!="" && !CheckDates($("#ticketValidFrom").val(),$("#departureDate").val())){				
					showERRMessage(raiseError("XBE-ERR-03","Departure Date", validFromDateFrmtStr));
					$("#departureDate").focus();		
					return false;
				}
				
			}
			
		}		
		
		/* Number Validations */
		if ($("#adultCount").val() != ""){
			if (!UI_commonSystem.numberValidate({id:"#adultCount", desc:"Number of Adults", minValue:0})){return false;}
		}else{
			$("#adultCount").val(0);
		}
		
		if ($("#childCount").val() != ""){
			if (!UI_commonSystem.numberValidate({id:"#childCount", desc:"Number of Children", minValue:0})){return false;}
		}else{
			$("#childCount").val(0);
		}
		
		if ($("#infantCount").val() != ""){
			if (!UI_commonSystem.numberValidate({id:"#infantCount", desc:"Number of Infants", minValue:0})){return false;}
			if(Number($("#infantCount").val()) > Number($("#adultCount").val())){
				showERRMessage(raiseError("XBE-ERR-05","Infant count", "adult count"));
				$("#infantCount").focus();	
				return false;
			}
		}else{
			$("#infantCount").val(0);
		}
		
		if (($("#adultCount").val() == "") || ($("#adultCount").val() == "0")){
			if (top.childBookingStatus != "Y") {
				showERRMessage(raiseError("XBE-ERR-01","Number of Adults"));
				$("#adultCount").focus();	
				return false;
			}
		}
		
		if (top.childBookingStatus == "Y") {
			if ((String(trim($("#childCount").val())) == "" || String(trim($("#childCount").val())) == "0")
			 		&& (String(trim($("#adultCount").val())) == "" || String(trim($("#adultCount").val())) == "0")) {
				 showERRMessage(raiseError("XBE-ERR-01","No of Adult or Children"));
				 $("#childCount").focus();
				 return false;
			}
		}
		
		if (typeof top.arrPrivi[PRIVI_GROUP_BOOKING]!=='undefined' && top.arrPrivi[PRIVI_GROUP_BOOKING] == 1){
		}else{
			var intTotPax = Number($("#adultCount").val()) + Number($("#childCount").val()) + Number($("#infantCount").val());
			if (intTotPax > top.intMaxGroup){
				showERRMessage(raiseError("XBE-ERR-15",String(top.intMaxGroup)));
				$("#adultCount").focus();	
				return false;
			}
		}
		
		if ($("#openReturn").attr('checked') && $("#validity").isFieldEmpty()){
			showERRMessage(raiseError("XBE-ERR-01","Validity Period"));
			$("#validity").focus();
			return false;
		}
		if(UI_commonSystem.strPGMode == "confirmOprt"){
			UI_tabSearchFlights.strOutDate = $("#returnDate").val();
		}
		else{
			UI_tabSearchFlights.strOutDate = $("#departureDate").val();
			UI_tabSearchFlights.strRetDate = $("#returnDate").val();
		}
		
		if ($("#returnDate").val() == ""){$("#returnVariance").val("")}
		if ($("#returnDate").val() != ""){
			if ($("#returnVariance").val() == ""){$("#returnVariance").val("0")}
		}		
		
		UI_tabSearchFlights.strOrigin = $("#fromAirport").val();
		UI_tabSearchFlights.strDestination = $("#toAirport").val();
		
// $("#btnSearch").disable();
// $("#btnFQ").disable();
		UI_commonSystem.showProgress();
		
		
		return true;
	}
	
	UI_tabSearchFlights.checkForNoFlightAndAlert = function(){
		var gridMsg = function(targetId, msg){
			$(targetId+' tbody').html('<tr class="ui-widget-content jqgrow" role="row" >'+
				'<td title="" style="text-align: center; " role="gridcell">'+msg+' </td></tr>');
		}
		var noFlightAvailMsg = 'No flights available';
		var outNoResults = false;
		if(jsonOutFlts== null || jsonOutFlts == 0){
			outNoResults = true;
			gridMsg("#tblOutboundFlights",noFlightAvailMsg);
		}
		
		var searchOtherMsg = noFlightAvailMsg;
		if ($("#selSearchOptions").val() == 'AA'){
			searchOtherMsg += ' in own airline. Try searching all airlines';
		}else{
			searchOtherMsg += ' in all airlines';
		}

		if (UI_tabSearchFlights.strRetDate != ""){			
			if(jsonRetFlts== null || jsonRetFlts.length == 0){
				if(outNoResults){
					if($("#selSearchOptions option").length >1 && !$('#selSearchOptions').attr('disable')){
						showERRMessage(searchOtherMsg);
					}
				}
				gridMsg("#tblInboundFlights",noFlightAvailMsg);				
			}
		}else if(outNoResults){
			if($("#selSearchOptions option").length >1 && !$('#selSearchOptions').attr('disable')){
				showERRMessage(searchOtherMsg);
			}
		}
		
	}
	
	UI_tabSearchFlights.isUserSearchedDateAvailable = function (departureDate, flightDepartureDate) {
		if(departureDate != undefined && flightDepartureDate != undefined && departureDate != "" && flightDepartureDate != ""){
			var splitedDateDep = departureDate.split("/");		
			var preferredDate = new Date(splitedDateDep[2], Number(splitedDateDep[1])-1,splitedDateDep[0]);			
			
			var flightDepDate = new Date(splitedDateDep[2], Number(flightDepartureDate.substring(2,4))-1, flightDepartureDate.substring(4,6));
			
			if(flightDepDate.getTime() == preferredDate.getTime()){
				return true;
			}			
		}	
		return false;
	}
	
	/*
	 * Ajax Return Call
	 */
	UI_tabSearchFlights.returnCallSearchFlight = function (response){
				
		UI_commonSystem.hideProgress();
		UI_tabSearchFlights.populateFltDates();
		UI_tabSearchFlights.disablePageControls();
		UI_tabSearchFlights.pgObj.lockTabs();
		UI_tabSearchFlights.objResSrchParams = response.searchParams;
		UI_tabSearchFlights.flightInfo = response.flightInfo;
		UI_tabSearchFlights.jsonFlightInfo = $.toJSON(UI_tabSearchFlights.flightInfo);
		UI_tabSearchFlights.strBkgClassDesc = response.classOfServiceDesc;

		UI_tabSearchFlights.displayBasedOnTemplates = response.displayBasedOnTemplates;
		
		UI_tabSearchFlights.clearAvilability();
		UI_tabSearchFlights.isOpenReturn = false;
		UI_tabSearchFlights.openReturnInfo = null;
		if(response.success) {	
			
			jsonOutFlts = response.outboundFlights;
			jsonRetFlts = response.returnFlights;
			UI_tabSearchFlights.isOpenReturn = response.openReturn;
			UI_tabSearchFlights.openReturnInfo = response.openReturnInfo;			
			
			UI_tabSearchFlights.busCarrierCodes = response.busCarrierCodes;
			
			
			$("#divResultsPane").show();
			$("#trFareSection").hide();
			if(!UI_tabSearchFlights.isRequote()){
				top.gstAppliedForReservation = response.serviceTaxApplicable;
				UI_tabSearchFlights.buildFareQuoteHD();	
			} else {
				$("#tblIBOBFlights").show();
				$("#divRetPane").hide();
			}
			
			var SendTOOutBoundGrid = UI_tabSearchFlights.flightNumberLabeling(response.outboundFlights, 'OB');
			UI_commonSystem.fillGridData({id:"#tblOutboundFlights", data:SendTOOutBoundGrid});
			// highlight the inbound grid
			UI_tabSearchFlights.highlightOutBoundGrid();
			
			// show the outbound grid
			$("#tblOutboundFlights").show();
			var prefDateSelected = false;
			for(var i=0; i < jsonOutFlts.length; i++){
				if(jsonOutFlts[i].selected){
					$("#trSearchParams_icon").trigger("click");
					prefDateSelected = true;
					break;
				}
			}

			if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode != "confirmOprt"){
				var SendTOInBoundGrid = UI_tabSearchFlights.flightNumberLabeling(response.returnFlights, 'IB');
				UI_commonSystem.fillGridData({id:"#tblInboundFlights", data:SendTOInBoundGrid});				
				
				for(var i=0; i < jsonRetFlts.length; i++){
					if(jsonRetFlts[i].selected & prefDateSelected){
						prefDateSelected = true;
						break;
					}
				}
				UI_tabSearchFlights.highlightInBoundGrid();
				// show the inbound grid
				$("#tblInboundFlights").show();
			}else if(UI_tabSearchFlights.isOpenReturn){	
				UI_commonSystem.fillGridData({id:"#tblOpenReturnInfo", data:[response.openReturnInfo]});
				$('#tblOpenReturnInfo').show();				
			}
			UI_tabSearchFlights.checkForNoFlightAndAlert();
			response.prefDateSelected = prefDateSelected;
			UI_tabSearchFlights.selectedOutDate = UI_tabSearchFlights.strOutDate;
			UI_tabSearchFlights.selectedRetDate = UI_tabSearchFlights.strRetDate;
			
			$('#divOpenRetPane').hide();
			if(UI_commonSystem.strPGMode != "confirmOprt" && !UI_tabSearchFlights.isRequote()){
				UI_tabSearchFlights.returnCallShowFareQuoteInfo(response);
			}
			UI_tabSearchFlights.airportMessage = response.airportMessage;
			
			if(UI_commonSystem.strPGMode == "transferSegment" && response.outboundFlights!=null  && response.outboundFlights.length > 0){				
				$('#btnBook').show();
				$('#btnBook').enable();
				$('#divFarePane').hide();
			}
			
			if(UI_commonSystem.strPGMode == "confirmOprt"){				
				$('#btnBook').show();
				$('#btnBook').enable();
				$("#trFareSection").hide();
				$("#tblInboundFlights").hide();
				UI_tabSearchFlights.showHideNextPrevLink();
			}
			
			DATA_ResPro.initialParams = response.rpParams;			
			UI_tabSearchFlights.checkPrivileges();
			UI_tabSearchFlights.flightNumberLabelingEventBinding();// to bind
																	// event of
																	// the +
																	// mark
			if(UI_tabSearchFlights.isRequote() && DATA_ResPro.initialParams.requoteAutoFQEnabled
					&& DATA_ResPro.initialParams.requoteAutoFltSelectEnabled){
				if(jsonOutFlts != null && jsonOutFlts.length > 0){
					for(var i = 0 ; i < jsonOutFlts.length ; i++){
						if(jsonOutFlts[i].seatAvailable && 
								UI_tabSearchFlights.isUserSearchedDateAvailable(UI_tabSearchFlights.objResSrchParams.departureDate, 
										jsonOutFlts[i].depatureTimeList[0])){							
							$("#tblOutboundFlights").setSelection(jsonOutFlts[i].index, true);
							break;
						}
					}
				}
			}
			UI_tabSearchFlights.flightRPHWaitListingStatus = {};
			if(UI_tabSearchFlights.objResSrchParams.bookingType == 'WAITLISTING'){
				UI_tabSearchFlights.flightRPHWaitListingStatus = UI_tabSearchFlights.createFlightRPHWaitListMap();	
				$("#btnCAllFare").hide();
				$("#btnCFare").hide();
			}
			if(typeof(UI_tabPassenger) != 'undefined'){	
				UI_tabPassenger.serviceTaxApplicable = response.serviceTaxApplicable;
			}
		}else{
			showERRMessage(response.messageTxt);
		}
		// $("#btnFQ").enable();
		$("#btnSearch").enable();
		$("#lnkOP").focus();
		UI_tabSearchFlights.loadAirportMessage();	
		var outBoundAvailbleBundleFareArray = new Array();
		var inBoundAvailbleBundleFareArray = new Array();
		
		if(response.ondLogicalCCList != undefined && response.ondLogicalCCList != null){			
			$.each(response.ondLogicalCCList, function(index, ondViseLogicalCCList) {
				var ondSequnce = ondViseLogicalCCList[0].sequence ;
				if(ondSequnce == 0 ){
					$.each(ondViseLogicalCCList[0].availableLogicalCCList,function(avilableLogicalCCIndex, logicalCC){
						if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
							var arrIndex = outBoundAvailbleBundleFareArray.length
							outBoundAvailbleBundleFareArray[arrIndex] = new Array();
							outBoundAvailbleBundleFareArray[arrIndex].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,
								"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, 
								"bundleFareName":logicalCC.logicalCCDesc});		
						}
						
					});
				}else {
					$.each(ondViseLogicalCCList[0].availableLogicalCCList,function(avilableLogicalCCIndex, logicalCC){
						if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
							var arrIndex = inBoundAvailbleBundleFareArray.length
							inBoundAvailbleBundleFareArray[arrIndex] = new Array();
							inBoundAvailbleBundleFareArray[arrIndex].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,
								"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, 
								"bundleFareName":logicalCC.logicalCCDesc});		
						}	
					});
				}
			});
		}
		UI_tabSearchFlights.resAvailableBundleFareLCClassStr[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = outBoundAvailbleBundleFareArray;	
		UI_tabSearchFlights.resAvailableBundleFareLCClassStr[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = inBoundAvailbleBundleFareArray;
		
		UI_tabSearchFlights.ondWiseFlexiAvailable = UI_tabSearchFlights.convertArrayToMap(response.ondWiseTotalFlexiAvailable);
		$("#resFlexiAvailableStr").val($.toJSON(UI_tabSearchFlights.ondWiseFlexiAvailable));
		UI_tabSearchFlights.ondwiseFlexiFare =  response.ondWiseTotalFlexiCharge;
		UI_tabSearchFlights.flexiEnableInAnci = response.isFlexiEnableInAnci;
		
		if($.inArray($('#fromAirport').val(), top.gstMessageEnabledAirports) != -1 && UI_commonSystem.strPGMode == ""){
			$("#gstMessage").html(top.gstMessage);
			$("#divGstMessageAlert").show();
		}
		
	}
	
	UI_tabSearchFlights.createFlightRPHWaitListMap = function() {
		var ind=0;
		var flightRPHWLStatus = {};
		if(jsonOutFlts != null){
			for(var i=0; i < jsonOutFlts.length; i++){
				for(ind =0; ind< jsonOutFlts[i].waitListedList.length; ind++){
					flightRPHWLStatus[jsonOutFlts[i].flightRPHList[ind]] = jsonOutFlts[i].waitListedList[ind];
				}
			}	
		}
		
		if(jsonRetFlts != null){
			for(var i=0; i < jsonRetFlts.length; i++){
				for(ind =0; ind< jsonRetFlts[i].waitListedList.length; ind++){
					if(jsonRetFlts[i].waitListedList[ind] && jsonRetFlts[i].selected){
						flightRPHWLStatus[jsonRetFlts[i].flightRPHList[ind]] = jsonRetFlts[i].waitListedList[ind];
					}
				}
			}	
		}
		return flightRPHWLStatus;
	}
	
	UI_tabSearchFlights.highlightInBoundGrid = function() {
		// highlight the selected row
		var selectedVal = $("input[name='returnSelection']:checked").val();
		if(selectedVal != null){
			$("#tblInboundFlights").setSelection(selectedVal);
		}
	}
	
	UI_tabSearchFlights.highlightOutBoundGrid = function() {
		// highlight the inbound grid
		var selectedVal = $("input[name='depatureSelection']:checked").val();
		if(selectedVal != null){UI_tabSearchFlights.applyFareDiscount
			$("#tblOutboundFlights").setSelection(selectedVal);
		}
	}
	
	UI_tabSearchFlights.constructOpenReturnGrid= function(){
		$('#tblOpenReturnInfo').jqGrid({
			datatype: "local",
			height: 110,
			width: 555,
			colNames:[  'Segment', 'Confirm Before', 'Travel Expiry'],
			colModel:[
				{name:'segmentCode',  index:'segmentCode', width:150, align:"left"},
				{name:'confirmExpiryDate', index:'confirmExpiryDate', width:150, align:"center"},
				{name:'travelExpiryDate', index:'travelExpiryDate', width:150, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true
		});
	}
	
	UI_tabSearchFlights.populateFltDates = function(){
		UI_tabSearchFlights.strOutDateFmt = dateChk(UI_tabSearchFlights.strOutDate, UI_tabSearchFlights.strDTFormatDDMMMYYYY);
		UI_tabSearchFlights.strRetDateFmt = "";
		$("#divOutDate").html("Outgoing " + UI_tabSearchFlights.strOutDateFmt);
		if(UI_tabSearchFlights.strRetDate != undefined){
			if (UI_tabSearchFlights.strRetDate != ""){
			UI_tabSearchFlights.strRetDateFmt = dateChk(UI_tabSearchFlights.strRetDate, UI_tabSearchFlights.strDTFormatDDMMMYYYY);
			$("#divRetDate").html("Return " + UI_tabSearchFlights.strRetDateFmt);
			}
		}
		
	}
	
	UI_tabSearchFlights.updateFareQuoteHeaderSummary = function(){
		var strHTMLText = "";
		var strFareQuote = geti18nData('fareQuote_PriceDetails','Fare Quote');
		if(UI_tabSearchFlights.selectedOutDate != ""){
			var formatedOutDate = dateChk(UI_tabSearchFlights.selectedOutDate, UI_tabSearchFlights.strDTFormatDDMMMYYYY);
			strHTMLText = "&nbsp;"+strFareQuote+" - Outgoing " + formatedOutDate ;
		}
		
		if (UI_tabSearchFlights.selectedRetDate != ""){
			var formatedRetDate = dateChk(UI_tabSearchFlights.selectedRetDate, UI_tabSearchFlights.strDTFormatDDMMMYYYY);
			strHTMLText += " - Return " + formatedRetDate;
		}
		strHTMLText += " - " + UI_tabSearchFlights.strBkgClassDesc;
		if(UI_tabSearchFlights.isMulticity()){
			strHTMLText = "&nbsp;"+strFareQuote+" - Journey Start&nbsp;"+UI_tabSearchFlights.flightInfo[0].departureDate.substring(4,6)+"/"+UI_tabSearchFlights.flightInfo[0].departureDate.substring(6,9)+"/20"+UI_tabSearchFlights.flightInfo[0].departureDate.substring(9) + "&nbsp;- Journey End &nbsp;" +UI_tabSearchFlights.flightInfo[UI_tabSearchFlights.flightInfo.length-1].departureDate.substring(4,6)+"/"+UI_tabSearchFlights.flightInfo[UI_tabSearchFlights.flightInfo.length-1].departureDate.substring(6,9)+"/20"+UI_tabSearchFlights.flightInfo[0].departureDate.substring(9);
		}
		$("#divFareQT").html(strHTMLText);
	}
	
	/*
	 * Build the Fare Quote Heading
	 */
	UI_tabSearchFlights.buildFareQuoteHD = function(){	
		$("#divBreakDownPane").hide();

		var selectedFlightFound = false;
		if(jsonOutFlts != null && jsonOutFlts.length > 0 ){
			selectedFlightFound = true;
		}
		UI_tabSearchFlights.populateFltDates();
		
		if (UI_tabSearchFlights.strRetDate != ""){
			$("#divRetPane").show();
			if(jsonOutFlts == null || jsonRetFlts.length == 0){
				selectedFlightFound = false;
			}
		}else {
			$("#divRetPane").hide();	
		}
		
		if(UI_tabSearchFlights.isOpenReturn){
			$('#divOpenRetPane').show();
		}else{
			$('#divOpenRetPane').hide();
		}

		// open return search didn't find matching end flight
		if($("#openReturn").attr('checked') && (!UI_tabSearchFlights.isOpenReturn ||UI_tabSearchFlights.openReturnInfo == null )){
			selectedFlightFound = false;
		}

		if(selectedFlightFound){
			// Hide Fare Section
			if (UI_commonSystem.strPGMode != "transferSegment")  {
				$("#trFareSection").show();
				$("#btnCAllFare").hide();
			}
		}
		if( UI_commonSystem.strPGMode == "confirmOprt"){
			$('#btnBook').show();
			$('#btnBook').enable();
			$("#trFareSection").hide();
			$("#divRetPane").hide();
		}
	}
	
	/*
	 * ------------------------------------------------ Navigations Start
	 * -----------------------------------------------
	 */
	/*
	 * Grid Day Navigation
	 */
	UI_tabSearchFlights.gridDayNavigation = function(inParams){	
		UI_tabSearchFlights.clearFareQuote();
		
		if(inParams.id == UI_tabSearchFlights.strNavOP || inParams.id == UI_tabSearchFlights.strNavON){			
			UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = false;
			UI_tabSearchFlights.ondWiseBundleFareSelected[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = null;
		} else {			
			UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = false;
			UI_tabSearchFlights.ondWiseBundleFareSelected[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = null;
		}
		
		$.each(UI_tabSearchFlights.ondRebuildLogicalCCAvailability, function(key, value){
			UI_tabSearchFlights.ondRebuildLogicalCCAvailability[key] = true;
		});
			
		if (UI_tabSearchFlights.blnNPLoaded && UI_tabSearchFlights.gridDayNavigationValidation(inParams)){
			UI_tabSearchFlights.disableEnablePageControls(false);
			UI_tabSearchFlights.blnNPLoaded = false;
			var data = {};		
			UI_tabSearchFlights.createSubmitData({data:data, mode:UI_commonSystem.strPGMode});			
			
			data = $.extend(data, UI_tabSearchFlights.createGridDayNavigationParams(inParams));
			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'nextPreviousSearchFlights.action', dataType: 'json', data:data,
											success:UI_tabSearchFlights.returnCallGridDayNavigation,
											error:UI_commonSystem.setErrorStatus});
											
			return false;
		}
	}
	
	/*
	 * Navigate Validations
	 */
	UI_tabSearchFlights.gridDayNavigationValidation = function(inParams){	
		UI_commonSystem.initializeMessage();
		UI_tabSearchFlights.disablePageControls();
		UI_tabSearchFlights.strNavDate = "";
		UI_tabSearchFlights.strNavLast = "";
		
		switch (inParams.id){
			case UI_tabSearchFlights.strNavOP :
				if(UI_commonSystem.strPGMode == "confirmOprt"){
					if(UI_tabSearchFlights.strOutDate==null || UI_tabSearchFlights.strOutDate==""){
						UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate($("#returnDate").val()), -1));
						UI_tabSearchFlights.strOutDate = UI_tabSearchFlights.strNavDate;
					}
					else{
						UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate(UI_tabSearchFlights.strOutDate), -1));
					}
				}
				else{
					UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate(UI_tabSearchFlights.strOutDate), -1));
					
				}
				
				if (!CheckDates(top.strSysDate, UI_tabSearchFlights.strNavDate)){
					showERRMessage(raiseError("XBE-ERR-03","From date", top.strSysDate));
					return false;
				}
				
				break;
				
			case UI_tabSearchFlights.strNavON :
				if(UI_commonSystem.strPGMode == "confirmOprt"){
					if(UI_tabSearchFlights.strOutDate==null || UI_tabSearchFlights.strOutDate==""){
						UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate($("#returnDate").val()), 1));
						UI_tabSearchFlights.strOutDate = UI_tabSearchFlights.strNavDate;
					}
					else{
						UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate(UI_tabSearchFlights.strOutDate), 1));
					}
				}
				else{
					UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate(UI_tabSearchFlights.strOutDate), 1));
				}
				break;
				
			case UI_tabSearchFlights.strNavRP :
				UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate(UI_tabSearchFlights.strRetDate), -1));
				if (!CheckDates(top.strSysDate, UI_tabSearchFlights.strNavDate)){
					showERRMessage(raiseError("XBE-ERR-03","Return date", top.strSysDate));
					return false;
				}
				break;
				
			case UI_tabSearchFlights.strNavRN :
				UI_tabSearchFlights.strNavDate = dateToString(addDays(stringToDate(UI_tabSearchFlights.strRetDate), 1));
				break;
		}
		
		if(false){
// if(UI_commonSystem.strPGMode != "confirmOprt" &&
// $("#ticketExpiryEnabled").val()!="" && $("#ticketExpiryEnabled").val()){
			
			var ticketExpiry = $("#ticketExpiryDate").val();
			var ticketExpiryDate;
			var ticketExpDateFrmtStr;			
			if(ticketExpiry!=null && ticketExpiry!=""){
				var ticketExpArr = ticketExpiry.split("-");
				ticketExpiryDate = new Date(ticketExpArr[2].substring(0,4),(ticketExpArr[1] -1),ticketExpArr[0]);
				ticketExpDateFrmtStr = ticketExpArr[0]+"/"+ticketExpArr[1]+"/"+ticketExpArr[2].substring(0,4);
			}			
			
			var validFrom = $("#ticketValidFrom").val();			
			var validFromDate;
			var validFromDateFrmtStr;			
			if (validFrom!=null && validFrom!=""){			
				var validFromArr = validFrom.split("-");
				validFromDate = new Date(validFromArr[2].substring(0,4),(validFromArr[1] -1),validFromArr[0]);
				validFromDateFrmtStr = validFromArr[0]+"/"+validFromArr[1]+"/"+validFromArr[2].substring(0,4);
			}
			
			
			
			if (ticketExpiry!="" && !CheckDates(UI_tabSearchFlights.strNavDate,$("#ticketExpiryDate").val())){
				showERRMessage(raiseError("XBE-ERR-05","Departure Date", ticketExpDateFrmtStr));
				$("#departureDate").focus();		
				return false;
			}
			
			if (validFrom!="" && !CheckDates($("#ticketValidFrom").val(),UI_tabSearchFlights.strNavDate)){				
				showERRMessage(raiseError("XBE-ERR-03","Departure Date", validFromDateFrmtStr));
				$("#departureDate").focus();		
				return false;
			}	
		}		
		
		UI_tabSearchFlights.strNavLast = inParams.id;
		return true;
	}
	
	/*
	 * Create Navigate params
	 */
	UI_tabSearchFlights.createGridDayNavigationParams = function(inParams){
		// var strSrchParams = "";
		var strSrchParams = {};
		
	
		var strJSDTO = "searchNavParams";
		
		if ((UI_tabSearchFlights.strNavLast == UI_tabSearchFlights.strNavOP) || (UI_tabSearchFlights.strNavLast == UI_tabSearchFlights.strNavON)){
			strSrchParams[strJSDTO  + ".fromAirport"]= UI_tabSearchFlights.objResSrchParams.fromAirport;
			strSrchParams[strJSDTO  + ".toAirport"] = UI_tabSearchFlights.objResSrchParams.toAirport;
			strSrchParams[strJSDTO  + ".departureVariance"] = UI_tabSearchFlights.objResSrchParams.departureVariance;
			strSrchParams["direction"]="depature";
		}else{
			strSrchParams[strJSDTO  + ".fromAirport"] = UI_tabSearchFlights.objResSrchParams.toAirport;
			strSrchParams[strJSDTO  + ".toAirport"] = UI_tabSearchFlights.objResSrchParams.fromAirport;
			strSrchParams[strJSDTO  + ".departureVariance"] = UI_tabSearchFlights.objResSrchParams.returnVariance;
			strSrchParams["direction"]="return";
		}
		strSrchParams[strJSDTO  + ".departureDate"] = UI_tabSearchFlights.strNavDate;
		if(UI_tabSearchFlights.objResSrchParams.validity!=null){
			strSrchParams[strJSDTO  + ".validity"] = UI_tabSearchFlights.objResSrchParams.validity;
		}
		strSrchParams[strJSDTO  + ".adultCount"] = UI_tabSearchFlights.objResSrchParams.adultCount;
		strSrchParams[strJSDTO  + ".childCount"] = UI_tabSearchFlights.objResSrchParams.childCount;
		strSrchParams[strJSDTO  + ".infantCount"] = UI_tabSearchFlights.objResSrchParams.infantCount;
		strSrchParams[strJSDTO  + ".classOfService"] = UI_tabSearchFlights.objResSrchParams.classOfService;
		strSrchParams[strJSDTO  + ".logicalCabinClass"] = UI_tabSearchFlights.objResSrchParams.logicalCabinClass;
		strSrchParams[strJSDTO  + ".selectedCurrency"] = UI_tabSearchFlights.objResSrchParams.selectedCurrency;
		strSrchParams[strJSDTO  + ".bookingType"] = UI_tabSearchFlights.objResSrchParams.bookingType;		
		strSrchParams[strJSDTO  + ".returnVariance"] = UI_tabSearchFlights.objResSrchParams.returnVariance;
		strSrchParams[strJSDTO  + ".paxType"] = UI_tabSearchFlights.objResSrchParams.paxType;
		strSrchParams[strJSDTO  + ".fareType"] = UI_tabSearchFlights.objResSrchParams.fareType;
		strSrchParams[strJSDTO  + ".openReturn"] = UI_tabSearchFlights.objResSrchParams.openReturn;
		strSrchParams[strJSDTO  + ".searchSystem"] = UI_tabSearchFlights.objResSrchParams.searchSystem;
		strSrchParams[strJSDTO  + ".stayOverTimeInMillis"] = UI_tabSearchFlights.objResSrchParams.stayOverTimeInMillis;
		strSrchParams[strJSDTO  + ".fromAirportSubStation"] = UI_tabSearchFlights.objResSrchParams.fromAirportSubStation;
		strSrchParams[strJSDTO  + ".toAirportSubStation"] = UI_tabSearchFlights.objResSrchParams.toAirportSubStation;
		strSrchParams[strJSDTO  + ".openReturnConfirm"] = UI_tabSearchFlights.objResSrchParams.openReturnConfirm;
		strSrchParams[strJSDTO  + ".travelAgentCode"] = UI_tabSearchFlights.objResSrchParams.travelAgentCode==null ? '' : UI_tabSearchFlights.objResSrchParams.travelAgentCode;
		strSrchParams[strJSDTO  + ".busForDeparture"] = UI_tabSearchFlights.objResSrchParams.busForDeparture;
		strSrchParams[strJSDTO  + ".busConValiedTimeFrom"] = UI_tabSearchFlights.objResSrchParams.busConValiedTimeFrom;
		strSrchParams[strJSDTO  + ".busConValiedTimeTo"] = UI_tabSearchFlights.objResSrchParams.busConValiedTimeTo;
		strSrchParams[strJSDTO  + ".bookingClassCode"] = UI_tabSearchFlights.objResSrchParams.bookingClassCode;
		strSrchParams[strJSDTO  + ".promoCode"] = UI_tabSearchFlights.objResSrchParams.promoCode;
		strSrchParams[strJSDTO  + ".bankIdentificationNo"] = UI_tabSearchFlights.objResSrchParams.bankIdentificationNo;
		strSrchParams[strJSDTO  + ".hubTimeDetailJSON"] = JSON.stringify(UI_tabSearchFlights.hubTimeDetailJSON, null);
		strSrchParams[strJSDTO  + ".ondWiseCitySearchStr"] = $.toJSON(UI_tabSearchFlights.ondWiseCitySearch);
		
		return strSrchParams;
	}
	
	UI_tabSearchFlights.addBusCarrierCodes = function(newCodes){
		for(var j=0; j < newCodes.length; j++){
			var found = false;
			for(var i =0; i < UI_tabSearchFlights.busCarrierCodes.length; i++){
				if(newCodes[j] == UI_tabSearchFlights.busCarrierCodes[i] ){
					found = true;
					break;
				}
			}
			if(!found){
				UI_tabSearchFlights.busCarrierCodes[UI_tabSearchFlights.busCarrierCodes.length] = newCodes[j];
			}
		}
	}
	UI_tabSearchFlights.getBusCarrierCode = function(carrierCode){
		if(UI_tabSearchFlights.isBusCarrierCode(carrierCode)){
			return '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">';
		}
		return carrierCode;
	}
	
	UI_tabSearchFlights.isBusCarrierCode = function(carrierCode){
		for(var i =0; i < UI_tabSearchFlights.busCarrierCodes.length; i++){
			if(carrierCode == UI_tabSearchFlights.busCarrierCodes[i] ){
				return true;
			}
		}
		return false;
	}
	/*
	 * TODO Grid Navigation Return
	 */
	UI_tabSearchFlights.returnCallGridDayNavigation = function (response){
		UI_commonSystem.hideProgress();
		UI_tabSearchFlights.hideFareQuotePane();
		UI_tabSearchFlights.blnNPLoaded = true;		
		UI_tabSearchFlights.isOpenReturn = false;
		UI_tabSearchFlights.openReturnInfo = null;
		if(response.success) {
			UI_tabSearchFlights.isOpenReturn = response.openReturn;			
			UI_tabSearchFlights.openReturnInfo = response.openReturnInfo;
			UI_tabSearchFlights.addBusCarrierCodes(response.busCarrierCodes);
			
			if ((UI_tabSearchFlights.strNavLast == UI_tabSearchFlights.strNavOP) || (UI_tabSearchFlights.strNavLast == UI_tabSearchFlights.strNavON ||UI_commonSystem.strPGMode == "confirmOprt")){
				UI_tabSearchFlights.strOutDate = UI_tabSearchFlights.strNavDate;
				jsonOutFlts = response.outboundFlights;
				var nextPrvGridData = UI_tabSearchFlights.flightNumberLabeling(response.outboundFlights , 'OB');
				UI_commonSystem.fillGridData({id:"#tblOutboundFlights", data:nextPrvGridData});
				UI_tabSearchFlights.highlightOutBoundGrid();
				
			}else{
				jsonRetFlts = response.outboundFlights;
				UI_tabSearchFlights.strRetDate = UI_tabSearchFlights.strNavDate;
				var nextPrvGridData = UI_tabSearchFlights.flightNumberLabeling(response.outboundFlights , 'IB');
				UI_commonSystem.fillGridData({id:"#tblInboundFlights", data:nextPrvGridData});
			}
			
			if (UI_tabSearchFlights.isOpenReturn){
				UI_commonSystem.fillGridData({id:"#tblOpenReturnInfo", data:[response.openReturnInfo]});
				$('#tblOpenReturnInfo').show();
			}else{
				$('#tblOpenReturnInfo').hide();
			}
			UI_tabSearchFlights.checkForNoFlightAndAlert();
			UI_tabSearchFlights.airportMessage = response.airportMessage;
			UI_tabSearchFlights.loadAirportMessage();	
			UI_tabSearchFlights.populateFltDates();
			if(UI_commonSystem.strPGMode == "transferSegment" && response.outboundFlights!=null  && response.outboundFlights.length > 0){				
				$('#btnBook').show();
				$('#btnBook').enable();
			}
			else if(UI_commonSystem.strPGMode == "confirmOprt"){
				$('#btnBook').show();
				$('#btnBook').enable();
				UI_tabSearchFlights.showHideNextPrevLink();
				UI_tabSearchFlights.highlightOutBoundGrid();				
			}
			// UI_tabSearchFlights.buildFareQuoteHD();
			UI_tabSearchFlights.flightNumberLabelingEventBinding();
		}else{
			showERRMessage(response.messageTxt);
		}
		
		UI_tabSearchFlights.clearLogicalCCData(); 		
		UI_tabSearchFlights.disableEnablePageControls(true);
	}
	
	
	/*
	 * Hide Fare Quote Pane
	 */
	UI_tabSearchFlights.showHideNextPrevLink = function(){
		
		// disable the prev and next links
		if ( DATA_ResPro != null &&  DATA_ResPro.modifySegments != null ){
	    var modifySeg =	DATA_ResPro.modifySegments;
		
		var firstDepDate = modifySeg.firstDepature;
	 	var firstDepDateStr = firstDepDate.split("-");					
		var firstDepDate = new Date(firstDepDateStr[0], (firstDepDateStr[1]-1) , firstDepDateStr[2].substring(0,2) );
		
	    var returnDate = modifySeg.lastArrival;
	    var returnDateStr = returnDate.split("-");
	    var returnDate = new Date(returnDateStr[0], (returnDateStr[1]-1) , returnDateStr[2].substring(0,2) );
	    
	    var selectedDate= UI_tabSearchFlights.strOutDate;
	    var selectedDateStr=selectedDate.split("\/");
	    var selectedDate = new Date(selectedDateStr[2], (selectedDateStr[1]-1) , selectedDateStr[0]);
	    
	    var isPreEnable = selectedDate > firstDepDate; 
	    var isNextEnable = selectedDate < returnDate; 
	  
		isPreEnable ?  $("#lnkOP").show() : $("#lnkOP").hide();
		isNextEnable ?  $("#lnkON").show() : $("#lnkON").hide();
		}
		
	}
	
	
	
	/*
	 * ------------------------------------------------ Navigations End
	 * -----------------------------------------------
	 */
	
	/*
	 * ------------------------------------------------ Fare Quote Start
	 * -----------------------------------------------
	 */
	
	/*
	 * Hide Fare Quote Pane
	 */
	UI_tabSearchFlights.hideFareQuotePane = function(){
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		$("#divFareDiscountpop").hide();
		UI_commonSystem.readOnly(false);
		// clear fare discount stuff
		UI_tabSearchFlights.fareDiscount.percentage = "0";
		UI_tabSearchFlights.fareDiscount.notes = "";
		UI_tabSearchFlights.fareDiscount.code = "";
		$("#lblDiscountApplied").text(0)
		$("#divFareQuotePane").hide();
		$("#divBreakDownPane").hide();
		$("#btnDiscountPop").text("Apply");
		$("#btnFareDiscountRemove").hide();
		$("#divFareQT").html("");
		
		$("#btnFQ").enable();
		// Enable btnBook for transfer segment
		if(UI_commonSystem.strPGMode != "transferSegment") {
			$("#btnBook").disable();
		}
		else{
			if(UI_commonSystem.strPGMode == "confirmOprt"){			
				UI_tabSearchFlights.clearFareQuote();
				$('#btnBook').show();
				$('#btnBook').enable();
			}
		}
		
		UI_tabSearchFlights.pgObj.lockTabs();
	};
	
	UI_tabSearchFlights.requote = function(){
		UI_requote.requoteFareQuote();
		flightToViewBCFares = null;
		fromViewBCFare = false;
	}
	
	UI_tabSearchFlights.addONDOnClick = function(){
		UI_tabSearchFlights.resetBundledServiceSelection();
		UI_tabSearchFlights.resetFlexiSelection();
		// TODO Validate the selection
		UI_requote.addOndFlights(UI_tabSearchFlights.getFlightRPHList(), UI_tabSearchFlights.jsonOndFQParams());
		if (UI_requote.overlappingOND) {
			return false;
		}
		$("#trFareSection").slideDown('fast');
		$("#divFareQuotePane").slideUp(0);
		flightToViewBCFares = null;
		fromViewBCFare = false;
		$("#trCheckStopOver").hide();		
		$("#btnCAllFare").hide();
		
	};
	/*
	 * Load Fare Quote for the Selected Flights
	 */
	UI_tabSearchFlights.loadFareQuoteOnClick =  function(inParam){
		inParam = $.extend({async:true}, inParam);
		$("#btnFQ").blur();	
		UI_tabSearchFlights.releaseBlockSeats();
		if (UI_commonSystem.strPGMode != "transferSegment"){
			UI_tabAnci.resetAnciData();
			UI_tabPassenger.resetData();
		}

		if (UI_tabSearchFlights.validateFareQuote()) {
			UI_tabSearchFlights.disableEnablePageControls(false);
			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', async:inParam.async, data:  UI_tabSearchFlights.createFareQuoteParams(), url:"loadFareQuote.action",
												success:UI_tabSearchFlights.returnCallShowFareQuoteInfo,
												error:UI_commonSystem.setErrorStatus});
		}		
		return false;
	};
		
	// fill the modifySegment
	UI_tabSearchFlights.fillModifySegment =  function(modifySeg){
		if(modifySeg.mode =="confirmOprt"){
			UI_tabSearchFlights.isOpenReturnConfirm = true ;
		}
		if(modifySeg != null) {		
			UI_tabSearchFlights.modifySearch = true;
			if(modifySeg.from != null) {
				$("#fromAirport").val(modifySeg.from);
				// $("#fAirport").val(modifySeg.from);
				UI_tabSearchFlights.fromCombo.setValue(modifySeg.from+"_N");
				// $('[name=searchParams\\.fromAirport__sexyCombo]').val($("#fromAirport
				// option:selected").text());
			}

			if(modifySeg.to != null) {
				$("#toAirport").val(modifySeg.to);
				// $("#tAirport").val(modifySeg.to);
				UI_tabSearchFlights.toCombo.setValue(modifySeg.to+"_N");
			// $('[name=searchParams\\.toAirport__sexyCombo]').val($("#toAirport
			// option:selected").text());
			}
			
			// criteria on segment modification
			if(UI_commonSystem.strPGMode != "transferSegment" && modifySeg.isAllowModifyByRoute != true){
				$("#fromAirport").disable();
				$("#toAirport").disable();
				UI_tabSearchFlights.fromCombo.disable();
				UI_tabSearchFlights.toCombo.disable();
			}
			
			if(UI_tabSearchFlights.isOpenReturnConfirm != true){
				if(modifySeg.departureDate != null) $("#departureDate").val(modifySeg.departureDate);	
			}
			else{
				$("#departureDate").val("");
				$("#departureDate").disableEnable(false);
			}
			$("#returnDate").val("");
			// criteria on segment modification
			if(UI_commonSystem.strPGMode != "transferSegment" && modifySeg.isAllowModifyByDate != true){
				$("#departureDate").disable();
				$("#departureDate").datepicker('disable');
				$("#returnDate").disable();
				$("#returnDate").datepicker('disable');				
				$("#returnVariance").disable();
				$("#departureVariance").disable();
				$("#departureVariance").val("0");
				$("#returnVariance").val("0");
				
				// bug fix == AARESAA-5463 Issue 2
				if( UI_tabSearchFlights.isOpenReturnConfirm ){		
					$("#chkStopOverAdd").hide();
					$("#returnDate").disableEnable(false);			
					$('#returnDate').datepicker().datepicker('enable');
				}else {
					$("#lnkON").remove();
					$("#lnkOP").remove();
				}
			}
			
			if(modifySeg.noOfAdults != null) $("#adultCount").val(modifySeg.noOfAdults);
			if(modifySeg.noOfChildren != null) $("#childCount").val(modifySeg.noOfChildren);
			if(modifySeg.noOfInfants != null) $("#infantCount").val(modifySeg.noOfInfants);
			if(modifySeg.bookingType != null) $("#bookingType").val(modifySeg.bookingType);
			
		// if(modifySeg.selectedCurrency != null)
		// $("#selectedCurrency").val(modifySeg.selectedCurrency);
			if(modifySeg.bookingclass != null) $("#classOfService").val(modifySeg.bookingclass);
			if(modifySeg.firstDepature != null) $("#firstDepature").val(modifySeg.firstDepature);
			if(modifySeg.lastArrival != null) $("#lastArrival").val(modifySeg.lastArrival);
			
			if(false){
// if(UI_commonSystem.strPGMode == "modifySegment" &&
// modifySeg.ticketExpiryEnabled && !UI_tabSearchFlights.isOpenReturnConfirm){
				
				$("#returnDate").disableEnable(true);			
				$('#returnDate').datepicker().datepicker('disable');	
				$("#returnVariance").disableEnable(true);
				
				if(modifySeg.ticketExpiryDate!=null && modifySeg.ticketExpiryDate!=""){					
				
					var ticketExpDateArr = modifySeg.ticketExpiryDate.split("-");
					var maxDate = new Date(ticketExpDateArr[2].substring(0,4),ticketExpDateArr[1]-1,ticketExpDateArr[0]);
					
					if(modifySeg.ticketValidFromDate!=null && modifySeg.ticketValidFromDate!=""){
						var tckValidFromArr = modifySeg.ticketValidFromDate.split("-");
						var minDate = new Date(tckValidFromArr[2].substring(0,4),tckValidFromArr[1]-1,tckValidFromArr[0]);
						
						if (CheckDates(top.strSysDate, modifySeg.ticketValidFromDate)){
							$("#departureDate").datepicker( "option", "minDate",minDate );	
						}					
						
					}					
					
					if (CheckDates(top.strSysDate, modifySeg.ticketExpiryDate)){
						$("#departureDate").datepicker( "option", "maxDate",maxDate );
					}
					
				}
				
				$("#ticketExpiryDate").val(modifySeg.ticketExpiryDate);
				$("#ticketValidFrom").val(modifySeg.ticketValidFromDate);
				$("#ticketExpiryEnabled").val(modifySeg.ticketExpiryEnabled);	
				
			}			
			
			if(UI_commonSystem.strPGMode == "modifySegment" && modifySeg.ticketExpiryEnabled && !UI_tabSearchFlights.isOpenReturnConfirm){
				if(modifySeg.ticketExpiryDate!=null && modifySeg.ticketExpiryDate!=""){	
					$("#ticketExpiryDate").val(modifySeg.ticketExpiryDate);
					$("#ticketValidFrom").val(modifySeg.ticketValidFromDate);
					$("#ticketExpiryEnabled").val(modifySeg.ticketExpiryEnabled);	
				}
			}				
			
			if(UI_commonSystem.strPGMode == "modifySegment" && modifySeg.ticketExpiryEnabled && !UI_tabSearchFlights.isOpenReturnConfirm){
				if(modifySeg.ticketExpiryDate!=null && modifySeg.ticketExpiryDate!=""){	
					$("#ticketExpiryDate").val(modifySeg.ticketExpiryDate);
					$("#ticketValidFrom").val(modifySeg.ticketValidFromDate);
					$("#ticketExpiryEnabled").val(modifySeg.ticketExpiryEnabled);	
				}
			}				
			
			if(UI_tabSearchFlights.isOpenReturnConfirm){
				// return date should be between today and expired date
				if(modifySeg.lastArrival != null){
					// 12/11/2010 <-- 2010-11-02
					var retDate = modifySeg.lastArrival;
					var retDateArr = retDate.split("-");
					retDate = retDateArr[2].substring(0,2)+"/"+retDateArr[1]+"/"+retDateArr[0];
					$("#returnDate").val(retDate);
					// set max date ( max date = expired date)
					$("#returnDate").datepicker( "option", "maxDate",new Date(retDateArr[0],(retDateArr[1] -1),retDateArr[2].substring(0,2)) );
					$("#returnDate").datepicker( "refresh" );
				}
				
				if (modifySeg.firstDepature != null){
					var firstDepDate = modifySeg.firstDepature;
					var firstDepDateStr = firstDepDate.split("-");					
					var fstDate = new Date(firstDepDateStr[0], (firstDepDateStr[1]-1) , firstDepDateStr[2].substring(0,2) );
					var today = new Date();
					if ( fstDate < today )
					{
						fstDate = today; 
					}
					// set max date ( max date = expired date)
					$("#returnDate").datepicker( "option", "minDate",fstDate );	
					$("#returnDate").datepicker( "refresh" );
					
				}
				
				$('#bookingType').empty();
				var arrOpenRtBookingTypes = new Array();
				arrOpenRtBookingTypes[0] = new Array('NORMAL','NORMAL');
				
				if((top.arrBookingTypes.length > 1 && top.arrBookingTypes[1][0] == 'OVERBOOK') || (top.arrBookingTypes.length > 2 && top.arrBookingTypes[2][0] == 'OVERBOOK')){
					arrOpenRtBookingTypes[1] = new Array('OVERBOOK','OVERBOOK');
				}
				$("#bookingType").fillDropDown( { dataArray:arrOpenRtBookingTypes , keyIndex:0 , valueIndex:1 });
				$("#bookingType").val("NORMAL");
				
			}
			if(modifySeg.groupPNR != null && modifySeg.groupPNR != 'null' && modifySeg.groupPNR != ''){
				$("#selSearchOptions").val('INT');
				//$("#selSearchOptions").disableEnable(true);
			}else {
				$("#selSearchOptions").val('AA');
			}
		}
	};
	
	UI_tabSearchFlights.fillRequote = function(requoteData){
		UI_tabSearchFlights.fillAddSegment(requoteData);
		$('#spnRetDate').hide();
		$('#returnDate').hide();
		$('#returnVariance').hide();
		$('#divOpen').hide();
		$('#openReturn').hide();
		$('#validity').hide();
		$('#spnVarianceDaysTxt').hide();
		$('#spnVarianceDaysTxt').hide();
		$('#bookingType').empty();
		//Removing wait listing option from requote flow 
		var reqouteBookingTypeArray = new Array();
		for(var ind=0;ind<top.arrBookingTypes.length;ind++){
			if(top.arrBookingTypes[ind][0] != 'WAITLISTING'){
				reqouteBookingTypeArray[ind] = top.arrBookingTypes[ind];
			}
		}
		$("#bookingType").fillDropDown( { dataArray:reqouteBookingTypeArray , keyIndex:0 , valueIndex:1 });
		$("#bookingType").val("NORMAL");
		UI_tabSearchFlights.objResSrchParams = {};
		UI_tabSearchFlights.objResSrchParams.adultCount = requoteData.noOfAdults;
		UI_tabSearchFlights.objResSrchParams.childCount = requoteData.noOfChildren;
		UI_tabSearchFlights.objResSrchParams.infantCount = requoteData.noOfInfants;
		UI_tabSearchFlights.objResSrchParams.selectedCurrency = $('#selectedCurrency').val();
		UI_tabSearchFlights.objResSrchParams.bookingType = $("#bookingType").val();		
		UI_tabSearchFlights.objResSrchParams.paxType = $('#selPaxTypeOptions').val();
		UI_tabSearchFlights.objResSrchParams.fareType = $('#selFareTypeOptions').val();
		UI_tabSearchFlights.objResSrchParams.bookingClassCode = $('#selBookingClassCode').val();
		
		var cos = $("#classOfService").val();
		var cosArr = cos.split("-");
		
		// If user selects logical cabin class, value patter will be
		// 'XXX-XXX' (logical_cc-cabin_class)
		if(cosArr.length > 1){
			UI_tabSearchFlights.objResSrchParams.logicalCabinClass = cosArr[0];
			UI_tabSearchFlights.objResSrchParams.classOfService = cosArr[1];
		} else if(cosArr.length == 1) {
			UI_tabSearchFlights.objResSrchParams.classOfService = cosArr[0];
		}
		
		
		if(requoteData.groupPNR != null && requoteData.groupPNR != 'null' && requoteData.groupPNR != ''){
			UI_tabSearchFlights.objResSrchParams.searchSystem = 'INT';
		}else {
			UI_tabSearchFlights.objResSrchParams.searchSystem = 'AA';
		}
	}
	/**
	 * Fill Add Segment Data
	 */
	UI_tabSearchFlights.fillAddSegment =  function(addSegment){
		if(addSegment != null) {					
			if(addSegment.noOfAdults != null) $("#adultCount").val(addSegment.noOfAdults);
			if(addSegment.noOfChildren != null) $("#childCount").val(addSegment.noOfChildren);
			if(addSegment.noOfInfants != null) $("#infantCount").val(addSegment.noOfInfants);
			$("#adultCount").disableEnable(true);
			$("#childCount").disableEnable(true);
			$("#infantCount").disableEnable(true);
			if(DATA_ResPro.reservationInfo.firstDepature == null){
				$("#firstDepature").val("");
				$("#lastArrival").val("");
			}else{
				$("#firstDepature").val(DATA_ResPro.reservationInfo.firstDepature);
				$("#lastArrival").val(DATA_ResPro.reservationInfo.firstDepature);
			}
			var resInfo = DATA_ResPro.reservationInfo;
			if(resInfo.firstDepature != null) $("#firstDepature").val(resInfo.firstDepature);
			if(resInfo.lastArrival != null) $("#lastArrival").val(resInfo.lastArrival);			
			if(addSegment.groupPNR != null && addSegment.groupPNR != 'null' && addSegment.groupPNR != ''){
				$("#selSearchOptions").val('INT');
				//$("#selSearchOptions").disableEnable(true);
			}else {
				$("#selSearchOptions").val('AA');
			}
			if(false){
// if(addSegment.ticketExpiryEnabled){
				$("#returnDate").disableEnable(true);			
				$('#returnDate').datepicker().datepicker('disable');	
				$("#returnVariance").disableEnable(true);
				
				if(addSegment.ticketExpiryDate!=null && addSegment.ticketExpiryDate!=""){					
				
					var ticketExpDateArr = addSegment.ticketExpiryDate.split("-");
					var maxDate = new Date(ticketExpDateArr[2].substring(0,4),ticketExpDateArr[1]-1,ticketExpDateArr[0]);
					
					if(addSegment.ticketValidFromDate!=null && addSegment.ticketValidFromDate!=""){
						var tckValidFromArr = addSegment.ticketValidFromDate.split("-");
						var minDate = new Date(tckValidFromArr[2].substring(0,4),tckValidFromArr[1]-1,tckValidFromArr[0]);
						
						if (CheckDates(top.strSysDate, addSegment.ticketValidFromDate)){
							$("#departureDate").datepicker( "option", "minDate",minDate );	
						}					
						
					}					
					
					if (CheckDates(top.strSysDate, addSegment.ticketExpiryDate)){
						$("#departureDate").datepicker( "option", "maxDate",maxDate );
					}
					
				}
				
				$("#ticketExpiryDate").val(addSegment.ticketExpiryDate);
				$("#ticketValidFrom").val(addSegment.ticketValidFromDate);
				$("#ticketExpiryEnabled").val(addSegment.ticketExpiryEnabled);	
			}
			
			
		}
	}
	 
	 UI_tabSearchFlights.fillAddGroundSegment = function (groundSegmentAirports){
		 if(groundSegmentAirports != null ){
			 
		 }
	 }
	
	/*
	 * Validate retrieve fare quote informations
	 */
	UI_tabSearchFlights.validateFareQuote = function(){
		UI_commonSystem.initializeMessage();
		if (!UI_tabSearchFlights.isRequote() && !UI_tabSearchFlights.isMulticity()) {
    		var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow');
    		var intRetFlt = null;
    
    		if (intOutFlt == null) {
    			showERRMessage(raiseError("XBE-ERR-06", "departure"));
    			return false;
    		}
    
    		if (UI_tabSearchFlights.strRetDate != "") {
    			intRetFlt = $("#tblInboundFlights").getGridParam('selrow');
    
    			if (intRetFlt == null) {
    				showERRMessage(raiseError("XBE-ERR-06", "return"));
    				return false;
    			}
    			if (!CheckDates(UI_tabSearchFlights.strOutDate,
    			        UI_tabSearchFlights.strRetDate)) {
    				showERRMessage(raiseError("XBE-ERR-03", "Return date",
    				        "from date"));
    				return false;
    			}
    
    			/*
    			 * Cross system check commentted
    			 */
    			/*
    			 * if(jsonOutFlts[parseInt(intOutFlt,10)-1].system !=
    			 * jsonRetFlts[parseInt(intRetFlt,10)-1].system){
    			 * showERRMessage(raiseError('XBE-ERR-76')); return false; }
    			 */
    
    			var outLastArrival = jsonOutFlts[parseInt(intOutFlt, 10) - 1].arrivalTimeList[jsonOutFlts[parseInt(
    			        intOutFlt, 10) - 1].arrivalTimeList.length - 1];
    			var inFirstDeparture = jsonRetFlts[parseInt(intRetFlt, 10) - 1].depatureTimeList[0];
    			var transitDiff = UI_tabSearchFlights._getDate(inFirstDeparture)
    			        .getTime()
    			        - UI_tabSearchFlights._getDate(outLastArrival).getTime();
    			if (transitDiff <= DATA_ResPro.systemParams.minimumTransitionTimeInMillis) {
    				showERRMessage(raiseError('XBE-ERR-27'));
    				return false;
    			}
    
    			// TODO
    			// Date Time Validations to be add
    		}
    	}
// $("#btnFQ").disable();
// $("#btnSearch").disable();
		UI_commonSystem.showProgress();
		return true;
	}
	
	/**
	 * Format yyMMddHHmm
	 */
	UI_tabSearchFlights._getDate = function(dstr){
		var i = function(str){ return parseInt(str,10);};
		return new Date(i(dstr.substr(0,2)), i(dstr.substr(2,2))-1, i(dstr.substr(4,2)), i(dstr.substr(6,2)), i(dstr.substr(8,2)),0 );
	}
	
	UI_tabSearchFlights.createSearchParams = function(varName){
		
		var pData = {};
		pData[varName+".fromAirport"]	= UI_tabSearchFlights.objResSrchParams.fromAirport;
		pData[varName+".toAirport"]		= UI_tabSearchFlights.objResSrchParams.toAirport;
		pData[varName+".departureDate"]	= UI_tabSearchFlights.objResSrchParams.departureDate;
		pData[varName+".returnDate"]	= (UI_tabSearchFlights.objResSrchParams.returnDate==null?'':UI_tabSearchFlights.objResSrchParams.returnDate);
		pData[varName+".validity"]		= UI_tabSearchFlights.objResSrchParams.validity;
		pData[varName+".adultCount"]	= UI_tabSearchFlights.objResSrchParams.adultCount;
		pData[varName+".childCount"]	= UI_tabSearchFlights.objResSrchParams.childCount;
		pData[varName+".infantCount"]	= UI_tabSearchFlights.objResSrchParams.infantCount;
		pData[varName+".classOfService"]= UI_tabSearchFlights.objResSrchParams.classOfService;
		pData[varName+".logicalCabinClass"] = UI_tabSearchFlights.objResSrchParams.logicalCabinClass;
		pData[varName+".selectedCurrency"]= UI_tabSearchFlights.objResSrchParams.selectedCurrency;
		pData[varName+".bookingType"]	= UI_tabSearchFlights.objResSrchParams.bookingType;
		pData[varName+".paxType"]		= UI_tabSearchFlights.objResSrchParams.paxType;
		pData[varName+".fareType"]		= UI_tabSearchFlights.objResSrchParams.fareType;
		pData[varName+".openReturn"]	= UI_tabSearchFlights.objResSrchParams.openReturn;
		pData[varName+".searchSystem"]	= UI_tabSearchFlights.getSelectedSystem();//UI_tabSearchFlights.objResSrchParams.searchSystem;
		pData[varName+".fromAirportSubStation"]	= UI_tabSearchFlights.objResSrchParams.fromAirportSubStation;
		pData[varName+".toAirportSubStation"]	= UI_tabSearchFlights.objResSrchParams.toAirportSubStation;
		pData[varName+".openReturnConfirm"]	= UI_tabSearchFlights.objResSrchParams.openReturnConfirm;
		pData[varName+".travelAgentCode"]	= UI_tabSearchFlights.objResSrchParams.travelAgentCode==null ? '' : UI_tabSearchFlights.objResSrchParams.travelAgentCode;
		pData[varName+".bookingClassCode"]	= UI_tabSearchFlights.objResSrchParams.bookingClassCode==null ? '' : UI_tabSearchFlights.objResSrchParams.bookingClassCode;		
		pData[varName+'.fareQuoteLogicalCCSelection'] = $.toJSON(UI_tabSearchFlights.logicalCabinClassSelection);
		pData[varName+'.fareQuoteOndSegLogicalCCSelection'] = $.toJSON(UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection); 
		pData[varName+'.fareQuoteSegWiseLogicalCCSelection'] = $.toJSON(UI_tabSearchFlights.segWiseLogicalCabinClassSelection);
		pData[varName+'.fareQuoteOndSegBookingClassSelection'] = $.toJSON(UI_tabSearchFlights.ondSegWiseBookingClassSelection);
		pData[varName+'.ondQuoteFlexiStr'] = $.toJSON(UI_tabSearchFlights.ondWiseFlexiQuote);
		pData[varName+'.ondAvailbleFlexiStr'] = $.toJSON(UI_tabSearchFlights.ondWiseFlexiAvailable);
		pData[varName+'.ondSelectedFlexiStr'] = $.toJSON(UI_tabSearchFlights.ondWiseFlexiSelected);
		pData[varName+'.promoCode'] = UI_tabSearchFlights.objResSrchParams.promoCode;
		pData[varName+'.bankIdentificationNo'] = UI_tabSearchFlights.objResSrchParams.bankIdentificationNo;
		pData[varName+'.preferredBundledFares'] = $.toJSON(UI_tabSearchFlights.ondWiseBundleFareSelected);
		pData[varName+'.preferredBookingCodes'] = $.toJSON(UI_tabSearchFlights.ondWiseBookingClassSelected);
		pData[varName+'.ondSegBookingClassStr'] = $.toJSON(UI_tabSearchFlights.segWiseBookingClassOverride);		
		pData[varName+'.allowOverrideSameOrHigherFare'] = DATA_ResPro.initialParams.allowOverrideSameOrHigherFare;		
		pData[varName+'.ondWiseCitySearchStr'] = $.toJSON(UI_tabSearchFlights.ondWiseCitySearch);
		
		// Add exclude Charges
		UI_tabSearchFlights.composeExcludeChargesData(pData, varName);
		
		return pData;
	}
	
	/**
	 * Add exclude charge data
	 * 
	 * */
	UI_tabSearchFlights.composeExcludeChargesData = function(pData, varName){
		
		if(DATA_ResPro.initialParams.allowExcludeCharges){
			var excludedCharges = UI_tabSearchFlights.objResSrchParams.excludeCharge;
			if((typeof(excludedCharges) != 'undefined') && excludedCharges != null){
				for ( var i = 0; i < excludedCharges.length; i++) {
					pData[varName+'.excludeCharge[' + i + ']'] = excludedCharges[i];
				}
			}
		}
		
		return pData;
	}
	
	UI_tabSearchFlights.jsonOndFQParams = function(){
		var pData = {
				adultCount		: UI_tabSearchFlights.objResSrchParams.adultCount,
				childCount  	: UI_tabSearchFlights.objResSrchParams.childCount,
				infantCount 	: UI_tabSearchFlights.objResSrchParams.infantCount,
				cabinClass		: UI_tabSearchFlights.objResSrchParams.classOfService,
				bookingClass	:UI_tabSearchFlights.objResSrchParams.bookingClassCode,
				logicalCabinClass: UI_tabSearchFlights.objResSrchParams.logicalCabinClass,
				bookingType		: UI_tabSearchFlights.objResSrchParams.bookingType
		};
//		pData[varName+'.fareQuoteLogicalCCSelection'] = $.toJSON(UI_tabSearchFlights.logicalCabinClassSelection);
		return pData;
	}
	
	UI_tabSearchFlights.inOutFlightRPHList = function(){
		var fData = {};
		if(!UI_tabSearchFlights.isRequote() && !UI_tabSearchFlights.isMulticity()){
    		var intOutFlt  = 0;
    		
    		// if( UI_commonSystem.strPGMode != "confirmOprt"){
    			intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
    		// }
    		// set the flight ref numbers
    		for(var i=0; i<jsonOutFlts[intOutFlt].flightRPHList.length; i++) {
    			// this will generate params like outFlights[i]=123123231
    			fData["outFlightRPHList[" + i + "]"]= jsonOutFlts[intOutFlt].flightRPHList[i];
    			//Check wailisting stutus in at least one segment
    			if(UI_tabSearchFlights.flightRPHWaitListingStatus[jsonOutFlts[intOutFlt].flightRPHList[i]]){
    				UI_tabSearchFlights.waitListedSegmentAvailable = true;
    			}
    		}
    		var outDate = jsonOutFlts[intOutFlt].flightRPHList[0].split("$")[3].substr(0,8);
    	    UI_tabSearchFlights.selectedOutDate = outDate.substring(6, 8) + "/" + outDate.substring(4, 6) +  "/" + outDate.substring(0, 4);
    		
    		var retSystem = "";
    		if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode != "confirmOprt"){
    			var intRetFlt  = $("#tblInboundFlights").getGridParam('selrow') - 1;
    			retSystem = jsonRetFlts[intRetFlt].system;
    			for(var i=0; i<jsonRetFlts[intRetFlt].flightRPHList.length; i++) {
    				// this will generate params like retFlights[i]=123123231
    				fData["retFlightRPHList[" + i + "]"]= jsonRetFlts[intRetFlt].flightRPHList[i];
    				//Check wailisting stutus in at least one segment
        			if(UI_tabSearchFlights.flightRPHWaitListingStatus[jsonRetFlts[intRetFlt].flightRPHList[i]]){
        				UI_tabSearchFlights.waitListedSegmentAvailable = true;
        			}
    			}
    			var retDate = jsonRetFlts[intRetFlt].flightRPHList[0].split("$")[3].substr(0,8);
    			UI_tabSearchFlights.selectedRetDate = retDate.substring(6, 8) + "/" + retDate.substring(4, 6) +  "/" + retDate.substring(0, 4);
    		}
    		
    		if (jsonOutFlts[intOutFlt].system == "INT" || retSystem == "INT") {
    			fData["fareQuoteParams.searchSystem"] = "INT";
    		}
    		else {
    			fData["fareQuoteParams.searchSystem"] = "AA";
    		}
		}
		
		return fData;
	}
	
	UI_tabSearchFlights.bagageRatesInOutFlightRPHList = function(){
		var fData = {};
		if(!UI_tabSearchFlights.isMulticity()){
    		var intOutFlt  = 0;
    			intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
    		for(var i=0; i<jsonOutFlts[intOutFlt].flightRPHList.length; i++) {
    			fData["outFlightRPHList[" + i + "]"]= jsonOutFlts[intOutFlt].flightRPHList[i];
    		}
    		
    		if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode != "confirmOprt"){
    			var intRetFlt  = $("#tblInboundFlights").getGridParam('selrow') - 1;
    			retSystem = jsonRetFlts[intRetFlt].system;
    			for(var i=0; i<jsonRetFlts[intRetFlt].flightRPHList.length; i++) {
    				fData["retFlightRPHList[" + i + "]"]= jsonRetFlts[intRetFlt].flightRPHList[i];
    			}
    		}
		}
		
		return fData;
	}
	
	/*
	 * Fare Quote Params
	 */
	UI_tabSearchFlights.createFareQuoteParams = function(){
		if(DATA_ResPro.blnIsFromNameChange || DATA_ResPro.blnIsFromNameChange=="true"){
			var fData = {};
			if (DATA_ResPro.reservationInfo.isInterline) {
    			fData["fareQuoteParams.searchSystem"] = "INT";
    		}
    		else {
    			fData["fareQuoteParams.searchSystem"] = "AA";
    		}
			fData["fareQuoteParams.selectedCurrency"] = $("#selectedCurrency").val();
			fData["fareQuoteParams.adultCount"] = DATA_ResPro.reservationInfo.noOfAdults; 
			fData["fareQuoteParams.childCount"] = DATA_ResPro.reservationInfo.noOfChildren;
			fData["fareQuoteParams.infantCount"] = DATA_ResPro.reservationInfo.noOfInfants;
			fData["fareQuoteParams.fromNameChange"] = true;		
			fData["fareQuoteParams.allowOverrideSameOrHigherFare"] = DATA_ResPro.initialParams.allowOverrideSameOrHigherFare;
		} else {
			var fData = UI_tabSearchFlights.createSearchParams('fareQuoteParams');
		}
		
		fData = $.airutil.dom.concatObjects(fData, UI_tabSearchFlights.inOutFlightRPHList());
		UI_tabSearchFlights.createSubmitData({
			data: fData,
			mode: UI_commonSystem.strPGMode
		});
		
		if(DATA_ResPro["modifyFlexiBooking"] != undefined &&
				DATA_ResPro["modifyFlexiBooking"]){
			var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);
			$.each(ondFlexiQuote, function(key, value){
				ondFlexiQuote[key] = false;
			});
			fData['fareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(ondFlexiQuote);
			fData['fareQuoteParams.ondWiseCitySearchStr'] = $.toJSON(UI_tabSearchFlights.ondWiseCitySearch);
		}
		
		return fData;
	}
	
	UI_tabSearchFlights.isReturn = function(){
		if (UI_tabSearchFlights.strRetDate != ""){
			return true;
		}else{
			return false;
		}
	}
	
	UI_tabSearchFlights.isFlexiSelected = function(){
		return $.toJSON(UI_tabSearchFlights.ondWiseFlexiSelected);
	}
	
	UI_tabSearchFlights.getSelectedRPHList = function(){
		var intOutFlt  = $("#tblOutboundFlights").getGridParam('selrow') - 1;;
		var intRetFlt  = null;	
		var rphList = jsonOutFlts[intOutFlt].flightRPHList;
		
		
		if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode != "confirmOprt"){
			intRetFlt  = $("#tblInboundFlights").getGridParam('selrow') - 1;
			rphList = rphList.concat(jsonRetFlts[intRetFlt].flightRPHList);
		}
		return rphList;
	}
	
	UI_tabSearchFlights.updateFlightCabinFromChangeFare = function(chgArr){
		if(UI_tabSearchFlights.flightInfo != null && '' != UI_tabSearchFlights.flightInfo
				&& UI_tabSearchFlights.flightInfo.length > 0){
			for(var i=0; i < UI_tabSearchFlights.flightInfo.length ; i++){
				for(var j = 0 ; j < chgArr.length; j++){
					if(UI_tabSearchFlights.flightInfo[i].flightRefNumber== chgArr[j].fltRefNo){
						UI_tabSearchFlights.flightInfo[i].cabinClass = chgArr[j].ccCode;
					}
				}
			}
		}
		
	}
	
	UI_tabSearchFlights.fillFareQuoteData = function(data){
		if(data.showFareRules){
			UI_tabSearchFlights.showFareRuleAlert = true;
			UI_tabSearchFlights.fareRulesInfo = data.fareRulesInfo;
			$("#btnFareRulesB").show();
		} else{
			UI_tabSearchFlights.showFareRuleAlert = false;
			$("#btnFareRulesB").hide();
		}
		UI_tabSearchFlights.flexiFareRulesInfo = data.flexiFareRulesInfo;
		jsonFareQuote = data.fareQuoteTO;
		jsonFareQuoteSummary = data.fareQuoteSummaryTO;
		UI_tabSearchFlights.jsonFareDiscountInfo = data.fareDiscountInfo;
		UI_tabSearchFlights.jsonBDSurcharge = data.surchargeTOs;
		UI_tabSearchFlights.jsonBDTax = data.taxTOs;
		jsonPaxPrice = data.paxFareTOs;
		jsonPrice = data.paxPriceTOs;

		/* Fare quote Details */
		UI_tabSearchFlights.buildFareQuoteDetails();

		/* Passenger information */
		
		
		if (UI_commonSystem.strPGMode == ""){
			UI_tabPassenger.updatePaxArrays(UI_tabSearchFlights.cachePassengers(data));
		}else {
			$("#trhnglCharge").hide();
		}
	}	
	
	/*
	 * Fare Quote Informations
	 */
	UI_tabSearchFlights.returnCallShowFareQuoteInfo = function(response){

		if (UI_commonSystem.strPGMode != "cancelSegment") {
			$("#btnBook").hide();
			$("#btnBook").disable();	
		}
		response = $.extend({prefDateSelected:true}, response);
		
		if(UI_tabSearchFlights.isMulticity()|| UI_tabSearchFlights.isRequote()){
			var ondListString = response.ondListString;
			var flightRPHListobj = JSON.parse(ondListString);
			if(flightRPHListobj != null){
				UI_tabSearchFlights.multiCityRequoteFightRPHList = flightRPHListobj;
			}
		}
				
		UI_tabSearchFlights.displayBasedOnTemplates = response.displayBasedOnTemplates;
		
		UI_commonSystem.hideProgress();
		UI_tabSearchFlights.disablePageControls();
		UI_tabSearchFlights.isOpenReturn = false;
		UI_tabSearchFlights.openReturnInfo = null;
		var hasFares = false;
		if(response.success) {
			$("#trFareSection").show();
			UI_tabSearchFlights.createLogicalCabinAvailability(response);			
			UI_tabSearchFlights.strBkgClassDesc = $("#classOfService option:selected").text().replace('--', '');
			UI_tabSearchFlights.isOpenReturn = response.openReturn;			
			UI_tabSearchFlights.openReturnInfo = response.openReturnInfo;
			if(response.overrideFareDiscount != undefined){
				UI_tabSearchFlights.overrideFareDiscount = response.overrideFareDiscount;	
			}
			if(UI_tabSearchFlights.overrideFareDiscount){
				$('#btnFareDiscountRemove').show();
			} else {
				$('#btnFareDiscountRemove').hide();
			}
			
			if(UI_tabSearchFlights.isOpenReturn){				
				UI_commonSystem.fillGridData({id:"#tblOpenReturnInfo", data:[response.openReturnInfo]})
				$('#tblOpenReturnInfo').show();				
				$('#divOpenRetPane').show();
			}else{
				$('#divOpenRetPane').hide();
			}
			UI_tabSearchFlights.IntCommentShowHide();
		
			UI_tabSearchFlights.flightInfo = response.flightInfo;
			UI_tabSearchFlights.jsonFlightInfo = $.toJSON(UI_tabSearchFlights.flightInfo);
			
			if(response.subJourneyGroupMap != undefined){
				UI_tabSearchFlights.fltSegIdByJourneyMap = response.subJourneyGroupMap;
			}
			if(response.segmentsMapPerSegType != undefined){
				UI_tabSearchFlights.segmentsMapPerSegType = response.segmentsMapPerSegType;
			}
			
			if(response.busCarrierCodes!=undefined && response.busCarrierCodes!=null 
					&& response.busCarrierCodes.length > 0) {
				UI_tabSearchFlights.busCarrierCodes = response.busCarrierCodes;
			}
			
			UI_tabSearchFlights.setAvailableBundleFareLCClassStr(response.ondLogicalCCList); 
			
			if(UI_tabSearchFlights.fareAvailable(response.availableFare)) {		
				
				var strHTMLText = "&nbsp;Fare quote" ;
				
				hasFares = true;
				
				if(!UI_tabSearchFlights.isRequote() && !UI_tabSearchFlights.isMulticity()){
					strHTMLText += " - Outgoing " + UI_tabSearchFlights.strOutDateFmt ;
					if (UI_tabSearchFlights.strRetDateFmt != ""){
						strHTMLText += " - Return " + UI_tabSearchFlights.strRetDateFmt;
					}
					strHTMLText += " - " + UI_tabSearchFlights.strBkgClassDesc; 
				}
				
				UI_tabSearchFlights.updateFareQuoteHeaderSummary();
				// TODO this value should come from server
			
				$("#divFareQuotePane").show();
				$("#btnFQ").disable();
				$("#btnBook").show();
				$("#btnBook").enable();			
								
				$("#tdResultPane").scrollTop($("#tdResultPane").scrollTop() + parseInt($("#tdResultPane").css("height"),10))
				
				UI_tabSearchFlights.availableFare = response.availableFare;
				UI_tabSearchFlights.showFareRuleInfoInSummary=response.availableFare.showFareRuleInfoInSummary;
				
				/* Set recall Data from Server */
				UI_tabSearchFlights.pgObj.tabDataRetrive.tab1 = true;
								
				currencyRound = response.currencyRound;				
				showFareDiscountCodes = response.showFareDiscountCodes;
				
				if(response.showAgentCommission){
					$('#trAgentCommission').show();
					var totAgentCom = response.availableFare.totAgentCommission;
					$("#divAgentCom").html(totAgentCom);
					$("#divAgentComCurr").html(response.availableFare.fareQuoteSummaryTO.currency);
				   
					if(totAgentCom != "" && parseFloat(totAgentCom) > 0){
						UI_tabSearchFlights.isAgentCommissionApplied = true;
					} 
				}
				
				if (showFareDiscountCodes) {
					$("#selDiscountType").empty();
					$("#selDiscountType").fillDropDown({ dataArray:response.fareDiscountCodeList , keyIndex:0 , valueIndex:1, firstEmpty: true });
				}
				UI_tabSearchFlights.isDomFareDiscountApplied = response.domFareDiscountApplied;
				UI_tabSearchFlights.appliedFareDiscountInfo = response.appliedFareDiscount;
				UI_tabSearchFlights.dummyCreditDiscount = response.dummyCreditDiscountAmount;
				UI_tabSearchFlights.fillFareQuoteData(response.availableFare);
				
				if (UI_commonSystem.strPGMode == ""){
					UI_tabPassenger.updatePassengerTabTO(response);
				}else {
					$("#trhnglCharge").hide();
				}
				var carrierList =  UI_tabSearchFlights.buildCarrierList(response.flightInfo);
				UI_tabSearchFlights.isAllowChangeFare = UI_tabSearchFlights.isChangeFareAllowed(carrierList) && response.availableFare.allRequestedOndsHasFares;
				
				if(UI_tabSearchFlights.isAllowChangeFare){
					$("#btnCFare").show();
					$("#btnCAllFare").show();
				} else {
					$("#btnCFare").hide();
					$("#btnCAllFare").hide();
				}				
				
				if(response["isChangeFare"] == null){
					if(response.rpParams!=null && response.rpParams != undefined && !jQuery.isEmptyObject(response.rpParams)){
						DATA_ResPro.initialParams = response.rpParams;
					}					
					
				}
				UI_tabSearchFlights.checkPrivileges();
				
				if(UI_tabSearchFlights.isRequote()){
					UI_requote.setDisplayViewAllBCButton();
					$("#btnShowLogicalOptions").show();
					UI_requote.bindGridActions();
				}
				$(".PBRKDown").show();
				
				if(UI_tabSearchFlights.isMulticity() && !response.availableFare.allRequestedOndsHasFares) {
					$("#btnBook").hide();
					showCommonError("WARNING",raiseError('XBE-ERR-92'));
				}
			} else {
				//UI_tabSearchFlights.clearFareQuote();
				UI_tabSearchFlights.clearFareQuoteDataOnly();
				UI_tabSearchFlights.checkPrivileges();
				
				if(!UI_tabSearchFlights.isRequote() && !UI_tabSearchFlights.isMulticity()){
    				var intOutFlt  = $("#tblOutboundFlights").getGridParam('selrow') - 1;				
    				if (jsonOutFlts != null && jsonOutFlts[intOutFlt] != null && jsonOutFlts[intOutFlt].length > 0){
    					$("#trFareSection").hide();					
    				}
    				var showNoFareError = false;
    				if (UI_tabSearchFlights.strRetDate != ""){
    					// intRetFlt =
    					// $("#tblInboundFlights").getGridParam('selrow') - 1;
    					if(jsonOutFlts!=null && jsonOutFlts.length >0 && jsonRetFlts!=null && jsonRetFlts.length> 0 ){
    						showNoFareError = true;
    					}
    				}else if(jsonOutFlts!=null && jsonOutFlts.length >0){
    					showNoFareError = true;
    				}
				} else {
					UI_requote.setDisplayViewAllBCButton();
					showNoFareError = true;
					
					var wrongFlightsError = false;
					
					if(!UI_tabSearchFlights.isRequote()){
						var flightRPHObjects = $.parseJSON(UI_tabSearchFlights.fltRPHInfo);
						var segmentCode1 , segmentCode2;
						
						if(flightRPHObjects.length >= 2){
							for(i = 0 ; i < flightRPHObjects.length-1 ; i++){
								segmentCode1 = flightRPHObjects[i].segmentCode.split('/');
								segmentCode2 = flightRPHObjects[i+1].segmentCode.split('/');
								if(segmentCode1[1] == segmentCode2[0] && flightRPHObjects[i].arrivalTime > flightRPHObjects[i+1].departureTime){
									wrongFlightsError = true;
									break;
								}
							}
						}
					}
															
					$("#divFareQuotePane").hide();
				}
				
				if(wrongFlightsError){
					showCommonError("WARNING",raiseError('XBE-ERR-99'));
				}else if(showNoFareError){
					showCommonError("WARNING",raiseError('XBE-ERR-75'));
				} else {
					showCommonError("WARNING",raiseError('XBE-ERR-77'));
				}	
				if(DATA_ResPro.blnIsFromNameChange){
					UI_modifyResRequote.loadHomePage();
				}
			}			

			UI_tabSearchFlights.airportMessage = response.airportMessage;
			UI_tabSearchFlights.loadAirportMessage();

		}else {
			UI_tabSearchFlights.clearFareQuote();
			showERRMessage(response.messageTxt);
			
			if(DATA_ResPro.blnIsFromNameChange){
				UI_modifyResRequote.loadHomePage();
			}
			
			if(UI_tabSearchFlights.isRequote()){
				$("#btnShowLogicalOptions").hide();
				$("#btnReset").click();
			}
		}
		if(response.changeFareQuoteParams == undefined || response.changeFareQuoteParams == null ){
			UI_tabSearchFlights.ondwiseFlexiFare = response.ondWiseTotalFlexiCharge;
			UI_tabSearchFlights.ondWiseFlexiAvailable = UI_tabSearchFlights.convertArrayToMap(response.ondWiseTotalFlexiAvailable);
		}
		if(DATA_ResPro.blnIsFromNameChange && hasFares){
			UI_requote.balanceQuery();
		}
		
		if($.inArray($('#fromAirport').val(), top.gstMessageEnabledAirports) != -1){
			$("#gstMessage").html(top.gstMessage);
			$("#divGstMessageAlert").show();
		}
		
	}
	
	
	UI_tabSearchFlights.isChangeFareAllowed = function(carrierList){
		if(UI_tabSearchFlights.getSelectedSystem() == "AA"){
			return true;
		} else {
			
			var filteredCarrierCodeList = [];
			var changeFareAllowed = false;
			if(carrierList.length > 1) {
				if(UI_tabSearchFlights.busCarrierCodes.length > 0) {
					for(var carrierIndex = 0 ; carrierIndex < carrierList.length ; carrierIndex ++){
						for(var busCarrierCodeIndex = 0 ;busCarrierCodeIndex < UI_tabSearchFlights.busCarrierCodes.length; busCarrierCodeIndex++){
							if(carrierList[carrierIndex] != UI_tabSearchFlights.busCarrierCodes[busCarrierCodeIndex]) {
								var busCarrierImage = '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">';
								
								if(carrierList[carrierIndex] !=  busCarrierImage){
									filteredCarrierCodeList.push(carrierList[carrierIndex]);
								}
							}
						}
					}
					carrierList = filteredCarrierCodeList;
				}
				
				if(carrierList.length > 1) {
					changeFareAllowed = UI_tabSearchFlights.isOnlyDry(carrierList);
				} else {
					changeFareAllowed = true;
				}
					
			} else {
				changeFareAllowed = true;
			}
			
			return changeFareAllowed;
		}
	}
	
	UI_tabSearchFlights.buildCarrierList =  function(flightInfo){
		var carrierList  = []; 
		if(jsonOutFlts && !UI_tabSearchFlights.isRequote()){
			var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
			carrierList = jsonOutFlts[intOutFlt].carrierList;
			if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt") {
				var intRetFlt = $("#tblInboundFlights").getGridParam('selrow') - 1;
				var retCarrierList = jsonRetFlts[intRetFlt].carrierList;        
            
				carrierList = carrierList.concat(retCarrierList);
			}
			return carrierList;
		}else {
			$.each(flightInfo,function(key,objFlightInfo){
				carrierList.push(objFlightInfo.airLine);
			});
			return carrierList;
		}
	}
	
	UI_tabSearchFlights.isOnlyDry = function(cList){
		var c = "";
		for(var i=0;i<cList.length;i++){
			for ( var j = 0; j < cList.length; j++) {
				if(i == j){
					continue;
				} else {
					if(cList[i] != cList[j]){
						return false;
					}
				}
			}
		}
		return true;
	}
	
	UI_tabSearchFlights.checkDryforBusSegment = function(cList){
		var c = "";
		var busSegmentId = '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">';
		for(var i=0;i<cList.length;i++){
			if(c == "" && cList[i] !=busSegmentId){
			    c = cList[i];
			} else if (c != cList[i] && cList[i] !=busSegmentId){
			    return false;
			}
		}
		return true;
	}
	
	UI_tabSearchFlights.cachePassengers = function(response) {		
		if(response.paxAdults.length < jsonPaxAdtObj.paxAdults.length){
			jsonPaxAdtObj.paxAdults.length = response.paxAdults.length;
		}
		
		for(var al=0; al< jsonPaxAdtObj.paxAdults.length;al++) {
			response.paxAdults[al].displayAdultTitle = jsonPaxAdtObj.paxAdults[al].displayAdultTitle;
			response.paxAdults[al].displayAdultFirstName =  jsonPaxAdtObj.paxAdults[al].displayAdultFirstName;
			response.paxAdults[al].displayAdultLastName =   jsonPaxAdtObj.paxAdults[al].displayAdultLastName;
			response.paxAdults[al].displayAdultNationality =  jsonPaxAdtObj.paxAdults[al].displayAdultNationality;
			response.paxAdults[al].displayAdultDOB =   jsonPaxAdtObj.paxAdults[al].displayAdultDOB;
			response.paxAdults[al].displayNationalIDNo =   jsonPaxAdtObj.paxAdults[al].displayNationalIDNo;
			response.paxAdults[al].displayPnrPaxCatFOIDNumber =   jsonPaxAdtObj.paxAdults[al].displayPnrPaxCatFOIDNumber;
			response.paxAdults[al].displayPnrPaxCatFOIDExpiry =   jsonPaxAdtObj.paxAdults[al].displayPnrPaxCatFOIDExpiry;
			response.paxAdults[al].displayPnrPaxCatFOIDPlace =   jsonPaxAdtObj.paxAdults[al].displayPnrPaxCatFOIDPlace;
			response.paxAdults[al].displayPnrPaxPlaceOfBirth =   jsonPaxAdtObj.paxAdults[al].displayPnrPaxPlaceOfBirth;
			response.paxAdults[al].displayTravelDocType =   jsonPaxAdtObj.paxAdults[al].displayTravelDocType;
			response.paxAdults[al].displayVisaDocNumber =   jsonPaxAdtObj.paxAdults[al].displayVisaDocNumber;
			response.paxAdults[al].displayVisaDocPlaceOfIssue =   jsonPaxAdtObj.paxAdults[al].displayVisaDocPlaceOfIssue;
			response.paxAdults[al].displayVisaDocIssueDate =   jsonPaxAdtObj.paxAdults[al].displayVisaDocIssueDate;
			response.paxAdults[al].displayVisaApplicableCountry =   jsonPaxAdtObj.paxAdults[al].displayVisaApplicableCountry;
			response.paxAdults[al].displayFFID =   jsonPaxAdtObj.paxAdults[al].displayFFID;
		}
		
		if(response.paxInfants != null && jsonPaxAdtObj.paxInfants != null && response.paxInfants.length > 0) {	
			if(response.paxInfants.length < jsonPaxAdtObj.paxInfants.length){
				jsonPaxAdtObj.paxInfants.length = response.paxInfants.length;
			}
			
			for(var al=0; al< jsonPaxAdtObj.paxInfants.length;al++) {
				response.paxInfants[al].displayInfantFirstName =  jsonPaxAdtObj.paxInfants[al].displayInfantFirstName;
				response.paxInfants[al].displayInfantLastName =   jsonPaxAdtObj.paxInfants[al].displayInfantLastName;
				response.paxInfants[al].displayInfantNationality =  jsonPaxAdtObj.paxInfants[al].displayInfantNationality;
				response.paxInfants[al].displayInfantDOB =   jsonPaxAdtObj.paxInfants[al].displayInfantDOB;
				response.paxInfants[al].displayInfantTravellingWith =   jsonPaxAdtObj.paxInfants[al].displayInfantTravellingWith;
				response.paxInfants[al].displayPnrPaxCatFOIDNumber =   jsonPaxAdtObj.paxInfants[al].displayPnrPaxCatFOIDNumber;
				response.paxInfants[al].displayPnrPaxCatFOIDNumber =   jsonPaxAdtObj.paxInfants[al].displayPnrPaxCatFOIDNumber;
				response.paxInfants[al].displayPnrPaxCatFOIDExpiry =   jsonPaxAdtObj.paxInfants[al].displayPnrPaxCatFOIDExpiry;
				response.paxInfants[al].displayPnrPaxCatFOIDPlace =   jsonPaxAdtObj.paxInfants[al].displayPnrPaxCatFOIDPlace;
				response.paxInfants[al].displayPnrPaxPlaceOfBirth =   jsonPaxAdtObj.paxInfants[al].displayPnrPaxPlaceOfBirth;
				response.paxInfants[al].displayTravelDocType =   jsonPaxAdtObj.paxInfants[al].displayTravelDocType;
				response.paxInfants[al].displayVisaDocNumber =   jsonPaxAdtObj.paxInfants[al].displayVisaDocNumber;
				response.paxInfants[al].displayVisaDocPlaceOfIssue =   jsonPaxAdtObj.paxInfants[al].displayVisaDocPlaceOfIssue;
				response.paxInfants[al].displayVisaDocIssueDate =   jsonPaxAdtObj.paxInfants[al].displayVisaDocIssueDate;
				response.paxInfants[al].displayVisaApplicableCountry =   jsonPaxAdtObj.paxInfants[al].displayVisaApplicableCountry;
			}
		}
		return  response;		
	}
	
	UI_tabSearchFlights.clearFareQuote = function(){
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		$("#divFareQuotePane").hide();
		$("#trFareSection").hide();
		jsonFareQuote = null;
		jsonFareQuoteSummary = null;
		UI_tabSearchFlights.jsonBDSurcharge = null;
		UI_tabSearchFlights.jsonBDTax = null;
		
		// clear fare discount stuff
		UI_tabSearchFlights.fareDiscount.percentage = null;
		UI_tabSearchFlights.fareDiscount.notes = null;
		UI_tabSearchFlights.fareDiscount.code = null;
		
		UI_tabSearchFlights.clearPopulatedFareDisc();
	}
	
	UI_tabSearchFlights.clearFareQuoteDataOnly = function(){
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}	
		jsonFareQuote = null;
		jsonFareQuoteSummary = null;
		UI_tabSearchFlights.jsonBDSurcharge = null;
		UI_tabSearchFlights.jsonBDTax = null;	
		//FIXME me uncomment when the method is available
//		UI_tabSearchFlights.clearFlexiSelection();
		
		// clear fare discount stuff
		UI_tabSearchFlights.fareDiscount.percentage = null;
		UI_tabSearchFlights.fareDiscount.notes = null;
		UI_tabSearchFlights.fareDiscount.code = null;
		UI_tabSearchFlights.clearPopulatedFareDisc();
	}
	
	UI_tabSearchFlights.clearAvilability = function(){
		if(!UI_tabSearchFlights.isRequote()){
			$("#divResultsPane").hide();
		} else {		
			$("#tblIBOBFlights").hide();
			$("#trAddOND").hide();
		}
		$("#tblOutboundFlights").hide();
		$("#tblInboundFlights").hide();
		$('#tblOpenReturnInfo').hide();
		jsonOutFlts = null;
		jsonRetFlts = null;
	}
	
	UI_tabSearchFlights.clearFlexiSelection = function(){
		$('#addOutFlexi').attr('checked', false);
		$('#addInFlexi').attr('checked', false);
		$.each(UI_tabSearchFlights.ondWiseFlexiSelected, function(key, val){
			UI_tabSearchFlights.ondWiseFlexiSelected[key] = false;
		});
	}

	UI_tabSearchFlights.clearAll = function(){
		UI_tabSearchFlights.clearFareQuote();
		UI_tabSearchFlights.clearAvilability();
		UI_tabSearchFlights.clearLogicalCCData();
		UI_tabSearchFlights.hideAirportMessage();
		$("#btnBook").hide();
	}
	
	/*
	 * Fare Quote Break Down
	 */
	UI_tabSearchFlights.buildFareQuoteDetails = function(){
		UI_tabSearchFlights.pgObj.lockTabs();
		// Map the Currency to the DTO
		if (jsonFareQuoteSummary.currency == ""){jsonFareQuoteSummary.currency = UI_tabSearchFlights.objResSrchParams.selectedCurrency;}
		
		if (jsonFareQuoteSummary.selectedCurrency == ""){jsonFareQuoteSummary.selectedCurrency = UI_tabSearchFlights.objResSrchParams.selectedCurrency;}
		
		/* Header */
		if(currencyRound){
			$("#lblHndChge").text("Handling Charge + Currency Rounding Diff");
			$("#divHDChg").html(jsonFareQuoteSummary.totalHandlingCharge + " + " + jsonFareQuoteSummary.totalCurrencyRoundUpDiff);
		}else {
			$("#lblHndChge").text("Handling Charge");
			$("#divHDChg").html(jsonFareQuoteSummary.totalHandlingCharge);
		}
		if(jsonFareQuoteSummary.adminFee !== null && jsonFareQuoteSummary.adminFee !== ""){
			$("#lblAdminFee").text("Admin Fee");			
			$("#divADFeeAmount").html(jsonFareQuoteSummary.adminFee);
		} else {
			$("#trAdminFee").hide();
		}
		$("#divADFeeCurr").html(jsonFareQuoteSummary.currency);
		
		$("#divHDTxnCurr").html(jsonFareQuoteSummary.currency);
		UI_tabSearchFlights.clearPopulatedFareDisc();
		var discInfoJson = UI_tabSearchFlights.appliedFareDiscountInfo;
		
		if(!UI_tabSearchFlights.isDomFareDiscountApplied && UI_tabSearchFlights.jsonFareDiscountInfo != null 
				&& UI_tabSearchFlights.jsonFareDiscountInfo.fareDiscountAvailable && top.arrPrivi[PRIVI_ADD_FARE_DISCOUNT]==1){
		
			
			if(discInfoJson!=null 
					&& discInfoJson.oldFareDiscountApplied 
					&& jsonFareQuoteSummary.totalFareDiscount!=null 
					&& jsonFareQuoteSummary.totalFareDiscount!='0.00'){
				
				$("#btnDiscountPop").show();
				$("#fareDiscountPercentage").show();
				$("#btnDiscountPop").text("Change");
				if(UI_tabSearchFlights.overrideFareDiscount){
					$("#btnFareDiscountRemove").show();
				} else {
					$("#btnFareDiscountRemove").hide();	
				}
				$("#btnFareDiscountApply").show();
				$("#lblFareDiscountAvail").text("Fare Discountable Range - [ " + UI_tabSearchFlights.jsonFareDiscountInfo.fareDiscountMin + 
						" % - "+ UI_tabSearchFlights.jsonFareDiscountInfo.fareDiscountMax+" %]");
				
				UI_tabSearchFlights.fareDiscount.percentage = discInfoJson.farePercentage;
				UI_tabSearchFlights.fareDiscount.notes = discInfoJson.notes;
				UI_tabSearchFlights.fareDiscount.code = discInfoJson.code;
				
				$("#lblFareDiscounted").text("Fare Discount ");
				$("#lblDiscountApplied").text(UI_tabSearchFlights.fareDiscount.percentage);
				$("#divFareDiscVal").html(jsonFareQuoteSummary.totalFareDiscount);
				$('#trFareDiscount').show();
				
			}else{
				$("#btnDiscountPop").show();
				$("#fareDiscountPercentage").show();
				$("#btnFareDiscountApply").show();
				$("#lblFareDiscountAvail").text("Fare Discountable Range - [ " + UI_tabSearchFlights.jsonFareDiscountInfo.fareDiscountMin + 
						" % - "+ UI_tabSearchFlights.jsonFareDiscountInfo.fareDiscountMax+" %]");
				$("#lblFareDiscounted").text("Fare Discount ");
				$("#divFareDiscVal").html(jsonFareQuoteSummary.totalFareDiscount);
				$('#trFareDiscount').show();
			}
			$('#trFareDiscountCredit').hide();
			
		} else if(UI_tabSearchFlights.isDomFareDiscountApplied && jsonFareQuoteSummary.totalFareDiscount!=null 
				&& jsonFareQuoteSummary.totalFareDiscount!='0.00'){	
			$('#trFareDiscount').show();
			$("#fareDiscountPercentage").hide();
			$("#btnFareDiscountApply").hide();
			$("#btnDiscountPop").hide();
			$("#btnFareDiscountRemove").hide();
			$("#lblFareDiscountAvail").text("Domestic Fare Discount");
//			$("#lblDiscountApplied").text("50");
			$("#lblDiscountApplied").text(jsonFareQuoteSummary.domFareDiscountPerc);			
			$("#divFareDiscVal").html(jsonFareQuoteSummary.totalFareDiscount);
			$('#trFareDiscountCredit').hide();
		} else if(discInfoJson != null && discInfoJson.promotionId != null){
			var discountApplied = discInfoJson.description;
			//if discription is empty, replace with promotion name
			if(discountApplied == null || discountApplied == ''){
				discountApplied = discInfoJson.notes;
			} else {
				var discBannerAmt = '';
				if(discInfoJson.discountType == UI_tabSearchFlights.promoDiscountType.PERCENTAGE){
					discBannerAmt = discInfoJson.farePercentage + '%';				
				} else if(discInfoJson.discountType == UI_tabSearchFlights.promoDiscountType.VALUE){
					discBannerAmt = jsonFareQuoteSummary.currency + ' ' + discInfoJson.farePercentage;				
				}
				
				discountApplied = discountApplied.replace("{amount}", discBannerAmt);
				discountApplied = discountApplied.replace("{Amount}", discBannerAmt);
			}
			
			var discountAmount = "0.00";
			var discSign = '';
			var discountLabel = '';
			if(discInfoJson.discountAs == UI_tabSearchFlights.promoDiscountAs.MONEY){
				discSign = '-';
				discountLabel = "PROMOTION DISCOUNT";
			} else {
				discountLabel = "CREDIT FOR FUTURE USAGE";
			}
			
			if(jsonFareQuoteSummary.totalFareDiscount!=null && jsonFareQuoteSummary.totalFareDiscount!='0.00'){
				discountAmount = discSign + jsonFareQuoteSummary.totalFareDiscount;
			} else if(UI_tabSearchFlights.dummyCreditDiscount != null && UI_tabSearchFlights.dummyCreditDiscount != '0.00') {
				discountAmount = discSign + UI_tabSearchFlights.dummyCreditDiscount;
			}
			
			$('#trFareDiscount').show();
			$("#fareDiscountPercentage").hide();
			$("#btnFareDiscountApply").hide();
			$("#btnDiscountPop").hide();
			$("#btnFareDiscountRemove").hide();
			$(".fareDiscountPercentage").hide();
			$(".lblFareDiscountAvail").text(discountLabel);
			$(".lblDiscountAppliedDesc").text("Applied Promotion: ")
			$(".lblDiscPercent").hide();
			$(".lblDiscountApplied").text(discountApplied);
			$(".divFareDiscVal").html(discountAmount);
			
			if(discInfoJson.discountAs == UI_tabSearchFlights.promoDiscountAs.MONEY){
				$('#trFareDiscount').show();
				$('#trFareDiscountCredit').hide();
			} else {
				$('#trFareDiscountCredit').show();
				$('#trFareDiscount').hide();
			}
			
		} else {
			$('#trFareDiscount').hide();
			$('#trFareDiscountCredit').hide();
			$("#btnFareDiscountApply").hide();
			$(".fareDiscountPercentage").hide();
			$(".lblFareDiscountAvail").text("");
			$(".lblFareDiscounted").text("");
			$(".divFareDiscVal").html('');
			if(UI_tabSearchFlights.objResSrchParams.promoCode!=null
					&& UI_tabSearchFlights.objResSrchParams.promoCode!=""){
				showERRMessage("The promo code entered is no longer valid");
			}
				
		}
		
		var strPerPaxSummary = "";		
		if(jsonPrice != null) {
			for(var i = 0; i < jsonPrice.length; i++) {
				if(i==0){
					if(jsonFareQuoteSummary.selectedCurrency != jsonFareQuoteSummary.currency){						
						strPerPaxSummary = jsonFareQuoteSummary.selectedCurrency + " "+ jsonPrice[i].paxTotalPriceInSelectedCurr;						
					} else {						
						strPerPaxSummary = jsonFareQuoteSummary.currency + " "+ jsonPrice[i].paxTotalPrice;
					}
				}else { 
					if(jsonFareQuoteSummary.selectedCurrency != jsonFareQuoteSummary.currency) {
						strPerPaxSummary = strPerPaxSummary + "<span> + </span>" + jsonPrice[i].paxTotalPriceInSelectedCurr;
					} else {						
						strPerPaxSummary = strPerPaxSummary + "<span> + </span>" + jsonPrice[i].paxTotalPrice;
					}
				}
			}
		}
		
		/* Footer */
		if(jsonFareQuoteSummary.selectedCurrency != jsonFareQuoteSummary.currency){
			var selectedtotalPrice = jsonFareQuoteSummary.selectedtotalPrice;			
			$("#divFQTxnCurr").html("ER : "+jsonFareQuoteSummary.selectedCurrency+" 1.00 = "+jsonFareQuoteSummary.currency +" " +jsonFareQuoteSummary.selectedEXRate +"&nbsp;  <font color='red'>"+jsonFareQuoteSummary.selectedCurrency + " "+selectedtotalPrice+"</font>");
		} else {
			$("#divFQTxnCurr").html("");
		}
		
		$("#divFQPerPaxSummary").html(strPerPaxSummary);
		$("#divFQTotCurr").html(jsonFareQuoteSummary.currency);		
		$("#divFQTotAmt").html(jsonFareQuoteSummary.totalPrice);
		
		/* Initialize the Details */
		$("#tblFareQuoteBreakDown").find("tr").remove();
		
		var strHTMLText = "";
		var intRowSpan = 0;
		var intColSpan = 8;
		
		var objJFQ = jsonFareQuote;
		var intLen = objJFQ.length;
		var i = 0;
		
		/* Detail break down */
		
		var intLenDet = 0;
		var x = 0;
		
		var tblRow = null;
		do{
			intRowSpan = objJFQ[i].paxWise.length;
			intLenDet = intRowSpan;
			x = 0;
			do{
				if (objJFQ[i].ondCode != null) {
					tblRow = document.createElement("TR");									
				
					if (x == 0){
						tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].ondCode, rowSpan:intRowSpan, css:UI_commonSystem.strDefClass + " thinBorderL"}));
					}
				
					tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].paxWise[x].paxType, css:UI_commonSystem.strDefClass}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].paxWise[x].fare, align:"right", css:UI_commonSystem.strDefClass + " rowHeight"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].paxWise[x].tax, align:"right", css:UI_commonSystem.strDefClass}));
				
					tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].paxWise[x].sur, align:"right", css:UI_commonSystem.strDefClass}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].paxWise[x].perPax, align:"right", css:UI_commonSystem.strDefClass}));
				
					tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].paxWise[x].noPax, align:"right", css:UI_commonSystem.strDefClass}));
				
					tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].paxWise[x].total, align:"right", css:UI_commonSystem.strDefClass}));
				
					if (x == 0){					
						tblRow.appendChild(UI_commonSystem.createCell({desc:objJFQ[i].totalPrice, rowSpan:intRowSpan, align:"right", css:UI_commonSystem.strDefClass}));
					}
					var FareRuleTO = this.availableFare.fareRulesInfo.fareRules;
				
					var setFareRuleInfor = function(frTO, ondCode, ondSequence, str){
						/*
						 * Split the passed full ond code into an array. Necessary for evaluations since ondCode can contain N amount of segs.
						 * eg: ABC/DEF/GHI/JKL etc.
						 */
						var ondCodeArr=ondCode.split("/");
					
						/* Create two each segments to be matched with FareRule orignNDest. ABC/DEF/GHI/JKL etc. will be
						 * ABC/DEF,DEF/GHI,GHI/JKL etc. This is necessary since FareRule's orignNDest sometime have no via points
						 * and match connecting flight segments only (eg: for ABC/DEF flight going through GHI full ondCode will be 
						 * ABC/GHI/DEF and on return DEF/GHI/ABC but fare rules that apply here will have orignNDest codes like ABC/GHI, GHI/DEF etc.).
						 */
						var orignNDestSplittedArray=[];
						for(var i = 0;i < (ondCodeArr.length - 1);i++){
							orignNDestSplittedArray[i]=ondCodeArr[i]+'/'+ondCodeArr[i+1];
						}
					
						var remoundescore = function(str){
							var returnString="";
							if(str!=undefined && str!=null)
							{
								returnString=str.replace("_", "<br>");
							}
							return returnString;
						};
						var retunVal = '';
						$.each(frTO, function(){
							/*
							 * Split the passed full ond code into an array. Necessary for evaluations since FareRule's orignNDest can 
							 * contain N amount of segs due to via points. eg: ABC/DEF/GHI/JKL etc.
							 */
							var ruleONDArr = this.orignNDest.split("/");
						
							/*
							 * Checks if codes match directly by comparing the first elements and the last elements. For ABC/DEF/n1/n2/GHI and
							 * ABC/x1/x2/x3/GHI will match [0] (ABC) == [0] (ABC) and [length - 1] (GHI) == [length - 1] (GHI)
							 */
							var isEqualInFullCode=ruleONDArr[0] == ondCodeArr[0] && ruleONDArr[ruleONDArr.length-1] == ondCodeArr[ondCodeArr.length-1];
						
							/*
							 * For rules with no via points match by checking if they are in the created orignNDestSplittedArray array.
							 */
							var belongToFareOnd=$.inArray(this.orignNDest, orignNDestSplittedArray) > -1;
						
							if ((isEqualInFullCode || belongToFareOnd) && this.ondSequence == ondSequence) {
								if(retunVal != ''){
									retunVal += ',<br>';
								}
								if (str == "fareRuleCode") {
									if (this[str] == null
											|| this[str] == 'null') {
										retunVal += "";
									} else {
										retunVal += this[str]
												+ ' <a href="javascript:void(0)" role="'
												+ remoundescore(this["comments"])
												+ '" class="commentToolTip" style="position:relative" ><img border="0" alt="" src="../images/info_no_cache.gif"></a>';
									}
								} else {
									retunVal += this[str];
								}
							}
						});
						return retunVal;
					
					}
					if (x == 0){
						tblRow.appendChild(UI_commonSystem.createCell({desc:setFareRuleInfor(FareRuleTO, objJFQ[i].fullOndCode,objJFQ[i].ondSequence,'fareRuleCode'), rowSpan:intRowSpan, align:"center", css:UI_commonSystem.strDefClass + " toHide"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:setFareRuleInfor(FareRuleTO, objJFQ[i].fullOndCode,objJFQ[i].ondSequence,'fareBasisCode'), rowSpan:intRowSpan, align:"center", css:UI_commonSystem.strDefClass + " toHide"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:setFareRuleInfor(FareRuleTO, objJFQ[i].fullOndCode,objJFQ[i].ondSequence,'bookingClassCode'), rowSpan:intRowSpan,align:"center", css:UI_commonSystem.strDefClass + " toHide"}));
					}
					$("#tblFareQuoteBreakDown").append(tblRow);
				
					x++;
				}
			}while(--intLenDet);
			
			i++;
		}while(--intLen);
		UI_tabSearchFlights.IntCommentShowHide();
		UI_tabSearchFlights.initBaggageTooltipShowHide();
		//If app parameter is disabled(false) hide the fields
		if(!UI_tabSearchFlights.showFareRuleInfoInSummary)
		{
			$(".toHide").css("display", "none");
		}
		
	}
	
	/*
	 * ------------------------------------------------ Fare Quote Start
	 * -----------------------------------------------
	 */
	
	/*
	 * Change Fare Quote
	 */
	UI_tabSearchFlights.changeFareClick =  function(inParams){		
		UI_tabSearchFlights.objResSrchParams.searchSystem = UI_tabSearchFlights.getSelectedSystem();
		UI_tabSearchFlights.createCarrierWiseOndMap();
		UI_tabSearchFlights.isAllFareChange = false;
		var strFileName = "showNewFile!displayChangeFares.action";
		UI_tabSearchFlights.ticketValidityExist = $('#ticketExpiryEnabled').val();
		UI_tabSearchFlights.ticketValidityMax = $('#ticketExpiryDate').val();		
		var inHeight = 630;
		if ($.browser.msie){inHeight = 640;}
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		top.objCWindow =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 900));
	}
	
	UI_tabSearchFlights.viewBCFareClick =  function(rowId){	
		flightToViewBCFares = allFlights[rowId - 1];
		UI_tabSearchFlights.objResSrchParams.searchSystem = UI_tabSearchFlights.getSelectedSystem();
		UI_tabSearchFlights.createCarrierWiseOndMap();
		UI_tabSearchFlights.isAllFareChange = false;
		var strFileName = "showNewFile!displayChangeFares.action";
		UI_tabSearchFlights.ticketValidityExist = $('#ticketExpiryEnabled').val();
		UI_tabSearchFlights.ticketValidityMax = $('#ticketExpiryDate').val();		
		var inHeight = 630;
		if ($.browser.msie){inHeight = 640;}
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		var temp =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 900));
		return false;
	}
	
	/*
	 * Change Fare Quote
	 */
	UI_tabSearchFlights.changeAllFareClick =  function(inParams){
		if(!UI_tabSearchFlights.isRequote() && 
				!UI_tabSearchFlights.isOpenReturn && jsonFareQuote == null){
			if(jsonFareQuote == null){
				UI_tabSearchFlights.loadFareQuoteOnClick({async:false});
			}
			if(jsonFareQuote == null){
				return false;
			}
		}
		UI_tabSearchFlights.objResSrchParams.searchSystem = UI_tabSearchFlights.getSelectedSystem();
		UI_tabSearchFlights.createCarrierWiseOndMap();
		UI_tabSearchFlights.isAllFareChange = true;
		
		var strFileName = "showNewFile!displayChangeFares.action";
		var inHeight = 630;
		if ($.browser.msie){inHeight = 640;}
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		top.objCWindow =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 900));
	}
	
	// Setting carrier wise OND map for change fare
	UI_tabSearchFlights.createCarrierWiseOndMap = function(){
		if(UI_tabSearchFlights.isRequote()){
			ondMap = UI_requote.createCarrierWiseOndMap();
		}else if(UI_tabSearchFlights.isMulticity()){
				ondMap = UI_Multicity.createCarrierWiseOndMap();
		} else {			
			// outbound ond
			var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
			var outOnd = jsonOutFlts[intOutFlt].ondCode;
			var outcarrier = jsonOutFlts[intOutFlt].carrierList[0];	
			if (outcarrier == '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">') {
				outcarrier = UI_tabSearchFlights.busCarrierCodes[0];
			}		
			var inMap = '"'+outcarrier+'":"'+outOnd+'"';
			
			// inbound ond
			var retMap = null;
			if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt") {			
				var intRetFlt = $("#tblInboundFlights").getGridParam('selrow') - 1;
				var retOnd = jsonRetFlts[intRetFlt].ondCode;
				var retCarrier = jsonRetFlts[intRetFlt].carrierList[0];
				if (retCarrier == '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">') {
					retCarrier = UI_tabSearchFlights.busCarrierCodes[0];
				}
				retMap = '"'+retCarrier+'":"'+retOnd+'"';
			}
			ondMap = null;
			if(retMap != null){
				ondMap = '[{' + inMap + '},{' + retMap + '}]';
			} else {
				ondMap = '[{' + inMap + '}]';
			}
		}
		
		return ondMap;
	}
	
	
	UI_tabSearchFlights.getFlightInfo = function(){
		if(UI_tabSearchFlights.flightInfo != null && UI_tabSearchFlights.flightInfo.length > 0){
			return UI_tabSearchFlights.flightInfo;
		} else {
			var flts = [];
			var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
			flts[0] = jsonOutFlts[intOutFlt];
			if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt") {	
				var intRetFlt = $("#tblInboundFlights").getGridParam('selrow') - 1;
				flts[1] = jsonRetFlts[intOutFlt];
			}
			var fltInfoArr = [];
			for(var i = 0 ; i < flts.length ; i++){
				for(var j = 0 ; j < flts[i].segmentCodeList.length ;j++){
					var obj = {
						orignNDest : flts[i].segmentCodeList[j], 
						flightNo : flts[i].flightNoList[j],
						airLine : flts[i].carrierList[j],
						departureDate : flts[i].depatureList[j],
						departureDateTimeZ: flts[i].departureTimeZuluList[j],
						arrivalDateTimeZ:  flts[i].arrivalTimeZuluList[j],
						flightRefNumber : flts[i].flightRPHList[j],
						returnFlag : flts[i].returnFlag,
						requestedOndSequence: flts[i].requestedOndSequenceList[j]
						};
					fltInfoArr[fltInfoArr.length] = obj;
				}
			}
			return fltInfoArr;
		}
	}
	
	UI_tabSearchFlights.getFlightToGetBCInfo = function(){
		var fltInfoArr = [];
		var cos = $("#classOfService").val();
		if (flightToViewBCFares != null && fromViewBCFare == true) {
			for (var x=0; x<flightToViewBCFares.flightSegIdList.length; x++ ) {
				var obj = {
				orignNDest : flightToViewBCFares.segmentCodeList[x], 
				flightNo : flightToViewBCFares.flightNoList[x],
				airLine : flightToViewBCFares.carrierList[x],
				departureDate : flightToViewBCFares.depatureList[x],
				departureDateTimeZ: flightToViewBCFares.departureTimeZuluList[x],
				arrivalDateTimeZ:  flightToViewBCFares.arrivalTimeZuluList[x],
				flightRefNumber : flightToViewBCFares.flightRPHList[x],
				returnFlag : flightToViewBCFares.returnFlag,
				cabinClass: cos
				};
				fltInfoArr[x] = obj;
			}
			return fltInfoArr;
		} else {
			return null;
		}
	}
	
	UI_tabSearchFlights.getJsonFlightInfo = function(){
		return $.toJSON(UI_tabSearchFlights.getFlightInfo());
	}
	
	UI_tabSearchFlights.getFareQuote = function(){
		return jsonFareQuote;
	}
	
	UI_tabSearchFlights.getSelectedFareType = function(){
		if(jsonFareQuoteSummary != null){
			return jsonFareQuoteSummary.fareType;
		}
		return -1;
	}
	
	
	UI_tabSearchFlights.getFlightInfo = function(){
		if(UI_tabSearchFlights.flightInfo != null && UI_tabSearchFlights.flightInfo.length > 0){
			return UI_tabSearchFlights.flightInfo;
		} else {
			var flts = [];
			var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
			flts[0] = jsonOutFlts[intOutFlt];
			if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt") {	
				var intRetFlt = $("#tblInboundFlights").getGridParam('selrow') - 1;
				flts[1] = jsonRetFlts[intOutFlt];
			}
			var fltInfoArr = [];
			for(var i = 0 ; i < flts.length ; i++){
				for(var j = 0 ; j < flts[i].segmentCodeList.length ;j++){
					var obj = {
						orignNDest : flts[i].segmentCodeList[j], 
						flightNo : flts[i].flightNoList[j],
						airLine : flts[i].carrierList[j],
						departureDate : flts[i].depatureList[j],
						departureDateTimeZ: flts[i].departureTimeZuluList[j],
						arrivalDateTimeZ:  flts[i].arrivalTimeZuluList[j],
						flightRefNumber : flts[i].flightRPHList[j],
						returnFlag : flts[i].returnFlag
						};
					fltInfoArr[fltInfoArr.length] = obj;
				}
			}
			return fltInfoArr;
		}
	}
	
	UI_tabSearchFlights.getJsonFlightInfo = function(){
		return $.toJSON(UI_tabSearchFlights.getFlightInfo());
	}
	
	UI_tabSearchFlights.getFareQuote = function(){
		return jsonFareQuote;
	}
	
	UI_tabSearchFlights.getSelectedFareType = function(){
		if(jsonFareQuoteSummary != null){
			return jsonFareQuoteSummary.fareType;
		}
		return -1;
	}
	
	
	UI_tabSearchFlights.getFlightInfo = function(){
		if(UI_tabSearchFlights.flightInfo != null && UI_tabSearchFlights.flightInfo.length > 0){
			return UI_tabSearchFlights.flightInfo;
		} else {
			var flts = [];
			var intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
			flts[0] = jsonOutFlts[intOutFlt];
			if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode !="confirmOprt") {	
				var intRetFlt = $("#tblInboundFlights").getGridParam('selrow') - 1;
				flts[1] = jsonRetFlts[intOutFlt];
			}
			var fltInfoArr = [];
			for(var i = 0 ; i < flts.length ; i++){
				for(var j = 0 ; j < flts[i].segmentCodeList.length ;j++){
					var obj = {
						orignNDest : flts[i].segmentCodeList[j], 
						flightNo : flts[i].flightNoList[j],
						airLine : flts[i].carrierList[j],
						departureDate : flts[i].depatureList[j],
						departureDateTimeZ: flts[i].departureTimeZuluList[j],
						arrivalDateTimeZ:  flts[i].arrivalTimeZuluList[j],
						flightRefNumber : flts[i].flightRPHList[j],
						returnFlag : flts[i].returnFlag
						};
					fltInfoArr[fltInfoArr.length] = obj;
				}
			}
			return fltInfoArr;
		}
	}
	
	UI_tabSearchFlights.getJsonFlightInfo = function(){
		return $.toJSON(UI_tabSearchFlights.getFlightInfo());
	}
	
	UI_tabSearchFlights.getFareQuote = function(){
		return jsonFareQuote;
	}
	
	UI_tabSearchFlights.getSelectedFareType = function(){
		if(jsonFareQuoteSummary != null){
			return jsonFareQuoteSummary.fareType;
		}
		return -1;
	}
	
	UI_tabSearchFlights.setSearchSystem = function(){
		var intOutFlt  = 0;
		if( UI_commonSystem.strPGMode != "confirmOprt"){
			intOutFlt = $("#tblOutboundFlights").getGridParam('selrow') - 1;
		}
		
		var retSystem = "";
		if (UI_tabSearchFlights.strRetDate != "" && UI_commonSystem.strPGMode != "confirmOprt"){
			var intRetFlt  = $("#tblInboundFlights").getGridParam('selrow') - 1;
			retSystem = jsonRetFlts[intRetFlt].system;
		}
		
		if(jsonOutFlts == null && intOutFlt == -1 && retSystem == ""){			
			return $("#selSearchOptions").val();
		} else {
			if(jsonOutFlts[intOutFlt].system == "INT" || retSystem == "INT"){
				return "INT";
			} else {
				return "AA";
			}
		}
	}
	
	/*
	 * Fare rule details
	 */
	UI_tabSearchFlights.fareRulesOnClick =  function(){
		var strFileName = "showNewFile!displayFareRules.action";
		var inHeight = 400;
		if ($.browser.msie){inHeight = 390;}
		var tmpFareRuleInfo = UI_tabSearchFlights.fareRulesInfo;
		if(tmpFareRuleInfo == null || tmpFareRuleInfo.fareRules ==null || tmpFareRuleInfo.fareRules.length < 1){
			var inHeight = 220;
			if ($.browser.msie){inHeight = 210;}
		}
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		top.objCWindow =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 900));
	}

	UI_tabSearchFlights.fillBCDropDown = function(){
			var cos = $("#classOfService").val().split("-")[0];	
			var bookingClassCode =  new Array();
			var count = 0;
			
			for(var i = 0;i<top.arrBookingClassCode.length;i++){
				if(UI_tabSearchFlights.isMulticity()) {
					var bcCodeArr = new Array();
					bcCodeArr[0] = top.arrBookingClassCode[i][0];
					bcCodeArr[1] = top.arrBookingClassCode[i][3];
					bookingClassCode[count] = bcCodeArr;
					count++;
				} else {
					if($("#classOfService").val().split("-")[1] == null){
						if(top.arrBookingClassCode[i][2] == cos){
							var bcCodeArr = new Array();
							bcCodeArr[0] = top.arrBookingClassCode[i][0];
							bcCodeArr[1] = top.arrBookingClassCode[i][3];
							bookingClassCode[count] = bcCodeArr;
							count++;
						}
					}else if(top.arrBookingClassCode[i][1] == cos){
						var bcCodeArr = new Array();
						bcCodeArr[0] = top.arrBookingClassCode[i][0];
						bcCodeArr[1] = top.arrBookingClassCode[i][3];
						bookingClassCode[count] = bcCodeArr;
						count++;
					}
				}
			}			
		$("#selBookingClassCode").empty();
		$("#selBookingClassCode").fillDropDown({ dataArray:bookingClassCode , keyIndex:0 , valueIndex:1, firstEmpty: true });
	}	
	
	
	/**
	 * Agents drop down
	 * */
	UI_tabSearchFlights.fillAgentsDropDown = function(){

		var tAgents = new Ext.data.ArrayStore({
							fields: ['code','name'],
							data : top.arrAllTravelAgents
						});

		if($('#tAgent').length > 0){
			UI_tabSearchFlights.tAgentCombo = new Ext.form.ComboBox({
			    store: tAgents,
			    displayField:'name',
			    typeAhead: false,
			    mode: 'local',
			    triggerAction: 'all',
			    emptyText:'Please select a value',
			    selectOnFocus:true,
			    applyTo: 'tAgent',
		 	    anyMatch: true,
		        caseSensitive: false,
		        forceSelection: true,
		        valueField:'code',
		        hiddenName:'searchParams.travelAgentCode',
		        hiddenId:'travelAgentCode',
		        listeners: {
		            'blur': function(f,new_val,oldVal) {
						UI_tabSearchFlights.onExtComboOnChange(f,new_val,oldVal)
		            }
		        }
			});
		}
		
	}
	/*
	 * Tax Surcharge details
	 */
	UI_tabSearchFlights.taxesSurchargesOnClick =  function(){
		var strFileName = "showNewFile!displayTaxesSurcharges.action";
		var inHeight = 200;
		if ($.browser.msie){inHeight = 190;}
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		top.objCWindow =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 700));
	}
	
	/*
	 * TODO Fare type On Click
	 */
	UI_tabSearchFlights.fareTypeClick = function(){
		switch ($("input[name='radFFare']:checked").val()){
			case "F" :
				break;
			case "S" : 
				break;
		}
		
		alert("TODO \n UI_tabSearchFlights.fareTypeClick -  " + $("input[name='radFFare']:checked").val());
	}
	
	/*
	 * Build the Break down Informations
	 */
	UI_tabSearchFlights.buildBreakDownInfo = function(inParams){
		$("#divBKHD").html(inParams.desc);
		$("#divBreakDownPane").show();
		$(inParams.id).show();
		UI_commonSystem.fillGridData({id:inParams.idGrid, data:inParams.data});
	}
	
	UI_tabSearchFlights.originDestinationOnBlur = function(){
		doTrim = function(o){
			var t = "";
			if ( o != null || o != "" ){
				t = o.split("-");
			}
			return $.trim(String(t[0]).toUpperCase());
		}
		splitSubStationAndOwnerAirport = function (value, topElem, subElem){
			var valSplited = UI_tabSearchFlights.getSubstationAndOwnerAirport(value);
			if(valSplited != null){
				topElem.val(valSplited[0]);
				subElem.val(valSplited[1]);
			}			
		}		
		
		// It seems to be wrong to get value through $("#fAirport").val()
		// because this is returining the display value.
		// the "code" is returned by UI_tabSearchFlights.fromCombo.getValue()
		// method.
		// since I need to get the top level airport as well as the sub airport
		// i'm calling the
		// doTrim() method only if it's not a surface airport.//@author by
		// Navod.
		// $("#fromAirport").val(doTrim( $.trim($("#fAirport").val()) ));
		// $("#toAirport").val(doTrim( $.trim($("#tAirport").val()) ));
		var spliter = UI_tabSearchFlights.surfaceSegmentConfigs.sufaceSegmentAppendStr;
		if($("#fAirport").val() != null ){
			if( $.trim($("#fAirport").val()).indexOf(spliter) != 0){
				$("#fromAirport").val(doTrim( $.trim($("#fAirport").val()) ));
				$("#fromAirportSubStation").val("");
			}else{
				var value = UI_tabSearchFlights.getFromAirportCode();
				splitSubStationAndOwnerAirport(value,$("#fromAirport"),$("#fromAirportSubStation"));
			}
		}
		if($("#toAirport").val() != null){
			if($.trim($("#tAirport").val()).indexOf(spliter) != 0){
				$("#toAirport").val(doTrim( $.trim($("#tAirport").val()) ));
				$('#toAirportSubStation').val("");
			}else {
				var value = UI_tabSearchFlights.getToAirportCode()
				splitSubStationAndOwnerAirport(value,$("#toAirport"),$('#toAirportSubStation'));
				
			}
		}
		UI_tabSearchFlights.ignoreSelectionIfSurfaceSegment(UI_tabSearchFlights.fromCombo,$.trim($("#fromAirport").val()));
		UI_tabSearchFlights.ignoreSelectionIfSurfaceSegment(UI_tabSearchFlights.toCombo,$.trim($("#tAirport").val()));
		
	}
	
	UI_tabSearchFlights.getSubstationAndOwnerAirport = function (value){
		if(value != null  && value != ""){
			return value.split(UI_tabSearchFlights.surfaceSegmentConfigs.surfSegCodeSplitter);
		}else {
			return null;
		}
		
	}
	
	UI_tabSearchFlights.ignoreSelectionIfSurfaceSegment = function (selectorID, value){
		var blnIsSubStationParent = false;
		var subStationCode = "";
		// TODO: Figure out if value is a sub station parent
		if(blnIsSubStationParent){
			selectorID.selectByValue(subStationCode);// Only for EXT JS Combo
														// BOX. Also does not
														// fire any attached
														// select events
		}
	}
	
	UI_tabSearchFlights.originDestinationChanged = function(){
		var from = UI_tabSearchFlights.getFromAirportCode();
		var to = UI_tabSearchFlights.getToAirportCode();

		if(from == '' || to == '') return;
		
		if(!$('#selSearchOptions').is(':disabled')){			
			UI_tabSearchFlights.setDefaultSearchOption(from, to);
		}
	}
	
	UI_tabSearchFlights.originChanged = function(){
		var from = UI_tabSearchFlights.getFromAirportCode();
		var fromKey = UI_tabSearchFlights.fromCombo.getValue();
		
		var toStore = null;
		if(fromKey != '' &&
				UI_tabSearchFlights.ondDataMap[fromKey]){	
			var toData;
			
			if(DATA_ResPro.initialParams.allowRouteSelectionForAgents){
				toData = UI_tabSearchFlights.agentWiseOndDataMap[fromKey];
			} else {
				toData = UI_tabSearchFlights.ondDataMap[fromKey];		
				
				if(DATA_ResPro.modifySegments != null) {
					toData = arrGroundSegmentParentTo;			
				}
				
				// is ground segment groundSegmentAirports are present. if just
				// addsegment only pure airports are present
				if(DATA_ResPro.isAddSegment && DATA_ResPro.isAddSegment == "true" ){
					toData = arrGroundSegmentParentTo;
				}

			}			
			
			toStore = new Ext.data.ArrayStore({
				fields: ['code', 'x', 'name','city','code_city'],
				data : toData,
			    sortInfo: { field: 'city', direction: 'DESC' }
			}); 			
		} else {
			
			var toData = top.arrAirportsV2;
			if(DATA_ResPro.initialParams.allowRouteSelectionForAgents){
				toData = UI_tabSearchFlights.agentWiseOndDataMap[fromKey];
			}
			
			if(DATA_ResPro.modifySegments != null) {
				toData = arrGroundSegmentParentTo;			
			}
			
			// is ground segment groundSegmentAirports are present. if just
			// addsegment only pure airports are present
			if(DATA_ResPro.isAddSegment && DATA_ResPro.isAddSegment == "true" ){
				toData = arrGroundSegmentParentTo;
			}
			
			toStore = new Ext.data.ArrayStore({
				fields: ['code', 'x', 'name','city','code_city'],
				data : toData,
			    sortInfo: { field: 'city', direction: 'DESC' }
			});
		}
		if(DATA_ResPro.initialParams.allowRouteSelectionForAgents) {
			var included = false; 
			var destinations = UI_tabSearchFlights.agentWiseOndDataMap[fromKey];
			if(destinations != undefined) {
				for (var key in destinations) {
					if(destinations.hasOwnProperty(key)) {
						if(destinations[key][0] == Ext.getCmp("extToAirport").getValue()) {
							included = true;
							break;
						}
					}					
				}
			}
			if(!included) {
				Ext.getCmp("extToAirport").clearValue();
				Ext.getCmp("extToAirport").applyEmptyText();
			}			
			
		}
	
		Ext.getCmp("extToAirport").bindStore(toStore);

		var to = UI_tabSearchFlights.getToAirportCode();
		if(from == '' || to == '') return;
		
		if(!$('#selSearchOptions').is(':disabled')){			
			UI_tabSearchFlights.setDefaultSearchOption(from, to);
		}
	}
	
	UI_tabSearchFlights.setDefaultSearchOption = function (from, to){
		$("#trCheckStopOver").hide();//to hide after ond selection
		UI_tabSearchFlights.hubTimeDetailJSON = [];
		if(DATA_ResPro.systemParams.dynamicOndListPopulationEnabled
				&& UI_tabSearchFlights.ondListLoaded()){			
			var fromIndex = -1;
			var toIndex = -1;
			// get from, to indexes
			var airportsLength = airports.length;
			for(var i=0;i<airportsLength;i++){
				if(from == airports[i].code){
					fromIndex = i;
				} else if(to == airports[i].code) {
					toIndex = i;
				}
				
				if(fromIndex != -1 && toIndex != -1){
					break;
				}
			}
			
			if(fromIndex != -1 && origins[fromIndex] != undefined){			
				var availDestOptLength = origins[fromIndex].length;
				for(var i=0;i<availDestOptLength;i++){
					var destOptArr = origins[fromIndex][i];
					if(destOptArr[0] == toIndex){
						var isInt = false;
						var optCarriersArr = destOptArr[3].split(",");
						for(var j=0;j<optCarriersArr.length;j++){
							if(optCarriersArr[j] != carrierCode){
								isInt = true;
								break;
							}
						}
						
						if(isInt){
							$('#selSearchOptions').val('INT');
						} else {
							$('#selSearchOptions').val('AA');
						}
						
						if(DATA_ResPro.systemParams.userDefineTransitTimeEnabled){
							//prepare HUB stopover time option 
							var hubCodeList = destOptArr[5];
							if(hubCodeList != null && hubCodeList.length > 0){
								for(var k = 0; k < hubCodeList.length; k++){
									var hubCodes = hubCodeList[k];
									if(hubCodes != null && hubCodes.length > 0){
										for(var l = 0; l < hubCodes.length ; l++){
											var tempModel = {};
											tempModel.hubCode = hubCodes[l];
											tempModel.outboundStopOverTime = 0;
											tempModel.inboundStopOverTime = 0;
											UI_tabSearchFlights.hubTimeDetailJSON[UI_tabSearchFlights.hubTimeDetailJSON.length] = tempModel;
											//Calling to the chkStopOverAdd(); to display the checkbox	
											chkStopOverAdd();
										}
										
									}
								}
							}
						}
						break;
					}
				}
			} else {			
				$('#selSearchOptions').val('AA');
			}
		} else {
			if (UI_tabSearchFlights.modifySearch) {
				// do nothing as values are defaulted
			} else if ((top.airportOwnerList[from] == 'OWN') && (top.airportOwnerList[to] == 'OWN')) {
				$('#selSearchOptions').val('AA');
			} else if ((top.airportOwnerList[from] == 'OWN') || (top.airportOwnerList[to] == 'OWN')) {
				$('#selSearchOptions').val('INT');
			} else {
				$('#selSearchOptions').val('INT');
			}
		}
	}
	
	function chkStopOverAdd(){
		$("#trCheckStopOver").show();
		UI_tabSearchFlights.showStopOver();
		$("input[name='stopOver']").off("keydown").on("keydown", function(e){
			if (e.keyCode == 13) {
				UI_tabSearchFlights.applyStopOverTime();
				$("#stopOverTimePopup").hide();
			}
		})
		$("#chkStopOverAdd").off("click").on("click", function(){
			UI_tabSearchFlights.showStopOver();
			$("#stopOverTimePopup").show();
			$("#stopOver_0").numeric();
			$("#stopOver_0").focus();
		});
		$("#btnCanStopOver").off("click").on("click", function(){
			$("#stopOverTimePopup").hide();
		});
		$("#btnApplyStopOver").off("click").on("click", function(){
			UI_tabSearchFlights.applyStopOverTime()
			$("#stopOverTimePopup").hide();
		});
	}
	
	UI_tabSearchFlights.showStopOver = function(){
		$(".stopOverTimeInner").empty();
		var table = $("<table></table>").append("<tr><th width='40%'>Hub</th><th width='60%' colspan='2'>StopOver Time in days</th></tr>");
		$.each(UI_tabSearchFlights.hubTimeDetailJSON, function(i,j){
			var outboundInput = "<input type='text' class='aa-input' id='outboundStopOver_"+i+"' name='outboundStopOver' maxlength='2' value='"+j.outboundStopOverTime+"'/>";
			var inTranSitTime = j.inboundStopOverTime, strDisable = "";
			if($.trim($("#returnDate").val())==''){
				inTranSitTime = 0;
				strDisable = "disabled='disabled'"
			}
			var inboundInput = "<input type='text' class='aa-input' id='inboundStopOver_"+i+"' name='inboundStopOver' maxlength='2' value='"+inTranSitTime+"' "+strDisable+"/>";
			var tRow = $("<tr></tr>").append("<td valign='bottom'>"+j.hubCode+"</td><td> Outbound"+ outboundInput +"</td><td> Inbound"+ inboundInput + "</td>");
			$(".stopOverTimeInner").append(table.append(tRow));
		});

		$("input[name='outboundStopOver']").on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		});

		$("input[name='inboundStopOver']").on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		});	
		
		$(document).off("keyup").on("keyup", function(e){
			if (e.keyCode == 27) {
				$(':focus').closest(".popupXBE").hide();
			}
		})
		UI_tabSearchFlights.createStopOverStr();
	};
	
	UI_tabSearchFlights.createStopOverStr = function(){
		var strST = "(";
		$.each(UI_tabSearchFlights.hubTimeDetailJSON, function(i,j){
			if(i==UI_tabSearchFlights.hubTimeDetailJSON.length-1){
				strST += j.hubCode + ":"+ j.outboundStopOverTime + ":" + j.inboundStopOverTime;
			}else{
				strST += j.hubCode + ":"+ j.outboundStopOverTime + ":" + j.inboundStopOverTime + " ,";
			}
		});
		strST += ")";
		$("#chkStopOverAdd").text(btnText.replace("{0}", strST));
	};
	
	
	UI_tabSearchFlights.applyStopOverTime = function(){
		var strST = "(";
		$.each(UI_tabSearchFlights.hubTimeDetailJSON, function(i,j){
			j.outboundStopOverTime = ($.trim($("#outboundStopOver_"+i).val())=="")?0:parseInt($.trim($("#outboundStopOver_"+i).val()),10);
			j.inboundStopOverTime = ($.trim($("#inboundStopOver_"+i).val())=="")?0:parseInt($.trim($("#inboundStopOver_"+i).val()),10);
			if(i==UI_tabSearchFlights.hubTimeDetailJSON.length-1){
				strST += j.hubCode + ":"+ j.outboundStopOverTime + ":" + j.inboundStopOverTime
			}else{
				strST += j.hubCode + ":"+ j.outboundStopOverTime + ":" + j.inboundStopOverTime + " ,"
			}
		});
		strST += ")";
		$("#chkStopOverAdd").text(btnText.replace("{0}", strST));
	}
	
	UI_tabSearchFlights.ondListLoaded = function(){
		if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
			return false;
		} else {
			return true;
		}
	}
	
	UI_tabSearchFlights.isAvailableAnList = function (combo, val){
		var dataObj = combo.store.data.items;
		for (var i = 0; i < dataObj.length; i++){
			if (val == dataObj[i].data.code_city){
				return true;
				break;
			}
		}
		return false;
	}
	
	UI_tabSearchFlights.onExtComboOnChange = function (combo,record,index){
		var newVal = combo.getValue();
		if(UI_tabSearchFlights.isGroundSegmentAirport(newVal)){			
			// combo.reset();
			// combo.select();
		}
		if (!UI_tabSearchFlights.isAvailableAnList(combo, newVal)){
			combo.reset();
			combo.select();
		}
	}
			
	UI_tabSearchFlights.isGroundSegmentAirport = function (airportCode){
		var identifier = UI_tabSearchFlights.surfaceSegmentConfigs.groundStationPrimaryIdentifier;
		if(airportCode != null ){
			if( $.trim(airportCode).indexOf(identifier) != 0){
				return false;
			}
			return true;
		}
	}
	
	UI_tabSearchFlights.identifySelectedJsonFlightSegment = function (){
		var selected = selectedFlightSegRefNumber;			
		if(DATA_ResPro['flightDetails'] != null && selected != null && selected != ""){
			var dataObj = $.parseJSON(DATA_ResPro['flightDetails']);
			for ( var i = 0; i < dataObj.length; i++) {
				var flightSegID = dataObj[i].flightSegmentRefNumber;
				if(selected == flightSegID){
					return dataObj[i];
				}
			}
		}
		return null;
	}

	UI_tabSearchFlights.setCalanderOnFromToChange = function (){
		var jsonFlightInfoObj = UI_tabSearchFlights.identifySelectedJsonFlightSegment();
		if(jsonFlightInfoObj == null) return ;
		var spliter = UI_tabSearchFlights.surfaceSegmentConfigs.sufaceSegmentAppendStr;
		
		var splitedDateDep = jsonFlightInfoObj.departureDate.split("/");
		var splitedDateArrival = jsonFlightInfoObj.arrivalDate.split("/");
		var newDateinDateFormatDep = new Date(splitedDateDep[2],Number(splitedDateDep[1])-1,splitedDateDep[0]);
		var newDateinDateFormatArrival = new Date(splitedDateArrival[2],Number(splitedDateArrival[1]) - 1,splitedDateArrival[0]); 
		if(newDateinDateFormatDep < new Date()){
			newDateinDateFormatDep = new Date();
		}
		if(newDateinDateFormatArrival < new Date){
			newDateinDateFormatArrival = new Date();
		}
		$( "#departureDate" ).datepicker( "option", "minDate",  newDateinDateFormatDep);
		$( "#returnDate" ).datepicker( "option", "minDate", newDateinDateFormatArrival);

	}
	
	/*
	 * Fill Drop Down Data for the Page
	 */
	UI_tabSearchFlights.fillDropDownData = function() {
			// $("#fromAirport").fillDropDown( { dataArray:top.arrAirportsV2 ,
			// keyIndex:0 , valueIndex:2, firstEmpty:true });
			// $("#toAirport").fillDropDown( { dataArray:top.arrAirportsV2 ,
			// keyIndex:0 , valueIndex:2, firstEmpty:true });
			$("#selectedCurrency").fillDropDown( { dataArray:top.arrCurrency , keyIndex:0 , valueIndex:0 });
			$("#classOfService").fillDropDown( { dataArray:top.arrLogicalCC , keyIndex:0 , valueIndex:1 });
			$("#bookingType").fillDropDown( { dataArray:top.arrBookingTypes , keyIndex:0 , valueIndex:1 });
			$("#selSearchOptions").fillDropDown( { dataArray:top.arrSearchOptions , keyIndex:0 , valueIndex:1 });
			$("#selFareTypeOptions").fillDropDown( { dataArray:top.arrFareCat , keyIndex:0 , valueIndex:1 });
			$("#selPaxTypeOptions").fillDropDown( { dataArray:top.arrCusType , keyIndex:0 , valueIndex:1 });
			$("#validity").fillDropDown( { dataArray:top.arrRetValidity , keyIndex:0 , valueIndex:1, firstEmpty: true });
			UI_tabSearchFlights.fillBCDropDown();
			
			if(top.arrFareCat.length <=1){
				$('#spnFareType').hide();
				$("#selFareTypeOptions").hide();
			}else{
				$('#spnFareType').show();
				$("#selFareTypeOptions").show();
			}
			if(top.arrCusType.length <=1){
				$('#spnPaxType').hide();
				$("#selPaxTypeOptions").hide();
				
				if(top.arrFareCat.length <=1){
					$('#trPaxFare').hide();
				}
			}else{
				$('#spnPaxType').show();
				$("#selPaxTypeOptions").show();
			}
			
			// if (UI_commonSystem.strPGMode == ""){
				// $("#fromAirport").sexyCombo({emptyText: "Please select a
				// value", tabIndex: 1});
				// $("#toAirport").sexyCombo({emptyText: "Please select a
				// value", tabIndex: 2});

			var filteredAirportsList = UI_tabSearchFlights.filterAirportsForDropdowns();
			var toData = filteredAirportsList;
			var fromData = filteredAirportsList;

			var allAgentsList = top.arrAllTravelAgents;
			if(DATA_ResPro.modifySegments != null) {
				var fromData = arrGroundSegmentParentFrom;
				var toData = arrGroundSegmentParentTo;				
			}
			
			// is ground segment groundSegmentAirports are present. if just
			// addsegment only pure airports are present
			if(DATA_ResPro.isAddSegment && DATA_ResPro.isAddSegment == "true" ){
				fromData = arrGroundSegmentParentFrom;
				toData = arrGroundSegmentParentTo;
			}

			if(DATA_ResPro.initialParams.allowRouteSelectionForAgents){
				fromData = UI_tabSearchFlights.agentWiseOrigins;
			}			
			
				var fromStore = new Ext.data.ArrayStore({
				    fields: ['code', 'x', 'name','city','code_city'],
				    data : fromData,
				    sortInfo: { field: 'city', direction: 'DESC' }
				});
			 
				var toStore = new Ext.data.ArrayStore({
				    fields: ['code', 'x', 'name','city','code_city'],
				    data : toData,
				    sortInfo: { field: 'city', direction: 'DESC' }
				}); 
				
				var tAgents = new Ext.data.ArrayStore({
					fields: ['code','name'],
					data : allAgentsList
				});
				
				// Flight Search shortcut by passed via extjs custom
				// keymap[AARESAA-4810 -Issue 1 & 2]
				var keyMap = new Ext.KeyMap(document,{
					key: "abdfgijklmnprstuvwxyz4",
					alt:true,
					fn: function(){ UI_PageShortCuts.onKeyPressCommon(Ext.EventObject); },
					stopEvent:true	
				});

				UI_tabSearchFlights.fromCombo = new Ext.form.ComboBox({
				    store: fromStore,
				    id: 'extFromAirport',
				    displayField:'name',
				    typeAhead: false,
				    mode: 'local',
				    triggerAction: 'all',
				    emptyText:'Please select a value',
				    selectOnFocus:true,
				    applyTo: 'fAirport',
			 	    anyMatch: true,
			        caseSensitive: false,
			        forceSelection: true,
			        valueField:'code_city',
			        hiddenName:'searchParams.fromAirport',
			        hiddenId:'fromAirport',
			        listeners: {
			            'blur': function(f,new_val,oldVal) {
							UI_tabSearchFlights.onExtComboOnChange(f,new_val,oldVal)
			            }
			        }
				});
				
				UI_tabSearchFlights.toCombo = new Ext.form.ComboBox({
				    store: toStore,
				    id: 'extToAirport',
				    displayField:'name',
				    typeAhead: false,
				    mode: 'local',
				    triggerAction: 'all',
				    emptyText:'Please select a value',
				    selectOnFocus:true,
				    applyTo: 'tAirport',
			 	    anyMatch: true,
			        caseSensitive: false,
			        valueField:'code_city',
			        hiddenName:'searchParams.toAirport',
			        hiddenId:'toAirport',
			        listeners: {
			            'blur': function(f,new_val,oldVal) {
							UI_tabSearchFlights.onExtComboOnChange(f,new_val,oldVal)
			            }
			        }
				});
				
				if($('#tAgent').length > 0){
					UI_tabSearchFlights.tAgentCombo = new Ext.form.ComboBox({
					    store: tAgents,
					    displayField:'name',
					    typeAhead: false,
					    mode: 'local',
					    triggerAction: 'all',
					    emptyText:'Please select a value',
					    selectOnFocus:true,
					    applyTo: 'tAgent',
				 	    anyMatch: true,
				        caseSensitive: false,
				        forceSelection: true,
				        valueField:'code',
				        hiddenName:'searchParams.travelAgentCode',
				        hiddenId:'travelAgentCode',
				        listeners: {
				            'blur': function(f,new_val,oldVal) {
								UI_tabSearchFlights.onExtComboOnChange(f,new_val,oldVal)
				            }
				        }
					});
				}				
				
			UI_tabSearchFlights.toCombo.on('change', function(){
				UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originDestinationChanged();
			});
			UI_tabSearchFlights.toCombo.on('focus', function(){
				UI_tabSearchFlights.clearAll();
			});
		
			UI_tabSearchFlights.fromCombo.on('change', function(){
				UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originChanged();
			});
			
			UI_tabSearchFlights.fromCombo.on('select', function(){
				 UI_tabSearchFlights.originChanged();
			});
	}

    UI_tabSearchFlights.filterAirportsForDropdowns = function() {
        if (isMinimumFare) {
            return top.arrAirportsV2.filter( function(airport) {
                // if airport is not a dry and not a surface (bus) and not a city
                return !airport[5] && !airport[6] && 'Y' !== airport[3];
        	});
        } else {
            return top.arrAirportsV2;
        }
    }
	
	var selrowBy = function(id, p){
		$("#"+p).jqGrid("setSelection", id);
	}
	
	/*
	 * Grid Constructor
	 */
	UI_tabSearchFlights.constructGrid = function(inParams){
		// construct grid
		
		var radioButtonFmt = function(index, options, rowObject){
			var prefix = '';
			if( inParams.id == '#tblOutboundFlights'){
				prefix = 'depature';
			}else if (inParams.id == '#tblInboundFlights'){
				prefix = 'return';
			}
			var checked = '';
			if(rowObject.selected && rowObject.seatAvailable){
				checked = "checked='checked'";
			}
			
			var disabled = '';
			if(!rowObject.seatAvailable){
				disabled = " disabled='disabled' ";
			}
			var ttem = index +",'"+ inParams.id.replace("#","") +"'";
			return '<input type="radio" id="' + index + prefix + 'Selection" name="' + prefix + 'Selection" '	+ checked +  disabled +'  value="' + index + '" onclick="selrowBy('+ttem+')"/>';
		}
		
		var navButton = function(cellvalue, options, rowObject) {
			fromViewBCFare = true;
			allFlights[options.rowId - 1] = rowObject;
	        return '<input onclick="UI_tabSearchFlights.viewBCFareClick(' + options.rowId  + ');" id="bcView_' + options.rowId + '" type="button"  value="bc avl." >';
	    }
		
		var arrBreakFmt = function (arr, options, rowObject){
			var outStr = '';
			var first = true;
			for(var i = 0; i < arr.length ; i++){
				if(!first) outStr  += '<br />';
				outStr +=arr[i];
				first = false;
			}
			return outStr ;
		}
		
		var carrierCodeFmt = function (arr, options, rowObject){
			for(var i = 0; i < arr.length; i++){				
				arr[i] = UI_tabSearchFlights.getBusCarrierCode(arr[i]);
			}
			return arrBreakFmt(arr, options, rowObject);
		}
		
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 110,
			width: 620,
			colNames:['&nbsp;',geti18nData('availability_Segment','segment'), geti18nData('availability_Flight','Flight'), geti18nData('availability_OperatingCarrier','Op'),
			          geti18nData('availability_Departure','Departure') , geti18nData('availability_Arrival','Arrival') , geti18nData('availability_Available','Avl'), '&nbsp;'],
			colModel:[
				{name:'index',  index:'index', width:20, align:"center", formatter:radioButtonFmt, editable: false },
				{name:'ondCode',  index:'ondCode', width:110, align:"left", formatter: segBreakFmt},
				{name:'flightNoList', index:'flightNoList', width:80, align:"center", formatter: arrBreakFmt},
				{name:'carrierList', index:'carrierList', width:40, align:"center", formatter: carrierCodeFmt},
				{name:'depatureList', index:'depatureList', width:110, align:"center", formatter: arrBreakFmt},
				{name:'arrivalList', index:'arrivalList', width:110, align:"center", formatter: arrBreakFmt},
				{name:'availabilityList', index:'availabilityList', width:50, align:"right", formatter: arrBreakFmt},
				{name: 'nav',    index : 'nav', width : 50, align :  "center", formatter : navButton, hidden: hideBCFare}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				if(!$("#" + rowid + inParams.radioID).attr('disabled')){
					$("#" + rowid + inParams.radioID).attr('checked', true);					
					
					if ($("#openReturn").attr('checked') && 
							(!UI_tabSearchFlights.isOpenReturn || UI_tabSearchFlights.openReturnInfo == null)) {
						if(!UI_tabSearchFlights.isRequote()){
							$("#trFareSection").show();
							$("#btnFQ").show();
							$("#btnRequote").hide();
						} else {
							UI_tabSearchFlights.hideFareQuotePane();
						}
					} else {
						UI_tabSearchFlights.hideFareQuotePane();
					}
					
					if(!UI_tabSearchFlights.isRequote()){
						UI_tabSearchFlights.buildFareQuoteHD();
					}else {
						UI_requote.setDisplayViewAllBCButton();
						UI_tabSearchFlights.showAddOndButton();
					}
					UI_tabSearchFlights.checkPrivileges();
					if(UI_tabSearchFlights.isRequote()){
						UI_requote.setDisplayViewAllBCButton();
					}
					UI_tabSearchFlights.checkPrivileges();
					
					
					UI_tabSearchFlights.resetLogicalCabinClassSelection(rowid, inParams.id);
					UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = false;
					UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = false;
					UI_tabSearchFlights.segWiseBookingClassOverride = {};
				}else{
					$(inParams.id).resetSelection();
				}
			}
		});
	}		
	
	
// UI_tabSearchFlights.constructAllONDGrid = function(){
// $('#tblAllONDs').jqGrid({
// datatype: "local",
// height: 75,
// width: 655,
// colNames:[ '&nbsp;', 'Segment', 'Flight', 'Op', 'Departure', 'Arrival',
// 'Avl.'],
// colModel:[
// {name:'index', index:'index', width:20, align:"center",
// formatter:radioButtonFmt },
// {name:'ondCode', index:'ondCode', width:110, align:"left", formatter:
// segBreakFmt},
// {name:'flightNoList', index:'flightNoList', width:80, align:"center",
// formatter: arrBreakFmt},
// {name:'carrierList', index:'carrierList', width:50, align:"center",
// formatter: carrierCodeFmt},
// {name:'depatureList', index:'depatureList', width:100, align:"center",
// formatter: arrBreakFmt},
// {name:'arrivalList', index:'arrivalList', width:100, align:"center",
// formatter: arrBreakFmt},
// {name:'availabilityList', index:'availabilityList', width:60, align:"right",
// formatter: arrBreakFmt}
// ],
// imgpath: UI_commonSystem.strImagePath,
// multiselect: false,
// viewrecords: true,
// onSelectRow: function(rowid){
// if(!$("#" + rowid + inParams.radioID).attr('disabled')){
// $("#" + rowid + inParams.radioID).attr('checked', true);
// UI_tabSearchFlights.hideFareQuotePane();
// if(!UI_tabSearchFlights.isRequote()){
// UI_tabSearchFlights.buildFareQuoteHD();
// }else {
// UI_tabSearchFlights.showAddOndButton();
// }
// UI_tabSearchFlights.resetLogicalCabinClassSelection(rowid, inParams.id);
// if( inParams.id == '#tblOutboundFlights'){
// UI_tabSearchFlights.isOutboundFlexiSelected = true;
// }else if (inParams.id == '#tblInboundFlights'){
// UI_tabSearchFlights.isInboundFlexiSelected = true;
// }
// }else{
// $(inParams.id).resetSelection();
// }
// }
// });
// }
	
	UI_tabSearchFlights.showAddOndButton = function (){
		$("#trAddOND").show();
	}
	
	UI_tabSearchFlights.isOutBoundSegment = function (segCode){
		$.each(UI_tabSearchFlights.flightInfo, function(i, flightInfo){
			var fltSegCode = flightInfo.flightRefNumber.split("$")[1];
			if(segCode == fltSegCode){
				if(flightInfo.returnFlag){
					return false;
				} else {
					return true;
				}
			}
		});
	}

	/*
	 * Initializations
	 */
	UI_tabSearchFlights.initialization = function(){
		UI_commonSystem.initializeMessage();
		
		$("#frmSrchFlt").reset();
		
		if ((typeof(oldResAdultCount) != "undefined" && oldResAdultCount > 0)
			|| (typeof(oldResChildCount) != "undefined" && oldResChildCount > 0)) {
			$("#adultCount").val(oldResAdultCount);
			$("#childCount").val(oldResChildCount);
			$("#infantCount").val(oldResInfantCount);
		} else {
			$("#adultCount").val(1);
			$("#childCount").val(0);
			$("#infantCount").val(0);
		}
	
		if(UI_tabSearchFlights.multicity){
			$("#departureVariance_1").val(top.strDefDV);
		}
		
		$("#departureVariance").val(top.strDefDV);
		$("#returnVariance").val(top.strDefDV);
		$("#departureDate").val(top.strSysDate);
		
		$("#selectedCurrency").val(DATA_ResPro.initialParams.defaultCurrencyCode);
		$("#classOfService").val(top.strDefCos);
		
		top.bookingType = ""; // TODO - If the variable available in the top
								// page remove this line
		top.bookingType = top.bookingType == "" ? top.arrBookingTypes[0][0] : top.bookingType;
		$("#bookingType").val(top.bookingType);
		
		// $("#classOfService").disable();
		// Handle transfer segment
// if (UI_commonSystem.strPGMode != "transferSegment") {
// $("#btnBook").hide();
// }
		$("#btnBook").hide();
		if (UI_commonSystem.strPGMode == '' && UI_commonSystem.strPGMode == "modifySegment"){
			UI_tabSearchFlights.resetOpenReturnControls(true);
		}
		$("#btnFQ").enable();
		$("#returnDate").enable();
		$('#returnDate').datepicker().datepicker('enable')
		
		if(!UI_tabSearchFlights.isRequote()){
			$("#divResultsPane").hide();
		} else {		
			UI_requote.reset();
			$("#tblIBOBFlights").hide();
		}
		$("#divTaxBD").hide();
		$("#divSurBD").hide();
		$("#divBreakDownPane").hide();
		
		$("#fAirport").focus();
		UI_tabSearchFlights.hideAirportMessage();
	}
	
	/*
	 * Auto Date generation
	 */
	UI_tabSearchFlights.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 :
				if($.trim($("#departureDate").val()) != ''){
					// $("#departureDate").val(dateChk($("#departureDate").val()));
					dateChk("departureDate");
				}else{
					$("#departureDate").val('');
				}
				break;
			case 1 : 
				if($.trim($("#returnDate").val()) != ''){
					// $("#returnDate").val(dateChk($("#returnDate").val()));
					dateChk("returnDate");
				}else{
					$("#returnDate").val('');
					//Set inbound transit to 0 in all hubs
					$.each(UI_tabSearchFlights.hubTimeDetailJSON, function(){
						this.inboundStopOverTime = 0;
					});
					UI_tabSearchFlights.createStopOverStr();
				}
				break;
		}
	}
	 // FIXME remove this
	UI_tabSearchFlights.triggerSearch = function(externalParams) {
		
		$("#trSearchParams").hide();
		$("#trExtSearchMessage").show();

		if(externalParams != null) {		
			if(externalParams.fromAirport != null) {
				$("#fromAirport").val(externalParams.fromAirport);
				// $("#fAirport").val(externalParams.fromAirport);
				UI_tabSearchFlights.fromCombo.setValue(externalParams.fromAirport);
				// $('[name=searchParams\\.fromAirport__sexyCombo]').val($("#fromAirport
				// option:selected").text());
			}

			if(externalParams.toAirport != null) {
				$("#toAirport").val(externalParams.toAirport);
				// $("#tAirport").val(externalParams.toAirport);
				UI_tabSearchFlights.toCombo.setValue(externalParams.toAirport);
				// $('[name=searchParams\\.toAirport__sexyCombo]').val($("#toAirport
				// option:selected").text());
			}
			
			if(externalParams.departureDate != null) $("#departureDate").val(externalParams.departureDate);
			if(externalParams.returnDate != null) $("#returnDate").val(externalParams.returnDate);
			if(externalParams.departureVariance != null) $("#departureVariance").val(externalParams.departureVariance);
			if(externalParams.returnVariance != null) $("#returnVariance").val(externalParams.returnVariance);
			
			if(externalParams.adultCount != null) $("#adultCount").val(externalParams.adultCount);
			if(externalParams.childCount != null) $("#childCount").val(externalParams.childCount);
			if(externalParams.infantCount != null) $("#infantCount").val(externalParams.infantCount);
			
			if(externalParams.selectedCurrency != null) $("#selectedCurrency").val(externalParams.selectedCurrency);
			if(externalParams.classOfService != null) $("#classOfService").val(externalParams.classOfService);
			if(externalParams.bookingType != null) $("#bookingType").val(externalParams.bookingType);
			if(externalParams.redirectedFromV1 != null) $("#redirectedFromV1").val(externalParams.redirectedFromV1);
			if(externalParams.searchOption != null) $("#selSearchOptions").val(externalParams.searchOption);
			
			UI_tabSearchFlights.searchOnClick();
		}
	}

    // FIXME me clean up
	UI_tabSearchFlights.showInterlineSearch = function(externalParams) {
		$("#redirectedFromV1").val("false");
		$("#trSearchParams").show();
		$("#trExtSearchMessage").hide();
		$("#btnBackToAirline2").show();
	} 
	 // FIXME me clean up
	UI_tabSearchFlights.setSearchOption = function() {
		if (UI_commonSystem.strPGMode != "") {
			$("#selSearchOptions").val("V2");
		}
	}
	
	UI_tabSearchFlights.loadAirportMessage = function() {
		var html = '<a href="javascript:void(0)" onclick="UI_tabSearchFlights.hideAirportMessage()"><img border="0" src="../images/A150_no_cache.gif" title="Close" align="right"><\/a> ';
		var apMsg = UI_tabSearchFlights.airportMessage;		
		$('#airportMsg').html('');	
		$('#airportMsg').append(html + apMsg);
		
		if(apMsg != null && apMsg != ''){
			$('#airportMsg').slideDown('slow');
		} else {
			$('#airportMsg').hide();
		}	
		
	}
	
	UI_tabSearchFlights.hideAirportMessage = function(){		
		$("#airportMsg").hide();
	}
	
	UI_tabSearchFlights.getOperationType =  function() {
		if (UI_commonSystem.strPGMode == "addSegment"){
			return "ADS_BLS";
		} else if (UI_commonSystem.strPGMode == "transferSegment") {
			return "TNS_BLS";
		} else if (UI_commonSystem.strPGMode == "modifySegment"){
			return "MDS_RES";// Modify reservation
		} else {
			return "CRE_BLS";// Create reservation
		}
	}
	
	UI_tabSearchFlights.replaceHTMLData =  function(htmlData) {
		var delimitedData = '';
		
		if (htmlData != null) {
			delimitedData = htmlData.replace('<br>', ','); 
		}
		
		return delimitedData;	
	}
	
	UI_tabSearchFlights.createSubmitData =  function(params) {
		if(UI_tabSearchFlights.isRequote()){
			params.data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			params.data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;
			params.data['resSegments']= $.toJSON(DATA_ResPro.resSegments);
			UI_tabSearchFlights.PNR = DATA_ResPro.reservationInfo.PNR;
			params.data['addGroundSegment'] = UI_requote.addGroundSegment;
			params.data['selectedFltSeg'] = UI_tabSearchFlights.replaceHTMLData(UI_requote.selectedFltSeg);
			if(UI_requote.addGroundSegment){
				params.data['addSegment'] = true;	
			}		 
			
		} else if (params.mode == "addSegment" ) {
			params.data['addSegment'] = true;
			params.data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			params.data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;				
			params.data['resSegments']= $.toJSON(DATA_ResPro.resSegments);
			if(DATA_ResPro.addGroundSegment && DATA_ResPro.addGroundSegment == "true"){
				params.data['addGroundSegment'] = true;
			}	
			UI_tabSearchFlights.PNR = DATA_ResPro.reservationInfo.PNR;
			// Wrong meaning for identifier name "selectedPnrSeg". It contains
			// selected flight segment ids. Hence using it from here.
			params.data['selectedFltSeg'] = DATA_ResPro.selectedPnrSeg;
		} else if (params.mode == "transferSegment") {
			params.data['transferSegment'] = true;
			// params.data['pnr'] = DATA_ResPro.transferSegment.PNR;
			params.data['groupPNR'] = DATA_ResPro.transferSegment.groupPNR;
			params.data['modifyingSegmentOperatingCarrier'] = DATA_ResPro.transferSegment.modifyingSegmentOperatingCarrier;
			// params.data['resSegments']= $.toJSON(DATA_ResPro.resSegments);
			UI_tabSearchFlights.PNR = DATA_ResPro.transferSegment.PNR;
		} else 	if (UI_commonSystem.strPGMode != "") {
			params.data['modifyBooking'] = true;
			params.data['pnr'] = DATA_ResPro.modifySegments.PNR;
			params.data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
			params.data['modifySegment'] = DATA_ResPro.modifySegments.modifyingSegments;
			params.data['selCurrency'] = UI_modifySegment.selCurrency;
			params.data['resSegments']= $.toJSON(DATA_ResPro.resSegments);
			UI_tabSearchFlights.jsonResSegments = $.toJSON(DATA_ResPro.resSegments);
			UI_tabSearchFlights.PNR = DATA_ResPro.modifySegments.PNR;
		}
	}
	
	UI_tabSearchFlights.applyDiscountPop = function(){
		$("#fareDiscountPercentage").val(UI_tabSearchFlights.fareDiscount.percentage);
		$("#fareDiscountNotes").val(UI_tabSearchFlights.fareDiscount.notes);
		$("#selDiscountType").val(UI_tabSearchFlights.fareDiscount.code);
		UI_commonSystem.readOnly(true);
		$("#divFareDiscountpop").show();
		if (!showFareDiscountCodes) {
			$("#discountCodeList").hide();
		}
	}
	UI_tabSearchFlights.closeDiscuntPop = function(){
		$("#divFareDiscountpop").hide();
		UI_commonSystem.readOnly(false);
	}
	
	UI_tabSearchFlights.checkPrivileges = function(){
		if(!DATA_ResPro.initialParams.overideFare){
			$("#btnCFare").hide();
			$("#btnCAllFare").hide();
		} else {
			if(UI_tabSearchFlights.isAllowChangeFare){				
				if(DATA_ResPro.initialParams.allCoSChangeFareEnabled && UI_commonSystem.strPGMode != "cancelSegment"){
					$("#btnCFare").hide();
					$("#btnCAllFare").decorateButton();
					$("#btnCAllFare").show();
				} else {
					$("#btnCFare").decorateButton();
					$("#btnCFare").show();
					$("#btnCAllFare").hide();
				}
			}
		}
	};
	
	UI_tabSearchFlights.flightNumberLabeling = function(dataSet, flg){
		if (DATA_ResPro.initialParams.fltStopOverDurInfoToolTipEnabled){
			var tpemList = [];
			var tDataSet = $.extend(true, [], dataSet);
			$.each(tDataSet, function(i,obj){
				if (typeof (obj.flightNoList) == 'object'){
					var t = [];
					$.each(obj.flightNoList, function(j, flItem){
						var tlabel = "<span class='fl-Details' id='fld_"+flg+"_"+obj.index+"_"+j+"' style='cursor:pointer'><img src='../images/info_no_cache.gif' alt='i'/> </span>"+flItem;
						t[j] = tlabel;
					});
					obj.flightNoList = t;
				}
				tpemList[tpemList.length] = obj;
			});
			return tpemList;
		}else{
			return dataSet;
		}
	};
	
	UI_tabSearchFlights.flightNumberLabelingEventBinding = function(){
		$(".fl-Details").unbind("mouseover").bind("mouseover",function(){
			UI_tabSearchFlights.displayFlightAddInfoToolTip(this.id.split("_")[1], this.id.split("_")[2], this.id.split("_")[3]);
		});
		$(".fl-Details").unbind("mouseout").bind("mouseout",function(){
			UI_tabSearchFlights.hideFlightAddInfoToolTip();
		});
	};
	
	/** Flight Info tool tip display */
	UI_tabSearchFlights.displayFlightAddInfoToolTip = function(flg,rid,flIndex) {
		  var sHtm  ="";
		  if (rid != "") {
			  var id = (new  Number(rid)) - 1;
			  
			  var fltDuration = "";
			  var fltModelDesc = "";
			  var fltModel = "";
			  var segmentCode = "";
			  var flightNO = "";
  			  var specialNote = "";
  			  var strData = "";
  			  var stopOverDuration ="";
  			  var arrivalTerminalName="";
			  var departureTerminalName="";
			  var csOcCarrierCode="";
			
			if(flg=="OB"){
				fltDetails = jsonOutFlts[id];
			}else if(flg=="IB"){
				fltDetails = jsonRetFlts[id];
			}
  				  
			fltDuration = fltDetails.flightDurationList[flIndex];
			fltModelDesc = fltDetails.flightModelDescriptionList[flIndex];
			fltModel = fltDetails.flightModelNumberList[flIndex];
			segmentCode = fltDetails.segmentCodeList[flIndex];
			flightNO = fltDetails.flightNoList[flIndex];
			specialNote = fltDetails.remarksList[flIndex];					  
			stopOverDuration = fltDetails.flightStopOverDurationList[flIndex];
			noOfStops = fltDetails.noOfStopsList[flIndex];
			csOcCarrierCode = fltDetails.csOcCarrierList[flIndex];
			if (typeof(fltDetails.arrivalTerminal) != "undefined") {
				arrivalTerminalName = fltDetails.arrivalTerminal[flIndex];
			}
			if (typeof(fltDetails.departureTerminal) != "undefined") {
				departureTerminalName = fltDetails.departureTerminal[flIndex];
			}
  				
  			  
 			    strData += "<font>Flight No&nbsp;:&nbsp;"+flightNO+"</font><br>";
				strData += "<font>Segment Code&nbsp;:&nbsp;"+segmentCode+"</font><br>";
				strData += "<font>Arrival Terminal&nbsp;:&nbsp;"+arrivalTerminalName+"</font><br>";	
				strData += "<font>Departure Terminal&nbsp;:&nbsp;"+departureTerminalName+"</font><br>";	
				strData += "<font>Flight Duration&nbsp;:&nbsp;"+fltDuration+"</font><br>";
				strData += "<font>No of Stops&nbsp;:&nbsp;"+noOfStops+"</font><br>";
				strData += "<font>Transit Duration&nbsp;:&nbsp;"+stopOverDuration+"</font><br>";
				// strData += "<font>Aircraft
				// Model&nbsp;:&nbsp;"+fltModel+"</font><br>";
				strData += "<font>Aircraft Model&nbsp;:&nbsp;"+fltModelDesc+"</font><br>";
				strData += "<font>Additional Note&nbsp;:&nbsp;"+specialNote+"</font><br>";
				if(typeof(csOcCarrierCode) != "undefined" && csOcCarrierCode != "" && csOcCarrierCode != null){
					strData += "<font>Operated By&nbsp;:&nbsp;"+csOcCarrierCode+"</font><br>";						
				}				
  			sHtm += strData;			  
			  				
		  } 
		  $("#flightAddInfoToolTipBody").html(sHtm);
		  var pos = $("#fld_" +flg+"_"+ rid+"_"+flIndex).offset();	   
	    
	    
	    $("#flightAddInfoToolTip").css({
	        left: (pos.left-100) + 'px',
	        top: pos.top + 20 + 'px'
	       
	    });      
	   $("#flightAddInfoToolTip").show(); 	  
		}

	UI_tabSearchFlights.hideFlightAddInfoToolTip = function() {
		  $("#flightAddInfoToolTip").hide();
	}
	
	UI_tabSearchFlights.fareAvailable = function(availableFare){
		if(availableFare==null || availableFare.fareQuoteTO == null || availableFare.fareQuoteTO.length==0){
			return false;
		}
		return true;
	}
	
	// ********************** EXT JS extention for combo box
	// **********************************//
	$(document).ready(function() {
		Ext.override(Ext.form.ComboBox, {
		    filter : function(value){
		    	if(this.filterFn){
		    		return this.store.filterBy(this.filterFn.createDelegate(this, [value], true));
		    	}
		    	var regExp = new RegExp("^"+value+".*","i");
		    	return this.store.filter(this.displayField, regExp, this.anyMatch, this.caseSensitive);
		    },
		    doQuery : function(q, forceAll){
		        q = Ext.isEmpty(q) ? '' : q;
		        var qe = {
		            query: q,
		            forceAll: forceAll,
		            combo: this,
		            cancel:false
		        };
		        if(this.fireEvent('beforequery', qe)===false || qe.cancel){
		            return false;
		        }
		        q = qe.query;
		        forceAll = qe.forceAll;
		        if(forceAll === true || (q.length >= this.minChars)){
		            if(this.lastQuery !== q){
		                this.lastQuery = q;
		                if(this.mode == 'local'){
		                    this.selectedIndex = -1;
		                    if(forceAll){
		                        this.store.clearFilter();
		                    }else{
		                    	this.store.filter(this.displayField, q, this.anyMatch);
		                    }
		                    this.onLoad();
		                }else{
		                    this.store.baseParams[this.queryParam] = q;
		                    this.store.load({
		                        params: this.getParams(q)
		                    });
		                    this.expand();
		                }
		            }else{
		                this.selectedIndex = -1;
		                this.onLoad();
		            }
		        }
		    }
		});
	
	});
	
	
	// ##################### Logical Cabin Class Related Methods  #######################
	
	UI_tabSearchFlights.createLogicalCabinAvailability = function(response){
		UI_tabSearchFlights.clearLogicalCCData();
		UI_tabSearchFlights.recreateAvailableLogicalCCInfoList(response);
		UI_tabSearchFlights.recreateAvailableBookingClassInfoList(response);
		
		for(var ondSeq = 0; ondSeq < UI_tabSearchFlights.availableOndLogicalCCInfo.length; ondSeq++){
			var logicalCcInfoObj = UI_tabSearchFlights.availableOndLogicalCCInfo[ondSeq];
			var tblId =  'tblLogicalCabins_' + ondSeq;
			var tdOndConfig = 'tdLogicalCabins_' + ondSeq;
			
			var parentElem = '';
			if(UI_tabSearchFlights.isRequote()){
				parentElem = $('#tdRQLogicalCCList');
			} else if(UI_tabSearchFlights.isMulticity()){
				parentElem = $('#OnDLogicalCCBody_' + ondSeq);
			} else {
				if(ondSeq == UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND){
					parentElem = $('#tdOutBoundLogicalCabins');
				} else {
					parentElem = $('#tdInBoundLogicalCabins');
				}
			}
			
			if(logicalCcInfoObj != null && logicalCcInfoObj.length != 0){
				$.each(logicalCcInfoObj, function(i, obj){
					var ondCode = obj.ondCode;			
					var availLogicalCabins = obj.availableLogicalCCList;
					if(availLogicalCabins != null && availLogicalCabins.length > 0){
						availLogicalCabins.sort(function (cabin1, cabin2) {
							return (cabin1.bundledFareDelta || 0) - (cabin2.bundledFareDelta || 0);
						});
						UI_tabSearchFlights.setDefaultLogicalCCSelection(availLogicalCabins, ondSeq, ondCode);
						var renamedOndCode = ondCode.replace(/\//g,"_");

						var divObj = $('#' + tdOndConfig);
						if(!divObj.length){								
							divObj = $("<div></div>").attr({
								'id':tdOndConfig
							});
						} else {
							divObj.empty();
						}

						parentElem.append(divObj);

						var tblObj = $("<table></table>").attr({
							"class":"scroll",
							"cellpadding":"0",
							"cellspacing":"0",
							"id":tblId
						});

						divObj.append(tblObj);
						tblObj.GridUnload();
						UI_tabSearchFlights.constructLogicalCabinGrid({
							id:"#" + tblId, 
							ondSequence: ondSeq, 
							ondCode: renamedOndCode, 
							reqOndSequence: obj.requestedOndSequence});
						$('#' + tblId).width("100%");
						UI_commonSystem.fillGridData({id:"#" + tblId, data:availLogicalCabins});
						UI_tabSearchFlights.setSelectedLogicalCabinGridRow(availLogicalCabins, "#" + tblId);
						//if(UI_tabSearchFlights.isRequote()){								
						//	$.each(UI_tabSearchFlights.ondRebuildLogicalCCAvailability, function(ondSequence, val){
						//		UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSequence] = true;
						//	});
						//}
					}
				});

				$("#" + tdOndConfig + " .ui-jqgrid-hbox").hide();
				$("#" + tdOndConfig + " .ui-icon-circle-triangle-n").hide();
				$("#" + tdOndConfig).show();
				if(top.isLogicalCCEnabled){
					parentElem.show();				
				}else{
					parentElem.hide();
				}
			} else {
				$("#" + tdOndConfig).hide();
			}				
		}
		
		if(UI_tabSearchFlights.isRequote()){
			$(tblAllONDs).jqGrid().setSelection(1, true);
		}

		if (!UI_tabSearchFlights.isRequote() && UI_commonSystem.strPGMode == "transferSegment"){
			$("#tdOutBoundLogicalCabins").hide();
			$("#tdInBoundLogicalCabins").hide();
		}
		
	}
	
	UI_tabSearchFlights.showLogicalCabinInfo = function(showOnd){
		for(var ondSeq = 0; ondSeq < UI_tabSearchFlights.availableOndLogicalCCInfo.length; ondSeq++){
			var tdOndConfig = 'tdLogicalCabins_' + ondSeq;
			var showLogicalCC = false;
			for(var i = 0; i < UI_tabSearchFlights.availableOndLogicalCCInfo[ondSeq].length; i++){
				if(UI_tabSearchFlights.availableOndLogicalCCInfo[ondSeq][i].requestedOndSequence == showOnd){
					showLogicalCC = true;
					break;
				}
			};
			if(showLogicalCC){				
				$("#" + tdOndConfig).show();
			}else{
				$("#" + tdOndConfig).hide();
			}			
		}		
	}

	UI_tabSearchFlights.constructLogicalCabinGrid = function(params){
		isLogicalCCSelected = false;
		
		var actualOndCode = params.ondCode.replace(/_/g,"/");
		var ondSequence = params.ondSequence;
		
		var radioButtonFmt = function(index, options, rowObject){				
						
			var checked = '';
			if(rowObject.selected){
				checked = " checked='checked' ";
			}
				
			var ttem = options.rowId +",'"+ params.id.replace("#","") +"'";
			return '<input type="radio" id="' + options.rowId + '_' + params.ondSequence + '" name="' + params.ondSequence + '_Selection" '	
				+ checked +' value="' + options.rowId + '" onclick="selrowBy('+ttem+')"/>'
		}
		
		var commentFmt = function(index, options, rowObject) {
		    var tClass = "";
		    var tDelta = "";
		    var baggageInfohtml = "";
		    var displayString = "";

		    if (rowObject.bundledFareDelta !== null && rowObject.bundledFareDelta !== 0) {

		        if (rowObject.bundledFareDelta > 0) {
		            tDelta = "&nbsp;<b style='padding-left: 5px;'>+" + Math.round(Math.abs(rowObject.bundledFareDelta)) + "</b>";
		        } else {
		            tDelta = "&nbsp;<b style='padding-left: 5px;'>-" + Math.round(Math.abs(rowObject.bundledFareDelta)) + "</b>";
		        }
		    }

		    if (UI_tabSearchFlights.displayBasedOnTemplates) {

		        if (!(jQuery.isEmptyObject(rowObject.bundleFareDescriptionTemplateDTO))) {
		        	
		        	var paidContent = rowObject.bundleFareDescriptionTemplateDTO.defaultPaidServicesContent;
		        	var freeContent = rowObject.bundleFareDescriptionTemplateDTO.defaultFreeServicesContent;

		        	if (!jQuery.isEmptyObject(rowObject.bundleFareDescriptionTemplateDTO.translationTemplates)) {
		        	    if (!(rowObject.bundleFareDescriptionTemplateDTO.translationTemplates[top.selectedLangunage] === undefined)) {
		        	        freeContent = (rowObject.bundleFareDescriptionTemplateDTO.translationTemplates[top.selectedLangunage].freeServicesContent != "") ? 
		        	        		rowObject.bundleFareDescriptionTemplateDTO.translationTemplates[top.selectedLangunage].freeServicesContent : freeContent;
		        	        
		        	        paidContent = (rowObject.bundleFareDescriptionTemplateDTO.translationTemplates[top.selectedLangunage].paidServicesContent != "") ? 
		        	        		rowObject.bundleFareDescriptionTemplateDTO.translationTemplates[top.selectedLangunage].paidServicesContent : paidContent;
		        	    }
		        	}
		            if (paidContent && paidContent !== "") {

						var startPoint = paidContent.indexOf('<a href="BAGGAGE">');
						if (startPoint != -1) {
							var commentSplit = paidContent.substring(startPoint);
							var newStartPoint = 18; // # of charaters in: <a href = 'BAGGAGE'
							var endPoint = commentSplit.indexOf("</a>");
							displayString = commentSplit.substring(newStartPoint, endPoint);
						}
						// to remove the styles, content will be written to a hidden div and value is retrieved
					}

					$("#bundleDescSetterTempDiv").html(freeContent);

					var bundleDescFreeContentArray = $("#bundleDescSetterTempDiv").text().split("\n");
					var bundleContentDisplayText = "";
					$.each(bundleDescFreeContentArray, function (i, item) {

						if (item != "") {
							if (bundleContentDisplayText == "") {
								bundleContentDisplayText = item;
							} else {
								bundleContentDisplayText += "," + item;
							}
						}

					});
					$("#bundleDescSetterTempDiv").html("");

					baggageInfohtml = "<br><div style='white-space: nowrap;width: 176px;overflow: hidden;" +
						"text-overflow: ellipsis;' class='commentToolTip' role='" + bundleContentDisplayText + "'>"
						+ bundleContentDisplayText + "</div>";

					var baggageHrefText = '';

					if (displayString != "") {
						var fareClassId = 'fareClassId' + index + "_" + Math.random().toString(36).substr(2, 5);
						fareClassId = fareClassId.split(' ').join('');
						fareClassId = fareClassId.replace(/[^\w\s]/gi, '');

						var baggageParam = {
							"logicalCCCode": rowObject.logicalCCCode,
							"ondCode": actualOndCode,
							"ondSequence": ondSequence,
							"bundledFarePeriodId": rowObject.bundledFarePeriodId,
							"bookingClasses": rowObject.bookingClasses
						};

						UI_tabSearchFlights.baggageSearchParams[fareClassId] = baggageParam;

						baggageHrefText = '<div style="position:relative;float:right;"><span>' +
							'<a href="javascript:void(0)" class="baggageToolTip" id="' + fareClassId + '">' +
							displayString + '</a></span></div>';
					}
					baggageInfohtml = baggageHrefText + baggageInfohtml;
				}
		    }

		    if (rowObject.comment != null && !UI_tabSearchFlights.displayBasedOnTemplates) {
		        tClass = "<img src='../images/info_no_cache.gif' border='0' alt=''/>";
		    }

		    return "<strong>" + rowObject.logicalCCDesc + "</strong>" + " <a href='javascript:void(0)' role='" + rowObject.comment + "' class='commentToolTip' style='position:relative' >" +
		        tClass + "</a>" + tDelta + baggageInfohtml;
		}

				
		$(params.id).jqGrid({
			datatype: "local",
			height: 110,
			width: 250,
			colNames:[ '&nbsp;', 'Logical Cabin Class'],
			colModel:[
				{name:'index', width:20, align:"center", formatter:radioButtonFmt },
				{name:'logicalCCDesc',  index:'logicalCCDesc', width:75, align:"left", formatter:commentFmt}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				var elementId = "#" + rowid + "_" + params.ondSequence;
				if(!$(elementId).attr('disabled')){
					isLogicalCCSelected = true;
					var logicalCCData = UI_tabSearchFlights.getSelectedAvailableLogicalCCList(actualOndCode, params.ondSequence);
					var selectedLogicalCCObj = logicalCCData[rowid -1];
					var selectedLogicalCC = selectedLogicalCCObj.logicalCCCode;
					
					$(elementId).attr('checked', true);
					UI_tabSearchFlights.hideFareQuotePane();
					UI_tabSearchFlights.buildFareQuoteHD();
					UI_tabSearchFlights.ondRebuildLogicalCCAvailability[params.reqOndSequence] = false;
					UI_tabSearchFlights.setSelectedLogicalCCOption(logicalCCData, selectedLogicalCCObj)
					UI_tabSearchFlights.setSelectedLogicalCabinClassCode(selectedLogicalCC, params.ondSequence, params.reqOndSequence, actualOndCode);
					
					if(selectedLogicalCCObj.bundledFarePeriodId != null || selectedLogicalCCObj.bundledFarePeriodId != ''){
						UI_tabSearchFlights.segWiseBookingClassOverride[params.ondSequence] = null;
						UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability[params.ondSequence] = true;
						if(selectedLogicalCCObj.bookingClasses != null && selectedLogicalCCObj.bookingClasses.length > 0){
							var bkClassNum = selectedLogicalCCObj.bookingClasses.length;
							if(bkClassNum == 1){
								UI_tabSearchFlights.ondWiseBookingClassSelected[params.reqOndSequence] = selectedLogicalCCObj.bookingClasses[0];
							} else {
								UI_tabSearchFlights.segWiseBookingClassOverride[params.reqOndSequence] = selectedLogicalCCObj.segmentBookingClasses;
							}
						}
					}
					
					// Set flexi selection
					if(selectedLogicalCCObj.withFlexi && selectedLogicalCCObj.bundledFarePeriodId){
						UI_tabSearchFlights.ondWiseFlexiSelected[params.reqOndSequence] = true;
					} else {
						UI_tabSearchFlights.ondWiseFlexiSelected[params.reqOndSequence] = false;						
					}
					
					//set bundledFarePeriodId ondWiseBundleFareSelected
					UI_tabSearchFlights.ondWiseBundleFareSelected[params.reqOndSequence] = selectedLogicalCCObj.bundledFarePeriodId;	
				
					if(UI_tabSearchFlights.isOpenReturn){
						//Always rebuild inbound available logical cabin class if open return search to
						//get the correct selected logical cabin class
						UI_tabSearchFlights.ondRebuildLogicalCCAvailability[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = true;
						
						//Set outbound flexi quote option to inbound ond if open return search 
						UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = UI_tabSearchFlights.ondWiseFlexiSelected[params.ondSequence];
						
						//Set outbound bundled fare option to inbound ond if open return search 
						//UI_tabSearchFlights.ondWiseBundleFareSelected[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = UI_tabSearchFlights.ondWiseBundleFareSelected[params.ondSequence];
					}					
					
				}else{
					$(params.id).resetSelection();
				}
			}
		});
		$(params.id).setCaption('Segment - ' + actualOndCode);
	}
	
	UI_tabSearchFlights.createBaggageRatesParams = function(fareClassId){
		
	 	var varName = 'fareQuoteParams';
		var searchParams = UI_tabSearchFlights.baggageSearchParams[fareClassId];

		var logicalCCSelection = {};
		logicalCCSelection[0]=searchParams.logicalCCCode;
		
		var ondLogocalMap = {};
		ondLogocalMap[ searchParams.ondCode] = searchParams.logicalCCCode;
		
		var ondSegLogicalCCSelection = {};
		ondSegLogicalCCSelection[0]=ondLogocalMap;
		
		var ondSegBookingClassSelection = {};
		var index = 0;
		$.each(UI_tabSearchFlights.ondSegWiseBookingClassSelection, function(i,item){
			if(item[searchParams.ondCode]!= null){
				ondSegBookingClassSelection[0] = null;//UI_tabSearchFlights.ondSegWiseBookingClassSelection[i];
				index = i;
			}
		});
	 	
	 	var preferredBundledFares = {};
		preferredBundledFares[0]=searchParams.bundledFarePeriodId
		var ondArry = (searchParams.ondCode).split("/");
		var fromAirport = ondArry[0];
		var toAirport = ondArry[ondArry.length-1];
	 	var pData = {};
		
	 	pData[varName+".fromAirport"]	= fromAirport;
	 	pData[varName+".toAirport"]		= toAirport;
	 	if(UI_tabSearchFlights.isMulticity()){
			pData[varName+".bookingType"]	= UI_tabSearchFlights.multiCityRequoteFightRPHList[index].bookingType;
		}else{
			pData[varName+".bookingType"]	= UI_tabSearchFlights.objResSrchParams.bookingType;
			if(UI_tabSearchFlights.objResSrchParams.fromAirport == fromAirport && UI_tabSearchFlights.objResSrchParams.toAirport == toAirport){
				pData[varName+".departureDate"]	= UI_tabSearchFlights.objResSrchParams.departureDate;
			}else{
				pData[varName+".departureDate"]	=(UI_tabSearchFlights.objResSrchParams.returnDate==null?'':UI_tabSearchFlights.objResSrchParams.returnDate);
			}
		}
	 	pData[varName+".adultCount"]	= UI_tabSearchFlights.objResSrchParams.adultCount;
	 	pData[varName+".classOfService"]= UI_tabSearchFlights.objResSrchParams.classOfService;
	 	pData[varName+".logicalCabinClass"] = UI_tabSearchFlights.objResSrchParams.logicalCabinClass;
	 	pData[varName+".selectedCurrency"]= UI_tabSearchFlights.objResSrchParams.selectedCurrency;
	 	pData[varName+".paxType"]		= UI_tabSearchFlights.objResSrchParams.paxType;
	 	pData[varName+".fareType"]		= UI_tabSearchFlights.objResSrchParams.fareType;
	 	pData[varName+".searchSystem"]	= UI_tabSearchFlights.getSelectedSystem(); //UI_tabSearchFlights.objResSrchParams.searchSystem;
	 	pData[varName+'.promoCode'] = UI_tabSearchFlights.objResSrchParams.promoCode;
	 	pData[varName+'.fareQuoteSegWiseLogicalCCSelection'] = $.toJSON(UI_tabSearchFlights.segWiseLogicalCabinClassSelection);
	 	pData[varName+'.bankIdentificationNo'] = UI_tabSearchFlights.objResSrchParams.bankIdentificationNo;
	 	pData[varName+'.fareQuoteLogicalCCSelection'] = $.toJSON(logicalCCSelection);
	 	pData[varName+'.fareQuoteOndSegLogicalCCSelection'] = $.toJSON(ondSegLogicalCCSelection); 
	 	pData[varName+'.fareQuoteOndSegBookingClassSelection'] = $.toJSON(ondSegBookingClassSelection);
	 	pData[varName+'.preferredBundledFares'] = $.toJSON(preferredBundledFares);
	 	
	 	var segWiseBookingClassOverride = {};
	 	segWiseBookingClassOverride[0] = UI_tabSearchFlights.segWiseBookingClassOverride[index];
	 	pData[varName+'.ondSegBookingClassStr'] = $.toJSON(segWiseBookingClassOverride);
	 	
	 	var ondWiseCitySearch = {};
	 	ondWiseCitySearch[0] = UI_tabSearchFlights.ondWiseCitySearch[searchParams.ondSequence];
	 	pData[varName+'.ondWiseCitySearchStr'] = $.toJSON(ondWiseCitySearch);
	 	
	 	var  flightDataString = {};
	 	if(UI_tabSearchFlights.isMulticity()|| UI_tabSearchFlights.isRequote()){
	 		//ToDo multicity
	 		var flightAr = UI_tabSearchFlights.multiCityRequoteFightRPHList[searchParams.ondSequence].flightRPHList;
	 		var len = flightAr.length;
	 		for (var i = 0;i<len ; i++){
				var temp = {};
				temp["outFlightRPHList["+i+"]"] = flightAr[i];
				pData = $.airutil.dom.concatObjects(pData,temp );
			}
	 		if(pData[varName+".departureDate"]== '' || pData[varName+".departureDate"] == undefined){ 
				var setDate = UI_tabSearchFlights.multiCityRequoteFightRPHList[searchParams.ondSequence].departureDate;
				setDate = setDate.split("T")[0];
				setDate = setDate.split('-').join('/')
				pData[varName+".departureDate"] = setDate;
			}
	 		
	 	}else{
	 		
	 		flightDataString = UI_tabSearchFlights.bagageRatesInOutFlightRPHList();
	 		for (var i = 0;i<3 ; i++){
				var indexString = (searchParams.ondSequence == 0)? "outFlightRPHList["+i+"]":"retFlightRPHList["+i+"]";
				if(flightDataString[indexString] != null){
					var temp = {};
					temp["outFlightRPHList["+i+"]"]=flightDataString[indexString];
					pData = $.airutil.dom.concatObjects(pData,temp );
				}
				
			}
	 	}
		 
	 	if(DATA_ResPro["modifyFlexiBooking"] != undefined &&
	 			DATA_ResPro["modifyFlexiBooking"]){
	 		var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);
	 		$.each(ondFlexiQuote, function(key, value){
	 			ondFlexiQuote[key] = false;
	 		});
	 		pData['fareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(ondFlexiQuote[index]);
	 		pData['fareQuoteParams.ondWiseCitySearchStr'] = $.toJSON(UI_tabSearchFlights.ondWiseCitySearch[index]);
	 	}
	 	return pData;
	 }
	
	UI_tabSearchFlights.baggageInfoRetrieve = function(event,fareClassId){

		if (UI_tabSearchFlights.validateFareQuote()) {
			UI_tabSearchFlights.disableEnablePageControls(false);
			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({
				dataType: 'json', async:true, 
				data:  UI_tabSearchFlights.createBaggageRatesParams(fareClassId), 
				url:"baggageRates.action",
				success: (function(response){
					response = $.extend(response, {"fareClassId":fareClassId,"event": event} );
					UI_tabSearchFlights.returnBaggageRates(response);
				}),
				error:UI_commonSystem.setErrorStatus
			});
		}
		event.stopPropagation();
		return false;
	}
	
	UI_tabSearchFlights.returnBaggageRates = function (response) {
		UI_tabSearchFlights.showBaggageRatesToolTip(response.baggageRatesDTO, response.fareClassId, response.event);
		UI_commonSystem.hideProgress();
	}
	
	UI_tabSearchFlights.showBaggageRatesToolTip = function(baggageRatesDTO, fareClassId, event) {
		//default msg
		var baggageHtml = "<div style='width: 220px;'><table><tbody><tr><tr><td> No Baggage Rates Available </td></tbody></table></div>";
		if(!(jQuery.isEmptyObject(baggageRatesDTO))){
			if (!(jQuery.isEmptyObject(baggageRatesDTO.baggageRates[0]))) {
		        var baggageRates = baggageRatesDTO.baggageRates[0].baggageRates;
		        if (!(jQuery.isEmptyObject(baggageRates))) {

		            sortedBaggageRates = baggageRates.sort(UI_tabSearchFlights.baggageRatesComparator);

		            baggageHtml = "<div style='width: 220px;'><table><tbody><tr>";
		            $.each(sortedBaggageRates, function(i, item) {
		                if (item && item.baggageDescription) {
		                    baggageHtml += "<tr><td style='width: 134px;'>" + item.baggageDescription + "</td><td style='width: 84px;'>" + DATA_ResPro.initialParams.baseCurrencyCode + " " + item.baggageCharge + "</td></tr>";
		                }
		            });
		            baggageHtml += "</tbody></table></div>";
		        }
		    }
		}
        UI_tabSearchFlights.showBaggageTooltip(fareClassId, baggageHtml, event);
		
	}
	
	UI_tabSearchFlights.showBaggageTooltip = function(fareClassId, baggageHtml,clickEvent) {
	    $("#" + fareClassId).find("#" + fareClassId + "_div").remove();
	    var div = $('<div id="' + fareClassId + '_div"></div>').css({
	        "display": "none",
	        "position": "absolute",
	        "width": "200px",
	        "z-index": 20000,
	        "background-color": "#fffed4",
	        "border-radius": "10px",
	        "border": "1px solid #ECEC00",
	        "padding": "10px",
	        "opacity": .9,
	        "font-family": "Lucida Grande,Lucida Sans,Arial,sans-serif"
	    });
	    $("#divResultsPane").append(div);
	    $("#" + fareClassId).parent().removeAttr("title");
	    $("#" + fareClassId + "_div").html(baggageHtml);
	    $("#" + fareClassId + "_div").css({
	        "top": clickEvent.clientY+ 8,
	        "left": clickEvent.clientX-140
	    })
		$("#" + fareClassId + "_div").show();
		UI_tabSearchFlights.availableBaggageTooltip[fareClassId] = baggageHtml;

		if (setTimeoutForBaggageToolTip) {
			setTimeoutForBaggageToolTip = false;
			setTimeout(function () {
				$("#" + fareClassId + "_div").hide();
			}, 2000);
		}
	}
	
	UI_tabSearchFlights.baggageRatesComparator = function (first, second){
		var fAmount = first.baggageCharge;
		var sAmount = second.baggageCharge;
		//TODO : if charge is same ==>  improve to have baggage name as well 
		return fAmount == sAmount ? 0 : (fAmount < sAmount ? -1 : 1);
	}
	
	UI_tabSearchFlights.getSelectedAvailableLogicalCCList = function(ondCode, ondSequence){
		var selList = [];		
		$.each(UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence], function(i, obj){
			if(obj.ondCode == ondCode &&
					obj.sequence == ondSequence){
				selList = obj.availableLogicalCCList;
			}
		});
			
		return selList;
	}
	
	// This method sets the selected attribute of available logical CCs of
	// particular flight segment
	UI_tabSearchFlights.setSelectedLogicalCCOption = function(logicalCCList, selectedLogicalCCObj){
		if(logicalCCList != null) {
			$.each(logicalCCList, function(i, obj){
				if(obj.logicalCCCode == selectedLogicalCCObj.logicalCCCode 
						&& obj.withFlexi == selectedLogicalCCObj.withFlexi
						&& obj.bundledFarePeriodId == selectedLogicalCCObj.bundledFarePeriodId){
					obj.selected = true;
				} else {
					obj.selected = false;
				}
			});
		}
	}
		
	UI_tabSearchFlights.setDefaultLogicalCCSelection = function(logicalCCList, ondSequence, reqOndSequence,ondCode){
		if(logicalCCList != null){	
			var selectedLogicalCCCode = "";
			$.each(logicalCCList, function(i, elem){				
				if (elem.selected) {
					selectedLogicalCCCode = elem.logicalCCCode;
				}
			});
			
			UI_tabSearchFlights.setSelectedLogicalCabinClassCode(selectedLogicalCCCode, ondSequence, reqOndSequence, ondCode);
		}
	}
	
	UI_tabSearchFlights.getSelectedLogicalCC = function(ondSequence, ondCode){
		var selLogicalCC = '';		
		if(ondSequence!=null && ondCode!=null){			
			var selectedOndLogicalList = UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence];			
			if(selectedOndLogicalList != null ){
				$.each(selectedOndLogicalList, function(i, elem){
					if(elem.sequence==ondSequence && elem.ondCode==ondCode){
						$.each(elem.availableLogicalCCList, function(key, obj){
							if(obj.logicalCCCode!=undefined && obj.logicalCCCode!=null){
								selLogicalCC = obj.logicalCCCode;
								return;	
							}							
						});
						
					}					
				});
			}			
		}
		return selLogicalCC;		
	}	
	
	UI_tabSearchFlights.setSelectedLogicalCabinClassCode = function(selLogicalCC, ondSequence, reqOndSequence, ondCode){	
		var selectedOndLogicalList = UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence];
		if(selectedOndLogicalList != null){
			
			if(selLogicalCC==null || $.trim(selLogicalCC)==""){
				selLogicalCC = UI_tabSearchFlights.getSelectedLogicalCC(ondSequence, ondCode);
			}			
			
			if(UI_tabSearchFlights.logicalCabinClassSelection == null){
				UI_tabSearchFlights.logicalCabinClassSelection = {};
			}
			/*
			 * AMSWAT-110 - Fix
			 * Correct ond sequence is retrieved by ondSequence
			 */
			UI_tabSearchFlights.logicalCabinClassSelection[ondSequence] = selLogicalCC;
						
			if(UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection == null){
				UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection = {};
			}
			
			if((typeof UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[reqOndSequence] =="undefined") || UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[reqOndSequence] == null){
				UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[reqOndSequence] = {};
				UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[reqOndSequence][ondCode] = selLogicalCC;
			}else{
				UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[reqOndSequence][ondCode]= selLogicalCC;
			}
			
			if(UI_tabSearchFlights.isOpenReturn){
				var inboundOnd = ondCode.split('/')[1] + "/" + ondCode.split('/')[0];
				//Set outbound logical cabin class to inbound ond if open return search 
				UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = {};
				UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND][inboundOnd] = selLogicalCC;
			}
			
			$("#btnCAllFare").hide();
		}
		
		if(UI_tabSearchFlights.isAllowChangeFare){			
			if(DATA_ResPro.initialParams.allCoSChangeFareEnabled){
				$("#btnCFare").hide();
				$("#btnCAllFare").show();
			} else {
				$("#btnCFare").show();
				$("#btnCAllFare").hide();
			}
		}
	}
	
	UI_tabSearchFlights.overrideBundledFareLogicalCabinClasses = function(ondLogicalCCObjList, ondSequence){
		if(UI_tabSearchFlights.logicalCabinClassSelection != null){
			var currentOndLogicalCCObjList = UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence];
			var selectedLogicalCabinClass = UI_tabSearchFlights.logicalCabinClassSelection[ondSequence];
					
			for ( var i = 0; i < currentOndLogicalCCObjList.length; i++) {
				var logicalCCWiseArray = {};
				
				var currenntLogicalCCList = currentOndLogicalCCObjList[i].availableLogicalCCList;
				
				for ( var j = 0; j < currenntLogicalCCList.length; j++) {
					var currentLogicalCCObj = currenntLogicalCCList[j];
					
					if(logicalCCWiseArray[currentLogicalCCObj.logicalCCCode] == undefined ||
							logicalCCWiseArray[currentLogicalCCObj.logicalCCCode] == null){
						logicalCCWiseArray[currentLogicalCCObj.logicalCCCode] = new Array();
					}
					
					var logicalCCArray = logicalCCWiseArray[currentLogicalCCObj.logicalCCCode];
					
					if((currentLogicalCCObj.bundledFarePeriodId == null || currentLogicalCCObj.bundledFarePeriodId == '')
							|| (currentLogicalCCObj.logicalCCCode != selectedLogicalCabinClass 
									&& currentLogicalCCObj.bundledFarePeriodId != null && currentLogicalCCObj.bundledFarePeriodId != '')){

                        var newLogicalCCList = ondLogicalCCObjList[i].availableLogicalCCList;
                        var existingNonBundleFare = null;

                        if(newLogicalCCList != undefined && newLogicalCCList != null){
                            for ( var k = 0; k < newLogicalCCList.length; k++) {
                                var newOndLogicalCCObj = newLogicalCCList[j];

                                if(newOndLogicalCCObj.bundledFarePeriodId === null || newOndLogicalCCObj.bundledFarePeriodId === ''){
                                    existingNonBundleFare = newOndLogicalCCObj;
                                }
                            }
                        }

                        if(existingNonBundleFare != null) {
                            currentLogicalCCObj.bundledFareDelta = existingNonBundleFare.bundledFareDelta;
                        }

						logicalCCArray.push(currentLogicalCCObj);
					}
				}
				
				var newLogicalCCList = ondLogicalCCObjList[i].availableLogicalCCList;
				var newSelectedLogicalCCCode = '';
				var newSelectedBundledFarePeriodId = null;
				var withFlexi = false;
				
				
				if(newLogicalCCList != undefined && newLogicalCCList != null){
					
					for ( var j = 0; j < newLogicalCCList.length; j++) {
						
						var newOndLogicalCCObj = newLogicalCCList[j];
						
						if(logicalCCWiseArray[newOndLogicalCCObj.logicalCCCode] == undefined ||
								logicalCCWiseArray[newOndLogicalCCObj.logicalCCCode] == null){
							logicalCCWiseArray[newOndLogicalCCObj.logicalCCCode] = new Array();
						}
						
						var logicalCCArray = logicalCCWiseArray[newOndLogicalCCObj.logicalCCCode];
						
						if(newOndLogicalCCObj.logicalCCCode == selectedLogicalCabinClass 
								&& newOndLogicalCCObj.bundledFarePeriodId != null && newOndLogicalCCObj.bundledFarePeriodId != ''){
							logicalCCArray.push(newOndLogicalCCObj);
						} else if(UI_tabSearchFlights.isRequote()) {
							//Check if new ond logical CC not exists in current logical CCs if it is not bundled service
							var alreadyExists = false;
							for ( var k = 0; k < currenntLogicalCCList.length; k++) {
								var currentLogicalCCObj = currenntLogicalCCList[k];
								if((currentLogicalCCObj.logicalCCCode == newOndLogicalCCObj.logicalCCCode &&
										currentLogicalCCObj.withFlexi == newOndLogicalCCObj.withFlexi)
										|| (newOndLogicalCCObj.bundledFarePeriodId != null && newOndLogicalCCObj.bundledFarePeriodId != '')){
									alreadyExists = true;
									break;
								}
							}
							
							if(!alreadyExists){
								logicalCCArray.push(newOndLogicalCCObj);
							}
						}
						
						
						if(newOndLogicalCCObj.selected){
							newSelectedLogicalCCCode = newOndLogicalCCObj.logicalCCCode;
							newSelectedBundledFarePeriodId = newOndLogicalCCObj.bundledFarePeriodId;
							withFlexi = newOndLogicalCCObj.withFlexi;
						}
					}
					
				}
				
				if(logicalCCWiseArray != undefined && logicalCCWiseArray != null){
					UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence][i].availableLogicalCCList = new Array();
					$.each(logicalCCWiseArray, function(logicalCCCode, logicalCCList){
						
						for ( var k = 0; k < logicalCCList.length; k++) {
							var logicalCCObj = logicalCCList[k];
							if(logicalCCObj.logicalCCCode == newSelectedLogicalCCCode 
									&& newSelectedBundledFarePeriodId == logicalCCObj.bundledFarePeriodId 
									&& withFlexi == logicalCCObj.withFlexi){
								logicalCCObj.selected = true;
							}
						}
						
						UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence][i].availableLogicalCCList = $.merge(UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence][i].availableLogicalCCList, logicalCCList);
					});
				}
			}			
		}
	}
	
	UI_tabSearchFlights.recreateAvailableLogicalCCInfoList = function(response){	
		// Set OND wise logical cc info
		if(response.ondLogicalCCList != null && response.ondLogicalCCList.length > 0){
			for(var ondSequence=0;ondSequence<response.ondLogicalCCList.length;ondSequence++){
				var ondLogicalCCObj = response.ondLogicalCCList[ondSequence];
				if(typeof(UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence]) == 'undefined' ||
						UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence] == null){
					UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence] = ondLogicalCCObj;
				} else if(UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSequence]){
					UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence] = ondLogicalCCObj;
				} else if(UI_tabSearchFlights.availableOndLogicalCCInfo[ondSequence] != null && 
						UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability[ondSequence]){
					UI_tabSearchFlights.overrideBundledFareLogicalCabinClasses(ondLogicalCCObj, ondSequence);
				}
				
				//purpose of following operation is to make sure all OND has a param, if not we will fill it with true/false   
				if(typeof(UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSequence]) == 'undefined' ||
						UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSequence] == null){
					UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSequence] = true;
				}
				
			}
		} else {
			if(UI_tabSearchFlights.ondRebuildLogicalCCAvailability != null
					&& UI_tabSearchFlights.availableOndLogicalCCInfo != null){
				$.each(UI_tabSearchFlights.ondRebuildLogicalCCAvailability, function(ondSeq, isRebuild){
					if(isRebuild){						
						UI_tabSearchFlights.availableOndLogicalCCInfo[ondSeq] = [];
					}
				});
			} else {
				UI_tabSearchFlights.availableOndLogicalCCInfo = [];
			}
		}		
	}
	
	UI_tabSearchFlights.recreateAvailableBookingClassInfoList = function(response){	
		// Set OND wise logical cc info
		if(response.availableFare != null && response.availableFare.fareRulesInfo != null
				&& response.availableFare.fareRulesInfo.fareRules != null && response.availableFare.fareRulesInfo.fareRules.length > 0){

			for(var ondSequence=0;ondSequence<response.availableFare.fareRulesInfo.fareRules.length;ondSequence++){
				
				var fareRule = response.availableFare.fareRulesInfo.fareRules[ondSequence];
				var ondCode = fareRule.orignNDest;
				
				if(UI_tabSearchFlights.ondSegWiseBookingClassSelection == null){
					UI_tabSearchFlights.ondSegWiseBookingClassSelection = {};
				}
				
				if((typeof UI_tabSearchFlights.ondSegWiseBookingClassSelection[ondSequence] =="undefined") || UI_tabSearchFlights.ondSegWiseBookingClassSelection[ondSequence] == null){
					UI_tabSearchFlights.ondSegWiseBookingClassSelection[ondSequence] = {};
					UI_tabSearchFlights.ondSegWiseBookingClassSelection[ondSequence][ondCode] = fareRule.bookingClassCode;
				}else{
					UI_tabSearchFlights.ondSegWiseBookingClassSelection[ondSequence][ondCode]= fareRule.bookingClassCode;
				}
				
			}
			
		} 
		
	}
	
	UI_tabSearchFlights.setSelectedLogicalCabinGridRow = function(dataArr, gridId){
		for(var i=1;i<=dataArr.length;i++){
			if(dataArr[i-1].selected){
				$(gridId).setSelection(i);
			}
		}

		if (UI_tabSearchFlights.displayBasedOnTemplates) {
			$(gridId + " td").removeAttr('title');
		}
	}
	
	UI_tabSearchFlights.clearLogicalCCData = function(){
		UI_tabSearchFlights.availableOndLogicalCCInfo = [];
		UI_tabSearchFlights.logicalCabinClassSelection = null;
		UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection = null;
		UI_tabSearchFlights.segWiseLogicalCabinClassSelection = null;
		$("#tdOutBoundLogicalCabins").empty();
		$("#tdInBoundLogicalCabins").empty();
		$("#tdOutBoundLogicalCabins").hide();
		$("#tdInBoundLogicalCabins").hide();
	}
	
	UI_tabSearchFlights.resetLogicalCabinClassSelection = function(rowId, gridId){				
		$("#tdOutBoundLogicalCabins").empty();
		$("#tdInBoundLogicalCabins").empty();
		
		if(UI_tabSearchFlights.availableOndLogicalCCInfo != null){
			
			$.each(UI_tabSearchFlights.availableOndLogicalCCInfo, function(ondSequence, lccObj){
				
				UI_tabSearchFlights.ondRebuildLogicalCCAvailability[ondSequence] = true;
				
				if (UI_commonSystem.strPGMode != "transferSegment"){			
					UI_tabSearchFlights.ondWiseBundleFareSelected[ondSequence] = null;
					UI_tabSearchFlights.ondWiseBookingClassSelected[ondSequence] = null;
					
					if(jsonOutFlts != null && jsonOutFlts.length > 0 &&
							jsonRetFlts != null && jsonRetFlts.length > 0){
						if(UI_tabSearchFlights.logicalCabinClassSelection != null){				
							if(typeof(UI_tabSearchFlights.logicalCabinClassSelection[ondSequence]) != 'undefined'){
								UI_tabSearchFlights.logicalCabinClassSelection[ondSequence] = null;
								UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection[ondSequence] = null;
							}
						}
						if (UI_tabSearchFlights.segWiseLogicalCabinClassSelection != null) {
							var fltData = null;
							
							if(ondSequence == UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND){
								fltData = jsonOutFlts[rowId - 1];
							} else {
								fltData = jsonRetFlts[rowId - 1];
							}
							
							if(fltData != null && fltData != undefined){
								$.each(fltData.segmentCodeList, function(i, segCode){
									UI_tabSearchFlights.segWiseLogicalCabinClassSelection[segCode] = null;
								});
							}
						}
					} else {
						UI_tabSearchFlights.clearLogicalCCData();
					}					
				}					
			});		
		}		
		for(var ondSeq = 0; ondSeq < UI_tabSearchFlights.availableOndLogicalCCInfo.length; ondSeq++){
			var logicalCcInfoObj = UI_tabSearchFlights.availableOndLogicalCCInfo[ondSeq];
			if(logicalCcInfoObj!=undefined && logicalCcInfoObj!=null){
				$.each(logicalCcInfoObj, function(i, obj){
					var ondCode = obj.ondCode;			
					var availLogicalCabins = obj.availableLogicalCCList;
					UI_tabSearchFlights.setDefaultLogicalCCSelection(availLogicalCabins, 
							ondSeq, logicalCcInfoObj.reqOndSequence,  ondCode);
				});
			}
		}
	};
	
	UI_tabSearchFlights.getJsonLogicalCabinClassSelection = function(){
		return $.toJSON(UI_tabSearchFlights.logicalCabinClassSelection);
	};
	
	UI_tabSearchFlights.getondSegWiselogicalCabinClassSelection = function(){
		return $.toJSON(UI_tabSearchFlights.ondSegWiselogicalCabinClassSelection);
	};
	
	UI_tabSearchFlights.initBaggageTooltipShowHide = function() {
	    $(".baggageToolTip").unbind('click').bind('click', function(event) {
	        var fareClassId = $(this).attr('id');
	        
			if(UI_tabSearchFlights.availableBaggageTooltip[fareClassId] != null){
				$("#" + fareClassId + "_div").show();
			}else{
				UI_tabSearchFlights.baggageInfoRetrieve(event, fareClassId);
			}
	    });

	    $(".baggageToolTip").unbind("mouseout").bind("mouseout", function() {
	        var fareClassId = $(this).attr('id');
			if (!UI_tabSearchFlights.availableBaggageTooltip[fareClassId]) {
				setTimeoutForBaggageToolTip = true;
			}
			$("#" + fareClassId + "_div").hide();
	    });
	}
	
	UI_tabSearchFlights.IntCommentShowHide = function(){
		$(".commentToolTip").unbind("mouseover").bind("mouseover",function(event){
			UI_tabSearchFlights.displayCommentTooltTip(event, this);
		});
		$(".commentToolTip").unbind("mouseout").bind("mouseout",function(){
			UI_tabSearchFlights.hideCommentTooltTip();
		});
	};
	UI_tabSearchFlights.hideCommentTooltTip = function(){
		$("#commentToolTip").hide();
		$("#divResultsPane").find("#commentToolTip").remove();
	};
	
	UI_tabSearchFlights.displayCommentTooltTip = function(e, role){
		$("#divResultsPane").find("#commentToolTip").remove();
		var div = $( '<div id="commentToolTip"></div>' ).css({
			"display":"none",
			"position": "absolute",
			"width":"220px",
			"z-index":20000,
			"background-color":"#fffed4",
			"border-radius": "10px",
			"border": "1px solid #ECEC00",
			"padding": "10px",
			"opacity":.9,
			"font-family":"Lucida Grande,Lucida Sans,Arial,sans-serif"
		});
		$("#divResultsPane").append(div);
		$(role).parent().removeAttr("title");
		$("#commentToolTip").html($(role).attr("role"));
		$("#commentToolTip").css({
			"top":e.pageY+8,
			"left":e.pageX - 120
		})
		$("#commentToolTip").show();
	};
	
	UI_tabSearchFlights.updateTravelAgentCode = function(){		
		if($("#tAgent").val()==undefined || $("#tAgent").val()==null || $.trim($("#tAgent").val())==""){
			$("#travelAgentCode").val("");
		}
	};
	
	UI_tabSearchFlights.populateExcludeChargeList = function(populateList){
		if(populateList && excludableCharges != null && excludableCharges != {}){
			$.each(excludableCharges, function(key, value){
				var tmpId = 'trExclChgTempd_' + key;
				var chId = 'chkExclChg_' + key;
				
				var clone = $('#trExcludeChargeTemp').clone().show();
				clone.attr('id',tmpId);
				clone.appendTo($('#trExcludeChargeTemp').parent());  
				
				var chBox = $('#'+tmpId+' input');
				chBox.attr('id',chId);		
				chBox.attr('tabIndex',9000);
				chBox.val(key);
				chBox.addClass('clsExclChg');
				chBox.click(function(){UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll();});
									
				var label = $('#'+tmpId+' label');
				label.attr('id','lblExclChg_'+key);
				label.html(value + ' - ' + key);
			});
		}
	}
	
	UI_tabSearchFlights.getPromoDiscountObject = function(discountType){
		var promoObj = null;
		if(UI_tabSearchFlights.isPromotionApplied()
				&& !UI_tabSearchFlights.isSkipDiscount
				&& UI_tabSearchFlights.appliedFareDiscountInfo.promotionType == discountType){
			promoObj = UI_tabSearchFlights.appliedFareDiscountInfo;
		}
		return promoObj;
	}
	
	UI_tabSearchFlights.removeAppliedPromotion = function(){
		if(UI_tabSearchFlights.isPromotionApplied()){
			UI_tabSearchFlights.isSkipDiscount = true;
		}
	}
	
	UI_tabSearchFlights.restoreAppliedPromotion = function(){
		if(UI_tabSearchFlights.isPromotionApplied()){
			UI_tabSearchFlights.isSkipDiscount = false;
		}
	}
	
	UI_tabSearchFlights.isPromotionApplied = function(){
		if(UI_tabSearchFlights.appliedFareDiscountInfo != null 
				&& UI_tabSearchFlights.appliedFareDiscountInfo.promotionId > 0){
			return true;
		}
		
		return false;
	}
	
	UI_tabSearchFlights.convertArrayToMap = function(arr){
		var map = {};
		if(arr != null && arr.length > 0){
		    $.map( arr, function( val, index ) {
		        map[index] = val;
		    });
		}
		
		return map;
	}
	
	UI_tabSearchFlights.setAvailableBundleFareLCClassStr = function(ondLogicalCCList){
		if(ondLogicalCCList != undefined && ondLogicalCCList != null){
			$.each(ondLogicalCCList, function(index, ondViseLogicalCCList) {
				var outBoundAvailbleBundleFareArray = new Array();
				var ondSequnce = ondViseLogicalCCList[0].sequence ;				
				$.each(ondViseLogicalCCList[0].availableLogicalCCList,function(avilableLogicalCCIndex, logicalCC){
					if(logicalCC.bundledFarePeriodId!== null && logicalCC.bundledFarePeriodId !== undefined){
						var arrIndex = outBoundAvailbleBundleFareArray.length;
						outBoundAvailbleBundleFareArray[arrIndex] = new Array();
						outBoundAvailbleBundleFareArray[arrIndex].push({"bundledFarePeriodId":logicalCC.bundledFarePeriodId ,
							"bundleFareFee":logicalCC.bundledFareFee , "bundleFareFreeServices":logicalCC.bundledFareFreeServiceName, 
							"bundleFareName":logicalCC.logicalCCDesc});		
					}					
				});				
				UI_tabSearchFlights.resAvailableBundleFareLCClassStr[ondSequnce] = outBoundAvailbleBundleFareArray;				
			});
		}		
	}	
	
//	UI_tabSearchFlights.openReuestSubmitWindow = function(){
//		
//		var pnr = '';
//		
//		if(!(DATA_ResPro.reservationInfo === undefined)){
//			
//			pnr = DATA_ResPro.reservationInfo.PNR;
//			
//		}
//		
//		var strUrl = "showRequestSubmitPopup.action?pageId=ASP&PNR=" + pnr;
//		
//		top.objCW= window.open(strUrl,"",$("#popWindow").getPopWindowProp(280, 650, true));
//		if (top.objCW) {top.objCW.focus()}
//		
//	}
	// ################ End of Logical Cabin Class Related Methods
	// #####################
	
	UI_tabSearchFlights.getFromAirportCode = function(){		 
		var code = UI_tabSearchFlights.fromCombo.getValue();
		UI_tabSearchFlights.fromCitySearch = false;
		
		if(code==undefined || code==null || code == '' ) return '';
		
		var value = code;		
		var arr = code.split('_');		
		
		if(arr!=null && arr.length > 1 && arr[1]==='Y'){
			UI_tabSearchFlights.fromCitySearch = true;
		}
		
		value = arr[0];
		
		UI_tabSearchFlights.setCitySearchFlagByOriginDestination();
		
		return value;
	}
	
	UI_tabSearchFlights.getToAirportCode = function(){		 
		var code = UI_tabSearchFlights.toCombo.getValue();
		UI_tabSearchFlights.toCitySearch = false;
		
		if(code==undefined || code==null || code == '' ) return '';
		
		var value = code;
		var arr = code.split('_');		
		
		if(arr!=null && arr.length > 1 && arr[1]==='Y'){
			UI_tabSearchFlights.toCitySearch = true;
		}
		
		value = arr[0];

		UI_tabSearchFlights.setCitySearchFlagByOriginDestination();
		
		return value;
	}
	
	UI_tabSearchFlights.setCitySearchFlagByOriginDestination = function(){		
		
		UI_tabSearchFlights.ondWiseCitySearch[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND]={'DEP':UI_tabSearchFlights.fromCitySearch,'ARR':UI_tabSearchFlights.toCitySearch};
		UI_tabSearchFlights.ondWiseCitySearch[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND]={'DEP':UI_tabSearchFlights.toCitySearch,'ARR':UI_tabSearchFlights.fromCitySearch};
		
	}
	/*
	 * ------------------------------------------------ end of File
	 * ----------------------------------------------
	 */
	
	