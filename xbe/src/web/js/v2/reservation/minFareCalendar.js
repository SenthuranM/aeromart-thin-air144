/**
 * Created by baladewa on 6/9/15.
 */
var sampleDate = [
    {
        origin: 'SHJ',
        destination: 'CMB',
        cabin: 'Economy',
        currencyCode: 'AED',
        reCallTimeOut:5,
        calendarData: [{
            date: '2015-06-13',
            dateOfWeek: 'SUN',
            status: 'AVL',
            fareDetails: {
                fareAmount: 500,
                fareIndicator: null,
                selectedSegments: [{
                    segmentCode: 'SHJ/CMB',
                    departureDateTime: '2015-04-01T04:35:00',
                    bookingCode: 'N1'
                }]
            }
        },{
            date: '2015-06-14',
            dateOfWeek: 'MON',
            status: 'AVL',
            fareDetails: {
                fareAmount: 450,
                fareIndicator: 'LOWEST',
                selectedSegments: [{
                    segmentCode: 'SHJ/CMB',
                    departureDateTime: '2015-04-01T04:35:00',
                    bookingCode: 'N1'
                }]
            }
        },{
            date: '2015-06-15',
            dateOfWeek: 'TUE',
            status: 'AVL',
            fareDetails: {
            fareAmount: 500,
                fareIndicator: null,
                selectedSegments: [{
                    segmentCode: 'SHJ/CMB',
                    departureDateTime: '2015-04-01T04:35:00',
                    bookingCode: 'N1'
                }]
            }
        },{
        date: '2015-06-16',
        dateOfWeek: 'WED',
        status: 'AVL',
        fareDetails: {
            fareAmount: 550,
            fareIndicator: "HIGHEST",
            selectedSegments: [{
                segmentCode: 'SHJ/CMB',
                departureDateTime: '2015-04-01T04:35:00',
                bookingCode: 'N1'
                }]
            }
        },{
            date: '2015-06-17',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-18',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-19',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-17',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-18',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-19',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-17',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-18',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-19',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-17',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-18',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-19',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        }]
    },
    {
        origin: 'SHJ',
        destination: 'CMB',
        cabin: 'Economy',
        currencyCode: 'AED',
        reCallTimeOut:5,
        calendarData: [{
            date: '2015-06-20',
            dateOfWeek: 'SUN',
            status: 'AVL',
            fareDetails: {
                fareAmount: 500,
                fareIndicator: null,
                selectedSegments: [{
                    segmentCode: 'SHJ/CMB',
                    departureDateTime: '2015-04-01T04:35:00',
                    bookingCode: 'N1'
                }]
            }
        },{
            date: '2015-06-21',
            dateOfWeek: 'MON',
            status: 'AVL',
            fareDetails: {
                fareAmount: 450,
                fareIndicator: 'LOWEST',
                selectedSegments: [{
                    segmentCode: 'SHJ/CMB',
                    departureDateTime: '2015-04-01T04:35:00',
                    bookingCode: 'N1'
                }]
            }
        },{
            date: '2015-06-22',
            dateOfWeek: 'TUE',
            status: 'AVL',
            fareDetails: {
                fareAmount: 500,
                fareIndicator: null,
                selectedSegments: [{
                    segmentCode: 'SHJ/CMB',
                    departureDateTime: '2015-04-01T04:35:00',
                    bookingCode: 'N1'
                }]
            }
        },{
            date: '2015-06-23',
            dateOfWeek: 'WED',
            status: 'AVL',
            fareDetails: {
                fareAmount: 550,
                fareIndicator: "HIGHEST",
                selectedSegments: [{
                    segmentCode: 'SHJ/CMB',
                    departureDateTime: '2015-04-01T04:35:00',
                    bookingCode: 'N1'
                }]
            }
        },{
            date: '2015-06-24',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-25',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        },{
            date: '2015-06-26',
            dateOfWeek: 'THUR',
            status: 'NOTAVL',
            fareDetails:null
        }]
    }
];

(function($) {
    $.fn.minFareCalendar = function(settings) {
        var jsDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat'];
        var jsMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var current_Block = 0;
        var config = {
            displayFormat:"DDD DD MMM",
            dataClearTimeOut:2000,
            displayItems : {flightDate:"",fare:"",minFareFlag:true},
            calData:[], // json Array to send
            onSelectDate : function(event, selectedDate){},
            onNextPrev: function(event, npObj){}
        }

        return this.each(function() {
            //override configs
            if (settings){
                settings = $.extend(config, settings);
            }

            var dayCount = 7;
            var nodPerWeek =dayCount;
            var	el = $(this);
            var displayCurrency = null, cabinClass=null;

            el.on("update", {
                calendar_Event: "update"
            }, function( event, arg1 ) {
                settings.calData = arg1.calJson;
                buildCalBlock();
            });

            el.on("updateBlock", {
                calendar_Event: "update-block"
            }, function( event, arg1 ) {
                settings.calData = arg1;
                var blockId = current_Block[1]+"_"+ arg1.origin + "_" + arg1.destination;
                buildDaysBody(blockId, arg1);
            });

            function buildCalBlock(){
                el.empty();
                var calData = $.extend(true, [], settings.calData);
                for (var i=0;i<calData.length;i++){
                    var calText = "", title=""
                    var noOfDays=calData[i].calendarData.length;
                    displayCurrency = calData[i].currencyCode;
                    cabinClass = calData[i].cabin;
                    var nor = Math.ceil(noOfDays/nodPerWeek), calBlockID = i+"_"+ calData[i].origin + "_" + calData[i].destination;
                    calText = '<div class="calBlock" id="calBlock_'+calBlockID+'" >';
                    title = "Sector " + calData[i].origin + "/" + calData[i].destination + " for " + cabinClass + " class";
                    calText +='<div class="titleRow"><h2>'+title+'</h2></div>';
                    calText +='<a href="javascript:;" class="navigte nextNav" title="Next"> > </a> <a href="javascript:;" class="navigte prevNav" title="Prev"> < </a>';
                    calText +='<div class="calTable">';
                    /*calText += '<div class="calTableRow headerRow">';
                    for (var j=0;j<dayCount;j++){
                        calText +='<div class="calTableCell"><label>'+jsDays[j]+'</label></div>';
                    }
                    calText +='</div>';*/
                    calText +='</div></div>';
                    el.append(calText);
                    buildDaysBody(calBlockID, calData[i]);
                }
            }

            function buildDaysBody(blockId, blkData){
                var stDate = blkData.calendarData[0].date, enDate=blkData.calendarData[blkData.calendarData.length-1].date;
                blkData.startEndDateFlag = stDate + "_" + enDate;
                $.data(el.find('#calBlock_' + blockId)[0],"blockData", blkData);
                blkData = $.extend(true, {}, blkData);
                el.find('#calBlock_' + blockId + ' .calTable').find(".dayItem").fadeOut();
                el.find('#calBlock_' + blockId + ' .calTable').find(".bodyRow").remove();
                var bodyData = blkData.calendarData;
                var nor = Math.ceil(bodyData.length/dayCount);
                var htmlH = '';
                for (var i=0;i<nor;i++){
                    var stIndex = 0;
                    var len=(bodyData.length>=dayCount)?dayCount:bodyData.length;
                    var rowData = bodyData.splice(stIndex,len);
                    htmlH+='<div class="calTableRow bodyRow">';
                    htmlH+=buildARow(i, rowData);
                    htmlH+='</div>';
                }
                el.find('#calBlock_' + blockId + ' .calTable').append(htmlH);
                el.find('#calBlock_' + blockId + ' .calTable').find(".dayItem").fadeIn();
                initMethods(blockId);
            }

            function buildARow(rowIndex, rowData){
                var htmlH='';
                for (var i=0;i<dayCount;i++){
                    var tempDay=null;
                    if(rowData[i]!=undefined){
                        tempDay=rowData[i];
                    }
                    htmlH+='<div class="calTableCell">'+buildADay(tempDay)+'</div>';
                }
                return htmlH;
            }

            function setDateFormat(date){
                var temdate = date.split("-"),send=date;
                var newDate = new Date(temdate[0]+"/"+temdate[1]+"/"+temdate[2]);
                if (settings.displayFormat = "DDD DD MMM"){//can apply other date formats later
                     send=jsDays[newDate.getDay()] + " " + newDate.getDate() + " " + jsMonths[newDate.getMonth()];
                }
                return send
            };

            function buildADay(dayData){
                var htmlH = "", clickCLass="";
                if (dayData!=null){
                    if(dayData.fareDetails!=null){
                        clickCLass="fareClick"
                    }
                    htmlH='<div class="dayItem '+clickCLass+'">';
                    htmlH+='<div class="day-header"><span class="date-hidden">'+dayData.date+'</span>';
                    htmlH+='<span class="date-shown">'+setDateFormat(dayData.date)+'</span></div>';
                    if (dayData.fareDetails!=null){
                        htmlH+='<div class="day-fare"><div class="showFare">';
                        htmlH+=dayData.fareDetails.fareAmount+' <span>'+displayCurrency+'</span>';
                        htmlH+='</div></div>'
                        htmlH+='<div class="day-minFare">';
                        if(dayData.fareDetails.fareIndicator!=null){
                            htmlH+='<span>'+dayData.fareDetails.fareIndicator+'</span>';
                        }
                        htmlH+='</div>'
                    }else{
                        htmlH+='<div class="day-fare">Not Available</div>';
                    }
                    htmlH+='</div>';
                }
                return htmlH;
            }

            function getObjectByData(tdate, array){
                var send=null;
                $.each(array.calendarData, function(i, obj){
                    if (obj.date===tdate){
                        send = obj;
                    }
                })
                return send;
            }

            function zeroBased(dig){
                if (String(dig).length===1){
                    dig = "0"+dig;
                }
                return dig
            }

            function addDays(dtDate,intDays) {
                var dtArray = dtDate.split("-");
                var newDate = new Date(dtArray[0]+"/"+dtArray[1]+"/"+dtArray[2]);
                var sendDate = new Date(newDate.getTime() + Number(intDays) *24*60*60*1000);
                return sendDate.getFullYear() +"-"+ zeroBased((sendDate.getMonth() + 1)) +"-"+ zeroBased(sendDate.getDate());
            }

            function initMethods(blockId){
                el.find('#calBlock_' + blockId).find(".fareClick").off("click").on('click', function(event){
                    var selectedDate = $.trim($(this).find('.date-hidden').text()),
                        selectedBlock = $(this).parent().parent().parent().parent(),
                        selectedBlockData = $.data(selectedBlock[0],"blockData");
                    selectedBlock.find(".fareClick").removeClass("selected-day");
                    $(this).addClass("selected-day");
                    settings.onSelectDate(event, getObjectByData(selectedDate, selectedBlockData));
                });

                el.find('#calBlock_' + blockId).find(".navigte").off("click").on("click", function(event){
                    nextPreviousAction(event, this);
                });
            }

            function nextPreviousAction(event, obj){
                var blockID = $(obj).parent().attr("id");
                var direction = $(obj).attr("title");
                var tem = {};
                tem["way"]=$(obj).attr("title");
                var bIndex= blockID.split("_");
                current_Block = bIndex;
                var currentDB = $.data(el.find('#' + blockID)[0],"blockData");
                var oldDatArray=$.data(el.find('#' + blockID)[0],"blockOldDataArray")||[];
                var startDate ="", length = currentDB.calendarData.length, endDate="";
                var reCallTimeoutForBlock = currentDB.reCallTimeOut * 1000;
                if (direction.toLowerCase()==='prev'){
                    var tStDt = currentDB.startEndDateFlag.split("_")[0];
                    startDate = addDays(tStDt, -length);
                    endDate = addDays(startDate, (length-1));
                }else{
                    var tStDt = currentDB.startEndDateFlag.split("_")[1];
                    startDate = addDays(tStDt, 1);
                    endDate = addDays(tStDt, length);
                }
                tem["startDate"]=startDate;
                tem["endDate"]=endDate;
                tem["origin"]=bIndex[2];
                tem["destination"]=bIndex[3];
                dateFlg = startDate + "_" + endDate;
                var dt = new Date();
                var currentDate = dt.getFullYear() + "-" + zeroBased((dt.getMonth() + 1)) + "-" + zeroBased(dt.getDate());
                if (compareDate(currentDate, startDate)>=0){

                    //check the availability of oldData
                    var dataBlock = getNextPrevDataBlock(dateFlg, oldDatArray);
                    //Inner function for delete when the timeout comes
                    var deleteArrayItem = function(ind,arr){
                        arr.splice(ind, 1);
                    }


                    if (oldDatArray.length<5){
                        oldDatArray[oldDatArray.length]=currentDB;
                        setTimeout(function(){
                            oldDatArray.splice(oldDatArray.length-1, 1);
                            $.data(el.find('#' + blockID)[0],"blockOldDataArray", oldDatArray);
                        }, reCallTimeoutForBlock);
                    }
                    $.data(el.find('#' + blockID)[0],"blockOldDataArray", oldDatArray);

                    if (dataBlock==null){
                        settings.onNextPrev(event, tem);
                    }else{
                        var blId = bIndex[1] + "_" + bIndex[2] + "_" +bIndex[3];
                        buildDaysBody(blId, dataBlock);
                    }
                }else{
                    alert ('can not go to previous');
                }
            }

            function getNextPrevDataBlock(blockFlg, array){
                var send = null;
                for(var i=0;i<array.length;i++){
                    if (array[i].startEndDateFlag === blockFlg){
                        send= array[i];
                        break;
                    }
                }
                return send;
            }


            function compareDate(date1,date2){
                var tempDay = date1.split("-");
                var tmpDate1=(tempDay[0]+tempDay[1]+tempDay[2]);
                tempDay = date2.split("-");
                var tmpDate2=(tempDay[0]+tempDay[1]+tempDay[2]);
                if (tmpDate1 < tmpDate2){
                    return 1;
                }else if (tmpDate1 == tmpDate2){
                    return 0;
                } else {
                    return -1;
                }

            }

        })
    };
})(jQuery);