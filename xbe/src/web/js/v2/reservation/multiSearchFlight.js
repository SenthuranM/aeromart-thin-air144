/*
 * Multiple city search related methods
 * Author			: Baladewa Welathanthi
 * Version			: 1.0
 * Created			: 07th August 2012
 */
function UI_Multicity() {
	
	this.ONDHTML = null;
	this.OnDGRIDHTML = null;
	this.fqParams = null;
	this.flightInfo = [];
	
	
	var airportMessage = "";
	actLogCabClassList = [];
	var modifySearch = false
	var ondDataMap = [];
	var ondListStr = [];
	var ondListFQStr = [];
	var busCarrierCodes = [];
	this.ready = function(){
		var frmHTML = $("#frmSrchFlt>table").clone();
		UI_tabSearchFlights.multicity = true;
		UI_tabSearchFlights.ready();
		$("#frmSrchFlt").empty();
		$("#frmSrchFlt").append(frmHTML);
		frmHTML = null;
		if(DATA_ResPro.initialParams.promoCodeEnabled) {
			$("#trPromoCode").show();
		} else {
			$("#trPromoCode").hide();
		}
		fillDropDownData();
		setOnDHTML();
		createOnDHTML(null,0);
		$("#btnSearch").decorateButton();
		$("#btnSearch").click(function() { searchOnClick();});
		$("#btnFQ").unbind("click").click(function() { loadFareQuoteOnClick();});
		clearAll();
		$('#frmSrchFlt').bind("keypress", function(keyEvent){UI_tabSearchFlights.checkEnterAndSubmit(keyEvent);});
		if(DATA_ResPro.systemParams.dynamicOndListPopulationEnabled
				&& ondListLoaded()){
			initDynamicOndList();
		} else if(DATA_ResPro.initialParams.allowRouteSelectionForAgents) {
			UI_tabSearchFlights.initAgentWiseOrigins(0, false);
			UI_tabSearchFlights.initAgentWiseOndList();
		}
		
		if(!DATA_ResPro.initialParams.bCSelectionAtAvailabilitySearchEnabled){
		    $('.spnBookingClass').hide();
		}
		
		if(!DATA_ResPro.initialParams.promoCodeEnabled) {
            $("#trPromoCode").hide();
        } else {
            $("#trPromoCode").show();
        }
		UI_commonSystem.pageOnChange();
		$("#trSearchParams").find("input, select").change(function(){
			clearAll();
		});
		$('#frmSrchFlt').unbind("keypress").bind("keypress", function(keyEvent){ checkEnterAndSubmit(keyEvent);});
		initialization();
		
		//initialize exclude charges UI
		UI_tabSearchFlights.initExcludeChargesView(false);
	};
	
	checkEnterAndSubmit = function(keyEvent){
		 if(keyEvent.which == 13){
             searchOnClick();
         }
	};
	
	fillDropDownData = function(){
		$("#selectedCurrency").fillDropDown( { dataArray:top.arrCurrency , keyIndex:0 , valueIndex:0 });
		$("#classOfService").fillDropDown( { dataArray:top.arrLogicalCC , keyIndex:0 , valueIndex:1 });
		var msBookingTypeArray = new Array();
		for(var ind=0;ind<top.arrBookingTypes.length;ind++){
			if(top.arrBookingTypes[ind][0] != 'WAITLISTING'){
				msBookingTypeArray[ind] = top.arrBookingTypes[ind];
			}
		}
		$("#bookingType").fillDropDown( { dataArray:msBookingTypeArray , keyIndex:0 , valueIndex:1 });
		//$("#bookingType").fillDropDown( { dataArray:top.arrBookingTypes , keyIndex:0 , valueIndex:1 });
		$("#selSearchOptions").fillDropDown( { dataArray:top.arrSearchOptions , keyIndex:0 , valueIndex:1 });
		$("#selFareTypeOptions").fillDropDown( { dataArray:top.arrFareCat , keyIndex:0 , valueIndex:1 });
		$("#selPaxTypeOptions").fillDropDown( { dataArray:top.arrCusType , keyIndex:0 , valueIndex:1 });
		$("#validity").fillDropDown( { dataArray:top.arrRetValidity , keyIndex:0 , valueIndex:1, firstEmpty: true });
		UI_tabSearchFlights.fillBCDropDown();
		UI_tabSearchFlights.fillAgentsDropDown();
	};
		
	setOnDHTML = function(){
		$(".spnClassOfService").hide();
		ONDHTML = $("#multiOndsTemplate");
		OnDGRIDHTML = $("#OnDTeml");
		$("#multiOnds").empty();
		$("#tblONDFlights").empty();
	};
	
	Array.prototype.max = function() {
		var max = this[0];
		var len = this.length;
		for (var i = 1; i < len; i++) if (this[i] > max) max = this[i];
		return max;
	};
	
	createOnDHTML = function(Type, index){
		var isModifyOND = (Type==null)?false:true;
		var Ty = Type;
		if (!isModifyOND){
			var x = 0;
			Ty = "";
			x = parseInt(x,10) + 1;
			if ($("#multiOnds").children().length > 0){
				var rowArray = [];
				$.each($("#multiOnds").find("tr.trMultiOND"), function(){
					rowArray[rowArray.length] = $(this).attr("role");
				});
				x = parseInt(rowArray.max()) + 1;
			}
		}else{
			x = index;
			Ty="_"+Ty
		}

		var tempTR = ONDHTML.clone();
		tempTR.attr("id",tempTR.attr("id")+"_"+x+Ty);
		tempTR.attr("role",x);
		$.each(tempTR.find("input"),function(){
			$(this).attr("id", this.id+"_"+x+Ty);
			$(this).attr("tabindex", x+""+this.tabIndex);
		});
		$.each(tempTR.find("select"),function(){
			$(this).attr("id", this.id+"_"+x+Ty);
			$(this).attr("tabindex", x+""+this.tabIndex);
		});
		$.each(tempTR.find("button"),function(){
			$(this).attr("id", this.id+"_"+x+Ty);
			$(this).attr("tabindex", x+""+this.tabIndex);
		});
		if (!isModifyOND){
			if (index == 0){
				$("#multiOnds").append(tempTR);	
			}else{
				$("#multiOnds").find("#multiOndsTemplate_"+index).after(tempTR);
			}
		}else{
			$("#modTable_"+index).remove();
			var table = $("<table></table>").attr({width:"100%", id:"modTable_"+index});
			var btnSearch = $("<button type='button'>Search OND</button>").attr({
				"id":"SearchOND_"+index+"_Mod",
				"class":"ui-state-default ui-corner-all"
				});
			btnSearch.click(function(e){
				reSearchOND(e,'MOD');
			});
			var btnCell = tempTR.find("#addOND_"+index+"_Mod").parent();
			btnCell.empty();
			btnCell.append(btnSearch);
			
			var ondSearchData = $.data($("#OnDTeml_"+index)[0], "OnDData");

			var fromCitySearchText = "_N";
			var toCitySearchText = "_N";
			
			if(ondSearchData.fromCitySearch){
				fromCitySearchText = "_Y";
			}
			
			if(ondSearchData.toCitySearch){
				toCitySearchText = "_Y";
			}
			
			var fromAirportCode = ondSearchData.fromAirport+""+fromCitySearchText;
			var toAirportCode = ondSearchData.toAirport+""+toCitySearchText;
			
			tempTR.find("#fromAirport_"+index+"_Mod").val(fromAirportCode);
			tempTR.find("#fAirport_"+index+"_Mod").val(fromAirportCode);
			tempTR.find("#toAirport_"+index+"_Mod").val(toAirportCode);
			tempTR.find("#tAirport_"+index+"_Mod").val(toAirportCode);
			tempTR.find("#departureDate_"+index+"_Mod").val(ondSearchData.depDate);
			tempTR.find("#departureVariance_"+index+"_Mod").val(ondSearchData.departureVariance);
			tempTR.find("#bookingType_"+index+"_Mod").val(ondSearchData.bookingType);
			
			table.append(tempTR);
			$("#OnDTeml_"+index).prepend(table)
		}
		$("#departureDate_"+x+Ty).datepicker({ minDate: new Date(), showOn: "button", buttonImage: "../images/Calendar_no_cache.gif", buttonImageOnly: true, maxDate:'+2Y', yearRange:'-1:+2', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
		$("#departureVariance_"+x+Ty).val(top.strDefDV);
		if(!DATA_ResPro.initialParams.bCSelectionAtAvailabilitySearchEnabled){
            $('.spnBookingClass').hide();
        }
		
		var toData = top.arrAirportsV2;
		var fromData = top.arrAirportsV2;
		
		
		if(DATA_ResPro.initialParams.allowRouteSelectionForAgents){
			fromData = UI_tabSearchFlights.agentWiseOrigins;
		}	
		
		var fromStore = new Ext.data.ArrayStore({
		    fields: ['code', 'x', 'name','city','code_city'],
		    data : fromData,
		    sortInfo: { field: 'city', direction: 'DESC' }
		});
	 
		var toStore = new Ext.data.ArrayStore({
		    fields: ['code', 'x', 'name','city','code_city'],
		    data : toData,
		    sortInfo: { field: 'city', direction: 'DESC' }
		});
		
		var fromCombo = new Ext.form.ComboBox({
		    store: fromStore,
		    id: 'extFromAirport_'+x+Ty,
		    displayField:'name',
		    typeAhead: false,
		    mode: 'local',
		    triggerAction: 'all',
		    emptyText:'Please select a value',
		    selectOnFocus:true,
		    applyTo: 'fAirport_'+x+Ty,
	 	    anyMatch: true,
	        caseSensitive: false,
	        forceSelection: true,
	        valueField:'code_city',
	        hiddenName:'searchParams.fromAirport',
	        hiddenId:'fromAirport_'+x+Ty,
	        listeners: {
	            'blur': function(f,new_val,oldVal) {
					UI_tabSearchFlights.onExtComboOnChange(f,new_val,oldVal)
	            }
	        }
		});
		
		var toCombo = new Ext.form.ComboBox({
		    store: toStore,
		    id: 'extToAirport_'+x+Ty,
		    displayField:'name',
		    typeAhead: false,
		    mode: 'local',
		    triggerAction: 'all',
		    emptyText:'Please select a value',
		    selectOnFocus:true,
		    applyTo: 'tAirport_'+x+Ty,
	 	    anyMatch: true,
	        caseSensitive: false,
	        valueField:'code_city',
	        hiddenName:'searchParams.toAirport',
	        hiddenId:'toAirport_'+x+Ty,
	        listeners: {
	            'blur': function(f,new_val,oldVal) {
	            	UI_tabSearchFlights.onExtComboOnChange(f,new_val,oldVal);
					//check and remove
					//originDestinationChanged(this);
	
	            }
	        }
		});
		
		UI_tabSearchFlights.toCombo.on('change', function(){
			UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originDestinationChanged();
		});
		UI_tabSearchFlights.toCombo.on('focus', function(){
			UI_tabSearchFlights.clearAll();
		});
	
		UI_tabSearchFlights.fromCombo.on('change', function(){
			UI_commonSystem.pageOnChange();UI_tabSearchFlights.clearAll(); UI_tabSearchFlights.originChanged();
		});
		
		UI_tabSearchFlights.fromCombo.on('select', function(){
			 UI_tabSearchFlights.originChanged();
		});
		
		
		$("#classOfService_"+x+Ty).change(function(){UI_commonSystem.pageOnChange();clearFareQuote();});
		$(document).on('change',"#fromAirport_"+x+Ty, function(){UI_commonSystem.pageOnChange(); originChanged(this);clearFareQuote();});
		$(document).on('change',"#toAirport_"+x+Ty, function(){UI_commonSystem.pageOnChange(); originDestinationChanged(this);clearFareQuote();});
		$("#fAirport_"+x+Ty).change(function(){UI_commonSystem.pageOnChange(); originChanged(this);clearFareQuote();});
		$("#tAirport_"+x+Ty).change(function(){UI_commonSystem.pageOnChange(); originDestinationChanged(this);clearFareQuote();});
		$("#fAirport_"+x+Ty).select(function(){UI_commonSystem.pageOnChange(); originChanged(this);clearFareQuote();});
		$("#tAirport_"+x+Ty).select(function(){UI_commonSystem.pageOnChange(); originDestinationChanged(this);clearFareQuote();});
		$("#departureDate_"+x+Ty).change(function(){UI_commonSystem.pageOnChange();clearFareQuote();});
		$("#departureVariance_"+x+Ty).change(function(){UI_commonSystem.pageOnChange();clearFareQuote();});
		$("#departureDate_"+x+Ty).blur(function() { dateOnBlur({id:x,flg:Ty});clearFareQuote();});
		$("#bookingType_"+x+Ty).change(function(){UI_commonSystem.pageOnChange();clearFareQuote();});
		$("#addOND_"+x+Ty).click(function(){addNewONDHTML(this.id);clearFareQuote();});
		$("#removeOND_"+x+Ty).click(function(){removeONDHTML(this.id);clearFareQuote();});
	};
	
	setResultPaneHeight = function(flg){
		var curH = parseInt($("#tdResultPane").css("height"),10);
		var newh = 0;
		if (flg=='P'){newh = curH - 33}else{newh = curH + 33}
		$("#tdResultPane").css("height",newh);
	}
	
	addNewONDHTML = function(id){
		//TODO number of ONDs are limited due to UI gets un organized when user adds more.
		//needs to support the UI as when OND size grows/reduces UI also to stretch & shrink		
		var maxOND = DATA_ResPro.systemParams.allowedMaxNoONDMulticitySearch;
		if ($("#multiOnds").children().length < maxOND){
			createOnDHTML(null, id.split("_")[1]);
			setResultPaneHeight('P');
		}else{
			showERRMessage("Reach maximum ONDs");
		}
	};
	
	removeONDHTML = function(id){
		var x = id.split("_")[1];
		if ($("#multiOnds").children().length > 1){
			var x = id.split("_")[1];
			$("#multiOndsTemplate_"+x).remove();
			setResultPaneHeight('M');
			resetSearchSystem();
		}else{
			showERRMessage("Can not remove the last OND");
		}
	};
	
	clearAll = function(){
		clearFareQuote();
		clearAvilability();
		UI_tabSearchFlights.hideAirportMessage();
		//$("#btnBook").hide();
		$("#trFareSection").hide();
	};
	
	clearFareQuote = function(){
		$('#btnCAllFare').attr('disabled',true)
		$("#divFareQuotePane").hide();
		$("#trFareSection").show();
		$("#btnBook").hide();
	};
	
	clearAvilability = function(){
		$("#tblONDFlights").hide();
		clearFareQuote();
	};
	

	originDestinationChanged = function(obj){
		var t = obj.id.split("_")[1];
		var objArr = obj.id.split("_");
		if(objArr.length > 2 && objArr[2]=='Mod'){
			t = t+'_Mod';
		}
		var from = getAirportCode($("#fromAirport_"+t).val());
		var to = getAirportCode($("#toAirport_"+t).val());

		if(from == '' || to == '') return;
		
		if(!$('#selSearchOptions').is(':disabled')){			
			setDefaultSearchOption(from, to);
		}
	};
	
	originChanged = function(obj){
		var t = obj.id.split("_")[1];
		var objArr = obj.id.split("_");
		if(objArr.length > 2 && objArr[2]=='Mod'){
			t = t+'_Mod';
		}
		var fr = $("#fromAirport_"+t).val(); 
		var from = getAirportCode(fr);
		
		var toStore = null;
		if(fr != '' &&
				ondDataMap[fr]){	
			
			var toData = ondDataMap[fr];
			
			if(DATA_ResPro.modifySegments != null) {
				toData = arrGroundSegmentParentTo;			
			}
			
			if(DATA_ResPro.initialParams.allowRouteSelectionForAgents){
				toData = UI_tabSearchFlights.agentWiseOndDataMap[fr];
			} else {
				toData = ondDataMap[fr];		
			}
			
			toStore = new Ext.data.ArrayStore({
				fields: ['code', 'x', 'name','city','code_city'],
				data : toData,
			    sortInfo: { field: 'city', direction: 'DESC' }
			}); 			
		} else {
			
			var toData = top.arrAirportsV2;
			
			if(DATA_ResPro.modifySegments != null) {
				toData = arrGroundSegmentParentTo;			
			}
			
	
			toStore = new Ext.data.ArrayStore({
				fields: ['code', 'x', 'name','city','code_city'],
				data : toData,
			    sortInfo: { field: 'city', direction: 'DESC' }
			});
		}
		
		if(DATA_ResPro.initialParams.allowRouteSelectionForAgents) {
			if(fr != "") {
				var included = false; 
				var destinations = UI_tabSearchFlights.agentWiseOndDataMap[fr];
				if(destinations != undefined) {
					for (var key in destinations) {
						if(destinations.hasOwnProperty(key)) {
							if(destinations[key][0] == Ext.getCmp("extToAirport_"+t).getValue()) {
								included = true;
								break;
							}
						}
					}
				}
				if(!included) {
					Ext.getCmp("extToAirport_"+t).clearValue();
					Ext.getCmp("extToAirport_"+t).applyEmptyText();
				}			
			}			
		}	
		
		Ext.getCmp("extToAirport_"+t).bindStore(toStore);

		var to = $("#toAirport_"+t).val(); 
		if(from == '' || to == '') return;
		
		if(!$('#selSearchOptions').is(':disabled')){			
			setDefaultSearchOption(from, to);
		}
	};

	// Flight Search shortcut by passed via extjs custom
	// keymap[AARESAA-4810 -Issue 1 & 2]
	var keyMap = new Ext.KeyMap(document,{
		key: "abdfgijklmnprstuvwxyz4",
		alt:true,
		fn: function(){ UI_PageShortCuts.onKeyPressCommon(Ext.EventObject); },
		stopEvent:true	
	});

	setDefaultSearchOption = function (from, to){
		if(DATA_ResPro.systemParams.dynamicOndListPopulationEnabled
				&& ondListLoaded()){			
			var fromIndex = -1;
			var toIndex = -1;
			// get from, to indexes
			var airportsLength = airports.length;
			for(var i=0;i<airportsLength;i++){
				if(from == airports[i].code){
					fromIndex = i;
				} else if(to == airports[i].code) {
					toIndex = i;
				}
				
				if(fromIndex != -1 && toIndex != -1){
					break;
				}
			}
			
			var tempSystem = $('#selSearchOptions').val();
			var updateSearchOption = true;
			if(tempSystem!=undefined && tempSystem!=null && tempSystem=='INT'){
				updateSearchOption = false;
			}			
			
			if(fromIndex != -1){			
				var availDestOptLength = origins[fromIndex].length;
				for(var i=0;i<availDestOptLength;i++){
					var destOptArr = origins[fromIndex][i];
					if(destOptArr[0] == toIndex){
						var isInt = false;
						var optCarriersArr = destOptArr[3].split(",");
						for(var j=0;j<optCarriersArr.length;j++){
							if(optCarriersArr[j] != carrierCode){
								isInt = true;
								break;
							}
						}
						
						if(isInt){
							$('#selSearchOptions').val('INT');
						} else {
							$('#selSearchOptions').val('AA');
						}
						
						break;
					}
				}
			} else {			
				$('#selSearchOptions').val('AA');
			}
			
			if(!updateSearchOption){
				$('#selSearchOptions').val('INT');
			}
		} else {
			if (modifySearch) {
				// do nothing as values are defaulted
			} else if ((top.airportOwnerList[from] == 'OWN') && (top.airportOwnerList[to] == 'OWN')) {
				$('#selSearchOptions').val('AA');
			} else if ((top.airportOwnerList[from] == 'OWN') || (top.airportOwnerList[to] == 'OWN')) {
				$('#selSearchOptions').val('INT');
			} else {
				$('#selSearchOptions').val('INT');
			}
		}
		
	};
	
	ondListLoaded = function(){
		if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
			return false;
		} else {
			return true;
		}
	};
	
	validateSearchFlight = function (){
		
		UI_commonSystem.initializeMessage();
		ondListStr = [];
		var allOND = $("#multiOnds").find(".trMultiOND");
		for (var i = 0;i<allOND.length;i++){
			var counter = i + 1;
			var t = allOND[i].id.split("_")[1];

			var from = getAirportCode($('#fromAirport_'+t).val());
			var to = getAirportCode($('#toAirport_'+t).val());
			
			if(from.toUpperCase() == 'PLEASE SELECT A VALUE' ||
					from.toUpperCase() == ''){
				showERRMessage(raiseError("XBE-ERR-01", "Departure location of OND " + counter ));
				$("#fromAirport_"+t).focus();			
				return false;
			}
			
			if(to.toUpperCase() == 'PLEASE SELECT A VALUE' ||
					to.toUpperCase() == ''){
				showERRMessage(raiseError("XBE-ERR-01", "Arrival location of OND " + counter));
				$("#toAirport_"+t).focus();			
				return false;
			}
			
			if (from == to){
				showERRMessage(raiseError("XBE-ERR-02","Departure location","Arrival location of OND " + counter));
				$("#fromAirport_"+t).focus();			
				return false;
			}
			
			if ($("#departureDate_"+t).val() == "" || ($("#departureDate_"+t).val().indexOf("/") == -1)){
				showERRMessage("Wrong date format of OND" + counter);
				$("#departureDate_"+t).focus();		
				return false;
			}
			
			if (!CheckDates(top.strSysDate, $("#departureDate_"+t).val())){
				showERRMessage(raiseError("XBE-ERR-03","From date of OND " + counter, top.strSysDate));
				$("#departureDate_"+t).focus();		
				return false;
			}
			
			if ($("#departureVariance_"+t).val() != ""){
				if($("#departureVariance_"+t).val() > top.strDefMaxV){
					showERRMessage(raiseError("XBE-ERR-07","Departure Variance of "+counter));
					$("#departureVariance_"+t).focus();	
					return false;
				}
				if (!UI_commonSystem.numberValidate({id:"#departureVariance_"+t, desc:"Departure Variance of OND " +counter, minValue:0, maxValue : top.strDefMaxV})){return false;}
			}else {
				$("#departureVariance_"+t).val(0);
			}
			
			
			var tempOnd = {};
			tempOnd.fromAirport = from.toUpperCase();
			tempOnd.toAirport = to.toUpperCase();
			tempOnd.fromCitySearch = isCitySearched($('#fromAirport_'+t).val());
			tempOnd.toCitySearch = isCitySearched($('#toAirport_'+t).val());
			tempOnd.flightRPHList = [];
			tempOnd.flownOnd = false;
			tempOnd.existingFlightRPHList = [];
			tempOnd.existingResSegRPHList = [];
			tempOnd.departureDate = converAAToJson($("#departureDate_"+t).val());
			tempOnd.depDate = $("#departureDate_"+t).val();
			tempOnd.departureVariance = $("#departureVariance_"+t).val();
			tempOnd.dateInteger = dateVal(dataformater($("#departureDate_"+t).val())).d;
			// Set cabin class & logical cabin class based on selection
			var cos = $("#classOfService_"+t).val();
			var cosArr = cos.split("-");
			// If user selects logical cabin class, value patter will be
			// 'XXX-XXX' (logical_cc-cabin_class)
			if(cosArr.length > 1){
				tempOnd.classOfService = cosArr[0];
				tempOnd.logicalCabinClass = cosArr[1];
			} else if(cosArr.length == 1) {
				tempOnd.classOfService = cosArr[0];
				tempOnd.logicalCabinClass = null;
			}
			tempOnd.bookingType = $("#bookingType_"+t).val();
			tempOnd.bookingClass = ($("#selBookingClassCode_"+t).val() == "")?null:$("#selBookingClassCode_"+t).val();
			ondListStr[ondListStr.length] = tempOnd;
		}
		/* Number Validations */
		if ($("#adultCount").val() != ""){
			if (!UI_commonSystem.numberValidate({id:"#adultCount", desc:"Number of Adults", minValue:0})){return false;}
		}else{
			$("#adultCount").val(0);
		}
		
		if ($("#childCount").val() != ""){
			if (!UI_commonSystem.numberValidate({id:"#childCount", desc:"Number of Children", minValue:0})){return false;}
		}else{
			$("#childCount").val(0);
		}
		
		if ($("#infantCount").val() != ""){
			if (!UI_commonSystem.numberValidate({id:"#infantCount", desc:"Number of Infants", minValue:0})){return false;}
			if(Number($("#infantCount").val()) > Number($("#adultCount").val())){
				showERRMessage(raiseError("XBE-ERR-05","Infant count", "adult count"));
				$("#infantCount").focus();	
				return false;
			}
		}else{
			$("#infantCount").val(0);
		}
		
		if (($("#adultCount").val() == "") || ($("#adultCount").val() == "0")){
			if (top.childBookingStatus != "Y") {
				showERRMessage(raiseError("XBE-ERR-01","Number of Adults"));
				$("#adultCount").focus();	
				return false;
			}
		}
		
		if((Number($("#adultCount").val())) > top.arrParams[1]){
			
			showERRMessage(raiseError("XBE-ERR-05","Adult count", "max pax count allowed ( "+top.arrParams[1]+" ) "));
			$("#adultCount").focus();
			return false;
			
		}
		
		if (top.childBookingStatus == "Y") {
			if ((String(trim($("#childCount").val())) == "" || String(trim($("#childCount").val())) == "0")
			 		&& (String(trim($("#adultCount").val())) == "" || String(trim($("#adultCount").val())) == "0")) {
				 showERRMessage(raiseError("XBE-ERR-01","No of Adult or Children"));
				 $("#childCount").focus();
				 return false;
			}
		}
		
		
		if (top.arrPrivi[PRIVI_GROUP_BOOKING] == 1){
		}else{
			var intTotPax = Number($("#adultCount").val()) + Number($("#childCount").val()) + Number($("#infantCount").val());
			if (intTotPax > top.intMaxGroup){
				showERRMessage(raiseError("XBE-ERR-15",String(top.intMaxGroup)));
				$("#adultCount").focus();	
				return false;
			}
		}
		UI_commonSystem.showProgress();
		return true;
	};
	
	
	initialization = function(){
		UI_commonSystem.initializeMessage();
		
		$("#frmSrchFlt").reset();
		
		if ((typeof(oldResAdultCount) != "undefined" && oldResAdultCount > 0)
				|| (typeof(oldResChildCount) != "undefined" && oldResChildCount > 0)) {
			$("#adultCount").val(oldResAdultCount);
			$("#childCount").val(oldResChildCount);
			$("#infantCount").val(oldResInfantCount);
		}
		else{
			
			$("#adultCount").val(1);
			$("#childCount").val(0);
			$("#infantCount").val(0);
		}
		
		$("#departureVariance").val(top.strDefDV);
		$("#departureDate_1").val(top.strSysDate);
		
		$("#selectedCurrency").val(DATA_ResPro.initialParams.defaultCurrencyCode);
		$("#classOfService").val(top.strDefCos);
		
		top.bookingType = ""; // TODO - If the variable available in the top
								// page remove this line
		top.bookingType = top.bookingType == "" ? top.arrBookingTypes[0][0] : top.bookingType;
		$("#bookingType").val(top.bookingType);

		$("#btnBook").hide();
		$("#btnFQ").enable();
		$("#returnDate").enable();
		$('#returnDate').datepicker().datepicker('enable')
		
		$("#divTaxBD").hide();
		$("#divSurBD").hide();
		$("#divBreakDownPane").hide();
		
		UI_tabSearchFlights.hideAirportMessage();
	};
	
	searchOnClick = function() {	
		$("#btnSearch").blur();
		clearAll();
		UI_tabSearchFlights.disableEnablePageControls(false);
		UI_tabSearchFlights.releaseBlockSeats();
		// Handling for transfer segment
		var isValidate = validateSearchFlight();
		if (isValidate) {
			resetSearchSystem();
			$("#frmSrchFlt").action("multiCitySearch.action");
			var data = {};
			ondListStr = ondListStr.sort(sortOnds);
			data['searchParams.ondListString'] =  JSON.stringify(ondListStr, null);
			if($('#selSearchOptions').attr('disabled')){
				data['searchParams.searchSystem'] = $('#selSearchOptions').val();
			}
			
			UI_tabSearchFlights.setExcludeChargesSearchParams(data);
			
			$("#frmSrchFlt").ajaxSubmit({ dataType: 'json',	data:data,										
											success: precessSearchData,
											error:UI_commonSystem.setErrorStatus});
			
		} else {
			if(UI_commonSystem.strPGMode != "") {
				UI_tabSearchFlights.disableEnablePageControls(true);
			}	
		}		
		UI_tabSearchFlights.hideAirportMessage();		
		return false;
	};
	
	extractDataAddCabinClass = function(dataObj, index){
		$.each(dataObj, function(flInd, flObj){
			var t_avaiCCList = flObj.availabilityByCOSList;
			var t_flightSegIdList = flObj.flightSegIdList;
			//actLogCabClassList active availabel cabin calss list
			if (t_avaiCCList.length == t_avaiCCList.length){
				if (t_avaiCCList.length != 0){
					for (var i=0;i<t_avaiCCList.length;i++){
						if (t_flightSegIdList[i] == t_avaiCCList[i].split("$")[0]){
							var ccInfo = t_avaiCCList[i].split("$")[1].split("#");
							for (var t = 0; t < ccInfo.length; t++){
								ccInfo[t] += ","+index;
								flObj["ccItem_"+t] = ccInfo[t];
							};
						}
					}
				}else{
					for (var d=0;d<actLogCabClassList.length;d++){
						flObj["ccItem_"+d] = "";
					}
				}
			}else{
				showERRMessage("Unmatch Class of service recived");
				return [];
			}
		});
		return dataObj;
	};
	
	this.createCarrierWiseOndMap = function() {
		var ondMap = '';
		for ( var i = 0; i < ondListStr.length; i++) {
			if (i == 0) {
				ondMap = '[{';
			}

			var ondObj = ondListStr[i];
			var firstFlightRef = ondObj.flightRPHList[0];			
			if(firstFlightRef != undefined){
				
				var carrierWiseOnd = UI_Multicity.buildCarrierWiseOnd(ondObj);

				if (i == (ondListStr.length - 1)) {
					ondMap += carrierWiseOnd + '}]';
				} else {
					ondMap += carrierWiseOnd + '},{';
				}
			} else if(i == (ondListStr.length - 1)) {
				ondMap += '}]';
			}				

		}
		return ondMap;
	};
	
	this.buildCarrierWiseOnd = function (ondObj) {
		var firstFlightRef = ondObj.flightRPHList[0];
		var lastFlightRef = ondObj.flightRPHList[ondObj.flightRPHList.length-1];
		var ond = '';		
		var fltRefInfoArray = firstFlightRef.split('$');
		var carrier = fltRefInfoArray[0];
		if (firstFlightRef==lastFlightRef) {			
			var airportCodes = UI_Multicity.getAirportCodesFromFlightRPH(firstFlightRef);			
			ond = airportCodes[0]+ '/' + airportCodes[airportCodes.length-1];	
		} else {			
			var firstSegAirportCodes = UI_Multicity.getAirportCodesFromFlightRPH(firstFlightRef);
			var lastSegAirportCodes = UI_Multicity.getAirportCodesFromFlightRPH(lastFlightRef);			
			ond = firstSegAirportCodes[0]+ '/' + lastSegAirportCodes[lastSegAirportCodes.length-1];
		}		
		var carrierWiseOnd = '"' + carrier + '":"' + ond + '"';
		return carrierWiseOnd;
	}
	
	this.getAirportCodesFromFlightRPH = function (fltRef) {
		var airportCodes = '';
		if (fltRef!=undefined && fltRef!=null) {			
			var fltRefInfoArray = fltRef.split('$');			
			var segmentCode = fltRefInfoArray[1];
			airportCodes = segmentCode.split('/');			
		}
		return airportCodes;
	}
	
	
	precessSearchData = function(response){
		if(response.success) {
			actLogCabClassList = response.actLogCabClassList;
			var ondSeqFlightsList = response.ondSeqFlightsList;
			//flightInfo = response.ondSeqFlightsList;
			airportMessage = response.airportMessage;
			busCarrierCodes = response.busCarrierCodes;
			UI_tabSearchFlights.busCarrierCodes = busCarrierCodes;
			UI_tabSearchFlights.objResSrchParams = response.searchParams;
			$("#tblONDFlights").empty();
			$.each(ondSeqFlightsList, function(index, dataObj){
					dataObj = extractDataAddCabinClass(dataObj, index)
					createFlightGridHTML(index);
					constructFlightGrid(index);
					fillFlightGrid(index, dataObj);
					if(dataObj.length == 0){
					    var ondSearchData = $.data($("#OnDTeml_"+index)[0], "OnDData");
			            var ond = ondSearchData.fromAirport+'/'+ondSearchData.toAirport;
					    showCommonError("WARNING",raiseError('XBE-ERR-91', ond));
					}
					
			});
			UI_commonSystem.hideProgress();
			$("#btnSearch").enable();
			$("#divResultsPane, #tblONDFlights").show();
			$("#trFareSection").show();
			if(!DATA_ResPro.initialParams.allCoSChangeFareEnabled){
				$("#btnCAllFare").hide();
			}
			$('#btnCAllFare').attr('disabled',true);
			UI_tabSearchFlights.loadAirportMessage();
			$("#trSearchParams_icon").trigger("click");
				
			if(typeof(UI_tabPassenger) != 'undefined'){	
				UI_tabPassenger.serviceTaxApplicable = response.serviceTaxApplicable;
			}
			
		} else {
			UI_commonSystem.hideProgress();
			showCommonError("ERROR", response.messageTxt);			
		}
	};
	
	dataformater = function(txt){
		 if (txt != null || txt != undefined) 
			 text = txt.split("/");
		 var dat = null;
		 if ($.browser.msie)
			 dat = Number(text[2])+"/"+Number(text[1])+"/"+Number(text[0]);
		 else
			 dat = Number(text[1])+"/"+Number(text[0])+"/"+Number(text[2]);
		 
		 return dat;
	 };
	
	dateVal = function( str1 ) {
	    var diff = Date.parse( str1 );
	    return isNaN( diff ) ? NaN : {
	    	diff : diff,
	    	ms : Math.floor( diff            % 1000 ),
	    	s  : Math.floor( diff /     1000 %   60 ),
	    	m  : Math.floor( diff /    60000 %   60 ),
	    	h  : Math.floor( diff /  3600000 %   24 ),
	    	d  : Math.floor( diff / 86400000        )
	    };
	};
	
	DateToString =function(dtdate){
		var dtCM = dtdate.getMonth() + 1;
		var dtCD = dtdate.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}
		return dtCD + "/" + dtCM + "/" + dtdate.getFullYear();;
	};
	
	addDaysAsString = function (strDate, intDays){
		 if (strDate != null){
			 var dtDate = new Date(Number(strDate.substring(6,10)), Number(strDate.substring(3,5))-1, Number(strDate.substring(0,2)));
			 var gmtOffsetOri = dtDate.getTimezoneOffset();
			 var calNewDate = new Date(dtDate.getTime() + Number(intDays) *24*60*60*1000);
			 var gmtOffsetNew = calNewDate.getTimezoneOffset();
			 //Assumption they won't be multiple dst changes with these days
			 if (gmtOffsetOri != gmtOffsetNew){
				 var offsetDifference = gmtOffsetOri - gmtOffsetNew;
				 calNewDate = new Date(calNewDate.getTime() - (offsetDifference*60*1000));	
			 } 
			 return DateToString(calNewDate);
		 }
	};
	
	searchNextPrevDay = function(flg, id){
		var index = id.split("_")[1];
		var _ondListStr = [];
		_ondListStr[_ondListStr.length] = ondListStr[index];
		_ondListStr[0].flightRPHList = [];
		var extDate = ondListStr[index].depDate;
		var boolSearch = false;
		if (flg=='next'){
			_ondListStr[0].depDate = addDaysAsString(extDate, 1);
			_ondListStr[0].departureDate = converAAToJson(addDaysAsString(extDate, 1));
			boolSearch = true;
		}else{
			if (dateVal(dataformater(top.strSysDate)).d < dateVal(dataformater(extDate)).d){
				_ondListStr[0].depDate = addDaysAsString(extDate, -1);
				_ondListStr[0].departureDate = converAAToJson(addDaysAsString(extDate, -1));
				boolSearch = true;
			}
		}
		if (boolSearch){
			$.data($("#OnDTeml_"+index)[0], "OnDData",_ondListStr[index]);
			$("#divOutDate_"+index).text(_ondListStr[0].depDate);
			researchCallAction(index, _ondListStr);
		}
	};
	
	initDynamicOndList = function(){
		var defaultLanguage = DATA_ResPro.systemParams.defaultLanguage;
		
		for(var originIndex=0;originIndex<origins.length;originIndex++){	
			if(typeof(origins[originIndex]) != "undefined"){
				var tempDestArray = [];
				for (var destIndex=0;destIndex<origins[originIndex].length;destIndex++) {
					// check whether operating carrier exists
					var curDest = origins[originIndex][destIndex];
				
					if(curDest[0] != null){					
						var currentSize = tempDestArray.length;
						tempDestArray[currentSize] = [];
						tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
						tempDestArray[currentSize][1] = airports[curDest[0]][defaultLanguage];
						tempDestArray[currentSize][2] = airports[curDest[0]]['code'] + ' - ' + airports[curDest[0]]['en'];
						var cityCode = 'N';
						if(airports[curDest[0]]['city']!=undefined && airports[curDest[0]]['city']!=null 
								&& (airports[curDest[0]]['city'] || airports[curDest[0]]['city']=="true")){
							cityCode = 'Y';
						}						
						tempDestArray[currentSize][3] = cityCode ;
						tempDestArray[currentSize][4] = airports[curDest[0]]['code']+'_'+cityCode ;						
					}
				
				}
			
				if (tempDestArray.length > 0) {				
					tempDestArray.sort(UI_tabSearchFlights.customSort);
					var citySearch = 'N';
					if(airports[originIndex]['city']!=undefined && airports[originIndex]['city']!=null 
							&& (airports[originIndex]['city'] || airports[originIndex]['city']=="true")){
						citySearch = 'Y';
					}
					
					
					ondDataMap[airports[originIndex]['code']+'_'+citySearch] = tempDestArray;
				}
			}
		}
	};
	
	dateOnBlur = function(inParam){
		if($.trim($("#departureDate_"+inParam.id+inParam.flg).val()) != ''){
			// $("#departureDate").val(dateChk($("#departureDate").val()));
			dateChk("departureDate_"+inParam.id+inParam.flg);
		}else{
			$("#departureDate_"+inParam.id+"_"+inParam.flg).val('');
		}
	};
	
	/*
	 * Fill Grid with data
	 */
	fillFlightGrid = function(ind, dataSet){
		if (dataSet != null  && dataSet.length > 0 && dataSet[0].flightSegIdList.length != 0){
			UI_commonSystem.fillGridData({id:"#tblONDFlights_"+ind, data:dataSet});
		}
		$.data($("#OnDTeml_"+ind)[0], "OnDData", ondListStr[ind]);
		$.data($("#OnDTeml_"+ind)[0],"OnDResult", dataSet);
		$("#divOutDate_"+ind).text(ondListStr[ind].depDate)
		UI_tabSearchFlights.strOutDate = ondListStr[ind].depDate;
		IntCommentShowHide();
	};
	
	
	sortOnds = function(ond1, ond2) {
	/*	if (ond1.dateInteger === ond2.dateInteger){
			if(ond1.toAirport == ond2.fromAirport){
				return -1;
			}else if(ond1.fromAirport == ond2.toAirport){
				return 1;
			}
			return 0;
		}else{*/
			return ond1.dateInteger > ond2.dateInteger;
	/*	}*/
	}
	
	/*
	 * Grid HTML Constructor
	 */
	createFlightGridHTML = function(index){
		var tClone = OnDGRIDHTML.clone();
		tClone.attr("id", tClone.attr("id")+"_"+index);
		$.each(tClone.find("a, table, span"),function(){
			$(this).attr("id", this.id+"_"+index);
		});
		$("#tblONDFlights").append(tClone);
		$("#modOND_"+index).click(function(){
			var index = parseInt(this.id.split("_")[1],10);
			createOnDHTML("Mod", index)
		});
	};
	/*
	 * Grid Constructor
	 */
	constructFlightGrid = function(ind){
		// construct grid
		
		var segBreakFmt = function (objValue, options, rowObject){
			var arr = objValue.split("/");
			var outStr = '';
			for(var i = 0; i < arr.length ; i++){
				if( i == 0){					
					outStr +=arr[i];  
				}else if(i%3 == 0){
					outStr+= '/<br />' +arr[i];
				}else {
					outStr+= '/' +arr[i];
				}
				
			}
			return outStr ;
		};
		
		var radioButtonFmt = function(index, options, rowObject){
			return "";
		}
		
		var arrBreakFmt = function (arr, options, rowObject){
			var outStr = '';
			var first = true;
			for(var i = 0; i < arr.length ; i++){
				if(!first) outStr  += '<br />';
				outStr +=arr[i];
				first = false;
			}
			return outStr ;
		}
		
		var carrierCodeFmt = function (arr, options, rowObject){
			for(var i = 0; i < arr.length; i++){				
				arr[i] = getBusCarrierCode(arr[i]);
			}
			return arrBreakFmt(arr, options, rowObject);
		};
		
		ccInfoBraker = function (arr, options, rowObject){
			arr = arr.split(",");	
			var seatAvaiList = rowObject.seatAvaiStatusByCOSList;
			var cabinClassAvailable = false;
			$.each(seatAvaiList, function(i, j){
				var cabinClass = j.split("#");
				if(cabinClass[0] == arr[0] && cabinClass[1] == "true") {
					cabinClassAvailable = true;
					return false;
				}
			});
			
			var name = "selFl_"+ arr[arr.length-1];
			//var role = arr[arr.length-2];
			var role = rowObject.availabilityList;
			var id = "selFl_"+options.rowId+"_"+arr[0];
			var val = rowObject.flightRPHList.toString();
			var str = "N/A";
			if (cabinClassAvailable){
				str = "<input type='radio' role='"+role+"' id='"+id+"' name='"+name+"' value='"+val+"' class='selectFlight' onclick='changeSelectedFlight(this.id)'/>";
			}
			return str;
		};
		
		var gColNameModel = ['&nbsp;',geti18nData('availability_Segment','segment'), geti18nData('availability_Flight','Flight'), geti18nData('availability_OperatingCarrier','Op'),
					          geti18nData('availability_Departure','Departure') , geti18nData('availability_Arrival','Arrival') , geti18nData('availability_Available','Avl')];
			
		var gColModel = [
			{name:'index',  index:'index', width:20, align:"center", formatter:radioButtonFmt },
			{name:'ondCode',  index:'ondCode', width:110, align:"left", formatter: segBreakFmt},
			{name:'flightNoList', index:'flightNoList', width:80, align:"center", formatter: arrBreakFmt},
			{name:'carrierList', index:'carrierList', width:50, align:"center", formatter: carrierCodeFmt},
			{name:'depatureList', index:'depatureList', width:100, align:"center", formatter: arrBreakFmt},
			{name:'arrivalList', index:'arrivalList', width:100, align:"center", formatter: arrBreakFmt},
			{name:'availabilityList', index:'availabilityList', width:60, align:"right", formatter: arrBreakFmt, hidden:true}
		];

		if (actLogCabClassList.length > 0){
			$.each(actLogCabClassList,function(i, j){
				gColNameModel[gColNameModel.length] = j;
				var t = {name:"ccItem_"+i, index:"ccItem_"+i, width:50, align:"center", formatter: ccInfoBraker}
				gColModel[gColModel.length] = t;
			});
		}
		
		$("#tblONDFlights_"+ind).jqGrid({
			datatype: "local",
			height: 110,
			width: 645,
			colNames:gColNameModel,
			colModel:gColModel,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				UI_tabSearchFlights.clearLogicalCCData();
				for ( var i = 0; i < ondListStr.length; i++) {										
					$('#OnDLogicalCCBody_' + i).empty();
					UI_tabSearchFlights.ondWiseFlexiSelected[i] = false;
					UI_tabSearchFlights.ondWiseFlexiQuote[i] = true;
					UI_tabSearchFlights.ondWiseBundleFareSelected[i] = null;
					UI_tabSearchFlights.ondWiseBookingClassSelected[i] = null;
				}
			}
		});
	};
	
	changeSelectedFlight = function(id){
		UI_tabSearchFlights.segWiseBookingClassOverride = {};
		clearFareQuote();
		$("#btnFQ").removeAttr("disabled");
	};
	
	decodeAAToJson= function(dstr) {
		var yy = parseInt(dstr.substring(0, 2), 10) + 2000;
		var MM = dstr.substring(2, 4);
		var dd = dstr.substring(4, 6);
		var hh = dstr.substring(6, 8);
		var mm = dstr.substring(8, 10);
		return yy + '-' + MM + '-' + dd + 'T' + hh + ':' + mm + ':00';
	};
	
	
	converAAToJson = function(dstr) {
		var sTDAr = dstr.split("/");
		var yy = sTDAr[2];
		var MM = sTDAr[1];
		var dd = sTDAr[0];
		var hh = '00';
		var mm = '00';
		return yy + '-' + MM + '-' + dd + 'T' + hh + ':' + mm + ':00';
	};
	
	getDepartureDateForCalendar = function(departureDateTime) {
		var dtArr = departureDateTime.split("T");
		var dateArr = dtArr[0].split("-");
		return dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
	};
	
	getBusCarrierCode = function(carrierCode){
		if(isBusCarrierCode(carrierCode)){
			return '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">';
		}
		return carrierCode;
	};
	
	isBusCarrierCode = function(carrierCode){
		if(busCarrierCodes!=undefined && busCarrierCodes!=null){
			for(var i =0; i < busCarrierCodes.length; i++){
				if(carrierCode == busCarrierCodes[i] ){
					return true;
				}
			}
		}		
		return false;
	};
	
	researchCallAction = function(tIndex, reqData){
		$("#OnDTeml_"+tIndex).find(".OnDTemlBody").empty();
		$("#OnDTeml_"+tIndex).find(".OnDTemlBody").append('<table id="tblONDFlights_'+tIndex+'" class="scroll"></table>');
		constructFlightGrid(tIndex);
		resetSearchSystem();
		$("#frmSrchFlt").action("multiCitySearch.action");
		var data = {}; 
		data['searchParams.ondListString'] =  JSON.stringify(reqData, null);
		if($('#selSearchOptions').attr('disabled')){
			data['searchParams.searchSystem'] = $('#selSearchOptions').val();
		}
		clearFareQuote();
		UI_commonSystem.showProgress();
		$("#frmSrchFlt").ajaxSubmit({ dataType: 'json',	data:data,										
										success: function(response){
											if(response.success){
												if (response.ondSeqFlightsList[0].length!=0){
													//flightInfo = response.ondSeqFlightsList;
													dataObj = extractDataAddCabinClass(response.ondSeqFlightsList[0], tIndex)
													busCarrierCodes = response.busCarrierCodes;
													UI_tabSearchFlights.busCarrierCodes = busCarrierCodes;
													fillFlightGrid(tIndex, dataObj);
													UI_commonSystem.hideProgress();
												}else{
													UI_commonSystem.hideProgress();
													var ondSearchData = $.data($("#OnDTeml_"+tIndex)[0], "OnDData");
								                    var ond = ondSearchData.fromAirport+'/'+ondSearchData.toAirport;
								                    showCommonError("WARNING",raiseError('XBE-ERR-91', ond));
												}
											}else{
												UI_commonSystem.hideProgress();
												showERRMessage(response.messageTxt);
											}
										},
										error:UI_commonSystem.setErrorStatus});
	};
	
	
	reSearchOND = function(event, param){
		var tIndex = event.target.id.split("_")[1];
		//validate one ond
		var from = getAirportCode($('#fromAirport_'+tIndex+'_Mod').val());
		var to = getAirportCode($('#toAirport_'+tIndex+'_Mod').val());
		
		var isValidate = function(){
			
			if(from.toUpperCase() == 'PLEASE SELECT A VALUE' ||
					from.toUpperCase() == ''){
				showERRMessage(raiseError("XBE-ERR-01", "Departure location of OND " + counter ));
				$('#fromAirport_'+tIndex+'_Mod').focus();			
				return false;
			}
			
			if(to.toUpperCase() == 'PLEASE SELECT A VALUE' ||
					to.toUpperCase() == ''){
				showERRMessage(raiseError("XBE-ERR-01", "Arrival location of OND " + counter));
				$('#toAirport_'+tIndex+'_Mod').focus();			
				return false;
			}
			
			if (from == to){
				showERRMessage(raiseError("XBE-ERR-02","Departure location","Arrival location of OND " + counter));
				$('#fromAirport_'+tIndex+'_Mod').focus();			
				return false;
			}
			
			if(UI_commonSystem.strPGMode != "confirmOprt"){
				if (!CheckDates(top.strSysDate, $('#departureDate_'+tIndex+'_Mod').val())){
					showERRMessage(raiseError("XBE-ERR-03","From date of OND " + counter, top.strSysDate));
					$('#departureDate_'+tIndex+'_Mod').focus();		
					return false;
				}
			}
			
			
			if ($('#departureVariance_'+tIndex+'_Mod').val() != ""){
				if($('#departureVariance_'+tIndex+'_Mod').val() > top.strDefMaxV){
					showERRMessage(raiseError("XBE-ERR-07","Departure Variance of "+counter));
					$('#departureVariance_'+tIndex+'_Mod').focus();	
					return false;
				}
				if (!UI_commonSystem.numberValidate({id:'#departureVariance_'+tIndex+'_Mod', desc:"Departure Variance of OND " +counter, minValue:0, maxValue : top.strDefMaxV})){return false;}
			}else {
				$('#departureVariance_'+tIndex+'_Mod').val(0);
			}
			return true;
		};
		
		
		if (isValidate) {
			UI_tabSearchFlights.disableEnablePageControls(false);
			var tempOnd = $.data($("#OnDTeml_"+tIndex)[0], "OnDData");
			tempOnd.fromAirport = from.toUpperCase();
			tempOnd.toAirport = to.toUpperCase();
			tempOnd.fromCitySearch = isCitySearched($('#fromAirport_'+tIndex+'_Mod').val());
			tempOnd.toCitySearch = isCitySearched($('#toAirport_'+tIndex+'_Mod').val());
			tempOnd.flightRPHList = [];
			tempOnd.flownOnd = false;
			tempOnd.existingFlightRPHList = [];
			tempOnd.existingResSegRPHList = [];
			tempOnd.departureDate = converAAToJson($('#departureDate_'+tIndex+'_Mod').val());
			tempOnd.depDate = $('#departureDate_'+tIndex+'_Mod').val();
			tempOnd.departureVariance = $('#departureVariance_'+tIndex+'_Mod').val();
			// Set cabin class & logical cabin class based on selection
			var cos = $('#classOfService_'+tIndex+'_Mod').val();
			var cosArr = cos.split("-");
			// If user selects logical cabin class, value patter will be
			// 'XXX-XXX' (logical_cc-cabin_class)
			if(cosArr.length > 1){
				tempOnd.classOfService = cosArr[0];
				tempOnd.logicalCabinClass = cosArr[1];
			} else if(cosArr.length == 1) {
				tempOnd.classOfService = cosArr[0];
				tempOnd.logicalCabinClass = null;
			}
			tempOnd.bookingType = $('#bookingType_'+tIndex+'_Mod').val();
			tempOnd.bookingClass = ($('#selBookingClassCode_'+tIndex+'_Mod').val()=="")?null:$('#selBookingClassCode_'+tIndex+'_Mod').val();
			$.data($("#OnDTeml_"+tIndex)[0], "OnDData",tempOnd);
			var tempONDArr = [];
			tempONDArr[tempONDArr.length] = tempOnd;
			researchCallAction(tIndex, tempONDArr);
			$("#modTable_"+tIndex).remove();
			UI_tabSearchFlights.hideAirportMessage();		
			return false;
		} else {
			if(UI_commonSystem.strPGMode != "") {
				UI_tabSearchFlights.disableEnablePageControls(true);
			}
			return false;
		}

	};
	
	
	getFlightInfo = function(fltRPH) {
		var flightInfo = UI_Multicity.flightInfo;
		var fltInfoJson;
		if(flightInfo!=null && flightInfo.length >0){
			for(var i=0;i< flightInfo.length;i++){
				for(var x=0;x< flightInfo[i].length;x++){
					var flt = flightInfo[i][x];
					
					var selFlightRPHList = flt.flightRPHList;					
					
					for(var j=0;j<selFlightRPHList.length;j++){
						if(selFlightRPHList[j] == fltRPH){
							//Selected Flight Found
							fltInfoJson= {
								flightRPH : flt.flightRPHList[j],
								segmentCode : flt.segmentCodeList[j],
								flightNo : flt.flightNoList[j],
								arrivalTime : flt.arrivalTimeList[j],
								departureTime : flt.depatureTimeList[j],
								carrierCode : flt.carrierList[j],
								departureTimeZulu : flt.departureTimeZuluList[j],
								arrivalTimeZulu : flt.arrivalTimeZuluList[j],
								returnFlag : flt.returnFlag,
								domesticFlight : flt.domesticFlightList[j],
								ondSequence :i
							};
							
							
						}
					}
				}
				
			}
		}
		return fltInfoJson;
	}
	
	this.getFlightRPHList = function() {
		var outFlt = ondListFQStr;
		var rphArray = [];
		
		for ( var i = 0; i < outFlt.length; i++) {
			var flightRPHList = outFlt[i].flightRPHList;
			for ( var x = 0; x < flightRPHList.length; x++) {
				rphArray[rphArray.length] = getFlightInfo(flightRPHList[x]);
			}
			
		}
		
		UI_tabSearchFlights.fltRPHInfo = $.toJSON(rphArray);
		return rphArray;
	};
	
	this.hasOverbookOnd = function() {
		var ondList = ondListFQStr;
		var list = [];
		for ( var i = 0; i < ondList.length; i++) {			
			if (ondList[i].bookingType == 'OVERBOOK') {
				return true;
			}
		}
		return false;
	}
	
	
	this.ondBookType = function() {
		
		var ondList = ondListFQStr;
		var prevBookType = '';
		var isMixBookType = false;
		for ( var i = 0; i < ondList.length; i++) {			
			
			if(i > 0 && prevBookType!=ondList[i].bookingType) {
				isMixBookType = true;
			}			
			prevBookType=ondList[i].bookingType;						
		}	
		
		if(!isMixBookType){
			return prevBookType;
		}		
		return null;
	}
	

	this.validateFareQuote = function() {		
		var rphArray = UI_Multicity.getFlightRPHList();		
		if (rphArray != null && rphArray.length > 0) {			

			//checks for OND overlapping
			for ( var i = 0; i < rphArray.length; i++) {				
				for ( var j = 1; j < rphArray.length; j++) {
					var obj1 = rphArray[i];
					var obj2 = rphArray[j];
					if(obj1.flightRPH != obj2.flightRPH){
						var error = false;		
						var flt1ArrivalTime = decodeAADate(obj1.arrivalTimeZulu).getTime();
						var flt2ArrivalTime = decodeAADate(obj2.arrivalTimeZulu).getTime();							
						var flt1DepartureTime = decodeAADate(obj1.departureTimeZulu).getTime()
						var flt2DepartureTime = decodeAADate(obj2.departureTimeZulu).getTime()	
							
						if (flt2ArrivalTime <= flt1ArrivalTime
								&& flt2ArrivalTime  >= flt1DepartureTime) {
							error = true;
						}

						if (flt2ArrivalTime > flt1ArrivalTime
								&& flt2DepartureTime <= flt1ArrivalTime) {
							error = true;
						}

						if (error) {						
							showERRMessage("Invalid flights selection, flights are overlapping!")						
							return false;
						}
					}
				}
			}
			
			return true;
		}else{
			showERRMessage(raiseError("XBE-ERR-06","OND"));	
		}
		
	}
	
	function decodeAADate(dstr) {
		var yy = parseInt(dstr.substring(0, 2), 10) + 2000;
		var MM = parseInt(dstr.substring(2, 4), 10) - 1;
		var dd = parseInt(dstr.substring(4, 6), 10);
		var hh = parseInt(dstr.substring(6, 8), 10);
		var mm = parseInt(dstr.substring(8, 10), 10);
		// new Date(year, month, day, hours, minutes, seconds,
		// milliseconds);
		return new Date(yy, MM, dd, hh, mm, 0, 0);
	}
	
	this.ondWiseFlexiSelected = function() {
		var ondList = ondListFQStr;
		var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);				
		for ( var i = 0; i < ondList.length; i++) {
			if(ondFlexiQuote[i] == undefined || ondFlexiQuote[i] == null){				
				ondFlexiQuote[i]=false;									
			}
		}	
		UI_tabSearchFlights.ondWiseFlexiSelected = ondFlexiQuote;
		return ondFlexiQuote;
	}
	
	this.ondWiseLogicalCabinClassSelection = function() {
		var ondList = ondListFQStr;
		var logicalCabinClassSelection = $.airutil.dom.cloneObject(UI_tabSearchFlights.logicalCabinClassSelection);				
		for ( var i = 0; i < ondList.length; i++) {
			
			if(ondList[i].logicalCabinClass!=null && ondList[i].logicalCabinClass!=""){
				logicalCabinClassSelection[i]=ondList[i].logicalCabinClass;	
			}else{
				logicalCabinClassSelection[i]=ondList[i].classOfService;	
			}			
												
		}	
		UI_tabSearchFlights.logicalCabinClassSelection =logicalCabinClassSelection;
		return logicalCabinClassSelection;
	}
	
	loadFareQuoteOnClick = function(){
		UI_Multicity.flightInfo = [];
		ondListFQStr = [];
		var j = 0;
		var isValidFlightSelection = true;
		$.each(ondListStr, function(intIndex, OND){
			var searchData = $.data($("#OnDTeml_"+intIndex)[0], "OnDData");
			var resultsData = $.data($("#OnDTeml_"+intIndex)[0], "OnDResult");
			UI_Multicity.flightInfo[UI_Multicity.flightInfo.length] = resultsData;
			if(!UI_tabSearchFlights.ondRebuildLogicalCCAvailability[j]){
				UI_tabSearchFlights.ondRebuildLogicalCCAvailability[j] = true;
			}
			j++;
			var selectedFl = $("#tblONDFlights_"+intIndex).find("input.selectFlight:checked")
			if (selectedFl.length!=0){
				var tRPHList = selectedFl.val().split(",");
				var rowID = parseInt(selectedFl[0].id.split("_")[1], 10);
				var cc = selectedFl[0].id.split("_")[2];
				var dipTime = resultsData[rowID-1].depatureTimeList;
				for (var i=0; i<dipTime.length;i++){
					searchData.departureDate = decodeAAToJson(dipTime[i]);
				}
				searchData.classOfService = cc;
				//searchData.logicalCabinClass = cc;
				UI_tabSearchFlights.objResSrchParams.classOfService = cc;
 
				searchData.flightRPHList = tRPHList;
				ondListFQStr[ondListFQStr.length] = searchData;
			} else {
				//some ONDs fligths are not selected 
				isValidFlightSelection = false;
			}
		});
		
		if(isValidFlightSelection && UI_Multicity.validateFareQuote()){
			resetSearchSystem();			
			var data = {};
			data['fareQuoteParams.ondListString'] = JSON.stringify(ondListFQStr, null);
			data['fareQuoteParams.selectedCurrency'] = $("#selectedCurrency").val();
			data['fareQuoteParams.searchSystem'] = $("#selSearchOptions").val();
			data['fareQuoteParams.paxType'] = $("#selPaxTypeOptions").val();
			data['fareQuoteParams.adultCount'] = $("#adultCount").val();
			data['fareQuoteParams.childCount'] = $("#childCount").val();
			data['fareQuoteParams.infantCount'] = $("#infantCount").val();			
			data['fareQuoteParams.bookingType'] = UI_Multicity.ondBookType();
			data['fareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(UI_tabSearchFlights.ondWiseFlexiQuote);
			data['fareQuoteParams.ondAvailbleFlexiStr'] = $.toJSON(UI_tabSearchFlights.ondWiseFlexiAvailable);
			data['fareQuoteParams.ondSelectedFlexiStr'] = $.toJSON(UI_tabSearchFlights.ondWiseFlexiSelected);
			data['fareQuoteParams.preferredBundledFares'] = $.toJSON(UI_tabSearchFlights.ondWiseBundleFareSelected);
			data['fareQuoteParams.preferredBookingCodes'] = $.toJSON(UI_tabSearchFlights.ondWiseBookingClassSelected);
			data['fareQuoteParams.promoCode'] = $('#promoCode').val();
			data['fareQuoteParams.ondSegBookingClassStr'] = $.toJSON(UI_tabSearchFlights.segWiseBookingClassOverride);
			data['fareQuoteParams.travelAgentCode'] = $("#travelAgentCode").val();
			
			// Add exclude Charges
			UI_tabSearchFlights.composeExcludeChargesData(data, 'fareQuoteParams');
			
			UI_Multicity.fqParams = data;
			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', async:true, data:data , url:"multiCityFareQuote.action",
				success:returnCallShowFareQuoteInfo,
				error:UI_commonSystem.setErrorStatus});
		} else if (!isValidFlightSelection) {
			showERRMessage(raiseError("XBE-ERR-06","OND"));
		}
		$("#btnCAllFare").removeAttr("disabled");
	};
	
	returnCallShowFareQuoteInfo = function(response){
		response = $.extend(response, {"ondListString":JSON.stringify(ondListFQStr, null)} );
		UI_tabSearchFlights.returnCallShowFareQuoteInfo(response);
	};
	
	this.getONDSearchDTOList = function() {
		return ondListFQStr;
	}
	
	this.getJsonONDSearchDTOList = function(){
		return $.toJSON(ondListFQStr);
	}
	
	IntCommentShowHide = function(){
		$(".selectFlight").unbind("mouseover").bind("mouseover",function(event){
			displayCommentTooltTip(event, this);
		});
		$(".selectFlight").unbind("mouseout").bind("mouseout",function(){
			hideCommentTooltTip();
		});
	};
	
	displayCommentTooltTip = function(e, role){
		$("#divResultsPane").find("#commentToolTip").remove();
		var div = $( '<div id="commentToolTip"></div>' ).css({
			"display":"none",
			"position": "absolute",
			"width":"220px",
			"z-index":20000,
			"background-color":"#fffed4",
			"border-radius": "10px",
			"border": "1px solid #ECEC00",
			"padding": "10px",
			"opacity":.9,
			"font-family":"Lucida Grande,Lucida Sans,Arial,sans-serif"
		});
		$("#divResultsPane").append(div);
		$(role).parent().removeAttr("title");
		$("#commentToolTip").html($(role).attr("role"));
		$("#commentToolTip").css({
			"top":e.pageY+8,
			"left":e.pageX - 200
		})
		$("#commentToolTip").show();
	};
	
	hideCommentTooltTip = function(){
		$("#commentToolTip").hide();
		$("#divResultsPane").find("#commentToolTip").remove();
	};	
	
	resetSearchSystem = function() {

		var allOND = $("#multiOnds").find(".trMultiOND");
		var isInt = false;
		if (DATA_ResPro.systemParams.dynamicOndListPopulationEnabled
				&& ondListLoaded() && allOND != null && allOND.length > 0) {
			for ( var p = 0; p < allOND.length; p++) {
				var t = allOND[p].id.split("_")[1];
				var isFromSel = true;
				var isToSel = true;
				var from = getAirportCode($('#fromAirport_' + t).val());
				var to = getAirportCode($('#toAirport_' + t).val());
				if (from.toUpperCase() == 'PLEASE SELECT A VALUE'
						|| from.toUpperCase() == '') {
					isFromSel = false;
				}

				if (to.toUpperCase() == 'PLEASE SELECT A VALUE'
						|| to.toUpperCase() == '') {
					isToSel = false;
				}

				if (from == to) {
					isFromSel = false;
					isToSel = false;
				}

				if (!isInt && isFromSel && isToSel) {

					var fromIndex = -1;
					var toIndex = -1;
					// get from, to indexes
					var airportsLength = airports.length;
					for ( var i = 0; i < airportsLength; i++) {
						if (from == airports[i].code) {
							fromIndex = i;
						} else if (to == airports[i].code) {
							toIndex = i;
						}
					}

					if (fromIndex != -1) {
						var availDestOptLength = origins[fromIndex].length;
						for ( var j = 0; j < availDestOptLength; j++) {
							var destOptArr = origins[fromIndex][j];
							if (destOptArr[0] == toIndex) {
								var isInt = false;
								var optCarriersArr = destOptArr[3].split(",");
								for ( var n = 0; n < optCarriersArr.length; n++) {
									if (optCarriersArr[n] != carrierCode) {
										isInt = true;										
									}
								}

							}
						}
					}					

				}			

			}
		
			if (isInt) {
				$('#selSearchOptions').val('INT');
			} else {
				$('#selSearchOptions').val('AA');
			}

		}

	};	
		
};
this.getJsonOndWiseFlexiSelected = function(){
	return $.toJSON(UI_Multicity.ondWiseFlexiSelected());
};

this.getAirportCode = function(code){
	
	if(code==undefined || code==null || code == '' ) return '';
	
	var value = code;
	var arr = code.split('_');	
	value = arr[0];	
	return value;
}

this.isCitySearched = function(code){

	
	if(code==undefined || code==null || code == '' ) return false;
	
	var value = code;
	var arr = code.split('_');		
	
	if(arr!=null && arr.length > 1 && arr[1]==='Y'){
		return true;
	}
	
	return false;	
}

UI_Multicity = new UI_Multicity();