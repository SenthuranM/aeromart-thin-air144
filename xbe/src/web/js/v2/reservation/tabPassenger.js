/*
 *********************************************************
	Description		: XBE Make Reservation - Passenger Tab
	Author			: Rilwan A. Latiff
	Version			: 1.0
	Created			: 25th August 2008
	Last Modified	: 25th August 2008
 *********************************************************	
 */

var jsonPaxObj = [];

var jsonPaxAdtObj = ( {
	"paxAdults" : [],

	"paxInfants" : []
});

//Value accessible from paxDetails.js. Used to specify whether passport details were validated or not
var passportDetailsvalidated=true;
var internationalFlightDetailsValidated = true;

//Value to make the close button of file upload dialog not popup the paxdetails window if parsing has failed.
var parseSuccessful=false;

var phoneObj = top.arrCountryPhoneJson;

//store loaded profile email address
var profEmailAddress = "";

/* Flight Details */
var jsonFltDetails = [];
var jsonOutFlts = null;
var jsonRetFlts = null;
var jsonOpenRetInfo = null;

/* Price Informations */
var jsonPaxPrice = [];

var jsonGoQuoServicesAsSSR;

var jsonPaxTypes = "";
var jsonInfPaxTypes = ":;MSTR:Mstr;MISS:Miss";
var jsonNationality = "";
var jsonTravellingWith = ""; // Traveling with Drop Down Data
var paxValidation;

//this will keep the mapping of validation group to
//client error code
var jsonGroupValidation = {"1":"XBE-ERR-26"};

//this will keep the mapping of fieldname+groupid of t_contact_details_config
//to html form element
var jsonFieldToElementMapping = {"phoneNo1":"#txtPhoneNo", "mobileNo1":"#txtMobiNo"};

/* variables will keep pax configuration of AD, CH & IN*/
var paxConfigAD = "";
var paxConfigCH = "";
var paxConfigIN = "";

var tempCarrierList = "";
var tempPaxType = "";
var lastSegmentDepartureDate = "";
var csvFileUpload = false;


UI_tabPassenger.isFFID = true;
UI_tabPassenger.isCsPnr = false;

/*
 * Passenger Tab
 */
function UI_tabPassenger() {
}

UI_tabPassenger.intLastSSRRow = null;
UI_tabPassenger.strLastSSRType = null;

UI_tabPassenger.strAdtCode = "ADT";
UI_tabPassenger.strInfCode = "INF";
UI_tabPassenger.xbeTBA = "T B A";

UI_tabPassenger.isTabLoaded = false;
UI_tabPassenger.allowBufferTimeOnHold = true;

UI_tabPassenger.foidIds = "";
UI_tabPassenger.foidCode = "";
UI_tabPassenger.segmentList = "";
//UI_tabPassenger.blackListedPaxList=null;
UI_tabPassenger.reasonForAllowBlPax ="";

UI_tabPassenger.anciAvailabilityResponse = null;
flightLsttSegmentDeptDate = "";

UI_tabPassenger.showTaxRegNo = false;
UI_tabPassenger.showState = false;
UI_tabPassenger.showAddress = false;
UI_tabPassenger.serviceTaxApplicable = false;

/*
 * passenger Informations Tab Page On Load
 */
UI_tabPassenger.resetData = function() {
	UI_tabPassenger.anciAvailabilityResponse = null;
}


UI_tabPassenger.adjustSpacesBetweenFields = function (resetCity) {
	var vOffsetForServiceTaxGroup = -21;

	if (!contactConfig.address1.xbeVisibility) {
		if (!UI_tabPassenger.showAddress) {
			vOffsetForServiceTaxGroup -= 34;
			$('#tblCity').css({"position": "relative", "top": "-34px"});
		}
		if (resetCity) {
			$('#tblCity').css({"position": "relative", "top": "0px"});
		}
	}
	if (!contactConfig.zipCode1.xbeVisibility) {
		vOffsetForServiceTaxGroup -= 17;
	}
	if (!contactConfig.nationality1.xbeVisibility) {
		vOffsetForServiceTaxGroup -= 17;
	}
	$('.service-tax-group').css({"position": "relative", "top": vOffsetForServiceTaxGroup + "px"});
};


UI_tabPassenger.ready = function() {
	
	var data = {};
	data['system'] = UI_tabSearchFlights.getSelectedSystem();
	data['carriers'] = UI_tabPassenger.createCarrierList();
	data['paxType'] =  UI_tabSearchFlights.strPaxType;
	
	if(DATA_ResPro.initialParams.allowDisplayPaxNames == false){
		$("#btnPaxNames").hide();
	}
	
	tempCarrierList = data['carriers'];
	
	if(!UI_tabPassenger.checkPaxContactConfigAvailable(data['carriers'], data['paxType'])){		
		tempPaxType = data['paxType'];
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, async:false, url:"loadPaxContactConfig.action",							
			success: UI_tabPassenger.contactConfigSuccess,error:UI_commonSystem.setErrorStatus});
	}
		
	if(DATA_ResPro.initialParams.countryPaxConfigEnabled){
		var submitData = {};
		submitData['system'] = UI_tabSearchFlights.getSelectedSystem();
		submitData['origin'] = UI_tabSearchFlights.objResSrchParams.fromAirport;
		submitData['destination'] = UI_tabSearchFlights.objResSrchParams.toAirport;
		submitData['paxType'] = UI_tabSearchFlights.strPaxType;
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:submitData, async:false, url:"loadPaxContactConfigCountry.action",							
			success: UI_tabPassenger.contactConfigCountrySuccess,error:UI_commonSystem.setErrorStatus});
	}
	UI_tabPassenger.isCsPnr = false;
	for (flt = 0; flt < jsonFltDetails.length; flt++) {
		if (jsonFltDetails[flt].csOcCarrierCode != null
				&& jsonFltDetails[flt].csOcCarrierCode.length == 2) {
			UI_tabPassenger.isCsPnr = true;
			break;
		}
	}
		
	if(contactConfig != null && contactConfig != ""){
		jsonPaxObj = [{
			id : "#selTitle",
			desc : "Title",
			required : (contactConfig.title1.mandatory & contactConfig.title1.xbeVisibility) 
		}, {
			id : "#txtFirstName",
			desc : "First Name",
			required : (contactConfig.firstName1.mandatory & contactConfig.firstName1.xbeVisibility)
		}, {
			id : "#txtLastName",
			desc : "Last Name",
			required : (contactConfig.lastName1.mandatory & contactConfig.lastName1.xbeVisibility)
		}, {
			id : "#txtStreet",
			desc : "Street",
			required : (contactConfig.address1.mandatory & contactConfig.address1.xbeVisibility)
		}, {
			id : "#txtAddress",
			desc : "Address",
			required : false
		},{
			id : "#selNationality",
			desc : "Nationality",
			required: (contactConfig.nationality1.mandatory & contactConfig.nationality1.xbeVisibility)
		}, {
			id : "#txtZipCode",
			desc : "Zip Code",
			required: (contactConfig.zipCode1.mandatory & contactConfig.zipCode1.xbeVisibility)
		}, {
			id : "#txtCity",
			desc : "City / Town",
			required : (contactConfig.city1.mandatory & contactConfig.city1.xbeVisibility)
		}, {
			id : "#selCountry",
			desc : "Country",
			required : (contactConfig.country1.mandatory & contactConfig.country1.xbeVisibility)
		}, {
			id : "#txtMobCntry",
			desc : "Mobile Country Code",
			required : (contactConfig.mobileNo1.mandatory & contactConfig.mobileNo1.xbeVisibility)
		}, {
			id : "#txtMobArea",
			desc : "Mobile Area Code",
			required : (contactConfig.mobileNo1.mandatory & contactConfig.mobileNo1.xbeVisibility)
		}, {
			id : "#txtMobiNo",
			desc : "Mobile No",
			required : (contactConfig.mobileNo1.mandatory & contactConfig.mobileNo1.xbeVisibility)
		}, {
			id : "#txtPhoneCntry",
			desc : "Phone Country Code",
			required : (contactConfig.phoneNo1.mandatory & contactConfig.phoneNo1.xbeVisibility)
		 }, {
			id : "#txtPhoneArea",
			desc : "Phone Area Code",
			required : (contactConfig.phoneNo1.mandatory & contactConfig.phoneNo1.xbeVisibility)
		}, {
			id : "#txtPhoneNo",
			desc : "Phone No",
			required : (contactConfig.phoneNo1.mandatory & contactConfig.phoneNo1.xbeVisibility)
		}, {
			id : "#txtFaxCntry",
			desc : "Fax Country Code",
			required : (contactConfig.fax1.mandatory & contactConfig.fax1.xbeVisibility)
		}, {
			id : "#txtFaxArea",
			desc : "Fax Area Code",
			required : (contactConfig.fax1.mandatory & contactConfig.fax1.xbeVisibility)
		}, {
			id : "#txtFaxNo",
			desc : "Fax No",
			required : (contactConfig.fax1.mandatory & contactConfig.fax1.xbeVisibility)
		}, {
			id : "#txtEmail",
			desc : "Email",
			required : (contactConfig.email1.mandatory & contactConfig.email1.xbeVisibility)
		}, {
			id : "#txtUserNotes",
			desc : "User Notes",
			required : (contactConfig.userNote1.mandatory & contactConfig.userNote1.xbeVisibility)
		}, {
			id : "#selPrefLang",
			desc : "Preferred Language",
			required : (contactConfig.preferredLang1.mandatory & contactConfig.preferredLang1.xbeVisibility)
		}, {
			id : "#txtPNR",
			desc : "Customer Profile"
		}, {
			id : "#selEmgnTitle",
			desc : "Emergency Contact Title",
			required: (contactConfig.title2.mandatory & contactConfig.title2.xbeVisibility)
		}, {
			id : "#txtEmgnFirstName",
			desc : "Emergency Contact First Name",
			required: (contactConfig.firstName2.mandatory & contactConfig.firstName2.xbeVisibility)
		}, {
			id : "#txtEmgnLastName",
			desc : "Emergency Contact Last Name",
			required: (contactConfig.lastName2.mandatory & contactConfig.lastName2.xbeVisibility)
		}, {
			id : "#txtEmgnPhoneCntry",
			desc : "Emergency Contact Phone Country Code",
			required : (contactConfig.phoneNo2.mandatory & contactConfig.phoneNo2.xbeVisibility)
		}, {
			id : "#txtEmgnPhoneArea",
			desc : "Emergency Contact Phone Area Code",
			required : (contactConfig.phoneNo2.mandatory & contactConfig.phoneNo2.xbeVisibility)
		}, {
			id : "#txtEmgnPhoneNo",
			desc : "Emergency Contact Phone No",
			required : (contactConfig.phoneNo2.mandatory & contactConfig.phoneNo2.xbeVisibility)
		}, {
			id : "#txtEmgnEmail",
			desc : "Emergency Contact Email",
			required: (contactConfig.email2.mandatory & contactConfig.email2.xbeVisibility)
		}];
	}
	
	// Show/Hide Contact Info & Emergency contact info based on the configurations
	UI_tabPassenger.showHideContactInfo();
	
	// if tab is not yet loaded load it
	if (!UI_tabPassenger.isTabLoaded) {				
		$("#btnPHold").decorateButton();
		$("#btnPReset").decorateButton();
		$("#btnPCancel").decorateButton();
		$("#btnContinue").decorateButton();
		$("#btnSSRConfirm").decorateButton();
		$("#btnSSRCancel").decorateButton();
		$("#btnWaitList").decorateButton();
		$("#btnPromoRemoveOk").decorateButton();
		$('#btnPromoRemoveNo').decorateButton();
		
		$('#btnPromoRemoveOk').click(function(){ 
			$('#divPromoRemoveConfirm').hide();
			UI_tabPassenger.continueToAnci(true, false);
		});
		$('#btnPromoRemoveNo').click(function(){
			$('#divPromoRemoveConfirm').hide();			
		});

		if (top.arrPrivi[PRIVI_LOAD_PROFILE] == 1) {
			$("#chkUpdateCustProf").disable();
			$("#btnLoadProfile").decorateButton();
			$("#btnLoadProfile").click(function() {
				UI_tabPassenger.loadProfileOnClick();
			});
			
			$("#txtEmail").keyup(function(){UI_tabPassenger.validateProfileEmail(this)});
			$("#txtEmail").keypress(function(){UI_tabPassenger.validateProfileEmail(this)});
		}

		if (top.arrPrivi[PRIVI_TBA_BKG] == 1) {
			$("#btnTBA").decorateButton();
			$("#btnTBA").click(function() {
				UI_tabPassenger.applyTBAOnClick();
			});
		}

		$("#btnPaxNames").decorateButton();
		$("#btnPaxNames").click(function(){
			UI_tabPassenger.paxNamessOnClick();
		});
		$("#btnPaxDetails").decorateButton();
		$("#btnPaxDetails").click(function() {
			UI_tabPassenger.paxDetailsOnClick();
		});
		
		
		$("#btnInternationalFlight").decorateButton();
				$("#btnInternationalFlight").decorateButton();
		$("#btnInternationalFlight").click(function() {
			UI_tabPassenger.internationalFlightDetailsOnClick();
		});

		$("#btnUploadFromCSV").decorateButton();
		$("#btnCSVUploadSampleFile").decorateButton();

		$( "#csvFileUploadDiv" ).dialog({
			modal: true,
			width: 500,
			height: 300,
			title: "Upload File",
			autoOpen: false,
			buttons: {
				Close: function() {
					$( this ).dialog( "close" );
					if($('#btnPaxDetails').is(":visible") && parseSuccessful){
						$('#btnPaxDetails').trigger('click');
					} else if ($('#btnInternationalFlight').is(":visible") && parseSuccessful) {
						$('#btnInternationalFlight').trigger('click');
					}
				}
			}
		});
		
		$("#btnUploadFromCSV").click(function() {
			csvFileUpload = true;
			$("#csvFileUploadDiv").dialog('open');
		});
		$("#btnCSVUploadSampleFile").click(function() {
			$("#sampleFileIFrameDiv").append('<iframe width="0" height="0" frameborder="0" src="SampleFileDownload.action"></iframe>');
		});
		
		if(DATA_ResPro.initialParams.allowPassengerDetailCSVUpload || DATA_ResPro.initialParams.tbaGroupNameUpload){
			$("#btnUploadFromCSV").show();
			$("#btnCSVUploadSampleFile").show();
		}else{
			$("#btnUploadFromCSV").hide();
			$("#btnCSVUploadSampleFile").hide();
		}
		
		$("#btnSetData").decorateButton();
		$("#btnSetData").click(function() {
			UI_tabPassenger.setDummyData();
		});	
		
		$("#btnPHold").click(function() {
			$("#selCountry").prop('disabled', false);
			$("#selState").prop('disabled', false);
			UI_tabPassenger.onHoldOnClick();
		});
		
		$("#btnWaitList").click(function() {
			UI_tabPassenger.waitListOnClick();
		});

		$("#btnPReset").click(function() {
			UI_tabPassenger.resetOnClick();
		});
		$("#btnContinue").click(function() {
			UI_tabPassenger.continueOnClick();
		});
		$("#btnPCancel").click(function() {
			UI_tabPassenger.cancelOnClick();
		});

		$("#btnSSRConfirm").click(function() {
			UI_tabPassenger.ssrConfirmOnClick();
		});
		$("#btnSSRCancel").click(function() {
			UI_tabPassenger.ssrCancelOnClick();
		});
		UI_tabPassenger.fillDropDownData();
		
		$("#txtMobCntry").alphaNumericWhiteSpace();
		$("#txtMobArea").alphaNumericWhiteSpace();
		$("#txtMobiNo").alphaNumericWhiteSpace();

		$("#txtPhoneCntry").alphaNumericWhiteSpace();
		$("#txtPhoneArea").alphaNumericWhiteSpace();
		$("#txtPhoneNo").alphaNumericWhiteSpace();

		$("#txtFaxCntry").alphaNumericWhiteSpace();
		$("#txtFaxArea").alphaNumericWhiteSpace();
		$("#txtFaxNo").alphaNumericWhiteSpace();

		$("#txtUserNotes").focus(function() {
			$(this).height("50px");
			var position = $(this).position(); // position = { left: 42, top:
												// 567 }
			position.top -= 32;
			position.position = "";
			$(this).css(position);
		});
		$("#txtUserNotes").blur(function() {
			$(this).css({position:''});
			$(this).height("18px");
		});
		if ($.browser.msie) {
			$("#txtUserNotes").css( {
				"top" : "592px"
			})
		}

		$("#divSSRPane").hide();
		
		if(contactConfig.title1.xbeVisibility){
			$("#selTitle").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.firstName1.xbeVisibility){
			$("#txtFirstName").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.lastName1.xbeVisibility){
			$("#txtLastName").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.address1.xbeVisibility){
			$("#txtStreet").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
			
			$("#txtAddress").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.city1.xbeVisibility){
			$("#txtCity").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.country1.xbeVisibility){
			$("#selCountry").change(function() {
				$("#txtMobArea").val('');
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
				UI_tabPassenger.countryOnChange(true);
			});
		}
		if(contactConfig.mobileNo1.xbeVisibility){
			$("#txtMobCntry").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
			$("#txtMobArea").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
			$("#txtMobiNo").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.phoneNo1.xbeVisibility){
			$("#txtPhoneCntry").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
			$("#txtPhoneArea").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
			$("#txtPhoneNo").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.fax1.xbeVisibility){
			$("#txtFaxCntry").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
			$("#txtFaxArea").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
			$("#txtFaxNo").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.email1.xbeVisibility){
			$("#txtEmail").change(function() {
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}
		if(contactConfig.preferredLang1.xbeVisibility){
			$("#selPrefLang").change(function(){
				UI_tabPassenger.passengerInfoChanged();
				UI_commonSystem.pageOnChange();
			});
		}

		UI_tabPassenger.adjustSpacesBetweenFields();

		$("#txtUserNotes").change(function() {
			UI_tabPassenger.passengerInfoChanged();
			UI_commonSystem.pageOnChange();
		});
		if(!DATA_ResPro.initialParams.allowClassifyUserNotes){   //snk
			$('#userNoteType').hide();
		}

		$("#txtTaxRegNo").change(function() {
			UI_tabPassenger.passengerInfoChanged();
			UI_commonSystem.pageOnChange();
		});
		
		// $('#tblPaxTopMost input').change(function (){
		// UI_tabPassenger.passengerInfoChanged(); });
		// $('#tblPaxTopMost select').change(function (){
		// UI_tabPassenger.passengerInfoChanged(); });
		UI_tabPassenger.isTabLoaded = true;
	}
	var recordFound = false;
	for (flt = 0; flt < jsonFltDetails.length; flt++) {
		if (jsonFltDetails[flt].airLine != null && top.hubCountryArry != undefined && recordFound == false) {
			for (hub = 0; hub < top.hubCountryArry.length; hub++) {
				if (top.hubCountryArry[hub][0] == jsonFltDetails[flt].airLine) {
					$('#selOriginCountryOfCall').val(top.hubCountryArry[hub][1]);
					recordFound = true;
					break;
				}
			}
		}
	}
		
	if(paxConfigAD!= undefined && paxConfigAD.passportNo != undefined && paxConfigAD.passportNo.xbeVisibility != undefined && !paxConfigAD.passportNo.xbeVisibility &&
			!paxConfigCH.passportNo.xbeVisibility && !paxConfigIN.passportNo.xbeVisibility &&
			!paxConfigAD.groupId.xbeVisibility && !paxConfigCH.groupId.xbeVisibility &&
			!paxConfigIN.groupId.xbeVisibility && !paxConfigAD.visaDocNumber.xbeVisibility &&
			!paxConfigCH.visaDocNumber.xbeVisibility && !paxConfigIN.visaDocNumber.xbeVisibility){
		$("#btnPaxDetails").hide();
	} else {
		$("#btnPaxDetails").show();
	}

	if(!DATA_ResPro.initialParams.allowCaptureExternalInternationalFlightDetails) {
		$("#btnInternationalFlight").hide();
	} else {
		$("#btnInternationalFlight").show();
	}
	
	/* TODO remove this */
	UI_tabPassenger.passengerDetailsOnLoadCall();
	UI_commonSystem.pageOnChange();
	UI_PageShortCuts.initilizeMetaDataForPassengerScreen();

	if(!top.arrParams[44]){
		$("#mobileNoHelpRow").disable();
	}
	UI_tabPassenger.loadPaxContactDataFromOldReservation();	
	
	
	$("#blPaxChkAlertConfirm").click(function() {
		UI_tabPassenger.clickBlacklistpaxAlertContinue();
	});
	
	$("#blPaxChkAlertCancel").click(function() {
		UI_tabPassenger.clickBlacklistpaxAlertCancel();
	});
	
	$("#reasonToAllowBlacklist").val("");

	$("#saveFrequentCustomerPopUp").dialog({ 
		autoOpen: false,
		modal:true,
		height: 'auto',
		width: '900px',
		title:'Save Frequent Customer',
		close:function() {$("#saveFrequentCustomerPopUp").dialog('close');}
	});
	
	$("#populateFrequentCustomerPopUp").dialog({ 
		autoOpen: false,
		modal:true,
		height: 'auto',
		width: '990px',
		title:'Populate Frequent Customer',
		close:function() {$("#populateFrequentCustomerPopUp").dialog('close');}
	});
			
	UI_tabPassenger.showHideTaxRegNoDetails();
}

/**
 * Validates the uploading file size and extension.
 * File size validation may not be supported in IE or older browsers will skip size validation if unavailable
 * 
 * NOTE: This method signature must match whats being called in xbe/src/web/WEB-INF/jsp/v2/reservation/PassengerDetailsParseResult.jsp
 */
validatePaxCounts = function(adultCount,childCount,infantCount){
	var infantLength=0;
	var adultLength=0;
	var childLength=0;
	
	if(jsonPaxAdtObj.paxInfants != null){
		infantLength=jsonPaxAdtObj.paxInfants.length;
	}
	
	for(var i=0;i<jsonPaxAdtObj.paxAdults.length;i++){
		if (jsonPaxAdtObj.paxAdults[i].displayAdultType == "AD") {
			adultLength++;
		} else if(jsonPaxAdtObj.paxAdults[i].displayAdultType == "CH"){
			childLength++;
		}
	}
	
	if(adultLength==adultCount && childLength==childCount && infantLength==infantCount){
		return true;
	}
	else{
		return false;
	}
}

/**
 * Sets the passed on adult details. If the selected amount of adult passengers are lesser than the number of parsed data
 * this function will break execution flow.
 * 
 * NOTE: This method signature must match whats being called in xbe/src/web/WEB-INF/jsp/v2/reservation/PassengerDetailsParseResult.jsp
 */
setAdultDetails = function (adultList, loadPaxInfoFromCSV){
	for (var i = 0; i < adultList.length; i++) {
		var paxRec=adultList[i];
		
		if(loadPaxInfoFromCSV){
			$("#" + (i + 1) + "_displayAdultTitle option").each(function() {
				if($(this).text().toUpperCase().trim().replace(/[.]/g , '') == paxRec.displayAdultTitle.toUpperCase().trim().replace(/[.]/g , '')) {
					    $(this).attr('selected', 'selected');            
				}                        
			});		
			
			$("#" + (i + 1) + "_displayAdultNationality option").each(function() {
				if($(this).text().toUpperCase().trim() == paxRec.displayAdultNationality.toUpperCase().trim()) {
				    $(this).attr('selected', 'selected');           
				}                        
			});
		} else {
			$("#" +(i + 1)+ "_displayAdultTitle").val(paxRec.displayAdultTitle);
			$("#" +(i + 1)+ "_displayAdultNationality").val(paxRec.displayAdultNationality);
		}
		
		$("#" + (i + 1) + "_displayAdultFirstName").val(paxRec.displayAdultFirstName);
		$("#" + (i + 1) + "_displayAdultLastName").val(paxRec.displayAdultLastName);
		
		$("#" + (i + 1) + "_displayFFID").val(paxRec.displayFFID);
		
		$("#" + (i + 1) + "_displayAdultNationality option").each(function() {
			if($(this).text().toUpperCase().trim() == paxRec.displayAdultNationality.toUpperCase().trim()) {
			    $(this).attr('selected', 'selected');           
			}                        
		});
		
		$("#" + (i + 1) + "_displayAdultDOB").val(paxRec.displayAdultDOB);
		
		jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber=(paxConfigAD !== undefined && paxConfigAD.passportNo != undefined && !paxConfigAD.passportNo.xbeVisibility) ? null : paxRec.displayPnrPaxCatFOIDNumber;
		jsonPaxAdtObj.paxAdults[i].displayNationalIDNo=paxRec.displayNationalIDNo;
		jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry=(paxConfigAD !== undefined && paxConfigAD.passportExpiry != undefined && !paxConfigAD.passportExpiry.xbeVisibility) ? null : paxRec.displayPnrPaxCatFOIDExpiry;
		jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDPlace=(paxConfigAD !== undefined && paxConfigAD.passportIssuedCntry != undefined && !paxConfigAD.passportIssuedCntry.xbeVisibility) ? null : paxRec.displayPnrPaxCatFOIDPlace;
		jsonPaxAdtObj.paxAdults[i].displayPnrPaxPlaceOfBirth=paxRec.displayPnrPaxPlaceOfBirth;
		jsonPaxAdtObj.paxAdults[i].displayTravelDocType=paxRec.displayTravelDocType;
		jsonPaxAdtObj.paxAdults[i].displayVisaDocNumber=paxRec.displayVisaDocNumber;
		jsonPaxAdtObj.paxAdults[i].displayVisaDocPlaceOfIssue=paxRec.displayVisaDocPlaceOfIssue;
		jsonPaxAdtObj.paxAdults[i].displayVisaDocIssueDate=paxRec.displayVisaDocIssueDate;
		jsonPaxAdtObj.paxAdults[i].displayVisaApplicableCountry=paxRec.displayVisaApplicableCountry;
		jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber=paxRec.displaypnrPaxArrivalFlightNumber;
		jsonPaxAdtObj.paxAdults[i].displaypnrPaxFltArrivalDate=paxRec.displaypnrPaxFltArrivalDate;
		jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalTime=paxRec.displaypnrPaxArrivalTime;
		jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber=paxRec.displaypnrPaxDepartureFlightNumber;
		jsonPaxAdtObj.paxAdults[i].displaypnrPaxFltDepartureDate=paxRec.displaypnrPaxFltDepartureDate;
		jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureTime=paxRec.displaypnrPaxDepartureTime;
		jsonPaxAdtObj.paxAdults[i].displayPnrPaxGroupId=paxRec.displayPnrPaxGroupId;
		jsonPaxAdtObj.paxAdults[i].displayFFID=paxRec.displayFFID;
	}
}

/**
 * Sets the passed on infant details.
 * 
 * NOTE: This method signature must match whats being called in xbe/src/web/WEB-INF/jsp/v2/reservation/PassengerDetailsParseResult.jsp
 */
setInfantDetails = function(infantList, loadPaxInfoFromCSV){
	for ( var i = 0; i < infantList.length; i++) {
		
		var infantRec=infantList[i];
		
		$("#" + (i + 1) + "_displayInfantFirstName").val(infantRec.displayInfantFirstName);
		$("#" + (i + 1) + "_displayInfantLastName").val(infantRec.displayInfantLastName);
		$("#" + (i + 1) + "_displayInfantTitle").val(infantRec.displayInfantTitle);
		
		
		if(loadPaxInfoFromCSV) {
			$("#" + (i + 1) + "_displayInfantNationality option").each(function() {
				if($(this).text().toUpperCase() == infantRec.displayInfantNationality.toUpperCase()) {
				    $(this).attr('selected', 'selected');            
				}                        
			});
		} else {
			$("#" +(i + 1)+ "_displayInfantNationality").val(infantRec.displayInfantNationality);
		}
		
		
		$("#" + (i + 1) + "_displayInfantDOB").val(infantRec.displayInfantDOB);
		
		$("#" + (i + 1) + "_displayInfantTravellingWith").val(infantRec.displayInfantTravellingWith);

		
		jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDNumber=(paxConfigIN !== undefined && paxConfigIN.passportNo != undefined && !paxConfigIN.passportNo.xbeVisibility) ? null : infantRec.displayPnrPaxCatFOIDNumber;
		jsonPaxAdtObj.paxInfants[i].displayNationalIDNo=infantRec.displayNationalIDNo;
		jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDExpiry=(paxConfigIN !== undefined && paxConfigIN.passportExpiry != undefined && !paxConfigIN.passportExpiry.xbeVisibility)?null:infantRec.displayPnrPaxCatFOIDExpiry;
		jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDPlace=(paxConfigIN !== undefined && paxConfigIN.passportIssuedCntry != undefined && !paxConfigIN.passportIssuedCntry.xbeVisibility)?null:infantRec.displayPnrPaxCatFOIDPlace;
		jsonPaxAdtObj.paxInfants[i].displayPnrPaxPlaceOfBirth=infantRec.displayPnrPaxPlaceOfBirth;
		jsonPaxAdtObj.paxInfants[i].displayTravelDocType=infantRec.displayTravelDocType;
		jsonPaxAdtObj.paxInfants[i].displayVisaDocNumber=infantRec.displayVisaDocNumber;
		jsonPaxAdtObj.paxInfants[i].displayVisaDocPlaceOfIssue=infantRec.displayVisaDocPlaceOfIssue;
		jsonPaxAdtObj.paxInfants[i].displayVisaDocIssueDate=infantRec.displayVisaDocIssueDate;
		jsonPaxAdtObj.paxInfants[i].displayVisaApplicableCountry=infantRec.displayVisaApplicableCountry;
		jsonPaxAdtObj.paxInfants[i].displaypnrPaxArrivalFlightNumber=infantRec.displaypnrPaxArrivalFlightNumber;
		jsonPaxAdtObj.paxInfants[i].displaypnrPaxFltArrivalDate=infantRec.displaypnrPaxFltArrivalDate;
		jsonPaxAdtObj.paxInfants[i].displaypnrPaxArrivalTime=infantRec.displaypnrPaxArrivalTime;
		jsonPaxAdtObj.paxInfants[i].displaypnrPaxDepartureFlightNumber=infantRec.displaypnrPaxDepartureFlightNumber;
		jsonPaxAdtObj.paxInfants[i].displaypnrPaxFltDepartureDate=infantRec.displaypnrPaxFltDepartureDate;
		jsonPaxAdtObj.paxInfants[i].displaypnrPaxDepartureTime=infantRec.displaypnrPaxDepartureTime;
		jsonPaxAdtObj.paxInfants[i].displayPnrPaxGroupId=infantRec.displayPnrPaxGroupId;
	}
}

UI_tabPassenger.createCarrierList = function(){	
	var carrierList = '';
	if(jsonFltDetails != []){
		for(var i=0;i<jsonFltDetails.length;i++){
			if(!UI_tabPassenger.isCarrierExists(carrierList, jsonFltDetails[i].airLine)){
				if(i == (jsonFltDetails.length - 1)){
					carrierList += jsonFltDetails[i].airLine;
				} else {
					carrierList += jsonFltDetails[i].airLine + ",";
				}
			}
		}
	}
	
	return carrierList;
}

UI_tabPassenger.isCarrierExists = function(arr, elem){
	var spltArr = arr.split(",");
	for(var i=0;i<spltArr.length;i++){
		if(spltArr[i] == elem){
			return true;		 
		}
	}	
	return false;
}

UI_tabPassenger.checkPaxContactConfigAvailable = function(carrierList, paxType){
	var cArr = getArrayFromStr(carrierList);
	var carrierCombinations = getCombinations(cArr);
	
	var exists = false;
	for(var i=0;i<carrierCombinations.length;i++){
		if(typeof(top.globalPaxContactConfig[carrierCombinations[i]+paxType]) != 'undefined'){
			contactConfig = top.globalPaxContactConfig[carrierCombinations[i]+paxType].contactConfig;
			validationGroup = top.globalPaxContactConfig[carrierCombinations[i]+paxType].validationGroup;
			paxConfigAD = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxConfigAD;
			paxConfigCH = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxConfigCH;
			paxConfigIN = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxConfigIN;
			paxValidation = top.globalPaxContactConfig[carrierCombinations[i]+paxType].paxValidation;
			alternativeEmailForXBE = top.globalPaxContactConfig[carrierCombinations[i]+paxType].alternativeEmailForXBE;
			exists = true;
			break;
		}
	}
	
	return exists;
	
}

UI_tabPassenger.contactConfigSuccess = function(response){
	if(response.success){
		
		//Save configuration in top
		var carrierArr = getArrayFromStr(tempCarrierList);
		var carrierStr = "";
		for ( var i = 0; i < carrierArr.length; i++) {
			carrierStr += carrierArr[i];
		}
		
		top.globalPaxContactConfig[carrierStr+tempPaxType] = response;
		if(DATA_ResPro.initialParams.countryPaxConfigEnabled){
			top.tempGlobalPaxContactConfig[carrierStr+tempPaxType] = jQuery.extend(true, new Object(), response);
		}
		contactConfig = response.contactConfig;
		alternativeEmailForXBE = response.alternativeEmailForXBE;
		validationGroup = response.validationGroup;
		paxConfigAD = response.paxConfigAD;
		paxConfigCH = response.paxConfigCH;
		paxConfigIN = response.paxConfigIN;
		paxValidation = response.paxValidation;
	}
}

UI_tabPassenger.contactConfigCountrySuccess = function(response){
	
	var carrierArr = getArrayFromStr(tempCarrierList);
	var carrierStr = "";
	for ( var i = 0; i < carrierArr.length; i++) {
		carrierStr += carrierArr[i];
	}
	
	var paxType = UI_tabSearchFlights.strPaxType;
	if(response.success && DATA_ResPro.initialParams.countryPaxConfigEnabled){
		var paxCountryConfigAD = response.countryPaxConfigAD;
		var paxCountryConfigCH = response.countryPaxConfigCH;
		var paxCountryConfigIN = response.countryPaxConfigIN;
		// For Autocheckin
		var paxAutocheckinConfigAD = response.countryAutoCheckinPaxConfigAD;
		var paxAutocheckinConfigCH = response.countryAutoCheckinPaxConfigCH;
		var paxAutocheckinConfigIN = response.countryAutoCheckinPaxConfigIN;
		
		UI_tabPassenger.overridePaxConfig(paxCountryConfigAD,carrierStr,paxType,'AD');
		UI_tabPassenger.overridePaxConfig(paxCountryConfigCH,carrierStr,paxType,'CH');
		UI_tabPassenger.overridePaxConfig(paxCountryConfigIN,carrierStr,paxType,'IN');
		// For Autocheckin
		UI_tabPassenger.overrideAutocheckinConfig(paxAutocheckinConfigAD,carrierStr,paxType,'AD');
		UI_tabPassenger.overrideAutocheckinConfig(paxAutocheckinConfigCH,carrierStr,paxType,'CH');
		UI_tabPassenger.overrideAutocheckinConfig(paxAutocheckinConfigIN,carrierStr,paxType,'IN');
	} else if(!response.success && DATA_ResPro.initialParams.countryPaxConfigEnabled){
		// restore pax config deatils from original pax config
		var indexStr = carrierStr + paxType;
		top.globalPaxContactConfig[indexStr] = $.airutil.dom.cloneObject(top.tempGlobalPaxContactConfig[indexStr]);
		paxConfigAD = top.globalPaxContactConfig[indexStr].paxConfigAD;
		paxConfigCH = top.globalPaxContactConfig[indexStr].paxConfigCH;
		paxConfigIN = top.globalPaxContactConfig[indexStr].paxConfigIN;
	}
	
}

UI_tabPassenger.overridePaxConfig = function(paxCountryConfig,carrierStr,paxType,paxCatType){
	var indexStr = carrierStr + paxType;
	
	if(paxCatType == 'AD') {
		top.globalPaxContactConfig[indexStr].paxConfigAD.title.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.title.xbeMandatory || paxCountryConfig.title);
		top.globalPaxContactConfig[indexStr].paxConfigAD.firstName.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.firstName.xbeMandatory || paxCountryConfig.firstName);
		top.globalPaxContactConfig[indexStr].paxConfigAD.lastName.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.lastName.xbeMandatory || paxCountryConfig.lastName);
		top.globalPaxContactConfig[indexStr].paxConfigAD.nationality.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.nationality.xbeMandatory || paxCountryConfig.nationality);
		top.globalPaxContactConfig[indexStr].paxConfigAD.dob.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.dob.xbeMandatory || paxCountryConfig.dob);
		top.globalPaxContactConfig[indexStr].paxConfigAD.passportNo.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.passportNo.xbeMandatory || paxCountryConfig.passportNo);
		top.globalPaxContactConfig[indexStr].paxConfigAD.passportExpiry.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.passportExpiry.xbeMandatory || paxCountryConfig.passportExpiry);
		top.globalPaxContactConfig[indexStr].paxConfigAD.passportIssuedCntry.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.passportIssuedCntry.xbeMandatory || paxCountryConfig.passportIssuedCntry);
		top.globalPaxContactConfig[indexStr].paxConfigAD.groupId.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigAD.groupId.xbeMandatory || paxCountryConfig.groupId);		
	} else if (paxCatType == 'CH') {
		top.globalPaxContactConfig[indexStr].paxConfigCH.title.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.title.xbeMandatory || paxCountryConfig.title);
		top.globalPaxContactConfig[indexStr].paxConfigCH.firstName.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.firstName.xbeMandatory || paxCountryConfig.firstName);
		top.globalPaxContactConfig[indexStr].paxConfigCH.lastName.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.lastName.xbeMandatory || paxCountryConfig.lastName);
		top.globalPaxContactConfig[indexStr].paxConfigCH.nationality.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.nationality.xbeMandatory || paxCountryConfig.nationality);
		top.globalPaxContactConfig[indexStr].paxConfigCH.dob.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.dob.xbeMandatory || paxCountryConfig.dob);
		top.globalPaxContactConfig[indexStr].paxConfigCH.passportNo.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.passportNo.xbeMandatory || paxCountryConfig.passportNo);
		top.globalPaxContactConfig[indexStr].paxConfigCH.passportExpiry.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.passportExpiry.xbeMandatory || paxCountryConfig.passportExpiry);
		top.globalPaxContactConfig[indexStr].paxConfigCH.passportIssuedCntry.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.passportIssuedCntry.xbeMandatory || paxCountryConfig.passportIssuedCntry);
		top.globalPaxContactConfig[indexStr].paxConfigCH.groupId.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigCH.groupId.xbeMandatory || paxCountryConfig.groupId);	
	} else if (paxCatType == 'IN') {
		top.globalPaxContactConfig[indexStr].paxConfigIN.travelWith.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.travelWith.xbeMandatory || paxCountryConfig.travelWith);
		top.globalPaxContactConfig[indexStr].paxConfigIN.firstName.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.firstName.xbeMandatory || paxCountryConfig.firstName);
		top.globalPaxContactConfig[indexStr].paxConfigIN.lastName.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.lastName.xbeMandatory || paxCountryConfig.lastName);
		top.globalPaxContactConfig[indexStr].paxConfigIN.nationality.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.nationality.xbeMandatory || paxCountryConfig.nationality);
		top.globalPaxContactConfig[indexStr].paxConfigIN.dob.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.dob.xbeMandatory || paxCountryConfig.dob);
		top.globalPaxContactConfig[indexStr].paxConfigIN.passportNo.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.passportNo.xbeMandatory || paxCountryConfig.passportNo);
		top.globalPaxContactConfig[indexStr].paxConfigIN.passportExpiry.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.passportExpiry.xbeMandatory || paxCountryConfig.passportExpiry);
		top.globalPaxContactConfig[indexStr].paxConfigIN.passportIssuedCntry.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.passportIssuedCntry.xbeMandatory || paxCountryConfig.passportIssuedCntry);
		top.globalPaxContactConfig[indexStr].paxConfigIN.groupId.xbeMandatory = 
			( top.tempGlobalPaxContactConfig[indexStr].paxConfigIN.groupId.xbeMandatory || paxCountryConfig.groupId);
	} 
}

UI_tabPassenger.overrideAutocheckinConfig = function(paxAutocheckinConfigAD,carrierStr,paxType,paxCatType){
	var indexStr = carrierStr + paxType;
	
	if(paxCatType == 'AD') { 
		if(paxAutocheckinConfigAD.title != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.title.autoCheckinMandatory = paxAutocheckinConfigAD.title;
		}
		if(paxAutocheckinConfigAD.firstName != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.firstName.autoCheckinMandatory = paxAutocheckinConfigAD.firstName;
		}
		if(paxAutocheckinConfigAD.lastName != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.lastName.autoCheckinMandatory = paxAutocheckinConfigAD.lastName;
		}
		if(paxAutocheckinConfigAD.nationality != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.nationality.autoCheckinMandatory = paxAutocheckinConfigAD.nationality;
		}
		if(paxAutocheckinConfigAD.dob != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.dob.autoCheckinMandatory = paxAutocheckinConfigAD.dob;
		}
		if(paxAutocheckinConfigAD.passportNo != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportNo.autoCheckinMandatory = paxAutocheckinConfigAD.passportNo;
		}
		if(paxAutocheckinConfigAD.passportExpiry != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportExpiry.autoCheckinMandatory = paxAutocheckinConfigAD.passportExpiry;
		}
		if(paxAutocheckinConfigAD.passportIssuedCntry != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportIssuedCntry.autoCheckinMandatory = paxAutocheckinConfigAD.passportIssuedCntry;
		}
		if(paxAutocheckinConfigAD.groupId != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.groupId.autoCheckinMandatory = paxAutocheckinConfigAD.groupId;
		}
	} else if (paxCatType == 'CH') {
		if(paxAutocheckinConfigAD.title != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.title.autoCheckinMandatory = paxAutocheckinConfigAD.title;
		}
		if(paxAutocheckinConfigAD.firstName != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.firstName.autoCheckinMandatory = paxAutocheckinConfigAD.firstName;
		}
		if(paxAutocheckinConfigAD.lastName != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.lastName.autoCheckinMandatory = paxAutocheckinConfigAD.lastName;
		}
		if(paxAutocheckinConfigAD.nationality != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.nationality.autoCheckinMandatory = paxAutocheckinConfigAD.nationality;
		}
		if(paxAutocheckinConfigAD.dob != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.dob.autoCheckinMandatory = paxAutocheckinConfigAD.dob;
		}
		if(paxAutocheckinConfigAD.passportNo != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportNo.autoCheckinMandatory = paxAutocheckinConfigAD.passportNo;
		}
		if(paxAutocheckinConfigAD.passportExpiry != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportExpiry.autoCheckinMandatory = paxAutocheckinConfigAD.passportExpiry;
		}
		if(paxAutocheckinConfigAD.passportIssuedCntry != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportIssuedCntry.autoCheckinMandatory = paxAutocheckinConfigAD.passportIssuedCntry;
		}
		if(paxAutocheckinConfigAD.groupId != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.groupId.autoCheckinMandatory = paxAutocheckinConfigAD.groupId;
		}
		console.log("top.globalPaxContactConfig[indexStr].paxConfigAD " + top.globalPaxContactConfig[indexStr].paxConfigAD);
	} else if (paxCatType == 'IN') {
		if(paxAutocheckinConfigAD.title != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.title.autoCheckinMandatory = paxAutocheckinConfigAD.title;
		}
		if(paxAutocheckinConfigAD.firstName != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.firstName.autoCheckinMandatory = paxAutocheckinConfigAD.firstName;
		}
		if(paxAutocheckinConfigAD.lastName != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.lastName.autoCheckinMandatory = paxAutocheckinConfigAD.lastName;
		}
		if(paxAutocheckinConfigAD.nationality != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.nationality.autoCheckinMandatory = paxAutocheckinConfigAD.nationality;
		}
		if(paxAutocheckinConfigAD.dob != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.dob.autoCheckinMandatory = paxAutocheckinConfigAD.dob;
		}
		if(paxAutocheckinConfigAD.passportNo != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportNo.autoCheckinMandatory = paxAutocheckinConfigAD.passportNo;
		}
		if(paxAutocheckinConfigAD.passportExpiry != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportExpiry.autoCheckinMandatory = paxAutocheckinConfigAD.passportExpiry;
		}
		if(paxAutocheckinConfigAD.passportIssuedCntry != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.passportIssuedCntry.autoCheckinMandatory = paxAutocheckinConfigAD.passportIssuedCntry;
		}
		if(paxAutocheckinConfigAD.groupId != null){
			top.globalPaxContactConfig[indexStr].paxConfigAD.groupId.autoCheckinMandatory = paxAutocheckinConfigAD.groupId;
		}
	} 
}

UI_tabPassenger.validateProfileEmail = function(field){
	if((field.value).toUpperCase() != profEmailAddress.toUpperCase()){
		$("#chkUpdateCustProf").attr("checked", false);
		$("#chkUpdateCustProf").disable();
	} else {
		$("#chkUpdateCustProf").enable();
	}
}

UI_tabPassenger.showHideContactInfo = function(){
	
	//Reservation contact related configurations
	if(contactConfig.title1.xbeVisibility){
		$("#tblTitle").show();
		if(!contactConfig.title1.mandatory){
			$("#spnTitleMand").hide();
		} else {
			$("#spnTitleMand").show();
		}
	} else {
		$("#tblTitle").hide();
	}
	
	if(contactConfig.firstName1.xbeVisibility){
		$("#tblFirstName").show();
		if(!contactConfig.firstName1.mandatory){
			$("#spnFNameMand").hide();
		} else {
			$("#spnFNameMand").show();
		}
	} else {
		$("#tblFirstName").hide();
	}
	
	if(contactConfig.lastName1.xbeVisibility){
		$("#tblLastName").show();
		if(!contactConfig.lastName1.mandatory){
			$("#spnLNameMand").hide();
		} else {
			$("#spnLNameMand").show();
		}
	} else {
		$("#tblLastName").hide();
	}
	
	if(contactConfig.address1.xbeVisibility){
		$("#tblAddr1").show();
		$("#tblAddr2").show();
		if(!contactConfig.address1.mandatory){
			$("#spnAddr1Mand").hide();
			$("#spnAddrMand").hide();
		} else {
			$("#spnAddr1Mand").show();
			$("#spnAddrMand").show();
		}
	} else {
		$("#tblAddr1").hide();;
		$("#tblAddr2").hide();
	}
	
	if(contactConfig.nationality1.xbeVisibility){
		$("#tblNationality").show();
		if(!contactConfig.nationality1.mandatory){
			$("#spnNationalityMand").hide();
		} else {
			$("#spnNationalityMand").show();
		}
	} else {
		$("#tblNationality").hide();
	}
	
	if(contactConfig.city1.xbeVisibility){
		$("#tblCity").show();
		if(!contactConfig.city1.mandatory){
			$("#spnCityMand").hide();
		} else {
			$("#spnCityMand").show();
		}
	} else {
		$("#tblCity").hide();
	}
	
	if(contactConfig.country1.xbeVisibility){
		$("#tblCountry").show();
		if(!contactConfig.country1.mandatory){
			$("#spnCountryMand").hide();
		} else {
			$("#spnCountryMand").show();
		}
	} else {
		$("#tblCountry").hide();
	}
	
	if(contactConfig.zipCode1.xbeVisibility){
		$("#tblZipCode").show();
		if(!contactConfig.zipCode1.mandatory){
			$("#spnZipCodeMand").hide();
		} else {
			$("#spnZipCodeMand").show();
		}
	} else {
		$("#tblZipCode").hide();
	}
	
	if(contactConfig.mobileNo1.xbeVisibility){
		$("#tblMobileNo").show();
		if(!contactConfig.mobileNo1.mandatory){
			$("#spnMobileNoMand").hide();
		} else {
			$("#spnMobileNoMand").show();
			// add maximum text field lengths for mobile Number
			if(contactConfig.mobileNo1.maxLength && top.arrParams[44]){
				var maxLength = contactConfig.mobileNo1.maxLength.split('-');
				var areaCodeMaxLen = maxLength[0];
				var mobileMaxLen = maxLength[1];
				
				$("#txtMobArea").attr('maxlength', areaCodeMaxLen);
				$("#txtMobiNo").attr('maxlength', mobileMaxLen);
			}
		}
	} else {
		$("#tblMobileNo").hide();
	}
	
	if(contactConfig.phoneNo1.xbeVisibility){
		$("#tblPhoneNo").show();
		if(!contactConfig.phoneNo1.mandatory){
			$("#spnPhoneNoMand").hide();
		} else {
			$("#spnPhoneNoMand").show();
		}
	} else {
		$("#tblPhoneNo").hide();
	}
	
	if(contactConfig.fax1.xbeVisibility){
		$("#tblFax").show();
		if(!contactConfig.fax1.mandatory){
			$("#spnFaxMand").hide();
		} else {
			$("#spnFaxMand").show();
		}
	} else {
		$("#tblFax").hide();
	}
	
	if(contactConfig.email1.xbeVisibility){
		$("#tblEmail").show();
		if(!contactConfig.email1.mandatory){
			$("#spnEmailMand").hide();
		} else {
			$("#spnEmailMand").show();
		}
	} else {
		$("#tblEmail").hide();
	}
	
	if(contactConfig.preferredLang1.xbeVisibility){
		$("#tblPreferredLang").show();
		if(!contactConfig.preferredLang1.mandatory){
			$("#spnPreferredLangMand").hide();
		} else {
			$("#spnPreferredLangMand").show();
		}
	} else {
		$("#tblPreferredLang").hide();
	}
	
	if(contactConfig.userNote1.xbeVisibility){
		$("#tblUserNote").show();
		if(!contactConfig.userNote1.mandatory){
			$("#spnUserNotesMand").hide();
		} else {
			$("#spnUserNotesMand").show();
		}
	} else {
		$("#tblUserNote").hide();
	}
	
	if(contactConfig.taxRegNo1.xbeVisibility){
		$("#tblTaxRegNo").show();
		if(!contactConfig.taxRegNo1.mandatory){
			$("#spnTaxRegNoMand").hide();
		} else {
			$("#spnTaxRegNoMand").show();
		}
	} else {
		$("#tblTaxRegNo").hide();
	}
	
	if(contactConfig.state1.xbeVisibility){
		$("#tblState").show();
		if(!contactConfig.state1.mandatory){
			$("#spnStateMand").hide();
		} else {
			$("#spnStateMand").show();
		}
	} else {
		$("#tblState").hide();
	}
	
	//Emergency Contact related configurations
	if(contactConfig.firstName2.xbeVisibility ||
			contactConfig.lastName2.xbeVisibility ||
			contactConfig.phoneNo2.xbeVisibility){
		$("#trEmgnContactInfo").show();
		
		if(contactConfig.title2.xbeVisibility){
			$("#tblEmgnTitle").show();
			if(!contactConfig.title2.mandatory){
				$("#spnEmgnTitleMand").hide();
			} else {
				$("#spnEmgnTitleMand").show();
			}
		} else {
			$("#tblEmgnTitle").hide();
		}
		
		if(contactConfig.firstName2.xbeVisibility){
			$("#tblEmgnFirstName").show();
			if(!contactConfig.firstName2.mandatory){
				$("#spnEmgnFNameMand").hide();
			} else {
				$("#spnEmgnFNameMand").show();
			}
		} else {
			$("#tblEmgnFirstName").hide();
		}
		
		if(contactConfig.lastName2.xbeVisibility){
			$("#tblEmgnLastName").show();
			if(!contactConfig.lastName2.mandatory){
				$("#spnEmgnLNameMand").hide();
			} else {
				$("#spnEmgnLNameMand").show();
			}
		} else {
			$("#tblEmgnLastName").hide();
		}
		
		if(contactConfig.phoneNo2.xbeVisibility){
			$("#tblEmgnPhoneNo").show();
			if(!contactConfig.phoneNo2.mandatory){
				$("#spnEmgnPhoneNoMand").hide();
			} else {
				$("#spnEmgnPhoneNoMand").show();
			}
		} else {
			$("#tblEmgnPhoneNo").hide();
		}
		
		if(contactConfig.email2.xbeVisibility){
			$("#tblEmgnEmail").show();
			if(!contactConfig.email2.mandatory){
				$("#spnEmgnEmailMand").hide();
			} else {
				$("#spnEmgnEmailMand").show();
			}
		} else {
			$("#tblEmgnEmail").hide();
		}		
	} else {
		$("#trEmgnContactInfo").hide();
	}
	
}

UI_tabPassenger.passengerInfoChanged = function() {
	UI_makeReservation.tabStatus.tab2 = false;
	UI_makeReservation.tabStatus.tab3 = false;
}

UI_tabPassenger.countryOnChange = function(setStates) {
	UI_tabPassenger.passengerInfoChanged();
	UI_commonSystem.pageOnChange();
	for ( var cl = 0; cl < phoneObj.length; cl++) {
		counObj = phoneObj[cl];
		if (counObj.countryCode == $("#selCountry").val()) {
			$("#txtMobCntry").val(counObj.countryPhoneCode);
			//$("#txtPhoneCntry").val(counObj.countryPhoneCode);
			$("#txtFaxCntry").val(counObj.countryPhoneCode);
			
			/*
			 * The following piece of Code is Specific for W5.
			 * It prepends '9' for the area code of the Mobile Number.
			 * 
			 * */
			if(counObj.countryCode == 'IR' && top.arrParams[44]){ // checking the app parameter value
				var mobAreaCode = $("#txtMobArea").val();			
				if(mobAreaCode==undefined || mobAreaCode==null 
						|| $.trim(mobAreaCode)=="" || !(/^9/i.test(mobAreaCode))){
					$("#txtMobArea").val('9');	
				}
								
				$("#txtMobArea").on("keydown",function(){		
					var len = $(this).val().length;
					$(this)[0].setSelectionRange( len, len ); 
					
					if(len == 0 && $("#selCountry").val() == 'IR'){
						$(this).val('9');
					}
				});
				
				$("#txtMobArea").on("keyup",function(){		
					var len = $(this).val().length;					
					if(len == 0 && $("#selCountry").val() == 'IR'){
						$(this).val('9');
					}
				});
			}
			
			break;
		}
		
		
	}

	var showAddressPreviousState = UI_tabPassenger.showAddress;

	var serviceTaxEnabledCountries = DATA_ResPro.initialParams.taxRegistrationNumberEnabledCountries.split(',');
	for ( var cl = 0; cl < serviceTaxEnabledCountries.length; cl++) {
		if (serviceTaxEnabledCountries[cl] == $("#selCountry").val()) {
			$("#tblTaxRegNo").show();
			UI_tabPassenger.showTaxRegNo = true;

			$("#tblState").show();
			$("#spnStateMand").show();
			$("#tblAddr1").show();
			$("#tblAddr2").show();
			$("#spnAddr1Mand").show();
			UI_tabPassenger.showState = true;
			UI_tabPassenger.showAddress = true;

			if (setStates) {
				$('#selState').find('option').remove().end();
				if (top.countryStateArry[counObj.countryCode]) {
					$("#selState").append("<option value=''></option>");
					$("#selState").append(top.countryStateArry[counObj.countryCode]);
				}
			}
			break;
		}else{
			$("#tblTaxRegNo").hide();
			$("#tblState").hide();
			$("#spnStateMand").hide();
			$("#spnTaxRegNo").hide();
			$('#selState').find('option').remove().end();
			$("#txtTaxRegNo").val(null);
			if (contactConfig.address1.xbeVisibility) {
				$("#tblAddr1").show();
				$("#tblAddr2").show();
				if (contactConfig.address1.mandatory) {
					$("#spnAddr1Mand").show();
				} else {
					$("#spnAddr1Mand").hide();
				}
			} else {
				$("#tblAddr1").hide();
				$("#tblAddr2").hide();
			}
			UI_tabPassenger.showTaxRegNo = false;
			UI_tabPassenger.showState = false;
			UI_tabPassenger.showAddress = false;
		}
	}
	UI_tabPassenger.adjustSpacesBetweenFields(!showAddressPreviousState && UI_tabPassenger.showAddress);
}

UI_tabPassenger.showHideTaxRegNoDetails = function () {

	$("#tblTaxRegNo").hide();
	$("#tblState").hide();
	$("#spnStateMand").hide();
	$("#spnTaxRegNo").hide();
	$('#selState').find('option').remove().end();
	$("#txtTaxRegNo").val(null);
	UI_tabPassenger.showTaxRegNo = false;
	UI_tabPassenger.showState = false;

	if ($.trim($("#selCountry").val())) {
		UI_tabPassenger.countryOnChange(true);
	}

}

UI_tabPassenger.validateCountryAndAreaCodes = function(){
	
	var choseCountry = $("#selCountry").val();
	for ( var cl = 0; cl < phoneObj.length; cl++) {
		counObj = phoneObj[cl];
		
		if(counObj.countryCode == choseCountry) {	
			
			if(	$("#txtMobCntry").val() == counObj.countryPhoneCode){
				if(choseCountry == 'IR' && top.arrParams[44]){
					/*
					 * The following piece of Code is Specific for W5.
					 * It prepends '9' for the area code of the Mobile Number.
					 * And Show Help Labels
					 * */	
					if($("#txtMobArea").val().toString().charAt(0) != '9'){
						$("#txtMobArea").val('9');
						return false;
					}
				}
				return true;
			}else{
				return false;
			}	
		}
	}
	
}

UI_tabPassenger.updatePaxArrays = function(data){
	jsonPaxAdtObj.paxAdults = data.paxAdults;
	jsonPaxAdtObj.paxInfants = data.paxInfants;
}

/*
 * Passenger Tab Grid Infor DTOs
 */
UI_tabPassenger.updatePassengerTabTO = function(response) {

	jsonFltDetails = response.flightInfo;
	jsonOpenRetInfo = response.openReturnInfo; //FIXME me get reset when next/prv is clicked
}
// TODO REMOVE THIS
UI_tabPassenger.updatePassengerTabTOForPaxDetails = function() {
// if((arrPaxFoidCat==null || arrPaxFoidCat.length ==null ||
// arrPaxFoidCat.length ==0 ||
// arrPaxFoidCat[0].paxFoidCode == "" )&& !showPaxETicket) {
// $("#btnPaxDetails").hide();
// }
}

/*
 * This method will call from the flight search booking button click Depend on
 * the flight search data this informations should fill
 */
UI_tabPassenger.passengerDetailsOnLoadCall = function() {
// if (jsonFareQuoteSummary.onHoldRestricted) {
// $("#btnPHold").disable();
// if ($("#selSalesPoint")) {
// $("#selSalesPoint").disable();
// }
// } else {
// $("#btnPHold").enable();
// if ($("#selSalesPoint")) {
// $("#selSalesPoint").enable();
// }
// }
	// on hold privilege
	if (DATA_ResPro.initialParams.onHold) {
		$("#btnPHold").show();
		$('#btnPHold').attr('title','');
		if ($("#selSalesPoint")) {
			$("#selSalesPoint").show();
		}
		if (jsonFareQuoteSummary.onHoldRestricted && (
				UI_tabSearchFlights.getSelectedSystem() == 'INT' || !DATA_ResPro.initialParams.overrrideOnHoldRestricted) ) {
			$("#btnPHold").disable();
			$('#btnPHold').attr('title', 'Not allowed for user with privilege.');
			if ($("#selSalesPoint")) {
				$("#selSalesPoint").disable();
			}
		} else if (!UI_tabPassenger.allowBufferTimeOnHold) {
			$("#btnPHold").disable();
			$('#btnPHold').attr('title', 'Not allowed for user with privilege.');				
		} else {
			$("#btnPHold").enable();
			if ($("#selSalesPoint")) {
				$("#selSalesPoint").enable();
			}
		}
	} else {
		$("#btnPHold").hide();
		$("#btnPHold").disable();
		if ($("#selSalesPoint")) {
			$("#selSalesPoint").hide();
		}
	}
	if(UI_tabSearchFlights.objResSrchParams.bookingType == "WAITLISTING" && UI_tabSearchFlights.waitListedSegmentAvailable && !UI_tabSearchFlights.ignoreWaitListing && DATA_ResPro.initialParams.allowWaitListBooking){
		$("#btnContinue").hide();	
		$("#btnPHold").hide();
		$("#btnWaitList").show();
	}
	else{
		$("#btnWaitList").hide();
		$("#btnContinue").show();	
		$("#btnPHold").show();
	}
	if (UI_tabPassenger.isCsPnr) {
		//$("#btnContinue").hide();
		$("#btnTBA").hide();
	}
	if(!(DATA_ResPro.initialParams.transferOwnership || DATA_ResPro.initialParams.transferOwnershipToAnyCarrier)){
		$("#selSalesPoint").hide();
	}
	UI_tabPassenger.generateTravellingWithData();
	UI_tabPassenger.constructGridAdult( {
		id : "#tblPaxAdt"
	});	
	UI_tabPassenger.constructGridInf( {
		id : "#tblPaxInf"
	});
	UI_tabPassenger.buildPaxPriceBreakDown();
	UI_tabPassenger.buildFlightDetails();
	UI_tabPassenger.defaultFocus();
	
	var tmpPaxAdults = $.airutil.dom.cloneObject(jsonPaxAdtObj.paxAdults);
	var paxAdultLen = $.fn.objLen(tmpPaxAdults);
	var seqNo = 1;
	if(paxAdultLen > 0){
		var index = 0;
		do{
			tmpPaxAdults[index].sequenceNo = seqNo++;
			index++;
		} while(--paxAdultLen)
	}
	seqNo = 1;
	if(jsonPaxAdtObj.paxInfants!=null){
		for(var i = 0; i < jsonPaxAdtObj.paxInfants.length; i++){
			jsonPaxAdtObj.paxInfants[i].sequenceNo = seqNo++;
		}
	}
	UI_commonSystem.fillGridData( {
		id : "#tblPaxAdt",
		data : tmpPaxAdults,
		editable : true
	});
	
	UI_commonSystem.fillGridData( {
		id : "#tblPaxInf",
		data : jsonPaxAdtObj.paxInfants,
		editable : true
	});
	if (jsonPaxAdtObj.paxInfants != null) {
		if (jsonPaxAdtObj.paxInfants.length > 0) {
			$("#divInfantPane").show();
		} else {
			$("#divInfantPane").hide();
		}
	} else {
		$("#divInfantPane").hide();
	}
	
	UI_tabPassenger.mapCalendar();
	UI_tabPassenger.applyTabIndexOrder();
	
	UI_tabPassenger.showHideADGrid();
	UI_tabPassenger.adjustPageHeight(tmpPaxAdults, jsonPaxAdtObj.paxInfants);
}

UI_tabPassenger.adjustPageHeight = function(paxAdts, infs){
	var max_Adt = 9,lengthINF = $.fn.objLen(infs), lengthADT = $.fn.objLen(paxAdts);
	if ($("#divInfantPane").css("display") != "none"){
		if (lengthINF > 2){
			$("#gview_tblPaxInf > .ui-jqgrid-bdiv").css('height',60);
		}else{
			$("#gview_tblPaxInf > .ui-jqgrid-bdiv").css("height","100%");
		}
		max_Adt = 4;
	}
	if (lengthADT > max_Adt){
		$("#gview_tblPaxAdt > .ui-jqgrid-bdiv").css('height',20 * (max_Adt + 1));
	}else{
		$("#gview_tblPaxAdt > .ui-jqgrid-bdiv").css("height","100%");
	}
} 


UI_tabPassenger.showHideADGrid = function(){
	var objJPax = jsonPaxAdtObj.paxAdults;
	var paxCount = objJPax.length;
	var i = 0;
	if(paxCount > 0){
		do {
			var paxConfig = ""
			if (objJPax[i].displayAdultType == "AD") {
				paxConfig = paxConfigAD;
			} else if(objJPax[i].displayAdultType == "CH"){
				paxConfig = paxConfigCH;
			}
			
			if(paxConfig.title.xbeVisibility){
				$("#" + (i + 1) + "_displayAdultTitle").show();
			} else {
				$("#" + (i + 1) + "_displayAdultTitle").hide();
			}
			
			if(paxConfig.firstName.xbeVisibility){
				$("#" + (i + 1) + "_displayAdultFirstName").show();
			} else {
				$("#" + (i + 1) + "_displayAdultFirstName").hide();
			}
			
			if(paxConfig.lastName.xbeVisibility){
				$("#" + (i + 1) + "_displayAdultLastName").show();
			} else {
				$("#" + (i + 1) + "_displayAdultLastName").hide();
			}
			
			if(paxConfig.nationality.xbeVisibility){
				$("#" + (i + 1) + "_displayAdultNationality").show();
			} else {
				$("#" + (i + 1) + "_displayAdultNationality").hide();
			}
			
			if(paxConfig.dob.xbeVisibility){
				$("#" + (i + 1) + "_displayAdultDOB").show();
			} else {
				$("#" + (i + 1) + "_displayAdultDOB").hide();
			}			
			
			if(paxConfig.ffid.xbeVisibility){
				$("#" + (i + 1) + "_displayFFID").show();
			} else {
				$("#" + (i + 1) + "_displayFFID").hide();
			}

			if(UI_tabPassenger.isFFID){
				if(paxConfig.ffid.xbeVisibility){
					$("#" + (i + 1) + "_displayFFID").show();
				} else {
					$("#" + (i + 1) + "_displayFFID").hide();
				}
			}
				
		} while(paxCount--)
	}
}

UI_tabPassenger.lastTime = null;
UI_tabPassenger.outputMsg = '';

UI_tabPassenger.recordTime = function(id){
	if(UI_tabPassenger.lastTime == null){
		UI_tabPassenger.lastTime = new Date();
	}else{
		var d = new Date();
		var timeEl = d.getTime() -  UI_tabPassenger.lastTime.getTime();
		UI_tabPassenger.outputMsg += (' [id:'+id+',time:'+timeEl+']'); 
		UI_tabPassenger.lastTime = new Date();
	}
}

/*
 * Default focus of the Passenger Details Tab
 */
UI_tabPassenger.defaultFocus = function() {
	$("#selTitle").focus();
}

/*
 * TODO If you know how to bind the calendar to jqgrid remove this method
 */
UI_tabPassenger.mapCalendar = function() {
	/* Adult */
	var intLen = jsonPaxAdtObj.paxAdults.length;
		
	var curDateArr = top.arrParams[0].split('/');
	//var deptDateArr =  UI_tabSearchFlights.strOutDate.split('/');
	
	var lastArrivalDate = UI_tabPassenger.extractFlightLastSegArrivalDate();
	var formattedLastArrivalDate = UI_tabPassenger.convertDateFormat(lastArrivalDate);
	var lastArrivalDateArr = formattedLastArrivalDate.split('/');

	var firstDepatureDate = UI_tabPassenger.extractFlightFirstSegOutDate();
	var formattedFirstDepatureDate = UI_tabPassenger.convertDateFormat(firstDepatureDate);
	var firstDepatureDateArr = formattedFirstDepatureDate.split('/');

	var firstDepatureDay = firstDepatureDateArr[0];
	var firstDepatureMon = firstDepatureDateArr[1];
	var firstDepatureYear =  firstDepatureDateArr[2];

	var lastArrivalDay = lastArrivalDateArr[0];
	var lastArrivalMon = lastArrivalDateArr[1];
	var lastArrivalYear =  lastArrivalDateArr[2];
	
	var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;
	
	var depDate = UI_tabPassenger.extractFlightFirstSegOutDate();
	var formatedDepDate = UI_tabPassenger.convertDateFormat(depDate);	
	var deptDateArr =  formatedDepDate.split('/');
	
	var curDay = (curDateArr[0] * curDateArr[0] ) / curDateArr[0];
    var curMon = (curDateArr[1] * curDateArr[1] ) / curDateArr[1];
    var curYer = curDateArr[2];
	
	var day = (deptDateArr[0] * deptDateArr[0] ) / deptDateArr[0];
	var mon = (deptDateArr[1] * deptDateArr[1] ) / deptDateArr[1];
	var yer =  deptDateArr[2];
	
	if (intLen > 0) {
		var i = 1;
		$("#tblPaxAdt")
				.find("tr")
				.each(
						function() {
							$(this)
									.find("input")
									.each(
											function() {
												if (this.name == "displayAdultDOB") {													
													var minDate;
													var maxDate;
													var range = '';														
													if(jsonPaxAdtObj.paxAdults[i - 1].displayAdultType == 'CH'){
														//minDate = day + 'd-' + mon + 'm-' + (paxValidation.childAgeCutOverYears-yer) + 'y';							
														//maxDate = (parseInt(day)-1) + 'd-' + mon + 'm-' + (paxValidation.infantAgeCutOverYears-yer) + 'y';
														minDate = new Date((yer-paxValidation.childAgeCutOverYears), lastArrivalMon-1, lastArrivalDay);
														maxDate = new Date((yer-paxValidation.infantAgeCutOverYears), lastArrivalMon-1, lastArrivalDay-1);
														range = '-' + paxValidation.childAgeCutOverYears + ':+' + paxValidation.childAgeCutOverYears ;
													}else{
														minDate = new Date((yer-paxValidation.adultAgeCutOverYears), lastArrivalMon-1, lastArrivalDay);
														maxDate = new Date((yer-paxValidation.childAgeCutOverYears), lastArrivalMon-1, lastArrivalDay-1);
														range = '-' + paxValidation.adultAgeCutOverYears + ':+' + paxValidation.adultAgeCutOverYears;														
													}
													$("#" + this.id)
															.datepicker(
																	{
																		minDate : minDate,
																		maxDate : maxDate,																		
																		dateFormat : UI_commonSystem.strDTFormat,
																		changeMonth : 'true',
																		changeYear : 'true',
																		yearRange: range,
																		showButtonPanel : 'true'
																	});
													
													if(jsonPaxAdtObj.paxAdults[i - 1].displayAdultType == 'CH'){
														$("#"+ i + "_displayAdultTitle").empty();
														$("#"+ i + "_displayAdultTitle").fillDropDown( {
															dataArray : top.arrTitleChild,
															keyIndex : 0,
															valueIndex : 1,
															firstEmpty : true
														});
													}

													$(
															"#"
																	+ i
																	+ "_displayAdultTitle")
															.val(
																	jsonPaxAdtObj.paxAdults[i - 1].displayAdultTitle);
													$(
															"#"
																	+ i
																	+ "_displayAdultNationality")
															.val(
																	jsonPaxAdtObj.paxAdults[i - 1].displayAdultNationality);
													i++;
												}
											});
						});
	}
		
	/* Infant */
	if (jsonPaxAdtObj.paxInfants != null) {
		intLen = jsonPaxAdtObj.paxInfants.length;
		if (intLen > 0) {
			var i = 1;
			$("#tblPaxInf")
					.find("tr")
					.each(
							function() {
								$(this)
										.find("input")
										.each(
												function() {
													if (this.name == "displayInfantDOB") {
														var minDate = new Date((lastArrivalYear-paxValidation.infantAgeCutOverYears), lastArrivalMon-1, lastArrivalDay);
														var maxDate = getMaximumPossibleInfantDOB(curDateArr,deptDateArr,infantAgeLowerBoundInDays);
														var range = '-' + paxValidation.infantAgeCutOverYears + ':+' + paxValidation.infantAgeCutOverYears;
														
														$("#" + this.id)
																.datepicker(
																		{
																			minDate : minDate,
																			maxDate : maxDate,
																			dateFormat : UI_commonSystem.strDTFormat,
																			changeMonth : 'true',
																			changeYear : 'true',
																			yearRange: range,
																			showButtonPanel : 'true'
																		});
														$(
																"#"
																		+ i
																		+ "_displayInfantTitle")
																.val(
																		jsonPaxAdtObj.paxInfants[i - 1].displayInfantTitle);
														$(
																"#"
																		+ i
																		+ "_displayInfantTravellingWith")
																.val(
																		jsonPaxAdtObj.paxInfants[i - 1].displayInfantTravellingWith);
														$(
																"#"
																		+ i
																		+ "_displayInfantNationality")
																.val(
																		jsonPaxAdtObj.paxInfants[i - 1].displayInfantNationality);
														i++;
													}
												});
							});
		}
	}
	
	
	function getMaximumPossibleInfantDOB(curentDateArr, firstDepDateArr, infantAgeLowerDays){
		
		var depDay =  firstDepDateArr[0] ;
		var depMon =  firstDepDateArr[1] ;
		var depYear =  firstDepDateArr[2] ;
		
		var currDay = curentDateArr[0];
		var currMon = curentDateArr[1];
		var currYear = curentDateArr[2];
		
		var maxDate = new Date(depYear,depMon-1,depDay-infantAgeLowerDays-1) ;
		var currentDate = new Date (currYear,currMon-1,currDay);
		 
		 if (maxDate < currentDate){			 
			 return maxDate;
		 } else {
		     return currentDate ;
		 }		
	}
}

/*
 * Validate Passenger Tab Data
 */
UI_tabPassenger.validate = function() {
	UI_commonSystem.initializeMessage();
	/*
	 * If alternativeEmailForXBE has a value set then loop over the jsonPaxObj array and find the Email field.
	 * If email validation is enabled and the email is blank then set the alternativeEmailForXBE. The check for
	 * vaidation required is done becaue there's no need to set the email if it's not required in IBE and XBE.
	 * There's no way to seperately change the mandatory status for Email in XBE and IBE currently. Fell free to
	 * change that check if necessary.
	 */
	if(alternativeEmailForXBE != null && alternativeEmailForXBE != 'null' && trim(alternativeEmailForXBE) != ''){
		$.each(jsonPaxObj,function(index,element){
			if(element.desc=='Email'){
				var currentEmail=$(element.id).val();
				if(currentEmail=='' && element.required==1){
					$(element.id).val(trim(alternativeEmailForXBE));
				}
				return;
			}
		});
	}
	
	if($('#btnPaxDetails').is(":visible") && $('#btnUploadFromCSV')[0]){
		if(!passportDetailsvalidated){
			showERRMessage("Please open Pax Details popup window and validate the passport details.");
			return false;
		}
	}
	
	if($('#btnInternationalFlight').is(":visible") && $('#btnUploadFromCSV')[0]){
		if(!internationalFlightDetailsValidated){
			showERRMessage("Please open International Flight popup window and validate the flight details.");
			return false;
		}
	}
	
	/* Mandatory Validations */
	if (!UI_commonSystem.validateMandatory(jsonPaxObj)) {
		return false;
	}

	/* Invalid Character Validations */
	if (!UI_commonSystem.validateInvalidChar(jsonPaxObj)) {
		return false;
	}

	if (!UI_commonSystem.validateANW( {
		id : "#txtFirstName",
		desc : "First Name"
	})) {
		return false;
	}
	if (!UI_commonSystem.validateANW( {
		id : "#txtLastName",
		desc : "Last Name"
	})) {
		return false;
	}

	/*if ($.trim($("#txtMobiNo").val()) == ""
			&& $.trim($("#txtPhoneNo").val()) == "") {
		showERRMessage(raiseError("XBE-ERR-26"));
		return false;
	}*/
	
	

	if ($.trim($("#txtMobiNo").val()) != "") {

		$("#txtMobCntry").val($.trim($("#txtMobCntry").val()));
		$("#txtMobArea").val($.trim($("#txtMobArea").val()));
		$("#txtMobiNo").val($.trim($("#txtMobiNo").val()));

		if (!UI_commonSystem.numberValidate( {
			id : "#txtMobCntry",
			desc : "Mobile Country Code",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.numberValidate( {
			id : "#txtMobArea",
			desc : "Mobile Area Code",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.numberValidateWithStartingZero( {
			id : "#txtMobiNo",
			desc : "Mobile No",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.validateCountryPhone($("#selCountry").val(),
				phoneObj, $("#txtMobCntry").val(), $("#txtMobArea").val(), $(
						"#txtMobiNo").val(), "MOBILE")) {
			return false
			
		}else if(!UI_tabPassenger.validateCountryAndAreaCodes()){
			showERRMessage(raiseError("XBE-ERR-93", "Country Code"));
			$("#txtMobCntry").focus();
			return false;
		}

	} else {
		if ($.trim($("#selCountry").val()) != "") {
			UI_tabPassenger.countryOnChange(false);
		} else {
			$("#txtMobCntry").val("");
		}
		$("#txtMobArea").val("");
	}

	if ($.trim($("#txtPhoneNo").val()) != "") {

		$("#txtPhoneCntry").val($.trim($("#txtPhoneCntry").val()));
		$("#txtPhoneArea").val($.trim($("#txtPhoneArea").val()));
		$("#txtPhoneNo").val($.trim($("#txtPhoneNo").val()));

		if (!UI_commonSystem.numberValidate( {
			id : "#txtPhoneCntry",
			desc : "Phone Country Code",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.numberValidate( {
			id : "#txtPhoneArea",
			desc : "Phone Area Code",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.numberValidateWithStartingZero( {
			id : "#txtPhoneNo",
			desc : "Phone No",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.validateCountryPhone($("#selCountry").val(),
				phoneObj, $("#txtPhoneCntry").val(), $("#txtPhoneArea").val(),
				$("#txtPhoneNo").val(), "LAND")) {
			return false
		}
	} else {
		if ($.trim($("#selCountry").val()) != "") {
			UI_tabPassenger.countryOnChange(false);
		} else {
			$("#txtPhoneCntry").val("");
		}
		$("#txtPhoneArea").val("");
	}

	if ($.trim($("#txtFaxNo").val()) != "") {

		$("#txtFaxCntry").val($.trim($("#txtFaxCntry").val()));
		$("#txtFaxArea").val($.trim($("#txtFaxArea").val()));
		$("#txtFaxNo").val($.trim($("#txtFaxNo").val()));

		if (!UI_commonSystem.numberValidate( {
			id : "#txtFaxCntry",
			desc : "Fax Country Code",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.numberValidate( {
			id : "#txtFaxArea",
			desc : "Fax Area Code",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_commonSystem.numberValidate( {
			id : "#txtFaxNo",
			desc : "Fax No",
			minValue : 1
		})) {
			return false;
		}
	} else {
		if ($.trim($("#selCountry").val()) != "") {
			UI_tabPassenger.countryOnChange(false);
		} else {
			$("#txtFaxCntry").val("");
		}
		$("#txtFaxArea").val("");
	}

	if ($.trim($("#txtEmail").val()) != "") {
		if (!checkEmail($.trim($("#txtEmail").val()))) {
			showERRMessage(raiseError("XBE-ERR-04", "Email address"));
			$("#txtEmail").focus();
			return false;
		} 
	}

	if ($.trim($("#txtUserNotes").val()).length > 250) {
		showERRMessage(raiseError("XBE-ERR-18", "250", "User Notes"));
		$("#txtUserNotes").focus();
		return false;
	}
	
   /* validate BookingType*/	
	if ($("#selBookingCategory").val() =="" || $("#selBookingCategory").val() == null){
		showERRMessage(raiseError("XBE-ERR-78", "Booking Category"));
		$("#selBookingCategory").focus();
		return false;
	}
	
	if($.trim($("#txtZipCode").val()) != ""){
		if(!UI_tabPassenger.validateZipCode($.trim($("#txtZipCode").val()))){
			showERRMessage(raiseError("XBE-ERR-04", "Zip Code. Enter XXXXX or XXXXX-XXXX code"));
			$("#txtZipCode").focus();
			return false;
		}
	}
	
	if(UI_tabPassenger.showState && $("#selState").val() == ''){
		showERRMessage(raiseError("XBE-ERR-01", "State"));
		$("#selState").focus();
		return false;
	}
	
	if (UI_tabPassenger.showTaxRegNo && $.trim($("#txtTaxRegNo").val()) != null && $.trim($("#txtTaxRegNo").val()) != '' && $.trim($("#txtTaxRegNo").val()).length != 15) {
		showERRMessage(raiseError("XBE-ERR-16", "15", "Tax Registration Number"));
		$("#txtTaxRegNo").focus();
		return false;
	}
	
	if (UI_tabPassenger.showTaxRegNo && $.trim($("#txtTaxRegNo").val()) != null && $.trim($("#txtTaxRegNo").val()) != '' && $.trim($("#txtTaxRegNo").val()).substring(0, 2) != $("#selState").val()) {
		showERRMessage("Tax Registration Number does not match with the state. Please check..");
		$("#txtTaxRegNo").focus();
		return false;
	}

	if (UI_tabPassenger.showTaxRegNo && $.trim($("#txtTaxRegNo").val()) &&
		!/\d{2}[a-zA-Z]{5}\d{4}[a-zA-Z]{1}[a-zA-Z\d]{1}[a-zA-Z]{1}[a-zA-Z\d]{1}/.test($.trim($("#txtTaxRegNo").val()))) {
		showERRMessage(raiseError("XBE-ERR-04", "Tax Registration Number. Please check.."));
		$("#txtTaxRegNo").focus();
		return false;
	}
	
	if (UI_tabPassenger.showTaxRegNo && $.trim($("#txtTaxRegNo").val()) != null && $.trim($("#txtTaxRegNo").val()) != '' 
			&& $.trim($("#txtAddress").val()) == '' && $.trim($("#txtStreet").val()) == '') {
		showERRMessage(raiseError("XBE-ERR-01", "Street and Address") + " Address registered with GST Authorities should be entered.");
		$("#txtStreet").focus();
		return false;
	}

	if (UI_tabPassenger.showTaxRegNo && $.trim($("#txtTaxRegNo").val()) == ''
		&& $.trim($("#txtAddress").val()) == '' && $.trim($("#txtStreet").val()) == '') {
		showERRMessage(raiseError("XBE-ERR-01", "Street and Address"));
		$("#txtStreet").focus();
		return false;
	}
	
	if (UI_tabPassenger.showTaxRegNo && $.trim($("#txtTaxRegNo").val()) != null && $.trim($("#txtTaxRegNo").val()) != '' 
		&& $.trim($("#txtCity").val()) == '') {
		showERRMessage(raiseError("XBE-ERR-01", "Town/City"));
		$("#txtCity").focus();
	return false;
}
	
	if(!groupFieldValidation(validationGroup, jsonFieldToElementMapping, jsonGroupValidation)){
		return false;
	}
	
	if(contactConfig.firstName2.xbeVisibility ||
			contactConfig.lastName2.xbeVisibility ||
			contactConfig.phoneNo2.xbeVisibility){
		
		if((contactConfig.firstName2.xbeVisibility && contactConfig.firstName2.mandatory) || 
				(!contactConfig.firstName2.mandatory && $("#txtEmgnFirstName").val() != "")){
			if (!UI_commonSystem.validateANW( {
				id : "#txtEmgnFirstName",
				desc : "Emergency Contact First Name"
			})) {
				return false;
			}
		}
		if((contactConfig.lastName2.xbeVisibility && contactConfig.lastName2.mandatory) || 
				(!contactConfig.lastName2.mandatory && $("#txtEmgnLastName").val() != "")){
			if (!UI_commonSystem.validateANW( {
				id : "#txtEmgnLastName",
				desc : "Emergency Contact Last Name"
			})) {
				return false;
			}
		}
		
		if(contactConfig.phoneNo2.xbeVisibility && contactConfig.phoneNo2.mandatory){
			if (!UI_commonSystem.numberValidate( {
				id : "#txtEmgnPhoneCntry",
				desc : "Emergency Contact Phone Country Code",
				minValue : 1
			})) {
				return false;
			}
			if (!UI_commonSystem.numberValidate( {
				id : "#txtEmgnPhoneArea",
				desc : "Emergency Contact Phone Area Code",
				minValue : 1
			})) {
				return false;
			}
			if (!UI_commonSystem.numberValidate( {
				id : "#txtEmgnPhoneNo",
				desc : "Emergency Contact Phone No",
				minValue : 1
			})) {
				return false;
			}
		} else if (contactConfig.phoneNo2.xbeVisibility && !contactConfig.phoneNo2.mandatory){
			if($("#txtEmgnPhoneNo").val() != "") {
				$("#txtEmgnPhoneCntry").val($.trim($("#txtEmgnPhoneCntry").val()));
				$("#txtEmgnPhoneArea").val($.trim($("#txtEmgnPhoneArea").val()));
				$("#txtEmgnPhoneNo").val($.trim($("#txtEmgnPhoneNo").val()));
	
				if (!UI_commonSystem.numberValidate( {
					id : "#txtEmgnPhoneCntry",
					desc : "Emergency Contact Phone Country Code",
					minValue : 1
				})) {
					return false;
				}
				if (!UI_commonSystem.numberValidate( {
					id : "#txtEmgnPhoneArea",
					desc : "Emergency Contact Phone Area Code",
					minValue : 1
				})) {
					return false;
				}
				if (!UI_commonSystem.numberValidate( {
					id : "#txtEmgnPhoneNo",
					desc : "Emergency Contact Phone No",
					minValue : 1
				})) {
					return false;
				}
			} else {
				$("#txtEmgnPhoneCntry").val("");
				$("#txtEmgnPhoneArea").val("");
			}
		} 
		
		if ((contactConfig.email2.xbeVisibility && contactConfig.email2.mandatory) || 
				(!contactConfig.email2.mandatory && $("#txtEmgnEmail").val() != "")) {
			if (!checkEmail($("#txtEmgnEmail").val())) {
				showERRMessage(raiseError("XBE-ERR-04", "Emergency Contact Email Address"));
				$("#txtEmgnEmail").focus();
				return false;
			} 
		}		
		
	}

	// Adult Validations
	var objJPax = jsonPaxAdtObj.paxAdults;
	var intLen = objJPax.length;
	var i = 0;
	var intAge = null;
	var intLowerAge = null;
	var lstSegDate = UI_tabPassenger.extractFlightLastSegOutDate();
	var frstSegDate = UI_tabPassenger.extractFlightFirstSegOutDate();
	var lastSegArrDate = UI_tabPassenger.extractFlightLastSegArrivalDate();
	var flightLastSegInDate = UI_tabPassenger.convertDateFormat(lastSegArrDate);
	var flightLastSegOutDate = UI_tabPassenger.convertDateFormat(lstSegDate);
	lastSegmentDepartureDate = UI_tabPassenger.convertDateFormat(lstSegDate);
	var flightFirstSegOutDate = UI_tabPassenger.convertDateFormat(frstSegDate);
	var strPaxDesc = "";
	var paxConfig = "";
    UI_tabPassenger.pendingFFIDPopupDisplayed = false;
	
	var requireDOB = function(index, fn, ln){ 
		if(fn == UI_tabPassenger.xbeTBA && ln == UI_tabPassenger.xbeTBA){
			return false;
		} else {
			return true;
		}		
	};
		
	var requireNationality = function(index, fn, ln){ 
		if(fn == UI_tabPassenger.xbeTBA && ln == UI_tabPassenger.xbeTBA){
			return false;
		}
		return true;
	};

	var requireTitle = function(index, fn, ln){
		if(index == 0) return true;
		if(fn == UI_tabPassenger.xbeTBA && ln == UI_tabPassenger.xbeTBA){
			return false;
		}
		return true;
	};
	
	var hasduplicates = function(length) {
	    var i =length, j, val;

	    while (i) {
	        val = $("#"+i+"_displayFFID").val();
	        j = i--;
	        if(val.length > 0 ){
		        while (j-- && j > 0) {
		        	val_int = $("#"+j+"_displayFFID").val();
		            if (val_int.toLowerCase() === val.toLowerCase()) {
		                return true;
		            }
		        }
		    }
	    }
	    return false;
	}
		
	if(hasduplicates(intLen)){
		showERRMessage("Air Rewards ID cannot be same");
		return false;
	}
	
	
	if (intLen > 0) {
		do {
			if (objJPax[i].displayAdultType == "AD") {
				intAge = paxValidation.adultAgeCutOverYears;
				intLowerAge = paxValidation.childAgeCutOverYears;
				strPaxDesc = "Adult";
				paxConfig = paxConfigAD;
			} else {
				intAge = paxValidation.childAgeCutOverYears;
				intLowerAge = paxValidation.infantAgeCutOverYears;
				strPaxDesc = "Child";
				paxConfig = paxConfigCH;
			}

			if(paxConfig.title.xbeVisibility && paxConfig.title.xbeMandatory
					&& requireTitle(i, $("#" + (i + 1) + "_displayAdultFirstName").val(), $("#" + (i + 1) + "_displayAdultLastName").val())){
				if (!UI_commonSystem.controlValidate( {
					id : "#" + (i + 1) + "_displayAdultTitle",
					desc : "Title",
					required :  requireTitle(i, $("#" + (i + 1) + "_displayAdultFirstName").val(), $("#" + (i + 1) + "_displayAdultLastName").val() )
				})) {
					return false;
				}
			}			
			
			if(paxConfig.firstName.xbeVisibility){
				if (!UI_commonSystem.controlValidateName( {
					id : "#" + (i + 1) + "_displayAdultFirstName",
					desc : "First Name",
					required : paxConfig.firstName.xbeMandatory
				})) {
					return false;
				}
				
			}			
			
			if(!$("#" + (i + 1) + "_displayAdultFirstName").isFieldEmpty()){
				if (!UI_commonSystem.validateANW( {
					id : "#" + (i + 1) + "_displayAdultFirstName",
					desc : "First Name"
				})) {
					return false;
				}
				
				if (!UI_commonSystem.validateANW( {
					id : "#" + (i + 1) + "_displayAdultFirstName",
					desc : "First Name"
				})) {
					return false;
				}
			}
			
			if(paxConfig.lastName.xbeVisibility){
				if (!UI_commonSystem.controlValidateName( {
					id : "#" + (i + 1) + "_displayAdultLastName",
					desc : "Last Name",
					required : paxConfig.lastName.xbeMandatory
				})) {
					return false;
				}				
			}
			
			
			
			if(!$("#" + (i + 1) + "_displayAdultLastName").isFieldEmpty()){
				if (!UI_commonSystem.validateANW( {
					id : "#" + (i + 1) + "_displayAdultLastName",
					desc : "Last Name"
				})) {
					return false;
				}
			}
			
			if(paxConfig.nationality.xbeVisibility && paxConfig.nationality.xbeMandatory
					&& requireTitle(i, $("#" + (i + 1) + "_displayAdultFirstName").val(), $("#" + (i + 1) + "_displayAdultLastName").val())){
				if (!UI_commonSystem.controlValidate( {
					id : "#" + (i + 1) + "_displayAdultNationality",
					desc : "Nationality",
					required : requireNationality(i, $("#" + (i + 1) + "_displayAdultFirstName").val(), $("#" + (i + 1) + "_displayAdultLastName").val() )
				})) {
					return false;
				}
			}
			
			if( paxConfig.dob.xbeVisibility && paxConfig.dob.xbeMandatory ){
				if(!UI_commonSystem.controlValidate ({
					id: "#" + (i + 1) + "_displayAdultDOB",
					desc : "Date Of Birth",
					required : requireDOB(i, $("#" + (i + 1) + "_displayAdultFirstName").val(), $("#" + (i + 1) + "_displayAdultLastName").val() )
				})) {
					return false;
				}
			}
			
			if (!$("#" + (i + 1) + "_displayAdultDOB").isFieldEmpty()) {
				if (!CheckDates($("#" + (i + 1) + "_displayAdultDOB").val(), UI_tabSearchFlights.strOutDate)){					
					showERRMessage(raiseError("XBE-ERR-63", "Date of birth"));
					$("#" + (i + 1) + "_displayAdultDOB").focus();
					return false;
				}

				if (objJPax[i].displayAdultType == "AD") {
					if (!ageCompare($("#" + (i + 1) + "_displayAdultDOB").val(),
							flightLastSegOutDate, intAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + (i + 1) + "_displayAdultDOB").focus();
						return false;
					}
				} else {
					if (!ageCompare($("#" + (i + 1) + "_displayAdultDOB").val(),
							flightFirstSegOutDate, intAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + (i + 1) + "_displayAdultDOB").focus();
						return false;
					}
				}
				
				if (objJPax[i].displayAdultType == "AD") {
					if (ageCompare($("#" + (i + 1) + "_displayAdultDOB").val(),
							flightLastSegOutDate, intLowerAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + (i + 1) + "_displayAdultDOB").focus();
						return false;
					}
				} else {
					if (ageCompare($("#" + (i + 1) + "_displayAdultDOB").val(),
							flightLastSegInDate, intLowerAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + (i + 1) + "_displayAdultDOB").focus();
						return false;
					}
				}
				if (objJPax[i].displayAdultType == "AD") {
					if (ageCompare($("#" + (i + 1) + "_displayAdultDOB").val(),
							UI_tabSearchFlights.strOutDate, intLowerAge)) {
						showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,
								strPaxDesc));
						$("#" + (i + 1) + "_displayAdultDOB").focus();
						return false;
					}
				}
			}
			
			if(UI_tabPassenger.isFFID){			
				if(UI_tabPassenger.isFFID){
					if(paxConfig.ffid.xbeVisibility){
						if (!UI_commonSystem.controlValidateName( {
							id : "#" + (i + 1) + "_displayFFID",
							desc : "Air Rewards ID",
							required : paxConfig.ffid.xbeMandatory
						})) {
							return false;
						}				
					}
				}
				
				var ffidVal = $.trim($("#" + (i + 1) + "_displayFFID").val());	
				
				if(ffidVal != "" && ffidVal != null){
					var ffidAvail = UI_tabPassenger.ffidAvailability(i + 1);				
					if(!ffidAvail){
						return false;
					}
				}
			}
			
			
					
			if(paxConfig.passportNo.xbeVisibility && paxConfig.passportNo.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber == null || 
						jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passport Number"));				
					return false;
				}				
			}
			
			
			
			if(paxConfig.passportExpiry.xbeVisibility && paxConfig.passportExpiry.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry == null || 
						jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passport Expiry Date"));				
					return false;
				}				
				
				if(jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry != null && jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry != ""){
					if(!compareDate(jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDExpiry , flightLastSegInDate)){
						showERRMessage("TAIR-91156: Pax Passport is being expire by selected travel date");
						return false;
					}
				}
			}
			
			if(paxConfig.passportIssuedCntry.xbeVisibility && paxConfig.passportIssuedCntry.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDPlace == null || 
						jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDPlace == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passport Place of Issue"));				
					return false;
				}				
			}
			if(paxConfig.passportNo.xbeVisibility && paxConfig.passportNo.xbeMandatory){
				var firstPxFOIDNumber = jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber;
				for(var j =i+1; j < jsonPaxAdtObj.paxAdults.length; j++){
					if((jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber != null && 
							jsonPaxAdtObj.paxAdults[i].displayPnrPaxCatFOIDNumber != "") &&
							!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
						if(jsonPaxAdtObj.paxAdults[j].displayAdultType == "AD"){
							if(firstPxFOIDNumber == jsonPaxAdtObj.paxAdults[j].displayPnrPaxCatFOIDNumber){
								showERRMessage(raiseError("XBE-ERR-68","Passport"));				
								return false;
							}
						}
					}
				}
			}
			if (!DATA_ResPro.initialParams.allowSkipNic) {
				if(paxConfig.nationalIDNo.xbeVisibility && top.isRequestNICForReservationsHavingDomesticSegments == "true"){
					for(var j = 0; j < jsonPaxAdtObj.paxAdults.length; j++){
						if(!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
										$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){		
							if(UI_tabPassenger.isDomesticFlightExist() == true &&
									(jsonPaxAdtObj.paxAdults[i].displayNationalIDNo == "" || jsonPaxAdtObj.paxAdults[i].displayNationalIDNo == null)){
								showERRMessage(raiseError("XBE-ERR-01", "National ID No"));
								return false;
							}
						}
					}
				} else if(paxConfig.nationalIDNo.xbeVisibility && paxConfig.nationalIDNo.xbeMandatory){
					if(!($("#" + i + "_displayAdultFirstName").val() == 'T B A' &&
							$("#" + i + "_displayAdultLastName").val() == 'T B A')){
						if(jsonPaxAdtObj.paxAdults[i].displayNationalIDNo == "" || jsonPaxAdtObj.paxAdults[i].displayNationalIDNo == null){
							showERRMessage(raiseError("XBE-ERR-01", "National ID No"));
							return false;
						}
					}
				}
			}
			
			if(paxConfig.placeOfBirth.xbeVisibility && paxConfig.placeOfBirth.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayPnrPaxPlaceOfBirth == null || 
						jsonPaxAdtObj.paxAdults[i].displayPnrPaxPlaceOfBirth == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Passenger Place of Birth"));				
					return false;
				}				
			}
			if(paxConfig.travelDocumentType.xbeVisibility && paxConfig.travelDocumentType.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayTravelDocType == null || 
						jsonPaxAdtObj.paxAdults[i].displayTravelDocType == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Travel Document Type"));				
					return false;
				}				
			}
			if(paxConfig.visaDocNumber.xbeVisibility && paxConfig.visaDocNumber.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayVisaDocNumber == null || 
						jsonPaxAdtObj.paxAdults[i].displayVisaDocNumber == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Document Number"));				
					return false;
				}				
			}
			if(paxConfig.visaDocPlaceOfIssue.xbeVisibility && paxConfig.visaDocPlaceOfIssue.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayVisaDocPlaceOfIssue == null || 
						jsonPaxAdtObj.paxAdults[i].displayVisaDocPlaceOfIssue == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Document Place Of Issue"));				
					return false;
				}				
			}
			if(paxConfig.visaDocIssueDate.xbeVisibility && paxConfig.visaDocIssueDate.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayVisaDocIssueDate == null || 
						jsonPaxAdtObj.paxAdults[i].displayVisaDocIssueDate == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Document Issue Date"));				
					return false;
				}				
			}
			if(paxConfig.visaApplicableCountry.xbeVisibility && paxConfig.visaApplicableCountry.xbeMandatory){
				if((jsonPaxAdtObj.paxAdults[i].displayVisaApplicableCountry == null || 
						jsonPaxAdtObj.paxAdults[i].displayVisaApplicableCountry == "") &&
						!($("#" + (i + 1) + "_displayAdultFirstName").val() == 'T B A' &&
								$("#" + (i + 1) + "_displayAdultLastName").val() == 'T B A')){
					showERRMessage(raiseError("XBE-ERR-01","Visa Applicable Country"));				
					return false;
				}				
			}
			i++;
		} while (--intLen);
	}
	UI_tabPassenger.setAdultPassenger();

	// Infant Validation
	objJPax = jsonPaxAdtObj.paxInfants;
	if (objJPax != null) {
		var intInfants = objJPax.length
		var intWLen = 0;
		var intTW = 0;
		var infAgeMan = (top.isInfAgeMandXbe == "true");
		intLen = intInfants;
		i = 0;
		var x = 0
		if (intLen > 0) {
			do {				
				
				if(paxConfigIN.title.xbeVisibility){
					if (!UI_commonSystem.controlValidateName( {
						id : "#" + (i + 1) + "_displayInfantTitle",
						desc : "Title",
						required : paxConfigIN.title.xbeMandatory
					})) {
						return false;
					}
				}
				
				
				if(!$("#" + (i + 1) + "_displayInfantTitle").isFieldEmpty()){
					if (!UI_commonSystem.validateANW( {
						id : "#" + (i + 1) + "_displayInfantTitle",
						desc : "Infant" + UI_commonSystem.strAPS + "s Title"
					})) {
						return false;
					}
				}
				
				if(paxConfigIN.firstName.xbeVisibility){
					if (!UI_commonSystem.controlValidateName( {
						id : "#" + (i + 1) + "_displayInfantFirstName",
						desc : "First Name",
						required : paxConfigIN.firstName.xbeMandatory
					})) {
						return false;
					}
				}
				
				
				if(!$("#" + (i + 1) + "_displayInfantFirstName").isFieldEmpty()){
					if (!UI_commonSystem.validateANW( {
						id : "#" + (i + 1) + "_displayInfantFirstName",
						desc : "Infant" + UI_commonSystem.strAPS + "s First Name"
					})) {
						return false;
					}
				}
				
				if(paxConfigIN.lastName.xbeVisibility){
					if (!UI_commonSystem.controlValidateName( {
						id : "#" + (i + 1) + "_displayInfantLastName",
						desc : "Last Name",
						required : paxConfigIN.lastName.xbeMandatory
					})) {
						return false;
					}
				}
				
				if(!$("#" + (i + 1) + "_displayInfantLastName").isFieldEmpty()){
					if (!UI_commonSystem.validateANW( {
						id : "#" + (i + 1) + "_displayInfantLastName",
						desc : "Infant" + UI_commonSystem.strAPS + "s Last Name"
					})) {
						return false;
					}
				}
				
				if(paxConfigIN.nationality.xbeVisibility && paxConfigIN.nationality.xbeMandatory){
					if (!UI_commonSystem.controlValidate( {
						id : "#" + (i + 1) + "_displayInfantNationality",
						desc : "Nationality",
						required : true
					})) {
						return false;
					}
				}
				
				if(paxConfigIN.dob.xbeVisibility && paxConfigIN.dob.xbeMandatory){
					if (!UI_commonSystem.controlValidate( {
						id : "#" + (i + 1) + "_displayInfantDOB",
						desc : "Date of Birth",
						required : true
					})) {
						return false;
					}
				}
				
				if (!$("#" + (i + 1) + "_displayInfantDOB").isFieldEmpty()) {
					if (!CheckDates($("#" + (i + 1) + "_displayInfantDOB").val(), top.arrParams[0])) {							
						showERRMessage(raiseError("XBE-ERR-63", "Date of birth"));
						$("#" + (i + 1) + "_displayInfantDOB").focus();
						return false;
					}

					if (!ageCompare($("#" + (i + 1) + "_displayInfantDOB")
							.val(), flightFirstSegOutDate,
							top.strDefInfantAge)) {
						showERRMessage(raiseError("XBE-ERR-64", "Infant",
								"Infant"));
						$("#" + (i + 1) + "_displayInfantDOB").focus();
						return false;
					}
					
					if (!ageCompare($("#" + (i + 1) + "_displayInfantDOB")
							.val(), flightLastSegOutDate,
							top.strDefInfantAge)) {
						showERRMessage(raiseError("XBE-ERR-64", "Infant",
								"Infant"));
						$("#" + (i + 1) + "_displayInfantDOB").focus();
						return false;
					}
					
					var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;					
					var flightLastSegArrivalDate = UI_tabPassenger.extractFlightLastSegArrivalDate();
					var formatedFlightLastSegArrivalDate = UI_tabPassenger.convertDateFormat(flightLastSegArrivalDate);
					//Infant age should be greater than 14 days					
					if(!UI_tabPassenger.checkInfantMinAge(($("#" + (i+1) + "_displayInfantDOB").val()),formatedFlightLastSegArrivalDate,infantAgeLowerBoundInDays))  {
							showERRMessage(raiseError("XBE-ERR-70","Date of birth",infantAgeLowerBoundInDays));
							$("#" + (i+1) + "_displayInfantDOB").focus();
							return false;
					}
				}
				
				if(paxConfigIN.travelWith.xbeVisibility && paxConfigIN.travelWith.xbeMandatory){
					if (!UI_commonSystem.controlValidate( {
						id : "#" + (i + 1) + "_displayInfantTravellingWith",
						desc : "With Adult",
						required : true
					})) {
						return false;
					}
				}

				/* Traveling With Validations */
				x = 0
				intWLen = intInfants;
				do {
					if (x != i) {
						if ($("#" + (i + 1) + "_displayInfantTravellingWith")
								.val() == $(
								"#" + (x + 1) + "_displayInfantTravellingWith")
								.val()) {
							showERRMessage(raiseError("XBE-ERR-09"));
							$("#" + (i + 1) + "_displayInfantTravellingWith")
									.focus();
							return false;
						}
					}
					x++;
				} while (--intWLen);

				
				if(paxConfigIN.passportNo.xbeVisibility && paxConfigIN.passportNo.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDNumber == null || 
							jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDNumber == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passport Number"));				
						return false;
					}				
				}
				
				if(paxConfigIN.passportExpiry.xbeVisibility && paxConfigIN.passportExpiry.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDExpiry == null || 
							jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDExpiry == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passport Expiry Date"));				
						return false;
					}				
				}
				
				if(paxConfigIN.passportIssuedCntry.xbeVisibility && paxConfigIN.passportIssuedCntry.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDPlace == null || 
							jsonPaxAdtObj.paxInfants[i].displayPnrPaxCatFOIDPlace == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passport Place of Issue"));				
						return false;
					}				
				}	
				
				if(paxConfig.placeOfBirth.xbeVisibility && paxConfig.placeOfBirth.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayPnrPaxPlaceOfBirth == null || 
							jsonPaxAdtObj.paxInfants[i].displayPnrPaxPlaceOfBirth == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Passenger Place of Birth"));				
						return false;
					}				
				}
				
				if(paxConfig.travelDocumentType.xbeVisibility && paxConfig.travelDocumentType.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayTravelDocType == null || 
							jsonPaxAdtObj.paxInfants[i].displayTravelDocType == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Travel Document Type"));				
						return false;
					}				
				}
				if(paxConfig.visaDocNumber.xbeVisibility && paxConfig.visaDocNumber.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayVisaDocNumber == null || 
							jsonPaxAdtObj.paxInfants[i].displayVisaDocNumber == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Document Number"));				
						return false;
					}				
				}
				if(paxConfig.visaDocPlaceOfIssue.xbeVisibility && paxConfig.visaDocPlaceOfIssue.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayVisaDocPlaceOfIssue == null || 
							jsonPaxAdtObj.paxInfants[i].displayVisaDocPlaceOfIssue == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Document Place Of Issue"));				
						return false;
					}				
				}
				if(paxConfig.visaDocIssueDate.xbeVisibility && paxConfig.visaDocIssueDate.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayVisaDocIssueDate == null || 
							jsonPaxAdtObj.paxInfants[i].displayVisaDocIssueDate == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Document Issue Date"));				
						return false;
					}				
				}
				if(paxConfig.visaApplicableCountry.xbeVisibility && paxConfig.visaApplicableCountry.xbeMandatory){
					if((jsonPaxAdtObj.paxInfants[i].displayVisaApplicableCountry == null || 
							jsonPaxAdtObj.paxInfants[i].displayVisaApplicableCountry == "") &&
							!($("#" + (i + 1) + "_displayInfantFirstName").val() == 'T B A' &&
									$("#" + (i + 1) + "_displayInfantLastName").val() == 'T B A')){
						showERRMessage(raiseError("XBE-ERR-01","Visa Applicable Country"));				
						return false;
					}				
				}
				if(paxConfigIN.title.xbeVisibility && paxConfigIN.title.xbeMandatory
						&& requireTitle(i, $("#" + (i + 1) + "_displayInfantFirstName").val(), $("#" + (i + 1) + "_displayInfantLastName").val())){
					if (!UI_commonSystem.controlValidate( {
						id : "#" + (i + 1) + "_displayInfantTitle",
						desc : "Title",
						required :  requireTitle(i, $("#" + (i + 1) + "_displayInfantFirstName").val(), $("#" + (i + 1) + "_displayInfantLastName").val() )
					})) {
						return false;
					}
				}
				
				i++;
			} while (--intLen);
		}
		UI_tabPassenger.setInfantPassenger();
	}
	
	if ($.trim($("#txtEmail").val()) != "" && (top.validateEmailDomain == true)) {		
		return UI_tabPassenger.checkEmailDomain($("#txtEmail").val());			
	}
	
	
	if(DATA_ResPro.initialParams.duplicateNameCheckEnabled){ // check whether duplicate check is enabled
		if(UI_tabPassenger.isDuplicateExists()){
			if(UI_tabPassenger.isBookingDuplicateExists()){
				if(DATA_ResPro.initialParams.duplicateNameSkipPrivilege && !UI_tabPassenger.isCsPnr){ //duplicate override privilege exists
					if(confirm(raiseError("XBE-ERR-72"))){
						return true;
					} else {
						return false;
					}
				} else {
					showERRMessage(raiseError("XBE-ERR-71"));
					return false;
				}
			} else {
				if(DATA_ResPro.initialParams.duplicateNameSkipPrivilege){ //duplicate override privilege exists
					if(confirm(raiseError("XBE-ERR-80"))){
						return true;
					} else {
						return false;
					}
				} else {
					showERRMessage(raiseError("XBE-ERR-79"));
					return false;
				}
			}
		}
		
	}
	
	if(UI_tabPassenger.isTBAViolated()){
		showERRMessage(raiseError("XBE-ERR-103"));
		return false;
	}
	if ($.trim($("#txtEmail").val()) != "" && (top.validateEmailDomain == true)) {		
		return UI_tabPassenger.checkEmailDomain($("#txtEmail").val());			
	}
	
	return true;
}

UI_tabPassenger.isBookingDuplicateExists = function(){
	var dups = {};
	for(var i=0; i <  jsonPaxAdtObj.paxAdults.length; i++){
		var fname = jsonPaxAdtObj.paxAdults[i].displayAdultFirstName;
		var lname = jsonPaxAdtObj.paxAdults[i].displayAdultLastName;
		if(UI_tabPassenger.xbeTBA != fname && UI_tabPassenger.xbeTBA != lname ){
			var name = fname + ' ' + lname;
			name = name.toUpperCase();
			if(dups[name] == null){
				dups[name] = name;
			} else {
				return true;
			}
		}
	}
	return false;
}

UI_tabPassenger.isDuplicateExists = function(){
	if(UI_tabSearchFlights.isCalenderMinimumFare()){
		var data = UI_MinimumFare.createSearchParams('searchParams');
	}else{
		var data = UI_tabSearchFlights.createSearchParams('searchParams');
	}	
	data['selectedFlightList']=UI_tabPassenger.getFlightRPHList();
	data['paxInfo'] = UI_tabPassenger.createAdultPaxInfo();
	data['system'] = UI_tabSearchFlights.getSelectedSystem();
	UI_commonSystem.showProgress();
	var hasDuplicates = false;
	UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', async:false, processData: false, data:data, url:"duplicateCheck.action",							
		success: function(response) {
			if(response != null && response.success){
				hasDuplicates = response.hasDuplicates;
			}
		}, 
		error : UI_commonSystem.setErrorStatus
	});
	UI_commonSystem.hideProgress();
	return hasDuplicates;
	
}

UI_tabPassenger.isBlacklistedPaxExist = function(){  

		var data = {};
		if (UI_tabSearchFlights.isCalenderMinimumFare()) {
			data = UI_MinimumFare.createSearchParams('searchParams');
		} else {
			data = UI_tabSearchFlights.createSearchParams('searchParams');
		}			
		data['selectedFlightList']=UI_tabPassenger.getFlightRPHList();
		data['paxInfo'] = UI_tabPassenger.createAdultPaxInfo();
		data['system'] = UI_tabSearchFlights.getSelectedSystem();
		data['flow'] = "C";
		UI_commonSystem.showProgress();
		
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', async:false, processData: false, data:data, url:"blacklistPaxExistCheck!checkBlackListPaxIncluding.action",							
			success: function(response) {
				if(response != null && response.msgType!="Error"){
					UI_commonSystem.hideProgress();	
					
					blackListedPaxList = response.blackListedPaxList;
					if(blackListedPaxList != null && blackListedPaxList !="" && blackListedPaxList.length>0){ // uncomment
						
						UI_tabPassenger.showBlacklistAlert(blackListedPaxList[0]);
					}else{                           
						UI_tabPassenger.continueToAnciOnBlacklistCnfirm(UI_tabPassenger.isOnHold ,UI_tabPassenger.isWaitListing);//call for continue
					}									
				}
			}, 
			error : UI_commonSystem.setErrorStatus
		});
		UI_commonSystem.hideProgress();		
}

UI_tabPassenger.showBlacklistAlert = function(blackListedPax){
	msgString = blackListedPax.fName+" "+blackListedPax.lName+", ";
	msgString = msgString +	" is a blacklist passenger ,</br> do you want to continue booking ?";
	$("#alertMsgId").html(msgString);
	$("#divBlklstPaxResAlertPOP").show();
	$("#wrapperPop").show();
}

UI_tabPassenger.clickBlacklistpaxAlertContinue = function(){
	reason =trim($("#reasonToAllowBlacklist").val());
	if(reason == "" || reason==null){
		alert("Reason Can't be empty");
		return false;
	}
	if(reason.length>100){
		alert("Reason length exceeds maximum length");
		return false;
	}
	UI_tabPassenger.reasonForAllowBlPax=reason;
	//for(i=0; i< UI_tabPassenger.blackListedPaxList.length ; i++){
	//	UI_tabPassenger.blackListedPaxList[i].reasonToAllow = reason;
	//}
	
	$("#divBlklstPaxResAlertPOP").hide();
	$("#wrapperPop").hide();
	UI_tabPassenger.continueToAnciOnBlacklistCnfirm(UI_tabPassenger.isOnHold ,UI_tabPassenger.isWaitListing);//call for continue
}

UI_tabPassenger.clickBlacklistpaxAlertCancel = function(){
	$("#divBlklstPaxResAlertPOP").hide();
	$("#wrapperPop").hide();
	
}

UI_tabPassenger.setAdultPassenger = function() {
	var objJPax = jsonPaxAdtObj.paxAdults;
	for ( var jl = 0; jl < objJPax.length; jl++) {
		objJPax[jl].displayAdultTitle = $("#" + (jl + 1) + "_displayAdultTitle")
				.val();
		objJPax[jl].displayAdultFirstName = $.trim($(
				"#" + (jl + 1) + "_displayAdultFirstName").val());
		objJPax[jl].displayAdultLastName = $.trim($(
				"#" + (jl + 1) + "_displayAdultLastName").val());
		objJPax[jl].displayAdultNationality = $(
				"#" + (jl + 1) + "_displayAdultNationality").val();
		objJPax[jl].displayAdultDOB = $("#" + (jl + 1) + "_displayAdultDOB")
				.val();
		if(UI_tabPassenger.isFFID){
			objJPax[jl].displayFFID = $("#" + (jl + 1) + "_displayFFID").val();
		}
		objJPax[jl].displayInfantTravelling = "N";
		objJPax[jl].displayInfantWith = "";
		objJPax[jl].displayPaxCategory = UI_tabSearchFlights.strPaxType;
	}
}

UI_tabPassenger.setInfantPassenger = function() {
	objJPax = jsonPaxAdtObj.paxInfants;
	if (objJPax != null) {
		for ( var jl = 0; jl < objJPax.length; jl++) {
			objJPax[jl].displayInfantFirstName = $.trim($(
					"#" + (jl + 1) + "_displayInfantFirstName").val());
			objJPax[jl].displayInfantLastName = $.trim($(
					"#" + (jl + 1) + "_displayInfantLastName").val());
			objJPax[jl].displayInfantNationality = $(
					"#" + (jl + 1) + "_displayInfantNationality").val();
			objJPax[jl].displayInfantDOB = $(
					"#" + (jl + 1) + "_displayInfantDOB").val();
			objJPax[jl].displayInfantTravellingWith = $(
					"#" + (jl + 1) + "_displayInfantTravellingWith").val();
			objJPax[jl].displayInfantTitle = $(
					"#" + (jl + 1) + "_displayInfantTitle").val();
			objJPax[jl].displayPaxCategory = UI_tabSearchFlights.strPaxType;
			

			// Parents JSON Object
			// Map the traveling with Infant
			if ($("#" + (jl + 1) + "_displayInfantTravellingWith").val() != "") {
				jsonPaxAdtObj.paxAdults[objJPax[jl].displayInfantTravellingWith - 1].displayInfantTravelling = "Y";
				jsonPaxAdtObj.paxAdults[objJPax[jl].displayInfantTravellingWith - 1].displayInfantWith = objJPax[jl].displayInfantTravellingWith;
			}

		}
	}
}

UI_tabPassenger.getAdultDetails = function() {
	var paxAdults = jsonPaxAdtObj.paxAdults;
	for ( var jl = 0; jl < paxAdults.length; jl++) {
		paxAdults[jl].displayAdultTitle = $("#" + (jl + 1) + "_displayAdultTitle")
				.val();
		paxAdults[jl].displayAdultFirstName = $.trim($(
				"#" + (jl + 1) + "_displayAdultFirstName").val());
		paxAdults[jl].displayAdultLastName = $.trim($(
				"#" + (jl + 1) + "_displayAdultLastName").val());
		paxAdults[jl].displayAdultNationality = $(
				"#" + (jl + 1) + "_displayAdultNationality").val();
		paxAdults[jl].displayAdultDOB = $("#" + (jl + 1) + "_displayAdultDOB")
				.val();
		
		if(UI_tabPassenger.isFFID){
			paxAdults[jl].displayFFID = $("#" + (jl + 1) + "_displayFFID").val();
		}
		paxAdults[jl].displayInfantTravelling = "N";
		paxAdults[jl].displayInfantWith = "";
		paxAdults[jl].displayPaxCategory = UI_tabSearchFlights.strPaxType;
	}
	return paxAdults;
}

UI_tabPassenger.getInfantDetails = function() {
	var paxInfants = jsonPaxAdtObj.paxInfants;
	if (paxInfants != null) {
		for ( var jl = 0; jl < paxInfants.length; jl++) {
			paxInfants[jl].displayInfantFirstName = $.trim($(
					"#" + (jl + 1) + "_displayInfantFirstName").val());
			paxInfants[jl].displayInfantLastName = $.trim($(
					"#" + (jl + 1) + "_displayInfantLastName").val());
			paxInfants[jl].displayInfantNationality = $(
					"#" + (jl + 1) + "_displayInfantNationality").val();
			paxInfants[jl].displayInfantDOB = $(
					"#" + (jl + 1) + "_displayInfantDOB").val();
			paxInfants[jl].displayInfantTravellingWith = $(
					"#" + (jl + 1) + "_displayInfantTravellingWith").val();
			paxInfants[jl].displayInfantTitle = $(
					"#" + (jl + 1) + "_displayInfantTitle").val();
			paxInfants[jl].displayNationaIDNo = $(
					"#" + (jl + 1) + "_displayNationaIDNo").val();

			// Parents JSON Object
			// Map the traveling with Infant
			if ($("#" + (jl + 1) + "_displayInfantTravellingWith").val() != "") {
				jsonPaxAdtObj.paxAdults[paxInfants[jl].displayInfantTravellingWith - 1].displayInfantTravelling = "Y";
				jsonPaxAdtObj.paxAdults[paxInfants[jl].displayInfantTravellingWith - 1].displayInfantWith = paxInfants[jl].displayInfantTravellingWith;
			}

		}
	}
	return paxInfants;
}

UI_tabPassenger.paxNamessOnClick = function() {
	UI_tabPassenger.showPaxNames();
}

UI_tabPassenger.showPaxNames = function(){
	var strUrl = 'showNewFile!loadPaxNamesInOtherLanguages.action?mode=CREATE';

	if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(245, 600));
}

UI_tabPassenger.paxDetailsOnClick = function() {
	UI_tabPassenger.showPaxDetails(false);
}

UI_tabPassenger.internationalFlightDetailsOnClick = function() {
	UI_tabPassenger.showInternationalFlightDetails(false);
}

/*
 * Show Pax Details
 */
UI_tabPassenger.showPaxDetails = function(){
	var strUrl = 'showNewFile!loadPaxDetails.action?mode=CREATE&isDomesticFlightExist=' + UI_tabPassenger.isDomesticFlightExist();

	if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(265, 625));
}

/*
 * Show External flight Details
 */
UI_tabPassenger.showInternationalFlightDetails = function(){
	var strUrl = 'showNewFile!loadExternalInternationalFlightDetails.action?mode=CREATE';

	if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(245, 800, true));
}

/*
 * Continue Button Click
 */
UI_tabPassenger.continueOnClick = function() {
	UI_tabPassenger.continueToAnci(false, false);
}
UI_tabPassenger.lastState = '';
UI_tabPassenger.continueToAnci = function(isOnHold, isWaitListing) {
	if (UI_tabPassenger.validate()) {
		if (UI_makeReservation.tabDataRetrive.tab2) {
			if(UI_tabPassenger.lastState != ''){
				if(isOnHold && UI_tabPassenger.lastState == 'CF' || !isOnHold && UI_tabPassenger.lastState == 'OH'){
					UI_tabAnci.resetAnciData();
					UI_tabPassenger.passengerInfoChanged();
				}
			}
			
			if(isOnHold) UI_tabPassenger.lastState = 'OH';
			else UI_tabPassenger.lastState = 'CF';
			
			if (UI_tabPassenger.anciAvailabilityResponse == null) {
				var data = UI_tabSearchFlights
						.createSearchParams('searchParams');
				data['onHoldBooking'] = isOnHold;
				data['waitListingBooking'] = isWaitListing;
				data['flightRPHList'] = UI_tabPassenger.getFlightRPHList();
				//data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
								
			// data['paxAdults'] = UI_tabPassenger.createAdultPaxInfo();
			// data['paxInfants'] = UI_tabPassenger.createInfantPaxInfo();
			// data['flightDetails'] = $.toJSON(jsonFltDetails);
				data['foidIds'] = UI_tabPassenger.foidIds;
				data['foidCode'] = UI_tabPassenger.foidCode;
				data['segmentList'] = UI_tabPassenger.segmentList;
				data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();
				data['mulipleMealsSelectionEnabled'] = DATA_ResPro.initialParams.multiMealEnabled;
				data['isOpenReturnReservation'] = (jsonOpenRetInfo != null) ? true : false;

				UI_commonSystem.showProgress();
				UI_commonSystem.getDummyFrm().ajaxSubmit( {
					dataType : 'json',
					url : 'ancillaryAvailability.action',
					data : data,
					success : UI_tabPassenger.continueOnClickSuccess,
					error : UI_commonSystem.setErrorStatus
				});
			} else {
				UI_tabPassenger
						.continueOnClickSuccess(UI_tabPassenger.anciAvailabilityResponse);
			}
		} else {
			showERRMessage('Ancillary is not ready');
		}
	}
}

UI_tabPassenger.continueToAnciOnBlacklistCnfirm = function(isOnHold, isWaitListing){
	if(isOnHold) UI_tabPassenger.lastState = 'OH';
	else UI_tabPassenger.lastState = 'CF';
	
	if (UI_tabPassenger.anciAvailabilityResponse == null) {
		var data = {};
		if (UI_tabSearchFlights.isCalenderMinimumFare()) {
			data = UI_MinimumFare.createSearchParams('searchParams');
		} else {
			data = UI_tabSearchFlights.createSearchParams('searchParams');
		}
		data['onHoldBooking'] = isOnHold;
		data['waitListingBooking'] = isWaitListing;
		data['flightRPHList'] = UI_tabPassenger.getFlightRPHList();
		//data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
						
	// data['paxAdults'] = UI_tabPassenger.createAdultPaxInfo();
	// data['paxInfants'] = UI_tabPassenger.createInfantPaxInfo();
	// data['flightDetails'] = $.toJSON(jsonFltDetails);
		data['foidIds'] = UI_tabPassenger.foidIds;
		data['foidCode'] = UI_tabPassenger.foidCode;
		data['segmentList'] = UI_tabPassenger.segmentList;
		data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();
		data['mulipleMealsSelectionEnabled'] = DATA_ResPro.initialParams.multiMealEnabled;
		data['isOpenReturnReservation'] = (jsonOpenRetInfo != null) ? true : false;

		UI_commonSystem.showProgress();
		UI_commonSystem.getDummyFrm().ajaxSubmit( {
			dataType : 'json',
			url : 'ancillaryAvailability.action',
			data : data,
			success : UI_tabPassenger.continueOnClickSuccess,
			error : UI_commonSystem.setErrorStatus
		});
	} else {
		UI_tabPassenger
				.continueOnClickSuccess(UI_tabPassenger.anciAvailabilityResponse);
	}
}


UI_tabPassenger.getFlightRPHList = function() {
	
	var rphArray = [];
	if(UI_tabSearchFlights.isMulticity()){		
		rphArray = UI_Multicity.getFlightRPHList();
	}else if(UI_tabSearchFlights.isCalenderMinimumFare()){
		rphArray=UI_MinimumFare.getFlightRPHList();
	}else{
		rphArray = UI_tabSearchFlights.getFlightRPHList();
	}
	
	return $.toJSON(rphArray);
}

UI_tabPassenger.isTBAViolated = function(){
	var adults = UI_tabPassenger.getAdultDetails();
	var infants = UI_tabPassenger.getInfantDetails();
	
	var TBA_NO_SPACE ="TBA";
	
	function normalizeName(name){
		return name.trim().replace(/\s/g, '').toUpperCase();
	}
	
	function isHasTBA(paxArr){
		if(paxArr){
			for (var i =0; i < paxArr.length; i++){
				if (paxArr[i].displayAdultType == "AD" || paxArr[i].displayAdultType == "CH") {
					if(normalizeName(paxArr[i].displayAdultFirstName) == TBA_NO_SPACE || normalizeName(paxArr[i].displayAdultLastName) == TBA_NO_SPACE ){
						return true;
					}					
				}
				else if(normalizeName(paxArr[i].displayInfantFirstName) == TBA_NO_SPACE || normalizeName(paxArr[i].displayInfantLastName) == TBA_NO_SPACE ){
					return true;
				}
				
			}
		}
		return false;
	}
	
	if( (isHasTBA(adults) || isHasTBA(infants)) &&   !(top.arrPrivi[PRIVI_TBA_BKG] == 1)) {
		return true;
	}
	return false;
}


/*
 * OnHold Button Click
 * 
 */
UI_tabPassenger.onHoldOnClick = function() {
	if(UI_tabSearchFlights.isAgentCommissionApplied){
		var proceedWithoutAgentCommission = confirm(raiseError("XBE-CONF-06", "Onhold"));
		if(!proceedWithoutAgentCommission){
			return ;
		}
	}
	
	if(UI_tabSearchFlights.isPromotionApplied()){		
		$('#divPromoRemoveConfirm').show();
	} else {		
		UI_tabPassenger.continueToAnci(true, false);
	}
}

/*
 * WaitList Button Click
 * 
 */
UI_tabPassenger.waitListOnClick = function() {
	if(UI_tabSearchFlights.isPromotionApplied()){		
		$('#divPromoRemoveConfirm').show();
	} else {		
		UI_tabPassenger.continueToAnci(true, true);
	}
}

/*
 * Create Adult Passenger Json Strings
 */
UI_tabPassenger.createAdultPaxInfo = function() {
	var objAdt = jsonPaxAdtObj.paxAdults;
	return ($.toJSON(objAdt));
	
}

UI_tabPassenger.createCountryAdultPaxInfo = function() {
	var carriers  = tempCarrierList.split(',')[0];
	var paxType = UI_tabSearchFlights.strPaxType;
	
	var indexStr = carriers + paxType;
	var objCountryAdt = top.globalPaxContactConfig[indexStr];
	
	return objCountryAdt;
}

/*
 * Create Adult Passenger Json Strings
 */
UI_tabPassenger.createInfantPaxInfo = function() {
	var strJS = "";
	if (jsonPaxAdtObj.paxInfants != null) {
		var objAdt = jsonPaxAdtObj.paxInfants;
		strJS = $.toJSON(objAdt);
	}
	return strJS;
}

/*
 * Continue On Click success
 */
UI_tabPassenger.continueOnClickSuccess = function(response) {

	if (response != null) {
		if (!response.success) {
			showERRMessage(response.messageTxt);
			UI_commonSystem.hideProgress();
			return false;
		}
		response.paxAdults = UI_tabPassenger.createAdultPaxInfo();
		response.paxInfants = UI_tabPassenger.createInfantPaxInfo();
		UI_tabAnci.prepareAncillary(response, jsonFareQuoteSummary);
		if(response.onHoldBooking){
			UI_tabSearchFlights.removeAppliedPromotion();
		} else {
			UI_tabSearchFlights.restoreAppliedPromotion();
		}
	}
}

/*
 * Apply TBA Click;
 */
UI_tabPassenger.applyTBAOnClick = function() {
	/* Adult */
	var objJPax = jsonPaxAdtObj.paxAdults;
	var intLen = objJPax.length;
	var i = 0;
	if (intLen > 0) {
		do {
			$("#" + (i + 1) + "_displayAdultFirstName").val(
					UI_tabPassenger.xbeTBA);
			$("#" + (i + 1) + "_displayAdultLastName").val(
					UI_tabPassenger.xbeTBA);
			if(i==0){
				$("#" + (i + 1) + "_displayAdultTitle").val("MR");
			}

			i++;
		} while (--intLen);
	}

	/* Infant */
	objJPax = jsonPaxAdtObj.paxInfants;
	if (objJPax != null) {
		intLen = objJPax.length;
		i = 0;
		if (intLen > 0) {
			do {
				$("#" + (i + 1) + "_displayInfantFirstName").val(
						UI_tabPassenger.xbeTBA);
				$("#" + (i + 1) + "_displayInfantLastName").val(
						UI_tabPassenger.xbeTBA);
				$("#" + (i + 1) + "_displayInfantTravellingWith").val(i + 1);
				$("#" + (i + 1) + "_displayInfantTitle").val(i + 1);

				i++;
			} while (--intLen);
		}
	}
}

/*
 * SSR Confirmation Click
 */
UI_tabPassenger.ssrConfirmOnClick = function() {
	var strSSRCode = $("#selSSRCode").val();
	var strSSRInfo = $("#txtComment").val();

	switch (UI_tabPassenger.strLastSSRType) {
	case UI_tabPassenger.strAdtCode:
		jsonPaxAdtObj.paxAdults[UI_tabPassenger.intLastSSRRow].displayAdultSSRCode = strSSRCode;
		jsonPaxAdtObj.paxAdults[UI_tabPassenger.intLastSSRRow].displayAdultSSRInformation = strSSRInfo;
		break;
	case UI_tabPassenger.strInfCode:
		jsonPaxAdtObj.paxInfants[UI_tabPassenger.intLastSSRRow].displayInfantSSRCode = strSSRCode;
		jsonPaxAdtObj.paxInfants[UI_tabPassenger.intLastSSRRow].displayInfantSSRInformation = strSSRInfo;
		break;
	}

	UI_tabPassenger.ssrCancelOnClick();
}

/*
 * SSR Cancel Click
 */
UI_tabPassenger.ssrCancelOnClick = function() {
	$("#selSSRCode").val("");
	$("#txtComment").val("");
	$("#divSSRPane").hide();

	UI_tabPassenger.intLastSSRRow = null;
	UI_tabPassenger.strLastSSRType = null;
}

/*
 * Reset Contact Details
 */
UI_tabPassenger.resetOnClick = function() {
	UI_commonSystem.initializeMessage();

	var objJCntrl = jsonPaxObj;
	var intLen = objJCntrl.length;
	if (intLen > 0) {
		var i = 0;
		do {
			$(objJCntrl[i].id).val("");
			i++;
		} while (--intLen);
	}
	
	if(DATA_ResPro["systemParams"] != null){
		$("#selPrefLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
	}	
	
	UI_tabPassenger.defaultFocus();
}

/*
 * Load Search Flight Tab
 */
UI_tabPassenger.cancelOnClick = function() {
	UI_tabSearchFlights.releaseBlockSeats();
	UI_makeReservation.openTab(0);
}

/*
 * Load Profile Button Click
 */
UI_tabPassenger.loadProfileOnClick = function() {
	// $("#frmMakeBkg").action("loadProfile.action");
	var data = $('input').filterData('name','contactInfo\.mobile');
	data = $.extend(data, $('input').filterData('name','contactInfo\.phone'));
	data['profileID']  = $('#txtPNR').val();
	UI_commonSystem.getDummyFrm().ajaxSubmit( {
		url: 'loadProfile.action',
		dataType : 'json',
		data:data,
		beforeSubmit : UI_tabPassenger.validateLoadProfileClick,
		success : UI_tabPassenger.returnCallFillProfile,
		error : UI_commonSystem.setErrorStatus
	});
	return false;
}

/*
 * Validate Load Profile
 */
UI_tabPassenger.validateLoadProfileClick = function() {
	UI_commonSystem.initializeMessage();

	if ($("#txtPNR").isFieldEmpty() && $("#txtMobiNo").isFieldEmpty()
			&& $("#txtPhoneNo").isFieldEmpty()) {
		showERRMessage(raiseError("XBE-ERR-01", "Profile/PNR Or Contact No"));
		return false;
	}

	UI_commonSystem.showProgress();
	return true;
}

/*
 * Fill Passenger Profile
 */
UI_tabPassenger.returnCallFillProfile = function(response) {
	UI_commonSystem.hideProgress();

	if (response.profileInfoTO != null) {
		var jsonPaxProfile = response.profileInfoTO;

		$("#selTitle").val(jsonPaxProfile.title);
		$("#txtFirstName").val(jsonPaxProfile.firstName);
		$("#txtLastName").val(jsonPaxProfile.lastName);
		$("#txtCity").val(jsonPaxProfile.city);
		$("#txtZipCode").val(jsonPaxProfile.zipCode);
		$("#selNationality").val(jsonPaxProfile.nationality);
		$("#txtStreet").val(jsonPaxProfile.addressStreet);
		$("#txtAddress").val(jsonPaxProfile.addressLine);
		$("#selCountry").val(jsonPaxProfile.country);
		$("#txtMobCntry").val(jsonPaxProfile.mobileCountry);
		$("#txtMobArea").val(jsonPaxProfile.mobileArea);
		$("#txtMobiNo").val(jsonPaxProfile.mobileNo);
		$("#txtPhoneCntry").val(jsonPaxProfile.phoneCountry);
		$("#txtPhoneArea").val(jsonPaxProfile.phoneArea);
		$("#txtPhoneNo").val(jsonPaxProfile.phoneNo);
		$("#txtFaxCntry").val(jsonPaxProfile.faxCountry);
		$("#txtFaxArea").val(jsonPaxProfile.faxArea);
		$("#txtFaxNo").val(jsonPaxProfile.faxNo);
		$("#txtEmail").val(jsonPaxProfile.email);
		$("#selEmgnTitle").val(jsonPaxProfile.emgnTitle);
		$("#txtEmgnFirstName").val(jsonPaxProfile.emgnFirstName);
		$("#txtEmgnLastName").val(jsonPaxProfile.emgnLastName);
		$("#txtEmgnPhoneCntry").val(jsonPaxProfile.emgnPhoneCountry);
		$("#txtEmgnPhoneArea").val(jsonPaxProfile.emgnPhoneArea);
		$("#txtEmgnPhoneNo").val(jsonPaxProfile.emgnPhoneNo);
		$("#txtEmgnEmail").val(jsonPaxProfile.emgnEmail);
		
		profEmailAddress = jsonPaxProfile.email;
		$("#chkUpdateCustProf").enable();
	} else {
		showERRMessage(raiseError("XBE-ERR-04", "Profile"));
		$("#txtPNR").focus();
	}
}

/*
 * SSR Button click
 */
UI_tabPassenger.btnSSROnClick = function(inParams) {
	UI_tabPassenger.intLastSSRRow = inParams.row;
	UI_tabPassenger.strLastSSRType = inParams.type;

	var strSSRCode = "";
	var strSSRInfo = "";
	switch (UI_tabPassenger.strLastSSRType) {
	case "ADT":
		strSSRCode = jsonPaxAdtObj.paxAdults[UI_tabPassenger.intLastSSRRow].displayAdultSSRCode;
		strSSRInfo = jsonPaxAdtObj.paxAdults[UI_tabPassenger.intLastSSRRow].displayAdultSSRInformation;
		break;
	case "INF":
		strSSRCode = jsonPaxAdtObj.paxInfants[UI_tabPassenger.intLastSSRRow].displayInfantSSRCode;
		strSSRInfo = jsonPaxAdtObj.paxInfants[UI_tabPassenger.intLastSSRRow].displayInfantSSRInformation;
		break;
	}

	$("#selSSRCode").val(strSSRCode);
	$("#txtComment").val(strSSRInfo);
	$("#divSSRPane").show();
}

/*
 * Flight details
 */
UI_tabPassenger.buildFlightDetails = function() {
	$("#flightTempleteHead").hide();

	/* Initialize the Details */
	$("#flightTemplete").find("tr").remove();

	var objFltDt = jsonFltDetails;
	var objOpenRTInfo  = jsonOpenRetInfo;
	var intLen = objFltDt.length;
	var i = 0;
	var tblRow = null;
	var intRowSpan = intLen;
	var strClass = "";
	
	var strFlightNo = "";
	var strDepDate  = "";
	var strDepTime  = "";
	var arrivalTime = "";
	var strOpenReturnMouseOver ="";
	var strOpenReturnMouseOut = "";
	
	var strFltInfoDisplayMouseOver = "";
	var strFltInfoDisplayMouseOut = ""; 
	
	if(DATA_ResPro.initialParams.fltStopOverDurInfoToolTipEnabled){
		strFltInfoDisplayMouseOver =  UI_tabPassenger.displayFlightAddInfoToolTip;
		strFltInfoDisplayMouseOut  =  UI_tabPassenger.hideFlightAddInfoToolTip;
	}
	
	if (intLen > 0) {
		
		do{
		
		strClass = "";
		tblRow = document.createElement("TR");
		
		if(objFltDt[i].openReturnSegment){
			//Display empty flight data
			 strFlightNo = "";
			 strDepDate  = "";
			 strDepTime  = "";
			 arrivalTime = "";
			 strFltInfoDisplayMouseOver = "";
			 strFltInfoDisplayMouseOut = "";
			 
//			 strOpenReturnMouseOver =  UI_tabPassenger.displayOPRTSegmentInfoToolTip;
//			 strOpenReturnMouseOut  =  UI_tabPassenger.hideOPRTSegmentInfoToolTip;
		}else{
			 strFlightNo = objFltDt[i].flightNo;
			 strDepDate  = objFltDt[i].departureDate;
			 strDepTime  = objFltDt[i].departureTime;
			 arrivalTime = objFltDt[i].arrivalTime;
		}

		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : objFltDt[i].orignNDest,
			css : UI_commonSystem.strDefClass + " thinBorderL rowHeight"
		}));
	
		
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : strFlightNo,
			id:"td_" + i + "_1", 
			css : UI_commonSystem.strDefClass,
			align : "center",
			mouseOver:strFltInfoDisplayMouseOver, 
			mouseOut:strFltInfoDisplayMouseOut
		}));
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : UI_tabSearchFlights.getBusCarrierCode(objFltDt[i].airLine),
			css : UI_commonSystem.strDefClass,
			align : "center",
			bold : true,
			textCss : "color:red;"
		}));
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : strDepDate,
			css : UI_commonSystem.strDefClass,
			align : "center"
		}));

		if (i == 0) {
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : " ",
				rowSpan : intRowSpan,
				css : UI_commonSystem.strDefClass + " lightBG"
			}));
		}
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : objFltDt[i].departureText,
			css : UI_commonSystem.strDefClass,
			align : "center",
			bold : true
		}));
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : objFltDt[i].departure,
			css : UI_commonSystem.strDefClass,
			align : "center"
		}));
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : strDepTime,
			css : UI_commonSystem.strDefClass,
			align : "center"
		}));

		if (i == 0) {
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : " ",
				rowSpan : intRowSpan,
				css : UI_commonSystem.strDefClass + " lightBG"
			}));
		}
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : objFltDt[i].arrivalText,
			css : UI_commonSystem.strDefClass,
			align : "center",
			bold : true
		}));
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : objFltDt[i].arrival,
			css : UI_commonSystem.strDefClass,
			align : "center"
		}));
		tblRow.appendChild(UI_commonSystem.createCell( {
			desc : arrivalTime,
			css : UI_commonSystem.strDefClass,
			align : "center"
		}));
		/*if(objFltDt[i].openReturnSegment==true && objOpenRTInfo!=null){
			var imageID = "img_" + i + "_14";
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : desc:"" + '&nbsp;<img border="0" id="'+imageID +'" src="../images/AA169_N_no_cache.gif"/>',
				css : UI_commonSystem.strDefClass,
				align : "center"
				click:strOnClick,
				mouseOver:strOpenReturnMouseOver,
				mouseOut:strOpenReturnMouseOut
			}));
		}
		else{
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : "",
				css : UI_commonSystem.strDefClass,
				align : "center"
			}));
		}*/
		
		

		$("#flightTemplete").append(tblRow);
		i++;
	} while (--intLen);
	}
}

/*UI_tabPassenger.displayOPRTSegmentInfoToolTip = function(){
	  var sHtm  ="";
	  var objAltDt = jsonOpenRetInfo;
	  if (this.id != "") {
			var arrID = this.id.split("_");
			var id = (new  Number(arrID[1])) - 0;
			var pnrSegID = jsonFltDetails[id].flightSegmentRefNumber;			
			var strData = "";	
			if (pnrSegID != "" && objAltDt.length != 0) {
				for (var i = 0; i < objAltDt.length; i++) {
					if (pnrSegID == objAltDt[i].flightSegmantRefNumber) {
						strData += "<font>";
						for (var a = 0 ; a  < objAltDt[i].alertTO.length; a++) {
									strData +=objAltDt[i].alertTO[a].content + " ";
						}
						strData += "</font>";
						break;
					}
				}
			}
				sHtm += strData;					
	  } 
	  $("#openReturnToolTipBody").html(sHtm);	 
	  var pos = $("#" + this.id).offset();
      var width = $("#" +this.id).width();
      $("#openReturnToolTip").css({
          left: (pos.left) -200 + 'px',
          top: pos.top + 20 + 'px'
         
      });      
     $("#openReturnToolTip").show(); 
}

UI_tabPassenger.hideOPRTSegmentInfoToolTip= function(){
	 $("#openReturnToolTip").hide();
}*/


/** Flight Info tool tip display*/
UI_tabPassenger.displayFlightAddInfoToolTip = function() {
	  var sHtm  ="";
	  if (this.id != "") {	  
		  var arrID = this.id.split("_");
		  var id = (new  Number(arrID[1])) - 0;
		  var fltDuration = jsonFltDetails[id].flightDuration;
		  var fltModelDesc = jsonFltDetails[id].flightModelDescription;
		  var fltModel = jsonFltDetails[id].flightModelNumber;
		  var segmentCode = jsonFltDetails[id].orignNDest;
		  var flightNO = jsonFltDetails[id].flightNo;	
		  var specialNote = jsonFltDetails[id].remarks;
		  var stopOverDuration = jsonFltDetails[id].flightStopOverDuration;		  
		  var noOfStops = jsonFltDetails[id].noOfStops;
		  var arrivalTerminalName = jsonFltDetails[id].arrivalTerminal;
		  var departureTerminalName = jsonFltDetails[id].departureTerminal;
		  var csOcCarrierCode = jsonFltDetails[id].csOcCarrierCode;
		  
		  var strData = "";
				
			strData += "<font>Flight No&nbsp;:&nbsp;"+flightNO+"</font><br>";
			strData += "<font>Segment Code&nbsp;:&nbsp;"+segmentCode+"</font><br>";
			strData += "<font>Arrival Terminal&nbsp;:&nbsp;"+arrivalTerminalName+"</font><br>";	
			strData += "<font>Departure Terminal&nbsp;:&nbsp;"+departureTerminalName+"</font><br>";	
			strData += "<font>Flight Duration&nbsp;:&nbsp;"+fltDuration+"</font><br>";
			strData += "<font>No of Stops&nbsp;:&nbsp;"+noOfStops+"</font><br>";
			strData += "<font>Transit Duration&nbsp;:&nbsp;"+stopOverDuration+"</font><br>";				
			//strData += "<font>Aircraft Model&nbsp;:&nbsp;"+fltModel+"</font><br>";
			strData += "<font>Aircraft Model&nbsp;:&nbsp;"+fltModelDesc+"</font><br>";
			strData += "<font>Additional Note&nbsp;:&nbsp;"+specialNote+"</font><br>";
			if(typeof(csOcCarrierCode) != "undefined" && csOcCarrierCode != "" && csOcCarrierCode != null){
				strData += "<font>Operated By&nbsp;:&nbsp;"+csOcCarrierCode+"</font><br>";						
			}
			
			sHtm += strData;					
	  } 
	  $("#flightAddInfoToolTipBody").html(sHtm);	 
	  var pos = $("#" + this.id).offset();

    $("#flightAddInfoToolTip").css({
        left: (pos.left-100) + 'px',
        top: pos.top + 20 + 'px'
       
    });      
   $("#flightAddInfoToolTip").show(); 	  
	}

UI_tabPassenger.hideFlightAddInfoToolTip = function() {
	  $("#flightAddInfoToolTip").hide();
}



/*
 * Price Break down
 */
UI_tabPassenger.buildPaxPriceBreakDown = function() {
	// Map the Currency to the DTO
	if (jsonFareQuoteSummary.currency == "") {
		jsonFareQuoteSummary.currency = UI_tabSearchFlights.objResSrchParams.selectedCurrency;
	}

	$("#divPBTxnCurr").html(jsonFareQuoteSummary.currency);
	$("#divPBFareAmt").html(jsonFareQuoteSummary.totalHandlingCharge);
	
	var tmpSelectedtotalPrice = jsonFareQuoteSummary.selectedtotalPrice;
	var tmpTotalWithoutHandlingCharge = jsonFareQuoteSummary.totalWithoutHandlingCharge;
	var tmpTotalPrice = jsonFareQuoteSummary.totalPrice;

	if (jsonFareQuoteSummary.selectedCurrency != jsonFareQuoteSummary.currency) {
		$("#divPBTaxAmt").html(
				"<font color='red'>" + tmpSelectedtotalPrice
						+ " " + jsonFareQuoteSummary.selectedCurrency
						+ "</font>");
	} else {
		$("#divPBTaxAmt").html(tmpTotalWithoutHandlingCharge);
	}
	//TODO show hide fare discount 
	//$('#trPBFareDisc').show();
	
	var discountAmount = "0.00";
	var lblTotDisc = top.arrError['XBE-LABEL-01'];
	if(jsonFareQuoteSummary.totalFareDiscount!=null && jsonFareQuoteSummary.totalFareDiscount!='0.00'){
		discountAmount = '-' + jsonFareQuoteSummary.totalFareDiscount;
		$("#trPBFareDisc").show();
		$("#trPBFareDiscCredit").hide();
	} else if(UI_tabSearchFlights.dummyCreditDiscount != null && UI_tabSearchFlights.dummyCreditDiscount != '0.00') {
		discountAmount = UI_tabSearchFlights.dummyCreditDiscount;
		lblTotDisc = top.arrError['XBE-LABEL-02'];
		$("#trPBFareDiscCredit").show();
		$("#trPBFareDisc").hide();
	} else {
		$("#trPBFareDisc").hide();
		$("#trPBFareDiscCredit").hide();
	}
	
	$(".divPBFareDisc").html(discountAmount);
	$(".lblTotDisc").text(lblTotDisc);
	
	$("#divPBTotAmt").html(tmpTotalPrice);

	$("#divPBADFee").html(jsonFareQuoteSummary.adminFee);

	/* Initialize the Details */
	$("#paxPriceTemplete").find("tr").remove();

	var objPaxDt = jsonPaxPrice;
	var intLen = objPaxDt.length;
	var i = 0;
	var tblRow = null;
	var strClass = "";
	if (intLen > 0) {
		do {
			strClass = "";
			var surcharge = objPaxDt[i].sur;
			var total = objPaxDt[i].total;
			
			tblRow = document.createElement("TR");

			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : objPaxDt[i].paxType,
				css : UI_commonSystem.strDefClass + " thinBorderL rowHeight"
			}));
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : objPaxDt[i].noPax,
				css : UI_commonSystem.strDefClass,
				align : "right"
			}));
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : objPaxDt[i].fare,
				css : UI_commonSystem.strDefClass,
				align : "right"
			}));
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : objPaxDt[i].tax,
				css : UI_commonSystem.strDefClass,
				align : "right"
			}));
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : surcharge,
				css : UI_commonSystem.strDefClass,
				align : "right"
			}));
			tblRow.appendChild(UI_commonSystem.createCell( {
				desc : total,
				css : UI_commonSystem.strDefClass,
				align : "right"
			}));

			$("#paxPriceTemplete").append(tblRow);
			i++;
		} while (--intLen);
	}
}

/*
 * Grid Constructor
 */
UI_tabPassenger.constructGridAdult = function(inParams) {
	// construct grid
	var fields = ['title', 'firstName','lastName','nationality','dob','ffid'];
	var fieldConfig = {};
	for (var i = 0; i < fields.length; i++) {
		var obj = $.extend({
			visible : true,
			mandatory : ""
		}, {
			visible : isVisible(paxConfigAD, paxConfigCH, fields[i]),
			mandatory : isMandatory(paxConfigAD, paxConfigCH, fields[i])
		});
		fieldConfig[fields[i]] = obj;
	}
	
	
	function isMandatory(configFirst, configSecond, field){
		if(configFirst[field] == undefined || configSecond[field] == undefined)
			return UI_commonSystem.strTxtMandatory;
		if (configFirst[field].xbeMandatory || configSecond[field].xbeMandatory) 
			return UI_commonSystem.strTxtMandatory;
		return "";
	}
	
	function isVisible(configFirst, configSecond, field){
		if(configFirst[field] == undefined || configSecond[field] == undefined)
			return true;
		return (configFirst[field].xbeVisibility || configSecond[field].xbeVisibility) ;
	}
	
	$(inParams.id).GridUnload();
	
	var columnModel = [ { name: 'sequenceNo',
    	index: 'sequenceNo',
    	width: 20,
    	align: "center"						            		
    }, {
		name : 'displayAdultType',
		index : 'displayAdultType',
		width : 50,
		align : "center"
	}, {
		name : 'displayAdultTitle',
		index : 'displayAdultTitle',
		width : 70,
		align : "center",
		editable : true,
		edittype : "select",
		editoptions : {
			className : "aa-input",
			value : jsonPaxTypes,
			style : "width:50px;font-size:11px",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabPassenger.titleOnChange()
				}
			}]
		},
		hidden:!fieldConfig['title'].visible
	}, {
		name : 'displayAdultFirstName',
		index : 'displayAdultFirstName',
		width : 150,
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input fontCapitalize",
			maxlength : "30",
			// onchange : 'UI_tabPassenger.firstNameOnChange()', //
			// this binding is not working in IE used the below one
			style : "width:130px;",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabPassenger.firstNameOnChange();
				}
			}]
		},
		hidden:!fieldConfig['firstName'].visible
	}, {
		name : 'displayAdultLastName',
		index : 'displayAdultLastName',
		width : 150,
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input fontCapitalize",
			maxlength : "30",
			// onchange : 'UI_tabPassenger.lastNameOnChange()',
			style : "width:130px;",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabPassenger.lastNameOnChange()
				}
			}]
		},
		hidden:!fieldConfig['lastName'].visible
	}, {
		name : 'displayAdultNationality',
		index : 'displayAdultNationality',
		width : 180,
		align : "center",
		editable : true,
		edittype : "select",
		editoptions : {
			className : "aa-input",
			value : jsonNationality,
			// onchange :
			// 'UI_tabPassenger.nationalityOnChange(event)',
			style : "width:150px;",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabPassenger.nationalityOnChange(e);
				}
			}]
		},
		hidden:!fieldConfig['nationality'].visible
	}, {
		name : 'displayAdultDOB',
		index : 'displayAdultDOB',
		width : 100,
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			size : "10",
			maxlength : "10",
			style : "width:95px",
			// onchange : 'UI_tabPassenger.dobOnBlur(this)'
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabPassenger.dobOnBlur(this);
				}
			}]
		},
		hidden:!fieldConfig['dob'].visible
	}];
	
	var columnNames = [ '',
	                    geti18nData('passenger_Type','Type'), geti18nData('passenger_Title','Title') + fieldConfig['title'].mandatory,
	         geti18nData('passenger_FirstName','First Name') + fieldConfig['firstName'].mandatory,
	         geti18nData('passenger_LastName','Last Name') + fieldConfig['lastName'].mandatory,
	         geti18nData('passenger_Nationality','Nationality') + fieldConfig['nationality'].mandatory,
	         geti18nData('passenger_DateOfBirth','Date of Birth') + fieldConfig['dob'].mandatory];
	
	
		columnNames.push( geti18nData('passenger_AirewardsID','Air Rewards ID'));
		columnModel.push({name : 'displayFFID',	index : 'displayFFID',	width : 150, align : "center", editable : true,
			editoptions : {	className : "aa-input",	maxlength : "255", style : "width:130px", 
				dataEvents:[{
					type : 'change',
					fn: function(e){
						UI_tabPassenger.nationalityOnChange(e);
					}
				}]
			},
			hidden:!fieldConfig['ffid'].visible,
		});
		
	columnNames.push(geti18nData('passenger_Total','Total'));
	columnModel.push({name : 'displayAdultCharg', index : 'displayAdultCharg', width : 80, align : "right"});
	
	if(!top.arrPrivi['xbe.tool.frequentcustomer.save']){
		columnNames.push('');
		columnModel.push({
			name : 'displayFQCus',
			index : 'displayFQCus',
			width : 30,
			align : "center",
			editable : true,
			edittype : "button",
			editoptions : {
				className : "btnMedium ui-state-default ui-corner-all",
				size : "10",
				maxlength : "10",
				style : "background: url(../images/star_outline.png); width:20px; height:20px; border:none;",
				dataEvents:[{
					type : 'click',
					fn: function(e){
						UI_tabPassenger.saveFrequentAdultCustomerOnClick();
					}
				}]
			},
			hidden:false
		});
	}
	

	
	$(inParams.id).jqGrid(
			{
				height : 30,
				width : 910,
				colNames : columnNames,
				colModel : columnModel,

				imgpath : UI_commonSystem.strImagePath,
				multiselect : false,
				datatype : "local",
				viewrecords : true
			});
}

/*
 * Grid Constructor
 */
UI_tabPassenger.constructGridInf = function(inParams) {
	var blnFName = true;
	var blnLName = true;
	var blnNationality = true;
	var blnDOB = true;
	var blnFFID = true;
	var blnWithAdult = true;
	var strFNameMand = "";
	var strLNameMand = "";
	var strNationalityMand = "";
	var strDOBMand = "";
	var strWithAdult = "";
	var strTitleMand = "";
	var blnTitle = true;
	var blnFQCus = false;
	
	if(paxConfigIN.firstName.xbeVisibility){
		blnFName = false;
	}
	
	if(paxConfigIN.lastName.xbeVisibility){
		blnLName = false;
	}
	
	if(paxConfigIN.nationality.xbeVisibility){
		blnNationality = false;
	}
	
	if(paxConfigIN.dob.xbeVisibility){
		blnDOB = false;
	}
	
	if(paxConfigIN.travelWith.xbeVisibility){
		blnWithAdult = false;
	}	
	
	if(paxConfigIN.title.xbeVisibility){
		blnTitle = false;
	}
	
	if(paxConfigIN.firstName.xbeMandatory){
		strFNameMand = UI_commonSystem.strTxtMandatory;
	}
	
	if(paxConfigIN.lastName.xbeMandatory){
		strLNameMand = UI_commonSystem.strTxtMandatory;
	}
	
	if(paxConfigIN.nationality.xbeMandatory){
		strNationalityMand = UI_commonSystem.strTxtMandatory;
	}
	
	if(paxConfigIN.dob.xbeMandatory){
		strDOBMand = UI_commonSystem.strTxtMandatory;
	}
	
	if(paxConfigIN.travelWith.xbeMandatory){
		strWithAdult = UI_commonSystem.strTxtMandatory;
	}
	
	if(paxConfigIN.title.xbeMandatory){
		strTitleMand = UI_commonSystem.strTxtMandatory;
		blnTitle = false;
	}
	
	if(!top.arrPrivi['xbe.tool.frequentcustomer.save']){
		blnFQCus = true;
	}
	
	// construct grid
	$(inParams.id)
			.jqGrid(
					{
						height : 30,
						width : 910,
						colNames : [ '',geti18nData('passenger_Type','Type'), geti18nData('passenger_Title','Title') + strTitleMand,
					       	         geti18nData('passenger_FirstName','First Name') + strFNameMand,
					       	         geti18nData('passenger_LastName','Last Name') + strLNameMand,
					       	         geti18nData('passenger_Nationality','Nationality') + strNationalityMand,
					       	         geti18nData('passenger_DateOfBirth','Date of Birth') + strDOBMand,
					       	         geti18nData('passenger_WithAdult','With Adult') + strWithAdult,
					       	         ''],
						colModel : [
						            { name: 'sequenceNo',
						            	index: 'sequenceNo',
						            	width: 20,
						            	align: "center"				            		
						            },
								{
									name : 'displayInfantType',
									index : 'displayInfantType',
									width : 50,
									align : "center"
								},
								{
									name : 'displayInfantTitle',
									index : 'displayInfantTitle',
									width : 70,
									align : "center",
									editable : true,
									edittype : "select",
									editoptions : {
										className : "aa-input",
										value : jsonInfPaxTypes,
										style : "width:50px;font-size:11px",
										dataEvents:[{
											type : 'change',
											fn: function(e){
												UI_tabPassenger.titleOnChange()
											}
										}]
									},
									hidden:blnTitle
								},
								{
									name : 'displayInfantFirstName',
									index : 'displayInfantFirstName',
									width : 175,
									align : "center",
									editable : true,
									editoptions : {
										className : "aa-input fontCapitalize",
										maxlength : "30",
										onchange : 'UI_tabPassenger.passengerInfoChanged();UI_commonSystem.pageOnChange()',
										style : "width:130px;"
									},
									hidden:blnFName
								},
								{
									name : 'displayInfantLastName',
									index : 'displayInfantLastName',
									width : 175,
									align : "center",
									editable : true,
									editoptions : {
										className : "aa-input fontCapitalize",
										maxlength : "30",
										onchange : 'UI_tabPassenger.passengerInfoChanged();UI_commonSystem.pageOnChange()',
										style : "width:130px;"
									},
									hidden:blnLName
								},
								{
									name : 'displayInfantNationality',
									index : 'displayInfantNationality',
									width : 180,
									align : "center",
									editable : true,
									edittype : "select",
									editoptions : {
										className : "aa-input",
										value : jsonNationality,
										style : "width:150px;",
										onchange : 'UI_tabPassenger.passengerInfoChanged();UI_commonSystem.pageOnChange()'
									},
									hidden:blnNationality
								},
								{
									name : 'displayInfantDOB',
									index : 'displayInfantDOB',
									width : 130,
									align : "center",
									editable : true,
									editoptions : {
										className : "aa-input",
										size : "10",
										maxlength : "10",
										style : "width:125px;",
										onchange : 'UI_tabPassenger.dobOnBlur(this)'
									},
									hidden:blnDOB
								},
								{
									name : 'displayInfantTravellingWith',
									index : 'displayInfantTravellingWith',
									width : 100,
									align : "center",
									editable : true,
									edittype : "select",
									editoptions : {
										className : "aa-input",
										value : jsonTravellingWith,
										onchange : 'UI_tabPassenger.passengerInfoChanged();UI_commonSystem.pageOnChange()'
									},
									hidden:blnWithAdult
								},
								{
									name : 'displayInftFQCus',
									index : 'displayInftFQCus',
									width : 30,
									align : "center",
									editable : true,
									edittype : "button",
									editoptions : {
										className : "btnMedium ui-state-default ui-corner-all",
										size : "10",
										maxlength : "10",
										style : "background: url(../images/star_outline.png);width:20px; height:20px; border:none;",
										dataEvents:[{
											type : 'click',
											fn: function(e){
												UI_tabPassenger.saveFrequentInftCustomerOnClick();
											}
										}]
									},
									hidden:blnFQCus
								} ],

						imgpath : UI_commonSystem.strImagePath,
						multiselect : false,
						datatype : "local",
						viewrecords : true
					});
	$(inParams.id)
			.setColProp(
					'displayInfantTravellingWith',
					{
						editoptions : {
							value : jsonTravellingWith,
							className : "aa-input",
							onchange : 'UI_tabPassenger.passengerInfoChanged();UI_commonSystem.pageOnChange()'
						}
					});
}

/*
 * Nationality On Change
 */
UI_tabPassenger.nationalityOnChange = function(obj) {
	var id = parseInt(obj.target.id.split('_')[0], 10);
	if (!$("#1_displayAdultNationality").isFieldEmpty() && id == 1) {
		var strNationValue = $("#1_displayAdultNationality").val();
		var i = 0;
		var strCntrlID = "";
		/* Adults */
		$.each(jsonPaxAdtObj.paxAdults, function() {
			if (i > 0) {
				strCntrlID = "#" + (i + 1) + "_displayAdultNationality";
				if ($(strCntrlID).isFieldEmpty()) {
					$(strCntrlID).val(strNationValue);
				}
			}
			i++;
		});

		/* Infant */
		i = 0;
		if (jsonPaxAdtObj.paxInfants != null) {
			$.each(jsonPaxAdtObj.paxInfants, function() {
				strCntrlID = "#" + (i + 1) + "_displayInfantNationality";
				if ($(strCntrlID).isFieldEmpty()) {
					$(strCntrlID).val(strNationValue);
				}
				i++;
			});
		}
		UI_tabPassenger.passengerInfoChanged();
	}
}

/*
 * Title On Change
 */
UI_tabPassenger.titleOnChange = function() {
	if (!$("#1_displayAdultTitle").isFieldEmpty()) {
		if ($("#selTitle").isFieldEmpty()) {
			$("#selTitle").val($("#1_displayAdultTitle").val().toUpperCase());
		}
	}
	UI_tabPassenger.passengerInfoChanged();
	UI_commonSystem.pageOnChange();
}

/*
 * First Name on Change
 */
UI_tabPassenger.firstNameOnChange = function() {
	if (!$("#1_displayAdultFirstName").isFieldEmpty()) {
		if ($("#txtFirstName").isFieldEmpty()) {
			$("#txtFirstName").val($("#1_displayAdultFirstName").val());
		}
	}
	UI_tabPassenger.passengerInfoChanged();
	UI_commonSystem.pageOnChange();
}

/*
 * First Name on Change
 */
UI_tabPassenger.lastNameOnChange = function() {
	if (!$("#1_displayAdultLastName").isFieldEmpty()) {
		if ($("#txtLastName").isFieldEmpty()) {
			$("#txtLastName").val($("#1_displayAdultLastName").val());
		}
	}
	UI_tabPassenger.passengerInfoChanged();
	UI_commonSystem.pageOnChange();
}

/*
 * Date On Blur
 */
UI_tabPassenger.dobOnBlur = function(objC) {
	var strDT = dateChk(objC.id);
	strDT = strDT != true ? strDT : objC.value;
	objC.value = strDT;
	UI_tabPassenger.passengerInfoChanged();
	UI_commonSystem.pageOnChange();
}

/*
 * Save Frequent Adult Customer On Click
 */

UI_tabPassenger.saveFrequentAdultCustomerOnClick = function(){
	
	var rowId =$("#tblPaxAdt").getGridParam('selrow');  
	var rowData = $("#tblPaxAdt").getRowData(rowId);
	
	$("#"+rowId+"_displayFQCus").css('background-image', 'url(../images/star_yellow_filled.png)');
	
	saveFrequentCustomerPopupInitialize();

	var dateOfBirth = $("#"+rowId+"_displayAdultDOB").val();
	
	$("#firstName").val($("#"+rowId+"_displayAdultFirstName").val());
	$("#lastName").val($("#"+rowId+"_displayAdultLastName").val());
	$("#nationality").val($("#"+rowId+"_displayAdultNationality").val());
	$("#paxType").val(rowData['displayAdultType']);
	$("#title").val($("#"+rowId+"_displayAdultTitle").val());

	$("#titleLable").show();
	$("#titleSelect").show();
	$("#emailLable").show();
	$("#emailInput").show();
	$("#passportDetails").show();
	
	setTimeout(
		function(){
			var d = $.datepicker.parseDate("dd/mm/yy",  dateOfBirth);
			$('#dateOfBirth').datepicker("setDate", d);
		}, 100);
	
	$("#saveFrequentCustomerPopUp").dialog('open');
	
}

/*
 * Save Frequent Infant Customer On Click
 */

UI_tabPassenger.saveFrequentInftCustomerOnClick = function(){
	
	var rowId =$("#tblPaxInf").getGridParam('selrow');  
	var rowData = $("#tblPaxInf").getRowData(rowId);
	
	$("#"+rowId+"_displayInftFQCus").css('background-image', 'url(../images/star_yellow_filled.png)');
	
	saveFrequentCustomerPopupInitialize();
	
	var paxType = rowData['displayInfantType'];
	var title = $("#"+rowId+"_displayInfantTitle").val();
	var dateOfBirth = $("#"+rowId+"_displayInfantDOB").val();
	
	$("#firstName").val($("#"+rowId+"_displayInfantFirstName").val());
	$("#lastName").val($("#"+rowId+"_displayInfantLastName").val());
	$("#nationality").val($("#"+rowId+"_displayInfantNationality").val());
	
	if(paxType == "BABY"){
		$("#paxType").val("IN");
		$("#title").val("");
		$("#titleLable").hide();
		$("#titleSelect").hide();
		$("#emailLable").hide();
		$("#emailInput").hide();
		$("#passportDetails").hide();
	}else{
		$("#paxType").val(paxType);
		$("#title").val(title);
	}

	setTimeout(
		function(){
			var d = $.datepicker.parseDate("dd/mm/yy",  dateOfBirth);
			$('#dateOfBirth').datepicker("setDate", d);
		}, 100);
	
	$("#saveFrequentCustomerPopUp").dialog('open');

}

/*
 * Fill Drop down data for the Passenger tab
 */
UI_tabPassenger.fillDropDownData = function() {
	$("#selTitle").fillDropDown( {
		dataArray : top.arrTitle,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : true
	});
	$("#selCountry").fillDropDown( {
		dataArray : top.arrCountry,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : true
	});
	$("#selSSRCode").fillDropDown( {
		dataArray : top.arrSSR,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : true
	});
	$("#selNationality").fillDropDown( {
		dataArray : top.arrNationality,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : true
	});
	$("#selEmgnTitle").fillDropDown( {
		dataArray : top.arrPaxTitle,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : true
	});
	
	$("#selPrefLang").fillDropDown({
		dataArray:top.jsonItnLang, 
		keyIndex:"id", 
		valueIndex:"desc", 
		firstEmpty:false
	});
	
	$("#selBookingCategory").fillDropDown( {
		dataArray : top.arrBookingCategories,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : (top.arrBookingCategories.length>1)
	});
	
	$("#selOriginCountryOfCall").fillDropDown( {
		dataArray : top.arrCountry,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : false
	});
	
	$("#selItineraryFareMask").fillDropDown( {
		dataArray : top.arrItineraryFareMaskOptions,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : false
	});

	
	if(top.agentFareMaskStatus == 'Y'){
		$("#selItineraryFareMask").val("Y");
	}else{
		$("#selItineraryFareMask").val("N");
	}
	

	var agentLang = UI_tabPassenger.getAgentPreferedLang(top.strAgentCode);
	if( agentLang != null ){
		$("#selPrefLang").val(agentLang);
	} else {
		if(DATA_ResPro["systemParams"] != null){
			$("#selPrefLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
		}	
	}	

	if (DATA_ResPro.initialParams.onHold
			&& (DATA_ResPro.initialParams.transferOwnership || DATA_ResPro.initialParams.transferOwnershipToAnyCarrier)) {
		UI_tabPassenger.fnAsync = function(){
			if(DATA_ResPro.initialParams.transferOwnershipToAnyCarrier){
				$("#selSalesPoint").fillDropDown( {
					dataArray : top.arrAllTransferTravelAgents,
					keyIndex : 0,
					valueIndex : 1,
					firstEmpty : true
				});
			}else if(DATA_ResPro.initialParams.transferOwnership){
				$("#selSalesPoint").fillDropDown( {
					dataArray : top.arrOwnTravelAgents,
					keyIndex : 0,
					valueIndex : 1,
					firstEmpty : true
				});
			}
		};
		setTimeout('UI_tabPassenger.fnAsync()',1000);
	}else{
		$("#selSalesPoint").hide();
		$("#salesPointlblSpn").hide();
	}
	UI_tabPassenger.fillDropFromArray();

	// TODO If enabled this method data is not pupulated for a pages with tabs
	// $("#selCountry").sexyCombo();
}

/*
 * Travelling With Data
 */
UI_tabPassenger.generateTravellingWithData = function() {
	jsonTravellingWith = ":";
	var i = 1;
	var j = 1;
	$.each(jsonPaxAdtObj.paxAdults, function() {
		if (jsonPaxAdtObj.paxAdults[i - 1].displayAdultType == "AD") {
			jsonTravellingWith += ";" + j + ":" + j;
			j++;
		}
		i++;
	});
}

/*
 * Temporary This method is introduced Once data retrevied from server as json
 * string remove this method
 */
UI_tabPassenger.fillDropFromArray = function() {
	var i = 0;

	// Eg : jsonPaxTypes = ":;Mr:Mr;Ms:Ms;Mrs:Mrs;Master:Master;Miss:Miss";
	var intLen = top.arrTitle.length;
	jsonPaxTypes = ":";
	if (intLen > 0) {
		i = 0;
		do {
			jsonPaxTypes += ";" + top.arrTitle[i][0] + ":" + top.arrTitle[i][1];
			i++;
		} while (--intLen);
	}

	// Eg : jsonNationality = ":;LK:Sri Lankan;AE:Emirati";
	intLen = top.arrNationality.length;
	jsonNationality = ":";
	if (intLen > 0) {
		i = 0;
		do {
			jsonNationality += ";" + top.arrNationality[i][0] + ":"
					+ top.arrNationality[i][1];
			i++;
		} while (--intLen);
	}
}

/*
 * Re apply Tab Index
 */
UI_tabPassenger.applyTabIndexOrder = function() {
	/* Adult */
	var intLen = jsonPaxAdtObj.paxAdults.length;
	var i = 0;
	var intTI = 1;
	if (intLen > 0) {
		var i = 0;
		$("#tblPaxAdt").find("tr").each(function() {
			$(this).find("td").each(function() {
				$(this).find("select").each(function() {
					$("#" + this.id).setTabIndex(intTI++);
				});
				$(this).find("input").each(function() {
					$("#" + this.id).setTabIndex(intTI++);
				});
			});
		});
	}

	/* Infants */
	if (jsonPaxAdtObj.paxInfants != null) {
		intLen = jsonPaxAdtObj.paxInfants.length;
		i = 0;
		if (intLen > 0) {
			$("#tblPaxInf").find("tr").each(function() {
				$(this).find("td").each(function() {
					$(this).find("select").each(function() {
						$("#" + this.id).setTabIndex(intTI++);
					});
					$(this).find("input").each(function() {
						$("#" + this.id).setTabIndex(intTI++);
					});
				});
			});
		}
	}

	/* Buttons */
	$("#btnSetData").attr('tabIndex', intTI++);
	$("#btnPaxDetails").attr('tabIndex', intTI++);
	$("#btnTBA").attr('tabIndex', intTI++);
	$("#btnTBA").attr('tabIndex', intTI++);
	
	/* Contact Details */
	i = 0
	$.each(jsonPaxObj, function() {		
		$(jsonPaxObj[i].id).setTabIndex(intTI++);
		i++;
	});
	
	$("#btnLoadProfile").attr('tabIndex', intTI++);	
	$("#btnPHold").attr('tabIndex', intTI++);
	$("#selSalesPoint").attr('tabIndex', intTI++);
	$("#btnContinue").attr('tabIndex', intTI++);
	$("#btnWaitList").attr('tabIndex', intTI++);
	$("#btnPReset").attr('tabIndex', intTI++);
	$("#btnPCancel").attr('tabIndex', intTI++);
	
}

/*
 * Set dummy pax data. Will be used in dev mode
 */
UI_tabPassenger.setDummyData = function() {
	/* Adult */
	var objJPax = jsonPaxAdtObj.paxAdults;
	var intLen = objJPax.length;
	
	function PaxInfo(){
	    var titles = {adult:["MR", "MS", "MRS"], child:["MSTR", "MISS"], infant:["MSTR", "MISS"]};
	    var ALPHABET = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
	    var NAME_LENGTH = 3;
	    var paxMeta = {};
	    this.getPax = function(paxType){
	    	if(!paxMeta[paxType]){
	    		paxMeta[paxType] = {uniform: true, index:0, nameIndex:0};
	    	}
	    	var meta = paxMeta[paxType];
	    	var name = '';
	    	if(meta.uniform){
	    		for (var i = 0 ; i< NAME_LENGTH; i++){
	    			name += ALPHABET[meta.index];
	    		}
	    		if(meta.index == ALPHABET.length - 1){
	    			meta.index = 0;
	    			meta.nameIndex = NAME_LENGTH - 1;
	    			meta.uniform = false;
	    			meta.nameArr = [];
	    			for(var i = 0; i< NAME_LENGTH; i++){
	    				meta.nameArr[i] = 0;
	    			}
	    		} else {
	    			meta.index = meta.index+1;
	    		}
	    	} else {
	    		do {
	    			var found = false;
	    			for(var i = 1 ; i < meta.nameArr.length; i++){
	    				if(meta.nameArr[i] != meta.nameArr[0]){
	    					found = true;
	    					break;
	    				}
	    			}
	    			
	    			name  = '';
	    			for(var i = 0 ; i < meta.nameArr.length; i++){
	    				name += ALPHABET[meta.nameArr[i]];
	    			}
	    			
	    			for(var i = meta.nameArr.length -1 ; i >= 0 ; i--){
		    			if(meta.nameArr[i] == ALPHABET.length - 1){
	    					meta.nameArr[i] = 0;
	    				}else {
	    					meta.nameArr[i] += 1;
	    					break;
	    				}
	    			}
	    		} while (!found);
	    	}
	    	if(paxType == 'AD'){	
	            return {first:name, last:'ADULT', title:getAdultTitle()};
	    	} else if (paxType == 'CH'){
	    		return {first:name, last:'CHILD', title:getChildTitle()};
	    	} else{
	    		return {first:name, last:'INFANT', title:getInfantTitle()};
	    	}
	    	
	    }
	    
    	
    	function random(max){
    	    return Math.floor((Math.random()*max));
    	}
    	
    	function getAdultTitle(){
    	    return titles.adult[random(titles.adult.length)];
    	}
    	
    	function getChildTitle(){
            return titles.child[random(titles.child.length)];
        }
    	
    	function getInfantTitle(){
    		return titles.infant[random(titles.infant.length)];
    	}
    	
	}
	
	var paxInfo = new PaxInfo();
	
	var i = 0;
	if (intLen > 0) {
		do {
		    var pax = paxInfo.getPax(objJPax[i].displayAdultType)
		    $("#" + (i + 1) + "_displayAdultFirstName").val(pax.first);
		    $("#" + (i + 1) + "_displayAdultLastName").val(pax.last);
		    $("#" + (i + 1) + "_displayAdultTitle").val(pax.title);
			$("#" + (i + 1) + "_displayAdultNationality").val($("#" + (i + 1) + "_displayAdultNationality option:eq(2)").val());
			i++;
		} while (--intLen);
	}

	function getDOB(){
		var lastArrivalDate = UI_tabPassenger.extractFlightLastSegArrivalDate();
		var formattedLastArrivalDate = UI_tabPassenger.convertDateFormat(lastArrivalDate);
		var lastArrivalDateArr = formattedLastArrivalDate.split('/');

		var lastArrivalDay = lastArrivalDateArr[0];
		var lastArrivalMon = lastArrivalDateArr[1];
		var lastArrivalYear =  lastArrivalDateArr[2];
		var date = new Date((lastArrivalYear-paxValidation.infantAgeCutOverYears/2), lastArrivalMon-1, lastArrivalDay);

		return ('0' + date.getDate()).slice(-2) +'/'+('0' + (date.getMonth()+1)).slice(-2)+'/'+(''+date.getFullYear()).slice(-4);
	}
	
	/* Infant */
	objJPax = jsonPaxAdtObj.paxInfants;
	if (objJPax != null) {
		intLen = objJPax.length;
		i = 0;
		if (intLen > 0) {
			do {
			    var pax = paxInfo.getPax('IN');
				$("#" + (i + 1) + "_displayInfantFirstName").val(pax.first);
				$("#" + (i + 1) + "_displayInfantLastName").val(pax.last);
				$("#" + (i + 1) + "_displayInfantTravellingWith").val(i + 1);
				$("#" + (i + 1) + "_displayInfantTitle").val(pax.title);
				$("#" + (i + 1) + "_displayInfantNationality").val($("#" + (i + 1) + "_displayInfantNationality option:eq(2)").val());
				$("#" + (i + 1) + "_displayInfantDOB").val(getDOB());
				
				i++;
			} while (--intLen);
		}
	}    
	$("#selTitle").val("MR");
	$("#txtFirstName").val("ISA");
	$("#txtLastName").val("Developer");
	$("#txtCity").val("Cmb");
	$("#selCountry").val("LC");
	$("#txtPhoneCntry").val("758");
	$("#txtPhoneArea").val("11");
	$("#txtPhoneNo").val("1234567");
	$("#txtMobCntry").val("758");
	$("#txtMobArea").val("11");
	$("#txtMobiNo").val("1234567");
	$("#txtEmail").val("isa@gmail.com");
	$("#selBookingCategory").val("STD");
	
	if(contactConfig.firstName2.xbeVisibility ||
			contactConfig.lastName2.xbeVisibility ||
			contactConfig.phoneNo2.xbeVisibility){
		$("#selEmgnTitle").val("MR");
		$("#txtEmgnFirstName").val("ISA");
		$("#txtEmgnLastName").val("Developer");
		$("#txtEmgnPhoneCntry").val("94");
		$("#txtEmgnPhoneArea").val("112");
		$("#txtEmgnPhoneNo").val("112233");
	}
}

UI_tabPassenger.loadPaxContactDataFromOldReservation = function() {
	
	var adultsAndChildCount = jsonPaxAdtObj.paxAdults.length;
	var infantCount = jsonPaxAdtObj.paxInfants != null?jsonPaxAdtObj.paxInfants.length:0;

	if(oldResAdultPaxInfo != null && (adultsAndChildCount== oldResAdultCount + oldResChildCount) && infantCount == oldResInfantCount)  {			
		setAdultDetails(oldResAdultPaxInfo, false);
		if(oldResInfantPaxInfo != null) {
			setInfantDetails(oldResInfantPaxInfo, false);	
		}
	
	}
	
	if(oldResContactInfo != null) {
		$("#selTitle").val(oldResContactInfo.title);
		$("#txtFirstName").val(oldResContactInfo.firstName);
		$("#txtLastName").val(oldResContactInfo.lastName);
		$("#txtCity").val(oldResContactInfo.city);
		$("#selCountry").val(oldResContactInfo.country);
		$("#txtPhoneCntry").val(oldResContactInfo.phoneCountry);
		$("#txtPhoneArea").val(oldResContactInfo.phoneArea);
		$("#txtPhoneNo").val(oldResContactInfo.phoneNo);
		$("#txtMobCntry").val(oldResContactInfo.mobileCountry);
		$("#txtMobArea").val(oldResContactInfo.mobileArea);
		$("#txtMobiNo").val(oldResContactInfo.mobileNo);
		$("#txtEmail").val(oldResContactInfo.email);
		$("#txtStreet").val(oldResContactInfo.street);
		$("#txtAddress").val(oldResContactInfo.address);
		$("#selPrefLang").val(oldResContactInfo.preferredLang);
		$("#txtFaxCntry").val(oldResContactInfo.faxCountry);
		$("#txtFaxArea").val(oldResContactInfo.faxArea);
		$("#txtFaxNo").val(oldResContactInfo.faxNo);
		
		if(contactConfig.firstName2.xbeVisibility ||
				contactConfig.lastName2.xbeVisibility ||
				contactConfig.phoneNo2.xbeVisibility){
			$("#selEmgnTitle").val(oldResContactInfo.emgnTitle);
			$("#txtEmgnFirstName").val(oldResContactInfo.emgnFirstName);
			$("#txtEmgnLastName").val(oldResContactInfo.emgnLastName);
			$("#txtEmgnPhoneCntry").val(oldResContactInfo.emgnPhoneCountry);
			$("#txtEmgnPhoneArea").val(oldResContactInfo.emgnPhoneArea);
			$("#txtEmgnPhoneNo").val(oldResContactInfo.emgnPhoneNo);
		}
	}
		
}

// validate E-mail domain
UI_tabPassenger.checkEmailDomain = function(emailAdd){
	var data = {};
	data['domain'] = emailAdd.substring(emailAdd.indexOf("@")+1);		
	
	var retValue = false;
	$("#frmMakeBkg").ajaxSubmit({dataType: 'json', url:"checkEMailDomain.action", async:false,
		data:data,
		success: function(response){ 
		retValue = response.exists;
		if(!retValue){
			showERRMessage(raiseError("XBE-ERR-04","Email address"));
			$("#txtEmail").focus();			
		} 		
	},
	error : UI_commonSystem.setErrorStatus
	});
	
	return retValue;
}

UI_tabPassenger.ffidAvailability = function(field){
	var firstName1 = $("#"+field+"_displayAdultFirstName").val();
	var lastName1 = $("#"+field+"_displayAdultLastName").val();
	
	//Check for T B A bookings
	if(firstName1 == UI_tabPassenger.xbeTBA || lastName1 == UI_tabPassenger.xbeTBA){
		showERRMessage(raiseError("XBE-ERR-97"));
		return false;
	}
	
	firstName1 = firstName1.toLowerCase();
	lastName1 = lastName1.toLowerCase();

	var data = {};
	data['ffid'] = $.trim($("#"+field+"_displayFFID").val());
	data['firstName'] = $("#"+field+"_displayAdultFirstName").val();
	data['lastName'] = $("#"+field+"_displayAdultLastName").val();
	var firstName1 = $("#"+field+"_displayAdultFirstName").val().toLowerCase();
	var lastName1 = $("#"+field+"_displayAdultLastName").val().toLowerCase();
	var firstName2 = '';
	var lastName2 = '';
	
	var retValue = false;
	$("#frmMakeBkg").ajaxSubmit({
		dataType : 'json',
		url : "duplicateCheck!ffidCheck.action",
		async : false,
		data : data,
		async : false,
		success : function(response) {
			retValue = response.ffidAvailability;
			if (!retValue) {
				showERRMessage(raiseError("XBE-ERR-96"));
				$("#" + field + "_displayFFID").focus();
			} else {
				firstName2 = response.firstName.toLowerCase();
				lastName2 = response.lastName.toLowerCase();
				var nameValidation = UI_commonSystem.nameCompare(firstName1, lastName1, firstName2, lastName2);

				if (!nameValidation) {
					retValue = retValue && nameValidation;
					showERRMessage(raiseError("XBE-ERR-95"));
					$("#" + field + "_displayAdultFirstName").focus();
				} else {
					if (response.isEmailConfirmed == 'N') {
						if (!UI_tabPassenger.pendingFFIDPopupDisplayed) {
							UI_tabPassenger.pendingFFIDPopupDisplayed = true;
							if (!confirm(raiseError("XBE-WARN-07"))) {
								UI_tabPassenger.pendingFFIDPopupDisplayed = false;
								UI_commonSystem.hideProgress();
								retValue = false;
							}
						}
					}
				}
			}
		},

		error : UI_commonSystem.setErrorStatus
	});

	return retValue;
}

UI_tabPassenger.validateZipCode = function(field){
	var valid = "0123456789-";
	var hyphencount = 0;
	
	if (field.length!=5 && field.length!=10) {
		return false;
	}
	for (var i=0; i < field.length; i++) {
		var temp = "" + field.substring(i, i+1);
		if (temp == "-"){
			hyphencount++;
		}		
		if (valid.indexOf(temp) == "-1") {
			return false;
		}
		if ((hyphencount > 1) || ((field.length==10) && ""+field.charAt(5)!="-")) {
			return false;
		}
	}
	return true;
}

UI_tabPassenger.getAgentPreferedLang = function(agentCode){
	for( var i = 0 ; i < top.arrAllTravelAgents.length ; i++){
		
		if(top.arrAllTravelAgents[i][0] == agentCode){							
			return top.arrAllTravelAgents[i][4];
		}
	}
}

UI_tabPassenger.convertDateFormat = function(ddMMMyy_date){
	
	if(ddMMMyy_date.indexOf(" ") > 0){
		//Wed 20Aug14
		ddMMMyy_date = ddMMMyy_date.split(" ")[1];
	}
	
	var day = ddMMMyy_date.substring(0,2);
	var m = ddMMMyy_date.substring(2,5);
	var y = ddMMMyy_date.substring(5,ddMMMyy_date.length);
	var year = '20'+y;
	
	var mon = (m == 'Jan' || m == 'jan') ? '01' :
				  (m == 'Feb' || m == 'feb') ? '02' :
					  (m == 'Mar' || m == 'mar') ? '03' :
						  (m == 'Apr' || m == 'apr') ? '04' :
							  (m == 'May' || m == 'may') ? '05' :
								  (m == 'Jun' || m == 'jun') ? '06' :
									  (m == 'Jul' || m == 'jul') ? '07' :
										  (m == 'Aug' || m == 'aug') ? '08' :
											  (m == 'Sep' || m == 'sep') ? '09' :
												  (m == 'Oct' || m == 'oct') ? '10' :
													  (m == 'Nov' || m == 'nov') ? '11' : '12';
	
	
	var formatedDate = day + '/' + mon + '/' + year;
	return formatedDate;
}

UI_tabPassenger.checkInfantMinAge = function(dateOfBirth, strAgeDate,infantMinAgeDays) {
	var sD = Number(dateOfBirth.substring(0,2));
	var sM = Number(dateOfBirth.substring(3,5));
	var sY = Number(dateOfBirth.substring(6,10));

	var eD = Number(strAgeDate.substring(0,2));
	var eM = Number(strAgeDate.substring(3,5));
	var eY = Number(strAgeDate.substring(6,10));

	var dob = new Date(sY, sM - 1, sD);
	var depDate = new Date(eY, eM - 1, eD);
	
	var one_day=1000*60*60*24
	var dobInMilSec = dob.getTime();
	var depDateInMilSec = depDate.getTime();
	
	var noOfDays = -1;
	
	if(depDateInMilSec > dobInMilSec) {
		noOfDays = Math.ceil((depDateInMilSec - dobInMilSec)/ (one_day));
	} 
	else {		
		noOfDays = 0;		
	}
	if(noOfDays >= 0 && noOfDays <=infantMinAgeDays) {
		return false;
	} else {
		return true;
	}

}

UI_tabPassenger.extractFlightLastSegOutDate = function() {
	
	var objFlightData = jsonFltDetails;	
	var segCount = objFlightData.length;
	var departureDate = UI_tabSearchFlights.strOutDate;
	if(segCount != null && segCount > 0){
		departureDate = objFlightData[segCount-1].departureDate;
	}	
	return departureDate;
}

UI_tabPassenger.extractFlightFirstSegOutDate = function() {
	
	var objFlightData = jsonFltDetails;	
	var segCount = objFlightData.length;
	var departureDate = UI_tabSearchFlights.strOutDate;
	if(segCount != null && segCount > 0){
		departureDate = objFlightData[0].departureDate;
	}
	return departureDate;
}

UI_tabPassenger.extractFlightLastSegArrivalDate = function() {
	var objFlightData = jsonFltDetails;	
	var segCount = objFlightData.length;
	var arrivalDate = UI_tabSearchFlights.strRetDate;
	if(segCount != null && segCount > 0){
		arrivalDate = objFlightData[segCount-1].arrivalDate;
	}
	return arrivalDate;
}

UI_tabPassenger.extractFlightFirstSegOutDateObj = function() {
	
	var objFlightData = jsonFltDetails;	
	var segCount = objFlightData.length;
	var departureDate;
	if(segCount != null && segCount > 0){
		departureDate = objFlightData[0].departureDateLong;
		return new Date(departureDate);
	}
	return new Date();
}


UI_tabPassenger.getFlightLsttSegmentDeptDate = function(){
	var lstSegDate = UI_tabPassenger.extractFlightLastSegOutDate();
	var flightLastSegOutDate = UI_tabPassenger.convertDateFormat(lstSegDate);
	
	flightLsttSegmentDeptDate = flightLastSegOutDate;
}

UI_tabPassenger.isDomesticFlightExist = function() {
	var flightList = jQuery.parseJSON(UI_tabPassenger.getFlightRPHList());
	for(var i = 0;i < flightList.length;i++){
		if(flightList[i].domesticFlight == true){
			return true;
		}
	}
	return false;
}


UI_tabPassenger.loadPaxDetailsOnClick = function(){
	pupulateFrequentCustomerPopupInitialize();
	$("#populateFrequentCustomerPopUp").dialog('open');
}

UI_tabPassenger.saveFrequentCustomer = function(){
	
	if(!saveFrequentCustomerPopupvalidate($("#paxType").val())){
		return false;
	}
	
	data = {};
	
	data['agentFrequentCustomer.paxType'] = $("#paxType").val();
	data['agentFrequentCustomer.title'] = $("#title").val();
	data['agentFrequentCustomer.firstName'] = $("#firstName").val();
	data['agentFrequentCustomer.lastName'] = $("#lastName").val();
	data['agentFrequentCustomer.email'] = $("#email").val();
	data['dateOfBirth'] = $("#dateOfBirth").val();
	data['passportExpiryDate'] = $("#passportExpDate").val();
	data['agentFrequentCustomer.nationalityCode'] = $("#nationality").val();
	data['agentFrequentCustomer.passportNumber'] = $("#passportNumber").val();
	data['operation'] = "save";
	
	$.ajax({
        url : 'agentFrequentCustomer!saveOrUpdateFrequentCustomer.action',
        data: data,
        dataType:"json",
        beforeSend :ShowPopProgress(),
        type : "POST",		           
		success: function(response){	 
			HidePopProgress();
			alert(response.message);	
			saveFrequentCustomerPopupclearFields();
			$("#saveFrequentCustomerPopUp").dialog('close');
        },
        error: function(reponse){
        	HidePopProgress();
        	showERRMessage(response.message);
        }
     });
}

saveFrequentCustomerPopupvalidate = function(paxType){
	
	if(paxType != "IN"){
		
		jsonCustomerObj = [{
			id : "#title",
			desc : "Title",
			required : true
		}, {
			id : "#email",
			desc : "Email Address",
			required : true 
		}];
		
		if (!UI_commonSystem.validateMandatory(jsonCustomerObj)) {
			return false;
		}
		
		if (!checkEmail($("#email").val())) {
			showERRMessage("Email address is wrong");
			$("#email").focus();
			return false;
		}
		
	}
	
	jsonCustomerObj = [{
		id : "#firstName",
		desc : "First Name",
		required : true
	}, {
		id : "#lastName",
		desc : "Last Name",
		required : true
	}, {
		id : "#nationality",
		desc : "Nationaliy Of The Customer",
		required : true
	}, {
		id : "#paxType",
		desc : "Pax Type",
		required : true 
	}, {
		id : "#dateOfBirth",
		desc : "Date of Birth",
		required : true 
	}];
	
	if (!UI_commonSystem.validateMandatory(jsonCustomerObj)) {
		return false;
	}
	
	if($.trim($("#dateOfBirth").val()) != ""){
		if(!dateValidDate($("#dateOfBirth").val())){
			showERRMessage("Wrong Date for Birth Day");
			$("#dateOfBirth").focus();
			return false;
		}
	}
	
	if($.trim($("#passportExpDate").val()) != ""){
		
		if($.trim($("#passportNumber").val()) == ""){
			showERRMessage("Passport expiry date can't be added without the passport number");
			$("#passportNumber").focus();
			return false;
		}
		
		if(!dateValidDate($("#passportExpDate").val())){
			showERRMessage("Wrong Date for Passport Expiry Date");
			$("#passportExpDate").focus();
			return false;
		}
		
	}
	
	return true;
}

saveFrequentCustomerPopupclearFields = function(){
	$("#title").val("");
	$("#firstName").val("");
	$("#lastName").val("");
	$("#email").val("");
	$("#dateOfBirth").val("");
	$("#passportExpDate").val("");
	$("#nationality").val("");
	$("#passportNumber").val("");
}

function ShowPopProgress() {
	setVisible("divLoadMsg", true);
}

function HidePopProgress() {
	setVisible("divLoadMsg", false);
}

saveFrequentCustomerPopupdateOnBlur = function(id){
	if(id == 0){
		dateChk("dateOfBirth");
	}else if(id == 1){
		dateChk("passportExpDate");
	}
}

saveFrequentCustomerPopuponlyAlphaWhiteSpace = function(objC){
	
	if (!isAlphaWhiteSpace(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}
	
}

saveFrequentCustomerPopupInitialize = function(){
	
	$("#btnSave").unbind();
	$("#btnClose").unbind();
	$("#dateOfBirth").unbind();
	$("#passportExpDate").unbind();
	$("#firstName").unbind();
	$("#lastName").unbind();
	
	$(function() {
	    $( "#dateOfBirth" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      yearRange: "-99:+0",
	      maxDate: "-2Y",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      changeMonth: true,
	      changeYear: true
	    });
	    
	    $( "#dateOfBirth" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	    
	  });
	
	$(function() {
	    $( "#passportExpDate" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      yearRange: "0:+20",
	      minDate: "0",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      changeMonth: true,
	      changeYear: true
	    });
	    
	    $( "#passportExpDate" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	    
	  });
	
	$("#btnSave").click(function(){UI_tabPassenger.saveFrequentCustomer();});
	$("#btnClose").click(function(){$("#saveFrequentCustomerPopUp").dialog('close');});
	
	$("#dateOfBirth").blur(function(){saveFrequentCustomerPopupdateOnBlur(0)});
	$("#passportExpDate").blur(function(){saveFrequentCustomerPopupdateOnBlur(1)});
	
	$("#firstName").keyup(function(event) {saveFrequentCustomerPopuponlyAlphaWhiteSpace(this);});
	$("#lastName").keyup(function(event) {saveFrequentCustomerPopuponlyAlphaWhiteSpace(this);});
	
	$.each(top.arrNationality ,function(key, value) 
			{
			    $("#nationality").append('<option value=' + value[0] + '>' + value[1] + '</option>');
			});
	
	$.each(top.arrTitle ,function(key, value) 
			{
			    $("#title").append('<option value=' + value[0] + '>' + value[1] + '</option>');
			});
}

pupulateFrequentCustomerPopupInitialize = function(){
	$("#populate_popup_btnSearch").unbind();
	$("#btnPopulate").unbind();
	$("#populate_popup_btnClose").unbind();
	$("#populate_popup_btnSearch").click(function(){createFrequentCustomerGrid();});
	$("#btnPopulate").click(function(){populateCustomerDetails();});
	$("#populate_popup_btnClose").click(function(){$("#populateFrequentCustomerPopUp").dialog('close');});

	createFrequentCustomerGrid();		
}

createFrequentCustomerGrid = function(){
	var nationalityFomatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			for(var i=0; i<top.arrNationality.length; i++) {
				if(top.arrNationality[i][0]==cellVal){
					treturn = top.arrNationality[i][1];
					break
				}
			}
		}
		return treturn;
	}	
	
	var dateFormatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			treturn = $.datepicker.formatDate('dd-M-yy', new Date(cellVal.split('T')[0]));
		}
		return treturn;
	}
	
	$("#frequentCustomerProfilesGridContainer").empty().append('<table id="frequentCustomerProfilesGrid"></table><div id="frequentCustomerProfilesPages"></div>')
	var data = {};
	
	data['agentFrequentCustomer.firstName'] = $("#search_firstName").val();
	data['agentFrequentCustomer.lastName'] = $("#search_lastName").val();
	data['agentFrequentCustomer.email'] = $("#search_email").val();
	
	var frequentCustomerProfilesGrid = $("#frequentCustomerProfilesGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'agentFrequentCustomer!searchFrequentCustomer.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :ShowPopProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(frequentCustomerProfilesGrid[0], "gridData", mydata.agentFrequentCustomerList);
			            		  frequentCustomerProfilesGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  HidePopProgress();
			              }
			           }
			        });
			    },
		    height: 150,
		    colNames: ['Pax Type', 'Title', 'First Name', 'Last Name', 'Date of Birth', 'Nationality', 'Email', 'passport Number', 'passport Expiry Date', 'frequentCustomerId', 'nationalityCode'],
		    colModel: [
		        {name: 'paxType',index: 'paxType',jsonmap:'agentFrequentCustomerDTO.paxType', width: 60},
			    {name: 'title',index: 'title',jsonmap:'agentFrequentCustomerDTO.title', width: 30},
			    {name: 'firstName', index: 'firstName',jsonmap:'agentFrequentCustomerDTO.firstName', width: 110, sorttype: "string"},
			    {name: 'lastName',index: 'lastName',jsonmap:'agentFrequentCustomerDTO.lastName', width: 100, sorttype: "string"},
			    {name: 'dateOfBirth',index: 'dateOfBirth', jsonmap:'agentFrequentCustomerDTO.dateOfBirth', formatter:dateFormatter},
			    {name: 'nationality',index: 'nationality', jsonmap:'agentFrequentCustomerDTO.nationalityCode', width: 100, formatter:nationalityFomatter},
			    {name: 'email', index: 'email',jsonmap:'agentFrequentCustomerDTO.email' ,width: 140},
			    {name: 'passportNumber',index: 'passportNumber', jsonmap:'agentFrequentCustomerDTO.passportNumber', width: 100},
				{name: 'passportExpDate',index: 'passportExpDate', jsonmap:'agentFrequentCustomerDTO.passportExpiryDate', width: 120, formatter:dateFormatter},
				{name: 'frequentCustomerId',index: 'frequentCustomerId', jsonmap:'agentFrequentCustomerDTO.frequentCustomerId', hidden: true},
				{name: 'nationalityCode',index: 'nationalityCode', jsonmap:'agentFrequentCustomerDTO.nationalityCode', hidden: true},
		    ],
		    caption: "Frequnet Customer Details",
		    rowNum : 20,
			viewRecords : true,
			width : 980,
			pager: jQuery('#frequentCustomerProfilesPages'),
			jsonReader: { 
				root: "agentFrequentCustomerList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			multiselect: true,
	        multiboxonly: true,
		    
	}).navGrid("#frequentCustomerProfilesPages",{refresh: true, edit: false, add: false, del: false, search: false});
}

populateCustomerDetails = function(){

	var date;

	var adults = 0;
	var childs = 0;
	var infants = 0;
	
	var addedAdults = 0;
	var addedChilds = 0;
	
	var adultArray = [];
	var childArray = [];
	var infantArray = [];
	
	var position = 1;
	
	var adultGridRecords;
	var adultTableName;
	var infantTableName;
	
	var selectedRows = $("#frequentCustomerProfilesGrid").getGridParam('selarrrow');
	
	for(i = 0 ; i < selectedRows.length ; i++){
		
		var rowData = $("#frequentCustomerProfilesGrid").getRowData(selectedRows[i]);
		if(rowData['paxType'] == 'AD'){
			adultArray.push(rowData);
		}else if(rowData['paxType'] == 'CH'){
			childArray.push(rowData);
		}else if(rowData['paxType'] == 'IN'){
			infantArray.push(rowData);
		}
	
	}

	adultGridRecords = $("#tblPaxAdt").getGridParam('records');
	infants = $("#tblPaxInf").getGridParam('records');

	for(i = 1 ; i <= adultGridRecords ; i++){
		var rowData = $("#tblPaxAdt").getRowData(i);
		if(rowData['displayAdultType'] == 'AD'){
			adults++;
		}else if(rowData['displayAdultType'] == 'CH'){
			childs++;
		}
	}
	
	$.each(adultArray , function(index , value){
		if(position <= adults){

			$("#"+position+"_displayAdultTitle").val(value['title']);
			$("#"+position+"_displayAdultFirstName").val(value['firstName']);
			$("#"+position+"_displayAdultLastName").val(value['lastName']);
			$("#"+position+"_displayAdultNationality").val(value['nationalityCode']);
					
			date = $.datepicker.parseDate("dd-M-yy",  value['dateOfBirth']);
			$("#"+position+"_displayAdultDOB").val($.datepicker.formatDate('dd/mm/yy', date));
					
			if(value['passportExpDate'] != "" && value['passportExpDate'] != null){
				date = $.datepicker.parseDate("dd-M-yy",  value['passportExpDate']);
				jsonPaxAdtObj.paxAdults[position].displayPnrPaxCatFOIDExpiry = $.datepicker.formatDate('dd/mm/yy', date);
			}
					
			jsonPaxAdtObj.paxAdults[position-1].displayPnrPaxCatFOIDNumber = value['passportNumber'];

			position++;
		}		
	});
	
	position = adults + 1;
	
	$.each(childArray , function(index , value){
		if(position <= adultGridRecords){

			$("#"+position+"_displayAdultTitle").val(value['title']);
			$("#"+position+"_displayAdultFirstName").val(value['firstName']);
			$("#"+position+"_displayAdultLastName").val(value['lastName']);
			$("#"+position+"_displayAdultNationality").val(value['nationalityCode']);
					
			date = $.datepicker.parseDate("dd-M-yy",  value['dateOfBirth']);
			$("#"+position+"_displayAdultDOB").val($.datepicker.formatDate('dd/mm/yy', date));
					
			if(value['passportExpDate'] != "" && value['passportExpDate'] != null){
				date = $.datepicker.parseDate("dd-M-yy",  value['passportExpDate']);
				jsonPaxAdtObj.paxAdults[position].displayPnrPaxCatFOIDExpiry = $.datepicker.formatDate('dd/mm/yy', date);
			}
					
			jsonPaxAdtObj.paxAdults[position-1].displayPnrPaxCatFOIDNumber = value['passportNumber'];

			position++;
		}	
	});
	
	position = 1;
	
	$.each(infantArray , function(index , value){
		if(position <= infants){

			$("#"+position+"_displayInfantFirstName").val(value['firstName']);
			$("#"+position+"_displayInfantLastName").val(value['lastName']);
			$("#"+position+"_displayInfantNationality").val(value['nationalityCode']);
					
			date = $.datepicker.parseDate("dd-M-yy",  value['dateOfBirth']);
			$("#"+position+"_displayInfantDOB").val($.datepicker.formatDate('dd/mm/yy', date));
			position++;
		}		
	});

}
/*
 * ------------------------------------------------ end of File
 * ----------------------------------------------
 */

