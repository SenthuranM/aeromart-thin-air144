	/*
	*********************************************************
		Description		: XBE Make Reservation - Payment Tab
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/
	
	var jsonPaySumObj = ({currency:"AED" ,
							totalFare:0.00 ,
							totalTaxSurCharges:0.00 ,
							modificationCharges:0.00 ,
							handlingCharges:0.00 ,
							ticketPrice:0.00 ,
							paid:0.00 ,
							balanceToPay:0.00,
							balanceToPaySelcur:0.00,
							selectedCurrency:""
						});
							
	var jsonPaxPayObj = [];
	var vouchersRedeemed = false;
	var currencyExchangeRate = {};
	var voucherIDs = new Array();
	var voucherHTMLID = new Array();
	var countryCurrExchangeRate = {};
	UI_tabPayment.creditPaxPayObj = null;
	var jsonEnableItineryPrint = null;
	var removeElementG="";	
	var indexG="";
	var offlinePaymentOptions = top.arrOfflineIPGs;
	var totalRedeemedVoucherAmount = 0;
	
	/*
	 * Passenger Tab
	 */
	function UI_tabPayment(){}
	
	UI_tabPayment.intBalToPay = 0;
	
	UI_tabPayment.strCashCode = "CASH";
	UI_tabPayment.strCCCode = "CRED";
	UI_tabPayment.strAccCode = "ACCO";
	UI_tabPayment.strBspCode = "BSP";
	
	UI_tabPayment.isTabLoaded = false;
	
	UI_tabPayment.isReservation = false;
	UI_tabPayment.isModify = false;
	UI_tabPayment.isSegModify = false;
	UI_tabPayment.isRequote = false;
	UI_tabPayment.isAddModifyAnci = false;
	UI_tabPayment.isAddInfant = false;
	UI_tabPayment.isAddSegment = false;
	UI_tabPayment.isRequoteAddSegment = false;
	UI_tabPayment.isRequoteSegModify = false;
	
	
	UI_tabPayment.capturePayRef = false;
	UI_tabPayment.payRefMandatory = false;
	UI_tabPayment.strPaymentMsg = "";
	UI_tabPayment.actPayMethodCash = false;
	UI_tabPayment.actPayMethodCC = false;
	UI_tabPayment.actPayMethodAcc = false;
	UI_tabPayment.actPayMethodBsp = false;
	
	UI_tabPayment.airportMessage = null;
	UI_tabPayment.airportCardPaymentMessages = null; 
	UI_tabPayment.paymentProcessed = false;
	UI_tabPayment.autoCancelPopupMessage = null;
	UI_tabPayment.pickBINPromotionSucess = false;
	UI_tabPayment.promotionBIN = null;
	UI_tabPayment.isCsPnr = false;
	UI_tabPayment.pnr = "";
	UI_tabPayment.showForceConfirm =false;
	
	UI_tabPayment.brokerType = "";
	
	UI_tabPayment.ready = function(){
	
		// if tab is not yet loaded load it
		if(!UI_tabPayment.isTabLoaded) {
			
			$("#btnPayReset").decorateButton();
			$("#btnPayCancel").decorateButton();
			$("#btnPayContinue").decorateButton();
			$("#btnCRConfirm").decorateButton();
			$("#btnCRReset").decorateButton();
			$("#btnCRCancel").decorateButton();
			
			$("#btnAutoCancelOk").decorateButton();
			$("#btnAutoCancelNo").decorateButton();
			
			$('#btnAutoCancelOk').click(function(){ 
				UI_commonSystem.hideProgress();
				$('#divAutoCancelConfirm').hide();
				UI_tabPayment.payConfirmExecute();
				
			});
			$('#btnAutoCancelNo').click(function(){
				UI_commonSystem.hideProgress();
				$('#divAutoCancelConfirm').hide();
								
			});
			
			if(DATA_ResPro.initialParams.promoCodeEnabled && UI_tabPayment.isReservation){
				$('#reloadPromotion').show();
				$('#reloadPromotions').decorateButton();
				$("#reloadPromotionsBtn").click(function(){
					UI_tabPayment.reloadPromotions();
				});
			}
			
			$("#btnPayConfirm").decorateButton();
			$("#btnPayConfirm").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.payConfirmOnClick();});
			
			$("#btnPayReset").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.resetOnClick();});
			
			$("#btnPayCancel").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.cancelOnClick();});

			$("#btnPayContinue").click(function(){
			  $("#btnPayContinue").prop("disabled", true);
				$("#btnRedeem").prop('disabled', true);
				$("#btnVoucherOtpConfirm").prop('disabled', true);
				$("#btnVoucherOtpResend").prop('disabled', true);
				$("#btnUseAnotherVoucher").prop('disabled', true);
				$("#chkVoucherRedeem").prop('disabled', true);
			  	UI_commonSystem.pageOnChange();
				$("#txtPayMode").val("");
				UI_tabPayment.continueOnClick();
				$("#btnPayContinue").prop("disabled", false);
			});
			
			$("#radPaymentTypeC").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.onPaymentTypeClick(); UI_tabPayment.paymentTypeCClick();});
			$("#radPaymentTypeOffline").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.onPaymentTypeClick(); UI_tabPayment.paymentTypeOfflineClick();});
			$("#radPaymentTypeA").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.onPaymentTypeClick(); UI_tabPayment.paymentTypeAClick();});
			$("#radPaymentTypeR").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.onPaymentTypeClick(); UI_tabPayment.paymentTypeRClick();});
			$("#radPaymentTypeBsp").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.onPaymentTypeClick(); UI_tabPayment.paymentTypeBspClick();});
			$("#selAgents").change(function(){UI_commonSystem.pageOnChange();UI_tabPayment.agentselected();});
			
			$(document).on("click","input[name='chkAllPax']", function(e){UI_commonSystem.pageOnChange();UI_tabPayment.selectUnSelectAllClick(e);});
					
			$("#btnRedeem").click(function(){UI_tabPayment.redeemVouchers(false);});
			$("#chkVoucherRedeem").change(function(){UI_tabPayment.onRedeemCheckChange();});
			$("#creditCardDet").hide();
			$("#btnCRCancel").click(function() { UI_commonSystem.pageOnChange();UI_tabPayment.cancelCROnClick();});
			$("#btnCRConfirm").click(function() { UI_commonSystem.pageOnChange();UI_tabPayment.confirmOnClick();});
			$("#btnCRReset").click(function() {UI_commonSystem.pageOnChange(); UI_tabPayment.resetCCOnClick();});
			$("#chkPrint").click(function() {
			        $("#hdnPrintItineraryLang").val($("#selLang").val());
			});
			
			window.onbeforeunload = function (event) {
				UI_tabPayment.unblockRedeemedVouchers();
			};
			
			$('#btnGrpBookingValidate').click(function(){ 
				UI_tabPayment.validateBookingRequest();
			});
			$("#grpBookingVal").text('');
			$("#groupBookingReference").change(function(){
				$("#grpBookingVal").text('');
				$("#minimumPayment").val('0');
				$('#paymentCash').hide();
				$('#paymentCreditCard').hide();
			});
			$("#groupBookingReference").numeric({allowDecimal:false});
			UI_tabPayment.isTabLoaded = true;
			
		}

		if (DATA_ResPro.initialParams.forceConfirm || DATA_ResPro.initialParams.partialPaymentModify){
			$("#btnPayForce").decorateButton();
			$("#btnPayForce").click(function(){UI_commonSystem.pageOnChange();UI_tabPayment.payForceOnClick();});
		}

		if(!UI_tabPayment.isReservation || UI_tabPayment.isModify) {
			offlinePaymentOptions = top.arrOfflineIPGsModifyEnabled;
		}
		
		UI_tabPayment.fillDropDownData();
		
		if(DATA_ResPro.initialParams.cashPayments){
			$("#rowCashPayments").show();
		} else {
			$("#rowCashPayments").hide();
		}
		
		if(DATA_ResPro.initialParams.creditPayments && top.arrIPGs.length != 0){
			$("#rowCardPayments").show();
		} else {
			$("#rowCardPayments").hide();
		}
		
		if(DATA_ResPro.initialParams.onAccountPayments){
			$("#rowAccPayments").show();
		} else {
		    $("#rowAccPayments").hide();
		}
		
		if(DATA_ResPro.initialParams.onAccountMultiAgentPayments){
			$("#rowAccPayments2").show();
		} else {
		    $("#rowAccPayments2").hide();
		}
		
		
		if(DATA_ResPro.initialParams.bspPayments){
			$("#rowBspPayments").show();
		} else {
			$("#rowBspPayments").hide();
		}
		
		if(DATA_ResPro.reservationStatus == undefined && DATA_ResPro.reservationInfo != undefined){
			DATA_ResPro.reservationStatus = DATA_ResPro.reservationInfo.status;
		}
		
		if(DATA_ResPro.initialParams.offlinePayments && offlinePaymentOptions.length != 0){
			$("#rowOfflinePayments").show();
		} else {
			$("#rowOfflinePayments").hide();
		}
		
		if(!UI_tabPayment.isReservation){
			if(jsonBkgInfo != null && jsonBkgInfo.status == 'CNF'){
				$("#rowOfflinePayments").hide();
			} else if ((UI_tabPayment.isSegModify || UI_tabPayment.isAddModifyAnci || 
			        UI_tabPayment.isAddSegment || UI_tabPayment.isRequote) && (DATA_ResPro.reservationStatus == 'CNF' || (DATA_ResPro.reservationInfo != undefined && DATA_ResPro.reservationInfo.status == 'CNF'))){
				$("#rowOfflinePayments").hide();
			}
		} else {
			if(jsonFareQuoteSummary.onHoldRestricted && (
					UI_tabSearchFlights.getSelectedSystem() == 'INT' || !DATA_ResPro.initialParams.overrrideOnHoldRestricted)){
				$("#rowOfflinePayments").hide();
			}
		}
		
		
		if(DATA_ResPro.reservationStatus == 'OHD'&& (DATA_ResPro.initialParams.modifyOnholdConfirm || DATA_ResPro.initialParams.forceConfirm)){
			UI_tabPayment.handleForceConfirmBehavior(false);
		} else if((DATA_ResPro.reservationStatus == 'CNF' || (DATA_ResPro.reservationInfo != undefined && DATA_ResPro.reservationInfo.status == 'CNX')) 
				&& DATA_ResPro.initialParams.partialPaymentModify){
			UI_tabPayment.handleForceConfirmBehavior(true);
		}else if(!DATA_ResPro.initialParams.forceConfirm){
			$("#btnPayForce").hide();
			UI_tabPayment.showForceConfirm =false;
		}
		
		if(DATA_ResPro.initialParams.agentCommissionEnabled){
			$("#lblEntComm").show();
			$("#divPAgentCommission").show();
			$("#lblPenComm").show();
			$("#divPendingAgentCommission").show();
			$(".pendingComm").show();
		} else {
			$("#lblEntComm").hide();
			$("#divPAgentCommission").hide();
			$("#lblPenComm").hide();
			$("#divPendingAgentCommission").hide();
			$(".pendingComm").hide();
		}
		
		if($.trim($("#txtEmail").val()) == ""){
			$('#trEmail').find('.tdEmail').hide();
		}else{
			$('#trEmail').find('.tdEmail').show();
		}
		UI_tabPayment.paymentProcessed = false;
		/* TODO remove this */
		UI_tabPayment.paymentDetailsOnLoadCall();
		$("#txtPayAmt").disable();
		
		/* Remove the email/print itinerary option from the modification process */
		if (UI_tabPayment.isModify || UI_tabPayment.isSegModify || 
				UI_tabPayment.isAddModifyAnci || UI_tabPayment.isAddInfant || 
				UI_tabPayment.isAddSegment || UI_tabPayment.isRequote) 	{				
			$('#trEmail').hide();			
		}
		
		UI_tabPayment.strPaymentMsg = "";
		$("#txtPayMode").val("");
		$("#btnPayContinue").enable();
		UI_tabPayment.intBalToPay = 0;
		UI_commonSystem.pageOnChange();
		UI_PageShortCuts.initilizeMetaDataForPaymentScreen();
		
		// to show the print option if its already hidden
		$("#chkPrint").show();
        $("#tdPrint").html("Print Itinerary");
        UI_tabPayment.validationForEnablingConfirmButton();
        UI_tabPayment.hidePaymentOptions();
        $("#hdnPrintItineraryLang").val($("#selLang").val());
		if(voucherHTMLID.length == 0){
			voucherHTMLID.push("voucherID_0");
		}
        $("#trVoucher").hide();
        if($("#chkVoucherRedeem").is(":checked")){
        	 $("#trVoucher").show();
        }
        $("#voucherID_0").numeric();

		$('#voucherID_0').keypress(function(event) {
			UI_tabPayment.peventUnnecessarySubmitOnEnterClick();
		});

		$('#voucherOtpId').keypress(function(event) {
			UI_tabPayment.peventUnnecessarySubmitOnEnterClick();
		});

        if(vouchersRedeemed){
        	UI_tabPayment.redeemVouchers(true);
        }
        if(voucherHTMLID.length == 0){
    		voucherHTMLID.push("voucherID_0");
    	}
        $("#trVoucherOtp").hide();
        $("#voucherOtpId").numeric();
	}
	

	UI_tabPayment.peventUnnecessarySubmitOnEnterClick = function() {
		if (event.keyCode == 13 || event.which == 13) {
			event.preventDefault();
		}
	}
	/*
	 * If reservation is entitled to a promotion which allow only credit card
	 * payment, this method use to hide other payment options
	 */
	UI_tabPayment.hidePaymentOptions = function(){
		if(UI_tabPayment.isBINPromotion()){
			$("#rowCashPayments").hide();
			$("#rowAccPayments").hide();
			$("#rowAccPayments2").hide();
			$("#rowBspPayments").hide();
			$("#rowOfflinePayments").hide();
		}
	}
	
	UI_tabPayment.isBINPromotion = function(){
		var applicableBINs = UI_tabPayment.getPromotionApplicableBINs();
		if(applicableBINs != null && (applicableBINs.length > 0)){
			return true;
		}
		
		return false;
	}
	
	UI_tabPayment.getPromotionApplicableBINs = function(){
		var applicableBINs = null;
		if(UI_tabPayment.isReservation){
			var discountInfo = UI_tabSearchFlights.appliedFareDiscountInfo;
			if(discountInfo != null && discountInfo.promotionId != null){
				applicableBINs = discountInfo.applicableBINs;
			}
		} else if(UI_tabPayment.isModify){
			applicableBINs = UI_reservation.applicableBINs;
		}
		
		return applicableBINs;
	}
	
	UI_tabPayment.onPaymentTypeClick = function(){
		if ($("#radPaymentTypeBsp").attr('checked')){
			$("#divCCurrTotalBSP").show();
			$("#divPTotAmount").hide();
			$("#rowBSPFee").show();
		} else {
			$("#divCCurrTotalBSP").hide();
			$("#divPTotAmount").show();
			$("#rowBSPFee").hide();	
		}
	}
	
	UI_tabPayment.selectUnSelectAllClick = function(e){
		for(var pl =0; pl < jsonPaxPayObj.length;pl++){
			var y = pl+1;
			var rowTR = $("#"+ y + "select").closest('tr');
			$("#"+ y + "_select").click();
		}
	};
	
	UI_tabPayment.validationForEnablingConfirmButton = function() {

		if (UI_tabPayment.isSegModify || UI_tabPayment.isAddSegment ||  UI_tabPayment.isRequote) {

			var reservationStatus = "";
			if (UI_tabPayment.isSegModify) {
				reservationStatus = DATA_ResPro.modifySegments.status;
			} else {
				reservationStatus = DATA_ResPro.reservationInfo.status;
			}

			if (jsonFareQuoteSummary.onHoldRestricted
					&& reservationStatus == 'OHD') {
				if (!DATA_ResPro.initialParams.overrrideOnHoldRestricted) {
					$("#btnPayConfirm").disable();
				} else {
				    if(DATA_ResPro.initialParams.modifyOnholdConfirm){
				        $("#btnPayConfirm").enable();
				    }
				}
			} else if (!jsonFareQuoteSummary.onHoldRestricted){
				UI_tabPayment.showHideBasedOnResStatus(reservationStatus);
			}
		} else if (UI_tabPayment.isAddModifyAnci) {
			UI_tabPayment.showHideBasedOnResStatus(DATA_ResPro.reservationStatus);
		}
	}
	
	UI_tabPayment.handleForceConfirmBehavior = function(blnShowForceConfirm){
		if(blnShowForceConfirm){
			$("#btnPayForce").enable();
			$("#btnPayForce").show();
			$("#btnPayConfirm").hide();
			UI_tabPayment.showForceConfirm =true;
		}else{
			$("#btnPayConfirm").enable();
			$("#btnPayConfirm").show();
			$("#btnPayForce").hide();
			UI_tabPayment.showForceConfirm =false;
		}
	}
	
	/*
	 * Passenger Tab Grid Infor DTOs
	 */
	UI_tabPayment.updatePayemntTabTO = function(response){
		jsonPaySumObj = response.paymentSummaryTO;
		jsonPaxPayObj = response.passengerPayment;
		if(vouchersRedeemed){
			
		}
		/*
		 * if(!UI_tabPayment.isModify) { currencyExchangeRate =
		 * response.currencyExchangeRate; }
		 */
		if(typeof(response.currencyExchangeRate) != 'undefined'){			
			currencyExchangeRate = response.currencyExchangeRate;
		}
		
		if(typeof(response.countryCurrExchangeRate) != 'undefined'){			
			countryCurrExchangeRate = response.countryCurrExchangeRate;
		}
		var pnr = "";
		if(UI_tabPayment.isSegModify){
			pnr = response.pnr;
			$("#divPnrNoPay").html(response.pnr);
			$("#divBkgStatusPay").html(DATA_ResPro.modifySegments.status);
		}else if (UI_tabPayment.isAddModifyAnci){
			pnr = response.pnr;
			$("#divPnrNoPay").html(response.pnr);
			// $("#divBkgStatusPay").html(DATA_ResPro.modifySegments.status);
		} else if(UI_tabPayment.isAddSegment){
			pnr = DATA_ResPro.reservationInfo.PNR;
			$("#divPnrNoPay").html(DATA_ResPro.reservationInfo.PNR);
		} else if(UI_tabPayment.isRequote){
			pnr = DATA_ResPro.reservationInfo.PNR;
			$("#divPnrNoPay").html(DATA_ResPro.reservationInfo.PNR);
		}
		UI_tabPayment.pnr = pnr;
		jsonEnableItineryPrint = response.blnPrintItinerary;
		UI_tabPayment.airportMessage = response.airportMessage;
		UI_tabPayment.airportCardPaymentMessages = response.airportCardPaymentMessages;
		UI_tabPayment.isCsPnr = response.csPnr;
		UI_tabPayment.loadAirportMessage();
		if(DATA_ResPro.initialParams.creditPayments && top.arrIPGs.length != 0 && Number(jsonPaySumObj.balanceToPay) > 0){
			$("#rowCardPayments").show();
		} else {
			$("#rowCardPayments").hide();
		}
	}
	
	
	/*
	 * Payment Details
	 */
	UI_tabPayment.buildPaymentSummary = function(){
	
		// Map the Currency to the DTO
		if (jsonPaySumObj != null){
			if (jsonPaySumObj.currency == ""){jsonPaySumObj.currency = UI_tabSearchFlights.objResSrchParams.selectedCurrency;}
			
			$("#divPTotFare").html(jsonPaySumObj.totalFare);
			$("#divPTotTaxSur").html(jsonPaySumObj.totalTaxSurCharges);
			$("#divPModChg").html(jsonPaySumObj.modificationCharges);
			$("#divPHandChg").html(jsonPaySumObj.handlingCharges);
			
			if(jsonPaySumObj.totalServiceTax != null && UI_tabPayment.isReservation){
				$("#divServiceTax").html(jsonPaySumObj.totalServiceTax);
				$(".serviceTax").show();
			} else {
				$(".serviceTax").hide();
			}
			
			$("#divPTktPrice").html(jsonPaySumObj.ticketPrice);
			$("#divPTotPaid").html(jsonPaySumObj.paid);
			if(jsonPaySumObj.totalDiscount == null){
				jsonPaySumObj.totalDiscount = "0.00";
			}
			if(jsonPaySumObj.totalDiscount == null){
				jsonPaySumObj.totalDiscount = "0.00";
			}
			
			if(DATA_ResPro.initialParams.agentCommissionEnabled){
				$("#divPAgentCommission").html(jsonPaySumObj.totalAgentCommission);
				$("#divPendingAgentCommission").html(jsonPaySumObj.pendingAgentCommission);
			}
			
			$("#divPBalToPay").html(jsonPaySumObj.balanceToPay);
			
			$("#divPayCurr").html(jsonPaySumObj.currency);
		//	$("#divPayAmountCurr").html(jsonPaySumObj.currency);
			
			$("#divPFareDiscount").html('-' + jsonPaySumObj.totalDiscount);
			if(jsonPaySumObj.creditDiscount){
				$("#divPFareDiscount").html(jsonPaySumObj.totalDiscount);
			}
			UI_tabPayment.buildBalanceToPay();
		}
	}
	
	/*
	 * Balance To Pay
	 */
	UI_tabPayment.buildBalanceToPay = function(){
		
		
		if(jsonPaySumObj.selectedCurrency != null && jsonPaySumObj.currency != jsonPaySumObj.selectedCurrency){
			var intBalToPaySelcur = jsonPaySumObj.balanceToPaySelcur;
			if(typeof(currencyExchangeRate) != 'undefined' && typeof(currencyExchangeRate.multiplyingExchangeRate) != 'undefined') {
				intBalToPaySelcur = $.airutil.format.currency(parseFloat(UI_tabPayment.intBalToPay) * parseFloat(currencyExchangeRate.multiplyingExchangeRate));
				jsonPaySumObj.balanceToPaySelcur = $.airutil.format.currency(parseFloat(jsonPaySumObj.balanceToPay) * parseFloat(currencyExchangeRate.multiplyingExchangeRate));
			}
			
			var strSelectedCurrency = geti18nData('payement_SelectedCurrency','In Selected Currency');
			$("#divBaltoPay").html(jsonPaySumObj.balanceToPay + " " + jsonPaySumObj.currency + " ["+ intBalToPaySelcur +" "+ jsonPaySumObj.selectedCurrency  + " ]");
			$("#divPTotAmount").html(jsonPaySumObj.balanceToPay + " " + jsonPaySumObj.currency+ " [ "+ strSelectedCurrency +"<font color='red'>"+jsonPaySumObj.balanceToPaySelcur +" "+ jsonPaySumObj.selectedCurrency + "</font> ]");
		} else {
			$("#divBaltoPay").html(UI_tabPayment.intBalToPay + " " + jsonPaySumObj.currency);
			$("#divPTotAmount").html(jsonPaySumObj.balanceToPay + " " + jsonPaySumObj.currency);
		}
		
		if(countryCurrExchangeRate != null && !$.isEmptyObject(countryCurrExchangeRate)){
			var balPayInCCurr = $.airutil.format.currency(parseFloat(jsonPaySumObj.balanceToPay) * parseFloat(countryCurrExchangeRate.multiplyingExchangeRate));
			$("#divCCurrTotalBSP").hide();
			$("#divCCurrTotalBSP").html(jsonPaySumObj.balanceToPay + " " + jsonPaySumObj.currency
					+ " [In BSP Currency <font color='red'>"+
					balPayInCCurr +" "+ jsonPaySumObj.countryCurrencyCode+ "</font> ]");
		}

        if(DATA_ResPro.initialParams.offlinePayments && offlinePaymentOptions.length != 0 && jsonPaySumObj != null && jsonPaySumObj.balanceToPay != 0) {
			$("#rowOfflinePayments").show();
		} else {
			$("#rowOfflinePayments").hide();
		}
        
        if(!UI_tabPayment.isReservation){
			if(jsonBkgInfo != null && jsonBkgInfo.status == 'CNF'){
				$("#rowOfflinePayments").hide();
			} else if ((UI_tabPayment.isSegModify || UI_tabPayment.isAddModifyAnci || 
			        UI_tabPayment.isAddSegment || UI_tabPayment.isRequote) && (DATA_ResPro.reservationStatus == 'CNF' || (DATA_ResPro.reservationInfo != undefined && DATA_ResPro.reservationInfo.status == 'CNF'))){
				$("#rowOfflinePayments").hide();
			}
		} else {
			if(jsonFareQuoteSummary.onHoldRestricted && (
					UI_tabSearchFlights.getSelectedSystem() == 'INT' || !DATA_ResPro.initialParams.overrrideOnHoldRestricted)){
				$("#rowOfflinePayments").hide();
			}
		}
		
		$("#txtPayAmt").val(jsonPaySumObj.balanceToPay);
			
	}
	/*
	 * enable disable BSP payment option
	 */
	UI_tabPayment.enableDisableBspPaymentOption = function()
	{
		if (jsonPaySumObj != null ){
			if (parseFloat(jsonPaySumObj.balanceToPay) != parseFloat(jsonPaySumObj.ticketPrice)){				
				if(!DATA_ResPro.initialParams.bspPaymentsForAlterRes) {
					$("#rowBspPayments").hide();
					$("#rowBSPFee").hide();
				}						
			}else {
				if (DATA_ResPro.initialParams.bspPayments){
					$("#rowBspPayments").show();	
				}			
			}
		}		
	}
	
	/*
	 * Credit Payment
	 */
	UI_tabPayment.buildCreditBalanceToPay = function(response){
		$("#divTxnFee").html(response.ccCharges + " " + jsonPaySumObj.currency);
		// if(jsonPaySumObj.currency != jsonPaySumObj.selectedCurrency &&
		// UI_tabPayment.isReservation)
		$("#txtTxnCharge").val(response.ccCharges);
		$("#txtPaymentWithCharge").val(response.totalPayAmountWithccCharges);
		if(response.currencyInfo.currencyCode!=null && response.currencyInfo.currencyCode!= '' && jsonPaySumObj.currency != response.currencyInfo.currencyCode ){
			$("#divTotPay").html(response.totalPayAmountWithccCharges + " " + jsonPaySumObj.currency+"(Approx. Payment Amount <font color='red'>["+ response.totalPayAmountWithccChargesSelcur +" "+ response.currencyInfo.currencyCode + " ]</font>");
		}
		else{
			$("#divTotPay").html(response.totalPayAmountWithccCharges + " " + jsonPaySumObj.currency);
	    }
		$("#divTxtCurr").html(" "+ response.currencyInfo.currencyCode);
	}
	
	UI_tabPayment.paymentTypeOfflineClick = function(){
		if (!$("#txtEmail").val()) {
			$('#offlineEmailAddressContainer').show();
		}
		$('#selPaymentGateway').show();
		$("#selAgents").hide();
		$("#selAgents2").hide();		
		$("#selCardType").hide();
		$("#paymentAgentB").hide();
		$("#paymentAgentA").hide();
	}
	/*
	 * Mode of Payment Selection
	 */
	UI_tabPayment.paymentTypeCClick = function(){			
			
		if(UI_tabPayment.actPayMethodCash) {
			$('#lblReferenceC').show();	
			$('#selActualPayMethodC').show();

		}
		
		// Resetting current agent to logged in agent
		var agent = UI_tabPayment.getAgent(top.strAgentCode);
		if(agent!=null){
			UI_tabPayment.capturePayRef = (agent[2] == 'Y');
			UI_tabPayment.payRefMandatory = (agent[3] == 'Y');
		}
		
		if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef) {
			$('#lblReferenceC').show();	
			$('#paymentReferenceC').show();
			if(UI_tabPayment.payRefMandatory){
				$('#referenceMandatoryC').show();
			}
		}
			if(Number($.trim($('#minimumPayment').val()))>0){
				$('#paymentCash').show();
				$('#paymentCreditCard').hide();
			}
			else{
				$('#paymentCash').hide();
				$('#paymentCreditCard').hide();
			}
			
			$('#lblReferenceR').hide();	
			$('#selActualPayMethodR').val("");
			$('#selActualPayMethodR').hide();
			$("#paymentReferenceR").val("");
			$('#paymentReferenceR').hide();
			$('#referenceMandatoryR').hide();
			$('#lblReferenceA').hide();	
			$('#selActualPayMethodA').val("");
			$('#selActualPayMethodA').hide();	
			$("#paymentReferenceA").val("");
			$('#paymentReferenceA').hide();
			$('#referenceMandatoryA').hide();
			$("#paymentAgentA").val("");
			$("#paymentAgentB").val("");
			$('#paymentAgentA').hide();
			$('#paymentAgentB').hide();

		
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}
		if($("#selAgents2")[0]){
			$("#selAgents2")[0].selectedIndex = 0;	
		}
		
		$("#selCardType").val("");
		$("#lblAgentCurrency").html("");
		$("#selAgents").hide();
		$("#selAgents2").hide();		
		$("#selCardType").hide();
		$('#selPaymentGateway').hide();
		$('#offlineEmailAddressContainer').hide();
		UI_tabPayment.resetCCOnClick();
	}
	
	UI_tabPayment.paymentTypeAClick = function(){		
		
		if(UI_tabPayment.actPayMethodAcc) {
			$('#lblReferenceA').show();	
			$('#selActualPayMethodA').show();
		}
			$('#paymentCash').hide();
			$('#paymentCreditCard').hide();
			$('#lblReferenceC').hide();				
			$('#selActualPayMethodC').val("");
			$('#selActualPayMethodC').hide();
			$("#paymentReferenceC").val("");
			$('#paymentReferenceC').hide();
			$('#referenceMandatoryC').hide();
			$('#lblReferenceR').hide();			
			$('#selActualPayMethodR').val("");
			$('#selActualPayMethodR').hide();	
			$("#paymentReferenceR").val("");
			$('#paymentReferenceR').hide();
			$('#referenceMandatoryR').hide();
			if(DATA_ResPro.initialParams.onAccountMultiAgentPayments){
    			$("#paymentAgentA").val("");
    			$('#paymentAgentA').show();
    			$("#paymentAgentB").val("");
    			$('#paymentAgentB').show();
			} else {
			    $("#paymentAgentA").val("");
                $('#paymentAgentA').hide();
                $("#paymentAgentB").val("");
                $('#paymentAgentB').hide();
                
                if(Number($.trim($('#minimumPayment').val()))>0){
    				$('#paymentAgentA').show();
    			}
			}
						
		$("#selAgents").show();
		$("#selAgents2").show();
		$("#selCardType").hide();			
		$('#selPaymentGateway').hide();
		$('#offlineEmailAddressContainer').hide();
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}
		if($("#selAgents2")[0]){
			$("#selAgents2")[0].selectedIndex = 0;	
		}
		UI_tabPayment.agentselected();
		UI_tabPayment.resetCCOnClick();
	}
	
	UI_tabPayment.paymentTypeBspClick = function(){		
		
		if(UI_tabPayment.actPayMethodBsp) {
			$('#lblReferenceBsp').show();	
			$('#selActualPayMethodBsp').show();
		}

		// Resetting current agent to logged in agent
		var agent = UI_tabPayment.getAgent(top.strAgentCode);
		if(agent!=null){
			UI_tabPayment.capturePayRef = (agent[2] == 'Y');
			UI_tabPayment.payRefMandatory = (agent[3] == 'Y');
		}
		
		if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef) {
			$('#lblReferenceBsp').show();	
			$('#paymentReferenceBsp').show();
			if(UI_tabPayment.payRefMandatory){
				$('#referenceMandatoryBsp').show();
			}
		}
			$('#paymentCash').hide();
			$('#paymentCreditCard').hide();
			$('#lblReferenceR').hide();	
			$('#selActualPayMethodR').val("");
			$('#selActualPayMethodR').hide();
			$("#paymentReferenceR").val("");
			$('#paymentReferenceR').hide();
			$('#referenceMandatoryR').hide();
			$('#lblReferenceA').hide();	
			$('#selActualPayMethodA').val("");
			$('#selActualPayMethodA').hide();	
			$("#paymentReferenceA").val("");
			$('#paymentReferenceA').hide();
			$('#referenceMandatoryA').hide();
			$('#paymentAgentA').val("");
			$('#paymentAgentA').hide();	
			$('#paymentAgentB').val("");
			$('#paymentAgentB').hide();	
			
						
			if($("#selAgents")[0]){
				$("#selAgents")[0].selectedIndex = 0;	
			}
			if($("#selAgents2")[0]){
				$("#selAgents2")[0].selectedIndex = 0;	
			}
			$("#selCardType").val("");
			$("#lblAgentCurrency").html("");
			$("#selAgents").hide();
			$("#selAgents2").hide();
			$("#selCardType").hide();	
			$('#selPaymentGateway').hide();
		    $('#offlineEmailAddressContainer').hide();
			UI_tabPayment.resetCCOnClick();			
			UI_tabPayment.calculateBSPFee();	
	}
	
	UI_tabPayment.calculateBSPFee = function() {
		var data = {};
		UI_tabPayment.setPaymentData(data);
		var validOption = true;		
		data['paxPayments'] = $.toJSON(jsonPaxPayObj);
		
		if(UI_tabPayment.isReservation){
			data['action'] = "CREATE";
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			data["groupPNR"]	= "";
			data["pnr"] 	    = "";
			data['selectedCurrency'] = jsonPaySumObj.selectedCurrency;			
			data['selectedFlightListSize'] = UI_tabPayment.getFlightRPHList().length; 
			data['selectedFlightList'] = $.toJSON(UI_tabPayment.getFlightRPHList());
	        data['paxState'] = UI_tabSearchFlights.paxState;
	        data['paxCountryCode'] = UI_tabSearchFlights.paxCountryCode;
	        data['paxTaxRegistered'] = UI_tabSearchFlights.paxTaxRegistered;
			var searchData = UI_tabSearchFlights.createSearchParams('searchParams');
			data = $.airutil.dom.concatObjects(data,searchData);
		}else if(UI_tabPayment.isModify) {
			data['action'] = "MODIFY";
			data['flightSegmentToList'] = $.toJSON(UI_reservation.flightSegmentToList);
			data['resContactInfo'] = UI_reservation.contactInfo;
			data['paxWiseAnci'] = $.toJSON(UI_reservation.paxWiseAnci);			
			data['selectedFareType'] = '';
			data["groupPNR"]	= jsonGroupPNR;
			data["pnr"] 	    = jsonPNR;
			data['selectedCurrency'] = $('#hdnSelCurrency').val();		
			data['selectedFlightListSize'] = UI_reservation.totalSegmentCount;
			data['resPaxs'] = UI_reservation.jsonPassengers;
		} else if(UI_tabPayment.isSegModify){
			data['action'] = "SEGMENT_MODIFY";
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			data['pnr'] = DATA_ResPro.modifySegments.PNR;
			data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
			data['selectedCurrency'] = $('#hdnSelCurrency').val();			
			data['selectedFlightListSize'] = UI_tabPayment.getFlightRPHList().length; 
		} else if (UI_tabPayment.isAddModifyAnci){	
			data['action'] = "ADD_MODIFY_ANCI";
			data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
			data['flightRPHList'] = $.toJSON(DATA_ResPro["flightRPHList"]);
			
			data['selectedFareType'] = '';
			data['pnr'] = DATA_ResPro.pnrNo;
			data['groupPNR'] = DATA_ResPro.groupPNRNo;
			data['selectedCurrency'] = $('#hdnSelCurrency').val();		
			data['selectedFlightListSize'] = UI_tabAnci.modifiedSegemntCount; 																				
		} else if(UI_tabPayment.isAddSegment){
			data['action'] = "ADD_SEGMENT";
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;
			data['selectedCurrency'] = UI_addSegment.selCurrency;		
			data['selectedFlightListSize'] = UI_tabPayment.getFlightRPHList().length; 
		} else if(UI_tabPayment.isRequote){		
			data['action'] = "REQUOTE";
			data = UI_tabPayment.addFareQuoteParams(data);
			data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
			data['flightRPHList'] = $.toJSON(UI_requote.populateFlightSegmentList());			
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;
			data['selectedCurrency'] = $('#hdnSelCurrency').val();			
			data['selectedFlightListSize'] = UI_requote.getCCChargeableFltSegmentsCount(); 
			data['selectedFlightList'] = $.toJSON(UI_requote.populateFlightSegmentList());
	        data['paxState'] = UI_tabSearchFlights.paxState;
	        data['paxCountryCode'] = UI_tabSearchFlights.paxCountryCode;
	        data['paxTaxRegistered'] = UI_tabSearchFlights.paxTaxRegistered;
	        data['nameChangeFlightSegmentList'] = $.toJSON(DATA_ResPro.resSegments);
			data['resPaxs'] = $.toJSON(DATA_ResPro.resPaxs);
			data['addSegment'] = DATA_ResPro.isAddSegment;
	        var searchData = UI_tabSearchFlights.createSearchParams('searchParams');
			data = $.airutil.dom.concatObjects(data,searchData);
		} else {
			validOption = false;
		}	
		
		if(!UI_tabPayment.paymentProcessed && validOption){
			UI_tabPayment.paymentProcessed = true;
			UI_commonSystem.showProgress();
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'bSP!calculateBSPFee.action', dataType: 'json', type: 'post', processData: false, data:data, 
				success: UI_tabPayment.applyBSPFeeSuccess, 
				error:function(response){UI_tabPayment.paymentProcessed = false; UI_commonSystem.setErrorStatus(response)}});
		} else {
			showERRMessage("Invalid BSP payment action.");
		}
		
	}
	
	UI_tabPayment.buildBSPBalanceToPay = function(response){
		var agentCurrencyCode = response.agentCurrencyCode; 
		$("#totalPayAmountWithBSPFee").val(response.totalPayAmountWithBSPFee);
		$("#BSPFee").text(response.bspFee + " " + jsonPaySumObj.currency);		
		if(agentCurrencyCode != "" && jsonPaySumObj.currency != ""){
			$("#divCCurrTotalBSP").html(response.totalPayAmountWithBSPFee + " " + jsonPaySumObj.currency+"(Approx. Payment Amount <font color='red'>["+ response.totalPayAmountWithBSPFeeSelcur +" "+  agentCurrencyCode + " ]</font>");
		}
		else{
			$("#divCCurrTotalBSP").html(response.totalPayAmountWithBSPFee + " " + jsonPaySumObj.currency);
	    }		
	}
	
	UI_tabPayment.applyBSPFeeSuccess = function (response) {		
		
		if (response != null){
			if(response.success) {				
				UI_tabPayment.buildBSPBalanceToPay(response);				
			}else {
				showERRMessage(response.messageTxt);
				return false;
			}			
		}
		
		UI_commonSystem.hideProgress();
		UI_tabPayment.paymentProcessed = false;
		
	}
	
	UI_tabPayment.paymentTypeRClick = function(){	
		
		if(UI_tabPayment.actPayMethodCC) {
			$('#lblReferenceR').show();			
			$('#selActualPayMethodR').show();

		}
		
		// Resetting current agent to logged in agent
		var agent = UI_tabPayment.getAgent(top.strAgentCode);
		if(agent!=null){
			UI_tabPayment.capturePayRef = (agent[2] == 'Y');
			UI_tabPayment.payRefMandatory = (agent[3] == 'Y');
		}
		
		if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef) {
			$('#lblReferenceR').show();		
			$('#paymentReferenceR').show();
			if(UI_tabPayment.payRefMandatory){
				$('#referenceMandatoryR').show();
			}
		}
			$('#paymentCash').hide();
			if(Number($.trim($('#minimumPayment').val()))>0){
				$('#paymentCreditCard').show();
			}
			else{
				$('#paymentCreditCard').hide();
			}
			
			$('#lblReferenceC').hide();				
			$('#selActualPayMethodC').val("");
			$('#selActualPayMethodC').hide();
			$("#paymentReferenceC").val("");
			$('#paymentReferenceC').hide();
			$('#referenceMandatoryC').hide();
			$('#lblReferenceA').hide();	
			$('#selActualPayMethodA').val("");
			$('#selActualPayMethodA').hide();	
			$("#paymentReferenceA").val("");
			$('#paymentReferenceA').hide();
			$('#referenceMandatoryA').hide();
			$('#paymentAgentA').val("");
			$('#paymentAgentA').hide();	
			$('#paymentAgentB').val("");
			$('#paymentAgentB').hide();	
				
		$("#selAgents").hide();
		$("#selAgents2").hide();
		$("#selCardType").show();				
		$('#selPaymentGateway').hide();
		$('#offlineEmailAddressContainer').hide();

		UI_tabPayment.airportMessage = UI_tabPayment.airportCardPaymentMessages
		UI_tabPayment.loadAirportMessage();
		
		
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
		}
		if($("#selAgents2")[0]){
			$("#selAgents2")[0].selectedIndex = 0;	
		}
		$("#lblAgentCurrency").html("");
	}
	UI_tabPayment.cachedAgents = {};
	
	UI_tabPayment.getAgent = function(agentCode){
		if(UI_tabPayment.cachedAgents[agentCode] == null){
			for( var i = 0 ; i < top.arrAllTravelAgents.length ; i++){
				if(UI_tabPayment.cachedAgents[top.arrAllTravelAgents[i][0]] == null){
					UI_tabPayment.cachedAgents[top.arrAllTravelAgents[i][0]] = top.arrAllTravelAgents[i];
				}
				if(top.arrAllTravelAgents[i][0] == agentCode){					
					break;
				}
			}
		}
		return UI_tabPayment.cachedAgents[agentCode];
	}
	
	UI_tabPayment.agentselected = function() {	
		UI_tabPayment.capturePayRef = false;
		UI_tabPayment.payRefMandatory = false;
		
		if($("#selAgents").val() != "") {
			var selectedAgentCurrCode = "";
			var arrAgentInfo;
			// TODO move this- accessing top to a common method
			for( var i=0; i < top.arrTravelAgents.length; i++) {
				arrAgentInfo = top.arrTravelAgents[i];
				if($("#selAgents").val() == arrAgentInfo[1]) {
					selectedAgentCurrCode = arrAgentInfo[2];
					break;
				}				
			}			
			
			
			if(arrAgentInfo != null && arrAgentInfo != "") {
				$("#lblAgentCurrency").html("<b>Your payment will be processed in "+ selectedAgentCurrCode +"</b>");
			}else {
				$("#lblAgentCurrency").html("");
			}			
			
		}else {			
			$("#lblAgentCurrency").html("");
		}
		var agentCode = $("#selAgents").val();
		if(agentCode == '') agentCode = top.strAgentCode;
		var agent = UI_tabPayment.getAgent(agentCode);
		if(agent!=null){
			UI_tabPayment.capturePayRef = (agent[2] == 'Y');
			UI_tabPayment.payRefMandatory = (agent[3] == 'Y');
		}
		if(UI_tabPayment.capturePayRef && DATA_ResPro.initialParams.allowCapturePayRef){ 
			$('#lblReferenceA').show();				
			$('#paymentReferenceA').show().attr('style','display:inline;');
			
			if(UI_tabPayment.payRefMandatory){
				$('#referenceMandatoryA').show().attr('style','display:inline;');
			}
		}else{
			if(!UI_tabPayment.actPayMethodAcc) {
			   $('#lblReferenceA').hide();
			}
			$('#paymentReferenceA').val('');
			$('#paymentReferenceA').hide();
			$('#referenceMandatoryA').hide();
		}
	}
	
	/*
	 * Pay Confirm
	 */
	UI_tabPayment.payConfirmOnClick = function(){
		
		if(DATA_ResPro.initialParams.autoCancellationEnable){
			
			if(UI_tabPayment.prepareAutoCancelMessage()){
				$('#lblAutoCancelCtdConfirm').text(UI_tabPayment.autoCancelPopupMessage);
				$('#divAutoCancelConfirm').show();
				
			}else{
				UI_tabPayment.payConfirmExecute();
			}
		}else{
			UI_tabPayment.payConfirmExecute();
		}
		
		/*$("#txtPayMode").val("CONFIRM");
		 TODO Validation  
		UI_tabPayment.continueOnClick();	*/	
	}
	
	UI_tabPayment.payConfirmExecute = function(){
		$("#txtPayMode").val("CONFIRM");
		/* TODO Validation */ 
		UI_tabPayment.continueOnClick();		
	}
	
	/*
	 * Force Pay Click
	 */
	UI_tabPayment.payForceOnClick = function(){
		var blnNoPay = true;
		for(var pl =0; pl < jsonPaxPayObj.length;pl++){
			var y = pl+1;
			var rowTR = $("#"+ y + "select").closest('tr');
			if($("#"+ y + "_select").attr('checked')){
				blnNoPay = false;
				break;
			}
		}		
		if(blnNoPay){
			$("#txtPayMode").val("FORCE");
			for(var x=0;x<jsonPaxPayObj.length;x++){
				var index = x+1;
				var rowTR = $("#"+ index + "select").closest('tr');
				if($("#"+ index + "_select").attr('checked')){
					jsonPaxPayObj[x].hasPayment = "true";
				}
				else{				
					jsonPaxPayObj[x].hasPayment = "false";
				} 
			}		
			/* TODO Validation */
			UI_tabPayment.continueOnClick();
		} else {
			alert("All passengers should be unselected to do force confirm.");
		}
	}
	
	
	/*
	 * Reset Contact Details
	 */
	UI_tabPayment.resetOnClick = function(){
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		
		if(UI_tabPayment.isModify){
			if($("#selITNLang").val() != null && $("#selITNLang").val() != ""){
				$("#selLang").val($("#selITNLang").val());
			} else {
				$("#selLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
			}
		} else {
			if($("#selPrefLang").val() != null && $("#selPrefLang").val() != ""){
				$("#selLang").val($("#selPrefLang").val());
			} else if(DATA_ResPro["systemParams"] != null){
				$("#selLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
			}
	
		}
		
		if($("#selAgents")[0]){
			$("#selAgents")[0].selectedIndex = 0;	
			UI_tabPayment.agentselected();
		}
		$("#selCardType").val("");
		if(!DATA_ResPro.initialParams.onAccountPayments){
			$("#radPaymentTypeR").attr('checked',true);
			UI_tabPayment.paymentTypeRClick();
		}else {
			$("#radPaymentTypeA").attr('checked',true);
			UI_tabPayment.paymentTypeAClick();
		}
			
		var emailFilled = $.trim($("#txtEmail").val()) != "";
		$("#chkEmail").attr('checked', emailFilled);
		$("#chkPrint").attr('checked', false);		
		$('#chkPayPaxChg').attr('checked', false);
		
	}
	
	/*
	 * Grid Constructor
	 */
	UI_tabPayment.constructGrid = function(inParams){
		// construct grid
		var columnNames = {};
		var columnModels = {};
		
		if(DATA_ResPro.initialParams.allowNamedCreditTransfer || DATA_ResPro.initialParams.allowAnyCreditTransfer) {
			if (DATA_ResPro.initialParams.forceConfirm || UI_tabPayment.showForceConfirm) {
				$('#divCheckAll').show();
				columnNames = [  geti18nData('payement_Select','Select'),  geti18nData('payement_PassengerName','Passenger Name'),  geti18nData('payement_Total','Total'),  geti18nData('payement_Paid','Paid'),  geti18nData('payement_ToPay','To Pay'),  geti18nData('testLbl','Used Credit'),  geti18nData('payement_Credit','Credit'),  geti18nData('payement_Remove','Remove'),  geti18nData('payement_PassengerID','Pax Id')];
				columnModels = [
					{name:'select', index:'select', width:50, align:"right",sortable:false,editable:true,edittype:"checkbox", formatter:'checkbox',
						editoptions:{
							value:'0:1',
							dataEvents:[{
								type : 'click',
								fn: function(e){
									UI_tabPayment.selectClick(this);
								}
							}]
						}},				                
					{name:'displayPassengerName',  index:'displayPassengerName', width:310, align:"left"},
					{name:'displayTotal', index:'displayTotal', width:100, align:"right"},
					{name:'displayPaid', index:'displayPaid', width:100, align:"right"},
					{name:'displayToPay', index:'displayToPay', width:100, align:"right"},
					{name:'displayUsedCredit', index:'displayUsedCredit', width:100, align:"right"},
					{name:'displayCreditButton', index:'displayCreditButton', width:100, align:"center", editable : true, edittype : "button",
						editoptions : {
							className : "ui-state-default ui-corner-all",							
							value : 'Credit',
							dataEvents:[{
								type : 'click',
								fn: function(e){
									UI_tabPayment.btnCreditOnClick(this);
								}
							}]
							
						}},
					{name:'displayRemoveButton', index:'displayRemoveButton', width:100, align:"center", editable : true, edittype : "button",
						editoptions : {
							className : "ui-state-default ui-corner-all",
							value : geti18nData('btn_Remove','Remove'),
							dataEvents:[{
								type : 'click',
								fn: function(e){
									UI_tabPayment.btnRemoveOnClick(this);
								}
							}]
							
						}},
					{name:'displayPaxID', index:'displayPaxID', width:100, align:"right", hidden:true}
				];
			} else {
				$('#divCheckAll').hide();
				columnNames = [ geti18nData('payement_PassengerName','Passenger Name'), geti18nData('payement_Total','Total') ,  geti18nData('payement_Paid','Paid'),  geti18nData('payement_ToPay','To Pay'), geti18nData('payement_UsedCredit', 'Used Credit'), geti18nData('payement_Credit','Credit') , geti18nData('payement_Remove','Remove'),  geti18nData('payement_PassengerID','Pax Id')];
				columnModels = [
					{name:'displayPassengerName',  index:'displayPassengerName', width:310, align:"left"},
					{name:'displayTotal', index:'displayTotal', width:100, align:"right"},
					{name:'displayPaid', index:'displayPaid', width:100, align:"right"},
					{name:'displayToPay', index:'displayToPay', width:100, align:"right"},
					{name:'displayUsedCredit', index:'displayUsedCredit', width:100, align:"right"},
					{name:'displayCreditButton', index:'displayCreditButton', width:100, align:"center", editable : true, edittype : "button",
						editoptions : {
							className : "ui-state-default ui-corner-all",
							value : 'Credit',
							dataEvents:[{
								type : 'click',
								fn: function(e){
									UI_tabPayment.btnCreditOnClick(this);
								}
							}]
							
						}},
					{name:'displayRemoveButton', index:'displayRemoveButton', width:100, align:"center", editable : true, edittype : "button",
						editoptions : {
							className : "ui-state-default ui-corner-all",
              						value : geti18nData('btn_Remove','Remove'),
							dataEvents:[{
								type : 'click',
								fn: function(e){
									UI_tabPayment.btnRemoveOnClick(this);
								}
							}]
							
						}},
					{name:'displayPaxID', index:'displayPaxID', width:100, align:"right", hidden:true}
				];
			} 
		} else {
			if (DATA_ResPro.initialParams.forceConfirm || UI_tabPayment.showForceConfirm) {
				$('#divCheckAll').show();
				columnNames = [  geti18nData('payement_Select','Select'),  geti18nData('payement_PassengerName','Passenger Name'),geti18nData('payement_Total','Total') ,  geti18nData('payement_Paid','Paid'), geti18nData('payement_ToPay','To Pay'), geti18nData('payement_UsedCredit', 'Used Credit')];
				columnModels = [
					{name:'select', index:'select', width:50, align:"right",sortable:false,editable:true,edittype:"checkbox", formatter:'checkbox',
						editoptions:{
							value:'0:1',
							dataEvents:[{
								type : 'click',
								fn: function(e){
									UI_tabPayment.selectClick(this);
								}
							}]
						}},				                
					{name:'displayPassengerName',  index:'displayPassengerName', width:310, align:"left"},
					{name:'displayTotal', index:'displayTotal', width:100, align:"right"},
					{name:'displayPaid', index:'displayPaid', width:100, align:"right"},
					{name:'displayToPay', index:'displayToPay', width:100, align:"right"},
					{name:'displayUsedCredit', index:'displayUsedCredit', width:100, align:"right"}
				];
			} else {
				$('#divCheckAll').hide();
				columnNames = [ geti18nData('payement_PassengerName','Passenger Name'),  geti18nData('payement_Total','Total'),  geti18nData('payement_Paid','Paid'),  geti18nData('payement_ToPay','To Pay'),  geti18nData('payement_UsedCredit','Used Credit')];
				columnModels = [
					{name:'displayPassengerName',  index:'displayPassengerName', width:310, align:"left"},
					{name:'displayTotal', index:'displayTotal', width:100, align:"right"},
					{name:'displayPaid', index:'displayPaid', width:100, align:"right"},
					{name:'displayToPay', index:'displayToPay', width:100, align:"right"},
					{name:'displayUsedCredit', index:'displayUsedCredit', width:100, align:"right"}
				];
			 }
		}
		
		$(inParams.id).jqGrid({			
			height: 150,
			width: 910,
			colNames:columnNames,
			colModel:columnModels,			
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			datatype: "local",
			viewrecords: true
		});
	}

	/*
	 * Select passenger
	 */
	UI_tabPayment.selectClick = function(inParams){
		UI_commonSystem.pageOnChange();
		var blnAllPay = true;
		var selectedRow = inParams.id.split("_")[0];
		if($("#"+ selectedRow + "_select").attr('checked')){
			jsonPaxPayObj[selectedRow - 1].hasPayment = "true";
			if(parseFloat(jsonPaxPayObj[selectedRow - 1].displayToPay) > 0) {
				jsonPaySumObj.balanceToPay = $.airutil.format.currency(parseFloat(jsonPaySumObj.balanceToPay) + parseFloat(jsonPaxPayObj[selectedRow - 1].displayToPay));
				UI_tabPayment.intBalToPay = $.airutil.format.currency(parseFloat(UI_tabPayment.intBalToPay) - parseFloat(jsonPaxPayObj[selectedRow - 1].displayToPay));
				UI_tabPayment.buildBalanceToPay();
			}
			if(parseFloat(UI_tabPayment.intBalToPay) == 0){
                $("#chkPrint").show();
                $("#tdPrint").html("Print Itinerary");
			}
		} else{
			jsonPaxPayObj[selectedRow - 1].hasPayment = "false";
			if(parseFloat(jsonPaxPayObj[selectedRow - 1].displayToPay) > 0) {
				jsonPaySumObj.balanceToPay = $.airutil.format.currency(parseFloat(jsonPaySumObj.balanceToPay) - parseFloat(jsonPaxPayObj[selectedRow - 1].displayToPay));
				UI_tabPayment.intBalToPay = $.airutil.format.currency(parseFloat(UI_tabPayment.intBalToPay) + parseFloat(jsonPaxPayObj[selectedRow - 1].displayToPay));
				UI_tabPayment.buildBalanceToPay();
			}
			if(parseFloat(UI_tabPayment.intBalToPay) > 0 && !jsonEnableItineryPrint){
                $("#chkPrint").hide();
                $("#chkPrint").attr('checked', false);	
                $("#tdPrint").html("");
			}
		}
		for(var pl =0; pl < jsonPaxPayObj.length;pl++){
			if((jsonPaxPayObj[pl].hasPayment == "false") &&(Number(jsonPaxPayObj[pl].displayToPay) > 0)){
				blnAllPay = false;
				break;
			}
		}
		
		$("#btnPayContinue").enable();
		
		if(!UI_tabPayment.isReservation && !UI_tabPayment.isModify){
			if(!blnAllPay){
				$("#btnPayContinue").disable();
			}
		}
		// if(!blnAllPay){
	// $("#btnPayContinue").disable();
	// }else {
			
	// }
		UI_tabPayment.strPaymentMsg = "";
		var blnNoPay = true;
		for(var pl =0; pl < jsonPaxPayObj.length;pl++){
			if(jsonPaxPayObj[pl].hasPayment != "false"){
				blnNoPay = false;
				break;
			}
		}		
		if(blnNoPay){
			UI_tabPayment.strPaymentMsg = "NOPAY";
		}		
	}
	
	/*
	 * Remove Button click
	 */
	UI_tabPayment.btnRemoveOnClick = function(inParams){
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		
		UI_commonSystem.pageOnChange();
		var selectedRow = inParams.id.split("_")[0];
		UI_tabPayment.selectedPax = $("#tblPaxPayDet").getCell(selectedRow,'displayPaxID');
		var formName = 	"#frmMakeBkg";
		var data = {};
		data['selCurrency'] = $("#selectedCurrency").val();
		data['paxPayments'] = $.toJSON(jsonPaxPayObj);
		data['selectedCredit'] = new Array();		
		data['blnApply'] = false;
		UI_commonSystem.showProgress();
		if(UI_tabPayment.isReservation) {
			data['paxId'] =   Number(UI_tabPayment.selectedPax);
			data["groupPNR"]	= "";
			data["pnr"] 	    = "";
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			formName = "#frmMakeBkg";
		}else if (UI_tabPayment.isModify) {
			data['paxId'] =  Number(UI_tabPayment.selectedPax);
			data["groupPNR"]	= jsonGroupPNR;
			data["pnr"] 	    = jsonPNR;
			data['resPaySumm'] = $.toJSON(jsonPaySumObj);		
			data['selectedFareType'] = '';
			formName = "#frmModiBkg";
		}else if (UI_tabPayment.isAddModifyAnci){
			data['paxId'] =  Number(UI_tabPayment.selectedPax);
			data['pnr'] = DATA_ResPro.pnrNo;
			data['groupPNR'] = DATA_ResPro.groupPNRNo;
			data['modifyAnci'] = true;
			data['resPaySumm'] = $.toJSON(jsonPaySumObj);
			data['selectedFareType'] = '';
			formName = "#frmMakeBkg";
		}else if(UI_tabPayment.isAddSegment){				
			data['paxId'] = Number(UI_tabPayment.selectedPax);
			data["resPaySumm"] = $.toJSON(jsonPaySumObj);
			data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			data['groupPNR'] =  DATA_ResPro.reservationInfo.groupPNR;			
			data['selectedFareType'] = '';			
		}else if(UI_tabPayment.isRequote){				
			data['paxId'] = Number(UI_tabPayment.selectedPax);
			data["resPaySumm"] = $.toJSON(jsonPaySumObj);
			data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			data['groupPNR'] =  DATA_ResPro.reservationInfo.groupPNR;		
			data['requote'] =  true;	
			data['selectedFareType'] = '';			
		} else {			
			data['paxId'] =  Number(UI_tabPayment.selectedPax);
			data['pnr'] = DATA_ResPro.modifySegments.PNR;
			data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
			data['selectedFareType'] = '';
			data['resPaySumm'] = $.toJSON(jsonPaySumObj);
			formName = "#frmMakeBkg";
		}
		$(formName).attr('target', "_self");
		$(formName).action("applyPaxCredit.action");	
		$(formName).ajaxSubmit({ dataType: 'json', data:data,
			success: UI_tabPayment.applyPaxCreditSuccess,
			error:UI_commonSystem.setErrorStatus});
		
	}
	
	/*
	 * TODO Credit Button click
	 */
	UI_tabPayment.btnCreditOnClick = function(inParams){
		UI_commonSystem.pageOnChange();
		// we have the pax sequence id here
		var selectedRow = inParams.id.split("_")[0];
		UI_tabPayment.creditPaxPayObj = $.toJSON(jsonPaxPayObj);
		UI_tabPayment.selectedPax = $("#tblPaxPayDet").getCell(selectedRow,'displayPaxID');		
		var strFileName = "showNewFile!loadCredit.action";
		var inHeight = 630;
		if ($.browser.msie){inHeight = 640;}
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
		top.objCWindow =  window.open(strFileName, "popWindow", $("#popWindow").getPopWindowProp(inHeight, 900));
	}
	
	/*
	 * Apply Credit
	 */
	UI_tabPayment.applyPaxCredit = function(inParams){
		UI_commonSystem.pageOnChange();
		var data = {};
		var paxId = UI_tabPayment.selectedPax;
		var formName = 	"#frmMakeBkg";
		data['paxPayments'] = $.toJSON(jsonPaxPayObj);
		data['selectedCredit'] = inParams;
		data['blnApply'] = true;
		if(UI_tabPayment.isReservation) {
			data['paxId'] = Number(paxId); 
			data['selCurrency'] = $("#selectedCurrency").val();
			data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
			formName = "#frmMakeBkg";
		} else if (UI_tabPayment.isModify) {
			data['paxId'] = Number(paxId);
			data['selCurrency'] = $("#hdnSelCurrency").val();
			data["resPaySumm"] = $.toJSON(UI_reservation.paySummary);
			data["groupPNR"]	= jsonGroupPNR;
			data["pnr"] 	    = jsonPNR;	
			data['selectedFareType'] = '';
			formName = "#frmModiBkg";		
		} else if (UI_tabPayment.isAddModifyAnci) {			
			data['modifyAnci'] = true;
			data['paxId'] = Number(paxId);
			data["resPaySumm"] = $.toJSON(jsonPaySumObj);
			data['pnr'] = DATA_ResPro.pnrNo;
			data['groupPNR'] = DATA_ResPro.groupPNRNo;
			data['selectedFareType'] = '';
			formName = "#frmMakeBkg";		
		} else if(UI_tabPayment.isAddSegment){				
			data['paxId'] = Number(paxId);
			data["resPaySumm"] = $.toJSON(jsonPaySumObj);
			data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			data['groupPNR'] =  DATA_ResPro.reservationInfo.groupPNR;			
			data['selCurrency'] = UI_addSegment.selCurrency;			
		} else if(UI_tabPayment.isRequote){				
			data['paxId'] = Number(paxId);
			data["resPaySumm"] = $.toJSON(jsonPaySumObj);
			data['pnr'] = DATA_ResPro.reservationInfo.PNR;
			data['groupPNR'] =  DATA_ResPro.reservationInfo.groupPNR;			
			data['requote'] =  true;	
			data['selCurrency'] = UI_modifyResRequote.getSelectedCurrencyCode();			
		} else {
			data['paxId'] = Number(paxId);			
			data['selCurrency'] = $("#selectedCurrency").val();
			data['pnr'] = DATA_ResPro.modifySegments.PNR;
			data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
			data['selectedFareType'] = '';
			formName = "#frmMakeBkg";
		}
		data['csPnr'] = UI_tabPayment.isCsPnr;
		$(formName).attr('target', "_self");
		$(formName).action("applyPaxCredit.action");
		$(formName).ajaxSubmit({ dataType: 'json', data:data,
										success: UI_tabPayment.applyPaxCreditSuccess,
										error:UI_commonSystem.setErrorStatus});
		
	}
	
	/*
	 * Apply Pax Credit Success
	 */ 
	UI_tabPayment.applyPaxCreditSuccess = function(response){
		UI_commonSystem.hideProgress();
		if(response.success) {
			UI_tabPayment.updatePayemntTabTO(response);
			if(UI_tabPayment.isReservation) {
				UI_tabPayment.paymentDetailsOnLoadCall();
			}else if (UI_tabPayment.isModify) {
				jsonResPaymentSummary = jsonPaySumObj;
				jsonPaxPriceTo = jsonPaxPayObj;
				UI_tabPayment.paymentModifyDetailsOnLoadCall();
			}else {
				UI_tabPayment.paymentDetailsOnLoadCall();
			}
			UI_tabPayment.enableDisableBspPaymentOption();
		}else {
			showERRMessage(response.messageTxt);			
		}
		top.objCWindow.close();
		return false;
	}
	
	/*
	 * Fill Drop down data for the Payment tab
	 */
	UI_tabPayment.fillDropDownData = function(){
		
		if(DATA_ResPro.initialParams.onAccountPayments || DATA_ResPro.initialParams.onAccountPaymentsReporting) {
			$("#selAgents").removeAttr('disabled');
			$("#selAgents").show();
		} else {			
			$("#selAgents").append("<option value='"+top.strAgentCode+"'>"+top.strAgentCode+"</option>");
			$("#selAgents option:eq(1)").attr("selected", "selected");			
			$("#selAgents").attr('disabled', true);
			$("#selAgents").hide();
			
		}
		UI_tabPayment.agentselected();
		$("#selCardType").empty();
		$("#selLang").empty();
		$("#selPaymentGateway").empty();

		$("#selCardType").fillDropDown( { dataArray:top.arrIPGs , keyIndex:1 , valueIndex:0, firstEmpty:false });
		$("#selPaymentGateway").fillDropDown( { dataArray:offlinePaymentOptions, keyIndex:1 , valueIndex:0, firstEmpty:false });
		$("#selLang").fillDropDown({dataArray:top.jsonItnLang, keyIndex:"id", valueIndex:"desc", firstEmpty:false});
		
		if(UI_tabPayment.isModify){
			if($("#selITNLang").val() != null && $("#selITNLang").val() != ""){
				$("#selLang").val($("#selITNLang").val());
			} else {
				$("#selLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
			}
		} else {
			if($("#selPrefLang").val() != null && $("#selPrefLang").val() != ""){
				$("#selLang").val($("#selPrefLang").val());
			} else if (DATA_ResPro["systemParams"] != null) {
				$("#selLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
			}
	
		}
		
		var arr;
		arr = top.mapActualPaymentMethodData['CA'];
		
		if(arr != null) {
		    $("#selActualPayMethodC").empty();
			$("#selActualPayMethodC").fillDropDown({dataArray:arr, keyIndex:0, valueIndex:1, firstEmpty:false});
			UI_tabPayment.actPayMethodCash = true;
		}
		
		arr = top.mapActualPaymentMethodData['OA'];
		if(arr != null) {
			$("#selActualPayMethodA").fillDropDown({dataArray:arr, keyIndex:0, valueIndex:1, firstEmpty:false});
			UI_tabPayment.actPayMethodAcc = true;
		}
		
		arr = top.mapActualPaymentMethodData['CC'];
		if(arr != null) {
			$("#selActualPayMethodR").fillDropDown({dataArray:arr, keyIndex:0, valueIndex:1, firstEmpty:false});
			UI_tabPayment.actPayMethodCC = true;
		}
		
		arr = top.mapActualPaymentMethodData['BSP'];
		if(arr != null) {
			$("#selActualPayMethodBsp").fillDropDown({dataArray:arr, keyIndex:0, valueIndex:1, firstEmpty:false});
			UI_tabPayment.actPayMethodBsp = true;
		}
		
	
	}
	
	UI_tabPayment.showHideBasedOnResStatus = function(reservationStatus){
		
		if(reservationStatus ==null || reservationStatus==''){
			reservationStatus =DATA_ResPro.reservationInfo !=undefined?DATA_ResPro.reservationInfo.status:DATA_ResPro.reservationStatus;
		}
		if ((reservationStatus == 'OHD'&& (DATA_ResPro.initialParams.modifyOnholdConfirm || DATA_ResPro.initialParams.forceConfirm))){
			UI_tabPayment.handleForceConfirmBehavior(false);	
		} else if((DATA_ResPro.reservationStatus == 'CNF' || (DATA_ResPro.reservationInfo != undefined && DATA_ResPro.reservationInfo.status == 'CNX')) 
				&& DATA_ResPro.initialParams.partialPaymentModify) {
			UI_tabPayment.handleForceConfirmBehavior(true);
		}else if(!DATA_ResPro.initialParams.forceConfirm){
			$("#btnPayForce").hide();
			UI_tabPayment.showForceConfirm =false;
		}
	}
	
	/** -------------------------------------------------------------------------------------------------* */
	/** Taken from tabmakePayment * */
	
	var jsonCEObj = [
						{id:"#cardType", desc:"Card Type", required:true},
						{id:"#cardNo", desc:"Card No", required:true},
						{id:"#cardHoldersName", desc:"Card Holders Name", required:true},
						{id:"#expiryDate", desc:"Expiry Date", required:true},
						{id:"#cardCVV", desc:"Card CVV", required:true}
						];
						

		/*
		 * This method will call from the flight search booking button click
		 * Depend on the flight search data this informations should fill
		 */ 
		UI_tabPayment.paymentDetailsOnLoadCall = function(){			
			UI_tabPayment.constructGrid({id:"#tblPaxPayDet"});
			UI_tabPayment.buildPaymentSummary();
			UI_tabPayment.resetOnClick();			
			$("#payOptions").show();
			var payOpts = $('input[type="radio"][name="payment.type"]');
			if(payOpts.length ==1){
				payOpts.attr('checked',true);
				payOpts.click();
			}
			$("#creditCardDet").hide();
			$("#payOptButtons").show();
					
			$("#btnPayConfirm").hide();
			UI_commonSystem.fillGridData({id:"#tblPaxPayDet", data:jsonPaxPayObj});	
			$('#tblPaxPayDet').trigger('reloadGrid');
			UI_tabPayment.disableGridButtons();			
			UI_tabPayment.selectPayPax();
			if(UI_tabPayment.isReservation) {
				if(UI_tabSearchFlights.getSelectedSystem() != "AA") {
					$("#btnPayForce").hide();
					UI_tabPayment.showForceConfirm =false;
				}
				$("#lblPNr").hide();
				$("#divPnrNoPay").hide();
				$("#hdnOrigin").val(UI_tabSearchFlights.strOrigin);
				$("#hdnDestination").val(UI_tabSearchFlights.strDestination);
				$("#hdnSelCurrency").val(jsonPaySumObj.selectedCurrency );
			}else if(UI_tabPayment.isAddModifyAnci){
				$("#hdnSelCurrency").val(DATA_ResPro.selCurrency);
			}else if(UI_tabPayment.isSegModify){
				$("#hdnSelCurrency").val(UI_modifySegment.selCurrency);
			}else if(UI_tabPayment.isAddSegment){
				$("#hdnSelCurrency").val(UI_addSegment.selCurrency);
			}else if(UI_tabPayment.isRequote){
				$("#hdnSelCurrency").val(UI_modifyResRequote.getSelectedCurrencyCode());
			}else { // on balance payment
				$("#hdnSelCurrency").val(jsonResPaymentSummary.selectedCurrency );
			}
			if ((UI_tabPayment.isSegModify || UI_tabPayment.isAddModifyAnci || 
			        UI_tabPayment.isAddSegment || UI_tabPayment.isRequote)){
				UI_tabPayment.showHideBasedOnResStatus(DATA_ResPro.reservationInfo !=undefined?DATA_ResPro.reservationInfo.status:DATA_ResPro.reservationStatus);
				
			}  else if (UI_tabPayment.isSegModify || UI_tabPayment.isAddModifyAnci || 
                    UI_tabPayment.isAddSegment || UI_tabPayment.isRequote){
			    $("#btnPayForce").hide();
			    UI_tabPayment.showForceConfirm =false;
			}
			
			UI_tabPayment.validationForEnablingConfirmButton();			
						
			if(UI_tabPayment.isAddInfant) {
				$("#btnPayForce").hide();
				UI_tabPayment.showForceConfirm =false;
			}
			if (UI_tabPayment.isCsPnr) {
				$("#btnPayConfirm").hide();
				$("#btnPayForce").hide();
			}
		}				
		
		UI_tabPayment.disableGridButtons = function() {
			for(var gl=1;gl < jsonPaxPayObj.length+1;gl++){
				var balToPay = Number($("#tblPaxPayDet").getCell(gl,'displayToPay'));
				if(balToPay > 0) {
					$("#"+gl+"_displayCreditButton").enable();
				}else {
					$("#"+gl+"_displayCreditButton").disable();
				}
				var usdCredit = Number($("#tblPaxPayDet").getCell(gl,'displayUsedCredit'));
				if(usdCredit > 0) {
					$("#"+gl+"_displayRemoveButton").enable();
				}else {
					$("#"+gl+"_displayRemoveButton").disable();
				}
				
				if(UI_tabPayment.isBINPromotion()){
					$("#"+gl+"_displayCreditButton").disable();
					$("#"+gl+"_displayRemoveButton").disable();
				}
			}			
		}
		
		UI_tabPayment.selectPayPax = function() {
			for(var gl=0;gl < jsonPaxPayObj.length;gl++){
				jsonPaxPayObj[gl].hasPayment = "true";
			}			
		}
		
		
		/*
		 * Cancel Button Click
		 */
		UI_tabPayment.cancelOnClick = function(){
			
			if (UI_tabPayment.isModify) {				
				UI_reservation.openTab(0);			
			}else if(UI_tabPayment.isReservation) {
				UI_tabAnci.releaseBlockedAnci();
				UI_tabSearchFlights.resetOnClick();
				UI_makeReservation.openTab(0);
			}else if(UI_tabPayment.isSegModify){
				UI_modifySegment.openTab(0);
			} else if (UI_tabPayment.isAddModifyAnci){
				UI_modifyAncillary.openTab(0);
			} else if (UI_tabPayment.isAddSegment){
				UI_addSegment.openTab(0);
			} else if (UI_tabPayment.isRequote){
				UI_modifyResRequote.openTab(2);
			}
		}
		
		
		
		/*
		 * TODO Continue Click
		 */
		UI_tabPayment.continueOnClick = function(){
			var payModeSet = false;
			if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}
			UI_commonSystem.showProgress();		
			
			if($("#chkEmail").attr('checked') && $.trim($("#txtEmail").val()) == "") {
				UI_commonSystem.hideProgress();
				showERRMessage("TAIR-90025: To email the itinerary, Email address cannot be blank.");
				return false;
			}
			
			// using the old way undefined is not workin
			if(Number(jsonPaySumObj.balanceToPay) > 0 && (!$("#radPaymentTypeC").attr('checked') && !$("#radPaymentTypeOffline").attr('checked') && !$("#radPaymentTypeA").attr('checked') && !$("#radPaymentTypeR").attr('checked') && !$("#radPaymentTypeBsp").attr('checked'))
			    && ($("#txtPayMode").val() != "CONFIRM" && $("#txtPayMode").val() != "FORCE")){
				UI_commonSystem.hideProgress();
				showERRMessage(raiseError("XBE-ERR-01","Payment option"));
				return false;
			}
			
			if(!DATA_ResPro.initialParams.cashPayments 
					&& !DATA_ResPro.initialParams.offlinePayments
					&& !DATA_ResPro.initialParams.creditPayments 
					&& !DATA_ResPro.initialParams.onAccountPayments 
					&& !DATA_ResPro.initialParams.bspPayments && ($("#txtPayMode").val() != "CONFIRM" &&  $("#txtPayMode").val() != "FORCE")){
				UI_commonSystem.hideProgress();
				showERRMessage("TAIR-90026: Payment options not authorized.");
				return false;
			}
			
			switch ($("input[name='payment.type']:checked").val()){
				case UI_tabPayment.strCCCode :
					if(Number(jsonPaySumObj.balanceToPay) > 0 && ($("#txtPayMode").val() != "CONFIRM" &&  $("#txtPayMode").val() != "FORCE")){
						var groupBookingReference = $('#groupBookingReference').val();
						var partialPaymentAllowed = false;
						if(groupBookingReference != null && $.trim(groupBookingReference).length > 0){
							if(Number($.trim($('#minimumPayment').val()))>0){
								partialPaymentAllowed = true;
							}
							else{
								UI_commonSystem.hideProgress();
								showERRMessage("Please validate group booking request reference before continue with this group booking");
								return false;
								
							}
						}
						var paymentAmount=Number($.trim($('#paymentCreditCard').val()));
						if( (partialPaymentAllowed) && (isNaN(paymentAmount) || Number($.trim($('#minimumPayment').val())) > paymentAmount) ){
								UI_commonSystem.hideProgress();
								showERRMessage("Payment must be bigger than the minimum payment required");
								return false;
						}
						if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef && UI_tabPayment.payRefMandatory && $.trim($('#paymentReferenceR').val()) == ''){
							UI_commonSystem.hideProgress();
							showERRMessage(raiseError("XBE-ERR-01","Reference"));
							return false;
						} else {
							UI_tabPayment.identifyPGWBrokerType();
						}
						break;
					}					
				default :
					/* TODO Validation */
					var payType = $("input[name='payment.type']:checked").val();
					if(!$("#radPaymentTypeC").attr('checked') && !$("#radPaymentTypeOffline").attr('checked') && !$("#radPaymentTypeA").attr('checked') && !$("#radPaymentTypeR").attr('checked') && !$("#radPaymentTypeBsp").attr('checked') ){
						payType = 'ACCO';
					}
					
					var groupBookingReference = $('#groupBookingReference').val();
					var partialPaymentAllowed = false;
					if(groupBookingReference != null && $.trim(groupBookingReference).length > 0){
						if(Number($.trim($('#minimumPayment').val()))>0){
							partialPaymentAllowed = true;
						}
						else{
							UI_commonSystem.hideProgress();
							showERRMessage("Please validate group booking request reference before continue with this group booking");
							return false;
							
						}
					}
				
					//TODO check the reservation or balance payment on hold for partial payment on groupBooking Reference
					var creditPaxAmount=0;
					for(var gl=0;gl < jsonPaxPayObj.length;gl++){
						creditPaxAmount = creditPaxAmount+ Number(jsonPaxPayObj[gl].displayUsedCredit);
					}
					switch(payType)	{
					   case 'ACCO':
						if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef && UI_tabPayment.payRefMandatory && $.trim($('#paymentReferenceA').val()) == ''){
							UI_commonSystem.hideProgress();
							showERRMessage(raiseError("XBE-ERR-01","Reference"));
							return false;
						}
						
						var isSelAgentsSelected  = $.trim($('#selAgents').val()) != '';
						var isSelAgents2Selected = $.trim($('#selAgents2').val()) != '';
						
						//Double Agent Pay Validations
						if(DATA_ResPro.initialParams.onAccountMultiAgentPayments){
    						if(isSelAgentsSelected && isSelAgents2Selected ) {
    							
    							if($.trim($('#selAgents').val()) == $.trim($('#selAgents2').val())){
    								UI_commonSystem.hideProgress();
    								showERRMessage("Agents must be Diffrenet");
    								return false;
    							}else {
    								var firstCurrency = "";
    								var firstAGentInfo;
    								var secondCurrency = "";
    								var secondAGentInfo;
    								
    								for( var i=0; i < top.arrTravelAgents.length; i++) {
    									firstAGentInfo = top.arrTravelAgents[i];
    									if($("#selAgents").val() == firstAGentInfo[1]) {
    										firstCurrency = firstAGentInfo[2];
    										break;
    									}				
    								}	
    								
    								for( var i=0; i < top.arrTravelAgents.length; i++) {                                                                                                                                                                                                                                                                                                                                                                                                                                     
    									secondAGentInfo = top.arrTravelAgents[i];
    									if($("#selAgents2").val() == secondAGentInfo[1]) {
    										secondCurrency = secondAGentInfo[2];
    										break;
    									}				
    								}								
    								if(firstCurrency != secondCurrency){								
    									UI_commonSystem.hideProgress();
    									showERRMessage("Agents currencies must be same");
    									return false;
    								}else if($.trim($('#paymentAgentA').val()) == ''){
    									UI_commonSystem.hideProgress();
    									showERRMessage("First Agent amount is blank");
    									return false;
    								}else if($.trim($('#paymentAgentB').val()) == ''){
    									UI_commonSystem.hideProgress();
    									showERRMessage("Second Agent amount is blank");
    									return false;
    								}else if(isNaN($.trim($('#paymentAgentB').val())) || isNaN($.trim($('#paymentAgentA').val())) 
    										|| ( !partialPaymentAllowed && (Number(jsonPaySumObj.balanceToPay) != 
    										((Number($.trim($('#paymentAgentB').val())) +Number($.trim($('#paymentAgentA').val()))).toFixed(2))))){
    									UI_commonSystem.hideProgress();
    									showERRMessage("Total must match the payment amount");
    									return false;
    								}
    								else if(isNaN($.trim($('#paymentAgentB').val())) || isNaN($.trim($('#paymentAgentA').val())) 
    										|| ( partialPaymentAllowed && (Number($.trim($('#minimumPayment').val())) > 
    										((creditPaxAmount+Number($.trim($('#paymentAgentB').val())) +Number($.trim($('#paymentAgentA').val()))).toFixed(2))))){
    									UI_commonSystem.hideProgress();
    									showERRMessage("Payment must be bigger than the minimum payment required");
    									return false;
    								}
    							}
    						}
    						// Validation when paying using a single agent
    						else if(isSelAgentsSelected || isSelAgents2Selected){
    							var paymentAmount=creditPaxAmount;
    							var agentName;
    							if(isSelAgentsSelected){
    								paymentAmount = $.trim($('#paymentAgentA').val());
    								agentName = "First";
    								if($.trim($('#paymentAgentB').val()) != ''){
    									UI_commonSystem.hideProgress();
    									showERRMessage("Please select Second agent");
    									return false;
    								}
    							}else{
    								paymentAmount = $.trim($('#paymentAgentB').val());
    								agentName = "Second";
    								if($.trim($('#paymentAgentA').val()) != ''){
    									UI_commonSystem.hideProgress();
    									showERRMessage("Please select First agent");
    									return false;
    								}
    							}
    							if(paymentAmount == ''){
    								UI_commonSystem.hideProgress();
    								showERRMessage(agentName + " Agent amount is blank");
    								return false;
    							}
    							else if((!partialPaymentAllowed) && (isNaN(paymentAmount) || ((Number(jsonPaySumObj.balanceToPay) != paymentAmount) && (($("#hdnVoucherRedemption").val() != paymentAmount)||(!vouchersRedeemed))))){
    								UI_commonSystem.hideProgress();
    								showERRMessage("Total must match the payment amount");
    								return false;
    							}
    							else if( (partialPaymentAllowed) && (isNaN(paymentAmount) || Number($.trim($('#minimumPayment').val())) > paymentAmount) ){
    								UI_commonSystem.hideProgress();
    								showERRMessage("Payment must be bigger than the minimum payment required");
    								return false;
    							}
    						}else if(creditPaxAmount > 0){
    							if( partialPaymentAllowed && Number($.trim($('#minimumPayment').val())) > creditPaxAmount ){
    								UI_commonSystem.hideProgress();
    								showERRMessage("Payment must be bigger than the minimum payment required");
    								return false;
    							}
    						}else{		
    							var amount1 = parseFloat($('#paymentAgentA').val()); 
    							var amount2 = parseFloat($('#paymentAgentB').val()); 
    							
    							amount1 = isNaN(amount1) ? 0 : amount1;
    							amount2 = isNaN(amount2) ? 0 : amount2;    						
    					    							
    							if(partialPaymentAllowed && (creditPaxAmount + amount1 + amount2) < Number($.trim($('#minimumPayment').val()))){
    								UI_commonSystem.hideProgress();
    								showERRMessage("Payment must be bigger than the minimum payment required");
    								return false;
    							}    							
    						}    						
    						
						}else if(isNaN($.trim($('#paymentAgentA').val()))   // no agent selected and making payment from his/her own credit
									|| ( partialPaymentAllowed && (Number($.trim($('#minimumPayment').val())) > 
									((creditPaxAmount + Number($.trim($('#paymentAgentA').val()))).toFixed(2))))){
								UI_commonSystem.hideProgress();
								showERRMessage("Payment must be bigger than the minimum payment required");
								return false;
						}
						
						break;
					   case 'CRED':
						if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef && UI_tabPayment.payRefMandatory && $.trim($('#paymentReferenceR').val()) == ''){
							UI_commonSystem.hideProgress();
							showERRMessage(raiseError("XBE-ERR-01","Reference"));
							return false;
						}
						
						var paymentAmount=creditPaxAmount+Number($.trim($('#paymentCreditCard').val()));
						 if( (partialPaymentAllowed) && (isNaN(paymentAmount) || Number($.trim($('#minimumPayment').val())) > paymentAmount) ){
								UI_commonSystem.hideProgress();
								showERRMessage("Payment must be bigger than the minimum payment required");
								return false;
						}
					
						break;
					   case 'OFFLINE':
						    var selectedPGW = $("#selPaymentGateway").val();
						    var emailAddress = $("#txtEmail").val() || $('#contactEmailForOfflinePayments').val();
							var paymentAmount=creditPaxAmount+Number($.trim($('#paymentCreditCard').val()));
							 if(selectedPGW == ""){
									UI_commonSystem.hideProgress();
									showERRMessage("Select a payment gateway");
									return false;
							}
						    if (!emailAddress) {
								UI_commonSystem.hideProgress();
								showERRMessage("Email Address is Required");
								return false;
							}
						   if (emailAddress != null) {
							   var emailFilter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
							   if (!emailFilter.test(emailAddress)) {
								   UI_commonSystem.hideProgress();
								   showERRMessage("Please enter a valid email address.");
								   return false;
							   }
						   }
						   break;
					   case 'CASH':
						if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef && UI_tabPayment.payRefMandatory && $.trim($('#paymentReferenceC').val()) == ''){
							UI_commonSystem.hideProgress();
							showERRMessage(raiseError("XBE-ERR-01","Reference"));
							return false;
						}
						var paymentAmount=creditPaxAmount+Number($.trim($('#paymentCash').val()));
						 if( (partialPaymentAllowed) && (isNaN(paymentAmount) || Number($.trim($('#minimumPayment').val())) > paymentAmount) ){
								UI_commonSystem.hideProgress();
								showERRMessage("Payment must be bigger than the minimum payment required");
								return false;
						}
						break;
					   case 'BSP':
							if(DATA_ResPro.initialParams.allowCapturePayRef && UI_tabPayment.capturePayRef && UI_tabPayment.payRefMandatory && $.trim($('#paymentReferenceBsp').val()) == ''){
								UI_commonSystem.hideProgress();
								showERRMessage(raiseError("XBE-ERR-01","Reference"));
								return false;
						}
						break;
					}
					
					var applicableCommission = trim($("#divPAgentCommission").text());
					if(Number(applicableCommission) > 0){
						var proceed = true;	
						var paymentMode = $("#txtPayMode").val();
						if(paymentMode == "FORCE") {
							proceed = confirm(raiseError("XBE-CONF-06", "Force Confirm"));
						}
						if(paymentMode == "CONFIRM") {
							proceed = confirm(raiseError("XBE-CONF-06", "Confirm"));
						}
						if(!proceed){
							UI_commonSystem.hideProgress();
							return false;
						}
					}
					
					var frmUrl = "";
					var data = {};
					var paymentInfo = $('input,select').filterData('name','payment');
					data = $.extend(data, paymentInfo);
					
					
					if(data['payment.type'] == 'CRED'){	
						var creditInfo = $('input,select').filterData('name','creditInfo');
						data = $.extend(data, creditInfo);
						data['hdnIPGId']=$("#selCardType").val();
						data['payment.actualPaymentMethod'] = $("#selActualPayMethodR").val();
						data['payment.paymentReference'] = $("#paymentReferenceR").val();
						data['totalAmount']=Number($.trim($('#txtPaymentWithCharge').val()));
						if(UI_tabPayment.brokerType = "external"){
							data['totalAmount'] = $("#txtPayAmt").val();							
						}
					}
					
					if(data['payment.type'] == 'OFFLINE'){
						data['onHoldBooking'] = true;
						data['actualPayment'] = false;	
						data['hdnIPGId']=$("#selPaymentGateway").val();
						data['totalAmount']=Number($.trim($('#txtPaymentWithCharge').val()));
					}
					
					if(data['payment.type'] == 'ACCO'){
						data['payment.actualPaymentMethod'] = $("#selActualPayMethodA").val();
						data['payment.paymentReference'] = $("#paymentReferenceA").val();
						data['groupBookingReference'] = groupBookingReference;
						data['totalAmount']=Number($.trim($('#paymentAgentB').val())) +Number($.trim($('#paymentAgentA').val()));
						if(vouchersRedeemed) {
							data['payment.amount'] = $.trim($("#hdnVoucherRedemption").val());
						}
					}
					
					if(data['payment.type'] == 'CASH'){
						data['payment.actualPaymentMethod'] = $("#selActualPayMethodC").val();
						data['payment.paymentReference'] = $("#paymentReferenceC").val();
						data['groupBookingReference'] = groupBookingReference;
						data['totalAmount']=Number($.trim($('#paymentCash').val()));
						if(vouchersRedeemed) {
							data['payment.amount'] = $.trim($("#hdnVoucherRedemption").val());
						}
					}
					if(data['payment.type'] == 'BSP'){
						var totalPayAmountWithBSPFee = Number($.trim($('#totalPayAmountWithBSPFee').val()));
						
						data['payment.actualPaymentMethod'] = $("#selActualPayMethodBsp").val();
						data['payment.paymentReference'] = $("#paymentReferenceBsp").val();
						data['payment.amount'] = totalPayAmountWithBSPFee;
						data['totalAmount']= totalPayAmountWithBSPFee;
						if(vouchersRedeemed) {
							data['payment.amount'] = $.trim($("#hdnVoucherRedemption").val());
						}
					}
				    
				
						
					$("#txtPayAmt").enable();
					if ($('#chkEmail').is(':checked')) {
						data['chkEmail'] = "on";
					}					
					data['selLang'] = $("#selLang").val();
					
					if(DATA_ResPro.blnIsFromNameChange){
						data['paxNameDetails'] = DATA_ResPro.paxNameDetails;
					}					
					
				//	data['blackListPnrpaxList'] = UI_tabPassenger.blackListedPaxList;
					if (typeof UI_tabPassenger != 'undefined') {
						data['reasonForAllowBlPax'] = UI_tabPassenger.reasonForAllowBlPax;
					}
					if(UI_tabPayment.isReservation){
						frmUrl = "confirmationTab.action";										
						var contactInfo = $('input,select,textarea').filterData('name','contactInfo');

						if (DATA_ResPro.initialParams.loadProfile) {
							if($("#chkUpdateCustProf").attr("checked")){
								data['customerProfileUpdate'] = true;
							} else {
								data['customerProfileUpdate'] = false;
							}
						} else {
							data['customerProfileUpdate'] = false;
						}
						
						
						data = $.extend(data, contactInfo);
						data['contactInfo.email'] = data['contactInfo.email'] || $('#contactEmailForOfflinePayments').val();
						
						data['hdnOrigin']=UI_tabSearchFlights.strOrigin;
						data['hdnDestination']=UI_tabSearchFlights.strDestination;
						data['hdnSelCurrency']=jsonPaySumObj.selectedCurrency;
						data['paxInfants'] = UI_tabPassenger.createInfantPaxInfo();		
						data['selectedFlightList'] = $.toJSON(UI_tabPayment.getFlightRPHList());	
						data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
						data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
						data['selectedBookingCategory'] = $('#selBookingCategory').val();
						data['paxState'] = UI_tabSearchFlights.paxState;
						data['paxCountryCode'] = UI_tabSearchFlights.paxCountryCode;
						data['paxTaxRegistered'] = UI_tabSearchFlights.paxTaxRegistered;
						
						if (top.arrPrivi[PRIVI_ITINERARY_FARE_MASK] == 1) {
							data['itineraryFareMaskOption'] = $('#selItineraryFareMask').val();
						} else {
							data['itineraryFareMaskOption'] = 'N';
						}
						
						var searchData = null; // check whether Calender minimum fare or not
						if(UI_tabSearchFlights.isCalenderMinimumFare()){
							data = $.airutil.dom.concatObjects(data, UI_MinimumFare.inOutFlightRPHList())
							searchData= UI_MinimumFare.createSearchParams('searchParams');
						}else{
							data = $.airutil.dom.concatObjects(data, UI_tabSearchFlights.inOutFlightRPHList())
							searchData=  UI_tabSearchFlights.createSearchParams('searchParams');
						}

						if (top.arrPrivi[PRIVI_ADD_CALL_ORIGIN_COUNTRY] == 1) {
							data['selectedOriginCountry'] = $('#selOriginCountryOfCall').val();
						} else {
							data['selectedOriginCountry'] = '';
						}
						
						data = $.airutil.dom.concatObjects(data,searchData);
					}else if(UI_tabPayment.isModify){						
						if(UI_tabPayment.isAddInfant){
							paymentType = data['payment.type'];
							UI_tabBookingInfo.saveInfantsOnClick("SAVEINF");
							return;
						}						
						frmUrl = "paxAccountPaymentConfirm.action";												
						data['version'] = jsonBkgInfo.version;
						data['resContactInfo'] = UI_reservation.contactInfo;
						data['resPax'] = UI_reservation.jsonPassengers;
						data['pnr'] = jsonPNR;
						data['groupPNR'] = jsonGroupPNR;
						data['pnrSegments'] = UI_reservation.jsonSegs;
						data['hdnSelCurrency'] = $('#hdnSelCurrency').val();
						data['isInfantPaymentSeparated'] = UI_reservation.isInfantPaymentSeparated;
					}else if(UI_tabPayment.isSegModify){						
						frmUrl = "modifyPaymentConfirm.action";
						data['ownerAgentCode'] = DATA_ResPro.modifySegments.ownerAgentCode;
						data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
						data['pnr'] = DATA_ResPro.modifySegments.PNR;
						data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;		
						data['hdnSelCurrency'] = $('#hdnSelCurrency').val();
						data['userNote'] = UI_modifySegment.userNote;
					}else if(UI_tabPayment.isAddModifyAnci){
						frmUrl = "modifyAnciPaymentConfirm.action";
						data['version'] = DATA_ResPro.version;
						if($("#txtPayMode").val() == "FORCE") {
						//if($("#txtPayMode").val() == "CONFIRM" || $("#txtPayMode").val() == "FORCE") {
							data['forceConfirm'] = true;
							payModeSet = true;
						} else {
							data['forceConfirm'] = false;
						}
						data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
						data['pnr'] = DATA_ResPro.pnrNo;
						data['groupPNR'] = DATA_ResPro.groupPNRNo;
						data['pnrSegments'] = $.toJSON(DATA_ResPro.flightRPHList);
						data['resPaxs'] =$.toJSON(DATA_ResPro.resPaxs);
						data['hdnSelCurrency'] = $('#hdnSelCurrency').val();
					} else if(UI_tabPayment.isAddSegment){						
						frmUrl = "addSegmentPaymentConfirm.action";
						data['hdnSelCurrency'] = $('#hdnSelCurrency').val();			
						data['pnr'] = DATA_ResPro.reservationInfo.PNR;
						data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;
						data['ownerAgentCode'] = DATA_ResPro.reservationInfo.ownerAgentCode;
						data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);						
					} else if(UI_tabPayment.isRequote){						
						frmUrl = "requotePaymentConfirm.action";
						data = $.extend(data, UI_requote.paymentSubmitData());
//						data['ownerAgentCode'] = DATA_ResPro.reservationInfo.ownerAgentCode;
//						data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
//						data['pnr'] = DATA_ResPro.reservationInfo.PNR;
//						data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;		
						data['hdnSelCurrency'] = $('#hdnSelCurrency').val();
						//data["reasonForAllowBlPax"] = UI_tabBookingInfo.reasonForAllowBlPax;
						data["reasonForAllowBlPax"] = DATA_ResPro.reasonForAllowBlPax;
						if($("#txtPayMode").val() != "FORCE" && UI_tabPayment.brokerType == "external"){
							data['payment.mode'] = "";							
						}
					}
					
					var selectedAgent = $('#selAgents').val();
					if(!selectedAgent || trim(selectedAgent) == "") {
						// If no agent is selected, or the drop down box is
						// disabled, then use the logged in agent
						selectedAgent = top.strAgentCode;
					}	
					data['paxPayments'] = $.toJSON(jsonPaxPayObj);
					data['payment.agent'] = selectedAgent;
					
					if(!$("#radPaymentTypeC").attr('checked') && !$("#radPaymentTypeA").attr('checked') && !$("#radPaymentTypeOffline").attr('checked') && !$("#radPaymentTypeR").attr('checked') && !$("#radPaymentTypeBsp").attr('checked')){
						data['payment.type'] = "ACCO"; 
					}
					
					if(!payModeSet){
						if($("#txtPayMode").val() == "FORCE") {
							data['forceConfirm'] = true;
						} else {
							data['forceConfirm'] = false;
							data['actualPayment'] = true;
						}	
					}
					
					if(!UI_tabPayment.paymentProcessed){
						UI_tabPayment.paymentProcessed = true;
						UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:frmUrl,							
							success: UI_tabPayment.continueOnClickSuccess, error:UI_commonSystem.setErrorStatus});
						
					}
					break;
			}
		}
		
		/*
		 * Payment Continue Click
		 */
		UI_tabPayment.continueOnClickSuccess = function(response){			
			UI_tabPayment.paymentProcessed = false;
			UI_commonSystem.hideProgress();	
			
			if (response != null){
				if(!response.success){
					if(UI_tabPayment.isModify){
						eval(response.reservationResponse);
						UI_tabBookingInfo.onLoadCall();						
						UI_tabContactInfo.onLoadCall();
					}
					if(response.ohdResExpired != undefined && response.ohdResExpired){
						alert(response.messageTxt);
						response.messageTxt = "";
						var groupPnr = "";
						var pnr = "";
						if(UI_tabPayment.pnr != ""){
							pnr = UI_tabPayment.pnr;
						}else if(response.pnr != undefined && response.pnr != ""){
							pnr = response.pnr;
						}
						if(response.groupPNR != undefined && response.groupPNR != "" ){
							groupPnr = response.groupPNR;
						}
						if(response.originallyGroupPNR != undefined && !response.originallyGroupPNR){
								groupPnr = "";
						}
						location.replace("showNewFile!loadRes.action?pnrNO=" + pnr+"&groupPNRNO="+groupPnr);
					}
					showERRMessage(response.messageTxt);
					return false;
				}
			}
			// issue fix- Credit card payment with wrong payment gateway
			if(UI_tabPayment.isReservation){
				UI_tabPayment.fillDropDownData();	
			}	
			
			UI_tabPayment.cancelCROnClick();
			UI_tabPayment.ccEnableTab(true);
			
			// Updating the group pnr if other carrier pax credit is used
			UI_tabPayment.updateGroupPnr(response);
			
			if(response.agentBalance != null){
				top.intAgtBalance = response.agentBalance;
				top.agentAvailableCreditInAgentCurrency = response.agentBalanceInAgentCurrency;
			}
			if(DATA_ResPro.initialParams.viewAvailableCredit) {
				UI_commonSystem.showAgentCredit();
				UI_commonSystem.showAgentCreditInAgentCurrency();
			} else {
				$("#divCreditPnl").hide();
			}
			if(UI_tabPayment.isReservation){
				UI_commonSystem.showProgress();
				UI_tabItinerary.itineraryDetailsOnLoadCall(response);
				UI_commonSystem.hideProgress();
				if (UI_tabPayment.strPaymentMsg == "NOPAY") {
					showCommonError("CONFIRMATION", "Booking is successfully force confirmed");
				}
				
				UI_makeReservation.tabDataRetrive.tab1 = true;
				UI_makeReservation.tabDataRetrive.tab2 = true;
				UI_makeReservation.tabDataRetrive.tab3 = true;
				
				UI_makeReservation.tabStatus.tab1 = false;
				UI_makeReservation.tabStatus.tab2 = false;
				UI_makeReservation.tabStatus.tab3 = false;
				
				UI_makeReservation.tabStatus.tab4 = true;
				UI_makeReservation.openTab(4);
			}else if(UI_tabPayment.isModify){
				if (UI_tabPayment.strPaymentMsg == "NOPAY") {
					UI_reservation.loadReservation("NOPAY", response,false);					
				} else  {
					UI_reservation.loadReservation("BALPAY", response,false);
				}
				return false;						
			}else if(UI_tabPayment.isSegModify){				
				UI_modifySegment.loadHomePage();
				return;
			}else if (UI_tabPayment.isAddModifyAnci){
				UI_modifyAncillary.loadHomePage();
				return ;
			} else if (UI_tabPayment.isAddSegment){
				UI_addSegment.loadHomePage();
				return ;
			} else if (UI_tabPayment.isRequote){
				UI_modifyResRequote.loadHomePage();
			}
		}
		

		UI_tabPayment.getRedirectRequestDataSuccess = function(response){
			var winpops;
			if(response.redirectionMechanism == "FORM"){
				winpops = window.open('',
						"_blank",
				"toolbar=no,scrollbars=no,resizable=no,top=10,left=10,width=800,height=800");
				if(!winpops || winpops.closed || typeof winpops.closed=='undefined'){
					UI_commonSystem.hideProgress();
					showERRMessage("Popup Blocked. Please Enable and Check");
				} else {
					popUpManuallyClosed = false;
					winpops.document.write('<div id="content">'+response.redirectionString+'</div>');									
				}
			} else if(response.redirectionMechanism == "URL"){
				winpops = window.open(response.redirectionString,
						"_blank",
				"toolbar=no,scrollbars=no,resizable=no,top=10,left=10,width=800,height=800");
			}
			var timer = setInterval(function() { 
				if(winpops.closed && !popUpManuallyClosed) {
					clearInterval(timer);
					UI_tabPayment.externalPGWPageClosed();
				}
			}, 1000);
		}

		UI_tabPayment.externalPGWPageClosed = function(){
			UI_commonSystem.hideProgress();
			//Call Refund Function
		}
		
		UI_tabPayment.externalPaymentGatewayFeedback = function(){
            UI_commonSystem.hideProgress();
            popUpManuallyClosed = true;
            UI_tabPayment.payConfirmExecute();
		}
		
		UI_tabPayment.getRedirectRequestData = function(){
			var data = {};
			data["pgwId"]=$("#selCardType").val();
			var contactInfo = $('input,select,textarea').filterData('name','contactInfo');
			data = $.extend(data, contactInfo);
			if(data['contactInfo'] == undefined){
				data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);				
			}
			
			data['amount'] 	= jsonPaySumObj.balanceToPaySelcur;
  	  if (jsonPaySumObj.balanceToPaySelcur == null && jsonPaySumObj.currency == jsonPaySumObj.selectedCurrency) {
  	       data['amount'] = jsonPaySumObj.balanceToPay;
  	  }
			
			if(UI_tabPayment.isReservation){
				data["pnr"] 	    = "";
				data['selectedCurrency'] = jsonPaySumObj.selectedCurrency;
			} else if(UI_tabPayment.isAddSegment){
				data['pnr'] = DATA_ResPro.reservationInfo.PNR;
				data['selectedCurrency'] = UI_addSegment.selCurrency;
			} else if(UI_tabPayment.isRequote){
				data['pnr'] = DATA_ResPro.reservationInfo.PNR;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
			} else if(UI_tabPayment.isModify) {
				data["pnr"] 	    = jsonPNR;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
			} else if(UI_tabPayment.isSegModify){
				data['pnr'] = DATA_ResPro.modifySegments.PNR;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
			} else if (UI_tabPayment.isAddModifyAnci){
				data['pnr'] = DATA_ResPro.pnrNo;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
			} else if(jsonPaySumObj != undefined && jsonPaySumObj != null && jsonPaySumObj.currency != null ){
				data['selectedCurrency'] = jsonPaySumObj.currency;
			}
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'generateExternalPGWFormRequest.action', dataType: 'json', data:data,
				success: UI_tabPayment.getRedirectRequestDataSuccess, 
				error:UI_commonSystem.setErrorStatus});
		}
		
		UI_tabPayment.redirectToInternalExternalPGW = function(response){			
			if (response != null){
				if(response.success) {
					UI_tabPayment.brokerType = response.pgwBrokerType;
					if(response.pgwBrokerType == "external"){						
						UI_tabPayment.getRedirectRequestData();
					} else {
						UI_tabPayment.creditCardPayment();
					}
				} else {
					showERRMessage(response.messageTxt);
					return false;
				}
			}
		}
		
		UI_tabPayment.identifyPGWBrokerType = function(){
			var data = {};
			data["pgwId"]=$("#selCardType").val();
			if(UI_tabPayment.isReservation){
				data['selectedCurrency'] = jsonPaySumObj.selectedCurrency;
			} else if(UI_tabPayment.isAddSegment){
				data['selectedCurrency'] = UI_addSegment.selCurrency;
			} else if(UI_tabPayment.isRequote){
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
			}
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'derivePGWConfig.action', dataType: 'json', data:data,
				success: UI_tabPayment.redirectToInternalExternalPGW, 
				error:UI_commonSystem.setErrorStatus});
		}

		UI_tabPayment.setPaymentData = function(data) {
			
			if(Number($.trim($("#paymentCreditCard").val()))>0){
				data["payment.amount"]	= $("#paymentCreditCard").val();
			}
			else{
				data["payment.amount"]	= $("#txtPayAmt").val();
			}
			data["payment.mode"] = $("#txtPayMode").val();
			
		}		

		/*
		 * Credit Card Payment
		 */
		UI_tabPayment.creditCardPayment = function(){	
			
			$("#hdnIPGId").val($("#selCardType").val());
			$("#hdnIPGDesc").val($("#selCardType").text());
			$("#hdnSysDate").val(top.strSysDate);
				
			var data = {};
			data["hdnIPGId"]=$("#selCardType").val();
			data["hdnIPGDesc"]=$("#selCardType").text();
			data["hdnSysDate"]=top.strSysDate;
			
			UI_tabPayment.setPaymentData(data);
			// var searchData =
			// UI_tabSearchFlights.createSearchParams('searchParams');
			// data = $.airutil.dom.concatObjects(data,searchData);
			if ($('#chkEmail').is(':checked')) {
				data['chkEmail'] = "on";
			}
			
			var validOption = true;
			// Changing the form for reservation and modify reservation
			if(UI_tabPayment.isReservation){
				data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
				data["groupPNR"]	= "";
				data["pnr"] 	    = "";
				data['selectedCurrency'] = jsonPaySumObj.selectedCurrency;
				// to calculate the fixed value transaction fee
				data['selectedFlightListSize'] = UI_tabPayment.getFlightRPHList().length; // $.toJSON(UI_tabSearchFlights.getFlightRPHList());
				data['selectedFlightList'] = $.toJSON(UI_tabPayment.getFlightRPHList());
		        data['paxState'] = UI_tabSearchFlights.paxState;
		        data['paxCountryCode'] = UI_tabSearchFlights.paxCountryCode;
		        data['paxTaxRegistered'] = UI_tabSearchFlights.paxTaxRegistered;
				var searchData = UI_tabSearchFlights.createSearchParams('searchParams');
				data = $.airutil.dom.concatObjects(data,searchData);
			}else if(UI_tabPayment.isModify) {
				data['flightSegmentToList'] = $.toJSON(UI_reservation.flightSegmentToList);
				data['resContactInfo'] = UI_reservation.contactInfo;
				data['paxWiseAnci'] = $.toJSON(UI_reservation.paxWiseAnci);
				//UI_reservation.fltRPHInfo;
				//UI_reservation.getFlightRPHList();
				data['selectedFareType'] = '';
				data["groupPNR"]	= jsonGroupPNR;
				data["pnr"] 	    = jsonPNR;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
				// to calculate the fixed value transaction fee
				data['selectedFlightListSize'] = UI_reservation.totalSegmentCount;
			} else if(UI_tabPayment.isSegModify){
				data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
				data['pnr'] = DATA_ResPro.modifySegments.PNR;
				data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
				// to calculate the fixed value transaction fee
				data['selectedFlightListSize'] = UI_tabPayment.getFlightRPHList().length; // $.toJSON(UI_tabSearchFlights.getFlightRPHList());
			} else if (UI_tabPayment.isAddModifyAnci){
				// params for apply service tax for cc fee
				data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
				data['flightRPHList'] = $.toJSON(DATA_ResPro["flightRPHList"]);
				
				data['selectedFareType'] = '';
				data['pnr'] = DATA_ResPro.pnrNo;
				data['groupPNR'] = DATA_ResPro.groupPNRNo;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
				// to calculate the fixed value transaction fee
				data['selectedFlightListSize'] = UI_tabAnci.modifiedSegemntCount;  // DATA_ResPro["flightRPHList"].length;
																					// //jsonFltDetails.length;
			} else if(UI_tabPayment.isAddSegment){
				data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
				data['pnr'] = DATA_ResPro.reservationInfo.PNR;
				data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;
				data['selectedCurrency'] = UI_addSegment.selCurrency;
				// to calculate the fixed value transaction fee
				data['selectedFlightListSize'] = UI_tabPayment.getFlightRPHList().length; // $.toJSON(UI_tabSearchFlights.getFlightRPHList());
			} else if(UI_tabPayment.isRequote){
				// params for apply service tax for cc fee
				data = UI_tabPayment.addFareQuoteParams(data);
				data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
				data['flightRPHList'] = $.toJSON(UI_requote.populateFlightSegmentList());
				data['nameChangeFlightSegmentList'] = $.toJSON(DATA_ResPro.resSegments);
				data['resPaxs'] = $.toJSON(DATA_ResPro.resPaxs);
				data['addSegment'] = DATA_ResPro.isAddSegment;
				
				data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
				data['pnr'] = DATA_ResPro.reservationInfo.PNR;
				data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;
				data['selectedCurrency'] = $('#hdnSelCurrency').val();
				// to calculate the fixed value transaction fee
				data['selectedFlightListSize'] = UI_requote.getCCChargeableFltSegmentsCount(); // $.toJSON(UI_tabSearchFlights.getFlightRPHList());
				data['selectedFlightList'] = $.toJSON(UI_requote.populateFlightSegmentList());
		        data['paxState'] = UI_tabSearchFlights.paxState;
		        data['paxCountryCode'] = UI_tabSearchFlights.paxCountryCode;
		        data['paxTaxRegistered'] = UI_tabSearchFlights.paxTaxRegistered;
		        var searchData = UI_tabSearchFlights.createSearchParams('searchParams');
				data = $.airutil.dom.concatObjects(data,searchData);
			} else {
				validOption = false;
			}			
			data['vouchersRedeemed'] = vouchersRedeemed;
			if(validOption && !UI_tabPayment.paymentProcessed){
				UI_tabPayment.paymentProcessed = true;
				UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'creditCardPayment.action', dataType: 'json', type: 'post', processData: false, data:data,
					success: UI_tabPayment.applyCreditCardSuccess, 
					error:function(response){UI_tabPayment.paymentProcessed = false; UI_commonSystem.setErrorStatus(response)}});
			}
		}
		
		UI_tabPayment.addFareQuoteParams = function(data){
			var fareQuoteParamsData = UI_tabSearchFlights.createFareQuoteParams();
			fareQuoteParamsData['fareQuoteParams.ondListString'] = $.toJSON(UI_requote.getONDSearchDTOList());
			fareQuoteParamsData['fareQuoteParams.ticketValidTillStr'] = UI_requote.getTicketValidTill();
			fareQuoteParamsData['fareQuoteParams.lastFareQuoteDateStr'] = UI_requote.getLastFareQuoteDate();
			delete fareQuoteParamsData['fareQuoteParams.bookingClassCode'] ;
					
			var mergedData = {};
			mergedData = Object.assign(fareQuoteParamsData, data);
			return mergedData;
		}
		
		UI_tabPayment.applyCreditCardSuccess = function(response){
			UI_tabPayment.paymentProcessed = false;
			UI_commonSystem.hideProgress();
			if (response != null){
				if(response.success) {
					$("#payOptions").hide();
					$("#creditCardDet").show();
					// paypal
					 if($("#selCardType").find("option:selected").text()=="PAYPAL MOTO"){
						 $("#extraCardDetailForPayPalPannel").show();
				      }
					$("#payOptButtons").hide();
					$("#divDisableLayer").show();
					var selectedCC = $("#cardType").val();
					$("#cardType").children().remove();
					var arr = top.mapCCTypeForPgw[$("#selCardType").val()];
					arr = (arr==null?[]:arr);
					$("#cardType").fillDropDown( { dataArray: arr, keyIndex:0 , valueIndex:1, firstEmpty:true});
					$("#cardType").val(selectedCC);
					var strCCMsg = top.spMsgCC;					
					$("#divCCMsg").html(strCCMsg);
					UI_tabPayment.buildCreditBalanceToPay(response);
					UI_tabPayment.ccEnableTab(false);
					$("#cardType").focus();
					$("#sendEmail").val(response.sendEmail);
				}else {
					showERRMessage(response.messageTxt);
					return false;
				}			
			}	
		}
		
		/*
		 * Credit Card Confirm Click
		 */
		UI_tabPayment.confirmOnClick = function(){
			if (UI_tabPayment.validate()){
				UI_commonSystem.showProgress();
				$("#txtPayAmt").enable();
				
				var data = {};
				var paymentInfo = $('input,select').filterData('name','payment');
				data = $.extend(data, paymentInfo);
				if(data['payment.type'] == 'CRED'){
					var creditInfo = $('input,select').filterData('name','creditInfo');
					data = $.extend(data, creditInfo);
					data['hdnIPGId']=$("#selCardType").val();
					data['payment.actualPaymentMethod'] = $("#selActualPayMethodR").val();
					data['payment.paymentReference'] = $("#paymentReferenceR").val();
					data["payment.amount"]	= Number($.trim($('#txtPaymentWithCharge').val()));
					if(UI_tabPayment.brokerType == "external"){
						data['totalAmount'] = $("#txtPayAmt").val();							
					} else {
						data['totalAmount']=Number($.trim($('#txtPaymentWithCharge').val()));
					}
				}
				
				data['actualPayment'] = true;
				
				var groupBookingReference = $('#groupBookingReference').val();
				var partialPaymentAllowed = false;
				if(groupBookingReference != null && $.trim(groupBookingReference).length > 0){
					if(Number($.trim($('#minimumPayment').val()))>0){
						partialPaymentAllowed = true;
						data['groupBookingReference'] = groupBookingReference;
					}
					else{
						UI_commonSystem.hideProgress();
						showERRMessage("Please validate group booking request reference before continue with this group booking");
						return false;
						
					}
				}
				
				if(DATA_ResPro.blnIsFromNameChange){
					data['paxNameDetails'] = DATA_ResPro.paxNameDetails;
				}
				
				if(UI_tabPayment.isReservation) {
					
					var selectedAgent = $('#selAgents').val();
					if(!selectedAgent || trim(selectedAgent) == "") {
						// If no agent is selected, or the drop down box is
						// disabled, then use the logged in agent
						selectedAgent = top.strAgentCode;
					}						
					
					data['paxInfants'] = UI_tabPassenger.createInfantPaxInfo();
					// .val is not working short cut
					var contactInfo = $('input,select,textarea').filterData('name','contactInfo');
					data = $.extend(data, contactInfo);
					data['contactInfo.email'] = data['contactInfo.email'] || $('#contactEmailForOfflinePayments').val();
					data['hdnOrigin']=UI_tabSearchFlights.strOrigin;
					data['hdnDestination']=UI_tabSearchFlights.strDestination;
					data['hdnSelCurrency']=jsonPaySumObj.selectedCurrency;
					data['payment.agent'] = selectedAgent;
					data['paxPayments'] = $.toJSON(jsonPaxPayObj);
					data['selectedFlightList'] = $.toJSON(UI_tabPayment.getFlightRPHList());	
					data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
					data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
					data['chkEmail'] = $('#sendEmail').val();
					data['selectedBookingCategory'] = $('#selBookingCategory').val();
					
					if (top.arrPrivi[PRIVI_ITINERARY_FARE_MASK] == 1) {
						data['itineraryFareMaskOption'] = $('#selItineraryFareMask').val();
					} else {
						data['itineraryFareMaskOption'] = 'N';
					}

					if (top.arrPrivi[PRIVI_ADD_CALL_ORIGIN_COUNTRY] == 1) {
						data['selectedOriginCountry'] = $('#selOriginCountryOfCall').val();
					} else {
						data['selectedOriginCountry'] = '';
					}



					if($("#txtPayMode").val() == "FORCE") {
						data['forceConfirm'] = true;
						data['actualPayment'] = false;
					} else {
						data['forceConfirm'] = false;
					}
					
					//data['blackListPnrpaxList'] = UI_tabPassenger.blackListedPaxList;
					if (typeof UI_tabPassenger != 'undefined') {
						data['reasonForAllowBlPax'] = UI_tabPassenger.reasonForAllowBlPax;
					}
					
					var searchData = {};
					if (UI_tabSearchFlights.isCalenderMinimumFare()) {
						searchData = UI_MinimumFare.createSearchParams('searchParams');
					} else {
						searchData = UI_tabSearchFlights.createSearchParams('searchParams');
					}
					data = $.airutil.dom.concatObjects(data,searchData);				
					data = $.airutil.dom.concatObjects(data, UI_tabSearchFlights.inOutFlightRPHList())
					data['paxState'] = UI_tabSearchFlights.paxState;
					data['paxCountryCode'] = UI_tabSearchFlights.paxCountryCode;
					data['paxTaxRegistered'] = UI_tabSearchFlights.paxTaxRegistered;
					if(!UI_tabPayment.paymentProcessed){
						UI_tabPayment.paymentProcessed = true;
						UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', processData: false, data:data, url:"confirmationTab.action",							
							success: UI_tabPayment.continueOnClickSuccess, 
							error:function(response){UI_tabPayment.paymentProcessed = false;UI_commonSystem.setErrorStatus(response)}});			
					}
				}else if(UI_tabPayment.isModify) {
					if(UI_tabPayment.isAddInfant){
						paymentType = data['payment.type'];
						UI_tabBookingInfo.saveInfantsOnClick("SAVEINF");
						return;
					}	
					data['paxPayments'] = $.toJSON(jsonPaxPayObj);
					data['version']=jsonBkgInfo.version;
					data['resContactInfo']=UI_reservation.contactInfo;
					data['resPax']=UI_reservation.jsonPassengers;
					data['pnr'] = jsonPNR;
					data['groupPNR'] = jsonGroupPNR;
					data['pnrSegments'] = UI_reservation.jsonSegs;
					data['hdnSelCurrency'] = $('#hdnSelCurrency').val();	
					data['ownerAgentCode'] = UI_reservation.ownerAgent;
					data['isInfantPaymentSeparated'] = UI_reservation.isInfantPaymentSeparated;
					if(!UI_tabPayment.paymentProcessed){
						UI_tabPayment.paymentProcessed = true;
						UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', processData: false, data:data, url:"paxAccountPaymentConfirm.action",	
													success: UI_tabPayment.continueOnClickSuccess,
													error:function(response){UI_tabPayment.paymentProcessed = false;UI_commonSystem.setErrorStatus(response)}});
					}
				}else if(UI_tabPayment.isSegModify){					
					data['paxPayments'] = $.toJSON(jsonPaxPayObj);
					data['ownerAgentCode'] = DATA_ResPro.modifySegments.ownerAgentCode;
					data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
					data['pnr'] = DATA_ResPro.modifySegments.PNR;
					data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
					data['hdnSelCurrency'] = $('#hdnSelCurrency').val();	
					if(!UI_tabPayment.paymentProcessed){
						UI_tabPayment.paymentProcessed = true;
						UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', processData: false, data:data, url:"modifyPaymentConfirm.action",
							success: UI_tabPayment.continueOnClickSuccess,
							error:function(response){UI_tabPayment.paymentProcessed = false;UI_commonSystem.setErrorStatus(response)}});
					}
				}else if(UI_tabPayment.isAddModifyAnci){
					data['version'] = DATA_ResPro.version;
					data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
					data['pnr'] = DATA_ResPro.pnrNo;
					data['groupPNR'] = DATA_ResPro.groupPNRNo;
					data['paxPayments'] = $.toJSON(jsonPaxPayObj);
					data['pnrSegments'] = $.toJSON(DATA_ResPro.flightRPHList);
					data['hdnSelCurrency'] = $('#hdnSelCurrency').val();	
					data['ownerAgentCode'] = DATA_ResPro.ownerAgentCode;	
					data['resPaxs'] =$.toJSON(DATA_ResPro.resPaxs);
					if(!UI_tabPayment.paymentProcessed){
						UI_tabPayment.paymentProcessed = true;
						UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', processData: false, data:data,url:"modifyAnciPaymentConfirm.action",
							success: UI_tabPayment.continueOnClickSuccess,
							error:function(response){UI_tabPayment.paymentProcessed = false;UI_commonSystem.setErrorStatus(response)}});
					}
				} else if(UI_tabPayment.isAddSegment){						
					data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
					data['paxPayments'] = $.toJSON(jsonPaxPayObj);
					data['pnr'] = DATA_ResPro.reservationInfo.PNR;
					data['groupPNR'] = DATA_ResPro.reservationInfo.groupPNR;
					data['hdnSelCurrency'] = $('#hdnSelCurrency').val();	
					data['ownerAgentCode'] = DATA_ResPro.reservationInfo.ownerAgentCode;					
					if(!UI_tabPayment.paymentProcessed){
						UI_tabPayment.paymentProcessed = true;
						UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', processData: false, data:data, url:"addSegmentPaymentConfirm.action",
							success: UI_tabPayment.continueOnClickSuccess,
							error:function(response){UI_tabPayment.paymentProcessed = false;UI_commonSystem.setErrorStatus(response)}});
					}
				} else if(UI_tabPayment.isRequote){					
					data['paxPayments'] = $.toJSON(jsonPaxPayObj);
//					data['ownerAgentCode'] = DATA_ResPro.modifySegments.ownerAgentCode;
//					data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
//					data['pnr'] = DATA_ResPro.modifySegments.PNR;
//					data['groupPNR'] = DATA_ResPro.modifySegments.groupPNR;
					data['hdnSelCurrency'] = $('#hdnSelCurrency').val();						
					data = $.extend(data, UI_requote.paymentSubmitData());
					if($("#txtPayMode").val() != "FORCE" && UI_tabPayment.brokerType == "external"){
						data['payment.mode'] = "";							
					}
					if(!UI_tabPayment.paymentProcessed){
						UI_tabPayment.paymentProcessed = true;
						UI_commonSystem.getDummyFrm().ajaxSubmit({ dataType: 'json', processData: false, data:data, url:"requotePaymentConfirm.action",
							success: UI_tabPayment.continueOnClickSuccess,
							error:function(response){UI_tabPayment.paymentProcessed = false;UI_commonSystem.setErrorStatus(response)}});
					}
				}
			}
		}
		
		/*
		 * Validate Passenger Tab Data
		 */
		UI_tabPayment.validate = function(){
			if (!UI_tabPayment.validatePageControl(jsonCEObj)){return false;}
			if (!ValidateCardNos($("#cardType").val(), $("#cardNo").val())){
				showERRMessage(raiseError("XBE-ERR-04","Credit card No"));
				$("#cardNo").focus();
				return false;
			}
			if(UI_tabPayment.pickBINPromotionSucess){
				var currentBIN = $('#cardNo').val().slice(0,6);
				if(currentBIN !=UI_tabPayment.promotionBIN){
					UI_tabPayment.onBinPromotionMissmatch();
					return false;
				}
			}
			
			if ($("#expiryDate").val().length < 4){
				showERRMessage(raiseError("XBE-ERR-04","Expriy Date"));
				$("#expiryDate").focus();
				return false;
			}
			
			var strFullYear = top.strSysDate.substr(6,4);
			var strExpriyDateMM = $("#expiryDate").val().substr(0,2);
			var strExpriyDateYY = $("#expiryDate").val().substr(2,4);
			if (!CheckDates("01/" + top.strSysDate.substr(3,2) + "/" + strFullYear.substr(2,2) , "01/" + strExpriyDateMM + "/" + strExpriyDateYY)){
				showERRMessage(raiseError("XBE-ERR-03","Card expiry date", "current month"));
				$("#expiryDate").focus();
				return false;
			}			
			
			var applicableBINs = UI_tabPayment.getPromotionApplicableBINs();
						
			if(applicableBINs != null && (applicableBINs.length > 0)){
				var cardNo = $("#cardNo").val();
				var matched = false;
				for ( var i = 0; i < applicableBINs.length; i++) {
					var bin = applicableBINs[i];
					var intCardNo = parseInt(cardNo.substr(0,6), 10);
					if(bin == intCardNo){
						matched = true;
					}
				}

				if(!matched){
					showERRMessage(raiseError("XBE-ERR-88"));
					$("#cardNo").focus();
					return false;
				}
			}
			
			var creditPaxAmount=0;
			for(var gl=0;gl < jsonPaxPayObj.length;gl++){
				creditPaxAmount = creditPaxAmount+ Number(jsonPaxPayObj[gl].displayUsedCredit);
			}
			var groupBookingReference = $('#groupBookingReference').val();
			
			if(groupBookingReference != null && $.trim(groupBookingReference).length > 0){
				
				
				var minPayment=Number($.trim($('#minimumPayment').val()));
				if(minPayment> 0){
					if(minPayment>creditPaxAmount+Number($("#txtPaymentWithCharge").val())){
						showERRMessage("Payment should be higher than minimum payment");
						return false;	
					}
					
				}
				else{
					UI_commonSystem.hideProgress();
					showERRMessage("Please validate group booking request reference before continue with this group booking");
					return false;
					
				}
				
			}
		
		
			
			return true;
		}
		
		/*
		 * IF we use other carrier pax credit for payments we need to load the
		 * reservation as a interline reservation. so updating the group pnr
		 */
		UI_tabPayment.updateGroupPnr = function(responce) {
			if(responce.convertedToGroupReservation){
				if(UI_tabPayment.isReservation) {
					jsonGroupPNR = responce.groupPNR;
					return;
					
				} else if(UI_tabPayment.isModify) {
					jsonGroupPNR = responce.groupPNR;
					return;
					
				} else if(UI_tabPayment.isSegModify) {
					DATA_ResPro.modifySegments.groupPNR = responce.groupPNR;
					return;
					
				} else if (UI_tabPayment.isAddModifyAnci) {
					DATA_ResPro.groupPNRNo = responce.groupPNR;
					return ;
					
				} else if (UI_tabPayment.isAddSegment) {
					DATA_ResPro.reservationInfo.groupPNR = responce.groupPNR;
					return ;
				} else if (UI_tabPayment.isRequote) {
					DATA_ResPro.reservationInfo.groupPNR = responce.groupBookingReference;
					return ;
				}
			} else {
				if(typeof(responce.originallyGroupPNR) != "undefined" && responce.originallyGroupPNR){
					jsonGroupPNR = responce.groupPNR;
				}else{
					jsonGroupPNR = "";
				}				
				return ;
			}
		}
		
		
		/*
		 * reset on Click
		 */
		UI_tabPayment.resetCCOnClick = function(){			
			$("#cardType").val("");
			$("#cardNo").val("");
			$("#cardHoldersName").val("");
			$("#expiryDate").val("");
			$("#cardCVV").val("");
		}
		
		/*
		 * Cancel Click
		 */
		UI_tabPayment.cancelCROnClick = function(){
			$("#btnRedeem").prop('disabled', false);
			$("#btnVoucherOtpConfirm").prop('disabled', false);
			$("#btnVoucherOtpResend").prop('disabled', false);
			$("#btnUseAnotherVoucher").prop('disabled', false);
			$("#chkVoucherRedeem").prop('disabled', false);
			if(UI_tabPayment.pickBINPromotionSucess){
				UI_tabPayment.removeBINPromoton();
			}
				
			UI_tabPayment.resetCCOnClick();	
			$("#cardType").children().remove();
			$("#payOptions").show();
			$("#creditCardDet").hide();
			$("#payOptButtons").show();
			$("#divDisableLayer").hide();
			arrError = new Array();
			$("#divCCMsg").html("");
			UI_tabPayment.ccEnableTab(true);
			
		}
		
		
		
		/*
		 * Validate Page Control
		 */
		UI_tabPayment.validatePageControl = function(p){
			var blnReturn = true;
			var strErrImgPath = UI_commonSystem.strImagePathError;
			var intLen = p.length;
			var strChkEmpty = "";
			if (intLen > 0){
				var i = 0;
				do{
					p[i] = $.extend({required: false}, p[i]);
					if (p[i].required){
						if ($(p[i].id).isFieldEmpty()) {
							showERRMessage(raiseError("XBE-ERR-01",p[i].desc));
							$(p[i].id).focus();
							blnReturn = false;
							break;
						}
					}
					
					/* Invalid Character */
					strChkEmpty = checkInvalidChar($(p[i].id).val(), top.arrError["XBE-ERR-12"], p[i].desc);
					if (strChkEmpty != ""){
						if (strChkEmpty.indexOf("/") != -1){
						}else{
							showERRMessage(strChkEmpty);
							$(p[i].id).focus();
							blnReturn = false;
							break;
						}
					}
					i++;
				}while(--intLen);
			}
			
			return blnReturn;
		}
	
		UI_tabPayment.ccEnableTab = function(cond) {
			
			if(UI_tabPayment.isReservation) {
				UI_makeReservation.tabStatus.tab0 = cond; 
				UI_makeReservation.tabStatus.tab1 = cond;
				UI_makeReservation.tabStatus.tab2 = (cond && UI_tabAnci.tabStatus);
			} else if(UI_tabPayment.isModify){
				UI_reservation.tabStatus.tab0 = cond;
				UI_reservation.tabStatus.tab1 = cond;
				UI_reservation.tabStatus.tab2 = cond; 
				UI_reservation.tabStatus.tab3 = cond;
			} else if(UI_tabPayment.isAddModifyAnci){
				UI_modifyAncillary.tabStatus.tab0 = cond;
				UI_modifyAncillary.tabStatus.tab1 = cond;
			} else if(UI_tabPayment.isAddSegment){
				UI_addSegment.tabStatus.tab0 = cond;
				UI_addSegment.tabStatus.tab1 = cond;
				UI_addSegment.tabStatus.tab2 = cond;
			} else if(UI_tabPayment.isRequote){
				UI_modifyResRequote.tabStatus.tab0 = cond;
				UI_modifyResRequote.tabStatus.tab1 = cond;
				UI_modifyResRequote.tabStatus.tab2 = cond;
			} else {
				UI_modifySegment.tabStatus.tab0 = cond;
				UI_modifySegment.tabStatus.tab1 = false;
				UI_modifySegment.tabStatus.tab2 = false; 
			}
			
		}	
	
	/** ------------ tabmakePayment-------------------------------------------- */	
	
	/** -------------------------------------------------------------------------------------------------* */	
	/** Taken from taPaymentModify.js * */	
		

	UI_tabPayment.paymentModifyDetailsOnLoadCall = function(){
		
		jsonPaySumObj = jsonResPaymentSummary;
		jsonPaxPayObj = jsonPaxPriceTo;
		UI_tabPayment.buildPaymentSummary();
		UI_tabPayment.buildBalanceToPay();
		UI_tabPayment.resetOnClick();
		UI_tabPayment.fillBookingInfo();
		
		$("#divCreditCardPayment").css({ "margin-left":"-10px"} )		
		UI_commonSystem.fillGridData({id:"#tblPaxPayDet", data:jsonPaxPayObj});
		UI_tabPayment.disableGridButtons();
		UI_tabPayment.selectPayPax();
		if (UI_tabPayment.isModify || UI_tabPayment.isAddModifyAnci) {	
			$("#trEmail").hide();
			if(jsonGroupPNR !=null && jsonGroupPNR != "") {
				$("#btnPayForce").hide();
				UI_tabPayment.showForceConfirm =false;
			}					
		}
		
		if(UI_tabPayment.isAddInfant) {
			$("#btnPayForce").hide();
			UI_tabPayment.showForceConfirm =false;
		}
		
		$("#tblPaxPayDet").children('checkbox').disable();
		
		if(typeof(isCsPnr) != 'undefined'){
       	 if(isCsPnr ) {
            	$("#btnPayForce").hide();
            	$("input:checkbox[name=select]").each(function(){
        			$(this).attr('disabled', true);					
        		});
            }
        }
		
			
		if (jsonPaySumObj != null){
			if (jsonPaySumObj.balanceToPay != ""){
				UI_reservation.tabStatus.tab2 = true;
			}
		}
		// set owner agent to the dropdown
        if (!UI_tabPayment.isReservation && UI_tabPayment.isModify && DATA_ResPro.initialParams.onAccountPayments){
        	if (jsonBkgInfo.bookingCategory == 'CHT' && UI_reservation != undefined){
        		$("#selAgents").val(UI_reservation.ownerAgent);
        	}
        }
        $("#payOptions").show();
		var payOpts = $('input[type="radio"][name="payment.type"]');
		if(payOpts.length ==1){
			payOpts.attr('checked',true);
			payOpts.click();
		}
	}
	
	/*
	 * Fill Booking Info
	 */
	UI_tabPayment.fillBookingInfo = function(){
		if (jsonBkgInfo != null){
			UI_tabPayment.pnr = jsonBkgInfo.PNR;
			$("#divPnrNoPay").html(jsonBkgInfo.PNR);
			var status = jsonBkgInfo.displayStatus;
			$("#divBkgStatusPay").html(status);
			$("#divAgentPay").html(jsonBkgInfo.agent);
		}
	}	
	
	UI_tabPayment.loadAirportMessage = function() {
		var html = '<a href="javascript:void(0)" onclick="UI_tabPayment.hideAirportMessage()"><img border="0" src="../images/A150_no_cache.gif" title="Close" align="right"><\/a> ';
		var apMsg = UI_tabPayment.airportMessage;		
		$('#payPageAirportMsg').html('');	
		$('#payPageAirportMsg').append(html + apMsg);
		
		if(apMsg != null && apMsg != ''){
			$('#payPageAirportMsg').slideDown('slow');
		} else {
			$('#payPageAirportMsg').hide();
		}	
		
	}
	
	UI_tabPayment.hideAirportMessage = function(){		
		$("#payPageAirportMsg").hide();
	}
	
	UI_tabPayment.getFlightRPHList = function() {
		
		var rphArray = [];
		if(UI_tabSearchFlights.isMulticity()){		
			rphArray = UI_Multicity.getFlightRPHList();
		}else if(UI_tabSearchFlights.isCalenderMinimumFare()){
			rphArray = UI_MinimumFare.getFlightRPHList();
		}else{
			rphArray = UI_tabSearchFlights.getFlightRPHList();
		}
		
		return rphArray;
	}
	
	UI_tabPayment.prepareAutoCancelMessage = function(){
		
		var showPopUp = false;		
		var reservationStatus = "";		
		
		if(UI_tabPayment.isSegModify || UI_tabPayment.isRequoteSegModify){
			showPopUp = true;
			UI_tabPayment.autoCancelPopupMessage = raiseError("XBE-CONF-05","Segment Modification")
		}else if(UI_tabPayment.isAddModifyAnci){
			showPopUp = true;
			UI_tabPayment.autoCancelPopupMessage = raiseError("XBE-CONF-05","Added/Modified Ancillaries")
			reservationStatus = DATA_ResPro.reservationStatus;
		}else if(UI_tabPayment.isAddSegment || UI_tabPayment.isRequoteAddSegment){
			showPopUp = true;
			UI_tabPayment.autoCancelPopupMessage = raiseError("XBE-CONF-05","Segment Addition")
		}
		
		if (UI_tabPayment.isSegModify) {
			reservationStatus = DATA_ResPro.modifySegments.status;
		} else if(!UI_tabPayment.isAddModifyAnci) {
			reservationStatus = DATA_ResPro.reservationInfo.status;
		}
		
		return showPopUp && (reservationStatus == 'CNF');
	} 
	
	UI_tabPayment.validateBookingRequest=function(){
		var data = {};
		var frmUrl = "";
		if(UI_tabPayment.isReservation){
				frmUrl = "confirmationTab!groupBookingValidate.action";
				var searchData = {};
				if (UI_tabSearchFlights.isCalenderMinimumFare()) {
					searchData = UI_MinimumFare.createSearchParams('searchParams');
				} else {
					searchData = UI_tabSearchFlights.createSearchParams('searchParams');
				}
				data = $.airutil.dom.concatObjects(data, UI_tabSearchFlights.inOutFlightRPHList())
				data = $.airutil.dom.concatObjects(data,searchData);
				data["totalAmount"]=Number(jsonPaySumObj.totalFare);
		}
		else if (UI_tabPayment.isModify){
			frmUrl = "paxAccountPaymentConfirm!groupBookingValidate.action";	
			data['pnrSegments'] = UI_reservation.jsonSegs;
			data['resPax'] = UI_reservation.jsonPassengers;
			data["totalAmount"]=jsonPaySumObj.totalFare;
			
		}
		else if(UI_tabPayment.isSegModify){						
			frmUrl = "modifyPaymentConfirm!groupBookingValidate.action";
			data["totalAmount"]=Number(jsonPaySumObj.totalFare);
			
		}
		else if(UI_tabPayment.isAddModifyAnci){
			frmUrl = "modifyAnciPaymentConfirm!groupBookingValidate.action";	
			data['pnrSegments'] = $.toJSON(DATA_ResPro.flightRPHList);
			data['resPaxs'] =$.toJSON(DATA_ResPro.resPaxs);
			data["totalAmount"]=jsonPaySumObj.totalFare;
			
		}
		else if(UI_tabPayment.isAddSegment)	{
			frmUrl = "addSegmentPaymentConfirm!groupBookingValidate.action";
			data["totalAmount"]=Number(jsonPaySumObj.totalFare);
		}
		else{						
		
			frmUrl = "requotePaymentConfirm!groupBookingValidate.action";
			data["totalAmount"]=Number(jsonPaySumObj.totalFare);
		}
			
		data["groupBookingReference"]=$("#groupBookingReference").val();
		
		
		UI_commonSystem.showProgress();
		UI_commonSystem.getDummyFrm().ajaxSubmit({ url:frmUrl, dataType: 'json', type: 'post', processData: false, data:data,
			success: UI_tabPayment.applyGroupBookingSuccess, 
			error:UI_tabPayment.applyGroupBookingFail});
	}
	UI_tabPayment.applyGroupBookingSuccess=function(responce){
		//succees path
		  if(responce.groupBookingRequest!=null) {
			  var grpBooking=responce.groupBookingRequest;
			  $("#grpBookingVal").text("Minimum payment required : "+grpBooking.minPaymentRequired +"  Target payment date : "+grpBooking.targetPaymentDate.split("T")[0]);
			  $("#minimumPayment").val(grpBooking.minPaymentRequired);
				var payType = $("input[name='payment.type']:checked").val();
				if(payType=='CASH'){
					$('#paymentCash').show();
				}
				else{
					$('#paymentCash').hide();
				}
				if(payType=='CRED'){
					$('#paymentCreditCard').show();
				}
				else{
					$('#paymentCreditCard').hide();
				}
				
				if(payType=='ACCO'){
					$('#paymentAgentA').show();
				}else{
					$('#paymentAgentA').hide();
				}
          }
		  else{
			  $("#grpBookingVal").text("There is no group booking request with seat released for this request ID");
		  }
		UI_commonSystem.hideProgress();
	}
	UI_tabPayment.applyGroupBookingFail=function(responce){
		//Fail path
		UI_commonSystem.hideProgress();
	}
	
	UI_tabPayment.reloadPromotions = function(overrideExistinPromotion){
		var cardNumber = $('#cardNo').val();
		var bin = cardNumber.slice(0,6);
		
		if(UI_tabPayment_validateBIN()){
			var frmUrl = 'reloadPromotion!promotionsForBankIdentification.action';
			var data = {};
			var searchData = {};
			if (UI_tabSearchFlights.isCalenderMinimumFare()) {
				searchData = UI_MinimumFare.createSearchParams('searchParams');
			} else {
				searchData = UI_tabSearchFlights.createSearchParams('searchParams');
			}

			data = $.airutil.dom.concatObjects(data,searchData);
			data["selectedFlightList"] = $.toJSON(UI_tabPayment.getFlightRPHList());
			data["bin"] = bin;
			//get the promo code val entered in popup
			data["promoCode"] = $('#promoCodeInPaymentPage').val(); 
			if(typeof overrideExistinPromotion != 'undefined'){
				data['overrideExistingPromotion'] = overrideExistinPromotion;
			}
			var contactInfo =  $('input,select').filterData('name','contactInfo');
			data = $.airutil.dom.concatObjects(data,contactInfo);
			
			UI_commonSystem.getDummyFrm().ajaxSubmit({ url:frmUrl, dataType: 'json', type: 'post', processData: false, data:data,
				beforeSend:UI_commonSystem.showProgress(), success: UI_tabPayment.onSuccessReloadPromotions,error:UI_tabPayment.pickPromoError});
			}
	}
	
	UI_tabPayment.onSuccessReloadPromotions = function(responce){
		var promotionDialog = $("#reloadPromotions");
		var promotionSelectionDialog = $("#promoSelection");
		var promoCode = $('#promoCodeInPaymentPage').val();
		UI_commonSystem.hideProgress();
		UI_tabPayment.clearReloadPromotionsDialog();
		if(responce.success && responce.hasBinPromotion){
			if (responce.hasExistingPromotion) {
				promotionSelectionDialog.dialog();
				$("#binPromotionDiscount").html(responce.binPromotionDiscountAmount + " " + jsonPaySumObj.currency);
				$("#alreadyAppliedDiscount").html(responce.existingDiscountAmount + " " + jsonPaySumObj.currency);
				promotionSelectionDialog.parent().css({"z-index":"3"});
				$("#existingPromotionBtn").decorateButton();
				$("#existingPromotionBtn").unbind("click").bind("click",function(){$("#promoSelection").dialog('close');});
				$("#binPromotionBtn").decorateButton();
				$("#binPromotionBtn").unbind("click").bind("click",function(){UI_tabPayment.reloadPromotions(true);});
				
			}else{
				UI_tabPayment.promotionBIN = $('#cardNo').val().slice(0,6);
				UI_tabPayment.updatePayemntTabTO(responce);
				UI_tabPayment.buildPaymentSummary();
				UI_tabPayment.creditCardPayment();
				UI_tabPayment.selectPayPax();
				UI_tabPayment.pickBINPromotionSucess = true;
			}
		}else{
			if(responce.checkPromotionsWithCode){
				$('#promoCodeInPaymentPage').val(""); 
				alert("No Promotions found for : "+ promoCode );
			}else if(responce.hasExistingPromotion){
				alert("Promotion already applied for current credit card");
			}else{
				$("#promocodeEmpty").hide();
				promotionDialog.dialog();
				promotionDialog.parent().css({"z-index":"3"})
				$("#pickPromotion").decorateButton();
				$("#pickPromotion").unbind("click").bind("click",UI_tabPayment.pickPromotionWithPromocodeAndBin);
			}
		}
	}
	
	UI_tabPayment.pickPromotionWithPromocodeAndBin = function(){
		$("#promocodeEmpty").hide();
		var promoCode = $('#promoCodeInPaymentPage').val(); 
		if(promoCode == ""){
			$("#promocodeEmpty").show();
			$("#promoCodeInPaymentPage").focus();
		}else{
			UI_tabPayment.reloadPromotions();
		}
	}
	
	UI_tabPayment.pickPromoError = function(){
		UI_tabPayment.clearReloadPromotionsDialog();
		UI_commonSystem.hideProgress();
	}
	
	UI_tabPayment.clearReloadPromotionsDialog= function(){
		if ($('#reloadPromotions').is(":visible")) {
			$("#reloadPromotions").dialog('close');
		}
		if( $('#promoSelection').is(":visible") ){
			$("#promoSelection").dialog('close');
		}
	}
	
	UI_tabPayment_validateBIN = function(){
		if($("#cardType").val() == ""){
			showERRMessage(raiseError("XBE-ERR-01","Card type"));
			$("#cardType").focus();
			return false;
		}
		if( $("#cardNo").val() == ""){
			showERRMessage(raiseError("XBE-ERR-01","Credit card No"));
			$("#cardNo").focus();
			return false;
		}
		if (!ValidateCardNos($("#cardType").val(), $("#cardNo").val())){
			showERRMessage(raiseError("XBE-ERR-04","Credit card No"));
			$("#cardNo").focus();
			return false;
		}
		return true;
	}
	
	UI_tabPayment.removeBINPromoton = function(){
		var frmUrl = 'reloadPromotion!removeBINPromotion.action';
		var data = {};
		var searchData = {};
		if (UI_tabSearchFlights.isCalenderMinimumFare()) {
			data = UI_MinimumFare.createSearchParams('searchParams');
		} else {
			data = UI_tabSearchFlights.createSearchParams('searchParams');
		}
		data = $.airutil.dom.concatObjects(data,searchData);
		data["selectedFlightList"] = $.toJSON(UI_tabPayment.getFlightRPHList());
		
		UI_commonSystem.getDummyFrm().ajaxSubmit({ url:frmUrl, dataType: 'json', type: 'post', processData: false, data:data,
			beforeSend:UI_commonSystem.showProgress(), success: UI_tabPayment.onSuccessRemoveBINPromotion});
	}
	
	UI_tabPayment.onSuccessRemoveBINPromotion = function(responce){
		UI_tabPayment.pickBINPromotionSucess = false;
		UI_tabPayment.updatePayemntTabTO(responce);
		UI_tabPayment.buildPaymentSummary();
		UI_tabPayment.creditCardPayment();
		UI_tabPayment.selectPayPax();
		UI_commonSystem.hideProgress();
	}
	
	UI_tabPayment.onBinPromotionMissmatch = function(){
		var removeBinPromotion =confirm('Promotion is not applied for this Credit card \nRemove the promotion and proceed');
		if (removeBinPromotion==true) {
			UI_tabPayment.removeBINPromoton();
		 }
		else {
			showERRMessage("Please enter the credit card you enter for promotion");
		 } 
	}
	
	UI_tabPayment.redeemVouchers = function (onLoad){
		if(UI_tabPayment.checkVoucherEmptyFields()){
			var data = {};
			if (!onLoad){
					var voucherID = trim($("#voucherID_0").val());
					if(voucherID != null && voucherID != "") {
						data["voucherRedeemRequest.voucherIDList["+0+"]"]= voucherID;
					} else {
						showERRMessage("Please add the voucher ID of voucher number " + position);
					}
			}
			data["onLoad"] = onLoad;
			if (typeof UI_tabAnci != 'undefined' && UI_tabAnci.jsonFareQuoteSummary != null) {
				data["selCurrencyCode"] = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
			}
			if (typeof UI_reservation != 'undefined') {
				data['resPax'] = UI_reservation.jsonPassengers;
			}
			if (typeof UI_tabPassenger != 'undefined') {
				data['paxInfants'] = UI_tabPassenger.createInfantPaxInfo();
			}
			$.ajax({
			    url : 'redeemVoucherList.action',
			    beforeSend : UI_commonSystem.showProgress,
			    dataType:"json",
			    data : data,
			    type : "POST",		           
			    success : UI_tabPayment.showOtpBox
			});
		}
	}
	UI_tabPayment.afterVoucherRedemption = function(response){
		if (response.voucherRedeemResponse != null) {
			var redeemedAmount = parseFloat(response.voucherRedeemResponse.redeemedTotal);
			totalRedeemedVoucherAmount = totalRedeemedVoucherAmount + redeemedAmount
		//	$("#divAmountAfterRedeem").empty().append(parseFloat(response.voucherRedeemResponse.balTotalPay) + " " + response.selCurrencyCode);
			$("#divVoucherRedemptionId").empty().append(response.voucherRedeemResponse.redeemVoucherList[response.voucherRedeemResponse.redeemVoucherList.length - 1].voucherId);
			$("#hdnVoucherRedemption").val(parseFloat(response.voucherRedeemResponse.balTotalPay));
			$("#hdnVoucherRedeemAmount").val(totalRedeemedVoucherAmount);
			var strSelectedCurrency = geti18nData('payement_SelectedCurrency',
				'In Selected Currency');
		$("#divAmountAfterRedeem")
				.empty()
				.append(
						response.voucherRedeemResponse.balTotalPay
								+ " "
								+ jsonPaySumObj.currency
								+ " [ "
								+ strSelectedCurrency
								+ "<font color='red'>"
								+ response.voucherRedeemResponse.balanceToPaySelectedCurrency
								+ " " + response.selCurrencyCode + "</font> ]");
			$("#trVoucherRedemption").show();
			$("#trOtpConfirmation").show();
			if(parseFloat(response.voucherRedeemResponse.balTotalPay) > 0){
				$("#btnUseAnotherVoucher").decorateButton();
				$("#btnUseAnotherVoucher").unbind("click").bind("click",function(){UI_tabPayment.useAnotherVoucher();});
			}
			voucherHTMLID.push(response.voucherRedeemRequest.voucherIDList[0]);
			vouchersRedeemed = true;
			UI_tabPayment.disableRedeemedVouchers();
			var vouchers = "";
			for (var i = 0 ;response.voucherRedeemResponse.redeemVoucherList.length > i; i++) {
				if (i != 0){
					vouchers += ", " ;
				}
				vouchers += response.voucherRedeemResponse.redeemVoucherList[i].voucherId;
			}
			
			if (!response.onLoad) {
				showConfirmation(response.message + vouchers);
			}
			
			if(response.voucherRedeemResponse.balTotalPay ==0)
			{
				$('#radPaymentTypeC, #radPaymentTypeA, #radPaymentTypeR,#selAgents, #selAgents2, #btnAddVoucher_0').prop('disabled', true);	
				$("#radPaymentTypeA").prop("checked", true);
			} else {
				$('#radPaymentTypeC, #radPaymentTypeA, #radPaymentTypeR,#selAgents, #selAgents2, #btnAddVoucher_0').prop('disabled', false);
			}
			if(voucherHTMLID.length==0){
				$(removeElementG).prop('disabled', false);
				voucherHTMLID.push("voucherID_"+indexG);
				$("#voucherID_"+indexG).val("");
				removeElementG="";	
				indexG="";
				vouchersRedeemed = false;
				$("#trVoucherRedemption").hide();	
			}
		} 
	}
	UI_tabPayment.showOtpBox = function(response){
		var success = response.success;
		if (success){
			if(response.voucherRedeemResponse === null){
				$("#trVoucherOtp").show();
				$("#trVoucher").hide();
				$("#btnVoucherOtpConfirm").decorateButton();
				$("#btnVoucherOtpConfirm").unbind("click").bind("click",function(){UI_tabPayment.voucherOtpConfirm(false,response.voucherRedeemRequest.voucherIDList[0]);});
				$("#btnVoucherOtpResend").decorateButton();
				$("#btnVoucherOtpResend").unbind("click").bind("click",function(){UI_tabPayment.voucherOtpResend(false,response.voucherRedeemRequest.voucherIDList[0]);});
				voucherHTMLID.push(response.voucherRedeemRequest.voucherIDList[0]);
				UI_tabPayment.disableRedeemedVouchers();
			}else{
				$("#trVoucher").hide();
				UI_tabPayment.afterVoucherRedemption(response);
			}
		} else {
			showERRMessage(response.message);
			$("#trVoucher").show();
		}
		UI_commonSystem.hideProgress();
	}
	
	UI_tabPayment.voucherOtpConfirm = function(onload,voucherId){
		if($("#voucherOtpId").val() !== null){
			var data = {};
			data["voucherRedeemRequest.voucherIDList["+0+"]"]= voucherId;
			data["onLoad"] = onload;
			if (typeof UI_tabAnci != 'undefined' && UI_tabAnci.jsonFareQuoteSummary != null) {
				data["selCurrencyCode"] = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
			}
			if (typeof UI_reservation != 'undefined') {
				data['resPax'] = UI_reservation.jsonPassengers;
			}
			data["otp"] = $("#voucherOtpId").val()
			$.ajax({
			    url : 'redeemVoucherList.action',
			    beforeSend : UI_commonSystem.showProgress,
			    dataType:"json",
			    data : data,
			    type : "POST",		           
			    success : UI_tabPayment.redeemSuccess
			});
		}
	}
	UI_tabPayment.voucherOtpResend = function(onload,voucherId){
		if(trim($("#voucherID_0").val()) !== null){
			var data = {};
			data["voucherRedeemRequest.voucherIDList["+0+"]"]= voucherId;
			data["onLoad"] = onload;
			if (typeof UI_tabAnci != 'undefined' && UI_tabAnci.jsonFareQuoteSummary != null) {
				data["selCurrencyCode"] = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
			}
			if (typeof UI_reservation != 'undefined') {
				data['resPax'] = UI_reservation.jsonPassengers;
			}
			$.ajax({
			    url : 'redeemVoucherList.action',
			    beforeSend : UI_commonSystem.showProgress,
			    dataType:"json",
			    data : data,
			    type : "POST",		           
			    success : UI_tabPayment.otpResendSuccess
			});
		}
	}
	UI_tabPayment.otpResendSuccess = function(response){
		var success = response.success;
		if (success){
			showConfirmation(response.message);
		}else{
			showERRMessage(response.message);
		}
		UI_commonSystem.hideProgress();
	}	
	UI_tabPayment.useAnotherVoucher = function(){
		voucherHTMLID = new Array();
		if(voucherHTMLID.indexOf("voucherID_0") < 0){
			voucherHTMLID.push("voucherID_0");
		}
		$("#trVoucherOtp").hide();
		$("#trOtpConfirmation").hide();
		$("#trVoucher").find('input:text').val('');
		$("#trVoucherOtp").find('input:text').val('');
		$("#btnRedeem").prop('disabled', false);
		$("#voucherOtpId").val('')
		UI_tabPayment.enableRedeemedVouchers();
		$("#trVoucher").show();
	}
	UI_tabPayment.redeemSuccess = function(response){
		var success = response.success;
		if (success){
			if (response.voucherRedeemResponse != null) {
				$("#trVoucherOtp").hide();
				UI_tabPayment.afterVoucherRedemption(response);
			} 
		}else {
			showERRMessage(response.message);
		}
		
		if(response.disableRedeem){
			$("#btnRedeem").prop('disabled', true);
		}
		UI_commonSystem.hideProgress();
	}
	
//	UI_tabPayment.removeVoucher = function (){
//		if(voucherHTMLID.length > 1){
//			var index = voucherHTMLID.length-1;
//			var removeElement = "#voucherID_"+index;
//			$(removeElement).parent().parent().remove();
//			voucherHTMLID.pop();
//		}
//	}
	
	UI_tabPayment.removeVoucher = function (id){
		var index = id.split("_")[1];
		var removeElement = "#voucherID_"+index;
		if(voucherHTMLID.length > 1){
			var position = voucherHTMLID.indexOf("voucherID_"+index);
			voucherHTMLID.splice(position, 1);
			if($(removeElement).is(':disabled')){
				UI_tabPayment.redeemVouchers(false);
			}
			$(removeElement).parent().parent().remove();

			var previous = voucherHTMLID[voucherHTMLID.length-1].split("_")[1];
			$("#btnAddVoucher_"+previous).show();
			
		} else {
			var previous = voucherHTMLID[voucherHTMLID.length-1].split("_")[1];
			$("#btnAddVoucher_"+previous).show();
			
			if($(removeElement).val()!=""){
				var position = voucherHTMLID.indexOf("voucherID_"+index);
				voucherHTMLID.splice(position, 1);
			}
			if($(removeElement).is(':disabled')){
				removeElementG=removeElement;	
				indexG=index;
				UI_tabPayment.redeemVouchers(false);
			}
			
		}
	}


	UI_tabPayment.addVoucher = function (id){
		var index = parseInt(id.split("_")[1])+(1);
		if (trim($("#voucherID_"+id.split("_")[1]).val())=="") {
			alert("Please add the voucher ID to the field..");
		} else {
			$("#"+id).hide();
			var voucherString = "<tr id='voucherSet_"+index+"'><td><input type=\"text\"id=\"voucherID_"+index+"\"></td>"+
								"<td valign='top'>"+
								"<input type='button' name='btnRemoveVoucher' style='width:25px' class='Button' id='btnRemoveVoucher_"+index+"' value='-' onclick='UI_tabPayment.removeVoucher(this.id)'/>"+
								"<input type='button' name='btnAddVoucher' style='width:25px' class='Button' id='btnAddVoucher_"+index+"' value='+' onclick='UI_tabPayment.addVoucher(this.id)'/></td></tr>";				
			voucherHTMLID.push("voucherID_"+index);
			$("#tblVoucherIDs").append(voucherString);
			$("#voucherID_"+index).numeric();
		}
	}
	

	UI_tabPayment.onRedeemCheckChange = function(e) {
		if ($("#chkVoucherRedeem").is(":checked")) {
			$("#trVoucher").show();
			$("#btnRedeem").prop('disabled', false);
			for (i = 0; i < voucherHTMLID.length; i++) {
				$("#" + voucherHTMLID[i]).prop('disabled', false);
				$("#" + voucherHTMLID[i]).val('');
			}
			$("#btnPayForce").hide();
			UI_tabPayment.showForceConfirm = false;
		} else {
			UI_tabPayment.voidVoucherRedeem();
		}
	}
	
	UI_tabPayment.voidVoucherRedeem = function(){
		var data = {};
		if (typeof UI_tabAnci != 'undefined' && UI_tabAnci.jsonFareQuoteSummary != null) {
			data["selCurrencyCode"] = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
		}

		$.ajax({
		    url : 'voucherRedeemVoid.action',
		    beforeSend : UI_commonSystem.showProgress,
		    dataType:"json",
		    data : data,
		    type : "POST",		           
		    success : UI_tabPayment.voidRedeemSuccess
		});
	}
	
	UI_tabPayment.voidRedeemSuccess = function(response){
		var success = response.success;
		if (success){
			if (response.voidRedeemResponse != null) {
				var redeemedAmount = parseFloat(response.voidRedeemResponse.redeemedTotal);
				totalRedeemedVoucherAmount = totalRedeemedVoucherAmount + redeemedAmount
				$("#divAmountAfterRedeem").empty().append(parseFloat(response.voidRedeemResponse.balTotalPay) + " " + response.selCurrencyCode);
				$("#hdnVoucherRedemption").val(parseFloat(response.voidRedeemResponse.balTotalPay));
				$("#hdnVoucherRedeemAmount").val(totalRedeemedVoucherAmount);
				
				$("#trVoucherRedemption").hide();
				$("#trOtpConfirmation").hide();
				$("#trVoucherOtp").hide();
				$("#trVoucher").hide();
				
				$("#btnPayForce").show();			
				UI_tabPayment.showForceConfirm = true;
				
				vouchersRedeemed = false;			
				$("#btnRedeem").prop('disabled', true);
		
				
				for (i = 0; i < voucherHTMLID.length; i++) {
					$("#" + voucherHTMLID[i]).prop('disabled', true);
				}
				
				UI_tabPayment.disableRedeemedVouchers();
				var vouchers = "";
			} 
		}else {
			showERRMessage(response.message);
		}
		
		UI_commonSystem.hideProgress();
	}
	
	UI_tabPayment.disableRedeemedVouchers = function () {
		for(var i = 0; i < voucherHTMLID.length ; i++) {
			var disableElement = "#voucherID_"+i;
			$(disableElement).prop('disabled', true);
		}
	}
	UI_tabPayment.enableRedeemedVouchers = function () {
		for(var i = 0; i < voucherHTMLID.length ; i++) {
			var disableElement = "#voucherID_"+i;
			$(disableElement).prop('disabled', false);
		}
		if(voucherHTMLID.length ==0 )
		{
			var disableElement = "#voucherID_"+i;
			$(disableElement).prop('disabled', false);
		}
	}
	
	UI_tabPayment.unblockRedeemedVouchers = function (){
		$.ajax({
	        url : 'redeemVoucherList!unblockRedeemedVouchers.action',
	        beforeSend : UI_commonSystem.showProgress,
	        dataType:"json",
	        type : "POST",		           
			success : UI_commonSystem.hideProgress,
		});
	}
	
	UI_tabPayment.checkVoucherEmptyFields = function() {
		var result = true;
		for (i = 0; i<voucherHTMLID.length; i++) {
			if ($("#"+voucherHTMLID[i]).val() != undefined) {
				var voucherID = trim($("#"+voucherHTMLID[i]).val());
				if(voucherID == null || voucherID == "") {
					showERRMessage("Please remove empty Voucher ID fields...");
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	

	/**
	 * ------------
	 * taPaymentModify.js--------------------------------------------
	 */
	
	/*
	 * ------------------------------------------------ end of File
	 * ----------------------------------------------
	 */