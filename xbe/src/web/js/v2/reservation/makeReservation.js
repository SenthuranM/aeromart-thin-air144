	/*
	*********************************************************
		Description		: XBE Make Reservation 
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	/*
	 * Make Reservation related Methods
	 */
	function UI_makeReservation(){}
	UI_makeReservation.tabStatus = ({tab0:true, tab1:false, tab2:false, tab3:false, tab4:false});		// tab Status
	UI_makeReservation.tabDataRetrive = ({tab0:false, tab1:true, tab2:false, tab3:true, tab4:true});	// Data Retrieval Status
	
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divBookingTabs").getLanguage();
		$("#divSearchTab").getLanguage();
		$("#divLegacyRootWrapper").fadeIn("slow");
		$("#divBookingTabs").tabs();
		$('#divBookingTabs').bind('tabsbeforeactivate', function(event, ui) {
			UI_PageShortCuts.clearShortCutToolTip();
			if(eval("UI_makeReservation.tabStatus.tab" + ui.newTab.index())) {
				UI_commonSystem.pageOnChange();
				if(ui.index == 0){//search
					if(interlineBookingCreated){ //redirect to V1 search if a booking is created and user click on the search tab.
						UI_tabSearchFlights.backToV1AirlineSearch();
					}
					UI_tabPassenger.setAdultPassenger();
					UI_tabPassenger.setInfantPassenger();
					UI_PageShortCuts.initilizeMetaDataForSearchFlight();
				}else if (ui.index == 1){//passenger
					UI_PageShortCuts.initilizeMetaDataForPassengerScreen();
				}else if (ui.index == 2){//ancillary
					UI_PageShortCuts.initilizeMetaDataForReservationAncillary();
				}else if (ui.index == 3){//payment
					UI_PageShortCuts.initilizeMetaDataForPaymentScreen();
				}
			}		
			return eval("UI_makeReservation.tabStatus.tab" + ui.newTab.index());
		});
		
		if(DATA_ResPro.initialParams.viewAvailableCredit) {
			if(intAgtBalance != null && typeof intAgtBalance !== "undefined"){
				top.intAgtBalance = intAgtBalance;				
			}
			UI_commonSystem.showAgentCredit();
			UI_commonSystem.showAgentCreditInAgentCurrency();
		} else {
			$("#divCreditPnl").hide();
		}
		
		// Search Flight Tab -----------------------------------------------------				
		if(isMinimumFare){
			$('input:radio[name="searchType"][value="F"]').prop('checked', true);
			UI_MinimumFare.ready();
		}else{
			if(DATA_ResPro.initialParams.allowMultiCitySearch){
				if (isMultiCity){
					UI_Multicity.ready();
					$('input:radio[name="searchType"][value="M"]').prop('checked', true);
				}else{
					UI_tabSearchFlights.ready();
					$('input:radio[name="searchType"][value="R"]').prop('checked', true);
				}	
			}else{
				UI_tabSearchFlights.ready();
				$("#divSearchTypeTab").hide();			
			}
		}		
		// Passenger Tab --------------------------------------------------------
		//UI_tabPassenger.ready();
		
		// Payment Tab ---------------------------------------------------------- 
		//UI_tabPayment.ready();
		
		/* Remove This Method Call */
		UI_tabItinerary.ready();
		
		UI_commonSystem.strPGID = "SC_RESV_CC_005";
		
		//trigger auto search if auto search is set to true
		if(DATA_ResPro["initialParams"].triggerExternalSearch){
			UI_tabSearchFlights.triggerSearch(DATA_ResPro["externalSearchParams"]);
		}
	});
	
	/*
	 * Open Tab from the Function Call
	 */
	UI_makeReservation.openTab = function(intIndex){
		$('#divBookingTabs').tabs("option", "active",  intIndex); 	
	}
	
	/*
	 * Lock Tabs
	 */ 
	UI_makeReservation.lockTabs = function(){
		UI_makeReservation.tabStatus = ({tab0:true, 
										tab1:false, 
										tab2:false, 
										tab3:false, 
										tab4:false});
		
		UI_makeReservation.tabDataRetrive = ({tab0:false, 
										tab1:true, 
										tab2:true, 
										tab3:true, 
										tab4:true});
	}
	
	
	
	UI_makeReservation.showSearchParam = function(val){
//		showLoader();
		if(val=='M'){
//			UI_makeReservation.isMultiCity = true;
//			$("#multicitySearchTab").show();
//			$("#returnSearchTab").hide();
//			UI_Multicity.ready();
			$('#oldAdultPaxInfo').val($.toJSON(oldResAdultPaxInfo));
			$('#oldInfantPaxInfo').val($.toJSON(oldResInfantPaxInfo));
			$('#oldContactInfo').val($.toJSON(oldResContactInfo));
			$('#oldNoOfAdults').val(oldResAdultCount);
			$('#oldNoOfChildren').val(oldResChildCount);
			$('#oldNoOfInfants').val(oldResInfantCount);
			$("#frmMultiBkg").action("makeReservation.action?criteria=multiCity");
			$("#frmMultiBkg").submit();
		}else if(val=='R'){
//			UI_makeReservation.isMultiCity = false;
//			$("#returnSearchTab").show();
//			$("#multicitySearchTab").hide();
//			UI_tabSearchFlights.ready();
			window.location.href = "makeReservation.action?criteria=oneWay";
		}else{
			window.location.href = "makeReservation.action?criteria=minimumFare";
		}
	}
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	