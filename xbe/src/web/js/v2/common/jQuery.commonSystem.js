	/*
	*********************************************************
		Description		: Common system variables / function 
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created on		: 26th August 2009
		Last Modified	: 26th August 2009
	*********************************************************	
	*/
	
	function UI_commonSystem(){}
	
	/* Common Variables */
	UI_commonSystem.strImagePath = "../themes/default/images/jquery";
	UI_commonSystem.strImagePathError = "../themes/default/images/accelaero/";
	UI_commonSystem.strImagePathAA = "../themes/default/images/accelaero/";
	UI_commonSystem.strDefClass = "thinBorderB thinBorderR";
	UI_commonSystem.strHDClass = "gridBG thinBorderT " + UI_commonSystem.strDefClass;
	UI_commonSystem.strTxtMandatory = "<font class='mandatory'>&nbsp;*</font>";
	UI_commonSystem.strDTFormat = "dd/mm/yy";
	UI_commonSystem.strAPS = "&#39;";
	UI_commonSystem.strPGID = "";
	UI_commonSystem.strPGMode = "";
	
	UI_commonSystem.strOHDCode = "OHD";
	UI_commonSystem.strCNFCode = "CNF";
	UI_commonSystem.strCNXCode = "CNX";
	UI_commonSystem.strWLCode = "WL";
	
	UI_commonSystem.strAdtCode = "AD";
	UI_commonSystem.strInfCode = "IN";
	UI_commonSystem.strChdCode = "CH";
	
	UI_commonSystem.strCashCode = "CASH";
	UI_commonSystem.strCCCode = "CRED";
	UI_commonSystem.strAccCode = "ACCO";
	UI_commonSystem.strBspCode = "BSP";
	UI_commonSystem.strOfflineCode = "OFFLINE";
	
	/*
	 *  Initialize Message
	 */
	UI_commonSystem.initializeMessage = function(){
		top[2].MessageReset();
	}
	
	/*
	 * Validate Mandatory Fields
	 * Param Contols Eg :
		var strCntrl = [
						{id:"#fromAirport", desc:"Departure location", required:true}
						];
	 *
	 *
	 */
	UI_commonSystem.validateMandatory = function(p){
		var blnReturn = true;
		var intLen = p.length;
		if (intLen > 0){
			var i = 0;
			do{
				p[i] = $.extend({required: false}, p[i]);
				if (p[i].required){
					if ($(p[i].id).isFieldEmpty()) {
						showERRMessage(raiseError("XBE-ERR-01",p[i].desc));
						$(p[i].id).focus();
						blnReturn = false;
						break;
					}
				}
				i++;
			}while(--intLen);
		}
		
		return blnReturn;
	}
	
	/*
	 * Validate Invalid Characters
	 */
	UI_commonSystem.validateInvalidChar = function(p){
		var blnReturn = true;
		var intLen = p.length;
		var strChkEmpty = "";
		if (intLen > 0){
			var i = 0;
			var strErr = "";
			do{
				if($(p[i].id).val()!=null)
					strChkEmpty = checkInvalidChar($(p[i].id).val(), top.arrError["XBE-ERR-12"], p[i].desc);
				if (strChkEmpty != ""){
// 				AARESAA-5122 - Town/City field validation for apostrophe
//					if (strChkEmpty.indexOf("/") != -1){
//					}else{
						showERRMessage(strChkEmpty);
						$(p[i].id).focus();
						blnReturn = false;
						break;
//					}
				}	
				i++;
			}while(--intLen);
		}
		
		return blnReturn;
	}
	
	/*
	 * Validate Mandatory Fields
	 * Param Contols Eg :
		var strCntrl = [
						{id:"#fromAirport", desc:"Departure location", required:true}
						];
	 *
	 *
	 */
	UI_commonSystem.validateMandatoryWindow = function(p){
		var blnReturn = true;
		var intLen = p.length;
		if (intLen > 0){
			var i = 0;
			do{
				p[i] = $.extend({required: false}, p[i]);
				if (p[i].required){
					if ($(p[i].id).isFieldEmpty()) {
						UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-01",p[i].desc)});
						$(p[i].id).focus();
						blnReturn = false;
						break;
					}
				}
				i++;
			}while(--intLen);
		}
		
		return blnReturn;
	}
	
	/*
	 * Validate Invalid Characters
	 */
	UI_commonSystem.validateInvalidCharWindow = function(p){
		var blnReturn = true;
		var intLen = p.length;
		if (intLen > 0){
			var i = 0;
			var strErr = "";
			do{
				strChkEmpty = checkInvalidChar($(p[i].id).val(), opener.top.arrError["XBE-ERR-12"], p[i].desc);
				if (strChkEmpty != ""){
					if (strChkEmpty.indexOf("/") != -1){
					}else{
						UI_message.showErrorMessage({messageText:strChkEmpty});
						$(p[i].id).focus();
						blnReturn = false;
						break;
					}
				}	
				i++;
			}while(--intLen);
		}
		
		return blnReturn;
	}
	
	/*
	 * Controls Validate
	 */
	UI_commonSystem.controlValidate = function(p){
		p = $.extend({required: false}, p);
		/* Empty Validate */
		if (p.required == true){
			if ($(p.id).isFieldEmpty()){
				showERRMessage(raiseError("XBE-ERR-01", p.desc));
				$(p.id).focus();
				return false;
			}
		}
		
		/* Invalid Character */
		strChkEmpty = checkInvalidChar($(p.id).val(), top.arrError["XBE-ERR-12"], p.desc);
		if (strChkEmpty != ""){
			if (strChkEmpty.indexOf("/") != -1){
			}else{
				showERRMessage(strChkEmpty);
				$(p.id).focus();
				return false;
			}
		}	
		
		return true;
	}
	
	/*
	 * Controls Validate
	 */
	UI_commonSystem.controlValidateName = function(p){
		var minLength=2;
		for (var key in top.globalPaxContactConfig) {
			var tmpLength=top.globalPaxContactConfig[key].paxConfigAD.firstName.minLength;
			if(tmpLength!=undefined && tmpLength!='undefined' && tmpLength!=null){
				minLength=tmpLength;
			}
			break;
		}
		
		p = $.extend({required: false}, p);
		/* Empty Validate */
		if (p.required == true){
			if ($(p.id).isFieldEmpty()){
				showERRMessage(raiseError("XBE-ERR-01", p.desc));
				$(p.id).focus();
				return false;
			}
		}
		
		/*
		 * AARESAA-6541
		 * */
		if (!$(p.id).isFieldEmpty()){			
			fieldParam = trim($(p.id).val());
			fieldParam = fieldParam.replace(/\s+/g, " ");

			if(fieldParam.length < minLength){
				showERRMessage(raiseError("XBE-ERR-03", p.desc,"2"));
				$(p.id).focus();
				return false;	
			}
			
			if(fieldParam.indexOf('--') != -1 || fieldParam.indexOf('- -') != -1){
				if(fieldParam.indexOf('--') != -1){
					invalidChar = "--";
				}else if(fieldParam.indexOf('- -') != -1){
					invalidChar = "- -";
				}
				showERRMessage(buildError(top.arrError["XBE-ERR-12"],invalidChar, p.desc));
				$(p.id).focus();
				return false;	
			}
			
		}
		
		/* Invalid Character */
		strChkEmpty = checkInvalidCharForName($(p.id).val(), top.arrError["XBE-ERR-12"], p.desc);
		if (strChkEmpty != ""){		
			showERRMessage(strChkEmpty);
			$(p.id).focus();
			return false;		
		}	
		
		return true;
	}
	
	/*
	 * Validate Alpha Numeric White Space 
	 */
	UI_commonSystem.validateANW = function(p){
		if (!isAlphaWhiteSpace($(p.id).val())){
			showERRMessage(raiseError("XBE-ERR-34", p.desc));
			$(p.id).focus();
			return false;
		}
		return true;
	}
	
	UI_commonSystem.numberValidateWithStartingZero = function(p){

		p = $.extend({minValue: null,
						maxxValue: null,
						blnDecimal:false
					}, p);
	
		var blnReturn = true;
		if (isNaN($(p.id).val())) {
			showERRMessage(raiseError("XBE-ERR-04", p.desc));
			$(p.id).focus();
			return false;
		}
		
		if (p.minValue != null){
			if (Number($(p.id).val()) < Number(p.minValue)){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
		}
		
		if (p.maxValue != null){
			if (Number($(p.id).val()) > Number(p.maxValue)){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
		}
		
		if (!p.blnDecimal){
			if (isDecimal($(p.id).val())){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
			if (!isInt($(p.id).val())){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
		}else{
			/* Perentage Values cannot be greater than 100 */
			if (p.maxValue == null){
				if (Number($(p.id).val()) > 100){
					showERRMessage(raiseError("XBE-ERR-04", p.desc));
					$(p.id).focus();
					return false;
				} 
			}
		}
		
		return true;
	}
	
	/*
	 * Number Validations
	  p:id				// Id
	  p:desc			// Description
	  p:minValue		// Minimum Value
	  p:maxxValue		// Maximum Value
	  p:blnDecimal		// Decimal Allowed or not [true/false]
	 */
	UI_commonSystem.numberValidate = function(p){
		p = $.extend({minValue: null,
						maxxValue: null,
						blnDecimal:false
					}, p);
	
		var blnReturn = true;
		if (isNaN($(p.id).val())) {
			showERRMessage(raiseError("XBE-ERR-04", p.desc));
			$(p.id).focus();
			return false;
		}
		
		$(p.id).val(Number($(p.id).val()) * 1);
		if (p.minValue != null){
			if (Number($(p.id).val()) < Number(p.minValue)){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
		}
		
		if (p.maxValue != null){
			if (Number($(p.id).val()) > Number(p.maxValue)){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
		}
		
		if (!p.blnDecimal){
			if (isDecimal($(p.id).val())){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
			if (!isInt($(p.id).val())){
				showERRMessage(raiseError("XBE-ERR-04", p.desc));
				$(p.id).focus();
				return false;
			}
		}else{
			/* Perentage Values cannot be greater than 100 */
			if (p.maxValue == null){
				if (Number($(p.id).val()) > 100){
					showERRMessage(raiseError("XBE-ERR-04", p.desc));
					$(p.id).focus();
					return false;
				} 
			}
		}
		
		return true;
	}
	
	/*
	 * Number Validations
	  p:id				// Id
	  p:desc			// Description
	  p:minValue		// Minimum Value
	  p:maxValue		// Maximum Value
	  p:blnDecimal		// Decimal Allowed or not [true/false]
	 */
	UI_commonSystem.numberValidateWindow = function(p){
		p = $.extend({minValue: null,
						maxValue: null,
						blnDecimal:false,
						blnPercentage:false,
						maxDecimal : 2
					}, p);
					
		var blnReturn = true;
		if (isNaN($(p.id).val())) {
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", p.desc)});
			$(p.id).focus();
			return false;
		}
		
		if (p.minValue != null){
			if ($(p.id).val() < p.minValue){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", p.desc)});
				$(p.id).focus();
				return false;
			}
		}
		
		if (p.maxValue != null){
			if ($(p.id).val() > p.maxValue){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", p.desc)});
				$(p.id).focus();
				return false;
			}
		}
		
		if(p.blnPercentage){
			/* Percentage Values cannot be greater than 100 */
			if (p.maxValue == null){
				if (Number($(p.id).val()) > 100){
					UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", p.desc)});
					$(p.id).focus();
					return false;
				} 
			}
		}
		
		if (!p.blnDecimal){
			if (isDecimal($(p.id).val())){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", p.desc)});
				$(p.id).focus();
				return false;
			}
			if (!isInt($(p.id).val())){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", p.desc)});
				$(p.id).focus();
				return false;
			}
		}else{//int or decimal valus
			if (!isInt($(p.id).val())){
				if (!isDecimalWithMaxDecimalPoints($(p.id).val(),p.maxDecimal)){
					UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-04", p.desc)});
					$(p.id).focus();
					return false;
				}
			}
		}
		
		return true;
	}
	
	UI_commonSystem.getDummyFrm = function(){
		var frmName = 'dummaryFrm';
		$('#'+frmName).remove();
		$('body').append('<form method="post" name="'+frmName+'" id="'+frmName+'"></form>');
		return $('#'+frmName);
	}
	
	/*
	 * Hide Progress Bar
	 */
	UI_commonSystem.hideProgress = function(){
		$("#divLoadMsg").hide();
		$("#divLoadBg").removeClass('loadingInProgress');
		$("#divLoadBg").addClass('loadingDone');
		try{
			UI_commonSystem.resetTimeOut();
		}catch(ex){}
	}
	
	/*
	 * Show Progress Bar
	 */
	UI_commonSystem.showProgress = function(){
		$("#divLoadBg").removeClass('loadingDone');
		$("#divLoadBg").addClass('loadingInProgress');
		
		$("#divLoadMsg").show();
	}
	
	/*
	 * Reset Page Time out 
	 */
	UI_commonSystem.resetTimeOut = function(){
		if (arguments.length == 1){
			opener.top.ResetTimeOut();
		}else{
			top.ResetTimeOut();
		}
	}
	
	/*
	 * Agents Credit 
	 */
	UI_commonSystem.showAgentCredit = function(){		
		$("#divAvailCredit").html("<p class='txtWhite txtBold'>" + top.strDefCurr  + " " + CurrencyFormat(top.intAgtBalance, top.intDecimals + "<\/p>"));
	}
	
	UI_commonSystem.showAgentCreditInAgentCurrency = function(){		
		try{
			if(top.showAgentCreditInAgentCurrency == "true"){
				$("#divAvailCreditAgentCurrency").html("<p class='txtWhite txtBold'>"+"|" + top.agentCurrency  + " " + CurrencyFormat(top.agentAvailableCreditInAgentCurrency, top.intDecimals + "<\/p>"));
			}
		}catch(error){
			console.log("Error while converting agent credit to agent currency | JQuery.commonSys" + error);
		}
	}
	
	/*
	 * Fill the Grid with Data
	 */
	UI_commonSystem.fillGridData = function(p){
		var i = 0;
		p = $.extend({id:"",
					 data:null,
					 editable:false}, 
					p);
					
		$(p.id).jqGrid("clearGridData", true);
		$(p.id).empty();
		if (p.data != null){
			$.each(p.data, function(){
				$(p.id).addRowData(i + 1, p.data[i]);
				if ($(p.editable)){
					$(p.id).editRow(String(i+1));
				}
				i++;
			});
			//fix to remove className
			if ($().jquery > '1.4.2'){ //TODO to remove this condition once the 1.7.2
				$.each( $(p.id).find("input, button, select"), function(){
					if ($(this).attr("className") != ""){
						$(this).addClass($(this).attr("className"));
						$(this).removeAttr("className");
					}
				});
			}
		}
	}
	
	/*
	 * Page On Change 
	 */
	UI_commonSystem.pageOnChange = function(){
		top[2].HidePageMessage();
	}
	
	/*
	 * Create Cell
	 */
	UI_commonSystem.createCell = function(p){
		var tblCell = document.createElement("TD");
		var strBold = "";
		var strStyle = "";
		var colspan = 0;
		if (p.colspan != undefined && trim(p.colspan) != "") {
			colspan = p.colspan;
		}
		
		p = $.extend({id:"",
						rowSpan: 0,
						colSpan: colspan,
						bold:false,
						textCss:"",
						click:"",
						mouseOver:"",
						mouseOut:"",
						title:"",
						width:"",
						role:"",
						css:"",
						align:"",
						vAlign:""},  
					p);
		
		if (p.bold){strBold = "txtBold";}
		if (p.classVal != ""){strBold += " " + p.classVal;}
		if (p.textCss != ""){strStyle = "style ='" + p.textCss  + "'";}
		
		if (p.id != ""){tblCell.setAttribute("id", p.id);}
		if (p.title != ""){tblCell.setAttribute("title", p.title);}
		if (p.rowSpan > 1){tblCell.setAttribute("rowSpan", p.rowSpan);}
		if (p.colSpan > 1){tblCell.setAttribute("colSpan", p.colSpan);}
		if (p.width != ""){tblCell.setAttribute("width", p.width);}
		if (p.css != ""){tblCell.className = p.css;}
		if (p.align != ""){tblCell.setAttribute("align", p.align);}
		if (p.vAlign != ""){tblCell.setAttribute("valign", p.vAlign);}
		if (p.role != ""){tblCell.setAttribute("role", p.role);}
		if (p.click != ""){tblCell.onclick = p.click;}
		if (p.mouseOver != ""){tblCell.onmouseover = p.mouseOver;}
		if (p.mouseOut != ""){tblCell.onmouseout = p.mouseOut;}
		
		tblCell.innerHTML = "<p class='" + strBold + "' " + strStyle + ">" + p.desc + "<\/p>";
		
		return tblCell;
	}
	
	/*
	 * Return Status Color
	 */ 
	UI_commonSystem.getStatusColor = function(strStatus){
		var strColor = "RED";
		switch (strStatus){
			case UI_commonSystem.strCNXCode : strColor = "RED" ; break;
			case UI_commonSystem.strOHDCode : strColor = "#FFB900" ; break;
			case UI_commonSystem.strWLCode : strColor = "#FFB900" ; break;
			case UI_commonSystem.strCNFCode : strColor = "#09AF00" ; break;
		}
		return "color:" + strColor ; 
	}
	
	UI_commonSystem.setErrorStatus = function(resObj){		
		if(resObj.responseText != null){
			//if user session expired then logout the user
			if(resObj.responseText.indexOf("adfsdfj123dfa893435#@!bnyufUYR345245!RirasdfnM#djf") != -1) {
				top[1].location.replace("showMain");
			}
		}
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
	}
	
	UI_commonSystem.validateCountryPhone = function(countryCode, phoneObj, phCountyCode, phAreaCode, phNo, type){
		var hasNo = false;
		if(phoneObj.length > 0) {
			var counObj;
			for(var cl=0;cl< phoneObj.length;cl++){
				counObj = phoneObj[cl];
				if(counObj.countryPhoneCode == phCountyCode){
					var hasNo = true;
					if(counObj.validateAreaCode && counObj.areaCodes.length > 0){
						var blnFound = false;
						for(al=0;al< counObj.areaCodes.length;al++){
							if(counObj.areaCodes[al].type == type && counObj.areaCodes[al].status == "ACT"){
								if(counObj.areaCodes[al].areaCode == phAreaCode){
									blnFound = true;
									break;
								}
							}
						}
						if(!blnFound){
							showERRMessage(raiseError("XBE-ERR-61"));
							return false;
						}						
					}
					if(phNo.length < Number(counObj.minLength)){
						showERRMessage(raiseError("XBE-ERR-03", type + " NO. " ,Number(counObj.minLength)));
						return false;
					}
					if(phNo.length > Number(counObj.maxlength)){
						showERRMessage(raiseError("XBE-ERR-05",type + " NO. " ,Number(counObj.maxlength)));
						return false;
					}
					break;
				} 
			}
			if(!hasNo) {
				showERRMessage(raiseError("XBE-ERR-61"));
				return false;				
			}
		}
		return true;
	}
	
	/**
	 * Validate time of the format HH:mm:ss
	 */
	UI_commonSystem.timeValidator = function (timeStr){
		var msg = "Invalid time format expected HH:mm:ss";
		if(timeStr == null || $.trim(timeStr) == ''){
			showERRMessage("Time cannot be blank");
			return false;			
		} else {
			var arr = timeStr.split(":");
			if(arr == null || arr.length != 3){
				showERRMessage(msg);
				return false;		
			} else {
				var patt = /[^\d]+/g;
				for(var i = 0 ; i < arr.length ; i++){
					if (patt.test(arr[i])){
						showERRMessage(msg);
						return false;
					}
				}
				
				var hours = parseInt(arr[0], 10);
				var mins = parseInt(arr[1], 10);
				var secs = parseInt(arr[2], 10);
				if(hours < 0 || hours > 23){
					showERRMessage("Invalid hours");
					return false;	
				}
				if(mins < 0 || mins > 59){
					showERRMessage("Invalid minutes");
					return false;	
				}
				if(secs < 0 || secs > 59){
					
					showERRMessage("Invalid seconds");
					return false;	
				}
			}
		}
		return true;
	}
	
	UI_commonSystem.dateFormatter = function (mask) {
		var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
			timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
			timezoneClip = /[^-+\dA-Z]/g,
			pad = function (val, len) {
				val = String(val);
				len = len || 2;
				while (val.length < len) val = "0" + val;
				return val;
			};
		// Some common format strings
		var _dateFormat = {};
		_dateFormat.masks = {
				"accelaero":	"ddmmmyy HH:MM",
				"default":      "ddd mmm dd yyyy HH:MM:ss",
				shortDate:      "m/d/yy",
				mediumDate:     "mmm d, yyyy",
				longDate:       "mmmm d, yyyy",
				fullDate:       "dddd, mmmm d, yyyy",
				shortTime:      "h:MM TT",
				mediumTime:     "h:MM:ss TT",
				longTime:       "h:MM:ss TT Z",
				isoDate:        "yyyy-mm-dd",
				isoTime:        "HH:MM:ss",
				isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
				isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
			};

		// Internationalization strings
		_dateFormat.i18n = {
				dayNames: [
					"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT",
					"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
				],
				monthNames: [
					"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC",
					"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
				]
			};

		// Regexes and supporting functions are cached through closure
		return function (date,  utc) {
			var dF = _dateFormat;

			// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
			if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
				mask = date;
				date = undefined;
			}

			// Passing date through Date applies Date.parse, if necessary
			date = date ? new Date(date) : new Date;
			if (isNaN(date)) throw SyntaxError("invalid date");

			mask = String(dF.masks[mask] || mask || dF.masks["default"]);

			// Allow setting the utc argument via the mask
			if (mask.slice(0, 4) == "UTC:") {
				mask = mask.slice(4);
				utc = true;
			}

			var	_ = utc ? "getUTC" : "get",
				d = date[_ + "Date"](),
				D = date[_ + "Day"](),
				m = date[_ + "Month"](),
				y = date[_ + "FullYear"](),
				H = date[_ + "Hours"](),
				M = date[_ + "Minutes"](),
				s = date[_ + "Seconds"](),
				L = date[_ + "Milliseconds"](),
				o = utc ? 0 : date.getTimezoneOffset(),
				flags = {
					d:    d,
					dd:   pad(d),
					ddd:  dF.i18n.dayNames[D],
					dddd: dF.i18n.dayNames[D + 7],
					m:    m + 1,
					mm:   pad(m + 1),
					mmm:  dF.i18n.monthNames[m],
					mmmm: dF.i18n.monthNames[m + 12],
					yy:   String(y).slice(2),
					yyyy: y,
					h:    H % 12 || 12,
					hh:   pad(H % 12 || 12),
					H:    H,
					HH:   pad(H),
					M:    M,
					MM:   pad(M),
					s:    s,
					ss:   pad(s),
					l:    pad(L, 3),
					L:    pad(L > 99 ? Math.round(L / 10) : L),
					t:    H < 12 ? "a"  : "p",
					tt:   H < 12 ? "am" : "pm",
					T:    H < 12 ? "A"  : "P",
					TT:   H < 12 ? "AM" : "PM",
					Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
					o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
					S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
				};

			return mask.replace(token, function ($0) {
				return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
			});
		};
	}
	
//remove special Characters entered in the input fields and text areas - start
	
	//keypress when paste with CTRL+V  special characters
	$(document).on("keyup",'textarea, input:text, input:password',function(e) {
		if (e.which == 17 || e.which == 86 && e.ctrlKey) {
			var patt = /['|"`~]+/g;
			if (patt.test($(this).val()))
				$(this).val($(this).val().replace(patt,''));
		}
	});
	
	$(document).on("keypress",'textarea, input:text, input:password',function(e) {
		var patt = /['|"`~]+/g;
		if (patt.test($(this).val())){
			$(this).val($(this).val().replace(patt,''));
		}
	});
	//onblur when paste with menu special characters
	$(document).on("blur", 'textarea, input:text, input:password', function(e) {
		var patt = /['|"`~]+/g;
		if (patt.test($(this).val()))
		$(this).val($(this).val().replace(patt,''));
	});
	
	//remove special Characters entered in the input fields and text areas - end
	
	//make readonly page
	UI_commonSystem.readOnly = function (status){
	   	if (status){
	   		$("#page-mask").remove();
	   		$("BODY").append('<div id="page-mask"></div>');
			$("#page-mask").css({
				background: '#ffffff',
				position: 'absolute',
				zIndex: 1000,
				top: '0px',
				left: '0px',
				width: '100%',
				height: $(document).height(),
				opacity:0.1
			});
	   	}else{
	   		$("#page-mask").remove();
	   	}
   	};

   	(function($){
	   	$.fn.filterData = function(attr, startsWith){
	   		var filterFn = function (){
	   			var regex = new RegExp(startsWith+'*');
	   	        return regex.test($(this).attr(attr));
	   		}
	   		var data = {};
	   		this.filter(filterFn).each(function(i, itm){
	   			var jItm = $(itm);
	   			if(jItm.attr('type') != 'radio' || jItm.attr('checked') ) {
	   				data[$(itm).attr('name')] = $(itm).val();
	   			}
	   		});
	   		return data;
	   	}
   	})(jQuery);

   	/* -------------------------------------S O U N D E X    N A M E   V A L I D A T I O N---------------------------------------*/
   	/* ---- Algorithm 1 Steps Start ---- */
   	/* Step 1 - Remove H if next char is a vowel and remove hypens and remove all single letters */
   	function removeH(source){	
   		var vowels = ["a", "e", "i", "o", "u"],
   			sourceLen = source.length;
   			result = "";	
   		for (var idx = 0; idx < sourceLen; idx++) {
   		   if((source[idx].toUpperCase() === "H" && idx < (sourceLen - 1) && vowels.indexOf(source[idx + 1] )== -1) || 
   		   (source[idx].toUpperCase() === "H" && idx === (sourceLen - 1))) {
   				continue;
   		   }
   		   result += source[idx];
   		}
   		
   		var chksingle = result.split(" ");
   		result = "";
   		for (var chk = 0; chk < chksingle.length; chk++){
   			if (chksingle[chk].length === 1){
   			
   			}else{
   				result+=chksingle[chk]+ " ";
   			}
   	   }
   		result=result.trim();
   		
   		return result.replace(/-/g,"");
   	}

   	/* Step 2 - Remove Vowels */
   	function removeVowels(sourceStrng){
   		var vowels=/[aeiou]/gi;
   		replaceWith=""
   		return sourceStrng.replace(vowels,replaceWith);
   	}

   	/* Step 3 - Replace X With KS and TH with TY */
   	function rplceStrings(strngData){	
   		return strngData.replace(/X/g, "KS").replace(/TH/g, "TY");		 
   	}

   	/* Step 4 - Remove repeated and spaces */
   	function removeDuplicates(replData){
   		return replData.replace(/([A-Z])\1+/ig, "$1").replace(/\s/g,"");
   	}

   	/* Step 5 - Compare lengths and search shorter names */
   	function algorithm1(fname1actual, fname2actual, sname1actual, sname2actual){
   		var fname1removeH = removeH(fname1actual).toUpperCase();
   		var fname1removeV = removeVowels(fname1removeH);
   		var fname1replace = rplceStrings(fname1removeV);
   		var fname1removeD = removeDuplicates(fname1replace);
   		
   		var fname2removeH = removeH(fname2actual).toUpperCase();
   		var fname2removeV = removeVowels(fname2removeH);
   		var fname2replace = rplceStrings(fname2removeV);
   		var fname2removeD = removeDuplicates(fname2replace);
   		
   		var sname1removeH = removeH(sname1actual).toUpperCase();
   		var sname1removeV = removeVowels(sname1removeH);
   		var sname1replace = rplceStrings(sname1removeV);
   		var sname1removeD = removeDuplicates(sname1replace);
   		
   		var sname2removeH = removeH(sname2actual).toUpperCase();
   		var sname2removeV = removeVowels(sname2removeH);
   		var sname2replace = rplceStrings(sname2removeV);
   		var sname2removeD = removeDuplicates(sname2replace);
   		
   		var fnameResult;
   		if (fname1removeD.length > fname2removeD.length){
   			fnameResult = fname1removeD.indexOf(fname2removeD)>=0?true:false;
   		}
   		else{
   			fnameResult = fname2removeD.indexOf(fname1removeD)>=0?true:false;
   		}
   		
   		var snameResult;
   		if (sname1removeD.length > sname2removeD.length){
   			snameResult = sname1removeD.indexOf(sname2removeD)>=0?true:false;
   		}
   		else{
   			snameResult = sname2removeD.indexOf(sname1removeD)>=0?true:false;
   		}
   		
   		if (fnameResult && snameResult){
   			return true;;
   		}else{ 
   			/* Step 6 - IF mismatch replace incoming name with db surname and incoming surname with db name */
   			if (fname1removeD.length > sname2removeD.length){
   				fnameResult = fname1removeD.indexOf(sname2removeD)>=0?true:false;
   			}
   			else{
   				fnameResult = sname2removeD.indexOf(fname1removeD)>=0?true:false;
   			}
   			
   			if (sname1removeD.length > fname2removeD.length){
   				snameResult = sname1removeD.indexOf(fname2removeD)>=0?true:false;
   			}
   			else{
   				snameResult = fname2removeD.indexOf(sname1removeD)>=0?true:false;
   			}
   			if (fnameResult && snameResult){
   				return true;
   			}else{
   				return false;
   			}
   		}
   	}
   	/* ---- Algorithm 1 Steps End ---- */

   	/* ---- Algorithm 3 Steps Start ---- */
   	var soundex = function (s) {
   		 var a = s.toLowerCase().split('')
   			 f = a.shift(),
   			 r = '',
   			 codes = {
   				 a: '', e: '', i: '', o: '', u: '',
   				 b: 1, f: 1, p: 1, v: 1,
   				 c: 2, g: 2, j: 2, k: 2, q: 2, s: 2, x: 2, z: 2,
   				 d: 3, t: 3,
   				 l: 4,
   				 m: 5, n: 5,
   				 r: 6
   			 };
   		 
   		 r = f +
   			 a
   			 .map(function (v, i, a) { return codes[v] })
   			 .filter(function (v, i, a) { return ((i === 0) ? v !== codes[f] : v !== a[i - 1]); })
   			 .join('');

   		 return (r + '000').slice(0, 4).toUpperCase();
   	};

   	function getFormatString(name){
   		var param = name;
   		var getData = param.split(" ");
   		var length = 0;
   		var position = 0;
   		var res;
   		
   		return getData.sort(function (a, b) { return b.length - a.length; })[0].replace(/-/g,'');;
   	}

   	UI_commonSystem.nameCompare = function(firstName1, lastName1 , firstName2, lastName2) {		
   		
   		/* Step 1 Start */
   		var fname1 = firstName1.trim();
   		var fname2 = firstName2.trim();
   		var sname1 = lastName1.trim();
   		var sname2 = lastName2.trim();
   		
   		var regex = new RegExp("^[a-zA-Z]+[.-]?[a-zA-Z]*$");
   		
   		if((!fname1 || !sname1) || (!regex.test(fname1.replace(/ /g,"")) || !regex.test(sname1.replace(/ /g,"")))){
   			return;
   		}
   		
   		fname1 = getFormatString(fname1);
   		fname2 = getFormatString(fname2);
   		sname1 = getFormatString(sname1);
   		sname2 = getFormatString(sname2);
   		
   		if((fname1 === fname2) && (sname1 === sname2)){
   			return true;
   		}
   		else{
   		/* Step 2 Start */
   			
   			var param1 = soundex(fname1);
   			var param2 = soundex(fname2);
   			var param3 = soundex(sname1);
   			var param4 = soundex(sname2);
   			
   			if((param1 === param2) && (param3 === param4)){				
   				return true;
   			}else{
   				/* Step 3 Start */
   				
   				var fname1actual = firstName1;
   				var fname1value = fname1actual.split(" ").join("");
   				var fname2actual = firstName2;
   				var fname2value = fname2actual.split(" ").join("");
   				var sname1actual = lastName1;
   				var sname1value = sname1actual.split(" ").join("");
   				var sname2actual = lastName2;
   				var sname2value = sname2actual.split(" ").join("");
   				
   				if((fname1value === fname2value) && (sname1value === sname2value)){			
   					return true;
   				}
   				else{
   				/* Step 4 Start */
   					
   					var param1value = soundex(fname1value);
   					var param2value = soundex(fname2value);
   					var param3value = soundex(sname1value);
   					var param4value = soundex(sname2value);
   					
   					if((param1value === param2value) && (param3value === param4value)){
   						return true;
   					}
   					else{
   					/* Step 5 Start */
   						
   						if((fname1actual === sname2actual) && (fname2actual === sname1actual)){
   							return true;
   						}
   						else{
   							if((fname1 === sname2) && (fname2 === sname1)){
   								return true;
   							}
   							else{
   								if((param3 === param2) && (param1 === param4)){
   									return true;
   								}
   								else{
   									if((fname1value === sname2value) && (fname2value === sname1value)){
   										return true;
   									}
   									else{
   										if((param1value === param4value) && (param3value === param2value)){
   											return true;
   										}
   										else{
   											return algorithm1(fname1actual, fname2actual, sname1actual, sname2actual);
   										}
   									}
   								}
   							}
   						}
   					}
   				}
   			}
   		}
   	}
   	
	
	/* ------------------------------------------------------- End of File ---------------------------------- */
	