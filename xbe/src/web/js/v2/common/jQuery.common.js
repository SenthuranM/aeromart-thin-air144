	/*
	*********************************************************
		Description		: Common prototype routings
		Author			: Rilwan A. Latiff, Baladewa Wealthanthri
		Version			: 1.2
		Created         : 1st June 2005
		Last Modified	: 25th August 2009
    ---------------------------------------------------------
    Ver 1.0
    *********************************************************	
	*/
	
	/* Disable Any Control */
	$.fn.disable = function() {$(this).attr('disabled', true);}
	
	/* Enable Any control */
	$.fn.enable = function() {$(this).attr('disabled', false);}
	
	/* Read only Any Control */
	$.fn.readOnly = function(blnStatus) {$(this).attr('readOnly', blnStatus);}
	
	/* Enable Disable */
	$.fn.disableEnable = function(blnStatus) {
		if (blnStatus){$(this).disable();}else{$(this).enable();}
	}
	/* Check status of controller */
	$.fn.isEnable = function(){
		if($(this).attr('disabled')){
			return false;
		}else{
			return true;
		}
		
		
	}
	/* Reset Form Data */
	$.fn.extend({reset: function(){
		return this.each(function() {
				try{
					$(this).is('form') && this.reset();
				}catch(ex){}
			});
		}
	});
	
	
	
	/* Set Tab Index Any control */
	$.fn.setTabIndex = function(intIndex) {$(this).attr('tabindex', intIndex);}
	
	/* Set Form Action */
	$.fn.action = function(strFileName) {$(this).attr('action', strFileName);}
	
	/* Alpha Numeric Only Allowed */
	$.fn.alphaNumeric = function(p) { 
		p = $.extend({paste: true}, p);
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlphaNumeric(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	  
	/* Alpha Only allowed */
	$.fn.alpha = function(p) { 
		p = $.extend({paste: true}, p);
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlpha(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	
	/* Numeric Only Allowed */
	$.fn.numeric = function(p){
		return this.each(function() {
			$(this).keypress(function (e){
				var charCode = (e.which>=0) ? e.which : e.keyCode;
				if (p != undefined && p.allowDecimal == true){
				  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46  && charCode != 39 ) {
					  return false;
				  }
				  if($(this).val().indexOf(".") > -1 && e.which == 46){
					  return false; 
				  }
				  //if($(this).val().indexOf(".") > -1 && $(this).val().split(".")[1].length > 1 && charCode != 8 && charCode != 46 && charCode != 37 && charCode != 38 && charCode != 39 && charCode != 40){
					//  return false; 
				  //}
				}else{
					if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 39 ) {
					    return false;
					 }
				}
			});
		});
	};
	  
	/* Alpha White Space allowed */
	$.fn.alphaWhiteSpace = function(p) {
		p = $.extend({paste: true}, p); 
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlphaWhiteSpace(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	  
	/* Alpha Numeric White space Allowed */
	$.fn.alphaNumericWhiteSpace = function(p) { 
		p = $.extend({paste: true}, p);
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlphaNumericWhiteSpace(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	/* Collapse Panel
	 * 
	 */
	$.fn.collapsePanel = function(p){
		o = {"state":"expand","startLabel":"","collapseLabel":"test","eapandLabel":"open", "extaFun":null};
		s = $.extend(o, p);
		var difFheight = 0;
		var state = false;
		var eleId = this;
		var divICO = $("<div id='"+eleId[0].id+"_icon' class='icon'>&nbsp;</div>").css({"float":"right","width":15,"height":13});
		divICO.addClass(s.state.toLowerCase());
		divICO.click(function(){
			if (divICO.hasClass(s.state.toLowerCase())){
				difFheight=eleId.height();
				state = true;
				eleId.slideUp();
				if (s.extaFun!=null){
					s.extaFun(state, difFheight);
				}
				divICO.removeClass(s.state.toLowerCase());
				divICO.addClass("collapse");
				
				$("#"+eleId[0].id+"_label").text(s.collapseLabel);
			}else{
				eleId.show();
				difFheight=eleId.height();
				divICO.removeClass("collapse");
				state = false;
				divICO.addClass(s.state.toLowerCase());
				$("#"+eleId[0].id+"_label").text(s.eapandLabel);
				if (s.extaFun!=null){
					s.extaFun(state, difFheight);
				}
			}
			$(this).focus();
		});
		var divLBL = $("<div id='"+eleId[0].id+"_label'>"+s.startLabel+"</div>").css({"float":"right", "text-align":"right", "padding":"0px 5px"});
		var div = $("<div id='"+eleId[0].id+"_div' class='collapser'></div>").append(divICO, divLBL);
		eleId.css("clear","both");
		eleId.before(div);
	};
	
	/* get the Popup Window Properties  */
	$.fn.getPopWindowProp =  function (intHeigh, intWidth){
		var intTop 	= (window.screen.height - intHeigh) / 2;
		var intLeft = (window.screen.width -  intWidth) / 2;
		var strScroll = "no";
		if (arguments.length > 2){
			if (arguments[2] == true){
				strScroll = "yes";
			}
		}
		var strProp = 'toolbar=no,' +
		              'location=no,' +
		              'status=no,' +
		              'menubar=no,' +
		              'scrollbars=' + strScroll + ','+
		              'width=' + intWidth + ',' +
		              'height=' + intHeigh + ',' +
		              'resizable=no,' +
		              'top=' + intTop +',' +
		              'left=' + intLeft
		return strProp
	}
	//Right Click Disable
	
	$(document).bind("contextmenu",function(e){
        return false;
 	});
	
	$.extend($.expr[':'],{
	    'absolute': function(el) {
	        return $(el).css('position') === 'absolute';
	    },
	    'relative': function (el) {
	        return $(el).css('position') === 'relative';
	    },
	    'static': function (el) {
	        return $(el).css('position') === 'static';
	    },
	    'fixed': function (el) {
	        return $(el).css('position') === 'fixed';
	    }
	});
	
	$(document).bind("ready",function(){
		try{
		if (top.db_toolbar){
			if(typeof dashbordToolBar == "undefined") {
				jQuery.getScript('../js/v2/common/dashbordToolBar.js', function() {
		        	dashbordToolBar.init();
		        });
			}
		}
		}catch(e){
			console.log(e);
		}
 	});
 	$(document).keydown(function(e) {
	  if (e.keyCode === 8 && (e.target.type != "text" && e.target.tagName != "TEXTAREA"))
	  {
	     return false;
	  }
	});
 	//Auto crop facility for height issue of element wrap with a div and apply autoCrop();
 	$.fn.autoCrop = function(p){
		return this.each(function() {
			var h = $(this).height();
			var ch = parseInt(p.h,10)
			var w = $(this).width();
			var cw = parseInt(p.w,10)
			if (p.h!="auto" && ch<h){
				$(this).parent().css({"overflow":"hidden","height":ch});
				$(this).css({marginTop:-((h-ch)/2)});
				$(this).find(":absolute").each(function(){
					$(this).css("top", $(this).position().top - ((h-ch)/2) );
				});
			}
			if (p.w!="auto" && cw<w){
				$(this).parent().css({"overflow":"hidden", "width":cw});
				$(this).css({marginLeft:-((w-cw)/2)});
				$(this).find(":absolute").each(function(){
					$(this).css("left", $(this).position().left - ((w-cw)/2) );
				});
			}
		});
 	};
 	//get JSON obj length
 	$.fn.objLen = function(data){
 		var key, count = 0;
 		if (data instanceof Array){
 			count = data.length;
 		}else{
 			for(key in data) {
 		 		  if(data.hasOwnProperty(key) && typeof data[key] !== 'function') {
 		 		    count++;
 		 		  }
 		 		}
 		}
 		return count;
 	}
 	
 	//this will be extensions  the functionality of jqGris GridToForm 
	// if GridToForm return an error please check whether this is included in the jsp
	$.fn.extend({
	 	GridToForm : function( rowid, formid ) {
	 		repDot = function(val){
	 			return val.replace(".","\\.");
	 		};
			return this.each(function(){
				var $t = this;
				if (!$t.grid) { return; } 
				var rowdata = $($t).getRowData(rowid);
				if (rowdata) {
					for(var i in rowdata) {
						if ( $("[name="+repDot(i)+"]",formid).is("input:radio") || $("[name="+repDot(i)+"]",formid).is("input:checkbox"))  {
							$("[name="+repDot(i)+"]",formid).each( function() {
								if( $(this).val() == rowdata[i] ) {
									$(this).prop("checked",true);
								} else {
									$(this).prop("checked",false);
								}
							});
						} else {
						// this is very slow on big table and form.
							$("[name="+repDot(i)+"]",formid).val(rowdata[i]);
						}
					}
				}
			});
		}
	});
 