dateValidity = {};
var opType = "PLUS";
var arrDay_short = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
var arrDay = ["Sunday","Monday","Tuesday","Wednsday","Thursday","Friday","Saturday"];
var arrMonth_short = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
var arrMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"];
dateValidity.ready = function(){
	$( "#txtDateFrom" ).datepicker({
		showOn: "button",
		buttonImage: "../images/Calendar_no_cache.gif",
		buttonImageOnly: true,
		dateFormat: UI_commonSystem.strDTFormat,
		changeMonth: 'true',
		changeYear: 'true',
		showButtonPanel: true
	});
	$( "#txtDateFrom" ).blur(function(){
		if($.trim($(this).val()) != ''){
			dateChk("txtDateFrom");
			$("#txtTimeFrom").val(getCurrentTime());
		} else {
			$("#txtTimeFrom").val(getCurrentTime());
		}
	});
	$( "#txtnoDays" ).numeric({allowDecimal:false});
	$( "#btnValidate" ).click(function(){
		if(dateChk("txtDateFrom")){
			var tookDate = new Date();
			if ($( "#txtDateFrom" ).val() == ''){
				alert("From Date is blank used today as From Date");
				$( "#txtDateFrom" ).datepicker( "setDate" , tookDate );
			}
			if($( "#txtTimeFrom" ).val() == ''){
				alert("From Time cannot be blank.");
				$("#txtTimeFrom").val(getCurrentTime());
			}else if(!validateTimeFormat($( "#txtTimeFrom" ).val())) {
				alert("Time is incorrect.");
				$("#txtTimeFrom").val(getCurrentTime());
			}
		} else {
			var tookDate = new Date();
			$( "#txtDateFrom" ).datepicker( "setDate" , tookDate );
		}		
		setDateValue();
	});
	$( "input[name='radVarious']" ).click(function(){
		opType = $(this).val();
		setDateValue();
	});
};

function validateTimeFormat(curentTime){
	var time = curentTime.split(":");
	if(time.length == 2 && curentTime.indexOf(":") == 2){
		if(!isNaN(Number(time[0])) && !isNaN(Number(time[1]))){
			return true;
		}
		return false;
	}
	return false;
}

function getCurrentTime(){
	var today = new Date();
	var hrs = today.getHours();
	var mins = today.getMinutes();
	if (parseInt(hrs) < 10) { 
		hrs = "0" + mins; 
	}
	if (parseInt(mins) < 10) { 
		mins = "0" + mins; 
	}
	return hrs + ":" + mins;
}

	
function setDateValue(){
	serZero = function(v){
		var t = "";
		var v = String(v);
		if (v.length == 1){
			t = "0"+v;
		}else{
			t = v;
		}
		return t;
	}
	
	var tookDate = new Date();
	//split date
	var textDate = $("#txtDateFrom" ).val().split("/");
	var textTime = $("#txtTimeFrom" ).val().split(":");
	
	//set date
	tookDate.setFullYear(parseInt( textDate[2]) );
	tookDate.setMonth(parseInt( textDate[1] - 1));
	tookDate.setDate( textDate[0] );
	tookDate.setHours(textTime[0]);
	tookDate.setMinutes(textTime[1]);
	
	var dateChage = null;

	if (opType.toUpperCase() == "PLUS"){
		dateChage = new Date( tookDate.getTime() + ((parseInt($( "#txtnoDays" ).val())) *24*60*60*1000 ) );
	}else{
		dateChage = new Date( tookDate.getTime() - ((parseInt($( "#txtnoDays" ).val())) *24*60*60*1000 ) );
	}
	
	var hrs = dateChage.getHours();
	var mins = dateChage.getMinutes();
	if (parseInt(hrs) < 10) { 
		hrs = "0" + mins; 
	}
	if (parseInt(mins) < 10) { 
		mins = "0" + mins; 
	}
	var dipFullDate = arrDay[ dateChage.getDay() ] + " " + arrMonth[ dateChage.getMonth() ] + " " + dateChage.getDate() + " " + dateChage.getFullYear() + " " + hrs + ":" + mins;
	var dipShortDate = serZero( dateChage.getDate() ) + "/" + serZero( (parseInt( dateChage.getMonth()) + 1) ) + "/" + dateChage.getFullYear() + " " + hrs + ":" + mins;
	$( "#fullDate" ).text(dipFullDate);
	$( "#shortDate" ).text(dipShortDate);
}