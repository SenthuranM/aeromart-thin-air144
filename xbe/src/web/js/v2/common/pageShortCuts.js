/**
 * Common Page Short cuts for Make / Modify Reservation
 * @author Navod Ediriweera
 * //TODO: Meals/ HALA services are depending on priviliges.. Need to make the legend creation privilige based
 **/	
function UI_PageShortCuts (){}
	
/*** Reservation Legend
 		
 		Alt + Ctrl Menu of shortcut keys
 		
		Alt + A Adults
		Alt + B Book
		Alt + C Calendar
		Alt + D Departure Date
		
		Alt + F From
		Alt + G Return Variance
		Alt + I Infants
		Alt + J Children
		
		Alt + K Class
		Alt + L Tax Breakdown
		Alt + M Modify Fares
		Alt + N Next Days Info
		
		Alt + O Flight Info
		Alt + P Previous Days Info
		Alt + R Return Date
		Alt + S Expand/Collapse Search
		
		Alt + T To
		Alt + U Fare Quote
		Alt + V Departure Variance
		Alt + W Reset
		
		Alt + X  Sign Out
		Alt + Y  Search
		Alt + Z  Close
		Alt + Shift + $  Currency
****/

UI_PageShortCuts.isAjaxInProgress = false;
UI_PageShortCuts.objClearShortCutTimer = null;
UI_PageShortCuts.metaData = {
	focus : {},
	click:{},
	execute:{},
	jqGridTabSelect: {}
}
$(document).ready(function(){
	$("#spnShortCutInfo").hide();		
	UI_PageShortCuts.attachAjaxData();
	UI_PageShortCuts.attachOnKeyPressFunction();
	
	UI_PageShortCuts.attachOnKeyDown();
});

UI_PageShortCuts.showShortCuts = function(){		
	//UI_PageShortCuts.objClearShortCutTimer = setTimeout("UI_PageShortCuts.hideShortCuts()",10000);
	$("#spnShortCutInfo").show();
}

UI_PageShortCuts.hideShortCuts = function(){
	$("#spnShortCutInfo").hide();
	//clearTimeout(UI_PageShortCuts.objClearShortCutTimer);
}

UI_PageShortCuts.writeShortCuts = function(strMsg){	
	if (arguments.length == 2){
		getFieldByID("spnShortCutInfo").style.top = arguments[1];
	}
	if (arguments.length == 3){
		getFieldByID("spnShortCutInfo").style.width = arguments[2];
		getFieldByID("spnShortCutInfo").style.left = 50;
	}
  	DivWrite("spnShortCutDetails", strMsg);
}

UI_PageShortCuts.clearShortCutToolTip = function (){
	$('#spnShortCutDetails').empty();
}

UI_PageShortCuts.attachAjaxData = function (){
	$(document).ajaxSend(function(){		
		UI_PageShortCuts.isAjaxInProgress = true;
	});
	$(document).ajaxStop(function(){	
		UI_PageShortCuts.isAjaxInProgress = false;
	});
}

UI_PageShortCuts.attachOnKeyPressFunction = function (){		
	$(document).keyup (function (event){
		UI_PageShortCuts.onKeyPressCommon(event);	
	});	
}

UI_PageShortCuts.attachOnKeyDown = function (){		
	$(document).keydown  (function (event){
		$("#spnShortCutDetails").find("td").removeClass("heigh-light");
		
		/* Using just the Alt key was giving problems in key boards using the Alt key for auxiliary functions, 
		 * like Italian key boards. Therefore the menu will be created for Ctrl + Alt now.
		 */
		if (event.ctrlKey) {
			if (event.keyCode == 18) {
				UI_PageShortCuts.showShortCuts();
			}
		}
		if (event.keyCode == 27){
			UI_PageShortCuts.hideShortCuts();	//escape
		}
	});
	$(document).keyup(function (event){
		if (event.ctrlKey) {
			if (event.which == 18){
				UI_PageShortCuts.hideShortCuts();
			}else{
				setTimeout('UI_PageShortCuts.hideShortCuts()',700);
			}
		}
	});
}

UI_PageShortCuts.onKeyPressCommon = function (event){
	if(UI_PageShortCuts.isAjaxInProgress) return true;
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	if(keyCode == 13){
		UI_PageShortCuts.doOperation('enter');										//Enter
	}
	if (event.altKey){		
		switch (keyCode){
			case 51 : 
				UI_PageShortCuts.doOperation('alt3');							
				break;																// 3
			case 52 : 
				if (event.shiftKey){
					UI_PageShortCuts.doOperation('altShiftDollar');						// Shift + $
				} else {			
					UI_PageShortCuts.doOperation('alt4');							// 4
				}
				break;																
			case 53 : 
				UI_PageShortCuts.doOperation('alt5');
				break;																// 5
			case 54 : 
				UI_PageShortCuts.doOperation('alt6');
				break;																// 6
			case 65 : 
				UI_PageShortCuts.doOperation('altA');
				break;																// A
			case 66 : 																					
				UI_PageShortCuts.doOperation('altB');
				break;																// B
			case 67 :																						
				UI_PageShortCuts.doOperation('altC');
				break;																// C
			case 68 : 
				UI_PageShortCuts.doOperation('altD');
				break;																// D	
			case 69 :  
				UI_PageShortCuts.doOperation('altE');
				break;  															//E
			case 70 :  
				UI_PageShortCuts.doOperation('altF');
				break;    															// F
			case 71 : 
				UI_PageShortCuts.doOperation('altG');
				break;																// G
			case 72 : 
				UI_PageShortCuts.doOperation('altH');
				break;																// H
			case 73 : 
				UI_PageShortCuts.doOperation('altI');
				break;																// I															
			case 74 : 
				UI_PageShortCuts.doOperation('altJ');
				break;																// J							
			case 75 : 
				UI_PageShortCuts.doOperation('altK');
				break;																// K
			case 76 :	
				UI_PageShortCuts.doOperation('altL');		
				break ;																// L	
			case 77 :
				UI_PageShortCuts.doOperation('altM');				
				break ;																// M
			case 78 : 		
				UI_PageShortCuts.doOperation('altN');
				break;																// N
			case 79 : 
				UI_PageShortCuts.doOperation('altO');																		
				break;																// O	
			case 80 : 
				UI_PageShortCuts.doOperation('altP');								
				break;																// P
			case 81 : 				
				UI_PageShortCuts.doOperation('altQ');				
				break ;																// Q		
			case 82 : 				
				UI_PageShortCuts.doOperation('altR');				
				break ;																// R									 				
			case 83 : 
				UI_PageShortCuts.doOperation('altS');		
				break;																// S
			case 84 : 
				UI_PageShortCuts.doOperation('altT');								
				break;   															// T
			case 85 : 
				UI_PageShortCuts.doOperation('altU');
				break;																// U							
			case 86 : 
				UI_PageShortCuts.doOperation('altV');				
				break;																// V																			
			case 87 : 
				UI_PageShortCuts.doOperation('altW');				
				break;																// W																			
			case 88 : 
				UI_PageShortCuts.doOperation('altX'); 
				break;																// X
			case 89 : 
				UI_PageShortCuts.doOperation('altY');					
				break;																// Y
			case 90 : close
				UI_PageShortCuts.doOperation('altZ');	
				break;																// Z

		}		
	}
}

UI_PageShortCuts.doOperation = function(objKeyCode){
	$('.'+objKeyCode.toLowerCase()).addClass("heigh-light");
	if(objKeyCode==null) return ;
	if(UI_PageShortCuts.isPresent(UI_PageShortCuts.metaData.focus[objKeyCode])) {
		$('#'+UI_PageShortCuts.metaData.focus[objKeyCode]).focus();
	}
	else if(UI_PageShortCuts.isPresent(UI_PageShortCuts.metaData.click[objKeyCode])) {
		$('#'+UI_PageShortCuts.metaData.click[objKeyCode]).click(); 
	}
	else if(UI_PageShortCuts.isPresent(UI_PageShortCuts.metaData.jqGridTabSelect['tabParentID'])) {
		var divTabParent = UI_PageShortCuts.metaData.jqGridTabSelect['tabParentID']; 
		$('#'+divTabParent).tabs("option", "active", UI_PageShortCuts.metaData.jqGridTabSelect[objKeyCode]); 
	} else if(UI_PageShortCuts.metaData.execute[objKeyCode] != null) {
		eval(UI_PageShortCuts.metaData.execute[objKeyCode]);
	}
}

UI_PageShortCuts.isPresent = function (obj){	
	if(obj==null) return false;
	var blnIsPresent = $('#'+obj).length > 0 ;
	if(!blnIsPresent) return false;
	if($('#'+obj).attr('disabled')) return false;
	return 	$('#'+obj).is(":visible");
}

UI_PageShortCuts.replaceMetaData = function (newMetaDataObj){	
	UI_PageShortCuts.clearShortcutToolTip(); 
	UI_PageShortCuts.metaData = newMetaDataObj;
	if(UI_PageShortCuts.metaData['focus'] == null)
		UI_PageShortCuts.metaData['focus'] = {};
	if(UI_PageShortCuts.metaData['click'] == null)
		UI_PageShortCuts.metaData['click'] = {};
	if(UI_PageShortCuts.metaData['execute'] == null)
		UI_PageShortCuts.metaData['execute'] = {};
	if(UI_PageShortCuts.metaData['jqGridTabSelect'] == null)
		UI_PageShortCuts.metaData['jqGridTabSelect'] = {};
}

/**
 * params = {
 * 	{"Alt + A","Adults"},..
 * }
 * OBJs containing {altA:Adults}
 */
UI_PageShortCuts.writeShortCutKeys = function(displayObj){	
	trimSpecial = function(txt){
		var temp = "";
		if (txt.indexOf(" ") > -1){
			var txtArr = txt.split(" ");
			for (var i = 0; i < txtArr.length; i++){
				if (txtArr[i] != "+")
					temp +=  txtArr[i];
			}
		}
		return temp.toLowerCase();
	}
	var strHTMLText = "";
	strHTMLText += "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
	var index = 0;
	for ( var obj in displayObj) {
		if(index == 0) strHTMLText	+= " <\/tr>";
		else if(index % 4 == 0){
			strHTMLText	+= " <\/tr> <tr>";
		}
		strHTMLText	+= "	<td class='short-cut-box "+trimSpecial(obj)+"'><font class='short-cut-abbr'>" + obj 
		+ " <\/font><font class='short-cut-desc'>= "+ displayObj[obj] +"<\/font><\/td>";
		index++;
	}
	strHTMLText += " <\/tr><\/table>";
	if(arguments.length==2)
		UI_PageShortCuts.writeShortCuts(strHTMLText, arguments[1]);
	else if(arguments.length==3)
		UI_PageShortCuts.writeShortCuts(strHTMLText, arguments[1], arguments[2]);
	else
		UI_PageShortCuts.writeShortCuts(strHTMLText);
}
 
UI_PageShortCuts.clearShortcutToolTip = function (){
	UI_PageShortCuts.writeShortCutKeys({});
}

UI_PageShortCuts.initilizeMetaDataForSearchFlight = function (){
	var metaData = {
		focus : {		
			altA :"adultCount",
			altD: "departureDate",	
			altF:"fAirport",
			altG: "returnVariance",
			altI:"infantCount",
			altJ:"childCount",
			altK: "classOfService",
			altR: "returnDate",
			altT:"tAirport",		
			altV: "departureVariance",		
			altShiftDollar:"selectedCurrency"
		},
		click:{
			altB: "btnBook",		
			altL:"btnTaxSurB",
			altU:"btnFQ",
			altM:"btnCFare",
			altN:"lnkON",
			altP:"lnkOP",
			nextDayReturn:"lnkRN",
			previousDayReturn:"lnkRP",
			altW:"btnReset",			
			altY:"btnSearch",
			altZ:"btnCancel",
			enter:"btnBook", // b4 search is done book button is not shown. so search is executed
			altQ:"trSearchParams_icon"	
		},
		execute : {
			altX:"top.CloseWindow();"
		}
		
	};
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {			
		"Alt + A":"Adults",
		"Alt + B":"Book",
		"Alt + D":"Departure Date",
		"Alt + F":"From",
		"Alt + G":"Return Variance",
		"Alt + I":"Infants",
		"Alt + J":"Children",
		"Alt + K":"Class",
		"Alt + L":"Tax Breakdown",
		"Alt + M":"Modify Fares",
		"Alt + N":"Next Days Info",
		"Alt + P":"Previous Days Info",
		"Alt + R":"Return Date",
		"Alt + T":"To",
		"Alt + U":"Fare Quote",
		"Alt + V":"Departure Variance",
		"Alt + W":"Reset",
		"Alt + X":"Sign Out",
		"Alt + Y":"Search",
		"Alt + Z":"Close",
		"Alt + Shift + $":"Currency",
		"Alt + Q":"Show/Hide Search Panel"	
	};
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay,"420");
}

UI_PageShortCuts.initilizeMetaDataForPassengerScreen = function (){
	var metaData = {
		focus : {		
			altF:"selTitle",
			altH:"selSalesPoint",			
			altP: "txtPNR"
		},
		click:{
			altB: "btnTBA",	
			altE: "btnContinue",			
			altL:"btnLoadProfile",			
			altO:"btnPHold",
			altW:"btnPReset",
			altZ: "btnPCancel"			
		}			
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
		"Alt + B":"TBA",	
		"Alt + E":"Continue",
		"Alt + F":"Focus to Title",
		"Alt + H":"Sales points",
		"Alt + L":"Load",
		"Alt + O":"On Hold",
		"Alt + P":"Enter Profile",
		"Alt + W":"Reset",	
		"Alt + Z":"Cancel"
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay,450);
}

UI_PageShortCuts.initilizeMetaDataForPaymentScreen = function (){
	var metaData = {
		focus : {		
			altA:"txtPayAmt",					
			altK: "cardType",
			altP: "selCardType",
			altU:"radPaymentTypeA"
		},
		click:{
			altE:"btnPayConfirm",	
			altF: "btnPayForce",
			altW:"btnPayReset",
			altZ:"btnPayCancel"	
		}			
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
		"Alt + A":"Amount",	
		"Alt + E":"Confirm",
		"Alt + F":"Force Confirm",
		"Alt + K":"Card Type",
		"Alt + P":"Payment Method",
		"Alt + U":"On Account",
		"Alt + W":"Reset",
		"Alt + Z":"Cancel"
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay,450);
}

UI_PageShortCuts.initilizeMetaDataForModificationSearch = function (){
	var metaData = {
		focus : {		
			altA:"firstName",					
			altB: "lastName",
			altD:"depatureDate",
			altF:"fromAirport",					
			altG: "flightNo",
			altH: "authCode",
			altI:"passport",			
			altK:"creditCardNo",					
			altL: "cardExpiry",
			altM: "bookingType",
			altN:"txtPhoneCntry",
			altP:"pnr",					
			altR: "returnDate",			
			altT:"toAirport"				
		},
		click:{
			altS: "lnkAdvSrch",
			altW: "btnReset",
			altY: "btnSearch",
			altZ:"btnClose"
		}			
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
		"Alt + A":"First Name",	
		"Alt + B":"Last Name",
		"Alt + D":"Departure Date",
		"Alt + F":"From",
		"Alt + G":"Flight No",
		"Alt + H":"CC auth. Code",
		"Alt + I":"Passport",
		"Alt + K":"Credit Card",	
		"Alt + L":"Expiry Date",
		"Alt + M":"BC Type",
		"Alt + N":"Telephone No",
		"Alt + P":"PNR",
		"Alt + R":"Arrival Date",
		"Alt + S":"Advance search",
		"Alt + T":"To",
		"Alt + W":"Reset",
		"Alt + Y":"Search",
		"Alt + Z":"Close"
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay,510,900);
}

UI_PageShortCuts.initilizeMetaDataForModoficationBookingTab = function (){
	var metaData = {
		click:{
			altA:"btnAddSegment",					
			altB: "btnClose",
			altC:"btnCancel",
			altE:"btnEmailItn",
			altF:"btnViewItn",
			altG: "btnCanSegment",	
			altH:"btnExtend",
			altI:"btnAddInfants",			
			altL: "btnSplit",
			altM:"btnModSegment",
			altO:"btnTransfer",
			altR: "btnRemovePax",			
			altS: "btnSave",
			altV:"btnViewNotes",
			altW:"btnReset"
		},
		jqGridTabSelect : {	
			tabParentID: "divBookingTabs",
			alt3:0,
			alt4:1,
			alt5:2,
			alt6:3		
		}
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
			"Alt + A":"Add Segment",	
			"Alt + B":"Back",
			"Alt + C":"Cancel Reservation",
			"Alt + E":"Email Itinerary",
			"Alt + F":"View Itinerary",
			"Alt + G":"Cancel Segment",
			"Alt + H":"Extend On-Hold",
			"Alt + I":"Add/Save Infant",
			"Alt + L":"Split Reservation",			
			"Alt + M":"Modify Segment",
			"Alt + O":"Transfer Ownership",
			"Alt + R":"Remove Passengers",
			"Alt + S":"Edit / Save",
			"Alt + V":"View User Notes",
			"Alt + W":"Reset",
			"Alt + 3":"Passenger Details",
			"Alt + 4":"Contact Details",
			"Alt + 5":"Payments",
			"Alt + 6":"History"
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay);
}

UI_PageShortCuts.initilizeMetaDataForModoficationContactDetailsTab = function (){
	var metaData = {
		jqGridTabSelect : {	
			tabParentID: "divBookingTabs",
			alt3:0,
			alt4:1,
			alt5:2,
			alt6:3	
		}
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
			"Alt + 3":"Passenger Details",
			"Alt + 4":"Contact Details",
			"Alt + 5":"Payments",
			"Alt + 6":"History"
				
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay);
}

UI_PageShortCuts.initilizeMetaDataForModificationPaymentTab = function (){
	var metaData = {
		click:{
			altA:"btnAddSegment",					
			altE:"btnClose",
			altF:"btnCancel",
			altK:"btnEmailItn",
			altP:"btnViewItn",
			altU:"btnCanSegment",	
			altW:"btnExtend",
			altZ:"btnAddInfants"
		},
		jqGridTabSelect : {	
			tabParentID: "divBookingTabs",
			alt3:0,
			alt4:1,
			alt5:2,
			alt6:3		
		}
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
			"Alt + A":"Amount",
			"Alt + E":"Confirm",
			"Alt + F":"Force Confirm",
			"Alt + K":"Card Type",
			"Alt + P":"Payment Method",
			"Alt + U":"On Account",
			"Alt + W":"Reset",
			"Alt + Z":"Cancel",				
			"Alt + 3":"Passenger Details",
			"Alt + 4":"Contact Details",
			"Alt + 5":"Payments",
			"Alt + 6":"History"
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay);
}

UI_PageShortCuts.initilizeMetaDataForModoficationHistoryTab = function (){
	var metaData = {
		jqGridTabSelect : {	
			tabParentID: "divBookingTabs",
			alt3:0,
			alt4:1,
			alt5:2,
			alt6:3	
		}
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
			"Alt + 3":"Passenger Details",
			"Alt + 4":"Contact Details",
			"Alt + 5":"Payments",
			"Alt + 6":"History"
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay);
}

UI_PageShortCuts.initilizeMetaDataForComfirmUpdate = function (){
	var metaData = {
		click:{
			altA:"btnOverride",					
			altE:"btnClose",
			altO:"btnConfirm",
			altW:"btnReset",
			altZ:"btnClose"
		}
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
			"Alt + A":"Override",
			"Alt + E":"Goto CNX/MOD Charge",
			"Alt + O":"Confirm",
			"Alt + W":"Reset",
			"Alt + Z":"Close"
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay);
}
UI_PageShortCuts.initilizeMetaDataForReservationAncillary = function (){
	var metaData = {
		click:{
			altE:"btnAnciContinue",				
			altI:"imgTab_4",
			altM:"imgTab_1",
			altR:"imgTab_0",
			altS:"imgTab_2",
			altV:"btnAnciView",
			altZ:"btnAnciCancel"
		}
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
			"Alt + E":"Continue",
			"Alt + R":"SSR",
			"Alt + I":"Insuarance",			
			"Alt + S":"Seats",		
			"Alt + V":"View Summary",			
			"Alt + Z":"Cancel"			
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay);
}

UI_PageShortCuts.initilizeMetaDataForModificationAncillary = function (){
	var metaData = {
		click:{
			altM:"btnAnciEdit"
		}
	}
	UI_PageShortCuts.replaceMetaData(metaData);
	var shortCutDisplay = {
			"Alt + M":"Add/Modify Ancillary"
			
	}
	UI_PageShortCuts.writeShortCutKeys(shortCutDisplay);
}