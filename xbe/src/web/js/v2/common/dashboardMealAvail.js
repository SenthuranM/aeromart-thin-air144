
function UI_dashboardMeal(){}

UI_dashboardMeal.ready = function(){
	
	$('#departureDateMeal').datepicker(
			{
		       minDate: new Date(),
		       dateFormat: 'dd/mm/yy'
		    });
	
	$("#departureDateMeal").blur(function() { dateChk('departureDateMeal');});
	
	$("#btnSearchMeal").click(function() {
		UI_dashboardMeal.searchMealOnClick();
		});
	
	UI_dashboardMeal.fillDropDownData();
}

UI_dashboardMeal.fillDropDownData = function(){
	
	$("#classOfServiceMeal").fillDropDown( { dataArray:top.arrLogicalCC , keyIndex:0 , valueIndex:1 });
}

UI_dashboardMeal.searchMealOnClick = function() {
	
	if(UI_dashboardMeal.validateData()){
		var url = "dashboardMealAvailability.action";
		var data = {};
		data.flightNumber = $.trim($("#flightNum").val());
		data.departureDate = $.trim($("#departureDateMeal").val());
		
		var cos = $("#classOfServiceMeal").val();
		var cosArr = cos.split("-");
		if(cosArr.length > 1){
			data.logicalCabinClass = cosArr[0];
			data.classOfService = cosArr[1];
		} else if(cosArr.length == 1) {
			data.classOfService = cosArr[0];
			data.logicalCabinClass = '';
		}	
		UI_commonSystem.showProgress();
		$.ajax({type: "POST", dataType: 'json',data: data , url:url, 	
		success:UI_dashboardMeal.searchMealOnSuccess, error:UI_dashboardMeal.searchMealError, cache : false});
	}

}

UI_dashboardMeal.searchMealOnSuccess = function(mealResponse) {
	
	$("#mealDisplayDiv").empty();
	
	if(mealResponse.success == true){
			var gridHeadings = [];
		    var gridColumns = [];
		    gridHeadings = ['Meal', 'Category','Charge','Available'];
		    gridColumns = [{
		        name: 'mealName',
		        index: 'mealName',
		        width: 215,
		        align: "left"
		    }, {
		        name: 'mealCategoryCode',
		        index: 'mealCategoryCode',
		        width: 105,
		        align: "center"
		    },{
		        name: 'mealCharge',
		        index: 'mealCharge',
		        width: 90,
		        align: "center"
		    },{
		        name: 'availableMeals',
		        index: 'availableMeals',
		        width: 90,
		        align: "center"
		    }];
		          
		    		var flightNumberLabel = $('<label>').attr( "id", "flightNumberLabelMeal");
		    		var mealGrid = $('<table>').attr( "id", "mealGrid");
		        	
		        	$("#mealDisplayDiv").append(flightNumberLabel);
		        	$("#mealDisplayDiv").append(mealGrid);
		        	mealGrid.jqGrid({
		        	    datatype: "local",
		        	    colNames: gridHeadings,
		        	    colModel: gridColumns,
		        	    scrollrows: true,
		                cellsubmit: 'clientArray',
		                viewrecords: true,
		                height:220
		        	});
		        	
		        	var gridData = mealResponse.meals;
		        	$.each(gridData, function(i, rowData){
		        		mealGrid.addRowData(i + 1, rowData);
		        	});
		        	UI_commonSystem.hideProgress();
	} else {
		UI_commonSystem.hideProgress();
		$("#mealDisplayDiv").empty();
		showWindowCommonError("Error", mealResponse.messageTxt);
	}        	
}

UI_dashboardMeal.searchMealError = function() {
	UI_commonSystem.hideProgress();
	$("#mealDisplayDiv").empty();
	window.close();
}

UI_dashboardMeal.validateData = function(mealResponse) {
	if ($("#departureDateMeal").val() == "") {
		showWindowCommonError("Error", "Departure Date Can Not be Empty");
		return false;
		
	}else if ($("#departureDateMeal").val() == "" || ($("#departureDateMeal").val().indexOf("/") == -1)){
		showWindowCommonError("Error","Wrong date format of Departure Date");
		return false;
		
	} else if (!CheckDates(UI_dashboardMeal.currentDate(), $("#departureDateMeal").val())){
		showWindowCommonError("Error","Entered date should not be less than " + UI_dashboardMeal.currentDate());	
		return false;
	}
	else if ($("#flightNum").val() == "") {
		showWindowCommonError("Error", "Flight Number Can Not be Empty");
		return false;
	} else {
		return true;
	}
}

UI_dashboardMeal.currentDate = function () {
	return window.opener.strSysDate;
}

UI_dashboardMeal();
UI_dashboardMeal.ready();