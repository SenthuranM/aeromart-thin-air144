	
	var strSYSError = "";
	// ------------------------ Common Functions -----------------------
	
	/* focus to a control */
	function setFocus(id){
		var objControl = getFieldByID(id);
		objControl.focus();
	}
	
	/* Set Display */
	function setDisplay(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if (!blnVisible){
			objControl.style.display = "none";
		}else{
			objControl.style.display = "block";
		}
	}
	
	/* Fill characters  */
	function fillChar(strValue, intLength, strChar){
		strValue = trim(String(strValue));
		var intVLength = strValue.length; 
		var strZero = "";
		for (var i = intVLength ; i < intLength ; i++){
			strZero += strChar;
		}
		
		if (arguments.length == 4){
			if (arguments[3]){
				strValue = String(strValue) + String(strZero);
			}else{
				strValue = String(strZero) + String(strValue);
			}
		}else{
			strValue = String(strZero) + String(strValue);
		}
		return strValue;
	}
	//--------------------------------------------------------
	
	
	/*
	 * Initialize Message
	 */
	function initializeMessage(){
		top[2].HidePageMessage();
	}
	
	
	// date convertion 
	function StringToDate(strDate){
		var strDay=strDate.substring(0,2);
		var strMonth=strDate.substring(3,5);
		var strYear=strDate.substring(6,10); 
		var dtDate = new Date(strYear, Number(strMonth) - 1, strDay);
		return dtDate
	}
	
	// compare 2 dates
	function CheckDates(tempIStartDate,tempIEndDate){
		var tempDay;
		var tempMonth;
		var tempYear;
	
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);
		
		
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);
		
		if (arguments.length == 2){
			if (tempOEndDate >= tempOStartDate){
				return true;
			}else{
				return false;
			}	
		}else{
			if (arguments[2] == true){
				if (tempOEndDate > tempOStartDate){
					return true;
				}else{
					return false;
				}	
			}
		}
	}
	
	createCell = function(p){
		var tblCell = document.createElement("TD");
		var strBold = "";
		var strStyle = "";
		
		p.id        = (p.id != null) ? p.id : "";
		p.rowSpan   = (p.rowSpan != null) ? p.rowSpan : 0;
		p.bold      = (p.bold != null) ? p.bold : false;
		p.textCss   = (p.textCss != null) ? p.textCss : "";
		p.click     = (p.click != null) ? p.click : "";
		p.width     = (p.width != null) ? p.width : "";
		p.css       = (p.css != null) ? p.css : "";
		p.align     = (p.align != null) ? p.align : "";
		
		if (p.bold){strBold = "txtBold";}
		if (p.textCss != ""){strStyle = "style ='" + p.textCss  + "'";}
		
		if (p.id != ""){tblCell.setAttribute("id", p.id);}
		if (p.rowSpan > 1){tblCell.setAttribute("rowspan", p.rowSpan);}
		if (p.colSpan > 1){tblCell.setAttribute("colspan", p.colSpan);}
		if (p.width != ""){tblCell.setAttribute("width", p.width);}
		if (p.css != ""){tblCell.setAttribute("class", p.css);}
		if (browser.isIE){tblCell.className =  p.css;}
		if (p.align != ""){tblCell.setAttribute("align", p.align);}
		
		if (p.click != ""){tblCell.onclick = p.click;}
		
		if (p.desc == ""){p.desc = "&nbsp;";}
		
		tblCell.innerHTML = "<font class='" + strBold + "' " + strStyle + ">" + p.desc + "<\/font>";
		
		return tblCell;
	}
	

	function getTotalPercentageValue(intValue, intHighest){
		return Math.round((Number(intValue) / Number(intHighest)) * 100);
	}
	
	function deleteTableRows(objTbl){
		var intRows = objTbl.rows.length;
        if (intRows > 0){
            var i = 0;
		    do{
			    objTbl.deleteRow(intRows - 1);
		    }while(--intRows);
	    }
	}
	
	function handleCommonAjaxInvocationErrors(resObj){
		if (resObj.responseText != null) {
			//if user session expired then logout the user
			if (resObj.responseText
					.indexOf("adfsdfj123dfa893435#@!bnyufUYR345245!RirasdfnM#djf") != -1) {
				top[1].location.replace("showMain");
			}
		}
		if ((top.objCW) && (!top.objCW.closed)) {
			top.objCW.close();
		}	
	}
	
	function UI_Common(){}
	UI_Common.displayMsgObj = {};
	UI_Common.displayMsgObj.message  = "";
	UI_Common.displayMsgObj.msgType = "";
	UI_Common.displayMsgObj.clearData = function (){
		UI_Common.displayMsgObj.message = "";
		UI_Common.displayMsgObj.msgType = "";
	}

	UI_Common.displayResponseMessage = function (response) {
		UI_Common.displayMessage({msgType:response.msgType, message:response.message});
	}

	UI_Common.displayMessage = function (params){
		var messageType = params.msgType;
		var errorMessage = params.message;
		if(messageType == null) {
			messageType = "Error";
		}
		UI_Common.showCommonError (messageType,errorMessage);	
	}
	//msgTypes = //Error //Warning //Confirmation
	UI_Common.showCommonError = function (msgType,msg) {
		top[2].objMsg.MessageText = msg ;
		top[2].objMsg.MessageType = msgType; 
		top[2].ShowPageMessage();
		return true;
	}

	UI_Common.buildError =  function(strMessage){
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMessage = strMessage.replace("#" + (i+1), arguments[i+1]);
			}
			strMessage = strMessage
		}
		return strMessage;
	}

	UI_Common.initilizeMessage = function (){
		top[2].HidePageMessage();
	}

	UI_Common.hideERRMessage = function () {
		top[2].HidePageMessage();
	}

	UI_Common.loadCheck = function (){
		return true;
	}

	// ------------------ end of Common Functions