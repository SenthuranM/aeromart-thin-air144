$.fn.fillDropDown = function(options) {
	try {
		var optsTxt = [];
		var c = 0;
		if(options.firstEmpty) {
			//$(this).append('<option value="">&nbsp;</option');
			optsTxt[c++] = '<option value="">&nbsp;</option>';
		}

		if(options.dataArray != null) {
			for(var i=0; i< options.dataArray.length; i++) {
				//$(this).append('<option value="' + options.dataArray[i][options.keyIndex] + '">' + options.dataArray[i][options.valueIndex] + '</option>');
				optsTxt[c++] = '<option value="';
				optsTxt[c++] = options.dataArray[i][options.keyIndex];
				optsTxt[c++] = '">';
				optsTxt[c++] = options.dataArray[i][options.valueIndex] + " ("+ options.dataArray[i][options.keyIndex] + ")";
				optsTxt[c++] = '</option>';
					
			}
		}
		if(optsTxt.length >0){
			$(this).append(optsTxt.join(''));
		}
	} catch (ex) {
		alert("Error filling dropdown box");
	}
}

exchageRatesCalc = {};
exchageRatesCalc.response = {};
exchageRatesCalc.baseCurrency = opener.strDefCurr;
exchageRatesCalc.ready = function(){
	$("#btnSwap").hide();//disable a swapping feature
	$("#selFromCurrency, #selToCurrency").fillDropDown({ dataArray:opener.arrCurrency , keyIndex:0 , valueIndex:1 });
	$("#btnCalc").click(function(){exchageRatesCalc.calculate();});
	$("#btnSwap").click(function(){exchageRatesCalc.swapCalc();});
	$("#btnSwap").disable();
	$("#chkRoundDecimal").click(function(){exchageRatesCalc.showResults();});
	//$("#selFromCurrency, #selToCurrency").change(function(){
	//	$("#btnSwap").disable();
	//});
	$("#selFromCurrency, #selToCurrency").val(exchageRatesCalc.baseCurrency);
	//$("#selFromCurrency").disable();
	$("#txtAmount").numeric({allowDecimal:true});
};

exchageRatesCalc.swapCalc = function(){
	$("#selFromCurrency").val(exchageRatesCalc.response.toCurrency);
	$("#selToCurrency").val(exchageRatesCalc.response.fromCurrency);
	$("#txtAmount").val(exchageRatesCalc.response.amount);
	exchageRatesCalc.calculate();
};

exchageRatesCalc.calculate = function(){
	if(exchageRatesCalc.isValidSelection()){
		exchageRatesCalc.clearResults();
		var url = "extRateConvertor.action";
		var data = {};
		data.amount = $.trim($("#txtAmount").val());
		data.fromCurrency = $.trim($("#selFromCurrency").val());
		data.toCurrency = $.trim($("#selToCurrency").val());
		$.ajax({type: "POST", dataType: 'json',data: data , url:url, 	
		success:exchageRatesCalc.processResults, error:exchageRatesCalc.errorLoading, cache : false});
		$("#btnSwap").enable();
	} 
	return false;
};

exchageRatesCalc.isValidSelection = function(){
	var boolValied = false;
	var amt = $.trim($("#txtAmount").val());
	if (amt == ""){
		alert("Amount cannot be blank");
	}else if(amt == "."){
		alert("Please Enter correct value for the amount field");
	}else if($.trim($("#selFromCurrency").val()) == $.trim($("#selToCurrency").val())){
		alert("Please specify different currencies!");
	}else if($.trim($("#selFromCurrency").val()) != exchageRatesCalc.baseCurrency && $.trim($("#selToCurrency").val()) != exchageRatesCalc.baseCurrency){ 
		 alert("Conversion only allowed from/to from "+ exchageRatesCalc.baseCurrency);
	}else if(amt.indexOf(".") > -1 ){
		var t = amt.split(".");
		if(amt.charAt(amt.length-1) == "."){
			alert("Please Enter correct value for the amount field");
		}else if(t.length > 2){
			alert("Please Enter correct value for the amount field");
		}else{
			boolValied = true;
		}
	}else{
		boolValied = true;
	}
	return boolValied;
	
	
	 if($.trim($("#selFromCurrency").val()) == $.trim($("#selToCurrency").val())){
		 alert("Please specify different currencies!");
		 return false;
	 }else if($.trim($("#selFromCurrency").val()) != exchageRatesCalc.baseCurrency && $.trim($("#selToCurrency").val()) != exchageRatesCalc.baseCurrency){ 
		 alert("Conversion only allowed from/to from "+ exchageRatesCalc.baseCurrency);
		 return false;
	 }
	 return true;
}

exchageRatesCalc.processResults = function(response){
	if (response.success && response.messageTxt == ""){
		exchageRatesCalc.response.exchageRateWithRound = response.exchageRateWithRound;
		exchageRatesCalc.response.exchageRateWithOutRound = response.exchageRateWithOutRound;
		exchageRatesCalc.response.convetedAmountWithRuond = response.convetedAmountWithRuond;
		exchageRatesCalc.response.convetedAmountWithOutRuond = response.convetedAmountWithOutRuond;
		exchageRatesCalc.response.fromCurrency = $.trim($("#selFromCurrency").val());
		exchageRatesCalc.response.toCurrency = $.trim($("#selToCurrency").val());
		exchageRatesCalc.response.amount = $.trim($("#txtAmount").val());
		exchageRatesCalc.response.valuesFilled = true; 
		exchageRatesCalc.showResults();
	}else{
		alert(response.messageTxt)
	}
};

exchageRatesCalc.clearResults = function(){
	$("#resultAmt1").html("");
	$("#resultAmt").html("");
};


exchageRatesCalc.showResults = function(){
	if ($("#chkRoundDecimal").attr("checked")){
		if (exchageRatesCalc.response.valuesFilled){
			var rateString = "1 " + $.trim($("#selFromCurrency").val()) + " = " + exchageRatesCalc.response.exchageRateWithRound + " " + $.trim($("#selToCurrency").val());
			$("#resultAmt1").html(rateString);
			$("#resultAmt").html(exchageRatesCalc.response.convetedAmountWithRuond);
		}
	}else{
		if (exchageRatesCalc.response.valuesFilled){
			var rateString = "1 " + $.trim($("#selFromCurrency").val()) + " = " + exchageRatesCalc.response.exchageRateWithOutRound + " " + $.trim($("#selToCurrency").val());
			$("#resultAmt1").html(rateString);
			$("#resultAmt").html(exchageRatesCalc.response.convetedAmountWithOutRuond);
		}
	}
};

exchageRatesCalc.errorLoading = function (response){
	alert(response);
};