(function($) {
dashbordToolBar = {};
dashbordToolBar.disply = top.dbToolDispay;
isDbAnciViewAllowed = top.dbAnciDispay;

dashbordToolBar.displayOn = "V"; // V = vertical or H = horizontal
dashbordToolBar.displyMessage = "";
dashbordToolBar.animDuration = 500;
dashbordToolBar.newMessage = false;
dashbordToolBar.messagesRecheckEnable = top.msgSyncEnable;
dashbordToolBar.messagesRecheckTime = top.msgSyncTime;
dashbordToolBar.init = function(){
	var container = $("<div></div>").attr({
		"id":"db_Container",
		"style":"display:none"
	});
	var db_headerImg = "";
	var db_height = 0;
	var db_width = 0;
	var db_height_h = 0;
	var db_width_h = 0;
	var db_float = "none";
	var db_top = 0;
	var db_margin = "0px 10px";
	var db_h_padding = "4px 7px";
	var db_left = $(window).height();
	var image_header1 = new Image();
	var image_header2 = new Image();
	var image_header3 = new Image();
	var image_header4 = new Image();
	var image_header5 = new Image();
	//dashbordToolBar.displyMessage = (top.dashboardContent == undefined)?"":top.dashboardContent;
	image_header1.src = "../images/dashboard/slide-button-left_no_cache.gif";
	image_header2.src = "../images/dashboard/slide-button-left-active_no_cache.gif";
	image_header3.src = "../images/messageico1_no_cache.gif";
	image_header4.src = "../images/messageico_no_cache.gif";
	image_header5.src = "../images/dashboardanci_no_cache.png";
	
	if (dashbordToolBar.displayOn.toUpperCase() == "V"){
		db_height = 470;
		db_height_h = 450;
		db_width = 303;
		db_width_h = 28;
		db_top = ($(window).height()/2) - (db_height/2);
		db_left = 0;
		db_float = "right";
		container.addClass("ui-corner-right");
		db_margin = "0px 0px";
		db_h_padding = "7px 0px"
	}else{
		db_height = 60;
		db_height_h = 23;
		db_width = 500;
		db_width_h = 500;
		db_top = $(window).height() - db_height_h;
		db_left = ($(window).width()/2) - (db_width/2);
		db_float = "none";
		container.addClass("ui-corner-top");
		db_margin = "0px 0px";
		db_h_padding = "4px 7px"
	}
	container.css({
		"position":"absolute",
		"top":db_top,
		"left":db_left
	});
	var _headerDiv = $("<a></a>").attr({
		"class":"db-header",
		"id":"sideBarTab",
		"href":"#"
	});
	var imgsObjWrapper = $("<div></div>").css({
		"background":"url('../images/dashboard/sidebutton-bgt_no_cache.gif') no-repeat scroll 0 100%",
		"padding-bottom":"3px"
	})
	var imgsObj = $("<img id='clicker' title='Dashboard' alt='Dashboard' src='"+image_header1.src+"'>");
	imgsObjWrapper.append(imgsObj);
	var imgsObjMsg = $("<img title='New Msssage' alt='New Msssage' src='"+image_header3.src+"' id='newMsgBD'>");
	imgsObjMsg.hide();
	imgsObjWrapper.append(imgsObjMsg);
	_headerDiv.append(imgsObjWrapper);
	_headerDiv.css({
		"width":db_width_h
	});
	_headerDiv.click(function(){
		if ($(this).prev().css("display")  == "none"){
			$(this).prev().show();
			$("#sideBarContentsInner").hide();
			$("#sideBarTab").find("img#clicker").attr("src",image_header2.src);
			if (dashbordToolBar.displayOn.toUpperCase() == "V"){
				$(this).prev().animate({width: db_width, height:db_height},{duration:dashbordToolBar.animDuration,
					complete:function(){
						$("#sideBarContentsInner").show();
					}});
			}else{
				$(this).parent().animate({top: (db_top - db_height)},{duration:dashbordToolBar.animDuration});
			}
			if (dashbordToolBar.newMessage){
				$("#tabMsgId").attr("src",image_header3.src);
				$("#newMsgBD").hide();
			}
			if (dashbordToolBar.newMessage){
				dashbordToolBar.newMessage = false;
				$("#tabMsgId").attr("src",image_header4.src);
				//Call to action to read new messages
				dashbordToolBar.readNewMessage();
			}
		}else{
			var t = $(this);
			$("#sideBarTab").find("img#clicker").attr("src",image_header1.src);
			$("#sideBarContentsInner").hide();
			if (dashbordToolBar.displayOn.toUpperCase() == "V"){
				t.prev().animate({width: 0, height:150},{duration:dashbordToolBar.animDuration,
				 complete: function() {										 
					 t.prev().hide();
				 }});
			}else{
				t.parent().animate({top: (db_top)},{duration:dashbordToolBar.animDuration,
				 complete: function() {										 
					 t.prev().hide();
				 }});
			}
			if (dashbordToolBar.newMessage){
				$("#newMsgBD").show();
			}
			if ($( "#tabsDB" ).tabs( "option", "active" ) == 1 && dashbordToolBar.newMessage){
				$("#newMsgBD").hide();
				dashbordToolBar.newMessage = false;
				$("#tabMsgId").attr("src",image_header4.src);
				//Call to action to read new messages
				dashbordToolBar.readNewMessage();
			}
		}
	});
	var _bodyDiv = $("<div>&nbsp;</div>").attr({
		"class":"db-body",
		"id":"sideBar"
	});
	function getSelectionText() {
	    var text = "";
	    if (window.getSelection) {
	        text = window.getSelection().toString();
	    } else if (document.selection && document.selection.type != "Control") {
	        text = document.selection.createRange().text;
	    }
	    return parseFloat(text);
	}
	var objList = top.db_Items_Collection;
	var _divInner = $("<div></div>").attr({"id":"sideBarContentsInner"});
	_divInner.css({
		"width":"auto",
		"padding": "0px 5px"
	});
	
	var _divTab = $("<div></div>").attr({
		"id":"tabsDB"
	});
	var imageUrlStringAnci = "../themes/default/images/accelaero"
	var ulTabs = $("<ul></ul>");
	var liToolBar = $("<li id='tabTools'><a href='#spnToolbar' title='Tools'><img src='../images/toolsico_no_cache.png' alt='Messages' align='absmiddle' border='0'> &nbsp; Tools</a></li>").css("display","none");
	var liMessage = $("<li id='tabMsg'><a href='#spnDashboard' title='Messages'><img id='tabMsgId' src='"+image_header4.src+"' alt='Messages' align='absmiddle' border='0'> &nbsp; Messages</a></li>");
	var liAnci = $("<li id='tabAnci'><a href='#spnAnci' title='Ancillary'><img id='tabAnciId' src='"+image_header5.src+"' alt='Ancillary' align='absmiddle' border='0'> &nbsp; Ancillary</a></li>");
	ulTabs.append(liToolBar, liMessage, liAnci);
	var _divTabToobar = $("<div></div>").attr({
		"id":"spnToolbar"
	});
	_divTabToobar.hide();
	var _divMessage = $("<div></div>").attr({
		"id":"spnDashboard"
	});
	
	var _divAnci = $("<div></div>").attr({
		"id":"spnAnci"
	});
	_divAnci.hide();
	_divTab.append(ulTabs, _divTabToobar, _divMessage,_divAnci);

	$.each(objList, function(i, obj){
		_divTabToobar.append(dashbordToolBar.addApp(obj));
	});
	
	if (top.db_Clocks.dbtoolbar){
		_divTabToobar.append(dashbordToolBar.addClocks(top.db_Clocks));
	}
	
	$.each(top.db_Anci_Collection, function(i, obj){
		_divAnci.append(dashbordToolBar.addApp(obj));
	});
	
	_divInner.append(_divTab);
	_bodyDiv.append( _divInner);
	_bodyDiv.css({
		"height":150,
		"width":1,
		"display":"none",
		"padding": db_margin,
		"float":"left",
		"position":"relative",
		"top":0,
		"left":0,
		"backgroundPosition":"left top",
		"overflow":"hidden"
	});
	container.append( _bodyDiv, _headerDiv);
	$($("body").get(0)).append(container);
	$("#tabsDB").tabs({
		active: 1,
		tabsbeforeactivate: function(event, ui) {
			if (ui.newTab.index()==1){
				if (dashbordToolBar.newMessage){
					dashbordToolBar.newMessage = false;
					$("#tabMsgId").attr("src",image_header4.src);
					//Call to action to read new messages
					dashbordToolBar.readNewMessage();
				}
			}
		}
	});
	if (top.db_Clocks.dbtoolbar){
		dashbordToolBar.applyClockPlugin(top.db_Clocks);
	}
	if (dashbordToolBar.disply){
		liToolBar.show();
		_divTabToobar.show();
		$("#tabsDB").tabs("option", "active", 0);
	}
	if(isDbAnciViewAllowed){
		liAnci.show();
		_divAnci.show();
	}
	//check dashboard message & setTimer for check Messages if enable
	dashbordToolBar.messagesRecheck();
};

dashbordToolBar.readNewMessage = function(){
		var url = "loadDBMessages!updateDBMessageExistStatus.action";
		var data = {};
		$.ajax({type: "POST", dataType: 'json',data: data , url:url, 	
		cache : false});
};

dashbordToolBar.addMessges = function(){
	$("#sideBarMsg").remove();
	if (dashbordToolBar.displyMessage != ""){
		var divBock = $("<div></div>").attr("id", "sideBarMsg");
		divBock.attr("style", "height:355px;overflow-y:auto");
		var h2Msg = $("<h2>AccelAero Dashboard Messages</h2>");
		divBock.append(h2Msg);
		$.each(dashbordToolBar.displyMessage, function(i,msg){
			var h3Msg = $("<h3>"+msg.messageTypeDesc+"</h3>")
			var strUL = $("<ul></ul>");
			var t = i+1;
			var li = $("<li> &nbsp;"+ t + ". "+ msg.subject+ "</li>");
			li.click(function(){
				
			});
			strUL.append(li);
			divBock.append(h3Msg, strUL);
		});
		$("#spnDashboard").append(divBock);
		$("#tabMsg").show();
		if (dashbordToolBar.newMessage && ($("#sideBar").css("display") == "none") ){
			$("#newMsgBD").show();
		}
	}else{
		$("#tabMsg").hide();
		$("#tabsDB").tabs( "option", "active", 0 );
	}
};

dashbordToolBar.messagesRecheck = function(){
	recheckCallError = function(response){
		alert(response);
		$("#tabMsg").hide();
		$("#spnDashboard").hide();
	}
	recheckCallSucess = function(response){
		if (response.success && response.messageTxt == ""){
			dashbordToolBar.displyMessage = (response.dashboardMessageList == null || response.dashboardMessageList.length == 0)?"":response.dashboardMessageList;
			dashbordToolBar.newMessage = response.unread;
			dashbordToolBar.addMessges();
			
		}
		//final check whether the dashboard toolbar to display
		if (dashbordToolBar.disply || isDbAnciViewAllowed || response.dashboardMessageList.length > 0){
			$("#db_Container").show();
			
			if (dashbordToolBar.disply){
				$("#tabTools").show();
				$("#spnToolbar").show();
			} else {				
				$("#tabTools").hide();
				$("#spnToolbar").hide();
			}
			if(isDbAnciViewAllowed){
				$("#tabAnci").show();
				$("#spnAnci").show();
			} else {
				$("#tabAnci").hide();
				$("#spnAnci").hide();
			}			
			//If new messages are present then automatically display the message tab and dashboard.
			if(dashbordToolBar.newMessage){
				$("#sideBarTab").click();
				$("#ui-id-2").click();
			}
		
		}else{
			$("#db_Container").hide();
		}
	}
	recheckCall = function(){
		var url = "loadDBMessages.action";
		var data = {};
		$.ajax({type: "POST", dataType: 'json',data: data , url:url, 	
		success:recheckCallSucess, error:recheckCallError, cache : false});
	}
	recheckCall();
	if (dashbordToolBar.messagesRecheckEnable){
		var t = setInterval(recheckCall, (dashbordToolBar.messagesRecheckTime * 60 * 1000));
	}
};

dashbordToolBar.addApp = function(obj){
	var imgObj = $("<img/>").attr({
		"src" : obj.imgPath,
		"border":0,
		"alt":"",
		"align":"absmiddle"
	});
	imgObj.css({"margin":"2px 5px","width":28});
	var divObj = $("<div></div>").attr({
		"class":"appItemDIV"
	});
	divObj.css({
		"margin":"3px 10px"
	})
	var labelObj = $("<label>"+obj.title+"</label>").attr({
		"class":"appItemLabel",
		"align":"center"
	});
	divObj.append(imgObj,labelObj);
	var wrapObj = $("<a></a>").attr({
		"href":"javascript:void(0)",
		"class":"appItem",
		"title":obj.title,
		"id":obj.id
	});
	
	if (dashbordToolBar.displayOn.toUpperCase() == "V"){
		wrapObj.css({"outline":"none"});
	}else{
		wrapObj.css({"float":"left", "margin":2,"outline":"none"});
	}
	wrapObj.bind("click", obj.listner);
	wrapObj.append(divObj);
	return wrapObj;
};

dashbordToolBar.applyClockPlugin = function(clkObj){
	getITDecimal = function(t){
		var tsone = t;
		var tb = [];
		if ( t.indexOf(":")>-1 ){
			tb = t.split(":");
			tb[1] = Number(tb[1])*100/60
			tsone = tb[0]+"."+tb[1];
		}
		return tsone
	};
	$.each(clkObj.zoneclock, function(i, obj){
		var zone = obj.tzone;
		var reqTimeZone = "UTC+5"
		if ( obj.sumapplied ){
			reqTimeZone = zone.split("|")[2];
		}else{
			reqTimeZone = zone.split("|")[1];
		}
		if (reqTimeZone != "-"){
			reqTimeZone = reqTimeZone.replace("UTC","");
			reqTimeZone = getITDecimal(reqTimeZone);
		}
		$('#lblClk'+i).find("label").text(zone.split("|")[0]);
		if ($.browser.msie){
			clkObj.clocktype = 'digital';
		}
		$('#clockAD'+i).clock({"offset": reqTimeZone, "type": clkObj.clocktype});
	});
};

dashbordToolBar.addClocks = function(clkObj){
	createClickContect = function(id){
		createLi = function(c){
			var li = $("<li></li>").attr({
				"class":c
			})
			return li;
		};
		var ul = $("<ul></ul>").attr({
			"id":"clockAD"+id,
			"class": clkObj.clocktype
		});
		var ulWrap = $("<div></div>").attr({
			"class" : "wrap"+clkObj.clocktype
		});
		var clkLabel = $("<div><label></label><div>").attr({
			"id":"lblClk"+id,
			"class":"clkLabel"
		});
		ulWrap.append(clkLabel , ul.append(createLi("hour"), createLi("min"), createLi("sec"), createLi("meridiem")) );
		return ulWrap;
	};
	
	var str = $("<div style='padding:2px 0px'> <label class='appItemLabel'>&nbsp;Clocks </label></div>");
	var img = $("<img/>").attr({
		 "src":"../images/settings_no_cache.png",
		 "id":"wc",
		 "align": "absmiddle",
		 "height":"20",
		 "width":"20"
	 	});
	img.bind("click", clkObj.listner);
	str.append(img);
	var innerD = $("<div></div>").attr({"class":"allClocks"});
	if ($.browser.msie){
		clkObj.clocktype = 'digital';
	}
	$.each(clkObj.zoneclock, function(i, obj){
		innerD.append(createClickContect(i));
	});
	innerD.append($("<div style='clear:both;height:1px'></div>"));
	
	str.append(innerD);
	return str;
}
})(jQuery);