function UI_dashboardBaggage(){}


UI_dashboardBaggage.fromCombo = null;
UI_dashboardBaggage.toCombo = null;
UI_dashboardBaggage.ondDataMap = [];
UI_dashboardBaggage.originList = [];

UI_dashboardBaggage.surfaceSegmentConfigs = {
		sufaceSegmentAppendStr : "-",
		surfSegCodeSplitter : ":",
		groundStationPrimaryIdentifier : "!"
	};

UI_dashboardBaggage.ready = function() {

	$('#departureDateBaggage').datepicker({
		minDate : new Date(),
		dateFormat: 'dd/mm/yy'
	});
	$("#departureDateBaggage").blur(function() { dateChk('departureDateBaggage');});
	
	if(isOndJsEnabled && UI_dashboardBaggage.ondListLoaded()){
		UI_dashboardBaggage.initDynamicOndList();
	}
	
	$("#fAirport").change(function() {
		UI_dashboardBaggage.originChanged();
	});
	$("#tAirport").change(function() {
		UI_dashboardBaggage.originDestinationChanged();
	});	
	UI_dashboardBaggage.fillDropDownData();
	
	$("#btnSearchBaggage").click(function() {
		UI_dashboardBaggage.searchBaggageOnClick();
		});
	
};

UI_dashboardBaggage.fillDropDownData = function(){

	$("#selSearchOptions").fillDropDown( {
		dataArray : arrSearchOptions , keyIndex:0 , valueIndex:1
		});
	
	$("#classOfServiceBaggage").fillDropDown( { dataArray:top.arrLogicalCC , keyIndex:0 , valueIndex:1 });
	
	var toData = arrAirportsV2;
	var fromData = arrAirportsV2;
	
		var fromStore = new Ext.data.ArrayStore({
		    fields: ['code', 'x', 'name'],
		    data : fromData
		});
	 
		var toStore = new Ext.data.ArrayStore({
		    fields: ['code', 'x', 'name'],
		    data : toData
		}); 

		UI_dashboardBaggage.fromCombo = new Ext.form.ComboBox({
		    store: fromStore,
		    id: 'extFromAirport',
		    displayField:'name',
		    typeAhead: false,
		    mode: 'local',
		    triggerAction: 'all',
		    emptyText:'Please select a value',
		    selectOnFocus:true,
		    applyTo: 'fAirport',
	 	    anyMatch: true,
	        caseSensitive: false,
	        forceSelection: true,
	        valueField:'code',
	        hiddenName:'searchParams.fromAirport',
	        hiddenId:'fromAirport',
	        listeners: {
	            'blur': function(f,new_val,oldVal) {
					UI_dashboardBaggage.onExtComboOnChange(f,new_val,oldVal)
	            }
	        }
		});
		
		UI_dashboardBaggage.toCombo = new Ext.form.ComboBox({
		    store: toStore,
		    id: 'extToAirport',
		    displayField:'name',
		    typeAhead: false,
		    mode: 'local',
		    triggerAction: 'all',
		    emptyText:'Please select a value',
		    selectOnFocus:true,
		    applyTo: 'tAirport',
	 	    anyMatch: true,
	        caseSensitive: false,
	        valueField:'code',
	        hiddenName:'searchParams.toAirport',
	        hiddenId:'toAirport',
	        listeners: {
	            'blur': function(f,new_val,oldVal) {
					UI_dashboardBaggage.onExtComboOnChange(f,new_val,oldVal)
	            }
	        }
		});
					
		
	UI_dashboardBaggage.toCombo.on('change', function(){
		UI_dashboardBaggage.originDestinationChanged();
	});

	UI_dashboardBaggage.fromCombo.on('change', function(){
		UI_dashboardBaggage.originChanged();
	});
}

UI_dashboardBaggage.originChanged = function(){
	var from = UI_dashboardBaggage.fromCombo.getValue(); 
	
	var toStore = null;
	if(from != '' &&
			UI_dashboardBaggage.ondDataMap[from]){	
		
		var toData = UI_dashboardBaggage.ondDataMap[from];		
		toStore = new Ext.data.ArrayStore({
			fields: ['code', 'x', 'name'],
			data : toData
		}); 			
	} else {
		
		var toData = arrAirportsV2;
		toStore = new Ext.data.ArrayStore({
			fields: ['code', 'x', 'name'],
			data : toData
		});
	}
	Ext.getCmp("extToAirport").bindStore(toStore);

	var to = UI_dashboardBaggage.toCombo.getValue(); 		
	if(from == '' || to == '') return;
	
	if(!$('#selSearchOptions').is(':disabled')){			
		UI_dashboardBaggage.setDefaultSearchOption(from, to);
	}
}

UI_dashboardBaggage.originDestinationChanged = function(){
	var from = UI_dashboardBaggage.fromCombo.getValue(); 
	var to = UI_dashboardBaggage.toCombo.getValue(); 

	if(from == '' || to == '') return;
	
	if(!$('#selSearchOptions').is(':disabled')){			
		UI_dashboardBaggage.setDefaultSearchOption(from, to);
	}
}


UI_dashboardBaggage.ondListLoaded = function(){
	if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
		return false;
	} else {
		return true;
	}
}

UI_dashboardBaggage.initDynamicOndList = function(){
	 	
	for(var originIndex=0;originIndex<origins.length;originIndex++){	
		if(typeof(origins[originIndex]) != "undefined"){
			var tempDestArray = [];
			for (var destIndex=0;destIndex<origins[originIndex].length;destIndex++) {
				// check whether operating carrier exists
				var curDest = origins[originIndex][destIndex];
			
				if(curDest[0] != null){					
					var currentSize = tempDestArray.length;
					tempDestArray[currentSize] = [];
					tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
					tempDestArray[currentSize][1] = airports[curDest[0]][defaultLanguage];
					tempDestArray[currentSize][2] = airports[curDest[0]]['code'] + ' - ' + airports[curDest[0]]['en'];						
				}
			
			}
		
			if (tempDestArray.length > 0) {				
				tempDestArray.sort(UI_dashboardBaggage.customSort);
				UI_dashboardBaggage.ondDataMap[airports[originIndex]['code']] = tempDestArray;
			}
		}
	}
};

UI_dashboardBaggage.customSort = function(a,b) {

	a = a[0];
	b = b[0];
	return a == b ? 0 : (a < b ? -1 : 1);

};

UI_dashboardBaggage.onExtComboOnChange = function (combo,record,index){
	var newVal = combo.getValue();
	if(UI_dashboardBaggage.isGroundSegmentAirport(newVal)){			
		// combo.reset();
		// combo.select();
	}
	if (!UI_dashboardBaggage.isAvailableAnList(combo, newVal)){
		combo.reset();
		combo.select();
	}
}

UI_dashboardBaggage.isGroundSegmentAirport = function (airportCode){
	var identifier = UI_dashboardBaggage.surfaceSegmentConfigs.groundStationPrimaryIdentifier;
	if(airportCode != null ){
		if( $.trim(airportCode).indexOf(identifier) != 0){
			return false;
		}
		return true;
	}
}

UI_dashboardBaggage.isAvailableAnList = function (combo, val){
	var dataObj = combo.store.data.items;
	for (var i = 0; i < dataObj.length; i++){
		if (val == dataObj[i].data.code){
			return true;
			break;
		}
	}
	return false;
}

UI_dashboardBaggage.setDefaultSearchOption = function (from, to){
	
	if(isOndJsEnabled
			&& UI_dashboardBaggage.ondListLoaded()){			
		var fromIndex = -1;
		var toIndex = -1;
		// get from, to indexes
		var airportsLength = airports.length;
		for(var i=0;i<airportsLength;i++){
			if(from == airports[i].code){
				fromIndex = i;
			} else if(to == airports[i].code) {
				toIndex = i;
			}
			
			if(fromIndex != -1 && toIndex != -1){
				break;
			}
		}
		
		if(fromIndex != -1){			
			var availDestOptLength = origins[fromIndex].length;
			for(var i=0;i<availDestOptLength;i++){
				var destOptArr = origins[fromIndex][i];
				if(destOptArr[0] == toIndex){
					var isInt = false;
					var optCarriersArr = destOptArr[3].split(",");
					for(var j=0;j<optCarriersArr.length;j++){
						if(optCarriersArr[j] != carrierCode){
							isInt = true;
							break;
						}
					}
					
					if(isInt){
						$('#selSearchOptions').val('INT');
					} else {
						$('#selSearchOptions').val('AA');
					}
				}
			}
		} else {			
			$('#selSearchOptions').val('AA');
		}
	} else {
		 if ((airportOwnerList[from] == 'OWN') && (airportOwnerList[to] == 'OWN')) {
			$('#selSearchOptions').val('AA');
		} else if ((airportOwnerList[from] == 'OWN') || (airportOwnerList[to] == 'OWN')) {
			$('#selSearchOptions').val('INT');
		} else {
			$('#selSearchOptions').val('INT');
		}
	}
}

UI_dashboardBaggage.searchBaggageOnClick = function() {
	
	$("#baggageGrid").empty();
	if(UI_dashboardBaggage.validateData()){
		var url = "dashboardBaggageAvailability.action";
		var data = {};
		data.origin = $.trim($("#fromAirport").val());
		data.destination = $.trim($("#toAirport").val());
		data.searchOption = $.trim($("#selSearchOptions").val());
		data.departureDate = $.trim($("#departureDateBaggage").val());
		
		var cos = $("#classOfServiceBaggage").val();
		var cosArr = cos.split("-");
		if(cosArr.length > 1){
			data.logicalCabinClass = cosArr[0];
			data.classOfService = cosArr[1];
		} else if(cosArr.length == 1) {
			data.classOfService = cosArr[0];
			data.logicalCabinClass = '';
		}   
		
		UI_commonSystem.showProgress();
		$.ajax({type: "POST", dataType: 'json',data: data , url:url, 	
		success:UI_dashboardBaggage.searchBaggageOnSuccess, error:UI_dashboardBaggage.searchBaggageError, cache : false});
	}	
}

UI_dashboardBaggage.searchBaggageOnSuccess = function(baggageResponse) {
	
	$("#baggageGrid").empty();	
	if (baggageResponse.success == true){
		var gridHeadings = [];
	    var gridColumns = [];
	    gridHeadings = ['Baggage', 'Charge'];
	    gridColumns = [{
	        name: 'baggageDescription',
	        index: 'baggageDescription',
	        width: 350,
	        align: "left"
	    }, {
	        name: 'baggageCharge',
	        index: 'baggageCharge',
	        width: 130,
	        align: "center"
	    }];
	        
	    	$.each(baggageResponse.baggageDisplayResponse, function(flightNumber, baggages){
	    		var i = 0;
	    		var flightNumberDiv = $('<div>').attr( "id", "flightNumberDiv_" + i);
	    		var flightNumberLabel = $('<label>').attr( "id", "flightNumberLabel_" + i);
	    		flightNumberDiv.append(flightNumberLabel);
	    		flightNumberDiv.css('margin-top',15);
	    		flightNumberDiv.css('margin-bottom',07);
	    		flightNumberLabel.text(flightNumber);
	        	var baggageGrid = $('<table>').attr( "id", "baggageGrid_" + i);
	       
	        	
	        	$("#baggageGrid").append(flightNumberDiv);
	        	$("#baggageGrid").append(baggageGrid);
	        	baggageGrid.jqGrid({
	        	    datatype: "local",
	        	    colNames: gridHeadings,
	        	    colModel: gridColumns,
	        	    scrollrows: true,
	                cellsubmit: 'clientArray',
	                viewrecords: true,
	                height:'auto'
	        	});
	        	
	        	var gridData = baggages;
	        	$.each(gridData, function(i, rowData){
	        		baggageGrid.addRowData(i + 1, rowData);
	        	});
	        	
	        	i++;
	    	
	    	});	
	    	
	    	UI_commonSystem.hideProgress();
	
	} else {
		UI_commonSystem.hideProgress();
		showWindowCommonError("Error", baggageResponse.messageTxt);
	}	    
}



UI_dashboardBaggage.searchBaggageError = function() {
	UI_commonSystem.hideProgress();
	$("#baggageGrid").empty();
	 window.close();
}

UI_dashboardBaggage.validateData = function (){
		
	if ($("#departureDateBaggage").val() == "") {
		showWindowCommonError("Error", "Departure Date Can Not be Empty");
		return false;
		
	} else if($("#departureDateBaggage").val() == "" || ($("#departureDateBaggage").val().indexOf("/") == -1)){
		showWindowCommonError("Error","Wrong date format of Departure Date");
		return false;
		
	} else if (!CheckDates(UI_dashboardBaggage.currentDate(), $("#departureDateBaggage").val())){
		showWindowCommonError("Error","Entered date should not be less than " + UI_dashboardBaggage.currentDate());	
		return false;
		
	} else if ($("#toAirport").val() == "") {
		showWindowCommonError("Error", "Destination Can Not be Empty");
		return false;
		
	} else if ($("#fromAirport").val() == "") {
		showWindowCommonError("Error", "Origin Can Not be Empty");
		return false;
	} else {
		return true;
	}	
	
}

UI_dashboardBaggage.currentDate = function () {
	return window.opener.strSysDate;
}

UI_dashboardBaggage();
UI_dashboardBaggage.ready();
