worldsClock = {};
worldsClock.id = 0;
worldsClock.chagesDone = false;
worldsClock.sendData = {}; 
worldsClock.AMPM = opener.db_Clocks.useampm;
worldsClock.clockType = opener.db_Clocks.clocktype;
worldsClock.defaultZone = opener.db_Clocks.zoneclock;
worldsClock.dbtoolbarApplied = opener.db_Clocks.dbtoolbar;
worldsClock.ready = function(){
	function SortByZone(x,y) {
		return ((x.zone == y.zone) ? 0 : ((x.zone > y.zone) ? 1 : -1 ));
	}
	worldsClock.createTable();
	// Call Sort By Name
	time_zone.sort(SortByZone);
	worldsClock.setDefault(worldsClock.defaultZone);
	$("#btnAddClock").click(function(){
		if ($(".clockContainer").length < 4){
			worldsClock.chagesDone = true;
			worldsClock.createClock(worldsClock.defaultZone[0]);
			worldsClock.id ++;
		}else{
			alert("You have reached to the maximum limits of clocks in the system");
		}
	});
	if (worldsClock.dbtoolbarApplied){
		$("#chkAddtoDashBoard").prop("checked", true);
	}
	$("#chkAddtoDashBoard").click(function(){
		worldsClock.chagesDone = true;
		if ($("#chkAddtoDashBoard").prop("checked") == true){
			worldsClock.dbtoolbarApplied = true;
		}else{
			worldsClock.dbtoolbarApplied = false;
		}
	});
	$("#btnApplyClk").click(function(){worldsClock.saveApply()});
	$("#btnClose").click(function(){
		if (worldsClock.chagesDone){
			x = confirm("Changes will not been saved, Do you want to Continue?")
			if (x){
				window.close();
			}
		}else{
			window.close();
		}
			
	});
	$("#txtAmount").numeric({allowDecimal:true});
};

worldsClock.saveApply = function(){
	if (worldsClock.chagesDone){
		x = confirm("Do you want to save the changes?");
		if (x){
			var data = {};
			data.useampm = worldsClock.AMPM;
			data.clocktype = worldsClock.clockType;
			data.dbtoolbar = worldsClock.dbtoolbarApplied;
			data.zoneclock = worldsClock.collectAllClockInfo(false);
			worldsClock.sendData = data;
			var url = "clockSave.action";
			
			var data2 = {};
			data2.dashboardPref = $.toJSON(data); 
			$.ajax({type: "POST", dataType: 'json',data: data2 , url:url, 	
			success:worldsClock.processSaving, error:worldsClock.errorSaving, cache : false});
			return false;
		}
	}else{
		alert("No Changes were done to save");
	}
};

worldsClock.errorSaving = function(response){
	alert(response);
};

worldsClock.processSaving = function(response){
	if (response.success){
		alert("Change were saved sucessfully, Re-start the system to effect the changes");
		//update the top.db_Clocks object for instantly apply the changes 
		opener.updateClocks(worldsClock.sendData);
		worldsClock.chagesDone = false;
	}else{
		alert("Error while saving");
	}
};

worldsClock.setDefault = function(avaiClockList){
	$.each(avaiClockList, function(i, obj){
		worldsClock.createClock(obj);
		worldsClock.id ++;
	});
};

worldsClock.setTimeofZone = function(i){
	getITDecimal = function(t){
		var tsone = t;
		var tb = [];
		if ( t.indexOf(":")>-1 ){
			tb = t.split(":");
			tb[1] = Number(tb[1])*100/60
			tsone = tb[0]+"."+tb[1];
		}
		return tsone
	};
	
	var zone = $.data($("#clk_"+i)[0], "thiszone");
	if ( $("#chksum_"+i).prop("checked") && zone.split("|")[2] != "-"){
		var reqTimeZone = zone.split("|")[2];
	}else{
		var reqTimeZone = zone.split("|")[1];
	}
	var title = zone.split("|")[0];
	if (zone.split("|")[2] == "-"){
		$("#chksum_"+i).prop("checked", false);
		$("#chksum_"+i).prop("disabled", true);
	}else{
		$("#chksum_"+i).prop("disabled", false);
	}
	$("#clk_"+i).find(".dipTime").empty();
	$("#clk_"+i).find(".dipTime").append(worldsClock.createClickContect(i));
	if (reqTimeZone != "-"){
		reqTimeZone = reqTimeZone.replace("UTC","");
		reqTimeZone = getITDecimal(reqTimeZone);
		worldsClock.applyClockPlugin(i, reqTimeZone);
		$("#clk_"+i).find(".clockHeader").html(title+ " " + reqTimeZone);
	}else{
		$("#clk_"+i).find(".dipTime").html( "" );
		$("#chksum_"+i).attr("disabled", true);
	}
};

worldsClock.setClockType = function(obk){
	worldsClock.chagesDone = true;
	worldsClock.clockType = obk.value;
	worldsClock.id = 0;
	var avaiClockList = worldsClock.collectAllClockInfo(true);
	worldsClock.setDefault(avaiClockList)
};

worldsClock.collectAllClockInfo = function(remove){
	var allClockList = $(".clockContainer");
	var clockList = [];
	$.each(allClockList, function(i, obj){
		var clk = {};
		clk.tzone = $(obj).find("select").val();
		clk.sumapplied = $(obj).find("input[type='checkbox']").prop("checked");
		clockList.push(clk);
	});
	if (remove){allClockList.remove();}
	return clockList;
};

worldsClock.createTable = function(){
	var zoneTD = $("<th><label class='txtWhite'>Time Zone</label></th>").attr("width","30%");
	zoneTD.css({
		"height":25
	})
	var summarTD = $("<th><label class='txtWhite'>Apply Summer</label></th>").attr("width","25%");
	var timeTD = $("<th></th>").attr("width","40%");
	var lblDig = $("<label class='txtWhite'> Digital </label>");
	var radClkTypeD = $("<input type='radio'/>").attr({
		"name":"radClkType",
		"id":"radClkType_D",
		"value":"digital"
	});
	var lblAna = $("<label class='txtWhite'> Analog * </label>")
	var radClkTypeA = $("<input type='radio'/>").attr({
		"name":"radClkType",
		"value":"analog",
		"id":"radClkType_A",
		"disabled": ($.browser.msie)?true:false
	});
	radClkTypeD.click(function(){
		worldsClock.setClockType(this);
	});
	radClkTypeA.click(function(){
		worldsClock.setClockType(this);
	});
	timeTD.append(radClkTypeA, lblAna, radClkTypeD, lblDig);
	var blkTD = $("<th>&nbsp;</th>").attr("width","5%");
	var headerTR = $("<tr></tr>").attr("class","darkBG");
	headerTR.append(zoneTD, summarTD, timeTD, blkTD);
	var tbl = $("<table></table>").attr({
		"width":"100%",
		"border":"0",
		"cellpadding":3,
		"cellspacing":0,
		"id" : "clockTable"
	});
	tbl.append(headerTR);
	$("#clockSettings").append(tbl);
	if (worldsClock.clockType == "digital"){
		$("#radClkType_D").prop("checked", true);	
	}else{
		$("#radClkType_A").prop("checked", true);	
	}
};

worldsClock.createClock = function(CL){
	createZone = function(){
		var selZone = $("<select></select>").attr({
			"id":"selZone_" + worldsClock.id,
			"class":"aa-input"
			});
		//var opt_d = $("<option>&nbsp;</option>").attr({"value":"-"});
		//selZone.append(opt_d);
		$.each(time_zone, function(i, zone){
			var optval = zone.zone +"|"+ zone.nortime +"|"+zone.sumtime;
			var opt = $("<option>"+zone.zone+"</option>").attr({"value":optval})
			selZone.append(opt);
		});
		selZone.css({
			"width":135
		});
		selZone.change(function(){
			var i = this.id.split("_")[1];
			$.data($("#clk_"+i)[0], "thiszone", $(this).val());
			worldsClock.chagesDone = true;
			worldsClock.setTimeofZone(i);
		});
		var zoneWraper = $("<td></td>").attr({
			"class":"zoneArea thinBorderB thinBorderL thinBorderR",
			"align":"center"
		});
		zoneWraper.append(selZone);
		
		var chkSum = $("<input type='checkbox'/>").attr({
			"id":"chksum_"+worldsClock.id,
			"title":"Apply Summer",
			"checked": CL.sumapplied?true:false,
			"class":"aa-input"
		});
		chkSum.css("margin","0 4px");
		chkSum.click(function(){
			worldsClock.chagesDone = true;
			var i = this.id.split("_")[1];
			worldsClock.setTimeofZone(i);
		})
		var summerWraper = $("<td></td>").attr({
			"align": "center",
			"class"	: "thinBorderB  thinBorderR"
		});
		summerWraper.append(chkSum);
		var spnClockTime = $("<span></span>").attr({
			"class":"dipTime"
		});
		var timeWrapper = $("<td></td>").attr({"align":"center", "valign":"top", "class":"thinBorderB  thinBorderR"});
		var clockLbl = $("<div></div>").attr({
			"class":"clockHeader",
			"style":"background:#e4e4e4; color:red; padding:2px;font-size:14px"
		});
		timeWrapper.append(clockLbl,spnClockTime);
		var removeBtn = $("<input/>").attr({
			"id":"btnRem_"+worldsClock.id,
			"value" : " - ",
			"type" : "button",
			"style":"width:17px"
		});
		removeBtn.click(function(){
			var i = this.id.split("_")[1];
			if ( $("#clk_"+i).parent().children().length > 2){
				$("#clk_"+i).remove();
				worldsClock.chagesDone = true;
			}else{
				alert("Cannot remove last clock");
			}
		});
		var removeWrapper = $("<td></td>").attr({"align":"right", "class"	: "thinBorderB  thinBorderR"});
		removeWrapper.append(removeBtn);
		var container = $("<tr></tr>").attr({"class":"clockContainer", "id":"clk_"+worldsClock.id});
		container.append( zoneWraper, summerWraper, timeWrapper, removeWrapper );
		return container;
	};

	var tbl = $("#clockTable");
	tbl.append(createZone());
	jQuery.data($("#clk_"+worldsClock.id)[0], "thiszone", CL.tzone);
	$("#clk_"+worldsClock.id).find("select").val(CL.tzone);
	worldsClock.setTimeofZone(worldsClock.id);

};

worldsClock.applyClockPlugin = function(id, offSet){
	$('#clockAD'+id).clock({offset: offSet, type: worldsClock.clockType});
};


worldsClock.createClickContect = function(id){
	createLi = function(c){
		var li = $("<li></li>").attr({
			"class":c
		})
		return li;
	};
	var ul = $("<ul></ul>").attr({
		"id":"clockAD"+id,
		"class":worldsClock.clockType
	});
	var ulWrap = $("<div></div>");
	if (worldsClock.clockType != "analog"){ulWrap.css("height","30px");}
	ulWrap.append( ul.append(createLi("hour"), createLi("min"), createLi("sec"), createLi("meridiem")) );
	return ulWrap;
};


