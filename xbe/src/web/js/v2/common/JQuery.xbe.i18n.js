
function i18n_xbe(){}


$.fn.getLanguageForTitle = function() {
	
	$(this).find("img").each(function( intIndex ){
		var messageId = $(this).attr("i18n_key_title");
		
		if(messageId){
			var i18Message = geti18nDataMsg(messageId);
			
			if(i18Message){
				var attribute = 'title';
				$(this).attr(attribute,i18Message);
			}
			
		}

	});
}



$.fn.getLanguage = function() {
	
	$(this).find("label, span, div, a, font, button, td, option, p, b, input").each(function( intIndex ){
		var messageId = $(this).attr("i18n_key");
		
		if(messageId){
			var i18Message = geti18nDataMsg(messageId);
			
			if(i18Message != null){
				if ($(this).attr("type") == "button" && $(this).attr("name") != null){ 
					$(this).val(i18Message);
				}else{
					$(this).text(i18Message);
				}
				
			}
			
		}

	});
}



i18n_xbe.populateLabels = function (response){
	setLanguageStore(response.languageData);
	
}

var languageDataStore;

var setLanguageStore = function(responseData){
	languageDataStore = responseData;
}

var geti18nData = function(key, msg){

	if(typeof top.languageDataStore === "undefined"){
		return msg;
	}else if(key){
		return top.languageDataStore[key] || msg;
	}
	
 }

var getOpeneri18nData = function(key, msg){
	
	if(typeof opener.top.languageDataStore === "undefined"){
		return msg;
	}else if (key) {
		return opener.top.languageDataStore[key] || msg;
	}

}

var geti18nDataMsg = function(key){
	try {
		return top.languageDataStore[key];
	} catch (e) {
		console.log(e);
	}
	return null;
}



$.fn.getLanguageForOpener = function() {
	
	$(this).find("label, span, div, a, font, button, td, option, p, b, input").each(function( intIndex ){
		var messageId = $(this).attr("i18n_key");
		
		if(messageId){
			var i18Message = geti18nDataMsgOpener(messageId);
			
			if(i18Message != null){
				if ($(this).attr("type") == "button" && $(this).attr("name") != null){ 
					$(this).val(i18Message);
				}else{
					$(this).text(i18Message);
				}
				
			}
			
		}

	});
}


var geti18nDataMsgOpener = function(key) {
	try {
		return opener.top.languageDataStore[key];
	} catch (e) {
		console.log(e);
	}
	return null;
}









