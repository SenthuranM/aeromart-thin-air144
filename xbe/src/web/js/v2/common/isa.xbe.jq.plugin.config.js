$(function() {
	
	//setting common JQ Grid Properties
	$.extend($.jgrid.defaults,{
		loadui:'disable',
		mtype:'POST',
		onPaging:  function (e){
			if(Number(this.page) > Number(this.lastpage) ){
				UI_Common.displayMsgObj.message = "Record number should be less than the total number of records.";
				UI_Common.displayMsgObj.msgType = "Error";
				UI_Common.displayMessage(UI_Common.displayMsgObj);
				this.page = "1";				
				
			}
		},		
		loadError: function(requestObj, status, error){
			handleCommonAjaxInvocationErrors(requestObj);
		},
		beforeSelectRow: function (rowID,e){
			return true;
		},
		sortable: true,
		sortlocally: true,	
		//override beforeSelectRow if inline edit is enabled		
		onNoRecordsFunc: function (){
			UI_Common.displayMessage({message:"No records found.", msgType: "Error"});
		}

	});
	
	//setting up progress bar for ajax events
	$(document).ajaxSend(function(){		
		$("#divLoadMsg").show();
	});

	$(document).ajaxStop(function(){	
		$("#divLoadMsg").hide();
	});
	
	$(document).ajaxError(function(){	
		$("#divLoadMsg").hide();
	});
	
	$(document).ajaxSuccess(function(){	
		//top[0].$('.progressBar').hide();
	});	
	
});