	/*
	*********************************************************
		Description		: Show Error Message 
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created         : 3rd September 2009
		Last Modified	: 3rd September 2009
    ---------------------------------------------------------
    */
	
	function UI_message(){}
	UI_message.strErrImgPath = UI_commonSystem.strImagePathError;

	/*
	 * Initialize Message
	 */
	UI_message.initializeMessage = function(){
		//$("#tdSTBar").removeClass('errorStatsBar');
		//$("#tdSTBar").addClass('errorStatsBarDef');
		//$("#imgError").hide();
		$("#divError").html("");
	}
	
	/*
	 * Show Error message
	 */
	UI_message.showErrorMessage = function(p){
		//UI_message.showMessage();
		
		//$("#imgError").attr("src", UI_message.strErrImgPath + "Err_no_cache.gif");
		//$("#imgError").show();
		//$("#divError").html("<label style='color:red' class='txtDef txtBold'>&nbsp;" + p.messageText + "</label>");
		dispalyError(p.messageText,"Error");
	}
	
	/*
	 * Show Error message
	 */
	UI_message.showConfirmationMessage = function(p){
		//$("#tdSTBar").removeClass('errorStatsBarDef');
		//$("#tdSTBar").addClass('errorStatsBar');
		
		//$("#imgError").attr("src", UI_message.strErrImgPath + "Conf_no_cache.gif");
		//$("#imgError").show();
		//$("#divError").html("<label style='color:black' class='txtDef txtBold'>&nbsp;" + p.messageText + "</label>");
		dispalyError(p.messageText,"Confirm");
	}
	
	/*
	 * Show Error message
	 */
	UI_message.showWarningMessage = function(p){
		//$("#tdSTBar").removeClass('errorStatsBarDef');
		//$("#tdSTBar").addClass('errorStatsBar');
		
		//$("#imgError").attr("src", UI_message.strErrImgPath + "Warn_no_cache.gif");
		//$("#imgError").show();
		//$("#divError").html("<label style='color:black' class='txtDef txtBold'>&nbsp;" + p.messageText + "</label>");
		dispalyError(p.messageText,"Warning");
	}
	
	/*
	 * Hide Message 
	 */
	UI_message.hideMessage = function(){
		$("#tdSTBar").removeClass('errorStatsBar');
		$("#tdSTBar").addClass('errorStatsBarDef');
		$("#divError").hide();
	}
	
	/*
	 * Show Message 
	 */
	UI_message.showMessage = function(){
		$("#tdSTBar").removeClass('errorStatsBarDef');
		$("#tdSTBar").addClass('errorStatsBar');
		$("#divError").show();
	}
	/* ------------------------------------------------------- End of File ---------------------------------- */