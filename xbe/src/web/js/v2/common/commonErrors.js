$(document).ready(function() {
	
	$addErrorHTML = '<div id="errorMSGouter" style="display: none;">'+
		'<div class="errorBox">'+
				'<span id="errorMSG" style=""></span>'+
				'<span class="errorClose">&nbsp;</span>'+
			'</div>'+
		'</div>';
	$addPopupHTML ='<div id="errorPopup" style="display: none;">'+
			'<div class="errorPopupHeader">'+
				'<span class="text">Error Console</span>'+
				'<span class="errorClose">&nbsp;</span>'+
				'<div style="clear:both"></div>'+
			'</div>'+
			'<div class="errorPopupButton" style="">'+
				'<a href="javascript:void(0)" class="errorClear">Clear All</a>'+
			'</div>'+
				'<div class="errorPopupbody" style="">'+
					'<table border="0" width="100%" cellspacing="0" cellpadding="0">'+
						'<tr>'+
							'<td>'+
								'<div>'+
									'<table id="errorPopupTable" border="0" width="100%" cellspacing="0" cellpadding="4">'+
									'</table>'+
								'</div>'+
							'</td>'+
						'</tr>'+
					'</table>'+
				'</div>'+
		'</div>';
		
		
		var errorLog = [];
		addtoErrorLog = function(o){
			errorLog[errorLog.length] = o
			setTimeout("closeError()",7000);
			if (errorLog.length > 0){
				$("#spnErrorLog").show();
				$("#spnErrorLog").css({
					height:"18px",
					width:"22px",
					display: "block"
				});
			}
		};
		loadErrors = function(){
			var str ="";
			var errObj = errorLog; 
			$.each(errObj, function(i,k){
				str += "<tr>";
				str += "<td width='5%' class='errorBoder "+k.type+"' >&nbsp;</td>";
				str += "<td width='20%' class='errorBoder code'>"+k.code+"</td>";
				str += "<td width='75%' class='errorBoder'>"+k.discription+"</td>"
				str += "</tr>";
			});
			$("#errorPopup").find("#errorPopupTable").html(str);
		};
		$("#spnErrorLog").click(function(){
			$("body").append(addPopupHTML);
			$("#errorPopup").css({
				position:"absolute",
				top:"0px",
				right:"10px",
				zIndex:"3000",
				display:"block",
				width:"400px",
				height:"30px"
			});
			$("#errorPopup").animate({height:"251px"},{duration:300});
			$(".errorPopupHeader .errorClose").bind("click",function(){
				$(this).parent().parent().remove();
				if (errorLog == "")
					$("#spnErrorLog").hide();
			});
			$("#errorPopup .errorClear").bind("click",function(){
				$("#errorPopup").find("#errorPopupTable").html("");
				errorLog = [];
			});
			loadErrors();
		});

		closeError = function(){
			$("#errorMSGouter").fadeOut();
			$("#errorMSGouter").remove();
		};
		
		dispalyError = function (errorTxt,type){
			
			var obj = "";
			if (errorTxt.indexOf(":")>-1){
				var temp = errorTxt.split(":");
				obj = {"code":temp[0],"discription":temp[1],"type":type};
			}else{
				obj = {"code":"","discription":errorTxt,"type":type};
			}
			addtoErrorLog(obj);
			
			//alert(errorTxt + "" +$addErrorHTML)
			$("body").append($addErrorHTML);
			$(".errorClose").bind("click",function(){
				closeError();
			});
			$("#errorMSG").html(errorTxt);
			$("#errorMSG").addClass(type);
			$("#errorMSGouter").css({
				position:"absolute",
				top:"0px",
				zIndex:"1000",
				display:"block",
				width:"100%"
			});
			$(".errorBox").css({
				position:"relative",
				opacity:"0.9",
				left:"90px",
				width: $(window).width() - 200 +"px"
			});
		};
});