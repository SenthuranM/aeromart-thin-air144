$(document).ready(function(){

	$(function() {
	    $( "#txtFromDate" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      yearRange: "0:+99",
	      maxDate: "+99Y",
	      minDate: "0Y",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      changeMonth: true,
	      changeYear: true
	    });
	    
	    $( "#txtFromDate" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	    
	    $( "#txtToDate" ).datepicker({
		      showOn: "button",
		      buttonImage: "../images/Calendar_no_cache.gif",
		      yearRange: "0:+99",
		      maxDate: "+99Y",
		      minDate: "0Y",
		      buttonImageOnly: true,
		      buttonText: "Select date",
		      changeMonth: true,
		      changeYear: true
		    });
		    
		    $( "#txtToDate" ).datepicker( "option", "dateFormat", "dd/mm/yy" );	 
	
	});

	createGrid();
	
});

createGrid = function(){
	
	$("#palcalTimings").empty().append('<table id="palcalDetailsGrid"></table><div id="palcalDetailsPages"></div>')
	
	var statusFormatter = function(cellVal){
		if(cellVal =='ACT'){
			return 'Active';
		}else if(cellVal=='INA'){
			return 'In-Active';
		}
	};
	
	var fltNoFormatter = function(cellVal){
		
		if(cellVal !=null){
			return cellVal;
		}else{
			return 'ALL FLIGHTS'
		}
	};
	
	
	var data = {};
	data['airportCode'] = $("#selFrom").val();
	data['flightNo'] = $("#txtFlightNoSearch").val();
	
	var customerGrid = $("#palcalDetailsGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'showPalCalTimigs!searchPalCalTimings.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :ShowProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(customerGrid[0], "gridData", mydata.palCalTimingsList);
			            		  customerGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  HideProgress();
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames: ['Flight No','Airport', 'PAL Depature Gap', 'CAL Time Interval', 'Start Date', 'End Date','Last Cal Gap', 'Status', 'PalaCalId','Version', 'CAL Gap After Cut-Off'],
		    colModel: [
		               
				{name: 'flightNumber', index: 'flightNumber',jsonmap:'PalCalTimingDTO.flightNumber', width: 140, align: "center", sorttype: "string"},
		        {name: 'airportCode', index: 'airportCode',jsonmap:'PalCalTimingDTO.airportCode', width: 140, align: "center", sorttype: "string"},
				{name: 'palDepatureGap',index: 'palDepatureGap',jsonmap:'PalCalTimingDTO.palDepGap', width: 160, align: "center", sorttype: "string"},
				{name: 'calTimeInterval', index: 'calTimeInterval',jsonmap:'PalCalTimingDTO.calRepIntv' ,width: 140, align: "center"},
				{name: 'startDate',index: 'startDate', jsonmap:'PalCalTimingDTO.strStartingZuluDate', width: 100, align: "center"},
				{name: 'endDate',index: 'endDate', jsonmap:'PalCalTimingDTO.strEndingZuluDate', width: 120, align: "center"},
				{name: 'lstCalGap',index: 'lstCalGap', jsonmap:'PalCalTimingDTO.lastCalGap', width: 120, align: "center"},				
				{name: 'staus',index: 'staus', jsonmap:'PalCalTimingDTO.status', width: 120, align: "center", formatter:statusFormatter},
				{name: 'palCalId',index: 'palCalId', jsonmap:'PalCalTimingDTO.palCalTimingId', width: 120, hidden: true},
				{name: 'palCalVersion',index: 'palCalVersion', jsonmap:'PalCalTimingDTO.version', width: 120, hidden: true},
				{name: 'calTimeAfterCutoffTime',index: 'calTimeAfterCutoffTime', jsonmap:'PalCalTimingDTO.calRepIntvAfterCutoff', width: 120, hidden: true}				
			    
		    ],
		  
		    rowNum : 20,
			viewRecords : true,
			width : 920,
			pager: jQuery('#palcalDetailsPages'),
			jsonReader: { 
				root: "palCalTimingsList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid("#palcalDetailsPages",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$("#palcalDetailsGrid").click(function(){ gridOnClick(); });
}	

	function closeClick() {	
		top.LoadHome();		
	}


	function addClick() {
		disableInputSection(false);
		clearInputSection();
		disableButtons(false);
		setField("hdnMode","ADD");
		$('#chkStatus').prop('checked', true);  
		disableGridButtons(true);		
	}
	
	function editClick() {			
		disableInputSection(false);
		Disable("selAirport", true);
		disableButtons(false);
		setField("hdnMode","EDIT");			
	}

	function deleteClick(){
		if (isCancelConfirm()) {
			
			$.ajax({
			           url : 'showPalCalTimigs!deletePAlCalTiming.action',
			           data: getSaveOrUpdateData(),
			           dataType:"json",
			           beforeSend :ShowProgress(),
			           type : "POST",		           
			           success: loadSuccess,
			           error : errorFunction
			        });
			
		}
	}
	
	function isCancelConfirm() {
		 
		return confirm("This will Cancel The selected PAL CAL configuration! Do you wish to Continue?");	
	}
	
	function saveClick() {
		
		if(validateSaveData()) {
			$.ajax({
		           url : 'showPalCalTimigs!savePAlCalTiming.action',
		           data: getSaveOrUpdateData(),
		           dataType:"json",
		           beforeSend :ShowProgress(),
		           type : "POST",		           
		           success: loadSuccess,
			   error : errorFunction
		        });
		}
	}
	
	function loadSuccess(response){
		showResponceMessage(response)
		HideProgress();
		createGrid();
		clearInputSection() 		
		return true;
	}

	 function errorFunction(response) {
		showResponceMessage(response)
		HideProgress();
	}
	 
	 function showResponceMessage(response){
		 var reqMessage = response.messageString;
		 var reqMessageType = response.messageType;
		 
		if ((reqMessageType != null) && (reqMessageType == "Confirmation")) {			
			showConfirmation(reqMessage);
		} else {
			showERRMessage(reqMessage);
		}
	}

	function getSaveOrUpdateData(){
		data ={};

		data['palCaltimingDTO.palCalTimingId'] =$("#palCalId").val();
		data['palCaltimingDTO.airportCode'] = $("#selAirport" ).val();
		data['palCaltimingDTO.palDepGap'] =$("#txtPALTimeGap" ).val();
		data['palCaltimingDTO.calRepIntv'] =$("#txtCALTimInterval" ).val();
		data['palCaltimingDTO.strStartingZuluDate'] =$("#txtFromDate" ).val(); 
		data['palCaltimingDTO.strEndingZuluDate'] =$("#txtToDate" ).val();
		data['palCaltimingDTO.status'] =   $("#chkStatus").attr("checked") ? 'ACT' : 'INA';
		data['palCaltimingDTO.version'] = $("#palCalVersion").val();
		
		data['palCaltimingDTO.lastCalGap'] = $("#txtLstCAL").val();
		data['palCaltimingDTO.calRepIntvAfterCutoff'] = $("#txtCALTimeAfterCutoffTime").val();
		data['palCaltimingDTO.flightNumber'] =$("#txtFlightNo").val();
		data['palCaltimingDTO.allowAll'] =  $("#chkAllFlts").attr("checked") ? 'true' : 'false';
		

		return data;	
	}
	
	function gridOnClick(){
		clearInputSection();
		var rowId =$("#palcalDetailsGrid").getGridParam('selrow');  
		var rowData = $("#palcalDetailsGrid").getRowData(rowId);
		disableInputSection(true);
		pupulatePalCalDetails(rowData);
	}
	
	function allSelected() {
		if(document.forms[0].chkAllFlts.checked) {
			$("#txtFlightNo" ).val("");
			Disable("txtFlightNo", true);
		}else{
			Disable("txtFlightNo", false);
		}
	}

	function SearchClick() {
	
		if(SearchValid()==false) return;
		
		createGrid();
		disableInputSection(true)
		clearInputSection()
		disableButtons(true)
		disableGridButtons(false)	
		
	}
	
	function SearchValid(){
	//Mandatory fields		
		if (getFieldByID("selFrom").value == "") {
			showERRMessage(arrError['airportnull']);
			getFieldByID("selFrom").focus();
			return false;
		}else {
			return true;
		}
	}



	function disableInputSection(cond) {
		Disable("selAirport", cond);
		Disable("txtPALTimeGap", cond);
		Disable("txtCALTimInterval", cond);
		Disable("txtFromDate", cond);
		Disable("txtToDate", cond);
		Disable("chkStatus", cond);
		Disable("txtLstCAL", cond);
		Disable("txtCALTimeAfterCutoffTime", cond);
		Disable("chkAllFlts", cond);
		Disable("txtFlightNo", cond);
	
	}

	function clearInputSection() {
		
		$("#palCalId").val(null);
		$("#selAirport" ).val("");
		$("#txtPALTimeGap" ).val("");
		$("#txtCALTimInterval" ).val("");
		$("#txtFromDate" ).val(""); 
		$("#txtToDate" ).val("");
		
		$("#palCalVersion").val(-1);
		$("#txtCALTimeAfterCutoffTime").val("");
		$("#txtLstCAL").val("");
		
		$('#chkStatus').prop('checked', false); 
		$('#chkAllFlts').prop('checked', false);  
		$("#txtFlightNo").val("");	
	}
	
	function pupulatePalCalDetails(rowData){
		
		$("#palCalId").val(rowData['palCalId']);		
		$("#selAirport" ).val(rowData['airportCode']);
		$("#txtPALTimeGap" ).val(rowData['palDepatureGap']);
		$("#txtCALTimInterval" ).val(rowData['calTimeInterval']);
		$("#txtFromDate" ).val(rowData['startDate']); 
		$("#txtToDate" ).val(rowData['endDate']);
		
		$("#palCalVersion").val(rowData['palCalVersion']);
		$("#txtCALTimeAfterCutoffTime").val(rowData['calTimeAfterCutoffTime']);
		$("#txtLstCAL").val(rowData['lstCalGap']);
		if(rowData['staus']=='Active'){
			$('#chkStatus').prop('checked', true);  
		}else{
			$('#chkStatus').prop('checked', false);  
		}
	
		if(rowData['flightNumber'] == "ALL FLIGHTS") {
			$('#chkAllFlts').prop('checked', true);  
		}else {
			$('#chkAllFlts').prop('checked', false);  
			$("#txtFlightNo").val(rowData['flightNumber']);		
		}
		

	}

	function disableButtons(cond) {
		Disable("btnReset", cond);
		Disable("btnSave", cond);
	}
	
	function disableGridButtons(cond) {
		Disable("btnEdit", cond);
		Disable("btnCancel", cond);
	}

	function validateSaveData() {
		
		if((getFieldByID("hdnMode").value == "ADD") && trim(getFieldByID("selAirport").value) == "") {
			showERRMessage(arrError['airportnull']);
			getFieldByID("selAirport").focus();
			return false;
		}
		
		if((trim(getFieldByID("txtFlightNo").value) == "") && (!document.forms[0].chkAllFlts.checked)) {
			showERRMessage(arrError['noFlight']);
			getFieldByID("txtFlightNo").focus();
			return false;
		} 
		if(trim(getFieldByID("txtPALTimeGap").value) == "") {
			showERRMessage(arrError['nopaltime']);
			getFieldByID("txtPALTimeGap").focus();
			return false;
		}
		if(!validateTimeGapString(getFieldByID("txtPALTimeGap").value)) {
			showERRMessage(arrError['invalidtimegap']);
			getFieldByID("txtPALTimeGap").focus();
			return false;
		}
		if(trim(getFieldByID("txtCALTimInterval").value) == "") {
			showERRMessage(arrError['nocaltime']);
			getFieldByID("txtCALTimInterval").focus();
			return false;
		}
		if(!validateTimeGapString(getFieldByID("txtCALTimInterval").value)) {
			showERRMessage(arrError['invalidtimegap']);
			getFieldByID("txtCALTimInterval").focus();
			return false;
		}

		if(strMinPALGap != "" && Number(strMinPALGap) > getTimeInMinutes(getFieldByID("txtPALTimeGap").value)) {
			showERRMessage(arrError['lessminpal']+" "+formatMinitues(strMinPALGap));
			getFieldByID("txtPALTimeGap").focus();
			return false;
		}
		
		if(strMinCALInterval != "" && Number(strMinCALInterval) > getTimeInMinutes(getFieldByID("txtCALTimInterval").value)) {
			showERRMessage(arrError['lessmincal']+" "+formatMinitues(strMinCALInterval));
			getFieldByID("txtCALTimInterval").focus();
			return false;
		}
		if(getTimeInMinutes(getFieldByID("txtCALTimInterval").value) > Number(strMinPALGap)) {
			showERRMessage(arrError['greatmaxcal'] + formatMinitues(strMinPALGap));
			getFieldByID("txtCALTimInterval").focus();
			return false;
		} 
 
		if(trim(getFieldByID("txtFromDate").value) == "") {
			showERRMessage(arrError['startdate']);
			getFieldByID("txtFromDate").focus();
			return false;
		} 
		if(trim(getFieldByID("txtToDate").value) == "") {
			showERRMessage(arrError['enddate']);
			getFieldByID("txtToDate").focus();
			return false;
		} 
		if (!dateChk("txtFromDate")){
			showERRMessage(arrError['invaliddate']);
			getFieldByID("txtFromDate").focus();
			return false;
		}
		if (!dateChk("txtToDate")){
			showERRMessage(arrError['invaliddate']);
			getFieldByID("txtToDate").focus();
			return false;
		}
		//compare start date and stop date
		var startD = getFieldByID("txtFromDate").value;
		var stopD = getFieldByID("txtToDate").value;
		var strToday = getFieldByID("hdnToday").value;
		
		if ((getFieldByID("hdnMode").value == "ADD") && !CheckDates(strToday,startD)){
			showERRMessage(arrError['startdategreat']);
			return false;
		}
		
		if (!CheckDates(startD,stopD)){
			showERRMessage(arrError['stopdategreat']);
			return false;
		}
		
		if(maintainCALAfterCutoff =="true" && !validateTimeGapString(getFieldByID("txtCALTimeAfterCutoffTime").value)) {
			showERRMessage(arrError['invalidtimegap']);
			getFieldByID("txtCALTimeAfterCutoffTime").focus();
			return false;
		}
		
		if(maintainCALAfterCutoff =="true" && getTimeInMinutes(getFieldByID("txtCALTimeAfterCutoffTime").value) < Number(strMinCALAfterCutoff)) {
			showERRMessage(arrError['lessmincal'] + formatMinitues(strMinCALAfterCutoff));
			getFieldByID("txtCALTimeAfterCutoffTime").focus();
			return false;
		} 
		
		var lastCALValue =  getFieldByID("txtLstCAL").value ;
		if(trim(lastCALValue) == "") {
			showERRMessage(arrError['nopaltime']);
			getFieldByID("txtPALTimeGap").focus();
			return false;
		}
		if(lastCALValue != null && lastCALValue != ""){
			if(!validateTimeGapString(lastCALValue)) {
				showERRMessage(arrError['invalidtimegap']);
				getFieldByID("txtLstCAL").focus();
				return false;
			}
		}
		if(getTimeInMinutes(getFieldByID("txtLstCAL").value) < Number(strMinCalClausureTime)) {
			showERRMessage(arrError['lessmincal'] + formatMinitues(strMinCalClausureTime));
			getFieldByID("txtLstCAL").focus();
			return false;
		} 
		
		return true;
	}
	
	function validateTimeGapString(timeGap){
		// Assumption time Gap string is not null
		//Should be in DD:HH:MM format
		var validated = false;
		var timeGapArray = timeGap.split(":");
		
		if(timeGapArray != null && timeGapArray.length == 3){
			
			var mins = Number(trim(timeGapArray[2]));
			var hrs = Number(trim(timeGapArray[1]));
			var days = Number(trim(timeGapArray[0]));
			
			if(!isNaN(mins)){
				
				if(mins > 59 || mins < 0){
					return false;
				}
				
				if(!isNaN(hrs)){
					
					if(hrs > 23 || hrs < 0){
						return false;
					}
					
					if(isNaN(days)){
						return false;						
					} else {
						return true;
					}
					
				} else {
				
					return false
				}
				
			} else {
				return false;					
			}

		} else {
		
			return false;
		}
		
	}

	function getTimeInMinutes(timeGapString){
		//Data string format to be parsed "DD:HH:MM"
		var dataArray = timeGapString.split(":");
	
		//We parse only the data in correct format
		if(dataArray != null && dataArray.length == 3){
		
			var days = Number(trim(dataArray[0]));
			var hours = Number(trim(dataArray[1]));
			var mins = Number(trim(dataArray[2]));
		
			return (days * 1440 + hours * 60 + mins);
		
		} else {
		
			return false
		}
	}
	
	function formatMinitues (minutes){
		
		var mins = Number(minutes);
		var days = Math.floor(mins/1440);
		
		var hours = Math.floor((mins%1440)/60);
		
		var mins = (mins%1440)%60;
		
		if(days > 0 && hours > 0 && mins > 0){
			return days + " day(s) " + hours + " hour(s) " + mins + " min(s)";
		} else if (days > 0 && hours > 0){
			return days + " day(s) " + hours + " hour(s)" ;
		} else if (days > 0 && mins > 0){
			return days + " day(s) " + mins + " min(s)";
		} else if (days > 0){
			return days + " day(s) " + mins + " min(s)";
		}else if (hours > 0 && mins > 0){
			return hours + " hour(s) " + mins + " min(s)";
		}else if (hours > 0 ){
			return hours + " hour(s) ";
		}else if (mins > 0){
			return mins + " min(s)";
		} else {
			return days + " day(s) " + hours + " hour(s) " + mins + " min(s)";
		}
		
	}
	