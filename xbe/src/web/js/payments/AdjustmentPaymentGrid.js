	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "10%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "" ;
	objCol1.headerText = "Date";
	objCol1.itemAlign = "center"
	objCol1.sort = "true";

	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "12%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "" ;
	objCol2.headerText = "Debit Amount";
	objCol2.itemAlign = "right"
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "12%";
	objCol3.arrayIndex = 3;
	objCol3.toolTip = "" ;
	objCol3.headerText = "Credit Amount";
	objCol3.itemAlign = "right"

	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "27.5%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "" ;
	objCol4.headerText = "Remarks";
	objCol4.itemAlign = "left"
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "9%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "" ;
	objCol5.headerText = "TNX No.";
	objCol5.itemAlign = "left"
	 
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "9%";
	//objCol6.arrayIndex = 6;
	objCol6.toolTip = "" ;
	objCol6.headerText = "TNX No.";
//	objCol5.itemAlign = "left"	

	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "10%";
	objCol7.arrayIndex = 6;
	objCol7.toolTip =  "";
	objCol7.headerText = "User ID";
	objCol7.itemAlign = "left";
	
	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "7%";
	objCol8.arrayIndex = 7;
	objCol8.toolTip =  "";
	objCol8.headerText = "Carrier";
	objCol8.itemAlign = "left";
	
	var objCol9 = new DGColumn();
	objCol9.arrayIndex = 8;
	objCol9.width = "0px";
	
	var objCol10 = new DGColumn();
	objCol10.arrayIndex = 9;
	objCol10.width = "0px";
	// ---------------- Grid	
	var objDG = new DataGrid("spnAdjustments");
	objDG.addColumn(objCol5);
//	objDG.addColumn(objCol6);
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol7);
	objDG.addColumn(objCol8);
//	objDG.addColumn(objCol9);
//	objDG.addColumn(objCol10);
	

	objDG.width = "100%";
	objDG.height = "385px";
	objDG.headerBold = false;
	objDG.rowSelect = true;	
	objDG.arrGridData = parent.arrAdjData;  //arrData;
	objDG.seqNo = true;
	objDG.rowOver = false;
	objDG.backGroundColor = "#F4F4F4" ;	 //
/*	objDG.seqStartNo = intrecno;	
	objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
	objDG.paging = true
	objDG.pgnumRecPage = 20;
	objDG.rowClick = "RowClick";
	objDG.pgonClick = "gridNavigations"; */
	objDG.rowClick = "rowClick";
	objDG.displayGrid();
	