	//var strTabs = parent.parent.frames["frmTabs"].objTab.getTabID;
	//var arrTabs = strTabs.split(",");
	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "12%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "" ;
	objCol1.headerText = "Invoice Date";
	objCol1.itemAlign = "center"
	objCol1.sort = "true";

	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "12%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "" ;
	objCol2.headerText = "Invoice Number";
	objCol2.itemAlign = "right"
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "12%";
	objCol3.arrayIndex = 3;
	objCol3.toolTip = "" ;
	objCol3.headerText = "Invoice Amount";
	objCol3.itemAlign = "right"

	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "12%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "" ;
	objCol4.headerText = "Amount Paid";
	objCol4.itemAlign = "right"
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "12%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "" ;
	objCol5.headerText = "Balance to be <br>Paid";
	objCol5.itemAlign = "right"
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "custom";
	objCol6.width = "12%";
	objCol6.arrayIndex = 6 
	objCol6.ID = 'txtAmount';	
	objCol6.htmlTag = "<input type='text' id='txtAmount' name='txtAmount' size='10' maxlength='20' class='rightText' onKeyPress='parent.validateDecimalAmount(this)' onKeyUp='parent.validateDecimalAmount(this)' :CUSTOM: >" ;
	objCol6.toolTip = "" ;
	objCol6.headerText = "Amount<font class='mandatory'>&nbsp*</font>";
	objCol6.itemAlign = "center"
		
    var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "10%";
	objCol7.arrayIndex = 7;
	objCol7.toolTip = "" ;
	objCol7.headerText = "Granted Commission";
	objCol7.itemAlign = "right"
		
	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "10%";
	objCol8.arrayIndex = 8;
	objCol8.toolTip = "" ;
	objCol8.headerText = "Entity";
	objCol8.itemAlign = "center"

	 
	
	// ---------------- Grid	
	var objDG = new DataGrid("spnInvoiceDetails");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol6);
	objDG.addColumn(objCol7);
	objDG.addColumn(objCol8);

	objDG.width = "100%";
	objDG.height = "100px";
	objDG.headerBold = false;
	objDG.arrGridData = parent.frames["frmTabs"].arrInvoice;  //arrData;
	objDG.seqNo = true;
	objDG.rowOver = false;
	objDG.backGroundColor = "#F4F4F4" ;
	objDG.displayGrid();

	function selOnChange(objObj, strRow, strCol){
		setPageEdited(true);
	}
	