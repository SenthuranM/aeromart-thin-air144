//JavaScript Document
//Author -Shakir
var screenId = "SC_FACM_005";
var objWin;
var strArr=""
var intStat="0"
var strSearchTemp;
var objForm;

var objCal1 = new Calendar();
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtInvoiceDate", strDate);
		break;	
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	objOnFocus();
	if(IsTransEnable != 'true'){
		document.getElementById('spnTransactionGrid').style.display = 'none';
	}
	
	setField("hdnMode", "");	
	getFieldByID("selMonth").selected = currentMonth;	
	strSearchTemp = top[0].manifestSearchValue;
	
	if(IsTransEnable == 'true'){
		drawTransactionUI();
	}
	if ((strSearchTemp != "") && (strSearchTemp != null)) {
		var strSearch = strSearchTemp.split(",");
		if(IsTransEnable == 'true'){
			setField("selAgent",strSearch[0]);
			setField("txtInvoiceDate",strSearch[1]);
			if(trim(strSearch[2]) == 'Y'){
				arrInvData = top[0].arrInvoice;
				Disable("btnEmail", false);
				Disable("txtEmail", false);
				Disable("btnSend", false);
			}
		}else{
			setField("selAgent",strSearch[0]);
			setField("selYear",strSearch[1]);
			setField("selMonth",strSearch[2]);
			setField("selBP",strSearch[3]);
			setField("selEntity",strSearch[4]);
			if(strSearch[3]==1){
				strArr="Invoice Amount for the Period 1"
			}else{
				strArr="Invoice Amount for the Period 2"
			}
		}
	}
	
	Disable("btnDetails", true);
	Disable("btnEmail", true);
	Disable("txtEmail", true);
	Disable("btnSend", true);
	if(blnBaseCurr && getFieldByID("chkBase")) {
		getFieldByID("chkBase").checked = true;
	}else if (getFieldByID("chkBase")){
		getFieldByID("chkBase").checked = false;
	}
	buildInvoiceView();
}

function searchClick(){
	
	if(IsTransEnable == 'true'){
		var strTemp = getValue("selAgent")+","+getValue("txtInvoiceDate")+","+"N";
		setField("hdnSearchCriteria",strTemp);
		setTabSearch();
		setField("hdnMode","SEARCH");
		objForm  = document.getElementById("viewInvoice");
		objForm.target="_self";
		document.forms[0].submit();
	}else{
	var strMonth = (Number(getValue("selMonth")) - 1);
	var strYear = Number(getValue("selYear"));
	
	var enteredDate = new Date();
	enteredDate.setMonth(strMonth);
	enteredDate.setYear(strYear);
	
	var currentDate = new Date();
	currentDate.setMonth(currentMonth);
	currentDate.setYear(currentYear);
	
	if (getValue("selAgent") == "") {
		showCommonError("Error", arrError["agentEmpty"]);
		getFieldByID("selAgent").focus();	
		return false;
	} else if (enteredDate > currentDate) {
		showCommonError("Error", arrError["futureDate"]);
		getFieldByID("selMonth").focus();	
		return false;
	} else {
		
		var strTemp = getValue("selAgent")+","+getValue("selYear")+","+getValue("selMonth")+","+getValue("selBP")+","+getValue("selEntity") + "," + getText("selEntity");
		setField("hdnSearchCriteria",strTemp);
		setTabSearch();
		setField("hdnMode","SEARCH");
		objForm  = document.getElementById("viewInvoice");
		objForm.target="_self";
		document.forms[0].submit();
	} 
	}
}

function detailsClick(){
	if ((strSearchTemp != "") && (strData.length != 0)) {		
		setField("hdnRpt","DETAILS");
		setField("hdnAgents",getValue("selAgent"));		
		setField("hdnAgentName",getText("selAgent"));
		setField("hdnInvoice",strData[0]);
		setField("selYear",getValue("selYear"));
		setField("selMonth",parseInt(getValue("selMonth")));
		setField("selBP",parseInt(getValue("selBP")));
		setField("hdnAgentCurr", strData[11]);
		setField("radReportOption","HTML");
		setField("hdnMode","VIEW");
		setField("selEntity",getValue("selEntity"));
		setField("hdnEntityText",getText("selEntity"));
		if(document.getElementById("chkBase") != null){
			setField("chkBase",getValue("chkBase"));
		}
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		
		top[0].objWindow = window.open("about:blank","CWindow",strProp);			
		 objForm  = document.getElementById("viewInvoice");
		objForm.target = "CWindow";
		objForm.action = "saveViewInvoice.action";
		objForm.submit();
		
	}

}

function buildInvoiceView() {
	var strHTMLText = "";	
	
	
	if ((strSearchTemp != "") && (strData.length == 0)) {
		if(IsTransEnable == 'true' && (strSearchTemp != "") && (arrInvData.length == 0)){
			showCommonError("Error", arrError["noInvoice"]);
		}else if(IsTransEnable != 'true'){
			showCommonError("Error", arrError["noInvoice"]);
		}
		strHTMLText	= "<font class='fntBold'>" + arrError["invoiceEmpty"] + "</font>";
	} else if (strData.length != 0) {
		
		strHTMLText += "<table width='100%' border='0' cellpadding='2'  cellspacing='0'>";
		strHTMLText += "	<tr>";	
		strHTMLText += "		<td width='15%'>";
		strHTMLText += "			<font class='fntBold'>Invoice Number&nbsp;&nbsp;&nbsp;&nbsp;:</font>";
		strHTMLText += "		</td>";
		strHTMLText += "		<td width='20%'>";
		strHTMLText += "			<font>" + strData[0] + "</font>";
		strHTMLText += "		</td>";
		strHTMLText += "		<td width='45%'>";
		strHTMLText += "			<font class='fntBold'>" + strData[6] + "</font>";
		strHTMLText += "		</td>";
		strHTMLText += "		<td width='10%'>";
		strHTMLText += "			<font class='fntBold'>Entity &nbsp;&nbsp;&nbsp;&nbsp;:</font>";
		strHTMLText += "		</td>";
		strHTMLText += "		<td width='10%'>";
		strHTMLText += "			<font>" + getText("selEntity") + "</font>";
		strHTMLText += "		</td>";
		strHTMLText += "	</tr>";
		strHTMLText += "	<tr>";	
		strHTMLText += "		<td width='15%'>";
		strHTMLText += "			<font class='fntBold'>Invoice Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</font>";
		strHTMLText += "		</td>";
		strHTMLText += "		<td width='20%'>";
		strHTMLText += "			<font>" + strData[5] + "</font>";
		strHTMLText += "		</td>";
		strHTMLText += "		<td width='65%' colspan='3'>";
		strHTMLText += "			<font class='fntBold'>" + strData[3] + "</font>";
		strHTMLText += "		</td>";
		strHTMLText += "	</tr>";
		strHTMLText += "	<tr>";
		
		if(strData[24] == 'true'){
			strHTMLText += "		<td colspan='5' style='height:220px;' valign='top'>";
			strHTMLText += "			<br><table width='100%' border='0' cellpadding='2'  cellspacing='1' bgcolor='black'>";
			strHTMLText += "					<tr bgcolor='#ECECEC'>";	
			strHTMLText += "						<td align='center' width='10%'>";
			strHTMLText += "							<font class='fntBold'>PNR </font>";
			strHTMLText += "						</td>";
			strHTMLText += "						<td align='center' width='15%'>";
			strHTMLText += "							<font class='fntBold'>Name </font>";
			strHTMLText += "						</td>";
			strHTMLText += "						<td align='center' width='10%'>";
			strHTMLText += "							<font class='fntBold'>Route </font>";
			strHTMLText += "						</td>";
			strHTMLText += "						<td align='center' width='10%'>";
			strHTMLText += "							<font class='fntBold'>Booked Date </font>";
			strHTMLText += "						</td>";
			
			if(strData[23] == 'true'){
				strHTMLText += "						<td align='center' width='10%'>";
				strHTMLText += "							<font class='fntBold'>Fare </font>";
				strHTMLText += "						</td>";
				strHTMLText += "						<td align='center' width='10%'>";
				strHTMLText += "							<font class='fntBold'>Tax </font>";
				strHTMLText += "						</td>";
				strHTMLText += "						<td align='center' width='10%'>";
				strHTMLText += "							<font class='fntBold'>Modify </font>";
				strHTMLText += "						</td>";
				strHTMLText += "						<td align='center' width='10%'>";
				strHTMLText += "							<font class='fntBold'>Surcharge </font>";
				strHTMLText += "						</td>";
			}
			strHTMLText += "						<td align='center' width='15%'>";
			strHTMLText += "							<font class='fntBold'>Amount " + strData[11] + "</font>";
			strHTMLText += "						</td>";
			strHTMLText += "					</tr>";
			
			strHTMLText += "				<tr bgcolor='white'>";	
			strHTMLText += "					<td align='center'>";
			strHTMLText += "						<font>" + strData[25] + "</font>";
			strHTMLText += "					</td>";
			strHTMLText += "					<td align='center'>";
			strHTMLText += "						<font>" + strData[26] + "</font>";
			strHTMLText += "					</td>";	
			strHTMLText += "					<td align='center'>";
			strHTMLText += "						<font>" + strData[27] + "</font>";
			strHTMLText += "					</td>";
			strHTMLText += "					<td align='center'>";
			strHTMLText += "						<font>" + strData[28] + "</font>";
			strHTMLText += "					</td>";
			if(strData[23] == 'true') {
				strHTMLText += "				<td align='right'>";
				strHTMLText += "					<font>" + strData[17] + "</font>";
				strHTMLText += "				</td>";
				strHTMLText += "				<td align='right'>";
				strHTMLText += "					<font>" + strData[18] + "</font>";
				strHTMLText += "				</td>";
				strHTMLText += "				<td align='right'>";
				strHTMLText += "					<font>" + strData[19] + "</font>";
				strHTMLText += "				</td>";
				strHTMLText += "				<td align='right'>";
				strHTMLText += "					<font>" + strData[20] + "</font>";
				strHTMLText += "				</td>";
			}
			strHTMLText += "				<td align='right'>";
			strHTMLText += "					<font>" + strData[2] + "</font>";
			strHTMLText += "				</td>";
			strHTMLText += "				</tr>";
			
		} else if(strData[12] == 'true'){
				strHTMLText += "		<td colspan='5' style='height:220px;' valign='top'>";
				strHTMLText += "			<br><table width='100%' border='0' cellpadding='2'  cellspacing='1' bgcolor='black'>";
				strHTMLText += "					<tr bgcolor='#ECECEC'>";	
				strHTMLText += "						<td align='center' width='50%'>";
				strHTMLText += "							<font class='fntBold'>Particulars </font>";
				strHTMLText += "						</td>";
				strHTMLText += "						<td align='center' width='25%'>";
				strHTMLText += "							<font class='fntBold'>Amount - " + strData[11] + "</font>";
				strHTMLText += "						</td>";
				
				if(blnBaseCurr) {
					strHTMLText += "						<td align='center' width='25%'>";
					strHTMLText += "							<font class='fntBold'>Amount - " + baseCurrency + "</font>";
					strHTMLText += "						</td>";
				
				}
				strHTMLText += "					</tr>";
				
				strHTMLText += "				<tr bgcolor='white'>";	
				strHTMLText += "					<td align='left'>";
				strHTMLText += "						<font>Total Fare Amount of " + strData[4] + " - " + strData[5]+ "</font>";
				strHTMLText += "					</td>";
				strHTMLText += "					<td align='right'>";
				strHTMLText += "						<font>" + strData[13] + "</font>";
				strHTMLText += "					</td>";	
				
				if(blnBaseCurr) {
					strHTMLText += "				<td align='right'>";
					strHTMLText += "					<font>" + strData[17] + "</font>";
					strHTMLText += "				</td>";
				}		
				strHTMLText += "				</tr>";
				
				strHTMLText += "				<tr bgcolor='white'>";	
				strHTMLText += "					<td align='left'>";
				strHTMLText += "						<font>Total Tax Amount of " + strData[4] + " - " + strData[5]+ "</font>";
				strHTMLText += "					</td>";
				strHTMLText += "					<td align='right'>";
				strHTMLText += "						<font>" + strData[14] + "</font>";
				strHTMLText += "					</td>";
				
				if(blnBaseCurr) {
					strHTMLText += "				<td align='right'>";
					strHTMLText += "					<font>" + strData[18] + "</font>";
					strHTMLText += "				</td>";
				}	
							
				strHTMLText += "				</tr>";
					
				strHTMLText += "				<tr bgcolor='white'>";	
				strHTMLText += "					<td align='left'>";
				strHTMLText += "						<font>Total Modify Amount of " + strData[4] + " - " + strData[5]+ "</font>";
				strHTMLText += "					</td>";
				strHTMLText += "					<td align='right'>";
				strHTMLText += "						<font>" + strData[15] + "</font>";
				strHTMLText += "					</td>";	
				
				if(blnBaseCurr) {
					strHTMLText += "				<td align='right'>";
					strHTMLText += "					<font>" + strData[19] + "</font>";
					strHTMLText += "				</td>";
				}	
						
				strHTMLText += "				</tr>";
					
				strHTMLText += "				<tr bgcolor='white'>";	
				strHTMLText += "					<td align='left'>";
				strHTMLText += "						<font>Total Surcharge Amount of " + strData[4] + " - " + strData[5]+ "</font>";
				strHTMLText += "					</td>";
				strHTMLText += "					<td align='right'>";
				strHTMLText += "						<font>" + strData[16] + "</font>";
				strHTMLText += "					</td>";
				
				if(blnBaseCurr) {
					strHTMLText += "				<td align='right'>";
					strHTMLText += "					<font>" + strData[20] + "</font>";
					strHTMLText += "				</td>";
				}	
				
							
				strHTMLText += "				</tr>";
				
				if( strData[21] && strData[22] && Number(strData[22] > 0)) {
						
					strHTMLText += "				<tr bgcolor='white'>";	
					strHTMLText += "					<td align='left'>";
					strHTMLText += "						<font>Holidays Sales of " + strData[4] + " - " + strData[5]+ "</font>";
					strHTMLText += "					</td>";
					strHTMLText += "					<td align='right'>";
					strHTMLText += "						<font>" + strData[22] + "</font>";
					strHTMLText += "					</td>";	
					if(blnBaseCurr) {
						strHTMLText += "				<td align='right'>";
						strHTMLText += "					<font>" + strData[21] + "</font>";
						strHTMLText += "				</td>";
					}	
					
					strHTMLText += "				</tr>";
				
				}else {
					strData[21]  = 0;
					strData[22]  = 0;
				}
					
				strHTMLText += "			<tr bgcolor='white'>";	
				strHTMLText += "				<td align='left'>";
				strHTMLText += "					<font>Total Sales of " + strData[4] + " - " + strData[5]+ "</font>";
				strHTMLText += "				</td>";
				strHTMLText += "				<td align='right'>";
				strHTMLText += "					<font>" +strData[10]+ "</font>";
				strHTMLText += "				</td>";
				if(blnBaseCurr) {
					strHTMLText += "				<td align='right'>";
					strHTMLText += "					<font>" + strData[2] + "</font>";
					strHTMLText += "				</td>";
				}	
				
			
			}else {
			
				
				strHTMLText += "		<td colspan='5' style='height:220px;' valign='top'>";
				strHTMLText += "			<br><table width='100%' border='0' cellpadding='2'  cellspacing='1' bgcolor='black'>";
				strHTMLText += "					<tr bgcolor='#ECECEC'>";	
				strHTMLText += "						<td align='center' width='50%'>";
				strHTMLText += "							<font class='fntBold'>Particulars </font>";
				strHTMLText += "						</td>";
				strHTMLText += "						<td align='center' width='25%'>";
				strHTMLText += "							<font class='fntBold'>Amount - " + baseCurrency + "</font>";
				strHTMLText += "						</td>";
				
				if(baseCurrency != strData[11] && strData[12] == 'true') {
					strHTMLText += "						<td align='center' width='25%'>";
					strHTMLText += "							<font class='fntBold'>Amount - " + strData[11] + "</font>";
					strHTMLText += "						</td>";
									
				}
				
				strHTMLText += "					</tr>";
		
				var extProdAmtsObj = JSON.parse(strData[31]);
		        for ( var key in extProdAmtsObj) {
		          if (extProdAmtsObj.hasOwnProperty(key)) {
		            strHTMLText += "        <tr bgcolor='white'>";
		            strHTMLText += "          <td align='left'>";
		            strHTMLText += "         <font> " + key + " Sales of " + strData[4] + " - " + strData[5] + "</font>";
		            strHTMLText += "          </td>";
		            strHTMLText += "          <td align='right'>";
		            strHTMLText += "            <font>" + extProdAmtsObj[key] + "</font>";
		            strHTMLText += "          </td>";
		            strHTMLText += "        </tr>";
		
		          }
		        }
  	

				strHTMLText += "			<tr bgcolor='white'>";	
				strHTMLText += "				<td align='left'>";
				strHTMLText += "					<font>Sales of " + strData[4] + " - " + strData[5]+ "</font>";
				strHTMLText += "				</td>";
				strHTMLText += "				<td align='right'>";
				strHTMLText += "					<font>" + strData[2] + "</font>";
				strHTMLText += "				</td>";
				if(baseCurrency != strData[11] && strData[12] == 'true') {
					
					strHTMLText += "				<td align='right'>";
					strHTMLText += "                    <font>" + strData[10] + "</font>";
					strHTMLText += "                </td>";
				}
			}		
		
		strHTMLText += "</tr>";
		strHTMLText += "</table>";
		strHTMLText += "</td>";
		strHTMLText += "</tr>";
		strHTMLText += "<tr>";	
		strHTMLText += "<td colspan='3'>";
		strHTMLText += "<table width='100%' border='0' cellpadding='2'  cellspacing='1'>";					
		strHTMLText += "<tr>";					
		strHTMLText += "<td width='16%'>";
		strHTMLText += "<font class='fntBold'>Payment Terms</font>";
		strHTMLText += "</td>";
		strHTMLText += "<td width='2%'><font class='fntBold'>:</font></td>";	
		strHTMLText += "<td>";
		strHTMLText += "<font>" + paymentTerms + "</font>";
		strHTMLText += "</td>";
		strHTMLText += "</tr>";
		strHTMLText += "<tr>";	
		strHTMLText += "<td>";
		strHTMLText += "<font class='fntBold'>Account Number</font>";
		strHTMLText += "</td>";
		strHTMLText += "<td><font class='fntBold'>:</font></td>";		
		strHTMLText += "<td>";
		strHTMLText += "<font>" +accountNumber + "</font>";
		strHTMLText += "</td>";
		strHTMLText += "</tr>";
		strHTMLText += "<tr>";	
		strHTMLText += "<td>";
		strHTMLText += "<font class='fntBold'>Bank Name & Address</font>";
		strHTMLText += "</td>";
		strHTMLText += "<td><font class='fntBold'>:</font></td>";			
		strHTMLText += "<td>";
		strHTMLText += "<font>" + bankName + ', ' + bankAddress+"</font>";
		strHTMLText += "</td>";	
		strHTMLText += "</tr>";
		
		strHTMLText += "<tr>";	
		strHTMLText += "<td>";
		strHTMLText += "<font class='fntBold'>Swift Code</font>";
		strHTMLText += "</td>";
		strHTMLText += "<td><font class='fntBold'>:</font></td>";			
		strHTMLText += "<td>";
		strHTMLText += "<font>" + swiftCode +"</font>";
		strHTMLText += "</td>";	
		strHTMLText += "</tr>";
		
		strHTMLText += "<tr>";	
		strHTMLText += "<td>";
		strHTMLText += "<font class='fntBold'>IBAN No</font>";
		strHTMLText += "</td>";
		strHTMLText += "<td><font class='fntBold'>:</font></td>";			
		strHTMLText += "<td>";
		strHTMLText += "<font>" + bankIBANCode +"</font>";
		strHTMLText += "</td>";	
		strHTMLText += "</tr>";
		
		strHTMLText += "<tr>";	
		strHTMLText += "<td>";
		strHTMLText += "<font class='fntBold'>Beneficiary Name</font>";
		strHTMLText += "</td>";
		strHTMLText += "<td><font class='fntBold'>:</font></td>";			
		strHTMLText += "<td>";
		strHTMLText += "<font>" + bankBeneficiaryName +"</font>";
		strHTMLText += "</td>";	
		strHTMLText += "</tr>";
		
		
		if(strData[29] == 'true'){
			strHTMLText += "<tr>";	
			strHTMLText += "<td>";
			strHTMLText += "<font class='fntBold'>Offered Commission</font>";
			strHTMLText += "</td>";
			strHTMLText += "<td><font class='fntBold'>:</font></td>";			
			strHTMLText += "<td>";
			strHTMLText += "<font>" + strData[30] + " " +baseCurrency + " (Already credited to the agents account)" +"</font>";
			strHTMLText += "</td>";
			strHTMLText += "</tr>";
		}
		
		strHTMLText += "</table>";			
		strHTMLText += "</td>";
		strHTMLText += "</tr>";
		strHTMLText += "</table>";		
		
		setField("agentEmail", strData[7]);
		if(IsTransEnable != 'true'){
			Disable("btnDetails", false);
		}
		Disable("btnEmail", false);
		Disable("txtEmail", false);
		Disable("btnSend", false);
	}
	DivWrite('spnInvoiceDetails', strHTMLText);	
}

function sendEmail() {
	if(IsTransEnable == 'true'){
		
		var tempSearch = top[0].manifestSearchValue.split(",");
		var strTemp = getValue("selAgent")+","+getValue("txtInvoiceDate")+","+"Y";
		setField("hdnPNR",trim(tempSearch[4]));
		setField("hdnSearchCriteria",strTemp);
		setField("hdnLive", "LIVE");
		setTabSearch();
		
	}else{
		var strTemp = getValue("selAgent")+","+getValue("selYear")+","+getValue("selMonth")+","+getValue("selBP");
		setField("hdnSearchCriteria",strTemp);
		setTabSearch();
		setField("selYear",getValue("selYear"));
		setField("selMonth",parseInt(getValue("selMonth")));
		setField("selBP",parseInt(getValue("selBP")));
	}
	setField("hdnAgents",getValue("selAgent"));	
	setField("hdnAgentName",getText("selAgent"));	
	setField("hdnMode","EMAIL1");
	setField("radReportOption","PDF");
	setField("hdnInvoice",strData[0]);
	
	setField("paymentTerms",paymentTerms);	
	setField("accountNumber",accountNumber);
	setField("accountName",accountName);
	setField("bankName", bankName);
	setField("bankAddress",bankAddress);	
	setField("bankBeneficiaryName", bankBeneficiaryName);
	setField("bankIBANCode",bankIBANCode);
	setField("swiftCode",swiftCode);	
	setField("hdnAgentCurr", strData[11]);	
	setField("selEntity",getValue("selEntity"));
	setField("hdnEntityText",getText("selEntity"));
	
	objForm  = document.getElementById("viewInvoice");
	objForm.target='ifrmHidden';	
	objForm.submit();
	showCommonError("CONFIRMATION", arrError["agentEmail"]);			
}

function sendEmailToGivenEmail() {
	if(isEmpty(getText("txtEmail"))){
		showCommonError("Error",arrError["EMailisEmpty"]);
		getFieldByID("txtEmail").focus();
	} else if(!validateEmail()){
		showCommonError("Error",arrError["InvalidEmail"]);
		getFieldByID("txtEmail").focus();
	} else{
		if(IsTransEnable == 'true'){
			var tempSearch = top[0].manifestSearchValue.split(",");
			setField("hdnPNR",trim(tempSearch[4]));
			var strTemp = getValue("selAgent")+","+getValue("txtInvoiceDate")+","+"Y";
			setField("hdnSearchCriteria",strTemp);
			setTabSearch();
		}else{
			var strTemp = getValue("selAgent")+","+getValue("selYear")+","+getValue("selMonth")+","+getValue("selBP");
			setField("hdnSearchCriteria",strTemp);
			setTabSearch();			
			setField("selYear",getValue("selYear"));
			setField("selMonth",parseInt(getValue("selMonth")));
			setField("selBP",parseInt(getValue("selBP")));
		}
		setField("hdnAgentName",getText("selAgent"));
		setField("hdnAgents",getValue("selAgent"));	
		setField("hdnMode","EMAIL2");
		setField("radReportOption","PDF");		
		setField("paymentTerms",paymentTerms);	
		setField("accountNumber",accountNumber);
		setField("accountName",accountName);
		setField("bankName", bankName);
		setField("bankAddress",bankAddress);
		setField("swiftCode",swiftCode);
		setField("bankIBANCode",bankIBANCode);
		setField("bankBeneficiaryName",bankBeneficiaryName);
		setField("hdnAgentCurr", strData[11]);
		setField("hdnInvoice",strData[0]);
		setField("selEntity",getValue("selEntity"));
		setField("hdnEntityText",getText("selEntity"));
		
		var objForm  = document.getElementById("viewInvoice");
		objForm.target='viewInvoice';	
		objForm.submit();				
		setField("txtEmail", "");
		showCommonError("CONFIRMATION", arrError["givenEmail"]);
	}
}

function unload(){
	if ((top[0].objWin) && (!top[0].objWin.closed))	{
		top[0].objWin.close();
	}
}

function validateEmail(){	
	objOnFocus();
	var tempVar = checkEmail(trim(getText("txtEmail")));
	if(!tempVar){
		setField("txtEmail",""); 
		getFieldByID("txtEmail").focus();
	}
	return tempVar;	
}

function objOnFocus(){
	top[2].HidePageMessage();
}

function closeClick(){
	if (top.loadCheck(top.pageEdited)) {
		objOnFocus();
		top[0].manifestSearchValue="";
		top[1].objTMenu.tabRemove("SC_FACM_005");
		
	}
}

function setTabSearch(){
	var setValue = getText("hdnSearchCriteria");
	top[0].manifestSearchValue = setValue;
}

function drawTransactionUI(){	
	var strHTML = '';
	strHTML += '<input name="txtInvoiceDate" type="text" id="txtInvoiceDate" size="10" style="width:80px;" onBlur="dateChk('+ "'txtInvoiceDate'" +')" maxlength="10">';
	strHTML += '&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Invoice Date">';
	strHTML += '<img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>&nbsp;*</b></font> ';
	
	DivWrite('invSearchTbl', strHTML);
}

function rowClick(strRowID, strID){
	var strInvoiceNo = arrInvData[strRowID][0];
	var pnr = arrInvData[strRowID][2];
	setField("hdnInvoice",strInvoiceNo);
	setField("hdnPNR",pnr);
	top[0].arrInvoice = arrInvData;
	var strTemp = getValue("selAgent")+","+getValue("txtInvoiceDate")+","+"Y"+","+strInvoiceNo+","+pnr;
	setField("hdnSearchCriteria",strTemp);
	setTabSearch();
	setField("hdnMode","SEARCH");
	objForm  = document.getElementById("viewInvoice");
	objForm.target="_self";
	document.forms[0].submit();
}
winOnLoad();