	
	var creditNotReversed = true;
	var isReverseOperation = false;
	
	function CHOnLoad(strMsg, strMsgType) {
		ShowProgress();
		if (strMsg != null && strMsgType == "Error") {
			showWindowCommonError(strMsgType, strMsg);
		}
		if(arrFormData) {
			var numCreditLim = parseFloat(arrFormData[1]);
			var numAmtDue = parseFloat(arrFormData[6]);
			var numInvsettle = parseFloat(arrFormData[7]);
			var numBSPCreditLim = parseFloat(arrFormData[19]);			
			checkCash(arrFormData[0]);
			DivWrite("spnAgentName", arrFormData[5]);
			if(numCreditLim != 0.00) 
				DivWrite("spnCreditLimit", arrFormData[1]);
			if(numAmtDue != 0.00) 
				DivWrite("spnAmountDue", arrFormData[6]);
			if(numInvsettle != 0.00) 					
				DivWrite("spnAvlInvSettle", arrFormData[7]);
			if(numBSPCreditLim != 0.00) 					
				DivWrite("spnBSPCreditLimit", arrFormData[19]);
			
			
					
			if(arrFormData[14] != "FROMPAYMENT") {
				
				setField("txtDStart",arrFormData[2]);
				setField("txtDStop", arrFormData[3]);

				
				if(arrFormData[8] && arrFormData[8] == 'true'){
					getFieldByID("chkTypeR").checked = true;
				}else {
					getFieldByID("chkTypeR").checked = false;
				}
				if(arrFormData[9] && arrFormData[9] == 'true'){
					if(getFieldByID("chkTypeC"))
					getFieldByID("chkTypeC").checked = true;
				}else {
					if(getFieldByID("chkTypeC"))
					getFieldByID("chkTypeC").checked = false;
				}
				if(arrFormData[10] && arrFormData[10] == 'true'){
					if(getFieldByID("chkTypeD"))
					getFieldByID("chkTypeD").checked = true;
				}else {
					if(getFieldByID("chkTypeD"))
					getFieldByID("chkTypeD").checked = false;
				}
				if(arrFormData[11] && arrFormData[11] == 'true'){
					if(getFieldByID("chkTypeRR"))
					getFieldByID("chkTypeRR").checked = true;
				}else {
					if(getFieldByID("chkTypeRR"))
					getFieldByID("chkTypeRR").checked = false;
				}
				if(arrFormData[12] && arrFormData[12] == 'true'){
					getFieldByID("chkTypeA").checked = true;
				}else {
					getFieldByID("chkTypeA").checked = false;
				}
				if(arrFormData[15] && arrFormData[15] == 'true'){
					if(getFieldByID("chkTypeQ"))
					getFieldByID("chkTypeQ").checked = true;
				}else {
					if(getFieldByID("chkTypeQ"))
					getFieldByID("chkTypeQ").checked = false;
				}
				if(arrFormData[13] && arrFormData[13] == 'true'){
					if(getFieldByID("chkTypeRV"))
					getFieldByID("chkTypeRV").checked = true;
				}else {
					if(getFieldByID("chkTypeRV"))
					getFieldByID("chkTypeRV").checked = false;
				}
				
				if(arrFormData[18] && arrFormData[18] != ''){
					setField("searchCarrier", arrFormData[18]);
				}else{
					setField("searchCarrier", 'ALL');
				}
				
				if(arrFormData[20] && arrFormData[20] == 'true'){
					if(getFieldByID("chkTypeBSP"))
					getFieldByID("chkTypeBSP").checked = true;
				}else {
					if(getFieldByID("chkTypeBSP"))
					getFieldByID("chkTypeBSP").checked = false;
				}
				
				if(arrFormData[22] && arrFormData[22] == 'true'){
					if(getFieldByID("chkTypeCG"))
					getFieldByID("chkTypeCG").checked = true;
				}else {
					if(getFieldByID("chkTypeCG"))
					getFieldByID("chkTypeCG").checked = false;
				}
				
				if(arrFormData[23] && arrFormData[23] == 'true'){
					if(getFieldByID("chkTypeCR"))
					getFieldByID("chkTypeCR").checked = true;
				}else {
					if(getFieldByID("chkTypeCR"))
					getFieldByID("chkTypeCR").checked = false;
				}
				if(arrFormData[28] && arrFormData[28] == 'true'){
					if(getFieldByID("chkTypeAO"))
					getFieldByID("chkTypeAO").checked = true;
				}else {
					if(getFieldByID("chkTypeAO"))
					getFieldByID("chkTypeAO").checked = false;
				}
				if(arrFormData[29] && arrFormData[29] == 'true'){
					if(getFieldByID("chkTypeCCD"))
					getFieldByID("chkTypeCCD").checked = true;
				}else {
					if(getFieldByID("chkTypeCCD"))
						getFieldByID("chkTypeCCD").checked = false;
				}
				
			}else {
				
				getFieldByID("chkTypeR").checked = true;
				if(getFieldByID("chkTypeC"))			
					getFieldByID("chkTypeC").checked = true;		
				if(getFieldByID("chkTypeD"))
					getFieldByID("chkTypeD").checked = true;			
				if(getFieldByID("chkTypeRR"))
					getFieldByID("chkTypeRR").checked = true;		
				getFieldByID("chkTypeA").checked = true;
				if(getFieldByID("chkTypeQ"))
					getFieldByID("chkTypeQ").checked = true;
				if(getFieldByID("chkTypeRV"))
					getFieldByID("chkTypeRV").checked = true;
				if(getFieldByID("chkTypeBSP"))
					getFieldByID("chkTypeBSP").checked = true;
				getFieldByID("chkTypeCG").checked = true;
				getFieldByID("chkTypeCR").checked = true;
				getFieldByID("chkTypeAO").checked = true;
				getFieldByID("chkTypeCCD").checked = true;
					
				setField("searchCarrier", 'ALL');	
			
			}
							
		}
		populateCurrCombo();
		payCurrType();
		HideProgress();
		hideEmptyAdjustmentCarrierList();
		//show popup
		if (trim(popupmessage).length != 0) {
			alert(popupmessage);
		}				
		$("#txtAmount").bind('keypress', function(e) {
			return isValidNumericChar({allowDecimal:true, allowNegative:false}, e);			
		});
		if(isPreventDuplicateRecord == "true" && (isDuplicateAgentAmountCombinationExists == "true" || isExactMatchExists == "true")){
			raiseWarningForDuplicateRecords();
		}
		
		if (hasPriviForViewPayOnly != undefined && hasPriviForViewPayOnly == true) {			
			$("#paymentOptionData").hide();
		} else {
			$("#paymentOptionData").show();
		}
	}

	function checkCash (agentCode) {

		if (searchGSA != undefined && searchGSA == true) {
			  getFieldByID("selType").remove("Select");
			  
		}
		
		if( agentCode != undefined && searchGSA != undefined && searchGSA == true && 
		gNameNCode!="" && gNameNCode.indexOf("^") > 0 && trim(agentCode) == trim(gNameNCode.split("^")[1]) ) {
		      
		 if (hasPriviForViewPayOnly != undefined && hasPriviForViewPayOnly != true) {	
		    	   getFieldByID("txtAmount").disabled = true;
			       getFieldByID("selType").disabled = true;
			       getFieldByID("txtRemarks").disabled = true;
		    	   getFieldByID("btnConfirm").disabled = true;
			       setVisible("btnConfirm", false); 
		 }              	                  
		} 

	}

	/**
	 * hideEmptyCarrierList
	 * @param {type} param 
	 */
	 function hideEmptyAdjustmentCarrierList() {
	 	setVisible("selCarrierDiv", enableAdjustmentCarrier);
	 	setVisible("adjustmentCarrierSearchDiv", enableAdjustmentCarrier);
	 	setVisible("selCarrierlbl", enableAdjustmentCarrier);
	}
 
	function validateDecimal(obj) {
		setPageEdited(true);
		var strCR = getFieldByID(obj).value;
		var strLen = strCR.length;
		var blnVal;
		
		
		if (isEmpty(getText("selType")) || trim(getText("selType")) == "Select") {
			showWindowCommonError("Error", arrError["typenull"]);		
			getFieldByID("selType").focus();
			return false;
		}
		var type = getText("selType");
		
		if(type == "Adjustment") {
			blnVal = anyCurrencyValidate(strCR, 10,2);
		}else {
			blnVal = currencyValidate(strCR, 10,2);		
		}		
		
		var wholeNumber;
		if(!blnVal){
			if (strCR.indexOf(".") != -1) {
				wholeNumber = strCR.substr(0, strCR.indexOf("."));
				if (wholeNumber.length > 13) {
					setField(obj, strCR.substr(0, wholeNumber.length - 1)); 	
				} else {
					setField(obj,strCR.substr(0, strLen-1)); 		
				}
			} else {
				setField(obj, strCR.substr(0, strLen - 1)); 		
			}	
			getFieldByID(obj).focus();
		}
	}

	function confirmClick(isFreshRecord) {
		
		$("#selCarrier").prop('disabled', false);
		$("#selType").prop('disabled', false);
		
		if(!isReverseOperation && $("#selType").val() == "RVCRE"){
			showWindowCommonError("Error", arrError["selectCredit"]);
			$("#selCarrier").prop('disabled', true);
			$("#selType").prop('disabled', true);
			return false;
		}
		
		var amt = trim(getText("txtAmount"));
		var poolamt = trim(arrFormData[7]);
		var cnfMessage = "The record (";
		var payType = trim(getText("selType"));		
		var receiptNo = trim(getText("txtReciptNo"));
		var warnForDuplicateRecord = true;
		var duplicateRecordWithReceipt = false;
		var duplicateRecordWithoutReceipt = false;
		var currentRecord = payType + amt + receiptNo;
		var agentCode  = arrFormData[5];
		
		if (isEmpty(getText("selType")) || trim(getText("selType")) == "Select") {
			showWindowCommonError("Error", arrError["typenull"]);		
			getFieldByID("selType").focus();
			return false;
		}
		
		
		if (payType == "BSP" && !maintainAgentBspCredLmt) {
			showWindowCommonError("Error", arrError["bspCredDisabled"]);		
			getFieldByID("selType").focus();
			return false;
		}
		
		
		if(enableAdjustmentCarrier){
			if (isEmpty(getText("selCarrier")) || trim(getText("selCarrier")) == "Select") {
				showWindowCommonError("Error", "TAIR-71359: Carrier is Blank");		
				getFieldByID("selCarrier").focus();
				return false;
			}
		}			
		
		if (isEmpty(getText("txtAmount"))) {
			showWindowCommonError("Error", arrError["amountnull"]);		
			getFieldByID("txtAmount").focus();
			return false;
		}
		if (parseFloat(amt) == 0.00) {
			showWindowCommonError("Error", arrError["amountnull"]);		
			getFieldByID("txtAmount").focus();
			return false;
		}	       
	
		if (isEmpty(trim(getText("txtRemarks")))) {
			showWindowCommonError("Error", arrError["remarksnull"]);		
			getFieldByID("txtRemarks").focus();		
			return false;
		} 
		
		if (isEmpty(trim(getText("txtReciptNo")))) {
			showWindowCommonError("Error", "TAIR-71359: Recipt No is Blank");		
			getFieldByID("txtReciptNo").focus();
			return false;
		}
		
		var tempName = trim(getText("txtRemarks"));
		tempName=replaceall(tempName,"'" , "`");
		tempName=replaceEnter(tempName);
		setField("txtRemarks",tempName);
		var currCode = '';
		if(arrFormData[16] != 'B'){	
			currCode = arrFormData[17];
		}else{
			currCode = baseCurrency;
		}
		
		if(isFreshRecord == true) {
			cnfMessage = cnfMessage + payType + " / " + amt +" " + currCode + " ) will be saved. Do You wish to Continue ?";				
		} else {
			if(isExactMatchExists == "true"){
				cnfMessage = cnfMessage + payType + " / " + amt +" " + currCode + " ) with Receipt No.: " + receiptNo + " already exists for " 
				+ arrFormData[5] + ". Please enter valid new receipt no. to proceed further.";	
			} else if(isDuplicateAgentAmountCombinationExists == "true"){
				cnfMessage = cnfMessage + payType + " / " + amt +" " + currCode + " ) for " + agentCode
				+ " already exists. Do you still wish to continue?"; 
			} 
		}			
		
		setPageEdited(false);	
		setField("selType", trim(getText("selType")));
		if(enableAdjustmentCarrier){
			setField("selCarrier", trim(getText("selCarrier")));
		}
		setField("txtRemarks", trim(getText("txtRemarks")));
		setField("hdnFromPage","FROMADJUST");
		
		
		if( isReverseOperation && !creditNotReversed){
			showWindowCommonError("Error", arrError["reversed"]);
			$("#selCarrier").prop('disabled', true);
			$("#selType").prop('disabled', true);
			return false;
		}
		
		if(isPreventDuplicateRecord == "true"){
			if(payType != "Cheque" && payType != "Cash") {
				if(confirm(cnfMessage)){
					saveRecords(true);
				} else { 
					reSetFormFields();
				}
			} else if(isFreshRecord == true) {
				if(confirm(cnfMessage)){
					saveRecords(false);
				} else {
					if($("#selType").val() == "RVCRE"){
						$("#selCarrier").prop('disabled', true);
						$("#selType").prop('disabled', true);
					}
					reSetFormFields();
				}			
			} else {
				if(confirm(cnfMessage)){
					if(isExactMatchExists == "true"){
						reSetFormFields();
					} else if(isExactMatchExists == "false"){
						saveRecords(true);
					} 
				} else {
					reSetFormFields();
					if($("#selType").val() == "RVCRE"){
						$("#selCarrier").prop('disabled', true);
						$("#selType").prop('disabled', true);
					}
				}
			}
		} else {
			if( confirm(cnfMessage)) {
				getFieldByID("btnConfirm").disabled = true;
				setField("hdnCMode", "SAVEADJPAAYMENT");
				setField("hdnCode", arrFormData[0]);
				setField("hdnCredit", arrFormData[1]);
			
				setField("hdnAgentID",arrFormData[0]);
				setField("hdnAgentName",arrFormData[5]);
				setField("hdnCreditLimit",arrFormData[1]);
					
				document.forms[0].action ="showAdjustmentPayment.action";
				document.forms[0].submit();
			} else {
				getFieldByID("btnConfirm").disabled = false;
				if($("#selType").val() == "RVCRE"){
					$("#selCarrier").prop('disabled', true);
					$("#selType").prop('disabled', true);
				}
			}
		}
	}
	
	function saveRecords(isAcceptRecord){
		setField("hdnCMode", "SAVEADJPAAYMENT");
		setField("hdnCode", arrFormData[0]);
		setField("hdnCredit", arrFormData[1]);
	
		setField("hdnAgentID",arrFormData[0]);
		setField("hdnAgentName",arrFormData[5]);
		setField("hdnCreditLimit",arrFormData[1]);
		setField("hdnIsAcceptRecord",isAcceptRecord);
		setField("hdnIsFreshRecord",!isAcceptRecord);
			
		document.forms[0].action ="showAdjustmentPayment.action";
		document.forms[0].submit();
	}
	
	function setFormFields() { 
		setField("txtAmount",arrFormData[24]);
		setField("txtRemarks",arrFormData[25]);
		setField("txtReciptNo",arrFormData[26]);
		setField("selType",arrFormData[27]);
	}
	
	function reSetFormFields() { 
		setField("txtAmount","");
		setField("txtRemarks","");
		setField("txtReciptNo","");
		setField("selType","SELECT");
	}
	
	function raiseWarningForDuplicateRecords(){		
		setFormFields();
		confirmClick(false);				
		isExactMatchExists = "";
		isDuplicateAgentAmountCombinationExists = "";
		if(isDuplicateAgentAmountCombinationExists == "" && isExactMatchExists == ""){
			reSetFormFields();
		}
	}

	function setPageEdited(isEdited) {
		top.pageEdited = isEdited;
	}
	
	function datachange() {
		setPageEdited(true);
	}
	
	function closeClick() {
		if (loadCheck()) {
			setPageEdited(false);
			opener.searchClick();
			window.close();
		}
	}

	function loadCheck() {
		if (top.pageEdited) {
			return confirm("Changes will be lost! Do you wish to Continue?");
		} else {
			return true;
		}
	}

	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.blnDragCalendar = true;
	objCal1.buildCalendar();
	
	function setDate(strDate, strID){
		switch (strID){
			case "0" : setField("txtDStart", strDate); setPageEdited(true); break;
			case "1" : setField("txtDStop", strDate); setPageEdited(true); break;
		}
	}
	
	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 10;
		if (strID == "0"){
			objCal1.left = 0;
		}else if (strID == "1"){
			objCal1.left = 220;
		}
		objCal1.showCalendar(objEvent);
		objCal1.onClick = "setDate";
	}
	
	function searchClick() {
		if(trim(getFieldByID("txtDStart").value) == "") {
			validateError(arrError["startnull"]);
			getFieldByID("txtDStart").focus();
			return;
		}
		if(trim(getFieldByID("txtDStop").value) == "") {
			validateError(arrError["stopnull"]);
			getFieldByID("txtDStop").focus();
			return;
		}
		if (!dateChk("txtDStart")){
			validateError(arrError["invaliddate"]);
			getFieldByID("txtDStart").focus();
			return;
		}
		if (!dateChk("txtDStop")){
			validateError(arrError["invaliddate"]);
			getFieldByID("txtDStop").focus();
			return;
		}
		
		var from = getFieldByID("txtDStart").value;
		var to = getFieldByID("txtDStop").value;
		if( (CheckDates(to,from)) && !(CheckDates(to,from) && CheckDates(from,to))) {			
			validateError(arrError["shldbegreater"]);
			getFieldByID("txtDStop").focus();
			return;		
		}
		if(getFieldByID("chkTypeR").checked 
			|| (getFieldByID("chkTypeC") && getFieldByID("chkTypeC").checked)
			|| (getFieldByID("chkTypeD") && getFieldByID("chkTypeD").checked) 
			|| (getFieldByID("chkTypeRR") && getFieldByID("chkTypeRR").checked)
			|| getFieldByID("chkTypeA").checked
			|| (getFieldByID("chkTypeQ") && getFieldByID("chkTypeQ").checked) 
			|| (getFieldByID("chkTypeRV") && getFieldByID("chkTypeRV").checked) 
			|| getFieldByID("chkTypeCG").checked
			|| getFieldByID("chkTypeCR").checked
			|| getFieldByID("chkTypeBSP").checked 
			|| getFieldByID("chkTypeAO").checked
			|| getFieldByID("chkTypeCCD").checked ) {  
		}else {
			validateError(arrError["checkthebox"]);
			getFieldByID("chkTypeR").focus();
			return;
		}
		setField("hdnMode","showAdjustPayment");
		setField("hdnCode", arrFormData[0]);
		setField("hdnCredit", arrFormData[1]);
		setField("hdnSelectedCarriers",getValue("searchCarrier"));
		
		
		setField("hdnAgentID",arrFormData[0]);
		setField("hdnAgentName",arrFormData[5]);
		setField("hdnCreditLimit",arrFormData[1]);
		setField("hdnFromPage","FROMADJUST");
				
		document.forms[0].action ="showAdjustmentPayment.action";
		document.forms[0].submit();	
	}

	function validateError(msg) {
		objMsg.MessageText = msg;
		objMsg.MessageType = "Error" ; 
		ShowPageMessage();
	}
	
	function validateTA(objControl, length) {
				
		var strValue = objControl.value;
		var strLength = strValue.length;
		var blnVal = isEmpty(strValue);
		if(blnVal){
			setField("txtRemarks",strValue.substr(0,strLength-1)); 
			getFieldByID("txtRemarks").focus();
		}
		if (strLength > length)	{
			strValue = strValue.substr(0,length);
			validateTextArea(objControl);
			objControl.value = strValue;
		}
	}
	
	function ShowProgress(){
		setVisible("divLoadMsg", true);
	}
	
	function HideProgress(){
		setVisible("divLoadMsg", false);
	}
	function payCurrType(){
		var htmC = arrFormData[17];
		if(htmC != '' && arrFormData[16] != 'B'){
			DivWrite("spnCurrCode", htmC);	
		}else{
			DivWrite("spnCurrCode", baseCurrency);	
		}
	}
	
	function populateCurrCombo(){
		var htm='';
		if(arrFormData[16]=='L'){
			setField("selCurrIn","L"); 
			htm+= arrFormData[17];
		}else{
			setField("selCurrIn","B"); 
			htm+= baseCurrency;
		}
		DivWrite("spnCurr", htm);	
	}
	
	function adjustmentTypeChange() {
		
		var adjustmentType = trim(getText("selType")); 
		
		$("#txtAmount").unbind('keypress');
		if(adjustmentType == 'BSP') {
			$("#txtAmount").bind('keypress', function(e) {
				return isValidNumericChar({allowDecimal:true, allowNegative:true}, e);			
			});
		} else {
			$("#txtAmount").bind('keypress', function(e) {
				return isValidNumericChar({allowDecimal:true, allowNegative:false}, e);			
			});
		}
		
		setField("txtAmount",'');
		setField("txtRemarks",'');
		
		datachange();
	}
	
	function rowClick(strRowNo){
		
		$("#txtAmount").val("");
		$("#txtReciptNo").val("");
		$("#hdnOriginTnxId").val("");
		$("#selCarrier").val("SELECT");
		$("#selType").val("SELECT");
		$("#txtRemarks").val("");
		$("#txtAmount").prop('readonly', false);
		$("#txtReciptNo").prop('readonly', false);
		$("#selCarrier").prop('disabled', false);
		$("#selType").prop('disabled', false);
		$("#txtRemarks").prop('readonly', false);
		
		$("#hdnOriginTnxId").val("");
		
		isReverseOperation = false;
		creditNotReversed = true;
			
		var dataRowNum = parseInt(strRowNo);
		var dataArray = parent.arrAdjData[dataRowNum];
		
		if(enableTACreditReverse == "true" && dataArray[10] == "CR"){
			
			var creditReversed = dataArray[9];
			creditNotReversed = (creditReversed == "false");
			isReverseOperation = true;
			$("#txtAmount").val(dataArray[3]);
			$("#txtReciptNo").val("RE:" + dataArray[8]);
			$("#selCarrier").val(dataArray[7]);
			$("#hdnOriginTnxId").val(dataArray[5].split(" ")[1]);
			$("#selType").val("RVCRE");
			$("#txtRemarks").val("Reverse Entry : "+ dataArray[4]);
			$("#txtAmount").prop('readonly', true);
			$("#txtReciptNo").prop('readonly', true);
			$("#selCarrier").prop('disabled', true);
			$("#selType").prop('disabled', true);
			$("#txtRemarks").prop('readonly', true);
	
		}
	}
	
	$("#selType").on('change', function() {
		if (this.value == "RVCRE") {
			$("#txtAmount").prop('readonly', true);
			$("#txtReciptNo").prop('readonly', true);
			$("#selCarrier").prop('disabled', true);
			$("#txtRemarks").prop('readonly', true);
		} else {
			$("#txtAmount").prop('readonly', false);
			$("#txtReciptNo").prop('readonly', false);
			$("#selCarrier").prop('disabled', false);
			$("#txtRemarks").prop('readonly', false);
		}
	});

	