
function AgentSelfTopup(){}
	function CHOnLoad(strMsg, strMsgType) {
		ShowProgress();
		if (strMsg != null && strMsgType == "Error") {
			showWindowCommonError(strMsgType, strMsg);
		}
		enableFields();
		if(arrFormData) {
			var numCreditBalLocal = parseFloat(arrFormData[12]);
			var numCreditBalBase = parseFloat(arrFormData[2]);
			var creditLocal = "(";
			var creditBase = "";
			
			var displayCurrency = baseCurrency;
			if(arrFormData[6] == "L"){
				if(numCreditBalBase != 0.00){
					creditBase = creditBase + baseCurrency + " " + CurrencyFormat(numCreditBalBase, arrFormData[17]) ;
				} else {
					creditBase = creditBase + baseCurrency + " 0.00" ;
				}
				if(numCreditBalLocal != 0.00){
					creditLocal = creditLocal + arrFormData[7] + " " + CurrencyFormat(numCreditBalLocal,arrFormData[16]) + ")";
					DivWrite("spnAgentCurrAvailBal",creditLocal);
				}
				displayCurrency= arrFormData[7];
			} else if(arrFormData[6] == "B"){
				if(numCreditBalBase != 0.00){
					creditBase = creditBase + baseCurrency + " " + CurrencyFormat(numCreditBalBase, arrFormData[17]) ;
				} else {
					creditBase = creditBase + baseCurrency + " 0.00" ;
				}
			}
			
			var minimimAmount = arrFormData[18];
			setVisible("trMinimumTopup", false);
			
			if(minimimAmount != "0.00") {
			   setVisible("trMinimumTopup", true);
			   DivWrite("spnMinimimTopup",minimimAmount +" " + displayCurrency);
			}
			
	  	DivWrite("spnBaseCurrAvailBal",creditBase);
					
		}
		populateCurrCombo();
		payCurrType();
		HideProgress();		
		//show popup
		if (trim(popupmessage).length != 0) {
			alert(popupmessage);
		}				
		$("#txtAmount").bind('keypress', function(e) {
			return isValidNumericChar({allowDecimal:true, allowNegative:false}, e);			
		});
	}

	function validateError(msg) {
		objMsg.MessageText = msg;
		objMsg.MessageType = "Error" ; 
		ShowPageMessage();
	}
	
	function validateDecimal(obj) {
		setPageEdited(true);
		var strCR = getFieldByID(obj).value;
		var strLen = strCR.length;
		var blnVal = currencyValidate(strCR, 10,2);
		var wholeNumber;
		if(!blnVal){
			if (strCR.indexOf(".") != -1) {
				wholeNumber = strCR.substr(0, strCR.indexOf("."));
				if (wholeNumber.length > 13) {
					setField(obj, strCR.substr(0, wholeNumber.length - 1)); 	
				} else {
					setField(obj,strCR.substr(0, strLen-1)); 		
				}
			} else {
				setField(obj, strCR.substr(0, strLen - 1)); 		
			}	
			getFieldByID(obj).focus();
		}
	}

	function confirmClick(isFreshRecord) {
		var amt = trim(getText("txtAmount"));
		var poolamt = trim(arrFormData[7]);
		var agentCode  = arrFormData[5];
		
		if (isEmpty(getText("txtAmount"))) {
			validateError(arrError["amountnull"]);
			getFieldByID("txtAmount").focus();
			return false;
		}
		if (parseFloat(amt) == 0.00) {
			validateError(arrError["amountnull"]);		
			getFieldByID("txtAmount").focus();
			return false;
		}	       
	
		if (isEmpty(trim(getText("txtRemarks")))) {
			validateError(arrError["remarksnull"]);		
			getFieldByID("txtRemarks").focus();		
			return false;
		} 
		if (isEmpty(getText("selPaymentGateway")) || trim(getText("selPaymentGateway")) == "Select") {
			validateError(arrError["emptyPaymentMethod"]);
			getFieldByID("selPaymentGateway").focus();
			return false;
		}
		
		setPageEdited(false);	
		setField("selPaymentGateway", trim(getText("selPaymentGateway")));
		setField("txtRemarks", trim(getText("txtRemarks")));
		setField("txtAmount", trim(getText("txtAmount")));
		setField("hdnMode","topup");
		setField("contactInfo.firstName", arrFormData[1]);
		setField("contactInfo.email", arrFormData[13]);
		setField("contactInfo.phoneNo", arrFormData[14]);
		setField("contactInfo.mobileNo", arrFormData[15]);
		disableFields();
		ShowProgress();
		redirectToExternalPGW();
	}

	redirectToExternalPGW = function(){
		var data = {};
    data["pgwId"]=$("#selPaymentGateway").val();
    var contactInfo = $('input').filterData('name','contactInfo');
    data = $.extend(data,contactInfo);
    data['amount']  = trim(getText("txtAmount"));   
    data["remarks"]       = trim(getText("txtRemarks"));
    if(arrFormData[6] != 'B'){  
      data['selectedCurrency'] = arrFormData[7];
    }else{
      data['selectedCurrency'] = baseCurrency;
    }
		UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'generateCCTopupEPGWFormRequest.action', dataType: 'json', data:data,
			success:getRedirectRequestDataSuccess, 
			error:UI_commonSystem.setErrorStatus});
	}
	
	getRedirectRequestDataSuccess = function(response){
		HideProgress();
		
		if(response.success){
		  if(response.redirectionMechanism == "FORM"){
	      setField("hdnMode","extPGW");
	      top.objCW = window.open('showFile!dummyPage.action',
	          "myWindow",
	      "toolbar=no,scrollbars=no,resizable=no,top=10,left=10,width=800,height=800");
	      if(!top.objCW || top.objCW.closed || typeof top.objCW.closed=='undefined'){
	        UI_commonSystem.hideProgress();
	        showERRMessage("Popup Blocked. Please Enable and Check");
	      } else {
	        popUpManuallyClosed = false;
	        top.objCW.document.write('<div id="content">'+response.redirectionString+'</div>');                 
	      }
	    } else if(response.redirectionMechanism == "URL"){
	      popUpManuallyClosed = false;
	      top.objCW = window.open(response.redirectionString,
	          "_blank",
	      "toolbar=no,scrollbars=no,resizable=no,top=10,left=10,width=800,height=800");
	    } else if(response.redirectionMechanism == "FORMSUBMIT"){
	      top.objCW = window.open('showFile!dummyPage.action',"myWindow","toolbar=no,scrollbars=no,resizable=no,top=10,left=10,width=800,height=800");
	      if (window.focus) {
	        top.objCW.focus();
	      }
	      popUpManuallyClosed = false;
	      top.objCW.document.write('<div id="iframeContent"><iframe src="about:blank" id="frmEPGW" name="frmEPGW"  frameborder="0" scrolling="yes" style="position:absolute; width:100%; height:100%;"></iframe></div>');
	      top.objCW.document.write('<div id="content">'+response.redirectionString+'</div>');
	      top.objCW.document.getElementsByTagName("form")[0].setAttribute("target", "frmEPGW");     
	      top.objCW.document.forms[0].submit();
	      
	    }
	    var timer = setInterval(function() { 
	      if(top.objCW.closed && !popUpManuallyClosed) {
	        clearInterval(timer);
	        externalPGWPageClosed();
	      }
	    }, 1000); 
		  
		}else{
		  
		  var errorMessage= response['messageTxt'];
		  if(response.minimumTopupAmount!= null && response.selectedCurrency != null ){
		       errorMessage = errorMessage + " " + response.minimumTopupAmount + " " + response.selectedCurrency;
		   }
		    enableFields();
			  showCommonError("ERROR",  errorMessage);

		}
		
	}

	externalPGWPageClosed = function(){
		HideProgress();
		enableFields();
	}
	
	externalPaymentGatewayFeedback = function(){
		if(!top.objCW.closed){
			top.objCW.close();
		}
		popUpManuallyClosed = true;
        HideProgress();        
        payConfirmExecute();
	}
		
	payConfirmExecute = function(){
		enableFields();
		setField("hdnMode","saveSuccess");
		document.forms[0].action ="agentSelfTopup.action";
		document.forms[0].submit();
	}

	function setPageEdited(isEdited) {
		top.pageEdited = isEdited;
	}
	
	function datachange() {
		setPageEdited(true);
	}
	
	function closeClick() {
		top.LoadHome();
	}

	function loadCheck() {
		if (top.pageEdited) {
			return confirm("Changes will be lost! Do you wish to Continue?");
		} else {
			return true;
		}
	}

	enableFields = function(){
		getFieldByID("btnTopup").disabled = false;
		getFieldByID("btnClose").disabled = false;
		getFieldByID("txtAmount").disabled = false;
		getFieldByID("txtRemarks").disabled = false;
		getFieldByID("selPaymentGateway").disabled = false;
	}
	
	disableFields = function(){
		getFieldByID("btnTopup").disabled = true;
		getFieldByID("btnClose").disabled = true;
		getFieldByID("txtAmount").disabled = true;
		getFieldByID("txtRemarks").disabled = true;
		getFieldByID("selPaymentGateway").disabled = true;
	}
	
	function validateError(msg) {
		objMsg.MessageText = msg;
		objMsg.MessageType = "Error" ; 
		ShowPageMessage();
	}
	
	function validateTA(objControl, length) {
				
		var strValue = objControl.value;
		var strLength = strValue.length;
		var blnVal = isEmpty(strValue);
		if(blnVal){
			setField("txtRemarks",strValue.substr(0,strLength-1)); 
			getFieldByID("txtRemarks").focus();
		}
		if (strLength > length)	{
			strValue = strValue.substr(0,length);
			validateTextArea(objControl);
			objControl.value = strValue;
		}
	}
	
	function payCurrType(){
		var htmC = arrFormData[7];
		if(htmC != '' && arrFormData[6] != 'B'){
			DivWrite("spnCurrCode", htmC);	
		}else{
			DivWrite("spnCurrCode", baseCurrency);	
		}
	}
	
	function populateCurrCombo(){
		var htm='';
		if(arrFormData[6]=='L'){
			setField("selCurrIn","L"); 
			htm+= arrFormData[7];
		}else{
			setField("selCurrIn","B"); 
			htm+= baseCurrency;
		}
		DivWrite("spnCurr", htm);	
	}
	function ShowProgress(){
		setVisible("divLoadMsg", true);
	}
	
	function HideProgress(){
		setVisible("divLoadMsg", false);
	}
	