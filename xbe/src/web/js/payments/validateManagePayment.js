//Javascript Document
//Author -Shakir 
adjustPayClick
var strGridRow;
var intStat=0;
var strRowData;
var availableForInvSet;
var availableAsAdv;
var objWindow;
var falgGSA = false;
var screenId = "SC_FACM_001";

function winOnLoad(strMsg,strMsgType) {	
	
    falgGSA = false;
	
	if (hasPriviGSA != undefined && hasPriviGSA == true) {
			setVisible("optGSA", true);
			setVisible("gsaText", true);
			setVisible("optSubGSA", true);
			setVisible("sgsaText", true);
	} else {
		setVisible("optGSA", false);
		setVisible("gsaText", false);
		setVisible("optSubGSA", true);
		setVisible("sgsaText", true);
	}
	if (searchGSA != undefined && searchGSA == true) {
		setVisible("optGSA", true);
		setVisible("gsaText", true);
		setVisible("optSubGSA", true);
		setVisible("sgsaText", true);
	} 
	objWindow = top[0].objWindow;		
	setSearchCriteria();	
	Disable("btnAdjustPay",true);
		
	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved");				
		}
	}	
		
	if(strMsg != null && strMsgType != null)
		showCommonError(strMsgType,strMsg);
	
	if (defaultAirlineCode != "") {
		document.getElementById("spnAirlineCode").innerHTML=defaultAirlineCode;
		$("#spnAirlineCode").show();
	} else {
		$("#spnAirlineCode").hide();
	}
	
	setDisplay("GSAV2", true);
	if (hasPriviForViewPayOnly != undefined && hasPriviForViewPayOnly == true) {
		document.getElementById('btnAdjustPay').value = "View Payment";
	}
	
	focusChildWindow();	
}

function focusChildWindow() {
	objWindow = top[0].objWindow;
	if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
		objWindow.focus();
	}
}

function setSearchCriteria() {
	var strSearchTemp = top[1].objTMenu.tabGetValue("SC_FACM_001") ; // top[0].manifestSearchValue; //
	if ((strSearchTemp != "") && (strSearchTemp != null)) {
		var strSearch = strSearchTemp.split(",");
		setField("hdnRecNo", strSearch[0]);
		setField("selAgentType", strSearch[1]);
		chkClick();			
		setField("txtAgentName", strSearch[2]);	
		if (defaultAirlineCode != "") 
			setField("txtAgentCode", strSearch[3].substr(3));
		else
			setField("txtAgentCode", strSearch[3]);
		setField("txtAgentIataCode",strSearch[4]);
	} 
}

function rowClick(strRowNo){
	if (top.loadCheck(top.pageEdited)) {
		Disable("btnAdjustPay",false);
		try{
			availableForInvSet = objDG.getCellValue(strRowNo,"8");//DEFECT FIX AARESAA-2338
			availableAsAdv = objDG.getCellValue(strRowNo,"7");
			objOnFocus();
			intStat=1;
			setPageEdited(false);
			strRowData = objDG.getRowValue(strRowNo);
			strGridRow=strRowNo;
			setField("AgentCode",strRowData[0]);			
			checkForSA(strRowData[0]);		
			copyData();
			setFields(strGridRow);
		}catch(Exception){}
	}
}

function checkForSA(agentCode) {
 if( agentCode != undefined && searchGSA != undefined && searchGSA == true && 
	gNameNCode!="" && gNameNCode.indexOf("^") > 0 && trim(agentCode) == trim(gNameNCode.split("^")[1]) ) {
	   
	   frames["frmTabs"].getFieldByID("btnSave").disabled = true;
	   falgGSA = true;
	} 

}

function setFields(strRowNo){
	var objFrm = frames["frmTabs"];
	objFrm.DivWrite("spnAgentId",strRowData[1]);	
}

function copyData(){
	try{
		frames["frmTabs"].setField("hdnAgentCode",strRowData[0]);
		frames["frmTabs"].setField("hdnAgentCurr",strRowData[8]);
		frames["frmTabs"].document.forms["frmInvoicesettlement"].submit();
		if(frames["frmTabs"].objDG.loaded){
			frames["frmTabs"].DivWrite("spnAgentId",strRowData[1]);
			frames["frmTabs"].showGrid();
		}
	}catch(Exception){}
	
}

function gridNavigations(intRecNo, intErrMsg){
	if (top.loadCheck(top.pageEdited)) {
		objOnFocus();		
		setField("hdnRecNo",intRecNo);
		//top[0].intLastRec = intRecNo;
		
		var strTempStr = "";
		strTempStr = getFieldByID("txtAgentName").value;
		var strTempStr2 = getFieldByID("txtAgentCode").value;
		if (strTempStr2 != null && strTempStr2 != "") {
			strTempStr2 = defaultAirlineCode + strTempStr2;
		}
		var strTempStr3 = getFieldByID("txtAgentIataCode").value;
		var strTemp =getText("selAgentType")+","+strTempStr+","+strTempStr2+","+strTempStr3;
		
		setField("hdnSearchCriteria",strTemp);
		setTabSearch();

		setField("hdnMode","SEARCH");
		if (intErrMsg != ""){
			top[2].objMsg.MessageText = "<li> " + intErrMsg ;
			top[2].objMsg.MessageType = "Error" ; 
			top[2].ShowPageMessage();
		}else{
			if (top.loadCheck(top.pageEdited)) {
				setPageEdited(false);
				setField("hdnAgentName", getFieldByID("txtAgentName").value);
				setField("hdnAgCode", getFieldByID("txtAgentCode").value);
				setField("hdnAgentIataCode",getFieldByID("txtAgentIataCode").value);
				
				document.forms[0].target = "_self";
				document.forms[0].action = "showManagePayment.action";
				document.forms[0].submit();
				ShowProgress();
			}
		}
	}
}

function searchClick(){
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		objOnFocus();
		var blnGoAhead=true;			
		setField("hdnRecNo","1");
		setField("hdnMode", "SEARCH");
			
		var strTempStr = "";
		strTempStr = getFieldByID("txtAgentName").value;
		var strTempStr2 = getFieldByID("txtAgentCode").value;
		if (strTempStr2 != null && strTempStr2 != "") {
			strTempStr2 = defaultAirlineCode + strTempStr2;
		}
		var strTempStr3 = getFieldByID("txtAgentIataCode").value;
		var strTemp =getText("selAgentType")+","+strTempStr+","+strTempStr2+","+strTempStr3;
		setField("hdnSearchCriteria",strTemp);
		setField("hdnAgentName", getFieldByID("txtAgentName").value);	
		setField("hdnAgCode", strTempStr2);	
		setField("hdnAgentIataCode",strTempStr3);
			
		setTabSearch();			
		document.forms[0].target = "_self";
		document.forms[0].action = "showManagePayment.action";
		document.forms[0].submit();	
		ShowProgress();		
	}
}

  function chkClick() {
	
	setField("txtAgentName", "");
	setField("txtAgentCode", "");
	setField("txtAgentIataCode","");
	getFieldByID("txtAgentName").focus();
}

function chkClick1() { 

    setField("txtAgentName", "");
    setField("txtAgentCode", "");
    setField("txtAgentIataCode","");
	getFieldByID("txtAgentName").focus();
	
  	if(getFieldByID("optGSA").checked && searchGSA != undefined && searchGSA == true && 
	gNameNCode!="" && gNameNCode.indexOf("^") > 0 ) {
	setField("txtAgentName",gNameNCode.split("^")[0]);
	setField("txtAgentCode", gNameNCode.split("^")[1]);
	searchClick();
	
	}	

}

function objOnFocus(){
	top[2].HidePageMessage();
}

function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}

function saveInvSettle(){
	setPageEdited(false);
	objOnFocus();
	top[0].TAMode = "INVSET"	
	//setField("hdnRecNo","1");
	setField("hdnAction","INVSET");
	setField("hdnMode","SAVE");

	var strTempStr = "";
	strTempStr = getFieldByID("txtAgentName").value;
	var strTempStr2 = getFieldByID("txtAgentCode").value;
	if (strTempStr2 != null && strTempStr2 != "") {
		strTempStr2 = defaultAirlineCode + strTempStr2;
	}
	var strTempStr3 = getFieldByID("txtAgentIataCode").value;
	var strTemp =getText("selAgentType")+","+strTempStr+","+strTempStr2+","+strTempStr3;
	setField("hdnSearchCriteria",strTemp);
	setField("hdnAgentName", getFieldByID("txtAgentName").value);	
	setField("hdnAgCode", strTempStr2);	
	setField("hdnAgentIataCode",strTempStr3);
	
	setTabSearch();
	document.forms[0].target="_self";
	document.forms[0].action ="showManagePayment.action";
	document.forms[0].submit();
}

function setTabSearch(){
	var setValue = getText("hdnRecNo") + "," + getText("hdnSearchCriteria");
	//top[0].manifestSearchValue = setValue;
	top[1].objTMenu.tabSetValue("SC_FACM_001", setValue);
	//top[0].intLastRec = getText("hdnRecNo");
}

function adjustPayClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		if(intStat==1){			
			setField("hdnMode","showAdjustPayment");
			setField("hdnAgentID",strRowData[0]);
			setField("hdnAgentName",strRowData[1]);
			setField("hdnCreditLimit",strRowData[2]);
			setField("hdnFromPage","FROMPAYMENT");			
			setField("hdnAgCode",strRowData[0]);
	
			closeChildWindow();
			
			var intLeft = (1024 - window.screen.width) /2;
			var intTop = (768 - window.screen.height) /2;
			
			if (hasPriviForViewPayOnly != undefined && hasPriviForViewPayOnly == true) {
				var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=590,resizable=no,top=' + intTop + ',left=' + intLeft
			}else{
				var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=700,resizable=no,top=' + intTop + ',left=' + intLeft	
			}
	
			objWindow = window.open("about:blank","CWindow",strProp);
			top[0].objWindow = objWindow;
			
			var objForm  = document.getElementById("frmManagePayment");
			objForm.target = "CWindow";
	//
			objForm.action = "showAdjustmentPayment.action";
			objForm.submit();	
		}
	}
}

function closeChildWindow() {
	objWindow = top[0].objWindow;
	if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
		objWindow.close();
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
		top[0].objWindow.close();
	}
	closeChildWindow();
}
