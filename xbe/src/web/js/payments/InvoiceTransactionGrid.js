	var screenId = "SC_FACM_005";
	var recstNo = top[0].intLastRec;
	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "25%";
	objCol1.arrayIndex = 0;
	//objCol1.toolTip = "Invoice Number" ;
	objCol1.headerText = "Invoice Number";
	objCol1.itemAlign = "left"
	//objCol1.sort = "true"
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "25%";
	objCol2.arrayIndex = 1;
	//objCol2.toolTip = "Invoice Date" ;
	objCol2.headerText = "Invoice Date";
	objCol2.itemAlign = "right"
	//objCol2.sort = "true"
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "25%";
	objCol3.arrayIndex = 2;
	//objCol3.toolTip = "PNR" ;
	objCol3.headerText = "PNR";
	objCol3.itemAlign = "center"
	
	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "25%";
	objCol4.arrayIndex = 3;
	//objCol3.toolTip = "Amount" ;
	objCol4.headerText = "Amount";
	objCol4.itemAlign = "right"	

	
	// ---------------- Grid	
	var objDG = new DataGrid("spnInvoiceTrans");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);

	objDG.width = "100%";
	objDG.height = "165px";
	objDG.headerBold = false;
	objDG.rowSelect = true;
	objDG.arrGridData = arrInvData;
	objDG.seqNo = true;
	objDG.seqStartNo = recstNo;  //Grid
	objDG.paging = true
	objDG.pgnumRecPage = 20;
	objDG.pgonClick = "";	
	objDG.rowClick = "rowClick";
	objDG.displayGrid();
	
	

	