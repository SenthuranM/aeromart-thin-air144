
	var recNumPerPage = 20;
	var recstNo = top[0].intLastRec;
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "12%";
	objCol2.arrayIndex = 1;
	objCol2.toolTip = "Flight No" ;
	objCol2.headerText = "Flight<br>No";
	objCol2.itemAlign = "center";
	objCol2.sort = true;
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "12%";
	objCol3.arrayIndex = 2;
	objCol3.toolTip = "Air port" ;
	objCol3.headerText = "Airport";
	objCol3.itemAlign = "center";
	objCol3.sort = true;
	
	
	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "12%";
	objCol4.arrayIndex = 3;
	objCol4.toolTip = "PNL Departure Gap" ;
	objCol4.headerText = "PNL Departure Gap";
	objCol4.itemAlign = "center";
	objCol4.sort = true;
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "12%";
	objCol5.arrayIndex = 4;
	objCol5.toolTip = "ADL Time Interval" ;
	objCol5.headerText = "ADL Time Interval";
	objCol5.itemAlign = "center";
	objCol5.sort = true;
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "12%";
	objCol6.arrayIndex = 5;
	objCol6.toolTip = "Start Date" ;
	objCol6.headerText = "Start Date";
	objCol6.itemAlign = "center";
	objCol6.sort = true;
		
	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "12%";
	objCol7.arrayIndex = 6;
	objCol7.toolTip = "End Date" ;
	objCol7.headerText = "End Date";
	objCol7.itemAlign = "center";	
	
	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "13%";
	objCol8.arrayIndex = 7;
	objCol8.toolTip = "Status" ;
	objCol8.headerText = "Status";
	objCol8.itemAlign = "center";
	objCol8.sort = true;
	
	var objCol9 = new DGColumn();
	objCol9.columnType = "label";
	objCol9.width = "12%";
	objCol9.arrayIndex = 11;
	objCol9.toolTip = "Last ADL Gap" ;
	objCol9.headerText = "Last ADL Gap";
	objCol9.itemAlign = "center";
//	objCol9.sort = true;	

	
	
	// ---------------- Grid	
	
	var objDG = new DataGrid("spnThruchecks");	
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol9);
	objDG.addColumn(objCol6);
	objDG.addColumn(objCol7);
	objDG.addColumn(objCol8);
	
	
	objDG.width = "99%";
	objDG.height = "190px";
	objDG.headerBold = false;
	objDG.rowSelect = true;
	objDG.arrGridData = thruchkData;
	objDG.seqNo = true;
	objDG.seqNoWidth = "3%";	
	objDG.backGroundColor = "#ECECEC";
	objDG.seqStartNo = recstNo;
	objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
	objDG.paging = true;
	objDG.pgnumRecPage = recNumPerPage;
	objDG.pgonClick = "gridNavigations";	
	objDG.rowClick = "gridClick";
	objDG.displayGrid();
	
	
