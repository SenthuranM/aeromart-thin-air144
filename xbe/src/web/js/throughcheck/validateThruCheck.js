
var invScreenID = "SC_SHDS_002";
var tabValue = getTabValue();
var strGridRow;
var blnCalander = false;
var dataArr = thruchkData;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

	function setDate(strDate, strID){
		switch (strID){
			case "0" : setField("txtFromDate",strDate);break ;
			case "1" : setField("txtToDate",strDate);break ;				
		}
	}
	
	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		if (strID == 0) {
			objCal1.top = 310;
			objCal1.left = 375;
		}else if (strID == 1) {
			objCal1.top = 330;
			objCal1.left = 375;
		}
		objCal1.onClick = "setDate";
		if ((strID == 0) && (!getFieldByID("txtFromDate").disabled)) {
			objCal1.showCalendar(objEvent);		
		} else if ((strID == 1) && (!getFieldByID("txtToDate").disabled)) {
			objCal1.showCalendar(objEvent);
		} else if(blnCalander) {
			objCal1.showCalendar(objEvent);
		}
	}
	
	function dataChanged(){
		setPageEdited(true);
		 objOnFocus();
	}
	
	function setPageEdited(isEdited) {
		top.pageEdited = isEdited;
	}		

	function winOnLoad(strMsg, strMsgType, strWarnMsg) {
		setPageEdited(false);
		clearSearchControls();
		clearInputSection();
		if (getTabValue() != "" && getTabValue() != null ) {
				var strSearch = getTabValue().split(",");
				setField("txtFlightNoSearch",strSearch[1]);
				setField("selFrom",strSearch[0]);			
		}
		disableInputSection(true);
		disableButtons(true);
		disableGridButtons(true);
		
		if(maintainADLAfterCutoff == "false"){
			document.getElementById("adlAfterCutoverTd").style.visibility="hidden";
			document.getElementById("lblADLTimeAfterCutoffTime").style.visibility="hidden";
			document.getElementById("txtADLTimeAfterCutoffTime").style.visibility="hidden";
		}
		
		if(strMsg != null && strMsg != "" && strMsgType != null && strMsgType != "") {
			showCommonError(strMsgType,strMsg);
			
			if(strMsgType == "Error") {
				if (getPnlAdlData() != "" && getPnlAdlData() != null ) {
					var modData = getPnlAdlData().split(",");
					setField("selAirport",modData[0]);
					setField("txtFlightNo",modData[1]);
					setField("txtPNLTime",modData[2]);
					setField("txtADLTime",modData[3]);
					setField("txtFromDate",modData[4]);
					setField("txtToDate",modData[5]);
					setField("txtLstADL",modData[11]);
					setField("txtADLTimeAfterCutoffTime",modData[12]);					
					
					if(modData[6] == "true") {
						document.forms[0].chkStatus.checked = true;
					}else {
						document.forms[0].chkStatus.checked = false;
					}
					
					if(modData[7] == "true") {
						document.forms[0].chkAllFlts.checked = true;
					}else {
						document.forms[0].chkAllFlts.checked = false;
					}					
					setField("hdnVersion",modData[8]);
					setField("hdnThruID",modData[9]);
					disableInputSection(false);
					if(modData[9] != "") {
						Disable("selAirport", true);
					}else if(modData[10] == "DELETE") {
						disableInputSection(true);
					}
					disableButtons(false);
				}
				
			}
		}	
	}
	
	function clearSearchControls() {
		setField("txtFlightNoSearch","");
		setField("selFrom","");			
	}
	
	function disableInputSection(cond) {
		Disable("selAirport", cond);
		Disable("txtFlightNo", cond);
		Disable("txtPNLTime", cond);
		Disable("txtADLTime", cond);
		Disable("txtFromDate", cond);
		Disable("txtToDate", cond);
		Disable("chkStatus", cond);
		Disable("chkAllFlts", cond);
		Disable("txtLstADL", cond);
		Disable("txtADLTimeAfterCutoffTime", cond);
	}
	
	function disableButtons(cond) {
		Disable("btnReset", cond);
		Disable("btnSave", cond);
	}
	
	function disableGridButtons(cond) {
		Disable("btnEdit", cond);
		Disable("btnCancel", cond);
	}
	
	//hide the message
	function objOnFocus(){
		top[2].HidePageMessage();
	}
	
	function isPageEdited() {
		if(top.pageEdited) {
			var cnf =  confirm("Changes will be lost! Do you wish to Continue?");
			return cnf;			
		}else {
			return true;
		
		}
	}


	function SearchData() {
	
		if(SearchValid()==false) return;
		
		top.blnProcessCompleted=true;
		setSearchCriteria();
		setSearchRecNo(1);	
		document.forms[0].hdnMode.value = "SEARCH";		
		document.forms[0].submit();
		ShowProgress();	

	}
	
	function SearchValid(){
		//Mandatory fields		
		if (getFieldByID("selFrom").value == "") {
			showERRMessage(arrError['airportnull']);
			getFieldByID("selFrom").focus();
			return false;
		}else {
			return true;
		}
	}
	
	function getTabValue() {
		var tabValue = top[0].pnladlsearch;	
		if ((tabValue == "") || (tabValue == null)) {
			tabValue = new Array("");
		}
		return tabValue;
	}

	function setSearchCriteria() {
		top[0].pnladlsearch = getFieldByID("selFrom").value+","+getFieldByID("txtFlightNoSearch").value;
	
	}
	
	function getPnlAdlData() {
		var pnlModelvalue = top[0].pnladldata;
		if ((pnlModelvalue == "") || (pnlModelvalue == null)) {
			pnlModelvalue = new Array("");
		}
		return pnlModelvalue;
			
	}
	
	function setPnlAdlData() {
		var strAirPt = getFieldByID("selAirport").value;
		var strFltNo = getFieldByID("txtFlightNo").value;
		var strPnlTime = getFieldByID("txtPNLTime").value;
		var stradltime = getFieldByID("txtADLTime").value;
		var strstDate = getFieldByID("txtFromDate").value;
		var strStopDate = getFieldByID("txtToDate").value;
		var strlstGap = getFieldByID("txtLstADL").value;
		var strStatus =  "";
		var strAllFlts = "";		
		if(document.forms[0].chkStatus.checked) {
			strStatus = "true";
		}else {
			strStatus = "false";
		}
		
		if(document.forms[0].chkAllFlts.checked) {
			strAllFlts = "true";
		}else {
			strAllFlts = "false";
		}
		var strVersion = getFieldByID("hdnVersion").value;
		var strTruId   = getFieldByID("hdnThruID").value;
		var strActMode = getFieldByID("hdnMode").value;
		
		top[0].pnladldata =  strAirPt +"," + strFltNo +"," + strPnlTime +"," + stradltime +"," + strstDate +"," + strStopDate +"," + strStatus +"," + strAllFlts +"," + strVersion +"," + strTruId +","+strActMode+","+strlstGap;
	}
	
	function setSearchRecNo(recNo) {
		top[0].intLastRec = recNo;
	}

	function addClick() {
		 objOnFocus();	
		if (isPageEdited()) {
			disableInputSection(false);
			clearInputSection();
			disableButtons(false);
			setField("hdnMode","ADD");
			document.forms[0].chkStatus.checked = true;
			disableGridButtons(true);
			setPageEdited(false);
		}		
	}
	
	function editClick() {
		 objOnFocus();
		if (isPageEdited()) {
			disableInputSection(false);
			Disable("selAirport", true);
			disableButtons(false);
			setField("hdnMode","EDIT");	
			setPageEdited(false);	
		}	
	}
	
	function clearInputSection() {
		setField("selAirport","");
		setField("txtFlightNo","");
		setField("txtPNLTime","");
		setField("txtADLTime","");
		setField("txtFromDate","");
		setField("txtToDate","");
		setField("txtLstADL","");
		setField("txtADLTimeAfterCutoffTime","");
		
		document.forms[0].chkStatus.checked = false;
		document.forms[0].chkAllFlts.checked = false;
		setField("hdnVersion","");
		setField("hdnThruID","");
	
	}
	
	function cancelClick() {
		 objOnFocus();
		if (isCancelConfirm()) {
			setField("hdnMode","DELETE");
			disableInputSection(false);
			setSearchCriteria();
			setPnlAdlData();
			setPageEdited(false);			
			setField("hdnRecNo", top[0].intLastRec);	
			document.forms[0].hdnMode.value = "DELETE";		
			document.forms[0].submit();
			ShowProgress();
		
		}
	
	}
	
	function isCancelConfirm() {
		 
		return confirm("This will Cancel The selected PNL ADL configuration! Do you wish to Continue?");	
	}
	
	function resetClick() {
		 objOnFocus();
		if(getFieldByID("hdnMode").value == "EDIT") {
			gridClick(strGridRow);
			disableInputSection(false);
			disableButtons(false);
			
		
		}else if(getFieldByID("hdnMode").value == "ADD") {
			addClick();
		}
	
	}
	
	function saveClick() {
		 objOnFocus();	
		if(validateSaveData()) {
			if(getFieldByID("hdnMode").value == "EDIT") {
				Disable("selAirport", false);
			}
			setField("hdnMode","SAVE");
			setSearchCriteria();
			setPnlAdlData();			
			setField("hdnRecNo", top[0].intLastRec);	
			document.forms[0].hdnMode.value = "SAVE";		
			document.forms[0].submit();
			ShowProgress();
		}
	
	}
	
	
	function validateSaveData() {
		
		if((getFieldByID("hdnMode").value == "ADD") && trim(getFieldByID("selAirport").value) == "") {
			showERRMessage(arrError['airportnull']);
			getFieldByID("selAirport").focus();
			return false;
		}
		if((trim(getFieldByID("txtFlightNo").value) == "") && (!document.forms[0].chkAllFlts.checked)) {
			showERRMessage(arrError['noFlight']);
			getFieldByID("txtFlightNo").focus();
			return false;
		} 
		if(trim(getFieldByID("txtPNLTime").value) == "") {
			showERRMessage(arrError['nopnltime']);
			getFieldByID("txtPNLTime").focus();
			return false;
		}
		if(!validateTimeGapString(getFieldByID("txtPNLTime").value)) {
			showERRMessage(arrError['invalidtimegap']);
			getFieldByID("txtPNLTime").focus();
			return false;
		}
		if(trim(getFieldByID("txtADLTime").value) == "") {
			showERRMessage(arrError['noadltime']);
			getFieldByID("txtADLTime").focus();
			return false;
		}
		if(!validateTimeGapString(getFieldByID("txtADLTime").value)) {
			showERRMessage(arrError['invalidtimegap']);
			getFieldByID("txtADLTime").focus();
			return false;
		}
		if(maintainADLAfterCutoff =="true" && !validateTimeGapString(getFieldByID("txtADLTimeAfterCutoffTime").value)) {
			showERRMessage(arrError['invalidtimegap']);
			getFieldByID("txtADLTimeAfterCutoffTime").focus();
			return false;
		}
		if(strMinPNL != "" && Number(strMinPNL) > getTimeInMinutes(getFieldByID("txtPNLTime").value)) {
			showERRMessage(arrError['lessminpnl']+" "+formatMinitues(strMinPNL));
			getFieldByID("txtPNLTime").focus();
			return false;
		}
		
		if(strMinADL != "" && Number(strMinADL) > getTimeInMinutes(getFieldByID("txtADLTime").value)) {
			showERRMessage(arrError['lessminadl']+" "+formatMinitues(strMinADL));
			getFieldByID("txtADLTime").focus();
			return false;
		}
		if(strMinADLAfterCutoff != "" && Number(strMinADLAfterCutoff) > getTimeInMinutes(getFieldByID("txtADLTimeAfterCutoffTime").value)) {
			showERRMessage(arrError['lessminadl']+" "+formatMinitues(strMinADLAfterCutoff));
			getFieldByID("txtADLTimeAfterCutoffTime").focus();
			return false;
		}
		if(getTimeInMinutes(getFieldByID("txtADLTime").value) > Number(strMinPNL)) {
			showERRMessage(arrError['greatmaxadl'] + formatMinitues(strMinPNL));
			getFieldByID("txtADLTime").focus();
			return false;
		} 
		if(getTimeInMinutes(getFieldByID("txtADLTimeAfterCutoffTime").value) > Number(strMinPNL)) {
			showERRMessage(arrError['greatmaxadl'] + formatMinitues(strMinPNL));
			getFieldByID("txtADLTimeAfterCutoffTime").focus();
			return false;
		} 
		if(trim(getFieldByID("txtFromDate").value) == "") {
			showERRMessage(arrError['startdate']);
			getFieldByID("txtFromDate").focus();
			return false;
		} 
		if(trim(getFieldByID("txtToDate").value) == "") {
			showERRMessage(arrError['enddate']);
			getFieldByID("txtToDate").focus();
			return false;
		} 
		if (!dateChk("txtFromDate")){
			showERRMessage(arrError['invaliddate']);
			getFieldByID("txtFromDate").focus();
			return false;
		}
		if (!dateChk("txtToDate")){
			showERRMessage(arrError['invaliddate']);
			getFieldByID("txtToDate").focus();
			return false;
		}
		//compare start date and stop date
		var startD = getFieldByID("txtFromDate").value;
		var stopD = getFieldByID("txtToDate").value;
		var strToday = getFieldByID("hdnToday").value;
		
		if ((getFieldByID("hdnMode").value == "ADD") && !CheckDates(strToday,startD)){
			showERRMessage(arrError['startdategreat']);
			return false;
		}
		
		if (!CheckDates(startD,stopD)){
			showERRMessage(arrError['stopdategreat']);
			return false;
		}
		
		var lastAdlValue =  getFieldByID("txtLstADL").value ;
		
		if(lastAdlValue != null && lastAdlValue != ""){
			if(!validateTimeGapString(lastAdlValue)) {
				showERRMessage(arrError['invalidtimegap']);
				getFieldByID("txtLstADL").focus();
				return false;
			}
		}
		
		return true;
	}
	
	
	function gridNavigations(intRecNo, intErrMsg) {	
		 objOnFocus();
		if (intRecNo <= 0) {
			intRecNo = 1;
		}
		setSearchRecNo(intRecNo);	
		setSearchCriteria()
		setField("hdnRecNo", intRecNo);
		if (intErrMsg != ""){		
			showERRMessage(intErrMsg);
		}else{				
			setPageEdited(false);
			document.forms[0].hdnMode.value="SEARCH";
			document.forms[0].submit();
			ShowProgress();
		
		}
	}

	function closeClock() {	
		 objOnFocus();	
		top.LoadHome();		
	}
	
	function gridClick(strRowNo) {
		 objOnFocus();
		if (isPageEdited()) {
			strGridRow = strRowNo;
			clearInputSection();
			disableInputSection(false);			
			setField("selAirport",dataArr[strGridRow][2]);
			
			if(dataArr[strGridRow][1] == "ALL FLIGHTS") {
				setField("chkAllFlts","true");
			}else {
				setField("txtFlightNo",dataArr[strGridRow][1]);
			}
			
			setField("txtPNLTime",dataArr[strGridRow][3]);
			setField("txtADLTime",dataArr[strGridRow][4]);
			setField("txtFromDate",dataArr[strGridRow][5]);
			setField("txtToDate",dataArr[strGridRow][6]);
			setField("hdnVersion",dataArr[strGridRow][9]);
			setField("hdnThruID",dataArr[strGridRow][10]);
			setField("txtLstADL",dataArr[strGridRow][11]);
			setField("txtADLTimeAfterCutoffTime",dataArr[strGridRow][12]);
			if(dataArr[strGridRow][7] == 'Active') {
				document.forms[0].chkStatus.checked=true;
			}else {
				document.forms[0].chkStatus.checked=false;
			}
			if(dataArr[strGridRow][8] == 'Y') {
				document.forms[0].chkAllFlts.checked=true;
			}else {
				document.forms[0].chkAllFlts.checked=false;
			}
			disableGridButtons(false);
			disableInputSection(true);
			disableButtons(true);
			setPageEdited(false);
		}
		
	}
	
	
	function allSelected() {
		dataChanged();
		if(document.forms[0].chkAllFlts.checked) {
			setField("txtFlightNo","");	
		}
	
	}
	
	function changeCase(varfld,varFlightNo){
		setField(varfld,varFlightNo.value.toUpperCase());
	}
	
	function validateTimeGapString(timeGap){
		// Assumption time Gap string is not null
		//Should be in DD:HH:MM format
		var validated = false;
		var timeGapArray = timeGap.split(":");
		
		if(timeGapArray != null && timeGapArray.length == 3){
			
			var mins = Number(trim(timeGapArray[2]));
			var hrs = Number(trim(timeGapArray[1]));
			var days = Number(trim(timeGapArray[0]));
			
			if(!isNaN(mins)){
				
				if(mins > 59 || mins < 0){
					return false;
				}
				
				if(!isNaN(hrs)){
					
					if(hrs > 23 || hrs < 0){
						return false;
					}
					
					if(isNaN(days)){
						return false;						
					} else {
						return true;
					}
					
				} else {
				
					return false
				}
				
			} else {
				return false;					
			}

		} else {
		
			return false;
		}
		
	}
	/**
	 * Formats time gap strings on blur to DD:HH:MM format
	 * @return
	 */
	function formatOnBlur(element){
		
		var dateStringField = getFieldByID(element);
		
		var data = dateStringField.value;
		var formattedString;
		
		if(data != null){
			
			if(data.indexOf(":") == -1) {
				//add colon to each after 2 charcters
				var dd = "00";
				var hh = "00";
				var mm = "00";
				
				switch(data.length){
					case 1:
						dd = "0" + data;
						formattedString = dd + ":" + hh + ":" + mm;
						break;
					case 2:
						dd = data;
						formattedString = dd + ":" + hh + ":" + mm;
						break;
					case 3:
						dd = data.substring(0,2);
						hh = "0" + data.substring(2,3);
						formattedString = dd + ":" + hh + ":" + mm;
						break;
					case 4:
						dd = data.substring(0,2);
						hh = data.substring(2,4);
						formattedString = dd + ":" + hh + ":" + mm;
						break;
					case 5:
						dd = data.substring(0,2);
						hh = data.substring(2,4);
						mm = "0" + data.substring(4,5);
						formattedString = dd + ":" + hh + ":" + mm;
						break;
					case 6:
						dd = data.substring(0,2);
						hh = data.substring(2,4);
						mm = data.substring(4,6);
						formattedString = dd + ":" + hh + ":" + mm;
						break;
					default:
						//keep as it is, will fail at validations
						formattedString = data;
						break;
					
				}
	
			} else {
				formattedString = data;
			}
			
			//Correct date string if its in correct format dd:hh:mm
			dateStringField.value = correctDateString(formattedString);
			
		}
		
	}
/**
 * Returns time gap in minutes
 * @param timeGapString
 * @return
 */	
function getTimeInMinutes(timeGapString){
	//Data string format to be parsed "DD:HH:MM"
	var dataArray = timeGapString.split(":");
	
	//We parse only the data in correct format
	if(dataArray != null && dataArray.length == 3){
		
		var days = Number(trim(dataArray[0]));
		var hours = Number(trim(dataArray[1]));
		var mins = Number(trim(dataArray[2]));
		
		return (days * 1440 + hours * 60 + mins);
		
	} else {
		
		return false
	}
}

/**
 * Converts input number in minutes to DD:HH:MM
 * @return
 */
function formatMinitues (minutes){
	
	var mins = Number(minutes);
	var days = Math.floor(mins/1440);
	
	var hours = Math.floor((mins%1440)/60);
	
	var mins = (mins%1440)%60;
	
	if(days > 0 && hours > 0 && mins > 0){
		return days + " day(s) " + hours + " hour(s) " + mins + " min(s)";
	} else if (days > 0 && hours > 0){
		return days + " day(s) " + hours + " hour(s)" ;
	} else if (days > 0 && mins > 0){
		return days + " day(s) " + mins + " min(s)";
	} else if (days > 0){
		return days + " day(s) " + mins + " min(s)";
	}else if (hours > 0 && mins > 0){
		return hours + " hour(s) " + mins + " min(s)";
	}else if (hours > 0 ){
		return hours + " hour(s) ";
	}else if (mins > 0){
		return mins + " min(s)";
	} else {
		return days + " day(s) " + hours + " hour(s) " + mins + " min(s)";
	}
	
}

function correctDateString (timeGapString) {
	//We accept only a string in the format dd:hh:mm
	var dataArray = timeGapString.split(":");
	
	//We parse only the data in correct format
	if(dataArray != null && dataArray.length == 3){
		
		var days = Number(trim(dataArray[0]));
		var hours = Number(trim(dataArray[1]));
		var mins = Number(trim(dataArray[2]));
		
		if(!isNaN(days) && !isNaN(hours) && !isNaN(mins)){
			//Rounding
			var correctedMins = mins%60;
			hours = hours + Math.floor(mins/60);
			var correctedHours = hours%24;	
			var correctedDays = (days + Math.floor(hours/24))%100;
			
			if(correctedMins.toString().length == 1){
				correctedMins = "0" + correctedMins;
			}
			if(correctedHours.toString().length == 1){
				correctedHours = "0" + correctedHours;
			}
			if(correctedDays.toString().length == 1){
				correctedDays = "0" + correctedDays;
			}
			
			return  correctedDays + ":" + correctedHours + ":" + correctedMins;
		
		} else {
			return timeGapString;
		}		
		
	} else {
		
		return timeGapString;
	}
	
	
}

	
	

