var objWindow;

function OnLoad() {
	if (graphData != null && graphData != "") {
		createPopUps();
		PopUpData();
		POSenable();
		setGraphContent();

	}
}
function PopUpData() {

	var html = '';
	html += '<div style="margin: 0pt auto; width: 500px;">';
	html += '<div style="position: relative; width: 500px;">';
	html += '<canvas id="chart" width="450" height="450"></canvas>';
	DivWrite("spnPopupData", html);
}

function POSenable() {
	showPopUp(1);
}

function POSdisable() {
	hidePopUp(1);
}

function CloseWindow() {
	top.LoadHome();
}

function setGraphContent() {
	eval(graphData);
}
