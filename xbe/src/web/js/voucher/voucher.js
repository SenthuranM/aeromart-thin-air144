var arrError = new Array();
var arrCurrency = new Array();
var currencyOptions = "";
var saveRequestSent = false;
var cancelRequestSent = false;
var emailRequestSent = false;
var printRequestSent = false;
UI_voucher.currencyList = "";
UI_voucher.baseCurrencyCode = "";

$(document).ready(function() {
	UI_voucher.ready();
});

function UI_voucher() {
}

UI_voucher.ready = function() {
	$("#btnClose").click(function() {
		top.LoadHome();
	});
	$("#voucherIDSrch").numeric();
	$("#issuedAgntSrch").alphaNumeric();
	$("#fnameSrch").alphaWhiteSpace();
	$("#lnameSrch").alphaWhiteSpace();
	$("#voucherId").numeric();
	$("#paxFirstName").alphaWhiteSpace();
	$("#paxLastName").alphaWhiteSpace();

	$("#voucherId").hide();
	$("#lblVoucherId").hide();
	$("#btnSave").hide();
	$("#btnCancel").hide();
	$("#btnEmail").hide();
	$("#btnPrint").hide();
	$("#selLanguage").hide();
	$("#btnCancelConfirm").hide();
	$("#btnReset").hide();
	$("#selLanguage").val("en");
	$("tr.visible").hide();

	$("#validFrom").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "+0:+99",
		minDate : "0Y",
		onSelect: function () {
            var selDate = $('#validFrom').datepicker('getDate');
            $('#validTo').datepicker('option', 'minDate', selDate);
        },
		buttonImageOnly : true,
		buttonText : "Select date",
		changeMonth : true,
		changeYear : true
	});

	$("#validTo").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "+0:+99",
		minDate : "0Y",
		buttonImageOnly : true,
		buttonText : "Select date",
		changeMonth : true,
		changeYear : true
	});

	$("#btnReset").click(function() {
		UI_voucher.formReset();
	});

	$("#btnIssue").click(function() {
		UI_voucher.issueVoucher();
		$("#btnCancel").hide();
		$("tr.visible").hide();
		$("#btnCancelConfirm").hide();
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#selLanguage").hide();
	});

	$("#btnSave").click(function() {
		UI_voucher.saveClick();
	});

	$("#buttonSearch").click(function() {
		UI_voucher.createGrid();

	});

	$("#btnCancel").click(function() {
		$("#btnCancelConfirm").show();
		$("tr.visible").show();
		$("#cancelRemarks").prop('disabled', false);
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#selLanguage").hide();

	});

	$("#btnCancelConfirm").click(function() {
		UI_voucher.cancelVoucher();
	});

	$("#btnEmail").click(function() {
		UI_voucher.emailVoucher();
	});

	$("#btnPrint").click(function() {
		UI_voucher.printVoucher();
	});

	UI_voucher.getVoucherScreenDetails();

	UI_voucher.createGrid();
	
	UI_voucher.formDisable();

}

UI_voucher.getVoucherScreenDetails = function() {
	$.ajax({
		url : 'showVoucherDetail.action',
		dataType : "json",
		beforeSend : ShowPopProgress(),
		type : "POST",

		success : UI_voucher.voucherDetailSuccess
	});
}

UI_voucher.voucherDetailSuccess = function(response) {
	UI_voucher.currencyList = response.currencyList;
	UI_voucher.baseCurrencyCode = response.baseCurrency;
	try {
		eval(response.errorList);
	} catch (err) {
		console.log("eval() error ==> errorList");
	}

	try {
		eval(response.currencyList);
		for (var i = 0; i < arrCurrency.length; i++) {
			currencyOptions += "<option value = '" + arrCurrency[i][0] + "' >"
					+ arrCurrency[i][0] + "</option>"
		}
		$("#currencyCode").html(currencyOptions);
		$("#currencyCode").val(UI_voucher.baseCurrencyCode).change();

	} catch (err) {
		console.log("eval() error ==> currencyList");
	}
	HidePopProgress();
}

UI_voucher.saveClick = function() {
	if (UI_voucher.formValidate() && !saveRequestSent) {
		saveRequestSent = true;
		var data = {};
		data["voucherDTO.validFrom"] = $("#validFrom").val();
		data["voucherDTO.validTo"] = $("#validTo").val();
		data["voucherDTO.amountInLocal"] = $("#amount").val();
		data["voucherDTO.currencyCode"] = $("#currencyCode").val();
		data["voucherDTO.paxFirstName"] = $("#paxFirstName").val();
		data["voucherDTO.paxLastName"] = $("#paxLastName").val();
		data["voucherDTO.email"] = $("#email").val();
		data["voucherDTO.remarks"] = $("#remarks").val();
		$.ajax({
			url : 'generateVoucher.action',
			dataType : "json",
			beforeSend : ShowPopProgress(),
			data : data,
			type : "POST",
			async : true,
			success : UI_voucher.voucherSaveSuccess
		});
	}
}

UI_voucher.voucherSaveSuccess = function() {
	UI_voucher.formReset();
	UI_voucher.formDisable();
	showConfirmation(arrError["saveSuccess"]);
	HidePopProgress();
	saveRequestSent = false;
	UI_voucher.createGrid();
}

UI_voucher.issueVoucher = function() {
	$("#voucherId").hide();
	$("#lblVoucherId").hide();
	$("#btnSave").show();
	$("#btnCancel").show();
	$("#btnReset").show();
	UI_voucher.formReset();
	UI_voucher.formEnable();
}

UI_voucher.formValidate = function() {
	var validFromDate = $("#validFrom").datepicker("getDate");
	var validToDate = $("#validTo").datepicker("getDate");
	var validFrom = $("#validFrom").val();
	var validTo = $("#validTo").val();
	var amount = $("#amount").val();
	var paxFirstName = $("#paxFirstName").val();
	var paxLastName = $("#paxLastName").val();
	var email = $("#email").val();
	var remarks = $("#remarks").val();

	var valid = true;
	// "fnameEmpty","fnameInvalid","lnameEmpty","lnameInvalid","emailInvalid","emailEmpty",
	// "fromDateEmpty","toDateInvalid","periodInvalid","amountInvalid","amountEmpty"

	if (trim(validFrom) == "" || trim(validFrom) == null) {
		showERRMessage(arrError["fromDateEmpty"]);
		valid = false;
	} else if (trim(validTo) == "" || trim(validTo) == null) {
		showERRMessage(arrError["toDateEmpty"]);
		valid = false;
	} else if (validToDate - validFromDate < 0) {
		showERRMessage(arrError["periodInvalid"]);
		valid = false;
	}

	if (trim(amount) == "" || trim(amount) == null) {
		showERRMessage(arrError["amountEmpty"]);
		valid = false;
	} else if (!$.isNumeric(trim(amount))) {
		showERRMessage(arrError["amountInvalid"]);
		valid = false;
	}

	if (trim(paxFirstName) == "" || trim(paxFirstName) == null) {
		showERRMessage(arrError["fnameEmpty"]);
		valid = false;
	} else if (!isAlphaWhiteSpace(trim(paxFirstName))) {
		showERRMessage(arrError["fnameInvalid"]);
		valid = false;
	}

	if (trim(paxLastName) == "" || trim(paxLastName) == null) {
		showERRMessage(arrError["lnameEmpty"]);
		valid = false;
	} else if (!isAlphaWhiteSpace(trim(paxLastName))) {
		showERRMessage(arrError["lnameInvalid"]);
		valid = false;
	}

	if (trim(email) == "" || trim(email) == null) {
		showERRMessage(arrError["emailEmpty"]);
		valid = false;
	} else if (!checkEmail(trim(email))) {
		showERRMessage(arrError["emailInvalid"]);
		valid = false;
	}

	return valid;
}

UI_voucher.formReset = function() {
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amount").val("");
	$("#paxFirstName").val("");
	$("#paxLastName").val("");
	$("#email").val("");
	$("#remarks").val("");
	
	if (UI_voucher.baseCurrencyCode != null && UI_voucher.baseCurrencyCode != "") {
		$("#currencyCode").val(UI_voucher.baseCurrencyCode).change();
	}	
}

UI_voucher.formDisable = function() {
	$("#validFrom").datepicker("option", "disabled", true);
	$("#validTo").datepicker("option", "disabled", true);
	$("#amount").prop('disabled', true);
	$("#paxFirstName").prop('disabled', true);
	$("#paxLastName").prop('disabled', true);
	$("#currencyCode").prop('disabled', true);
	$("#email").prop('disabled', true);
	$("#remarks").prop('disabled', true);
	$("#cancelRemarks").prop('disabled', true);

}

UI_voucher.formEnable = function() {
	$("#validFrom").datepicker("option", "disabled", false);
	$("#validTo").datepicker("option", "disabled", false);
	$("#amount").prop('disabled', false);
	$("#paxFirstName").prop('disabled', false);
	$("#paxLastName").prop('disabled', false);
	$("#currencyCode").prop('disabled', false);
	$("#email").prop('disabled', false);
	$("#remarks").prop('disabled', false);
}

UI_voucher.createGrid = function() {

	var voucherDTOJsonToString = function(cellVal, options, rowObject) {
		return JSON.stringify(cellVal);
	}

	var statusFomatter = function(cellVal, options, rowObject) {
		var treturn = "&nbsp;";
		if (cellVal != null) {
			if (cellVal == "I") {
				treturn = "ISSUED";
			} else if (cellVal == "B") {
				treturn = "BLOCKED";
			} else if (cellVal == "C") {
				treturn = "CANCELED";
			} else if (cellVal == "U") {
				treturn = "USED";
			} else if (cellVal == "E") {
				treturn = "EXPIRED";
			}
		}
		return treturn;
	}

	$("#VoucherDetailsGridContainer")
			.empty()
			.append(
					'<table id="voucherDetailsGrid"></table><div id="voucherDetailsPages"></div>')
	var data = {};
	data['voucherSearchRequest.voucherId'] = $("#voucherIDSrch").val();
	data['voucherSearchRequest.issuedAgent'] = $("#issuedAgntSrch").val();
	data['voucherSearchRequest.email'] = $("#emailSrch").val();
	data['voucherSearchRequest.firstName'] = $("#fnameSrch").val();
	data['voucherSearchRequest.lastName'] = $("#lnameSrch").val();

	var voucherGrid = $("#voucherDetailsGrid").jqGrid(
			{
				datatype : function(postdata) {
					$
							.ajax({
								url : 'searchVoucher.action',
								data : $.extend(postdata, data),
								dataType : "json",
								type : "POST",
								complete : function(jsonData, stat) {
									if (stat == "success") {
										mydata = eval("("
												+ jsonData.responseText + ")");
										if (mydata.msgType != "Error") {
											$.data(voucherGrid[0], "gridData",
													mydata.voucherList);
											voucherGrid[0].addJSONData(mydata);
										} else {
											alert(mydata.message);
										}
										HidePopProgress();
									}
								}
							});
				},
				height : 200,

				colNames : [ 'Voucher ID','Cus. First Name', 'Cus. Last Name', 
				             'Cus. Email','Valid From', 'Valid To', 'Amount',
				             'Amount In Local', 'Currency Code', 'Issued User ID',
				             'Issued Date',  'Voucher Template ID', 'Voucher Status',
				             'Remarks', 'Cancel Remarks', 'Redeemed Amount',
				             'VoucherDTO' ],
				colModel : [ {
					name : 'voucherID',
					index : 'voucherID',
					jsonmap : 'voucherDTO.voucherId',
					width : 70
				},{
					name : 'paxFirstName',
					index : 'paxFirstName',
					jsonmap : 'voucherDTO.paxFirstName',
					width : 90
				},{
					name : 'paxLastName',
					index : 'paxLastName',
					jsonmap : 'voucherDTO.paxLastName',
					width : 90
				}, {
					name : 'email',
					index : 'email',
					jsonmap : 'voucherDTO.email',
					width : 110
				}, {
					name : 'validFrom',
					index : 'validFrom',
					jsonmap : 'voucherDTO.validFrom',
					width : 70,
					sorttype : "string",
					hidden : true
				}, {
					name : 'validTo',
					index : 'validTo',
					jsonmap : 'voucherDTO.validTo',
					width : 70,
					sorttype : "string",
					hidden : true
				}, {
					name : 'amountInBase',
					index : 'amountInBase',
					jsonmap : 'voucherDTO.amountInBase',
					width : 70,
					hidden : true
				}, {
					name : 'amountInLocal',
					index : 'amountInLocal',
					jsonmap : 'voucherDTO.amountInLocal',
					width : 100,
					sorttype : "string"
				}, {
					name : 'currencyCode',
					index : 'currencyCode',
					jsonmap : 'voucherDTO.currencyCode',
					width : 90
				}, {
					name : 'issuedUserId',
					index : 'issuedUserId',
					jsonmap : 'voucherDTO.issuedUserId',
					width : 90
				}, {
					name : 'issuedTime',
					index : 'issuedTime',
					jsonmap : 'voucherDTO.issuedTime',
					hidden : true
				}, {
					name : 'templateId',
					index : 'templateId',
					jsonmap : 'voucherDTO.templateId',
					hidden : true
				}, {
					name : 'status',
					index : 'status',
					jsonmap : 'voucherDTO.status',
					width : 90,
					formatter : statusFomatter
				}, {
					name : 'remarks',
					index : 'remarks',
					jsonmap : 'voucherDTO.remarks',
					hidden : true
				}, {
					name : 'cancelRemarks',
					index : 'cancelRemarks',
					jsonmap : 'voucherDTO.cancelRemarks',
					hidden : true
				}, {
					name : 'redeemdAmount',
					index : 'redeemdAmount',
					jsonmap : 'voucherDTO.redeemdAmount',
					hidden : true
				}, {
					name : 'voucherDTO',
					index : 'voucherDTO',
					jsonmap : 'voucherDTO',
					formatter : voucherDTOJsonToString,
					hidden : true
				} ],
				caption : "Voucher Details",
				rowNum : 20,
				viewRecords : true,
				width : 920,
				pager : jQuery('#voucherDetailsPages'),
				jsonReader : {
					root : "voucherList",
					page : "page",
					total : "total",
					records : "records",
					repeatitems : false,
					id : "0"
				},

			}).navGrid("#voucherDetailsPages", {
		refresh : true,
		edit : false,
		add : false,
		del : false,
		search : false
	});

	$("#voucherDetailsGrid").click(function() {
		UI_voucher.voucherDetailsGridOnClick();
	});
}

UI_voucher.voucherDetailsGridOnClick = function() {

	UI_voucher.formReset();
	$("tr.visible").hide();
	var rowId = $("#voucherDetailsGrid").getGridParam('selrow');
	var rowData = $("#voucherDetailsGrid").getRowData(rowId);

	UI_voucher.populateVoucherDetails(rowData);

}

UI_voucher.populateVoucherDetails = function(rowData) {
	$("#voucherId").show();
	$("#lblVoucherId").show();
	$("#btnSave").hide();
	$("#btnCancelConfirm").hide();
	$("#btnReset").hide();
	UI_voucher.formDisable();
	var voucherDTO = JSON.parse(rowData['voucherDTO']);
	$("#voucherId").val(voucherDTO.voucherId);
	$("#validFrom").val(voucherDTO.validFrom);
	$("#validTo").val(voucherDTO.validTo);
	$("#amount").val(voucherDTO.amountInLocal);
	$("#paxFirstName").val(voucherDTO.paxFirstName);
	$("#paxLastName").val(voucherDTO.paxLastName);
	$("#email").val(voucherDTO.email);
	$("#currencyCode").val(voucherDTO.currencyCode);
	$("#remarks").val(voucherDTO.remarks);
	$("#cancelRemarks").val(voucherDTO.cancelRemarks);

	var status = voucherDTO.status;

	$("#btnCancel").show();
	$("#btnEmail").show();
	$("#btnPrint").show();
	$("#selLanguage").show();

	if (status != "I") {
		$("#btnCancel").hide();
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#selLanguage").hide();
	}
	if (status == "C") {
		$("tr.visible").show();
	}
}

ShowPopProgress = function() {
	setVisible("divLoadMsg", true);
}

HidePopProgress = function() {
	setVisible("divLoadMsg", false);
}

UI_voucher.cancelVoucher = function() {

	var rowId = $("#voucherDetailsGrid").getGridParam('selrow');
	var rowData = $("#voucherDetailsGrid").getRowData(rowId);

	var voucherObject = JSON.parse(rowData['voucherDTO']);
	var status = voucherObject.status;
	var voucherID = voucherObject.voucherId;
	var cancelRemarks = $("#cancelRemarks").val();

	if (UI_voucher.validateCancel(status, cancelRemarks) && !cancelRequestSent) {
		cancelRequestSent = true;
		var data = {};
		data["voucherDTO.voucherId"] = voucherID;
		data["voucherDTO.cancelRemarks"] = cancelRemarks;
		$.ajax({
			url : 'cancelVoucher.action',
			dataType : "json",
			data : data,
			beforeSend : ShowPopProgress(),
			type : "POST",
			async : true,
			success : UI_voucher.voucherUpdateSuccess(voucherID)
		});
		$("#voucherIDSrch").val(voucherID);
		UI_voucher.createGrid();
	}
}

UI_voucher.validateCancel = function(status, cancelRemarks) {

	if (status != "I") {
		showERRMessage(arrError["statusInvalid"]);
		return false;
	} else if (trim(cancelRemarks) == "" || trim(cancelRemarks) == null) {
		showERRMessage(arrError["emptyCancelRemarks"]);
		$("#cancelRemarks").focuss;
		return false;
	} else {
		return true;
	}
}

UI_voucher.voucherUpdateSuccess = function(voucherID) {
	UI_voucher.formReset();
	UI_voucher.formDisable();
	cancelRequestSent = false;
	HidePopProgress ();

	$("#voucherId").hide();
	$("#lblVoucherId").hide();
	$("#btnSave").hide();
	$("#btnCancel").hide();
	$("#btnCancelConfirm").hide();
	$("#btnReset").hide();
	$("#cancelRemarks").val("");

}

UI_voucher.emailVoucher = function() {
	if (!emailRequestSent) {
		emailRequestSent = true;
		var rowId = $("#voucherDetailsGrid").getGridParam('selrow');
		var rowData = $("#voucherDetailsGrid").getRowData(rowId);
		var language = $("#selLanguage").val();
		var voucherObject = JSON.parse(rowData['voucherDTO']);
		var data = {};
		data["voucherDTO.validFrom"] = $("#validFrom").val();
		data["voucherDTO.validTo"] = $("#validTo").val();
		data["voucherDTO.amountInLocal"] = $("#amount").val();
		data["voucherDTO.currencyCode"] = $("#currencyCode").val();
		data["voucherDTO.paxFirstName"] = $("#paxFirstName").val();
		data["voucherDTO.paxLastName"] = $("#paxLastName").val();
		data["voucherDTO.email"] = $("#email").val();
		data["voucherDTO.voucherId"] = $("#voucherId").val();
		data["voucherDTO.templateId"] = voucherObject.templateId;
		data["voucherDTO.printLanguage"] = language;
		data["voucherDTO.remarks"] = $("#remarks").val();
		$.ajax({
			url : 'emailVoucher.action',
			dataType : "json",
			beforeSend : ShowPopProgress(),
			data : data,
			type : "POST",
			async : true,
			success : UI_voucher.voucherEmailSuccess
		});
	}
}

UI_voucher.voucherEmailSuccess = function() {

	emailRequestSent = false;
	HidePopProgress();
	showConfirmation(arrError["emailSuccess"]);
}

UI_voucher.voucherPrintSuccess = function() {

	printRequestSent = false;
	HidePopProgress();
	showConfirmation(arrError["printSuccess"]);

}

UI_voucher.printVoucher = function() {
	if (!printRequestSent) {
		printRequestSent = true;
		var rowId = $("#voucherDetailsGrid").getGridParam('selrow');
		var rowData = $("#voucherDetailsGrid").getRowData(rowId);
		var language = $("#selLanguage").val(); 
		var voucherObject = JSON.parse(rowData['voucherDTO']);
		
		var templateId = voucherObject.templateId;
		var validFrom = $("#validFrom").val();
		var validTo = $("#validTo").val();
		var amountInLocal = $("#amount").val();
		var currencyCode = $("#currencyCode").val();
		var paxFirstName = $("#paxFirstName").val();
		var paxLastName = $("#paxLastName").val();
		var voucherId = $("#voucherId").val();
		

		var strUrl = "printVoucher.action" + "?validFrom=" + validFrom
				+ "&validTo=" + validTo + "&amountInLocal=" + amountInLocal
				+ "&currencyCode=" + currencyCode + "&paxFirstName="
				+ paxFirstName + "&paxLastName=" + paxLastName + "&voucherId="
				+ voucherId + "&templateId=" + templateId + "&languageCode="
				+ language;

		ShowPopProgress(),

		top.objCW = window.open(strUrl, "myWindow", $("#popWindow")
				.getPopWindowProp(630, 900, true));
		if (top.objCW) {
			top.objCW.focus()
		}
		UI_voucher.voucherPrintSuccess();
	}

}