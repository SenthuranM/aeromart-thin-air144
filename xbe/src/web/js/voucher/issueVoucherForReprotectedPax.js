var arrError = new Array();
var reprotectedSelectedPaxList = new Array();

$(document).ready(function() {
	UI_voucherRePax.ready();
});

function UI_voucherRePax() {
}

UI_voucherRePax.ready = function() {	
	
	UI_voucherRePax.getErrorList();
	
	$("#btnIssueVoucher").hide();
	$("#btnReset").hide();
	$("#btnClose").click(function() {
		top.LoadHome();
	});
	
	UI_voucherRePax.formDisable();

	$("#dateSrch").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "+0:+99",
		buttonImageOnly : true,
		buttonText : "Select date",
		changeMonth : true,
		changeYear : true
	});
	
	$("#validFrom").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "+0:+99",
		minDate : "0Y",
		buttonImageOnly : true,
		buttonText : "Select date",
		onSelect: function (date) {
			var date2 = $('#validFrom').datepicker('getDate');
			date2.setDate(date2.getDate() + 1);
			$('#validTo').datepicker('setDate', date2);
			$('#validTo').datepicker('option', 'minDate', date2);
		},
		changeMonth : true,
		changeYear : true
	});

	$("#validTo").datepicker({
		showOn : "button",
		dateFormat : 'dd/mm/yy',
		buttonImage : "../images/Calendar_no_cache.gif",
		yearRange : "+0:+99",
		minDate : "0Y",
		buttonImageOnly : true,
		buttonText : "Select date",
		onClose: function () {
			var dt1 = $('#validFrom').datepicker('getDate');
			var dt2 = $('#validTo').datepicker('getDate');
	            
	        if (dt2 <= dt1) {
	        	var minDate = $('#validTo').datepicker('option', 'minDate');
	        	$('#validTo').datepicker('setDate', minDate);
	        }
	    },
		changeMonth : true,
		changeYear : true
	});

	$("#btnSearch").click(function() {
		if(UI_voucherRePax.validateSearch()){
			UI_voucherRePax.searchRePax();
		}		
	});

	$("#btnIssue").click(function() {
		UI_voucherRePax.getCheckedPaxDetails();
		$("#btnIssueVoucher").show();
		$("#btnReset").show();
	});
	
	$("#btnIssueVoucher").click(function() {
		UI_voucherRePax.issueVoucher();
	});
	
	$("#btnReset").click(function() {
		UI_voucherRePax.formReset();
	});

}

UI_voucherRePax.getErrorList = function () {
	$.ajax({
		url : 'showReprotectedPaxDetail!returnErrorList.action',
		dataType : "json",
		beforeSend : ShowPopProgress(),
		type : "POST",
		success : UI_voucherRePax.errorDetailSuccess
	});
}

UI_voucherRePax.errorDetailSuccess = function (response) {
	try {
		eval(response.errorList);
	} catch (err) {
		console.log("eval() error ==> errorList");
	}
	HidePopProgress();
}
//selectPax

UI_voucherRePax.validateSearch = function() {
	var fltNum = $("#flightNumSrch").val();
	var dateSrch =  $("#dateSrch").val();
	if (trim(fltNum) == "" || trim(fltNum) == null) {
		showERRMessage(arrError["fltNumInvalid"]);
		return false;
	} else if (trim(dateSrch) == "" || trim(dateSrch) == null) {
		showERRMessage(arrError["srchDateEmpty"]);
		return false;
	} else if (!isValidDate (dateSrch)){
		showERRMessage(arrError["srchDateInvalid"]);
		return false;
	} else {
		return true;
	} 
}

UI_voucherRePax.searchRePax = function() {

	var reprotectedpaxDTOJsonToString = function(cellVal, options, rowObject) {
		return JSON.stringify(cellVal);
	}

	$("#reprotectedPassengersContainer")
			.empty()
			.append(
					'<table id="rePaxDetailsGrid"></table><div id="rePaxDetailsPages"></div>')
	var data = {};
	data['reprotectedPaxSearchrequest.flightNumber'] = $("#flightNumSrch")
			.val();
	data['reprotectedPaxSearchrequest.date'] = $("#dateSrch").val();
	data['flightNum'] = $("#flightNumSrch").val();

	var reprotectedPaxGrid = $("#rePaxDetailsGrid").jqGrid(
			{
				datatype : function(postdata) {
					$
							.ajax({
								url : 'showReprotectedPaxDetail.action',
								data : $.extend(postdata, data),
								dataType : "json",
								type : "POST",
								beforeSend : ShowPopProgress(),
								complete : function(jsonData, stat) {
									if (stat == "success") {
										mydata = eval("("
												+ jsonData.responseText + ")");
										if (mydata.msgType != "Error") {
											$.data(reprotectedPaxGrid[0],
													"gridData",
													mydata.reProtectedPaxList);
											reprotectedPaxGrid[0]
													.addJSONData(mydata);
										} else {
											alert(mydata.message);
										}
										HidePopProgress();
									}
								}
							});
				},
				height : 200,

				colNames : [ 'PNR', 'First Name', 'Last Name', 'Flight Number',
						'Flight Date', 'ReprotectedPaxDTO' ],
				colModel : [ {
					name : 'pnr',
					index : 'pnr',
					jsonmap : 'rePaxDTO.pnr',
					sorttype:'text',
					width : 35
				},{
					name : 'firstName',
					index : 'firstName',
					jsonmap : 'rePaxDTO.paxFirstName',
					sorttype:'text',
					width : 70
				}, {
					name : 'lastName',
					index : 'lastName',
					jsonmap : 'rePaxDTO.paxlastName',
					width : 70,
					sorttype : "string"
				}, {
					name : 'flightNumber',
					index : 'flightNumber',
					jsonmap : 'rePaxDTO.flightNumber',
					width : 35,
					sorttype : "string"
				}, {
					name : 'flightDate',
					index : 'flightDate',
					jsonmap : 'rePaxDTO.flightDate',
					width : 70
				}, {
					name : 'reprotectedpaxDTO',
					index : 'reprotectedpaxDTO',
					jsonmap : 'rePaxDTO',
					formatter : reprotectedpaxDTOJsonToString,
					hidden : true
				} ],
				caption : "Reprotected Passenger Details",
				rowNum : 20,
				viewRecords : true,
				width : 920,
				pager : jQuery('#rePaxDetailsPages'),
				multiselect: true ,
				jsonReader : {
					root : "reProtectedPaxList",
					page : "page",
					total : "total",
					records : "records",
					repeatitems : false,
					id : "0"
				},
				gridComplete : function() {
					var ids = jQuery("#rePaxDetailsGrid").getDataIDs();
					for (var i = 0; i < ids.length; i++) {
						var cl = ids[i];
						UI_voucherRePax.disableIfVoucherIssued(cl)
					}					
				},
				 beforeSelectRow : function(rowid, e) {
					var grid = rePaxDetailsGrid;
					var cbsdis = $("tr#" + rowid
							+ ".jqgrow > td > input.cbox:disabled", grid[0]);
					if (cbsdis.length === 0) {
						return true; // allow select the row
					} else {
						return false; // not allow select the row
					}
				},
		        sortable: true ,
		        sortname: 'firstName',
				onSelectAll : function(aRowids, status) {
					if (status) {
						var grid = rePaxDetailsGrid;
						var cbs = $("tr.jqgrow > td > input.cbox:disabled",
								grid[0]);
						cbs.removeAttr("checked");
						grid[0].p.selarrrow = grid.find(
								"tr.jqgrow:has(td > input.cbox:checked)").map(
								function() {
									return this.id;
								}) 
						.get(); 
					}
				}
			}).navGrid("#rePaxDetailsPages", {
		refresh : true,
		edit : false,
		add : false,
		del : false,
		search : false
	});

	$("#rePaxDetailsGrid").click(function() {
	});
}

UI_voucherRePax.disableIfVoucherIssued = function (row) {
	var rePaxDTO = JSON.parse(jQuery("#rePaxDetailsGrid").getCell(row,
			'reprotectedpaxDTO'));
	var newId = "jqg_" + row;
	if (rePaxDTO.voucherId==null || rePaxDTO.voucherId=="" ) {
		$("#"+newId).prop('disabled', false);
	} else {
		$("#"+newId).prop('disabled', true);
	}
}

UI_voucherRePax.getCheckedPaxDetails = function() {
	var rowCount = $("#rePaxDetailsGrid").getGridParam("reccount");
	var paxList = [];
	var chkBoxList = [] ;
	for (i = 1; i < rowCount + 1; i++) {
		var id = "jqg_" + i;
		var checkBoxPax = document.getElementById(id);
		if (checkBoxPax != null && checkBoxPax.checked == true) {
			var rePaxDTO = JSON.parse(jQuery("#rePaxDetailsGrid").getCell(i,
					'reprotectedpaxDTO'));
			paxList.push(rePaxDTO);
			chkBoxList.push(id);
		}
	}
	if (paxList.length == 0) {
		showERRMessage(arrError["selectPax"]);
	}
	else {
		UI_voucherRePax.formEnable();
		UI_voucherRePax.chkBoxDisable(chkBoxList);
		reprotectedSelectedPaxList = paxList;
		$("#btnIssue").hide();
	}
}

UI_voucherRePax.issueVoucher = function() {
	if (UI_voucherRePax.voucherFormValidate()) {
		var validFrom = $("#validFrom").val();
		var validTo = $("#validTo").val();
		var amount = Math.floor($("#pfValue").val() * 100) / 100;
		var remarks = $("#remarks").val();
		var issueType = $("#issueBy").val();
		
		var data = {};
		data["validFrom"] = validFrom;
		data["validTo"] = validTo;
		data["amount"] = amount;
		data["remarks"] = remarks;
		data["issueType"] = issueType;
		
		for (i = 0; i < reprotectedSelectedPaxList.length; i++) {
			var paxObject = reprotectedSelectedPaxList[i];
			data["selectedPaxList[" + i + "].flightDate"] = paxObject.flightDate;
			data["selectedPaxList[" + i + "].flightNumber"] = paxObject.flightNumber;
			data["selectedPaxList[" + i + "].paxEmail"] = paxObject.paxEmail;
			data["selectedPaxList[" + i + "].mobileNumber"] = paxObject.mobileNumber;
			data["selectedPaxList[" + i + "].paxFirstName"] = paxObject.paxFirstName;
			data["selectedPaxList[" + i + "].paxlastName"] = paxObject.paxlastName;
			data["selectedPaxList[" + i + "].pnrPaxId"] = paxObject.pnrPaxId;
			data["selectedPaxList[" + i + "].reprotectedPaxid"] = paxObject.reprotectedPaxid;
			data["selectedPaxList[" + i + "].voucherId"] = paxObject.voucherId;
			data["selectedPaxList[" + i + "].frmFltSegId"] = paxObject.frmFltSegId;
			data["selectedPaxList[" + i + "].toFltSegId"] = paxObject.toFltSegId;
			data["selectedPaxList[" + i + "].pnrSegmentId"] = paxObject.pnrSegmentId;
		}		
		$.ajax({
			url : 'showReprotectedPaxDetail!issueVoucher.action',
			dataType : "json",
			data : data,
			beforeSend : ShowPopProgress(),
			type : "POST",
			async : true,
			success : UI_voucherRePax.voucherIssuedSuccess
		});		
	}
}

UI_voucherRePax.voucherIssuedSuccess = function () {
	HidePopProgress();
	showConfirmation(arrError["issueSuccess"]);	
	UI_voucherRePax.formReset ();
	UI_voucherRePax.searchRePax();
	reprotectedSelectedPaxList = [];		
}

UI_voucherRePax.voucherFormValidate = function() {
	var validFromDate = $("#validFrom").datepicker("getDate");
	var validToDate = $("#validTo").datepicker("getDate");
	var validFrom = $("#validFrom").val();
	var validTo = $("#validTo").val();
	var amount = $("#pfValue").val();
	var issueBy = $("#issueBy").val();

	var valid = true;

	if (trim(validFrom) == "" || trim(validFrom) == null) {
		showERRMessage(arrError["fromDateEmpty"]);
		valid = false;
	} else if (trim(validTo) == "" || trim(validTo) == null) {
		showERRMessage(arrError["toDateInvalid"]);
		valid = false;
	} else if (validToDate - validFromDate <= 0) {
		showERRMessage(arrError["periodInvalid"]);
		valid = false;
	} else if (trim(amount) == "" || trim(amount) == null) {
		showERRMessage(arrError["amountEmpty"]);
		valid = false;
	} else if (!$.isNumeric(trim(amount))) {
		showERRMessage(arrError["amountInvalid"]);
		valid = false;
	} else if (issueBy =="PF" && !isPercentage(amount)) {
		showERRMessage(arrError["percentageInvalid"]);
		valid = false;
	}

	return valid;
}

UI_voucherRePax.formDisable = function () {
	$("#validFrom").datepicker("option", "disabled", true);
	$("#validTo").datepicker("option", "disabled", true);
	$("#pfValue").prop('disabled', true);
	$("#validFrom").prop('disabled', true);
	$("#validTo").prop('disabled', true);	
	$("#issueBy").prop('disabled', true);
	$("#remarks").prop('disabled', true);
}

UI_voucherRePax.formEnable = function () {
	$("#validFrom").datepicker("option", "disabled", false);
	$("#validTo").datepicker("option", "disabled", false);
	$("#pfValue").prop('disabled', false);
	$("#issueBy").prop('disabled', false);
	$("#validFrom").prop('disabled', false);
	$("#validTo").prop('disabled', false);	
	$("#remarks").prop('disabled', false);
}

UI_voucherRePax.chkBoxDisable = function() {
	var rowCount = $("#rePaxDetailsGrid").getGridParam("reccount");
	for (i = 1; i < rowCount+1; i++) {
		var id = "jqg_" + i;
		$("#"+id).prop('disabled', true);
	}
}

UI_voucherRePax.formReset = function() {
	$("#validFrom").val("");
	$("#pfValue").val("");
	$("#validTo").val("");
	$("#btnIssue").prop('disabled', false);
	$("#remarks").val("");
	$("#issueBy").val("V");
	
	var rowCount = $("#rePaxDetailsGrid").getGridParam("reccount");
	for (var i=0; i<rowCount; i++) {	
		var row = i+1;
		var rePaxDTO = JSON.parse(jQuery("#rePaxDetailsGrid").getCell(row,
		'reprotectedpaxDTO'));
		var id = "jqg_" + row;
		if (rePaxDTO.voucherId==null || rePaxDTO.voucherId=="" ) {
			$("#"+id).prop('disabled', false);
		} else {
			$("#"+id).prop('disabled', true);
		}
		$("#"+id).checked = false;	
		
	}
	reprotectedSelectedPaxList = [];
	$("#btnIssueVoucher").hide();
	$("#btnIssue").show();
	UI_voucherRePax.formDisable();
	
}

ShowPopProgress = function() {
	setVisible("divLoadMsg", true);
}

HidePopProgress = function() {
	setVisible("divLoadMsg", false);
}

isPercentage = function(amount) {
	var x = parseFloat(amount);
	if (isNaN(x) || x < 0 || x > 100) {
	    return false;
	} else {
		return true;
	}
}

isNumberKey = function(evt) {
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode != 46 && charCode > 31 
     && (charCode < 48 || charCode > 57))
      return false;
   return true;
}

isValidDate = function(dateString){
	re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/; 
	if(dateString != '') { 
		if(regs = dateString.match(re)) {
			if(regs[1] < 1 || regs[1] > 31) { 
				 return false;
			} else if(regs[2] < 1 || regs[2] > 12) { 
				 return false;
			} else if(regs[3] < 2000 || regs[3] > 2100) { 
				return false; 
			} else {
				return true;
			}			
		} else {
			return false;
		}
	} else {
		return false;
	}
}