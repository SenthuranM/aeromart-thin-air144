var objWindow;
var screenId="SC_FACM_007";	
var valueSeperator = "~";
var strSearchTemp = "";

	function winOnLoad(strMsg, strMsgType) {
	
		getFieldByID("selYear1").focus();
		strSearchTemp = top[0].manifestSearchValue;

		if ((strSearchTemp != "") && (strSearchTemp != null)) {
			var strSearch = strSearchTemp.split("#");
			setField("selYear1",strSearch[0]);
			setField("selMonth1",strSearch[1]);
			setField("selAgencies",strSearch[2]);
			setField("selBPStart",strSearch[4]);
			setField("selBPEnd",strSearch[10]);
			if (strSearch[3] == 'true') {
				getFieldByID("chkTAs").checked = true;
			} else {
				getFieldByID("chkTAs").checked = false;		
				Disable('chkTAs',true);
			}
			if(strSearch[5] && strSearch[5] != "") {
				ls.selectedData(strSearch[5]);
			}
			if(strSearch[6] && strSearch[6] != 'false') {
				document.forms[0].chkZero1.checked =true;

			}
			if(!strSearch[6]) {
				document.forms[0].chkZero1.checked =true;
			}
			if (strSearch[7] == 'true')	{
				getFieldByID("chkCOs").checked = true;
			}else {
				getFieldByID("chkCOs").checked = false;		
				Disable('chkCOs',true);
			}
			
			setField("selYear2",strSearch[8]);
			setField("selMonth2",strSearch[9]);
			
		}else {
			var strSysDateArr=getSystemDate().split("/");
			Disable('chkTAs',true);
			Disable('chkCOs',true);
		}		

		Disable('btnSend',true);
		Disable('btnEmail', true);
		Disable('btnDetails', true);
		Disable('txtEmail', true);
		Disable('chkAllAgent', true);
		Disable('chkIndiAgent', true);
		
		if (invoiceData.length == 0) {
			Disable('chkPage',true);
			Disable('chkAll',true);
		}else {
			Disable('chkPage',false);
			Disable('chkAll',false);
		}
		///-----------------//////
		validatePeriod(); 
		if(strMsg != null && strMsg != "" && strMsgType != null && strMsgType != "") {
			showCommonError(strMsgType,strMsg);
		}
		
	}

	function gridNavigations(intRecNo, intErrMsg) {	
		setField("hdnRecNo",intRecNo);
		
		if (intErrMsg != ""){
			showCommonError("Error",intErrMsg);
		}else{
			setField("hdnAgents",getAgents());
			document.forms[0].hdnMode.value="SEARCH";
			var strSearchCriteria = getValue("selYear1")+"#"+getValue("selMonth1")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBPStart")+"#"+ls.getselectedData()+"#"+getValue("chkZero1")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selYear2")+"#"+getValue("selMonth2")+"#"+getValue("selBPEnd")+"#";
			top[0].manifestSearchValue = strSearchCriteria;			
			setTabSearch();
			document.forms[0].submit();
			ShowProgress();
		}

	}
	
	function setTabSearch(){
		top[0].intLastRec = getText("hdnRecNo");
	}

	function rowClick(strRowNo) {	
		document.forms[0].chkAll.checked = false;
		document.forms[0].chkPage.checked = false;
		var intLength = invoiceData.length ;		
		for (var i = 0 ; i < intLength ; i++){
		   	objDG.setCellValue(i, 9, "" );  
		}
		objDG.setCellValue(strRowNo, 9, "true");	
		Disable('btnDetails',false);
		Disable('chkAllAgent', false);
		Disable('chkIndiAgent', false);			
	}
	
	function clickAgencies(){	    
		if (getValue("selAgencies") == 'GSA'){
			Disable('chkTAs',false);
			Disable('chkCOs',false);
		}else if (getValue("selAgencies") == 'SGSA' || getValue("selAgencies") == 'TA'){
			Disable('chkTAs',false);
			Disable('chkCOs',true);
			getFieldByID("chkCOs").checked = false;		
		} else{
			Disable('chkTAs',true);
			getFieldByID("chkTAs").checked = false;
			Disable('chkCOs',true);
			getFieldByID("chkCOs").checked = false;			
		}	
	}

	function changeAgencies(){
		ls.clear();		
	}
	
	function getAgentClick() {
	
		if(getValue("selAgencies") == ""){
			showERRMessage(arrError["agentTypeRqrd"]);			
			getFieldByID("selAgencies").focus();
		}else{
			var strSearchCriteria = getValue("selYear1")+"#"+getValue("selMonth1")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBPStart")+"# #"+getValue("chkZero1")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selYear2")+"#"+getValue("selMonth2")+"#"+getValue("selBPEnd")+"#";
			top[0].manifestSearchValue = strSearchCriteria;
			setField("hdnMode","AGENTS");
			setField("hdnAgents","");
			var objForm  = document.getElementById("frmInvoiceEmail");
			objForm.target = "_self";
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	
	}
	
	function closeClick() {		
		if (top.loadCheck(top.pageEdited)) {
		objOnFocus();
		top[0].manifestSearchValue="";
		top.LoadHome();		
		}
	}
	
	function sendClick() {

		var strrec = top[0].intLastRec;
		setField("hdnAgents",getAgents());		
		setField("hdnSelectedAgents",getSelectedAgents());
		
		setField("hdnBillingEmail",getSelectedBillingEmailIDs());
		setField("hdnFromDate", getFromDate());
		setField("hdnToDate", getToDate());	
	
		setField("hdnMode","EMAIL");		
		setField("hdnRecNo", strrec);
		var strSearchCriteria = getValue("selYear1")+"#"+getValue("selMonth1")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBPStart")+"#"+ls.getselectedData()+"#"+getValue("chkZero1")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selYear2")+"#"+getValue("selMonth2")+"#"+getValue("selBPEnd")+"#";
		top[0].manifestSearchValue = strSearchCriteria;
		setTabSearch();
		var objForm  = document.getElementById("frmInvoiceEmail");
		objForm.target = "_self";		
		document.forms[0].submit();
	
	}
	
	function sendEmailToGivenEmail() {
		
		if(isEmpty(getText("txtEmail"))){
			showERRMessage(arrError["EMailisEmpty"]);
			getFieldByID("txtEmail").focus();
		} else if(!validateEmail()){
			showERRMessage(arrError["InvalidEmail"]);
			getFieldByID("txtEmail").focus();
		} else{
		
			var strrec = top[0].intLastRec;
			setField("hdnAgents",getAgents());		
			setField("hdnSelectedAgents",getSelectedAgents());
			setField("hdnFromDate", getFromDate());
			setField("hdnToDate", getToDate());		
		
			setField("hdnMode","EMAIL2");		
			setField("hdnRecNo", strrec);
			var strSearchCriteria = getValue("selYear1")+"#"+getValue("selMonth1")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBPStart")+"#"+ls.getselectedData()+"#"+getValue("chkZero1")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selYear2")+"#"+getValue("selMonth2")+"#"+getValue("selBPEnd")+"#";
			top[0].manifestSearchValue = strSearchCriteria;
			setTabSearch();
			var objForm  = document.getElementById("frmInvoiceEmail");
			objForm.target = "_self";		
			document.forms[0].submit();
	
		}
	}
		
	function validateEmail(){	
		objOnFocus();
		var tempVar = checkEmail(trim(getText("txtEmail")));
		if(!tempVar){
			setField("txtEmail",""); 
			getFieldByID("txtEmail").focus();
		}
		return tempVar;	
	}
	
	function objOnFocus(){
		top[2].HidePageMessage();
	}

	function chkvalidate() {
		var blnselected = false;
		var blnNoSelect = false;
		var seltdAgents = objDG.getSelectedColumn(9);
		Disable('btnEmail',true);
		document.forms[0].chkAll.checked = false;
		for (var i =0 ;i < seltdAgents.length ;i++ ){			
			if(seltdAgents[i][1] == true) {
				blnselected = true;	
				break;
			}
		}
		for (var i =0 ;i < seltdAgents.length ;i++ ){			
			if(seltdAgents[i][1] != true) {
				blnNoSelect = true;	
				break;
			}
		}
		if(blnNoSelect) {			
			document.forms[0].chkPage.checked = false;
			Disable('btnDetails',true);
			Disable('txtEmail',true);
			Disable('chkAllAgent', 	true);
			Disable('chkIndiAgent', true);		
			Disable('btnSend',true);
			
		}			
		if(blnselected == true) {
			Disable('btnSend',false);	
			Disable('btnDetails',false);
			Disable('txtEmail',false);
			Disable('chkAllAgent', false);
			Disable('chkIndiAgent', false);	
		
		}		
	}
	
	function getSelectedAgents() {
		var strArray = ""
		var count = 0;
		var blnAgents = false;
		
		if(!document.forms[0].chkAll.checked) {
			var selscedtobld = objDG.getSelectedColumn(9);				
			for (var i =0 ;i < selscedtobld.length ;i++ ){			
				if(selscedtobld[i][1]) {				
					strArray += invoiceData[i][1] +",";
					blnAgents = true;
				}							
			}
		} else {
			var intLength = totalInvoiceData.length ;
			for(var k =  0; k < intLength; k++) {
				strArray += totalInvoiceData[k][1] +",";
				blnAgents = true;
			}
		}
		
		if(blnAgents == true) {
			Disable('chkAllAgent', false);
			Disable('chkIndiAgent', false);
		}		
		return strArray;	
	
	}
	
	function getSelectedBillingEmailIDs() {
		var strArray = ""
		var count = 0;
		var blnAgents = false;
		var selscedtobld = objDG.getSelectedColumn(9);				
		for (var i =0 ;i < selscedtobld.length ;i++ ){			
			if(selscedtobld[i][1]) {				
				strArray += invoiceData[i][11] +",";
				blnAgents = true;
			}							
		}		
		return strArray;	
	
	}
	
	
	
	function searchClick() {
	
		setField("hdnAgents",getAgents());
		var strSysDateArr=getSystemDate().split("/");
		var numField1=Number(getFieldByID("selMonth1").value);
		var thisMonth=Number(strSysDateArr[1]);
		var selecYear1 = Number(getFieldByID("selYear1").value);
		var thisyear = Number(strSysDateArr[2]);
		
		var fromDate = new Date(getFromDate());
		var toDate = new Date(getToDate());
		
		if (getFieldByID("selYear1").value==""){
			showERRMessage(arrError["yearEmpty"]);				
			getFieldByID("selYear1").focus();
		} else if (getFieldByID("selYear2").value==""){

				showERRMessage(arrError["yearEmpty"]);
				getFieldByID("selYear2").focus();
		}
		 else if (getFieldByID("selMonth1").value==""){

				showERRMessage(arrError["monthEmpty"]);
				getFieldByID("selMonth1").focus();
		} 
		 else if (getFieldByID("selMonth2").value==""){

				showERRMessage(arrError["monthEmpty"]);
				getFieldByID("selMonth2").focus();
		}
		else if(getText("selAgencies")==""){

				showERRMessage(arrError["agentTypeRqrd"]);
				getFieldByID("selAgencies").focus();
		} else if(ls.getSelectedDataWithGroup()=="" && getText("selAgencies") != "All"){

				showERRMessage(arrError["agentsRqrd"]);
		}else if(thisyear == selecYear1 && numField1 >thisMonth){

				showERRMessage(arrError["monthGreater"]);
//		} else if(getFromDate() > getToDate()) { 
		}else if(fromDate > toDate) {

				showERRMessage(arrError["fromDateGreater"]);
		} else {	
				setField("hdnMode","SEARCH");
				var strSearchCriteria = getValue("selYear1")+"#"+getValue("selMonth1")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBPStart")+"#"+ls.getselectedData()+"#"+getValue("chkZero1")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selYear2")+"#"+getValue("selMonth2")+"#"+getValue("selBPEnd")+"#";
				document.getElementById("frmInvoiceEmail").target = "_self";
				document.forms[0].hdnMode.value="SEARCH";
				top[0].manifestSearchValue = strSearchCriteria;		
				document.forms[0].submit();
				top[2].ShowProgress();				
		}
	
	}
	
	function setPageEdited(isEdited){
		top.pageEdited = isEdited;
	}
	
	
	function beforeUnload(){
		if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
			top[0].objWindow.close();
		}
	}
	
	function getSystemDate(){
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD} 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();
		return strSysDate;

	}
	
	function getAgents(){
		var strAgents=ls.getSelectedDataWithGroup();
		var newAgents;
		if(strAgents.indexOf(":")!=-1){
			strAgents=replaceall(strAgents,":" , ",");
		}
		if(strAgents.indexOf("|")!=-1){
			newAgents=replaceall(strAgents,"|" , ",");
		}else{
			newAgents=strAgents;
		}
		return newAgents;
	}
	
	function clickAll() {
	
		document.forms[0].chkPage.checked = false;		
		Disable('btnSend',true);
		var intLength = invoiceData.length ;
		
		if (document.forms[0].chkAll.checked) {
			document.forms[0].chkAll.checked = true;
			Disable('btnDetails',false);
			Disable('chkAllAgent', false);
			Disable('chkIndiAgent', false);
			
		}
		else {			
		 	for (var i = 0 ; i < intLength ; i++){
		   		objDG.setCellValue(i, 9, "" );  
		 	}			
		 	Disable('btnEmail',true);	
		 	Disable('txtEmail',true);
		 	Disable('btnDetails',true);	
		 	Disable('chkAllAgent', true);
			Disable('chkIndiAgent', true); 
			Disable('btnSend', true);		
		}
	}
	
	function clickPage() {
	
	document.forms[0].chkAll.checked = false;
		var intLength = invoiceData.length ;
		if (document.forms[0].chkPage.checked) {			
			for (var i = 0 ; i < intLength ; i++){
		   		objDG.setCellValue(i, 9, "true" );  
		 	}		 	

		 	Disable('btnDetails',false);
		 	Disable('chkAllAgent', false);
			Disable('chkIndiAgent', false);	
		 	document.forms[0].chkPage.checked = true;		 	
		}else {			
		 	for (var i = 0 ; i < intLength ; i++){
		   		objDG.setCellValue(i, 9, "" );  
		 	}			
		 	Disable('btnEmail',true);
		 	Disable('txtEmail',true);
		 	Disable('btnDetails',true);
		 	Disable('chkAllAgent', true);
			Disable('chkIndiAgent', true);	
			Disable('btnSend', true);		 	
		}
	}
	
	function clickAllAgent() {
		document.forms[0].chkIndiAgent.checked = false;	
		if(document.forms[0].chkAllAgent.checked == true) {
			Disable('btnEmail', false);
			Disable('txtEmail',false);
			Disable('btnSend', false);	
			
		} else {
			Disable('btnEmail', true);
			Disable('txtEmail',true);
			Disable('btnSend', true);	
		}
		
	}
	
	function clickSingleAgent() {
		document.forms[0].chkAllAgent.checked = false;
		
		if(document.forms[0].chkIndiAgent.checked == true) {
			Disable('btnEmail', false);
			Disable('txtEmail',false);
			Disable('btnSend', false);
		} else {
			Disable('btnEmail', true);
			Disable('txtEmail',true);
			Disable('btnSend', true);	
		}	
	}
	
	function validatePeriod() {		
	
		var strSysCur = getValue("hdnCurrentDate").split("/");		
		
		var numField1=Number(getFieldByID("selMonth1").value);
		var numField2 = Number(getFieldByID("selMonth2").value);		
		var thisMonth=Number(strSysCur[1]);
		
		var selecYear1 = Number(getFieldByID("selYear1").value);
		var selecYear2 = Number(getFieldByID("selYear2").value);		
		var thisyear = Number(strSysCur[2]);
		
		var thisDay = Number(strSysCur[0]);
		if(selecYear1 == "" || numField1 == "" ) {
			return;
		}
		if(selecYear2 == "" || numField2 == "" ) {
			return;
		}
	
	}

	function viewReport() {
		setField("hdnMode","VIEW");
		setField("hdnSelectedAgents",getSelectedAgents());	
		setField("hdnFromDate", getFromDate());
		setField("hdnToDate", getToDate());	
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank","CWindow",strProp);	
		var objForm  = document.getElementById("frmInvoiceEmail");
		objForm.target = "CWindow";	
		objForm.action = "showEmailOutstandingBalance.action";	
		objForm.submit();
		top[2].HidePageMessage();
		
				
	}

	function getFromDate() {
		var fromday = getValue("selBPStart")==1?"01":"16";
		var year = getValue("selYear1");
		var month = getValue("selMonth1");

		var fromDate = fromday+"/"+month+"/"+year;
		return fromDate;
	}
	
	function getToDate() {
		var fromday2 = getValue("selBPEnd")==1?"01":"16";
		var year2 = getValue("selYear2");
		var month2 = getValue("selMonth2");

		var toDate = fromday2+"/"+month2+"/"+year2;
		return toDate;
	}
	
	
		
	
