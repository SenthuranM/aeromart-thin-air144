	var objWindow;
		
		var objCal1 = new Calendar("spnCalendarDG1");
		objCal1.onClick = "setDate";
		objCal1.blnDragCalendar = false;
		objCal1.buildCalendar();
		
		function setDate(strDate, strID){
			switch (strID){
				case "0" : setField("txtFromDate",strDate);break ;
				case "1" : setField("txtToDate",strDate);break ;
			}
		}
	
		function LoadCalendar(strID, objEvent){
			objCal1.ID = strID;
			objCal1.top = 1 ;
			objCal1.left = 0 ;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
	
	var screenId="UC_REPM_026";
	
	
function closeClick() {
	if (top.loadCheck(top.pageEdited)){
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function beforeUnload(){
		if ((top.objCWindow) && (!top.objCWindow.closed))	{
			top.objCWindow.close();
		}
}
function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}



	function validateDate(){	
		var tempDay;
		var tempMonth;
		var tempYear;
		var validate = false;
		
		var dateFrom = getText("txtFromDate");
		var dateTo = getText("txtToDate");
	
		tempIStartDate = dateFrom;
		tempIEndDate = dateTo;
				
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM;}
		if (dtCD < 10){dtCD = "0" + dtCD;}
	 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
		
		strSysODate=(tempYear+tempMonth+tempDay); 		
		
		if(tempOStartDate > tempOEndDate){
			showERRMessage(arrError["fromDtExceed"]);
			
			getFieldByID("txtToDate").focus();
		} else {
			validate = true;				
		}
		return validate;	
	}
	
	function viewClick(){
		
		if(getText("txtFromDate")==""){			
			showERRMessage(arrError["fromDtEmpty"]);
			getFieldByID("txtFromDate").focus();
			
		} else if(dateValidDate(getText("txtFromDate"))==false){		
			showERRMessage(arrError["fromDtInvalid"]);
			getFieldByID("txtFromDate").focus();
			
		}  else if(getText("txtToDate")==""){			
			showERRMessage(arrError["toDtEmpty"]);
			getFieldByID("txtToDate").focus();
			
		} else if(dateValidDate(getText("txtToDate"))==false){			
			showERRMessage(arrError["toDtinvalid"]);
			getFieldByID("txtToDate").focus();
			
		}
		else if(checkDates()==false){			
			showERRMessage(arrError["fromDtExceed"]);
			getFieldByID("txtFromDate ").focus();
			
		}
		else if(validateAirport()==false){			
			showERRMessage(arrError["sameSegment"]);
			getFieldByID("selToAirport ").focus();
		}
		else{				
			setField("hdnMode","VIEW");	
			setField("hdnReportView","SUMMARY");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
			top[0].objWindow = window.open("about:blank","CWindow",strProp);			
			var objForm  = document.getElementById("frmViewBookedHalaServices");
			objForm.target = "CWindow";				
			objForm.action = "showBookedHalaServicesReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		
		}
	}
	/*
	function validateCarrierCode() {
		var validate = false;
		if(!showCarrierCombo || trim(lscc2.getselectedData()).length > 0) {
			validate = true;
		} else {
			showERRMessage(arrError["carrierCodeRqrd"]);
			
		}
		return validate;
	}*/
	
	/**
	 * Validates the arrival & departure locations
	 * @return
	 */
	function validateAirport(){
		var from = getValue("selFromAirport");		
		var to = getValue("selToAirport");	
		if(!isEmpty(from) && !isEmpty(to)){
			if(from==to){			
				return false;
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
	}
	
	/**
	 * Validates the from date & to date
	 * @return
	 */
	function checkDates(){
		var frmDate = getText("txtFromDate");
		var toDate = getText("txtToDate");
		var retVal = compareDate(frmDate,toDate);		
		if(retVal>0)
			return false;	
		else
			return true;
			
	}
	
	function pageLoad(){
		//setField("txtCarrierCode", "G9");		
		getFieldByID("txtFromDate").focus();
		//setField("txtToDate", strSysDate);		
		//Disable("txtFlightNumber",true);
		
		if (showCarrierCombo) {
						
			document.getElementById("carrierCode").style.display = "block";
			var listBox = document.getElementById("lstCarrierCodes1");		
			
			var listBox2 = document.getElementById("lstAssignedCarrierCodes1");
			listBox2 = lscc2;		
		
			if(methods.length > 0) {			
				for(var i=0; i<listBox.options.length; i++) {			
					if(listBox.options[i].value == defaultCarrier) {											
						listBox2.selectedData(defaultCarrier);
						
					} 
				}
			}
		} 
	}



	function chkClick(){			
		
	}
	
	