var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function beforeUnload() {
	if ((top.objWindow) && (!top.objWindow.closed)) {
		top.objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	if (getFieldByID("radReportPeriodO").checked == true) {
		objCal1.ID = strID;
		objCal1.top = 1;
		objCal1.left = 0;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}
}
var screenId = "UC_REPM_010";

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function viewClick(isSchedule) {
	
	if (getFieldByID("radReportPeriodO").checked == true
			&& getText("txtFromDate") == "") {
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();
	} else if (getFieldByID("radReportPeriodO").checked == true
			&& dateValidDate(getText("txtFromDate")) == false) {
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
	} else if (getFieldByID("radReportPeriodO").checked == true
			&& getText("txtToDate") == "") {
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
	} else if (getFieldByID("radReportPeriodO").checked == true
			&& dateValidDate(getText("txtToDate")) == false) {
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtToDate").focus();
	} else if (ls.getselectedData() == "") {
		showERRMessage(arrError["selectedUsers"]);
		getFieldByID("lstUsers").focus();
	} else if (lscc2.getselectedData() == "") {
		showERRMessage(arrError["paymentmodesRqrd"]);
		getFieldByID("lstBookingPayments").focus();
	} else if (getFieldByID("radReportPeriodO").checked == true
			&& dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)
			|| getFieldByID("radReportPeriodO").checked == false) {

		setField("hdnMode", "VIEW");
		var userIDs = trim(ls.getselectedData());
		var paymentTypes = trim(lscc2.getselectedData());
		setField("hdnUserCode", userIDs);
		setField("hdnBookingPaymentType", paymentTypes);
		
		if(isSchedule) {
			scheduleReport();		
		} else {
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
				top.objWindow = window.open("about:blank", "CWindow", strProp);
				var objForm = document.getElementById("frmMonitorPerformance");
				objForm.target = "CWindow";
				objForm.action = "showMonitorPerformanceOfSalesStaff.action";
				objForm.submit();
				top[2].HidePageMessage();
		}	
	}
}

function scheduleReport() {

	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmMonitorPerformance', 
		composerName: 'staffPerformanceReport',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function winOnLoad() {
	Disable("txtFromDate", true);
	Disable("txtToDate", true);
	if (top.strSearchCriteria != ""
			&& top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");

		if (strSearch[3] == "Today") {
			getFieldByID("radReportPeriodT").checked = true;
		} else if (strSearch[3] == "ThisWeek") {
			getFieldByID("radReportPeriodTW").checked = true;
		} else if (strSearch[3] == "ThisMonth") {
			getFieldByID("radReportPeriodTM").checked = true;
		} else if (strSearch[3] == "ThisYear") {
			getFieldByID("radReportPeriodTY").checked = true;
		} else if (strSearch[3] == "Other") {
			getFieldByID("radReportPeriodO").checked = true;
			Disable("txtFromDate", false);
			Disable("txtToDate", false);
			setField("txtFromDate", strSearch[0]);
			setField("txtToDate", strSearch[1]);
			getFieldByID("txtFromDate").focus();
		}
//		setField("selAgencies", strSearch[2]);
	}
}

function getAgentClick() {

	var period;

	for ( var i = 0; i < document.frmMonitorPerformance.radReportPeriod.length; i++) {
		if (document.frmMonitorPerformance.radReportPeriod[i].checked) {
			period = document.frmMonitorPerformance.radReportPeriod[i].value;
		}
	}
	var strSearchCriteria = getValue("txtFromDate") + "#"
			+ getValue("txtToDate") + "#" // + getValue("selAgencies") + "#"
			+ period;
	top.strSearchCriteria = strSearchCriteria;

	setField("hdnMode", "SEARCH");
	document.forms[0].target = "_self";
	document.forms[0].submit();
	ShowProgress();
}

function chkClickPeriod() {
	if (getFieldByID("radReportPeriodO").checked == true) {
		Disable("txtFromDate", false);
		Disable("txtToDate", false);
	//	setField("selAgencies", "");
		getFieldByID("txtFromDate").focus();
		ls.removeAllFromListbox();
	} else {
		Disable("txtFromDate", true);
		Disable("txtToDate", true);
		setField("txtFromDate", "");
		setField("txtToDate", "");
	//	setField("selAgencies", "");
		ls.removeAllFromListbox();
	}
}
function AgentOnChange() {
	ls.removeAllFromListbox();
	ls.clear();
}
