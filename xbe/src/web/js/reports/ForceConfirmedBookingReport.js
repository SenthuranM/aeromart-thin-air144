var objWindow;
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function winOnLoad() {
	
}

function setDate(strDate, strID){
	switch (strID){
	
	case "0" : setField("txtBkngFromDate",strDate);break ;
	case "1" : setField("txtBkngToDate",strDate);break ;
	case "2" : setField("txtDeptFromDate", strDate);break;
	case "3" : setField("txtDeptToDate", strDate);break;
	
	}
}

function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}

function beforeUnload(){
	if ((top.objCWindow) && (!top.objCWindow.closed))	{
		top.objCWindow.close();
	}
}
	
function closeClick() {
	if (top.loadCheck(top.pageEdited)){
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function scheduleReport() {

	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmForceConfrmdRpt', 
		composerName: 'forcedConfirmedReservationsReport',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function viewClick(isSchedule){
	
	if(getText("selDeparture")==getText("selArrival") 
			&& getText("selDeparture") !="" && getText("selArrival") !=""  ){
		showERRMessage(arrError["sameSegment"]);
		getFieldByID("selArival").focus();
		
	}
	
	if (getText("txtBkngFromDate") == "") {
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtBkngFromDate").focus();

	} else if (dateValidDate(getText("txtBkngFromDate")) == false) {

		showERRMessage(arrError["bookedFromDtInvalid"]);
		getFieldByID("txtBkngFromDate").focus();

	} else if (getText("txtBkngToDate") == "") {
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtBkngToDate").focus();

	} else if (dateValidDate(getText("txtBkngToDate")) == false) {
		showERRMessage(arrError["bookedToDtInvalid"]);
		getFieldByID("txtBkngToDate").focus();

	} else if ((getText("txtDeptFromDate") != "")
			&& (dateValidDate(getText("txtDeptFromDate")) == false)) {

		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtDeptFromDate").focus();

	} else if ((getText("txtDeptToDate") != "")
			&& (dateValidDate(getText("txtDeptToDate")) == false)) {

		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtDeptToDate").focus();

	}else if ((getText("txtBkngFromDate") != "")&& (!checkWithSysDate("txtBkngFromDate"))) {
		
		showERRMessage(arrError["fromDtExceedsToday"]);				
		getFieldByID("txtBkngFromDate").focus();

	}else if ((getText("txtBkngToDate") != "")&& (!checkWithSysDate("txtBkngToDate"))) {
		
		showERRMessage(arrError["toDtExceedsToday"]);				
		getFieldByID("txtBkngToDate").focus();

	}else{
		if(isSchedule){
			scheduleReport();
		}else if(dateValidDate(getText("txtBkngFromDate"))==true && dateValidDate(getText("txtBkngToDate"))==true && validateDate()){
			setField("hdnMode","VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
			top.objCWindow = window.open("showBlank","CWindow",strProp);			
			var objForm  = document.getElementById("frmForceConfrmdRpt");
			objForm.target = "CWindow";
			objForm.action = "showForceConfirmedBookingReport.action";
			objForm.submit();			
			top[2].HidePageMessage();		
		}
	}
		
}

function validateDate(){	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
		
	var dateFrom = getText("txtBkngFromDate");
	var dateTo = getText("txtBkngToDate");
	
	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
				
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM;}
	if (dtCD < 10){dtCD = "0" + dtCD;}
	 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
		
	strSysODate=(tempYear+tempMonth+tempDay); 
	if(tempOStartDate > tempOEndDate){
		showERRMessage(arrError["fromDtExceed"]);
		getFieldByID("txtToDate").focus();
	} else {
		validate = true;				
	}
	return validate;	
}


function checkWithSysDate(strdate){
	
	var dateValue = getText(strdate);
	var tempDate = dateValue;
	
	tempDay=tempDate.substring(0,2);
	tempMonth=tempDate.substring(3,5);
	tempYear=tempDate.substring(6,10); 	
	var tempODate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
			
	if(tempODate > strSysODate){		
		return false;
	} else{		
		return true;
	}
}