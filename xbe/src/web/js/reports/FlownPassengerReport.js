$(document).ready(function(){
	
	
});

setField("hdnMode","VIEW");	
var screenId = "UC_REPM_099";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFromDate").focus();

	if (top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("txtFlightNumber", strSearch[5]);		
	}
}

function viewClick() {
	setField("hdnMode","VIEW");	
	viewReport(false);
}

function scheduleClick() {
	setField("hdnMode","SCHEDULE");	
	viewReport(true);
}

function viewReport(isSchedule){
	var startDate = getText("txtFromDate");
	var endDate = getText("txtToDate");
	
	if(startDate == ""){
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();
	}else if(endDate == ""){
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
	}else if(dateValidDate(startDate)==false){
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
	}else if(dateValidDate(endDate)==false){
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtFromDate").focus();
	} else if ((endDate != "" && startDate != "") && !CheckDates(startDate, endDate)){		
		showERRMessage(arrError["fltDatePerInvld"]);
		getFieldByID("txtToDate").focus();	
	} else if ((getText("txtFlightNumber") != "") &&  (!isFlightNoValid("txtFlightNumber"))) {			
		showERRMessage(arrError["flightNoInvalid"]);				
		getFieldByID("txtFlightNumber").focus();	
	} else {		
		
		var startD = startDate.substring(0,2);
		var startM = startDate.substring(3,5);
		var startY = startDate.substring(6,10);
		var endD = endDate.substring(0,2);
		var endM = endDate.substring(3,5);
		var endY = endDate.substring(6,10);
		
		if(Number(endY) - Number(startY) > 0){
			showERRMessage(arrError["searchValidityInvalid"]);
			return false;
		} else if(Number(endM) - Number(startM) > 1){
			showERRMessage(arrError["searchValidityInvalid"]);
			return false;
		} else if(Number(endM) - Number(startM) == 1){
			if(Number(endD) - Number(startD) > 0){
				showERRMessage(arrError["searchValidityInvalid"]);
				return false;
			}
		} else if(Number(endD) - Number(startD) > 31){
			showERRMessage(arrError["searchValidityInvalid"]);
			return false;
		}
		if(isSchedule){
			scheduleReport();
		} else {
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
			top.objWindow = window.open("showBlank","CWindow",strProp);
			var objForm = document.getElementById("frmFlownPassengerReport");
			objForm.target = "CWindow";	
			objForm.action = "showFlownPaxReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		}	
	}
}

function beforeUnload() {
	if ((top.objCWindow) && (!top.objCWindow.closed)) {
		top.objCWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function isFlightNoValid(txtfield) {	
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}

function scheduleReport(){
	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmFlownPassengerReport', 
		composerName: 'flownPassengerReport',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}