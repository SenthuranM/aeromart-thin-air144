var objWindow;
var value;
var screenId = "UC_REPM_091";
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();


function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}

function beforeUnload(){
	if ((top.objCWindow) && (!top.objCWindow.closed))	{
		top.objCWindow.close();
	}
}


function winOnLoad() {			
	
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDateFrom", strDate);
		break;
	case "1":
		setField("txtDateTo", strDate);
		break;
	}
}


function closeClick() {
	setPageEdited(false);
	objOnFocus();
	FareDetailsReport.strSearchCriteria = "";
	top.LoadHome();

}



function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}

	return newAgents;
}


function viewClick() {
	
	var reportType = getValue("radReportType");		 	
	setField("hdnMode", "VIEW");
	var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	var objForm = document.getElementById("frmTaxDetailReport");
	objForm.target = "CWindow";
	objForm.action = "showTaxDetailsReport.action";
	objForm.submit();
	top[2].HidePageMessage();		
}


function setWithoutCharges() {
	var rateCat = getText("radRateCat");
	if (rateCat == "NoCharges") {
		Disable("txtDateFrom", "true");
		Disable("txtDateTo", "true");
	} else if (rateCat == "WithDateRange") {		
		setField("txtDateFrom", "");
		setField("txtDateTo", "");
		Disable("txtDateFrom", "");
		Disable("txtDateTo", "");
	}
}


function objOnFocus() {
	top[2].HidePageMessage();
}


function LoadCalendar(strID, objEvent) {
	var status = getText("radRateCat");	
	if (status == "WithDateRange") {
		objCal1.ID = strID;
		objCal1.top = 100;
		objCal1.left = 650;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}
}

function LoadCalendarTo(strID, objEvent) {
	var status = getText("radRateCat");	
	if (status == "WithDateRange") {
		objCal1.ID = strID;
		objCal1.top = 100;
		objCal1.left = 700;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}
}




