var objWindow;
var screenId = "UC_REPM_094";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function beforeUnload() {
	if ((top.objCWindow) && (!top.objCWindow.closed)) {
		top.objCWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function radioOptionChanged() {
	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");

	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");

	}

}

function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFromDate").focus();

	if (top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");
		setField("txtFromDate", strSearch[0]);
		setField("selAgencies", strSearch[1]);

		if (strSearch[2] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[3] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		setField("selOriginStation", strSearch[4]);
		setField("selDestinationStation", strSearch[5]);

		if (getFieldByID("chkBase")) {
			if (strSearch[6] == 'true') {
				getFieldByID("chkBase").checked = true;
			} else {
				getFieldByID("chkBase").checked = false;
			}
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function changeAgencies() {
	ls.clear();
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else if (getValue("selAgencies") == 'SGSA' || getValue("selAgencies") == 'TA'){
		Disable('chkTAs',false);
		Disable('chkCOs',true);
		getFieldByID("chkCOs").checked = false;		
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}

}

function validateDate() {
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;

	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;

	tempDay = tempIStartDate.substring(0, 2);
	tempMonth = tempIStartDate.substring(3, 5);
	tempYear = tempIStartDate.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = tempIEndDate.substring(0, 2);
	tempMonth = tempIEndDate.substring(3, 5);
	tempYear = tempIEndDate.substring(6, 10);
	var tempOEndDate = (tempYear + tempMonth + tempDay);

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM;
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD;
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);

	strSysODate = (tempYear + tempMonth + tempDay);
	if (tempOStartDate <= tempOEndDate) {		
		validate = true;
	}
	return validate;
}

function getAgents() {
	var strAgents;
	if (getText("selAgencies") != "All") {
		strAgents = ls.getSelectedDataWithGroup();
	} else {
		strAgents = ls.getselectedData();
	}
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function viewClick() {
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", "");
	}

	if (getText("txtFromDate") == "") {
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();

	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
		
	}else if ((displayAgencyMode == 1) && getText("hdnAgents") == "") {
		showCommonError("Error", arrError["agentsRqrd"]);
	
	}else  {
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
		top.objCWindow = window.open("showBlank", "CWindow", strProp);
		var objForm = document.getElementById("frmPage");
		objForm.target = "CWindow";
		objForm.action = "showInternationalFlightDetails.action";
		objForm.submit();
		top[2].HidePageMessage();
	}

}

function checkDate(strdate){
	
	var dateValue = getText(strdate);
	var tempDate = dateValue;
	
	tempDay=tempDate.substring(0,2);
	tempMonth=tempDate.substring(3,5);
	tempYear=tempDate.substring(6,10); 	
	var tempODate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
			
	if(tempODate > strSysODate){		
		return false;
	} else{		
		return true;
	}
}


function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#"
			    + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#" + getValue("selOriginStation")
				+ "#" + getValue("selDestinationStation");
		if (getFieldByID("chkBase")) {
			strSearchCriteria += "#" + getFieldByID("chkBase").checked;
		} else {
			strSearchCriteria += "#" + false;
		}
		top.strSearchCriteria = strSearchCriteria;
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmPage");
		objForm.target = "_self";
		document.forms[0].submit();
		ShowProgress();
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}