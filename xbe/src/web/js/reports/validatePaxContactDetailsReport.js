	var objWindow;
	var screenId="UC_REPM_051";
	setField("hdnLive",repLive);	
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.blnDragCalendar = false;
	objCal1.buildCalendar();
	
	function setBookingDate(strDate, strID) {
		switch (strID) {
			case "0" : setField("txtFromDate",strDate);break ;
			case "1" : setField("txtToDate",strDate);break ;
		}
	}
	
	function setFlightDate(strDate, strID) {
		switch (strID) {
			case "0" : setField("txtFlightFromDate",strDate);break ;
			case "1" : setField("txtFlightToDate",strDate);break ;
		}
	}
	
	function beforeUnload() {
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
	}

	function LoadBookingCalendar(strID, objEvent) {
		objCal1.ID = strID;
		objCal1.top = 1 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setBookingDate";
		objCal1.showCalendar(objEvent);
	}
	
	function LoadFlightCalendar(strID, objEvent) {
		objCal1.ID = strID;
		objCal1.top = 1 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setFlightDate";
		objCal1.showCalendar(objEvent);
	}
		
	function closeClick() {
		if (top.loadCheck(top.pageEdited)){
			setPageEdited(false);
			top.strSearchCriteria = "";
			top.LoadHome();
		}
	}

	function setPageEdited(isEdited) {
		top.pageEdited = isEdited;
	}
	
	function validateDate() {	
		var tempDay;
		var tempMonth;
		var tempYear;
		var validate = false;
		
		var dateFrom = getText("txtFromDate");
		var dateTo = getText("txtToDate");
	
		tempIStartDate = dateFrom;
		tempIEndDate = dateTo;
				
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}
	 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
		
		strSysODate=(tempYear+tempMonth+tempDay);	
		
		if(tempOStartDate > tempOEndDate){
			showERRMessage(arrError["fromDtExceed"]);
			getFieldByID("txtToDate").focus();
		} else if (!CheckDates(repStartDate,getText("txtFromDate"))) {
			showERRMessage(arrError["rptReriodExceeds"] +" "+ repStartDate);
			getFieldByID("txtFromDate").focus();			
		} else {
			validate = true;				
		}
		return validate;	
	}

	function viewClick() {
		
		if (displayAgencyMode == 1 || displayAgencyMode == 2) {
			setField("hdnAgents", getAgents());
		} else {
			setField("hdnAgents", "");
		}
	
		if(checkEmptyBookingDates() && checkEmptyFlightDates()){
			showCommonError("Error", "TAIR-71499: At least one date range should be selected.");
			getFieldByID("txtFromDate").focus();
		}
		else{
			if(!checkEmptyBookingDates()){					
				if (getText("txtFromDate") == "") {
					showCommonError("Error", "From Date cannot be Empty");
					getFieldByID("txtFromDate").focus();
				} else if (dateValidDate(getText("txtFromDate")) == false) {
					showCommonError("Error", fromDtInvalid);
					getFieldByID("txtFromDate").focus();
				} else if (getText("txtToDate") == "") {
					showCommonError("Error", "To Date cannot be Empty");
					getFieldByID("txtToDate").focus();
				} else if (dateValidDate(getText("txtToDate")) == false) {
					showCommonError("Error", toDtinvalid);
					getFieldByID("txtToDate").focus();
				} else if(!checkDate("txtFromDate")){
					showCommonError("Error", "Booking date range should be less then today date");
					getFieldByID("txtFromDate").focus();
					return false;
				} else if(!checkDate("txtToDate")){
					showCommonError("Error", "Booking date range should be less then today date");
					getFieldByID("txtToDate").focus();
					return false;
				}
			}
			if(!checkEmptyFlightDates()){				
				if (getText("txtFlightFromDate") == "") {
					showCommonError("Error", "From Date cannot be Empty");
					getFieldByID("txtFlightFromDate").focus();
					return false;
				} else if (dateValidDate(getText("txtFlightFromDate")) == false) {
					showCommonError("Error", fromDtInvalid);
					getFieldByID("txtFlightFromDate").focus();
				} else if (getText("txtFlightToDate") == "") {
					showCommonError("Error", "To Date cannot be Empty");
					getFieldByID("txtFlightToDate").focus();
					return false;
				} else if (dateValidDate(getText("txtFlightToDate")) == false) {
					showCommonError("Error", toDtinvalid);
					getFieldByID("txtFlightToDate").focus();
				}
			}
		}
		/** End of JIRA : AARESAA3016  */
		if (getText("selPrintDetailsOf") == "") {
			showCommonError("Error", printDTinvalid);
			getFieldByID("selPrintDetailsOf").focus();
		} else if (validateReportingPeriod()) {
					
			setField("hdnMode","VIEW");		
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
			top[0].objWindow = window.open("about:blank","CWindow",strProp);			
			var objForm  = document.getElementById("frmPaxContactDetails");
			objForm.target = "CWindow";
			
			objForm.action = "showPaxContactDetailsReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		
		}
	}
	
	function checkDate(strdate){
		
		var dateValue = getText(strdate);
		var tempDate = dateValue;
		
		tempDay=tempDate.substring(0,2);
		tempMonth=tempDate.substring(3,5);
		tempYear=tempDate.substring(6,10); 	
		var tempODate=(tempYear+tempMonth+tempDay);			

		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}
	 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
		
		strSysODate=(tempYear+tempMonth+tempDay); 
				
		if(tempODate > strSysODate){		
			return false;
		} else{		
			return true;
		}
	}

	function load(){		
		getFieldByID("txtFromDate").focus();
		if(top.strSearchCriteria != "" 
				&& top.strSearchCriteria != null) {
			var strSearch=top.strSearchCriteria.split("#");
			setField("txtFromDate",strSearch[0]);
			setField("txtToDate",strSearch[1]);
			setField("selAgencies",strSearch[2]);
			setField("txtFlightNumber",strSearch[5]);
			setField("txtFlightFromDate",strSearch[6]);
			setField("txtFlightToDate",strSearch[7]);
			if (strSearch[3] == 'true') {
				getFieldByID("chkTAs").checked = true;
			} else {
				getFieldByID("chkTAs").checked = false;		
				Disable('chkTAs',true);
			}
			if (strSearch[4] == 'true') {
				getFieldByID("chkCOs").checked = true;
			}else {
				getFieldByID("chkCOs").checked = false;		
				Disable('chkCOs',true);
			}
		}else {
			Disable('chkTAs',true);
			Disable('chkCOs',true);
		}	
	} 

	function radioOptionChanged(){
	
		if(getText("radAgencey")=="Agencey") {
			Disable("selAgencies","true");
			Disable("btnGetAgent","true");			
		}else {
			Disable("selAgencies","");
			Disable("btnGetAgent","");			
		}	
	}

	function clickAgencies(){
		    
		if (getValue("selAgencies") == 'GSA') {
			Disable('chkTAs',false);
			Disable('chkCOs',false);
		} else if (getValue("selAgencies") == 'SGSA' || getValue("selAgencies") == 'TA'){
			Disable('chkTAs',false);
			Disable('chkCOs',true);
			getFieldByID("chkCOs").checked = false;		
		} else {
			Disable('chkTAs',true);
			getFieldByID("chkTAs").checked = false;
			Disable('chkCOs',true);
			getFieldByID("chkCOs").checked = false;			
		}		
	}

	function changeAgencies(){
		//ls.clear();		
	}

	function getAgents() {
		var strAgents;
		if(getText("selAgencies")!="All") {
			 strAgents=ls.getSelectedDataWithGroup();
		}else {			
			strAgents=ls.getselectedData();
		}
		var newAgents;
		if(strAgents.indexOf(":")!=-1){
			strAgents=replaceall(strAgents,":" , ",");
		}
		if(strAgents.indexOf("|")!=-1){
			newAgents=replaceall(strAgents,"|" , ",");
		}else{
			newAgents=strAgents;
		}
		return newAgents;
	}

	function getAgentClick() {
		if(getValue("selAgencies") == "") {
			showERRMessage(arrError["agentTypeRqrd"]);			
			getFieldByID("selAgencies").focus();
		}else {
			var strSearchCriteria = getValue("txtFromDate")+"#"+getValue("txtToDate")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getFieldByID("chkCOs").checked+"#"+getValue("txtFlightNumber")+"#"+getValue("txtFlightFromDate")+"#"+getValue("txtFlightToDate");
			top.strSearchCriteria = strSearchCriteria;
			setField("hdnAgents","");
			setField("hdnMode","SEARCH");		
			var objForm  = document.getElementById("frmPaxContactDetails");
			objForm.target = "_self";
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
	
	function checkEmptyFlightDates(){
		
		var fromFlightDate =  getText("txtFlightFromDate");
		var toFlightDate =  getText("txtFlightToDate");
		if(fromFlightDate=="" && toFlightDate=="")
			return true;
		else
			return false;
		
			
	}
	
	function checkEmptyBookingDates(){
		var fromBookingDate = getText("txtFromDate");
		var toBookingDate  = getText("txtToDate");
		if(fromBookingDate=="" && toBookingDate=="")
			return true;
		else
			return false;
	}

	function validateBookingDates(){
		if(dateValidDate(getText("txtFromDate"))==true && dateValidDate(getText("txtToDate")) == true)
			return true;
		else
			return false;
		}

	function validateFlightDates(){
		if(dateValidDate(getText("txtFlightFromDate"))==true && dateValidDate(getText("txtFlightToDate")) == true)
			return true;
		else
			return false;
		
	}

	function validateReportingPeriod(){
		if(!checkEmptyBookingDates()){
			if(validateBookingDates()==true && validateReportDate("txtFromDate", "txtToDate", repStartDate) &&
					validateReportDate("txtFlightFromDate", "txtFlightToDate", repStartDate))
				return true;
			else
				return false;
		}
		else if(!checkEmptyFlightDates()){
			if(validateFlightDates()==true && validateReportDate("txtFromDate", "txtToDate", repStartDate) &&
					validateReportDate("txtFlightFromDate", "txtFlightToDate", repStartDate))
				return true;
			else
				return false;
		}
		else{
			return false;
		}
	}

	function validateReportDate(strFromDate, strTodate, repStartDate){
		var tempDay;
		var tempMonth;
		var tempYear;
		var validate = false;
		
		var dateFrom = getText(strFromDate);
		var dateTo = getText(strTodate);

		tempIStartDate = dateFrom;
		tempIEndDate = dateTo;
				
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);			

		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}
	 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
		
		strSysODate=(tempYear+tempMonth+tempDay); 
		
		var rptReriodExceeds = "Report period cannot be less than "		
		if(tempOStartDate > tempOEndDate){
			showCommonError("Error","To Date cannot be less than from date");
			getFieldByID(strTodate).focus();
		} else if (dateFrom != '' && !CheckDates(repStartDate,getText(strFromDate))) {
			showCommonError("Error",rptReriodExceeds +" "+ repStartDate);
			getFieldByID(strFromDate).focus();			
		}
		else {
			validate = true;				
		}
		

		return validate;	
	}
 
