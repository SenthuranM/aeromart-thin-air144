var objWindow;
var screenId = "UC_REPM_028";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function beforeUnload() {
	if ((top.objCWindow) && (!top.objCWindow.closed)) {
		top.objCWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function radioOptionChanged() {
	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");

	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");

	}

}

function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFromDate").focus();

	if (top.strSearchCriteria != "" && top.strSearchCriteria != null
			&& top.strSearchCriteria.split("#").length == 7) {
		var strSearch = top.strSearchCriteria.split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		lscc.selectedData(strSearch[5]);

		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[4] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
//		setField("selStation", strSearch[5]);
//		setField("selJourneyType", strSearch[6]);

		if (getFieldByID("chkBase")) {
			if (strSearch[7] == 'true') {
				getFieldByID("chkBase").checked = true;
			} else {
				getFieldByID("chkBase").checked = false;
			}
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function changeAgencies() {
	ls.clear();
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else if (getValue("selAgencies") == 'SGSA' || getValue("selAgencies") == 'TA'){
		Disable('chkTAs',false);
		Disable('chkCOs',true);
		getFieldByID("chkCOs").checked = false;		
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}

}

function validateDate() {
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;

	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;

	tempDay = tempIStartDate.substring(0, 2);
	tempMonth = tempIStartDate.substring(3, 5);
	tempYear = tempIStartDate.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = tempIEndDate.substring(0, 2);
	tempMonth = tempIEndDate.substring(3, 5);
	tempYear = tempIEndDate.substring(6, 10);
	var tempOEndDate = (tempYear + tempMonth + tempDay);

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM;
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD;
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);

	strSysODate = (tempYear + tempMonth + tempDay);
	if (tempOStartDate <= tempOEndDate) {		
		validate = true;
	}
	return validate;
}

function getAgents() {
	var strAgents;
	if (getText("selAgencies") != "All") {
		strAgents = ls.getSelectedDataWithGroup();
	} else {
		strAgents = ls.getselectedData();
	}
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function viewClick() {
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", "");
	}
	
	setField("hdnChargeAdjustmentChargeCodes",lscc.getselectedData());

	if (getText("txtFromDate") == "") {
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();

	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();

	} else if (getText("txtToDate") == "") {
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();

	} else if (dateValidDate(getText("txtToDate")) == false) {
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtToDate").focus();
	} else if (!checkDate("txtFromDate")) {
		showCommonError("Error",
				arrError["fromDtExceedsToday"]);
		getFieldByID("txtFromDate").focus();
		return false;
	} else if (!checkDate("txtToDate")) {
		showCommonError("Error",
				arrError["toDtExceedsToday"]);
		getFieldByID("txtToDate").focus();
		return false;
	} else if(!validateDate()){
		showCommonError("Error",
				arrError["fromDtExceed"]);
		getFieldByID("txtToDate").focus();
		return false;
	} else if(lscc.getselectedData()==""){
		showERRMessage(arrError["adjustmentChargeCodesRqrd"]);
	} else if((displayAgencyMode == 1 || displayAgencyMode == 2) && getText("hdnAgents")==""){
		showERRMessage(arrError["agentsRqrd"]);		
	} 

	else  {
		
		if(offlineReportParams != ""){
			var offlineReportParamArr = offlineReportParams.split(",");
			var isOfflineReportEnabled = (offlineReportParamArr[0] == "Y") ? true:false;
			if(isOfflineReportEnabled){
				var noOfDaysToLiveReport = offlineReportParamArr[1];
				var currentSystemDate = stringToDate(systemDate);
				var validFromDate = DateToString(addDays(currentSystemDate, -1 * noOfDaysToLiveReport));
				var selectedToDate = getText("txtToDate");
				var selectedFromDate = getText("txtFromDate");	
				
				if(compareDate(selectedToDate, validFromDate) != -1 && compareDate(selectedFromDate, validFromDate) == -1){					
					showERRMessage(arrError["invalidLiveReport"]);
					getFieldByID("txtToDate").focus();
					return false;				
				}
			}
		}
		
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
		top.objCWindow = window.open("showBlank", "CWindow", strProp);
		var objForm = document.getElementById("frmPage");
		objForm.target = "CWindow";
		objForm.action = "showChargeAdjustmentsReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function checkDate(strdate){
	
	var dateValue = getText(strdate);
	var tempDate = dateValue;
	
	tempDay=tempDate.substring(0,2);
	tempMonth=tempDate.substring(3,5);
	tempYear=tempDate.substring(6,10); 	
	var tempODate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
			
	if(tempODate > strSysODate){		
		return false;
	} else{		
		return true;
	}
}


function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#"
				+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#" + lscc.getselectedData();
		if (getFieldByID("chkBase")) {
			strSearchCriteria += "#" + getFieldByID("chkBase").checked;
		} else {
			strSearchCriteria += "#" + false;
		}
		top.strSearchCriteria = strSearchCriteria;
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmPage");
		objForm.target = "_self";
		document.forms[0].submit();
		ShowProgress();
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}