var objWindow;
	
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtFromDate",strDate);break ;
		case "1" : setField("txtToDate",strDate);break ;
		case "2" : setField("txtBookedFromDate",strDate);break;
		case "3" : setField("txtBookedToDate",strDate);break;
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}

var screenId="UC_REPM_015";
	
	
function closeClick() {
	if (top.loadCheck(top.pageEdited)){
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function beforeUnload(){
		if ((top.objCWindow) && (!top.objCWindow.closed))	{
			top.objCWindow.close();
		}
}
function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}

function reportTypeOnChage(){
	var currval = getValue("selReportType");
	setField("txtBookedFromDate", "");
	setField("txtBookedToDate", "");
	if(currval == "BY_AGENT"){
		setDisplay("tblBookedDateOpt", true);
		document.getElementById("departureMandatory").style.display = "block";
		document.getElementById("arrivalMandatory").style.display = "block";
		
	} else {
		setDisplay("tblBookedDateOpt", false);
		document.getElementById("departureMandatory").style.display = "none";
		document.getElementById("arrivalMandatory").style.display = "none";
	}
}

function validateDate(fromDate, toDate){	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
	
	var dateFrom = getText(fromDate);
	var dateTo = getText(toDate);

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
			
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
	
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM;}
	if (dtCD < 10){dtCD = "0" + dtCD;}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 		
	
	if(tempOStartDate > tempOEndDate){
		showERRMessage(arrError["fromDtExceed"]);
		
		getFieldByID(toDate).focus();
	} else {
		validate = true;				
	}
	return validate;	
}

function viewClick(){
	var rptVal = getValue("selReportType");
	var isValidateSuccess = false;
	if(rptVal == ""){
		showERRMessage(arrError["rptTypeRqrd"]);
		getFieldByID("selReportType").focus();
	} else if(rptVal == "BY_AGENT"){
		if (getText("txtFromDate") == "") {
			showERRMessage(arrError["fromDtEmpty"]);
			getFieldByID("txtFromDate").focus();
		} else if (dateValidDate(getText("txtFromDate")) == false) {
			showERRMessage(arrError["fromDtInvalid"]);
			getFieldByID("txtFromDate").focus();
		} else if (getText("txtToDate") == "") {
			showERRMessage(arrError["toDtEmpty"]);
			getFieldByID("txtToDate").focus();
		} else if (dateValidDate(getText("txtToDate")) == false) {
			showERRMessage(arrError["toDtinvalid"]);
			getFieldByID("txtToDate").focus();
		} else if (validateDate("txtFromDate", "txtToDate")) {
			if (getText("txtBookedFromDate") != "" && dateValidDate(getText("txtBookedFromDate")) == false) {
			    showERRMessage(arrError["bookedFromDtInvalid"]);
			    getFieldByID("txtBookedFromDate").focus();
			} else if (getText("txtBookedToDate") != "" && dateValidDate(getText("txtBookedToDate")) == false) {
				showERRMessage(arrError["bookedToDtInvalid"]);
				getFieldByID("txtBookedToDate").focus();
			} else if (getText("txtBookedToDate") != "" && getText("txtBookedFromDate") == "") {
				showERRMessage(arrError["fromDtEmpty"]);
				getFieldByID("txtBookedFromDate").focus();
			} else if (getText("txtBookedFromDate") != "" && getText("txtBookedToDate") == "") {
				showERRMessage(arrError["toDtEmpty"]);
				getFieldByID("txtBookedToDate").focus();
			} else if (getText("txtBookedFromDate") != "" && getText("txtBookedToDate") != "" 
				&& !validateDate("txtBookedFromDate", "txtBookedToDate")) {
			    showERRMessage(arrError["bookedFromDtInvalid"]);
			    getFieldByID("txtBookedFromDate").focus();
			} else if(getText("selDept")== ""){
				showERRMessage(arrError["depatureRqrd"]);
			    getFieldByID("selDept").focus();
			} else if (getText("selArival")== ""){
				showERRMessage(arrError["arrivalRqrd"]);
			    getFieldByID("selArival").focus();
			} else {
				isValidateSuccess = true;
			}
		} else {
			showERRMessage(arrError["fromDtInvalid"]);
			getFieldByID("txtFromDate").focus();
		}
	} else if(rptVal == "BY_FLIGHT" || rptVal == "BY_STATION"){
		if (getText("txtFromDate") == "") {
			showERRMessage(arrError["fromDtEmpty"]);
			getFieldByID("txtFromDate").focus();
		} else if (dateValidDate(getText("txtFromDate")) == false) {
			showERRMessage(arrError["fromDtInvalid"]);
			getFieldByID("txtFromDate").focus();
		} else if (getText("txtToDate") == "") {
			showERRMessage(arrError["toDtEmpty"]);
			getFieldByID("txtToDate").focus();
		} else if (dateValidDate(getText("txtToDate")) == false) {
			showERRMessage(arrError["toDtinvalid"]);
			getFieldByID("txtToDate").focus();
		} else if(validateDate("txtFromDate", "txtToDate")){
			isValidateSuccess = true;
		}
	}
	
	if(isValidateSuccess){
		if(getText("selDept")==getText("selArival") 
				&& getText("selDept") !="" && getText("selArival") !=""  ){
			showERRMessage(arrError["sameSegment"]);
			getFieldByID("selArival").focus();
			
		} else if(getText("selDept")== getText("selVia1") 
				&& getText("selDept") !="" && getText("selVia1") !="" 
				|| getText("selDept") == getText("selVia2") 
				&& getText("selDept") !="" && getText("selVia2") !="" 
				|| getText("selDept") == getText("selVia3") 
				&& getText("selDept") !="" && getText("selVia3") !="" 
				|| getText("selDept") == getText("selVia4") 
				&& getText("selDept") !="" && getText("selVia4") !=""){
			showERRMessage(arrError["sameViaDept"]);
			getFieldByID("selDept").focus();
			
		} else if(getText("selArival")== getText("selVia1") 
				&& getText("selArival") !="" && getText("selVia1") !=""
				|| getText("selArival") == getText("selVia2") 
				&& getText("selArival") !="" && getText("selVia2") !="" 
				|| getText("selArival") == getText("selVia3") 
				&& getText("selArival") !="" && getText("selVia3") !="" 
				|| getText("selArival") == getText("selVia4") 
				&& getText("selArival") !="" && getText("selVia4") !=""){
			showERRMessage(arrError["sameViaArival"]);
			getFieldByID("selVia2").focus();
			
		} else if(getText("selVia1")!="" 
				&&  ( getText("selVia1") == getText("selVia2") 
				|| getText("selVia1") == getText("selVia3") || getText("selVia1") == getText("selVia4")) ){
			showERRMessage(arrError["sameVia"]);
			getFieldByID("selVia1").focus();
			
		} else if(getText("selVia2")!="" 
				&&  ( getText("selVia2") == getText("selVia3") || getText("selVia2") == getText("selVia4")) ){
			showERRMessage(arrError["sameVia"]);
			getFieldByID("selVia2").focus();
			
		} else if(getText("selVia3")!="" 
				&&  ( getText("selVia3") == getText("selVia4")) ){
			showERRMessage(arrError["sameVia"]);
			getFieldByID("selVia3").focus();
		}else if(validateCarrierCode()) {
					
			setField("hdnMode","VIEW");		
			
			if(showCarrierCombo) {
				setField("hdnCarrierCode",lscc2.getselectedData());
			}
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
			top[0].objWindow = window.open("about:blank","CWindow",strProp);			
			var objForm  = document.getElementById("frmViewSeatInventoryCollections");
			objForm.target = "CWindow";				
			objForm.action = "showViewSeatInventoryCollection.action";
			objForm.submit();
			top[2].HidePageMessage();
		
		}
	}
}

function validateCarrierCode() {
	var validate = false;
	if(!showCarrierCombo || trim(lscc2.getselectedData()).length > 0) {
		validate = true;
	} else {
		showERRMessage(arrError["carrierCodeRqrd"]);
		
	}
	return validate;
}

function pageLoad(){
	setDisplay("tblBookedDateOpt", false);
	getFieldByID("selReportType").focus();
	//setField("txtCarrierCode", "G9");		
	//getFieldByID("selReportType").focus();
	//setField("txtToDate", strSysDate);		
	//Disable("txtFlightNumber",true);
	//Disable("selVia1",true);
	//Disable("selVia2",true);
	//Disable("selVia3",true);
	//Disable("selVia4",true);
	//Disable("selDept",true);
	
	if (showCarrierCombo) {
					
		document.getElementById("carrierCode").style.display = "block";
		var listBox = document.getElementById("lstCarrierCodes1");		
		
		var listBox2 = document.getElementById("lstAssignedCarrierCodes1");
		listBox2 = lscc2;		
	
		if(methods.length > 0) {			
			for(var i=0; i<listBox.options.length; i++) {			
				if(listBox.options[i].value == defaultCarrier) {											
					listBox2.selectedData(defaultCarrier);
					
				} 
			}
		}
	} 
}

function chkClick(){			
	
}