var objWindow;
var screenId="UC_REPM_023";
var objCal1 = new Calendar("spnCalendarDG1");
var selectedAllAgents = false;
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtFromDate",strDate);break ;
		case "1" : setField("txtToDate",strDate);break ;
	}
}

function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}

function beforeUnload(){
	if ((top.objCWindow) && (!top.objCWindow.closed))	{
		top.objCWindow.close();
	}
}
	
function closeClick() {
	if (top.loadCheck(top.pageEdited)){
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function radioOptionChanged(){
	if(getText("radAgencey")=="Agencey"){
		Disable("selAgencies","true");
		Disable("btnGetAgent","true");
		
	}else{
		Disable("selAgencies","");
		Disable("btnGetAgent","");
		
	}

}

function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFromDate").focus();
	if(repShowpay == 'true') {		
		document.getElementById('divPay').style.display= 'block';
	}else {
		document.getElementById('divPay').style.display= 'none';		
	}
	if (modDetailsEnabled == 'false') {
		setVisible("chkModifications", false);
		document.getElementById('divModifications').style.display = 'none';
	}
	if(top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch=top.strSearchCriteria.split("#");
		setField("txtFromDate",strSearch[0]);
		setField("txtToDate",strSearch[1]);
		setField("selAgencies",strSearch[2]);
		lspm.selectedData(strSearch[3]);
		var subAgentTypes=strSearch[4].split(",");
		$('#selRepotingAgentTypes').html("");
		$('#selRepotingAgentTypes').html(agentSubtypes[$('#selAgencies').val()]);		
		$("#selRepotingAgentTypes").val(subAgentTypes);
//		if (strSearch[3] == 'true')	{
//			getFieldByID("chkTAs").checked = true;
//		}else {
//			getFieldByID("chkTAs").checked = false;		
//			Disable('chkTAs',true);
//		}
//		if (strSearch[5] == 'true') {
//			getFieldByID("chkCOs").checked = true;
//		}else {
//			getFieldByID("chkCOs").checked = false;		
//			Disable('chkCOs',true);
//		}
		if(getFieldByID("chkBase")){
			if (strSearch[5] == 'true') {
				getFieldByID("chkBase").checked = true;
			}else {
				getFieldByID("chkBase").checked = false;			
			}
		}
		if(getFieldByID("chkSales")){
			if (strSearch[6] == 'true') {
				getFieldByID("chkSales").checked = true;
			}else {
				getFieldByID("chkSales").checked = false;			
			}
		}
		if(getFieldByID("chkRefunds")){
			if (strSearch[7] == 'true') {
				getFieldByID("chkRefunds").checked = true;
			}else {
				getFieldByID("chkRefunds").checked = false;			
			}
		}
		setField("selFlightType",strSearch[8]);
		if(getFieldByID("chkModifications")){
			if (strSearch[9] == 'true') {
				getFieldByID("chkModifications").checked = true;
			}else {
				getFieldByID("chkModifications").checked = false;			
			}
		}
		setField("selFareDiscountCode",strSearch[10]);
		if(getFieldByID("chkLocalTime")){
			if (strSearch[11] == 'true') {
				getFieldByID("chkLocalTime").checked = true;
			}else {
				getFieldByID("chkLocalTime").checked = false;			
			}
		}
		setField("agentStatusFilter",strSearch[12]);
		if(stationFilterEnabled == "true"){
			stls.selectedData(strSearch[13]);
		}
	}else {
//		Disable('chkTAs',true);
//		Disable('chkCOs',true);
	}
	$('#selAgencies').on('change', function () {
		$('#selRepotingAgentTypes').html("");
		$('#selRepotingAgentTypes').html(agentSubtypes[this.value]);
	});
}

function changeAgencies(){
	ls.clear();		
}

function clickAgencies(){	    
//	 if (getValue("selAgencies") == 'GSA'){
//		Disable('chkTAs',false);
//		Disable('chkCOs',false);
//	}else if (getValue("selAgencies") == 'SGSA' || getValue("selAgencies") == 'TA'){
//		Disable('chkTAs',false);
//		Disable('chkCOs',true);
//		getFieldByID("chkCOs").checked = false;		
//	}else{
//		Disable('chkTAs',true);
//		getFieldByID("chkTAs").checked = false;	
//		Disable('chkCOs',true);
//		getFieldByID("chkCOs").checked = false;		
//	}
	
}


function validateDate(){	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
		
	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");
	
	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
				
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM;}
	if (dtCD < 10){dtCD = "0" + dtCD;}
	 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
		
	strSysODate=(tempYear+tempMonth+tempDay); 
	if(tempOStartDate > tempOEndDate){
//		showCommonError("Error",fromDtExceed);
		showERRMessage(arrError["fromDtExceed"]);
		getFieldByID("txtToDate").focus();
	} else {
		validate = true;				
	}
	return validate;	
}


function getAgents(){
	var strAgents;
//	if(getText("selAgencies")!="All"){
		 strAgents=ls.getSelectedDataWithGroup();
/*	}else{		
		strAgents=ls.getselectedData();
		if(ls.getNotSelectedDataWithGroup() == "" && strAgents != ""){
			selectedAllAgents = true;
			return "";
		}
	}*/
	var newAgents;
	if(strAgents.indexOf(":")!=-1){
		strAgents=replaceall(strAgents,":" , ",");
	}
	if(strAgents.indexOf("|")!=-1){
		newAgents=replaceall(strAgents,"|" , ",");
	}else{
		newAgents=strAgents;
	}
	return newAgents;
}


function viewClick(){
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents",getAgents());
	} else {
		setField("hdnAgents","");
	}
	
	if(stationFilterEnabled == "true"){
		setField("hdnStations", stls.getselectedData());
	}else{
		setField("hdnStations", "");
	}
	
	setField("hdnPayments",lspm.getselectedData());
	setField("hdnReportView","SUMMARY");
	if(getText("txtFromDate")==""){
		showERRMessage(arrError["fromDtEmpty"]);
		
		getFieldByID("txtFromDate").focus();
		
	} else if(dateValidDate(getText("txtFromDate"))==false){
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
		
	}  else if(getText("txtToDate")==""){
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
		
	} else if(dateValidDate(getText("txtToDate"))==false){
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtToDate").focus();
	} else if(lspm.getselectedData()==""){
		showERRMessage(arrError["paymentmodesRqrd"]);

	} else if((stationFilterEnabled == "false") && displayAgencyMode == 1  && getText("selAgencies")==""){
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();

	} else if ((stationFilterEnabled == "true")
			&& ((displayAgencyMode == 1 || displayAgencyMode == 2) && (getText("hdnAgents") == "" && !selectedAllAgents))
			&& stls.getselectedData() == "") {
		showERRMessage(arrError["agentsOrStationsRqrd"]);
	} 
	else if((stationFilterEnabled == "false")
			&& (displayAgencyMode == 1 || displayAgencyMode == 2) && (getText("hdnAgents") == "" && !selectedAllAgents)){
		showERRMessage(arrError["agentsRqrd"]);
		
	} else if(!getFieldByID("chkSales").checked && !getFieldByID("chkRefunds").checked  && !getFieldByID("chkModifications").checked){
		showERRMessage(arrError["salesOrRefundsNotSelected"]);
		getFieldByID("chkSales").focus();
	} else if(dateValidDate(getText("txtFromDate"))==true && dateValidDate(getText("txtToDate"))==true && validateDate()) {
		
		if(offlineReportParams != ""){
			var offlineReportParamArr = offlineReportParams.split(",");
			var isOfflineReportEnabled = (offlineReportParamArr[0] == "Y") ? true:false;
			if(isOfflineReportEnabled){
				var noOfDaysToLiveReport = offlineReportParamArr[1];
				var currentSystemDate = stringToDate(systemDate);
				var validFromDate = DateToString(addDays(currentSystemDate, -1 * noOfDaysToLiveReport));
				var selectedToDate = getText("txtToDate");
				var selectedFromDate = getText("txtFromDate");	
				
				if(compareDate(selectedToDate, validFromDate) != -1 && compareDate(selectedFromDate, validFromDate) == -1){					
					showERRMessage(arrError["invalidLiveReport"]);
					getFieldByID("txtToDate").focus();
					return false;				
				}
			}
		}
		 
		if(selectedAllAgents){
			setField("hdnSelectedAllAgents", "true");
			//setField("hdnAgents","");
		} else {
			setField("hdnSelectedAllAgents", "false");
		}
		
		
		setField("hdnMode","VIEW");
		//setField("hdnRptType","SUMMARY");
		setField("hdnEntityText",getText("selEntity"));
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
		top.objCWindow = window.open("showBlank","CWindow",strProp);			
		var objForm  = document.getElementById("frmPage");
		objForm.target = "CWindow";
		objForm.action = "showCompanyPaymentReport.action";
		objForm.submit();			
		top[2].HidePageMessage();		
	}
	
}

function getAgentClick(){
	if(getValue("selAgencies") == ""){
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	}else{
		
		$("#hdnReportingAgentTypes").val("");
		$('#selRepotingAgentTypes :selected').each(function(i, selected){
			if ($("#hdnReportingAgentTypes").val() == "") {
				$("#hdnReportingAgentTypes").val(this.value);
			} else {
				$("#hdnReportingAgentTypes").val($("#hdnReportingAgentTypes").val()+","+this.value);
			}
		});
		var strSearchCriteria = getValue("txtFromDate")
						+"#"+getValue("txtToDate")
						+"#"+getValue("selAgencies")
//						+"#"+getFieldByID("chkTAs").checked
						+"#"+lspm.getselectedData()
						+"#"+$("#hdnReportingAgentTypes").val();
//						+"#"+getFieldByID("chkCOs").checked;
		if(getFieldByID("chkBase")){
			strSearchCriteria += "#"+getFieldByID("chkBase").checked;
		}else {
			strSearchCriteria += "#"+false;
		}
		strSearchCriteria += "#" + getFieldByID("chkSales").checked + "#"
				+ getFieldByID("chkRefunds").checked + "#"
				+ getValue("selFlightType") + "#"
				+ getFieldByID("chkModifications").checked + "#"
				+ getValue("selFareDiscountCode") + "#"
				+ getFieldByID("chkLocalTime").checked + "#"
				+ getValue("agentStatusFilter") + "#" + stls.getselectedData();
		
		top.strSearchCriteria = strSearchCriteria;
		//ls.removeAllFromListbox();
		setField("hdnAgents","");
		setField("hdnMode","SEARCH");
		var objForm  = document.getElementById("frmPage");
		objForm.target = "_self";
		document.forms[0].submit();
		ShowProgress();
	}
}

function objOnFocus(){
	top[2].HidePageMessage();
}