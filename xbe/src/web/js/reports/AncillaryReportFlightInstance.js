var invScreenID = "SC_RESV_CC_016";
var tabValue = getTabValue();
var flightNo;
var fltDate;
var fltRoute;
var objProgressCheck;
var isPaxDetail;
var strGridRow;
var strRowData;

var hdnMode = "";
var fliData = new Array();
var hdnRecNo = 1;
var totalRecNo = 0;
var recstNo = 1;
var cabinClasses;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtStartDateSearch", strDate);
		break;
	case "1":
		setField("txtStopDateSearch", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	if (strID == 0) {
		objCal1.top = 20;
		objCal1.left = 75;
	} else if (strID == 1) {
		objCal1.top = 20;
		objCal1.left = 175;
	}
	objCal1.onClick = "setDate";
	if ((strID == 0) && (!getFieldByID("txtStartDateSearch").disabled)) {
		objCal1.showCalendar(objEvent);
	} else if ((strID == 1) && (!getFieldByID("txtStopDateSearch").disabled)) {
		objCal1.showCalendar(objEvent);
	} else if (blnCalander) {
		objCal1.showCalendar(objEvent);
	}
}

function LoadCalendarNew(strID, objEvent) {
	objCal.top = 0;
	objCal.left = 20;
	objCal.ShowCalendar(objEvent);
	objCal.onClick = "setDate";
}

function dataChanged() {
	setPageEdited(true);
}

// call the functoion onload
function winOnLoad(strMsg, strMsgType, strWarnMsg) {
	//document.getElementById("txtFlightNoStartSearch").innerHTML = parent.carrierCodeOptions;
	isPaxDetail = parent.isPrintPaxDet;
	// FIXME
	// Disable("btnLoad", true);
	
	setField("selOperationTypeSearch", parent.defaultOperationType); // Search
	setField("selStatusSearch", parent.defaultStatus);
	setField("txtFlightNoStartSearch", parent.defCarrCode);
	strMsg = parent.strMsg;
	setPageEdited(false);
	clearSearchSection();
	Disable("btnPreview", true);
	
	if (getTabValue() != "" && getTabValue() != null) {
		var strSearch = getTabValue().split(",");
		setField("txtStartDateSearch", strSearch[0]);
		setField("txtStopDateSearch", strSearch[1]);
		setField("txtFlightNoStartSearch", strSearch[7]);
		setField("txtFlightNoSearch", strSearch[2]);
		setField("selFromStn6", strSearch[3]);
		getFieldByID("selFromStn6").selected = strSearch[3];
		setField("selToStn6", strSearch[4]);
		getFieldByID("selToStn6").selected = strSearch[4];
		setField("selOperationTypeSearch", strSearch[5]);
		getFieldByID("selOperationTypeSearch").selected = strSearch[5];
		setField("selStatusSearch", strSearch[6]);
		getFieldByID("selStatusSearch").selected = strSearch[6];

		if (strMsg != null && strMsg != "" && strMsg != "Undefined") {
			showERRMessage(strMsg);
		}
		ClearProgressbar();
	}
}

function isMultipleLogicalCCAvailable(){
	if(top.arrLogicalCC != null){
		var count = 0;
		for(var i=0;i<top.arrLogicalCC.length;i++){
			var logicalCC = top.arrLogicalCC[i][0];
			var lccArr = logicalCC.split('-');
			if(lccArr.length > 1){
				count++;
			}
			
			if(count > 1){
				return true;
			}
		}
	}
	
	return false;
}

function ClearProgressbar() {
	if (objDG.loaded) {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
		top.blnProcessCompleted = false;
	}
}

function SearchData() {

	// once search button was pressed set the hidden value to "SEARCH"
	if (SearchValid() == false)
		return;
	top.blnProcessCompleted = true;
	setSearchCriteria();
	setSearchRecNo(1);
	document.forms[0].hdnMode.value = "SEARCH";
	hdnMode = "SEARCH";
	remoteSubmit();
}

// Validtaions when searching
function SearchValid() {
	// Mandatory fields
	if (getFieldByID("txtStartDateSearch").value == "") {
		showERRMessage(parent.arrError['startdate']);
		return false;
	}
	if (getFieldByID("txtStopDateSearch").value == "") {
		showERRMessage(parent.arrError['stopdate']);
		return false;
	}
	if (!dateChk("txtStartDateSearch")) {
		showERRMessage(parent.arrError['invalidDate']);
		getFieldByID("txtStartDateSearch").focus();
		return false;
	}
	if (!dateChk("txtStopDateSearch")) {
		showERRMessage(parent.arrError['invalidDate']);
		getFieldByID("txtStopDateSearch").focus();
		return false;
	}
	if (getFieldByID("txtFlightNoStartSearch").value == "") {
		showERRMessage(parent.arrError['invalidCarrierCode']);
		getFieldByID("txtFlightNoStartSearch").focus();
		return false;
	}

	// compare start date and stop date
	var schStartD = getFieldByID("txtStartDateSearch").value;
	var schStopD = getFieldByID("txtStopDateSearch").value;

	if (!CheckDates(schStartD, schStopD)) {
		showERRMessage(parent.arrError['stopdategreat']);
		return false;
	}
	if (((trim(getFieldByID("selFromStn6").value) == "") && (trim(getFieldByID("selToStn6").value) == ""))
			&& (trim(getFieldByID("txtFlightNoSearch").value) == "")) {
		showERRMessage(parent.arrError['invalidSearch']);
		return false;
	}

	return true;
}

function timeCheck(id, obj) {
	if (!isTimeFormatValid(obj)) {
		switch (id) {
		case 0:
			msg = parent.arrError['invalidConnectionLimit'];
			break;
		case 1:
			msg = parent.arrError['invalidStartTime'];
			break;
		case 2:
			msg = parent.arrError['invalidEndTime'];
			break;
		}
		showERRMessage(msg);
		getFieldByID(obj).focus();
		return false;
	}
	return true;
}

function isTimeFormatValid(objTime) {
	// Checks if time is in HH:MM format - HH can be more than 24 hours
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = getValueTrimed(objTime).match(timePattern);
	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + getValueTrimed(objTime));
	}
	return true;
}

function getValueTrimed(strValue) {
	return trim(getValue(strValue));
}

// clear the search area
function clearSearchSection() {
	setField("txtFlightNoSearch", "");
	setField("selFromStn6", "");
	setField("selToStn6", "");
}

// hide the message
function objOnFocus() {
	top[2].HidePageMessage();
}

function gridNavigations(intRecNo, intErrMsg) {
	if (intRecNo <= 0) {
		intRecNo = 1;
	}
	setSearchRecNo(intRecNo);
	setSearchCriteria()
	setField("hdnRecNo", intRecNo);
	hdnRecNo = intRecNo;
	hdnMode = "SEARCH";
	if (intErrMsg != "") {
		showERRMessage(intErrMsg);
	} else {
		setPageEdited(false);
		remoteSubmit();
	}
}



function previewFlightAncillaryReport() {
	
	var requestParamString = "";

	requestParamString = generateParamString();
	if(requestParamString == null || requestParamString == "") {
		showERRMessage(parent.arrError['ancillaryCheckedStatus']);
	}
	else if(requestParamString != null && requestParamString != "") {
	
		var intWidth = 800;
		var intHeight = 600;
		var strProp = 'toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,width='
				+ intWidth
				+ ',height='
				+ intHeight
				+ ',resizable=yes,top='
				+ ((window.screen.height - intHeight) / 2)
				+ ',left='
				+ (window.screen.width - intWidth) / 2;

		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmF");
		objForm.target = "CWindow";
		hdnMode = "VIEW";
		objForm.action = "showFlightAncillaryDetailsReport.action?hdnMode="+hdnMode+"&"+requestParamString;
		objForm.submit();
		showAncillaryReportOptionsWindow(0);
			
	}

}

function showAncillaryReportOptionsWindow(show) {
	
	var optionsDiv = getFieldByID("flightAncillaryOptions");
	var windowDiv = getFieldByID("flightResultsWindow");
	
	if(show){			
		fltRoute = trim(fliData[strGridRow][5]);
		
		var airports = fltRoute.split("/");
		var airportData = new Array(airports.length);
		for(var i in airports){
			airportData[i] = new Array(airports[i],airports[i]);
		}
		
		//Reset previously set values of input fields to default
		windowDiv.style.display = "block";
		optionsDiv.style.display = "block";
		
	} else {
		optionsDiv.style.display = "none";
		windowDiv.style.display = "none";
		//Clear data as well
		resetManifestOptions();
		
	}

}

function resetManifestOptions() {

	setField("radViewOption",'Html');
	
	setChecked("chkSeatSelected",true);
	setChecked("chkMealSelected",true);
	setChecked("chkBaggageSelected",true);
	setChecked("chkInsuranceSelected",true);
	setChecked("chkHalaSelected",true);
	setChecked("chkFlexiSelected",true);
	setChecked("chkBusSelected",true);
}

function generateParamString() {
	
	var validated = false;
	
	top[2].HidePageMessage();
	flightNo = fliData[strGridRow][1];
	fltDate = fliData[strGridRow][2];
	fltRoute = fliData[strGridRow][5];
	var fltID = fliData[strGridRow][16];
	
	var viewOpt = getText("radViewOption");
	var seatChecked = getChecked("chkSeatSelected");
	var mealChecked = getChecked("chkMealSelected");
	var baggageChecked = getChecked("chkBaggageSelected");
	var insuranceChecked = getChecked("chkInsuranceSelected");
	var halaChecked = getChecked("chkHalaSelected");
	var flexiChecked = getChecked("chkFlexiSelected");
	var busChecked = getChecked("chkBusSelected");
	
	var requestParamString = "hdnFlightNo="+flightNo+"&hdnFltRoute="+fltRoute+"&hdnFltDate="+fltDate+"&";
	
	
	if (seatChecked || mealChecked || baggageChecked || insuranceChecked || halaChecked || flexiChecked || busChecked) {
		requestParamString += "hdnView="+viewOpt+"&";
		requestParamString += "chkSeatSelected="+seatChecked+"&";
		requestParamString += "chkMealSelected="+mealChecked+"&";
		requestParamString += "chkBaggageSelected="+baggageChecked+"&";
		requestParamString += "chkInsuranceSelected="+insuranceChecked+"&";
		requestParamString += "chkHalaSelected="+halaChecked+"&";
		requestParamString += "chkFlexiSelected="+flexiChecked+"&";
		requestParamString += "chkBusSelected="+busChecked;
		
		return requestParamString;
	} else {
		return "";
	}
	
	

}

function validateConnectionTime(input){

if (isEmpty(getValueTrimed(input))) {
	showERRMessage(parent.arrError['connectionlimitEmpty']);
	getFieldByID(input).focus();
	
	return false;
}

if (!isEmpty(getValueTrimed(input))) {
	if (!timeCheck(0, input)) {
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
		return false;
	}
	if (errMsg != "") {
		showERRMessage(errMsg);
		getFieldByID(input).focus();
		return false;
	}
}
return true;

}


function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function isPageEdited() {
	if (top.pageEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		return cnf;
	} else {
		return true;
	}
}

function gridClick(strRowNo) {
	
	if(strGridRow != null && strGridRow == strRowNo){
		Disable("btnPreview", true);
		strGridRow = null;
	} else {
		if (top.loadCheck(top.pageEdited)) {
			setPageEdited(false);
			objOnFocus();
			strGridRow = strRowNo;
			Disable("btnPreview", false);
			
		}
	}
}

function getTabValue() {
	var tabValue = top[0].manifestSearchValue;
	if ((tabValue == "") || (tabValue == null)) {
		tabValue = new Array("");
	}
	return tabValue;
}

function setSearchCriteria() {

	top[0].manifestSearchValue = getFieldByID("txtStartDateSearch").value + ","
			+ getFieldByID("txtStopDateSearch").value + ","
			+ getFieldByID("txtFlightNoSearch").value + ","
			+ getFieldByID("selFromStn6").value + ","
			+ getFieldByID("selToStn6").value + ","
			+ getFieldByID("selOperationTypeSearch").value + ","
			+ getFieldByID("selStatusSearch").value + ","
			+ getFieldByID("txtFlightNoStartSearch").value;

}

function setSearchRecNo(recNo) {
	top[0].intLastRec = recNo;
}

function resetClick() {
	clearSearchSection();
	setField("selOperationTypeSearch", defaultOperationType);
	setField("selStatusSearch", defaultStatus);
}

function checkTime(first, second) {
	var fstArr = first.split(":");
	var scdArr = second.split(":");
	if (Number(fstArr[0]) < Number(scdArr[0]))
		return true;
	else {
		if (Number(fstArr[1]) < Number(scdArr[1]))
			return true;
		else
			showERRMessage(parent.arrError['endtimeless']);
		return false;
	}
}

/*
 * Ajax submit
 * 
 */
function remoteSubmit() {
	var url = "";
	var params = "";

	var fromDate = getText("txtStartDateSearch");
	var toDate = getText("txtStopDateSearch");
	var flightNo = getText("txtFlightNoSearch");
	var flightNoStart = getText("txtFlightNoStartSearch");
	var operaTyp = getValue("selOperationTypeSearch");
	var stationFrm = getValue("selFromStn6");
	var stationTo = getValue("selToStn6");
	var status = getValue("selStatusSearch");
	
	
	url = "showAncillaryReportFlightSearch.action";
	params = "hdnMode=" + hdnMode + "&txtStartDateSearch=" + fromDate
			+ "&txtStopDateSearch=" + toDate + "&txtFlightNoSearch="
			+ flightNo + "&txtFlightNoStartSearch=" + flightNoStart
			+ "&selOperationTypeSearch=" + operaTyp + "&selFromStn6="
			+ stationFrm + "&selToStn6=" + stationTo + "&selStatusSearch="
			+ status + "&hdnRecNo=" + hdnRecNo;
	
	parent.ShowProgress();
	makePOST(url, params);

}


function processStateChange() {
	if (httpReq.readyState == 4) {
		parent.HideProgress();
		if (httpReq.status == 200) {
			strSYSError = "";
			eval(httpReq.responseText);
			if (strSYSError == "") {
				ClearProgressbar();

				arrayClone(flightData, fliData);
				objDG.arrGridData = flightData;
				objDG.paging = true;
				objDG.seqStartNo = recstNo;
				objDG.pgnumRecTotal = totalRecNo;
				objDG.refresh();
				
			} else {
				showERRMessage(strSYSError);
			}
		}
	}

}