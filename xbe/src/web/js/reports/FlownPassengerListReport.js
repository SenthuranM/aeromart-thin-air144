$(document).ready(function(){
	$('#chkIbePax').click(function(){
		if($('#chkIbePax').is(":checked")){
			$("#spn1_4").click();
			$("#selAgencies").attr("disabled", "disabled");
			$("#btnGetAgent").attr("disabled", "disabled");
			$('#lstRoles').empty();
			$('#lstAssignedRoles').empty();
		}
		else{
			$("#selAgencies").removeAttr("disabled");
			$("#btnGetAgent").removeAttr("disabled");
		}
	});
});

setField("hdnMode","VIEW");	
var screenId = "UC_REPM_088";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "2":
		setField("txtBookedFromDate", strDate);
		break;
	case "3":
		setField("txtBookedToDate", strDate);
		break;	
	}
}

function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFromDate").focus();

	if (top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		setField("txtFlightNumber", strSearch[5]);		
		
		setField("txtBookedFromDate", strSearch[6]);
		setField("txtBookedToDate", strSearch[7]);
		
		if(strSearch[4]!=null && strSearch[4]!=""){
			setAllSegments(strSearch[4]);	
		}
	}
}

function viewClick() {
	setField("hdnMode","VIEW");	
	viewReport();
}

function viewSummaryClick(){
	setField("hdnMode","VIEWSUMMARY");
	viewReport();
}

function viewReport(){
	if(getValue("txtFromDate") == ""){
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();
	}else if(getValue("txtToDate") == ""){
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
	}else if(displayAgencyMode == 1 && getText("selAgencies")=="" && !$('#chkIbePax').is(":checked")){
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	}else if(displayAgencyMode == 1 && ls.getselectedData().length == 0 && !$('#chkIbePax').is(":checked")){
		showERRMessage(arrError["agentsRqrd"]);
		getFieldByID("lstAssignedRoles").focus();
	}else if(displayAgencyMode == 2 && ls.getselectedData().length == 0 && !$('#chkIbePax').is(":checked")){
		showERRMessage(arrError["agentsRqrd"]);
		getFieldByID("lstAssignedRoles").focus();
	}else if(dateValidDate(getText("txtFromDate"))==false){
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
	}else if(dateValidDate(getText("txtToDate"))==false){
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtFromDate").focus();
	} else if ((getText("txtToDate") != "") && !CheckDates(getText("txtFromDate"), getText("txtToDate"))){		
		showERRMessage(arrError["fltDatePerInvld"]);
		getFieldByID("txtToDate").focus();	
	}else if ((getText("txtBookedFromDate") != "") && (dateValidDate(getText("txtBookedFromDate")) == false)) {		
		showERRMessage(arrError["bookedFromDtInvalid"]);
		getFieldByID("txtBookedFromDate").focus();
	} else if ((getText("txtBookedFromDate") != "") && (getText("txtBookedToDate") == "")) {		
		showERRMessage(arrError["bookedToDtInvalid"]);
		getFieldByID("txtBookedFromDate").focus();
	} else if ((getText("txtBookedToDate") != "") && (dateValidDate(getText("txtBookedToDate")) == false)) {		
		showERRMessage(arrError["bookedToDtInvalid"]);
		getFieldByID("txtBookedToDate").focus();
	} else if ((getText("txtBookedToDate") != "") && !CheckDates(getText("txtBookedFromDate"), getText("txtBookedToDate"))){
		showERRMessage(arrError["bookDatePerInvld"]);
		getFieldByID("txtBookedToDate").focus();
	} else if ((getText("txtFlightNumber") != "") &&  (!isFlightNoValid("txtFlightNumber"))) {			
		showERRMessage(arrError["flightNoInvalid"]);				
		getFieldByID("txtFlightNumber").focus();	
	} else {		
		if(displayAgencyMode != 0){
			var agentIds = trim(ls.getselectedData());
			setField("hdnAgents",agentIds);	
		}
		
		var seg = getAllSegments().substr(0, getAllSegments().length - 1);
		setField("hdnSegments", seg);
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
		top[0].objWindow = window.open("about:blank","CWindow",strProp);
		var objForm = document.getElementById("frmFlownPassengerList");
		objForm.target = "CWindow";	
		objForm.action = "showFlownPassengerListReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function getAgentClick(){	
	
	if (getValue("selAgencies") == "") {		
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	} else {
		var selSegStr = getSelectedSegment();
		
		var strSearchCriteria = getValue("txtFromDate") + "#"+ getValue("txtToDate") + "#" + getValue("selAgencies")
								 +"#" + false+"#"+selSegStr+"#"+getValue("txtFlightNumber")+"#"
								 +getValue("txtBookedFromDate")+"#"+getValue("txtBookedToDate") ;
		
		top.strSearchCriteria = strSearchCriteria;
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmFlownPassengerList");
		objForm.target = "_self";
		document.forms[0].submit();
		ShowProgress();
	}		
}

function AgentOnChange(){	
 	ls.removeAllFromListbox();
 	ls.clear();
}

function beforeUnload() {
	if ((top.objCWindow) && (!top.objCWindow.closed)) {
		top.objCWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selDeparture");
	var arr = getValue("selArrival");
	var via1 = getValue("selVia1");
	var via2 = getValue("selVia2");
	var via3 = getValue("selVia3");
	var via4 = getValue("selVia4");

	if (dept == "") {
		showERRMessage(arrError["depatureRequired"]);
		getFieldByID("selDeparture").focus();

	} else if (arr == "") {
		showERRMessage(arrError["arrivalRequired"]);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showERRMessage(arrError["depatureArriavlSame"]);
		getFieldByID("selArrival").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arrError["arriavlViaSame"]);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(arrError["depatureViaSame"]);
		getFieldByID("selDeparture").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia2").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia3").focus();

	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(arrError["viaSame"]);
		getFieldByID("selVia1").focus();

	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(arrError["viaSame"]);
		getFieldByID("selVia2").focus();

	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(arrError["viaSame"]);
		getFieldByID("selVia3").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(arrError["OnDExists"]);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function clearStations() {
	getFieldByID("selDeparture").value = '';
	getFieldByID("selArrival").value = '';
	getFieldByID("selVia1").value = '';
	getFieldByID("selVia2").value = '';
	getFieldByID("selVia3").value = '';
	getFieldByID("selVia4").value = '';
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}

}

function getAllSegments() {
	var control = document.getElementById("selSegment");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		values += control.options[t].text + ",";
	}
	return values;
}

function setAllSegments(segs) {
	var control = document.getElementById("selSegment");
	var selSegs = segs.split(",");

	for ( var i = 0; i < selSegs.length; i++) {
		if (trim(selSegs[i]) != "") {
			control.options[control.length] = new Option(selSegs[i], selSegs[i]);
		}
	}
}

function getSelectedSegment(){
	var control = document.getElementById("selSegment");

	var segmentStr='';
	var intCount = control.length;
	for ( var i = 0; i < intCount; i++) {
		segmentStr += control.options[i].value + ",";		
	}	
	
	return segmentStr;
}

function isFlightNoValid(txtfield) {	
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}