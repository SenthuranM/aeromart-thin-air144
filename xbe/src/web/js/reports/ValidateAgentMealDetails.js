var objWindow;

var screenId = "UC_REPM_052";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID){
	switch (strID){
	case "0" : setField("txtFlightDate", strDate);break ;
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function closeClick() {
	top.LoadHome();
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFlightDate").focus();
	disableDetPax(true);
	
}

function changeAgencies() {
	ls.clear();
}

function viewClick() {
	
	if (getText("txtFlightDate") == "") {
		showERRMessage(arrError["flightDtEmpty"]);
		getFieldByID("txtFlightDate").focus();

	} else if (dateValidDate(getText("txtFlightDate")) == false) {
		showERRMessage(arrError["flightDtInvalid"]);
		getFieldByID("txtFlightDate").focus();

	} else if (getText("txtFlightNo") == "") {
		showERRMessage(arrError["flightNoRqrd"]);
		getFieldByID("txtFlightNo").focus();
	  
		
	}else if (dateValidDate(getText("txtFlightDate")) == true
			&& getText("txtFlightNo") != "") {
			var bln = hasWhiteSpace(getText("txtFlightNo"));
			var blnIsAlpNum = isAlphaNumeric(trim(getText("txtFlightNo")));	
			if(blnIsAlpNum != true){
				showERRMessage(arrError["flightNoInvalid"]);
				getFieldByID("txtFlightNo").focus();
				return false;
			}
	    	if(bln == true){
			showERRMessage(arrError["flightNoInvalid"]);		
		}else{
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
				top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmPage");
			objForm.target = "CWindow";
			objForm.action = "showAgentMealDetailsReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	}

}

function disableDetPax(bln){
	Disable("radReportPaxCat", bln);
}

function hasWhiteSpace(str){
	if(trim(str).search(/ /) == -1){
		return false;
	}else{
		return true;
	}
}

var r={
	'special':/[^0-9a-zA-Z.]+$/  
}

function valid(o,w){
	  o.value = o.value.replace(r[w],'');
}
function objOnFocus() {
	top[2].HidePageMessage();
}