var objWindow;
var value;
var screenId="UC_REPM_060";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtUsegeFrom", strDate);
		break;
	case "1":
		setField("txtUsegeTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function winOnLoad() {
	if(top.arrPrivi["rpt.res.loyal.pdfonly"] == 1){
	//	setVisible("tdHtml", false);
	//	setVisible("tdExcel", false);
	//	setVisible("tdCsv", false);
		radPdfReportOption.checked = true;
	}
}

function closeClick() {
	top.LoadHome();
}

function viewClick() {
	if(validateFields()){
	setField("hdnMode", "VIEW");
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	var objForm = document.getElementById("frmBlockedLoyaltyPointsReport");
	objForm.target = "CWindow";
	objForm.action = "showLMSBlockedCreditReport.action";
	objForm.submit();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}


function validateFields() {

	if (!dateValidDate(getText("txtUsegeFrom"))) {
		showERRMessage(arrError["fromDtInvalid"]);
		return false;
	}
	if (!dateValidDate(getText("txtUsegeTo"))) {
		showERRMessage(arrError["toDtinvalid"]);
		return false;
	}

	var isDateRangeValid = validateDate();

	if (!isDateRangeValid) {
		showERRMessage(arrError["fromDtExceed"]);
		return false;
	}
	
	if(getText("lmsAccountId") != ""){
		if(hasWhiteSpace(getText("lmsAccountId"))){
			showERRMessage(arrError["accCodeInvalid"]);
			return false;
		}
	}

	return true;
}

function disableUsageSummary(bln){
	Disable("txtUsegeFrom", bln);
	Disable("txtUsegeTo", bln);
}

function hasWhiteSpace(str){
	if(trim(str).search(/ /) == -1){
		return false;
	}else{
		return true;
	}
}


function validateDate(){	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
		
	var dateFrom = getText("txtUsegeFrom");
	var dateTo = getText("txtUsegeTo");
	
	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
				
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM;}
	if (dtCD < 10){dtCD = "0" + dtCD;}
	 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
		
	strSysODate=(tempYear+tempMonth+tempDay); 
	if(tempOStartDate <= tempOEndDate){
		validate = true;
	}
	return validate;	
}