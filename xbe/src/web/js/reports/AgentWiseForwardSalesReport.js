var screenId = "UC_REPM_089";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
var objForm = document.getElementById("frmAgentWiseForwardSalesReport");
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFromDate").focus();

	if (top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selStations",strSearch[2]);
		setField("selCountry",strSearch[3]);
		setField("selAgencies",strSearch[4]);
	}
}

function viewClick() {
	if(getValue("txtFromDate") == ""){
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();
	}else if(getValue("txtToDate") == ""){
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
	}else if(getText("selAgencies")=="" || ls.getselectedData().length == 0){
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	}else if(dateValidDate(getText("txtFromDate"))==false){
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
	}else if(dateValidDate(getText("txtToDate"))==false){
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtFromDate").focus();
	}else if (validateReportDate("txtFromDate", "txtToDate")){
	setField("hdnMode","VIEW");	
	var agentIds = trim(ls.getselectedData());
	setField("hdnAgents",agentIds);	

	objForm.target = "CWindow";	
	objForm.action = "showAgentWiseForwardSalesReport.action";
	objForm.submit();
	top[2].HidePageMessage();
	}
}

function validateReportDate(strFromDate, strTodate){
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
	
	var dateFrom = getText(strFromDate);
	var dateTo = getText(strTodate);

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
			
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
	
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);	
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
	
	if(tempOStartDate > tempOEndDate){
		showCommonError("Error","To Date Cannot Be Less Than From Date");
		getFieldByID(strTodate).focus();
	}
	else {
		validate = true;				
	}
	return validate;	
}

function getAgentClick(){	
		var strSearchCriteria = getValue("txtFromDate") + "#" 
									+ getValue("txtToDate") + "#" 
									+ getValue("selStations") + "#" 
									+ getValue("selCountry") + "#" 
									+ getValue("selAgencies");
		strSearchCriteria += "#" + false;
		
		top.strSearchCriteria = strSearchCriteria;
		setField("hdnAgents", "");
		objForm.target = "_self";
		document.forms[0].submit();
		ShowProgress();
}

function AgentOnChange(){	
 	ls.removeAllFromListbox();
 	ls.clear();
}

function beforeUnload() {
	if ((top.objCWindow) && (!top.objCWindow.closed)) {
		top.objCWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}