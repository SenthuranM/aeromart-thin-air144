var objWindow;
var value;
var screenId="UC_REPM_053";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtUsegeFrom", strDate);
		break;
	case "1":
		setField("txtUsegeTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent){
	if(getValue("radCreditSumm") == "CRE_SUM"){
		return false;
	}
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function winOnLoad() {
	if(top.arrPrivi["rpt.res.loyal.pdfonly"] == 1){
		setVisible("tdHtml", false);
		setVisible("tdExcel", false);
		setVisible("tdCsv", false);
		radPdfReportOption.checked = true;
	}
	getFieldByID("txtAccountNo").focus();
	setField("txtExpDays","365");
	disableAccExpDays(true);
	
}

function closeClick() {
	top.LoadHome();
}

function viewClick() {
	if(validateFields()){
	setField("hdnMode", "VIEW");
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	var objForm = document.getElementById("frmLoyaltyPointsReport");
	objForm.target = "CWindow";
	objForm.action = "showLoyaltyPointsReport.action";
	objForm.submit();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}


function validateFields(){
	if(getValue("radCreditSumm") == "USE_SUM"){
		if(getText("txtUsegeFrom") == ""){
			showERRMessage(arrError["fromDtEmpty"]);
			return false;
		}
		if(getText("txtUsegeTo") == ""){
			showERRMessage(arrError["toDtEmpty"]);
			return false;
		}
		if(getText("txtAccountNo") == ""){
			showERRMessage(arrError["accCodeEmpty"]);
			return false;
		}else{
			if(hasWhiteSpace(getText("txtAccountNo"))){
				showERRMessage(arrError["accCodeInvalid"]);
				return false;
			}
		}
		if(!dateValidDate(getText("txtUsegeFrom")) ){
			showERRMessage(arrError["fromDtInvalid"]);
			return false;
		}
		if( !dateValidDate(getText("txtUsegeTo"))){
			showERRMessage(arrError["toDtinvalid"]);
			return false;
		}
	}
	return true;
}

function disableAccExpDays(bln){
	Disable("txtExpDays", bln);
}

function disableCreditSummary(bln){
	Disable("radAccountType", bln);
	Disable("txtExpDays", bln);
}

function disableUsageSummary(bln){
	Disable("txtUsegeFrom", bln);
	Disable("txtUsegeTo", bln);
}

function hasWhiteSpace(str){
	if(trim(str).search(/ /) == -1){
		return false;
	}else{
		return true;
	}
}