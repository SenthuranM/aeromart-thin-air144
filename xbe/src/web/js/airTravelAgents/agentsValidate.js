//Javascript Document
//@author -Shakir  Thushara - moving to xbe 
//@author dilan
var screenId = "SC_ADMN_015";
var agentsCurrency ="";
var currLocCredit = "";
var currLocAdvance = "";
var currLocBSP = "";
var shift = false;
var ra = false; 
var statusStart = '<select id="selStatus" name="selStatus" size="1" onChange="selStatus_onChange()" tabindex="30">';
var statusEnd = '</select>';
var tooltipfeed = '';
var arrAgentPayInfo = agentsPayOpt.split(",");
var isAllowCC = false;
var isAllowOA = false;
var isAllowCA = false;
var isAllowBSP = false;
var isAllowOffline = false;
var isAllowVoucher = false;
var isCalEnabled = false;
var vrSyaDate = "";
var localTime = new Date();
var sysLocDiff;

var terrtitoryStationMultiSelect = new Listbox2('lstTerritoryStations', 'lstAssignedTerritoryStations', 'spnTerritoryStationMultiSelect','terrtitoryStationMultiSelect');
terrtitoryStationMultiSelect.height = '165px'; 
terrtitoryStationMultiSelect.width = '200px';

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

//HMF
var grdArray = new Array();
var selectedHMF= null;

function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtAdvnaceExpiryDate",strDate);break ;
		case "1" : setField("txtInactiveDate",strDate); break;
	}
}

function LoadCalendar(strID, objEvent){
	if(isCalEnabled){
		objCal1.ID = strID;
		objCal1.top = 0 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}
}

/**
 * 
 * @param dateToCheck
 * @returns {Boolean}
 */
function isDatePast(dateToCheck){
	if(dateToCheck > new Date()){
	   return false;
	  }else{
	   return true;
	 }
}

function isServerTimePast(dateToCheck){
	localTime = new Date();
	if(sysLocDiff >=0){
		vrSyaDate.setTime(localTime - sysLocDiff);
	} else if(sysLocDiff >0){
		vrSyaDate.setTime(localTime + sysLocDiff);
	}
	if(dateToCheck > vrSyaDate){
	   return false;
	}else{
	   return true;
	}
}

	for(var a = 0; a < arrAgentPayInfo.length; a++){
		if(arrAgentPayInfo[a] =="CC"){ isAllowCC = true; }
		if(arrAgentPayInfo[a] =="OA"){ isAllowOA = true; }
		if(arrAgentPayInfo[a] =="CA"){ isAllowCA = true; }
		if(arrAgentPayInfo[a] =="BSP"){ isAllowBSP = true; }
		if(arrAgentPayInfo[a] =="VO"){ isAllowVoucher = true; }
		if(arrAgentPayInfo[a] =="Ol"){ isAllowOffline = true; }
	}
var lastPayInCurr  = null;

function agentsOnLoad(strMsg,strMsgType) {
//jquery tabs	
	$( "#tabs" ).tabs({
		activate: function(event, ui) {
			switch (ui.newPanel[0].id)
			{
				case "tabs-1":
					showColbolayers();
					setVisibleItems();
					
					break;
				case "tabs-2":
					hideColbolayers();
					setVisibleItems();
					
					break;
				case "tabs-3":
					hideColbolayers();
					
					break;
				case "tabs-4":
					hideColbolayers();
					
					break;
				case "tabs-5":
					hideColbolayers();
					break;					
				default:
			}
		}
	}); 
	
	setVisible("spnMandGSA", false);
	setVisible("spnMand", false);
	 tooltipfeed = '';
	 if(agentCommList =='INA'){
		setVisible("showAgentComm", false);
	}
	 
	if(reqAgntLevelEtickets == 'false'){
		setVisible("agentLevelETicket", false);
		setVisible("chkEnableAgentLevelEticket", false);
	}
	 
	if(hideGDSOnholdOption == "true"){
		setVisible("showGDSOHD",false);
		setVisible("chkOnHold",false);
	}
	if(userAgentType =='GSA'){
	 
		if(userCurr=='B'){
			DivWrite("LocCurr1", "");
			DivWrite("LocCurr", "");
			DivWrite("LocCurrBSP", "");
			$("#txtLocCredit").hide();
			$("#txtLocAdvnace").hide();
			$("#txtLocBSPGuarantee").hide();
			
		}else{
			DivWrite("LocCurr1", agentAgentCurr);
			DivWrite("LocCurr", agentAgentCurr);
			DivWrite("LocCurrBSP", agentAgentCurr);
			$("#txtLocCredit").show();
			$("#txtLocAdvnace").show();
			$("#txtLocBSPGuarantee").show();
			
		}
		setVisible("showGDSOHD", false);
		setVisible("chkOnHold",false);
		setVisible("showAutoInv", false);
		DivWrite("showTerrt", "Station");
		setVisible("spnMandGSA", true);
	}else{
		DivWrite("showTerrt", "Territory/Station");
		setVisible("spnMand", true);
	}
	
	if(!blnsearchall){
		if(!isAllowCC){
			setVisible("spnCC", false);
			document.getElementById('spnCC').style.display= 'none';
		}
		if(!isAllowCA){
			setVisible("spnCH", false);
			document.getElementById('spnCH').style.display= 'none';
		}
		if(!isAllowOA){
			setVisible("spnOA", false);
			document.getElementById('spnOA').style.display= 'none';
		}
		if(!isAllowBSP){
			setVisible("spnBSP", false);
			document.getElementById('spnBSP').style.display= 'none';
			setVisible("spnBSPText", false);
			document.getElementById('spnBSPText').style.display= 'none';
		}
		
	}
	

	if(!isAllowVoucher){
		setVisible("spnVoucher", false);
		document.getElementById('spnVoucher').style.display= 'none';
	}
	populateAgentComm();
	
	writeDefaultStatus();		
	DivWrite("spnLanguages", strLanguages);
	
	//GSA V2 controls
	if (enableGSAV2){
		loadVisibleTerritories(getValue("selAgentType"), null);
		$("#trFixedCredit").show();
		$("#trCreditLimit").hide();
		$("#trBankGuarantee").hide();
		$("#trBankGuaranteeExpire").hide();
	}else{
		$("#trFixedCredit").hide();
		$("#trCreditLimit").show();
		$("#trBankGuarantee").show();
		$("#trBankGuaranteeExpire").show();
		$("#tabVisibleTerSta").hide();
		$("#tabs-4").hide();
	}
	
	showCreditSummary(false);
	clearControls();
	disableInputControls();	
	objCmb4.disable(true);
	if(arrCurrency.length == 2){
		selectedCurrency = arrCurrency[0][0];
	}	
	objCmbCurrency.setComboValue(selectedCurrency);		
	comboCurrencyChange();
	
	setField("hdnCredit", "");
	setField("hdnMode", "");
	DivWrite("baseCurr", baseCurrency);
	DivWrite("baseCurr1", baseCurrency);
	DivWrite("baseCurr2", baseCurrency);
	
	DivWrite("baseCurrMin", baseCurrency);
	DivWrite("baseCurrMax", baseCurrency);
	DivWrite("baseCurrBSP", baseCurrency);	
	
	setSearchCriteria();
	Disable("btnEdit", true);
	Disable("btnDelete", true);
	Disable("btnCreditLimit", true);
	Disable("btnTktRange", true);
	
	setPageEdited(false);
	if (strMsg != null && strMsgType != null) {
		if (strMsgType == "CONFIRMATION"){
			strMsgType = "CONFIRMATION"; top[2].newMsgType = "Confirm";
		}	
		showCommonError(strMsgType,strMsg);
	}
			
	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved!");				
		}
	}
	
	if(payRefEnabled=="true"){		
		setVisible("spnPerofacPay", true);
		document.getElementById('spnPerofacPay').style.display= '';
	} else {	
		setVisible("spnPerfofacPay", false);
		document.getElementById('spnPerofacPay').style.display = 'none';
	}

	if ((arrFormData != null) && (arrFormData.length > 0)) {		
		enableInputControls();
		setField("hdnModel", arrFormData[0][0]);

		setFields(0, arrFormData);
		if (getText("hdnModel") == "ADD") {
			addClickControls();
		} else {
			editClickControls();
		}
		if (arrFormData[0][20] == "OA") {
			getFieldByID("chkOnAcc").checked = true;
		}
		if (arrFormData[0][21] == "CA") {
			getFieldByID("chkCash").checked = true;
		}
		if (arrFormData[0][22] == "CC") {
			getFieldByID("chkCC").checked = true;
		}
		if (arrFormData[0][63] == "BSP") {
			getFieldByID("chkBSP").checked = true;
		}
		if (arrFormData[0][74] == "OL") {
			getFieldByID("chkOffline").checked = true;
		}
		if (arrFormData[0][78] == "VO") {
			getFieldByID("chkVoucher").checked = true;
		}
		setField("selCurrIn", arrFormData[0][40]);
		setField("selStatus", arrFormData[0][7]);
		setField("selGSA", arrFormData[0][66]);
		setField("txtEmailsToNotify", arrFormData[0][70]);
		setField("txtEmailsToNotify", arrFormData[0][70]);
		
		var inactiveDateTime = arrFormData[0][69];
		if(inactiveDateTime != ""){
			setField("txtInactiveDate",inactiveDateTime.split(" ")[0]);
			setField("txtInactiveTime",inactiveDateTime.split(" ")[1]);	
		}else{
			setField("txtInactiveDate","");
			setField("txtInactiveTime","");
		}
		
		if(isServCh){
			setField("selServiceChannel", arrFormData[0][42]);
		}
		// setAmountSpn(arrFormData[0][40]);
		// currencyChange();
		
		setField("txtLocationURL", arrFormData[0][41]); // Lalanthi-
														// JIRA:3031(Issue 3)
		//loadVisibleTerritories("TA", (arrFormData[0][72]).split(","));
		if (getFieldByID("chkBilling").checked) {
			Disable("txtBillingEmail", true);		
		} else {
			Disable("txtBillingEmail", false);		
		}
	}
	if(!isIncentiveEnabled || isIncentiveEnabled == "false"){  	  	
  	  	setDisplay("btnIncentiveScheme", false);
  	  	setDisplay("divIncentive", false);		
  	}else {
  		setDisplay("btnIncentiveScheme", true);
  		setDisplay("divIncentive", true);
  	}
	if(isRemoteUser == 'true') {
		Disable("btnAdd", true);
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		Disable("btnCreditLimit", true);	
		Disable("btnTktRange", true);	
	}
	// Page control by app parameter -
	// isAllowAirlineCarrierPrefixForAgentsAndUsers
	controlPageDisplayCarrierPrefix();
	
	// hide agent handling fee information
	hideAgentHandlingFeeInfo();
	hideRouteSelectionInfo();
	
}

function loadVisibleTerritories(agentType, assignedData) {

	if (agentType == 'GSA') {
		terrtitoryStationMultiSelect.group1 = arrVisibleTerritories;
		terrtitoryStationMultiSelect.headingLeft = '&nbsp;&nbsp;Territories';
		terrtitoryStationMultiSelect.headingRight = '&nbsp;&nbsp;Assigned Territories';
		$("#tabVisibleTerSta").show();

	} else if (agentType == 'TA') {
		terrtitoryStationMultiSelect.group1 = arrVisibleStations;
		terrtitoryStationMultiSelect.headingLeft = '&nbsp;&nbsp;Stations';
		terrtitoryStationMultiSelect.headingRight = '&nbsp;&nbsp;Assigned Stations';
		$("#tabVisibleTerSta").show();
	} else {
		terrtitoryStationMultiSelect.group1 = new Array();
		$("#tabVisibleTerSta").hide();
	}

	if (typeof (assignedData) == 'undefined' && assignedData == null) {
		terrtitoryStationMultiSelect.group2 = new Array();
	}

	if (document.getElementById('spnTerritoryStationMultiSelect')) {
		terrtitoryStationMultiSelect.drawListBox();
		if (typeof (assignedData) != 'undefined' && assignedData != null) {
			terrtitoryStationMultiSelect.selectedData(assignedData);
		}
	}
}

function controlPageDisplayCarrierPrefix() {	
	if (isAllowAirlineCarrierPrefix == 'false') {
		$("#selAirlineCode").hide();
	} else {
		$("#selAirlineCode").show();
	}	
}
function hideAgentHandlingFeeInfo() {	
	if (isHandlingFeeInfoEnable == 'false' ) {
		$("#tabs-3").hide();
	} 
}

function hideRouteSelectionInfo() {	
	if (isRouteSelectionEnable == 'false' ) {
		$("#tabRouteSelection").hide();
	} else {
		$("#tabRouteSelection").show();
	} 
}

function resetClick() {
	setPageEdited(false);
	lastPayInCurr = null;
	objOnFocus();
	if (intStat==1) {
		setFields(strGridRow, arrData);
		editClick();
		setPageEdited(false);
	} else {
		addClick();
		setPageEdited(false);
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);		
		// top[0].manifestSearchValue = "";
		top[1].objTMenu.tabSetValue("SC_ADMN_015", "");
		top[1].objTMenu.tabRemove("SC_ADMN_015");
		
		// top.LoadHome();
	}
}

function saveClick() {
	objOnFocus();
	var test = getText('txtLocAdvnace');
	var blnGoAhead = false;
	var accCode=getText("txtAccount");
	setField("hdnTerritoryCode", "");	
	setField("hdnStationCode", "");
	setField("hdnOndList", getAllOndList());

	if (getText("hdnMode")=="Edit") {
		 if (getText("selAgentType") == "GSA") {
		 
			setField("hdnTerritoryCode", objCmb6.getText());		
		 }
		setField("hdnStationCode", objCmb5.getText());		
		
	}else{
		if (getText("selAgentType") == "GSA"){
			if (objCmb6.getText() != "" && objCmb6.comboValidate() == true)	{
				setField("hdnTerritoryCode", objCmb6.getText());
			}else {					
				showERRMessage(arrError["TerritoryNotExist"]);	
				$("#tabs").tabs( "option", "active", 0 );
				objCmb6.focus();			
				return false;
			}
		}
		if (objCmb5.getText() != "" && objCmb5.comboValidate() == true)	{
			setField("hdnStationCode", objCmb5.getText());
		}else{
			showERRMessage(arrError["StationNotExist"]);	
			$("#tabs").tabs( "option", "active", 0 );
			objCmb5.focus();
			return false;
		}		
	}
	
	// Check if the station belong to the selected territory
	if (getText("selAgentType") == "GSA") {
		for (var i = 0; i < arrStatTer.length; i++) {
			if ((arrStatTer[i][0] == getValue("hdnStationCode")) 
					&& (arrStatTer[i][1] == getValue("hdnTerritoryCode"))) {
				blnGoAhead = true;
				break;
			} else {
				blnGoAhead = false;
			}			
		}
		if (!blnGoAhead && enableMultipleGSAsForAStation == false) {
			$("#tabs").tabs( "option", "active", 0 );
			showERRMessage(arrError["StationNotBelongToTerritory"]);				
			objCmb5.focus();
			return false;
		}
	}
	
	// validating txtEmailsToNotify value to be a list of comma separated emails
	var listOfEmails = trim(getText("txtEmailsToNotify"));
	listOfEmails = listOfEmails.split(",");
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	for(var i = 0; i<listOfEmails.length; i++){
		  if(listOfEmails != "" && !regex.test(listOfEmails[i])){
			  showERRMessage(arrError["invalidListOfEmails"]);
			  $("#tabs").tabs( "option", "active", 0 );
			  getFieldByID("txtEmailsToNotify").focus();
			  return false;
		  }
	}
	
	
	// If the agent is reporting to a GSA get the territory code of the station
	blnGoAhead = false;
	if (getText("selAgentType") != "GSA") {
		blnGoAhead = false;
		setField("hdnTerritoryCode", "");
		for (var i = 0; i < arrStatTer.length; i++) {
			if (arrStatTer[i][0] == getValue("hdnStationCode")) {
				blnGoAhead = true;
				setField("hdnTerritoryCode", arrStatTer[i][1]);
				break;
			} 		
		}
		if (!blnGoAhead && enableMultipleGSAsForAStation == false) {
			$("#tabs").tabs( "option", "active", 0 );
			showERRMessage(arrError["StationNotBelongToTerritory"]);				
			objCmb5.focus();
			return false;
		}		
	}
		
	if(isEmpty(getText("selAgentType"))) {		
		showERRMessage(arrError["AgentTypeisempty"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("selAgentType").focus();
	
	}else if(isEmpty(getText("txtName"))){
		showERRMessage(arrError["NameisEmpty"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtName").focus();
	} else if(!isEmpty(getText("txtName")) && !isAlphaNumericWhiteSpaceAnper(getText("txtName"))){
		showERRMessage(arrError["invalidAgentName"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtName").focus();
		
	}else if(isEmpty(getText("txtAdd1"))){
		showERRMessage(arrError["AddressisEmpty"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtAdd1").focus();
	
	}else if(!isEmpty(getText("txtAdd1")) && !isAlphaNumericWhiteSpaceAnper(getText("txtAdd1"))){
		showERRMessage(arrError["invalidAddressLine1"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtAdd1").focus();
		
	}else if(!isEmpty(getText("txtAdd2")) && !isAlphaNumericWhiteSpaceAnper(getText("txtAdd2"))){
		showERRMessage(arrError["invalidAddressLine2"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtAdd2").focus();
		
	} else if(isEmpty(getText("txtCity"))){
		showERRMessage(arrError["CityisEmpty"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtCity").focus();
	
	}else if(!isEmpty(getText("txtCity")) && !isAlphaNumericWhiteSpaceAnper(getText("txtCity"))){
		showERRMessage(arrError["invalidCity"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtCity").focus();
		
	}else if(!isEmpty(getText("txtState")) && !isAlphaNumericWhiteSpaceAnper(getText("txtState"))){
		showERRMessage(arrError["invalidState"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtState").focus();	
		
	} else if(!isEmpty(getText("txtPostal")) && !isAlphaNumericWhiteSpaceAnper(getText("txtPostal"))){
		showERRMessage(arrError["invalidPostalCode"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtPostal").focus();	
		
	} else if(!isEmpty(getText("txtFaxNo")) && !isAlphaNumericWhiteSpace(getText("txtFaxNo"))){
		showERRMessage(arrError["invalidFaxNumber"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtFaxNo").focus();	
		
	} else if(!isEmpty(getText("txtMobile")) && !isAlphaNumericWhiteSpace(getText("txtMobile"))){
		showERRMessage(arrError["invalidMobileNumber"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtMobile").focus();	
		
	} else if(!isEmpty(getText("txtContactPerson")) && !isAlphaNumericWhiteSpaceAnper(getText("txtContactPerson"))){
		showERRMessage(arrError["invalidContactPerson"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtContactPerson").focus();	
		
	} else if(!isEmpty(getText("txtLicense")) && !isAlphaNumericWhiteSpaceAnper(getText("txtLicense"))){
		showERRMessage(arrError["invalidLicense"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtLicense").focus();	
		
	} else if(!isEmpty(getText("txtNotes")) && !isValidText(getText("txtNotes"))){
		showERRMessage(arrError["invalidNote"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtNotes").focus();	
		
	} else if(isEmpty(getText("txtTelNo"))){
		showERRMessage(arrError["TelephoneNumberisEmpty"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtTelNo").focus();	
	
	} else if(!isEmpty(getText("txtTelNo")) && !isAlphaNumericWhiteSpace(getText("txtTelNo"))){
		showERRMessage(arrError["invalidTelephoneNumber"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtTelNo").focus();
		
	} else if(enableGSAV2 && getFieldByID("chkGSA").checked != true && actualUserAgentType != airlineAgentType){
		var agentType = getText("selAgentType");	
		if (agentType == "SGSA" || agentType == "TA" || agentType == "STA" ){
			showERRMessage(arrError["selectReportTo"]);
			$("#tabs").tabs( "option", "active", 0 );
			getFieldByID("chkGSA").focus();
		}
	} else if(getText("selAgentType")== "GSA" && isEmpty(getText("hdnTerritoryCode"))){
		showERRMessage(arrError["TerritoryCannotbeblank"]);
		$("#tabs").tabs( "option", "active", 0 );
		objCmb6.focus();
	
	}else if(getText("hdnStationCode") == "") {
		showERRMessage(arrError["StationCannotbeblank"]);
		$("#tabs").tabs( "option", "active", 0 );
		objCmb5.focus();
	
	}else if (getFieldByID("chkBSP").checked && getText("txtIATA") == "" ){
		showERRMessage(arrError["IATANumberEmpty"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtIATA").focus();
		
	} else if(!isEmpty(getText("txtIATA")) && !isAlphaNumericWhiteSpace(getText("txtIATA"))){
		showERRMessage(arrError["invalidIataNumber"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtIATA").focus();
		
	} else if (getFieldByID("chkBSP").checked &&  getText("txtIATA").length != 8){
		showERRMessage(arrError["InvalidIATANumber"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtIATA").focus();		
	
//	}else if (getFieldByID("chkBSP").checked && (objCmbCurrency.getText() != "") && !isStationVsCurrencyMatched(objCmb5.getText())){
//		showERRMessage(arrError["stationCurrencyDoNotMatch"]+" "+ getCountryNameForStation());
//		$("#tabs").tabs( "option", "active", 0 );
//		objCmbCurrency.focus();		
	
	}else if (getFieldByID("chkBSP").checked && !isValidIATAAgentCode(getText("txtIATA"))){
		showERRMessage(arrError["InvalidIATANumber"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtIATA").focus();
		
	}else if(!disableCreditLimit() && (getFieldByID("chkFixedCredit").checked == true && isEmpty(getText("txtCredit"))) && getValue("selCurrIn") == "B") {
		showERRMessage(arrError["CreditLimitisEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtCredit").focus();		
	
	} else if(!disableCreditLimit() && !isEmpty(getText("txtCredit")) && getValue("selCurrIn") == "B" && !isLikeDecimal(getText("txtCredit")) ) {
		showERRMessage(arrError["invalidCrediLimit"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtCredit").focus();		
	
	} else if(!disableCreditLimit() && (getFieldByID("chkFixedCredit").checked == true && isEmpty(getText("txtLocCredit"))) && getValue("selCurrIn") == "L" ) {
		showERRMessage(arrError["CreditLocalLimitisEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtLocCredit").focus();		
	
	}else if(!disableCreditLimit() && !isEmpty(getText("txtLocCredit")) && getValue("selCurrIn") == "L" && !isLikeDecimal(getText("txtLocCredit"))) {
		showERRMessage(arrError["invalidCrediLimit"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtLocCredit").focus();		
	
	}else if(isEmpty(getText("txtEmail"))){
		showERRMessage(arrError["EMailisEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtEmail").focus();				
	
	}else if(!validateEmail()){
		showERRMessage(arrError["InvalidEmail"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtEmail").focus();			
	
	}
	else if(!validateGoogleMapURL()){
		showERRMessage(arrError["invalidMapURL"]);
		$("#tabs").tabs( "option", "active", 0 );
		getFieldByID("txtLocationURL").focus();				
	}
	else if(getFieldByID("chkBilling").checked == false 
			&& isEmpty(getText("txtBillingEmail")) ){				
		showERRMessage(arrError["BillingEmail"]);
		if(!getFieldByID("txtBillingEmail").disabled) {
			getFieldByID("txtBillingEmail").focus();
		}
		
	}else if (!validateBillingEmail()) {
		showERRMessage(arrError["InvalidBillingEmail"]);
		if(!getFieldByID("txtBillingEmail").disabled) {
			getFieldByID("txtBillingEmail").focus();
		}			
	}else if (getFieldByID("chkBilling").checked == false 
			&& getText("txtBillingEmail") ==  getText("txtEmail")) {
		showERRMessage(arrError["SameBillingEmail"]);
		if(!getFieldByID("txtBillingEmail").disabled) {
			getFieldByID("txtBillingEmail").focus();
		}					
	
	}else if(isEmpty(getText("txtAccount")) && trim(varAccountCode) != ""){
		showERRMessage(arrError["AccountCodeisEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtAccount").focus();
	
	}else if (objCmbCurrency.getText() == "") {
		showERRMessage(arrError["currencyCannotbeblank"]);		
		$("#tabs").tabs( "option", "active", 0 );
		objCmbCurrency.focus();
	}else if(!isEmpty(accCode) && ((trim(varAccountCode) != "" && accCode.indexOf(varAccountCode)!=0) || accCode.length<=3) ){
		showERRMessage(arrError["accountCodeInvalid"]);		
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtAccount").focus();		
	
	} else if(getFieldByID("chkFixedCredit").checked == true && isEmpty(getText("txtAdvnace")) && isEmpty(getText("txtAdvnace")) && getValue("selCurrIn") == "B") {
		showERRMessage(arrError["AdvanceisEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtAdvnace").focus();		
	
	}else if(!isEmpty(getText("txtAdvnace")) && !isLikeDecimal(getText("txtAdvnace"))) {
		showERRMessage(arrError["invalidBankGuarantee"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtAdvnace").focus();		
	
	}else if(Number(getValue("txtAdvnace")) > 0 && isEmpty(getText("txtAdvnaceExpiryDate"))){
		showERRMessage(arrError["expiryDateEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtAdvnaceExpiryDate").focus();
		
	}else if(getFieldByID("chkFixedCredit").checked == true && !isEmpty(getText("txtAdvnaceExpiryDate")) && !dateValidDate(getText("txtAdvnaceExpiryDate"))){
		showERRMessage(arrError["expiryDateInvalid"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtAdvnaceExpiryDate").focus();
		
	} else if(getFieldByID("chkFixedCredit").checked == true && !isEmpty(getText("txtAdvnaceExpiryDate")) && isDatePast(stringToDate(getText("txtAdvnaceExpiryDate")))){
		showERRMessage(arrError["expiryDatePast"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtAdvnaceExpiryDate").focus()
		
	} else if(getFieldByID("chkFixedCredit").checked == true && isEmpty(getText("txtLocAdvnace")) && isEmpty(getText("txtLocAdvnace")) && getValue("selCurrIn") == "L") {
		showERRMessage(arrError["LocalAdvanceisEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtLocAdvnace").focus();		
	
	} else if(!isEmpty(getText("txtLocAdvnace")) && !isLikeDecimal(getText("txtLocAdvnace")) ) {
		showERRMessage(arrError["invalidBankGuaranteeLocal"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtLocAdvnace").focus();		
	
	} else if(!isEmpty(getText("txtBSPGuarantee")) && !isLikeDecimal(getText("txtBSPGuarantee")) ) {
		showERRMessage(arrError["invalidBspGuarantee"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtBSPGuarantee").focus();		
	
	} else if(!isEmpty(getText("txtLocBSPGuarantee")) && !isLikeDecimal(getText("txtLocBSPGuarantee")) ) {
		showERRMessage(arrError["invalidBspGuaranteeLocal"]);
		$("#tabs").tabs( "option", "active", 1 );
		getFieldByID("txtLocBSPGuarantee").focus();		
	
	} else if(!isEmpty(getText("txtOWAmount")) && !isLikeDecimal(getText("txtOWAmount")) ) {
		showERRMessage(arrError["invalidOneWayAmount"]);
		$("#tabs").tabs( "option", "active", 2 );
		getFieldByID("txtOWAmount").focus();		
	
	} else if(!isEmpty(getText("txtRTAmount")) && !isLikeDecimal(getText("txtRTAmount")) ) {
		showERRMessage(arrError["AddressisEmpty"]);
		$("#tabs").tabs( "option", "active", 2);
		getFieldByID("txtRTAmount").focus();		
	
	} else if(!isEmpty(getText("txtMinAmount")) && !isLikeDecimal(getText("txtMinAmount")) ) {
		showERRMessage(arrError["invalidMinimumAmount"]);
		$("#tabs").tabs( "option", "active", 2 );
		getFieldByID("txtMinAmount").focus();		
	
	} else if(!isEmpty(getText("txtMaxAmount")) && !isLikeDecimal(getText("txtMaxAmount")) ) {
		showERRMessage(arrError["invalidMaxAmount"]);
		$("#tabs").tabs( "option", "active", 2 );
		getFieldByID("txtMaxAmount").focus();		
	
	} else if(!getFieldByID("chkOnAcc").checked && !getFieldByID("chkCash").checked 
			&& !getFieldByID("chkCC").checked && !getFieldByID("chkBSP").checked && !getFieldByID("chkVoucher").checked) {
		showERRMessage(arrError["PaymentModeisEmpty"]);
		$("#tabs").tabs( "option", "active", 1 );
		if (disableCreditLimit()) {
			getFieldByID("chkCash").focus();
		} else {
			getFieldByID("chkOnAcc").focus();
		}
		
	} else if (enableMultipleGSAsForAStation == true && getFieldByID("chkGSA").checked == true && getValue("selGSA") == "SELECT") {
		showERRMessage(arrError["SelectGSA"]);
		getFieldByID("selGSA").focus();
		
	} else if( !isEmpty(getText("txtInactiveDate")) && !dateValidDate(getText("txtInactiveDate"))){		
		showERRMessage(arrError["invalidInactiveDate"]);
		$("#tabs").tabs( "option", "active", 0);
		getFieldByID("txtInactiveDate").focus();
		
	} else if( !isEmpty(getText("txtInactiveTime")) && !checkTime(getText("txtInactiveTime"))){		
		showERRMessage(arrError["invalidInactiveTime"]);
		$("#tabs").tabs( "option", "active", 0);
		getFieldByID("txtInactiveTime").focus();
		
	} else if( !isEmpty(getText("txtInactiveDate")) && !isEmpty(getText("txtInactiveTime")) && isServerTimePast(getDate( trim(getText("txtInactiveDate")),trim(getText("txtInactiveTime")) )) ){
		showERRMessage(arrError["inactiveDatePast"]);
		getFieldByID("txtInactiveTime").focus();
	} else {
		
		if(enableGSAV2 && getText("selAgentType") === "GSA" && !isOwnTerritorySelected()){
			if(!confirm("Selected Territory of the GSA is not assigned to visble territory. Do you wish to continue?")){
				return false;
			}
		}
		
		if(enableGSAV2 && getFieldByID("chkFixedCredit").checked == false){
			
			if(( carrierAgent === getText("selAgentType") ||getText("selAgentType") === "SO" 
				|| getText("selAgentType") === "CO")){							
				showERRMessage(arrError["invalidCreditBasisForType"]);
				$("#tabs").tabs( "option", "active", 1 );
				getFieldByID("txtCredit").focus();
				return false;
			} else if(getFieldByID("chkGSA").checked == false) {
				showERRMessage(arrError["invalidCreditBasisForType"]);				
				$("#tabs").tabs( "option", "active", 0 );
				getFieldByID("selGSA").focus();
				return false;
			} else if (getValue("selGSA") == "") {
				showERRMessage(arrError["selectReportingCreditSharing"]);				
				$("#tabs").tabs( "option", "active", 0 );
				getFieldByID("selGSA").focus();
				return false;
			}
		}
		
		
		if ((getValue("selCurrIn") == "B" && parseFloat(getText("txtCredit")) > parseFloat(getText("txtAdvnace"))) ||
				(getValue("selCurrIn") == "L" && parseFloat(getText("txtLocCredit")) > parseFloat(getText("txtLocAdvnace")))) {
			if (!confirm("Credit Limit Exceeds Bank Guarantee. Do you wish to continue?")) {
				if(getValue("selCurrIn") == "B") {
					if (!getFieldByID("txtCredit").enable) {
						getFieldByID("txtAdvnace").focus();				
					} else {
						getFieldByID("txtCredit").focus();
					}
				}else {
					if (!getFieldByID("txtLocCredit").enable) {
						getFieldByID("txtLocAdvnace").focus();				
					} else {
						getFieldByID("txtLocCredit").focus();
					}
				}
				return false;
			}	
		}
		
		if(enableGSAV2 && document.getElementById('spnTerritoryStationMultiSelect') && trim(terrtitoryStationMultiSelect.getselectedData()) == ""){
			if(getText("selAgentType") == "GSA" && assignVisibleTerritories) {
				if(!confirm("You are going to save without any visible territories for the GSA. Are you sure?")) {
					$("#tabs").tabs("option","active",3);
					return;
				}
			} else if (getText("selAgentType") == "TA" && assignVisibleStations){
				if(!confirm("You are going to save without any visible stations for the TA. Are you sure?")) {
					$("#tabs").tabs("option","active",3);
					return;
				}
			}	
		}
		

		if(trim(getText("txtNotes")).length > 2000) {
			$("#tabs").tabs( "option", "active", 0 );
			showERRMessage(arrError["notetoolong"]);
			getFieldByID("txtNotes").focus();
			return false;	
		}
		
		if( blnEdit && selectedAgentTypeCode != "" && selectedAgentTypeCode != carrierAgent && getValue("selAgentType") == carrierAgent) {
			showERRMessage(arrError["invalidchange"]);
			$("#tabs").tabs( "option", "active", 0 );
			getFieldByID("selAgentType").focus();
			return false;
		} 
		setField("hdnMode","SAVE");
		setField("hdnPaymentmode",getValue("chkOnAcc")+","+getValue("chkCash")+","+getValue("chkCC")+","+getValue("chkBSP")+","+getValue("chkVoucher")+","+getValue("chkOffline"));
		
		if (!getFieldByID("txtCredit").enable){
			Disable("txtCredit", "");
			Disable("txtLocCredit", "");
			Disable("selAgentType", "");
		}
		if (getFieldByID("chkBilling").checked) {				
			setField("txtBillingEmail", getText("txtEmail"));		
		}
		
		var tempName = trim(getText("txtName"));
		tempName=replaceall(tempName,"," , "_");
		setField("txtName", tempName);
		setField("txtAdd1", trim(getText("txtAdd1")));
		setField("txtEmail", trim(getText("txtEmail")));
		setField("txtAdd2", trim(getText("txtAdd2")));
		setField("txtCity", trim(getText("txtCity")));
		setField("txtIATA", trim(getText("txtIATA")));
		setField("txtState", trim(getText("txtState")));
		setField("txtCredit", trim(removeInvalidChars(getText("txtCredit"))));
		setField("txtPostal", trim(getText("txtPostal")));
		setField("txtAccount", trim(getText("txtAccount")));
		setField("txtTelNo", trim(getText("txtTelNo")));
		setField("txtMobile", trim(getText("txtMobile")));
		setField("txtAdvnace", trim(removeInvalidChars(getText("txtAdvnace"))));
		setField("txtLocAdvnace", trim(removeInvalidChars(getText("txtLocAdvnace"))));
		setField("txtFaxNo", trim(getText("txtFaxNo")));
		setField("txtBillingEmail", trim(getText("txtBillingEmail")));
		setField("maxAdultAllowed" , trim(getText("maxAdultAllowed")));
		setField("txtBSPGuarantee", trim(removeInvalidChars(getText("txtBSPGuarantee"))));
//*
		setField("hdnHandling", trim(removeInvalidChars(getText("hdnHandling"))));
		setField("txtNotes", trim(getText("txtNotes")));
		setField("txtContactPerson", trim(getText("txtContactPerson")));
		setField("txtLicense", trim(getText("txtLicense")));
		
		setField("hdnAgentCommition", trim(getValue("selCommiType")));
		setField("hdnPrefLanguage", trim(getValue("selLanguage")));
		setField("hdnPrefRprFormat", trim(getValue("selPrefRprFormat")));
		setField("hdnSelStatus",getValue("selStatus"));
		
		setField("hdnAgentOrGsaId", $("#spnAgentCode")[0].innerText);
		setField("hdnCountry", $("#spnCountry")[0].innerText);
		
		var tempTime = "";
		if(trim(getText("txtInactiveDate")) != ""){
			if(trim(getText("txtInactiveTime")) != ""){
				tempTime = trim(getText("txtInactiveTime"));
			}else{
				tempTime = "00:00";
			}			
		}
		setField("hdnInactiveDate", trim(getText("txtInactiveDate")) + " " + tempTime);
		if(enableGSAV2) {
			setField("hdnVisibleTerritoryStationValues",trim(terrtitoryStationMultiSelect.getselectedData()));
		}
		tempName = trim(getText("txtNotes"));
		tempName=replaceall(tempName,"'" , "`");
		tempName=replaceEnter(tempName);
		setField("txtNotes", tempName);
		
		//HFM
		$("#hdnHandlingFeeModStr").val(buildHandlingFeeMod());
		$("#hdnHandlingFeeModApp").val(
				trim($("#selModHandlingAppliesto").val()));
		if ($('#chkHFModActiveForAgent').is(":checked")) {
			$("#hdnHandlingFeeModAct").val("Y");
		} else {
			$("#hdnHandlingFeeModAct").val("N");
		}
		
		if ((objCmbCurrency.getText() == "") 
				|| (objCmbCurrency.getText() == selectedCurrency)) {
			setField("selCurrency", selectedCurrency);
			agentsCurrency = "";			
		} else {
			setField("selCurrency", objCmbCurrency.getText());
			agentsCurrency = objCmbCurrency.getText();
		}
		// Disable("selCurrIn", false);
		Disable("selAgentType", false);
		Disable("txtCredit", false);
		Disable("txtLocCredit", false);
		Disable("txtBillingEmail", false);	
		Disable("selStatus", false);
		Disable("chkGSA", false);	
		Disable("selGSA", false);
		Disable("selStatus", false);		
		
		var strTempStr = "";			
		
		strTempStr = getText("txtSearchName");
		strTempStr +=","+getText("txtAgentCode");
		
		var strTempSearch = getText("chkGroup1") + "," + objCmb3.getText() + "," + objCmb4.getText() + "," + strTempStr+","+getValue("selSearchStatus") + "," + getValue("selAirlineCode") + "," +getText("txtAgentIataCode");
		setField("hdnSearchCriteria", strTempSearch);
		setTabSearch();
		setSearchCriteria();
		setPageEdited(false);	
		document.forms[0].target = "_self";
		document.forms[0].action = "showTravelAgent.action";
		ShowProgress();
		document.forms[0].submit();	
		test = getText('txtLocAdvnace');
		Disable("btnSave", true);		
	}
}

function buildHandlingFeeMod(){
	var handlingFeeStr = "";
	for(var j=0;j<grdArray.length;j++){
		handlingFeeStr += grdArray[j]["op"]+",";
		handlingFeeStr += grdArray[j]["ammount"]+",";
		handlingFeeStr += grdArray[j]["type"]+",";
		handlingFeeStr += grdArray[j]["status"]+",";
		handlingFeeStr += grdArray[j]["hmfID"]+",";
		handlingFeeStr += "~";	
   	}
	return handlingFeeStr;
}

function getDate(txtDate,txtTime){
	
	var year = txtDate.substring(6,10);
	var  day = txtDate.substring(0,2);
	var month = txtDate.substring(3,5);
	var hours =	txtTime.substring(0,2);
	var minutes = txtTime.substring(3,5);
	
	var date = new Date(year, Number(month) - 1, day, hours, minutes, 00 , 00); 
	return date;
}

function clearControls(){
	objCmb5.setText("");
	objCmb6.setText("");
	objCmbCurrency.setText("");	
	objCmb5.setSelectedText("");
	objCmb6.setSelectedText("");
	objCmbCurrency.setSelectedText("");	
	setField("txtName","");
	setField("txtAdd1","");
	setField("txtEmail","");
	getFieldByID("chkOnAcc").checked = false;
	getFieldByID("chkCash").checked = false;
	getFieldByID("chkCC").checked = false;
	getFieldByID("chkBSP").checked = false;
	getFieldByID("chkVoucher").checked = false;
	getFieldByID("chkOffline").checked = false;
	setField("txtAdd2","");	
	getFieldByID("chkOnHold").checked = false;
	setField("selStatus","INA");
	setField("txtCity","");
	setField("txtIATA","");
	setField("txtState","");
	setField("txtCredit","");
	setField("txtPostal","");
	setField("txtAccount","");
	setField("txtTelNo","");
	setField("txtMobile","");
	setField("txtAdvnace","");
	setField("txtAdvnaceExpiryDate","");
	setField("txtBSPGuarantee","");
	setField("txtFaxNo","");
	setField("txtContactPerson", "");
	setField("txtLicense", "");	
	setField("txtEmailsToNotify", "");
	getFieldByID("chkGSA").checked = false;
	$("#selectGSA").hide();
	getFieldByID("chkAutoInvoice").checked = false;
	getFieldByID("chkBilling").checked = false;
	setField("txtBillingEmail","");
	setField("txtEmail","");
	setField("maxAdultAllowed" , "");
	// setField("txtHandlingCharge","");
	setField("hdnHandling","");
	setField("hdnTerritoryCode","");
	setField("hdnStationCode","");
	setField("txtNotes","");
	setField("hdnStationCMB", "");		
	setField("hdnTerritoryCMB", "");
	setField("hdnAgentCMB", "");
	setField("hdnCode", "");
	setField("hdnVersion", "");
	setField("selCommiType" , "");
	setField("txtLocAdvnace" , "");
	setField("txtLocCredit" , "");
	setField("txtLocBSPGuarantee" , "");
	setField("selCurrIn" , "");
	// setAmountSpn("");
	DivWrite("LocCurr1", "");
	DivWrite("LocCurr", "");
	DivWrite("LocCurrBSP", "");
	setField("selLanguage", "");
	setField("txtLocationURL", "");	
	setField("selPrefRprFormat", "");
	getFieldByID("chkCapExtPay").checked = false;
	getFieldByID("chkExtPayMand").checked = false;
	getFieldByID("chkPublish").checked = false;
	setField("selContactPersonDes", "");
	agentsCurrency ="";
	
	// reset handling fee values
	setField("selAppliesto","6");
	setField("selType","V");
	setField("txtOWAmount" , "");
	setField("txtRTAmount" , "");
	setField("txtMinAmount" , "");
	setField("txtMaxAmount" , "");	
	getFieldByID("chkHFActive").checked = false;
	getFieldByID("chkEnableAgentLevelEticket").checked = false;
	getFieldByID("chkCharterAgent").checked = false;
	getFieldByID("chkEnableAgentLevelFareMask").checked = false;
	
	// handling fee mod
	
	getFieldByID("chkHFModActiveForAgent").checked = false;
	setField("selModHandlingAppliesto","6");
	setField("selHandlingFeeModType","V");
	setField("amountModHandlingFee" , "");
	setField("selHandlingFeeOp",0);
//	getFieldByID("selHandlingFeeModStatus").checked = false;

	
	if(enableGSAV2){
		$("#trFixedCredit").find("input").attr("disabled", false);
		$("#trCreditLimit").find("input").attr("disabled", false);
		$("#trBankGuarantee").find("input").attr("disabled", false);
		$("#trBankGuaranteeExpire").find("input").attr("disabled", false);
		if(document.getElementById('spnTerritoryStationMultiSelect') && trim(terrtitoryStationMultiSelect.getselectedData()) != "") {
			terrtitoryStationMultiSelect.removeAllFromListbox("lstTerritoryStations","lstAssignedTerritoryStations",2);
		}		
	}
	
	blnEdit = false;
	
		
}

function disableInputControls() {
	Disable("selAgentType", true);
	objCmb5.disable(true);
	objCmb6.disable(true);
	Disable("txtName", true);
	Disable("txtAdd1", true);
	Disable("txtEmail", true);
	DisableOnlyOnAcc("chkOnAcc", true);
	Disable("chkCash", true);
	Disable("chkCC", true);
	Disable("chkBSP", true);
	Disable("chkVoucher", true);
	Disable("txtAdd2", true);
	Disable("selStatus", true);
	Disable("txtCity", true);
	Disable("txtIATA", true);
	Disable("txtState", true);
	Disable("txtCredit", true);
	Disable("txtPostal", true);
	Disable("txtAccount", true);
	Disable("txtTelNo", true);
	Disable("txtMobile", true);
	Disable("txtAdvnace", true);
	Disable("txtAdvnaceExpiryDate", true);
	isCalEnabled=false;
	Disable("txtBSPGuarantee", true);
	Disable("txtFaxNo", true);
	Disable("txtContactPerson", true);
	Disable("txtLicense", true);
	Disable("chkGSA", true);
	Disable("txtEmailsToNotify", true);
	if (enableMultipleGSAsForAStation == true && getFieldByID("chkGSA").checked == true) {
		$("#selectGSA").show();
		Disable("selGSA", true);
	}
	
	if(enableGSAV2){
		$("#trFixedCredit").find("input").attr("disabled", true);
		$("#trCreditLimit").find("input").attr("disabled", true);
		$("#trBankGuarantee").find("input").attr("disabled", true);
		$("#trBankGuaranteeExpire").find("input").attr("disabled", true);
		if(document.getElementById('spnTerritoryStationMultiSelect')) {
			terrtitoryStationMultiSelect.disable(true);
		}		
	}
	Disable("chkAutoInvoice", true);
	Disable("chkCapExtPay", true);
	Disable("chkExtPayMand", true);
	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("txtBillingEmail", true);
	// Disable("txtHandlingCharge", true);
	Disable("chkBilling", true);
	Disable("chkOnHold", true);
	objCmbCurrency.disable(true);
	Disable("txtNotes", true);
	Disable("selCommiType", true);
	Disable("txtLocCredit", true);
	Disable("txtLocAdvnace", true);
	Disable("txtLocBSPGuarantee", true);	
	// Disable("selCurrIn", true);
	Disable("selLanguage", true);	
	Disable("agentPayCurrIn", true);
	Disable("txtLocationURL", true);
	Disable("selPrefRprFormat", true);	
	if(isServCh){
		Disable("selServiceChannel", true);
	}
	Disable("chkPublish", true);	
	Disable("chkIncentive", true);	
	Disable("selContactPersonDes", true);
	Disable("maxAdultAllowed", true);
	
	// disable handling fee controls
	Disable("selAppliesto", true);	
	Disable("selType", true);	
	Disable("txtOWAmount", true);
	Disable("txtRTAmount", true);
	Disable("txtMinAmount", true);
	Disable("txtMaxAmount", true);
	Disable("chkHFActive", true);
	Disable("chkEnableAgentLevelEticket", true);
	Disable("chkCharterAgent", true);
	Disable("chkEnableAgentLevelFareMask", true);
	
	//handling fee modification
	
	Disable("chkHFModActiveForAgent", true);
	Disable("selModHandlingAppliesto", true);	
	Disable("selHandlingFeeModType", true);	
	Disable("amountModHandlingFee", true);
	Disable("selHandlingFeeModStatus", true);
	Disable("selHandlingFeeOp", true);
	
}

function DisableOnlyOnAcc(strControlID, blnEnable) {
	var objControl = getFieldByID(strControlID) ;
	if (objControl != null) {
		// objControl.disabled = blnEnable ;
			objControl.disabled = blnEnable; // chk box is not disabled in
												// any case
									// as per new requirement
		// if(userAgentType=='GSA'){
		// objControl.disabled = true;
		// }
	}
}

function enableInputControls(){
	Disable("selAgentType", false);
	objCmb5.disable(false);
	objCmb6.disable(false);
	objCmbCurrency.disable(false);		
	Disable("txtName", false);
	Disable("txtAdd1", false);
	Disable("txtEmail", false);
	DisableOnlyOnAcc("chkOnAcc", false);	
	Disable("txtAdd2", false);
	Disable("selStatus", false);
	Disable("txtCity", false);
	Disable("txtIATA", false);
	Disable("txtState", false);
	Disable("txtCredit", false);
	Disable("txtPostal", false);
	Disable("txtAccount", false);
	Disable("txtTelNo", false);
	Disable("txtMobile", false);
	Disable("txtAdvnace", false);
	Disable("txtAdvnaceExpiryDate", false);
	isCalEnabled = true;
	Disable("txtBSPGuarantee", false);
	Disable("txtFaxNo", false);
	Disable("txtContactPerson", false);
	Disable("txtLicense", false);
	Disable("chkGSA", false);
	if (enableMultipleGSAsForAStation == true && getFieldByID("chkGSA").checked == true) {
		$("#selectGSA").show();
		Disable("selGSA", false);
	}
	
	if(enableGSAV2){
		$("#trFixedCredit").find("input").attr("disabled", false);
		$("#trCreditLimit").find("input").attr("disabled", false);
		$("#trBankGuarantee").find("input").attr("disabled", false);
		$("#trBankGuaranteeExpire").find("input").attr("disabled", false);
		if(document.getElementById('spnTerritoryStationMultiSelect')) {
			terrtitoryStationMultiSelect.disable(false);
		}		
	}
	Disable("chkAutoInvoice", false);
	Disable("chkCapExtPay", false);
	Disable("chkExtPayMand", false);
	Disable("btnReset", false);
	Disable("txtBillingEmail", false);
	// Disable("txtHandlingCharge", false);
	Disable("chkBilling", false);
	Disable("chkOnHold", false);
	Disable("txtNotes", false);
	Disable("btnSave", false);
	Disable("selCommiType", false);
	
	Disable("txtLocCredit", false);
	Disable("txtLocBSPGuarantee", false);
	// Disable("selCurrIn", false);
	Disable("selPrefRprFormat", false);
	Disable("selLanguage", false);
	Disable("txtLocationURL", false);
	Disable("chkPublish", false);
	Disable("selContactPersonDes", false);
	
	// enable handling fee controls
	Disable("selAppliesto", false);	
	Disable("selType", false);	
	Disable("txtOWAmount", false);
	Disable("txtRTAmount", false);
	Disable("txtMinAmount", false);
	Disable("txtMaxAmount", false);
	Disable("chkHFActive", false);
	Disable("chkEnableAgentLevelEticket", false);
	Disable("chkCharterAgent", false);
	Disable("chkEnableAgentLevelFareMask", false);
	Disable("txtEmailsToNotify", false);
	
	//handling fee Mod
	//Disable("selModHandlingAppliesto", false);	
	//Disable("selHandlingFeeModType", false);
	Disable("amountModHandlingFee", false);
	Disable("selHandlingFeeOp", false);
	Disable("selHandlingFeeModStatus", false);
	Disable("chkHFModActiveForAgent", false);
	
	if(isServCh && isServChEditable){
		Disable("selServiceChannel", false);
	}
	// document.getElementById('selPrefRprFormat').disabled = false;
	
	if(userAgentType!='GSA'){
		Disable("agentPayCurrIn", false);
	}
	
	if(invalidCurAdd){
		Disable("txtLocAdvnace", true);
		Disable("txtLocBSPGuarantee", true);
	}else{
		Disable("txtLocAdvnace", false);
		Disable("txtLocBSPGuarantee", true);
	}
	
	// if(blnsearchall) {
		if(isAllowCA){
			Disable("chkCash", false);
		}
		if(isAllowCC){
		Disable("chkCC", false);
		}
		if(isAllowBSP){
			Disable("chkBSP", false);
		}
		if(isAllowVoucher){
			Disable("chkVoucher", false);
		}
		if(isAllowOffline){
			Disable("chkVoucher", false);
		}
	// }
	Disable("chkIncentive", false);	
	
	if(maintainAgentBspCredLmt == "false"){
		Disable("txtBSPGuarantee", true);
		Disable("txtLocBSPGuarantee", true);
	}
}


function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	return  num + '.' + cents;
}

function getExchangeRate(cur){
	for (var i =0; i< arrExchangeRate.length; i++){
		var t = arrExchangeRate[i];
		if(t[0]==cur){
			return t[1];
		}
	}
	return -1; // no exchange rate found - cur/exchR
}

function copyToBase(){
	setField("txtCredit",getText("txtLocCredit"));
	setField("txtAdvnace",getText("txtLocAdvnace"));
	setField("txtBSPGuarantee",getText("txtLocBSPGuarantee"));
}

function clearLocal(){
	setField("txtLocCredit","");
	setField("txtLocAdvnace","");
	setField("txtBSPGuarantee","");
}

function updateLocCreditLimit(){
	if(objCmbCurrency.getText() != baseCurrency){
		updateLocOnExchangeRate("txtLocCredit","txtCredit",currLocCredit);
	}
}

function updateLocBankGuarantee(){
	if(objCmbCurrency.getText() != baseCurrency){
		updateLocOnExchangeRate("txtLocAdvnace","txtAdvnace",currLocAdvance);
	}
}
function updateLocBSPGuarantee(){
	if(objCmbCurrency.getText() != baseCurrency){
		updateLocOnExchangeRate("txtLocBSPGuarantee","txtBSPGuarantee",currLocBSP);
	}
}

function updateBaseCreditLimit(){
	if(objCmbCurrency.getText() != baseCurrency){
		updateBaseOnExchangeRate("txtLocCredit","txtCredit");
	}
}
function updateBaseBankGuarantee(){
	if(objCmbCurrency.getText() != baseCurrency){
		updateBaseOnExchangeRate("txtLocAdvnace","txtAdvnace");
	}
}
function updateBaseBSPGuarantee(){
	if(objCmbCurrency.getText() != baseCurrency){
		updateBaseOnExchangeRate("txtLocBSPGuarantee","txtBSPGuarantee");
	}
}

function updateBaseOnExchangeRate(locField,baseField){
	var cur  = 	objCmbCurrency.getText();	
	if(cur == ""){
		return ;
	}
	if(isEmpty(getText(locField))){
		return;
	}
	var value = 0;
	
	var exchangeRate = parseFloat(getExchangeRate(cur));
	if(exchangeRate>=0){
		value = parseFloat(getText(locField))*exchangeRate;
	}else{
		return;
	}		

	setField(baseField,formatCurrency(value));
}
function updateLocOnExchangeRate(locField,baseField,def){	
	var cur  = 	objCmbCurrency.getText();	
	if(cur == ""){
		return ;
	}
	if(isEmpty(getText(baseField))){
		return;
	}
	var value = 0;
	if (cur == baseCurrency){
		value == getText(baseField);
	}else {
		var exchangeRate = parseFloat(getExchangeRate(cur));
		if(exchangeRate>0){
			value = parseFloat(getText(baseField))/exchangeRate;
		}else{
			value = def;
		}		
	}

	setField(locField,formatCurrency(value));
}

function validateAlphaNumeric(objTextBox){
	objOnFocus();
	setPageEdited(true);
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isAlphaNumericWhiteSpace(strText);
	if(!blnVal){
		objTextBox.value =  strText.substr(0,strLen-1); 
		objTextBox.focus();
	}
}

function validateAlphaNumericAnper(objTextBox){
	objOnFocus();
	setPageEdited(true);
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isAlphaNumericWhiteSpaceAnper(strText);
	if(!blnVal){
		objTextBox.value =  strText.substr(0,strLen-1); 
		objTextBox.focus();
	}
}

function positiveInt(objTextBox){
	setPageEdited(true);
	objOnFocus();
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isPositiveInt(strText);
	if(!blnVal){
		objTextBox.value = strText.replace(/[^0-9]/g,"");
		objTextBox.focus();
	}
}

function validateDecimal(strField) {
	setPageEdited(true);
	objOnFocus();
	var strCR = getText(strField);
	var strLen = strCR.length;
	var negativeValue = (parseFloat(strCR) < 0);
	if(negativeValue){
		strCR =  strCR.substr(1,strLen);		
	}
	strLen = strCR.length;
	var curlen = 13;
	if(document.getElementById(strField).name == "txtLocCredit"){
		curlen = 16;
	}
	var blnVal = anyCurrencyValidate(strCR, curlen,2);
	var wholeNumber;
	if(!blnVal){
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > curlen) {
				setField(strField, strCR.substr(0, wholeNumber.length - 1)); 	
			} else {
				setField(strField,strCR.substr(0,strLen-1)); 		
			}
		} else {
			setField(strField,strCR.substr(0,strLen-1)); 		
		}	
		getFieldByID(strField).focus();
	}else
	{
		setField(strField,strCR);
	}
}

function validateHandlingChrg(strField) {
	setPageEdited(true);
	objOnFocus();
	var strCR = getText(strField);
	var negativeValue = (parseFloat(strCR) < 0);
	if(negativeValue){
		strCR =  strCR.substr(1,strLen);		
	}
	var strLen = strCR.length;
	var blnVal = currencyValidate(strCR, 10,5);
	var wholeNumber;
	if(!blnVal){
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > 10) {
				setField(strField, strCR.substr(0, wholeNumber.length - 1)); 	
			} else {
				setField(strField,strCR.substr(0,strLen-1)); 		
			}
		} else {
			setField(strField,strCR.substr(0,strLen-1)); 		
		}	

		getFieldByID(strField).focus();
	}
}

function validateCheck() {
	if (getFieldByID("chkBilling").checked == true) {
		Disable("txtBillingEmail", true);
		setField("txtBillingEmail", trim(getText("txtEmail")));
	} else if (getFieldByID("chkBilling").checked == false) {
		Disable("txtBillingEmail", false);
	}
}

function validateCheckFixedCredit(){
	
	if(getValue("selAgentType") == "GSA" && getFieldByID("chkFixedCredit").checked == false){
		getFieldByID("chkFixedCredit").checked = true;
		showERRMessage(arrError["invalidCreditBasis"]);
	}
	
	if (getFieldByID("chkFixedCredit").checked == true) {
		$("#trCreditLimit").show();
		$("#trBankGuarantee").show();
		$("#trBankGuaranteeExpire").show();
		currencyChange();
	} else if (getFieldByID("chkFixedCredit").checked == false) {
		$("#trCreditLimit").hide();
		$("#trBankGuarantee").hide();
		$("#trBankGuaranteeExpire").hide();
	}	
}

/**
 * Enable all payment modes if the user is non-GSA and have 'Add ANY Travel
 * Agent' privilege
 * 
 * @return Lalanthi - JIRA:2553
 */
function enablePaymentModes(){
	
	if(userAgentType !='GSA' && blnsearchall == true){
		if(isAllowCA || actualUserAgentType == airlineAgentType){
			Disable("chkCash", false);
		}
		if(isAllowCC || actualUserAgentType == airlineAgentType){
			Disable("chkCC", false);
		}
		if(isAllowBSP || actualUserAgentType == airlineAgentType){
			Disable("chkBSP", false);
		}
		if(isAllowOA || actualUserAgentType == airlineAgentType){
			Disable("chkOnAcc", false);
		}
		Disable("chkOffline", false);
		Disable("chkVoucher", false);
		setVisible("spnCH", true);
		setVisible("spnCC", true);
		setVisible("spnBSP", true);
		setVisible("spnBSPText", true);
	}
}

/**
 * Validates the input google map url. JIRA -AARESAA:3031 (Lalanthi Date
 * 09/10/2009)
 * 
 * @return
 */
function validateGoogleMapURL(){
	
	if(!isEmpty(getText("txtLocationURL"))){
		var mapUrl = getText("txtLocationURL");
		var location = mapUrl.indexOf("http://maps.google.com/maps");
		if(location<0)
			return false;
		else
			return true;
	}
	else
		return true;
}

function validateEmail(){
	setPageEdited(true);
	objOnFocus();
	var tempVar;
	var mailaddrs = trim(getText("txtEmail"));
	if(mailaddrs.indexOf(",") != -1) {
		var mailarray = mailaddrs.split(",");
		for(var i=0;i < mailarray.length; i++) {
			tempVar = checkEmail(mailarray[i]);
			if(!tempVar) break;
		}
	}else {
		tempVar = checkEmail(trim(getText("txtEmail")));
		
	}
	if(!tempVar){
		setField("txtEmail",""); 
		getFieldByID("txtEmail").focus();
	} else {
		setField("txtEmail", trim(getText("txtEmail")));
		if (tempVar && getFieldByID("chkBilling").checked ) {
			if(!getFieldByID("chkBilling").disabled) {
				setField("txtBillingEmail", trim(getText("txtEmail")));
			}			
		}
	}
	return tempVar;
}

function validateBillingEmail(){
	setPageEdited(true);
	objOnFocus();
	var tempVar;
	var mailaddrs = trim(getText("txtBillingEmail"));
	if(mailaddrs.indexOf(",") != -1) {
		var mailarray = mailaddrs.split(",");
		for(var i=0;i < mailarray.length; i++) {
			tempVar = checkEmail(mailarray[i]);
			if(!tempVar) break;
		}
	}else {
		tempVar = checkEmail(trim(getText("txtBillingEmail")));
		
	}	
	if(!tempVar){
		setField("txtBillingEmail","");
		if(!getFieldByID("txtBillingEmail").disabled) {
			getFieldByID("txtBillingEmail").focus();
		}
		
	}
	return tempVar;
}

function objOnFocus(){
	top[2].HidePageMessage();
}

function disableCreditLimit() {
	var disableCredit = true;
	for (var i = 0 ; i < arrAgentCA.length; i++) { 
		if (arrAgentCA[i][0] == getValue("selAgentType")) {
			if (arrAgentCA[i][1] == "false") { // if credit not applicable
				disableCredit = true;
				break;
			} else {
				disableCredit = false;
			}
		} 
	}
	return disableCredit;
}

function isValidIATAAgentCode(iataCode) {
	var isValid=false;
	var value = Number(iataCode);
	if (Math.floor(value) == value) {
		var agentCode = iataCode.substring(0,7);
		var givenCheckDigit = iataCode.substring(7);
		var agentCodeInt = parseInt(agentCode);
		var givenCheckDigitInt = parseInt(givenCheckDigit);
		var calculatedCheckDigit = agentCodeInt % 7;
		if (calculatedCheckDigit == givenCheckDigitInt) {
			isValid=true;
		}
	}
	
	return isValid;
	
}

function disableReportingToGSA() {
	var disableGSA = false;
	if ((getValue("selAgentType") == "GSA") 
			|| (trim(carrierAgent) != "" && (getValue("selAgentType") == carrierAgent))) {
		disableGSA = true;
		$("#selectGSA").hide();
	} else {
		disableGSA = false;
		$("#selectGSA").show();
	}
	return disableGSA;
}

function setGSADropDown() {
	if (enableMultipleGSAsForAStation == true && getFieldByID("chkGSA").checked == true) {
		if(getValue("selAgentType") == "GSA"){
			$("#selectGSA").hide();
		} else {
			$("#selectGSA").show();
		}		
	} else if (enableGSAV2 && (getValue("selAgentType") != "GSA")  && (getValue("selAgentType") != carrierAgent)){
		$("#selectGSA").show();
	} else {
		$("#selectGSA").hide();
	}
}

function removeInvalidChars(val) {
	var temp = val;
	for (var i = 0; i < temp.length; i++) {
		if ((temp.charAt(i) != ".") && (!isInt(temp.charAt(i)))) {
			temp = temp.replace(temp.charAt(i), "");
		}
	}
	return temp;
}
/*******************************************************************************
 * Moved from validations was in two files merge it to have one file
 ******************************************************************************/


// JavaScript Documet
// Author - Shakir


var strGridRow;
var strTabIDs;
var intStat=0;
var strRowData;
var objWindow;
var selectedAgentTypeCode;
var blnEdit = false;

var objCmb3 = new combo();
var objCmb4 = new combo();
var objCmb5 = new combo();
var objCmb6 = new combo();
var objCmbCurrency = new combo();
invalidCurAdd = false;
var payIn ;
// var objCmbPayCurr = new combo();

buildCombos(); 
populateAgentCurr();
function focusChildWindow() {
	objWindow = top[0].objWindow;
	if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
		objWindow.focus();
	}
}

function closeChildWindow() {
	objWindow = top[0].objWindow;
	if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
		objWindow.close();
	}
}

function startTime() {
	var arrdt = sysDate.split(" ");
	var arryear = arrdt[0].split("/");
	var arrhrs = arrdt[1].split(":");
	vrSyaDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
			Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
			Number(arrhrs[2]));
	localTime = new Date();
	sysLocDiff = localTime.getTime() - vrSyaDate.getTime();
}

function winOnLoad(strMsg, strMsgType) {
	objOnFocus();	
	agentsOnLoad(strMsg,strMsgType);
	focusChildWindow();
	controlRouteSelection(true);
	startTime(); 
	
	jQuery("#handlingFeeList").jqGrid({
        datatype: 'local',
	        colNames:['Id','Operation','Ammount','Type', 'Status', 'op','hmfID'], 
	        colModel :[
	        {name:'Id',index:'mId', width:40, sorttype:'int'},
	        {name:'operation',index:'operation',  width:200},
	        {name:'ammount',index:'ammount', width:150},
	        {name:'type',index:'type', width:120},
	        {name:'status',index:'status',width:70},
	        {name:'op',index:'op',width:50, hidden:true},
	        {name:'hmfID',index:'hmfID', width:50, align:"center", hidden:true,}],
        viewrecords: true,
        loadui:'block',
        height:100,
        onSelectRow: function(rowid){
        selectedHMF =  rowid;
			populateHmfGridData();
        }
       	
	});
}


// To Handle Paging
function gridNavigations(intRecNo, intErrMsg){
	objOnFocus();
	setField("hdnMode","SEARCH");	
	setField("hdnRecNo",intRecNo);
	var strTempStr = "";
	strTempStr = getText("txtSearchName");
	strTempStr += ","+getText("txtAgentCode");
	
	var strTempSearch = getText("chkGroup1") + "," + objCmb3.getText() + "," + objCmb4.getText() + "," + strTempStr+","+getValue("selSearchStatus") + "," + getValue("selAirlineCode") + ","+getText("txtAgentIataCode");
	setField("hdnSearchCriteria", strTempSearch);
	setTabSearch();
	setSearchCriteria();
	if (intErrMsg != ""){
		top[2].objMsg.MessageText = "<li> " + intErrMsg ;
		top[2].objMsg.MessageType = "Error" ; 
		top[2].ShowPageMessage();
	} else {
		if (top.loadCheck(top.pageEdited)) {
			setPageEdited(false);
			document.forms[0].target = "_self";
			document.forms[0].action = "showTravelAgent.action";
			document.forms[0].submit();
			ShowProgress();
		}
	}
}

function searchClick() {
	
	setPageEdited(false);
	var blnGoAhead = true;
	var agentName = getFieldByID("txtSearchName").value;
	var agentCode = getFieldByID("txtAgentCode").value;
	var agentStatus = getFieldByID("selSearchStatus").value;
	var agentIataCode = getFieldByID("txtAgentIataCode").value;
	var airline = getValue("selAirlineCode");
	
	agentName = trim(agentName);
	objOnFocus();
	if (getText("chkGroup1")=="TA")	{
		setField("hdnTerritoryCMB", "");	
		setField("hdnAgentCMB", agentName);	
		setField("hdnSearchCode", agentCode);
		setField("hdnSearchStatus", agentStatus);
		setField("hdnSearchAirline", airline);
		setField("hdnSearchIataCode", agentIataCode);
		
		if (objCmb3.getText() == "") {
			setField("hdnStationCMB", ""); // hdnAirportCMB
		 }else{
		 	if (objCmb3.getText() != "" && objCmb3.comboValidateAlternate() == true && blnGoAhead) {
				setField("hdnStationCMB",objCmb3.getText()); // hdnAirportCMB
			} else {
				blnGoAhead = false;
				showERRMessage(arrError["StationNotExist"]);
				objCmb3.focus();
			}
		}
	} else if (getText("chkGroup1")=="GSA") {
		setField("hdnStationCMB", "");		
		setField("hdnAgentCMB", agentName);
		setField("hdnSearchCode", agentCode);
		setField("hdnSearchStatus", agentStatus);
		setField("hdnSearchAirline", airline);
		setField("hdnSearchIataCode", agentIataCode);
		
		if (objCmb4.getText() == "") {
			setField("hdnTerritoryCMB", "");
		 } else {
			if (objCmb4.getText() != "" && objCmb4.comboValidateAlternate() == true && blnGoAhead)	{
				setField("hdnTerritoryCMB",objCmb4.getText());
			}else{
				blnGoAhead = false;
				showERRMessage(arrError["TerritoryNotExist"]);
				objCmb4.focus();
			}
		 }	
	}

	if (blnGoAhead)	{			
		setField("hdnRecNo","1");
		setField("hdnMode","SEARCH");
		var strTempStr = "";
		strTempStr = agentName+","+agentCode;
		
		var strTempSearch = getText("chkGroup1") + "," + objCmb3.getText() + "," + objCmb4.getText() + "," + strTempStr+","+getValue("selSearchStatus") + "," + getValue("selAirlineCode")+ ","+agentIataCode;
		
		setField("hdnSearchCriteria", strTempSearch);
		setTabSearch();
		document.forms[0].target = "_self";
		document.forms[0].action = "showTravelAgent.action";
		ShowProgress();
		document.forms[0].submit();
			
	}
	showCreditSummary(false);
}

function rowClick(strRowNo) {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		blnEdit = false;
		objOnFocus();
		clearControls();		
		intStat=1;
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow=strRowNo;
		strRowNo1=strRowNo;
		// check if invalid currency //
		if(parseFloat(getExchangeRate(arrData[strGridRow][27]))<0 && !invalidCurAdd){
			var tempArr = arrCurrency;
			tempArr[tempArr.length] = new Array(arrData[strGridRow][27],arrData[strGridRow][27]);
			invalidCurAdd = true;
			// objCmbCurrency.updateCombo(tempArr);
		}else{
			if(invalidCurAdd){
				arrCurrency.splice(arrCurrency.length-1,1);
				// objCmbCurrency.updateCombo(arrCurrency);
				invalidCurAdd = false;
			}
		}
		var identityCode = arrData[strRowNo][1].substring(0,3);
		if (isAllowAirlineCarrierPrefix == 'false') {
			identityCode = defaultAirlineCode;
		}
		if (identityCode != defaultAirlineCode) {
			objCmb6.arrData = arrTerritories;
		} else {
			objCmb6.arrData = arrOwnTerritories;
		}
		setFields(strGridRow, arrData);	
		disableInputControls();// should be after setFields
		if (identityCode != defaultAirlineCode || isRemoteUser == 'true') {
			Disable("btnEdit", true);
			Disable("btnDelete", true);
			Disable("btnCreditLimit", true);		
			Disable("btnTktRange", true);
		} else {
			Disable("btnEdit", false);
			Disable("btnDelete", false);
			Disable("btnCreditLimit", false);
			Disable("btnTktRange", false);
		}
		Disable("txtBillingEmail", true);		
		
		if(arrData[strRowNo][48]=="N"){
			Disable("btnStockLimit", true);		
		}else{
			Disable("btnStockLimit", false);			
		}
		showCreditSummary(false);
	}
}

/*
 * Disabling the show/adjust ticket stock button
 */
function enableStockButton(){
	if(getFieldByID("chkIMT").checked != true){
		Disable("btnStockLimit", true);		
	}else{
		Disable("btnStockLimit", false);			
	}
}

function tktStockClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		if(intStat==1){	
			closeChildWindow();
			
			var intLeft = (1024 - window.screen.width) /2;
			var intTop = (768 - window.screen.height) /2;	
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=510,resizable=no,top=' + intTop + ',left=' + intLeft;			
			var url="showNewFile!loadAgentTicketStock.action";			
			var params = "reqAgentCode="+arrData[strGridRow][1];		
			top.objCWindow=window.open(url+"?"+params,"myWindow",strProp);
		}
	}
}


function setFields(strRowNo, data) {
	
	objCmb5.setText("");	
	objCmb6.setText("");		
	objCmbCurrency.setText("");	
	setField("hdnCode",data[strRowNo][1]);
	DivWrite("spnAgentCode", data[strRowNo][1]);
	setField("txtName", data[strRowNo][2]);
	setField("selAgentType", data[strRowNo][18]);
	selectedAgentTypeCode = data[strRowNo][18];
	getFieldByID("selAgentType").selected =  data[strRowNo][18];
	setField("txtCity", data[strRowNo][23]);
	
	if(enableGSAV2)	 {	
		$("#trFixedCredit").show();
		loadVisibleTerritories(data[strRowNo][18],data[strRowNo][72]);
		if (data[strRowNo][73] == "Y") {
			getFieldByID("chkFixedCredit").checked = true;
			$("#trCreditLimit").show();
			$("#trBankGuarantee").show();
			$("#trBankGuaranteeExpire").show();
							
		} else {
			getFieldByID("chkFixedCredit").checked = false;
			if (data[strRowNo][18] == 'GSA') {
				setField("selGSA",  'SELECT');
				$("#selectGSA").hide();
				getFieldByID("chkFixedCredit").checked = true;
				$("#trFixedCredit").hide();
			}else{
				$("#trFixedCredit").show();	
			}			
			$("#trCreditLimit").hide();
			$("#trBankGuarantee").hide();
			$("#trBankGuaranteeExpire").hide();
		}
	} else {
		$("#trFixedCredit").hide();
	}

	setField("txtAdd1", data[strRowNo][8]);
	setField("txtAdd2", data[strRowNo][9]);
	setField("txtState", data[strRowNo][10]);
	setField("txtPostal", data[strRowNo][11]);
	setField("txtTelNo", data[strRowNo][12]);
	setField("txtFaxNo", data[strRowNo][13]);
	setField("txtEmail", data[strRowNo][14]);
	setField("txtIATA", data[strRowNo][15]);
	setField("txtAccount", data[strRowNo][16]);
	
	if(isServCh){
		setField("selServiceChannel", data[strRowNo][45]);
	}
	
	if (data[strRowNo][42] != null)
	{
	setField("txtContactPerson", data[strRowNo][42]);
	}
	else
	{
		setField("txtContactPerson", "");
	}
	setField("txtLicense", data[strRowNo][34]);	
	if(data[strRowNo][7] == 'Active'){
		setField("selStatus",  'ACT');
	}else if(data[strRowNo][7] == 'New'){
		setField("selStatus",  'NEW');
	} else if(data[strRowNo][7] == 'Black Listed'){
		setField("selStatus",  'BLK');
	} else {
		setField("selStatus",  'INA');
	}


	if (data[strRowNo][6] == "Y") {
		getFieldByID("chkGSA").checked = true;	
		setField("selGSA",  data[strRowNo][66]);
		if (enableMultipleGSAsForAStation == true) {
			$("#selectGSA").show();
		} else {
			DivWrite("spnGSA",  data[strRowNo][26]);	
		}
	} else {
		getFieldByID("chkGSA").checked = false;
		$("#selectGSA").hide();
		DivWrite("spnGSA", "");
	}
	
	if (data[strRowNo][37] == "Y") {
		getFieldByID("chkAutoInvoice").checked = true;		
			
	} else {
		getFieldByID("chkAutoInvoice").checked = false;
		
	}
	
	
	if (data[strRowNo][20] == "OA") {
		getFieldByID("chkOnAcc").checked = true;
	}
	if (data[strRowNo][21] == "CA") {
		getFieldByID("chkCash").checked = true;
	}
	if (data[strRowNo][22] == "CC") {
		getFieldByID("chkCC").checked = true;
	}
	if (data[strRowNo][63] == "BSP") {
		getFieldByID("chkBSP").checked = true;
	}
	if (data[strRowNo][74] == "OL") {//change val
		getFieldByID("chkOffline").checked = true;
	}
	if (data[strRowNo][78] == "VO") {
		getFieldByID("chkVoucher").checked = true;
	}
	setField("hdnVersion",  data[strRowNo][19]);
	
	if (data[strRowNo][18] == "GSA") {	
		objCmb6.setComboValue(data[strRowNo][3]);
	}
	objCmb5.setComboValue(data[strRowNo][29]);

	if (data[strRowNo][4] != "") {
		DivWrite("spnCountry", data[strRowNo][4]);
	} else {
		DivWrite("spnCountry", getCountryNameForStation());
	}
		
	setField("txtBillingEmail", data[strRowNo][24]);
	if (trim(data[strRowNo][14])== trim(data[strRowNo][24])) {
		getFieldByID("chkBilling").checked = true;
	} else {
		getFieldByID("chkBilling").checked = false;	
	}
	
	if (trim(data[strRowNo][35])=="Y"){
		getFieldByID("chkOnHold").checked = true;
	} else{
		getFieldByID("chkOnHold").checked = false;
	}
	
	// setField("txtHandlingCharge", removeInvalidChars(data[strRowNo][25]));
	setField("hdnHandling", removeInvalidChars(data[strRowNo][25]));
	
	objCmbCurrency.setComboValue(data[strRowNo][27]);	
	agentsCurrency = data[strRowNo][27];
	setField("hdnCredit", data[strRowNo][5]);	
	setField("txtNotes", data[strRowNo][28]);	
	setField("hdnCurr", agentsCurrency);
 
	setField("selCommiType", data[strRowNo][33])
	
	if(trim(data[strRowNo][46])=="Y"){
		getFieldByID("chkCapExtPay").checked = true;
	} else {
		getFieldByID("chkCapExtPay").checked = false;
	}
	
	if(trim(data[strRowNo][47])=="Y"){
		getFieldByID("chkExtPayMand").checked = true;
	} else {
		getFieldByID("chkExtPayMand").checked = false;
	}
	
	if (trim(data[strRowNo][49])=="Y"){
		getFieldByID("chkPublish").checked = true;
	} else{
		getFieldByID("chkPublish").checked = false;
	}
	
	if (objCmbCurrency.getText() == "") {
		objCmbCurrency.setComboValue("");		
		objCmbCurrency.selectedText = selectedCurrency;
	}
	setField("txtCredit", removeInvalidChars(data[strRowNo][5]));
	setField("txtAdvnace", removeInvalidChars(data[strRowNo][17]));
	setField("txtBSPGuarantee", removeInvalidChars(data[strRowNo][60]));

	payIn = data[strRowNo][40];
	if(data[strRowNo][40]=='L') {
		
		currLocAdvance = data[strRowNo][38];
		currLocCredit = data[strRowNo][39];
		currLocBSP =data[strRowNo][61];
		setField("txtLocAdvnace", data[strRowNo][38]);
		setField("txtLocCredit", data[strRowNo][39]);
		setField("txtLocBSPGuarantee", data[strRowNo][61]);
		DivWrite("LocCurr1", data[strRowNo][27]);
		DivWrite("LocCurr", data[strRowNo][27]);
		DivWrite("LocCurrBSP", data[strRowNo][27]);
		$("#txtLocCredit").show();
		$("#txtLocAdvnace").show();
		$("#txtLocBSPGuarantee").show();
		setField("agentPayCurrIn",agentsCurrency);
		
		// fillAgentCurr(data[strRowNo][40], data[strRowNo][27]);
	} else {
		currLocAdvance = "0";
		currLocCredit = "0";
		currLocBSP ="0";
		setField("txtLocAdvnace", "0");
		setField("txtLocCredit", "0");
		setField("txtLocBSPGuarantee","0");
		DivWrite("LocCurr1", "");
		DivWrite("LocCurr", "");
		DivWrite("LocCurrBSP", "");
		$("#txtLocCredit").hide();
		$("#txtLocAdvnace").hide();
		$("#txtLocBSPGuarantee").hide();
		setField("agentPayCurrIn",baseCurrency);
		// fillAgentCurr(data[strRowNo][40], data[strRowNo][27]);
	}
	populateAgentCurr(true);	
	// comboCurrencyChange();
	tooltipfeed = data[strRowNo][40];
 
	setField("selCurrIn", data[strRowNo][40]);
	setField("selLanguage",data[strRowNo][41]);
	setField("txtLocationURL", data[strRowNo][43]);
	setField("selPrefRprFormat", data[strRowNo][44]);
	setPageEdited(false);
	setField("txtMobile", data[strRowNo][50]);
	Disable("selContactPersonDes", false);	
	setField("selContactPersonDes", data[strRowNo][51]);
	Disable("selContactPersonDes", true);
	
	// set agent handling fee details
	setField("selAppliesto", data[strRowNo][52]);
	setField("selType", data[strRowNo][53]);
	setField("txtOWAmount", data[strRowNo][54]);
	setField("txtRTAmount", data[strRowNo][55]);
	setField("txtMinAmount", data[strRowNo][56]);
	setField("txtMaxAmount", data[strRowNo][57]);	
	getFieldByID("chkHFActive").checked  = (trim(data[strRowNo][58])=="Y");
	
	// set agent handling fee modification details
	
	getFieldByID("chkHFModActiveForAgent").checked  = (trim(data[strRowNo][76])=="Y");
	
	// agent ticketing info
	getFieldByID("chkEnableAgentLevelEticket").checked  = (trim(data[strRowNo][62])=="Y");	
	getFieldByID("chkCharterAgent").checked  = (trim(data[strRowNo][67])=="Y");
	
	setField("txtAdvnaceExpiryDate", data[strRowNo][65]);
	
	getFieldByID("chkEnableAgentLevelFareMask").checked  = (trim(data[strRowNo][68])=="Y");	
	
	var inactiveDateTime = data[strRowNo][69];
	if(inactiveDateTime != ""){
		setField("txtInactiveDate",inactiveDateTime.split(" ")[0]);
		setField("txtInactiveTime",inactiveDateTime.split(" ")[1]);	
	}else{
		setField("txtInactiveDate","");
		setField("txtInactiveTime","");
	}
	setField("txtEmailsToNotify", data[strRowNo][70]);
	setField("maxAdultAllowed", data[strRowNo][74]);
	
	prepareDataForGrid(data[strRowNo][75]);
}


function prepareDataForGrid(arrHandlingFee) {

	grdArray = new Array();
	for (var i = 0; i < arrHandlingFee.length; i++) {
		grdArray[i] = new Array();
		grdArray[i]["Id"] = i + 1;
		grdArray[i]["status"] = arrHandlingFee[i][3];
		grdArray[i]["type"] = arrHandlingFee[i][4];
		grdArray[i]["operation"] =$('#selHandlingFeeOp option[value$="' + arrHandlingFee[i][2] + '"]').text();
		grdArray[i]["ammount"] = arrHandlingFee[i][1];
		grdArray[i]["op"] = $('#selHandlingFeeOp option[value$="' + arrHandlingFee[i][2] + '"]').val();
		grdArray[i]["hmfID"] = arrHandlingFee[i][0];
	}

	populateMealGrid(grdArray);
}


function populateMealGrid(grdArray) {
	jQuery("#handlingFeeList").clearGridData();
	for (var i = 0; i < grdArray.length; i++) {
		var arrObj = $.extend({}, grdArray[i]);
		jQuery("#handlingFeeList").addRowData(i + 1, arrObj);
	}
}

function populateHmfGridData(){

	$("#selHandlingFeeOp").val(grdArray[selectedHMF-1]["op"]);
	$("#amountModHandlingFee").val(grdArray[selectedHMF-1]["ammount"]);
	$("#selHandlingFeeModType").val(grdArray[selectedHMF-1]["type"]);
	$("#selHandlingFeeModStatus").val(grdArray[selectedHMF-1]["status"]);

	$("#selModHandlingAppliesto").prop('disabled', true);
	$('#selHandlingFeeModType').prop("disabled", true); 
}


function setSearchCriteria() {
	var strSearchTemp =top[1].objTMenu.tabGetValue("SC_ADMN_015");// top[0].manifestSearchValue;//
																	// getTabValue();
																	// //
	if ((strSearchTemp != "") && (strSearchTemp != null)) {
		var strSearch = strSearchTemp.split(",");
		setField("chkGroup1", strSearch[1]);
		setField("hdnRecNo", strSearch[0]);		
			
		chkClick();
		 
		if (strSearch[1] == "TA") {
			
			objCmb3.disable(false);	
			objCmb4.disable(true);	
			setField("txtSearchName", strSearch[4]);
						 
			objCmb3.setComboValue(strSearch[2]);
			setField("hdnStationCMB", objCmb3.getText());					
			setField("hdnAgentCMB", strSearch[4]);	
			setField("txtAgentCode", strSearch[5]);
			setField("hdnSearchCode", strSearch[5]);
			setField("hdnSearchStatus", strSearch[6]);
			setField("selSearchStatus", strSearch[6]);
			setField("selAirlineCode", strSearch[7]);
			setField("hdnSearchAirline", strSearch[7]);
			setField("hdnSearchIataCode",strSearch[8]);
			setField("txtAgentIataCode",strSearch[8]);
			
		} else {
			 
			objCmb3.disable(true);
			objCmb4.disable(false);						
				
			setField("txtSearchName", strSearch[4]);
			objCmb4.setComboValue(strSearch[3]);
			setField("hdnTerritoryCMB",objCmb4.getText());
			setField("hdnAgentCMB", strSearch[4]);
			setField("txtAgentCode", strSearch[5]);
			setField("hdnSearchCode", strSearch[5]);
			setField("hdnSearchStatus", strSearch[6]);
			setField("selSearchStatus", strSearch[6]);
			setField("selAirlineCode", strSearch[7]);
			setField("hdnSearchAirline", strSearch[7]);
			setField("hdnSearchIataCode",strSearch[8]);
			setField("txtAgentIataCode",strSearch[8]);
		}
	} 
}

function editClick() {
	if (top.loadCheck(top.pageEdited)) {
		agentFormMode = "EDIT";
		setPageEdited(false);		
		objOnFocus();
		if (intStat==1) {
			$("#tabs").tabs( "option", "active", 0 );
			enableInputControls();
			editClickControls();
			currencyChange();	
			if(agentsCurrency != '' && parseFloat(getExchangeRate(agentsCurrency))<0){
				objCmbCurrency.setComboValue(agentsCurrency);
			}
			enablePaymentModes(); /** Lalanthi - JIRA:2553 */
			Disable('selStatus',true);
			//Enable agent status dropdown only if the user has the required privilege
			if(hasStatusChangePriv)
			{
				Disable('selStatus',false);
			}
		} else {
			showERRMessage(arrError["invalidchange"]);
		}
		showCreditSummary(false);
	}
}

function addHFMClick() {
	
	var operationAlredayAdded = false;
	var msize = grdArray.length;
	var selectedOperation = trim($("#selHandlingFeeOp option:selected").val());
	var amountAded = trim($("#amountModHandlingFee").val());
	
	for(var grdIndx = 0 ; grdIndx < msize ; ++grdIndx){
		if(grdArray[grdIndx]["op"] == selectedOperation ){
			operationAlredayAdded = true;
		}
	}
	
	if(operationAlredayAdded){
		showERRMessage(arrError["operationAdded"]);
	} else if(!($.isNumeric(amountAded))){
		showERRMessage(arrError["invalidOpAmount"]);
	}else if(selectedOperation == 0){
		showERRMessage(arrError["operationNotAdded"]);
	}else{
		grdArray[msize] = new Array();				
		grdArray[msize]["Id"] = msize+1;
		grdArray[msize]["status"] = trim($("#selHandlingFeeModStatus").val());
		grdArray[msize]["type"] =  trim($("#selHandlingFeeModType").val());;
		grdArray[msize]["operation"] = trim($("#selHandlingFeeOp option:selected").text());
		grdArray[msize]["ammount"] = trim($("#amountModHandlingFee").val());
		grdArray[msize]["op"] = trim($("#selHandlingFeeOp option:selected").val());
		grdArray[msize]["hmfID"] = 0;

		var arrObj = $.extend({}, grdArray[msize]);
		jQuery("#handlingFeeList").addRowData(msize+1, arrObj);
	}
	
}

function editHFMClick() {
	
	var hmfStatus = trim($("#selHandlingFeeModStatus").val());
	var hmfAmount = trim($("#amountModHandlingFee").val());
	
	grdArray[selectedHMF-1]["status"] = trim($("#selHandlingFeeModStatus").val());
	grdArray[selectedHMF-1]["ammount"] = trim($("#amountModHandlingFee").val());
	
	jQuery("#handlingFeeList").setRowData( selectedHMF, { status:hmfStatus, ammount: hmfAmount})	
}


function addClick() {
	if (top.loadCheck(top.pageEdited)) {
		agentFormMode = "ADD";
		setPageEdited(false);
		objOnFocus();
		intStat=0;
		writeStatusSpan();
		clearControls();
		tooltipfeed ='';
		enableInputControls();
		DivWrite("spnAgentCode","");
		DivWrite("spnCountry", "");
		DivWrite("spnGSA", "");		
		getFieldByID("chkOnAcc").checked = true;		
		setField("selStatus", "ACT");
		getFieldByID("chkBilling").checked = true;
		// setField("txtHandlingCharge", handlingCharge);
		setField("hdnHandling", handlingCharge);
		objCmb5.setText("");
		objCmb6.setText("");
		// setField("txtAccount","AR-");
		objCmbCurrency.setComboValue(selectedCurrency);		
		objCmbCurrency.selectedText = selectedCurrency;
		
		addClickControls();	
		populateAgentCurr(true);
		comboToggle("selCurrencyCmb");
		currencyChange();
		getFieldByID("selAgentType").focus();
		objCmb6.arrData = arrOwnTerritories;
		enablePaymentModes();/** Lalanthi - JIRA:2553 */
		Disable("chkExtPayMand", true)
		//Always enable the agent select status for adding
		Disable('selStatus',false);
		showCreditSummary(false);
	}
}

function payRefValidation(){
	if(getFieldByID("chkCapExtPay").checked == true){Disable("chkExtPayMand", false);}else{getFieldByID("chkExtPayMand").checked = false; Disable("chkExtPayMand", true)}
}

function creditLimitClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		if(intStat==1){			
			setField("hdnMode","showCredit");
	
			closeChildWindow();
			
			var intLeft = (1024 - window.screen.width) /2;
			var intTop = (768 - window.screen.height) /2;
	
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=610,resizable=no,top=' + intTop + ',left=' + intLeft
			objWindow = window.open("about:blank","CWindow",strProp);
			top[0].objWindow = objWindow;
			
			var objForm  = document.getElementById("frmTravelAgent");
			objForm.target = "CWindow";
	
			objForm.action = "showAdjustCreditLimit.action";
			objForm.submit();	
		}
	}
}

function incentiveSchemeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		if(intStat==1){	
			closeChildWindow();			
			var intLeft = (1024 - window.screen.width) /2;
			var intTop = (768 - window.screen.height) /2;	
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=500,resizable=no,top=' + intTop + ',left=' + intLeft;			
			var url="showAgentIncentive.action";			
			var params = "hdnAgentCode="+arrData[strGridRow][1];		
			top.objCWindow=window.open(url+"?"+params,"myWindow",strProp);
		}
	}
}

function deleteClick() {
	if (top.loadCheck(top.pageEdited)) {
		setSearchCriteria();
		setPageEdited(false);
		if (intStat == 1) {
			var strConf = confirm(arrError["DeleteConfirm"]);
			if (strConf == true) {
				objOnFocus();
				var strTempStr = "";				

				strTempStr = getText("txtSearchName");
				strTempStr += ","+ getText("txtAgentCode");

				var strTempSearch = getText("chkGroup1") + "," + objCmb3.getText() + "," + objCmb4.getText() + "," + strTempStr+","+getValue("selSearchStatus") + "," + getValue("selAirlineCode")+ ","+getText("txtAgentIataCode");
				setField("hdnSearchCriteria", strTempSearch);
				setTabSearch();
				setField("hdnMode","DELETE");
				setField("hdnRecNo","1");
				document.forms[0].target = "_self";
				document.forms[0].action = "showTravelAgent.action";
				document.forms[0].submit();
			} else {
				disableInputControls();
			}
		} else {
			showERRMessage(arrError["SelectAgentToDelete"]);
		}
	}
}

function chkClick() {
	objOnFocus();
	if (getText("chkGroup1") == "TA"){			
		objCmb3.disable(false);
		if(blnsearchall) {
			objCmb4.setText("");
			objCmb4.disable(true);
		}
		objCmb3.focus();
	} else {		
		objCmb3.disable(true);
		objCmb3.setText("");
		objCmb4.disable(false);
		objCmb4.focus();
	}		
}


function objOnFocus(){
	top[2].HidePageMessage();
}

function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}

function buildCombos(){

	objCmb3.id = "selAirportCmb";
	objCmb3.top = "36";	
	objCmb3.left = "250";
	objCmb3.width = "110";
	objCmb3.tabIndex = 2;
	objCmb3.arrData = arrAirports;
	objCmb3.onChange = "combo3Change";
	
	objCmb4.id = "selTerritoryCmb";
	objCmb4.top = "58";	
	objCmb4.left = "250";
	objCmb4.width = "110";
	objCmb4.tabIndex = 2;
	objCmb4.arrData = arrTerritories;
	objCmb4.onChange = "combo4Change";
	

	objCmb6.id = "selTerritoryCmb1";
	objCmb6.top = "378";
	objCmb6.left = "399";
	objCmb6.width = "90";
	objCmb6.tabIndex = 21;
	objCmb6.arrData = arrTerritories;	
	objCmb6.onChange = "combo6Change";
	
	objCmb5.id = "selAirportCmb1";
	objCmb5.top = "378";
	if(userAgentType =='GSA'){
		objCmb5.left = "505";
	}else{
		objCmb5.left = "505";
	}
	objCmb5.width = "90";
	objCmb5.tabIndex = 21;
	objCmb5.arrData = arrAirports;
	objCmb5.onChange = "combo5Change";	

	objCmbCurrency.id = "selCurrencyCmb";
	if(userAgentType =='GSA'){
		objCmbCurrency.top = "412";
	}else{
		objCmbCurrency.top = "394";
	}
	objCmbCurrency.left = "727";
	objCmbCurrency.width = "80";	
	objCmbCurrency.tabIndex = 28;
	objCmbCurrency.arrData = arrCurrency;	
	objCmbCurrency.onChange = "comboCurrencyChange";
 
	// objCmbPayCurr.id = "selPayCurrencyCmb";
	// objCmbPayCurr.top = "570";
	// objCmbPayCurr.left = "820";
	// objCmbPayCurr.width = "60";
	// objCmbPayCurr.tabIndex = 38;
	// objCmbPayCurr.arrData = arrPayCurrency;
	// objCmbPayCurr.onChange = "currencyChange";
	
	objCmbCurrency.buildCombo();
	
	
	if(userAgentType =='GSA'){
		objCmb6.visible(false);
	}

	 
	
	
	if(blnsearchall) {
		objCmb4.visible(true);
	}else {
		objCmb4.visible(false);
	}

}

function combo3Change() {
	objCmb3.focus();
}

function combo4Change() {
	objCmb4.focus();
}

function combo5Change() {
	setPageEdited(true);
	objCmb5.focus();
	DivWrite("spnCountry", getCountryNameForStation());
}

function combo6Change() {	
	setPageEdited(true);
	objCmb6.focus();
}

function comboCurrencyChange() {
	setPageEdited(true);
	objCmbCurrency.focus();
	if(objCmbCurrency.getText().length > 2){
		populateAgentCurr(false);
		if((objCmbCurrency.getText() != '')&& (objCmbCurrency.getText() != baseCurrency) && (getValue("agentPayCurrIn")!=baseCurrency)){
			$("#txtLocCredit").show();
			$("#txtLocAdvnace").show();
			$("#txtLocBSPGuarantee").show();
			DivWrite("LocCurr1", objCmbCurrency.getText());
			DivWrite("LocCurr", objCmbCurrency.getText());
			DivWrite("LocCurrBSP", objCmbCurrency.getText());
			
		}else{
			$("#txtLocCredit").hide();
			$("#txtLocAdvnace").hide();
			$("#txtLocBSPGuarantee").hide();
			DivWrite("LocCurr1", "");
			DivWrite("LocCurr", "");
			DivWrite("LocCurrBSP", "");
		}		
		currencyChange();
		onAgentCurChange();
	}		 
}

function getCountryNameForStation() {
	var countryName = "";
	for (i = 0; i < arrStationCountry.length; i++) {
		if (arrStationCountry[i][0] == objCmb5.getText()) {
			countryName = arrStationCountry[i][1];
			break;
		}
	}
	return countryName;
}

function controlRouteSelection(status){
	Disable("selDepature", status);
	Disable("selArrival", status);
	Disable("selVia1", status);
	Disable("selVia2", status);
	Disable("selVia3", status);
	Disable("selVia4", status);
	Disable("selSelected", status);
	Disable("btnAddSeg", status);
	Disable("btnRemSeg", status);
}

function addClickControls() {
	getFieldByID("selAgentType").focus();
	setField("hdnMode","Add");
	setField("hdnModel","ADD");
	Disable("btnEdit", true);
	Disable("btnDelete", true);	
	Disable("btnCreditLimit", true);
	Disable("btnTktRange", true);
// Disable("btnAdjustPay", true);
	Disable("txtBillingEmail", true);
	var blnOneMethod = (arrAgentPayInfo.length == 2);
	if (getValue("selAgentType") == "GSA") {
		objCmb5.disable(false);			
		objCmb6.disable(false);
		getFieldByID("chkOnAcc").checked = false;
		getFieldByID("chkOnHold").checked = true;
		Disable("chkOnHold",true);
	} else {
		objCmb6.disable(true);			
		objCmb5.disable(false);			
		getFieldByID("chkOnAcc").checked = false;
		getFieldByID("chkOnHold").checked = true;
		Disable("chkOnHold",true);
	}
	DisableOnlyOnAcc("chkOnAcc", false);
	
	Disable("txtCredit", disableCreditLimit());
	Disable("txtLocCredit", disableCreditLimit());
	Disable("chkGSA", disableReportingToGSA());
	Disable("selGSA", disableReportingToGSA());
	Disable("btnSave", false);
	if (enableGSAV2) {
		$("#trFixedCredit").show();
	} else {
		$("#trFixedCredit").hide();
		$("#tabVisibleTerSta").hide();
		$("#tabs-4").hide();
	}
	if(!blnsearchall) {
		if(blnsearchall)
		if(!isAllowCA){
			Disable("chkCash", true);
			document.getElementById('spnCH').style.display= 'none';
			setVisible("spnCH", false);
		}
		if(!isAllowCC){
			Disable("chkCC", true);
			setVisible("spnCC", false);
			document.getElementById('spnCC').style.display= 'none';
		}
		
		if(!isAllowBSP){
			Disable("chkBSP", true);
			setVisible("spnBSP", false);
			setVisible("spnBSPText", false);
			document.getElementById('spnBSP').style.display= 'none';
			document.getElementById('spnBSPText').style.display= 'none';
		}
		
		if(!isAllowVoucher){
			Disable("chkVoucher", true);
			setVisible("spnVoucher", false);
			document.getElementById('spnVoucher').style.display= 'none';
		}
		
		if(isAllowOA){
			getFieldByID("chkOnAcc").checked = false;
		}else{
			Disable("chkOnAcc", true);
			document.getElementById('spnOA').style.display= 'none';
			getFieldByID("chkOnAcc").checked = false;
		}
		
		if(!isAllowOffline){
			Disable("chkOffline", true);
			setVisible("spnOffline", false);
			document.getElementById('spnOffline').style.display= 'none';
		}
	 
		
		getFieldByID("chkGSA").checked = true;
		if (enableMultipleGSAsForAStation == true || (enableGSAV2 && (getValue("selAgentType") != "GSA")  && (getValue("selAgentType") != carrierAgent))) {
			$("#selectGSA").show();
			Disable("selGSA", false);
		}
		// getFieldByID("chkOnAcc").checked = true;
		if (!enableGSAV2) {
			Disable("chkGSA", true);
		}		
		
	}
	
	if(!isAllowVoucher){
		Disable("chkVoucher", true);
		setVisible("spnVoucher", false);
		document.getElementById('spnVoucher').style.display= 'none';
	}
	
	if(blnOneMethod){
		if(isAllowCA)
			getFieldByID("chkCash").checked = true;
		if(isAllowCC)
			getFieldByID("chkCC").checked = true;
		if(isAllowOA)
			getFieldByID("chkOnAcc").checked = true;
		if(isAllowBSP)
			getFieldByID("chkBSP").checked = true;
		if(isAllowOffline)
			getFieldByID("chkOffline").checked = true;
		if(isAllowVoucher)
			getFieldByID("chkVoucher").checked = true;
		
		
	}
	// setField("selCurrIn","B");
	controlRouteSelection(false);
}

function editClickControls() {
	getFieldByID("txtName").focus();
	Disable("selAgentType", true);
	if(isServCh && isServChEditable){
		Disable("selServiceChannel", false);
	}
	blnEdit = true;	
	Disable("btnDelete", true);	
	Disable("btnEdit", true);
	Disable("txtCredit", true);
	Disable("tblSelApplicableOnd", false);
	Disable("btnHFMAdd", false);
	Disable("btnHFMEdit", false);
	// Disable("agentPayCurrIn", true);
	// objCmbCurrency.disable(true);
	Disable("txtLocCredit", true);
	setField("hdnMode", "Edit");
	setField("hdnModel", "EDIT");	
	if(getValue("selAgentType") == "GSA"){
		objCmb6.disable(false);
		objCmb5.disable(false);
		getFieldByID("chkOnHold").checked = true;
		Disable("chkOnHold",true);
	}else{
		objCmb6.disable(true);
		objCmb5.disable(false);		
		getFieldByID("chkOnHold").checked = true;
		Disable("chkOnHold",true);
	}
	Disable("btnSave", false);
	if (getFieldByID("chkBilling").checked) {
		Disable("txtBillingEmail", true);		
	} else {
		Disable("txtBillingEmail", false);		
	}	
	DisableOnlyOnAcc("chkOnAcc", false);

	$("#trFixedCredit").hide();
	if (enableGSAV2) {
		Disable("chkGSA", true);
		Disable("selGSA", true);
	} else {
		Disable("chkGSA", disableReportingToGSA());
	}
	
	if(!blnsearchall) {
		if(!isAllowCC){
			Disable("chkCC", true);
			setVisible("spnCC", false);
			document.getElementById('spnCC').style.display= 'none';
		}
		if(!isAllowCA){
			Disable("chkCash", true);
			setVisible("spnCH", false);
			document.getElementById('spnCH').style.display= 'none';
		}
		if(!isAllowOA){
			Disable("chkOnAcc", true);
			document.getElementById('spnOA').style.display= 'none';
			getFieldByID("chkOnAcc").checked = false;
		}
		if(!isAllowBSP){
			Disable("chkBSP", true);
			document.getElementById('spnBSP').style.display= 'none';
			document.getElementById('spnBSPText').style.display= 'none';
			getFieldByID("chkBSP").checked = false;
		}
		if(!isAllowOffline){
			Disable("chkOffline", true);
			document.getElementById('spnOffline').style.display= 'none';
			getFieldByID("chkOffline").checked = false;
		}
		Disable("chkGSA", true);
	}

	if(!isAllowVoucher){
		Disable("chkVoucher", true);
		document.getElementById('spnVoucher').style.display= 'none';
		getFieldByID("chkVoucher").checked = false;
	}
	controlRouteSelection(false);
}

function setTabSearch(){
	// top[0].manifestSearchValue = getText("hdnRecNo") + "," +
	// getText("hdnSearchCriteria");
	var setValue = getText("hdnRecNo") + "," + getText("hdnSearchCriteria");
	top[1].objTMenu.tabSetValue("SC_ADMN_015", setValue);
	// top[0].intLastRec = getText("hdnRecNo");
}

function adjustPayClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		if(intStat==1){			
			setField("hdnMode","showAdjustPayment");
	
			closeChildWindow();
			
			var intLeft = (1024 - window.screen.width) /2;
			var intTop = (768 - window.screen.height) /2;
	
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=700,resizable=no,top=' + intTop + ',left=' + intLeft
			objWindow = window.open("about:blank","CWindow",strProp);
			top[0].objWindow = objWindow;
			
			var objForm  = document.getElementById("frmTravelAgent");
			objForm.target = "CWindow";
	
			objForm.action = "showAdjustmentPayment.action";
			objForm.submit();	
		}
	}
}


	// moved the js function to the js file
	function selAgentType_onChange(pageEdited) {		
		if( blnEdit && selectedAgentTypeCode != "" && (trim(carrierAgent) != "" && selectedAgentTypeCode != carrierAgent && getValue("selAgentType") == carrierAgent)) {
			showERRMessage(arrError["invalidchange"]);
			getFieldByID("selAgentType").focus();
			return false;
		} 
		
		if (getValue("selAgentType") == "GSA") {
			objCmb5.setText("");
			objCmb6.visible(true);
			objCmb5.visible(true);
			objCmb6.disable(false);
			objCmb5.disable(false);
			getFieldByID("chkOnAcc").checked = false;
			getFieldByID("chkOnHold").checked = true;
			Disable("chkOnHold",true);
			getFieldByID("chkGSA").checked = false;
			setField("selGSA",  'SELECT');
			$("#selectGSA").hide();
			// GSA V2
			getFieldByID("chkFixedCredit").checked = true;
			$("#trFixedCredit").hide();
			$("#trCreditLimit").show();
			$("#trBankGuarantee").show();
			$("#trBankGuaranteeExpire").show();
			
		} else {			
			objCmb6.setText("");
			objCmb6.visible(true);
			objCmb5.visible(true);
			objCmb5.disable(false);
			objCmb6.disable(true);
			getFieldByID("chkOnAcc").checked = false;
			getFieldByID("chkOnHold").checked = true;
			Disable("chkOnHold",true);		
			
			//GSA v2
			if (enableGSAV2 && !(carrierAgent == getValue("selAgentType")) && !(getValue("selAgentType") == "SO") 
					&& !(getValue("selAgentType") == "CO")){
				getFieldByID("chkFixedCredit").checked = false;				
				$("#trFixedCredit").show();
				$("#trCreditLimit").hide();
				$("#trBankGuarantee").hide();
				$("#trBankGuaranteeExpire").hide()
				$("#chkFixedCredit").show();
			} else {
				getFieldByID("chkFixedCredit").checked = true;
				$("#trFixedCredit").hide();
				$("#trCreditLimit").show();
				$("#trBankGuarantee").show();
				$("#trBankGuaranteeExpire").show();
			}
		}
		
		if(enableGSAV2) {
			loadVisibleTerritories(getValue("selAgentType"), null);
			showCreditSummary(true);
		}
			
		if (trim(carrierAgent) != "" && getValue("selAgentType") == carrierAgent) {
			getFieldByID("chkOnAcc").checked = false;
		}		
		DisableOnlyOnAcc("chkOnAcc", false);
		
		if(!blnEdit){
			setField("txtCredit", "");
			setField("txtLocCredit", "");
		}
		
		Disable("txtCredit", disableCreditLimit());
		Disable("txtLocCredit", disableCreditLimit());
		Disable("chkGSA", disableReportingToGSA());
		Disable("selGSA", disableReportingToGSA());
		currencyChange();
		setPageEdited(pageEdited);
	}
	
	function getTabValue() {
		var tabValue =top[1].objTMenu.tabGetValue("SC_ADMN_015"); // top[0].manifestSearchValue;
		if ((tabValue == "") || (tabValue == null)) {
			tabValue = new Array("");
		}
		return tabValue;
	}
	function setAmountSpn(e){
		var htmC = '';
		
		if(tooltipfeed == 'L'){
				htmC = '<font class="font">GSA/Agent Currency</font>';
		}else if (tooltipfeed == 'B'){
		        htmC = '<font class="font">Base Currency</font>';
		}else{
			    htmC = '<font class="font"></font>';
		}
		if(tooltipfeed!=''){
		 showTooltip(e, htmC);
		}
	}
	
	
	function onAgentCurChange(){
		var strAgentPay = getValue("agentPayCurrIn");
		var payIn='';
		
		if(strAgentPay == baseCurrency){
			payIn ='B';			 
		}else{
			payIn ='L';
		}
		
		if(payIn==''){
			alert("Unexpected Error Occurred.");
		}	
		var curr = objCmbCurrency.getText()
		if(blnEdit){ // on edit
			if(payIn == 'L'){
				updateLocCreditLimit();
				updateLocBankGuarantee();
				updateLocBSPGuarantee();				
			}
		}else{ // on add
			if(payIn == 'L'){
				if(lastPayInCurr != null && lastPayInCurr == payIn){
					updateBaseCreditLimit();
					updateBaseBankGuarantee();
					updateBaseBSPGuarantee();
				}else{					
					updateLocCreditLimit();
					updateLocBankGuarantee();
					updateLocBSPGuarantee();
				}
			}else{
				updateBaseCreditLimit();
				updateBaseBankGuarantee();
				updateBaseBSPGuarantee();
			}
		}
		lastPayInCurr = payIn;
	}
	
	function onAgentPayCurrInChange(){
		var strAgentPay = getValue("agentPayCurrIn");
		var payIn='';
		
		if(strAgentPay == baseCurrency){
			payIn ='B';			 
		}else{
			payIn ='L';
		}
		
		if(payIn==''){
			alert("Unexpected Error Occurred.");
		}	
		
		
		if(blnEdit){ // on edit
			if(payIn == 'L'){
				updateLocCreditLimit();
				updateLocBankGuarantee();
				updateLocBSPGuarantee();
			}
		}else{	// on add
			if(payIn == 'L'){
				updateLocCreditLimit();
				updateLocBankGuarantee();
				updateLocBSPGuarantee();
			}else{
				updateBaseCreditLimit();
				updateBaseBankGuarantee();
				updateBaseBSPGuarantee();
			}
		}
	}
	
	function currencyChange(){
		
		 var strAgentPay = getValue("agentPayCurrIn");
	 
		 var strCurr='';
		 var htmC = '';
			if(strAgentPay == baseCurrency){
				strCurr ='B';
				setField("selCurrIn","B")  
				 
			}else{
				strCurr ='L';
				setField("selCurrIn","L");
			}
			
		// setAmountSpn(strCurr);
		 
		if(strCurr==''){
			alert("Unexpected Error Occurred.");
		}	
		
		if(!blnEdit){			// add
			if(strCurr == "L" && allowLocalCR=='true') {
				
				$("#txtLocCredit").show();
				$("#txtLocAdvnace").show();
				$("#txtLocBSPGuarantee").show();	
				
				DivWrite("LocCurr1", objCmbCurrency.getText());
				DivWrite("LocCurr", objCmbCurrency.getText());
				DivWrite("LocCurrBSP", objCmbCurrency.getText());
				Disable("txtAdvnace", true);
				Disable("txtAdvnaceExpiryDate", false);
				isCalEnabled = true;
				Disable("txtCredit", true);	
				Disable("txtBSPGuarantee", true);	
				if(!disableCreditLimit()){
					Disable("txtLocCredit", false);
				}
				Disable("txtLocAdvnace", false);				
				
				if(maintainAgentBspCredLmt == "true"){
					Disable("txtLocBSPGuarantee", false);
				}
				
				
			}else {
				
				Disable("txtAdvnace", false );
				Disable("txtAdvnaceExpiryDate", false );
				isCalEnabled = true;
				
				
				if(!disableCreditLimit()){
					Disable("txtCredit", false);
				}
				Disable("txtLocCredit", true);
				Disable("txtLocAdvnace", true);	
				Disable("txtLocBSPGuarantee", true);
				$("#txtLocCredit").hide();
				$("#txtLocAdvnace").hide();
				$("#txtLocBSPGuarantee").hide();
				
				if(maintainAgentBspCredLmt == "true"){
					Disable("txtBSPGuarantee", false );
				}
				
				DivWrite("LocCurr1", "");
				DivWrite("LocCurr","");
				DivWrite("LocCurrBSP","");
			}
			
		}else {			// edit
			if(strCurr == "L" && allowLocalCR=='true') {						
				
				Disable("txtAdvnace", true);				
				var selCurrency = objCmbCurrency.getText();
				if(parseFloat(getExchangeRate(selCurrency))<0){
					Disable("txtLocAdvnace", true);					
					Disable("txtLocBSPGuarantee", true);					
				} else {
					Disable("txtLocAdvnace", false);
					if(maintainAgentBspCredLmt == "true"){
						Disable("txtLocBSPGuarantee", false);
					}
				}
				$("#txtLocCredit").show();
				$("#txtLocAdvnace").show();
				$("#txtLocBSPGuarantee").show();	
				DivWrite("LocCurr1", selCurrency);
				DivWrite("LocCurr", selCurrency);
				DivWrite("LocCurrBSP", selCurrency);
				Disable("txtAdvnace", true);
				Disable("txtAdvnaceExpiryDate", false);
				isCalEnabled = true;
				Disable("txtCredit", true);
				Disable("txtBSPGuarantee", true);				
				
			}else {				
				// setField("txtLocAdvnace", "0");
				Disable("txtAdvnace", false);
				Disable("txtAdvnaceExpiryDate", false);
				isCalEnabled = true;
				Disable("txtLocAdvnace", true);	
				
				if(maintainAgentBspCredLmt == "true"){
					Disable("txtBSPGuarantee", false);
				}				
								
				Disable("txtLocBSPGuarantee", true);		
				$("#txtLocCredit").hide();
				$("#txtLocAdvnace").hide();
				$("#txtLocBSPGuarantee").hide();	
				Disable("txtLocCredit", true);
				Disable("txtCredit", true);
				DivWrite("LocCurr1", "");
				DivWrite("LocCurr","");
				DivWrite("LocCurrBSP","");
			}
			
		}
	}
	
	function selStatus_onChange() {		
		setPageEdited(true);
	}
	
	function writeStatusSpan(){ 
		if(userAgentType !='GSA'){
			DivWrite("spnSelStatus", statusStart + statusList + statusEnd);
		}else{
			DivWrite("spnSelStatus", statusStart + "<option value='ACT'>Active</option>" + statusEnd);
		}
	}
	
	function writeDefaultStatus(){
		DivWrite("spnSelStatus", statusStart + statusList + statusEnd);
	}
	
	function populateAgentComm(){
		var htmAc = '';
		htmAc += '<font>Agent Commission</font>';
		htmAc += '<select id="selCommiType" name="selCommiType" size="1" onChange="" tabindex="">';
		htmAc += '<option value=""></option>';
		if(agentCommList != 'INA'){
			htmAc += agentCommList;
		}
		htmAc +='</select>';	
		DivWrite("showAgentComm", htmAc);
	}
	
	function fillAgentCurr(currType, currCode){
		var htm  = '<select id="agentPayCurrIn" name="agentPayCurrIn" size="1" onChange="currencyChange();onAgentPayCurrInChange();" tabindex="43">';
		if(currType=='L'){
		 	htm+='<option value = '+currCode+'>'+currCode+'</option>';
		}else{
			htm+='<option value = '+baseCurrency+'>'+baseCurrency+'</option>';
		}
		htm+='</select>';
		DivWrite("spnPayIn", htm);
		Disable("agentPayCurrIn", true);
	}
	
	function disableBG(){
		
	}
	
	function populateAgentCurr(selectGlobalPayIn){
		var htm  = '<select id="agentPayCurrIn" name="agentPayCurrIn" size="1" onChange="currencyChange();onAgentPayCurrInChange();" tabindex="43">';
		if(userAgentType=='GSA'){
			
			htm+=agentAgentCurr;
		}else{	
				var selCur = objCmbCurrency.getText();
				 if((selCur!='') && (allowLocalCR=='true') && selCur!= baseCurrency && !(parseFloat(getExchangeRate(selCur)) < 0 && ( payIn == 'B' ))){ 
				 	htm+='<option value = '+objCmbCurrency.getText()+'>'+objCmbCurrency.getText()+'</option>';
				 }
				 htm+=agentAgentCurr;
		}
		 htm+='</select>';
		 DivWrite("spnPayIn", htm);
		 if(selectGlobalPayIn){
			 if( payIn == 'L' ){
				setField('agentPayCurrIn',objCmbCurrency.getText());
			 }else{
				setField('agentPayCurrIn',baseCurrency)
			 }		
		 }
	}
	document.onkeyup = KeyCheck;       

	function KeyCheck(e){
		try{
			var KeyID = (window.event) ? event.keyCode : e.keyCode;
			switch(KeyID)
			   {
			      case 16:
					shift = true;
				  break;
				  case 39:
				  	ra = true; 
				  break;	
			   }
			   if(shift && ra){
			   	 // setField("txtLocAdvnace", arrData[strGridRow][38]);
				 // setField("txtLocCredit", arrData[strGridRow][39]);
				 
				 shift = false;
				 ra = false; 
			   }
		}catch(err){}
	}
	
	function validateAgentNotesTextArea(objControl) {
		var strValue = objControl.value;
		var strLength = strValue.length;
		
		if (strLength > 2000){
			strValue = strValue.substr(0,2000);
			objControl.value = strValue;
		}
	}
	
	function enableDisableAgentMinMaxMargin() {
		
		var minMaxDisable = ( getValue('selAppliesto') == '6' &&  getValue('selType') =='V' );	
		
		Disable("txtMinAmount", minMaxDisable );	
		Disable("txtMaxAmount", minMaxDisable );	
		setField("txtMinAmount",""); 
		setField("txtMaxAmount",""); 
		
	}
	
	function isStationVsCurrencyMatched(stationCode){
		
		for (i = 0; i < arrStationCurrency.length; i++) {
			if (arrStationCurrency[i][0] == objCmb5.getText()) {
				var matchingCurrency = arrStationCurrency[i][1];
			 	var selectedCurrency = objCmbCurrency.getText();
				  if(matchingCurrency==selectedCurrency){
				      return true;
				  } else{
					  return false;
				  }
			}
		}
	}
	
	function showCreditSummary(visibility){
		if(visibility && enableGSAV2){
			if(showCreditSummaryFlag){
				$("#trCreditSummary").show();
				$("#spnAvailableCredit").text(agentCreditLimit);
				$("#spnSharedCredit").text(agentAvailbleCredit);
				$("#spnDistributedCredit").text(agentDistributedCredit);
			} else {
				$("#trCreditSummary").hide();
			}
		} else {
			$("#trCreditSummary").hide();
		}
	}
	
	function isOwnTerritorySelected(){
		var allOptions = new Array;
		$('#lstAssignedTerritoryStations option').each(function(){
		      allOptions.push($(this).val());
		});
		var found = false; 
		if(allOptions.length > 0){
			var selectedTerritory = $("#txt_comboLayer_selTerritoryCmb1").val();
			if(selectedTerritory != null && selectedTerritory !== ""){
				$.each(allOptions, function( index, value ) {
					 if(value === selectedTerritory){
						 found = true;
					 }
				});
			}
		} 
		return found;
	}

	function btnAddSegment_click() {
		var isContained = false;
		top[2].HidePageMessage();
		var dept = $("#selDepature option:selected").text();
		var arr = $("#selArrival option:selected").text();
		var via1 = $("#selVia1 option:selected").text();
		var via2 = $("#selVia2 option:selected").text();
		var via3 = $("#selVia3 option:selected").text();
		var via4 = $("#selVia4 option:selected").text();

		var deptVal = $("#selDepature").val();
		var arrVal = $("#selArrival").val();
		var via1Val = $("#selVia1").val();
		var via2Val = $("#selVia2").val();
		var via3Val = $("#selVia3").val();
		var via4Val = $("#selVia4").val();

		if (dept == "") {
			showERRMessage(arrError["depatureRqrd"]);
			$("#selDepature").focus();

		} else if (dept != "" && deptVal == "INA") {
			showERRMessage(arrError["departureInactive"]);
			$("#selDepature").focus();

		} else if (arr == "") {
			showERRMessage(arrError["arrivalRqrd"]);
			$("#selArrival").focus();

		} else if (arr != "" && arrVal == "INA") {
			showERRMessage(arrError["arrivalInactive"]);
			$("#selArrival").focus();

		} else if (dept == arr && dept != "All" && (via1 == "" && via2 == "" && via3 == "" && via4 == "" )) {		
			showERRMessage(arrError["depatureArriavlSame"]);
			$("#selArrival").focus();
		} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
			showERRMessage(arrError["arriavlViaSame"]);
			$("#selArrival").focus();

		} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
			showERRMessage(arrError["depatureViaSame"]);
			$("#selDepature").focus();

		} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
			showERRMessage(arrError["vianotinSequence"]);
			$("#selVia1").focus();

		} else if ((via3 != "" || via4 != "") && via2 == "") {
			showERRMessage(arrError["depatureRqrd"]);	
			showERRMessage(vianotinSequence);
			$("#selVia1").focus();

		} else if ((via4 != "") && via3 == "") {
			showERRMessage(arrError["vianotinSequence"]);
			$("#selVia1").focus();

		} else if ((via1 != "" && via2 != "" && via3 != "")
				&& (via1 == via2 || via1 == via3 || via1 == via4)) {
			showERRMessage(arrError["viaEqual"]);
			$("#selVia1").focus();

		} else if ((via1 != "" && via2 != "" && via3 != "")
				&& (via2 == via3 || via2 == via4)) {
			showERRMessage(arrError["viaEqual"]);
			$("#selVia2").focus();

		} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
			showERRMessage(arrError["viaEqual"]);
			$("#selVia4").focus();

		} else if ((via1 != "" && via2 != "") && via1 == via2) {
			showERRMessage(arrError["viaEqual"]);
			$("#selVia1").focus();

		} else if (via1 != "" && via1Val == "INA") {
			showERRMessage(via1 + " " + arrError["viaInactive"]);
			$("#selVia1").focus();
		} else if (via2 != "" && via2Val == "INA") {
			showERRMessage(via2 + " " + arrError["viaInactive"]);
			$("#selVia2").focus();
		} else if (via3 != "" && via3Val == "INA") {
			showERRMessage(via3 + " " + arrError["viaInactive"]);
			$("#selVia3").focus();
		} else if (via4 != "" && via4Val == "INA") {
			showERRMessage(via4 + " " + arrError["viaInactive"]);	
			$("#selVia4").focus();
		} else {
			var str = "";
			str += dept;

			if (via1 != "") {
				str += "/" + via1;
			}
			if (via2 != "") {
				str += "/" + via2;
			}
			if (via3 != "") {
				str += "/" + via3;
			}
			if (via4 != "") {
				str += "/" + via4;
			}
			str += "/" + arr;

			var control = document.getElementById("selSelected");
			for ( var r = 0; r < control.options.length; r++) {
				if (control.options[r].text == str) {
					isContained = true;
					showERRMessage(arrError["ondRequired"]);
					break;
				}
			}
			if (isContained == false) {
				control.options[control.length] = new Option(str, str);
			}
		}
	}
	
	function btnRemoveSegment_click() {
		var control = document.getElementById("selSelected");

		var selIndex = control.selectedIndex;
		if (selIndex != -1) {
			for (i = control.length - 1; i >= 0; i--) {
				if (control.options[i].selected) {
					control.options[i] = null;
				}
			}
			if (control.length > 0) {
				control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			}
		}

	}

	function clearAllOnds() {
		var control = document.getElementById("selSelected");
		control.options.length = 0;
		for ( var t = 0; t <= (control.length); t++) {
			control.options[t] = null;
		}

	}
	

	function getAllOndList() {
		var control = document.getElementById("selSelected");
		var values = "";
		for ( var t = 0; t < (control.length); t++) {
			var str = control.options[t].text;
			var strReplace = control.options[t].text;
			if (str.indexOf("All") != -1) {
				strReplace = str.replace(/All/g, "***");
			}
			values += strReplace + ",";
		}
		return values;
	}
	
	function setApplicableOnds(rowId){
		clearAllOnds();
		var selectedOnds = arrData[rowId][72].split(',')
		var control = document.getElementById("selSelected");
		
		for (i = 0; i < selectedOnds.length; i++) {
			if(selectedOnds[i] != ""){
				control.options[control.length] = new Option(selectedOnds[i], selectedOnds[i]);				
			}
		}
		setField("hdnOndList", getAllOndList());
	}
