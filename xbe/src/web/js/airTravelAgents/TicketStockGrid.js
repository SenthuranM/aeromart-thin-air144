	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "13%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "" ;
	objCol1.headerText = "Date";
	objCol1.itemAlign = "center"
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "14%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "" ;
	objCol2.headerText = "Login ID";
	objCol2.itemAlign = "left"
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "14%";
	objCol3.arrayIndex = 3;
	objCol3.toolTip = "" ;
	objCol3.headerText = "User Name";
	objCol3.itemAlign = "left"
	
	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "13%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "Series From" ;
	objCol4.headerText = "Series From";
	objCol4.itemAlign = "right"
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "13%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "" ;
	objCol5.headerText = "Series To";
	objCol5.itemAlign = "left"	
		
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "13%";
	objCol6.arrayIndex = 6;
	objCol6.toolTip = "Unused Balance" ;
	objCol6.headerText = "Un-used Balance";
	objCol6.itemAlign = "right"

	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "20%";
	objCol7.arrayIndex = 7;
	objCol7.toolTip = "Reason for Change" ;
	objCol7.headerText = "Reason for Change";
	objCol7.itemAlign = "right"
		
	// ---------------- Grid	
	var objDG = new DataGrid("spnStockHistory");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol6);
	objDG.addColumn(objCol7);
	
	
	objDG.width = "100%";
	objDG.height = "144px";
	objDG.headerBold = false;
	objDG.arrGridData = arrStockData;
	objDG.seqNo = true;
	objDG.rowOver = true;
	objDG.backGroundColor = "#F4F4F4";
	objDG.rowClick = "stockClick";
	objDG.displayGrid();
