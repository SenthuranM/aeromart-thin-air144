//Java script Document


var strCarry;
var strLen=0;

var arrGroup2 = new Array();
var arrData2 = new Array();
arrData2[0] = new Array();
var ls = new Listbox('lstSchemes', 'lstAssignedSchemes', 'spnIncentive');
ls.group1 = schemes;
ls.height = '200px'; 
ls.width = '300px';
ls.headingLeft = '&nbsp;&nbsp;All Schemes';
ls.headingRight = '&nbsp;&nbsp;Selected Schemes';
ls.drawListBox();

function OnLoad(strMsg, strMsgType){
	if(curentSchemes != null && curentSchemes != ""){
		curentSchemes = ""+curentSchemes;//making sure this is a String
		ls.selectedData(curentSchemes);
	}
	if (strMsg != null && strMsgType != null && strMsg != "") {
		//showCommonError(strMsgType,strMsg);	
		showWindowCommonError(strMsgType, strMsg);
	}
}

function saveClick() {
	
	var selSchemes = ls.getselectedData();
	if(selSchemes == ""){
		showWindowCommonError("Error",'Select a scheme');		
		return false;
	}
	setField("hdnSelctdIncentive", selSchemes);
	setField("hdnAgentCode", opener.arrData[opener.strGridRow][1]);
	setField("hdnMode", "SAVE");
	document.forms[0].submit();
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function closeClick() {
		setPageEdited(false);
		window.close();	
}