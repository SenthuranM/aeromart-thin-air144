//Java script Document


var strCarry;
var strLen=0;
var objInterval = setInterval("gridLoad()", 300);

function gridLoad() {
	if (objDG.loaded == true) {
		clearInterval(objInterval);		
	}
}
 
function CHOnLoad(strMsg, strMsgType) {
	clearFields();
	DisableFields(true);
	Disable("btnSave", true);
	Disable("btnEdit", true);
	DivWrite("spnAgentName", opener.arrData[opener.strGridRow][2]);		
	DivWrite("spnRunSeries", lastEticket);
	
	if ((strMsg != null) && (strMsgType != null) && (strMsg != "")) {
		if (arrFormData != null && strMsgType == "Error") {
			setField("txtSrsQty", arrFormData[0]);
			setField("txtRemarks", arrFormData[2]);
		}	
		showWindowCommonError(strMsgType, strMsg);
	}	
}

function saveClick() {
	if (isEmpty(getText("txtSrsQty"))) {
		showWindowCommonError("Error", "Stock Quantity is Blank");		
		getFieldByID("txtSrsQty").focus();
		return false;
	}  else if (isEmpty(trim(getText("txtRemarks")))) {
		showWindowCommonError("Error", "Remarks is Blank");		
		getFieldByID("txtRemarks").focus();		
		return false;
	}else if( !validateInteger(getText("txtSrsQty"))) {
		showWindowCommonError("Error", "Stock Quantity must be a number");		
		getFieldByID("txtSrsQty").focus();		
		return false;
	}
	
	var tempName = trim(getText("txtRemarks"));
	tempName=replaceall(tempName,"'" , "`");
	setField("txtRemarks", tempName);
	setField("hdnAgentCode", opener.arrData[opener.strGridRow][1]);
	setField("hdnMode", "SAVE");
	document.forms[0].submit();
	
	
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function closeClick() {
	if (loadCheck()) {
		setPageEdited(false);
		window.close();
	}
}

function addClick() {
	clearFields();
	DisableFields(false);
	Disable("btnSave", false);
}

function loadCheck() {
	if (top.pageEdited) {
		return confirm("Changes will be lost! Do you wish to Continue?");
	} else {
		return true;
	}
}

function removeInvalidChars(val) {
	var temp = val;
	for (var i = 0; i < temp.length; i++) {
		if ((temp.charAt(i) != ".") && (!isInt(temp.charAt(i)))) {
			temp = temp.replace(temp.charAt(i), "");
		}
	}
	return temp;
}

function DisableFields(cond) {
	Disable("txtSrsQty", cond);
	Disable("txtRemarks", cond);
}

function clearFields() {
	setField("txtSrsQty", "");
	setField("txtRemarks", "");
}



