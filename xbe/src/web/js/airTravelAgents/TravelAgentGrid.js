	var tabValue =top[1].objTMenu.tabGetValue("SC_ADMN_015"); // top[0].manifestSearchValue;
	var recNo =1;	
	if (tabValue != null && tabValue != "") {
		recNo = tabValue.split(",")[0];
		}
	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "9%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "" ;
	objCol1.headerText = "Agent / GSA ID";
	objCol1.itemAlign = "left";
	objCol1.sort = "true";
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "10%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "" ;
	objCol2.headerText = "Name";
	objCol2.itemAlign = "left";
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "8%";
	objCol3.arrayIndex = 30;
	objCol3.toolTip = "" ;
	objCol3.headerText = "Territory <br>/ Station";
	objCol3.itemAlign = "left"	
	
	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "8%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "" ;
	objCol4.headerText = "Country";
	objCol4.itemAlign = "left";
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "10%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "" ;
	objCol5.headerText = "Credit Limit  " + baseCurrency;
	objCol5.itemAlign = "right";
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "5%";
	objCol6.arrayIndex = 6;
	objCol6.toolTip = "" ;
	objCol6.headerText = "Reporting <br> to GSA";
	objCol6.itemAlign = "center";
	
	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "5%";
	objCol7.arrayIndex = 7;
	objCol7.toolTip = "" ;
	objCol7.headerText = "Status";
	objCol7.itemAlign = "left";
	
	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "9%";
	objCol8.arrayIndex = 31;
	objCol8.toolTip = "" ;
	objCol8.headerText = "Available <br>Credit";
	objCol8.itemAlign = "right";
	
	var objCol9 = new DGColumn();
	objCol9.columnType = "label";
	objCol9.width = "6%";
	objCol9.arrayIndex = 32;
	objCol9.toolTip = "" ;
	objCol9.headerText = "Amount<br> Due";
	objCol9.itemAlign = "right";
	
	/*var objCol10 = new DGColumn();
	objCol10.columnType = "label";
	objCol10.width = "8%";
	objCol10.arrayIndex = 42;
	objCol10.toolTip = "Contact Person's Name" ;
	objCol10.headerText = "Contact<br> Person";
	objCol10.itemAlign = "left";*/

	/*var objCol11 = new DGColumn();
	objCol11.columnType = "label";
	objCol11.width = "6%";
	objCol11.arrayIndex = 34;
	objCol11.toolTip = "License Number" ;
	objCol11.headerText = "License";
	objCol11.itemAlign = "left";*/

	var objCol12 = new DGColumn();
	objCol12.columnType = "label";
	objCol12.width = "8%";
	objCol12.arrayIndex = 35;
	objCol12.toolTip = "Is on hold booking allowed" ;
	objCol12.headerText = "OHD";
	objCol12.itemAlign = "center";
	
	var objCol13 = new DGColumn();
	objCol13.columnType = "label";
	objCol13.width = "10%";
	objCol13.arrayIndex = 59;
	objCol13.toolTip = "" ;
	objCol13.headerText = "Credit Limit Specified";
	objCol13.itemAlign = "right";
	
	var objCol14 = new DGColumn();
	objCol14.columnType = "label";
	objCol14.width = "8%";
	objCol14.arrayIndex = 64;
	objCol14.toolTip = "" ;
	objCol14.toolTip = "Available BSP credit" ;
	objCol14.headerText = "BSP Credit";
	objCol14.itemAlign = "right";
	
	var objCol15 = new DGColumn();
	objCol15.columnType = "label";
	objCol15.width = "7%";
	objCol15.arrayIndex = 71;
	objCol15.toolTip = "" ;
	objCol15.headerText = "IATA code";
	objCol15.itemAlign = "left";
	
	
	// ---------------- Grid	
	var objDG = new DataGrid("spnTravelAgents");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol15);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol13);
	objDG.addColumn(objCol8);
	objDG.addColumn(objCol9);
	objDG.addColumn(objCol14);	
	objDG.addColumn(objCol6);
	objDG.addColumn(objCol7);
	/*objDG.addColumn(objCol10);
	objDG.addColumn(objCol11);*/
	objDG.addColumn(objCol12);
	
	objDG.width = "100%";
	
	if(arrData.length > 0) {
		if(blnsearchall) {
			objDG.height = "123px";
		}else {
			objDG.height = "143px";
		}		
	}else {
		if(blnsearchall) {
			objDG.height = "143px";
		}else {
			objDG.height = "163px";
		}	
		
	}
	objDG.headerBold = false;
	objDG.rowSelect = true;
	objDG.arrGridData = arrData;
	objDG.seqNo = true;
	objDG.seqStartNo = recNo;
	objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
	objDG.paging = true
	objDG.pgnumRecPage = 10;
	objDG.pgonClick = "gridNavigations";	
	objDG.rowClick = "rowClick";	
	objDG.displayGrid();
	
	function RecMsgFunction(intRecNo, intErrMsg){
		top[2].HidePageMessage();
		if (intErrMsg != ""){
			top[2].objMsg.MessageText = intErrMsg ;
			top[2].objMsg.MessageType = "Error" ; 
			top[2].ShowPageMessage();
		}
	}
	
