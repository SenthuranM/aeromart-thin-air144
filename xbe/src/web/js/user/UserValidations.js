/*
 * Author : Thushara
*/
var strRowData;
var strGridRow;
var passwordChanged;
var blnEdited = false;

var screenId = "SC_ADMN_002";
var valueSeperator = "~";
var strRemoteID = "";
var PLAIN_PASSWORD="";
var reqResponseString="";

var selectedUserIndex = -1;
var pswd="";

/*
 * Function Call for the Page Load
 */
	function winOnLoad(strMsg,strMsgType) {	
		setPasswordEncrypt();
		passwordChanged=false;
		clearUser();	
		setPageEdited(false);
		Disable("btnSave", "false");
		Disable("btnReset","false");
		setField("hdnEditable","Y");
		setVisible("dvAdmin", false);
		
		document.getElementById("spnAirlineCode").innerHTML=defaultAirlineCode;	
		if (saveSuccess == 1) {
			alert("Record Successfully saved!");
			saveSuccess = 0;
		}
		var strTmpSearch =  searchData; //getTabValues(screenId, valueSeperator, "strSearchCriteria");
		if(strTmpSearch !="" && strTmpSearch != null){	//sets the search data	
			var strSearch=strTmpSearch.split(",");
			setField("txtLoginIdSearch",strSearch[0]);
			setField("txtLastNameSearch",strSearch[1]);
			setField("selAirportSearch",strSearch[2]);
						
			if(strSearch.length > 7){
				setField("selTravelAgentSearch",strSearch[3]+","+strSearch[4]); 	
				setField("selSearchStatus",strSearch[6]);
				setField("selAirlineCode",strSearch[7]);
			}else {
				setField("selTravelAgentSearch",strSearch[3]);
				setField("selSearchStatus",strSearch[5]);
				setField("selAirlineCode",strSearch[6]);
			}			
		}			
		
		if(arrFormData != null && arrFormData.length > 0) { //if Error
			strGridRow = svdGird; // getTabValues(screenId, valueSeperator, "strGridRow"); //Get saved grid Row
			setField("hdnEditable",preEditable);
			setField("txtLoginId",validCarrierPreFixValue(arrFormData[0][1]));
			setField("pwdPassword",arrFormData[0][2]);
			setField("pwdVeriPassword",arrFormData[0][2]);			
			setField("txtFirstName",arrFormData[0][3]);
			setField("txtEmail",arrFormData[0][5]);				
			setField("selTravelAgent",arrFormData[0][8]);
			setField("selDefCarrier",arrFormData[0][12]);
			setField("selTheme",arrFormData[0][22]);
			setField("txtFFPNumber",arrFormData[0][23]);
			if(isServCh){
				setField("selServiceChannel",arrFormData[0][21]);
			}
			if(setMyIDCarrier){
				setField("txtMyIDCarrier",arrFormData[0][24]);
			}
			var strCarrCode = arrFormData[0][13];
			if (trim(strCarrCode)!=""){
				var strCarrArray = strCarrCode.split(",");
				for ( var x=0; x<strCarrArray.length ; x++){
					setSelectedCarrier(strCarrArray[x]);
				}
			}
							
			if (arrFormData[0][9] == "Active" || arrFormData[0][9] =="active"
			|| arrFormData[0][9] =="on" || arrFormData[0][9] =="ACT" )
				getFieldByID("chkStatus").checked = true;
			else
				getFieldByID("chkStatus").checked = false;	
				
			if(document.getElementById('spn1'))
			ls.selectedData(arrFormData[0][10]);
	
			if(isAddMode) {
				enableControls();			
			}
			else{
				enableControls();
				Disable("txtLoginId",true);				
			}
			Disable("btnSave", "");	
			Disable("btnReset","");
			setPageEdited(true);
			setField("hdnAction",preAction);
			if (prePWDChanged == "Y") {
				passwordChanged = true;
			} else {
				passwordChanged = false;
			}
			setField("hdnVersion",preVersion);
			if (saveSuccess == 3) {
				saveSuccess = 0;			
			}
			if(arrFormData[0][14] == 'ADMIN'){
				getFieldByID("chkAdmin").checked = true;
				adminClick();
				if(arrFormData[0][15] == 'ANY'){
					getFieldByID("chkAUAny").checked = true;
				}
				if(arrFormData[0][16] == 'OWNER'){
					getFieldByID("chkAUOwner").checked = true;
				}
				if(arrFormData[0][17] == 'REPORTING'){
					getFieldByID("chkAUReporting").checked = true;
				}
				if(arrFormData[0][18] == 'ANY'){
					getFieldByID("chkOUAny").checked = true;
				}
				if(arrFormData[0][19] == 'OWNER'){
					getFieldByID("chkOUOwner").checked = true;
				}
				if(arrFormData[0][20] == 'REPORTING'){
					getFieldByID("chkOUReporting").checked = true;
				}
			}					
	
		}
		
		if(strMsg != null && strMsgType != null) {
			if(strMsgType == "Error") {
				showPageERRMessage(strMsg);
			}
			if(strMsgType == "Confirmation") {
				showPageConfMessage(strMsg);
			}
		}
		
		if(noOfCarriers == "1") {
			if(getFieldByID("selCarrierCode")){
				if(getFieldByID("selCarrierCode").options[0]){
					getFieldByID("selCarrierCode").options[0].selected =true;
					var carr = getFieldByID("selCarrierCode").options[0].value;			
					setField("selDefCarrier",carr);
				}
				
			}
			
		}	 
		setField("hdnMode","DISPLAY");
		
		if(strDryUserEnable == "N" ){
			setVisible("fntDryUserLevel", false);
			setVisible("fntDrySellUserLevel", false);
			setVisible("fntDrySellUserLeveMan",false);
		}
		if(isRemoteUser == "true") {
			Disable("btnAdd",true);
			Disable("btnEdit",true);
			Disable("btnDelete",true);
			Disable("btnSetPass",true);
			Disable("btnSave", true);
			Disable("btnReset",true);
			Disable("btnGetPassword",true)
			Disable("btnUnAdd",true);
			Disable("btnUnView",true);
		}
		// Page control by app parameter - isAllowAirlineCarrierPrefixForAgentsAndUsers
		controlPageDisplayCarrierPrefix();
		
	}
	
function controlPageDisplayCarrierPrefix() {
	if (isAllowAirlineCarrierPrefix == 'false') {
		$("#spnCtlAirlineCode").hide();
		$("#selAirlineCode").hide();
		$("#tdAirlineCode").hide();			
	} else {
		$("#selAirlineCode").show();
		$("#tdAirlineCode").show();
		$("#spnCtlAirlineCode").show();
	}	
}

function hide(id) {
	document.getElementById(id).style.display = "none";
}

function validCarrierPreFixValue(value) {	
	if (isAllowAirlineCarrierPrefix != 'false')  {
		value = value.substr(3);
	}
	return value;
}

/*
 * Function to clear the input Fields
 */
function clearUser() {
	objOnFocus();
	setPageEdited(false);
	setField("txtLoginIdSearch", "");	
	setField("txtLoginId","");
	setField("pwdPassword","");
	setField("pwdVeriPassword","");
	setField("txtFirstName","");
	setField("txtEmail","");
	setField("selDefCarrier","");
	setField("maxAdultAllowed","");
	if(isServCh){
		setField("selServiceChannel","SELECT");
	}
	if(setMyIDCarrier){
		setField("txtMyIDCarrier","");
	}
	getFieldByID("chkStatus").checked = false;
	deselectCarriers();
	if(noOfCarriers == "1") {
		if(getFieldByID("selCarrierCode")){
			if(getFieldByID("selCarrierCode").options[0]){
				getFieldByID("selCarrierCode").options[0].selected =true;
				var carr = getFieldByID("selCarrierCode").options[0].value;			
				setField("selDefCarrier",carr);
			}
		}
		
	}
	disableInputControls();	
	
}
/*
 * Function call for SEARCH Button Click
 */
function searchUser() {
	objOnFocus();
	if (checkPageEdited(blnEdited)) {	
		setField("hdnMode","SEARCH");
		searchData = getText("txtLoginIdSearch")+","+getText("txtLastNameSearch")+","+
		 			 getValue("selAirportSearch")+","+getValue("selTravelAgentSearch") 
		 			 +",''," + getValue("selSearchStatus") + "," + getValue("selAirlineCode");
		setField("hdnRecNo",1);
		setField("hdnSearchData",searchData);
		setField("hdnSearchAirline",getValue("selAirlineCode"));
		document.frmUser.submit();
		ShowProgress();
	}	
}


var formLoadAgentSuccess = function(response) {
	var agentOptions = '<option VALUE="SELECT">Select</option>' + response.agentOptions;
	$("#selTravelAgent").html(agentOptions);
}

var searchLoadAgentSuccess = function(response) {
	var agentOptions = '<option VALUE="ALL">All</option>' + response.agentOptions;
	$("#selTravelAgentSearch").html(agentOptions);
}

function searchLoadAgentClick() {
	var data = {};
	data["station"] = getValue("selAirportSearch");
	loadAgentsForStation(data, searchLoadAgentSuccess);
}

function formLoadAgentClick() {
	var data = {};
	data["station"] = getValue("selAirport");
	loadAgentsForStation(data, formLoadAgentSuccess);
}

function loadAgentsForStation(data, successfn) {
	ShowProgress();
	$.ajax({ 
		url : 'showUser!loadAgents.action',
		beforeSend : ShowProgress(),
		data : data,
		type : "POST",
		async:false,
		dataType : "json",
		success : successfn,
		complete:function(response){
			HideProgress();		
		}
	});
}

/*
 * Function call for GET PASSWORD Button Click
 */
function getPassword() {
	objOnFocus();
	var url;
	
	if(document.forms[0].hdnMode.value=="ADD"){
		alert("User details should be saved first, to get the password");	
	}
	else
	{
		setPasswordEncrypt();
		setField("hdnMode","GETPASSWORD");	
		
		if(!isAlphaNumericUnderscore(getText("txtLoginId"))) 
		{		
			showPageERRMessage(arrError["userIdInvalid"]) ; 
			getFieldByID("txtLoginId").focus();
		}
		else
		{
			//setPassword();
			//ShowProgress();
			strRemoteID="GETPASSWORD";
			url="saveXBEData.action"
			var params="hdnCommands=GETPASSWORD&txtLoginId="+getValue('txtLoginId');
			makePOST(url,params);
			
		}
	}
		
	
}


function setPassword(){

	if(document.getElementById("pwdPassword").style.display=="none"){

	document.getElementById("pwdPassword").style.display="block";
	document.getElementById("txtPlnPassword").style.display="none";

	}

	else if(document.getElementById("pwdPassword").style.display=="block"){

	document.getElementById("pwdPassword").style.display="none";
	document.getElementById("txtPlnPassword").style.display="block";
	}
}

function setPasswordEncrypt()
{


	document.getElementById("pwdPassword").style.display="block";
	document.getElementById("txtPlnPassword").style.display="none";

}

function processStateChange() {
	if (httpReq.readyState == 4){
  		if (httpReq.status == 200){
	    
			strSYSError	  = "";
			eval(httpReq.responseText);
      		if (strSYSError == ""){

      			var arrRemoteID = strRemoteID.split(",");
      			
	      		for (var a = 0 ; a < arrRemoteID.length ; a++){

	      			switch (arrRemoteID[a]){
	      				case "GETPASSWORD" :
						setPassword();
						
//						for (var b = 0; b <httpReq.responseText.length; b++)
//						{
//						
//						pswd = pswd + httpReq.responseText[b];
//						
//						
//						}
						
						pswd= pswd.replace(/^\s+|\s+$/g, '') ;

	      					setField("txtPlnPassword",pswd);
	      					break;
	      
	      		}
	      		}
      		}else{
		    	showERRMessage(strSYSError);
	    	}
	    }else{
      		parent.HideProgress();

  		}
	}

}

/*
 * Function call for GRID ROW ITEM Click
 */
function rowClick(strRowNo){
	selectedUserIndex = strRowNo;
	objOnFocus();
	setPasswordEncrypt();
	document.forms[0].hdnMode.value="";
	if (checkPageEdited(blnEdited)) {
		setPageEdited(false);
		var optValue = userData[strRowNo][9];
		var optDomain = userData[strRowNo][8];	
		strRowData = frames["grdUsers"].objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		clearControls();
		setField("hdnEditable",userData[strGridRow][14]);
		setField("txtLoginId",validCarrierPreFixValue(strRowData[0]));
		var usersAirline = strRowData[0].substring(0,3);
		if (isAllowAirlineCarrierPrefix == 'false') {
			usersAirline = defaultAirlineCode;
		}
		document.getElementById("spnAirlineCode").innerHTML= usersAirline;
		setField("pwdPassword",userData[strRowNo][11]);
		setField("pwdVeriPassword",userData[strRowNo][11]);
		setField("txtFirstName",strRowData[1]);		
		if(strRowData[2] == "&nbsp") {
			setField("txtEmail","");
		}else {
			setField("txtEmail",strRowData[2]);
		}		
		setField("selTravelAgent",userData[strRowNo][13]);	
		setField("hdnVersion",userData[strRowNo][10]);		
		setField("hdnStatus",userData[strRowNo][7]);
		setField("hdnCarrierCode",userData[strRowNo][16]);
		setField("selDefCarrier",userData[strRowNo][17]);
		var strCarrCode = userData[strRowNo][16];
		if (trim(strCarrCode)!=""){
			var strCarrArray = strCarrCode.split(",");
			for ( var x=0; x<strCarrArray.length ; x++){
				setSelectedCarrier(strCarrArray[x]);
			}
		}
		if(userData[strRowNo][7] == "Active") {
			getFieldByID("chkStatus").checked = true;
		}
		else{
			getFieldByID("chkStatus").checked = false;
		}
	
		if(userData[strRowNo][12] != "") {
			if(document.getElementById('spn1'))
			ls.selectedData(userData[strRowNo][12]);
		}	
		clearAdminstatus();
		displayAdminGrdData(userData[strRowNo][18]);
		displayNormalGrdData(userData[strRowNo][19]);
		if(userData[strRowNo][18] != 'N' || userData[strRowNo][19] != 'N'){
			getFieldByID("chkAdmin").checked = true;
		}else {
			getFieldByID("chkAdmin").checked = false;
		}	
		if(isServCh){
			setField('selServiceChannel',userData[strRowNo][20]);
		}
		setField('selTheme',userData[strRowNo][21]);
		setField('txtFFPNumber',userData[strRowNo][22]);
		if(setMyIDCarrier){
			setField("txtMyIDCarrier",userData[strRowNo][23]);
		}
		setField('maxAdultAllowed',userData[strRowNo][24]);
		adminClick();
		disableInputControls();	
		if (getText("hdnEditable") == "Y" && usersAirline == defaultAirlineCode && isRemoteUser != "true") {
			Disable("btnEdit",false);
			Disable("btnDelete",false);
			Disable("btnSetPass",false);
			Disable("btnSave", true);			
			Disable("btnReset",true);
			Disable("btnGetPassword",false)
			Disable("btnUnAdd",false);
			Disable("btnUnView",false);
			Disable("btnManageFFP",false);
		} else {
			Disable("btnEdit",true);
			Disable("btnDelete",true);
			Disable("btnSetPass",true);
			Disable("btnSave", true);
			Disable("btnReset",true);
			Disable("btnGetPassword",true)
			Disable("btnUnAdd",true);
			Disable("btnUnView",true);
			Disable("btnManageFFP",true);
		}
		if (usersAirline != defaultAirlineCode) {
			if(document.getElementById('spn1')) {
				document.getElementById('spn1').style.visibility = "hidden";
			}
			document.getElementById('selTravelAgent').style.visibility = "hidden";
			document.getElementById("spnAgent").innerHTML=userData[strRowNo][6];	
		} else {
			if(document.getElementById('spn1')) {
				document.getElementById('spn1').style.visibility = "visible";
			}
			document.getElementById('selTravelAgent').style.visibility = "visible";
			document.getElementById("spnAgent").innerHTML="";			
		}
	}
}

/*
 * Function Call for ADD Button Click
 */
function addUser() {
	objOnFocus();
	setPasswordEncrypt();
	document.getElementById("spnAirlineCode").innerHTML=defaultAirlineCode;
	if(document.getElementById('spn1')) {
		document.getElementById('spn1').style.visibility = "visible";
	}
	document.getElementById('selTravelAgent').style.visibility = "visible";
	document.getElementById("spnAgent").innerHTML="";
	if (checkPageEdited(blnEdited)) {
		setPageEdited(false);
		setField("hdnEditable","Y");			
		enableControls();
		clearControls();
		adminClick();
		Disable("btnEdit", "false");
		Disable("btnSetPass","false");
		Disable("btnDelete","false");
		getFieldByID("txtLoginId").focus();	
		setField("hdnMode","ADD");
		setField("hdnAction","ADD");						
		setField("hdnVersion","");
		getFieldByID("chkStatus").checked = true;			
		Disable("btnSave", "");	
		Disable("btnReset","");
		Disable("btnSave", "");
		Disable("btnGetPassword","true")
		Disable("btnUnAdd",true);
		Disable("btnUnView",true);
		
		if(isFFPMaintainable){
			Disable("txtFFPNumber",false);
		}
		else{
			Disable("txtFFPNumber",true);
		}
		Disable("chkRoleChange",false);
		$( "#tabs" ).tabs( "option", "active", 0 );
	}
}

/*
 * Function call for EDIT Button Click
 */
function editUser() {
	objOnFocus();
	enableControls();
	setPasswordEncrypt();
	adminClick();
	adminAnyClick();
	NormAnyClick();
	if(getFieldByID("chkAdmin").checked && getFieldByID("selTravelAgent").value != "SELECT"){
 		var agType = getFieldByID("selTravelAgent").value;
 		var arrAgTypes = agType.split(",");
 		if(arrAgTypes.length > 1 && arrAgTypes[1] != 'GSA'){
			if(getFieldByID("chkAUReporting")){
	 			getFieldByID("chkAUReporting").checked = false;
	 			Disable("chkAUReporting", true);
	 		}
	 		if(getFieldByID("chkOUReporting")) {
	 			getFieldByID("chkOUReporting").checked = false;
	 			Disable("chkOUReporting", true);
	 		} 			
 		}
	}
	Disable("txtLoginId",true);	
	Disable("pwdPassword", true);
	Disable("pwdVeriPassword", true);
	getFieldByID("txtFirstName").focus();	
	Disable("btnSave", false);	
	Disable("btnReset",false);	
	Disable("btnDelete",true);
	Disable("btnSetPass",true);
	setField("hdnMode","EDIT");
	setField("hdnAction","EDIT");	
	Disable("btnGetPassword",false);
	
	if (userAgentType != carAgent && enableGSAV2) {
		Disable("selTravelAgent", true);
		Disable("selAirport", true);
		Disable("btnLoadAgents", true);
	}
	
	if(isFFPMaintainable){
		Disable("txtFFPNumber",false);
	}
	else{
		Disable("txtFFPNumber",true);
	}

	Disable("chkRoleChange",false);
	$( "#tabs" ).tabs( "option", "active", 0 );
}

/*
 * Function for DELETE Button Click
 */
function deleteUser() {
	objOnFocus();
setPasswordEncrypt();
	var strConfirm = confirm(arrError["deleteConf"]);
	if (strConfirm == true) {
		setField("hdnMode","DELETE");
		Disable("txtLoginId",false);		
		setField("hdnRecNo",1);
		document.frmUser.submit();
		ShowProgress();		
	}
}



function setSelectedCarrier(code){
	
	if(getFieldByID("selCarrierCode")){
		var intCount = getFieldByID("selCarrierCode").length ;
		 for (var i = 0; i < intCount; i++){
		 	if (trim(getFieldByID("selCarrierCode").options[i].value)==trim(code)){
		   	  getFieldByID("selCarrierCode").options[i].selected=true;
		   	}
		}
	}	
}

function deselectCarriers(){
	if(getFieldByID("selCarrierCode")){
		var intCount = getFieldByID("selCarrierCode").length ;
		 for (var i = 0; i < intCount; i++){
		   	  getFieldByID("selCarrierCode").options[i].selected=false;
		}		
	}	
}

function isSelectCarriers(){
	if(getFieldByID("selCarrierCode")){
		var intCount = getFieldByID("selCarrierCode").length ;
		 for (var i = 0; i < intCount; i++){
		   	  if(getFieldByID("selCarrierCode").options[i].selected){
		   	  	return true;
		   	  }
		}
	}else {
		return true;
	}	
	return false;
}

/*
 * Function call for SAVE Button Click
 */
function saveData() {
	var hdnAction = getValue("hdnAction");
	objOnFocus();
	setField("hdnMode","SAVE");

	var strVisibile="";
	if(getFieldByID("selCarrierCode")){
		var intCount = getFieldByID("selCarrierCode").length ;
		 for (var i = 0; i < intCount; i++){
		 	if (getFieldByID("selCarrierCode").options[i].selected){
		   	  strVisibile+=getFieldByID("selCarrierCode").options[i].value+",";
		   	}
		 }
	}

    if (getValue("hdnAction") == "CHANGEPASS") {
        resetSave();
        return;
    } else if (hdnAction == "CHANGEFFP") {
        saveFFP();
        return;
    }
	setField("hdnCarrierCode", strVisibile);
	Disable("txtLoginId","");	
	if(isEmpty(getText("txtLoginId"))) {		
		showPageERRMessage(arrError["userIdRqrd"]) ; 
		getFieldByID("txtLoginId").focus();
	}else {
		if(!isAlphaNumericUnderscore(getText("txtLoginId"))) {		
			showPageERRMessage(arrError["userIdInvalid"]) ; 
			getFieldByID("txtLoginId").focus();
		}else {
			if(isEmpty(getText("pwdPassword")) && getValue("hdnAction") != "EDIT") {			
				showPageERRMessage(arrError["userPasswordRqrd"]) ;
				getFieldByID("pwdPassword").focus();
			}else {		
				if(getText("pwdPassword").length < 8  && getValue("hdnAction") != "EDIT") {
					showPageERRMessage(arrError["userPasswordlength"]) ;
					getFieldByID("pwdPassword").focus();
				}else if(getText("pwdPassword").length > 12  && getValue("hdnAction") != "EDIT"){
					showPageERRMessage(arrError["userMaxPasswordlength"]) ;
					getFieldByID("pwdPassword").focus();
				}else {

					if(validatePassword(trim(getText("pwdPassword"))) == false && getValue("hdnAction") != "EDIT") {			
						showPageERRMessage(arrError["cmbinvalid"]) ;
						getFieldByID("pwdPassword").focus();
					}else {
						if(isEmpty(getText("txtFirstName"))) {	
							showPageERRMessage(arrError["userDisplayNameRqrd"]) ;										
							getFieldByID("txtFirstName").focus();
						}else {
							if(!isAlphaNumericWhiteSpaceUnderscoreBracket(getText("txtFirstName"))) {			
								showPageERRMessage(arrError["userDisplayNameInvalid"]) ;										
								getFieldByID("txtFirstName").focus();
							}else {
								if(isEmpty(getText("txtEmail"))) {						
									showPageERRMessage(arrError["userEmailRqrd"]) ;
									getFieldByID("txtEmail").focus();
								}else { 
									if(!checkEmail(getText("txtEmail"))) {						
										showPageERRMessage(arrError["useremailFormat"]) ;
										getFieldByID("txtEmail").focus();
									}else {
										if((getText("selTravelAgent")=="SELECT") || (getText("selTravelAgent")=="Select")) {						
											showPageERRMessage(arrError["userAgentRqrd"]) ;
											getFieldByID("selTravelAgent").focus();
										}else {
											if(!isSelectCarriers()){
												showPageERRMessage(arrError["carierNull"]) ;
												if(!getFieldByID("selCarrierCode").disabled) {
													getFieldByID("selCarrierCode").focus();
												}
											}else {
												if(getFieldByID("selDefCarrier") && isEmpty(getText("selDefCarrier"))) {
													showPageERRMessage(arrError["defcarierNull"]) ;
													if(!getFieldByID("selDefCarrier").disabled) {
														getFieldByID("selDefCarrier").focus();
													}
													
												} else {
													if(!isFromList()) {
														showPageERRMessage(arrError["assignNotmatch"]) ;
														if(!getFieldByID("selDefCarrier").disabled) {
															getFieldByID("selDefCarrier").focus();
														}											
													}else {
														if(getValue("hdnAction") != "EDIT" && !changeVerifyPassword()) {
															return false;
														}else {															
															if(isServCh && ((getText("selServiceChannel")=="SELECT") || (getText("selServiceChannel")=="Select"))) {						
																showPageERRMessage(arrError["userServiceChRqrd"]) ;
																getFieldByID("selServiceChannel").focus();
															} else {
																	
																	if(document.getElementById('spn1'))	{		
																		if(trim(ls.getselectedData()) == ""){
																			if(!confirm("You are going to save user without any assigned roles. Are you sure?")) {
																			 return;
																			}
																		}
																		
																		setField("hdnRoleValues",trim(ls.getselectedData()));
																	}
																	
																	setField("txtLoginId",trim(getText("txtLoginId")));	
																	
																	if (passwordChanged) {														
																		setField("pwdPassword",trim(getText("pwdPassword")));
																	} else {
																		if (getText("hdnAction") == "EDIT") {															
																			setField("hdnPassword",trim(getText("pwdPassword")));
																			setField("pwdPassword","");
																		}
																	}
																	var tempName = trim(getText("txtFirstName"));
																	tempName=replaceall(tempName,"," , "_");
																	setField("txtFirstName",tempName);
																	setField("txtEmail",trim(getText("txtEmail")));	
																	setField("hdnEditable","Y");										
																	enableControls();
																	Disable("selCarrierCode", "");
																	Disable("selDefCarrier", "");
																	setField("hdnRecNo",initialrec);
																	setField("hdnSearchData",searchData);
																	setField("hdnGridData",strGridRow);	
																	setField("hdnDryUserEnable", strDryUserEnable);
																	setField("maxAdultAllowed" , trim(getText("maxAdultAllowed")));
																	document.frmUser.submit();
																	ShowProgress();																
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

function isFromList() {
	if(getFieldByID("selCarrierCode")){
		var intCount = getFieldByID("selCarrierCode").length ;
		var selDefcarrier = getText("selDefCarrier");
		 for (var i = 0; i < intCount; i++){
		   	  if(getFieldByID("selCarrierCode").options[i].selected){
		   	  	if(getFieldByID("selCarrierCode").options[i].value == selDefcarrier) {
		   	  		return true;
		   	  	}
		   	  }
		}
	}else {
		return true;
	}
	
	return false;

}

/*
 * Function to identify PASSWORD change
 */
function changePassword(){
	passwordChanged=true;
	clickChange();		
}

function changeVerifyPassword(){
	if(trim(getText("pwdPassword")) != trim(getText("pwdVeriPassword"))){
		showPageERRMessage(arrError["userVerifypass"]) ;										
		getFieldByID("pwdPassword").focus();
		return false;
	}
	return true;
}

/*
 * Function call for Click on UI controls
 */
function clickChange(){
	objOnFocus();
	setPageEdited(true);	
}

/*
 * Function to set PAGE status
 */
function setPageEdited(isEdited){
	objOnFocus();
	blnEdited = isEdited;
}

/*
 * Function to Disable Inputt Controls
 */
function disableInputControls() {	
	Disable("txtLoginId",true);	
	Disable("pwdPassword",true);
	Disable("pwdVeriPassword",true);
	Disable("txtFirstName",true);
	Disable("txtEmail",true);
	Disable("selTravelAgent",true);	
	Disable("selAirport", true);
	Disable("btnLoadAgents", true);
	if(isServCh){
		Disable("selServiceChannel",true);
	}
	if(setMyIDCarrier){
		Disable("txtMyIDCarrier",true);
	}
	Disable("btnEdit",true);
	Disable("btnDelete",true);
	Disable("btnSetPass",true);
	Disable("btnManageFFP",true);
	Disable("chkStatus",true);
	Disable("selDefCarrier",true);
	Disable("chkAdmin",true);
	disableAdminInputs(true);
	if(document.getElementById('spn1'))
	ls.disable(true);
	Disable("btnUnAdd",true);
	Disable("btnUnView",true);
	Disable("selTheme",true);
	Disable("txtFFPNumber",true);
	Disable("chkRoleChange",true);
	Disable("maxAdultAllowed",true);
}

/*
 * Function call for RESET Button Click
 */
function resetClick(){
	var strMode = getText("hdnAction");
	setPasswordEncrypt();
	objOnFocus();
	if(strMode=="ADD"){
		clearControls();
		getFieldByID("txtLoginId").focus();	
		getFieldByID("chkStatus").checked = true;
		setField("hdnEditable","Y");
		setVisible("dvAdmin", false);
	}
	if(strMode=="CHANGEFFP"){
		if (strGridRow<userData.length && userData[strGridRow][1] == getText("txtLoginId")) {
			setField('txtFFPNumber', userData[selectedUserIndex][22]);
			getFieldByID("txtFFPNumber").focus();
		}
	}

	if(strMode=="EDIT"){
		if (strGridRow<userData.length && userData[strGridRow][1] == getText("txtLoginId")) {
			setField("hdnEditable",userData[strGridRow][14]);
			setField("txtLoginId",userData[strGridRow][1]);
			setField("pwdPassword",userData[strGridRow][11]);
			setField("pwdVeriPassword",userData[strGridRow][11]);
			setField("txtFirstName",userData[strGridRow][2]);
			setField("txtEmail",userData[strGridRow][5]);
			setField("selTravelAgent",userData[strGridRow][13]);
			setField("hdnVersion",userData[strGridRow][10]);
			setField("selDefCarrier",userData[strGridRow][17]);
			if(userData[strGridRow][7] == "Active") {
				getFieldByID("chkStatus").checked = true;
			}
			else{
				getFieldByID("chkStatus").checked = false;
			}
			getFieldByID("txtFirstName").focus();
			if(document.getElementById('spn1'))	
			ls.removeAllFromListbox("lstAssignedRoles","lstRoles",2);
			if(userData[strGridRow][12] != "") {
				if(document.getElementById('spn1'))
				ls.selectedData(userData[strGridRow][12]);
			}
			clearAdminstatus();
			displayAdminGrdData(userData[strGridRow][18]);
			displayNormalGrdData(userData[strGridRow][19]);
			if(userData[strGridRow][18] != 'N' || userData[strGridRow][19] != 'N'){
				getFieldByID("chkAdmin").checked = true;
			}else {
				getFieldByID("chkAdmin").checked = false;
			}
			if(isServCh){
				setField("selServiceChannel",userData[strGridRow][20]);
			}
			
			if(setMyIDCarrier){
				setField("txtMyIDCarrier",userData[strGridRow][23]);
			}
			setField('selTheme',userData[strGridRow][21]);
			setField('txtFFPNumber',userData[strGridRow][22]);
			adminClick();
		} else { 
			//Grid row not found.  Clear it off.
			clearUser();				
			disableInputControls();	
			Disable("btnSave", "false");
			Disable("btnReset","false");
			setField("hdnEditable","Y");
			//buttonSetFocus("btnAdd");
		}
	}
	setPageEdited(false);
	passwordChanged=false;
}

/*
 * Function to Enable INPUT Controls
 */
function enableControls() {
	Disable("txtLoginId","");			
	if (getText("hdnEditable") == "Y") {
		Disable("txtFirstName",false);	
		Disable("pwdPassword", false);
		Disable("pwdVeriPassword", false);
		Disable("txtEmail",false);
		Disable("chkStatus", false);
		Disable("selTravelAgent",false);
		Disable("selAirport", false);
		Disable("btnLoadAgents", false);
		if(isServCh && isServChEditable){
			Disable("selServiceChannel",false);
		}
		if(setMyIDCarrier){
			Disable("txtMyIDCarrier",false);
		}
		Disable("selCarrierCode",false);
		Disable("selDefCarrier",false);
		Disable("chkAdmin",false);				
		if(document.getElementById('spn1'))
		ls.disable(false);
		disableAdminInputs(false);
		Disable("selTheme",false);
		Disable("maxAdultAllowed",false);
	} else {
		Disable("txtFirstName",true);	
		Disable("pwdPassword",true);
		Disable("pwdVeriPassword",true);
		Disable("txtEmail",true);
		Disable("chkStatus",true);
		Disable("selTravelAgent",true);
		Disable("selAirport", true);
		Disable("btnLoadAgents", true);
		if(isServCh){
			Disable("selServiceChannel",true);
		}
		if(setMyIDCarrier){
			Disable("txtMyIDCarrier",true);
		}
		if(document.getElementById('spn1'))
		ls.disable(true);
		Disable("selCarrierCode",true);
		Disable("selDefCarrier",true);
		Disable("chkAdmin",true);
		disableAdminInputs(true);	
		Disable("maxAdultAllowed",true);
	}
	if(noOfCarriers == "1") {
		Disable("selCarrierCode", true);
		Disable("selDefCarrier", true);	
	}
}

/*
 * Function to Clear the INPUT Fields
 */
function clearControls() {
	objOnFocus();
	setField("hdnEditable","Y");
	setField("txtLoginId","");
	setField("pwdPassword","");
	setField("pwdVeriPassword","");
	setField("txtFirstName","");
	setField("txtEmail","");
	if(isServCh && isServChEditable){
		setField("selServiceChannel","SELECT");
	}
		
	if(setMyIDCarrier){
		setField("txtMyIDCarrier","");
	}
	setField("selTravelAgent","SELECT");
	getFieldByID("chkStatus").checked = false;
	getFieldByID("chkAdmin").checked = false;
	clearAdminstatus();
	if(document.getElementById('spn1'))
	ls.removeAllFromListbox("lstAssignedRoles","lstRoles",2);
	deselectCarriers();
	setField("selDefCarrier","");
	if(noOfCarriers == "1" && getFieldByID("selCarrierCode")) {		
		getFieldByID("selCarrierCode").options[0].selected =true;
		var carr = getFieldByID("selCarrierCode").options[0].value;			
		setField("selDefCarrier",carr);
	}
	setField("selTheme","default");
	setField("txtFFPNumber","");
	setField("maxAdultAllowed","");
}

/*
 * Function to Reset the page messages
 */
function objOnFocus(){
	resetPageERRMessage();
}

/*
 * Function to Handle Page Navigation
 */
function gridNavigations(intRecNo, intErrMsg){
	if (checkPageEdited(blnEdited)) {
		objOnFocus();
		if (intRecNo<=0)
			intRecNo=1;	
		setField("hdnRecNo",intRecNo);
		setField("hdnSearchData",searchData);
		setField("hdnSearchAirline",getValue("selAirlineCode"));
		if (intErrMsg != ""){		
			showPageERRMessage(intErrMsg);
		}else{
		document.forms[0].submit();		
		ShowProgress();
		}
	}
}

/*
 * Function to Validate String Inputs
 */
function validateString(field) {
	objOnFocus();
	var strStringVal = getText(field);
	var strLen = strStringVal.length;
	var blnVal = isAlphaNumericUnderscore(strStringVal);
	if(!blnVal){
		setField(field,strStringVal.substr(0,strLen-1)); 
		getFieldByID(field).focus();
	}
}

/*
 * Function to validate alphanumberics
 */
function validateAlphaNumeric(objTextBox){
	objOnFocus();
	setPageEdited(true);
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isAlphaNumeric(strText);
	if(!blnVal){
		objTextBox.value =  strText.substr(0,strLen-1); 
		objTextBox.focus();
	}
}

/*
 * Function to Valide String Inputs with space
 */
function validateStringWhiteSpace(field) {
	objOnFocus();
	var strStringVal = getText(field);
	var strLen = strStringVal.length;
	var blnVal = isAlphaNumericWhiteSpaceUnderscore(strStringVal);
	if(!blnVal){
		setField(field,strStringVal.substr(0,strLen-1)); 
		getFieldByID(field).focus();
	}
}

function kpAlphaNumericWhiteSpace(control) {
	setPageEdited(true);
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlphaNumericWhiteSpace(strCC);
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}

/*
 * Function call for CLOSE Button Click
 */
function closeClick(){
	var strUrl = getNonSecureURL();
	if(blnEdited) {
		var cnf =  confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			blnEdited = false;
			document.forms[0].hdnMode.value = "CLOSE";
			getFieldByID("frmUser").target = "_self";
			window.location.replace(strUrl + 'showUserPageClose');
		}	
	}else { 
		blnEdited = false;
		document.forms[0].hdnMode.value = "CLOSE";
		getFieldByID("frmUser").target = "_self";		
		window.location.replace(strUrl + 'showUserPageClose');
		
	}
}

/*
 * Function to Validate the Entered password
 */
function validatePassword(pass) {
	var  charArray = new Array();
	var  strChar = "";
	var numCount = 0;
	for(var i= 0;i < pass.length; i++) {
		strChar = pass.substr(i,1); 
		if(isPositiveInt(strChar)){
			numCount++;
		}	
	}
	if((numCount < 3) || (numCount > (pass.length - 3))){
		return false;
	} 
}

function enableFFP() {
    if (selectedUserIndex == -1) {
        return;
    }
    Disable("txtFFPNumber", false);
    getFieldByID("txtFFPNumber").focus();
    Disable("pwdPassword", true);
    Disable("pwdVeriPassword", true);
    Disable("btnSave", false);
    Disable("btnReset", false);
    Disable("btnDelete", true);
    Disable("btnEdit", true);
    setField("hdnMode", "EDIT");
    setField("hdnAction", "CHANGEFFP");
    Disable("btnUnAdd", true);
    Disable("btnUnView", true);
    $("#tabs").tabs("option", "active", 0);
}


function resetPassword(){
	objOnFocus();
	setField("pwdPassword","");
	setField("pwdVeriPassword","");		
	Disable("pwdPassword", false);
	Disable("pwdVeriPassword", false);
	getFieldByID("pwdPassword").focus();	
	Disable("btnSave", false);	
	Disable("btnReset",false);	
	Disable("btnDelete",true);
	Disable("btnEdit",true);
	setField("hdnMode","EDIT");
	setField("hdnAction","CHANGEPASS");	
	Disable("btnUnAdd",true);
	Disable("btnUnView",true);
	$( "#tabs" ).tabs( "option", "active", 0 );
}

function saveFFP() {
    var ffpNumberValue = getText("txtFFPNumber");
    if (ffpNumberValue == userData[selectedUserIndex][22]) {
        showPageERRMessage(arrError["ffpnochanges"]);
        getFieldByID("txtFFPNumber").focus();
        return;
    }
    if (ffpNumberValue) {
        ffpNumberValue = "";
    }
    setField("hdnMode", "EDIT_FFP");
    Disable("txtLoginId", false);
    setField("hdnRecNo", initialrec);
    setField("hdnSearchData", searchData);
    document.frmUser.submit();
    ShowProgress();
}

function resetSave(){
	objOnFocus();
	if(isEmpty(getText("pwdPassword"))) {			
		showPageERRMessage(arrError["userPasswordRqrd"]) ;
		getFieldByID("pwdPassword").focus();
		return false;
	}		
	if(getText("pwdPassword").length < 8 ) {
		showPageERRMessage(arrError["userPasswordlength"]) ;
		getFieldByID("pwdPassword").focus();
		return false;			
	}
	if(getText("pwdPassword").length > 12 ) {
		showPageERRMessage(arrError["userMaxPasswordlength"]) ;
		getFieldByID("pwdPassword").focus();
		return false;			
	}
	if(validatePassword(trim(getText("pwdPassword"))) == false) {
		showPageERRMessage(arrError["cmbinvalid"]) ;
		getFieldByID("pwdPassword").focus();
		return false;
	}
	if(!changeVerifyPassword()) {
		return false;
	}
	var strConfirm = confirm(arrError["resetConfirm"]);	
	if (strConfirm == true) {
		setField("hdnMode","RESET");
		Disable("txtLoginId",false);
		setField("hdnRecNo",initialrec);
		setField("hdnSearchData",searchData);		
		document.frmUser.submit();
		ShowProgress();		
	}else {
		return false;
	}
	
}

/*
 * Function to Check Page status
 */	
function checkPageEdited(blnEdit) {
	if(blnEdit) {
		var strConf = confirm("Changes will be lost.Do you wish to Continue?");
		return strConf;
	}
	return true;		
}

/*
 * Function to set Confirmation Messages
 */	
function showPageConfMessage(strMsg){
	if(request['protocolStatus'] != "true") {	
		DivWrite("spnError", "<div class='errorBoxHttps'><img src='../images/Conf_no_cache.gif' align='absmiddle'> " + strMsg + '<\/div>');
	}else {
		showCommonError("CONFIRMATION",strMsg);
	}
}

function showDetails() {
	var message = "<font> <br><b>Note : Password must be</b><br>1. Between 8 and 12 alphanumaric charaters<br>2. Must contain at least 3 letters and 3 digits e.g. ABC3y50N, 0123XYZ4<br>3. Is case sensitive i.e. A is not the same as a</font>";
	return	overlib(message, BACKGROUND, '', CENTER, BELOW, WIDTH, 250, BORDER, 2);
}

function displayAdminGrdData(strVal){
	if(strVal == "A"){
		if(getFieldByID("chkAUAny"))
			getFieldByID("chkAUAny").checked = true;		
	}else if(strVal == "B"){
		if(getFieldByID("chkAUOwner"))
		getFieldByID("chkAUOwner").checked = true;
		if(getFieldByID("chkAUReporting"))
		getFieldByID("chkAUReporting").checked = true;
	}else if(strVal == "R"){
		if(getFieldByID("chkAUReporting"))
		getFieldByID("chkAUReporting").checked = true;
	}else if(strVal == "O"){
		if(getFieldByID("chkAUOwner"))
		getFieldByID("chkAUOwner").checked = true;
	}
	
}

function displayNormalGrdData(strVal){
	if(strVal == "A"){
		if(getFieldByID("chkOUAny"))
		getFieldByID("chkOUAny").checked = true;
	}else if(strVal == "B"){
		if(getFieldByID("chkOUOwner"))
		getFieldByID("chkOUOwner").checked = true;
		if(getFieldByID("chkOUReporting"))
		getFieldByID("chkOUReporting").checked = true;
	}else if(strVal == "R"){
		if(getFieldByID("chkOUReporting"))
		getFieldByID("chkOUReporting").checked = true;
	}else if(strVal == "O"){
		if(getFieldByID("chkOUOwner"))
		getFieldByID("chkOUOwner").checked = true;
	}
}

function showAdmin(swVal){	
	setVisible("dvAdnTXT", true);
	switch (swVal) {
		case 1:
			setVisible("dvAdmAny", true);
			setVisible("dvAdmOwner", true);
			setVisible("dvAdmARep", true);
			Disable("chkAUAny", false);	
			Disable("chkAUOwner", false);	
			Disable("chkAUReporting", false);			
			break;
		case 2:			
			setVisible("dvAdmOwner", true);
			setVisible("dvAdmARep", true);
			Disable("chkAUOwner", false);	
			Disable("chkAUReporting", false);			
			break;
		case 3:			
			setVisible("dvAdmARep", true);
			Disable("chkAUReporting", false);			
			break;
		case 4:			
			setVisible("dvAdmOwner", true);
			Disable("chkAUOwner", false);				
			break;
		default:
			setVisible("dvAdnTXT", false);			
			setVisible("dvAdmAny", false);
			setVisible("dvAdmOwner", false);
			setVisible("dvAdmARep", false);	
			Disable("chkAUAny", true);	
			Disable("chkAUOwner", true);	
			Disable("chkAUReporting", true);		
			break;
	}
}

function showNormal(swVal){	
	setVisible("dvNormTXT", true);
	switch (swVal) {
		case 1:
			setVisible("dvNormAny", true);
			setVisible("dvNormOwner", true);
			setVisible("dvNormRep", true);
			Disable("chkOUAny", false);	
			Disable("chkOUOwner", false);
			Disable("chkOUReporting", false);
			break;
		case 2:			
			setVisible("dvNormOwner", true);
			setVisible("dvNormRep", true);	
			Disable("chkOUOwner", false);
			Disable("chkOUReporting", false);		
			break;
		case 3:			
			setVisible("dvNormRep", true);
			Disable("chkOUReporting", false);	
			break;
		case 4:	
			setVisible("dvNormOwner", true);
			Disable("chkOUOwner", false);			
			break;		
			
		default:
			setVisible("dvNormTXT", false);			
			setVisible("dvNormAny", false);
			setVisible("dvNormOwner", false);
			setVisible("dvNormRep", false);
			Disable("chkOUAny", true);	
			Disable("chkOUOwner", true);	
			Disable("chkOUReporting", true);				
			break;			
	}
}

function adminAnyClick(){
	if(getFieldByID("chkAUAny").checked){		
		getFieldByID("chkAUOwner").checked = false;
 		getFieldByID("chkAUReporting").checked = false;
 		Disable("chkAUOwner", true);
		Disable("chkAUReporting", true);
	}else {
		Disable("chkAUOwner", false);
		Disable("chkAUReporting", false);
	}
}

function NormAnyClick(){
	if(getFieldByID("chkOUAny").checked){		
		getFieldByID("chkOUOwner").checked = false;
 		getFieldByID("chkOUReporting").checked = false;
 		Disable("chkOUOwner", true);
		Disable("chkOUReporting", true);
	}else {
		Disable("chkOUOwner", false);
		Disable("chkOUReporting", false);
	}
	
}

function disableAdminInputs(stat){
	if(getFieldByID("chkAUOwner"))
		Disable("chkAUOwner", stat);
	if(getFieldByID("chkAUReporting"))
		Disable("chkAUReporting", stat);
	if(getFieldByID("chkAUAny"))
		Disable("chkAUAny", stat);
	if(getFieldByID("chkOUOwner"))
		Disable("chkOUOwner", stat);
	if(getFieldByID("chkOUReporting"))
		Disable("chkOUReporting", stat);
	if(getFieldByID("chkOUAny"))
		Disable("chkOUAny", stat);
}


 function adminClick() { 
 	adminInt = parseInt(adminInt);
 	var passAdmin = 0;
 	var passNormal = 0;
 	passAdmin = adminInt;
 	normalInt =  parseInt(normalInt);
 	passNormal = normalInt;	
 	if(getFieldByID("chkAdmin").checked && getFieldByID("selTravelAgent").value != "SELECT"){
 		var agType = getFieldByID("selTravelAgent").value;
 		var arrAgTypes = agType.split(",");
 		if((adminInt ==3)  &&  arrAgTypes.length > 1 
 				&& ((arrAgTypes[1] != 'GSA' && !enableGSAV2) 
 				|| (arrAgTypes[1] != 'GSA' && arrAgTypes[1] != 'SGSA' && arrAgTypes[1] != 'TA' && enableGSAV2))){
 			passAdmin = 4;
 		}
 		
 		if(normalInt ==3  &&  arrAgTypes.length > 1 
 				&& ((arrAgTypes[1] != 'GSA' && !enableGSAV2) 
 		 		|| (arrAgTypes[1] != 'GSA' && arrAgTypes[1] != 'SGSA' && arrAgTypes[1] != 'TA' && enableGSAV2))){
 			passNormal = 4;
 		}
 		
 		setVisible("dvAdmin", true);
 		showAdmin(0);
	 	showAdmin(passAdmin);
	 	showNormal(0);
	 	showNormal(passNormal);
	 	
	 	if((arrAgTypes[1] != 'GSA' && !enableGSAV2) 
 		 		|| (arrAgTypes[1] != 'GSA' && arrAgTypes[1] != 'SGSA' && arrAgTypes[1] != 'TA' && enableGSAV2)){
	 		if(getFieldByID("chkAUReporting")){
	 			getFieldByID("chkAUReporting").checked = false;
	 			Disable("chkAUReporting", true);
	 		}
	 		if(getFieldByID("chkOUReporting")) {
	 			getFieldByID("chkOUReporting").checked = false;
	 			Disable("chkOUReporting", true);
	 		}
	 	}
	 	if(arrAgTypes[1] != carAgent){
	 		if(getFieldByID("chkAUAny")){
	 			getFieldByID("chkAUAny").checked = false;
	 			Disable("chkAUAny", true);
	 		}
	 		if(getFieldByID("chkOUAny")) {
	 			getFieldByID("chkOUAny").checked = false;
	 			Disable("chkOUAny", true);
	 		}
	 	}	 	
 	}else {
 		clearAdminstatus();
 		setVisible("dvAdmin", false);
 		showAdmin(0);
 		showNormal(0); 		
 	}	 	
 }
 
 function clearAdminstatus(){
 	getFieldByID("chkOUAny").checked = false;
 	getFieldByID("chkOUOwner").checked = false;
 	getFieldByID("chkOUReporting").checked = false;
 	getFieldByID("chkAUAny").checked = false;
 	getFieldByID("chkAUOwner").checked = false;
 	getFieldByID("chkAUReporting").checked = false;
 }

/*
 * Function to set Error Messages
 */	
function showPageERRMessage(strMsg){
	if(request['protocolStatus'] != "true") {
		DivWrite("spnError", "<div class='errorBoxHttps'><img src='../images/Err_no_cache.gif' align='absmiddle'> " + strMsg + '<\/div>');
	}else {
		showCommonError("ERROR",strMsg);
	}
}

/*
 * Function to Reset the Error Messages
 */	
function resetPageERRMessage(){
	if(request['protocolStatus'] != "true") {
		DivWrite("spnError", "");
	}else {
		top[2].HidePageMessage();
	}
	
}

/*
 * Function to show progress Bar
 */
function ShowProgress(){
	setVisible("divLoadMsg", true);
}

/*
 * Function to Hide Progress Bar
 */	
function HideProgress(){
	setVisible("divLoadMsg", false);
}

function isAlphaNumericWhiteSpaceUnderscore(s){return RegExp("^[a-zA-Z0-9_\& \w\s]+$").test(s);}

function isAlphaNumericWhiteSpaceUnderscoreBracket(s){return RegExp("^[a-zA-Z0-9_()\& \w\s]+$").test(s);}

function isAlphaNumericUnderscore(s) { return RegExp("^[a-zA-Z0-9_]+$").test(s);} 


function getNonSecureURL(){
		if (request['contextPath'] != ""){
		if  (request['contextPath'].indexOf("http") != -1){
			strHttpPath	= request['contextPath'] + "/private/";
			return strHttpPath;
		}
	}
}

function addUserNote() {
	var intHeight = 330;
	var intWidth = 600;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showUserUNotes.action?hdnMode='ADD'";
	objWindow1 = window.open(strFilename, "CWindow1", strProp);

}

function viewUserNote() {
	var intHeight = 600;
	var intWidth = 800;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var newLoginID = getValue("txtLoginId");
	var strFilename = "showUserUNotes.action?hdnMode=VIEW&hdnUserId="+newLoginID;
	objWindow1 = window.open(strFilename, "CWindow1", strProp);
}

function positiveInt(objTextBox){
	setPageEdited(true);
	objOnFocus();
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isPositiveInt(strText);
	if(!blnVal){
		objTextBox.value =  strText.substr(0,strLen-1); 
		objTextBox.focus();
	}
}