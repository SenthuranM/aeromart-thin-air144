	if (typeof(arrData) != "object")
	{
		var arrData = new Array();
	} ;

	var objTimer = null;
	if (arrData.length > 0) {
		setVisible("spnAlert", true);
		setVisible("spnSelectAll", true);			
	} else {
		Disable("btnClearAlert", true);
		setVisible("spnAlert", true);	
	}
	//var recstNo = top[0].intLastRec;
	
	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "10%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "Alert Date" ;
	objCol1.headerText = "Alert Date";
	objCol1.itemAlign = "center";
	objCol1.sort = true;
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "10%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "Flight" ;
	objCol2.headerText = "Flight";
	objCol2.itemAlign = "left";
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "10%";
	objCol3.arrayIndex = 3;
	objCol3.toolTip = "Departure Date" ;
	objCol3.headerText = "Departure Date";
	objCol3.itemAlign = "center"

	var objCol4 = new DGColumn();
	objCol4.columnType = "link";
	objCol4.width = "11%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "PNR" ;
	objCol4.headerText = "PNR";
	objCol4.linkOnClick = "callModifyBooking"	;
	objCol4.itemAlign = "left"
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "50%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "Alert" ;
	objCol5.headerText = "Alert";
	objCol5.itemAlign = "left";
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "checkbox";
	objCol6.width = "6%";
	objCol6.arrayIndex = 6;
	objCol6.toolTip = "Select";
	objCol6.headerText = "Select";
	objCol6.itemAlign = "center";
	
	var objDG = new DataGrid("spnAlert");
	objDG.addColumn(objCol1); //Alert Date
	objDG.addColumn(objCol4); //PNR
	objDG.addColumn(objCol2); //Flight
	objDG.addColumn(objCol3); //Departure Date	
	objDG.addColumn(objCol5); //Alert
	objDG.addColumn(objCol6); //Select
	
	objDG.width = "100%";
	objDG.height = "300px";
	objDG.headerBold = false;
	objDG.arrGridData = arrData;
	objDG.seqNo = true;
	objDG.seqStartNo = recstNo;
	objDG.pgnumRecTotal = totNoOfAlerts ;
	objDG.paging = true;
	objDG.pgnumRecPage = 20;
	objDG.pgonClick = "gridNavigations"
	//alert("gridNavigation");
	//objDG.rowClick = "parent.RowClick";
	ShowProgress();
	objDG.displayGrid();
	//HideProgress();
	

	function selectAll() {
		var selectedAlerts = objDG.getSelectedColumn(5);	
		if (getFieldByID("chkSelectAll").checked) {
			for (var i = 0; i < selectedAlerts.length; i++) {
				if (arrData[i][8] != "disable") {
					objDG.setCellValue(i, 5, "true");
				} else {
					objDG.setCellValue(i, 5, "false");				
				}
			}
		} else {
			for (var i = 0; i < selectedAlerts.length; i++) {
				objDG.setCellValue(i, 5, "false"); 	
			}			
		}
	}
	
	function checkSelection() {
		var selectAllChecked = true;
		var selectedAlerts = objDG.getSelectedColumn(5);	
		for (var i = 0; i < selectedAlerts.length; i++) {
			if (selectedAlerts[i][1] == null) {
				selectAllChecked = false;
			}
		}
		getFieldByID("chkSelectAll").checked = selectAllChecked;
	}
	
	function callModifyBooking(rowId){
		var arrColData = objDG.getSelectedColumnValue("3");	
		var isInterline = "false";
		if (arrColData[rowId][1] != null) {
			objDG.selectRow(rowId);
			intSelRow = rowId;
			var objF = getFieldByID("frmAlert");			
			isInterline = arrData[rowId][9];
			airlineCode = arrData[rowId][10];
			
			setField("hdnAction", "VIEWPNR");
			setField("hdnPnr", objDG.getSelectedColumnValue("1")[intSelRow][1]);			
					
			//buildAlertInfo();
			top.strPageFrom		= objDG.getSelectedColumnValue("1")[intSelRow][1];
			top.strPageReturn	= "showAlert.action";			
			
			// added by LT
			top.blnSearchBooking = true
			top.strSearchBookingPNR = objDG.getSelectedColumnValue("1")[intSelRow][1];	
			
			if (isInterline == "false") {
				setField("pnrNO", objDG.getSelectedColumnValue("1")[intSelRow][1]);	
				setField("groupPNRNO", "");	
				objF.action = "showNewFile!loadRes.action";				
			} else{
				setField("pnrNO", "");
				setField("groupPNRNO", objDG.getSelectedColumnValue("1")[intSelRow][1]);
				setField("marketingAirlineCode", airlineCode);
				setField("airlineCode", airlineCode);
				objF.action = "showNewFile!loadRes.action";				
			}
			objF.method = "post";
			objF.submit();
		}
	 }

	function disableRow() {
		var disabledRows = 0;
		if (objDG.loaded) {
			clearInterval(objTimer);		
			for (var i = 0; i < arrData.length; i++) {
				if (arrData[i][8] == "disable") {
					//objDG.setDisable(i, 3, true);
					objDG.setDisable(i, 5, true);
					disabledRows++;
				}
			}
			if (disabledRows == arrData.length) {
				Disable("btnClearAlert", true);
				Disable("chkSelectAll", true);
			} else {
				Disable("btnClearAlert", false);
				Disable("chkSelectAll", false);
			}
		} else {
			Disable("btnClearAlert", true);		
			Disable("chkSelectAll", true);
		}
	}

	objTimer = setInterval("disableRow()", 300)
