if (typeof(arrSummaryData) != "object") {
    var arrSummaryData = new Array();
}

if (arrSummaryData.length == 0) {

    getFieldByID('gridViewNotifications').innerHTML = '';
    getFieldByID('gridViewNotifications').insertAdjacentHTML('beforeend',
        '<table border="1" >' +
        '<tr>' +
        '<td class="CalendarGridItemHead">SUN</td>' +
        '<td class="CalendarGridItemHead">MON</td>' +
        '<td class="CalendarGridItemHead">TUE</td>' +
        '<td class="CalendarGridItemHead">WED</td>' +
        '<td class="CalendarGridItemHead">THU</td>' +
        '<td class="CalendarGridItemHead">FRI</td>' +
        '<td class="CalendarGridItemHead">SAT</td>' +
        '</tr>' +
        '<tr>' +
        '<td class="CalendarGridItemHead"> </td>' +
        '<td class="CalendarGridItemHead"> </td>' +
        '<td class="CalendarGridItemHead"> </td>' +
        '<td class="CalendarGridItemHead"><font> <b>No Data Found</b></font></td></td>' +
        '<td class="CalendarGridItemHead"> </td>' +
        '<td class="CalendarGridItemHead"> </td>' +
        '<td class="CalendarGridItemHead"> </td>' +
        '</tr>' +
        '</table>');

} else {

    if (getFieldByID('txtFromDate').value == undefined || getFieldByID('txtFromDate').value == '') {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        getFieldByID("txtFromDate").value = dd + '/' + mm + '/' + yyyy;

        mm = today.getMonth() + 2; //January is 0!
        yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        } else if (mm > 12) {
            mm = '01';
            yyyy = yyyy + 1;
        }

        getFieldByID("txtToDate").value = dd + '/' + mm + '/' + yyyy;
    }

    var parts = getFieldByID('txtFromDate').value.split('/');
    var date = new Date(parts[2], parts[1] - 1, parts[0]);
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();

    var month = ["January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December"];

    var iMonth = date.getMonth();
    var monthName = month[iMonth];

    getFieldByID('gridViewNotifications').innerHTML = '';
    getFieldByID('gridViewNotifications').insertAdjacentHTML('beforeend',
        '<table border="1" >' +
        '<tr><td> ' + monthName + " " + parts[2] + ' </td></tr>' +
        '</table>');

    var rows = '<table border="1" >';
    rows = rows +
    '<tr>' +
    '<td class="CalendarGridItemHead">SUN</td>' +
    '<td class="CalendarGridItemHead">MON</td>' +
    '<td class="CalendarGridItemHead">TUE</td>' +
    '<td class="CalendarGridItemHead">WED</td>' +
    '<td class="CalendarGridItemHead">THU</td>' +
    '<td class="CalendarGridItemHead">FRI</td>' +
    '<td class="CalendarGridItemHead">SAT</td>' +
    '</tr>';
    var d = 1;
    var disable = 0;
    if (firstDay.getDay() > 0) {
        var ndate = date;
        ndate.setDate(0);
        d = ndate.getDate() - firstDay.getDay() + 1;
        iMonth = ndate.getMonth();
        if (iMonth < 10) {
            iMonth = '0' + iMonth
        }
    }
    for (var i = 0; i < 6; i++) {
        rows = rows + '<tr>';
        for (var x = 0; x < 7; x++) {
            if (((i == 0 && x == firstDay.getDay()) || d > lastDay) && d != 2) {
                d = 1;
                if(iMonth == parts[1]){
                    date.setMonth(Number(iMonth)+1)
                    iMonth = date.getMonth();
                    if (iMonth < 10) {
                        iMonth = '0' + iMonth
                    }
                } else {
                    iMonth = parts[1];
                }
                disable++;
            }
            if (arrSummaryData.length > 0) {
                var not = getNotificationRow(d, iMonth);
                if (not.length > 0) {
                    rows = rows + '<td id="tdid" onclick="showDetailView(getSelectedDate(getDate(this.innerText)))" class="CalendarGridItemRow"> ' + d++ + ' ' +
                    '<table width="100%">' +
                    '<tr><td class="CalendarGridItemInnerRow">Sent SMS       : ' + not[8] + '</td></tr>' +
                    '<tr><td class="CalendarGridItemInnerRow">Sent Email     : ' + not[11] + '</td></tr>' +
                    '<tr><td class="CalendarGridItemInnerRow">Failed SMS   : ' + not[9] + '</td></tr>' +
                    '<tr><td class="CalendarGridItemInnerRow">Failed Email : ' + not[12] + '</td></tr>' +
                    '</table> ' +
                    '</td>';
                } else {
                    rows = rows + getRow(disable);
                }
            } else {
                rows = rows + getRow(disable);
            }
        }
        rows = rows + '</tr>';
    }
    rows = rows + '</table>';

    getFieldByID('gridViewNotifications').insertAdjacentHTML('beforeend', rows);
}

function getDate(txt) {
    return txt.split(' ')[1];
}

function getRow(disable) {

    if (disable == 1 && arrSummaryData.length > 0) {
        return '<td class="CalendarGridItemRow"> ' + d++ + ' ' +
            '<table width="100%">' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '</table> ' +
            '</td>';
    } else {
        return '<td disabled="disabled" class="CalendarGridItemRowDisabled"> ' + d++ + ' ' +
            '<table width="100%">' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '<tr><td class="CalendarGridItemInnerRow"> </td></tr>' +
            '</table> ' +
            '</td>';
    }
}

function getNotificationRow(date, month) {

    if (date < 10) {
        date = '0' + date
    }

    var ret = [];
    for (var i = 0; i < arrSummaryData.length; i++) {

        var d = arrSummaryData[i][2].split("/");

        if (d == null || d == '') {
            d = arrSummaryData[i][5].split("_")[1];//G9464_29052018_CHG1
            d = [d.substring(0, 2), d.substring(2, 4), d.substring(4)];
        }

        if (d[0] == date && d[1] == month) {
            if (ret.length == 0) {
                ret = arrSummaryData[i].slice(0);
            } else {
                ret[8] = Number(ret[8]) + Number(arrSummaryData[i][8]);
                ret[11] = Number(ret[11]) + Number(arrSummaryData[i][11]);
                ret[9] = Number(ret[9]) + Number(arrSummaryData[i][9]);
                ret[12] = Number(ret[12]) + Number(arrSummaryData[i][12]);
            }
        }
    }

    return ret;
}

function getSelectedDate(day){    
    var parts = getFieldByID('txtFromDate').value.split('/');
    var date=day+"/"+parts[1]+"/"+parts[2];
    return date;
}

function showDetailView(date) {
	document.getElementById('changeView').innerHTML = "Switch to Grid View";
    document.getElementById('trCalendar').style.visibility = "hidden";
    document.getElementById('tdCalendar').style.visibility = "hidden";
    document.getElementById('gridViewNotifications').style.visibility = "hidden";
    document.getElementById('trSpnViewNotificationSummary').style.visibility = "hidden";
    document.getElementById('spnViewNotificationSummary').style.visibility = "hidden";
    document.getElementById('trSpnViewNotificationDetails').style.visibility = "hidden";
    document.getElementById('spnViewNotificationDetails').style.visibility = "hidden";
    document.getElementById('trSpnViewPaxContactsDetails').style.visibility = "hidden";
    document.getElementById('trSpnViewFlightNotificationGrid').style.visibility = "visible";
    document.getElementById('spnViewFlightNotificationGrid').style.visibility = "visible";
    document.getElementById('rtTxtMessage').style.visibility = "hidden";

    var url = "showFlightPnrViewNotification.action";
    var params = "hdnAction=VIEWMESSAGELIST&hdnDepDate=" + date;
    setField("hdnCalendarLoad","true");
    makePOST(url, params);

}