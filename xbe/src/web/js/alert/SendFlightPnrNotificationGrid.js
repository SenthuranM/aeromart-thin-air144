	if (typeof(arrData) != "object")
	{
		var arrData = new Array();
	} ;

	var arrSmsPnrList = new Array();
	var arrEmailPnrList = new Array();
	var arrSentNotices = new Array();
	
	var objTimer = null;

	var recstNo = top[0].intLastRec;
	
	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "6%";
	objCol1.arrayIndex = 3;
	objCol1.toolTip = "PNR * Denotes PNR with an Alert" ;
	objCol1.headerText = "PNR Number";
	objCol1.itemAlign = "left";
	objCol1.linkOnClick = "callModifyBooking"	
	objCol1.sort = true;
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "7%";
	objCol2.arrayIndex = 4;
	objCol2.toolTip = "Route" ;
	objCol2.headerText = "Route";
	objCol2.itemAlign = "center";
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "10%";
	objCol3.arrayIndex = 5;
	objCol3.toolTip = "Mobile" ;
	objCol3.headerText = "Mobile";
	objCol3.itemAlign = "center"

	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "14%";
	objCol4.arrayIndex = 6;
	objCol4.toolTip = "Email Address" ;
	objCol4.headerText = "Email Address";
	objCol4.itemAlign = "center"
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "10%";
	objCol5.arrayIndex = 7;
	objCol5.toolTip = "Landphone" ;
	objCol5.headerText = "Landphone";
	objCol5.itemAlign = "left";
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "checkbox";
	objCol6.width = "3%";
	objCol6.arrayIndex = 12;
	objCol6.toolTip = "Select";
	objCol6.headerText = "Select";
	objCol6.itemAlign = "center";
	objCol6.linkOnClick = "checkSelectAll";

	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "3%";
	objCol7.arrayIndex = 12;
	objCol7.toolTip = "Adults";
	objCol7.headerText = "Ad";
	objCol7.itemAlign = "right";
	
	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "3%";
	objCol8.arrayIndex = 13;
	objCol8.toolTip = "Children";
	objCol8.headerText = "Ch";
	objCol8.itemAlign = "right";
	
	var objCol9 = new DGColumn();
	objCol9.columnType = "label";
	objCol9.width = "3%";
	objCol9.arrayIndex = 14;
	objCol9.toolTip = "Infants";
	objCol9.headerText = "In";
	objCol9.itemAlign = "right";
	
	var objCol10 = new DGColumn();
	objCol10.columnType = "label";
	objCol10.width = "7%";
	objCol10.arrayIndex = 15;
	objCol10.toolTip = "Alert Created Date";
	objCol10.headerText = "Alert Date";
	objCol10.itemAlign = "right";
	

	var objCol11 = new DGColumn();
	objCol11.columnType = "label";
	objCol11.width = "14%";
	objCol11.arrayIndex = 16;
	objCol11.toolTip = "InBound Connection Details";
	objCol11.headerText = "Inbound Connection";
	objCol11.itemAlign = "right";
	
	var objCol12 = new DGColumn();
	objCol12.columnType = "label";
	objCol12.width = "15%";
	objCol12.arrayIndex = 17;
	objCol12.toolTip = "OutBound Connection Details";
	objCol12.headerText = "Outbound Connection";
	objCol12.itemAlign = "right";
	
	var objCol13 = new DGColumn();
	objCol13.columnType = "label";
	objCol13.width = "16%";
	objCol13.arrayIndex = 18;
	objCol13.toolTip = "Phone Numbers";
	objCol13.headerText = "Mobile/Landline";
	objCol13.itemAlign = "right";
	
	var objCol14 = new DGColumn();
	objCol14.columnType = "label";
	objCol14.width = "6%";
	objCol14.arrayIndex = 19;
	objCol14.toolTip = "Flight Number";
	objCol14.headerText = "Flight Number";
	objCol14.itemAlign = "right";
	
	var objCol15 = new DGColumn();
	objCol15.columnType = "label";
	objCol15.width = "6%";
	objCol15.arrayIndex = 20;
	objCol15.toolTip = "Flight Dates";
	objCol15.headerText = "Flight Dates";
	objCol15.itemAlign = "right";
			
	var objDG = new DataGrid("spnPNR");
	objDG.addColumn(objCol6); //Select
	objDG.addColumn(objCol1); //PNR
	//objDG.addColumn(objCol2); //Route
	objDG.addColumn(objCol13); //Mobile/LandLine No
	//objDG.addColumn(objCol3); //Mobile
	objDG.addColumn(objCol4); //Email	
	//objDG.addColumn(objCol5); //Landphone	
	objDG.addColumn(objCol7); //Number of Adults
	objDG.addColumn(objCol8); //Number of Children
	objDG.addColumn(objCol9); //Number of Infants
	objDG.addColumn(objCol10); //Alert Created Date
	objDG.addColumn(objCol11); //InBound Connection Details
	objDG.addColumn(objCol12); //OutBound Connection Details
	objDG.addColumn(objCol15); //Flight Dates
	objDG.addColumn(objCol14); //Flight Number
	
	objDG.width = "158%";
	objDG.height = "120px";
	objDG.headerBold = false;
	objDG.arrGridData = arrData;
	objDG.seqNo = true;
	objDG.seqStartNo = recstNo;
	//objDG.pgnumRecTotal = totNoOfAlerts ;
	objDG.paging = true;
	objDG.pgnumRecPage = 20;
	objDG.pgonClick = "gridNavigations"
	//alert("gridNavigation");
	objDG.rowClick = "editData";
	objDG.rowSelect = true;
	//ShowProgress();
	objDG.displayGrid();
	//HideProgress();
	 	

	function selectAll() {
		var selectedPNRs = objDG.getSelectedColumn("0");
		if (getFieldByID("chkSelectAll").checked) {
			for (var i = 0; i < selectedPNRs.length; i++) {
					objDG.setCellValue(i, 0, "true");
			}
		} else {
			for (var i = 0; i < selectedPNRs.length; i++) {
				objDG.setCellValue(i, 0, "false"); 	
			}			
		}
	}
	
		
	function addToSmsPnrList(pnr)
	{
		if(arrSmsPnrList != null)
		{
			var found = false;
			for(var i = 0; i < arrSmsPnrList.length; i++)
				if(arrSmsPnrList[i] == pnr) {
					found = true;
					break;
			}
			if(found == false)
				arrSmsPnrList[arrSmsPnrList.length] = pnr;
		}
	}
	
	function addToEmailPnrList(pnr)
	{
		if(arrEmailPnrList != null)
		{
			var found = false;
			for(var i = 0; i < arrEmailPnrList.length; i++)
				if(arrEmailPnrList[i] == pnr) {
					found = true;
					break;
			}
			if(found == false)
				arrEmailPnrList[arrEmailPnrList.length] = pnr;
		}
	}

	function checkSelection() {
		var selectAllChecked = true;
		var selectedPNRs = objDG.getSelectedColumn(5);	
		for (var i = 0; i < selectedPNRs.length; i++) {
			if (selectedPNRs[i][0] == null) {
				selectAllChecked = false;
			}
		}
		for (var i = 0; i < objDG.arrGridData.length; i++) {
			if(objDG.getCellValue(i,0) == false)
				selectAllChecked = false;
		}
		
		getFieldByID("chkSelectAll").checked = selectAllChecked;
	}
	
	
	function setSelectedRows() {
		
		var nChannel = getValue("selNotifyChannel");
		arrSmsPnrList = new Array();
		arrEmailPnrList = new Array();
		switch(nChannel) {
		case "1":
			for (var i = 0; i < objDG.arrGridData.length; i++) {
				if (objDG.getCellValue(i,0) == true){
				if (objDG.arrGridData[i][5] != null && objDG.arrGridData[i][5].toString().replace("-","") != "")
					addToSmsPnrList(objDG.arrGridData[i][9]);
				}
			}
			break;
		case "2":
			for (var i = 0; i < objDG.arrGridData.length; i++) {
				if (objDG.getCellValue(i,0) == true){
				if (objDG.arrGridData[i][6] != null && objDG.arrGridData[i][6] != "")
					addToEmailPnrList(objDG.arrGridData[i][9]);
				}
			}
			 break;
		case "3":
			for (var i = 0; i < objDG.arrGridData.length; i++) {
				if (objDG.getCellValue(i,0) == true){
				if (objDG.arrGridData[i][5] != null && objDG.arrGridData[i][5].toString().replace("-","") != ""
					&& objDG.arrGridData[i][6] != null && objDG.arrGridData[i][6] != "") {
					addToSmsPnrList(objDG.arrGridData[i][9]);
					addToEmailPnrList(objDG.arrGridData[i][9]);
				}
				}
			}
			break;
		}
		
			
	}
	
	function selectPNRs() {
		
		var nChannel = getValue("selNotifyChannel");
				
		var selNCode = getValue("selExistingCodes");
		var sentSms = false;
		var sentEmail = false;
		setEmailVisibilty();
		switch(nChannel) {
		case "1":
			
			for (var i = 0; i < objDG.arrGridData.length; i++) {
				arrSentNotices = objDG.arrGridData[i][1];
				for (var k = 0; k < arrSentNotices.length; k++){
						if(selNCode != -1 && selNCode == arrSentNotices[k][1]) {
							if(arrSentNotices[k][4] == "SMS") {
								sentSms = true;
							}
						}
				}
					
				if (objDG.arrGridData[i][5] != null && objDG.arrGridData[i][5].toString().replace(/-/g,"") != "" && !sentSms)
				{
					objDG.setCellValue(i, 0, "true");
					
				}
				else
					objDG.setCellValue(i, 0, "false");

				sentSms = false;
			}
			break;
		case "2":
			for (var i = 0; i < objDG.arrGridData.length; i++) {
				
				arrSentNotices = objDG.arrGridData[i][1];
				for (var k = 0; k < arrSentNotices.length; k++){
						if(selNCode != -1 && selNCode == arrSentNotices[k][1]) {
							if(arrSentNotices[k][2] == "EMAIL") {
								sentEmail = true;
							break;
							}
						}
				}
				
				if (objDG.arrGridData[i][6] != null && objDG.arrGridData[i][6] != "" && !sentEmail)
				{
					objDG.setCellValue(i, 0, "true");
					
				}
				else
					objDG.setCellValue(i, 0, "false");
				sentEmail = false;
			}
			 break;
		case "3":
			for (var i = 0; i < objDG.arrGridData.length; i++) {
				
				arrSentNotices = objDG.arrGridData[i][1];
				for (var k = 0; k < arrSentNotices.length; k++){
						if(selNCode != -1 && selNCode == arrSentNotices[k][1]) {
							if(arrSentNotices[k][2] == "SMS") {
								sentSms = true;
							break;
							}
						}
				}
				for (var k = 0; k < arrSentNotices.length; k++){
						if(selNCode != -1 && selNCode == arrSentNotices[k][1]) {
							if(arrSentNotices[k][2] == "EMAIL") {
								sentEmail = true;
							break;
							}
						}
				}
				
				if (objDG.arrGridData[i][5] != null && objDG.arrGridData[i][5].toString().replace(/-/g,"") != ""
					&& objDG.arrGridData[i][6] != null && objDG.arrGridData[i][6] != "" && !sentSms && !sentEmail) {
					objDG.setCellValue(i, 0, "true");
					
				}
				else
					objDG.setCellValue(i, 0, "false");
				
				sentSms = false;
				sentEmail = false;
			}
			break;
		default:
				for (var i = 0; i < objDG.arrGridData.length; i++) {
					objDG.setCellValue(i, 0, "false");
				}
			break;
		}
		checkSelection();
			
	}


	
	function setHiddenParameters()
	{
		//setField("hdnFlightId", arrData[0][8]);
		//setField("hdnDepDate", arrData[0][2]);
//		setField("hdnDepDate", selectedFlightData[1]);
		setField("hdnSmsPnrSegList", arrSmsPnrList);
		setField("hdnEmailPnrSegList", arrEmailPnrList);
		
	}
	
	function callModifyBooking(rowId){
		var arrColData = objDG.getSelectedColumnValue("0");
		alert("arrColData-- "+ arrColData);
		if (arrColData[rowId][0] != null) {
			objDG.selectRow(rowId);
			intSelRow = rowId;
			setField("hdnAction", "VIEWPNR");
			setField("hdnPnr", objDG.getSelectedColumnValue("0")[intSelRow][0]);			
			setField("pnrNo", objDG.getSelectedColumnValue("0")[intSelRow][0]);			
			//buildAlertInfo();
			top.strPageFrom		= objDG.getSelectedColumnValue("0")[intSelRow][0];
			top.strPageReturn	= "showFlightPnrNotification.action";			
			
			// added by LT
			top.blnSearchBooking = true
			top.strSearchBookingPNR = objDG.getSelectedColumnValue("0")[intSelRow][0];
			var objF = getFieldByID("frmNotify");
			objF.action = ""; // TODO : need to call the new action. 
			// i.e. showNewFile!loadRes.action. Please set the correct parameters 
			objF.method = "post";
			objF.submit();
					
		}
	 }
	
	function editData(strRowNo){
	    strRowData = objDG.arrGridData[strRowNo];
	    var intWidth = 500;
	    var intHeight = 300;
	    var intLeft = (window.screen.width - 1000) / 2;
	    var intTop = (window.screen.height - 710) / 2;
	    var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + intTop + ',left=' + intLeft;
	    top.objCWindow = window.open("../../private/showFile!contactDetails.action", "updateContactDetails", strProp);

	    top.objCWindow.onload = function () {
	        top.objCWindow.document.getElementById('txtpnr').value = strRowData[3].split("<")[0];
	        var mobile = strRowData[5].split("-");
	        var land = strRowData[7].split("-");
	        if (mobile.length>0) top.objCWindow.document.getElementById('txtMobile1').value = mobile[0];
	        if (mobile.length>1) top.objCWindow.document.getElementById('txtMobile2').value = mobile[1];
	        if (mobile.length>2) top.objCWindow.document.getElementById('txtMobile3').value = mobile[2];
	        top.objCWindow.document.getElementById('txtEmail').value = strRowData[6];
	        if (land.length>0) top.objCWindow.document.getElementById('txtLand1').value = land[0];
	        if (land.length>1) top.objCWindow.document.getElementById('txtLand2').value = land[1];
	        if (land.length>2) top.objCWindow.document.getElementById('txtLand3').value = land[2];

	    };
	}
	
	function sendUpdateContactData() {
		if (valiadatePhoneNumber()){
			var url = "manageFlightPnrNotification.action";
			var mobile = "";
			var land = "";
			if (top.objCWindow.document.getElementById('txtMobile1').value != ""){
				 mobile =  top.objCWindow.document.getElementById('txtMobile1').value +"-" +
					top.objCWindow.document.getElementById('txtMobile2').value +"-" +
					top.objCWindow.document.getElementById('txtMobile3').value;
			}
			if (top.objCWindow.document.getElementById('txtLand1').value != ""){
				land = top.objCWindow.document.getElementById('txtLand1').value +"-" +
				top.objCWindow.document.getElementById('txtLand2').value + "-" +
				top.objCWindow.document.getElementById('txtLand3').value;
			}
		    var params = "hdnAction=UPDATE_CONTACT_DATA&pnr=" + top.objCWindow.document.getElementById('txtpnr').value
		        + "&mobile=" + mobile
		        + "&email=" + top.objCWindow.document.getElementById('txtEmail').value
		        + "&land=" + land + "";
		    console.log(mobile, land);
		    makePOST(url, params);
			setTimeout ( function (){
				top.objCWindow.close();
				var objForm  = document.getElementById("frmNotify");
				objForm.target = "_self";
				objForm.action = "manageFlightPnrNotification.action";
				objForm.async = false;
				top.isFlight = false;
				setField("hdnAction", "LOAD");
				setField("hdnLoadPnrCriteria", document.getElementById('hdnLoadPnrCriteria').value);						
				setField("hdnSelectedPnr", "");	
				setField("hdnRecNo", 1);
				setField("hdnPnrLoad","true");
				var url="manageFlightPnrNotification.action";
				var params="hdnAction=LOAD&hdnLoadPnrCriteria="+document.getElementById('hdnLoadPnrCriteria').value;
				makePOST(url,params);
			},1000);	
		} else {
			top.objCWindow.close();
			showERRMessage("Please enter correct Mobile/Land number");
		}
			
	}
	
	function checkSelectAll(rowId){
		var selectedRows=objDG.getSelectedColumn("0");
        for (var i=0; i<selectedRows.length; i++){
        	if(selectedRows[i][1]==false){
        		document.getElementById("chkSelectAll").checked = false;
        		return;
        	}
        }
        document.getElementById("chkSelectAll").checked = true;
	}
	
	function valiadatePhoneNumber(){
		if ((top.objCWindow.document.getElementById('txtMobile1').value=="" && 
				top.objCWindow.document.getElementById('txtMobile2').value=="" &&
				top.objCWindow.document.getElementById('txtMobile3').value=="")){
			return true;
		}
		else if (top.objCWindow.document.getElementById('txtMobile1').value=="" || 
				top.objCWindow.document.getElementById('txtMobile2').value=="" ||
				top.objCWindow.document.getElementById('txtMobile3').value==""){
			return false;
		}
		if ((top.objCWindow.document.getElementById('txtLand1').value=="" && 
				top.objCWindow.document.getElementById('txtLand2').value=="" &&
				top.objCWindow.document.getElementById('txtLand3').value=="")){
			return true;
		}
		else if (top.objCWindow.document.getElementById('txtLand1').value=="" || 
				top.objCWindow.document.getElementById('txtLand2').value=="" ||
				top.objCWindow.document.getElementById('txtLand3').value==""){
			return false;
		}
		return true;
	}
