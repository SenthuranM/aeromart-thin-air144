if (typeof(arrMsgListData) != "object") {
    var arrMsgListData =  [];
}
;

if (arrMsgListData.length > 0) {
    setVisible("spnViewFlightNotificationGrid", true);
} else {
	setVisible("spnViewFlightNotificationGrid", false);
}
var recstNo = top[0].intLastRec;


var objCol0 = new DGColumn();
objCol0.columnType = "checkbox";
objCol0.width = "5%";
objCol0.toolTip = "Select";
objCol0.headerText = "SL";
objCol0.itemAlign = "center";

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Flight Number" ;
objCol1.headerText = "Flight Number";
objCol1.itemAlign = "left";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "8%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "All alerts";
objCol2.headerText = "All Alerts";
objCol2.itemAlign = "right";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "8%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Created Emails";
objCol3.headerText = "Created Emails";
objCol3.itemAlign = "right";

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "8%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Created SMS";
objCol4.headerText = "Created SMS";
objCol4.itemAlign = "right";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "8%";
objCol5.arrayIndex = 2;
objCol5.toolTip = "Total";
objCol5.headerText = "Total";
objCol5.itemAlign = "right";

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "8%";
objCol6.arrayIndex = 5;
objCol6.toolTip = "Cleared Emails";
objCol6.headerText = "Cleared Emails";
objCol6.itemAlign = "right";

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "8%";
objCol7.arrayIndex = 6;
objCol7.toolTip = "Cleared SMS";
objCol7.headerText = "Cleared SMS";
objCol7.itemAlign = "right";

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "8%";
objCol8.arrayIndex = 7;
objCol8.toolTip = "Total";
objCol8.headerText = "Total";
objCol8.itemAlign = "right";

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "8%";
objCol9.arrayIndex = 8;
objCol9.toolTip = "Pending Emails";
objCol9.headerText = "Pending Emails";
objCol9.itemAlign = "right";

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "8%";
objCol10.arrayIndex = 9;
objCol10.toolTip = "Pending SMS";
objCol10.headerText = "Pending SMS";
objCol10.itemAlign = "right";

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "8%";
objCol11.arrayIndex = 10;
objCol11.toolTip = "Total";
objCol11.headerText = "Total";
objCol11.itemAlign = "right";

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "7%";
objCol12.arrayIndex = 13;
objCol12.toolTip = "Sector" ;
objCol12.headerText = "Sector";
objCol12.itemAlign = "left";


var viewFlightNotificationDG = new DataGrid("spnViewFlightNotificationGrid");
viewFlightNotificationDG.addColumn(objCol1); //Flight Number
viewFlightNotificationDG.addColumn(objCol12); //Segment
viewFlightNotificationDG.addColumn(objCol2); //All alerts
viewFlightNotificationDG.addColumn(objCol3); //Created emails
viewFlightNotificationDG.addColumn(objCol4); //Created SMS
viewFlightNotificationDG.addColumn(objCol5); //Totalv created
viewFlightNotificationDG.addColumn(objCol6); //Delivered Email
viewFlightNotificationDG.addColumn(objCol7); // Delivered SMS
viewFlightNotificationDG.addColumn(objCol8); //Total delivered
viewFlightNotificationDG.addColumn(objCol9); // Pending Emails
viewFlightNotificationDG.addColumn(objCol10); //Pending Sms
viewFlightNotificationDG.addColumn(objCol11); //Total pending

viewFlightNotificationDG.width = "100%";
viewFlightNotificationDG.height = "120px";
viewFlightNotificationDG.headerBold = false;
viewFlightNotificationDG.arrGridData = arrMsgListData;
viewFlightNotificationDG.seqNo = true;
viewFlightNotificationDG.seqStartNo = recstNo;
viewFlightNotificationDG.paging = true;
viewFlightNotificationDG.pgnumRecPage = 200;
viewFlightNotificationDG.pgonClick = "gridNavigations"

viewFlightNotificationDG.rowClick = "showFailedEmailSMSPopUP";
viewFlightNotificationDG.rowSelect = true;

viewFlightNotificationDG.displayGrid();

showFailedEmailSMSPopUP = function(strRowNo){
	var arrData = viewFlightNotificationDG.arrGridData;
	var strEmail = arrData[strRowNo][11];
	var strSms = arrData[strRowNo][12];

 	if(strEmail!="" || strSms!=""){
 		var emails= strEmail.split(",");
 		var mobiles= strSms.split(",");
 		var len = (emails.length < mobiles.length)?mobiles.length:emails.length;
 		var intWidth = 500;
 	    var intHeight = 150 + len*30;
 	    var intLeft = (window.screen.width - 1000) / 2;
 	    var intTop = (window.screen.height - 710) / 2;
 	    var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + intTop + ',left=' + intLeft;
 	    top.objCWindow = window.open("../../private/showFile!pendingEmailSMSDetails.action", "showPendingEmailSMS", strProp);

 	    top.objCWindow.onload = function () {
 	    	var dynamicHtml = "<table><tr><th width='50%'><font>Email List</font></th><td valign='top' width='10%'><th width='40%'><font>SMS List</font></th></tr>";
 	    	
 	    	for(var i = 0; i < len; i++){
 	    		dynamicHtml += "<tr><td width='50%'>" + ((i < emails.length)?"<input type='text' style='width:95%' class='uppercase' maxlength='40' value='" + emails[i] + "'disabled='disabled'/></td>":"")+
 							 	"<td valign='top' width='10%'>"+
 							 	((i < mobiles.length)?"<td width='40%'><input type='text' style='width:95%' class='uppercase' maxlength='20' value='" + mobiles[i] + "'disabled='disabled'/></td>":"") + "</tr>"
 	    	}
 	    	dynamicHtml += "</table>"
 	    	top.objCWindow.document.getElementById("FailedEmailSMSDetails").innerHTML = "";
 	    	top.objCWindow.document.getElementById("FailedEmailSMSDetails").innerHTML = dynamicHtml; 
 	    	top.objCWindow.document.getElementById('viewFailedEmailSMSPopup').style.visibility = "visible";
 	    };
 	}else{
 		alert("There is no pending email/sms");
 	}
}

closePopUp = function(){
	top.objCWindow.close();
}

