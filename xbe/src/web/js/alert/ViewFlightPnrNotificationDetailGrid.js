if (typeof(arrDetailData) != "object") {
    var arrDetailData = [];
}

var arrSmsPnrList = [];
var arrEmailPnrList = [];


if (arrDetailData.length > 0) {
    setVisible("spnViewNotificationDetails", true);
} else {
    setVisible("spnViewNotificationDetails", false);
}

var recstNo = top[0].intLastRec;


var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "8%";
objCol1.arrayIndex = 3;
objCol1.toolTip = "PNR";
objCol1.headerText = "PNR";
objCol1.itemAlign = "center";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "13%";
objCol2.arrayIndex = 23;
objCol2.toolTip = "Mobile";
objCol2.headerText = "Mobile";
objCol2.itemAlign = "left";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "13%";
objCol3.arrayIndex = 24;
objCol3.toolTip = "Landphone";
objCol3.headerText = "Landphone";
objCol3.itemAlign = "left";

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "15%";
objCol4.arrayIndex = 6;
objCol4.toolTip = "Email";
objCol4.headerText = "Email";
objCol4.itemAlign = "left";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "3%";
objCol5.arrayIndex = 13;
objCol5.toolTip = "Adults";
objCol5.headerText = "Adlt";
objCol5.itemAlign = "center";

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "3%";
objCol10.arrayIndex = 21;
objCol10.toolTip = "Children";
objCol10.headerText = "Chld";
objCol10.itemAlign = "center";

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "3%";
objCol11.arrayIndex = 22;
objCol11.toolTip = "Infants";
objCol11.headerText = "Inft";
objCol11.itemAlign = "center";

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "10%";
objCol6.arrayIndex = 11;
objCol6.toolTip = "Connection Inbound";
objCol6.headerText = "Inbound";
objCol6.itemAlign = "left";

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "10%";
objCol7.arrayIndex = 16;
objCol7.toolTip = "Connection Outbound";
objCol7.headerText = "Outbound";
objCol7.itemAlign = "left";

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "15%";
objCol8.arrayIndex = 25;
objCol8.toolTip = "Flight Dates";
objCol8.headerText = "Flight Dates";
objCol8.itemAlign = "left";

var objCol9 = new DGColumn();
objCol9.columnType = "checkbox";
objCol9.checkAll = true;
objCol9.width = "3%";
objCol9.arrayIndex = 0;
objCol9.toolTip = "Select";
objCol9.headerText = " ";
objCol9.itemAlign = "center";

var objDGDetail = new DataGrid("spnViewNotificationDetails");
objDGDetail.addColumn(objCol9); //select
objDGDetail.addColumn(objCol1); //PNR
objDGDetail.addColumn(objCol3); //Land
objDGDetail.addColumn(objCol2); //mobile
objDGDetail.addColumn(objCol5); //pax
objDGDetail.addColumn(objCol10); //pax
objDGDetail.addColumn(objCol11); //pax
objDGDetail.addColumn(objCol6); //connection inbound
objDGDetail.addColumn(objCol7); //connection outbound
objDGDetail.addColumn(objCol8); //date
objDGDetail.addColumn(objCol4); //Email

objDGDetail.width = "100%";
objDGDetail.height = "175px";
objDGDetail.headerBold = false;
objDGDetail.arrGridData = arrDetailData;
objDGDetail.seqNo = true;
objDGDetail.seqStartNo = recstNo;
objDGDetail.paging = true;
objDGDetail.pgnumRecPage = 200;
objDGDetail.pgonClick = "gridNavigations";
objDGDetail.rowClick = "editData";
objDGDetail.rowSelect = true;
//ShowProgress();
objDGDetail.displayGrid();
//HideProgress();


function editData(strRowNo){
	strRowData = objDGDetail.arrGridData[strRowNo];
    var intWidth = 500;
    var intHeight = 300;
    var intLeft = (window.screen.width - 1000) / 2;
    var intTop = (window.screen.height - 710) / 2;
    var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + intTop + ',left=' + intLeft;
    top.objCWindow = window.open("../../private/showFile!contactDetails.action", "updateContactDetails", strProp);

    top.objCWindow.onload = function () {

        top.objCWindow.document.getElementById('txtpnr').value = strRowData[3];
        var mobile = strRowData[4].split("-");
        var land = strRowData[8].split("-");
        if (mobile.length>0) top.objCWindow.document.getElementById('txtMobile1').value = mobile[0];
        if (mobile.length>1) top.objCWindow.document.getElementById('txtMobile2').value = mobile[1];
        if (mobile.length>2) top.objCWindow.document.getElementById('txtMobile3').value = mobile[2];
        top.objCWindow.document.getElementById('txtEmail').value = strRowData[6];
        if (land.length>0) top.objCWindow.document.getElementById('txtLand1').value = land[0];
        if (land.length>1) top.objCWindow.document.getElementById('txtLand2').value = land[1];
        if (land.length>2) top.objCWindow.document.getElementById('txtLand3').value = land[2];

    };
}

function sendUpdateContactData() {
	if (valiadatePhoneNumber()){
		var url = "manageFlightPnrViewNotification.action";
		var mobile = "";
		var land = "";
		if (top.objCWindow.document.getElementById('txtMobile1').value !=""){
			 mobile =  top.objCWindow.document.getElementById('txtMobile1').value +"-" +
				top.objCWindow.document.getElementById('txtMobile2').value +"-" +
				top.objCWindow.document.getElementById('txtMobile3').value;
		}
		if (top.objCWindow.document.getElementById('txtLand1').value !=""){
			land = top.objCWindow.document.getElementById('txtLand1').value +"-" +
			top.objCWindow.document.getElementById('txtLand2').value + "-" +
			top.objCWindow.document.getElementById('txtLand3').value;
		}	 
	    
	    var params = "hdnAction=UPDATE_CONTACT_DATA&pnr=" + top.objCWindow.document.getElementById('txtpnr').value
	    		+ "&mobile=" + mobile
	    		+ "&email=" + top.objCWindow.document.getElementById('txtEmail').value
	    		+ "&land=" + land + "";
	    makePOST(url, params);
	    top.objCWindow.close();
	} else {
		top.objCWindow.close();
		showERRMessage("Please enter correct Mobile/Land number");
	}   
}

function selectAll(txt, cCode, action) {
    var selectedPNRs = objDGDetail.getSelectedColumn(1);
    switch (txt) {
        case 'all':
        for (var i = 0; i < selectedPNRs.length; i++) {
            objDGDetail.setCellValue(i, 0, "true");
        }
            break;
        case 'code':
            var mob = objDGDetail.getSelectedColumn(2);
            for (var i = 0; i < mob.length; i++) {
                if (objDGDetail.arrGridData[i][4].split('-')[0] == cCode || objDGDetail.arrGridData[i][4].substring(0, cCode.length) == cCode) {
                    objDGDetail.setCellValue(i, 0, action);
                }
            }
            break;
        default :
        for (var i = 0; i < selectedPNRs.length; i++) {
            objDGDetail.setCellValue(i, 0, "false");
        }

}
}


function addToSmsPnrList(pnr)
{
    if(arrSmsPnrList != null)
    {
        var found = false;
        for(var i = 0; i < arrSmsPnrList.length; i++)
            if(arrSmsPnrList[i] == pnr) {
                found = true;
                break;
            }
        if(found == false)
            arrSmsPnrList[arrSmsPnrList.length] = pnr;
    }
}

function addToEmailPnrList(pnr)
{
    if(arrEmailPnrList != null)
    {
        var found = false;
        for(var i = 0; i < arrEmailPnrList.length; i++)
            if(arrEmailPnrList[i] == pnr) {
                found = true;
                break;
            }
        if(found == false)
            arrEmailPnrList[arrEmailPnrList.length] = pnr;
    }
}

function getSelectedRows() {

    var nChannel = getValue("selNotifyChannel");
    arrSmsPnrList = [];
    arrEmailPnrList = [];
    switch(nChannel) {
        case "1":
            for (var i = 0; i < objDGDetail.arrGridData.length; i++) {
                if (objDGDetail.getCellValue(i, 0) == true) {
                    if (objDGDetail.getCellValue(i,2) != null && objDGDetail.getCellValue(i,2).toString().replace("-","") != "")
                        addToSmsPnrList(objDGDetail.arrGridData[i][2]);
                }
            }
            break;
        case "2":
            for (var i = 0; i < objDGDetail.arrGridData.length; i++) {
                if (objDGDetail.getCellValue(i, 0) == true) {
                    if (objDGDetail.getCellValue(i,3) != null && objDGDetail.getCellValue(i,3) != "")
                        addToEmailPnrList(objDGDetail.arrGridData[i][19]);
                }
            }
            break;
        case "3":
            for (var i = 0; i < objDGDetail.arrGridData.length; i++) {
                if (objDGDetail.getCellValue(i, 0) == true) {
                    if (objDGDetail.getCellValue(i,2) != null && objDGDetail.getCellValue(i,2).toString().replace("-","") != ""
                        && objDGDetail.getCellValue(i,3) != null && objDGDetail.getCellValue(i,3) != "") {
                        addToSmsPnrList(objDGDetail.arrGridData[i][19]);
                        addToEmailPnrList(objDGDetail.arrGridData[i][19]);
                    }
                }
            }
            break;
    }
}


function resendMessages() {
    getSelectedRows();
    var mode = getMode(objDGDetail.arrGridData[0][2]);
    var url = "manageFlightPnrViewNotification.action";
    var params = "hdnAction=SEND&txtFlightNo=" + getValue("txtFlightNo") + "&selNotifyChannel=" + getValue("selNotifyChannel")
        + "&hdnSmsPnrSegList=" + arrSmsPnrList + "&hdnEmailPnrSegList=" + arrEmailPnrList + "&txtNewDepDate=&" +
        "hdnNtCode=" + getValue("txtCode") + "&txtDepTime1=&hdnMode=" + mode + "&hdnMode=" + "" + "&hdnDepDateFrom=" + getValue("txtFromDate") +
        "&hdnDepDateTo=" + getValue("txtToDate") + "&hdnSelectedPnr=" + selectedPnrArr + "&selGenLanguage=en&hdnDepDate=" + ""
        + "&hdnNtMessage=" + getValue("txtMessage") + "&hdnFlightId=" + getValue("hdnFlightId") + "&hdnLoadPnrCriteria=" + buildPnrLoadCriteria(objDGDetail.arrGridData[0][2]) + "";
    makePOST(url, params);
}

function getMode(notiId) {
    var action = notiId.split("_")[2].substring(0, 3);
    if (action == 'CNX') {
        return 'CANCEL';
    } else if (action == 'CHG') {
        return 'UPDATE';
    } else if (action == 'RPT') {
        return 'REPROTECT';
    }
}

function buildPnrLoadCriteria(notiId) {
    var d = notiId.split("_")[1];
    var pnrLoadInfo = [];
    pnrLoadInfo[0] = getValue("hdnAction");
    pnrLoadInfo[1] = getValue("txtFlightNo");
    pnrLoadInfo[2] = d.substring(0, 2) + '/' + d.substring(2, 4) + '/' + d.substring(4);
    pnrLoadInfo[3] = getValue("txtPnr");
    pnrLoadInfo[4] = "-1";
    pnrLoadInfo[5] = "-1";
    pnrLoadInfo[6] = "-1";
    pnrLoadInfo[7] = "";
    pnrLoadInfo[8] = "-1";
    pnrLoadInfo[9] = " ";
    pnrLoadInfo[10] = " ";
    pnrLoadInfo[11] = " ";
    pnrLoadInfo[12] = " ";
    pnrLoadInfo[13] = " ";
    pnrLoadInfo[14] = " ";
    pnrLoadInfo[15] = " ";
    pnrLoadInfo[16] = " ";
    pnrLoadInfo[17] = " ";
    pnrLoadInfo[18] = " ";
    pnrLoadInfo[19] = " ";
    pnrLoadInfo[20] = " ";
    pnrLoadInfo[21] = " ";
    pnrLoadInfo[22] = getValue("txtFromDate");
    pnrLoadInfo[23] = getValue("txtToDate");

    return pnrLoadInfo;
}

function valiadatePhoneNumber(){
	if ((top.objCWindow.document.getElementById('txtMobile1').value=="" && 
			top.objCWindow.document.getElementById('txtMobile2').value=="" &&
			top.objCWindow.document.getElementById('txtMobile3').value=="")){
		return true;
	}
	else if (top.objCWindow.document.getElementById('txtMobile1').value=="" || 
			top.objCWindow.document.getElementById('txtMobile2').value=="" ||
			top.objCWindow.document.getElementById('txtMobile3').value==""){
		return false;
	}
	if ((top.objCWindow.document.getElementById('txtLand1').value=="" && 
			top.objCWindow.document.getElementById('txtLand2').value=="" &&
			top.objCWindow.document.getElementById('txtLand3').value=="")){
		return true;
	}
	else if (top.objCWindow.document.getElementById('txtLand1').value=="" || 
			top.objCWindow.document.getElementById('txtLand2').value=="" ||
			top.objCWindow.document.getElementById('txtLand3').value==""){
		return false;
	}
	return true;
}
