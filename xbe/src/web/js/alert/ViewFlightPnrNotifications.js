var selectedPnrArr = new Array();
var intSelRow = -1;
var strRowData;
var strGridRow;
var intLastRec;
var screenId = "SC_RESV_CC_028";
var valueSeperator = "~";
var arrDetailData = new Array();
var arrMsgListData = new Array();


function setDate(strDate, strID) {
    switch (strID) {
        case "4" :
            setField("txtFromDate", strDate);
            break;
        case "5" :
            setField("txtToDate", strDate);
            break;
        case "3" :
            setField("txtDepDate", strDate);
            break;
    }
}

function loadCalendar(strID, objEvent) {
    var displayCal = true;
    objCal1.ID = strID;
    if (strID == 4) {
        if (getFieldByID("txtFromDate").disabled) {
            displayCal = false;
        }
        objCal1.top = 85;
        objCal1.left = 300;
    } else if (strID == 5) {
        if (getFieldByID("txtToDate").disabled) {
            displayCal = false;
        }
        objCal1.top = 85;
        objCal1.left = 500;
    } else if (strID == 3) {
        objCal1.top = 130;
        objCal1.left = 555;
    }
    if (displayCal) {
        objCal1.onClick = "setDate";
        objCal1.showCalendar(objEvent);
    }
}


function validate() {
    top[2].MessageReset();

    if (getValue("txtPnr") == "" && getValue("txtFlightNo") == "" && getValue("txtFromDate") == "" && getValue("txtToDate") == "") {
        showERRMessage(raiseError("XBE-ERR-01", "Flight No ,PNR OR DateRange"));
        getFieldByID("txtFlightNo").focus();
        return false;
    }


    if (getValue("txtFromDate") == "") {
        var fromDate = new Date();
        var dd = fromDate.getDate();
        var mm = fromDate.getMonth() + 1; //January is 0!
        var yyyy = fromDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        fromDate = dd + '/' + mm + '/' + yyyy;
        getFieldByID("txtFromDate").value = fromDate;
    }
    if (getValue("txtToDate") == "" || compareDate(getText("txtFromDate"), getText("txtToDate")) == 1) {

        var td = getValue("txtFromDate").split('/');

        var toDate = new Date(td[2], td[1] - 1, td[0]);
        var dd = toDate.getDate();
        var mm = toDate.getMonth() + 2; //January is 0!
        var yyyy = toDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        } else if (mm > 12) {
            mm = '01';
            yyyy = yyyy + 1;
        }

        toDate = dd + '/' + mm + '/' + yyyy;
        getFieldByID("txtToDate").value = toDate;

    }
    return true;
}

function buildPnrViewCriteria() {
    var pnrViewInfo = new Array();

    pnrViewInfo[0] = getValue("hdnAction");
    pnrViewInfo[1] = getValue("txtFlightNo");
    pnrViewInfo[2] = getValue("txtPnr");
    pnrViewInfo[3] = getValue("txtFromDate");
    pnrViewInfo[4] = getValue("txtToDate");

    setField("hdnViewPnrCriteria", pnrViewInfo);
    top.strSearchCriteria = getText("hdnViewPnrCriteria");

}

function pageOnChange() {
    top[2].HidePageMessage();
}

function initialize() {
    //setVisible("spnViewNotificationSummary", false);
    //setVisible("spnViewNotificationDetails", false);
    top.strPageFrom = "";
    top.strPageReturn = "";

}

function populatePnrViewCriteria(strPnrInfor) {
    if ((strPnrInfor != null) && (strPnrInfor != "")) {
        setField("hdnViewPnrCriteria", strPnrInfor);
        var pnrViewInfo = strPnrInfor.split(",");
        setField("hdnAction", pnrViewInfo[0]);
        setField("txtFlightNo", pnrViewInfo[1]);
        setField("hdnFlightNo", pnrViewInfo[1]);
        setField("hdnPnr", pnrViewInfo[2]);
        setField("txtPnr", pnrViewInfo[2]);
        setField("txtFromDate", pnrViewInfo[3]);
        setField("txtToDate", pnrViewInfo[4]);
    }
}

function closeNotification() {
    top.strSearchCriteria = "";
    top.LoadHome();
}

function onLoad() {
    top.ResetTimeOut();
    initialize();
    if (top.strSearchCriteria != "") {
        populatePnrViewCriteria(top.strSearchCriteria);
    }
}

onLoad();


//To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {

    //-----------------------------
    if (intRecNo <= 0) {
        intRecNo = 1;
    }
    setField("hdnRecNo", intRecNo);
    setSearchRecNo(intRecNo);
    //setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
    if (intErrMsg != "") {
        showERRMessage(intErrMsg);
    } else {
        setPageEdited(false);
        setField("hdnMode", "PAGE");
        document.forms[0].submit();
        ShowProgress();

    }
}

function setSearchRecNo(recNo) {
    top[0].intLastRec = recNo;
}

function setTotalRecords(totRec) {
    top[0].totalRec = totRec;
}


function winOnLoad() {
    //buildPnrViewCriteria();
    setField("hdnFlightId", strFlightId);
}

function showGridView() {
    if (document.getElementById('changeView').innerHTML == "Switch to Table View") {

        document.getElementById('changeView').innerHTML = "Switch to Grid View";
        document.getElementById('trCalendar').style.visibility = "collapse";
        document.getElementById('tdCalendar').style.visibility = "collapse";
        document.getElementById('gridViewNotifications').style.visibility = "hidden";
        document.getElementById('trSpnViewNotificationSummary').style.visibility = "visible";
        document.getElementById('spnViewNotificationSummary').style.visibility = "visible";
        document.getElementById('trSpnViewNotificationDetails').style.visibility = "visible";
        document.getElementById('spnViewNotificationDetails').style.visibility = "visible";
        document.getElementById('trSpnViewPaxContactsDetails').style.visibility = "visible";
        document.getElementById('trSpnViewFlightNotificationGrid').style.visibility = "collapse";
        document.getElementById('spnViewFlightNotificationGrid').style.visibility = "collapse";        
        document.getElementById('rtTxtMessage').style.visibility = "visible";
    } else {

        document.getElementById('changeView').innerHTML = "Switch to Table View";
        document.getElementById('trCalendar').style.visibility = "visible";
        document.getElementById('tdCalendar').style.visibility = "visible";
        document.getElementById('gridViewNotifications').style.visibility = "visible";
        document.getElementById('trSpnViewNotificationSummary').style.visibility = "hidden";
        document.getElementById('spnViewNotificationSummary').style.visibility = "hidden";
        document.getElementById('trSpnViewNotificationDetails').style.visibility = "hidden";
        document.getElementById('spnViewNotificationDetails').style.visibility = "hidden";
        document.getElementById('trSpnViewPaxContactsDetails').style.visibility = "hidden";
        document.getElementById('trSpnViewFlightNotificationGrid').style.visibility = "collapse";
        document.getElementById('spnViewFlightNotificationGrid').style.visibility = "collapse";
        document.getElementById('rtTxtMessage').style.visibility = "hidden";
    }

}

function showSummaryView() {
    if (validate()) {
        setField("hdnAction", "VIEWSUMMARY");
        setSearchRecNo(1);
        setField("hdnRecNo", top[0].intLastRec);
        buildPnrViewCriteria();
        ShowProgress();
        getFieldByID("frmViewNotify").submit();
    }
}

function RowClick(strRowNo) {
    strRowData = objDG.getRowValue(strRowNo);
    strGridRow = strRowNo;
    setField("hdnFlightNotificationId", arrSummaryData[strRowNo][13] + "," + arrSummaryData[strRowNo][4]);
    setField("hdnAction", "VIEWDETAILS");
    buildPnrViewCriteria();
    document.getElementById('txtCode').value = arrSummaryData[strRowNo][5];
    document.getElementById('txtMessage').value = arrSummaryData[strRowNo][6];
    reqChannel = arrSummaryData[strRowNo][14];
    var url = "showFlightPnrViewNotification.action";
    var params = "hdnAction=VIEWDETAILS&hdnFlightNotificationId=" + getValue("hdnFlightNotificationId")+ "&hdnPnr=" +  getValue("txtPnr");
    makePOST(url, params);

}

function processStateChange() {
    var arrayString;

    if (httpReq.readyState == 4) {
        if (httpReq.status == 200) {
            if (httpReq.responseURL.indexOf('manageFlightPnrViewNotification.action') > 0) {
                showSummaryView();
                return;
            }
            if(getValue("hdnCalendarLoad") == "true" ){
            	arrayString = httpReq.responseText;
                console.log("Array ",httpReq);
                arrayString = arrayString.replace(/^\s+|\s+$/g, '');
                eval(arrMsgListData = arrayString);
                viewFlightNotificationDG.arrGridData = arrMsgListData;
                if (arrMsgListData.length > 0) {
                    setVisible("spnViewFlightNotificationGrid", true);
                }
                viewFlightNotificationDG.refresh();
            }else{
            	arrayString = httpReq.responseText;
                console.log("Array ",httpReq);
                arrayString = arrayString.replace(/^\s+|\s+$/g, '');
                eval(arrDetailData = arrayString);
                objDGDetail.arrGridData = arrDetailData;
                if (arrDetailData.length > 0) {
                    setVisible("spnViewNotificationDetails", true);
                }
                objDGDetail.refresh();
                
                document.getElementById('spnViewPaxContactsDetails').innerHTML = '';
                document.getElementById('spnViewPaxContactsDetails').insertAdjacentHTML('beforeend', '<td align="left" <label class="GridItem" id="lblSelLoc">Select Location</label></td>');


                var countryList = arrDetailData[(arrDetailData.length - 1)][15];
                var res = countryList.split(",");
                for (var x = 0; x < res.length; x++) {
                    var dat = res[x].split('-');
                    if (dat[1]!=null){
                        document.getElementById('spnViewPaxContactsDetails').insertAdjacentHTML('beforeend', '<td align="left" class="GridItem"> <input onclick="resetDetailList(this)" class="GridItem" checked value="' + dat[1] + '" id="' + res[x] + '" type="checkbox">' + dat[0] + " (" + dat[2] + ")" + '</td>');
                    }
                }
                selectAll('all', '', 'true');

                setField("hdnFlightId", arrDetailData[(arrDetailData.length - 1)][18]);
            }
        } else {
            showPageERRMessage(raiseError('XBE-ERR-33'), "Details not found");
        }
    }

}

function resetDetailList(code) {
    if (code.checked) {
        selectAll('code', code.value, 'true');
    } else {
        selectAll('code', code.value, 'false');

    }
}
