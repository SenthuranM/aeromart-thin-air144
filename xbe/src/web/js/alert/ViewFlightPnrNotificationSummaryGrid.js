if (typeof(arrSummaryData) != "object") {
    var arrSummaryData = new Array();
}
;

if (arrSummaryData.length > 0) {
    setVisible("spnViewNotificationSummary", true);
} else {
    if (top.strSearchCriteria == "")
        setVisible("spnViewNotificationSummary", false);
    else
        setVisible("spnViewNotificationSummary", true);

}
var recstNo = top[0].intLastRec;


var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 4;
objCol1.toolTip = "Notification Date";
objCol1.headerText = "Notification Date";
objCol1.itemAlign = "left";
objCol1.sort = true;

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "18%";
objCol2.arrayIndex = 5;
objCol2.toolTip = "Notification Code";
objCol2.headerText = "Notification Code";
objCol2.itemAlign = "left";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "34%";
objCol3.arrayIndex = 6;
objCol3.toolTip = "Notification Message";
objCol3.headerText = "Notification Message";
objCol3.itemAlign = "center";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "8%";
objCol5.arrayIndex = 8;
objCol5.toolTip = "Delivered SMS";
objCol5.headerText = "Delivered SMS";
objCol5.itemAlign = "right";

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "8%";
objCol6.arrayIndex = 11;
objCol6.toolTip = "Delivered Email";
objCol6.headerText = "Delivered Email";
objCol6.itemAlign = "right";

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "8%";
objCol7.arrayIndex = 9;
objCol7.linkURL = "";
objCol7.toolTip = "Failed SMS";
objCol7.headerText = "Failed SMS";
objCol7.itemAlign = "center";

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "8%";
objCol8.arrayIndex = 12;
objCol7.linkURL = "";
objCol8.toolTip = "Failed Email";
objCol8.headerText = "Failed Email";
objCol8.itemAlign = "center";

var objDG = new DataGrid("spnViewNotificationSummary");
objDG.addColumn(objCol1); //Date
objDG.addColumn(objCol2); //code
objDG.addColumn(objCol3); //Message
objDG.addColumn(objCol5); //Delivered Sms
objDG.addColumn(objCol6); // Failed Sms
objDG.addColumn(objCol7); //Delivered Email
objDG.addColumn(objCol8); // Failed Email

objDG.width = "100%";
objDG.height = "150px";
objDG.headerBold = false;
objDG.arrGridData = arrSummaryData;
objDG.seqNo = true;
objDG.seqStartNo = recstNo;
objDG.paging = true;
objDG.pgnumRecPage = 200;
objDG.rowSelect = true;
objDG.pgonClick = "gridNavigations"
objDG.rowClick = "RowClick";

//ShowProgress();
objDG.displayGrid();
//HideProgress();

