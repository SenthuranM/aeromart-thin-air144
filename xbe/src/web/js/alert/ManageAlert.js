	var selectedAlertArr = new Array();
	var intSelRow	= -1;
	var strRowData;
	var strGridRow;
	var intLastRec;
	var screenId = "SC_RESV_CC_012";
	var valueSeperator = "~";
	var totalRec;
		
	
	
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.buildCalendar();
	
	function setDate(strDate, strID) {
		switch (strID) {
			case "0" : setField("txtFromDate", strDate); break;
			case "1" : setField("txtToDate", strDate); break;
			case "2" : setField("txtDepDate", strDate); break;
			case "3" : setField("txtOriDepDate", strDate); break;			
		}
	}

	function loadCalendar(strID, objEvent) {
		var displayCal = true;
		objCal1.ID = strID;
		if (strID == 0) {
			if (getFieldByID("txtFromDate").disabled) {
				displayCal = false;
			}
			objCal1.top = 85; 
			objCal1.left = 300;
		} else if (strID == 1) {
			if (getFieldByID("txtToDate").disabled) {
				displayCal = false;
			}
			objCal1.top = 85; 
			objCal1.left = 500;
		} else if (strID == 2) {
			objCal1.top = 130; 
			objCal1.left = 555;
		} else if(strID == 3) {
			objCal1.top = 85;
			objCal1.left = 750;
		}	
		if (displayCal) {	
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
	}
	
	function pageBtnClick(intID) {
		switch (intID){
			case 0 : 
				if (searchValidations()) {
					top.blnProcessCompleted=true;
					setField("hdnAction", "SEARCH");
					setField("hdnAlertInfo", "");						
					setField("hdnSelectedAlerts", "");	
					setField("hdnRecNo", 1);
					setSearchRecNo(1);								
					buildAlertInfo();
//					alert("aaa Just Before Submit hdnAlertInfo : " + getValue("hdnAlertInfo"));
					//top[2].ShowProgress();
					ShowProgress();
					getFieldByID("frmAlert").submit();
				}
				break;
			case 1 :
				if (searchValidations()) {
					if (clearAlertValidations()) {
						if (confirm(arrError["deleteConfirmation"])) {
							Disable("btnClearAlert", true);
							setField("hdnSelectedAlerts", selectedAlertArr);								
							setField("hdnSearchAirline", selectedAlertArr);	
							setField("hdnAction", "DELETE");
							setField("hdnRecNo", top[0].intLastRec);
							buildAlertInfo();		
							getFieldByID("frmAlert").submit();
						}
					}
				}
				break;
		}
	}		
	
	function searchValidations() {
		top[2].MessageReset();
		if (getFieldByID("rdoAlertOption2").checked) {
			if (getValue("txtFromDate") == "") {
				showERRMessage(raiseError("XBE-ERR-01", "From date"));
				getFieldByID("txtFromDate").focus();		
				return false;
			}
			if (!dateValidDate(getValue("txtFromDate"))){
				showERRMessage(raiseError("XBE-ERR-04", "from date"));
				getFieldByID("txtFromDate").focus();	
				return false;
			}
			if (!CheckDates(getValue("txtFromDate"), top.strSysDate)) {
				showERRMessage(raiseError("XBE-ERR-05", "From date", top.strSysDate));
				getFieldByID("txtFromDate").focus();	
				return false;
			}
			if (getValue("txtToDate") == "") {
				showERRMessage(raiseError("XBE-ERR-01", "To date"));
				getFieldByID("txtToDate").focus();		
				return false;
			}	
			if (!dateValidDate(getValue("txtToDate"))){
				showERRMessage(raiseError("XBE-ERR-04", "to date"));
				getFieldByID("txtToDate").focus();	
				return false;
			}
			if (!CheckDates(getValue("txtToDate"), top.strSysDate)) {
				showERRMessage(raiseError("XBE-ERR-05", "To date", top.strSysDate));
				getFieldByID("txtToDate").focus();	
				return false;
			}
			if (!CheckDates(getValue("txtFromDate"), getValue("txtToDate"))){
				showERRMessage(raiseError("XBE-ERR-03", "To date", "From date"));
				getFieldByID("txtToDate").focus();
				return false;
			}	
		} 
		if (getValue("txtDepDate") != "") {
			if (!dateValidDate(getValue("txtDepDate"))){
				showERRMessage(raiseError("XBE-ERR-04", "departure date"));
				getFieldByID("txtDepDate").focus();	
				return false;
			}
		}
		if(getValue("txtOriDepDate") != "") {
			if(!dateValidDate(getValue("txtOriDepDate"))) {
				showERRMessage(raiseError("XBE-ERR-04", "original departure date"));
				getFieldByID("txtOriDepDate").focus();	
				return false;
			}
		}
		/*if (trim(getValue("txtResultLimit")) != "") {
			if(!validateInteger(getValue("txtResultLimit")) ){
				showERRMessage(raiseError("XBE-ERR-04", "results limit"));				
				getFieldByID("txtResultLimit").focus();	
				return false;
			}
			if (Number(getValue("txtResultLimit")) > 
					Number(getValue("hdnResultsLimit"))) {
				showERRMessage(raiseError(
				"XBE-ERR-05", "Limit results to", getValue("hdnResultsLimit")));
				getFieldByID("txtResultLimit").focus();	
				return false;
			}
			if (getValue("txtResultLimit") < 1) {
				showERRMessage(raiseError("XBE-ERR-03", "Limit results to", "1"));
				getFieldByID("txtResultLimit").focus();	
				return false;
			}			
		}*/
		return true;
	}
		
	function clearAlertValidations() {
		top[2].MessageReset();
		var count = 0;
		var selectedAlerts = objDG.getSelectedColumnValue(5);
		selectedAlertArr = new Array();
		for (var i = 0; i < selectedAlerts.length; i++) {
			if (selectedAlerts[i][1] != null) {
				selectedAlertArr[count] = selectedAlerts[i][1] 
				count++;
			}
		}
		if (selectedAlertArr.length <= 0) {
			return false;
		}
//		alert("aaa selectedAlertArr : " + selectedAlertArr);
		return true;
	}
	
	function buildAlertInfo() {
		var alertInfo = new Array();
		var resultsLimit;
		if (getValue("hdnAlertInfo") != "") {
			alertInfo = getValue("hdnAlertInfo").split(",");
			alertInfo[0] = getValue("hdnAction");	
			if (getValue("hdnAction") == "VIEWPNR") {		
				alertInfo[6] = getValue("hdnPnr");
			} else {
				alertInfo[6] = "";
			}
		} else {
			/*resultsLimit = getValue("txtResultLimit");
			if ((resultsLimit == "") || (resultsLimit <= 0)) {
				resultsLimit = getValue("hdnResultsLimit");
			}*/
			alertInfo[0] = getValue("hdnAction");		
			alertInfo[1] = getValue("txtFromDate");
			alertInfo[2] = getValue("txtToDate");
			alertInfo[3] = getValue("txtFlightNo");		
			alertInfo[4] = getValue("txtDepDate");	
			alertInfo[5] = 0; // resultsLimit;
			alertInfo[6] = getValue("hdnPnr");
			alertInfo[7] = getValue("selDept");	
			alertInfo[8] = getValue("txtOriDepDate");
			alertInfo[9] = getValue("selSearchAirline");
			alertInfo[10] = getValue("selAnciReprotectStatus");
			alertInfo[11] = getValue("selStatusSearch");
		}		
		setField("hdnAlertInfo", alertInfo);
		top.strSearchCriteria = getText("hdnAlertInfo");
//		alert("aaa When build the search hdnAlertInfo : " + getText("hdnAlertInfo"));
//		alert("aaa When build top.strSearchCriteria : " + top.strSearchCriteria);
	}	
	
	
	function pageOnChange() {
		top[2].HidePageMessage();
	}

	function keyValidate(strID, objControl) {
		pageOnChange();
		switch (strID) {
			case 0 : 
				var strValue = objControl.value ;
				if (!validateInteger(strValue)) {
					objControl.value = strValue.substr(0, strValue.length - 1);
					objControl.focus();
				}
				break;
		}
	}
	
	function checkRadio() {
		if (getFieldByID("rdoAlertOption1").checked) {
			setField("txtFromDate", "");		
			setField("txtToDate", "");					
			Disable("txtFromDate", true);
			Disable("txtToDate", true);
		} else {
			Disable("txtFromDate", false);
			Disable("txtToDate", false);
		}
	}
		
	function initialize() {
		setVisible("spnSelectAll", false);
		setVisible("spnAlert", false);
		Disable("btnClearAlert", true);
		//setField("txtResultLimit", getValue("hdnResultsLimit"));
		top.strPageFrom		= "";
		top.strPageReturn	= "";
		checkRadio();
		//top[2].HideProgress();
	}
	
	function populateAlertInfo(strAlertInfor) {		
//		alert("aaa populateAlertInfo(strAlertInfor) : " + strAlertInfor);
		if ((strAlertInfor != null) && (strAlertInfor != "")) {			
			setField("hdnAlertInfo", strAlertInfor);
			var alertInfo = strAlertInfor.split(",");
			setField("hdnAction",alertInfo[0]);		
			setField("txtFromDate",alertInfo[1]);
			setField("txtToDate",alertInfo[2]);
			setField("txtFlightNo",alertInfo[3]);		
			setField("txtDepDate",alertInfo[4]);	
			//setField("txtResultLimit",alertInfo[5]);
			setField("hdnPnr",alertInfo[6]);
			setField("selDept",alertInfo[7]);
			getFieldByID("selDept").selected=alertInfo[7];			
			setField("selSearchAirline",alertInfo[9]);
			getFieldByID("selSearchAirline").selected=alertInfo[9];			
			setField("selAnciReprotectStatus",alertInfo[10]);
			getFieldByID("selAnciReprotectStatus").selected=alertInfo[10];
			setField("selSearchAirline",alertInfo[9]);
			getFieldByID("selSearchAirline").selected=alertInfo[9];			
			setField("selAnciReprotectStatus",alertInfo[10]);
			getFieldByID("selAnciReprotectStatus").selected=alertInfo[10];
			setField("txtOriDepDate", alertInfo[8]);
			if (alertInfo[1] != "" || alertInfo[2] != ""){
				setField("rdoAlertOption1","alertsRange");
				setField("rdoAlertOption2","alertsRange");
				Disable("txtFromDate",false);
				Disable("txtToDate",false);
			}
		}
	}	
	
	
	function onLoad() {
		top.ResetTimeOut();
		initialize();
//		alert("aaa When load top.strSearchCriteria : " + top.strSearchCriteria);
//		alert("aaa When load reqAlertInfo : " + reqAlertInfo);
		//top[2].ShowProgress();
		if (top.strSearchCriteria != "" ) {
		//if ((!top.strSearchCriteria == null ) && (!top.strSearchCriteria == "" )) {
//			alert("aaa About to call populateAlertInfo with top.strSearchCriteria " );
			populateAlertInfo(top.strSearchCriteria);				
			if (reqAlertInfo != "SEARCHCALLED" ) {
//				alert("aaa About to call search ! ");	
				pageBtnClick(0);
			}
//		} else {
//			populateAlertInfo(reqAlertInfo);				
		}
				
	}
	
	function showAlertResponceMessage(){
				  			
		if ((reqMessage != null) && (reqMessage != "")) {
			if ((reqMessageType != null) && (reqMessageType == "confirmation")) {			
				showConfirmation(reqMessage);
			} else {
				showERRMessage(reqMessage);
			}
		} else {
			top[2].HidePageMessage();
		}
	}
	
	onLoad();
	
	function closeAlert(){
		top.strSearchCriteria = "";
		top.LoadHome();
	}
	
	//To Handle Paging
function gridNavigations(intRecNo, intErrMsg){

	//-----------------------------
	if (intRecNo <= 0) {
			intRecNo = 1;
		}
		//setSearchRecNo(intRecNo);	
		//setSearchCriteria()
		setField("hdnRecNo", intRecNo);
		setSearchRecNo(intRecNo);
		//setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		if (intErrMsg != ""){		
			showERRMessage(intErrMsg);
		}else{				
			setPageEdited(false);
			//document.forms[0].hdnMode.value="PAGE";
			setField("hdnMode", "PAGE");
			document.forms[0].submit();
			ShowProgress();
		
		}
}

function setSearchRecNo(recNo) {
	top[0].intLastRec = recNo;
}

function setTotalRecords(totRec){
	top[0].totalRec=totRec;
}


function winOnLoad() {
	buildAlertInfo();
}
