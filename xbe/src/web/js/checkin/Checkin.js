
var screenId = "SC_RESV_CH_001";
var paxChkscreenId = "SC_RESV_CH_002";
var tabValue = getTabValue();
var flightNo ;
var fltDate ;
var fltRoute;
var objProgressCheck;
var isPaxDetail;
var fliData = new Array();
var fltID;
var fltStatus;
var airportCode;
var manualCheckinEnable;
var flightSegId ;
var flightCheckinStatus;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();
	
function setDate(strDate, strID){
	switch (strID) {
		case "0" : setField("txtStartDateSearch",strDate);break ;
		case "1" : setField("txtStopDateSearch",strDate);break ;	
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	if (strID == 0){
		objCal1.top = 20;
		objCal1.left = 75;
	}else if (strID == 1){
		objCal1.top = 20;
		objCal1.left = 175;
	}
	objCal1.onClick = "setDate";
	if ((strID == 0) && (!getFieldByID("txtStartDateSearch").disabled)) {
		objCal1.showCalendar(objEvent);
	} else if ((strID == 1) && (!getFieldByID("txtStopDateSearch").disabled)) {
		objCal1.showCalendar(objEvent);
	} else if(blnCalander) {
		objCal1.showCalendar(objEvent);
	}
}
	
function LoadCalendarNew(strID, objEvent){
	objCal.top = 0;
	objCal.left = 20;
	objCal.ShowCalendar(objEvent);
	objCal.onClick = "setDate";
}
	
function dataChanged(){
	setPageEdited(true);
}

	
//call the functoion  onload
function winOnLoad(strMsg, strMsgType, strWarnMsg) {
	setField("selStatusSearch",defaultStatus);
	setField("txtFlightNoStartSearch",defCarrCode);
	strMsg = strMsg;
	setPageEdited(false);
	clearSearchSection();
	if (getTabValue() != "" && getTabValue() != null ) {
		var strSearch = getTabValue().split(",");
		setField("txtStartDateSearch",strSearch[1]);
		setField("txtStopDateSearch",strSearch[2]);
		setField("txtFlightNoStartSearch",strSearch[8]);
		setField("txtFlightNoSearch",strSearch[3]);
		setField("selFromStn6",strSearch[4]);
		getFieldByID("selFromStn6").selected=strSearch[4];
		setField("selToStn6",strSearch[5]);
		getFieldByID("selToStn6").selected=strSearch[5];
		setField("selCheckinStatusSearch",strSearch[6]);
		getFieldByID("selCheckinStatusSearch").selected=strSearch[6];
		setField("selStatusSearch",strSearch[7]);
		getFieldByID("selStatusSearch").selected=strSearch[7];

		if(strMsg != null && strMsg != "" && strMsg != "Undefined") {
			showERRMessage(strMsg);
		}
		ClearProgressbar();		
	}
}

function ClearProgressbar() {
	if (objDG.loaded) {
		clearTimeout(objProgressCheck);
		HideProgress();
		top.blnProcessCompleted=false;
	}
}
	
function SearchData() {
	if(SearchValid()==false) return;
	setSearchCriteria();
	setSearchRecNo(1);
	document.forms[0].hdnMode.value = "CHECKIN";
	document.forms[0].submit();
	ShowProgress();
}

//Validtaions when searching
function SearchValid(){
	//Mandatory fields		
	if (getFieldByID("txtStartDateSearch").value == "") {
		showERRMessage(arrError['startdate']);
		return false;
	}
	if (getFieldByID("txtStopDateSearch").value == "") {
		showERRMessage(arrError['stopdate']);
		return false;
	}
	if (!dateChk("txtStartDateSearch")){
		showERRMessage(arrError['invalidDate']);
		getFieldByID("txtStartDateSearch").focus();
		return false;
	}
	if (!dateChk("txtStopDateSearch")){
		showERRMessage(arrError['invalidDate']);
		getFieldByID("txtStopDateSearch").focus();
		return false;
	}
		
	//compare start date and stop date
	var schStartD = getFieldByID("txtStartDateSearch").value;
	var schStopD = getFieldByID("txtStopDateSearch").value;
	
	if (!CheckDates(schStartD,schStopD)){
		showERRMessage(arrError['stopdategreat']);
		return false;
	}
	if(trim(getFieldByID("txtFlightNoSearch").value) == ""){
		showERRMessage(arrError['invalidFlightSearch']);
		return false;
	}
	if(trim(getFieldByID("selFromStn6").value) == ""){
		showERRMessage(arrError['invalidOriginSearch']);
		return false;
	}	
	
	return true;
}

function timeCheck(id, obj) {
	if (!isTimeFormatValid(obj)) {
		switch (id) {
			case 0 : msg = arrError['invalidConnectionLimit']; break;
			case 1 : msg = arrError['invalidStartTime']; break;
			case 2 : msg = arrError['invalidEndTime']; break;
		}
		showERRMessage(msg);
		getFieldByID(obj).focus();
		return false;
	}
	return true;
}

function isTimeFormatValid(objTime) {
	// Checks if time is in HH:MM format - HH can be more than 24 hours
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = getValueTrimed(objTime).match(timePattern);
	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + getValueTrimed(objTime));
	}
	return true;	
}

function getValueTrimed(strValue){
	return trim(getValue(strValue));
}

//clear the search area
function clearSearchSection() {
	setField("txtFlightNoSearch","");
	setField("selFromStn6","");
	setField("selToStn6","");
}

//hide the message
function objOnFocus(){
	top[2].HidePageMessage();
}

function gridNavigations(intRecNo, intErrMsg) {
	if (intRecNo <= 0) {
		intRecNo = 1;
	}
	setSearchRecNo(intRecNo);
	setSearchCriteria()
	setField("hdnRecNo", intRecNo);
	if (intErrMsg != "") {
		showERRMessage(intErrMsg);
	}else {
		setPageEdited(false);
		document.forms[0].hdnMode.value="CHECKIN";
		document.forms[0].submit();
		ShowProgress();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function isPageEdited() {
	if(top.pageEdited) {
		var cnf =  confirm("Changes will be lost! Do you wish to Continue?");
		return cnf;
	}else {
		return true;
	}
}

function gridClick(strRowNo) {
	fltID = flightData[strRowNo][17];
	fltStatus = flightData[strRowNo][15];
	manualCheckinEnable = flightData[strRowNo][18];
	airportCode = document.getElementById("selFromStn6").value;
	flightSegId = flightData[strRowNo][19];
	flightNo = flightData[strRowNo][1];
	fltDate = flightData[strRowNo][2] + flightData[strRowNo][3];
	fltStatus  = flightData[strRowNo][15];
	flightCheckinStatus = flightData[strRowNo][16];
	
	var btnOpenForCheckin  = document.getElementById("btnOpenForCheckin");
	var btnStartCheckin  = document.getElementById("btnStartCheckin");
	var btnCompleteCheckin  = document.getElementById("btnCompleteCheckin");	
	var btnCloseForCheckin  = document.getElementById("btnCloseForCheckin");
	var btnReopenCheckin  = document.getElementById("btnReopenCheckin");
	var btnAddComment  = document.getElementById("btnAddComment");
	var btnViewComment  = document.getElementById("btnViewComment");
	var btnView  = document.getElementById("btnView");
	if(btnOpenForCheckin){btnOpenForCheckin.style.visibility = 'hidden';}
	if(btnStartCheckin){btnStartCheckin.style.visibility = 'hidden';}
	if(btnCompleteCheckin){btnCompleteCheckin.style.visibility = 'hidden';}
	if(btnCloseForCheckin){btnCloseForCheckin.style.visibility = 'hidden';}
	if(btnReopenCheckin){btnReopenCheckin.style.visibility = 'hidden';}
	if(btnAddComment){btnAddComment.style.visibility = 'hidden';}
	if(btnViewComment){btnViewComment.style.visibility = 'hidden';}
	if(btnView){btnView.style.visibility = 'visible';}
	
	var currentStatus = flightData[strRowNo][16];
	if(fltStatus != "CNX" && manualCheckinEnable == "Y"){
		switch(currentStatus) {
			case "" : 
				btnOpenForCheckin.style.visibility = 'visible';
				btnView.style.visibility = 'hidden';
				break;
			case "NPN" : 
				btnOpenForCheckin.style.visibility = 'visible';
				btnView.style.visibility = 'hidden';
				break;
			case "OPN" :
				btnStartCheckin.style.visibility = 'visible';
				btnCompleteCheckin.style.visibility = 'visible';				
				break;
			case "CLS" :				
				btnReopenCheckin.style.visibility = 'visible';
				break;
			case "COM" :
				btnOpenForCheckin.style.visibility = 'visible';
				btnCloseForCheckin.style.visibility = 'visible';
				break;
			case "RPN" :
				btnStartCheckin.style.visibility = 'visible';
				btnCompleteCheckin.style.visibility = 'visible';
				break;
		}
		if(btnAddComment){btnAddComment.style.visibility = 'visible';}
		if(btnViewComment){btnViewComment.style.visibility = 'visible';}
	}
	
}

function getTabValue() {
	var tabValue = top[0].manifestSearchValue;
	if ((tabValue == "") || (tabValue == null)) {
		tabValue = new Array("");
	}
	return tabValue;
}

function setSearchCriteria() {

	top[0].manifestSearchValue = fltID+","+getFieldByID("txtStartDateSearch").value+","
		+ getFieldByID("txtStopDateSearch").value+","+ getFieldByID("txtFlightNoSearch").value
		+","+ getFieldByID("selFromStn6").value+","+ getFieldByID("selToStn6").value+","
		+ getFieldByID("selCheckinStatusSearch").value+","+ getFieldByID("selStatusSearch").value
		+","+ getFieldByID("txtFlightNoStartSearch").value;
}

function setSearchRecNo(recNo) {
	top[0].intLastRec = recNo;
}

function resetClick() {
	clearSearchSection();
	setField("selCheckinStatusSearch",defaultStatus);
	setField("selStatusSearch",defaultStatus);	
}

function checkinFunctions(id){
	document.forms[0].hdnFlightId.value=fltID;
	document.forms[0].hdnAirportCode.value=airportCode;
	document.forms[0].hdnFlightSegId.value=flightSegId; 
	document.forms[0].hdnMode.value = "CHECKIN";
	switch (id) {
		case 0 : 
			document.forms[0].hdnCheckinMode.value = "OPENCHECKIN"; 
			break;
		case 1 :
			setSearchCriteria();
			var strAction = "showPassengerCheckin.action?hdnMode=VIEW&hdnFlightID="+fltID+"&hdnAirportCode"+airportCode;
			getFieldByID("frmCheckin").action = strAction;
			getFieldByID("frmCheckin").submit();
			break;
		case 2 : 
			document.forms[0].hdnCheckinMode.value = "COMPLETECHECKIN";
			setSearchCriteria();
			break;
		case 3 : 
			document.forms[0].hdnCheckinMode.value = "CLOSECHECKIN";
			break;
		case 4 : 
			document.forms[0].hdnCheckinMode.value = "REOPENCHECKIN";
			break;
		case 5 : 
			setSearchCriteria();
			var strAction = "showPassengerCheckin.action?hdnMode=VIEWONLY&hdnFlightID="+fltID+"&hdnAirportCode"+airportCode;
			getFieldByID("frmCheckin").action = strAction;
			getFieldByID("frmCheckin").submit();
			break;
	}
	if(id != 1){
		document.forms[0].submit();
	}
}

function viewCheckinNote() {
	var intHeight = 600;
	var intWidth = 800;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showCheckinNotes.action?hdnMode=VIEW&hdnFlightSegId="+flightSegId;
	objWindow1 = window.open(strFilename, "CWindow1", strProp);
}


function addCheckinNote() {
	var intHeight = 330;
	var intWidth = 600;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showCheckinNotes.action?hdnMode=ADD&hdnFlightSegId="+flightSegId;
	objWindow1 = window.open(strFilename, "CWindow1", strProp);

}
