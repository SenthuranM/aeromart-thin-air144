var screenId = "SC_RESV_CH_003";
var paxStatusArr= new Array("GOSHO");
var paxStatusOptArr= new Array("G");
var isPfsScreenEdited = false;

function winOnLoad(strMsg,strMsgType) {
	if(strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType,strMsg);
	populateStatusCombo(arrTitle,"selTitle",arrTitle);
	populatePaxTypeCombo(paxTypes,"selPaxType",paxTypes);
	populateCombo(paxStatusArr,"selAddStatus",paxStatusOptArr);
}

function populateStatusCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	control.options[0]=new Option("","");
	var p=0;
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
		p=t+1;
		control.options[p]=new Option(strArr[1],strArr[0]);
	}
}

function populatePaxTypeCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	var p=0;
	sort(dataArr,1);
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
		control.options[t]=new Option(strArr[1],strArr[0]);
	}
}

function populateCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	for(var t=0;t<dataArr.length;t++){
		control.options[t]=new Option(dataArr[t],optArr[t]);
	}
}

function resetPFS(){
	setField("selTitle","");
	setField("selPaxType",paxTypes[0][0]);
	setField("txtFirstName","");
	setField("txtLastName","");
	setField("selDest","");
	setDefaultAirport();
	setField("selAddStatus","N");
	setField("selCC","Y");
	setField("selPaxCat","A");
	isPfsScreenEdited = false;
}

function setDefaultAirport(){
	if(getFieldByID("selDest").options.length==2){
			getFieldByID("selDest").options[1].selected=true;
	}
}

function closeWindow(){	
	if(pfsDetailChanged){
		var strTxt=opener.top[0].checkinPfsDetails;
		var arrTabData=new Array()
		if(strTxt!=""){
			 arrTabData=strTxt.split(",");
		}
		opener.location = "showPassengerCheckin.action?hdnFlightId="+arrTabData[1]+"&hdnAirportCode="+arrTabData[2];
		window.close();
	}
}

function pfsDetailChanged(){
	validateData();
	if(isPfsScreenEdited){
		return confirm("Changes will be lost! Do you wish to Continue?");
	}
	return true;
}

function processSavePFS() {
	if(validateData()){
		var strTxt=opener.top[0].checkinPfsDetails;
		
		var arrTabData=new Array()
		if(strTxt!=""){
			 arrTabData=strTxt.split(",");
		}
		
		document.forms[0].hdnMode.value="SAVE";
		document.forms[0].hdnFlightSegId.value=arrTabData[0];
		document.forms[0].hdnFltId.value=arrTabData[1];
		document.forms[0].hdnPaxPnr.value=arrTabData[3];
		document.forms[0].hdnPaxType.value=arrTabData[4];
		isPfsScreenEdited = false;
		document.forms[0].submit();		
	}
}

function validateData(){
	if(getText("selTitle") == "") {
		showWindowCommonError("Error","Please select pax title!");
		getFieldByID("selTitle").focus();
		return false;
	} else {
		isPfsScreenEdited = true;
	}
	if(getText("txtFirstName") == "") {
		showWindowCommonError("Error","Please enter the first name!");
		getFieldByID("txtFirstName").focus();
		return false;
	} else {
		isPfsScreenEdited = true;
	}
	if(getText("txtLastName") == "") {
		showWindowCommonError("Error","Please enter the last name!");
		getFieldByID("txtLastName").focus();
		return false;
	} else {
		isPfsScreenEdited = true;
	}
	if(getText("selDest") == "") {
		showWindowCommonError("Error","Please select the destination!");
		getFieldByID("selDest").focus();
		return false;
	} else {
		isPfsScreenEdited = true;
	}
	return true;
}
