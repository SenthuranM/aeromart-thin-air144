var recNumPerPage = 20;
var recstNo = top[0].intLastRec;
var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "7%";
objCol2.arrayIndex = 1;
objCol2.toolTip = "Flight No" ;
objCol2.headerText = "Flight<br>No";
objCol2.itemAlign = "center";
objCol2.sort = true;

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "10%";
objCol3.arrayIndex = 2;
objCol3.toolTip = "Depature Date" ;
objCol3.headerText = "D.Date<br>local";
objCol3.itemAlign = "center";
objCol3.sort = true;

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "5%";
objCol4.arrayIndex = 3;
objCol4.toolTip = "Origin" ;
objCol4.headerText = "Org.<br>";
objCol4.itemAlign = "center";
objCol4.sort = true;

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "5%";
objCol5.arrayIndex = 4;
objCol5.toolTip = "Destination" ;
objCol5.headerText = "Dest.<br>";
objCol5.itemAlign = "center";
objCol5.sort = true;

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "8%";
objCol6.arrayIndex = 5;
objCol6.toolTip = "Segments" ;
objCol6.headerText = "Segments";
objCol6.itemAlign = "left"
	
    var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "6%";
objCol7.arrayIndex = 6;
objCol7.toolTip = "Estimated Time Departure - Zulu" ;
objCol7.headerText = "ETD<BR>zulu";
objCol7.itemAlign = "center";
objCol7.sort = true;

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "6%";
objCol8.arrayIndex = 7;
objCol8.toolTip = "Estimated Time Departure - Local" ;
objCol8.headerText = "ETD<br>local";
objCol8.itemAlign = "center";

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "8%";
objCol9.arrayIndex =8;
objCol9.toolTip = "Aircraft Model" ;
objCol9.headerText = "Aircraft<br>Model";
objCol9.itemAlign = "center";

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "5%";
objCol10.arrayIndex = 9;
objCol10.toolTip = "Seats Allocated" ;
objCol10.headerText = "Seats<br>Alloc.";
objCol10.itemAlign = "right";

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 10;
objCol11.toolTip = "Seats Sold" ;
objCol11.headerText = "Seats<br>Sold";
objCol11.itemAlign = "right";

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "5%";
objCol12.arrayIndex = 11;
objCol12.toolTip = "Seats on Hold" ;
objCol12.headerText = "On<br>Hold";
objCol12.itemAlign = "right";

var objCol13 = new DGColumn();
objCol13.columnType = "label";
objCol13.width = "5%";
objCol13.arrayIndex = 12;
objCol13.toolTip = "Stand by" ;
objCol13.headerText = "Stand<br>by";
objCol13.itemAlign = "right";

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "6%";
objCol14.arrayIndex = 13;
objCol14.toolTip = "Checked in" ;
objCol14.headerText = "Checked<br>in";
objCol14.itemAlign = "right";

var objCol15 = new DGColumn();
objCol15.columnType = "label";
objCol15.width = "6%";
objCol15.arrayIndex = 14;
objCol15.toolTip = "To be checked in" ;
objCol15.headerText = "To be<br>checked in";
objCol15.itemAlign = "right";

var objCol16 = new DGColumn();
objCol16.columnType = "label";
objCol16.width = "6%";
objCol16.arrayIndex = 15;
objCol16.toolTip = "Flight status" ;
objCol16.headerText = "Flight<br>Status";
objCol16.itemAlign = "center";

var objCol17 = new DGColumn();
objCol17.columnType = "label";
objCol17.width = "7%";
objCol17.arrayIndex = 16;
objCol17.toolTip = "Checkin status" ;
objCol17.headerText = "Checkin<br>Status";
objCol17.itemAlign = "center";
        

// ---------------- Grid	

var objDG = new DataGrid("spnInstances");	
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);
objDG.addColumn(objCol12);
objDG.addColumn(objCol13);
objDG.addColumn(objCol14);
objDG.addColumn(objCol15);
objDG.addColumn(objCol16);
objDG.addColumn(objCol17);

objDG.width = "99%";
objDG.height = "350px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = flightData;
objDG.seqNo = true;

var seqSize = Number(recstNo) + (Number(recNumPerPage) - 1);
if (new String(seqSize).length > 2) {
	objDG.seqNoWidth = "4%";
} else {
	objDG.seqNoWidth = "3%";	
}
objDG.backGroundColor = "#ECECEC";
objDG.seqStartNo = recstNo;
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = recNumPerPage;
objDG.pgonClick = "gridNavigations";	
objDG.rowClick = "gridClick";
objDG.displayGrid();
	
