var screenId = "SC_RESV_CH_002";
var paxChkscreenId = "SC_RESV_CH_002";

var paxPnr;
var paxType;
		

function winOnLoad() {
	
	setField("hdnFltId", fltId);
	setField("hdnAirportCode", airportCode);
	
	//flight details
	var strFlightDetails = "<table width='90%' border='0' cellpadding='0' cellspacing='4'><tr>"
		+"<td align='left' width='25%'><font><b>Flight Number : </b>"+ flightNumber +"</font></td>"
		+"<td align='left' width='35%'><font><b>Date/Time : </b>"+ flightDateTime +"</font></td>"
		+"<td align='left' width='15%'><font><b>Status : </b>"+ flightStatus +"</font></td>"
		+"<td align='left' width='25%'><font><b>Check-in Status : </b>"+ flightCheckinStatus +"</font></td></tr></table>"	
	document.getElementById("spnFlightDetails").innerHTML = strFlightDetails;
		
	//Pax load status
	var cos = arrCOS.split("#");
	var details ;
	
	var strPaxLoadStatus = "<table width='90%' border='0' cellpadding='0' cellspacing='2' BORDER='1' RULES='NONE' FRAME='BOX'><tr>"
		+"<td align='left' width='14%'><font><b>Load Status : </font></td>";
		for(var i=0 ; i<cos.length -1; i++){
			details = cos[i].split(",");
			if(i>0){
				strPaxLoadStatus += "<td align='left' width='14%'><font>&nbsp</font></td>";
			}
			strPaxLoadStatus += "<td align='left' width='6%'><font><b>"+details[0]+"</b> :- </font></td>"
			+"<td align='left' width='16%' width='14%'><font><b>Male : </font></b><font>"+details[1]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Female : </font></b><font>"+details[2]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Child : </font></b><font>"+details[3]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Infant : </font></b><font>"+details[4]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Other : </font></b><font>"+details[5]+"</font></td></tr>";
		}
		strPaxLoadStatus += "</table>";	
	document.getElementById("spnPaxLoad").innerHTML = strPaxLoadStatus;
	
	
	//Load status
	var load = arrLoad.split("#");
	var strLoadStatus = "<table width='90%' border='0' cellpadding='0' cellspacing='2' BORDER='1' RULES='NONE' FRAME='BOX'><tr>"
		+"<td align='left' width='14%'><font><b>Load Status : </font></td>";
		for(var i=0 ; i<cos.length -1; i++){
			details = load[i].split(",");
			if(i>0){
				strLoadStatus += "<td align='left' width='14%'><font>&nbsp</font></td>";
			}
			strLoadStatus += "<td align='left' width='6%'><font><b>"+details[0]+"</b> :- </font></td>"
			+"<td align='left' width='16%'><font><b>Confirmed : </font></b><font>"+details[1]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Standby : </font></b><font>"+details[2]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Checkedin : </font></b><font>"+details[3]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Goshow : </font></b><font>"+details[4]+"</font></td>"
			+"<td align='left' width='16%'><font><b>Pending : </font></b><font>"+details[5]+"</font></td></tr>";
		}
		strLoadStatus += "</table>";	
	document.getElementById("spnLoadStatus").innerHTML = strLoadStatus;	
	
}

function selectCheckedin(){	
	var strAction ="showPassengerCheckin.action?hdnMode=VIEW&hdnSelMode=CHECKEDIN&hdnFlightId="+fltId;
	getFieldByID("frmPassengerCheckin").action = strAction;
	getFieldByID("frmPassengerCheckin").submit();
	isAllSelected = false;
}

function selectToBeCheckedin(){
	var strAction ="showPassengerCheckin.action?hdnMode=VIEW&hdnSelMode=TOBECHECKEDIN&hdnFlightId="+fltId;
	getFieldByID("frmPassengerCheckin").action = strAction;
	getFieldByID("frmPassengerCheckin").submit();
	isAllSelected = false;
}

function selectStandBy(){
	var strAction ="showPassengerCheckin.action?hdnMode=VIEW&hdnSelMode=STANDBY&hdnFlightId="+fltId;
	getFieldByID("frmPassengerCheckin").action = strAction;
	getFieldByID("frmPassengerCheckin").submit();
	isAllSelected = false;
}

function selectGoshow(){
	var strAction ="showPassengerCheckin.action?hdnMode=VIEW&hdnSelMode=GOSHOW&hdnFlightId="+fltId;
	getFieldByID("frmPassengerCheckin").action = strAction;
	getFieldByID("frmPassengerCheckin").submit();
	isAllSelected = false;
}

function selectAllPax() {
	var strAction ="showPassengerCheckin.action?hdnMode=VIEW&hdnFlightId="+fltId;
	getFieldByID("frmPassengerCheckin").action = strAction;
	getFieldByID("frmPassengerCheckin").submit();
	isAllSelected = false;
}

function clickBack(){
	//top[1].objTMenu.tabSetTabInfo("SC_INVN_002", "SC_FLGT_001",
		//	"showFlight.action", "Flights", false);
	var strCheckinSearch = top[0].manifestSearchValue.split(",");
	location.replace("showCheckin.action?hdnMode=CHECKIN&hdnFlightId="+ strCheckinSearch[0] 
                    + "&txtStartDateSearch=" + strCheckinSearch[1] + "&txtStopDateSearch=" + strCheckinSearch[2] 
                    + "&txtFlightNoSearch="	+ strCheckinSearch[3] + "&selFromStn6=" + strCheckinSearch[4] 
                    + "&selToStn6="	+ strCheckinSearch[5] + "&selCheckinStatusSearch=" + strCheckinSearch[6] 
                    + "&selStatusSearch=" + strCheckinSearch[7] + "&txtFlightNoStartSearch=" + strCheckinSearch[8]);	
}

function viewCheckinNote() {
	var intHeight = 600;
	var intWidth = 800;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showCheckinNotes.action?hdnMode=VIEW&hdnFlightSegId="+flightSegId;
	objWindow1 = window.open(strFilename, "CWindow1", strProp);
}


function addCheckinNote() {
	var intHeight = 330;
	var intWidth = 600;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showCheckinNotes.action?hdnMode=ADD&hdnFlightSegId="+flightSegId;
	objWindow1 = window.open(strFilename, "CWindow1", strProp);

}

function addGoshow() {

	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
	
	var selPaxPnr;
	var selPaxType;
	
	var selectedPassengers = objDG.getSelectedColumn(11);
	var selectedCount = 0;
	for (var i = 0; i < selectedPassengers.length; i++) {
		if (selectedPassengers [i][1] == true) {
			selectedCount++;
			if(selectedCount > 1){
				alert("To Add GoShow, Select only one record");
				return false;
			}
			selPaxPnr = arrCheckinPassengers[i][1];
			selPaxType = arrCheckinPassengers[i][6];
		}
	}
	
	
	if(selPaxPnr == null || 
			selPaxPnr == '' ||
			selPaxType == null ||
			selPaxType == ''){		
		selPaxPnr = paxPnr;
		selPaxType = paxType;
	}

	
	
	var strFilename = "showPFSDetailsProcessing.action?hdnFlightSegId="+flightSegId+"&hdnFltId="+fltId+"&hdnPaxPnr="+paxPnr+"&hdnPaxType="+paxType;
	top[0].checkinPfsDetails = flightSegId+","+fltId+","+airportCode+","+selPaxPnr+","+selPaxType;
	var intHeight = 300;
	var intWidth = 620;
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	paxType = '';
	paxPnr = '';
	top[0].objWindow = window.open(strFilename, "PFSDetails", strProp);

}

function gridClick(strRowNo){	
	paxPnr = arrCheckinPassengers[strRowNo][1];
	paxType = arrCheckinPassengers[strRowNo][6];
}
