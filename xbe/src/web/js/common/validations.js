<!--
//validations.js
//Sudheera Liyanage

function validateUSPhone( strValue ) {
/************************************************
Ex. (999) 999-9999 or (999)999-9999
*/
	var objRegExp  = /^\([1-9][0-9]{2}\)\s?[0-9]{3}\-[0-9]{4}$/;

  	//check for valid us phone with or without space between
  	//area code
  	return objRegExp.test(strValue);
}

function  isNumeric( strValue ) {
	var objRegExp  =  /(^-?[0-9][0-9]*\.[0-9]*$)|(^-?[0-9][0-9]*$)|(^-?\.[0-9][0-9]*$)/;

  	//check for numeric characters
  	return objRegExp.test(strValue);
}

function validateInteger( strValue ) {
	var objRegExp  = /(^-?[0-9][0-9]*$)/;

  	//check for integer characters
  	return objRegExp.test(strValue);
}


function validateValue(strValue,strMatchPattern ){
	var objRegExp = new RegExp( strMatchPattern);

 	//check if string matches pattern
 	return objRegExp.test(strValue);
}


function currencyValidate(strValue, nValue){
	var strNValue = "*"
	var strDValue = "*" ;
	var strPattern = "" ;
	var nDecimal = top.opener.top.strDecimalPoints;
	 
	if (nValue != ""){
		strNValue = "{0," + nValue + "}";
	}
	
	if (nDecimal != ""){
		strDValue = "{0," + nDecimal + "}";
	}
	strPattern = "(^-?[0-9]" + strNValue + "\\.[0-9]" + strDValue + "$)|(^-?[0-9]" + strNValue + "$)|(^-?\\.[0-9]" + strDValue + "$)";
	return validateValue(strValue, strPattern);
}

function isEmpty(s) {return (trim(s).length==0);}
function hasWSpace(s){return RegExp("^\s*$").test(s);}
function isAlpha(s){return RegExp("^[a-zA-Z]+$").test(s);}
function isInt(n){return RegExp("^[-+]?[0-9]+$").test(n);}
function isPositiveInt(n){return RegExp("^[+]?[0-9]+$").test( n );}
function isNegativeInt(n){return RegExp("^-[0-9]+$").test(n);}
function isAlphaNumeric(s) {return RegExp("^[a-zA-Z0-9-/]+$").test(s);}
function isAlphaNumericWithhyphen(s) {return RegExp("^[a-zA-Z0-9]+$").test(s);}  
function isAlphaNumericWhiteSpace(s){return RegExp("^[a-zA-Z0-9-/ \w\s]+$").test(s);}
function isAlphaWhiteSpace(s){return RegExp("^[a-zA-Z-/ \w\s]+$").test(s);}
function isDecimal(n){return RegExp("^[-+]?[0-9]+[.][0-9]+$").test(n);}
function isLikeDecimal(n){
	window.status=isInt(n)+'||'+RegExp("^[-+]?[0-9]+[.]+$").test(n)+'||'+isDecimal(n);
	return isInt(n)||RegExp("^[-+]?[0-9]+[.]$").test(n)||isDecimal(n);
}
function isPositiveDecimal(n){return (RegExp("^[+]?[0-9]+[.][0-9]+$").test(n));}
function isNegativeDecimal(n){return RegExp("^-[0-9]+[.][0-9]+$").test(n);}
function isAlphaNumericWhiteSpaceAnper(s){return RegExp("^[a-zA-Z0-9-/ \& \w\s]+$").test(s);}
function checkTime(s){ return RegExp( "^[012][0-9]:[0-5][0-9]$" ).test( s );}
function checkEmail(s){return RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?([.][a-z0-9]([a-z0-9-]*[a-z0-9])?)+$").test(s);}

function isValidText(s){return RegExp( "^[a-zA-Z0-9-()./%& ]*$" ).test(s); }

//**********************
function attachEvents(){
    document.onkeypress=checkOnKeyPress;
    //document.oncontextmenu=stopRightMouse;
	document.body.onpaste=validatePaste;
}	


//**********************
function checkOnKeyPress() {
    var e=event;
    var kCode=e.keyCode;
    var keys1=[34,39,60,62,94,126];
    //alert("validKeysOnly=" + kCode);

    if (kCode=="13"){
		onEnterKeyPressed();
		return;
	}
    keys1="_"+keys1.join("_")+"_";
    if(keys1.indexOf("_"+kCode+"_")!=-1)e.returnValue=false;
}


//**********************
function validatePaste() {
	
    var val=window.clipboardData.getData("Text");
    var obj=event.srcElement
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
			window.clipboardData.setData("Text",removeInvalids(val));
        }
    }
}

//**********************
function removeInvalids(val){
    var re1=/'|"/g;
    var re2=/<|>|~|\^/g;

    val=val.replace(re1,"");
    val=val.replace(re2,"-");
    return val;
}


//**********************
function stopRightMouse(){
    var e=event;
    var obj=e.srcElement;
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
            return true;
        }
    }
    return false;
}

//**********************
function doAfterLoad(){
	attachEvents();
}

//***********************************************
function isError(e,msg,id){
    var blnErr=false;
    var val=getVal(id);
    var obj=getField(id);

    if(e=="NULL"){
        if (isEmpty(val))blnErr=true;
    }else if(e=="NOT_ALPHA"){
        blnErr=!isAlpha(val);
    }else if(e=="NOT_ALPHANUMERIC"){
        blnErr=!isAlphaNumeric(val);
    }else if(e=="NaN"){
            blnErr=(isNaN(val));
    }else if(e=="NOT_FLOAT"){
            blnErr=(!isNaN(val))&&(val.indexOf(".")!=-1);
    }else if(e.substr(0,6)=="MAXVAL"){
        blnErr=(Number(val)>Number(e.substr(6)));
	}else if(e.substr(0,6)=="MINVAL"){
        blnErr=(Number(val)<Number(e.substr(6)));
    }else if(e.substr(0,6)=="MAXLEN"){
        blnErr=(val.length>Number(e.substr(6)));
    }else if(e.substr(0,6)=="MINLEN"){
        blnErr=(val.length<Number(e.substr(6)));
    }else if(e.substr(0,6)=="WHITESPACE"){
        blnErr=hasWSpace();
    }else{
        alert("isErr:"+e+":"+"invalid error type" );
    }

    if (blnErr){
        alert(msg);
		if(obj[0]){
			if (obj.tagName=="SELECT"){
				obj.focus();
			}else{
				obj[0].focus()
			}
		}else if(!(obj.readOnly||obj.disabled||obj.type=='hidden'||obj.style.visibility=='hidden'||obj.style.display=='none')){
			obj.focus();
		}
    }else{
        setField(id,trim(val));
    }

    return blnErr;
}


function removeChars(str,regX){
	return str.replace(new RegExp(regX),"");
}

function keyPressCheckEmail(){
	var obj=window.event.srcElement;
	if(!isEmpty(obj.value)&&!checkEmail(obj.value)){
		alert('Invalid Email Address');
		obj.focus();
	}
}
//TIME Validation............
function IsValidTime(timeStr) {
	// Checks if time is in HH:MM:SS AM/PM format.
	// The seconds and AM/PM are optional.
	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
		//alert("Time is not in a valid format.");
		return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	
	if (second=="") { second = null; }
	//if (ampm=="") { ampm = null }
	if (hour < 0  || hour > 23) {
		//alert("Hour must be between 1 and 12. (or 0 and 23 for military time)");
		return false;
	}
	//if (hour <= 12 && ampm == null) {
	//if (confirm("Please indicate which time format you are using.  OK = Standard Time, CANCEL = Military Time")) {
	//alert("You must specify AM or PM.");
	//return false;
	//   }
	//}
	//if  (hour > 12 && ampm != null) {
	//alert("You can't specify AM or PM for military time.");
	//return false;
	//}
	if (minute<0 || minute > 59) {
		//alert ("Minute must be between 0 and 59.");
		return false;
	}
	if (second != null && (second < 0 || second > 59)) {
		//alert ("Second must be between 0 and 59.");
		return false;
	}
	return false;
}

function numberKeyPress(e){
	var key,isCtrl;

	if(window.event) {
		key = e.keyCode;
		isCtrl = window.event.ctrlKey
	}
	else if(e.which) {
		key = e.which;
		isCtrl = e.ctrlKey;
	}

	if(key==8||isCtrl){
		return true;
	}else{
		var keychar = String.fromCharCode(key);
	}

	return isInt(keychar);

}

function blockNonNumbers(obj, e, allowDecimal, allowNegative){
	var key;
	var isCtrl=false;
	var keychar;
	var reg;
		
	if(window.event){
		key=e.keyCode;
		isCtrl=window.event.ctrlKey
	}
	else if(e.which){
		key=e.which;
		isCtrl=e.ctrlKey;
	}
	
	if(isNaN(key))return true;
	
	keychar=String.fromCharCode(key);
	
	// check for backspace or delete, or if Ctrl was pressed
	if (key==8||isCtrl){
		return true;
	}
	
	var isInvalid=obj.value.length!=0&&(keychar=='-'||keychar=='+');
	if(isInvalid){
		return false;
	}

	reg=/\d/;
	var isFirstNeg=allowNegative?keychar=='-'&&obj.value.indexOf('-')==-1:false;
	var isFirstPos=keychar=='+'&&obj.value.indexOf('+')==-1;

	var isFirstD=allowDecimal?keychar=='.'&&obj.value.indexOf('.')==-1:false;
	
	return isFirstNeg||isFirstPos||isFirstD||reg.test(keychar);
}

function formatTime(strTime,forceFormat) {    
    var text = strTime.value;
    
    if(text.length == 0)
        return;
        
    var numericPart = '';
    var characterPart = '';
    var i;
    var current;
    
    // Break the text input into numeric and character parts for easier parsing			    
    for(i = 0; i < text.length; i++) {  
        current = text.charAt(i);			        
        
        if(isInt(current))
            numericPart = numericPart + current;
            
        if(isAlpha(current))
            characterPart = characterPart + current;
   }
   					   
   var formattedDate;
   var hour;
   var minute;
   var dayPart;
   
   // Handle AM/PM by looking for an a, otherwise treat as PM
    if(characterPart.indexOf('a') > -1 || characterPart.indexOf('A') > -1)
        dayPart = 'AM';
    else
        dayPart = 'PM';
     
    if(numericPart.length >= 4) {
		hour = numericPart.substring(0, 2);
		minute = numericPart.substring(2, 4);
	} else if(numericPart.length == 3) {	 
		hour = numericPart.substring(0, 1);
		minute = numericPart.substring(1, 3);
	} else if(numericPart.length == 2) {	 
		hour = numericPart.substring(0, 2);
		minute = '00';		
	} else if(numericPart.length == 1) {	 
		hour = numericPart.substring(0, 1);
		minute = '00';									
	} else {
		if(!forceFormat)return;
		// Just use the current hour
		var d = new Date();
		hour = d.getHours();	
		
		minute = '00';
	}	
	
	// Apply 24 hour logic
	if(hour > 12) {
		if(hour <= 24) {
			//hour -= 12;
			dayPart = 'PM';
		} else {
			// If the hour is still > 12 then the user has inputed something that doesn't exist
			if(!forceFormat)return;

			//else just use current hour
			hour = (new Date()).getHours();				
					
			if(hour > 12) {		
				//hour -= 12;
				dayPart = 'PM';
			} else {		
				dayPart = 'AM';
			}
		}
	}	
	
	if(hour == 0) {
		hour = 12;
		dayPart = 'AM';
	}
		
	 strTime.value = hour + ':' + minute + ' ' + dayPart;				 
}

function checkAlpha(obj,e,allowChars){
	var key;
	var isCtrl=false;
	var keychar;
	var reg;
		
	if(window.event){
		key=e.keyCode;
		isCtrl=window.event.ctrlKey
	}
	else if(e.which){
		key=e.which;
		isCtrl=e.ctrlKey;
	}
	
	if(isNaN(key))return true;
		
	// check for backspace or delete, or if Ctrl was pressed
	if (key==8||key==46||isCtrl){
		return true;
	}
	
	keychar=String.fromCharCode(key);

	if(allowChars&&allowChars!=null&&allowChars.indexOf(keychar)!=-1){
		return true;
	}

	return isAlpha(keychar);
}

function checkAlphaNumeric(obj,e,allowChars){
	try{
		var key;
		var isCtrl=false;
		var keychar;
		var reg;
			
		if(window.event){
			key=e.keyCode;
			isCtrl=window.event.ctrlKey
		}
		else if(e.which){
			key=e.which;
			isCtrl=e.ctrlKey;
		}
		
		if(isNaN(key))return true;
			
		// check for backspace or delete, or if Ctrl was pressed
		if (key==8||key==46||isCtrl){
			return true;
		}
		
		keychar=String.fromCharCode(key);
	
		if(allowChars&&allowChars!=null&&allowChars.indexOf(keychar)!=-1){
			return true;
		}
	
		return isAlpha(keychar)||isNumber(keychar);
	}catch(e){}
}

function checkName(obj,e){
	var allowChars="";
	return checkAlphaNumeric(obj,e,allowChars);
}

var showXBEMain="../private/showXBEMain.action";
var winParams=new Array();


function refineJS(strJS){
	var winCC=null;
	if(strJS&&strJS!=null){
		if(strJS.indexOf('../js/login/Login.js')!=-1){
			strJS="aMessage['code']='ERROR';aMessage['desc']='';";
		}
	}
	
	if(top.name=='winCC'){
		winCC=top;
	}else{
		if(top.opener&&!top.opener.closed){
			winCC=top.opener.top;
		}
	}
	
	if(winCC!=null && winCC.ResetTimeOut != undefined){
		winCC.ResetTimeOut();
	}
	strJS=strJS.replace(/\r\n|\r|\n/g, '');
	return strJS;
}

function setTimeWithColon(txtField) {
	var strTime = txtField.value;
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			txtField.value = strTime;
		} else {
			var mn = "00";
			if(strTime.length == 3 || strTime.length == 4)
				mn = strTime.substr(index,2);

			var hr = 0;
			if(strTime.length == 3) {
				hr =  strTime.substr(0,1); 
			}else {
				hr = strTime.substr(0,2); 
			}
			var timecolon = hr +":" + mn ;
			txtField.value = timecolon;
			strTime = timecolon;
		}
	}
	return IsValidTime(strTime);
}

//Taking it from Admin with GSA Move -Thush

function showWindowCommonError(msgType,msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = msgType; 
	ShowPageMessage();
	return true;
}

function anyCurrencyValidate(strValue, nValue, nDecimal){
	var strNValue = "*"
	var strDValue = "*" ;
	var strPattern = "" ;
	
	if (nValue != ""){
		strNValue = "{0," + nValue + "}";
	}
	
	if (nDecimal != ""){
		strDValue = "{0," + nDecimal + "}";
	}
	strPattern = "(^[-+]?[0-9]" + strNValue + "\\.[0-9]" + strDValue + "$)|(^[-+]?[0-9]" + strNValue + "$)|(^\\.[0-9]" + strDValue + "$)";
	return validateValue(strValue, strPattern);
}
function validateTextArea(objControl) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	/*if (!isAlphaNumericWhiteSpace(strValue)) {
		objControl.value = strValue.substr(0,strValue.length - 1);
	}*/	
	if (strLength > 255)
	{
		strValue = strValue.substr(0,255);
		objControl.value = strValue;
	}
}

function alphaValidate(objTxt){
	var strValue = objTxt.value;
	if (!isAlpha(strValue)) {
		objTxt.value = removeChars(strValue,/[0-9`!@#$\ss&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
	}	
}

function isValidNumericChar(p, e){
	var charCode = (e.which) ? e.which : e.keyCode;
	var charValue = String.fromCharCode(charCode);			
	
	if (p.allowDecimal && !p.allowNegative){
		if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
			return false;
		}
	} else if (!p.allowDecimal && p.allowNegative){
		if (charCode > 31 && (charCode < 48 || charCode > 57) && charValue != '-') {
			return false;
			  }
	} else if (p.allowDecimal && p.allowNegative){
		if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46 && charValue != '-') {
			return false;
		}
	} else {
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		    return false;
		}
	}
	
	return true;
}
function validateReportDate(strFromDate, strTodate, repStartDate){	
		var tempDay;
		var tempMonth;
		var tempYear;
		var validate = false;
		
		var dateFrom = getText(strFromDate);
		var dateTo = getText(strTodate);
	
		tempIStartDate = dateFrom;
		tempIEndDate = dateTo;
				
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}
	 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
		
		strSysODate=(tempYear+tempMonth+tempDay); 
		
		if(tempOStartDate > tempOEndDate){
			showERRMessage(arrError["fromDtExceed"]);
			getFieldByID(strTodate).focus();
		} else if (!CheckDates(repStartDate,getText(strFromDate))) {
			showERRMessage(arrError["rptReriodExceeds"] +" "+ repStartDate);
			getFieldByID(strFromDate).focus();			
		} else {
			validate = true;				
		}
		return validate;	
}


//-->

