	/*
	*********************************************************
		Description		: Drop down filling (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 16th August 2005
		Last Modified	: 07th October 2005		
	*********************************************************	
	*/
	
	function listBox(){
		this.id = "lst1";								// List Box ID
		this.dataArray = new Array();					// Data Array
		this.valueIndex = 0;							// Value field array index
		this.textIndex = 1;								// Text field array index
		this.blnFirstEmpty = false;						// first value empty or not
		this.blnMergeTextValue = false;					// merge text field with the value field or not
		this.blnMergeStyle = "-";						// merge Style allowed  - or ( 
		this.firstValue = ""							// first value 
		this.firstTextValue = "Please Select a value"	// first Text
		this.frameName	= "";							// frame Name
		
		// methords
		this.fillListBox = fillListBox;					// filling the list box 
		this.fillListBoxByName = fillListBoxName		// filling the list box by Name
	}

	function fillListBox(){
		var objC = this ;
		var intLength = objC.dataArray.length ;
		var arrID = objC.id.split(",");
		var objDL; 

		// get the selectet box objects to array
		for (var x = 0 ; x < arrID.length ; x++){
			arrID[x] = document.getElementById(arrID[x]);
			objDL = arrID[x];
			objDL.length = 0 ; 
		}
		
		// fill the first value
		if (objC.blnFirstEmpty){
			for (var x = 0 ; x < arrID.length ; x++){
				objDL = arrID[x];
				objDL.length =  objDL.length + 1
				objDL.options[objDL.length - 1].text = objC.firstTextValue;
				objDL.options[objDL.length - 1].value = objC.firstValue;
			}
		}

		// fill the data
		var idx=(objC.blnFirstEmpty)?1:0;
		for (var i=0;i<intLength;i++){
			opt=new Option();
			opt.value=objC.dataArray[i][objC.valueIndex];

			for (var y=0;y<arrID.length;y++){
				objDL = arrID[y];
				//objDL.length = objDL.length + 1;
				
				if (objC.blnMergeTextValue){
					switch (objC.blnMergeStyle){
						case "-" :
							//objDL.options[objDL.length - 1].text = unescape(objC.dataArray[i][objC.valueIndex] + " - " + objC.dataArray[i][objC.textIndex]);
							opt.text=unescape(objC.dataArray[i][objC.valueIndex] + " - " + objC.dataArray[i][objC.textIndex]);
							break;
						case "(" :
							//objDL.options[objDL.length - 1].text = unescape(objC.dataArray[i][objC.textIndex] + " (" + objC.dataArray[i][objC.valueIndex] + ")");
							opt.text=unescape(objC.dataArray[i][objC.textIndex] + " (" + objC.dataArray[i][objC.valueIndex] + ")");
							break;
						}					
				}else{
					//objDL.options[objDL.length - 1].text = unescape(objC.dataArray[i][objC.textIndex]);
					opt.text=unescape(objC.dataArray[i][objC.textIndex]);
				}
				//objDL.options[objDL.length - 1].value = objC.dataArray[i][objC.valueIndex];
				objDL.options[idx]=opt;
				idx++;
				
			}
		}
	}
	
	function fillListBoxName(){
		var objC = this ;
		var intLength = objC.dataArray.length ;
		var arrID = objC.id.split(",");
		var objDL; 
		
		// get the selectet box objects to array
		for (var x = 0 ; x < arrID.length ; x++){
			var objControls;
			if (objC.frameName == ""){
				objControls = document.getElementsByName(arrID[x]);
			}else{
				objControls = frames[objC.frameName].document.getElementsByName(arrID[x]);
			}
			var intCount = objControls.length ; 
			for (var i = 0 ; i < intCount ; i++){
				objDL = objControls[i];
				objDL.length = 0 ; 
			}
		}
		
		// fill the first value
		if (objC.blnFirstEmpty){
			for (var x = 0 ; x < arrID.length ; x++){
				var objControls;
				if (objC.frameName == ""){
					objControls = document.getElementsByName(arrID[x]);
				}else{
					objControls = frames[objC.frameName].document.getElementsByName(arrID[x]);
				}
			
				var intCount = objControls.length ; 
				for (var i = 0 ; i < intCount ; i++){
					objDL = objControls[i];
					objDL.length =  objDL.length + 1
					objDL.options[objDL.length - 1].text = objC.firstTextValue;
					objDL.options[objDL.length - 1].value = objC.firstValue;
				}
			}
		}
		
		// fill the data
		for (var i = 0 ; i < intLength ; i++){
			for (var y = 0 ; y < arrID.length ; y++){
				var objControls;
				if (objC.frameName == ""){
					objControls = document.getElementsByName(arrID[y]);
				}else{
					objControls = frames[objC.frameName].document.getElementsByName(arrID[y]);
				}
				var intCount = objControls.length ; 
				for (var x = 0 ; x < intCount ; x++){
					objDL = objControls[x];
					objDL.length = objDL.length + 1
					if (objC.blnMergeTextValue){
						objDL.options[objDL.length - 1].text = objC.dataArray[i][objC.valueIndex] + " - " + objC.dataArray[i][objC.textIndex];
					}else{
						objDL.options[objDL.length - 1].text = objC.dataArray[i][objC.textIndex];
					}
					objDL.options[objDL.length - 1].value = objC.dataArray[i][objC.valueIndex];
				}
			}
		}
		
	}
	// --------------------------------- End of Drop down filling