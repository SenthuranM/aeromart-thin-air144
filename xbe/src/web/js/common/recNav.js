/*
	*********************************************************
		Description		: Record navigate control (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 14th July 2005
	*********************************************************	
	*/
	
	function recNavigate(){
		this.divID = "";							// Div ID
		this.numStart = 0;							// Starting Record Number
		this.numRecPage = 10;						// Paging size 
		this.numRecTotal = 0;						// Total Record count
		this.onClick = "";							// Onclick client function
		this.align = "left"							// Alignment
		this.style = 1 ;							// Record Navigation Style available styles 2
		this.intPages = 10;							// Style 2 page size

		this.tabIndexPrev = 0;						// Tab Index for Prev Button
		this.tabIndexNext = 0;						// Tab Index for Next Button
		this.tabIndexNo	= 0;						// Tab Index for Text Box
		this.tabIndexGo = 0;						// Tab Index for Go Button
		
		this.tooltipPrevious = "Previous Page";		// Previous arrow tooltip
		this.tooltipNext = "Next Page";				// Next arrow tooltip
		this.tooltipGo = "Go to Record Number";		// Go tooltip
		
		this.errMsgPreviousPage = "First record is already displayed.";						// Previous Page error Message
		this.errMsgNextPage = "Last record is already displayed.";							// Next Page error messange
		this.errMsgZero = "Record number should be greater than zero.";						// Lowest record error message
		this.errMsgMax = "Record number should be less than the total number of records.";	// highest record error message
		this.errMsgInRecord = "Record number # is already displayed.";						// Go error message
		this.errMsgInvalid = "Please enter a valid number";									// Invalid Numbers
		
		this.writeNavigate = writeNavigate ;		// write the Navigate control 
		this.refreshNavigate = refreshNavigate ;	// Refresh the Navigate control
		this.focus = recNavigateFocus;				// focus to text box control
		this.recNavigateClear = recNavigateClear	// Clear Goto
		
		/*
		*------------------------------------------------------------------------------------------------*
		* **************************** DONT CHANGE ANYTHING BELOW THIS LINE **************************** *
		*------------------------------------------------------------------------------------------------*
		*/
		
		//------------------------
		// Return Properties 
		this.numRecNext = 0;
		this.numRecTo = 0; 
		this.errorMsg = "";
		
		//------------------------
		window._recImgPath = "../images/";
		window._recImgPrev = "AA114_no_cache.gif";
		window._recImgNext = "AA115_no_cache.gif";
		window._recImgGo = "AA116_no_cache.gif";
		
		window._recLayer = "spnRecLayer";
		if (!window._arrRecNav) window._arrRecNav = new Array();
		window._arrRecNav[this.ID] = this;
		window._arrRecNav[window._arrRecNav.length] = this;
	}

	function writeNavigate(){
		document.writeln('<span id="' + window._recLayer  + '" style="visibility:hidden;">?<\/span>');
		var objCont = document.getElementById(window._recLayer);
		objCont.arrRecNav = new Array();
		for (var i = 0 ; i < window._arrRecNav.length ; i++){
			objCont.arrRecNav[i] = window._arrRecNav[i];
			objRec = objCont.arrRecNav[i];
			objRec.refreshNavigate();
		}
	}

	function refreshNavigate(){
		var objCont = document.getElementById(window._recLayer);
		var strRecHTMLText = "" ;
		var objRec;
		var strRecText
		var intRecTo = 0 ;
		var strLTxt = "" ;
		var strRTxt = "";
		var strPrevTabIndex = "";
		var strNextTabIndex = "";
		var strTxtTabIndex  = "";
		var strGoTabIndex   = "";

		for (var i = 0 ; i < objCont.arrRecNav.length ; i++){
			objRec = objCont.arrRecNav[i];
			if (objRec.divID == this.divID){
				objRec.numRecTotal = Number(objRec.numRecTotal);
				if (objRec.numRecTotal > 0){
					objRec.numStart = Number(objRec.numStart);
					objRec.numRecPage = Number(objRec.numRecPage);
					
					
					var strRecHTMLText = "";
					var strRecText = objRec.numStart + ' to '
					
					if (objRec.numStart == objRec.numRecTotal){
						intRecTo = objRec.numRecTotal
					}else{
						intRecTo = objRec.numStart + (objRec.numRecPage - 1) ;
						if (intRecTo > objRec.numRecTotal){
							intRecTo = objRec.numRecTotal
						}
					}
					objRec.numRecTo = intRecTo;
					

					strPrevTabIndex = "";
					strNextTabIndex = "";
					strTxtTabIndex  = "";
					strGoTabIndex   = "";

					if (objRec.tabIndexPrev != ""){strPrevTabIndex = ' tabindex = "' + objRec.tabIndexPrev + '"';}
					if (objRec.tabIndexNext != ""){strNextTabIndex = ' tabindex = "' + objRec.tabIndexNext + '"';}
					if (objRec.tabIndexNo != ""){strTxtTabIndex = ' tabindex = "' + objRec.tabIndexNo + '"';}
					if (objRec.tabIndexGo != ""){strGoTabIndex = ' tabindex = "' + objRec.tabIndexGo + '"';}

					strLTxt = "";
					strRTxt = "";
					switch (objRec.align.toUpperCase()){
						case "LEFT" : strLTxt = '<td><font>&nbsp;<\/font><\/td>'; break;
						case "RIGHT" : strRTxt = '<td><font>&nbsp;<\/font><\/td>'; break;
						default :
							strLTxt = '<td><font>&nbsp;<\/font><\/td>';
							strRTxt = '<td><font>&nbsp;<\/font><\/td>';
							break;
					}
					
					
					strRecText += objRec.numRecTo + " of " + objRec.numRecTotal;
					
					strRecHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">';
					strRecHTMLText += '<tr>';
					strRecHTMLText += strRTxt;

					switch (objRec.style){
						case 1:
							strRecHTMLText += '<td width="10" align="center"><a href="javascript:recNavigate_Click(' + "'" + objRec.divID  + "','P'" + ')" title="' + objRec.tooltipPrevious + '" ' + strPrevTabIndex + '><img src=' + window._recImgPath + window._recImgPrev + ' border="0" ><\/a><\/td>';
							strRecHTMLText += '<td width="' + (strRecText.length * 6) + '" align="center" nowrap="true"><font>' + strRecText + '<\/font><\/td>';
							strRecHTMLText += '<td width="10" align="center"><a href="javascript:recNavigate_Click(' + "'" + objRec.divID  + "','N'" + ')" title="' + objRec.tooltipNext + '" ' + strNextTabIndex + '><img src=' + window._recImgPath + window._recImgNext + ' border="0"><\/a><\/td>';
							strRecHTMLText += '<td width="50" align="center"><input type="text" id="txtRecGo_' + objRec.divID + '" size="5" maxlength="' + String(objRec.numRecTotal).length + '" onkeyup="rectxtRecGo_KeyPress(this, event,' + "'" + objRec.divID + "'" + ')" onkeypress="rectxtRecGo_KeyPress(this, event,' + "'" + objRec.divID + "'" + ')" style="text-align:right;" ' + strTxtTabIndex + '><\/td>';
							strRecHTMLText += '<td width="20" align="center"><a href="javascript:recNavigate_Click(' + "'" + objRec.divID  + "','G'" + ')" title="' + objRec.tooltipGo + '" id="lnkGo_' + objRec.divID + '" ' + strGoTabIndex + '><img src=' + window._recImgPath + window._recImgGo + ' border="0" ><\/a><\/td>';
							break;
						case 2:
							if (objRec.tooltipGo.toUpperCase() == "GO TO RECORD NUMBER"){
								objRec.tooltipGo = "Go to page number ";
							}
							strRecText = "";
							var intTotalPg = 0;
							var intStartPg = 1;
							var intCurrPg = 1 ;
							if (objRec.numStart > objRec.numRecPage){
								var strLead = String(objRec.numStart / objRec.numRecPage).split(".");
								intStartPg = Number(strLead[0]) + 1 ;
								intCurrPg = intStartPg;
							}
							
							intTotalPg = objRec.numRecTotal / objRec.numRecPage
							intTotalPg = Math.round(intTotalPg);
							if (intTotalPg * objRec.numRecPage < objRec.numRecTotal){
								intTotalPg = intTotalPg + 1
							}
							
							if (intTotalPg == 0){intTotalPg = 1;}
							if (intTotalPg < objRec.intPages){
								intStartPg = 1;
							}else{
								if ((intTotalPg - intStartPg) < (objRec.intPages - 1)){
									intStartPg = Math.abs(Math.round((objRec.intPages -1) - intTotalPg));
								}
							}
							var intCount = 1
							var intSize = ""
							for (var x = intStartPg ; x <= intTotalPg ; x++){
							
								if (x != intCurrPg){
									strRecText += '<a href="javascript:recNavigate_Click(' + "'" + objRec.divID  + "','PG','" + Number(((x * objRec.numRecPage) - objRec.numRecPage) + 1) + "'" + ')" title="' + objRec.tooltipGo + ' ' + x  + '"><u>';
								}
								strRecText +=  x ;
								if (x != intCurrPg){
									strRecText += '</u></a> ';
								}else{
									strRecText += ' ' ;
								}
								intCount ++
								
								if (intCount > objRec.intPages){
									break;
								}
								intSize += x + ' ';
							}
							
							strRecHTMLText += '<td width="8" align="center"><a href="javascript:recNavigate_Click(' + "'" + objRec.divID  + "','P'" + ')" title="' + objRec.tooltipPrevious + '" ' + strPrevTabIndex + '><img src=' + window._recImgPath + window._recImgPrev + ' border="0"><\/a><\/td>';
							strRecHTMLText += '<td width="' + (intSize.length * 7) + '" align="center" nowrap="true"><font>' + strRecText + '<\/font><\/td>';
							strRecHTMLText += '<td width="8" align="center"><a href="javascript:recNavigate_Click(' + "'" + objRec.divID  + "','N'" + ')" title="' + objRec.tooltipNext + '" ' + strNextTabIndex + '><img src=' + window._recImgPath + window._recImgNext + ' border="0"><\/a><\/td>';
							break;
					}
					strRecHTMLText += strLTxt;
					strRecHTMLText += '<\/tr>';
					strRecHTMLText += '<\/table>';
				}
				
				DivWrite(objRec.divID, strRecHTMLText);
				break;
			}
		}
	}

	function recNavigate_Click(strID, strType, intRecNo){
		var objCont = document.getElementById(window._recLayer);
		for (var i = 0 ; i < objCont.arrRecNav.length ; i++){
			var objRec = objCont.arrRecNav[i];
			if (objRec.divID == strID){
				if (objRec.onClick != ""){
					var intNextNo = 0;
					var strErrMsg = "";
					var blnContinue = true;
					
					switch (strType.toUpperCase()){
						case "P" :
							if (objRec.numStart <= 0 || objRec.numStart == 1){
								strErrMsg = objRec.errMsgPreviousPage;
							}else{
								intNextNo = objRec.numStart - objRec.numRecPage;
								if (intNextNo < 0){
									intNextNo = 1;
								}
							}
							break ;
						case "N" :
							if (objRec.numRecTo != objRec.numRecTotal){
								intNextNo = (objRec.numStart + objRec.numRecPage) ;
							}
							else{
								strErrMsg = objRec.errMsgNextPage;
							}	
							break ;
						case "G" :
							intNextNo = document.getElementById("txtRecGo_" + objRec.divID).value;
							if (intNextNo == ""){
								blnContinue = false;			
							}else{
								if (isNaN(intNextNo)){
									intNextNo = 0
									strErrMsg = objRec.errMsgInvalid;
								}else{
									if (intNextNo < 1){
										intNextNo = 0 ; 
										strErrMsg = objRec.errMsgZero;
									}else{
										if (intNextNo > objRec.numRecTotal){
											intNextNo = 0 ; 
											strErrMsg = objRec.errMsgMax;
										}else{
											if (intNextNo == objRec.numStart){
												intNextNo = 0 ; 
												strErrMsg = objRec.errMsgInRecord.replace("#", objRec.numStart);
											}
										}							
									}
								}
							}
							break ;
						case "PG" :
							intNextNo = intRecNo;
							break ;
					}
					
					if (blnContinue){
						eval(objRec.onClick)(intNextNo, strErrMsg);
					}
				}
				break ;
			}
		}
	}
	
	function recNavigateClear(){
		var objCont = document.getElementById(window._recLayer);
		for (var i = 0 ; i < objCont.arrRecNav.length ; i++){
			objRec = objCont.arrRecNav[i];
			if (objRec.divID == this.divID){
				switch (objRec.style){
					case 1:
						document.getElementById('txtRecGo_' + objRec.divID).value = "";
						break;
				}
				break;
			}
		}
	}

	function recNavigateFocus(){
		var objCont = document.getElementById(window._recLayer);
		for (var i = 0 ; i < objCont.arrRecNav.length ; i++){
			objRec = objCont.arrRecNav[i];
			if (objRec.divID == this.divID){
				switch (objRec.style){
					case 1:
						document.getElementById('txtRecGo_' + objRec.divID).focus();
						break;
				}
				break;
			}
		}
	}

	function rectxtRecGo_KeyPress(objControl, objEvent, strID ){
		var strValue = objControl.value 
		var intKC = objEvent.keyCode ;
		var blnNumber = false;
		if (intKC == 13){
			recNavigate_Click(strID, "G");
		}else{
			if (intKC >= 48 && intKC <= 57){
				blnNumber = true;
			}else{
				if (intKC == 8 || intKC == 37 || intKC == 38 || intKC == 39 || intKC == 40 || intKC == 46 || intKC == 45){
					blnNumber = true;
				}
			}
			if (!blnNumber){
				var intCount = strValue.length ;
				var strNValue = ""
				for (var i = 0 ; i < intCount ; i++){
					if (!isNaN(strValue.substr(i,1))){
						strNValue += strValue.substr(i,1);
					}
				}
				objControl.value = strNValue;
			}
		}
	}
	// -------------------------------- End of Page -----------------------------------