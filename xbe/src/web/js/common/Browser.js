	// ************************************************
	// Description : Identify the Bowser 
	// ************************************************
	var browser = new Browser();
	function Browser() {
		var ua, s, i;
		this.isIE    = false;
		this.isNS    = false;
		this.isFF    = false;
		this.isGC    = false;
		this.version = null;
		this.subversions = null;

		ua = navigator.userAgent;
		 
		s = "MSIE";
		if ((i = ua.indexOf(s)) >= 0) {
			this.isIE = true;
			this.version = parseFloat(ua.substr(i + s.length));
			return;
		}
		
		s = "Trident/";
		if ((i = ua.indexOf(s)) >= 0) {
			this.isIE = true;
			this.version = parseFloat(ua.substr(i + s.length));
			return;
		}

		s = "Netscape6/";
		if ((i = ua.indexOf(s)) >= 0) {
			this.isNS = true;
			this.version = parseFloat(ua.substr(i + s.length));
			return;
		}

		s = "Firefox/";
		if ((i = ua.indexOf(s)) >= 0) {
			this.isFF = true;			
			this.version = parseFloat(ua.substr(i + s.length));
			this.subversions = ua.substr(i + s.length).split('.');
			return;
		}

		s = "Chrome/";
		if ((i = ua.indexOf(s)) >= 0) {
			this.isGC = true;
			this.version = 6.1;
			return;
		}
		// Treat any other "Gecko" browser as NS 6.1.
		s = "Gecko";
		if ((i = ua.indexOf(s)) >= 0) {
			this.isNS = true;
			this.version = 6.1;
			return;
		}
		
		
		
	}
	// ----------------------------------- End of Browser ----------------------------------------

if(window.navigator.userAgent.indexOf("MSIE")==-1){
	HTMLElement.prototype.__defineGetter__("innerText",function(){return(this.textContent);});
	HTMLElement.prototype.__defineSetter__("innerText",function(txt){this.textContent=txt;});

	Event.prototype.__defineGetter__("srcElement",getTarget);
}

function getTarget(){
	 try{
		var node=this.target;
		while(node.nodeType!=node.ELEMENT_NODE){node=node.parentNode;}
	 }catch(e){ return node; }
	return node;
}

