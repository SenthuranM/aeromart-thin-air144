		//use this function to validate the credit card numbers. This function returns a boolean value
		//depending on the validation. supports Amex, Dinners, Master and Visa.
		function ValidateCardNos(strCardType, strCardNo){
			var isValid = false;	
			var intStart ;
			switch (trim(strCardType.toUpperCase())){
				case "3" : // AMEX
					if(strCardNo.length == 15){
						intStart = strCardNo.charAt(0);
						intStart = intStart +  strCardNo.charAt(1);
						if((intStart == 37) || (intStart == 34)){						
							isValid = luhnFormula(strCardNo);
						}
					}
					break;
			
				case "4" : // DINNERS
					if(strCardNo.length == 14){
						intStart = strCardNo.charAt(0);
						intStart = intStart + strCardNo.charAt(1);
						if((intStart == 36) || (intStart == 38)){						
							isValid = luhnFormula(strCardNo);
						}else{
							intStart = intStart +  strCardNo.charAt(2);
							if((intStart == 300) || (intStart == 301) || (intStart == 302) || (intStart == 303) || (intStart == 304) || (intStart == 305)){
								isValid = luhnFormula(strCardNo);
							}
						}
					}				
					break;
					
				case "1" :	// MASTER
					if(strCardNo.length == 16){
						intStart = strCardNo.charAt(0);
						intStart = intStart + strCardNo.charAt(1);
						if((intStart == 51) || (intStart == 52) || (intStart == 53) || (intStart == 54) || (intStart == 55))
						{						
							isValid = luhnFormula(strCardNo);
						}
					}				
					break;
				
				case "2" : // VISA
					if((strCardNo.length == 16) || (strCardNo.toString().length == 13)){
						intStart = strCardNo.toString().charAt(0);
						if(intStart == 4){						
							isValid = luhnFormula(strCardNo);
						}
					}				
					break;
					
				case "5" : // CB
					if((strCardNo.length >= 13 && strCardNo.length <= 19)){
						isValid = cbValidation(strCardNo);
					}
					break;
			}
			
			// validation using Luhn's Formula
			function luhnFormula(strCardNo){
				var ar = new Array(strCardNo.length );
				var i = 0,sum = 0;

    			for( i = 0; i < strCardNo.length; ++i ) {
    				ar[i] = parseInt(strCardNo.charAt(i));
    			}
    			for( i = ar.length -2; i >= 0; i-=2 ) {  // you have to start from the right, and work back.
    				ar[i] *= 2;							 // every second digit starting with the right most (check digit)
    				if( ar[i] > 9 ) ar[i]-=9;			 // will be doubled, and summed with the skipped digits.
    			}										 // if the double digit is > 9, ADD those individual digits together 

        		for( i = 0; i < ar.length; ++i ) {
        			sum += ar[i];						 // if the sum is divisible by 10 mod10 succeeds
        		}
        		return (((sum%10)==0)?true:false);	
			}
			//----------------------------------------------------
		
			return isValid;						
		}
		
		function trim(strText) { 
			// this will get rid of leading spaces 
			strText = strText.toString();
			while (strText.substring(0,1) == ' ') 
				strText = strText.substring(1, strText.length);
			// this will get rid of trailing spaces 
			while (strText.substring(strText.length-1,strText.length) == ' ')
				strText = strText.substring(0, strText.length-1);
			return strText;
		} 
		
		// CB card validation
		function cbValidation(strCardNo){
			var isNotDigit = true;
			for( var i = 0; i < strCardNo.length; i++ ) {
				
				if (strCardNo.charAt(i) == ' ') { 
					isNotDigit = true;
				} else {
					isNotDigit = isNaN(strCardNo.charAt(i));
				}
				
				if(isNotDigit) {
					return false;
				}
			}
    		return true;	
		}
