
$(document).ready(function(){

    $( "#birthDate" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      buttonImageOnly: true,
	      buttonText: "Select date"
	});
    
    $("#btnSearch").click(function(){searchCustomer();});
	createGrid();
	
});

function createGrid(){
	var data = {};
	data['searchEmail'] = $("#searchEmailAddress").val();
	var customerGrid = $("#customerDetailsGrid").jqGrid({
		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'searchCustomer.action',
			           data: $.extend(postdata, data),
			           dataType:"json",
			           beforeSend :ShowPopProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(customerGrid[0], "gridData", mydata.customerList);
			            		  customerGrid[0].addJSONData(mydata);				            	
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  HidePopProgress();
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames: ['Title', 'First Name', 'Last Name', 'Email', 'Mobile Number', 'Nationality', 'Country of Residence', 'Zip Code', 'Number During Traval', 'Fax Number', 'Date of Birth', 'Communication Language', 'Passport Number', 'Reffering Email', 'Family Head Email'],
		    colModel: [
			    {name: 'title',index: 'title',jsonmap:'customerDTO.title', width: 30},
			    {name: 'firstName', index: 'firstName',jsonmap:'customerDTO.firstName', width: 140, sorttype: "string"},
			    {name: 'lastName',index: 'lastName',jsonmap:'customerDTO.lastName', width: 140, sorttype: "string"},
			    {name: 'email', index: 'email',jsonmap:'customerDTO.emailId' ,width: 140},
			    {name: 'mobile',index: 'mobile', jsonmap:'customerDTO.mobile', width: 90, sorttype: "string"},
			    {name: 'nationalityCode',index: 'nationalityCode', jsonmap:'customerDTO.nationalityCode', width: 90},
				{name: 'countryCode',index: 'countryCode', jsonmap:'customerDTO.countryCode', width: 90},
				{name: 'zipCode',index: 'zipCode', jsonmap:'customerDTO.zipCode', hidden: true},
			    {name: 'telephone',index: 'telephone', jsonmap:'customerDTO.telephone', hidden: true},
			    {name: 'fax',index: 'fax', jsonmap:'customerDTO.fax', hidden: true},
				{name: 'dateOfBirth',index: 'dateOfBirth', jsonmap:'customerDTO.lmsMemberDTO.dateOfBirth', hidden: true},
			    {name: 'language',index: 'language', jsonmap:'customerDTO.lmsMemberDTO.language', hidden: true}, 
			    {name: 'passportNum',index: 'passportNum', jsonmap:'customerDTO.lmsMemberDTO.passportNum', hidden: true},
			    {name: 'refferedEmail',index: 'refferedEmail', jsonmap:'customerDTO.lmsMemberDTO.refferedEmail', hidden: true},
			    {name: 'headOFEmailId',index: 'headOFEmailId', jsonmap:'customerDTO.lmsMemberDTO.headOFEmailId', hidden: true},
		    ],
		    caption: "Customer Details",
		    rowNum : 20,
			viewRecords : true,
			width : 920,
			pager: jQuery('#customerDetailsPages'),
			jsonReader: { 
				root: "customerList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid("#customerDetailsPages",{refresh: true, edit: false, add: false, del: false, search: false});
}

searchCustomer = function (){
	createGrid();
}

searchCustomerSuccess = function(response){
	var resp = response;
}

ShowPopProgress = function() {
	setVisible("divLoadMsg", true);
}

HidePopProgress = function() {
	setVisible("divLoadMsg", false);
}


