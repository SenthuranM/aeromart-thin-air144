/*
*	@author suneth
*
*/

var operation;

$(document).ready(function(){

	$(function() {
	    $( "#dateOfBirth" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      yearRange: "-99:+0",
	      maxDate: "-2Y",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      changeMonth: true,
	      changeYear: true
	    });
	    
	    $( "#dateOfBirth" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	    
	  });
	
	$(function() {
	    $( "#passportExpDate" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      yearRange: "0:+20",
	      minDate: "0",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      changeMonth: true,
	      changeYear: true
	    });
	    
	    $( "#passportExpDate" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	    
	  });
	
	$("#btnSearch").click(function(){frequentCustomerSearch();});
	$("#btnAdd").click(function(){addFrequentCustomer();});
	$("#btnEdit").click(function(){editFrequentCustomer();});
	$("#btnDelete").click(function(){deleteFrequentCustomer();});
	
	$("#btnSave").click(function(){saveFrequentCustomer();});
	$("#btnClose").click(function(){top.LoadHome();});
	
	$("#dateOfBirth").blur(function(){dateOnBlur(0)});
	$("#passportExpDate").blur(function(){dateOnBlur(1)});
	
	$("#firstName").keyup(function(event) {onlyAlphaWhiteSpace(this);});
	$("#lastName").keyup(function(event) {onlyAlphaWhiteSpace(this);});
	
	$("#search_firstName").keyup(function(event) {onlyAlphaWhiteSpace(this);});
	$("#search_lastName").keyup(function(event) {onlyAlphaWhiteSpace(this);});
	
	$("#paxType").change(function(){paxTypeOnChange();});
	
	$('#search_firstName').keypress(function (e) {
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
			 frequentCustomerSearch();
		  }
		});
	
	$('#search_lastName').keypress(function (e) {
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
			 frequentCustomerSearch();
		  }
		});
	
	$('#search_email').keypress(function (e) {
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
			 frequentCustomerSearch();
		  }
		});
	
	$.each(top.arrTitle ,function(key, value) 
			{
			    $("#title").append('<option value=' + value[0] + '>' + value[1] + '</option>');
			});
	
	clearFields();
	disableFields();
	createGrid();
	
});

createGrid = function(){

	clearFields();
	
	var nationalityFomatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			for(var i=0; i<top.arrNationality.length; i++) {
				if(top.arrNationality[i][0]==cellVal){
					treturn = top.arrNationality[i][1];
					break
				}
			}
		}
		return treturn;
	}	
	
	var dateFormatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			treturn = $.datepicker.formatDate('dd-M-yy', new Date(cellVal.split('T')[0]));
		}
		return treturn;
	}
	
	$("#frequentCustomerProfilesGridContainer").empty().append('<table id="frequentCustomerProfilesGrid"></table><div id="frequentCustomerProfilesPages"></div>')
	var data = {};
	
	data['agentFrequentCustomer.firstName'] = $("#search_firstName").val();
	data['agentFrequentCustomer.lastName'] = $("#search_lastName").val();
	data['agentFrequentCustomer.email'] = $("#search_email").val();
	
	var frequentCustomerProfilesGrid = $("#frequentCustomerProfilesGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'agentFrequentCustomer!searchFrequentCustomer.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :ShowPopProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  responseData = eval("("+jsonData.responseText+")");
			            	  if (responseData.msgType != "Error"){
			            		  $.data(frequentCustomerProfilesGrid[0], "gridData", responseData.agentFrequentCustomerList);
			            		  frequentCustomerProfilesGrid[0].addJSONData(responseData);
			            	  }else{
			            		  alert(responseData.message);
			            	  }
			            	  HidePopProgress();
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames: ['Pax Type' , 'Title', 'First Name', 'Last Name', 'Date of Birth', 'Nationality', 'Email', 'passport Number', 'passport Expiry Date', 'frequentCustomerId', 'nationalityCode'],
		    colModel: [
		        {name: 'paxType',index: 'paxType',jsonmap:'agentFrequentCustomerDTO.paxType', width: 60},
			    {name: 'title',index: 'title',jsonmap:'agentFrequentCustomerDTO.title', width: 40},
			    {name: 'firstName', index: 'firstName',jsonmap:'agentFrequentCustomerDTO.firstName', width: 130, sorttype: "string"},
			    {name: 'lastName',index: 'lastName',jsonmap:'agentFrequentCustomerDTO.lastName', width: 130, sorttype: "string"},
			    {name: 'dateOfBirth',index: 'dateOfBirth', jsonmap:'agentFrequentCustomerDTO.dateOfBirth', width: 85, formatter:dateFormatter},
			    {name: 'nationality',index: 'nationality', jsonmap:'agentFrequentCustomerDTO.nationalityCode', width: 110, formatter:nationalityFomatter},
			    {name: 'email', index: 'email',jsonmap:'agentFrequentCustomerDTO.email' ,width: 140},
			    {name: 'passportNumber',index: 'passportNumber', jsonmap:'agentFrequentCustomerDTO.passportNumber', width: 120},
				{name: 'passportExpDate',index: 'passportExpDate', jsonmap:'agentFrequentCustomerDTO.passportExpiryDate', width: 130, formatter:dateFormatter},
				{name: 'frequentCustomerId',index: 'frequentCustomerId', jsonmap:'agentFrequentCustomerDTO.frequentCustomerId', hidden: true},
				{name: 'nationalityCode',index: 'nationalityCode', jsonmap:'agentFrequentCustomerDTO.nationalityCode', hidden: true},
		    ],
		    caption: "Frequnet Customer Details",
		    rowNum : 20,
			viewRecords : true,
			width : 970,
			pager: jQuery('#frequentCustomerProfilesPages'),
			jsonReader: { 
				root: "agentFrequentCustomerList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid("#frequentCustomerProfilesPages",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$("#frequentCustomerProfilesGrid").click(function(){ frequentCustomerProfilesGridOnClick(); });
}	

frequentCustomerProfilesGridOnClick = function(){
	
	clearFields();
	disableFields();
	
	var rowId =$("#frequentCustomerProfilesGrid").getGridParam('selrow');  
	var rowData = $("#frequentCustomerProfilesGrid").getRowData(rowId);
	
	$("#paxType").val(rowData['paxType']);
	$("#title").val(rowData['title']);
	$("#firstName").val(rowData['firstName']);
	$("#lastName").val(rowData['lastName']);
	$("#email").val(rowData['email']);
	$("#passportNumber").val(rowData['passportNumber']);	
	$("#frequentCustomerId").val(rowData['frequentCustomerId']);
	$("#nationality").val(rowData['nationalityCode']); 
	
	var dob = $.datepicker.parseDate("dd-M-yy",  rowData['dateOfBirth']);
	$('#dateOfBirth').datepicker("setDate", dob);

	var passportExpDate = $.datepicker.parseDate("dd-M-yy",  rowData['passportExpDate']);
	$('#passportExpDate').datepicker("setDate", passportExpDate);
	
}

frequentCustomerSearch = function(){
	
	createGrid();
	
}

addFrequentCustomer = function(){
	
	clearFields();
	enableFields();
	operation = "save";
		
}

saveFrequentCustomer = function(){
	
	var rowId;
	var strUrl;
	
	if(!validate()){
		return false;
	}
	
	data = {};
	
	data['agentFrequentCustomer.paxType'] = $("#paxType").val();
	data['agentFrequentCustomer.title'] = $("#title").val();
	data['agentFrequentCustomer.firstName'] = $("#firstName").val();
	data['agentFrequentCustomer.lastName'] = $("#lastName").val();
	data['agentFrequentCustomer.email'] = $("#email").val();
	data['dateOfBirth'] = $("#dateOfBirth").val();
	data['passportExpiryDate'] = $("#passportExpDate").val();
	data['agentFrequentCustomer.nationalityCode'] = $("#nationality").val();
	data['agentFrequentCustomer.passportNumber'] = $("#passportNumber").val();
	data['agentFrequentCustomer.frequentCustomerId'] = ($("#frequentCustomerId").val() == "") ? null : $("#frequentCustomerId").val();
	data['operation'] = operation;
	
	$.ajax({
        url : 'agentFrequentCustomer!saveOrUpdateFrequentCustomer.action',
        data: data,
        dataType:"json",
        beforeSend :ShowPopProgress(),
        type : "POST",		           
		complete: function(response){	 
			responseData = eval("("+response.responseText+")");
			HidePopProgress();
			if(responseData.success == true){
				showConfirmation(responseData.message);
			}else{
				showERRMessage(responseData.message);
			}
			clearFields();
        }
     });
	
	if(data['agentFrequentCustomer.frequentCustomerId'] != null){
		rowId =$("#frequentCustomerProfilesGrid").getGridParam('selrow');
		createGrid();
		setTimeout(
				function(){
					$("#frequentCustomerProfilesGrid").setSelection(rowId, true);
				}, 100);
	}
	
}

editFrequentCustomer = function(){
	enableFields();
	paxTypeOnChange();
	operation = "update";
}

deleteFrequentCustomer = function(){
	
	if (confirm("You are going to delete the record. Are you sure?")) {
		data = {};
		
		data['agentFrequentCustomer.paxType'] = $("#paxType").val();
		data['agentFrequentCustomer.title'] = $("#title").val();
		data['agentFrequentCustomer.firstName'] = $("#firstName").val();
		data['agentFrequentCustomer.lastName'] = $("#lastName").val();
		data['agentFrequentCustomer.email'] = $("#email").val();
		data['dateOfBirth'] = $("#dateOfBirth").val();
		data['passportExpiryDate'] = $("#passportExpDate").val();
		data['agentFrequentCustomer.nationalityCode'] = $("#nationality").val();
		data['agentFrequentCustomer.passportNumber'] = $("#passportNumber").val();
		data['agentFrequentCustomer.frequentCustomerId'] = ($("#frequentCustomerId").val() == "") ? null : $("#frequentCustomerId").val();
		
		$.ajax({
	        url : 'agentFrequentCustomer!deleteFrequentCustomer.action',
	        data: data,
	        dataType:"json",
	        beforeSend :ShowPopProgress(),
	        type : "POST",
	        complete: function(response){	 
				responseData = eval("("+response.responseText+")");
				HidePopProgress();
				if(responseData.success == true){
					showConfirmation(responseData.message);
				}else{
					showERRMessage(responseData.message);
				}
				clearFields();
	        }
	     });
		
		createGrid();
    }
	
}

validate = function(){
	
	if($("#paxType").val() != "IN"){
	
		jsonCustomerObj = [{
			id : "#title",
			desc : "Title",
			required : true
		}, {
			id : "#email",
			desc : "Email Address",
			required : true 
		}];
		
		if (!UI_commonSystem.validateMandatory(jsonCustomerObj)) {
			return false;
		}
		
		if (!checkEmail($("#email").val())) {
			showERRMessage(raiseError("XBE-ERR-04", "Email address"));
			$("#email").focus();
			return false;
		}
		
	}
	
	jsonCustomerObj = [{
		id : "#firstName",
		desc : "First Name",
		required : true
	}, {
		id : "#lastName",
		desc : "Last Name",
		required : true
	}, {
		id : "#nationality",
		desc : "Nationaliy Of The Customer",
		required : true
	}, {
		id : "#paxType",
		desc : "Pax Type",
		required : true 
	}, {
		id : "#dateOfBirth",
		desc : "Date of Birth",
		required : true 
	}];
	
	if (!UI_commonSystem.validateMandatory(jsonCustomerObj)) {
		return false;
	}
	
	if($.trim($("#dateOfBirth").val()) != ""){
		if(!dateValidDate($("#dateOfBirth").val())){
			showERRMessage(raiseError("XBE-ERR-04", "Birthday"));
			$("#dateOfBirth").focus();
			return false;
		}
	}
	
	if($.trim($("#passportExpDate").val()) != ""){
		
		if($.trim($("#passportNumber").val()) == ""){
			showERRMessage("Passport expiry date can't be added without the passport number");
			$("#passportNumber").focus();
			return false;
		}
		
		if(!dateValidDate($("#passportExpDate").val())){
			showERRMessage(raiseError("XBE-ERR-04", "Passport Expiry Date"));
			$("#passportExpDate").focus();
			return false;
		}
		
	}
	
	return true;
}

disableFields = function(){
	$("#paxType").prop('disabled', true);
	$("#title").prop('disabled', true);
	$("#firstName").prop('disabled', true);
	$("#lastName").prop('disabled', true);
	$("#email").prop('disabled', true);
	$("#dateOfBirth").prop('disabled', true);
	$("#passportExpDate").prop('disabled', true);
	$("#nationality").prop('disabled', true);
	$("#passportNumber").prop('disabled', true);
	$("#btnSave").prop('disabled', true);
	
	$("#paxType").addClass('aa-input');
	$("#title").addClass('aa-input');
	$("#firstName").addClass('aa-input');
	$("#lastName").addClass('aa-input');
	$("#email").addClass('aa-input');
	$("#dateOfBirth").addClass('aa-input');
	$("#passportExpDate").addClass('aa-input');
	$("#nationality").addClass('aa-input');
	$("#passportNumber").addClass('aa-input');
	$("#btnSave").addClass('aa-input');
}

enableFields = function(){
	$("#paxType").prop('disabled', false);
	$("#title").prop('disabled', false);
	$("#firstName").prop('disabled', false);
	$("#lastName").prop('disabled', false);
	$("#email").prop('disabled', false);
	$("#dateOfBirth").prop('disabled', false);
	$("#passportExpDate").prop('disabled', false);
	$("#nationality").prop('disabled', false);
	$("#passportNumber").prop('disabled', false);
	$("#btnSave").prop('disabled', false);
	
	$("#paxType").removeClass('aa-input');
	$("#title").removeClass('aa-input');
	$("#firstName").removeClass('aa-input');
	$("#lastName").removeClass('aa-input');
	$("#email").removeClass('aa-input');
	$("#dateOfBirth").removeClass('aa-input');
	$("#passportExpDate").removeClass('aa-input');
	$("#nationality").removeClass('aa-input');
	$("#passportNumber").removeClass('aa-input');
	$("#btnSave").removeClass('aa-input');
}

clearFields = function(){
	$("#paxType").val("");
	$("#title").val("");
	$("#firstName").val("");
	$("#lastName").val("");
	$("#email").val("");
	$("#dateOfBirth").val("");
	$("#passportExpDate").val("");
	$("#nationality").val("");
	$("#passportNumber").val("");
	$("#frequentCustomerId").val("");
}

function ShowPopProgress() {
	setVisible("divLoadMsg", true);
}

function HidePopProgress() {
	setVisible("divLoadMsg", false);
}

dateOnBlur = function(id){
	if(id == 0){
		dateChk("dateOfBirth");
	}else if(id == 1){
		dateChk("passportExpDate");
	}
}

onlyAlphaWhiteSpace = function(objC){
	
	if (!isAlphaWhiteSpace(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}
	
}

paxTypeOnChange = function(){
	
	if($("#paxType").val() == "IN"){
		
		$("#title").prop('disabled', true);
		$("#email").prop('disabled', true);
		$("#passportExpDate").datepicker().datepicker('disable');
		$("#passportNumber").prop('disabled', true);
		$("#emailMandatory").hide();
		$("#titleMandatory").hide();
		$('#dateOfBirth').datepicker('option', 'minDate', "-2Y");
		$('#dateOfBirth').datepicker('option', 'maxDate', "-1D");
		
		$("#title").addClass('aa-input');
		$("#email").addClass('aa-input');
		$("#passportExpDate").addClass('aa-input');
		$("#passportNumber").addClass('aa-input');
		
	}else{
		
		$("#title").prop('disabled', false);
		$("#email").prop('disabled', false);
		$("#passportExpDate").prop('disabled', false);
		$("#passportExpDate").datepicker().datepicker('enable');
		$("#passportNumber").prop('disabled', false);
		$("#emailMandatory").show();
		$("#titleMandatory").show();
		
		$("#title").removeClass('aa-input');
		$("#email").removeClass('aa-input');
		$("#passportExpDate").removeClass('aa-input');
		$("#passportNumber").removeClass('aa-input');
		
		if($("#paxType").val() == "CH"){
			$('#dateOfBirth').datepicker('option', 'minDate', "-12Y");
			$('#dateOfBirth').datepicker('option', 'maxDate', "-2Y");
		}else if($("#paxType").val() == "AD"){
			$('#dateOfBirth').datepicker('option', 'minDate', "-99Y");
			$('#dateOfBirth').datepicker('option', 'maxDate', "-2Y");
		}else{
			$('#dateOfBirth').datepicker('option', 'minDate', "-99Y");
			$('#dateOfBirth').datepicker('option', 'maxDate', "-2Y");
		}
		
	}
	
}