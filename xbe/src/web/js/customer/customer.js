var countryObj = top.arrCountry;
var nationalityObj = top.arrNationality;
var arrError = new Array();
var loyaltyManagmentEnabled = true;
var regPrivilages = {};
var configList = {};
var isRegisterNewCustomer = true;

$(document).ready(function(){

	$(function() {
	    $( "#birthDate" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      yearRange: "-99:+0",
	      maxDate: "-2Y",
	      minDate: "-99Y",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      changeMonth: true,
	      changeYear: true
	    });
	    
	    $( "#birthDate" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	    
	  });

	$("#txtMobiCntry").keyup(function(event) {UI_customerValidation.onlyNumeric(this);});
	$("#txtMobiNo").keyup(function(event) {UI_customerValidation.onlyNumeric(this);});
	$("#txtTravCntry").keyup(function(event) {UI_customerValidation.onlyNumeric(this);});
	$("#txtTravNo").keyup(function(event) {UI_customerValidation.onlyNumeric(this);});
	$("#txtFaxCntry").keyup(function(event) {UI_customerValidation.onlyNumeric(this);});
	$("#txtFaxNo").keyup(function(event) {UI_customerValidation.onlyNumeric(this);});
	
	$("#firstName").keyup(function(event) {UI_customerValidation.onlyAlphaWhiteSpace(this);});
	$("#lastName").keyup(function(event) {UI_customerValidation.onlyAlphaWhiteSpace(this);});
	
	$("#residence").change(function(){UI_customerValidation.setCountryPhone();});
	$("#joinAirewards").change(function(){joinAirewards(this);});
	
	$( "#tabs" ).tabs( { disabled:[1] } );
	
	$("#btnSave").hide();
	$("#btnRegister").hide();
	
	$('#tabs-1').find('input,button, select').prop('disabled', true);
	$('#tabs-2').find('input,button, select').prop('disabled', true);

	$("#buttonSearch").click(function(){searchCustomer();});
	$("#btnRegister").click(function(){registerNewCustomerTest();});
	
	$("#btnSave").click(function(){updateCustomerTest();});
	$("#btnClose").click(function(){top.LoadHome();});
	$("#btnReset").click(function(){resetOnClick()});
	$("#lblLmsResend").click(function(){resendLmsConfirmation();});
	$("#lblPassResend").click(function(){resendProfileDetailsMail();});
	$("#btnNameUpdateOk").click(function(){
		$("#divCustNameUpdateConfirm").hide();
		UI_commonSystem.hideProgress();
		updateCustomer();
	});
	$("#btnNameUpdateNo").click(function(){
		$("#divCustNameUpdateConfirm").hide();
		UI_commonSystem.hideProgress();
	});
	
	$("#btnAdd").click(function(){ addOnClick(); });
	
	$("#btnEdit").click(function(){ editOnClick(); });
	
	loadCustomerPageDetails();
	createGrid();
	
	$("#displayTitle").fillDropDown({
		dataArray : arrPaxTitle,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : true
	});
	$("#selEmgnTitle").fillDropDown({
		dataArray : arrPaxTitle,
		keyIndex : 0,
		valueIndex : 1,
		firstEmpty : true
	});
	
	
	$("#customerDetailsGrid").click(function(){ customerDetailsGridOnClick(); });
	
	// Enter Key press event for Edit Email Address field
	$('#searchEmailAddress').keypress(function (e) {
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
			 searchCustomer();
		  }
		});
	
	$("#wrongNamePopupOk").click(function(){
		
		$("#customerContainer").children().prop('disabled',false);
		$("#lms-acount-exists-wrong-names").hide();
		
	});
	
	$("#mergePopupOk").click(function(){
		
		if(isRegisterNewCustomer){
			
			registerNewCustomer();
			
		}else{
			
			updateCustomer();
			
		}
		
		$("#customerContainer").children().prop('disabled',false);
		$("#lms-acount-exists-merge").hide();
		
		
	});
	
	$("#mergePopupClose").click(function(){
		
		$("#customerContainer").children().prop('disabled',false);
		$("#lms-acount-exists-merge").hide();
		
	});
	
});

joinAirewards = function(objC){	
	if($(objC).is(":checked")){		
		$("#tabs").tabs( "enable" , 1 ); 		
	}else{		
		$("#tabs").tabs( "disable" , 1 ); 		
	}	
}

createGrid = function(){
	
	var customerDTOJsonToString = function(cellVal, options, rowObject){
		
		return JSON.stringify(cellVal);
		
	}
	
	var countryFomatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			for(var i=0; i<top.arrCountry.length; i++) {
				if(top.arrCountry[i][0]==cellVal){
					treturn = top.arrCountry[i][1];
					break;
				}
			}
		}
		return treturn;
	}
	
	var nationalityFomatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			for(var i=0; i<top.arrNationality.length; i++) {
				if(top.arrNationality[i][0]==cellVal){
					treturn = top.arrNationality[i][1];
					break
				}
			}
		}
		return treturn;
	}	
	
	$("#customerDetailsGridContainer").empty().append('<table id="customerDetailsGrid"></table><div id="customerDetailsPages"></div>')
	var data = {};
	data['searchEmail'] = $("#searchEmailAddress").val();
	var customerGrid = $("#customerDetailsGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'searchCustomer.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :ShowPopProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(customerGrid[0], "gridData", mydata.customerList);
			            		  customerGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  HidePopProgress();
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames: ['Title', 'First Name', 'Last Name', 'Email', 'Mobile Number', 'Nationality', 'Country of Residence', 'Zip Code', 'Number During Traval', 'Fax Number', 'Date of Birth', 'Communication Language', 'Passport Number', 'Reffering Email', 'Family Head Email' , 'isLmsMember' ,'sendPromoEmail','customerDTO'],
		    colModel: [
			    {name: 'title',index: 'title',jsonmap:'customerDTO.title', width: 30},
			    {name: 'firstName', index: 'firstName',jsonmap:'customerDTO.firstName', width: 110, sorttype: "string"},
			    {name: 'lastName',index: 'lastName',jsonmap:'customerDTO.lastName', width: 100, sorttype: "string"},
			    {name: 'email', index: 'email',jsonmap:'customerDTO.emailId' ,width: 140},
			    {name: 'mobile',index: 'mobile', jsonmap:'customerDTO.mobile', width: 100, sorttype: "string"},
			    {name: 'nationalityCode',index: 'nationalityCode', jsonmap:'customerDTO.nationalityCode', width: 100, formatter:nationalityFomatter},
				{name: 'countryCode',index: 'countryCode', jsonmap:'customerDTO.countryCode', width: 120, formatter:countryFomatter},
				{name: 'zipCode',index: 'zipCode', jsonmap:'customerDTO.zipCode', hidden: true},
			    {name: 'telephone',index: 'telephone', jsonmap:'customerDTO.telephone', hidden: true},
			    {name: 'fax',index: 'fax', jsonmap:'customerDTO.fax', hidden: true},
				{name: 'dateOfBirth',index: 'dateOfBirth', jsonmap:'customerDTO.lmsMemberDTO.dateOfBirth', hidden: true},
			    {name: 'language',index: 'language', jsonmap:'customerDTO.lmsMemberDTO.language', hidden: true}, 
			    {name: 'passportNum',index: 'passportNum', jsonmap:'customerDTO.lmsMemberDTO.passportNum', hidden: true},
			    {name: 'refferedEmail',index: 'refferedEmail', jsonmap:'customerDTO.lmsMemberDTO.refferedEmail', hidden: true},
			    {name: 'headOFEmailId',index: 'headOFEmailId', jsonmap:'customerDTO.lmsMemberDTO.headOFEmailId', hidden: true},
			    {name: 'isLmsMember',index:'isLmsMember', jsonmap:'customerDTO.isLmsMember', hidden:true},
			    {name: 'sendPromoEmail',index:'sendPromoEmail', jsonmap:'customerDTO.sendPromoEmail', hidden:true},
			    {name: 'customerDTO',index:'customerDTO', jsonmap:'customerDTO' , formatter:customerDTOJsonToString ,hidden:true},
			    
		    ],
		    caption: "Customer Details",
		    rowNum : 20,
			viewRecords : true,
			width : 920,
			pager: jQuery('#customerDetailsPages'),
			jsonReader: { 
				root: "customerList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid("#customerDetailsPages",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$("#customerDetailsGrid").click(function(){ customerDetailsGridOnClick(); });
}	

registerNewCustomer = function(){
	
		var customerData = getCustomerData();
		
		$.ajax({
			
	           url : 'registerCustomer.action',
	           data: customerData,
	           dataType:"json",
	           beforeSend :ShowPopProgress(),
	           type : "POST",		           
			   success: function(response){

				   clearFields();
				   HidePopProgress();
				   showConfirmation(arrError.registerSuccessfull);
				   
			   },
			   error: function(response){
				   
				   HidePopProgress();
				   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
				   
			   }
		
	        });
		
		return true;
		
				
}

registerNewCustomerTest = function(){
	
	if(!regPrivilages.addCustomer){
		
		return
		
	}
	
	if(!UI_customerValidation.validate()){
		
		return;
		
	}
	
	if(UI_customerValidation.lmsValidationData != null && UI_customerValidation.lmsNameMismatch){

		$("#customerContainer").children().prop('disabled',true);
		$("#lms-acount-exists-wrong-names").show();
		
		return;
		
	}
	
	if(UI_customerValidation.lmsValidationData != null && !UI_customerValidation.accountExists){
		
		if(UI_customerValidation.lmsValidationData.emailId != null && UI_customerValidation.lmsValidationData.emailId == 'Y'){
			
			isRegisterNewCustomer = true;
			$("#customerContainer").children().prop('disabled',true);
	   		$("#lms-acount-exists-merge").show();
	   		
		}else{
			
			registerNewCustomer();
			
		}
		
		
	}else{
		
		registerNewCustomer();
		
	}
	
}

searchCustomer = function (){
	clearFields();
	createGrid();
}

searchCustomerSuccess = function(response){
	var resp = response;
}

ShowPopProgress = function() {
	setVisible("divLoadMsg", true);
}

HidePopProgress = function() {
	setVisible("divLoadMsg", false);
}

isNameSoundxChanged = function() {
	var rowId =$("#customerDetailsGrid").getGridParam('selrow');  
	var rowData = $("#customerDetailsGrid").getRowData(rowId);
	var customerDTO = JSON.parse(rowData['customerDTO']);
	return !UI_commonSystem.nameCompare(rowData['firstName'].toUpperCase(), rowData['lastName'].toUpperCase(), $("#firstName").val().toUpperCase(), $("#lastName").val().toUpperCase());
}

populateCustomerDetails = function(rowData){
	
	var customerDTO = JSON.parse(rowData['customerDTO']);
	$("#tdPassResend").show();
	$("#editEmailAddress").val(rowData['email']);
	$("#displayTitle").val(rowData['title']);
	$("#firstName").val(rowData['firstName']);
	$("#lastName").val(rowData['lastName']);
	$("#zipCode").val(rowData['zipCode']);
	$('input[name=\'customer.gender\'][value="' + customerDTO.gender + '"]').prop('checked', true);
	$("#residence").val(customerDTO.countryCode);
	$("#nationality").val(customerDTO.nationalityCode);

	UI_customerValidation.setCountryPhone();
	
	
	var mobile = splitPhoneNumber(rowData['mobile']);

	if (mobile.length > 0){
		$("#txtMobiCntry").val(mobile[0]);
	}

	if (mobile.length > 1){
		$("#txtMobiArea").val(mobile[1]);
	}
	
	if (mobile.length > 2){
		$("#txtMobiNo").val(mobile[2]);
	}
	
	
	
	var faxNo = splitPhoneNumber(rowData['fax']);
	
	if (faxNo.length > 0){
		$("#txtFaxCntry").val(faxNo[0]);
	}
	
	if (faxNo.length > 1){
		$("#txtFaxArea").val(faxNo[1]);
	}
	if (faxNo.length > 2){
		$("#txtFaxNo").val(faxNo[2]);
	}
	
	
	
	var officeTelephone = splitPhoneNumber(customerDTO.officeTelephone);
	
	if (officeTelephone.length > 0){
		$("#txtOCountry").val(officeTelephone[0]);
	}

	if (officeTelephone.length > 1){
		$("#txtOArea").val(officeTelephone[1]);
	}
	if (officeTelephone.length > 2){
		$("#txtOfficeTel").val(officeTelephone[2]);
	}
	

	
	var travNo = splitPhoneNumber(rowData['telephone']);
	
	if (travNo.length > 0){
		$("#txtTravCntry").val(travNo[0]);
	}
	
	if (travNo.length > 1){
		$("#txtTravArea").val(travNo[1]);
	}
	if (travNo.length > 2){
		$("#txtTravNo").val(travNo[2]);
	}
	

	
	if(customerDTO.lmsMemberDTO != null){
		
		$("#joinAirewards").prop('checked', true);
		$("#passportNum").val(customerDTO.lmsMemberDTO.passportNum);
		$("#birthDate").val(customerDTO.lmsMemberDTO.dateOfBirth);
		$("#refferEmail").val(customerDTO.lmsMemberDTO.refferedEmail);
		$("#familyHeadEmail").val(customerDTO.lmsMemberDTO.headOFEmailId);
		
		$("#tabs").tabs( "enable" , 1 );
		$("#ffid").val(rowData['email']);
		
		if(customerDTO.lmsMemberDTO.emailStatus=='Y'){
			$("#tdLmsResend").hide();
		} else {
			$("#tdLmsResend").show();
		}
		
	}
	
	if($.isEmptyObject(customerDTO.lmsMemberDTO)){
		
		$("#lmsMessage").text("This user is not an Airewards member");
		
	}else if(customerDTO.lmsMemberDTO.emailStatus == "N"){

		$("#lmsMessage").text("Airewards email confirmation is pending");
		
	}else{
		
		$("#lmsMessage").text("");
		
	}
	
	

	$("#selEmgnTitle").val(customerDTO.emgnTitle);
	$("#txtEmgnFirstName").val(customerDTO.emgnFirstName);
	$("#txtEmgnLastName").val(customerDTO.emgnLastName);
	$("#txtEmgnEmail").val(customerDTO.emgnEmail);
	
	
	var emgNo = splitPhoneNumber(customerDTO.emgnPhoneNo);
	
	if (emgNo.length > 0){
		$("#txtEmgnPCountry").val(emgNo[0]);
	}

	if (travNo.length > 1){
		$("#txtEmgnPArea").val(emgNo[1]);
	}
	if (travNo.length > 2){
		$("#txtEmgnTelephone").val(emgNo[2]);
	}
	
	
		
	$("#txtAddr1").val(customerDTO.addressLine);
	$("#txtAddr2").val(customerDTO.addressStreet);
	
	var questionaire = customerDTO.customerQuestionaireDTO;
	
	if(questionaire.length > 0){
		
		if (questionaire[1].questionAnswer == "Y"){
			$("#cheQ1").prop("checked", true);
		} else {
			$("#cheQ1").prop("checked", false);
		}
		$("#promotional").val(questionaire[1].questionAnswer);
		
		$('input[id=\'radQ3\'][value="' + questionaire[2].questionAnswer + '"]').prop('checked', true);
		$('input[id=\'radQ4\'][value="' + questionaire[3].questionAnswer + '"]').prop('checked', true);
		$("#selQ5").val(questionaire[4].questionAnswer);
		
	}
	
	
}

getCustomerData = function(){
	
	data = {};
	
	var mobileNo = "";
	var faxNo = "";
	var travNo = "";
	
	if($("#txtMobiNo").val() != ""){
		
		var mobileNo = $("#txtMobiNo").val();
		
	}
	
	if($("#txtFaxNo").val() != ""){
		
		var faxNo = $("#txtFaxNo").val();
		
	}
	
	if($("#txtTravNo").val() != ""){
		
		var travNo = $("#txtTravNo").val();
		
	}
	
	var email = $("#editEmailAddress").val();
	var title = $("#displayTitle").val();
	var firstName = $("#firstName").val();
	var lastName = $("#lastName").val();
	var nationalityCode = $("#nationality").val();
	var residenceCode = $("#residence").val();
	var zipCode = $("#zipCode").val();
	
	var numberOnTravel;
	var faxNumber;
	var mobileNumber;
		
	numberOnTravel =  $("#txtTravCntry").val() + "-" + $("#txtTravArea").val() + "-" + travNo;
	faxNumber = $("#txtFaxCntry").val() + "-" + $("#txtFaxArea").val() + "-" + faxNo;
	mobileNumber = $("#txtMobiCntry").val() + "-" + $("#txtMobiArea").val() + "-" + mobileNo;
	
	data['carrierCode'] = top.carrierCode;
	
	if($("#joinAirewards").is(":checked")){
		
		data['optLMS'] = 'Y';
		data['lmsDetails.dateOfBirth'] = $("#birthDate").val();
		data['lmsDetails.language'] = $("#selPrefLang").val();
		data['lmsDetails.passportNum'] = $("#passportNum").val();
		data['lmsDetails.refferedEmail'] = $("#refferEmail").val();
		data['lmsDetails.headOFEmailId'] = $("#familyHeadEmail").val();
		
		data['lmsDetails.emailId'] = email;
		data['lmsDetails.firstName'] = firstName;
		data['lmsDetails.lastName'] = lastName;
		data['lmsDetails.mobileNumber'] = mobileNumber;
		data['lmsDetails.phoneNumber'] = numberOnTravel;
		data['lmsDetails.nationalityCode'] = nationalityCode;
		data['lmsDetails.residencyCode'] = residenceCode;
		data['lmsDetails.genderCode'] = title;
		data['customer.dateOfBirth'] = $("#birthDate").val();
		
	}else{

		data['optLMS'] = 'N';
		data['customer.dateOfBirth'] = "";
		
	}
		
	//customer details
	data['customer.emailId'] = email;
	data['customer.title'] = title;
	data['customer.firstName'] = firstName;
	data['customer.lastName'] = lastName;
	data['customer.telephone'] = numberOnTravel;
	data['customer.mobile'] = mobileNumber;
	data['customer.fax'] = faxNumber;
	data['customer.countryCode'] = residenceCode;
	data['customer.nationalityCode'] = nationalityCode;
	data['customer.zipCode'] = zipCode;
	data['customer.sendPromoEmail'] = false;
	
	// emergency details
	if((configList.firstName2.userRegVisibilty ||
			configList.lastName2.userRegVisibilty ||
			configList.phoneNo2.userRegVisibilty)){
		
		data['customer.emgnEmail'] = $("#txtEmgnEmail").val();
		data['customer.emgnTitle'] = $("#selEmgnTitle").val();
		data['customer.emgnFirstName'] = $("#txtEmgnFirstName").val();
		data['customer.emgnLastName'] = $("#txtEmgnLastName").val();			
		data['customer.emgnPhoneNo'] = $("#txtEmgnPCountry").val() + "-" + $("#txtEmgnPArea").val() + "-" + $("#txtEmgnTelephone").val();
		
	} 
	
	// address
	if(configList.address1.userRegVisibilty){
		
		data['customer.addressLine'] = $("#txtAddr1").val();
		data['customer.addressStreet'] = $("#txtAddr2").val();
		
	}
	
	if(configList.promotionPref1.userRegVisibilty && configList.emailPref1.userRegVisibilty
			&& configList.smsPref1.userRegVisibilty && configList.languagePref1.userRegVisibilty){
		
		var questionaire;
		
		var receivePromotions;
		
		if($("#chkQ1").is(":checked")){
			
			receivePromotions = "Y";
			
		}else{
			
			receivePromotions = "N";
			
		}
		
		questionaire = [{
							"questionAnswer":"",
							"questionKey":"1"
						}, 
						{
							"questionAnswer":receivePromotions,
							"questionKey":"2"
						},
						{
							"questionAnswer":$("#radQ3").val(),
							"questionKey":"3"
						},
						{
							"questionAnswer":$("#radQ4").val(),
							"questionKey":"4"
						},
						{
							"questionAnswer":$("#selQ5").val(),
							"questionKey":"5"
						},
						{
							"questionAnswer":"",
							"questionKey":"6"
						}];
		
		//data['customer.customerQuestionaireDTO'] = questionaire;
		data['jsonQuestionaire'] = JSON.stringify(questionaire);
		
	}else{
		
		data['jsonQuestionaire'] = "";
		
	}
	
	return data;
	
}

resetOnClick = function(){
	
	var rowId =$("#customerDetailsGrid").getGridParam('selrow');
	
	if(rowId >= 0){
		
		var rowData = $("#customerDetailsGrid").getRowData(rowId);
		
		populateCustomerDetails(rowData);
		clearFields();
		
	}else{
		
		clearFields();
		
	}
	
}

clearFields = function(){
	
	$("#editEmailAddress").val("");
	$("#displayTitle").val("");
	$("#firstName").val("");
	$("#lastName").val("");
	$("#residence").val("");
	$("#nationality").val("");
	$("#txtMobiNo").val("");
	$("#txtTravNo").val("");
	$("#txtFaxNo").val("");
	$("#txtMobiCntry").val("");
	$("#txtTravCntry").val("");
	$("#txtFaxCntry").val("");
	$("#zipCode").val("");
	$("#ffid").val("");
	$("#emailID").val("");
	$("#tdPassResend").hide();
	$("#tdLmsResend").hide();
	$("#lmsMessage").text("");
	
	
	$("#joinAirewards").prop('checked', false);
	$("#passportNum").val("");
	$("#birthDate").val("");
	$("#refferEmail").val("");
	$("#familyHeadEmail").val("");
	
	if((configList.firstName2.userRegVisibilty ||
			configList.lastName2.userRegVisibilty ||
			configList.phoneNo2.userRegVisibilty)){
		
		$("#txtEmgnEmail").val("");
		$("#selEmgnTitle").val("");
		$("#txtEmgnFirstName").val("");
		$("#txtEmgnLastName").val("");
		$("#txtEmgnPCountry").val("");
		$("#txtEmgnTelephone").val("");
		
	}
	
	$("#txtMobiArea").val("");
	$("#txtFaxArea").val("");
	$("#txtTravArea").val("");
	$("#txtEmgnPArea").val("");
	$("#lmsMessage").text("");
}

addOnClick = function(){
	clearFields();
	$('#tabs-1').find('input,button, select').prop('disabled', false);
	$('#tabs-2').find('input,button, select').prop('disabled', false);
	$("#birthDate").datepicker("enable");
	$("#mashreqBirthDate").datepicker("enable");
	$( '#tabs' ).tabs( { disabled:[1] } );
	$( '#tabs' ).tabs( "option", "active", 0 );
	$("#btnSave").hide();
	$("#btnRegister").show();
}

editOnClick = function(){
	
	if(regPrivilages.editCustomer) {
		
		var rowId =$("#customerDetailsGrid").getGridParam('selrow');
		
		if(rowId >= 0 && rowId != null){
			
			$("#btnSave").show();
			var rowData = $("#customerDetailsGrid").getRowData(rowId);
			populateCustomerDetails(rowData);
			
			$('#tabs-1').find('input,button, select').prop('disabled', false);
			$('#tabs-2').find('input,button, select').prop('disabled', false);
			
			$("#btnRegister").hide();
			$("#btnSave").show();
			
			$("#editEmailAddress").prop('disabled', true);
			if (!regPrivilages.editName) {
				$("#displayTitle").prop('disabled', true);
				$("#firstName").prop('disabled', true);
				$("#lastName").prop('disabled', true);
			} else {
				$("#displayTitle").prop('disabled', false);
				$("#firstName").prop('disabled', false);
				$("#lastName").prop('disabled', false);
			}
			$("#birthDate").datepicker("disable");
			
				
			$("#refferEmail").prop('disabled', true);
			
			if($("#joinAirewards").is(":checked")){
				
				$("#tabs").tabs( "enable" , 1 ); 
				$("#joinAirewards").prop('disabled', true);
				
			}else{
				
				$("#birthDate").datepicker("enable");
				
			}
		
	}
	
	}
	
}

updateCustomer = function(){

	
			var customerData = getCustomerData();

	
			$.ajax({
				url : 'registerCustomer!updateCustomer.action',
				data : customerData,
				dataType : "json",
				beforeSend : ShowPopProgress(),
				type : "POST",
				success : function(response) {

					HidePopProgress();
					showConfirmation(arrError.modificationSuccessfull);
					createGrid();

				},
				error : function(response) {

					HidePopProgress();
					showERRMessage("TAIR-91002: Cannot process your request, please try again later");

				}

			});

	return true;

	
}

updateCustomerTest = function(){
	
	if(!regPrivilages.editCustomer) {
		
		return;
		
	}
	
	if(!UI_customerValidation.validateOnUpdateCustomer(false)){
		
		return;
		
	}
	
	if(UI_customerValidation.lmsValidationData != null && UI_customerValidation.lmsNameMismatch){

		$("#customerContainer").children().prop('disabled',true);
		$("#lms-acount-exists-wrong-names").show();
		
		return;
		
	}
	
	if(UI_customerValidation.lmsValidationData != null && UI_customerValidation.lmsValidationData.emailId == 'Y'){

		isRegisterNewCustomer = false;
		$("#customerContainer").children().prop('disabled',true);
	   	$("#lms-acount-exists-merge").show();

	}else{
		if (isNameSoundxChanged() && regPrivilages.editName) {
			UI_commonSystem.showProgress();
			$("#divCustNameUpdateConfirm").show();
		} else {
			updateCustomer();
		}
		
	}
	
}

customerDetailsGridOnClick = function(){
	
	clearFields();
	$('#tabs-1').find('input,button, select').prop('disabled', true);
	$('#tabs-2').find('input,button, select').prop('disabled', true);
	$( '#tabs' ).tabs( { disabled:[1] } );
	$( '#tabs' ).tabs( "option", "active", 0 );
	
	var rowId =$("#customerDetailsGrid").getGridParam('selrow');  
	var rowData = $("#customerDetailsGrid").getRowData(rowId);
	
	populateCustomerDetails(rowData);
	
}

loadCustomerPageDetails = function (){
	$.ajax({
		url:"showCustomerDetail.action",
		dataType: 'json',
		beforeSubmit:ShowPopProgress,
		success: loadCustomerPageDataSucess		
	});
}

loadCustomerPageDataSucess = function (response) {
	loyaltyManagmentEnabled = response.loyaltyManagmentEnabled;
	configList = response.configList;
	regPrivilages = response.regPrivilages;
	showHideButtons();
	showHideCustomerFields();
	try {
		eval(response.errorList);
	} catch (e) {
		console.log("customer.js ==> Error in eval");
	}
	HidePopProgress();
}

showHideCustomerFields = function () {
	/*if(!configList.email1.userRegVisibilty){
		$("#trEmail").hide();
	} else if(!configList.email1.mandatory){
		$("#lblEmailMand").hide();
	}
	
	*/
	if(!configList.title1.userRegVisibilty){
		$("#tdTitle").hide();
		$("#tdTitleField").hide();
	} else if(!configList.title1.mandatory){
		$("#lblTitleMand").hide();
	}
	
	if(!configList.firstName1.userRegVisibilty){
		$("#tdFirstName").hide();
		$("#tdFirstNameField").hide();
	} else if(!configList.firstName1.mandatory){
		$("#lblFirstNameMand").hide();
	}
	
	if(!configList.lastName1.userRegVisibilty){
		$("#tdLastName").hide();
		$("#tdLastNameField").hide();
	} else if(!configList.lastName1.mandatory){
		$("#lblLastNameMand").hide();
	}
	
	if(!configList.gender1.userRegVisibilty){
		$("#tdGender").hide();
		$("#tdGenderField").hide();
	} else if(!configList.gender1.mandatory){
		$("#lblGenderMand").hide();
	}
	
	if(!configList.nationality1.userRegVisibilty){
		$("#tdNationality").hide();
		$("#tdNationalityField").hide();
	} else if(!configList.nationality1.mandatory){
		$("#lblNationalityMand").hide();
	}
	
	if(!configList.address1.userRegVisibilty){
		$("#tdAddress").hide();
	} else if(!configList.address1.mandatory){
		$("#lblAddrMand").hide();
	}
	
	if(!configList.city1.userRegVisibilty){
		$("#tdCity").hide();
	} else if(!configList.city1.mandatory){
		$("#lblCityMand").hide();
	}
	
	if(!configList.zipCode1.userRegVisibilty){
		$("#tdZipCode").hide();
		$("#tdZipCodeField").hide();
	} else if(!configList.zipCode1.mandatory){
		$("#lblZipCodeMand").hide();
	}
	
	if(!configList.country1.userRegVisibilty){
		$("#tdCountry").hide();
	} else if(!configList.country1.mandatory){
		$("#lblCountryMand").hide();
	}
	
	if(!configList.address1.userRegVisibilty){
		$("#tdAddress").hide();
	} else if(!userRegConfig.address1.mandatory){
		$("#lblAddrMand").hide();
	}
	
	if(!configList.phoneNo1.userRegVisibilty && !configList.mobileNo1.userRegVisibilty
			&& !configList.homeOfficeNo1.userRegVisibilty && !configList.fax1.userRegVisibilty){
		$("#trContactNo").hide();
	} else {
		if(!configList.phoneNo1.userRegVisibilty){
			$("#tdPhoneNo").hide();
			$("#tdPhoneNoField").hide();
		} else if(!configList.phoneNo1.mandatory){
			$("#lblPhoneMand").hide();
		}
		
		if(!configList.mobileNo1.userRegVisibilty){
			$("#tdMobileNo").hide();
			$("#tdMobileNoField").hide();
		} /*else if(!configList.mobileNo1.mandatory){
			$("#lblMobileMand").hide();
		}
		*/
		if(!configList.homeOfficeNo1.userRegVisibilty){
			$("#tdHomeOfficeNo").hide();
			$("#tdHomeOfficeNoField").hide();
		} else if(!configList.homeOfficeNo1.mandatory){
			$("#lblHomeOfficeMand").hide();
		}
		
		
		if(!configList.fax1.userRegVisibilty){
			$("#tdFaxNo").hide();
			$("#tdFaxNoField").hide();
		} else if(!configList.fax1.mandatory){
			$("#lblFaxMand").hide();
		}
	}
	
	//Emergency contact details show/hide
	if(!(configList.firstName2.userRegVisibilty ||
			configList.lastName2.userRegVisibilty ||
			configList.phoneNo2.userRegVisibilty)){
		$("a[href='#tabs-3']").hide();
	} else {
		if(!configList.title2.userRegVisibilty){
			$("#trEmgnTitle").hide();
		} else if(!configList.title2.mandatory){
			$("#lblEmgnTitleMand").hide();
		}
		
		if(!configList.firstName2.userRegVisibilty){
			$("#trEmgnFirstName").hide();
		} else if(!configList.firstName2.mandatory){
			$("#lblEmgnFirstNameMand").hide();
		}
		
		if(!configList.lastName2.userRegVisibilty){
			$("#trEmgnLastName").hide();
		} else if(!configList.lastName2.mandatory){
			$("#lblEmgnLastNameMand").hide();
		}
		
		if(!configList.phoneNo2.userRegVisibilty){
			$("#emgnPhone1").hide();
			$("#emgnPhone2").hide();
		} else if(!configList.phoneNo2.mandatory){
			$("#lblEmgnPhoneMand").hide();
		}
		
		if(!configList.email2.userRegVisibilty){
			$("#trEmgnEmail").hide();
		} else if(!configList.email2.mandatory){
			$("#lblEmgnEmailMand").hide();
		}
	}
	
	// show/hide preference
	if(!configList.promotionPref1.userRegVisibilty && !configList.emailPref1.userRegVisibilty
			&& !configList.smsPref1.userRegVisibilty && !configList.languagePref1.userRegVisibilty){
		$("a[href='#tabs-4']").hide();
	} else {
		if(!configList.promotionPref1.userRegVisibilty){
			$("#trPromotionPref").hide();
		}
		
		if(!configList.emailPref1.userRegVisibilty){
			$("#tblEmailPref").hide();
		}
		
		if(!configList.smsPref1.userRegVisibilty){
			$("#tblSmsPref").hide();
		}
		
		if(!configList.languagePref1.userRegVisibilty){
			$("#tblLanguagePref").hide();
		}
	}	

	if (!loyaltyManagmentEnabled) {
		$("a[href='#tabs-2']").hide();
		$("#tdJoinAirewards").hide();
		$("#lblCustNameUpdateCNF").hide();
		$("#tdJoinAirewardsField").hide();
	}
	
	$("#tdLmsResend").hide();
	$("#tdPassResend").hide();
}

showHideButtons = function () {
	if(!regPrivilages.addCustomer){
		$("#btnAdd").hide();
		$("#btnRegister").hide();
	}
	if(!regPrivilages.editCustomer){
		$("#btnEdit").hide();
		$("#btnSave").hide();
	}
}

splitPhoneNumber = function(phoneNumber){
	
	if(phoneNumber != "--" && phoneNumber != "" && phoneNumber != null){
		
		return phoneNumber.split('-');
	
	}else{

		return ["" , "" , ""];
		
	}
	
}

resendLmsConfirmation = function(){
	var url = "registerCustomer!sendlmsConfEmail.action";
	var data = {}
	data["ffid"] = $("#editEmailAddress").val();
	data["carrierCode"] = top.carrierCode;
	var response = $.ajax({
		type: "POST",
		data: data,
		asynch: false,
		url:url,		
		success: resendLmsConfirmationSuccess,
		error:UI_commonSystem.setErrorStatus
	});
}

resendLmsConfirmationSuccess = function(){
	alert("Mail sent");
}

resendProfileDetailsMail= function(){
	var url = "registerCustomer!resendProfileDetailsMail.action";
	var data = {}
	data["emailId"] = $("#editEmailAddress").val();
	data["carrierCode"] = top.carrierCode;
	var response = $.ajax({
		type: "POST",
		data: data,
		asynch: false,
		url:url,		
		success: resendProfileDetailsMailSuccess,
		error:UI_commonSystem.setErrorStatus
	});
}

resendProfileDetailsMailSuccess = function(){
	alert("Profile Email Sent");
}