var screenId = "SC_SHDS_001";
var valueSeperator = "~";

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "15%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Airport" ;
objCol1.headerText = "Airport";
objCol1.itemAlign = "left";
objCol1.sort="true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "10%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Message Type" ;
objCol2.headerText = "Message Type";
objCol2.itemAlign = "left";
objCol2.sort="true";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "20%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Timestamp (Zulu)" ;
objCol3.headerText = "Timestamp (Zulu)";
objCol3.itemAlign = "left";
	
var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "15%";
objCol4.arrayIndex = 8;
objCol4.toolTip = "Transmission Status" ;
objCol4.headerText = "Transmission Status";
objCol4.itemAlign = "left";
	
var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "20%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Sent To" ;
objCol5.headerText = "Sent To";
objCol5.itemAlign = "Left";

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "20%";
objCol6.arrayIndex = 10;
objCol6.toolTip = "Timestamp (Local)" ;
objCol6.headerText = "Timestamp (Local)";
objCol6.itemAlign = "left";

// ---------------- Grid	
var objDG = new DataGrid("spnReprintGenInv");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol6);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);

objDG.width = "99%";
objDG.height = "150px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrPALCALData;
objDG.seqNo = true;
objDG.seqStartNo = 1; // top[0].intLastRec;		
//objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec");  //Grid
//objDG.pgnumRecTotal = parent.totalRecords ; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";	
objDG.rowClick = "RowClick";	
ShowProgress();
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";