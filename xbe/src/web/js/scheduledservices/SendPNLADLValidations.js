
// Author : Menaka P. Wedige


var objWindow;
var screenId = "SC_SHDS_001";
var valueSeperator = "~";
var strRowData;
var strGridRow;
var intLastRec;

//On Page loading make sure that all the page input fields are cleared
//function winOnLoad(strMsg,strMsgType) {
function winOnLoad(strMsg, strMsgType, strWarnMsg) {
		objOnFocus();
		if (saveSuccess == 1) {
			alert("Send new PNL operartion Successful!");
			saveSuccess = 0;
		}else if (saveSuccess == 2) {
			alert("Send new ADL operartion Successful!");
			saveSuccess = 0;
		}else if (saveSuccess == 3) {
			alert("Resend PNL operartion Successful!");
			saveSuccess = 0;
		} else if (saveSuccess == 4) {
			alert("Resend ADL operartion Successful!");
			saveSuccess = 0;
		}		
		setVisible("spnInput",false);
		setVisible("spn1",false);	
		Disable("btnSendNew", "true");
		Disable("btnResend", "true");
		Disable("btnPrint", "true");
		Disable("btnReset", "true");

	//Keep the search variables remain same
	if(isSearchMode){
//		ls.drawListBox();
//		ls.disable(false);	
		if(arrPNLADLData == "" || arrPNLADLData == null){
			getFieldByID("btnSendNew").value = "Send New PNL";
			getFieldByID("btnResend").value = "Resend PNL";
			Disable("btnSendNew", "");

		}
		document.forms[0].hdnUIMode.value="search";	
		setField("selMailServer",defaultMailServer); //set the defalut mail ser from parameter file
		if (isDCSWS) {
			getFieldByID("spnInput").visibility="hidden";
			setVisible("btnResend", false);
			setVisible("btnPrint", false);
			//setVisible("spnInput",false);
		} else {
			setVisible("spnInput",true);
			setVisible("spn1",true);
		}
		
		Disable("btnReset", "");
		if(strMsg != null && strMsgType != null && strMsg != ''){
			//showERRMessage(strMsg);
			showCommonError(strMsgType,strMsg);
		}
		//to set the Agent after search
		if(airportCode != null){
			setField("selAirport",airportCode);
		}
		if(flightNo != null){
			setField("txtFlightNo",flightNo);
		}
		if(flightDate != null){
			setField("txtDate",flightDate);
		}
	}
	
	if(arrFormData != null && arrFormData.length > 0) {
//Removed Temperally		ls.selectedData(arrFormData[0][1]);
		setField("selMailServer",arrFormData[0][2]);
		Disable("btnReset", "");
	//	setField("txtaLogEntry",arrFormData[0][3]);
		if (isDCSWS) {
			getFieldByID("spnInput").visibility="hidden";
			getFieldByID("spn1").visibility="hidden";
			setVisible("btnResend", false);
			setVisible("btnPrint", false);
			//setVisible("spnInput",false);
		} else {
			setVisible("spnInput",true);
			setVisible("spn1",true);
		}
		
		
//Removed Temperally		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); //Get saved grid Row
	}


	getFieldByID("txtDate").focus();
//	setVisible("spnInput",false);
	setPageEdited(false);	
//	top[2].HideProgress();
}

//on Grid Row click
function RowClick(strRowNo){
	objOnFocus();
//	setVisible("spnInput",true);
//	setVisible("spn1",true);
	if(top.loadCheck()){
//	if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow",strRowNo);
		setField("hdnADLTimestamp",strLastADLTimestamp);
		setField("hdnPNLTimestamp",strLastPNLTimestamp);
		setField("hdnmsgType",arrPNLADLData[strRowNo][2]);
		setField("hdnTransmitStatus",arrPNLADLData[strRowNo][4]);
		setField("hdnFlightID",arrPNLADLData[strRowNo][9]);
		setField("hdnTS",arrPNLADLData[strRowNo][3]);
		setField("hdnRowSITA",arrPNLADLData[strRowNo][5]);
		
		
		setPageEdited(false);
		setField("hdnMode","");
		if(arrPNLADLData[strRowNo][5] != "") {
			ls.selectedData(arrPNLADLData[strRowNo][5]);
		}

		getFieldByID("btnResend").value = "Resend";
		getFieldByID("btnSendNew").value = "Send New";
		
//		Disable("rndPNLADL1", true);	
//		Disable("rndPNLADL2", true);
		Disable("btnSendNew", true);
		Disable("btnResend", true);
		Disable("btnReset", "");
		
		if(arrPNLADLData[strRowNo][2]=='PNL' && (arrPNLADLData[strRowNo][4]=="Y" || arrPNLADLData[strRowNo][4]=="P") && strPNLStatusYes=="Y" ){ //&& (strLastPNLTimestamp==arrPNLADLData[strRowNo][3])){
			Disable("btnResend", false);
			Disable("btnSendNew",false);
			getFieldByID("btnResend").value = "Resend PNL";
			getFieldByID("btnSendNew").value = "Send New ADL";
		}
		else if(arrPNLADLData[strRowNo][2]=='PNL' && arrPNLADLData[strRowNo][4] == "N"  && strPNLStatusYes == '' && strLastPNLTimestamp==arrPNLADLData[strRowNo][3]){
				//Disable("btnResend", "");
				Disable("btnResend", true);
				Disable("btnSendNew",false);
				getFieldByID("btnSendNew").value = "Send New PNL";
				getFieldByID("btnResend").value = "Resend PNL";
		}
		else if(arrPNLADLData[strRowNo][2]=='PNL' && arrPNLADLData[strRowNo][4]=="N" && strADLStatusYes == '' ){
			//Disable("btnResend", "");
			Disable("btnResend", true);
			Disable("btnSendNew",false);
			getFieldByID("btnResend").value = "Resend PNL";
			getFieldByID("btnSendNew").value = "Send New PNL";
		} 
		else if(arrPNLADLData[strRowNo][2]=='PNL' && arrPNLADLData[strRowNo][4]=="N" && strADLStatusYes=="Y" ){
			//Disable("btnResend", "");
			Disable("btnResend", true);
			Disable("btnSendNew",false);
			getFieldByID("btnResend").value = "Resend PNL";
			getFieldByID("btnSendNew").value = "Send New PNL";
		} 

		else if(arrPNLADLData[strRowNo][2]=='ADL'  && arrPNLADLData[strRowNo][4]=="N" && strLastADLTimestamp==arrPNLADLData[strRowNo][3]){
				Disable("btnResend", true);
				Disable("btnSendNew",false);
				getFieldByID("btnSendNew").value = "Send New ADL";
				getFieldByID("btnResend").value = "Resend ADL";
		}
		
		else if(arrPNLADLData[strRowNo][2]=='ADL' && arrPNLADLData[strRowNo][4]=="Y" && strADLStatusYes=="Y" ){
			//Disable("btnResend", "");
			Disable("btnResend", false);
			Disable("btnSendNew",true);
			getFieldByID("btnResend").value = "Resend ADL";
			getFieldByID("btnSendNew").value = "Send New ADL";
		}		
		
		Disable("btnPrint", "");
	}
}

function dateValidation(cntfield){
	if (!dateChk(cntfield))	{
	//	showCommonError("Error",flightDateInvalid);
		showERRMessage(arrError['flightDateInvalid']);
		getFieldByID(cntfield).focus();
		return;
	}
}

function searchData() {
	objOnFocus();
	if (isEmpty(getText("txtDate"))) {		
//		showCommonError("Error",flightDateRqrd);
		showERRMessage(arrError['flightDateRqrd']);
		getFieldByID("txtDate").focus();
		return;
	}
	
	dateValidation('txtDate');
		
//	if(document.getElementById("txtFlightNo").value == ""){
	if (isEmpty(getText("txtFlightNo"))) {	
//		showCommonError("Error",flightNumberRqrd);
		showERRMessage(arrError['flightNumberRqrd']);
		getFieldByID("txtFlightNo").focus();
		return;
	}
	if(document.getElementById("selAirport").value == "-1"){
//		showCommonError("Error",airportRqrd);
		showERRMessage(arrError['airportRqrd']);
		getFieldByID("selAirport").focus();
		return;
	}

//	if(top.loadCheck()){
//	if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		top[0].initializeVar();
		//once search button was pressed set the hidden value to "SEARCH"
		var fltno = getText("txtFlightNo"); 
		setField("txtFlightNo",fltno.toUpperCase());
		document.forms[0].hdnUIMode.value="search";
		document.forms[0].hdnMode.value="";
		document.forms[0].target="_self";
		document.forms[0].action = "saveSendPNLADL.action";
		document.forms[0].submit();
		setPageEdited(false);
//	}
}	

function resetData(){
	objOnFocus();
	if(arrPNLADLData !="" ){
		ls.removeAllFromListbox("lstAssignedSITAAdresses","lstSITAAddresses",2);
		if(arrPNLADLData[strGridRow][5] != "") {
			ls.selectedData(arrPNLADLData[strGridRow][5]);
		}
	}
	setField("selMailServer",defaultMailServer); //set the defalut mail ser from parameter file
}

function sendNew() {
	objOnFocus();
//	alert(ls.getselectedData());
	if(ls.getselectedData() == "" && !isDCSWS){
	//	showCommonError("Error",sitaAddressRqrd);
		showERRMessage(arrError['sitaAddressRqrd']);
		getFieldByID("lstSITAAddresses").focus();
		return;
	}

	if(document.getElementById("selMailServer").value=="-1" && !isDCSWS){
	//	showCommonError("Error",mailServerRqrd);
		showERRMessage(arrError['mailServerRqrd']);
		getFieldByID("selMailServer").focus();
		return;
	}
	
	if(allowManuallySendPnl != null && (allowManuallySendPnl == "false" || !allowManuallySendPnl)){
		showERRMessage(arrError['insufficientPnlAdlPriv']);
		return;
	}
	//Get Confirmation
	var confirmStr;

	//When no data for the selected search criteria can send a new PNL
	if(arrPNLADLData==""){
		//alert("send 1");
	//	confirmStr = confirm(sendnewPNLRecoredCfrm);
		confirmStr = confirm(arrError['sendnewPNLRecoredCfrm']);
		document.forms[0].hdnMode.value="NEWPNL";
	}
//	if(document.getElementById("rndPNLADL1").checked==true)
	if(arrPNLADLData!="" && arrPNLADLData[strGridRow][2]=='PNL'){
				//alert("send 2");
	//	confirmStr = confirm(sendnewPNLRecoredCfrm);
		if(arrPNLADLData[strGridRow][4]=="Y" || arrPNLADLData[strGridRow][4]=="P"){
			confirmStr = confirm(arrError['sendnewADLRecoredCfrm']);
			document.forms[0].hdnMode.value="NEWADL";
		}else{
			confirmStr = confirm(arrError['sendnewPNLRecoredCfrm']);
			document.forms[0].hdnMode.value="NEWPNL";
		}

	}
	//	if(document.getElementById("rndPNLADL2").checked==true)
	
	if(arrPNLADLData!="" && arrPNLADLData[strGridRow][2]=='ADL'){
		//alert("send 3");
	//	confirmStr = confirm(sendnewADLRecoredCfrm);
		confirmStr = confirm(arrError['sendnewADLRecoredCfrm']);
		document.forms[0].hdnMode.value="NEWADL";
	}
	if(!confirmStr)
		return;
	setField("hdnSITAValues",trim(ls.getselectedData()));
	setField("hdnRecNo",(top[0].intLastRec));	
	//setField("hdnRecNo",getTabValues(screenId, valueSeperator, "intLastRec"));
	//setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); //Save the grid Row
	document.forms[0].target="_self";
	document.forms[0].action = "saveSendPNLADL.action";
	ShowProgress();
	document.forms[0].submit();
	setPageEdited(false);

}

function reSend() {
	objOnFocus();

	if(ls.getselectedData() == "" && !isDCSWS){
	//	showCommonError("Error",sitaAddressRqrd);
		showERRMessage(arrError['sitaAddressRqrd']);
		getFieldByID("lstSITAAddresses").focus();
		return;
	}
	if(document.getElementById("selMailServer").value=="-1" && !isDCSWS){
	//	showCommonError("Error",mailServerRqrd);
		showERRMessage(arrError['mailServerRqrd']);
		getFieldByID("selMailServer").focus();
		return;
	}
	//Get Confirmation
	var confirmStr;

//	if(document.getElementById("rndPNLADL1").checked==true)
	if(arrPNLADLData[strGridRow][2]=='PNL'){
		//alert("1");
	//	confirmStr = confirm(reSendPNLRecoredCfrm);
		confirmStr = confirm(arrError['reSendPNLRecoredCfrm']);
		document.forms[0].hdnMode.value="RESENDPNL";
//	if(document.getElementById("rndPNLADL2").checked==true)
	}
	if(arrPNLADLData[strGridRow][2]=='ADL'){
			//	alert("2");
		//confirmStr = confirm(reSendADLRecoredCfrm);
		confirmStr = confirm(arrError['reSendADLRecoredCfrm']);
		document.forms[0].hdnMode.value="RESENDADL";
	}
	if(!confirmStr)
		return;
	setField("hdnSITAValues",trim(ls.getselectedData()));
	setField("hdnRecNo",(top[0].intLastRec));
//	setField("hdnRecNo",getTabValues(screenId, valueSeperator, "intLastRec"));
//	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); //Save the grid Row		
//	document.forms[0].hdnMode.value="RESEND";
	document.forms[0].target="_self";
	document.forms[0].action = "saveSendPNLADL.action";
	ShowProgress();
	document.forms[0].submit();
	setPageEdited(false);

}

function valFlightNumber(objTextBox){
	setPageEdited(true);
	objOnFocus();
	var strCC = objTextBox.value;
	var strLen = strCC.length;
	var blnVal = isEmpty(strCC);
	if(blnVal){
		setField("txtFlightNo",strCC.substr(0,strLen-1)); 
		getFieldByID("txtFlightNo").focus();
	}
}

function objOnFocus(){

	top[2].HidePageMessage();
}

function pageOnChange(){
	top.pageEdited = true;
//	top[1].objTMenu.tabPageEdited(screenId, true);
}
function setPageEdited(isEdited){
	top.pageEdited=isEdited;	
//	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function editReSend() {  
	if ((objWindow) && (!objWindow.closed))	{
		objWindow.close();
	}
	//objOnFocus();
	var confirmStr;
	var intWidth = 0;
	var intHeight = 0 ; 
	var strProp = '';
	var strMsgType;
	
	if(arrPNLADLData[strGridRow][2]=='PNL'){
		//confirmStr = confirm(rePrintPNLRecoredCfrm);
		confirmStr = confirm(arrError['rePrintPNLRecoredCfrm']);
	}
	if(arrPNLADLData[strGridRow][2]=='ADL'){
		//confirmStr = confirm(rePrintADLRecoredCfrm);
		confirmStr = confirm(arrError['rePrintADLRecoredCfrm']);
	}
	if(!confirmStr)
		return;

	
	
	document.forms[0].hdnMode.value="EDIT";

	strMsgType = arrPNLADLData[strGridRow][2];	
	setField("hdnRecNo",(top[0].intLastRec));	
	setPageEdited(false);
	intHeight = 295;
	intWidth = 720;
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=Yes,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + ((window.screen.height - intHeight) / 2) + ',left=' + (window.screen.width - intWidth) / 2;
	objWindow = window.open("about:blank","CWindow",strProp);
	objForm  = document.getElementById("frmPNLADL");
	objForm.target = "CWindow";
	objForm.action = "showPrintPNLADL.action?strMsgType="+strMsgType;
	objForm.submit();

}


function ctrl_print_click() {  
	if ((objWindow) && (!objWindow.closed))	{
		objWindow.close();
	}
	//objOnFocus();
	var confirmStr;
	var intWidth = 0;
	var intHeight = 0 ; 
	var strProp = '';
	var strMsgType;
	
	if(arrPNLADLData[strGridRow][2]=='PNL'){
		//confirmStr = confirm(rePrintPNLRecoredCfrm);
		confirmStr = confirm(arrError['rePrintPNLRecoredCfrm']);
	}
	if(arrPNLADLData[strGridRow][2]=='ADL'){
		//confirmStr = confirm(rePrintADLRecoredCfrm);
		confirmStr = confirm(arrError['rePrintADLRecoredCfrm']);
	}
	if(!confirmStr)
		return;

	
	
	document.forms[0].hdnMode.value="PRINT";

	strMsgType = arrPNLADLData[strGridRow][2];	
	setField("hdnRecNo",(top[0].intLastRec));	
	//setField("hdnRecNo",getTabValues(screenId, valueSeperator, "intLastRec"));
	//setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); //Save the grid Row		
	setPageEdited(false);
	intHeight = 295;
	intWidth = 720;
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=Yes,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + ((window.screen.height - intHeight) / 2) + ',left=' + (window.screen.width - intWidth) / 2;
	objWindow = window.open("about:blank","CWindow",strProp);
	objForm  = document.getElementById("frmPNLADL");
	objForm.target = "CWindow";
	objForm.action = "showPrintPNLADL.action?strMsgType="+strMsgType;
	objForm.submit();

}
function beforeUnload(){
	if ((objWindow) && (!objWindow.closed))	{
		objWindow.close();
	}
}

 //Calender
	var objCal1 = new Calendar("spnCalendarDG1");
	
	objCal1.onClick = "setDate";
	objCal1.buildCalendar();
	
	function setDate(strDate, strID){
		switch (strID){
			case "0" : setField("txtDate",strDate);break ;
		}
	}
	
	function LoadCalendar(strID, objEvent){
			objCal1.ID = strID;
			objCal1.top = 130 ;
			objCal1.left = 0 ;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
	}

	
	