var objWindow;
var screenId="SC_FACM_006";	
var objCal1 = new Calendar();
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
var tmOut;

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtInvoiceFrom", strDate);
		break;
	case "1":
		setField("txtInvoiceTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 25 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

var valueSeperator = "~";
var tabSearch = top[1].objTMenu.tabGetValue(screenId);
	function winOnLoad(strMsg, strMsgType) {
		setVisible("forAll",false); 
		
		if(agentType !='GSA'){
				Disable('chkAll',false);
				setVisible("forAll",true); 
		}
		if(IsTransEnable == 'true' || IsTransEnable == true){
			buildTransactionUI();	
			getFieldByID("txtInvoiceFrom").focus();	
			
		}else{
			getFieldByID("selYear").focus();
		}	
		if(IsTransEnable != 'true') {
			var strSearch = top[0].manifestSearchValue.split("#");//; top[0].manifestSearchValue.split("#")
			setField("selYear",strSearch[0]);
			setField("selMonth",strSearch[1]);
			setField("selAgencies",strSearch[2]);
			setField("selBP",strSearch[4]);
			if (strSearch[3] == 'true') {
				if(getFieldByID("chkTAs"))
				getFieldByID("chkTAs").checked = true;
			} else {
				if(getFieldByID("chkTAs"))
					getFieldByID("chkTAs").checked = false;		
				Disable('chkTAs',true);
			}
			if(strSearch[5] && strSearch[5] != "") {
				ls.selectedData(strSearch[5]);
			}
			if(strSearch[6] && strSearch[6] != 'false') {
				document.forms[0].chkZero.checked =true;

			}
			if(!strSearch[6]) {
				document.forms[0].chkZero.checked =true;
			}
			if (strSearch[7] == 'true')	{
				if(getFieldByID("chkCOs"))
				getFieldByID("chkCOs").checked = true;
			}else {
				if(getFieldByID("chkCOs"))
				getFieldByID("chkCOs").checked = false;		
				Disable('chkCOs',true);
			}
			setField("selEntity",strSearch[8]);
		}else {
			if(tabSearch != "" && IsTransEnable == 'true') {
				var strSearch=tabSearch.split("#");
				setField("txtInvoiceFrom",strSearch[0]);
				setField("txtInvoiceTo",strSearch[1]);
				setField("selAgencies",strSearch[2]);
				if (strSearch[3] == 'true') {
					if(getFieldByID("chkTAs"))
					getFieldByID("chkTAs").checked = true;
				} else {
					if(getFieldByID("chkTAs"))
						getFieldByID("chkTAs").checked = false;		
					Disable('chkTAs',true);
				}
				if(strSearch[4] && strSearch[4] != "") {
					ls.selectedData(strSearch[4]);
				}
				if (strSearch[5] == 'true')	{
					if(getFieldByID("chkCOs"))
					getFieldByID("chkCOs").checked = true;
				}else {
					if(getFieldByID("chkCOs"))
					getFieldByID("chkCOs").checked = false;		
					Disable('chkCOs',true);
				}
				setField("selEntity",strSearch[6]);
			} else{
				var strSysDateArr=getSystemDate().split("/");			
				setField("selMonth",strSysDateArr[1]);
				Disable('chkTAs',true);
				Disable('chkCOs',true);
			}
		}		
		Disable('btnGenerate',false);
		Disable('btnTrnsfer',true);
		Disable('btnSend',true);
		if(isShowReport){
			setVisible('btnViewReport',true);
			setVisible('btnPrint',true);
			Disable("btnViewReport",true);
			Disable("btnPrint",true);
		}else {
			setVisible('btnViewReport',false);
			setVisible('btnPrint',false);
		}
		
		if (invoiceData.length == 0) {
			Disable('chkPage',true);
			Disable('chkAll',true);
		}else {
			Disable('chkPage',false);
			
		}
		///-----------------//////
		if(IsTransEnable == 'true' || IsTransEnable == true){
			setVisible('btnGenerate',false);
		}else{
			validatePeriod();
		}
		if(strMsg != null && strMsg != "" && strMsgType != null && strMsgType != "") {
			showCommonError(strMsgType,strMsg);
		}
	}

	function gridNavigations(intRecNo, intErrMsg) {	
		setField("hdnRecNo",intRecNo);
		
		if (intErrMsg != ""){
			top[2].objMsg.MessageText = "<li> " + intErrMsg ;
			top[2].objMsg.MessageType = "Error" ; 
			top[2].ShowPageMessage();
		}else{
			if(IsTransEnable == 'true' || IsTransEnable == true){
				var invoiceFrm = getText('txtInvoiceFrom');
				var invoiceTo = getText('txtInvoiceTo');
				setField("hdnMode","SEARCH");
				var strSearchCriteria = invoiceFrm+"#"+invoiceTo+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+ls.getselectedData()+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
				document.getElementById("frmInvoiceEmail").target = "_self";
				document.forms[0].hdnMode.value="SEARCH";
				top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
				//top[0].manifestSearchValue = strSearchCriteria;			
				document.forms[0].submit();
				ShowProgress();	
			}else{
				setField("hdnAgents",getAgents());
				document.forms[0].hdnMode.value="SEARCH";
				var strSearchCriteria = getValue("selYear")+"#"+getValue("selMonth")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBP")+"#"+ls.getselectedData()+"#"+getValue("chkZero")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
				top[0].manifestSearchValue = strSearchCriteria;			
				top[0].intLastRec = intRecNo;
				document.forms[0].submit();
				ShowProgress();
			}
		}

	}

	function rowClick(strRowNo) {
		if(getFieldByID("chkAll"))	
		document.forms[0].chkAll.checked = false;
		document.forms[0].chkPage.checked = false;
		var intLength = invoiceData.length ;		
		for (var i = 0 ; i < intLength ; i++){
			if(IsTransEnable == 'true' || IsTransEnable == true){
		   		objDG.setCellValue(i, 10, "" );  
		   	}else{
		   		objDG.setCellValue(i, 9, "" );  
		   	}
		}
		if(IsTransEnable == 'true' || IsTransEnable == true){
			objDG.setCellValue(strRowNo, 10, "true");
		}else{
			objDG.setCellValue(strRowNo, 9, "true");
		}				
		Disable('btnSend',false);
		Disable('btnTrnsfer',true);	
		displayDownLoadReportBtn();
	}
	
	function clickAgencies(){	    
		if (getValue("selAgencies") == 'GSA'){
			Disable('chkTAs',false);
			Disable('chkCOs',false);
		}else if (getValue("selAgencies") == 'SGSA' || getValue("selAgencies") == 'TA'){
			Disable('chkTAs',false);
			Disable('chkCOs',true);
			if(getFieldByID("chkCOs"))
			getFieldByID("chkCOs").checked = false;			
		} else{
			Disable('chkTAs',true);
			if(getFieldByID("chkTAs"))
			getFieldByID("chkTAs").checked = false;
			Disable('chkCOs',true);
			if(getFieldByID("chkCOs"))
			getFieldByID("chkCOs").checked = false;			
		}	
	}

	function changeAgencies(){
		ls.clear();		
	}

	
	function getAgentClick() {
	
		if(getValue("selAgencies") == ""){
			showCommonError("Error",arrError["agentTypeRqrd"]);
			getFieldByID("selAgencies").focus();
		}else{
			setField("hdnMode","AGENTS");
			if(IsTransEnable == 'true' || IsTransEnable == true){
				var invoiceFrm = getText('txtInvoiceFrom');
				var invoiceTo = getText('txtInvoiceTo');
				var strSearchCriteria = invoiceFrm+"#"+invoiceTo+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+ls.getselectedData()+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
				document.getElementById("frmInvoiceEmail").target = "_self";
				document.forms[0].hdnMode.value="AGENTS";
				top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
				//top[0].manifestSearchValue = strSearchCriteria;
				document.forms[0].submit();
				ShowProgress();	
			}else{	
				var strSearchCriteria = getValue("selYear")+"#"+getValue("selMonth")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBP")+"# #"+getValue("chkZero")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
				top[0].manifestSearchValue = strSearchCriteria;
				setField("hdnAgents","");
				var objForm  = document.getElementById("frmInvoiceEmail");
				objForm.target = "_self";
				document.forms[0].submit();
				ShowProgress();
			}
		}
	
	}
	
	function closeClick() {
		if (top.loadCheck(top.pageEdited)) {
			setPageEdited(false);
			top[0].manifestSearchValue="";
			top[1].objTMenu.tabRemove("SC_FACM_006");
		}
	}
	
	function transferClick() {
	
		setField("hdnAgents",getAgents());
		setField("hdnSelectedAgents",getSelectedAgents());	

		setField("hdnMode","TRANSFER");
		if(IsTransEnable == 'true' || IsTransEnable == true){
			var invoiceFrm = getText('txtInvoiceFrom');
			var invoiceTo = getText('txtInvoiceTo');
			var strSearchCriteria = invoiceFrm+"#"+invoiceTo+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+ls.getselectedData()+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
			document.getElementById("frmInvoiceEmail").target = "_self";
			top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
			//top[0].manifestSearchValue = strSearchCriteria;			
			document.forms[0].submit();
			ShowProgress();	
		}else{
			var strSearchCriteria = getValue("selYear")+"#"+getValue("selMonth")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBP")+"#"+ls.getselectedData()+"#"+getValue("chkZero")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
			top[0].manifestSearchValue = strSearchCriteria;			
			var objForm  = document.getElementById("frmInvoiceEmail");
			objForm.target = "_self";
			document.forms[0].submit();
			ShowProgress();	
		}
	}
	
	function sendClick() {
	
		var strrec = top[0].intLastRec;
		setField("hdnAgents",getAgents());		
		
	
		setField("hdnMode","EMAIL");		
		setField("hdnRecNo", strrec);
		if(IsTransEnable == 'true' || IsTransEnable == true){
			setField("hdnSelectedAgents",getSelectedAgents());
			setField("hdnSelectedInvoices",getSelectedInvoiceNOs());
			var invoiceFrm = getText('txtInvoiceFrom');
			var invoiceTo = getText('txtInvoiceTo');
			
			var strSearchCriteria = invoiceFrm+"#"+invoiceTo+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+ls.getselectedData()+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
			top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
			document.getElementById("frmInvoiceEmail").target = "_self";
			//top[0].manifestSearchValue = strSearchCriteria;
			top[0].intLastRec = strrec;
			document.forms[0].submit();
			ShowProgress();	
		}else{
			setField("hdnSelectedAgents",getSelectedAgents());	
			//setField("hdnSelectedInvoices",getSelectedInvoiceNOs());
			var strSearchCriteria = getValue("selYear")+"#"+getValue("selMonth")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBP")+"#"+ls.getselectedData()+"#"+getValue("chkZero")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
			top[0].manifestSearchValue = strSearchCriteria;
			top[0].intLastRec = strrec;
			var objForm  = document.getElementById("frmInvoiceEmail");
			objForm.target = "_self";		
			document.forms[0].submit();
			ShowProgress();	
		}
	}
	
	function generateClick() {
		var message = "Invoice will be generated for "+getValue("selYear")+ " " +getValue("selMonth")+ " " +getValue("selBP")+" period.\r\n Do you wish to continue ?";
		var cnf = confirm(message);
		if(cnf) {			
			setField("hdnAgents",getAgents());
			setField("hdnMode","GENERATE");
			if(IsTransEnable == 'true' || IsTransEnable == true){
				var invoiceFrm = getText('txtInvoiceFrom');
				var invoiceTo = getText('txtInvoiceTo');
				var strSearchCriteria = invoiceFrm+"#"+invoiceTo+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+ls.getselectedData()+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
				document.getElementById("frmInvoiceEmail").target = "_self";
				top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
				//top[0].manifestSearchValue = strSearchCriteria;
				document.forms[0].submit();
				ShowProgress();	
			}else{
				
				var strSearchCriteria = getValue("selYear")+"#"+getValue("selMonth")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBP")+"#"+ls.getselectedData()+"#"+getValue("chkZero")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
				top[0].manifestSearchValue = strSearchCriteria;			
				var objForm  = document.getElementById("frmInvoiceEmail");
				objForm.target = "_self";
				document.forms[0].submit();
				ShowProgress();
			}
		}		
	
	}
	
	function chkvalidate() {
		var blnselected = false;
		var blnNoSelect = false;
		var seltdAgents = "";
		if(IsTransEnable == 'true' || IsTransEnable == true){
			seltdAgents = objDG.getSelectedColumn(10);
		}else{
			seltdAgents = objDG.getSelectedColumn(9);
		}
		Disable('btnTrnsfer',true);
		Disable('btnSend',true);
		disableViewReportBtn();
		if(getFieldByID("chkAll"))
		document.forms[0].chkAll.checked = false;
		for (var i =0 ;i < seltdAgents.length ;i++ ){			
			if(seltdAgents[i][1] == true) {
				blnselected = true;	
				break;
			}
		}
		for (var i =0 ;i < seltdAgents.length ;i++ ){			
			if(seltdAgents[i][1] != true) {
				blnNoSelect = true;	
				break;
			}
		}
		if(blnNoSelect) {			
			document.forms[0].chkPage.checked = false;
			
		}			
		if(blnselected == true) {			
			if(getFieldByID("chkAll") && !document.forms[0].chkAll.checked) {
				Disable('btnSend',false);
				displayDownLoadReportBtn();
			}
		}		
	}
	
	function getSelectedInvoiceNOs() {
		var strArray = ""
		var count = 0;
		var blnAgents = false;
		var selscedtobld ="";
		if(IsTransEnable == 'true' || IsTransEnable == true){
			selscedtobld = objDG.getSelectedColumn(10);		
		}else{
			selscedtobld = objDG.getSelectedColumn(9);
		}		
		for (var i =0 ;i < selscedtobld.length ;i++ ){			
			if(selscedtobld[i][1]) {				
				strArray += invoiceData[i][4] +",";
				blnAgents = true;
			}							
		}		
		return strArray;	
	
	}
	
	function getSelectedAgents() {
		var strArray = ""
		var count = 0;
		var blnAgents = false;
		var selscedtobld ="";
		if(IsTransEnable == 'true' || IsTransEnable == true){
			selscedtobld = objDG.getSelectedColumn(10);		
		}else{
			selscedtobld = objDG.getSelectedColumn(9);
		}		
		for (var i =0 ;i < selscedtobld.length ;i++ ){			
			if(selscedtobld[i][1]) {				
				strArray += invoiceData[i][1] +",";
				blnAgents = true;
			}							
		}		
		return strArray;	
	
	}
	
	
	function searchClick() {
	
		setField("hdnAgents",getAgents());
		var strSysDateArr=getSystemDate().split("/");
		if(IsTransEnable == 'true' || IsTransEnable == true){
			var invoiceFrm = getText('txtInvoiceFrom');
			var invoiceTo = getText('txtInvoiceTo');
			if (getFieldByID("txtInvoiceFrom").value==""){
				showCommonError("Error",arrError["fromDateEmpty"]);
				getFieldByID("txtInvoiceFrom").focus();
			} else if (getFieldByID("txtInvoiceTo").value==""){
					showCommonError("Error",arrError["toDateEmpty"]);
					getFieldByID("txtInvoiceTo").focus();
			} else if(getText("selAgencies")==""){
					showCommonError("Error",arrError["agentTypeRqrd"]);
					getFieldByID("selAgencies").focus();
			} else if(ls.getSelectedDataWithGroup()=="" && getText("selAgencies") != "All"){
				showCommonError("Error",arrError["agentsRqrd"]);
			}else {				
				setField("hdnMode","SEARCH");
				var strSearchCriteria = invoiceFrm+"#"+invoiceTo+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+ls.getselectedData()+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
				document.getElementById("frmInvoiceEmail").target = "_self";
				document.forms[0].hdnMode.value="SEARCH";
				top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
				//top[0].manifestSearchValue = strSearchCriteria;			
				document.forms[0].submit();
				ShowProgress();
			}
		}else{
			var numField=Number(getFieldByID("selMonth").value);
			var thisMonth=Number(strSysDateArr[1]);
			var selecYear = Number(getFieldByID("selYear").value);
			var thisyear = Number(strSysDateArr[2]);
			if (getFieldByID("selYear").value==""){
					showCommonError("Error",arrError["yearEmpty"]);
					getFieldByID("selYear").focus();
			} else if (getFieldByID("selMonth").value==""){
					showCommonError("Error",arrError["monthEmpty"]);
					getFieldByID("selMonth").focus();
			} else if(getText("selAgencies")==""){
					showCommonError("Error",arrError["agentTypeRqrd"]);
					getFieldByID("selAgencies").focus();
			} else if(ls.getSelectedDataWithGroup()=="" && getText("selAgencies") != "All"){
					showCommonError("Error",arrError["agentsRqrd"]);
			}else if(thisyear == selecYear && numField >thisMonth){
					showCommonError("Error",arrError["monthGreater"]);
			} else {	
					setField("hdnMode","SEARCH");
					var strSearchCriteria = getValue("selYear")+"#"+getValue("selMonth")+"#"+getValue("selAgencies")+"#"+getFieldByID("chkTAs").checked+"#"+getValue("selBP")+"#"+ls.getselectedData()+"#"+getValue("chkZero")+"#"+getFieldByID("chkCOs").checked+"#"+getValue("selEntity")+"#";
					document.getElementById("frmInvoiceEmail").target = "_self";
					document.forms[0].hdnMode.value="SEARCH";
					top[0].manifestSearchValue = strSearchCriteria;			
					document.forms[0].submit();
					ShowProgress();				
			}
		}
	
	}
	
	function setPageEdited(isEdited){
		top.pageEdited = isEdited;
	}
	
	
	function beforeUnload(){
		if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
			top[0].objWindow.close();
		}
	}
	
	function getSystemDate(){
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD} 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();
		return strSysDate;

	}
	
	function getAgents(){
		var strAgents=ls.getSelectedDataWithGroup();
		var newAgents;
		if(strAgents.indexOf(":")!=-1){
			strAgents=replaceall(strAgents,":" , ",");
		}
		if(strAgents.indexOf("|")!=-1){
			newAgents=replaceall(strAgents,"|" , ",");
		}else{
			newAgents=strAgents;
		}
		return newAgents;
	}
	
	function clickAll() {
	
		document.forms[0].chkPage.checked = false;		
		Disable('btnSend',true);
		disableViewReportBtn();
		var intLength = invoiceData.length ;
		
		if (getFieldByID("chkAll") && document.forms[0].chkAll.checked) {
			for (var i = 0 ; i < intLength ; i++){
				if(IsTransEnable == 'true' || IsTransEnable == true){
					objDG.setCellValue(i, 10, "" );  
				}else{
					objDG.setCellValue(i, 9, "" );  
				}
		   		
			}	
			Disable('btnTrnsfer',false);			
			document.forms[0].chkAll.checked = true;
		}else {
			Disable('btnTrnsfer',true);			
		}
	}
	
	function clickPage() {
		if(getFieldByID("chkAll"))
		document.forms[0].chkAll.checked = false;
		var intLength = invoiceData.length ;
		Disable('btnTrnsfer',true);
		if (document.forms[0].chkPage.checked) {			
			for (var i = 0 ; i < intLength ; i++){
				if(IsTransEnable == 'true' || IsTransEnable == true){
		   			objDG.setCellValue(i, 10, "true" );  
		   		}else{
		   			objDG.setCellValue(i, 9, "true" );  
		   		}
		 	}		 	
		 	Disable('btnSend',false);
		 	displayDownLoadReportBtn();
		 	document.forms[0].chkPage.checked = true;		 	
		}else {			
		 	for (var i = 0 ; i < intLength ; i++){
		 		if(IsTransEnable == 'true' || IsTransEnable == true){
		   			objDG.setCellValue(i, 10, "" );  
		   		}else{
		   			objDG.setCellValue(i, 9, "" );  
		   		}
		 	}			
		 	Disable('btnSend',true);	
		 	disableViewReportBtn();
		}
	}
	
	function validatePeriod() {		
	
		var strSysCur = getValue("hdnCurrentDate").split("/");		
		
		var numField=Number(getFieldByID("selMonth").value);
		var thisMonth=Number(strSysCur[1]);
		var selecYear = Number(getFieldByID("selYear").value);
		
		var thisyear = Number(strSysCur[2]);
		var period = Number(getFieldByID("selBP").value);
		var thisDay = Number(strSysCur[0]);
		var lastDay = getDaysInMonth(thisMonth,thisyear);
		if(selecYear == "" || numField == "" ) {
			Disable('btnGenerate',true);
			return;
		}
		if(selecYear < thisyear) {
			Disable('btnGenerate',false);
		} else if(selecYear > thisyear) {
			Disable('btnGenerate',true);
		}else {
		
			if(numField < thisMonth) {
				Disable('btnGenerate',false);
			}else if(numField > thisMonth) {
				Disable('btnGenerate',true);
			}else {			
				if(period == 1 && thisDay < 15) {
					Disable('btnGenerate',true);
				}else if(period == 2 && thisDay < lastDay) {
					Disable('btnGenerate',true);
				}else {
					Disable('btnGenerate',false);
				}
			}
		}
	
	}
	
	function buildTransactionUI(){
		var strHTML = '';
		strHTML += '<table width="80%" border="0" cellpadding="0" cellspacing="2"><tr>';
		strHTML += '<td width="20%"><font>Invoice Transaction From </font></td>';
		strHTML += '<td width="10%"><input name="txtInvoiceFrom" type="text" id="txtInvoiceFrom" size="10" style="width:80px;" onBlur="dateChk('+ "'txtInvoiceFrom'" +')" maxlength="10"></td>';
		strHTML += '<td width="5%" valign="bottom"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="From Date">';
		strHTML += '<img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>&nbsp;*</b></font> </td>';
		strHTML += '<td width="20%" ><font>&nbsp;&nbsp;&nbsp;Invoice Transaction To </font></td>';
		strHTML += '<td width="10%"><input name="txtInvoiceTo" type="text" id="txtInvoiceTo" size="10" style="width:80px;" onBlur="dateChk('+ "'txtInvoiceTo'" +')" maxlength="10" invalidText="true"></td>';
		strHTML += '<td width="35%" valign="bottom" ><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="TO Date">';
		strHTML += '<img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>&nbsp;*</b></font> </td>';
		strHTML += '</tr></table>';
		
		DivWrite('divTable', strHTML);
	}
	
	function viewReport(){
		if(IsTransEnable == 'true' || IsTransEnable == true){
			setField("hdnSelectedInvoices",getSelectedInvoiceNOs());			
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);	
			document.forms[0].target = "CWindow";
			document.forms[0].hdnMode.value = "REPORT";	
			document.forms[0].submit();
			top[2].HidePageMessage();
			document.forms[0].target="_self";
		}
	}
	
	function printReport (){
		if(IsTransEnable == 'true' || IsTransEnable == true){
			setField("hdnSelectedInvoices",getSelectedInvoiceNOs());			
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);	
			document.forms[0].target = "CWindow";			
			document.forms[0].hdnMode.value = "PRINT";	
			document.forms[0].submit();		
			tmOut = setTimeout("printIt()", 1000);
			top[2].HidePageMessage();				
			document.forms[0].target="_self";
		}
	}
			
	function printIt(){
		clearTimeout(tmOut);  
		if (top[0].objWindow.print) {
			top[0].objWindow.print() ;  
		}
	}
	
	function displayDownLoadReportBtn (){	
		if(isShowReport){
			Disable("btnViewReport", false);
			Disable("btnPrint",false);
		}else {
			Disable("btnViewReport",true);
			Disable("btnPrint",true);
		}
	}
	
	function disableViewReportBtn (){
		Disable("btnViewReport",true);
		Disable("btnPrint",true);
	}
	
	function clickEntity(){
		Disable("btnViewReport",true);
		Disable("btnPrint",true);
	}
	
	function changeEntity(){
		Disable("btnViewReport",true);
		Disable("btnPrint",true);
	}
