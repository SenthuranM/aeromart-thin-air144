	var screenId = "SC_FACM_006";
	var recstNo = top[0].intLastRec;
	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "8%";
	objCol1.arrayIndex = 1;
	//objCol1.toolTip = "Agent Code" ;
	objCol1.headerText = "Agent Code";
	objCol1.itemAlign = "left"
	objCol1.sort = "true"
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "20%";
	objCol2.arrayIndex = 2;
	//objCol2.toolTip = "Agent Name" ;
	objCol2.headerText = "Agent Name";
	objCol2.itemAlign = "left"
	objCol2.sort = "true"
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "8%";
	objCol3.arrayIndex = 3;
	//objCol3.toolTip = "Account Code" ;
	objCol3.headerText = "Account Code";
	objCol3.itemAlign = "left"	

	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "11%";
	objCol4.arrayIndex = 4;
	//objCol4.toolTip = "Invoice Number" ;
	objCol4.headerText = "Invoice Number";
	objCol4.itemAlign = "center"

	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "9%";
	objCol5.arrayIndex = 5;
	//objCol5.toolTip = "Invoice Amount" ;
	objCol5.headerText = "Invoice Amount";
	objCol5.itemAlign = "right"

	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "8%";
	objCol6.arrayIndex = 6;
	//objCol6.toolTip = "Invoice Date" ;
	objCol6.headerText = "Invoice Date";
	objCol6.itemAlign = "center"
	
	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "10%";
	objCol7.arrayIndex = 7;
	//objCol7.toolTip = "Status" ;
	objCol7.headerText = "Status";
	objCol7.itemAlign = "center"
	
	var objCol8 = new DGColumn();
	objCol8.columnType = "checkbox";
	objCol8.width = "6%";
	objCol8.arrayIndex = 9;
	objCol8.checkAll = false;	
	objCol8.linkOnClick = "chkvalidate";
	objCol8.headerText = "";
	objCol8.itemAlign = "center"
	
	var objCol9 = new DGColumn();
	objCol9.columnType = "label";
	objCol9.width = "5%";
	objCol9.arrayIndex = 8;	
	//objCol9.toolTip = "no of times" ;
	objCol9.headerText = "Emails";
	objCol9.itemAlign = "right"
	
	var objCol10 = new DGColumn();
	objCol10.columnType = "label";
	objCol10.width = "4%";
	objCol10.arrayIndex = 10;	
	//objCol10.toolTip = "Currency Code" ;
	objCol10.headerText = "C.C.";
	objCol10.itemAlign = "center"
		
	var objCol11 = new DGColumn();
	objCol11.columnType = "label";
	objCol11.width = "4%";
	objCol11.arrayIndex = 11;	
	//objCol10.toolTip = "PNR" ;
	objCol11.headerText = "PNR";
	objCol11.itemAlign = "center"
		
	var objCol12 = new DGColumn();
	objCol12.columnType = "label";
	objCol12.width = "9%";
	objCol12.arrayIndex = 12;	
	//objCol12.toolTip = "Entity" ;
	objCol12.headerText = "Entity";
	objCol12.itemAlign = "center"	


	// ---------------- Grid	
	var objDG = new DataGrid("spnInvoices");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol12);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol10);
	objDG.addColumn(objCol6);
	objDG.addColumn(objCol7);
	objDG.addColumn(objCol9);
	if(IsTransEnable == 'true' || IsTransEnable == true){
		objDG.addColumn(objCol11);
	}
	objDG.addColumn(objCol8);
	

	objDG.width = "100%";
	objDG.height = "120px";
	objDG.headerBold = false;
	objDG.rowSelect = true;
	objDG.arrGridData = invoiceData;
	objDG.seqNo = true;
	objDG.seqStartNo = recstNo;  //Grid
	objDG.pgnumRecTotal = totalNoOfRecords ; // remove as per return record size
	objDG.paging = true
	objDG.pgnumRecPage = 50;
	objDG.pgonClick = "gridNavigations";	
	objDG.rowClick = "rowClick";
	objDG.displayGrid();
	
	
	function writeTotal() {
		var strHTMLText = "";
		
		if(totalNoOfRecords == 0) {
			strHTMLText += '<table width="99%" border="0" cellpadding="0" cellspacing="0" ID="Table10" align="center"   style="position:absolute;left:580px;top:515px;>';
		}else {
			strHTMLText += '<table width="99%" border="0" cellpadding="0" cellspacing="0" ID="Table10" align="center"   style="position:absolute;left:580px;top:523px;>';
		}		
		strHTMLText += '<tr>';
		strHTMLText += '	<td align="left">';
		strHTMLText += '	<font><b>'+totString+'<\/b><\/font>';	
		strHTMLText += '	<\/td>';
		strHTMLText += '	<\/tr>';
		strHTMLText += '	<\/table>';
		 
		if(agentType !='GSA'){
			setVisible("forAll",true); 
			DivWrite("spnTotal",strHTMLText);
		}

	}

	writeTotal();

	