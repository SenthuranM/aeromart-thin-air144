function i18n_login(){}

var languageDatastore;

i18n_login.populateLanguageLabels = function(response) {

	setLanguageDataStore(response.languageData);
}

function setLanguageDataStore(languageData) {

	languageDatastore = languageData;
}

function getLoginLanguageData() {

	var data = {};
	$.ajax({
		type : "POST",
		url : "../public/loginLanguage.action",
		dataType : "json",
		
		data : data,
		async : false,
		success : i18n_login.populateLanguageLabels,
		cache : false
	});

}

function loadLanguage(language){
	
	$("#mainTableLogin").getLanguageForMenu(language);
	
	var data = {};
	data['prefLangunage'] = language;
	
	$.ajax({
		type : "POST",
		url : "../public/xbeLanguage.action",
		dataType : "json",
		
		data : data,
		async : false,
		success : "",
		cache : false
	});
	
}



$.fn.getLanguageForMenu = function(lang) {
	
	$(this).find("label,span,td,div,font, input").each(function( intIndex ){
		var messageId = $(this).attr("i18n_key");
		
		if(messageId){
			
			var i18Message = geti18nLoginMsg(messageId, lang);
			if(i18Message != null){
				
				if ($(this).attr("type") == "button"){ 
					$(this).val(i18Message);
				}else{
					$(this).text(i18Message);
				}
				
			}
					
        }
		
	});
}


var geti18nLoginMsg = function(key, lang){
	
	var langObj = languageDatastore[lang];
	return langObj[key];
	
	
}
