var captchaTxt;
var captchaEnabled = false;

function validate(){
	var blnStatus = true;
	var userId=trim(getValue("username_txt"));
	var password=trim(getValue("j_password"));
	captchaTxt=trim(getValue("captcha_txt"));
	
	setField("username_txt",userId);
	setField("j_password",password);
	setField("captcha_txt",captchaTxt);

	objMsg.MessageType = "Error";

	if (userId==""){
		objMsg.MessageText = raiseError("XBE-ERR-01","User ID") ;
		getFieldByID("username_txt").focus();
		ShowPageMessage();
		return false;
	}
	if (password==""){
		objMsg.MessageText = raiseError("XBE-ERR-01","Password") ;
		getFieldByID("j_password").focus();
		ShowPageMessage();
		return false;
	}
	
	if (captchaEnabled && captchaTxt==""){
		objMsg.MessageText = raiseError("XBE-ERR-01","Captcha") ;
		getFieldByID("captcha_txt").focus();
		ShowPageMessage();
		refreshCaptcha();
		return false;
	}
	
	if (captchaEnabled && !validateCaptcha()){
		objMsg.MessageText = raiseError("XBE-ERR-04","Captcha") ;
		getFieldByID("captcha_txt").focus();
		getFieldByID("captcha_txt").value = "";
		ShowPageMessage();
		refreshCaptcha();
		return false;
	}
	
	return true;
}
	
function login(){
	if (validate() && getValue("j_token") == "0"){
		setField("j_token","1");
		Disable("btnLogin",true);
		// Check Application parameter to append carrier prefix		
		if (request['isAllowCarrierPrefix'] != null && request['isAllowCarrierPrefix'] == 'false') {
			defaultAirlineId = "";
		}
		
		var modifiedUserName = "";
		if(!autologin){ //prepending the default Ariline code to the username if it is not a dry user login
			modifiedUserName = defaultAirlineId + getValue("username_txt").toUpperCase();
		}else{
			modifiedUserName = getValue("username_txt").toUpperCase();
		}
		setField("j_username", modifiedUserName + "@" + getValue("carrierCode") + "$XBETYPE$" + "|" + ($(window).height()|$(document).height()));		
		getFieldByID("formLogin").target = "_self";
		getFieldByID("formLogin").submit();
	}
}

function getLanguage(){
	var blnStatus = true;
	var language=trim(getValue("prefLangunage"));
	setField("prefLangunage",language);
	
}

function setLanguage(){
		getLanguage();
		Disable("btnSubmit",true);	
		getFieldByID("formLng").submit();
	
}

	
function onLoad(logStatus){
	
	captchaEnabled = isCaptchaEnabled();
	
	if (serverErrorMsg != null && serverErrorMsg != "") {	
		arrError['XBE-ERR-77'] = 'TAIR-90007: #1. ';	
		reloadLoginAction();
		objMsg.MessageText = raiseError("XBE-ERR-77",serverErrorMsg) ;	
		ShowPageMessage();	
	} else 	if (logStatus == "LOGINFAILED"){	
		reloadLoginAction();
		if(getText("hdnUserStatus") == "true"){
			objMsg.MessageText = raiseError("XBE-ERR-94") ;
		} else {
			objMsg.MessageText = raiseError("XBE-ERR-04","User ID / Password") ;
		}
		ShowPageMessage();	
	} 
		
	
	window.focus();
	getFieldByID("username_txt").focus();
  if (captchaEnabled) {
    document.getElementById('spnWelcome').innerHTML = "To access the " + carrier + " online site, please enter your <b>User ID</b> , <b>Password</b> and <b>Captcha</b> and click <b>Login</b>.";
  } else {
    document.getElementById('spnWelcome').innerHTML = "To access the " + carrier + " online site, please enter your <b>User ID</b> and <b>Password</b> and click <b>Login</b>.";
  }
	document.getElementById('carrierCode')
	//setTimeout('window.location.replace("../public/showError408")',120000);
}

function fullScreen(strID){
	var strFileName = 'showLogin.action'
	
	switch (strID){
		case 0 :
			window.open(strFileName, 'winCC', 'fullscreen=yes, scrollbars=no');
			break;
		default :
			window.open(strFileName,"winCC",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=1024,height=768,resizable=no,top=1,left=1');
			break;
	}
}
	
function fullScreen2(){
	var strFileName=request['secureLoginUrl'];
	//var strFileName="../private/showXBEMain.action";
	var win;

	if(window.name!="winCC"){	
		//if(strFileName=='null'||strFileName==""){
		//if(strFileName.indexOf("https://") != -1){
		if(request['useSecureLogin'] && (window.location.href.indexOf("https://") == -1) && (strFileName.indexOf("https://") != -1)){
			strFileName=window.location.href;
		}

		reOpen=true;

		if(top.name=='winCC'){
			top.location=strFileName;
		}else{
			if(window.opener&&!window.opener.closed){
				win=window.opener.top;
				if(!win.closed){
					win.location=strFileName;
				}else{
					window.open(strFileName, 'winCC', 'fullscreen=yes, scrollbars=no');
				}
				window.close();
			}else{
				window.open(strFileName, 'winCC', 'fullscreen=yes, scrollbars=no');
			}
		}
	}
	else if((window.location.href.indexOf("https://") == -1) && (strFileName.indexOf("https://") != -1)){ 
		//if(strFileName.indexOf("https://") != -1){
		//else if(strFileName!='null'&&strFileName!=""){
		window.location.replace(strFileName);
	}
}

function onEnterKeyPressed(){
	
}

function CloseWindow(){
	//window.close();
}

function onAutoSubmit(objEvent){
	var strKeyValue = objEvent.keyCode ;
	if (strKeyValue == 13){
		login();
	}
}

function isCaptchaEnabled(){
	
	var data = {};
	var isCaptchaEnabled = false;
	$.ajax({
		type : "POST",
		url : "../public/showUserCaptcha.action",
		dataType : "json",
		data : data,
		async : false,
		success: function (response) {
			
			if (response.captchaEnabled){
				isCaptchaEnabled = true;
			}

		}
	});
	
	return isCaptchaEnabled;
	
}

function validateCaptcha(){
	
	var data = {};
	var isCaptchaValidated = false;
	data['captchaTxt'] = captchaTxt;
	$.ajax({
		type : "POST",
		url : "../public/xbeUserCaptchaValidation.action",
		dataType : "json",
		data : data,
		async : false,
		success: function (response) {
			isCaptchaValidated = response.captchaValidated;
		}
	});
	
	return isCaptchaValidated;
}

function refreshCaptcha() {
    $("#txtCaptcha").val("");
    document['imgCPT'].src = '../jcaptcha_new?relversion=' + (new Date()).getTime();
}
