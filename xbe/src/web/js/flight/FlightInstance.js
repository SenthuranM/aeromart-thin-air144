var invScreenID = "SC_RESV_CC_016";
var tabValue = getTabValue();
var flightNo;
var fltDate;
var fltRoute;
var objProgressCheck;
var isPaxDetail;
var strGridRow;
var strRowData;

var strSunday = "";
var strMonday = "";
var strTuesday = "";
var strWednesday = "";
var strThursday = "";
var strFriday = "";
var strSaturday = "";
var hdnMode = "";
var fliData = new Array();
var hdnRecNo = 1;
var totalRecNo = 0;
var recstNo = 1;
var cabinClasses;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtStartDateSearch", strDate);
		break;
	case "1":
		setField("txtStopDateSearch", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	if (strID == 0) {
		objCal1.top = 20;
		objCal1.left = 75;
	} else if (strID == 1) {
		objCal1.top = 20;
		objCal1.left = 175;
	}
	objCal1.onClick = "setDate";
	if ((strID == 0) && (!getFieldByID("txtStartDateSearch").disabled)) {
		objCal1.showCalendar(objEvent);
	} else if ((strID == 1) && (!getFieldByID("txtStopDateSearch").disabled)) {
		objCal1.showCalendar(objEvent);
	} else if (blnCalander) {
		objCal1.showCalendar(objEvent);
	}
}

function LoadCalendarNew(strID, objEvent) {
	objCal.top = 0;
	objCal.left = 20;
	objCal.ShowCalendar(objEvent);
	objCal.onClick = "setDate";
}

function dataChanged() {
	setPageEdited(true);
}

// call the functoion onload
function winOnLoad(strMsg, strMsgType, strWarnMsg) {
	//document.getElementById("txtFlightNoStartSearch").innerHTML = parent.carrierCodeOptions;
	isPaxDetail = parent.isPrintPaxDet;
	// FIXME
	// Disable("btnLoad", true);
	document.getElementById('btnLoad').style.visibility='hidden';

	setField("selOperationTypeSearch", parent.defaultOperationType); // Search
	setField("selStatusSearch", parent.defaultStatus);
	setField("txtStartTime", "00:00");
	setField("txtEndTime", "23:59");
	setField("txtMaxInwardConnTime",connLimit);
	setField("txtMaxOutwardConnTime",connLimit);
	setField("txtInwardFlightNoPrefix", getFieldByID("txtFlightNoStartSearch").value);
	setField("txtOutwardFlightNoPrefix", getFieldByID("txtFlightNoStartSearch").value);
	setField("txtFlightNoStartSearch", parent.defCarrCode);
	strMsg = parent.strMsg;
	setPageEdited(false);
	clearSearchSection();
	Disable("btnPreview", true);
	Disable("btnEmail", true);
	Disable("txtEmailId", true);
	
	if(top.isLogicalCCEnabled &&
			isMultipleLogicalCCAvailable()){
		
		setField("hdnCosType", 'lcc');
				
		cabinClasses = new Array(top.arrLogicalCC.length + 1);
		cabinClasses[0] = new Array("All","All");
		
		for(var i = 1; i < cabinClasses.length;i++){
			cabinClasses[i] = new Array(top.arrLogicalCC[i-1][0],top.arrLogicalCC[i-1][1]);
		}
		
		DivWrite("classOfServiceSelect",generateSelectionList(cabinClasses,"selClassOfService", false));		
	} else if(!top.isLogicalCCEnabled &&
			top.arrCos.length > 1){
		setField("hdnCosType", 'cos');
		cabinClasses = new Array(top.arrCos.length + 1);
		cabinClasses[0] = new Array("All","All");
		
		for(var i = 1; i < cabinClasses.length;i++){
			cabinClasses[i] = new Array(top.arrCos[i-1][0],top.arrCos[i-1][1]);
		}
		
		DivWrite("classOfServiceSelect",generateSelectionList(cabinClasses,"selClassOfService", false));
	}
		
	if (getTabValue() != "" && getTabValue() != null) {
		var strSearch = getTabValue().split(",");
		setField("txtStartDateSearch", strSearch[0]);
		setField("txtStopDateSearch", strSearch[1]);
		setField("txtFlightNoStartSearch", strSearch[7]);
		setField("txtFlightNoSearch", strSearch[2]);
		setField("selFromStn6", strSearch[3]);
		getFieldByID("selFromStn6").selected = strSearch[3];
		setField("selToStn6", strSearch[4]);
		getFieldByID("selToStn6").selected = strSearch[4];
		setField("selOperationTypeSearch", strSearch[5]);
		getFieldByID("selOperationTypeSearch").selected = strSearch[5];
		setField("selStatusSearch", strSearch[6]);
		getFieldByID("selStatusSearch").selected = strSearch[6];
		if (strSearch[9] != "") {
			setField("txtStartTime", strSearch[9]);
		}
		if (strSearch[10] != "") {
			setField("txtEndTime", strSearch[10]);
		}

		if (strSearch[11] == "true")
			document.getElementById("chkSunday").checked = true;
		if (strSearch[12] == "true")
			document.getElementById("chkMonday").checked = true;
		if (strSearch[13] == "true")
			document.getElementById("chkTuesday").checked = true;
		if (strSearch[14] == "true")
			document.getElementById("chkWednesday").checked = true;
		if (strSearch[15] == "true")
			document.getElementById("chkThursday").checked = true;
		if (strSearch[16] == "true")
			document.getElementById("chkFriday").checked = true;
		if (strSearch[17] == "true")
			document.getElementById("chkSaturday").checked = true;
		if (strMsg != null && strMsg != "" && strMsg != "Undefined") {
			showERRMessage(strMsg);
		}
		ClearProgressbar();
	}
}

function isMultipleLogicalCCAvailable(){
	if(top.arrLogicalCC != null){
		var count = 0;
		for(var i=0;i<top.arrLogicalCC.length;i++){
			var logicalCC = top.arrLogicalCC[i][0];
			var lccArr = logicalCC.split('-');
			if(lccArr.length > 1){
				count++;
			}
			
			if(count > 1){
				return true;
			}
		}
	}
	
	return false;
}

function ClearProgressbar() {
	if (objDG.loaded) {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
		top.blnProcessCompleted = false;
	}
}

function SearchData() {

	// once search button was pressed set the hidden value to "SEARCH"
	if (SearchValid() == false)
		return;
	top.blnProcessCompleted = true;
	setSearchCriteria();
	setSearchRecNo(1);
	hdnRecNo =1;
	document.forms[0].hdnMode.value = "SEARCH";
	hdnMode = "SEARCH";
	remoteSubmit();
}

// Validtaions when searching
function SearchValid() {
	// Mandatory fields
	if (getFieldByID("txtStartDateSearch").value == "") {
		showERRMessage(parent.arrError['startdate']);
		return false;
	}
	if (getFieldByID("txtStopDateSearch").value == "") {
		showERRMessage(parent.arrError['stopdate']);
		return false;
	}
	if (!dateChk("txtStartDateSearch")) {
		showERRMessage(parent.arrError['invalidDate']);
		getFieldByID("txtStartDateSearch").focus();
		return false;
	}
	if (!dateChk("txtStopDateSearch")) {
		showERRMessage(parent.arrError['invalidDate']);
		getFieldByID("txtStopDateSearch").focus();
		return false;
	}
	if (getFieldByID("txtFlightNoStartSearch").value == "") {
		showERRMessage(parent.arrError['invalidCarrierCode']);
		getFieldByID("txtFlightNoStartSearch").focus();
		return false;
	}

	// compare start date and stop date
	var schStartD = getFieldByID("txtStartDateSearch").value;
	var schStopD = getFieldByID("txtStopDateSearch").value;

	if (!CheckDates(schStartD, schStopD)) {
		showERRMessage(parent.arrError['stopdategreat']);
		return false;
	}
	if (((trim(getFieldByID("selFromStn6").value) == "") && (trim(getFieldByID("selToStn6").value) == ""))
			&& (trim(getFieldByID("txtFlightNoSearch").value) == "")) {
		showERRMessage(parent.arrError['invalidSearch']);
		return false;
	}

	return true;
}

function timeCheck(id, obj) {
	if (!isTimeFormatValid(obj)) {
		switch (id) {
		case 0:
			msg = parent.arrError['invalidConnectionLimit'];
			break;
		case 1:
			msg = parent.arrError['invalidStartTime'];
			break;
		case 2:
			msg = parent.arrError['invalidEndTime'];
			break;
		}
		showERRMessage(msg);
		getFieldByID(obj).focus();
		return false;
	}
	return true;
}

function isTimeFormatValid(objTime) {
	// Checks if time is in HH:MM format - HH can be more than 24 hours
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = getValueTrimed(objTime).match(timePattern);
	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + getValueTrimed(objTime));
	}
	return true;
}

function getValueTrimed(strValue) {
	return trim(getValue(strValue));
}

// clear the search area
function clearSearchSection() {
	setField("txtFlightNoSearch", "");
	setField("selFromStn6", "");
	setField("selToStn6", "");
}

// hide the message
function objOnFocus() {
	top[2].HidePageMessage();
}

function gridNavigations(intRecNo, intErrMsg) {
	if (intRecNo <= 0) {
		intRecNo = 1;
	}
	setSearchRecNo(intRecNo);
	setSearchCriteria()
	setField("hdnRecNo", intRecNo);
	hdnRecNo = intRecNo;
	hdnMode = "SEARCH";
	if (intErrMsg != "") {
		showERRMessage(intErrMsg);
	} else {
		setPageEdited(false);
		remoteSubmit();
	}
}

function sendEmail() {

	var validated = true;
	var requestParams = generateParamString();
	
	if(requestParams != null && requestParams != "") {
	
		if (getValue("txtEmailId") == "") {
			showERRMessage(parent.arrError["emailEmpty"]);
			getFieldByID("txtEmailId").focus();
			validated = false;
		}
	
		var strChkEmpty = checkInvalidChar(getValue("txtEmailId"),
				parent.arrError['emailInvalid'], "");
		if (strChkEmpty != "") {
			showERRMessage(strChkEmpty);
			getFieldByID("txtEmailId").focus();
			validated = false;
		}
	
		if (getValue("txtEmailId") != "") {
			if (!checkEmail(getValue("txtEmailId"))) {
				showERRMessage(parent.arrError['emailInvalid']);
				getFieldByID("txtEmailId").focus();
				validated = false;
			}
		}
	} else {
		validated = false;
	}

	if (validated) {
		top[2].HidePageMessage();
		flightNo = fliData[strGridRow][1];
		hdnMode = "EMAIL";
		requestParams += "&hdnMode="+hdnMode+"&hdnIsPaxDetails="+ isPaxDetail 
		requestParams += "&hdnEmailID=" + getValue("txtEmailId");	
		showManifestOptionsWindow(0);
		parent.ShowProgress();
		makePOST("showFlightDetail.action", requestParams);
				
	}

}

function previewManifest() {
	
	var requestParamString = "";

	requestParamString = generateParamString();
	if(requestParamString != null && requestParamString != "") {
	
		var intWidth = 1024;
		var intHeight = 1024;
		var strProp = 'toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,width='
				+ intWidth
				+ ',height='
				+ intHeight
				+ ',resizable=yes,top='
				+ ((window.screen.height - intHeight) / 2)
				+ ',left='
				+ (window.screen.width - intWidth) / 2;

		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmF");
		objForm.target = "CWindow";
		hdnMode = "VIEW";
		objForm.action = "showFlightDetail.action?hdnLive="+dataSource+"&hdnMode="+hdnMode+"&"+requestParamString;
		objForm.submit();
		showManifestOptionsWindow(0);
			
	}

}

function viewAndDownloadManifest() {
	
	var requestParamString = "";

	requestParamString = generateParamString();
	if(requestParamString != null && requestParamString != "") {
	
		var intWidth = 1024;
		var intHeight = 1024;
		var strProp = 'toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,width='
				+ intWidth
				+ ',height='
				+ intHeight
				+ ',resizable=yes,top='
				+ ((window.screen.height - intHeight) / 2)
				+ ',left='
				+ (window.screen.width - intWidth) / 2;

		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmF");
		objForm.target = "CWindow";
		hdnMode = "DOWNLOAD";
		objForm.action = "showFlightDetail.action?hdnMode="+hdnMode+"&"+requestParamString;
		objForm.submit();
		showManifestOptionsWindow(0);
			
	}

}

function showManifestOptionsWindow(show) {
	
	var optionsDiv = getFieldByID("manifestOptions");
	var windowDiv = getFieldByID("manifestWindow");
	var cosDiv = getFieldByID("classOfService");
	
	if(show){			
		fltRoute = trim(fliData[strGridRow][5]);
		
		var airports = fltRoute.split("/");
		var airportData = new Array(airports.length);
		for(var i in airports){
			airportData[i] = new Array(airports[i],airports[i]);
		}
		DivWrite("inwardBoardingAirport",generateSelectionList(airportData,"selInwardBoardingAirport",true));
		DivWrite("outwardBoardingAirport",generateSelectionList(airportData,"selOutwardBoardingAirport",true));
		
		//cabin class code array		
		if(top.isLogicalCCEnabled &&
				isMultipleLogicalCCAvailable()){
			cosDiv.style.display = "block";
		} else if(!top.isLogicalCCEnabled &&
				top.arrCos.length > 1){
			cosDiv.style.display = "block";
		}
		
		//Reset previously set values of input fields to default
		windowDiv.style.display = "block";
		optionsDiv.style.display = "block";
		
		if(window.parent.showPSPTFilter){ 
			document.getElementById('psptFilter').style.display = "block";
		} else {
			document.getElementById('psptFilter').style.display = "none";
		}
		
		if(window.parent.showDOCSFilter){
			document.getElementById('docsFilter').style.display = "block";
		} else {
			document.getElementById('docsFilter').style.display = "none";
		}
		
		setField("txtInwardFlightNoPrefix", getFieldByID("txtFlightNoStartSearch").value);
		setField("txtOutwardFlightNoPrefix", getFieldByID("txtFlightNoStartSearch").value);
		
	} else {
		cosDiv.style.display = "none";
		optionsDiv.style.display = "none";
		windowDiv.style.display = "none";
		//Clear data as well
		resetManifestOptions();
		
	}

}

function resetManifestOptions() {

	setField("txtMaxInwardConnTime",connLimit);
	setField("selInwardOrigin","");
	setField("selInwardBoardingAirport","");
	setField("txtInwardFlightNoPrefix",getFieldByID("txtFlightNoStartSearch").value);
	setField("txtInwardFlightNo","");
	setField("txtMaxOutwardConnTime",connLimit);
	setField("selOutwardDestination","");
	setField("selOutwardBoardingAirport","");
	setField("txtOutwardFlightNoPrefix",getFieldByID("txtFlightNoStartSearch").value);
	setField("txtOutwardFlightNo","");
	setField("radSortOption",'Pnr');
	setField("selPassport",'any');
	setField("radViewOption",'detailed');
	setField("radSummaryOption",'segment');
	setField("radSummaryPosition",'top');
	setField("txtEmailId","");
	setField("selPassport","ANY");
	setField("selDOCS","ANY");
	
	if(top.isLogicalCCEnabled &&
			isMultipleLogicalCCAvailable()){
		setField("selClassOfService","All");
	} else if(!top.isLogicalCCEnabled &&
			top.arrCos.length > 1){
		setField("selClassOfService","All");
	}
	
	setChecked("chkInwardConn",true);
	setChecked("chkOutwardConn",true);
	onCheckClick(true);
	onCheckClick(false);
	

}

function onCheckClick(isInward) {


	if(isInward) {
	
		if(getFieldByID("chkInwardConn").checked) {
			
			getFieldByID("txtMaxInwardConnTime").disabled = false;
			getFieldByID("selInwardOrigin").disabled = false;
			getFieldByID("selInwardBoardingAirport").disabled = false;
			getFieldByID("txtInwardFlightNoPrefix").disabled = false;
			getFieldByID("txtInwardFlightNo").disabled = false;
			
		} else {
			setField("txtMaxInwardConnTime",connLimit);
			setField("selInwardOrigin","");
			setField("selInwardBoardingAirport","");
			setField("txtInwardFlightNoPrefix",getFieldByID("txtFlightNoStartSearch").value);
			setField("txtInwardFlightNo","");
			getFieldByID("txtMaxInwardConnTime").disabled = true;
			getFieldByID("selInwardOrigin").disabled = true;
			getFieldByID("selInwardBoardingAirport").disabled = true;
			getFieldByID("txtInwardFlightNoPrefix").disabled = true;
			getFieldByID("txtInwardFlightNo").disabled = true;
		}
	
	} else {
		if(getFieldByID("chkOutwardConn").checked) {
			
			getFieldByID("txtMaxOutwardConnTime").disabled = false;
			getFieldByID("selOutwardDestination").disabled = false;
			getFieldByID("selOutwardBoardingAirport").disabled = false;
			getFieldByID("txtOutwardFlightNoPrefix").disabled = false;
			getFieldByID("txtOutwardFlightNo").disabled = false;
			
		} else {
			setField("txtMaxOutwardConnTime",connLimit);
			setField("selOutwardDestination","");
			setField("selOutwardBoardingAirport","");
			setField("txtOutwardFlightNoPrefix",getFieldByID("txtFlightNoStartSearch").value);
			setField("txtOutwardFlightNo","");
			getFieldByID("txtMaxOutwardConnTime").disabled = true;
			getFieldByID("selOutwardDestination").disabled = true;
			getFieldByID("selOutwardBoardingAirport").disabled = true;
			getFieldByID("txtOutwardFlightNoPrefix").disabled = true;
			getFieldByID("txtOutwardFlightNo").disabled = true;
		}
	
	}

}



function generateParamString() {
	
	var validated = false;
	
	top[2].HidePageMessage();
	flightNo = fliData[strGridRow][1];
	fltDate = fliData[strGridRow][2];
	fltRoute = fliData[strGridRow][5];
	var fltID = fliData[strGridRow][16];
	
	var sortOpt = getText("radSortOption");
	var viewOpt	= getText("radViewOption");
	var summaryOpt	= getText("radSummaryOption");
	var summaryPos	= getText("radSummaryPosition");
	var cos = getValueTrimed("selClassOfService");
	var cosType = getValueTrimed("hdnCosType");
	var passportStatus = getValueTrimed("selPassport");
	var docsInfoStatus = getValueTrimed("selDOCS");
	var inwardChecked = getChecked("chkInwardConn");
	var outwardChecked = getChecked("chkOutwardConn");
	var inwardFlightNo="";
	var outwardFlightNo="";


	if((getValueTrimed("txtInwardFlightNoPrefix") != null && getValueTrimed("txtInwardFlightNoPrefix") != "") && (getValueTrimed("txtInwardFlightNo") != null && getValueTrimed("txtInwardFlightNo") != "")) {
		inwardFlightNo = getValueTrimed("txtInwardFlightNoPrefix")+getValueTrimed("txtInwardFlightNo"); 
	} 

	if((getValueTrimed("txtOutwardFlightNoPrefix") != null && getValueTrimed("txtOutwardFlightNoPrefix") != "") && (getValueTrimed("txtOutwardFlightNo") != null && getValueTrimed("txtOutwardFlightNo") != "")) {
		outwardFlightNo = getValueTrimed("txtOutwardFlightNoPrefix")+getValueTrimed("txtOutwardFlightNo"); 
	} 

	
	var requestParamString = "hdnFlightID="+ fltID + "&hdnFlightNo="+flightNo+"&hdnFltRoute="+fltRoute+"&hdnFltDate="+fltDate+"&";
	
	
	if (inwardChecked && outwardChecked) {
		// both
		
		validated = ((validateConnectionTime("txtMaxInwardConnTime"))&& (validateConnectionTime("txtMaxOutwardConnTime")));
		
		if(validated) {
			requestParamString += "hdnConnMode=both&";
			requestParamString += "hdnInConnLimit="+getValueTrimed("txtMaxInwardConnTime")+"&";
			requestParamString += "hdnInOrigin="+getValueTrimed("selInwardOrigin")+"&";
			requestParamString += "hdnInBoard="+getValueTrimed("selInwardBoardingAirport")+"&";
			requestParamString += "hdnInFlightNo="+inwardFlightNo+"&";
			requestParamString += "hdnOutConnLimit="+getValueTrimed("txtMaxOutwardConnTime")+"&";
			requestParamString += "hdnOutDest="+getValueTrimed("selOutwardDestination")+"&";
			requestParamString += "hdnOutBoard="+getValueTrimed("selOutwardBoardingAirport")+"&";
			requestParamString += "hdnOutFlightNo="+outwardFlightNo+"&";
		}

	} else if(inwardChecked) {
		//add inward parameters			
		validated = validateConnectionTime("txtMaxInwardConnTime")
		
		if(validated) {
			requestParamString += "hdnConnMode=in&";
			requestParamString += "hdnInConnLimit="+getValueTrimed("txtMaxInwardConnTime")+"&";
			requestParamString += "hdnInOrigin="+getValueTrimed("selInwardOrigin")+"&";
			requestParamString += "hdnInBoard="+getValueTrimed("selInwardBoardingAirport")+"&";
			requestParamString += "hdnInFlightNo="+inwardFlightNo+"&";
		}
		
	} else if (outwardChecked) {
		// add outward parameters
		validated = validateConnectionTime("txtMaxOutwardConnTime")
		
		if(validated) {
			requestParamString += "hdnConnMode=out&";
			requestParamString += "hdnOutConnLimit="+getValueTrimed("txtMaxOutwardConnTime")+"&";
			requestParamString += "hdnOutDest="+getValueTrimed("selOutwardDestination")+"&";
			requestParamString += "hdnOutBoard="+getValueTrimed("selOutwardBoardingAirport")+"&";
			requestParamString += "hdnOutFlightNo="+outwardFlightNo+"&";
		}
	
	} else {
		//none
		requestParamString += "hdnConnMode=none&";
		validated = true;
	}
	
	if(validated) {
		
		requestParamString += "hdnSort="+sortOpt+"&";
		requestParamString += "hdnView="+viewOpt+"&";
		requestParamString += "hdnSummaryMode="+summaryOpt+"&";
		requestParamString += "hdnSummaryPosition="+summaryPos+"&";
		requestParamString += "hdnCos="+cos+"&";
		requestParamString += "hdnCosType="+cosType+"&";
		requestParamString += "hdnPassportStatus="+passportStatus+"&";
		requestParamString += "hdnDOCSInfoStatus="+docsInfoStatus;
		
		
		
		return requestParamString;
	} 
	
	return "";
}


function generateSelectionList(data,selectId, isWithBlankOption) {
	
	var strHtml="<select id='"+selectId+"' name='"+selectId+"'>" ;
	
	if(isWithBlankOption) {
		strHtml += "<option value=''></option>" ;
	}
	
	for(var i = 0 ; i < data.length ; i++) {
		
		strHtml += "<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
		
	}
	
	strHtml += "</select>";	
	return strHtml;
	
}

function validateConnectionTime(input){

var errMsg = checkMaxtime(input);
if (isEmpty(getValueTrimed(input))) {
	showERRMessage(parent.arrError['connectionlimitEmpty']);
	getFieldByID(input).focus();
	
	return false;
}

if (!isEmpty(getValueTrimed(input))) {
	if (!timeCheck(0, input)) {
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
		return false;
	}
	if (errMsg != "") {
		showERRMessage(errMsg);
		getFieldByID(input).focus();
		return false;
	}
}
return true;

}

function chkClick() {
	// setPageEdited(true);
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function isPageEdited() {
	if (top.pageEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		return cnf;
	} else {
		return true;
	}
}

function gridClick(strRowNo) {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		objOnFocus();
		strGridRow = strRowNo;
		
		Disable("btnPreview", false);
		Disable("btnEmail", false);
		Disable("txtEmailId", false);
	}

}

function getTabValue() {
	var tabValue = top[0].manifestSearchValue;
	if ((tabValue == "") || (tabValue == null)) {
		tabValue = new Array("");
	}
	return tabValue;
}

function setSearchCriteria() {

	if (document.getElementById("chkSunday").checked == true) {
		strSunday = "true";
	}
	if (document.getElementById("chkMonday").checked == true) {
		strMonday = "true";
	}
	if (document.getElementById("chkTuesday").checked == true) {
		strTuesday = "true";
	}
	if (document.getElementById("chkWednesday").checked == true) {
		strWednesday = "true";
	}
	if (document.getElementById("chkThursday").checked == true) {
		strThursday = "true";
	}
	if (document.getElementById("chkFriday").checked == true) {
		strFriday = "true";
	}
	if (document.getElementById("chkSaturday").checked == true) {
		strSaturday = "true";
	}

	top[0].manifestSearchValue = getFieldByID("txtStartDateSearch").value + ","
			+ getFieldByID("txtStopDateSearch").value + ","
			+ getFieldByID("txtFlightNoSearch").value + ","
			+ getFieldByID("selFromStn6").value + ","
			+ getFieldByID("selToStn6").value + ","
			+ getFieldByID("selOperationTypeSearch").value + ","
			+ getFieldByID("selStatusSearch").value + ","
			+ getFieldByID("txtFlightNoStartSearch").value + ","
			+ getFieldByID("txtStartTime").value + ","
			+ getFieldByID("txtEndTime").value + "," + strSunday + ","
			+ strMonday + "," + strTuesday + "," + strWednesday + ","
			+ strThursday + "," + strFriday + "," + strSaturday;

}

function setSearchRecNo(recNo) {
	top[0].intLastRec = recNo;
}

function resetClick() {
	clearSearchSection();
	setField("selOperationTypeSearch", defaultOperationType);
	setField("selStatusSearch", defaultStatus);
}

function checkMaxtime(strConnId) {

	if (trim(connLimit) != "") {
		var arrtime = parent.connLimit.split(":");
		var timeInMin = ((Number(arrtime[0]) * 60) + Number(arrtime[1]));
		var contime = getText(strConnId);
		var cnTme = contime.split(":");
		var ContmMin = ((Number(cnTme[0]) * 60) + Number(cnTme[1]));
		if (ContmMin > timeInMin) {
			return parent.arrError['maxtimeexceed'];
		} else if (trim(minConnLimit) != "") {
			var arrtime1 = parent.minConnLimit.split(":");
			var timeInMin1 = ((Number(arrtime1[0]) * 60) + Number(arrtime1[1]));
			var contime1 = getText(strConnId);
			var cnTme1 = contime1.split(":");
			var ContmMin1 = ((Number(cnTme1[0]) * 60) + Number(cnTme1[1]));
			if (ContmMin1 < timeInMin1) {
				return parent.arrError['lessthanMinTime'];
			}
		}
	}
	return "";
}

function checkTime(first, second) {
	var fstArr = first.split(":");
	var scdArr = second.split(":");
	if (Number(fstArr[0]) < Number(scdArr[0]))
		return true;
	else {
		if (Number(fstArr[1]) < Number(scdArr[1]))
			return true;
		else
			showERRMessage(parent.arrError['endtimeless']);
		return false;
	}
}

function loadClick() {

	if (SearchValid() == false)
		return;
	
	if (trim(getFieldByID("txtStartTime").value) != "") {
		if (!timeCheck(1, "txtStartTime"))
			return;
		var fstArr = getFieldByID("txtStartTime").value.split(":");
		if (Number(fstArr[0]) < 0 || Number(fstArr[0]) > 23) {
			showERRMessage(parent.arrError['invalidStartTime']);
			return;
		}
	}
	if (trim(getFieldByID("txtEndTime").value) != "") {
		if (!timeCheck(2, "txtEndTime"))
			return;
		var fstArr = getFieldByID("txtEndTime").value.split(":");
		if (Number(fstArr[0]) < 0 || Number(fstArr[0]) > 23) {
			showERRMessage(parent.arrError['invalidEndTime']);
			return;
		}
	}
	// compare start date and stop date
	var schStartD = getFieldByID("txtStartDateSearch").value;
	var schStopD = getFieldByID("txtStopDateSearch").value;
	var schStartT = getFieldByID("txtStartTime").value;
	var schStopT = getFieldByID("txtEndTime").value;
	var dStrDateTime;
	var dEndDateTime;
	
	if (CheckDates(schStartD, schStopD) && CheckDates(schStopD, schStartD)) {		
		if (trim(getFieldByID("txtStartTime").value) != ""
				&& trim(getFieldByID("txtEndTime").value) != "") {
			if (!checkTime(getFieldByID("txtStartTime").value,
					getFieldByID("txtEndTime").value))
				return;
		}				
	}
	
	if(schStartD != "" && schStopD != ""){		
		var sStartDateArr = schStartD.split("/");   
		var sEndDateArr   = schStopD.split("/");
		
		dStrDateTime = new Date(sStartDateArr[2], sStartDateArr[1]-1, sStartDateArr[0]);
		dEndDateTime = new Date(sEndDateArr[2], sEndDateArr[1]-1, sEndDateArr[0]);
		
		if(schStartT != null && trim(schStartT) != ""){				
			var sStrtTimeArr = schStartT.split(":");
			dStrDateTime.setHours(sStrtTimeArr[0]);
			dStrDateTime.setMinutes(sStrtTimeArr[1]);
		} else {			
			dStrDateTime.setHours(0,0);
		}		
		
		if(schStopT != null && trim(schStopT) != ""){			
			var sEndTimeArr = schStopT.split(":");
			dEndDateTime.setHours(sEndTimeArr[0]);
			dEndDateTime.setMinutes(sEndTimeArr[1]);
		}
		
		if(dEndDateTime.getTime() - dStrDateTime.getTime() > 24 * 60 * 60 * 1000){			
			showERRMessage(parent.arrError['dateRangeExceeds']);			
			return;	
		}		
	}
	
	top.blnProcessCompleted = true;
	setSearchCriteria();
	setSearchRecNo(1);
	top[2].MessageReset();
	hdnMode = "LOAD";
	document.forms[0].hdnMode.value = "LOAD";
	document.forms[0].submit();
}

/*
 * Ajax submit
 * 
 */
function remoteSubmit() {
	var url = "";
	var params = "";

	var fromDate = getText("txtStartDateSearch");
	var toDate = getText("txtStopDateSearch");
	var flightNo = getText("txtFlightNoSearch");
	var flightNoStart = getText("txtFlightNoStartSearch");
	var operaTyp = getValue("selOperationTypeSearch");
	var stationFrm = getValue("selFromStn6");
	var stationTo = getValue("selToStn6");
	var status = getValue("selStatusSearch");
	var starTime = getText("txtStartTime");
	var endTime = getText("txtEndTime");
	var chkSunday = getChecked("chkSunday");
	var chkMonday = getChecked("chkMonday");
	var chkTuesday = getChecked("chkTuesday");
	var chkWednesday = getChecked("chkWednesday");
	var chkThursday = getChecked("chkThursday");
	var chkFriday = getChecked("chkFriday");
	var chkSaturday = getChecked("chkSaturday");
	var sortOpt = getText("radSortOption");

	switch (hdnMode) {
	case "SEARCH":
		url = "showFlight.action";
		params = "hdnLive=" + dataSource + "&hdnMode=" + hdnMode + "&txtStartDateSearch=" + fromDate
				+ "&txtStopDateSearch=" + toDate + "&txtFlightNoSearch="
				+ flightNo + "&txtFlightNoStartSearch=" + flightNoStart
				+ "&selOperationTypeSearch=" + operaTyp + "&selFromStn6="
				+ stationFrm + "&selToStn6=" + stationTo + "&selStatusSearch="
				+ status + "&txtStartTime=" + starTime + "&txtEndTime="
				+ endTime + "&chkSunday=" + chkSunday + "&chkMonday="
				+ chkMonday + "&chkTuesday=" + chkTuesday + "&chkWednesday="
				+ chkWednesday + "&chkThursday=" + chkThursday + "&chkFriday="
				+ chkFriday + "&chkSaturday=" + chkSaturday + "&hdnRecNo=" + hdnRecNo;
		break;
	case "EMAIL":
		url = "showFlightDetail.action";
		flightNo = fliData[strGridRow][1];
		fltDate = fliData[strGridRow][2];
		fltRoute = fliData[strGridRow][5];
		var fltID = fliData[strGridRow][16];
		params = "hdnLive=" + dataSource + "&hdnMode=EMAIL&hdnIsPaxDetails="+ isPaxDetail 
				+ "&hdnEmailID=" + getValue("txtEmailId")
				+ "&" + generateParamString();
		break;

	}
	parent.ShowProgress();
	makePOST(url, params);

}
/*
 * Ajax retrun call
 */
function processStateChange() {
	if (httpReq.readyState == 4) {
		parent.HideProgress();
		if (httpReq.status == 200) {
			strSYSError = "";
			eval(httpReq.responseText);
			if (strSYSError == "") {
				ClearProgressbar();
				switch (hdnMode) {
				case "SEARCH":
					arrayClone(flightData, fliData);
					objDG.arrGridData = flightData;
					objDG.paging = true;
					objDG.seqStartNo = recstNo;
					objDG.pgnumRecTotal = totalRecNo;
					objDG.refresh();
					break;
				case "EMAIL":
					if (emailStatus == "success") {
						alert("Email sent successfully");
						setField("txtEmailId", "");
					} else if (emailStatus == "error") {
						showERRMessage(parent.arrError['emailfailure']);
					}
					break;
				}
			} else {
				showERRMessage(strSYSError);
			}
		}
	}

}