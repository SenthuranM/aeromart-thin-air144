/**
 * @author suneth
 *
 */

var jsonRequestObj = [];
var statusCode;
var newStatusCode;

$(document).ready(function(){
	
	disableFields();
	hideButtons();
	$("#btnSave").hide();
	
	$(" #txtFromDate, #txtToDate").datepicker({
		minDate : '-2Y',
		maxDate : new Date(),
		showOn : "button",
		buttonImage : "../images/Calendar_no_cache.gif",
		buttonImageOnly : true,
		dateFormat : 'dd-M-yy'
	}).keyup(function(e) {
		if (e.keyCode == 8 || e.keyCode == 46) {
			$.datepicker._clearDate(this);
		}
	});
	
	$("#remarkPopup").dialog({ 
		autoOpen: false,
		modal:true,
		height: 'auto',
		width: 'auto',
		title:'Add Remark',
		close:function() {$("#remarkPopup").dialog('close');}
	});
	
	$("#requestStatusAction option[value='OPEN']").remove();
	$("#requestStatusAction option[value='ROPN']").remove();
	$("#requestStatusAction option[value='EDIT']").remove();
	$("#requestStatus option[value='EDIT']").remove();
	
	$("#requestStatus option[value='ROPN']").remove();
	
	$("#buttonSearch").click(function(){searchRequest();});
	$("#btnCreateRequest").click(function(){createRequest();});
	$("#btnEdit").click(function(){editRequest();});
	$("#btnDelete").click(function(){deleteRequest();});
	$("#btnReopen").click(function(){$("#remarkPopup").dialog('open');});
	$("#btnSubmit").click(function(){remarkSubmitClick();});
	$("#btnViewHistory").click(function(){viewHistoryOfRequest();});
	$("#btnSave").click(function(){saveRequest();});
	$("#btnClose").click(function(){top.LoadHome();});

	$("#lastTenRequests").change(function(){lastTenRequestsChange();});
	
	$("#btnSetInProgress").click(function(){setInProgress();});
	$("#btnCloseRequest").click(function(){setClosed();});
	$("#btnReject").click(function(){setRejected();});
	$("#btnToOpen").click(function(){setToOpen();});
	
	if(top.arrPrivi['xbe.tool.requestmanagement.action']){
		
		createGridForAction();
		
	}
	
	if(top.arrPrivi['xbe.tool.requestmanagement.submit']){
		
		createGrid();
		
	}		
	
});

createGrid = function(){
	
	var dateFormatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		var time;
		if (cellVal!=null){
			treturn = $.datepicker.formatDate('dd-M-yy', new Date(cellVal.split('T')[0]));
			time = cellVal.split('T')[1];
			if(time.split(':')[0] != "00" && time.split(':')[1] != "00" && time.split(':')[2] != "00"){
				treturn = treturn + " " + time;
			}
		}
		return treturn;
	}	
	
	var linkFormatter = function(cellVal, options, rowObject){
		if(cellVal != null){
			return "<label id='pnr_"+rowObject.id+"' style='text-decoration: underline; cursor: pointer;' title='"+cellVal+"'>"+cellVal+"</label>";
		}
		return "";
	}
	
	$("#requestsGridContainer").empty().append('<table id="requestsGrid"></table><div id="requestsPages"></div>')
	var data = {};
	
	if($('#lastTenRequests').is(":checked")){
		data['queueId'] = "";
		data['applicablePageID'] = "";
		data['status'] = "";
		data['pnr'] = "";
		data['fromDate'] = "";
		data['toDate'] = "";
		data['lastTenRequests'] = true;
	}else{
		data['queueId'] = $("#searchQueueType").val();
		data['applicablePageID'] = $("#applicablePage").val();
		data['status'] = $("#requestStatus").val();
		data['pnr'] = $("#pnrSearch").val();
		data['fromDate'] = $("#txtFromDate").val();
		data['toDate'] = $("#txtToDate").val();
		data['lastTenRequests'] = false;
	}
		
	var requestGrid = $("#requestsGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'searchRequests.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :UI_commonSystem.showProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(requestGrid[0], "gridData", mydata.requestsList);
			            		  requestGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  UI_commonSystem.hideProgress();
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames: ['Request ID', 'Queue', 'Created By', 'Created On', 'Status', 'Description', 'Modified By', 'Modified Date', 'PNR', 'Assigned To', 'Assigned On','QueueID','statusCode'],
		    colModel: [
		               
			    {name: 'requestId',index: 'requestId',jsonmap:'queueRequestDTO.requestId', width: 75 , resizable: false, align: "center" },
			    {name: 'queueType', index: 'queueType',jsonmap:'queueRequestDTO.queueName', width: 110, resizable: false, align: "left"},
			    {name: 'createdBy',index: 'createdBy',jsonmap:'queueRequestDTO.createdBy', width: 100, hidden: true, align: "center"},
			    {name: 'createdDate', index: 'createdDate',jsonmap:'queueRequestDTO.createdDate' ,width: 140, resizable: false, align: "left", formatter:dateFormatter , sortable:true},
			    {name: 'status',index: 'status', jsonmap:'queueRequestDTO.status', width: 100, resizable: false, align: "center"},
			    {name: 'description',index: 'description', jsonmap:'queueRequestDTO.description', width: 100, resizable: false, align: "left"},
			    {name: 'modifiedBy',index: 'modifiedBy', jsonmap:'queueRequestDTO.modifiedBy', width: 100 , resizable: false, align: "center"},
				{name: 'modifiedDate',index: 'modifiedDate', jsonmap:'queueRequestDTO.modifiedDate', width: 120 ,resizable: false, align: "center", formatter:dateFormatter},
				{name: 'pnr',index: 'pnr', jsonmap:'queueRequestDTO.pnr' , width: 80 , resizable: false, align: "center", formatter:linkFormatter},
			    {name: 'assignedTo',index: 'assignedTo', jsonmap:'queueRequestDTO.assignedTo', width: 80 , resizable: false, align: "center"},
			    {name: 'assignedOn',index: 'assignedOn', jsonmap:'queueRequestDTO.assignedOn', width: 80 , resizable: false, align: "center", formatter:dateFormatter},
			    {name: 'queueId',index: 'queueId', jsonmap:'queueRequestDTO.queueId', hidden: true},
			    {name: 'statusCode',index: 'statusCode', jsonmap:'queueRequestDTO.statusCode', hidden: true},
			    
		    ],
		    caption: "Requests Submitted",
		    rowNum : 20,
			viewRecords : true,
			width : 960,
			pager: jQuery('#requestsPages'),
			jsonReader: { 
				root: "requestsList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			onCellSelect: function(row, col, content, event) {
		        var cm = $("#requestsGrid").getGridParam('colModel');
		        if(cm[col].name == 'pnr'){
		        	var rowData = $("#requestsGrid").getRowData(row);
		        	if(rowData.pnr != ""){
		        		$('#pnrNO').val($("#pnr_" + (row-1).toString()).attr('title'));
		        		$("#frmRes").submit();
		        	}
		        }
		    },
		    
	}).navGrid("#requestsPages",{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true , viewsortcols:[true,'vertical',true]});
	
	$("#requestsGrid").click(function(){ requestsGridOnClick(); });
	clearFields();
	
}	

createGridForAction = function(){
	
	var dateFormatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		var time;
		if (cellVal!=null){
			treturn = $.datepicker.formatDate('dd-M-yy', new Date(cellVal.split('T')[0]));
			time = cellVal.split('T')[1];
			if(time.split(':')[0] != "00" && time.split(':')[1] != "00" && time.split(':')[2] != "00"){
				treturn = treturn + " " + time;
			}
		}
		return treturn;
	}	
	
	$("#requestsGridContainer").empty().append('<table id="requestsGrid"></table><div id="requestsPages"></div>')
	var data = {};
	data['queueId'] = $("#searchQueueType").val();
	data['status'] = $("#requestStatus").val();
	
	var requestGrid = $("#requestsGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'searchRequests!searchRequestsForActioning.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :UI_commonSystem.showProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(requestGrid[0], "gridData", mydata.requestsList);
			            		  requestGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  UI_commonSystem.hideProgress();
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames: ['Created By', 'Description', 'Queue', 'Status', 'createdDate', 'PNR', 'statusCode', 'requestId', 'queueId'],
		    colModel: [
		        
		        {name: 'createdBy',index: 'createdBy',jsonmap:'queueRequestDTO.createdBy', width: 100, align: "center"},
		        {name: 'description',index: 'description', jsonmap:'queueRequestDTO.description', width: 450, resizable: false, align: "left"},
		        {name: 'queueType', index: 'queueType',jsonmap:'queueRequestDTO.queueName', width: 150, resizable: false, align: "left"},
			    {name: 'status',index: 'status', jsonmap:'queueRequestDTO.status', width: 100, resizable: false, align: "center"},
			    {name: 'createdDate',index: 'createdDate', jsonmap:'queueRequestDTO.createdDate', sorttype: "date", formatter:dateFormatter},
			    {name: 'pnr',index: 'pnr', jsonmap:'queueRequestDTO.pnr' , width: 80 , resizable: false, align: "center"},
			    {name: 'statusCode',index: 'statusCode', jsonmap:'queueRequestDTO.statusCode', hidden: true},
			    {name: 'requestId',index: 'requestId', jsonmap:'queueRequestDTO.requestId', hidden: true},
			    {name: 'queueId', index: 'queueId',jsonmap:'queueRequestDTO.queueId', hidden: true}
			    
		    ],
		    caption: "Requests Submitted",
		    rowNum : 20,
			viewRecords : true,
			width : 960,
			pager: jQuery('#requestsPages'),
			jsonReader: { 
				root: "requestsList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid("#requestsPages",{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true});
	
	$("#requestsGrid").click(function(){ requestsGridOnClickForAction(); });
	clearFields();
	
}

requestsGridOnClick = function(){
	
	$("#btnViewHistory").show();
	$("#btnSave").hide();
	
	disableFields();
	
	var rowId =$("#requestsGrid").getGridParam('selrow');  
    var rowData = $("#requestsGrid").getRowData(rowId);
    
    var queueId = rowData.queueId;
    var description = rowData.description;
    var pnr = $("#pnr_" + (rowId-1).toString()).attr('title');
    var statusCode = rowData.statusCode;
    
    if(statusCode == 'OPEN'){
    	
    	$("#btnEdit").show();
    	$("#btnDelete").show();
    	
    	$("#btnSave").val("Save");
    	
    }else{
    	
    	$("#btnEdit").hide();
    	$("#btnDelete").hide();
    	
    }
	
    if(statusCode == 'CLSE'){
    	
    	$("#btnReopen").show();
    	
    }else{
    	
    	$("#btnReopen").hide();
    	
    }
    
	$("#pnr").val(pnr);
	$("#description").val(description);
    $("#addEditQueueType").val(queueId);
    
}

requestsGridOnClickForAction = function(){
	
	$("#btnViewHistory").show();
	
	disableFields();
	
	var rowId =$("#requestsGrid").getGridParam('selrow');  
    var rowData = $("#requestsGrid").getRowData(rowId);

    var queueId = rowData.queueId;
    var description = rowData.description;
    statusCode = rowData.statusCode;
    
    $("#addEditQueueType").val(queueId);
    $("#description").val(description);
    $("#pnr").val(rowData.pnr);
    $("#createdDateLbl").text(rowData.createdDate);
    
    $("#btnSetInProgress").hide();
	$("#btnCloseRequest").hide();
	$("#btnReject").hide();
	$("#btnToOpen").hide();
    
    if(statusCode == 'OPEN'){
    	$("#btnSetInProgress").show();
    	$("#btnReject").show();
    }else if(statusCode == 'INP'){
    	$("#btnToOpen").show();
    	$("#btnCloseRequest").show();
    	$("#btnReject").show();
    }
    
}

searchRequest = function(){

	if(top.arrPrivi['xbe.tool.requestmanagement.action']){
		
		createGridForAction();
		
	}
	
	if(top.arrPrivi['xbe.tool.requestmanagement.submit']){
		
		createGrid();
		
	}	
	
}

createRequest = function(){
	
	enableFields();
	$("#pnr").val("");
	$("#description").val("");
	$("#btnSave").val("Submit");
	$("#btnSave").show();
	hideButtons();
	
}

saveRequest = function(){
	
	var data = {};
	
	if($("#btnSave").val() == 'Submit'){
		
		if(!validate()){ return false;}
		
		data['pnr'] = $("#pnr").val();
		data['queueId'] = $("#addEditQueueType").val();
		data['description'] = $("#description").val();

		$.ajax({
			
	        url : 'requestManagement.action',
	        data: data,
	        dataType:"json",
	        beforeSend :UI_commonSystem.showProgress(),
	        type : "POST",		           
			   success: function(response){

				   clearFields();
				   createGrid();
				   UI_commonSystem.hideProgress();
				   showConfirmation("Request Submitted Successfully.");
				   
			   },
			   error: function(response){
				   
				   UI_commonSystem.hideProgress();
				   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
				   
			   }
		
	     });
		
		return true;
		
	}else if($("#btnSave").val() == 'Save'){
		
		if(!validate()){ return false;}
		
		var rowId =$("#requestsGrid").getGridParam('selrow');  
	    var rowData = $("#requestsGrid").getRowData(rowId);
	    var requestId = rowData.requestId;
		
		data['requestId'] = requestId;
		data['pnr'] = $("#pnr").val();
		data['queueId'] = $("#addEditQueueType").val();
		data['description'] = $("#description").val();

		$.ajax({
			
	        url : 'requestManagement!editRequest.action',
	        data: data,
	        dataType:"json",
	        beforeSend :UI_commonSystem.showProgress(),
	        type : "POST",		           
			   success: function(response){

				   clearFields();
				   $("#requestsGrid").trigger('reloadGrid');
				   UI_commonSystem.hideProgress();
				   showConfirmation("Request Updated Successfully.");
				   setTimeout(function(){
						$("#requestsGrid").setSelection(rowId , true);
				   }, 500);
				   
			   },
			   error: function(response){
				   
				   UI_commonSystem.hideProgress();
				   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
				   
			   }
		
	     });
		
		return true;	
		
	}
	
}

deleteRequest = function(){

	var data = {};
	var rowId =$("#requestsGrid").getGridParam('selrow');  
    var rowData = $("#requestsGrid").getRowData(rowId);
	
	data['requestId'] = rowData.requestId;
	
	$.ajax({
		
        url : 'requestManagement!deleteRequest.action',
        data: data,
        dataType:"json",
        beforeSend :UI_commonSystem.showProgress(),
        type : "POST",		           
		   success: function(response){

			   clearFields();
			   $("#requestsGrid").trigger('reloadGrid');
			   UI_commonSystem.hideProgress();
			   showConfirmation("Request Deleted Successfully.");
			   
		   },
		   error: function(response){
			   
			   UI_commonSystem.hideProgress();
			   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
			   
		   }
	
     });
	
}

reopenRequest = function(){
	
	var data = {};
	var rowId =$("#requestsGrid").getGridParam('selrow');  
    var rowData = $("#requestsGrid").getRowData(rowId);
	
	data['requestId'] = rowData.requestId;
	data['remark'] = $("#remark").val();
	
	$.ajax({
		
        url : 'requestManagement!reopenRequest.action',
        data: data,
        dataType:"json",
        beforeSend :UI_commonSystem.showProgress(),
        type : "POST",		           
		   success: function(response){

			   clearFields();
			   $("#requestsGrid").trigger('reloadGrid');
			   UI_commonSystem.hideProgress();
			   showConfirmation("Request reopened Successfully.");
			   $("#remarkPopup").dialog('close');
			   setTimeout(function(){
					$("#requestsGrid").setSelection(rowId , true);
			   }, 500);
			   
		   },
		   error: function(response){
			   
			   UI_commonSystem.hideProgress();
			   $("#remarkPopup").dialog('close');
			   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
			   
		   }
	
     });
	
}

viewHistoryOfRequest = function(){
	
	var rowId =$("#requestsGrid").getGridParam('selrow');  
    var rowData = $("#requestsGrid").getRowData(rowId);
    
    var strUrl = "showRequestHistory.action?requestId="+rowData.requestId;
		
	top.objCW= window.open(strUrl,"",$("#popWindow").getPopWindowProp(500, 910, true));
	if (top.objCW) {top.objCW.focus()}
	
}

editRequest = function(){
	
	enableFields();
	$("#btnSave").val("Save");
	$("#btnSave").show();
    
}


saveActionedRequest = function(){
	
	var data = {};
	
	var rowId =$("#requestsGrid").getGridParam('selrow');  
    var rowData = $("#requestsGrid").getRowData(rowId);
    var requestId = rowData.requestId;
	
	data['requestId'] = requestId;
	data['status'] = newStatusCode;
	data['remark'] = $("#remark").val();

	$.ajax({
		
        url : 'requestManagement!actionRequest.action',
        data: data,
        dataType:"json",
        beforeSend :UI_commonSystem.showProgress(),
        type : "POST",		           
		   success: function(response){

			   clearFields();
			   $("#requestsGrid").trigger('reloadGrid');
			   UI_commonSystem.hideProgress();
			   showConfirmation("Request Updated Successfully.");
			   $("#remarkPopup").dialog('close');
			   setTimeout(function(){
					$("#requestsGrid").setSelection(rowId , true);
			   }, 500);
			   requestsGridOnClickForAction();
		   },
		   error: function(response){
			   
			   UI_commonSystem.hideProgress();
			   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
			   $("#remarkPopup").dialog('close');
			   requestsGridOnClickForAction();
		   }
	
     });
	
	return true;
	
}

searchRequestForAction = function(){
	
	createGridForAction();
	
}

enableFields = function(){
	
	$("#addEditQueueType").prop('disabled', false);
	$("#pnr").prop('disabled', false);
	$("#description").prop('disabled', false);
	$("#btnSave").prop('disabled', false);
	
	$("#addEditQueueType").removeClass('aa-input');
	$("#pnr").removeClass('aa-input');
	$("#description").removeClass('aa-input');
	$("#requestStatusAction").removeClass('aa-input');
	
}

disableFields = function(){
	
	$("#addEditQueueType").prop('disabled', true);
	$("#pnr").prop('disabled', true);
	$("#description").prop('disabled', true);
	$("#btnSave").prop('disabled', true);
	$("#requestStatusAction").prop('disabled', true);
	
	$("#addEditQueueType").addClass('aa-input');
	$("#pnr").addClass('aa-input');
	$("#description").addClass('aa-input');
	
}

clearFields = function(){
	
	$("#pnr").val("");
	$("#description").val("");
	$("#addEditQueueType").val("");
	$("#btnSave").val("Submit");
	$("#requestStatusAction").val("");
	
}

hideButtons = function(){
	
	$("#btnEdit").hide();
	$("#btnDelete").hide();
	$("#btnReopen").hide();
	$("#btnViewHistory").hide();
	$("#btnViewHistory").hide();
	$("#btnSetInProgress").hide();
	$("#btnCloseRequest").hide();
	$("#btnReject").hide();
	$("#btnToOpen").hide();
}

validate = function(){
	
	jsonRequestObj = [{
		id : "#addEditQueueType",
		desc : "Queue Type",
		required : true 
	}, {
		id : "#description",
		desc : "Description",
		required : true
	}];
	
	if (!UI_commonSystem.validateMandatory(jsonRequestObj)) {
		
		return false;
		
	}else{
		
		return true;
		
	}
	
}

disableSearchFields = function(){
	$("#pnrSearch").prop('disabled', true);
	$("#txtFromDate").prop('disabled', true);
	$("#txtToDate").prop('disabled', true);
	$("#applicablePage").prop('disabled', true);
	$("#requestStatus").prop('disabled', true);
	$("#searchQueueType").prop('disabled', true);
}

enableSearchFields = function(){
	$("#pnrSearch").prop('disabled', false);
	$("#txtFromDate").prop('disabled', false);
	$("#txtToDate").prop('disabled', false);
	$("#applicablePage").prop('disabled', false);
	$("#requestStatus").prop('disabled', false);
	$("#searchQueueType").prop('disabled', false);
}

lastTenRequestsChange = function(){
	if($('#lastTenRequests').is(":checked")){
		disableSearchFields();
	}else{
		enableSearchFields();
	}
}

setInProgress = function(){
	newStatusCode = "INP";
	$("#remarkPopup").dialog('open');
}

setClosed = function(){
	newStatusCode = "CLSE";
	$("#remarkPopup").dialog('open');
}

setRejected = function(){
	newStatusCode = "RJCT";
	$("#remarkPopup").dialog('open');
}

setToOpen = function(){
	newStatusCode = "OPEN";
	$("#remarkPopup").dialog('open');
}

remarkSubmitClick = function(){
	
	if (!UI_commonSystem.validateMandatory([{
		id : "#remark",
		desc : "Remark For Reopening The Request",
		required : true 
	}])) {
		
		return false;
		
	}
	
	if(top.arrPrivi['xbe.tool.requestmanagement.action']){
		
		saveActionedRequest();
		
	}
	
	if(top.arrPrivi['xbe.tool.requestmanagement.submit']){
		
		reopenRequest();
		
	}	
	
	$("#remark").val("");
	newStatusCode = "";
}
