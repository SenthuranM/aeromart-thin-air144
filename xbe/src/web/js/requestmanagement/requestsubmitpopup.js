/**
 * @author suneth
 *
 */

$(document).ready(function(){

	$("#btnClosePopup").click(function(){$("#requestSubmitPopup").dialog('close');});	
	$("#btnSavePopup").click(function(){submitRequest();});	

	var pageID;
	
	if(top.strClickedFID == 'SC_RESV_CC_005'){
		pageID = 'BIN';
	}else if(top.strClickedFID == 'SC_RESV_CC_001'){
		pageID = 'ASP';
	}
	
	data = { };
	data['pageID'] = pageID;

	$.ajax({
		
        url : 'requestManagement!getQueueNames.action',
        data: data,
        dataType:"json",
        type : "POST",		           
		   success: function(response){
				  $("#queueType").append(response.queuesForPage); 	   			   
		   },
		   error: function(response){
			   
			   UI_commonSystem.hideProgress();
			   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
			   
		   }
	
     });
	
});

submitRequest = function(){
	
	data = {};
	
	var queueId = $("#queueType").val();
	var description = $("#description").val();
	var pnr = null;
	
	if(!(DATA_ResPro.reservationInfo === undefined)){
		
		pnr = DATA_ResPro.reservationInfo.PNR;
		
	}
	
	if(!(typeof jsonBkgInfo == 'undefined' | jsonBkgInfo == null)){
		if(!(jsonBkgInfo.PNR === undefined)){
			pnr = jsonBkgInfo.PNR;
		}
	}
	
	if(!validate()){ return false;}
	
	data['pnr'] = pnr;
	data['queueId'] = queueId;
	data['description'] = description;

	$.ajax({	
		
        url : 'requestManagement.action',
        data: data,
        dataType:"json",
        beforeSend :UI_commonSystem.showProgress(),
        type : "POST",		           
		   success: function(response){
			   
			   $("#description").val("");
			   UI_commonSystem.hideProgress();
			   showConfirmation("Request Submitted Successfully.");
			   
		   },
		   error: function(response){
			   
			   UI_commonSystem.hideProgress();
			   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
			   
		   }
	
     });
	$("#requestSubmitPopup").dialog('close');
	//return true;
	
}

validate = function(){
	
	jsonRequestObj = [{
		id : "#queueType",
		desc : "Queue Type",
		required : true 
	}, {
		id : "#description",
		desc : "Description",
		required : true
	}];
	
	if (!UI_commonSystem.validateMandatory(jsonRequestObj)) {
		
		return false;
		
	}else{
		
		return true;
		
	}
	
}
