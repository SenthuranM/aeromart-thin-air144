var screenId = "SC_SHDS_043";
var valueSeperator = "~";

var recNumPerPage = 20;
var recstNo = top[0].intLastRec;


var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Message Type" ;
objCol1.headerText = "Message Type";
objCol1.itemAlign = "left";
objCol1.sort="true";
objCol1.width = "9%";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.arrayIndex = 2;
objCol2.toolTip = "GDS Name" ;
objCol2.headerText = "GDS";
objCol2.itemAlign = "left";
objCol2.sort="true";
objCol2.width = "9%";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Received Date" ;
objCol3.headerText = "Received Date";
objCol3.itemAlign = "left";
objCol3.width = "9%";
	
var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Communication Reference" ;
objCol4.headerText = "Comm.. Reference";
objCol4.itemAlign = "left";
objCol4.sort="true";
objCol4.width = "9%";
	
var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Process Status" ;
objCol5.headerText = "Status";
objCol5.itemAlign = "left";
objCol5.width = "9%";

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.arrayIndex = 6;
objCol6.toolTip = "Oreginator Address" ;
objCol6.headerText = "Oreginator";
objCol6.itemAlign = "left";
objCol6.width = "9%";

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.arrayIndex = 7;
objCol7.toolTip = "Responder Address" ;
objCol7.headerText = "Responder";
objCol7.itemAlign = "left";
objCol7.width = "9%";

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.arrayIndex = 13;
objCol8.toolTip = "GDS PNR" ;
objCol8.headerText = "GDS PNR";
objCol8.itemAlign = "left";
objCol8.width = "9%";

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.arrayIndex = 16;
objCol9.toolTip = "Message Broker Reference" ;
objCol9.headerText = "Message Broker";
objCol9.itemAlign = "left";
objCol9.width = "9%";

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.arrayIndex = 14;
objCol10.toolTip = "GDS Agent Code" ;
objCol10.headerText = "GDS Agent";
objCol10.itemAlign = "left";
objCol10.width = "9%";

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.arrayIndex = 15;
objCol11.toolTip = "User ID" ;
objCol11.headerText = "User ID";
objCol11.itemAlign = "left";
objCol11.width = "10%";



// ---------------- Grid	
var objDG = new DataGrid("spnReprintGenInv");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);

objDG.width = "99%";

if (browser.isIE){
	objDG.height = "100px";
}else{
	objDG.height = "140px";
}

objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrGDSData;
objDG.seqNo = true;


//objDG.seqStartNo = 1;
//objDG.paging = true;
//objDG.pgnumRecPage = 20;

var seqSize = Number(recstNo) + (Number(recNumPerPage) - 1);
if (new String(seqSize).length > 2) {
	objDG.seqNoWidth = "4%";
} else {
	objDG.seqNoWidth = "3%";	
}
objDG.backGroundColor = "#ECECEC";
objDG.seqStartNo = recstNo;
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = recNumPerPage;

objDG.pgonClick = "gridNavigations";	
objDG.rowClick = "RowClick";	
objDG.shortCutKeyFunction = "Body_onKeyDown";
ShowProgress();
objDG.displayGrid();







