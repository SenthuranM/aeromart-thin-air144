
// Author : Dhanushka


var objWindow;
var screenId = "SC_SHDS_043";
var valueSeperator = "~";
var strRowData;
var strGridRow;
var intLastRec;

function dataChanged(){
	setPageEdited(true);
	objOnFocus();
}

function gridNavigations(intRecNo, intErrMsg) {
	if (intRecNo <= 0) {
		intRecNo = 1;
	}
	setSearchRecNo(intRecNo);
	setField("hdnRecNo", intRecNo);
	if (intErrMsg != "") {
		showERRMessage(intErrMsg);
	}else {
		document.forms[0].hdnUIMode.value="search";
		document.forms[0].target="_self";
		document.forms[0].action = "showGdsMessages.action";
		document.forms[0].submit();
		setPageEdited(false);
		ShowProgress();
	}
}

function setSearchRecNo(recNo) {
	top[0].intLastRec = recNo;
}

//On Page loading make sure that all the page input fields are cleared
//function winOnLoad(strMsg,strMsgType) {
function winOnLoad(strMsg, strMsgType, strWarnMsg) {
		objOnFocus();
	//Keep the search variables remain same
	if(isSearchMode){
		document.forms[0].hdnUIMode.value="search";	
		if(strMsg != null && strMsgType != null && strMsg != ''){
			showCommonError(strMsgType,strMsg);
		}
		
		var gDSType = arrSearchData["selGDSType"];
		if(gDSType!=null && gDSType!=''){
			setField("selGDSType",gDSType);
		}
		
		var mSGType = arrSearchData["selMSGType"];
		if(mSGType!=null && mSGType!=''){
			setField("selMSGType",mSGType);
		}
		
		var processStatus = arrSearchData["selProcessStatus"];
		if(processStatus!=null && processStatus!=''){
			setField("selProcessStatus",processStatus);
		}
		
		var commReference = arrSearchData["txtCommReference"];
		if(commReference!=null && commReference!=''){
			setField("txtCommReference",commReference);
		}
		
		var fromDate = arrSearchData["txtFromDate"];
		if(fromDate!=null && fromDate!=''){
			setField("txtFromDate",fromDate);
		}
		
		var toDate = arrSearchData["txtToDate"];
		if(toDate!=null && toDate!=''){
			setField("txtToDate",toDate);
		}
		
		var reqXml = arrSearchData["txtReqXml"];
		if(reqXml!=null && reqXml!=''){
			setField("txtReqXml",reqXml);
		}
		
		var resXml = arrSearchData["txtResXml"];
		if(resXml!=null && resXml!=''){
			setField("txtResXml",resXml);
		}
		
		var msgBrokerRef = arrSearchData["txtMsgBrokerRef"];
		if(msgBrokerRef!=null && msgBrokerRef!=''){
			setField("txtMsgBrokerRef",msgBrokerRef);
		}
		
		var gdsPnr = arrSearchData["txtGDSPnr"];
		if(gdsPnr!=null && gdsPnr!=''){
			setField("txtGDSPnr",gdsPnr);
		}
		
		
	}

	Disable("btnReqXML", true);
	Disable("btnResXML", true);

	setPageEdited(false);	
}

//on Grid Row click
function RowClick(strRowNo){
	objOnFocus();
	if(top.loadCheck()){
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow",strRowNo);
		
		setPageEdited(false);
		
		setField("frmMessageType",arrGDSData[strRowNo][1]);
		setField("frmReceivedDate",arrGDSData[strRowNo][3]);
		setField("frmGDSName",arrGDSData[strRowNo][2]);
		setField("frmProcessStatus",arrGDSData[strRowNo][5]);
		setField("frmComRef",arrGDSData[strRowNo][4]);
		setField("frmOreginator",arrGDSData[strRowNo][6]);
		setField("frmResponder",arrGDSData[strRowNo][7]);
		setField("frmAAObjectID",arrGDSData[strRowNo][8]);
		setField("frmStatusHistory",arrGDSData[strRowNo][9]);
		setField("frmProcessDes",arrGDSData[strRowNo][10]);
		setField("frmGdsPnr",arrGDSData[strRowNo][13]);
		setField("frmMsgBrokerRef",arrGDSData[strRowNo][16]);
		Disable("btnReqXML", false);
		Disable("btnResXML", false);
		
	}
}

function showRequestXML(){
	if ((objWindow) && (!objWindow.closed))	{
		objWindow.close();
	}

	var intWidth = 0;
	var intHeight = 0 ; 
	var strProp = '';
	var strMsgType = "GDS Request XML";
	
	document.forms[0].hdnUIMode.value="VIEW_REQ_XML";

	setField("hdnGdsMsgID",arrGDSData[strGridRow][11]);	
	setPageEdited(false);

	if (browser.isIE){
		intHeight = 320;
	}else{
		intHeight = 295;
	}
	
	intWidth = 720;
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=Yes,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + ((window.screen.height - intHeight) / 2) + ',left=' + (window.screen.width - intWidth) / 2;
	objWindow = window.open("about:blank","CWindow",strProp);
	objForm  = document.getElementById("frm111");
	objForm.target = "CWindow";
	objForm.action = "showGdsDetailMessage.action?strMsgType="+strMsgType;
	objForm.submit();
}

function showResponseXML(){
	if ((objWindow) && (!objWindow.closed))	{
		objWindow.close();
	}

	var intWidth = 0;
	var intHeight = 0 ; 
	var strProp = '';
	var strMsgType = "GDS Response XML";
	
	document.forms[0].hdnUIMode.value="VIEW_RES_XML";

	setField("hdnGdsMsgID",arrGDSData[strGridRow][11]);	
	setPageEdited(false);

	if (browser.isIE){
		intHeight = 320;
	}else{
		intHeight = 295;
	}

	intWidth = 720;
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=Yes,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + ((window.screen.height - intHeight) / 2) + ',left=' + (window.screen.width - intWidth) / 2;
	objWindow = window.open("about:blank","CWindow",strProp);
	objForm  = document.getElementById("frm111");
	objForm.target = "CWindow";
	objForm.action = "showGdsDetailMessage.action?strMsgType="+strMsgType;
	objForm.submit();
}



function dateValidation(cntfield){
	if (!dateChk(cntfield))	{
		showERRMessage(arrError['invalidDate']);
		getFieldByID(cntfield).focus();
		return false;
	}
	return true;
}

function searchData() {
	objOnFocus();
	
	if(!dateValidation('txtToDate') || !dateValidation('txtFromDate')){
		return;
	}

	top[0].initializeVar();
	document.forms[0].hdnUIMode.value="search";
	document.forms[0].target="_self";
	document.forms[0].action = "showGdsMessages.action";
	document.forms[0].submit();
	setPageEdited(false);
	ShowProgress();
}	

function objOnFocus(){

	top[2].HidePageMessage();
}

function pageOnChange(){
	top.pageEdited = true;
}
function setPageEdited(isEdited){
	top.pageEdited=isEdited;	
}


function beforeUnload(){
	if ((objWindow) && (!objWindow.closed))	{
		objWindow.close();
	}
}

 //Calender
	var objCal1 = new Calendar("spnCalendarDG1");
	
	objCal1.onClick = "setDate";
	objCal1.buildCalendar();
	
	function setDate(strDate, strID){
		switch (strID){
			case "0" : setField("txtFromDate",strDate);break ;
			case "1" : setField("txtToDate",strDate);break ;
		}
	}
	
	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 130 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}

	
	