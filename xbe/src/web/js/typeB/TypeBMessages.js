jQuery(document).ready(function(){
	$("#divLegacyRootWrapper").fadeIn("slow");
	$("#tblTypeBMessages").jqGrid({
		datatype: "local",		
		height: 500,
		width: 842,
		colNames:[ 'Message Direction', 'Date & Time', 'Row Message', 'Process Status'],
		colModel:[
			{name:'messageDirection',  index:'messageDirection', width:150, align:"center"},
			{name:'dateTime',  index:'dateTime', width:100, align:"center",
				formatter:function(cellValue){return cellValue.replace("T" , " ");}},
			{name:'rowMessage', index:'rowMessage', width:442, align:"left",
  				edittype:"textarea", 
  				formatter:function(v){ return $.jgrid.htmlEncode(v);},
  			    editoptions:{"class":"","rows":"7", "wrap":"off","style":"width:442px;", "readonly":"readonly"},
     			editrules:{"edithidden":true},"editable":true,"width":442, classes:"jqgrid-readonlycolumn"
			},
			{name:'processStatus', index:'processStatus', width:150, align:"center"}
		],
		imgpath: UI_commonSystem.strImagePath,
		multiselect: false,
		viewrecords: true
	});
	$('#btnSearch').click(function(){
		if($("#recordLocator").val() == ""){
			showCommonError("Error", "Please insert Record Locator");
			return false;
		} else {
			if($("#recordLocator").val().length != 6){
				showCommonError("Error", "Please insert valid Record Locator. Record Locator length should be 6");
				return false;
			} else {
				searchTypeBMessages();				
			}
		}
	});
});

function searchTypeBMessages() {
	UI_commonSystem.showProgress();
	var data = {};	
	data['recordLocator'] =  $("#recordLocator").val(); 
	var frmUrl = "showTypeBMessages.action";
	$("#frmTypeBMessages").ajaxSubmit({dataType: 'json', processData:false, data:data, url:frmUrl, 
		success: loadTypeBMessagesSuccess, error:UI_commonSystem.setErrorStatus});
	UI_commonSystem.showProgress();
	return false;
}

function loadTypeBMessagesSuccess(response) {
	UI_commonSystem.fillGridData({id:"#tblTypeBMessages", data:response.typeBMessages});
	if(response.typeBMessages.length == 0){
		showCommonError("Error", "No matching records found");
		UI_commonSystem.hideProgress();	
		return false;
	}
	$("#tblTypeBMessages").css("width", "842px");
	$(".ui-jqgrid-htable").css("width", "842px");
	UI_commonSystem.hideProgress();	
}