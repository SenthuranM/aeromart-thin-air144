var selectedMainRequest;
var rollForwardedRequests;

function convertDate(inputFormat) {
	var d = inputFormat.split("-");
	var d1 = new Date(d[0], d[1] - 1, d[2]);

	return [ d1.getDate(), d1.getMonth() + 1, d1.getFullYear() ].join('/');
}

function selectAll(isOut) {
	if (getFieldByID("chkOutAll").checked == true) {
		$('input:checkbox[name=appDay]').attr('checked', true);		
	} else {	
			$('input:checkbox[name=appDay]').attr('checked', false);	
		
	}
}

function UI_grpBookingRollForward() {

	$("#grpBookingRFWDetails").decoratePanel("Roll-Forward Group Booking");
	this.ready = function() {
		$(" #txtFrom, #txtTo").datepicker({
			minDate : new Date(),
			showOn : "button",
			buttonImage : "../images/Calendar_no_cache.gif",
			buttonImageOnly : true,
			dateFormat : 'yy-mm-dd'
		}).keyup(function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				$.datepicker._clearDate(this);
			}
		});
		
		
		$("#btnRollForward").click(executeRollForward);
		$("#btnSaveRollForward").click(saveRollForward);
	}
	executeRollForward = function() {
		if (validateFormRFD()) {
			
			rollForwardedRequests = new Array();
			
			var selRequest=selectedMainRequest;
			var routeData=selRequest.groupBookingRoutes;
			
			var fromDate=$("#txtFrom").datepicker("getDate");
			var toDate=$("#txtTo").datepicker("getDate");

			var selDays = new Array();
		
			
			
			$("input:checkbox[name=appDay]:checked").each(function() {
				selDays.push(parseInt($(this).val(),10));
			});
			
			var rollForwardingDates = new Array();
			while(fromDate < toDate){      
			   var dayOfTheWeek = fromDate.getDay();
			   if(selDays.indexOf(dayOfTheWeek) > -1){
				   rollForwardingDates.push(fromDate);
			   }
			   var newDate = fromDate.setDate(fromDate.getDate() + 1);
			   fromDate = new Date(newDate);
			}
			
			 for(var i=0;i<rollForwardingDates.length;i++){
				 var rollForwardedDate = rollForwardingDates[i];
				 
				 for(var j=0;j<routeData.length;j++){
					 if(rollForwardedRequests.length >= 15){
						 alert("The number of sub requests per main request must be 15 or less");
						 return false;
					 }
					 var route=routeData[j];
					 
					 var rfMoment=moment(rollForwardedDate);
					 var outbound = moment(route.departureDate);
						
					 var dateDiff = null;
					 
					 var rollForwardedObject={
						ond:route.via == "" ? route.departure+"/"+route.arrival : route.departure+"/"+route.via+"/"+route.arrival,
						departureDate:rfMoment.format("DD/MM/YYYY"),
						returningDate:null,
						flightNumbers:route.flightNumber,
						paxCount:selRequest.adultAmount+"/"+selRequest.childAmount+"/"+selRequest.infantAmount,
						adultCount:selRequest.adultAmount,
						childCount:selRequest.childAmount,
						infantCount:selRequest.infantAmount,
						oalFare:selRequest.oalFare,
						remarks:selRequest.agentComments,
						arrival:route.arrival,
						departure:route.departure,
						via:route.via
					 };
					 
					 var existingData= rollForwardedDateAlreadyExist(rollForwardedObject);
					 
					 if(existingData != null){
						 rollForwardedObject.flightNumbers = existingData.flightNumbers,
						 rollForwardedObject.paxCount = existingData.paxCount,
						 rollForwardedObject.adultCount = existingData.adultAmount,
						 rollForwardedObject.childCount = existingData.childAmount,
						 rollForwardedObject.infantCount = existingData.infantAmount,
						 rollForwardedObject.oalFare = existingData.oalFare,
						 rollForwardedObject.remarks =  existingData.remarks
					 }
					 
					 if(route.returningDate != null){
						 var inbound = moment(route.returningDate);
						 dateDiff = inbound.diff(outbound,'days');
						 rollForwardedObject.returningDate = rfMoment.add('days',dateDiff).format("DD/MM/YYYY");
					 }
					 
					 rollForwardedRequests.push(rollForwardedObject);					 
				 }
			 }
			 
			 UI_commonSystem.fillGridData({id:"#subRequestDetails", data:rollForwardedRequests});
		}
	}
	
	saveRollForward = function(){
		var subRequests = new Array();
		
		var rowIDs=$("#subRequestDetails").getDataIDs();
		
		var nextDepartureDate = null;
		
		var subRequest={
				adultAmount:0,
				childAmount:0,
				infantAmount:0,
				groupBookingRoutes:new Array(),
				oalFare:"",
				agentComments:""
		};
		
		var subRequests = new Array();
		
		for(var i=0;i<rowIDs.length;i++){
			
			var rollForwardedRequest = rollForwardedRequests[i];
			
			var nextRequest=rollForwardedRequests[i+1];
			
			if((i+1)<rollForwardedRequests.length){
				nextDepartureDate=nextRequest.departureDate;
			}else{
				nextDepartureDate="LAST_REQ";
			}
			
			var rowData = $("#subRequestDetails").getRowData(rowIDs[i]);
			var rowID = rowIDs[i];
			var flightNumbers = $("#"+rowID+"_flightNumbers").val();
			var oalFare = $("#"+rowID+"_oalFare").val();
			var remarks = $("#"+rowID+"_remarks").val();
			var paxCount = $("#"+rowID+"_paxCount").val();
			
			var paxCountArr=paxCount.split("/");
			
			if(!valudateEditedPaxCount(paxCountArr)){
				alert("Error in Pax Count at row : "+rowID+" Pax count should be in the AD/CH/IN format.")
				return false;
			}
			
			var adult = paxCountArr[0];
			var child = paxCountArr[1];
			var infant = paxCountArr[2];
			
			subRequest.adultAmount=adult;
			subRequest.childAmount=child;
			subRequest.infantAmount=infant;
			subRequest.oalFare=oalFare;
			subRequest.agentComments=remarks;
			
			var routeObj = {
				flightNumber:flightNumbers,
				arrival:rollForwardedRequest.arrival,
				departure:rollForwardedRequest.departure,
				via:rollForwardedRequest.via == "" ? null : rollForwardedRequest.via,
				departureDateStr:rollForwardedRequest.departureDate,
				returningDateStr:rollForwardedRequest.returningDate
			};
			
			subRequest.groupBookingRoutes.push(routeObj);
			
			if(nextDepartureDate != rollForwardedRequest.departureDate){
				subRequests.push(subRequest);
				subRequest={
						adultAmount:0,
						childAmount:0,
						infantAmount:0,
						groupBookingRoutes:new Array(),
						oalFare:"",
						agentComments:""
				};
				
			}
			
			nextDepartureDate=rollForwardedRequest.departureDate;
				
		}
		
		var data = {
				requestID:selectedMainRequest.requestID,
				subRequestJSON:JSON.stringify(subRequests),
		};
		
		$.ajax({ 
			url : 'manageGroupBooking!executeRollForward.action',
			beforeSend : UI_commonSystem.showProgress(),
			data : data,
			type : "POST",
			async:false,
			dataType : "json",
			complete:function(response){
				UI_commonSystem.hideProgress();
				showInfoMessage("Rollforward request saved successfully");
				resetRollForwardForm();
				$("#groupBookingTabs").tabs("option", "active", 0);
				$("#groupBookingTabs").tabs( "disable", 1);
				loadInitData();
				tLink.click();
			}
		});
	}
	
	valudateEditedPaxCount = function(paxArr){
		if(paxArr.length != 3){
			return false;
		}
		
		for(var i=0;i<paxArr.length;i++){
			var count = paxArr[i];
			if(!isUint(+count)){
				return false;
			}
		}
		
		return true;
	}
	
	function isUint(n) {
	    return +n === n && !(n % 1) && n >= 0;
	}
	
	rollForwardedDateAlreadyExist = function(rollForwardedObject){
		var rowIDs=$("#subRequestDetails").getDataIDs();
		for(var i=0;i<rowIDs.length;i++){
			var rowData=$("#subRequestDetails").getRowData(rowIDs[i]);
			if(rollForwardedObject.ond == rowData.ond && rollForwardedObject.departureDate == rowData.departureDate){
				
				rowData.flightNumbers = $("#"+rowIDs[i]+"_flightNumbers").val(),
				rowData.paxCount = $("#"+rowIDs[i]+"_paxCount").val(),
				rowData.oalFare = $("#"+rowIDs[i]+"_oalFare").val(),
				rowData.remarks =  $("#"+rowIDs[i]+"_remarks").val()
				return rowData;
			}
		}
		
		return null;
	}

	validateFormRFD = function() {

		var valied = true;
		var checked = $("input:checkbox[name=appDay]:checked").length > 0;
		if (!checked) {
			alert("Please check at least one checkbox");
			return false;
		}
		if ($("#txtFrom").val() == "") {
			alert("Please enter from date");
			return false;
		}
		if ($("#txtTo").val() == "") {
			alert("Please enter to date");
			return false;
		}

		return valied;
	}
	resetSaveFormRFD = function() {
		$("#txtFrom").val("");
		$("#txtTo").val("");
		$('input:checkbox[name=appDay]').attr('checked', false);

		window.close();
	}
	function showRWDERRMessage(strErrMessage) {
		top[1].objMsg.MessageText = strErrMessage;
		top[1].objMsg.MessageType = "Error";
		top[1].ShowPageMessage();
		return true;
	}
	function showInfoMessage(strInfoMessage) {
		top[2].objMsg.MessageText = strInfoMessage;
		top[2].objMsg.MessageType = "Confirmation" ; 
		top[2].ShowPageMessage();
		return true;
	}
	resetRollForwardForm = function(){
		UI_commonSystem.fillGridData({id:"#mainRequestDetails", data:{}});
		UI_commonSystem.fillGridData({id:"#subRequestDetails", data:{}});
		selectAll(false);
		$("#txtFrom").val("");
		$("#txtTo").val("");
		selectedMainRequest={};
		rollForwardedRequests={};
	}

}

UI_grpBookingRollForward = new UI_grpBookingRollForward();
UI_grpBookingRollForward.ready();
