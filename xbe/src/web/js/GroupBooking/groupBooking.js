var tLink;
$.fn.multiline = function(text){
    this.text(text);
    this.html(this.html().replace(/\n/g,'<br/>'));
    return this;
}
function convertDate(inputFormat) {
	var d =inputFormat.split("-");
	var d1 = new Date(d[0], d[1] - 1, d[2]);

   return [d1.getDate(), d1.getMonth()+1, d1.getFullYear()].join('/');
}
function UI_groupBooking(){
	
	var bCurrency='AED';
	var status = [["0","Pending Approval"],["1","Approve"],["2","Reject"],["3","Re-Quote"],["4","Pending after Re-Quote"],["5","Request Release Seat"],["6","Withdraw"],["7","Released Seats"],["8","Booked"]];//
	var dataMap = {
			Airport: [ "cmb" ]
	}
	
	var agentNames;
	var selectedRowData = null;
	var routeArray = [];
	this.ready = function(){
		
		$(".Button").decorateButton();
		tLink = $("<a href='javascript:void(0)'><img src='../images/tri-down1_no_cache.gif' alt=''>&nbsp;Search Group Request</a>");
		tLink.click(hideAddEditGrpB);
		$("#divGrpBSearch").decoratePanel(tLink);
		$("#grpBookingDetails").decoratePanel("Add/Edit Group Request");
		$("#grpBookingDetails").parent().hide();
		loadInitData();
		searchGrpRequest();
		$(" #txtDeparture, #txtReturn" ).datepicker({
			minDate: new Date(),
			showOn: "button",
			buttonImage: "../images/Calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
		}).keyup(function(e) {
		    if(e.keyCode == 8 || e.keyCode == 46) {
		        $.datepicker._clearDate(this);
		    }
		});

		$('#txtReturn').datepicker(this.checked ? "enable" : "disable");
        
        $("#returning").click(function(){
        	$('#txtReturn').datepicker(this.checked ? "enable" : "disable");
        });

		$("#btnSave").click(saveGroupRequest);
		
		$("#btnSearch").click(searchGrpRequest);
		$("#btnSearch").click(function(){
			$("#groupBookingTabs").tabs("option", "active", "#groupBookingSub");
		});
		$("#btnAdd").click(function(){
			showAddEditGroupBooking('add');
			isNew = true;
			$("#btnDelete,#btnRequote,#btnReleaseSeat,#btnRollFwd").hide();
			$( "#groupBookingTabs" ).tabs( "disable", 1);
			$('#btnSave').show();
			$('input:radio[name=requestType]')[0].checked = true;
			makeSaveFormToEdit();
		
		});
		$("#btnEdit").click(function(){
			showAddEditGroupBooking('edit');
			isNew = true;
			fillSubRequestData();
		});
		$(".addItem").click(function(e){
			addItemsToSelect(e.target.id);
		});
		$(".removeItem").click(function(e){
			removeItemsFromSelect(e.target.id);
		});
		
				
		$("#groupBookingTabs").tabs({
			activate: function( event, ui ) {
				 if(ui.newPanel[0].id == 'groupBookingSub'){
					 var routeData;
					 routeData = selectedMainRequest.groupBookingRoutes;
					 UI_commonSystem.fillGridData({id:"#mainRequestDetails", data:routeData});
					 rollForwardedRequests={};
				 }
			 }
		});
		
		$("#btnReleaseSeat").click(updateSeatRelease);
		$("#btnDelete").click(withdrawRequest);
		$("#oalFare").numeric({allowDecimal:true});
		$("#requestedFare").numeric({allowDecimal:true});
		$("#adultCount").numeric({allowDecimal:false});
		$("#childCount").numeric({allowDecimal:false});
		$("#infantCount").numeric({allowDecimal:false});
        $("#agentRemarks").alphaNumericWhiteSpace();
	};
	
	fillSubRequestData = function(){
		var subRequests = selectedMainRequest.groupBookingRoutes;
		if (selectedRowData!=null){
			subRequests = selectedRowData.groupRequest.subRequests;
		}
		var rowsToAdd = new Array();
		var count = 1;
		
		for(var i=0;i<subRequests.length;i++){			
			var subRequest=subRequests[i];
			var routes=subRequest.groupBookingRoutes;
			
			for(var j=0;j<routes.length;j++){
				var route=routes[j];
				
				var rowData = {
						ond:route.via == "" || route.via == null ? route.departure+"/"+route.arrival : route.departure+"/"+route.via+"/"+route.arrival,
						departureDate:moment(route.departureDate).format("DD/MM/YYYY"),
						returningDate:moment(route.returningDate).format("DD/MM/YYYY"),
						flightNumbers:route.flightNumber,
						paxCount:subRequest.adultAmount+"/"+subRequest.childAmount+"/"+subRequest.infantAmount,
						adultCount:subRequest.adultAmount,
						childCount:subRequest.childAmount,
						infantCount:subRequest.infantAmount,
						oalFare:subRequest.oalFare,
						remarks:subRequest.agentComments,
						arrival:route.arrival,
						departure:route.departure
				};
				rowsToAdd.push(rowData);
			}
		}
		$("#subRequestDetails").clearGridData();
		UI_commonSystem.fillGridData({id:"#subRequestDetails", data:rowsToAdd});
	}
	


	fillInitdata = function(response){
		$("#selFrom, #selTo,#selFrom_1,#selTo_1").fillDropDown({firstEmpty:false, dataArray:dataMap.Airport , keyIndex:0, valueIndex:0});
		$("#selVia_1").fillDropDown({firstEmpty:true, dataArray:dataMap.Airport , keyIndex:0, valueIndex:0});
		if($("#status").find('option').length == 0){
			$("#status").fillDropDown({firstEmpty:false, dataArray:status , keyIndex:0, valueIndex:1});
		}
		$("#travelAgent").html(response.agents);
		agentNames = response.agentNames;
	};
	
	loadInitData=function(){
		$.ajax({ 
			url : 'loadGroupBookingReqForInit.action',
			beforeSend :ShowPopProgress(),
			type : "GET",
			async:false,
			dataType : "json",
			complete:function(response){
				 var response = eval("(" + response.responseText + ")");
				 dataMap.Airport =objectToArray(response.airports);
				 $("#spnCurrency").text(response.baseCurrency);
				 bCurrency=" "+response.baseCurrency;
				 fillInitdata(response);
				 HidePopProgress()
			
			}
		});
	}
	objectToArray = function(obj){
		rObj = [];
		$.each(obj, function(key, val){
			t = [];
			t[0] = key;
			t[1] = val;
			rObj[rObj.length] = t;
		})
		return rObj
	}
	
	saveGroupRequest=function(){
		if (validateForm()){
			var submitData=getSubmitData();
			$.ajax({ 
				url : 'manageGroupBooking!save.action',
				beforeSend :ShowPopProgress(),
				data : submitData,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					HidePopProgress();
					selectedMainRequest = eval("(" + response.responseText + ")").gbRequest;
					showInfoMessage("Group request saved successfully");
					if ($('input:radio[name=requestType]:checked').val()=="series"){
						$("#groupBookingTabs").tabs("enable", 1);
						$("#groupBookingTabs").tabs("option", "active", 1);
					}else{
						resetSaveForm();
						tLink.click();
					}
				}
			});
		}
	}
	
	
	
	getSubmitData=function(){
		var data={};
	
		if($("#requestID").val()>0){
			data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
		}
		else{
			data["grpBookingReq.version"] =-1;
		}
		data["grpBookingReq.statusID"] = 0;
		data["grpBookingReq.adultAmount"] =$("#adultCount").val();
		data["grpBookingReq.childAmount"] =$("#childCount").val();
		data["grpBookingReq.infantAmount"] =$("#infantCount").val();
		data["grpBookingReq.oalFare"] =$("#oalFare").val();
		data["grpBookingReq.requestedFare"] =$("#requestedFare").val();
		data["grpBookingReq.isRollForward"] =0;
		data["grpBookingReq.requestID"] =$("#requestID").val();
		data["grpBookingReq.agentComments"] =$("#agentRemarks").val();
		data["grpBookingReq.requestedAgentCode"]=$("#travelAgent").val();
		
		var arrayItem=routeArray.split(";");
		var rowIDs=$("#addEditViewData").getDataIDs();
		for(var i=0;i<rowIDs.length;i++){
			var rowData=$("#addEditViewData").getRowData(rowIDs[i]);            
            data["groupBookingRoutes["+i+"].departure"] = rowData.departure;
            data["groupBookingRoutes["+i+"].arrival"] = rowData.arrival;
            data["groupBookingRoutes["+i+"].via"] = rowData.via;
            data["groupBookingRoutes["+i+"].ondCode"] = rowData.ondCode;
            data["groupBookingRoutes["+i+"].departureDateStr"]=convertDate(rowData.departureDate);
            data["groupBookingRoutes["+i+"].flightNumber"]= rowData.flightNumber;
            if(rowData.returningDate != "" && rowData.returningDate != null){
            	data["groupBookingRoutes["+i+"].returningDateStr"]=convertDate(rowData.returningDate); 
            }
        }
		
		
		return data;
	};
	

	
	searchGrpRequest = function(){
		constructGrpReqGrid();
	};
	
	getSearchData=function(){
		var data={};
		data["groupBookingRoutes[0].arrival"] =$("#selTo").val();
		data["groupBookingRoutes[0].departure"] =$("#selFrom").val();
		data["grpBookingReq.statusID"] =$("#status").val();
		data["grpBookingReq.requestID"] =$("#txtReqId").val();
		data["mainReqestOnly"] = $('#mainReqestOnly').is(':checked');
		data["sharedRequest"] = $('#shared').is(':checked');
		data["grpBookingReq.flightNumber"] =$("#flightNumber").val();
		return data;
	}

	
	showAddEditGroupBooking = function(type){		
		if ($(".ui-state-highlight").length == 0 && type=='edit'){ 
			showERRMessage("Select a row to edit");	
		}else{
			$("#btnReleaseSeat").hide();
			$("#grpBookingDetails").parent().show();
			$("#divGrpBSearch").parent().find("a>img").attr("src", "../images/tri-right1_no_cache.gif");
			$("#divGrpBSearch").hide();
			routeArray="";
			resetSaveForm();
			if (type=='edit'){
				setValuesToForm();
			}
			else{
				$("#spnAppprove").hide();
			}
		}
		
	};
	

	
	setValuesToForm = function(){
		
		$("#requestID").val(selectedRowData.groupRequest.requestID);
		$("#adultCount").val(selectedRowData.groupRequest.adultAmount);
		$("#childCount").val(selectedRowData.groupRequest.childAmount);
		$("#infantCount").val(selectedRowData.groupRequest.infantAmount)
		$("#oalFare").val(selectedRowData.groupRequest.oalFare);
		$("#requestedFare").val(selectedRowData.groupRequest.requestedFare);
		$("#agentRemarks").val(selectedRowData.groupRequest.agentComments);
		$("#travelAgent").val(selectedRowData.groupRequest.requestedAgentCode);
		$("#groupBookingTabs").tabs( "option", "active", 0);
		if(selectedRowData.groupRequest.subRequestsNum==0){				
			$("#groupBookingTabs").tabs( "disable", 1);
			$("#groupBookingTabs").tabs("option", "active", 0);
			$('input:radio[name=requestType]')[0].checked = true;
		}
		else{
			$("#groupBookingTabs").tabs( "enable", 1);
			$("#groupBookingTabs").tabs( "option", "active", 0);
			$('input:radio[name=requestType]')[1].checked = true;
		}
		var routes=selectedRowData.groupRequest.groupBookingRoutes;

		UI_commonSystem.fillGridData({id:"#addEditViewData", data:{}});
		
		var routeList = [];
		var itr=0;
		for(var i=0;i<routes.length;i++){
			var routeVal;
			var routeText
			if (routes[i].returningDate==null){
				routeVal=routes[i].departure+"|"+routes[i].arrival+"|"+routes[i].via+"&"+routes[i].departureDate.split("T")[0]+"&"+routes[i].flightNumber;
				routeText=routes[i].ondCode +"("+routes[i].departureDate.split("T")[0]+") : "+routes[i].flightNumber;
				routes[i].routeValue = routeVal;
			}else{
				routeVal=routes[i].departure+"|"+routes[i].arrival+"|"+routes[i].via+"&"+routes[i].departureDate.split("T")[0]+"&"+routes[i].flightNumber+"&"+routes[i].returningDate.split("T")[0];
				routeText=routes[i].ondCode +"("+routes[i].departureDate.split("T")[0]+") Return   - ("+routes[i].returningDate.split("T")[0]+") : "+routes[i].flightNumber;
				routes[i].routeValue = routeVal;
			}
	        var boolAdd=true;
	        
	           
	        if(boolAdd){
	           
	            if(routeArray==""){
	                routeArray=routeVal;
	            }
	            else{
	                routeArray=routeArray+";"+routeVal;
	            }        
	            
	        }
			
		}
		
		UI_commonSystem.fillGridData({id:"#addEditViewData", data:routes});
		
		var minPayment;
		if(selectedRowData.groupRequest.agreedFarePercentage){
			if(selectedRowData.groupRequest.agreedFare != null){
				minPayment = selectedRowData.groupRequest.agreedFare / 100 * selectedRowData.groupRequest.minPaymentRequired;
			}
		}else{
			minPayment = selectedRowData.groupRequest.minPaymentRequired;
		}
		
		//Check the status of the group request and set the UI appropriately
		//[["0","Pending Approval"],["1","Approve"],["2","Reject"],["3","Re-Quote"],["4","Pending after Re-Quote"],["5","Request Release Seat"],["6","Withdraw"],["7","Booked"]];
		if(selectedRowData.groupRequest.statusID==0){
		
			
			$("#spnAppprove").hide();
			$("#btnReleaseSeat,#btnRequote").hide();
			$("#btnSave,#btnDelete,#btnRollFwd").show();
			$("#travelAgent").enable();
			makeSaveFormToEdit();
		}
		
		else if(selectedRowData.groupRequest.statusID==1){
			$("#btnReleaseSeat").removeAttr('disabled');
			$("#btnReleaseSeat").click(updateSeatRelease);
			$("#spnAppprove").show();
			
			$("#spnAppprove").multiline("Group booking request approved.\n Agreed Fare :"+selectedRowData.groupRequest.agreedFare +bCurrency+"\nApprover comments :" +selectedRowData.groupRequest.approvalComments+"\nTarget Payment Date :"+selectedRowData.groupRequest.targetPaymentDate.split("T")[0]+"\nMinimum payment required :"+minPayment+bCurrency+"\nFare valid until "+selectedRowData.groupRequest.fareValidDate.split("T")[0]);
	
			
			$("#btnSave,#btnRequote,#btnRollFwd").hide();
			$("#travelAgent").disable();
			$("#btnReleaseSeat,#btnDelete").show();
			makeSaveFormReadOnly();
			
		}
		
		else if(selectedRowData.groupRequest.statusID==2){
			$("#spnAppprove").show();
			$("#spnAppprove").multiline("Group booking request rejected. \n Administrator comments :" +selectedRowData.groupRequest.approvalComments);
			
			$("#btnSave,#btnDelete,#btnRequote,#btnReleaseSeat,#btnRollFwd").hide();
			$("#travelAgent").disable();
			makeSaveFormReadOnly();
		}
		
		else if(selectedRowData.groupRequest.statusID==3){
			$("#btnRequote").removeAttr('disabled');
			$("#btnRequote").click(updateReQuote);
			$("#spnAppprove").show();
			$("#spnAppprove").multiline("Group booking request asked to re-quote. \n Administrator comments :" +selectedRowData.groupRequest.approvalComments);
	
			$("#btnReleaseSeat,#btnRollFwd,#btnSave").hide();
			$("#travelAgent").disable();
			$("#btnRequote,#btnDelete").show();
			makeSaveFormReadOnly();
		}
			
		else if(selectedRowData.groupRequest.statusID==4){ 
	
			$("#spnAppprove").hide();
			$("#btnSave,#btnRequote,#btnReleaseSeat,#btnRollFwd").hide();
			$("#travelAgent").disable();
			$("#btnDelete").show();
			makeSaveFormReadOnly();
		}
		else if(selectedRowData.groupRequest.statusID==5){
	
			$("#spnAppprove").hide();
			$("#btnSave,#btnDelete,#btnRequote,#btnRollFwd,#btnReleaseSeat").hide();
			$("#travelAgent").disable();
			$("#spnAppprove").multiline("Request for release seats. \n Agreed Fare :"+selectedRowData.groupRequest.agreedFare+bCurrency+"\nApprover comments :" +selectedRowData.groupRequest.approvalComments+"\nTarger Payment Date :"+selectedRowData.groupRequest.targetPaymentDate.split("T")[0]+"\nMinimum payment required :"+minPayment+bCurrency+"\nFare valid until "+selectedRowData.groupRequest.fareValidDate.split("T")[0]);
			makeSaveFormReadOnly();
		}
		else if(selectedRowData.groupRequest.statusID==6){
		
			$("#spnAppprove").hide();
			$("#btnSave,#btnDelete,#btnRequote,#btnReleaseSeat,#btnRollFwd").hide();
			$("#travelAgent").disable();
			makeSaveFormReadOnly();
		}
		else if(selectedRowData.groupRequest.statusID==7){
		
			$("#spnAppprove").show();
			$("#btnSave,#btnDelete,#btnRequote,#btnReleaseSeat,#btnRollFwd").hide();
			$("#travelAgent").disable();
			$("#spnAppprove").multiline("Booking request seat realeased. \n Agreed Fare:"+selectedRowData.groupRequest.agreedFare+bCurrency +"\nApprover comments :" +selectedRowData.groupRequest.approvalComments+"\nTarger Payment Date :"+selectedRowData.groupRequest.targetPaymentDate.split("T")[0]+"\nMinimum payment required :"+minPayment+bCurrency+"\nFare valid until "+selectedRowData.groupRequest.fareValidDate.split("T")[0]);
			makeSaveFormReadOnly();
		}
		else if(selectedRowData.groupRequest.statusID==8){
			
			$("#spnAppprove").show();
			$("#btnSave,#btnDelete,#btnRequote,#btnReleaseSeat,#btnRollFwd").hide();
			$("#travelAgent").disable();
			$("#spnAppprove").multiline("Booking request booked. \n Agreed Fare:"+selectedRowData.groupRequest.agreedFare+bCurrency +"\nApprover comments :" +selectedRowData.groupRequest.approvalComments+"\nTarger Payment Date :"+selectedRowData.groupRequest.targetPaymentDate.split("T")[0]+"\nMinimum payment required :"+minPayment+bCurrency+"\nFare valid until "+selectedRowData.groupRequest.fareValidDate.split("T")[0]+"\nPayment done :"+selectedRowData.groupRequest.paymentAmount);
			makeSaveFormReadOnly();
		}
		
	}
		
	hideAddEditGrpB = function(){
		$(this).find("img").attr("src", "../images/tri-down1_no_cache.gif");
		$("#grpBookingDetails").parent().hide();
		$("#divGrpBSearch").show();
		constructGrpReqGrid();
	};
	constructGrpReqGrid = function(){
		var newUrl = "manageGroupBooking!search.action";
		$("#jqGridGrpBContainer").empty();
		var gridTable = $("<table></table>").attr("id", "jqGridGrpBData");
		var gridPager = $("<div></div>").attr("id", "jqGridGrpBPages");
		$("#jqGridGrpBContainer").prepend(gridTable, gridPager);
		
		var reqData=getSearchData();
		var dateFomatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";
			if (cellVal!=null){
				treturn = cellVal.split("T")[0];
			}
			return treturn;
		}
		var statusFomatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";
			if (cellVal!=null){
				for(var i=0; i<status.length; i++) {
					if(status[i][0]==cellVal){
						treturn = status[i][1];
					}
				}
			}
			return treturn;
		}
		
		var agentFomatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";
			if (cellVal!=null){
				for(var i=0; i<agentNames.length; i++) {
					if(agentNames[i][1]==cellVal){
						treturn = agentNames[i][0];
					}
				}
			}
			return treturn;
		}
		
		
		var paxCountFormatter = function(cellVal, options, rowObject)
		{
		     return rowObject.adults + '/' + rowObject.children+'/'+rowObject.infants;
		}
		
		var temGrid = $("#jqGridGrpBData").jqGrid({
			datatype: function(postdata) {
		        $.ajax({
		           url : 'manageGroupBooking!search.action',
		           beforeSend :ShowPopProgress(),
		           data: $.extend(postdata, reqData),
		           dataType:"json",
		           type : "POST",		           
				   complete: function(jsonData,stat){	        	  
		              if(stat=="success") {
		            	  mydata = eval("("+jsonData.responseText+")");
		            	  if (mydata.msgType != "Error"){
			            	 $.data(temGrid[0], "gridData", mydata.rows);
			            	 temGrid[0].addJSONData(mydata);
			            	
		            	  }else{
		            		  alert(mydata.message);
		            	  }
		            	  HidePopProgress();
		              }
		           }//Group request saved successfully
		        });
		    },
			colNames:[ '&nbsp;', 'Request ID', 'OND', 'Dep. Date', 'Ret. Date', 'AD/CH/IN', 'OAL Fare', 'Flight Number', "Status", "Agent"],//'Requested Fare', "Req. Date", "Req. Status", 
			colModel:[
			    {name:'id',  index:'id', width:25},
			    {name:'requestID', index:'requestID', width:70,jsonmap:"groupRequest.requestID", align:"center"},
			    {name:'ondCode', index:'departureDate', width:90, jsonmap:"groupRequest.firstRouteForDisplay.ondCode", align:"center"},
			    {name:'departureDate', index:'departureDate', width:90, jsonmap:"groupRequest.firstRouteForDisplay.departureDate", align:"center",formatter:dateFomatter},
			    {name:'returningDate', index:'returningDate', width:90, jsonmap:"groupRequest.firstRouteForDisplay.returningDate", align:"center",formatter:dateFomatter},
				{name:'paxCount',  width:80, align:"center",jsonmap:"groupRequest.paxCount"},
				{name:'oalFare', width:70, align:"right",jsonmap:"groupRequest.oalFare"},
				//{name:'requestedFare', width:120,jsonmap:"groupRequest.requestedFare" , align:"left"},
				//{name:'requestDate',  width:150, align:"center",jsonmap:"groupRequest.requestDate",formatter:dateFomatter},
				//{name:'paymentStatus', index:'status', width:80, align:"center",jsonmap:"groupRequest.statusID",formatter:statusFomatter},
				{name:'flightaNumber', width:90, align:'center', jsonmap:'groupRequest.firstRouteForDisplay.flightNumber'},
				{name:'status', width:110, align:'center', jsonmap:'groupRequest.statusID', formatter:statusFomatter},
				{name:'agent', width:120, align:'center', jsonmap:'groupRequest.requestedAgentCode', formatter:agentFomatter},
				
			],
			rowNum:20,
			height: "100%",   
			imgpath: "",
			pager: jQuery('#jqGridGrpBPages'),
			multiselect: false,
			viewrecords: true,
			rowNum:20, 
			sortable:true,
			altRows:true,
			altclass:"GridAlternateRow",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			onSelectRow: function(rowid){
				var selectID=rowid%20;
				setSelectedRow(rowid-1);
			}
		
		}).navGrid("#jqGridGrpBPages",{refresh: true, edit: false, add: false, del: false, search: false});
		
		$("#mainRequestDetails").jqGrid({
			dataType:'local',
			colNames:[ 'Segment' , 'Departure Date' ,'Returning Date', 'Flight Number'],
			colModel:[
			    {name:'ondCode', index:'ondCode', width:110, align:"center"},
				{name:'departureDate', index:'departureDate',  width:110, align:"center",formatter:dateFomatter},
				{name:'returningDate', index:'returningDate', width:110, align:"center",formatter:dateFomatter},
				{name:'flightNumber', index:'flightNumber', width:110, align:"center"}
			]
		});
		
		$("#subRequestDetails").jqGrid({
			dataType:'local',
			colNames:[ 'OND' , 'Departure Date' ,'Flignt Numbers', 'AD/CH/IN',"OAL Fare","Remarks","Main Route"],
			colModel:[
			    {name:'ond', index:'ond', width:70, align:"center"},
				{name:'departureDate', index:'departureDate',  width:110, align:"center"},
				{name:'flightNumbers', index:'flightNumbers', width:110, align:"center",editable:true},
				{name:'paxCount', index:'paxCount', width:110, align:"center",editable:true},
				{name:'oalFare', index:'oalFare', width:110, align:"center",editable:true},
				{name:'remarks', index:'remarks', width:110, align:"center",editable:true},
				{name:'mainRouteRequest', index:'mainRouteRequest', width:110, align:"center",hidden:true}
			],
			allowEdit:true
		});
		
		$("#addEditViewData").jqGrid({
			dataType:'local',
			colNames:[ 'Segment' , 'Departure Date' ,'Returning Date', 'Flight Number', 'Route Value', 'Arrival', 'Departure' ,'Via'],
			colModel:[
			    {name:'ondCode', index:'arrival', width:130, align:"center"},
				{name:'departureDate', index:'departureDate',  width:130, align:"center",formatter:dateFomatter},
				{name:'returningDate', index:'returningDate', width:130, align:"center",formatter:dateFomatter},
				{name:'flightNumber', index:'departure', width:130, align:"center"},
				{name:'routeValue', index:'routeValue', width:110, align:"center", hidden: true },
				{name:'arrival', index:'arrival', width:110, align:"center", hidden: true },
				{name:'departure', index:'departure', width:110, align:"center", hidden: true },
				{name:'via', index:'via', width:110, align:"center", hidden: true }
				
			]
		});
		
	};
	
	
	
	
	//requestedFare
	setSelectedRow = function(id){
		var selectID=id%20;
		selectedRowData = $.data($("#jqGridGrpBData")[0], "gridData")[selectID];
		selectedMainRequest = selectedRowData.groupRequest;
	};
	
	addItemsToSelect = function(id){
		if($("#txtDeparture").val()==""){
			showERRMessage("Please select the departure date");
			return false;
		}
		if($("#txtArrival").val()==""){
			showERRMessage("Please select the arrival date");
			return false;
		}
		if($("#selFrom_1").val()==$("#selTo_1").val()){
			showERRMessage("Depature and arrival cannot be same");
			return false;
		}
		if($("#flightNumber").val()==""){
			showERRMessage("Please give the filght number");
			return false;
		}
		else{  
			var str = $.trim($("#flightNumber").val());
		    var regx = /^[A-Za-z0-9\s,]+$/;
		    if (!regx.test(str)) {
		      	showERRMessage("Only alphanumeric characters, space(' '), and comma(',') are allowed in flight number!");
		       	return false;
		    }
		}
	    var Date1 = new Date($("#txtArrival").val());
		var Date2 = new Date($("#txtDeparture").val());

        if($("#selFrom_1").val()==$("#selTo_1").val()){
            showERRMessage("Depature and return cannot be same"); 
            return false;
        }

        if($("#selVia_1").val()==$("#selTo_1").val() || $("#selVia_1").val()==$("#selForm_1").val()){
            showERRMessage("Via point cannot be same to depature or arrival "); 
            return false;
        }
        
      	var routeVal;
        var routeText;
        var routeData;
        var ret_date = "";
        if($("#selVia_1").val()==""){
        	var ond_code = $("#selFrom_1").val()+"/"+$("#selTo_1").val();
        }
        else{
        	var ond_code = $("#selFrom_1").val()+"/"+$("#selVia_1").val()+"/"+$("#selTo_1").val();
        }
        var dep_date = $("#txtDeparture").val();
        var flight_number = $("#flightNumber").val();
        
        if($('#returning').is(':checked')){
        	routeVal=$("#selFrom_1").val()+"|"+$("#selTo_1").val()+"|"+$("#selVia_1").val()+"&"+$("#txtDeparture").val()+"&"+$("#flightNumber").val()+"&"+$("#txtReturn").val();
            routeText=$("#selFrom_1").val()+"/"+$("#selVia_1").val()+"/"+$("#selTo_1").val()+"("+$("#txtDeparture").val()+")        Return   -  ("+$("#txtReturn").val()+") : "+$("#flightNumber").val();
            ret_date = $("#txtReturn").val();
	        if($("#txtReturn").val()=="")
	        {
	            showERRMessage("Please select the return date");
	            return false;
	        }
            var Date1 = new Date($("#txtReturn").val());
            var Date2 = new Date($("#txtDeparture").val());
            
	        if(Date2 > Date1)
	        {
	            showERRMessage("Departure date cannot be higher than the return date");
	            return false;
	        }
        } else{
         	routeVal=$("#selFrom_1").val()+"|"+$("#selTo_1").val()+"|"+$("#selVia_1").val()+"&"+$("#txtDeparture").val()+"&"+$("#flightNumber").val();
            routeText=$("#selFrom_1").val()+"/"+$("#selVia_1").val()+"/"+$("#selTo_1").val()+"("+$("#txtDeparture").val()+") : "+$("#flightNumber").val();
            var ret_date = "";
        }
        routeData = {ondCode:ond_code, departureDate:dep_date, returningDate:ret_date, flightNumber:flight_number, routeValue:routeVal,	arrival:$("#selTo_1").val(), departure:$("#selFrom_1").val(), via:$("#selVia_1").val()};
       
        var boolAdd=true;
        
        var entryCount = $("#addEditViewData").getGridParam("records");
        if(entryCount<=0)
        	entryCount = 0;
        for(var i=0;i<=entryCount;i++){
        	var rowValue = $('#addEditViewData').getRowData(i);
        	var cellValue = rowValue.routeValue;
            if(cellValue==routeVal){
                boolAdd=false;
            }
        }
       
        if(boolAdd){            
        	var entryCount = $("#addEditViewData").getGridParam("records");
            $("#addEditViewData").addRowData(entryCount+1,routeData);
            if(routeArray==""){
                routeArray=routeVal;
            }
            else{
                routeArray=routeArray+";"+routeVal;
            }
        }
       
    }
    removeItemsFromSelect= function(id){
        var row_id = $('#addEditViewData').getGridParam('selrow');
        var rowValue = $('#addEditViewData').getRowData(row_id);
        var cellValue = rowValue.routeValue;
        $("#addEditViewData").delRowData(row_id);
        var arrayItem=routeArray.split(";");
        for(var i=0;i<arrayItem.length;i++){
            if(arrayItem[i]==cellValue){
                arrayItem.splice(i,1);
                break;
            }
        }
        routeArray="";
        for(var i=0;i<arrayItem.length;i++){
            if(i==0){
                routeArray=arrayItem[i];
            }
            else{
                routeArray=routeArray+";"+arrayItem[i];
            }
        }
       
    }
    validateForm = function(){
        top[2].HidePageMessage();
        var valied = true;
        if ($("#addEditViewData").getGridParam("reccount")==0){
            showERRMessage("Please select the routes");
            valied = false;
        }
        if($("#oalFare").val()==""){
            showERRMessage("Please enter oal fare");
            return false;
        }               
        if(($("#adultCount").val())+($("#childCount").val())+($("#infantCount").val())<1){
            showERRMessage("Passenger count cannot be zero");
            return false;
        }

        
       if($("#adultCount").val()!=""){
        	if(validateField("Adult count","#adultCount")==false){
        		return false;
        	}
        }        
        if($("#childCount").val()!=""){
        	if(validateField("Child count","#childCount")==false){
        		return false
        	}        		
        }        
        if($("#infantCount").val()!=""){
        	if(validateField("Infant count","#infantCount")==false){
        		return false;
        	}
        }
        if($("#oalFare").val()!=""){
        	if(validateFareField("OAL Fare","#oalFare")==false){
        		return false;
        	}
        }
        if($("#requestedFare").val()!=""){
        	if(validateFareField("Requested Fare","#requestedFare")==false){
        		return false;
        	}
        }
        if($("#agentRemarks").val()!=""){
        	var str = $.trim($("#agentRemarks").val());
		    var regx = /^[A-Za-z0-9\s]+$/;
		    if (!regx.test(str)) {
		      	showERRMessage("Only alphanumeric characters are allowed in agent remarks!");
		       	return false;
		    }
        }
        return valied;
    }
    
    validateField = function(fieldName,fieldId){
	    var regx = /^[0-9]+$/;
	    var str = $.trim($(fieldId).val());
	    if (!regx.test(str)) {
	      	showERRMessage("Only numeric characters are allowed in " + fieldName);
	       	return false;
	    }
    }
    validateFareField = function(fieldName,fieldId){
	    var regx1 = /[0-9]+$/;
	    var regx2 = /[0-9]*[.][0-9]+$/;
	    var str = $.trim($(fieldId).val());
	    if ((!regx1.test(str))&&(!regx2.test(str))) {
	      	showERRMessage("Only decimal values are allowed in " + fieldName);
	       	return false;
	    }
    }
    
resetSaveForm = function(){
       
        $("#requestID").val(0);
        $("#adultCount").val(0);
        $("#childCount").val(0);
        $("#infantCount").val(0)
        $("#oalFare").val("");
        $("#requestedFare").val("");
        $("#agentRemarks").val("");
        $("#txtDeparture,#txtReturn").val("");
		$("#flightNumber").val("");        
		$("#selFrom_1,#selTo_1","#selVia_1").attr('selectedIndex', 0);
		UI_commonSystem.fillGridData({id:"#addEditViewData", data:{}});
        $("#spnAppprove").hide();  

    }
makeSaveFormReadOnly = function(){
	

    $("#adultCount").attr('readonly','readonly');
    $("#childCount").attr('readonly','readonly');
    $("#infantCount").attr('readonly','readonly');
    $("#oalFare").attr('readonly','readonly');
    $("#requestedFare").attr('readonly','readonly');
    //$("#agentRemarks").attr('readonly','readonly');
    $("#txtDeparture,#txtReturn").attr('readonly','readonly');
	$("#flightNumber").attr('readonly','readonly');     
	$(".addItem").attr("disabled", true);
    $(".removeItem").attr("disabled", true);
	
}

makeSaveFormToEdit = function(){
   
    $("#adultCount").removeAttr('readonly');
    $("#childCount").removeAttr('readonly');
    $("#infantCount").removeAttr('readonly');
    $("#oalFare").removeAttr('readonly');
    $("#requestedFare").removeAttr('readonly');
    //$("#agentRemarks").removeAttr('readonly');
    //$("#txtDeparture,#txtReturn").removeAttr('readonly');
    $("#flightNumber").removeAttr('readonly');
    $(".addItem").attr("disabled", false);
    $(".removeItem").attr("disabled", false);
   
}
updateSeatRelease=function(){
	var data={};
	
	data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
	data["grpBookingReq.requestID"] =$("#requestID").val();
	data["grpBookingReq.statusID"] =5; //Approval 
	data["grpBookingReq.agentComments"] =$("#agentRemarks").val();

	$.ajax({ 
		url : 'manageGroupBooking!agentReqUpdate.action',
		beforeSend : ShowPopProgress(),
		data : data,
		type : "POST",
		async:false,
		dataType : "json",
		complete:function(response){
			resetSaveForm();
			searchGrpRequest();
	//		HidePopProgress();
			showInfoMessage("Released seat request is made successfully")
		}
	});
}

updateReQuote=function(){
	var data={};
	
	data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
	data["grpBookingReq.requestID"] =$("#requestID").val();
	data["grpBookingReq.statusID"] =4; //Approval 
	data["grpBookingReq.agentComments"] =$("#agentRemarks").val();

	$.ajax({ 
		url : 'manageGroupBooking!agentReqUpdate.action',
		beforeSend : ShowPopProgress(),
		data : data,
		type : "POST",
		async:false,
		dataType : "json",
		complete:function(response){
			resetSaveForm();
			searchGrpRequest();
//			HidePopProgress();
			showInfoMessage("Group booking request Re-Quote successfully")
		}
	});
}
withdrawRequest=function(){
	var data={};
	
	data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
	data["grpBookingReq.requestID"] =$("#requestID").val();
	data["grpBookingReq.statusID"] =6; //Approval 
	data["grpBookingReq.agentComments"] =$("#agentRemarks").val();

	$.ajax({ 
		url : 'manageGroupBooking!agentReqUpdate.action',
		beforeSend : ShowDelete,
		data : data,
		type : "POST",
		async:false,
		dataType : "json",
		complete:function(response){
			resetSaveForm();
			searchGrpRequest();
			HidePopProgress();
			showInfoMessage("Group booking request withdraw successfully")
		}
	});
}
function ShowPopProgress() {
	setVisible("divLoadMsg", true);
}

function HidePopProgress() {
	setVisible("divLoadMsg", false);
}
function ShowDelete() {				
    return confirm("Are you sure you want to withdraw this group booking request?"); 
}
function showInfoMessage(strInfoMessage) {
	top[2].objMsg.MessageText = strInfoMessage;
	top[2].objMsg.MessageType = "Confirmation" ; 
	top[2].ShowPageMessage();
	return true;
}
}

UI_groupBooking = new UI_groupBooking();
UI_groupBooking.ready();