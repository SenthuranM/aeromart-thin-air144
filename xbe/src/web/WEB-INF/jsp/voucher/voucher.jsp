<%@ page language="java"%>
<%@ include file="../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link href="../css/loadCSS_no_cache.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css" />
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<%-- 	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
<script
	src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script
	src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script
	src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/voucher/voucher.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<style>
#spnError div.errorBoxHttps {
	padding: 2px 9px 0px 8px;
	background: #F3F376;
	-moz-border-radius: 4px;
}
</style>

</head>
<body style="background: rgb(255, 255, 255);"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown="return Body_onKeyDown(event)">
	<!--<body style="background: transparent;"> -->
	<%@ include file="../common/IncludeTop.jsp"%>
	<div
		style="overflow: auto; height: 650px; width: 970px; margin: 0 auto"
		id="voucherContainer">
		<!-- Page Background Top page -->
		<table width="100%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Search Vouchers <%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="2" cellspacing="2">
						<tr>
							<td width="15%"><font>Voucher ID </font></td>
							<td width="10%"><input name="voucherIDSrch" type="text"
								id="voucherIDSrch" size="20" maxlength="60" /></td>
							<td width="15%"><font>Issued Agent </font></td>
							<td width="10%"><input name="issuedAgntSrch" type="text"
								id="issuedAgntSrch" size="20" maxlength="60" /></td>
						</tr>
						<tr>
							<td width="15%"><font>Email </font></td>
							<td width="10%"><input name="emailSrch" type="text"
								id="emailSrch" size="30" maxlength="60" /></td>
							<td width="15%"><font>First name </font></td>
							<td width="10%"><input name="fnameSrch" type="text"
								id="fnameSrch" size="20" maxlength="60" /></td>
							<td width="15%"><font>Last name </font></td>
							<td width="10%"><input name="lnameSrch" type="text"
								id="lnameSrch" size="20" maxlength="60" /></td>
							<td>&nbsp;</td>
							<td align="left" colspan="3"><input name="btnSearch"
								type="button" class="Button" id="buttonSearch" value="Search" />
							</td>
						</tr>

					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Vouchers<%@ include
						file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="4">
						<tr>
							<td id="VoucherDetailsGridContainer"></td>
						</tr>
						<tr>
							<td><u:hasPrivilege privilegeId="xbe.voucher.gen">
									<input type="button" name="Voucher.addButton" id="btnIssue"
										class="Button" value="Issue" />
								</u:hasPrivilege> <u:hasPrivilege privilegeId="xbe.voucher.cancel">
									<input name="voucher.btnCancel" type="button" class="Button"
										id="btnCancel" value="Cancel" />
								</u:hasPrivilege></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Add Vouchers &nbsp;&nbsp;<%@ include
						file="../common/IncludeFormHD.jsp"%>
					<table width="98%" border="0" cellpadding="2" cellspacing="2"
						align="center">
						<tr>
							<td style='height: 4px'></td>
						</tr>
						<tr>
							<td id="lblVoucherId">Voucher ID</td>
							<td width="10%">&nbsp</td>
							<td width="10%"><input name="voucher.voucherId" type="text"
								id="voucherId" size="20" maxlength="60" readonly="readonly" /></td>
						</tr>
						<tr>
							<td>
								<font class="fntBold">Customer Details</font>
							</td>
							<td width="8%">First name</td>
							<td width="17%"><input name="voucher.paxFirstName"
								type="text" id="paxFirstName" size="20" maxlength="60" /> <label
								id="paxFirstNameMand" class="mandatory">*</label></td>
							<td width="8%">Last name</td>
							<td width="17%"><input name="voucher.paxLastName"
								type="text" id="paxLastName" size="20" maxlength="60" /> <label
								id="paxLastNameMand" class="mandatory">*</label></td>
							<td width="8%">Email</td>
							<td width="17%"><input name="voucher.email" type="text"
								id="email" size="30" maxlength="60" /> <label id="emailMand"
								class="mandatory">*</label></td>
						</tr>
						<tr>
							<td><font class="fntBold">Voucher Details</font></td>
							<td width="8%">Valid from</td>
							<td width="17%"><input name="voucher.validFrom" type="text"
								id="validFrom" size="20" maxlength="60" readonly="readonly" /> <label
								id="validFromMand" class="mandatory">*</label></td>
							<td width="8%">Valid to</td>
							<td width="17%"><input name="voucher.validTo" type="text"
								id="validTo" size="20" maxlength="60" readonly="readonly" /> <label
								id="validToMand" class="mandatory">*</label></td>
							<td width="8%">Amount</td>
							<td width="17%"><input name="voucher.amount" type="text"
								id="amount" size="20" maxlength="60" /> <label id="validToMand"
								class="mandatory">*</label> <select name="voucher.currencyCode"
								id="currencyCode"></select> <label id="validToMand"
								class="mandatory">*</label></td>
						</tr>
						<tr><td><font class="fntBold">&nbsp</font></td>
							<td align="justify">Message</td>
							<td width="10%" colspan="3"><textarea name="voucher.remarks"
									id="remarks" cols="50" rows="6"></textarea></td>
						</tr>
						<tr class="visible">
							<td align="justify">Cancel Remarks</td>
							<td width="10%">&nbsp;</td>
							<td width="10%" colspan="3"><textarea
									name="voucher.cancelRemarks" id="cancelRemarks" cols="50"
									rows="4"></textarea> <label id="validToMand" class="mandatory">*</label>
							</td>
						</tr>
						<tr>
							<td width="15%">&nbsp;</td>
							<td style="height: 15px;" valign="bottom" align="right"
								colspan="6"><u:hasPrivilege privilegeId="xbe.voucher.gen">
									<input name="btnSave" type="button" class="Button" id="btnSave"
										value="Save" />
									<select id="selLanguage">									
										<c:out value="${requestScope.reqLanguageHtml}" escapeXml="false" />
									</select>
									<input name="btnEmail" type="button" class="Button"
										id="btnEmail" value="Email" />
									<input name="btnPrint" type="button" class="Button"
										id="btnPrint" value="Print" />
									<input name="btnReset" type="button" class="Button"
										id="btnReset" value="Reset" />
								</u:hasPrivilege> <u:hasPrivilege privilegeId="xbe.voucher.cancel">
									<input name="btnCancelConfirm" type="button" class="Button"	id="btnCancelConfirm" style="width:150px" value="Confirm Cancel" />
								</u:hasPrivilege></td>
						</tr>
						<tr>
							<td><font class="fntSmall">&nbsp;</font></td>
						</tr>
						<tr>
							<td>
								<div id='divLoadMsg'
									style='visibility: hidden; z-index: 1000; background: transparent;'>
									<iframe src="showFile!loadMsg.action" id='frmLoadMsg'
										name='Voucher.frmLoadMsg' frameborder='0' scrolling='no'
										style='position: absolute; top: 50%; left: 40%; width: 260; height: 40;'></iframe>
								</div>
							</td>
						</tr>

					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td align="right">
					<button name="Voucher.btnClose" id="btnClose" class="Button">Close</button>
				</td>
			</tr>
		</table>



		<script type="text/javascript">
			<c:out value="${requestScope.reqUserRoleHtmlData}" escapeXml="false" />
		</script>

		<!-- 	</form>	 -->
		<script
			src="../js/voucher/voucher.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
			type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {
				$("#tabs").tabs();
				$("#lstRoles, #lstAssignedRoles").css("width", "335px");
				$("#lstRoles, #lstAssignedRoles").css("height", "150px");
			});
		</script>
	</div>
	<%@ include file="../common/IncludeBottom.jsp"%>
</body>
</html>