<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/voucher/issueVoucherForReprotectedPax.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<style>
		#spnError div.errorBoxHttps{
			padding: 2px 9px 0px 8px;
			background: #F3F376;
			-moz-border-radius:4px;
		}
	</style>

</head>
<body style="background: rgb(255, 255, 255);"	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"	onkeydown="return Body_onKeyDown(event)">
	<!--<body style="background: transparent;"> -->
	<%@ include file="../common/IncludeTop.jsp"%>
	<div style="overflow: auto; height: 650px; width: 970px; margin: 0 auto" id="voucherRePaxContainer">
		<!-- Page Background Top page -->
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Reprotected Passengers To Issue Voucher						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="2" cellspacing="2">
							<tr>
								<td width="8%">
									<font>Flight Number </font>
								</td>
								<td width="10%">
									<input  name="flightNumSrch" type="text" id="flightNumSrch" size="20" maxlength="60" style="text-transform:uppercase"/>
									<label	id="validToMand" class="mandatory">*</label>
								</td>
								<td width="12%" align="center">
									<font> Flight Date </font>
								</td>
								<td width="15%">
									<input name="dateSrch" type="text" id="dateSrch" size="20" maxlength="60" onblur="dateChk('dateSrch')"/>
									<label	id="validToMand" class="mandatory">*</label>
								</td>				
						        <td align="left" width="20%" >
								  	<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search" /> 
								</td>
							</tr>							
	 				    </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Reprotected Passengers<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4">							
							<tr>
								<td id="reprotectedPassengersContainer">
								
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="xbe.voucher.gen">
										<input type="button"  name="Voucher.issueButton" id="btnIssue" class="Button" value="Issue"/>
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>
					Voucher Details &nbsp;&nbsp;<%@ include file="../common/IncludeFormHD.jsp"%>
					
						<table width="100%">
							<tr>
								<td width="10%" >Issue Type</td>
								<td width="5%">&nbsp</td>
								<td width="15%">
									<select name="issueBy" id="issueBy" size="1" style="width:105;" class="UCase">
										<option value="V">V</option>
										<option value="PF">PF</option>										
									</select>
			  					 </td>
			  					 <td width="70%">&nbsp</td>
							</tr>
							<tr>
								<td width="10%">Value / Percentage</td>
								<td width="5%">&nbsp</td>
								<td width="15%">
									<input  name="pfValue" type="text" id="pfValue" size="20" maxlength="13" onkeypress="return isNumberKey(event)" />
									<label id="pfValueMand" class="mandatory">*</label>
			  					</td>
			  					<td width="70%">&nbsp</td>
							</tr>
							<tr>
								<td width="10%">Valid from</td>
								<td width="5%">&nbsp</td>
								<td width="18%"><input name="voucher.validFrom" type="text"
									id="validFrom" size="20" maxlength="60" readonly="readonly"/>
									<label id="validFromMand" class="mandatory">*</label></td>
								<td width="70%"><table width="100%">
									<tr>
										<td width="15%" align="center">Valid to</td>
										<td width="30%"><input name="voucher.validTo" type="text"
											id="validTo" size="20" maxlength="60" readonly="readonly"/> <label
											id="validToMand" class="mandatory">*</label></td>
										</tr>
									</table>
								</td>		
							
							</tr>
							<tr>
								<td width="10%">Message</td>
								<td width="5%">&nbsp</td>
								<td width="40%" colspan="3"><textarea name="voucher.remarks"
										id="remarks" cols="50" rows="6"></textarea></td>
							</tr>
							<tr id="trButtons">
								<td width="15%" >&nbsp;</td>
			                    <td style="height:15px;" valign="bottom" align="right" colspan="6">
									<u:hasPrivilege privilegeId="xbe.voucher.gen">
										<input name="btnIssue" type="button" class="Button" id="btnIssueVoucher"  value="Issue"/>
										<input name="btnReset" type="button" class="Button" id="btnReset"  value="Reset"/>	
									</u:hasPrivilege> 
				              	</td>
							</tr>
							<tr>
								<td><font class="fntSmall">&nbsp;</font></td>
							</tr>
							<tr>
								<td>
			  						<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
										<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='Voucher.frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
									</div>
								</td>
							</tr>

			           </table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>  
			    </td>
			</tr>
			    <tr>	
			    	<td align="right">    	
						<button name="Voucher.btnClose" id="btnClose" class="Button">Close</button>
			    	</td>
			    </tr>			
			</table>	
	


	<script type="text/javascript">
		<c:out value="${requestScope.reqUserRoleHtmlData}" escapeXml="false" />
	</script>
		

  <script type="text/javascript">

  	$(function() {
		$( "#tabs" ).tabs();
		$("#lstRoles, #lstAssignedRoles").css("width","335px");
		$("#lstRoles, #lstAssignedRoles").css("height","150px");
	});

  </script>
</div>
	<%@ include file="../common/IncludeBottom.jsp"%>
</body>
</html>