<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/voucher/giftVoucher.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<style>
		#spnError div.errorBoxHttps{
			padding: 2px 9px 0px 8px;
			background: #F3F376;
			-moz-border-radius:4px;
		}
	</style>

</head>
<body style="background: rgb(255, 255, 255);"	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"	onkeydown="return Body_onKeyDown(event)">
	<!--<body style="background: transparent;"> -->
	<%@ include file="../common/IncludeTop.jsp"%>
	<div style="overflow: auto; height: 650px; width: 970px; margin: 0 auto" id="voucherContainer">
		<!-- Page Background Top page -->
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Gift Vouchers						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="2" cellspacing="2">
							<tr>
								<td width="15%">
									<font>Voucher ID </font>
								</td>
								<td width="10%">
									<input  name="voucherIDSrch" type="text" id="voucherIDSrch" size="20" maxlength="60" />
								</td>
								<td width="15%">
									<font>Issued Agent </font>
								</td>
								<td width="10%">
									<input  name="issuedAgntSrch" type="text" id="issuedAgntSrch" size="20" maxlength="60" />
								</td>
							</tr>
							<tr>
								<td width="15%">
									<font>Email </font>
								</td>
								<td width="10%">
									<input  name="emailSrch" type="text" id="emailSrch" size="30" maxlength="60" />
								</td>
								<td width="15%">
									<font>First name </font>
								</td>
								<td width="10%">
									<input  name="fnameSrch" type="text" id="fnameSrch" size="20" maxlength="60" />
								</td>
								<td width="15%">
									<font>Last name </font>
								</td>
								<td width="10%">
									<input  name="lnameSrch" type="text" id="lnameSrch" size="20" maxlength="60" />
								</td>
								<td>&nbsp;</td>						
						        <td align="left" colspan="3">
								  	<input name="btnSearch" type="button" class="Button" id="buttonSearch" value="Search" /> 
								</td>
							</tr>
							
	 				    </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Gift Vouchers<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4">							
							<tr>
								<td id="GiftVoucherGridContainer">
								
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="xbe.voucher.gen">
										<input type="button"  name="Voucher.addButton" id="btnIssue" class="Button" value="Issue"/>
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="xbe.voucher.gift.cancel">
									<input type="button" name="Voucher.cancelButton" id="btnCancel"
										class="Button" value="Cancel" />
									</u:hasPrivilege>
								</td>							
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>
					Sell Gift Vouchers &nbsp;&nbsp;<%@ include file="../common/IncludeFormHD.jsp"%>
					
					<div id="tabs">
						<ul>
							<li><a href="#tabs-1">Gift Voucher</a></li>
							<li><a href="#tabs-2">Payment</a></li>
							<li><span id="spnError">&nbsp;</span></li>
						</ul>
						<div id="tabs-1">
							<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
							<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
								<tr>
									<td>
										<font class="fntBold">Customer Details</font>
									</td>
								
								</tr>
								<tr>
									<td width="8%">
										First name
									</td>
									<td width="17%">
										<input  name="voucher.paxFirstName" type="text" id="paxFirstName" size="20" maxlength="60" />
										<label id="paxFirstNameMand" class="mandatory">*</label>
									</td>
									<td width="8%">
										Last name
									</td>
									<td width="17%">
										<input  name="voucher.paxLastName" type="text" id="paxLastName" size="20" maxlength="60" />
									<label id="paxLastNameMand" class="mandatory">*</label>
									</td>
										<td width="5%">
											Email
										</td>
										<td width="17%">
											<input  name="voucher.email" type="text" id="email" size="25" maxlength="60" style='width : 80%'/>
											<label id="emailMand" class="mandatory">*</label>
										</td>
									<td width="10%">
										Mobile Number
									</td>
									<td width="13%">
										<input  name="voucher.mobileNumber" type="text" id="mobileNumber" min="10" size="15"/>
										<label id="mobileNumberMand" class="mandatory">*</label>
									</td>
									</tr>
								</table>					
								<%@ include file="../common/IncludeFormBottom.jsp"%> 
								<table><tr><td>
									<font class="fntTiny">&nbsp;</font> 
								</td></tr></table>
							<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
							<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
								<tr>
									<td style='height: 4px'></td>
								</tr>
								<tr><td><font class='fntTiny'>&nbsp;</font></td></tr>	
								<tr id="trVoucherSearch">
									<td id="lblVoucherSearch">
										<font class="fntBold">Voucher Search</font>
									</td>
									<td width="10%">
										Amount
									</td>
									<td width="10%">
										<select  name="selVoucherAmount" id="selVoucherAmount"/>
									</td>
									<td id="lblVoucherCurrency">
										Sell Currency
									</td>
									<td width="10%">
										<select  name="selVoucherCurrency" id="selVoucherCurrency"/>
									</td>
									<td id="lblVoucherValidity">
										Validity
									</td>
									<td width="10%">
										<select  name="selVoucherValidity" id="selVoucherValidity"/>
									</td>									
								</tr>
								<tr><td>
									<font class="fntTiny">&nbsp;</font> 
								</td></tr>
								<tr>
									<td id="lblVoucherName">
										Voucher Name
									</td>
									<td width="10%">
										&nbsp;
									</td>
									<td width="10%">
										<select  name="selVoucherName" id="voucherName"/>
									</td>
									<td id="lblVoucherQuantity">
										Quantity
									</td>
									<td width="10%">
										<input type="number" name="voucher.quantity"  id="quantity" size="10" maxlength="5" min="1" value="1" style="width:30px"/>
										<label id="quantityMand" class="mandatory">*</label>
									</td>	
								</tr>
								<tr><td>
									<font class="fntTiny">&nbsp;</font> 
								</td></tr>
								<tr>
									<td>
										<font class="fntBold">Voucher Details</font>
									</td>
									<td width="10%">
										Valid from
									</td>
									<td width="17%">
										<input  name="voucher.validFrom" type="text" id="validFrom" size="20" maxlength="60" readonly="readonly"/>
										<label id="validFromMand" class="mandatory">*</label>
									</td>
									<td width="10%">
										Valid to
									</td>
									<td width="17%">
										<input  name="voucher.validTo" type="text" id="validTo" size="20" maxlength="60" readonly="readonly"/>
										<label id="validToMand" class="mandatory">*</label>								
									</td>
									<td width="10%">
										Amount
									</td>
									<td width="20%">
										<label id="amount"></label>
										<label id="currencyCode"></label>
									</td>
								</tr>
								<tr><td>
									<font class="fntTiny">&nbsp;</font> 
								</td></tr>
								<tr>
									<td align="left" valign="top">
										Message
									</td>
									<td width="10%">
										&nbsp;
									</td>
									<td width="10%" colspan="3">
										<textarea  name="voucher.remarks" id="remarks" cols="50" rows="6"></textarea>
									</td>												
									<td align="right" valign="bottom" rowspan='4'>
										
									</td>
									<td align="right" valign="bottom" rowspan='4'>
										<input name="btnAdd" type="button" class="Button"	id="btnAdd" style="width:80px" value="Add Another" />
										<input name="btnAddToCart" type="button" class="Button"	id="btnAddToCart" style="width:80px" value="Add To Buy" />
									</td>
								</tr>
								<tr><td>
									<font class="fntTiny">&nbsp;</font> 
								</td></tr>
								<tr class="visible">
									<td align="justify">Cancel Remarks</td>
									<td width="10%">&nbsp;</td>
									<td width="10%" colspan="3"><textarea
											name="voucher.cancelRemarks" id="cancelRemarks" cols="50"
											rows="4"></textarea> <label id="validToMand"
										class="mandatory">*</label></td>
								</tr>
								<tr><td>
									<font class="fntTiny">&nbsp;</font> 
								</td></tr>
							</table>
							<%@ include file="../common/IncludeFormBottom.jsp"%>  
							<table><tr><td>
									<font class="fntTiny">&nbsp;</font> 
								</td></tr></table>
							<div  id="summaryPannel">
							<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
							<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8"><tr>
									<td>
										<font class="fntBold">Vouchers To Buy</font>
									</td>
									<td width="10%">
										Total
									</td>
									<td width="17%">
										<input  name="voucher.totalQ" type="text" id="totalQuantity" size="10" maxlength="60" readonly/>
									</td>
									<td width="10%">
										Full Amount
									</td>
									<td width="17%">
										<input  name="voucher.totalAmount" type="text" id="totalAmount" size="20" maxlength="60" readonly/>
									</td>
										<td width="10%">
										</td>
										<td width="20%">
										</td>
									</tr></table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>  
								</div>
						</div>
						<div id="tabs-2">
							<table id="paymentTypes" width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
								<u:hasPrivilege  privilegeId="xbe.res.make.pay.cash">
									<tr>
										<td id="radioPaymentCash">
											<input name="paymentType" type="radio" id="paymentType" value="CA"/>
										</td>
										<td>
											<label id="lblPaymentCash">Cash</label>
										</td>
									</tr>
								</u:hasPrivilege>
								<u:hasPrivilege  privilegeId="xbe.res.make.pay.card">
									<tr>
										<td id="radioPaymentCC" width="5%">
											<input name="paymentType" type="radio" id="paymentType" value="CC"/>
										</td>
										<td width="20%">
											<label id="lblPaymentCC">Credit Card</label>
										</td>
										<td width="75%">
											<select id="selCCType" style="display:none">
											</select>
										</td>
									</tr>
								</u:hasPrivilege>
								<u:hasPrivilege  privilegeId="xbe.res.make.pay.acc">							
									<tr>
										<td id="radioOnAccount" width="5%">
											<input name="paymentType" type="radio" id="paymentType" value="OA"/>
										</td>
										<td width="20%">
											<label id="lblPaymentOnAccount" >On Account</label>
										</td>
										<td width="75%">
											<select id="selAgent" style="display:none">
											</select>
										</td>
									</tr>
									
									<tr id="trOAPaymentProcess">
										<td width="25%" colspan="2">
											<label id="lblPaymentOnAccount" >Your Payment will be processed in</label>
										</td>
										<td width="75%">
											<label id="oaCurrencyCodeDesc" ></label>
										</td>
									</tr>
									<tr id="trOAPayment">
										<td width="25%" colspan="2">
											<label id="lblPaymentOnAccount" >Your payment</label>
										</td>
										<td width="75%">
											<label id="oaAmount" ></label>
											<label id="oaCurrencyCode" ></label>
										</td>
									</tr>
									
								</u:hasPrivilege>
							</table>
							<u:hasPrivilege  privilegeId="xbe.res.make.pay.card">
							<table id="ccDetails" width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
								<tr>
									<td width="40%">
										<table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
											<tr>
												<td style='height: 4px'></td>
											</tr>
											<tr id="trCardType">
												<td width="40%" id="tdLblCardType">
													<label id="lblCardType">
														Card Type 
													</label>
												</td>
												<td id="tdSelCardType">
													<select  name="selCardType" id="selCardType"/>
												</td>
											</tr>
											<tr id="trCardNumber">
												<td width="40%" id="tdLblCardNumber">
													<label id="lblCardNumber">
														Card Number
													</label>
												</td>
												<td id="tdTxtCardNumber">
													<input type="text" name="txtCardNumber" id="txtCardNumber"/>
												</td>
											</tr>
											<tr id="trCardHolderName">
												<td width="40%" id="tdLblCardHolderName">
													<label id="lblCardHolderName">
														Card Holder Name
													</label>
												</td>
												<td id="tdTxtCardHolderName">
													<input type="text" name="txtCardHolderName" id="txtCardHolderName"/>
												</td>
											</tr>
											<tr id="trCardExpiry">
												<td width="40%" id="tdLblCardExpiry">
													<label id="lblCardExpiry">
														Card Expiry Date <b>(MMYY)</b>
													</label>
												</td>
												<td id="tdTxtCardExpiry">
													<input type="text" name="txtCardExpiry" id="txtCardExpiry"/>
												</td>
											</tr>
											<tr id="trCardCVV">
												<td width="40%" id="tdLblCardCVV">
													<label id="lblCardCVV">
														Card's CVV
													</label>
												</td>
												<td id="tdTxtCardCVV">
													<input type="text" name="txtCardCVV" id="txtCardCVV"/>
												</td>
											</tr>
											<tr id="trTnxFee">
												<td width="40%" id="tdLblTnxFee">
													<label id="lblTnxFee">
														Transaction Fee
													</label>
												</td>
												<td id="tdTxtTnxFee">
													<label id="tnxFee"></label>
													<label id="currencyCodeTxtTnxFee"></label>
												</td>
											</tr>
											<tr id="trFullAmount">
												<td width="40%" id="tdLblFullAmount">
													<label id="lblFullAmount">
														Full Amount
													</label>
												</td>
												<td id="tdTxtCardAmount">
													<label id="fullAmount"></label>
													<label id="currencyCodeFullAmount"></label>
												</td>
											</tr>
											
											<tr>
												<td>
													<input type="hidden" id="hdnFullAmount"/>
													<input type="hidden" id="hdnTnxFee"/>
													<input type="hidden" id="hdnCurrencyCode"/>
												</td>
											</tr>
										</table>
									</td>
									<td width='15%' align='center'>
										<a href="#" onclick='UI_giftVoucher.showVeriSign(); return false;'><img src="../images/VSign_no_cache.gif" border="0"/></a>
									</td>
								</tr>
								<tr>
									<td width="15%" >&nbsp;</td>
						            <td style="height:15px;" valign="bottom" align="right">
										<input name="ccPyamentConfirm" type="button" class="Button" id="ccPyamentConfirm"  value="Confirm"/>
										<input name="ccPyamentReset" type="button" class="Button" id="ccPyamentReset"  value="Reset"/>
										<input name="ccPyamentCancel" type="button" class="Button" id="ccPyamentCancel"  value="Cancel"/>            
							    	</td>
								</tr>
							</table>
							</u:hasPrivilege>
						</div>
						<u:hasPrivilege  privilegeId="xbe.res.make.pay.card">
						<div id="tabs-3">
							
						</div>
						</u:hasPrivilege>
					</div>
						<table width="100%">				
							<tr id="trButtons">
								<td width="15%" >&nbsp;</td>
			                    <td style="height:15px;" valign="bottom" align="right" colspan="6">
									<u:hasPrivilege privilegeId="xbe.voucher.gen">
										<input name="btnPay" type="button" class="Button" id="btnPay" style="width:150px" value="Pay & Confirm"/>
										<input name="btnSave" type="button" class="Button" id="btnSave" value="Continue"/>
										<select id="selLanguage">									
											<c:out value="${requestScope.reqLanguageHtml}" escapeXml="false" />
										</select>
										<input name="btnEmail" type="button" class="Button" id="btnEmail"  value="Email"/>
										<input name="btnPrint" type="button" class="Button" id="btnPrint"  value="Print"/>
										<input name="btnReset" type="button" class="Button" id="btnReset"  value="Reset"/>	
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="xbe.voucher.cancel">
										<input name="btnCancelConfirm" type="button" class="Button"	id="btnCancelConfirm" style="width:150px" value="Confirm Cancel" />
									</u:hasPrivilege>              
				              	</td>
							</tr>
							<tr>
								<td><font class="fntSmall">&nbsp;</font></td>
							</tr>
							<tr>
								<td>
			  						<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
										<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='Voucher.frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
									</div>
								</td>
							</tr>

			           </table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>  
			    </td>
			</tr>
			    <tr>	
			    	<td align="right">    	
						<button name="Voucher.btnClose" id="btnClose" class="Button">Close</button>
			    	</td>
			    </tr>			
			</table>	
	


	<script type="text/javascript">
		<c:out value="${requestScope.reqUserRoleHtmlData}" escapeXml="false" />
	</script>
		
<!-- 	</form>	 -->
<%-- 	<script src="../js/voucher/voucher.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
  <script type="text/javascript">

  	$(function() {
		$( "#tabs" ).tabs();
		$("#lstRoles, #lstAssignedRoles").css("width","335px");
		$("#lstRoles, #lstAssignedRoles").css("height","150px");
	});

  </script>
</div>
	<%@ include file="../common/IncludeBottom.jsp"%>
</body>
</html>