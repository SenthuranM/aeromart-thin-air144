<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
    <link rel='stylesheet' type='text/css' href='../css/Style_no_cache.css'>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>   
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>       
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>		
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>   	
  </head>
 <body onkeydown='return Body_onKeyDown(event)' class='PageBackGround' ondrag='return false' scroll="yes" oncontextmenu="return showContextMenu()">
	 <table width='330' align='center' border='0' cellpadding='0' cellspacing='0'>
 		<tr>
 			<td height="20" width="15"><img src="../images/AA004_no_cache.gif"></td>
 			<td height="20" style="background-image:url(../images/AA005_no_cache.jpg);"></td>
 			<td height="20" width="15"><img src="../images/AA006_no_cache.gif"></td>
		</tr>
		<tr>
 			<td style="background-image:url(../images/AA010_no_cache.jpg);"></td>
 			<td style="background-image:url(../images/AA011_no_cache.jpg);">
				<table width='300' align='center' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td align="center">
							<img src="../images/AA165_no_cache.jpg">
							<br>
							<font><span id="spnMessage">Request timeout has occurred. <br>Press <b>Ok</b> to view the Login Page</span><br>
							<br><br>
						</td>
					</tr>
					<tr>
						<td align="center">
							<input type="button" id="btnOK" value="OK" class="Button" NAME="btnContinue" onclick="btnContinueOnClick(0)">
						</td>
					</tr>
				</table>
 			</td>
 			<td style="background-image:url(../images/AA012_no_cache.jpg);"></td>	
 		</tr>
 		<tr>
 			<td height="20"><img src="../images/AA007_no_cache.gif"></td>
 			<td height="20" style="background-image:url(../images/AA008_no_cache.jpg);"></td>
 			<td height="20"><img src="../images/AA009_no_cache.gif"></td>
		</tr>	
	</table>
	<script type="text/javascript">
	<!--
	function btnContinueOnClick(strID){
		switch (strID){
			case 0 :
				var strHTMLText = "<table width='250' border='0' cellpadding='2' cellspacing='1' align='center'>";
				strHTMLText += "<tr>";
				strHTMLText += "		<td align='center' style='height:30; background-image: url(../images/Progress_no_cache.gif)'>";
				strHTMLText += "			<font class=fntBold>Loading in progress. Please Wait...</font>";
				strHTMLText += "		</td>";
				strHTMLText += "	</tr>";
				strHTMLText += "</table>";
				DivWrite("spnMessage", strHTMLText);
				Disable("btnOK", true);
				makePOST("../public/logout.action","");		
				break;
		}
	}
	
	function processStateChange(){
		if (httpReq.readyState==4){
			top.location.replace("closeMenu");
		}
	}
	
	function CloseWindow(){
		//window.close();
	}
	//-->
	</script>
</body>	
</html>
			
