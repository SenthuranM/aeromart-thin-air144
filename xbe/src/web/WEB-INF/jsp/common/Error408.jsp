<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
    <link rel='stylesheet' type='text/css' href='../css/Style_no_cache.css'>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
  </head>
 <body class='PageBackGround' ondrag='return false' scroll="no" oncontextmenu="return showContextMenu()">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td style="height:50"><font>&nbsp;</font></td>
		</tr>
		<tr>
			<td align="right">
				<img src="../images/AA110_no_cache.jpg">
			</td>
		</tr>
		<tr>
			<td style="height:50"><font>&nbsp;</font></td>
		</tr>
		<tr>
			<td>
				<table width='330' align='center' border='0' cellpadding='0' cellspacing='0'>
			 		<tr>
			 			<td height="20" width="15"><img src="../images/AA004_no_cache.gif"></td>
			 			<td height="20" style="background-image:url(../images/AA005_no_cache.jpg);"></td>
			 			<td height="20" width="15"><img src="../images/AA006_no_cache.gif"></td>
					</tr>
					<tr>
			 			<td style="background-image:url(../images/AA010_no_cache.jpg);"></td>
			 			<td style="background-image:url(../images/AA011_no_cache.jpg);">
							<table width='300' align='center' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td align="center">
										<font>
										Request timeout has occurred. <br><br>
										Click <b>Retry</b> to view the Login Page or <b>Exit</b> to Close the window.
										</font>
										<br><br>
									</td>
								</tr>
								<tr>
									<td align="center">
										<input type="button" id="btnRetry" value="Retry" class="Button" name="btnRetry" onclick="btnRetryOnClick()">
										<input type="button" id="btnClose" value="Exit" class="Button" name="btnClose" onclick="btnCloseOnClick(1)">
									</td>
								</tr>
							</table>
			 			</td>
			 			<td style="background-image:url(../images/AA012_no_cache.jpg);"></td>	
			 		</tr>
			 		<tr>
			 			<td height="20"><img src="../images/AA007_no_cache.gif"></td>
			 			<td height="20" style="background-image:url(../images/AA008_no_cache.jpg);"></td>
			 			<td height="20"><img src="../images/AA009_no_cache.gif"></td>
					</tr>	
				</table>
			</td>
		</tr>
		<tr>
			<td style="height:50"><font>&nbsp;</font></td>
		</tr>
		<tr>
			<td>
				<font style="color:#E4E4E4">&nbsp;Powered by <b>ISA</b></font>
			</td>
		</tr>
	</table>
 	
	<script type="text/javascript">
	<!--
	function btnRetryOnClick(){
		top.location.replace('../private/showXBEMain.action');
	}
	
	function btnCloseOnClick(){
		top.close();	
	}
	
	function onLoad(){
		document.getElementById("btnRetry").focus();
	}
	
	function CloseWindow(){
		//window.close();
	}
	
	onLoad();
	//-->
	</script>
</body>	
</html>
			
