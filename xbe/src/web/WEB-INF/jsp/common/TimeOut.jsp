<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
    <meta HTTP-EQUIV="REFRESH" CONTENT="180; URL=showTimeOutCloseME"> 
    <link rel='stylesheet' type='text/css' href='../css/Style_no_cache.css'>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>   
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>       
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>		
	<script src="../js/common/selfFocus.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>   	
  </head>
 <body onkeydown='return Body_onKeyDown(event)' class='PageBackGround' ondrag='return false' scroll="no" oncontextmenu="return showContextMenu()">
 	<br>
 	<table width='530' align='center' border='0' cellpadding='0' cellspacing='0'>
 		<tr>
 			<td height="20" width="15"><img src="../images/AA004_no_cache.gif"></td>
 			<td height="20" style="background-image:url(../images/AA005_no_cache.jpg);"></td>
 			<td height="20" width="15"><img src="../images/AA006_no_cache.gif"></td>
		</tr>
		<tr>
 			<td style="background-image:url(../images/AA010_no_cache.jpg);"></td>
 			<td style="background-image:url(../images/AA011_no_cache.jpg);">
				<table width='500' align='center' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td align="center">
							<font>System has been inactive for <span id="spnMinutes"></span> minutes. You will be automatically logged off after 3 minutes. Please confirm whether you want to retain this session.<br><br>
							Click <b>OK</b> to Close. <b>Cancel</b> to retain.
							</font>
							<br><br>
						</td>
					</tr>
					<tr>
						<td align="center">
							<input type="button" id="btnOK" value="OK" class="Button" NAME="btnContinue" onclick="btnContinueOnClick(0)">
							<input type="button" id="btnCancel" value="Cancel" class="Button" NAME="btnContinue" onclick="btnContinueOnClick(1)">
						</td>
					</tr>
				</table>
 			</td>
 			<td style="background-image:url(../images/AA012_no_cache.jpg);"></td>	
 		</tr>
 		<tr>
 			<td height="20"><img src="../images/AA007_no_cache.gif"></td>
 			<td height="20" style="background-image:url(../images/AA008_no_cache.jpg);"></td>
 			<td height="20"><img src="../images/AA009_no_cache.gif"></td>
		</tr>	
	</table>
	<script type="text/javascript">
	<!--
	function btnContinueOnClick(strID){
		switch (strID){
			case 0 :
				location.replace("showTimeOutCloseME")		
				break;
			case 1 :
				opener.top.remoteSubmit() ;
				window.close()
				break;
		}
	}
	
	function onLoad(){
		DivWrite("spnMinutes", Math.round((parseFloat(opener.top.intTimerMiliSeconds) / (60 * 1000))) + " ")
		getFieldByID("btnOK").focus();
	}
	
	function CloseWindow(){
		//window.close();
	}
	
	onLoad();
	//-->
	</script>
</body>	
</html>
			
