<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
    <meta HTTP-EQUIV="REFRESH" CONTENT="60; URL=showTimeOutCloseME"> 
    <link rel='stylesheet' type='text/css' href='../css/Style_no_cache.css'>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>   
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>       
	<script src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type='text/javascript'></script>   	
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>		
  </head>
 <body onkeydown='return Body_onKeyDown(event)' class='PageBackGround' ondrag='return false' scroll="yes" oncontextmenu="return showContextMenu()">
	<table width='670' align='center' border='0' cellpadding='0' cellspacing='0'>
 		<tr>
 			<td align="center">
 				<font>Page Timed Out<br>
 				</font>
			</td>
		</tr>
		<tr>
			<td align="center">
				<input type="button" id="btnOK" value="Login Again" style="width:auto;" class="Button" NAME="btnContinue" onclick="btnContinueOnClick(0)">
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	<!--
	function btnContinueOnClick(strID){
		switch (strID){
			case 0 :
				window.open('private/showXBEMain.action', 'winCC', 'fullscreen=yes, scrollbars=no');
				break;
		}
	}
	getFieldByID("btnOK").focus();
	//-->
	</script>
</body>	
</html>
			
