<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<%@ include file='../common/inc_ShortCuts.jsp'%>	

	<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<style>
		.HeaderButton{
			display : none;
		}
	</style>
	
	<script type="text/javascript">
		var agentWiseRouteSelection = eval('(' +'<c:out value="${requestScope.agentWiseRouteSelection}" escapeXml="false" />' + ')');
		var viewAvailableCredit =  eval('(' +'<c:out value="${requestScope.viewAvailableCredit}" escapeXml="false" />' + ')');
	</script>
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/search/searchFlightShedules.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper">
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td class='ui-widget'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td class='rowHeight ui-widget' style='height:40px'>
									<p class='txtBold txtMedium'>Search Flight Schedules </p>
								</td>
							</tr>
							<tr>
								<td class="ui-widget ui-widget-content ui-corner-all">
									<form method='post' action="searchFlightSchedule.action" id="frmSrchFlightSchedules">
									<table width='100%' border='0' cellpadding='1' cellspacing='0'>
										<tr>
											<td class='paneHD'>
												<p class='txtWhite txtBold'>Search</p>
											</td>
										</tr>
										<tr>
											<td>
												<table width='99%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td> Return : </td>
														<td>
															<input type='checkbox' id='chkreturn' name='searchFlightScheduleTO.roundTrip' class='noBorder' tabindex="1"/>
														</td>												   
													</tr>
													
													<tr>
														<td width='5%'> From : </td>
														<td width='17%'>
															<input type="text" id="fAirport" class="aa-input" name="searchFlightScheduleTO.fromAirport" style="width:150px"  tabindex="1"/>															
														</td>
														<td width='1%'><%@ include file='../common/inc_MandatoryField.jsp' %> </td>
														<td width='5%' align="center">To : </td>
														<td width='17%'>
															<input type="text" id="tAirport" class="aa-input" name="searchFlightScheduleTO.toAirport" style="width:150px"  tabindex="2" />															
														</td>
														<td width='1%'><%@ include file='../common/inc_MandatoryField.jsp' %> </td>
														<td width='10%' align="center">From Date : </td>
														<td width='12%'>
															<input id="departureDate" name="searchFlightScheduleTO.fromDate" type="text" class="aa-input" style="width:80px" maxlength="10" tabindex="3"/><%@ include file='../common/inc_MandatoryField.jsp' %>
														</td>
														<td width='10%' align="center">To Date : </td>
														<td width='12%'>
															<input id="returnDate" name="searchFlightScheduleTO.toDate" type="text" class="aa-input" style="width:80px" maxlength="10" tabindex="3"/><%@ include file='../common/inc_MandatoryField.jsp' %>
														</td>
														<td width='8%' align="center">
															<button id="btnSearch" type="button" class="btnMargin" tabindex="6">Search</button>
														</td>														
													</tr>
													<tr>
														<td class='singleGap'></td>																		   
													</tr>
												</table>
											</td>
										</tr>									
									</table>
									</form>
								</td>
							</tr>
							<tr>
								<td class='singleGap'>
								</td>
							</tr>
							<tr>
								<td class='singleGap'>
								</td>
							</tr>
							<tr>							
								<td valign='top' style='height:480px;'>
								
									<div id='divSchResults'  class="search-shedule">
									
											<div id='divOutboundSchResults'  class="search-shedule" style="margin-top:10px;margin-bottom:10px;height:225px;overflow-x: hidden">
																						
												<table width='100%' border='0' cellpadding='1' cellspacing='0' id="outBoundSchTable">
												
													<!--<tr id="outBoundSchRow">
														<td class="ui-widget ui-widget-content ui-corner-all">
														<table width='100%' border='0' cellpadding='1' cellspacing='0'>
															<tr>
																<td class='paneHD'>
																	<span class='txtWhite txtBold' id="outBoundHeader"></span>
																</td>
															</tr>
															<tr>
																<td >
																	<div id="fltOutWrapper" style="height:225px;overflow-y: auto;overflow-x: hidden"> 
																	 place to hold data grids 
										                     		</div>
																</td>
															</tr>												
														</table>													
														</td>
													</tr>
																											
												--></table>
																								
											</div>
											
											<div id='divInboundSchResults'  class="search-shedule" style="margin-top:10px;margin-bottom:10px;height:225px;overflow-y: auto;overflow-x: hidden">
												<table width='100%' border='0' cellpadding='1' cellspacing='0' id="inBoundSchTable">
												
													<!--<tr id="outBoundSchRow">
														<td class="ui-widget ui-widget-content ui-corner-all">
														<table width='100%' border='0' cellpadding='1' cellspacing='0'>
															<tr>
																<td class='paneHD'>
																	<span class='txtWhite txtBold' id="outBoundHeader"></span>
																</td>
															</tr>
															<tr>
																<td >
																	<div id="fltOutWrapper" style="height:225px;overflow-y: auto;overflow-x: hidden"> 
																	 place to hold data grids 
										                     		</div>
																</td>
															</tr>												
														</table>													
														</td>
													</tr>														
												--></table>
											</div>																	
									</div>									
								</td>							
								
							</tr>
							<tr>
								<td class='singleGap'></td>
							</tr>
							<tr>
								<td align='right'>
									<button id="btnReset" type="button" class="btnMargin">Reset</button><button id="btnClose" type="button" class="btnMargin">Close</button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
	<%@ include file='../common/inc_Credit.jsp' %>  
	  
</body>
</html>