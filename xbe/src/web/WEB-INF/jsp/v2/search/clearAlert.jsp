<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');		
	//-->
	</script>
<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/search/clearAlert.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapperPopUp" style="display:block;width:650px;height:300px">
		<table style='width:650px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:650px;">
						<form method='post' action="" id="frmClearAlert">
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td colspan="2">
										<label class='txtLarge txtBold'>Action Alert</label>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
									<td>
									   <div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 5px; margin-left:0px;margin-right:10px;">	  
										<table width='90%' border='0' cellpadding='0' cellspacing='0' style="margin:auto;margin-top: 5px;margin-bottom: 5px;">
											<tr>
											<td class="ui-widget ui-widget-content ui-corner-all" style="height: 220px;margin-top: 5px" valign="top">
												<div id="clearAlertPannel">
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td class='paneHD'>						    
																<p class='txtWhite txtBold'  style="padding-left:3px;">Details</p>
															</td>
														</tr>
														<tr>
															<td  class='singleGap'></td>
														</tr>
														<tr>
															<td>
																<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																	<tr>
																		<td align="right">Action Performed :</td>
																		<td align="left">
																			<select id="selAction" size="1" style="width:200" class="aa-input">
																				<c:out value="${requestScope.ClearAlertActions}" escapeXml="false" />											
																		    </select>										
																			<input type="checkbox" class="aa-input" name="chkTrnsfr" id="chkTrnsfr" checked="checked"/>
																		</td>
																	</tr>
																   <tr>							   	 
																		<td align="right">
																			Additional Information :
																		</td>
																		<td align="left">
																			<textarea id="txtaComment"  class="aa-input" cols="50" rows="5"></textarea>
																		</td>									
																	</tr>
																</table>
														    </td>
													 </tr>
												  </table>
												</div>
												<div id="transferSegmentPannel" style="display: none">
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td class='paneHD'>						    
																<p class='txtWhite txtBold'  style="padding-left:3px;">Transfer Connection/Alert Segment Details</p>
															</td>
														</tr>
														<tr>
															<td  class='singleGap'></td>
														</tr>
														<tr>
															<td  class='singleGap'></td>
														</tr>
														<tr>
															<td>
																<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																	<tr>
																		<td align="right">Transfer Performed :</td>
																		<td align="left">
																			<select id="selTransfer" size="1" style="width:200" class="aa-input">
																				<option value="">--Select--</option>
																				<option value="tranConn">Connection Transfer</option>
																				<option value="tranSelAltSeg">Selected Alert Segment Transfer</option>								
																		    </select>																			
																		</td>
																	</tr>																   
																</table>
														    </td>
													 </tr>
												  </table>
												</div>
											</td>
											<td></td>
										</tr>
										<tr>
											<td align="right" colspan="2" style="padding-top:5px">
												<u:hasPrivilege  privilegeId="xbe.res.alt.alert">
													<input type="button" id="btnConfirm" value="Clear Alert" class="btnMargin btnMedium"/>
												</u:hasPrivilege>
												<u:hasPrivilege  privilegeId="xbe.res.alt.seg.tnx">
													<input type="button" id="btnTrnsfrConfirm" value="Confirm" class="btnMargin btnMedium"/>
												</u:hasPrivilege>
												<u:hasPrivilege  privilegeId="xbe.res.alt.seg.tnx">
													<input type="button" id="btnTrnsfr" value="Transfer" class="btnMargin btnMedium"/>
												</u:hasPrivilege>
												<input type="button" id="btnExit" value="Cancel" class="btnMargin btnMedium"/>
											</td>
										</tr>
									  </table>
									 </div>	
									</td>
								</tr>								
								
							</table>
						</form>
					</div>
				</td>
			</tr> 
		
		</table>
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
</body>
</html>