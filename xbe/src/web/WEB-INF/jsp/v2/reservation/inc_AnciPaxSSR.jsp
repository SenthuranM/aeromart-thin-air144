						<span id='spnAnciPaxWise_0' style='position:relative ;display:none;height:230px;width:385px;overflow:auto;'>
								<table width='100%' border='0' cellpadding='1' cellspacing='0'>
									<thead>
										<tr>
											<td colspan='4' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight'>
												<b i18n_key="ancillary_Paxwisebreakdownfor">Pax wise breakdown for <span id='spnSSRPaxSeg'></span></b>
											</td>
										</tr>
										<tr>
											<td width='50%' align='center' class='thinBorderB thinBorderR gridBG  thinBorderT ui-corner-TL rowHeight txtBold' i18n_key="ancillary_PassengerName">Passenger Name</td>
											<td width='28%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold' i18n_key="ancillary_SSRInfo">SSR Info</td>
											<td width='15%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold'i18n_key="ancillary_Amt">Amt</td>
											<td width='7%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR rowHeight txtBold'>&nbsp;</td>
										</tr>
									</thead>
									<tbody>
										<tr><td colspan='4'>
											<div style="height: 200px;overflow: auto;">
												<table width='99%' border='0' cellpadding='1' cellspacing='0'>
													<thead>
														<tr>
															<td width='51%' style="height: 1px"></td>
															<td width='28%' style="height: 1px"></td>
															<td width='15%' style="height: 1px"></td>
															<td width='7%' style="height: 1px"></td>
														</tr>
													</thead>
													<tbody id="tbdyAnciSSRPax" ></tbody>
												</table>
											</div>
											</td>
										</tr>
									</tbody>
								</table>
							</span>