<span id='spnAnciContainer_8' style='display:none;'>
	<table width="100%" border="0" cellpadding="1" cellspacing="0">
		<tbody>
		   <tr>
			  <td class="paneHD">
				 <table>
				    <tbody>
				       <tr>
				          <td>
				             <p class="txtWhite txtBold" i18n_key="passenger_PassengerInformation" id ="spnAutoCheckinPassenger"></p>
				          </td>
				          <td>
				             <div id="divPaxHD" class="txtWhite txtBold"></div>
				          </td>
				       </tr>
				    </tbody>
				 </table>
			  </td>
		   </tr>
		   <tr>
			  <td>
				<table id="tblAnciACI" class="scroll" cellpadding="0" cellspacing="0"></table>
			 </td>
		  </tr>
		  <tr>
			  <td>
				<table id="tblAnciACIInfant" class="scroll" cellpadding="0" cellspacing="0"></table>
			 </td>
		  </tr>
		<tr>
			<td class='doubleGap' colspan='2'>
			</td>
		</tr>
		</tbody>
	</table>
</span>