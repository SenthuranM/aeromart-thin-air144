							<span id='spnAnciContainer_2' style='display:none;'>
								<table id='tblSeatMap' width='100%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td  align='center' valign="middle" >
										<div style='height:220px;overflow: hidden;' >
											<table border='0' celpadding='0' cellspacing='0' class='autoCrop'>
												<tr>
													<td class='wingRight' valign='bottom' align='center'>
														<table border='0' cellpadding='0' cellspacing='0'>
															<tbody id='tbdyWingRight'></tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td align='center'>
														<table border='0' cellpadding='0' cellspacing='0'>
															<tbody id='tbdySeatMap'></tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td class='wingLeft' valign='top' align='center'>
														<table border='0' cellpadding='0' cellspacing='0'>
															<tbody id='tbdyWingLeft'></tbody>
														</table>
													</td>
													
												</tr>
											</table>
										</div>
										</td>
									</tr>
									<tr>
										<td>
											<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												<tr>	
													<td>
														<table border='0' cellpadding='1' cellspacing='0'>
															<tr>
																<!-- <td class='notAvailSeat'></td>
																<td>Occupied Seat</td>
																<td>&nbsp;</td> -->
																<td class='matchCC availSeat'></td>
																<td i18n_key="ancillary_AvailableSeat">Available Seat</td>
																<td>&nbsp;</td>
																<td class='matchCC notAvailSeat'></td>
																<td i18n_key="ancillary_OccupiedSeat">Occupied Seat</td>
																<td>&nbsp;</td>
																<td class='matchCC SltdSeat'></td>
																<td i18n_key="ancillary_SelectedSeat">Selected Seat</td>
																<td>&nbsp;</td>
																
																<td ><div style="padding-left:250px;">
<button style="width: 160px;" paddingLeft = "350px" class="btnMargin btnMedium ui-state-default ui-corner-all" type="button" id="btnAnciResetSeat" i18n_key="ancillary_SelectedSeat">Reset Seat Selection</button></div></td>
															</tr>
														</table>
													</td>
													<td align='right'></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<span id='spnAnciNoSeat' i18n_key="btn_SeatMapNotAvailable">Seat Map Not available</span>
							</span>