<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/reservation/displayTaxesSurcharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

</head>
<body class="legacyBody">
	<div id="divBreakDownPane">
		<table style='width:700px;' border='0' cellpadding='0' cellspacing='0'>
			<tr class='ui-widget ui-widget-content ui-corner-all'>
				<td>
					<div id="divTaxBD">
						<table id="tblTaxBreakDown" class="scroll" cellpadding="0" cellspacing="0"></table>
					</div>
				</td>
				<td>
					<div id="divSurBD">
						<table id="tblSurBreakDown" class="scroll" cellpadding="0" cellspacing="0"></table>
					</div>
				</td>
			</tr>
			<tr>
				<td align='right' colspan="2"><button id="btnClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button></td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='inc_pageError.jsp' %>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>

<script type="text/javascript">
$("#divBreakDownPane").getLanguageForOpener();

</script>