	<div id='divAnciGSTConfirm' style='position:absolute;top:300px;left:350px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key="ancillary_AncillaryGSTConfirm"> &nbsp;Total Service Tax</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>	
						<tr >
							<td align="center" class='popBGColor'>
							<table width='95%' border='0' cellpadding='1' cellspacing='0'>
							<tr>
								<td class='popBGColor' align="left"><label id='lblAnciGSTCtdConfirm'>Total service tax amount</label></td>
								<td class='popBGColor' align="left"><span id="spnGSTAnciModifyMsg"></span></td>
							</tr>					
							<tr>
								<td class='popBGColor txtBold' align="left"><span id="spnGSTAnciMsg"></span></td>
							</tr>
							<tr>
								<td class='popBGColor singleGap'></td>
							</tr>
							<tr>
								<td class='popBGColor singleGap'></td>
							</tr>						
							<tr>
								<td class='popBGColor rowHeight' align='right'>
									<button id="btnAnciGSTContOk" type="button" class="btnMargin" i18n_key="btn_Yes">OK</button>
								</td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>