<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<table width='100%' border='0' cellpadding='0' cellspacing='0' id='tabAnciTbl'>
		<tr>
			<td colspan='2'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
								<tr>
									<td class='lightHD' style='height:50px;' align='center'>
										<table width='98%' border='0' cellpadding='1' cellspacing='0' align='center' id='anciHeaderTbl'>
											<tr>
												<td width='17%' align='left'>
													<span id='divAnciHD' class='txtBold txtMedium txtWhite' style="line-height: 45px;" i18n_key="ancillary_SpecialServiceRequirementNeed">SSR Needs</span>
												</td>
												<td width='80%' align='right'>
													<div style="float:right;">
														<div id='imgTab_2' class="anci_button_wrapper">
															<span i18n_key="ancillary_Seats" class="seat">Seat</span>
															<img i18n_key_title ="ancillary_SeatButtonMsg"  src='../themes/default/images/accelaero/LCAnsi02_A_no_cache.jpg' border='0' title='Click to select Seats' alt='Seat' class='cursorPointer'>
														</div>

														<div id='imgTab_8' class="anci_button_wrapper">
															<span i18n_key="ancillary_AutoCheckin" class="flexi">Automatic Checkin</span>
															<img  i18n_key_title ="ancillary_AutoCheckinRequestButtonMsg" src='../themes/default/images/accelaero/LCAnsi07_A_no_cache.jpg' border='0' title='Click to select Flexi Fare Services' alt='Flexi Fare' class='cursorPointer' > 
														</div>
														
														<div id='imgTab_1' class="anci_button_wrapper">
															<span i18n_key="ancillary_Meals" class="meal">Meal</span>
															<img i18n_key_title ="ancillary_MealButtonMsg"  src='../themes/default/images/accelaero/LCAnsi01_A_no_cache.jpg' border='0' title='Click to select Meals' alt='Meal' class='cursorPointer' >
														</div>
														
														<div id='imgTab_5' class="anci_button_wrapper">
															<span i18n_key="ancillary_Baggage" class="baggage">Baggage</span>
															<img i18n_key_title ="ancillary_BaggageButtonMsg"  src='../themes/default/images/accelaero/LCAnsibag2_no_cache.jpg' border='0' title='Click to select Baggages' alt='Baggage' class='cursorPointer' >
														</div>
														
														<div id='imgTab_4' class="anci_button_wrapper">
															<span i18n_key="ancillary_Insurance" class="insurance">Insurance</span>
															<img  i18n_key_title ="ancillary_TravelInsuranceButtonMsg"  src='../themes/default/images/accelaero/LCAnsi04_A_no_cache.jpg' border='0' title='Click to select Insurance' alt='Insurance' class='cursorPointer' >
														</div>
														
														<div id='imgTab_0' class="anci_button_wrapper">
															<span i18n_key="ancillary_SSR" class="ssr">SSR</span>
															<img  i18n_key_title ="ancillary_SSRNeedsButtonMsg" src='../themes/default/images/accelaero/LCAnsi00_A_no_cache.jpg' border='0' title='Click to select Special Services' alt='SSR' class='cursorPointer'> 
														</div>
														
														<div id='imgTab_3' class="anci_button_wrapper">
															<span i18n_key="ancillary_AirportService" class="hala">Airport Service</span>
															<img  i18n_key_title ="ancillary_AirportServicesButtonMsg" src='../themes/default/images/accelaero/LCAnsi03_A_no_cache.jpg' border='0' title='Click to select Hala Services' alt='Hala' class='cursorPointer' > 
														</div>
														
														<div id='imgTab_6' class="anci_button_wrapper">
															<span i18n_key="ancillary_AirportTransfer" class="airportTransfer">Airport Transfer</span>
															<img  i18n_key_title ="ancillary_AirportTransferButtonMsg" src='../themes/default/images/accelaero/LCAnsi06_A_no_cache.jpg' border='0' title='Click to select Airport Transfers' alt='AirportTransfer' class='cursorPointer' >
														</div>
														
														<div id='imgTab_7' class="anci_button_wrapper">
															<span i18n_key="ancillary_Flexi" class="flexi">Flexi</span>
															<img  i18n_key_title ="ancillary_FlexiRequestButtonMsg" src='../themes/default/images/accelaero/LCAnsi07_A_no_cache.jpg' border='0' title='Click to select Flexi Fare Services' alt='Flexi Fare' class='cursorPointer' > 
														</div>
														
														
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>		
			</td>
		</tr>
		<tr>
			<td  class='singleGap' colspan='2'></td>
		</tr>
		<tr>
			<td width='15%' id='tdAnciSeg' class="ui-widget ui-widget-content ui-corner-all" style='height:500px;' valign='top'>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<thead>
						<tr>
							<td class='thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight' align='center' i18n_key="ancillary_Segment">
								<b><span id="spanFltSegTitle">Segments</span></b>
							</td>
						</tr>
					</thead>
					<tbody><tr><td>
						<div style="height: 495px;overflow-y:auto;overflow-x: hidden; "><table style="width: 100%;border-collapse:collapse;">
							<tbody id="tbdyAnciSeg"></tbody>
						</table></div>
					</td></tr>
					</tbody>
				</table>
			</td>
			<td width='85%' valign='top'>
				<table width='100%' border='0' cellpadding='0' cellspacing='1'>
					<tr>
						<td colspan='2'  class="ui-widget ui-widget-content ui-corner-all" style='height:250px;' valign='top'>
							<!-- SSR -->
							<%@ include file='inc_AnciContentSSR.jsp' %>
							
							<!-- Meal Request -->
							<%@ include file='inc_AnciContentMeal.jsp' %>

							<!-- Baggage Request -->
							<%@ include file='inc_AnciContentBaggage.jsp' %>
							
							<!-- Seat map -->
							<%@ include file='inc_AnciContentSeat.jsp' %>
							
							<!-- HALA -->
							<%@ include file='inc_AnciContentAPService.jsp' %>
							
							<!-- Insurance -->
							<%@ include file='inc_AnciContentIns.jsp' %>
							
							<!-- AIRPORT TRANSFER -->
							<%@ include file='inc_AnciContentAPTransfer.jsp' %>
							
							<!-- Flexi -->
							<%@ include file='inc_AnciContentFlexi.jsp' %>
							
							<!-- Automatic Checkin -->
							<%@ include file='inc_AnciContentAutoCheckin.jsp' %>
							
							
						</td>
					</tr>
					<tr>
						<td  class='singleGap' colspan='2'></td>
					</tr>
					<tr>
						<td width='50%' class="ui-widget ui-widget-content ui-corner-all" style='height:230px;' valign='top'>
							<!-- SSR Pax Details -->
							<%@ include file='inc_AnciPaxSSR.jsp' %>
							
							<!-- Meal Request Pax Details -->
							<%@ include file='inc_AnciPaxMeal.jsp' %>
							
							<!-- Baggage Request Pax Details -->
							<%@ include file='inc_AnciPaxBaggage.jsp' %>

							<!-- Seat Map Pax Details -->
							<%@ include file='inc_AnciPaxSeat.jsp' %>
							
							<!-- HALA Pax Details -->
							<%@ include file='inc_AnciPaxAPService.jsp' %>
							
							<!-- Insurance Pax Details -->
							<%@ include file='inc_AnciPaxIns.jsp' %>
							
							<!-- Airport Transfer Details -->
							<%@ include file='inc_AnciPaxAPTransfer.jsp' %>
							
							<!-- Flexi Pax Details -->
							<%@ include file='inc_AnciPaxFlexi.jsp' %>
							
							<!-- Automatic Checkin -->
							<%@ include file='inc_AnciPaxAutoCheckin.jsp' %>
						
						</td>
						<td width='50%' class="ui-widget ui-widget-content ui-corner-all" valign='top' id="tblPriceSummaryContainer" style="width:385px;min-width: 385px;">
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight'>
										<table border='0' cellpadding='1' cellspacing='0' align='center' width='95%'>
											<tr>	
												<td width="40%" class="txtBold" i18n_key="ancillary_PriceBreakdown">Price Breakdown</td>
												<td width="10"></td>
												<td width="25%" align="right" class="txtBold"><span id='spnSelCur' ></span></td>
												<td width="25%" align="right" class="txtBold"><span id='spnBaseCur' ></span></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align='center'>
										<table width='95%' border='0' cellpadding='1' cellspacing='0'>
											<tbody id="tbdyAnciPriceSum"></tbody>	
										</table>
									</td>
								</tr>
								<tr>
									<td class='paneHD' align='center'>
										<table border='0' cellpadding='1' cellspacing='0' align='center' width='95%'>
											<tr>	
												<td width="40%" style='height:25px;' align="left"><div class='txtWhite txtBold' i18n_key="ancillary_Total">T O T A L </div></td>
												<td width="10"></td>
												<td width="25%" align="right"><div id='divAnciTotalinSelCur' class='txtWhite txtBold'></div></td>
												<td width="25%" align="right"><div id='divAnciTotal' class='txtWhite txtBold'></div></td>
											</tr>
											
										</table>
									</td>
								</tr>
								<tr>
									<td align='center'>
										<table width='95%' border='0' cellpadding='1' cellspacing='0'>
											<tbody id="tbdyAnciDiscount"></tbody>	
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>					
				</table>
			</td>
		</tr>
		<tr>
			<td colspan='2' align='right'>
				<button id="btnAnciContinue" type="button" class="btnMargin btnMedium" i18n_key="btn_Continue">Continue</button> <button id="btnAnciView" type="button" class="btnMargin btnMedium" i18n_key="btn_ViewSummary">View Summary</button><button id="btnAnciCancel" type="button" class="btnMargin" i18n_key="btn_Cancel">Cancel</button>
			</td>
		</tr>
	</table>
	<span id='spnSeatInfo' style='position:absolute;background-color:#FF2200;width:90px;height:40px; display:none;' class='seatTT ui-corner-all'>
		<table width='100%' border='0' cellpadding='0' cellspacing='1'>
			<tr>
				<td class='txtBold txtWhite' align='center'><span id='spnTTSeatNo'></span></td>
			</tr>
			<tr>
				<td class='txtBold txtWhite' align='center'><span id='spnTTSeatChg'></span></td>
			</tr>
		</table>
	</span>

<table width='100%' border='0' cellpadding='1' cellspacing='0' id="tblAnciPriceBreakDown" style='display:none;'>
	<tr>
		<td
			class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight'>
		<table border='0' cellpadding='1' cellspacing='0' align='center'
			width='95%'>
			<tr>
				<td width="40%" class="txtBold" i18n_key="ancillary_PriceBreakdown">Price Breakdown</td>
				<td width="10"></td>
				<td width="25%" align="right" class="txtBold" i18n_key="ancillary_Old">Old</td>
				<td width="25%" align="right" class="txtBold" i18n_key="ancillary_New">New</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align='center'>
		<table width='95%' border='0' cellpadding='1' cellspacing='0'>
			<tbody id="tbdyAnciPriceBreakDown">
			<tr id='trPriceBreakDownTmpl'>
				<td width="40%" class="txtBold" height="10" name="name"></td>
				<td width="10"></td>
				<td width="25%" align="right" class="txtBold" name="old"></td>
				<td width="25%" align="right" class="txtBold" name="new"></td>
			</tr>
			</tbody>
		</table>
		</td>
	</tr>
	<tr>
		<td class='paneHD' align='center'>
		<table border='0' cellpadding='1' cellspacing='0' align='center'
			width='95%'>
			<tr>
				<td width="40%" style='height: 25px;' align="left">
				<div class='txtWhite txtBold' i18n_key="ancillary_BALANCE">B A L A N C E</div>
				</td>
				<td width="10"></td>
				<td width="25%" align="right">
				</td>
				<td width="25%" align="right">
				<div id='divAnciBalance' class='txtWhite txtBold'></div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<!-- Ancillary summary-->
	<%@ include file='inc_AnciSummary.jsp' %>
	<%@ include file='inc_AnciContConfirm.jsp' %>
	<%@ include file='inc_AnciGSTConfirm.jsp' %>
	<%@ include file='inc_FreeAnciNotification.jsp' %>
	
<script type="text/javascript">

	$("#anciHeaderTbl").getLanguageForTitle();
	$("#tabAnciTbl").getLanguage();
	$("#divAnciSummary").getLanguage();
	$("#divAnciContConfirm").getLanguage();
	
</script>