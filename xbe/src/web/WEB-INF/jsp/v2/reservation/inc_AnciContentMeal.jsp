							<span id='spnAnciContainer_1' style='display:none;'>
								<table width='100%' border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td align='center' colspan="2">
											<table id="tblAnciMeal" class="scroll" cellpadding="0" cellspacing="0"></table>
										</td>
									</tr>
									<tr>
										<td class='doubleGap' colspan="2">
										</td>
									</tr>
									<tr>
										<td align='left' i18n_key="ancillary_mealDescription">
											Segment wise meals can be selected by clicking segments on left hand side
										</td>
										<td align='right'>
											<button id="btnAnciMealMenu" type="button" class="btnMargin btnMedium" i18n_key="btn_ViewMenu">View Menu</button>
											<button id="btnAnciMealApplyToAll" type="button" class="btnMargin btnMedium" i18n_key="btn_ApplyToAll">Apply To All</button>
											<button id="btnAnciMealApply" type="button" class="btnMargin" i18n_key="btn_Apply">Apply</button>
										<td>
									</tr>
								</table>
							</span>