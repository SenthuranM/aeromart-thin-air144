<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css" />
	<link rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css" />			
 	
  </head>
  <body >
 	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>			
		
		<tr>
			<td>
				<table width='95%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td valign='top' align="center"><span id="spnMeals"></span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<script type="text/javascript">	
		var selMeals = opener.UI_tabAnci.getDisplayMeals();		
		var count = Math.floor(selMeals.length/3);		
		var mMod = selMeals.length%3;
		var strCurrency = "";
		if(opener.UI_tabAnci.getDefaultCurrency() != null) {
			strCurrency = opener.UI_tabAnci.getDefaultCurrency();
		} else {
			strCurrency = opener.DATA_ResPro.selCurrency;
		}
		var mealCount = 0;
		var strNotMessage = 'Image Is Not Available';
		var halaMessage  = "All Meals served is Halal";
		var strHtml = "<table width='80%' border='0' cellpadding='0' cellspacing='0' style='font-size:1em;'>"
		strHtml += "<tr>";
		strHtml += "<td colspan='3' align='left'><font style='color:red;font-size:17;weight:bold'>"+halaMessage;
		strHtml += "</font></td>";
			strHtml += "</tr>";	
		
		
		for(var i=0;i< count +1;i++) {
			if(count == i && mMod == 0) break;
			strHtml += "<tr>";
			strHtml += "<td>&nbsp;</td>";
			strHtml += "</tr>";
			strHtml += "<tr>";
			strHtml += "<td  bgcolor='#53504A' style='width:251'>";
			strHtml += "<table border='0'>";
			strHtml += "<tr>";
			strHtml += "<td><img height='220' width='251'  src='"+selMeals[mealCount]['mealImagePath'] +"' alt='"+strNotMessage+"'/>";
			strHtml += "</td>";
			strHtml += "</tr>";
			strHtml += "</table>";
			strHtml += "</td>";
			if(count != i || (count == i && mMod != 1)) {
				strHtml += "<td width='15%'>&nbsp;</td>";
				strHtml += "<td  bgcolor='#53504A' style='width:251'>";
				strHtml += "<table border='0'>";
				strHtml += "<tr>";
				strHtml += "<td><img height='220' width='251' alt='"+strNotMessage+"' src='"+selMeals[mealCount +1]['mealImagePath'] +"'/>";
				strHtml += "</td>";
				strHtml += "</tr>";
				strHtml += "</table>";
				strHtml += "</td>";
			}
			if(count != i) {
				strHtml += "<td width='15%'>&nbsp;</td>";
				strHtml += "<td  bgcolor='#53504A' style='width:251'>";
				strHtml += "<table border='0'>";
				strHtml += "<tr>";
				strHtml += "<td><img height='220' width='251' alt='"+strNotMessage+"' src='"+selMeals[mealCount +2]['mealImagePath'] +"'/>";
				strHtml += "</td>";
				strHtml += "</tr>";
				strHtml += "</table>";
				strHtml += "</td>";
			}
			
			
			strHtml += "</tr>";
			strHtml += "<tr>";
			strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount]['mealCharge']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount]['mealDescription'];
			strHtml += "</font></td>";
			if(count != i || (count == i && mMod != 1)) {
				strHtml += "<td>&nbsp;</td>";
				strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount +1]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount + 1]['mealCharge']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount + 1]['mealDescription'];
				strHtml += "</font></td>";
			}
			if(count != i) {
				strHtml += "<td>&nbsp;</td>";
				strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount +2]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount + 2]['mealCharge']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount + 2]['mealDescription'];
				strHtml += "</font></td>";
			}
			
			strHtml += "</tr>";
			mealCount = mealCount +3;
			
		}		
		strHtml += "</table>"
		document.getElementById("spnMeals").innerHTML=strHtml;
	</script>		
  </body>
</html>