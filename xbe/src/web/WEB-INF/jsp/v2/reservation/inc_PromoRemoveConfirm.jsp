	<div id='divPromoRemoveConfirm' style='position:absolute;top:300px;left:350px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key="passenger_PromotionRemovalConfirm"> &nbsp;Promotion Removal Confirm</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>	
						<tr >
							<td align="center" class='popBGColor'>
							<table width='95%' border='0' cellpadding='1' cellspacing='0'>
							<tr>
								<td class='popBGColor' align="left"><label id='passenger_PromotionRemovalConfirmOnHold'>Promotions are not applicable for On-hold reservations. Any promotion applied for this reservation will automatically remove if proceed.</label></td>
							</tr>
							<tr>
								<td class='popBGColor singleGap'></td>
							</tr>
							<tr >
								<td class='popBGColor ' align="left" i18n_key="passenger_WouldYouLikeToContinue">Would you like to continue ?</td>
							</tr>	
							<tr>
								<td class='popBGColor singleGap'></td>
							</tr>						
							<tr>
								<td class='popBGColor rowHeight' align='right'>
									<button id="btnPromoRemoveOk" type="button"  class="btnMargin" i18n_key="btn_Yes">Yes </button>
									<button id="btnPromoRemoveNo" type="button"   class="btnMargin" i18n_key="btn_No">No </button>
								</td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>