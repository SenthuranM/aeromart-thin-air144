<span id='spnAnciPaxWise_8' style='position:relative ;display:none;height:230px;width:385px;overflow:auto;'>
	<table width='100%' border='0' cellpadding='1' cellspacing='0'>
		<thead>
			<tr>
				<td colspan='3' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight'>
					<b i18n_key="ancillary_Paxwisebreakdownfor">Pax wise breakdown for <span id='spnACIPaxSeg'></span></b>
				</td>
			</tr>
			<tr>
				<td width='50%' align='center' class='thinBorderB thinBorderR gridBG  thinBorderT ui-corner-TL rowHeight txtBold' i18n_key="ancillary_PassengerName">Passenger Name</td>
				<td width='28%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold' i18n_key="ancillary_SeatPref">Seat Pref</td>
				<td width='22%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold'i18n_key="ancillary_SeatTogether">Seat together</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan='3'>
					<div style="height: 200px;overflow: auto;">
						<table width='99%' border='0' cellpadding='1' cellspacing='0'>
							<thead>
								<tr>
									<td width='51%' style="height: 1px"></td>
									<td width='28%' style="height: 1px"></td>
									<td width='22%' style="height: 1px"></td>
								</tr>
							</thead>
							<tbody id="tbdyAnciACIPax" ></tbody>
						</table>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
   <table width='100%' border='0' cellpadding='1' cellspacing='0'>
		<tbody>
			<tr>
				<td align='right'>
					<button id="btnAnciAutoCheckinReset" type="button" class="btnMargin btnMedium" i18n_key="btn_Reset">reset</button>
					<button id="btnAnciAutoCheckinApply" type="button" class="btnMargin btnMedium" i18n_key="btn_Apply">Apply</button>
				</td>
			</tr>
			<tr>
				<td class='doubleGap' colspan='2'></td>
			</tr>
		</tbody>
	</table>
</span>