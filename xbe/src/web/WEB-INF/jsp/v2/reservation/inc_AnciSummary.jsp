	<div id='divAnciSummary' class="popupXBE ui-corner-all" style="z-index: 11">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD' width="97%">
								<p  class='txtWhite txtBold' style="margin:2px 5px;float: left" i18n_key="ancillary_AncillarySummary">Ancillary Summary</p>
								<div style="float: right;margin:2px 5px">
									<a class="winClose" title="Close Popup" href="javascript:void(0)" id="btnAnciSummaryClose">
										<img width="17" border="0" src="../images/spacer_no_cache.gif" id="imgMMCl">
									</a>
								</div>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap' colspan="2"></td>
						</tr>
						<tr>
							<td class='popBGColor'  align='center' colspan="2">
								<div style="" id="divBodyAnciSummary">
								<div>
									<table width='95%' border='0' cellpadding='0' cellspacing='0'>										
										<tr>
											<td  class='singleGap'></td>
										</tr>
										<tr id='trSummaryAnciSeat'>
											<td class="ui-widget ui-widget-content">
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td class='rowHeight ui-widget' valign='top'> 
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tr>
																	<td class='paneHD'>
																		<table>
																			<tr>
																				<td>
																					<p class='txtWhite txtBold' i18n_key="ancillary_SeatSelectionDetails">Seat Selection Details</p>
																				</td>
																				<td>
																					<div class='txtWhite txtBold'></div>
																				</td>
																			</tr>
																		</table>													
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='max-height:30px;' valign='top'>
															<table id="tblSummarySeats" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>								
												</table>
											</td>
										</tr>
										<tr>
											<td  class='singleGap'></td>
										</tr>					
										<tr id='trSummaryAnciMeal'>
											<td class="ui-widget ui-widget-content" valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<table>
																<tr>
																	<td>
																		<p class='txtWhite txtBold' i18n_key="ancillary_MealSelectionDetails">Meals Selection Details</p>
																	</td>
																	<td>
																		<div class='txtWhite txtBold'></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='max-height:30px;' valign='top'>
															<table id="tblSummaryMeals" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>	
												</table>
											</td>
										</tr>
										<tr>
											<td  class='singleGap'></td>
										</tr>					
										<tr id='trSummaryAnciBaggage'>
											<td class="ui-widget ui-widget-content" valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<table>
																<tr>
																	<td>
																		<p class='txtWhite txtBold' i18n_key="ancillary_BaggageSelectionDetails">Baggage Selection Details</p>
																	</td>
																	<td>
																		<div class='txtWhite txtBold'></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='max-height:30px;' valign='top'>
															<table id="tblSummaryBaggages" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>	
												</table>
											</td>
										</tr>
										<tr>
											<td  class='singleGap'></td>
										</tr>
										<tr id='trSummaryAnciInsurance'>
											<td class="ui-widget ui-widget-content"  valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<table>
																<tr>
																	<td>
																		<p class='txtWhite txtBold' i18n_key="ancillary_Insurance">Insurance</p>
																	</td>
																	<td>
																		<div class='txtWhite txtBold'></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='max-height:15px;' valign='top'>
															<table id="tblSummaryInsurance" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>	
												</table>
											</td>
										</tr>					
										<tr>
											<td  class='singleGap'></td>
										</tr>
										<tr id='trSummaryAnciSSR'>
											<td class="ui-widget ui-widget-content" valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<table>
																<tr>
																	<td>
																		<p class='txtWhite txtBold' i18n_key="ancillary_SpecialServiceRequests">Special Service Requests</p>
																	</td>
																	<td>
																		<div class='txtWhite txtBold'></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='max-height:30px;' valign='top'>
															<table id="tblSummarySSR" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>	
												</table>
											</td>
										</tr>
										<tr>
											<td class='singleGap'></td>
										</tr>	
										<tr id='trSummaryAnciAPService'>
											<td class="ui-widget ui-widget-content" valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<table>
																<tr>
																	<td>
																		<p class='txtWhite txtBold' i18n_key="ancillary_AirportServices">Airport Services</p>
																	</td>
																	<td>
																		<div class='txtWhite txtBold'></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='max-height:30px;' valign='top'>
															<table id="tblSummaryAPService" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>	
												</table>
											</td>
										</tr>
										<tr>
											<td class='singleGap'></td>
										</tr>	
										<tr id='trSummaryAnciAPTransfer'>
											<td class="ui-widget ui-widget-content" valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<table>
																<tr>
																	<td>
																		<p class='txtWhite txtBold' i18n_key="ancillary_AirportTransfer">Airport Transfer</p>
																	</td>
																	<td>
																		<div class='txtWhite txtBold'></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='max-height:30px;' valign='top'>
															<table id="tblSummaryAPTransfer" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>	
												</table>
											</td>
										</tr>
										<tr>
											<td class='singleGap'></td>
										</tr>																	
									</table>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap' colspan="2"></td>
						</tr>
						
						<!--<tr>
							<td class='popBGColor rowHeight' align='right' colspan="2">
								<button id="btnAnciSummaryClose" type="button" class="btnMargin">Close</button>
							</td>
						</tr>
						--><tr>
							<td class='popBGColor singleGap' colspan="2"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>