
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td style='height: 560px;' valign="top">
			<div id="tdSearchFltArea" style="width:99%">
			<div id="trSearchParams"
				class="ui-widget ui-widget-content ui-corner-all">
				<form method='post' action="minFareCalculation.action"
					id="frmSrchFlt">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'
						align='center'>
						<tr>
							<td colspan='11' class='singleGap'></td>
						</tr>
						<tr>
							<td style="min-width: 35px">From:</td>
							<td style="min-width: 170px">
								<table border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td><input type="text" id="fAirport" class="aa-input"
											style="width: 150px" tabindex="4" />
										</td>
										<td><%@ include file='../common/inc_MandatoryField.jsp'%>
										</td>
									</tr>
								</table>
							</td>
							<td style="min-width: 80px">Departure Date:</td>
							<td style="min-width: 100px">
								<input id="departureDate" name="searchParams.departureDate" type="text" class="aa-input"
								style="width: 80px" maxlength="10" tabindex="6" />
								<%@ include file='../common/inc_MandatoryField.jsp'%>
							</td>
							<td style="min-width: 45px">Adults:</td>
							<td style="min-width: 45px">
								<input id="adultCount" name="searchParams.adultCount" type="text"
								class="aa-input rightAlign" style="width: 22px;" maxlength="3"
								tabindex="8" />
								<%@ include
									file='../common/inc_MandatoryField.jsp'%>
							</td>
							<td style="min-width: 60px">Class:</td>
							<td>
								<select id="classOfService" name="classOfService"
								class="aa-input" style="width: 80px" tabindex="11">
								</select>
								<%@ include
									file='../common/inc_MandatoryField.jsp'%>
							</td>
							<td align="right" valign="bottom" rowspan='4'>
								<button id="btnMinFareSearch" type="button" style="margin: 1px; width: 60px" tabindex="19">
									Search
								</button>
							</td>
						</tr>
						<tr>
							<td>To:</td>
							<td>
								<table border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td><input type="text" id="tAirport" class="aa-input"
											style="width: 150px" tabindex="5" /></td>
										<td><%@ include file='../common/inc_MandatoryField.jsp'%>
										</td>
									</tr>
								</table>
							</td>
							<td style="min-width: 80px">Duration:</td>
							<td style="min-width: 100px">
								<select id="departureVarince" name="departureVarince"
								class="aa-input" style="width: 80px" tabindex="7">
									<option value="7">7 Days</option>
  									<option value="14">14 Days</option>
  									<option value="21">21 Days</option>
 									<option value="One Month">28 Days</option>
								</select>
							</td>
							<td>Children:</td>
							<td><input id="childCount" name="searchParams.childCount"
								type="text" class="aa-input"
								style="width: 22px; text-align: right;" maxlength="3"
								tabindex="9" />
							</td>
							<td>Currency:</td>
							<td><select id="selectedCurrency"
								name="searchParams.selectedCurrency" class="aa-input"
								style="width: 80px" tabindex="12"></select><%@ include
									file='../common/inc_MandatoryField.jsp'%>
							</td>
						</tr>
						<tr>
							<td style="min-width: 35px"></td>
							<td style="min-width: 170px"></td>
							<td style="min-width: 80px"></td>
							<td style="min-width: 100px"></td>
							<td>Infants:</td>
							<td><input id="infantCount" name="searchParams.infantCount"
								type="text" class="aa-input rightAlign" style="width: 22px;"
								maxlength="3" tabindex="10" />
							</td>
							<td>Type:</td>
							<td><select id="bookingType" name="searchParams.bookingType"
								class="aa-input" style="width: 80px" tabindex="13"></select><%@ include
									file='../common/inc_MandatoryField.jsp'%>
							</td>
						</tr>
						<tr>
							<td colspan='11' class='singleGap'></td>
						</tr>
					</table>					
				</form>
			</div>
			</div>
			<div class='singleGap'></div>
			<div class='singleGap'></div>
			<div class='singleGap'></div>
			<div class='singleGap'></div>
			<div class='singleGap'></div>
			<div class='singleGap'></div>
			<div id="tdResultPane" style="height:440px;width:100%;overflow-x:hidden;overflow-y:auto">
				<div id='divResultsPane' style="width:99%">
           		 <div id="minFareCal">
           		 </div>
           		 <div id="tblIBOBFlights">
					<div id='divOutPane'>
					<div class='singleGap'></div>
					<div class='singleGap'></div>
					<div class='singleGap'></div>
					<div>
						<table width="100%" cellpadding="1" cellspacing="0" border="0">
							<tr>
								<td style='width: 630px;' valign='top'>
									<table id="tblOutboundFlights" class="scroll" cellpadding="0"
										cellspacing="0"></table>
								</td>
								<td class='singleGap'></td>
								<!--<td style=' width: 263px;' valign='top'
									id="tdOutBoundLogicalCabins">
								</td>-->
							</tr>
						</table>
					</div>
					</div>
					<div class='singleGap'></div>
				</div>
				<table border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td style="min-width: 750px"></td>
							<td align="right" valign="bottom" rowspan='4'>
								<input name="btnDetailedPrice" type="button" id="btnDetailedPrice" value="View Detailed Price" class="btnLarge button">								
							</td>
						</tr>
				</table>
				<div class='singleGap' style="clear:both"></div>
				<div class='singleGap' style="clear:both"></div>
				<div class='singleGap' style="clear:both"></div>
				<div id="trFareSection" style=''
					class="ui-widget ui-widget-content ui-corner-all">
					<div id='divFarePane'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td class='darkBG' style='height: 20px;'>
								</td>
							</tr>
							<tr>
								<td><div id='divFareQuotePane'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td width='78%' valign='top'>
													<table width='100%' border='0' cellpadding='1'
														cellspacing='0'>
														<tr>
															<td>
																<table width='100%' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td>
																			<button id="btnTaxSurB" type="button"
																				class="btnMargin btnLarge" style="width: 200px;"
																				tabindex="29">Show Taxes and Surcharges</button>
																			<button id="btnFareRulesB" type="button"
																				class="btnMargin btnMedium" tabindex="30">Fare
																				Rules</button>
																		</td>
																		<td align='right'>
																		<button id="btnTogglePBRKDown" type="button"
																				class="btnMargin btnMedium" tabindex="31" style="width:150px;">Price Breakdown</button>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="thinBorderT">
																<table width='100%' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td>
																			<div class="PBRKDown PBRKDownHeader">
																				<table width='100%' border='0' cellpadding='1'
																					cellspacing='0'>
																					<thead class="ui-state-default">
																						<tr>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL'
																								width='15%' style='height: 21px;'></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'><b>Pax Type</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'><b>Fare</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'><b>Tax</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'><b>Sur.</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'><b>Per Pax</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='6%' align='center'><b>No. Pax</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='10%' align='center'><b>Total</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='10%' align='center'><b>Total
																									Price</b></td>
																							<td 
																								class='toHide thinBorderB thinBorderR gridBG thinBorderT'
																								width='7%' align='center'><b>Fare Rule</b></td>
																							<td 
																								class='toHide thinBorderB thinBorderR gridBG thinBorderT'
																								width='7%' align='center'><b>Fare BC</b></td>
																							<td 
																								class='toHide thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR'
																								width='5%' align='center'><b>BC</b></td>
																						</tr>
																					</thead>
																				</table>
																			</div>
																			<div class="PBRKDown PBRKDownBody">
																				<table width='100%' border='0' cellpadding='1'
																					cellspacing='0'>
																					<thead>
																						<tr>
																							<td width='15%' style='height: 0px;'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='6%' align='center'></td>
																							<td width='10%' align='center'></td>
																							<td width='10%' align='center'></td>
																							<td width='7%' align='center' class='toHide'></td>
																							<td width='7%' align='center' class='toHide'></td>
																							<td width='5%' align='center' class='toHide'></td>
																						</tr>
																					</thead>
																					<tbody id="tblFareQuoteBreakDown">
																					</tbody>
																				</table>
																			</div>																			
																			<div id='divFareDetails'></div>
																		</tD>
																	</tr>
																</table>
															</td>
															<td width='0%' valign='top'>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div></td>
							</tr>
						</table>
					</div>
					<div style='text-align: left'>
						<div id='airportMsg' class="spMsg"
							style='padding: 7px; padding-left: 20px; position: relative; width: 893px; z-index: 50; color: red; border: 1px solid red; background: #fff'
							onclick='UI_tabSearchFlights.hideAirportMessage()'></div>
					</div>
				</div>
				
           	</div>
           	</div>
		</td>
	</tr>
	<tr>
		<td align='right'>
			<button id="btnBook" type="button" class="btnMargin" tabindex="27">Book</button>
			<button id="btnReset" type="button" class="btnMargin" tabindex="28">Reset</button>
			<button id="btnCancel" type="button" class="btnMargin" tabindex="29">Close</button>
		</td>
	</tr>
</table>

