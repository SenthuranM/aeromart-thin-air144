							<span id='spnAnciPaxWise_2' style='position:relative ;display:none;height:230px;width:385px;overflow:auto;'>
								<table width='385' border='0' cellpadding='1' cellspacing='0'>
									<thead>
										<tr>
											<td colspan='3' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight'>
												<b i18n_key="ancillary_Paxwisebreakdownfor" >Pax wise breakdown for <span id='spnSeatPaxSeg'></span></b>
											</td>
										</tr>
										<tr>
											<td width='53%' align='center' class='thinBorderB thinBorderR gridBG  thinBorderT ui-corner-TL rowHeight txtBold' i18n_key="ancillary_PassengerName">Passenger Name</td>
											<td width='20%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold' i18n_key="ancillary_SeatNo">Seat No</td>
											<td width='20%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold' i18n_key="ancillary_SeatCharge">Seat Charge</td>
											<td width='7%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR rowHeight'>&nbsp;</td>
										</tr>
									</thead>
									<tbody>
										<tr><td colspan='4'>
											<div style="height: 200px;overflow: auto;">
												<table width='99%' border='0' cellpadding='1' cellspacing='0'>
													<thead>
														<tr>
															<td width='54%' style="height: 1px"></td>
															<td width='21%' style="height: 1px"></td>
															<td width='21%' style="height: 1px"></td>
															<td width='6%' style="height: 1px"></td>
														</tr>
													</thead>
													<tbody id="tbdyAnciSeatPax" ></tbody>
												</table>
											</div></td></tr>
									</tbody>
								</table>
							</span>