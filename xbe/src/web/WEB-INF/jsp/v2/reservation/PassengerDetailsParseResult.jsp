<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<%@ include file='../common/inc_PgHD.jsp' %>
	<style>
		#uploadForm {
			display: block;
			margin: 20px auto;
			background: #eee;
			border-radius: 10px;
			padding: 15px
		}
		
		#db_Container{
			display:none! important;
		}
		
		body{
			background:#fff;
		}
	</style>
	<script type="text/javascript">
	<!--
		var tempResponse = '<%=request.getAttribute("paxDetailsFileUploadResultData")%>';
		$(document).ready(function(){
			$("#db_Container").remove();
			hideUploadInformation();
			
			//File submitted successfully 
			if(tempResponse!=null && tempResponse!="null"){
				hideUploadInformation();
				
				var jsonString=tempResponse;
				var resultData=$.parseJSON(jsonString);
				if(resultData.success){
					$("#lblUploadStatus").text("Parsing successfull for :");
					$("#lblUploadStatus").show();
					
					$("#lblAdultCount").html("<B>Adults : </B>"+resultData.adultCount);
					$("#lblChildCount").html("<B>Children : </B>"+resultData.childCount);
					$("#lblInfantCount").html("<B>Infants : </B>"+resultData.infantCount);
					$("#uploadSuccessDetails").show();
					
					if(!parent.validatePaxCounts(resultData.adultCount, resultData.childCount, resultData.infantCount)){
						$("#lblUploadStatus").text("Reservation passenger type count doesn't match file's passenger type count. " +
								"File's passenger type count is :");
						parent.parseSuccessful=false;
					}else{
						parent.passportDetailsvalidated=false;
						parent.internationalFlightDetailsValidated=false;
						parent.parseSuccessful=true;
					}
					
					if(parent.parseSuccessful){
						parent.setAdultDetails(resultData.adultList, true);
						parent.setInfantDetails(resultData.infantList, true);
					}
				}
				else{
					parent.parseSuccessful=false;
					$("#lblUploadStatus").text("Parsing failed.");
					$("#lblUploadStatus").show();
					
					resultData.messageText==null?"Unknown Reason.":resultData.messageText;
					$("#lblErrorMessage").html("<B>Reason : </B>"+resultData.messageText);
					$("#lblFailedLineNumber").html("<B>Failed Line : </B>"+resultData.unparsableRecordCount);
					$("#uploadFailureDetails").show();
				}
			}
		});
		
		/**
		 * Hide all divs related to fileUpload progress
		 */
		hideUploadInformation = function(){
			$("#lblUploadStatus").hide();
			$("#uploadSuccessDetails").hide();
			$("#uploadFailureDetails").hide();
			$("#dataSettingFailureDetails").hide();
			$("#indeterminateImageDiv").hide();
		}
		
	-->
	</script>
</head>
<body>
<form id="uploadForm" action="PassengerDetailsParse.action" method="post" enctype="multipart/form-data" >
	<input type="file" id="csvPaxFile" name="csvPaxFile"/><br/>
	<input type="submit" value="Upload And Parse File"/>
</form>
<label id="lblUploadStatus"></label>
<div id="uploadSuccessDetails">
	<label id="lblAdultCount">Adults : </label>
	<label id="lblChildCount">Children : </label>
	<label id="lblInfantCount">Infants : </label>
</div>
<div id="uploadFailureDetails">
	<label id="lblFailedLineNumber">Failed Line : </label><br/>
	<label id="lblErrorMessage">Reason : </label>
</div>
<div id="dataSettingFailureDetails">
	<label id="lblDataSettingFailure"></label>
</div>
<div id="indeterminateImageDiv">
	<label id="lblPleaseWait">Please wait, uploading file...</label>
	<img src="../images/wait30trans.gif"/>
</div>
</body>
</html>