<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	
<%@ include file='../common/inc_PgHD.jsp' %>
<%@ include file='../common/inc_ShortCuts.jsp'%>	
	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		DATA_ResPro["externalSearchParams"] = eval('(' + '<c:out value="${requestScope.externalSearchParams }" escapeXml="false" />' + ')');
		DATA_ResPro["systemParams"] = eval('(' + '<c:out value="${requestScope.systemParams }" escapeXml="false" />' + ')');
		var carrierCode = '<c:out value="${requestScope.carrierCode}" escapeXml="false" />';
		var contactConfig = "";
		var alternativeEmailForXBE = ""; //Set here so it will be gloabally available for the scripts
		var validationGroup = "";
		var spMsgCC = "";
		var spMsgCC2 = "";
		var arrError	= new Array();
		var interlineBookingCreated = false;
   		var isMultiCity = <c:out value="${requestScope.isMultiCity }" escapeXml="false" />;
   		var excludableCharges = eval('(' + '<c:out value="${requestScope.excludableCharges }" escapeXml="false" />' + ')');
   		
   		
   		var oldResAdultPaxInfo= null;
   		var oldResInfantPaxInfo= null;
   		var oldResContactInfo = null;
   		var oldResAdultCount = 0;
   		var oldResChildCount = 0;
   		var oldResInfantCount = 0;
   		var intAgtBalance = '<c:out value="${requestScope.sesAgentBalance}" escapeXml="false" />';
	//-->
	var isMinimumFare = <c:out value="${requestScope.isMinimumFare}" escapeXml="false" />;
	
	</script>
	<c:if test="${not empty(requestScope.oldResAdultPax)}">
		<script type="text/javascript">
		var oldResAdultCount = <c:out value="${requestScope.oldResNoOfAdults }" escapeXml="false" />;
		var oldResChildCount = <c:out value="${requestScope.oldResNoOfChildren }" escapeXml="false" />;
		var oldResInfantCount = <c:out value="${requestScope.oldResNoOfInfants }" escapeXml="false" />;
   		
		var oldResAdultPaxInfo = <c:out value="${requestScope.oldResAdultPax }" escapeXml="false" />;
   		if(oldResInfantCount > 0) {
   			var oldResInfantPaxInfo= <c:out value="${requestScope.oldResInfantPax }" escapeXml="false" />;
		}
   
   		var oldResContactInfo = <c:out value="${requestScope.oldResContactInfo }" escapeXml="false" />;	
   		</script>
   	</c:if>	   	
		
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/multiSearchFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/modify/tabRequote.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/reservation/tabSearchFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/tabPassenger.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/tabAnci.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/tabAnciSummary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<!-- script type="text/javascript" src="../js/v2/reservation/tabRBD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script-->
	
	<script type="text/javascript" src="../js/v2/reservation/tabPayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
	<script type="text/javascript" src="../js/v2/reservation/tabItinerary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/reservation/makeReservation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/pageShortCuts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/minFareCalendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/calenderMinimumFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/reservation/changeFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/displayTaxesSurcharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/displayFareRules.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
 	
	<c:if test="${not empty(requestScope.sysOndSource)}">
	 <script src="<c:out value='${requestScope.sysOndSource}' escapeXml='false'/>" type="text/javascript"></script>		
	</c:if>	

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:none;">
		<div style="height:5px;"></div>
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:648px;">
			<ul>
				<li><a href="#divSearchTab" style="width:110px" i18n_key="availability_SearchFlights"> Search Flights</a></li>
				<li><a href="#divPaxTab" style="width:90px" i18n_key = "availability_Passengers">Passengers</a></li>
				<li><a href="#divAncillaryTab" onclick="UI_tabPassenger.continueOnClick()" style="width:90px" i18n_key = "availability_Ancillary">Ancillary</a></li>
				<li><a href="#divPaymentTab" onclick="UI_tabPassenger.continueOnClick()" style="width:90px" i18n_key = "availability_Payments">Payments</a></li>
				<li><a href="#divItineraryTab" style="width:90px" i18n_key = "availability_Itinerary">Itinerary</a></li>
			</ul>
			<div id="divSearchTab" style="padding-top:4px">
				<div id="divSearchTypeTab" style="padding-top:4px">
					<input type='radio' id='searchType' name='searchType' value='R' tabindex='1' onclick="UI_makeReservation.showSearchParam('R')"/>
					<span i18n_key="opt_OneWayReturn" style="margin-right:10px">One way/ Return</span>
					<input type='radio' id='searchType' name='searchType' value='M' tabindex='2' onclick="UI_makeReservation.showSearchParam('M')"/>
					<span  i18n_key="opt_MutiCity" style="margin-right:10px">Multicity</span>
					<c:choose>
						<c:when test='${requestScope.isXBEMinimumFareEnabled == "true"}'>
							<input type='radio' id='searchType' name='searchType' value='F' tabindex='3' onclick="UI_makeReservation.showSearchParam('F')"/>
							<span style="margin-right:10px">Lowest Fares Calendar</span>
						</c:when>
					</c:choose>
				</div>			
				
				<c:choose>
   			    	<c:when test='${requestScope.isMinimumFare == "true"}'>
      					 <%@ include file='inc_MinimumFare.jsp' %>
    				</c:when>
   					<c:when test='${requestScope.isMinimumFare == "false" && requestScope.isMultiCity == "true" }'>
       						 <%@ include file='inc_TabSearchMultiCity.jsp' %>
    				</c:when>
    				<c:otherwise>
        					<%@ include file='inc_TabSearchFlights.jsp' %>
    				</c:otherwise>
				</c:choose>	
				
			</div>
			
			<form method='post' action="" id="frmMakeBkg">
				<div id="divPaxTab" style="padding-top:5px">
					<%@ include file='inc_TabPassengerDetails.jsp' %>
				</div>
				
				<div id="divAncillaryTab" style="padding-top:5px">
					<%@ include file='inc_TabAnci.jsp' %>
				</div>
				<input id="resAvailableBundleFareLCClassStr" name="resAvailableBundleFareLCClassStr" type="hidden" value='<c:out value="${resAvailableBundleFareLCClassStr}" escapeXml="false"/>'/>
				<input id="resFlexiAvailableStr" name="resFlexiAvailableStr" type="hidden" value='<c:out value="${resFlexiAvailableStr}" escapeXml="false"/>'/>
				
			</form>	
				<div id="divPaymentTab" style="padding-top:5px">
				 	<%@ include file='inc_TabPayment.jsp' %>
				</div>			
			<div id="divItineraryTab" style="padding-top:5px">
				<%@ include file='inc_TabItinerary.jsp' %>
			</div>
		</div>
		<div id="csvFileUploadDiv" style="display: none;">
			<iframe name="uploadResultFrame" id="uploadResultFrame" frameborder="0" style="width:100%;height:100%;border:0" src="PassengerDetailsParse.action"></iframe>
		</div>
	</div>
	<form method='post' action="" id="frmTabLoad"></form>
	<form method='post' action="" id="frmMultiBkg">
		<!-- Data to auto fill new pnr from this pnr -->
		<input type="hidden" name="oldAdultPaxInfo" id="oldAdultPaxInfo"/>
		<input type="hidden" name="oldInfantPaxInfo" id="oldInfantPaxInfo"/>
		<input type="hidden" name="oldContactInfo" id="oldContactInfo"/>
		<input type="hidden" name="oldNoOfAdults" id="oldNoOfAdults"/>
		<input type="hidden" name="oldNoOfChildren" id="oldNoOfChildren"/>
		<input type="hidden" name="oldNoOfInfants" id="oldNoOfInfants"/>
	</form>
	<%@ include file='../common/inc_Credit.jsp' %>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
	<%@ include file='../reservation/inc_FlightStopOverDetails.jsp' %>
</body>



</html>