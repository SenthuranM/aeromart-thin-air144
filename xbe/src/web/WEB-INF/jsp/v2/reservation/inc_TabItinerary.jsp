<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

	<div id="mainDivItinerary">
	<table width='100%' border='0' cellpadding='0' cellspacing='0'>		
		
		<tr>
			<td class="txtBold txtMediumSmall"  i18n_key="itinerary_NewbookingSuccessfullyCompleted">
				Reservation View : New booking is successfully completed
			</td>
		</tr>
		<tr id="offlinePaymentMsg">
			<td class="txtBold txtMediumSmall"  i18n_key="itinerary_offlinePaymentMessage">
				Please visit any of our agents and do the payment or make the payment at  
				selected offline payment gateway for booking confirmation.
			</td>
		</tr>
		<tr id="trSplitBooking">
			<td>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td width='5%' i18n_key="PassengerNameRecord">PNR : </td>
									<td><div id='divSplitPNR' class='txtBold txtLarge'></div></td>									
									<td width='40%'> <div id='divSplitStatus' class='txtBold txtLarge'></div></td>
									<td align='right'><label id="lblSplitRelease" i18n_key="itinerary_ReleaseDateAndTime">Release Date &amp; Time :</label></td>
									<td align='right'>
										<div id='divSplitRelease' class='txtBold txtLarge'></div>
									</td>						
								</tr>
													
							</table>
						</td>
					</tr>
					<tr>
						<td style='height:145px;' valign='top'>
							<div id='divSplitItnPane' style='position:absolute;width:935px;height:140px;overflow-X:hidden; overflow-Y:auto;' class='thinBorderT thinBorderB thinBorderR thinBorderL'>							
								<table width='100%' border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td>
											<table cellpadding="0" cellspacing="0" border="0" width="100%">
												<thead id="itnSplitFlightHead">
													<tr>
														<td colspan='8' class='lightBG rowHeight'>
															<label id="itnSplitType" class='txtBold' i18n_key="ItnType">itnType</label>
														</td>
													</tr>
													<tr>
														<td width='17%' class='thinBorderB thinBorderR thinBorderL rowHeight' align='center' i18n_key="itinerary_ItnOND">itnOND</td>
														<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnFlightNo">itnFlightNo</td>
														<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnAirLine">itnAirLine</td>
														<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnDepartureText">itnDepartureText</td>
														<td width='22%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnDepartureDateTime">itnDepartureDateTime</td>
														<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnArrivalText">itnArrivalText</td>
														<td width='22%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnArrivalDateTime">itnArrivalDateTime</td>
														<td width='13%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnStatus">itnStatus</td>
													</tr>
													<tr>
														<td colspan='8' class='singleGap'>
														</td>
													</tr>
												</thead>
												<tbody id="itnSplitFlightDetails"></tbody>
											</table>
										</td>
									</tr>
															
									<tr>
										<td>
											<table cellpadding="0" cellspacing="0" border="0" width="100%">
												<thead>
													<tr>
														<td colspan='2' class='lightBG txtWhite rowHeight' i18n_key="itinerary_PassengerInformation">
															<b>Passenger Information </b>
														</td>
													</tr>
													<tr>
														<td class='rowHeight' align='center' i18n_key="itinerary_PassengerName"><b>Passenger Name </b></td>
														<td class='rowHeight' align='center' i18n_key="itinerary_AdditionalServiceDetails"><b>Additional Service Details  </b></td>											
													</tr>
													<tr>
														<td colspan='2' class='noPadding'>
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tbody id="itnSplitPaxDet"></tbody>
															</table>
														</td>
													</tr>
												</thead>
											</table>
										</td>
									</tr>									
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td width='5%' class="txtBold txtMedium" i18n_key="PassengerNameRecord">PNR : </td>
						<td  class="txtBold txtMedium"><div id='divPNR' class='txtBold txtMedium'></div></td>
						<td width='40%'><div id='divStatus' class='txtBold txtMediumSmall'></div></td>
						<td style="display:none;" id="lblREF" align='center',width='15%' i18n_key="PassengerNameRecord">Fatourati Ref : </td>
						<td><div style="display:none;"  id='divREF' class="txtBold txtMedium"></div></td>
						<td align='right'><label id="lblRelease" class="txtBold txtMediumSmall" i18n_key="itinerary_ReleaseDateAndTime">Release Date &amp; Time :</label></td>
						<td align='right'>
							<div id='divRelease' class='txtBold txtMediumSmall'></div>
						</td>
						
						
					</tr>
					<tr>
						<td class='singleGap'></td>
					</tr>
					<tr id='trInsurance'>
						<td colspan="3" class="txtBold txtMediumSmall" i18n_key="itinerary_PolicyCode">POLICY CODE : <span id='divPolicyCode' class='txtBold txtMediumSmall'></span></td>
						<td align='right'></td>
						<td align='right'></td>						
					</tr>
					<tr id="trPaymentGatewayBillNumber" style="display: none">
						<td colspan="5" class="txtBold txtMediumSmall">
							<span id="paymentGatewayBillNumberLabel">Bill Number : </span>
							<span id="paymentGatewayBillNumber"></span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class='singleGap'></td>
		</tr>
		<tr>
			<td  id="trCNFPaX"style='height:505px;' valign='top'>
				<div id='divItnPane' style='position:absolute;width:935px;height:500px;overflow-X:auto; overflow-Y:auto;' class='thinBorderT thinBorderB thinBorderR thinBorderL'>
					<table width='99%' border='0' cellpadding='0' cellspacing='0' align="center">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<thead id="itnFlightHead">
										<tr>
											<td colspan='8' class='lightBG rowHeight'>
												<label id="itnType" class='txtBold' i18n_key="itinerary_ItnType">itnType</label>
											</td>
										</tr>
										<tr>
											<td width='17%' class='thinBorderB thinBorderR thinBorderL rowHeight' align='center' i18n_key="ItnOND">itnOND</td>
											<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnFlightNo">itnFlightNo</td>
											<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnAirLine">itnAirLine</td>
											<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnDepartureText">itnDepartureText</td>
											<td width='22%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnDepartureDateTime">itnDepartureDateTime</td>
											<td width='7%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnArrivalText">itnArrivalText</td>
											<td width='22%' class='thinBorderB thinBorderR' align='center' i18n_key="itinerary_ItnArrivalDateTime">itnArrivalDateTime</td>
											<td width='13%' class='thinBorderB thinBorderR' align='center' i18n_key="ItnStatus">itnStatus</td>
										</tr>
										<tr>
											<td colspan='8' class='singleGap'>
											</td>
										</tr>
									</thead>
									<tbody id="itnFlightDetails"></tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class='doubleGap'></td>
						</tr>						
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<thead>
										<tr>
											<td colspan='2' class='lightBG txtWhite rowHeight' i18n_key="itinerary_PassengerInformation">
												<b>Passenger Information </b>
											</td>
										</tr>
										<tr>
											<td class='rowHeight' align='center' i18n_key="itinerary_PassengerName"><b>Passenger Name </b></td>
											<td class='rowHeight' align='center' i18n_key="itinerary_AdditionalServiceDetails"><b>Additional Service Details  </b></td>
										</tr>
										<tr>
											<td colspan='2' class='noPadding'>
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tbody id="itnPaxDet"></tbody>
												</table>
											</td>
										</tr>
									</thead>
								</table>
							</td>
						</tr>
						<tr>
							<td class='doubleGap'></td>
						</tr>
						<tr>
							<td>
								<table cellpadding="1" cellspacing="0" border="0" width="100%">
									<thead>
										<tr>
											<td colspan='2' class='lightBG txtWhite rowHeight'>
												<b i18n_key="itinerary_PaymentDetails">Payment Details </b>
											</td>
										</tr>
									</thead>
									<tbody id="itnPriceDetails"></tbody>
									<tfoot>
										<tr id="trITNDiscount">
											<td class='thinBorderB thinBorderR thinBorderL rowHeight rowHeight' align='center' width='80%'>
												<label id="lblTotalFareDiscount" class='lblTotalFareDiscount' i18n_key="itinerary_TotalDiscount">Total Discount</label>
											</td>
											<td class='thinBorderB thinBorderR' align='right' width='20%'>
												<div id='divITNFareDiscount' class='divITNFareDiscount'></div>
											</td>
										</tr>
										<tr>
											<td class='thinBorderB thinBorderR thinBorderL rowHeight rowHeight' align='center' width='80%'>
												<label id="lblTotalAmt" i18n_key="itinerary_TotalAmount"><b>Total Amount </b> </label><label id="lblselTotalAmt"></label>
											</td>
											<td class='thinBorderB thinBorderR' align='right' width='20%'>
												<div id='divITNTotalAmount' class='txtBold'></div>
											</td>
										</tr>
										<tr id="trITNDiscountCredit">
											<td class='thinBorderB thinBorderR thinBorderL rowHeight rowHeight' align='center' width='80%'>
												<label id="lblTotalFareDiscount" class='lblTotalFareDiscount' i18n_key="itinerary_TotalDiscount">Total Discount</label>
											</td>
											<td class='thinBorderB thinBorderR' align='right' width='20%'>
												<div id='divITNFareDiscount' class='divITNFareDiscount'></div>
											</td>
										</tr>
									</tfoot>
								</table>
							</td>
						</tr>
						<tr id="flexiDetails">
							<td>
							<table cellpadding="1" cellspacing="0" border="0" width="100%">
								<thead>
									<tr>
										<td colspan='4' class='lightBG txtWhite rowHeight' i18n_key="itinerary_FlexiRuleDetails">
											<b>Flexi Rule Details</b>
										</td>
									</tr>
								</thead>
								<tbody id="flexiData"></tbody>
							</table>
							</td>
						</tr>
						<tr>
							<td class='doubleGap'></td>
						</tr>
						<tr>
							<td>
								<table cellpadding="1" cellspacing="0" border="0" width="100%">
									<thead>
										<tr>
											<td colspan='4' class='lightBG txtWhite rowHeight' i18n_key="itinerary_ContactDetails">
												<b>Contact Details </b>
											</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrFName">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_FirstName">First Name :</td>
														<td><div id='divCDFirstName' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td>
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrLName">
													<tr>
														<td width='20%' i18n_key="itinerary_LastName">Last Name :</td>
														<td><div id='divCDLastName' class='txtBold'></div></td>
													</tr>
												</table>
											</td>				
										</tr>
										<tr id="tblItnrAddr1">
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_Address">Address :</td>
														<td><div id='divCDStreet' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td></td>
										</tr>
										<tr id="tblItnrAddr2">
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0">
													<tr>
														<td width='20%' class='rowHeight'></td>
														<td><div id='divCDAddress' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td></td>
										</tr>
										<tr>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrCity">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_City">City :</td>
														<td><div id='divCDCity' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrMobile">
													<tr>
														<td width='20%' i18n_key="itinerary_Mobile">Mobile No :</td>
														<td><div id='divCDMobileNo' class='txtBold'></div></td>	
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrCountry">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_Country">Country :</td>
														<td><div id='divCDCountry' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrPhone">
													<tr>
														<td width='20%' i18n_key="itinerary_PhoneNo">Phone No :</td>
														<td><div id='divCDPhoneNo' class='txtBold'></div></td>
													</tr>
												</table>
											</td>									
										</tr>
										<tr>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrEmail">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_Email">E-mail :</td>
														<td><div id='divCDEmail' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrFax">
													<tr>
														<td width='20%' i18n_key="itinerary_FaxNo">Fax No :</td>
														<td><div id='divCDFaxNo' class='txtBold'></div></td>
													</tr>
												</table>
											</td>											
										</tr>
										<tr>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrState">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_State">State :</td>
														<td><div id='divState' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrTaxRegNo">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_TaxRegNo">Tax Registration No :</td>
														<td><div id='divTaxRegNo' class='txtBold'></div></td>
													</tr>
												</table>
											</td>											
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class='doubleGap'></td>
						</tr>
						<tr id="trItnrEmgnContactDetails">
							<td>
								<table cellpadding="1" cellspacing="0" border="0" width="100%">
									<thead>
										<tr>
											<td colspan='4' class='lightBG txtWhite rowHeight' i18n_key="itinerary_EmergencyContactDetails">
												<b>Emergency Contact Details </b>
											</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrEmgnFName">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_FirstName">First Name :</td>
														<td width='80%'><div id='divEmgnDispFName' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrEmgnLName">
													<tr>
														<td width='20%' i18n_key="itinerary_LastName">Last Name :</td>
														<td width='80%'><div id='divEmgnDispLName' class='txtBold'></div></td>
													</tr>
												</table>
											</td>								
										</tr>
										<tr>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrEmgnPhone">
													<tr>
														<td width='20%' i18n_key="itinerary_PhoneNo">Phone No :</td>
														<td><div id='divEmgnDispPhoneNo' class='txtBold'></div></td>
													</tr>
												</table>
											</td>
											<td width="50%">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" id="tblItnrEmgnEmail">
													<tr>
														<td width='20%' class='rowHeight' i18n_key="itinerary_Email">E-mail :</td>
														<td><div id='divEmgnDispEmail' class='txtBold'></div></td>
													</tr>
												</table>
											</td>											
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td class='singleGap'></td>
		</tr>
		<tr>
			<td>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td width='15%' i18n_key="itinerary_ItineraryLanguage">
							Itinerary Language 
						</td>
						<td>
							<select id='selITNLangIte' name='selITNLangIte' class="aa-input" style="width:100px;" tabindex="2"></select>
							<u:hasPrivilege  privilegeId="xbe.res.itn.chg">
								Print Ticket Chg <input type='checkbox' id='chkPax' name='chkPax' class="noBorder" checked="checked" tabindex="3">
							</u:hasPrivilege>
							&nbsp;
							<u:hasPrivilege  privilegeId="xbe.res.itn.withTAndC">
								Print T & C <input type='checkbox' id='chkTC' name='chkTC' class="noBorder" checked="checked" tabindex="4">
							</u:hasPrivilege>
						</td>
						<td align='right'>
						    <u:hasPrivilege  privilegeId="xbe.res.rcpt.print">
							<button id="btnPrintReciept" type="button" class="btnMargin" tabindex="5" style="width:100px"  i18n_key="PrintReciept">Print Reciept</button> &nbsp;
							</u:hasPrivilege>							
							
							<button id="btnPrint" type="button" class="btnMargin" tabindex="6" i18n_key="btn_Print">Print</button>
						</td>
					</tr>
					<tr>
						<td width='15%'>
							<label id="lblTaxInvoice" i18n_key="itinerary_invoice">Invoice</label>
						</td>
						<td align='left'>						
							<button id="btnPrintInvoice" type="button" class="btnMargin" tabindex="6" i18n_key="btn_Print_Invoice">Print Invoice</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<%@ include file='../modify/displayGroupBookingItinerary.jsp' %>
	<%@ include file='../modify/displayInvoices.jsp' %>
	
<script type="text/javascript">
	$("#mainDivItinerary").getLanguage();
	$("#divGroupItineraryDisplay").getLanguage();
	
</script>