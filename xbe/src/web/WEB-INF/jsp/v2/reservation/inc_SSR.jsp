	<div id='divSSRPane' style='position:absolute;top:200px;left:100px;width:700px;z-index:2'>
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key="passenger_Details">Details</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr>
							<td class='popBGColor'  align='center'>
								<table width='95%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td i18n_key="passenger_SSRCode">SSR Code : </td>
										<td>
											<select id="selSSRCode" name="selSSRCode" class="aa-input"  size="1" style="width:200px" ></select>
										</td>
									</tr>
									<tr>
										<td valign='top' i18n_key="passenger_AdditionalInformation">Additional Information : </td>
										<td>
											<textarea class="aa-input" id="txtComment" cols="80" rows="5"></textarea>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
						<tr>
							<td class='popBGColor rowHeight' align='right'>
								<button id="btnSSRConfirm" type="button" class="btnMargin" i18n_key="btn_Confirm">Confirm</button><button id="btnSSRCancel" type="button" class="btnMargin">Cancel</button>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>