
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr id="trSearchParams">
		<td class="ui-widget ui-widget-content ui-corner-all">
			<form method='post' action="availabilitySearch.action"
				id="frmSrchFlt">
				<table width='98%' border='0' cellpadding='1' cellspacing='0'
					align='center'>
					<tr>
						<td align="left" colspan='11'>
							<div id='airportMsg' class="spMsg"
								style='padding: 7px; padding-left: 20px; position: absolute; width: 893px; z-index: 50;'
								onclick='UI_tabSearchFlights.hideAirportMessage()'></div>
						</td>
					</tr>
					<tr>
						<td colspan='11' class='singleGap'></td>
					</tr>
					<tr>
						<td style="min-width: 35px">From:</td>
						<td style="min-width: 170px">
							<table border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td><input type="text" id="fAirport" class="aa-input"
										style="width: 150px" tabindex="1" />
									</td>
									<td><%@ include file='../common/inc_MandatoryField.jsp'%>
									</td>
								</tr>
							</table>
						</td>
						<td style="min-width: 80px">Departure Date:</td>
						<td style="min-width: 100px"><input id="departureDate"
							name="searchParams.departureDate" type="text" class="aa-input"
							style="width: 80px" maxlength="10" tabindex="3" /><%@ include
								file='../common/inc_MandatoryField.jsp'%></td>
						<td style="min-width: 25px"><input id="departureVariance"
							name="searchParams.departureVariance" type="text"
							class="aa-input rightAlign" style="width: 22px;" maxlength="1"
							tabindex="4" /></td>
						<td style="min-width: 70px">+/- Days</td>
						<td style="min-width: 45px">Adults:</td>
						<td style="min-width: 45px"><input id="adultCount"
							name="searchParams.adultCount" type="text"
							class="aa-input rightAlign" style="width: 22px;" maxlength="3"
							tabindex="9" /><%@ include
								file='../common/inc_MandatoryField.jsp'%></td>
						<td style="min-width: 60px">Class:</td>
						<td><select id="classOfService"
							name="searchParams.classOfService" class="aa-input"
							style="width: 80px" tabindex="12"></select><%@ include
								file='../common/inc_MandatoryField.jsp'%></td>
						<td align="right" valign="bottom" rowspan='4'>
							<button id="btnSearch" type="button"
								style="margin: 1px; width: 60px" tabindex="15">Search</button>
						</td>
					</tr>
					<tr>
						<td>To:</td>
						<td>
							<table border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td><input type="text" id="tAirport" class="aa-input"
										style="width: 150px" tabindex="2" />
									</td>
									<td><%@ include file='../common/inc_MandatoryField.jsp'%>
									</td>
								</tr>
							</table>
						</td>
						<td>Return Date:</td>
						<td><input id="returnDate" name="searchParams.returnDate"
							type="text" class="aa-input" style="width: 80px" maxlength="10"
							tabindex="5" /></td>
						<td><input id="returnVariance"
							name="searchParams.returnVariance" type="text"
							class="aa-input rightAlign" style="width: 22px;" maxlength="1"
							tabindex="6" /></td>
						<td>+/- Days</td>
						<td>Children:</td>
						<td><input id="childCount" name="searchParams.childCount"
							type="text" class="aa-input"
							style="width: 22px; text-align: right;" maxlength="3"
							tabindex="10" /></td>
						<td>Currency:</td>
						<td><select id="selectedCurrency"
							name="searchParams.selectedCurrency" class="aa-input"
							style="width: 80px" tabindex="13"></select><%@ include
								file='../common/inc_MandatoryField.jsp'%></td>
					</tr>
					<tr>
						<td>Search Options</td>
						<td><select id="selSearchOptions"
							name="searchParams.searchSystem" class="aa-input" tabindex="14"
							style="width: 150px" title="Flight Search Options"></select>
						</td>
						<td><div id='divValidity'>Validity:</div></td>
						<td><select id="validity" name="searchParams.validity"
							class="aa-input" style="width: 80px" tabindex="7"></select></td>
						<td><input id="openReturn" name="searchParams.openReturn"
							type="checkbox" class="noBorder" value="true" tabindex="8" /></td>
						<td><div id='divOpen'>Open</div></td>
						<td>Infants:</td>
						<td><input id="infantCount" name="searchParams.infantCount"
							type="text" class="aa-input rightAlign" style="width: 22px;"
							maxlength="3" tabindex="11" /></td>
						<td>Type:</td>
						<td><select id="bookingType" name="searchParams.bookingType"
							class="aa-input" style="width: 80px" tabindex="14"></select><%@ include
								file='../common/inc_MandatoryField.jsp'%></td>
					</tr>
					<tr id='trPaxFare'>
						<td><span id='spnFareType'>Fare Type:</span></td>
						<td><select id="selFareTypeOptions"
							name="searchParams.fareType" class="aa-input" tabindex=""
							style="width: 100px" title="Fare Type Options"></select>
						</td>
						<td><span id='spnPaxType'>Pax Type:</span></td>
						<td><select id="selPaxTypeOptions"
							name="searchParams.paxType" class="aa-input" tabindex=""
							style="width: 100px" title="Customer type"></select>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><u:hasPrivilege privilegeId="xbe.res.make.agent.fare">
								<span id='spnTAgent'>Travel Agent:</span>
							</u:hasPrivilege>
						</td>
						<td><u:hasPrivilege privilegeId="xbe.res.make.agent.fare">
								<input type="text" id="tAgent" class="aa-input"
									style="width: 150px" />
							</u:hasPrivilege>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><span id='spnBookingClass' title="Booking Class">B.
								Class</span>
						</td>
						<td><select id="selBookingClassCode"
							name="searchParams.bookingClassCode" class="aa-input"
							tabindex="15" style="width: 80px" title="Booking Class"></select>
						</td>
					</tr>
					<tr>
						<td colspan='11' class='singleGap'></td>
					</tr>



				</table>
				<input id="firstDepature" name="searchParams.firstDeparture"
					type="hidden" /> <input id="lastArrival"
					name="searchParams.lastArrival" type="hidden" /> <input
					id="fromAirportSubStation"
					name="searchParams.fromAirportSubStation" type="hidden" /> <input
					id="toAirportSubStation" name="searchParams.toAirportSubStation"
					type="hidden" /> <input type="hidden" name="ticketExpiryDate"
					id="ticketExpiryDate" /> <input type="hidden"
					name="ticketValidFrom" id="ticketValidFrom" /> <input
					type="hidden" name="ticketExpiryEnabled" id="ticketExpiryEnabled" />
				<!-- <input id="travelAgent" name="searchParams.travelAgent" type="hidden"/> -->
			</form>
		</td>
	</tr>
	<tr id="trRbdResults">
		<td>
			<div id="spnavailabilityPane" style="visibility: visible;">
				<table cellspacing="0" cellpadding="0" border="0" align="center"
					width="100%" style="border: 1px solid #AAA">
					<tr>
						<td style="height: 10px" style="background-color:#Fff;"></td>
					</tr>
					<tr>
						<td align="center" style="background-color: #Fff;" width="98%">
							<div class="resultContainer" id="panelContainer">
								<div class="panels" id="panel" style="display:none;">
									<div class="zoomerHeader">
										<table width="98%">
											<tr>
												<td align="left"><a id='lnkOP' href='#'
													class='noTexDeco classNav' tabindex="16">&lt;Previous Day</a></td>
												<td align="center"><font class="FormHeader"><label id="ondCode" ></label>
														<span ><label id="date" ></label></span> </font>
												</td>
												<td align="right"><a id='lnkON' href='#'
													class='noTexDeco classNav' tabindex="17">Next Day&gt;</a></td>
											</tr>
										</table>
									</div>
									<div class="sectionZoomerouter">
										<div class="sectionZoomer">
											<table cellspacing="0" cellpadding="2" border="0"
												align="center" width="100%" class="midTable">
												<tr>
													<td width="40%" valign="top">
														<!--<div style="overflow-x:scroll;overflow-y:hidden;width:340px">
			            			-->
														<table id="flightTable" class="scroll"></table> <!--</div>
			                     --></td>
													<td width="60%" valign="top" id="fareTable"><font>Select
															flight to show the booking classes</font>
													</td>
												</tr>
											</table>

										</div>
										<div class="sectionZoomerHandler"></div>
									</div>
								</div>
								<!-- <div class="panels">
									<div class="zoomerHeader">
										<table width="98%">
											<tr>
												<td align="left"><a id='lnkOP' href='#'
													class='noTexDeco' tabindex="16">&lt;Previous Day</a></td>
												<td align="center"><font class="FormHeader">Incoming
														<span id="spnOD">31/Jun/2010</span> </font>
												</td>
												<td align="right"><a id='lnkON' href='#'
													class='noTexDeco' tabindex="17">Next Day&gt;</a></td>
											</tr>
										</table>
									</div>
									<div class="sectionZoomerouter">
										<div class="sectionZoomer">
											<table cellspacing="0" cellpadding="2" border="0"
												align="center" width="100%" class="midTable">
												<tr>
													<td width="40%" valign="top">
														<table id="table2" class="scroll"></table> <!--</div>
			                     </td>
													<td width="700" valign="top" id="fareTable2"></td>
												</tr>
											</table>

										</div>
										<div class="sectionZoomerHandler"></div>
									</div>
								</div> -->
								<div style="padding-left: 10px">
									<font>* Agent is responsible to ensure correct booking
										class is combined</font>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" align="right">
							<div class="sectionSelection" style="display: none;">
								<table cellspacing="1" cellpadding="3" border="0" align="center"
									width="100%">
									<thead>
										<tr>
											<th colspan="10"><font class="FormHeader">Selected
													Flights</font></th>
										</tr>
									</thead>
									<tbody>
										<tr id="outbound">

										</tr>
										<tr id="inbound">

										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" align="right" style="padding: 0 10px"><input
							type="button" name="btBackresults" style="width: 100px;"
							class="Button" value="Back to Results" id="btBackresults" /> <input
							type="button" name="btnFareQuote" style="width: 100px;"
							class="Button" value="Fare Quote" id="btnFareQuote"
							disabled="disabled" />
						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="fareQuoteData">
								<table cellspacing="1" cellpadding="2" border="0" align="center"
									width="100%"
									class="GridRow GridHeader GridHeaderBand GridHDBorder">
									<tbody>
										<tr>
											<td align="left"><font class="fntBold">Fare quote
													- Outgoing 02/Aug/2010 - Return 06/Aug/2010 - Economy Class</font>
											</td>
										</tr>
										<tr>
											<td align="center" valign="top" style="height: 187px;">
												<table cellspacing="1" cellpadding="2" border="0"
													width="100%" id="Table10">
													<tbody>
														<tr>
															<td width="28%"><font class="fntBold">&nbsp;</font>
															</td>
															<td align="center" width="14%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Pax Type</font></td>
															<td align="center" width="10%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Fare &nbsp; </font></td>
															<td align="center" width="10%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Tax</font> &nbsp;</td>
															<td align="center" width="10%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Sur.</font> &nbsp;</td>
															<td align="center" width="10%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Per Pax</font>
															</td>
															<td align="center" width="8%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">No. pax</font></td>
															<td align="center" width="10%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Total &nbsp; </font></td>
															<td align="center" width="10%"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Total Price</font></td>
														</tr>
														<tr>
															<td style="height: 2px;" colspan="9"></td>
														</tr>
														<tr>
															<td valign="middle"
																class="GridItemRow GridItem GridItemHover defaultCursor">
																<font title="Outgoing">Damascus - Sabiha Gokcen |
																	6Q201 </font></td>
															<td
																class="GridItemRow GridItem GridItemHover defaultCursor"><font>Adult(s)&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><span
																class="editFare"><img src="./../images/edit.gif" /><font>990.00</font>&nbsp;</span>
																<div style="display: none">
																	<input type="text" class="faretext" size="5"
																		style="text-align: right" />
																</div>
															</td>

															<td align="right" class="GridItemRow GridItem"><font>296.00&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><font>205.00&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><font
																class="row">1491.00</font>&nbsp;</td>
															<td align="right" class="GridItemRow GridItem"><font>1&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><font
																class="row">1491.00</font>&nbsp;</td>

															<td align="right" class="GridItemRow GridItem"><font
																class="row">1491.00</font>&nbsp;</td>

														</tr>
														<tr>
															<td style="height: 2px;" colspan="9"></td>
														</tr>
														<tr>
															<td valign="middle"
																class="GridItemRow GridItem GridItemHover defaultCursor">
																<font title="Return">Sabiha Gokcen - Damascus |
																	6Q945</font>
															</td>
															<td
																class="GridItemRow GridItem GridItemHover defaultCursor"><font>Adult(s)&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><span
																class="editFare"><img src="./../images/edit.gif" /><font>990.00</font>&nbsp;</span>
																<div style="display: none">
																	<input type="text" class="faretext" size="5"
																		style="text-align: right" />
																</div>
															</td>

															<td align="right" class="GridItemRow GridItem"><font>55.00&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><font>245.00&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><font
																class="row">1290.00</font>&nbsp;</td>
															<td align="right" class="GridItemRow GridItem"><font>1&nbsp;</font>
															</td>
															<td align="right" class="GridItemRow GridItem"><font
																class="row">1290.00</font>&nbsp;</td>

															<td align="right" class="GridItemRow GridItem"><font
																class="row">1290.00</font>&nbsp;</td>

														</tr>
														<tr>
															<td style="height: 2px;" colspan="9"></td>
														</tr>
														<tr>
															<td align="left" valign="middle" colspan="6"
																class="GridItemRow GridItem GridItemHover defaultCursor">
																<table cellspacing="0" cellpadding="2" border="0"
																	width="100%">
																	<tbody>
																		<tr>
																			<td align="left"></td>
																			<td align="right"><font class="fntBold">AED</font>
																				<font title="Per pax charge in AED for Adult"
																				class="fntBold tot"> 2781.00</font>&nbsp;</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td align="right" colspan="2"
																class="GridItemRow GridItem GridItemHover defaultCursor"><font
																class="fntBold">Total Price AED&nbsp;</font></td>
															<td align="right" valign="middle"
																class="GridItemRow GridItem GridItemHover defaultCursor">
																<font class="fntBold tot">2781.00</font>&nbsp;</td>
														</tr>
														<tr>
															<td colspan="2">&nbsp;</td>
														</tr>
														<tr>
															<td colspan="10" align="right"><input type="button"
																name="btnContinue" class="Button" value="Book"
																id="btnContinue" style="visibility: visible;" /> <input
																type="button" name="btnReset" class="Button"
																value="Reset" id="btnReset" /> <input type="button"
																name="btnExit" class="Button" value="Close" id="btnExit" />
															</td>
														</tr>

													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</div></td>
	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>
	<tr>
		<td style='height: 470px;' valign='top'>
			<div id='divResultsPane'></div></td>
	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>
	<tr>
		<td align='right'>
			<button id="btnBook" type="button" class="btnMargin" tabindex="27">Book</button>
			<button id="btnReset" type="button" class="btnMargin" tabindex="28">Reset</button>
			<button id="btnCancel" type="button" class="btnMargin" tabindex="29">Close</button>
		</td>
	</tr>
</table>
