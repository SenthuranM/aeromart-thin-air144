<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Fares Terms & Conditions</title>
<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/reservation/displayFareRules.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapperPopUp" style="display:none;">
		<table style='width:900px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:880px;">
						<table width='97%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr id="trFareRuleDetails" style="display: none;">
								<td class="ui-widget ui-widget-content ui-corner-all" height="30">
									<table id="tblFareRuleDetails" cellpadding="0" cellspacing="0" width='100%'>
									</table>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr id="trFareRuleDescription" style="display: none;">
								<td class="ui-widget ui-widget-content ui-corner-all" >
									<table id="tblFareRuleDescription" cellpadding="0" cellspacing="0" width='100%'>
									</table>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr id="trFlexiFareRuleDetails" style="display: none;">
								<td class="ui-widget ui-widget-content ui-corner-all" height="30">
									<table id="tblFlexiFareRuleDetails" cellpadding="0" cellspacing="0" width='100%'>
									</table>
								</td>
							</tr>
							<tr>
								<td  class='doubleGap'></td>
							</tr>
							<tr>
								<td align='right'>
									<button id="btnClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
								</td>
							</tr>
							<tr>
								<td  class='doubleGap'></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='inc_pageError.jsp' %>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguageForOpener();

</script>