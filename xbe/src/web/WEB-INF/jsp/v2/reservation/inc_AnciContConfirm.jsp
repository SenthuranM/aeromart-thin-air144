	<div id='divAnciContConfirm' style='position:absolute;top:300px;left:350px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key="ancillary_AncillarySelectionConfirm"> &nbsp;Ancillary Selection Confirm</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>	
						<tr >
							<td align="center" class='popBGColor'>
							<table width='95%' border='0' cellpadding='1' cellspacing='0'>
							<tr>
								<td class='popBGColor' align="left"><label id='lblCheapBundledFare' i18n_key="ancillary_YouCanReduceAncillaryCostBySelecting"> You can reduce ancillary cost by selecting  </label></td>
							</tr>					
							<tr >
								<td class='popBGColor txtBold' align="left"><span id="spanCheapBundledFare" ></span></td>
							</tr>
							
							<tr>
								<td class='popBGColor' align="left"><label id='lblAnciCtdConfirm' i18n_key="ancillary_YouHaveNotSelectedAnyOfTheFollowingAncillaries"> You have not selected any of the following ancillaries</label></td>
							</tr>					
							<tr >
								<td class='popBGColor txtBold' align="left"><span id="spnNotSelectedAnci" ></span></td>
							</tr>
							<tr>
								<td class='popBGColor singleGap'></td>
							</tr>
							<tr >
								<td class='popBGColor ' align="left" i18n_key="ancillary_WouldYouLikeToContinue">Would you like to continue ?</td>
							</tr>	
							<tr>
								<td class='popBGColor singleGap'></td>
							</tr>						
							<tr>
								<td class='popBGColor rowHeight' align='right'>
									<button id="btnAnciContOk" type="button"  class="btnMargin" i18n_key="btn_Yes">Yes </button>
									<button id="btnAnciContNo" type="button"   class="btnMargin" i18n_key="btn_No">No </button>
								</td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>