<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<form id="frmPayment" method="post" action="">
	<div style="overflow: auto; height: 600px; width:940px; margin: 0 auto" id="paymentContainer">
	<table width='100%' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td align="left" >				
				<div id='payPageAirportMsg' class="spMsg" style='padding:7px;padding-left:20px;position:absolute;width:893px;z-index:50;display:none' onclick='UI_tabPayment.hideAirportMessage()'></div>
			</td>
		</tr>
		<tr>
			<td class="ui-widget ui-widget-content ui-corner-all" style='height:110px;' valign='top'>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td class='paneHD'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='20%'><p class='txtWhite txtBold' i18n_key="payement_PaymentDetails">Payment Details</p></td>
									<td width='32%'>
										<div id='divBkgStatusPay' class='txtYellow txtBold'></div>
									</td>
									<td width='4%'><label id="lblPNr"><b i18n_key="PassengerNameRecord">PNR : </b></label>										
									</td>
									<td width='15%'>										
										<div id='divPnrNoPay' class='txtYellow txtBold'></div>
									</td>
									<td align='right'>
										<div id='divAgentPay' class='txtYellow txtBold'></div>
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr>
						<td align='center'>
							<table width='70%' border='0' cellpadding='1' cellspacing='2'>
								<tr>
									<td width='23%'><b i18n_key="payement_TotalFare">Total Fare</b></td>
									<td width='23%' align='right'><div id='divPTotFare'></div></td>
									<td width='8%'>&nbsp;</td>
									<td width='23%'><b i18n_key="payement_TicketPrice">Ticket Price</b></td>
									<td width='23%' align='right'><div id='divPTktPrice'></div></td>
								</tr>
													
								<tr>
									<td><b i18n_key="payement_TotalTaxesCharges">Total Taxes/Charges</b></td>
									<td align='right'><div id='divPTotTaxSur'></div></td>
									<td>&nbsp;</td>
									<td><b i18n_key="payement_Paid">Paid</b></td>
									<td align='right'><div id='divPTotPaid'></div></td>
								</tr>
							
								<tr>
									<td><b i18n_key="payement_CNXMODCharge">CNX/MOD Charge</b></td>
									<td align='right'><div id='divPModChg'></div></td>
									<td>&nbsp;</td>
									<td><b><label id='lblPayTotDisc' i18n_key="payement_PROMOTIONDISCOUNT">Fare Discount</label></b></td>
									<td align='right'><div id='divPFareDiscount'></div></td>
								</tr>
								<tr class='serviceTax'>
									<td><b i18n_key="<c:out value='${requestScope.serviceTaxLabel}' escapeXml='false'/>">Service Tax</b></td>
								    <td align='right'><div id='divServiceTax'></div></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>							
								<tr>
									<td><b i18n_key="payement_HandlingCharge">Handling Charge</b></td>
									<td align='right'><div id='divPHandChg'></div></td>
									<td>&nbsp;</td>
									<td><label id="lblEntComm"><b i18n_key="payement_EntitledCommision">Entitled Commission</b></label></td>
									<td align='right'><div id='divPAgentCommission'></div></td>
								</tr>
								<tr class='pendingComm'>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><label id="lblPenComm"><b i18n_key="payement_PendingCommision">Pending Commission</b></label></td>
									<td align='right'><div id='divPendingAgentCommission'></div></td>
								</tr>
								<tr>
									<td><b i18n_key="payement_BalanceToPay">Balance to Pay</b></td>
									<td align='right'><div id='divPBalToPay'></div></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td  class='singleGap'></td>
		</tr>
		<tr>
			<td class="ui-widget ui-widget-content ui-corner-all" style='height:190px;' valign='top'>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td class='paneHD'>
							<p class='txtWhite txtBold' i18n_key="payement_PassengerPaymentDetails">Passenger Payment Details</p>
						</td>
					</tr>
					<tr>
						<td valign='top'>
							<div id="divCheckAll" class='txtBold'>
								<input type="checkbox" id="chkAllPax" name="chkAllPax" checked="checked"> <font i18n_key="payement_SelectUnselectPassengers">Select/Un Select All Passengers </font>
							</div>
						</td>
					</tr>
					<tr>
						<td style='height:190px;' valign='top'>
							<table id="tblPaxPayDet" class="scroll" cellpadding="0" cellspacing="0"></table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="1" cellspacing="0">
								<tr>
									<td>
										<table>
											<tr>
												<td i18n_key="payement_BalanceToPayRemainingPassengers">
													Balance to pay for the remaining passengers : 
												</td>
												<td>
													<div id="divBaltoPay" class='txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
									<td align="right">
										<font i18n_key="payement_AmountPaidToSelectedPassengers">Amount paid for the selected passengers :</font>
										<input type="text" id="txtPayAmt" name="payment.amount" maxlength="20"  style='width:80px;' class="aa-input rightAlign txtBold"/>
									</td>
									<td>
										<div id='divPayCurr' class='txtBold'></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td  class='singleGap'></td>
		</tr>
		<u:hasPrivilege privilegeId="xbe.validate.groupbookingreference">
		<tr id="rowGroupBookingReference">
			<td>
			<table width="100%">
			  <tr>
				<td width="15%">  
				<label i18n_key="payement_GroupBookingReference"> Group Booking Reference : </label><input type='text' id='groupBookingReference' class="aa-input" name='payment.groupBookingReference' value='' tabindex='6' maxlength="10" />
				<button id="btnGrpBookingValidate" type="button" class="btnMargin" tabindex="12" i18n_key="btn_Validate">Validate</button>
				<input type="hidden" name="minimum" id="minimumPayment" value="0">
				</td>
				<td  width="15%">
				<span id="grpBookingVal" />
				</td>	
			  </tr>
			</table>
			</td>				
		</tr>
		</u:hasPrivilege>
		<tr>
			<td  class='singleGap'></td>
		</tr>
		<u:hasPrivilege privilegeId="xbe.res.make.pay.voucher">
		<tr id="rowVoucherPayment">
			<td class="ui-widget ui-widget-content ui-corner-all" style='word-wrap:break-word;' valign='top'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class='paneHD' colspan="4">
							<p class='txtWhite txtBold'>Pay With Voucher</p>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr>
						<td valign="top" width="30%">
							Pay with Gift Voucher / Voucher 
						</td>
						<td width="70%">
							<input type="checkbox" id="chkVoucherRedeem">
						</td>
					</tr>
					<tr id="trVoucher">
						<td valign="top" width="30%">
							Insert voucher IDs to redeem
						</td>
						<td width="70%">
							<table id="tblVoucherRedeem">
								<tr>
									<td>
										<table id="tblVoucherIDs">
											<tr>
												<td><input type="text" id="voucherID_0"></td>
												<!-- <td valign="top">
													<input type="button" name="btnRemoveVoucher" style="width:25px" class="Button" id="btnRemoveVoucher_0" value="-" onclick="UI_tabPayment.removeVoucher(this.id)"/>
													<input type="button" name="btnAddVoucher" style="width:25px" class="Button" id="btnAddVoucher_0" value="+" onclick="UI_tabPayment.addVoucher(this.id)"/>
												</td> -->
											</tr>
										</table>
									</td>
								</tr>
								<tr id="trVoucherRedeem">
									<td colspan="2" align="right">
										<input type="button" id="btnRedeem" value="Redeem"/>
									</td>
								</tr>
							</table>
						</td>					
					</tr>
					<!-- Otp Implementation CR Gift Voucher  -->
					<tr id="trVoucherOtp">
						<td valign="top" width="30%" i18n_key="lbl_otp_authentication">
							An authentication code has been sent to the voucher registered mobile number. Please use this to redeem your vouchers
						</td>
						<td width="70%">
							<table id="tblVoucherOtp">
								<tr>
									<td><input type="text" id="voucherOtpId"></td>
								</tr>
								<tr id="trVoucherOtpBtnId">
									<td colspan="4" align="right">
										<input type="button" id="btnVoucherOtpConfirm" i18n_key="btn_otp_confirm" value="Confirm"/>
										<input type="button" id="btnVoucherOtpResend" i18n_key="btn_otp_resend" value="Resend" style="margin-left:5px;"/>
									</td>
								</tr>
							</table>
						</td>					
					</tr>
					<tr id="trOtpConfirmation"  style="display: none">
						<td i18n_key="lbl_redemption">	
							&nbsp;Redemption successful for voucher id : 
						</td>
						<td>
							<div id='divVoucherRedemptionId' class='txtBold'></div>
						</td>
						<td colspan="2">
							<input type="button" id="btnUseAnotherVoucher" i18n_key="btn_use_another_voucher" value="Use another voucher"/>
						</td>
					</tr>
				</table>
			</td>				
		</tr>
		<tr>
			<td  class='singleGap'></td>
		</tr>
		</u:hasPrivilege>	
		<tr id="payOptions">
			<td class="ui-widget ui-widget-content ui-corner-all" style='height:200px;' valign='top'>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td class='paneHD'>
							<p class='txtWhite txtBold' i18n_key="payement_Option">Payment Option</p>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
							     <u:hasPrivilege  privilegeId="xbe.res.make.pay.cash">
									<tr id="rowCashPayments">
										<td width='15%'>  
											<input type='radio' id='radPaymentTypeC' name='payment.type' value='CASH' tabindex='1'/><label for="CASH" i18n_key="payement_Cash">Cash</label>
										</td>
										<td>
										&nbsp;
										<input type='text' id='paymentCash' class="aa-input" name='payment.paymentCach' value='' tabindex='5' maxlength="15" />	
										<label id="lblReferenceC" style="display:none;" i18n_key="payement_Reference">Reference</label>&nbsp;						
										<select  id='selActualPayMethodC' class="aa-input" name='payment.actualPaymentMethod' value='' style="display:none;" tabindex='5' maxlength="15"/>		
										<input type='text' id='paymentReferenceC' class="aa-input" name='payment.paymentReference' value='' tabindex='5' maxlength="15" style="display:none"/>
										<font id="referenceMandatoryC" class="mandatory" style="display:none;">*</font>
										</td>
									</tr>
								 </u:hasPrivilege>
								 <u:hasPrivilege  privilegeId="xbe.res.make.pay.offline.payment">
								 	<tr id="rowOfflinePayments">
										<td width='15%'>  
											<input type='radio' id='radPaymentTypeOffline' name='payment.type' value='OFFLINE' tabindex='4'/><label for="OFFLINE" i18n_key="payement_Offline">Offline Payment</label>
										</td>
										<td width ='40%' style="padding-top: 15px !important;">
											<table>
												<select id='selPaymentGateway' name='payment.pgw' class="aa-input" style="width:220px;display:none;" tabindex="5"></select>
											</table>
											&nbsp;
										</td>
										<td width ='40%' id="offlineEmailAddressContainer" style="display : none;" >
											<label id="contactEmailLabelOfflinePayments"  style="width:20%;" >Email Address</label>
											<input type="text" id="contactEmailForOfflinePayments" style ="width:50%;" />
											<span id="offlineEmailMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
										</td>
									</tr>
								 </u:hasPrivilege>
								 <u:hasPrivilege  privilegeId="xbe.res.make.pay.card">
									<tr id="rowCardPayments">
										<td width='15%'>
											<input type='radio' id='radPaymentTypeR' name='payment.type' value='CRED' tabindex='2'/><label for="CRED" i18n_key="payement_CreditCard">Credit Card</label>
										</td>
										<td>
										<table>
											<select id='selCardType' name='payment.cardType' class="aa-input" style="width:200px;display:none;" tabindex="5"></select>
											<input type='text' id='paymentCreditCard' class="aa-input" name='payment.ccAmount' value='' tabindex='5' maxlength="15" />	
											</table>
											&nbsp;	
										<label id="lblReferenceR" style="display:none;" i18n_key="payement_Reference">Reference</label>&nbsp;		
											<select  id='selActualPayMethodR' class="aa-input" name='payment.actualPaymentMethod' value='' style="display:none;" tabindex='5' maxlength="15"/>	
										
										
										<input type='text' id='paymentReferenceR' class="aa-input" name='payment.paymentReference' value='' tabindex='5' maxlength="15" style="display:none"/>
										<font id="referenceMandatoryR" class="mandatory" style="display:none;" i18n_key="">*</font>
										</td>
										
									</tr>
								</u:hasPrivilege>
								<u:hasPrivilege  privilegeId="xbe.res.make.pay.acc">
									<tr id="rowAccPayments">
										<td width='15%'>  
											<input type='radio' id='radPaymentTypeA' name='payment.type' value='ACCO' tabindex='3' checked/><label for="ACCO" i18n_key="payment_OnAccount">On Account</label>
										</td>
										<td>
											<select id='selAgents' name='selAgents' class="aa-input" style="width:250px;" tabindex='4'>
												<option value=""></option>
												<c:out value="${requestScope.reqPayAgents }" escapeXml="false" />
											</select>
											&nbsp;	
											<input type='text' id='paymentAgentA' class="aa-input" name='payment.agentOneAmount' value='' tabindex='5' maxlength="15" /><font id="referenceMandatoryA" class="mandatory" style="display:none;">*</font>
											<label id="lblReferenceA" style="display:none;" i18n_key="payement_Reference">Reference</label>&nbsp;		
											<select  id='selActualPayMethodA' class="aa-input" name='payment.actualPaymentMethod' value='' tabindex='5' style="display:none;" maxlength="15"/>		
										<input type='text' id='paymentReferenceA' class="aa-input" name='payment.paymentReference' value='' tabindex='5' maxlength="15" style="display:none"/>
										<font id="referenceMandatoryA" class="mandatory" style="display:none;">*</font>
										
										</td>
										
									</tr>
									<tr id="rowAccPayments2">
										<td width='15%'>  
											&nbsp;
										</td>
										<td>
											<select id='selAgents2' name='payment.agentTwo' class="aa-input" style="width:250px;" tabindex='4'>
												<option value=""></option>
												<c:out value="${requestScope.reqPayAgents }" escapeXml="false" />
											</select>
											&nbsp;	
											<input type='text' id='paymentAgentB' class="aa-input" name='payment.agentTwoAmount' value='' tabindex='5' maxlength="15" /><font id="referenceMandatoryA" class="mandatory" style="display:none;">*</font>
												
											
										</td>
										
									</tr>
									<tr>
										<td width='15%'>  
											&nbsp;
										</td>
										<td width='75%' colspan="2">			
										<label id="lblAgentCurrency"></label>								
										</td>
									</tr>
									
								  </u:hasPrivilege>
								  <u:hasPrivilege  privilegeId="xbe.res.make.pay.bsp">
									<tr id="rowBspPayments">
										<td width='15%'>  
											<input type='radio' id='radPaymentTypeBsp' name='payment.type' value='BSP' tabindex='3' checked/><label for="BSP" i18n_key="payement_BSP"> BSP </label>
										</td>
										<td>
										&nbsp;	
										<label id="lblReferenceBsp" style="display:none;" i18n_key="payement_Reference">Reference</label>&nbsp;						
										<select  id='selActualPayMethodBsp' class="aa-input" name='payment.actualPaymentMethod' value='' style="display:none;" tabindex='5' maxlength="15"/>		
										<input type='text' id='paymentReferenceBsp' class="aa-input" name='payment.paymentReference' value='' tabindex='5' maxlength="15" style="display:none"/>
										<font id="referenceMandatoryBsp" class="mandatory" style="display:none;">*</font>
										</td>									
									</tr>
									<tr height="15px"></tr>
									<tr style="display: none" id="rowBSPFee">
										<td>&nbsp;BSP Fee</td>
										<td><label id="BSPFee"></label></td>
									</tr>									
							    </u:hasPrivilege>
								<tr>
									<td style='height:25px;' valign='bottom' i18n_key="payement_Total" >
										&nbsp;Total :
									</td>
									<td valign='bottom'>
										<div id='divPTotAmount' class='txtBold'></div>
										<div id='divCCurrTotalBSP' class='txtBold'></div>
									</td>
								</tr>
								<tr id="trVoucherRedemption"  style="display: none">
									<td>	
										&nbsp;Total after redeem :
									</td>
									<td>
										<div id='divAmountAfterRedeem' class='txtBold'></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="creditCardDet">
			<td align='center'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='paneHD' colspan='3'>
										<p class='txtWhite txtBold' i18n_key="payement_CreditCardDetails">Credit Card Details</p>
									</td>
								</tr>
								<tr>
									<td class='doubleGap' colspan='3'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<div style="height:175px;overflow-y:auto;">
										<table width='100%' border='0' cellpadding='2' cellspacing='0'>
										<tr>
											<td width='45%' valign='top'>
												<table width='100%' border='0' cellpadding='2' cellspacing='0' style='text-align:left;'>
													<tr>
														<td width='30%' i18n_key="payement_CardType">Card Type : </td>
														<td width='70%' class='noPadding'>
															<table>
																<tr>
																	<td>
																		<select id="cardType" name="creditInfo.cardType" size="1" tabindex="1" style="width:100px; display: inline;" class='aa-input'>
																		</select>
																	</td>
																	<td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td i18n_key="payement_CardNumber">Card Number :</td>
														<td>
															<input type="text" id="cardNo" name="creditInfo.cardNo" tabindex="2" maxlength="20" size="20" class="aa-input txtBold"/>
															<span id="reloadPromotion" style="display:none">
																<button id="reloadPromotionsBtn" type="button" class="btnMargin" tabindex="6" style="width: 120px">Check for Promotions</button>
															</span>
														</td>
														
													</tr>
													<tr>
														<td i18n_key="payement_CardHoldersName">Card Holders Name :</td>
														<td>
															<input type="text" id="cardHoldersName" name="creditInfo.cardHoldersName" tabindex="3" maxlength="30" size="30" class="aa-input"/>
														</td>
													</tr>
													<tr>
														<td i18n_key="payement_ExpiryDate">Expiry Date :</td>
														<td class='noPadding'>
															<table>
																<tr>
																	<td>
																		<input type="text" id="expiryDate" name="creditInfo.expiryDate" tabindex="4" maxlength="4" size="4" class="aa-input"/>
																	</td>
																	<td>
																		<p class='txtBold'>(MMYY)</p>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td i18n_key="payement_CardCvv">Card's CVV :</td>
														<td>
															<input type="text" id="cardCVV" name="creditInfo.cardCVV" tabindex="5" maxlength="4" size="4" class="aa-input"/>
														</td>
													</tr>
													
													<tr id="extraCardDetailForPayPalPannel" style="display:none">
														<td colspan="2">
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																
																<tr>
														            <td width='40%' i18n_key="payement_BillingAddress1">Billing Address1 </td>
														            <td>
															            <input type="text" id="billingAddress1" name="creditInfo.billingAddress1" tabindex="6" maxlength="30" size="30" class="aa-input"/>
														            </td>
													            </tr>
													            <tr>
														            <td width='40%' i18n_key="payement_BillingAddress2">Billing Address2 </td>
														            <td>
															            <input type="text" id="billingAddress2" name="creditInfo.billingAddress2" tabindex="7" maxlength="30" size="30" class="aa-input"/>
														            </td>
													            </tr>
													            <tr>
														        <td width='40%' i18n_key="payement_City">City </td>
														            <td>
															            <input type="text" id="city" name="creditInfo.city" tabindex="8" maxlength="30" size="30" class="aa-input"/>
														            </td>
													           </tr>
													           <tr>
														            <td width='40%' i18n_key="payement_PostalCode">Postal Code </td>
														            <td>
															            <input type="text" id="postalCode" name="creditInfo.postalCode" tabindex="9" maxlength="30" size="30" class="aa-input"/>
														            </td>
													           </tr>
													           <tr>
														            <td width='40%' i18n_key="payement_State">State </td>
														            <td>
															            <input type="text" id="state" name="creditInfo.state" tabindex="10" maxlength="30" size="30" class="aa-input"/>
														            </td>
													           </tr>
																
																
															</table>
														</td>
													</tr>
													
													<tr>
														<td i18n_key="payement_TransactionFee">Transaction Fee:</td>
														<td>
															<div id='divTxnFee' class='txtBold'></div>
														</td>
													</tr>
													
													<tr>
														<td i18n_key="payement_TotalAmount">Total Amount:</td>
														<td>
															<div id='divTotPay' class='txtBold'></div>
														</td>
													</tr>
													<tr>
														<td colspan='2'>
															<table>
																<tr>
																	<td i18n_key="payement_YourPaymentWouldBeProcessedIn">
																		Your Payment would be processed in 
																	</td>
																	<td>
																		<div id='divTxtCurr' class='txtBold txtRed'></div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
											<td width='15%' align='center'>
												<a href="#" onclick='UI_tabPayment.showVeriSign(); return false;'><img src="../images/VSign_no_cache.gif" border="0"/></a>
											</td>
											<td width='40%'>
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td><div id='divCCHD' class='txtRed' i18n_key="payement_ApplytoAllCreditCardPayments">Apply to All Credit Card Payments</div></td>
													</tr>
													<tr>
														<td class='doubleGap'></td>
													</tr>
													<tr>
														<td>
															<div id='divCCMsg'></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										</table>
										</div>
									</td>
								</tr>
								<tr>
									<td class='doubleGap' colspan='3'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<table width="100%" border="0" cellpadding="0" cellspacing="1">
											<!-- tr>
												<td style="width:20px" valign="bottom"><img id="imgError" name="imgError" src="../images/spacer_no_cache.gif" border="0"/></td>
												<td id="tdSTBar"><div id="divError">&nbsp;</div></td>
											</tr -->
										</table>
									</td>
									<td align='right'>
										<button id="btnCRConfirm" type="button" class="btnMargin" tabindex="6" i18n_key="btn_Confirm">Confirm</button>
										<button id="btnCRReset" type="button" class="btnMargin" tabindex="7" i18n_key="btn_Reset">Reset</button>
										<button id="btnCRCancel" type="button" class="btnMargin" tabindex="8" i18n_key="btn_Cancel">Cancel</button>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td  class='singleGap'></td>
		</tr>
		<tr id="payOptButtons">
			<td>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td>
							<table border='0' cellpadding='1' cellspacing='0'>
								<tr id="trEmail">
									<td >
										<select id='selLang' name='selLang' class="aa-input" style="width:100px;" tabindex="6"></select>
									</td>
									<td class='tdEmail' i18n_key="payement_EmailItinerary">
										Email Itinerary
									</td>
									<td class='tdEmail'>
										<input type='checkbox' id='chkEmail' name='chkEmail' tabindex="7"/>
									</td>
									<td i18n_key="payement_PrintItinerary">
										Print Itinerary
									</td>
									<td>
										<input type='checkbox' id='chkPrint' name='chkPrint'tabindex="8" />
									</td>
									<!-- 
									<u:hasPrivilege  privilegeId="xbe.res.itn.chg">
									<td>
										Print Ticket Chg
									</td>
									<td>
										<input type='checkbox' id='chkPayPaxChg' name='chkPayPaxChg' />
									</td>					
									</u:hasPrivilege>				
									 -->
								</tr>
							</table>
						</td>
						<td align='right'>
				
								<button id="btnPayConfirm" type="button" class="btnMargin" tabindex="9"  i18n_key="btn_Confirm">Confirm</button>
								<button id="btnPayForce" type="button" class="btnMargin btnLarge" tabindex="10" i18n_key="btn_ForceConfirm">Force Confirm</button>
								<button id="btnPayContinue" type="button" class="btnMargin btnLarge" tabindex="11" i18n_key="btn_PaynConfirm">Pay &amp; Confirm</button>
								<button id="btnPayReset" type="button" class="btnMargin" tabindex="12" i18n_key="btn_Reset">Reset</button>
								<button id="btnPayCancel" type="button" class="btnMargin" tabindex="13" i18n_key="btn_Cancel">Cancel</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input type="hidden" id="txtPaymentWithCharge" name="payment.amountWithCharge" value="0"> 
	<input type="hidden" id="totalPayAmountWithBSPFee" value="0">
	<input type="hidden" id="txtTxnCharge" value="0">
	<input type="hidden" id="txtPayMode" name="payment.mode">
	<input type="hidden" id="hdnIPGId" name="hdnIPGId" value="0">
	<input type="hidden" id="hdnIPGDesc" name="hdnIPGDesc">
	<input type="hidden" id="hdnSysDate" name="hdnSysDate">
	<input type="hidden" id="hdnOrigin" name="hdnOrigin">
	<input type="hidden" id="hdnDestination" name="hdnDestination">
	<input type="hidden" id="hdnSelCurrency" name="hdnSelCurrency">
	<input type="hidden" id="hdnPrintItineraryLang" name="hdnPrintItineraryLang">
	<input type="hidden" id="sendEmail" name="sendEmail">
	<input type="hidden" id="hdnVoucherRedemption" name="hdnVoucherRedemption">
	<input type="hidden" id="hdnVoucherRedeemAmount" name="hdnVoucherRedeemAmount">
	<div id="divDisableLayer" style="position:absolute;top:0;margin-left:-15px;width:940px;height:400px;background-color:white;display:none;z-index:2" class="fulltransParent">
		<iframe src="showBlank" id='frmDisableLayer' name='frmDisableLayer'  frameborder='0' scrolling='no' style='width:100%; height:400px;' class="fulltransParent"></iframe>
	</div>
	<div id="reloadPromotions" style="display: none;" title="Please try with the promo code">
		<table  align="center">
			<tr>
				<td><label i18n_key="">Promo Code: </label></td>
				<td><input type="text" id="promoCodeInPaymentPage" name="promoCodeInPaymentPage"></td>
				<td id="promocodeEmpty" style="display: none;" class="txtRed" i18n_key="">Enter promocode</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="2" align="left"><button name="button" id="pickPromotion" i18n_key="">Pick Promotions</button></td>
			</tr>
		</table>
	</div>
	<div id="promoSelection" style="display: none;" title="Please Select required promotion">
		<table  align="center">
			<tr>
				<td><label i18n_key="">Already applied discount: </label></td>
				<td id="alreadyAppliedDiscount"></td>
			</tr>
			<tr>
				<td><label i18n_key="">Discount for credit card: </label></td>
				<td id="binPromotionDiscount"></td>
			</tr>
			<tr>
				<td  align="left"><button name="button" id="existingPromotionBtn" i18n_key="btn_UseAppliedPromotion">Use Applied Promotion</button></td>
				<td  align="left"><button name="button" id="binPromotionBtn" i18n_key="btn_CreditCardPropmotions">Credit Card Promotion</button></td>
			</tr>
		</table>
	</div>
	</div>
	</form>	
	<%@ include file='../modify/confirmAutoCancellation.jsp' %>
<script type="text/javascript">
	$("#mainTablePayment").getLanguage();
</script>
	