<!-- %@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

</body>
</html-->
<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link href="../css/loadCSS_no_cache.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css" />
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<%-- 	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
<script
	src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script
	src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script
	src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/v2/reservation/blacklistedPaxReservation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<style>
#spnError div.errorBoxHttps {
	padding: 2px 9px 0px 8px;
	background: #F3F376;
	-moz-border-radius: 4px;
}
</style>
<script type="text/javascript">
	var arrError = new Array();
	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
</script>

</head>
<body style="background: rgb(255, 255, 255);"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown="return Body_onKeyDown(event)">
	<!--<body style="background: transparent;"> -->
	<%@ include file="../../common/IncludeTop.jsp"%>
	<div
		style="overflow: auto; height: 650px; width: 970px; margin: 0 auto"
		id="blacklistedPaxContainer">
		<!-- Page Background Top page -->
		<table width="100%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				 <td><!-- < %@ include file="../../common/IncludeMandatoryText.jsp"%> --></td>
			</tr>
			<tr>
				<td><%@ include file="../../common/IncludeFormTop.jsp"%>
					Search Blacklisted Passengers <%@ include
						file="../../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="2" cellspacing="2">

						<tr>
							<td><font>Passenger Name</font></td>
							<td colspan="3"><input name="paxName" type="text" id="paxName"	size="46" maxlength="100"/></td>	
							<td><font>Passport No</font></td>
							<td><input name="passportNo" type="text" id="passportNo"
								size="40" maxlength="60" style="width: 120px;" /></td>
							<td width="12%">Status</td>
								<td><select name="blResStatus" id="blResStatus" style="width:110px;">
										<option selected="selected" value="N">Blacklisted</option>
										<option value="Y">Whitelisted</option>
								</select></td>
						</tr>

						<tr>
							<td><font>Nationality</font></td>
							<td><select name="nationality" id="nationality"
								style="width: 120px;">
									<option value="0">All</option>
									<c:out value="${requestScope.nationalityList}"
										escapeXml="false" />
							</select></td>
							<td><font>Date Of Birth</font></td>
							<td><input name="dateOfBirth" type="text" id="dateOfBirth"
								size="40" maxlength="60" readonly="readonly"
								style="width: 120px;" /></td>
							<td><font>PNR</font></td>
							<td><input name="pnr" type="text" id="pnr" size="20"
								maxlength="60" style="width: 120px;" /></td>
							<td colspan="2"></td>
						</tr>


						<tr>
							<td><font>Blacklist Type</font></td>
							<td><select name="blacklistType" id="blacklistType"
								style="width: 120px;">
									<option value="T">Tempory</option>
									<option value="P">Permanent</option>
							</select></td>
							<td><font>From Date</font></td>
							<td><input name="fromDate" type="text" id="fromDate"
								size="20" maxlength="60" readonly="readonly"
								style="width: 120px;" /></td>
							<td><font>To Date</font></td>
							<td><input name="toDate" type="text" id="toDate" size="20"
								maxlength="60" readonly="readonly" style="width: 120px;" /></td>
							<td></td>
							<td><input name="searchBlacklistPax" type="button"
								class="Button" id="searchBlacklistPax" value="Search" /></td>

						</tr>


					</table> <%@ include file="../../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../../common/IncludeFormTop.jsp"%>Blacklisted
					Passenger Queue<%@ include file="../../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="4">
						<tr>
							<td id="blacklistedPaxGridContainer"></td>
						</tr>
						<tr>
						<td><font class="fntSmall"> <font class="mandatory"><b>*</b></font>Click on PNR to load the Reservation</font></td>
						</tr>

					</table> <%@ include file="../../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../../common/IncludeFormTop.jsp"%>
					Set Status &nbsp;&nbsp;<%@ include
						file="../../common/IncludeFormHD.jsp"%>


					<table width="100%">
						<u:hasPrivilege privilegeId="pax.blacklist.res.action">
							<tr id="trButtons">
								<td width="12%">Status</td>
								<td><select name="isActioned" id="isActioned"  style="width:110px;">
										<option value="N">Blacklisted</option>
										<option value="Y">Whitelisted</option>
								</select></td>
								<td>Reson for Whitelist</td>
								<td colspan="3">
								<textarea name="reasonForWhitelisted" id="reasonForWhitelisted" rows="2" cols="50"></textarea>								
								</td>
								<td style="height: 15px;" valign="bottom" align="right"
									colspan="2"><input name="btnSave" type="button"
									class="Button" id="btnSave" value="Save" /> <input
									name="btnCancel" type="button" class="Button" id="btnCancel"
									value="Cancel" /></td>
							</tr>
						</u:hasPrivilege>
						<tr>
							<td><font class="fntSmall">&nbsp;</font></td>
						</tr>
						<tr>
							<td>
								<div id='divLoadMsg'
									style='visibility: hidden; z-index: 1000; background: transparent;'>
									<iframe src="showFile!loadMsg.action" id='frmLoadMsg'
										name='Voucher.frmLoadMsg' frameborder='0' scrolling='no'
										style='position: absolute; top: 50%; left: 40%; width: 260; height: 40;'></iframe>
								</div>
							</td>
						</tr>

					</table> <%@ include file="../../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td align="right">
					<button name="btnClose" id="btnClose" class="Button">Close</button>
				</td>
			</tr>
		</table>
<!-- showNewFile!searchRes.action -->
<!-- showNewFile!loadRes.action -->
		<form method='post' action="showNewFile!loadRes.action" id="frmRes">
			<input type='hidden' id='pnrNO' name='pnrNO' value="" /> <input
				type='hidden' id='groupPNRNO' name='groupPNRNO' value="" /> <input
				type='hidden' id='marketingAirlineCode' name='marketingAirlineCode'
				value="" /> <input type='hidden' id='interlineAgreementId'
				name='interlineAgreementId' value="" /> <input type='hidden'
				id='airlineCode' name='airlineCode' value="" />
		</form>

		<script type="text/javascript">
			$(function() {
				$("#tabs").tabs();
				$("#lstRoles, #lstAssignedRoles").css("width", "335px");
				$("#lstRoles, #lstAssignedRoles").css("height", "150px");
			});
		</script>
	</div>
	<%@ include file="../../common/IncludeBottom.jsp"%>
</body>
</html>