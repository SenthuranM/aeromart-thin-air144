
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td style='height: 560px;' valign="top">
			<div id="trAllONDs" style="display: none;">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td style="height: 200px; width: 607px" valign="top">						
							<div style="height: 200px; width: 607px; margin-bottom: 2px;"
								class="ui-widget ui-widget-content ui-corner-all">
								<table id="tblAllONDs"  style="width: 650px" class="scroll" cellpadding="0"
									cellspacing="0"></table>
								<div class='singleGap'></div>
								<div align="right"><button id="btnAddNewOND" type="button" class="btnMargin btnMedium"
									tabindex="20" i18n_key="btn_AddNewOriginAndDestination">Add New OND</button>
									<button id="btnAddGroundSegment" type="button" class="btnMargin btnMedium" 
									tabindex="20"i18n_key="btn_AddBusSegment">Add Bus Segment</button>
									<!-- <button id="btnAddGroundSegment" type="button" class="btnMargin ui-state-default ui-corner-all" style="width:auto;" disabled="disabled">Add Bus Segment</button> -->
									</div>
							</div>
						</td>
						<td class='singleGap'>&nbsp;</td>
						   
						<td style=' width: 263px;' valign='top'>
									<div id="tdRQLogicalCCList" style="vertical-align: top;"></div>
									</td>
									
						<!--    
						<td style="vertical-align: bottom;">
							<div id="divRQLogicalCCList"  style='position:absolute;top:200px;left:325px;width:300px;z-index:1002;display: block;' 
								class="popupXBE ui-corner-all">
								<table width='100%' border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
											<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td class='paneHD'>
														<p class='txtWhite txtBold'> &nbsp;OND Fare Options</p>
													</td>
												</tr>
												<tr>
													<td  class='popBGColor singleGap'></td>
												</tr>
												<tr>
													<td class='popBGColor'>
														<div id="tdRQLogicalCCList" style="vertical-align: top;"></div>
														<br/>
														<div align="right"><button id="btnLogicalOptionClose" type="button" class="btnMargin ui-state-default ui-corner-all">Close</button></div>	
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>								
							</div>
						</td>
						 -->
					</tr>
				</table>
			</div>
			<div id="tdSearchFltArea" style="width:99%">
			<div id="trSearchParams"
				class="ui-widget ui-widget-content ui-corner-all">
				<form method='post' action="availabilitySearch.action"
					id="frmSrchFlt">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'
						align='center'>
						<!--<tr>
										<td align="left" colspan='11' >				
											<div id='airportMsg' class="spMsg" style='padding:7px;padding-left:20px;position:absolute;width:893px;z-index:50;' onclick='UI_tabSearchFlights.hideAirportMessage()'></div>
										</td>
									</tr>
									-->
						<tr>
							<td colspan='11' class='singleGap'></td>
						</tr>
						<tr>
							<td style="min-width: 35px" i18n_key="availability_From">From:</td>
							<td style="min-width: 170px">
								<table border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td><input type="text" id="fAirport" class="aa-input"
											style="width: 150px" tabindex="1" /></td>
										<td><%@ include file='../common/inc_MandatoryField.jsp'%>
										</td>
									</tr>
								</table>
							</td>
							<td style="min-width: 80px" i18n_key="availability_DepartureDate">Departure Date:</td>
							<td style="min-width: 100px"><input id="departureDate"
								name="searchParams.departureDate" type="text" class="aa-input"
								style="width: 80px" maxlength="10" tabindex="3" /><%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
							<td style="min-width: 25px"><input id="departureVariance"
								name="searchParams.departureVariance" type="text"
								class="aa-input rightAlign" style="width: 22px;" maxlength="1"
								tabindex="4" /></td>
							<td style="min-width: 70px" i18n_key="availability_Days">+/- Days</td>
							<td style="min-width: 45px" i18n_key="availability_Adults">Adults:</td>
							<td style="min-width: 45px"><input id="adultCount"
								name="searchParams.adultCount" type="text"
								class="aa-input rightAlign" style="width: 22px;" maxlength="3"
								tabindex="9" /><%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
							<td style="min-width: 60px" i18n_key="availability_Class">Class:</td>
							<td><select id="classOfService" name="classOfService"
								class="aa-input" style="width: 80px" tabindex="12"></select><%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
							<td align="right" valign="bottom" rowspan='4'>
								<u:hasPrivilege privilegeId="xbe.res.exclude.charges">
								<div>
									<button id="btnExcludeCharge" type="button" title="Exclude Charges"
										style="margin: 1px; width: 60px" tabindex="18">Excl Chg</button>							
								</div>
								</u:hasPrivilege>
								<button id="btnSearch" type="button"
									style="margin: 1px; width: 60px" tabindex="19" i18n_key="btn_Search">Search</button>
							</td>
						</tr>
						<tr>
							<td i18n_key="availability_To">To:</td>
							<td>
								<table border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td><input type="text" id="tAirport" class="aa-input"
											style="width: 150px" tabindex="2" /></td>
										<td><%@ include file='../common/inc_MandatoryField.jsp'%>
										</td>
									</tr>
								</table>
							</td>
							<td><span id="spnRetDate" i18n_key="availability_ReturnDate">Return Date:</span></td>
							<td><input id="returnDate" name="searchParams.returnDate"
								type="text" class="aa-input" style="width: 80px" maxlength="10"
								tabindex="5" /></td>
							<td><input id="returnVariance"
								name="searchParams.returnVariance" type="text"
								class="aa-input rightAlign" style="width: 22px;" maxlength="1"
								tabindex="6" /></td>
							<td><span id='spnVarianceDaysTxt' i18n_key="availability_Days" >+/- Days</span></td>
							<td i18n_key="availability_Children">Children:</td>
							<td><input id="childCount" name="searchParams.childCount"
								type="text" class="aa-input"
								style="width: 22px; text-align: right;" maxlength="3"
								tabindex="10" /></td>
							<td i18n_key="availability_Currency">Currency:</td>
							<td><select id="selectedCurrency"
								name="searchParams.selectedCurrency" class="aa-input"
								style="width: 80px" tabindex="13"></select><%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
						</tr>
						<tr id="trCheckStopOver" style="display: none">
							<td></td>
							<td colspan="8">
								<button type="button" id="chkStopOverAdd" id="chkStopOverAdd"></button>
								<div id="stopOverTimePopup" style='position: absolute; width: 350px; z-index: 1001; display: none;' class="popupXBE ui-corner-all popBGColor popupXBEborder">
									<div class="paneHD"><p class="txtWhite txtBold paddingTD" >Set Stop Over Time</p><span id="iconStopOverClose" class="stopOverPopupClose" ></span></div>
									<div class="stopOverTimeInner paddingTD" >
									</div>
									<div class="paddingTD" style="text-align: center;">
										<input type="button" value="Cancel" id="btnCanStopOver" class="btnMargin btnMedium"/>
										<input type="button" value="Apply" id="btnApplyStopOver" class="btnMargin btnMedium"/>
									</div>						
									</div>
							</td>
						</tr>
						<tr>
							<td i18n_key="availability_SearchOptions">Search Options</td>
							<td><select id="selSearchOptions"
								name="searchParams.searchSystem" class="aa-input" tabindex="14"
								style="width: 150px" title="Flight Search Options"></select></td>
							<td><div id='divValidity' i18n_key="availability_Validity">Validity:</div></td>
							<td><select id="validity" name="searchParams.validity"
								class="aa-input" style="width: 80px" tabindex="7"></select></td>
							<td><input id="openReturn" name="searchParams.openReturn"
								type="checkbox" class="noBorder" value="true" tabindex="8" /></td>
							<td><div id='divOpen' i18n_key="availability_Open">Open</div></td>
							<td i18n_key="availability_Infants">Infants:</td>
							<td><input id="infantCount" name="searchParams.infantCount"
								type="text" class="aa-input rightAlign" style="width: 22px;"
								maxlength="3" tabindex="11" /></td>
							<td i18n_key="availability_Type">Type:</td>
							<td><select id="bookingType" name="searchParams.bookingType"
								class="aa-input" style="width: 80px" tabindex="14"></select><%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
						</tr>
						<tr id="trPromoCode">
							<td i18n_key="availability_PromoCode">Promo Code:</td>
							<td><input id="promoCode" name="searchParams.promoCode"
								type="text" class="aa-input rightAlign" style="width: 80px;"
								maxlength="40" tabindex="16" onkeyUp="return ValidateFlagText(this,event);" onkeyPress="return ValidateFlagText(this,event);"/></td>
							<td style="display: none;" i18n_key="availability_Bin">BIN:</td>
							<td style="display: none;"><input id="bankIdNo" name="searchParams.bankIdentificationNo"
								type="text" class="aa-input" style="width: 80px" maxlength="6" 
								tabindex="17"/></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr id='trPaxFare'>
							<td><span id='spnFareType' i18n_key="availability_FareType">Fare Type:</span></td>
							<td><select id="selFareTypeOptions"
								name="searchParams.fareType" class="aa-input" tabindex=""
								style="width: 100px" title="Fare Type Options"></select></td>
							<td><span id='spnPaxType' i18n_key="availability_PaxType">Pax Type:</span></td>
							<td><select id="selPaxTypeOptions"
								name="searchParams.paxType" class="aa-input" tabindex=""
								style="width: 100px" title="Customer type"></select></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><u:hasPrivilege privilegeId="xbe.res.make.agent.fare">
									<span id='spnTAgent' i18n_key="availability_TravelAgent">Travel Agent:</span>
								</u:hasPrivilege></td>
							<td><u:hasPrivilege privilegeId="xbe.res.make.agent.fare">
									<input type="text" id="tAgent" class="aa-input"
										style="width: 150px" />
								</u:hasPrivilege></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td><span id='spnBookingClass' title="Booking Class" i18n_key="availability_BookingClass">B.
									Class</span></td>
							<td><select id="selBookingClassCode"
								name="searchParams.bookingClassCode" class="aa-input"
								tabindex="15" style="width: 80px" title="Booking Class"></select>
							</td>
						</tr>
						<tr>
							<td colspan='11' class='singleGap'></td>
						</tr>
					</table>
					<input id="firstDepature" name="searchParams.firstDeparture"
						type="hidden" /> <input id="lastArrival"
						name="searchParams.lastArrival" type="hidden" /> <input
						id="fromAirportSubStation"
						name="searchParams.fromAirportSubStation" type="hidden" /> <input
						id="toAirportSubStation" name="searchParams.toAirportSubStation"
						type="hidden" /> <input type="hidden" name="ticketExpiryDate"
						id="ticketExpiryDate" /> <input type="hidden"
						name="ticketValidFrom" id="ticketValidFrom" /> <input
						type="hidden" name="ticketExpiryEnabled" id="ticketExpiryEnabled" />
						<input id="oldPerPaxFare" name="oldPerPaxFare" type="hidden"/>
						<input id="oldPerPaxFareId" name="oldPerPaxFareId" type="hidden"/>
						
					<!-- <input id="travelAgent" name="searchParams.travelAgent" type="hidden"/> -->
						<input id="hdnBookingClassCode" name="hdnBookingClassCode" type="hidden"/>
						<input id="hdnCabinClassCode" name="hdnCabinClassCode" type="hidden"/>
					<u:hasPrivilege privilegeId="xbe.res.exclude.charges">
						<div id="excludeChargePopUp" style="display: none;">
						<%@ include file='inc_ExcludeCharge.jsp' %>
						</div>
					</u:hasPrivilege>
					
				</form>
			</div>
			</div>

			<div id="tdResultPane" style="height:460px;width:100%;overflow-x:hidden;overflow-y:auto">
			<div class='singleGap'></div>
			<div id="bundleDescSetterTempDiv" style="display: none;"> </div>
			<div id='divResultsPane' style="width:99%">
				<div id="tblIBOBFlights">
					<div id='divOutPane'>
					<div>
						<table width="620" cellpadding="1" cellspacing="0" border="0">
							<tr>
								<td style='padding: 0 2px'><a id='lnkOP' href='#'
									class='noTexDeco' tabindex="20" i18n_key="availability_PreviousDay">&lt;Previous Day</a></td>
								<td align="center"><div id='divOutDate' class='txtBold'></div></td>
								<td align='right' style='padding: 0 5px'><a id='lnkON'
									href='#' class='noTexDeco' tabindex="21" i18n_key="availability_NextDay">Next Day&gt;</a></td>
							</tr>
						</table>
					</div>
					<div>
						<table width="100%" cellpadding="1" cellspacing="0" border="0">
							<tr>
								<td style='width: 630px;' valign='top'>
									<table id="tblOutboundFlights" class="scroll" cellpadding="0"
										cellspacing="0"></table>
								</td>
								<td class='singleGap'></td>
								<td style=' width: 263px;' valign='top'
									id="tdOutBoundLogicalCabins"></td>
							</tr>
						</table>
					</div>
					</div>
					<div class='singleGap'></div>
					<div id='divRetPane'>
						<div>
							<table width="620" cellpadding="1" cellspacing="0" border="0">
								<tr>
									<td style='padding: 0 2px'><a id='lnkRP' href='#'
										class='noTexDeco' tabindex="22"  i18n_key="availability_PreviousDay">&lt;Previous Day</a></td>
									<td align="center"><div id='divRetDate' class='txtBold'></div></td>
									<td align='right' style='padding: 0 5px'><a id='lnkRN'
										href='#' class='noTexDeco' tabindex="23"  i18n_key="availability_NextDay">Next Day&gt;</a></td>
								</tr>
							</table>
						</div>
						<div>
							<table width="100%" cellpadding="1" cellspacing="0" border="0"
								align='right'>
								<tr>
									<td valign='top' style='width: 630px;'>
										<table id="tblInboundFlights" class="scroll" cellpadding="0"
											cellspacing="0"></table>
									</td>
									<td class='singleGap'></td>
									<td valign='top' style='width: 263px;'
										id="tdInBoundLogicalCabins"></td>
								</tr>
							</table>
						</div>
					</div>
					<div id='divOpenRetPane' style="height: 87px">
						<table width="100%" cellpadding="1" cellspacing="0" border="0"
							align='right'>
							<tr>
								<td colspan="3" valign='top'>
									<table id="tblOpenReturnInfo" class="scroll" cellpadding="0"
										cellspacing="0"></table>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div class='singleGap' style="clear:both"></div>

				<div id="trAddOND" style="display: none; text-align: right">
					<button id="btnAddOND" type="button" class="btnMargin btnMedium"
						tabindex="24"  i18n_key="btn_AddOnd">Add OND</button>
				</div>
				<div id="trFareSection" style=''
					class="ui-widget ui-widget-content ui-corner-all">
					<div id='divFarePane'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td class='darkBG' style='height: 20px;'>
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td>
												<div id='divFareQT' class='txtBold txtWhite'></div>
											</td>
											<td align='right'>
												<button id="btnCAllFare" type="button"
													class="btnMargin btnLarge" tabindex="25"  i18n_key="btn_ViewAllBCFare">View All BC Fares</button>
												<button id="btnRequote" type="button"
													class="btnMargin btnMedium" tabindex="26"
													style="display: none"  i18n_key="btn_Requote">Requote</button>
												<button id="btnFQ" type="button" class="btnMargin btnMedium"
													tabindex="27" i18n_key="btn_FareQuote">Fare Quote</button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><div id='divFareQuotePane'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td width='78%' valign='top'>
													<table width='100%' border='0' cellpadding='1'
														cellspacing='0'>
														<tr>
															<td>
																<table width='100%' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td>
																			
																			<button id="btnCFare" type="button"
																				class="btnMargin btnMedium" tabindex="28" i18n_key="btn_ChangeFare">Change
																				Fare</button>
																			<button id="btnTaxSurB" type="button"
																				class="btnMargin btnLarge" style="width: 200px;"
																				tabindex="29" i18n_key="btn_ShowTaxesandSurcharges">Show Taxes and Surcharges</button>
																			<button id="btnFareRulesB" type="button"
																				class="btnMargin btnMedium" tabindex="30" i18n_key="btn_FareRules">Fare
																				Rules</button>
																		</td>
																		<td align='right'>
																		<button id="btnTogglePBRKDown" type="button"
																				class="btnMargin btnMedium" tabindex="31" style="width:150px;" i18n_key="btn_PriceBreakdown">Price Breakdown</button>
																			<%-- div id='divFareTypes'>
																								<input type="radio" name="radFareType" id="radFFareF" value='FIXED'/>
																								<label for="FIXED">Fixed Fare</label>
																								<input type="radio" name="radFareType" id="radFFareS" value='STANDARD'/>
																								<label for="STANDARD">Standard Fare</label>
																							</div --%>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="thinBorderT">
																<table width='100%' border='0' cellpadding='0'
																	cellspacing='0'>
																	<tr>
																		<td>
																			<div class="PBRKDown PBRKDownHeader">
																				<table width='100%' border='0' cellpadding='1'
																					cellspacing='0'>
																					<thead class="ui-state-default">
																						<tr>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL'
																								width='15%' style='height: 21px;'></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'  i18n_key="fareQuote_PaxType"><b>Pax Type</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'  i18n_key="fareQuote_Fare"><b>Fare</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'  i18n_key="fareQuote_Tax"><b>Tax</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'  i18n_key="fareQuote_Surcharges"><b>Sur.</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='8%' align='center'  i18n_key="fareQuote_PerPax"><b>Per Pax</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='6%' align='center'  i18n_key="fareQuote_NoPax"><b>No. Pax</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='10%' align='center'  i18n_key="fareQuote_Total"><b>Total</b></td>
																							<td
																								class='thinBorderB thinBorderR gridBG thinBorderT'
																								width='10%' align='center'  i18n_key="fareQuote_TotalPrice"><b>Total
																									Price</b></td>
																							<td 
																								class='toHide thinBorderB thinBorderR gridBG thinBorderT'
																								width='7%' align='center'  i18n_key="fareQuote_FareRule"><b>Fare Rule</b></td>
																							<td 
																								class='toHide thinBorderB thinBorderR gridBG thinBorderT'
																								width='7%' align='center'  i18n_key="fareQuote_FareBC"><b>Fare BC</b></td>
																							<td 
																								class='toHide thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR'
																								width='5%' align='center'  i18n_key="fareQuote_BC"><b>BC</b></td>
																						</tr>
																					</thead>
																				</table>
																			</div>
																			<div class="PBRKDown PBRKDownBody">
																				<table width='100%' border='0' cellpadding='1'
																					cellspacing='0'>
																					<thead>
																						<tr>
																							<td width='15%' style='height: 0px;'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='6%' align='center'></td>
																							<td width='10%' align='center'></td>
																							<td width='10%' align='center'></td>
																							<td width='7%' align='center' class='toHide'></td>
																							<td width='7%' align='center' class='toHide'></td>
																							<td width='5%' align='center' class='toHide'></td>
																						</tr>
																					</thead>
																					<tbody id="tblFareQuoteBreakDown">
																					</tbody>
																				</table>
																			</div>
																			<div class="PBRKDownFooter">
																				<table width='100%' border='0' cellpadding='1'
																					cellspacing='0'>
																					<thead>
																						<tr>
																							<td width='15%' style='height: 0px;'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='8%' align='center'></td>
																							<td width='6%' align='center'></td>
																							<td width='10%' align='center'></td>
																							<td width='10%' align='center'></td>
																							<td width='7%' align='center' class='toHide'></td>
																							<td width='7%' align='center' class='toHide'></td>
																							<td width='5%' align='center' class='toHide'></td>
																						</tr>
																					</thead>
																					<tfoot>
																						<tr id="trhnglCharge">
																							<td class='thinBorderR' colspan='8' align='right'>
																								<table>
																									<tr>
																										<td><label id="lblHndChge"></label></td>
																										<td><div id='divHDTxnCurr'></div></td>
																									</tr>
																								</table>
																							</td>
																							<td class='thinBorderB thinBorderR rowHeight'
																								align='right'><div id='divHDChg'>0</div></td>
																							<td class='toHide thinBorderB thinBorderR rowHeight'
																								align='right' colspan="3">&nbsp;</td>
																						</tr>
																							<tr id="trAdminFee">
																									<td class='thinBorderB thinBorderR' colspan='8' align='right'>
																								<table>
																									<tr>
																										<td><label id="lblAdminFee"></label></td>
																										<td><div id='divADFeeCurr'></div></td>
																									</tr>
																								</table>
																							</td>	
																							<td class='thinBorderB thinBorderR rowHeight'
																								align='right'><div id='divADFeeAmount'>12</div></td>
																								<td class='toHide thinBorderB thinBorderR rowHeight'
																								align='right' colspan="3">&nbsp;</td>
																							</tr>
																							<tr id="trAgentCommission" style="display: none;">
																							<td class='thinBorderR' colspan='8' align='right'>
																								<table>
																									<tr>
																										<td><label id="lblAgentCommission">Agent Commission</label></td>
																										<td><div id='divAgentComCurr'>(base currency)</div></td>
																									</tr>
																								</table>
																							</td>
																						 	<td class='thinBorderB thinBorderR rowHeight'
																								align='right'><div id='divAgentCom'>(default value)</div></td>
																							<td class='toHide thinBorderB thinBorderR rowHeight'
																								align='right' colspan="3">&nbsp;</td>  
																						</tr>
																						<tr id="trFareDiscount">
																							<td class='thinBorderR thinBorderT' colspan='8'
																								align='right'>
																								<table width="99%">
																									<tr>																									
																										<td class='thinBorderR' align="left"
																											style="width: 250px"><label
																											id="lblFareDiscountAvail" class="lblFareDiscountAvail"></label></td>
																										<td>
																											<label id="lblDiscountAppliedDesc" class="lblDiscountAppliedDesc" i18n_key="availability_PromotionDiscount">Applied discount: </label>
																											<label id="lblDiscountApplied" class="lblDiscountApplied">0</label>&nbsp;
																											<label id="lblDiscPercent" class="lblDiscPercent">%</label>&nbsp;
																											<u:hasPrivilege privilegeId="xbe.res.make.addFareDisc">
																												<button id="btnDiscountPop" type="button"
																													class="btnMargin" style="" tabindex="" i18n_key="btn_Apply">Apply</button>
																												<button id="btnFareDiscountRemove"
																													type="button" class="btnMargin"
																													style="display: none" tabindex="" i18n_key="btn_Remove">Remove</button>
																											</u:hasPrivilege>		
																										</td>
																									
																										<td>
																											<div id="draggable"
																												style='position: absolute; top: 40%; left: 45%; right: 0; width: 150px; z-index: 1001;'>
																												<div id='divFareDiscountpop'
																													style='position: absolute; top: 0; left: 0; right: 0; width: 150px; z-index: 1001; display: none;'
																													class="popupXBE ui-corner-all">

																													<table width='100%' border='0'
																														cellpadding='0' cellspacing='0'>
																														<tr>
																															<td
																																class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
																																<table width='100%' border='0'
																																	cellpadding='0' cellspacing='0'>
																																	<tr>
																																		<td class='paneHD'>
																																			<p class='txtWhite txtBold'>
																																				&nbsp;Apply Fare Discount</p>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td align="center">
																																			<p align="left">
																																				&nbsp;Fare Discount<%@ include
																																					file='../common/inc_MandatoryField.jsp'%></p>
																																			<input type="text"
																																			id="fareDiscountPercentage"
																																			class="aa-input"
																																			style="width: 87%; text-align: right"
																																			maxlength="3" /> %
																																		</td>
																																	</tr>
																																	<tr id='discountCodeList'>
																																		<td align="center">
																																			<p align="left">
																																				&nbsp;Type<%@ include
																																					file='../common/inc_MandatoryField.jsp'%></p>
																																			<select align="left" id="selDiscountType"
																																			name="selDiscountType" class="aa-input"
																																			style="width: 95%;"></select>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td align="center">
																																			<p align="left">
																																				&nbsp;Note<%@ include
																																					file='../common/inc_MandatoryField.jsp'%></p>
																																			<textarea id="fareDiscountNotes"
																																				class='aa-input' style="width: 95%;"></textarea>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td align="center">
																																			<button id="btnFareDiscountApply"
																																				type="button" tabindex="">Apply</button>
																																			<button id="btnFareDiscountCancel"
																																				type="button" tabindex="">Close</button>
																																		</td>
																																	</tr>
																																</table>
																															</td>
																														</tr>
																													</table>
																												</div>
																											</div>
																										</td>
																									</tr>
																								</table>
																							</td>
																							<td class='thinBorderB thinBorderR rowHeight'
																								align='right'><div id='divFareDiscVal' class='divFareDiscVal'></div></td>
																							<td class='toHide thinBorderB thinBorderR rowHeight'
																								align='right' colspan="3">&nbsp;</td>
																						</tr>
																						<tr>
																							<td class='thinBorderR gridBG' colspan='8'>
																								<table width="100%" border="0" cellpadding='0'
																									cellspacing='0'>
																									<tr>
																										<td style="width: 255px" colspan="4"><div
																												id='divFQTxnCurr' class='txtBold'></div></td>
																										<td style="width: 250px; text-align: right"
																											class='thinBorderR'><div
																												id='divFQPerPaxSummary' class='txtBold'></div></td>

																										<td style="width: 105px;" i18n_key="availability_Total"><b>&nbsp;&nbsp;&nbsp;&nbsp;Total
																												Price</b></td>
																										<td><div id='divFQTotCurr'
																												class='txtBold'></div></td>
																									</tr>
																								</table>
																							</td>
																							<td
																								class='thinBorderB thinBorderR rowHeight darkBG'
																								align='right'>
																								<div id='divFQTotAmt' class='txtWhite txtBold'></div>
																							</td>
																							<td class='toHide thinBorderB thinBorderR rowHeight'
																								align='right' colspan="3">&nbsp;</td>
																						</tr>
																						<tr id="trFareDiscountCredit">
																						   <td class='thinBorderR thinBorderT' colspan='8'
																						      align='right'>
																						      <table width="99%">
																						         <tr>
																						            <td class='thinBorderR' align="left"
																						               style="width: 250px"><label
																						               id="lblFareDiscountAvail" class="lblFareDiscountAvail"></label></td>
																						            <td>
																						               <label id="lblDiscountAppliedDesc" class="lblDiscountAppliedDesc" i18n_key="availability_PromotionDiscount">Applied discount: </label>
																						               <label id="lblDiscountApplied" class="lblDiscountApplied">0</label>&nbsp;
																						               <label id="lblDiscPercent" class="lblDiscPercent">%</label>&nbsp;
																						            </td>
																						         </tr>
																						      </table>
																						   </td>
																						   <td class='thinBorderB thinBorderR rowHeight'
																						      align='right'>
																						      <div id='divFareDiscVal' class='divFareDiscVal'></div>
																						   </td>
																						   <td class='toHide thinBorderB thinBorderR rowHeight'
																						      align='right' colspan="3">&nbsp;</td>
																						</tr>
																					</tfoot>
																				</table>
																			</div>
																			<div id='divFareDetails'></div>
																		</tD>
																	</tr>
																</table>
															</td>
															<td width='0%' valign='top'>
																<%-- div id='divFlexiPane' style="display:none;">
																		<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																			<tr><td style='height:21px;'>&nbsp;</td></tr>
																			<tr>
																				<td valign='top'>
																					<div id='divFlexi'>
																						<table id="tblFlexi" class="scroll" cellpadding="0" cellspacing="0" width="100%">
																							<thead class="ui-state-default">
																								<tr>
																									<td class='thinRedBorderB thinRedBorderR gridBG thinRedBorderL thinRedBorderT ui-corner-TL ui-corner-TR' 
																										width='100%' style='height:24px;' colspan="2" align="center">Add Flexibilities</td>
																								</tr>
																							</thead>
																							<tbody id="tblFlexiDetails" align="center">
																								<tr id="trOutFlexiDetails" style="visibility: hidden;">
																									<td class="thinRedBorderB thinRedBorderR thinRedBorderL">+<span id="spnOutAmount"></span> for a <a id='lnkOutFlexiDes' href='#' tabindex="24"><u>flexible fare</u></a></td>
																									<td class="thinRedBorderB thinRedBorderR"><input id="addOutFlexi" name="addOutFlexi" type="checkbox" value="true" tabindex="25"/></td>
																								</tr>
																								<tr id="trInFlexiDetails" style="display:none;">
																									<td class="thinRedBorderB thinRedBorderR thinRedBorderL thinRedBorderT">+<span id="spnInAmount"></span> for a <a id='lnkInFlexiDes' href='#' tabindex="26"><u>flexible fare</u></a></td>
																									<td class="thinRedBorderB thinRedBorderR thinRedBorderT"><input id="addInFlexi" name="addInFlexi" type="checkbox"value="true" tabindex="27"/></td>
																								</tr>
																							</tbody>
																						</table>
																					</div>
																				</td>
																			</tr>
																		</table>
																	</div --%>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div></td>
							</tr>
						</table>
					</div>
					<div style='text-align: left'>
						<div id='airportMsg' class="spMsg"
							style='padding: 7px; padding-left: 20px; position: relative; width: 893px; z-index: 50; color: red; border: 1px solid red; background: #fff'
							onclick='UI_tabSearchFlights.hideAirportMessage()'></div>
					</div>
					<div id="requestSubmitPopup" style="display: none;min-height: 260px">
						<%@ include file='../../requestmanagement/RequestSubmitPopup.jsp' %>
					</div>
				</div>
			</div>
		</div>
		</td>
	</tr>
	<tr>
		<td align='right'>
			<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
				<button id="btnCreateRequest" type="button" class="btnMargin btnMedium ui-state-default ui-corner-all" tabindex="30">Create Request</button>
			</u:hasPrivilege>
			<button id="btnBook" type="button" class="btnLarge" tabindex="27" i18n_key="btn_Book">Make Reservation</button>
			<button id="btnReset" type="button" class="btnMargin" tabindex="28" i18n_key="btn_Reset">Reset</button>
			<button id="btnCancel" type="button" class="btnMargin" tabindex="29" i18n_key="btn_Close">Close</button>
		</td>
	</tr>
<!-- 	<tr> -->
<!-- 		<td> -->
<!-- 			<div id="requestSubmitPopup" style="display: none;min-height: 260px"> -->
<%-- 				<%@ include file='../../requestmanagement/RequestSubmitPopup.jsp' %> --%>
<!-- 			</div> -->
<!-- 		</td> -->
<!-- 	</tr> -->
</table>

<div id='divGstMessageAlert' style='position:absolute;top:200px;left:350px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
	<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold'> &nbsp; Alert</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr >
							<td class='popBGColor'  align='center'>
								<div id="gstMessage">
								</div>
							</td>							
						</tr>						
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>	
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>						
						<tr>
							<td class='popBGColor rowHeight' align='right'>
								<button id="btnAcceptMessage" type="button"  class="btnMargin" onclick="$('#divGstMessageAlert').hide();"> Ok </button>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
</div>
