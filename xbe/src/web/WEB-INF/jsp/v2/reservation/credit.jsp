<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" language="javascript">
 var lccEnabled = false;
 lccEnabled = <c:out value="${requestScope.lccEnabled }" escapeXml="false" />;
</script>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript" src="../js/v2/jquery/jquery.combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/reservation/credit.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapperPopUp" style="display:none;width:900px;">
		<table style='width:900px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:880px;">
						<form method='post' action="loadPaxCredit.action" id="frmSrchCredit">
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
										<label class='txtLarge txtBold'>User Previous Credits</label>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td class="ui-widget ui-widget-content ui-corner-all">
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='paneHD'>
													<p class='txtWhite txtBold'>Search</p>
												</td>
											</tr>
											<tr>
												<td>
													<table width='98%' border='0' cellpadding='1' cellspacing='0'>
														<tr>
															<td>Exact Match : </td>
															<td>
																<input type='checkbox' id='chkExact' name='searchParams.exactMatch' class='noBorder' checked />
															</td>
															<td>Load Zero Balance :</td>
															<td>
																<input type='checkbox' id='chkZero' name='searchParams.loadZeroBalance' class='noBorder'/>
															</td>
															<!-- check for lcc enabled privilege-->
															<td><span id="spnChkSearchOther" >Search Other Carriers :</span></td>
															<td>
																<input type='checkbox' id='chkSearchOther' name='searchParams.searchOtheCarrier' class='noBorder'/>
															</td>
															<td></td>
														</tr>
														<tr>
															<td width='13%'>PNR : </td>
															<td width='20%'>
																<input type="text" id="pnr" name="searchParams.pnr"  class="aa-input" maxlength="20" tabindex="1"/>
															</td>
															<td width='15%'>First Name : </td>
															<td width='18%'>
																<input type="text" id="firstName" name="searchParams.firstName" class="aa-input" maxlength="20" tabindex="1"/>
															</td>
															<td width='12%'>Last Name : </td>
															<td width='15%'>
																<input type="text" id="lastName" name="searchParams.lastName" class="aa-input" maxlength="20" tabindex="1"/>
															</td>
															<td width='7%'>
																<button id="btnSearch" type="button" class="btnMargin" tabindex="1">Search</button>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class='rowHeight'>
													<a id='lnkAdvSrch' href='#'><b>Advance Search</b></a>
												</td>
											</tr>
											<tr>
												<td style='height:60px;' valign='top'>
													<div id='divAdvSearch'>
														<table width='98%' border='0' cellpadding='1' cellspacing='0'>
															<tr>
																<td width='13%'>From :</td>
																<td width='20%'>
																	<select id="fromAirport" name="searchParams.fromAirport" class="aa-input" style="width:150px" tabindex="1"></select>
																</td>
																<td width='15%'>Departure Date :</td>
																<td width='18%'>
																	<input id="depatureDate" name="searchParams.depatureDate" type="text" class="aa-input" style="width:80px" maxlength="10" tabindex="3"/>
																</td>
																<td width='12%'>Telephone No : </td>
																<td width='15%'>
																	<input id="phoneNo" name="searchParams.phoneNo" type="text" class="aa-input"  maxlength="20" tabindex="3"/>
																</td>
																<td width='7%'></td>
															</tr>
															<tr>
																<td>To :</td>
																<td>
																	<select id="toAirport" name="searchParams.toAirport" class="aa-input" style="width:150px" tabindex="1"></select>
																</td>
																<td>Arrival Date :</td>
																<td>
																	<input id="returnDate" name="searchParams.returnDate" type="text" class="aa-input" style="width:80px" maxlength="10" tabindex="3"/>
																</td>
																<td>Flight No :</td>
																<td>
																	<input id="flightNo" name="searchParams.flightNo" type="text" class="aa-input" style="width:80px" maxlength="7" tabindex="3"/>
																</td>
																<td></td>
															</tr>
															<tr>
																<td>Credit Card No. :</td>
																<td>
																	<input id="creditCardNo" name="searchParams.creditCardNo" type="text" class="aa-input" style="width:40px" maxlength="4" tabindex="3"/>
																	<b>(Last 4 digits)</b>
																</td>
																<td>Expiry Date :</td>
																<td>
																	<input id="cardExpiry" name="searchParams.cardExpiry" type="text" class="aa-input" style="width:40px" maxlength="4" tabindex="3"/>
																	<b>(MM/YY)</b>
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class="ui-widget ui-widget-content ui-corner-all" style='height:405px;' valign='top'>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td colspan='2' class='paneHD'>
																<p class='txtWhite txtBold'>PNR Details</p>														
															</td>
														</tr>
														<tr>
															<td  class='singleGap'></td>
														</tr>
														<tr>
															<td colspan='2' style='height:370px;' valign='top' align='left'>
																<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td>
																			<table width='98%' border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td width='10%' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight' align='center'><b>PNR</b></td>
																					<td width='18%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Name</b></td>
																					<td width='6%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Carrier</b></td>
																					<td width='10%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Flight No</b></td>
																					<td width='15%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Dept. Date</b></td>
																					<td width='15%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Arrival Date</b></td>
																					<td width='10%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Segment</b></td>
																					<td width='8%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Pax</b></td>
																					<td width='8%' class='thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR' align='center'><b>Status</b></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td valign='top'>
																			<div id='divCreditGrid' style='width:850px;height:330px;overflow-X:hidden; overflow-Y:scroll;'>
																				<table cellpadding="1" cellspacing="0" border="0" width="100%">
																					<tbody id="tblCredit"></tbody>
																				</table>
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td width='50%'>
																<table>
																	<tr>
																		<td>
																			<b>Amount Due : </b>
																		</td>
																		<td>
																			<div id='divAmtDue'>0.00</div>
																		</td>
																	</tr>
																</table>
															</td>
															<td width='50%'>
																<table>
																	<tr>
																		<td>
																			<b>Amount Paying : </b>
																		</td>
																		<td>
																			<div id='divAmtPaying'>0.00</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td align='right'>
										<button id="btnConfirm" type="button" class="btnMargin">Confirm</button><button id="btnCancel" type="button" class="btnMargin">Cancel</button>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
							</table>
							<input type="hidden" name="paxID" id="paxID" />
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='inc_pageError.jsp' %>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
</body>
</html>

<script type="text/javascript">
	$("#divLegacyRootWrapperPopUp").getLanguageForOpener();
</script>