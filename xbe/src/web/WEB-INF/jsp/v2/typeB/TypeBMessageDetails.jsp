<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View TypeB Messages</title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<%@ include file='../common/inc_PgHD.jsp' %>
	<%@ include file='../common/inc_ShortCuts.jsp'%>
	
	<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script type="text/javascript" src="../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
  </head>
  
	<body class="legacyBody">
		  	<form method="post" action="" id="frmTypeBMessages" name="frmTypeBMessages">
		  	<div id="divLegacyRootWrapper" style="display:none;">
			<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
		  	<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class="ui-widget ui-widget-content ui-corner-all">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>									
								<td class='rowHeight ui-widget' style='height:40px'>
									<p class='txtBold txtMedium'>Search TypeB Messages</p>
								</td>
								</tr>
								<tr>
									<td class='paneHD'>
										<p class='txtWhite txtBold'>Search</p>
									</td>
								</tr>
								
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td align='center'>		
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td width="4%"></td>
												<td width="25%" align="left"><font>Record Locator : </font><input name="recordLocator"  class="aa-input" style="text-transform:uppercase" type="text" id="recordLocator"  maxlength="6">
												<td width="7%" align="right"><button id="btnSearch" type="button" class="Button btnMargin">Search</button></td>
												<td width="60%" ></td>
											</tr>													
											<tr>
												<td  class='singleGap'></td>
											</tr>
											<tr>
												<td  class='singleGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>	
									<td align='center'> 	
										<table id="tblTypeBMessages"></table>
									</td>			
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</div>
				</div>
			</form>
			<%@ include file='../common/inc_LoadMsg.jsp'%>
	</body>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
	<script src="../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/typeB/TypeBMessages.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</html>
				
	