<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
 
      <title>Manage Credit</title>

      <%@ include file='../common/inc_PgHD.jsp' %>
      
      		
	  <script type="text/javascript" src="../js/v2/airTravelAgents/CreditManagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		
	  <script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
      <script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
      
      <script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

		<script type="text/javascript">		    
			var strAgentCode = "<c:out value="${requestScope.reqAgentCode}" escapeXml="false" />";
			
			var arrError = new Array();
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />	
			
	    </script>
	    
  </head>
  <body class='tabBGColor'>
   	
	<form id = "frmModify" method="post" action="loadCreditManagement!saveCredit.action">
	
	<div id="divDisplayAgentTktStock" style="height:250px;text-align:left;background-color:#ECECEC;">
		<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" >
			<tr>
				<td width = '50%' valign="top">
					<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
					
					    <tr><td>&nbsp;</td></tr>
							
						<tr>
							<td align="right"><font  class="fntBold">           Total Credit Limit</font></td>
							<td>
								<input name="totalCreditLimit" type="text" class = "aa-input frm_editable" id="totalCreditLimit" size="20"  maxlength="8" />						
							</td>
						</tr>	
						
						<tr><td>&nbsp;</td></tr>		
						<tr>
							<td align="right"><font  class="fntBold">           Fixed Credit Limit</font></td>
							<td>
								<input name="fixedCreditLimit" type="text" class = "aa-input frm_editable" id="fixedCreditLimit" size="20"  maxlength="8" />						
							</td>
						</tr>	
						
						<tr><td>&nbsp;</td></tr>						
						
					    <tr>
							<td align="right"><font  class="fntBold">           Shared Credit Limit</font></td>
							<td>
								<input name="sharedCreditLimit" type="text" class = "aa-input frm_editable" id="sharedCreditLimit" size="20"  maxlength="8" />						
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>					
						
					</table>				
				</td>				
			</tr>						
		</table>
		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center">
						
					<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" class="btnDefault" />
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault" />
						</td>
						<td align="right" valign="bottom">
							<input name="btnEdit" type="button"  id="btnEdit" value="Edit" class="btnDefault" />
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault" />				
						</td>
					</tr>
		</table>
	</div>
			<input type="hidden" name="totalCreditLimit" id="hdnTotalCreditLimit" value="">
			<input type="hidden" name="fixedCreditLimit" id="hdnFixedCreditLimit" value="">
			<input type="hidden" name="sharedCreditLimit" id="hdnSharedCreditLimit" value="">
	
	</form>
	</div>
  </body>
</html>