<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
 
    <title>View / Edit Incentive Scheme</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<%@ include file='../common/inc_PgHD.jsp' %>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<script src="../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/airTravelAgents/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	
   	<script type="text/javascript" src="../js/v2/jquery/jquery.jqGrid.RESCRIPTED.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/common/isa.xbe.jq.plugin.config.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<script type="text/javascript" src="../js/v2/airTravelAgents/IncetiveScheme.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	<script type="text/javascript">
  		var arrFormData = new Array();
  		var arrError = new Array();
  		var runNo = "";
  		var stns = new Array();
  		var agentsArr = new Array();  			 
  		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
  </script>
  
  </head>
  <body class="">
  	 <div id="" >		
  		<div style="height:5px;"></div>
		<div style="margin-top:0px;margin-left:5px;margin-right:3px; height:520px;"> 		
		<div id="divSearchPanel" style="height:30px;text-align:left; ">		
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
				<tr>
					<td width="15%">
						<font class="txtBold">Scheme</font>
						<select id="searchScheme" size="1" name="searchScheme" onClick="" onChange="" style="width:76px" tabindex="1">
						<option value="" selected="selected">All</option>
						<c:out value="${requestScope.reqSchemeList}" escapeXml="false" />					
						</select>
					</td>
					<td width="18%">
						<font class="txtBold">Status</font>
						<select id="searchStatus" size="1" name="searchStatus" onClick="" onChange="" style="width:76px" tabindex="2">
						<option value="">All</option>
						<option value="ACT" selected="selected">Active</option>
						<option value="INA">Inactive</option>				
						</select>
					</td>
					<td width="20%"><input type="checkbox" name="searchEffCheck" id="searchEffCheck" class="noborder" tabindex="3"><font class="txtBold">Effective During</font>					
					</td>
					<td width="8%"><font class="txtBold">From Date</font></td>
					<td width="13%"><input type="text" name="searchFrom" id="searchFrom" size="10" maxlength="10" tabindex="5">
						
						
					</td>
					<td width="7%"><font class="txtBold">To Date</font></td>
					<td width="13%" align="left"><input type="text" name="searchTo" id="searchTo" size="10" maxlength="10" tabindex="5">
						
					</td>
					<td align="right"><input type="button" id="btnSearch"  name="btnSearch" value="Search" tabindex="6"></td>
				</tr>
			</table>
		</div>
		<div id="divResultsPanel" style="height:200px;text-align:left;">	
				<table id="tblIncentive" class="scroll" cellpadding="0" cellspacing="0" width="100%"></table>
				<div id="incentvpager" class="scroll" style="text-align:center;"></div>
						<u:hasPrivilege privilegeId="ta.incentive.add">
							<input name="btnAdd"  type="button" id="btnAdd" value="Add" class="btnDefault" tabindex="7">
						</u:hasPrivilege>
						<u:hasPrivilege privilegeId="ta.incentive.edit">
							<input name="btnEdit"  type="button" id="btnEdit" value="Edit" class="btnDefault" tabindex="8">
						</u:hasPrivilege>
						<u:hasPrivilege privilegeId="ta.incentive.delete">
							<input name="btnDelete"  type="button" id="btnDelete" value="Delete" class="btnDefault" tabindex="9">
						</u:hasPrivilege>
						<u:hasPrivilege privilegeId="ta.incentive.link">
							<input name="btnLinkAgent"  type="button" id="btnLinkAgent" value="Link Agent" class="btnDefault" tabindex="10">
						</u:hasPrivilege>								  	
		</div>		
		<div id="divDispIncentive" style="height:285px;text-align:left;">
			<form method="post" action="showIncentive.action" id="frmIncentive" enctype="multipart/form-data">
			<table width = "100%" cellpadding="2" cellspacing="0" border="0">	
					
				<tr>
				<td width="35%">
						<div id="divSlabData">
							<table><tr>
							<td>
								<input type="hidden" id="txtSchemeId" name="incentive.agentIncSchemeID" >
								
							</td></tr>					
							<tr><td>	
							<font>Scheme Name</font></td><td>
									<input type="text" id="txtSchemeName" name="incentive.schemeName" size="18"><font class="mandatory">*</font>
									<span id="spnSchName" style="display:none;">Incentive Scheme Name</span>
							</td></tr></table>	
						</div>
						<div id="divIncSlabs" style="height:200px;text-align:left;padding-left:4px;padding-top:5px;">
						<table id="tblIncSlabs" class="scroll" cellpadding="0" cellspacing="0" width="100%"></table>
						<div id="divPager" class="scroll" style="text-align:center;height:30px;"></div>
						</div>
				</td>
				<td width="65%">
					<table width = "100%" cellpadding="2" cellspacing="2" border="0">
						<tr>
							<td><font class="txtBold">Validation</font><font class="mandatory">*</font></td>
							<td colspan="2">
							<div id="divInclutions">
								<table width = "100%" cellpadding="2" cellspacing="2" border="0">
									<tr>
										<td colspan="2"><font class="txtBold">Inclusions</font><font class="mandatory">*</font></td>
									</tr>
									<tr>
										<td ><input type="checkbox" name="incentive.ticketPriceComponent" id="chkTktPrice" class="noborder" tabindex="31">
									
										<font class="txtBold">Fare Only</font></td>
										<td ><input type="checkbox" name="incentive.incToalFare" id="chkToalFare" class="noborder" tabindex="32">
												<font class="txtBold">Fare + SUR CHG</font></td>
									</tr>
									<tr>						
										<td><input type="checkbox" name="incentive.incPromoBookings" id="chkPromoFare" class="noborder" tabindex="33"><font class="txtBold">Incl Promo Fares&nbsp;</font></td>
										<td><input type="checkbox" name="incentive.incOwnTRFBookings" id="chkTRFBkng" class="noborder" tabindex="34">
											<font class="txtBold">Incl Ownership Tnsfr Bkgs.</font></td>
									</tr>
									<tr><td colspan="2"><input type="checkbox" name="incentive.incRepAgentSales" id="chkAgntRptSls" class="noborder" tabindex="33"><font class="txtBold">Incl Rpt Agnt Sales&nbsp;</font></td>
									</tr>
									<tr>
						
									<td colspan="2"><div id="divBasis" class="ui-widget ui-widget-content ui-corner-all ui-state-hover"><font class="txtBold">Basis</font><font class="mandatory">*</font>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="incentive.incentiveBasisA" id="chkBasisAll" class="noborder" tabindex="37">
										<font class="txtBold">All Issued</font>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="incentive.incentiveBasisF" id="chkBasisFlwn" class="noborder" tabindex="38">
										<font class="txtBold">Flown Only</font>
										</div></td>
								
									</tr>
								</table>
							</div>
							</td>
						</tr>	
						<tr>
							<td><font class="txtBold">Scheme Period</font></td>
							<td><input type="text" name="incentive.schemePrdFrom" id="schemePrdFrom" size="10" maxlength="10"><font class="mandatory">*</font>
							<span id="spnPrdFrm" style="display:none;">Scheme Period From</span>
								</td>
							<td><input type="text" name="incentive.schemePrdTo" id="schemePrdTo" size="10" maxlength="10"><font class="mandatory">*</font>
							<span id="spnSchPrdTo" style="display:none;">Scheme Period To</span>
								 </td>				
						</tr>
						<tr>
							<td><font class="txtBold">Comments</font></td>
							<td colspan="2"><textarea name="incentive.comments" rows="3" id="comments"></textarea>
							<span id="spnComments" style="display:none;">Comments</span></td>				
						</tr>							
					</table>					
				</td>
				</tr>
				<tr>
				
							<td>
								<input name="btnClose" type="button" class="btnDefault" id="btnClose" value="Close">
								&nbsp;&nbsp;
								<input name="btnReset" type="button" class="btnDefault" id="btnReset" value="Reset">
							</td>					
							<td align="right">
								<input name="btnSave" type="button" class="btnDefault" id="btnSave" value="Save">
							</td>						
				</tr>
		</table>
				<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="incentive.version" id="hdnVersion">
		<input type="hidden" name="incentive.status" id="hdnStatus">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnAgentNames" id="hdnAgentNames" value="">
		<input type="hidden" name="hdnSlabs" id="hdnSlabs" value="">

		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		</form>	
	</div>	
</div>	
</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>		
			

	<script type="text/javascript">
  		
  		  			 <c:out value="${requestScope.reqAgentList}" escapeXml="false" />
  		  			 
  		  			
	
	
  		
  </script>
  </body>
  
  
  
</html>