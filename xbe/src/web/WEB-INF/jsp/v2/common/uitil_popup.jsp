<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script type="text/javascript" src="../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<c:if test='${requestScope.workclock== "true"}'>
<script type="text/javascript" src="../js/v2/common/time_zone.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</c:if>
</head>
<body style="background: #fff">
<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr>
<td>
<div id="accordion">
<c:if test='${requestScope.dateValidity == "true"}'>
    <h3><a href="#">Validity Date Checker</a></h3>
    <div>
    	<div><label>This validity widget is depend on your system date & time, Make sure to set the correct date & time in you system</label></div>
		<table cellpadding="2" cellspacing="2" border="0" width="100%">
			<tr>
				<td width="35%"><label class="txtBold">From Date & time (DD/MM/YYYY hh:mm)</label></td>
				<td width="15%"><label class="txtBold">No. of Days</label></td>
				<td width="10%" align="center"><label class="txtBold"> + </label></td>
				<td width="10%" align="center"><label class="txtBold"> - </label></td>
				<td width="30%"></td>
			</tr>
			<tr>
				<td>
					<input type="text" id="txtDateFrom" name="txtDateFrom" style="width:90px" class="aa-input"/>
					<input type="text" id="txtTimeFrom" name="txtTimeFrom" style="width:40px" class="aa-input"/>
				</td>
				<td><input type="text" id="txtnoDays" name="txtnoDays" size="5" class="aa-input" value="1" style="text-align:right"/></td>
				<td align="center"><label><input type="radio" id="radPlus" name="radVarious" checked="checked" value="plus"/></label></td>
				<td align="center"><label><input type="radio" id="radMinus" name="radVarious" value="minus"/></label></td>
				<td align="center"><input type="button" id="btnValidate" value="Validate" class="ui-state-default ui-corner-all"/></td>
			</tr>
			<tr>
				<td colspan="5"><hr/></td>
			</tr>
			<tr>
				<td colspan="2">
					<label class="txtBold">Full Date</label>
				</td>
				<td>&nbsp;</td>
				<td colspan="2">
					<label class="txtBold">Short Date (DD/MM/YYYY)</label>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="33">
					<label id="fullDate" class="txtLarge"></label>
				</td>
				<td>&nbsp;</td>
				<td colspan="2">
					<label id="shortDate" class="txtLarge"></label>
				</td>
			</tr>
		</table>
	</div>
</c:if>
<c:if test='${requestScope.extCurrency == "true"}'>
	<h3><a href="#">Exchange Rates Calculator</a></h3>
    <div>
    	<table cellpadding="2" cellspacing="2" border="0" width="100%">
			<tr>
				<td width="20%"><label class="txtBold">Amount</label></td>
				<td width="29%"><label class="txtBold">Convert</label></td>
				<td width="29%"><label class="txtBold">Into</label></td>
				<td width="22%">&nbsp;</td>
			</tr>
			<tr>
				<td>
					<input type="text" id="txtAmount" name="txtAmount" value="1" class="aa-input" style="width:96%;text-align:right"/>
				</td>
				<td>
					<select id="selFromCurrency" class="aa-input" style="width:96%"></select>
				</td>
				<td>
					<select id="selToCurrency" class="aa-input" style="width:96%"></select>
				</td>
				<td align="right">
					<input type="button" id="btnCalc" value="Calculate" />
					<button id="btnSwap">Swap</button>
				</td>
			</tr>
			<tr>
				<td colspan="5"><hr/></td>
			</tr>
			<tr>
				<td colspan="5">
					<label class="txtBold">Converted Amount</label>&nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="chkRoundDecimal"/> 
					<label class="txtBold">* Apply AccelAreo Default Decimal Policy</label>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="33">
					<label id="resultAmt" class="txtLarge"></label>
				</td>
				<td colspan="3" height="33" align="right">
					<label id="resultAmt1" class=""></label>
				</td>
			</tr>
			<tr><td colspan="5">
				<label class="fntSamll txtBold">
					* Note This AccelAreo Default Decimal Policy is applied to the System Generated Total amounts only.
				</label>
			</td></tr>
		</table>
    </div>
</c:if>
<c:if test='${requestScope.workclock== "true"}'>
	<h3><a href="#">World Clock</a></h3>
    <div>
    	<div><label>All clocks times are depend on your system time, Make sure to set the correct date & time in you system</label><br/>
    	<label> * -  Internet Explorer is not a compatible browser for Analog clock, use Mozilla Firefox or Google Chrome for better usage
    	</div>
    	<div id="clockSettings">
    	
    	</div>
    	<div style="text-align:right"><input type="button" id="btnAddClock" value=" + "/></div>
    	<div>
    	<table border="0" width="100%"><tr><td align="left">
    	<input type="checkbox" id="chkAddtoDashBoard"/> Add clocks to dashboard tools bar
    	</td><td align="right">
    		<input type="button" id="btnApplyClk" value="Apply"/>
    		<input type="button" id="btnClose" value="Close"/>
    	</td></tr></table></div>
    </div>
</c:if>

<c:if test='${requestScope.mealAvl== "true"}'>

 <h3><a href="#">Meal Availability</a></h3>
 <div id="dashboardMeal">
						<div>
							<table>
								<tr>
									<td>Flight Number</td>
									<td><input type="text" value="" id="flightNum"
										name="flightNum" style="margin: 4px" tabindex="1"/> <%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
								
								    <td>Departure Date</td>
									<td><input type="text" value="" id="departureDateMeal"
										name="departureDateMeal" style="margin: 4px" tabindex="2"/><%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
									<td>
										<button name ="btnSearchMeal" id ="btnSearchMeal" style="width:80px;height:20px ; margin-left: 20px;" tabindex="4">Search</button>
									</td>
								</tr>
								<tr>
								<td>Class</td>		
										<td><select id="classOfServiceMeal" name="classOfServiceMeal"
								     class="aa-input" style="width: 110px; margin-left: 4px;" tabindex="3"></select><%@ include
									file='../common/inc_MandatoryField.jsp'%></td>
								</tr>
							</table>
							
							<table>
								<tr>
									<td>
							<div  id="mealDisplayDiv" style="margin-left: 20px; height: 250px; overflow-y: hidden; margin-top: 5px;"></div>
									</td>
								</tr>
							</table>
							
							<table width="100%">
								<tr align="right">
									<td colspan="5">
										<button name="btnClose" id="btnClose"
											style="width: 80px; height: 20px; margin: 5px" tabindex="6"
											onclick="javascript:window.close()">Close</button>
									</td>
								</tr>
							</table>
					</div>
					</div>
</c:if>

<c:if test='${requestScope.baggageAvl== "true"}'>

						<h3>
							<a href="#">Baggage Availability</a>
						</h3>
						<div id="dashboardBag">
							<table>
								<tr>
									<td>
										<table width="100%">
											<tr>
												<td>From</td>
												<td><input type="text" id="fAirport" class="aa-input"
													style="width: 140px" tabindex="1" /></td>
												<td><font class="mandatory" style="margin-right: 10px;">&nbsp;*</font></td>
												<td>Departure Date</td>
												<td><input type="text" id="departureDateBaggage"
													class="aa-input" name="departureDateBaggage"
												    tabindex="3" /></td>
												<td><font class="mandatory" style="margin-right: 10px;">&nbsp;*</font></td>
												<td>
													<button name="btnSearchBaggage" id="btnSearchBaggage"
														style="width: 80px; height: 20px" tabindex="6">Search</button>
												</td>
											</tr>
											<tr>
												<td>To</td>

												<td><input type="text" id="tAirport" class="aa-input"
													style="width: 140px" tabindex="2" /></td>
												<td><font class="mandatory" style="margin-right: 10px;">&nbsp;*</font></td>
												<td>Class</td>
												<td><select id="classOfServiceBaggage"
													name="classOfServiceBaggage" class="aa-input"
													style="width: 110px" tabindex="4"></select></td>
											</tr>
											<tr>
												<td>Search Options</td>

												<td><select id="selSearchOptions"
													name="searchParams.searchSystem" class="aa-input"
													style="width: 150px" title="Flight Search Options"
													tabindex="5"></select></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>

										<table width="100%">
											<tr>
												<td>
													<div style="margin-left: 25px; min-height: 210px"
														id="baggageGrid"></div>
												</td>
											</tr>
										</table>

									</td>
								</tr>
								<tr>
									<td>
										<table width="100%">
											<tr align="right">
												<td colspan="5">
													<button name="btnClose" id="btnClose"
														style="width: 80px; height: 20px; margin-top: 20px"
														tabindex="6" onclick="javascript:window.close()">Close</button>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</c:if>
</div>
</td></tr></table>
<%@ include file='../common/inc_LoadMsg_popup.jsp'%>
</body>
<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/extjs/ext-jquery-adapter.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/extjs/ext-all.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<c:if test='${requestScope.workclock== "true"}' >
<script type="text/javascript" src="../js/v2/common/jquery.clock.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/worldsClock.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</c:if>
<c:if test='${requestScope.dateValidity == "true"}'>
<script type="text/javascript" src="../js/v2/common/dateValidity.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</c:if>
<c:if test='${requestScope.extCurrency == "true"}'>
<script type="text/javascript" src="../js/v2/common/exchageRatesCalc.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</c:if>

<c:if test='${requestScope.baggageAvl == "true"}'>
 
   <script type="text/javascript">
   var airportOwnerList = new Array();
   var arrSearchOptions = new Array();
   var arrAirportsV2 	= new Array();
   var arrLogicalCC    = new Array();
   var carrierCode = '<c:out value="${requestScope.carrierCode}" escapeXml="false" />';
   var isOndJsEnabled = '<c:out value="${requestScope.isOndJsEnabled}" escapeXml="false" />';
   var defaultLanguage = '<c:out value="${requestScope.defaultLanguage}" escapeXml="false" />';
   <c:out value='${requestScope.airportOwnerList}' escapeXml='false' />   
   <c:out value='${requestScope.arrSearchOptions}' escapeXml='false' />      
   <c:out value='${requestScope.airportListV2}' escapeXml='false' />
   <c:out value='${requestScope.reqHtmlCosWithLogicalData}' escapeXml='false' />
   </script>
   
    <c:if test='${requestScope.isOndJsEnabled =="true"}'>
	 <script src="<c:out value='${requestScope.sysOndSource}' escapeXml='false'/>" type="text/javascript"></script>	 	
	</c:if>
	<script type="text/javascript" src="../js/v2/common/dashboardBaggageAvail.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</c:if>

<c:if test='${requestScope.mealAvl == "true"}'>
  <script type="text/javascript">
   var arrLogicalCC    = new Array();
   <c:out value='${requestScope.reqHtmlCosWithLogicalData}' escapeXml='false' />
   </script>
<script type="text/javascript" src="../js/v2/common/dashboardMealAvail.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</c:if>

<script type="text/javascript">
<!--
$(function() {
	$( "#accordion" ).accordion({
	autoHeight: false,
		navigation: true
	});
	$( "#accordion" ).css({
		"height": $(window).height(),
		"overflow-y":"hidden"
	});
	
	<c:if test='${requestScope.mealAvl == "true"}'>
	$( "#dashboardMeal" ).css({
		"height": 345,
		"overflow-y":"auto"
	});
	</c:if>
	<c:if test='${requestScope.baggageAvl == "true"}'>
		$( "#dashboardBag" ).css({
		"height": 345,
		"overflow-y":"auto"
	});
	</c:if>
	
	<c:if test='${requestScope.dateValidity == "true"}'>
	dateValidity.ready();
	</c:if>
	<c:if test='${requestScope.workclock== "true"}'>
	worldsClock.ready();
	</c:if>
	<c:if test='${requestScope.extCurrency == "true"}'>
	exchageRatesCalc.ready();
	</c:if>
});
-->
</script>
</html>