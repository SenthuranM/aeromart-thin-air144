<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advanced Search</title>
<%@ include file='../common/inc_PgHD.jsp' %>	
<script src="../js/v2/demo/advancedsearch.js" type="text/javascript"> </script> 

<style>
.multicitiStart{display:none}
#spnAdvancedSearchPane{display:none}
.FormHeader{color:#333333}
#spnFltSearchHD{color:#555}
#fareCoteData{display:none}
.ui-state-highlight, .ui-widget-content .ui-state-highlight{background:#FFEF9C;border:1px solid #D5CB8E;}
a.c{color:#0000FF! important;text-decoration:underline! important;}
.zoomerHeader{background: #AED1F1;width:892px;-moz-border-radius-topright:4px;-moz-border-radius-topleft:4px;}

.sectionZoomerouter{width:892px; 
	padding: 0px;padding-bottom: 0px}
.sectionZoomer{
	background: #AED1F1;
	border:1px solid #AED1F1;
	width:890px;
	margin: 0 auto; 
	display: block;
	height:160px;
	overflow:hidden;
}

.sectionSelection{background: #AED1F1;padding: 3px;margin: 0 10px}
.sectionSelection p.p{padding: 2px 0}
.sectionSelection table td{background: #fff;}
.sectionSelection table td.flightOut{background: url("../images/outFlight.jpg") no-repeat scroll 50% 50% #fff}
.sectionSelection table td.flightIn{background: url("../images/inFlight.jpg") no-repeat scroll 50% 50% #fff}
#table1 p.p, #table2 p.p{padding: 5px 0 }
#fareTable1 p.p,#fareTable2 p.p {margin: 0px; display: inline}

#table1 .ui-state-highlight, #table1 .ui-widget-content .ui-state-highlight{
	border:1px solid #B00606;
}
#table2 .ui-state-highlight, #table2 .ui-widget-content .ui-state-highlight{
	border:1px solid #B00606;
}
.sectionZoomerHandler{height: 8px;display: none;cursor: pointer;
	background: url("../images/upSlider.jpg") no-repeat scroll 50% 0 #7996B1;
}

.upsideDown{background: url("../images/downSlider.jpg") no-repeat scroll 50% 0 #7996B1;}

#fareTable1 .ui-state-default, #fareTable2 .ui-state-default  {
background:#f6eec3;
color:#B00606;
font-weight:bold;
outline:medium none;
}

#fareTable1 .ui-widget-header, #fareTable2 .ui-widget-header{
background:#FFEF9C;
border:1px solid #AAAAAA;
color:#B00606;
font-weight:bold;
}

#fareTable1 .ui-state-highlight, #fareTable2 .ui-widget-content .ui-state-highlight,
#fareTable1 .ui-state-hover, #fareTable2 .ui-state-hover{
background:#fff;
border:1px solid #D5CB8E;
}
.panels{height: 190px}
#fareTable1 a.selV, #fareTable2 a.selV{background:#444;padding: 3px 5px;color: #fff }
#fareTable1 .ui-widget-content .ui-state-highlight a.selV, #fareTable2 .ui-widget-content .ui-state-highlight a.selV{background:#B00606;padding: 3px 5px;color: #fff }
#fareTable1 .ui-state-hover a.selV, #fareTable2 .ui-state-hover a.selV{background:#ff6a6a;padding: 3px 5px;color: #fff }
</style>

</head>

<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:block;">
		<div style="height:5px;"></div>
<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
		<ul>
				<li><a href="#divSearchTab" style="width:100px">Search Flights</a></li>
		</ul>
<div style="width:914px;display: block;margin: 15px auto;">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
				<tbody><tr>
					<td height="20" width="10"></td>
					<td height="20"  class="FormHeadBackGround">
						<a href="javascript:void(0)" class="showHide" id="backto-search"><font class="FormHeader">
                        <span id="spnFltSearchHD">Back to Search Flights</span></font>
						</a>
					</td>

				  <td height="20" width="11"></td>
				</tr>
			</tbody></table>

            <span id="spnavailabilityPane" style="visibility: visible;">
             <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="border: 1px solid #AAA">
             <tr><td style="height:10px" style="background-color:#Fff;"></td></tr>
             <tr><td align="center" style="background-color:#Fff;" width="98%">
             <div class="resultContainer">
            		<div class="panels">
	            		<div class="zoomerHeader">
						 	<table width="98%">
		                    	<tr>
								<td align="left">
								<a id="lnkOP" href="javascript:void(0)"> &lt; </a></td>
								<td align="center">
								<font class="FormHeader">Outgoing <span id="spnOD">02/Jun/2010</span></font>							</td>
								<td align="right">
								<a id="lnkON" href="javascript:void(0)"> &gt; </a></td>
								</tr>
		                    </table>
						</div>
						<div class="sectionZoomerouter">
                     	<div class="sectionZoomer">
	                     	<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" class="midTable">
	                     	<tr>
			                    <td width="40%" valign="top">
			                       <!--<div style="overflow-x:scroll;overflow-y:hidden;width:340px">
			            			--><table id="table1" class="scroll" ></table>
			                        <!--</div>
			                     --></td>
			                    <td width="60%" valign="top" id="fareTable1">
			            			<font>Select flight to show the booking classes</font>
			                    </td>
		                    </tr>
		                    </table>
		                    
	                     </div>
	                     <div class="sectionZoomerHandler"></div>
	                     </div>
	          	 </div>
                  <div class="panels">
	            		<div class="zoomerHeader">
						<table width="98%">
                     	<tr>
							<td align="left">
							<a id="lnkOP" href="javascript:void(0)"> &lt; </a></td>
							<td align="center">
							<font class="FormHeader">Incoming <span id="spnOD">31/Jun/2010</span></font>
							</td>
							<td align="right">
							<a id="lnkON" href="javascript:void(0)"> &gt; </a></td>
						</tr>
                      </table>
					</div>
					<div class="sectionZoomerouter">
                     	<div class="sectionZoomer">
	                     	<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" class="midTable">
		                     	<tr>
			                     <td width="40%" valign="top">
			                        <!--<div style="overflow-x:scroll;overflow-y:hidden;width:340px">
			            			--><table id="table2" class="scroll" ></table>
			                        <!--</div>
			                     --></td>
				                      <td width="700" valign="top" id="fareTable2">
				            			
				                     </td>
	                     		</tr>
                    	 	</table>
                    	 	
                     	</div>
                     	<div class="sectionZoomerHandler"></div>
                     </div>
                     </div>
                     <div style="padding-left:10px"><font>* Agent is responsible to ensure correct booking class is combined</font></div>
                     </div>
                     </td>
                     </tr>
	                 <tr>
                     	<td>&nbsp;</td>
                     </tr>
                     <tr><td colspan="2" align="right" >
                     	<div class="sectionSelection" style="display: none;">
	                     	<table cellspacing="1" cellpadding="3" border="0" align="center" width="100%" >
	                     		<thead>
	                     			<tr>
	                     				<th colspan="10"><font class="FormHeader">Selected Flights</font></th>
	                     			</tr>
	                     		</thead>
	                     		<tbody>
		                     		<tr id="outbound">
		                     			
		                     		</tr>
		                     		<tr id="inbound">
		                     			
		                     		</tr>
	                     		</tbody>
	                     	</table>
                     	</div>
                     </td></tr>
                     <tr>
                     	<td>&nbsp;</td>
                     </tr>
                     <tr><td colspan="2" align="right" style="padding: 0 10px">
                     <input type="button" name="btBackresults" style="width: 100px;" class="Button" value="Back to Results" id="btBackresults" />
                     <input type="button" name="btnFareQuote" style="width: 100px;" class="Button" value="Fare Quote" id="btnFareQuote" disabled="disabled"/>
                     </td></tr>
                     <tr><td colspan="2">&nbsp;</td></tr>
                     <tr>
                     <td colspan="2">
                     <div id="fareCoteData">
                     <table cellspacing="1" cellpadding="2" border="0" align="center" width="100%" class="GridRow GridHeader GridHeaderBand GridHDBorder"><tbody><tr><td align="left">
                     <font class="fntBold">Fare quote - Outgoing 02/Aug/2010 - Return 06/Aug/2010  - Economy Class</font></td></tr><tr>	<td align="center" valign="top" style="height: 187px;">	<table cellspacing="1" cellpadding="2" border="0" width="100%" id="Table10">		<tbody><tr>			<td width="28%"><font class="fntBold">&nbsp;</font></td>			<td align="center" width="14%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Pax Type</font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Fare	&nbsp;			</font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Tax</font>			&nbsp;		</td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Sur.</font>			&nbsp;			</td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Per Pax</font>			</td>			<td align="center" width="8%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">No. pax</font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Total	    &nbsp;            </font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Total Price</font></td>		</tr>		<tr><td style="height: 2px;" colspan="9"></td></tr>	<tr>		<td valign="middle" class="GridItemRow GridItem GridItemHover defaultCursor">
                     <font title="Outgoing">Damascus - Sabiha Gokcen | 6Q201 </font></td>		<td class="GridItemRow GridItem GridItemHover defaultCursor"><font>Adult(s)&nbsp;</font></td>		
                     <td align="right" class="GridItemRow GridItem">
                     <span class="editFare"><font>990.00</font>&nbsp;</span>
                     <div style="display:none"><input type="text" class="faretext" size="5" style="text-align:right"/></div>
                     </td>
                     
                     		<td align="right" class="GridItemRow GridItem"><font>296.00&nbsp;</font></td>		<td align="right" class="GridItemRow GridItem"><font>205.00&nbsp;</font></td>		<td align="right" class="GridItemRow GridItem"><font class="row">1491.00</font>&nbsp;</td>		<td align="right" class="GridItemRow GridItem"><font>1&nbsp;</font></td>		
                            <td align="right" class="GridItemRow GridItem"><font class="row">1491.00</font>&nbsp;</td>
                            
                            		<td align="right" class="GridItemRow GridItem"><font class="row">1491.00</font>&nbsp;</td>
                                    
                                    	</tr><tr><td style="height: 2px;" colspan="9"></td></tr>	<tr>		<td valign="middle" class="GridItemRow GridItem GridItemHover defaultCursor">
                                        <font title="Return">Sabiha Gokcen - Damascus | 6Q945</font>
                                        </td>		<td class="GridItemRow GridItem GridItemHover defaultCursor"><font>Adult(s)&nbsp;</font></td>		
                            <td align="right" class="GridItemRow GridItem">
                            <span class="editFare"><font>990.00</font>&nbsp;</span>
                     		<div style="display:none"><input type="text" class="faretext" size="5" style="text-align:right"/></div>
  
                            </td>
                            
                            		<td align="right" class="GridItemRow GridItem"><font>55.00&nbsp;</font></td>		<td align="right" class="GridItemRow GridItem"><font>245.00&nbsp;</font></td>		<td align="right" class="GridItemRow GridItem"><font class="row">1290.00</font>&nbsp;</td>		<td align="right" class="GridItemRow GridItem"><font>1&nbsp;</font></td>		
                                    <td align="right" class="GridItemRow GridItem"><font class="row">1290.00</font>&nbsp;</td>
                                    
                                    		<td align="right" class="GridItemRow GridItem"><font class="row">1290.00</font>&nbsp;</td>
                                            
                                            				</tr><tr><td style="height: 2px;" colspan="9"></td></tr>				<tr>					<td align="left" valign="middle" colspan="6" class="GridItemRow GridItem GridItemHover defaultCursor"> 						<table cellspacing="0" cellpadding="2" border="0" width="100%"><tbody><tr>							<td align="left">							</td>							<td align="right"><font class="fntBold">AED</font>								
                                                            <font title="Per pax charge in AED for Adult" class="fntBold tot"> 2781.00</font>&nbsp;								</td>							</tr>						</tbody></table>					</td>					<td align="right" colspan="2" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Total Price AED&nbsp;</font></td>					<td align="right" valign="middle" class="GridItemRow GridItem GridItemHover defaultCursor">
                                                            <font class="fntBold tot">2781.00</font>&nbsp;
                                                            
                                                            </td>				</tr>
                                                            <tr><td colspan="2">&nbsp;</td></tr>
                                                            <tr><td colspan="10" align="right">
							<input type="button" name="btnContinue" class="Button" value="Book" id="btnContinue" style="visibility: visible;"/>
							<input type="button" name="btnReset" class="Button" value="Reset" id="btnReset"/>
							<input type="button" name="btnExit" class="Button" value="Close" id="btnExit"/>
                     </td></tr>
                                                            
                                                            			</tbody></table>		</td>	</tr></tbody></table>
                     </div>
                     </td>
                     </tr>
                  </table>
                 </span>
</div></div>
<script type="text/javascript"><!--
$(function () {
		jQuery("#table1").jqGrid({
			url:'./../js/v2/demo/gridFlightData.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['','Segment','Flight','Op.','Departure','Arrival','Avl'],
			colModel :[ 
			   {name:'rad',width:25, label:''},
			   {name:'segment',width:90, label:'Segment'},
			   {name:'flight',width:40, label:'Flight'},
			   {name:'op',label:'Op.', width:20},
			   {name:'departure',label:'Departure', width:100},
			   {name:'arrival',label:'Arrival', width:100},
			   {name:'avl',label:'Avl',width:40,align:'right'}
		   ],
			viewrecords: true,
			height: "auto",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "id" 
			},
			imgpath: 'themes/basic/images',
			hidegrid: false, 
			onSelectRow: function(rowid){
				var cont = $('#table1').getCell(rowid, 'segment');
				cont +='<span style="padding:0 10px"></span>' + $('#table1').getCell(rowid, 'flight').replace(/<br>/gi, " - ");
				cont +='<span style="padding:0 10px"></span>' + $('#table1').getCell(rowid, 'departure').replace(/<br>/gi, " - ");;
				$('#table1 tbody:first-child').find("#"+rowid).find("input[type='radio']").attr("checked","checked");
				createFareTable("#fareTable1","tableF1","./../js/v2/demo/gridFareDetails.js",cont);
			},
			loadComplete: function(){
				//var tt = $('#table1 tbody:first-child tr:first').attr('id');
				//$('#table1 tbody:first-child tr:first').find("input[type='radio']").attr("checked","checked");
				//jQuery("#table1").setSelection(tt, true);
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });
		 
		 $("#table2").jqGrid({
			url:'./../js/v2/demo/gridFlightDatat2.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['', 'Segment','Flight','Op.','Departure','Arrival','Avl'],
			colModel :[ 
			   {name:'rad',width:25, label:''},
			   {name:'segment',width:90, label:'Segment'},
			   {name:'flight',width:40, label:'Flight'},
			   {name:'op',label:'Op.', width:20},
			   {name:'departure',label:'Departure', width:100},
			   {name:'arrival',label:'Arrival', width:100},
			   {name:'avl',label:'Avl',width:40,align:'right'}
		   ],
		    hidegrid: false, 
			viewrecords: true,
			height: "auto",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "id" 
			},
			imgpath: 'themes/basic/images',
			onSelectRow: function(rowid){
				var cont = $('#table2').getCell(rowid, 'segment');
				cont +='<span style="padding:0 10px"></span>' + $('#table1').getCell(rowid, 'flight').replace(/<p class='p'>/gi, " - ");
				cont +='<span style="padding:0 10px"></span>' + $('#table1').getCell(rowid, 'departure').replace(/<p class='p'>/gi, " - ");
				$('#table2 tbody:first-child').find("#"+rowid).find("input[type='radio']").attr("checked","checked");
				createFareTable("#fareTable2","tableF2","./../js/v2/demo/gridFareDetails.js",cont);
			},
			loadComplete: function(){ 
				//var tt = $('#table2 tbody:first-child tr:first').attr('id');
				//$('#table2 tbody:first-child tr:first').find("input[type='radio']").attr("checked","checked");
				//$("#table2").setSelection(tt, true);
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });
		 
		 function createFareTable(pId,tabelID,url,capt,rID){
			$(pId).html('<table id="'+tabelID+'" class="scroll" ></table>')
			jQuery("#"+tabelID).jqGrid({
			url:url,
			datatype: 'json',
			mtype: 'GET',
			colNames:['AVS','ADL','CHD','INF','OW/RT','Basis','Validity','Combine'],
		    colModel :[ 
			   {name:'avs',width:50, label:'AVS',align:'center'},
			   {name:'adl',label:'ADL', width:40,align:'right'},
			   {name:'chd',label:'CHD', width:40,align:'right'},
			   {name:'inf',label:'INF', width:40,align:'right'},
			   {name:'owrt',label:'OW/RT', width:60,align:'center'},
			   {name:'basis',label:'Basis',width:60,align:'center'},
			   {name:'validity',label:'Validity',width:80,align:'center'},
			   {name:'combine',label:'Combine',width:90,align:'center'}
		   ],
			viewrecords: true,
			height: "100",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "id" 
			},
			hidegrid: false, 
			imgpath: 'themes/basic/images',
			caption: capt,
			onSelectRow: function(rowid){
				var slObj = {classItem:"HK", bookingClass:$('#'+tabelID).find("#"+rowid).find("a.selV").html(),
						 adlfare:"AED " + $('#'+tabelID).getCell(rowid, 'adl'),
						 inffare:"AED " + $('#'+tabelID).getCell(rowid, 'adl')};
				if (tabelID == "tableF1"){
					updateSelectedTable(slObj,"outBound")
				}else{
					updateSelectedTable(slObj,"inBound")
				}

			/*if (rowid != "V"){
				createFareTable("#fareTable2","tableF2","./../js/v2/reservation/gridFareDetailsH.js",capt);
			}else{
				createFareTable("#fareTable2","tableF2","./../js/v2/reservation/gridFareDetails.js",capt);
			}*/

			},
			loadComplete: function(){ 
				//var tt = $("#"+tabelID+" tbody:first-child tr:first").attr('id')
				//$("#"+tabelID).setSelection(tt, true);
				//enebleTooltip();
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });
				//enebleTooltip();
		 		
		};

		function updateSelectedTable(slObj,dir){
			
			var str = "";
			if (dir == "outBound"){
				var id = jQuery("#table1").find(".ui-state-highlight").attr("id");
				str += "<td width='4%' class='flightOut'>&nbsp;</td>";
				str += "<td width='21%' align='left'><font>"+$('#table1').getCell(id, 'segment')+"</font></td>";
				str += "<td width='10%' align='center'><font>"+$('#table1').getCell(id, 'flight')+"</font></td>";
				str += "<td width='5%' align='center'><font>"+$('#table1').getCell(id, 'op')+"</font></td>";
				str += "<td width='17%' align='center'><font>"+$('#table1').getCell(id, 'departure')+"</font></td>";
				str += "<td width='17%' align='center'><font>"+$('#table1').getCell(id, 'arrival')+"</font></td>";
				str += "<td width='5%' align='center'><font>"+slObj.classItem+"</font></td>";
				str += "<td width='5%' align='center'><font>"+slObj.bookingClass+"</font></td>";
				str += "<td width='8%' align='right'><font>"+slObj.adlfare+"</font></td>";
				str += "<td width='8%' align='right'><font>"+slObj.inffare+"</font></td>";
				$("#outbound").html(str);
			}else{
				var id = jQuery("#table2").find(".ui-state-highlight").attr("id");
				str += "<td width='4%' class='flightIn'>&nbsp;</td>";
				str += "<td width='21%' align='left'><font align='center'>"+$('#table2').getCell(id, 'segment')+"</font></td>";
				str += "<td width='10%'align='center'><font>"+$('#table2').getCell(id, 'flight')+"</font></td>";
				str += "<td width='5%' align='center'><font>"+$('#table2').getCell(id, 'op')+"</font></td>";
				str += "<td width='17%' align='center'><font>"+$('#table2').getCell(id, 'departure')+"</font></td>";
				str += "<td width='17%' align='center'><font>"+$('#table2').getCell(id, 'arrival')+"</font></td>";
				str += "<td width='5%' align='center'><font>"+slObj.classItem+"</font></td>";
				str += "<td width='5%' align='center'><font>"+slObj.bookingClass+"</font></td>";
				str += "<td width='8%' align='right'><font>"+slObj.adlfare+"</font></td>";
				str += "<td width='8%' align='right'><font>"+slObj.inffare+"</font></td>";
				$("#inbound").html(str);
			}
			$(".sectionSelection").show();
		}
		
		function enebleTooltip(){
			$('a.c').tooltip({ 
					track: true, 
					delay: 0, 
					showURL: false, 
					showBody: " - ", 
					fade: 250 
			});
		};
 /*
		 $("#tableF2").jqGrid({
			url:'./../js/gridFareDetails.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['FC','Basis','F.ADL','F.CHI','F.INF','Avl.','Max/Min','Season','Permit. COMB','OW/RT','&nbsp;'],
		    colModel :[ 
			   {name:'fc',width:25, label:'FC',align:'center'},
			   {name:'basis',label:'Basis', width:55},
			   {name:'fareAD',label:'Fare', width:40,align:'right'},
			   {name:'fareCH',label:'Fare', width:40,align:'right'},
			   {name:'fareINF',label:'Fare', width:40,align:'right'},
			   {name:'aval',label:'Avl.',width:32,align:'center'},
			   {name:'maxmin',label:'Max/Min',width:65,align:'center'},
			   {name:'season',label:'Season',width:80},
			   {name:'PCOMB',label:'Permit. COMB',width:80,align:'center'},
			   {name:'OWoRT',label:'OW/RT',width:60,align:'center'},
			   {name:'link',label:'&nbsp;',width:35,align:'center'}  
		   ],
			viewrecords: true,
			height: "80",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			hidegrid: false, 
			imgpath: 'themes/basic/images',
			caption: '6Q201',
			onSelectRow: function(rowid){
				hideFareQuote();
			},
			loadComplete: function(){ 
				var tt = $('#tableF2 tbody:first-child tr:first').attr('id')
				$("#tableF2").setSelection(tt, true);
				$("#fareCoteData").show();
			}
		 });
*/

		 $("#btnFareQuote").click(function(){
			 showLoader();
			 $(".resultContainer").slideUp(); 
		 	$("#fareCoteData").slideDown();
		 	$("#btnFareQuote").hide()
		 	$("#btBackresults").show()
		 });


		$("#btBackresults").click(function(){
			 $(".resultContainer").slideDown(); 
		 	$("#fareCoteData").slideUp();
		 	$("#btnFareQuote").show()
		 	$("#btBackresults").hide()
		 });
		 
		//if (event.keyCode == 18){
		var clicked = true;
		$(".sectionZoomerHandler").click(function(){
			if (clicked){
				$(".sectionZoomer").css({height:160,zIndex:0,opacity:0.5});
				$(".sectionZoomerouter").parent().css({
						position:"static",
						zIndex:1
				});
				var pos = $(this).prev().position();
				$(this).parent().css({
					position:"absolute",
					zIndex:1000,
					top:pos.top,
					left:pos.left
				 });
				$(this).prev().animate({height:160,opacity:1},{duration:200});
				$(this).addClass("upsideDown");
				clicked = false;
			}else{
				var pos =$(this).prev().position();
				$(this).prev().animate({height:160},{duration:50 ,
					complete:function(){
					$(this).parent().css({
						position:"static",
						zIndex:1,
						top:pos.top,
						left:pos.left
					 	});
					 	}
			 	});
				$(".sectionZoomer").css({opacity:1});
				$(this).removeClass("upsideDown");
				clicked = true;	
			}
		});

		$(".sectionZoomerouter").parent().bind("mouseenter", function(){
			//var t = $(this).find(".sectionZoomer").css("opacity")
			//if (t == 1)
				//$(this).find(".sectionZoomerHandler").fadeIn("slow");
		});

		$(".sectionZoomerouter").parent().bind("mouseleave", function(){
			//$(this).find(".sectionZoomerHandler").fadeOut("fast");
		});
		 
		/* 
		$(".sectionZoomerHandler").bind("click",function(){
			$(".sectionZoomer").css({"height":"93px","z-index":0})
			var pos =$(this).position();
		 	 	$(this).animate({height:93},{duration:50 ,
				 	complete:function(){
			 			$(".sectionZoomer").css({
						"position":"static",
						"z-index":0,
						"top":pos.top,
						"left":pos.left
					 	});
				 }});
		 });
		*/
		 function hideFareQuote(){
		 	$("#btnFareQuote").attr("disabled","");
		 	$("#fareCoteData").hide();
		 }
		$("#btnContinue").click(function(){
			showLoader();
			alert("All feature is not available in Demo mode. Please do a new search");
			window.location.href = "makeReservation.action";// Demo availability page does not have good enough information to redirect to the Passenger page, it direct to the normal search page with an alert
		});
		
		hideLoader = function(){
			$("#divLoadMsg").hide();	
		}
		showLoader = function(){
			$("#divLoadMsg").show();
			setTimeout('hideLoader()',1000);
		}
		showLoader();
		
});
--></script>
</div>
<div style="position: absolute; left: 650px; top: 10px; width: 300px; display: none;" id="divAgentBalance"><table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridRow GridHeader GridHeaderBand GridHDBorder">	<tbody><tr>		<td>			<table cellspacing="1" cellpadding="2" border="0" width="100%">				<tbody><tr>					<td align="center" width="58%" class="GridItemRow GridItem GridItemHover">						<font class="fntBold">Agents Available Credit </font>					</td>					<td align="right" class="GridItemRow GridItem">						<font class="fntBold">USD 0.00</font>					</td>				</tr>			</tbody></table>		</td>	</tr></tbody></table></div>
	<div style="background: none repeat scroll 0% 0% transparent; visibility: visible;" id="divLoadMsg">
		<iframe scrolling="no" frameborder="0" style="position: absolute; top: 50%; left: 40%; width: 260px; height: 40px; z-index: 2;" name="frmLoadMsg" id="frmLoadMsg" src="showFile!loadMsg.action"></iframe>
	</div>
</body>
</html>
