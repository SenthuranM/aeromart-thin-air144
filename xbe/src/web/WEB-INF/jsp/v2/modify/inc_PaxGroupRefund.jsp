<%-- <script
	src="../js/common/jquery_ui1.8.23.selectable.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script> --%>
<%@ include file='../common/inc_PgHD.jsp' %>
	<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<table width='675' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td colspan="2">
			<span style="float:left;width:60%"><label i18n_key="BookingInfo_PassengerInfo_lbl_selectTransactions">Select Transactions </label>
			<button id="selectDeselectTxn" type="button" style="width: auto;" i18n_key="btn_selectDeselectAll">Select/Deselect All</button></span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
<br/>
			<table width="100%" border="0"><tr>
			<td style="width:26%;"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_PassengerName">Passenger Name </label></b></td>
			<td style="width:14%;"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_PaidAmount">Paid Amount</label></b></td>
			<td style="width:31%;"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_Method">Method</label></b></td>
			<td style="width:12%;"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_TotalRefundable">Total Refundable </label></b></td>
			<td style="width:12%;"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_TotalCredit">Total Credit</label></b></td>
			<td style="width:5%;"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_PayCarrier">Pay Carrier</label></b></td>
			</tr></table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div style="height:300px;overflow-y:auto;overflow-x:hidden;">
			<ul style="float: left; width: 100%" id="listTxnIDs" class="scroll">			
			</ul>
			</div>
		</td>

	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>

	<tr id="trRefundTxt">
		<td class='txtMedium txtBold' i18n_key="BookingInfo_PassengerInfo_lbl_RefundInfo">Refund Info</td>
	</tr>
	<tr id="groupPayOptions">
		<td>
			<table width='100%' border='0' cellpadding='1' cellspacing='0'>
				<tr id="trPayOptionInputs">
					<td>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td width='20%' i18n_key="BookingInfo_PassengerInfo_lbl_PerTnxAmount">Per Tnx Amount :</td>
								<td width='80%'>
									<table border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td>
											<input id="txtPayAmount" name="txtPayAmount"
												type="text" class="aa-input" style="width: 120px"
												tabindex='1' />
												<%@ include	file='../common/inc_MandatoryField.jsp'%>
											</td>
											<td>&nbsp;</td>
											<td>
												<div id='divPayTotalCurr' class='txtBold'
													style="display: inline"></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign='top' i18n_key="BookingInfo_PassengerInfo_lbl_UserNotes">User notes :</td>
								<td valign='top'>
									<table cellpadding='0' cellspacing='0'>
										<tr>
											<td><textarea id='txtPayUN' name='txtPayUN' rows='4'
													cols='60' class="aa-input" tabindex='2'></textarea></td>
											<td valign='top'><%@ include
													file='../common/inc_MandatoryField.jsp'%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class='singleGap'></td>
				</tr>
				<tr id="trPayOptionSelections">
					<td>
						<table width='100%' border='0' cellpadding='1' cellspacing='0'>

							<tr id="trCA">
								<td width='15%'><input type='radio' id='radPaymentTypeC'
									name='radPaymentType' value='CASH' tabindex='3' /><label
									for="CASH" i18n_key="PassengerInfo_opt_Cash">Cash</label></td>
								<td colspan="2"></td>
							</tr>


							<tr id="trOA">
								<td width='15%'><input type='radio' id='radPaymentTypeA'
									name='radPaymentType' value='ACCO' tabindex='4' checked="true" /><label
									for="ACCO" i18n_key="PassengerInfo_opt_OnAccount">On Account</label></td>
								<td width='45%'><select id='selAgent' name='selAgent'
									class="aa-input" style="width: 250px;" tabindex='5'>
										<option value=""></option>
										<c:out value="${requestScope.reqPayAgents }" escapeXml="false" />
								</select></td>
								<td width='40%'><label id="lblAgentCurrency"></label></td>
							</tr>

							<tr id="trCC">
								<td width='15%'><input type='radio' id='radPaymentTypeR'
									name=radPaymentType value='CRED' tabindex='6' /><label
									for="CRED" i18n_key="PassengerInfo_opt_CreditCard">Credit Card</label></td>
								<td><select id='selCardType' name='selCardType'
									class="aa-input" style="width: 200px; display: none;"
									tabindex="5">
								</select></td>
								<td width='40%'></td>
							</tr>
							<tr id="trBSP">
									<td width='15%'>
										<input type='radio' id='radPaymentTypeBsp' name=radPaymentType value='BSP' tabindex='7'/><label for="BSP" i18n_key="PassengerInfo_opt_BusinessSettlementPlan">BSP</label>
									</td>
									<td>
										<select id='selCardType' name='selCardType' class="aa-input" style="width:200px;display:none;" tabindex="5">
										</select>
									</td>
									<td width='40%'></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class='singleGap'></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr id="groupPayOptButtons">
		<td align='right'>
			<button id="btnGroupRefundConfirm" type="button" class="btnMargin" i18n_key="btn_Confirm">Confirm</button>
			<button id="btnGroupRefundReset" type="button" class="btnMargin" i18n_key="btn_Reset">Reset</button>
			<button id="btnGroupRefundClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
		</td>
	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>
	<tr id="groupCreditCardDet">
														<td align='center'>
															<table width='100%' border='1' cellpadding='0' cellspacing='0'>
																<tr>
																	<td class="ui-widget ui-widget-content ui-corner-all" style='height:160px;' valign='top'>
																		<table width='100%' border='0' cellpadding='2' cellspacing='0'>
																			<tr>
																				<td class='paneHD' colspan='3'>
																					<p class='txtWhite txtBold' i18n_key="">Credit Card Details</p>
																				</td>
																			</tr>
																			<tr>
																				<td class='doubleGap' colspan='3'></td>
																			</tr>
																			<tr>
																				<td width='60%' valign='top'>
																					<table width='100%' border='0' cellpadding='2' cellspacing='0'>
																						<tr>
																							<td width='40%' i18n_key="">Card Type : </td>
																							<td width='60%' class='noPadding'>
																								<table>
																									<tr>
																										<td>
																											<select id="groupCardType" name="groupCardType" size="1" tabindex="1" style="width:100" class='aa-input'/>
																												<option id="groupCardOptions"></option>
																											</select>
																										</td>
																										<td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td i18n_key="">Card Number :</td>
																							<td>
																								<input type="text" id="groupCardNo" name="groupCardNo" tabindex="2" maxlength="20" size="20" class="aa-input txtBold"/>
																							</td>
																						</tr>
																						<tr>
																							<td i18n_key="">Card Holders Name :</td>
																							<td>
																								<input type="text" id="groupCardHoldersName" name="groupCardHoldersName" tabindex="3" maxlength="30" size="30" class="aa-input"/>
																							</td>
																						</tr>
																						<tr id="trGroupCardEDate">
																							<td i18n_key="">Expiry Date :</td>
																							<td class='noPadding'>
																								<table>
																									<tr>
																										<td>
																											<input type="text" id="groupExpiryDate" name="groupCardExpiryDate" tabindex="4" maxlength="4" size="4" class="aa-input"/>
																										</td>
																										<td>
																											<p class='txtBold'>(MMYY)</p>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr id="trGroupCardCVV">
																							<td i18n_key="">Card's CVV :</td>
																							<td>
																								<input type="text" id="groupCardCVV" name="groupCardCVV" tabindex="5" maxlength="4" size="4" class="aa-input"/>
																							</td>
																						</tr>
																						<tr id="trGroupCardTotPayAmt">
																							<td i18n_key="">Per Tnx Refund Amount:</td>
																							<td>
																								<div id='divGroupTotPay' class='txtBold'></div>
																							</td>
																						</tr>
																						<tr>
																							<td colspan='2'>
																								<table>
																									<tr>
																										<td i18n_key="">
																											Your Payment would be processed in 
																										</td>
																										<td>
																											<div id='divGroupTxtCurr' class='txtBold txtRed'></div>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																				<td width='20%' align='center'>
																					<a href="#" onclick='UI_tabPayment.showVeriSign(); return false;'><img src="../images/VSign_no_cache.gif" border="0"/></a>
																				</td>																				
																			</tr>
																			<tr>
																				<td class='doubleGap' colspan='3'></td>
																			</tr>
																			<tr>
																				<td colspan='2'>
																					<table width="100%" border="0" cellpadding="0" cellspacing="1">
																					</table>
																				</td>
																				<td align='right'>																					
																					<button id="btnGroupCRConfirm" type="button" class="btnMargin" i18n_key="">Confirm</button><button id="btnGroupCRCancel" type="button" class="btnMargin" i18n_key="">Cancel</button>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
</table>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
