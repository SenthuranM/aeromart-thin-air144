<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/modify/createAlert.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapperPopUp" style="display:block;width:650px;height:250px">
		<table style='width:650px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:650px;">
						<form method='post' action="createAlert.action" id="frmCreateAlert">
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td colspan="2">
										<label class='txtLarge txtBold' i18n_key="">Create Alert</label>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
									<td>
									   <div class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 5px; margin-left:0px;margin-right:10px;">	  
										<table width='90%' border='0' cellpadding='0' cellspacing='0' style="margin:auto;margin-top: 5px;margin-bottom: 5px;">
											<tr>
											<td class="ui-widget ui-widget-content ui-corner-all" style="height: 160px;margin-top: 5px" valign="top">
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td class='paneHD'>						    
															<p class='txtWhite txtBold'  style="padding-left:3px;">Create Alert</p>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																<tr>
																	<td  class='singleGap'></td>
																</tr>
															   <tr>							   	 
																	<td align="right" i18n_key="">
																		Description :
																	</td>
																	<td align="left">
																		<textarea id="txtDescription" name="description" class="aa-input" cols="60" rows="5"></textarea>
																	</td>									
																</tr>
															</table>
													    </td>
												 </tr>
											  </table>
											</td>
											<td></td>
										</tr>
										<tr>
											<td align="right" colspan="2" style="padding-top:5px">
												<input type="button" id="btnSave" value="Create Alert" class="btnMargin btnMedium"/>
												<input type="button" id="btnClose" value="Close" class="btnMargin btnMedium"/>
											</td>
										</tr>
									  </table>
									 </div>	
									</td>
								</tr>								
							</table>
							<input type="hidden" id="pnr" name="pnr" value="<c:out value='${requestScope.reqPnr}' escapeXml='false' />"/>
							<input type="hidden" id="pnrSegIds" name="pnrSegIds" value="<c:out value='${requestScope.reqPnrSegIds}' escapeXml='false' />"/>
							<input type="hidden" id="jsonGroupPnr" name="jsonGroupPnr" value=""/>
						</form>
					</div>
				</td>
			</tr> 
		
		</table>
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
</body>
</html>
<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguage();
</script>