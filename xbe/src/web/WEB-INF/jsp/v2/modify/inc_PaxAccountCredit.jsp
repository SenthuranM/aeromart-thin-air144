												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td style='height:300px;' valign='top'>
															<table id="tblCredit" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpading='0' cellspacing='0'>
																<tr>
																	<td width='20%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_TotalAvailableCredit">Total Available Credit :</td>
																	<td width='17%'><div id='divAvailCredit' class='txtMediumSmall txtBold'></div></td>
																	<td width='20%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_TotalExpiredCredit">Total Expired Credit :</td>
																	<td width='17%'><div id='divExpCredits' class='txtMediumSmall txtBold'></div></td>
																	<td width='12%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_ActualCredit">Actual Credit :</td>
																	<td width='14%'><div id='divActualCredit' class='txtMediumSmall txtBold'></div></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																<tr>
																	<td width='20%' i18n_key="PaxAccDetails_Amount">Amount : </td>
																	<td width='80%'>
																		<table border='0' cellpadding='0' cellspacing='0'>
																		<tr>
																			<td>
																				<input id="txtCreditAmount" name="txtCreditAmount" type="text" class="aa-input" style="width:120px"/><%@ include file='../common/inc_MandatoryField.jsp' %>
																			</td>
																			<td>
																				&nbsp;
																			</td>
																			<td align="left">
																				<div id='divCreditAdjustCurr' class='txtBold'></div>
																			</td>
																		</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td i18n_key="PaxAccDetails_ExpiryDate">Expiry Date : </td>
																	<td>
																		<input id="txtExpiryDate" name="txtExpiryDate" type="text" class="aa-input" style="width:120px"/><%@ include file='../common/inc_MandatoryField.jsp' %>	
																	</td>
																</tr>
																<tr>
																	<td valign='top' i18n_key="PaxAccDetails_Usernotes">User notes : </td>
																	<td valign='top'>
																		<table cellpadding='0' cellspacing='0'>
																			<tr>
																				<td><textarea id='txtCreditUN' name='txtCreditUN' rows='5' cols='60' class="aa-input"></textarea></td>
																				<td valign='top'>
																					<%@ include file='../common/inc_MandatoryField.jsp' %>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>
													<tr>
														<td align='right'>
															<button id="btnCreditExtend" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Extend">Extend</button><button id="btnCreditReInst" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_ReInstate">Re-Instate</button><button id="btnCreditReset" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Reset">Reset</button><button id="btnCreditClose" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Close">Close</button>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>													
												</table>
												<input type="hidden" id="txtCreditId" name="txtCreditId">
												<input type="hidden" id="txtTNXId" name="txtTNXId">
												<input type="hidden" id="txtCarrierCode" name="txtCarrierCode">
												<input type="hidden" id="ocCreditAmount" name="ocCreditAmount">