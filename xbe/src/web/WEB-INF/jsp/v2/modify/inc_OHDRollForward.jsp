	<div id='divOHDRollForward' style='position:absolute;top:200px;left:300px;width:450px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold'  i18n_key=""> &nbsp;On-Hold Roll Forward</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trAncillaryOpt'>
							<td class='popBGColor'  align='center'>
								<table width='95%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td align='left' i18n_key="">From Date&nbsp;</td>
										<td colspan="2" align='left'>
											<input id="rollForwardStartDate" name="rollForwardStartDate" type="text" 
												readonly="readonly" class="aa-input" style="width:80px;" maxlength="10"/>
										</td>
									</tr>
									<tr>
										<td align='left' i18n_key="">To Date&nbsp;</td>
										<td align='left'>
											<input id="rollForwardEndDate" name="rollForwardEndDate" type="text" 
												readonly="readonly" class="aa-input" style="width:80px;" maxlength="10"/>
										</td>
										<td align="left">
											<button id="btnOHDRollForwardSearch" type="button" class="btnMargin ui-state-default ui-corner-all">Search</button>
											&nbsp;&nbsp;
											<button id="btnOHDRollForwardCancel" type="button" class="btnMargin ui-state-default ui-corner-all">Close</button>
										</td>
									</tr>
									<tr class="OverBookClass">
										<td align='left' i18n_key="">Overbook&nbsp;</td>
										<td colspan="2" align='left'>
										<input type="checkbox" id="chkRollForwardOverBook" name="chkRollForwardOverBook" />
										</td>
									</tr>
									<tr>
										<td colspan="2" align='left'></td>
									</tr>									
								</table>
							</td>							
						</tr>
						<tr>
							<td  class='popBGColor singleGap' style='height: 15px'></td>
						</tr>
						<tr>
							<td class='popBGColor' align='center'>
								<div style="height: 200px;overflow-y:auto;overflow-x:hidden;" id="divRFAvailFlights">
								
									<table width='95%' border='0' cellpadding='1' cellspacing='0'>
										<tr >
											<td>
												<input type="checkbox" id="chkOhdRFAll"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="" >All</label>
											</td>
											<td>
												<input type="checkbox" id="chkOhdRFSun"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="" >Sun</label>
											</td>
											<td>
												<input type="checkbox" id="chkOhdRFMon"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="" >Mon</label>
											</td>
											<td>
												<input type="checkbox" id="chkOhdRFTue"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="" >Tue</label>
											</td>
											<td>
												<input type="checkbox" id="chkOhdRFWed"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="" >Wed</label>
											</td>
											<td>
												<input type="checkbox" id="chkOhdRFThu"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="">Thu</label>
											</td>
											<td>
												<input type="checkbox" id="chkOhdRFFri"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="" >Fri</label>
											</td>
											<td>
												<input type="checkbox" id="chkOhdRFSat"/>
											</td>
											<td align="left">
												<label class='txtBold' i18n_key="">Sat</label>
											</td>
											
										</tr>
									</table>
									<table width='95%' border='0' cellpadding='1' cellspacing='0'>
										<tr id="trOHDRollForwardTemp" style="display: none;">
											<td>
												<input type="checkbox" name="chkOhdRF"/>
											</td>
											<td align="left">
												<label class='txtBold' ></label>
											</td>
										</tr>
									</table>
								</div>
							</td>							
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trOhdRFConfirm' style="display: none;">
							<td class='popBGColor' align='center'>
								<table width='95%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td i18n_key="">
											Use Buffer time override<input type="checkbox" id="chkOHDRFOvrdOHDBuffer"/>
										</td>
										<td i18n_key="">
											Days Before <input id="txtOHDRFBufferDays" name="txtOHDRFBufferDays" type="text" 
												 class="aa-input" style="width:80px;" maxlength="4" disabled="disabled"/>
										</td>
										<td i18n_key="">
											Expiry time (Zulu) <input id="txtOHDRFBufferTime" name="txtOHDRFBufferTime" type="text" 
												 class="aa-input" style="width:80px;" maxlength="10" disabled="disabled"/>
										</td>
									</tr>
									<tr>
										<td colspan="3">
											<button id="btnOHDRollForwardConfirm" type="button" class="btnMargin ui-state-default ui-corner-all">Confirm</button>
										</td>
									</tr>
								</table>
							</td>							
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div id="divLoadBg" class="loadingDone" style=" "></div>	
	<div id='divLoadMsg' style='position:absolute;display:none; background:transparent;top:50%; left:40%; width:260; height:40;z-index:1500;'>
		<table width='260' border='0' cellpadding='2' cellspacing='1' align='center' class='pgBar'>
			<tr>
				<td> 
					<table width='250' border='0' cellpadding='2' cellspacing='1' align='center'>
						<tr>
							<td align='center' style="height:27px; background-image: url(../images/Progress_no_cache.gif); background-repeat:repeat-x;">
								<p class='txtDef txtBold' style="z-index:1000;">Loading in progress. Please Wait...</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	