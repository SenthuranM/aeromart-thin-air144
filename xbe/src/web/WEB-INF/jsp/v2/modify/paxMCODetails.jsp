<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<%@ include file='../common/inc_PgHD.jsp' %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="x-ua-compatible" content="IE=8"></meta>

	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		
		var strPaxID = "<c:out value="${requestScope.reqPaxID}" escapeXml="false" />";
		// Charge Types
		//var jsChargesTypes = eval('('+'<c:out value="${requestScope.reqChargeTypes}" escapeXml="false" />'+')');
	//-->
	var arrIPGs = [];
	<c:out value="${requestScope.reqAllIpgsHtml}" escapeXml="false" />
	</script>

<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/modify/paxMCODetails.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapperPopUp" style="display:none;width:900px;">
		<table style='width:900px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:880px;">
						<table width='97%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td>
									<label class='txtLarge txtBold' i18n_key="">Passenger MCO Details</label>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr>
								<td class="ui-widget ui-widget-content ui-corner-all" align='center'>
									<table width='95%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td colspan='3' class='singleGap'></td>
										</tr>
										<tr>
											<td width='5%'><label class='txtBold' i18n_key="Search_lbl_PNR">PNR : </lable></td>
											<td width='15%'><div id='divPNR'></div></td>
											<td width='10%'>
												<label class='txtBold' i18n_key="CancelRes_list_PassengerName">PAX Name :</label>
											</td>
											<td width='70%'>
												<select id='selPax' name='selPax' size='1' onchange='UI_MCODetails.selPaxOnChange()'>
												</select>
											</td>
										</tr>
										<tr>
											<td colspan='3' class='singleGap'></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr>
								<td style='height:555px;'>
									<div id="divPaxMCO" style="margin-top:0px;margin-left:0px;margin-right:0px;height:530px;">
										<div id='divPaxMCO'>
											<%@ include file="inc_MCODetails.jsp" %>
										</div>											
									</div>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='../reservation/inc_pageError.jsp' %>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
</body>
</html>

<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguageForOpener();
</script>

