<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="x-ua-compatible" content="IE=8"></meta>

<%@ include file='../common/inc_PgHD.jsp' %>
	<script type="text/javascript">
	
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
	
		var fltSegPaxMap = window.parent.fltSegPaxMap;	
	
	</script>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/modify/groupPaxStatusUpdate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</head>
<body style="background: white;">
	<div id="divLegacyRootWrapperPopUp" style="display:none;width:850px;overflow-x:hidden;height:600px;overflow-y:auto;max-height: 600px">
		<table style='width:850px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:850px;">
							<table width='95%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>
								<tr>
									<td>
										<label class='txtLarge txtBold' >Passengers E-Ticket Details</label>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>
								<tr>
						            <td align='left'>
						               <table width='98%' border='0' cellpadding='1' cellspacing='0'>
						                  <tr>
						                     <td width="50%">
						                        <table id="tblTitle" width='100%' border='0' cellpadding='0' cellspacing='1'>
						                           <tr>
						                              <td width='32%' >Selected Segment :</td>
						                              <td>
						                                 <select id='selSegment' name='selSegment' class="aa-input" style="width:250px;"></select>
						                              </td>
						                           </tr>
						                        </table>
						                     </td>
						                     <td></td>
						                  </tr>
						                  <tr>
						                     <td width="50%">
						                        <table id="tblFirstName" width='100%' border='0' cellpadding='0' cellspacing='1'>
						                           <tr>
						                              <td width='32%' >Segment Code :</td>
						                              <td>
						                                 <label id='txtSegmentCode' style="width:144px;"></label>
						                              </td>
						                           </tr>
						                        </table>
						                     </td>
						                     <td width="50%">
						                        <table id="tblLastName" width='100%' border='0' cellpadding='0' cellspacing='1'>
						                           <tr>
						                              <td width='32%' >Departure Date :</td>
						                              <td>
						                              	 <label id='txtDepDate' style="width:144px;"></label>
						                              </td>
						                           </tr>
						                        </table>
						                     </td>
						                  </tr>
						                  <tr>
						                     <td width="50%">
						                        <table id="tblAddr1" width='100%' border='0' cellpadding='0' cellspacing='1'>
						                           <tr>
						                              <td width='32%' >Flight Number :</td>
						                              <td>
						                              	 <label id='txtFltNum' style="width:144px;"></label>
						                             </td>
						                           </tr>
						                        </table>
						                     </td>
						                     <td width="50%">
						                        <table id="tblCity" width='100%' border='0' cellpadding='0' cellspacing='1'>
						                           <tr>
						                              <td width='32%' >Carrier :</td>
						                              <td>
						                              	 <label id='txtCarrier' style="width:144px;"></label>
						                              </td>
						                           </tr>
						                        </table>
						                     </td>
						                  </tr>
						                  <tr>
						                     <td width="50%">
						                        <table id="tblAddr1" width='100%' border='0' cellpadding='0' cellspacing='1'>
						                           <tr>
						                              <td width='32%' >Segment Status :</td>
						                              <td>
						                              	 <label id='txtSegmentStatus' style="width:144px;"></label>
						                             </td>
						                           </tr>
						                        </table>
						                     </td>
						                  </tr>						                  
						               </table>
						            </td>
						         </tr>				
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>			
								<tr>
									<td style='height:110px;' valign="top" class="ui-widget ui-widget-content ui-corner-all" align='center'>
										
										<table width='97%' border='0' cellpadding='0' cellspacing='0'>											
											<tr>
												<td style='height:100px;' valign='top'>
													 <table id="listPax" class="scroll" cellpadding="0" cellspacing="0"></table>
												</td>
											</tr>																					
										</table>												
									</td>
								</tr>
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>
								<tr id="trStatusInfo">
								   <td class="ui-widget ui-widget-content ui-corner-all">
								      <table width='100%' border='0' cellpadding='0' cellspacing='0'>
								         <tr>
								            <td class='paneHD'>
								               <table width='100%' border='0' cellpadding='0' cellspacing='0'>
								                  <tr>
								                     <td width='20%'>
								                        <p class='txtWhite txtBold' >Status Update</p>
								                     </td>
								                  </tr>
								               </table>
								            </td>
								         </tr>
								         <tr>
								            <td  class='singleGap'></td>
								         </tr>
								         <tr>
								            <td align='left'>
								               <table width='98%' border='0' cellpadding='1' cellspacing='0'>
								                  <tr>
								                     <td width="50%">
								                        <table id="tblTitle" width='100%' border='0' cellpadding='0' cellspacing='1'>
								                           <tr>
								                              <td width='32%' >Coupon Status </td>
								                              <td>
								                                 <select id='selETLStatus' name='selETLStatus' class="aa-input" style="width:50px;"></select>
								                                 <span id="spnTitleMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
								                              </td>
								                           </tr>
								                        </table>
								                     </td>
								                  </tr>
								                  <tr>
								                     <td width="50%">
								                        <table id="tblFirstName" width='100%' border='0' cellpadding='0' cellspacing='1'>
								                           <tr>
								                              <td width='32%' >Pax Status :</td>
								                              <td>
								                                <select id='selPFSStatus' name='selPFSStatus' class="aa-input" style="width:50px;"></select>
								                                 <span id="spnTitleMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
								                              </td>
								                           </tr>
								                        </table>
								                     </td>
								                  </tr>
								                  <tr>
								                     <td width="50%">
								                        <table id="tblAddr1" width='100%' border='0' cellpadding='0' cellspacing='1'>
								                           <tr>
								                              <td width='32%' >Remarks </td>
								                              <td>
								                                 <textarea id='txtRemarks' name='txtRemarks' class="aa-input fontCapitalize" style="width:144px;" maxlength="100"/></textarea>
								                                 <span id="spnAddrMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>	
								                              </td>
								                           </tr>
								                        </table>
								                     </td>
								                  </tr>
								               </table>
								            </td>
								         </tr>
								      </table>
								   </td>
								</tr>
								<tr>
									<td align="right">
										<button id="btnUpdate" type="button" class="btnMargin" >Update</button>
										<button id="btnClose" type="button" class="btnMargin" >Close</button>
									</td>									
								</tr>							
								<tr>
									<td  class='singleGap'></td>
								</tr>								
							</table>							
						
					</div>
				</td>
			</tr>
			<tr>
				<td>
				<%--
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='../reservation/inc_pageError.jsp'%>
					</div>
				 --%>
				</td>
			</tr>
			<tr>
			<td>
				<form method='post' action="" id="frmGroupPaxStatusUpdate" style="display:none;">
				</form>
			</td>
			</tr>
		</table>
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
</body>
</html>

<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguageForOpener();
</script>
