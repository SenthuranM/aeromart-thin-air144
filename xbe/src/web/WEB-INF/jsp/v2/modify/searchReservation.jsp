<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<%@ include file='../common/inc_ShortCuts.jsp'%>	

	<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		DATA_ResPro["load"] ="<c:out value="${requestScope.load }" escapeXml="false" />";
		DATA_ResPro["searchByEticket"] =<c:out value="${requestScope.searchByETicket }" escapeXml="false" />;
		
	//-->
	</script>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/pageShortCuts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		
	<script type="text/javascript" src="../js/v2/modify/searchReservation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:none;">
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td class='ui-widget'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td class='rowHeight ui-widget' style='height:40px'>
									<p class='txtBold txtMedium'  i18n_key="Search_lbl_SearchReservation">Search Reservation</p>
								</td>
							</tr>
							<tr>
								<td class="ui-widget ui-widget-content ui-corner-all">
									<form method='post' action="reservationList.action" id="frmSrchBkg">
									<table width='100%' border='0' cellpadding='1' cellspacing='0'>
										<tr>
											<td class='paneHD'>
												<p class='txtWhite txtBold' i18n_key="Search_lbl_Search">Search</p>
											</td>
										</tr>
										<tr>
											<td>
												<table width='99%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td i18n_key="Search_lbl_ExactMatch">Exact match:</td>
														<td >
															<input type='checkbox' id='chkExact' name='searchParams.exactMatch' class='noBorder' tabindex="1"/>
														</td>
														<td colspan="3">													   
														   <span id="spnEticket">
														   	<table>
															   	<tr>
															   		<td i18n_key="Search_lbl_SearchBy">Search By</td>
															   		<td><input type="radio" id="rSearchPNR" name="rSearch"  value ="pnr" class="NoBorder" tabindex="20" checked><label for="pnr" i18n_key="Search_lbl_PNR">PNR</label></td>
															   		<td><input type="radio" id="rSearchETicket" name="rSearch" value ="eticket" class="NoBorder" tabindex="21"><label for="eticket" i18n_key="Search_lbl_TicketNumber">Ticket Number</label></td>
															   		<c:if test="${requestScope.gdsEnabled }">
															   			<td><input type="radio" id="rSearchGDSRecordLocator" name="rSearch" value ="gdsRecLocator" class="NoBorder" tabindex="21"><label for="eticket" i18n_key="Search_lbl_GDSRecordLocator">GDS Record Locator</label></td>
															   		</c:if>
																	<u:hasPrivilege
																			privilegeId="xbe.res.allow.search.tax.invoice.number">
																		<td><input type="radio" id="rInvoiceNumber" name="rSearch"
																				   value="invoiceNumber" class="NoBorder"
																				   tabindex="21"><label for="gstInvoiceNumber"
																										i18n_key="Search_lbl_GST_InvoiceNumber">GST
																			Invoice Number</label></td>
																	</u:hasPrivilege>
															   	</tr>														   	
														   	</table>
														   </span>
													   </td>   
													   <td colspan="2">
													   		<span id="spnDisplayContact">
														   		<table>
														   			<tr>
														   				<td i18n_key="Search_lbl_DisplayContactDetails">Display Contact Details</td>
														   				<td><input type="checkbox" id="chkDspContDetails" name="chkDspContDetails" class="noBorder"></td>
														   			</tr>
														   		</table>
													   		</span>
													   </td>
													</tr>
													
													<tr>
														<td width='8%'><div id='divPNR'></div></td>
														<td width='13%'>
															<input type="text" id="pnr" name="searchParams.pnr"  class="aa-input" maxlength="21" tabindex="2"/>
														</td>
														<td class="name-search-fields" width='10%' align="center" i18n_key="Search_lbl_FirstName">First Name : </td>
														<td class="name-search-fields" width='12%'>
															<input type="text" id="firstName" name="searchParams.firstName" class="aa-input" maxlength="20" tabindex="3"/>
														</td>
														<td class="name-search-fields" width='10%' align="center" i18n_key="Search_lbl_LastName">Last Name : </td>
														<td width='15%' class="name-search-fields">
															<input type="text" id="lastName" name="searchParams.lastName" class="aa-input" maxlength="20" tabindex="4"/>
														</td>
														<td class="name-search-fields-dummy-row" width='10%' style="display: none">
														<td class="name-search-fields-dummy-row" width='12%' style="display: none">
														<td class="name-search-fields-dummy-row" width='10%' style="display: none">
														<td class="name-search-fields-dummy-row" width='15%' style="display: none">
														</td>
														<td width='12%' align="center" i18n_key="Search_lbl_SearchOptions">Search Options : </td>
														<td width='11%'>
															<select id="selSearchOptions" name="searchParams.searchSystem" class="aa-input" tabindex="5" style="width:85px"  title="Search Options"></select>
														</td>
														<td width='7%' align="center">
															<button id="btnSearch" type="button" class="btnMargin" tabindex="6" i18n_key="Search_lbl_Search">Search</button>
														</td>														
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class='rowHeight'>
												<div id='divAdvSearchHD'><a id='lnkAdvSrch' href='#' i18n_key="Search_lbl_AdvanceSearch"><b>Advance Search</b></a></div>
											</td>
										</tr>
										<tr>
											<td style='height:100px;' valign='top'><div id='divAdvSearch' style='display:none;'>
												<table width='99%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td width='13%' i18n_key="AdvSearch_lbl_From">From :</td>
														<td width='20%'>
															<select id="fromAirport" name="searchParams.fromAirport" class="aa-input" style="width:150px" tabindex="7"></select>
														</td>
														<td width='13%' i18n_key="AdvSearch_lbl_DepartureDate">Departure Date :</td>
														<td width='15%'>
															<input id="depatureDate" name="searchParams.depatureDate" type="text" class="aa-input" style="width:80px" maxlength="10" tabindex="8"/>
														</td>
														<td width='12%' i18n_key="AdvSearch_lbl_TelephoneNo">Telephone No : </td>
														<td width='20%'>
															<input type='text' id='txtPhoneCntry' name='searchParams.phoneCntry' class="aa-input" style="width:30px;" maxlength="4" tabindex="9"/><input type='text' id='txtPhoneArea' name='searchParams.phoneArea' class="aa-input" style="width:30px;" maxlength="4" tabindex="10"/><input type='text' id='txtPhoneNo' name='searchParams.phoneNo' class="aa-input" style="width:80px;" maxlength="10" tabindex="11"/>
														</td>
														<td width='7%'></td>
													</tr>
													<tr>
														<td i18n_key="AdvSearch_lbl_To">To :</td>
														<td>
															<select id="toAirport" name="searchParams.toAirport" class="aa-input" style="width:150px" tabindex="12"></select>
														</td>
														<td i18n_key="AdvSearch_lbl_ArrivalDate">Arrival Date :</td>
														<td>
															<input id="returnDate" name="searchParams.returnDate" type="text" class="aa-input" style="width:80px" maxlength="10" tabindex="13"/>
														</td>
														<td i18n_key="AdvSearch_lbl_FlightNo">Flight No : </td>
														<td>
															<input type='text' id='flightNo' name='searchParams.flightNo' class="aa-input" style="width:80px;" maxlength="10" tabindex="14"/>
														</td>
														<td></td>
													</tr>
													<tr>
														<td colspan='7'><hr/></td>
													</tr>
													<tr>
														<td i18n_key="AdvSearch_lbl_CreditCardNo">Credit Card No. :</td>
														<td>
															<input id="creditCardNo" name="searchParams.creditCardNo" type="text" class="aa-input" style="width:40px" maxlength="4" tabindex="15"/>
															<b i18n_key="Last_4_digits">(Last 4 digits)</b>
														</td>
														<td i18n_key="AdvSearch_lbl_ExpiryDate">Expiry Date : </td>
														<td>
															<input id="cardExpiry" name="searchParams.cardExpiry" type="text" class="aa-input" style="width:40px" maxlength="5" tabindex="16"/>
															<b  i18n_key="AdvSearch_lbl_MonthYear">(MM/YY)</b>
														</td>
														<td i18n_key="AdvSearch_lbl_CreditCardAuthorizationCode">CC Auth. code :</td>
														<td>
															<input id="authCode" name="searchParams.authCode" type="text" class="aa-input" style="width:100px" maxlength="4" tabindex="17"/>
														</td>
														<td></td>
													</tr>
													<tr>
														<td i18n_key="BCType">BC Type : </td>
														<td>
															<select id="bookingType" name="searchParams.bookingType" class="aa-input" style="width:100px" tabindex="18"></select>
														</td>
														<td i18n_key="AdvSearch_lbl_Passport">Passport :</td>
														<td>
															<input id="passport" name="searchParams.passport" type="text" class="aa-input" style="width:100px" maxlength="15" tabindex="19"/>
														</td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</table></div>
											</td>
										</tr>
									</table>
									</form>
								</td>
							</tr>
							<tr>
								<td class='singleGap'>
								</td>
							</tr>
							<tr>
								<td valign='top' style='height:385px;'><div id='divSrchResults'>
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td class="ui-widget ui-widget-content ui-corner-all" style='height:385px' valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<p class='txtWhite txtBold' i18n_key="SearchResults_lbl_SearchResults">Search Results</p>
														</td>
													</tr>
													<tr>
														<td>
															<table width='98.4%' border='0' cellpadding='0' cellspacing='0'>
																<tr>
																	<td width='2%' align='center' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight'></td>
																	<td width='8%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_PNR"><b>PNR</b></td>
																	<td width='20%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_Name"><b>Name</b></td>
																	<td width='8%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_PNRStatus"><b>PNR Status</b></td>
																	<td width='9%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_FlightNo"><b>Flight No</b></td>
																	<td width='13%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_DepartureDate"><b>Dept. Date</b></td>
																	<td width='13%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_ArrivalDate"><b>Arrival Date</b></td>
																	<td width='8%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_Segment"><b>Segment</b></td>
																	<td width='5%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_Passenger"><b>Pax</b></td>
																	<td width='9%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT' i18n_key="SearchResults_lbl_ClassOfService"><b>COS</b></td>
																	<td width='5%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR' i18n_key="SearchResults_lbl_Status"><b>Status</b></td>
																</tr>
																<tr>
																	<td valign='top'>
																		<div id='divResults' style='position:absolute;width:952px;height:330px;overflow-X:hidden; overflow-Y:scroll;'>
																			<table id='tblSrarch' width='100%' border='0' cellpadding='0' cellspacing='0'>
																			</table>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></div>
								</td>
							</tr>
							<tr>
								<td class='singleGap'></td>
							</tr>
							<tr>
								<td align='right'>
									<button id="btnReset" type="button" class="btnMargin" i18n_key="btn_Reset">Reset</button><button id="btnClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		
	</div>
	<%@ include file='../common/inc_Credit.jsp' %>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
	<form method='post' action="showNewFile!loadRes.action" id="frmRes">
		<input type='hidden' id='pnrNO' name='pnrNO'/>
		<input type='hidden' id='groupPNRNO' name='groupPNRNO'/>
		<input type='hidden' id='marketingAirlineCode' name='marketingAirlineCode'/>
		<input type='hidden' id='interlineAgreementId' name='interlineAgreementId'/>
		<input type='hidden' id='airlineCode' name='airlineCode'/>
	</form>
	<div id="divReservation" style="position:absolute;top:0px;width:1000px;height:677px;display:none;z-index:3">
		<iframe src="showBlank" id='frmReservation' name='frmReservation'  frameborder='0' scrolling='no' style='width:100%; height:100%'></iframe>
	</div>
	
	  <div id="divBookMarkReservation">
		   <table width="100%" border="0" cellpadding="0" cellspacing="1" style="height:570px" bgcolor="black">
		    <tr>
		     <td width="100%"  valign="top" align="center" id="tdBookMark" class="GridRow GridHeader GridHeaderBand GridHDBorder cursorHand">
		      <br/><img id="imgPNRList" src="../images/AA140_2_no_cache.jpg"/>
		     </td>
		    </tr>
		   </table>
	  </div>
	  <div id="divBookMarkReservation2" style="height:568px;width:150;position:absolute;left:850;top:70;z-index:1500;border:1px solid;">
		   <table width="100%" border="0" cellpadding="0" cellspacing="1" style="height:568px" id="Table1">
		    <tr>
		     <td width="100%" bgcolor='#FFFFCF' valign="top" align="center"  class="GridRow transParent">
		      <span id="spnPNRList"></span>
		     </td>
		    </tr>
		   </table>
	  </div>
	<div id="search_flightAddInfoToolTip" style="display:none;position: absolute;left:10px;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);font-family:Lucida Grande,Lucida Sans,Arial,sans-serif;" >
		<center>
			<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
				<tr>
					<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
						<font size="4px" style="font-weight:bold" i18n_key="BookingInfo_FlightDetails_lbl_FlightDetails"> Flight Details  </font>
						<img src="../images/AA171_no_cache.gif">
					</td>
				</tr>
				<tr>
					<td style="padding-top:10px;" id="search_flightAddInfoToolTipBody">
					</td>
				</tr> 
			</table>
		</center>
	</div>
</body>
</html>