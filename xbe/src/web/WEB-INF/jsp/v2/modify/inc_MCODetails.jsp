												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
										
													<tr>
														<td style='height:300px;' valign='top'>
															<table id="tblMCODetails" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td><div id="mcoDetailsHD" class='txtMedium txtBold'>MCO Details</div></td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>
													<tr>
														<td>
															<table width='50%' id="tblDetailsMCO" border='0' cellpading='5px' cellspacing='5px' >
																<tr>
																	<td class="txtMediumSmall txtBold" i18n_key="">Service :</td>
																	<td><div id='divService' class='txtMediumSmall'></div></td>
																</tr>
																<tr>
																	<td class="txtMediumSmall txtBold" i18n_key="">E-MCO Number :</td>
																	<td ><div id='divMCONumber' class='txtMediumSmall'></div></td>
																</tr>
																<tr>
																	<td class="txtMediumSmall txtBold" i18n_key="">Amount :</td>
																	<td><label id='divAmount' class='txtMediumSmall'></label>&nbsp;<label id='divCurrencyCode' class='txtMediumSmall'></label></td>
																</tr>
																<tr>
																	<td class="txtMediumSmall txtBold" i18n_key="">Flight Number :</td>
																	<td><div id='divFlightNumber' class='txtMediumSmall'></div></td>
																</tr>
																<tr>
																	<td class="txtMediumSmall txtBold" i18n_key="">Remarks :</td>
																	<td><div id='divRemarks' class='txtMediumSmall'></div></td>
																</tr>
																<tr>
																	<td class="txtMediumSmall txtBold" i18n_key="">Status :</td>
																	<td><div id='divStatus' class='txtMediumSmall'></div></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>
													<tr>
														<td align='right'>
															<button id="btnMCOClose" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Close">Close</button>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>													
												</table>
