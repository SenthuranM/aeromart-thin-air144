<%-- <script src="../js/common/jquery_ui1.8.23.selectable.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> --%>

<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td colspan="2">
			<span style="float:left;width:50%"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_SelectSegment">Select Segment </label></b>
			<button id="selectDeselectSegment" type="button" style="width: auto;" i18n_key="PassengerInfo_btn_SelectDeselectAll">Select/Deselect All</button></span>
			<span style="float:left;width:50%"><b><label i18n_key="BookingInfo_PassengerInfo_lbl_SelectPassenger">Select Passenger </label></b>
			<button id="selectDeselectPassenger" type="button" style="width: auto;" i18n_key="PassengerInfo_btn_SelectDeselectAll">Select/Deselect All</button></span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div style="height:300px;overflow-y:auto;overflow-x:hidden;">
			<ul style="float:left;width:50%" id="listSegments" class="scroll">
				
			</ul>
			<ul style="float:left;width:50%" id="listPassengers" class="scroll">
			</ul>
			</div>
		</td>
		
	</tr>
	<tr>
		<td class='singleGap'>
		</td>
	</tr>
	<tr>
		<td style="height:100px;">
			<table id="chargesAdjustmentInput" width='100%' border='0' cellpadding='1' cellspacing='0'>
				<tr>
					<td width='20%' i18n_key="BookingInfo_PassengerInfo_lbl_AdjustmentAmount">Adjustment Amount : </td>
					<td width='80%'>
						<table border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td>
									<input id="txtAdjustment" name="txtAdjustment" type="text" class="aa-input" style="width:120px" maxlength="10"/><%@ include file='../common/inc_MandatoryField.jsp' %>
								</td>
								<td>
									&nbsp;
								</td>
								<td>
									<div id='divChjgAdjustCurr' class='txtBold'></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td i18n_key="BookingInfo_PassengerInfo_lbl_AdjustmentType">Adjustment Type : </td>
					<td>
						<select id='selAdjustmentType' name='selAdjustmentType' size='1' class="aa-input" style="width:200px">
						</select>
					</td>
				</tr>
				<tr>
					<td i18n_key="BookingInfo_PassengerInfo_lbl_RefundableCharge">Refundable Charge: </td>
					<td>
						<input id='selChargeType' name='selChargeType' type="checkbox" class="aa-input" value="true"/>
					</td>
				</tr>
				<tr>
					<td valign='top' i18n_key="BookingInfo_PassengerInfo_lbl_UserNotes">User notes : </td>
					<td valign='top'>
						<table cellpadding='0' cellspacing='0'>
							<tr>
								<td><textarea id='txtUN' name='txtUN' rows='3' cols='60' class="aa-input"></textarea></td>
								<td valign='top'>
									<%@ include file='../common/inc_MandatoryField.jsp' %>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td  class='singleGap'></td>
	</tr>
	<tr>
		<td align='right'>
			<table width='100%' border='0' cellpading='0' cellspacing='0'>
				<tr>
					<td align='left' class="txtMediumSmall txtBold" id="amountWithSerivceTax"></td>
					<td align='right'>
						<button id="btnServiceTaxAmount" type="button" class="btnLarge" i18n_key="PaxAccDetails_btn_ServiceTax">Get Service Tax Amount</button>
						<button id="btnHandlingFeeAmount" type="button" class="btnLarge" i18n_key="PaxAccDetails_btn_ServiceTax">Get Handling Fee</button>
					</td>
				</tr>
			</table>
															
		</td>
	</tr>
	<tr>
		<td align='right'>
			<button id="btnGroupChargeConfirm" type="button" class="btnMargin" i18n_key="PassengerInfo_btn_Confirm">Confirm</button><button id="btnGroupChargeReset" type="button" class="btnMargin" i18n_key="PassengerInfo_btn_Reset">Reset</button><button id="btnGroupChargeClose" type="button" class="btnMargin" i18n_key="PassengerInfo_btn_Close">Close</button>
		</td>
	</tr>
	<tr>
		<td  class='singleGap'></td>
	</tr>													
</table>
