				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class="ui-widget ui-widget-content ui-corner-all">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='rowHeight ui-widget' valign='top'> 
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='paneHD'>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td width='20%'><p class='txtWhite txtBold' i18n_key="BookingInfo_FlightDetails_lbl_FlightDetails">Flight Details</p></td>
															<td width='32%'>
																<div id='divBkgStatus' class='txtYellow txtBold'></div>
															</td>
															<td width='4%' i18n_key="SearchResults_lbl_PNR">
																<b>PNR : </b>
															</td>
															<td width='15%'>
																<div id='divPnrNo' class='txtYellow txtBold'></div>
															</td>
															<td align='right'>
																<div id='divAgent' class='txtYellow txtBold'></div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='height:90px;' valign='top'>
										<div id='divFltSltd' style='position:absolute;width:915px;height:88px;overflow-X:hidden; overflow-Y:scroll;'>
											<table cellpadding="0" cellspacing="0" border="0" width="100%">
												<tbody id="flightTemplete"></tbody>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td align='right'>
										
											<!-- <button id="btnCpnCntrlRQ" type="button" class="btnMargin">Request Coupon CTRL</button> -->
											
											<button id="btnNewPnr" type="button" class="btnMargin" i18n_key="btn_NewPNR">New PNR</button>
											
											<button id="btnExchangeSegment" type="button" class="btnMargin" i18n_key="btn_TicketExchange">Ticket Exchange</button>
											
											<button id="btnModifyRes" type="button" class="btnMargin" i18n_key="btn_ModifyReservation">Modify Reservation</button>
											
											<button id="btnAddGroundSegment" type="button" class="btnMargin" i18n_key="btn_AddBusSegment">Add Bus Segment</button>
										
											<button id="btnAddSegment" type="button" class="btnMargin" i18n_key="btn_AddSegment">Add Segment</button>
										
											<button id="btnModSegment" type="button" class="btnMargin" i18n_key="btn_ModifySegment">Modify Segment</button>
										
											<button id="btnCanSegment" type="button" class="btnMargin" i18n_key="btn_CancelSegment">Cancel Segment</button>
										
											<button id="btnConfirmOpenRt" type="button" class="btnMargin" i18n_key="btn_ConfirmReturn">Confirm Return</button>
										
											<button id="btnCreateAlert" type="button" class="btnMargin" i18n_key="btn_CreateAlert">Create Alert</button>

											<button id="btnClearAllAlerts" type="button" class="btnMargin" i18n_key="btn_ClearAllAlerts">Clear All Alerts</button>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr>
						<td class="ui-widget ui-widget-content ui-corner-all" style='height:270px;' valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="BookingInfo_FlightDetails_lbl_PassengerInformation">Passenger Information</p>
												</td>
												<td>
													<div id='divPaxHD' class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
														<table id="tblPaxAdt" class="scroll" cellpadding="0" cellspacing="0"></table>
												</td>
											</tr>
											<tr>
												<td  class='singleGap'></td>
											</tr>
											<tr>
												<td><div id='divInfantPane'>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td i18n_key="BookingInfo_Infants"><b>Infant(s)</b></td>
														</tr>
														<tr>
															<td>
																<table id="tblPaxInf" class="scroll" cellpadding="0" cellspacing="0"></table>
															</td>
														</tr>
													</table></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align='right'> 	
										<u:hasPrivilege privilegeId="xbe.res.alt.acc.noshow.tax.reverse">
										<button id="btnReverseNoShow" type="button" class="btnMargin" title="" style="width:auto">Reverse No-Show tax</button>
										</u:hasPrivilege>
																
										<button id="btnNameChange" type="button" class="btnMargin" title="" style="width: 100px" i18n_key="btn_NameChange">Name Change</button>
										<u:hasPrivilege privilegeId="xbe.res.alt.acc.adj.multiple">
											<button id="btnGroupCharge" type="button" class="btnMargin" title="" i18n_key="btn_ChargeAdjustment">Charge Adjustment</button>
										</u:hasPrivilege>
										
										<u:hasPrivilege privilegeId="xbe.res.alt.acc.ref.multiple">
											<button id="btnGroupRefund" type="button" class="btnMargin" style="width: 100px" i18n_key="btn_GroupRefund">Group Refund</button>
										</u:hasPrivilege>
										
										<button id="btnUploadFromCSV" type="button" class="btnMedium" style="display: none;" i18n_key="btn_Upload From CSV">Upload From CSV</button>
										<button id="btnCSVUploadSampleFile" type="button" class="btnMedium" style="display: none;" i18n_key="btn_SampleFile">Sample File</button>
										
										<u:hasPrivilege  privilegeId="xbe.res.view.paxnames">		
										<button id="btnPaxNames" type="button" class="btnMedium" i18n_key="BookingInfo_PassengerNames">Pax Names</button>
										</u:hasPrivilege>	

										<button id="btnPaxDetails" type="button" class="btnMargin" i18n_key="BookingInfo_PassengerDetails">Pax Details</button>
										
										<button id="btnInternationalFlight" type="button" class="btnMargin" i18n_key="">Int'l Flights</button>
									
										<button id="btnAddInfants" type="button" class="btnMargin" i18n_key="btn_AddInfant">Add Infant</button>
									
										<button id="btnSaveInfants" type="button" class="btnMargin" style="width:auto; display:none;" i18n_key="btn_saveInfant">Save Infant</button>
									
										<button id="btnRemovePax" type="button" class="btnMargin" i18n_key="btn_RemovePassenger">Remove Pax</button>
									
										<button id="btnSplit" type="button" class="btnMargin" i18n_key="btn_Split">Split</button>
										
										<button id="btnAdvPaxSel" type="button" class="btnMargin"  style="width:auto;" i18n_key="">Adv Pax Sel</button>
										
									</td>
								</tr>
							</table>
							<div id="sampleFileIFrameDiv"></div>
						</td>
					</tr>
					<tr>
						<td class='singleGap'></td>
					</tr>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='0' cellspacing='1'>
								<tr>
									<td width='50%' valign='top'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width='100%' border='0' cellpadding='1' cellspacing='0'>
														<tr>
															<td width="0px" i18n_key="xbe_passenger_UserNotes">User Notes : 
                                                                <select id="selClassification" name="contactInfo.userNoteType" class="aa-input" style="width:145px;" tabindex="31">
                                                                   <option value="PUB">Public</option>
                                                                   <option value="PVT">Private</option>
                                                                </select>
                                                            </td>
															<td align='right'>
															    <button id="btnAddUserNote" type="button" class="btnMargin ui-state-default ui-corner-all" title="Add User Note" style="width:auto;" i18n_key="xbe_passenger_AddUserNotes">Add User Note</button>
															    <button id="btnSaveUserNote" type="button" class="btnMargin ui-state-default ui-corner-all" title="Save Notes" style="width:auto;" >Save User Note</button>
																<button id="btnViewNotes" type="button" class="btnMargin" title="View Notes" i18n_key="xbe_passenger_ViewNotes">View Notes</button>
															</td>
														</tr>
														<tr>
															<td colspan='2'>
																<textarea class='aa-input' id='txtUNotes' style='height:40px;width:450px;'></textarea>
															</td>
														</tr>
														<tr id="trEndosmentNotes">
															<td colspan='2'>
																<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tr>
																<td i18n_key="">Itinerary Endorsements : </td>
																<td align='right'>
																	<button id="btnViewEnNotes" type="button" class="btnMargin" style="display:none" title="View Endorsement Notes" i18n_key="">View Endorsement Notes</button>
																</td>
																</tr>
																<tr>
																	<td colspan='2'>
																		<textarea class='aa-input' id='txtEndosment' style='height:40px;width:450px;'></textarea>
																	</td>
																</tr>
																</table>
															</td>
															
														</tr>
													</table>
												</td>
											</tr>											
										</table>
									</td>
									<td width='50%' valign='top' style='height:115px;'><div id='divPriceSummary'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<thead>
												<tr>
													<td width='29%' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL' align='center'>
														<table>
															<tr>
																<td i18n_key="BookingInfo_lbl_TicketPrice">
																	<b>Ticket Price </b>
																</td>
																<td>
																	<div id='divPBTxnCurr' class='txtBold'></div>
																</td>
															</tr>
														</table>
													</td>
													<td id='tdPaxNoHD' width='10%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="BookingInfo_lbl_NoOfPassengers"><b>No. of<br>Pax</b></td>
													<td id='tdFareHD' width='13%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="BookingInfo_lbl_Fare"><b>Fare</b></td>
													<td id='tdTaxHD' width='9%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="BookingInfo_lbl_Tax"><b>Tax</b></td>
													<td id='tdSurChgHD' width='9%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="BookingInfo_lbl_Surcharge"><b>Sur</b></td>
													<td id='tdOtherHD' width='10%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="BookingInfo_lbl_Other"><b>Other</b></td>
													<td id='tdTotalHD' width='20%' class='thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR' align='center' i18n_key="BookingInfo_lbl_Total"><b>Total</b></td>
												</tr>
											</thead>
											<tbody id="paxPriceTemplete">
											</tbody>
											<tfoot>											
												<tr id="trGoquoAnciAmount">
													<td id='tdGoquoAnciLbl' class='thinBorderB thinBorderR thinBorderL rowHeight' colspan='6' ><div id="divGoquoAnciLabel" class="divGoquoAnciLabel" i18n_key="BookingInfo_lbl_GoquoAmount">GOQUO Ancillary</div></td>
													<td id='tdGoquoAnciAmt' class='thinBorderB thinBorderR' align='right'><div id='divGoquoAnciAmount' class='divGoquoAnciAmount'>0</div></td>
												</tr>
												<tr id="trFareDiscount">
													<td id='tdFareDiscountLbl' class='thinBorderB thinBorderR thinBorderL rowHeight' colspan='6' ><div id="divDiscLabel" class="divDiscLabel" i18n_key="CancelRes_lbl_TotalDiscounts">Total Discount</div></td>
													<td id='tdFareDiscountAmt' class='thinBorderB thinBorderR' align='right'><div id='divPBFareDiscount' class='divPBFareDiscount' i18n_key="">0</div></td>
												</tr>
												<tr id="trTotal">
													<td id='tdTotalLbl' class='thinBorderB thinBorderR thinBorderL rowHeight' colspan='6'  i18n_key="BookingInfo_lbl_Total"><b>Total</b></td>
													<td id='tdTotalAmt' class='thinBorderB thinBorderR' align='right'><div id='divPBTotAmt' class='txtBold'>0</div></td>
												</tr>
												<tr id="trBalance">
													<td id='tdBalanceLbl' class='thinBorderB thinBorderR thinBorderL rowHeight' i18n_key=""><b>Total Balance</b></td>
													<td class='thinBorderB thinBorderR' align='right'><div id='divPBTotAmtBase' class='txtBold' >0</div></td>
													<td id='tdPBTotAmtBaseLbl' class='thinBorderB thinBorderR' align="center" colspan='2'><div id='divPBTotAmtCurr' class='txtRed' >0</div></td>
													
													<td id='tdTPBTotAmtOhdLbl' class='thinBorderB thinBorderR thinBorderL rowHeight'  colspan='2' i18n_key="BookingInfo_FlightDetails_lbl_ToatalPrice"><b>Total Price</b></td>
													<td class='thinBorderB thinBorderR' align='right'><div id='divPBTotAmtOhd' class='txtBold'>0</div></td>
												</tr>
												<tr id="trFareDiscountCredit">
													<td id='tdDiscountCreditLbl' class='thinBorderB thinBorderR thinBorderL rowHeight' colspan='6' ><div id="divDiscLabel" class="divDiscLabel" i18n_key="CancelRes_lbl_TotalDiscounts">Total Discount</div></td>
													<td id='tdDiscountCreditAmt' class='thinBorderB thinBorderR' align='right'><div id='divPBFareDiscount' class='divPBFareDiscount'>0</div></td>
												</tr>
											</tfoot>
										</table>
										</div>
									</td>
									<td width='50%' valign='top' style='height:115px;'><div id='divTotal'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<thead>
												<tr>
													<td width='29%' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL' align='center'>
														<table>
															<tr>
																<td >
																	<b>Passenger Types </b>
																</td>
																<td>
																	<div id='divPBTxnCurr' class='txtBold'></div>
																</td>
															</tr>
														</table>
													</td>
													<td id='tdPaxNoHD' width='10%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>No. of<br>Pax</b></td>
												</tr>
											</thead>
											<tbody id="paxCountTemplete">
											</tbody>
											<tr>
													<td width='29%' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL' align='center'>
														<table>
															<tr>
																<td i18n_key="BookingInfo_lbl_TicketPrice">
																	<b class='txtBold'>Payment Information </b>
																</td>																															</tr>
														</table>
													</td>
													<td id='tdPaxNoHD' width='10%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center'><b>Amount</b></td>
												</tr>
											<tfoot>								
												<tr id="trTotal">
													<td id='tdTPBTotAmtOhdLbl' class='thinBorderB thinBorderR thinBorderL rowHeight'  i18n_key="BookingInfo_FlightDetails_lbl_ToatalPrice"><b>Total Price</b></td>
													<td class='thinBorderB thinBorderR' align='right'><div id='divPBGrandTotAmtOhd'>0.00</div></td>
												</tr>
											</tfoot>
										</table>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>					
					<tr>
						<td>
							<table border='0' cellpadding='0' cellspacing='1' width='97%'>
								<tr>
						
									<td width='10%'><select id='selTransfer' name='selTransfer'
										class="aa-input" style='width: 153px;'></select></td>
									<td width='10%'>
										<button id="btnTransfer" type="button" class="btnMargin" title="Transfer" i18n_key="BookingInfo_btn_Transfer">Transfer</button>
									</td>
						
									<td width='10%'>
										<select id='selExtend' name='selExtend'
											class="aa-input" style='width: 75px;'></select>
										<select id='selWLPriority' name='selWLPriority'
											class="aa-input" style='width: 50px;'></select>
									</td>
									<td width='5%'>										
										<input type="hidden" id="hdnExtendDate"	name="hdnExtendDate" value="" />
									</td>									
									<td width='14%'>
										<button id="btnExtend" type="button" class="btnMargin" title="Extend Onhold" i18n_key="BookingInfo_btn_ExtendOnhold">Extend
											Onhold</button>
										<button id="btnWLPriority" type="button" class="btnMargin" title="Change Priority" i18n_key="">Chg. Priority</button>	
									</td>
											
									<u:hasPrivilege privilegeId="xbe.res.alt.itin.fare.mask">
										<td width='18%' align="right"><span id="fareMasklblSpn" i18n_key="">Itinerary Fare
												Mask</span></td>															
										<td width='7%'><select id="selItineraryFareMask"
											name="selItineraryFareMask" class="aa-input" style="width:50px"></select>
										</td>
									</u:hasPrivilege>
									<td width='1%'>
										<input type="hidden" id="hdnItineraryFareMask" name="hdnItineraryFareMask" value="" />										
									</td>
									<td width='13%' i18n_key="BookingInfo_lbl_BookingCategory">																			
										Booking Category 
									</td>	
									<td width='11%'>
										<select id='selBookingCategory' name='selBookingCategory' class="aa-input" style='width:70px;'></select>
									</td>
									<u:hasPrivilege privilegeId="xbe.res.alt.callorigin.country">
										<td width="12%" align="right">
											<span id="originCountrylblSpn" i18n_key="">Origin Of Call</span>
										</td>
										<td width="10%">
											<select id="selOriginCountryOfCall" name="selOriginOfCall" class="aa-input"
													size="1" style="width:145px"></select>
										</td>
									</u:hasPrivilege>


									<td>&nbsp;</td>
						
								</tr>
							</table>									
						</td>
					</tr>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
										<table border='0' cellpadding='0' cellspacing='1'>
											<tr>
												<td><select id='selITNLang' name='selITNLang' class="aa-input" style="width:80px;" ></select></td>
												<td>
													<button id="btnViewItn" type="button" class="btnMargin" title="View Itinerary" i18n_key="BookingInfo_btn_ViewItinerary">View Itinerary</button>
													<button id="btnEmailItn" type="button" class="btnMargin" title="Email Itinerary" i18n_key="BookingInfo_btn_EmailItinerary">Email Itinerary</button>
													<!--<button id="btnItnViewGrp" type="button" class="btnMargin">View Group Itinerary</button>-->
												</td>
												
													<td><span id="paxItnTxtSpan" i18n_key="BookingInfo_chk_pax">Pax</span></td>
													<td><input type='checkbox' id='chkPax' name='chkPax' class="noBorder" checked="checked" ></td>
												
												
													<td><span id="payItnTxtSpan" i18n_key="BookingInfo_chk_pay">Pay</span></td>
													<td><input type='checkbox' id='chkPay' name='chkPay' class="noBorder" ></td>
												
												
												
													<td><span id="chgTxtSpan" i18n_key="BookingInfo_chk_chg">Chg</span></td>
													<td><input type='checkbox' id='chkChg' name='chkChg' class="noBorder" ></td>
												
												<u:hasPrivilege  privilegeId="xbe.res.itn.withTAndC">
													<td>T&C</td>
													<td><input type='checkbox' id='chkTC' name='chkTC' class="noBorder" ></td>
												</u:hasPrivilege>
												
																								
											</tr>
										</table>
									</td>
									<td align='right'>
										<button id="btnViewInvoice" type="button" class="btnMargin" title="View" i18n_key="">View Invoice</button>
										<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.view">
											<button id="btnViewRequestHistory" type="button" class="btnMargin btnMedium ui-state-default ui-corner-all" onclick="loadStatuses()">View Request History</button>
										</u:hasPrivilege>
										<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
											<button id="btnCreateRequest" type="button" class="btnMargin btnMedium ui-state-default ui-corner-all">Create Request</button>
										</u:hasPrivilege>
										<button id="btnClose" type="button" class="btnMargin" title="Close" i18n_key="btn_Close">Close</button>
										<button id="btnBookMark" type="button" class="btnMargin"  title="BookMark" i18n_key="btn_BookMark">BookMark</button>
										<button id="btnReset" type="button" class="btnMargin" title="Reset" i18n_key="btn_Reset">Reset</button>
										<button id="btnEdit" type="button" class="btnMargin" title="Edit" i18n_key="btn_Edit">Edit</button>
										<button id="btnSave" type="button" class="btnMargin" title="Save" i18n_key="btn_Save">Save</button>
										<u:hasPrivilege privilegeId="xbe.res.alt.ohd.roll.fwd">
											<button id="btnRollForward" type="button" class="btnMargin" title="Roll Forward" i18n_key="btn_RollForward">Roll Forward</button>
										</u:hasPrivilege>
											<button id="btnCancel" type="button" class="btnMargin" title="Cancel Reservation" i18n_key="btn_CancelRes">Cancel Res.</button>
											<button id="btnVoidReservation" type="button" class="btnMargin" title="Void Reservation" i18n_key="btn_VoidRes">Void Res.</button>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div id="groupChargeAdjusmentPopUp" style="display: none;">
					<%@ include file='../modify/inc_PaxGroupChargeAdjust.jsp' %>
				</div>
				<div id="groupPaxRefundPopUp" style="display: none;">
					<%@ include file='../modify/inc_PaxGroupRefund.jsp' %>
				</div>
				<div id="paxNameChangePopUp" style="display: none;min-height: 260px">
					<%@ include file='../modify/inc_PaxNameChange.jsp' %>
				</div>
				<div id="requestHistoryView" style="display: none;min-height: 260px">
					<%@ include file='../modify/inc_requestHistoryVw.jsp' %>
				</div>
				<div id="requestSubmitPopup" style="display: none;min-height: 260px">
					<%@ include file='../../requestmanagement/RequestSubmitPopup.jsp' %>
				</div>
				<div id="populateFrequentCustomerPopUp" style="display: none;">
					<%@ include file='../../customer/AgentFrequentCustomerPopulatePopup.jsp' %>
				</div>
				<%@ include file='../reservation/inc_SSR.jsp' %>
				<%@ include file='displayGroupBookingItinerary.jsp' %>
				<div id="noShowTaxRefundPopUp" style="display: none;">
					<%@ include file='../modify/inc_NoShowTaxRefund.jsp' %>
				</div>
				<!-- Tool tip -->				
				<div id="toolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
					<center>
						<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
							<tr>
								<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
									<font size="4px" style="font-weight:bold" i18n_key=""> Alerts </font>
									<img src="../images/AA171_no_cache.gif">
								</td>
							</tr>
							<tr>
								<td style="padding-top:10px;" id="toolTipBody">
								</td>
							</tr> 
						</table>
					</center>
				</div>
				<div id="flexiToolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
					<center>
						<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
							<tr>
								<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
									<font size="6px" style="font-weight:bold" i18n_key=""> Flexibilities </font>
									<img src="../images/AA171_no_cache.gif">
								</td>
							</tr>
							<tr>
								<td style="padding-top:10px;" id="flexiToolTipBody">
								</td>
							</tr> 
						</table>
					</center>
				</div>
				<div id="bundledFareToolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
					<center>
						<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
							<tr>
								<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
									<font size="6px" style="font-weight:bold" i18n_key=""> Bundled Fare </font>
									<img src="../images/AA169_N_no_cache.gif">
								</td>
							</tr>
							<tr>
								<td style="padding-top:10px;" id="bundledFareToolTipBody">
								</td>
							</tr> 
						</table>
					</center>
				</div>
				<div id="openReturnToolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
					<center>
						<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
							<tr>
								<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
									<font size="4px" style="font-weight:bold" i18n_key=""> Open Return Info </font>
									<img src="../images/AA171_no_cache.gif">
								</td>
							</tr>
							<tr>
								<td style="padding-top:10px;" id="openReturnToolTipBody">
								</td>
							</tr> 
						</table>
					</center>
				</div>
				<div id="segmentInfoToolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.8;filter: alpha(opacity=80);" >
					<center>
						<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
							<tr>
								<td style="border-bottom:2px solid red;background-color:#f6f5b9;" align="center">	
									<table border="0px" cellpadding="1" cellpadding="1" width="100%">
										<tr>
											<td width="15%" align="left">
												<img src="../images/AA171_1_no_cache.gif" border="1">
											</td>
											<td width="85%" align="center">
												<font size="5px" style="font-weight:bold" i18n_key=""> Bus Service </font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="padding-top:7px;" id="segmentInfoToolTipBody" class="txtBold">
								</td>
							</tr> 
						</table>
					</center>
				</div>			
				<div id="returnValidityToolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
					<center>
						<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
							<tr>
								<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
									<font size="4px" style="font-weight:bold" i18n_key=""> Return Validity Info </font>
									<img src="../images/AA171_no_cache.gif">
								</td>
							</tr>
							<tr>
								<td style="padding-top:10px;" id="returnValidityToolTipBody">
								</td>
							</tr> 
						</table>
					</center>
				</div>
				<%@ include file='inc_OHDRollForward.jsp' %>
				<%@ include file='inc_applyNameChangeChargeConfirm.jsp' %>
				<%@ include file='inc_NameChangeRequoteOverride.jsp' %>
				<%@ include file='displayInvoices.jsp' %>
				<%@ include file='../reservation/inc_BlacklistPaxResAlertPop.jsp' %>
