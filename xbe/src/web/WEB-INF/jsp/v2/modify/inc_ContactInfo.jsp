				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<form method='post' action="" id="frmContact">
					<tr>
						<td class="ui-widget ui-widget-content ui-corner-all">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td width='20%'><p class='txtWhite txtBold' i18n_key="ContactDetails_lbl_ContactDetails">Contact Details</p></td>
												<td width='32%'>
													<div id='divBkgStatusCont' class='txtYellow txtBold'></div>
												</td>
												<td width='4%' i18n_key="SearchResults_lbl_PNR">
													<b>PNR : </b>
												</td>
												<td width='15%'>
													<div id='divPnrNoCont' class='txtYellow txtBold'></div>
												</td>
												<td align='right'>
													<div id='divAgentCont' class='txtYellow txtBold'></div>
												</td>
											</tr>
										</table>
										
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td align='left'>
										<table width='98%' border='0' cellpadding='1' cellspacing='0'>
											<tr>
												<td width="50%">
													<table id="tblTitle" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="BookingInfo_PassengerInfo_lbl_Title">Title :</td>
															<td>
																<select id='selTitle' name='contactInfo.title' class="aa-input" style="width:50px;"></select>
																<span id="spnTitleMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
												<td></td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblFirstName" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="Search_lbl_FirstName">First Name :</td>
															<td>
																<input type='text' id='txtFirstName' name='contactInfo.firstName' class="aa-input fontCapitalize" style="width:144px;" maxlength="50"/>
																<span id="spnFNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
												<td width="50%">
													<table id="tblLastName" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="Search_lbl_LastName">Last Name :</td>
															<td>
																<input type='text' id='txtLastName' name='contactInfo.lastName' class="aa-input fontCapitalize" style='width:144px;' maxlength="50"/>
																<span id="spnLNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblAddr1" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_StreetAddress">Street Address :</td>
															<td>
																<input type='text' id='txtStreet' name='contactInfo.street' class="aa-input fontCapitalize" style="width:144px;" maxlength="100"/>
																<span id="spnAddrMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>	
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table id="tblMobileNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_MobileNo">Mobile No :</td>
															<td>
																<input type='text' id='txtMobCntry' name='contactInfo.mobileCountry' title="Enter Your Country Code" class="aa-input" style="width:30px;" maxlength="4"/>
																<input type='text' id='txtMobArea' name='contactInfo.mobileArea' title="Enter Your Area Code" class="aa-input" style="width:30px;" maxlength="4"/>
																<input type='text' id='txtMobiNo' name='contactInfo.mobileNo' title="Enter Your Mobile Number" class="aa-input" style="width:75px;" maxlength="10" />
																<span id="spnMobileNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblAddr2" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%'></td>
															<td>
																<input type='text' id='txtAddress' name='contactInfo.address' class="aa-input" style="width:144px;" maxlength="100" />
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table id="tblPhoneNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_PhoneNo">Phone No :</td>
															<td>
																<input type='text' id='txtPhoneCntry' name='contactInfo.phoneCountry' class="aa-input" style="width:30px;" maxlength="4"/>
																<input type='text' id='txtPhoneArea' name='contactInfo.phoneArea' class="aa-input" style="width:30px;" maxlength="4""/>
																<input type='text' id='txtPhoneNo' name='contactInfo.phoneNo' class="aa-input" style="width:75px;" maxlength="10"/>
																<span id="spnPhoneNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblCity" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_TownCity">Town / City :</td>
															<td>
																<input type='text' id='txtCity' name='contactInfo.city' class="aa-input fontCapitalize" style="width:144px;" maxlength="20"/>
																<span id="spnCityMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table id="tblFax" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_Fax">Fax :</td>
															<td>
																<input type='text' id='txtFaxCntry' name='contactInfo.faxCountry' class="aa-input" style="width:30px;" maxlength="4"/>
																<input type='text' id='txtFaxArea' name='contactInfo.faxArea' class="aa-input" style="width:30px;" maxlength="4"/>
																<input type='text' id='txtFaxNo' name='contactInfo.faxNo' class="aa-input" style="width:75px;" maxlength="10"/>
																<span id="spnFaxMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>												
											</tr>
											<tr>
												<td width="50%">
													<table id="tblNationality" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="">Nationality :</td>
															<td>
																<select id='selNationality' size='1' class="aa-input" style='width:144px;' name='contactInfo.nationality'></select>
																<span id="spnNationalityMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table id="tblEmail" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_EmailAddress">Email Address : </td>
															<td>
																<input type='text' id='txtEmail' name='contactInfo.email' class="aa-input" style="width:144px;" maxlength="100"/>
																<span id="spnEmailMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>														
											</tr>
											<tr>
												<td width="50%">
													<table id="tblCountry" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_Country">Country :</td>
															<td>
																<select id="selCountry" name="contactInfo.country" class="aa-input" style="width:144px;"></select>
																<span id="spnCountryMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>																
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table id="tblZipCode" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_ZipCode">Zip Code :</td>
															<td>
																<input type='text' id='txtZipCode' name="contactInfo.zipCode" class="aa-input" style='width:144px;' maxlength='10'  tabindex="7" />
																<span id="spnZipCodeMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>																
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblPreferredLang" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_PreferredLanguage">Preferred Language :</td>
															<td>
																<select id="selPrefLang" name="contactInfo.preferredLang" class="aa-input" style="width:145px;"></select>														
																<span id="spnPreferredLangMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>																
															</td>
														</tr>
													</table>
												</td>
												<td width="50%">
												</td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblState" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_State">State :</td>
															<td>
																<select id="selState" name="contactInfo.state" class="aa-input" style="width:145px;"></select>														
																<span id="spnState"><%@ include file='../common/inc_MandatoryField.jsp' %></span>																
															</td>
														</tr>
													</table>
												</td>
												<td width="50%">
													<table id="tblTaxRegNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td  id="taxRegNoLabel" width='32%' i18n_key="ContactDetails_lbl_TaxRegNo">Tax Registration No </td>
															<td>
																<input type='text' id='txtTaxRegNo' name="contactInfo.taxRegNo" class="aa-input" style='width:145px;' maxlength='15'/>
																<span id="spnTaxRegNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr id="trEmgnContactInfo">
						<td class="ui-widget ui-widget-content ui-corner-all">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<p class='txtWhite txtBold' i18n_key="">Emergency Contact Details</p>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td align='left'>
										<table width='98%' border='0' cellpadding='1' cellspacing='0'>
											<tr>
												<td width="50%">
													<table id="tblEmgnTitle" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="BookingInfo_PassengerInfo_lbl_Title">Title :</td>
															<td>
																<select id='selEmgnTitle' name='contactInfo.emgnTitle' class="aa-input" style="width:50px;"></select>
																<span id="spnEmgnTitleMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>										
												<td></td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblEmgnFirstName" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="Search_lbl_FirstName">First Name :</td>
															<td>
																<input type='text' id='txtEmgnFirstName' name='contactInfo.emgnFirstName' class="aa-input fontCapitalize" style="width:145px;" maxlength="50"/>
																<span id="spnEmgnFNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table id="tblEmgnLastName" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="Search_lbl_LastName">Last Name :</td>
															<td>
																<input type='text' id='txtEmgnLastName' name='contactInfo.emgnLastName' class="aa-input fontCapitalize" style='width:145px;' maxlength="50"/>
																<span id="spnEmgnLNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td width="50%">
													<table id="tblEmgnPhoneNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_PhoneNo">Phone No :</td>
															<td>
																<table border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td>
																			<input type='text' id='txtEmgnPhoneCntry' name='contactInfo.emgnPhoneCountry' class="aa-input" style="width:30px;" maxlength="4"/>
																			<input type='text' id='txtEmgnPhoneArea' name='contactInfo.emgnPhoneArea' class="aa-input" style="width:30px;" maxlength="4""/>
																			<input type='text' id='txtEmgnPhoneNo' name='contactInfo.emgnPhoneNo' class="aa-input" style="width:75px;" maxlength="10"/>
																		</td>
																		<td>
																			<span id="spnEmgnPhoneNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td>
													<table id="tblEmgnEmail" width='100%' border='0' cellpadding='0' cellspacing='1'>
														<tr>
															<td width='32%' i18n_key="ContactDetails_lbl_EmailAddress">Email :</td>
															<td>
																<input type='text' id='txtEmgnEmail' name='contactInfo.emgnEmail' class="aa-input" style="width:145px;" maxlength="100"/>
																<span id="spnEmgnEmailMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
							</table>
						</td>
					</tr>
					</form>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr>
						<td align='right'>
							
								<button id="btnEditContact" type="button" class="btnMargin btnMedium" tabindex="18" i18n_key="btn_Edit">Edit</button>
								<button id="btnSaveContact" type="button" class="btnMargin btnMedium" tabindex="19" i18n_key="btn_Save">Save</button>
								<button id="btnCancelSave" type="button" class="btnMargin btnMedium" tabindex="20" i18n_key="btn_Cancel">Cancel</button>
							
						</td>
					</tr>
				</table>