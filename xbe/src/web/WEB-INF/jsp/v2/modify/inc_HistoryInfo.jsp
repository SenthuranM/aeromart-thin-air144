
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class="ui-widget ui-widget-content ui-corner-all">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table border='0' cellpadding='0' cellspacing='0' width='100%'>
											<tr>
												<td width='20%'><p class='txtWhite txtBold'i18n_key="History_lbl_PNRModificationHistory">PNR Modification History</p></td>
												<td width='32%'>
													<div id='divBkgStatusHstr' class='txtYellow txtBold'></div>
												</td>
												<td width='6%'>
												<b>PNR : </b>
												</td>
												<td width='13%'>
													<div id='divPnrNoHstr' class='txtYellow txtBold'></div>
												</td>
												<td align='right'>
												<div id='divAgentHstr' class='txtYellow txtBold'></div>
												</td>
												
											</tr>
										</table>
									</td>
								</tr>													
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td align='center'>
										<table width='100%' border='0' cellpadding='1' cellspacing='0'>
											<tr>
												<td width='20%'>
													<p class='txtBold' i18n_key="History_lbl_CarrierCode">Carrier Code</p>
												</td>
												<td>
													<select id='selCarrierCodes' name='selCarrierCodes' class="aa-input" style="width:100px;"></select>				
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<table id="tblHistory" style='width:850px;' class="scroll" cellpadding="0" cellspacing="0"></table>
												</td>
											</tr>
								  		</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<button id="btnPrintHistory" type="button" class="btnMargin">Print History</button>
	