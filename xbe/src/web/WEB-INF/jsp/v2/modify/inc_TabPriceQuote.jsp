
<table width='100%' border='0' cellpadding='1' cellspacing='0'
	id='tblPaxTopMost'>
	<tr>
		<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
			<table id="tblFlights" class="scroll" cellpadding="1" cellspacing="0"></table>
		</td>
	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>
	<tr>
		<td valign='top'>
			<table width='97%' border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td class='singleGap'></td>
				</tr>
				<tr>
					<td>
						<table width='100%' border='0' cellpadding='0' cellspacing='1'>
							<tr>
								<td width='50%'
									class="ui-widget ui-widget-content ui-corner-all"
									style='height: 280px;' valign='top'>
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td class='paneHD'>
												<div id='divHDPane1' class='txtWhite txtBold'></div>
											</td>
										</tr>
										<tr>
											<td class='singleGap'></td>
										</tr>
										<tr>
											<td>
												<table id="tablPane1" class="scroll" cellpadding="0"
													cellspacing="0"></table>
											</td>
										</tr>
										<tr>
											<td class='singleGap'></td>
										</tr>
										<tr>
											<td align='center'>
												<table width='90%' border='0' cellpadding='0'
													cellspacing='0'>
													<tr>
														<td width='15%' i18n_key="CancelRes_lbl_Credit">Credit :</td>
														<td width='35%' i18n_key=""><div id='divCredit' class='txtBold'></div>
														</td>
														<td width='15%' i18n_key="CancelRes_lbl_Due">Due :</td>
														<td width='35%'><div id='divDue' class='txtBold'></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td width='50%'
									class="ui-widget ui-widget-content ui-corner-all"
									style='height: 280px;' valign='top'>
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td class='paneHD' valign="top">
												<div id='divHDPane2' class='txtWhite txtBold'></div>
											</td>
										</tr>
										<tr>
											<td class='doubleGap' valign="top"></td>
										</tr>
										<tr>
											<td align='left' valign="top">
												<table width='100%' border='0' cellpadding='0'
													cellspacing='0'>
													<tr>
														<td><label class='txtBold'>&nbsp; </label></td>
														<td align='left' valign="top" width="25%"><label
															class='txtBold confOverideOnly overideDisplay' i18n_key="CancelRes_lbl_OverrideBasis">Override
																Basis</label></td>
														<td align='left' valign="top"><select
															id="selDefineCanType" name="selDefineCanType" size="1"
															class="aa-input aa-requiredField confOverideOnly overideDisplay"
															style="width: 150px;">
																<option value="" i18n_key="CancelRes_list_select">-- select --</option>
																<option value="V" i18n_key="CancelRes_list_Value">Value</option>
																<option value="PF" i18n_key="CancelRes_list_PercentageOfFare">Percentage of Fare</option>
																<option value="PFS" i18n_key="CancelRes_list_PercentageOfFareSurcharge">Percentage of Fare &
																	Sur.Chg</option>
														</select> <font class="mandatory confOverideOnly overideDisplay">*</font>
														</td>
													</tr>
													<tr>
														<td><label class='txtBold'>&nbsp; </label></td>
														<td align='left' valign="top" width="25%"><label
															id='lblRouteType' name="lblRouteType"
															class='txtBold confOverideOnly' i18n_key="CancelRes_lbl_ApplicableBasis">Applicable Basis</label>
														</td>
														<td align='left' valign="top"><select
															id="selDefineRouteType" name="selDefineRouteType"
															size="1"
															class="aa-input aa-requiredField confOverideOnly overideDisplay"
															style="width: 150px;">
																<option value="" i18n_key="CancelRes_list_select">-- select --</option>
																<option value="POND" i18n_key="CancelRes_list_PerOriginAndDestination">Per OND</option>
																<option value="TOT" i18n_key="CancelRes_list_Total">Total</option>
														</select></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align='center' valign="top">
												<table class="scroll" cellpadding="1" cellspacing="0"
													width='90%'>
													<tbody id="tablPane2"></tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td class='doubleGap' valign="top"></td>
										</tr>
										<tr>
											<td align='right' valign="top">
												<input type='hidden' id='txtCNXHDNAdt' name='txtCNXHDNAdt'>
												<input type='hidden' id='txtCNXHDNChld' name='txtCNXHDNChld'>
												<input type='hidden' id='txtCNXHDNInf' name='txtCNXHDNInf'>
												<button id="btnOverride" type="button" class="btnMargin" i18n_key="btn_Override">Override</button>
												<button id="btnApply" type="button" class="btnMargin" i18n_key="btn_Apply">Apply</button>
											</td>
										</tr>
										<tr>
											<td class='doubleGap' valign="top"></td>
										</tr>
										<tr>
											<td id="flexiInfo" align='left' valign="bottom"
												style='height: 170px; display: none;' class='txtBold' i18n_key="">Remaining
												Flexibilities: <span id="spnFlexibilities"
												style="color: red;" i18n_key="">No more flexibilities available
													for the cancelled segment</span></span>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width='50%' class="ui-widget ui-widget-content ui-corner-all"
						style='height: 280px;' valign='top'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td class='paneHD'>
									<div id='divHDPane3' class='txtWhite txtBold'></div>
								</td>
							</tr>
							<tr>
								<td>
									<table id="tablPane3" class="scroll" cellpadding="0"
										cellspacing="0"></table>
								</td>
							</tr>
							<tr id="trUserNote">
								<td><label i18n_key="BookingInfo_lbl_UserNotes">User Notes : </label>
								<input id="txtUserNote" type="text" size="124s">
								 <select id="selClassification"  class="aa-input" style="width:135px;" tabindex="31">
                                      <option value="PUB">Public</option>
                                      <option value="PVT">Private</option>
                                 </select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>
	<tr>
		<td align='right'>
			<button id="btnRQConfirm" type="button" class="btnMargin ui-state-default ui-corner-all"
				tabindex="27" i18n_key="btn_Confirm">Confirm</button>
			<button id="btnRQReset" type="button" class="btnMargin ui-state-default ui-corner-all" tabindex="28" i18n_key="btn_Reset">Reset</button>
			<button id="btnRQCancel" type="button" class="btnMargin ui-state-default ui-corner-all"
				tabindex="29" i18n_key="btn_Cancel">Cancel</button>
		</td>
	</tr>

</table>
