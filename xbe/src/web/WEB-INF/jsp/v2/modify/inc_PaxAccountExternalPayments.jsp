												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td style='height:420px;' valign='top'>
															<table id="tblExternalPay" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>
													<tr>
														<td class='doubleGap'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																<tr>
																	<td width='20%' i18n_key="SearchResults_lbl_Status">Status : </td>
																	<td width='80%'>
																		<select id='selStatus' name='selStatus' class="aa-input" tabindex='1'>
																			<option value=''></option>
																			<option value='T1' i18n_key="PaxAccDetails_Test1">Test 1</option>
																			<option value='T2' i18n_key="PaxAccDetails_Test2">Test 2</option>
																		</select>
																		<%@ include file='../common/inc_MandatoryField.jsp' %>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td align='right'>
															<button id="btnEPUpdate" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Update">Update</button><button id="btnEPClose" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Close">Close</button>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>		
												</table>