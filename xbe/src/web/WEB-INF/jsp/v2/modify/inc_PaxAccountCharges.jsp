												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td>
															<!-- * All charges are displayed in airline currency -->
														</td>
													</tr>
													<tr>
														<td style='height:300px;' valign='top'>
															<table id="tblCharges" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpading='0' cellspacing='0'>
																<tr>
																	<td width='14%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_TotalCharges">Total Charges :</td>
																	<td width='21%'><div id='divChgTotal' class='txtMediumSmall txtBold'></div></td>
																	<td width='10%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_Balance">Balance :</td>
																	<td width='25%'><div id='divChgBalance' class='txtMediumSmall txtBold'></div></td>
																	<td width='14%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_ActualCredit">Actual Credit :</td>
																	<td width='16%'><div id='divActualChgCredit' class='txtMediumSmall txtBold'></div></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td style="height:100px;">
															<table id="chargesAdjustmentInput" width='100%' border='0' cellpadding='1' cellspacing='0'>
																<tr>
																	<td width='20%' i18n_key="PaxAccDetails_AdjustmentAmount">Adjustment Amount : </td>
																	<td width='80%'>
																		<table border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td>
																					<input id="txtAdjustment" name="txtAdjustment" type="text" class="aa-input" style="width:120px" maxlength="8"/><%@ include file='../common/inc_MandatoryField.jsp' %>
																				</td>
																				<td>
																					&nbsp;
																				</td>
																				<td>
																					<div id='divChjgAdjustCurr' class='txtBold'></div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td i18n_key="PaxAccDetails_AirlineSegment">Airline Segment : </td>
																	<td>
																		<select id='selAirlineOnd' name='selAirlineOnd' size='1' class="aa-input" style="width:124px">
																		</select><%@ include file='../common/inc_MandatoryField.jsp' %>
																	</td>
																</tr>
																<tr>
																	<td i18n_key="PaxAccDetails_AdjustmentType">Adjustment Type : </td>
																	<td>
																		<select id='selAdjustmentType' name='selAdjustmentType' size='1' class="aa-input" style="width:200px">
																		</select>
																	</td>
																</tr>
																<tr>
																	<td i18n_key="PaxAccDetails_Charge">Charge : </td>
																	<td>
																		<select id='selChargeType' name='selChargeType' size='1' class="aa-input" style="width:124px">																			
																		</select>
																	</td>
																</tr>
																<tr>
																	<td valign='top' i18n_key="PaxAccDetails_Usernotes">User notes : </td>
																	<td valign='top'>
																		<table cellpadding='0' cellspacing='0'>
																			<tr>
																				<td><textarea id='txtUN' name='txtUN' rows='3' cols='60' class="aa-input"></textarea></td>
																				<td valign='top'>
																					<%@ include file='../common/inc_MandatoryField.jsp' %>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>
													<tr>
														<td align='right'>
															<table width='100%' border='0' cellpading='0' cellspacing='0'>
																<tr>
																	<td align='left' class="txtMediumSmall txtBold" id="amountWithSerivceTax"></td>
																	<td align='right'>
																		<button id="btnServiceTaxAmount" type="button" class="btnLarge" i18n_key="PaxAccDetails_btn_ServiceTax">Get Service Tax Amount</button>
																		<button id="btnHandlingFeeAmount" type="button" class="btnLarge" i18n_key="PaxAccDetails_btn_ServiceTax">Get Handling Fee</button>
																	</td>
																</tr>
															</table>
															
														</td>
													</tr>
													<tr>
														<td align='right'>
															<button id="btnConfirm" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Confirm">Confirm</button><button id="btnReset" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Reset">Reset</button><button id="btnClose" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Close">Close</button>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>													
												</table>
