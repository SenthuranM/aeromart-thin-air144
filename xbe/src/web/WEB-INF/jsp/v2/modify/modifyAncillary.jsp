<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<%@ include file='../common/inc_ShortCuts.jsp' %>
	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		DATA_ResPro["anciAvail"] = <c:out value="${requestScope.anciAvail }" escapeXml="false" />;
		DATA_ResPro["fareQuote"] = '<c:out value="${requestScope.fareQuote }" escapeXml="false" />';
		DATA_ResPro["flightRPHList"] = eval('(' + '<c:out value="${requestScope.flightRPHList }" escapeXml="false" />' + ')');
		DATA_ResPro["resPaxs"] = eval('(' + '<c:out value="${requestScope.resPaxs }" escapeXml="false" />' + ')');		
		DATA_ResPro["resContactInfo"] = eval('(' + '<c:out value="${requestScope.resContactInfo }" escapeXml="false" />' + ')');	
		DATA_ResPro["resPaySummary"] = eval('(' + '<c:out value="${requestScope.resPaySummary }" escapeXml="false" />' + ')');	
		DATA_ResPro["resPaxWisePayments"] = eval('(' + '<c:out value="${requestScope.resPaxWisePayments }" escapeXml="false" />' + ')');	
		DATA_ResPro["pnrNo"] =  '<c:out value="${requestScope.pnrNo }" escapeXml="false" />' ;
		DATA_ResPro["groupPNRNo"] = '<c:out value="${requestScope.groupPNRNo }" escapeXml="false" />' ;
		DATA_ResPro["version"] = '<c:out value="${requestScope.version }" escapeXml="false" />' ;
		DATA_ResPro["selCurrency"] = '<c:out value="${requestScope.selCurrency }" escapeXml="false" />' ;
		DATA_ResPro["systemParams"] = eval('(' + '<c:out value="${requestScope.systemParams }" escapeXml="false" />' + ')');
		DATA_ResPro["reservationStatus"] =  '<c:out value="${requestScope.reservationStatus }" escapeXml="false" />' ;
		DATA_ResPro["jsonOnds"] =  '<c:out value="${requestScope.jsonOnds }" escapeXml="false" />' ;
		DATA_ResPro["bundleFareList"] =  eval('(' +'<c:out value="${requestScope.bundleFareList }" escapeXml="false" />' + ')');

		var carrierCode = '<c:out value="${requestScope.carrierCode}" escapeXml="false" />';
		var jsonResPaymentSummary = null;
		var jsonPaySumObj = null;
		var jsonBkgInfo = null;	
		var isMinimumFare = false;
		
	//-->
	</script>
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/pageShortCuts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/tabAnci.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/tabSearchFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/tabPayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>		
	
	<script type="text/javascript" src="../js/v2/reservation/tabAnciSummary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/reservation/tabPassenger.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/modify/modifyAncillary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:none;">
		<div style="height:5px;"></div>
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
			<ul>
				<li><a href="#divAncillaryTab" style="width:90px" i18n_key="BookingInfo_FlightDetails_lbl_Ancillary">Ancillary</a></li>
				<li><a href="#divPaymentTab" style="width:90px" i18n_key="BookingInfo_FlightDetails_lbl_Payments">Payments</a></li>
			</ul>

			<form method='post' action="" id="frmMakeBkg">
				
				<div id="divAncillaryTab" style="padding-top: 5px">
					<%@ include file='../reservation/inc_TabAnci.jsp' %>
				</div>
				
				<div id="divPaymentTab" style="padding-top: 5px">
				 	<%@ include file='../reservation/inc_TabPayment.jsp' %>
				</div>
			</form>
		</div>	
	</div>
	<form method='post' action="" id="frmTabLoad"></form>
	<%@ include file='../common/inc_Credit.jsp' %>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
	<%@ include file='../reservation/inc_FlightStopOverDetails.jsp' %>
</body>
</html>

<script type="text/javascript">
$("#divLegacyRootWrapper").getLanguage();
</script>