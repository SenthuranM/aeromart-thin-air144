												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td style='height:250px;' valign='top'>
															<table id="tblPayments" class="scroll" cellpadding="0" cellspacing="0"></table>
														</td>
													</tr>
													<tr>
														<td class='singleGap'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpading='0' cellspacing='0'>
																<tr>
																	<td width='14%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_TotalPayments">Total Payments :</td>
																	<td width='21%'><div id='divPayTotal' class='txtMediumSmall txtBold'></div></td>
																	<td width='10%' class="txtMediumSmall txtBold" i18n_key="CancelRes_lbl_Balance">Balance :</td>
																	<td width='25%'><div id='divPayBalance' class='txtMediumSmall txtBold'></div></td>
																	<td width='14%' class="txtMediumSmall txtBold" i18n_key="PaxAccDetails_ActualCredit">Actual Credit :</td>
																	<td width='16%'><div id='divActualPayCredit' class='txtMediumSmall txtBold'></div></td>																	
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class='doubleGap'>
														</td>
													</tr>
														<tr>
															<td id="taxSegmentDetails" style="display:none;" i18n_key="PaxAccDetails_Segment">Segment : 
																<select id="selectedSegment" name="selectedSegment" class="aa-input">
																	<option></option>
																</select>
																Do charge adjustment only :
																<input type="checkbox" value="true" id="chkOnlyAdjust" name="chkOnlyAdjust" i18n_key="PaxAccDetails_DoChargeAdjustmentOnly"/>
															</td>
														</tr>
														<tr id="trRefundTxt">
															<td class='txtMedium txtBold' i18n_key="BookingInfo_PassengerInfo_lbl_RefundInfo">
																Refund Info
															</td>
														</tr>
														<tr id="payOptions">
															<td>
																<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																	<tr id="trPayOptionInputs">
																		<td>
																			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																				<tr>
																					<td width='20%' i18n_key="Ancillary_lbl_Amount">Amount : </td>
																					<td width='80%'>
																						<table border='0' cellpadding='0' cellspacing='0'>
																							<tr>
																								<td>
																									<input id="txtPayAmount" name="txtPayAmount" type="text" class="aa-input" style="width:120px" tabindex='1'/><input id="txtPayCurrency" name="txtPayCurrency" type="text" class="aa-input" style="width:30px" tabindex='1'/><%@ include file='../common/inc_MandatoryField.jsp' %>
																								</td>
																								<td>
																									&nbsp;
																								</td>
																								<td>
																									<div id='divPayTotalCurr' class='txtBold'  style="display:inline"></div>
																									<span style="display:none;" class="txtBold txtRed" id="PaxAccDetails_voidReservationRefundableNotice">Reservation Marked as VOID. Full refund is possible.</span>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td valign='top' i18n_key="PaxAccDetails_Usernotes">User notes : </td>
																					<td valign='top'>
																						<table cellpadding='0' cellspacing='0'>
																							<tr>
																								<td><textarea id='txtPayUN' name='txtPayUN' rows='4' cols='60' class="aa-input" tabindex='2'></textarea></td>
																								<td valign='top'>
																									<%@ include file='../common/inc_MandatoryField.jsp' %>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class='singleGap'>
																		</td>
																	</tr>
																	<tr id="trPayOptionSelections">
																		<td>
																			<table width='100%' border='0' cellpadding='1' cellspacing='0'>
																				
																					<tr id="trCA">
																						<td width='15%'>  
																							<input type='radio' id='radPaymentTypeC' name='radPaymentType' value='CASH' tabindex='3'/><label for="CASH" i18n_key="PassengerInfo_opt_Cash">Cash</label>
																						</td>
																						<td colspan="2"></td>
																					</tr>
																				
																				
																					<tr id="trOA">
																						<td width='15%'>  
																							<input type='radio' id='radPaymentTypeA' name='radPaymentType' value='ACCO' tabindex='4' checked="true"/><label for="ACCO"  i18n_key="PassengerInfo_opt_OnAccount">On Account</label>
																						</td>
																						<td width='45%'>
																							<select id='selAgents' name='selAgent' class="aa-input" style="width:250px;" tabindex='5'>
																							<option value=""></option>
																							<c:out value="${requestScope.reqPayAgents }" escapeXml="false" />
																							</select>
																						</td>
																						<td width='40%'><label id="lblAgentCurrency"></label></td>
																					</tr>
																				
																					<tr id="trCC">
																						<td width='15%'>
																							<input type='radio' id='radPaymentTypeR' name=radPaymentType value='CRED' tabindex='6'/><label for="CRED" i18n_key="PassengerInfo_opt_CreditCard">Credit Card</label>
																						</td>
																						<td>
																							<select id='selCardType' name='selCardType' class="aa-input" style="width:200px;display:none;" tabindex="5">
																							</select>
																						</td>
																						<td width='40%'></td>
																					</tr>
																					
																					<tr id="trBSP">
																						<td width='15%'>
																							<input type='radio' id='radPaymentTypeBsp' name=radPaymentType value='BSP' tabindex='6'/><label for="BSP" i18n_key="PassengerInfo_opt_BusinessSettlementPlan">BSP</label>
																						</td>
																						<td>
																							<select id='selCardType' name='selCardType' class="aa-input" style="width:200px;display:none;" tabindex="5">
																							</select>
																						</td>
																						<td width='40%'></td>
																					</tr>
																					<tr id="trOL">
																						<td width='15%'>  
																							<input type='radio' id='radPaymentTypeOL' name='radPaymentType' value='OFFLINE' tabindex='7'/><label for="OFFLINE">Offline</label>
																						</td>
																						<td colspan="2"></td>
																					</tr>
																				
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td  class='singleGap'></td>
																	</tr>
																	<tr id="agentCommissionInfo" style="display:none;">
																		<td>
																			<table width='80%' border='0' cellpadding='1' cellspacing='0'>
																				<tr>
																					<td>
																						<input type="checkbox" checked="checked" id="chkAgentCom" name="chkAgentCom"/>
																						<span class="txtBold" id="rmvAgentCommissionInfo" i18n_key="PaxAccDetails_RemoveAgentCommission">Remove agent commission, if applied</span>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td  class='singleGap'></td>
																	</tr>
																	<tr>
																		<td align='right'>																			
																			<button id="btnRecieptPrint" type="button" class="btnMargin" style="width:100px" i18n_key="PaxAccDetails_PrintReciept" >Print Reciept</button>
																			<!-- 
																			<u:hasPrivilege privilegeId="xbe.res.calc.tax.noshow">
																				<button id="btnCalculateTaxInfo" type="button" class="btnMargin" style="width:165px" i18n_key="PaxAccDetails_CalculateRefundableTax">Calculate Refundable Tax</button>
																			</u:hasPrivilege>
																			 -->
																			<button id="btnPayConfirm" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Confirm">Confirm</button><button id="btnPayReset" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Reset">Reset</button><button id="btnPayClose" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Close">Close</button>
																		</td>
																	</tr>
																	<tr>
																		<td  class='singleGap'></td>
																	</tr>	
																</table>
															</td>
														</tr>
													
													<tr id="creditCardDet">
														<td align='center'>
															<table width='100%' border='1' cellpadding='0' cellspacing='0'>
																<tr>
																	<td class="ui-widget ui-widget-content ui-corner-all" style='height:240px;' valign='top'>
																		<table width='100%' border='0' cellpadding='2' cellspacing='0'>
																			<tr>
																				<td class='paneHD' colspan='3'>
																					<p class='txtWhite txtBold' i18n_key="PaxAccDetails_CreditCardDetails">Credit Card Details</p>
																				</td>
																			</tr>
																			<tr>
																				<td class='doubleGap' colspan='3'></td>
																			</tr>
																			<tr>
																				<td width='60%' valign='top'>
																					<table width='100%' border='0' cellpadding='2' cellspacing='0'>
																						<tr>
																							<td width='40%' i18n_key="AdvSearch_lbl_CreditCardType">Card Type : </td>
																							<td width='60%' class='noPadding'>
																								<table>
																									<tr>
																										<td>
																											<select id="cardType" name="cardType" size="1" tabindex="1" style="width:100" class='aa-input'/>
																												<option id="cardOptions"></option>
																											</select>
																										</td>
																										<td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td i18n_key="AdvSearch_lbl_CreditCardNo">Card Number :</td>
																							<td>
																								<input type="text" id="cardNo" name="cardNo" tabindex="2" maxlength="20" size="20" class="aa-input txtBold"/>
																							</td>
																						</tr>
																						<tr>
																							<td i18n_key="PaxAccDetails_CardHoldersName">Card Holders Name :</td>
																							<td>
																								<input type="text" id="cardHoldersName" name="cardHoldersName" tabindex="3" maxlength="30" size="30" class="aa-input"/>
																							</td>
																						</tr>
																						<tr id="trCardEDate">
																							<td i18n_key="AdvSearch_lbl_ExpiryDate">Expiry Date :</td>
																							<td class='noPadding'>
																								<table>
																									<tr>
																										<td>
																											<input type="text" id="expiryDate" name="cardExpiryDate" tabindex="4" maxlength="4" size="4" class="aa-input"/>
																										</td>
																										<td>
																											<p class='txtBold' i18n_key="PaxAccDetails_MMYY">(MMYY)</p>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr id="trCardCVV">
																							<td i18n_key="PaxAccDetails_CardCVV">Card's CVV :</td>
																							<td>
																								<input type="text" id="cardCVV" name="cardCVV" tabindex="5" maxlength="4" size="4" class="aa-input"/>
																							</td>
																						</tr>
																						<tr id="trCardTotPayAmt">
																							<td i18n_key="PaxAccDetails_TotalRefundAmount">Total Refund Amount:</td>
																							<td>
																								<div id='divTotPay' class='txtBold'></div>
																							</td>
																						</tr>
																						<tr>
																							<td colspan='2'>
																								<table>
																									<tr>
																										<td i18n_key="PaxAccDetails_YourPaymentwouldBeprocessedIn">
																											Your Payment would be processed in 
																										</td>
																										<td>
																											<div id='divTxtCurr' class='txtBold txtRed'></div>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																				<td width='20%' align='center'>
																					<a href="#" onclick='UI_tabPayment.showVeriSign(); return false;'><img src="../images/VSign_no_cache.gif" border="0"/></a>
																				</td>																				
																			</tr>
																			<tr>
																				<td class='doubleGap' colspan='3'></td>
																			</tr>
																			<tr>
																				<td colspan='2'>
																					<table width="100%" border="0" cellpadding="0" cellspacing="1">
																					</table>
																				</td>
																				<td align='right'>																					
																					<button id="btnCRConfirm" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Confirm">Confirm</button><button id="btnCRCancel" type="button" class="btnMargin" i18n_key="PaxAccDetails_btn_Cancel">Cancel</button>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>