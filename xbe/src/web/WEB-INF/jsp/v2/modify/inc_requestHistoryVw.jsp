<table width='600' border='0' cellpadding='0' cellspacing='0' height="290">
	<tr>
		<td>
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
                <tr>
                    <td>
                        <p class='txtBold'>Requests for this PNR</p>
                    </td>
                    <td>
                        <div id='pnrName' class='txtWhite txtBold'></div>
                    </td>
                </tr>
				<tr>
					<div id='p_requestTable'></div>
				</tr>
				<tr>
					<td colspan="2" class='doubleGap'></td>
				</tr>
				<tr>
					<td colspan="2">
						<div id='divInfantNamePane'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td><b>History for this Request</b></td>
								</tr>
								<tr>
									<div id='p_requestHistory'></div>	
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td class='singleGap' colspan="2"></td>
	</tr>
	<tr id="buttonsForDiolog">
		<td align='right' colspan="2">
			<button id="statusesClose" type="button" class="btnMargin">Close</button>
		</td>
	</tr>
</table>
