				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr >
						<td class="ui-widget ui-widget-content ui-corner-all">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='rowHeight ui-widget' valign='top'> 
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='paneHD'>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td width='10%'><p class='txtBold' i18n_key="Ancillary_lbl_STATUS">&nbsp; STATUS : </p></td>
															<td width='42%'>
																<div id='divAnciBkgStatus' class='txtYellow txtBold'></div>
															</td>
															<td width='4%' i18n_key="SearchResults_lbl_PNR">
																<b>PNR : </b>
															</td>
															<td width='15%'>
																<div id='divAnciPnrNo' class='txtYellow txtBold'></div>
															</td>
															<td align='right'>
																<div id='divAnciAgent' class='txtYellow txtBold'></div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>															
							</table>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr id='trAnciSeat'>
						<td class="ui-widget ui-widget-content ui-corner-all">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='rowHeight ui-widget' valign='top'> 
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='paneHD'>
													<table>
														<tr>
															<td>
																<p class='txtWhite txtBold' i18n_key="Ancillary_lbl_passengerDetails">Passenger Seat Selection Details</p>
															</td>
															<td>
																<div class='txtWhite txtBold'></div>
															</td>
														</tr>
													</table>													
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:30px;' valign='top'>
										<table id="tblSeats" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>								
							</table>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>					
					<tr id='trAnciMeal'>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="Ancillary_lbl_Meal">Meals</p>
												</td>
												<td>
													<div class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:30px;' valign='top'>
										<table id="tblMeals" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					
					
					<tr id='trAnciBaggage'>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="Ancillary_lbl_Baggages">Baggages</p>
												</td>
												<td>
													<div class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:30px;' valign='top'>
										<table id="tblBaggages" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<tr>
						<td  class='singleGap'></td>
					</tr>
					
					
					
					<tr id='trAnciInsurance'>
						<td class="ui-widget ui-widget-content ui-corner-all"  valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="Ancillary_lbl_Insurance">Insurance</p>
												</td>
												<td>
													<div class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:15px;' valign='top'>
										<table id="tblInsurance" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>	
							</table>
						</td>
					</tr>					
					<tr>
						<td  class='singleGap'></td>
					</tr>
					<tr id='trAnciSSR'>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="Ancillary_lbl_SpecialServiceRequests">Special Service Requests</p>
												</td>
												<td>
													<div class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:30px;' valign='top'>
										<table id="tblSSR" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<tr>
						<td class='singleGap'></td>
					</tr>
					<tr id='trAnciAPService'>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="Ancillary_lbl_AirportServices">Airport Services</p>
												</td>
												<td>
													<div class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:30px;' valign='top'>
										<table id="tblAPService" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<tr>
						<td class='singleGap'></td>
					</tr>
					<tr id='trAnciAPTransfer'>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="">Airport Transfer</p>
												</td>
												<td>
													<div class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:30px;' valign='top'>
										<table id="tblAPTransfer" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<tr id='trAnciAutoCheckin'>
						<td class="ui-widget ui-widget-content ui-corner-all" valign='top'>
							<table width='100%' border='0' cellpadding='1' cellspacing='0'>
								<tr>
									<td class='paneHD'>
										<table>
											<tr>
												<td>
													<p class='txtWhite txtBold' i18n_key="">Automatic Checkin</p>
												</td>
												<td>
													<div class='txtWhite txtBold'></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style='max-height:30px;' valign='top'>
										<table id="tblAutoCheckin" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
										&nbsp;
									</td>
									<td align='right'>
										<button id="btnAnciEdit" type="button" class="btnMargin" i18n_key="btn_AddModify">Add/Modify</button>										
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table> 