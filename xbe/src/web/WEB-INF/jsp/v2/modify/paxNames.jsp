<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Passenger Names In Other Languages</title>
	<%@ include file='../common/inc_PgHD.jsp' %>
	<script type="text/javascript">
	
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		DATA_ResPro["mode"] = "<c:out value="${requestScope.mode }" escapeXml="false" />";
	</script>
	
	<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/modify/paxNames.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
</head>
<body class="legacyBody">
	<table style='width:600px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:580px;">
						<form method='post' action="" id="frmPaxNames">
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td class='paneHD'>
									<div id='divHDPane1' class='txtWhite txtBold'></div>
									</td>
								</tr>
								<tr>
									<td>
									<table id="tablPane1" class="scroll" cellpadding="0" cellspacing="0">
									</table>
									</td>
								</tr>
							</table>
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
									<div id="divLanguageCode" style="margin-top:0px;margin-left:20px;margin-right:20px;width:20px;"></div>
									</td>
									<td>
									<select id="selLanguageCode">
										<c:out value='${requestScope.reqLanguageList}' escapeXml='false' />
									</select>
									<font class="mandatory">&nbsp;*</font>
									</td>
									<td align='right'>
										<button id="btnEdit" type="button" class="btnMargin" i18n_key="btn_Edit">Edit</button>
										<button id="btnSave" type="button" class="btnMargin" i18n_key="btn_Save">Save</button>
										<button id="btnCancel" type="button" class="btnMargin" i18n_key="btn_Cancel">Cancel</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</td>
			</tr>			
	</table>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
<br /></body>
</html>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript">
$("#divPane").getLanguage();
</script>