<table width='600' border='0' cellpadding='0' cellspacing='0' height="290">
	<tr>
		<td>
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
                <tr>
                    <td>
                        <p class='txtBold' i18n_key="BookingInfo_PassengerInfo_lbl_Adults">Adult(s)</p>
                    </td>
                    <td>
                        <div id='divPaxHDName' class='txtWhite txtBold'></div>
                    </td>
                </tr>
				<tr>
					<td colspan="2" id="p_tblPaxAdtName">
							<table id="tblPaxAdtName" cellpadding="0" cellspacing="0"></table>
					</td>
				</tr>
				<tr>
					<td colspan="2" class='doubleGap'></td>
				</tr>
				<tr>
					<td colspan="2"><div id='divInfantNamePane'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td i18n_key="BookingInfo_PassengerInfo_lbl_Infants"><b>Infant(s)</b></td>
							</tr>
							<tr>
								<td id="p_tblPaxInfName">
									<table id="tblPaxInfName" cellpadding="0" cellspacing="0"></table>
								</td>
							</tr>
						</table></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td class='singleGap' colspan="2"></td>
	</tr>
	<tr id="paxNameChangeButtons">
		<td align='right' colspan="2">
			<u:hasPrivilege  privilegeId="xbe.tool.frequentcustomer.view">
				<button id="btnLoadFQPax" type="button" class="btnLarge ui-state-default ui-corner-all">Load FQ Pax Details</button>
			</u:hasPrivilege>
			<button id="btnNameChangeConfirm" type="button" class="btnMargin" i18n_key="btn_Confirm">Confirm</button>
			<button id="btnNameChangeReset" type="button" class="btnMargin" i18n_key="btn_Reset">Reset</button>
			<button id="btnNameChangeClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
		</td>
</table>