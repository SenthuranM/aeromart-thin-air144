<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<%@ include file='../common/inc_ShortCuts.jsp'%>	
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="-1"/>
	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = {};
		DATA_ResPro["systemParams"] = <c:out value="${requestScope.systemParams }" escapeXml="false" />;
		var jsonContactInfo = {};
		var jsonPaxAdtObj = {paxAdults: {},paxInfants: {}};
		var jsonPaxPrice = {};
		var jsonFltDetails = {};
		var jsonBkgInfo = {status:'',PNR:''};
		var jsonUserNotes = {};
		var jsonPaxPriceSummary = {};
		var jsonPaxPriceTo = {};
		var jsonResPaymentSummary = {};
		var jsonPaxWiseAnci = [];
		var jsonFirstFlightDate = null;
		var jsonLastFlightArrivalDate = null;
		var jsonLastFlightDate = null;
		var jsonAlertDetails = [];
		var jsonCarrierOptions = null;
		var jsonInsurance = null;
		var jsonGroupPNR = "<c:out value="${requestScope.groupPNR}" escapeXml="false" />";
		var jsonPNR = "<c:out value="${requestScope.pnr}" escapeXml="false" />";
		var jsonMarketingAirlineCode = "<c:out value="${requestScope.marketingAirlineCode}" escapeXml="false" />";
		var jsonAirlineCode = "<c:out value="${requestScope.airlineCode}" escapeXml="false" />";
		var jsonInterlineAgreementId = "<c:out value="${requestScope.interlineAgreementId}" escapeXml="false" />";
		var forwardMessage = "<c:out value="${requestScope.forwardmessage}" escapeXml="false" />";
		var isVoidReservation = "<c:out value="${requestScope.isVoidReservation}" escapeXml="false" />";
		var isAllowVoidReservation = "";
		var contactConfig = "";
		var validationGroup = "";
		var paxConfigAD = "";
		var paxConfigCH = "";
		var paxConfigIN = "";
		var jsonFlexiDetails = [];
		var jsonOpenReturnSegInfo = [];
		var jsonAdditionalSegInfo = [];
		var focMealEnabled = "<c:out value="${requestScope.focMealEnabled}" escapeXml="false" />";
		var jsonPaxWiseAnciForMod = [];
		var jsonEndorsements = {};
		var serviceTaxApplicableCountries = "";
	    	var taxRegistrationNumberLabel = "<c:out value="${requestScope.taxRegistrationNumberLabel}" escapeXml="false" />";
	    	var hasServiceTaxApplied = true;
	//-->
	</script>
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/pageShortCuts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/modify/tabBookingInfo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/modify/tabContactInfo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/modify/tabAnciInfo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/reservation/tabPayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
	
	<script type="text/javascript" src="../js/v2/modify/tabHistoryInfo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/modify/reservation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/modify/viewRequestStatuses.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:none;">
		<div style="height:5px;"></div>
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:640px;">
			<ul>
				<li><a href="#divBkgInfo" style="width:140px" i18n_key ="BookingInfo_lbl_BookingInfo">Booking Information</a></li>
				<li><a href="#divContact" style="width:100px" i18n_key ="ContactDetails_lbl_ContactDetails">Contact Details</a></li>
				<li><a href="#divPayment" style="width:80px" i18n_key ="BookingInfo_FlightDetails_lbl_Payments">Payment</a></li>
				<li><a href="#divHistory" style="width:80px" i18n_key ="BookingInfo_FlightDetails_lbl_History">History</a></li>
				<li><a href="#divAnci" style="width:80px" i18n_key ="BookingInfo_FlightDetails_lbl_Ancillary">Ancillary</a></li>
			</ul>
			
				<div id="divBkgInfo" style="padding-top: 5px;">
					<%@ include file='inc_BookingInfo.jsp' %>
				</div>
				
				<div id="divContact" style="padding-top: 5px;">
					<%@ include file='inc_ContactInfo.jsp' %>
				</div>
				
				<div id="divPayment" style="padding-top: 5px;">
					<%@ include file='../reservation/inc_TabPayment.jsp' %>
				</div>
				
				<div id="divHistory" style="padding-top: 5px;">
				 	<%@ include file='inc_HistoryInfo.jsp' %>
				</div>
				
				<div id="divAnci" style="padding-top: 5px;overflow-y:auto;height:575px ">
					<%@ include file='inc_AnciInfo.jsp' %>
				</div>
			<form method='post' action="" id="frmModiBkg">	
				<input type="hidden" name="pnrNo" id="pnrNo"/>
				<input type="hidden" name="groupPNRNo" id="groupPNRNo"/>
				<input type="hidden" name="fltSegment" id="fltSegment"/>
				<input type="hidden" name="version" id="version"/>
				<input type="hidden" name="resSegments" id="resSegments"/>
				<input type="hidden" name="resPax" id="resPax"/>
				<input type="hidden" name="pgMode" id="pgMode"/>
				<input type="hidden" name="ohdReleaseTime" id="ohdReleaseTime"/>       
				<input type="hidden" name="resHasInsurance" id="resHasInsurance"/>
				<input type="hidden" name="resNoOfAdults" id="resNoOfAdults"/>
				<input type="hidden" name="resNoOfchildren" id="resNoOfchildren"/>
				<input type="hidden" name="resNoOfInfants" id="resNoOfInfants"/>
				<input type="hidden" name="anciAvail" id="anciAvail" />
				<input type="hidden" name="resOwnerAgent" id="resOwnerAgent"/>
				<input type="hidden" name="resContactInfo" id="resContactInfo"/>
				<input type="hidden" name="resPaySummary" id="resPaySummary"/>
				<input type="hidden" name="resPaxWisePayments" id="resPaxWisePayments"/>
				<input type="hidden" name="flexiAlertInfo" id="flexiAlertInfo"/>
				<input type="hidden" name="modifyFlexiBooking" id="modifyFlexiBooking"/>
				<input type="hidden" name="modifyStatus" id="modifyStatus"/>
				<input type="hidden" name="bookingType" id="bookingType"/>
				<input type="hidden" name="addGroundSegment" id="addGroundSegment"/>
				<input type="hidden" name="selectedOnd" id="selectedOnd"/>
				<input type="hidden" name="flightDetails" id="flightDetails"/>
				<input type="hidden" name="selectedFlightSegRefNumber" id="selectedFlightSegRefNumber"/>
				<input type="hidden" name="selectedPnrSeg" id="selectedPnrSeg"/>
				<!--segment modification params-->
				<input type="hidden" name="allowModifyDate" id="allowModifyDate"/>
				<input type="hidden" name="allowModifyRoute" id="allowModifyRoute"/>
				<input type="hidden" name="ticketExpiryDate" id="ticketExpiryDate"/>
				<input type="hidden" name="ticketValidFrom" id="ticketValidFrom"/>
				<input type="hidden" name="ticketExpiryEnabled" id="ticketExpiryEnabled"/>
				<input type="hidden" name="isVoidReservation" id="isVoidReservation"/>
				<input type="hidden" name="selectedAlertSegment" id="selectedAlertSegment"/>
				<input type="hidden" name="transferAction" id="transferAction"/>
				<input type="hidden" name="jsonOnds" id="jsonOnds"/>
				<input type="hidden" name="resAlerts" id="resAlerts"/>
				
				<!-- Data to auto fill new pnr from this pnr -->
				<input type="hidden" name="oldAdultPaxInfo" id="oldAdultPaxInfo"/>
				<input type="hidden" name="oldInfantPaxInfo" id="oldInfantPaxInfo"/>
				<input type="hidden" name="oldContactInfo" id="oldContactInfo"/>
				<input type="hidden" name="oldNoOfAdults" id="oldNoOfAdults"/>
				<input type="hidden" name="oldNoOfChildren" id="oldNoOfChildren"/>
				<input type="hidden" name="oldNoOfInfants" id="oldNoOfInfants"/>
				<input type="hidden" name="blnIsFromNameChange" id="blnIsFromNameChange"/>
				<input type="hidden" name="paxNameDetails" id="paxNameDetails"/>
				<input type="hidden" name="gdsPnr" id="gdsPnr"/>
				<input type="hidden" name="reasonForAllowBlPax" id="reasonForAllowBlPax" />
				<input type="hidden" name="taxInvoicesExist" id="taxInvoicesExist"/>
			</form>
		</div>
		<div id="csvFileUploadDiv" style="display: none;">
			<iframe name="uploadResultFrame" id="uploadResultFrame" frameborder="0" style="width:100%;height:100%;border:0" src="PassengerDetailsParse.action"></iframe>
		</div>
	</div>
	<form method='post' action="" id="frmTabLoad"></form>
	<%@ include file='../common/inc_Credit.jsp' %>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
	<%@ include file='../reservation/inc_FlightStopOverDetails.jsp' %>
	<div id="autoCnxToolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
		<center>
			<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
				<tr>
					<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
						<font size="4px" style="font-weight:bold" i18n_key =""> Auto Cancellation Alerts </font>
						<img src="../images/AA171_no_cache.gif">
					</td>
				</tr>
				<tr>
					<td style="padding-top:10px;" id="autoCnxToolTipBody">
					</td>
				</tr> 
			</table>
		</center>
	</div>
	<div id="PromoCodeToolTip" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
		<center>
			<table border="0px" cellpadding="1" cellpadding="1" width="285px;">
				<tr>
					<td style="border-bottom:2px solid red;text-decoration:blink;background-color:#f6f5b9;" align="center">	
						<font size="4px" style="font-weight:bold" i18n_key =""> Promotion Information</font>
						<img src="../images/AA171_no_cache.gif">
					</td>
				</tr>
				<tr>
					<td style="padding-top:10px;" id="PromoCodeToolTipBody">
					</td>
				</tr> 
			</table>
		</center>
	</div>
</body>
</html>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript">
$("#divBookingTabs").getLanguage();
</script>