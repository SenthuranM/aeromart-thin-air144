<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Confirm Update</title>
	<%@ include file='../common/inc_PgHD.jsp' %>
	<%@ include file='../common/inc_ShortCuts.jsp'%>
	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		
		var strConfirmUpdateMode = "<c:out value="${requestScope.confirmUpdateMode}" escapeXml="false"/>";
		
			
	//-->
	</script>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/pageShortCuts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
	<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/jqValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/modify/confirmUpdate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapperPopUp" style="display:none;width:900px;">
		<table style='width:900px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:880px;">
						<form method='post' action="" id="frmConfirmUpdate">
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
										<label class='txtLarge txtBold' i18n_key="CancelRes_ConfirmUpdate">Confirm Update</label>
									</td>
								</tr>
								<tr>
									<td class='singleGap'></td>
								</tr>
								<tr>
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='1'>
											<tr>
												<td width='50%'  class="ui-widget ui-widget-content ui-corner-all" style='height:280px;' valign='top'>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td class='paneHD'>
																<div id='divHDPane1' class='txtWhite txtBold'></div>
															</td>
														</tr>
														<tr>
															<td class='singleGap'></td>
														</tr>
														<tr>
															<td>
																<table id="tablPane1" class="scroll" cellpadding="0" cellspacing="0"></table>
															</td>
														</tr>
														<tr>
															<td class='singleGap'></td>
														</tr>
														<tr>
															<td align='center'>
																<table width='90%' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td width='15%' i18n_key="CancelRes_Credit">Credit :</td>
																		<td width='35%'><div id='divCredit' class='txtBold'></div></td>
																		<td width='15%' i18n_key="CancelRes_Due">Due : </td>
																		<td width='35%'><div id='divDue' class='txtBold'></div></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td width='50%' id="tblOverideDisplay" class="ui-widget ui-widget-content ui-corner-all" style='height:280px;' valign='top'>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td class='paneHD' valign="top">
																<div id='divHDPane2' class='txtWhite txtBold'></div>
															</td>
														</tr>
														<tr>
															<td class='doubleGap' valign="top"></td>
														</tr>
														<tr>
															<td align='left' valign="top">
																<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																	<tr>
																		<td>
																			<label class = 'txtBold' >&nbsp; </label>
																		</td>
																		<td align='left' valign="top" width="25%">
																			<label class = 'txtBold confOverideOnly overideDisplay' i18n_key="CancelRes_lbl_OverrideBasis">Override Basis</label>
																		</td>
																		<td align='left' valign="top">
																		<select id="selDefineCanType" name="selDefineCanType" size="1" class = "aa-input aa-requiredField confOverideOnly overideDisplay" style="width: 150px;" >
																			<option value="" i18n_key="CancelRes_select"> -- select -- </option>
																			<option value="V" i18n_key="CancelRes_Value">Value</option>
																			<option value="PF" i18n_key="CancelRes_PercentageOfFare">Percentage of Fare</option>
																			<option value="PFS" i18n_key="CancelRes_PercentageOfFareSurcharge">Percentage of Fare & Sur.Chg</option>
																		</select>																		
																		<font class="mandatory confOverideOnly overideDisplay">*</font>																		
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<label class = 'txtBold' >&nbsp; </label>
																		</td>
																		<td align='left' valign="top" width="25%">
																			<label id = 'lblRouteType' name="lblRouteType" class = 'txtBold confOverideOnly' i18n_key="CancelRes_lbl_ApplicableBasis" >Applicable Basis</label>
																		</td>
																		<td align='left' valign="top">
																		<select id="selDefineRouteType" name="selDefineRouteType" size="1" class = "aa-input aa-requiredField confOverideOnly overideDisplay" style="width: 150px;" >
																			<option value=""  i18n_key="CancelRes_select"> -- select -- </option>
																			<option value="POND" i18n_key="CancelRes_PerOriginAndDestination">Per OND</option>
																			<option value="TOT" i18n_key="CancelRes_Total">Total</option>																			
																		</select>																
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align='center' valign="top">
																<table class="scroll" cellpadding="1" cellspacing="0" width='90%'>
																	<tbody id="tablPane2"></tbody>
																</table>
															</td>
														</tr>
														<tr>
															<td class='doubleGap' valign="top"></td>
														</tr>
														<tr>
															<td align='right' valign="top">
																<button id="btnOverride" type="button" class="btnMargin" i18n_key="btn_Overide">Override</button><button id="btnApply" type="button" class="btnMargin" i18n_key="btn_Apply">Apply</button>
															</td>
														</tr>
														<tr>
															<td class='doubleGap' valign="top"></td>
														</tr>
														<tr>
															<td id="flexiInfo" align='left' valign="bottom" style='height:120px; display: none;' class='txtBold' i18n_key="CancelRes_RemainingFlexibilities">Remaining Flexibilities: <span id="spnFlexibilities" style="color: red;" i18n_key="CancelRes_NoMoreFlexiAvailable">No more flexibilities available for the cancelled segment</span></span>
															</td>
														</tr>												
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width='50%'  class="ui-widget ui-widget-content ui-corner-all" style='height:280px;' valign='top'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='paneHD'>
													<div id='divHDPane3' class='txtWhite txtBold'></div>
												</td>
											</tr>
											<tr>
												<td>
													<table id="tablPane3" class="scroll" cellpadding="0" cellspacing="0"></table>
												</td>
											</tr>
											<tr id="trUserNote">
												<td><label i18n_key="CancelRes_UserNotes">User Notes : </label>
												  <input id="txtUserNote" type="text" size="124s"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align='right'>
										<button id="btnConfirm" type="button" class="btnMargin" i18n_key="btn_Confirm">Confirm</button><button id="btnReset" type="button" class="btnMargin" i18n_key="btn_Reset">Reset</button><button id="btnClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='../reservation/inc_pageError.jsp' %>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<input  type="hidden" id="hdnRouteType" value="" />
	<%@ include file='../common/inc_LoadMsg.jsp'%>
<br /></body>
</html>
<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguageForOpener();
</script>
