<%-- <script
	src="../js/common/jquery_ui1.8.23.selectable.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script> --%>
<%@ include file='../common/inc_PgHD.jsp'%>
<script type="text/javascript"
	src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<style>
.highlight {
    background-color:#e9f7ff!important;
    color:#000828!important;
    border-bottom:10px solid #000828!important;
}
</style>
<table width='675' border='0' cellpadding='0' cellspacing='0'>
	<tr>
	<td class="singleGap"></td>
	<form method='post' action="" id="frmLoadNoShowTaxData">
	</form>
	</tr>
	<tr>
		<td colspan="2"><span style="float: left; width: 80%"><label><b>Select Passengers to reverse tax</b></label></span>
						<span style="float: right; width: 20%"><b>PNR: </b><label id="pnrTaxRefund"></label></span>
						<br>
				
			<span style="float: left; width: 80%">Select pax to refund tax <button  class="checkRfnd" type="button" style="width: auto;" >Select/Deselect All</button></span></td> 
	</tr>
	<tr>
	<td class="singleGap"></td>
	</tr>
	<tr>
		<td colspan="2"><br />
			<table width="100%" border="0">
				<tr>
				<tbody>
					<tr>
						<td style="width: 5%;"></td>
						<td style="width: 60%;"><b>Passenger</b></td>
						<td style="width: 30%;" align="center"><b>Select</b></td>
						<td style="width: 5%;"></td>
					</tr>
				</tbody>

			</table></td>
	</tr>
	<tr>
		<td colspan="2">
			<div style="height: 300px; overflow-y: auto; overflow-x: hidden;">
				<ul style="float: left; width: 100%" id="listReverseNoShowTax" class="scroll">
				</ul>
			</div>
		</td>

	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>
	<tr id="trRefundTxt">
		<td class="txtBold">Total reversible tax amount : <lable id="totalRefundableTax"></lable></td>
	</tr>
	<tr id="trRefundTxt">
			<td class="txtMedium txtBold">Selected amount to reverse : <lable id="totalAmountToReverse"></lable></td>
	</tr>
	<tr id="groupPayOptButtons">
		<td align='right'>
			<button id="btnReverseNoShowTaxConfirm" type="button" class="btnMargin" >Confirm</button>
			<!-- <button id="btnReverseNoShowTaxReset" type="button" class="btnMargin" >Reset</button> -->
			<button id="btnReverseNoShowTaxClose" type="button" class="btnMargin" >Close</button>
		</td>
	</tr>
	<tr>
		<td class='singleGap'></td>
	</tr>
</table>
<script type="text/javascript"
	src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
