<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>	
<%@ include file='../common/inc_ShortCuts.jsp' %>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/pageShortCuts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript" src="../js/v2/reservation/tabSearchFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
	<script type="text/javascript" src="../js/v2/reservation/tabAnci.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript">
	<!--
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		DATA_ResPro["transferSegment"] = eval('(' + '<c:out value="${requestScope.transferSegment }" escapeXml="false" />' + ')');
		DATA_ResPro["resSegments"] = eval('(' + '<c:out value="${requestScope.resSegments }" escapeXml="false" />' + ')');
		DATA_ResPro["systemParams"] = eval('(' + '<c:out value="${requestScope.systemParams }" escapeXml="false" />' + ')');
		DATA_ResPro["resAlerts"] = eval('(' + '<c:out value="${requestScope.resAlerts }" escapeXml="false" />' + ')');
		
		var excludableCharges = eval('(' + '<c:out value="${requestScope.excludableCharges }" escapeXml="false" />' + ')');
		var carrierCode = '<c:out value="${requestScope.carrierCode}" escapeXml="false" />';
		var isMinimumFare = false;
	//-->
	</script>
</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:none;">
		<div style="height:5px;"></div>
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
			<ul>
				<li><a href="#divSearchTab" style="width:100px" i18n_key="BookingInfo_FlightDetails_lbl_SearchFlights">Search Flights</a></li>				
			</ul>
			<div id="divSearchTab" style="padding-top: 5px">
				<%@ include file='../reservation/inc_TabSearchFlights.jsp' %>
			</div>
			
			<form method='post' action="" id="frmMakeBkg">			
				
			</form>
		</div>	
	</div>
	<form method='post' action="" id="frmTabLoad"></form>
	<%@ include file='../common/inc_Credit.jsp' %>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
	<%@ include file='../reservation/inc_FlightStopOverDetails.jsp' %>
	<script type="text/javascript" src="../js/v2/modify/transferSegment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</body>
</html>
