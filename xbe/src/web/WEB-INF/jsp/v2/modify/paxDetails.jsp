<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Extra Passenger Details</title>
	<%@ include file='../common/inc_PgHD.jsp' %>
	<script type="text/javascript">
	
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');
		DATA_ResPro["mode"] = "<c:out value="${requestScope.mode }" escapeXml="false" />";
		var isDomesticFlightExist = "<c:out value="${requestScope.isDomesticFlightExist }" escapeXml="false" />";
		var isGdsPnr = "<c:out value="${requestScope.isGdsPnr }" escapeXml="false" />";

		<c:out value='${requestScope.reqDuplicatePassportNumberCheckOnlyWithinAdult}' escapeXml='false' />
	</script>
	
	<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/modify/paxDetails.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
</head>
<body class="legacyBody" style="width:620px;height:260px;overflow-x:hidden;">
	<div id="divLegacyRootWrapperPopUp" style="display:none;width:620px;height:260px;">
		<table style='width:1300px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:600px;height:240px;overflow-x:auto;">
						<form method='post' action="" id="frmPaxDetails">
							<table width='97%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
										<label class='txtLarge txtBold' i18n_key="">Extra Passenger Details</label>
									</td>
								</tr>
								<tr>
									<td class='singleGap'></td>
								</tr>								
								<tr>
									<td width='50%'  class="ui-widget ui-widget-content ui-corner-all" style='height:180px;' valign='top'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='paneHD'>
													<div id='divHDPane1' class='txtWhite txtBold'></div>
												</td>
											</tr>
											<tr>
												<td>
													<table id="tablPane1" class="scroll" cellpadding="0" cellspacing="0">
														<tbody id="paxInfoTemplete">
														</tbody>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align='right'>
										<button id="btnEdit" type="button" class="btnMargin" i18n_key="btn_Edit">Edit</button>
										<button id="btnConfirm" type="button" class="btnMargin" i18n_key="btn_Confirm">Confirm</button>
										<button id="btnCancel" type="button" class="btnMargin" i18n_key="btn_Cancel">Cancel</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</td>
			</tr>			
		</table>
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
<br /></body>
</html>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript">
$("#divPane").getLanguage();
</script>