<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
	<head>
		<title>Group Request</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
			   
		<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}'escapeXml='false'  />" type="text/javascript"></script>
		<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script src="../js/common/main.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script language="javascript" src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>			   
			
	 
		<script type="text/javascript" src="../js/graph/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../js/v2/moment/moment.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
 		<script type="text/javascript" src="../js/v2/common/jquery.clock.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
 
		<style>
			.innerPanel{
				width:98%;
				margin:0 auto;
			}
			.portSel{
				width:50px
			}
			select{
				margin:1px 1px;
			}
			.iconBtn{
				width:25px
			}
			#ui-datepicker-div{
				display:none;
			}
			.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
		</style>
		
	</head>
   
  
    <body class="tabBGColor" scroll="no"  oncontextmenu="return false" ondrag='return false'>
		<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
		<table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" valign="top">
  
					<input type="hidden" name="hdnGroupBookingReq" id="hdnGroupBookingReq" value=0>
			    	<input type="hidden" name="requestID" id="requestID" value="-1">
			  
			  		<span id="spanAgentDetails" style="background-color: #fff;display: block; text-align: right;" >
						<c:out value="${requestScope.returnData}" escapeXml="false" />
					</span>
			    	<div id="divGrpBSearch" style="background-color: #fff;" >
						<div class="innerPanel divsearch" >
							<table width="99%" border="0" cellpadding="0" cellspacing="2" >
								<tr>
									<td>
										<font>Request ID</font>
									</td>
									<td>
								 		<input type="text" name="requestID" id="txtReqId" value="" style="width: 62px;" maxlength="10" >
									</td>
									<td>
										<font>From</font>
										<select name="selFrom" id="selFrom" style="width: 50px;">
											<option value="">All</option>
										</select>
									</td>
									<td>
										<font>To</font>
										<select name="selTo"  id="selTo" style="width: 50px;">
											<option value="">All</option>
										</select>
									</td>
									<td>
										<font>&nbsp;&nbsp;Status</font>
									</td>
									<td>
										<select name="status"  id="status" style="width: 80%;">
										</select>
									</td>
									<td>
										<font>Main Request Only </font>
										<input type="checkbox" id="mainReqestOnly">
									</td>
									<td>
										<font>Shared </font>
										<input type="checkbox" id="shared">
									</td>
									
									<td>
										<font>&nbsp;&nbsp;</font>
										<input name="btnSearch" type="button" id="btnSearch" value="search" class="Button"/>
									</td>
								</tr>	
										
								<tr>
									<td colspan="9">		
										<div class="divResults" >
											<div id="jqGridGrpBContainer" style="margin-top: 5px; hight:100% " >
												<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpBData">
												</table>
												<div id="jqGridGrpBPages">
												</div>
											</div>
										</div>
									</td>
								</tr>
											
								<tr>
									<td colspan="7">				
										<input name="btnAdd" type="button" id="btnAdd" value="Add" class="Button"/>
										<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="Button"/>				
									</td>
								</tr>
				   			</table>		
						</div>
					</div>
			 
					<form method="post" action="manageGroupBookingRequest!save.action" id="frmGrpB">
						<div id="grpBookingDetails" style="background-color: #fff;">
					
							<div id="groupBookingTabs">
								<ul>
									<li><a href="#groupBookingMain">Single Request</a></li>
									<li><a href="#groupBookingSub">Sub Request</a></li>
								</ul>
								<div id="groupBookingMain" class='tab'>
								<table width="100%" border="0" cellpadding="0" cellspacing="2" >
								<tr><td colspan="6">
			      					<table width="700" border="0" cellpadding="0" cellspacing="2" >
			      						<tr>
			      							<td>
			      								<table>
			      									<tr>
			      										<td><font>Departure</font></td>
			      										<td><font>Departure Date</font></td>
			      									</tr>
			      									<tr>
			      										<td>
							   								<select name="selFrom_1" id="selFrom_1" style="width: 80px;">								
															</select>
														</td>
														<td>
															<input type="text" name="txtDeparture" id="txtDeparture" value="" style="width: 70px;" maxlength="10"  readonly="readonly">
														</td>
			      									</tr>
			      								</table>
			      							</td>
			      							<td>
			      								<table>
			      									<tr>
			      										<td><font>Arrival</font></td>
			      										<td><font>Via</font></td>
			      										<td>&nbsp;</td>
			      										<td><font>Return Date</font></td>
			      									</tr>
			      									<tr>
			      										<td>
							   								<select name="selTo_1" id="selTo_1" style="width: 80px;">								
															</select>
														</td>
														<td>
															<select name="selVia_1" id="selVia_1" style="width: 80px;">								
															</select>
														</td>
														<td>
															<input type="checkbox" id="returning" /><font>&nbsp;&nbsp;Return</font> 
														</td>
														<td>
															<input type="text" name="txtReturn" id="txtReturn" value="" style="width: 70px;" maxlength="10"  readonly="readonly">
														</td>
			      									</tr>
			      								</table>
			      							</td>
			      							<td>
			      								<table>
			      									<tr>
			      										<td><font>Flight(s)</font> </td>
			      										<td>&nbsp;</td>
			      									</tr>
			      									<tr>
			      										<td>
							   								<input type="text" id="flightNumber" style="width:70px" class="leftAlign"  />
														</td>
														
			      									</tr>
			      								</table>
			      							</td>
			      							<td >
			      								<table>
			      									<tr>
			      										<td>&nbsp;</td>
			      									</tr>
			      									<tr>
			      										<td>
							   								<input type="button" id="add_Routes" value=" + " class="iconBtn addItem" /> <font class="mandatory">&nbsp;*</font>
														</td>
			      									</tr>
			      								</table>
												
											</td>
			      						</tr>
																			    
										<tr>											
											<td colspan="3" valign="top">
												<div class="addEditViewDiv" >
													<div id="addEditViewContainer" style="margin-top: 5px; hight:100% " >
														<table width="100%" border="0" cellpadding="0" cellspacing="0" id="addEditViewData">
														</table>
														<div id="addEditViewPages">
														</div>
													</div>
												</div>			
											</td>
											<td valign="top">
												<input type="button" id="del_Routes" value=" - " class="iconBtn removeItem" />
											</td>
										</tr></table>
										</td>
										</tr>	
										
										<tr>
											<td>
												<font>Adult count</font>
											</td>
											<td>
												<input type="text" id="adultCount" style="width:90%" class="rightAlign"  />
											</td>
											<td>
												<font>Child count</font>
											</td>
											<td>
												<input type="text" id="childCount" style="width:90%" class="rightAlign" />
											</td>
											<td>
												<font>Infant count</font>
											</td>
											<td>
												<input type="text" id="infantCount" style="width:90%" class="rightAlign" />
											</td>
										</tr>
								   
										<tr>				  
											<td>
												<font>Fare declaration</font>
											</td>
											<td>
												<font>OAL fare</font><font class="mandatory">&nbsp;*</font>
											</td>
											<td>
												<input type="text" id="oalFare" style="width:90%" class="leftAlign"/>
											</td>
											<td>
												<font>Requested fare</font>
											</td>
											<td>
												<input type="text" id="requestedFare" style="width:60%" class="rightAlign"/>  <span id="spnCurrency" style="font-weight: bold;"/>
											</td>
								   		</tr>
								   			
										<tr>
											<td >
												<font>Agent remarks</font><font class="mandatory">&nbsp;</font>
											</td>
											<td colspan="4">
												<textarea name="agentRemarks" id="agentRemarks" cols="40" rows="3" class="leftAlign" maxlength="99">
												</textarea> 
											</td>											
											<td>
												<input type="radio" id="single" name="requestType" value="single"><font>&nbsp;&nbsp;Single Request</font><br>
												<input type="radio" id="series" name="requestType" value="series"><font>&nbsp;&nbsp;Series Request</font><br>
											</td>			      
										</tr>
										<tr>
											<td>
												<font>Requested Agent</font>
											</td>
											<td>
												<select name="travelAgent"  id="travelAgent" style="width: 100%;">
												</select>
											</td>
										</tr>
										<tr>
										    <td colspan="5">
										    	<span id="spnAppprove" visible="false"></span>
										    </td>
										<tr>
											
										<tr>
											<td>
												<input name="btnSave" type="button" id="btnSave" value="save" class="Button"/>								
												<input name="btnRequote" type="button" id="btnRequote" value="Re-Quote" class="Button" style="width:auto"/>									
										   		<input name="btnReleaseSeat" type="button" id="btnReleaseSeat" value="Release-Seat" class="Button" style="width:auto"/>						   			
										   		<input name="btnDelete" type="button" id="btnDelete" value="Withdraw" class="Button"/>
											</td>
							   			</tr>
							   
									</table>
			   
								</div>
								<div id="groupBookingSub" class='tab'>
			    					<%@ include file="GroupBookingRollForward.jsp" %>
			    				</div>
			    			</div>
						</div>
				    	<div>
				    		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
								<tr>
									<td colspan="3" style="height: 42px;" valign="bottom"><input name="btnClose" type="button" id="btnClose" value="Close" onclick="top.LoadHome()" class="Button"/>
									</td>
								</tr>
							</table>
						</div>
					</form>   
			    
					<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
						<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
					</div>
				</td>
			</tr>
		</table>	   	
  
    		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
	    <script type="text/javascript" src="../js/GroupBooking/groupBooking.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
		<script type="text/javascript" src="../js/GroupBooking/grpBookingRollForward.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
	 	<script type="text/javascript">
			var screenId = 'SC_RESV_CC_029';
	
			  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
			  	function ClearProgressbar(){  		
			  		if (typeof(objDG) == "object"){
				  		if (objDG.loaded){
				  			clearTimeout(objProgressCheck);
				  			HideProgress();
				  		}	  	
				  	}
			  	}
		</script>
			    
	</body>
</html>