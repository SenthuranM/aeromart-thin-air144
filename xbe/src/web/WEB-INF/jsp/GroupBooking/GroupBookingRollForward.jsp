<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- Page Background Top page -->
<table width="100%">
	<tr>
		<td style="padding: 5px"><input type="hidden"
			name="hdnGroupBookingReq" id="hdnGroupBookingReq" value=0>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>
						<div id="mainRequestDetailContainer">
							<table id="mainRequestDetails"></table>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="checkbox" id="chkOutAll" name="appDay" onClick="selectAll(true)"/> All
						<input type="checkbox" id="chkOutSun"  name="appDay" value="0" /> Sunday 
						<input type="checkbox" id="chkOutMon" name="appDay" value="1" /> Monday 
						<input type="checkbox" id="chkOutTues" name="appDay" value="2" /> Tuesday 
						<input type="checkbox" id="chkOutWed" name="appDay" value="3" /> Wednesday 
						<input type="checkbox" id="chkOutThu" name="appDay" value="4" /> Thursday 
						<input type="checkbox" id="chkOutFri" name="appDay" value="5" /> Friday 
						<input type="checkbox" id="chkOutSat" name="appDay" value="6" /> Saturday 
						
						
						<font>From</font>
						<font class="mandatory">&nbsp;*</font> 
						<input type="text" name="txtFrom" id="txtFrom" value="" style="width: 62px;" maxlength="10" readonly="readonly"> 
						
						<font>To</font> 
						<font class="mandatory">&nbsp;*</font> 
						<input type="text" name="txtTo" id="txtTo" value="" style="width: 62px;" maxlength="10" readonly="readonly"> 
						
						<input name="btnRollForward" type="button" id="btnRollForward" value="Roll Forward" class="Button" style="width: auto">
					</td>
				</tr>
				<tr>
					<td>
						<div id="subRequestGridContainer">
							<table id="subRequestDetails"></table>
						</div>
					</td>
				</tr>
				<tr>
					<td align="right"><input name="btnSaveRollForward"
						type="button" id="btnSaveRollForward" value="Save & Roll Forward"
						class="Button" style="width: auto">
					</td>
					
				</tr>
				
			</table></td>
	</tr>
</table>
