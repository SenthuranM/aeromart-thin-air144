 <%-- 
	 @Author 	: 	Shakir
 --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Tabs.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/payments/validateInvoiceSettle.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
 <body >
 <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return showContextMenu()" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" 
		 onLoad="invoiceLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
	<form name="frmInvoicesettlement" id="frmInvoicesettlement" action="showInvoicesettlement.action" method="post">
	<script type="text/javascript">
			var arrError = new Array();
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			var isSaveTransactionSuccessful = false;
			var isSuccessfulSaveMessageDisplayEnabled = false;
			
			<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
			<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	
			
			var arrInvoice = new Array();
			<c:out value="${requestScope.reqInvoiceArray}" escapeXml="false" />	
			var parnagentCode ="<c:out value="${requestScope.reqAgentCode}" escapeXml="false" />";
			var parnagentName ="<c:out value="${requestScope.reqAgentName}" escapeXml="false" />";
	</script>

	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td><%@ include file="../common/IncludeFormTop.jsp"%><font class="fntBold">&nbsp;Invoice Settlement for Agent : <span id="spnAgentId"></span></font><%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">	
					<tr>
						<td colspan="2"><span id="spnInvoiceDetails"></span></td>
					</tr>
					<tr><td colspan="2"><font>&nbsp;</font></td></tr>		
					<tr>
						<td width="50%">
							<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick();">
							<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()">
						</td>
						<td width="50%" align="right">
							<u:hasPrivilege privilegeId="ta.pay.settle.invoice">
								<input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="saveClick()">				
							</u:hasPrivilege>
						</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
	</table>
	<script src="../js/payments/InvoiceDetailGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<input type="hidden" name="hdnMode" id="hdnMode" value="">
	<input type="hidden" name="hdnData" id="hdnData" value="">
	<input type="hidden" name="hdnAgentCode" id="hdnAgentCode" value="">
	<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
	<input type="hidden" name="hdnAgentCurr" id="hdnAgentCurr" value="">	
	</form>
</body>
</html>
	