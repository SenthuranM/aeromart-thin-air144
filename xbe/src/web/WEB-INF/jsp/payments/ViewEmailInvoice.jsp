 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Invoice View</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>			
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
		var arrError = new Array();
		var blnBaseCurr = false;
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		var arrInvData = new Array();
		var strData = new Array();			
		<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
		//GridArray
		<c:out value="${requestScope.reqInvoiceSummData}" escapeXml="false" />
		var paymentTerms = "<c:out value="${requestScope.reqPaymentTerms}" escapeXml="false" />";
		var accountNumber = "<c:out value="${requestScope.reqAccountNo}" escapeXml="false" />";
		var accountName = "<c:out value="${requestScope.reqAccountName}" escapeXml="false" />";
		var bankName = "<c:out value="${requestScope.reqBankName}" escapeXml="false" />";
		var bankAddress = "<c:out value="${requestScope.reqBankAddress}" escapeXml="false" />";
		var bankIBANCode = "<c:out value="${requestScope.reqBankIbanCode}" escapeXml="false" />";
		var bankBeneficiaryName = "<c:out value="${requestScope.reqBankBeneficiaryName}" escapeXml="false" />";
		var baseCurrency = "<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
		var swiftCode = "<c:out value="${requestScope.reqSwiftCode}" escapeXml="false" />";		
		var currentMonth = "<c:out value="${requestScope.reqCurrentMonth}" escapeXml="false" />";
		var currentYear = "<c:out value="${requestScope.reqCurrentYear}" escapeXml="false" />";
		var IsTransEnable = "<c:out value="${requestScope.IsTransEnable}" escapeXml="false" />";
	</script>
  </head>
	<body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" 
	scroll="yes" style="background: transparent;">
		<%@ include file="../common/IncludeTabTop.jsp"%><!-- Page Background Top page -->
  		<form  name ="viewInvoice" id="viewInvoice" action="saveViewInvoice.action" method="post">
		 
			<table width="99%" align="center" border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>	
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Select Agent<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblHead">
							<tr><td colspan="5"><font class="fntSmall">&nbsp;</font></td></tr>
						  	<tr>
						  		
						  		<td width="40%"><font>Agent/GSA</font><font class="mandatory">&nbsp;*</font>
									<select name="selAgent" id="selAgent" size="1" style="width:250px" onChange="objOnFocus()">
										<OPTION value=""></OPTION>
										<c:out value="${requestScope.reqAgentList}" escapeXml="false" />
									</select>
						  		</td>
						  		<td><div id="invSearchTbl">
						  		<table width="100%" border="0" cellpadding="0" cellspacing="2">
						  		<tr>
								<td width="18%">
									<font>Year</font>
										<select name="selYear" id="selYear" size="1" style="width:50px" onChange="objOnFocus()">
											<c:out value="${requestScope.reqBasePrevious}" escapeXml="false" />
									</select>
								  </td>
								  <td width="25%">
									<font>Month</font>
									<select name="selMonth" id="selMonth" size="1" style="width:75px" onChange="objOnFocus()">
										<OPTION value="1">January</OPTION>
										<OPTION value="2">February</OPTION>
										<OPTION value="3">March</OPTION>
										<OPTION value="4">April</OPTION>
										<OPTION value="5">May</OPTION>
										<OPTION value="6">June</OPTION>
										<OPTION value="7">July</OPTION>
										<OPTION value="8">August</OPTION>
										<OPTION value="9">September</OPTION>
										<OPTION value="10">October</OPTION>
										<OPTION value="11">November</OPTION>
										<OPTION value="12">December</OPTION>
									</select>
								  </td>
								  <td width="28%">
									<font>Billing Period</font>
									<select name="selBP" id="selBP" size="1" style="width:40px" onChange="objOnFocus()">
										<c:out value="${requestScope.reqInvPeriods}" escapeXml="false" />
									</select>
								  </td>
								  <td width="35%">
									<font>Entity &nbsp;</font>
									<select name="selEntity" id="selEntity" size="1" style="width:90px" onChange="objOnFocus()">
										<c:out value="${requestScope.reqEntities}" escapeXml="false" />
									</select>
								  </td>
								  <td>
								  	<u:hasPrivilege privilegeId="ta.invoice.base">
										<font>Include Base Currency</font>
										<input type="checkbox" name="chkBase" id="chkBase" value="on" class="noborder">
									</u:hasPrivilege>
									</td>							  
								  </td>
								  <td>	<u:hasPrivilege privilegeId="rpt.rev.new.view">						  	
										<font>New View</font>
										<input type="checkbox" name="chkNewview" id="chkNewview" value="on" class="noborder">
										</u:hasPrivilege>									
									</td>
									</tr>
								  </table>
								  </div>
								 </td>
						  	</tr>
							  <tr><td colspan="5"><font class="fntSmall">&nbsp;</font></td></tr>
							  <tr>
								<td align="right" colspan="5">
									<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search" onClick="searchClick()">
								</td>
							  </tr>
						  </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
				  		
					</td>
				</tr>
				<tr>
					<td>
						<span id="spnTransactionGrid" style="visibility:block;">
						<%@ include file="../common/IncludeFormTop.jsp"%>Invoice Summary<%@ include file="../common/IncludeFormHD.jsp"%>
							<table width="100%" border="0" cellpadding="0" cellspacing="2" id="tblInvSumm">
								<tr>
								<td><font class="fntSmall">&nbsp;</font></td>
								</tr>
								<tr>
								<td style="height:120;" valign="top" align="center">
										<span id="spnInvoiceTrans"></span>
									</td>
								</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Invoice Details<%@ include file="../common/IncludeFormHD.jsp"%>
							 <table width="100%" border="0" cellpadding="0" cellspacing="2" id="tblInvDet">
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
								<tr>
									<td style="height:350;" valign="top" align="center">
										<span id="spnInvoiceDetails"></span>
									</td>
								</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="47%">
										<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">										
									</td>
									<td width="53%">
										<!-- input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" -->
										<input name="btnDetails" type="button" class="Button" id="btnDetails" value="Details" onClick="detailsClick()">
										<input name="btnEmail" type="button" class="Button" id="btnEmail" value="Email" onClick="sendEmail()" title="Email to Agent ID" >
										<input name="txtEmail" type="text" id="txtEmail">	
										<input name="btnSend" type="button" class="Button" id="btnSend" value="Send" onClick="sendEmailToGivenEmail()" title="Enter Email ID" >

									</td>
								</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode" value="">
		<input type="hidden" name="hdnUserId" id="hdnUserId" value="">
		<input type="hidden" name="hdnAgentId" id="hdnAgentId" value="">
		<input type="hidden" name="hdnInvNo" id="hdnInvNo" value="">
		<input type="hidden" name="hdnSearchCriteria" id="hdnSearchCriteria" value="">

		<input type="hidden" name="hdnRpt" id="hdnRpt" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnInvoice" id="hdnInvoice" value="">
		<input type="hidden" name="selYear" id="selYear" value="">
		<input type="hidden" name="selMonth" id="selMonth" value="">
		<input type="hidden" name="selBP" id="selBP" value="">
		<input type="hidden" name="radReportOption" id="radReportOption" value="">
		<input type="hidden" name="invoiceSummaryHtml" id="invoiceSummaryHtml" value="">
		<input type="hidden" name="agentEmail" id="agentEmail" value="">
		<input type="hidden" name="paymentTerms" id="paymentTerms" value="">
		<input type="hidden" name="accountNumber" id="accountNumber" value="">
		<input type="hidden" name="accountName" id="accountName" value="">
		<input type="hidden" name="bankName" id="bankName" value="">
		<input type="hidden" name="bankAddress" id="bankAddress" value="">
		<input type="hidden" name="swiftCode" id="swiftCode" value="">
		<input type="hidden" name="bankIBANCode" id="bankIBANCode" value="">
		<input type="hidden" name="bankBeneficiaryName" id="bankBeneficiaryName" value="">
		<input type="hidden" name="hdnFrompage" id="hdnFrompage" value="AccView">
		<input type="hidden" name="hdnAgentCurr" id="hdnAgentCurr" value="">
		<input type="hidden" name="hdnInvoiceNo" id="hdnInvoiceNo" value="">
		<input type="hidden" name="hdnPNR" id="hdnPNR" value="">
		<input type="hidden" name="selEntity" id="selEntity" value="">			
		<input type="hidden" name="hdnEntityText" id="hdnEntityText" value="">
		</form>
		<%@ include file="../common/IncludeTabBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
  
	<script src="../js/payments/validateViewInvoice.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/payments/InvoiceTransactionGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
	  	<!--
	  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
	  	function ClearProgressbar(){
			clearTimeout(objProgressCheck);
			HideProgress();
	  	}
  	 
  	//-->
	</script>
	<%@ include file="../common/IncludeLoadMsg.jsp"%>
</html>