<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Agent Account</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<%@ include file='../v2/common/inc_PgHD.jsp' %>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    
    <script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return true" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" 
		onLoad="CHOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
   <form method="post" id="frmAdjPay" name="frmAdjPay">

  <script type="text/javascript">
  		var arrError = new Array();
		<c:out value="${requestScope.reqAdjustedPayment}" escapeXml="false" />
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />	
		<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
		<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	
		<c:out value="${requestScope.reqFormData}" escapeXml="false" />
		<c:out value="${requestScope.searchGSA}" escapeXml="false" />
		<c:out value="${requestScope.gNameNCode}" escapeXml="false" />
		<c:out value="${requestScope.maintainAgentBspCredLmt}" escapeXml="false" />
		<c:out value="${requestScope.hasPriviForViewPayOnly}" escapeXml="false" />
		
		var enableTACreditReverse = "<c:out value="${requestScope.enableTACreditReverse}" escapeXml="false" />";
		var baseCurrency = "<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
		var popupmessage = "<c:out value="${requestScope.popupmessage}" escapeXml="false" />";
		var enableAdjustmentCarrier = <c:out value="${requestScope.enableAdjustmentCarrier}" escapeXml="false" />;
		var isPreventDuplicateRecord = "<c:out value="${requestScope.reqIsPreventDuplicateRecord}" escapeXml="false" />"; 
		var isExactMatchExists = "<c:out value="${requestScope.isExactMatchExists}" escapeXml="false" />";
		var isDuplicateAgentAmountCombinationExists = "<c:out value="${requestScope.isDuplicateAgentAmountCombinationExists}" escapeXml="false" />";
  </script>

   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">		
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Agent Account<%@ include file="../common/IncludeFormHD.jsp"%>
		  			<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr><td colspan="12">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
  							<tr>		  						
  								<td colspan="2"><font class="fntBold"><span id="spnAgentName"></span></font></td>
  								<td><font>Avail Credit</font></td>
  								<td><font class="fntBold"><span id="spnCreditLimit"></span></font></td>
								<td><font>Due</font></td>
  								<td><font class="fntBold"><span id="spnAmountDue"></span></font></td>
								<td><font>INV Pool</font></td>
  								<td colspan="2"><font class="fntBold"><span id="spnAvlInvSettle"></span></font></td>
  								<td><font>BSP Credit</font></td>
  								<td><font class="fntBold"><span id="spnBSPCreditLimit"></span></font></td>
  							</tr>
                        </table>
                     </td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
  						<tr>
  							<td width="7%">
								<font>From Date</font>
							</td>
							<td width="10%">
								<input type="text" name="txtDStart" id="txtDStart" maxlength="10" size="10" align="right" onBlur="dateChk('txtDStart')" invalidText="true">
							
							</td>
							<td width="5%">
								<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img SRC="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font>
							</td>
							<td width="7%">
								<font>To Date</font>
							</td>
							<td width="10%">
								<input type="text" name="txtDStop" id="txtDStop" maxlength="10" size="10" align="right" onBlur="dateChk('txtDStop')" invalidText="true">
							
							</td>
							<td width="5%">
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img SRC="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font>
							</td>
							<td width="50%">
								&nbsp;&nbsp;
  								<input name="chkTypeR" id="chkTypeR" maxlength="1" type="checkbox" value="R" title="Reservation" class="noBorder">
  								<font>R</font>
  								<u:hasPrivilegeWithFlag privilegeId="ta.pay.settle.payment.any">
  									<input name="chkTypeC" id="chkTypeC" maxlength="1" type="checkbox" value="C" title="Credit" class="noBorder">
	  								<font>C</font>
	  								<input name="chkTypeD" id="chkTypeD" maxlength="1" type="checkbox" value="D" title="Debit" class="noBorder">
	  								<font>D</font>
	  								<input name="chkTypeRR" id="chkTypeRR" maxlength="1" type="checkbox" value="RR" title="Reservation Refund" class="noBorder">
	  								<font>RR</font>
  								</u:hasPrivilegeWithFlag>
  								<input name="chkTypeA" id="chkTypeA" maxlength="1" type="checkbox" value="A" title="Cash" class="noBorder">
  								<font>CH</font>
  								<u:hasPrivilegeWithFlag privilegeId="ta.pay.settle.payment.any">
	  								<input name="chkTypeQ" id="chkTypeQ" maxlength="1" type="checkbox" value="Q" title="Cheque" class="noBorder">
	  								<font>CQ</font>
	  								<input name="chkTypeRV" id="chkTypeRV" maxlength="1" type="checkbox" value="RV" title="Adjustment" class="noBorder">
	  								<font>A</font>
	  								<input name="chkTypeBSP" id="chkTypeBSP" maxlength="1" type="checkbox" value="BSP" title="BSP Settlement" class="noBorder">
	  								<font>BSP</font>
  								</u:hasPrivilegeWithFlag>
  								<input name="chkTypeCG" id="chkTypeCG" maxlength="1" type="checkbox" value="CG" title="Commision Granted" class="noBorder">
  								<font>CG</font>
  								<input name="chkTypeCR" id="chkTypeCR" maxlength="1" type="checkbox" value="CR" title="Commision Removed" class="noBorder">
  								<font>CR</font>
  								<input name="chkTypeAO" id="chkTypeAO" maxlength="1" type="checkbox" value="OA" title="Agent Operations" class="noBorder">
  								<font>AO</font>
								<input name="chkTypeCCD" id="chkTypeCCD" maxlength="1" type="checkbox" value="CCD" title="Credit Card" class="noBorder">
  								<font>Credit Card</font>
  								<font class="mandatory">&nbsp;*</font>
  							</td>
  							<td width="7%">
  								<div id="adjustmentCarrierSearchDiv">
									<select multiple name="searchCarrier" id="searchCarrier" size="2" style="height : 32px;">
										<OPTION value="ALL">All</OPTION>
										<c:out value="${requestScope.reqPaymentCarrierList}" escapeXml="false" />
									</select>
  								</div>
  							</td>
							<td align="right">
									<input name="btnSearch"  type="button" class="Button" id="btnSearch" value="Search" onClick="searchClick()">
							</td>			
  						
						</tr>
						
  						<tr>
  							<td width="90%" colspan="8">
  								<input name="chkTypeCR" id="chkTypeCR" maxlength="1" type="checkbox" value="CR" title="Commision Removed" class="noBorder">
  								<font>Commision Removed</font>
  								&nbsp;    								
  								<u:hasPrivilegeWithFlag privilegeId="ta.pay.settle.payment.any">
  									<input name="chkTypeC" id="chkTypeC" maxlength="1" type="checkbox" value="C" title="Credit" class="noBorder">
	  								<font>Credit</font>
	  								&nbsp;
	  								<input name="chkTypeD" id="chkTypeD" maxlength="1" type="checkbox" value="D" title="Debit" class="noBorder">
	  								<font>Debit</font>
	  								&nbsp;
	  								<input name="chkTypeRR" id="chkTypeRR" maxlength="1" type="checkbox" value="RR" title="Reservation Refund" class="noBorder">
	  								<font>Reservation Refund</font>
	  								&nbsp;
  								</u:hasPrivilegeWithFlag>  								
  								<u:hasPrivilegeWithFlag privilegeId="ta.pay.settle.payment.any">
	  								<input name="chkTypeQ" id="chkTypeQ" maxlength="1" type="checkbox" value="Q" title="Cheque" class="noBorder">
	  								<font>Cheque</font>
	  								&nbsp;
	  								<input name="chkTypeRV" id="chkTypeRV" maxlength="1" type="checkbox" value="RV" title="Adjustment" class="noBorder">
	  								<font>Adjustment</font>
	  								&nbsp;
	  								<input name="chkTypeBSP" id="chkTypeBSP" maxlength="1" type="checkbox" value="BSP" title="BSP Settlement" class="noBorder">
	  								<font>BSP Settlement</font>
	  								&nbsp;
  								</u:hasPrivilegeWithFlag>  								
  								<font class="mandatory">&nbsp;*</font>
  							</td>
							<td>
									&nbsp;
							</td>			
  						
						</tr>
		  		</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Adjustments<%@ include file="../common/IncludeFormHD.jsp"%>
		  			<table width="100%" border="0" cellpadding="0" cellspacing="0">									
  						<tr>
		  					<td colspan="2">
				  				<span id="spnAdjustments"></span>		
		  					</td>
		  					<td>
		  						<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
									<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
								</div>
							</td>		  					
  						</tr>		  						  				
		  	</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr id="paymentOptionData">
			<td>
			<%@ include file="../common/IncludeFormTop.jsp"%>Make an Adjustment<%@ include file="../common/IncludeFormHD.jsp"%>				
					<table width="100%" border="0" cellpadding="0" cellspacing="0">	
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr>
							<td width="8%">
								<font>Type</font>
							</td>
							<td width="15%" >
								<select name="selType" id="selType" onChange="adjustmentTypeChange()">
									<OPTION value="SELECT">Select</OPTION>
									<c:out value="${requestScope.reqPaymentType}" escapeXml="false" />
								</select>
								<font class="mandatory">&nbsp;*</font>
							</td>
								
							<td width="8%">
								<span id="selCarrierlbl"><font>Carrier</font></span>
							</td>
							<td width="15%" >
								<span id = "selCarrierDiv">							
									<select name="selCarrier" id="selCarrier">
										<OPTION value="SELECT">Select</OPTION>
										<c:out value="${requestScope.reqPaymentCarrierList}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>
								</span>	
							</td>						
													
							<td width="8%">
								<font>Amount</font>
							</td>
							<td colspan="1">
								<input type="text" name="txtAmount" id="txtAmount" maxlength="16" onKeyUp="validateDecimal('txtAmount')" onChange="datachange()" onKeyPress="validateDecimal('txtAmount')" class="rightText">
								<fontEx><span id="spnCurrCode" name="spnCurrCode"></span></fontEx>&nbsp;<font class="mandatory">&nbsp;*</font>
							</td>
							<td><font>Amounts In </font></td>
								<td><font><span id="spnCurr" name="spnCurr"></span> Currency</font></td>
						</tr>
						<tr>
							<td valign="top" ><font>Remarks</font></td>
							<td colspan="3" valign="top">
								<textarea name="txtRemarks" id="txtRemarks" cols="40" rows="2" onkeyUp="validateTextArea(this)" onkeyUp="validateTA(this,255)" onChange="datachange()" onkeyPress="validateTA(this,255)"></textarea>
								<font class="mandatory">&nbsp;*</font>
							</td>
							<td>
								<font>Recipt No</font>
							</td>
							<td colspan="3">
								<input type="text" name="txtReciptNo" id="txtReciptNo" maxlength="20">
								<font class="mandatory">&nbsp;*</font>
							</td>
						</tr>						
						<tr>
							<td colspan="7">
								<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onClick="closeClick()">
								
							</td>
							<td align="right">
								<u:hasPrivilege privilegeId="ta.pay.settle.payment">
									<input name="btnConfirm"  type="button" class="Button" id="btnConfirm" value="Save" onClick="confirmClick(true)">
								</u:hasPrivilege>
							</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
	</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
  	<script src="../js/payments/AdjustmentPaymentGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/payments/validateAdjustmentPayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	  <script type="text/javascript">
  		<!--
  		var objProgressCheck = setInterval("ClearProgressbar()", 500);
  		function ClearProgressbar(){	
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			HideProgress();
	  		}  
  		}
  	 
  	//-->
  </script>
	<input type="hidden" name="hdnCode" id="hdnCode" value="">
	<input type="hidden" name="hdnMode" id="hdnMode" value="">
	<input type="hidden" name="hdnCMode" id="hdnCMode" value="">
	<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
	<input type="hidden" name="hdnBankGuarantee" id="hdnBankGuarantee" value="">
	<input type="hidden" name="hdnCMode" id="hdnCMode" value="">
	<input type="hidden" name="hdnCurrectCreditLimit" id="hdnCurrectCreditLimit" value="">
	<input type="hidden" name="hdnNLimit" id="hdnNLimit" value="">
	<input type="hidden" name="hdnCredit" id="hdnCredit" value="">	
	<input type="hidden" name="ForceCancel" id="ForceCancel" value="">
	<input type="hidden" name="hdnSelectedCarriers" id="hdnSelectedCarriers" value="">
	
	<input type="hidden" name="hdnAgentID" id="hdnAgentID" value="">
	<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
	<input type="hidden" name="hdnCreditLimit" id="hdnCreditLimit" value="">
	<input type="hidden" name="hdnFromPage" id="hdnFromPage" value="">	
	<input type="hidden" name="hdnIsAcceptRecord" id="hdnIsAcceptRecord" value="">	
	<input type="hidden" name="hdnIsFreshRecord" id="hdnIsFreshRecord" value="">
	<input type="hidden" name="hdnOriginTnxId" id="hdnOriginTnxId" value="">	
	<input type="hidden" id="selCurrIn" name="selCurrIn">
  </form>
  </body>
</html>



	