<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<%@ include file='../v2/common/inc_PgHD.jsp' %>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

    <script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return true" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" 
  onLoad="CHOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">  
	  <%@ include file="../common/IncludeTop.jsp"%>
		<table width="99%" cellpadding="2" cellspacing="0" border="0" align="center">
		<tr>
			<td><font class="Header">Agent Self Topup</font></td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>			
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Agent Self Topup<%@ include file="../common/IncludeFormHD.jsp"%>
					<br>
					<form method="post" action="agentSelfTopup.action" id="frmAgentSelfTopup" name="frmAgentSelfTopup">
						  <script type="text/javascript">
					  		var arrError = new Array();
							<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
							<c:out value="${requestScope.reqFormData}" escapeXml="false" />							
							var baseCurrency = "<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
							var popupmessage = "<c:out value="${requestScope.popupmessage}" escapeXml="false" />";
					  	</script>
						<table width="100%" border="0" cellpadding="1" cellspacing="2" align="left">
							<tr>
								<td><font class="Header">Current Available Balance : </font></td>
								<td>
									<font class="fntBold"><span id="spnBaseCurrAvailBal"></span></font>
  									<font class="fntBold"><span id="spnAgentCurrAvailBal"></span></font>
  								</td>				 
							</tr>
							<tr>
								<td width="23%">
									<font>Amount</font>
								</td>
								<td align="left" width="30%">
									<input type="text" name="txtAmount" id="txtAmount" maxlength="16" onKeyUp="validateDecimal('txtAmount')" onChange="datachange()" onKeyPress="validateDecimal('txtAmount')" class="rightText">
									<fontEx><span id="spnCurrCode" name="spnCurrCode"></span></fontEx>&nbsp;<font class="mandatory">&nbsp;*</font>
								</td>
								<td align="left" width="40%"><font>Amounts In </font>
                                    <font><span id="spnCurr" name="spnCurr">EGP</span> Currency</font>
    							</td>
							</tr>
							<tr>
 								<td valign="top">

								</td>
								 <td align=right valign="top">							
							</td>								
							</tr>
							<tr>
								<td>
									<font>Remarks</font>
								</td>
								<td align="left">
									<textarea name="txtRemarks" id="txtRemarks" cols="40" rows="2" onkeyUp="validateTextArea(this)" onkeyUp="validateTA(this,255)" onChange="datachange()" onkeyPress="validateTA(this,255)"></textarea>
								<font class="mandatory">&nbsp;*</font>
								</td>
								 
							</tr>
							<tr>
 							<td valign="top">

							</td>
							 <td align=right valign="top">							
							 </td>								
							 </tr>
							<tr>
								<td>
									<font>Payment Method</font>
								</td>
								<td align="left">
									<select id='selPaymentGateway' name='selPaymentGateway' tabindex="5">
										<OPTION value="SELECT">Select</OPTION>
										<c:out value="${requestScope.reqPGWType}" escapeXml="false" />
									</select>
									<font id="pgwMandatoryRef" class="mandatory" >&nbsp;*</font>
								&nbsp;	
								</td>
								 
							</tr>
							<tr id="trMinimumTopup">
								<td><font class="fntBoldAndRed"><span>Agent Self Topup Minimum Amount </span></font></td>
								<td><font class="fntBoldAndRed"><span id="spnMinimimTopup"></span></font></td>
							</tr>
							<tr>
								<td align="left">
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onClick="closeClick()">
								</td>
									
								<td colspan="2" align=right>
									<input type="button" id="btnTopup" name="btnTopup" value="Topup" class="Button" onclick="confirmClick(true)">									
								</td>
							</tr>
						</table>
						<input type="hidden" id="selCurrIn" name="selCurrIn">
						<input type="hidden" name="hdnMode" id="hdnMode" value="">
						<input type='hidden' id="contactInfo.firstName" name='contactInfo.firstName' value=""/>
						<input type='hidden' id="contactInfo.email" name='contactInfo.email' value=""/>
						<input type='hidden' id="contactInfo.phoneNo" name='contactInfo.phoneNo' value=""/>
						<input type='hidden' id="contactInfo.mobileNo" name='contactInfo.mobileNo' value=""/>						
						<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
							<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
						</div>
					</form>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
	</table>
	<script src="../js/payments/validateCCAgentTopup.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script language="javascript">
	<!--
	
		var request=new Array();
		var arrError=new Array();
		var reqMessage='<c:out value='${requestScope.reqMessage}' escapeXml='false' />';
		<c:out value='${requestScope.reqRequest}' escapeXml='false' /> 
		<c:out value='${requestScope.reqClientErrors}' escapeXml='false' /> 
	//-->
	</script>
	  <%@ include file="../common/IncludeBottomNoprogress.jsp"%>
</body>
</html>
