<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Manaage MCO</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>	
	<script type="text/javascript" src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
  	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

  
  </head>
  <body scroll="yes" class="tabBGColor" oncontextmenu="return false" ondrag='return false'>

  <%@ include file="../common/IncludeTabTop.jsp"%><!-- Page Background Top page -->
	<form method="post" name="frmSearchMCO" id="frmSearchMCO">
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>				
				<tr>
					<td>
						<div>
							<%@ include file="../common/IncludeFormTop.jsp"%>Manage MCO<%@ include file="../common/IncludeFormHD.jsp"%>
							<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table2">
								<tr>
									<td width="20%">
										<font>MCO Number</font>
									</td>
									<td width="20%">
										<input type="text" name="txtMCO" id="txtMCO" maxlength="24" size="20" value="" tabIndex="1">
										<font class="mandatory">&nbsp*</font>  
									</td>
	                                
									<td align="right"><input name="btnSearch" type="button" class="button" id="btnSearch" value="Search" style="width:100px" tabIndex="2">
									</td>
								</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
				  		</div>
					</td>
				</tr>
		</table>
		</form>
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">							
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Manage Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table id="listMCO" height="50px" class="scroll" cellpadding="0" cellspacing="0"></table>
							<div id="temppager" class="scroll" style="text-align:center;"></div>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<u:hasPrivilege privilegeId="xbe.tool.mco.issue">
										<input type="button" id="btnIssue"  name="btnIssue" class="Button" value="Issue MCO" tabindex="3">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="xbe.tool.mco.void">
										<input type="button" id="btnVoid"  name="btnVoid" class="Button" value="Void MCO" tabindex="4">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
					</td>
				</tr>
		</table>
		<form action="manageMCO!seachMCO.action" method="post" name="frmManageMCO" id="frmManageMCO">
				<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Manage MCO<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">					  			
							<tr>
								<td width="20%">
									<font>PNR</font>
								</td>
								<td width="20%">
									<input type="text" name="pnr" id="txtPnr" maxlength="10" size="20" value="" tabIndex="5">
									<font class="mandatory">&nbsp*</font>
								</td>
								<td>
									<u:hasPrivilege privilegeId="xbe.tool.mco.issue">  
										<input name="btnLoadPassengers"  type="button" class="Button" id="btnLoadPassengers" value="Load Passengers" style="width:140px" tabindex="6">
									</u:hasPrivilege>
								</td>
								<td width="1%">
								</td>
							<tr>
							<tr>
								<td width="8%">
									<font>Passenger Name</font>
								</td>
								<td width="15%">
									<select id="selPaxNames" name="mco.pnrPaxId" tabindex="7">
									</select> 
								</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td width="8%">
									<font>MCO Service Number</font>
								</td>
								<td width="15%">
									<select id="selMCOService" name="mco.mcoServiceId" tabindex="8">
										<c:out value="${requestScope.reqMCOService}" escapeXml="false" />
									</select>
								</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td width="8%">
									<font>Flight Number</font>
								</td>
								<td width="15%">
									<input type="text" name="mco.flightNo" id="txtFlightNo" maxlength="7" value="" tabIndex="9">
									<font class="mandatory">&nbsp*</font>  
								</td>
								<td width="15%"></td>
							</tr>	
							<tr>	
								<td width="8%">
									<font>Flight Departure Date</font>
								</td>
								<td width="15%">
									<input type="text" name="mco.flightDate" id="txtFlightDate" maxlength="24" size="20" value="" tabIndex="10">
									<font class="mandatory">&nbsp*</font>  
								</td>
								<td width="15%"></td>
							</tr>
							<tr>	
								<td width="8%">
									<font>Remarks</font>
								</td>
								<td width="15%">
									<input type="text" name="mco.remarks" id="txtRemarks" maxlength="24" size="20" value="" tabIndex="12">  
								</td>
								<td width="15%"></td>
							</tr>
							<tr>	
								<td width="8%">
									<font>Amount</font>
								</td>
								<td width="15%">
									<input type="text" name="mco.amountInLocal" id="txtAmount" maxlength="24" size="20" value="" tabIndex="13" maxlength="5">
									<font class="mandatory">&nbsp*</font>
									<select id="selCurrency" name="mco.currency" tabindex="14">
										<c:out value="${requestScope.reqCurrencyCombo}" escapeXml="false" />
									</select>  
								</td>
								<td width="15%"></td>
							</tr>
							<tr>	
								<td width="8%">
									<font>Email</font>
								</td>
								<td width="15%">
									<input type="text" name="mco.email" id="txtEmail" maxlength="50" size="20" value="" tabIndex="15">
								</td>
								<td width="15%"></td>
							</tr>
							<tr id="trMco">	
								<td width="8%">
									<font>MCO Number</font>
								</td>
								<td width="15%">
									<input type="text" name="mcoNumber" id="txtMcoNumber" maxlength="50" size="20" value="" tabIndex="15">
								</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td width="8%"></td>
								<td width="15%">
								</td>
								<td  width="50%" align="right">
									<u:hasPrivilege privilegeId="xbe.tool.mco.issue">
										<input name="btnPay"  type="button" class="Button" id="btnPay" value="Pay" style="width:100px" tabindex="16">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="xbe.tool.mco.print">
										<input name="btnPrint"  type="button" class="Button" id="btnPrint" value="Print MCO" style="width:100px" tabindex="17">
									</u:hasPrivilege>	
									<input name="btnClose" type="button" class="button" id="btnClose" value="Close" style="width:100px" tabIndex="18">
								</td>
							</tr>							
							</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>								
			</table>
			<input type="text" name="rowNo"  id="rowNo" style="display: none;" />
	</form>
	<%@ include file="../common/IncludeTabBottom.jsp"%>
  </body>
  <script src="../js/payments/ManageMCO.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  			<script type="text/javascript">
				var objCal1 = new Calendar();
	objCal1.onClick = "setDate";
	objCal1.blnDragCalendar = false;
	objCal1.buildCalendar();
	
	function setDate(strDate, strID){
		switch (strID){
			case "0" : setField("txtFlightDate",strDate);break ;
		}
	}
	
	function LoadCalendar(strID, objEvent){
			objCal1.ID = strID;
			objCal1.top = 130 ;
			objCal1.left = 0 ;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
	}
	</script>
  <%@ include file="../common/IncludeLoadMsg.jsp"%>
</html>