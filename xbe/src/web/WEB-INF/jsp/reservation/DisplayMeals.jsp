<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">			
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
 	
  </head>
  <body oncontextmenu="return showContextMenu()" onmousewheel="return false;" class="TabBGColor" onKeyUp="pageShortCuts(event)" ondrag='return false'  onkeydown='return PageKeyDown(event)' scroll="<u:scroll />">
 	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>			
		
		<tr>
			<td>
				<br>
				<table width='80%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td valign='top' align="center"><span id="spnMeals"></span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<script type="text/javascript">	
		var selMeals = opener.arrSelMealids;		
		var count = Math.floor(selMeals.length/3);		
		var mMod = selMeals.length%3;
		var imagPath = opener.mealImagePath;
		var strCurrency = opener.top.strDefCurr;
		var mealCount = 0;
		var strNotMessage = 'Image Is Not Available';
		var halaMessage  = "All Meals served is Halal";
		var strHtml = "<table width='50%' border='0' cellpadding='0' cellspacing='0'>"
		strHtml += "<tr>";
		strHtml += "<td colspan='3' align='left'><font style='color:red;font-size:17;weight:bold'>"+halaMessage;
		strHtml += "</font></td>";
			strHtml += "</tr>";	
		/** if(mMod == 1) {
			strHtml += "<tr>";
			strHtml += "<td colspan='3' align='center'>";
			strHtml += "<table border='0'>";
			strHtml += "<tr>";
			strHtml += "<td>";
			strHtml += "</td>";
			strHtml += "<td style='width:251'>";
			strHtml += "<table border='0'  bgcolor='#53504A' >";
			strHtml += "<tr>";
			strHtml += "<td colspan='2' align='center'><img height='220' width='220' alt='"+strNotMessage+"' src='"+imagPath+"meal_"+selMeals[0]['mealId'] +"_no_cache.jpg'/>";
			strHtml += "</td>";
			strHtml += "</tr>";
			strHtml += "<tr>";
			strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 2ex;'><font style='color:white;font-size:15;'><font style='color:white;font-size:15;'>"+selMeals[0]['mealName']+ "&nbsp;&nbsp;" +selMeals[0]['amount']+ "&nbsp;" + strCurrency+"</font><br><font style='color:white;font-size:12;'>"+selMeals[0]['description'];
			strHtml += "</font></td>";
			strHtml += "</tr>";
			strHtml += "</table>";
			strHtml += "</td>";
			strHtml += "</td>";
			strHtml += "<td>";
			strHtml += "</td>";
			strHtml += "</tr>";
			strHtml += "</table>";
			strHtml += "</td>";
			strHtml += "</tr>";
			mealCount = mealCount +1;
		}	**/		
		
		for(var i=0;i< count +1;i++) {
			if(count == i && mMod == 0) break;
			strHtml += "<tr>";
			strHtml += "<td>&nbsp;</td>";
			strHtml += "</tr>";
			strHtml += "<tr>";
			strHtml += "<td  bgcolor='#53504A' style='width:251'>";
			strHtml += "<table border='0'>";
			strHtml += "<tr>";
			strHtml += "<td><img height='220' width='251'  src='"+imagPath+"meal_"+selMeals[mealCount]['mealId'] +"_no_cache.jpg' alt='"+strNotMessage+"'/>";
			strHtml += "</td>";
			strHtml += "</tr>";
			strHtml += "</table>";
			strHtml += "</td>";
			if(count != i || (count == i && mMod != 1)) {
				strHtml += "<td width='15%'>&nbsp;</td>";
				strHtml += "<td  bgcolor='#53504A' style='width:251'>";
				strHtml += "<table border='0'>";
				strHtml += "<tr>";
				strHtml += "<td><img height='220' width='251' alt='"+strNotMessage+"' src='"+imagPath+"meal_"+selMeals[mealCount +1]['mealId'] +"_no_cache.jpg'/>";
				strHtml += "</td>";
				strHtml += "</tr>";
				strHtml += "</table>";
				strHtml += "</td>";
			}
			if(count != i) {
				strHtml += "<td width='15%'>&nbsp;</td>";
				strHtml += "<td  bgcolor='#53504A' style='width:251'>";
				strHtml += "<table border='0'>";
				strHtml += "<tr>";
				strHtml += "<td><img height='220' width='251' alt='"+strNotMessage+"' src='"+imagPath+"meal_"+selMeals[mealCount +2]['mealId'] +"_no_cache.jpg'/>";
				strHtml += "</td>";
				strHtml += "</tr>";
				strHtml += "</table>";
				strHtml += "</td>";
			}
			
			
			strHtml += "</tr>";
			strHtml += "<tr>";
			strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount]['amount']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount]['description'];
			strHtml += "</font></td>";
			if(count != i || (count == i && mMod != 1)) {
				strHtml += "<td>&nbsp;</td>";
				strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount +1]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount + 1]['amount']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount + 1]['description'];
				strHtml += "</font></td>";
			}
			if(count != i) {
				strHtml += "<td>&nbsp;</td>";
				strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount +2]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount + 2]['amount']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount + 2]['description'];
				strHtml += "</font></td>";
			}
			
			strHtml += "</tr>";
			mealCount = mealCount +3;
			
		}		
		strHtml += "</table>"
		DivWrite("spnMeals", strHtml);
	</script>		
  </body>
</html>