<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Taxes &amp; Surcharges Breakdown</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<style>
	html, body{overflow-x:hidden }
	</style>
  </head>
  <body onmousewheel="return false;" class="TabBGColor" ondrag='return false' oncontextmenu="return showContextMenu()" onkeydown='return Body_onKeyDown(event)'>
  <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
	<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Taxes &amp; Surcharges Breakdown<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td style="height: 5px" colspan="3"></td></tr>
						<tr>
							<td width="50%" valign="top" class="GridRow GridHeader GridHeaderBand GridHDBorder" style="padding: 2px 4px"><font>Taxes</font></td>
							<td width="50%" valign="top" class="GridRow GridHeader GridHeaderBand GridHDBorder" style="padding: 2px 4px"><font>Surcharges</font></td>
						</tr>
						
						<tr>
							<td width="50%" valign="top">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><span id="spnTaxBd"></span></td>
									</tr>
								</table>
							</td>
							<td width="50%" valign="top">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><span id="spnSurchargesBd"></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="right" colspan="2">
								<input type="button" id="btnClose" value="Close" class="Button" onClick="window.close()">	
							</td>
						</tr>
					</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>		
	</table>
		<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->

		<script type="text/javascript">

		function writeTaxBd() {
			var strHTMLText = "";			
			strHTMLText += opener.top.taxBdHtml
			DivWrite("spnTaxBd",strHTMLText);
		}
		writeTaxBd();

		function writeSurchargeBd(){
			var strHTMLFlexiText = "";
			strHTMLFlexiText += opener.top.surchargeBdHtml;
			DivWrite("spnSurchargesBd", strHTMLFlexiText);
		}
		writeSurchargeBd();
		
	</script>
	</body>

</html>

