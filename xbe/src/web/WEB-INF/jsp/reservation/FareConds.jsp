<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Fares Terms &amp; Conditions</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
  </head>
  <body onmousewheel="return false;" class="TabBGColor" ondrag='return false' oncontextmenu="return showContextMenu()" onkeydown='return Body_onKeyDown(event)'>
  <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
	<table width="99%" align="center" border="0" cellpadding="2" cellspacing="0">		
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Fare Terms &amp; Conditions<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellspacing="2" cellpadding="0">
						<tr>
							<td><span id="spnFareCat"></span></td>
						</tr>
					</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Flexi Charges<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellspacing="2" cellpadding="0">
						<tr>
							<td><span id="spnFlexiFareCat"></span></td>
						</tr>
						<tr>
							<td align="right">
								<input type="button" id="btnClose" value="Close" class="Button" onClick="window.close()">	
							</td>
						</tr>
					</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>		
	</table>
		<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->

		<script type="text/javascript">
		var arrFarecat = opener.arrFareCats;
		var arrFareDetail = opener.arrFareDetails;

		function writeFareType() {
			var strHTMLText = "";			
			strHTMLText += opener.top.fareCondsHtml
			DivWrite("spnFareCat",strHTMLText);
		}
		writeFareType();

		function writeFlexiType(){
			var strHTMLFlexiText = "";
			strHTMLFlexiText += opener.top.flexiCondsHtml;
			DivWrite("spnFlexiFareCat", strHTMLFlexiText);
		}
		writeFlexiType();
		
	</script>
	</body>

</html>

