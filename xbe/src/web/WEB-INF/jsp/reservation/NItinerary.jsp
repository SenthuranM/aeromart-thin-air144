<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>:: <c:out value="${applicationScope.appCarrierName}" escapeXml="false" /></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/Style_no_cache.css'>
    <script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
   	<script src="../js/common/PrivilegeConstants.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
    <script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
    <script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>		
  </head>
  <body oncontextmenu="return showContextMenu()" class="TabBGColor"  onkeydown='return Body_onKeyDown(event)' ondrag='return false' onmousewheel="return false;">
	   <table width="100%" cellpadding="0" cellspacing="5" border="0" align="center" ID="Table1">
			<tr>
				<td>
					<table width='100%' border='0' cellpadding='2' cellspacing='0'>
						<tr>
							<td colspan='2'>
								<font class='fntBold'>New booking is successfully completed</font>
							</td>
						</tr>
						<tr>
							<td valign='top' colspan='2'>
								<font id="fntTitle" class='fntBold'>&nbsp;</font>
								<span id="spnOnHoldPNR"></span>
							</td>
						</tr>
						<tr>
							<td valign='bottom'>
								<font>PNR : <font class='fntBold fntLarge'><span id='spnPNR'></span></font></font>
							</td>
							<td valign='bottom' align="right">
								<font><span id='spnOnHold'></span></font>
							</td>
						</tr>
						<tr>
							<td valign='bottom' colspan='2'>
								<font class='fntBold'><span id='spnPolicyCode'></span></font>
							</td>
						</tr>
						<tr>
							<td colspan='2'>
								<table width='100%' border='0' cellpadding='2' cellspacing='1'>										
									<tr>
										<td>
											<span id='spnOutFlights'></span>
										</td>
									</tr>
									<tr>
										<td>
											<span id='spnRetFlights'></span>
										</td>
									</tr>
									<tr>
										<td class='Dots'>
										</td>
									</tr>
									<tr>
										<td>
											<span id='spnPassengers'></span>
										</td>
									</tr>
									<tr>
										<td class='Dots'>
										</td>
									</tr>
									<tr>
										<td>
											<span id='spnPayments'></span>
										</td>
									</tr>
									<tr>
										<td class='Dots'>
										</td>
									</tr>
									<tr>
										<td>
											<table width='100%' border='0' cellpadding='2' cellspacing='0'>
												<tr>
													<td colspan='4' class="GridHeader">
														<font class='fntBold'>Contact Details</font>
													</td>
												</tr>
												<tr>
													<td width='9%' class="GridItems NoFaresBGColor">
														<font class='fntArabic'>First Name :</font>
													</td>
													<td width='41%' class="GridItems NoFaresBGColor">
														<font class='fntBold fontCapitalize'><span id="spnFName"></span></font>
													</td>
													<td width='9%' class="GridItems NoFaresBGColor">
														<font>Last Name :</font>
													</td>
													<td width='41%' class="GridItems NoFaresBGColor">
														<font class='fntBold fontCapitalize'><span id="spnLName"></span></font>
													</td>
												</tr>
												<tr>
													<td class="GridItems NoFaresBGColor">
														<font>Address : </font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnAdd1"></span></font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font><!-- Nationality : --></font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnNationality"></span></font>
													</td>
												</tr>
												<tr>
													<td class="GridItems NoFaresBGColor"><font>&nbsp;</font></td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnAdd2"></span></font>
													</td>
													<td colspan='2' class="GridItems NoFaresBGColor"><font>&nbsp;</font></td>
												</tr>
												<tr>
													<td class="GridItems NoFaresBGColor">
														<font>City :</font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnCity"></span></font>
													</td>
													<td colspan='2' class="GridItems NoFaresBGColor"><font>&nbsp;</font></td>
												</tr>
												<tr>
													<td class="GridItems NoFaresBGColor">
														<font>Country :</font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnCountry"></span></font>
													</td>
													<td class="GridItems NoFaresBGColor"><font>Mobile No :</font></td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnMobile"></span></font>
													</td>
												</tr>
												<tr>
													<td class="GridItems NoFaresBGColor">
														<font>Phone No :</font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnPhone"></span></font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font>Fax :</font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnFax"></span></font>
													</td>
												</tr>
												<tr>
													<td class="GridItems NoFaresBGColor">
														<font>E-mail :</font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font class='fntBold'><span id="spnemail"></span></font>
													</td>
													<td class="GridItems NoFaresBGColor">
														<font>&nbsp;</font>
													</td>
													<td class="GridItems NoFaresBGColor">
														
													</td>
												</tr>
												<tr>
													<td colspan='4'  class="GridItems NoFaresBGColor Dots">
														<font>&nbsp;</font>
													</td>
												</tr>	
												<tr>
													<td colspan='2'>
														<span id="spnLanguage">
															<table width="100%" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="25%">
																		<font>Itinerary Language </font>
																	</td>
																	<td wrap=false>
																		<span id="spnLanguages"></span>
																	<td>
																	<u:hasPrivilege  privilegeId="xbe.res.itn.chg">
																	<td>
																		<input type="checkbox" id="chkPrintCharges" onclick="chkPrintChargesClick()" class="noBorder"><font>Print Charges</font>
																	</td>
																	</u:hasPrivilege>
																	<td>
																		<textarea id="txtTermsNCond" style="visibility:hidden;height:1px;width:1px;" cols="1" rows="1"><c:out value="${requestScope.termsNCond}" escapeXml="false" /></textarea>
																		<textarea id="txtTermsNCondAR" style="visibility:hidden;1px;width:1px;" cols="1" rows="1"><c:out value="${requestScope.termsNCondAR}" escapeXml="false" /></textarea>
																	</td>
																</tr>
															</table>
														</span>
													</td>
													<td colspan='2' align='right'>
														<input type='button' id='btnPrint' value='Print' class='Button' onclick='pageBtnOnClick(0)'>
														<u:hasPrivilege  privilegeId="xbe.res.rcpt.print">
															<input type='button' id='btnPrintReciept' value='Print Reciept' class='Button' onclick='pageRecptBtnOnClick(0)'>
														</u:hasPrivilege>
														<!-- <input type='button' id='btnEmail' value='Email' class='Button' onclick='pageBtnOnClick(1)'> -->
													</td>
												</tr>	
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
			</td>
		</tr>	
	  </table>								
  </body>
	<script type="text/javascript">	
	<!--

		var strError			= ""
		var strMsgDept 			= 'Departing Flight(s)';		
		var strMsgRet 			= 'Returning Flight(s)';	
		var strMsgPsgHD 		= 'Passenger Information';		
		var strMsgPayHD 		= 'Payment Details';	
		var strMsgConfAdult 	= 'Adult(s)';	
		var strMsgConfInfant 	= 'Infant(s)';	
		var strMsgConfChild 	= 'Children';	
		var strMsgConfPsngName 	= 'Passenger Name';	
		var strMsgConfTWidth 	= 'Travelling with';	
		var strPNR 				= "<c:out value='${requestScope.pnrNo}' escapeXml='false' />"
		var strBKGStatus 		= "<c:out value='${requestScope.bkgStatus}' escapeXml='false' />"
		strError 				= "<c:out value='${requestScope.Error}' escapeXml='false' />";
		var strAlign 			= "right";
		var strSeatDetails   	= 'Additional Service Details';	
		
		var arrPsg	   = new Array();
		var arrOFlt	   = new Array();
		var arrRFlt    = new Array();
		var arrFare	   = new Array();
		var arrPsng    = new Array();
		var arrPaxPay  = new Array();
		var arrSegChg  = new Array();
		var arrAgent   = new Array();
		var arrCompAdd = new Array();
		var arrPaxSeat = new Array();
		var strReleaseDateTime 		= "";
		
		var strOnHoldReleaseTime 	= "";
		var arrOnHoldPsng 			= new Array();
		var strOnHoldPNR 			= "<c:out value='${requestScope.pnrNoOnHold}' escapeXml='false' />";
		<c:out value='${requestScope.pnrInfo}' escapeXml='false' />
		<c:out value='${requestScope.comAddress}' escapeXml='false' />

		// On Hold info
		<c:out value='${requestScope.pnrInfoOnHold}' escapeXml='false' />
		
		var intAgtBalance = "<c:out value='${requestScope.sesAgentBalance}' escapeXml='false' />";
		var showAgentCreditInAgentCurrency = "<c:out value='${requestScope.sesShowAgentBalanceInAgentCurrency}' escapeXml='false' />";
		var agentAvailableCreditInAgentCurrency = "<c:out value='${requestScope.sesAgentBalanceInAgentCurrency}' escapeXml='false' />";

	//-->
	</script>			
	<script src="../js/reservation/NItinerary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
</html>
			