<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title><%=request.getParameter("strMsgType")%></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">


	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  </head>

  <body scroll="yes" class="tabBGColor" oncontextmenu="return false" ondrag='return false' onLoad="winOnLoad()">
  			<script type="text/javascript">
				<c:out value="${requestScope.reqGDSMessageData}" escapeXml="false" />
				<c:out value="${requestScope.reqIsExceptionOccured}" escapeXml="false" />
			</script>
		  <form method="post" >
				<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td align="left" colspan="2"><font><textarea id="strGDSMessageText" rows="17" style="width: 100%" readonly></textarea></font></td>
							</tr>
							<tr>
								<td width="50%" align="left"><input tabindex="1" type="button" id="btnClose" class="Button" value="Close" onclick="window.close()"></td>	
							</tr>
					  	</table>
					</td>
				</tr>
			</table>
	</form>
  </body>
  <script type="text/javascript">

	function winOnLoad() {
		if(isException == 'false' ){
			MsgData = MsgData.replace(/~/g, '\n');
			setField("strGDSMessageText",MsgData);	
			getFieldByID("btnClose").focus();
		}else{
			windowClose();
		}
	}

	function windowClose(){
		window.close();
	}

  </script>
</html>