<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<%-- Temporally Removed the theme changes in user wise --%>
	<%--<link href="../themes/<%=com.isa.thinair.xbe.core.web.util.ThemeUtil.getTheme(request) %>/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />--%>
	<%-- Added to Load css to be loaded client wise --%>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
    <script src="../js/common/menuG5FX.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/PrivilegeConstants.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
   <script language="javascript">
   
  
   <!--
    	
    		var arrInvoice ; 
    		var intLastRec = 1;
    		var manifestSearchValue = "";
    		var pnladlsearch = "";
    		var checkinPfsDetails = "";
    		var pnladldata = "";
    		var arrPNRHistory = new Array("","","","","","","","","","","","","","","");
    		// Variabl initialization onload of the every page 
    		// from the menu link 
    		function initializeVar(){
	    		intLastRec = 1 ;
	    		top.blnSeatBlocked = false;
	    		top.pageEdited = false;
	    		manifestSearchValue = "";
	    		checkinPfsDetails = "";
	    		pnladlsearch = "";
	    		pnladldata = "";
	    		top.varInitialize();
    		}
    		
    		function addPNRHistory(strPnrNo, blnSetName){

	    		if(typeof(top[1].frames["frmDetail"]) == "object" 
	    			&& top[1].frames["frmDetail"].arrPassengerData != null
	    			&& top[1].frames["frmDetail"].arrPassengerData.length > 0){
					//Check if its comin from alert page
				
					var objFrameName = top[1].frames["frmDetail"];
					var intMaxLength = arrPNRHistory.length - 1
					var blnTrue = false;
					for (var i = 0 ; i < arrPNRHistory.length ; i++)
	    			{
						if (arrPNRHistory[i] == (strPnrNo + ' - ' + converToTitleCase(objFrameName.arrPassengerData[0][3]))){
							blnTrue = true;
						}
	    			}
	
					if (!blnTrue){
						for(var i = 0 ; i < (intMaxLength) ; i++)
						{
							arrPNRHistory[i] = arrPNRHistory[i+1];
						}
						
						if(objFrameName.arrPassengerData[0][3] != null && objFrameName.arrPassengerData[0][3] != "" && blnSetName){
							arrPNRHistory[intMaxLength] = (strPnrNo + ' - ' +  converToTitleCase(objFrameName.arrPassengerData[0][3]));
						}else{
							arrPNRHistory[intMaxLength] = strPnrNo;
						}
					}
				}
    		}


    		function addPNRHistoryNew(strPnrNo, paxName){
	    		
				var intMaxLength = arrPNRHistory.length - 1
				var blnTrue = false;
				for (var i = 0 ; i < arrPNRHistory.length ; i++){
					if(paxName == ""){
						if (arrPNRHistory[i] == strPnrNo){
							blnTrue = true;
						}
					} else {
						
						if (arrPNRHistory[i] == strPnrNo + ' - ' + paxName){
							blnTrue = true;
						}
					}
	    		}
	
				if (!blnTrue){
					for(var i = 0 ; i < (intMaxLength) ; i++){
						arrPNRHistory[i] = arrPNRHistory[i+1];
					}
						
					if(paxName != ""){
						arrPNRHistory[intMaxLength] = (strPnrNo + ' - ' +  paxName);
					}else{
						arrPNRHistory[intMaxLength] = strPnrNo;
					}
				}
				
    		}

    		/*function addLCCPNRHistory(strPnrNo){
	    		var intMaxLength = arrPNRHistory.length - 1

				//using the same logic above
				var blnTrue = false;
	    		for (var i = 0 ; i < arrPNRHistory.length ; i++){
					if (arrPNRHistory[i] == (strPnrNo + ' - ' +"INTERLINE")){
						blnTrue = true;
					}
    			}
				if(!blnTrue) {
					for(var i = 0 ; i < (intMaxLength) ; i++) {
						arrPNRHistory[i] = arrPNRHistory[i+1];
					}
					arrPNRHistory[intMaxLength] = strPnrNo+ ' - ' +"INTERLINE";	
				}					
								
    		}*/
    		
    		function addLCCPNRHistory(strPnrNo, paxName){
	    		var intMaxLength = arrPNRHistory.length - 1

				//using the same logic above
				var blnTrue = false;
	    		for (var i = 0 ; i < arrPNRHistory.length ; i++){
					if (arrPNRHistory[i] == (strPnrNo + ' - ' + paxName + ' - ' +"INTERLINE")){
						blnTrue = true;
					}
    			}
				if(!blnTrue) {
					for(var i = 0 ; i < (intMaxLength) ; i++) {
						arrPNRHistory[i] = arrPNRHistory[i+1];
					}
					arrPNRHistory[intMaxLength] = strPnrNo + ' - ' + paxName + ' - ' +"INTERLINE";	
				}					
								
    		}

			function removePNRFromHistory(strPNR){
			
				var tempPNR = "";
				var blnFound = false;
				var intMaxLength = arrPNRHistory.length - 1

				for (var i = arrPNRHistory.length-1 ; i > 0 ; i--){
					var tempStr = arrPNRHistory[i].split("-");
					tempPNR = trim(tempStr[0]);
					
					if (tempPNR == strPNR){
						blnFound = true;
					}

					if (blnFound){
						arrPNRHistory[i] = arrPNRHistory[i-1];
					}
				}
			}
    		
    		function initPNRHistory(){
    			for(var i = 0 ; i < arrPNRHistory.length ; i++)	{
    				arrPNRHistory[i] = " ";
    			}
    		}
    		function showPNRIcon(){
    			for (var i = 0; i < arrPNRHistory.length; i++){
    				if(arrPNRHistory[i] != " "){
    					document.getElementById("spnPLRListIcon").style.display = "block";
    					break;
    				}
    			}
    		}
    		initPNRHistory();
			

    		
    	//
    --></script>	
  </head>
  <body onmousewheel="return false;" onload="initMenu('AAMenu','top'); setSubFrame('AAMenu',parent.main);" oncontextmenu="return showContextMenu()" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		<!--<body onmousewheel="return false;" onload="initMenu('AAMenu','top'); setSubFrame('AAMenu',parent.main);" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		--><table width="999" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
				<td width="12" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
				<!--<td height="27" align="right" valign="bottom" style="background-image:url(../images/AA126_1_no_cache.jpg)">
					-->
					<td align="right" class="topmenubg">
					<table width="430" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.action">
								<td width="70" align="center" valign="middle">
									<span id="requestAction" style="cursor: pointer;" title="View In Progress Requests">
										<img border="0" src="../themes/default/images/accelaero/pnrList_no_cache.gif" align="absmiddle"/>&nbsp;<b>Requests</b>
									</span>
								</td>
							</u:hasPrivilege>
							<td width="75" align="center" valign="middle">
								<span id="spnPLRListIcon" style="display:none;cursor: pointer;" title="View PNR List">
									<img border="0" src="../themes/default/images/accelaero/pnrList_no_cache.gif" align="absmiddle"/>&nbsp;<b>PNR List</b>
								</span>
							</td>
							<td width="5">&nbsp;</td>
							<td width="34" align="center" valign="middle">
								<span id="spnErrorLog" style="display:none;cursor: pointer;" title="View Error Messages">
									<img id="imgMM1" border="0" src="../images/error_icon_no_cache.gif" style="margin-top: 4px">
								</span>
							</td>
							<td valign="top" align="left" class="top-menu-end-end">
								<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
								<td class="top-menu-end-start" height="30" width="80" align="center">
									<span id="spnCarrierCode" class="carrier-code"></span>
								</td>
								<td width="110" height="30">
									<img alt="AccelAero" src="../images/aa_logo_top_xbe_no_cache.jpg" />
								</td>
								<td width="65" height="30" valign="top">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maxMin"><tr>
										<td valign="top" align="center">
											<a href="javascript:void(0)" onclick="window.blur()" 
											title="Minimize Window" class="winMinimize">
												<img id="imgMMMin" border="0" src="../images/spacer_no_cache.gif" width="17">
											</a>
										</td>
										<td valign="top" align="center">
											<a href="javascript:void(0)" onclick="CloseWindow()" 
											title="Sign Out" class="winClose">
												<img id="imgMMCl" border="0" src="../images/spacer_no_cache.gif" width="17">
											</a>
										</td>
									</tr>
									</table>
								</td>
								</tr></table>
							</td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>
	<span id="spnSysMessage" style="position:absolute;top:4px;left:430;"></span>
	</body>
	<script type="text/javascript">
	<!--
		function ShowSysMessage(){
			var strText = "";
			if (url.indexOf('agents') !=-1){
				strText = '<font class="fntBold fntLarge" style="color:#FF0000">LIVE BOOKINGS</font>';
				DivWrite("spnSysMessage", strText);
			}
			// Set Carrier Code
			if (top.carrierCode != undefined && top.carrierCode != "" ) {
				strText = '<font class="fntBold fntLarge" style="color:#FF0000">'+ top.carrierCode +'</font>';
				DivWrite("spnCarrierCode", strText);
			}
		}

		function loadSwitchMenu(){
			var autologinAction = "<c:out value='${sessionScope.autologinAction}' escapeXml='false'/>";
			<c:remove var="autologinAction" scope="session" />
			if(autologinAction == "MAKERESERVATION"){
				top.loadMakeBkgV2();
			}else if(autologinAction == "FINDRESERVATION"){
				top.loadModifyBkgV2();
			}	
		}

		function closeChildWindows(){
			if ((top.objCWindow)&& (!top.objCWindow.closed)){top.objCWindow.close();}
		}

		function processStateChange(){
			//setTimeout('window.top.close()',1000);
			closeChildWindows();
			top[1].location.replace("showMain");
		}
		function CloseWindow(){
			if (!confirm("TAIR-90116: Are you sure you want to Sign Out?")){
				return;
			}else{
				makePOST("../public/logout.action","");
			}		
		}
		

		ShowSysMessage();
		setTimeout("loadSwitchMenu()",500);		
	//-->
	</script>
</html>