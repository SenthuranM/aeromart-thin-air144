<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <style type="text/css">
  	.tabframe, .tabframe body{
  		overflow-x: hidden;
  		overflow-y: hidden;
  	}
  	
  </style>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		<%-- Temporally Removed the theme changes in user wise --%>
	<%--<link href="../themes/<%=com.isa.thinair.xbe.core.web.util.ThemeUtil.getTheme(request) %>/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />--%>
	<%-- Added to Load css to be loaded client wise --%>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/main.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script language="javascript" src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>		
	
	<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/jquery.clock.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/dashbordToolBar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<%-- 
		String dashboardEnabled = (String) request.getSession().getAttribute("dashboardEnabled");
		if(dashboardEnabled != null && dashboardEnabled.trim().equalsIgnoreCase("Y")) { --%>
			<%--
			<link rel="stylesheet" type="text/css" href="../css/dashboard/Dashboard_Style_no_cache.css">
			<script src="../js/dashboard/prototype.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/dashboard/effects.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			<script src="../js/dashboard/side-bar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
			--%>
	<%--  } --%>
  </head>
	 <!-- <body onmousewheel="return false;" scroll="no" oncontextmenu="return showContextMenu()" onkeydown='return Body_onKeyDown(event)' ondrag='return false'> -->
	 	 <body onmousewheel="return false;" scroll="no" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	 <table width="999" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
		<tr>
			<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
			<td width="974" valign="top">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
					<tr>
						<td width="30"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
						<td width="914" class="PageBackGround" style="height:390px;" valign="top">
						<span id="spnWelcomeMsg">						
						</span>
						<%--Commented temporary msg --%>
						<%-- 
						<div class="alignLeft spMsg">
							<label style="font-size: 1.3em;">
								&nbsp;&nbsp;&nbsp;Website is currently going through maintenance. If you face any problems, kindly call +97165580000 or email notify@airarabia.com
							</label>
						</div>
						--%>
						</td>
						<td width="30"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
					</tr>
					<tr>
						<td colspan="3" align="center" class="PageBackGround" style="height:267px;" valign="bottom">
							<table width="100%" cellpadding="0" cellspacing="0">
							<%if(application.getResource("/images/XBEmain_no_cache.jpg") == null){%>
								<tr>
									<td style="height:130px;width:134;background-image:url(../images/AA166_12_no_cache.jpg);"></td>
									<td style="height:130px;width:143;background-image:url(../images/AA166_13_no_cache.jpg);"></td>
									<td style="height:130px;width:143;background-image:url(../images/AA166_14_no_cache.jpg);"></td>
									<td id="imgMP1" style="height:130px;width:143;background-image:url(../images/AA166_1_no_cache.jpg);"></td>
									<td id="imgMP2" style="height:130px;width:152;background-image:url(../images/AA166_2_no_cache.jpg);"></td>
									<td id="imgMP3" style="height:130px;width:131;background-image:url(../images/AA166_3_no_cache.jpg);"></td>
									<td id="imgMP4" style="height:130px;width:128;background-image:url(../images/AA166_4_no_cache.jpg);"></td>
								</tr>
								<tr>
									<td style="height:137px;width:134;background-image:url(../images/AA166_5_no_cache.jpg);"></td>								
									<td style="height:137px;width:143;background-image:url(../images/AA166_6_no_cache.jpg);"></td>							
									<td style="height:137px;width:143;background-image:url(../images/AA166_7_no_cache.jpg);"></td>							
									<td id="imgMP5" style="height:137px;width:143;background-image:url(../images/AA166_8_no_cache.jpg);"></td>
									<td id="imgMP6" style="height:137px;width:152;background-image:url(../images/AA166_9_no_cache.jpg);"></td>
									<td id="imgMP7" style="height:137px;width:131;background-image:url(../images/AA166_10_no_cache.jpg);"></td>
									<td id="imgMP8" style="height:137px;width:128;background-image:url(../images/AA166_11_no_cache.jpg);"></td>
								</tr>

							<%}else {%>
								<tr>
									<td style="height:267px; background-image:url(../images/XBEmain_no_cache.jpg);"></td>
								</tr>
							<%}%>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
		</tr>
	</table>
	
	<div id="divExit" style='visibility:hidden; position:absolute; top:50%; left:40%; width:260; height:40;z-index:2;'>
		<table width='260' border='0' cellpadding='2' cellspacing='1' align='center' class='GridRow GridHeader GridHeaderBand GridHDBorder'>
			<tr>
				<td>
					<table width='250' border='0' cellpadding='2' cellspacing='1' align='center'>
						<tr>
							<td align='center' style="height:30; background-image: url(../images/Progress_no_cache.gif)">
								<font class="fntBold">Shutting down in progress. Please Wait...</font>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<span id="spnTabMenus" style="position:absolute;top:10px;width:97%;top:5px;left:14px;visibility:hidden;z-index:4;background:#fff;"></span>
	</body>
	<script language="javascript">
	<!-- 
	var objTMenu = new tabMenu("spnTabMenus");
	objTMenu.width = 954;
	objTMenu.height = 580;
	objTMenu.left = "30";
	objTMenu.scrolling = 'auto';
	window._imagePath = "../images/" ;
	objTMenu.tabOnClick = "top.defTabClick";
	objTMenu.selectedTabBold = true;
	objTMenu.onUnload = "tabOnUnload"
	objTMenu.buildTabMenu();
	
	function tabOnUnload(){
		top[2].HidePageMessage();
		top.setFID("");
	}
		
		var strPageID = "MENU";
		var reqMessage='<c:out value='${requestScope.reqMessage}' escapeXml='false' />';		
		document.getElementById('spnWelcomeMsg').innerHTML = "<br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class='fntLarge' i18n_key='loggedInText'>Welcome to "+top.carrier+"</font>";
		var request=new Array();
		
		<c:out value='${requestScope.reqJSArray}' escapeXml='false' />
		onLoad();
		
		if (top.blnExit){
			getFieldByID("imgMP1").style.backgroundImage = 'url("../images/AA166_1_A_no_cache.jpg")';
			getFieldByID("imgMP2").style.backgroundImage = 'url("../images/AA166_2_A_no_cache.jpg")';
			getFieldByID("imgMP3").style.backgroundImage = 'url("../images/AA166_3_A_no_cache.jpg")';
			getFieldByID("imgMP4").style.backgroundImage = 'url("../images/AA166_4_A_no_cache.jpg")';
			getFieldByID("imgMP5").style.backgroundImage = 'url("../images/AA166_8_A_no_cache.jpg")';
			getFieldByID("imgMP6").style.backgroundImage = 'url("../images/AA166_9_A_no_cache.jpg")';
			getFieldByID("imgMP7").style.backgroundImage = 'url("../images/AA166_10_A_no_cache.jpg")';
			getFieldByID("imgMP8").style.backgroundImage = 'url("../images/AA166_11_A_no_cache.jpg")';

			setVisible("divExit", true);
			setVisible("spnWelcomeMsg", false);
		}
		dashbordToolBar.init();		
		//document.getElementById('spnDashboard').innerHTML = top.dashboardContent;
		function loadDemoDB(){
			//if (top.arrPrivi['plan.fares360'] == "1"){
				var demoStr = '<iframe src="showDemoDB" name="DBFrame" frameborder="0"  scrolling="no" style="height:630px;width:950px;border: 0;display:block;" id="DBFrame"></iframe>';
				// create the container div
				var dv = document.createElement('div');
				var body = document.getElementsByTagName('body')[0];
				// apply class to container div
				dv.setAttribute('id', 'DBFrameDiv');
				dv.setAttribute('style', 'position:absolute; top:10px; left:25px; width:950px; height:630px;z-index:3;');
		        // put the divs having class C inside container div
				// finally append the container div to body
				body.appendChild(dv);
				document.getElementById('DBFrameDiv').innerHTML = demoStr;
			//}
		}
		if (top.demoDispay){
			loadDemoDB();
		}
			
	//-->
	</script>
</html>