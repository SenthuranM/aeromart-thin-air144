<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

	<script type="text/javascript">
		var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />		
	</script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmreportprb" id="frmreportprb" action="" method="post" enctype="multipart/form-data">		
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
			<tr><td><font class="Header">Report a Problem</font></td></tr>
			<tr>
				<td>
					<%@ include file="../common/IncludeMandatoryText.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Report a Problem<%@ include file = "../common/IncludeFormHD.jsp" %>
				 	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblrepProblem">
						<tr>
							<td width="25%"><font class="fntBold">Your Name</font></td>
							<td><input name="txtName" id="txtName" type="text"><font class="mandatory"><b>&nbsp;*</b></font></td>				
						</tr>
						<tr>
							<td><font class="fntBold">Your E-mail Address</font></td>
							<td><input name="txtEMail" id="txtEMail" type="text"><font class="mandatory"><b>&nbsp;*</b></font></td>				
						</tr>
						<tr>
							<td><font class="fntBold">Problem Summary</font></td>
							<td><input name="txtSummary" id="txtSummary" type="text"><font class="mandatory"><b>&nbsp;*</b></font></td>				
						</tr>
						<tr>
							<td valign="top"><font class="fntBold">Problem Description</font></td>
							<td><textarea rows="10" cols="60" name="txtProblem" id="txtProblem"></textarea><font class="mandatory"><b>&nbsp;*</b></font></td>				
						</tr>						
						<tr>
							<td><font class="fntBold">Upload a File</font></td>
							<td><input type="file" name="file"></td>
						</tr>
															
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr>
							<td colspan="2">
								<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblrepProblembtn">
									<tr>
										<td>
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
										</td>
										<td align="right">
											<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="reSetClick()">
											<input name="btnReport" type="button" class="Button" id="btnReport" value="Report" onClick="reportClick()">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
		<script src="../js/problem/reportProblem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">				
		</script>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">		
	</form>	
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
  <%@ include file="../common/IncludeLoadMsg.jsp"%>  
</html>
