<%@ page language="java"%>
<%
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setDateHeader("Expires", 0); //prevents caching at the proxy server
response.setHeader("Cache-Control", "private"); // HTTP 1.1 
response.setHeader("Cache-Control", "no-store"); // HTTP 1.1 
response.setHeader("Cache-Control", "max-stale=0"); // HTTP 1.1 
%>
<%@ include file="../common/Directives.jsp" %>
<html>
<head>
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
</head>
<body onkeydown='return Body_onKeyDown(event)' ondrag='return false' oncontextmenu="return showContextMenu()"  onbeforeunload="CloseChildWindow()">
<%@ include file="../common/IncludeTop.jsp"%>
			<c:out value='${sessionScope.sesFormHtml}' escapeXml='false' /> 
			<br><br><br><br><br><br><br><br><br><br><br>
			<center><div id="divMsg"></div></center>
			<br><br><br><br><br><br><br><br><br><br>
		</td>
		<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
	</tr>
</table>
</body>
<script>
if(document.forms[0]['redirectMsg']){
	DivWrite('divMsg',document.forms[0]['redirectMsg'].value);
}
document.forms[0].submit();
</script>
</html>
