<%@ page language="java" %>
<html>
<head>
    <title>External PaymentGateway Feedback</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
</head>
<body onload="callParent()">
<script>
    function callParent() {
        window.close();
        window.opener.UI_tabPayment.externalPaymentGatewayFeedback();
    }

</script>
</body>
</html>
