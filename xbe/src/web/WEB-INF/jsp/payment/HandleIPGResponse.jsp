<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<html>
<head>
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
</head>
<body>
<c:out value='${sessionScope.sesFormHtml}' escapeXml='false' />
<br><br><br><br><br><br><br><br><br><br><br>
<center><div id="divMsg"></div></center>
</body>
<script>
var frm=document.forms[0];
var errCode=frm['hdnPaymentErrorCode'].value;
if(frm['redirectMsg']){
	DivWrite('divMsg',frm['redirectMsg'].value);
}

opener.continueWithExternalPayment(errCode=='');	
</script>
</html>
