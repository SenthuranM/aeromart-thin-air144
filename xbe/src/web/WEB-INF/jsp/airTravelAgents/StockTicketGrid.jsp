<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View / Adjust Ticket Stock</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script type="text/javascript">
  		var arrFormData = new Array();
  		var arrStockData = new Array();
  		var runNo = "";
		var lastEticket = "";
  		<c:out value="${requestScope.reqStockData}" escapeXml="false" />
  		<c:out value="${requestScope.reqETktNo}" escapeXml="false" />
  		<c:out value="${requestScope.reqerrData}" escapeXml="false" />
  		<c:out value="${requestScope.reqLastETktNo}" escapeXml="false" />  		
		
  </script>
  
  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" 
		onLoad="CHOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
   <form method="post" id="frmCredit" name="frmCredit" action="showAdjustStock.action">
   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr><td><font class="Header">View / Adjust Ticket Stock</font></td></tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>	
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>			
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Ticket Stock History<%@ include file="../common/IncludeFormHD.jsp"%>
		  			<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr><td colspan="2"><font class="fntSmall">&nbsp;</font></td></tr>
  						<tr>
  							<td colspan="2"><font class="fntBold">Agent / GSA Name :<span id="spnAgentName"></span></font></td>
  						</tr>	
						<tr><td colspan="2"><font class="fntSmall">&nbsp;</font></td></tr>					
  						<tr><td colspan="2"><span id="spnStockHistory"></span></td></tr>
		  				<tr>
							<td>
								<u:hasPrivilege privilegeId="ta.stock.add">
									<input name="btnAdd"  type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()" tabindex="1">
								</u:hasPrivilege>																							  	
							</td>
							<td align="right">
																	
							</td>
						</tr>
		  			</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td><%@ include file="../common/IncludeFormTop.jsp"%>Ticket Stock Details<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td>	
							<table width="100%" border="0" cellpadding="2" cellspacing="0">
								<tr>
									<td width="20%"><font>Last Ticket Number</font></td>
									<td colspan="2"><font class="fntBold"><span id="spnRunSeries"></span></font></td>
								</tr>	
								<tr><td><font>Stock Quantity</font></td>
									<td colspan="2">
										<input type="text" name="txtSrsQty" id="txtSrsQty" maxlength="16" ><font class="mandatory">&nbsp;*</font>
									</td>
								</tr>

								<tr>
									<td valign="top" ><font>Reason for Change</font></td>
									<td colspan="2" valign="top">
										<textarea name="txtRemarks" id="txtRemarks" cols="40" rows="6" onkeyUp="validateTextArea(this)" onkeyPress="validateTextArea(this)"></textarea>
										<font class="mandatory">&nbsp;*</font>
									</td>
								</tr>
								<tr>
									<td colspan="3">	
										<font>&nbsp;</font>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onClick="closeClick()">
										<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()">
									</td>
									<td align="right">
										<u:hasPrivilege privilegeId="ta.maint.stock.update">
											<input name="btnSave"  type="button" class="Button" id="btnSave" value="Save" onClick="saveClick()">
										</u:hasPrivilege>
									</td>
								</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
			</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
  	<script src="../js/airTravelAgents/TicketStockGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/airTravelAgents/StockValidate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<input type="hidden" name="hdnMode" id="hdnMode" value="">
	<input type="hidden" name="hdnAgentCode" id="hdnAgentCode" value="">
	<input type="hidden" name="hdnCMode" id="hdnCMode" value="">	
  </form>
  </body>
</html>