<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Agent Incentives</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/MultiDropDownDup.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script type="text/javascript">
  		var arrFormData = new Array();  			
  		<c:out value="${requestScope.reqerrData}" escapeXml="false" />
  		<c:out value="${requestScope.reqIncentiveLst}" escapeXml="false" />
	
  </script>
  
  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" 
		onLoad="OnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" style="background: transparent;">
   <form method="post" id="frmCredit" name="frmCredit" action="showAgentIncentive.action">
   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr><td><font class="Header">View / Adjust Agent Incentive</font></td></tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>	
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>			
		<tr>
			<td><%@ include file="../common/IncludeFormTop.jsp"%>Agent Incentive<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td>	
							<table width="100%" border="0" cellpadding="2" cellspacing="0">
								
								<tr>
									<td colspan="2">	
										<span id="spnIncentive"></span>
									</td>
								</tr>
								<tr>
									<td>
										<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onClick="closeClick()">
									</td>
									<td align="right">
										<u:hasPrivilege privilegeId="ta.maint.stock.update"><!-- FIXME -->
											<input name="btnSave"  type="button" class="Button" id="btnSave" value="Update" onClick="saveClick()">
										</u:hasPrivilege>
									</td>
								</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
			</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
	<script type="text/javascript">
  		var curentSchemes = "";
  		<c:out value="${requestScope.reqSelectedIncentives}" escapeXml="false" />
		
  	</script>
  	<script src="../js/airTravelAgents/AgentIncentiveValidate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	
	<input type="hidden" name="hdnMode" id="hdnMode" value="">
	<input type="hidden" name="hdnAgentCode" id="hdnAgentCode" value="">
	<input type="hidden" name="hdnSelctdIncentive" id="hdnSelctdIncentive" value="">	
  </form>
  </body>
</html>