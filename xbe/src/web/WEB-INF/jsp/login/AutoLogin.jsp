<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
String screenWidth = (String)session.getAttribute("sesScreenWidth");
String closeMe = System.getProperty("com.isa.thinair.xbe.close.indexpage");
String host = request.getHeader("host");//TODO remove.
String context = request.getContextPath();//TODO remove.
String targetUrl = (String) request.getAttribute("targetUrl");

if(screenWidth == null){
	screenWidth = request.getParameter("screenWidth");

	if(screenWidth != null){
		session.setAttribute("sesScreenWidth", screenWidth);
	}
}	

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="pragma" content="no-cache" >
<meta http-equiv="cache-control" content="no-cache" >
<meta http-equiv="expires" content="-1" >
<LINK rel="stylesheet" type="text/css" href="css/Style_no_cache.css">
<script src="js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</head>
<body oncontextmenu="return false" class="PageBackGround" onload="showFullScreen()">
<OBJECT ID="WB" WIDTH=0 HEIGHT=0 
    CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2">
</OBJECT>

<script type="text/javascript">
<!--
var screenWidth='<%=screenWidth%>';
var closeMe	   ='<%=closeMe%>';

function fullScreen(width){
	
	var url = '<%=targetUrl%>' + '/private/showXBEMain.action?menuLink="reservation"';
	try{
		switch (width){
			case 0:
				window.open(url,'winCC','fullscreen=yes,scrollbars=no');
				break;
			default :
				var height=(width==1024)?768:580;
				window.open(url,'winCC','toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='+width+',height='+height+',resizable=no,top=1,left=1');
				break;
		}		
	}catch (e){}
}

function showFullScreen(){
	if (window.screen.height==768){
		strId=0;
	}else if (window.screen.height>=768){
		strId=1024;
	}else{
		strId=800;
	}
	fullScreen(strId);
}
//-->
</script>
</body>
</html>
