<%@ page language="java"%>
<%@ include file="../common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="pragma" content="no-cache" >
<meta http-equiv="cache-control" content="no-cache" >
<meta http-equiv="expires" content="-1" >
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script language="javascript">
	<!--
	var target  = "<c:out value='${requestScope.targetUrl}' escapeXml='false' />";
	var uname   = "<c:out value='${requestScope.uname}' escapeXml='false' />";
	var pwd     = "<c:out value='${requestScope.pwd}' escapeXml='false' />";
	var carrier = "<c:out value='${requestScope.carrier}' escapeXml='false' />";
	var carrierMap = "<c:out value='${requestScope.carrierUrlMap}' escapeXml='false' />";
	var action  = "<c:out value='${requestScope.autologin_action}' escapeXml='false' />";

	function loadCarrier(){
		setField("uname", uname);
		setField("pwd", pwd);
		setField("carrier", carrier);
		setField("carrierUrlMap", carrierMap);
		setField("autologin_action", action);
		
		var objF = getFieldByID("switchCarrier");
	 	objF.action = target;
	 	objF.method = "post";
	 	objF.target = "main";
	 	objF.submit()
	 	return;
	}
		
	//-->
	</script>
</head>
<body onload="loadCarrier()">
	<form id="switchCarrier" name="switchCarrier" method="post" action="">
		<input type="hidden" name="uname" id="uname">
		<input type="hidden" name="pwd" id="pwd">
		<input type="hidden" name="carrier" id="carrier">
		<input type="hidden" name="carrierUrlMap" id="carrierUrlMap">
		<input type="hidden" name="autologin_action" id="autologin_action">
	</form>
</body>
</html>
