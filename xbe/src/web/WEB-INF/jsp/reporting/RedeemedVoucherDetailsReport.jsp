<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var arrError = new Array();
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
</script>
  </head>
  <body class="tabBGColor" oncontextmenu="return false" onbeforeunload="beforeUnload()">
  	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmRedeemedVoucherDetails" id="frmRedeemedVoucherDetails" action="showRedeemedVoucherDetailsReport.action" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Redeemed Voucher Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						
							<tr><td><font>&nbsp;</font></td></tr>
							
							
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td><table width="90%" border="0" cellpadding="0" cellspacing="2">
										
										<td width="17%"><font>Redeemed From</font></td>
										<td width="33%"><input width="75px" type="text" name="txtFromDate" id="txtFromDate" size="10" onBlur="settingValidation('txtFromDate')" onchange="dataChanged()"  maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>					
										<td width="17%" align="right"><font>To</font></td>
										<td width="33%"><input width="75px" type="text" name="txtToDate" id="txtToDate" maxlength="10"  onBlur="settingValidation('txtToDate')" onchange="dataChanged()" size="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>	
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="90%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="17%"><font>Voucher Number</font></td>
										<td width="33%"><input type="text" name="voucherNo" id="voucherNo"></td>
										<td width="50%"></td>															
										</tr>
										</table>
									</td>	
								</tr>
								
							<tr><td><font>&nbsp;</font></td></tr>
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>				
							<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td><table width="80%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML" onClick="chkClick()" class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF" onClick="chkClick()" class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL" onClick="chkClick()" class="noBorder"><font>EXCEL</font></td>
								<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV" onClick="chkClick()" class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								</td>									
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  
				
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">

	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
  <%@ include file="../common/IncludeLoadMsg.jsp"%>
  
	<script src="../js/reports/redeemedVoucherDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

</html>

