<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Agent Sales/Refund Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var stns = new Array();
	var agentsArr = new Array();
	var arrError = new Array();
	var displayAgencyMode = 0;
	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
</script>
</head>
<body class="tabBGColor" onunload="beforeUnload()"
	oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)'
	onkeydown="return Body_onKeyDown(event)"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')"
	scroll="no">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<form action="showAgentSalesRefundSummaryReport.action" method="post"
		id="frmAgentSalesRefundReport" name="frmAgentSalesRefundReport">

		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">

			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>

			<tr>

				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Agent Sales/Refund Report <%@ include
						file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="tblHead">
						<tr>
							<td><font class="fntSmall">&nbsp;</font></td>
						</tr>

						<tr>
							<td>
								<table width="95%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="98%">
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td width="23%"><font class="fntBold">Payment
															Date Range</font>
													</td>
													<td width="14%" align="left"><font>From </font> <input
														name="txtFromDate" type="text" id="txtFromDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtFromDate')" invalidText="true">
													</td>
													<td width="4%" align="left"><a href="javascript:void(0)"
														onclick="LoadCalendar(0,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a> <font class="mandatory"><b>*</b>
													</font></td>
													<td width="12%"><font>To </font> <input
														name="txtToDate" type="text" id="txtToDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtToDate')" invalidText="true">
													</td>
													<td width="4%" align="left"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(1,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a> <font class="mandatory"><b>*</b>
													</font></td>
													<td width="2%">&nbsp;</td>
													<td width="12%"><font>Report Type</font>
													</td>
													<td width="18%"><select name="selReportType" size="1"
														id="selReportType" style="width: 130px">
															<option value=""></option>
															<option value="ALL">All</option>
															<option value="SALE">Sales</option>
															<option value="REFUND">Refund</option>
															<option value="MODIFY">Modify</option>
													</select>
													<font class="mandatory"><b>*</b> </font></td>
													<td>&nbsp;</td>
												</tr>
											</table>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table></td>
						</tr>						
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<table width="95%" border="0" cellpadding="0" cellspacing="2">
										<tr>	
											<td width="25%" id="flightNumberDetail"><font>Flight Number </font>&nbsp;
											<input name="txtFlightNumber" type="text" id="txtFlightNumber" size="7" style="width:75px;"  class="UCase" maxlength="7">
											</td>
											<td width="25%" align="left"><font>Flight Type </font>
												&nbsp; 
												<select name="selFlightType" id="selFlightType" size="1" title=" Flight Type">
														<c:out value="${requestScope.reqFlightTypes}" escapeXml="false" />
													</select>
											</td>
											<td width="20%"><font>COS</font>
											&nbsp;
												<select id="selCOS" name="selCOS" size="1" tabindex="9" style="width: 125px">																	
													<option value="ALL" selected="selected">All</option>
													<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
												</select>
											</td>				
											<td width="25%">&nbsp; </td>
											<%-- 
											
											<td width="25%"><font> Journey Type </font>
												&nbsp; 
												<select id="selJourneyType" size="1" name="selJourneyType" style="width:75px">																										
															<c:out value="${requestScope.reqJourneyType}" escapeXml="false" />
																			
											</select>
											</td>
											
											--%>							
															
											<td >&nbsp;</td>
											<td width="5%">&nbsp;</td>								
										</tr>																								
									</table>
								</td>
							</tr>					
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>							
						
						<tr><td><table width="100%" border="0" cellpadding="0" cellspacing="2"><tr>				
					<tr><td width="7%"><font>Departure</font></td>
					
										<td width="10%"><select id="selDept" size="1" name="selDept" style="width:75px" tabindex="1" maxlength="10">
											<option value="">All</option>
											<c:out value="${requestScope.airportList}" escapeXml="false" />
									     </select></td>	
									     
					<td width="18%"><font>Arrival&nbsp;&nbsp;</font><select id="selArival" size="1" name="selArival" style="width:75px" tabindex="1" maxlength="10">											
											<option value="">All</option>
											<c:out value="${requestScope.airportList}" escapeXml="false" />
									     </select></td>
					<td width="12%">&nbsp;</td>									     
					<td width="12%">&nbsp;</td>
					<td width="12%">&nbsp;</td>
					<td width="12%">&nbsp;</td>								     
					
					</tr></table></td>
				</tr>			
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				
					
						<tr>
							<td><table width="100%" border="0" cellpadding="0" cellspacing="2"><tr>				
								<tr><td width="7%"><font>Fare Basis</font></td>					
										<td width="10%">
										<select id="selBasisCode" name="selBasisCode" size="1" tabindex="11" style="width:90px;">													
													<option value="All" selected="selected">All</option>
													<c:out value="${requestScope.reqBasisList}" escapeXml="false" />
												</select>
										</td>	
									     
					<td width="9%"><font>Paid Currency</font></td>
					<td width="12%">
					<select name="selPaidCurrency" size="1" id="selPaidCurrency" style="width:75px;">
													<option value="All" selected="selected">All</option>
													<c:out value="${requestScope.currencyList}" escapeXml="false" />
												</select>
												
					</td>
					
					<td width="7%"><font>Pax Type</font></td>
					<td width="12%">
					<select name="selPaxType" size="1" id="selPaxType" style="width:75px;">
													<option value="All" selected="selected">All</option>
													<c:out value="${requestScope.selPaxType}" escapeXml="false" />
												</select>
												
					</td>					
					
					<td width="12%">&nbsp;</td>
					<td width="12%">&nbsp;</td>
					<td width="12%">&nbsp;</td>
					
					</tr></table></td>
				</tr>		
				
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				
					
						<tr>
							<td><table width="100%" border="0" cellpadding="0" cellspacing="2"><tr>				
								<tr><td width="7%"><font>City</font></td>					
										<td width="10%">
										<select name="selAgentCity" size="1" id="selAgentCity" style="width:125px;">
													<option value="All" selected="selected">All</option>
													<c:out value="${requestScope.reqAgentCityList}" escapeXml="false" />
												</select>
										</td>	
										<td width="1%">&nbsp;</td>
										<td width="7%"><font>Country</font></td>					
										<td width="10%">
										<select name="selCountry" size="1" id="selCountry" style="width:125px;">
													<option value="All" selected="selected">All</option>
													<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
												</select>
										</td>						
										<td width="1%">&nbsp;</td>		
										<td width="7%"><font>Territory</font></td>					
										<td width="10%">
										<select name="selTerritory" size="1" id="selTerritory" style="width:125px;">
													<option value="All" selected="selected">All</option>
													<c:out value="${requestScope.reqterritory}" escapeXml="false" />
												</select>
										</td>				     
									    <td width="1%">&nbsp;</td> 
										<td width="9%"><font>Stations</font></td>
										<td width="12%">
										<select name="selStations" size="1" id="selStations" style="width:55px;">
																	<option value="All" selected="selected">All</option>
																	<c:out value="${requestScope.reqStationList}" escapeXml="false" />
																	</select>
										</td>
														
										
										<td width="8%">&nbsp;</td>
										<td width="10%">&nbsp;</td>
										<td>&nbsp;</td>
					
					</tr></table>
					</td>
				</tr>			
						
							
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<div id="divSelectAgents">
										<table width="50%" border="0" cellpadding="0" cellspacing="2">
											<tr>
												<td width="30%"><input type="radio" onclick="agentSelectionFilter()" name="radAgentSelOption" id="radAgentSelOption"	value="A"  class="noBorder" checked>&nbsp;&nbsp;<font>Select all agents</font></td>
												<td width="30%"><input type="radio" onclick="agentSelectionFilter()" name="radAgentSelOption" id="radAgentSelOption" value="S"  class="noBorder">&nbsp;&nbsp;<font>Select one agent</font></td>												
												<td>&nbsp;</td>
											</tr>									
										</table>
									</div>								
								</td>
							</tr>
								
							<tr>
								<td>
									<div id="divAgencies">
										<table width="50%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<font class="fntBold">Agencies</font>
														<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												<td>
													<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
												</td>
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"><font class="mandatory"><b>&nbsp;*</b></font> 
												</td>
											</tr>
											<tr>
												<td><font>&nbsp;</font></td>
												<td>
													<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
												</td>												
											</tr>
										</table>
								</div>
							 </td>
							</tr>							
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
											<tr>
												<td valign="top"><span id="spn1"></span></td>								
											</tr>
										</table>
									</div>								
								</td>
							</tr>
						
						<tr>
							<td>&nbsp;</td>
						</tr>


						<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><input type="button" id="btnClose" name="btnClose"
											value="Close" class="Button" onclick="closeClick()">
										</td>
										<td align="right">
										<div id="divSchedFrom"></div>
									<input name="btnSched" type="button" class="Button schdReptButton" id="btnSched" value="Schedule" onClick="viewClick(true)">
										<input type="button" id="btnView"
											name="btnView" value="View" class="Button"
											onclick="viewClick()">
										</td>
										<td width="10%">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				var agentWiseRouteSelection = eval('(' +'<c:out value="${requestScope.agentWiseRouteSelection}" escapeXml="false" />' + ')');
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divSelectAgents').style.display= 'block';					
					
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divSelectAgents').style.display= 'block';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
					document.getElementById('divSelectAgents').style.display= 'none';
				}	
				
		</script>
		<input type="hidden" name="hdnMode" id="hdnMode"> <input
			type="hidden" name="hdnReportView" id="hdnReportView"> <input
			type="hidden" name="hdnLive" id="hdnLive">
			<input type="hidden" name="hdnAgents" id="hdnAgents" value="">

	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<script
	src="../js/reports/AgentSalesRefundSummary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
	var objProgressCheck = setInterval("ClearProgressbar()", 300);
	function ClearProgressbar() {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
	}
</script>
</html>
