 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>On-Hold Passengers Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

  </head>
  <body onload="load()" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" 
  oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)">
  <div style="overflow: auto;" id="lengthScreen">
  	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmOnHoldPag" id="frmOnHoldPag" action="" method="post">
		<script type="text/javascript">
			var arrError = new Array();
			var displayAgencyMode = 0;
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
		</script>
		<table width="99%" border="0" cellpadding="0" cellspacing="0">				
								<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>On-Hold Passengers<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>							
							<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td><font class="fntBold">Release Date Range</font></td>				
									</tr>
									<tr>
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="17%" align="left"><font>From </font></td>
												<td width="30%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
												<td width="6%"><font>To </font></td>
												<td><input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									
									<tr>
										<td><font class="fntBold">Departure Date Range</font></td>				
									</tr>
									<tr>
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="17%" align="left"><font>From </font></td>
												<td width="30%"><input name="txtFromDepDate" type="text" id="txtFromDepDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDepDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
												<td width="6%"><font>To </font></td>
												<td><input name="txtToDepDate" type="text" id="txtToDepDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDepDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
											</tr>
										</table>
										</td>
									</tr>
									
									<tr>
										<td><font class="fntBold">Booked Date Range</font></td>				
									</tr>
									<tr>
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="17%" align="left"><font>From </font></td>
												<td width="30%"><input name="txtBookFromDate" type="text" id="txtBookFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookFromDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(4,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
												<td width="6%"><font>To </font></td>
												<td><input name="txtBookToDate" type="text" id="txtBookToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookToDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(5,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
											</tr>
										</table>
										</td>
									</tr>
																	
									<tr>
										<td>
											<table>
												<tr>
													<td> <font class="fntBold">Report Options </font> </td>
												</tr>
												<tr>													
													<td><input type="radio" id="radOption" name="radOption" value="DETAIL" checked="checked"> <font> Detail</font></td>
													<td><input type="radio" id="radOptionSummary" name="radOption" value="SUMMARY"> <font> Summary</font></td>
												</tr>
											</table>
										</td>
									</tr>
									
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>	
													<td><font>Flight Number </font>&nbsp;&nbsp;<input
													name="txtFlightNumber" type="text" id="txtFlightNumber" size="7" style="width:75px;"  class="UCase" maxlength="7"><font class="mandatory"><b></b></font></td>
												</tr>
												<tr><td><font class="fntSmall">&nbsp;</font></td></tr>												
											</table>
										</td>
									</tr>
									
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>	
													<td><font>Booking Status </font>&nbsp;&nbsp;
													<select id="selBookCurStatus" size="1" name="selBookCurStatus" style="width:200px">
														<option value=""></option>
														<option value="OHD">On hold</option>
														<option value="CNX_SCHD">Canceled by Scheduler</option>
														<option value="CNX_USER">Canceled by User</option>
														<option value="CNF">Confirmed</option>																			
													</select><font class="mandatory"><b>*</b></font>
													</td>
													<td>
													<input type="checkbox" name="chkExtOnhold" id="chkExtOnhold" class="noBorder" align="left" value="EXTOHD"><font>On Hold Extended</font>
													</td>
												</tr>
												<tr><td><font class="fntSmall">&nbsp;</font></td></tr>												
											</table>
										</td>
									</tr>
							
							<tr>
								<td>
									<div id="divAgencies">
										<table width="50%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<font class="fntBold">Agencies</font>
														<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												<td>
													<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
												</td>
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"> <font class="mandatory"><b>&nbsp;*</b></font> 
												</td>
											</tr>
											<tr>
												<td><font>&nbsp;</font></td>
												<td>
													<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
												</td>												
											</tr>
										</table>
								</div>
							 </td>
							</tr>	
							
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn1"></span></td>
												<td altgn="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td></tr>
												</table></td>
											</tr>
											<tr>
												<td align="right" valign="bottom" height="30px">
													<input name="btnLoadUsers" type="button" class="Button" id="btnLoadUsers" value=" Load Users " onClick="loadUsersClick()">
												</td>
											</tr>
										</table>
									</div>						
								</td>
							</tr>
							
							<tr>
								<td>
									<div id="divUsers">
										<table>
											<tr>
												<td>
													<font class="fntBold">Users</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn2"></span></td>
												<td altgn="left" valign="bottom"> </td></tr>
												</table></td>
											</tr>
											
										</table>
									</div>
								</td>
							</tr>	
									
									<tr>				
										<td><font class="fntBold">Output Option</font></td>							
									</tr>
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
													value="HTML" class="noBorder" checked><font>HTML</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
													value="PDF"  class="noBorder"><font>PDF</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
												<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="CSV"  class="noBorder"><font>CSV</font></td>
												</tr>									
											</table>
										</td>									
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
										</td>
										<td align="right">
											<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
									
										</td>
									</tr>
								</table>
								</td>
							</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>					  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
		<script type="text/javascript">								
				<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />						
		</script>
		 
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}	
				
		</script> 
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">			
		<input type="hidden" name="hdnUsers" id="hdnUsers" value="">	
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value=""> 
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
	</div>
  </body>
  
<script src="../js/reports/validateOnHold.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
   document.getElementById("lengthScreen").style.height = parent.document.getElementById('mainFrame').style.height 
 </script>
</html>
