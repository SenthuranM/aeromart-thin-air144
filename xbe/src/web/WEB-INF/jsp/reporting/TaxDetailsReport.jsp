<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Tax Details Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../css/Style_no_cache.css">
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
</script>
	
</head>


<body class="tabBGColor" onunload="beforeUnload()"
	oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)'
	onkeydown="return Body_onKeyDown(event)"
	onLoad="winOnLoad()"
	scroll="no">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->	
	<div style="height:657px;overflow: auto;" id="lengthScreen">
	<form action="showTaxDetailsReport.action" method="post"
		id="frmTaxDetailReport" name="frmTaxDetailReport">
	
		<table width="99%" align="center" border="0" cellpadding="0"
				cellspacing="0">
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Charge
						Details<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2"
							ID="tblHead">
							
							<tr>
								<td>
									<%@ include file="../common/IncludeFormTop.jsp"%>Search Charges<%@ include file="../common/IncludeFormHD.jsp"%>
										<table width="100%" border="0" cellpadding="1" cellspacing="0" ID="Table9">
											<tr style="height:1px">
												<td width="10%"><font>Charge Code</font></td>
												<td width="10%"><font>Group</font></td>
												<td width="10%"><font>Category</font></td>
												<td colspan="4"><font>Show Charge With Rates</font> </td>
												<td width = "10%"><font>Status</font> </td>
												<td width="8%"><font>From Date</font></td>
												<td width="2%"><font>&nbsp;</font></td>
												<td width="8%"><font>To Date</font></td>
												<td width="30%"><font>&nbsp;</font></td>
											</tr>
											<tr>
												<td>
													<select id="selChargeCode" name="selChargeCode" style="width:74px">
														<OPTION value="All">All</OPTION>
														<c:out value="${requestScope.reqChargeCodeList}" escapeXml="false" />
													</select>
												</td>
												<td>
													<select id="selGroup" name="selGroup" style="width:70px" >
														<OPTION value="All">All</OPTION>
														<c:out value="${requestScope.reqGroupList}" escapeXml="false" />
													</select>
												</td>
												<td>
													<select id="selCategory" name="selCategory" style="width:80px">
														<OPTION value="All">All</OPTION>
														<c:out value="${requestScope.reqCategoryList}" escapeXml="false" />
													</select>
												</td>	
												<td width = "2%">
													<input type="radio" id="radRateCat" name="radRateCat"   class="NoBorder" value="NoCharges" onClick="setWithoutCharges()" >
												</td>
												<td width = "9%"><font>Without rates</font></td>
												<td width = "2%">
													<input type="radio" id="radEffective" name="radRateCat"   class="NoBorder"  value="WithDateRange" onClick="setWithoutCharges()" ></td>
												<td width = "12%"><font>Effective During</font>
												</td>
												<td><select id="selStatus" name="selStatus" size="1">
													<option value="ACT">Active</option>
													<option value="INA">Inactive</option>
													<option value="All">All</option>
												</select></td>
												<td>
													<input type="text" id="txtDateFrom" disabled="disabled" name="txtDateFrom" style="width:75px;" maxlength="12" onblur="dateChk('txtDateFrom')"  invalidText="true">
												</td>
												<td width="10%"><a href="javascript:void(0)"
														onclick="LoadCalendar(0,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a></td>
												<td>
													<input type="text" id="txtDateTo" disabled="disabled" name="txtDateTo" style="width:75px;" maxlength="12" onblur="dateChk('txtDateTo')" invalidText="true">
												</td>
												<td width="15%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(1,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a></td>
											</tr>
										</table>
					  				<%@ include file="../common/IncludeFormBottom.jsp"%>
								
								</td>
							</tr>
							
							<tr>
								<td>&nbsp;</td>
							</tr>		
							
							<tr>
							<td>
								<table width="60%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><font>Order </font></td>

										<td><select name="selSortByOrder" id="selSortByOrder"
											size="1" style="width: 100px" title="Status">

												<option value="ASC">Ascending</option>
												<option value="DESC">Descending</option>
										</select>
										</td>
									<tr>
								</table></td>
						</tr>
						
						<tr>
							<td>&nbsp;</td>
						</tr>
						
						<tr>
							<td><font class="fntBold">Output Options</font>&nbsp;&nbsp;</td>
						</tr>
						

						<tr>
							<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="20%"><input type="radio"
											name="radReportOption" id="radReportOption" value="HTML"
											class="noBorder" checked><font>HTML</font>
										</td>
										<td width="20%"><input type="radio"
											name="radReportOption" id="radReportOptionPDF" value="PDF"
											class="noBorder"><font>PDF</font>
										</td>
										<td width="20%"><input type="radio"
											name="radReportOption" id="radReportOptionEXCEL"
											value="EXCEL" class="noBorder"><font>EXCEL</font>
										</td>
										<td width="40%"><input type="radio"
											name="radReportOption" id="radReportOptionCSV" value="CSV"
											class="noBorder"><font>CSV</font>
										</td>
									</tr>
								</table> <c:out value="${requestScope.rptFormatOption}"
									escapeXml="false" /></td>
						</tr>

						<tr>
							<td>&nbsp;</td>
						</tr>
							
							
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><input type="button" id="btnClose" name="btnClose"
												value="Close" class="Button" onclick="closeClick()">
											</td>
											<td align="right"><input type="button" id="btnView"
												name="btnView" value="View" class="Button"
												onclick="viewClick()"></td>
											<td width="10%">&nbsp;</td>
										</tr>
									</table></td>
							</tr>
					   </table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
		</table>
		
		<input type="hidden" name="hdnMode" id="hdnMode"> <input
			type="hidden" name="hdnReportView" id="hdnReportView"> <input
			type="hidden" name="hdnLive" id="hdnLive"> <input
			type="hidden" name="hdnSegments" id="hdnSegments" value=""> <input
			type="hidden" name="hdnAgents" id="hdnAgents" value=""> <input
			type="hidden" name="hdnRptType" id="hdnRptType" value="">
		
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
	</div>
</body>  	
<script
	src="../js/reports/TaxDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
   document.getElementById("lengthScreen").style.height = parent.document.getElementById('mainFrame').style.height
</script>	
<script type="text/javascript">
	var objProgressCheck = setInterval("ClearProgressbar()", 300);
	function ClearProgressbar() {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
	}
</script>	
</html>