<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">
		var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		
</script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmLoyaltyPointsReport" id="frmLoyaltyPointsReport" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="96%" align="left" border="0" cellpadding="0" cellspacing="0">				
				<tr><td><font class="Header">Loyalty Points</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Loyalty Points<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr>
					<td width="22%" align="left"><font>Loyalty Account number</font></td>
					<td align="left" colspan="4"><input name="txtAccountNo" type="text" id="txtAccountNo" size="16" style="width:100px;" maxlength="16" ></td>
					
				</tr>
				<tr><td colspan="5"><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>				
					<td colspan="5"><font class="fntBold">Report Option</font></td>
											
				</tr>
				<tr>			
					<td colspan="5" width="2%" valign="top"><input type="radio" name="radCreditSumm" id="radCreditSumm"	value="CRE_SUM"  class="noBorder"  onClick="disableUsageSummary(true);disableCreditSummary(false);disableAccExpDays(true)" checked="true"><font>&nbsp;Credit Summary</font></td>
				</tr>				
				<tr>
					<td colspan="5">
					<table width="100%">
					<tr>
					<td width="5%">
				<td width="3%" ><input type="radio" name="radAccountType" id="radAccountType"	value="All"  class="noBorder"  onclick="disableAccExpDays(true)" checked="true"></td>
					<td width="18%" ><font>&nbsp;All Loyalty Accounts</font></td>
					<td width="3%" ><input type="radio" name="radAccountType" id="radAccountType"	value="CREDIT"  class="noBorder"  onclick="disableAccExpDays(false)"></td>
					
                	<td width="24%"><font>&nbsp;Loyalty Accounts with 
                  Credit</font></td>
					<td width="20%" ><font> with Credit Expiring within</font></td>
					<td id="tdTxtAccExp" width="27%"><input name="txtExpDays" type="text" id="txtExpDays" size="10" style="width:80px;" maxlength="3" > </td>
						</tr>
					</table>
					</td>
				</tr>	
				<tr>
					<td colspan="5"><font class="fntSmall">&nbsp;</font></td>
				</tr>	
				<tr>			
					<td colspan="5" width="2%" valign="top"><input type="radio" name="radCreditSumm" id="radCreditSumm"	value="USE_SUM"  class="noBorder"  onClick="disableUsageSummary(false);disableCreditSummary(true)"><font>&nbsp;Usage Summary</font></td>
				</tr>
				<tr>
				<td colspan="5">
					<table width="100%">
					<tr>
					<td width="5%"></td>
					<td width="15%"><font>Usage From Date</font></td>				
          			
					<td width="11%"><input name="txtUsegeFrom" type="text" id="txtUsegeFrom" size="10" style="width:80px;" onBlur="dateChk('txtUsegeFrom')" maxlength="10" invalidText="true">
					</td>
					<td width="14%" valign="bottom" id="tdCalFrom"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="From Date"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a>
                  <font class="mandatory"><b>&nbsp;*</b></font> </td>
					<td width="12%" ><font>Usage To Date</font></td>
					<td width="12%"><input name="txtUsegeTo" type="text" id="txtUsegeTo" size="10" style="width:80px;" onBlur="dateChk('txtUsegeTo')" maxlength="10" invalidText="true">
                  </td>
				  <td width="33%" valign="bottom" id="tdCalTo"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="TO Date"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a>
                  <font class="mandatory"><b>&nbsp;*</b></font> </td>
				  		</tr>
				  	</table>
				  </td>
				</tr>

				<tr>
					<td colspan="5"><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>				
					<td colspan="5"><font class="fntBold">Output Option</font></td>											
				</tr>
				<tr>
					<td colspan="5"><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
					<td width="18%" id="tdHtml"><input type="radio" name="radReportOption" id="radHtmlReportOption"
						value="HTML" class="noBorder"  checked><font>&nbsp;HTML</font></td>
					<td width="18%" id="tdPdf"><input type="radio" name="radReportOption" id="radPdfReportOption" 
						value="PDF" class="noBorder"><font>&nbsp;PDF</font></td>
					<td width="18%" id="tdExcel"><input type="radio" name="radReportOption" id="radExlReportOption"
						value="EXCEL" class="noBorder"><font>&nbsp;EXCEL</font></td>					
					<td width="46%" id="tdCsv"><input type="radio" name="radReportOption" id="radCsvReportOption" 
						value="CSV" class="noBorder"><font>&nbsp;CSV</font></td>
				</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
							</td>							
							<tr>
								<td colspan="2">
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
								</td>
								<td align="right" colspan="3">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnReportView" id="hdnReportView" value="">

		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
	</form>
	
	
  </body>
  
  <script src="../js/reports/LoyaltyPointsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  <script type="text/javascript">
   //<!--
//   var objProgressCheck = setInterval("ClearProgressbar()", 300);
//   function ClearProgressbar(){
//      clearTimeout(objProgressCheck);
//      HideProgress();
//   }
   //-->
  </script>
</html>
