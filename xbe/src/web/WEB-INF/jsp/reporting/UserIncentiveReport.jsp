<%@ page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>User Incentive Details Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var arrError = new Array();
	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />

	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
<!--For Calendar-->
</head>
<body onload="winOnLoad()" onbeforeunload="beforeUnload()"
	oncontextmenu="return false">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<form action="showBookedSSRDetailsReport.action" method="post"
		id="frmUserIncentiveRpt" name="frmUserIncentiveRpt">

		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr><td><font class="Header">User Incentive Details Report</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>User Incentive Report<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="tblHead">
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="2" cellspacing="5">
									<tr>
										<td>
											<table width="80%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="25%"><font class="fntBold">Sales
															Date</font>
													</td>

													<td width="20%" align="left"><font>From </font> <input
														name="txtFromDate" type="text" id="txtFromDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtFromDate')" invalidText="true">
													</td>
													<td width="20%"><a href="javascript:void(0)"
														onclick="LoadCalendar(0,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font></td>
													<td width="18%"><font>To </font> <input
														name="txtToDate" type="text" id="txtToDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtToDate')" invalidText="true">
													</td>
													<td width="17%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(1,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font></td>
												</tr>
                                                <tr>
                                                    <td width="25%"><font class="fntBold">Departure Date</font>
                                                    </td>

                                                    <td width="20%" align="left"><font>From </font> <input
                                                            name="txtDepFromDate" type="text" id="txtDepFromDate" size="10"
                                                            style="width: 75px;" maxlength="10"
                                                            onblur="dateChk('txtDepFromDate')" invalidText="true">
                                                    </td>
                                                    <td width="20%"><a href="javascript:void(0)"
                                                                       onclick="LoadCalendar(4,event); return false;"
                                                                       title="Date From"><img
                                                            SRC="../images/Calendar_no_cache.gif" border="0"
                                                            border="0"> </a><font class="mandatory"><b>*</b></td>
                                                    <td width="18%"><font>To </font> <input
                                                            name="txtDepToDate" type="text" id="txtDepToDate" size="10"
                                                            style="width: 75px;" maxlength="10"
                                                            onblur="dateChk('txtDepToDate')" invalidText="true">
                                                    </td>
                                                    <td width="17%"><a href="javascript:void(0)"
                                                                       onclick="LoadCalendarTo(5,event); return false;"
                                                                       title="Date From"><img
                                                            SRC="../images/Calendar_no_cache.gif" border="0"
                                                            border="0"> </a><font class="mandatory"><b>*</b></td>
                                                </tr>
											</table></td>
									</tr>
									<tr>
										<td>
											<table width="80%" border="0" cellpadding="0"
												cellspacing="2">
												<tr>
													<td width="30%"><font class="fntBold">User ID </font>
													</td>
													<td width="70%"><input type="text" style="width: 75px;"
														name="txtUserID" id="txtUserID" maxlength="30"
														onchange="" size="10"></td>
												</tr>
												<tr></tr>
												<tr>
													<td width="30%"><font class="fntBold">FFP Number</font></td>
													<td width="70%"><input type="text" style="width: 75px;"
														name="txtFfpNumber" id="txtFfpNumber" maxlength="15"
														onchange="" size="10"></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td><font class="fntBold">O &amp; D</font></td>
													<td><font>Departure</font></td>
													<td><select name="selDeparture" size="1"
														id="selDeparture" style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Arrival</font></td>
													<td><select name="selArrival" size="1" id="selArrival"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>&nbsp;</font></td>
													<td colspan="2" rowspan="3"><select id="selSegment"
														name="selSegment" multiple size="1"
														style="width: 200px; height: 85px">
													</select></td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 1</font></td>
													<td><select name="selVia1" size="1" id="selVia1"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 2</font></td>
													<td><select name="selVia2" size="1" id="selVia2"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="addToList()" title="Add to list"><img
															src="../images/AA115_no_cache.gif" border="0"> </a></td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 3</font></td>
													<td><select name="selVia3" size="1" id="selVia3"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 4</font></td>
													<td><select name="selVia4" size="1" id="selVia4"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="removeFromList()" title="Remove from list"><img
															src="../images/AA114_no_cache.gif" border="0"> </a></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="80%" border="0" cellpadding="0"
												cellspacing="2">
												<tr>
													<td width="30%"><font class="fntBold">No of Segments Sold </font>
													</td>
													<td width="18%">
														<select id="selSegQuantifier" name="selSegQuantifier" size="1"
																style="width: 100px;">
															<option value=""></option>
															<option value="LT">Less Than &gt;</option>
															<option value="EQ">Equal =</option>
															<option value="GT">Greater Than &lt;</option>
														</select></td>
													<td width="52%" align="left">
														<input type="text" style="width: 100px;"
														name="txtSegmentCount" id="txtSegmentCount" maxlength="9"
														onchange="" size="10">
													</td>
												</tr>
												<!-- 
												<tr>
													<td width="30%"><font class="fntBold">Booking Class</font></td>
													<td width="70%" colspan="2">
														<select id="selBookingClass" name="selBookingClass" size="1" style="width: 100px;">
														<option value=""></option>
															<c:out value="${requestScope.reqBookingClassCodes}"
																escapeXml="false" />
														</select>
													</td>
												</tr>
												 -->
												<tr>
													<td width="30%"><font class="fntBold">Flight Type</font></td>
													<td width="70%" colspan="2">
														<select id="selFlightType" name="selFlightType" size="1" style="width: 100px;">
														<option value="ALL">ALL</option>
															<c:out value="${requestScope.reqFlightTypes}"
																escapeXml="false" />
														</select>
													</td>
												</tr>

                                                <%--
                                                <tr>
                                                    <td width='48%' valign="bottom" height="30" >
                                                        <font class="fntBold">Booking Classes</font>
                                                    </td>
                                                </tr>
                                                --%>

											</table>
										</td>
									</tr>
									<tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <font class="fntBold">Booking Classes</font>
                                                    </td>
                                                    <td>
                                                        <font class="fntBold">Passenger Status</font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <span id="spnBC" class="FormBackGround"></span>
                                                    </td>
                                                    <td valign="top">
                                                        <span id="spnPS" class="FormBackGround"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                        <%--
										<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td colspan ="3" rowspan="7" valign="top">
													<span id="spnBC" class="FormBackGround"></span>
												</td>
										  	</tr>
										</table>
										</td>
                                        --%>

									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td></td>
						</tr>

						<tr>
							<td><font class="fntBold">Output Option</font></td>
						</tr>
						<tr>
							<td><table width="100%" border="0" cellpadding="0"
									cellspacing="2">
									<tr>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="HTML"
											class="noBorder" checked tabindex="9"><font>HTML</font>
										</td>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="PDF"
											class="noBorder"><font>PDF</font></td>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="EXCEL"
											class="noBorder"><font>EXCEL</font></td>
										<td><input type="radio" name="radReportOption"
											id="radReportOption" value="CSV" class="noBorder"><font>CSV</font>
										</td>
									</tr>
								</table> <c:out value="${requestScope.rptFormatOption}"
									escapeXml="false" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td width=90%><input type="button" id="btnClose"
								name="btnClose" value="Close" class="Button"
								onclick="closeClick()" tabindex="10"></td>
							<td width=10% align="right"><input type="button"
								id="btnView" name="btnView" value="View" class="Button"
								onclick="viewClick()" tabindex="11"></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode"> 
		<input type="hidden" name="hdnReportView" id="hdnReportView"> 
		<input type="hidden" name="fromDate" id="fromDate"> 
		<input type="hidden" name="toDate" id="toDate"> 
		<input type="hidden" name="origin" id="origin">  
		<input type="hidden" name="hdnSegments" id="hdnSegments">
		<input type="hidden" name="hdnBookingClasses" id="hdnBookingClasses">
		<input type="hidden" name="hdnPaxStatus" id="hdnPaxStatus">
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<%@ include file="../common/IncludeLoadMsg.jsp"%>
<script
	src="../js/reports/UserIncentiveDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	
<script type="text/javascript">
	<c:out value="${requestScope.reqBookingCodesList}" escapeXml="false" />
	
	if(bcls) {
		bcls.height = '125px';
		bcls.width = '100px';
		bcls.drawListBox();
	}


    <c:out value="${requestScope.reqPaxStatusList}" escapeXml="false" />

    if(listBoxPs) {
        listBoxPs.height = '125px';
        listBoxPs.width = '100px';
        listBoxPs.drawListBox();
    }

</script>	
 	
</html>