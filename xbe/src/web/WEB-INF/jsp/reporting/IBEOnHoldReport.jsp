 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>On-Hold Passengers Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/reports/validateIBEOnHold.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body onload="load()" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" 
  oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)">
  <div style="overflow: auto;" id="lengthScreen">
  	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmOnHoldPag" id="frmOnHoldPag" action="" method="post">
		<script type="text/javascript">
			var arrError = new Array();
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
		</script>
		<table width="99%" border="0" cellpadding="0" cellspacing="0">				
								<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>On-Hold Passengers<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>							
					  		<tr>
								<td>
									<table>
										<tr>
											<td> <font class="fntBold">Releasing In </font> </td>
										</tr>
										<tr>													
											<td><input type="radio" id="radOption" name="radOption" value="DATE_RANGE" checked="checked"> <font>Date Range</font></td>
											<td><input type="radio" id="radOptionSummary" name="radOption" value="HOURS"> <font>Hours</font></td>
										</tr>
									</table>
								</td>
							</tr>
							
							<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr id="releaseDateRange1">
										<td><font class="fntBold">Release Date Range</font></td>				
									</tr>
									<tr>
										<table>
											<tr>
												<td width="20%"><input type="radio" name="radReleaseTimeOption"
													id="radReleaseTimeOptionLOCAL" value="LOCAL" class="noBorder"><font>Local</font>
												</td>
												<td width="5"></td>
												<td width="20%"><input type="radio" name="radReleaseTimeOption"
													id="radReleaseTimeOptionZULU" value="ZULU" class="noBorder"
													checked><font>Zulu</font></td>
											</tr>
										</table>
									</tr>
									<tr id="releaseDateRange2">
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="20%" align="left"><font>From </font></td>
												<td width="30%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
												<td width="6%"><font>To </font></td>
												<td><input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr id="releaseInHours1" style="DISPLAY: none;">
										<td><font class="fntBold">Releasing within next </font></td>				
									</tr>
									<tr id="releaseInHours2" style="DISPLAY: none;">
										<td><input id="txtReleaseHours" name="txtReleaseHours"></input>hours</td>	
									</tr>
									<tr><td>&nbsp;</td></tr>
																		
									<tr>
											<td>
												<table>
													<tr>
														<td colspan="3"><font class="fntBold">Departure Date Range</font></td>													
													<tr>
													<tr>
														<td><input type="radio" name="radTimeOption"
															id="radTimeOptionLOCAL" value="LOCAL" class="noBorder"><font>Local</font>
														</td>
														<td width="5">&nbsp;</td>
														<td><input type="radio" name="radTimeOption"
															id="radTimeOptionZULU" value="ZULU" class="noBorder"
															checked><font>Zulu</font></td>
													</tr>
												</table>
											</td>
									</tr>												
												
									<tr>
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="20%" align="left"><font>From </font></td>
												<td width="30%"><input name="txtFromDepDate" type="text" id="txtFromDepDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDepDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
												<td width="6%"><font>To </font></td>
												<td><input name="txtToDepDate" type="text" id="txtToDepDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDepDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td><font class="fntBold">Booked Date Range</font></td>				
									</tr>
									<tr>
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="20%" align="left"><font>From </font></td>
												<td width="30%"><input name="txtBookFromDate" type="text" id="txtBookFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookFromDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(4,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
												<td width="6%"><font>To </font></td>
												<td><input name="txtBookToDate" type="text" id="txtBookToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookToDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(5,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
											</tr>
										</table>
										</td>
									</tr>
																	
									
									
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>	
													<td width="20%" height="40" ><font>Flight Number </font></td>
													<td><input name="txtFlightNumber" type="text" id="txtFlightNumber" size="7" style="width:75px;"  class="UCase" maxlength="7"></td>
													
												</tr>																			
											</table>
										</td>
									</tr>
									
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>	
													<td width="20%"><font>Booking Status </font></td>
													<td>
														<select id="selBookCurStatus" size="1"
															name="selBookCurStatus" style="width: 200px">
																<option value=""></option>
																<option value="ALL">All</option>
																<option value="OHD">On hold</option>
																<option value="CNX_SCHD">Canceled by Scheduler</option>
																<option value="CNX_USER">Canceled by User</option>
																<option value="CNF">Confirmed</option>
														</select><font class="mandatory"><b>*</b></font>
													</td>													
												</tr>																			
											</table>
											
											
											
									
										</td>
									</tr>
									
									<tr>				
										<td><font class="fntBold">Output Option</font></td>							
									</tr>
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
													value="HTML" class="noBorder" checked><font>HTML</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
													value="PDF"  class="noBorder"><font>PDF</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
												<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="CSV"  class="noBorder"><font>CSV</font></td>
												</tr>									
											</table>
										</td>									
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
										</td>
										<td align="right" width=20% >
											<div id="divSchedFrom"></div>
											<input name="btnSched" type="button" class="Button" id="btnSched"
												value="Schedule" onClick="viewIBEOnHoldClick(true)"></td>
										<td align="right">
											<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewIBEOnHoldClick(false)">
									
										</td>
									</tr>
								</table>
								</td>
							</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>					  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
		<script type="text/javascript">								
				<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />						
		</script>
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">			
		<input type="hidden" name="hdnUsers" id="hdnUsers" value="">	
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
	</div>
  </body>
  
<script type="text/javascript">
   document.getElementById("lengthScreen").style.height = parent.document.getElementById('mainFrame').style.height 
 </script>
</html>
