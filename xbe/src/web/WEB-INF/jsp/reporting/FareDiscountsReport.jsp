<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

	<script type="text/javascript">
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		var arrError = new Array();
		var displayAgencyMode = 0;
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />	
		<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
	</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return showContextMenu()" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
  <%@ include file="../common/IncludeTop.jsp"%>
  <div style="overflow: auto;" id="lengthScreen">
  	<form name="frmTax" id="frmTax" action="" method="post">
  	
  			 <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr><td><font class="Header">Fare Discount Report</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
					
					<!-- START -->				
				<%@ include file="../common/IncludeFormTop.jsp"%>Fare Discounts<%@ include file="../common/IncludeFormHD.jsp"%>
				  	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
						<tr>
							<td><font class="fntBold">Charge Date Range</font></td>				
						</tr>
						<tr>
							<td>
								<table width="62%" border="0" cellpadding="0" cellspacing="2">	
									<tr>
										<td width="25%" align="left"><font>From &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtFromDate')" invalidText="true"></td>
										<td width="15%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
										<td width="20%"><font>To </font><input	name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtToDate')" invalidText="true"></td>
										<td width="35%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
									</tr>
								</table>
							</td>
							<td>
							</td>
						</tr>	
						
							<tr>
								<td>
									<div id="divAgencies">
										<table width="50%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<font class="fntBold">Agencies</font>
														<select id="selAgencies" size="1" name="selAgencies" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
											
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"> <font class="mandatory"><b>&nbsp;*</b></font> 
												</td>
											</tr>
											<tr>
												<td><font>&nbsp;</font></td>
																								
											</tr>
										</table>
								</div>
							 </td>
							</tr>	
							
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn1"></span></td>
												<td altgn="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td></tr>
												</table></td>
											</tr>
											<tr>
												<td align="right" valign="bottom" height="30px">
													<input name="btnLoadUsers" type="button" class="Button" id="btnLoadUsers" value=" Load Users " onClick="loadUsersClick()">
												</td>
											</tr>
										</table>
									</div>						
								</td>
							</tr>
							
							<tr>
								<td>
									<div id="divUsers">
										<table>
											<tr>
												<td>
													<font class="fntBold">Users</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn2"></span></td>
												<td altgn="left" valign="bottom"> </td></tr>
												</table></td>
											</tr>
											
										</table>
									</div>
								</td>
							</tr>		
							<tr>
							<td><font class="fntBold">Get Discounts with Overriding Discounts for Fares</font>
								<input type="radio" id="radWithOverriddenDiscount" name="radDiscount" class="noBorder" value="OVERRIDE_DISCOUNT">
							</td>
							</tr>	
							<tr>
							<td><font class="fntBold">Get Discounts without Overriding Discounts for Fares</font>
								<input type="radio" id="radWithoutOverriddenDiscount" name="radDiscount" class="noBorder" value="NORMAL" checked="checked">
							</td>
							</tr>											
						<tr>
							<td width="60%"><font class="fntBold">Output Option</font></td>
						</tr>
						<tr>
							<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder" checked><font>HTML</font></td>
										<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font>PDF</font></td>
										<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
										<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><font>CSV</font></td>
									</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
							</td>							
						</tr>
						<tr>
							<td  width = "90%">
								<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
							</td>
							<td align="right">
								<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
							</td>
						</tr>
					</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>			
					
					<!--   END -->
				  	</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
						
			<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />				
				<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}				
		</script>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">		
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">		
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">			
		<input type="hidden" name="hdnUsers" id="hdnUsers" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value=""> 
	</form>
	</div>
  </body>  
  <script src="../js/reports/ValidateFareDiscountsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   document.getElementById("lengthScreen").style.height = parent.document.getElementById('mainFrame').style.height
  </script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
   //-->
  </script>
</html>
