 <%-- 
	 @Author 	: 	Dhanushka
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

  </head>
  <body onload="load()" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
  	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmPaxContactDetails" id="frmPaxContactDetails" action="" method="post">
		<script type="text/javascript">
			var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
			var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			var arrError = new Array();
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			var displayAgencyMode = 0;
			<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
		</script>
		<table width="99%" border="0" cellpadding="0" cellspacing="0">				
								<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Passenger Contact Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>							
							<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td><font class="fntBold">Booking Date Range</font></td>				
									</tr>
									<tr>
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="17%" align="left"><font>From Date</font></td>
												<td width="30%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')">
													<a href="javascript:void(0)" onclick="LoadBookingCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
												<td width="10%"><font>To Date</font></td>
												<td><input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')">
													<a href="javascript:void(0)" onclick="LoadBookingCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td><font class="fntBold">Flight Date Range</font></td>				
									</tr>
									<tr><td><table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="17%" align="left"><font>From Date</font></td>
												<td width="30%"><input name="txtFlightFromDate" type="text" id="txtFlightFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFlightFromDate')">
													<a href="javascript:void(0)" onclick="LoadFlightCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a></td>
												<td width="10%"><font>To Date</font></td>
												<td><input name="txtFlightToDate" type="text" id="txtFlightToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFlightToDate')">
													<a href="javascript:void(0)" onclick="LoadFlightCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a></td>
											</tr>
										</table></td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>	
													<td><font>Flight Number </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
													name="txtFlightNumber" type="text" id="txtFlightNumber" size="7" style="width:75px;"  class="UCase" maxlength="7"></td>
												</tr>
												<tr></tr>
												<tr>
													<td>
														<font><label>Run report in Local time : </label></font>
														<input id="chkLocalTime" type="checkbox" name="chkLocalTime" value="true"/>
													</td>
												</tr>											
											</table>
										</td>
									</tr>
									<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<div id="divAgencies">
										<table>
											<tr>
												<td>
													<font>Agencies</font>
													<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												<td>
													<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
												</td>
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
												</td>
											</tr>
											<tr>
												<td><font>&nbsp;</font></td>
												<td>
													<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
												</td>												
											</tr>
										</table>
									</div>
								</td>
							</tr>							
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn1"></span></td>
												<td align="left" valign="bottom"></td></tr>
												</table></td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							
									<tr>				
										<td><font class="fntBold">Output Option</font></td>							
									</tr>
									<tr>
										<td>
											<font>Print Details of </font>
											<select id="selPrintDetailsOf" size="1" name="selPrintDetailsOf" onClick="clickAgencies()" onChange="changeAgencies()">
												<option value="All" selected>All Users</option>
												<option value="REGISTERED">Registered Users</option>
											</select><font class="mandatory"><b>*</b></font>
										</td>									
									</tr>
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
													value="HTML" class="noBorder" checked><font>HTML</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
													value="PDF"  class="noBorder"><font>PDF</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
												<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="CSV"  class="noBorder"><font>CSV</font></td>
												</tr>									
											</table>
										</td>									
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
										</td>
										<td align="right">
											<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
									
										</td>
									</tr>
								</table>
								</td>
							</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>					  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />				
				
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}				
		</script>	

		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
<script src="../js/reports/validatePaxContactDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
