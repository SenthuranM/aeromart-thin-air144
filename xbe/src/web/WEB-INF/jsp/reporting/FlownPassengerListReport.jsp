<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Passenger List</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<%@ include file='../v2/common/inc_PgHD.jsp' %>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
<script type="text/javascript">
		var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		var selectBox;
		var displayAgencyMode = 0;
		<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
		var repShowpay = "<c:out value="${requestScope.reqSowPay}" escapeXml="false" />";	
</script>
</head>
  <body style="overflow-X:hidden; overflow-Y:auto;" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" 
  			onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmFlownPassengerList" id="frmFlownPassengerList" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr><td><font class="Header">Passenger List Report</font></td></tr>
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Passenger List<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
				  			<tr>
				  				<td><font class="fntSmall">&nbsp;</font></td></tr>							
							<tr>
							<tr>
								<td>
									<table width="95%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="98%">
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td width="10%"><font class="fntBold">Flight Date Range</font></td>
													<td width="5%" align="left"><font>From </font> 
													<input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtFromDate')" invalidText="true">
													</td>
													<td width="5%"><a href="javascript:void(0)" onClick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a>
											<font class="mandatory"> &nbsp;* </font></td>
													<td width="5%"><font>To </font> <input	name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtToDate')" invalidText="true">
													</td>
													<td width="15%"><a href="javascript:void(0)" onClick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a>
											<font class="mandatory"> &nbsp;* </font>
													</td>
												</tr>
												<tr>
													<td width="10%"><font class="fntBold">Booking Date Range</font></td>
													<td width="5%" align="left"><font>From </font> 
													<input name="txtBookedFromDate" type="text" id="txtBookedFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtBookedFromDate')" invalidText="true">
													</td>
													<td width="5%"><a href="javascript:void(0)" onClick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a>
													</td>
													<td width="5%"><font>To </font> <input	name="txtBookedToDate" type="text" id="txtBookedToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtBookedToDate')" invalidText="true">
													</td>
													<td width="15%"><a href="javascript:void(0)" onClick="LoadCalendar(3,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a>
													</td>
												</tr>
												
											</table></td>										
										<td>&nbsp;</td>
									</tr>
									</table>
								</td>								
							</tr>
							
							<tr>
								<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>	
													<td><font>Flight Number </font>&nbsp;&nbsp;<input
													name="txtFlightNumber" type="text" id="txtFlightNumber" size="7" style="width:75px;"  class="UCase" maxlength="7"><font class="mandatory"><b></b></font></td>													
												</tr>																								
											</table>
								</td>
							</tr>
							<tr>
								<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td><font>Status</font>&nbsp;&nbsp;
													<select id="selPaxSegStatus" size="1" name="selPaxSegStatus" style="width:76px">														
														<option value="All">All</option>
														<option value="F">Flown</option>
														<option value="UF">Unflown</option>																			
													</select>
												</td>
												</tr>																								
											</table>
								</td>
							</tr>
													
						<!-- O & D -->
						<tr>
							<td>
								<table width="98%" border="0" cellpadding="0" cellspacing="2"
									ID="Table9">
									<tr>
										<td><font class="fntBold">&nbsp;&nbsp;&nbsp;O
												&amp; D</font>
										</td>
										<td><font>Departure</font>
										</td>
										<td><select name="selDeparture" size="1"
											id="selDeparture" style="width: 55px;">			
												<option value=""></option>											
												<c:out value="${requestScope.airportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;&nbsp;Arrival</font>
										</td>
										<td><select name="selArrival" size="1" id="selArrival"
											style="width: 55px;">	
												<option value=""></option>													
												<c:out value="${requestScope.airportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;</font>
										</td>
										<td colspan="2" rowspan="3"><select id="selSegment"
											name="selSegment" multiple size="1"
											style="width: 200px; height: 85px">
										</select>										
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td><font>Via 1</font>
										</td>
										<td><select name="selVia1" size="1" id="selVia1"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.airportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;&nbsp;Via 2</font>
										</td>
										<td><select name="selVia2" size="1" id="selVia2"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.airportList}"
													escapeXml="false" />
										</select></td>
										<td align="center"><a href="javascript:void(0)"
											onclick="addToList()" title="Add to list"><img
												src="../images/AA115_no_cache.gif" border="0"> </a>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td><font>Via 3</font>
										</td>
										<td><select name="selVia3" size="1" id="selVia3"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.airportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;&nbsp;Via 4</font>
										</td>
										<td><select name="selVia4" size="1" id="selVia4"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.airportList}"
													escapeXml="false" />
										</select></td>
										<td align="center"><a href="javascript:void(0)"
											onclick="removeFromList()" title="Remove from list"><img
												src="../images/AA114_no_cache.gif" border="0"> </a>
										</td>
									</tr>

								</table></td>
						</tr>
							<tr>
									<td><font class="fntSmall">&nbsp;</font></td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<div id="divAgencies">
										<table>
											<tr>
												<td><font>Agencies</font>
													<select id="selAgencies" size="1" name="selAgencies" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqFlownPassengerList}" escapeXml="false" />					
													</select>
												</td>
												<td>													
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
													<font class="mandatory"> &nbsp;* </font>
												</td>
												<td>
													<input name="chkIbePax" id="chkIbePax" type="checkbox"/>
													<font>Select only passengers from IBE</font>
												</td>												
											</tr>				
										</table>
									</div>
								</td>
							</tr>							
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
												
											<tr>
												<td>
												<table width="62%" border="0" cellpadding="0" cellspacing="2">	
												<tr>
													<td valign="top" width="2%"><span id="spn1"></span></td>
													<td align="left" valign="bottom"> </td></tr>
												</table>
												</td>
												</tr>
										</table>
									</div>
								</td>
							</tr>			
							
							<tr><td width="60%">
									<font class="fntBold">Output Option</font>
								</td></tr>

							<tr><td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />
							</td>							
							<tr>
								<td>
									<table width="99%" border="0" cellpadding="0" cellspacing="0"><tr>
										<td width="78%" align="left">
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onClick="closeClick()">
										</td>
										<td width="9%" align="right">
											<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
										</td>
										<td width="11%" align="right">
											<input name="btnViewSummary" style="width:90px" type="button" class="Button" id="btnViewSummary" value="View Summary" onClick="viewSummaryClick()">
										</td>
									</tr></table>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<script type="text/javascript">				
				<c:out value="${requestScope.reqPaymentType}" escapeXml="false" />			
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}
		</script>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">		
		<input type="hidden" name="hdnSegments" id="hdnSegments" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">					 	
	</form>
	
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  </body> 
  <script src="../js/reports/FlownPassengerListReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
 </html>