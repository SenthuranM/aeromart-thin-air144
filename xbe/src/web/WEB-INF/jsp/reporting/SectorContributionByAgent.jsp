<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Segment Contribution by Agents</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">
	var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
</script>
</head>
<body onload="pageLoadSC()" onbeforeunload="beforeUnload()" oncontextmenu="return false">
<div id='POS' name='POS' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500; visibility:hidden;'></div>
	<div id='Country' name='Country' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500; visibility:hidden;'></div>
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<div style="overflow: auto;" id="lengthScreen">
<form action="" method="post" id="frmSegmentContributionByAgents" name="frmSegmentContributionByAgents">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTop.jsp"%>Contribution by Agents<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblHead">			
							<tr>
								<td><font class="fntBold">Booking Date Range</font></td>				
							</tr>
							<tr>
								<td>
									<table width="60%" border="0" cellpadding="0" cellspacing="2">	
										<tr>
											<td width="30%"><font>From </font><input
												name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" tabindex="1" maxlength="10" onblur="dateChk('txtFromDate')"></td>
											<td width="20%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
											<td width="24%"><font>To </font><input
												name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" tabindex="2" maxlength="10" onblur="dateChk('txtToDate')"></td>
											<td width="29%"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
										</tr>
									</table>
								</td>
							</tr>							
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td><font class="fntBold">Flight Date Range</font></td>				
							</tr>
							<tr>
								<td>
									<table width="60%" border="0" cellpadding="0" cellspacing="2">	
										<tr>
											<td width="30%"><font>From </font><input
												name="txtFlightFromDate" type="text" id="txtFlightFromDate" size="10" style="width:75px;" tabindex="1" maxlength="10" onblur="dateChk('txtFlightFromDate')"></td>
											<td width="20%"><a href="javascript:void(0)" onclick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
											<td width="24%"><font>To </font><input
												name="txtFlightToDate" type="text" id="txtFlightToDate" size="10" style="width:75px;" tabindex="2" maxlength="10" onblur="dateChk('txtFlightToDate')"></td>
											<td width="29%"><a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
										</tr>
									</table>
								</td>
							</tr>	
							<tr><td>&nbsp;</td></tr>		 							
							<tr>
								<td><font class="fntBold">Select Segments</font></td>				
							</tr>								
								<tr>
								<td>
								  <table width = "100%">
								  
								  <tr>
								    <td width="50%">
										<table border="0" width="60%">
											<tr>
												<td width="17%"><font>Departure</font></td>
												<td >
										  			<select id="selDepature" name="selDepature" size="1" style="width:60px;">
														<option value="All">All</option>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td width="16%"><font>Arrival</font></td>
												<td>
													<select id="selArrival" name="selArrival" size="1" style="width:60px;">
														<option value="All">All</option>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
											</tr>
											<tr>
												<td width="17%"><font>Via 1</font></td>
												<td colspan="2">
													<select id="selVia1" name="selVia1" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td rowspan="4">
													<select id="segmentsSelected" size="3" name="selSelected" style="width:150;height:70" multiple>
													</select>
												</td>
											</tr>
											<tr>
												<td width="17%"><font>Via 2</font></td>
												<td ><select id="selVia2" name="selVia2" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td align="center"><input type="button" id="btnAddSeg" name="btnAddSeg" class="Button" value=">" style="width:40px;height:17px" onClick="btnAddSegment_click()"> </td>
												<td></td>
											</tr>

											<tr>
												<td width="17%"><font>Via 3</font></td>
												<td ><select id="selVia3" name="selVia3" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td align="center"><input type="button" id="btnRemSeg" name="btnRemSeg" class="Button" value="<" style="width:40px;height:17px" onClick="btnRemoveSegment_click()"> </td>
												<td></td>
											</tr>
											<tr>
												<td width="17%"><font>Via 4</font></td>
												<td colspan="3"><select id="selVia4" name="selVia4" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td><font class="mandatory">&nbsp;*</font></td>														
											</tr>
										</table>
										</td>
										
									<td width = "50%">										
											<font  class="fntBold" style="text-align: left;">&nbsp;&nbsp;OR&nbsp; </font>		
											<font>&nbsp;&nbsp;Flight No&nbsp;</font> 
								 			<input name="txtFlightNo" type="text" id="txtFlightNo" size="10" style="width:75px;" tabindex="8" maxlength="12" onBlur="changeCase(this)">		     							 	   
							 		</td>									
									</tr>
							     </table>	
							    </td>
							    </tr>													     
							
							<tr><td>&nbsp;</td></tr>			 
							
							<tr>
								<td><font class="fntBold">Operation Type</font></td>				
							</tr>														
							<tr>
							  <td>
							     <font>&nbsp;&nbsp;&nbsp;&nbsp;Operation Type&nbsp;  </font><select name="selOperationType" id="selOperationType" size="1" tabindex="11" style="width:120px" title="Operation Type">																					
											<option value="All">All</option>
											<c:out value="${requestScope.sesOperationType}" escapeXml="false" />
											</select> 
							    </td>
							</tr>
							
							<tr><td>&nbsp;</td></tr>			 

							<tr>
								<td><input type="radio" name="radSegmentFlight" id="radCountry"	value="Country" onClick="chkClick(this.id)" tabindex="6" class="noBorder" checked>
									<font class="fntBold">Country Contribution</font>
								</td>					
							</tr>	
							 <tr>
							    <td>	
							    <table width =%60 >		
							        <tr>	 		
		 							 	<td width =%40 >
		 									<input id="btnCountries" type="button" value= "Select Countries" class="Button" onClick="return showCountryMultiSelect(true);" style="height:20px ;width:160px; " />											
		 							    </td>	
		 							    <td width =%60 >
											 <select id="selectedCntries" size="3" name="selectedCntries" style="width:150;height:70" multiple ="multiple">
											</select>
										</td>
									</tr>	
								</table>	
								</td>
														
	 						</tr> 							
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td><input type="radio" name="radSegmentFlight" id="radPOS" value="POS" onClick="chkClick(this.id)" tabindex="12" class="noBorder" >
									<font class="fntBold">POS Contribution</font>
								</td>
															
							</tr>	
							<tr>	
							  <td>	
								    <table width =%60 >		
								        <tr>						 		
			 							 	 <td>
			 									<input id="btnPOS" type="button" value= "Select POS" class="Button" onClick="return showPOSMultiSelect(true);" style="height:20px" />											
			 							    </td>	
			 							     <td>
												 <select id="selectedPOS" size="3" name="selectedPOS" style="width:150;height:70" multiple ="multiple">
												</select>
											 </td>										 
									 	</tr>	
									</table>	
								</td>					
		 					</tr>
																			 					
							<tr><td>&nbsp;</td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr>				
								<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td>
									<table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
												value="HTML" class="noBorder" checked="checked"><font>HTML</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption" 
												value="PDF" class="noBorder"><font>PDF</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
												value="EXCEL" class="noBorder"><font>EXCEL</font></td>
											<td width="40%"><input type="radio" name="radReportOption" id="radReportOption"
												value="CSV" class="noBorder"><font>CSV</font></td>
										</tr>									
									</table>
									<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td>&nbsp;</td></tr>	
							<tr>				
								<td width="90%"><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"> </td>
								<td width="10%" align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
							</tr>		
						</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%></td>
					</tr>
					<tr>
						<td><font class="fntSmall">&nbsp;</font></td>
					</tr>
				</table>
				<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
				<input type="hidden" name="hdnCountries" id="hdnCountry"/> 
				<input type="hidden" name="hdnPOSs" id="hdnPOSs"/>
				<input type="hidden" name="hdnSegments" id="hdnSegments"/>
				<input type="hidden" name="hdnLive" id="hdnLive" value="">
				
			</form>

<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</div>
</body>
<script src="../js/reports/SegmentContributionByAgents.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   document.getElementById("lengthScreen").style.height = parent.document.getElementById('mainFrame').style.height
 </script>
<script type="text/javascript">

<c:out value="${requestScope.reqActStationList}" escapeXml="false"></c:out>		
<c:out value="${requestScope.reqCountryList}" escapeXml="false"></c:out>	
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
 //   if (typeof(frames["grdCurrencies"]) == "object"){
 //    if (frames["grdCurrencies"].objDG.loaded){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   //  }
   // }
   }
    
   //-->
</script>
</html>