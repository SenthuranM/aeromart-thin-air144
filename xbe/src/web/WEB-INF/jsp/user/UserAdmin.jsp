<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="-1"/>
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/user/UserValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<!--<script src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>-->
	<style>
		#spnError div.errorBoxHttps{
			padding: 2px 9px 0px 8px;
			background: #F3F376;
			-moz-border-radius:4px;
		}
	</style>
	<script type="text/javascript">
		var arrError = new Array();			
		<c:out value="${requestScope.reqUserData}" escapeXml="false" />
		var totalNoOfRecords =<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
		var searchData = "<c:out value="${requestScope.reqSearchData}" escapeXml="false" />";
		var initialrec = "<c:out value="${requestScope.recordNo}" escapeXml="false" />";
		var svdGird = "<c:out value="${requestScope.reqGridData}" escapeXml="false" />";
		var isServCh = false;
		var isServChEditable = false;
		var setMyIDCarrier = false;
		var isFFPMaintainable = false;
		<c:out value="${requestScope.reqServChJS}" escapeXml="false" />
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		<c:out value="${requestScope.reqFormData}" escapeXml="false" />
		<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
		var request=new Array();
		<c:out value='${requestScope.reqRequest}' escapeXml='false' /> 
		var noOfCarriers = "";
		noOfCarriers= "<c:out value='${requestScope.reqNoofCarriers}' escapeXml='false' />"; 
		var adminInt = 0;
		var normalInt = 0;
		<c:out value='${requestScope.reqUseAddNormalStat}' escapeXml='false' />
		<c:out value='${requestScope.reqUseAddAdminStat}' escapeXml='false' />
		var carAgent = "<c:out value="${requestScope.reqCarrAgent}" escapeXml="false" />";
		<c:out value='${requestScope.reqDryUserEnable}' escapeXml='false' />
		var defaultAirlineCode = "<c:out value="${requestScope.reqDefaultAirlineCode}" escapeXml="false" />";
		var isRemoteUser = 	"<c:out value="${requestScope.reqIsRemoteUser}" escapeXml="false" />";
		var isAllowAirlineCarrierPrefix = 	"<c:out value="${requestScope.reqIsAllowCarrierPrefix}" escapeXml="false" />";
		var enableGSAV2 = ("true" == "<c:out value="${requestScope.reqGSAStructureV2Enabled}" escapeXml="false" />");
		var userAgentType = "<c:out value="${requestScope.reqUserAgentType}" escapeXml="false" />";	


	</script>
  </head>
   <body style="background: transparent;" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')"> 
  <!--<body style="background: transparent;"> -->
   <div style="width: 918px;margin: 0 auto">
   <form name="frmUser" id="frmUser" method="post" action="saveUser.action">
			<!-- Page Background Top page -->
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Users						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td width="13%"><font>Login ID </font></td>
								<td width="6%" id="tdAirlineCode" style="display: none;">
									<select title="Airline Code" name="selAirlineCode"  id="selAirlineCode" size="1" style="display: none">	
										<option VALUE="ALL">ALL</option>
										<c:out value="${requestScope.reqAirlineList}" escapeXml="false" />								 
								    </select>
							    </td>
								<td width="10%">
									<input  name="txtLoginIdSearch" type="text" id="txtLoginIdSearch" size="10" maxlength="20" onKeyUp="validateString('txtLoginIdSearch')" onKeyPress="validateString('txtLoginIdSearch')"/>
								</td>
								<td width="10%"><font>Status</font></td>
								<td width="25%">
									<select title="Status" name="selSearchStatus" id="selSearchStatus">
										<option VALUE="ACT">Active</option>
										<option VALUE="INA">In-Active</option>
										<option VALUE="ALL">All</option>		
									</select> 
								</td>
								<td width="10%"><font>Station </font></td>
								<td width="20%">
									<select name="selAirportSearch"  id="selAirportSearch" size="1" style="width: 118px">	
										<option VALUE="ALL">All</option>								 
										<c:out value="${requestScope.airportList}" escapeXml="false" />									  
								    </select>
							    </td>						    
								<td width="20%" colspan="2">&nbsp;</td>
							</tr>
							<tr>
							 	<!--  Merge dry sell user search here -->
								<td>
									<font>Display Name </font>
								</td>
								<td colspan="2">
									<input  name="txtLastNameSearch" type="text" id="txtLastNameSearch" size="19" maxlength="40"  onkeyup="validateStringWhiteSpace('txtLastNameSearch')" 
									onkeypress="validateStringWhiteSpace('txtLastNameSearch')" />
								</td>
								<td></td>									
							    <td>
									<input type="button" name="btnSearchLoadAgents" id="btnSearchLoadAgents" class="Button" value="Load Agents" style="width:90px;" onclick="searchLoadAgentClick()">
									<font>TA/ GSA </font>
								</td>
								<td colspan="3">
									<select title="Travel Agent / General Sales Agent" name="selTravelAgentSearch"  id="selTravelAgentSearch" size="1"  style="width:200px;">
										<option VALUE="ALL">All</option>								 
										<c:out value="${requestScope.reqAgentList}" escapeXml="false" />									  
								    </select>
							    </td>							    
								<td align="right" colspan="3">
								  <input name="btnSearch" type="button" class="Button" id="btnSearch" onclick="searchUser()" value="Search" /> 
							</td>
							</tr>
	 				    </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Users<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">							
							<tr>
								<td>
									  <iframe src="showFile!userAdminGrid.action" id="grdUsers" name="grdUsers" style="height:176px;" width="100%" frameborder="no" scrolling="no"></iframe> 
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.security.user.add">
										<input type="button"  name="btnAdd" id="btnAdd" class="Button" value="Add" onClick="addUser()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.security.user.edit">
										<input type="button"  name="btnEdit" id="btnEdit" class="Button" value="Edit" onClick="editUser()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.security.user.ffp">
										<input type="button"  name="btnManageFFP" id="btnManageFFP" class="Button" value="Manage FFP" onClick="enableFFP()" style="width:110px;">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.security.user.delete">
										<input type="button"  name="btnDelete" id="btnDelete" class="Button" value="Delete" onClick="deleteUser()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.security.user.reset.anyusers.passwd,sys.security.user.reset.rptusers.passwd,sys.security.user.reset.ownusers.passwd">
										<input type="button"  name="btnSetPass" id="btnSetPass" class="Button" value="password Reset" onClick="resetPassword()" style="width:110px;">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.security.user.addnote">
										<input type="button" name="btnUnAdd" id="btnUnAdd" class="Button" value="Add Note" onclick="addUserNote()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.security.user">
										<input type="button" name="btnUnView" id="btnUnView" class="Button" value="View Note" onclick="viewUserNote()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>
					Add/Modify Users &nbsp;&nbsp;<%@ include file="../common/IncludeFormHD.jsp"%>		
					<div id="tabs">
						<ul>
							<li><a href="#tabs-1">User Master Data</a></li>
							<li><a href="#tabs-2">Role Assignment Information</a></li>
							<li><span id="spnError">&nbsp;</span></li>
						</ul>
						<div id="tabs-1" style="padding: 0.5em 1.4em">
			               <table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">  
								  <tr>
								  <td colspan="2">
								  <table width="100%" border="0" cellpadding="1" cellspacing="2" align="center">	
					                  <tr>
					                    <td width="13%"><font>Login ID</font></td>
					                    <td width="20%"> <span style="display:none;" id="spnCtlAirlineCode"> <span id="spnAirlineCode"> </span>&nbsp; </span><input name="txtLoginId" type="text" id="txtLoginId" size="16" maxlength="20" onKeyUp="validateString('txtLoginId')" onKeyPress="validateString('txtLoginId')" onchange="clickChange()" tabindex="10"/> 
					                    <font class="mandatory">&nbsp;*</font> 
					                    </td>
					                    <td width="10%">&nbsp;</td>
					                    <td width="12%">
					                    	<u:hasPrivilege privilegeId="sys.security.user.carriers"><font>Carrier Code</font></u:hasPrivilege>
					                    </td>
					                    <td rowspan="4" width="12%">
											<u:hasPrivilege privilegeId="sys.security.user.carriers">
											<select multiple style="width:60px; height:76px;" class="ListBox" id="selCarrierCode" disabled="disabled" tabindex="19">
											<c:out value="${requestScope.reqCarrierList}" escapeXml="false" />		
											</select> <font class="mandatory"> &nbsp;* </font> 
											</u:hasPrivilege>
										</td>
										<td width="15%"><font>Active</font></td>
										<td width="18%">
						                   	<input name="chkStatus" type="checkbox" id="chkStatus" maxlength="1" size="1" value="Active" onchange="clickChange()" tabindex="11" />
					                    </td>
				                   </tr>
			    	               <tr>
				        	           	<td valign="top"><font>Password</font></td>				                   
										<td colspan="4">
											<table border="0" cellpadding="0" cellspacing="0">
												<tr><td>
													<input name="pwdPassword" type="password" id="pwdPassword" size="20" tabindex="12" maxlength="20" onChange="changePassword()" onmouseover="showDetails()" onMouseOut='nd();'onblur='nd();' style="display:block;"/> 
												</td>
												<td>
													<input  name="txtPlnPassword" type="text" id="txtPlnPassword" size="20" maxlength="20" style="display:none;" tabindex="13"/></td><td><font class="mandatory">&nbsp;&nbsp;*&nbsp;&nbsp;</font></td>
												<td>
													<u:hasPrivilege privilegeId="sys.security.user.viewpassword"> <input type="button" id="btnGetPassword" class="Button" value="Password" onclick="getPassword()" tabindex="14" style="height: 18px"/></u:hasPrivilege>
												</td>
												</tr>
											</table>
										 </td>
										 <td><font>Create Users</font></td>
										 <td align="left">
											<input name="chkAdmin" type="checkbox" id="chkAdmin" maxlength="1" size="1" value="ADMIN" onchange="clickChange()" onclick="adminClick()" tabindex="21"/>
										</td>
									</tr>
									<tr>
				        	           	<td valign="top"><font>Verify Password</font></td>				                   
										<td colspan="4"><input name="pwdVeriPassword" type="password" id="pwdVeriPassword" size="20" maxlength="20" onChange="changeVerifyPassword()" tabindex="13"/> <font class="mandatory">&nbsp;&nbsp;*&nbsp;&nbsp;</font> </td>
										<td colspan="2" rowspan="3">
												<div id="dvAdmin">
											 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
											 			<tr>
											 				<td width="40%">&nbsp;</td>
											 				<td align="center" width="20%"><font>&nbsp;Any</font></td>
											 				<td align="center" width="20%"><font>&nbsp;Own</font></td>
											 				<td align="center" width="20%"><font>Reporting</font></td>
											 			</tr>
											 			<tr>
											 				<td><div id="dvNormTXT"><font>Create Agent Users</font></div></td>
											 				<td align="center">
																<div id="dvNormAny">
													 				<input name="chkOUAny" type="checkbox" id="chkOUAny" maxlength="1" size="1" value="ANY" onclick="NormAnyClick()"  onchange="clickChange()" />
													 			</div>
															</td>
											 				<td align="center">
											 					<div id="dvNormOwner">
													 				<input name="chkOUOwner" type="checkbox" id="chkOUOwner" maxlength="1" size="1" value="OWNER" onchange="clickChange()" />
													 			</div>
											 				</td>
											 				<td align="center">
											 					<div id="dvNormRep">
													 				<input name="chkOUReporting" type="checkbox" id="chkOUReporting" maxlength="1" size="1" value="REPORTING" onchange="clickChange()" />
													 			</div>
											 				</td>
											 			</tr>
											 			<tr>
											 				<td><font>Create Admin Users</font></td>
											 				<td align="center">
											 					<div id="dvAdmAny">
													 				<input name="chkAUAny" type="checkbox" id="chkAUAny" maxlength="1" size="1" value="ANY" onclick="adminAnyClick()" onchange="clickChange()"/>
													 			</div>
											 				</td>
											 				<td align="center">
																<div id="dvAdmOwner">
													 				<input name="chkAUOwner" type="checkbox" id="chkAUOwner" maxlength="1" size="1" value="OWNER" onchange="clickChange()"/>
													 			</div>
															</td>
											 				<td align="center">
										 						<div id="dvAdmARep">
													 				<input name="chkAUReporting" type="checkbox" id="chkAUReporting" maxlength="1" size="1" value="REPORTING" onchange="clickChange()"/>
													 			</div>
											 				</td>
											 			</tr>
											 			</table>
									 				</div>
											</td> 
										</tr>									
					                  	 <tr>
					                    	<td><font>Display Name</font></td>
					                   		 <td colspan="2"><input name="txtFirstName" type="text" id="txtFirstName" size="33" tabindex="15" maxlength="70" onKeyUp="validateStringWhiteSpace('txtFirstName')" onKeyPress="validateStringWhiteSpace('txtFirstName')" onChange="clickChange()"> <font class="mandatory"> &nbsp;* </font> 
					                    	</td>
						                  </tr>								
					                  	<tr>
					                    <td><font>Email ID</font></td>
					                     <td colspan="2"><input name="txtEmail" type="text" id="txtEmail" maxlength="100" size="33" onChange="clickChange()" tabindex="16"> <font class="mandatory"> &nbsp;* </font></td>
				               			 <td><u:hasPrivilege privilegeId="sys.security.user.carriers"><font>Default Carrier</font></u:hasPrivilege></td>
				                    	 <td colspan="3"><u:hasPrivilege privilegeId="sys.security.user.carriers">
				                    		<select title="Default Carrier" name="selDefCarrier" id="selDefCarrier" onChange="clickChange()" style="width: 60px;" tabindex="20">
												<option VALUE=""></option>
				                        		<c:out value="${requestScope.reqCarrierList}" escapeXml="false" />
				                    		</select>
											<font class="mandatory"> &nbsp;* </font>
										</u:hasPrivilege>									
										</td>				               			
					                   </tr>
					                   <u:hasPrivilege privilegeId="sys.security.role.servicechannel.view">
					                   <tr>
										<td><font>Service Channel</font></td>
					                    <td colspan="6">
					                    <select title="Service Channel" name="selServiceChannel" id="selServiceChannel" onChange="clickChange();adminClick()" tabindex="18">										
					                        	<option VALUE="SELECT">Select</option>
					                        <c:out value="${requestScope.reqServiceChannels}" escapeXml="false" />
					                    </select>
										<font class="mandatory"> &nbsp;* </font>									
										</td>
					                  </tr>
					                  </u:hasPrivilege>
					                  <tr>
										<td valign="top"><font>Theme</font></td>
						                    <td colspan="2" valign="top">
						                    <select title="Theme" name="selTheme" id="selTheme" onChange="clickChange();adminClick()" tabindex="19" style="width: 148px">											
						                        <c:out value="${requestScope.reqUserThemes}" escapeXml="false" />
						                    </select>																		
											</td>
										<td><font>FFP Number</font></td>
										<td colspan="3"><input name="txtFFPNumber" type="text" id="txtFFPNumber" maxlength="15" size="33" tabindex="15" maxlength="15"  onkeyup="validateAlphaNumeric(this)" onkeypress="validateAlphaNumeric(this)"></td>
					                  </tr>
									  <tr>
									  	<td><font>Station </font></td>
										<td>
											<select name="selAirport"  id="selAirport" size="1" style="width: 118px">	
												<option VALUE="ALL">All</option>								 
												<c:out value="${requestScope.airportList}" escapeXml="false" />									  
										    </select>
									    </td>		
									    <td>
									    	<input type="button" name="btnLoadAgents" id="btnLoadAgents" style="width:90px;" class="Button" value="Load Agents" onclick="formLoadAgentClick()">
									    </td>
										<td><font>Travel Agent/ GSA</font></td>
						                <td colspan="3">
						                    <select title="Travel Agent / General Sales Agent" name="selTravelAgent" id="selTravelAgent" onChange="clickChange();adminClick()" style="width:250px;" tabindex="17">
													<option VALUE="SELECT">Select</option>
					                        <c:out value="${requestScope.reqAgentList}" escapeXml="false" />
					                    </select>
					                    <font class="fntBold"><span id="spnAgent"></span></font>
										<font class="mandatory"> &nbsp;* </font>									
										</td>
										<td>
											<font>Max Adults Allowed</font>
										</td>
									    <td>
  								 	        <input type="text" id="maxAdultAllowed" name="maxAdultAllowed" maxlength="3" onkeyup="" onkeypress="positiveInt(this)" size="3"/>
  							   	        </td>
					                  </tr>  
					                  <tr>
					                  <td><u:hasPrivilege privilegeId="sys.security.user.myidtravel"><font>MyID Carrier</font>
					                   </u:hasPrivilege>
					                  </td>
					                  <td><u:hasPrivilege privilegeId="sys.security.user.myidtravel">
					                  <input name="txtMyIDCarrier" type="text" id="txtMyIDCarrier" size="2" tabindex="20" maxlength="2"  onkeyup="validateAlphaNumeric(this)" onkeypress="validateAlphaNumeric(this)">
					                   </u:hasPrivilege>
					                  </td>
					                  </tr>
								 </table>
							</td>
							</tr></table>
							</div>
				            <div id="tabs-2">
				            	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							 	<tr> 
									<td valign="top" width="100%" colspan="2">
										<u:hasPrivilege privilegeId="sys.security.user.roles">
											<span id="spn1" class="FormBackGround"></span>
										</u:hasPrivilege>
									</td>
								 </tr>
								 <tr>
								 	<td width="35%">
								 		<div id="dvNormTXT"><font>Apply Role Changes to other users of same Agent</font></div>
								 	</td>
								 	<td>
								 		<input name="chkRoleChange" type="checkbox" id="chkRoleChange" maxlength="1" size="1" value="ROLECHANGE" onchange="clickChange()" tabindex="21"/>
								 	</td>
								 </tr>
				                </table>
	            			            
				            </div>
			          		</div>
			          		
				           <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				           <tr><td style='height: 4px'></td></tr>						
							<tr>
			                    <td style="height:15px;" valign="bottom">
									
									<input type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()"/>				                    
				                    <input name="btnReset" type="button" class="Button" id="btnReset"  onclick="resetClick()" value="Reset"/>
		
				                </td>
				                <td align="right" valign="bottom">
									<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveData()" value="Save"/>
								</td>
			                  </tr>
			                  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			                  <tr>
								<td style="height:30px" colspan="3">
									
								</td>
								<td>
		  						<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
									<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
								</div>
							</td>
							</tr>

			           </table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>  
			    </td>
		    </tr>			
			</table>

	<script type="text/javascript">
		<c:out value="${requestScope.reqUserRoleHtmlData}" escapeXml="false" />
	</script>
		<input type="hidden" name="hdnVersion" id="hdnVersion"/> 
		<input type="hidden" name="hdnPassword" id="hdnPassword"/> 
		<input type="hidden" name="hdnMode" id="hdnMode"/> 
		<input type="hidden" name="hdnRoleValues" id="hdnRoleValues"/>	
		<input type="hidden" name="hdnRecNo" id="hdnRecNo"/> 	
		<input type="hidden" name="hdnStatus" id="hdnStatus"/> 
		<input type="hidden" name="hdnAction" id="hdnAction"/>
		<input type="hidden" name="hdnEditable" id="hdnEditable"/>
		<input type="hidden" name="hdnCOSCapacities" id="hdnCOSCapacities"/>
		<input type="hidden" name="hdnSearchData" id="hdnSearchData"/>
		<input type="hidden" name="hdnSearchAirline" id="hdnSearchAirline"/> 
		<input type="hidden" name="hdnGridData" id="hdnGridData"/> 
		<input type="hidden" name="hdnCarrierCode" id="hdnCarrierCode"/>
		<input type="hidden" name="hdnDryUserEnable" id="hdnDryUserEnable"/>
	</form>	
  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){
  		if (typeof(frames["grdUsers"]) == "object"){
  			if (typeof(frames["grdUsers"].objDG) == "object"){
		  		if (frames["grdUsers"].objDG.loaded){
		  			clearTimeout(objProgressCheck);
		  			HideProgress();
		  		}
		  	}
	  	}
  	}
  	$(function() {
		$( "#tabs" ).tabs();
		$("#lstRoles, #lstAssignedRoles").css("width","335px");
		$("#lstRoles, #lstAssignedRoles").css("height","150px");
	});
  	 
  	//-->
  </script>
</div>
</body>
</html>
