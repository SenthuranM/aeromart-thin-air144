<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
		var request=new Array();
		<c:out value='${requestScope.reqRequest}' escapeXml='false' />		
	</script>
  </head>
  <body onLoad="winOnLoad()">
  	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<table>
	  <tr>
	    <td>
	      <div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
			<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
		</div>
	    </td>
	  </tr>  	
  	</table> 
  	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page --> 
  </body>
  <script type="text/javascript">
	function winOnLoad(){
		window.location.replace(request['secureUrl']);
		setVisible("divLoadMsg", true);	
	}
  </script>
 </html>

