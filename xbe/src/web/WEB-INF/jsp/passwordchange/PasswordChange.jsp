<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1"> 
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.js"></script>
	<script src="../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/jquery.pstrength.js"></script>
	<script language="javascript">
		var isReset = "<c:out value='${requestScope.pwdResetMsg}' escapeXml='false' />"; 
		var isPasswordExpired = "<c:out value='${requestScope.userPwdExpired}' escapeXml='false' />";
		var languageData = "<c:out value='${requestScope.languageData}' escapeXml='false' />"; 
	</script>
	<script src="../js/passwordchange/PasswordChange.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  </head>
  <body onLoad="onLoad()" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' oncontextmenu="return showContextMenu()" >  
	  <%@ include file="../common/IncludeTop.jsp"%>
	  <div id="divBookingTabs">
		<table width="99%" cellpadding="2" cellspacing="0" border="0" align="center" ID="passwordchange">
		<tr>
			<td><font class="Header" i18n_key="lbl_changepassword_title">Change Password</font></td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>			
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%><font i18n_key="lbl_changepassword_title">Password Change</font><%@ include file="../common/IncludeFormHD.jsp"%>
					<br>
					<form id="formChangePassword" name="formChangePassword" method="post" action="changePassword.action">
						<table width="100%" border="0" cellpadding="1" cellspacing="2" align="left">
							<tr>
								<td width="100%" colspan="3">
									<div id="pageInfo" name="pageInfo" valign="center"><img src="../images/icons/info_no_cache.gif">&nbsp;&nbsp;<font><label id="lblInfoMgs"></label></font><br><br></div>
								</td>							 
							</tr>
							<tr>
								<td width="23%">
									<font i18n_key="lbl_changepassword_userid">User ID </font>
								</td>
								<td align="left" width="60%">
									<input type="text" id="txtUserId"  name="txtUserId" size="20" onChange="pageOnChange()" DISABLED><font class="mandatory"> *</font>
								</td>
								<td rowspan="4"><font></font></td>
							</tr>
							<tr>
 								<td valign="top">

								</td>
								 <td align=right valign="top">							
								 </td>								
 							</tr>
							<tr>
								<td>
									<font i18n_key="lbl_changepassword_oldpassword">Old Password </font>
								</td>
								<td align="left">
									<input type="password" id="pwdOld" name="pwdOld" size="20" onChange="pageOnChange()"><font class="mandatory"> *</font>
								</td>
								 
							</tr>
							<tr>
 								<td valign="top">

								</td>
								 <td align=right valign="top">							
							</td>								
							</tr>
							<tr>
								<td valign="top">
									<font i18n_key="lbl_changepassword_newpassword">New Password </font>
								</td>
								<td align="left" valign="top">
									<input type="password" class="password" id="pwdNew" name="pwdNew" size="20" onChange="pageOnChange()">

								</td>
								
							</tr>
							<tr>

							<tr>
								<td>
									<font i18n_key="lbl_changepassword_confirmpassword">Confirm Password </font>
								</td>
								<td align="left">
									<input type="password" id="pwdConfirm"  name="pwdConfirm" size="20" onChange="pageOnChange()"><font class="mandatory"> *</font>
								</td>
								 
							</tr>
							<tr>
 							<td valign="top">

							</td>
							 <td align=right valign="top">							
							 </td>								
							 </tr>
							<tr>
								<td style="height:20px" colspan="3">
									<font class="mandatory fntBold"><span id="spnError">&nbsp;</span></font>
								</td>
							</tr>
							<tr>
								<td align="left"><input type="button" id="btnReset" value="Reset" class="Button" name="resetFields" onclick="resetMe()" i18n_key="btn_changepassword_reset"></td>
									
								<td colspan="2" align=right>
									<input type="button" id="btnSave" value="Save" class="Button" onclick="changePassword()" name="save" i18n_key="btn_changepassword_save">
									
								</td>
							</tr>
						</table>
					</form>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
	</table>
  </div>	
	<script language="javascript">
	<!--
	
		var request=new Array();
		var arrError=new Array();
		var reqMessage='<c:out value='${requestScope.reqMessage}' escapeXml='false' />';
		<c:out value='${requestScope.reqRequest}' escapeXml='false' /> 
		<c:out value='${requestScope.reqClientErrors}' escapeXml='false' /> 
	//-->
	</script>
	  <%@ include file="../common/IncludeBottomNoprogress.jsp"%>
</body>
</html>
