<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.DATE,1);
	dStopDate = cal.getTime(); //Next Month Date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%>
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">	
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/PrivilegeConstants.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		 
  </head>
  
  <body onkeypress='return Body_onKeyPress(event)'  class="PageBackGround" oncontextmenu="return true" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="<u:scroll />"
  		onload="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>' ,'<c:out value="${requestScope.warningmessage}"/>')" >

	<div id="manifestWindow" style="display: none;position: absolute;top: 0%;left: 0%;width: 100%;height: 100%;background-color: white;z-index:1001;-moz-opacity: 0.5;opacity:.50;filter: alpha(opacity=50);"></div>
  <!--body class="tabBGColor" oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" onbeforeunload="beforeUnload()" -->
	<form method="post" name="frmFlight" action="showFlight.action">
	
	  <script type="text/javascript">
			var connLimit="";
			var minConnLimit="";
			<c:out value="${sessionScope.sesConnectionLimit}" escapeXml="false" />
			<c:out value="${sessionScope.sesMinConnectionLimit}" escapeXml="false" />
			
			var dayOffset = "<c:out value="${requestScope.reqDayOffSet}" escapeXml="false" />";
			var emailStatus = "<c:out value="${requestScope.Confirmation}" escapeXml="false" />";
			var dataSource = "<c:out value="${param.hdnLive}" escapeXml="false" />";
		</script>
  		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0" class="PageBackGround">
				<tr>
					<td><font class="Header">Flight Manifest</font></td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>		
		
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Flight Manifest Search<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table2">
							<tr>
								<td colspan="2"><font>Start Date - Local</font></td>								
								<td colspan="2"><font>Stop Date - Local</font></td>								
								<td><font>Flight No</font></td>
								<td><font>From</font></td>
								<td><font>To</font></td>
								<td><font>Operation Type</font></td>
								<td><font>Status</font></td>								
							</tr>
							<tr>
								<td style="width:75px;">									
									<input type="text" name="txtStartDateSearch" id="txtStartDateSearch"  invalidText="true"  onBlur="dateChk('txtStartDateSearch')" value=<%=StartDate %> style="width:70px;"  maxlength="10" tabIndex="1">
								</td>
								<td>
									<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar" tabIndex="2"><img src="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font>
								</td>
								<td style="width:75px;">
									<input type="text" name="txtStopDateSearch" id="txtStopDateSearch" invalidText="true" onBlur="dateChk('txtStopDateSearch')" value=<%=StopDate %> style="width:70px;" maxlength="10" tabIndex="3">
								</td>
								<td>
									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0" tabIndex="4"></a><font class="mandatory"> &nbsp;* </font>
								</td>								
								<td><!-- 
									<select id="txtFlightNoStartSearch" size="1" name="txtFlightNoStartSearch" style="width:35px" tabindex="1" maxlength="10">																				
																																			
								    </select>-->
									<font class="mandatory"> &nbsp;* </font><input type="text" id="txtFlightNoStartSearch" name="txtFlightNoStartSearch" maxlength="2" style="width:20px" value="" tabIndex="7">
									<!--input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="4" style="width:105px" onkeyUp="flightValid('txtFlightNoSearch')" onkeyPress="flightValid('txtFlightNoSearch')"-->									
									<input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="5" style="width:50px" tabIndex="8"> 									
								</td>
								<td>
									<select name="selFromStn6" id="selFromStn6" size="1" maxlength="30" style="width:60px" title="Origin" tabIndex="9">
										<option></option>
										<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select name="selToStn6" id="selToStn6" size="1" maxlength="30" style="width:60px" title="Destination" tabIndex="10">										
										<option></option>
										<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select name="selOperationTypeSearch" id="selOperationTypeSearch" size="1" maxlength="30" tabIndex="11">
										<c:out value="${sessionScope.sesOperationType}" escapeXml="false" />
										<option>All</option>
									</select><font class="mandatory"> &nbsp;* </font>
								</td>
								<td>
									<select name="selStatusSearch" id="selStatusSearch" size="1" maxlength="20" title="Flight Status" tabIndex="12">
										<c:out value="${sessionScope.sesStatus}" escapeXml="false" />	
										<option>All</option>										
									</select><font class="mandatory"> &nbsp;* </font>
								</td>
								<td></td>
								<td align="right">
									<input type="button" id="btnSearch"  name="btnSearch" value="Search" class="button" onclick="SearchData()" tabIndex="13">
								</td>
							</tr>
							<tr>
								<td colspan="11"></td>								
							</tr>

							<tr>
<!--  								<td width="10%"><FONT>Maxmium Connection Limit</td>
								<td><input type="text" id="txtConnLimit" name="txtConnLimit" maxlength="5" size="5" tabIndex="14" onblur="setTimeWithColon(document.forms[0].txtConnLimit)" style="height:23px">
								<font class="mandatory"> &nbsp;* </font></td>-->
								<td><font>Start Time</font></td>
								<td><input type="text" name="txtStartTime" id="txtStartTime" onBlur="setTimeWithColon(document.forms[0].txtStartTime)"  tabIndex="15" value="" style="width:35px;" maxlength="5"></td>
								<td><font>End Time</font></td>
								<td><input type="text" name="txtEndTime" id="txtEndTime" onBlur="setTimeWithColon(document.forms[0].txtEndTime)" tabIndex="16" value="" style="width:35px;" maxlength="5"></td>
								<td  colspan="4">
								<span id="spnFrequency">
								</td>
								<td align="right" colspan="3">
								<input type="button" id="btnLoad" value="Load" class="Button" NAME="btnLoad" tabIndex="17"  onclick="loadClick()"></td>							
							</tr>	
																																	
						</table>
						
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr><br><tr>
					  		<td style="height:370px;" valign="top">
						  		<%@ include file="../common/IncludeFormTop.jsp"%>Manifest Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
										<tr>
										  <td style="height:5px">
										  </td>
										</tr>										
										<tr>
											<td valign="top"><span id="spnInstances"></span></td>
										</tr>
									</table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
					  		</td>
					  	</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
						<table width="17%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table8">
								<tr>
									
									<td align="right">
										<input type="button" id="btnExit" value="Close" class="Button" NAME="btnExit" onclick="top.LoadHome()">										
										
									</td>
									
									<td align="right">
										<input type="button" id="btnPreview" value="Preview" class="Button" NAME="btnPreview" onclick="showManifestOptionsWindow(1)">										
										
									</td>
									
				<!--  			<u:hasPrivilege privilegeId="xbe.res.stat.email"><td align="right">
										<input type="text" name="txtEmailId" id="txtEmailId" maxlength="100"  tabindex="25" >										
										
									</td>
									
									<td align="right">
										<input type="button" id="btnEmail" value="Email" class="Button" NAME="btnEmail" onclick="sendEmail()">										
										
									</td>
									</u:hasPrivilege>-->
								</tr>
						</table>					
						<%@ include file="../common/IncludeFormBottom.jsp"%>  	  		
					  </td>
				</tr><tr><td>
				<div id="manifestOptions" style="position:absolute;top:210px;left:175px;width:601px;z-index:1002;display:none;overflow-y: auto;background-color: white;height: 295px">
				<div class="ui-widget ui-widget-content ui-corner-all addPaddingBottom" style="width:598px;height: 292px">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">
		<tr>
			<td colspan="2" height="20" class="FormHeadBackGround"><font class="FormHeader">Manifest Options</font></td>
			<td width="23" height="20" class="FormHeadBackGround">
				<a class="winClose" title="Close Popup" href="javascript:showManifestOptionsWindow(0)">
										<img width="17" border="0" src="../images/spacer_no_cache.gif" id="imgMMCl">
				</a>
			</td>
		</tr>
		<tr>
			<td class="FormBackGround" width="10"><img width="10" border="0" src="../images/spacer_no_cache.gif"></td>
			<td class="FormBackGround" valign="top">
						<table width="100%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table8" border="1">
								<tr><td colspan="5" style="height:10px;"></td></tr>
								<tr><td colspan="5"><font style="weight:bold;">Connections</font></td></tr>
								<tr><td colspan="5" style="height:10px;"></td></tr>
								<tr>
									<td align="center">Type</td>
									<td align="center">Max. Connection Time</td>
									<td align="center">Origin/Destination</td>
									<td align="center">Boarding Airport</td>
									<td align="center">Connection Flight No</td>
								</tr>
								<tr><td colspan="5" style="height:4px;"></td></tr>
								<tr>
									<td>
										<input type="checkbox" id="chkInwardConn" name="chkInwardConn" onClick="onCheckClick(true)" checked>Inward</input>
									</td>
									<td align="center">
										<input type="text" id="txtMaxInwardConnTime" name="txtMaxInwardConnTime" onblur="setTimeWithColon(document.forms[0].txtMaxInwardConnTime)" style="width:50;"></input>
									</td>
									<td align="center">
									<select name="selInwardOrigin" id="selInwardOrigin" size="1" maxlength="30" style="width:60px" title="Origin" tabIndex="9">
										<option></option>
										<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
									</select>
									</td>
									<td align="center">
									<div id="inwardBoardingAirport">
									</div>
									</td>
									<td align="center">
									<input type="text" id="txtInwardFlightNoPrefix" name="txtInwardFlightNoPrefix" maxlength="2" style="width:20px" value="" tabIndex="7">						
	&nbsp								<input type="text" id="txtInwardFlightNo" name="txtInwardFlightNo" maxlength="4" style="width:50px" tabIndex="8">									
								</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" id="chkOutwardConn" name="chkOutwardConn" onClick="onCheckClick(false)" checked>Outward</input>
									</td>
									<td align="center">
										<input type="text" id="txtMaxOutwardConnTime" name="txtMaxConnTime" onblur="setTimeWithColon(document.forms[0].txtConnLimit)" style="width:50;"></input>
									</td>
									<td align="center">
									<select name="selOutwardDestination" id="selOutwardDestination" size="1" maxlength="30" style="width:60px" title="Origin" tabIndex="9">
										<option></option>
										<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
									</select>
									</td>
									<td align="center">
									<div id="outwardBoardingAirport">
									</div>
									</td>
									<td align="center">
									<input type="text" id="txtOutwardFlightNoPrefix" name="txtOutwardFlightNoPrefix" maxlength="2" style="width:20px" value="" tabIndex="7">								&nbsp;
									<input type="text" id="txtOutwardFlightNo" name="txtOutwardFlightNo" maxlength="4" style="width:50px" tabIndex="8">									
								</td>
								</tr>
								<tr><td colspan="5" style="height:10px;"></td>
								</tr>
								<%--</table>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table9" border="1">--%>
								<tr><td colspan="5" style="height:10px;"></td></tr>	
								<tr><td><span id="classOfService" style="display:none;">&nbsp;Class of Service</span></td>
									<td colspan="4" align="left">
										<span id="classOfServiceSelect">
										</span>	
									</td>
								</tr>	
								<tr><td colspan="5" style="height:10px;"></td></tr>
								<tr>
									<td>&nbsp;Sort By</td>
									<td align="left" colspan="4">
										<input type="radio" name="radSortOption" id="radSortOption"	value="Pnr" onClick="chkClick()" class="noBorder" checked>PNR</input>&nbsp;
										<input type="radio" name="radSortOption" id="radSortOption" value="FName" onClick="chkClick()" class="noBorder">First Name</input>&nbsp;
										<input type="radio" name="radSortOption" id="radSortOption" value="LName" onClick="chkClick()" class="noBorder">Last Name</input>&nbsp;
										<input type="radio" name="radSortOption" id="radSortOption" value="BookingDt" onClick="chkClick()" class="noBorder">Booking Date</input>&nbsp;
										<input type="radio" name="radSortOption" id="radSortOption" value="OnD" onClick="chkClick()" class="noBorder">OnD</input>							
										<input type="radio" name="radSortOption" id="radSortOption" value="Agency" onClick="chkClick()" class="noBorder">Agency</input>
										<input type="radio" name="radSortOption" id="radSortOption" value="ConnectionTime" onClick="chkClick()" class="noBorder">Con. Time</input>
									</td>

								</tr>
								<tr><td colspan="5" style="height:10px;"></td></tr>
								
								<tr>
									<td colspan="5">
										<table width="100%" cellspacing="0" border="0">
											<tr id = "psptFilter"> 
												<td align = "left" width = "100px" >&nbsp;Passport</td>
													<td  align="right" >
														<select name="selPassport" size="1" id="selPassport">
															<option value="ANY" selected="selected">Any</option>								
															<option value="EXISTING">Existing</option>
															<option value="MISSING">Missing</option>
													  	</select>							
												</td>
											</tr>
											
											<tr><td colspan="5" style="height:4px;"></td></tr>														
								
											<tr id = "docsFilter">								   
												<td align = "left" width = "100px" >&nbsp;DOCS Info</td>
													<td align="right">
														<select name="selDOCS" size="1" id="selDOCS">
															<option value="ANY" selected="selected">Any</option>										
															<option value="EXISTING">Existing</option>
															<option value="MISSING">Missing</option>
													  	</select>							
													</td>								  
											</tr>											
										</table>
									</td>
								</tr>
								
								
								
						
								<tr><td colspan="5" style="height:7px;"></td></tr>
	
								<u:hasPrivilege privilegeId="xbe.res.stat.manifest.viewopt">
								<tr>
									<td>&nbsp;View</td>
									<td  align="left" colspan="4"><input type="radio" name="radViewOption" id="radViewOption"	value="detailed" onClick="chkClick()" class="noBorder" checked>Detailed</input>&nbsp;
									<input type="radio" name="radViewOption" id="radViewOption"	value="summary" onClick="chkClick()" class="noBorder">Summary</input></td>
								</tr>
								<tr><td colspan="5" style="height:7px;"></td></tr>
								</u:hasPrivilege>							
								<u:hasPrivilege privilegeId="xbe.res.stat.manifest.summaryopt">
								<tr>
									<td>&nbsp;Summary Option</td>
									<td align="left" colspan="4"><input type="radio" name="radSummaryOption" id="radSummaryOption"	value="segment" onClick="chkClick()" class="noBorder" checked>Segment Wise</input>&nbsp;<input type="radio" name="radSummaryOption" id="radSummaryOption"	value="inout" onClick="chkClick()" class="noBorder">In/Out</input></td>
								</tr>
								<tr><td colspan="5" style="height:7px;"></td></tr>
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.res.stat.manifest.summarypos">
								<tr>
									<td>&nbsp;Summary Position</td>
									<td align="left" colspan="4"><input type="radio" name="radSummaryPosition" id="radSummaryPosition"	value="top" onClick="chkClick()" class="noBorder" checked>Top</input>&nbsp;<input type="radio" name="radSummaryPosition" id="radSummaryPosition"	value="bottom" onClick="chkClick()" class="noBorder">Bottom</input></td>
								</tr>
								<tr><td colspan="5" style="height:7px;"></td></tr>
								</u:hasPrivilege>
								<tr><td colspan="5" style="height:10px;"></td></tr>
								<%--</table>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table10" border="1">--%>
									<tr>
										<td align="left"><input type="button" id="btnClose" value="Close" class="Button" onclick="showManifestOptionsWindow(0)"/></td>
										<td colspan="4" align="right">
										<u:hasPrivilege privilegeId="xbe.res.stat.download">
										<input type="button" id="btnDownload"value="Download" class="Button" onclick="viewAndDownloadManifest()"/>
										</u:hasPrivilege>
										<input type="button" id="btnShow" value="Show" class="Button" onclick="previewManifest()"/>
										<u:hasPrivilege privilegeId="xbe.res.stat.email">
										<input type="text" name="txtEmailId" id="txtEmailId" maxlength="100">
										<input type="button" id="btnEmail"value="Email" class="Button" onclick="sendEmail()"/>
										</u:hasPrivilege>
										</td>	
									</tr>
								</table>
							</td>
							<td class="FormBackGround" width="10"><img width="10" border="0" src="../images/spacer_no_cache.gif"></td>
						</tr>	
				</table>
			</div></div>			
			</td>
		</tr>
	</table>
	
  	<%@ include file="../common/IncludeShortCuts.jsp"%>  	
   	<%@ include file="../common/IncludeLoadMsg.jsp"%>

	<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
	<input type="hidden" name="hdnRecNo" id="hdnRecNo"  value=""/>	
	<input type="hidden" name="hdnCosType" id="hdnCosType" value=""/>
			
	</form>
		<script src="../js/flight/FlightInstance.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/flight/FlightInstanceGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">	
	    
	   
		</script>	
		
		<form method="post" name="frmF" id ="frmF" action="showFlight.action">

		</form>

  </body>
</html>