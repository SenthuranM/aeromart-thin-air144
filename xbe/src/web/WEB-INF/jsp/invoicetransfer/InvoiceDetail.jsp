<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;	
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date	
	
	String CurrentDate =  formatter.format(dStartDate);
	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Invoice Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
<script type="text/javascript">
		var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		<c:out value="${requestScope.reqinvoiceGenDetails}" escapeXml="false" />
		<c:out value="${requestScope.reqViewReportStatus}" escapeXml="false" />
		var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
		var totString = "<c:out value="${requestScope.totalString}" escapeXml="false" />";
		var agentType = "<c:out value="${requestScope.reqShowTotal}" escapeXml="false" />";
		var IsTransEnable = "<c:out value="${requestScope.IsTransEnable}" escapeXml="false" />";
</script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" 
  scroll="yes" style="background: transparent;">
  	<%@ include file="../common/IncludeTabTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmInvoiceEmail" id=frmInvoiceEmail action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		 		<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>								
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Search Invoice<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">							
							<tr>
								<td colspan="7"><div id='divTable'>
									<table width="80%" border="0" cellpadding="0" cellspacing="2">	
										<tr>
										<td width="11%" align="left"><font>Year</font></td>
											<td><select name="selYear" id="selYear" size="1" style="width:100px" onChange= "validatePeriod()" title="Year">
												<OPTION value="" selected></OPTION>	
													<c:out value="${requestScope.reqBasePrevious}" escapeXml="false" />
												</select><font class="mandatory"><b>*</b></font>
											</td>
											<td><font>Month </font></td>
											<td align="left"><select name="selMonth" id="selMonth" size="1" style="width:100px" onChange="validatePeriod()" title="Month">
													<OPTION value=""></OPTION>	
													<OPTION value=01>January</OPTION>
													<OPTION value=02>February</OPTION> 
                                					<OPTION value=03>March</OPTION> 
                                					<OPTION value=04>April</OPTION> 
                                					<OPTION value=05>May</OPTION> 
                                					<OPTION value=06>June</OPTION> 
                                					<OPTION value=07>July</OPTION>
                                					<OPTION value=08>August</OPTION> 
                                					<OPTION value=09>September</OPTION> 
                                					<OPTION value=10>October</OPTION> 
                                					<OPTION value=11>November</OPTION> 
                                					<OPTION value=12>December</OPTION>
												</select><font class="mandatory"><b>*</b></font>
											</td>
											<td></td>
											<td>
												<table width="100%" border="0" cellpadding="0" cellspacing="2">
													<tr>
														<td><font>Invoice Period</font></td>
														<td>
															<select name="selBP" id="selBP" size="1" style="width:50px" onChange="validatePeriod()">
																<c:out value="${requestScope.reqInvPeriods}" escapeXml="false" />
															</select>
														</td>		
													</tr>	
												</table>
											</td>
											<td><input type="checkBox" name="chkZero" id="chkZero" value="ZERO" class="noBorder">
												<font>Exclude Zero Invoices</font></td>		
																					
										</tr>
									</table></div>
								</td>
							</tr>
							<tr>
								<td width="9%"><font>Agencies</font></td>
								<td width="15%"><select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:100px">									
										<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
									</select>
								</td>
								<td width="18%"><input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font></td>
								<td width="18%"><input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font></td>
								<td><input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"></td>
								<td width="9%"><font>Entity</font></td>
								<td width="15%"><select id="selEntity" size="1" name="selEntity" onClick="clickEntity()" onChange="changeEntity()" style="width:100px">									
										<c:out value="${requestScope.reqEntityList}" escapeXml="false" />					
									</select>
								</td>
							</tr>							
							<tr>
								<td>
									<font class="fntBold">Agents</font>
								</td>
							</tr>
							<tr>
								<td valign="top" colspan="7"><span id="spn1"></span>								
							</tr>
							<tr><td colspan="6" align="right">
							<u:hasPrivilege privilegeId="ta.invoice.generate">
										<input name="btnGenerate" type="button" class="Button" id="btnGenerate" value="Generate" onClick="generateClick()">
									</u:hasPrivilege>
							<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search" onclick="searchClick()">
							</td></tr>
						</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>							
				<tr>	
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Invoice Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr>
								<td colspan="7">									
  									<span id="spnInvoices"></span>
  									<span id="spnTotal"></span>									
								</td>
							</tr>
					        <tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
								</td>
								<td colspan="1" align="right">
									<input type="checkBox" name="chkPage" id="chkPage" value="Page" class="noBorder" onClick="clickPage()">
									<font>Agents in Page</font>
									<u:hasPrivilege privilegeId="ta.invoice.any">
									<span id="forAll" name="forAll">									
										<input type="checkBox" name="chkAll" id="chkAll" value="ALL" class="noBorder" onClick="clickAll()">
										<font>For All Agents</font>
									</span>	
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="ta.invoice.transfer">
										<input name="btnTrnsfer" type="button" class="Button" id="btnTrnsfer" value="Transfer" onClick="transferClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="ta.invoice.email">
										<input name="btnSend" type="button" class="Button" id="btnSend" value="Send" onClick="sendClick()">
									</u:hasPrivilege>		
								</td>
								<td valign="bottom">
									<input name="btnViewReport" type="button" class="Button" id="btnViewReport" value="Download" onClick="viewReport()" style="visibility: hidden">
								</td>
								<td valign="bottom" align = "left">
									<input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printReport()" style="visibility: hidden">
								</td>
							</tr>					
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
			</table>
				<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				
				</script>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnInvoice" id="hdnInvoice" value="">
		<input type="hidden" name="hdnSelectedAgents" id="hdnSelectedAgents" value="">
		<input type="hidden" name="hdnSelectedInvoices" id="hdnSelectedInvoices" value="">
		<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">	
	</form>
	<%@ include file="../common/IncludeTabBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
 <script src="../js/invoicetransfer/InvoiceValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 <script src="../js/invoicetransfer/InvoiceAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      HideProgress();
   }
   //-->
  </script>
  <%@ include file="../common/IncludeLoadMsg.jsp"%>
</html>
