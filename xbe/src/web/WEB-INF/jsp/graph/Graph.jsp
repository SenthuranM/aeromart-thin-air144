<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
	<head>
		<title>Agent User Transactions</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
			   
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script src="../js/common/main.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script language="javascript" src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>			   
			   
	    <script type="text/javascript" src="../js/mochikit/MochiKit.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<script type="text/javascript" src="../js/plotkit/Base.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<script type="text/javascript" src="../js/plotkit/Layout.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<script type="text/javascript" src="../js/plotkit/Canvas.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<script type="text/javascript" src="../js/plotkit/SweetCanvas.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<script type="text/javascript" src="../js/plotkit/excanvas.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<script type="text/javascript" src="../js/graph/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../js/graph/DrawGraph.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	</head>
   
    <script type="text/javascript">
    var graphData = "<c:out value='${requestScope.graphContent}' escapeXml='false' />";
	var graphTitle = "<c:out value='${requestScope.graphTitle}' escapeXml='false' />";
 			     
    </script>
    <body onLoad="OnLoad()" scroll="no">
        
    </body>
   
</html>