			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>
				<tr><td> &nbsp; </td></tr>
				<tr><td> &nbsp; </td></tr>
				
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Add New Frequent Customer<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="10">							
							
							<tr>
								<td style="width: 80px;"> <font>Pax Type</font> </td>
								<td>
									<select style="width:100px;font-size:12px" id="paxType" name="paxType" size="1" class="editable">
										<option value=""></option>
										<option value="AD">Adult</option>
										<option value="CH">Child</option>
										<option value="IN">Infant</option>
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
							</tr>
							<tr>
								<td style="width: 80px;" id="titleLable"> <font>Title</font> </td>
								<td id="titleSelect">
									<select style="width:100px;font-size:12px" id="title" name="title" size="1" class="editable">
										<option value=""></option>
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
								
								<td style="width: 95px;"><font>First Name </font></td>
								<td>
									<input type="text" name="firstName" id="firstName" style="width: 160px;" >
									<font class="mandatory">&nbsp;*</font>
								</td>
								
								<td><font>Last Name </font></td>
								<td>
									<input type="text" name="lastName" id="lastName"  style="width: 160px;">
									<font class="mandatory">&nbsp;*</font>
								</td>
								
							</tr>
							
							<tr>
								<td> <font>Date of Birth</font> </td>
								<td>
									<input id="dateOfBirth" name="dateOfBirth" type="text"/>
								</td>
								
								<td><font>Nationality</font></td>
								<td>
									<select style="width: 162px;" id="nationality" name="customer.nationality" size="1" class="" >
										<option value="">&nbsp;</option>
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
								
								<td id="emailLable"><font>Email</font></td>
								<td id="emailInput">
									<input type="text" name="email" id="email"  style="width: 160px;">
									<font class="mandatory">&nbsp;*</font>
								</td>
								
							</tr>
	
							<tr id="passportDetails">
	
								<td><font>Passport Number</font></td>
								<td>
									<input type="text" name="passportNumber" id="passportNumber">
								</td>
								<td><font>Passport Expiry Date</font></td>
								<td>			
									<input type="text" name="passportExpDate" id="passportExpDate">					
								</td>
	
							</tr>

							<tr>
								<td colspan="6">
									<u:hasPrivilege privilegeId="xbe.tool.frequentcustomer.save">
										<input type="button"  name="saveFrequentCustomer" id="btnSave" class="Button" value="Save" style="width:110px"/>
									</u:hasPrivilege>									
									<input name="btnClose"type="button" id="btnClose" class="Button" value="Close"/>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 			
			</table>		
