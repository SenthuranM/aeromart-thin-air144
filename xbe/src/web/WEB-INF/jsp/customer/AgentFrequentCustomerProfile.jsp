<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script  src="../js/customer/customerValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>


  </head>
   <body style="background: rgb(255,255,255);" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)"> 
  <!--<body style="background: transparent;"> -->
   <div style="overflow: auto; height:650px;width: 970px;margin: 0 auto" id="frequentCustomerContainer">
<!--    <form name="frmUser" id="frmUser" method="post" action="saveUser.action"> -->
			<!-- Page Background Top page -->
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>
				<tr><td> &nbsp; </td></tr>
				<tr><td> &nbsp; </td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td><font>First Name </font></td>
								<td >
									<input type="text" name="firstName" id="search_firstName" class="editable fontCapitalize"/>
								</td>
								
								<td><font>Last Name </font></td>
								<td>
									<input type="text" name="lastName" id="search_lastName" class="editable fontCapitalize"/>
								</td>
								
								<td><font>Email </font></td>
								<td>
									<input type="text" name="email" id="search_email"/>
								</td>
								
								<td>
									<input type="button"  name="searchProfile" id="btnSearch" class="Button" value="Search" style="width:110px"/>
								</td>
							</tr>
							
	 				    </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>	
				
				<tr><td> &nbsp; </td></tr>
				<tr><td> &nbsp; </td></tr>
				
				<tr>
					
					<td id="frequentCustomerProfilesGridContainer">
								
					</td>
				
				</tr>	

				<tr><td> &nbsp; </td></tr>
				<tr><td> &nbsp; </td></tr>
			
				<tr>
					<table width="100%" border="0" cellpadding="0" cellspacing="4">
						<tr>
							<td>
								<u:hasPrivilege privilegeId="xbe.tool.frequentcustomer.save">
									<input type="button"  name="addFrequentCustomer" id="btnAdd" class="Button" value="Add"/>
								</u:hasPrivilege>

								<u:hasPrivilege privilegeId="xbe.tool.frequentcustomer.edit">
									<input type="button"  name="editFrequentCustomer" id="btnEdit" class="Button" value="Edit"/>
								</u:hasPrivilege>

								<u:hasPrivilege privilegeId="xbe.tool.frequentcustomer.remove">
									<input type="button"  name="deleteFrequentCustomer" id="btnDelete" class="Button" value="Delete"/>
								</u:hasPrivilege>
							</td>
						</tr>
					</table>					
				</tr>	 
				
				<tr><td> &nbsp; </td></tr>
				
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Add / Edit<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="10">	
				  			
				  			<tr>
				  				<td style="width: 80px;"> <font>Pax Type</font> </td>
								<td>
									<select style="width:100px;font-size:12px" id="paxType" name="paxType" size="1" class="editable">
										<option value=""></option>
										<option value="AD">Adult</option>
										<option value="CH">Child</option>
										<option value="IN">Infant</option>
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
				  			</tr>						
							
							<tr>
								<td style="width: 80px;"> <font>Title</font> </td>
								<td>
									<input type="hidden" name="frequentCustomerId" id="frequentCustomerId" value=""/>
									<select style="width:100px;font-size:12px" id="title" name="title" size="1" class="editable">
										<option value=""></option>
									</select>
									<font class="mandatory" id="titleMandatory">&nbsp;*</font>
								</td>
								
								<td style="width: 95px;"><font>First Name </font></td>
								<td>
									<input type="text" name="firstName" id="firstName" style="width: 160px;" class="editable fontCapitalize"/>
									<font class="mandatory">&nbsp;*</font>
								</td>
								
								<td><font>Last Name </font></td>
								<td>
									<input type="text" name="lastName" id="lastName"  style="width: 160px;" class="editable fontCapitalize"/>
									<font class="mandatory">&nbsp;*</font>
								</td>
								
							</tr>
							
							<tr>
								<td> <font>Date of Birth</font> </td>
								<td>
									<input id="dateOfBirth" name="dateOfBirth" type="text" />
								</td>
								
								<td><font>Nationality</font></td>
								<td>
									<select style="width: 162px;" id="nationality" name="customer.nationality" size="1" class="">
										<option value="">&nbsp;</option>
										<c:out value="${requestScope.nationalityList}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
								
								<td><font>Email</font></td>
								<td>
									<input type="text" name="email" id="email"  style="width: 160px;"/>
									<font class="mandatory" id="emailMandatory">&nbsp;*</font>
								</td>
								
							</tr>
	
							<tr>
	
								<td><font>Passport Number</font></td>
								<td>
									<input type="text" name="passportNumber" id="passportNumber"/>
								</td>
								<td><font>Passport Expiry Date</font></td>
								<td>			
									<input type="text" name="passportExpDate" id="passportExpDate"/>					
								</td>
	
							</tr>

							<tr>
								<td colspan="6">
									<u:hasPrivilege privilegeId="xbe.tool.frequentcustomer.save">
										<input type="button"  name="saveFrequentCustomer" id="btnSave" class="Button" value="Save" style="width:110px"/>
									</u:hasPrivilege>									
									<input name="btnClose"type="button" id="btnClose" class="Button" value="Close"/>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
					<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
						<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
					</div>
				</tr>				 			
			</table>		

	<script src="../js/customer/agentFrequentCustomer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script>
		var screenId = 'SC_FQ_CUS';
	</script>	
</div>
</body>
</html>
