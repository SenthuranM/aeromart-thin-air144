<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/customer/customerValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<style>
		#spnError div.errorBoxHttps{
			padding: 2px 9px 0px 8px;
			background: #F3F376;
			-moz-border-radius:4px;
		}
	</style>

  </head>
   <body style="background: rgb(255,255,255);" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)"> 
  <!--<body style="background: transparent;"> -->
   <div style="overflow: auto; height:650px;width: 970px;margin: 0 auto" id="customerContainer">
<!--    <form name="frmUser" id="frmUser" method="post" action="saveUser.action"> -->
			<!-- Page Background Top page -->
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Customers						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="15%"><font>Email Address </font></td>
								<td width="10%">
									<input  name="customer.searchEmailAddress" type="text" id="searchEmailAddress" size="40" maxlength="60" />
								</td>
								<td  width="5%">
									<font class="mandatory">&nbsp;*</font> 
								</td>
						        <td align="left" colspan="3">
								  	<input name="customer.btnSearch" type="button" class="Button" id="buttonSearch" value="Search" /> 
								</td>
								<td width="20%" colspan="2">&nbsp;</td>
							</tr>
							
	 				    </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Customers<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4">							
							<tr>
								<td id="customerDetailsGridContainer">
								
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="xbe.cus.reg.add">
										<input type="button"  name="customer.addButton" id="btnAdd" class="Button" value="Add"/>
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="xbe.cus.reg.edit">
										<input type="button"  name="customer.editButton" id="btnEdit" class="Button" value="Edit"/>
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>
					<div id="customerFormHead">Add/Edit Customers &nbsp;&nbsp;</div><%@ include file="../common/IncludeFormHD.jsp"%>		
					<div id="tabs">
						<ul>
							<li><a href="#tabs-1">Customer Details</a></li>
							<li><a href="#tabs-2">Air Rewards</a></li>
							<li><a href="#tabs-3">Emergency Contact</a></li>
							<li><a href="#tabs-4">Customer Preferences</a></li>
							<li><span id="spnError">&nbsp;</span></li>
						</ul>
						<div id="tabs-1">
			               <table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">  
								  <tr>
								  <td colspan="2">
								  <table width="100%" border="0" cellpadding="1" cellspacing="2" align="left" style="border-spacing: 1em">	
									<tr>
										<td id="tdEmail">
											<font>Email Address</font>
										</td><td>
											<input  name="editEmailAddress" type="text" id="editEmailAddress" size="30" maxlength="60" />
											<font class="mandatory" id="lblEmailMand">&nbsp;*</font> 
										</td>
										<td colspan="4">
											<label id="lmsMessage" class="mandatory" >  </label>										
										</td>
					                </tr>  
					                  
					                  <tr>
										<td id="tdTitle">
											<font>Title</font>
										</td>
										<td  id="tdTitleField">
									    	<select style="width:50px;font-size:12px" id="displayTitle" name="displayTitle" size="1" class="editable aa-input">
									        	<!-- <option value=""></option> -->
									            <!-- <option value="MR">Mr.</option>
									            <option value="MS">Ms.</option> -->
									           <!--  <option value="MRS">Mrs.</option> -->
											</select>
									        <font class="mandatory" id="lblTitleMand">&nbsp;*</font>
										</td>
										<td id="tdFirstName">
											<font>First Name</font>
										</td>
										<td id="tdFirstNameField">
								        	<span style="display:none;" id="spnCtlFirstName"> <span id="spnFirstName"> </span>&nbsp; </span>
								            <input name="firstName" type="text" id="firstName" size="20" maxlength="30" class="fontCapitalize"/>
								            <font class="mandatory" id="lblFirstNameMand">&nbsp;*</font>
								        </td>
					                    <td id="tdLastName">
											<font>Last Name</font>
										</td>
										<td>
								        	<span style="display:none;" id="spnCtlLastName"> <span id="spnLastName"> </span>&nbsp; </span>
								            <input name="lastName" type="text" id="lastName" size="20" maxlength="30" class="fontCapitalize"/> 
										    <font class="mandatory" id="lblLastNameMand">&nbsp;*</font>
								        </td>
					              	</tr>
			    	               	<tr>
			    	               		<td id="tdGender">	
				    	               		<label id="lblGender">Gender</label>
										</td>
										<td id="tdGenderField">
											<font>:</font>
											<input checked="checked"  type="radio" class="NoBorder" name="customer.gender" id="radGender" value="M"/>
											<label id="lblMale">Male</label>
											<input type="radio" class="NoBorder" name="customer.gender" id="radFemale" value="F"/>
											<label id="lblFemale">Female</label>
										</td>		        	           
										<td  id="tdNationality">
											<font>Nationality</font>
										</td>
										<td id="tdNationalityField">
											<select style="width: 150px;" id="nationality" name="customer.nationality" size="1" class="aa-input">
												<option value="">&nbsp;</option>
												<c:out value="${requestScope.nationalityList}" escapeXml="false" />
											</select>
											<font class="mandatory" id="lblNationalityMand">&nbsp;*</font>								                   
					                    </td>
										<td id="tdCountry">
											<font>Country of residence</font>
										</td>
										<td id="tdCountryField">
											<select style="width: 150px;" id="residence" name="customer.residence" size="1" class="aa-input">
												<option value="">&nbsp;</option>
												<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
											</select>
											<font class="mandatory" id="lblCountryMand">&nbsp;*</font>								                   
										</td>
										<td id="tdZipCode">
											<font>Zip Code </font>
										</td>
										<td id="tdZipCodeField">
											<input  name="zipCode" type="text" id="zipCode" size="10" maxlength="10" />
											<font class="mandatory" id="lblZipCodeMand">&nbsp;*</font>
								        </td>										
									</tr>
									<tr id="trAddress">
										<td id="tdAddress">
											<table width="100%" border="0" cellpadding="1" cellspacing="2" align="left">
												<tr id="trAddr1">
													<td>
														<label id="lblAddress">Address </label>
												</td><td>
														<font>:</font>
														<input type="text" maxlength="100" name="customer.addressStreet" id="txtAddr1"/> 
														<label class="mandatory" id="lblAddrMand">*</label>
													</td>
												</tr>
												
												<tr id="trAddr2">
													<td>
														<label >&nbsp; </label>
													</td>
													<td>
														<font>:</font>
														<input type="text" maxlength="100" name="customer.addressLine" id="txtAddr2"/>
													</td>
												</tr>
											</table>
										</td>
									</tr>	
									
									<tr id="trContactNo">
										<td id="tdMobileNo">
											<font>Mobile No</font>
										</td>
										<td id="tdMobileNoField">
											<input type="text" id="txtMobiCntry" name="txtMobiCntry" maxlength="4" title="Enter Your Country Code" class="aa-input" style="width:30px;" />
											<input type="text"  maxlength="4" style="width: 30px;" name="txtMobiArea" id="txtMobiArea" class="areaCode" title="Enter Your Area Code"/>
											<input type="text" id="txtMobiNo" name="txtmobileNo" maxlength="10" title="Enter Your Mobile Number" class="aa-input" style="width:75px;" />
											<font class="mandatory" id="lblMobileMand">&nbsp;*</font>
								        </td>
										<td id="tdPhoneNo">
											<font>Number during travel</font>
										</td>
										<td id="tdPhoneNoField">
											<input type="text" id="txtTravCntry" name="txtTravCntry" maxlength="4" title="Enter Your Country Code" class="aa-input" style="width:30px;" />
											<input type="text"  maxlength="4" style="width: 30px;" name="txtTravArea" id="txtTravArea" class="areaCode" title="Enter Your Area Code"/>
											<input type="text" id="txtTravNo" name="txtTravNo" maxlength="10" title="Enter Your Telephone Number" class="aa-input" style="width:75px;" />
										   	<font class="mandatory" id="lblPhoneMand">&nbsp;*</font>
								        </td>
										<td  id="tdHomeOfficeNo">
											<label id="lblOfficeOHomeNo">Office/Home Telephone</label>
										</td>
										<td id="tdHomeOfficeNoField">
											<font>:</font>
											<input type="text"  maxlength="4" style="width: 30px;" name="txtOCountry" id="txtOCountry"/>
											<input type="text"  maxlength="4" style="width: 30px;" name="txtOArea" id="txtOArea" class="areaCode"/>
											<input type="text"  maxlength="10" style="width: 75px;" name="txtOfficeTel" id="txtOfficeTel"/>
											<label class="mandatory" id="lblHomeOfficeMand">*</label>
										</td>
										<td  id="tdFaxNo">
											<font>Fax No</font>
										</td>
										<td id="tdFaxNoField">
											<input type="text" id="txtFaxCntry" name="txtFaxCntry" maxlength="4" title="Enter Your Country Code" class="aa-input" style="width:30px;" />
											<input type="text"  maxlength="4" style="width: 30px;" name="txtFaxArea" id="txtFaxArea" class="areaCode" title="Enter Your Area Code"/>
											<input type="text" id="txtFaxNo" name="txtFaxNo" maxlength="10" title="Enter Your Fax Number" class="aa-input" style="width:75px;" />
										   	<font class="mandatory" id="lblFaxMand">&nbsp;*</font>
								        </td>
									</tr>
									
									<tr id="trJoinAirewards">
										<td>
											<font>Join Air Rewards</font>
										</td>
										<td id="trJoinAirewardsField">
									    	<input name="joinAirewards" type="checkbox" id="joinAirewards" maxlength="1" size="1" value="Active"/>
								        </td>
									</tr>
									<tr>
									 	<td class="paddingL5" colspan="3" id="tdPassResend">
									 		<u><label id="lblPassResend" class="paddingL5" style="cursor: pointer;">Resend Login Information</label></u>
									 		<input type="hidden" name="emailID" id="emailID"/>
									 	</td>
									 </tr>	                
								 </table>
							</td>
							</tr></table>
							</div>
				            <div id="tabs-2" >
				            	<table width="100%" border="0" cellpadding="6" cellspacing="0" align="center" style="border-spacing: 1em">
							 	<tr> 
									<td ><font>Date of Birth</font></td>
									<td>
										<input id="birthDate" name="customer.birthDate" type="text" />
<!-- 										<img class="ui-datepicker-trigger" src="../images/Calendar_no_cache.gif" alt="..." title="..."/> -->
										<font class="mandatory">&nbsp;*</font>
									</td>
								</tr>
								 <tr>
								 	<td ><font>Comm. Language</font></td>
								 	<td>
								 		<select id="selPrefLang" name="customer.selPrefLang"
											class="aa-input" style="width: 145px;" tabindex="31">
												<option value="en">English</option>
												<option value="ar">Arabic</option>
												<!-- <option value="ru">Russian</option>
												<option value="fr">French</option>
												<option value="es">Spanish</option>
												<option value="it">Italian</option>
												<option value="tr">Turkish</option>
												<option value="zh">Chinese</option> -->
										</select>
										</td>
										
									<td ><font>Passport Number</font></td>
								 	<td>
								 		<input type="text" id="passportNum" name="customer.passportNum" maxlength="15" class="aa-input" />
								 	</td>
								 </tr>
								 
								 <tr>
								 	
								 	<td ><font>Referring Email</font></td>
								 	<td>
								 		<input type="text" id="refferEmail" name="customer.refferEmail" maxlength="40" class="aa-input" />
									</td>
										
									<td ><font>Family Head Email</font></td>
								 	<td>
								 		<input type="text" id="familyHeadEmail" name="customer.familyHeadEmail" maxlength="40" class="aa-input" />
								 	</td>
								 	
								 </tr>
								 <tr>
								 	<td class="paddingL5" colspan="3" id="tdLmsResend">
								 		<u><label id="lblLmsResend" class="paddingL5" style="cursor: pointer;">Resend Air Rewards Confirmation Email</label></u>
								 		<input type="hidden" name="ffid" id="ffid"/>
								 	</td>
								 </tr>
								 
								 
								 
				                </table>
	            			            
				            </div>
				            <div id="tabs-3">
				            	<table width="100%" border="0" cellpadding="6" cellspacing="0" align="center" style="border-spacing: 1em" id=tblEmergencyContactInfo>
									<tr>
										<td>
											<label class="fntBold hdFontColor" id="lblEmergencyContactInfo"></label>
										</td>
									</tr>
								
									<tr id="trEmgnTitle">
										<td>
											<label id="lblTitle">Title</label>
										</td>
										<td>
											<font>:</font>
											<select  style="width: 50px;" size="1" name="customer.emgnTitle" id="selEmgnTitle">
												<!-- <option value=""></option> -->
									           <!--  <option value="MR">Mr.</option>
									            <option value="MS">Ms.</option>
									            <option value="MRS">Mrs.</option> -->
											</select>
											<label class="mandatory" id="lblEmgnTitleMand"> *</label>
										</td>
									</tr>
								
									<tr id="trEmgnFirstName">
										<td>
											<label id="lblFirstName">First Name</label>
										</td>
										<td>
											<font>:</font>
											<input type="text" class="fontCapitalize" maxlength="50" name="customer.emgnFirstName" id="txtEmgnFirstName"/> 
											<label class="mandatory" id="lblEmgnFirstNameMand">*</label>
										</td>
									</tr>
								
									<tr id="trEmgnLastName">
										<td>
											<label id="lblLastName">Last Name</label>
										</td>
										<td>
											<font>:</font>
											<input type="text"  class="fontCapitalize" maxlength="50" name="customer.emgnLastName" id="txtEmgnLastName"/> 
											<label class="mandatory" id="lblEmgnLastNameMand">*</label>
										</td>
									</tr>
								
									<tr id="emgnPhone2">
										<td>
											<label id="lblTelNo">Number During Travel</label>
										</td>
										<td>
											<font>:</font>
											<input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPCountry" id="txtEmgnPCountry"/>
											<input type="text"  maxlength="4" style="width: 50px;" name="txtEmgnPArea" id="txtEmgnPArea" class="areaCode"/>
											<input type="text"  maxlength="10" style="width: 100px;" name="txtEmgnTelephone" id="txtEmgnTelephone"/>
											<label class="mandatory" id="lblEmgnPhoneMand">*</label>
										</td>
									</tr>
								
									<tr id="trEmgnEmail">
										<td>
											<label id="lblEmail">Email</label>
										</td>
										<td>
											<font>:</font>
											<input type="text" maxlength="100" size="25" name="customer.emgnEmail" id="txtEmgnEmail"/> 
											<label class="mandatory" id="lblEmgnEmailMand">*</label>
										</td>
									</tr>
								</table>		            
				            </div>
				            <div id="tabs-4">					
								<table cellspacing="0" cellpadding="0" border="0" align="center" width="99%" id="tblPreference">
									<tr id="trPromotionPref">
										<td>
											<table cellspacing="0" cellpadding="2" border="0" class="alignLeft" width="100%">
											<tr>
												<td colspan="4">
													<label class="fntBold hdFontColor" id="lblYourPref"></label>
													<input type="hidden" name="questionaire[0].questionKey" value="1" />
													<input type="hidden" name="questionaire[0].questionAnswer" value="" />
												</td>
											</tr>
											<tr id="trPromotionPref">
												<td colspan="4">
													<input type="hidden" name="questionaire[1].questionKey" value="2" />
													<input type="hidden" name="questionaire[1].questionAnswer" value="Y" id="promotional" />
													<input type="checkbox"  checked="checked" value="Y" id="chkQ1" class="NoBorder" />
													&nbsp;<label id="lblInfoPromotional">Yes, I would like to receive promotional material from the airline and its partners</label>
												</td>
											</tr>
											<tr><td colspan="4"><label>&nbsp;</label></td></tr>
											</table>
										</td>
									</tr>
									<tr id="tblEmailPref">
										<td>
											<table border="0" cellpadding="2" cellspacing="0" width="100%" >
												<tr>
													<td>
														<label id="lblInfoEmailFormat">How would you like to receive emails from the airline?</label>
													</td>
												</tr>
												<tr>
													<td>
														<input type="hidden" name="questionaire[2].questionKey" value="3" />
														<input type="radio" checked="checked" value="HTML" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
														<label id="lblHtml">HTML full-colour with images</label>
													</td>
													<td>											
														<input type="radio" value="PLAIN" name="questionaire[2].questionAnswer" id="radQ3" class="NoBorder"/>
														<label id="lblText">Plain text only</label>
													</td>
												</tr>
												<tr><td colspan="4"><label>&nbsp;</label></td></tr>
											</table>
										</td>
									</tr>
									
									<tr id="tblSmsPref">
										<td>
											<table border="0" cellpadding="2" cellspacing="0" width="100%" >
												<tr>
													<td>
														<label id="lblInfoSMS">Would you like to receive SMS messages from the airline?</label>
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<input type="hidden" name="questionaire[3].questionKey" value="4" />
														<input type="radio" value="Yes" name="questionaire[3].questionAnswer" id="radQ4"  class="NoBorder"/>
														<label id="lblInfoMobile">Yes</label>
													</td>
													<td colspan="2">
														<input type="radio"  checked="checked"  value="No" name="questionaire[3].questionAnswer" id="radQ4" class="NoBorder"/>
														<label id="lblNo">No</label>
													</td>
												</tr>
												<tr><td  colspan="4"><label>&nbsp;</label></td></tr>
											</table>
										</td>
									</tr>
									
									<tr id="tblLanguagePref">
										<td>
											<table border="0" cellpadding="2" cellspacing="0" width="100%" >
												<tr>
													<td colspan="4">
														<label id="lblInfoLan">Which language would you prefer to receive the communication in?</label>
														<input type="hidden" name="questionaire[4].questionKey" value="5" />
													</td>
												</tr>	
												<tr id="templateLanguages">
													<td colspan="4">
														<select id="selQ5" name="customer.selPrefLang"
															class="aa-input" style="width: 145px;" tabindex="31">
																<option value="en">English</option>
																<option value="ar">Arabic</option>
																<option value="ru">Russian</option>
																<option value="fr">French</option>
																<option value="es">Spanish</option>
																<option value="it">Italian</option>
																<option value="tr">Turkish</option>
																<option value="zh">Chinese</option></select>
													</td>
												</tr>
												<tr><td colspan="4"><label>&nbsp;</label></td></tr>
											</table>
										</td>
									</tr>
								</table>
				            </div>
				            
			          		</div>
			          		
				           <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				           <tr><td style='height: 4px'></td></tr>						
							<tr>
								<td width="60%" >&nbsp;</td>
			                    <td style="height:15px;" valign="bottom" align="right">
									
									<input name="customer.btnSave" type="button" class="Button" id="btnSave" value="Save"/>
									<input name="customer.btnRegister" type="button" class="Button" id="btnRegister"  value="Register"/>
									<input name="customer.btnReset" type="button" class="Button" id="btnReset"  value="Reset"/>
									<input name="customer.btnClose"type="button" id="btnClose" class="Button" value="Close"/>				                    
				              		
		
				                </td>
				                
			                  </tr>
			                  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			                  <tr>
								<td style="height:30px" colspan="3">
									
								</td>
								<td>
		  						<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
									<iframe src="showFile!loadMsg.action" id='frmLoadMsg' name='customer.frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
								</div>
							</td>
							</tr>

			           </table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>  
			    </td>
		    </tr>			
			</table>	
	
	<div id='lms-acount-exists-wrong-names' style='position:absolute;top:300px;left:350px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold'> &nbsp;Air Rewards account already exists</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trAncillaryOpt'>
							<td class='popBGColor'  align='center'>
								<table width='95%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td colspan="2" align='left'>This Air Rewards account already exists with a different first name and last name. 
																		<br>Please insert the correct name or use a different email address to proceed.</br> </td>
									</tr>									
								</table>
							</td>							
						</tr>
												
						<tr>
							<td class='popBGColor rowHeight' align='right'>
								<button id="wrongNamePopupOk" type="button"  class="btnMargin">OK </button>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	
	<div id='lms-acount-exists-merge' style='position:absolute;top:300px;left:350px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold'> &nbsp;Air Rewards account already exists</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trAncillaryOpt'>
							<td class='popBGColor'  align='center'>
								<table width='95%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td colspan="2" align='left'>The account with this Air Rewards ID already exists. If you want to merge please click Ok. </td>
									</tr>									
								</table>
							</td>							
						</tr>
												
						<tr>
							<td class='popBGColor rowHeight' align='right'>
								<button id="mergePopupOk" type="button"  class="btnMargin">OK </button>
								<button id="mergePopupClose" type="button"  class="btnMargin">Close </button>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<script type="text/javascript">
		<c:out value="${requestScope.reqUserRoleHtmlData}" escapeXml="false" />
	</script>
	
	<script	type="text/javascript">
		var arrPaxTitle =  new Array();
		<c:out value='${requestScope.paxTitlesVisible}' escapeXml='false' />				
	</script>
		
<!-- 	</form>	 -->
	<script src="../js/customer/customer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">

  	$(function() {
		$( "#tabs" ).tabs();
		$("#lstRoles, #lstAssignedRoles").css("width","335px");
		$("#lstRoles, #lstAssignedRoles").css("height","150px");
	});

  </script>
</div>
<%@ include file='nameUpdatePopup.jsp' %>
</body>
</html>
