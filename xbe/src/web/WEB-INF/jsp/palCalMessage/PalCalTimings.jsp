<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;	
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date	
	String StartDate =  formatter.format(dStartDate);
	
%>

<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">	
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

  </head>
  
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="<u:scroll />"
  		onload="winOnLoad(strCnfMsg, strCnfMessageType ,'<c:out value="${requestScope.warningmessage}"/>')" >
  
	<form method="post" name="frmPalCalTiming" action="showPalCalTimigs.action">
	 <%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	  <script type="text/javascript">	
			var arrError = new Array();	
			var thruchkData = new Array();										
			var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";					
	    	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			<c:out value="${requestScope.reqThruCheckData}" escapeXml="false" />
			var strCnfMsg = "<c:out value="${requestScope.reqMessage}"  escapeXml="false" />";
			var strCnfMessageType = "<c:out value="${requestScope.reqMsgType}" escapeXml="false" />";
			var strMinPALGap = "<c:out value="${requestScope.min_pnl_time}" escapeXml="false" />";
			var strMinCALInterval = "<c:out value="${requestScope.min_adl_time}" escapeXml="false" />";
			var strMinCALAfterCutoff = "<c:out value="${requestScope.min_adl_time_after_cutoff}" escapeXml="false" />";
			var maintainCALAfterCutoff = "<c:out value="${requestScope.maintain_min_adl_time_after_cutoff}" escapeXml="false" />";
			var strMinCalClausureTime = "<c:out value="${requestScope.min_cal_clausure_time}" escapeXml="false" />";
			
			
		</script>
  		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font class="Header"> PAL/CAL</font></td>
				</tr>				
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>		
		
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>PAL/CAL Timings<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table2">
							<tr></tr>
							<tr>
								<td width='9%'><font>Airport</font></td>
								<td  width='16%'>
									<select name="selFrom" id="selFrom" size="1" maxlength="30" style="width:60px" title="Airport" tabindex="1">
										<option></option>
										<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
									</select><font class="mandatory"> &nbsp;* </font>
								</td>
								<td  width='10%'><font>Flight No</font></td>
								<td  width='15%'>								
									<input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="7" style="width:70px"  onBlur="changeCase('txtFlightNoSearch',this)" tabindex="2">									
								</td>				
								
								<td align='right'>
									<input type="button" id="btnSearch"  name="btnSearch" value="Search" class="button" onclick="SearchClick()" tabindex="3">
								</td>
							</tr>																				
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class='fntTiny'>&nbsp;</font></td></tr>
				<tr>
					  		<td valign="top">
						  		<%@ include file="../common/IncludeFormTop.jsp"%>PAL/CAL Timing Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
										<tr>
										  <td style="height:5px">
										  </td>
										</tr>										
										<tr>
											<td valign="top" id="palcalTimings"></td>
										</tr>
										<tr>
								<td>
									<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<u:hasPrivilege privilegeId="xbe.tool.pal.timing.add">
												<input type="button" id="btnAdd"  name="btnAdd" class="Button" value="Add" onClick="addClick()" tabindex="4">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="xbe.tool.pal.timing.edit">
												<input type="button" id="btnEdit"  name="btnEdit" class="Button" value="Edit" onClick="editClick()" tabindex="5">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="xbe.tool.pal.timing.delete">
												<input type="button" id="btnCancel"  name="btnCancel" class="Button" value="Delete" onclick="deleteClick()" tabindex="6">
												</u:hasPrivilege>
											</td>
										</tr>
									</table>
								</td>
							</tr>
									</table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
					  		</td>
					  	</tr>
					  	
						<tr><td><font class='fntTiny'>&nbsp;</font></td></tr>
						<tr>
					  		<td valign="top">
						  		<%@ include file="../common/IncludeFormTop.jsp"%>PAL/CAL Timing<%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="55%" cellpadding="0" cellspacing="0" border="0" align="left" ID="Table8">
										<tr><td><font>&nbsp;</font></td></tr>
										<tr>
										  <td><font>Airport</font></td>
										  <td><select id="selAirport" name="selAirport" size="1" maxlength="30"  style="width:65px" onChange="dataChanged()" tabindex="7">
										  	  	<option></option>										  	  	
										  	  	<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
										  	  </select><font class="mandatory"> &nbsp;* </font>
										  </td>
										</tr>										
									 	<tr>
											<td><font>Flight No</font></td>
											<td><input type="text" id="txtFlightNo" name="txtFlightNo" style="width:65px" maxlength='7' onChange="dataChanged()" onBlur="changeCase('txtFlightNo',this)" tabindex="8"></td>
											<td><font>Or</font>
												<input type="checkbox" id="chkAllFlts" name="chkAllFlts" size="1"  class='noBorder' onChange="allSelected()" tabindex="9"><font>&nbsp;Apply To All Flights</font><font class="mandatory"> &nbsp;* </font>
											</td>
										</tr>
										<tr>
											<td><font>PAL Departure Gap (DD:HH:MM)</font></td>
											<td>
												<input type="text" id="txtPALTimeGap" name="txtPALTimeGap"  style="width:65px" maxlength='8' onChange="dataChanged()" onBlur="formatOnBlur('txtPALTimeGap')" tabindex="10"><font class="mandatory"> &nbsp;* </font>
											</td>											
										</tr>
										<tr>
											<td><font>CAL Sending Interval (DD:HH:MM)</font></td>
											<td>
												<input type="text" id="txtCALTimInterval" name="txtCALTimInterval"  style="width:65px" maxlength='8' onChange="dataChanged()" onBlur="formatOnBlur('txtCALTimInterval')" tabindex="11"><font class="mandatory"> &nbsp;* </font>
											</td>											
										</tr>
										<tr>
											<td width='42%'><font>Start Date</font></td>
											<td width='18%'><input type="text" id="txtFromDate" name="txtFromDate"  style="width:65px" maxlength='10' tabindex="12"></td>
											<td>
											<font class="mandatory"> &nbsp;* </font>
											</td>
										</tr>
										<tr>
											<td><font>Stop date</font></td>
											<td><input type="text" id="txtToDate" name="txtToDate"  style="width:65px" maxlength='10' tabindex="13"></td>
											<td>
											<font class="mandatory"> &nbsp;* </font>
											</td>
										</tr>
										<tr>
											<td><font>Last CAL Gap (DD:HH:MM)</font></td>
											<td>
												<input type="text" id="txtLstCAL" name="txtLstCAL"  style="width:60px" maxlength='8' onChange="dataChanged()" onBlur="formatOnBlur('txtLstCAL')" tabindex="14">
											</td>											
										</tr>
										<tr id="adlAfterCutoverTd">
											<td id="lblADLTimeAfterCutoffTime"><font>CAL Sending Interval After Cut-off Time (DD:HH:MM)</font></td>
											<td>
												<input type="text" id="txtCALTimeAfterCutoffTime" name="txtCALTimeAfterCutoffTime"  style="width:65px" maxlength='8' onChange="dataChanged()" onBlur="formatOnBlur('txtCALTimeAfterCutoffTime')" tabindex="18"><font class="mandatory"> &nbsp;* </font>
											</td>											
										</tr> 
										<tr>
											<td><font>Status</font></td>
											<td><input type="checkbox" id="chkStatus" name="chkStatus" size="1" class='noBorder' onChange="dataChanged()" tabindex="15"></td>
										</tr>
									</table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
					  		</td>
					  	</tr>
				
				<tr><td><font class='fntTiny'>&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
						<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
								<tr>
									<td align="left">										
										<input type="button" id="btnExit" value="Close" class="Button" NAME="btnExit" onclick="closeClick()" tabindex="15">
									</td>
									<td align="right">
										<input type="button" id="btnReset" value="Reset" class="Button" NAME="btnReset" onclick="resetClick()" tabindex="16">
										<input type="button" id="btnSave" value="Save" class="Button" NAME="btnSave" onclick="saveClick()" tabindex="17">
									</td>
								</tr>
						</table>					
						<%@ include file="../common/IncludeFormBottom.jsp"%>  	  		
					  </td>
				</tr>		
			</table>			
			
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  	<%@ include file="../common/IncludeShortCuts.jsp"%>  	
   	<%@ include file="../common/IncludeLoadMsg.jsp"%>

	<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
	<input type="hidden" name="hdnRecNo" id="hdnRecNo"  value=""/>
	<input type="hidden" name="palCalVersion" id="palCalVersion"  value=""/>	
	<input type="hidden" name="palCalId" id="palCalId"  value=""/>	
	<input type="hidden" name="hdnToday" id="hdnToday" value=<%=StartDate %>/>	
	
			
	</form>		
	    <script src="../js/palcal/palcalTimingsConfig.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		
		
  </body>
</html>