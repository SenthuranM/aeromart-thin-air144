<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Test Alert</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	</head>
 	<body>
  		<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
		<form method="post" name="frmTest" id="frmTest" action="showAlert.action">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="Header">Test Alert</font></td></tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
 						<table width="100%" cellpadding="2" cellspacing="0" border="0" align="center">
					  		<tr>
					  			<td>
								</td>
							</tr>
							<tr>
								<td>
									<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
										<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td><font>Temporary Page - Will be removed..............</font></td>
												<td width="12%" align="right"><input name="btnAlert" id="btnAlert" type="submit" class="Button"  value="Alert" style="width:120px""></td>
											</tr>
											<tr>
												<td><font>Search parameters <%= request.getParameter("hdnAlertInfo")%></font></td>
											</tr>											
										</table>					
									<%@ include file="../common/IncludeFormBottom.jsp"%>  	  		
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" name="hdnAlertInfo" id="hdnAlertInfo" value='<%= request.getParameter("hdnAlertInfo")%>'>
		</form>
		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
	</body>
</html>