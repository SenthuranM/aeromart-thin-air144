<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Pending Details</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
    <LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
   
    <script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
</head>
<body>
	<table>
		<tr id="viewFailedEmailSMSPopup" style="visibility: collapse">
			<td>	
				<table width=100%>
					<tr>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						<td></td>
						<td colspan="2" align="center">
							Pending Email & SMS Details
						</td>
						<td></td>
						<%@ include file="../common/IncludeFormHD.jsp"%>
					</tr>
					<tr><td colspan="4" height="10px"></td></tr>
					<tr><td colspan="4"><table width="100%" id="FailedEmailSMSDetails"></table></td>
					</tr>
					<tr><td colspan="4" height="10px"></td></tr>
					<tr>
						<td colspan="3"></td>
						<td align="right">
							<input id="closeBtn" type="button" value="Close" onclick="parent.opener.closePopUp()" class="btnDefault Button"/>
						</td>
					</tr>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>