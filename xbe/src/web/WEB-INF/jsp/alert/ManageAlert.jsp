<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Manage Alert</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}'escapeXml='false'  />" type="text/javascript"></script>
		<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</head>
 	<body onkeypress='return Body_onKeyPress(event)' onload="winOnLoad();" onmousewheel="return false;" onkeydown="return Body_onKeyDown(event)" scroll="no" oncontextmenu="return showContextMenu()" ondrag="return false">
  		<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
		<form method="post" name="frmAlert" id="frmAlert" action="showAlert.action">
  			<script type="text/javascript">
				var totNoOfAlerts = -1;
				var allAlerts = -1;
				var recstNo = -1;
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />				
				var reqMessageType = "<c:out value='${requestScope.reqMsgType}' escapeXml='false' />";
				var reqMessage = "<c:out value='${requestScope.reqMessage}' escapeXml='false' />";
				var reqAlertInfo = "<c:out value='${requestScope.hdnAlertInfo}' escapeXml='false' />";
				<c:out value="${requestScope.reAjax}" escapeXml="false" />	
				//top[2].ShowProgress();
			</script>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="Header">Alert Manager</font></td></tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
 						<table width="100%" cellpadding="2" cellspacing="0" border="0" align="center">
					  		<tr>
					  			<td>
									<%@ include file="../common/IncludeFormTop.jsp"%>Search<%@ include file="../common/IncludeFormHD.jsp"%>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr style="height:15px"><td colspan="9" ></td></tr>
											<tr>
												<td width="3%"><input type="radio" name="rdoAlertOption" id="rdoAlertOption1" value="alertsLatest" onclick="checkRadio()" class="NoBorder" checked></td>
												<td colspan="8"><font>Show Latest Alerts</font></td>
											</tr>	
											<tr>
												<td width="3%"><input type="radio" name="rdoAlertOption" id="rdoAlertOption2" value="alertsRange" onclick="checkRadio()" class="NoBorder"></td>
												<td width="13%"><font>Alerts From</font></td>
												<td width="10%"><input type="text" name="txtFromDate" id="txtFromDate" size="10" maxlength="10" onblur="dateChk('txtFromDate')" onchange="pageOnChange()" invalidText="true"><font class="mandatory"> &nbsp;* </font></td>
												<td width="3%" align="left"><a href="javascript:void(0)" onclick="loadCalendar(0, event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0"></a></td>
												
												<td width="5%"><font>To</font>&nbsp;&nbsp;
												<input type="text" name="txtToDate" id="txtToDate" size="10" maxlength="10" onblur="dateChk('txtToDate')" onchange="pageOnChange()" invalidText="true"><font class="mandatory"> &nbsp;* </font>
												</td>												
												<td width="4%" align="left"><a href="javascript:void(0)" onclick="loadCalendar(1, event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0"></a></td>
												<td>&nbsp;</td>
												<td width="10%">
												<td></td>												
											</tr>
														<tr><td colspan="9"><font class="fntSmall">&nbsp;</font></td></tr>
														<tr>
															<td width="3%"></td>
															<td width="7%"><font>Flight Number</font></td>
															<td width="10%"><input type="text" name="txtFlightNo" id="txtFlightNo" size="10" maxlength="10" onchange="pageOnChange()"></td>
															<!-- <td >&nbsp;</td> -->
															<td width="10%"><font>Departure Date</font></td>
															<td width="15%"><input type="text" name="txtDepDate" id="txtDepDate" size="10" maxlength="10" onblur="dateChk('txtDepDate')" onchange="pageOnChange()" invalidText="true">
															<a href="javascript:void(0)" onclick="loadCalendar(2, event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0"></a></td>
															<td width="15%"><font> Original Departure Date</font> </td>
															<td width="15%">
																<input type="text" name="txtOriDepDate" id="txtOriDepDate" size="10" maxlength="10" onblur="dateChk('txtOriDepDate')" onchange="pageOnChange()" invalidText="true">
																<a href="javascript:void(0)" onclick="loadCalendar(3, event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0"></a>
															</td>
															
															
															<td width="10%"><font>Origin Airport</font></td>
															<td width="10%"><select id="selDept" size="1" name="selDept" style="width:75px" tabindex="1" maxlength="10">
																				<option></option>
																				<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
								     										</select></td>
														</tr>
														<tr>															  
															<td colspan="2"><font>Search Option</font></td>														
															<td colspan="1"><select id="selSearchAirline" size="1" name="selSearchAirline" style="width:50px" tabindex="1" maxlength="10">																				
																				<c:out value="${requestScope.selSearchAirline}" escapeXml="false" />															
								     										</select></td>
															<td colspan="1"><font>Ancillary Reprotect Status</font></td>														
															<td colspan="1"><select id="selAnciReprotectStatus" size="1" name="selAnciReprotectStatus" style="width:80px" tabindex="1" maxlength="10">																				
																		  <option value="REPROT_ANCI_ANY">Any</option>
						             									  <option value="REPROT_ANCI_FAILED">Failed</option>
																		  <option value="REPROT_ANCI_MEAL_FAILED">Meal Failed</option>															
																		  <option value="REPROT_ANCI_SEAT_FAILED">Seat Failed</option>
								     										</select></td>
								     						<!--  
															<td colspan="2"><input type="text" name="txtResultLimit" id="txtResultLimit" size="5" maxlength="3" onkeyUp="keyValidate(0, this)" onKeyPress="keyValidate(0, this)" </td>
															-->
															<td colspan="1"><font>Flight Status</font></td>
															<td colspan="2">
																			<select name="selStatusSearch" id="selStatusSearch" size="1" name="selStatusSearch" style="width:80px" tabIndex="1" maxlenght="10">
																			<option value = "">All</option>
																			<!--<c:out value="${requestScope.sesStatus}" escapeXml="false" /> -->
																			<option value = "CNX">Cancelled</option>
																			<option value = "RESCHEDULED">Rescheduled</option>										
																			</select>
															</td>
															<td	 align="right"><input name="btnSearch" type="button" class="button" id="btnSearch" value="Search" onClick="pageBtnClick(0)"></td>
														</tr>													
														
										</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%>
								</td>
							</tr>
							<tr>
  								<td style="height:360px;" valign="top">
									<%@ include file="../common/IncludeFormTop.jsp"%>Search Results <%@ include file="../common/IncludeFormHD.jsp"%>
										<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr style="height:1px"><td >&nbsp;</td></tr>
											<tr>
												<td align="left">
													<font>
														<span id="spnTotlaRecs">																
														</span>
													</font>
												</td>
												<td align="right">
													<span id="spnSelectAll">
														<input type="checkbox" name="chkSelectAll" id="chkSelectAll" value="alertsLatest" class="NoBorder" onClick="selectAll()">&nbsp;
														<font>Select All</font>
													</span>
												</td>
											</tr>
											<tr><td colspan="2" style="height:360px;"><span id="spnAlert" style='height:350px;width:auto;overflow-X:hidden; overflow-Y:auto;'></span></td></tr>
										</table>
			  						<%@ include file="../common/IncludeFormBottom.jsp"%>
								</td>
							</tr>
							<tr>
								<td>
									<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
										<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td>&nbsp;</td>
												<td width="12%" align="right"><input name="btnClearAlert" id="btnClearAlert" type="button" class="Button"  value="Clear Alert" style="width:120px" onClick="pageBtnClick(1)"></td>
												<td width="10%" align="right"><input name="btnClose" id="btnClose" type="button" class="Button"  value="Close" onclick="closeAlert()"></td>
											</tr>
										</table>					
									<%@ include file="../common/IncludeFormBottom.jsp"%>  	  		
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" name="hdnResultsLimit" id="hdnResultsLimit" value="100">	
			<input type="hidden" name="hdnAction" id="hdnAction" value="">	
			<input type="hidden" name="hdnSelectedAlerts" id="hdnSelectedAlerts" value="">
			<input type="hidden" name="hdnAlertInfo" id="hdnAlertInfo" value="<c:out value="${requestScope.hdnAlertInfo}" escapeXml="false" />">
			<input type="hidden" name="hdnPnr" id="hdnPnr" value="">				
			<input type="hidden" name="pnrNO" id="pnrNO" value="">
			<input type="hidden" name="groupPNRNO" id="groupPNRNO" value="">
			<input type="hidden" name="airlineCode" id="airlineCode" value="">
			<input type="hidden" name="marketingAirlineCode" id="marketingAirlineCode" value="">	
			<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
			<input type="hidden" name="hdnMode" id="hdnMode" value="">	
		</form>
		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
		<%@ include file="../common/IncludeLoadMsg.jsp"%>
		
		<c:out value="${requestScope.returnData}" escapeXml="false" />	
		
		<script src="../js/alert/ManageAlert.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 	<script src="../js/alert/AlertGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">
	  	<!--
			if (allAlerts!= null && allAlerts >= 0) {
	  			DivWrite("spnTotlaRecs", "Total number of alerts in the system :&nbsp; <b>" + 
	  						allAlerts + "</b>");	  	
	  		}
	  		showAlertResponceMessage();
	  	
		  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
		  	function ClearProgressbar(){
		  			if (typeof(objDG) == "object"){
				  		if (objDG.loaded){				  			
				  			clearTimeout(objProgressCheck);
				  			//top[2].HideProgress();
				  			//showAlertResponceMessage();
							HideProgress();
							top.blnProcessCompleted=false;
				  		}
				  	}
		  	}
		 		  	 
		  	//-->
	  </script>
	</body>
</html>