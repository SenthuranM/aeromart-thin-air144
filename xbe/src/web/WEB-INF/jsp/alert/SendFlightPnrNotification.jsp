<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Send Notification</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}'escapeXml='false'  />" type="text/javascript"></script>
		<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script>
			function validateNP() {
				var nPurpose = getValue("selNotifyPurpose");
				if(nPurpose == "UPDATE" && getValue("selExistingCodes") == "-1")
				{
					showNewDepartureDetails("tableNewDepDet");
					
				}
				else {
					hideNewDepartureDetails("tableNewDepDet");
				}
				setEmailVisibilty();
				
			}
			
			
			function showNewDepartureDetails(elem){
				document.getElementById(elem).style.display="block";
			}
			
			function hideNewDepartureDetails(elem){
				document.getElementById(elem).style.display="none";
			}

			function validateNCodeDetails() {
				var selectedCode = getValue("selExistingCodes");
				if(selectedCode != -1) {
					Disable("btnGenerate",true);
					Disable("btnEdit",true);
					Disable("txtMessage",true);
					var selText = document.getElementById("selExistingCodes").options[document.getElementById("selExistingCodes").selectedIndex].text;
					setField("txtCode", selText);
					setField("hdnNtCode", selText);
					for (var k=0; k < arrNCodes.length; k++)
					{
						arrNCodes[k][1] = arrNCodes[k][1].replace(/^\s+|\s+$/g, '') ;
						if(arrNCodes[k][1] == selectedCode) {
							setField("txtMessage", arrNCodes[k][2]);
							document.getElementById('smsCharLength').innerHTML = getMessageDefaultCount(arrNCodes[k][2]);
							setField("hdnNtMessage", arrNCodes[k][2]);
							break;
						}
					}
					hideNewDepartureDetails("tableNewDepDet");
					
					if(selText.indexOf('CNX') != -1)
						setField("selNotifyPurpose", 'CANCEL');
					else if((selText.indexOf('CHG') != -1))
						setField("selNotifyPurpose", 'UPDATE');
					else
						setField("selNotifyPurpose", '-1');
				}
				else {
					Disable("btnGenerate",false);
					Disable("btnEdit",false);
					setField("txtCode", "");
					setField("txtMessage", "");
					setField("hdnNtCode", "");
					setField("hdnNtMessage", "");
					setField("selNotifyPurpose","-1");
					//validateNotifyPurpose();
				}
				selectPNRs();
			}

			
		</script>
	</head>
<body  onkeypress='return Body_onKeyPress(event)' onload="winOnLoad();" onmousewheel="return false;" onkeydown="return Body_onKeyDown(event)" scroll="no" oncontextmenu="return showContextMenu()" ondrag="return false">
  		<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
		<form method="post" name="frmNotify" id="frmNotify" action="manageFlightPnrNotification.action">
  			<script type="text/javascript">
  			var totNoOfPnr = -1;
  			var totNoOfFlights = -1;
  			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />				
			var reqMessageType = "<c:out value='${requestScope.reqMsgType}' escapeXml='false' />";
			var reqMessage = "<c:out value='${requestScope.reqMessage}' escapeXml='false' />";
			var reqLoadPnrCriteria = "<c:out value='${requestScope.hdnLoadPnrCriteria}' escapeXml='false' />";
			var reqSearchFlightCriteria = "<c:out value='${requestScope.hdnSearchFlightCriteria}' escapeXml='false' />";
			var reqChannel = "<c:out value='${requestScope.selNotifyChannel}' escapeXml='false' />";
			var strFields = "<c:out value='${requestScope.hdnFields}' escapeXml='false' />";
			var strFlightId = "<c:out value='${requestScope.hdnFlightId}' escapeXml='false' />";
			var reqSmsPnrSegmentList = new Array();
			<c:out value='${requestScope.hdnSmsPnrSegList}' escapeXml='false' />;
			
			var reqEmailPnrSegmentList = new Array();
			<c:out value='${requestScope.hdnEmailPnrSegList}' escapeXml='false' />;

			<c:out value='${requestScope.reqNotificationMsgList}' escapeXml='false' />; // Array arrNCodes will be declared here.
			
			<c:out value="${requestScope.reAjax}" escapeXml="false" />	
			//top[2].ShowProgress();
		</script>
		<div style="height:600px; overflow-y:auto;">
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="Header">Send Notifications</font></td></tr>
				<!-- <tr><td><font class="fntSmall">&nbsp;</font></td></tr> -->
				<tr><td height="5px"></td></tr>
				<tr>
					<td>
 						<table width="100%" cellpadding="2" cellspacing="0" border="0" align="center">
					  		<tr>
					  			<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<!-- <tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr> -->
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="0"
						align="center">				
						<tr>
						<td><%@ include file="../common/IncludeFormTop.jsp"%>Search Flight List<%@ include
									file="../common/IncludeFormHD.jsp"%>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td colspan="11" height="5px"><!-- <font class="fntSmall">&nbsp;</font> --></td>
									</tr>
									<tr><td colspan='11'>
										<table width="100%" border="0" cellpadding="0" cellspacing="1">
											<td style="width: 40px"><font>PNR</font></td>
											<td width="15%"><input type="text" name="txtPnr" id="txtPnr"
												size="12" maxlength="16" onchange="pageOnChange()"><font class="Mandatory">&nbsp;*</font>
											</td>
											<td> <font> OR</font></td>
											<td><font>Flight Number</font> <input type="text"
												name="txtFlightNo" id="txtFlightNo" size="10" maxlength="10"
												onchange="pageOnChange()"> <font class="Mandatory">&nbsp;*</font>
											</td>

											<td><font>From</font></td>
											<td width="10%"><input type="text" name="txtDepDateFrom"
												id="txtDepDateFrom" size="10" maxlength="10"
												onblur="dateChk('txtDepDateFrom')" onchange="pageOnChange()"
												invalidText="true"></td>
											<td align="left"><a href="javascript:void(0)"
												onclick="loadCalendar(2, event); return false;"
												title="View Calendar"><img
													src="../images/Calendar_no_cache.gif" border="0"></a></td>
											<td><font>To</font></td>
											<td width="10%"><input type="text" name="txtDepDateTo"
												id="txtDepDateTo" size="10" maxlength="10"
												onblur="dateChk('txtDepDateTo')" onchange="pageOnChange()"
												invalidText="true"></td>
											<td align="left"><a href="javascript:void(0)"
												onclick="loadCalendar(5, event); return false;"
												title="View Calendar"><img
													src="../images/Calendar_no_cache.gif" border="0"></a></td>
										
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td><hr style="border: 0; border-top: 1px solid #ccc; margin: 1em 0; padding: 0;"></td>
								</tr>
								<tr>
									<td colspan="11"><font><b>Customise Search </b>&nbsp;
									</font></td>
								</tr>
								<!-- <tr>
									<td colspan="11"><font class="fntSmall">&nbsp;</font></td>
								</tr> -->

								<tr>
									<td colspan='11'>
										<table width="100%" border="0" cellpadding="0" cellspacing="1">
											<td><font>Departure </font></td>
											<td><select id="selSegFrom" size="1" name="selSegFrom"
												style="width: 75px" tabindex="4" maxlength="10">
													<option value=""></option>
													<c:out value="${requestScope.reqStationList}"
														escapeXml="false" />
											</select></td>
											<td><font>Arrival&nbsp; </font></td>
											<td><select id="selSegTo" size="1" name="selSegTo"
												style="width: 75px" tabindex="5" maxlength="10">
													<option value=""></option>
													<c:out value="${requestScope.reqStationList}"
														escapeXml="false" />
											</select></td>

											<td><font>Specify Delay</font></td>
											<td><input type="text" name="specificDelay" onKeyUp="validateSpecificTime()"
												id="specificDelay" size="10" maxlength="8"
												placeholder="DD:HH:MM"></td>

											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr colspan="7">
														<td><font>Days of operation</font><font
															class="fntSmall">&nbsp;</font></td>
														<td><input type="checkbox" name="chk_6" id="chk_6"
															value="6" class="noBorder" align="left"
															onClick="updateDayOfoperationChange(this.id)"
															onChange="updateDayOfoperationChange(this.id)"></td>
														<td><font>Su&nbsp;</font></td>
														<td><input type="checkbox" name="chk_0" id="chk_0"
															value="0" class="noBorder" align="left"
															onClick="updateDayOfoperationChange(this.id)"
															onChange="updateDayOfoperationChange(this.id)"></td>
														<td><font>Mo&nbsp;</font></td>
														<td><input type="checkbox" name="chk_1" id="chk_1"
															value="1" class="noBorder" align="left"
															onClick="updateDayOfoperationChange(this.id)"
															onChange="updateDayOfoperationChange(this.id)"></td>
														<td><font>Tu&nbsp;</font></td>
														<td><input type="checkbox" name="chk_2" id="chk_2"
															value="2" class="noBorder" align="left"
															onClick="updateDayOfoperationChange(this.id)"
															onChange="updateDayOfoperationChange(this.id)"></td>
														<td><font>We&nbsp;</font></td>
														<td><input type="checkbox" name="chk_3" id="chk_3"
															value="3" class="noBorder" align="left"
															onClick="updateDayOfoperationChange(this.id)"
															onChange="updateDayOfoperationChange(this.id)"></td>
														<td><font>Th&nbsp;</font></td>
														<td><input type="checkbox" name="chk_4" id="chk_4"
															value="4" class="noBorder" align="left"
															onClick="updateDayOfoperationChange(this.id)"
															onChange="updateDayOfoperationChange(this.id)"></td>
														<td><font>Fr&nbsp;</font></td>
														<td><input type="checkbox" name="chk_5" id="chk_5"
															value="5" class="noBorder" align="left"
															onClick="updateDayOfoperationChange(this.id)"
															onChange="updateDayOfoperationChange(this.id)"></td>
														<td><font>Sa&nbsp;</font></td>
													</tr>
												</table>
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan='11'>
										<table width="100%" border="0" cellpadding="0" cellspacing="1">
											<td width="15%" ><font>Notification Code</font></td>
											<td width="17%"><select id="selExistingCodes" size="1"
												name="selExistingCodes" style="width: 140px" tabindex="1"
												onChange="validateNCodeDetails()">
													<option value="-1" selected></option>
													<c:out value='${requestScope.reqNotificationCodeList}'
														escapeXml='false' />
											</select></td>
											
											<td  width="10%"><font>Alert</font>
											<td  width="12%"><select id="selAlert" name="selAlert"
												onchange="clearAlertDates();">
													<option value="ANY">Any</option>
													<option value="WA">With Alert</option>
													<option value="WOA">Without Alert</option>
											</select></td></td>
											<td><font>From</font></td>

											<td width="10%"><input type="text"
												name="txtAlertFromDate" id="txtAlertFromDate" size="10"
												onblur="dateChk('txtAlertFromDate')"
												onchange="pageOnChange()" invalidText="true"></td>
											<td  align="left"><a href="javascript:void(0)"
												onclick="loadCalendar(3, event); return false;"
												title="View Calendar"><img
													src="../images/Calendar_no_cache.gif" border="0"></a></td>

											<td><font>To</font></td>
											<td width="10%"><input type="text" name="txtAlertToDate"
												id="txtAlertToDate" size="10"
												onblur="dateChk('txtAlertToDate')" onchange="pageOnChange()"
												invalidText="true"></td>
											<td align="left"><a href="javascript:void(0)"
												onclick="loadCalendar(4, event); return false;"
												title="View Calendar"><img
													src="../images/Calendar_no_cache.gif" border="0"></a></td>

										</table></td></tr>
								<tr>
									<td colspan='11'>
										<table width="100%" border="0" cellpadding="0" cellspacing="1">
											 <td width = "15%"><font>Ancillary Reprotect Status</font></td>
												<td width="12%">
												<select id="selAnciReprotectStatus" size="1" name="selAnciReprotectStatus" style="width:80px" tabindex="1" maxlength="10">
												 <option value="REPROT_ANCI_ANY">Any</option>
												 <option value="REPROT_ANCI_FAILED">Failed</option>
												 <option value="REPROT_ANCI_MEAL_FAILED">Meal Failed</option>
												 <option value="REPROT_ANCI_SEAT_FAILED">Seat Failed</option>
												 <option value="REPORT_ANCI_BAGGAGE_FAILED">Baggage Failed</option>
												    </select>
												    </td>
											<td width="17%" colspan="2">
												<table width="100%" border="0" cellpadding="0"
													cellspacing="0">

													<td width = "30%"><font>Sort By </font></td>
													<td width="60%"><select name="selSortBy" id="selSortBy" size="1"
														style="width: 90px" title="sortBy">
															<option value="DEP_DATE" selected="selected">DEPT.DATE</option>
													</select></td>

												</table>
											</td>
											<td colspan="2">
												<table width="100%" border="0" cellpadding="0"
													cellspacing="0">

													<td width = "35%"><font>Sort By </font></td>
													<td width = "65%"><select name="selSortByOrder" id="selSortByOrder"
														size="1" style="width: 80px" title="sortByOrder">
															<option value="ASC">Ascending</option>
															<option value="DESC">Descending</option>
													</select></td>

												</table>
											</td>
											<td colspan="2">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td width = "40%"><font>Connection Criteria </font></td>
														<td width = "60%">
															<select name="selConnection" id="selConnection" size="1" title="selConnection">
																	<option value="ALL">All</option>
																	<option value="WC">With Connection</option>
																	<option value="WOC">Without Connection</option>
															</select>
														</td>
													</tr>
												</table>
											</td>

											<td align="right"><input name="btnLoadFlight" type="button"
												class="button" id="btnLoadFlight" value="SEARCH"
												onClick="pageBtnClick(0)"></td>
										</table>
							</tr>
							<tr>
  								<td style="height:37px;" valign="top">
									<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" id="flightTable">
										<!-- <tr style="height:1px"><td >&nbsp;</td></tr> -->
										<tr>
										<td width="5%"></td>
										<td align="left" width="2%">
											<span id="spnFltSelectAll">
												<input type="checkbox" name="chkSelectAll" id="chkSelectAllRecord" value="" class="NoBorder" onClick="selectAllRecord()">
											</span>	</td>
										<td width="10%">
											<span id="spnFltSelectAll">
												<font>Select All</font>
											</span>	
										</td>
										<td align="left" width="83%">
											<font>
												<span id="spnTotlFlightRecs"></span>
											</font>
										</td>	
										</tr>
										<tr><td colspan="4" >
										
										<div style="width:865px;overflow-x:auto;">
											<span id="spnFlight" ></span>
										</div>
										<!--</div>-->
										</td>
  										</tr>
  										<tr>
											<td height="6" class="FormBackGround"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
										</tr>
										<tr><td align="right" colspan="4"><input name="btnLoadPnr" type="button" class="button" id="btnLoadPnr" value="View PNR List" onClick="pageBtnClick(5)" style="width:90px"></td></tr><tr>
									</table>
								</td>
								</tr>
							</table>
							<%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>

							<tr>
  								<td style="height:37px;" valign="top">
								<%@ include file="../common/IncludeFormTop.jsp"%>PNR List <%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" id="pnrTable">
										<!-- <tr style="height:1px"><td >&nbsp;</td></tr> -->
										<tr>
											<td width="55px"></td>
											<td width="15px" align="left">
												<span id="spnPnrSelectAll">
													<input type="checkbox" name="chkSelectAll" id="chkSelectAll" value="" class="NoBorder" onClick="selectAll()">
												</span>	
											</td>
											<td width="45px">
												<span id="spnPnrSelectAll">
													<font>Select All</font>
												</span>	
											</td>
											<td width="40px"></td>
											<td align="left">
												<font>
													<span id="spnTotlaRecs"></span>
												</font>
											</td>	
										</tr>
										<tr>
											<td colspan="11" >
												<div style="width:865px;overflow-x:auto;">
													<span id="spnPNR" ></span>
												</div>
											</td>
										</tr>
										<tr>
											<td height="6" class="FormBackGround"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
										</tr>
										<tr>
											<td colspan="11">
												<font><b>Re-Cap of Passenger Contacts </b>&nbsp;</font>
											</td>
										</tr>
			                        	<tr>
			                        		<td colspan="3" width="115px" align="left">
			                        			<label class="GridItem" id="lblSelLoc" style="visibility:hidden;">Select Location</label>
			                        		</td>
			                        		<td colspan="8">
			                        			<div style="width:750px;overflow-x:auto;">
					                        		<table id="tablePaxContact" cellpadding="0" cellspacing="0" border="0" align="center">
					                        			<tr><td height="5px"></td></tr>
					                        			<tr id="spnPaxContactsDetails"></tr>
					                        			<tr><td height="5px"></td></tr>
					                        		</table>
				                        		</div>
			                        		</td>
			                        	</tr>
									</table>
			  						<%@ include file="../common/IncludeFormBottom.jsp"%>
								</td>
							</tr>
							<tr>
								<td>
									<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
										<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td width="15%"><font>Notification Purpose</font></td>
												<td><select id="selNotifyPurpose" size="1" name="selNotifyPurpose" style="width:125px" tabindex="1" onchange="validateNP()">
												<option value="-1" selected></option>
													<option value="UPDATE">Flight Time Change</option>
													<option value="CANCEL">Flight Cancellation</option>
													<option value="REPROTECT">Re-Protected</option>													
	     										</select><font class="Mandatory">&nbsp;*</font></td>
	     										<td colspan="4">
	     										<table id = "tableNewDepDet" width = "100%" cellpadding="0" cellspacing="0" border="0" align="center" style="display:none;">
	     										<tr>
	     										<td><font>New Dep. Date/Time</font></td>
	     										<td ><input type="text" name="txtNewDepDate" id="txtNewDepDate" size="10" maxlength="10" onblur="dateChk('txtNewDepDate')" onchange="pageOnChange()" invalidText="true"></td>
												<td align="left" ><a href="javascript:void(0)" onclick="loadCalendar(1, event); return false;" title="View Calendar" id="calNewDepDate"><img src="../images/Calendar_no_cache.gif" border="0"></a></td>
												<td>&nbsp;</td>
												<td><input type="text" name="txtDepTime1" id="txtDepTime1" maxlength="5" style="width:50px" onblur="setTimeWithColon('txtDepTime1', document.forms[0].txtDepTime1.value)"><font class="Mandatory">&nbsp;*</font></td>
												<tr>
												</table>
												</td>
					   							
					   						</tr>
					   						<tr>
					   						<td width="15%"><font>Notification Channel</font></td>
											<td><select id="selNotifyChannel" size="1" name="selNotifyChannel" style="width:125px" tabindex="1" onchange="selectPNRs()">
													<option value="-1" selected></option>
													<option value="1">Sms Only</option>
													<option value="2">Email Only</option>
													<option value="3">Sms + Email</option>
	     										</select><font class="Mandatory">&nbsp;*</font></td>
	     									<!--<td colspan="4"><font>
	     										<span id="spnEmail"> 
	     											<input type="checkbox" name="chkItinerary" value="ITINERARY" id="chkItinerary" onchange="pageOnChange()" >
	     											&nbsp; Attach Itinerary
	     										</span></font></td>-->
	     									<!-- <td><font>
	     										<span id="spnEmail"> 
	     											<input type="checkbox" name="chkItinerary" value="ITINERARY" id="chkItinerary" onchange="pageOnChange()" >
	     											&nbsp; Attach Itinerary
	     										</span></font>
	     									</td> -->
	     									<td align="right">
	     										Language&nbsp;&nbsp;
	     									</td>
	     									<td colspan="2">
	     										<select name="selGenLanguage" id="selGenLanguage" style="width: 100px">
	     											<option value="" selected="selected">Please select a language</option>
													<c:out value='${requestScope.reqPrefLanguageList}' escapeXml='false'/>
	     										</select>
	     										<font class="Mandatory">&nbsp;*</font>
	     									</td>	     									
	     									<td width="10%" align="right"><input name="btnGenerate" id="btnGenerate" type="button" class="Button"  value="Generate" onclick="pageBtnClick(1)"></td>
	     									</tr>
	     									</table>					
											<%@ include file="../common/IncludeFormBottom.jsp"%> 
									</td>
								</tr>
								
								<tr>
								<td>
									<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
									<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
     									<tr>
	     									<td>
	     										<font>Notification Code</font>
												<input type="text" name="txtCode" id="txtCode" size="20" maxlength="20" onchange="pageOnChange()">
											</td>
											<td id="emailSub">
												<font>Subject<font class="fntSmall">(Email Only)</font></font>
												<input type="text" name="txtSubject" id="txtSubject" size="30" maxlength="200" onchange="pageOnChange()">
											</td>
											<td><font>
	     										<span id="spnEmail">
	     											<input type="checkbox" name="chkItinerary" value="ITINERARY" id="chkItinerary" onchange="pageOnChange()" >
	     											&nbsp; Attach Itinerary
	     										</span></font>
	     									</td>
										</tr>
										<tr><td height="10px"></td></tr>
										<tr>
											<td colspan="2"><font>Generated Message</font>
												<font>(Length:<span style="align:right" id="smsCharLength">0</span>)</font>
												<br/>
												<textarea name="txtMessage" id="txtMessage" cols=85 rows=3  onchange="pageOnChange()" onkeyup="getMessageLength(this)">
														<c:out value='${requestScope.txtMessage}' escapeXml='false' />
												</textarea>
											</td>
											<td>
						   						<u:hasPrivilege privilegeId="xbe.notification.edit">
						   							<input name="btnEdit" id="btnEdit" type="button" class="Button" style="width:100px"  value="Edit Message" onclick="pageBtnClick(2)">
						   						</u:hasPrivilege>
						   						<br/><br/>
						   						<input name="btnViewMsg" id="btnViewMsg" type="button" class="Button" style="width:100px"  value="View Message" onclick="pageBtnClick(4)">
					   						</td>
					   						<td width="10%" align="right">
					   							<input name="btnSend" id="btnSend" type="button" class="Button"  value="Send" onclick="pageBtnClick(3)">
					   							<br/><br/>
					   							<input name="btnClose" id="btnClose" type="button" class="Button"  value="Close" onclick="closeNotification()">
					   						</td>
										</tr>
									</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%> 
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
<input type="hidden" name="hdnResultsLimit" id="hdnResultsLimit" value="100">	
<input type="hidden" name="hdnAction" id="hdnAction" value="">	
<input type="hidden" name="hdnSelectedPnr" id="hdnSelectedPnr" value="">
<input type="hidden" name="hdnLoadPnrCriteria" id="hdnLoadPnrCriteria" value="<c:out value="${requestScope.hdnLoadPnrCriteria}" escapeXml="false" />">
<input type="hidden" name="hdnSearchFlightCriteria" id="hdnSearchFlightCriteria" value="<c:out value="${requestScope.hdnSearchFlightCriteria}" escapeXml="false" />">

<input type="hidden" name="hdnFlightNoDepDate" id="hdnFlightNoDepDate" value="">
<input type="hidden" name="hdnOverrideMsgLength" id="hdnOverrideMsgLength" value="">
<input type="hidden" name="excludedDays" id="excludedDays" value="">
<input type="hidden" name="hdnPnr" id="hdnPnr" value="">				
<input type="hidden" name="pnrNo" id="pnrNo" value="">	
<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
<input type="hidden" name="hdnMode" id="hdnMode" value="<c:out value='${requestScope.hdnMode}' escapeXml='false' />">	
<input type="hidden" name="hdnFlightNo" id="hdnFlightNo" value="">	
<input type="hidden" name="hdnDepDate" id="hdnDepDate" value="">	
<input type="hidden" name="hdnFlightId" id="hdnFlightId" value="">	
<input type="hidden" name="hdnSmsPnrSegList" id="hdnSmsPnrSegList" value="">	
<input type="hidden" name="hdnEmailPnrSegList" id="hdnEmailPnrSegList" value="">	
<input type="hidden" name="flightPnrDetailDTO" id="flightPnrDetailDTO">	
<input type="hidden" name="hdnNtCode" id="hdnNtCode">
<input type="hidden" name="hdnNtMessage" id="hdnNtMessage">
<input type="hidden" name="hdnFields" id="hdnFields" value="<c:out value="${requestScope.hdnFields}" escapeXml="false" />">
<input type="hidden" name="hdnPnrLoad" id="hdnPnrLoad">

</form>
		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
		<%@ include file="../common/IncludeLoadMsg.jsp"%>
		
		<c:out value="${requestScope.returnData}" escapeXml="false" />	
			 	
	 	<script src="../js/alert/ManageFlightPnrNotifications.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 	<script src="../js/alert/SendFlightPnrNotificationGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 	<script src="../js/alert/SendFlightDetailsNotificationGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 	
	 	<script type="text/javascript">
	  		<!--

	  		if (totNoOfPnr != null && totNoOfPnr >= 0) {
	  			DivWrite("spnTotlaRecs", 'Total number of PNRs :&nbsp; <b>' + totNoOfPnr + '</b>');	  	
	  		}
	  		
	  		if (totNoOfFlights != null && totNoOfFlights >= 0) {
	  			DivWrite("spnTotlFlightRecs", 'Total number of flights :&nbsp; <b>' + totNoOfFlights + '</b>');
	  		}
	  		
	  		showNotificationResponseMessage();
  		 	
		  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
		  	function ClearProgressbar(){
		  			if (typeof(objDG) == "object"){
				  		if (objDG.loaded){				  			
				  			clearTimeout(objProgressCheck);
				  			//top[2].HideProgress();
				  			//showAlertResponceMessage();
							HideProgress();
							top.blnProcessCompleted=false;
				  		}
				  	}
		  	}
		 		  	 
		  	//-->
	  </script>
	  
</body>
</html>
