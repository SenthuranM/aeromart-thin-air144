<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
    <LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
    <script>
    	function validatePhone(num){
    		var str = num.value;
    		str = str.replace(/[^0-9]+/g, '');
    		num.value = str;
    	}
    </script>
    <script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
</head>
<body>
    <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
    <h3>Update Contact Details</h3>
    <table style="with: 100%">
        <tr>
            <td>PNR</td>
            <td><input type="text" name="pnr" id="txtpnr" disabled size="21"/></td>
        </tr>
        <tr>
            <td>Mobile</td>
			<td><input name="txtMobile1" id="txtMobile1" onkeyup="validatePhone(this)" size="1" type="text" maxlength="4">
  				-<input name="txtMobile2" id="txtMobile2" onkeyup="validatePhone(this)" size="1" type="text" maxlength="4">
 				-<input name="txtMobile3" id="txtMobile3" onkeyup="validatePhone(this)" size="3" type="text" maxlength="10"> </td> 
 		</tr>
        <tr>
            <td>Land Line</td>
			<td><input name="txtLand1" id="txtLand1" onkeyup="validatePhone(this)" size="1" type="text" maxlength="4">
 			-<input name="txtLand2" id="txtLand2" onkeyup="validatePhone(this)" size="1" type="text" maxlength="4">
 			-<input name="txtLand3" id="txtLand3" onkeyup="validatePhone(this)" size="3" type="text" maxlength="10"></td>
         </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="txtEmail" id="txtEmail" size="21"/></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td align="right">
                <input name="btnUpdateConfirm" type="button" class="Button" id="btnUpdateConfirm" value="Update"
                       title="Update Reservation Contact Details" onClick="parent.opener.sendUpdateContactData()">
            </td>
        </tr>
    </table>
</body>
</html>