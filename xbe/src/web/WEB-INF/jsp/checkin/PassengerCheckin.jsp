<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Passenger Checkin</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		<link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}'escapeXml='false'  />" type="text/javascript"></script>
		<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</head>
	<body  onkeypress='return Body_onKeyPress(event)' onload="winOnLoad();" onmousewheel="return false;" onkeydown="return Body_onKeyDown(event)" scroll="no" oncontextmenu="return showContextMenu()" ondrag="return false">
  		<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
		<form method="post" name="frmPassengerCheckin" id="frmPassengerCheckin" action="showPassengerCheckin.action">
  			<script type="text/javascript">
  			var totalNoOfRecords = "<c:out value="${sessionScope.totalNoOfRecords}" escapeXml="false" />";
  			var flightNumber = "<c:out value="${requestScope.flightNumber}" escapeXml="false" />";
  			var fltId = "<c:out value="${requestScope.fltId}" escapeXml="false" />";
  			var flightDateTime = "<c:out value="${requestScope.flightDateTime}" escapeXml="false" />";
  			var flightStatus = "<c:out value="${requestScope.flightStatus}" escapeXml="false" />";
  			var flightCheckinStatus = "<c:out value="${requestScope.flightCheckinStatus}" escapeXml="false" />";
  			var airportCode = "<c:out value="${requestScope.airportCode}" escapeXml="false" />";
  			var arrCheckinPassengers = new Array();
  			var arrCOS = "<c:out value="${sessionScope.reqCOS}" escapeXml="false" />";
  			var arrLoad = "<c:out value="${sessionScope.reqLoad}" escapeXml="false" />";
  			var modleCapacities = "<c:out value="${requestScope.modleCapacities}" escapeXml="false" />";
  			var flightSegId = "<c:out value="${requestScope.flightSegId}" escapeXml="false" />";
  			
  			<c:out value="${requestScope.reqCheckinPassengerData}" escapeXml="false" />
		</script>
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td>
						<table width="100%" cellpadding="2" cellspacing="0" border="0" align="center">
				  		<tr>
				  			<td>
								<%@ include file="../common/IncludeFormTop.jsp"%>Flight Details<%@ include file="../common/IncludeFormHD.jsp"%>																
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr><td><span id="spnFlightDetails" name="spnFlightDetails"></span></td></tr>
										<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
										<tr><td><span id="spnPaxLoad" name="spnPaxLoad"></span></td></tr>
										<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
										<tr><td><span id="spnLoadStatus" name="spnLoadStatus"></span></td></tr>
									</table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>
						
						<tr>
 							<td style="height:250px;" valign="top">
							<%@ include file="../common/IncludeFormTop.jsp"%>PAX Details <%@ include file="../common/IncludeFormHD.jsp"%>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
									<tr style="height:1px"><td >&nbsp;</td></tr>
									<tr>
										<td align="left">
											<span id="spnView"><font>View</font></span>
										</td>
										<td align="left">
											<span id="spnSelectAll"><font><a href="#" onclick="javascript:selectAll()">Select All</a></font></span>
										</td>
										<td align="right">
											<span id="spnSelectStandBy"><font><a href="#" onclick="javascript:selectAllPax()">All</a></font></span>	
										</td>
										<td align="right">
											<span id="spnSelectCheckin"><font><a href="#" onclick="javascript:selectCheckedin()">Checked-in</a></font></span>	
										</td>
										<td align="right">
											<span id="spnSelectToBeCheckin"><font><a href="#" onclick="javascript:selectToBeCheckedin()">To Be Checked-in</a></font></span>	
										</td>
										<td align="right">
											<span id="spnSelectStandBy"><font><a href="#" onclick="javascript:selectStandBy()">Standby</a></font></span>	
										</td>
										<td align="right">
											<span id="spnSelectGoshow"><font><a href="#" onclick="javascript:selectGoshow()">Goshow</a></font></span>	
										</td>
										<td align="right"></td>
									</tr>
									<tr><td colspan="7" ><span id="spnCheckinPassengers" ></span></td></tr>
									<tr style="height:1px"><td >&nbsp;</td></tr>
				   					<tr>
     									<td align="left" colspan="2"><input name="btnBack" id="btnBack" type="button" class="Button"  value="Back" onclick="clickBack()"></td>
     									<c:if test='${requestScope.reqViewOnly == "0"}'>
	     									<td align="right" colspan="5">
		     									<input name="btnCheckIn" id="btnCheckIn" type="button" class="Button"  value="Check-in" onclick="checkin()" style="width: 70px;">
		     									<input name="btnUnCheckIn" id="btnUnCheckIn" type="button" class="Button"  value="Undo check-in" onclick="unCheckin()"  style="width: 120px;">
		     									<input name="btnAddGoShow" id="btnAddGoShow" type="button" class="Button"  value="Add GoShow" onclick="addGoshow()" title="To Add Infant GoShow, First select an Adult by clicking on Adult Row." style="width: 100px;">
		     									<input name="btnAddComment" id="btnAddComment" type="button" class="Button"  value="Add Comment" onclick="addCheckinNote()" style="width: 100px;">
		     									<input name="btnViewComment" id="btnViewComment" type="button" class="Button"  value="View Comment" onclick="viewCheckinNote()" style="width: 100px;">
	     									</td>
     									</c:if>
     								</tr>
     							</table>					
								<%@ include file="../common/IncludeFormBottom.jsp"%> 
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdnFltId" id="hdnFltId"  value=""/>
		<input type="hidden" name="hdnAirportCode" id="hdnAirportCode"  value=""/>
		<input type="hidden" name="hdnPPFSIds" id="hdnPPFSIds"  value=""/>
		</form>
		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
		<%@ include file="../common/IncludeLoadMsg.jsp"%>
					 	
	 	<script src="../js/checkin/CheckinPassengers.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 	<script src="../js/checkin/CheckinPassengersGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

</body>
</html>
