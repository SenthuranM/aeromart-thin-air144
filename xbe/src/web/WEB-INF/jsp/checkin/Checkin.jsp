<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.DATE,1);
	dStopDate = cal.getTime(); //Next Month Date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%>
<html>
  <head>
    <title>AccelAero Check-in</title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script  src="../js/common/tooltipnew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript">			
		var arrError = new Array();
		var flightData = new Array();
		var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
    	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		<c:out value="${requestScope.reqFlightData}" escapeXml="false" />
		var defCarrCode = "<c:out value="${requestScope.reqcrriercode}" escapeXml="false" />";
		var strMsg = "<c:out value="${requestScope.reqMessage}"/>";
	</script>		
  </head>
  
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="<u:scroll />"
  		onload="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>' ,'<c:out value="${requestScope.warningmessage}"/>')" >
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<form method="post" name="frmCheckin" id="frmCheckin" action="showCheckin.action">
  		<table width="99%" border="0" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>				
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Flight Search<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table2">
						<tr>
							<td colspan="2"><font>Start Date - local</font></td>								
							<td colspan="2"><font>Stop Date - Local</font></td>								
							<td><font>Flight No</font></td>
							<td><font>From</font></td>
							<td><font>To</font></td>
							<td><font>Flight Status</font></td>
							<td><font>Checkin Status</font></td>								
						</tr>
						<tr>
							<td style="width:75px;">
								<input type="text" name="txtStartDateSearch" id="txtStartDateSearch"  invalidText="true"  onBlur="dateChk('txtStartDateSearch')" value=<%=StartDate %> style="width:70px;"  maxlength="10" tabIndex="1">
							</td>
							<td>
								<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar" tabIndex="2"><img src="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font>
							</td>
							<td style="width:75px;">
								<input type="text" name="txtStopDateSearch" id="txtStopDateSearch" invalidText="true" onBlur="dateChk('txtStopDateSearch')" value=<%=StopDate %> style="width:70px;" maxlength="10" tabIndex="3">
							</td>
							<td>
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0" tabIndex="4"></a><font class="mandatory"> &nbsp;* </font>
							</td>								
							<td>
								<input type="text" id="txtFlightNoStartSearch" name="txtFlightNoStartSearch" maxlength="2" style="width:20px" value="" tabIndex="7">									
								<input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="5" style="width:50px" tabIndex="8"><font class="mandatory"> &nbsp;* </font>									
							</td>
							<td>
								<select name="selFromStn6" id="selFromStn6" size="1" maxlength="30" style="width:60px" title="Origin" tabIndex="9">
									<option></option>
									<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
								</select><font class="mandatory"> &nbsp;* </font>
							</td>
							<td>
								<select name="selToStn6" id="selToStn6" size="1" maxlength="30" style="width:60px" title="Destination" tabIndex="10">										
									<option></option>
									<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
								</select>
							</td>							
							<td>
								<select name="selStatusSearch" id="selStatusSearch" size="1" maxlength="20" title="Flight Status" tabIndex="12">
									<c:out value="${sessionScope.sesStatus}" escapeXml="false" />
									<option>All</option>										
								</select><font class="mandatory"> &nbsp;* </font>
							</td>
							<td>
								<select name="selCheckinStatusSearch" id="selCheckinStatusSearch" size="1" maxlength="30" tabIndex="11">
									<option>All</option>
									<c:out value="${sessionScope.sesCheckinStatus}" escapeXml="false" />									
								</select><font class="mandatory"> &nbsp;* </font>
							</td>
							<td></td>
							<td align="right">
								<input type="button" id="btnSearch"  name="btnSearch" value="Search" class="button" onclick="SearchData()" tabIndex="13">
							</td>
						</tr>
						<tr>
							<td colspan="11"></td>								
						</tr>

					</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Flight Details<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
						<tr>
							<td style="height:5px"></td>
						</tr>								
						<tr>
							<td valign="top"><span id="spnInstances"></span></td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
						<tr>
							<td align="left">
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin,xbe.checkin.flight.agent"><input type="button" id="btnView" value="View" class="Button" NAME="btnView" onclick="checkinFunctions(5)" style="visibility: hidden; width: 40px;" ></u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin,xbe.checkin.flight.agent"><input type="button" id="btnOpenForCheckin" value="Open For Checkin" class="Button" NAME="btnOpenForCheckin" onclick="checkinFunctions(0)" style="visibility: hidden; width: 120px;" ></u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin"><input type="button" id="btnStartCheckin" value="Start Checkin" class="Button" NAME="btnStartCheckin" onclick="checkinFunctions(1)" style="visibility: hidden; width: 85px;" ></u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin"><input type="button" id="btnCompleteCheckin" value="Complete Checkin" class="Button" NAME="btnCompleteCheckin" onclick="checkinFunctions(2)" style="visibility: hidden; width: 110px;" ></u:hasPrivilege>						
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin"><input type="button" id="btnCloseForCheckin" value="Close For Checkin" class="Button" NAME="btnCloseForCheckin" onclick="checkinFunctions(3)" style="visibility: hidden; width: 120px;" ></u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin"><input type="button" id="btnReopenCheckin" value="Re-Open For Checkin" class="Button" NAME="btnReopenCheckin" onclick="checkinFunctions(4)" style="visibility: hidden; width: 120px;" ></u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin,xbe.checkin.flight.agent"><input type="button" id="btnAddComment" value="Add Comment" class="Button" NAME="btnAddComment" onclick="addCheckinNote()" style="visibility: hidden; width: 100px;" ></u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.checkin.flight.admin,xbe.checkin.flight.agent"><input type="button" id="btnViewComment" value="View Comment" class="Button" NAME="btnViewComment" onclick="viewCheckinNote()" style="visibility: hidden; width: 100px;" ></u:hasPrivilege>
								<input type="button" id="btnClose" value="Close" class="Button" NAME="btnClose" onclick="top.LoadHome()" style="width: 40px;" >
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>		
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
		<input type="hidden" name="hdnCheckinMode" id="hdnCheckinMode"  value=""/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo"  value=""/>
		<input type="hidden" name="hdnFlightId" id="hdnFlightId"  value=""/>
		<input type="hidden" name="hdnAirportCode" id="hdnAirportCode"  value=""/>
		<input type="hidden" name="hdnFlightSegId" id="hdnFlightSegId"  value=""/>
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
	<script src="../js/checkin/Checkin.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/checkin/CheckinGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
	<script type="text/javascript">  	
  		var objProgressCheck = setInterval("ClearProgressbar()", 300);
		function ClearProgressbar() {
	    	if (objDG.loaded) {
				clearTimeout(objProgressCheck);
				HideProgress();
			}
		}
  	</script>
  	<%@ include file="../common/IncludeLoadMsg.jsp"%>
  	
  </body>
</html>
