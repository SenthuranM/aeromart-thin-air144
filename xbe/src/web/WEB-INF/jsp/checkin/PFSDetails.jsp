<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>PFS Processing</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">

<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</head>
<body scroll="no" scroll="no" oncontextmenu="return false"
	onkeypress='return Body_onKeyPress(event)'
	onkeydown="return Body_onKeyDown(event)" ondrag='return false'
	onUnload="resetPFS()"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

<%@ include file="../common/IncludeWindowTop.jsp"%>

<form method="post" action="showPFSDetailsProcessing.action" id="frmPFSDetail">
<script type="text/javascript">
	var arrTitle=new Array();				
	var paxTypes = new Array();
	<c:out value="${requestScope.reqPaxTitles}" escapeXml="false" />
	<c:out value="${requestScope.reqPaxType}" escapeXml="false" />
</script>
<font class="fntSmall">&nbsp;</font>
<%@ include file="../common/IncludeFormTop.jsp"%>
		Add/Modify PFS Details <%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="98%" border="0" cellpadding="0" cellspacing="1" align="center" ID="Table1">
			<tr>
				<td>
				<table width="98%" border="0" cellpadding="0" cellspacing="1" align="left" ID="Table1">
					<tr>
						<td width="100px;" align="left"><font>Pax Title</font></td>
						<td><select name="selTitle" id="selTitle"
							size="1" style="width: 75;" tabindex="1">
						</select></td>
					</tr>
					<tr>
						<td><font>Pax Type</font></td>
						<td><select name="selPaxType" id="selPaxType"
							size="1" style="width: 75;" tabindex="2">
						</select></td>
						<td>&nbsp;</font></td>
					</tr>
					<tr>
						<td><font>Pax First Name</font></td>
						<td><input name="txtFirstName" type="text"
							id="txtFirstName" maxlength="50" size="30" tabindex="3"><font
							class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Pax Last Name</font></td>
						<td><input name="txtLastName" type="text" id="txtLastName"
							maxlength="50" size="30" tabindex="4"><font
							class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Status</font></td>
						<td><select name="selAddStatus"
							id="selAddStatus" size="1" style="width: 105;" tabindex="5">
						</select></td>
					</tr>

					<tr>
						<td><font>Destination</font></td>
						<td><select tabindex="6" name="selDest" id="selDest">
							<option value=""></option>
							<c:out value="${requestScope.reqAirportCombo}" escapeXml="false" />

						</select></td>
					</tr>
					<tr>
						<td><font>Class of Service</font></td>
						<td><select name="selCC" id="selCC" tabindex="7">
							<c:out value="${requestScope.reqCabinClassList}"
								escapeXml="false" />
						</select><font class="mandatory">&nbsp;*</font></td>

					</tr>
					<tr>
						<td><font>Pax Category</font></td>
						<td><select name="selPaxCat" id="selPaxCat" tabindex="8">
							<c:out value="${requestScope.reqPaxCatList}" escapeXml="false" />
						</select></td>
					</tr>
				</table>

				</td>
			</tr>
			<tr>
				<td style="height: 42px;" colspan="2" valign="bottom">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<input tabindex="9" type="button" id="btnClose" class="Button" value="Close" onclick="closeWindow()"> 
							<input tabindex="10" name="btnReset" type="button" class="Button" id="btnReset" onClick="resetPFS()" value="Reset">
						</td>
						<td align="right">
							<input tabindex="11" name="btnSave"	type="button" class="Button" id="btnSave" onClick="processSavePFS()" value="Save"
								style="width: 70px;">
						</td>
						<td>&nbsp;&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>

		<%@ include file="../common/IncludeFormBottom.jsp"%>

<%@ include file="../common/IncludeWindowBottom.jsp"%>
<input type="hidden" name="hdnMode" id="hdnMode"/>	
<input type="hidden" name="hdnFlightSegId" id="hdnFlightSegId" value=""/>
<input type="hidden" name="hdnNewFlightSegId" id="hdnNewFlightSegId" value=""/>
<input type="hidden" name="hdnFltId" id="hdnFltId"/>
<input type="hidden" name="hdnPaxPnr" id="hdnPaxPnr" value=""/>
<input type="hidden" name="hdnPaxType" id="hdnPaxType" value=""/>

<%@ include file="../common/IncludeLoadMsg.jsp"%>

</form>
</body>
<script	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../js/checkin/PfsValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>

</html>

