 <%-- 
	 @Author 	: 	Menaka P. Wedige
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Print - <%=request.getParameter("strMsgType")%> </title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">


	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<!--script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script-->	
	<!--script src="../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script-->

  </head>

  <body scroll="yes" class="tabBGColor" oncontextmenu="return false" ondrag='return false'  onbeforeprint="winOnBeforePrint()" onafterprint="winOnAfterPrint()"
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
  			<script type="text/javascript">
			//	var strPNLADLText=opener.pnlAdlText;
				<c:out value="${requestScope.reqPNLADLTextData}" escapeXml="false" />
				<c:out value="${requestScope.reqIsExceptionOccured}" escapeXml="false" />
			</script>
		  <form method="post" >
				<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td align="left" colspan="2"><font><span id="strPNLADLText"></span>
								<!--script type="text/javascript">
									alert("pnlAdlText1.... "+strPNLADLText);
								</script-->
								</font></td>

							</tr>
							<tr>
								<td width="50%" align="left"><input tabindex="1" type="button" id="btnClose" class="Button" value="Close" onclick="window.close()"></td>	
								<td width="50%" align="right"><input tabindex="2" name="btnPrint" type="button" class="button" id="btnPrint" onClick="printPNLADLText()"  value="Print"></td>
							</tr>
					  	</table>
				  		<!--%@ include file="../common/IncludeFormBottom.jsp"%-->
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
	</form>
  </body>
      <script type="text/javascript">
   <!--
//	var strPNLADLText= 'PNLG92006/23FEB<br>CMB PART1-KHI02BC7771AAAAAAAA/AAAAAAAAMR-A2<br>.R/TKNA L28XHN1BBBBBBB/BBBBBBBBMS-A2 .L/L28XHN.R/TKNA L28XHN-KHI/DOH03BC7771AAAAAAAAAAAAA/AAAAAAAAAMR-A3 .R/TKNA3CY8II1BBBBBB/BBBBBBBBBMR-A3.R/TKNA3CY8II1CCCCCCCCCC/CCCCCCCCMS-A3 .L/3CY8II .R/TKNA 3CY8IIENDPNL';

	function printPNLADLText(){
		window.print();	
	}

	//On Page loading make sure that all the page input fields are cleared
	function winOnLoad(strMsg,strMsgType) {
		//alert(isException+"----chlid isException");
		if(isException == 'false' ){
			document.getElementById("strPNLADLText").innerHTML=pnlAdlText;
			getFieldByID("btnPrint").focus();
		}else{
			if(strMsg != null && strMsgType != null)
				opener.showCommonError(strMsgType,strMsg);
			windowClose();
		}

	}

	function windowClose(){
		window.close();
	}

	function winOnBeforePrint(){
		hideButtons(true);
	}

	function winOnAfterPrint(){
		hideButtons(false);
	}

	function hideButtons(bln){
	//	visible("btnPrint",!bln);
	//	visible("btnClose",!bln);
		setVisible("btnPrint",!bln);
		setVisible("btnClose",!bln);
	}
	
   //-->
  </script>
</html>