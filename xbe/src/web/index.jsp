<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
String screenWidth = (String)session.getAttribute("sesScreenWidth");
String closeMe = System.getProperty("com.isa.thinair.xbe.close.indexpage");


if(screenWidth == null){
	screenWidth = request.getParameter("screenWidth");

	if(screenWidth != null){
		session.setAttribute("sesScreenWidth", screenWidth);
	}
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="-1"/>
    <meta name="robots" content="noindex" />
	<title>AccelAero</title>

<LINK rel="stylesheet" type="text/css" href="css/Style_no_cache.css">
<script src="js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</head>
<body oncontextmenu="return false" class="PageBackGround">
<div id="window" style="display: none;position: absolute;top: 0%;left: 0%;width: 100%;height: 100%;background-color: black;z-index:1001;-moz-opacity: 0.75;opacity:.75;filter: alpha(opacity=75);"></div>
<OBJECT ID="WB" WIDTH=0 HEIGHT=0 
    CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2">
</OBJECT>
<center>
	<font face="verdana" size="2">
		<!-- 
		<a href="javascript:void(0)" onclick="fullScreen(0)">full Screen</a>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:void(0)" onclick="fullScreen(1)">Child window
		-->
	</font>
</center>
<br><br><br><br><br><br><br>
<center>
	<table cellpadding="5">
		<tr>
			<td align="center">
				<img id="imgSysName" src="images/spacer_no_cache.gif">
			</td>
		</tr>		
		<tr>
			<td align="center">
			<a href="#" onClick="showFullScreen()"><u><font class="fntBold">If pop-up blocked, Click here to login</font></u></a></td>
		</tr>
		<tr>
			<td align="center"><font class="fntBold">Compatible Browsers -- Mozilla Firefox -V 3.5+, Internet Explorer - V 7+,Google Chrome <br>
									Recommended for best performance --  Mozilla Firefox -V 3.5+ ,Google Chrome </font></td>
		</tr>
		<tr>
		<td>
		<div style="position: absolute; z-index: 1002; display: block; overflow-y: auto; background-color: white; left: 400px; width: 600px; top: 100px; height: 200px;" id="message">
			<table width = "600px" height="200">
				<tr><td colspan = "2" align="center"> <font class="fntBoldAndRed">Your computer does not meet the system requirements. </font></td></tr>
				<tr><td colspan = "2" align="center"><font class="fntBoldAndRed">Please do the necessary changes.</font></td></tr>
				<tr><td width="40">&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;<font class="fntBold">System requirements:<font></td></tr>
				<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;Browser : Microsoft Internet Explorer above 6 and Firefox above 3.5.0 .</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;Screen Resolution : 800 x 600 pixels or above.</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;Pop-up windows - Turn off popup blocking.</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp; <font class="fntBold">Download and use latest FireFox for compatibility and fast access from <a href="http://www.mozilla.com/en-US/firefox/new/">here</font> <img id="firefox" src="images/firefox_no_cache.png" width="24px" height="24px"></a></td></tr>
				<tr><td colspan="2" align="center"><input type="button" id="btnClose"value="close" class="Button" onclick="showPopUp(0)"/></td></tr>
			</table>			
		</div>
		</td>
		</tr>
	</table>
</center>

<script type="text/javascript"><!--

var screenWidth='<%=screenWidth%>';
var closeMe='<%=closeMe%>';

function fullScreen(width){
	var url='private/showXBEMain.action';
	try{
		switch (width){
			case 0:
				window.open(url,'winCC','fullscreen=yes,scrollbars=no,location=no');
				break;
			case 1:
				var winWidth = 1024;
				var height=(width==1)?721:584;
				window.open(url,'winCC','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width='+winWidth+',height='+height+',resizable=no,top=1,left=1');
			default :
				var height=(width==1024)?731:584;//change 768-684 to match with the height whide screens
				window.open(url,'winCC','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width='+width+',height='+height+',resizable=no,top=1,left=1');
				break;
		}
		if (browser.isIE&&closeMe!='false'){WB.ExecWB(45,2);}
	}catch (e){}
}

function doLoad(){
	if(window.screen.width<='1024'&&screenWidth=='null'){
		window.location.replace('index.jsp?screenWidth='+window.screen.width);
	}else{
		showFullScreen();
	}
}

function showFullScreen(){
	var blnProceed = true;
	var isIE=browser.isIE;
	var strId;
	var isFF=browser.isFF;
	var isGC=browser.isGC;
	 
	if (!(isIE || isFF || isGC)){
		
		blnProceed=false;
	}
	
	if (isIE && (browser.version <= 6||window.screen.height<600)){
		blnProceed=false;
	}
	if (isFF && ( (browser.version == 3.5 && browser.subversions[2] == 0) || browser.version < 3.5 ||window.screen.height<600)){
		blnProceed=false;
	}
	
	//if(isGC){
	//		alert("Warning \n\n We do not recommend Chrome web browser. Please use FireFox or Internet Explorer");
	//}

	if (blnProceed){
		showPopUp(0);
		if (window.screen.width==1024){
			strId=0;
		}else if (window.screen.height==768){
			strId=1;
		}else if (window.screen.height==768){
			strId=1024;
		}else{
			strId=1024;
		}
		fullScreen(strId);
	}else {
		var strMsg = "Your computer does not meet the system requirements. \n" +
		 "Please do the necessary changes.\n\n\n" +
		 "System requirements	\n\n\n" +
		 "Browser : Microsoft Internet Explorer above 6 and Firefox above 3.5.0 .\n" +
		 "Screen Resolution : 800 x 600 pixels or above.\n" +
		 "Pop-up windows - Turn off popup blocking."
		//alert(strMsg)
showPopUp(1);
	}
	
}


var url=window.location.href;

if (url.indexOf('agents')==-1){
	setImage("imgSysName", "images/AA110_3_no_cache.jpg");
}else{
	setImage("imgSysName", "images/AA110_4_no_cache.jpg");	
}
	
function setImage(strImageID, strImagePath){
	var objControl = document.getElementById(strImageID);
	objControl.src = strImagePath;
}


function showPopUp(value){
  var windowDiv = document.getElementById('window');
 var messageDiv = document.getElementById('message');

if(value == 1){
  windowDiv.style.display = "block";
  messageDiv.style.display = "block";
} else {
  windowDiv.style.display = "none";
  messageDiv.style.display = "none";
}
 
}


doLoad();
//
--></script>
<!-- Google tracking start from Login screen  -->
</body>
</html>
