<%--
Author: Baladewa
// ******** Changing the Insurance Content to display in XBE Ancillary **** //
1. Get a copy of the "xbe/src/web/ext_html/ins_content.jsp" file and place in 
	to the XBE clients ext_html folder (if not exist create a folder in Client folder).
2. Be sure not change any of the ID in the existing elements and change the only the 
	HTML as client expect.
3. what ever additional (client specific) functionalities included in the client's ins_content.jsp
	with out breaking the flow
 --%>
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td width='85%' align='center'>
			<table width='95%' border='0' cellpadding='1' cellspacing='0' id="tblInsuranceDetails">
				<tr>
					<td class='doubleGap'>&nbsp;</td>
				</tr>
				<tr>
					<td align="left">
						<label id="lblInsCost"></label><span id='spnInsCost' class='txtBold'> </span>
					</td>
				</tr>
				<tr>
					<td class='doubleGap'>&nbsp;</td>
				</tr>
				<tr>
					<td class='rowGap' align="left">
						<!-- <label id="lblInsFurtherDetais">Sir / Ma'am, as mentioned, Zest Air Essential Travel Insurance is a group travel insurance policy taken out by Zest Air to give you protection from mishaps like accidents, losing your travel documents and baggage, trip cancellation and other travel inconveniences that may happen at the most unexpected moment, at the most inconvenient place and may easily ruin your trip <br /> NOTE: insurance cover is for a maximum period of 30 consecutive days and is only applicable to passengers 75 years old and below.</label>  -->
						<div class="comment more">
					</td>
				</tr>
				<tr>
					<td class='doubleGap'>&nbsp;</td>
				</tr>
				<tr>
					<td align="left">
						<table border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td><input type='checkbox' id='chkAnciIns' name='chkAnciIns' class='noBorder' ></td>
								<td>
									<label id='lblAddInsConf'>Yes, include Zest Air Essential Travel Insurance in the itinerary.</label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class='doubleGap'>&nbsp;</td>
				</tr>
				<tr>
					<td class='rowGap' align="left">
						<label id="lblTermsNCond"></label><a href='#' id="insTermsNCond" class='txtBold' >Terms & Conditions.</a>
					</td>
				</tr>
				<tr>
					<td class='' align="left">
						<label><input class='noBorder' type='checkbox' id="chkAccept" name="chkAccept" /> Customer Accepted Terms & Conditions</label>
					</td>
				</tr>
				<tr>
					<td class='doubleGap'>&nbsp;</td>
				</tr>
				<tr>
					<td class='' align="left">
						<label><input class='noBorder' type='checkbox' id="chkAgentAgreement" name="chkAgentAgreement" />Zest Air Agent to check this box as confirmation that Zest Air Essential Travel Insurance has been offered to passenger and option chosen above corresponds to passenger's reply to the offer</label>
					</td>
				</tr>
			</table>
		</td>
		<td width='15%' align='center' style='height:220px;'>
		   <table>
				<tr> 
					<td><img src='../images/inslogo_no_cache.png' alt='Logo'></td>
				<tr>
				<tr>
				 	<td><label> Zest Air Essential Travel Insurance is under written by Insurance Company of North America (ACE Insurance)</label></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>
$(function() {
	UI_tabAnci.anciInsuranceTCCheck = true;
	
	$("#chkViewMoreInfo").click(function(){
		$("#spnInsCost").hide();
		$("#lblInsFurtherDetais").show();
		$("#chkAnciIns").show();
		$("#insTermsNCond").show();
		$("#chkAccept").show();
		$("#chkAgentAgreement").show();
	});
	$("#chkAccept").click(function(){
		if ($("#chkAccept").attr("checked") == false){
			$("#chkAccept").attr("checked", false);
		}else{
			$("#chkAccept").attr("checked", true);
		}
	});
	
	$('.more').each(function() {
		var c='';
		var h ="Sir / Ma'am, as mentioned, Zest Air Essential Travel Insurance is a group travel insurance policy taken out by Zest Air to give you protection from mishaps like accidents, losing your travel documents and baggage, trip cancellation and other travel inconveniences that may happen at the most unexpected moment, at the most inconvenient place and may easily ruin your trip <br /> NOTE: insurance cover is for a maximum period of 30 consecutive days and is only applicable to passengers 75 years old and below.";
        var html = c +'<span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + "show-less" + '</a></span>'; 
        $(this).html(html);
	});

	$(".morelink").click(function(){
   	 if($(this).hasClass("more")) {
        $(this).removeClass("more");
        $(this).html("show-less");
    	} else {
       		 $(this).addClass("more");
        	$(this).html("show-more");
    	}
    	$(this).parent().prev().toggle();
    	$(this).prev().toggle();
    	return false;
	});
	
});
</script>


