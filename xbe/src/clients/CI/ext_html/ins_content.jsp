<%--
Author: Baladewa
// ******** Changing the Insurance Content to display in XBE Ancillary **** //
1. Get a copy of the "xbe/src/web/ext_html/ins_content.jsp" file and place in 
	to the XBE clients ext_html folder (if not exist create a folder in Client folder).
2. Be sure not change any of the ID in the existing elements and change the only the 
	HTML as client expect.
3. what ever additional (client specific) functionalities included in the client's ins_content.jsp
	with out breaking the flow
 --%>
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td width='70%' align='center'>
			<table width='95%' border='0' cellpadding='1' cellspacing='0' id="tblInsuranceDetails">
				<tr>
					<td>
						<label id="lblInsCost"></label><span id='spnInsCost' class='txtBold'> </span>
					</td>
				</tr>
				<tr>
					<td class='rowHeight'></td>
				</tr>
				<tr>
					<td>
						<table border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td><input type='checkbox' id='chkAnciIns' name='chkAnciIns' class='noBorder'></td>
								<td>
									<label id='lblAddInsConf'>Yes, Traveler like to purchase Travel Secure Travel Insurance.</label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class='rowHeight'></td>
				</tr>
				<tr>
					<td class='rowGap'>
						<label id="lblTermsNCond"></label><a href='#' id="insTermsNCond" class='txtBold'>Terms & Conditions.</a>
					</td>
				</tr>
				<tr>
					<td class=''>
						<label><input class='noBorder' type='checkbox' id="chkAccept" name="chkAccept"/> Customer Accepted Terms & Conditions</label>
					</td>
				</tr>
			</table>
		</td>
		<td width='30%' align='center' style='height:220px;'>
				<img src='../images/inslogo_no_cache.png' alt='Logo'>
		</td>
	</tr>
</table>
<script>
$(function() {
	UI_tabAnci.anciInsuranceTCCheck = true;
	$("#chkAccept").click(function(){
		if ($("#chkAccept").attr("checked") == false){
			$("#chkAccept").attr("checked", false);
		}else{
			$("#chkAccept").attr("checked", true);
		}
	});
});
</script>