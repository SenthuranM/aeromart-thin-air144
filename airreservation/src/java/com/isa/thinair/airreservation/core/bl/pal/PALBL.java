package com.isa.thinair.airreservation.core.bl.pal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airreservation.api.dto.pal.PALMetaDataDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.dataContext.PalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.FeaturePack;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdlDeliveryInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses.MessageResponseAdditionals;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

//TODO
/*
 * pal file path config @ reservation configs
 *
 */

public class PALBL {

	private static Log log = LogFactory.getLog(PALBL.class);

	private ReservationAuxilliaryDAO auxilliaryDAO;

	private ETicketDAO eTicketDAO;

	private Date currentDate;

	private Timestamp currentTimeStamp;

	private String msgType;

	private Set<String> pnlPnrs = new HashSet<String>();

	private String lastGroupCode = new String();

	private HashMap<String, Integer> ccPaxMap = new HashMap<String, Integer>();

	private ArrayList<String> generatedPnlfileNames = new ArrayList<String>();

	private final HashMap<String, String> mapPnrPaxSegIDGroupCode = new HashMap<String, String>();

	public PALBL() {
		this.auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		this.eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		this.currentDate = new Date();
		this.currentTimeStamp = new Timestamp(currentDate.getTime());
		this.msgType = PalConstants.PALGeneration.PAL;
	}
	

	public void sendPAL(String flightNumber, String departureStation, Date dateofflight, String[] sitaaddresses,
			String sendingMethod) throws ModuleException {		
		sendPALMessage(flightNumber, departureStation, dateofflight, sitaaddresses, sendingMethod);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendPALMessage(int flightId, String departureAirportCode) throws ModuleException {

		String flightNumber = "";
		HashMap sitaStatusMap = new HashMap();
		String fileContent = null;
		Date dateOfflightLocal = null;

		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		Flight flight = flightBD.getFlight(flightId);

		if (flight != null && flight.getStatus().equals(FlightStatusEnum.CANCELLED.getCode())) {

			log.info(" #####################     FLIGHT CANCELLED AND LOADING NEW FLIGHT ID : " + flightId);

			Integer actFlightId = null;

			for (FlightSegement fs : (flight.getFlightSegements())) {
				actFlightId = flightBD.getFlightID(fs.getSegmentCode(), flight.getFlightNumber(), fs.getEstTimeDepatureZulu());
				if (actFlightId != null) {
					flightId = actFlightId;
					log.info(" #####################     NEW FLIGHT ID TO SEND PAL : " + flightId);
					break;
				} else {
					log.info(" #####################     UNABLE TO FIND NEW FLIGHT ID AND USING OLD ONE : " + flightId);
				}
			}
		}

		flightNumber = auxilliaryDAO.getFlightNumber(flightId);

		log.debug("######### Retrieved Flight Number For: " + flightId + " Airport : " + departureAirportCode + " FlightNum :"
				+ flightNumber);

		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);

		log.debug("######### Retrieved SITA List For: " + flightId + " Airport : " + departureAirportCode + " Carrier code :"
				+ carrierCode);
		
		Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
		String ond = PnlAdlUtil.getOndFromDepartureAirport(departureAirportCode, legNumberWiseOndMap);

		Map<String, List<String>> sitaAddressesMap = auxilliaryDAO.getActiveSITAAddresses(departureAirportCode.trim(),
				flightNumber, ond, carrierCode, DCS_PAX_MSG_GROUP_TYPE.PAL_CAL);
		List<String> sitaAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
		List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
		List<String> sitaAddressAll = new ArrayList<String>();
		sitaAddressAll.addAll(sitaAddresses);
		sitaAddressAll.addAll(sitaTexAddress);

		dateOfflightLocal = auxilliaryDAO.getFlightLocalDate(flightId, departureAirportCode);

		sendPALMessage(flightNumber, departureAirportCode, dateOfflightLocal,
				(String[]) sitaAddressAll.toArray(new String[sitaAddressAll.size()]), PalConstants.SendingMethod.SCHEDSERVICE);

	}

	private void sendPALMessage(String flightNumber, String departureAirportCode, Date dateOfflightLocal, String[] sitaaddresses,
			String sendingMethod) throws ModuleException {
		
		
		synchronized (this) {
			int flightId = 0;
			boolean isFlightsAvailableForManulPNL = false;
			ModuleException exceptiontoThrow = null;
			HashMap sitaStatusMap = new HashMap();
			String fileContent = null;
			
			try {

				flightId = auxilliaryDAO.getFlightID(flightNumber, departureAirportCode, dateOfflightLocal);

				if (flightId == -1) {
					isFlightsAvailableForManulPNL = true;
					throw new ModuleException("airreservations.auxilliary.invalidflight", "airreservations");
				}

				checkPALTimeExceedsFlightClosureTime(flightId);

				List<String> sitas = PnlAdlUtil.composeSitaAddresses(sitaaddresses);
				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
				
				Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
				String ond = PnlAdlUtil.getOndFromDepartureAirport(departureAirportCode, legNumberWiseOndMap);
				
				Map<String, List<String>> sitaAddressesMap = PnlAdlUtil.getSitaAddressMap(departureAirportCode, flightNumber, ond,
						carrierCode, sitas, DCS_PAX_MSG_GROUP_TYPE.PAL_CAL);
				List<String> sitaAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
				List<String> arincAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
				PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddress, sitaTexAddress,arincAddresses);

				String[] monthDay = PnlAdlUtil.getMonthDayArray(dateOfflightLocal);

				PALMetaDataDTO palmetadatadto = new PALMetaDataDTO();
				
				palmetadatadto.setBoardingairport(departureAirportCode);
				palmetadatadto.setDay(monthDay[1]);
				palmetadatadto.setMonth(monthDay[0]);
				palmetadatadto.setFlight(flightNumber);
				palmetadatadto.setMessageIdentifier(msgType);
				palmetadatadto.setPartnumber(PNLConstants.PNLGeneration.PART_ONE);
				palmetadatadto.setPart2(true);
				
				/*if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
					palmetadatadto.setRbd(PnlAdlUtil.generateRBDValues(flightId));
					palmetadatadto.setRbdEnabled(true);
				}*/

				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.PAL_CAL_DATA_GENERATOR_MACRO);
				command.setParameter(CommandParamNames.PNL_DATA_CONTEXT, 
				
				createPALDataContext(flightNumber, departureAirportCode, dateOfflightLocal, sitaaddresses, sendingMethod));
					
					ServiceResponce serviceResponce = command.execute();
					
					MessageResponseAdditionals additionals = 
							(MessageResponseAdditionals)serviceResponce.getResponseParam
							(CommandParamNames.PNL_MESSAGE_RESPONSE_ADDITIONALS);
					Map<Integer,String> messageParts = (Map)serviceResponce.
							getResponseParam(CommandParamNames.PNL_MESSAGES_PARTS);
					BaseDataContext baseDataContext = (BaseDataContext)serviceResponce.
							getResponseParam(CommandParamNames.PNL_DATA_CONTEXT);
					
				//	if(baseDataContext.isWebserviceSuccess()){
				//		updateHistory(palmetadatadto.getBoardingairport(), flightId, PNLConstants.SITAMsgTransmission.SUCCESS);
				//	}else{
				//		updateHistory(palmetadatadto.getBoardingairport(), flightId, PNLConstants.SITAMsgTransmission.FAIL);
				//	}
					
					pnlPnrs = new HashSet<String>(additionals.getPnrCollection());
					lastGroupCode = additionals.getLastGroupCode();
					ccPaxMap = additionals.getFareClassWisePaxCount();
					this.generatedPnlfileNames = (ArrayList)createMessaeFile(messageParts, 
							flightNumber, departureAirportCode, palmetadatadto.getMonth()
							, palmetadatadto.getDay());

					
					Map<String, List<String>> deliverTypeAddressMap = new HashMap<String, List<String>>();

					deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddress);
					deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, arincAddresses);
					// sent pnl parts files to each sita address, return the sita add->status map
					sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedPnlfileNames, msgType);
					sitaStatusMap = PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddress, this.generatedPnlfileNames, msgType,
							sitaStatusMap, null);
				
					PnlAdlUtil.prepareGroupCodeMapWith(additionals.getPnrPaxIdvsSegmentIds(), mapPnrPaxSegIDGroupCode
							, additionals.getPnrPaxVsGroupCodes());
					
					updatePALStatus(getPnrPaxIds(additionals.getPnrPaxIdvsSegmentIds()),
							getPnrSegmentIds(additionals.getPnrPaxIdvsSegmentIds()), mapPnrPaxSegIDGroupCode, pnlPnrs);

					// get the content (all 3 parts merged)
					fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedPnlfileNames);

					// check of errors in the sita status map, if so
					PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);
					// End normal Sita PNL sending

			} catch (ModuleException exception) {
				exceptiontoThrow = exception;

			} catch (CommonsDataAccessException e) {
				exceptiontoThrow = new ModuleException(e, e.getExceptionCode());

			} catch (Exception e) {
				exceptiontoThrow = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

			} finally {

				log.debug("######### Before Inserting to History for PNL For FID: " + flightId + " Airport : "
						+ departureAirportCode + " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						departureAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoThrow, sendingMethod,
						lastGroupCode);

				log.debug("######### Inserted To History OK!");

				
				if (exceptiontoThrow != null) {
					// if error occured while sending error mail, a rollback to the entire operation
					// should not occur.
					try {
						String fromMethod = PnlAdlUtil.getSendingOperationDescription(sendingMethod);
						log.error("Error In PAL" + fromMethod + "  PAL Flight ID:" + flightId + " Flight Num : " + flightNumber
								+ " Airport :" + departureAirportCode, exceptiontoThrow);
						if (!isFlightsAvailableForManulPNL) {
							PnlAdlUtil.notifyAdminForManualAction(flightNumber, dateOfflightLocal, departureAirportCode,
									exceptiontoThrow, PNLConstants.MessageTypes.PAL + "\n ####" + fromMethod
											+ " SENDING FAILURE ###", null);
						}
						throw exceptiontoThrow;

					} catch (Throwable e) {
						if (e.equals(exceptiontoThrow)) {

							throw exceptiontoThrow;
						}
						log.error(
								"Error occured in  Logging Errors and Notifying Admin for Manual Action section in Finaly Method of sendPNL ",
								e);
					}

				}
				log.debug("######### Moving Generate Files FID: " + flightId + " Airport : " + departureAirportCode + " FlightNum :"
						+ flightNumber);
				PnlAdlUtil.moveGeneratedFiles(this.generatedPnlfileNames, msgType);
				log.debug("######### Moved OK!");
				
				log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- "
						+ departureAirportCode + " Flight Num : " + flightNumber);
			}
		}

	}

	private void updatePALStatus(Collection<Integer> pnrPaxIds,Collection<Integer> pnrSegIds, HashMap<String, String> groupPaxMap,
			Set<String> pnlPnrs) throws ModuleException {

		if (pnrSegIds.size() == 0 || pnrPaxIds.size() == 0) {
			log.error("Empty PnrSegIds or PnrPaxIds Found!");
			//because pal send when no pnr prm exists
			//throw new ModuleException(PNLConstants.ERROR_CODES.EMPTY_PNRSEGS_PAXIDS_ERROR);
		} else {

			PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIds, pnrPaxIds, PNLConstants.PaxPnlAdlStates.PNL_PNL_STAT,
					groupPaxMap, PNLConstants.MessageTypes.PAL);
			PnlAdlUtil.updateAirportMessagePaxStatus(pnrSegIds);

		}

	}

	private Collection<Integer> getPnrPaxIds(Map<Integer,Integer> pnrPaxVsSegmentIds){
		return pnrPaxVsSegmentIds.keySet();
	}
	
	private Collection<Integer> getPnrSegmentIds(Map<Integer,Integer> pnrPaxVsSegmentIds){
		return pnrPaxVsSegmentIds.values();
	}

	private PalDataContext createPALDataContext(String flightNumber, String departureAirportCode, Date dateOfflightLocal,
			String sitaaddresses[], String sendingMethod){
		
		FeaturePack featurePack = new FeaturePack();
	//	featurePack.setRbdEnabled(AppSysParamsUtil.isRBDPNLADLEnabled());
	//	featurePack.setShowBaggage(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE));
	//	featurePack.setShowMeals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL));
	//	featurePack.setShowSeatMap(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP));
	//	featurePack.setShowEticketDetails(AppSysParamsUtil.isShowTicketDetailsInPnlAdl());
		
		PalDataContext palDataContext = new PalDataContext();
		palDataContext.setFlightNumber(flightNumber);
		palDataContext.setDepartureAirportCode(departureAirportCode);
		palDataContext.setFlightLocalDate(dateOfflightLocal);
		palDataContext.setPnlAdlDeliveryInformation(createPalCalDeliveryInformation(sitaaddresses, sendingMethod));
		palDataContext.setFeaturePack(featurePack);
		palDataContext.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightNumber));		
		return palDataContext;
		
	}
	
	private PnlAdlDeliveryInformation createPalCalDeliveryInformation(String sitaaddresses[], String sendingMethod){
		
		PnlAdlDeliveryInformation pnlAdlDeliveryInformation = new PnlAdlDeliveryInformation();
		pnlAdlDeliveryInformation.setSendingMethod(sendingMethod);
		pnlAdlDeliveryInformation.setSitaaddresses(sitaaddresses);
		return pnlAdlDeliveryInformation;
		
	}

	private void checkPALTimeExceedsFlightClosureTime(int flightId) throws ModuleException {

		long nowts = currentTimeStamp.getTime();
		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		Flight flight = flightBD.getFlight(flightId);

		int flightClosureDepGap = Integer.parseInt(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
		long tsl = flight.getDepartureDate().getTime() - flightClosureDepGap * 60 * 1000;

		if (nowts > tsl)
			throw new ModuleException("airreservations.auxilliary.paltimeexceeded", "airreservations");
	}
	
	private List<String> createMessaeFile(Map<Integer, String> messageParts, String flightNo, String boardingAirport,
			String month, String day) throws ModuleException {

		List<String> messageFiles = new ArrayList<String>();
		try {
			for (Map.Entry<Integer, String> entry : messageParts.entrySet()) {
				messageFiles.add(createMessageFile(entry.getKey(), entry.getValue(), flightNo, boardingAirport, month, day));
			}
		} catch (Exception e) {
			log.error("Generate PAL files Error :", e);
			throw new ModuleException(e, PNLConstants.ERROR_CODES.PAL_GENERATION_ERROR_CODE);
		}

		return messageFiles;
	}
	
	private String createMessageFile(Integer partNumber,String messageContent,
			String flightNo,String boardingAirport,String month,String day) throws Exception{

		SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
		List<String> filenamelist = new ArrayList<String>();
		if (Integer.parseInt(day) <= 9)
			day = "0" + day;
		String generatedpnlfilepath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
				.getGenPalPath();
		String date = simpleDateFormat.format(new Date());
		String fileName=generatedpnlfilepath + "/" + flightNo + "-" + boardingAirport + "-" + month + "" + day + "-" + "PAL"
				+ "-" + date + "-" + "PART"+ partNumber + ".txt";
		
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(messageContent);
		bw.close();
		return fileName;
	}
	
}
