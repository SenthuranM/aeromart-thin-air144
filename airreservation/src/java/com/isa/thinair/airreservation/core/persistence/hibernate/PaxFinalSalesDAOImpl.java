/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.PFSMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.pfs.CSPaxBookingCount;
import com.isa.thinair.airreservation.api.dto.pfs.PFSDTO;
import com.isa.thinair.airreservation.api.model.CodeSharePfs;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.core.persistence.dao.PaxFinalSalesDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * PaxFinalSalesDAOImpl is the business DAO hibernate implementation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.dao-impl dao-name="PaxFinalSalesDAO"
 */
public class PaxFinalSalesDAOImpl extends PlatformBaseHibernateDaoSupport implements PaxFinalSalesDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PaxFinalSalesDAOImpl.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int getPfsEntriesCount(String flightNo, String airport, Date departureDate) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		ArrayList conditionList = new ArrayList();
		ArrayList valueList = new ArrayList();

		String sql = " SELECT count(*) PAXCOUNT FROM T_PFS ";

		if (flightNo != null && !flightNo.equals("")) {
			conditionList.add(" FLIGHT_NUMBER=? ");
			valueList.add(flightNo);
		}

		if (airport != null && !airport.equals("")) {
			conditionList.add(" FROM_AIRPORT=? ");
			valueList.add(airport);
		}

		if (departureDate != null) {
			conditionList.add(" DEPARTURE_DATE=? ");
			valueList.add(departureDate);
		}

		if (conditionList.size() > 0) {
			sql += " WHERE ";
		}

		for (int t = 0; t < conditionList.size(); t++) {
			sql += (String) conditionList.get(t) + " AND ";
		}

		if (sql.indexOf("AND") != -1) {
			sql = sql.substring(0, sql.lastIndexOf("AND"));
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Object[] params = valueList.toArray();
		Integer paxCount = (Integer) jt.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs != null) {
					while (rs.next()) {
						return new Integer(rs.getInt("PAXCOUNT"));
					}
				}

				return new Integer(0);
			}
		});

		log.debug("Exit getPfsParseEntryCount");
		return paxCount.intValue();
	}

	/**
	 * Return pfs parse entries
	 * 
	 * @param pfsId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public int getPfsParseEntriesCount(int pfsId, String processedStatus) throws CommonsDataAccessException {

		log.debug("Inside getPfsParseEntryCount");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = "";
		Object[] params;
		if (processedStatus == null) {
			params = new Object[] { new Integer(pfsId) };
			sql = " SELECT count(*) PAXCOUNT FROM T_PFS_PARSED WHERE PFS_ID = ? ";
		} else {
			params = new Object[] { new Integer(pfsId), processedStatus };
			sql = " SELECT count(*) PAXCOUNT FROM T_PFS_PARSED WHERE PFS_ID = ? and PROC_STATUS = ? ";
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL argument             : " + pfsId);
		log.debug("############################################");

		Integer paxCount = (Integer) jt.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs != null) {
					while (rs.next()) {
						return new Integer(rs.getInt("PAXCOUNT"));
					}
				}

				return new Integer(0);
			}
		});

		log.debug("Exit getPfsParseEntryCount");
		return paxCount.intValue();
	}

	/**
	 * Returns pfs paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 */
	public Page getPagedPFSData(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList) {
		Page pgPfs = getPagedData(criteria, startIndex, noRecs, Pfs.class, orderByFieldList);
		return pgPfs;
	}

	/**
	 * Returns the pfs
	 * 
	 * @param pfsID
	 * @return
	 */
	public Pfs getPFS(int pfsID) {
		return (Pfs) get(Pfs.class, new Integer(pfsID));
	}

	/**
	 * Save Pfs Entry
	 * 
	 * @param pfs
	 */
	public void savePfsEntry(Pfs pfs) {
		log.debug("Inside savePfsEntry");

		// Save the reservation
		hibernateSaveOrUpdate(pfs);

		log.debug("Exit savePfsEntry");
	}

	/**
	 * Save Pfs Entry and returns the pfs id
	 * 
	 * @param pfs
	 * 
	 */
	public int savePfsEntryForAudit(Pfs pfs) {

		log.debug("Inside savePfsEntryForAudit");

		hibernateSaveOrUpdate(pfs);
		return pfs.getPpId();
	}

	/**
	 * Delete Pfs
	 * 
	 * @param pfs
	 */
	public void deletePfsEntry(Pfs pfs) {
		log.debug("Inside deletePfsParseEntry");

		// Delete the PfsParseEntry
		delete(pfs);

		log.debug("Exit deletePfsParseEntry");
	}

	/**
	 * Save Pfs Parse Entry
	 * 
	 * @param pfsParsed
	 */
	public void savePfsParseEntry(PfsPaxEntry pfsParsed) {
		log.debug("Inside savePfsParseEntry");

		// Save the reservation
		hibernateSaveOrUpdate(pfsParsed);

		log.debug("Exit savePfsParseEntry");
	}

	/**
	 * Save Pfs Parse Entry
	 * 
	 * @param colPfsParseEntry
	 */
	public void savePfsParseEntries(Collection<PfsPaxEntry> colPfsParseEntry) {
		log.debug("Inside savePfsParseEntries");

		// Save the reservation
		hibernateSaveOrUpdateAll(colPfsParseEntry);

		log.debug("Exit savePfsParseEntries");
	}

	/**
	 * Delete PfsParseEntry
	 * 
	 * @param pfs
	 */
	public void deletePfsParseEntry(PfsPaxEntry pfsParsed) {
		log.debug("Inside deletePfsParseEntry");

		// Delete the PfsParseEntry
		delete(pfsParsed);

		log.debug("Exit deletePfsParseEntry");
	}

	/**
	 * Return pfs parse entries
	 * 
	 * @param pfsId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<PfsPaxEntry> getPfsParseEntries(int pfsId) throws CommonsDataAccessException {

		String hql = " SELECT pfsEntry FROM PfsPaxEntry AS pfsEntry " + " WHERE pfsEntry.pfsId = ? ";

		return find(hql, new Object[] { new Integer(pfsId) }, PfsPaxEntry.class);
	}

	public void deleteAllErrorEntries(int pfsId) throws CommonsDataAccessException {
		String hqlDelete = "DELETE FROM PfsPaxEntry WHERE pfsId = '" + pfsId + "' AND processedStatus = 'E'";
		Query query = getSession().createQuery(hqlDelete);
		int rowCount = query.executeUpdate();
		log.debug(" ROWS AFFECTED : " + rowCount);
	}

	/**
	 * Return pfs parse entries
	 * 
	 * @param pfsId
	 * @param status
	 * @return
	 */
	public Collection<PfsPaxEntry> getPfsParseEntries(int pfsId, String status) {
		if (status == null) {
			// By ordering it from pax type-decending, we can get infants first to process. so we can avoid adult
			// processed first and end up without processing infant due to cancellation of the segment
			// for GOSHOW passengers we should process adult first.Therefore added case to order by
			String hql = " SELECT pfsEntry FROM PfsPaxEntry AS pfsEntry "
					+ " WHERE pfsEntry.pfsId = ? AND pfsEntry.processedStatus != 'P' "
					+ " ORDER BY  CASE WHEN pfsEntry.entryStatus = 'N' THEN pfsEntry.paxType END DESC, "
					+ " CASE WHEN pfsEntry.entryStatus <> 'N' THEN pfsEntry.ppId END ASC ";

			return find(hql, new Object[] { new Integer(pfsId) }, PfsPaxEntry.class);
		} else {
			String hql = " SELECT pfsEntry FROM PfsPaxEntry AS pfsEntry "
					+ " WHERE pfsEntry.pfsId = ? AND pfsEntry.processedStatus = ? "
					+ " ORDER BY  CASE WHEN pfsEntry.entryStatus = 'N' THEN pfsEntry.paxType END DESC, "
					+ " CASE WHEN pfsEntry.entryStatus <> 'N' THEN pfsEntry.ppId END ASC ";

			return find(hql, new Object[] { new Integer(pfsId), status }, PfsPaxEntry.class);
		}
	}

	/**
	 * Returns true if record found
	 */
	public boolean hasPFS(String flightNumber, String airportCode, Date departureDate) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		String sql = "SELECT count(pfs_id)" + " FROM t_pfs " + " where flight_number=? and trunc(departure_date)='"
				+ dateFormat.format(departureDate) + "'  and  from_airport=?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, airportCode };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			log.info("Duplicate PFS detected. Flight NO - " + flightNumber + " airport code - " + airportCode
					+ " departureDate - " + departureDate);
			return true;
		}
		return false;
	}

	/**
	 * Returns true if record found
	 */
	public boolean hasPFSFile(Date date) {

		String sql = "SELECT count(pfs_id) as cnt FROM t_pfs where date_of_download=?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { date };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = 0;
				if (rs.next()) {
					count = new Integer(rs.getInt("cnt"));
				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		} else {
			return false;
		}
	}

	
	/**
	 * Returns true if record found
	 */
	public boolean checkIfPfsAlreadyExists(Date departureDate, String flightNumber, String fromAirport) {

		String sql = "SELECT count(pfs_id) as cnt FROM t_pfs where departure_date=? and flight_number=? and from_airport=?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { departureDate,flightNumber,fromAirport };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = 0;
				if (rs.next()) {
					count = new Integer(rs.getInt("cnt"));
				}
				return count;

			}
		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		} else {
			return false;
		}
	}

	
	/**
	 * Get Pax Names having error
	 * 
	 * @param pfsID
	 * @param Status
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getPaxNamesForMail(Integer pfsID, Collection<String> statuss) {

		if (pfsID == null || statuss == null || statuss.size() == 0) {
			return null;
		}
		String sqlInStr = PlatformUtiltiies.constructINStringForCharactors(statuss);
		String sql = " SELECT pnr, title, first_name, last_name, pax_status, error_description, pax_type_code "
				+ " FROM t_pfs_parsed where pfs_id = ? and proc_status in ( " + sqlInStr + ")" + " order by pax_status ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { pfsID };

		Collection<String> paxDetails = (Collection<String>) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> paxDetails = new ArrayList<String>();

				while (rs.next()) {
					String status = BeanUtils.nullHandler(rs.getString("pax_status"));
					String paxType = BeanUtils.nullHandler(rs.getString("pax_type_code"));

					if (status.equals(ReservationInternalConstants.PfsPaxStatus.GO_SHORE)) {
						status = "GOSHO";
					}
					if (status.equals(ReservationInternalConstants.PfsPaxStatus.NO_SHORE)) {
						status = "NOSHO";
					}
					if (status.equals(ReservationInternalConstants.PfsPaxStatus.NO_REC)) {
						status = "NOREC";
					}
					if (status.equals(ReservationInternalConstants.PfsPaxStatus.OFF_LD)) {
						status = "OFFLK";
					}

					String paxDetail = "PNR NUMBER: " + BeanUtils.nullHandler(rs.getString("pnr")) + " NAME : ["
							+ ReservationPax.getPassengerTypeDescription(paxType) + "] "
							+ BeanUtils.nullHandler(rs.getString("title")) + " "
							+ BeanUtils.nullHandler(rs.getString("first_name")) + " "
							+ BeanUtils.nullHandler(rs.getString("last_name")) + " " + status + "  ||"
							+ BeanUtils.nullHandler(rs.getString("error_description"));
					paxDetails.add(paxDetail);

				}

				return paxDetails;
			}

		});

		return paxDetails;
	}

	/**
	 * Returns the pfs
	 * 
	 * @param pfsID
	 * @return
	 */
	public int getPFS(String fromAirport, String flightNumber, Date departureDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		String sql = "SELECT pfs_id" + " FROM t_pfs " + " where flight_number=? and trunc(departure_date)='"
				+ dateFormat.format(departureDate) + "'  and  from_airport=?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, fromAirport };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer pfsId = new Integer(0);
				while (rs.next()) {
					pfsId = new Integer(rs.getInt(1));
				}
				return pfsId;
			}
		});

		return ((Integer) intObj).intValue();

	}

	public int getPaxTypeCount(Integer pfsID, String pnr, String paxType) {

		String sql = "select count(*) from t_pfs_parsed tpp " + " where tpp.pfs_id = ? " + " and tpp.pnr = ? "
				+ " and tpp.pax_type_code = ?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { pfsID, pnr, paxType };

		Object intPaxCountObj = template.query(sql, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer paxTypeCount = new Integer(0);
				while (rs.next()) {
					paxTypeCount = new Integer(rs.getInt(1));
				}
				return paxTypeCount;
			}

		});

		return ((Integer) intPaxCountObj).intValue();
	}

	@SuppressWarnings("unchecked")
	public String getReservationCabinclass(String pnr, Integer pnrSegID) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Object params[];

		String sql = "select bc.cabin_class_code as CABIN_CLASS_CODE " + " from T_reservation r,	" + " t_pnr_segment ps,	"
				+ " t_pnr_pax_fare_segment ppfs, " + " t_booking_class bc	" + " where r.pnr  = ?	" + " and r.pnr = ps.pnr	"
				+ " and ps.pnr_seg_id = ppfs.pnr_seg_id	" + " and ppfs.booking_code = bc.booking_code "
				+ " and ps.status = 'CNF' ";
		if (pnrSegID == null) {
			params = new Object[] { pnr };
		} else {
			sql += " and ps.pnr_seg_id = ?";
			params = new Object[] { pnr, pnrSegID };
		}
		String cabinClassCode;

		log.debug("###############################################");
		log.debug(" SQL to excute                         : " + sql);
		log.debug(" SQL parameter PNR			         : " + pnr);
		log.debug(" SQL parameter pnrSegID		         : " + pnrSegID);
		log.debug("###############################################");

		Collection<String> colCabinCodes = (Collection<String>) jt.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> colCabinClassCode = new ArrayList<String>();

				if (rs != null) {
					while (rs.next()) {
						colCabinClassCode.add(rs.getString("CABIN_CLASS_CODE"));
					}
				}

				return colCabinClassCode;
			}
		});

		cabinClassCode = (String) BeanUtils.getFirstElement(colCabinCodes);

		return cabinClassCode;
	}

	/***
	 * return Booking class
	 * 
	 * **/
	public String getReservationBookingclass(String pnr, Integer flightSegId, boolean isGoShow) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " select distinct ppfs.booking_code BC_CODE " + " from t_pnr_pax_fare_segment ppfs  "
				+ " where ppfs.pnr_seg_id in (        " + " 		select ps.pnr_seg_id         " + " 		from t_pnr_segment ps        ";
		if (isGoShow) {
			sql += "	where ps.pnr = '" + pnr + "' )		";
		} else {
			sql += " 	where ps.pnr = '" + pnr + "'   and ps.flt_seg_id = " + flightSegId + " )	";
		}

		log.debug("###############################################");
		log.debug(" SQL to excute                         : " + sql);
		log.debug(" SQL parameter PNR			          : " + pnr);
		log.debug("###############################################");

		String bookingClassCode = (String) jt.query(sql, new Object[] {}, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String bookingClass = "";
				if (rs != null) {
					while (rs.next()) {
						bookingClass = rs.getString("BC_CODE");
					}
				}
				return bookingClass;
			}
		});

		return bookingClassCode;

	}

	public void saveParent(PfsPaxEntry pfsParsed) {

		if (pfsParsed.getParentPpId() != null) {
			String hql = " FROM PfsPaxEntry AS pfsEntry where pfsEntry.ppId = ? ";
			Collection<PfsPaxEntry> pfsEntry = find(hql, new Object[] { pfsParsed.getParentPpId() }, PfsPaxEntry.class);
			Iterator<PfsPaxEntry> itePfsEntry = pfsEntry.iterator();
			while (itePfsEntry.hasNext()) {
				PfsPaxEntry pfsPaxEntry = (PfsPaxEntry) itePfsEntry.next();
				if (pfsPaxEntry.getIsParent() == null || "N".equals(pfsPaxEntry.getIsParent())) {
					pfsPaxEntry.setIsParent("Y");
					hibernateSave(pfsPaxEntry);
					break;
				}
			}

		}
	}

	public PfsPaxEntry getParent(Integer parentPPID) {

		PfsPaxEntry pfsPaxEntry = null;
		String hql = " FROM PfsPaxEntry AS pfsEntry where pfsEntry.parentPpId = ? ";
		// String hql = " FROM PfsPaxEntry AS pfsEntry where pfsEntry.ppId = ? ";

		List<PfsPaxEntry> pfsPaxEntries = find(hql, new Object[] { parentPPID }, PfsPaxEntry.class);
		if (pfsPaxEntries != null && !pfsPaxEntries.isEmpty()) {
			pfsPaxEntry = pfsPaxEntries.get(0);
		}

		return pfsPaxEntry;
	}

	public PfsPaxEntry getPFSPaxEntry(Integer ppId) {

		PfsPaxEntry pfsPaxEntry = null;
		String hql = " FROM PfsPaxEntry AS pfsEntry where pfsEntry.ppId = ? ";

		List<PfsPaxEntry> pfsPaxEntries = find(hql, new Object[] { ppId }, PfsPaxEntry.class);
		if (pfsPaxEntries != null && !pfsPaxEntries.isEmpty()) {
			pfsPaxEntry = pfsPaxEntries.get(0);
		}

		return pfsPaxEntry;
	}

	public boolean isNOSHOPaxEntry(String eTicketNo, String paxStatus, String segmentCode) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "SELECT ppfs.pax_status FROM "
				+ " T_PAX_E_TICKET pet, t_pnr_pax_fare_seg_e_ticket ppfst, t_pnr_pax_fare_segment ppfs,t_pnr_segment ps,t_flight_segment fs "
				+ " WHERE pet.e_ticket_number = '"
				+ eTicketNo
				+ "' AND pet.pax_e_ticket_id  =ppfst.pax_e_ticket_id AND ppfst.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id      =ps.pnr_seg_id"
				+ " AND fs.flt_seg_id = ps.flt_seg_id AND ps.status = 'CNF' AND fs.segment_code LIKE '" + segmentCode
				+ "' AND ppfs.pax_status='" + paxStatus + "'";
		return (Boolean) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return true;
				}
				return false;
			}
		});
	}


	public boolean isFlownOrNoShowPaxEntry(String eTicketNo, String segmentCode) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "SELECT ppfs.pax_status FROM "
				+ " T_PAX_E_TICKET pet, t_pnr_pax_fare_seg_e_ticket ppfst, t_pnr_pax_fare_segment ppfs,t_pnr_segment ps,t_flight_segment fs "
				+ " WHERE pet.e_ticket_number = '"
				+ eTicketNo
				+ "' AND pet.pax_e_ticket_id  =ppfst.pax_e_ticket_id AND ppfst.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id      =ps.pnr_seg_id"
				+ " AND fs.flt_seg_id = ps.flt_seg_id AND ps.status = 'CNF' AND fs.segment_code LIKE '" + segmentCode
				+ "' AND (ppfs.pax_status= 'F' OR ppfs.pax_status= 'N')";
		return (Boolean) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return true;
				}
				return false;
			}
		});
	}
	
	/**
	 * Returns the Map<Integer, PFSMetaDataDTO>
	 * 
	 * @param pfsID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, PFSMetaDataDTO> getWaitingPFSList(Integer passedDateCount) {
		Map<Integer, PFSMetaDataDTO> waitingPFSList = new HashMap<Integer, PFSMetaDataDTO>();

		String sql = "SELECT PFS_ID, FROM_AIRPORT, FLIGHT_NUMBER, DEPARTURE_DATE, DATE_OF_DOWNLOAD " + " FROM T_PFS  "
				+ " WHERE DEPARTURE_DATE > SYSDATE - ?  AND PROCESSED_STATUS = '"
				+ ReservationInternalConstants.PfsStatus.WAITING_FOR_ETL_PROCESS + "' ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { passedDateCount };

		waitingPFSList = (Map<Integer, PFSMetaDataDTO>) template.query(sql, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, PFSMetaDataDTO> tmpWaitingPFSList = new HashMap<Integer, PFSMetaDataDTO>();
				while (rs.next()) {
					PFSMetaDataDTO pfsMetaDataDTO = new PFSMetaDataDTO();
					pfsMetaDataDTO.setBoardingAirport(BeanUtils.nullHandler(rs.getString("FROM_AIRPORT")));
					pfsMetaDataDTO.setRealDepartureDate(rs.getDate("DEPARTURE_DATE"));
					pfsMetaDataDTO.setFlightNumber(BeanUtils.nullHandler(rs.getString("FLIGHT_NUMBER")));
					pfsMetaDataDTO.setDateDownloaded(rs.getDate("DATE_OF_DOWNLOAD"));
					Integer pfsId = new Integer(rs.getInt("PFS_ID"));
					tmpWaitingPFSList.put(pfsId, pfsMetaDataDTO);
				}

				return tmpWaitingPFSList;
			}

		});

		return waitingPFSList;

	}
	
	public Collection<PFSDTO> getCodeShareFlightDetailsForPfs(String flightNumber, Date departureDate)
			throws CommonsDataAccessException {

		try {
			StringBuilder sbSql = new StringBuilder();

			sbSql.append("SELECT CS.CS_FLIGHT_NUMBER,F.DEPARTURE_DATE,CS.CS_CARRIER_CODE,F.ORIGIN,F.DESTINATION,GS.GDS_CODE  ");
			sbSql.append("FROM T_FLIGHT F,  T_FLIGHT_GDS FG,  T_CS_FLIGHT CS,T_GDS GS ");
			sbSql.append("WHERE F.FLIGHT_NUMBER ='" + flightNumber + "' ");
			sbSql.append("AND F.DEPARTURE_DATE BETWEEN  TO_DATE(" + CalendarUtil.formatDateForSQL_toDate(departureDate));
			sbSql.append(") AND TO_DATE(" + CalendarUtil.formatForSQL_toDate(CalendarUtil.getEndTimeOfDate(departureDate)) + ") ");
			sbSql.append("AND F.FLIGHT_ID = FG.FLIGHT_ID AND CS.FLIGHT_ID = F.FLIGHT_ID ");
			sbSql.append("AND GS.CARRIER_CODE = CS.CS_CARRIER_CODE AND FG.GDS_ID = GS.GDS_ID ");
			sbSql.append("AND CS.FLIGHT_ID = FG.FLIGHT_ID AND CS.CS_FLIGHT_NUMBER IN ");
			sbSql.append("(SELECT PS.CS_FLIGHT_NUMBER  FROM T_FLIGHT F,T_FLIGHT_SEGMENT FS,T_PNR_SEGMENT PS ");
			sbSql.append("WHERE F.FLIGHT_ID = FS.FLIGHT_ID AND FS.FLT_SEG_ID = PS.FLT_SEG_ID ");
			sbSql.append("AND F.FLIGHT_NUMBER ='" + flightNumber + "' ");
			sbSql.append("AND PS.STATUS = '" + ReservationSegmentStatus.CONFIRMED + "'");
			sbSql.append("AND F.DEPARTURE_DATE BETWEEN  TO_DATE(" + CalendarUtil.formatDateForSQL_toDate(departureDate));
			sbSql.append(") AND TO_DATE(" + CalendarUtil.formatForSQL_toDate(CalendarUtil.getEndTimeOfDate(departureDate))
					+ ")) ");

			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);

			@SuppressWarnings("unchecked")
			Collection<PFSDTO> pfsDTOColl = (Collection<PFSDTO>) templete.query(sbSql.toString(), new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection<PFSDTO> pfsDTOList = new ArrayList<PFSDTO>();
					if (rs != null) {
						while (rs.next()) {

							PFSDTO pfsDTO = new PFSDTO();
							pfsDTO.setArrivalAirport(rs.getString("DESTINATION"));
							pfsDTO.setDepartureAirport(rs.getString("ORIGIN"));
							pfsDTO.setFlightDate(rs.getDate("DEPARTURE_DATE"));
							pfsDTO.setFlightNumber(rs.getString("CS_FLIGHT_NUMBER"));
							pfsDTO.setGdsCode(rs.getString("GDS_CODE"));
							pfsDTO.setCarrierCode(rs.getString("CS_CARRIER_CODE"));

							pfsDTOList.add(pfsDTO);

						}
					}
					return pfsDTOList;
				}
			});
			return pfsDTOColl;

		} catch (Exception e) {
			e.printStackTrace();
			throw new CommonsDataAccessException(e, null);
		}

	}

	public Collection<CSPaxBookingCount> getCodeSharePaxCountByBookingClass(String csFlightNumber, Date departureDate,
			String carrierCode) throws CommonsDataAccessException {
		try {
			StringBuilder sbSql = new StringBuilder();

			sbSql.append("SELECT GS.GDS_ID,FS.SEGMENT_CODE,PS.CS_BOOKING_CLASS,SUM(R.TOTAL_PAX_COUNT-R.TOTAL_PAX_INFANT_COUNT) AS PAX ");
			sbSql.append("FROM T_FLIGHT_SEGMENT FS,T_PNR_SEGMENT PS,T_RESERVATION R,T_GDS GS ");
			sbSql.append("WHERE R.PNR = PS.PNR AND GS.GDS_ID = R.GDS_ID AND PS.CS_FLIGHT_NUMBER IS NOT NULL ");
			sbSql.append("AND GS.CARRIER_CODE = '" + carrierCode + "' AND FS.FLT_SEG_ID=PS.FLT_SEG_ID ");
			sbSql.append("AND PS.CS_FLIGHT_NUMBER ='" + csFlightNumber + "' AND PS.CS_BOOKING_CLASS IS NOT NULL  ");
			sbSql.append("AND PS.STATUS='" + ReservationSegmentStatus.CONFIRMED + "' AND FS.EST_TIME_DEPARTURE_LOCAL ");
			sbSql.append("BETWEEN  TO_DATE(" + CalendarUtil.formatDateForSQL_toDate(departureDate));
			sbSql.append(") AND TO_DATE(" + CalendarUtil.formatForSQL_toDate(CalendarUtil.getEndTimeOfDate(departureDate)) + ") ");
			sbSql.append("GROUP BY GS.GDS_ID,PS.CS_FLIGHT_NUMBER,FS.SEGMENT_CODE,PS.CS_BOOKING_CLASS ");

			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);

			@SuppressWarnings("unchecked")
			Collection<CSPaxBookingCount> csPaxCountColl = (Collection<CSPaxBookingCount>) templete.query(sbSql.toString(),
					new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

							Collection<CSPaxBookingCount> csPaxCountList = new ArrayList<CSPaxBookingCount>();
							if (rs != null) {
								while (rs.next()) {
									CSPaxBookingCount paxBookingCount = new CSPaxBookingCount();
									paxBookingCount.setBookingClass(rs.getString("CS_BOOKING_CLASS"));
									paxBookingCount.setSegmentCode(rs.getString("SEGMENT_CODE"));
									paxBookingCount.setPaxCount(rs.getInt("PAX"));
									paxBookingCount.setGdsId(rs.getInt("GDS_ID"));

									csPaxCountList.add(paxBookingCount);

								}
							}
							return csPaxCountList;
						}
					});
			return csPaxCountColl;

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public void saveCodeSharePfsEntry(CodeSharePfs pfs) {
		log.debug("Inside saveCodeSharePfsEntry");
		hibernateSaveOrUpdate(pfs);
		log.debug("Exit saveCodeSharePfsEntry");
	}

	public boolean checkCodeSharePfsAlreadySent(Date departureDate, String flightNumber, String fromAirport, String carrierCode) {

		StringBuilder sbSql = new StringBuilder();
		sbSql.append("SELECT COUNT(CS_PFS_ID) AS CNT FROM T_CS_PFS ");
		sbSql.append("WHERE DEPARTURE_DATE ");
		sbSql.append("BETWEEN  TO_DATE(" + CalendarUtil.formatDateForSQL_toDate(departureDate));
		sbSql.append(") AND TO_DATE(" + CalendarUtil.formatForSQL_toDate(CalendarUtil.getEndTimeOfDate(departureDate)) + ") ");
		sbSql.append("AND CS_FLIGHT_NUMBER=?");
		sbSql.append("AND CS_CARRIER_CODE =? AND FROM_AIRPORT=? ");

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, carrierCode, fromAirport };

		Object intObj = template.query(sbSql.toString(), params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = 0;
				if (rs.next()) {
					count = new Integer(rs.getInt("CNT"));
				}
				return count;

			}
		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		} else {
			return false;
		}
	}
}
