package com.isa.thinair.airreservation.core.bl.tty;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ChangeCabinMessageCreator extends TypeBReservationMessageCreator {
	
	public ChangeCabinMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}
}
