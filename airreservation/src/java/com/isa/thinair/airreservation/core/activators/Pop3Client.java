/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.activators;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Reader;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.pop3.POP3Client;
import org.apache.commons.net.pop3.POP3MessageInfo;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.emirates.XAPNLFormatUtils;
import com.isa.thinair.airreservation.core.bl.pfs.PFSFormatUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Pop3 bridge
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class Pop3Client {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(Pop3Client.class);

	/**
	 * Pick Messages
	 * 
	 * @param path
	 * @param hostName
	 * @param userName
	 * @param password
	 * @param messageType
	 * @param deleteMessage
	 * @throws ModuleException
	 */
	public static void getMessages(String path, String hostName, String userName, String password, String messageType,
			boolean deleteMessage) throws ModuleException {

		// Validate the input parameters
		validateParams(path, hostName, userName, password, messageType);

		Date currentDate = new Date();
		BufferedReader bufferedReader;
		FileOutputStream fileOutputStream;
		Reader reader;
		File file;

		File messagesDir = new File(path);
		if (!messagesDir.exists()) {
			boolean status = messagesDir.mkdirs();

			// This could be due to some file creational problems
			if (!status) {
				throw new ModuleException("airreservations.pop3Client.mailFilesCreationError");
			}
		}

		POP3Client pop3 = new POP3Client();

		try {
			log.info(" ###################### Connecting to Mail Server " + hostName);
			pop3.connect(hostName);
			log.info(" ##################### Connected to Mail Server");
			
			pop3.login(userName, password);

			log.info(" ###################### Authentication Successful ");

			POP3MessageInfo[] messages = pop3.listMessages();

			if (messages != null) {
				for (int message = 0; message < messages.length; message++) {
					log.info(" ###################### Reading Message " + messages[message].number);

					reader = pop3.retrieveMessage(messages[message].number);

					// Returns the created appropriate file object
					file = getFileObject(messagesDir, currentDate, message, messageType);

					bufferedReader = new BufferedReader(reader);
					fileOutputStream = new FileOutputStream(file);

					int character;
					while ((character = bufferedReader.read()) != -1) {
						fileOutputStream.write(character);
					}

					fileOutputStream.close();
					bufferedReader.close();

					log.info(" ###################### File Created " + file.getName());

					if (deleteMessage) {
						log.info(" ###################### Started Deleting " + messages[message].number + " from the Mail box ");

						pop3.deleteMessage(messages[message].number);

						log.info(" ###################### Finished Deleting " + messages[message].number + " from the Mail box ");
					}
				}
			}

			pop3.logout();
			pop3.disconnect();

			log.info(" ###################### Disconnecting from Mail Server " + hostName);

		} catch (Exception e) {
			log.error(" Pop3 Error : ", e);
			throw new ModuleException("airreservations.pop3Client.genericMailError");
		} finally {
			try {
				pop3.logout();
			} catch (Exception e) {
			}
			try {
				pop3.disconnect();
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Validate the parameters
	 * 
	 * @param path
	 * @param hostName
	 * @param userName
	 * @param password
	 * @param messageType
	 * @throws ModuleException
	 */
	private static void validateParams(String path, String hostName, String userName, String password, String messageType)
			throws ModuleException {
		if (path == null || path.equals("") || hostName == null || hostName.equals("") || userName == null || password == null
				|| messageType == null || messageType.equals("")) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}

	/**
	 * Returns the file object
	 * 
	 * @param messagesDir
	 * @param currentDate
	 * @param message
	 * @param messageType
	 * @return
	 * @throws ModuleException
	 */
	private static File getFileObject(File messagesDir, Date currentDate, int message, String messageType) throws ModuleException {
		if (messageType.equals(ReservationInternalConstants.PopMessageType.PFS_MESSAGE)) {
			return new File(messagesDir, PFSFormatUtils.getPfsFileName(currentDate, message));
		} else if (messageType.equals(ReservationInternalConstants.PopMessageType.PNL_MESSAGE)) {
			return new File(messagesDir, XAPNLFormatUtils.getPnlFileName(currentDate, message));
		}

		throw new ModuleException("airreservations.arg.invalid.null");
	}

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String path = "c:/PfsProcessPath";

		String defaultMailServer = "mail.bluebottle.com";
		String username = "accelaerouser@bluebottle.com";
		String password = "password123";
		boolean deleteMessages = true;

		try {
			getMessages(path, defaultMailServer, username, password, ReservationInternalConstants.PopMessageType.PFS_MESSAGE,
					deleteMessages);
		} catch (Exception e) {
			log.error(e);
		}
	}
}