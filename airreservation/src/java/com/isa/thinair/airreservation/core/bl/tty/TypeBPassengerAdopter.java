package com.isa.thinair.airreservation.core.bl.tty;

import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;

public class TypeBPassengerAdopter {

	public static void adoptPax(NameDTO paxName, ReservationPax reservationPax) {
		paxName.setPaxTitle(reservationPax.getTitle());
		paxName.setFirstName(reservationPax.getFirstName());
		paxName.setLastName(reservationPax.getLastName());
		paxName.setFamilyID(reservationPax.getFamilyID());
	}

	public static void adoptChild(SSRChildDTO childDTO, ReservationPax reservationPax) {
		childDTO.setTitle(reservationPax.getTitle());
		childDTO.setFirstName(reservationPax.getFirstName());
		childDTO.setLastName(reservationPax.getLastName());
		childDTO.setDateOfBirth(reservationPax.getDateOfBirth());
	}

	public static void adoptInfant(SSRInfantDTO infantDTO, ReservationPax reservationPax) {
		infantDTO.setInfantTitle(reservationPax.getTitle());
		infantDTO.setInfantFirstName(reservationPax.getFirstName());
		infantDTO.setInfantLastName(reservationPax.getLastName());
		infantDTO.setInfantDateofBirth(reservationPax.getDateOfBirth());
		infantDTO.setGuardianTitle(reservationPax.getParent().getTitle());
		infantDTO.setGuardianFirstName(reservationPax.getParent().getFirstName());
		infantDTO.setGuardianLastName(reservationPax.getParent().getLastName());
	}
}
