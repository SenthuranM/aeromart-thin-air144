package com.isa.thinair.airreservation.core.bl.ticketing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.ets.DateAndTimeInformation;
import com.isa.thinair.commons.api.dto.ets.ExcessBaggageInfo;
import com.isa.thinair.commons.api.dto.ets.FareOndInfoDTO;
import com.isa.thinair.commons.api.dto.ets.FlightInformation;
import com.isa.thinair.commons.api.dto.ets.FormOfPayment;
import com.isa.thinair.commons.api.dto.ets.FormOfPaymentIndicator;
import com.isa.thinair.commons.api.dto.ets.FrequentTravellerInformation;
import com.isa.thinair.commons.api.dto.ets.InteractiveFreeText;
import com.isa.thinair.commons.api.dto.ets.IssueExchangeTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.Location;
import com.isa.thinair.commons.api.dto.ets.MessageFunction;
import com.isa.thinair.commons.api.dto.ets.MessageHeader;
import com.isa.thinair.commons.api.dto.ets.MessageIdentifier;
import com.isa.thinair.commons.api.dto.ets.MonetaryInformation;
import com.isa.thinair.commons.api.dto.ets.NumberOfUnits;
import com.isa.thinair.commons.api.dto.ets.OriginDestinationInformation;
import com.isa.thinair.commons.api.dto.ets.OriginatorInformation;
import com.isa.thinair.commons.api.dto.ets.PricingTicketingDetails;
import com.isa.thinair.commons.api.dto.ets.PricingTicketingDetails.PricingTicketingIndicators;
import com.isa.thinair.commons.api.dto.ets.PricingTicketingSubsequent;
import com.isa.thinair.commons.api.dto.ets.RelatedProductInfo;
import com.isa.thinair.commons.api.dto.ets.RequestIdentity;
import com.isa.thinair.commons.api.dto.ets.ReservationControlInformation;
import com.isa.thinair.commons.api.dto.ets.SystemDetails;
import com.isa.thinair.commons.api.dto.ets.TaxDetails;
import com.isa.thinair.commons.api.dto.ets.TaxDetails.Tax;
import com.isa.thinair.commons.api.dto.ets.TicketCoupon;
import com.isa.thinair.commons.api.dto.ets.TicketNumberDetails;
import com.isa.thinair.commons.api.dto.ets.TicketingAgentInformation;
import com.isa.thinair.commons.api.dto.ets.TicketingAgentInformation.InternalIdentificationDetails;
import com.isa.thinair.commons.api.dto.ets.Traveller;
import com.isa.thinair.commons.api.dto.ets.TravellerTicketingInformation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for finalize the assembled reservation
 *
 * @isa.module.command name="issueTicket"
 */
public class IssueTicket extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(IssueTicket.class);
	
	private final String EMPTY_STRING = "";
	
	private final String SPACE_STRING = " ";

	/**
	 * Execute method of the IssueTicket command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");
		String pnr = (String) this.getParameter(CommandParamNames.PNR);

		Reservation reservation = null;
		if (pnr != null) {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			pnrModesDTO.setLoadEtickets(true);
			pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
			pnrModesDTO.setLoadOndChargesView(true);
			reservation = ReservationProxy.getReservation(pnrModesDTO);

			if (AppSysParamsUtil.isEnableAeroMartETS() && reservation.isETRecordedInAeroMartETS()) {
				try {
					ReservationModuleUtils.getETSTicketServiceBD().issue(generateIssueExchangeTKTREQ(reservation));
				} catch (Exception e) {
					log.info(e);
				}
			}
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		if (output != null) {
			response = output;
		}
		if (pnr != null) {
			response.addResponceParam(CommandParamNames.ON_HOLD_PNR, pnr);
		}
		Map<String, String> paymentAuthIdMap = (Map<String, String>) this.getParameter(CommandParamNames.PAYMENT_AUTHID_MAP);
		if (paymentAuthIdMap != null) {
			response.addResponceParam(CommandParamNames.PAYMENT_AUTHID_MAP, paymentAuthIdMap);
		}
		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> updatedFlexibilitiesMap = (Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>>) this
				.getParameter(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP);
		if (updatedFlexibilitiesMap != null && !updatedFlexibilitiesMap.isEmpty()) {
			response.addResponceParam(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP, updatedFlexibilitiesMap);
		}
		if (reservation != null) {
			response.addResponceParam(CommandParamNames.VERSION, reservation.getVersion());
			response.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS,
					this.getParameter(CommandParamNames.FLIGHT_SEGMENT_IDS));
			response.addResponceParam(CommandParamNames.PNR, this.getParameter(CommandParamNames.PNR));
		}
		log.debug("Exit execute");
		return response;
	}

	private IssueExchangeTKTREQDTO generateIssueExchangeTKTREQ(Reservation reservation) throws ModuleException {

		IssueExchangeTKTREQDTO issueExchangeTKTREQDTO = new IssueExchangeTKTREQDTO();

		RequestIdentity requestIdentity = RequestIdentity.getInstance(AppSysParamsUtil.getDefaultCarrierCode(),
				TypeAConstants.RequestIdentitySystem.ETS);
		issueExchangeTKTREQDTO.setRequestIdentity(requestIdentity);

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.ISSUE);
		issueExchangeTKTREQDTO.setMessageFunction(messageFunction);

		OriginatorInformation originatorInformation = new OriginatorInformation();
		originatorInformation.setAgentId(reservation.getAdminInfo().getOriginAgentCode());
		issueExchangeTKTREQDTO.setOriginatorInformation(originatorInformation);

		List<ReservationControlInformation> reservationControlInformationList = new ArrayList<ReservationControlInformation>();
		ReservationControlInformation reservationControlInformation1 = new ReservationControlInformation();
		reservationControlInformation1.setAirlineCode(AppSysParamsUtil.getDefaultCarrierCode());
		reservationControlInformation1.setReservationControlNumber(reservation.getPnr());
		reservationControlInformation1.setReservationControlType(TypeAConstants.ReservationControlType.SYSTEM_REFERENCE);
		reservationControlInformationList.add(reservationControlInformation1);
		issueExchangeTKTREQDTO.setReservationControlInformation(reservationControlInformationList);

		List<InteractiveFreeText> textList = new ArrayList<InteractiveFreeText>();
		InteractiveFreeText text = new InteractiveFreeText();
		text.setTextSubjectQualifier(TypeAConstants.TextSubjectQualifier.CODED_AND_LITERAL_TEXT);
		text.setInformationType(TypeAConstants.InformationType.ISSUING_AGENCY);
		List<String> freeTextList = new ArrayList<String>();
		freeTextList.add(reservation.getAdminInfo().getOriginAgentCode());
		text.setFreeText(freeTextList);
		textList.add(text);
		issueExchangeTKTREQDTO.setText(textList);

		Map<Integer, ReservationSegmentDTO> segmentMap = new HashMap<Integer, ReservationSegmentDTO>();
		Iterator<ReservationSegmentDTO> iteReservationSegment = reservation.getSegmentsView().iterator();
		ReservationSegmentDTO reservationSegment;
		while (iteReservationSegment.hasNext()) {
			reservationSegment = iteReservationSegment.next();
			segmentMap.put(reservationSegment.getPnrSegId(), reservationSegment);
		}

		int numberOfTickets = 0;
		List<TravellerTicketingInformation> travellerTicketingInformationList = new ArrayList<TravellerTicketingInformation>();
		Iterator<ReservationPax> iteReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();

			TravellerTicketingInformation travellerTicketingInformation = new TravellerTicketingInformation();
			travellerTicketingInformation.setTravellerSurname(reservationPax.getLastName());
			if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())) {
				travellerTicketingInformation.setTravellerQualifier(TypeAConstants.TravellerQualifier.ADULT);
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType())) {
				travellerTicketingInformation.setTravellerQualifier(TypeAConstants.TravellerQualifier.CHILD);
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())) {
				travellerTicketingInformation.setTravellerQualifier(TypeAConstants.TravellerQualifier.INFANT);
			}

			List<Traveller> travellers = new ArrayList<Traveller>();
			Traveller traveller = new Traveller();
			traveller.setTitle(reservationPax.getTitle());
			traveller.setGivenName(reservationPax.getFirstName());
			travellers.add(traveller);
			travellerTicketingInformation.setTravellers(travellers);

			Collection<ReservationTnx> paxPaymentTnxs = reservationPax.getPaxPaymentTnxView();
			List<FormOfPayment> formOfPaymentList = new ArrayList<FormOfPayment>();
			BigDecimal totalPayAmount = BigDecimal.ZERO;
			String totalPayCurrencyCode = null;
			for (ReservationTnx paxPaymentTnx : paxPaymentTnxs) {
				if (ReservationTnxNominalCode.getPaymentTypeNominalCodes().contains(paxPaymentTnx.getNominalCode())) {
					FormOfPayment formOfPayment = new FormOfPayment();
					if (ReservationTnxNominalCode.getCreditCardNominalCodes().contains(paxPaymentTnx.getNominalCode())) {
						formOfPayment.setFormOfPaymentType(FormOfPaymentIndicator.CC);
					} else if (ReservationTnxNominalCode.getOnAccountTypeNominalCodes().contains(paxPaymentTnx.getNominalCode())
							|| ReservationTnxNominalCode.getBSPAccountTypeNominalCodes().contains(paxPaymentTnx.getNominalCode())) {
						formOfPayment.setFormOfPaymentType(FormOfPaymentIndicator.AGT);
					} else {
						formOfPayment.setFormOfPaymentType(FormOfPaymentIndicator.CA);
					}
					formOfPayment.setPaymentAmount(paxPaymentTnx.getAmount().negate().toString());
					totalPayAmount = AccelAeroCalculator.add(totalPayAmount, paxPaymentTnx.getAmount().negate());
					totalPayCurrencyCode = AppSysParamsUtil.getBaseCurrency();
					formOfPaymentList.add(formOfPayment);
				}
			}
			travellerTicketingInformation.setFormOfPayment(formOfPaymentList);

			Map<Integer, ChargesDetailDTO> chargeRateIdMap = new HashMap<Integer, ChargesDetailDTO>();
			if (reservationPax.getOndChargesView() != null) {
				for (ChargesDetailDTO chargeDetailsDTO : reservationPax.getOndChargesView()) {
					chargeRateIdMap.put(chargeDetailsDTO.getChargeRateId(), chargeDetailsDTO);
				}
			}
			Set<ReservationPaxFare> colResPaxFares = reservationPax.getPnrPaxFares();
			ChargesDetailDTO chargesDetailDTO = null;
			BigDecimal totalFareForPax = BigDecimal.ZERO;
			Map<String, Tax> taxesMap = new HashMap<String, Tax>();
			List<FareOndInfoDTO> fareOndInfoDTOs = new ArrayList<FareOndInfoDTO>();
			for (ReservationPaxFare reservationPaxFare : colResPaxFares) {
				FareOndInfoDTO fareOndInfoDTO = new FareOndInfoDTO();
				List<Integer> pnrSegIds = new ArrayList<Integer>();
				for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
					pnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
				}
				fareOndInfoDTO.setPnrSegIds(pnrSegIds);
				Collection<ReservationPaxOndCharge> colCharge = reservationPaxFare.getCharges();
				if (colCharge != null) {
					for (ReservationPaxOndCharge chgDetail : colCharge) {
						if (chgDetail.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
							totalFareForPax = AccelAeroCalculator.add(totalFareForPax, chgDetail.getAmount());
							fareOndInfoDTO.setFareAmount(AccelAeroCalculator.formatAsDecimal(chgDetail.getAmount()));
						} else {
							if (chgDetail.getChargeRateId() != null) {
								chargesDetailDTO = chargeRateIdMap.get(chgDetail.getChargeRateId());
								if (chargesDetailDTO != null) {
									if (taxesMap.containsKey(chargesDetailDTO.getChargeCode())) {
										Tax existingTax = taxesMap.get(chargesDetailDTO.getChargeCode());
										BigDecimal newAmount = AccelAeroCalculator.add(chgDetail.getAmount(), new BigDecimal(
												existingTax.getAmount()));
										existingTax.setAmount(newAmount.toString());
										taxesMap.put(chargesDetailDTO.getChargeCode(), existingTax);
									} else {
										Tax taxTo = new Tax();
										taxTo.setAmount(chgDetail.getAmount().toString());
										taxTo.setType(chargesDetailDTO.getChargeCode());
										taxTo.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
										taxesMap.put(chargesDetailDTO.getChargeCode(), taxTo);
									}
								}
							}
						}
					}
				}
				fareOndInfoDTOs.add(fareOndInfoDTO);
			}

			List<MonetaryInformation> monetaryInformationList = new ArrayList<MonetaryInformation>();
			MonetaryInformation monetaryInformation1 = new MonetaryInformation();
			monetaryInformation1.setAmountTypeQualifier(TypeAConstants.MonetaryType.BASE_FARE);
			monetaryInformation1.setAmount(totalFareForPax.toString());
			monetaryInformation1.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
			monetaryInformationList.add(monetaryInformation1);
			MonetaryInformation monetaryInformationTotal = new MonetaryInformation();
			monetaryInformationTotal.setAmountTypeQualifier(TypeAConstants.MonetaryType.TICKET_TOTAL);
			monetaryInformationTotal.setAmount(totalPayAmount.toString());
			monetaryInformationTotal.setCurrencyCode(totalPayCurrencyCode);
			monetaryInformationList.add(monetaryInformationTotal);
			travellerTicketingInformation.setMonetaryInformation(monetaryInformationList);

			List<TaxDetails> taxDetailsList = new ArrayList<TaxDetails>();
			TaxDetails taxDetails = new TaxDetails();
			List<Tax> taxes = new ArrayList<Tax>();
			for (Entry<String, Tax> taxEntry : taxesMap.entrySet()) {
				taxes.add(taxEntry.getValue());
			}
			taxDetails.setTax(taxes);
			taxDetailsList.add(taxDetails);
			travellerTicketingInformation.setTaxDetails(taxDetailsList);

			List<InteractiveFreeText> ttiText = new ArrayList<InteractiveFreeText>();
			InteractiveFreeText interactiveFreeText = new InteractiveFreeText();
			interactiveFreeText.setTextSubjectQualifier(TypeAConstants.TextSubjectQualifier.CODED_AND_LITERAL_TEXT);
			interactiveFreeText.setInformationType(TypeAConstants.InformationType.FARE_CALCULATION);
			List<String> freeTxtList = new ArrayList<String>();
			
			String fareEntryOrigin = null;
			String fareEntryMiddle = EMPTY_STRING;
			for (FareOndInfoDTO fareOndInfoDTO : fareOndInfoDTOs) {
				String fareEntry = null;
				String fareBasisCode = null;
				for (Integer pnrSegId : fareOndInfoDTO.getPnrSegIds()) {
					ReservationSegmentDTO resSegment = segmentMap.get(pnrSegId);
					String ocCarrierCode = resSegment.getCsOcCarrierCode() != null ? resSegment.getCsOcCarrierCode() : resSegment
							.getCarrierCode();
					if (resSegment != null) {
						if (fareEntryOrigin == null) {
							fareEntryOrigin = resSegment.getOrigin() + SPACE_STRING;
						}
						if (fareEntry == null) {
							if (fareOndInfoDTO.getPnrSegIds().size() > 1) {
								fareEntry = ocCarrierCode + " X/" + resSegment.getDestination();
							} else {
								fareEntry = ocCarrierCode + SPACE_STRING + resSegment.getDestination();
							}
						} else {
							fareEntry = fareEntry + SPACE_STRING + ocCarrierCode + SPACE_STRING + resSegment.getDestination();
						}
						fareBasisCode = resSegment.getFareTO().getFareBasisCode() + "1";
						// TODO: TypeA farebasisCode reg ex expects number at the end. So adding 1. Remove this later.
					}
				}
				fareEntry = fareEntry + fareOndInfoDTO.getFareAmount() + fareBasisCode;
				fareEntryMiddle = fareEntryMiddle + fareEntry + SPACE_STRING;
			}

			String fareEntryEnd = AppSysParamsUtil.getBaseCurrency() + totalFareForPax.toString() + "END";
			freeTxtList.add(fareEntryOrigin + fareEntryMiddle + fareEntryEnd);
			interactiveFreeText.setFreeText(freeTxtList);
			ttiText.add(interactiveFreeText);
			travellerTicketingInformation.setText(ttiText);

			PricingTicketingDetails pricingTicketingDetails = new PricingTicketingDetails();
			List<PricingTicketingIndicators> pricingTicketingIndicatorsList = new ArrayList<PricingTicketingIndicators>();
			PricingTicketingIndicators pricingTicketingIndicators = new PricingTicketingIndicators();
			pricingTicketingIndicators.setKey("01");
			pricingTicketingIndicators.setValue("N");
			pricingTicketingIndicatorsList.add(pricingTicketingIndicators);
			pricingTicketingDetails.setPricingTicketingIndicators(pricingTicketingIndicatorsList);
			pricingTicketingDetails.setTicketingTime(CalendarUtil.formatDate(new Date(), CalendarUtil.PATTERN14));
			travellerTicketingInformation.setPricingTicketingDetails(pricingTicketingDetails);

			int couponCount = 0;
			Map<String, List<TicketCoupon>> ticketCouponsMap = new HashMap<String, List<TicketCoupon>>();
			Collection<EticketTO> eTickets = reservationPax.geteTickets();
			for (EticketTO eTicket : eTickets) {
				ReservationSegmentDTO resSegment = segmentMap.get(eTicket.getPnrSegId());
				if (resSegment != null) {
					List<TicketCoupon> ticketCoupons;
					if (ticketCouponsMap.keySet().contains(eTicket.getEticketNumber())) {
						ticketCoupons = ticketCouponsMap.get(eTicket.getEticketNumber());
					} else {
						ticketCoupons = new ArrayList<TicketCoupon>();
					}

					TicketCoupon coupon = new TicketCoupon();
					coupon.setCouponNumber(eTicket.getCouponNo().toString());
					coupon.setStatus(eTicket.getTicketStatus());

					FlightInformation flightInformation = new FlightInformation();
					flightInformation.setDepartureDateTime(resSegment.getDepartureStringDate(CalendarUtil.PATTERN14));
					Location departureLocation = new Location();
					departureLocation.setLocationCode(resSegment.getOrigin());
					flightInformation.setDepartureLocation(departureLocation);
					Location arrivalLocation = new Location();
					arrivalLocation.setLocationCode(resSegment.getDestination());
					flightInformation.setArrivalLocation(arrivalLocation);
					FlightInformation.CompanyInfo companyInfo = new FlightInformation.CompanyInfo();
					String flightNo = resSegment.getFlightNo();
					companyInfo.setMarketingAirlineCode(flightNo.substring(0, 2));
					if (resSegment.getCsOcCarrierCode() != null) {
						companyInfo.setOperatingAirlineCode(resSegment.getCsOcCarrierCode());
					}
					flightInformation.setCompanyInfo(companyInfo);
					flightInformation.setFlightNumber(flightNo.substring(2));
					if (resSegment.getFareTO() != null && resSegment.getFareTO().getBookingClassCode() != null) {
						FlightInformation.FlightPreference flightPreference = new FlightInformation.FlightPreference();
						flightPreference.setBookingClass(resSegment.getFareTO().getBookingClassCode());
						flightInformation.setFlightPreference(flightPreference);
					}
					flightInformation.setActionCode(TypeAConstants.FlightSegmentActionCode.OK);
					coupon.setFlightInformation(flightInformation);

					if (resSegment.getFareTO() != null && resSegment.getFareTO().getFareBasisCode() != null) {
						PricingTicketingSubsequent pricingTicketingSubsequent = new PricingTicketingSubsequent();
						pricingTicketingSubsequent.setFareBasis(resSegment.getFareTO().getFareBasisCode());
						coupon.setPricingTicketingSubsequent(pricingTicketingSubsequent);
					}

					if (resSegment.getTicketValidTill() != null) {
						List<DateAndTimeInformation> dateAndTimeInformationList = new ArrayList<DateAndTimeInformation>();
						DateAndTimeInformation dateAndTimeInformation = new DateAndTimeInformation();
						dateAndTimeInformation.setQualifier(TypeAConstants.DateTimePeriodQualifier.NOT_VALID_AFTER);
						dateAndTimeInformation.setDate(CalendarUtil.formatDate(resSegment.getTicketValidTill(),
								CalendarUtil.PATTERN14));
						dateAndTimeInformationList.add(dateAndTimeInformation);
						coupon.setDateAndTimeInformation(dateAndTimeInformationList);
					}
					ticketCoupons.add(coupon);
					ticketCouponsMap.put(eTicket.getEticketNumber(), ticketCoupons);
				}
			}
			List<TicketNumberDetails> tickets = new ArrayList<TicketNumberDetails>();
			for (Entry<String, List<TicketCoupon>> ticketCouponsEntry : ticketCouponsMap.entrySet()) {
				TicketNumberDetails ticket = new TicketNumberDetails();
				ticket.setTicketNumber(ticketCouponsEntry.getKey());
				ticket.setDocumentType(TypeAConstants.DocumentType.TICKET);
				ticket.setDataIndicator(TypeAConstants.TicketDataIndicator.NEW);
				ticket.setTicketCoupon(ticketCouponsEntry.getValue());
				tickets.add(ticket);
				couponCount = couponCount + ticketCouponsEntry.getValue().size();
			}
			travellerTicketingInformation.setTickets(tickets);

			NumberOfUnits ttiNumberOfUnits = new NumberOfUnits();
			ttiNumberOfUnits.setQuantity(Integer.toString(couponCount));
			ttiNumberOfUnits.setQualifier(TypeAConstants.NumberOfUnitsQualifier.COUPONS_COUNT);
			travellerTicketingInformation.setNumberOfUnits(ttiNumberOfUnits);

			travellerTicketingInformationList.add(travellerTicketingInformation);
			numberOfTickets = numberOfTickets + ticketCouponsMap.keySet().size();
		}
		issueExchangeTKTREQDTO.setTravellerTicketingInformation(travellerTicketingInformationList);

		NumberOfUnits numberOfUnits = new NumberOfUnits();
		numberOfUnits.setQuantity(Integer.toString(numberOfTickets));
		numberOfUnits.setQualifier(TypeAConstants.NumberOfUnitsQualifier.TICKETS_COUNT);
		issueExchangeTKTREQDTO.setNumberOfUnits(numberOfUnits);

		return issueExchangeTKTREQDTO;
	}

	private IssueExchangeTKTREQDTO generateTestIssueExchangeTKTREQ() {

		IssueExchangeTKTREQDTO issueExchangeTKTREQDTO = new IssueExchangeTKTREQDTO();

		RequestIdentity requestIdentity = RequestIdentity.getInstance(AppSysParamsUtil.getDefaultCarrierCode(),
				TypeAConstants.RequestIdentitySystem.ETS);
		issueExchangeTKTREQDTO.setRequestIdentity(requestIdentity);

		MessageHeader messageHeader = new MessageHeader();
		messageHeader.setReferenceNo("1");
		messageHeader.setCommonAccessReference("092D51350000A6");
		MessageIdentifier messageIdentifier = new MessageIdentifier();
		messageIdentifier.setMessageType("TKTREQ");
		messageIdentifier.setVersion("08");
		messageIdentifier.setReleaseNumber("1");
		messageIdentifier.setControllingAgency("AT");
		messageHeader.setMessageId(messageIdentifier);
		issueExchangeTKTREQDTO.setMessageHeader(messageHeader);

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction("130");
		issueExchangeTKTREQDTO.setMessageFunction(messageFunction);

		OriginatorInformation originatorInformation = new OriginatorInformation();
		SystemDetails senderSystemDetails = new SystemDetails();
		senderSystemDetails.setCompanyCode("1A");
		Location senderSystemDetailsLocation = new Location();
		senderSystemDetailsLocation.setLocationCode("MUC");
		senderSystemDetails.setLocation(senderSystemDetailsLocation);
		originatorInformation.setSenderSystemDetails(senderSystemDetails);
		originatorInformation.setAgentId("33676381");
		originatorInformation.setOfficeId("067381");
		Location agentLocation = new Location();
		agentLocation.setLocationCode("MIA");
		originatorInformation.setAgentLocation(agentLocation);
		originatorInformation.setOriginatorTypeCode("T");
		originatorInformation.setAgentCountryCode("US");
		originatorInformation.setAgentCurrencyCode("USD");
		originatorInformation.setAgentLanguageCode("EN");
		originatorInformation.setOriginatorAuthorityCode("A0001AASU");
		originatorInformation.setCommunicationNumber("00145428");
		originatorInformation.setPartyId("1A");
		issueExchangeTKTREQDTO.setOriginatorInformation(originatorInformation);

		TicketingAgentInformation ticketingAgentInformation = new TicketingAgentInformation();
		ticketingAgentInformation.setCompanyIdentifier("7906");
		List<InternalIdentificationDetails> internalIdentificationDetailsList = new ArrayList<InternalIdentificationDetails>();
		InternalIdentificationDetails internalIdentificationDetails = new InternalIdentificationDetails();
		internalIdentificationDetails.setRequesterId("AA/SU");
		internalIdentificationDetails.setIdentificationType("B");
		internalIdentificationDetailsList.add(internalIdentificationDetails);
		ticketingAgentInformation.setInternalIdentificationDetails(internalIdentificationDetailsList);
		issueExchangeTKTREQDTO.setTicketingAgentInformation(ticketingAgentInformation);

		List<ReservationControlInformation> reservationControlInformationList = new ArrayList<ReservationControlInformation>();
		ReservationControlInformation reservationControlInformation1 = new ReservationControlInformation();
		reservationControlInformation1.setAirlineCode("RM");
		reservationControlInformation1.setReservationControlNumber("MKM3FD");
		reservationControlInformation1.setReservationControlType("1");
		reservationControlInformationList.add(reservationControlInformation1);
		// ReservationControlInformation reservationControlInformation2 = new ReservationControlInformation();
		// reservationControlInformation2.setAirlineCode("CC");
		// reservationControlInformation2.setReservationControlNumber("CCO627");
		// reservationControlInformation2.setReservationControlType("1");
		// reservationControlInformationList.add(reservationControlInformation2);
		issueExchangeTKTREQDTO.setReservationControlInformation(reservationControlInformationList);

		NumberOfUnits numberOfUnits = new NumberOfUnits();
		numberOfUnits.setQuantity("2");
		numberOfUnits.setQualifier("TD");
		issueExchangeTKTREQDTO.setNumberOfUnits(numberOfUnits);

		List<InteractiveFreeText> textList = new ArrayList<InteractiveFreeText>();
		InteractiveFreeText text = new InteractiveFreeText();
		text.setTextSubjectQualifier("4");
		text.setInformationType("39");
		List<String> freeTextList = new ArrayList<String>();
		freeTextList.add("MIAMI FL 33130");
		freeTextList.add("TOTAL TRAVEL ENTERPRIS");
		text.setFreeText(freeTextList);
		textList.add(text);
		issueExchangeTKTREQDTO.setText(textList);

		List<TravellerTicketingInformation> travellerTicketingInformationList = new ArrayList<TravellerTicketingInformation>();
		TravellerTicketingInformation travellerTicketingInformation = new TravellerTicketingInformation();
		travellerTicketingInformation.setTravellerSurname("Dhanushka");
		travellerTicketingInformation.setTravellerQualifier("A");
		List<Traveller> travellers = new ArrayList<Traveller>();
		Traveller traveller = new Traveller();
		traveller.setTitle("Mr");
		traveller.setGivenName("Manoj");
		travellers.add(traveller);
		travellerTicketingInformation.setTravellers(travellers);
		List<ReservationControlInformation> ttiReservationControlInformationList = new ArrayList<ReservationControlInformation>();
		ReservationControlInformation ttiReservationControlInformation = new ReservationControlInformation();
		ttiReservationControlInformation.setReservationControlNumber("111111");
		ttiReservationControlInformation.setReservationControlType("A");
		ttiReservationControlInformationList.add(ttiReservationControlInformation);
		travellerTicketingInformation.setReservationControlInformation(ttiReservationControlInformationList);
		List<MonetaryInformation> monetaryInformationList = new ArrayList<MonetaryInformation>();
		MonetaryInformation monetaryInformation1 = new MonetaryInformation();
		monetaryInformation1.setAmountTypeQualifier("B");
		monetaryInformation1.setAmount("2935.78");
		monetaryInformation1.setCurrencyCode("USD");
		monetaryInformationList.add(monetaryInformation1);
		MonetaryInformation monetaryInformation2 = new MonetaryInformation();
		monetaryInformation2.setAmountTypeQualifier("T");
		monetaryInformation2.setAmount("3216.00");
		monetaryInformation2.setCurrencyCode("USD");
		monetaryInformationList.add(monetaryInformation2);
		MonetaryInformation monetaryInformation3 = new MonetaryInformation();
		monetaryInformation3.setAmountTypeQualifier("F");
		monetaryInformation3.setAmount("164.31");
		monetaryInformation3.setCurrencyCode("USD");
		monetaryInformationList.add(monetaryInformation3);
		travellerTicketingInformation.setMonetaryInformation(monetaryInformationList);
		List<FormOfPayment> formOfPaymentList = new ArrayList<FormOfPayment>();
		FormOfPayment formOfPayment = new FormOfPayment();
		formOfPayment.setFormOfPaymentType(FormOfPaymentIndicator.CC);
		formOfPayment.setDataIndicator("3");
		formOfPayment.setReferenceNumber("370712345678900");
		FormOfPayment.AdditionalDetails additionalDetails = new FormOfPayment.AdditionalDetails();
		additionalDetails.setVendorCode("AX");
		additionalDetails.setExpirationDate("1205");
		additionalDetails.setApprovalCode("12344");
		additionalDetails.setSourceOfApproval("M");
		formOfPayment.setAdditionalDetails(additionalDetails);
		formOfPaymentList.add(formOfPayment);
		travellerTicketingInformation.setFormOfPayment(formOfPaymentList);
		PricingTicketingDetails pricingTicketingDetails = new PricingTicketingDetails();
		List<PricingTicketingIndicators> pricingTicketingIndicatorsList = new ArrayList<PricingTicketingIndicators>();
		PricingTicketingIndicators pricingTicketingIndicators1 = new PricingTicketingIndicators();
		pricingTicketingIndicators1.setKey("01");
		pricingTicketingIndicators1.setValue("N");
		pricingTicketingIndicatorsList.add(pricingTicketingIndicators1);
		PricingTicketingIndicators pricingTicketingIndicators2 = new PricingTicketingIndicators();
		pricingTicketingIndicators2.setKey("02");
		pricingTicketingIndicators2.setValue("II");
		pricingTicketingIndicatorsList.add(pricingTicketingIndicators2);
		PricingTicketingIndicators pricingTicketingIndicators3 = new PricingTicketingIndicators();
		pricingTicketingIndicators3.setKey("03");
		pricingTicketingIndicators3.setValue("D");
		pricingTicketingIndicatorsList.add(pricingTicketingIndicators3);
		pricingTicketingDetails.setPricingTicketingIndicators(pricingTicketingIndicatorsList);
		pricingTicketingDetails.setTicketingTime("1998-02-09T00:00:00");
		travellerTicketingInformation.setPricingTicketingDetails(pricingTicketingDetails);
		OriginDestinationInformation originDestinationInfo = new OriginDestinationInformation();
		originDestinationInfo.setDepartureAirportCityCode("IKA");
		originDestinationInfo.setArrivalAirportCityCode("DXB");
		travellerTicketingInformation.setOriginDestinationInfo(originDestinationInfo);
		NumberOfUnits ttiNumberOfUnits = new NumberOfUnits();
		ttiNumberOfUnits.setQuantity("4");
		ttiNumberOfUnits.setQualifier("TF");
		travellerTicketingInformation.setNumberOfUnits(ttiNumberOfUnits);
		List<TaxDetails> taxDetailsList = new ArrayList<TaxDetails>();
		TaxDetails taxDetails = new TaxDetails();
		List<Tax> taxes = new ArrayList<Tax>();
		Tax tax1 = new Tax();
		tax1.setAmount("264.22");
		tax1.setCurrencyCode("USD");
		tax1.setType("US");
		taxes.add(tax1);
		Tax tax2 = new Tax();
		tax2.setAmount("4.00");
		tax2.setCurrencyCode("USD");
		tax2.setType("ZP");
		taxes.add(tax2);
		Tax tax3 = new Tax();
		tax3.setAmount("12.00");
		tax3.setCurrencyCode("USD");
		tax3.setType("XF");
		taxes.add(tax3);
		taxDetails.setTax(taxes);
		taxDetailsList.add(taxDetails);
		travellerTicketingInformation.setTaxDetails(taxDetailsList);
		List<InteractiveFreeText> ttiText = new ArrayList<InteractiveFreeText>();
		InteractiveFreeText interactiveFreeText1 = new InteractiveFreeText();
		interactiveFreeText1.setTextSubjectQualifier("4");
		interactiveFreeText1.setInformationType("5");
		List<String> freeTxtList1 = new ArrayList<String>();
		freeTxtList1.add("0492947848");
		interactiveFreeText1.setFreeText(freeTxtList1);
		ttiText.add(interactiveFreeText1);
		InteractiveFreeText interactiveFreeText2 = new InteractiveFreeText();
		interactiveFreeText2.setTextSubjectQualifier("4");
		interactiveFreeText2.setInformationType("10");
		List<String> freeTxtList2 = new ArrayList<String>();
		freeTxtList2.add("NW ONLY/NONREF/CHG FEE");
		interactiveFreeText2.setFreeText(freeTxtList2);
		ttiText.add(interactiveFreeText2);
		InteractiveFreeText interactiveFreeText3 = new InteractiveFreeText();
		interactiveFreeText3.setTextSubjectQualifier("4");
		interactiveFreeText3.setInformationType("15");
		interactiveFreeText3.setStatus("0");
		List<String> freeTxtList3 = new ArrayList<String>();
		freeTxtList3.add("WAS YY X/ATL YY LAX1467.89F06 YY X/ATL YY WAS1467.89F06 USD2935.78END");
		freeTxtList3.add("ZP AD1ATL1LAX1ATL1XF IAD3ATL3LAX3ATL3");
		interactiveFreeText3.setFreeText(freeTxtList3);
		ttiText.add(interactiveFreeText3);
		InteractiveFreeText interactiveFreeText4 = new InteractiveFreeText();
		interactiveFreeText4.setTextSubjectQualifier("4");
		interactiveFreeText4.setInformationType("45");
		List<String> freeTxtList4 = new ArrayList<String>();
		freeTxtList4.add("9991234567890  MIA12JUL9798745632");
		interactiveFreeText4.setFreeText(freeTxtList4);
		ttiText.add(interactiveFreeText4);
		travellerTicketingInformation.setText(ttiText);
		List<TicketNumberDetails> tickets = new ArrayList<TicketNumberDetails>();
		TicketNumberDetails ticket1 = new TicketNumberDetails();
		ticket1.setTicketNumber("4370000000228");
		ticket1.setDocumentType("T");
		ticket1.setDataIndicator("3");
		List<TicketCoupon> ticketCoupons = new ArrayList<TicketCoupon>();
		TicketCoupon coupon1 = new TicketCoupon();
		coupon1.setCouponNumber("1");
		coupon1.setStatus("O");
		FlightInformation flightInformation = new FlightInformation();
		flightInformation.setDepartureDateTime("2017-05-12T05:30:00");
		Location departureLocation = new Location();
		departureLocation.setLocationCode("IKA");
		flightInformation.setDepartureLocation(departureLocation);
		Location arrivalLocation = new Location();
		arrivalLocation.setLocationCode("DXB");
		flightInformation.setArrivalLocation(arrivalLocation);
		FlightInformation.CompanyInfo companyInfo = new FlightInformation.CompanyInfo();
		companyInfo.setMarketingAirlineCode("RM");
		flightInformation.setCompanyInfo(companyInfo);
		flightInformation.setFlightNumber("061");
		FlightInformation.FlightPreference flightPreference = new FlightInformation.FlightPreference();
		flightPreference.setBookingClass("F");
		flightInformation.setFlightPreference(flightPreference);
		List<String> flightRemarks = new ArrayList<String>();
		flightRemarks.add("J");
		flightInformation.setFlightRemarks(flightRemarks);
		flightInformation.setActionCode("OK");
		coupon1.setFlightInformation(flightInformation);
		RelatedProductInfo relatedProductInfo = new RelatedProductInfo();
		List<String> relatedProductInfoQualifier = new ArrayList<String>();
		relatedProductInfoQualifier.add("OK");
		relatedProductInfo.setQualifier(relatedProductInfoQualifier);
		coupon1.setRelatedProductInfo(relatedProductInfo);
		PricingTicketingSubsequent pricingTicketingSubsequent = new PricingTicketingSubsequent();
		pricingTicketingSubsequent.setFareBasis("F06");
		coupon1.setPricingTicketingSubsequent(pricingTicketingSubsequent);
		ExcessBaggageInfo excessBaggageInformation = new ExcessBaggageInfo();
		excessBaggageInformation.setQuantity("20");
		excessBaggageInformation.setChargeQualifier("W");
		excessBaggageInformation.setMeasurementUnit("K");
		coupon1.setExcessBaggageInformation(excessBaggageInformation);
		FrequentTravellerInformation frequentTravellerInformation = new FrequentTravellerInformation();
		frequentTravellerInformation.setAirlineDesignator("RM");
		frequentTravellerInformation.setFrequentTravellerNumber("11316498");
		coupon1.setFrequentTravellerInformation(frequentTravellerInformation);
		List<DateAndTimeInformation> dateAndTimeInformationList = new ArrayList<DateAndTimeInformation>();
		DateAndTimeInformation dateAndTimeInformation1 = new DateAndTimeInformation();
		dateAndTimeInformation1.setQualifier("B");
		dateAndTimeInformation1.setDate("1998-05-12T00:00:00");
		dateAndTimeInformationList.add(dateAndTimeInformation1);
		DateAndTimeInformation dateAndTimeInformation2 = new DateAndTimeInformation();
		dateAndTimeInformation2.setQualifier("B");
		dateAndTimeInformation2.setDate("1998-05-12T00:00:00");
		dateAndTimeInformationList.add(dateAndTimeInformation2);
		coupon1.setDateAndTimeInformation(dateAndTimeInformationList);
		ticketCoupons.add(coupon1);
		ticket1.setTicketCoupon(ticketCoupons);
		tickets.add(ticket1);
		travellerTicketingInformation.setTickets(tickets);
		travellerTicketingInformationList.add(travellerTicketingInformation);
		issueExchangeTKTREQDTO.setTravellerTicketingInformation(travellerTicketingInformationList);

		return issueExchangeTKTREQDTO;
	}
}
