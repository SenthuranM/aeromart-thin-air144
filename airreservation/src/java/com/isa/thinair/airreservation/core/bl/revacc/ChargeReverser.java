/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.revacc.TnxFactory;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for Reverse the Refundable Charges[TAX,SUR,etc]. once this functionality is completed ideally -ADJ can be
 * eliminated for refundable charges.
 * 
 * @author M.Rikaz
 * @since 1.0
 * @isa.module.command name="chargeReverser"
 */
public class ChargeReverser extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ChargeReverser.class);

	private ReservationTnxDAO reservationTnxDao;

	private ReservationCreditDAO reservationCreditDAO;

	private boolean isNoShoTaxReversal;
	private Collection<String> chargeGroups;
	private Collection<String> chargeCodes;
	private Map<Integer, Boolean> refundableChargeByRateId;

	public ChargeReverser() {
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		reservationCreditDAO = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		refundableChargeByRateId = new HashMap<>();
	}

	/**
	 * Execute method of the ChargeReverser command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside ChargeReverser execute");

		String pnr = this.getParameter(CommandParamNames.PNR, String.class);
		Collection<Integer> pnrPaxIds = this.getParameter(CommandParamNames.PNR_PAX_IDS, Collection.class);
		Collection<Integer> pnrSegIds = this.getParameter(CommandParamNames.PNR_SEGMENT_IDS, Collection.class);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		this.isNoShoTaxReversal = getParameter(CommandParamNames.IS_NO_SHOW_TAX_REVERSAL, Boolean.FALSE, Boolean.class);
		this.chargeGroups = this.getParameter(CommandParamNames.REVERSABLE_CHARGE_GROUPS, Collection.class);
		this.chargeCodes = this.getParameter(CommandParamNames.REVERSABLE_CHARGE_CODES, Collection.class);

		boolean isTaxReversed = false;

		validateParams(pnr, pnrPaxIds, pnrSegIds);

		// Get the reservation
		Reservation reservation = loadReservation(pnr);

		if (reservation == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		Map<Integer, Collection<Integer>> noShowSegmentsByPaxId = extractNoShowSegmentsByPnrPaxId(reservation);

		validateNoShowTaxReversalPaxIds(pnrPaxIds, noShowSegmentsByPaxId);

		Map<Integer, Collection<ReservationPaxOndCharge>> addedChargesMap = new HashMap<>();

		// initialize valid charge-groups
		initValidChargeGroups();

		Map<String, Map<String, BigDecimal>> paxWiseChargeGroupWiseTotals = new HashMap<>();
		if (reservation.getPassengers() != null && !reservation.getPassengers().isEmpty()) {
			for (ReservationPax reservationPax : reservation.getPassengers()) {

				Integer pnrPaxId = reservationPax.getPnrPaxId();
				Integer parentPaxId = null;
				Integer associatedPaxId = null;
				if (ReservationApiUtils.isInfantType(reservationPax)) {
					parentPaxId = reservationPax.getParent().getPnrPaxId();
					associatedPaxId = parentPaxId;
				} else if (reservationPax.getInfants() != null) {
					INF_L: for (ReservationPax infantPax : reservationPax.getInfants()) {
						associatedPaxId = infantPax.getPnrPaxId();
						break INF_L;
					}
				}
				Map<String, BigDecimal> chargeGroupWiseTotal = new HashMap<>();

				if (pnrPaxIds.contains(pnrPaxId) || (parentPaxId != null && pnrPaxIds.contains(parentPaxId))) {

					Collection<Integer> paxValidSegIds = getValidSegIdsForChargeReversal(pnrPaxId, pnrSegIds,
							noShowSegmentsByPaxId, associatedPaxId);

					// REVERSE REFUNDABLE CHARGES[TAX,SUR.etc]

					Set<ReservationPaxFare> reservationPaxFareSet = reservationPax.getPnrPaxFares();

					if (reservationPaxFareSet != null && !reservationPaxFareSet.isEmpty()) {
						for (ReservationPaxFare reservationPaxFare : reservationPaxFareSet) {

							Set<ReservationPaxOndCharge> addedCharges = getPaxReversableCharges(reservationPaxFare,
									paxValidSegIds, credentialsDTO);
							if (addedCharges != null && !addedCharges.isEmpty()) {
								reservationPaxFare.getCharges().addAll(addedCharges);
								setPnrPaxFareTotalAmounts(reservationPaxFare);

								if (ReservationApiUtils.isInfantType(reservationPax)) {
									pnrPaxId = reservationPax.getParent().getPnrPaxId();
								}
								isTaxReversed = true;

								addPaxNewChargesMap(pnrPaxId, addedChargesMap, addedCharges);

								for (ReservationPaxOndCharge charge : addedCharges) {
									BigDecimal total = chargeGroupWiseTotal.get(charge.getChargeGroupCode());
									if (total == null) {
										total = AccelAeroCalculator.getDefaultBigDecimalZero();
									}
									total = AccelAeroCalculator.add(total, charge.getEffectiveAmount());
									chargeGroupWiseTotal.put(charge.getChargeGroupCode(), total);
								}
							}

						}
					}

				}
				if (!ReservationApiUtils.isInfantType(reservationPax)) {
					paxWiseChargeGroupWiseTotals.put(getPaxName(reservationPax), chargeGroupWiseTotal);
				}
			}

		}

		if (isTaxReversed) {
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
			ReservationProxy.saveReservation(reservation);

			doAudit(credentialsDTO, pnr, paxWiseChargeGroupWiseTotals);

			addTransactionAndRevenueRecords(reservation, addedChargesMap, credentialsDTO);
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		log.debug("Exit ChargeReverser execute");
		return response;
	}

	private void doAudit(CredentialsDTO credentialsDTO, String pnr,
			Map<String, Map<String, BigDecimal>> paxWiseChargeGroupWiseTotals) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setZuluModificationDate(new Date());
		String modType = AuditTemplateEnum.CHARGES_REVERSED.getCode();

		if (isNoShoTaxReversal) {
			modType = AuditTemplateEnum.NO_SHOW_TAX_REV.getCode();
		}

		reservationAudit.setModificationType(modType);
		reservationAudit.setUserId(credentialsDTO.getUserId());
		reservationAudit.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
		reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
		reservationAudit.setUserNote(null);

		// Setting the amount
		StringBuilder auditContent = new StringBuilder();
		for (Entry<String, Map<String, BigDecimal>> paxChargesEntry : paxWiseChargeGroupWiseTotals.entrySet()) {
			auditContent.append(" [ ");
			auditContent.append(paxChargesEntry.getKey());
			auditContent.append(" :  ");
			for (Entry<String, BigDecimal> paxTotals : paxChargesEntry.getValue().entrySet()) {
				auditContent.append(paxTotals.getKey());
				auditContent.append("=");
				auditContent.append(paxTotals.getValue().abs());
				auditContent.append(" | ");
			}
			auditContent.append("]");
		}

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChargesReversed.CONTENT, auditContent.toString());

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChargesReversed.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChargesReversed.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
	}

	private static BigDecimal getTolalCharges(Collection<ReservationPaxOndCharge> charges) {
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (charges != null && !charges.isEmpty()) {
			for (ReservationPaxOndCharge charge : charges) {
				amount = AccelAeroCalculator.add(amount, charge.getEffectiveAmount());
			}
		}
		return amount.abs();
	}

	private static void setPnrPaxFareTotalAmounts(ReservationPaxFare reservationPaxFare) {
		BigDecimal[] totalCharges = ReservationApiUtils.getChargeTypeAmounts();

		Iterator<ReservationPaxOndCharge> itCharges = reservationPaxFare.getCharges().iterator();
		ReservationPaxOndCharge reservationPaxOndCharge;

		while (itCharges.hasNext()) {
			reservationPaxOndCharge = itCharges.next();
			ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
					reservationPaxOndCharge.getAmount(), totalCharges, reservationPaxOndCharge.getDiscount(),
					reservationPaxOndCharge.getAdjustment());
		}

		reservationPaxFare.setTotalChargeAmounts(totalCharges);
	}

	private Integer retrieveCorrectTransactionSeq(String pnrPaxId) {
		Integer maxTxnSeq = 0;
		try {
			if (pnrPaxId != null) {
				maxTxnSeq = ReservationModuleUtils.getRevenueAccountBD().getPNRPaxMaxTransactionSeq(Integer.parseInt(pnrPaxId));
			}
		} catch (ModuleException e) {
			maxTxnSeq = 1;
		}
		return maxTxnSeq;
	}

	private boolean isChargeReversable(ReservationPaxOndCharge reservationPaxOndCharge) {

		if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() > 0
				&& chargeGroups.contains(reservationPaxOndCharge.getChargeGroupCode())
				&& "N".equalsIgnoreCase(reservationPaxOndCharge.getRefundableOperation()) && (chargeCodes.isEmpty()
						|| (!chargeCodes.isEmpty() && chargeCodes.contains(reservationPaxOndCharge.getChargeCode())))) {
			boolean isRefundableCharge = false;
			if (!ReservationInternalConstants.ChargeGroup.FAR.equals(reservationPaxOndCharge.getChargeGroupCode())) {
				if (refundableChargeByRateId.keySet().contains(reservationPaxOndCharge.getChargeRateId())) {
					isRefundableCharge = refundableChargeByRateId.get(reservationPaxOndCharge.getChargeRateId());
				} else {
					isRefundableCharge = ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO
							.isRefundableCharge(new Long(reservationPaxOndCharge.getPnrPaxOndChgId()));
					refundableChargeByRateId.put(reservationPaxOndCharge.getChargeRateId(), isRefundableCharge);
				}
			}

			return isRefundableCharge;
		}

		return false;
	}

	private Set<ReservationPaxOndCharge> getPaxReversableCharges(ReservationPaxFare reservationPaxFare,
			Collection<Integer> paxValidSegIds, CredentialsDTO credentialsDTO) {
		Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();
		Set<ReservationPaxOndCharge> addedCharges = new HashSet<>();
		Collection<Integer> availableSegIds = new ArrayList<>();
		if (paxFareSegments != null && !paxFareSegments.isEmpty()) {
			for (ReservationPaxFareSegment paxFareSegment : paxFareSegments) {
				ReservationSegment segment = paxFareSegment.getSegment();

				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus())) {
					availableSegIds.add(segment.getPnrSegId());
				}

			}

		}

		if (isNoShoTaxReversal && !availableSegIds.isEmpty() && paxValidSegIds.containsAll(availableSegIds)) {
			// refund allowed segment
			log.info("Reversing Charges from ");

			Set<ReservationPaxOndCharge> charges = reservationPaxFare.getCharges();
			Set<ReservationPaxOndCharge> reveresedCharges = new HashSet<>();

			if (charges != null && !charges.isEmpty()) {

				Map<String, BigDecimal> groupWiseRefundableTotals = groupWiseAvailableRefundableTotal(charges);
				if (groupWiseRefundableTotals != null && !groupWiseRefundableTotals.isEmpty()) {
					for (String chargeGroup : groupWiseRefundableTotals.keySet()) {

						BigDecimal availableChargeTotal = groupWiseRefundableTotals.get(chargeGroup);
						log.info("Reversing Charges from Group - " + chargeGroup + " - " + availableChargeTotal);
						if (availableChargeTotal.doubleValue() > 0) {
							Set<ReservationPaxOndCharge> filteredCharges = filterChargesGroupWise(charges, chargeGroup);
							if (filteredCharges != null && !filteredCharges.isEmpty()) {
								for (ReservationPaxOndCharge charge : filteredCharges) {
									if (isChargeReversable(charge) && availableChargeTotal.doubleValue() > 0) {
										BigDecimal chargeEffectiveAmount = charge.getEffectiveAmount();

										availableChargeTotal = AccelAeroCalculator.subtract(availableChargeTotal,
												chargeEffectiveAmount);
										ReservationPaxOndCharge reveresedCharge = charge.cloneForNew(credentialsDTO, true);
										reveresedCharge.setAmount(chargeEffectiveAmount.negate());
										reveresedCharge.setAdjustment(AccelAeroCalculator.getDefaultBigDecimalZero());
										reveresedCharge.setDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
										reveresedCharge.setReservationPaxFare(reservationPaxFare);
										reveresedCharges.add(reveresedCharge);

									}
								}
							}

						}

					}

				}

				if (!reveresedCharges.isEmpty()) {
					charges.addAll(reveresedCharges);
					addedCharges.addAll(reveresedCharges);
				} else {
					log.info("Requested Passenger doesn't have Reversable charges");
				}

			}

		}

		return addedCharges;
	}

	private void addPaxNewChargesMap(Integer pnrPaxId, Map<Integer, Collection<ReservationPaxOndCharge>> paxReversedChargesMap,
			Set<ReservationPaxOndCharge> addedCharges) {

		if (addedCharges != null && !addedCharges.isEmpty()) {

			if (paxReversedChargesMap.get(pnrPaxId) != null) {
				paxReversedChargesMap.get(pnrPaxId).addAll(addedCharges);
			} else {
				paxReversedChargesMap.put(pnrPaxId, addedCharges);
			}

		}

	}

	private void addTransactionAndRevenueRecords(Reservation reservation,
			Map<Integer, Collection<ReservationPaxOndCharge>> addedChargesMap, CredentialsDTO credentialsDTO)
			throws ModuleException {
		if (reservation.getPassengers() != null && !reservation.getPassengers().isEmpty() && addedChargesMap != null
				&& !addedChargesMap.isEmpty()) {

			for (ReservationPax reservationPax : reservation.getPassengers()) {
				Integer pnrPaxId = reservationPax.getPnrPaxId();
				if (!ReservationApiUtils.isInfantType(reservationPax) && addedChargesMap.containsKey(pnrPaxId)) {

					String userNotes = "Charge adjustment";

					Collection<ReservationPaxOndCharge> paxAddedCharges = addedChargesMap.get(reservationPax.getPnrPaxId());

					BigDecimal adjustedAmount = getTolalCharges(paxAddedCharges);

					if (adjustedAmount.doubleValue() > 0) {

						ReservationTnx reservationTnx = TnxFactory.getCreditInstance(pnrPaxId.toString(), adjustedAmount,
								ReservationTnxNominalCode.CHARGE_REVERSAL.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), null, null, null, false, true);
						reservationTnx.setRemarks(userNotes);
						reservationTnxDao.saveTransaction(reservationTnx);

						// If expiryDate is null, system will pick default system expire period
						ReservationCredit credit = CreditFactory.getInstance(reservationTnx.getTnxId().intValue(),
								pnrPaxId.toString(), adjustedAmount.abs(), null);
						// re-instate credit tnx id updated as reference, required in refund flow to check the
						// tnx-breakdown
						// flow
						Integer paymentRefTxnId = (Integer) this.getParameter(CommandParamNames.PAX_TXN_ID);
						if (paymentRefTxnId != null) {
							credit.setCreditReInstatedTnxId(paymentRefTxnId);
						}
						reservationCreditDAO.saveReservationCredit(credit);

						ReservationPaxPaymentMetaTO composedResPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
						composedResPaxPaymentMetaTO.setPnrPaxId(pnrPaxId);
						composedResPaxPaymentMetaTO
								.addColPerPaxWiseOndNewCharges(addedChargesMap.get(reservationPax.getPnrPaxId()));

						Integer transactionSeq = retrieveCorrectTransactionSeq(pnrPaxId.toString());

						TnxGranularityFactory.saveReservationPaxTnxBreakdownForChargesReversal(pnrPaxId.toString(),
								reservationTnx, composedResPaxPaymentMetaTO, true, transactionSeq);

					}

				}
			}
		}
	}

	private Reservation loadReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);

		// Get the reservation
		return ReservationProxy.getReservation(pnrModesDTO);
	}

	private Map<Integer, Collection<Integer>> extractNoShowSegmentsByPnrPaxId(Reservation reservation) {
		Map<Integer, Collection<Integer>> noShowSegmentsByPaxId = new HashMap<>();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			Integer pnrPaxId = reservationPax.getPnrPaxId();
			Set<ReservationPaxFare> reservationPaxFareSet = reservationPax.getPnrPaxFares();
			Collection<Integer> paxPnrSegIds = new ArrayList<>();
			if (reservationPaxFareSet != null && !reservationPaxFareSet.isEmpty()) {
				for (ReservationPaxFare reservationPaxFare : reservationPaxFareSet) {
					Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();
					boolean isNoShow = false;
					Collection<Integer> pnrSegIds = new ArrayList<>();
					if (paxFareSegments != null && !paxFareSegments.isEmpty()) {
						FARE_SEG: for (ReservationPaxFareSegment paxFareSegment : paxFareSegments) {
							if (ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE.equals(paxFareSegment.getStatus())) {
								isNoShow = true;
								pnrSegIds.add(paxFareSegment.getPnrSegId());
							} else {
								isNoShow = false;
								if (!pnrSegIds.isEmpty()) {
									pnrSegIds.clear();
								}
								break FARE_SEG;
							}

						}
					}

					if (isNoShow) {
						paxPnrSegIds.addAll(pnrSegIds);
					}
				}
			}

			noShowSegmentsByPaxId.put(pnrPaxId, paxPnrSegIds);

		}
		return noShowSegmentsByPaxId;
	}

	private void validateParams(String pnr, Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || pnrPaxIds == null || pnrPaxIds.isEmpty()) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	private void validateNoShowTaxReversalPaxIds(Collection<Integer> pnrPaxIds,
			Map<Integer, Collection<Integer>> noShowSegmentsByPaxId) throws ModuleException {

		if (!noShowSegmentsByPaxId.keySet().containsAll(pnrPaxIds)) {
			throw new ModuleException("airreservations.invalid.paxid");
		}

		if (this.isNoShoTaxReversal) {

			if (!noShowSegmentsByPaxId.keySet().containsAll(pnrPaxIds)) {
				throw new ModuleException("airreservations.no.show.tax.reversal.invalid.paxid");
			}

			if (noShowSegmentsByPaxId.values() == null || noShowSegmentsByPaxId.isEmpty()) {
				throw new ModuleException("airreservations.zero.no.show.segments");
			}
		}

	}

	private Collection<Integer> getValidSegIdsForChargeReversal(Integer pnrPaxId, Collection<Integer> pnrSegIds,
			Map<Integer, Collection<Integer>> noShowSegmentsByPaxId, Integer associatedPaxId) throws ModuleException {
		if (this.isNoShoTaxReversal) {
			if (noShowSegmentsByPaxId.get(pnrPaxId) == null || noShowSegmentsByPaxId.get(pnrPaxId).isEmpty()) {
				if (associatedPaxId == null || (associatedPaxId != null && (noShowSegmentsByPaxId.get(associatedPaxId) == null
						|| noShowSegmentsByPaxId.get(associatedPaxId).isEmpty()))) {
					throw new ModuleException("airreservations.zero.no.show.segments.for.tax.reversal");
				}

			}

			return noShowSegmentsByPaxId.get(pnrPaxId);
		}
		return pnrSegIds;
	}

	private String getPaxName(ReservationPax passenger) {
		return passenger.getTitle() + " " + passenger.getFirstName() + " " + passenger.getLastName();

	}

	private Map<String, BigDecimal> groupWiseAvailableRefundableTotal(Set<ReservationPaxOndCharge> charges) {

		Map<String, BigDecimal> groupWiseRefundableTotals = new HashMap<>();
		if (chargeGroups != null && !chargeGroups.isEmpty()) {
			for (String chargeGroup : chargeGroups) {
				groupWiseRefundableTotals.put(chargeGroup, getRefundableChargesByGroup(charges, chargeGroup));
			}
		}
		return groupWiseRefundableTotals;
	}

	private BigDecimal getRefundableChargesByGroup(Set<ReservationPaxOndCharge> charges, String chargeGroup) {
		BigDecimal groupTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (charges != null && !charges.isEmpty()) {
			for (ReservationPaxOndCharge charge : charges) {

				if (isChargeCodeRefundable(charge) && !StringUtil.isNullOrEmpty(chargeGroup)
						&& chargeGroup.equals(charge.getChargeGroupCode())) {
					groupTotalAmount = AccelAeroCalculator.add(groupTotalAmount, charge.getEffectiveAmount());
				}
			}

		}
		return groupTotalAmount;
	}

	private boolean isChargeCodeRefundable(ReservationPaxOndCharge charge) {
		boolean isRefundableCharge = false;
		if (!ReservationInternalConstants.ChargeGroup.FAR.equals(charge.getChargeGroupCode())) {
			if (refundableChargeByRateId.keySet().contains(charge.getChargeRateId())) {
				isRefundableCharge = refundableChargeByRateId.get(charge.getChargeRateId());
			} else {
				isRefundableCharge = ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO
						.isRefundableCharge(new Long(charge.getPnrPaxOndChgId()));
				refundableChargeByRateId.put(charge.getChargeRateId(), isRefundableCharge);
			}
		}
		return isRefundableCharge;
	}

	private Set<ReservationPaxOndCharge> filterChargesGroupWise(Set<ReservationPaxOndCharge> charges, String chargeGroup) {
		Set<ReservationPaxOndCharge> filteredCharges = new HashSet<>();
		if (charges != null && !charges.isEmpty()) {
			for (ReservationPaxOndCharge charge : charges) {
				if (!StringUtil.isNullOrEmpty(chargeGroup) && chargeGroup.equals(charge.getChargeGroupCode())) {
					filteredCharges.add(charge);
				}
			}

		}
		return filteredCharges;
	}

	private void initValidChargeGroups() {
		if (this.isNoShoTaxReversal) {
			this.chargeGroups = new ArrayList<>();
			this.chargeGroups.add(ReservationInternalConstants.ChargeGroup.TAX);

			this.chargeCodes = new ArrayList<>();
		}

		if (this.chargeGroups == null) {
			// FAR,DIS,SUR,TAX,INF,CNX,MOD,ADJ,PEN
			this.chargeGroups = new ArrayList<>();
			this.chargeGroups.add(ReservationInternalConstants.ChargeGroup.FAR);
			this.chargeGroups.add(ReservationInternalConstants.ChargeGroup.TAX);
			this.chargeGroups.add(ReservationInternalConstants.ChargeGroup.SUR);

			this.chargeCodes = new ArrayList<>();
		}

		if (this.chargeCodes == null) {
			this.chargeCodes = new ArrayList<>();
		}
	}

}
