package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.OSIAddressDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIEmailDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIPhoneNoDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ChangeContactDetailsMessageCreator extends TypeBReservationMessageCreator {
	
	public ChangeContactDetailsMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}

	@Override
	public List<OSIDTO> addOSIDetails(Reservation reservation, TypeBRequestDTO typeBRequestDTO) {
		List<OSIDTO> osiDTOs = new ArrayList<OSIDTO>();
		ReservationContactInfo originalContInfo = typeBRequestDTO.getOriginalContInfo(); 
		if (originalContInfo != null) {
			osiDTOs.addAll(composeOSIDetails(typeBRequestDTO, reservation.getContactInfo()));
		}
		return osiDTOs;
	}

	private List<OSIDTO> composeOSIDetails(TypeBRequestDTO typeBRequestDTO, ReservationContactInfo newContInfo) {
		List<OSIDTO> osiDTOs = new ArrayList<OSIDTO>();
		ReservationContactInfo originalContInfo = typeBRequestDTO.getOriginalContInfo(); 
		if ((originalContInfo.getPhoneNo() != null && !originalContInfo.getPhoneNo().equals(newContInfo.getPhoneNo()))
				|| (originalContInfo.getPhoneNo() == null && newContInfo.getPhoneNo() != null)) {
			OSIPhoneNoDTO phoneDto = new OSIPhoneNoDTO();
			phoneDto.setPhoneNo(newContInfo.getPhoneNo());
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				phoneDto.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				phoneDto.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			phoneDto.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_PHONE_NATURE_UNKNOWN.getCode());
			osiDTOs.add(phoneDto);
		}
		if ((originalContInfo.getMobileNo() != null && !originalContInfo.getMobileNo().equals(newContInfo.getMobileNo()))
				|| (originalContInfo.getMobileNo() == null && newContInfo.getMobileNo() != null)) {
			OSIPhoneNoDTO phoneDto = new OSIPhoneNoDTO();
			phoneDto.setPhoneNo(newContInfo.getMobileNo());
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				phoneDto.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				phoneDto.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			phoneDto.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_PHONE_MOBILE.getCode());
			osiDTOs.add(phoneDto);
		}
		if ((originalContInfo.getEmail() != null && !originalContInfo.getEmail().equals(newContInfo.getEmail()))
				|| (originalContInfo.getEmail() == null && newContInfo.getEmail() != null)) {
			OSIEmailDTO emailDto = new OSIEmailDTO();
			String rawEmail = newContInfo.getEmail();
			String email1 = rawEmail.replace("@", "//");
			String email = email1.replace("_", "..");
			emailDto.setEmail(email);
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				emailDto.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				emailDto.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			emailDto.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_EMAIL.getCode());
			osiDTOs.add(emailDto);
		}
		if ((originalContInfo.getStreetAddress1() != null && !originalContInfo.getStreetAddress1().equals(
				newContInfo.getStreetAddress1()))
				|| (originalContInfo.getStreetAddress1() == null && newContInfo.getStreetAddress1() != null)
				|| (originalContInfo.getStreetAddress2() != null && !originalContInfo.getStreetAddress2().equals(
						newContInfo.getStreetAddress2()))
				|| (originalContInfo.getStreetAddress2() == null && newContInfo.getStreetAddress2() != null)
				|| (originalContInfo.getCity() != null && !originalContInfo.getCity().equals(newContInfo.getCity()))
				|| (originalContInfo.getCity() == null && newContInfo.getCity() != null)) {
			OSIAddressDTO addressDto = new OSIAddressDTO();
			addressDto.setAddressLineOne(newContInfo.getStreetAddress1());
			addressDto.setAddressLineTwo(newContInfo.getStreetAddress2());
			addressDto.setCity(newContInfo.getCity());
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				addressDto.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				addressDto.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			addressDto.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_ADDRESS.getCode());
			osiDTOs.add(addressDto);
		}
		return osiDTOs;
	}
	
	@Override
	public boolean composingOSIDetailsSuccess(List<OSIDTO> osiDTOs) {
		if (osiDTOs.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
}
