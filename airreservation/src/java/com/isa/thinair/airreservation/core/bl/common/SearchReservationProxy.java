package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * @author Nilindra Fernando
 * 
 * @since 3:40 PM 10/28/2009
 */
public class SearchReservationProxy {

	public static Collection<ReservationDTO> getAfterReservations(int customerId, Date date,
			Collection<String> colReservationStates, TrackInfoDTO trackInfoDTO) throws ModuleException {
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setDepartureDate(date);
		reservationSearchDTO.setCustomerId(new Integer(customerId));
		reservationSearchDTO.setOwnerChannelId(trackInfoDTO.getOriginChannelId());

		// If reservation states do exist
		if (colReservationStates != null) {
			Iterator<String> itColReservationStates = colReservationStates.iterator();
			String reservationStatus;
			while (itColReservationStates.hasNext()) {
				reservationStatus = (String) itColReservationStates.next();
				reservationSearchDTO.addReservationStatus(reservationStatus);
			}
		}

		return fetchReservations(reservationSearchDTO);
	}

	public static Collection<ReservationDTO> getEarlyReservations(int customerId, Date date,
			Collection<String> colReservationStates, TrackInfoDTO trackInfoDTO) throws ModuleException {
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setEarlyDepartureDate(date);
		reservationSearchDTO.setCustomerId(new Integer(customerId));
		reservationSearchDTO.setOwnerChannelId(trackInfoDTO.getOriginChannelId());

		// If reservation states do exist
		if (colReservationStates != null) {
			Iterator<String> itColReservationStates = colReservationStates.iterator();
			String reservationStatus;
			while (itColReservationStates.hasNext()) {
				reservationStatus = (String) itColReservationStates.next();
				reservationSearchDTO.addReservationStatus(reservationStatus);
			}
		}

		return fetchReservations(reservationSearchDTO);
	}

	public static Collection<ReservationDTO> getReservations(ReservationSearchDTO reservationSearchDTO) throws ModuleException {
		return fetchReservations(reservationSearchDTO);
	}

	private static Collection<ReservationDTO> fetchReservations(ReservationSearchDTO reservationSearchDTO) throws ModuleException {
		ReservationDAO reservationDao = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		Collection<ReservationDTO> colReservationDTO = reservationDao.getReservations(reservationSearchDTO);
		
		if (colReservationDTO != null && colReservationDTO.size() > 0) {
			for (ReservationDTO reservationDTO : colReservationDTO) {
				ReservationProxy.composeReservationSegmentDisplayStates(reservationDTO.getSegments());
			}
		}
		return colReservationDTO;
	}

	public static Collection<ReservationPaxDTO> getPnrPassengerSumCredit(ReservationSearchDTO reservationSearchDTO)
			throws ModuleException {
		return fetchPassengerCredits(reservationSearchDTO, false);
	}

	public static Collection<ReservationPaxDTO> getReservationsForCreditCardInfo(ReservationPaymentDTO reservationPaymentDTO)
			throws ModuleException {
		Collection<ReservationPaxDTO> colReservationDTO = new ArrayList<ReservationPaxDTO>();

		// If Pnr number exist
		if (reservationPaymentDTO.getPnr() != null) {
			colReservationDTO.addAll(fetchPassengerCredits((ReservationSearchDTO) reservationPaymentDTO, false));
		}
		// If Pnr not exist which is a credit card search
		else {
			ReservationPaymentDAO reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;

			// Get pnrs
			Collection<String> pnrs = reservationPaymentDAO.getPnrsForCreditCardInfo(reservationPaymentDTO.getCreditCardNo(),
					reservationPaymentDTO.getEDate(), reservationPaymentDTO.getCcAuthorCode());
			Iterator<String> itPnrs = pnrs.iterator();
			String pnr;
			ReservationSearchDTO reservationSearchDTO;

			while (itPnrs.hasNext()) {
				pnr = (String) itPnrs.next();

				reservationSearchDTO = (ReservationSearchDTO) reservationPaymentDTO;
				reservationSearchDTO.setPnr(pnr);

				colReservationDTO.addAll(fetchPassengerCredits(reservationSearchDTO, false));
			}
		}

		return colReservationDTO;
	}
	
	public static String getPnrByExtRecordLocator(String extRLoc, String creationDate, boolean loadFares)
			throws ModuleException {
		Reservation reservation = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getReservationByExtRecordLocator(extRLoc, creationDate, loadFares);
		return reservation.getPnr();
	}

	private static Collection<ReservationPaxDTO> fetchPassengerCredits(ReservationSearchDTO reservationSearchDTO,
			boolean withAllPaxCredits) throws ModuleException {
		Collection<ReservationPaxDTO> colReservationPaxDTO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getPassengerCredit(
				reservationSearchDTO, withAllPaxCredits);

		Collection<String> pnrs = new HashSet<String>();
		for (ReservationPaxDTO reservationPaxDTO : colReservationPaxDTO) {
			pnrs.add(reservationPaxDTO.getPnr());
		}

		if (pnrs.size() > 0) {
			
			for (String pnr : pnrs) {

				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadExternalReference(true);
				Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

				Collection<ReservationSegmentDTO> reservationSegmentViews = reservation.getSegmentsView();
				for (ReservationSegmentDTO segementDTO : reservationSegmentViews) {
					if ((segementDTO.getCsOcCarrierCode() != null && !segementDTO.getCsOcCarrierCode().isEmpty())
							&& ReservationApiUtils.isCodeShareCarrier(segementDTO.getCsOcCarrierCode())) {
						throw new ModuleException("airreservations.csoc.credit.refund");
					}
				}

				if (!StringUtil.isNullOrEmpty(reservation.getExternalRecordLocator())
						&& ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())) {
					throw new ModuleException("airreservations.csoc.credit.refund");
				}

			}
			
			Map<String, Collection<ReservationExternalSegmentTO>> mapExternalSegmentsInfo = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
					.getExternalSegmentInformation(pnrs);

			if (mapExternalSegmentsInfo.size() > 0) {
				for (ReservationPaxDTO reservationPaxDTO : colReservationPaxDTO) {
					Collection<ReservationExternalSegmentTO> colReservationExternalSegmentDTO = mapExternalSegmentsInfo
							.get(reservationPaxDTO.getPnr());

					if (colReservationExternalSegmentDTO != null) {
						for (ReservationExternalSegmentTO reservationExternalSegmentTO : colReservationExternalSegmentDTO) {
							reservationPaxDTO.addExternalSegment(reservationExternalSegmentTO);
						}
					}
				}
			}
		}

		return colReservationPaxDTO;
	}
}
