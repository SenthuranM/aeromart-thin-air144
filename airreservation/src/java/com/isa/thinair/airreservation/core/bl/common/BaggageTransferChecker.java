package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * This Baggage Transfer checker verify the source flight segment id's baggage template id and target flight segment
 * id's baggage template id is matching or not. This also encapsulate the knowledge on whether the source flight is
 * operating OND baggages or segment baggages
 * 
 * @author Nilindra Fernando
 * @since July 19, 2012
 */
public class BaggageTransferChecker {

	private Integer sourceFlightSegId;
	private Integer sourcePnrSegId;
	private Reservation reservation;

	public BaggageTransferChecker(Integer sourceFlightSegId, Integer sourcePnrSegId, Reservation reservation) {
		this.setSourceFlightSegId(sourceFlightSegId);
		this.setSourcePnrSegId(sourcePnrSegId);
		this.setReservation(reservation);
	}

	public boolean isSrcAndTargetBaggageTemplateMatches(Integer targetFlightSegmentId) throws ModuleException {

		String ondGroupId = BeanUtils.nullHandler(getApplicableOndGroupId(this.getSourcePnrSegId(), this.getReservation()));

		// OND Baggages
		if (ondGroupId.length() > 0) {
			Integer sourceTemplateId = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.getONDChargeTemplateId(this
					.getSourcePnrSegId());

			if (sourceTemplateId != null) {
				Integer targetTemplateId = getTargetTemplateId(targetFlightSegmentId);

				if (targetTemplateId != null && sourceTemplateId.equals(targetTemplateId)) {
					return true;
				}
			}
			// Segment Baggages
		} else {
			Integer sourceTemplateId = getReservationBaggageTemplate(this.getSourceFlightSegId());

			if (sourceTemplateId != null) {
				Integer targetTemplateId = getReservationBaggageTemplate(targetFlightSegmentId);

				if (targetTemplateId != null && sourceTemplateId.equals(targetTemplateId)) {
					return true;
				}
			}
		}

		return false;
	}

	private Integer getTargetTemplateId(Integer targetFlightSegmentId) throws ModuleException {
		FlightSegement flightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(targetFlightSegmentId);
		List<FlightSegmentTO> lstFlightSegmentTO = new ArrayList<FlightSegmentTO>();
		lstFlightSegmentTO.add(new FlightSegmentTO(flightSegment));

		Map<Integer, List<FlightBaggageDTO>> baggageMap = ReservationModuleUtils.getBaggageBD()
				.getONDBaggages(lstFlightSegmentTO, false, false);

		List<FlightBaggageDTO> lstFlightBaggageDTO = BeanUtils.getFirstElement(baggageMap.values());

		if (lstFlightBaggageDTO != null && lstFlightBaggageDTO.size() > 0) {
			return BeanUtils.getFirstElement(lstFlightBaggageDTO).getTemplateId();
		} else {
			return null;
		}
	}

	private Integer getApplicableOndGroupId(Integer pnrSegId, Reservation reservation) {

		for (ReservationSegment reservationSegment : reservation.getSegments()) {
			if (reservationSegment.getPnrSegId().equals(pnrSegId)) {
				return reservationSegment.getBaggageOndGroupId();
			}
		}

		return null;
	}

	/**
	 * Populate Baggage information
	 * 
	 * @param res
	 * @param pnrSegId
	 * @throws ModuleException
	 */
	private static Integer getReservationBaggageTemplate(int flightSegId) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.getTemplateForSegmentId(flightSegId);
	}

	/**
	 * @return the sourceFlightSegId
	 */
	private Integer getSourceFlightSegId() {
		return sourceFlightSegId;
	}

	/**
	 * @param sourceFlightSegId
	 *            the sourceFlightSegId to set
	 */
	private void setSourceFlightSegId(Integer sourceFlightSegId) {
		this.sourceFlightSegId = sourceFlightSegId;
	}

	/**
	 * @return the reservation
	 */
	private Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation
	 *            the reservation to set
	 */
	private void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @return the sourcePnrSegId
	 */
	private Integer getSourcePnrSegId() {
		return sourcePnrSegId;
	}

	/**
	 * @param sourcePnrSegId
	 *            the sourcePnrSegId to set
	 */
	private void setSourcePnrSegId(Integer sourcePnrSegId) {
		this.sourcePnrSegId = sourcePnrSegId;
	}

}
