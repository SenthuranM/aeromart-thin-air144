package com.isa.thinair.airreservation.core.persistence.hibernate.external;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.external.AgentTopUpX3DTO;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.airreservation.core.persistence.dao.AgentTopUpTransferDAOImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * X3CreditSalesDAOImpl is the business DAO hibernate implementation
 * 
 * 
 * @isa.module.dao-impl dao-name="x3AgentTopUpDAO"
 */

public class X3AgentTopUpDAOImpl extends AgentTopUpTransferDAOImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(X3AgentTopUpDAOImpl.class);


	@Override
	public void insertTopUpToExternal(Collection<AgentTopUpX3DTO> col, AccontingSystemTemplate template)
			throws ClassNotFoundException, SQLException, Exception {
		Connection connection = null;

		try {
			connection = template.getExternalConnection();
			connection.setAutoCommit(false);
			Iterator<AgentTopUpX3DTO> it = col.iterator();
			// common sql query
			AgentTopUpX3DTO dto = null;
			String sql = "insert into  INT_T_AGENT_TOP_UP values (?,?,?,?,?,?,?)";// 11

			PreparedStatement stmt = connection.prepareStatement(sql);

			while (it.hasNext()) {
				dto = it.next();
				stmt.setInt(1, dto.getTnxId());
				stmt.setString(2, dto.getAgentCode());
				stmt.setBigDecimal(3, dto.getAmount());
				stmt.setDate(4,new java.sql.Date( dto.getTnxDate().getTime())); 
				stmt.setString(5, dto.getNcDestription());
				stmt.setBigDecimal(6, dto.getAmountLocal());
				stmt.setString(7, dto.getCorrencyCode());


				if (log.isDebugEnabled()) {
					log.debug(dto.getTnxId() + " | " + dto.getTnxDate() + " | " + dto.getAgentCode() + " | " + dto.getAmount()
							+ " | " + dto.getAmountLocal() + " | " + dto.getCorrencyCode() + " | " + dto.getNcDestription());
				}
				stmt.executeUpdate();
			}

		} catch (Exception exception) {
			log.error("###### Error Inserting agent top up to XDB", exception);
			connection.rollback();
			if (exception instanceof SQLException || exception instanceof ClassNotFoundException) {
				throw exception;
			} else {
				throw new CommonsDataAccessException(exception, "transfer.agenttopup.failed");
			}
		}

		try {
			log.debug("########## Committing agent top up to XDB");
			connection.commit();

		} catch (Exception e) {
			log.error("###### Error Committing agent top up to XDB", e);
			throw new CommonsDataAccessException(e, "transfer.agenttopup.failed");
		} finally {
			// connection.setAutoCommit(prevAutoCommitStatus);
			connection.close();
		}
		log.info("############ Successfuly Inserted Agent Topup To XDB");

	}

	@Override
	public BigDecimal getGeneratedCreditCardSalesTotal(java.util.Date fromDate, java.util.Date toDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getTransferedCreditCardSalesTotal(java.util.Date fromDate, java.util.Date toDate,
			AccontingSystemTemplate template) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] getCreditCardSalesHistory(java.util.Date fromDate, java.util.Date toDate) {
		// TODO Auto-generated method stub
		return null;
	}
		
}
