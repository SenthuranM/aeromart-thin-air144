package com.isa.thinair.airreservation.core.bl.external.config;

import java.util.Map;

public class ExternalSystemsConfig {
	
	private Map<String, Object> accountingSystemMap;

	public Map<String, Object> getAccountingSystemMap() {
		return accountingSystemMap;
	}

	public void setAccountingSystemMap(Map<String, Object> accountingSystemMap) {
		this.accountingSystemMap = accountingSystemMap;
	}	
	
}
