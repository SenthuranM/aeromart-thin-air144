/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;

/**
 * @author udithad
 *
 */
public class RuleResponse {

	private boolean proceedNextElement;
	private boolean isSpaceAvailable;
	private String[] suggestedElementText;
	private boolean isPnrValied;
	public boolean isProceedNextElement() {
		return proceedNextElement;
	}

	public void setProceedNextElement(boolean proceedNextElement) {
		this.proceedNextElement = proceedNextElement;
	}

	public String[] getSuggestedElementText() {
		return suggestedElementText;
	}

	public void setSuggestedElementText(String[] suggestedElementText) {
		this.suggestedElementText = suggestedElementText;
	}

	public boolean isSpaceAvailable() {
		return isSpaceAvailable;
	}

	public void setSpaceAvailable(boolean isSpaceAvailable) {
		this.isSpaceAvailable = isSpaceAvailable;
	}

	public boolean isPnrValied() {
		return isPnrValied;
	}

	public void setPnrValied(boolean isPnrValied) {
		this.isPnrValied = isPnrValied;
	}


	
}
