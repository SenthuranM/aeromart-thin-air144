package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author manoj
 *
 */
public class ExternalAirportTransferChargesBL {

	/**
	 * Reflect external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 *            TODO
	 * @param reservationDiscountDTO
	 * @param externalChgDTO
	 * @throws ModuleException
	 */
	public static void reflectExternalChgsForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		// Apply these changes for the reservation
		applyExternalChargesForAFreshBooking(reservation, credentialsDTO, paxDiscInfo, reservationDiscountDTO);
	}

	/**
	 * Applies the external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 *            TODO
	 * @param reservationDiscountDTO
	 * @param colExternalChgDTO
	 * @throws ModuleException
	 */
	private static void applyExternalChargesForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> ccExternalChgs = null;
		boolean updateExist = false;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();
			ccExternalChgs = new ArrayList<ExternalChgDTO>();

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				paymentAssembler = (PaymentAssembler) reservationPax.getPayment();

				Collection<ExternalChgDTO> ccAPTExternalChgs = paymentAssembler
						.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_TRANSFER);

				if (ccAPTExternalChgs != null && ccAPTExternalChgs.size() > 0) {
					ccExternalChgs.addAll(ccAPTExternalChgs);
				}

				if (ccExternalChgs.size() > 0) {

					Collection<DiscountChargeTO> discountChargeTOs = null;
					if (reservationDiscountDTO != null) {
						discountChargeTOs = reservationDiscountDTO.getPaxDiscountChargeTOs(reservationPax.getPaxSequence());
					}

					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
						AirportTransferExternalChgDTO aptExternalChgDTO = (AirportTransferExternalChgDTO) externalChgDTO;
						reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(reservationPax.getPnrPaxFares(),
								FlightRefNumberUtil.getSegmentIdFromFlightRPH(aptExternalChgDTO.getFlightRPH()), true);

						ReservationApiUtils.updatePaxOndExternalChargeDiscountInfo(paxDiscInfo, externalChgDTO, discountChargeTOs,
								reservationPax.getPaxSequence());

						ReservationCoreUtils.captureReservationPaxOndCharge(aptExternalChgDTO.getAmount(), null,
								aptExternalChgDTO.getChgRateId(), aptExternalChgDTO.getChgGrpCode(), reservationPaxFare,
								credentialsDTO, false, paxDiscInfo, null, null, 0);
						updateExist = true;
					}
				}
			}
		}
		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}
	}

	public static void reflectExternalChgForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Map<Integer, RevenueDTO> passengerRevenueMap,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen) throws ModuleException {
		// Apply these changes for the reservation
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForANewSegment(reservation,
				pnrPaxIdAndPayments, credentialsDTO, chgTxnGen);

		// // Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, pnrPaxIdAndPayments);

		// // Apply these changes to the passenger revenue map
		addExternalChgsForRevenueMap(mapPnrPaxIdAdjustments, passengerRevenueMap);
	}

	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen)
			throws ModuleException {
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;
		// BigDecimal perPaxTotalAmount;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());
				ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_TRANSFER);

				if (ccExternalChgs.size() > 0) {
					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
						AirportTransferExternalChgDTO airportTransferExternalChgDTO = (AirportTransferExternalChgDTO) externalChgDTO;
						reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(reservationPax.getPnrPaxFares(),
								airportTransferExternalChgDTO.getFlightSegId(), true);
						ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(
								airportTransferExternalChgDTO.getAmount(), null, airportTransferExternalChgDTO.getChgRateId(),
								airportTransferExternalChgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, null,
								null, null, chgTxnGen.getTnxSequence(reservationPax.getPnrPaxId()));

						lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(reservationPax.getPnrPaxId());

						if (lstReservationPaxOndCharge == null) {
							lstReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();
							lstReservationPaxOndCharge.add(reservationPaxOndCharge);

							mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), lstReservationPaxOndCharge);
						} else {
							lstReservationPaxOndCharge.add(reservationPaxOndCharge);
						}

						updateExist = true;
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	private static void addExternalChgsForPaymentAssembler(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, PaymentAssembler> passengerPayment) {
		Iterator<Integer> itPnrPaxIds = passengerPayment.keySet().iterator();
		PaymentAssembler paymentAssembler;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				paymentAssembler = passengerPayment.get(pnrPaxId);
				lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);

				for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
					paymentAssembler.setTotalChargeAmount(AccelAeroCalculator.add(paymentAssembler.getTotalChargeAmount(),
							reservationPaxOndCharge.getAmount()));
				}
			}
		}
	}

	private static void addExternalChgsForRevenueMap(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, RevenueDTO> passengerRevenueMap) {
		Iterator<Integer> itPnrPaxIds = passengerRevenueMap.keySet().iterator();
		RevenueDTO revenueDTO;
		Integer pnrPaxId;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				revenueDTO = passengerRevenueMap.get(pnrPaxId);
				List<ReservationPaxOndCharge> lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);

				for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
					revenueDTO.setAddedTotal(
							AccelAeroCalculator.add(revenueDTO.getAddedTotal(), reservationPaxOndCharge.getAmount()));
					revenueDTO.getAddedCharges().add(reservationPaxOndCharge);
				}
			}
		}
	}
}
