/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext;

/**
 * @author udithad
 *
 */
public class EndElementRuleContext extends RulesDataContext {

	private Integer passengerCount;

	public Integer getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}

}
