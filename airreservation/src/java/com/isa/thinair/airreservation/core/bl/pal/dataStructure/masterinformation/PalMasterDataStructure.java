package com.isa.thinair.airreservation.core.bl.pal.dataStructure.masterinformation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.dataContext.PalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerCollectionStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PnlADlBaseDataStructure;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PalMasterDataStructure<K extends BasePassengerCollection, T extends BaseDataContext>
extends PnlADlBaseDataStructure implements
MessageDataStructure<PassengerCollection, PalDataContext> {
	
	private Collection<DestinationFare> destinationFares;
	private PassengerCollection passengerCollection = null;
	private PalDataContext context;
	private PassengerCollectionStructure passengerCollectionStructure;
	private List<DestinationFare> dummyDestinationFares = null;

	public PalMasterDataStructure() {
		fareClassPaxCountMap = new HashMap<String, Integer>();
	}
	
	@Override
	public PassengerCollection getDataStructure(PalDataContext context) throws ModuleException {
		this.context = context;
		populateFlightIdentityInformation(context);
		passengerCollectionStructure = new PassengerCollectionStructure();
		populateFareCabinRbdMap(context.getFlightId());
		destinationFares = getPALRelatedDestinationInformation(
				context.getFlightId(), context.getDepartureAirportCode(),
				context.getFlightNumber());
		passengerCollection = passengerCollectionStructure
				.getPassengerCollection(destinationFares, cabinFareRbdMap);
		populateFareClassPassengerCountAndGroupCode(
				context.getDepartureAirportCode(), context.getFlightId());
		populatePnrWisePassengerCountsToPaxCollection();
		populateLastGroupCode();
		populateDataForEmptyPAL(context.getFlightId(), context.getDepartureAirportCode());
		populateDummyDestinationFares();
		sortDestinationFareCollection(passengerCollection.getPnlAdlDestinationMap());
		return passengerCollection;
	}

	private void populateDummyDestinationFares() {
		for (String destinationAirportCode : destinationAirportCodes) {
			dummyDestinationFares = createDummyEntriesForNonUsedFareClasses(destinationAirportCode);
			if (dummyDestinationFares != null
					&& !dummyDestinationFares.isEmpty()) {
				if (passengerCollection.getPnlAdlDestinationMap().containsKey(
						destinationAirportCode)) {
					passengerCollection.getPnlAdlDestinationMap()
							.get(destinationAirportCode)
							.addAll(dummyDestinationFares);
				}
			}
		}
	}
	
	private void sortDestinationFareCollection(Map<String,List<DestinationFare>> destinationFares){
		for (Map.Entry<String, List<DestinationFare>> entry : destinationFares
				.entrySet()) {
			passengerCollectionStructure.sortDestinationFares(entry.getValue());
		}
	}
	
	private Collection<DestinationFare> getPALRelatedDestinationInformation(
			Integer flightId, String departureAirportCode, String flightNumber) throws ModuleException {

		Collection<DestinationFare> destinationFares = null;
		Timestamp currentTimeStamp = new Timestamp(new Date().getTime());
		List<PassengerInformation> passengerInformation = auxilliaryDAO
				.getPALPassengerDetails(flightId, departureAirportCode,
						currentTimeStamp, context.getCarrierCode());
		if(passengerInformation != null/* && !passengerInformation.isEmpty()*/){
			destinationFares = getDestinationFareCollectionBy(passengerInformation);
				
		}else{
			destinationFares = new ArrayList<DestinationFare>();
			//throw new ModuleException("reservationauxilliary.pal.noreservations", "airreservations");
		}
		return destinationFares;

	}

	protected void populateFareClassPassengerCountAndGroupCode(
			String departureAirportCode, Integer flightId) {
		getnerateFareClassCountMap();
		lastGroupCode = getFirstTourIdCode();
	}

	@SuppressWarnings("unchecked")
	private void populatePnrWisePassengerCountsToPaxCollection() {
		passengerCollection.setGroupCodes(PnlAdlUtil.populateTourIds());
		passengerCollection.setPnrWisePassengerCount(pnrWisePassengerCount);
		passengerCollection
				.setPnrWisePassengerReductionCount(pnrWisePassengerReductionCount);
		passengerCollection.setPnrPaxIdvsSegmentIds(pnrPaxIdvsSegmentIds);
		passengerCollection.setPnrCollection(pnrCollection);
		passengerCollection.setFareClassWisePaxCount(fareClassWisePaxCount);
	}
	
	private void populateLastGroupCode(){
		passengerCollection.setLastGroupCode(lastGroupCode);
	}

	private void getnerateFareClassCountMap() {
		if (destinationFares != null) {
			for (DestinationFare destinationFare : destinationFares) {
				if (destinationFare.getAddPassengers() != null) {
					fareClassPaxCountMap.put(destinationFare.getFareClass(),
							destinationFare.getAddPassengers().size());
				}
			}
		}
	}

	private String getFirstTourIdCode() {
		int index = 0;
		String[] tourIds = PnlAdlUtil.populateTourIds();
		return getTrourIdByIndex(tourIds, index);
	}

	private String getTrourIdByIndex(String[] tourIds, int index) {
		String tourId = null;
		if (tourIds != null) {
			tourId = tourIds[index];
		}
		return tourId;
	}
	
	protected void populateDataForEmptyPAL(Integer flightId, String depAirportCode) throws ModuleException {
		if (destinationAirportCodes.isEmpty() && passengerCollection.getPnlAdlDestinationMap().isEmpty()) {

			Flight flight = ReservationModuleUtils.getFlightBD().getFlight(flightId);
			for (FlightLeg leg : flight.getFlightLegs()) {
				if (depAirportCode.equals(leg.getOrigin())) {
					destinationAirportCodes.add(leg.getDestination());
					
					if(!passengerCollection.getPnlAdlDestinationMap().containsKey(depAirportCode)){
						passengerCollection.getPnlAdlDestinationMap().put(leg.getDestination(), new ArrayList<>());
					}
				}
			}
		}
	}

}
