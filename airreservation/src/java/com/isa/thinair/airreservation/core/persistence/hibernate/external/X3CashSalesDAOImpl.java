/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate.external;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.CashPaymentDTO;
import com.isa.thinair.airreservation.api.model.CashSales;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.X3CashSalesDAO;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.ExternalAccountingSystem;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;

/**
 * X3CashSalesDAOImpl is the business DAO hibernate implementation
 * 
 * 
 * @isa.module.dao-impl dao-name="X3CashSalesDAO"
 */

public class X3CashSalesDAOImpl extends PlatformBaseHibernateDaoSupport implements X3CashSalesDAO {

	private static Log log = LogFactory.getLog(X3CashSalesDAOImpl.class);

	@Override
	public void transferCashSales(Date date, ExternalAccountingSystem accountingSystem) throws ModuleException {
		log.info("########### Starting Generation/Transferrniing Cash Sales For X3 DAO :" + date);
		// Dto Set for storing values retrived from pax tnx
		Collection<CashPaymentDTO> colCashPaymentDTOs = null;
		// POJO Set
		Collection<CashSales> cashSalesPOJOs = null;
		try {
			// clear and retrive
			clearCashSalesTable(date);
			colCashPaymentDTOs = getCashPayments(date);
		} catch (CommonsDataAccessException dae) {
			log.error("######### Error When Retriveing or Clearning Cash Sales For :" + date, dae);
			throw new ModuleException(dae, dae.getExceptionCode(), AirreservationConstants.MODULE_NAME);
		}

		if (colCashPaymentDTOs != null) {
			try {
				// saving to t_cash_sales
				log.debug("############ Inserting Cash For Date:" + date);
				cashSalesPOJOs = insertCashSalesToInternal(colCashPaymentDTOs);
			} catch (CommonsDataAccessException dae) {
				log.error("############### Error in Saving Cash POJO", dae);
				throw new ModuleException(dae, dae.getExceptionCode(), AirreservationConstants.MODULE_NAME);
			}

			// if configured to transfer to and X DB
			// if (ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer().equalsIgnoreCase("true")) {
			String transferToExternal = ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer();
			if (transferToExternal == null
					|| !(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM.equalsIgnoreCase(transferToExternal))) {
				throw new ModuleException("airreservation.exdb.notconfigured");
			}

			try {
				// to external sql server. -t_int_cash_sales.
				log.debug("##### Inserting Cash Sales To XDB For Date:" + date);
				insertCashSalesToExternal(colCashPaymentDTOs, (XDBConnectionUtil) accountingSystem);
				log.debug("##### After Inserting Cash Sales To XDB For Date:" + date);
			} /*
			 * catch (SQLException me) { log.error("#### Error Insertin Cash to XDB for date:" + date, me);
			 * 
			 * if (me.getErrorCode() == 2627) throw new ModuleException(me,
			 * "reservationauxilliary.externaltable.integrityviolation", AirreservationConstants.MODULE_NAME); else
			 * throw new ModuleException(me, "reservationauxilliary.externaltable.databaseunavailable",
			 * AirreservationConstants.MODULE_NAME); } catch (ClassNotFoundException cne) {
			 * log.error("############### Error in Transferring Cash Sales to XDB", cne); }
			 */catch (CommonsDataAccessException e) {
				log.error(
						" ############## Commons Exception Transferring Cash Sales to XDB" + e + "code :" + e.getExceptionCode(),
						e);
				throw new ModuleException(e, e.getExceptionCode(), AirreservationConstants.MODULE_NAME);
			} catch (Exception e) {
				log.error("######### !!!!!! Transfering to Interface Tables Failed ### !!!!!", e);
				throw new ModuleException(e, "transfer.cashsales.failed", AirreservationConstants.MODULE_NAME);

			}
			try {
				log.debug("############ Going to Update Cash Sales Status after Transferring");
				updateInternalCashTable(cashSalesPOJOs);
				log.debug("############ After Updating Cash Sales Status after Transferring");
			} catch (CommonsDataAccessException dae) {
				log.error("################# Error in updating the Internal Cash Sales .....");
				throw new ModuleException(dae, dae.getExceptionCode(), AirreservationConstants.MODULE_NAME);
			}

			// }
		}

	}

	/*
	 * Inserting Cash Sales to internal summary table
	 */
	@Override
	public Collection<CashSales> insertCashSalesToInternal(Collection<CashPaymentDTO> col) {

		log.info("######## Inserting Cash Sales To Internal");
		Date nowTime = new Date();
		Collection<CashSales> cashSalesPOJOs = new ArrayList<CashSales>();
		Iterator<CashPaymentDTO> it = col.iterator();

		if (col != null) {
			while (it.hasNext()) {
				CashPaymentDTO dto = it.next();
				CashSales cashSales = new CashSales();
				cashSales.setDateOfSales(dto.getDateOfSale());
				cashSales.setStaff_Id(dto.getStaffId());
				cashSales.setTotalDailySales(dto.getTotalSales());
				cashSales.setTransferStatus('N');
				cashSales.setTransferTimeStamp(nowTime);
				cashSales.setPaymentCurrencyCode(dto.getPaymentCurrencyCode());
				cashSalesPOJOs.add(cashSales);

			}
		}
		saveOrUpdateAllCashSales(cashSalesPOJOs);
		log.info("###### Finished Inserting Cash Sales To Internal");
		return cashSalesPOJOs;
	}

	/*
	 * Updates transferStatus in internal Summary table from 'N' to 'Y'
	 * 
	 * @Param Collection<CashSales>
	 */
	@Override
	public void updateInternalCashTable(Collection<CashSales> pojos) {

		Iterator<CashSales> iterPojos = pojos.iterator();
		while (iterPojos.hasNext()) {
			CashSales cashSales = iterPojos.next();
			cashSales.setTransferStatus('Y');
		}
		saveOrUpdateAllCashSales(pojos);

	}

	/*
	 * Save All CashSales POJO's
	 * 
	 * @param cashSales
	 */
	private void saveOrUpdateAllCashSales(Collection<CashSales> cashSales) {
		hibernateSaveOrUpdateAll(cashSales);
	}

	/*
	 * Get Cash Payments for the DATE
	 */
	@Override
	public Collection<CashPaymentDTO> getCashPayments(Date date) {

		// Holds the CashPaymentDTO Object Collection
		Collection<CashPaymentDTO> paymentDTOCol = new ArrayList<CashPaymentDTO>();
		// For Holding the Nominnal Codes.
		Collection<Integer> nominalCodes = new ArrayList<Integer>();
		// To Hold the User - > Total Amount
		HashMap<String, BigDecimal> userAmountMap = new HashMap<String, BigDecimal>();

		BigDecimal total = null;
		String userId = null;
		String paymentCurrencyCode = null;

		// Get All Agent Users with 'CA' ,
		// Note: For reporting purposes we have to insert a zero record for
		// agent users who have
		// no transaction in the pax transaction.
		Collection<String> agentUsers = getUsersForTravelAgentOutlets();

		// populate the nominal codes
		nominalCodes.add(new Integer(ReservationTnxNominalCode.CASH_PAYMENT.getCode()));
		nominalCodes.add(new Integer(ReservationTnxNominalCode.REFUND_CASH.getCode()));
		String thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();

		if ((nominalCodes.size() != 0)) {
			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate template = new JdbcTemplate(ds);
			Object params[] = { date, date };

			String sql = "SELECT x.user_id as user_id , sum(x.total) as total , x.paymentCurrencyCode from ( "
					+ "SELECT user_id, amount AS total, payment_currency_code AS paymentCurrencyCode "
					+ "FROM t_pax_transaction WHERE  nominal_code IN (" + Util.buildIntegerInClauseContent(nominalCodes)
					+ ") AND (PAYMENT_CARRIER_CODE = '" + thisAirlineCode
					+ "' OR PAYMENT_CARRIER_CODE IS NULL) AND  TRUNC(tnx_date) = ? " + "UNION ALL "
					+ "SELECT user_id, amount AS total, paycur_code AS paymentCurrencyCode "
					+ "FROM t_pax_ext_carrier_transactions WHERE  nominal_code IN ("
					+ Util.buildIntegerInClauseContent(nominalCodes)
					+ ") AND  TRUNC(txn_timestamp) = ? ) x GROUP BY x.paymentCurrencyCode, x.user_id";

			userAmountMap = (HashMap<String, BigDecimal>) template.query(sql, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					HashMap<String, BigDecimal> userAmountMap = new HashMap<String, BigDecimal>();

					if (rs != null) {
						String userId = null;
						BigDecimal total = null;
						String paymentCurrency = null;
						while (rs.next()) {
							userId = rs.getString("user_id");
							total = rs.getBigDecimal("total");
							paymentCurrency = rs.getString("paymentCurrencyCode");
							if ((userId != null) && (total != null) && (paymentCurrency != null)) {
								userAmountMap.put(userId + "_" + paymentCurrency, total);
							}
						}
					}
					return userAmountMap;
				}
			});
		}

		// popolate the DTOsa and put it to the collection
		if (userAmountMap != null && userAmountMap.size() > 0) {
			Iterator<String> iterator = userAmountMap.keySet().iterator();
			String key = null;
			String[] pnrNominal = null;
			while (iterator.hasNext()) {
				key = (String) iterator.next();
				userId = key;
				if (userId != null && !userId.isEmpty()) {
					pnrNominal = userId.split("_");
					userId = pnrNominal[0];
					if (pnrNominal.length == 2) {
						paymentCurrencyCode = pnrNominal[1];
					} else {
						paymentCurrencyCode = null;
					}
				}
				total = (BigDecimal) userAmountMap.get(key);

				CashPaymentDTO cashPaymentDTO = new CashPaymentDTO();
				cashPaymentDTO.setStaffId(userId);
				cashPaymentDTO.setDateOfSale(date);
				cashPaymentDTO.setTotalSales(total.negate());
				cashPaymentDTO.setPaymentCurrencyCode(paymentCurrencyCode);
				paymentDTOCol.add(cashPaymentDTO);
				if (agentUsers.contains(userId)) {
					agentUsers.remove(userId);
				}
			}

		}
		Iterator<String> it = agentUsers.iterator();
		while (it.hasNext()) {
			userId = it.next();
			if (userId != null) {
				CashPaymentDTO cashPaymentDTO = new CashPaymentDTO();
				cashPaymentDTO.setStaffId(userId);
				cashPaymentDTO.setDateOfSale(date);
				cashPaymentDTO.setTotalSales(AccelAeroCalculator.getDefaultBigDecimalZero());
				cashPaymentDTO.setPaymentCurrencyCode(null);
				paymentDTOCol.add(cashPaymentDTO);
			}
		}
		return paymentDTOCol;
	}

	/*
	 * Get All Agent Users with Payment code 'CA'
	 */
	public Collection<String> getUsersForTravelAgentOutlets() {

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		String sql = "select * from(select user_id from t_agent ta,t_user tu,T_AGENT_PAYMENT_METHOD pmeth  where  pmeth.PAYMENT_CODE='CA' and tu.agent_code=ta.agent_code and ta.agent_code=pmeth.AGENT_CODE)where rownum <= 10";
		final Collection<String> users = new ArrayList<String>();
		templete.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs != null) {

					while (rs.next()) {

						String userId = rs.getString("user_id");
						users.add(userId);

					}
				}
				return null;
			}
		});

		return users;
	}

	/*
	 * Clear sales in internal summary tables for a date
	 * 
	 * @Param date
	 */
	@Override
	public void clearCashSalesTable(Date date) {

		String hql = "select cashSales from CashSales cashSales where cashSales.dateOfSales=?";

		Collection<?> pojos = getSession().createQuery(hql).setDate(0, date).list();

		if (pojos != null && pojos.size() > 0) {
			deleteAll(pojos);
		}

	}

	/*
	 * Inserting Cash Sales data to external system
	 * 
	 * @Param Collection<CashPaymentDTO>
	 * 
	 * @Param XDBConnectionUtil
	 * 
	 * @throws ClassNotFoundException, SQLException, Exception
	 */
	@SuppressWarnings("rawtypes")
	public Collection<?> insertCashSalesToExternal(Collection<CashPaymentDTO> colCashPaymentDTO, XDBConnectionUtil template)
			throws SQLException, ClassNotFoundException, Exception {

		log.info("############## Going to Insert Cash Sales To External DB");
		Collection<?> output = new ArrayList();

		Connection connection = null;

		ResultSet res = null;

		Iterator<CashPaymentDTO> it = colCashPaymentDTO.iterator();

		CashPaymentDTO dto = null;

		try {

			connection = template.getExternalConnection();

			connection.setAutoCommit(false);

			String sql = "insert into  INT_T_CASH_SALES  values (?,?,?,?)";
			PreparedStatement select = connection
					.prepareStatement("select * from INT_T_CASH_SALES where DATE_OF_SALE=? and STAFF_ID=? and (SALES_CURRENCY_CODE=? OR SALES_CURRENCY_CODE IS NULL)");

			String sqlq = "delete from  INT_T_CASH_SALES where DATE_OF_SALE=? and STAFF_ID=? and (SALES_CURRENCY_CODE=? OR SALES_CURRENCY_CODE IS NULL)";
			PreparedStatement stmtq = connection.prepareStatement(sqlq);

			while (it.hasNext()) {

				dto = it.next();

				select.setDate(1, new java.sql.Date(dto.getDateOfSale().getTime()));
				select.setString(2, String.valueOf(dto.getStaffId()));
				select.setString(3, String.valueOf(dto.getPaymentCurrencyCode()));
				res = select.executeQuery();
				while (res.next()) {

					stmtq.setDate(1, res.getDate("DATE_OF_SALE"));
					stmtq.setString(2, res.getString("STAFF_ID"));
					stmtq.setString(3, res.getString("SALES_CURRENCY_CODE"));
					stmtq.executeUpdate();
				}
				PreparedStatement stmt = connection.prepareStatement(sql);
				stmt.setDate(1, new java.sql.Date(dto.getDateOfSale().getTime()));
				stmt.setString(2, dto.getStaffId());
				stmt.setBigDecimal(3, dto.getTotalSales());
				stmt.setString(4, dto.getPaymentCurrencyCode());
				stmt.executeUpdate();

			}
		}

		catch (Exception exception) {
			log.error("########### Error When Inserting Cash Sales to XDB:", exception);
			connection.rollback();
			if (exception instanceof SQLException || exception instanceof ClassNotFoundException) {
				throw exception;
			} else {
				throw new CommonsDataAccessException(exception, "transfer.cashsales.failed");
			}
		}

		try {
			log.debug("############ Committing Cash Sales Data to XDB");
			connection.commit();

		} catch (Exception e) {
			log.error("########### Error When Committing Cash Sales to XDB:", e);
			throw new CommonsDataAccessException(e, "transfer.creditcardsales.failed");
		} finally {
			// connection.setAutoCommit(prevAutoCommitStatus);
			res.close();
			connection.close();
		}
		log.info("############## Finished Inserting Cash Sales To External DB");
		return output;
	}

}
