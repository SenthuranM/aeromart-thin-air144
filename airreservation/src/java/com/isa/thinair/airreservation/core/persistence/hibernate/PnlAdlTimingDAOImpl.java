/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PnlAdlTimingDAO;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * XAPnlDAO is the business DAO hibernate implementation
 * 
 * @author Byorn de silva
 * @since 1.0
 * @isa.module.dao-impl dao-name="PnlAdlTimingDAO"
 */
public class PnlAdlTimingDAOImpl extends PlatformBaseHibernateDaoSupport implements PnlAdlTimingDAO {

	/**
	 * @see PnlAdlTimingDAO.getFlightsForPNLScheduling
	 * @param pnldepartureGap
	 * @return ArrayList of PNLTransMissionDetailsDTO
	 */
	public ArrayList<PNLTransMissionDetailsDTO> getFlightsForPNLScheduling(int pnldepartureGap, Date date) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		final long internationalCutoffTime = AppSysParamsUtil.getAgentReservationCutoverInMillis();
		final long domesticCutoffTime = AppSysParamsUtil.getReservationCutoverForDomesticInMillis();

		Timestamp dateLowerLimit = new Timestamp(date.getTime());
		Timestamp dateUpperLimit = new Timestamp(date.getTime() + ((24 + (pnldepartureGap / 60)) * 3600 * 1000));
		Object params[] = { dateUpperLimit, dateLowerLimit, "Y" };
		final ArrayList<PNLTransMissionDetailsDTO> flightList = new ArrayList<PNLTransMissionDetailsDTO>();

		String sql = " select distinct flight.flight_type, flight.FLIGHT_NUMBER,flightseg.FLIGHT_ID,flightSeg.EST_TIME_DEPARTURE_LOCAL,flightseg.EST_TIME_DEPARTURE_ZULU,substr(flightseg.segment_code,0,3) as ori,flightseg.FLT_SEG_ID from T_FLIGHT  flight,T_FLIGHT_SEGMENT"
				+ " flightseg  where flightseg.FLIGHT_ID=flight.FLIGHT_ID and flightseg.EST_TIME_DEPARTURE_ZULU<=? AND flightseg.EST_TIME_DEPARTURE_ZULU>? AND (flight.STATUS='ACT' OR flight.STATUS='CLS') and flightseg.SEGMENT_VALID_FLAG=? "
				+ " and flight.CS_OC_FLIGHT_NUMBER is null and flight.CS_OC_CARRIER_CODE is null "
				+ " order by flightseg.flight_id,flightseg.EST_TIME_DEPARTURE_ZULU ";

		templete.query(sql, params,

		new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {

					PNLTransMissionDetailsDTO dto = new PNLTransMissionDetailsDTO();

					int flightID = rs.getInt("FLIGHT_ID");

					Timestamp timeStampzulu = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");

					Timestamp timeStampLocal = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");

					String departureStation = rs.getString("ori");

					int flightSegmentId = rs.getInt("FLT_SEG_ID");

					String flightNumber = rs.getString("FLIGHT_NUMBER");

					dto.setDepartureStation(departureStation);
					dto.setDepartureTimeZulu(timeStampzulu);
					dto.setFlightId(flightID);
					dto.setFlightSegId(flightSegmentId);
					dto.setDeparturetimeLocal(timeStampLocal);
					dto.setFlightNumber(flightNumber);
					String flight_type = rs.getString("FLIGHT_TYPE");
					if (FlightUtil.FLIGHT_TYPE_INT.equals(flight_type)) {
						dto.setFlightCutoffTime(internationalCutoffTime);
					} else {
						dto.setFlightCutoffTime(domesticCutoffTime);
					}
					flightList.add(dto);

				}

				return flightList;
			}

		}

		);

		return flightList;
	}

	public ArrayList<PNLTransMissionDetailsDTO> getFlightsForPNLSchedulingForAirport(Date date, int defaultPnlGap, int overidingPnldepartureGap,
			String airportCode) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		final long internationalCutoffTime = AppSysParamsUtil.getAgentReservationCutoverInMillis();
		final long domesticCutoffTime = AppSysParamsUtil.getReservationCutoverForDomesticInMillis();

		Timestamp dateLowerLimit = new Timestamp(date.getTime() + ((24 + (defaultPnlGap / 60)) * 3600 * 1000));
		Timestamp dateUpperLimit = new Timestamp(date.getTime() + ((24 + (overidingPnldepartureGap / 60)) * 3600 * 1000));
		Object params[] = { dateUpperLimit, dateLowerLimit, "Y", airportCode };
		final ArrayList<PNLTransMissionDetailsDTO> flightList = new ArrayList<PNLTransMissionDetailsDTO>();

		String sql = "select distinct flight.flight_type, flight.FLIGHT_NUMBER,flightseg.FLIGHT_ID,flightSeg.EST_TIME_DEPARTURE_LOCAL,flightseg.EST_TIME_DEPARTURE_ZULU,substr(flightseg.segment_code,0,3) as ori,flightseg.FLT_SEG_ID from T_FLIGHT  flight,T_FLIGHT_SEGMENT"
				+ " flightseg  where flightseg.FLIGHT_ID=flight.FLIGHT_ID and flightseg.EST_TIME_DEPARTURE_ZULU<=? AND flightseg.EST_TIME_DEPARTURE_ZULU>? AND (flight.STATUS='ACT' OR flight.STATUS='CLS') and flightseg.SEGMENT_VALID_FLAG=? "
				+ " and substr(flightseg.segment_code,0,3) = ? "
				+ " order by flightseg.flight_id,flightseg.EST_TIME_DEPARTURE_ZULU";

		templete.query(sql, params,

		new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {

					PNLTransMissionDetailsDTO dto = new PNLTransMissionDetailsDTO();

					int flightID = rs.getInt("FLIGHT_ID");

					Timestamp timeStampzulu = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");

					Timestamp timeStampLocal = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");

					String departureStation = rs.getString("ori");

					int flightSegmentId = rs.getInt("FLT_SEG_ID");

					String flightNumber = rs.getString("FLIGHT_NUMBER");

					dto.setDepartureStation(departureStation);
					dto.setDepartureTimeZulu(timeStampzulu);
					dto.setFlightId(flightID);
					dto.setFlightSegId(flightSegmentId);
					dto.setDeparturetimeLocal(timeStampLocal);
					dto.setFlightNumber(flightNumber);

					String flight_type = rs.getString("FLIGHT_TYPE");
					if (FlightUtil.FLIGHT_TYPE_INT.equals(flight_type)) {
						dto.setFlightCutoffTime(internationalCutoffTime);
					} else {
						dto.setFlightCutoffTime(domesticCutoffTime);
					}

					flightList.add(dto);

				}

				return flightList;
			}

		}

		);

		return flightList;
	}

	/**
	 * @see PnlAdlTimingDAO.getFlightsForPNLSchedulingint pnldepartureGap, String flightNumber, Timestamp
	 *      flightDepTimeZulu)
	 * @param pnldepartureGap
	 * @return ArrayList of PNLTransMissionDetailsDTO
	 */
	public ArrayList<PNLTransMissionDetailsDTO> getFlightsForPNLScheduling(Date date, int defaultPnldepartureGap, int overridingPnlDepGap,
			String flightNumber) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Timestamp dateLowerLimit = new Timestamp(date.getTime() + ((24 + (defaultPnldepartureGap / 60)) * 3600 * 1000));
		Timestamp dateUpperLimit = new Timestamp(date.getTime() + ((24 + (overridingPnlDepGap / 60)) * 3600 * 1000));
		Object params[] = { dateUpperLimit, dateLowerLimit, "Y", };
		final ArrayList<PNLTransMissionDetailsDTO> flightList = new ArrayList<PNLTransMissionDetailsDTO>();

		String sql = "select distinct flight.FLIGHT_NUMBER,flightseg.FLIGHT_ID,flightSeg.EST_TIME_DEPARTURE_LOCAL,flightseg.EST_TIME_DEPARTURE_ZULU,substr(flightseg.segment_code,0,3) as ori,flightseg.FLT_SEG_ID from T_FLIGHT  flight,T_FLIGHT_SEGMENT"
				+ " flightseg  where flightseg.FLIGHT_ID=flight.FLIGHT_ID and flightseg.EST_TIME_DEPARTURE_ZULU<=? AND flightseg.EST_TIME_DEPARTURE_ZULU>? AND (flight.STATUS='ACT' OR flight.STATUS='CLS') and flightseg.SEGMENT_VALID_FLAG=? "
				+ " AND trim(flight.FLIGHT_NUMBER) = '"
				+ flightNumber
				+ "'"
				+ " order by flightseg.flight_id,flightseg.EST_TIME_DEPARTURE_ZULU ";

		templete.query(sql, params,

		new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {

					PNLTransMissionDetailsDTO dto = new PNLTransMissionDetailsDTO();

					int flightID = rs.getInt("FLIGHT_ID");

					Timestamp timeStampzulu = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");

					Timestamp timeStampLocal = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");

					String departureStation = rs.getString("ori");

					int flightSegmentId = rs.getInt("FLT_SEG_ID");

					String flightNumber = rs.getString("FLIGHT_NUMBER");

					dto.setDepartureStation(departureStation);
					dto.setDepartureTimeZulu(timeStampzulu);
					dto.setFlightId(flightID);
					dto.setFlightSegId(flightSegmentId);
					dto.setDeparturetimeLocal(timeStampLocal);
					dto.setFlightNumber(flightNumber);

					flightList.add(dto);

				}

				return flightList;
			}

		}

		);

		return flightList;
	}

	/**
	 * @see PnlAdlTimingDAO.hasPNLADLTiming(String flightNumber, String airportCode, Date flightDepZulu)
	 * @param flightNumber
	 * @param airportCode
	 * @param flightDepZulu
	 * @return if a timing record will return TRUE
	 */
	public boolean hasPNLADLTiming(String flightNumber, String airportCode, Date flightDepZulu) {

		String sql = " SELECT count(tc_id) " + " FROM t_pnl_adl_timings " + " WHERE trim(flight_number) = ? "
				+ " AND airport_code = ? " + " AND ? between zulu_start_date and zulu_end_date " + " AND status = ?";
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, airportCode, flightDepZulu, PNLConstants.PnlAdlTimings.ACTIVE };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int numOfRecs = 0;
		if (intObj != null) {
			numOfRecs = ((Integer) (intObj)).intValue();
		}

		if (numOfRecs == 1) {
			return true;
		} else {
			if (numOfRecs > 1) {
				// throw error
			}
			return false;
		}

	}

	/**
	 * @see PnlAdlTimingDAO().hasPNLADLTimingForAllFlightsInAirport(String airportCode, Date flightDepZulu)
	 * @param airportCode
	 * @param flightDepZulu
	 * @return if a timing record will return TRUE
	 */
	public boolean hasPNLADLTimingForAllFlightsInAirport(String airportCode, Date flightDepZulu) {

		String sql = " SELECT count(tc_id) " + " FROM t_pnl_adl_timings " + " WHERE " + " airport_code = ? "
				+ " AND ? between zulu_start_date and zulu_end_date " + " AND status = ?" + " AND apply_to_all = ?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { airportCode, flightDepZulu, PNLConstants.PnlAdlTimings.ACTIVE,
				PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int numOfRecs = 0;
		if (intObj != null) {
			numOfRecs = ((Integer) (intObj)).intValue();
		}

		if (numOfRecs == 1) {
			return true;
		} else {
			if (numOfRecs > 1) {
				// throw error
			}
			return false;
		}

	}

	/**
	 * @see PnlAdlTimingDAO().getPnlAdlTiming( PNLTransMissionDetailsDTO transMissionDetailsDTO)
	 * @param transMissionDetailsDTO
	 * @return PNLTransMissionDetailsDTO
	 */
	public PNLTransMissionDetailsDTO getPnlAdlTiming(PNLTransMissionDetailsDTO transMissionDetailsDTO) {

		String flightNumber = transMissionDetailsDTO.getFlightNumber();

		String airportCode = transMissionDetailsDTO.getDepartureStation();
		Date fltDepZulu = transMissionDetailsDTO.getDepartureTimeZulu();

		String sql = " SELECT pnl_dep_gap, adl_rep_int, last_adl_gap,adl_rep_int_cutoff_time ";
		sql += " FROM t_pnl_adl_timings ";
		sql += " WHERE trim(flight_number) = ? ";
		sql += " AND airport_code = ? ";
		sql += " AND ? between zulu_start_date and zulu_end_date ";
		sql += " AND status = ? ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, airportCode, fltDepZulu, PNLConstants.PnlAdlTimings.ACTIVE };

		Integer[] intPnlGap_AdlIntvl_LastAdlGap = (Integer[]) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer[] intPnlGap_AdlIntvl_LastAdlGap = new Integer[4];

				while (rs.next()) {

					intPnlGap_AdlIntvl_LastAdlGap[0] = new Integer(rs.getInt(1));
					intPnlGap_AdlIntvl_LastAdlGap[1] = new Integer(rs.getInt(2));
					intPnlGap_AdlIntvl_LastAdlGap[2] = new Integer(rs.getInt(3));
					intPnlGap_AdlIntvl_LastAdlGap[3] = new Integer(rs.getInt(4));
				}
				return intPnlGap_AdlIntvl_LastAdlGap;

			}

		});

		if (intPnlGap_AdlIntvl_LastAdlGap != null && intPnlGap_AdlIntvl_LastAdlGap.length == 4) {

			transMissionDetailsDTO.setPnlDepartureGap(intPnlGap_AdlIntvl_LastAdlGap[0].intValue());
			transMissionDetailsDTO.setAdlRepeatInterval(intPnlGap_AdlIntvl_LastAdlGap[1].intValue());
			transMissionDetailsDTO.setLastAdlGap(intPnlGap_AdlIntvl_LastAdlGap[2].intValue());
			transMissionDetailsDTO.setAdlRepeatIntervalAfterCutoffTime(intPnlGap_AdlIntvl_LastAdlGap[3].intValue());
		}

		return transMissionDetailsDTO;

	}

	/**
	 * @see PnlAdlTimingDAO().getPnlAdlTiming( PNLTransMissionDetailsDTO transMissionDetailsDTO)
	 * @param transMissionDetailsDTO
	 * @return PNLTransMissionDetailsDTO
	 */
	public PNLTransMissionDetailsDTO getPnlAdlAirportTiming(PNLTransMissionDetailsDTO transMissionDetailsDTO) {

		String airportCode = transMissionDetailsDTO.getDepartureStation();
		Date fltDepZulu = transMissionDetailsDTO.getDepartureTimeZulu();

		String sql = " SELECT pnl_dep_gap, adl_rep_int, last_adl_gap, adl_rep_int_cutoff_time ";
		sql += " FROM t_pnl_adl_timings ";
		sql += " WHERE ";
		sql += " airport_code = ? ";
		sql += " AND ? between zulu_start_date and zulu_end_date ";
		sql += " AND status = ? and apply_to_all = ?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { airportCode, fltDepZulu, PNLConstants.PnlAdlTimings.ACTIVE,
				PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES };

		Integer[] intPnlGap_AdlIntvl_LastAdlGap = (Integer[]) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer[] intPnlGap_AdlIntvl_LastAdlGap = new Integer[4];

				while (rs.next()) {

					intPnlGap_AdlIntvl_LastAdlGap[0] = new Integer(rs.getInt(1));
					intPnlGap_AdlIntvl_LastAdlGap[1] = new Integer(rs.getInt(2));
					intPnlGap_AdlIntvl_LastAdlGap[2] = new Integer(rs.getInt(3));
					intPnlGap_AdlIntvl_LastAdlGap[3] = new Integer(rs.getInt(4));
				}
				return intPnlGap_AdlIntvl_LastAdlGap;

			}

		});

		if (intPnlGap_AdlIntvl_LastAdlGap != null && intPnlGap_AdlIntvl_LastAdlGap.length == 4) {

			transMissionDetailsDTO.setPnlDepartureGap(intPnlGap_AdlIntvl_LastAdlGap[0].intValue());
			transMissionDetailsDTO.setAdlRepeatInterval(intPnlGap_AdlIntvl_LastAdlGap[1].intValue());
			transMissionDetailsDTO.setLastAdlGap(intPnlGap_AdlIntvl_LastAdlGap[2].intValue());
			transMissionDetailsDTO.setAdlRepeatIntervalAfterCutoffTime(intPnlGap_AdlIntvl_LastAdlGap[3].intValue());
		}

		return transMissionDetailsDTO;

	}

	/**
	 * @see ReservationAuxilliaryDAO().saveTiming(PnlAdlTiming timing)
	 * @param PnlAdlTiming
	 * 
	 */
	public void saveTiming(PnlAdlTiming timing) {
		hibernateSaveOrUpdate(timing);

	}

	/**
	 * @see ReservationAuxilliaryDAO().getAllTimings(String flightNumber, String airportCode, int startRec, int
	 *      numOfRecs)
	 * @return Page
	 */
	public Page getAllTimings(String flightNumber, String airportCode, int startRec, int numOfRecs) {

		List<ModuleCriterion> criteriaList = new ArrayList<ModuleCriterion>();

		if (flightNumber != null) {
			ModuleCriterion moduleCriterion = new ModuleCriterion();
			moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterion.setFieldName("flightNumber");
			List<String> values = new ArrayList<String>();
			values.add(flightNumber);
			moduleCriterion.setValue(values);
			criteriaList.add(moduleCriterion);
		}

		ModuleCriterion moduleCriterion2 = new ModuleCriterion();
		moduleCriterion2.setCondition(ModuleCriterion.CONDITION_EQUALS);
		moduleCriterion2.setFieldName("airportCode");
		List<String> values2 = new ArrayList<String>();
		values2.add(airportCode);
		moduleCriterion2.setValue(values2);

		criteriaList.add(moduleCriterion2);

		return getPagedData(criteriaList, startRec, numOfRecs, PnlAdlTiming.class, null);

	}

	/**
	 * @see ReservationAuxilliaryDAO().isOverlappingPNLADLTiming(String airportCode, String flightNumber, Date fromDate,
	 *      Date toDate)
	 * @return will return TRUE if overlappling record/duplicating record found
	 */
	public boolean isOverlappingPNLADLTiming(String airportCode, String flightNumber, Date fromDate, Date toDate) {

		String hql = "select count(*) from PnlAdlTiming tim where tim.airportCode like :airportcode"
				+ " and tim.flightNumber like :flightnumber"
				+ " and (:fromdate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or :todate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or (:todate<=tim.startingZuluDate and :fromdate >= tim.endingZuluDate) "
				+ " or (:todate>=tim.startingZuluDate and :fromdate <= tim.endingZuluDate)) " + " and tim.status = :status"
				+ " and tim.applyToAllFlights = :applytoall";

		Query query = getSession().createQuery(hql).setParameter("airportcode", airportCode)
				.setParameter("flightnumber", flightNumber).setTimestamp("fromdate", fromDate).setTimestamp("todate", toDate)
				.setString("status", PNLConstants.PnlAdlTimings.ACTIVE)
				.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_NO);
		;

		int count = ((Long) query.uniqueResult()).intValue();
		if (count == 0) {

			return false;
		} else {
			return true;
		}

	}

	public boolean isOverlappingPNLADLTimingEditMode(String airportCode, String flightNumber, Date fromDate, Date toDate,
			Integer id) {

		String hql = "select count(*) from PnlAdlTiming tim where tim.airportCode like :airportcode"
				+ " and tim.flightNumber like :flightnumber"
				+ " and (:fromdate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or :todate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or (:todate<=tim.startingZuluDate and :fromdate >= tim.endingZuluDate) "
				+ " or (:todate>=tim.startingZuluDate and :fromdate <= tim.endingZuluDate)) " + " and tim.status = :status"
				+ " and tim.applyToAllFlights = :applytoall" + " and tim.id != :id";

		Query query = getSession().createQuery(hql).setParameter("airportcode", airportCode)
				.setParameter("flightnumber", flightNumber).setTimestamp("fromdate", fromDate).setTimestamp("todate", toDate)
				.setString("status", PNLConstants.PnlAdlTimings.ACTIVE)
				.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_NO).setInteger("id", id.intValue());
		;

		int count = ((Long) query.uniqueResult()).intValue();
		if (count == 0) {

			return false;
		} else {
			return true;
		}

	}

	/**
	 * @see ReservationAuxilliaryDAO().isOverlappingPNLADLTiming(String airportCode, String flightNumber, Date fromDate,
	 *      Date toDate)
	 * @return will return TRUE if overlappling record/duplicating record found
	 */
	public boolean hasDuplicateGlobalPNLADLTiming(String airportCode, Date fromDate, Date toDate) {

		// String flightNumber="";

		String hql = "select count(*) from PnlAdlTiming tim where tim.airportCode like :airportcode"
				// + " and tim.flightNumber like :flightnumber"
				+ " and (:fromdate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or :todate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or (:todate<=tim.startingZuluDate and :fromdate >= tim.endingZuluDate) "
				+ " or (:todate>=tim.startingZuluDate and :fromdate <= tim.endingZuluDate)) " + " and tim.status = :status"
				+ " and tim.applyToAllFlights = :applytoall";

		Query query = getSession().createQuery(hql)
				.setParameter("airportcode", airportCode)
				// .setParameter("flightnumber",flightNumber)
				.setTimestamp("fromdate", fromDate).setTimestamp("todate", toDate)
				.setString("status", PNLConstants.PnlAdlTimings.ACTIVE)
				.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES);
		;

		int count = ((Long) query.uniqueResult()).intValue();
		if (count == 0) {

			return false;
		} else {
			return true;
		}

	}

	public boolean hasDuplicateGlobalPNLADLTimingEditMode(String airportCode, Date fromDate, Date toDate, Integer id) {

		// String flightNumber="";

		String hql = "select count(*) from PnlAdlTiming tim where tim.airportCode like :airportcode"
				// + " and tim.flightNumber like :flightnumber"
				+ " and (:fromdate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or :todate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or (:todate<=tim.startingZuluDate and :fromdate >= tim.endingZuluDate) "
				+ " or (:todate>=tim.startingZuluDate and :fromdate <= tim.endingZuluDate)) " + " and tim.status = :status"
				+ " and tim.applyToAllFlights = :applytoall" + " and tim.id != :id";

		Query query = getSession().createQuery(hql)
				.setParameter("airportcode", airportCode)
				// .setParameter("flightnumber",flightNumber)
				.setTimestamp("fromdate", fromDate).setTimestamp("todate", toDate)
				.setString("status", PNLConstants.PnlAdlTimings.ACTIVE)
				.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES).setInteger("id", id.intValue());
		;

		int count = ((Long) query.uniqueResult()).intValue();
		if (count == 0) {

			return false;
		} else {
			return true;
		}

	}

	public boolean hasAirportTiming(Date date) {

		String hql = "select count(*) from PnlAdlTiming tim "
				+ " where :nowdate between tim.startingZuluDate and tim.endingZuluDate " + " and tim.status = :status"
				+ " and tim.applyToAllFlights = :applytoall";

		Query query = getSession().createQuery(hql).setTimestamp("nowdate", date)
				.setString("status", PNLConstants.PnlAdlTimings.ACTIVE)
				.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES);
		;

		int count = ((Long) query.uniqueResult()).intValue();
		if (count == 0) {

			return false;
		} else {
			return true;
		}

	}

	/**
	 * getAirportTimings()
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<String[]> getAirportTimings(Date date) {

		String sql = " SELECT airport_code, pnl_dep_gap ";
		sql += " FROM t_pnl_adl_timings ";
		sql += " WHERE ";
		sql += " ? between zulu_start_date and zulu_end_date ";
		sql += " AND status = ? and apply_to_all = ?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { date, PNLConstants.PnlAdlTimings.ACTIVE, PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES };

		Collection<String[]> airportCodePnlGaps = (Collection<String[]>) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Collection<String[]> codeTimings = new ArrayList<String[]>();

				while (rs.next()) {
					String codeTime[] = new String[2];
					codeTime[0] = new String(rs.getString(1));
					codeTime[1] = new String(rs.getString(2));
					codeTimings.add(codeTime);
				}
				return codeTimings;

			}

		});
		return airportCodePnlGaps;
	}

	public boolean hasFlightTimings(Date date) {

		String hql = "select count(*) from PnlAdlTiming tim "
				+ " where :nowdate between tim.startingZuluDate and tim.endingZuluDate " + " and tim.status = :status"
				+ " and tim.applyToAllFlights = :applytoall";

		Query query = getSession().createQuery(hql).setTimestamp("nowdate", date)
				.setString("status", PNLConstants.PnlAdlTimings.ACTIVE)
				.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_NO);
		;

		int count = ((Long) query.uniqueResult()).intValue();
		if (count == 0) {

			return false;
		} else {
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	public Collection<String[]> getFlightTimings(Date date) {

		String sql = " SELECT flight_number, pnl_dep_gap ";
		sql += " FROM t_pnl_adl_timings ";
		sql += " WHERE ";
		sql += " ? between zulu_start_date and zulu_end_date ";
		sql += " AND status = ? and apply_to_all = ?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { date, PNLConstants.PnlAdlTimings.ACTIVE, PNLConstants.PnlAdlTimings.APPLY_TO_ALL_NO };

		Collection<String[]> airportCodePnlGaps = (Collection<String[]>) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Collection<String[]> codeTimings = new ArrayList<String[]>();

				while (rs.next()) {
					String codeTime[] = new String[2];
					codeTime[0] = new String(rs.getString(1));
					codeTime[1] = new String(rs.getString(2));
					codeTimings.add(codeTime);
				}
				return codeTimings;

			}

		});
		return airportCodePnlGaps;
	}

}