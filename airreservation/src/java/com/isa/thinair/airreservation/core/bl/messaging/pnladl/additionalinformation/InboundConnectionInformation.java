/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;

/**
 * @author udithad
 *
 */
public class InboundConnectionInformation extends
		PassengerAdditions<List<PassengerInformation>> {

	private ReservationAuxilliaryDAO reservationAuxilliaryDAO;
	private String departureAirportCode;
	private Integer flightId;

	public InboundConnectionInformation(String departureAirportCode,
			Integer flightId) {
		this.departureAirportCode = departureAirportCode;
		this.flightId = flightId;
		reservationAuxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
	}

	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> passengerInformations) {
		for (PassengerInformation passengerInformation : passengerInformations) {
			passengerInformation
					.setInboundInfo(getInboundConnectionBy(passengerInformation
							.getPnrPaxId()));
		}
	}

	private InboundConnectionDTO getInboundConnectionBy(Integer passengerId) {
		return this.inboundConnectionInformation.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformations) {
		for (PassengerInformation passengerInformation : passengerInformations) {
			InboundConnectionDTO inboundConnection = reservationAuxilliaryDAO
					.getInBoundConnectionList(passengerInformation.getPnr(),
							passengerInformation.getPnrPaxId(),
							departureAirportCode, flightId);
			this.inboundConnectionInformation.put(
					passengerInformation.getPnrPaxId(), inboundConnection);
		}
	}

}
