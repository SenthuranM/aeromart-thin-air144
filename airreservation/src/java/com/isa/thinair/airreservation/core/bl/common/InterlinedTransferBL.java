/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentTransferDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegmentTransfer;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.util.WSClientResponseCodes;

/**
 * Code Share related business methods
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class InterlinedTransferBL {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(InterlinedTransferBL.class);


	/**
	 * Hide the constructor
	 */
	private InterlinedTransferBL() {

	}

	/**
	 * Returns the PNR and Segment Information
	 * 
	 * @param executionTime
	 * @param interlinedAirLineTO
	 * @return
	 */
	public static Map<String, Collection<ReservationSegmentDTO>> getPNRAndSegmentInformation(Date executionTime, InterlinedAirLineTO interlinedAirLineTO) {
		Collection<String> colFlightLegends = new ArrayList<String>();
		colFlightLegends.add(interlinedAirLineTO.getCarrierCode());

		Collection<String> colSegmentStates = new ArrayList<String>();
		colSegmentStates.add(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);

		Collection<String> colStates = new ArrayList<String>();
		colStates.add(ReservationInternalConstants.ReservationSegmentTransfer.INITIATED);
		colStates.add(ReservationInternalConstants.ReservationSegmentTransfer.FAILED);

		return ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.getSegmentInformation(colFlightLegends, executionTime,
				interlinedAirLineTO.getInterlineCutOverInMinutes(), interlinedAirLineTO.getTransferCutOverInMinutes(),
				colSegmentStates, colStates);
	}

	/**
	 * Update the reservation segment transfer states
	 * 
	 * @param colReservationSegmentDTO
	 * @param noOfTimes
	 * @param transferPNR
	 * @param processDesc
	 * @param status
	 * @throws ModuleException
	 */
	public static void updateReservationSegmentTransferStates(Collection<ReservationSegmentDTO> colReservationSegmentDTO, String transferPNR,
			String processDesc, String status) throws ModuleException {
		if (colReservationSegmentDTO != null && colReservationSegmentDTO.size() > 0) {
			Collection<ReservationSegmentTransfer> updatablePNRSegmentTransfer = new ArrayList<ReservationSegmentTransfer>();
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
			ReservationSegmentTransfer reservationSegmentTransfer;
			ReservationSegmentDTO reservationSegmentDTO;

			for (Iterator<ReservationSegmentDTO> itr = colReservationSegmentDTO.iterator(); itr.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) itr.next();

				// If it is the initiated step
				if (ReservationInternalConstants.ReservationSegmentTransfer.INITIATED.equals(status)) {
					reservationSegmentTransfer = reservationSegmentDAO
							.getReservationSegmentTransferByPnrSegId(reservationSegmentDTO.getPnrSegId());

					if (reservationSegmentTransfer == null) {
						reservationSegmentTransfer = new ReservationSegmentTransfer(reservationSegmentDTO.getPnrSegId(),
								AppSysParamsUtil.extractCarrierCode(reservationSegmentDTO.getFlightNo()), new Date(),
								ReservationInternalConstants.ReservationSegmentTransfer.INITIATED);
						updatablePNRSegmentTransfer.add(reservationSegmentTransfer);
					} else {
						reservationSegmentTransfer.setExecutionTimeStamp(new Date());
						reservationSegmentTransfer.setProcessDesc("");
						reservationSegmentTransfer.setStatus(ReservationInternalConstants.ReservationSegmentTransfer.INITIATED);
						updatablePNRSegmentTransfer.add(reservationSegmentTransfer);
					}
					// If it is the failed step
				} else if (ReservationInternalConstants.ReservationSegmentTransfer.FAILED.equals(status)) {
					reservationSegmentTransfer = reservationSegmentDAO
							.getReservationSegmentTransferByPnrSegId(reservationSegmentDTO.getPnrSegId());

					if (reservationSegmentTransfer == null) {
						reservationSegmentTransfer = new ReservationSegmentTransfer(reservationSegmentDTO.getPnrSegId(),
								AppSysParamsUtil.extractCarrierCode(reservationSegmentDTO.getFlightNo()), new Date(),
								ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
						updatablePNRSegmentTransfer.add(reservationSegmentTransfer);
					} else {
						reservationSegmentTransfer.setExecutionTimeStamp(new Date());
						reservationSegmentTransfer.setProcessDesc(processDesc);
						reservationSegmentTransfer.setStatus(ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
						updatablePNRSegmentTransfer.add(reservationSegmentTransfer);
					}
					// If it's the success step
				} else if (ReservationInternalConstants.ReservationSegmentTransfer.SUCCESS.equals(status)) {
					reservationSegmentTransfer = reservationSegmentDAO
							.getReservationSegmentTransferByPnrSegId(reservationSegmentDTO.getPnrSegId());

					if (reservationSegmentTransfer == null) {
						throw new ModuleException("airreservations.arg.reservationSegmentTransferDoesNotExist");
					} else {
						reservationSegmentTransfer.setExecutionTimeStamp(new Date());
						reservationSegmentTransfer.setTransferPnr(transferPNR);
						reservationSegmentTransfer.setProcessDesc(processDesc);
						reservationSegmentTransfer.setStatus(ReservationInternalConstants.ReservationSegmentTransfer.SUCCESS);
						updatablePNRSegmentTransfer.add(reservationSegmentTransfer);
					}
				}
			}

			if (updatablePNRSegmentTransfer.size() > 0) {
				reservationSegmentDAO.saveReservationSegmentsTransfer(updatablePNRSegmentTransfer);
			}
		}
	}

	/**
	 * Process PNR For Segments
	 * 
	 * @param pnr
	 * @param interlinedAirLineTO
	 * @param colReservationSegmentDTO
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public static void processPNRForSegments(String pnr, InterlinedAirLineTO interlinedAirLineTO,
			Collection<ReservationSegmentDTO> colReservationSegmentDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug(" Started Processing PNR : " + pnr);
		}

		try {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadLastUserNote(true);

			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// Come on.. The booking should be confirmed in the first place in order to transfer it
			if (AccelAeroCalculator.add(reservation.getTotalChargeAmounts()).doubleValue() <= reservation.getTotalPaidAmount()
					.doubleValue()) {
				Collection<ReservationSegmentDTO> identifiedInterlineSegments = getIdentifiedInterlineSegments(reservation, interlinedAirLineTO,
						colReservationSegmentDTO);

				if (log.isDebugEnabled()) {
					log.debug(" Actually started processing PNR : " + reservation.getPnr());
					log.debug(" Identified PNR Segment Id(s) " + ReservationApiUtils.getPnrSegIds(identifiedInterlineSegments));
				}

				processTransfer(reservation, identifiedInterlineSegments, interlinedAirLineTO, credentialsDTO);
			}
		} catch (ModuleException e) {
			log.error("(((o))) processPNRForSegments[ModuleException] :: ", e);
			ReservationModuleUtils.getSegmentBD().updateReservationSegmentTransfer(colReservationSegmentDTO, null, e.getMessageString(),
					ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
		} catch (Exception e) {
			log.error("(((o))) processPNRForSegments[Exception] :: ", e);
			ReservationModuleUtils.getSegmentBD().updateReservationSegmentTransfer(colReservationSegmentDTO, null, e.getMessage(),
					ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
		}

		if (log.isDebugEnabled()) {
			log.debug(" Completed Processing PNR : " + pnr);
		}
	}

	/**
	 * Returns the identified segments
	 * 
	 * @param reservation
	 * @param interlinedAirLineTO
	 * @param colReservationSegmentDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<ReservationSegmentDTO> getIdentifiedInterlineSegments(Reservation reservation, InterlinedAirLineTO interlinedAirLineTO,
			Collection<ReservationSegmentDTO> colReservationSegmentDTO) throws ModuleException {
		String defaultCarrierCode = AppSysParamsUtil.getCarrierCode();
		Collection<Integer> ondGroupIds = ReservationApiUtils.getONDGroupIds(colReservationSegmentDTO);
		Collection<ReservationSegmentDTO> colTransferSegments = new ArrayList<ReservationSegmentDTO>();
		String carrierCode;

		ReservationSegmentDTO reservationSegmentDTO;
		for (Iterator<ReservationSegmentDTO> itr = reservation.getSegmentsView().iterator(); itr.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) itr.next();

			if (ondGroupIds.contains(reservationSegmentDTO.getFareGroupId())) {
				carrierCode = AppSysParamsUtil.extractCarrierCode(reservationSegmentDTO.getFlightNo());
				if (!defaultCarrierCode.equals(carrierCode) && carrierCode.equals(interlinedAirLineTO.getCarrierCode())) {
					colTransferSegments.add(reservationSegmentDTO);
				}
			}
		}

		Collection<ReservationSegmentDTO> colIdentifiedSegments = new ArrayList<ReservationSegmentDTO>();
		Collection<Integer> colPnrSegIds = ReservationApiUtils.getPnrSegIds(colTransferSegments);
		Map<Integer, ReservationSegmentTransfer> pnrSegAndExtSegs = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
				.getReservationTransferSegments(colPnrSegIds);
		ReservationSegmentTransfer reservationSegmentTransfer;

		for (Iterator<ReservationSegmentDTO> itr = colTransferSegments.iterator(); itr.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) itr.next();
			reservationSegmentTransfer = (ReservationSegmentTransfer) pnrSegAndExtSegs.get(reservationSegmentDTO.getPnrSegId());

			if (reservationSegmentTransfer == null
					|| !ReservationInternalConstants.ReservationSegmentTransfer.SUCCESS.equals(reservationSegmentTransfer
							.getStatus())) {
				colIdentifiedSegments.add(reservationSegmentDTO);
			}
		}

		return colIdentifiedSegments;
	}

	/**
	 * Process the transfer operation
	 * 
	 * @param reservation
	 * @param colReservationSegmentDTO
	 * @param interlinedAirLineTO
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private static void processTransfer(Reservation reservation, Collection<ReservationSegmentDTO> colReservationSegmentDTO,
			InterlinedAirLineTO interlinedAirLineTO, CredentialsDTO credentialsDTO) throws ModuleException {
		try {
			SegmentBD segmentBD = ReservationModuleUtils.getSegmentBD();
			segmentBD.updateReservationSegmentTransfer(colReservationSegmentDTO, null, null,
					ReservationInternalConstants.ReservationSegmentTransfer.INITIATED);

			ServiceResponce serviceResponce = ReservationModuleUtils.getWSClientBD().transferReservation(reservation,
					ReservationApiUtils.getPnrSegIds(colReservationSegmentDTO), interlinedAirLineTO);

			// Only if the response is success
			if (serviceResponce.isSuccess()) {
				// Need to extract the information
				String transferPNR = (String) serviceResponce
						.getResponseParam(WSClientResponseCodes.InterlinedTypes.TRANSFER_PNR);
				String segmentInfo = (String) serviceResponce
						.getResponseParam(WSClientResponseCodes.InterlinedTypes.SEGMENT_INFORMATION);
				String chargesInfo = (String) serviceResponce
						.getResponseParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PRICE_AMOUNT)
						+ " ["
						+ (String) serviceResponce.getResponseParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PRICE_CURRENCY)
						+ "] ";
				String paymentInfo = (String) serviceResponce
						.getResponseParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PAYMENT_AMOUNT)
						+ " ["
						+ (String) serviceResponce.getResponseParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PAYMENT_CURRENCY)
						+ "] ";
				Collection<ReservationSegmentDTO> successReservationSegmentDTOs = (Collection<ReservationSegmentDTO>) serviceResponce
						.getResponseParam(WSClientResponseCodes.InterlinedTypes.SUCCESSFUL_TRANSFER_SEGMENT_DTOS);
				Collection<ReservationSegmentDTO> failedReservationSegmentDTOs = (Collection<ReservationSegmentDTO>) serviceResponce
						.getResponseParam(WSClientResponseCodes.InterlinedTypes.FAILED_TRANSFER_SEGMENT_DTOS);
				String processDesc = (String) serviceResponce
						.getResponseParam(WSClientResponseCodes.InterlinedTypes.PROCESS_DESC);

				auditReservation(reservation.getPnr(), transferPNR, segmentInfo, chargesInfo, paymentInfo, credentialsDTO);

				// Success ones
				if (successReservationSegmentDTOs != null && successReservationSegmentDTOs.size() > 0) {
					String auditDesc = prepareTransferAudit(transferPNR, segmentInfo, chargesInfo, paymentInfo);
					segmentBD.updateReservationSegmentTransfer(successReservationSegmentDTOs, transferPNR, auditDesc,
							ReservationInternalConstants.ReservationSegmentTransfer.SUCCESS);
				}

				// Failed ones
				if (failedReservationSegmentDTOs != null && failedReservationSegmentDTOs.size() > 0) {
					String auditDesc = prepareFailureAudit(segmentInfo, processDesc, failedReservationSegmentDTOs);
					segmentBD.updateReservationSegmentTransfer(failedReservationSegmentDTOs, null, auditDesc,
							ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
				}
			} else {
				String errorDesc = (String) serviceResponce.getResponseParam(WSClientResponseCodes.InterlinedTypes.PROCESS_DESC);
				segmentBD.updateReservationSegmentTransfer(colReservationSegmentDTO, null, errorDesc,
						ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
			}
		} catch (ModuleException e) {
			log.error("(((o))) processTransfer[ModuleException] :: ", e);
			ReservationModuleUtils.getSegmentBD().updateReservationSegmentTransfer(
					colReservationSegmentDTO, null, e.getMessageString(),
					ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
		} catch (Exception e) {
			log.error("(((o))) processTransfer[Exception] :: ", e);
			ReservationModuleUtils.getSegmentBD().updateReservationSegmentTransfer(
					colReservationSegmentDTO, null, e.getMessage(),
					ReservationInternalConstants.ReservationSegmentTransfer.FAILED);
		}
	}

	/**
	 * Prepares the failure audit
	 * 
	 * @param segmentInfo
	 * @param failedReservationSegmentDTOs
	 * @return
	 */
	private static String prepareFailureAudit(String segmentInfo, String processDesc, Collection<ReservationSegmentDTO> failedReservationSegmentDTOs) {
		StringBuilder segmentsInfo = new StringBuilder();
		ReservationSegmentDTO reservationSegmentDTO;
		for (Iterator<ReservationSegmentDTO> itr = failedReservationSegmentDTOs.iterator(); itr.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) itr.next();

			if (segmentsInfo.length() == 0) {
				segmentsInfo.append(reservationSegmentDTO.toSimpleString());
			} else {
				segmentsInfo.append(" " + reservationSegmentDTO.toSimpleString());
			}
		}

		return " Transfer failed for the following segment(s) [" + segmentsInfo + "] " + BeanUtils.nullHandler(processDesc);
	}

	/**
	 * Prepare the transfer audit
	 * 
	 * @param transferPNR
	 * @param segmentInfo
	 * @param chargesInfo
	 * @param paymentInfo
	 * @return
	 */
	private static String prepareTransferAudit(String transferPNR, String segmentInfo, String chargesInfo, String paymentInfo) {
		return " Transfered to PNR [" + transferPNR + "] Segment Info [" + segmentInfo + "] Charges [" + chargesInfo
				+ "] Payments [" + paymentInfo + "] ";
	}

	/**
	 * Audit the Reservation
	 * 
	 * @param pnr
	 * @param transferPNR
	 * @param segmentInfo
	 * @param chargesInfo
	 * @param paymentInfo
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private static void auditReservation(String pnr, String transferPNR, String segmentInfo, String chargesInfo,
			String paymentInfo, CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.INTERLINED_SEGMENT_TRANSFER.getCode());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferInterlinedSegment.TRANSFER_PNR, transferPNR);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferInterlinedSegment.SEGMENT_INFO, segmentInfo);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferInterlinedSegment.CHARGES_INFO, chargesInfo);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferInterlinedSegment.PAYMENTS_INFO, paymentInfo);

		// Record the modification
		ReservationBO.recordModification(pnr, reservationAudit, null, credentialsDTO);
	}

	/**
	 * Notify reservation segment transfer failures
	 * 
	 * @param executionDate
	 * @throws ModuleException
	 */
	public static void notifySegmentTransferFailures(Date executionDate) throws ModuleException {
		Collection<String> states = new ArrayList<String>();
		states.add(ReservationInternalConstants.ReservationSegmentTransfer.INITIATED);
		states.add(ReservationInternalConstants.ReservationSegmentTransfer.FAILED);

		Collection<ReservationSegmentTransferDTO> colReservationSegmentTransferDTO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
				.getReservationTransferSegments(states, executionDate);

		if (colReservationSegmentTransferDTO != null && colReservationSegmentTransferDTO.size() > 0) {
			Map<String,Collection<ReservationSegmentTransferDTO>> statusAndSegments = groupByStates(colReservationSegmentTransferDTO);
			emailRecipients(statusAndSegments);
		}
	}

	/**
	 * Group the records by the status
	 * 
	 * @param colReservationSegmentTransferDTO
	 * @return
	 */
	private static Map<String,Collection<ReservationSegmentTransferDTO>> groupByStates(Collection<ReservationSegmentTransferDTO> colReservationSegmentTransferDTO) {
		ReservationSegmentTransferDTO reservationSegmentTransferDTO;
		Map<String,Collection<ReservationSegmentTransferDTO>> statusAndSegments = new HashMap<String,Collection<ReservationSegmentTransferDTO>>();

		for (Iterator<ReservationSegmentTransferDTO> iter = colReservationSegmentTransferDTO.iterator(); iter.hasNext();) {
			reservationSegmentTransferDTO = (ReservationSegmentTransferDTO) iter.next();

			if (statusAndSegments.containsKey(reservationSegmentTransferDTO.getTransferStatus())) {
				Collection<ReservationSegmentTransferDTO> segments = (Collection<ReservationSegmentTransferDTO>) statusAndSegments.get(reservationSegmentTransferDTO.getTransferStatus());
				segments.add(reservationSegmentTransferDTO);

			} else {
				Collection<ReservationSegmentTransferDTO> segments = new ArrayList<ReservationSegmentTransferDTO>();
				segments.add(reservationSegmentTransferDTO);

				statusAndSegments.put(reservationSegmentTransferDTO.getTransferStatus(), segments);
			}
		}

		return statusAndSegments;
	}

	/**
	 * Email the recipients
	 * 
	 * @param statusAndSegments
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void emailRecipients(Map<String,Collection<ReservationSegmentTransferDTO>> statusAndSegments) {
		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
		Map<String,String> recipients = ReservationModuleUtils.getAirReservationConfig().getInterlineSegTransRecipientsMap();
		Collection<ReservationSegmentTransferDTO> initiatedSegments = (Collection<ReservationSegmentTransferDTO>) statusAndSegments
				.get(ReservationInternalConstants.ReservationSegmentTransfer.INITIATED);
		Collection<ReservationSegmentTransferDTO> failedSegments = (Collection<ReservationSegmentTransferDTO>) statusAndSegments
				.get(ReservationInternalConstants.ReservationSegmentTransfer.FAILED);

		for (Iterator<String> recepientsIt = recipients.keySet().iterator(); recepientsIt.hasNext();) {
			String key = (String) recepientsIt.next();

			UserMessaging userMessaging = new UserMessaging();
			userMessaging.setFirstName(key);
			userMessaging.setToAddres((String) recipients.get(key));

			List<UserMessaging> messageList = new ArrayList<UserMessaging>();
			messageList.add(userMessaging);

			HashMap map = new HashMap();
			map.put("initiatedSegments", initiatedSegments);
			map.put("failedSegments", failedSegments);
			map.put("userName", key);

			Topic topic = new Topic();
			topic.setTopicParams(map);
			topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.SEGMENT_TRANSFER_FAILURES_TEMPLATE);

			MessageProfile msgProfile = new MessageProfile();
			msgProfile.setUserMessagings(messageList);
			msgProfile.setTopic(topic);

			List<MessageProfile> profList = new ArrayList<MessageProfile>();
			profList.add(msgProfile);

			messagingServiceBD.sendMessages(profList);
		}
	}
}