package com.isa.thinair.airreservation.core.bl.common;

import com.isa.thinair.airreservation.core.bl.tty.TypeBReservationMessageCreator;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

public abstract class TypeBMessageCreatorFactory {

	public abstract TypeBReservationMessageCreator createTypeBReservationMessageCreator(
			GDSInternalCodes.GDSNotifyAction gdsNotifyAction);

	public TypeBReservationMessageCreator getTypeBReservationMessageCreator(GDSInternalCodes.GDSNotifyAction gdsNotifyAction) {
		TypeBReservationMessageCreator typeBReservationMessageCreator = createTypeBReservationMessageCreator(gdsNotifyAction);
		return typeBReservationMessageCreator;
	}
}
