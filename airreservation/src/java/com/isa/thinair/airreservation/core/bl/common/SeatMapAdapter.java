package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapJDBCDAO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PassengerSeatingAssembler;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.model.OndPromotion;
import com.isa.thinair.promotion.api.model.OndPromotionCharge;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;

public class SeatMapAdapter {

	private static Log log = LogFactory.getLog(SeatMapAdapter.class);

	/**
	 * Called when modify only seats
	 * 
	 * @param res
	 * @param paxSeatingAssembler
	 * @throws ModuleException
	 */
	public static void prepareSeatsToModify(Reservation res, PassengerSeatingAssembler paxSeatingAssembler,
			TrackInfoDTO trackInfo) throws ModuleException {
		Map<Integer, Integer> flightSEatIdpnrSegId = paxSeatingAssembler.getFlightSeatIdPnrSegIdsMap();

		Map<String, PaxSeatTO> mapPaxSeats = paxSeatingAssembler.getPaxSeatingInfo();
		// get the pnr pax ids
		Collection<Integer> pnrPaxIds = res.getPnrPaxIds();
		SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;
		// get the existing seating list
		Collection<PassengerSeating> passengerSeating = seatMapDAO.getFlightSeats(pnrPaxIds);
		Map<String, PassengerSeating> paxSeatingMap = new HashMap<String, PassengerSeating>();

		if (passengerSeating == null || passengerSeating.size() > 0) {
			for (PassengerSeating paxSeat : passengerSeating) {
				String uniqueId = SMUtil.makeUniqueIdForModifications(paxSeat.getPnrPaxId(), paxSeat.getPpfId(),
						paxSeat.getPnrSegId(), paxSeat.getFlightSeatId());
				paxSeatingMap.put(uniqueId, paxSeat);
			}
		}

		Map<String, PaxSeatTO> seatsRemMap = paxSeatingAssembler.getPaxSeatToRemove();
		if (seatsRemMap == null) {
			seatsRemMap = new HashMap<String, PaxSeatTO>();
		}
		Set<String> seatIdSet = new HashSet<String>();
		for (PaxSeatTO paxS : seatsRemMap.values()) {
			seatIdSet.add(SMUtil.makeUniqueIdForModifications(paxS.getPnrPaxId(), paxS.getPnrPaxFareId(), paxS.getPnrSegId(),
					paxS.getSelectedFlightSeatId()));
		}

		// Verify with AARESAA-16131
		Collection<PassengerSeating> statusToCNx = new ArrayList<PassengerSeating>();

		for (String seatId : seatIdSet) {
			if (paxSeatingMap.containsKey(seatId)) {
				statusToCNx.add(paxSeatingMap.get(seatId));
			} else {
				throw new ModuleException("removing a seat that does not belong to user");
			}
		}

		if (statusToCNx.size() > 0) {
			seatMapDAO.deleteReservationSeats(statusToCNx);
		}

		// TODO validate newly adding seats do not belong to another pax/pnr

		// BELOW BLOCK OF CODE IS DEPRECATED AND KEPT INTACT TO PROVIDE BACKWARD COMPATIBILITY
		// FOR V1 FLOW... BUT ESSENTIALLY THIS IS NOT REQUIRED AFTER HEAD MERGE IS COMPLETED
		// TODO REMOVE THIS BLOCK AND REFACTOR THE CODE
		if (mapPaxSeats.size() > 0) {
			if (passengerSeating == null || passengerSeating.size() > 0) {

				paxSeatingAssembler.getPaxSeatToRemove();
				// get the passenger seating from front end.
				Collection<String> keysToRemove = new ArrayList<String>();

				// in the existing seating list, if the values are same from front end, do nothing, if existing list has
				// more.. mark them for deletion, the rest add them
				for (PassengerSeating seating : passengerSeating) {
					String uniqueId = SMUtil.makeUniqueIdForModifications(seating.getPnrPaxId(), seating.getPpfId(),
							seating.getFlightSeatId());

					PaxSeatTO paxSeatTO = mapPaxSeats.get(uniqueId);

					if (paxSeatTO == null) {
						// marked for deletion
						paxSeatingAssembler.removePassengerFromFlightSeat(seating.getPnrPaxId(), seating.getPpfId(),
								seating.getPnrSegId(), seating.getFlightSeatId(), seating.getChargeAmount(), trackInfo);
						statusToCNx.add(seating);
					} else {
						// remove from collection as the info already exists in db
						keysToRemove.add(uniqueId);
					}
				}
				for (String uniqueId : keysToRemove) {
					mapPaxSeats.remove(uniqueId);

				}
				// update the stautus to CNX for ones which should get removed
				seatMapDAO.deleteReservationSeats(statusToCNx);
			}

			// add the rest
			Collection<PaxSeatTO> newFlghtSeats = mapPaxSeats.values();
			Iterator<PaxSeatTO> newFlightSeatsIter = newFlghtSeats.iterator();
			while (newFlightSeatsIter.hasNext()) {
				PaxSeatTO paxSeatTO = newFlightSeatsIter.next();
				Integer pnrSegId = flightSEatIdpnrSegId.get(paxSeatTO.getSelectedFlightSeatId());
				paxSeatingAssembler.addPassengertoNewFlightSeat(paxSeatTO.getPnrPaxId(), paxSeatTO.getPnrPaxFareId(), pnrSegId,
						paxSeatTO.getSelectedFlightSeatId(), paxSeatTO.getChgDTO().getAmount(), paxSeatTO.getSeatCode(),
						paxSeatTO.getPaxType(), paxSeatTO.getAutoCancellationId(), trackInfo);
			}
		}
	}

	/**
	 * MOD SEATS
	 * 
	 * @param flightAmSeatIds
	 * @return
	 */
	private static String getSeatCodesAndSeatChargeForAuditContent(Collection<Integer> flightAmSeatIds) {
		if (flightAmSeatIds != null && flightAmSeatIds.size() > 0) {
			Collection<String> seatCodes = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
					.getSeatCodesAndSeatCharge(flightAmSeatIds);
			if (seatCodes != null && seatCodes.size() > 0) {
				return SMUtil.getSeatAuditString(seatCodes);
			}
		}
		return null;
	}

	/**
	 * Audit for Seat Modifications
	 * 
	 * @param pnr
	 * @param statusAndFlightSeatIdMap
	 * @param credentialsDTO
	 * @param userNote
	 * @throws ModuleException
	 */
	public static void recordAuditHistoryForModifySeats(String pnr, Map<String, Collection<Integer>> statusAndFlightSeatIdMap,
			CredentialsDTO credentialsDTO, String userNote) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		String deselectedSeatCodes = getSeatCodesAndSeatChargeForAuditContent(
				statusAndFlightSeatIdMap.get(AirinventoryCustomConstants.FlightSeatStatuses.VACANT));
		String newlySelectedSeatCodes = getSeatCodesAndSeatChargeForAuditContent(
				statusAndFlightSeatIdMap.get(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED));

		reservationAudit.setModificationType(AuditTemplateEnum.MODIFY_SEATS.getCode());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.REMOVED, deselectedSeatCodes);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.ADDED, newlySelectedSeatCodes);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		ReservationBO.recordModification(pnr, reservationAudit, userNote, credentialsDTO);
	}

	public static void modifySeats(Map<String, PaxSeatTO> paxSeatToAdd, Map<String, PaxSeatTO> paxSeatToRemove)
			throws ModuleException {
		// to add
		Map<Integer, String> fltStIdTypeMap = new HashMap<Integer, String>();
		Collection<PaxSeatTO> toAdd = paxSeatToAdd.values();
		for (PaxSeatTO paxSeatTO : toAdd) {
			fltStIdTypeMap.put(paxSeatTO.getSelectedFlightSeatId(), paxSeatTO.getPaxType());
		}

		// to remove
		Collection<Integer> flightAMSeatIds = new ArrayList<Integer>();
		if (paxSeatToRemove != null) {

			Collection<PaxSeatTO> toRem = paxSeatToRemove.values();
			for (PaxSeatTO paxSeatTO : toRem) {
				flightAMSeatIds.add(paxSeatTO.getSelectedFlightSeatId());
			}

		}
		ReservationModuleUtils.getSeatMapBD().modifySeat(fltStIdTypeMap, flightAMSeatIds);
	}

	/**
	 * Populate seat information
	 * 
	 * @param res
	 * @param pnrSegId
	 * @throws ModuleException
	 */
	public static void populateSeatInformation(Reservation res, Integer pnrSegId) throws ModuleException {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		pnrSegIds.add(pnrSegId);

		Collection<Integer> pnrPaxIds = res.getPnrPaxIds();
		Collection<PaxSeatTO> paxSeatDTOs = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO.getReservationSeats(pnrPaxIds,
				pnrSegIds);
		res.setSeats(paxSeatDTOs);
	}

	/**
	 * Populate seat information
	 * 
	 * @param res
	 * @param pnrSegId
	 * @throws ModuleException
	 */
	public static Integer getReservationSeatTemplate(int flightSegId) throws ModuleException {

		Integer templateId = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO.getTemplateForSegmentId(flightSegId);

		return templateId;

	}

	/**
	 * Method will cancell all reserved seats from existing flight segment. if the existing passenger's seat code
	 * available in the transferring segment then same seat no will be allocated for the pax, else seat charge and
	 * seatcode will be removed . : JIRA AARESAA-1352
	 * 
	 * @param res
	 *            existing reservation
	 * @param flightSegId
	 *            target flight segment id.
	 * @param chgTnxGen
	 * 
	 * @return : The canceled pax seats during flight reprotection.
	 * 
	 * @throws ModuleException
	 */
	public static Collection<PaxSeatTO> cancellUpdateSeatsWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res,
			Integer pnrSegId, CredentialsDTO credentialsDTO, Integer flightSegId, TrackInfoDTO trackInfo,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// hold the existing res seats
		Collection<PaxSeatTO> paxSeatDTOs = res.getSeats();
		// hold the seats to cancel
		Collection<PaxSeatTO> affectivePaxSeatTOs = new ArrayList<PaxSeatTO>();

		Collection<PaxSeatTO> toReservedPaxSeatTOs = new ArrayList<PaxSeatTO>();

		Collection<PaxSeatTO> toCancelPaxSeatTOs = new ArrayList<PaxSeatTO>();

		Collection<SeatDTO> colSeats = new ArrayList<SeatDTO>();

		/* Hold reservation seatCodes */
		Collection<String> seatCodeCollection = new ArrayList<String>();

		// if no seats do nothing
		if (paxSeatDTOs == null || paxSeatDTOs.size() == 0) {
			return toCancelPaxSeatTOs;
		}
		// populate the seats to cancel
		for (PaxSeatTO paxSeatTO : paxSeatDTOs) {
			if (paxSeatTO.getPnrSegId().equals(pnrSegId)) {
				affectivePaxSeatTOs.add(paxSeatTO);
			}
		}

		// if no effective seats to cancel not found, do nothing
		if (affectivePaxSeatTOs == null || affectivePaxSeatTOs.size() == 0) {
			return toCancelPaxSeatTOs;
		}

		Collection<Integer> flightSeatMapIds = new ArrayList<Integer>();

		for (PaxSeatTO paxSeatTO : affectivePaxSeatTOs) {
			flightSeatMapIds.add(paxSeatTO.getSelectedFlightSeatId());
		}

		for (Iterator<PaxSeatTO> seatCodeIterator = affectivePaxSeatTOs.iterator(); seatCodeIterator.hasNext();) {
			PaxSeatTO paxSeatDTO = seatCodeIterator.next();
			seatCodeCollection.add(paxSeatDTO.getSeatCode());
		}

		// if no effective seats code to cancel - since need to reprotect the corresponding seat codes : AARESAA-1352
		if (seatCodeCollection == null || seatCodeCollection.size() == 0) {
			return toCancelPaxSeatTOs;
		}

		if (log.isDebugEnabled()) {
			log.debug("EXISTING SEAT CODES : " + seatCodeCollection.toString());
		}

		PaxSeatTO sourcePaxSeatTO;
		PaxSeatTO tempPaxSeatTO;

		SeatMapJDBCDAO seatMapJDBCDAO = AirInventoryModuleUtils.getSeatMapJDBCDAO();
		FlightSeatsDTO flightSeatsDTO = seatMapJDBCDAO.getFlightSeatsWithCodes(flightSegId.intValue(), seatCodeCollection);
		if (flightSeatsDTO != null) {

			colSeats = flightSeatsDTO.getSeats(); // seats collection in target flight match with existing seat codes.
		}

		boolean flag = false;

		for (Iterator<PaxSeatTO> iterator = affectivePaxSeatTOs.iterator(); iterator.hasNext();) {
			flag = false;
			sourcePaxSeatTO = iterator.next();
			String sSeatCode = sourcePaxSeatTO.getSeatCode();

			for (SeatDTO seatDTO : colSeats) {
				String tSeatCode = seatDTO.getSeatCode();
				if (!tSeatCode.equals("") && seatDTO.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& tSeatCode.equals(sSeatCode)) {
					tempPaxSeatTO = new PaxSeatTO();
					tempPaxSeatTO = sourcePaxSeatTO;
					tempPaxSeatTO.setSelectedFlightSeatId(seatDTO.getFlightAmSeatID());
					tempPaxSeatTO.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
					toReservedPaxSeatTOs.add(tempPaxSeatTO);
					flag = true;
					break;

				}
			}

			if (!flag) {
				toCancelPaxSeatTOs.add(sourcePaxSeatTO);
			}

		}

		PassengerSeatingAssembler passengerSeatingAssembler = new PassengerSeatingAssembler(null, false);
		if (toCancelPaxSeatTOs != null && toCancelPaxSeatTOs.size() > 0) {
			// add the removing seats one by one to the assembler
			for (PaxSeatTO paxSeatTO : toCancelPaxSeatTOs) {
				passengerSeatingAssembler.removePassengerFromFlightSeat(paxSeatTO.getPnrPaxId(), paxSeatTO.getPnrPaxFareId(),
						paxSeatTO.getPnrSegId(), paxSeatTO.getSelectedFlightSeatId(), paxSeatTO.getChgDTO().getAmount(),
						trackInfo);
			}
			// refund the charges
			addPassengerSeatChargeAndAdjustCredit(adjustCreditBO, res, passengerSeatingAssembler.getPaxSeatToRemove(), " ", null,
					credentialsDTO, chgTnxGen);
		}

		if (toReservedPaxSeatTOs != null && toReservedPaxSeatTOs.size() > 0) {
			// Collection<Integer> toResflightSeatMapIds = new ArrayList<Integer>();
			for (Iterator<PaxSeatTO> iter = toReservedPaxSeatTOs.iterator(); iter.hasNext();) {
				PaxSeatTO paxSeat = (PaxSeatTO) iter.next();
				passengerSeatingAssembler.addPassengertoNewFlightSeat(paxSeat.getPnrPaxId(), paxSeat.getPnrPaxFareId(),
						paxSeat.getPnrSegId(), paxSeat.getSelectedFlightSeatId(), paxSeat.getChgDTO().getAmount(),
						paxSeat.getSeatCode(), paxSeat.getPaxType(), paxSeat.getAutoCancellationId(), trackInfo);

			}
			// reserve vacant seats in target seatmap.
			modifySeats(passengerSeatingAssembler.getPaxSeatToAdd(), null);
			// save passenger- seat info
			SeatMapAdapter.savePaxSeatingInfo(toReservedPaxSeatTOs, credentialsDTO);
		}

		Iterator<PaxSeatTO> canceledSeats = toCancelPaxSeatTOs.iterator();
		while (canceledSeats.hasNext()) {
			PaxSeatTO canceledSeat = canceledSeats.next();
			if (passengerSeatingAssembler.getPaxSeatToAdd() != null && !passengerSeatingAssembler.getPaxSeatToAdd().isEmpty()) {
				Collection<PaxSeatTO> reprotectingSeats = passengerSeatingAssembler.getPaxSeatToAdd().values();
				Iterator<PaxSeatTO> reprotectingSeatIt = reprotectingSeats.iterator();
				while (reprotectingSeatIt.hasNext()) {
					PaxSeatTO reprotectedSeat = reprotectingSeatIt.next();
					if (reprotectedSeat.getPnrPaxId().equals(canceledSeat.getPnrPaxId())
							&& reprotectedSeat.getPnrSegId().equals(canceledSeat.getPnrSegId())
							&& reprotectedSeat.getSeatCode().equals(canceledSeat.getSeatCode())) {
						canceledSeat.setReprotectedToAnotherSeat(true);
					}
				}
			}
		}

		if (toCancelPaxSeatTOs.size() > 0) {
			List<String> countedPaxInSeg = new ArrayList<String>();
			for (PaxSeatTO paxSeatTO : toCancelPaxSeatTOs) {
				if (!paxSeatTO.isReprotectedToAnotherSeat()) {
					String paxSegUniqId = SMUtil.makeUniqueIdForModifications(paxSeatTO.getPnrPaxId(), paxSeatTO.getPnrPaxId());
					if (!countedPaxInSeg.contains(paxSegUniqId)) {
						countedPaxInSeg.add(paxSegUniqId);
						removePassengerSeatPromotionAndAdjustCredit(paxSeatTO, adjustCreditBO, res, credentialsDTO, chgTnxGen);
					}
				}
			}
		}

		// Save the reservation
		ReservationProxy.saveReservation(res);

		// release the seats
		ReservationModuleUtils.getSeatMapBD().vacateSeats(flightSeatMapIds);

		// update the passenger seats table
		updateSeatsToCancel(flightSeatMapIds);

		return toCancelPaxSeatTOs;
	}

	/**
	 * If canceling seat is not reprotected and next seat promotion granted, refund the non-refundable charge
	 * 
	 * @param canceledSeat
	 *            ( this seat should be cancled + not-reprotected )
	 * @param adjustCreditBO
	 * @param reservation
	 * @param paxSeatToRemove
	 * @param string
	 * @param object
	 * @param credentialsDTO
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	private static void removePassengerSeatPromotionAndAdjustCredit(PaxSeatTO canceledSeat, AdjustCreditBO adjustCreditBO,
			Reservation reservation, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		Integer pnrPaxId = canceledSeat.getPnrPaxId();
		Integer resSegId = canceledSeat.getPnrSegId();

		ReservationPaxOndCharge paxOndCharge = null;
		Double chargeToRefund = null;
		Integer chargeRateId = null;
		int requestedSeats = 1;

		for (ReservationPax pax : reservation.getPassengers()) {
			if (pax.getPnrPaxId().equals(pnrPaxId)) {
				for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
					for (ReservationPaxFareSegment paxFareSegment : paxFare.getPaxFareSegments()) {
						boolean paxSegRefunded = false; // only refund to single pax, seg once
						if (paxFareSegment.getSegment().getPnrSegId().equals(resSegId) && !paxSegRefunded) {

							PromotionRequest promotionRequest = ReservationModuleUtils.getPromotionManagementBD()
									.getApprovedPromotions(PromotionType.PROMOTION_NEXT_SEAT_FREE, pnrPaxId, resSegId);

							if (promotionRequest != null) {

								for (PromotionRequestConfig promoRequestConfig : promotionRequest.getPromotionRequestConfigs()) {
									if (promoRequestConfig.getPromoReqParam().equals(PromoRequestParam.FreeSeat.SEATS_TO_FREE)) {
										requestedSeats = Integer.parseInt(promoRequestConfig.getParamValue());
										break;
									}
								}

								for (OndPromotion ondPromotion : promotionRequest.getPromotionTemplate().getOnds()) {
									for (OndPromotionCharge ondPromoCharge : ondPromotion.getOndPromotionCharges()) {
										if (ondPromoCharge.getChargeCode()
												.equals(PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG
														.getFreeSeatChargeParam(requestedSeats))) {
											chargeToRefund = ondPromoCharge.getAmount();
											chargeRateId = ondPromoCharge.getChargeRate();
											break;
										}
									}
								}

								BigDecimal amount = AccelAeroCalculator.parseBigDecimal(chargeToRefund).negate();
								promotionRequest
										.setStatus(PromotionsInternalConstants.PromotionRequestStatus.REJECTED.getStatusCode());
								promotionRequest.setModifiedBy(credentialsDTO.getUserId());
								promotionRequest.setModifiedDate(new Date());
								ReservationModuleUtils.getPromotionManagementBD().savePromotionRequest(promotionRequest);

								String paxName = BeanUtils.makeFirstLetterCapital(pax.getFirstName()) + " "
										+ BeanUtils.makeFirstLetterCapital(pax.getLastName());
								String userNotes = "Next seat promotion request rejected, seat charge refunded to " + paxName
										+ ", amount: " + amount.negate() + " " + AppSysParamsUtil.getBaseCurrency();

								String chargeGroupCode = ReservationInternalConstants.ChargeGroup.ADJ;
								paxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(amount, null, chargeRateId,
										chargeGroupCode, paxFare, credentialsDTO, false, null, null, null,
										chgTnxGen.getTnxSequence(pax.getPnrPaxId()));

								adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, paxOndCharge,
										credentialsDTO, reservation.isEnableTransactionGranularity());
								paxSegRefunded = true;
							}
						}
					}
				}
				break;
			}
		}
	}

	/**
	 * If the existing passenger's seat code available in the transferring segment then same seat no will be allocated
	 * for the pax
	 * 
	 * @param res
	 *            existing reservation
	 * @param flightSegId
	 *            target flight segment id.
	 * 
	 * @return : The canceled pax seats during flight reprotection.
	 * 
	 * @throws ModuleException
	 */
	public static void updateSeatsWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res, Integer pnrSegId,
			CredentialsDTO credentialsDTO, Integer flightSegId, TrackInfoDTO trackInfo) throws ModuleException {
		// hold the existing res seats
		Collection<PaxSeatTO> paxSeatDTOs = res.getSeats();
		// hold the seats to cancel
		Collection<PaxSeatTO> affectivePaxSeatTOs = new ArrayList<PaxSeatTO>();

		Collection<PaxSeatTO> toReservedPaxSeatTOs = new ArrayList<PaxSeatTO>();

		Collection<SeatDTO> colSeats = new ArrayList<SeatDTO>();

		/* Hold reservation seatCodes */
		Collection<String> seatCodeCollection = new ArrayList<String>();

		for (PaxSeatTO paxSeatTO : paxSeatDTOs) {
			if (paxSeatTO.getPnrSegId().equals(pnrSegId)) {
				affectivePaxSeatTOs.add(paxSeatTO);
			}
		}

		for (Iterator<PaxSeatTO> seatCodeIterator = affectivePaxSeatTOs.iterator(); seatCodeIterator.hasNext();) {
			PaxSeatTO paxSeatDTO = (PaxSeatTO) seatCodeIterator.next();
			seatCodeCollection.add(paxSeatDTO.getSeatCode());
		}

		if (log.isDebugEnabled()) {
			log.debug("EXISTING SEAT CODES : " + seatCodeCollection.toString());
		}

		String sSeatCode = "";
		String tSeatCode = "";
		PaxSeatTO sourcePaxSeatTO;
		PaxSeatTO tempPaxSeatTO;

		SeatMapJDBCDAO seatMapJDBCDAO = AirInventoryModuleUtils.getSeatMapJDBCDAO();
		FlightSeatsDTO flightSeatsDTO = seatMapJDBCDAO.getFlightSeatsWithCodes(flightSegId.intValue(), seatCodeCollection);
		if (flightSeatsDTO != null) {

			colSeats = flightSeatsDTO.getSeats(); // seats collection in target flight match with existing seat codes.
		}

		for (Iterator<PaxSeatTO> iterator = affectivePaxSeatTOs.iterator(); iterator.hasNext();) {
			sSeatCode = "";
			sourcePaxSeatTO = (PaxSeatTO) iterator.next();
			sSeatCode = sourcePaxSeatTO.getSeatCode();

			for (Iterator<SeatDTO> seatIterator = colSeats.iterator(); seatIterator.hasNext();) {
				tSeatCode = "";
				SeatDTO seatDTO = (SeatDTO) seatIterator.next();
				tSeatCode = seatDTO.getSeatCode();
				if (!tSeatCode.equals("") && seatDTO.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& tSeatCode.equals(sSeatCode)) {
					tempPaxSeatTO = new PaxSeatTO();
					tempPaxSeatTO = sourcePaxSeatTO;
					tempPaxSeatTO.setSelectedFlightSeatId(seatDTO.getFlightAmSeatID());
					tempPaxSeatTO.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
					toReservedPaxSeatTOs.add(tempPaxSeatTO);
					break;
				}
			}
		}

		PassengerSeatingAssembler p = new PassengerSeatingAssembler(null, false);
		// Save the reservation
		ReservationProxy.saveReservation(res);

		if (toReservedPaxSeatTOs != null && toReservedPaxSeatTOs.size() > 0) {
			for (Iterator<PaxSeatTO> iter = toReservedPaxSeatTOs.iterator(); iter.hasNext();) {
				PaxSeatTO paxSeat = (PaxSeatTO) iter.next();
				p.addPassengertoNewFlightSeat(paxSeat.getPnrPaxId(), paxSeat.getPnrPaxFareId(), paxSeat.getPnrSegId(),
						paxSeat.getSelectedFlightSeatId(), paxSeat.getChgDTO().getAmount(), paxSeat.getSeatCode(),
						paxSeat.getPaxType(), paxSeat.getAutoCancellationId(), trackInfo);

			}
			// reserve vacant seats in target seatmap.
			modifySeats(p.getPaxSeatToAdd(), null);
			// save passenger- seat info
			SeatMapAdapter.savePaxSeatingInfo(toReservedPaxSeatTOs, credentialsDTO);
		}

	}

	/**
	 * Cancels seats when transfering segments
	 * 
	 * @param res
	 * @param chgTnxGen
	 * @return : The canceled pax seats during re-protection.
	 * 
	 * @throws ModuleException
	 */
	// interesrted
	public static Collection<PaxSeatTO> cancellSeatsWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res,
			Integer pnrSegId, CredentialsDTO credentialsDTO, TrackInfoDTO trackInfo, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		// hold the existing res seats
		Collection<PaxSeatTO> paxSeatDTOs = res.getSeats();
		// hold the seats to cancel
		Collection<PaxSeatTO> affectivePaxSeatTOs = new ArrayList<PaxSeatTO>();

		// if no seats do nothing
		if (paxSeatDTOs == null || paxSeatDTOs.size() == 0) {
			return affectivePaxSeatTOs;
		}
		// populate the seats to cancel
		for (PaxSeatTO paxSeatTO : paxSeatDTOs) {
			if (paxSeatTO.getPnrSegId().equals(pnrSegId)) {
				affectivePaxSeatTOs.add(paxSeatTO);
			}
		}

		// if no effective seats to cancel not found, do nothing
		if (affectivePaxSeatTOs == null || affectivePaxSeatTOs.size() == 0) {
			return affectivePaxSeatTOs;
		}

		// use the assembler to gather information on seats to remove
		PassengerSeatingAssembler p = new PassengerSeatingAssembler(null, false);

		// add the removing seats one by one to the assembler
		Collection<Integer> flightSeatMapIds = new ArrayList<Integer>();

		for (PaxSeatTO paxSeatTO : affectivePaxSeatTOs) {
			p.removePassengerFromFlightSeat(paxSeatTO.getPnrPaxId(), paxSeatTO.getPnrPaxFareId(), paxSeatTO.getPnrSegId(),
					paxSeatTO.getSelectedFlightSeatId(), paxSeatTO.getChgDTO().getAmount(), trackInfo);
			flightSeatMapIds.add(paxSeatTO.getSelectedFlightSeatId());
		}
		// refund the charges
		addPassengerSeatChargeAndAdjustCredit(adjustCreditBO, res, p.getPaxSeatToRemove(), " ", null, credentialsDTO, chgTnxGen);

		Iterator<PaxSeatTO> canceledSeats = affectivePaxSeatTOs.iterator();
		List<String> countedPaxInSeg = new ArrayList<String>();
		while (canceledSeats.hasNext()) {
			PaxSeatTO canceledSeat = canceledSeats.next();
			String paxSegUniqId = SMUtil.makeUniqueIdForModifications(canceledSeat.getPnrPaxId(), canceledSeat.getPnrPaxId());
			if (!countedPaxInSeg.contains(paxSegUniqId)) {
				countedPaxInSeg.add(paxSegUniqId);
				removePassengerSeatPromotionAndAdjustCredit(canceledSeat, adjustCreditBO, res, credentialsDTO, chgTnxGen);
			}
		}

		// Save the reservation
		ReservationProxy.saveReservation(res);

		// release the seats
		ReservationModuleUtils.getSeatMapBD().vacateSeats(flightSeatMapIds);

		// update the passenger seats table
		updateSeatsToCancel(flightSeatMapIds);

		return affectivePaxSeatTOs;
	}

	/**
	 * Called when modifying seats only
	 * 
	 * @param reservation
	 * @param pnrPaxFareId
	 * @param passengerSeatMap
	 * @param userNotes
	 * @param trackInfoDTO
	 * @param credentialsDTO
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	public static void addPassengerSeatChargeAndAdjustCredit(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Map<String, PaxSeatTO> passengerSeatMap, String userNotes, TrackInfoDTO trackInfoDTO, CredentialsDTO credentialsDTO,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// Adding charge entry
		Map<String, ReservationPaxOndCharge> paxIdChargeMap = addSeatMapCharge(reservation, passengerSeatMap, credentialsDTO,
				chgTnxGen);

		// crediting for previous seat charege

		for (String uniqueId : paxIdChargeMap.keySet()) {
			Integer pnrPaxId = Integer.parseInt(SMUtil.getPnrPaxId(uniqueId));
			ReservationPaxOndCharge paxOndCharge = paxIdChargeMap.get(uniqueId);
			if (paxOndCharge.getEffectiveAmount().compareTo(BigDecimal.ZERO) != 0) { // skipping zero charge seats
				adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, paxOndCharge, credentialsDTO,
						reservation.isEnableTransactionGranularity());
			}
		}
	}

	/**
	 * 
	 * @param reservation
	 * @param passengerSeatChargeMap
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<String, ReservationPaxOndCharge> addSeatMapCharge(Reservation reservation,
			Map<String, PaxSeatTO> passengerSeatChargeMap, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		Map<String, ReservationPaxOndCharge> perPaxOndCharge = new HashMap<String, ReservationPaxOndCharge>();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			Collection<Integer> consumedPaxOndChgIds = new ArrayList<Integer>();
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				for (ReservationPaxFareSegment resPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
					if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL
							.equals(resPaxFareSegment.getSegment().getStatus())) {
						continue;
					}

					String uniqueId = SMUtil.makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
							reservationPaxFare.getPnrPaxFareId(), resPaxFareSegment.getPnrSegId());
					Collection<PaxSeatTO> paxSeatTOs = new ArrayList<PaxSeatTO>();
					for (String key : passengerSeatChargeMap.keySet()) {
						if (key.contains(uniqueId)) {
							paxSeatTOs.add(passengerSeatChargeMap.get(key));
						}
					}

					for (PaxSeatTO paxSeatTO : paxSeatTOs) {
						paxSeatTO.setPnrPaxId(reservationPax.getPnrPaxId());
						paxSeatTO.setPaxType(SMUtil.getPaxTypeCode(reservationPax));
						ExternalChgDTO externalChgDTO = paxSeatTO.getChgDTO();
						PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getAppliedDiscountInfoForRefund(reservationPaxFare,
								externalChgDTO, consumedPaxOndChgIds);
						ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(
								externalChgDTO.getAmount(), null, externalChgDTO.getChgRateId(), externalChgDTO.getChgGrpCode(),
								reservationPaxFare, credentialsDTO, false, paxDiscInfo, null, null,
								chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

						// rebuild unique id with flight seat id
						uniqueId = SMUtil.makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
								reservationPaxFare.getPnrPaxFareId(), resPaxFareSegment.getPnrSegId(),
								paxSeatTO.getSelectedFlightSeatId());

						perPaxOndCharge.put(uniqueId, reservationPaxOndCharge);
					}
				}
			}
		}

		if (perPaxOndCharge.size() == 0) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		return perPaxOndCharge;
	}

	/**
	 * Update Seats to cancel
	 * 
	 * @param flightAMSeatIds
	 */
	private static void updateSeatsToCancel(Collection<Integer> flightAMSeatIds) throws ModuleException {
		SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;
		Collection<PassengerSeating> passengerSeatings = seatMapDAO.getReservedFlightSeats(flightAMSeatIds);

		if (passengerSeatings != null && passengerSeatings.size() > 0) {
			for (PassengerSeating passengerSeating : passengerSeatings) {
				passengerSeating.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.PAX_CNX);
				passengerSeating.setTimestamp(new Date());
			}
			seatMapDAO.saveOrUpdate(passengerSeatings);
		} else {
			throw new ModuleException("airreservation.seat.inconsistent");
		}
	}

	/**
	 * called when creting new booking and modifying only
	 * 
	 * @param paxSeatTOs
	 * @param credentialsDTO
	 */
	public static void savePaxSeatingInfo(Collection<PaxSeatTO> paxSeatTOs, CredentialsDTO credentialsDTO) {
		Collection<PassengerSeating> passengerSeatings = new ArrayList<PassengerSeating>();

		for (PaxSeatTO paxSeatTO : paxSeatTOs) {
			PassengerSeating passengerSeat = new PassengerSeating();
			passengerSeat.setPnrPaxId(paxSeatTO.getPnrPaxId());
			passengerSeat.setChargeAmount(paxSeatTO.getChgDTO().getAmount());
			passengerSeat.setFlightSeatId(paxSeatTO.getSelectedFlightSeatId());
			passengerSeat.setStatus(paxSeatTO.getStatus());
			passengerSeat.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
			passengerSeat.setUserId(credentialsDTO.getUserId());
			passengerSeat.setPpfId(paxSeatTO.getPnrPaxFareId());
			passengerSeat.setPnrSegId(paxSeatTO.getPnrSegId());
			passengerSeat.setTimestamp(CalendarUtil.getCurrentSystemTimeInZulu());
			passengerSeat.setCustomerId(credentialsDTO.getCustomerId());
			passengerSeat.setAutoCancellationId(paxSeatTO.getAutoCancellationId());
			passengerSeatings.add(passengerSeat);
		}

		if (!passengerSeatings.isEmpty()) {
			ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO.saveOrUpdate(passengerSeatings);
		}
	}

}
