/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @version $Id$
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLDestinationDTO;
import com.isa.thinair.airreservation.api.dto.PassengerCheckinDTO;
import com.isa.thinair.airreservation.api.dto.PnlAdlDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.autocheckin.CheckinPassengersInfoDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.cesar.FlightLegPaxTypeCountTO;
import com.isa.thinair.airreservation.api.dto.eticket.PaxEticketTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.FlightLoadInfoDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestRecordDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.model.AirportPassengerMessageTxHsitory;
import com.isa.thinair.airreservation.api.model.FlightLoad;
import com.isa.thinair.airreservation.api.model.PalCalHistory;
import com.isa.thinair.airreservation.api.model.PnlAdlHistory;
import com.isa.thinair.airreservation.api.model.PnlPassenger;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlDataContext;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * ReservationAuxilliaryDAO
 * 
 * @author
 * 
 */
public interface ReservationAuxilliaryDAO {

	public Date getFlightLocalDate(int flightId, String depatureAirport) throws ModuleException;

	public Date getFlightZuluDate(int flightId, String depatureAirport) throws ModuleException;

	public Collection<AirportPassengerMessageTxHsitory> getPnlAdlHistory() throws ModuleException;

	public Collection<PNLADLDestinationDTO> getPNL(int flightId, String departureAirportCode, String currentCarrierCode);
	
	public void updatePnlPassegerStatusToSent(Collection<Integer> pnlPaxIds);

	public Collection<PNLADLDestinationDTO> getADL(int flightId, String departureAirportCode, Timestamp scheduledTimestamp,
			String currentCarrierCode);

	public List<PassengerInformation> getPNLPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimestamp, String carrierCode);
	
	public List<PassengerInformation> getADLPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimestamp, String carrierCode);

	public List<PassengerInformation> getDeletedADLPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimestamp, String carrierCode);

	public List<PassengerInformation> getPALPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimestamp, String carrierCode);

	public List<PassengerInformation> getCALPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimeStamp, String carrierCode);

	public List<PassengerInformation> getDeletedCALPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimestamp, String carrierCode);

	public Collection<AirportPassengerMessageTxHsitory> getPnlAdlHistory(String flightId, Date date, String airport);

	public Map<String, List<String>> getActiveSITAAddresses(String airportCode, String flightNumber, String ond, String carrierCode, DCS_PAX_MSG_GROUP_TYPE msgType);

	public int getFlightID(String flightNumber, String departureStation, Date dateOfflight);
	
	public PnlDataContext getFlightInfo(String flightNumber, String departureStation, Date dateOfflight);

	public String getFlightNumber(int flightID);
	
	public Map<String, String> getFlightLegs(int flightID);

	public InboundConnectionDTO getInBoundConnectionList(String pnr, int id, String departureStation, int flightId);

	public InboundConnectionDTO getInBoundConnectionList(String pnr, boolean confirmedReservationSegment,
			String departureStation, int flightId);

	/** called in PNL BL **/
	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int id, String departureStation, int flightId);

	/** called in ADL BL **/
	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int paxId, String departureStation, int flightId,
			boolean confirmedReservationSegment);

	/** called by manifest retriving query and called by the above two methods **/
	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int paxId, String departureStation, int flightId,
			boolean confirmedReservationSegment, String maximumConnectionTime);

	public Collection<FlightManifestRecordDTO> getFlightManifest(FlightManifestOptionsDTO manifestOptions);

	public String getFlightPassengerSummary(FlightManifestOptionsDTO manifestOptions);

	public String getSavedPnlAdl(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException;
	
	public String getSavedPalCal(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException;

	/**
	 * Returns a Collection of Failed PNL's Or Adls that fall within N number of minutes
	 * 
	 * @param numOfMinutes
	 * @param msgType TODO
	 * @return
	 * @throws ModuleException
	 */
	public HashMap<Integer, PnlAdlDTO> getFailuresWithin(int numOfMinutes, DCS_PAX_MSG_GROUP_TYPE msgType);

	public int getNumberOfFailedAttempts(Integer flightId, String sitaAddress, String msgType);

	public int getNumberOfSuccessAtempts(Integer flightId, String sitaAddress, String msgType, Timestamp timestamp);

	public Map<Integer, String> getPnrPaxFareSegIds(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds);

	public boolean hasPnlHistory(String flightNumber, Date flightDepZulu, String airportCode);

	public boolean hasAnyPnlHistory(String flightNumber, Date flightDepZulu, String airportCode);

	/**
	 * For Saving To PNLADL History Using Hibernate
	 * 
	 * @param pnlAdlHistory
	 */
	public void savePnlAdlHistory(PnlAdlHistory pnlAdlHistory);

	/**
	 * 
	 * @param flightId
	 * @param airportCode
	 * @return
	 */
	public Object[] getPaxCountInPnlADL(Integer flightId, String airportCode);

	/**
	 * 
	 * @param flightId
	 * @return
	 */
	public Collection<String> getClassOfServicesForFlight(int flightId);

	public void savePnlPassengers(Collection<PnlPassenger> pnlPassengers);

	public List<PnlPassenger> getPnlPassegers(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds, String status);

	public List<PnlPassenger> getPnlPassegers(Integer pnrSegID, String status, boolean excludeTBAPax);

	public List<PnlPassenger> getPnlPassegers(Set<String> pnrs, List<String> adlActions, String status);

	public List<PnlPassenger> getPnlPassegers(Collection<Integer> pnrSegIds);

	public List<PnlPassenger> getPnlPassegersFromPnlPaxIds(Collection<Integer> pnlPaxIds);

	public FlightLegPaxTypeCountTO getFlightLegPaxTypeCount(String flightNumber, String origin, String destination,
			Date departureDateZulu);

	public Map<String, List<Integer>> getInconsitentInsurances(Date startingFrom);

	public Map<Integer, Date[]> getFlightSegmentDepTimeZuluMap(Collection<Integer> pnrSegIds);

	public Map<Integer, Date[]> getFlightSegmentDepTimeZuluMapForAddSegment(Collection<Integer> fltSegIds);

	public Map<Integer, String> getPnrSegmentCodes(Collection<Integer> pnrSegmentIds);

	public Collection<PassengerCheckinDTO> getCheckinPassengers(int flightId, String airportCode, String checkinStatus,
			boolean standBy, boolean goShow);

	public void updatePaxCheckinStatus(ArrayList<Integer> pnrPaxFareSegId, String checkinStatus);

	public String loadAirportMessagesTranslation(Integer airportMsgId, String lang);

	public ArrayList<MealNotificationDetailsDTO> getFlightsForMealNotificaitionScheduling(int pnldepartureGap, Date date,
			List<String> hubs);

	public Collection<String> getMealNotificationTimings();

	public Collection<MealNotificationDetailsDTO> getMealNotificationDetails(boolean isLoadFlightLevel);
	
	public List<FlightLoad> getFlightLegLoadInfoScheduledForSending();

	public void saveFlightLoad(Collection<FlightLoad> flightLoads);

	public Collection<String> getAllflightSegments(int fid);

	public boolean hasFlownSegments(Collection<Integer> pnrSegIds);

	public Map<Integer, Collection<Integer>> getPaxWiseFlownSegments(Collection<Integer> pnrSegIds);

	public Map<Integer, String> getFltSegWiseBkgClasses(Collection<Integer> pnrSegIds);

	public Map<Integer, String> getFltSegWiseCabinClasses(Collection<Integer> pnrSegIds);

	public Map<Integer, Date> getFltSegWiseLastFQDates(List<Integer> flownPnrSegIds);

	public List<PaxEticketTO> getPaxEtickets(int flightId, String airportCode);
	
	public List<PnlPassenger> getSegmentTransferredPnlPassegers(Set<String> pnrs, List<String> adlActions,
			Collection<Integer> pnrSegIds);

	public List<PnlPassenger> getNonSegmentTransferredPnlPassegers(Set<String> pnrs, List<String> adlActions,
			Collection<Integer> pnrSegIds);
	
	public boolean hasPalSentHistory(String flightNumber, Date flightDepZulu, String airportCode);

	public void savePALCALTXHistory(PalCalHistory palCalHistory);
	
	public Collection<AirportPassengerMessageTxHsitory> getPalCalHistory(String flightNumber, Date date, String airport);
	
	public List<AirportMessagePassenger> getAirportMessagePassengers(Integer pnrPaxId, Integer pnrSegId, String status);
	
	public List<AirportMessagePassenger> getAirportMessagePassengers(Integer pnrPaxId, Integer pnrSegId);
	
	public List<AirportMessagePassenger> getAirportMessagePassengers(String pnr);
	
	public List<AirportMessagePassenger> getAirportMessagePassengersFromPnrSegIds(Collection<Integer> pnrSegIds);
	
	public List<AirportMessagePassenger> getAirportMessagePassengers(Collection<Integer> airportMsgPaxIds);
	
	public void saveAirportMessagePassenger(Collection<AirportMessagePassenger> paxlist);

	public boolean hasAnyPALCALHistory(String flightNumber, Date flightDepZulu, String airportCode, boolean isSchdeularSentMsgHistoryOnly);
	
	public Object[] getPaxCountInPALCAL(Integer flightId, String airportCode);
	
	public Map<String, LinkedHashMap<String, String>> getFlightSeatConfigDetails(Integer flightId);
	
	public Map<Integer, Collection<Integer>> getPaxWiseBoardedCheckedInSegments(Collection<Integer> pnrSegIds);

	public Collection<FlightLoadInfoDTO> getFlightLoadInfo(Date departureDateFromZulu, Date departureDateToZulu);


	public String loadI18AirportMessagesTranslation(String messageKey, String selectedLang);
	

	void updateAutomaticCheckin(Collection<Integer> pnrPaxIds, Integer flightId, Integer noOfAttempts, String checkinStatus,
			String dcsResponseText);

	public Collection<CheckinPassengersInfoDTO> getPassengerDetailsforCheckin(Integer flightId, Integer pnrPaxId)
			throws ModuleException;

	public PaxAutomaticCheckinTO getAutoCheckinPassengerDetails(Date departureDate, String flightNumber, String title,
			String firstName, String lastName, String pnr, String seatCode, String seatSelectionStatus);

	public List<ReservationPax> getReservationPaxSegAutoCheckin(Date departureDate, String flightNumber, String title,
			String firstName, String lastName, String pnr, String seatCode, String seatSelectionStatus);

	public Collection<PaxAutomaticCheckinTO> getPaxWiseAutoCheckinInfo(Integer flightId);

	public Collection<FlightSeatStatusTO> getFlightAvailableSeatMap(int flightId, int blockedSeatRows);

	public void updatePaxAutoCheckinSeats(LinkedHashMap<Integer, Integer> blockedSeatIds) throws ModuleException;

	public void updateCreiditNoteforPassenger(Integer flightId) throws ModuleException;

	public String getAutoCheckinSeatSelectionStatus(Date departureDate, String flightNumber, String title, String firstName,
			String lastName, String pnr, String checkInSeat);

	public Collection<CheckinPassengersInfoDTO> getInfantPassengerDetailsforCheckin(Integer flightId, Integer pnrPaxId)
			throws ModuleException;

	public Integer getInfantPpsacId(Integer ppsacId);

}
