package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;

public interface FlexiRuleDAO {

	public void saveOrUpdate(Collection<ReservationPaxOndFlexibility> paxOndFlexibilities);

	public Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> getReservationPaxOndFlexibilities(
			Collection<Integer> collPpfIds);

	public Collection<ReservationPaxOndFlexibility> getPaxOndFlexibilities(int ppfId);
}