package com.isa.thinair.airreservation.core.bl.tty;

import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SsrTicketingTimeLimitDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ExtendOnholdMessageCreator extends TypeBReservationMessageCreator {
	
	public ExtendOnholdMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}

	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {
		SsrTicketingTimeLimitDTO ssrTicketingTimeLimitDTO = new SsrTicketingTimeLimitDTO();
		if (typeBRequestDTO.getCsOCCarrierCode() != null) {
			ssrTicketingTimeLimitDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
		} else {
			ssrTicketingTimeLimitDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		}
		String station = null;
		String agentCode = reservation.getAdminInfo().getOriginAgentCode();
		if (agentCode != null && agentCode.length() > 3) {
			station = agentCode.substring(0, 3);
		}
		Date releaseTimeStamp = reservation.getReleaseTimeStamps()[0];
		ssrTicketingTimeLimitDTO.setSsrValue(GDSExternalCodes.ActionCode.SOLD.getCode() + "/" + station + " "
				+ CalendarUtil.getSegmentTime(releaseTimeStamp) + "/" + CalendarUtil.getSegmentDayMonth(releaseTimeStamp));
		ssrDTOs.add(ssrTicketingTimeLimitDTO);
		return ssrDTOs;
	}

}
