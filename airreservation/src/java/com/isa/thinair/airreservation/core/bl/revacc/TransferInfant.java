/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for purchase tickets
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="transferInfant"
 */
public class TransferInfant extends DefaultBaseCommand {

	// Dao's
	private ReservationTnxDAO reservationTnxDao;

	/**
	 * constructor of the TicketPurchase command
	 */
	public TransferInfant() {
		// looking up daos
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	/**
	 * execute method of the TicketPurchase command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		// getting command params

		String pnrPaxIdFrom = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		String pnrPaxIdTo = (String) this.getParameter(CommandParamNames.PNR_PAX_ID_TO);
		BigDecimal infantAmount = (BigDecimal) this.getParameter(CommandParamNames.AMOUNT);
		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = (ReservationPaxPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		boolean enableTransactionGranularity = (Boolean) this
				.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);

		// checking params
		this.checkParams(pnrPaxIdFrom, pnrPaxIdTo, infantAmount, credentialsDTO);

		Integer transactionSeq = retrieveCorrectTransactionSeq(pnrPaxIdTo);

		// credit the transfer amount of FROM-PAX
		ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxIdFrom, infantAmount,
				ReservationTnxNominalCode.INFANT_TRANSFER.getCode(), credentialsDTO,
				CalendarUtil.getCurrentSystemTimeInZulu(), null, null, null, false, true);
		reservationTnxDao.saveTransaction(creditTnx);

		TnxGranularityFactory.saveReservationPaxTnxBreakdownForRefundables(pnrPaxIdFrom, creditTnx.getNominalCode(),
				reservationPaxPaymentMetaTO, enableTransactionGranularity, transactionSeq);

		// get new FROM-PNR Pax balance
		BigDecimal pnrPaxBalance = reservationTnxDao.getPNRPaxBalance(pnrPaxIdFrom);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (pnrPaxBalance.doubleValue() < 0) {
			String ownCarrier = AppSysParamsUtil.getDefaultCarrierCode();
			if (pnrPaxBalance.negate().compareTo(infantAmount) >= 0) {
				PaxCreditDTO paxCreditDTO = new PaxCreditDTO(pnrPaxIdFrom, infantAmount, ownCarrier,
						new PayCurrencyDTO(AppSysParamsUtil.getBaseCurrency(), BigDecimal.ONE));
				response = (DefaultServiceResponse) ReservationModuleUtils.getCreditAccountBD()
						.carryForwardCredit(paxCreditDTO, credentialsDTO, enableTransactionGranularity);
			}

			// debit with pax balance amount
			else {
				PaxCreditDTO paxCreditDTO = new PaxCreditDTO(pnrPaxIdFrom, pnrPaxBalance.negate(), ownCarrier,
						new PayCurrencyDTO(AppSysParamsUtil.getBaseCurrency(), BigDecimal.ONE));
				response = (DefaultServiceResponse) ReservationModuleUtils.getCreditAccountBD()
						.carryForwardCredit(paxCreditDTO, credentialsDTO, enableTransactionGranularity);
			}
		}

		ReservationTnx debitTnx = TnxFactory.getDebitInstance(pnrPaxIdTo, infantAmount,
				ReservationTnxNominalCode.INFANT_TRANSFER.getCode(), credentialsDTO,
				CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);

		reservationTnxDao.saveTransaction(debitTnx);

		Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) response
				.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
		Collection<TnxCreditPayment> cfPayments = null;
		if (paxCreditPayments != null && !paxCreditPayments.isEmpty())
			cfPayments = getAllCreditPayments(paxCreditPayments);

		if (cfPayments != null) {
			ReservationPaxPaymentMetaTO newReservationPaxPaymentMetaTO = TnxGranularityReservationUtils
					.getReservationPaymentMetaTOForCreditsCarryForward(Integer.parseInt(pnrPaxIdTo),
							reservationPaxPaymentMetaTO, enableTransactionGranularity);

			cfPayments = generatePayCurrency(cfPayments);
			newReservationPaxPaymentMetaTO.setPerPaxBaseTotalPayAmount(getTotalPaymentAmount(cfPayments));

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECORD_PAYMENT);
			command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxIdTo);
			command.setParameter(CommandParamNames.PAYMENT_LIST, cfPayments);
			command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, newReservationPaxPaymentMetaTO);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
			response = (DefaultServiceResponse) command.execute();
		}

		if (response.isSuccess()) {
			return response;
		} else {
			throw new ModuleException("revacc.transferinfant.failed");
		}

	}

	private Integer retrieveCorrectTransactionSeq(String pnrPaxId) {
		Integer maxTxnSeq = 0;
		try {
			if (pnrPaxId != null) {
				maxTxnSeq = ReservationModuleUtils.getRevenueAccountBD()
						.getPNRPaxMaxTransactionSeq(Integer.parseInt(pnrPaxId));
			}
		} catch (ModuleException e) {
			maxTxnSeq = 1;
		}
		return maxTxnSeq;
	}

	private BigDecimal getTotalPaymentAmount(Collection<TnxCreditPayment> paymentsCollection) {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		TnxCreditPayment tnxCreditPayment;

		for (Iterator<TnxCreditPayment> itTnxCreditPayment = paymentsCollection.iterator(); itTnxCreditPayment
				.hasNext();) {
			tnxCreditPayment = (TnxCreditPayment) itTnxCreditPayment.next();
			totalAmount = AccelAeroCalculator.add(totalAmount, tnxCreditPayment.getAmount());
		}

		return totalAmount;
	}

	// Get the list of all credit records to be set to the response.
	private Collection<TnxCreditPayment> getAllCreditPayments(
			Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments) {
		Collection<TnxCreditPayment> allCreditPayments = new ArrayList<TnxCreditPayment>();
		for (Collection<TnxCreditPayment> payCollection : paxCreditPayments.values()) {
			allCreditPayments.addAll(payCollection);
		}
		return allCreditPayments;
	}

	/**
	 * Inject the Payment Currency DTO
	 * 
	 * Few facts:
	 * 
	 * Ideally it's better if we can take the payment currency information from
	 * the frontend. Since there is no frontend payment information coming for
	 * the updateReservation flow we have a problem.
	 * 
	 * Since I need to re tag easily and also credit carry forward is not that
	 * critical to track in local currency. I will be tracking it in the base
	 * currency.
	 * 
	 * Please improve this in the future. Nili [6:50 PM 7/7/2009]
	 * 
	 * @param paymentsCollection
	 * @return
	 */
	private Collection<TnxCreditPayment> generatePayCurrency(Collection<TnxCreditPayment> paymentsCollection) {
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(AppSysParamsUtil.getBaseCurrency(), new BigDecimal("1"));
		TnxCreditPayment tnxCreditPayment;

		for (Iterator<TnxCreditPayment> itTnxCreditPayment = paymentsCollection.iterator(); itTnxCreditPayment
				.hasNext();) {
			tnxCreditPayment = (TnxCreditPayment) itTnxCreditPayment.next();
			tnxCreditPayment.setPayCurrencyDTO((PayCurrencyDTO) payCurrencyDTO.clone());
			tnxCreditPayment.getPayCurrencyDTO().setTotalPayCurrencyAmount(tnxCreditPayment.getAmount());
		}

		return paymentsCollection;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param credentialsDTO
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxIdFrom, String pnrPaxIdTo, BigDecimal infantAmount,
			CredentialsDTO credentialsDTO) throws ModuleException {

		if (pnrPaxIdFrom == null || pnrPaxIdTo == null || infantAmount == null || credentialsDTO == null)// throw
																											// exception
			throw new ModuleException("airreservations.arg.invalid.null");
	}
}
