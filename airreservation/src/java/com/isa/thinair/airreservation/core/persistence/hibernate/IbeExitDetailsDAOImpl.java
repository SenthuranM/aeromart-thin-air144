package com.isa.thinair.airreservation.core.persistence.hibernate;

import com.isa.thinair.airreservation.api.model.IbeUserExitDetails;
import com.isa.thinair.airreservation.core.persistence.dao.IbeExitDetailsDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * @author Eshan
 * @since 1.0
 * @isa.module.dao-impl dao-name="IbeExitDetailsDAO"
 */
public class IbeExitDetailsDAOImpl extends PlatformBaseHibernateDaoSupport implements IbeExitDetailsDAO {
	@Override
	public void saveIbeExitDetails(IbeUserExitDetails ibeExitDetails) {
		hibernateSaveOrUpdate(ibeExitDetails);

	}

	@Override
	public IbeUserExitDetails getIbeUserExitDetails(Integer exitDetailsId) {
		return (IbeUserExitDetails) get(IbeUserExitDetails.class,exitDetailsId);
	}
}
