/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.core.constants.SystemParamKeys;

/**
 * @author Lasantha Pambagoda
 */
abstract class CreditFactory {

	protected static ReservationCredit getInstance(long tnxId, String pnrPaxId, BigDecimal amount, Date expDate) {

		ReservationCredit credit = new ReservationCredit();

		credit.setTnxId(tnxId);
		credit.setPnrPaxId(pnrPaxId);
		credit.setBalance(amount);

		if (expDate == null) {
			// This is for a new record
			String sysCredStr = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SYSTEM_CREDIT_PERIOD);
			int sysCreditPeriod = Integer.parseInt(sysCredStr);
			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(new Date(System.currentTimeMillis()));
			calander.add(GregorianCalendar.MONTH, sysCreditPeriod);
			expDate = calander.getTime();
		}
		// else is for a credit BF record
		credit.setExpiryDate(expDate);

		return credit;
	}
}
