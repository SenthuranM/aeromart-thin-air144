package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto;

import java.util.HashMap;
import java.util.Map;

public class RuleResponseDTO {

	Map<Integer, ModificationTypeWiseSegsResponse> affectedPaxWiseSegmentList = new HashMap<>();

	String ruleRef;

	public boolean hasEffectedPax() {
		return !affectedPaxWiseSegmentList.isEmpty();
	}

	public String getRuleRef() {
		return ruleRef;
	}

	public void setRuleRef(String ruleRef) {
		this.ruleRef = ruleRef;
	}

	public Map<Integer, ModificationTypeWiseSegsResponse> getAffectedPaxWiseSegment() {
		return affectedPaxWiseSegmentList;
	}

	public void setAffectedPaxWiseSegment(Map<Integer,ModificationTypeWiseSegsResponse> affectedPaxWiseSegment) {
		this.affectedPaxWiseSegmentList = affectedPaxWiseSegment;
	}

}
