/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context;

/**
 * @author udithad
 *
 */
public class EndElementContext extends ElementContext {

	private boolean moreElements;
	private int partNumber;

	public boolean isMoreElements() {
		return moreElements;
	}

	public void setMoreElements(boolean moreElements) {
		this.moreElements = moreElements;
	}

	public int getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(int partNumber) {
		this.partNumber = partNumber;
	}

}
