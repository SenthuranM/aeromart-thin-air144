/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.LmsBlockedCredit;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to Cancel blocked Un-Utilized LMS Credit record & expire credit status
 * 
 * @author M.Rikaz
 * @isa.module.command name="ClearLMSBlockedCreditPaymentsGravty"
 */
public class ClearIssuedUnConsumedLMSCreditPayments extends DefaultBaseCommand {

	// Dao's
	private ReservationDAO reservationDAO;

	private static Log log = LogFactory.getLog(ClearIssuedUnConsumedLMSCreditPayments.class);

	/**
	 * constructor of the ClearBlockedLMSCredits command
	 */
	public ClearIssuedUnConsumedLMSCreditPayments() {

		reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
	}

	/**
	 * execute method of the ClearBlockedLMSCredits command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		if (AppSysParamsUtil.isLMSEnabled()) {
			int clearBlockedCredits = AppSysParamsUtil.getCutOffTimeForCancelIssuedUnconsumedLMSPoints();
			clearBlockedCredits = clearBlockedCredits * -1;

			Date currentTime = CalendarUtil.getCurrentZuluDateTime();
			Date creditToBeRemoved = CalendarUtil.addMinutes(currentTime, clearBlockedCredits);

			log.info("CHECKING EXPIRED UNCONSUMED LMS BLOCKED CREDIT PAYMENTS CANCELLATION & CLEARANCE");
			log.info("CURRENT TIME:" + CalendarUtil.formatDate(currentTime, CalendarUtil.PATTERN5) + " EXPIRATION CUTOFF TIME:"
					+ CalendarUtil.formatDate(creditToBeRemoved, CalendarUtil.PATTERN5));

			Collection<LmsBlockedCredit> lmsBlockedCreditTnxs = reservationDAO.getOldUnconsumedLMSBlockedCreditInfoIds(creditToBeRemoved);

			if (lmsBlockedCreditTnxs != null && !lmsBlockedCreditTnxs.isEmpty()) {
				log.info("###########################################################");
				log.info("LOADING EXPIRED UNCONSUMED LMS BLOCKED CREDIT PAYMENTS");

				for (LmsBlockedCredit lmsBlockedCredit : lmsBlockedCreditTnxs) {
					if (lmsBlockedCredit != null && lmsBlockedCredit.getLmsBlockedId() != null) {
						cancelBlockedLMSCredit(lmsBlockedCredit.getLmsBlockedId());
					}
				}
				log.info("EXPIRED UNCONSUMED LMS BLOCKED CREDIT PAYMENTS HAS BEEN CANCELLED & CLEARED");
				log.info("###########################################################");
			}
			log.info("COMPLETED CHECKING EXPIRED UNCONSUMED LMS BLOCKED CREDIT PAYMENTS CANCELLATION & CLEARANCE");
		}

		return response;

	}

	private void cancelBlockedLMSCredit(Integer lmsCreditBlockedInfoId) {

		if (lmsCreditBlockedInfoId != null) {
			
			LmsBlockedCredit lmsBlockedCredit = reservationDAO.getLmsBlockCreditInfoById(lmsCreditBlockedInfoId);

			ServiceResponce serviceResponce = null;
			int cancelAttempts = lmsBlockedCredit.getLmsCancelAttempts();

			boolean errorAtCancelService = false;
			String remarks = null;
			if (cancelAttempts < 3) {
				try {
					Set<String> myIntegers = lmsBlockedCredit.getLmsRewardIds();
					String[] rewardIds = new String[myIntegers.size()];
					int index = 0;
					for (String i : myIntegers) {
						rewardIds[index++] = i;
					}

					log.info("CANCEL UNCONSUMED BLOCKED LMS REWARDS ATTEMPT :" + (cancelAttempts + 1) + " FOR MEMBER INO:"
							+ lmsBlockedCredit.toString());

					serviceResponce = ReservationModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(lmsBlockedCredit.getPnr(), rewardIds,
							lmsBlockedCredit.getMemberId());
					if (!serviceResponce.isSuccess()) {
						errorAtCancelService = true;
						log.info("FAILED ::cancelRedeemedUnconsumedLoyaltyPoints:: CANCEL BLOCKED LMS REWARDS ATTEMPT :"
								+ (cancelAttempts + 1) + " FOR MEMBER:" + lmsBlockedCredit.toString());
					}
				} catch (Exception e) {
					log.error("ERROR ::cancelRedeemedunconsumedLoyaltyPoints:: CANCEL BLOCKED LMS REWARDS ATTEMPT :" + (cancelAttempts + 1)
							+ " FOR MEMBER:" + lmsBlockedCredit.toString());
					errorAtCancelService = true;
					remarks = StringUtil.trimStringMessage(e.getMessage(), 90);
				}
			}

			if (!errorAtCancelService) {
				try {
					String cancelAttemptsFailed = null;
					if (cancelAttempts == 3) {
						cancelAttemptsFailed = "LMSServer - Unable to cancel LMS Blocked Credit";
					}
					reservationDAO.updateLmsBlockCreditStatus(lmsBlockedCredit.getTempTnxId(), LmsBlockedCredit.CANCEL,
							cancelAttemptsFailed);
					log.info("UPDATE LMS BLOCKED CREDIT UTILIZE STATUS " + LmsBlockedCredit.CANCEL);
				} catch (Exception e) {
					log.error("ERROR @ updateLmsBlockCreditStatus :: " + e.getMessage());
				}
			} else {
				lmsBlockedCredit.setLmsCancelAttempts(cancelAttempts + 1);
				lmsBlockedCredit.setRemarks(remarks);
				reservationDAO.saveLmsBlockCreditInfo(lmsBlockedCredit);

			}
		}

	}
	

}
