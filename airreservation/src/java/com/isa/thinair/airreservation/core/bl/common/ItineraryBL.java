/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.ItineraryValueFormatterUtil;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryChargesDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryPaymentsDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaymentDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryContactInfoTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFareRuleTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFlightOndGroup;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFlightTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPassengerTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxSegmentSSRStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils.TEMPLATE_TYPES;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.util.I18NMessagingUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.CommonTemplatingDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.HTTPResourceStatusLookupUtil;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Itinerary related business methods
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ItineraryBL {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ItineraryBL.class);

	/** Holds the airreservation config instance */
	private static final AirReservationConfig config = ReservationModuleUtils.getAirReservationConfig();

	/**
	 * Hide the constructor
	 */
	private ItineraryBL() {

	}

	/**
	 * Gets itinerary for print
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public static String getItineraryForPrint(ItineraryLayoutModesDTO itineraryLayoutModesDTO,
			Collection<Integer> colSelectedPnrPaxIds, CredentialsDTO credentialsDTO, boolean isCheckIndividual)
			throws ModuleException {
		String templateName = config.getItineraryPrintTemplate();
		itineraryLayoutModesDTO.setTemplateName(templateName);

		if (isCheckIndividual) {
			Iterator<Integer> iterator = colSelectedPnrPaxIds.iterator();

			StringBuilder sb = new StringBuilder();
			Collection<Integer> paxIds;

			while (iterator.hasNext()) {
				paxIds = new ArrayList<Integer>();
				paxIds.add(iterator.next());
				sb.append(ItineraryBL.getItineraryForPrint(itineraryLayoutModesDTO, paxIds, credentialsDTO));

				if (iterator.hasNext()) {
					sb.append("<h2 style='page-break-before: always;'></h2>");
				}
			}

			return sb.toString();
		} else {
			return ItineraryBL.getItineraryForPrint(itineraryLayoutModesDTO, colSelectedPnrPaxIds, credentialsDTO);
		}
	}

	/**
	 * Gets interline itinerary for print
	 * 
	 * @param lccClientReservation
	 * @return the interline itinerary ready for displaying in HTML format
	 * @throws ModuleException
	 */
	public static String getInterlineItineraryForPrint(ItineraryDTO itineraryDTO) throws ModuleException {

		// Create data map
		HashMap<String, Object> interlineItineraryDataMap = getInterlineItineraryDataMap(itineraryDTO);

		// Get interline data map
		String templateName = config.getInterlineItineraryPrintTemplate();
		String localeSpecificTemplateName = templateName;

		/*
		 * Locale locale = new Locale(itineraryDTO.getItineraryLanguage()); if
		 * (locale.getLanguage().compareTo(Locale.getDefault().getLanguage()) != 0) { if (templateName.indexOf(".") > 0)
		 * { localeSpecificTemplateName = templateName.substring(0, templateName.indexOf(".")) + "_" +
		 * locale.getLanguage().toUpperCase() + templateName.substring(templateName.indexOf(".")); } else {
		 * localeSpecificTemplateName = templateName + "_" + locale.getLanguage(); } }
		 */

		// Use engine to generate page
		StringWriter writer = new StringWriter();
		try {
			new TemplateEngine().writeTemplate(interlineItineraryDataMap, localeSpecificTemplateName, writer);
		} catch (Exception e) {
			throw new ModuleException("airreservations.itinerary.errorCreatingFromTemplate", e); // TODO : Change error
																									// code
		}

		return writer.toString();
	}

	private static HashMap<String, Object> getInterlineItineraryDataMap(ItineraryDTO itineraryDTO) throws ModuleException {

		Boolean isFirstDepartingCarrierWiseTNCEnabled = config.getFirstDepartingCarrierWiseTNCEnabled();
		Boolean isViewFareBasisCode = config.isViewFareBasisCode();
		boolean isCnxReservation = ReservationInternalConstants.ReservationStatus.CANCEL.equals(itineraryDTO.getBooking().getStatus());

		// Get Terms & Conditions
		List<String> operatingCarriers = new ArrayList<String>(itineraryDTO.getBooking().getPNR().keySet());
		String termsNConditionsData = ReservationTemplateUtils.getInterlineTermsNConditions(
				new Locale(itineraryDTO.getItineraryLanguage()), itineraryDTO.getBooking().getMarketingCarrier(),
				operatingCarriers, isFirstDepartingCarrierWiseTNCEnabled, itineraryDTO.getFirstDepartingCarrier(), isCnxReservation);

		// Data map
		HashMap<String, Object> interlineItineraryDataMap = new HashMap<String, Object>();

		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_CARRIER_LOGO_WITH_FLT_INFO))) {
			itineraryDTO.setIncludeThumbnailLogo(true);
		}

		// view fare basis code in the itinerary
		if(isViewFareBasisCode){
			itineraryDTO.setViewFareBasisCode(true);
		}
		
		boolean includePaymentDetails = false;
		if (itineraryDTO.isIncludePaymentDetails()) {
			includePaymentDetails = true;
		} else {
			if (AppSysParamsUtil.showCreditCardInfoInItineraryAlways()) {
				for (ItineraryPassengerTO passengerTO : itineraryDTO.getPassengers()) {
					if (passengerTO.getPayments() != null && passengerTO.getPayments().size() > 0) {
						includePaymentDetails = true;
					}
				}
			}
		}

		// Hide Stop Over Airport
		if (AppSysParamsUtil.isHideStopOverEnabled()) {
			hideStopOverAirportCodeToDisplay(itineraryDTO);
		}

		String strTotalFare = null;
		if (!itineraryDTO.isFareMasked()) {
			BigDecimal totalFare = new BigDecimal(0);
			List<ItineraryPassengerTO> passengers = itineraryDTO.getPassengers();
			for (ItineraryPassengerTO passenger : passengers) {
				if (passenger.getBaseCurrencyFinancials() != null && passenger.getBaseCurrencyFinancials().getTotalFare() != null) {
					totalFare = AccelAeroCalculator.add(totalFare, new BigDecimal(passenger.getBaseCurrencyFinancials()
							.getTotalFare()));
				}
			}
			strTotalFare = (AppSysParamsUtil.isHideBaseCurrencyDecimalsInItinerary() ? ItineraryValueFormatterUtil
					.safareDecimalRemover(totalFare.toString()) : totalFare.toString());
		} else {
			// This means fare masking is there and we do not need to show fares
			List<ItineraryPassengerTO> passengers = itineraryDTO.getPassengers();
			for (ItineraryPassengerTO passenger : passengers) {
				if (passenger.getBaseCurrencyFinancials() != null && passenger.getBaseCurrencyFinancials().getTotalFare() != null) {
					strTotalFare = passenger.getBaseCurrencyFinancials().getTotalFare();
				}
				break;
			}
		}

		interlineItineraryDataMap.put("includePaxFinancials", itineraryDTO.isIncludePaxFinancials());
		interlineItineraryDataMap.put("includePaymentDetails", includePaymentDetails); 
		interlineItineraryDataMap.put("includeTicketCharges", itineraryDTO.isIncludeTicketCharges());
		interlineItineraryDataMap.put("includeTermsAndConditions", itineraryDTO.isIncludeTermsAndConditions());
		interlineItineraryDataMap.put("includeFareRules", itineraryDTO.isIncludeFareRules());
		interlineItineraryDataMap.put("includeFlightBaggageAllowance", itineraryDTO.isIncludeFlightBaggageAllowance());
		interlineItineraryDataMap.put("includeStationContactDetails", itineraryDTO.isIncludeStationContactDetails());
		interlineItineraryDataMap.put("includePaxContactDetails", itineraryDTO.isIncludePaxContactDetails());
		interlineItineraryDataMap.put("showExtraChargeDetails", itineraryDTO.isShowExtraChargeDetails());
		interlineItineraryDataMap.put("includeCreditExpiryDate", itineraryDTO.isIncludePaxCreditExpiryDate());
		interlineItineraryDataMap.put("showEtktPerPax", itineraryDTO.isShowETicketPerPax());
		interlineItineraryDataMap.put("showDOB", itineraryDTO.isShowDOB());
		interlineItineraryDataMap.put("showFOID", itineraryDTO.isShowFOID());
		interlineItineraryDataMap.put("booking", itineraryDTO.getBooking());
		interlineItineraryDataMap.put("totalFare", strTotalFare == null ? "" : strTotalFare);
		interlineItineraryDataMap.put("agent", itineraryDTO.getAgent());
		interlineItineraryDataMap.put("ondCharges", itineraryDTO.getOndCharges());
		interlineItineraryDataMap.put("flightOndGroups", itineraryDTO.getFlightOndGroups());
		interlineItineraryDataMap.put("openRetFlightOndGroups", itineraryDTO.getOpenRetFlightOndGroups());
		interlineItineraryDataMap.put("fareRules", itineraryDTO.getFareRules());
		interlineItineraryDataMap.put("passengers", itineraryDTO.getPassengers());
		interlineItineraryDataMap.put("anciAvailability", itineraryDTO.getAnciAvailabilityTO());
		interlineItineraryDataMap.put("insurance", itineraryDTO.getInsuranceTO());
		interlineItineraryDataMap.put("termsNConditions", termsNConditionsData);
		// Endorsement
		interlineItineraryDataMap.put("includeEndorsements", itineraryDTO.isIncludeEndorsements());
		interlineItineraryDataMap.put("endorsements", itineraryDTO.getEndorsements());

		interlineItineraryDataMap.put("passengerTickets", itineraryDTO.getTicketInfo());
		interlineItineraryDataMap.put("flexiRuleDetails", itineraryDTO.getFlexiRules());

		//fare basis code
		interlineItineraryDataMap.put("viewFareBasisCode", itineraryDTO.isViewFareBasisCode());
		
		if (AppSysParamsUtil.isRakEnabled()) {
			interlineItineraryDataMap.put("showInsurance", true);
		} else {
			interlineItineraryDataMap.put("showInsurance", false);
		}

		// -- Logo	
		interlineItineraryDataMap.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		interlineItineraryDataMap.put("logoImageName",
				StaticFileNameUtil.getCorrected("LogoAni" + AppSysParamsUtil.getDefaultCarrierCode() + ".gif"));
		// OC logo for flight
		if (AppSysParamsUtil.isThumbnailLogoEnabled()) {
		interlineItineraryDataMap.put("showThumbnailLogoImageName",true);
		}
		// -- Company Address
		interlineItineraryDataMap.put("companyAddress", strItineraryAddress(AppSysParamsUtil.getDefaultCarrierCode()));

		// Itinerary Advertisement Image Path
		interlineItineraryDataMap.put("itineraryAdvertisementImageURL",	getInterlineItineraryAdvertismentResourceURL(itineraryDTO));

		Map<String, String> labels = I18NMessagingUtil.getItineraryMessages(new Locale(itineraryDTO.getItineraryLanguage()));

		interlineItineraryDataMap.put("label", labels);

		String direction = "ltr";
		if (itineraryDTO.getItineraryLanguage().equals("ar") || itineraryDTO.getItineraryLanguage().equals("fa")) {
			direction = "rtl";
		}
		interlineItineraryDataMap.put("direction", direction);
		interlineItineraryDataMap.put("isInterlineRelated", AppSysParamsUtil.isLCCConnectivityEnabled());

		// airport Msgs
		interlineItineraryDataMap.put("airportMsgs", itineraryDTO.getAirportMsgs());

		// show Class Of service
		interlineItineraryDataMap.put("showCOS", itineraryDTO.isShowCOS());

		// show logical cabin class
		interlineItineraryDataMap.put("showLogicalCC", itineraryDTO.isShowLogicalCC());

		// show Check in closing time
		interlineItineraryDataMap.put("showCheckInClosingTime", itineraryDTO.isShowCheckInClosingTime());

		interlineItineraryDataMap.put("OndTaxesFeesCharges", itineraryDTO.getOndTaxesFeesCharges());
		interlineItineraryDataMap.put("showPageBreak", (itineraryDTO.getPassengers() != null && itineraryDTO.getPassengers()
				.size() > 1) ? true : false);

		interlineItineraryDataMap.put("includeThumbnailLogo", itineraryDTO.isIncludeThumbnailLogo());
		interlineItineraryDataMap.put("showFlightStopOverInfo", itineraryDTO.isShowFlightStopOverInfo());
		interlineItineraryDataMap.put("showStandbyTravelValidity", itineraryDTO.isShowStandbyTravelValidity());

		interlineItineraryDataMap.put("paxContactInfo", itineraryDTO.getContactInfo());
		interlineItineraryDataMap.put("stationContactInfo", itineraryDTO.getStationContacts());

		interlineItineraryDataMap.put("includeAutoCancellationInfo", itineraryDTO.isIncludeAutoCancellationInfo());
		interlineItineraryDataMap.put("showDiscountAmounts", itineraryDTO.isShowDiscountAmounts());
		interlineItineraryDataMap.put("creditDiscount", itineraryDTO.isCreditDiscount());
		interlineItineraryDataMap.put("showBundledService", itineraryDTO.isShowBundledService());
		interlineItineraryDataMap.put("showItineraryFareBreakDown", itineraryDTO.isShowChargesInItineraryPassengerDetails());

		return interlineItineraryDataMap;
	}

	private static String getInterlineItineraryAdvertismentResourceURL(ItineraryDTO itineraryDTO) throws ModuleException {

		HTTPResourceStatusLookupUtil resourceLookup = HTTPResourceStatusLookupUtil.getInstance();
		String resourceHomeUrl = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ITINERARY_ADVERTISEMENT_IMAGE_URL);
		String itilanguage = itineraryDTO.getItineraryLanguage();
		String resourcePrefix = "";
		String resourceURL = "";
		String resourceToDisplay = "";

		if (resourceHomeUrl == null || resourceHomeUrl.trim().equals("")) {
			return "";
		}

		if (itilanguage == null || itilanguage.equals("")) {
			itilanguage = "EN";
		}

		String[] languages = { itilanguage, "EN" };

		Collection<ItineraryFlightOndGroup> colItineraryDto = itineraryDTO.getFlightOndGroups();
		
		if (AppSysParamsUtil.isAllowItineraryAdvertisement()) {
			resourceToDisplay = advertisementImageForOND(itineraryDTO, resourceLookup, itilanguage);
		}

		if (resourceToDisplay.equals("")) {
			for (ItineraryFlightOndGroup ondGrp : colItineraryDto) {
				Iterator<ItineraryFlightTO> iItineraryFlightTO = ondGrp.getFlights().iterator();
				ItineraryFlightTO itineraryFlightTO;
				StringBuffer segmentsBuff = new StringBuffer("");

				while (iItineraryFlightTO.hasNext()) {
					itineraryFlightTO = (ItineraryFlightTO) iItineraryFlightTO.next();
					segmentsBuff.append(itineraryFlightTO.getSegmentCode() + "/");
				}

				String segments[] = segmentsBuff.toString().split("/");
				for (int index = 0; index < 2; index++) {
					resourcePrefix = "DEP_" + segments[0].toUpperCase() + "_" + languages[index].toUpperCase()
							+ AppSysParamsUtil.getStaticFileSuffix();
					resourceURL = resourceLookup.availableResource(resourcePrefix);
					if (resourceURL != null && !resourceURL.equals("")) {
						resourceToDisplay = resourceURL;
					}
					if (!resourceURL.equals("")) {
						break;
					}

					resourcePrefix = "ARR_" + segments[segments.length - 1].toUpperCase() + "_" + languages[index].toUpperCase()
							+ AppSysParamsUtil.getStaticFileSuffix();
					resourceURL = resourceLookup.availableResource(resourcePrefix);
					if (resourceURL != null && !resourceURL.equals("")) {
						resourceToDisplay = resourceURL;
					}
					if (!resourceURL.equals("")) {
						break;
					}
				}
			}
		}
		return resourceToDisplay;
	}

	private static void hideStopOverAirportCodeToDisplay(ItineraryDTO itineraryDTO) {
		List<ItineraryFlightOndGroup> flightONDGrps = itineraryDTO.getFlightOndGroups();
		for (ItineraryFlightOndGroup flightOnd : flightONDGrps) {
			List<ItineraryFlightTO> flightTos = flightOnd.getFlights();
			for (ItineraryFlightTO itiFlightTo : flightTos) {
				String segmentCode = itiFlightTo.getSegmentCode();
				if (segmentCode != null && segmentCode.split("/").length > 2) {
					itiFlightTo.setSegmentCode(ReservationApiUtils.hideStopOverSeg(segmentCode));
				}
			}
		}

		List<ItineraryFareRuleTO> itiFareRuleTos = itineraryDTO.getFareRules();
		for (ItineraryFareRuleTO itiFareRuleTo : itiFareRuleTos) {
			String ondCode = itiFareRuleTo.getOndCode();
			if (ondCode != null && ondCode.split("/").length > 2) {
				itiFareRuleTo.setOndCode(ReservationApiUtils.hideStopOverSeg(ondCode));
			}
		}
	}

	/**
	 * Email the interline itinerary
	 * 
	 * @param itineraryDTO
	 * @throws ModuleException
	 */
	public static void emailInterlineItinerary(ItineraryDTO itineraryDTO, CredentialsDTO credentialsDTO) throws ModuleException {

		// Check to see if the required contact details are present
		ItineraryContactInfoTO contactInfo = itineraryDTO.getContactInfo();
		if (BeanUtils.nullHandler(contactInfo.getEmail()).length() == 0) {
			throw new ModuleException("airreservations.arg.invalidEmailOnlyUser");
		} else if (BeanUtils.nullHandler(contactInfo.getFirstName()).length() == 0
				|| BeanUtils.nullHandler(contactInfo.getLastName()).length() == 0) {
			throw new ModuleException("airreservations.arg.invalidEmailUser");
		}

		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		// User Message
		UserMessaging userMessaging = new UserMessaging();
		userMessaging.setFirstName(contactInfo.getFirstName());
		userMessaging.setLastName(contactInfo.getLastName());
		userMessaging.setToAddres(contactInfo.getEmail());

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		HashMap<String, Object> itineraryDataMap = getInterlineItineraryDataMap(itineraryDTO);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(itineraryDataMap);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.INTERLINE_ITINERARY_EMAIL);
		topic.setLocale(new Locale(itineraryDTO.getItineraryLanguage()));
		topic.setAttachMessageBody(true);
		topic.setAuditInfo(composeAudit(itineraryDTO.getBooking().getGDSPNR(), credentialsDTO));

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		messagingServiceBD.sendMessage(messageProfile);
	}

	/**
	 * Email the itinerary
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public static void emailItinerary(ItineraryLayoutModesDTO itineraryLayoutModesDTO, Collection<Integer> colSelectedPnrPaxIds,
			CredentialsDTO credentialsDTO, boolean isCheckIndividual) throws ModuleException {
		log.debug("Inside emailItinerary");

		Reservation reservation = getReservation(itineraryLayoutModesDTO, credentialsDTO);

		// Check the contact person's email address exist or not
		if (reservation.getContactInfo().getFirstName() == null || reservation.getContactInfo().getLastName() == null
				|| reservation.getContactInfo().getEmail() == null) {
			throw new ModuleException("airreservations.arg.invalidEmailUser");
		}

		if (isCheckIndividual) {
			Iterator<Integer> iterator = colSelectedPnrPaxIds.iterator();
			Collection<Integer> pnrPaxIds;

			while (iterator.hasNext()) {
				pnrPaxIds = new ArrayList<Integer>();
				pnrPaxIds.add(iterator.next());

				emailItinerary(reservation, pnrPaxIds, itineraryLayoutModesDTO, credentialsDTO);
			}
		} else {
			emailItinerary(reservation, colSelectedPnrPaxIds, itineraryLayoutModesDTO, credentialsDTO);
		}

		log.debug("Exit emailItinerary");
	}

	private static void emailItinerary(Reservation reservation, Collection<Integer> pnrPaxIds,
			ItineraryLayoutModesDTO itineraryLayoutModesDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		// User Message
		UserMessaging userMessaging = new UserMessaging();
		userMessaging.setFirstName(reservation.getContactInfo().getFirstName());
		userMessaging.setLastName(reservation.getContactInfo().getLastName());
		userMessaging.setToAddres(reservation.getContactInfo().getEmail());

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		HashMap<String, Object> itineraryDataMap = (HashMap<String, Object>) getItineraryDataMap(itineraryLayoutModesDTO,
				reservation, pnrPaxIds, credentialsDTO);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(itineraryDataMap);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.ITINERARY_EMAIL);
		topic.setLocale(itineraryLayoutModesDTO.getLocale());
		topic.setAttachMessageBody(true);
		topic.setAuditInfo(composeAudit(itineraryLayoutModesDTO.getPnr(), credentialsDTO));

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		messagingServiceBD.sendMessage(messageProfile);
	}

	/**
	 * Get the itinerary
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private static String getItineraryForPrint(ItineraryLayoutModesDTO itineraryLayoutModesDTO,
			Collection<Integer> colSelectedPnrPaxIds, CredentialsDTO credentialsDTO) throws ModuleException {
		Reservation reservation = getReservation(itineraryLayoutModesDTO, credentialsDTO);

		HashMap<String, Object> itineraryMap = (HashMap<String, Object>) getItineraryDataMap(itineraryLayoutModesDTO,
				reservation, colSelectedPnrPaxIds, credentialsDTO);
		TemplateEngine engine = null;
		String templateName = itineraryLayoutModesDTO.getTemplateName();
		Locale locale = itineraryLayoutModesDTO.getLocale();
		String localeSpecificTemplateName = templateName;
		StringWriter writer = null;

		if ((locale != null) && (!locale.getLanguage().equals(Locale.getDefault().getLanguage()))) {
			if (templateName.indexOf(".") > 0) {
				localeSpecificTemplateName = templateName.substring(0, templateName.indexOf(".")) + "_"
						+ locale.getLanguage().trim().toUpperCase() + templateName.substring(templateName.indexOf("."));
			} else {
				localeSpecificTemplateName = templateName + "_" + locale.getLanguage().trim();
			}
		}

		try {
			engine = new TemplateEngine();
			writer = new StringWriter();
		} catch (Exception e) {
			throw new ModuleException("airreservations.itinerary.errorCreatingFromTemplate", e);
		}
		if (itineraryMap != null) {
			engine.writeTemplate(itineraryMap, localeSpecificTemplateName, writer);
		}

		return writer.toString();
	}

	/**
	 * Loads the reservation
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Reservation getReservation(ItineraryLayoutModesDTO itineraryLayoutModesDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Reservation reservation = null;

		// Validating the parameters
		if (itineraryLayoutModesDTO.getPnr() == null || itineraryLayoutModesDTO.getLocale() == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		// Loads the reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(itineraryLayoutModesDTO.getPnr());
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		// If it's calling from XBE
		if (credentialsDTO.getSalesChannelCode() != null
				&& credentialsDTO.getSalesChannelCode().intValue() == SalesChannel.TRAVEL_AGENT) {
			pnrModesDTO.setLoadLocalTimes(true);
			reservation = PnrZuluProxy.getReservationWithLocalTimes(pnrModesDTO, credentialsDTO);
			String defaultCarrierCode = credentialsDTO.getDefaultCarrierCode();
			if (defaultCarrierCode == null || "".equals(defaultCarrierCode)) {
				defaultCarrierCode = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			}
			itineraryLayoutModesDTO.setCarrierCode(defaultCarrierCode);
			// If it's from IBE
		} else {
			reservation = ReservationProxy.getReservation(pnrModesDTO);
		}

		return reservation;
	}

	/**
	 * Returns the Itinerary Data Map
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param reservation
	 * @param colSelectedPnrPaxIds
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private static Map<String, Object> getItineraryDataMap(ItineraryLayoutModesDTO itineraryLayoutModesDTO,
			Reservation reservation, Collection<Integer> colSelectedPnrPaxIds, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside getItineraryMap");

		CommonTemplatingDTO commonTemplatingDTO = null;
		String termsNConditionsData;
		ReservationInsurance resinsurence = null;

		if (CommonsServices.getGlobalConfig().getActiveCabinClassesMap() == null
				|| CommonsServices.getGlobalConfig().getActiveCabinClassesMap().size() == 1) {
			itineraryLayoutModesDTO.setIncludeClassOfService(false);
		} else {
			itineraryLayoutModesDTO.setIncludeClassOfService(true);
		}

		if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			itineraryLayoutModesDTO.setIncludeLogicalCC(true);
		} else {
			itineraryLayoutModesDTO.setIncludeLogicalCC(false);
		}

		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_CARRIER_LOGO_WITH_FLT_INFO))) {
			itineraryLayoutModesDTO.setIncludeThumbnailLogo(true);
		}

		// If it's calling from XBE
		if (credentialsDTO.getSalesChannelCode() != null
				&& credentialsDTO.getSalesChannelCode().intValue() == SalesChannel.TRAVEL_AGENT) {
			termsNConditionsData = ReservationTemplateUtils.getTermsNConditions(itineraryLayoutModesDTO.getLocale(),
					TEMPLATE_TYPES.XBE_TEMPLATE);
			// If it's from IBE
		} else {
			termsNConditionsData = ReservationTemplateUtils.getTermsNConditions(itineraryLayoutModesDTO.getLocale(),
					TEMPLATE_TYPES.IBE_TEMPLATE);
			commonTemplatingDTO = AppSysParamsUtil.composeCommonTemplatingDTO(itineraryLayoutModesDTO.getCarrierCode());
		}

		// Check whether all passengers are correct passengers
		Map<Integer, Integer> adultAndInfantMap = checkValidityOfSelectedPassengers(reservation, colSelectedPnrPaxIds);

		if (colSelectedPnrPaxIds != null && adultAndInfantMap.size() == 0) {
			return null;
		} else {
			boolean showAmountsInPayCurrency = AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled();
			// Return the reservation for the template
			ReservationPaxDTO reservationPaxDTO = ReservationCoreUtils.parseReservation(reservation, adultAndInfantMap);

			// Setting the base currency
			String baseCurrency = AppSysParamsUtil.getBaseCurrency();
			reservationPaxDTO.setBaseCurrency(baseCurrency);
			reservationPaxDTO.setShowAmountsInPayCurrency(showAmountsInPayCurrency);

			// Set Agent details
			Agent ownerAgent = null;
			if (reservation.getAdminInfo().getOwnerAgentCode() != null) { // on account payment
				TravelAgentBD travelAgentBD = ReservationModuleUtils.getTravelAgentBD();
				ownerAgent = travelAgentBD.getAgent(reservation.getAdminInfo().getOwnerAgentCode());
			}

			if (showAmountsInPayCurrency) {
				String totalPaidAmountInPayCurrency = getTotalPaidAmountInPayCurrency(reservation.getPassengers());

				String totalAvailBalanceInPayCurrency = getAvailableBalance(reservation);

				reservationPaxDTO.setSpecifiedCurrencyCode("");

				// If no payments exist
				if (BeanUtils.nullHandler(totalPaidAmountInPayCurrency).isEmpty()) {
					String lastCurrencyCode = BeanUtils.nullHandler(reservation.getLastCurrencyCode());
					StringBuilder lastCurrencyAmtMsg = new StringBuilder();
					lastCurrencyAmtMsg.append(AccelAeroCalculator.getDefaultBigDecimalZero() + " ");

					if (lastCurrencyCode.isEmpty()) {
						lastCurrencyAmtMsg.append(AppSysParamsUtil.getBaseCurrency());
					} else {
						lastCurrencyAmtMsg.append(lastCurrencyCode);
					}
					reservationPaxDTO.setTotalPaidAmountInPayCurrency(lastCurrencyAmtMsg.toString());
					reservationPaxDTO.setTotalAvailableBalanceInPayCurrency(totalAvailBalanceInPayCurrency);
				} else {
					reservationPaxDTO.setTotalPaidAmountInPayCurrency(totalPaidAmountInPayCurrency);
					reservationPaxDTO.setTotalAvailableBalanceInPayCurrency(totalAvailBalanceInPayCurrency);
				}
			} else {
				String currencyCode = BeanUtils.nullHandler(getSpecifiedCurrencyCode(ownerAgent, baseCurrency));

				if (currencyCode.length() > 0) {
					reservationPaxDTO.setSpecifiedCurrencyCode(currencyCode);
					reservationPaxDTO.setSpecifiedCurrency(CurrencyConvertorUtils.getCurrency(currencyCode));
					reservationPaxDTO.setCurrencyExchangeRate(new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
							.getExchangeRate(baseCurrency, currencyCode));
				}
			}

			// If only segments exist. Cos this segments will have only the
			// confirmed segments.
			// Thus cancelled segments will be removed
			if (reservationPaxDTO.getSegments() != null && reservationPaxDTO.getSegments().size() != 0) {
				// Changing the segment codes to segment descriptions
				Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservationPaxDTO.getSegments().iterator();
				ReservationSegmentDTO reservationSegmentDTO;
				Collection<String> colAirportCodes = new ArrayList<String>();

				while (itReservationSegmentDTO.hasNext()) {
					reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
					colAirportCodes.addAll(Arrays.asList(reservationSegmentDTO.getSegmentCode().split("/")));
				}

				AirportBD airportBD = ReservationModuleUtils.getAirportBD();
				Map<String, CachedAirportDTO> mapAirportCodes = airportBD.getCachedOwnAirportMap(colAirportCodes);

				itReservationSegmentDTO = reservationPaxDTO.getSegments().iterator();
				while (itReservationSegmentDTO.hasNext()) {
					reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

					String composedSegment = "";
					String[] segments = reservationSegmentDTO.getSegmentCode().split("/");

					for (int i = 0; i < segments.length; i++) {
						CachedAirportDTO airport = mapAirportCodes.get(segments[i]);
						// Haider 4Feb09
						if (AppSysParamsUtil.isHideStopOverEnabled() && segments.length > 2 && i > 0 && i < (segments.length - 1))
							continue;
						if (i != 0) {
							composedSegment += " / ";
						}
						composedSegment += airport.getAirportName();
					}

					String departureAirportCode = null;
					if (segments.length > 0) {
						departureAirportCode = segments[0];
					}
					reservationSegmentDTO.setCheckInDate(ReservationApiUtils.getCheckInDate(
							reservationSegmentDTO.getDepartureDate(), departureAirportCode, reservationSegmentDTO.getFlightNo()));

					reservationSegmentDTO.setSegmentDescription(composedSegment);
				}
			}

			Map<Integer, Map<String, ItineraryPaymentsDTO>> itineraryPayDTOsByPaxByCur = new LinkedHashMap<Integer, Map<String, ItineraryPaymentsDTO>>();
			Collection<Serializable> colItineraryChargesDTO = new ArrayList<Serializable>();

			// Capturing the payment details
			if (itineraryLayoutModesDTO.isIncludeDetailPayments()) {
				itineraryPayDTOsByPaxByCur = getItineraryPaymentsDTOs(reservation, adultAndInfantMap);
			} else {
				// Check to see whether or not to show any credit card information
				// in itinerary
				if (AppSysParamsUtil.showCreditCardInfoInItineraryAlways()) {
					itineraryPayDTOsByPaxByCur = getItineraryCreditCardPaymentsOnly(reservation, adultAndInfantMap);
				}
			}

			boolean showPayCurrencyAmtsInPayDetails = false;
			Collection<Serializable> colItineraryPaymentDTOs = new ArrayList<Serializable>();
			if (itineraryPayDTOsByPaxByCur != null && itineraryPayDTOsByPaxByCur.size() > 0) {
				Integer pnrPaxId = null;

				Map<String, ItineraryPaymentsDTO> mapPaymentPerPaxByCur = null;
				Collection<ItineraryPaymentsDTO> colPaymentsPerPax = null;
				String currencyCode = null;
				for (Iterator<Integer> itineraryPayDTOsByPaxByCurIt = itineraryPayDTOsByPaxByCur.keySet().iterator(); itineraryPayDTOsByPaxByCurIt
						.hasNext();) {
					pnrPaxId = itineraryPayDTOsByPaxByCurIt.next();
					mapPaymentPerPaxByCur = itineraryPayDTOsByPaxByCur.get(pnrPaxId);
					if (!showPayCurrencyAmtsInPayDetails) {
						if (mapPaymentPerPaxByCur != null) {
							for (Iterator<String> mapPaymentPerPaxByCurKeyIt = mapPaymentPerPaxByCur.keySet().iterator(); mapPaymentPerPaxByCurKeyIt
									.hasNext();) {
								currencyCode = (String) mapPaymentPerPaxByCurKeyIt.next();
								if (!baseCurrency.equals(currencyCode)) {
									showPayCurrencyAmtsInPayDetails = true;
									break;
								}
							}
						}
					}

					colPaymentsPerPax = ReservationApiUtils.getSerializableValuesCollection(mapPaymentPerPaxByCur);
					if (colPaymentsPerPax != null && colPaymentsPerPax.size() > 0) {
						colItineraryPaymentDTOs.addAll(colPaymentsPerPax);
					}
				}
			}

			// Capturing the charges
			if (itineraryLayoutModesDTO.isIncludeDetailCharges()) {
				colItineraryChargesDTO = getItineraryChargesDTOs(reservation, adultAndInfantMap,
						itineraryLayoutModesDTO.getLocale().getLanguage());
			}

			// Get the external charges
			Collection<Serializable> colExternalChgDTO = getExternalCharges(reservation, adultAndInfantMap, credentialsDTO);

			// Get Seating, Meal details
			// Collection colItinerarySeatDTO = getSeatInformation(reservation);
			PaxSSRInfoDTO colItinerarySeatDTO = getAdditionalServiceInformation(reservation, false);

			PaxSSRInfoDTO colItinerarySSRDTO = getAdditionalServiceInformation(reservation, true);

			// Create carrier code legend
			String strCarrierCodeLegend = composeCarrierDescLegend(reservationPaxDTO);

			if (reservation.getReservationInsurance() != null && !reservation.getReservationInsurance().isEmpty()) {
				resinsurence = reservation.getReservationInsurance().get(0);
			}

			List<String> segCodesOrderByDep = new ArrayList<String>();
			for (ReservationSegmentDTO segment : (Collection<ReservationSegmentDTO>) reservationPaxDTO
					.getSegmentsOrderedByZuluDepartureDate()) {
				segCodesOrderByDep.add(segment.getSegmentCode());
			}

			// Setting the Paramters
			Map<String, Object> itineraryDatamap = new HashMap<String, Object>();
			itineraryDatamap.put("reservationPaxDTO", reservationPaxDTO);
			itineraryDatamap.put("termsNConditions", termsNConditionsData);
			itineraryDatamap.put("ownerAgent", ownerAgent);
			itineraryDatamap.put("includePrices", new Boolean(itineraryLayoutModesDTO.isIncludePassengerPrices()));
			itineraryDatamap.put("companyAddress", strItineraryAddress(itineraryLayoutModesDTO.getCarrierCode()));
			itineraryDatamap.put("showPayCurrencyAmtsInPayDetails", new Boolean(showPayCurrencyAmtsInPayDetails));
			itineraryDatamap.put("colItineraryPaymentsDTO", colItineraryPaymentDTOs);
			itineraryDatamap.put("colItineraryChargesDTO", colItineraryChargesDTO);
			itineraryDatamap.put("colExternalChgDTO", colExternalChgDTO);
			itineraryDatamap.put("cabinClassesLegend",
					ReservationApiUtils.getCabinClassesLegend(itineraryLayoutModesDTO.isIncludeClassOfService()));
			itineraryDatamap.put("barcode", generateBarcode(reservationPaxDTO.getPnr(), segCodesOrderByDep));
			itineraryDatamap.put("carrierLegend", strCarrierCodeLegend);
			itineraryDatamap.put("colSeatPaxDTO", colItinerarySeatDTO);
			itineraryDatamap.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
			if (commonTemplatingDTO != null) {
				itineraryDatamap.put("commonTemplatingDTO", commonTemplatingDTO);
			}
			if (resinsurence != null && resinsurence.getPolicyCode() != null) {
				itineraryDatamap.put("policyNo", resinsurence.getPolicyCode().trim());
			} else {
				itineraryDatamap.put("policyNo", "");
			}
			itineraryDatamap.put("logoImageName", itineraryLayoutModesDTO.getLogoImageName());
			itineraryDatamap.put("includeClassOfService", new Boolean(itineraryLayoutModesDTO.isIncludeClassOfService()));
			itineraryDatamap.put("includeThumbnailLogo", new Boolean(itineraryLayoutModesDTO.isIncludeThumbnailLogo()));
			itineraryDatamap.put("colSSRPaxDTO", colItinerarySSRDTO);
			// Itinerary Advertisement Image Path
			itineraryDatamap.put("itineraryAdvertisementImageURL",
					getItineraryAdvertismentResourceURL(itineraryLayoutModesDTO.getLocale(), reservationPaxDTO));
			log.debug("Exit getItineraryMap");
			return itineraryDatamap;
		}
	}

	private static String getItineraryAdvertismentResourceURL(Locale locale, ReservationPaxDTO reservationPaxDTO) {

		HTTPResourceStatusLookupUtil resourceLookup = HTTPResourceStatusLookupUtil.getInstance();
		String resourceHomeUrl = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ITINERARY_ADVERTISEMENT_IMAGE_URL);
		String itilanguage = locale.getLanguage();
		String resourcePrefix = "";
		String resourceURL = "";

		if (resourceHomeUrl == null || resourceHomeUrl.trim().equals("")) {
			return "";
		}

		if (itilanguage == null || itilanguage.equals("")) {
			itilanguage = "EN";
		}

		String[] languages = { itilanguage, "EN" };

		Collection<ReservationSegmentDTO> colReservationDTO = reservationPaxDTO.getSegments();

		if (colReservationDTO != null && colReservationDTO.size() > 0) {
			Iterator<ReservationSegmentDTO> iReservationSegmentPaxDTO = colReservationDTO.iterator();
			ReservationSegmentDTO reservationSegmentDTO;
			StringBuffer segmentsBuff = new StringBuffer("");

			while (iReservationSegmentPaxDTO.hasNext()) {
				reservationSegmentDTO = (ReservationSegmentDTO) iReservationSegmentPaxDTO.next();
				segmentsBuff.append(reservationSegmentDTO.getSegmentCode() + "/");
			}

			String segments[] = segmentsBuff.toString().split("/");

			for (int index = 0; index < 2; index++) {
				resourcePrefix = "DEP_" + segments[0].toUpperCase() + "_" + languages[index].toUpperCase()
						+ AppSysParamsUtil.getStaticFileSuffix();
				resourceURL = resourceLookup.availableResource(resourcePrefix);

				if (!resourceURL.equals("")) {
					break;
				}

				resourcePrefix = "ARR_" + segments[segments.length - 1].toUpperCase() + "_" + languages[index].toUpperCase()
						+ AppSysParamsUtil.getStaticFileSuffix();
				resourceURL = resourceLookup.availableResource(resourcePrefix);

				if (!resourceURL.equals("")) {
					break;
				}
			}
		}

		return resourceURL;
	}

	private static String getAvailableBalance(Reservation reservation) throws ModuleException {
		BigDecimal avlConvertedBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		String selectedCurrency = "";
		StringBuilder strAvailBal = new StringBuilder();

		if (reservation.getLastCurrencyCode() != null && reservation.getLastCurrencyCode() != "") {
			selectedCurrency = reservation.getLastCurrencyCode();
		} else {
			selectedCurrency = AppSysParamsUtil.getBaseCurrency();
		}

		BigDecimal avlBalance = reservation.getTotalAvailableBalance();

		try {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);
			BigDecimal exchangeRate = currExRate.getMultiplyingExchangeRate();

			if (currExRate != null && avlBalance.abs().doubleValue() > 0) {
				Currency currency = currExRate.getCurrency();
				avlConvertedBalance = AccelAeroRounderPolicy.convertAndRound(avlBalance, exchangeRate,
						currency.getBoundryValue(), currency.getBreakPoint());
			}

			strAvailBal.append(avlConvertedBalance.toString() + " " + selectedCurrency);
		} catch (ModuleException e) {
			log.warn("No exchange rate defined for currency " + selectedCurrency, e);
		}

		return strAvailBal.toString();
	}

	private static String getSpecifiedCurrencyCode(Agent ownerAgent, String baseCurrencyCode) {

		if (ownerAgent != null && !ownerAgent.getCurrencyCode().equals(baseCurrencyCode)) {
			return ownerAgent.getCurrencyCode();
		}

		return null;
	}

	private static String getTotalPaidAmountInPayCurrency(Collection<ReservationPax> passengers) throws ModuleException {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		Iterator<ReservationPax> itReservationPax = passengers.iterator();
		ReservationPax reservationPax;
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				pnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}

		Map<Integer, Collection<ReservationTnx>> mapTnxs = ReservationBO.getPNRPaxPaymentsAndRefunds(pnrPaxIds, false, true);
		Map<String, BigDecimal> currencyWisePayAmounts = new HashMap<String, BigDecimal>();
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown;

		for (Collection<ReservationTnx> tnx : mapTnxs.values()) {
			for (ReservationTnx reservationTnx : tnx) {
				reservationPaxTnxBreakdown = reservationTnx.getReservationPaxTnxBreakdown();

				if (reservationPaxTnxBreakdown != null) {
					BigDecimal amount = currencyWisePayAmounts.get(reservationPaxTnxBreakdown.getPaymentCurrencyCode());

					if (amount != null) {
						amount = AccelAeroCalculator.add(amount, reservationPaxTnxBreakdown.getTotalPriceInPayCurrency());
						currencyWisePayAmounts.put(reservationPaxTnxBreakdown.getPaymentCurrencyCode(), amount);
					} else {
						currencyWisePayAmounts.put(reservationPaxTnxBreakdown.getPaymentCurrencyCode(),
								reservationPaxTnxBreakdown.getTotalPriceInPayCurrency());
					}

				} else {
					BigDecimal amount = currencyWisePayAmounts.get(baseCurrencyCode);

					if (amount != null) {
						amount = AccelAeroCalculator.add(amount, reservationTnx.getAmount());
						currencyWisePayAmounts.put(baseCurrencyCode, amount);
					} else {
						currencyWisePayAmounts.put(baseCurrencyCode, reservationTnx.getAmount());
					}
				}
			}
		}

		StringBuilder strAmounts = new StringBuilder();
		for (String currencyCode : currencyWisePayAmounts.keySet()) {
			BigDecimal amount = currencyWisePayAmounts.get(currencyCode).abs();

			if (amount.doubleValue() != 0d) {
				if (strAmounts.length() == 0) {
					strAmounts.append(amount.toString() + " " + currencyCode);
				} else {
					strAmounts.append(" + " + amount.toString() + " " + currencyCode);
				}
			}
		}

		return strAmounts.toString();
	}

	// private static Collection<SeatPaxDTO> getSeatInformation(Reservation reservation) {
	//
	// Collection<SeatPaxDTO> colSeatDetails = new ArrayList<SeatPaxDTO>();
	// SeatPaxDTO paxSeat = null;
	// Collection<PaxSeatTO> colSeat = reservation.getSeats();
	// PaxSeatTO paxseatTo = null;
	// if (colSeat != null) {
	// Iterator<PaxSeatTO> seatIter = colSeat.iterator();
	// while (seatIter.hasNext()) {
	// paxseatTo = seatIter.next();
	// paxSeat = new SeatPaxDTO();
	// paxSeat.setSeatCode(paxseatTo.getSeatCode());
	// paxSeat.setAmount(paxseatTo.getChgDTO().getAmount());
	// ReservationSegmentDTO seg = getSegmentDTO(reservation.getSegmentsView(), paxseatTo.getPnrSegId());
	// // String strSegCode = getSegmentCode(reservation.getSegmentsView(), paxseatTo.getPnrSegId());
	// if (seg != null) {
	// String strSegCode = seg.getSegmentCode();
	// // Haider 1Mar09
	// if (AppSysParamsUtil.isHideStopOverEnabled() && strSegCode != null) {
	// String newSeg = "";
	// String[] segArr = strSegCode.split("/");
	// for (int i = 0; i < segArr.length; i++) {
	// if (segArr.length > 2 && i > 0 && i < (segArr.length - 1))
	// continue;
	// if (i > 0)
	// newSeg += "/";
	// newSeg += segArr[i];
	// }
	// strSegCode = newSeg;
	// }
	//
	// paxSeat.setSegment(strSegCode);
	// paxSeat.setPassenger(getPassengerName(reservation.getPassengers(), paxseatTo.getPnrPaxId()));
	// paxSeat.setFlightNumber(seg.getFlightNo());
	// paxSeat.setDepartureDate(seg.getDepartureStringDate("ddMMMyy HH:mm"));
	// colSeatDetails.add(paxSeat);
	// }
	// }
	//
	// }
	//
	// return colSeatDetails;
	// }

	/**
	 * Gets the Meal \ Seat \ Airport Services Details for Itinerary
	 * 
	 * @param reservation
	 * @return
	 */
	private static PaxSSRInfoDTO getAdditionalServiceInformation(Reservation reservation, boolean isAirportService)
			throws ModuleException {

		Collection<PaxSSRDTO> colSSRDetails = new ArrayList<PaxSSRDTO>();
		PaxSSRDTO paxssrDto = null;
		Collection<PaxSeatTO> colSeat = reservation.getSeats();
		Collection<PaxMealTO> colMeal = reservation.getMeals();
		Collection<ReservationPax> colPax = reservation.getPassengers();
		Collection<ReservationSegmentDTO> colSegs = reservation.getSegmentsView();
		PaxSSRInfoDTO paxInfoDto = new PaxSSRInfoDTO();

		for (ReservationPax resPax : colPax) {
			Integer paxId = resPax.getPnrPaxId();
			String strPax = "";
			if (resPax.getTitle() != null) {
				strPax = resPax.getTitle() + " " + resPax.getFirstName() + " " + resPax.getLastName();
			} else {
				strPax = resPax.getFirstName() + " " + resPax.getLastName();
			}
			for (ReservationSegmentDTO segDto : colSegs) {
				if (segDto.getStatus().equals(ReservationSegmentStatus.CANCEL)) {
					continue;
				}
				paxssrDto = new PaxSSRDTO();
				paxssrDto.setSeatCode("");
				paxssrDto.setMealCode("");

				if (!isAirportService) {
					List<String> additionalSeatCodes = new ArrayList<String>();
					for (PaxSeatTO paxSeat : colSeat) {
						if (paxId.intValue() == paxSeat.getPnrPaxId().intValue()
								&& segDto.getPnrSegId().intValue() == paxSeat.getPnrSegId().intValue()) {
							paxInfoDto.setHasSeats(true);
							if (paxssrDto.getSeatCode() == null || paxssrDto.getSeatCode().equals("")) {
								paxssrDto.setPnrSegId(paxSeat.getPnrSegId());
								paxssrDto.setSeatCode(paxSeat.getSeatCode());
							} else {
								paxInfoDto.setHasAdditionalSeats(true);
								additionalSeatCodes.add(paxSeat.getSeatCode());
							}
						}
					}
					paxssrDto.setAdditionalSeatCodes(additionalSeatCodes);

					for (PaxMealTO paxMeal : colMeal) {
						if (paxId.intValue() == paxMeal.getPnrPaxId().intValue()
								&& segDto.getPnrSegId().intValue() == paxMeal.getPnrSegId().intValue()) {
							paxInfoDto.setHasMeals(true);
							paxssrDto.setPnrSegId(paxMeal.getPnrSegId());
							paxssrDto.setMealCode(paxMeal.getMealCode());
						}
					}
				} else {
					// multi SSR info
					if (resPax.getPaxSSR() != null && !resPax.getPaxSSR().isEmpty()) {
						Collection<PaxSSRDTO> colSSrDTO = resPax.getPaxSSR();
						Collection<PaxSSRDetailDTO> colPaxSSRDetial = new ArrayList<PaxSSRDetailDTO>();
						// boolean validSSR =
						for (PaxSSRDTO passengerSSRDetail : colSSrDTO) {
							if ((passengerSSRDetail.getPnrSegId().compareTo(segDto.getPnrSegId()) == 0)
									&& (passengerSSRDetail.getSsrSubCatID().intValue() == 1 || passengerSSRDetail
											.getSsrSubCatID().intValue() == 2)
									&& ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(passengerSSRDetail.getStatus())) {
								PaxSSRDetailDTO paxSSRDetailDTO = new PaxSSRDetailDTO();

								paxSSRDetailDTO.setPnrPaxId(passengerSSRDetail.getPnrPaxId());
								paxSSRDetailDTO.setSsrId(passengerSSRDetail.getSsrId());
								paxSSRDetailDTO.setContextId(passengerSSRDetail.getContextId());
								paxSSRDetailDTO.setSsrText(passengerSSRDetail.getSsrText());
								paxSSRDetailDTO.setChargeAmount(passengerSSRDetail.getChargeAmount());
								paxSSRDetailDTO.setSsrCode(passengerSSRDetail.getSsrCode());
								paxSSRDetailDTO.setSsrDesc(passengerSSRDetail.getSsrDesc());
								paxssrDto.setPnrSegId(passengerSSRDetail.getPnrSegId());
								colPaxSSRDetial.add(paxSSRDetailDTO);

							}
						}
						paxssrDto.setColPaxSSRDetailDTO(colPaxSSRDetial);
						// paxssrDto.setColPaxSSRDetailDTO(resPax.getPaxSSR());
						if (!colPaxSSRDetial.isEmpty()) {
							paxInfoDto.setHasAirportService(true);
						}

					}
				}

				if (paxssrDto.getPnrSegId() != null
						&& (paxInfoDto.isHasMeals() || paxInfoDto.isHasSeats() || paxInfoDto.isHasAirportService())) {
					paxssrDto.setPassenger(strPax);

					String strSegCode = segDto.getSegmentCode();
					// Haider 1Mar09
					if (AppSysParamsUtil.isHideStopOverEnabled() && strSegCode != null) {
						String newSeg = "";
						String[] segArr = strSegCode.split("/");
						for (int i = 0; i < segArr.length; i++) {
							if (segArr.length > 2 && i > 0 && i < (segArr.length - 1))
								continue;
							if (i > 0)
								newSeg += "/";
							newSeg += segArr[i];
						}
						strSegCode = newSeg;
					}
					paxssrDto.setSegment(strSegCode);
					paxssrDto.setFlightNo(segDto.getFlightNo());
					paxssrDto.setDepartureDate(segDto.getDepartureStringDate("ddMMMyy HH:mm"));

					colSSRDetails.add(paxssrDto);
				}
			}

		}
		paxInfoDto.setSsrDto(colSSRDetails);
		return paxInfoDto;
	}

	/**
	 * method to get the segment for seta details
	 * 
	 * @param colSegs
	 * @param segId
	 * @return
	 */
	// private static String getSegmentCode(Collection<ReservationSegmentDTO> colSegs, Integer segId) {
	// String strSegCode = "";
	// ReservationSegmentDTO seg = null;
	// if (colSegs != null) {
	// Iterator<ReservationSegmentDTO> segiter = colSegs.iterator();
	// while (segiter.hasNext()) {
	// seg = segiter.next();
	// if (segId.intValue() == seg.getPnrSegId().intValue()) {
	// return seg.getSegmentCode();
	// }
	// }
	// }
	//
	// return strSegCode;
	// }

	/**
	 * method to get the segment DTO
	 * 
	 * @param colSegs
	 * @param segId
	 * @return
	 */
	// private static ReservationSegmentDTO getSegmentDTO(Collection<ReservationSegmentDTO> colSegs, Integer segId) {
	// ReservationSegmentDTO seg = null;
	// if (colSegs != null) {
	// Iterator<ReservationSegmentDTO> segiter = colSegs.iterator();
	// while (segiter.hasNext()) {
	// seg = segiter.next();
	// if (segId.intValue() == seg.getPnrSegId().intValue()) {
	// return seg;
	// }
	// }
	// }
	//
	// return null;
	// }

	/**
	 * Method to get the pax name for seat details
	 * 
	 * @param colPax
	 * @param paxId
	 * @return
	 */
	// private static String getPassengerName(Collection<ReservationPax> colPax, Integer paxId) {
	// String strPax = "";
	// ReservationPax pax = null;
	// if (colPax != null) {
	// Iterator<ReservationPax> paxIter = colPax.iterator();
	// while (paxIter.hasNext()) {
	// pax = paxIter.next();
	// if (paxId.intValue() == pax.getPnrPaxId().intValue()) {
	// if (pax.getTitle() != null) {
	// strPax = pax.getTitle() + " " + pax.getFirstName() + " " + pax.getLastName();
	// } else {
	// strPax = pax.getFirstName() + " " + pax.getLastName();
	// }
	// return strPax;
	// }
	// }
	// }
	// return strPax;
	// }

	/**
	 * Returns the external charges
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<Serializable> getExternalCharges(Reservation reservation, Map<Integer, Integer> adultAndInfantMap,
			CredentialsDTO credentialsDTO) throws ModuleException {
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = getExternalChargesMap(credentialsDTO);
		ExternalChgDTO handlingChargeDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
		BigDecimal totalHandlingChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxOndCharge reservationPaxOndCharge;

		for (Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = itReservationPax.next();

			if (adultAndInfantMap == null || adultAndInfantMap.containsKey(reservationPax.getPnrPaxId())) {
				for (Iterator<ReservationPaxFare> itReservationPaxFare = reservationPax.getPnrPaxFares().iterator(); itReservationPaxFare
						.hasNext();) {
					reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

					for (Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge = reservationPaxFare.getCharges().iterator(); itReservationPaxOndCharge
							.hasNext();) {
						reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();

						if (reservationPaxOndCharge.getChargeRateId() != null
								&& handlingChargeDTO.isChargeRateExist(reservationPaxOndCharge.getChargeRateId())) {
							totalHandlingChargeAmount = AccelAeroCalculator.add(totalHandlingChargeAmount,
									reservationPaxOndCharge.getAmount());
						}
					}
				}
			}
		}

		Collection<Serializable> colExternalChgDTO = new ArrayList<Serializable>();

		if (totalHandlingChargeAmount.doubleValue() > 0) {
			handlingChargeDTO.setAmount(totalHandlingChargeAmount);
			colExternalChgDTO.add(handlingChargeDTO);
		}

		return colExternalChgDTO;
	}

	/**
	 * Load the external charges map based on the external charge type Currently on the handling charge is shown
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private static Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap(CredentialsDTO credentialsDTO)
			throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);

		return ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MAKE_ONLY);
	}

	/**
	 * Encrypts a given string
	 * 
	 * @param msg
	 * @return
	 * @throws ModuleException
	 */
	private static String encrypt(String msg) throws ModuleException {
		return ReservationModuleUtils.getCryptoServiceBD().encrypt(msg);
	}

	/**
	 * Generates the barcode <img ... /img> URL
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public static String generateBarcode(String pnr, List<String> segmentsCodesOrderdByDep) throws ModuleException {
		Map<String, String> barCodeConfMap = config.getBarCodeConfMap();
		String showBarcode = (String) barCodeConfMap.get("showBarcode");

		StringBuilder URL = new StringBuilder();

		// create URL
		if (showBarcode.equalsIgnoreCase("true")) {
			String carrierCode = AppSysParamsUtil.getCarrierCode();

			String barcodeType = (String) barCodeConfMap.get("barcodeType");
			String humanReadable = (String) barCodeConfMap.get("humanReadable");
			String format = (String) barCodeConfMap.get("format");
			String resolution = (String) barCodeConfMap.get("resolution");
			String height = (String) barCodeConfMap.get("height");
			String path = PlatformUtiltiies.nullHandler(CommonsServices.getGlobalConfig().getBizParam(
					SystemParamKeys.ACCELAERO_IBE_URL))
					+ "/public";
			String showAirline = (String) barCodeConfMap.get("showAirline");
			String mw = (String) barCodeConfMap.get("widthFactor");
			String showSegments = (String) barCodeConfMap.get("showSegments");
			String showEticket = (String) barCodeConfMap.get("showEticket");
			String showPnr = (String) barCodeConfMap.get("showPnr");
			StringBuilder barcodeMsg = new StringBuilder();
			if (showAirline.equalsIgnoreCase("true")) {
				barcodeMsg.append(carrierCode);
			}
			if (showPnr.equalsIgnoreCase("true")) {
				barcodeMsg.append(pnr);
			} else if (showPnr.equalsIgnoreCase("false") && showEticket.equalsIgnoreCase("false")) {
				barcodeMsg.append(pnr);
			}

			if (showSegments.equalsIgnoreCase("true")) {
				StringBuilder strSeg = new StringBuilder();
				for (String segCode : segmentsCodesOrderdByDep) {
					strSeg.append(segCode);
				}
				barcodeMsg.append(strSeg.toString());
			}
			String enptBarCodeMsg = encrypt(barcodeMsg.toString());
			String humanReadablePhaseEnableOrNot = encrypt(humanReadable);

			try {
				enptBarCodeMsg = URLEncoder.encode(enptBarCodeMsg, "UTF-8");
				humanReadablePhaseEnableOrNot = URLEncoder.encode(humanReadablePhaseEnableOrNot, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				log.error("Barcode could not encode messege or parameter ");
				throw new ModuleException("airreservations.barcode.encodingNotSupported");
			}

			URL.append("<img src='").append(path).append("/genbc?type=").append(barcodeType).append("&msg=")
					.append(enptBarCodeMsg).append("&hrp=").append(humanReadablePhaseEnableOrNot).append("&fmt=").append(format)
					.append("&res=").append(resolution).append("&height=").append(height).append("&mw=").append(mw).append("'/>");

			log.debug("Barcode URL created");
		}

		return URL.toString();
	}

	/**
	 * Check validity of selected passengers
	 * 
	 * @param reservation
	 * @param colSelectedPnrPaxIds
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, Integer> checkValidityOfSelectedPassengers(Reservation reservation,
			Collection<Integer> colSelectedPnrPaxIds) throws ModuleException {

		Map<Integer, Integer> adultAndInfantMap = null;

		if (colSelectedPnrPaxIds != null) {
			if (colSelectedPnrPaxIds.size() == 0) {
				throw new ModuleException("airreservations.emailItinerary.paxNotLocated");
			} else {
				adultAndInfantMap = new HashMap<Integer, Integer>();
				ReservationPax reservationPax;
				for (Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator(); itReservationPax
						.hasNext();) {
					reservationPax = itReservationPax.next();

					if (colSelectedPnrPaxIds.contains(reservationPax.getPnrPaxId())) {
						// Parent
						if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
							adultAndInfantMap.put(reservationPax.getPnrPaxId(), reservationPax.getAccompaniedPaxId());
							// Adult
						} else if (ReservationApiUtils.isSingleAfterSave(reservationPax)) {
							adultAndInfantMap.put(reservationPax.getPnrPaxId(), null);
							// Child
						} else if (ReservationApiUtils.isChildType(reservationPax)) {
							adultAndInfantMap.put(reservationPax.getPnrPaxId(), null);
						}
					}
				}
			}
		}

		return adultAndInfantMap;
	}

	/**
	 * Return payment details
	 * 
	 * @param reservation
	 * @param lstNominalCodes
	 * @return Collection of PaymentDetailsDTO
	 */
	private static Collection<PaymentDetailDTO> getPaymentDetails(Reservation reservation, List<Integer> lstNominalCodes) {
		log.debug("Inside getPaymentDetails");

		// Capturing the adults and parents
		ReservationPax reservationPax;
		String fullName;
		Map<Integer, String> mapPaxIdAndName = new HashMap<Integer, String>();

		for (Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = itReservationPax.next();

			// If it's an adult
			if (ReservationApiUtils.isAdultType(reservationPax)) {
				fullName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), false);

				mapPaxIdAndName.put(reservationPax.getPnrPaxId(), fullName);
				// If it's a child
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				fullName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), true);

				mapPaxIdAndName.put(reservationPax.getPnrPaxId(), fullName);
			}
		}

		ReservationPaymentDAO reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		Map<Integer, Collection<PaymentDetailDTO>> mapPnrPaxIdsAndColPaymentDetailDTO = reservationPaymentDAO
				.getDetailPNRPaxPayments(mapPaxIdAndName.keySet(), lstNominalCodes);

		Iterator<Integer> itMapPaxIdAndName = mapPaxIdAndName.keySet().iterator();
		Integer pnrPaxId;
		String passengerName;
		Collection<PaymentDetailDTO> colPaymentDetailDTO;

		PaymentDetailDTO paymentDetailDTO;

		Collection<PaymentDetailDTO> colAllPaymentDetailDTO = new ArrayList<PaymentDetailDTO>();
		Collection<Integer> colTnxIds = new HashSet<Integer>();

		// Assembling the PaymentDetailDTO with basic information
		while (itMapPaxIdAndName.hasNext()) {
			pnrPaxId = itMapPaxIdAndName.next();
			passengerName = mapPaxIdAndName.get(pnrPaxId);
			colPaymentDetailDTO = (Collection<PaymentDetailDTO>) mapPnrPaxIdsAndColPaymentDetailDTO.get(pnrPaxId);

			if (colPaymentDetailDTO != null) {
				for (Iterator<PaymentDetailDTO> itColPaymentDetailDTO = colPaymentDetailDTO.iterator(); itColPaymentDetailDTO
						.hasNext();) {
					paymentDetailDTO = (PaymentDetailDTO) itColPaymentDetailDTO.next();
					paymentDetailDTO.setPassengerName(passengerName);

					colTnxIds.add(paymentDetailDTO.getTnxId());
					colAllPaymentDetailDTO.add(paymentDetailDTO);
				}
			}
		}

		if (colTnxIds.size() > 0) {
			// Assembling the PaymentDetailDTO with authorization ids
			Map<Integer, String> mapTnxIdAndAuthorizationId = reservationPaymentDAO.getAuthorizationIdsForCCPayments(colTnxIds);
			String authorizationId;

			for (Iterator<PaymentDetailDTO> itColPaymentDetailDTO = colAllPaymentDetailDTO.iterator(); itColPaymentDetailDTO
					.hasNext();) {
				paymentDetailDTO = itColPaymentDetailDTO.next();
				authorizationId = (String) mapTnxIdAndAuthorizationId.get(paymentDetailDTO.getTnxId());
				paymentDetailDTO.setAuthorizationId(BeanUtils.nullHandler(authorizationId));
			}
		}

		log.debug("Exit getPaymentDetails");
		return colAllPaymentDetailDTO;
	}

	/**
	 * Return charge details
	 * 
	 * @param reservation
	 * @return collection of ChargesDetailDTO
	 * @throws ModuleException
	 */
	protected static Collection<ChargesDetailDTO> getChargeDetails(Reservation reservation, String prefferedLanguage)
			throws ModuleException {
		log.debug("Inside getChargeDetails");

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxOndCharge reservationPaxOndCharge;
		ReservationPaxFareSegment reservationPaxFareSegment;
		ChargesDetailDTO chargesDetailDTO;
		Collection<ChargesDetailDTO> colChargesDetailDTO = new ArrayList<ChargesDetailDTO>();
		Collection<Integer> colChargeRateIds = new HashSet<Integer>();
		Collection<String> colAirportCodes = new HashSet<String>();
		String ondCode;
		String passengerName;
		String bookingCode;

		Map<Integer, ChargesDetailDTO> chargeRateIdMap = new HashMap<Integer, ChargesDetailDTO>();
		// Map<Integer,String> chargeRateIdMap = new HashMap<Integer, String>();
		for (ReservationPax reservationPassenger : reservation.getPassengers()) {
			if (reservationPassenger.getOndChargesView() != null) {
				for (ChargesDetailDTO chargeDetailsDTO : reservationPassenger.getOndChargesView()) {
					chargeRateIdMap.put(chargeDetailsDTO.getChargeRateId(), chargeDetailsDTO);
				}
			}
		}

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			passengerName = "";

			// If it's an adult
			if (ReservationApiUtils.isAdultType(reservationPax)) {
				passengerName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), false);
				// If it's a child
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				passengerName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), true);
				// If it's an infant
			} else if (ReservationApiUtils.isInfantType(reservationPax)) {
				passengerName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), true);
			}

			// Navigate through passengers
			for (Iterator<ReservationPaxFare> itReservationPaxFare = reservationPax.getPnrPaxFares().iterator(); itReservationPaxFare
					.hasNext();) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();
				ondCode = ReservationApiUtils.getOndCode(reservationPaxFare);
				colAirportCodes.addAll(Arrays.asList(ondCode.split("/")));
				String subStatus = null;
				// Pick Booking Codes
				Collection<String> colBookingCodes = new HashSet<String>();
				for (Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments()
						.iterator(); itReservationPaxFareSegment.hasNext();) {
					reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();
					bookingCode = BeanUtils.nullHandler(reservationPaxFareSegment.getBookingCode());
					subStatus = reservationPaxFareSegment.getSegment().getSubStatus();
					if (!bookingCode.equals("")) {
						colBookingCodes.add(bookingCode);
					}
				}
				Collection<String> exchangeExcludeChargeCodes = null;
				// Capturing the ChargesDetailDTO
				for (Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge = reservationPaxFare.getCharges().iterator(); itReservationPaxOndCharge
						.hasNext();) {
					reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();

					// Hide exchanged charges
					if (AppSysParamsUtil.hideExchangesCharges()
							&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equalsIgnoreCase(subStatus)) {
						if (exchangeExcludeChargeCodes == null) {
							exchangeExcludeChargeCodes = CancellationUtils.getExchangeExcludeChargeCodes();
						}
						if (reservationPaxOndCharge.getChargeRateId() == null
								|| (!chargeRateIdMap.isEmpty() && !exchangeExcludeChargeCodes.contains((chargeRateIdMap
										.get(reservationPaxOndCharge.getChargeRateId())).getChargeCode()))) {
							continue;
						}
					}

					chargesDetailDTO = new ChargesDetailDTO();
					chargesDetailDTO.setPassengerName(passengerName);
					chargesDetailDTO.setPnrPaxId(reservationPax.getPnrPaxId());
					chargesDetailDTO.setChargeDate(reservationPaxOndCharge.getLocalChargeDate());
					chargesDetailDTO.setChargeAmount(reservationPaxOndCharge.getAmount());
					chargesDetailDTO.setChargeGroupCode(reservationPaxOndCharge.getChargeGroupCode());
					chargesDetailDTO.setOndCode(ondCode);
					chargesDetailDTO.setBookingCodes(colBookingCodes);
					chargesDetailDTO.setChargeDate(reservationPaxOndCharge.getZuluChargeDate());

					if (reservationPaxOndCharge.getChargeRateId() != null) {
						colChargeRateIds.add(reservationPaxOndCharge.getChargeRateId());
						chargesDetailDTO.setChargeRateId(reservationPaxOndCharge.getChargeRateId());
					}

					colChargesDetailDTO.add(chargesDetailDTO);
				}
			}
		}

		AirportBD airportBD = ReservationModuleUtils.getAirportBD();
		Map<String, CachedAirportDTO> mapAirportCodes = airportBD.getCachedOwnAirportMap(colAirportCodes);

		FareBD fareBD = ReservationModuleUtils.getFareBD();
		ChargeBD chargeBD = ReservationModuleUtils.getChargeBD();
		Map<String, String> mapChargeGroups = chargeBD.getChargeGroups();
		FaresAndChargesTO faresAndChargesTO = fareBD.getRefundableStatuses(null, colChargeRateIds, prefferedLanguage);
		String chgGrpCodeDescription;
		ChargeTO chargeTO;

		// Assign the Charge Descriptions and Segment Descriptions
		for (Iterator<ChargesDetailDTO> itColChargesDetailDTO = colChargesDetailDTO.iterator(); itColChargesDetailDTO.hasNext();) {
			chargesDetailDTO = itColChargesDetailDTO.next();

			// Setting the segment descriptions
			String composedSegment = "";
			String[] segments = chargesDetailDTO.getOndCode().split("/");

			for (int i = 0; i < segments.length; i++) {
				CachedAirportDTO airport = mapAirportCodes.get(segments[i]);
				if (i != 0) {
					composedSegment += " / ";
				}
				composedSegment += airport.getAirportName();
			}
			chargesDetailDTO.setOndDescription(composedSegment);

			// Setting the charge descriptions
			chgGrpCodeDescription = (String) mapChargeGroups.get(chargesDetailDTO.getChargeGroupCode());
			chargesDetailDTO.setChargeType(chgGrpCodeDescription);

			if (chargesDetailDTO.getChargeRateId() != null) {
				chargeTO = (ChargeTO) faresAndChargesTO.getChargeTOs().get(chargesDetailDTO.getChargeRateId());
				chargesDetailDTO.setChargeCode(chargeTO.getChargeCode());
				chargesDetailDTO.setChargeDescription(chargeTO.getChargeDescription());
			} else {
				chargesDetailDTO.setChargeCode(chargesDetailDTO.getChargeGroupCode());
				bookingCode = BeanUtils.nullHandler(chargesDetailDTO.getBookingCode());

				// If it's an adult or a child
				if (!bookingCode.equals("")) {
					chargesDetailDTO.setChargeDescription(chargesDetailDTO.getChargeType() + "/" + bookingCode);
					// If it's an infant
				} else {
					chargesDetailDTO.setChargeDescription(chargesDetailDTO.getChargeType());
				}
			}
		}

		log.debug("Exit getChargeDetails");
		return colChargesDetailDTO;
	}

	/**
	 * Assemble detail charges
	 * 
	 * @param colChargesDetailDTO
	 * @return
	 */
	private static Collection<Serializable> assembleCharges(Collection<ChargesDetailDTO> colChargesDetailDTO) {
		log.debug("Inside assembleCharges");

		ChargesDetailDTO chargesDetailDTO;
		Collection<ChargesDetailDTO> ondSpecificCharges;
		BigDecimal chargeAmount;
		Map<String, Collection<ChargesDetailDTO>> ondCharges = new HashMap<String, Collection<ChargesDetailDTO>>();
		Map<String, BigDecimal> ondChargeAmounts = new HashMap<String, BigDecimal>();

		for (Iterator<ChargesDetailDTO> itColChargesDetailDTO = colChargesDetailDTO.iterator(); itColChargesDetailDTO.hasNext();) {
			chargesDetailDTO = itColChargesDetailDTO.next();

			if (ondCharges.containsKey(chargesDetailDTO.getOndDescription())) {
				ondSpecificCharges = ondCharges.get(chargesDetailDTO.getOndDescription());
				chargeAmount = ondChargeAmounts.get(chargesDetailDTO.getOndDescription());

				ondSpecificCharges.add(chargesDetailDTO);
				ondChargeAmounts.put(chargesDetailDTO.getOndDescription(),
						AccelAeroCalculator.add(chargeAmount, chargesDetailDTO.getChargeAmount()));
			} else {
				ondSpecificCharges = new ArrayList<ChargesDetailDTO>();
				ondSpecificCharges.add(chargesDetailDTO);

				ondCharges.put(chargesDetailDTO.getOndDescription(), ondSpecificCharges);
				ondChargeAmounts.put(chargesDetailDTO.getOndDescription(), chargesDetailDTO.getChargeAmount());
			}
		}

		ItineraryChargesDTO itineraryChargesDTO;
		String ondDescription;
		Collection<Serializable> colItineraryChargesDTO = new ArrayList<Serializable>();

		for (Iterator<String> itOndInformation = ondCharges.keySet().iterator(); itOndInformation.hasNext();) {
			ondDescription = itOndInformation.next();

			itineraryChargesDTO = new ItineraryChargesDTO();
			itineraryChargesDTO.setOndDescription(ondDescription);
			itineraryChargesDTO.setColChargesDetailDTO(ondCharges.get(ondDescription));
			itineraryChargesDTO.setOndChargeAmount(ondChargeAmounts.get(ondDescription));

			colItineraryChargesDTO.add(itineraryChargesDTO);
		}

		log.debug("Exit assembleCharges");
		return colItineraryChargesDTO;
	}

	/**
	 * Assemble Payments
	 * 
	 * @param paymentDetails
	 * @return
	 */
	private static Map<Integer, Map<String, ItineraryPaymentsDTO>> assemblePayments(
			Collection<PaymentDetailDTO> colPaymentDetailDTO) {
		log.debug("Inside assemblePayments");

		PaymentDetailDTO paymentDetailDTO = null;
		ItineraryPaymentsDTO itineraryPaymentsDTO = null;

		Map<Integer, Map<String, ItineraryPaymentsDTO>> paymentsByPax = new LinkedHashMap<Integer, Map<String, ItineraryPaymentsDTO>>();
		Map<String, ItineraryPaymentsDTO> paymentsPerPaxByCur = null;
		String strBaseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		String payCurrencyCode = null;
		BigDecimal payCurrencyAmount = null;

		for (Iterator<PaymentDetailDTO> itColPaymentDetailDTO = colPaymentDetailDTO.iterator(); itColPaymentDetailDTO.hasNext();) {
			paymentDetailDTO = itColPaymentDetailDTO.next();

			if (paymentsByPax.containsKey(paymentDetailDTO.getPnrPaxId())) {
				paymentsPerPaxByCur = paymentsByPax.get(paymentDetailDTO.getPnrPaxId());
			} else {
				paymentsPerPaxByCur = new LinkedHashMap<String, ItineraryPaymentsDTO>();
				paymentsByPax.put(paymentDetailDTO.getPnrPaxId(), paymentsPerPaxByCur);
			}

			if (paymentDetailDTO.getPayCurrencyCode() == null || "".equals(paymentDetailDTO.getPayCurrencyCode())) {
				// Assumption : When payCurrencyCode is not present, payment is assumed to be in base currency
				payCurrencyCode = strBaseCurrencyCode;
				payCurrencyAmount = paymentDetailDTO.getAmount();
			} else {
				payCurrencyCode = paymentDetailDTO.getPayCurrencyCode();
				payCurrencyAmount = paymentDetailDTO.getPayCurrencyAmount();
			}

			if (paymentsPerPaxByCur.containsKey(payCurrencyCode)) {
				itineraryPaymentsDTO = paymentsPerPaxByCur.get(payCurrencyCode);
			} else {
				itineraryPaymentsDTO = new ItineraryPaymentsDTO();
				itineraryPaymentsDTO.setPaymentCurrencyCode(payCurrencyCode);
				paymentsPerPaxByCur.put(payCurrencyCode, itineraryPaymentsDTO);
			}

			itineraryPaymentsDTO.addPaymentDetailDTO(paymentDetailDTO);
			itineraryPaymentsDTO.setPaxPaymentAmount(AccelAeroCalculator.add(itineraryPaymentsDTO.getPaxPaymentAmount(),
					paymentDetailDTO.getAmount()));
			itineraryPaymentsDTO.setPaxPaymentAmountInPayCurrency(AccelAeroCalculator.add(
					itineraryPaymentsDTO.getPaxPaymentAmountInPayCurrency(), payCurrencyAmount));
		}

		log.debug("Exit assemblePayments");
		return paymentsByPax;
	}

	/**
	 * Return itinerary specific payment information
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, Map<String, ItineraryPaymentsDTO>> getItineraryPaymentsDTOs(Reservation reservation,
			Map<Integer, Integer> adultAndInfantMap) throws ModuleException {
		return assemblePayments(filterPassengersFromPayments(
				getPaymentDetails(reservation, ReservationTnxNominalCode.getPaymentAndRefundNominalCodes()), adultAndInfantMap));
	}

	/**
	 * Returns itinerary specific payment information for a given nominal codes
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, Map<String, ItineraryPaymentsDTO>> getItineraryCreditCardPaymentsOnly(Reservation reservation,
			Map<Integer, Integer> adultAndInfantMap) throws ModuleException {
		return assemblePayments(filterPassengersFromPayments(
				getPaymentDetails(reservation, ReservationTnxNominalCode.getCreditCardNominalCodes()), adultAndInfantMap));
	}

	/**
	 * Return itinerary specific charge information
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<Serializable> getItineraryChargesDTOs(Reservation reservation,
			Map<Integer, Integer> adultAndInfantMap, String prefferedLanguage) throws ModuleException {
		return assembleCharges(filterPassengersFromCharges(getChargeDetails(reservation, prefferedLanguage), adultAndInfantMap));
	}

	/**
	 * Filter passengers from charges
	 * 
	 * @param colChargesDetailDTO
	 * @param adultAndInfantMap
	 * @return
	 */
	private static Collection<ChargesDetailDTO> filterPassengersFromCharges(Collection<ChargesDetailDTO> colChargesDetailDTO,
			Map<Integer, Integer> adultAndInfantMap) {
		if (adultAndInfantMap != null) {
			Collection<Integer> colAllPassengerIds = new ArrayList<Integer>();
			Integer pnrPassengerId;
			Integer pnrInfantId;

			for (Iterator<Integer> itPnrPaxId = adultAndInfantMap.keySet().iterator(); itPnrPaxId.hasNext();) {
				pnrPassengerId = itPnrPaxId.next();
				pnrInfantId = adultAndInfantMap.get(pnrPassengerId);
				colAllPassengerIds.add(pnrPassengerId);

				if (pnrInfantId != null) {
					colAllPassengerIds.add(pnrInfantId);
				}
			}

			Collection<ChargesDetailDTO> colFilteredChargesDetailDTO = new ArrayList<ChargesDetailDTO>();
			ChargesDetailDTO chargesDetailDTO;

			for (Iterator<ChargesDetailDTO> itChargeDetailDTO = colChargesDetailDTO.iterator(); itChargeDetailDTO.hasNext();) {
				chargesDetailDTO = itChargeDetailDTO.next();

				// Adding only the passengers we needed
				if (colAllPassengerIds.contains(chargesDetailDTO.getPnrPaxId())) {
					colFilteredChargesDetailDTO.add(chargesDetailDTO);
				}
			}

			return colFilteredChargesDetailDTO;
		} else {
			return colChargesDetailDTO;
		}
	}

	/**
	 * Filter passengers from payments
	 * 
	 * @param colPaymentDetailDTO
	 * @param adultAndInfantMap
	 * @return
	 */
	private static Collection<PaymentDetailDTO> filterPassengersFromPayments(Collection<PaymentDetailDTO> colPaymentDetailDTO,
			Map<Integer, Integer> adultAndInfantMap) {
		if (adultAndInfantMap != null) {
			Collection<PaymentDetailDTO> colFilteredPaymentDetailDTO = new ArrayList<PaymentDetailDTO>();
			PaymentDetailDTO paymentDetailDTO;

			for (Iterator<PaymentDetailDTO> itPaymentDetailDTO = colPaymentDetailDTO.iterator(); itPaymentDetailDTO.hasNext();) {
				paymentDetailDTO = itPaymentDetailDTO.next();

				if (adultAndInfantMap.keySet().contains(paymentDetailDTO.getPnrPaxId())) {
					colFilteredPaymentDetailDTO.add(paymentDetailDTO);
				}
			}

			return colFilteredPaymentDetailDTO;
		} else {
			return colPaymentDetailDTO;
		}
	}

	/**
	 * Compose the audit
	 * 
	 * @param pnr
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<ReservationAudit> composeAudit(String pnr, CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.EMAIL_ITINERARY.getCode());
		ReservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

		return colReservationAudit;
	}

	/**
	 * Compose carrier Legend
	 * 
	 * @param reservationPaxDTO
	 * @return
	 * @throws ModuleException
	 */
	private static String composeCarrierDescLegend(ReservationPaxDTO reservationPaxDTO) throws ModuleException {

		String carrierLegend = "";

		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_OPERATING_CARRIER_LEGEND))) {
			Map<String, Collection<String>> carrierCodeAndFlightsMap = new HashMap<String, Collection<String>>();
			Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationPaxDTO.getSegments();

			if (colReservationSegmentDTO != null && colReservationSegmentDTO.size() > 0) {
				Iterator<ReservationSegmentDTO> itReservationSegmentDTO = colReservationSegmentDTO.iterator();
				ReservationSegmentDTO reservationSegmentDTO;
				Collection<String> colFlights;
				String carrierCode;
				String strFltNo;

				while (itReservationSegmentDTO.hasNext()) {
					reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
					strFltNo = reservationSegmentDTO.getFlightNo();

					carrierCode = AppSysParamsUtil.extractCarrierCode(strFltNo);

					// add description to exsisting carrier
					if (carrierCodeAndFlightsMap.containsKey(carrierCode)) {
						colFlights = carrierCodeAndFlightsMap.get(carrierCode);
						colFlights.add(strFltNo);

						// add description to new carrier
					} else {
						colFlights = new HashSet<String>();
						colFlights.add(strFltNo);

						carrierCodeAndFlightsMap.put(carrierCode, colFlights);
					}
				}
			}
			carrierLegend = ReservationApiUtils.formatCarrierLegend(carrierCodeAndFlightsMap);
		}

		return carrierLegend;
	}

	/**
	 * Formats the company address in the itinerary
	 * 
	 * @param carrierCode
	 * @return
	 * @throws ModuleException
	 */
	private static String strItineraryAddress(String carrierCode) throws ModuleException {
		if (AppSysParamsUtil.isHideAddressInItinarary()) {
			return "";
		} else {
			String homeUrl = "";
			if (!"".equals(AppSysParamsUtil.getAirlineUrlToDisplayInItinerary(carrierCode))) {
				homeUrl += " <br>" + AppSysParamsUtil.getAirlineUrlToDisplayInItinerary(carrierCode);
				return ReservationApiUtils.getCompanyAddressInformation(carrierCode) + homeUrl;
			} else {
				return ReservationApiUtils.getCompanyAddressInformation(carrierCode);
			}
		}
	}

	private static String advertisementImageForOND(ItineraryDTO itineraryDTO, HTTPResourceStatusLookupUtil resourceLookup,
			String itilanguage) throws ModuleException {

		ItineraryFlightTO previousItineraryFlightTO = null;
		String[] previousflightSegments = null;
		Collection<ItineraryFlightOndGroup> colItineraryDto = itineraryDTO.getFlightOndGroups();
		ArrayList<String> segmentList = new ArrayList<String>();

		for (ItineraryFlightOndGroup ondGrp : colItineraryDto) {

			Iterator<ItineraryFlightTO> iItineraryFlightTO = ondGrp.getFlights().iterator();
			ItineraryFlightTO itineraryFlightTO;

			ArrayList<String> tempSegmentList = new ArrayList<String>();
			String[] currentflightSegments = null;

			while (iItineraryFlightTO.hasNext()) {
				itineraryFlightTO = (ItineraryFlightTO) iItineraryFlightTO.next();
				if (!(previousItineraryFlightTO == null)) {
					currentflightSegments = itineraryFlightTO.getSegmentCode().split("/");
					if (CommonsServices.getGlobalConfig().isHubAirport(previousflightSegments[previousflightSegments.length - 1])) {
						if (currentflightSegments[0].equals(previousflightSegments[previousflightSegments.length - 1])) {
							if (previousItineraryFlightTO.getArrivalDateTimeZulu().compareTo(
									itineraryFlightTO.getDepartureDateTimeZulu()) < 0) {
								Date previousArrivalDate = previousItineraryFlightTO.getArrivalDateTimeZulu();
								Date currentDepartureDate = itineraryFlightTO.getDepartureDateTimeZulu();

								// in milliseconds
								long dateDifference = currentDepartureDate.getTime() - previousArrivalDate.getTime();

								String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
										previousflightSegments[previousflightSegments.length - 1], null, null);

								String[] minTransitHoursMin = minMaxTransitDurations[0].split(":");
								String[] maxTransitHoursMin = minMaxTransitDurations[1].split(":");

								long minRangeLimit = Long.parseLong(minTransitHoursMin[0], 10) * 3600000
										+ Long.parseLong(minTransitHoursMin[1], 10) * 60000;
								long maxRangeLimit = Long.parseLong(maxTransitHoursMin[0], 10) * 3600000
										+ Long.parseLong(maxTransitHoursMin[1], 10) * 60000;

								if (minRangeLimit <= dateDifference && dateDifference <= maxRangeLimit) {
									tempSegmentList.remove(tempSegmentList.size() - 1);
									String mergeStr = "";
									for (String str : previousflightSegments) {
										mergeStr += str + "/";
									}
									for (int i = 1; i < currentflightSegments.length; i++) {
										mergeStr += currentflightSegments[i] + "/";
									}
									mergeStr = mergeStr.substring(0, mergeStr.length() - 1);
									tempSegmentList.add(mergeStr);
									previousflightSegments = itineraryFlightTO.getSegmentCode().split("/");
									previousItineraryFlightTO = itineraryFlightTO;

								} else {
									if (!(CalendarUtil.getCurrentZuluDateTime().compareTo(
											itineraryFlightTO.getDepartureDateTimeZulu()) >= 0)) {
										tempSegmentList.add(itineraryFlightTO.getSegmentCode());
										previousflightSegments = itineraryFlightTO.getSegmentCode().split("/");
										previousItineraryFlightTO = itineraryFlightTO;
									}
								}
							} else {
								if (!(CalendarUtil.getCurrentZuluDateTime().compareTo(
										itineraryFlightTO.getDepartureDateTimeZulu()) >= 0)) {
									tempSegmentList.add(itineraryFlightTO.getSegmentCode());
									previousflightSegments = itineraryFlightTO.getSegmentCode().split("/");
									previousItineraryFlightTO = itineraryFlightTO;
								}
							}
						} else {
							if (!(CalendarUtil.getCurrentZuluDateTime().compareTo(itineraryFlightTO.getDepartureDateTimeZulu()) >= 0)) {
								tempSegmentList.add(itineraryFlightTO.getSegmentCode());
								previousflightSegments = itineraryFlightTO.getSegmentCode().split("/");
								previousItineraryFlightTO = itineraryFlightTO;
							}
						}
					} else {
						if (!(CalendarUtil.getCurrentZuluDateTime().compareTo(itineraryFlightTO.getDepartureDateTimeZulu()) >= 0)) {
							tempSegmentList.add(itineraryFlightTO.getSegmentCode());
							previousflightSegments = itineraryFlightTO.getSegmentCode().split("/");
							previousItineraryFlightTO = itineraryFlightTO;
						}
					}
				} else {
					if (!(CalendarUtil.getCurrentZuluDateTime().compareTo(itineraryFlightTO.getDepartureDateTimeZulu()) >= 0)) {
						tempSegmentList.add(itineraryFlightTO.getSegmentCode());
						previousflightSegments = itineraryFlightTO.getSegmentCode().split("/");
						previousItineraryFlightTO = itineraryFlightTO;
					}
				}
			}
			if (tempSegmentList.size() > 0) {
				for (String segmentStr : tempSegmentList) {
					segmentList.add(segmentStr);
				}
			}
		}
		String advertisementImage = "";
		String resourceToDisplay = "";
		String resourceURL = "";
		List<String> segmentsForCheck = new ArrayList<String>();
		if (segmentList.size() > 0) {
			if (resourceToDisplay.equals("")) {
				for (String segemtnStr : segmentList) {
					segmentsForCheck = generateSegmentsForCheck(segemtnStr);
					if (segmentsForCheck.size() > 0) {
						try {
							for (String segmentCode : segmentsForCheck) {
								if (resourceToDisplay.equals("")) {
									advertisementImage = ReservationModuleUtils.getCommonMasterBD().getAdvertisementImage(
											segmentCode, itilanguage);
									if (!(advertisementImage == "")) {
										try {
											String[] strArr = advertisementImage.split("_no_cache");
											advertisementImage = strArr[0] + "_no_cache";
										} catch (Exception e) {
											log.error("airreservations.itinerary.errorInImageFormat :" + e);
										}
										resourceURL = resourceLookup.availableResource(advertisementImage);
										if (!StringUtil.isNullOrEmpty(resourceURL)) {
											resourceToDisplay = resourceURL;
										}
										if (!resourceURL.equals("")) {
											break;
										}
									}
								}
							}
						} catch (Exception e) {
							log.error("airreservations.itinerary.errorGettingAdvertisementImage :" + e);
						}
					}
					if (!resourceToDisplay.equals("")) {
						break;
					}

				}
			}
		}

		return resourceToDisplay;
	}

	private static List<String> generateSegmentsForCheck(String strSegment) {

		List<String> resuStrings = new ArrayList<String>();
		String[] segmentArray = strSegment.toString().split("/");
		resuStrings.add(strSegment);
		resuStrings.add(segmentArray[0] + "/" + "***");
		resuStrings.add("***" + "/" + segmentArray[segmentArray.length - 1]);
		if (segmentArray.length > 2) {
			resuStrings.add(segmentArray[0] + "/" + segmentArray[segmentArray.length - 1]);
			if (segmentArray.length == 3) {
				resuStrings.add("***" + "/" + segmentArray[1] + "/" + "***");
			}
		}
		return resuStrings;
	}

}
