/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CashPaymentDTO;
import com.isa.thinair.airreservation.api.model.CashSales;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;

public interface X3CashSalesDAO extends ExternalCashSalesTransferDAO {

	public void clearCashSalesTable(Date date);

	public Collection<CashPaymentDTO> getCashPayments(Date date);

	public Collection<CashSales> insertCashSalesToInternal(Collection<CashPaymentDTO> col);

	public void updateInternalCashTable(Collection<CashSales> col);

	@SuppressWarnings("rawtypes")
	public Collection insertCashSalesToExternal(Collection<CashPaymentDTO> col, XDBConnectionUtil template) throws SQLException,
			ClassNotFoundException, Exception;

}
