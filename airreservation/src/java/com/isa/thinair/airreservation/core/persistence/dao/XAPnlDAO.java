/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.XAPnl;
import com.isa.thinair.airreservation.api.model.XAPnlPaxEntry;

/**
 * XAPnlDAO is the business DAO interface for the external airline PNL service apis
 * 
 * @author Byorn de Silva
 * @since 1.0
 */
public interface XAPnlDAO {

	/**
	 * Returns the XA pnl
	 * 
	 * @param xaPnlId
	 * @return
	 */
	public XAPnl getXAPnlEntry(int xaPnlId);

	/**
	 * Save XAPnl Entry
	 * 
	 * @param XAPnl
	 */
	public void saveXAPnlEntry(XAPnl xaPnl);

	/**
	 * Save XA Pnl Pax Entry
	 * 
	 * @param xaPnlPaxEntry
	 */
	public void saveXAPnlPaxEntry(XAPnlPaxEntry xaPnlPaxEntry);

	/**
	 * Save XA Pnl Pax Entries
	 * 
	 * @param colXAPnlPaxEntry
	 */
	public void saveXAPnlPaxEntries(Collection<XAPnlPaxEntry> colXAPnlPaxEntry);

	/**
	 * Return XA Pnl Passenger entries
	 * 
	 * @param xaPnlId
	 * @param status
	 * @return
	 */
	public Collection<XAPnlPaxEntry> getXAPnlPaxEntries(int xaPnlId, String status);

	/**
	 * 
	 * @param attemptLimit
	 * @return
	 */
	public Collection<Integer> getFailedXAPnlIds(int attemptLimit);

	/**
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @param pnlContent
	 * @return
	 */
	public boolean hasAnEqualXAPnl(String flightNumber, String airportCode, Date departureDate, Integer partNumber);

}
