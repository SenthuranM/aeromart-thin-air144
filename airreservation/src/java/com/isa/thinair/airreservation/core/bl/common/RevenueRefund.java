/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.commons.api.dto.ets.MessageFunction;
import com.isa.thinair.commons.api.dto.ets.NumberOfUnits;
import com.isa.thinair.commons.api.dto.ets.RefundTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.RequestIdentity;
import com.isa.thinair.commons.api.dto.ets.TicketCoupon;
import com.isa.thinair.commons.api.dto.ets.TicketNumberDetails;
import com.isa.thinair.commons.api.dto.ets.VoidTKTREQDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for reverse accounting per passenger
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="revenueRefund"
 */
public class RevenueRefund extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(RevenueRefund.class);

	/**
	 * Execute method of the ReverseAccounting command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Map pnrPaxIdAndPayments = (Map) this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Collection<String> prefferedRefundOrder = (Collection<String>) this
				.getParameter(CommandParamNames.PREFERRED_REFUND_ORDER);
		Collection<Long> pnrPaxOndChgIds = (Collection<Long>) this.getParameter(CommandParamNames.PNR_PAX_OND_CHARGE_IDS);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		ServiceResponce serviceResonse = (ServiceResponce) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		Boolean isManualRefund = getParameter(CommandParamNames.IS_MANUAL_REFUND, Boolean.FALSE, Boolean.class);

		// Checking params
		this.validateParams(pnr, pnrPaxIdAndPayments, version, credentialsDTO);

		// TODO Thihara : Remove this load. It seems unnnecessary if proper optimistic locking mechanism is implemented

		// Reservation information is not needed at all. Any way loading in a minor mode to update the version
		// This is purely to support concurrent update
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadEtickets(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Check Refund Constraints
		this.checkRefundConstraints(reservation, pnrPaxIdAndPayments, version);

		Iterator itPaxIds = pnrPaxIdAndPayments.keySet().iterator();
		Iterator itPaymentInfo;
		PaymentInfo paymentInfo;
		ITnxPayment iTnxPayment;
		Integer pnrPaxId;
		Collection<ITnxPayment> passengerPayments;
		PaymentAssembler paymentAssembler;
		Set<Integer> refundedPaxIds = new HashSet<Integer>();

		while (itPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPaxIds.next();
			refundedPaxIds.add(pnrPaxId);

			paymentAssembler = ((PaymentAssembler) pnrPaxIdAndPayments.get(pnrPaxId));
			itPaymentInfo = paymentAssembler.getPayments().iterator();

			passengerPayments = new ArrayList<ITnxPayment>();
			while (itPaymentInfo.hasNext()) {
				paymentInfo = (PaymentInfo) itPaymentInfo.next();

				// For Pax Credit Payment
				if (paymentInfo instanceof PaxCreditInfo) {
					PaxCreditDTO paxCreditDTO = ((PaxCreditInfo) paymentInfo).getPaxCredit();
					// we are not supporting refund pax credit across hubs at the moment
					iTnxPayment = TnxPaymentFactory.getCreditPaymentInstance(paxCreditDTO.getDebitPaxId(), null,
							credentialsDTO.getAgentCode(), paxCreditDTO.getBalance(), paxCreditDTO.getPayCurrencyDTO(),
							paxCreditDTO.getPayCarrier(), paxCreditDTO.getLccUniqueId(), paymentInfo.getPaymentTnxId());
					passengerPayments.add(iTnxPayment);
				}

				// For Card Payment
				else if (paymentInfo instanceof CardPaymentInfo) {
					CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getCardPaymentInstance(paymentInfo.getTotalAmount(),
							cardPaymentInfo.getType(), cardPaymentInfo.getNo(), cardPaymentInfo.getName(),
							cardPaymentInfo.getAddress(), cardPaymentInfo.getEDate(), cardPaymentInfo.getSecurityCode(),
							cardPaymentInfo.getPnr(), cardPaymentInfo.getPaymentBrokerRefNo(), paymentInfo.getPayCurrencyDTO(),
							cardPaymentInfo.getPaymentReferanceTO(), cardPaymentInfo.getPayCarrier(),
							cardPaymentInfo.getLccUniqueId(), cardPaymentInfo.getAuthorizationId(),
							cardPaymentInfo.getPaymentTnxId());
					passengerPayments.add(iTnxPayment);
				}

				// For Agent Credit
				else if (paymentInfo instanceof AgentCreditInfo) {
					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getOnAccountPaymentInstance(paymentInfo.getTotalAmount(),
							agentCreditInfo.getAgentCode(), paymentInfo.getPayCurrencyDTO(),
							agentCreditInfo.getPaymentReferenceTO(), agentCreditInfo.getPayCarrier(),
							agentCreditInfo.getLccUniqueId(), agentCreditInfo.getPaymentTnxId());

					passengerPayments.add(iTnxPayment);
				}

				// For Cash Payment
				else if (paymentInfo instanceof CashPaymentInfo) {
					CashPaymentInfo cashPaymentInfo = (CashPaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getCashPaymentInstance(paymentInfo.getTotalAmount(),
							paymentInfo.getPayCurrencyDTO(), cashPaymentInfo.getPaymentReferanceTO(),
							cashPaymentInfo.getPayCarrier(), cashPaymentInfo.getLccUniqueId(), cashPaymentInfo.getPaymentTnxId());
					passengerPayments.add(iTnxPayment);
				}

				// For Loyalty Payment
				else if (paymentInfo instanceof LMSPaymentInfo) {
					LMSPaymentInfo lmsPaymentInfo = (LMSPaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getLMSPaymentInstance(lmsPaymentInfo.getLoyaltyMemberAccountId(),
							lmsPaymentInfo.getRewardIDs(), paymentInfo.getTotalAmount(), paymentInfo.getPayCurrencyDTO(),
							lmsPaymentInfo.getPayCarrier(), lmsPaymentInfo.getLccUniqueId(), lmsPaymentInfo.getPaymentTnxId());
					passengerPayments.add(iTnxPayment);
				} else if (paymentInfo instanceof OfflinePaymentInfo) {
					OfflinePaymentInfo offlinePaymentInfo = (OfflinePaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getOfflinePaymentInstance(paymentInfo.getTotalAmount(),
							paymentInfo.getPayCurrencyDTO(), offlinePaymentInfo.getPayCarrier(),
							offlinePaymentInfo.getPaymentTnxId());
					passengerPayments.add(iTnxPayment);
				}
			}

			// Record record refund
			serviceResonse = ReservationModuleUtils.getRevenueAccountBD().recordRefund(pnrPaxId.toString(),
					paymentAssembler.getTotalPayAmount(), passengerPayments, userNotes, credentialsDTO,
					reservation.isEnableTransactionGranularity(), prefferedRefundOrder, pnrPaxOndChgIds, isManualRefund,
					userNoteType);
		}
		
		/**
		 * +Move inside ^
		 */
		if (AppSysParamsUtil.isEnableAeroMartETS() && reservation.isETRecordedInAeroMartETS()) {//check status cnx
			try {
				if (reservation.isVoidReservation()) {
					callVoidTicket(reservation, pnrPaxIdAndPayments.keySet());
				} else {
					callRefundTKTREQ(reservation, pnrPaxIdAndPayments.keySet());
				}
			} catch (Exception ex) {
				log.error("ERROR WHILE UPDATING ETS" + ex);
			}
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, serviceResonse);
		response.addResponceParam(CommandParamNames.RESERVATION, reservation);
		response.addResponceParam(CommandParamNames.REMOVE_AGENT_COM_PNR_PAX_IDS, refundedPaxIds);
		log.debug("Exit execute");
		return response;
	}

	/**
	 * Check Refund Constraints
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param version
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkRefundConstraints(Reservation reservation, Map pnrPaxIdAndPayments, Long version) throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		// Check if it's valid payments or not
		ReservationCoreUtils.checkValidPaxPaymentsForReservation(reservation, pnrPaxIdAndPayments, null);
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param pnrPaxIdAndPayments
	 * @param version
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(String pnr, Map pnrPaxIdAndPayments, Long version, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || pnrPaxIdAndPayments == null || version == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	private void callRefundTKTREQ(Reservation reservation, Set<Integer> pnrPaxIdSet) throws ModuleException {

		Set<ReservationPax> reservationPax = reservation.getPassengers();
		
		for (ReservationPax passenger : reservationPax) {
			for (Integer pnrPaxId : pnrPaxIdSet) {

				if (pnrPaxId.equals(passenger.getPnrPaxId())) {

					RefundTKTREQDTO ticketRefundRequest = new RefundTKTREQDTO();
					RequestIdentity requestIdentity = RequestIdentity.getInstance(AppSysParamsUtil.getDefaultCarrierCode(),
							TypeAConstants.RequestIdentitySystem.ETS);
					ticketRefundRequest.setRequestIdentity(requestIdentity);

					MessageFunction messageFunction = new MessageFunction();
					messageFunction.setMessageFunction(TypeAConstants.MessageFunction.REFUND);
					ticketRefundRequest.setMessageFunction(messageFunction);

					Collection<EticketTO> etickets = passenger.geteTickets();
					Map<String, List<TicketCoupon>> ticketCouponsMap = new HashMap<String, List<TicketCoupon>>();

					for (EticketTO ticket : etickets) {
						List<TicketCoupon> ticketCoupons = new ArrayList<TicketCoupon>();
						TicketCoupon coupon = new TicketCoupon();
						coupon.setCouponNumber(ticket.getCouponNo().toString());
						coupon.setStatus(ticket.getTicketStatus());
						ticketCoupons.add(coupon);
						ticketCouponsMap.put(ticket.getEticketNumber(), ticketCoupons);
					}

					List<TicketNumberDetails> tickets = new ArrayList<TicketNumberDetails>();

					for (String tickectNumber : ticketCouponsMap.keySet()) {

						TicketNumberDetails ticketDetatils = new TicketNumberDetails();
						ticketDetatils.setTicketNumber(tickectNumber);
						ticketDetatils.setDocumentType(TypeAConstants.DocumentType.TICKET);
						ticketDetatils.setDataIndicator(TypeAConstants.TicketDataIndicator.OLD);
						ticketDetatils.setTicketCoupon(ticketCouponsMap.get(tickectNumber));
						tickets.add(ticketDetatils);

					}

					ticketRefundRequest.setTickets(tickets);

					NumberOfUnits numberOfUnits = new NumberOfUnits();
					numberOfUnits.setQuantity(Integer.toString(tickets.size()));
					numberOfUnits.setQualifier(TypeAConstants.NumberOfUnitsQualifier.TICKETS_COUNT);
					ticketRefundRequest.setNumberOfUnits(numberOfUnits);
					
					ReservationModuleUtils.getETSTicketRefundServiceBD().refund(ticketRefundRequest);
					
				}
			}
		}
	}
	
	private void callVoidTicket(Reservation reservation, Set<Integer> pnrPaxIdSet) throws ModuleException {
//Single method for void and refund
		Set<ReservationPax> reservationPax = reservation.getPassengers();

		for (ReservationPax passenger : reservationPax) {
			for (Integer pnrPaxId : pnrPaxIdSet) {

				if (pnrPaxId.equals(passenger.getPnrPaxId())) {

					VoidTKTREQDTO ticketVoidRequest = new VoidTKTREQDTO();
					RequestIdentity requestIdentity = RequestIdentity.getInstance(AppSysParamsUtil.getDefaultCarrierCode(),
							TypeAConstants.RequestIdentitySystem.ETS);
					ticketVoidRequest.setRequestIdentity(requestIdentity);

					MessageFunction messageFunction = new MessageFunction();
					messageFunction.setMessageFunction(TypeAConstants.MessageFunction.VOID);
					ticketVoidRequest.setMessageFunction(messageFunction);

					Collection<EticketTO> etickets = passenger.geteTickets();
					Map<String, List<TicketCoupon>> ticketCouponsMap = new HashMap<String, List<TicketCoupon>>();

					for (EticketTO ticket : etickets) {
						List<TicketCoupon> ticketCoupons = new ArrayList<TicketCoupon>();
						TicketCoupon coupon = new TicketCoupon();
						coupon.setCouponNumber(ticket.getCouponNo().toString());
						coupon.setStatus(ticket.getTicketStatus());
						ticketCoupons.add(coupon);
						ticketCouponsMap.put(ticket.getEticketNumber(), ticketCoupons);
					}

					List<TicketNumberDetails> tickets = new ArrayList<TicketNumberDetails>();

					for (String tickectNumber : ticketCouponsMap.keySet()) {

						TicketNumberDetails ticketDetatils = new TicketNumberDetails();
						ticketDetatils.setTicketNumber(tickectNumber);
						ticketDetatils.setDocumentType(TypeAConstants.DocumentType.TICKET);
						ticketDetatils.setDataIndicator(TypeAConstants.TicketDataIndicator.OLD);
						ticketDetatils.setTicketCoupon(ticketCouponsMap.get(tickectNumber));
						tickets.add(ticketDetatils);

					}

					ticketVoidRequest.setTickets(tickets);

					NumberOfUnits numberOfUnits = new NumberOfUnits();
					numberOfUnits.setQuantity(Integer.toString(tickets.size()));
					numberOfUnits.setQualifier(TypeAConstants.NumberOfUnitsQualifier.TICKETS_COUNT);
					ticketVoidRequest.setNumberOfUnits(numberOfUnits);

					ReservationModuleUtils.getETSTicketVoidServiceBD().voidTicket(ticketVoidRequest);

				}
			}
		}
	}
}
