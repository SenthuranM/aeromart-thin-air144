/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.login.util.LoginUtilities;

/**
 * Reservation zulu Proxy is the proxy for the reservation zulu related functions
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PnrZuluProxy {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(PnrZuluProxy.class);

	/**
	 * Hide the constructor
	 */
	private PnrZuluProxy() {

	}

	/**
	 * Returns the reservation with local times
	 * 
	 * @param pnrModesDTO
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation getReservationWithLocalTimes(LCCClientPnrModesDTO pnrModesDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside getReservationWithLocalTimes");

		if (credentialsDTO.getColUserDST() == null) {
			throw new ModuleException("airreservations.arg.cantLocateDSTInformation");
		}

		// Get the reservation
		pnrModesDTO.setAgentCode(credentialsDTO.getAgentCode());
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Setting the pnr booking time stamp
		Date localTimeStamp = LoginUtilities.getAgentLocalTime(credentialsDTO.getColUserDST(),
				reservation.getZuluBookingTimestamp());
		reservation.setLocalBookingTimestamp(localTimeStamp);

		// Setting the passenger times
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxOndCharge reservationPaxOndCharge;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			// Setting the local start time stamp
			localTimeStamp = LoginUtilities.getAgentLocalTime(credentialsDTO.getColUserDST(),
					reservationPax.getZuluStartTimeStamp());
			reservationPax.setLocalStartTimeStamp(localTimeStamp);

			// Setting the local release time stamp
			localTimeStamp = LoginUtilities.getAgentLocalTime(credentialsDTO.getColUserDST(),
					reservationPax.getZuluReleaseTimeStamp());
			reservationPax.setLocalReleaseTimeStamp(localTimeStamp);

			// If load fares
			if (pnrModesDTO.isLoadFares()) {
				itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
				while (itReservationPaxFare.hasNext()) {
					reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

					itReservationPaxOndCharge = reservationPaxFare.getCharges().iterator();
					while (itReservationPaxOndCharge.hasNext()) {
						reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();

						localTimeStamp = LoginUtilities.getAgentLocalTime(credentialsDTO.getColUserDST(),
								reservationPaxOndCharge.getZuluChargeDate());

						reservationPaxOndCharge.setLocalChargeDate(localTimeStamp);
					}
				}
			}
		}

		log.debug("Exit getReservationWithLocalTimes");
		return reservation;
	}

	/**
	 * Return reservation modifications with local times
	 * 
	 * @param pnr
	 * @param isUserNotes
	 * @param credentialsDTO
	 * @param isClassifyUN
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ReservationModificationDTO> getReservationModificationsWithLocalTimes(String pnr, boolean isUserNotes,
			CredentialsDTO credentialsDTO, boolean isClassifyUN) throws ModuleException {
		log.debug("Inside getReservationModificationsWithLocalTimes");

		if (credentialsDTO.getColUserDST() == null) {
			throw new ModuleException("airreservations.arg.cantLocateDSTInformation");
		}

		// Get the auditor delegate
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();

		Collection<ReservationModificationDTO> colReservationModification;

		if (isUserNotes) {
			colReservationModification = auditorBD.getUserNotes(pnr, isClassifyUN, credentialsDTO.getAgentCode());
		} else {
			colReservationModification = auditorBD.getHistory(pnr);
		}

		Iterator<ReservationModificationDTO> itColReservationModification = colReservationModification.iterator();
		ReservationModificationDTO reservationModificationDTO;
		Date localTimeStamp;

		while (itColReservationModification.hasNext()) {
			reservationModificationDTO = (ReservationModificationDTO) itColReservationModification.next();

			localTimeStamp = LoginUtilities.getAgentLocalTime(credentialsDTO.getColUserDST(),
					reservationModificationDTO.getZuluModificationDate());
			reservationModificationDTO.setLocalModificationDate(localTimeStamp);
		}

		log.debug("Exit getReservationModificationsWithLocalTimes");
		return colReservationModification;
	}
}
