/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder;

import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;

/**
 * @author udithad
 *
 */
public interface PnlAdlMessage {
	public PnlAdlMessageResponse buildMessage(BaseDataContext context);
}
