/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.PassengerSeating;

/**
 * SalesDAO is the business DAO interface for the sales service apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface SeatMapDAO {

	public void saveOrUpdate(Collection<PassengerSeating> seating);

	public void deleteReservationSeats(Collection<PassengerSeating> seating);

	public Collection<PassengerSeating> getFlightSeatsForPnrPaxFare(Collection<Integer> ppfIds);

	public Collection<PassengerSeating> getFlightSeats(Collection<Integer> pnrPaxIds);

	public Collection<PaxSeatTO> getReservationSeats(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds);

	public Map<Integer, String> getReservationSeatsForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds);

	public Map<Integer, List<AncillaryDTO>> getReservationSeatsForPNLADL(Collection<Integer> pnrPaxIds,
			Collection<Integer> pnrSegIds);

	public Collection<PassengerSeating> getPassengerSeats(Collection<Integer> pkeys);

	public Collection<PassengerSeating> getReservedFlightSeats(Collection<Integer> flightAMSeatIds);

	public Collection<String> getSeatCodes(Collection<Integer> flightAmSeatIds);

	public Collection<String> getSeatCodesAndSeatCharge(Collection<Integer> flightAmSeatIds);

	public Integer getTemplateForSegmentId(int flightSegId);

	/**
	 * Return expired seats based on auto cancellation flag
	 * 
	 * @param cancellationIds
	 * @param marketingCarrier
	 * @return
	 */
	public Map<ReservationLiteDTO, List<PaxSeatTO>> getExpiredSeats(Collection<Integer> cancellationIds, String marketingCarrier);

	/**
	 * Return expired lcc reservations due to seat expire based on auto cancellation flag
	 * 
	 * @param marketingCarrier
	 * @param cancellationIds
	 * 
	 * @return
	 */
	public Map<String, Integer> getExpiredLccSeatsInfo(String marketingCarrier, Collection<Integer> cancellationIds);

}
