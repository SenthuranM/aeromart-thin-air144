/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.EffectedFlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.FlightChangeInfo;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationAlertTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.util.AlertReservationsUtil;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for alerting reservations
 * 
 * Business Rules: (1) This should be called for per flight
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="alertReservations"
 */
public class AlertReservations extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(AlertReservations.class);

	/** Holds the credentials information */
	private CredentialsDTO credentialsDTO;

	/** Holds the FlightReservationAlertDTO information */
	private Collection<FlightReservationAlertDTO> colFlightReservationAlertDTO;

	/** Holds the whether or not to send emails for flight alterations */
	private boolean sendEmailsForFlightAlterations;

	/** Holds the whether or not to send sms for flight alterations */
	private boolean sendSMSForFlightAlterations;

	/** Holds the whether or not to send sms for confirmed bookings */
	private boolean sendSMSForConfirmedBookings;

	/** Holds the whether or not to send sms for on hold bookings */
	private boolean sendSMSForOnHoldBookings;

	/** Holds the whether or not to send emails for flight reschedule */
	private boolean sendEmailsForFlightReschedule;

	/** Holds the whether or not to send sms for flight alterations */
	private boolean sendSMSForFlightReschedule;

	/**
	 * Execute method of the AlertReservations command
	 * 
	 * @throws ModuleException
	 */
	/* (non-Javadoc)
	 * @see com.isa.thinair.commons.core.framework.DefaultBaseCommand#execute()
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Collection<FlightReservationAlertDTO> colFlightReservationAlertDTO = (Collection<FlightReservationAlertDTO>) this
				.getParameter(CommandParamNames.FLIGHT_RESERVATION_ALERT_DTOS);
		Collection<FlightReservationAlertDTO> colFlightReservationAlertSeatLost = (Collection<FlightReservationAlertDTO>) this
				.getParameter(CommandParamNames.FLIGHT_RESERVATION_ALERT_DTOS_SEAT_LOST);
		Boolean isRescheduleFlight = (Boolean) this.getParameter(CommandParamNames.IS_RESCHEDULE_FLIGHT);
		Boolean isCancelFlight = (Boolean) this.getParameter(CommandParamNames.IS_CANCEL_FLIGHT);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);

		// Checking params
		this.validateParams(colFlightReservationAlertDTO, isRescheduleFlight, isCancelFlight, flightAlertDTO, credentialsDTO);

		// Set Arguments
		this.setArguments(colFlightReservationAlertDTO, credentialsDTO, flightAlertDTO);
		// For cancel flight
		if (isCancelFlight.booleanValue()) {
			if (sendEmailsForFlightAlterations || sendSMSForFlightAlterations || sendEmailsForFlightReschedule
					|| sendSMSForFlightReschedule) {
				this.generateCustomerNotificationsForPnrSegments(colFlightReservationAlertDTO, flightAlertDTO);
			}
			if (flightAlertDTO.isEmailForCancelledFlight()) {
				// Generate emails for pnr segments
				this.generateEMailsForPnrSegments(colFlightReservationAlertDTO, flightAlertDTO);
			}

			// Get the effected flight segment dtos
			Collection<EffectedFlightSegmentDTO> colEffectedFlightSegmentDTO = this.getEffectedFlightSegmentDTOs();

			if (flightAlertDTO.isAlertForCancelledFlight()) {

				// Alerts for actual pnr segments
				this.generateAlertsForActualPnrSegments(colEffectedFlightSegmentDTO,
						ReservationInternalConstants.ReservationAlertTypes.CANCEL_FLIGHT);

				// Alerts for effected pnr segments
				this.generateAlertsForEffectedPnrSegments(colEffectedFlightSegmentDTO,
						ReservationInternalConstants.ReservationAlertTypes.CANCEL_FLIGHT);

			}

			// sending SMS for canceled flights to an official if the app parameter say so.
			String enabledFlag = AppSysParamsUtil.isToNotifyAnOfficial();
			if (enabledFlag != null && enabledFlag.equals("Y")) {
				List<String>  notifyAnOfficial =	toNotifyAnOfficial();
				for(int i=0;i<notifyAnOfficial.size();i++){
			
					this.generateSMSAlertForAnOfficial(colFlightReservationAlertDTO, flightAlertDTO, notifyAnOfficial.get(i));
				}
			}
		}

		// For Re Schedule flight
		else if (isRescheduleFlight.booleanValue()) {
			if (sendEmailsForFlightReschedule || sendSMSForFlightReschedule || sendEmailsForFlightAlterations
					|| sendSMSForFlightAlterations) {
				this.generateCustomerNotificationsForFlightSegments(colFlightReservationAlertDTO, flightAlertDTO);
			}
			if (flightAlertDTO.isEmailForRescheduledFlight()) {
				// Generate emails for flight segments
				this.generateEMailsForFlightSegments(colFlightReservationAlertDTO, flightAlertDTO);
			}
			if (flightAlertDTO.isAlertForRescheduledFlight()) {
				// Generate alerts for pnr segments
				this.generateAlertsForPnrSegments(colFlightReservationAlertDTO, flightAlertDTO,
						ReservationInternalConstants.ReservationAlertTypes.RESCHEDULE_FLIGHT);
			}

			// sending SMS for changed flights to an official if the app parameter say so.
			String enabledFlag = AppSysParamsUtil.isToNotifyAnOfficial();
			
			if (enabledFlag != null && enabledFlag.equals("Y")) {
				
				List<String>  notifyAnOfficial =	toNotifyAnOfficial();
				for(int i=0;i<notifyAnOfficial.size();i++){
					this.generateSMSAlertForAnOfficial(colFlightReservationAlertDTO, flightAlertDTO, notifyAnOfficial.get(i));
				}
			}
		}
		if (flightAlertDTO.isSendEmailWhenSeatLost() && colFlightReservationAlertSeatLost != null) {
			this.generateEMailsForSeatLost(colFlightReservationAlertSeatLost, flightAlertDTO);
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * SMS notifications for an official in the AirLine service, about fight cancels, schedule modifications and .etc
	 * 
	 * @param mobileNumber
	 * @param colFlightReservationAlertDTO
	 * @param flightAlertDTO
	 * @throws ModuleException
	 */

	private void generateSMSAlertForAnOfficial(Collection<FlightReservationAlertDTO> colFlightReservationAlertDTO,
			FlightAlertDTO flightAlertDTO, String officialsMobileNumber) throws ModuleException {

		// Profile List
		List messageProfiles = new ArrayList();
		String senderTemplate = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_SMS_FOR_AN_OFFICIAL;

		for (Iterator iterator = colFlightReservationAlertDTO.iterator(); iterator.hasNext();) {
			FlightReservationAlertDTO flightReservationAlertDTO = (FlightReservationAlertDTO) iterator.next();

			MessageProfile profile = getMessageProfileForAnOfficial(flightAlertDTO, flightReservationAlertDTO, senderTemplate,
					officialsMobileNumber, null);
			messageProfiles.add(profile);

		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
			// call for sms the official
		}
	}

	/**
	 * Generate Customer Notifications for flight segments
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param flightAlertDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateCustomerNotificationsForFlightSegments(
			Collection<FlightReservationAlertDTO> colFlightReservationAlertDTO, FlightAlertDTO flightAlertDTO)
			throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Profile List
		List messageProfiles = new ArrayList();
		FlightReservationAlertDTO flightReservationAlertDTO;
		Collection colReservationSegment;
		boolean DSTTemplate = false;

		for (Iterator<FlightReservationAlertDTO> itFlightReservationAlertDTO = colFlightReservationAlertDTO.iterator(); itFlightReservationAlertDTO
				.hasNext();) {

			flightReservationAlertDTO = (FlightReservationAlertDTO) itFlightReservationAlertDTO.next();

			Set s = flightReservationAlertDTO.getAlertDetails().keySet();
			for (Iterator i = s.iterator(); i.hasNext();) {
				AlertTemplateEnum alertTemp = (AlertTemplateEnum) i.next();
				if (alertTemp.equals(AlertTemplateEnum.DAY_LIGHT_SAVING_TIME_CHANGED)) {
					DSTTemplate = true;
				}

			}

			if (flightReservationAlertDTO.getFlightSegmentIds().size() > 0) {
				// Get pnr for the pnr segments
				colReservationSegment = reservationSegmentDAO.getReservationSegments(
						flightReservationAlertDTO.getFlightSegmentIds(), null,
						ReservationInternalConstants.ReservationStatus.CONFIRMED, true, false);

				// Proceed only if pnr exists
				if (colReservationSegment != null && colReservationSegment.size() > 0) {
					// Reservation Object list
					List<Reservation> emailReservationList = new ArrayList<Reservation>();
					List<Reservation> smsReservationList = new ArrayList<Reservation>();

					Iterator itColReservationSegment = colReservationSegment.iterator();

					while (itColReservationSegment.hasNext()) {
						ReservationSegment reservationSegment = (ReservationSegment) itColReservationSegment.next();
						if (reservationSegment.getOpenReturnConfirmBeforeZulu() != null) {
							continue;
						}

						String emailAddress = BeanUtils.nullHandler(reservationSegment.getReservation().getContactInfo()
								.getEmail());
						String mobileNumber = BeanUtils.nullHandler(reservationSegment.getReservation().getContactInfo()
								.getMobileNo());

						if (emailAddress.length() > 0 && emailAddress.indexOf("@") != -1) {
							emailReservationList.add(reservationSegment.getReservation());
						}

						if (mobileNumber.length() > 0) {
							smsReservationList.add(reservationSegment.getReservation());
						}
					}

					if ((sendEmailsForFlightAlterations && emailReservationList.size() > 0)
							|| (sendEmailsForFlightReschedule && emailReservationList.size() > 0)) {
						String templateName = "";
						if (DSTTemplate) {
							templateName = ReservationInternalConstants.PnrTemplateNames.DST_CHANGE_EMAIL;
						} else {
							templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_EMAIL;
						}
						for (Reservation booking : emailReservationList) {
							Reservation reservation = loadDetailedReservation(booking.getPnr());
							Collection colReservationAudit = composeEmailAudit(reservation.getPnr(),
									flightReservationAlertDTO.getNewFlightAlertInfoDTO(),
									flightReservationAlertDTO.getAlertTemplateIds());

							MessageProfile profile = getMessageProfileForFlightAlterations(reservation, flightAlertDTO,
									flightReservationAlertDTO, templateName, reservation.getContactInfo().getEmail(),
									colReservationAudit);
							messageProfiles.add(profile);
						}
					}

					if ((sendSMSForFlightAlterations && smsReservationList.size() > 0)
							|| (sendSMSForFlightReschedule && smsReservationList.size() > 0)) {
						String senderTemplate = "";
						if (DSTTemplate) {
							senderTemplate = ReservationInternalConstants.PnrTemplateNames.DST_CHANGE_SMS;
						} else {
							senderTemplate = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_SMS;
						}
						for (Reservation booking : smsReservationList) {
							Reservation reservation = loadDetailedReservation(booking.getPnr());

							boolean isBookingOnhold = AccelAeroCalculator.add(reservation.getTotalChargeAmounts()).doubleValue() > reservation
									.getTotalPaidAmount().doubleValue();

							if (isBookingOnhold && sendSMSForOnHoldBookings) {
								MessageProfile profile = getMessageProfileForFlightAlterations(reservation, flightAlertDTO,
										flightReservationAlertDTO, senderTemplate, reservation.getContactInfo().getMobileNo(),
										null);
								messageProfiles.add(profile);
							}

							if (!isBookingOnhold && sendSMSForConfirmedBookings) {
								MessageProfile profile = getMessageProfileForFlightAlterations(reservation, flightAlertDTO,
										flightReservationAlertDTO, senderTemplate, reservation.getContactInfo().getMobileNo(),
										null);
								messageProfiles.add(profile);
							}
						}
					}
				}
			}

		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	/**
	 * Generate Customer Notifications for Pnr Segments
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param flightAlertDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateCustomerNotificationsForPnrSegments(Collection colFlightReservationAlertDTO,
			FlightAlertDTO flightAlertDTO) throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Profile List
		List messageProfiles = new ArrayList();
		EffectedFlightSegmentDTO effectedFlightSegmentDTO;
		Collection colReservationSegment;
		FlightReservationAlertDTO flightReservationAlertDTO;
		// boolean DSTTemplate = false;
		for (Iterator itColFlightReservationAlertDTO = colFlightReservationAlertDTO.iterator(); itColFlightReservationAlertDTO
				.hasNext();) {
			flightReservationAlertDTO = (FlightReservationAlertDTO) itColFlightReservationAlertDTO.next();

			// Reservation Object list
			List<Reservation> emailReservationList = new ArrayList<Reservation>();
			List<Reservation> smsReservationList = new ArrayList<Reservation>();

			for (Iterator itColEffectedFlightSegmentDTO = flightReservationAlertDTO.getColEffectedFlightSegmentDTO().iterator(); itColEffectedFlightSegmentDTO
					.hasNext();) {

				effectedFlightSegmentDTO = (EffectedFlightSegmentDTO) itColEffectedFlightSegmentDTO.next();

				if (effectedFlightSegmentDTO.getActualPnrSegmentIds().size() > 0) {
					// Get pnr for the pnr segments
					colReservationSegment = reservationSegmentDAO.getReservationSegments(null,
							effectedFlightSegmentDTO.getActualPnrSegmentIds(), null, true, false);

					// Proceed only if pnr exists
					if (colReservationSegment != null && colReservationSegment.size() > 0) {
						Iterator itColReservationSegment = colReservationSegment.iterator();

						while (itColReservationSegment.hasNext()) {
							ReservationSegment reservationSegment = (ReservationSegment) itColReservationSegment.next();
							if (reservationSegment.getOpenReturnConfirmBeforeZulu() != null) {
								continue;
							}
							String emailAddress = BeanUtils.nullHandler(reservationSegment.getReservation().getContactInfo()
									.getEmail());
							String mobileNumber = BeanUtils.nullHandler(reservationSegment.getReservation().getContactInfo()
									.getMobileNo());

							if (emailAddress.length() > 0 && emailAddress.indexOf("@") != -1) {
								emailReservationList.add(reservationSegment.getReservation());
							}

							if (mobileNumber.length() > 0) {
								smsReservationList.add(reservationSegment.getReservation());
							}
						}
					}
				}
			}

			if ((sendEmailsForFlightAlterations && emailReservationList.size() > 0)
					|| (sendEmailsForFlightReschedule && emailReservationList.size() > 0)) {
				for (Reservation booking : emailReservationList) {
					Reservation reservation = loadDetailedReservation(booking.getPnr());
					Collection colReservationAudit = composeEmailAudit(reservation.getPnr(),
							flightReservationAlertDTO.getOldFlightAlertInfoDTO(), flightReservationAlertDTO.getAlertTemplateIds());
					MessageProfile profile = getMessageProfileForFlightAlterations(reservation, flightAlertDTO,
							flightReservationAlertDTO, ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_EMAIL,
							reservation.getContactInfo().getEmail(), colReservationAudit);
					messageProfiles.add(profile);
				}
			}

			if ((sendSMSForFlightAlterations && smsReservationList.size() > 0)
					|| (sendSMSForFlightReschedule && smsReservationList.size() > 0)) {

				for (Reservation booking : smsReservationList) {
					Reservation reservation = loadDetailedReservation(booking.getPnr());

					String sender = "";
					String senderTemplate = "";
					if (SystemPropertyUtil.isSmsToEmail()) {
						sender = reservation.getContactInfo().getEmail();
						senderTemplate = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_SMS;
					} else {
						sender = reservation.getContactInfo().getMobileNo();
						senderTemplate = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_SMS;
					}

					boolean isBookingOnhold = AccelAeroCalculator.add(reservation.getTotalChargeAmounts()).doubleValue() > reservation
							.getTotalPaidAmount().doubleValue();

					if (isBookingOnhold && sendSMSForOnHoldBookings) {
						MessageProfile profile = getMessageProfileForFlightAlterations(reservation, flightAlertDTO,
								flightReservationAlertDTO, senderTemplate, sender, null);
						messageProfiles.add(profile);
					}

					if (!isBookingOnhold && sendSMSForConfirmedBookings) {
						MessageProfile profile = getMessageProfileForFlightAlterations(reservation, flightAlertDTO,
								flightReservationAlertDTO, senderTemplate, sender, null);
						messageProfiles.add(profile);
					}
				}
			}

		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
			// call for sms the official
		}
	}

	/**
	 * Loads the detail reservation. Earlier reservation entity only contains the high level reservation information
	 * only
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	private Reservation loadDetailedReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	/**
	 * Create Message profile for flight alterations
	 * 
	 * @param reservation
	 * @param flightAlertDTO
	 * @param flightReservationAlertDTO
	 * @param topicName
	 * @param toAddress
	 * @param colReservationAudit
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private MessageProfile getMessageProfileForFlightAlterations(Reservation reservation, FlightAlertDTO flightAlertDTO,
			FlightReservationAlertDTO flightReservationAlertDTO, String topicName, String toAddress,
			Collection colReservationAudit) throws ModuleException {

		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightReservationAlertDTO.getOldFlightAlertInfoDTO()
				.getFlightNumber());
		String templateName = "";
		if (ReservationAlertTypes.CANCEL_FLIGHT.equalsIgnoreCase(flightReservationAlertDTO.getReservationAlertType())) {
			templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_SMS;
		} else if (ReservationAlertTypes.RESCHEDULE_FLIGHT.equalsIgnoreCase(flightReservationAlertDTO.getReservationAlertType())) {
			templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_SMS;
		}
		// Email ids list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(toAddress);
		messageList.add(user);

		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();
		String preferredLanguage = reservation.getContactInfo().getPreferredLanguage();
		topic.setTopicName(topicName);

		topic.setLocale(new Locale(preferredLanguage));

		map.put("alertTemplateEnum", flightReservationAlertDTO.getAlertTemplateIds());
		map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
		map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());

		map.put("pnr", reservation.getPnr());
		map.put("title", BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getTitle()));
		map.put("firstName", BeanUtils.getFirst20Characters(reservation.getContactInfo().getFirstName(),20));
		map.put("user",
				BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getFirstName()) + " "
						+ BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getLastName()));
		map.put("companyAddress", ReservationApiUtils.getCompanyAddressInformation(carrierCode));
		map.put("reservationPaxDTOs", getAffectedSegments(reservation, flightReservationAlertDTO.getOldFlightAlertInfoDTO()));
		map.put("ibeURL", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		map.put("phone", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
		map.put("logoImageName", getLogoImageName(carrierCode));
		map.put("message", AlertReservationsUtil.getMessage(flightReservationAlertDTO, preferredLanguage, map, templateName));

		topic.setTopicParams(map);

		topic.setAuditInfo(colReservationAudit);
		topic.setAttachMessageBody(true);
		profile.setTopic(topic);

		return profile;
	}

	/**
	 * Create Message Profile to send a message for an official of the airline service
	 * 
	 * @param reservation
	 * @param flightAlertDTO
	 * @param flightReservationAlertDTO
	 * @param topicName
	 * @param toAddress
	 * @param colReservationAudit
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private MessageProfile getMessageProfileForAnOfficial(FlightAlertDTO flightAlertDTO,
			FlightReservationAlertDTO flightReservationAlertDTO, String topicName, String toAddress,
			Collection colReservationAudit) throws ModuleException {

		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightReservationAlertDTO.getOldFlightAlertInfoDTO()
				.getFlightNumber());
		String templateName = "";
		if (ReservationAlertTypes.CANCEL_FLIGHT.equalsIgnoreCase(flightReservationAlertDTO.getReservationAlertType())) {
			templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_SMS_FOR_AN_OFFICIAL;
		} else if (ReservationAlertTypes.RESCHEDULE_FLIGHT.equalsIgnoreCase(flightReservationAlertDTO.getReservationAlertType())) {
			templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_SMS_FOR_AN_OFFICIAL;
		}

		List messageList = new ArrayList();
		String[] addressArr = toAddress.split(",");

		for (int i = 0; i < addressArr.length; i++) {
			UserMessaging user = new UserMessaging();
			user.setToAddres(addressArr[i]);
			messageList.add(user);
		}

		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();
		topic.setTopicName(templateName);

		// topic.setLocale(new Locale(preferredLanguage));

		map.put("alertTemplateEnum", flightReservationAlertDTO.getAlertTemplateIds());
		map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
		map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());
		map.put("user", "Sir");
		map.put("companyAddress", ReservationApiUtils.getCompanyAddressInformation(carrierCode));
		map.put("ibeURL", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		// map.put("phone", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
		map.put("logoImageName", getLogoImageName(carrierCode));
		map.put("message", AlertReservationsUtil.getMessageForAnOfficer(flightReservationAlertDTO, map, templateName));

		topic.setTopicParams(map);

		topic.setAuditInfo(colReservationAudit);
		topic.setAttachMessageBody(true);
		profile.setTopic(topic);

		return profile;
	}

	/**
	 * Returns the logo image name
	 * 
	 * @param carrierCode
	 * @return
	 */
	private String getLogoImageName(String carrierCode) {
		String logoImageName = "LogoAni.gif";
		if (carrierCode != null) {
			logoImageName = "LogoAni" + carrierCode + ".gif";
		}
		return StaticFileNameUtil.getCorrected(logoImageName);
	}

	/**
	 * Compose the Email Audit
	 * 
	 * @param pnr
	 * @param flightAlertInfoDTO
	 * @param colAlertTemplateIds
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection composeEmailAudit(String pnr, FlightAlertInfoDTO flightAlertInfoDTO, Collection colAlertTemplateIds) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.EMAIL_FLIGHT_ALTERATION.getCode());
		ReservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailFlightAlterations.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailFlightAlterations.ORIGIN_CARRIER,
				AppSysParamsUtil.extractCarrierCode(flightAlertInfoDTO.getFlightNumber()));

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailFlightAlterations.FLIGHT_ALTERATIONS,
				getFlightAlteration(flightAlertInfoDTO, colAlertTemplateIds));

		Collection colReservationAudit = new ArrayList();
		colReservationAudit.add(reservationAudit);

		return colReservationAudit;
	}

	/**
	 * Return Flight Alterations
	 * 
	 * @param flightAlertInfoDTO
	 * @param colAlertTemplateIds
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private String getFlightAlteration(FlightAlertInfoDTO flightAlertInfoDTO, Collection colAlertTemplateIds) {
		StringBuilder changes = new StringBuilder();
		changes.append(" Flight Number : " + flightAlertInfoDTO.getFlightNumber());
		changes.append(" Departure Date : " + flightAlertInfoDTO.getDepartureStringDate("EEE, dd MMM yyyy"));
		changes.append(" Origin Airport : " + flightAlertInfoDTO.getOriginAptCode());
		changes.append(" Destination Airport : " + flightAlertInfoDTO.getDestinationAptCode());

		if (flightAlertInfoDTO.getRoute() != null) {
			changes.append(" Route : " + flightAlertInfoDTO.getRoute());
		}

		if (flightAlertInfoDTO.getSegmentDetails() != null) {
			changes.append(" Segment Details : " + flightAlertInfoDTO.getSegmentDetails());
		} else {
			changes.append(" Leg Details : " + flightAlertInfoDTO.getLegDetails());
		}

		changes.append(" Effected by : ");

		for (Iterator iter = colAlertTemplateIds.iterator(); iter.hasNext();) {
			AlertTemplateEnum alertTemplateEnum = (AlertTemplateEnum) iter.next();
			changes.append(" " + alertTemplateEnum.getCodeDesc() + " ");
		}

		return changes.toString();
	}

	/**
	 * Returns the affected segments
	 * 
	 * @param reservation
	 * @param oldFlightAlertInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection getAffectedSegments(Reservation reservation, FlightAlertInfoDTO oldFlightAlertInfoDTO)
			throws ModuleException {
		Collection colReservationSegmentDTO = reservation.getSegmentsView();
		List lstAffectedReservationSegmentDTO = new ArrayList();
		Collection colAirportCodes = new ArrayList();
		ReservationSegmentDTO reservationSegmentDTO;

		for (Iterator iter = colReservationSegmentDTO.iterator(); iter.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) iter.next();

			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())
					&& reservationSegmentDTO.getFlightNo().equals(oldFlightAlertInfoDTO.getFlightNumber())
					&& reservationSegmentDTO.getDepartureDate().compareTo(oldFlightAlertInfoDTO.getDepartureDate()) == 0
					&& reservationSegmentDTO.getSegmentCode().indexOf(oldFlightAlertInfoDTO.getOriginAptCode()) != -1
					&& reservationSegmentDTO.getSegmentCode().indexOf(oldFlightAlertInfoDTO.getDestinationAptCode()) != -1) {
				lstAffectedReservationSegmentDTO.add(reservationSegmentDTO);
				colAirportCodes.addAll(Arrays.asList(reservationSegmentDTO.getSegmentCode().split("/")));
			}
		}

		if (lstAffectedReservationSegmentDTO.size() > 0) {
			AirportBD airportBD = ReservationModuleUtils.getAirportBD();
			Map<String, CachedAirportDTO> mapAirportCodes = airportBD.getCachedOwnAirportMap(colAirportCodes);

			for (Iterator iter = lstAffectedReservationSegmentDTO.iterator(); iter.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) iter.next();

				String composedSegment = "";
				String[] segments = reservationSegmentDTO.getSegmentCode().split("/");

				for (int i = 0; i < segments.length; i++) {
					CachedAirportDTO airport = mapAirportCodes.get(segments[i]);
					if (i != 0) {
						composedSegment += " / ";
					}
					composedSegment += airport.getAirportName();
				}

				reservationSegmentDTO.setSegmentDescription(composedSegment);
			}

			// Sorting via the depature date
			Collections.sort(lstAffectedReservationSegmentDTO);
		}

		return lstAffectedReservationSegmentDTO;
	}

	/**
	 * Return the effected flight segment dtos
	 * 
	 * @param colFlightReservationAlertDTO2
	 * @return
	 */
	private Collection<EffectedFlightSegmentDTO> getEffectedFlightSegmentDTOs() {
		Iterator<FlightReservationAlertDTO> itColFlightReservationAlertDTO = colFlightReservationAlertDTO.iterator();
		FlightReservationAlertDTO flightReservationAlertDTO;
		Collection<EffectedFlightSegmentDTO> colEffectedFlightSegmentDTO = new ArrayList<EffectedFlightSegmentDTO>();

		while (itColFlightReservationAlertDTO.hasNext()) {
			flightReservationAlertDTO = (FlightReservationAlertDTO) itColFlightReservationAlertDTO.next();
			colEffectedFlightSegmentDTO.addAll(flightReservationAlertDTO.getColEffectedFlightSegmentDTO());
		}

		return colEffectedFlightSegmentDTO;
	}

	/**
	 * Generate Alerts for effected pnr segments
	 * 
	 * @param colEffectedFlightSegmentDTO
	 * @param strReservationAlertType
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateAlertsForEffectedPnrSegments(Collection colEffectedFlightSegmentDTO, String strReservationAlertType)
			throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		Iterator itColEffectedFlightSegmentDTO = colEffectedFlightSegmentDTO.iterator();
		Collection colEffectedPnrSegmentIds = new ArrayList();
		EffectedFlightSegmentDTO effectedFlightSegmentDTO;

		while (itColEffectedFlightSegmentDTO.hasNext()) {
			effectedFlightSegmentDTO = (EffectedFlightSegmentDTO) itColEffectedFlightSegmentDTO.next();

			if (effectedFlightSegmentDTO.getEffectedPnrSegmentIds().size() > 0) {
				// Handling for effected pnr segments
				Collection colReservationSegment = reservationSegmentDAO.getReservationSegments(null,
						effectedFlightSegmentDTO.getEffectedPnrSegmentIds(), null, false, false);
				colEffectedPnrSegmentIds.addAll(effectedFlightSegmentDTO.getEffectedPnrSegmentIds());

				if (colReservationSegment != null && colReservationSegment.size() > 0) {
					this.processAlertsForEffectedFlightSegment(colReservationSegment,
							effectedFlightSegmentDTO.getFlightSegmentCode(), strReservationAlertType);
				}
			}

		}

		if (colEffectedPnrSegmentIds.size() > 0) {
			// Set reservation alert 'ON' for PNR segments
			reservationSegmentDAO.updateReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_TRUE, null,
					colEffectedPnrSegmentIds, null);
		}
	}

	/**
	 * Set Arguments
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param credentialsDTO
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setArguments(Collection colFlightReservationAlertDTO, CredentialsDTO credentialsDTO,
			FlightAlertDTO flightAlertDTO) {
		this.credentialsDTO = credentialsDTO;
		this.colFlightReservationAlertDTO = colFlightReservationAlertDTO;
		this.sendEmailsForFlightAlterations = flightAlertDTO.isSendEmailsForFlightAlterations();
		this.sendSMSForFlightAlterations = flightAlertDTO.isSendSMSForFlightAlterations();
		this.sendSMSForConfirmedBookings = flightAlertDTO.isSendSMSForConfirmedBookings();
		this.sendSMSForOnHoldBookings = flightAlertDTO.isSendSMSForOnHoldBookings();
		this.sendEmailsForFlightReschedule = flightAlertDTO.isSendEmailsForRescheduledFlight();
		this.sendSMSForFlightReschedule = flightAlertDTO.isSendSMSForRescheduledFlight();

	}

	/**
	 * Generate Emails for flight segments
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param flightAlertDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateEMailsForFlightSegments(Collection colFlightReservationAlertDTO, FlightAlertDTO flightAlertDTO)
			throws ModuleException {

		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Email ids list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(flightAlertDTO.getEmailTo());
		messageList.add(user);

		// Profile List
		List messageProfiles = new ArrayList();
		FlightReservationAlertDTO flightReservationAlertDTO;
		Collection colReservationSegment;

		for (Iterator itFlightReservationAlertDTO = colFlightReservationAlertDTO.iterator(); itFlightReservationAlertDTO
				.hasNext();) {

			flightReservationAlertDTO = (FlightReservationAlertDTO) itFlightReservationAlertDTO.next();

			if (flightReservationAlertDTO.getFlightSegmentIds().size() > 0) {
				// Get pnr for the pnr segments
				colReservationSegment = reservationSegmentDAO.getReservationSegments(
						flightReservationAlertDTO.getFlightSegmentIds(), null,
						ReservationInternalConstants.ReservationStatus.CONFIRMED, false, false);

				// Proceed only if pnr exists
				if (colReservationSegment != null && colReservationSegment.size() > 0) {
					// Pnr list
					List pnrList = new ArrayList();

					Iterator itColReservationSegment = colReservationSegment.iterator();

					while (itColReservationSegment.hasNext()) {
						ReservationSegment reservationSegment = (ReservationSegment) itColReservationSegment.next();
						if (reservationSegment.getOpenReturnConfirmBeforeZulu() != null) {
							continue;
						}
						// Build pnr list
						pnrList.add(reservationSegment.getReservation().getPnr());
					}

					MessageProfile profile = new MessageProfile();
					profile.setUserMessagings(messageList);

					Topic topic = new Topic();
					HashMap map = new HashMap();

					map.put("subject", flightAlertDTO.getEmailSubject());
					map.put("description", flightAlertDTO.getEmailContent());

					map.put("alertTemplateEnum", flightReservationAlertDTO.getAlertTemplateIds());
					map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
					map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());

					// Build flight segment Id Collection
					map.put("pnrList", pnrList);

					topic.setTopicParams(map);
					topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.FLIGHT_GENERAL_EMAIL);
					topic.setLocale(Locale.ENGLISH);
					profile.setTopic(topic);
					messageProfiles.add(profile);
				}
			}

		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateEMailsForSeatLost(Collection colFlightReservationAlertDTO, FlightAlertDTO flightAlertDTO)
			throws ModuleException {

		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Email ids list

		// Profile List
		List messageProfiles = new ArrayList();
		FlightReservationAlertDTO flightReservationAlertDTO;
		Collection colReservationSegment;
		Iterator itFlightReservationAlertDTO = colFlightReservationAlertDTO.iterator();

		while (itFlightReservationAlertDTO.hasNext()) {

			flightReservationAlertDTO = (FlightReservationAlertDTO) itFlightReservationAlertDTO.next();

			if (flightReservationAlertDTO.getFlightSeatChangesInfo().size() > 0) {
				Iterator itColReservationSegment = flightReservationAlertDTO.getFlightSeatChangesInfo().iterator();
				while (itColReservationSegment.hasNext()) {
					FlightChangeInfo flightCInfo = (FlightChangeInfo) itColReservationSegment.next();
					MessageProfile profile = new MessageProfile();
					List messageUser = new ArrayList();
					UserMessaging userEmails = new UserMessaging();
					List messageList = new ArrayList();
					UserMessaging user = new UserMessaging();
					user.setToAddres(flightCInfo.getEmail());
					messageList.add(user);

					messageUser.add(userEmails);
					profile.setUserMessagings(messageList);

					Topic topic = new Topic();
					HashMap map = new HashMap();

					map.put("subject", "Subject");
					map.put("message",
							"Your reservation seat " + flightCInfo.getSeatNumber() + " for reservation " + flightCInfo.getPnr()
									+ " lost due to the flight aircraft model change. Please book a new seat");
					map.put("user", flightCInfo.getReservationName());
					map.put("pnr", flightCInfo.getPnr());
					// map.put("pnr", "");
					// map.put("alertTemplateEnum", flightReservationAlertDTO.getAlertTemplateIds());
					// map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
					// map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());

					topic.setTopicParams(map);
					topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.FLIGHT_SEATCHANGE_EMAIL);
					topic.setLocale(Locale.ENGLISH);
					profile.setTopic(topic);
					messageProfiles.add(profile);
				}
			}

		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	/**
	 * Generate Emails for cancel reservation segments
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param flightAlertDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateEMailsForPnrSegments(Collection colFlightReservationAlertDTO, FlightAlertDTO flightAlertDTO)
			throws ModuleException {

		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		MessageProfile profile;
		// Email ids list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(flightAlertDTO.getEmailTo());
		messageList.add(user);

		// Profile List
		List messageProfiles = new ArrayList();
		EffectedFlightSegmentDTO effectedFlightSegmentDTO;
		Collection colReservationSegment;
		FlightReservationAlertDTO flightReservationAlertDTO;

		for (Iterator itColFlightReservationAlertDTO = colFlightReservationAlertDTO.iterator(); itColFlightReservationAlertDTO
				.hasNext();) {
			flightReservationAlertDTO = (FlightReservationAlertDTO) itColFlightReservationAlertDTO.next();

			// Pnr list
			List pnrList = new ArrayList();

			for (Iterator itColEffectedFlightSegmentDTO = flightReservationAlertDTO.getColEffectedFlightSegmentDTO().iterator(); itColEffectedFlightSegmentDTO
					.hasNext();) {

				effectedFlightSegmentDTO = (EffectedFlightSegmentDTO) itColEffectedFlightSegmentDTO.next();

				if (effectedFlightSegmentDTO.getActualPnrSegmentIds().size() > 0) {
					// Get pnr for the pnr segments
					colReservationSegment = reservationSegmentDAO.getReservationSegments(null,
							effectedFlightSegmentDTO.getActualPnrSegmentIds(), null, false, false);

					// Proceed only if pnr exists
					if (colReservationSegment != null && colReservationSegment.size() > 0) {
						Iterator itColReservationSegment = colReservationSegment.iterator();

						while (itColReservationSegment.hasNext()) {
							ReservationSegment reservationSegment = (ReservationSegment) itColReservationSegment.next();
							if (reservationSegment.getOpenReturnConfirmBeforeZulu() != null) {
								continue;
							}
							// Build pnr list
							pnrList.add(reservationSegment.getReservation().getPnr());
						}
					}
				}
			}

			profile = new MessageProfile();
			profile.setUserMessagings(messageList);

			Topic topic = new Topic();
			HashMap map = new HashMap();

			map.put("subject", flightAlertDTO.getEmailSubject());
			map.put("description", flightAlertDTO.getEmailContent());

			map.put("alertTemplateEnum", flightReservationAlertDTO.getAlertTemplateIds());
			map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
			map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());

			// Build flight segment Id Collection
			map.put("pnrList", pnrList);
			topic.setLocale(Locale.ENGLISH);
			topic.setTopicParams(map);
			topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.FLIGHT_GENERAL_EMAIL);
			profile.setTopic(topic);
			messageProfiles.add(profile);
		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	/**
	 * Generate reservation alerts for pnr segments
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param flightAlertDTO
	 * @param strReservationAlertType
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateAlertsForPnrSegments(Collection colFlightReservationAlertDTO, FlightAlertDTO flightAlertDTO,
			String strReservationAlertType) throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		FlightReservationAlertDTO flightReservationAlertDTO;

		Collection confirmedPnrSegIds = new ArrayList();

		for (Iterator itColFlightReservationAlertDTO = colFlightReservationAlertDTO.iterator(); itColFlightReservationAlertDTO
				.hasNext();) {
			flightReservationAlertDTO = (FlightReservationAlertDTO) itColFlightReservationAlertDTO.next();

			if (flightReservationAlertDTO.getFlightSegmentIds().size() > 0) {
				// Generate alerts in Alert Table
				Collection colReservationSegment = reservationSegmentDAO.getReservationSegments(
						flightReservationAlertDTO.getFlightSegmentIds(), null,
						ReservationInternalConstants.ReservationStatus.CONFIRMED, false, false);

				if (colReservationSegment != null && colReservationSegment.size() > 0) {

					confirmedPnrSegIds.addAll(this.processAlertsForFlightSegments(colReservationSegment,
							flightReservationAlertDTO.getAlertDetails(), flightReservationAlertDTO.getFlightSegments(), false,
							strReservationAlertType));
				}
			}
		}
		// Made this to update from pnr segments in order to avoid open return segments
		if (confirmedPnrSegIds.size() > 0) {
			// Set reservation alert 'ON' for PNR segments
			reservationSegmentDAO.updateReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_TRUE, null,
					confirmedPnrSegIds, ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		}
	}

	/**
	 * Generate reservation alerts for actual pnr segments
	 * 
	 * @param colEffectedFlightSegmentDTO
	 * @param strReservationAlertType
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateAlertsForActualPnrSegments(Collection colEffectedFlightSegmentDTO, String alertType)
			throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		EffectedFlightSegmentDTO effectedFlightSegmentDTO;
		Collection confirmActualPnrSegmentIds = new ArrayList();

		for (Iterator itColEffectedFlightSegmentDTO = colEffectedFlightSegmentDTO.iterator(); itColEffectedFlightSegmentDTO
				.hasNext();) {
			effectedFlightSegmentDTO = (EffectedFlightSegmentDTO) itColEffectedFlightSegmentDTO.next();

			if (effectedFlightSegmentDTO.getActualPnrSegmentIds().size() > 0) {
				// Generate alerts in Alert Table
				Collection colReservationSegment = reservationSegmentDAO.getReservationSegments(null,
						effectedFlightSegmentDTO.getActualPnrSegmentIds(), null, false, false);

				if (colReservationSegment != null && colReservationSegment.size() > 0) {
					FlightReservationAlertDTO flightReservationAlertDTO = this
							.getFlightReservationAlertDTO(effectedFlightSegmentDTO.getFlightSegmentId());

					confirmActualPnrSegmentIds.addAll(this.processAlertsForFlightSegments(colReservationSegment,
							flightReservationAlertDTO.getAlertDetails(), flightReservationAlertDTO.getFlightSegments(), false,
							alertType));
				}
			}
		}

		if (confirmActualPnrSegmentIds.size() > 0) {
			// Set reservation alert 'ON' for PNR segments
			reservationSegmentDAO.updateReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_TRUE, null,
					confirmActualPnrSegmentIds, null);
		}
	}

	/**
	 * Return the mapping FlightReservationAlertDTO
	 * 
	 * @param flightSegmentId
	 * @return
	 * @throws ModuleException
	 */
	private FlightReservationAlertDTO getFlightReservationAlertDTO(Integer flightSegmentId) throws ModuleException {
		Iterator<FlightReservationAlertDTO> itFlightReservationAlertDTO = this.colFlightReservationAlertDTO.iterator();
		FlightReservationAlertDTO flightReservationAlertDTO;
		FlightReservationAlertDTO flightReservationAlertDTOHolder = null;

		while (itFlightReservationAlertDTO.hasNext()) {
			flightReservationAlertDTO = (FlightReservationAlertDTO) itFlightReservationAlertDTO.next();

			if (flightReservationAlertDTO.getFlightSegments().containsKey(flightSegmentId)) {
				flightReservationAlertDTOHolder = flightReservationAlertDTO;
				break;
			}
		}

		if (flightReservationAlertDTOHolder == null) {
			throw new ModuleException("airreservations.arg.cannotFindFlightSegments");
		}

		return flightReservationAlertDTOHolder;
	}

	/**
	 * Process alerts for effected flight segment
	 * 
	 * @param colReservationSegment
	 * @param flightSegmentCode
	 * @param strReservationAlertType
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void processAlertsForEffectedFlightSegment(Collection colReservationSegment, String flightSegmentCode,
			String strReservationAlertType) throws ModuleException {
		AlertingBD alertingBD = ReservationModuleUtils.getAlertingBD();

		HashMap mapAlertDetails = this.createAlertsMap(flightSegmentCode);
		String alertMessage = this.buildGeneralAlertMessage(mapAlertDetails);

		// Generate alert message for each flight segment id.
		List alertList = new ArrayList();
		ReservationSegment reservationSegment;
		String completeAlertMessage;
		Alert alert;

		for (Iterator itColReservationSegment = colReservationSegment.iterator(); itColReservationSegment.hasNext();) {
			reservationSegment = (ReservationSegment) itColReservationSegment.next();

			completeAlertMessage = Util.setStringToPlaceHolderIndex(alertMessage, AlertTemplateEnum.TemplateParams.PNR,
					reservationSegment.getReservation().getPnr());

			alert = new Alert(completeAlertMessage, reservationSegment.getPnrSegId());

			alertList.add(alert);
		}

		// Record Modifications
		ReservationBO.recordAlertModifications(colReservationSegment, credentialsDTO, AuditTemplateEnum.CREATED_ALERT, null,
				strReservationAlertType);

		// Add alerts
		alertingBD.addAlerts(alertList);
	}

	/**
	 * Create alerts map
	 * 
	 * @param flightSegmentCode
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private HashMap createAlertsMap(String flightSegmentCode) {
		HashMap alertDetails = new HashMap();
		HashMap alertParams = new HashMap();

		alertParams.put(AlertTemplateEnum.TemplateParams.CANCELLED_SEGMENT, flightSegmentCode);
		alertDetails.put(AlertTemplateEnum.TEMPLATE_AFFECTED_SEGMENT, alertParams);

		return alertDetails;
	}

	/**
	 * Generate segment alerts for flight segments in the Alert table
	 * 
	 * @param colReservationSegment
	 *            Collection of PNR segments
	 * @param mapAlertDetails
	 *            type of HashMap with Template Id as the Key, which each row consits of a HashMap of values for each
	 *            paramter in the relevant template
	 * @param mapFlightSegments
	 *            type of HashMap with flight segment id as the key and as the value a hashmap that contains pnr,
	 *            segment code, old & new departure time and old & new arrival time details
	 * @param returnFlgSegIds
	 * @param strReservationAlertType
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection processAlertsForFlightSegments(Collection colReservationSegment, HashMap mapAlertDetails,
			HashMap mapFlightSegments, boolean returnFlgSegIds, String strReservationAlertType) throws ModuleException {

		AlertingBD alertingBD = ReservationModuleUtils.getAlertingBD();
		String alertMessage = this.buildGeneralAlertMessage(mapAlertDetails);

		// Generate alert message for each flight segment id.
		List alertList = new ArrayList();
		Collection colReturnKeyIds = new ArrayList();
		ReservationSegment reservationSegment;
		String completeAlertMessage;
		Alert alert;
		Date oldSegmentDeparture = null;

		for (Iterator itColReservationSegment = colReservationSegment.iterator(); itColReservationSegment.hasNext();) {
			reservationSegment = (ReservationSegment) itColReservationSegment.next();
			// skip open return dummy segments
			if (reservationSegment.getOpenReturnConfirmBeforeZulu() != null)
				continue;
			// Get the corresponding hashmap for the flight segment id
			HashMap alertMesgReplaceKeyValuePairs = (HashMap) mapFlightSegments.get(reservationSegment.getFlightSegId());

			// Add PNR number to the hash map
			alertMesgReplaceKeyValuePairs.put(AlertTemplateEnum.TemplateParams.PNR, reservationSegment.getReservation().getPnr());

			// Get the completed message
			completeAlertMessage = Util.setStringToPlaceHolderIndex(alertMessage, alertMesgReplaceKeyValuePairs);

			HashMap map = (HashMap) mapAlertDetails.get(mapAlertDetails.keySet().toArray()[0]);

			Set keySet = map.keySet();
			if (keySet != null) {
				Iterator itrKeys = keySet.iterator();

				while (itrKeys.hasNext()) {
					Object key = (Object) itrKeys.next();
					if (key.equals(AlertTemplateEnum.TemplateParams.OLD_DEPARTURE_DATE)) {
						Date oldDepartureDate = (Date) map.get(key);
						oldSegmentDeparture = new Date(oldDepartureDate.getTime());
						break;
					}
				}

			}
			alert = new Alert(completeAlertMessage, reservationSegment.getPnrSegId());
			if (oldSegmentDeparture != null) {
				alert.setOriginalDepDate(oldSegmentDeparture);
			}
			alertList.add(alert);

			// Prepare the return key ids
			if (returnFlgSegIds) {
				colReturnKeyIds.add(reservationSegment.getFlightSegId());
			} else {
				colReturnKeyIds.add(reservationSegment.getPnrSegId());
			}
		}

		// Record Modifications
		ReservationBO.recordAlertModifications(colReservationSegment, credentialsDTO, AuditTemplateEnum.CREATED_ALERT, null,
				strReservationAlertType);

		// Add alerts
		alertingBD.addAlerts(alertList);

		return colReturnKeyIds;
	}

	/**
	 * Builds general alert message for given parameter values in the HashMap
	 * 
	 * @param mapAlertDetails
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String buildGeneralAlertMessage(HashMap mapAlertDetails) throws ModuleException {
		AlertingBD alertingBD = ReservationModuleUtils.getAlertingBD();
		AlertTemplateEnum templateID;
		AlertTemplate alertTemplate;
		String alertMessage = "";

		// Get each reffering alert Template
		for (Iterator iterAlertDetails = mapAlertDetails.keySet().iterator(); iterAlertDetails.hasNext();) {
			templateID = (AlertTemplateEnum) iterAlertDetails.next();

			if (templateID == null) {
				throw new ModuleException("airreservations.arg.invalidAlertTemplateID");
			}

			// Get alert template
			alertTemplate = alertingBD.getAlertTemplate(templateID);

			if (alertTemplate == null) {
				throw new ModuleException("airreservations.arg.invalidAlertTemplateID");
			}

			// Build alert message for current template
			alertMessage += Util.setStringToPlaceHolderIndex(alertTemplate.getAlertTemplateContent(),
					(HashMap) mapAlertDetails.get(templateID));

		}

		return alertMessage;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param isRescheduleFlight
	 * @param isCancelFlight
	 * @param flightAlertDTO
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Collection colFlightReservationAlertDTO, Boolean isRescheduleFlight, Boolean isCancelFlight,
			FlightAlertDTO flightAlertDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (colFlightReservationAlertDTO == null || isRescheduleFlight == null || isCancelFlight == null
				|| flightAlertDTO == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
	
	
	public static List<String> toNotifyAnOfficial() {
		List<String>  mobileNumbers = null;

			try {
				mobileNumbers = ReservationModuleUtils.getReservationBD().getOfficersMobileNumbersList();
			} catch (ModuleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return mobileNumbers;
	}

}
