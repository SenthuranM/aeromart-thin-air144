/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.InsuranceHelper;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

/**
 * Command for creating a modification object for updating a reservation payment
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cMForUpdatePayment"
 */
public class CMForUpdatePayment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CMForUpdatePayment.class);

	/**
	 * Execute method of the CMForUpdatePayment command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Boolean triggerPnrForceConfirmed = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		Map<Integer, PaymentAssembler> pnrPaxIdAndPayments = (Map<Integer, PaymentAssembler>) this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		List<InsuranceResponse> insuranceResponses = (List<InsuranceResponse>) this.getParameter(CommandParamNames.INSURANCE_RES);

		// Checking params
		this.validateParams(pnr, reservation, pnrPaxIdAndPayments, triggerPnrForceConfirmed, credentialsDTO);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		// get insurance policy info for audit content
		String insuranceAudit = InsuranceHelper.getAudit(insuranceResponses);

		// Compose audit
		Collection<ReservationAudit> colReservationAudit = this.composeAudit(pnr, reservation, triggerPnrForceConfirmed, pnrPaxIdAndPayments,
				credentialsDTO, insuranceAudit);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		if (this.getParameter(CommandParamNames.SPLITTED_PAX_IDS) != null) {
			response.addResponceParam(CommandParamNames.SPLITTED_PAX_IDS, this.getParameter(CommandParamNames.SPLITTED_PAX_IDS));
		}

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Compose Audit
	 * 
	 * @param pnr
	 * @param reservation
	 * @param triggerPnrForceConfirmed
	 * @param pnrPaxIdAndPayments
	 * @param credentialsDTO
	 * @param insurancePolicy
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationAudit> composeAudit(String pnr, Reservation reservation, Boolean triggerPnrForceConfirmed,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, String insurancePolicy) throws ModuleException {
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		// If force confirm happens
		if (triggerPnrForceConfirmed.booleanValue()) {
			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setPnr(pnr);
			reservationAudit.setModificationType(AuditTemplateEnum.PAYMENT_MADE.getCode());
			reservationAudit.setUserNote(null);

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentMade.FORCE_CONFIRM,
					ReservationTemplateUtils.AUDIT_FORCE_CONFIRM_TEXT);

			// Setting the ip address
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentMade.IP_ADDDRESS, credentialsDTO
						.getTrackInfoDTO().getIpAddress());
			}

			// Setting the origin carrier code
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentMade.ORIGIN_CARRIER, credentialsDTO
						.getTrackInfoDTO().getCarrierCode());
			}
			if (insurancePolicy != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.INSURANCE, insurancePolicy);
			}
			colReservationAudit.add(reservationAudit);
		} else {
			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setPnr(pnr);
			reservationAudit.setModificationType(AuditTemplateEnum.PAYMENT_MADE.getCode());
			reservationAudit.setUserNote(null);

			// Get the passenger payment information
			String audit = ReservationCoreUtils.getPassengerPaymentInfo(pnrPaxIdAndPayments, reservation);

			// Setting the new pnr information
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentMade.PASSENGERS_PAYMENTS, audit);

			// Setting the ip address
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentMade.IP_ADDDRESS, credentialsDTO
						.getTrackInfoDTO().getIpAddress());
			}

			// Setting the origin carrier code
			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PaymentMade.ORIGIN_CARRIER, credentialsDTO
						.getTrackInfoDTO().getCarrierCode());
			}
			if (insurancePolicy != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.INSURANCE, insurancePolicy);
			}

			colReservationAudit.add(reservationAudit);
		}

		return colReservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param triggerPnrForceConfirmed
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Reservation reservation, Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Boolean triggerPnrForceConfirmed,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || reservation == null || pnrPaxIdAndPayments == null || triggerPnrForceConfirmed == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}
}
