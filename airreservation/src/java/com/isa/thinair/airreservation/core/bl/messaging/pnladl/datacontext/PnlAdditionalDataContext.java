/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext;

import java.util.Map;

import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;

/**
 * @author udithad
 *
 */
public class PnlAdditionalDataContext extends PnlDataContext {

	private PassengerCollection passengerCollection;

	public PassengerCollection getPassengerCollection() {
		return passengerCollection;
	}

	public void setPassengerCollection(PassengerCollection passengerCollection) {
		this.passengerCollection = passengerCollection;
	}

}
