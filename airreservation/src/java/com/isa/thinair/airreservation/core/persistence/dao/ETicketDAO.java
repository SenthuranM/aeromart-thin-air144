package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegEtktTransition;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.utils.TicketGeneratorCriteriaType;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface ETicketDAO {

	public void saveOrUpdate(Collection<ReservationPaxFareSegmentETicket> eTickets) throws ModuleException;

	public Collection<ReservationPaxFareSegmentETicket> getReservationPaxFareSegmentETickets(Integer pnrPaxId)
			throws ModuleException;

	public Collection<EticketTO> getReservationETickets(String pnr) throws ModuleException;

	public String getNextIATAETicketSQID() throws ModuleException;

	public String getNextIATAETicketSQID(String sequenceName) throws ModuleException;

	public ReservationPaxFareSegmentETicket getPaxFareSegmentEticket(String eticketNumber, int coupNumber,
			String msgType) throws ModuleException;

	public EticketTO getPassengerETicketDetail(Integer pnrPaxId, Integer flightId) throws ModuleException;

	public String getAgentEticketSequnceName(String agentCode, TicketGeneratorCriteriaType criteriaType) throws ModuleException;

	public void updatePaxFareSegmentsEticketStatus(Collection<Integer> colPpfsId);

	public Integer getPassengerIdByETicketNumber(String eticketNumber) throws ModuleException;

	public String getPNRByEticketNo(String eticketNumber);

	public Map<String, Set<String>> getPNRsByEticketNo(List<String> eticketNumbers) throws ModuleException;

	public Collection<ReservationPaxFareSegmentETicket> getReservationPaxFareSegmentETickets(
			Collection<Integer> pnrPaxFareSegmentIds) throws ModuleException;

	public void updatePassengerCouponStatus(Integer couponId, String couponStatus) throws ModuleException;

	public void updatePassengerStatus(Integer couponId, String paxStatus) throws ModuleException;

	public EticketTO getEticketInfo(String eticketNumber, Integer coupNumber) throws ModuleException;

	public String getBookingClassType(String eticketNumber, Integer coupNumber) throws ModuleException;

	public void saveOrUpdate(ReservationPaxFareSegEtktTransition eTicketTransition) throws ModuleException;

	public LccClientPassengerEticketInfoTO getEticketInfo(Integer couponId) throws ModuleException;
	
	public EticketTO getEticketDetailsByExtETicketNumber(String extEticketNumber, Integer extCoupNumber) throws ModuleException;
	
	public boolean isCodeSharePNR(String extEticketNumber)throws ModuleException;

	public void removeEtickets(Collection<Integer> pnrPaxFareSegIds) throws ModuleException;
	
	public Collection<Integer> getPaxFareSegmentETicketIDs(Collection<Integer> pnrSegIds) throws ModuleException;

}
