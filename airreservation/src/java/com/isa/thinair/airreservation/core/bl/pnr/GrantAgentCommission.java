package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndChgCommission;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.util.AgentCommissionBinder;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for applying agent commission
 * 
 * 1) bind reservation level entries in memory : t_pnr_pax_ond_chg_commission(DR)
 * 
 * 2) create agent transaction entries
 * 
 * 3) update agent available credits
 * 
 * @isa.module.command name="grantAgentCommission"
 * @author rimaz
 */
public class GrantAgentCommission extends DefaultBaseCommand {

	private static final Log log = LogFactory.getLog(GrantAgentCommission.class);
	private final FareRuleBD fareRuleBD;
	private final TravelAgentFinanceBD travelAgentFinanceBD;

	public GrantAgentCommission() {
		fareRuleBD = ReservationModuleUtils.getFareRuleBD();
		travelAgentFinanceBD = ReservationModuleUtils.getTravelAgentFinanceBD();
	}

	@Override
	public ServiceResponce execute() throws ModuleException {

		log.debug("Inside AgentCommission command");

		@SuppressWarnings("unchecked")
		Collection<PaymentInfo> colPaymentInfo = (Collection<PaymentInfo>) this.getParameter(CommandParamNames.PAYMENT_INFO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Boolean isCapturePayment = (Boolean) this.getParameter(CommandParamNames.IS_CAPTURE_PAYMENT);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		AgentCommissionDetails agentCommissionDetails = (AgentCommissionDetails) this
				.getParameter(CommandParamNames.AGENT_COMMISSION_DTO);
		Boolean offerAsPendingCommission = (Boolean) this.getParameter(CommandParamNames.ONHOLD_OR_FORCE_CONFIRM);
		if (offerAsPendingCommission == null) {
			offerAsPendingCommission = false;
		}

		Set<Integer> commissionApplicableFareIds = new HashSet<Integer>();
		String performingAgent = null;
		if (agentCommissionDetails != null) {
			commissionApplicableFareIds = agentCommissionDetails.getCommissionApplicableFareIds();
		}
		boolean grantAgentCommission = isCapturePayment && !commissionApplicableFareIds.isEmpty() && credentialsDTO != null;

		if (grantAgentCommission && this.allowedPaymentTypes(colPaymentInfo)) {
			performingAgent = credentialsDTO.getAgentCode();
			String pnr = reservation.getPnr();
			Iterator<ReservationPax> reservationPaxIt = reservation.getPassengers().iterator();
			ReservationPax reservationPax = null;
			while (reservationPaxIt.hasNext()) {
				reservationPax = reservationPaxIt.next();
				if (ReservationApiUtils.isAdultType(reservationPax) || ReservationApiUtils.isChildType(reservationPax)) {
					processAgentCommissionForAdultOrChild(reservationPax, pnr, commissionApplicableFareIds,
							offerAsPendingCommission, performingAgent);
				}
			}
		}

		log.debug("Leaving AgentCommission");

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		return response;
	}

	private boolean allowedPaymentTypes(Collection<PaymentInfo> colPaymentInfo) {

		boolean isAllowedPaymentType = true;
		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();

		while (itColPaymentInfo.hasNext()) {
			PaymentInfo paymentInfo = (PaymentInfo) itColPaymentInfo.next();
			if (paymentInfo.getPayCarrier().equals(AppSysParamsUtil.getDefaultCarrierCode())) {

				if (paymentInfo instanceof AgentCreditInfo) {
					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					// TODO - after clarifications: validations on payments methods for agents,
				}

			}
		}
		return isAllowedPaymentType;
	}

	private void processAgentCommissionForAdultOrChild(ReservationPax reservationPax, String pnr,
			Set<Integer> commissionApplicableFareIds, boolean offerAsPendingCommission, String performingAgent)
			throws ModuleException {

		BigDecimal agentCommissionByPax = AccelAeroCalculator.getDefaultBigDecimalZero();
		Set<ReservationPaxFare> resPaxFares = reservationPax.getPnrPaxFares();

		for (ReservationPaxFare resPaxFare : resPaxFares) {
			for (ReservationPaxOndCharge resPaxOndCharge : resPaxFare.getCharges()) {
				if (resPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
					if (resPaxOndCharge.getPnrPaxOndChgId() == null) {
						BigDecimal perPaxFreshCommission = offerCommissionsForFreshOnd(resPaxOndCharge,
								commissionApplicableFareIds, offerAsPendingCommission, performingAgent);
						agentCommissionByPax = AccelAeroCalculator.add(agentCommissionByPax, perPaxFreshCommission);
					} else if (!offerAsPendingCommission) {
						BigDecimal pendingConfirmedCommission = processPendingCommssions(resPaxOndCharge, performingAgent);
						agentCommissionByPax = AccelAeroCalculator.add(agentCommissionByPax, pendingConfirmedCommission);
					}
				}
			}
		}

		// per pax agent transaction updated
		if (agentCommissionByPax.compareTo(BigDecimal.ZERO) > 0) {
			travelAgentFinanceBD.recordAgentCommission(performingAgent, agentCommissionByPax, pnr, pnr, null);
		}
	}

	private BigDecimal offerCommissionsForFreshOnd(ReservationPaxOndCharge resPaxOndCharge,
			Set<Integer> commissionApplicableFareIds, boolean offerAsPendingCommission, String performingAgent)
			throws ModuleException {
		BigDecimal confirmedCommission = AccelAeroCalculator.getDefaultBigDecimalZero();
		Integer fareId = resPaxOndCharge.getFareId();
		if (commissionApplicableFareIds.contains(fareId)) {

			BigDecimal paxOndFareCommission = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal paxOndFareAmount = resPaxOndCharge.getAmount();
			String userId = resPaxOndCharge.getUserId();
			FareRule applicableFareRule = fareRuleBD.getFareRuleForFareID(fareId);
			String agentCommissionType = applicableFareRule.getAgentCommissionType();
			BigDecimal commissionByFareRule = null;

			if (applicableFareRule.getAgentCommissionAmount() != null) {
				commissionByFareRule = new BigDecimal(applicableFareRule.getAgentCommissionAmount());
			}

			if (agentCommissionType != null && commissionByFareRule != null
					&& commissionByFareRule.compareTo(BigDecimal.ZERO) > 0) {

				if (agentCommissionType.equals(FareRule.CHARGE_TYPE_V)) {
					paxOndFareCommission = commissionByFareRule;
				} else if (agentCommissionType.equals(FareRule.CHARGE_TYPE_PF)) {
					paxOndFareCommission = calculatePercentage(paxOndFareAmount, commissionByFareRule);
				}

				if (paxOndFareCommission.compareTo(BigDecimal.ZERO) > 0) {
					if (offerAsPendingCommission) {
						AgentCommissionBinder.bindDebitInstance(resPaxOndCharge, paxOndFareCommission, performingAgent, userId,
								AirTravelAgentConstants.AgentCommissionStatus.PENDING);
					} else {
						confirmedCommission = confirmedCommission.add(paxOndFareCommission);
						AgentCommissionBinder.bindDebitInstance(resPaxOndCharge, paxOndFareCommission, performingAgent, userId,
								AirTravelAgentConstants.AgentCommissionStatus.CONFIRMED);
					}

				}
			}
		}
		return confirmedCommission;
	}

	private BigDecimal processPendingCommssions(ReservationPaxOndCharge resPaxOndCharge, String performingAgent) {
		BigDecimal agentCommissionByPax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (isConfirmedNotCheckedSegment(resPaxOndCharge)) {
			Set<ReservationPaxOndChgCommission> appliedCommissions = resPaxOndCharge.getResPaxOndChgCommission();
			if (appliedCommissions != null && !appliedCommissions.isEmpty()) {
				for (ReservationPaxOndChgCommission appliedCommission : appliedCommissions) {
					String offeredCommisssionStatus = appliedCommission.getStatus();
					String commissionOwnerAgent = appliedCommission.getAgentCode();
					if (offeredCommisssionStatus.equals(AirTravelAgentConstants.AgentCommissionStatus.PENDING)) {
						if (commissionOwnerAgent.equals(performingAgent)) {
							appliedCommission.setStatus(AirTravelAgentConstants.AgentCommissionStatus.CONFIRMED);
							agentCommissionByPax = agentCommissionByPax.add(appliedCommission.getAmount());
						} else {
							appliedCommission.setStatus(AirTravelAgentConstants.AgentCommissionStatus.CANCELED);
						}
					}
				}
			}
		}

		return agentCommissionByPax;
	}

	private boolean isConfirmedNotCheckedSegment(ReservationPaxOndCharge resPaxOndCharge) {
		ReservationPaxFare resPaxFare = resPaxOndCharge.getReservationPaxFare();
		boolean isNotCheckedConfirmed = false;
		for (ReservationPaxFareSegment paxFareSeg : resPaxFare.getPaxFareSegments()) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(paxFareSeg.getSegment().getStatus())) {
				if (paxFareSeg.getCheckinStatus().equals(ReservationInternalConstants.PassengerCheckinStatus.TO_BE_CHECKED_IN)
						&& paxFareSeg.getStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED)) {
					isNotCheckedConfirmed = true;
				}
			}
		}
		return isNotCheckedConfirmed;
	}

	private BigDecimal calculatePercentage(BigDecimal input, BigDecimal percentage) {
		return AccelAeroCalculator.divide(AccelAeroCalculator.multiply(input, percentage), 100);
	}

}
