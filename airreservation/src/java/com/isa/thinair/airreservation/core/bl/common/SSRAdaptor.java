package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxSegmentSSRStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SSRUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class SSRAdaptor {
	// private static Log log = LogFactory.getLog(SSRAdaptor.class);

	private static Map<String, Integer> oldAirportMap = new HashMap<String, Integer>();
	private static Map<Integer, String> newAirportMap = new HashMap<Integer, String>();
	private static Map<String, Integer> paxSSRCount = new HashMap<String, Integer>();

	public static Map<String, List<PaxSSRDTO>> reprotectAirportServices(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Integer pnrSegId, CredentialsDTO credentialsDTO, Integer flightSegId, boolean isSegmentTransferredByNorecProcess,
			BundledFareDTO bundledFareDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		Map<String, List<PaxSSRDTO>> paxSSRToRemove = new HashMap<String, List<PaxSSRDTO>>();
		Map<String, List<PaxSSRDTO>> paxSSRToRetain = new HashMap<String, List<PaxSSRDTO>>();

		// populateSSRInfo(reservation, pnrSegId);

		String segmentCode = getSourceSegmentCode(pnrSegId, reservation);

		FlightSegement targetFlightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(flightSegId);
		boolean skipeCutoverValidation = false;
		if (isSegmentTransferredByNorecProcess && AppSysParamsUtil.skipAnciModificationCutoffForNorec()) {
			skipeCutoverValidation = true;
		}

		// Populate Maps for airports in new & old segments
		String[] oldAirportArr = segmentCode.split("/");
		for (int i = 0; i < oldAirportArr.length; i++) {
			oldAirportMap.put(oldAirportArr[i], i);
		}

		String[] newAirportArr = targetFlightSegment.getSegmentCode().split("/");
		for (int i = 0; i < newAirportArr.length; i++) {
			newAirportMap.put(i, newAirportArr[i]);
		}

		Map<Integer, List<String>> TargetAirportsMap = populateAirportsForFlightSegment(targetFlightSegment);

		// Get Available airport services for airports in flight segment
		Map<String, List<AirportServiceDTO>> targetAPSMap = ReservationModuleUtils.getSsrServiceBD()
				.getAvailableAirportServices(TargetAirportsMap, skipeCutoverValidation);
		targetAPSMap = populateAirportWiseService(targetAPSMap);

		Set<ReservationPax> resPaxs = (Set<ReservationPax>) reservation.getPassengers();
		if (resPaxs != null) {
			for (ReservationPax resPax : resPaxs) {
				Collection<PaxSSRDTO> colPaxSSR = resPax.getPaxSSR();

				if (colPaxSSR != null) {
					Collection<PaxSSRDTO> affectiveSSRs = getAffectivePaxSSRs(colPaxSSR, pnrSegId);

					for (PaxSSRDTO paxSSRDTO : affectiveSSRs) {

						// Check the selected service is an airport service
						if (paxSSRDTO.getAirportCode() != null) {
							boolean isReprotectable = isAirportServiceReprotectable(targetAPSMap, paxSSRDTO);
							isReprotectable = !BundledServiceAdaptor.isServiceIncludedInBundle(bundledFareDTO,
									EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());
							String uniqueKey = makeUniqueIdForModifications(paxSSRDTO.getPnrPaxId(), paxSSRDTO.getPnrSegId(),
									paxSSRDTO.getPnrPaxSegmentSSRId());

							if (!isReprotectable) {

								if (paxSSRToRemove.get(uniqueKey) == null) {
									paxSSRToRemove.put(uniqueKey, new ArrayList<PaxSSRDTO>());
								}

								paxSSRDTO.setChgDTO(getExternalChgDTOForSSR(new BigDecimal(paxSSRDTO.getChargeAmount()),
										credentialsDTO.getTrackInfoDTO(), EXTERNAL_CHARGES.AIRPORT_SERVICE));

								paxSSRToRemove.get(uniqueKey).add(paxSSRDTO);
							} else {

								if (paxSSRToRetain.get(uniqueKey) == null) {
									paxSSRToRetain.put(uniqueKey, new ArrayList<PaxSSRDTO>());
								}

								paxSSRToRetain.get(uniqueKey).add(paxSSRDTO);
							}
						}

					}
				}

			}
		}

		// Refund SSR charges
		if (paxSSRToRemove.size() > 0 || paxSSRToRetain.size() > 0) {
			updateSSRToCancelAndUpdate(reservation, paxSSRToRemove, paxSSRToRetain, SSRCategory.ID_AIRPORT_SERVICE);
			if (paxSSRToRemove.size() > 0) {
				addPassengerSSRChargeAndAdjustCredit(adjustCreditBO, reservation, paxSSRToRemove, "", null, credentialsDTO,
						chgTnxGen);
			}

			// Save the reservation
			ReservationProxy.saveReservation(reservation);
		}

		return paxSSRToRemove;
	}

	public static void reprotectAirportServicesWithoutCancelling(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Integer pnrSegId, CredentialsDTO credentialsDTO, Integer flightSegId) throws ModuleException {

		Map<String, List<PaxSSRDTO>> paxSSRToRetain = new HashMap<String, List<PaxSSRDTO>>();

		// populateSSRInfo(reservation, pnrSegId);

		String segmentCode = getSourceSegmentCode(pnrSegId, reservation);

		FlightSegement targetFlightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(flightSegId);

		// Populate Maps for airports in new & old segments
		String[] oldAirportArr = segmentCode.split("/");
		for (int i = 0; i < oldAirportArr.length; i++) {
			oldAirportMap.put(oldAirportArr[i], i);
		}

		String[] newAirportArr = targetFlightSegment.getSegmentCode().split("/");
		for (int i = 0; i < newAirportArr.length; i++) {
			newAirportMap.put(i, newAirportArr[i]);
		}

		Map<Integer, List<String>> TargetAirportsMap = populateAirportsForFlightSegment(targetFlightSegment);

		// Get Available airport services for airports in flight segment
		Map<String, List<AirportServiceDTO>> targetAPSMap = ReservationModuleUtils.getSsrServiceBD()
				.getAvailableAirportServices(TargetAirportsMap, false);
		targetAPSMap = populateAirportWiseService(targetAPSMap);

		Set<ReservationPax> resPaxs = (Set<ReservationPax>) reservation.getPassengers();
		if (resPaxs != null) {
			for (ReservationPax resPax : resPaxs) {
				Collection<PaxSSRDTO> colPaxSSR = resPax.getPaxSSR();

				if (colPaxSSR != null) {
					Collection<PaxSSRDTO> affectiveSSRs = getAffectivePaxSSRs(colPaxSSR, pnrSegId);

					for (PaxSSRDTO paxSSRDTO : affectiveSSRs) {

						// Check the selected service is an airport service
						if (paxSSRDTO.getAirportCode() != null) {
							boolean isReprotectable = isAirportServiceReprotectable(targetAPSMap, paxSSRDTO);
							String uniqueKey = makeUniqueIdForModifications(paxSSRDTO.getPnrPaxId(), paxSSRDTO.getPnrSegId(),
									paxSSRDTO.getPnrPaxSegmentSSRId());

							if (isReprotectable) {

								if (paxSSRToRetain.get(uniqueKey) == null) {
									paxSSRToRetain.put(uniqueKey, new ArrayList<PaxSSRDTO>());
								}

								paxSSRToRetain.get(uniqueKey).add(paxSSRDTO);
							}
						}

					}
				}

			}
		}

		// Refund SSR charges
		if (paxSSRToRetain.size() > 0) {
			updateSSRToCancelAndUpdate(reservation, null, paxSSRToRetain, SSRCategory.ID_AIRPORT_SERVICE);
			// Save the reservation
			ReservationProxy.saveReservation(reservation);
		}
	}

	public static void reprotectInflightServices(AdjustCreditBO adjustCreditBO, Reservation reservation, Integer sourceFltSegId,
			Integer targetFltSegId, Integer pnrSegId, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		paxSSRCount = new HashMap<String, Integer>();
		Set<ReservationPax> resPaxs = (Set<ReservationPax>) reservation.getPassengers();
		Integer sourceTemplateId = ReservationModuleUtils.getSsrServiceBD().getTemplateIdForFlightSegment(sourceFltSegId);
		Integer targetTemplateId = ReservationModuleUtils.getSsrServiceBD().getTemplateIdForFlightSegment(targetFltSegId);

		Map<String, List<PaxSSRDTO>> paxSSRToRemove = new HashMap<String, List<PaxSSRDTO>>();
		Map<String, List<PaxSSRDTO>> paxSSRToRetain = new HashMap<String, List<PaxSSRDTO>>();

		if (sourceTemplateId != null && targetTemplateId != null && sourceTemplateId.intValue() == targetTemplateId.intValue()) {
			FlightSegement targetFlightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(targetFltSegId);

			Map<Integer, ClassOfServiceDTO> flightSegCosMap = new HashMap<Integer, ClassOfServiceDTO>();
			ClassOfServiceDTO cos = ReservationApiUtils.getClassOfServiceDTOForReservedSegment(pnrSegId, reservation);
			cos.setSegmentCode(targetFlightSegment.getSegmentCode());
			flightSegCosMap.put(targetFltSegId, cos);

			Map<Integer, List<SpecialServiceRequestDTO>> availableInflightServices = ReservationModuleUtils.getSsrServiceBD()
					.getAvailableSSRs(flightSegCosMap, SalesChannel.TRAVEL_AGENT + "",
							AppSysParamsUtil.isInventoryCheckForSSREnabled(), false, false);

			List<SpecialServiceRequestDTO> ssrList = (availableInflightServices != null)
					? availableInflightServices.get(targetFltSegId)
					: null;

			if (resPaxs != null) {
				for (ReservationPax resPax : resPaxs) {
					Collection<PaxSSRDTO> colPaxSSR = resPax.getPaxSSR();

					if (colPaxSSR != null) {
						Collection<PaxSSRDTO> affectiveSSRs = getAffectivePaxSSRs(colPaxSSR, pnrSegId);

						for (PaxSSRDTO paxSSRDTO : affectiveSSRs) {
							// Check the selected service is an in-flight service
							if (paxSSRDTO.getAirportCode() == null) {
								boolean isReprotectable = isInflightServiceReprotectable(ssrList, paxSSRDTO);
								String uniqueKey = makeUniqueIdForModifications(paxSSRDTO.getPnrPaxId(), paxSSRDTO.getPnrSegId(),
										paxSSRDTO.getPnrPaxSegmentSSRId());

								if (!isReprotectable) {

									if (paxSSRToRemove.get(uniqueKey) == null) {
										paxSSRToRemove.put(uniqueKey, new ArrayList<PaxSSRDTO>());
									}

									paxSSRDTO.setChgDTO(getExternalChgDTOForSSR(new BigDecimal(paxSSRDTO.getChargeAmount()),
											credentialsDTO.getTrackInfoDTO(), EXTERNAL_CHARGES.INFLIGHT_SERVICES));

									paxSSRToRemove.get(uniqueKey).add(paxSSRDTO);
								} else {

									if (paxSSRToRetain.get(uniqueKey) == null) {
										paxSSRToRetain.put(uniqueKey, new ArrayList<PaxSSRDTO>());
									}

									paxSSRToRetain.get(uniqueKey).add(paxSSRDTO);
								}
							}

						}
					}

				}
			}
		} else {
			// cancel all SSRs
			if (resPaxs != null) {
				for (ReservationPax resPax : resPaxs) {
					Collection<PaxSSRDTO> colPaxSSR = resPax.getPaxSSR();

					if (colPaxSSR != null) {
						Collection<PaxSSRDTO> affectiveSSRs = getAffectivePaxSSRs(colPaxSSR, pnrSegId);

						for (PaxSSRDTO paxSSRDTO : affectiveSSRs) {

							// Check the selected service is an in-flight service
							if (paxSSRDTO.getAirportCode() == null) {
								String uniqueKey = makeUniqueIdForModifications(paxSSRDTO.getPnrPaxId(), paxSSRDTO.getPnrSegId(),
										paxSSRDTO.getPnrPaxSegmentSSRId());

								if (paxSSRToRemove.get(uniqueKey) == null) {
									paxSSRToRemove.put(uniqueKey, new ArrayList<PaxSSRDTO>());
								}

								paxSSRDTO.setChgDTO(getExternalChgDTOForSSR(new BigDecimal(paxSSRDTO.getChargeAmount()),
										credentialsDTO.getTrackInfoDTO(), EXTERNAL_CHARGES.INFLIGHT_SERVICES));

								paxSSRToRemove.get(uniqueKey).add(paxSSRDTO);
							}

						}
					}

				}
			}
		}

		boolean isSsrRefundable = isSsrRefundable();

		if (isSsrRefundable) {
			// Refund SSR charges
			if (paxSSRToRemove.size() > 0 || paxSSRToRetain.size() > 0) {
				updateSSRToCancelAndUpdate(reservation, paxSSRToRemove, paxSSRToRetain, SSRCategory.ID_INFLIGHT_SERVICE);
				if (paxSSRToRemove.size() > 0) {
					addPassengerSSRChargeAndAdjustCredit(adjustCreditBO, reservation, paxSSRToRemove, "", null, credentialsDTO,
							chgTnxGen);
				}

				// Save the reservation
				ReservationProxy.saveReservation(reservation);
			}
		}

	}

	public static void reprotectInflightServicesWithoutCancelling(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Integer sourceFltSegId, Integer targetFltSegId, Integer pnrSegId, CredentialsDTO credentialsDTO)
			throws ModuleException {

		paxSSRCount = new HashMap<String, Integer>();
		Set<ReservationPax> resPaxs = (Set<ReservationPax>) reservation.getPassengers();
		Integer sourceTemplateId = ReservationModuleUtils.getSsrServiceBD().getTemplateIdForFlightSegment(sourceFltSegId);
		Integer targetTemplateId = ReservationModuleUtils.getSsrServiceBD().getTemplateIdForFlightSegment(targetFltSegId);

		Map<String, List<PaxSSRDTO>> paxSSRToRetain = new HashMap<String, List<PaxSSRDTO>>();

		if (sourceTemplateId != null && targetTemplateId != null && sourceTemplateId.intValue() == targetTemplateId.intValue()) {
			FlightSegement targetFlightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(targetFltSegId);

			Map<Integer, ClassOfServiceDTO> flightSegCosMap = new HashMap<Integer, ClassOfServiceDTO>();
			ClassOfServiceDTO cos = ReservationApiUtils.getClassOfServiceDTOForReservedSegment(pnrSegId, reservation);
			cos.setSegmentCode(targetFlightSegment.getSegmentCode());
			flightSegCosMap.put(targetFltSegId, cos);

			Map<Integer, List<SpecialServiceRequestDTO>> availableInflightServices = ReservationModuleUtils.getSsrServiceBD()
					.getAvailableSSRs(flightSegCosMap, SalesChannel.TRAVEL_AGENT + "",
							AppSysParamsUtil.isInventoryCheckForSSREnabled(), false, false);

			List<SpecialServiceRequestDTO> ssrList = (availableInflightServices != null)
					? availableInflightServices.get(targetFltSegId)
					: null;

			if (resPaxs != null) {
				for (ReservationPax resPax : resPaxs) {
					Collection<PaxSSRDTO> colPaxSSR = resPax.getPaxSSR();

					if (colPaxSSR != null) {
						Collection<PaxSSRDTO> affectiveSSRs = getAffectivePaxSSRs(colPaxSSR, pnrSegId);

						for (PaxSSRDTO paxSSRDTO : affectiveSSRs) {

							// Check the selected service is an in-flight service
							if (paxSSRDTO.getAirportCode() == null) {
								boolean isReprotectable = isInflightServiceReprotectable(ssrList, paxSSRDTO);
								String uniqueKey = makeUniqueIdForModifications(paxSSRDTO.getPnrPaxId(), paxSSRDTO.getPnrSegId(),
										paxSSRDTO.getPnrPaxSegmentSSRId());

								if (isReprotectable) {

									if (paxSSRToRetain.get(uniqueKey) == null) {
										paxSSRToRetain.put(uniqueKey, new ArrayList<PaxSSRDTO>());
									}

									paxSSRToRetain.get(uniqueKey).add(paxSSRDTO);
								}
							}

						}
					}

				}
			}
		}

		boolean isSsrRefundable = isSsrRefundable();

		if (isSsrRefundable) {
			// Refund SSR charges
			if (paxSSRToRetain.size() > 0) {
				updateSSRToCancelAndUpdate(reservation, null, paxSSRToRetain, SSRCategory.ID_INFLIGHT_SERVICE);
				// Save the reservation
				ReservationProxy.saveReservation(reservation);
			}
		}

	}

	
	private static boolean isSsrRefundable() {
		Charge ssrCharge = null;
		boolean isSsrRefundable = true;

		try {
			ssrCharge = ReservationModuleUtils.getChargeBD().getCharge("SSR");
			if (ssrCharge.getRefundableChargeFlag() == 0 && ssrCharge.getRefundableOnlyforModifications().equals("N")) {
				isSsrRefundable = false;
			} else {
				isSsrRefundable = true;
			}
		} catch (ModuleException e) {
			e.printStackTrace();
		}

		return isSsrRefundable;
	}

	public static void populateSSRInfo(Reservation reservation, Integer pnrSegId) throws ModuleException {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		pnrSegIds.add(pnrSegId);
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
			Collection<PaxSSRDTO> paxSSRDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getReservationSSR(pnrPaxIds,
					pnrSegIds, reservation.getContactInfo().getPreferredLanguage());
			Iterator<ReservationPax> iter = reservation.getPassengers().iterator();
			while (iter.hasNext()) {
				ReservationPax resPax = (ReservationPax) iter.next();
				Collection<PaxSSRDTO> colPaxSSR = new ArrayList<PaxSSRDTO>();
				Iterator<PaxSSRDTO> ssriter = paxSSRDTOs.iterator();
				while (ssriter.hasNext()) {
					PaxSSRDTO paxSSRDTO = (PaxSSRDTO) ssriter.next();
					if (paxSSRDTO.getPnrPaxId().intValue() == resPax.getPnrPaxId().intValue()) {
						colPaxSSR.add(paxSSRDTO);
					}
				}
				resPax.setPaxSSR(colPaxSSR);
			}
		} else {
			throw new ModuleException("airreservations.load.reservation");
		}
	}

	private static String getSourceSegmentCode(Integer pnrSegmentId, Reservation reservation) throws ModuleException {
		Integer fltSegId = null;
		if (pnrSegmentId != null && reservation.getSegments() != null) {
			for (ReservationSegment segment : (Set<ReservationSegment>) reservation.getSegments()) {
				if (segment.getPnrSegId().intValue() == pnrSegmentId.intValue()) {
					fltSegId = segment.getFlightSegId();
					break;
				}
			}
		}

		if (fltSegId != null) {
			FlightSegement sourceFlightSeg = ReservationModuleUtils.getFlightBD().getFlightSegment(fltSegId);
			return (sourceFlightSeg != null) ? sourceFlightSeg.getSegmentCode() : "";
		}

		return "";
	}

	private static void updateSSRToCancelAndUpdate(Reservation reservation, Map<String, List<PaxSSRDTO>> paxSSRToRemove,
			Map<String, List<PaxSSRDTO>> paxSSRToRetain, Integer ssrCategory) {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;

		// Integer pnrPaxId = null;
		Collection<ReservationPaxFareSegment> paxFareSegments;
		Iterator<ReservationPaxFareSegment> paxFareSegmentsIterator;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				paxFareSegments = reservationPaxFare.getPaxFareSegments();
				paxFareSegmentsIterator = paxFareSegments.iterator();
				while (paxFareSegmentsIterator.hasNext()) {
					ReservationPaxFareSegment resPaxFareSegment = (ReservationPaxFareSegment) paxFareSegmentsIterator.next();

					if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL
							.equals(resPaxFareSegment.getSegment().getStatus())) {
						continue;
					}

					Set<ReservationPaxSegmentSSR> segmentSSRs = resPaxFareSegment.getReservationPaxSegmentSSRs();

					for (Iterator<ReservationPaxSegmentSSR> itSegmentSSRs = segmentSSRs.iterator(); itSegmentSSRs.hasNext();) {
						ReservationPaxSegmentSSR segmentSSR = (ReservationPaxSegmentSSR) itSegmentSSRs.next();
						String uniqueKey = makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
								resPaxFareSegment.getPnrSegId(), segmentSSR.getPnrPaxSegmentSSRId());

						if (paxSSRToRemove != null) {
							List<PaxSSRDTO> canceledPaxSSRDTOs = paxSSRToRemove.get(uniqueKey);

							if (canceledPaxSSRDTOs != null) {
								for (PaxSSRDTO paxSSRDTO : canceledPaxSSRDTOs) {

									Integer ssrId = SSRUtil.getSSRId(paxSSRDTO.getSsrCode());

									if (ssrCategory.intValue() == SSRCategory.ID_AIRPORT_SERVICE.intValue()) {
										if (paxSSRDTO.getAirportCode().equals(segmentSSR.getAirportCode())
												&& (ssrId != null && ssrId.intValue() == segmentSSR.getSSRId().intValue())) {
											segmentSSR.setAirportCode(paxSSRDTO.getNewAirportCode());
											segmentSSR.setApplyOn(paxSSRDTO.getAirportType());
											segmentSSR.setStatus(ReservationPaxSegmentSSRStatus.CANCEL.getCode());
										}
									} else if (ssrCategory.intValue() == SSRCategory.ID_INFLIGHT_SERVICE.intValue()) {
										if (ssrId != null && ssrId.intValue() == segmentSSR.getSSRId().intValue()) {
											segmentSSR.setStatus(ReservationPaxSegmentSSRStatus.CANCEL.getCode());
										}
									}
								}
							}
						}

						List<PaxSSRDTO> retainedPaxSSRDTOs = paxSSRToRetain.get(uniqueKey);
						if (retainedPaxSSRDTOs != null) {
							for (PaxSSRDTO paxSSRDTO : retainedPaxSSRDTOs) {

								Integer ssrId = SSRUtil.getSSRId(paxSSRDTO.getSsrCode());

								if (ssrCategory.intValue() == SSRCategory.ID_AIRPORT_SERVICE.intValue()) {
									if (paxSSRDTO.getAirportCode().equals(segmentSSR.getAirportCode())
											&& (ssrId != null && ssrId.intValue() == segmentSSR.getSSRId().intValue())) {
										segmentSSR.setAirportCode(paxSSRDTO.getNewAirportCode());
										segmentSSR.setApplyOn(paxSSRDTO.getAirportType());
									}
								}

							}
						}

					}

				}
			}
		}
	}

	private static ExternalChgDTO getExternalChgDTOForSSR(BigDecimal charge, TrackInfoDTO trackInfo, EXTERNAL_CHARGES extChgType)
			throws ModuleException {
		if (charge == null) {
			return null;
		}

		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(extChgType);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationModuleUtils.getReservationBD()
				.getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfo, null);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) ((ExternalChgDTO) mapExternalChgs.get(extChgType)).clone();
		externalChgDTO.setAmount(charge);
		return externalChgDTO;
	}

	private static void addPassengerSSRChargeAndAdjustCredit(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Map<String, List<PaxSSRDTO>> paxSSRToRemove, String userNotes, TrackInfoDTO trackInfoDTO,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// Adding charge entry
		Map<String, ReservationPaxOndCharge> paxIdChargeMap = addSSRCharge(reservation, paxSSRToRemove, credentialsDTO,
				chgTnxGen);

		for (Entry<String, ReservationPaxOndCharge> enrty : paxIdChargeMap.entrySet()) {
			Integer pnrPaxId = Integer.parseInt(getPnrPaxId(enrty.getKey()));
			adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, enrty.getValue(), credentialsDTO,
					reservation.isEnableTransactionGranularity());

		}

	}

	@SuppressWarnings("unused")
	private static Map<String, ReservationPaxOndCharge> addSSRCharge(Reservation reservation,
			Map<String, List<PaxSSRDTO>> paxSSRToRemove, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		Map<String, ReservationPaxOndCharge> pnrPaxIdCharge = new HashMap<String, ReservationPaxOndCharge>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;

		Integer pnrPaxId = null;
		Collection<ReservationPaxFareSegment> paxFareSegments;
		Iterator<ReservationPaxFareSegment> paxFareSegmentsIterator;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
			Collection<Integer> consumedPaxOndChgIds = new ArrayList<Integer>();
			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				paxFareSegments = reservationPaxFare.getPaxFareSegments();
				paxFareSegmentsIterator = paxFareSegments.iterator();
				while (paxFareSegmentsIterator.hasNext()) {
					ReservationPaxFareSegment resPaxFareSegment = (ReservationPaxFareSegment) paxFareSegmentsIterator.next();

					if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL
							.equals(resPaxFareSegment.getSegment().getStatus())) {
						continue;
					}

					if (resPaxFareSegment.getReservationPaxSegmentSSRs() != null) {

						for (ReservationPaxSegmentSSR resPaxSegSSR : (Set<ReservationPaxSegmentSSR>) resPaxFareSegment
								.getReservationPaxSegmentSSRs()) {
							String uniqueKey = makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
									resPaxFareSegment.getPnrSegId(), resPaxSegSSR.getPnrPaxSegmentSSRId());

							List<PaxSSRDTO> paxSSRDTOs = paxSSRToRemove.get(uniqueKey);

							if (paxSSRDTOs != null) {
								for (PaxSSRDTO paxSSRDTO : paxSSRDTOs) {
									ExternalChgDTO chgDTO = null;
									if (paxSSRDTO != null) {
										chgDTO = paxSSRDTO.getChgDTO();
										paxSSRDTO.setPnrPaxId(reservationPax.getPnrPaxId());
									}

									if (chgDTO != null) {
										chgDTO.setAmount(AccelAeroCalculator.multiply(chgDTO.getAmount(), -1));
										PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getAppliedDiscountInfoForRefund(
												reservationPaxFare, chgDTO, consumedPaxOndChgIds);
										ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
												.captureReservationPaxOndCharge(chgDTO.getAmount(), null, chgDTO.getChgRateId(),
														chgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false,
														paxDiscInfo, null, null,
														chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

										if (ReservationApiUtils.isInfantType(reservationPaxFare.getReservationPax())) {
											pnrPaxId = reservationPaxFare.getReservationPax().getParent().getPnrPaxId();
										} else {
											pnrPaxId = reservationPaxFare.getReservationPax().getPnrPaxId();
										}

										pnrPaxIdCharge.put(uniqueKey, reservationPaxOndCharge);

									}
								}
							}
						}

					}

				}
			}
		}

		if (pnrPaxIdCharge.size() == 0) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		return pnrPaxIdCharge;
	}

	private static boolean isAirportServiceReprotectable(Map<String, List<AirportServiceDTO>> availableAirportServiceMap,
			PaxSSRDTO paxSSRDTO) {

		if (oldAirportMap != null && newAirportMap != null) {
			Integer sourceAirportIndex = oldAirportMap.get(paxSSRDTO.getAirportCode());
			String newAirport = newAirportMap.get(sourceAirportIndex);

			paxSSRDTO.setNewAirportCode(newAirport);

			if (0 == sourceAirportIndex.intValue()) {
				paxSSRDTO.setAirportType(ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE);
			} else {
				paxSSRDTO.setAirportType(ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL);
			}

			if (newAirport != null) {
				List<AirportServiceDTO> airportServiceList = availableAirportServiceMap.get(newAirport);
				if (airportServiceList != null) {
					for (AirportServiceDTO airportServiceDTO : airportServiceList) {
						if (airportServiceDTO.getSsrCode().equals(paxSSRDTO.getSsrCode())) {
							return true;
						}
					}
				}
			}

		}

		return false;
	}

	/*
	 * Checks re-protecting pax's requested SSR is available in the target flight (re-protecting flight), If so transfer
	 * pax with requested SSR. If not, transfer pax with assignable SSRs & refund credit for remaining SSR amount
	 */
	private static boolean isInflightServiceReprotectable(List<SpecialServiceRequestDTO> availableInflightServices,
			PaxSSRDTO paxSSRDTO) {

		if (availableInflightServices != null && availableInflightServices.size() > 0) {
			for (SpecialServiceRequestDTO ssr : availableInflightServices) {
				if (ssr.getSsrCode().equals(paxSSRDTO.getSsrCode())) {
					if (AppSysParamsUtil.isInventoryCheckForSSREnabled()) {

						String paxSSRKey = paxSSRDTO.getPnrSegId() + "#" + paxSSRDTO.getSsrCode();

						if (!paxSSRCount.containsKey(paxSSRKey)) {
							paxSSRCount.put(paxSSRKey, 0);
						}

						int usedCount = paxSSRCount.get(paxSSRKey) + 1;
						paxSSRCount.put(paxSSRKey, usedCount);

						if (ssr.getAvailableQty() > 0 && usedCount <= ssr.getAvailableQty()) {
							return true;
						} else {
							return false;
						}
					} else {
						return true;
					}
				}
			}
		}

		return false;
	}

	private static Map<String, List<AirportServiceDTO>>
			populateAirportWiseService(Map<String, List<AirportServiceDTO>> availableAirportServiceMap) {
		Map<String, List<AirportServiceDTO>> airportWiseMap = new HashMap<String, List<AirportServiceDTO>>();

		for (Entry<String, List<AirportServiceDTO>> entry : availableAirportServiceMap.entrySet()) {
			airportWiseMap.put(entry.getKey().split("\\" + AirportServiceDTO.SEPERATOR)[0], entry.getValue());
		}

		return airportWiseMap;
	}

	private static Map<Integer, List<String>> populateAirportsForFlightSegment(FlightSegement fltSeg) {
		Map<Integer, List<String>> map = new HashMap<Integer, List<String>>();

		String[] segmentCodeArr = fltSeg.getSegmentCode().split("/");
		List<String> airportList = new ArrayList<String>();

		for (String airport : segmentCodeArr) {
			airportList.add(airport);
		}

		map.put(fltSeg.getFltSegId(), airportList);

		return map;
	}

	private static String makeUniqueIdForModifications(Integer paxId, Integer pnrSegId, Integer pnrPaxSegmentSSRId) {
		return paxId.toString() + "|" + pnrSegId.toString() + "|" + pnrPaxSegmentSSRId.toString();
	}

	private static String getPnrPaxId(String uniqueId) {
		return uniqueId.substring(0, uniqueId.indexOf("|"));
	}

	private static Collection<PaxSSRDTO> getAffectivePaxSSRs(Collection<PaxSSRDTO> colPaxSSR, Integer sourceFltSegId) {
		Collection<PaxSSRDTO> affectivePaxSSRDTOs = new ArrayList<PaxSSRDTO>();
		for (PaxSSRDTO paxSSRDTO : colPaxSSR) {
			if (paxSSRDTO.getPnrSegId().equals(sourceFltSegId)) {
				affectivePaxSSRDTOs.add(paxSSRDTO);
			}
		}
		return affectivePaxSSRDTOs;
	}
}
