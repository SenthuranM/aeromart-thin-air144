package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class SegmentsCommonComposer implements SegmentsComposingStrategy {

	@Override
	public List<BookingSegmentDTO> composeSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {

		int paxCount = TTYMessageCreatorUtil.getPaxCount(reservation, typeBRequestDTO);
		List<BookingSegmentDTO> bookingSegmentDTOs = new ArrayList<BookingSegmentDTO>();
		BookingSegmentDTO segment;

		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (!ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equalsIgnoreCase(reservationSegmentDTO
					.getSubStatus())) {

				segment = TypeBSegmentAdopter.adoptResSegmentToTypeBSegment(reservationSegmentDTO,
						GDSApiUtils.getGDSCarrierCode(reservation.getGdsId()), paxCount);
				if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
					segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
				} else {
					segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CANCELLED.getCode());
				}
				bookingSegmentDTOs.add(segment);
			}
		}
		return bookingSegmentDTOs;
	}

	@Override
	public List<BookingSegmentDTO> composeCSSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {

		int paxCount = TTYMessageCreatorUtil.getPaxCount(reservation, typeBRequestDTO);
		List<BookingSegmentDTO> bookingSegmentDTOs = new ArrayList<BookingSegmentDTO>();
		BookingSegmentDTO segment;

		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		GDSStatusTO gdsStatusTO = globalConfig.getActiveGdsMap().values().stream().filter(new Predicate<GDSStatusTO>() {
			public boolean test(GDSStatusTO gdsStatusTO) {
				return gdsStatusTO.getCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode());
			}
		}).findFirst().get();

		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (!ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equalsIgnoreCase(reservationSegmentDTO
					.getSubStatus())
					&& reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				if (reservationSegmentDTO.getCsOcCarrierCode() != null
						&& reservationSegmentDTO.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())) {

					segment = TypeBSegmentAdopter.adoptResSegmentToTypeBCSSegment(reservationSegmentDTO, typeBRequestDTO.getCsOCCarrierCode(),
							paxCount);
					segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					segment.setPrimeFlight(gdsStatusTO.getPrimeFlightEnabled());
					bookingSegmentDTOs.add(segment);

				} else if (TTYMessageUtil.getOALSegmentsSendingEligibility(typeBRequestDTO.getCsOCCarrierCode())) {

					segment = TypeBSegmentAdopter.adoptResSegmentToTypeBSegment(reservationSegmentDTO, typeBRequestDTO.getCsOCCarrierCode(),
							paxCount);
					segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					bookingSegmentDTOs.add(segment);
				}
			}
		}
		return bookingSegmentDTOs;
	}

}
