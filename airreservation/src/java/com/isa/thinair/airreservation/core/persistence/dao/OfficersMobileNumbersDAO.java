package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airreservation.api.model.OfficersMobileNumbers;
import com.isa.thinair.commons.api.dto.OfficerMobNumsConfigsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface OfficersMobileNumbersDAO {

	public void saveOrUpdateOfficersMobileNumbersConfig(OfficersMobileNumbers OfficersMobNums) throws ModuleException;

	public void deleteOfficersMobileNumbersConfig(int releaseTimeId) throws ModuleException;

	public OfficersMobileNumbers getOfficersMobNums(int releaseTimeId) throws ModuleException;

	public Page getOfficersMobileNumbers(OfficerMobNumsConfigsSearchDTO officerMobNumsConfigsSearchDTO ,int startIndex,int pageLength) throws ModuleException;

	public Collection<String> getOfficersEmailList() throws ModuleException;

	public List<String> getOfficersMobileNumbersList() throws ModuleException;


}