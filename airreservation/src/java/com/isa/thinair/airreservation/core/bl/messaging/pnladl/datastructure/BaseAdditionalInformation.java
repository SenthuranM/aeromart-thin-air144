/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public abstract class BaseAdditionalInformation<T> {

	protected Map<Integer, List<AncillaryDTO>> mealAncillaryInformation;
	protected Map<Integer, List<AncillaryDTO>> seatAncillaryInformation;
	protected Map<Integer, List<AncillaryDTO>> ssrAncillaryInformation;
	protected Map<Integer, List<AncillaryDTO>> ssrAncillaryInformationDel;
	protected Map<Integer, List<AncillaryDTO>> baggageAncillaryInformation;
	protected Map<Integer,ReservationWLPriority> reservationWaitListPriority;
	protected Map<Integer,InboundConnectionDTO> inboundConnectionInformation;
	protected Map<Integer,ArrayList<OnWardConnectionDTO>> onwardConnectionsInformation;
	protected Map<Integer,Set<ReservationPax>> infantInformation;

	public abstract void populatePassengerInformation(T t) throws ModuleException;

	public abstract void getAncillaryInformation(T t);

	public BaseAdditionalInformation() {
		mealAncillaryInformation = new HashMap<Integer, List<AncillaryDTO>>();
		seatAncillaryInformation = new HashMap<Integer, List<AncillaryDTO>>();
		ssrAncillaryInformation = new HashMap<Integer, List<AncillaryDTO>>();
		baggageAncillaryInformation = new HashMap<Integer, List<AncillaryDTO>>();
		reservationWaitListPriority = new HashMap<Integer,ReservationWLPriority>();
		inboundConnectionInformation = new HashMap<Integer,InboundConnectionDTO>();
		onwardConnectionsInformation = new HashMap<Integer,ArrayList<OnWardConnectionDTO>>();
		infantInformation = new HashMap<Integer,Set<ReservationPax>>();
	}

	public void populateAdditionalInformation(T t) throws ModuleException {
		getAncillaryInformation(t);
		populatePassengerInformation(t);
	}
	
}
