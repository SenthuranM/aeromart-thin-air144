/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ChildElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.DOCOElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class DOCOElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> docoElmentRuleExecutor = new DOCOElementRuleExecutor();

	private boolean isStartWithNewLine = true;

	@Override
	public void buildElement(ElementContext context) {
		currentElement = "";
		if (context != null && context.getFeaturePack() != null) {
			initContextData(context);
			docoElementTemplate();
		}
		executeNext();
	}

	private void docoElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation != null
					&& passengerInformation.getVisaDocNumber() != null) {
				buildDocoElement(elementTemplate, passengerInformation);
				currentElement = elementTemplate.toString();
				if (currentElement != null && !currentElement.isEmpty()) {
					ammendmentPreValidation();
				}

			}
		}
	}

	private void buildDocoElement(StringBuilder elementTemplate,
			PassengerInformation passengerInformation) {

		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMMyy");
		if (passengerInformation.getVisaDocNumber() != null) {
			elementTemplate.setLength(0);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.DOCO);
			elementTemplate.append(space());
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
			elementTemplate.append(forwardSlash());
			elementTemplate
					.append(passengerInformation.getPlaceOfBirth() != null ? passengerInformation
							.getPlaceOfBirth().toUpperCase() : "");
			elementTemplate.append(forwardSlash());
			elementTemplate
					.append(passengerInformation.getTravelDocumentType() != null ? passengerInformation
							.getTravelDocumentType() : "");
			elementTemplate.append(forwardSlash());
			elementTemplate
					.append(passengerInformation.getVisaDocNumber() != null ? passengerInformation
							.getVisaDocNumber() : "");
			elementTemplate.append(forwardSlash());
			elementTemplate.append(passengerInformation
					.getVisaDocPlaceOfIssue() != null ? passengerInformation
					.getVisaDocPlaceOfIssue().toUpperCase() : "");
			elementTemplate.append(forwardSlash());
			if (passengerInformation.getVisaDocIssueDate() != null) {
				elementTemplate.append(dateFormatter.format(
						passengerInformation.getVisaDocIssueDate())
						.toUpperCase());
			}
			elementTemplate.append(forwardSlash());
			elementTemplate.append(passengerInformation
					.getVisaApplicableCountry());
			elementTemplate.append("-1");
			elementTemplate.append(passengerInformation.getLastName());
			elementTemplate.append(forwardSlash());
			elementTemplate.append(passengerInformation.getFirstName());
			elementTemplate.append(passengerInformation.getTitle());
		}

	}

	private void ammendmentPreValidation() {
		RuleResponse response;
		if (isStartWithNewLine && currentElement != null
				&& !currentElement.isEmpty()) {
			executeConcatenationElementBuilder(uPContext);
		}
		response = validateSubElement(currentElement);
		if (response.isProceedNextElement()) {
			ammendMessageDataAccordingTo(currentElement);
		} else {
			if (response.getSuggestedElementText() != null
					&& response.getSuggestedElementText().length > 0) {
				ammendSuggestedElements(response);
			}
		}
	}

	private void ammendSuggestedElements(RuleResponse response) {
		for (int i = 0; i < response.getSuggestedElementText().length; i++) {
			if (i == 0) {
				ammendMessageDataAccordingTo(response.getSuggestedElementText()[i]);
			} else {
				if (isStartWithNewLine) {
					executeConcatenationElementBuilder(uPContext);
				}
				ammendMessageDataAccordingTo(MessageComposerConstants.PNLADLMessageConstants.REMARKS_CONT
						+ response.getSuggestedElementText()[i]);
			}
		}
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
	}

	private void ammendMessageDataAccordingTo(String element) {
		if (currentElement != null && !currentElement.isEmpty()) {
			ammendToBaseLine(element, currentLine, messageLine);
		}
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = docoElmentRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
