/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.isa.thinair.airreservation.api.dto.pnl.PNLMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.util.StringBufferedHandler;

/**
 * @author Byorn This class will be used to generate PNL files
 * @todo hardCoded Values should be removed
 */
class PNLFormatter {

	private final ArrayList<String> filenamelist = new ArrayList<String>();

	public ArrayList<String> getFileNameList() {
		return this.filenamelist;
	}

	protected PNLFormatter(ArrayList<ArrayList<PassengerCountRecordDTO>> pnlList, PNLMetaDataDTO pnlmetadata) throws Exception {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
		Properties p = new Properties();
		int maxLineLength = PNLConstants.PNLADLElements.MAX_LINE_LENGTH_IATA;
		String path = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig()).getTemplatesroot();
		String templatesRootDir = path;

		p.setProperty("file.resource.loader.path", templatesRootDir);

		Velocity.init(p);

		VelocityContext context = new VelocityContext();
		context.put("fileelementlist", pnlList);// list
		context.put("metadata", pnlmetadata);
		context.put("foo", "34");

		Template t = Velocity.getTemplate("pnlfiletemplate.vm");

		StringWriter sw = new StringWriter();
		t.merge(context, sw);

		StringBufferedHandler buf = new StringBufferedHandler();

		StringTokenizer str = new StringTokenizer(sw.toString().toUpperCase(), "\n");

		while (str.hasMoreTokens()) {

			String text = str.nextToken().trim();
			if (!text.equals("") && text.charAt(text.length() - 1) == '\r') {
				text = text.substring(0, text.length() - 1);
			}

			int lines = PnlAdlUtil.linesRequired(text);

			if (lines > 1) {

				String s = null;
				char delimiter = ' ';

				int cc = 0;
				boolean hasMoreRecords = true;
				while (hasMoreRecords) {
					if (text.length() > maxLineLength) {
						s = text.substring(0, maxLineLength);

						delimiter = text.substring(maxLineLength).trim().charAt(0);

					} else {

						if (text.trim().indexOf(".") == 0) {
							buf.write(text.trim());
							buf.newLine();
						} else if (text.length() < 61) {
							buf.write(".RN/");
							buf.write(text.trim());
							buf.newLine();
						} else {
							int dotPlace = text.lastIndexOf(".");
							int hkPlace = text.lastIndexOf("HK1");
							int breakIndex = dotPlace;
							if (dotPlace > 0) {
								breakIndex = dotPlace;
							} else if (hkPlace > 0) {
								breakIndex = hkPlace;
							}

							if (breakIndex > -1) {

								buf.write(".RN/");
								buf.write(text.substring(0, breakIndex));
								buf.newLine();
								buf.write(text.substring(breakIndex));
								buf.newLine();
							} else {
								// Lenth >60 and no .X in the middle
								buf.write(".RN/");
								buf.write(text.substring(0, 60));
								buf.newLine();
								if (text.length() > 60) {
									buf.write(".RN/");
									buf.write(text.substring(60));
									buf.newLine();
								}
							}
						}

						break;
					}

					int splitIndex = s.lastIndexOf('.');

					int dotPlace = s.lastIndexOf(".");
					if (dotPlace > 0) {
						splitIndex = dotPlace;
					} else if (delimiter == '.') {
						splitIndex = 64;
					}

					if (splitIndex < 0) {

						String part1 = ".RN/" + text.substring(0, 60);

						buf.write(part1.trim());
						buf.newLine();
						int len = text.length();
						text = text.substring(60, len);

					}

					else if (splitIndex == 0) {
						String tmptext = text.substring(0, maxLineLength);
						int tmplast = tmptext.lastIndexOf(".");

						if (tmplast == splitIndex) {

							String part1 = text.substring(0, maxLineLength);

							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(maxLineLength, len);

						}

					}

					else {
						if (!(PnlAdlUtil.isNumber(text.charAt(0)) && (cc == 0)) & splitIndex > 0 & text.charAt(0) != '.') {

							String part1 = ".RN/" + text.substring(0, splitIndex);

							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(splitIndex, len);

						}

						else if (!PnlAdlUtil.isNumber(text.charAt(0)) & splitIndex < 0 & text.charAt(0) != '.') {
							String part1 = ".RN/" + text.substring(0, 64);

							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(maxLineLength, len);

						}

						else {

							String part1 = text.substring(0, splitIndex);

							buf.write(part1.trim());
							buf.newLine();
							int len = text.length();
							text = text.substring(splitIndex, len);

						}

					}
					
                    if(text.length() == 0){
                    	hasMoreRecords = false;
                    }
					cc++;
				}
			} else {

				buf.write(text.trim());
				buf.newLine();

			}

		}

		String flightNo = pnlmetadata.getFlight();
		String board = pnlmetadata.getBoardingairport();
		String month = pnlmetadata.getMonth();
		String day = pnlmetadata.getDay();
		if (Integer.parseInt(day) <= 9)
			day = "0" + day;

		StringBufferedHandler bufin = buf;

		String generatedpnlfilepath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
				.getGenpnlpath();

		File dir = new File(generatedpnlfilepath);
		// If directory does not exist create one
		if (!dir.exists()) {
			dir.mkdirs();
		}
        
		String fileName = simpleDateFormat.format(new Date());
		
		FileWriter fw = new FileWriter(generatedpnlfilepath + "/" + flightNo + "-" + board + "-" + month + "" + day + "-" + "PNL"
				+ "-" + fileName + "-" + "PART1" + ".txt");

		filenamelist.add(generatedpnlfilepath + "/" + flightNo + "-" + board + "-" + month + "" + day + "-" + "PNL" + "-"
				+ fileName + "-" + "PART1" + ".txt");

		BufferedWriter bufout = new BufferedWriter(fw);
		String str1 = null;
		String line1 = null, line2 = null, line3 = null;

		int lineCount = 0;
		int multiply = 1;
		int count1 = 0;

		boolean withO = false;
		while ((str1 = bufin.readLine()) != null) {

			if (!str1.equalsIgnoreCase("")) {

				if ("ENDPNL".equals(str1.trim())) {
					bufout.write(str1.trim());
					break;
				}

				if (lineCount >= maxLineLength - 4 && PnlAdlUtil.isNumber(str1.charAt(0))) {

					if (PnlAdlUtil.isNumber(str1.charAt(0)) & str1.indexOf(".O") < 0 & withO) {

						bufin.reset();
						lineCount = count1;

					}

					bufout.write("ENDPART" + multiply);
					lineCount = 0;
					bufout.close();
					fw.close();
					multiply++;
					String dateForFileName = simpleDateFormat.format(new Date());

					fw = new FileWriter(generatedpnlfilepath + "/" + flightNo + "-" + board + "-" + month + "" + day + "-"
							+ "PNL" + "-" + dateForFileName + "-" + "PART" + multiply + ".txt");
					filenamelist.add(generatedpnlfilepath + "/" + flightNo + "-" + board + "-" + month + "" + day + "-" + "PNL"
							+ "-" + dateForFileName + "-" + "PART" + multiply + ".txt");

					bufout = new BufferedWriter(fw);

					bufout.write(line1);
					bufout.newLine();
					lineCount++;
					String oline2 = line2.substring(0, line2.length() - 1);
					bufout.write(oline2 + multiply);
					lineCount++;
					bufout.newLine();
					bufout.write(line3);
					lineCount++;
					bufout.newLine();

					bufout.write(str1.trim());
					bufout.newLine();

					lineCount++;

					if (str1.charAt(0) == '-') {
						line3 = str1.trim();
					}

					if (lineCount == 1)
						line1 = str1.trim();
					if (lineCount == 2)
						line2 = str1.trim();
					if (str1.charAt(0) == '.' | (PnlAdlUtil.isNumber(str1.charAt(0)) & str1.indexOf(".O") > 0)) {

						bufin.mark(lineCount);
						count1 = lineCount;
					}

				} else {
					bufout.write(str1.trim());
					bufout.newLine();

					lineCount++;

					if (str1.charAt(0) == '-') {
						line3 = str1.trim();
					}

					if (lineCount == 1)
						line1 = str1.trim();
					if (lineCount == 2)
						line2 = str1.trim();
					if (str1.charAt(0) == '.' | (PnlAdlUtil.isNumber(str1.charAt(0)) & str1.indexOf(".O") > 0)) {

						bufin.mark(lineCount);
						count1 = lineCount;
					}
				}
			}

		}

		bufout.close();
		fw.close();
		bufin.close();

	}

	// to check whether a character is a number

}