package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to create a dummy reservation to fulfill the payment carrier
 * 
 * @author mekanayake
 * @isa.module.command name="createOrUpdatePayCarrierRes"
 */
public class CreateOrUpdatePayCarrierRes extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CreateOrUpdatePayCarrierRes.class);

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("Inside CreateOrUpdatePayCarrierRes.execute");
		}

		// Getting command parameters
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		ReservationAdminInfoTO reservationAdminInfoTO = (ReservationAdminInfoTO) this
				.getParameter(CommandParamNames.RESERVATION_ADMIN_INFO_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Map<Integer, Collection<ReservationPaxExtTnx>> paxWiseExtTnx = (Map<Integer, Collection<ReservationPaxExtTnx>>) this
				.getParameter(CommandParamNames.EXT_PAX_TNX_MAP);

		this.validateParams(reservation, reservationAdminInfoTO, credentialsDTO);

		Map<Integer, ReservationPax> allPassengersMap = new HashMap<Integer, ReservationPax>();

		for (ReservationPax pax : (Collection<ReservationPax>) reservation.getPassengers()) {
			allPassengersMap.put(pax.getPaxSequence(), pax);

		}

		trackParentInfantMappings(reservation, allPassengersMap);

		// Sets the admin information
		this.setAdminInformation(reservation, reservationAdminInfoTO, credentialsDTO);

		// Saves the reservation
		ReservationProxy.saveReservation(reservation);
		
		//save in blacklistPaxReservation
		if(AppSysParamsUtil.isBlacklistpassengerCheckEnabled() && 'Y'==reservation.getDummyBooking()){			
			ReservationBO.saveBlacklistedPnrPaxIfExist(reservation ,String.valueOf(this.getParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX)));
		}

		// set the pnrPaxId to externalPaxTnx and save
		ReservationPaymentDAO paymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			if (paxWiseExtTnx != null) {
				Collection<ReservationPaxExtTnx> paxExtTnxs = paxWiseExtTnx.get(reservationPax.getPaxSequence());
				if (paxExtTnxs != null) {
					for (ReservationPaxExtTnx tnx : paxExtTnxs) {
						tnx.setPnrPaxId(reservationPax.getPnrPaxId());
						paymentDAO.saveReservationPaxExtTnx(tnx);
						if (tnx.getCreditCardDetail() != null) {
							CreditCardDetail cardDetail = tnx.getCreditCardDetail();
							cardDetail.setExtTransactionId(tnx.getPaxExternalTnxId());
							cardDetail.setPnr(reservation.getPnr());
							cardDetail.setPnrPaxId(reservationPax.getPnrPaxId());
							ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveCreditCardDetail(cardDetail);
						} else if (tnx.getExternalReference() != null) {
							ReservationModuleUtils.getVoucherBD().recordRedemption(tnx.getExternalReference(), null,
									tnx.getAmount().toString(), tnx.getPaxExternalTnxId());
						}
					}
				}
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Exitting CreateOrUpdatePayCarrierRes.execute");
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.PNR, reservation.getPnr());
		response.addResponceParam(CommandParamNames.VERSION, Long.valueOf(reservation.getVersion()));
		return response;
	}

	// Sets the parent infant mappings for the dummy reservation.
	private void trackParentInfantMappings(Reservation reservation, Map<Integer, ReservationPax> allPassengers) {
		if (reservation != null && allPassengers != null && allPassengers.size() > 0) {
			// if the reservation is a dummy one we need to add the parent infant relationships correctly.
			if (ReservationInternalConstants.DummyBooking.YES == reservation.getDummyBooking()) {
				ReservationPax mappingPax;
				Integer parentOrInfantId;

				for (ReservationPax currentPax : (Collection<ReservationPax>) reservation.getPassengers()) {
					// Carrying out the mapping
					// Parent
					if (ReservationApiUtils.isParentBeforeSave(currentPax)) {
						parentOrInfantId = currentPax.getIndexId();
						if (parentOrInfantId != null) {
							mappingPax = (ReservationPax) allPassengers.get(parentOrInfantId);
							currentPax.addInfants(mappingPax);
						}
					}
					// Infant
					else if (ReservationApiUtils.isInfantType(currentPax)) {
						if (currentPax.getParent() == null) {
							parentOrInfantId = currentPax.getIndexId();
							if (parentOrInfantId != null) {
								mappingPax = (ReservationPax) allPassengers.get(parentOrInfantId);
								currentPax.addParent(mappingPax);
							} else {
								log.error("No parent sequence linked for infant");
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Sets the admin information TODO move to single place
	 * 
	 * @param reservation
	 * @param reservationAdminInfoTO
	 * @param credentialsDTO
	 */
	private void setAdminInformation(Reservation reservation, ReservationAdminInfoTO reservationAdminInfoTO,
			CredentialsDTO credentialsDTO) {

		ReservationAdminInfo adminInfo = new ReservationAdminInfo();
		reservation.setAdminInfo(adminInfo);

		adminInfo.setOriginAgentCode(credentialsDTO.getAgentCode());
		adminInfo.setOriginChannelId(credentialsDTO.getSalesChannelCode());
		adminInfo.setOriginUserId(credentialsDTO.getUserId());
		adminInfo.setOwnerChannelId(credentialsDTO.getSalesChannelCode());
		adminInfo.setOwnerAgentCode(credentialsDTO.getAgentCode());

		if (credentialsDTO.getTrackInfoDTO() != null) {
			String carrierCode;

			carrierCode = BeanUtils.nullHandler(credentialsDTO.getTrackInfoDTO().getCarrierCode());
			if (carrierCode.length() > 0) {
				adminInfo.setOriginCarrierCode(carrierCode);
			}

			adminInfo.setOriginIPAddress(credentialsDTO.getTrackInfoDTO().getIpAddress());
			adminInfo.setOriginIPNumber(credentialsDTO.getTrackInfoDTO().getOriginIPNumber());
		}

		if (reservationAdminInfoTO != null) {
			adminInfo.setOriginSalesTerminal(reservationAdminInfoTO.getOriginSalesTerminal());
			adminInfo.setLastSalesTerminal(reservationAdminInfoTO.getLastSalesTerminal());

			if (reservationAdminInfoTO.isOverrideOriginAgentCode()) {
				adminInfo.setOriginAgentCode(reservationAdminInfoTO.getOriginAgentCode());
			}

			if (reservationAdminInfoTO.isOverrideOriginUserId()) {
				adminInfo.setOriginUserId(reservationAdminInfoTO.getOriginUserId());
			}

			if (reservationAdminInfoTO.isOverrideOwnerAgentCode()) {
				adminInfo.setOwnerAgentCode(reservationAdminInfoTO.getOwnerAgentCode());
			}

			if (reservationAdminInfoTO.isOverrideOwnerChannelId()) {
				adminInfo.setOwnerChannelId(reservationAdminInfoTO.getOwnerChannelId());
			}

			if (reservationAdminInfoTO.isOverrideOriginChannelId()) {
				adminInfo.setOriginChannelId(reservationAdminInfoTO.getOriginChannelId());
			}
		}

	}

	private void validateParams(Reservation reservation, ReservationAdminInfoTO reservationAdminInfoTO,
			CredentialsDTO credentialsDTO) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Inside validateParams");
		}
		if (reservation == null || reservation.getContactInfo() == null || reservation.getPassengers() == null
				|| reservation.getPassengers().size() == 0 || credentialsDTO == null) {

			log.error("airreservations.arg.invalid.null");
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Exit validateParams");
		}
	}
}
