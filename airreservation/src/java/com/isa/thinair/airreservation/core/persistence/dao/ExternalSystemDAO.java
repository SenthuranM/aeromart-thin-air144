/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;

public interface ExternalSystemDAO {
	
	public void clearAgentTopUpTable(Date date);

	public BigDecimal getGeneratedCreditCardSalesTotal(Date fromDate, Date toDate);

	public BigDecimal getTransferedCreditCardSalesTotal(Date fromDate, Date toDate, AccontingSystemTemplate template);

	public Object[] getCreditCardSalesHistory(Date fromDate, Date toDate);

}
