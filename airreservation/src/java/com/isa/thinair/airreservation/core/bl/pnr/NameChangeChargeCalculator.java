package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.segment.ChargeBO;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author Jagath
 * 
 */

public class NameChangeChargeCalculator extends BaseExtraFeeCalculator implements IExtraFeeCalculator {

	private Map<Integer, NameDTO> changedPaxNamesMap;
	private String pnr;
	private Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges;
	private Map<Integer, IPayment> pnrPaxIdAndPayments;
	private CredentialsDTO credentialsDTO;

	public NameChangeChargeCalculator(String pnr, Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Map<Integer, NameDTO> changedPaxNamesMap, Map<Integer, IPayment> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO) {
		this.pnr = pnr;
		this.changedPaxNamesMap = changedPaxNamesMap;
		this.reprotectedExternalCharges = reprotectedExternalCharges;
		this.pnrPaxIdAndPayments = pnrPaxIdAndPayments;
		this.credentialsDTO = credentialsDTO;
	}

	@Override
	public void calculate() throws ModuleException {
		// if (changedPaxNamesMap != null && changedPaxNamesMap.size() > 0) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadEtickets(true);
		reservation = ReservationProxy.getReservation(pnrModesDTO);

		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
		Map<Integer, List<ReservationPaxFare>> unflownPnrPaxFareMap = new HashMap<Integer, List<ReservationPaxFare>>();
		Map<Integer, ReservationPax> originalReservationPaxMap = new HashMap<Integer, ReservationPax>();

		Map<Integer, ReservationSegmentDTO> pnrSegIdAndReservationSegmentDTOMap = new HashMap<Integer, ReservationSegmentDTO>();
		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			pnrSegIdAndReservationSegmentDTOMap.put(reservationSegmentDTO.getPnrSegId(), reservationSegmentDTO);
		}

		for (ReservationPax reservationPax : reservation.getPassengers()) {

			Collection<EticketTO> eTicketTOs = reservationPax.geteTickets();
			Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
			ReservationPaxFareSegment reservationPaxFareSegment;
			ReservationSegment reservationSegment;
			ReservationSegmentDTO reservationSegmentDTO;
			Integer pnrPaxIdTmp = reservationPax.getPnrPaxId();
			originalReservationPaxMap.put(pnrPaxIdTmp, reservationPax);

			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				List<ReservationPaxFareSegment> resPaxFareSegments = new ArrayList<ReservationPaxFareSegment>(
						reservationPaxFare.getPaxFareSegments());
				Comparator<ReservationPaxFareSegment> segSeqOrder = new Comparator<ReservationPaxFareSegment>() {
					public int compare(ReservationPaxFareSegment seg1, ReservationPaxFareSegment seg2) {
						return seg1.getSegment().getSegmentSeq().compareTo(seg2.getSegment().getSegmentSeq());
					}
				};

				Collections.sort(resPaxFareSegments, segSeqOrder);
				itReservationPaxFareSegment = resPaxFareSegments.iterator();
				while (itReservationPaxFareSegment.hasNext()) {
					reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();
					reservationSegment = reservationPaxFareSegment.getSegment();
					Integer pnrSegId = reservationSegment.getPnrSegId();
					reservationSegmentDTO = (ReservationSegmentDTO) pnrSegIdAndReservationSegmentDTOMap.get(pnrSegId);
					boolean isFlown = false;
					if (reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
						break;
					}

					if (reservationPaxFareSegment.getPnrPaxFareSegId() != null
							&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
									.equals(reservationSegment.getStatus())
							&& AppSysParamsUtil.getFlownCalculateMethod() != FLOWN_FROM.DATE && eTicketTOs != null
							&& eTicketTOs.size() > 0) {

						for (EticketTO eticketTO : eTicketTOs) {
							if (eticketTO.getPnrPaxFareSegId().equals(reservationPaxFareSegment.getPnrPaxFareSegId())) {
								if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET
										&& eticketTO.getTicketStatus().equals(EticketStatus.FLOWN.code())) {
									isFlown = true;
								} else if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.PFS && eticketTO
										.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
									isFlown = true;
								} else if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()
										&&	(EticketStatus.CHECKEDIN.code().equals(eticketTO.getTicketStatus())
												|| EticketStatus.BOARDED.code().equals(eticketTO.getTicketStatus()))){
									isFlown = true;
								}
								break;
							}
						}
					} else {
						if (currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) < 0
								&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
										.equals(reservationSegment.getStatus())) {
							isFlown = true;
						}
					}
					if (!isFlown) {
						if (unflownPnrPaxFareMap.containsKey(pnrPaxIdTmp)) {
							unflownPnrPaxFareMap.get(pnrPaxIdTmp).add(reservationPaxFare);
						} else {
							List<ReservationPaxFare> tmpPnrPaxFare = new ArrayList<ReservationPaxFare>();
							tmpPnrPaxFare.add(reservationPaxFare);
							unflownPnrPaxFareMap.put(pnrPaxIdTmp, tmpPnrPaxFare);
						}
					} else {
						break;
					}
				}
			}

		}

		calculateNameChangeCharge(unflownPnrPaxFareMap, originalReservationPaxMap,
				reservation.isInfantPaymentRecordedWithInfant());

	}

	private void calculateNameChangeCharge(Map<Integer, List<ReservationPaxFare>> unflownPnrPaxFareMap,
			Map<Integer, ReservationPax> originalReservationPaxMap, boolean isInfantPaymentSeparated) throws ModuleException {

		if (changedPaxNamesMap != null && changedPaxNamesMap.size() > 0 && unflownPnrPaxFareMap.size() > 0) {
			for (Integer pnrPaxId : changedPaxNamesMap.keySet()) {
				NameDTO nameDTO = changedPaxNamesMap.get(pnrPaxId);
				if (unflownPnrPaxFareMap.containsKey(pnrPaxId)) {
					ReservationPax tmpReservationPax = originalReservationPaxMap.get(pnrPaxId);

					Map<Integer, List<Integer>> fareIdFltSegIdMap = new HashMap<Integer, List<Integer>>();
					for (ReservationPaxFare reservationPaxFare : unflownPnrPaxFareMap.get(pnrPaxId)) {
						for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
							Integer fareId = reservationPaxFare.getFareId();
							if (fareIdFltSegIdMap.get(fareId) == null) {
								fareIdFltSegIdMap.put(fareId, new ArrayList<Integer>());
							}
							fareIdFltSegIdMap.get(fareId).add(reservationPaxFareSegment.getSegment().getFlightSegId());
						}
					}

					List<Integer> processedPPFList = new ArrayList<Integer>();

					for (ReservationPaxFare reservationPaxFare : unflownPnrPaxFareMap.get(pnrPaxId)) {
						if (!processedPPFList.contains(reservationPaxFare.getPnrPaxFareId())) {
							List<ReservationPaxFare> reservationPaxFareTarget = new ArrayList<ReservationPaxFare>();
							reservationPaxFareTarget.add(reservationPaxFare);
							List<ExternalChgDTO> paxExternalChgDTOs = new ChargeBO(false, credentialsDTO)
									.getOndChargesForNameChange(tmpReservationPax, reservationPaxFareTarget, fareIdFltSegIdMap,
											isInfantPaymentSeparated);
							processedPPFList.add(reservationPaxFare.getPnrPaxFareId());

							if (paxExternalChgDTOs.size() > 0) {
								if (!reprotectedExternalCharges.containsKey(pnrPaxId)) {
									reprotectedExternalCharges.put(pnrPaxId, new ArrayList<ExternalChgDTO>());
								}

								reprotectedExternalCharges.get(pnrPaxId).addAll(paxExternalChgDTOs);

								Integer paymentPaxId = pnrPaxId;
								if (!isInfantPaymentSeparated && PaxTypeTO.INFANT.equals(tmpReservationPax.getPaxType())) {
									paymentPaxId = tmpReservationPax.getParent().getPnrPaxId();
								}

								if (pnrPaxIdAndPayments != null) {
									if (!pnrPaxIdAndPayments.containsKey(paymentPaxId)) {
										pnrPaxIdAndPayments.put(paymentPaxId, new PaymentAssembler());
									}
									pnrPaxIdAndPayments.get(paymentPaxId).addExternalCharges(paxExternalChgDTOs);
								}
							}
						}
					}
				}
			}
		}

	}
}
