/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * factory to get credit and debit tnx
 * 
 * @author Lasantha Pambagoda
 */
public abstract class TnxFactory {

	protected static ReservationTnx getCreditInstance(String pnrPaxId, BigDecimal amount, int nc, CredentialsDTO credentialsDTO,
			Date timestamp, String recietpNumber, String paymentCarrier, String lccUniqueTnxId, boolean isGoShowProcess,
			boolean isActualPayment) throws ModuleException {

		ReservationTnx tnx = getReservationTnxInstance(pnrPaxId, amount, nc, credentialsDTO, timestamp, paymentCarrier,
				lccUniqueTnxId, isGoShowProcess);

		if ((ReservationTnxNominalCode.CASH_PAYMENT.getCode() == nc) && !isActualPayment
				&& AccelAeroCalculator.isEqual(amount, new BigDecimal(0))) {
			tnx.setDummyPayment("Y");
		}

		tnx.setAmount(tnx.getAmount().negate());
		tnx.setTnxType(ReservationInternalConstants.TnxTypes.CREDIT);
		tnx.setRecieptNo(recietpNumber);
		return tnx;
	}

	private static ReservationTnx getReservationTnxInstance(String pnrPaxId, BigDecimal amount, int nc,
			CredentialsDTO credentialsDTO, Date timestamp, String paymentCarrier, String lccUniqueTnxId, boolean isGoShowProcess)
			throws ModuleException {

		if (amount.doubleValue() < 0) {
			throw new ModuleException("revac.invalid.amount");
		}
		ReservationTnx tnx = new ReservationTnx();
		tnx.setPnrPaxId(pnrPaxId);
		tnx.setAmount(amount);
		tnx.setNominalCode(BeanUtils.parseInteger(nc));
		tnx.setDateTime(timestamp);
		if (isGoShowProcess) {
			String userId = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + User.SCHEDULER_USER_ID;
			tnx.setUserId(userId);
		} else {
			tnx.setUserId(BeanUtils.nullHandler(credentialsDTO.getUserId()));
		}
		tnx.setCustomerId(credentialsDTO.getCustomerId());
		tnx.setAgentCode(credentialsDTO.getAgentCode());
		tnx.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
		tnx.setPaymentCarrier(paymentCarrier);
		tnx.setLccUniqueId(lccUniqueTnxId);
		tnx.setDummyPayment("N");
		return tnx;
	}

	protected static ReservationTnx getDebitInstance(String pnrPaxId, BigDecimal amount, int nc, CredentialsDTO credentialsDTO,
			Date timestamp, String paymentCarrier, String lccUniqueTnxId, boolean isGoShowProcess) throws ModuleException {

		ReservationTnx tnx = getReservationTnxInstance(pnrPaxId, amount, nc, credentialsDTO, timestamp, paymentCarrier,
				lccUniqueTnxId, isGoShowProcess);
		tnx.setTnxType(ReservationInternalConstants.TnxTypes.DEBIT);
		return tnx;
	}

	public static TnxCreditPayment getTnxCreditPayment(PaxCreditDTO paxCreditDTO, String agentCode) {
		TnxCreditPayment payment = new TnxCreditPayment();
		payment.setPnrPaxId(paxCreditDTO.getDebitPaxId());
		payment.setAmount(paxCreditDTO.getBalance());
		payment.setAgentCode(agentCode);
		payment.setPaymentType(PaymentType.CREDIT_BF.getTypeValue());
		payment.setExpiryDate(paxCreditDTO.getDateOfExpiry());
		payment.setPaymentCarrier(paxCreditDTO.getPayCarrier());
		payment.setPayCurrencyDTO(paxCreditDTO.getPayCurrencyDTO());
		payment.setLccUniqueTnxId(paxCreditDTO.getLccUniqueId());
		return payment;
	}
}
