package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.ResChangesDetectorConstants;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.AddAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.ChangeAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.DelAddAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.DelAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.base.BaseObseverActionTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ObserverActionDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public abstract class AirportMessageObserver extends BaseResModifyObserver<ResModifyDtectorDataContext> {

	public AirportMessageObserver(String observerRef) {
		super(observerRef);
	}

	protected abstract boolean validForObserver();
	protected List<BaseObseverActionTemplate<ObserverActionDataContext>> actionTemplateList = new ArrayList<>();
	
	protected Map<Integer, Set<Integer>> addPaxWiseSegMap = new HashMap<>();
	protected Map<Integer, Set<Integer>> delPaxWiseSegMap = new HashMap<>();
	protected Map<Integer, Set<Integer>> chgPaxWiseSegMap = new HashMap<>();

	private final String COMPOUND_KEY_SPLITTER = "#";

		
	//Map<Integer, Set<String>> paxWiseAffectedRule = new HashMap<>();
	//private Set<String> observerValidAffectedRules;
	//private Set<Integer> cancelSegIDsFromProcess = new HashSet<>();
	//private Set<Integer> newSegIDsFromFrocess = new HashSet<>();
	
	protected Set<String> removeAllSSROnUpdate = new HashSet<String>() ;



	
	
	protected void populateObserverValidRuleResponses() {

		Set<String> observerValidRuleResfs = dataContext.getObserverWiseRulesAndActionMap().get(observerRef).keySet();
		for (String ruleRef : observerValidRuleResfs) {

			if (dataContext.getRuleWiseResponseMap().get(ruleRef) != null) {
				observerValidRuleResponses.add(dataContext.getRuleWiseResponseMap().get(ruleRef));
			}
		}
	}

	protected String createCompundKey(Integer paxId, Integer segId){
		return paxId + COMPOUND_KEY_SPLITTER + segId;
	}
	
	
//-----------------------------------------------------------------------------------------------------------------------
/*	private void setObserverActions() throws ModuleException {

		Map<Integer, Set<Integer>> addPaxsforAllaffectedSegs = null;
		Map<Integer, Set<Integer>> delPaxsforAllaffectedSegs = null;
		Map<Integer, Set<Integer>> changeFromAllAffectedSegs = null;
		if (allPaxAddAllSegs()) {

			addPaxsforAllaffectedSegs = getValidPaxWiseSegMapFORPALCAL(dataContext.getUpdatedReservation().getPassengers());
			populateNewSegIDsFromFrocess();

			paxwiseSegmentsIntersectWithAffectedSegs(addPaxsforAllaffectedSegs, newSegIDsFromFrocess);

		} else if (allPaxDelAllSegs()) {

			delPaxsforAllaffectedSegs = getValidPaxWiseSegMapFORPALCAL(dataContext.getExisitingReservation().getPassengers());
			populatecanCelSegIDsFromProcess();

			paxwiseSegmentsIntersectWithAffectedSegs(delPaxsforAllaffectedSegs, cancelSegIDsFromProcess);

		} else if (allPaxAddDelFromSelectedSegs()) {

			addPaxsforAllaffectedSegs = getValidPaxWiseSegMapFORPALCAL(dataContext.getUpdatedReservation().getPassengers());
			delPaxsforAllaffectedSegs = getValidPaxWiseSegMapFORPALCAL(dataContext.getExisitingReservation().getPassengers());

			populateNewSegIDsFromFrocess();
			populatecanCelSegIDsFromProcess();

			paxwiseSegmentsIntersectWithAffectedSegs(addPaxsforAllaffectedSegs, newSegIDsFromFrocess);
			paxwiseSegmentsIntersectWithAffectedSegs(delPaxsforAllaffectedSegs, cancelSegIDsFromProcess);

		} else {

			Set<ReservationPax> paxDelFromAllSegs = new HashSet<>();
			Set<ReservationPax> paxAddDelFromAllSegs = new HashSet<>();
			Set<Integer> paxChnges = new HashSet<>();

			for (Integer pnrpaxId : paxWiseAffectedRule.keySet()) {
				Set<String> ruleRefSet = paxWiseAffectedRule.get(pnrpaxId);

				if (selectedPaxDelFromAllSegs(ruleRefSet)) {

					paxDelFromAllSegs.add(getPaxFromReservation(dataContext.getExisitingReservation(), pnrpaxId));

				} else if (selectdPaxAddDelFromAllSegs(ruleRefSet)) {

					paxAddDelFromAllSegs.add(getPaxFromReservation(dataContext.getUpdatedReservation(), pnrpaxId));

				} else if (selectedPaxChng(ruleRefSet)) {

					paxChnges.add(getPaxFromReservation(dataContext.getUpdatedReservation(), pnrpaxId).getPnrPaxId());
				}
			}

			Map<Integer, Set<Integer>> deleteFromAllAffectedSegs = getValidPaxWiseSegMapFORPALCAL(paxDelFromAllSegs);
			Map<Integer, Set<Integer>> deleteAndAddFromAllAffectedSegs = getValidPaxWiseSegMapFORPALCAL(paxAddDelFromAllSegs);

			addPaxsforAllaffectedSegs = deleteAndAddFromAllAffectedSegs;
			delPaxsforAllaffectedSegs = getUnionMap(deleteFromAllAffectedSegs, deleteAndAddFromAllAffectedSegs);

			changeFromAllAffectedSegs = getChangeEffectedPaxWiseSegs(paxChnges);
			if(changeFromAllAffectedSegs.isEmpty()){
				changeFromAllAffectedSegs =null;
			}
			if(addPaxsforAllaffectedSegs.isEmpty()){
				addPaxsforAllaffectedSegs =null;
			}
			if(delPaxsforAllaffectedSegs.isEmpty()){
				delPaxsforAllaffectedSegs =null;
			}
		}
		
		
		ObserverActionDataContext oAdataContext = getObserverActionDataContext(addPaxsforAllaffectedSegs, delPaxsforAllaffectedSegs, changeFromAllAffectedSegs);

		if(addPaxsforAllaffectedSegs !=null && delPaxsforAllaffectedSegs !=null ){
			actionTemplateList.add(new DelAddAirportMessagePaxTemplate());
			
		}else if (addPaxsforAllaffectedSegs !=null && delPaxsforAllaffectedSegs ==null  ){
			
			actionTemplateList.add(new AddAirportMessagePaxTemplate());
			
		}else if (addPaxsforAllaffectedSegs ==null && delPaxsforAllaffectedSegs !=null  ){
			
			actionTemplateList.add(new DelAirportMessagePaxTemplate());			
		}
		
		if(changeFromAllAffectedSegs !=null){
			
			actionTemplateList.add(new ChangeAirportMessagePaxTemplate());
		}
		
		
		for(BaseObseverActionTemplate<ObserverActionDataContext> actionTrmpl : actionTemplateList){
			actionTrmpl.executeTemplateAction(oAdataContext);
		}
		
	}*/

/*	private Map<Integer, Set<Integer>> getUnionMap(Map<Integer, Set<Integer>> mapA, Map<Integer, Set<Integer>> mapB) {

		Map<Integer, Set<Integer>> unionMap = new HashMap<Integer, Set<Integer>>();
		mergeMap(mapA, unionMap);
		mergeMap(mapB, unionMap);
		return unionMap;
	}
*/
/*	private void mergeMap(Map<Integer, Set<Integer>> mapA, Map<Integer, Set<Integer>> unionMap) {
		for (Integer key : mapA.keySet()) {

			if (unionMap.get(key) != null) {
				unionMap.get(key).addAll(mapA.get(key));
			} else {
				unionMap.put(key, mapA.get(key));
			}

		}
	}
*/
	protected ObserverActionDataContext getObserverActionDataContext() {

		ObserverActionDataContext obaObserverActionDataContext = new ObserverActionDataContext();
		obaObserverActionDataContext.setAddMap(addPaxWiseSegMap);
		obaObserverActionDataContext.setDelMap(delPaxWiseSegMap);
		obaObserverActionDataContext.setChgMap(chgPaxWiseSegMap);
		obaObserverActionDataContext.setNewReservation(dataContext.getUpdatedReservation());
		obaObserverActionDataContext.setOldReservation(dataContext.getExisitingReservation());
		obaObserverActionDataContext.setRemoveAllSSROnUpdate(removeAllSSROnUpdate);
		

		return obaObserverActionDataContext;
	}

/*	private void paxwiseSegmentsIntersectWithAffectedSegs(Map<Integer, Set<Integer>> addPaxsforAllaffectedSegs,
			Set<Integer> affectedSegIds) {

		for (Integer pnrPaxId : addPaxsforAllaffectedSegs.keySet()) {

			addPaxsforAllaffectedSegs.get(pnrPaxId).retainAll(affectedSegIds);
		}

	}*/



/*	private Map<Integer, Set<Integer>> getChangeEffectedPaxWiseSegs(Set<Integer> paxIDS) {

		Set<String> changesKeySet = new HashSet<String>();

		final Map<String, Set<String>> existSSRcodes = classifySSRCodes(getSelectedPaxSetFromReservation(
				dataContext.getExisitingReservation(), paxIDS));
		final Map<String, Set<String>> updateSSRcodes = classifySSRCodes(getSelectedPaxSetFromReservation(
				dataContext.getUpdatedReservation(), paxIDS));

		Set<String> unionSet = new HashSet<String>();
		unionSet.addAll(existSSRcodes.keySet());
		unionSet.addAll(updateSSRcodes.keySet());

		for (String key : unionSet) {

			// remove all ssr in existing rservation
			if (existSSRcodes.get(key) != null && updateSSRcodes.get(key) == null) {
				removeAllSSROnUpdate.addAll(existSSRcodes.keySet());
				changesKeySet.add(key);
				//return getPaxIdWiseSegIdList(existSSRcodes.keySet());

				// add new ssrs for non ssr existing reservation
			} else if (existSSRcodes.get(key) == null && updateSSRcodes.get(key) != null) {
				changesKeySet.add(key);
				//return getPaxIdWiseSegIdList(updateSSRcodes.keySet());

				// changtes ssr
			} else if (existSSRcodes.get(key) != null && updateSSRcodes.get(key) != null) {

				if (!(existSSRcodes.get(key).containsAll(updateSSRcodes.get(key))
						&& updateSSRcodes.get(key).containsAll(existSSRcodes.get(key)))) {
					changesKeySet.add(key);
				}

			}

		}

		return getPaxIdWiseSegIdList(changesKeySet);
	}*/
/*
	private Map<Integer, Set<Integer>> getPaxIdWiseSegIdList(Set<String> keySet) {
		String[] ids;
		Map<Integer, Set<Integer>> keyMap = new HashMap<>();

		for (String key : keySet) {
			ids = key.split(COMPOUND_KEY_SPLITTER);
			Integer paxID = Integer.valueOf(ids[0]);
			Integer segID = Integer.valueOf(ids[1]);

			if (keyMap.get(paxID) != null) {
				keyMap.get(paxID).add(segID);
			} else {
				Set<Integer> segIdSet = new HashSet<Integer>();
				segIdSet.add(segID);
				keyMap.put(paxID, segIdSet);
			}

		}

		return keyMap;
	}*/

/*	private Map<String, Set<String>> classifySSRCodes(Set<ReservationPax> passengers) {

		Map<String, Set<String>> paxSegPALValidSSRCodes = new HashMap<String, Set<String>>();

		for (ReservationPax pax : passengers) {
			if (pax.getPaxSSR() != null) {
				for (PaxSSRDTO ssr : pax.getPaxSSR()) {
					if (ssr.isInclideInPalCAl() && checkPALSengingStatus(pax, ssr.getPnrSegId())) {
						populatePaxSegPALValidSSRCodes(paxSegPALValidSSRCodes, ssr.getPnrPaxId(), ssr.getPnrSegId(),
								ssr.getSsrCode());
					}
				}
			}
		}

		return paxSegPALValidSSRCodes;
	}*/

/*	private void populatePaxSegPALValidSSRCodes(Map<String, Set<String>> paxSegPALValidSSRCodes, Integer pnrPaxId,
			Integer pnrSegId, String ssrCode) {
		String key = createCompoundKey(pnrPaxId, pnrSegId);
		if (paxSegPALValidSSRCodes.get(key) != null) {
			paxSegPALValidSSRCodes.get(key).add(ssrCode);
		} else {
			Set<String> ssrCodes = new HashSet<String>();
			ssrCodes.add(ssrCode);
			paxSegPALValidSSRCodes.put(key, ssrCodes);
		}

	}
	*/


/*	private String createCompoundKey(Integer paxID, Integer segId) {

		return paxID + COMPOUND_KEY_SPLITTER + segId;
	}

	private boolean selectedPaxChng(Set<String> ruleRefSet) {
		return ruleRefSet.contains(ResChangesDetectorConstants.RuleRef.SSR_UPDATE_RULE);
	}

	private boolean selectedPaxDelFromAllSegs(Set<String> ruleRefSet) {
		return ruleRefSet.contains(ResChangesDetectorConstants.RuleRef.REMOVE_PAX_RULE);
	}

	private boolean selectdPaxAddDelFromAllSegs(Set<String> ruleRefSet) {
		return ruleRefSet.contains(ResChangesDetectorConstants.RuleRef.NAME_CHANGE_RULE);
	}

	private boolean allPaxAddDelFromSelectedSegs() {

		return observerValidAffectedRules.contains(ResChangesDetectorConstants.RuleRef.SEG_MODIFY_RULE)
				|| observerValidAffectedRules.contains(ResChangesDetectorConstants.RuleRef.SEG_CNX_RULE)
				|| observerValidAffectedRules.contains(ResChangesDetectorConstants.RuleRef.BOOKING_CLASS_MOD_RULE)
				|| observerValidAffectedRules.contains(ResChangesDetectorConstants.RuleRef.CABIN_CLASS_MOD_RULE);
	}

	private boolean allPaxDelAllSegs() {
		return observerValidAffectedRules.contains(ResChangesDetectorConstants.RuleRef.RES_CNX_RULE);
	}*/

/*	private boolean allPaxAddAllSegs() {
		return observerValidAffectedRules.contains(ResChangesDetectorConstants.RuleRef.CREATE_CNF_RESERVATION_RULE)
				|| observerValidAffectedRules.contains(ResChangesDetectorConstants.RuleRef.CONFIRM_OHD_RESERVATION_RULE);
	}

	private void populatecanCelSegIDsFromProcess() {
				
		for (ReservationSegment seg : dataContext.getExisitingReservation().getSegments()) {
			if (!isCNXSeg(seg)) {
				for (ReservationSegment updatedSeg : dataContext.getUpdatedReservation().getSegments()) {
					if (seg.getPnrSegId().equals(updatedSeg.getPnrSegId()) && isCNXSeg(updatedSeg)) {
						cancelSegIDsFromProcess.add(updatedSeg.getPnrSegId());
					}
				}
			}
		}
	}*/

/*	private void populateNewSegIDsFromFrocess() {

		Set<Integer> existResSegids;

		if (dataContext.getExisitingReservation() != null) {
			existResSegids = (Set<Integer>) dataContext.getExisitingReservation().getPnrSegIds();
		} else {
			existResSegids = new HashSet<>();
		}

		for (ReservationSegment seg : dataContext.getUpdatedReservation().getSegments()) {
			if (!isCNXSeg(seg) && !existResSegids.contains(seg)) {
				newSegIDsFromFrocess.add(seg.getPnrSegId());
			}
		}
	}*/

/*	private boolean isCNXSeg(ReservationSegment seg) {

		return ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus());

	}*/

	protected boolean isCNXPax(ReservationPax pax) {
		return ReservationInternalConstants.ReservationPaxStatus.CANCEL.equals(pax.getStatus());
	}

/*	private ReservationPax getPaxFromReservation(Reservation reservation, Integer pnrPaxId) {

		ReservationPax selectedPax = null;

		for (ReservationPax pax : reservation.getPassengers()) {
			if (pnrPaxId.equals(pax.getPnrPaxId())) {
				selectedPax = pax;
				break;
			}
		}

		return selectedPax;
	}*/

/*	private Set<ReservationPax> getSelectedPaxSetFromReservation(Reservation reservation, Set<Integer> paxIDS) {

		Set<ReservationPax> selectedPaxSet = new HashSet<>();

		for (ReservationPax pax : reservation.getPassengers()) {
			if (paxIDS.contains(pax.getPnrPaxId())) {
				selectedPaxSet.add(pax);
				;
			}
		}

		return selectedPaxSet;
	}*/

/*	private void populatePaxWiseAffectedRule() {

		Set<Integer> pnrPaxidSet = new HashSet<>();

//R		for (String ruleRef : dataContext.getObserverWiseRuleMap().get(observerRef)) {
//R			if (dataContext.getRuleWiseAffectedPaxMap().get(ruleRef) != null) {
//R				pnrPaxidSet.addAll(dataContext.getRuleWiseAffectedPaxMap().get(ruleRef));
//R			}
//R		}

		for (Integer paxId : pnrPaxidSet) {

			//R			for (String ruleRef : dataContext.getRuleWiseAffectedPaxMap().keySet()) {
//R				if (dataContext.getObserverWiseRuleMap().get(observerRef).contains(ruleRef)) {
			//R					Set<Integer> paxIdSet = dataContext.getRuleWiseAffectedPaxMap().get(ruleRef);
			//R					if (paxIdSet.contains(paxId)) {
			//R						addToPaxWiseAffectedRuleMap(paxId, ruleRef);
					}
//R				}
			}
	//R		}
	//R	}
*/
/*	private void addToPaxWiseAffectedRuleMap(Integer paxId, String ruleRef) {

		if (paxWiseAffectedRule.keySet().contains(paxId)) {
			paxWiseAffectedRule.get(paxId).add(ruleRef);
		} else {
			Set<String> ruleRefSet = new HashSet<>();
			ruleRefSet.add(ruleRef);
			paxWiseAffectedRule.put(paxId, ruleRefSet);
		}

	}*/

/*	private void populateObserverValidAffectedRules() {
		observerValidAffectedRules = new HashSet<String>(); 
		//R		for (String affectedRuleRef : dataContext.getRuleWiseAffectedPaxMap().keySet()) {
//R			if (dataContext.getObserverWiseRuleMap().get(observerRef).contains(affectedRuleRef)) {
		//R				observerValidAffectedRules.add(affectedRuleRef);
//R			}
				//R		}

	}*/
}
