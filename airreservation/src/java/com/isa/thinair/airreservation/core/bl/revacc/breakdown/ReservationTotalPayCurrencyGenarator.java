package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * AARESAA-27605
 * @author rajitha
 *
 */
public class ReservationTotalPayCurrencyGenarator {

	private PayCurrencyDTO payCurrencyDTO;

	public ReservationTotalPayCurrencyGenarator(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	public BigDecimal getTotalPayByCurrency(Map<Long, ArrayList<ReservationPaxOndPayment>> exchangerateReservationOndPaymentMap,
			Map<Long, CurrencyExchangeRate> idExchangeRateMap) {

		BigDecimal totalPayCurrencyAmount = BigDecimal.ZERO;
		CurrencyExchangeRate currencyExchangeRate = null;

		Set<Long> exchangeRangeIdSet = exchangerateReservationOndPaymentMap.keySet();

		for (Long exchangeRateId : exchangeRangeIdSet) {

			currencyExchangeRate = idExchangeRateMap.get(exchangeRateId);
			ArrayList<ReservationPaxOndPayment> exchangerateResOndPaymentList = exchangerateReservationOndPaymentMap
					.get(exchangeRateId);
			if (!exchangerateResOndPaymentList.isEmpty()) {
				BigDecimal ammountToAddPerExchangeRate = BigDecimal.ZERO;
				for (ReservationPaxOndPayment reservationPaxOndPayment : exchangerateResOndPaymentList) {

					ammountToAddPerExchangeRate = AccelAeroCalculator.add(ammountToAddPerExchangeRate,
							reservationPaxOndPayment.getAmount());
				}

				totalPayCurrencyAmount = AccelAeroCalculator.add(totalPayCurrencyAmount,
						convertAmount(ammountToAddPerExchangeRate, getPayCurrencyDTO(), currencyExchangeRate));
			}
		}

		return totalPayCurrencyAmount;

	}

	private BigDecimal convertAmount(BigDecimal totalPayAmount, PayCurrencyDTO payCurrencyDTO,
			CurrencyExchangeRate currencyExchangeRate) {
		if (AppSysParamsUtil.getBaseCurrency().equals(payCurrencyDTO.getPayCurrencyCode())) {
			return totalPayAmount;
		} else {
			return AccelAeroCalculator.multiply(totalPayAmount, currencyExchangeRate.getMultiplyingExchangeRate());
		}
	}

	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

}
