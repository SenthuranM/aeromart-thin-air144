package com.isa.thinair.airreservation.core.remoting.ejb;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.sql.RowSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.CarrierDTO;
import com.isa.thinair.airreservation.api.dto.CashSalesStatusDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardInformationDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardSalesStatusDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.UserDetails;
import com.isa.thinair.airreservation.api.dto.autocheckin.CheckinPassengersInfoDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.cesar.FlightLegPaxTypeCountTO;
import com.isa.thinair.airreservation.api.dto.eticket.PaxEticketTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.FlightLoadInfoDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.dto.pal.PALTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVTrasmissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.AirportPassengerMessageTxHsitory;
import com.isa.thinair.airreservation.api.model.FlightLoad;
import com.isa.thinair.airreservation.api.model.PNRGOVTxHistoryLog;
import com.isa.thinair.airreservation.api.model.PalCalHistory;
import com.isa.thinair.airreservation.api.model.PalCalTiming;
import com.isa.thinair.airreservation.api.model.PnlAdlHistory;
import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.TermsTemplate;
import com.isa.thinair.airreservation.api.model.TermsTemplateAudit;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.TermsTemplateUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.ReservationAuxiliaryBL;
import com.isa.thinair.airreservation.core.bl.SalesBL;
import com.isa.thinair.airreservation.core.bl.adl.ADLBL;
import com.isa.thinair.airreservation.core.bl.automaticcheckin.AutomaticCheckinBL;
import com.isa.thinair.airreservation.core.bl.common.InsuranceHelper;
import com.isa.thinair.airreservation.core.bl.meal.MealNotifierBL;
import com.isa.thinair.airreservation.core.bl.pal.CALBL;
import com.isa.thinair.airreservation.core.bl.pal.PALBL;
import com.isa.thinair.airreservation.core.bl.pal.PalScheduleBL;
import com.isa.thinair.airreservation.core.bl.pal.ResendPalCalBL;
import com.isa.thinair.airreservation.core.bl.pnl.PNLBL;
import com.isa.thinair.airreservation.core.bl.pnl.PnlSchedulingBL;
import com.isa.thinair.airreservation.core.bl.pnl.ResendPnlAdlBL;
import com.isa.thinair.airreservation.core.bl.pnrgov.PNRGOVSchedulingBL;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SalesDAO;
import com.isa.thinair.airreservation.core.service.bd.ReservationAuxilliaryServiceBeanLocal;
import com.isa.thinair.airreservation.core.service.bd.ReservationAuxilliaryServiceBeanRemote;
import com.isa.thinair.airschedules.api.model.FlightsToPublish;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.TermsTemplateAuditDTO;
import com.isa.thinair.commons.api.dto.TermsTemplateDTO;
import com.isa.thinair.commons.api.dto.TermsTemplateSearchDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * Session bean to provide Reservation Service related functionalities
 * 
 * @author Isuru
 * 
 * @since 1.0
 */
@Stateless
@RemoteBinding(jndiBinding = "ReservationAuxilliaryService.remote")
@LocalBinding(jndiBinding = "ReservationAuxilliaryService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ReservationAuxilliaryServiceBean extends PlatformBaseSessionBean implements ReservationAuxilliaryServiceBeanLocal,
		ReservationAuxilliaryServiceBeanRemote {

	private static Log log = LogFactory.getLog(ReservationAuxilliaryServiceBean.class);

	/**
	 * Method Called By a scheduler Job, that checks for failed PNLS/ADLS
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void checkAndResendPNLADL() throws ModuleException {
		log.debug("!!!!!!!!!!!!  Before Checking and Resending PNLADL");
		try {
			new ResendPnlAdlBL().checkAndResend();
			log.debug("!!!!!!!!!!!!  After Checking and Resending PNLADL");
		} catch (CommonsDataAccessException e) {
			log.error("########## Data Access Exception occured : ", e);
			throw new ModuleException(e.getExceptionCode(), e);
		}

	}

	/**
	 * NOTE: Called By Scheduled Service Method. Will Generate and Transfer to Sql Server. Transfering is based on
	 * configureation.
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void transferCashSales(Date date) throws ModuleException {
		try {
			log.debug("!!!!!!!!!!  Before Generating and  Transfereing Cash Sales :" + date);
			SalesBL bl = new SalesBL();
			bl.transferCashSales(date);
			log.debug("!!!!!!!!!!  After Generating and Transfereing Cash Sales :" + date);
		} catch (CommonsDataAccessException cdaex) {
			log.error("!!!!!!!!!! Error When Generating and Transferring Cash Sales :" + date, cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * NOTE: Called By Scheduled Service Method. Will Generate and Transfer to Sql Server. Transfering is based on
	 * configureation.
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void transferCreditCardSales(Date date) throws ModuleException {
		try {
			log.debug("!!!!!!!!!!  Before Generating and  Transfereing Credit card Sales :" + date);
			SalesBL bl = new SalesBL();
			bl.transferCreditSales(date);
			log.debug("!!!!!!!!!!  After Generating and  Transfereing Credit card Sales :" + date);
		} catch (CommonsDataAccessException cdaex) {
			log.error("!!!!!!!!!! Error When Generating and Transferring Credit card Sales :" + date, cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * NOTE: Called By Scheduled Service Method. Will Generate and Transfer to Sql Server. Transfering is based on
	 * configureation.
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void transferAgentTopUp(Date date) throws ModuleException {
		try {
			log.debug("!!!!!!!!!!  Before Generating and  Transfereing Agent top up :" + date);
			SalesBL bl = new SalesBL();
			bl.transferTopUp(date);
			log.debug("!!!!!!!!!!  After Generating and  Transfereing Agent top up :" + date);
		} catch (CommonsDataAccessException cdaex) {
			log.error("!!!!!!!!!! Error When Generating and Transferring Agent top up :" + date, cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<AgentTotalSaledDTO> getAgentTotalSales(Date startDate, Date endDate) throws ModuleException {
		SalesBL bl = new SalesBL();
		try {
			return bl.getAgentTotalSales(startDate, endDate);
		} catch (CommonsDataAccessException dae) {
			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ArrayList<UserDetails> getUsersForTravelAgent(String agentName) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.SALES_DAO.getUsersForTravelAgent(agentName);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<CreditCardInformationDTO> getActiveCards() throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getCreditCardInformation();
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public CashSalesStatusDTO getCashSalesDetails(String users[], Date fromDate, Date toDate, String agentCode)
			throws ModuleException {
		try {
			SalesBL bl = new SalesBL();
			return bl.getCashSalesDetails(users, fromDate, toDate, agentCode);
		} catch (CommonsDataAccessException dae) {
			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public CreditCardSalesStatusDTO getCreditCardSalesDetails(Integer cards[], Date fromDate, Date toDate) throws ModuleException {
		try {
			SalesBL bl = new SalesBL();
			return bl.getCreditCardSalesDetails(cards, fromDate, toDate);
		} catch (CommonsDataAccessException dae) {
			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void transferManualCashSalesCollection(Collection<Date> datecollection) throws ModuleException {
		try {
			SalesBL bl = new SalesBL();
			bl.transferManualCashSalesCollection(datecollection);
		} catch (CommonsDataAccessException me) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

		catch (ModuleException me) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void transferManualCreditCardSalesCollection(Collection<Date> dateCollection) throws ModuleException {
		try {
			SalesBL bl = new SalesBL();
			bl.transferManualCreditCardSalesCollection(dateCollection);
		}

		catch (CommonsDataAccessException dae) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		} catch (ModuleException me) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public void sendPNLAuxiliary(String flightNumber, String departureStation, Date dateofflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException {
		try {

			PNLBL pnlbl = new PNLBL();
			pnlbl.sendPNLAuxiliary(flightNumber, departureStation, dateofflight, sitaaddresses, sendingMethod);

		} catch (CommonsDataAccessException dae) {
			log.error("########## ERROR in Send New Manual PNL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateofflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		} catch (ModuleException me) {
			log.error("########## ERROR in Send New Manual PNL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateofflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public void sendPNL(String flightNumber, String departureStation, Date dateofflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException {
		try {

			PNLBL pnlbl = new PNLBL();
			pnlbl.sendPNL(flightNumber, departureStation, dateofflight, sitaaddresses, sendingMethod);

		} catch (CommonsDataAccessException dae) {
			log.error("########## ERROR in Send New Manual PNL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateofflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		} catch (ModuleException me) {
			log.error("########## ERROR in Send New Manual PNL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateofflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public void sendADLAuxiliary(String flightNumber, String departureStation, Date dateOfflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException {
		try {

			// new ADLBL().sendADLAuxiliary(flightNumber, departureStation, dateOfflight, sitaaddresses, sendingMethod);
			// new ADL implementation
			new ADLBL().sendADL(flightNumber, departureStation, dateOfflight, sitaaddresses, sendingMethod);

		} catch (CommonsDataAccessException dae) {
			log.error("########## ERROR in Send New Manual ADL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateOfflight);
			this.sessionContext.setRollbackOnly();

			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		} catch (ModuleException me) {
			log.error("########## ERROR in Send New Manual ADL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateOfflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	/**
	 * Returns the saved PNL / ADL
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getSavedPnlAdl(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getSavedPnlAdl(flightID, timestamp, sitaAddress,
					departureStation, messageType, transmissionStatus);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Resends the saved PNL / ADL
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void resendExistingManualPNLADL(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus, String[] sitaaddresses, String mailserver, String flightNumber,
			Date dateOfFlight) throws ModuleException {
		try {
			ReservationAuxiliaryBL bl = new ReservationAuxiliaryBL();
			bl.resendExistingManualPNLADL(flightID, timestamp, sitaAddress, departureStation, messageType, transmissionStatus,
					sitaaddresses, mailserver, getUserPrincipal().getUserId(), flightNumber, dateOfFlight);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public ServiceResponce sendPNL(int fid, String departureairport) throws ModuleException {

		try {
			PNLBL pnlbl = new PNLBL();
			pnlbl.sendPNL(fid, departureairport);

			return new DefaultServiceResponse(true);

		} catch (CommonsDataAccessException cdaex) {
			log.error("########## ERROR sendPNL FltID:" + fid + " Dep Station :" + departureairport, cdaex);
			this.sessionContext.setRollbackOnly();

			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		} catch (ModuleException me) {
			log.error("########## ERROR sendPNL FltID:" + fid + " Dep Station :" + departureairport, me);
			this.sessionContext.setRollbackOnly();
			throw me;
		} catch (Exception e) {
			log.error("########## ERROR sendPNL FltID:" + fid + " Dep Station :" + departureairport, e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE, e);

		}
	}

	/**
	 * Will Save the POJO
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertToHistory(String msgType, HashMap<String, String> sitaMap, int flightId, String depAirport,
			String fileContent, Timestamp now, HashMap<String, Integer> ccPaxCountMap, ModuleException customError,
			String sendingMethodType, String lastGroupCode) throws ModuleException {

		ModuleException t = null;
		try {

            log.info("User id is : " + getUserPrincipal().getUserId() + " Flight ID is: " + flightId) ;		
			if(PNLConstants.MessageTypes.PNL.equals(msgType) || PNLConstants.MessageTypes.ADL.equals(msgType)){
				PnlAdlUtil.insertToHistory(msgType, sitaMap, flightId, depAirport, fileContent, now, ccPaxCountMap, customError,
				sendingMethodType, lastGroupCode, getUserPrincipal().getUserId());
			}else if(PNLConstants.MessageTypes.PAL.equals(msgType) || PNLConstants.MessageTypes.CAL.equals(msgType)){
				PnlAdlUtil.insertToPalCalHistory(msgType, sitaMap, flightId, depAirport, fileContent,
						now,ccPaxCountMap, customError, sendingMethodType, lastGroupCode,	getUserPrincipal().getUserId());
			}

		} catch (CommonsDataAccessException cdaex) {
			log.error("Error In Inserting "+ msgType +" to History:", cdaex);
			this.sessionContext.setRollbackOnly();
			t = new ModuleException(cdaex, cdaex.getExceptionCode());

		} catch (Throwable e) {
			log.error("Error In Inserting "+ msgType +" to History:", e);
			t = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);
			this.sessionContext.setRollbackOnly();

		} finally {
			// idieally code should never ever come here where t is not = to
			// null;
			if (t != null) {
				String heading = "INSERTING TO HISTORY FAILED FOR " + msgType;
				String details = "[Flight ID: :" + flightId + "\n Dep Airport: " + depAirport + "\n FILE CONTENT \n"
						+ fileContent + "\n";
				details += customError.getMessageString();

				PnlAdlUtil.notifyAdminForManualAction(null, null, depAirport, t, heading, details);

			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveDCSPnlAdlHistory(PnlAdlHistory pnlAdlHistory) throws ModuleException {
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		auxilliaryDAO.savePnlAdlHistory(pnlAdlHistory);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public ServiceResponce sendADL(int fid, String departureairport) throws ModuleException {

		try {
			ADLBL adlbl = new ADLBL();

			// adlbl.sendADL(fid, departureairport);
			// New ADL implementation
			adlbl.sendADL(fid, departureairport);
			return new DefaultServiceResponse(true);
		} catch (CommonsDataAccessException cdaex) {
			log.error("########## ERROR sendADL FltID:" + fid + " Dep Station :" + departureairport, cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (ModuleException me) {
			log.error("########## ERROR sendADL FltID:" + fid + " Dep Station :" + departureairport, me);
			this.sessionContext.setRollbackOnly();
			throw me;
		} catch (Exception ee) {
			log.error("########## ERROR sendADL FltID:" + fid + " Dep Station :" + departureairport, ee);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE, ee);

		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updatePaxFareSegments(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds,
			String status, Map<String, String> pnrPaxIdSegIdGroupId) throws ModuleException {
		try {
			PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIds, pnrPaxIds, status, pnrPaxIdSegIdGroupId, PNLConstants.MessageTypes.ADL);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updatePnlPassengerStatus(Collection<Integer> pnrSegIds) throws ModuleException {
		try {
			PnlAdlUtil.updatePnlPassengerStatus(pnrSegIds);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updatePnlPassengers(Collection<Integer> pnlPaxIds) throws ModuleException {
		try {
			PnlAdlUtil.updatePnlPassengers(pnlPaxIds);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<AirportPassengerMessageTxHsitory> getPnlAdlHistory(String flightId, Date date, String airport) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlAdlHistory(flightId, date, airport);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePnlAdlTiming(PnlAdlTiming timing) throws ModuleException {
		try {
			PnlSchedulingBL pnlSchedulingBL = new PnlSchedulingBL();
			pnlSchedulingBL.savePnlAdlTiming(timing);
		} catch (ModuleException e) {
			throw e;
		}

		catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page getAllTimings(String flightNumber, String airportCode, int startRec, int numOfRecs) throws ModuleException {
		try {
			if (airportCode == null || numOfRecs < 0 || startRec < 0) {
				throw new ModuleException("airreservations.auxilliary.timing.null");
			}
			return ReservationDAOUtils.DAOInstance.PNL_ADL_TIMINGS_DAO.getAllTimings(flightNumber, airportCode, startRec,
					numOfRecs);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ArrayList<PNLTransMissionDetailsDTO> getFlightForPnlAdlScheduling() throws ModuleException {
		try {
			return new PnlSchedulingBL().getFlightForPnlAdlScheduling();
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int getFlightForPnlAdlDepartureGap(PNLTransMissionDetailsDTO pnlTransMissionDetailsDTO) throws ModuleException {
		try {
			return new PnlSchedulingBL().getFlightForPnlAdlDepartureGap(pnlTransMissionDetailsDTO);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ArrayList<MealNotificationDetailsDTO> getFlightForMealNotificationScheduling() throws ModuleException {
		try {
			return new MealNotifierBL().getFlightForMealNotifyScheduling();
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean hasPnlSentHistory(String flightNumber, Date fligthDepZuluDate, String airportCode) throws ModuleException {

		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		try {
			return auxilliaryDAO.hasPnlHistory(flightNumber, fligthDepZuluDate, airportCode);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<FlightLegPaxTypeCountTO> getFlightLegPaxTypeCount(String flightNumber, String origin, String destination,
			Date departureDateZulu) throws ModuleException {

		log.debug(" getFlightLegPaxTypeCount entered");

		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Collection<FlightLegPaxTypeCountTO> dtos = new ArrayList<FlightLegPaxTypeCountTO>();

		try {

			if (log.isDebugEnabled()) {
				log.debug(" getFlightLegPaxTypeCount entered PARAMS are:" + flightNumber + "|" + origin + "|" + destination + "|"
						+ departureDateZulu);
			}
			FlightLegPaxTypeCountTO flptc = auxilliaryDAO.getFlightLegPaxTypeCount(flightNumber, origin, destination,
					departureDateZulu);
			dtos.add(flptc);
			return dtos;

		} catch (CommonsDataAccessException me) {
			log.error(me);
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (Throwable me) {
			log.error(me);
			throw new ModuleException(me, "module.runtime.error");
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void reconcileInsurancesForFailures(Date dateFrom) throws ModuleException {
		log.debug("######## reconcileInsurancesForFailures START Since :" + dateFrom);

		try {

			Map<String, List<Integer>> failedInsurances = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
					.getInconsitentInsurances(dateFrom);

			if (failedInsurances != null && !failedInsurances.isEmpty()) {
				for (String pnr : failedInsurances.keySet()) {
					List<Integer> insuranceRefIds = failedInsurances.get(pnr);
					reconcileFailedInsurances(pnr, insuranceRefIds);
				}
			}
		} catch (Exception e) {
			log.error("Error in reconcileInsurancesForFailures", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("module.runtime.error");
		}

		log.debug("######## reconcileInsurancesForFailures END Since :" + dateFrom);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<InsuranceResponse> reconcileFailedInsurances(String pnr, List<Integer> insuranceRefs) throws ModuleException {
		
		List<InsuranceResponse> insuranceResponses = null;

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);

		Reservation reservation = AirproxyModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
		List<InsuranceRequestAssembler> requestAssemblers = populateInsuranceRequests(reservation);

		// sell
		if (requestAssemblers != null && !requestAssemblers.isEmpty()) {
			if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
				// Improve this to have inside populateInsuranceRequests
				for (InsuranceRequestAssembler requestAssembler : requestAssemblers) {
					requestAssembler.setFlightSegments(InsuranceHelper.createFlightSegList(reservation));
					requestAssembler.getInsureSegment().setSalesChanelCode(SalesChannel.TRAVEL_AGENT);
				}
			}

			List<IInsuranceRequest> insuranceRequests = new ArrayList<IInsuranceRequest>();
			for (IInsuranceRequest insuranceReq : requestAssemblers) {
				insuranceRequests.add(insuranceReq);
			}

			insuranceResponses = AirproxyModuleUtils.getInsuranceClientBD().sellInsurancePolicy(insuranceRequests);
		}

		// persistence
		for (Integer insuranceRef : insuranceRefs) {
			for (InsuranceResponse insuranceResponse : insuranceResponses) {
				if (insuranceRef.equals(insuranceResponse.getInsuranceRefId())) {
					ReservationInsurance reservationInsurance = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
							.getReservationInsurance(insuranceResponse.getInsuranceRefId());

					reservationInsurance.setPolicyCode(insuranceResponse.getPolicyCode());
					String errorDetails = reservationInsurance.getDescription() + "|Scheduler|"
							+ insuranceResponse.getErrorCode() + "|" + insuranceResponse.getErrorMessage();

					if (insuranceResponse.getQuotedTotalPremiumAmount() != null
							&& reservationInsurance.getQuotedAmount() != null
							&& Math.round(insuranceResponse.getQuotedTotalPremiumAmount().doubleValue()) != Math
									.round(reservationInsurance.getQuotedAmount().doubleValue())) {
						errorDetails += "New Quoted Amount=" + insuranceResponse.getQuotedTotalPremiumAmount();
					} else if (insuranceResponse.isSuccess()) {
						reservationInsurance.setAmount(insuranceResponse.getTotalPremiumAmount());
						reservationInsurance.setQuotedAmount(insuranceResponse.getQuotedTotalPremiumAmount());
					}

					if (errorDetails.length() > 100) {
						errorDetails = errorDetails.substring(0, 99);
					}
					reservationInsurance.setDescription(errorDetails);
					if (insuranceResponse.isSuccess()) {
						reservationInsurance.setState(ReservationInternalConstants.INSURANCE_STATES.SC.toString());
						reservationInsurance.setSellTime(new Date());
					} else {
						reservationInsurance.setState(ReservationInternalConstants.INSURANCE_STATES.FL.toString());
					}
					ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveReservationInsurance(reservationInsurance);
				}
			}
		}

		return insuranceResponses;

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveSellInfo(List<ReservationInsurance> iRList) throws ModuleException {
		try {
			log.debug("Inside saveSellInfo");

			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveReservationInsurance(iRList);
			log.debug("exit saveSellInfo");

		} catch (Throwable e) {
			log.error("Error in Saving saveSellInfo", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airreservation.save.sell.failed");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservationInsurance getInsurance(Integer id) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getReservationInsurance(id);

	}

	/**
	 * Returns the All Carrier codes
	 * 
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<CarrierDTO> getCarriers() throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getCarriers();
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getCarriers ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public RowSet getSalesTransferServiceData(Collection<Date> dateList, Collection<Integer> nominalCodes) throws ModuleException {
		SalesDAO salesDAO = ReservationDAOUtils.DAOInstance.SALES_DAO;
		RowSet rs = salesDAO.getSalesByDate(dateList, nominalCodes);
		return rs;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String loadAirportMessagesTranslation(Integer airportMsgId, String lang) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.loadAirportMessagesTranslation(airportMsgId, lang);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String loadI18AirportMessagesTranslation(String airportMsgId, String lang) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.loadI18AirportMessagesTranslation(airportMsgId, lang);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendMealNotificaions(List<Integer> flightIds) throws ModuleException {

		log.debug("##################### SENDING MEAL NOTIFICATION ################################");
		try {
			new MealNotifierBL().sendMealNotificationsNew(flightIds);
			log.debug("#######################MEAL NOTIFICATION SENT ################################");
		} catch (CommonsDataAccessException e) {
			log.error("########## Data Access Exception occured : ", e);
			throw new ModuleException(e.getExceptionCode(), e);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendFlightLoadInfo() throws ModuleException {
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;

		try {
			log.info("Searching for flight load details that scheduled to send");
			List<FlightLoad> flightLoads = auxilliaryDAO.getFlightLegLoadInfoScheduledForSending();
			log.info("Flight load search complete. No of flight details to send [" + flightLoads.size() + "]");
			if (flightLoads.isEmpty()) {
				return;
			}

			boolean status = false;
			try {
				status = ReservationModuleUtils.getWSClientBD().publishFlightLoadInformation(flightLoads);
				log.info("Flight load details published. Status [" + status + "]");
			} catch (ModuleException e) {
				log.error("Error in publishing flight load details.", e);
			}

			for (FlightLoad flightLoad : flightLoads) {
				flightLoad.setStatus(status
						? FlightLoad.FLIGHT_LOAD_PUBLISH_STATUS_SUCCESS
						: FlightLoad.FLIGHT_LOAD_PUBLISH_STATUS_FAILED);
			}

			auxilliaryDAO.saveFlightLoad(flightLoads);
			log.info("Flight load details saved.");
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (Throwable me) {
			throw new ModuleException(me, "module.runtime.error");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean hasFlownSegments(Collection<Integer> pnrSegIds) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.hasFlownSegments(pnrSegIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, String> getFltSegWiseBkgClasses(Collection<Integer> pnrSegIds) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getFltSegWiseBkgClasses(pnrSegIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, String> getFltSegWiseCabinClasses(Collection<Integer> pnrSegIds) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getFltSegWiseCabinClasses(pnrSegIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Date> getFltSegWiseLastFQDates(List<Integer> flownPnrSegIds) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getFltSegWiseLastFQDates(flownPnrSegIds);
	}

	@Override
	public List<PNRGOVTrasmissionDetailsDTO> getFlightForPnrGovScheduling(String airportCode, int timePeriod,
			int pnrGovTrasmissionGap, boolean isOutbound) throws ModuleException {
		try {
			return new PNRGOVSchedulingBL().getFlightsForPnrGovScheuduling(airportCode, timePeriod, pnrGovTrasmissionGap,
					isOutbound);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	public boolean hasPnrGovSentHistory(int fltSegId, String countryCode, String airportCode, boolean isOutbound, int timePeriod)
			throws ModuleException {
		return ReservationDAOUtils.DAOInstance.PNRGOV_DAO.hasPnrGovMessageSent(fltSegId, countryCode, airportCode, isOutbound,
				timePeriod);
	}

	@Override
	public Page getPNRGOVReports(int startrec, int numofrecs) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PNRGOV_DAO.getPNRGOVReports(startrec, numofrecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), e.getModuleCode());
		}
	}

	@Override
	public Page getPNRGOVReports(int startrec, int numofrecs, List criteria) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PNRGOV_DAO.getPNRGOVReports(startrec, numofrecs, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), e.getModuleCode());
		}
	}

	public PNRGOVTxHistoryLog getHistoryLog(String logID) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.PNRGOV_DAO.getHistoryLog(logID);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), e.getModuleCode());
		}
	}

	@Override
	public void publishPendingFlights(Date startDate, Date endDate) throws ModuleException {
		log.info("Starting to publish flight details");

		try {

			if (AppSysParamsUtil.isCargoFlashFlightPublishingEnabled()) {
				publishFlightDetailsToCargoFlash(startDate, endDate);
			}

			log.info("Flight Details publishing Completed");
		} catch (CommonsDataAccessException e) {
			log.error("Error in CargoFlash Flight Detail Publishing", e);
			throw new ModuleException(e.getCause(), e.getExceptionCode(), e.getModuleCode());
		}

	}

	private void publishFlightDetailsToCargoFlash(Date startDate, Date endDate) throws ModuleException {

		try {
			// Cargo Flash flight details publishing enabled

			if (log.isDebugEnabled()) {
				log.debug(" #CARGO Flash flight details publishing is enabled, Starting to Fetch data to upload");
			}

			List<String> statusToSearch = new ArrayList<String>();
			statusToSearch.add(FlightsToPublish.PENDING);
			statusToSearch.add(FlightsToPublish.ERROR);

			// Find PENDING and ERROR status flights to publish
			List<FlightsToPublish> flightsToPublish = getFlightDAO().getFlightsToPublishByStatus(statusToSearch, startDate,
					endDate);

			if (flightsToPublish != null && !flightsToPublish.isEmpty()) {

				log.info("#CARGO Flights are waiting to be published to Cargo Flash" + flightsToPublish.size());

				boolean response = false;
				List<FlightsToPublish> sublist = null;
				int totalSize = flightsToPublish.size();
				// If the size is more than FlightsToPublish.MAX_BULK_SIZE, update in bulks
				for (int i = 0; i < totalSize; i = i + FlightsToPublish.MAX_BULK_SIZE) {

					try {
						sublist = new ArrayList<FlightsToPublish>();

						if (log.isDebugEnabled()) {
							log.debug("#CARGO Flash started publishing sublist no : " + i);
						}

						// Sub list is always from 0 because we remove the used flights from the original list
						if (flightsToPublish.size() <= FlightsToPublish.MAX_BULK_SIZE) {
							sublist = new LinkedList<FlightsToPublish>(flightsToPublish.subList(0, flightsToPublish.size()));
						} else {
							sublist = new LinkedList<FlightsToPublish>(
									flightsToPublish.subList(0, FlightsToPublish.MAX_BULK_SIZE));

						}

						response = ReservationModuleUtils.getWSClientBD().publishFlightChangeInformation(sublist);
						flightsToPublish.removeAll(sublist);

						if (log.isDebugEnabled()) {
							log.debug("#CARGO Flash end publishing sublist no : " + i);
						}

					} catch (Exception e) {
						response = false;
						log.error("Error in publishing cargo flash flight schdule sub list to webservice ", e);
					} finally {
						if (sublist != null && !sublist.isEmpty()) {
							if (response) {
								updateProcessedStatus(sublist, FlightsToPublish.SUCCESS);
							} else {
								updateProcessedStatus(sublist, FlightsToPublish.ERROR);
							}
						}
					}
				}

			} else {
				if (log.isDebugEnabled()) {
					log.debug("#CARGO Flights not found to publish , aborting");
				}
			}
			log.info("#CARGO Flights publishing is Completed");

		} catch (CommonsDataAccessException e) {
			log.error("Error in CargoFlash Flight Detail Publishing", e);
			throw new ModuleException(e.getCause(), e.getExceptionCode(), e.getModuleCode());
		} catch (Exception e) {
			log.error("Error in CargoFlash Flight Detail Publishing", e);
			throw new ModuleException(e, "module.runtime.error");
		}
	}

	private void updateProcessedStatus(List<FlightsToPublish> flightsToPublish, String status) throws ModuleException {
		List<Integer> idsToUpdate = new ArrayList<Integer>();

		for (FlightsToPublish toPublish : flightsToPublish) {
			idsToUpdate.add(toPublish.getFlightsToPublishId());
		}

		getFlightDAO().updateFlightsToPublishStatus(idsToUpdate, status);
	}

	private FlightDAO getFlightDAO() {
		return (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<TermsTemplateDTO> getTermsTemplates() throws ModuleException {
		List<TermsTemplate> termsTemplates = ReservationDAOUtils.DAOInstance.TERMS_TEMPLATE_DAO.getAllTermsTemplates();
		return TermsTemplateUtils.toTermsTemplateDTOCOllection(termsTemplates);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateTermsTemplate(TermsTemplateDTO termsTemplateDTO, TermsTemplateAuditDTO termsAuditDTO)
			throws ModuleException {
		try {
			TermsTemplate termsTemplate = TermsTemplateUtils.toTermsTemplate(termsTemplateDTO);
			TermsTemplateAudit termsTemplateAudit = TermsTemplateUtils.toTermsTemplateAudit(termsAuditDTO);
			ReservationDAOUtils.DAOInstance.TERMS_TEMPLATE_DAO.updateTermsTemplate(termsTemplate, termsTemplateAudit);
		} catch (Exception e) {
			throw new ModuleException(e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page<TermsTemplateDTO> getTermsTemplatePage(TermsTemplateSearchDTO searchCriteria, Integer start, Integer recSize)
			throws ModuleException {
		try {
			Page<TermsTemplate> termsTemplatePage = ReservationDAOUtils.DAOInstance.TERMS_TEMPLATE_DAO.getTermsTemplatePage(
					searchCriteria, start, recSize);

			return TermsTemplateUtils.convertPageToDTO(termsTemplatePage);

		} catch (Exception e) {
			throw new ModuleException(e, "module.runtime.error");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<PaxEticketTO> getPaxEtickets(int flightId, String stationCode) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPaxEtickets(flightId, stationCode);
	}

	// temporal functions
	/**
	 * NOTE: REFACTOR This is the same method used inside CreateInsurance
	 */
	private static List<InsuranceRequestAssembler> populateInsuranceRequests(Reservation reservation) {
		log.debug("Start - populateInsuranceRequests for resell");
		List<InsuranceRequestAssembler> requestAsssemblers = null;
		// the request obj to populate
		InsuranceRequestAssembler requestAsssembler = null;
		// get the hold insurance stored in db
		List<ReservationInsurance> reservationInsurances = reservation.getReservationInsurance();
		// contact info should be there for the reservation
		ReservationContactInfo reservationContacts = reservation.getContactInfo();
		// reservation passengers
		Set<ReservationPax> reservationPassengers = reservation.getPassengers();

		boolean isAllDomestic = true;
		if (reservation.getSegmentsView() != null) {
			for (ReservationSegmentDTO segment : reservation.getSegmentsView()) {
				if (segment.isInternationalFlight()) {
					isAllDomestic = false;
				}
			}
		}
		try {
			if (reservationInsurances != null && reservationContacts != null && reservationPassengers != null) {
				requestAsssemblers = new ArrayList<InsuranceRequestAssembler>();

				for (ReservationInsurance resIns : reservationInsurances) {
					InsureSegment insureSegment = new InsureSegment();
					requestAsssembler = new InsuranceRequestAssembler();
					requestAsssembler.setMessageId(resIns.getMessageId());
					requestAsssembler.setPnr(reservation.getPnr());
					requestAsssembler.setTotalPremiumAmount(resIns.getQuotedAmount());
					requestAsssembler.setInsuranceId(resIns.getInsuranceId());
					requestAsssembler.setQuotedTotalPremiumAmount(resIns.getQuotedAmount());
					requestAsssembler.setSsrFeeCode(resIns.getSsrFeeCode());
					requestAsssembler.setPlanCode(resIns.getPlanCode());
					requestAsssembler.setInsuranceType(resIns.getInsuranceType());
					requestAsssembler.setAllDomastic(isAllDomestic);

					insureSegment.setArrivalDate(resIns.getDateOfReturn());
					insureSegment.setDepartureDate(resIns.getDateOfTravel());
					insureSegment.setFromAirportCode(resIns.getOrigin());
					insureSegment.setToAirportCode(resIns.getDestination());
					insureSegment.setRoundTrip("Y".equals(resIns.getRoundTrip()) ? true : false);
					insureSegment.setSalesChanelCode(reservation.getAdminInfo().getOriginChannelId());

					requestAsssembler.addFlightSegment(insureSegment);
					Iterator<ReservationPax> iterPax = reservationPassengers.iterator();
					while (iterPax.hasNext()) {
						ReservationPax resPax = iterPax.next();
						InsurePassenger insurePassenger = new InsurePassenger();
						insurePassenger.setFirstName(resPax.getFirstName());
						insurePassenger.setLastName(resPax.getLastName());
						insurePassenger.setAddressLn1(reservationContacts.getStreetAddress1());
						insurePassenger.setAddressLn2(reservationContacts.getStreetAddress2());
						insurePassenger.setCity(reservationContacts.getCity());
						insurePassenger.setCountryOfAddress(reservationContacts.getCountryCode());
						insurePassenger.setDateOfBirth(resPax.getDateOfBirth());

						insurePassenger.setEmail(reservationContacts.getEmail());
						insurePassenger.setHomePhoneNumber(reservationContacts.getPhoneNo());
						insurePassenger.setInfant(resPax.getParent() == null ? false : true);
						requestAsssembler.addPassenger(insurePassenger);
					}
					requestAsssemblers.add(requestAsssembler);
				}
			}
		} catch (Exception e) {
			log.error("Error when trying to poplulate insurance request information", e);
		}

		log.debug("End - populateInsuranceRequests for resell");
		return requestAsssemblers;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePalCalTiming(PalCalTiming palCalTiming) throws ModuleException {
		try {
			ReservationDAOUtils.DAOInstance.PAL_CAL_TIMINGS_DAO.isduplicatesExistForPalCAlTimings(palCalTiming);
			
			ReservationDAOUtils.DAOInstance.PAL_CAL_TIMINGS_DAO.savePalCalTiming(palCalTiming);

		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page getAllPALCALTimings(String airportCode, int startRec, int numOfRecs, String flightNo) throws ModuleException {

		try {
			if (airportCode == null || numOfRecs < 0 || startRec < 0) {
				throw new ModuleException("airreservations.auxilliary.timing.null");
			}

			return ReservationDAOUtils.DAOInstance.PAL_CAL_TIMINGS_DAO.getAllPALCALTimings(airportCode, startRec, numOfRecs, flightNo);

		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	public void deletePAlCalTiming(PalCalTiming palcalTiming) throws ModuleException {
		try {
			ReservationDAOUtils.DAOInstance.PAL_CAL_TIMINGS_DAO.deletePAlCalTiming(palcalTiming);

		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	}

	@Override
	public ArrayList<PALTransMissionDetailsDTO> getFlightForPalCalScheduling() throws ModuleException {
		try {

			return new PalScheduleBL().getFlightForPalCalScheduling();

		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	public boolean hasPalSentHistory(String flightNumber, Date flightDepZulu, String airportCode) throws ModuleException {
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		try {
			return auxilliaryDAO.hasPalSentHistory(flightNumber, flightDepZulu, airportCode);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public ServiceResponce sendPALMessage(int flightId, String departureairport) throws ModuleException {
		try {

			PALBL palBL = new PALBL();
			palBL.sendPALMessage(flightId, departureairport);

			return new DefaultServiceResponse(true);

		} catch (CommonsDataAccessException cdaex) {
			log.error("########## ERROR sendPAL FltID:" + flightId + " Dep Station :" + departureairport, cdaex);
			this.sessionContext.setRollbackOnly();

			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		} catch (ModuleException me) {
			log.error("########## ERROR sendPAL FltID:" + flightId + " Dep Station :" + departureairport, me);
			this.sessionContext.setRollbackOnly();
			throw me;
		} catch (Exception e) {
			log.error("########## ERROR sendPAL FltID:" + flightId + " Dep Station :" + departureairport, e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE, e);

		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public ServiceResponce sendCALMessage(int flightId, String departureairport) throws ModuleException {
		try {

			CALBL calBL = new CALBL();

			calBL.sendCALMessage(flightId, departureairport);

			return new DefaultServiceResponse(true);
		} catch (CommonsDataAccessException cdaex) {
			log.error("########## ERROR sendCAL FltID:" + flightId + " Dep Station :" + departureairport, cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (ModuleException me) {
			log.error("########## ERROR sendCAL FltID:" + flightId + " Dep Station :" + departureairport, me);
			this.sessionContext.setRollbackOnly();
			throw me;
		} catch (Exception ee) {
			log.error("########## ERROR sendCAL FltID:" + flightId + " Dep Station :" + departureairport, ee);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE, ee);

		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void savePALCALTXHistory(PalCalHistory palCalHistory) throws ModuleException {
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		auxilliaryDAO.savePALCALTXHistory(palCalHistory);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public void sendPALMessage(String flightNumber, String departureStation, Date dateofflight, String[] sitaaddresses,
			String sendingMethod) throws ModuleException {

		try {

			PALBL pnlbl = new PALBL();
			pnlbl.sendPAL(flightNumber, departureStation, dateofflight, sitaaddresses, sendingMethod);

		} catch (CommonsDataAccessException dae) {
			log.error("########## ERROR in Send New Manual PAL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateofflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		} catch (ModuleException me) {
			log.error("########## ERROR in Send New Manual PAL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateofflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}

	
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public void sendCALAuxiliary(String flightNumber, String departureStation, Date dateOfflight, String[] sitaaddresses,
			String sendingMethod) throws ModuleException {

		try {

			new CALBL().sendCALMessage(flightNumber, departureStation, dateOfflight, sitaaddresses, sendingMethod);

		} catch (CommonsDataAccessException dae) {
			log.error("########## ERROR in Send New Manual CAL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateOfflight);
			this.sessionContext.setRollbackOnly();

			throw new ModuleException(dae, dae.getExceptionCode(), dae.getModuleCode());
		} catch (ModuleException me) {
			log.error("########## ERROR in Send New Manual CAL FNUM:" + flightNumber + " Dep Station :" + departureStation
					+ " Date Of Flight : " + dateOfflight);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void resendExistingManualPALCAL(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus, String[] sitaaddresses, String mailserver, String flightNumber,
			Date dateOfFlight) throws ModuleException {

		try {
			ReservationAuxiliaryBL bl = new ReservationAuxiliaryBL();
			bl.resendExistingManualPALCAL(flightID, timestamp, sitaAddress, departureStation, messageType, transmissionStatus,
					sitaaddresses, mailserver, getUserPrincipal().getUserId(), flightNumber, dateOfFlight);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getSavedPalCal(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException {
		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getSavedPalCal(flightID, timestamp, sitaAddress,
					departureStation, messageType, transmissionStatus);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<AirportPassengerMessageTxHsitory> getPalCalHistory(String flightNumber, Date date, String departureStation)
			throws ModuleException {

		try {
			return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPalCalHistory(flightNumber, date, departureStation);
		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void checkAndResendPALCAL() throws ModuleException {
		log.debug("!!!!!!!!!!!!  Before Checking and Resending PALCAL");
		try {
			new ResendPalCalBL().checkAndResend();
			log.debug("!!!!!!!!!!!!  After Checking and Resending PALCAL");
		} catch (CommonsDataAccessException e) {
			log.error("########## Data Access Exception occured : ", e);
			throw new ModuleException(e.getExceptionCode(), e);
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<FlightLoadInfoDTO> getFlightLoadInfo(Date departureDateFromZulu, Date departureDateToZulu)
			throws ModuleException {

		log.debug(" getFlightLegPaxTypeCount entered");

		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Collection<FlightLoadInfoDTO> dtos = new ArrayList<FlightLoadInfoDTO>();

		try {

			if (log.isDebugEnabled()) {
				log.debug(" getFlightLoadInfo entered PARAMS are::: departureDateFromZulu: " + departureDateFromZulu
						+ "| departureDateToZulu:  " + departureDateToZulu);
			}
			return auxilliaryDAO.getFlightLoadInfo(departureDateFromZulu, departureDateToZulu);

		} catch (CommonsDataAccessException me) {
			log.error(me);
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (Throwable me) {
			log.error(me);
			throw new ModuleException(me, "module.runtime.error");
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deriveAutoCheckinSeatMap(int flightId) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("-- deriveAutoCheckinSeatMap entered into method---");
		}

		try {
			ReservationAuxiliaryBL bl = new ReservationAuxiliaryBL();
			bl.deriveAutoCheckinSeatMap(flightId) ;
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		
	}

	@Override
	public void updateAutomaticCheckin(Collection<Integer> pnrPaxIds, Integer flightId, Integer noOfattempts,
			String dcsCheckinStatus, String dcsResponseText) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug(" updateAutomaticCheckin entered");
		}
		try {
			ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			auxilliaryDAO.updateAutomaticCheckin(pnrPaxIds, flightId, noOfattempts, dcsCheckinStatus, dcsResponseText);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public PaxAutomaticCheckinTO getAutoCheckinPassengerDetails(Date departureDate, String flightNumber, String title,
			String firstName, String lastName, String pnr, String seatCode, String seatSelectionStatus) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug(" getAutoCheckinPassengerDetails entered");
		}
		PaxAutomaticCheckinTO paxAutomaticCheckinTO = null;
		try {
			ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			paxAutomaticCheckinTO = auxilliaryDAO.getAutoCheckinPassengerDetails(departureDate, flightNumber, title, firstName,
					lastName, pnr, seatCode, seatSelectionStatus);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return paxAutomaticCheckinTO;
	}

	@Override
	public Collection<CheckinPassengersInfoDTO> getPassengerDetailsforCheckin(Integer flightId, Integer pnrPaxId)
			throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug(" getPassengerDetailsforCheckin entered");
		}
		Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs = null;
		try {
			ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			checkinPassengersInfoDTOs = auxilliaryDAO.getPassengerDetailsforCheckin(flightId, pnrPaxId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return checkinPassengersInfoDTOs;
	}

	@Override
	public void sendAutomaticCheckinDetails(Integer pnrPaxId, Integer flightId, Integer noOfAttempts, String dcsCheckinStatus,
			String dcsResponseText) throws ModuleException {
		AutomaticCheckinBL automaticCheckinBL = new AutomaticCheckinBL();
		if (log.isDebugEnabled()) {
			log.debug(" sendAutomaticCheckinDetails entered");
		}
		try {
			automaticCheckinBL.sendAutomaticCheckinDetails(pnrPaxId, flightId, noOfAttempts, dcsCheckinStatus, dcsResponseText);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void updateCreiditNoteforPassenger(Integer flightId) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug(" updateCreiditNoteforPassenger entered");
		}
		try {
			ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			auxilliaryDAO.updateCreiditNoteforPassenger(flightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
    @Override
	public String getAutoCheckinSeatSelectionStatus(Date departureDate, String flightNumber, String title, String firstName,
			String lastName, String pnr, String checkinSeat) throws ModuleException {
		String seatSelectionStatus;
		if (log.isDebugEnabled()) {
			log.debug(" getAutoCheckinSeatSelectionStatus entered");
		}
		try {
			ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			seatSelectionStatus = auxilliaryDAO.getAutoCheckinSeatSelectionStatus(departureDate, flightNumber, title, firstName,
					lastName, pnr, checkinSeat);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return seatSelectionStatus;
	}

	@Override
	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int paxId, String departureStation, int flightId)
			throws ModuleException {
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		return auxilliaryDAO.getAllOutBoundSequences(pnr, paxId, departureStation, flightId);
	}

}
