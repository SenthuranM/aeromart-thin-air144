/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.XAPnl;
import com.isa.thinair.airreservation.api.model.XAPnlPaxEntry;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.XAPnlDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * XAPnlDAO is the business DAO hibernate implementation
 * 
 * @author Byorn de silva
 * @since 1.0
 * @isa.module.dao-impl dao-name="XAPnlDAO"
 */
public class XAPnlDAOImpl extends PlatformBaseHibernateDaoSupport implements XAPnlDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(XAPnlDAOImpl.class);

	/**
	 * Returns the xa pnl
	 * 
	 * @param xaPnlId
	 * @return
	 */
	public XAPnl getXAPnlEntry(int xaPnlId) {
		return (XAPnl) get(XAPnl.class, new Integer(xaPnlId));
	}

	/**
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @param pnlContent
	 * @return
	 */
	public boolean hasAnEqualXAPnl(String flightNumber, String airportCode, Date departureDate, Integer partNumber) {

		String sql = "SELECT count(pnl_id)" + " FROM t_xa_pnl "
				+ " where flight_number=? and departure_date=? and  from_airport=? and part_number=? ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, departureDate, airportCode, partNumber };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		}
		return false;

	}

	/**
	 * Save XAPnl Entry
	 * 
	 * @param XAPnl
	 */
	public void saveXAPnlEntry(XAPnl xaPnl) {
		log.debug("Inside saveXAPnlEntry");

		hibernateSaveOrUpdate(xaPnl);

		log.debug("Exit saveXAPnlEntry");
	}

	/**
	 * Save XAPnl Pax Entry
	 * 
	 * @param xaPnlPaxEntry
	 */
	public void saveXAPnlPaxEntry(XAPnlPaxEntry xaPnlPaxEntry) {
		log.debug("Inside saveXAPnlPaxEntry");

		hibernateSaveOrUpdate(xaPnlPaxEntry);

		log.debug("Exit saveXAPnlPaxEntry");
	}

	/**
	 * Save XAPnl Pax Entry
	 * 
	 * @param colXAPnlPaxEntry
	 */
	public void saveXAPnlPaxEntries(Collection<XAPnlPaxEntry> colXAPnlPaxEntry) {
		log.debug("Inside saveXAPnlPaxEntries");

		hibernateSaveOrUpdateAll(colXAPnlPaxEntry);

		log.debug("Exit saveXAPnlPaxEntries");
	}

	/**
	 * Return XA PNL Pax entries
	 * 
	 * @param xaPnlId
	 * @param status
	 * @return
	 */
	public Collection<XAPnlPaxEntry> getXAPnlPaxEntries(int xaPnlId, String status) {
		if (status == null) {
			String hql = " SELECT xaPnlPaxEntry FROM XAPnlPaxEntry AS xaPnlPaxEntry " + " WHERE xaPnlPaxEntry.pnlId = ? ";

			return find(hql, new Object[] { new Integer(xaPnlId) }, XAPnlPaxEntry.class);
		} else {
			String hql = " SELECT xaPnlPaxEntry FROM XAPnlPaxEntry AS xaPnlPaxEntry "
					+ " WHERE xaPnlPaxEntry.pnlId = ? AND xaPnlPaxEntry.processedStatus = ? ";

			return find(hql, new Object[] { new Integer(xaPnlId), status }, XAPnlPaxEntry.class);
		}
	}

	@SuppressWarnings("unchecked")
	public Collection<Integer> getFailedXAPnlIds(int attemptLimit) {

		String sql = " SELECT distinct xapax.pnl_id " + " FROM t_xa_pnl xapnl, t_xa_pnl_parsed xapax " + " WHERE "
				+ " xapnl.pnl_id=xapax.pnl_id and " + " (xapax.proc_status = ? or xapax.proc_status = ? or xapax.pnr is null)"
				+ " and xapnl.number_of_attemps < ? " + " and xapnl.departure_date > ?";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Timestamp now = new Timestamp(new Date().getTime());

		Object params[] = { ReservationInternalConstants.XAPnlProcessStatus.NOT_PROCESSED,
				ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED, new Integer(attemptLimit), now };

		Collection<Integer> xapnlIds = (Collection<Integer>) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> xapnlIds = new ArrayList<Integer>();

				while (rs.next()) {
					xapnlIds.add(new Integer(rs.getInt(1)));
				}
				return xapnlIds;

			}

		});

		return xapnlIds;
	}

}