/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.core.security.UserPrincipal;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.model.BookingClass.BookingClassType;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CSTypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.FlightCOSDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationAuditCreator;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.airporttransfer.AirportTransferBL;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.ExternalGenericChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.flexi.FlexiBL;
import com.isa.thinair.airreservation.core.bl.flexi.FlexiRetainDTO;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for cancelling a segment
 * <p>
 * Notes: Supports per OND cancellation which means at a given time calling this method is for an OND only.
 * </p>
 * <p>
 * 
 * Business Rules:
 * </p>
 * <ol>
 * <li>(1) Cancellation charges applies for per passenger per fare (OND).</li>
 * <li>(2) Any infant will not have any cancellation or modification charges.</li>
 * <li>(3) All adult/infant charges are refundable based on refundable or not</li>
 * <li>(4) If it's refundable there will be record(s) written to ReservationPaxOndCharge level</li>
 * <li>(5) Custom cancellation charge is for an ond</li>
 * <li>(6) When writing refundble records to ReservationPaxOndCharge level corresponding fare Id or charge rate Id will
 * be removed and the refundable amount will be written as minus values</li>
 * </ol>
 * 
 * <p>
 * Happy Path: Will cancel the given ond and any ground segments linked if any.
 * </p>
 * <p>
 * Alternative Path:
 * <p>
 * <ol>
 * <li>(1) Throws an exception if the reservation is already cancelled</li>
 * <li>(2) Throws an exception if any one of the corresponding sent ond segments are cancelled</li>
 * </ol>
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cancelSegment"
 */
public class CancelSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CancelSegment.class);

	/** Construct the CancelSegment object */
	public CancelSegment() {
	}

	/**
	 * Execute method of the CancelSegment command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection<Integer> pnrSegmentIds = (Collection<Integer>) this.getParameter(CommandParamNames.PNR_SEGMENT_IDS);
		Boolean isCancelChgOperation = (Boolean) this.getParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION);
		Boolean isModifyChgOperation = (Boolean) this.getParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION);
		Boolean hasExternalCarrierPayments = (Boolean) this.getParameter(CommandParamNames.HAS_EXT_CARRIER_PAYMENTS);
		Boolean applyCancelCharge = (Boolean) this.getParameter(CommandParamNames.APPLY_CANCEL_CHARGE);
		Boolean applyModifyCharge = (Boolean) this.getParameter(CommandParamNames.APPLY_MODIFY_CHARGE);
		Boolean isPaxCancel = (Boolean) this.getParameter(CommandParamNames.IS_PAX_CANCEL);
		Boolean isCnxFlownSegs = (Boolean) this.getParameter(CommandParamNames.IS_CNX_FLOWN_SEGS);
		Boolean isRequote = getParameter(CommandParamNames.IS_REQUOTE, Boolean.FALSE, Boolean.class);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		CustomChargesTO customChargesTO = (CustomChargesTO) this.getParameter(CommandParamNames.CUSTOM_CHARGES);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Collection<Integer> orderdNewFlgSegIds = (Collection<Integer>) this
				.getParameter(CommandParamNames.ORDERED_NEW_FLIGHT_SEGMENT_IDS);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		FlownAssitUnit flownAssitUnit = getParameter(CommandParamNames.FLOWN_ASSIT_UNIT, FlownAssitUnit.class);
		Boolean updateFlownValidity = getParameter(CommandParamNames.UPDATE_FLOWN_VALIDITY, Boolean.FALSE, Boolean.class);
		Date ticketValidTillDate = getParameter(CommandParamNames.TICKET_VALID_TILL_DATE, Date.class);
		Collection<Integer> validityApplicableFltSegIds = getParameter(CommandParamNames.TICKET_VALID_FLT_SEG_IDS,
				Collection.class);
		Collection<Integer> bcNonChangedExgFltSegIds = this.getParameter(CommandParamNames.BC_NON_CHANGED_FLT_SEG_IDS,
				new HashSet<Integer>(), Collection.class);
		boolean doRevPurchase = getParameter(CommandParamNames.DO_REV_PURCHASE, Boolean.FALSE, Boolean.class);
		Boolean isVoidOperation = getParameter(CommandParamNames.IS_VOID_OPERATION, Boolean.FALSE, Boolean.class);
		boolean isFareAdjustmentValid = getParameter(CommandParamNames.FARE_ADJUSTMENT_VALID, Boolean.FALSE, Boolean.class);
		AgentCommissionDetails agentCommissionDetails = (AgentCommissionDetails) this
				.getParameter(CommandParamNames.AGENT_COMMISSION_DTO);
		Boolean removeAgentCommission = getParameter(CommandParamNames.REMOVE_AGENT_COMMISSION, Boolean.FALSE, Boolean.class);
		Set<Integer> commissionRemovalExcludedFareIds = new HashSet<Integer>();
		if (agentCommissionDetails != null) {
			commissionRemovalExcludedFareIds = agentCommissionDetails.getCommissionRemovalExcludedFareIds();
		}
		Collection<TempSegBcAlloc> blockIds = (Collection<TempSegBcAlloc>) this.getParameter(CommandParamNames.BLOCK_KEY_IDS);
		Collection<Integer> exchangedSegInvIds = getParameter(CommandParamNames.EXCHANGE_SEG_INV_IDS, Collection.class);
		Integer pnrPaymentTypes = this.getParameter(CommandParamNames.PNR_PAYMENT_TYPES, Integer.class);

		Map<Integer, IPayment> pnrPaxIdAndPayments = this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, Map.class);
		Collection<ReservationSegment> pnrSegments = this.getParameter(CommandParamNames.RESERVATION_SEGMENTS, Collection.class);
		ChargeTnxSeqGenerator chgTnxGen = this.getParameter(CommandParamNames.CHG_TRNX_GEN, ChargeTnxSeqGenerator.class);

		// Checking params
		this.validateParams(pnr, pnrSegmentIds, isPaxCancel, isCancelChgOperation, isModifyChgOperation, applyCancelCharge,
				applyModifyCharge, version, orderdNewFlgSegIds, customChargesTO, isCnxFlownSegs, credentialsDTO, isRequote);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);

		// Get the reservation
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
		// Get reservation's previous status
		String reservationStatusBeforeUpdate = reservation.getStatus();
		Collection<OndFareDTO> ondFareDTOs = (Collection<OndFareDTO>) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		Map<Integer, FlightCOSDTO> newfltCOSMap = null;
		if (ondFareDTOs != null && ondFareDTOs.size() > 0) {
			newfltCOSMap = new ChargeBO(false, credentialsDTO).isSameFlightCOSUpgrade(reservation, ondFareDTOs,
					orderdNewFlgSegIds);
		}

		ChargeAssitUnit chargeAssit = new ChargeAssitUnit(reservation, pnrSegmentIds, orderdNewFlgSegIds, isRequote,
				isModifyChgOperation, applyCancelCharge, applyModifyCharge, version, isVoidOperation, newfltCOSMap);
		// Collection<Collection<Integer>> colOndGroups = CancellationUtils.getOndWiseGroups(pnrSegmentIds,
		// reservation);
		// Collection<Integer> pnrSegIds = CancellationUtils.getFirstOndsPnrSegIds(colOndGroups);

		// Collect linked surface segments
		// Collection<Integer> linkedsurfaceSegments = null;
		// if (AppSysParamsUtil.isGroundServiceEnabled()) {
		// linkedsurfaceSegments = SurfaceSegmentUtil.getLinkedSurfaceSegments(reservation.getSegments(), pnrSegIds);
		// }
		if (chargeAssit.skipCancelSegmentOperations()) {
			doRevPurchase = true;
			ServiceResponce r = new DefaultServiceResponse(true);
			r.addResponceParam(CommandParamNames.DO_REV_PURCHASE, doRevPurchase);
			return r;
		} else if (isRequote) {
			doRevPurchase = false;
		}

		// Check cancel segment constraints
		this.checkCancelSegmentConstraints(reservation, chargeAssit.getCnxPnrSegIds(), isCnxFlownSegs, version);

		// boolean isVoidReservation = chargeAssit.isVoidReservation();
		//
		// boolean isSameFlightsModification = chargeAssit.isSameFlightModification();

		// Create the cancel segment business object
		CancelSegmentBO cancelSegmentBO = new CancelSegmentBO(isPaxCancel, customChargesTO, isCancelChgOperation,
				isModifyChgOperation, false, hasExternalCarrierPayments, credentialsDTO, bcNonChangedExgFltSegIds,
				removeAgentCommission, commissionRemovalExcludedFareIds, reservation.isInfantPaymentRecordedWithInfant());
		cancelSegmentBO.setTrackInfoDTO(credentialsDTO.getTrackInfoDTO());
		cancelSegmentBO.setUserPrincipal((UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL));

		// Capture inverse pnr segment ids
		// Map<Collection<Integer>, Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds =
		// ReservationApiUtils.getInverseSegments(
		// reservation, false);

		// Track Inverse Segment changes
		Map<Collection<Integer>, String> mapPnrSegIdsAndChanges = new HashMap<Collection<Integer>, String>();
		//
		// Collection<Integer> colSetInversePnrSegIds =
		// ReservationApiUtils.getInversePnrSegIds(mapSetPSegIdsAndSetInvPSegIds,
		// pnrSegIds);
		//
		// boolean isModifyProrate = CancellationUtils.isModifyProrate(pnrSegIds, colSetInversePnrSegIds,
		// orderdNewFlgSegIds,
		// reservation.getSegmentsView());
		//
		// boolean isOpenReturnInverseSegments = CancellationUtils.isOpenReturnInverseSegments(reservation,
		// colSetInversePnrSegIds);

		// Process credit for passengers
		// this.processCreditForPassengers(reservation, cancelSegmentBO, pnrSegIds, colSetInversePnrSegIds,
		// mapPnrSegIdsAndChanges,
		// linkedsurfaceSegments, isModifyProrate, isOpenReturnInverseSegments, isCancelChgOperation,
		// isModifyChgOperation,
		// applyCancelCharge, applyModifyCharge, isSameFlightsModification, isVoidReservation, isRequote);
		//
		if (chgTnxGen == null) {
			chgTnxGen = new ChargeTnxSeqGenerator(chargeAssit.getReservation());
		}
		this.processCreditForPassengers(cancelSegmentBO, chargeAssit, mapPnrSegIdsAndChanges, flownAssitUnit,
				isFareAdjustmentValid, chgTnxGen);

		// Move consume the exchanged booking classes in the requote flow, which are in the
		// same segment inventory
		if (blockIds != null) {
			Collection<TempSegBcAlloc> exchangedBlockSeats = new ArrayList<TempSegBcAlloc>();
			if (exchangedSegInvIds == null) {
				exchangedSegInvIds = new HashSet<Integer>();
			}
			boolean isOnhold = false;
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())
					&& (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)
							|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
									.equals(pnrPaymentTypes))) {
				isOnhold = true;
			}
			for (TempSegBcAlloc tmpBlkSeat : blockIds) {
				if ("Y".equals(tmpBlkSeat.getSkipSegInvUpdate())) {
					if (isOnhold) {
						tmpBlkSeat.setSkipSegInvUpdate("N");
						tmpBlkSeat.setBookingClassType(BookingClassType.OVERBOOK);
					}
					exchangedBlockSeats.add(tmpBlkSeat);
					exchangedSegInvIds.add(tmpBlkSeat.getFccaId());
				}
			}
			if (exchangedBlockSeats.size() > 0) {
				if (!isOnhold) {
					ReservationBO.moveBlockedSeatsToSold(exchangedBlockSeats);
				} else {
					ReservationBO.moveBlockSeats(exchangedBlockSeats, 0, 0);
				}
				blockIds.removeAll(exchangedBlockSeats);
			}

		}

		// Release Block Seats at a once for all passengers
		ReservationAudit reservationAudit = cancelSegmentBO.releaseInventory();

		// while modifying the out bound segment of an OpenRT booking, include inverse segments also for cancellation
		// along with out bound
		// if (isOpenReturnInverseSegments) {
		// pnrSegIds.addAll(colSetInversePnrSegIds);
		// }

		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> updatedFlexibilitiesMap = new HashMap<Integer, Collection<ReservationPaxOndFlexibilityDTO>>();
		boolean isFlexiApplied = false;
		FlexiRetainDTO flexiRetainDTO = new FlexiRetainDTO();
		if (pnrSegmentIds != null && pnrSegmentIds.size() > 0) {
			FlexiBL.reflectFlexiBalancesForCancelSegment(reservation, pnrSegmentIds, applyCancelCharge, applyModifyCharge,
					flexiRetainDTO);
			updatedFlexibilitiesMap = flexiRetainDTO.getPaxWiseUpdatedFlexiMap();
		}

		// add ground segments to the canceling segment list
		// if (linkedsurfaceSegments != null) {
		// pnrSegIds.addAll(linkedsurfaceSegments);
		// }
		Map<Integer, BigDecimal> handlingChargeByPaxId = null;
		// valid only for handling fee on cancel reservation
		if (isCancelChgOperation) {
			handlingChargeByPaxId = this.addHandlingFeeChargesForCancellation(reservation, pnrPaxIdAndPayments, credentialsDTO,
					chgTnxGen, chargeAssit.getCnxPnrSegIds());
		}
		

		// Cancels the reservation and segments
		this.cancelPnrAndSegments(chargeAssit, credentialsDTO);

		Collection<ReservationPax> eticketHavingPassengers = getEticketHavingPassengers(reservation);

		// Notify ADL With changes.- cancel segment
		if (!reservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			ADLNotifyer.updateConnectedSegmentsWhenCNXSegment(reservation, chargeAssit.getUserCancelledPnrSegIds());
			ADLNotifyer.recordCanceledSegmentsForAdl(reservation, chargeAssit.getUserCancelledPnrSegIds(),
					eticketHavingPassengers);
		}

		// Cancels ssrs of canceled segments
		Map<Integer, Collection<ReservationPaxSegmentSSR>> cancelledSSRMap = ReservationSSRUtil.cancelSegmentSSRs(reservation,
				chargeAssit.getAllNonFlownPnrSegIds());

		if (updateFlownValidity && flownAssitUnit != null) {
			FlownSegmentBO.updateTicketValidity(reservation, flownAssitUnit, ticketValidTillDate, null);
			updateFlownValidity = false;
		}

		// Saves the reservation
		ReservationProxy.saveReservation(reservation);
		if (isCancelChgOperation) {
			// records tnx record for handling fee on cancel segment only operation
			this.addTransactionRecords(handlingChargeByPaxId, credentialsDTO);
		}

		// send typeB message
		// Enable this if required
		// if (chargeAssit.hasUserCnxPnrSegIds()) {
		// TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
		// typeBRequestDTO.setReservation(reservation);
		// typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CANCEL_SEGMENT.getCode());
		// typeBRequestDTO.setCnxPnrSegIds(chargeAssit.getUserCancelledPnrSegIds());
		// typeBRequestDTO.setCnxInversePnrSegIds(chargeAssit.getCnxOpenReturnPnrSegIds());
		// TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
		// }

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
		TypeBRequestDTO typeBRequestDTO = null;
		CSTypeBRequestDTO csTypeBRequestDTO = null;
		if (csOCCarrirs != null) {
			// send Codeshare typeB message
			typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setCnxPnrSegIds(chargeAssit.getUserCancelledPnrSegIds());
			typeBRequestDTO.setCnxInversePnrSegIds(chargeAssit.getCnxOpenReturnPnrSegIds());
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.MODIFY_SEGMENT.getCode());

			// only when cnx is present in requote
			if ((isRequote && (pnrSegments == null || pnrSegments.size() == 0)) || !isRequote) {
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
				typeBRequestDTO = null;
			} else {
				csTypeBRequestDTO = new CSTypeBRequestDTO();
				csTypeBRequestDTO.setTypeBRequestDTO(typeBRequestDTO);
				csTypeBRequestDTO.setCsOCCarrirs(csOCCarrirs);
			}
		}

		// cancel e tickets
		ETicketBO.cancelETickets(reservation, chargeAssit.getUserExchangedPnrSegIds());

		if (!chargeAssit.skipCancelRevAccounting()) {
			cancelSegmentBO.propergateTheSavedChargeRevenueForAccounting(reservation);
		}

		Map<Integer, RevenueDTO> passengerRevenueMap = cancelSegmentBO.getMapPerPaxWiseRevenue();

		SSRBL.sendSSRCancelNotifications(reservation, cancelledSSRMap, credentialsDTO);

		Collection<Integer> userCancelledPnrSegs = chargeAssit.getUserCancelledPnrSegIds();
		if (!userCancelledPnrSegs.isEmpty()) {
			Collection<ReservationPaxSegAirportTransfer> userCancelledAptMap = ReservationApiUtils.cancelSegmentApts(reservation,
					chargeAssit.getAllNonFlownPnrSegIds(), credentialsDTO, chgTnxGen);
			AirportTransferBL.sendAirportTransferCancelNotifications(reservation, userCancelledPnrSegs, null, credentialsDTO,
					null);
		}

		// UPDATE Reservation Segment Seat and Meal Selection Status
		ReservationCoreUtils.updateSegmentsAncillaryStatus(reservation);

		// UPDATE Reservation Segment AUTO_CHECKIN Selection Status
		ReservationCoreUtils.updateSegmentsAutomaticCheckinStatus(reservation);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (reservationAudit == null) {
			reservationAudit = new ReservationAudit();
		}

		if (chargeAssit.hasUserCnxPnrSegIds()) {
			this.composeAudit(pnr, chargeAssit.getUserCancelledPnrSegIds(), pnrPaxIdAndPayments, reservation,
					chargeAssit.getCnxOpenReturnPnrSegIds(), passengerRevenueMap, mapPnrSegIdsAndChanges, credentialsDTO,
					updatedFlexibilitiesMap, isFlexiApplied, userNotes, reservationAudit, userNoteType);
		}

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		if (customChargesTO.getCustomAdultCCharge() != null || customChargesTO.getCustomChildCCharge() != null) {
			ReservationAuditCreator rAuditCreator = new ReservationAuditCreator();
			ReservationAudit reservationAuditCNXMODChargeChange = rAuditCreator.createCNXMODChargeOverideAudit(pnr,
					customChargesTO, credentialsDTO);
			colReservationAudit.add(reservationAuditCNXMODChargeChange);

		}

		if (pnrSegmentIds.size() > 0) {
			Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationSegmentDAO
					.getSegmentInformation(pnrSegmentIds);
			AuditAirinventory.doSeatRelease(colReservationSegmentDTO, credentialsDTO.getUserId(), reservation);
		}
		if (isRequote) {
			Collection<ReservationAudit> colRequoteReservationAudit = new ArrayList<ReservationAudit>();
			if (this.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION) != null) {
				colRequoteReservationAudit = (Collection<ReservationAudit>) this
						.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION);
			}
			colRequoteReservationAudit.addAll(colReservationAudit);
			response.addResponceParam(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION, colRequoteReservationAudit);
		}

		if (output == null) {
			output = new DefaultServiceResponse();
			output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		} else {
			output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		}

		response.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.PASSENGER_REVENUE_MAP, passengerRevenueMap);
		response.addResponceParam(CommandParamNames.CANCEL_SEGMENT_INFORMATION,
				reservationAudit.getContentMap().get(AuditTemplateEnum.TemplateParams.CanceledSegment.SEGMENT_INFORMATION));
		response.addResponceParam(CommandParamNames.VACATED_SEATS,
				reservationAudit.getContentMap().get(AuditTemplateEnum.TemplateParams.CanceledSegment.VACATED_SEATS));
		response.addResponceParam(CommandParamNames.VACATED_MEALS,
				reservationAudit.getContentMap().get(AuditTemplateEnum.TemplateParams.CanceledSegment.VACATED_MEALS));
		response.addResponceParam(CommandParamNames.FARE_CHANGES,
				reservationAudit.getContentMap().get(AuditTemplateEnum.TemplateParams.CanceledSegment.FARE_CHANGES));
		response.addResponceParam(CommandParamNames.ORIGINAL_SEGMENT_TOTAL,
				reservationAudit.getContentMap().get(AuditTemplateEnum.TemplateParams.CanceledSegment.ORIGINAL_SEGMENT_TOTAL));
		response.addResponceParam(CommandParamNames.CANCELLED_PAX_SEGMENT_SSR_MAP, cancelledSSRMap);
		response.addResponceParam(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP, updatedFlexibilitiesMap);
		response.addResponceParam(CommandParamNames.FLEXI_RETAIN_DTO, flexiRetainDTO);
		response.addResponceParam(CommandParamNames.UPDATED_FLEXIBILITIES_INFORMATION,
				ReservationApiUtils.getFlexibilityInformation(updatedFlexibilitiesMap));
		response.addResponceParam(CommandParamNames.UPDATE_FLOWN_VALIDITY, updateFlownValidity);
		response.addResponceParam(CommandParamNames.DO_REV_PURCHASE, doRevPurchase);
		response.addResponceParam(CommandParamNames.BLOCK_KEY_IDS, blockIds);
		response.addResponceParam(CommandParamNames.EXCHANGE_SEG_INV_IDS, exchangedSegInvIds);
		response.addResponceParam(CommandParamNames.CS_TYPE_B_REQUEST, csTypeBRequestDTO);
		response.addResponceParam(CommandParamNames.ETICKET_HAVING_PASSENGERS, eticketHavingPassengers);
		response.addResponceParam(CommandParamNames.CHG_TRNX_GEN, chgTnxGen);
		response.addResponceParam(CommandParamNames.RESERVATION_STATUS_BEFORE_UPDATE, reservationStatusBeforeUpdate);

		if (AppSysParamsUtil.isGroundServiceEnabled()) {
			Collection<Integer> linkedsurfaceSegments = chargeAssit.getCnxLinkedSurfacePnrSegIds();
			response.addResponceParam(CommandParamNames.CANCELLED_SURFACE_SEGMENT_LIST, linkedsurfaceSegments);
		}

		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		if (this.getParameter(CommandParamNames.SELECTED_ANCI_MAP) != null) {
			response.addResponceParam(CommandParamNames.SELECTED_ANCI_MAP,
					this.getParameter(CommandParamNames.SELECTED_ANCI_MAP));
		}

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Compose the audit
	 * 
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param pnrPaxIdAndPayments
	 * @param reservation
	 * @param colSetInversePnrSegIds
	 * @param passengerRevenueMap
	 * @param mapPnrSegIdsAndChanges
	 * @param credentialsDTO
	 * @param reservationAudit
	 * @param userNoteType TODO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private ReservationAudit composeAudit(String pnr, Collection<Integer> pnrSegmentIds, Map pnrPaxIdAndPayments,
			Reservation reservation, Collection<Integer> colSetInversePnrSegIds, Map passengerRevenueMap,
			Map mapPnrSegIdsAndChanges, CredentialsDTO credentialsDTO, Map updatedFlexibilitiesMap, boolean isFlexiApplied,
			String userNotes, ReservationAudit reservationAudit, String userNoteType) throws ModuleException {
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CANCELED_SEGMENT.getCode());
		reservationAudit.setUserNote(userNotes);
		reservationAudit.setUserNoteType(userNoteType);

		StringBuffer segmentInfomation = new StringBuffer(ReservationCoreUtils.getSegmentInformation(pnrSegmentIds));

		Collection<Integer> colSetPnrSegIds = new HashSet<Integer>();
		colSetPnrSegIds.addAll(pnrSegmentIds);
		// If any inverse segment exist
		if (colSetInversePnrSegIds != null && !colSetInversePnrSegIds.isEmpty()) {
			segmentInfomation.append(mapPnrSegIdsAndChanges.get(colSetPnrSegIds));
			StringBuffer fareChanges = new StringBuffer(ReservationCoreUtils.getSegmentInformation(colSetInversePnrSegIds));
			fareChanges.append(mapPnrSegIdsAndChanges.get(colSetInversePnrSegIds));
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.FARE_CHANGES, fareChanges.toString());
		} else if (!mapPnrSegIdsAndChanges.isEmpty() && pnrSegmentIds.size() > 0) {
			StringBuffer fareChanges = new StringBuffer();
			fareChanges.append(mapPnrSegIdsAndChanges.get(colSetPnrSegIds));
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.FARE_CHANGES, fareChanges.toString());
		}

		String audit = ReservationCoreUtils.getPassengerPaymentInfo(pnrPaxIdAndPayments, reservation);

		// Setting the segment information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.SEGMENT_INFORMATION,
				segmentInfomation.toString());

		// Return the segment total before cancellation
		BigDecimal totalCharges = this.getSegmentTotalBeforeCancellation(passengerRevenueMap);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.ORIGINAL_SEGMENT_TOTAL,
				totalCharges.toString() + audit);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Flexibilities.FLEXIBILITIES_INFO,
				(isFlexiApplied) ? "Utilized Flexibility" : "");

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier codecomposeAudit
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}
		if (userNotes != null && !userNotes.isEmpty()) {
			reservationAudit.setUserNote(userNotes);
		}

		return reservationAudit;
	}

	/**
	 * Returns segment total before cancellation
	 * 
	 * @param passengerRevenueMap
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private BigDecimal getSegmentTotalBeforeCancellation(Map passengerRevenueMap) {
		Iterator itRevenueDTO = passengerRevenueMap.values().iterator();
		RevenueDTO revenueDTO;
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		while (itRevenueDTO.hasNext()) {
			revenueDTO = (RevenueDTO) itRevenueDTO.next();
			totalAmount = AccelAeroCalculator.add(totalAmount, revenueDTO.getTotalChgBeforeCancellation());
		}

		return totalAmount;
	}

	/**
	 * Check cancel segment constraints
	 * 
	 * @param reservation
	 * @param pnrSegmentIds
	 * @param isCnxFlownSegs
	 * @param version
	 * @throws ModuleException
	 */
	private void checkCancelSegmentConstraints(Reservation reservation, Collection<Integer> pnrSegmentIds, boolean isCnxFlownSegs,
			Long version) throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		// Checking if the reservation is already cancelled
		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			throw new ModuleException("airreservations.arg.invalidPnrCancel");
		}

		Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
		ReservationSegmentDTO reservationSegmentDTO;
		Date currentDate = new Date();

		// Checking cancelling a already cancelled segment
		while (itReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itReservationSegmentDTO.next();

			if (pnrSegmentIds.contains(reservationSegmentDTO.getPnrSegId())) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {
					throw new ModuleException("airreservations.arg.invalidSegCancel");
				} else if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
						.equals(reservationSegmentDTO.getStatus())) {
					if (!isCnxFlownSegs && currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) > 0) {
						throw new ModuleException("airreservations.arg.flownSegmentsCannotCancel");
					}
				}
			}
		}

		// Checking any restricted fare exist or not
		ValidationUtils.checkRestrictedSegmentConstraints(reservation, pnrSegmentIds,
				"airreservations.cancellation.restrictedFareSegment");
	}

	private void processCreditForPassengers(CancelSegmentBO cancelSegmentBO, ChargeAssitUnit chargeAU,
			Map<Collection<Integer>, String> mapPnrSegIdsAndChanges, FlownAssitUnit flownAsstUnit, boolean isFareAdjustmentValid,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		List<ReservationPaxFare> reservationPaxFareTargets = null;
		List<ReservationPaxFare> reservationPaxFareEffected;
		List<ReservationPaxFare> reservationPaxFareOpenRTs = null;
		List<ReservationPaxFare> reservationPaxFareGroundSegments = null;
		List<ReservationPaxFare> reservationPaxFareExchanged = null;

		boolean overideCustomCharges = false;
		// For each passenger
		for (ReservationPax reservationPax : chargeAU.getReservation().getPassengers()) {

			reservationPaxFareTargets = chargeAU.getTargetPaxFare(reservationPax.getPnrPaxFares());
			reservationPaxFareEffected = chargeAU.getAffectedPaxFare(reservationPax.getPnrPaxFares());
			chargeAU.adjustOndFares(reservationPaxFareTargets, reservationPaxFareEffected, mapPnrSegIdsAndChanges);
		}

		// For each passenger
		for (ReservationPax reservationPax : chargeAU.getReservation().getPassengers()) {

			// For Parent or Adult
			if (!ReservationApiUtils.isInfantType(reservationPax) || cancelSegmentBO.isInfantPaymentSeparated()) {

				reservationPaxFareTargets = chargeAU.getTargetPaxFare(reservationPax.getPnrPaxFares());
				reservationPaxFareGroundSegments = chargeAU.getLinkedGroundFareSegments(reservationPax.getPnrPaxFares());
				reservationPaxFareOpenRTs = chargeAU.getAffectedOpenRTPaxFare(reservationPax.getPnrPaxFares());
				reservationPaxFareEffected = chargeAU.getAffectedPaxFare(reservationPax.getPnrPaxFares());
				reservationPaxFareExchanged = chargeAU.getExchangedPaxFare(reservationPax.getPnrPaxFares());
				// chargeAU.adjustOndFares(reservationPaxFareTargets, reservationPaxFareEffected,
				// mapPnrSegIdsAndChanges);

				if (!overideCustomCharges && reservationPaxFareTargets != null && reservationPaxFareTargets.size() > 0
						&& cancelSegmentBO != null) {
					FeeBO.calculateCustomChargesONDWise(cancelSegmentBO.getCustomChargesTO(), reservationPaxFareTargets.size());
					overideCustomCharges = true;
				}

				addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareTargets, chargeAU.isApplyCnxCharge(),
						chargeAU.isApplyModCharge(), chargeAU.isSameFlightModification(), chargeAU.isVoidReservation(),
						flownAsstUnit, false, isFareAdjustmentValid, true, true, chgTnxGen);
				addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareOpenRTs, false, false,
						chargeAU.isSameFlightModification(), chargeAU.isVoidReservation(), flownAsstUnit, false,
						isFareAdjustmentValid, false, false, chgTnxGen);
				addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareGroundSegments,
						chargeAU.isApplyCnxCharge(), chargeAU.isApplyModCharge(), chargeAU.isSameFlightModification(),
						chargeAU.isVoidReservation(), flownAsstUnit, false, isFareAdjustmentValid, false, false, chgTnxGen);
				addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareExchanged, false, false,
						chargeAU.isSameFlightModification(), chargeAU.isVoidReservation(), flownAsstUnit, true,
						isFareAdjustmentValid, false, false, chgTnxGen);

			}
		}

		// Set Reservation and Passenger level total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(chargeAU.getReservation());
	}

	/**
	 * Process Credit
	 * 
	 * @param reservation
	 * @param cancelSegmentBO
	 * @param pnrSegmentIds
	 * @param colSetInversePnrSegIds
	 * @param mapPnrSegIdsAndChanges
	 * @param pnrGroundSegmentIds
	 * @param isModifyProrate
	 * @param isOpenReturnInverseSegments
	 * @param isCancelChgOperation
	 * @param isModifyChgOperation
	 * @param applyCancelCharge
	 * @param applyModifyCharge
	 * @param isSameFlightsModification
	 * @param isVoidReservation
	 * @param isRequote
	 * @throws ModuleException
	 */
	// @SuppressWarnings({ "rawtypes", "unchecked" })
	// private void processCreditForPassengers(Reservation reservation, CancelSegmentBO cancelSegmentBO, Collection
	// pnrSegmentIds,
	// Collection colSetInversePnrSegIds, Map mapPnrSegIdsAndChanges, Collection pnrGroundSegmentIds,
	// boolean isModifyProrate, boolean isOpenReturnInverseSegments, boolean isCancelChgOperation,
	// boolean isModifyChgOperation, boolean applyCancelCharge, boolean applyModifyCharge,
	// boolean isSameFlightsModification, boolean isVoidReservation, boolean isRequote) throws ModuleException {
	//
	// Iterator itrReservationPax = reservation.getPassengers().iterator();
	// ReservationPax reservationPax;
	// ReservationPaxFare reservationPaxFareTarget = null;
	// ReservationPaxFare reservationPaxFareEffected;
	// ReservationPaxFare reservationPaxFareOpenRT = null;
	// ReservationPaxFare reservationPaxFareGroundSegments = null;
	//
	// // For each passenger
	// while (itrReservationPax.hasNext()) {
	// reservationPax = (ReservationPax) itrReservationPax.next();
	//
	// // For Parent or Adult
	// if (!ReservationApiUtils.isInfantType(reservationPax)) {
	// if (colSetInversePnrSegIds.size() == 0) {
	// // Get the corresponding Target Reservation Pax Fare
	// reservationPaxFareTarget = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
	// pnrSegmentIds);
	// if (pnrGroundSegmentIds != null && pnrGroundSegmentIds.size() > 0) {
	// reservationPaxFareGroundSegments = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
	// pnrGroundSegmentIds);
	// }
	// } else if (colSetInversePnrSegIds.size() > 0 && isOpenReturnInverseSegments) {
	// reservationPaxFareTarget = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
	// pnrSegmentIds);
	// reservationPaxFareOpenRT = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
	// colSetInversePnrSegIds);
	// }
	// // This means effected reservation pax fare exist
	// else if (colSetInversePnrSegIds.size() > 0) {
	//
	// reservationPaxFareTarget = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
	// pnrSegmentIds);
	// reservationPaxFareEffected = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
	// colSetInversePnrSegIds);
	//
	// if (isModifyProrate &&
	// !reservationPaxFareTarget.getFareId().equals(reservationPaxFareEffected.getFareId())) {
	// // exclude half return from adjusting fare if modifyprorate
	// } else {
	// // Adjust fare among ond(s)
	// CancellationUtils.adjustFareAmongOnds(reservationPaxFareTarget, reservationPaxFareEffected,
	// mapPnrSegIdsAndChanges, pnrSegmentIds, colSetInversePnrSegIds, isModifyProrate);
	// }
	// if (pnrGroundSegmentIds != null && pnrGroundSegmentIds.size() > 0) {
	// reservationPaxFareGroundSegments = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
	// pnrGroundSegmentIds);
	// }
	// }
	//
	// addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareTarget, applyCancelCharge,
	// applyModifyCharge, isSameFlightsModification, isVoidReservation);
	// addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareOpenRT, false, false,
	// isSameFlightsModification, isVoidReservation);
	// addPassengerRevenues(reservationPax, cancelSegmentBO, reservationPaxFareGroundSegments,
	// applyCancelCharge,
	// applyModifyCharge, isSameFlightsModification, isVoidReservation);
	//
	// }
	// }
	//
	// // Set Reservation and Passenger level total amounts
	// ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
	// }
	/**
	 * Add Passenger Revenues
	 * 
	 * @param reservationPax
	 * @param cancelSegmentBO
	 * @param showCnxCharge
	 * @param showModCharge
	 * @param isSameFlightsModification
	 * @param isVoidReservation
	 * @param isExchanged
	 * @param isApplyCustomCharge
	 * @param chgTnxGen
	 * @param reservationPaxFare
	 * @throws ModuleException
	 */
	private void addPassengerRevenues(ReservationPax reservationPax, CancelSegmentBO cancelSegmentBO,
			List<ReservationPaxFare> reservationPaxFares, boolean applyCnxCharge, boolean applyModCharge,
			boolean isSameFlightsModification, boolean isVoidReservation, FlownAssitUnit flownAsstUnit, boolean isExchanged,
			boolean isFareAdjustmentValid, boolean isTargetFare, boolean isApplyCustomCharge, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		if (reservationPaxFares != null) {
			int count = 0;
			for (ReservationPaxFare reservationPaxFare : reservationPaxFares) {
				cancelSegmentBO.processPaxCreditForOnd(reservationPax, reservationPaxFare, applyCnxCharge, applyModCharge,
						isSameFlightsModification, isVoidReservation, flownAsstUnit, isExchanged, isFareAdjustmentValid,
						isTargetFare, count, isApplyCustomCharge, chgTnxGen);
				count++;
			}
		}
	}

	/**
	 * Cancels the reservation and segments
	 * 
	 * @param chargeAU
	 * @param credentialsDTO
	 */
	private void cancelPnrAndSegments(ChargeAssitUnit chargeAU, CredentialsDTO credentialsDTO) throws ModuleException {
		Iterator<ReservationSegment> itReservationSegment = chargeAU.getReservation().getSegments().iterator();
		ReservationSegment reservationSegment;

		Collection<Integer> allPnrSegsToBeCnx = chargeAU.getAllPnrSegmentsToBeMarkedCancelled();
		Collection<Integer> exchangedPnrSegIds = chargeAU.getUserExchangedPnrSegIds();

		while (itReservationSegment.hasNext()) {
			reservationSegment = itReservationSegment.next();

			if (allPnrSegsToBeCnx.contains(reservationSegment.getPnrSegId())) {
				reservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
				reservationSegment.setBundledFarePeriodId(reservationSegment.getBundledFarePeriodId());
				if (exchangedPnrSegIds.contains(reservationSegment.getPnrSegId())) {
					reservationSegment.setSubStatus(ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED);
				}
				if (AppSysParamsUtil.isAutoCancellationEnabled() && reservationSegment.getAutoCancellationId() != null) {
					reservationSegment.setAutoCancellationId(null);
				}
				ReservationApiUtils.captureSegmentStatusChgInfo(reservationSegment, credentialsDTO);
			}

		}

		// if (isCancelChgOperation.booleanValue() == true && isModifyChgOperation.booleanValue() == false
		// && i == reservation.getSegments().size()) {

		if (chargeAU.isReservationCancelled()) {
			if (chargeAU.isVoidReservation()) {
				// only reservations are to be marked as void.
				chargeAU.getReservation().setIsVoidReservation(ReservationInternalConstants.VoidReservation.YES);
			} else {
				chargeAU.getReservation().setIsVoidReservation(ReservationInternalConstants.VoidReservation.NO);
			}

			// Cancells the reservation passengers
			Iterator<ReservationPax> itReservationPax = chargeAU.getReservation().getPassengers().iterator();
			ReservationPax reservationPax;
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CANCEL);
			}

			// Cancells the reservation
			chargeAU.getReservation().setStatus(ReservationInternalConstants.ReservationStatus.CANCEL);
		}
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param requestedPnrSegmentIds
	 * @param isPaxCancel
	 * @param isCancelChgOperation
	 * @param isModifyChgOperation
	 * @param applyModifyCharge
	 * @param applyCancelCharge
	 * @param version
	 * @param orderdNewFlgSegIds
	 * @param customChargesTO
	 * @param isCnxFlownSegs
	 * @param credentialsDTO
	 * @param isRequote
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(String pnr, Collection requestedPnrSegmentIds, Boolean isPaxCancel, Boolean isCancelChgOperation,
			Boolean isModifyChgOperation, Boolean applyCancelCharge, Boolean applyModifyCharge, Long version,
			Collection orderdNewFlgSegIds, CustomChargesTO customChargesTO, Boolean isCnxFlownSegs, CredentialsDTO credentialsDTO,
			Boolean isRequote) throws ModuleException {
		log.debug("Inside validateParams");

		if (isRequote) {
			if (pnr == null || isPaxCancel == null || isCancelChgOperation == null || isModifyChgOperation == null
					|| applyCancelCharge == null || applyModifyCharge == null || version == null || customChargesTO == null
					|| isCnxFlownSegs == null || credentialsDTO == null) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}
		} else if (pnr == null || requestedPnrSegmentIds == null || requestedPnrSegmentIds.size() == 0 || isPaxCancel == null
				|| isCancelChgOperation == null || isModifyChgOperation == null || applyCancelCharge == null
				|| applyModifyCharge == null || version == null || customChargesTO == null || isCnxFlownSegs == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (isModifyChgOperation.booleanValue() == true) {
			if (orderdNewFlgSegIds == null || orderdNewFlgSegIds.size() == 0) {
				throw new ModuleException("airreservations.cancellation.noNewFlgSegIds");
			}
		}

		log.debug("Exit validateParams");
	}

	private Collection<ReservationPax> getEticketHavingPassengers(Reservation reservation) throws ModuleException {
		Collection<ReservationPax> eTicketHavingPassengers = new ArrayList<ReservationPax>();
		Collection<EticketTO> existingEticket = ReservationDAOUtils.DAOInstance.ETicketDAO
				.getReservationETickets(reservation.getPnr());
		for (ReservationPax resPax : reservation.getPassengers()) {
			for (EticketTO eticket : existingEticket) {
				if (resPax.getPnrPaxId().intValue() == eticket.getPnrPaxId().intValue()
						&& !eTicketHavingPassengers.contains(resPax)) {
					eTicketHavingPassengers.add(resPax);
				}
			}
		}
		return eTicketHavingPassengers;
	}
	
	private Map<Integer, BigDecimal> addHandlingFeeChargesForCancellation(Reservation reservation,
			Map<Integer, IPayment> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen, Collection<Integer> allPnrSegsToBeCnx)
			throws ModuleException {
		Map<Integer, BigDecimal> handlingChargeByPaxId = new HashMap<>();
		if (pnrPaxIdAndPayments != null && !pnrPaxIdAndPayments.isEmpty()) {
			PaymentAssembler paymentAssembler;
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges = new HashMap<>();

			for (ReservationPax pax : reservation.getPassengers()) {
				Integer pnrPaxId = pax.getPnrPaxId();
				paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(pnrPaxId);
				if (paymentAssembler != null) {
					Collection<ExternalChgDTO> filteredExtChgs = paymentAssembler
							.getPerPaxExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
					if (filteredExtChgs != null && !filteredExtChgs.isEmpty()) {
						List<ExternalChgDTO> list = new ArrayList<>(filteredExtChgs);
						paxExternalCharges.put(pax.getPaxSequence(), list);
						BigDecimal totalChargeAmount = getTotalExternalCharges(list);
						handlingChargeByPaxId.put(pnrPaxId, totalChargeAmount);
						paymentAssembler.removePerPaxExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE);
					}
				}

			}
			if (paxExternalCharges != null && !paxExternalCharges.isEmpty()) {
				ExternalGenericChargesBL.reflectExternalChgForSegmentCancellation(reservation, paxExternalCharges, credentialsDTO,
						chgTnxGen, allPnrSegsToBeCnx);
			}
		}
		return handlingChargeByPaxId;
	}
	
	private BigDecimal getTotalExternalCharges(Collection<ExternalChgDTO> ccExternalChgs) {
		BigDecimal totalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (ccExternalChgs != null && !ccExternalChgs.isEmpty()) {
			for (ExternalChgDTO chg : ccExternalChgs) {
				totalChargeAmount = AccelAeroCalculator.add(totalChargeAmount, chg.getAmount());
			}
		}
		return totalChargeAmount;
	}
	
	private void addTransactionRecords(Map<Integer, BigDecimal> handlingChargeByPaxId, CredentialsDTO credentialsDTO)
			throws ModuleException {

		if (handlingChargeByPaxId != null && !handlingChargeByPaxId.isEmpty()) {
			for (Integer pnrPaxId : handlingChargeByPaxId.keySet()) {
				BigDecimal totalChargeAmount = handlingChargeByPaxId.get(pnrPaxId);
				if (totalChargeAmount.doubleValue() > 0) {

					ReservationModuleUtils.getRevenueAccountBD().recordHandlingFeeOnCancellation(pnrPaxId.toString(),
							totalChargeAmount, credentialsDTO);

				}
			}
		}

	}
	
	private boolean validateCancelOperationOnlyFlownSegmentRemains(Boolean isCancelChgOperation, Boolean isRequote,
			Collection<OndFareDTO> ondFareDTOs, Collection<ReservationSegment> pnrSegments) {
		if (isCancelChgOperation && isRequote
				&& (ondFareDTOs == null || ondFareDTOs.size() == 0 || pnrSegments == null || pnrSegments.size() == 0)) {
			return true;
		}
		return false;
	}	
}