package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.CnxModPenServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.segment.FlownAssitUnit;
import com.isa.thinair.airreservation.core.bl.segment.ModifyAssitUnit;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class ONDFareChargeProcessor {

	private Date quotedDate;
	private Collection<OndFareDTO> ondFares;

	private Map<String, Collection<ChargeMetaTO>> paxTypeFareMetaCharges = new HashMap<String, Collection<ChargeMetaTO>>();
	private Map<String, BigDecimal> paxTypePenaltyMetaCharges = new HashMap<String, BigDecimal>();
	private Map<String, List<CnxModPenServiceTaxDTO>> paxTypeServiceTaxPenaltyMetaCharges = new HashMap<String, List<CnxModPenServiceTaxDTO>>();

	private boolean processCharges = false;
	private FlownAssitUnit flownAssitUnit;
	private CredentialsDTO credentialsDTO;
	private Reservation reservation;

	public ONDFareChargeProcessor(Collection<OndFareDTO> ondFares, Date quotedDate, CredentialsDTO credentialsDTO) {
		this.ondFares = ondFares;
		this.quotedDate = quotedDate;
		this.credentialsDTO = credentialsDTO;
	}

	public ONDFareChargeProcessor(Collection<OndFareDTO> ondFares, Date quotedDate, FlownAssitUnit flownAssitUnit, CredentialsDTO  credentialsDTO, Reservation reservation) {
		this(ondFares, quotedDate, null);
		this.flownAssitUnit = flownAssitUnit;
		this.credentialsDTO = credentialsDTO;
		this.reservation = reservation;
	}

	private void addCharge(String paxType, ChargeMetaTO metaTO) {
		if (!paxTypeFareMetaCharges.containsKey(paxType)) {
			paxTypeFareMetaCharges.put(paxType, new ArrayList<ChargeMetaTO>());
		}
		paxTypeFareMetaCharges.get(paxType).add(metaTO);
	}

	private void addMetaCharges(String paxType, String chargeGroupCode, BigDecimal chargeAmount, BigDecimal discountAmount,
			BigDecimal adjustmentAmount) {

		if (((ReservationInternalConstants.ChargeGroup.ADJ.equals(chargeGroupCode)) && chargeAmount.doubleValue() < 0)
				|| chargeAmount.doubleValue() > 0) {
			ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(chargeGroupCode, chargeAmount, quotedDate, discountAmount,
					adjustmentAmount);
			addCharge(paxType, metaTO);
		}

	}

	private void addMetaCharge(String paxType, String chargeGroupCode, BigDecimal chargeAmount, BigDecimal discountAmount,
			BigDecimal adjustmentAmount, String chargeCode, Integer chargeRateId) {

		if (((ReservationInternalConstants.ChargeGroup.ADJ.equals(chargeGroupCode)) && chargeAmount.doubleValue() < 0)
				|| chargeAmount.doubleValue() > 0) {
			ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(chargeGroupCode, chargeAmount, quotedDate, discountAmount,
					adjustmentAmount, chargeCode, chargeRateId);
			addCharge(paxType, metaTO);
		}

	}

	private void addFareCharges(String paxType, BigDecimal chargeAmount, BigDecimal discountAmount, BigDecimal adjustmentAmount) {
		addMetaCharges(paxType, ReservationInternalConstants.ChargeGroup.FAR, chargeAmount, discountAmount, adjustmentAmount);
	}

	private void addPenalty(String paxType, BigDecimal chargeAmount) {
		if (chargeAmount.compareTo(BigDecimal.ZERO) > 0) {
			if (!paxTypePenaltyMetaCharges.containsKey(paxType)) {
				paxTypePenaltyMetaCharges.put(paxType, AccelAeroCalculator.getDefaultBigDecimalZero());
			}
			paxTypePenaltyMetaCharges.put(paxType, AccelAeroCalculator.add(paxTypePenaltyMetaCharges.get(paxType), chargeAmount));

		}
	}
	
	private BigDecimal calculateServiceTaxPenalty(String paxType, BigDecimal penaltyAmount) {
		
		Collection<String> nonTicketingRevenueChargeGroups = AppSysParamsUtil.getChargeGroupsToQuoteGSTOnNonTicketingRevenue();
		
		if((nonTicketingRevenueChargeGroups != null
				&& nonTicketingRevenueChargeGroups.contains(ReservationInternalConstants.ChargeGroup.PEN))){
			ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForNonTktDTO();
			Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxWiseCharges = new HashMap<>();
			PaxCnxModPenChargeForServiceTaxDTO paxCnxModPenChargeForServiceTaxDTO = new PaxCnxModPenChargeForServiceTaxDTO();
			ServiceTaxQuoteForNonTicketingRevenueRS rs = null;
			boolean trimPenalty = false;
			if (AccelAeroCalculator.isGreaterThan(penaltyAmount, AccelAeroCalculator.getDefaultBigDecimalZero())
					&& (credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_AGENT) || credentialsDTO
							.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_LCC))) {
				LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
				chgDTO.setCode(ReservationInternalConstants.ChargeGroup.PEN);
				chgDTO.setAmount(penaltyAmount);

				List<LCCClientExternalChgDTO> chgs = new ArrayList<>();
				chgs.add(chgDTO);

				paxCnxModPenChargeForServiceTaxDTO.setTotalAmountIncludingServiceTax(trimPenalty);
				paxCnxModPenChargeForServiceTaxDTO.setCharges(chgs);
				paxWiseCharges.put(1, paxCnxModPenChargeForServiceTaxDTO);

				UserPrincipal userPrincipal = new UserPrincipal();
				userPrincipal = (UserPrincipal) userPrincipal.createIdentity(null, credentialsDTO.getSalesChannelCode(),
						credentialsDTO.getAgentCode(), credentialsDTO.getAgentStation(), null, credentialsDTO.getUserId(),
						credentialsDTO.getColUserDST(), credentialsDTO.getPassword(), null, null, null, null,
						credentialsDTO.getAgentCurrencyCode(), null, null, null, null);

				serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(null);
				serviceTaxQuoteCriteriaDTO.setPaxCountryCode(reservation.getContactInfo().getCountryCode());
				serviceTaxQuoteCriteriaDTO.setPaxState(reservation.getContactInfo().getState());
				serviceTaxQuoteCriteriaDTO.setPaxWiseCharges(paxWiseCharges);
				serviceTaxQuoteCriteriaDTO.setPrefSystem(ProxyConstants.SYSTEM.AA);

				try {
					rs = ReservationModuleUtils.getAirproxyReservationBD().quoteServiceTaxForNonTicketingRevenue(
							serviceTaxQuoteCriteriaDTO, credentialsDTO.getTrackInfoDTO(), userPrincipal);
				} catch (ModuleException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (rs != null
						&& rs.getPaxWiseCnxModPenServiceTaxes() != null
						&& rs.getPaxWiseCnxModPenServiceTaxes().get(1) != null
						&& AccelAeroCalculator.isGreaterThan(rs.getPaxWiseCnxModPenServiceTaxes().get(1)
								.getEffectiveCnxModPenChargeAmount(), AccelAeroCalculator.getDefaultBigDecimalZero())) {
					if(trimPenalty){
						penaltyAmount = rs.getPaxWiseCnxModPenServiceTaxes().get(1).getEffectiveCnxModPenChargeAmount();
					}
					for (ServiceTaxDTO serviceTaxDTO : rs.getPaxWiseCnxModPenServiceTaxes().get(1).getCnxModPenServiceTaxes()) {
						ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(serviceTaxDTO.getChargeGroupCode(),
								serviceTaxDTO.getAmount(), quotedDate, AccelAeroCalculator.getDefaultBigDecimalZero(),
								AccelAeroCalculator.getDefaultBigDecimalZero(), serviceTaxDTO.getChargeCode(), null);
						addCharge(paxType, metaTO);
					}
				}
			}
		}else{
			
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteRS = null;
			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForTktDTO();
			Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			Map<Integer, String> paxWisePaxTypes = new HashMap<Integer, String>();
			
			if (AccelAeroCalculator.isGreaterThan(penaltyAmount, AccelAeroCalculator.getDefaultBigDecimalZero())
					&& (credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_AGENT) || credentialsDTO
							.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_LCC))) {

				Collection<FlightSegmentDTO> flightSegments = new ArrayList<FlightSegmentDTO>();
				for(OndFareDTO ondFare : ondFares){
					flightSegments.addAll(ondFare.getSegmentsMap().values());
				}
				
				LCCClientExternalChgDTO externalChargeDTO = new LCCClientExternalChgDTO();
					
				externalChargeDTO.setAmount(penaltyAmount);
				externalChargeDTO.setCarrierCode(flightSegments.iterator().next().getCarrierCode());
				externalChargeDTO.setExternalCharges(ReservationInternalConstants.EXTERNAL_CHARGES.ANCI_PENALTY); // TODO change to penalty
				
				externalChargeDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegments.iterator().next()));
				externalChargeDTO.setSegmentCode(flightSegments.iterator().next().getSegmentCode());
				externalChargeDTO.setSegmentSequence(1);
				
				if(paxWiseExternalCharges.get(1) == null){
					paxWiseExternalCharges.put(1, new ArrayList<LCCClientExternalChgDTO>());
					paxWiseExternalCharges.get(1).add(externalChargeDTO);
				}else{
					paxWiseExternalCharges.get(1).add(externalChargeDTO);
				}
				
				paxWisePaxTypes.put(1, paxType);
				
				BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
				baseAvailRQ.getAvailPreferences().setAppIndicator(ApplicationEngine.XBE);
				if(reservation.getOriginatorPnr() != null && !reservation.getOriginatorPnr().isEmpty()){
					baseAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.INT);
				}else{
					baseAvailRQ.getAvailPreferences().setSearchSystem(SYSTEM.AA);
				}
				
				List<FlightSegmentTO> flightSegmentTOs = ServiceTaxConverterUtil.adaptFlightSegmentsFromResSegmentDTOs(flightSegments);
				
				serviceTaxQuoteCriteriaDTO.setBaseAvailRQ(baseAvailRQ);
				serviceTaxQuoteCriteriaDTO.setFareSegChargeTO(null);
				serviceTaxQuoteCriteriaDTO.setFlightSegmentTOs(flightSegmentTOs);
				serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(null);
				serviceTaxQuoteCriteriaDTO.setPaxState(reservation.getContactInfo().getState());
				serviceTaxQuoteCriteriaDTO.setPaxCountryCode(reservation.getContactInfo().getCountryCode());

				String taxRegNo = reservation.getContactInfo().getTaxRegNo();
				serviceTaxQuoteCriteriaDTO.setPaxTaxRegistrationNo(taxRegNo);
				serviceTaxQuoteCriteriaDTO.setPaxTaxRegistered((taxRegNo != null && !taxRegNo.isEmpty()) ? true : false);

				serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
				serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
				serviceTaxQuoteCriteriaDTO.setCalculateOnlyForExternalCharges(true);
				
				try {
					serviceTaxQuoteRS = ReservationModuleUtils
							.getAirproxyReservationBD().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, credentialsDTO.getTrackInfoDTO());
				} catch (ModuleException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(serviceTaxQuoteRS != null && serviceTaxQuoteRS.values().iterator().next().getPaxWiseServiceTaxes() != null){
					Iterator<ServiceTaxQuoteForTicketingRevenueResTO> rs = serviceTaxQuoteRS.values().iterator();
					while(rs.hasNext()){
						ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS = rs.next();
						for(ServiceTaxTO serviceTaxTO : serviceTaxRS.getPaxWiseServiceTaxes().get(1)){
							ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(serviceTaxTO.getChargeGroupCode(),
									serviceTaxTO.getAmount(), quotedDate, AccelAeroCalculator.getDefaultBigDecimalZero(),
									AccelAeroCalculator.getDefaultBigDecimalZero(), serviceTaxTO.getChargeCode(), null);
							addCharge(paxType, metaTO);
						}
					}
					
				}
				
			}
			
		}

		return penaltyAmount;
	}

	private void processCharges() {
		if (!processCharges) {
			synchronized (this) {
				if (!processCharges) {
					if (ondFares != null) {
						for (OndFareDTO ondFare : ondFares) {
							if (flownAssitUnit != null) {
								if (flownAssitUnit.isFlownOnd(ondFare)) {
									continue;
								}

								if (flownAssitUnit.isPenaltyApplicableOnd(ondFare)) {
									addPenalty(PaxTypeTO.ADULT, calculateServiceTaxPenalty(PaxTypeTO.ADULT, flownAssitUnit.getPenaltyForOnd(ondFare, PaxTypeTO.ADULT)));
									addPenalty(PaxTypeTO.CHILD, calculateServiceTaxPenalty(PaxTypeTO.CHILD, flownAssitUnit.getPenaltyForOnd(ondFare, PaxTypeTO.CHILD)));
									addPenalty(PaxTypeTO.INFANT, calculateServiceTaxPenalty(PaxTypeTO.INFANT, flownAssitUnit.getPenaltyForOnd(ondFare, PaxTypeTO.INFANT)));
								}
							}
							
							ModifyAssitUnit modifyAsst = flownAssitUnit.getModifyAsst();
							BigDecimal adultDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
							BigDecimal childDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
							BigDecimal infantDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
							if (modifyAsst != null && modifyAsst.getReservationDiscountDTO() != null) {
								Map<String, BigDecimal> fareDiscountMap = modifyAsst.extractFareDiscountByPaxType(ondFare
										.getFlightSegmentIds());
								adultDiscount = fareDiscountMap.get(PaxTypeTO.ADULT);
								childDiscount = fareDiscountMap.get(PaxTypeTO.CHILD);
								infantDiscount = fareDiscountMap.get(PaxTypeTO.INFANT);
							}

							BigDecimal[] adjustments = AccelAeroCalculator.parseBigDecimal(ondFare.getTotalFareAdjustment());

							if (ReservationApiUtils.isFareExist(ondFare.getAdultFare())) {
								BigDecimal currentAdultFare = AccelAeroCalculator.parseBigDecimal(ondFare.getAdultFare());
								addFareCharges(PaxTypeTO.ADULT, currentAdultFare,adultDiscount.negate(), adjustments[0]);
							}

							if (ReservationApiUtils.isFareExist(ondFare.getInfantFare())) {
								BigDecimal currentInfantFare = AccelAeroCalculator.parseBigDecimal(ondFare.getInfantFare());
								addFareCharges(PaxTypeTO.INFANT, currentInfantFare,infantDiscount.negate(), adjustments[1]);
							}

							if (ReservationApiUtils.isFareExist(ondFare.getChildFare())) {
								BigDecimal currentChildFare = AccelAeroCalculator.parseBigDecimal(ondFare.getChildFare());
								addFareCharges(PaxTypeTO.CHILD, currentChildFare,childDiscount.negate(), adjustments[2]);
							}

							if(ondFare.getAllCharges() != null){
								
								applyChargesIntoCategory(ondFare.getAllCharges());
								
							}
							
						}
					}
					processCharges = true;
				}
			}
		}
	}

	private void applyChargesIntoCategory(Collection<QuotedChargeDTO> allCharges) {
		for (QuotedChargeDTO quotedChargeDTO : allCharges) {
			if (ChargeQuoteUtils.isChgApplicableForPaxType(quotedChargeDTO, PaxTypeTO.ADULT)) {
				addMetaCharge(PaxTypeTO.ADULT, quotedChargeDTO.getChargeGroupCode(),
						AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT)),
						new BigDecimal(0), null, quotedChargeDTO.getChargeCode(), quotedChargeDTO.getChargeRateId());
			}
			if (ChargeQuoteUtils.isChgApplicableForPaxType(quotedChargeDTO, PaxTypeTO.CHILD)) {
				addMetaCharge(PaxTypeTO.CHILD, quotedChargeDTO.getChargeGroupCode(),
						AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD)),
						new BigDecimal(0), null, quotedChargeDTO.getChargeCode(), quotedChargeDTO.getChargeRateId());
			}
			if (ChargeQuoteUtils.isChgApplicableForPaxType(quotedChargeDTO, PaxTypeTO.INFANT)) {
				addMetaCharge(PaxTypeTO.INFANT, quotedChargeDTO.getChargeGroupCode(),
						AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT)),
						new BigDecimal(0), null, quotedChargeDTO.getChargeCode(), quotedChargeDTO.getChargeRateId());
			}
		}

	}

	public Collection<ChargeMetaTO> getChargesPerPax(String paxType) {
		processCharges();
		if (paxTypeFareMetaCharges.containsKey(paxType)) {
			return paxTypeFareMetaCharges.get(paxType);
		}
		return new ArrayList<ChargeMetaTO>();
	}

	public boolean hasPenaltyChargesPerPax(String paxType) {
		return getPenaltyChargesPerPax(paxType).compareTo(BigDecimal.ZERO) > 0;
	}

	public BigDecimal getPenaltyChargesPerPax(String paxType) {
		processCharges();
		if (paxTypePenaltyMetaCharges.containsKey(paxType)) {
			return paxTypePenaltyMetaCharges.get(paxType);
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}
	
	public Collection<ChargeMetaTO> getPaxDueAdjustmentCharges(Integer pnrPaxId, String paxType) {
		Collection<ChargeMetaTO> list = new ArrayList<ChargeMetaTO>();
		if (ondFares != null) {
			for (OndFareDTO ondFare : ondFares) {
				Collection<QuotedChargeDTO> quotedChargeDTOList = ondFare.getAllCharges();

				if(quotedChargeDTOList != null) {
					for (QuotedChargeDTO quoChargeDTO : quotedChargeDTOList) {
						if (ChargeGroups.ADJUSTMENT_CHARGE.equals(quoChargeDTO.getChargeGroupCode())
								&& ChargeCodes.CNX_SEGMENT_SYSTEM_ADJ.equals(quoChargeDTO.getChargeCode())) {
							Double balanceDueAmount = quoChargeDTO.getPaxWiseBalanceDueAdjAmount(pnrPaxId);
							if (balanceDueAmount < 0) {
								ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(ChargeGroups.ADJUSTMENT_CHARGE,
										AccelAeroCalculator.parseBigDecimal(balanceDueAmount), quotedDate, new BigDecimal(0), null);
								list.add(metaTO);

							}

						}

					}
				}
			}
		}
		return list;
	}
}
