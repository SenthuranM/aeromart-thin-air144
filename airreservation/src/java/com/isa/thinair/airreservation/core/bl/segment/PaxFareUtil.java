package com.isa.thinair.airreservation.core.bl.segment;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;

public class PaxFareUtil {
	/**
	 * Return fare Ids and charge rate Ids
	 * 
	 * @param reservationPaxFare
	 * @param fareIds
	 * @param chgRateIds
	 */
	public static void getFareIdsAndChargeIds(ReservationPaxFare reservationPaxFare, Collection<Integer> fareIds,
			Collection<Integer> chgRateIds) {
		Iterator<ReservationPaxOndCharge> itCharges = reservationPaxFare.getCharges().iterator();
		ReservationPaxOndCharge reservationPaxOndCharge;

		while (itCharges.hasNext()) {
			reservationPaxOndCharge = (ReservationPaxOndCharge) itCharges.next();

			if (reservationPaxOndCharge.getChargeRateId() != null) {
				chgRateIds.add(reservationPaxOndCharge.getChargeRateId());
			} else if (reservationPaxOndCharge.getFareId() != null) {
				fareIds.add(reservationPaxOndCharge.getFareId());
			}
		}
	}

	public static void getFareIdsAndChargeIds(Reservation reservation, Collection<Integer> fareIds,
			Collection<Integer> chgRateIds) {
		for (ReservationPax pax : reservation.getPassengers()) {
			for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
				getFareIdsAndChargeIds(paxFare, fareIds, chgRateIds);
			}
		}
	}
}
