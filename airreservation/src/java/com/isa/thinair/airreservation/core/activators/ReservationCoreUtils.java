/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.activators;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.core.util.StringUtil;
import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TransferSegmentDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCreditPromotion;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxSegmentSSRStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAutomaticCheckinDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * Holds Reservation business object specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationCoreUtils {

	private static GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();

	private ReservationCoreUtils() {

	}

	/**
	 * Capture Ond charges
	 * 
	 * @param chargeGroupCode
	 *            the charge group code
	 * @param amount
	 *            the charge amount
	 * @param totalCharges
	 *            totalcharges
	 * @param discount
	 * @param adjustment
	 */
	public static void captureOndCharges(String chargeGroupCode, BigDecimal amount, BigDecimal[] totalCharges,
			BigDecimal discount, BigDecimal adjustment) {
		// --------------------------------------------------
		if (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeGroupCode)) {
			totalCharges[0] = AccelAeroCalculator.add(totalCharges[0], amount);

			// currently ROE adjustment is only for FARE applying only for FAR Group
			if (adjustment != null && adjustment.doubleValue() != 0) {
				totalCharges[5] = AccelAeroCalculator.add(totalCharges[5], adjustment);
			}
		}
		// --------------------------------------------------
		else if (ReservationInternalConstants.ChargeGroup.TAX.equals(chargeGroupCode)) {
			totalCharges[1] = AccelAeroCalculator.add(totalCharges[1], amount);
		}
		// --------------------------------------------------
		else if (ReservationInternalConstants.ChargeGroup.SUR.equals(chargeGroupCode)) {
			totalCharges[2] = AccelAeroCalculator.add(totalCharges[2], amount);
		} else if (ReservationInternalConstants.ChargeGroup.INF.equals(chargeGroupCode)) {
			totalCharges[2] = AccelAeroCalculator.add(totalCharges[2], amount);
		}
		// --------------------------------------------------
		else if (ReservationInternalConstants.ChargeGroup.CNX.equals(chargeGroupCode)) {
			totalCharges[3] = AccelAeroCalculator.add(totalCharges[3], amount);
		}
		// --------------------------------------------------
		else if (ReservationInternalConstants.ChargeGroup.MOD.equals(chargeGroupCode)) {
			totalCharges[4] = AccelAeroCalculator.add(totalCharges[4], amount);
		}
		// --------------------------------------------------
		else if (ReservationInternalConstants.ChargeGroup.ADJ.equals(chargeGroupCode)) {
			totalCharges[5] = AccelAeroCalculator.add(totalCharges[5], amount);
		}
		// --------------------------------------------------
		else if (ReservationInternalConstants.ChargeGroup.PEN.equals(chargeGroupCode)) {
			totalCharges[6] = AccelAeroCalculator.add(totalCharges[6], amount);
		}
		// --------------------------------------------------

		if (discount != null && discount.doubleValue() != 0) {
			totalCharges[7] = AccelAeroCalculator.add(totalCharges[7], discount);
		}

	}

	/**
	 * Create ReservationPaxOndCharge
	 * 
	 * 
	 * @param amount
	 * @param fareId
	 * @param chargeRateId
	 * @param chargeGroupCode
	 * @param reservationPaxFare
	 * @param credentialsDTO
	 * @param isRefundableOperation
	 * @param paxDiscInfo
	 * @param adjustment
	 * @param paxOndChargeDTO
	 * @param transactionSeq TODO
	 * @param discValuePercentage
	 * @return
	 */
	public static ReservationPaxOndCharge captureReservationPaxOndCharge(BigDecimal amount, Integer fareId, Integer chargeRateId,
			String chargeGroupCode, ReservationPaxFare reservationPaxFare, CredentialsDTO credentialsDTO,
			boolean isRefundableOperation, PaxDiscountInfoDTO paxDiscInfo, BigDecimal adjustment,
			PaxOndChargeDTO paxOndChargeDTO, Integer transactionSeq) {

		BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		double discValuePercentage = 0;
		if (paxDiscInfo != null) {
			discount = paxDiscInfo.getDiscount();
			if (paxDiscInfo.isCreditPromotion()) {
				// Setting the fare charge
				ReservationPaxOndCreditPromotion reservationPaxOndCreditPromo = TOAssembler
						.createReservationPaxOndCreditPromotion(amount, fareId, chargeRateId, chargeGroupCode, credentialsDTO,
								paxDiscInfo.getDiscount());

				// Adding the new charge
				reservationPaxFare.addPromotionCredits(reservationPaxOndCreditPromo);
				discount = AccelAeroCalculator.getDefaultBigDecimalZero();
			}

			if (paxDiscInfo.isDiscountApplied()) {
				reservationPaxFare.setDiscountCode(paxDiscInfo.getDicountCode());
			} else {
				discount = AccelAeroCalculator.getDefaultBigDecimalZero();
			}
			if (paxDiscInfo.getDiscount().doubleValue() != 0 && ChargeCodes.FARE_DISCOUNT.equals(paxDiscInfo.getDicountCode())) {
				discValuePercentage = paxDiscInfo.getDiscountPercentage();
			}
		}

		// Setting the fare charge
		ReservationPaxOndCharge reservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(amount, fareId, chargeRateId,
				chargeGroupCode, credentialsDTO, isRefundableOperation, discValuePercentage, discount, adjustment,
				paxOndChargeDTO, transactionSeq);

		BigDecimal[] totalCharges = reservationPaxFare.getTotalChargeAmounts();

		// Getting the total charge
		ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(), reservationPaxOndCharge.getAmount(),
				totalCharges, reservationPaxOndCharge.getDiscount(), reservationPaxOndCharge.getAdjustment());

		// Setting the total charge
		reservationPaxFare.setTotalChargeAmounts(totalCharges);
		// Adding the new charge
		reservationPaxFare.addCharge(reservationPaxOndCharge);

		return reservationPaxOndCharge;
	}

	public static ReservationPaxOndCharge captureReservationPaxOndCharge(BigDecimal amount, Integer fareId, Integer chargeRateId,
			String chargeGroupCode, ReservationPaxFare reservationPaxFare, String taxAppliedCategory,
			CredentialsDTO credentialsDTO, boolean isRefundableOperation, PaxDiscountInfoDTO paxDiscInfo, BigDecimal adjustment,
			PaxOndChargeDTO paxOndChargeDTO, Integer transactionSeq) {

		ReservationPaxOndCharge reservationPaxOndCharge = captureReservationPaxOndCharge(amount, fareId, chargeRateId,
				chargeGroupCode, reservationPaxFare, credentialsDTO, isRefundableOperation, paxDiscInfo, adjustment,
				paxOndChargeDTO, transactionSeq);
		if (!StringUtil.isNullOrEmpty(taxAppliedCategory)) {
			reservationPaxOndCharge.setTaxAppliedCategory(taxAppliedCategory);
		}
		return reservationPaxOndCharge;
	}

	/**
	 * Returns Segment seats for blocking the inventory
	 * 
	 * @return Collection of SegmentSeatDistsDTO
	 * @throws ModuleException
	 */
	public static Collection<SegmentSeatDistsDTO> getSegmentSeatDistsDTOs(Collection<OndFareDTO> ondFareDTOs)
			throws ModuleException {
		if (ondFareDTOs == null) {
			throw new ModuleException("airreservations.arg.invalidInfoFromInventory");
		}

		Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = new ArrayList<SegmentSeatDistsDTO>();
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			if (!ondFareDTO.isFlown()) {
				for (SegmentSeatDistsDTO segmentSeatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
					segmentSeatDistsDTO.setBelongToInboundOnd(ondFareDTO.isInBoundOnd());
					segmentSeatDistsDTOs.add(segmentSeatDistsDTO);
				}
			}
		}
		return segmentSeatDistsDTOs;
	}

	/**
	 * Set reservation and passenger total amounts
	 * 
	 * @param reservation
	 */
	public static void setPnrAndPaxTotalAmounts(Reservation reservation) {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		Iterator<ReservationPaxFare> itReservationPaxFare;
		BigDecimal[] totalPaxFareAmounts;
		BigDecimal[] totalReservationAmounts = ReservationApiUtils.getChargeTypeAmounts();
		// iterate each passenger
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			totalPaxFareAmounts = ReservationApiUtils.getChargeTypeAmounts();
			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = itReservationPaxFare.next();
				ReservationCoreUtils.updateTotals(totalPaxFareAmounts, reservationPaxFare.getTotalChargeAmounts(), true);
			}

			reservationPax.setTotalChargeAmounts(totalPaxFareAmounts);

			ReservationCoreUtils.updateTotals(totalReservationAmounts, reservationPax.getTotalChargeAmounts(), true);
		}

		reservation.setTotalChargeAmounts(totalReservationAmounts);
	}

	/**
	 * Parse the reservation and returns ReservationPaxDTO
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationPaxDTO parseReservation(Reservation reservation, Map<Integer, Integer> adultAndInfantMap)
			throws ModuleException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

		ReservationPaxDTO reservationPaxDTO = new ReservationPaxDTO();

		// Setting the contact information
		ReservationContactInfo contactInfoSrv = reservation.getContactInfo();
		ReservationContactInfo contactInfoHolder = new ReservationContactInfo();

		contactInfoHolder.setTitle(BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(contactInfoSrv.getTitle())));
		contactInfoHolder.setFirstName(BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(contactInfoSrv.getFirstName())));
		contactInfoHolder.setLastName(BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(contactInfoSrv.getLastName())));

		contactInfoHolder.setEmail(BeanUtils.nullHandler(contactInfoSrv.getEmail()));
		contactInfoHolder.setMobileNo(BeanUtils.nullHandler(contactInfoSrv.getMobileNo()));
		contactInfoHolder.setPhoneNo(BeanUtils.nullHandler(contactInfoSrv.getPhoneNo()));
		contactInfoHolder.setFax(BeanUtils.nullHandler(contactInfoSrv.getFax()));

		contactInfoHolder.setStreetAddress1(BeanUtils.nullHandler(contactInfoSrv.getStreetAddress1()));
		contactInfoHolder.setStreetAddress2(BeanUtils.nullHandler(contactInfoSrv.getStreetAddress2()));
		contactInfoHolder.setState(BeanUtils.nullHandler(contactInfoSrv.getState()));
		contactInfoHolder.setCity(BeanUtils.nullHandler(contactInfoSrv.getCity()));
		contactInfoHolder.setCountryCode(BeanUtils.nullHandler(contactInfoSrv.getCountryCode()));
		contactInfoHolder.setNationalityCode(BeanUtils.nullHandler(contactInfoSrv.getNationalityCode()));

		reservationPaxDTO.setReservationContactInfo(contactInfoHolder);

		// Setting the reservation admin information
		ReservationAdminInfo adminInfoSrv = reservation.getAdminInfo();
		ReservationAdminInfo adminInfoHolder = new ReservationAdminInfo();

		adminInfoHolder.setOriginAgentCode(adminInfoSrv.getOriginAgentCode());
		adminInfoHolder.setOwnerAgentCode(adminInfoSrv.getOwnerAgentCode());
		adminInfoHolder.setOriginChannelId(adminInfoSrv.getOriginChannelId());
		adminInfoHolder.setOwnerChannelId(adminInfoSrv.getOwnerChannelId());
		adminInfoHolder.setOriginUserId(adminInfoSrv.getOriginUserId());

		reservationPaxDTO.setReservationAdminInfo(adminInfoHolder);

		// Collect the release time stamps
		Date[] releaseTimeStamps = reservation.getReleaseTimeStamps();

		// Setting other pnr information
		reservationPaxDTO.setZuluReleaseTimeStamp(releaseTimeStamps[0]);
		reservationPaxDTO.setLocalReleaseTimeStamp(releaseTimeStamps[1]);
		reservationPaxDTO.setPnr(reservation.getPnr());
		reservationPaxDTO.setOriginatorPnr(reservation.getOriginatorPnr());
		reservationPaxDTO.setPnrDate(reservation.getZuluBookingTimestamp());
		reservationPaxDTO.setStatus(reservation.getStatus());

		// Setting the reservation passenger information
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxDetailsDTO reservationPaxDetailsDTO;

		// Used for Passenger Selection
		int adultCount = 0;
		int childCount = 0;
		int infantCount = 0;
		BigDecimal totalTicketFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalTicketCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalAvailBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			// Collect only adult pax info (excluding infants)
			if (!ReservationApiUtils.isInfantType(reservationPax)) {

				reservationPaxDetailsDTO = new ReservationPaxDetailsDTO();

				reservationPaxDetailsDTO.setPnr(reservationPax.getReservation().getPnr());
				reservationPaxDetailsDTO.setPnrPaxId(reservationPax.getPnrPaxId());
				reservationPaxDetailsDTO
						.setTitle(BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(reservationPax.getTitle())));
				reservationPaxDetailsDTO
						.setFirstName(BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(reservationPax.getFirstName())));
				reservationPaxDetailsDTO
						.setLastName(BeanUtils.makeFirstLetterCapital(BeanUtils.nullHandler(reservationPax.getLastName())));
				reservationPaxDetailsDTO.setDateOfBirth(reservationPax.getDateOfBirth());
				reservationPaxDetailsDTO.setNationalityCode(reservationPax.getNationalityCode());

				reservationPaxDetailsDTO.setPaxSequence(reservationPax.getPaxSequence());
				reservationPaxDetailsDTO.setPaxType(BeanUtils.nullHandler(reservationPax.getPaxType()));
				reservationPaxDetailsDTO.setStatus(BeanUtils.nullHandler(reservationPax.getStatus()));
				reservationPaxDetailsDTO.setAdultId(reservationPax.getAccompaniedPaxId());

				if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
					Collection<ReservationPax> infants = reservationPax.getInfants();
					Iterator<ReservationPax> itInfants = infants.iterator();
					// Assumption: Each pax can only accompany one infant
					if (itInfants.hasNext()) {
						ReservationPax infant = itInfants.next();
						reservationPaxDetailsDTO.setChildTitle(BeanUtils.nullHandler(infant.getTitle()));
						reservationPaxDetailsDTO.setChildFirstName(BeanUtils.nullHandler(infant.getFirstName()));
						reservationPaxDetailsDTO.setChildLastName(BeanUtils.nullHandler(infant.getLastName()));
						// reservationPaxDetailsDTO.setChildSsrCode(BeanUtils.nullHandler(infant.getSsrCode()));

						if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_PASSPORT_IN_ITINERARY))) {
							reservationPaxDetailsDTO
									.setChildFoid(BeanUtils.nullHandler(infant.getPaxAdditionalInfo().getPassportNo()));
						}
						if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_DOB_IN_ITINERARY))
								&& infant.getDateOfBirth() != null) {
							reservationPaxDetailsDTO
									.setChildDob(BeanUtils.nullHandler(dateFormat.format(infant.getDateOfBirth()).toString()));
						}

						// set infant ssr

						Collection<PaxSSRDTO> colInfantSSR = infant.getPaxSSR();
						if (colInfantSSR != null && !colInfantSSR.isEmpty()) {
							StringBuilder strInfantSSRCodes = new StringBuilder();
							Collection<String> colDispSSR = new ArrayList<String>();
							for (PaxSSRDTO infantSSRDTO : colInfantSSR) {
								if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(infantSSRDTO.getStatus())
										&& infantSSRDTO.getSsrSubCatID().intValue() == 0) {
									if (!colDispSSR.contains(infantSSRDTO.getSsrCode())) {

										strInfantSSRCodes.append(infantSSRDTO.getSsrCode()).append(" ");
										colDispSSR.add(infantSSRDTO.getSsrCode());
									}
								}
							}
							reservationPaxDetailsDTO.setChildSsrCode(strInfantSSRCodes.toString());
						}

					}
				}

				reservationPaxDetailsDTO.setTotalFare(reservationPax.getTotalFare());
				reservationPaxDetailsDTO.setTotalChargeAmount(
						AccelAeroCalculator.subtract(reservationPax.getTotalChargeAmount(), reservationPax.getTotalFare()));
				reservationPaxDetailsDTO.setTotalPaidAmount(reservationPax.getTotalPaidAmount());
				reservationPaxDetailsDTO.setCredit(reservationPax.getTotalAvailableBalance());

				if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_PASSPORT_IN_ITINERARY))) {
					reservationPaxDetailsDTO.setFoidNumber(BeanUtils.nullHandler((reservationPax.getPaxAdditionalInfo() == null)
							? null
							: reservationPax.getPaxAdditionalInfo().getPassportNo()));
				}
				if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_DOB_IN_ITINERARY))
						&& reservationPax.getDateOfBirth() != null) {
					reservationPaxDetailsDTO
							.setDob(BeanUtils.nullHandler(dateFormat.format(reservationPax.getDateOfBirth()).toString()));
				}

				Collection<PaxSSRDTO> colPaxSSR = reservationPax.getPaxSSR();
				if (colPaxSSR != null && !colPaxSSR.isEmpty()) {
					// StringBuilder paxSSRText = new StringBuilder();
					StringBuilder paxSSRCode = new StringBuilder();
					Collection<String> colDispSSR = new ArrayList<String>();
					for (PaxSSRDTO paxSSRDTO : colPaxSSR) {
						if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(paxSSRDTO.getStatus())
								&& (paxSSRDTO.getSsrSubCatID().intValue() == 0 || paxSSRDTO.getSsrSubCatID().intValue() == 3)) {
							if (!colDispSSR.contains(paxSSRDTO.getSsrCode())) {
								// paxSSRText.append(paxSSRDTO.getSsrText()).append(" ");
								paxSSRCode.append(paxSSRDTO.getSsrCode()).append(" ");
								colDispSSR.add(paxSSRDTO.getSsrCode());
							}
						}
					}
					colDispSSR = null;
					reservationPaxDetailsDTO.setSsrCode(paxSSRCode.toString());
				}

				// Filtering the passengers
				// If no passengers selected... picking all passengers
				if (adultAndInfantMap == null) {
					reservationPaxDTO.addPassenger(reservationPaxDetailsDTO);
					// Checking for selected passengers
				} else if (adultAndInfantMap.keySet().contains(reservationPax.getPnrPaxId())) {
					if (ReservationApiUtils.isSingleAfterSave(reservationPax)) {
						adultCount = adultCount + 1;
					} else if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
						adultCount = adultCount + 1;
					} else if (ReservationApiUtils.isChildType(reservationPax)) {
						childCount = childCount + 1;
					}

					// If it's a parent
					if (adultAndInfantMap.get(reservationPax.getPnrPaxId()) != null) {
						infantCount = infantCount + 1;
					}

					totalTicketFare = AccelAeroCalculator.add(totalTicketFare, reservationPaxDetailsDTO.getTotalFare());
					totalTicketCharge = AccelAeroCalculator.add(totalTicketCharge,
							reservationPaxDetailsDTO.getTotalChargeAmount());
					totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, reservationPaxDetailsDTO.getTotalPaidAmount());
					totalAvailBalance = AccelAeroCalculator.add(totalAvailBalance, reservationPaxDetailsDTO.getCredit());

					reservationPaxDTO.addPassenger(reservationPaxDetailsDTO);
				}
			}
		}

		// All Passengers
		if (adultAndInfantMap == null) {
			// Passenger Count
			reservationPaxDTO.setAdultCount(new Integer(reservation.getTotalPaxAdultCount()));
			reservationPaxDTO.setChildCount(new Integer(reservation.getTotalPaxChildCount()));
			reservationPaxDTO.setInfantCount(new Integer(reservation.getTotalPaxInfantCount()));

			// Setting the fare
			reservationPaxDTO.setTotalTicketFare(reservation.getTotalTicketFare());

			// Setting the charge
			reservationPaxDTO.setTotalTicketCharge(AccelAeroCalculator.add(reservation.getTotalTicketTaxCharge(),
					reservation.getTotalTicketSurCharge(), reservation.getTotalTicketCancelCharge(),
					reservation.getTotalTicketModificationCharge(), reservation.getTotalTicketAdjustmentCharge()));

			// Setting the paid amount
			reservationPaxDTO.setTotalPaidAmount(reservation.getTotalPaidAmount());
			reservationPaxDTO.setTotalAvailBalance(reservation.getTotalAvailableBalance());
			// Selected Passengers
		} else {
			// Passenger Count
			reservationPaxDTO.setAdultCount(new Integer(adultCount));
			reservationPaxDTO.setChildCount(new Integer(childCount));
			reservationPaxDTO.setInfantCount(new Integer(infantCount));

			// Setting the fare
			reservationPaxDTO.setTotalTicketFare(totalTicketFare);

			// Setting the charge
			reservationPaxDTO.setTotalTicketCharge(totalTicketCharge);

			// Setting the paid amount
			reservationPaxDTO.setTotalPaidAmount(totalPaidAmount);
			reservationPaxDTO.setTotalAvailBalance(totalAvailBalance);
		}

		// Setting the reservation segment information
		Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		while (itReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itReservationSegmentDTO.next();
			// Only segments with status = CNF needed
			if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {

				// Showing only flights where the operation type is defined as
				// shown in itinerary
				if (reservationSegmentDTO.isShownInItinerary()) {
					reservationPaxDTO.addSegment(reservationSegmentDTO);
				}
			}
		}

		return reservationPaxDTO;
	}

	/**
	 * Return total charges
	 * 
	 * @param totalChargesHolder
	 *            total charges holder
	 * @param total
	 *            charges
	 */
	public static void updateTotals(BigDecimal[] totalChargesHolder, BigDecimal[] totalCharges, boolean isSum) {
		if (isSum) {
			for (int i = 0; i < totalCharges.length; i++) {
				totalChargesHolder[i] = AccelAeroCalculator.add(totalChargesHolder[i], totalCharges[i]);
			}
		} else {
			for (int i = 0; i < totalCharges.length; i++) {
				totalChargesHolder[i] = AccelAeroCalculator.subtract(totalChargesHolder[i], totalCharges[i]);
			}
		}
	}

	/**
	 * Update reservation passenger totals
	 * 
	 * @param reservationPax
	 * @param totalCharges
	 * @param isSum
	 */
	public static void updateTotals(ReservationPax reservationPax, BigDecimal[] totalCharges, boolean isSum) {
		BigDecimal[] charges = reservationPax.getTotalChargeAmounts();
		ReservationCoreUtils.updateTotals(charges, totalCharges, isSum);
		reservationPax.setTotalChargeAmounts(charges);
	}

	/**
	 * Update reservation totals
	 * 
	 * @param reservation
	 * @param totalCharges
	 * @param isSum
	 */
	public static void updateTotals(Reservation reservation, BigDecimal[] totalCharges, boolean isSum) {
		BigDecimal[] charges = reservation.getTotalChargeAmounts();
		ReservationCoreUtils.updateTotals(charges, totalCharges, isSum);
		reservation.setTotalChargeAmounts(charges);
	}

	public static BigDecimal getTolalPenaltyCharges(Collection<ReservationPaxFare> paxFares) {
		BigDecimal charges = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPaxFare paxFare : paxFares) {
			charges = AccelAeroCalculator.add(charges, paxFare.getTotalPenaltyCharge());
		}
		return charges;
	}

	/**
	 * Return the passenger
	 * 
	 * @param pnrPaxId
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationPax getPassenger(Integer pnrPaxId, Reservation reservation) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax = null;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {
				break;
			}
		}

		if (reservationPax == null) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		return reservationPax;
	}

	/**
	 * Return the passenger payment information
	 * 
	 * @param pnrPaxIdAndPayments
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static String getPassengerPaymentInfo(Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Reservation reservation)
			throws ModuleException {

		PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;

		StringBuffer changes = new StringBuffer();
		if (pnrPaxIdAndPayments != null && pnrPaxIdAndPayments.keySet() != null && !pnrPaxIdAndPayments.keySet().isEmpty()) {
			Iterator<Integer> itPaxIds = pnrPaxIdAndPayments.keySet().iterator();
			Iterator<PaymentInfo> itPaymentInfo;
			Integer pnrPaxId;
			PaymentAssembler paymentAssembler;
			ReservationPax reservationPax;
			PaymentInfo paymentInfo;

			BigDecimal creditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal cardAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal agentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal cashAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal bspAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal loyaltyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal voucherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			StringBuffer creditChanges;
			StringBuffer creditCardChanges;
			StringBuffer onAccountChanges;
			StringBuffer bspChanges;
			StringBuffer loyaltyChanges;
			StringBuffer voucherChanges;

			// Map oldCarryForwardPnrMap = new HashMap();

			while (itPaxIds.hasNext()) {
				pnrPaxId = itPaxIds.next();

				// Get the reservation passenger
				if (reservation == null) {
					reservationPax = passengerDAO.getPassenger(pnrPaxId.intValue(), false, false, false);
				} else {
					reservationPax = ReservationCoreUtils.getPassenger(pnrPaxId, reservation);
				}

				changes.append(" <br>");

				// Componse Passengers audit
				String passengerDetails = composePassengersAudit(reservationPax);
				changes.append(passengerDetails);

				paymentAssembler = (pnrPaxIdAndPayments.get(pnrPaxId));
				itPaymentInfo = paymentAssembler.getPayments().iterator();

				creditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				cardAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				agentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				bspAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				cashAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				loyaltyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				voucherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				creditChanges = new StringBuffer();
				creditCardChanges = new StringBuffer();
				onAccountChanges = new StringBuffer();
				bspChanges = new StringBuffer();
				loyaltyChanges = new StringBuffer();
				voucherChanges = new StringBuffer();
				StringBuilder strPayRef = new StringBuilder();

				while (itPaymentInfo.hasNext()) {
					paymentInfo = itPaymentInfo.next();

					// For Pax Credit Payment
					if (paymentInfo instanceof PaxCreditInfo) {
						PaxCreditDTO paxCreditDTO = ((PaxCreditInfo) paymentInfo).getPaxCredit();
						creditAmount = AccelAeroCalculator.add(creditAmount, paxCreditDTO.getBalance());

						if (paxCreditDTO.getPayCarrier().equals(AppSysParamsUtil.getDefaultCarrierCode())
								&& paxCreditDTO.getDebitPaxId() != null) {
							// To avoid this block be executed for using pax credit
							// in OC for dry booking
							ReservationPax debitReservationPax = passengerDAO
									.getPassenger(new Integer(paxCreditDTO.getDebitPaxId()).intValue(), false, false, false);
							creditChanges.append(paxCreditDTO.getPayCarrier()).append(":");
							creditChanges.append(paxCreditDTO.getPnr()).append(" - ");
							creditChanges.append(BeanUtils.nullHandler(debitReservationPax.getTitle())).append(", ");
							creditChanges.append(BeanUtils.nullHandler(debitReservationPax.getFirstName())).append(", ");
							creditChanges.append(BeanUtils.nullHandler(debitReservationPax.getLastName())).append(" - ");
							creditChanges.append(debitReservationPax.getPaxTypeDescription()).append(" - ");
							creditChanges.append(paxCreditDTO.getBalance()).append(" ");
						} else { // credit utilize form other carrier.
							creditChanges.append(paxCreditDTO.getPayCarrier()).append(":");
							creditChanges.append(paxCreditDTO.getPnr()).append(" - ");
							creditChanges.append(paxCreditDTO.getBalance()).append(" ");
						}

						// Capturing the old pnr credit information
						/*
						 * if (oldCarryForwardPnrMap.containsKey(paxCreditDTO.getPnr( ))) { String info = (String)
						 * oldCarryForwardPnrMap.get(paxCreditDTO.getPnr()); info += " " +
						 * reservationPax.getReservation().getPnr() + " " + passengerDetails + " " +
						 * paxCreditDTO.getBalance(); oldCarryForwardPnrMap.put(paxCreditDTO.getPnr(), info); } else {
						 * oldCarryForwardPnrMap.put(paxCreditDTO.getPnr(), reservationPax.getReservation().getPnr() +
						 * " " + passengerDetails + " " + paxCreditDTO.getBalance()); }
						 */
					}
					// For Card Payment
					else if (paymentInfo instanceof CardPaymentInfo) {
						CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) paymentInfo;
						cardAmount = AccelAeroCalculator.add(cardAmount, cardPaymentInfo.getTotalAmount());

						creditCardChanges.append(PaymentType.getPaymentTypeDesc(cardPaymentInfo.getType()));

						if (StringUtils.isNotEmpty(cardPaymentInfo.getOperationType())) {
							creditCardChanges.append(cardPaymentInfo.getOperationType());
						}

						if (StringUtils.isNotEmpty(cardPaymentInfo.getAuthorizationId())) {
							creditCardChanges.append(" [" + cardPaymentInfo.getAuthorizationId() + "] ");
						}

						strPayRef.append(getPayModeDescription(cardPaymentInfo.getPaymentReferanceTO()));
					}
					// For Agent Credit
					else if (paymentInfo instanceof AgentCreditInfo) {
						AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;

						if (agentCreditInfo.getPaymentReferenceTO() != null
								&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType() != null
								&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
							bspChanges.append(agentCreditInfo.getAgentCode() + " ");
							bspAmount = AccelAeroCalculator.add(bspAmount, agentCreditInfo.getTotalAmount());

						} else {
							onAccountChanges.append(agentCreditInfo.getAgentCode() + " ");
							agentAmount = AccelAeroCalculator.add(agentAmount, agentCreditInfo.getTotalAmount());
						}
						strPayRef.append(getPayModeDescription(agentCreditInfo.getPaymentReferenceTO()));
					}
					// For Cash Payment
					else if (paymentInfo instanceof CashPaymentInfo) {
						cashAmount = AccelAeroCalculator.add(cashAmount, paymentInfo.getTotalAmount());
						CashPaymentInfo cashPaymentInfo = (CashPaymentInfo) paymentInfo;
						strPayRef.append(getPayModeDescription(cashPaymentInfo.getPaymentReferanceTO()));
					}

					// For Loyalty Payment
					else if (paymentInfo instanceof LMSPaymentInfo) {
						loyaltyAmount = AccelAeroCalculator.add(cashAmount, paymentInfo.getTotalAmount());
						LMSPaymentInfo lmsPaymentInfo = (LMSPaymentInfo) paymentInfo;
						loyaltyChanges.append("Loyalty Account: ").append(lmsPaymentInfo.getLoyaltyMemberAccountId());
						loyaltyChanges.append(", Reward IDs: ").append(Arrays.toString(lmsPaymentInfo.getRewardIDs()));
					}

					// For Voucher Payment
					else if (paymentInfo instanceof VoucherPaymentInfo) {
						
						VoucherPaymentInfo voucherPaymentInfo = (VoucherPaymentInfo) paymentInfo;
						if (voucherPaymentInfo.getVoucherDTO() != null) {
							voucherAmount = AccelAeroCalculator.add(voucherAmount, new BigDecimal(voucherPaymentInfo
									.getVoucherDTO().getRedeemdAmount()));
						} else{
							voucherAmount = AccelAeroCalculator.add(cashAmount, paymentInfo.getTotalAmount());
						}

						if (voucherChanges.toString().trim().isEmpty()) {
							voucherChanges.append("Voucher ID(s): ");
						} else {
							voucherChanges.append(", ");
						}
						voucherChanges.append(voucherPaymentInfo.getVoucherDTO().getVoucherId());
					}
				}

				changes.append(" CASH " + cashAmount + " CREDIT_CARD(" + creditCardChanges.toString().trim() + ") " + cardAmount
						+ " ON_ACC(" + onAccountChanges.toString().trim() + ") " + agentAmount + " BSP("
						+ bspChanges.toString().trim() + ") " + bspAmount + " CREDIT_BF(" + creditChanges.toString().trim() + ") "
						+ creditAmount + " LOYALTY(" + loyaltyChanges.toString().trim() + ") " + loyaltyAmount + " VOUCHER("
						+ voucherChanges.toString().trim() + ") " + voucherAmount);

				if (strPayRef.length() > 0) {
					changes.append(" PAY_REF(" + strPayRef.toString() + ") ");
				}

				changes.append(" </br> ");
			}
		}

		// Remove this later
		// Update the old pnr histories
		// Collection colReservationAudit =
		// updateOldPnrHistories(oldCarryForwardPnrMap);

		return changes.toString();
	}

	// FIXME
	public static Collection<ReservationAudit> composeAuditForCarryForwardCredit(PaxCreditDTO paxCreditDTO)
			throws ModuleException {
		Collection<ReservationAudit> reservationAudits = new ArrayList<ReservationAudit>();

		ReservationAudit reservationAuditCF = new ReservationAudit();
		reservationAuditCF.setPnr(paxCreditDTO.getPnr());
		reservationAuditCF.setModificationType(AuditTemplateEnum.CREDIT_CARRY_FORWARD.getCode());

		ReservationAudit reservationAuditBF = new ReservationAudit();
		reservationAuditBF.setPnr(paxCreditDTO.getCreditUtilizingPnr());
		reservationAuditBF.setModificationType(AuditTemplateEnum.CREDIT_BROUGHT_FORWARD.getCode());

		String auditContentCF = null;
		String auditContentBF = null;

		if (paxCreditDTO.getPayCarrier().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
			PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;
			ReservationPax debitReservationPax = passengerDAO.getPassenger(new Integer(paxCreditDTO.getDebitPaxId()), false,
					false, false);
			auditContentCF = "From: carrier: " + paxCreditDTO.getPayCarrier() + " pnr: " + paxCreditDTO.getPnr() + " pax: "
					+ (debitReservationPax.getTitle() == null ? "" : debitReservationPax.getTitle()) + ", "
					+ debitReservationPax.getFirstName() + ", " + debitReservationPax.getLastName() + ". To: pnr: "
					+ paxCreditDTO.getCreditUtilizingPnr() + ", paxseq: " + paxCreditDTO.getPaxSequence();
			auditContentBF = "From: carrier:" + paxCreditDTO.getPayCarrier() + "pnr: " + paxCreditDTO.getPnr() + ", pax: "
					+ (debitReservationPax.getTitle() == null ? " " : debitReservationPax.getTitle()) + ", "
					+ debitReservationPax.getFirstName() + ", " + debitReservationPax.getLastName() + ". To: paxseq: "
					+ paxCreditDTO.getPaxSequence();
		} else {
			auditContentCF = "From: carrier: " + paxCreditDTO.getPayCarrier() + " pnr: " + paxCreditDTO.getPnr() + ". To: pnr: "
					+ paxCreditDTO.getCreditUtilizingPnr() + ", paxseq: " + paxCreditDTO.getPaxSequence();
			auditContentBF = "From: carrier: " + paxCreditDTO.getPayCarrier() + "pnr: " + paxCreditDTO.getPnr() + ". To: paxseq: "
					+ paxCreditDTO.getPaxSequence();
		}

		// Setting the credit carry forward information
		reservationAuditCF.addContentMap(AuditTemplateEnum.TemplateParams.CreditCarryForward.CR_CARRY_FORWARD_INFORMATION,
				" CREDIT_CF(" + auditContentCF + ", amount: " + paxCreditDTO.getBalance() + ") ");
		reservationAudits.add(reservationAuditCF);

		// Setting the credit carry forward information
		reservationAuditBF.addContentMap(AuditTemplateEnum.TemplateParams.CreditBroughtForward.CR_BROUGHT_FORWARD_INFORMATION,
				" CREDIT_BF(" + auditContentBF + ", amount: " + paxCreditDTO.getBalance() + ") ");
		reservationAudits.add(reservationAuditBF);

		return reservationAudits;
	}

	/**
	 * Update old pnr histories
	 * 
	 * @param oldCarryForwardPnrMap
	 * @throws ModuleException
	 */
	// private static Collection updateOldPnrHistories(Map oldCarryForwardPnrMap) throws ModuleException {
	// Collection colReservationAudit = new ArrayList();
	// Iterator itOldCarryForwardPnrMap = oldCarryForwardPnrMap.keySet().iterator();
	// ReservationAudit reservationAudit;
	// String pnr;
	// String changes;
	//
	// while (itOldCarryForwardPnrMap.hasNext()) {
	// pnr = (String) itOldCarryForwardPnrMap.next();
	// changes = (String) oldCarryForwardPnrMap.get(pnr);
	//
	// reservationAudit = new ReservationAudit();
	// reservationAudit.setPnr(pnr);
	// reservationAudit.setModificationType(AuditTemplateEnum.CREDIT_CARRY_FORWARD.getCode());
	//
	// // Setting the credit carry forward information
	// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CreditCarryForward.CR_CARRY_FORWARD_INFORMATION,
	// " CREDIT_CF(" + changes + ") ");
	//
	// colReservationAudit.add(reservationAudit);
	// }
	//
	// return colReservationAudit;
	// }

	/**
	 * Compose Passenger Audit
	 * 
	 * @param reservationPax
	 * @param changes
	 */
	private static String composePassengersAudit(ReservationPax reservationPax) {

		StringBuilder changes = new StringBuilder();
		SimpleDateFormat smpdtDDMMYYY = new SimpleDateFormat("dd/MM/yyyy");
		// Main passenger(s) information
		changes.append(BeanUtils.nullHandler(reservationPax.getTitle()));
		changes.append(", " + BeanUtils.nullHandler(reservationPax.getFirstName()));
		changes.append(", " + BeanUtils.nullHandler(reservationPax.getLastName()));

		if (reservationPax.getDateOfBirth() != null) {
			changes.append(" ," + AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.DATE_OF_BIRITH + " :"
					+ smpdtDDMMYYY.format(reservationPax.getDateOfBirth()));

		}

		String ssrData = ReservationSSRUtil.getPaxSSRForAudit(reservationPax);

		if (ssrData != null && !"".equals(ssrData)) {
			changes.append(" ," + AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.SSR + " :" + ssrData);
		}

		changes.append(" - " + reservationPax.getPaxTypeDescription());

		// Infant information
		Collection<ReservationPax> infants = reservationPax.getInfants();
		if (infants != null && infants.size() != 0) {
			Iterator<ReservationPax> itReservationPax = reservationPax.getInfants().iterator();
			ReservationPax infant;
			while (itReservationPax.hasNext()) {
				infant = itReservationPax.next();
				changes.append(" / " + BeanUtils.nullHandler(infant.getTitle()));
				changes.append(", " + BeanUtils.nullHandler(infant.getFirstName()));
				changes.append(", " + BeanUtils.nullHandler(infant.getLastName()));
				if (infant.getDateOfBirth() != null) {
					changes.append(" ," + AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.DATE_OF_BIRITH + " :"
							+ smpdtDDMMYYY.format(infant.getDateOfBirth()));
				}

				String ssrInfData = ReservationSSRUtil.getPaxSSRForAudit(reservationPax);

				if (!BeanUtils.nullHandler(ssrInfData).equals("")) {
					changes.append(" ," + AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.SSR + " :" + ssrInfData);
				}

				changes.append(" - " + infant.getPaxTypeDescription());
			}
		}

		return changes.toString();
	}

	public static String getCabinClassFromPaxFare(ReservationPaxFare reservationPaxFare) throws ModuleException {
		String cabinClassCode = null;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;
		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegment.next();
			cabinClassCode = reservationPaxFareSegment.getSegment().getCabinClassCode();
			break;
		}
		return cabinClassCode;
	}

	/**
	 * Find out any non expired segment(s) exist or not
	 * 
	 * @param reservationPaxFare
	 * @return
	 */
	public static boolean isAnyOpenSegExists(ReservationPaxFare reservationPaxFare) {
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> colPnrSegIds = new ArrayList<Integer>();
		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegment.next();
			colPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
		}

		Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationPaxFare.getReservationPax().getReservation()
				.getSegmentsView();
		Iterator<ReservationSegmentDTO> itColReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO;
		boolean openSegmentsExists = false;
		// Date currentDate = new Date();

		Map<Integer, Collection<Integer>> paxWiseFlown = CancellationUtils
				.getPaxWiseFlownPnrSegments(reservationPaxFare.getReservationPax().getReservation());
		Collection<Integer> flownSegs = paxWiseFlown.get(reservationPaxFare.getReservationPax().getPnrPaxId());
		while (itColReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itColReservationSegmentDTO.next();

			// Exclude the flown and the current (cancelling) pnr segments
			if (!flownSegs.contains(reservationSegmentDTO.getPnrSegId())
					&& !colPnrSegIds.contains(reservationSegmentDTO.getPnrSegId())
					&& !ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {

				// if (currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) < 0) {
				openSegmentsExists = true;
				break;
				// }
			}
		}

		return openSegmentsExists;
	}

	/**
	 * Checks the reservation version compatibility
	 * 
	 * @param reservation
	 * @param version
	 * @throws ModuleException
	 */
	public static void checkReservationVersionCompatibility(Reservation reservation, long version) throws ModuleException {
		if (reservation.getVersion() != version) {
			throw new ModuleException("airreservations.arg.concurrentUpdate");
		}
	}

	/**
	 * Returns the ReservationPaxFare instance which belongs to the pnr segment id(s) in ReservationPaxFareSegment
	 * 
	 * @param pnrPaxFares
	 * @param pnrSegmentIds
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationPaxFare getPnrPaxFare(Set<ReservationPaxFare> pnrPaxFares, Collection<Integer> pnrSegmentIds)
			throws ModuleException {
		Iterator<ReservationPaxFare> itrPaxFare = pnrPaxFares.iterator();
		Iterator<ReservationPaxFareSegment> itrPaxFareSeg;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFare reservationPaxFareLocated = null;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> currentSegIds;

		while (itrPaxFare.hasNext()) {
			reservationPaxFare = itrPaxFare.next();
			itrPaxFareSeg = reservationPaxFare.getPaxFareSegments().iterator();

			currentSegIds = new ArrayList<Integer>();

			while (itrPaxFareSeg.hasNext()) {
				reservationPaxFareSegment = itrPaxFareSeg.next();

				if (reservationPaxFareSegment.getPnrSegId() != null) {
					currentSegIds.add(reservationPaxFareSegment.getPnrSegId());
				}
			}

			// If exact record is found.
			if (pnrSegmentIds.size() == currentSegIds.size() && pnrSegmentIds.containsAll(currentSegIds)) {
				reservationPaxFareLocated = reservationPaxFare;
				break;
			}
		}

		// If this happens it means front end had sent invalid segment ids
		// so why not throw an exception to them :-)
		if (reservationPaxFareLocated == null) {
			throw new ModuleException("airreservations.arg.invalidOnd");
		}

		return reservationPaxFareLocated;
	}

	/**
	 * Returns the ReservationPaxFare instance which belongs to the pnr segment id(s) in ReservationPaxFareSegment
	 * 
	 * @param pnrPaxFares
	 * @param pnrSegmentIds
	 * @return
	 * @throws ModuleException
	 */
	public static List<ReservationPaxFare> getPnrPaxFares(Set<ReservationPaxFare> pnrPaxFares, Collection<Integer> pnrSegmentIds)
			throws ModuleException {
		Iterator<ReservationPaxFare> itrPaxFare = pnrPaxFares.iterator();
		Iterator<ReservationPaxFareSegment> itrPaxFareSeg;
		ReservationPaxFare reservationPaxFare;
		List<ReservationPaxFare> reservationPaxFaresLocated = new ArrayList<ReservationPaxFare>();
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> currentSegIds;

		Collection<Integer> targetPnrSegmentIds = new ArrayList<Integer>(pnrSegmentIds);
		while (itrPaxFare.hasNext()) {
			reservationPaxFare = itrPaxFare.next();
			itrPaxFareSeg = reservationPaxFare.getPaxFareSegments().iterator();

			currentSegIds = new ArrayList<Integer>();

			while (itrPaxFareSeg.hasNext()) {
				reservationPaxFareSegment = itrPaxFareSeg.next();

				if (reservationPaxFareSegment.getPnrSegId() != null) {
					currentSegIds.add(reservationPaxFareSegment.getPnrSegId());
				}
			}

			if (targetPnrSegmentIds.containsAll(currentSegIds)) {
				reservationPaxFaresLocated.add(reservationPaxFare);
				targetPnrSegmentIds.removeAll(currentSegIds);
			}

			if (targetPnrSegmentIds.size() == 0) {
				break;
			}
		}

		// If this happens it means front end had sent invalid segment ids
		// so why not throw an exception to them :-)
		if (targetPnrSegmentIds.size() > 0 || reservationPaxFaresLocated.size() == 0) {
			throw new ModuleException("airreservations.arg.invalidOnd");
		}

		return reservationPaxFaresLocated;
	}

	/**
	 * Return Segment Information
	 * 
	 * @param pnrSegmentIds
	 * @return
	 */
	public static String getSegmentInformation(Collection<Integer> pnrSegmentIds) {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Collect the reservation segment information
		Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationSegmentDAO.getSegmentInformation(pnrSegmentIds);
		Iterator<ReservationSegmentDTO> itColReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		StringBuilder changes = new StringBuilder();

		while (itColReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itColReservationSegmentDTO.next();

			changes.append(" Flight No : " + reservationSegmentDTO.getFlightNo());
			changes.append(" Segment Code : " + reservationSegmentDTO.getSegmentCode());
			changes.append(" Departure Date : " + reservationSegmentDTO.getDepartureStringDate("E, dd MMM yyyy HH:mm:ss"));
		}

		return changes.toString();
	}

	/**
	 * Return Pnr Segment Ids
	 * 
	 * @param reservationPaxFare
	 * @return
	 */
	public static Collection<Integer> getPnrSegIds(ReservationPaxFare reservationPaxFare) {
		Collection<Integer> colPnrSegIds = new HashSet<Integer>();
		ReservationPaxFareSegment reservationPaxFareSegment;
		for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
			reservationPaxFareSegment = reservationPaxFareSegment2;
			colPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
		}

		return colPnrSegIds;
	}

	/**
	 * Is Credit Card Payment Passenger
	 * 
	 * @param reservationPax
	 * @param paymentAssembler
	 * @return
	 */
	// private static boolean isCCPayingPassenger(ReservationPax reservationPax, PaymentAssembler paymentAssembler) {
	// // If not an infant
	// if (!ReservationApiUtils.isInfantType(reservationPax)) {
	// return isAnyCCPaymentExist(paymentAssembler);
	// }
	//
	// return false;
	// }

	/**
	 * Is Any CC Payment Exist
	 * 
	 * @param paymentAssembler
	 * @return
	 */
	// private static boolean isAnyCCPaymentExist(PaymentAssembler paymentAssembler) {
	// Iterator<PaymentInfo> itPaymentInfo = paymentAssembler.getPayments().iterator();
	// CardPaymentInfo cardPaymentInfo;
	// PaymentInfo paymentInfo;
	//
	// while (itPaymentInfo.hasNext()) {
	// paymentInfo = (PaymentInfo) itPaymentInfo.next();
	//
	// // For Card Payment
	// if (paymentInfo instanceof CardPaymentInfo) {
	// cardPaymentInfo = (CardPaymentInfo) paymentInfo;
	//
	// if (cardPaymentInfo.getTotalAmount().doubleValue() > 0) {
	// return true;
	// }
	// }
	// }
	//
	// return false;
	// }

	/**
	 * Returns the first departure date's confirmed segment
	 * 
	 * @param lstReservationSegmentDTO
	 * @return
	 */
	public static ReservationSegmentDTO getFirstConfirmedSegment(List<ReservationSegmentDTO> lstReservationSegmentDTO) {
		ReservationSegmentDTO reservationSegmentDTO = null;

		if (lstReservationSegmentDTO != null) {
			Collections.sort(lstReservationSegmentDTO);
			Iterator<ReservationSegmentDTO> iter = lstReservationSegmentDTO.iterator();
			while (iter.hasNext()) {
				reservationSegmentDTO = iter.next();
				if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					break;
				} else {
					reservationSegmentDTO = null;
				}
			}
		}

		return reservationSegmentDTO;
	}

	/**
	 * Check if it's valid payments or not
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static void checkValidPaxPaymentsForReservation(Reservation reservation, Map pnrPaxIdAndPayments,
			Boolean isFlexiSelected) throws ModuleException {
		if (pnrPaxIdAndPayments != null && pnrPaxIdAndPayments.size() > 0) {
			Collection<Integer> colPnrPaxIds = new HashSet<Integer>();
			ReservationPax reservationPax;
			for (ReservationPax reservationPax2 : reservation.getPassengers()) {
				reservationPax = reservationPax2;

				if ((isFlexiSelected != null && isFlexiSelected.booleanValue())
						|| (reservation.isInfantPaymentRecordedWithInfant()
								|| !ReservationApiUtils.isInfantType(reservationPax))) {
					colPnrPaxIds.add(reservationPax.getPnrPaxId());
				}
			}

			// Clients could send payment information to the same number of
			// passengers in the reservation or
			// less than number of passengers in the reservation.
			if (pnrPaxIdAndPayments.keySet().size() <= colPnrPaxIds.size()) {
				if (!colPnrPaxIds.containsAll(pnrPaxIdAndPayments.keySet())) {
					throw new ModuleException("airreservations.arg.invalidPaxIdsForReservationPayments");
				}
			} else {
				throw new ModuleException("airreservations.arg.invalidPaxIdsForReservationPayments");
			}
		}
	}

	/**
	 * Returns the reservation pax fare
	 * 
	 * @param pnrPaxFares
	 * @param flightSegId
	 * @param skipExistingFlightSegment
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationPaxFare getReservationPaxFare(Collection<ReservationPaxFare> pnrPaxFares, Integer flightSegId,
			boolean skipExistingFlightSegment) throws ModuleException {
		Iterator<ReservationPaxFare> itReservationPaxFare = pnrPaxFares.iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				if ((reservationPaxFareSegment.getSegment().getFlightSegId().intValue() == flightSegId.intValue())
						&& (!skipExistingFlightSegment
								|| (skipExistingFlightSegment && reservationPaxFare.getPnrPaxFareId() == null))) {
					return reservationPaxFare;
				}
			}
		}

		throw new ModuleException("airreservations.seatmap.flightsegments.cannot.locate");
	}

	/**
	 * Returns the reservation pax fare segment
	 * 
	 * @param pnrPaxFares
	 * @param flightSegId
	 * @param pnrSegId
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationPaxFareSegment getReservationPaxFareSegment(Collection<ReservationPaxFare> pnrPaxFares,
			Integer flightSegId, Integer pnrSegId) throws ModuleException {

		if (flightSegId == null || pnrSegId == null) {
			throw new ModuleException("airreservations.seatmap.flightsegments.cannot.locate");
		}

		Iterator<ReservationPaxFare> itReservationPaxFare = pnrPaxFares.iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				if (reservationPaxFareSegment.getSegment().getPnrSegId().intValue() == pnrSegId.intValue()
						&& reservationPaxFareSegment.getSegment().getFlightSegId().intValue() == flightSegId.intValue()) {
					return reservationPaxFareSegment;
				}
			}
		}

		throw new ModuleException("airreservations.seatmap.flightsegments.cannot.locate");
	}

	/**
	 * Returns maximum ond group id
	 * 
	 * @param reservationSegments
	 */
	public static int getMaxOndGroupId(Set<ReservationSegment> reservationSegments) {
		int ondGroupId = 0;
		for (ReservationSegment reservationSegment : reservationSegments) {
			if (ondGroupId < reservationSegment.getOndGroupId().intValue()) {
				ondGroupId = reservationSegment.getOndGroupId().intValue();
			}
		}
		return ondGroupId;
	}

	/**
	 * 
	 * @param reservationSegments
	 * @return
	 */
	public static int getMaxReturnOndGroupId(Set<ReservationSegment> reservationSegments) {
		int returnOndGroupId = 0;
		for (ReservationSegment reservationSegment : reservationSegments) {
			if (reservationSegment.getReturnOndGroupId() != null
					&& returnOndGroupId < reservationSegment.getReturnOndGroupId().intValue()) {
				returnOndGroupId = reservationSegment.getReturnOndGroupId().intValue();
			}
		}
		return returnOndGroupId;
	}

	/**
	 * @param reservationSegments
	 * @return
	 */
	public static int getMaxSegmentSequence(Set<ReservationSegment> reservationSegments) {
		int segmentSeq = 0;
		for (ReservationSegment reservationSegment : reservationSegments) {
			if (segmentSeq < reservationSegment.getSegmentSeq().intValue()) {
				segmentSeq = reservationSegment.getSegmentSeq().intValue();
			}
		}

		int maxSegmentSeq = segmentSeq + 1;
		return maxSegmentSeq;
	}

	/**
	 * @param reservationSegments
	 * @return
	 */
	public static int getMaxJourneySequence(Set<ReservationSegment> reservationSegments) {
		int journeySeq = 0;
		for (ReservationSegment reservationSegment : (Set<ReservationSegment>) reservationSegments) {
			if (journeySeq < reservationSegment.getJourneySequence().intValue()) {
				journeySeq = reservationSegment.getJourneySequence().intValue();
			}
		}

		int maxJourneySeq = journeySeq + 1;
		return maxJourneySeq;
	}

	private static String getPayModeDescription(PaymentReferenceTO paymentReferenceTO) {
		StringBuilder strPayModeDesc = new StringBuilder();

		Map<Integer, String> mapActualPayModes = globalConfig.getActualPayModes();
		if (mapActualPayModes != null && !mapActualPayModes.isEmpty() && paymentReferenceTO != null) {
			strPayModeDesc.append((paymentReferenceTO.getActualPaymentMode() != null
					&& mapActualPayModes.get(paymentReferenceTO.getActualPaymentMode()) != null
							? mapActualPayModes.get(paymentReferenceTO.getActualPaymentMode()) + "- "
							: ""));
			strPayModeDesc.append((paymentReferenceTO.getPaymentRef() != null && !paymentReferenceTO.getPaymentRef().equals("")
					? paymentReferenceTO.getPaymentRef()
					: ""));
		}
		return strPayModeDesc.toString();
	}

	public static BigDecimal getTotalExternalCharge(ReservationPax reservationPax) {
		BigDecimal totalExternalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		PaymentAssembler assembler = (PaymentAssembler) reservationPax.getPayment();
		// Collects only external chage here
		if (assembler != null) {
			Collection<ExternalChgDTO> flexiCharges = assembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);
			for (ExternalChgDTO flexiCharge : flexiCharges) {
				totalExternalCharge = AccelAeroCalculator.add(totalExternalCharge, flexiCharge.getAmount());
			}
		}
		return totalExternalCharge;
	}

	public static void updateSegmentsAncillaryStatus(Reservation reservation) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;

		List<PassengerSeating> seatsList = (List<PassengerSeating>) reservationDAO
				.getReservationPaxSeatsList(reservation.getPnr());
		List<PassengerMeal> mealsList = (List<PassengerMeal>) reservationDAO.getReservationPaxMealsList(reservation.getPnr());
		List<PassengerBaggage> baggagesList = (List<PassengerBaggage>) reservationDAO
				.getReservationPaxBaggagesList(reservation.getPnr());
		Map<Integer, LinkedList<Integer>> flightSegmentsMap = getFlightSegmentMap(reservation);

		LinkedList<Integer> pnrSegList = new LinkedList<Integer>();
		boolean mealSelected = false;
		boolean seatSelected = false;
		boolean baggageSelected = false;
		int mealSelectedCount = 0;
		int seatSelectedCount = 0;
		int baggageSelectedCount = 0;
		int adultPaxCount = 0;
		String mealSelectionStatus = "NONE";
		String baggageSelectionStatus = "NONE";
		String seatSelectionStatus = "NONE";

		// Update SEAT_STATUS IN T_PNR_SEGMENT TABLE
		for (Integer flightSegId : flightSegmentsMap.keySet()) {
			pnrSegList = flightSegmentsMap.get(flightSegId);

			for (Integer pnrSegmentId : pnrSegList) {
				// Iterate each passanger
				Iterator<ReservationPax> itrResertationPax = reservation.getPassengers().iterator();
				while (itrResertationPax.hasNext()) {
					ReservationPax passanger = itrResertationPax.next();
					Integer paxID = passanger.getPnrPaxId();

					if (!passanger.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {

						adultPaxCount++;

						for (PassengerSeating paxSeating : seatsList) {
							if (paxSeating.getPnrPaxId().equals(paxID)) {
								if (paxSeating.getPnrSegId().equals(pnrSegmentId)) {
									seatSelected = true;
									break;
								}
							}
						}

						for (PassengerMeal paxMeal : mealsList) {
							if (paxMeal.getPnrPaxId().equals(paxID)) {
								if (paxMeal.getPnrSegId().equals(pnrSegmentId)) {
									mealSelected = true;
									break;
								}
							}
						}

						for (PassengerBaggage paxBaggage : baggagesList) {
							if (paxBaggage.getPnrPaxId().equals(paxID)) {
								if (paxBaggage.getPnrSegmentId().equals(pnrSegmentId)) {
									baggageSelected = true;
									break;
								}
							}
						}

						if (seatSelected) {
							seatSelectedCount++;
							seatSelected = false;
						}

						if (mealSelected) {
							mealSelectedCount++;
							mealSelected = false;
						}

						if (baggageSelected) {
							baggageSelectedCount++;
							baggageSelected = false;
						}
					}
				}

				if (seatSelectedCount == 0) {
					seatSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.NONE;
				} else if (seatSelectedCount < (pnrSegList.size() * adultPaxCount)) {
					seatSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.PARTIAL;
				} else if (seatSelectedCount == (pnrSegList.size() * adultPaxCount)) {
					seatSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.ALL;
				}

				if (mealSelectedCount == 0) {
					mealSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.NONE;
				} else if (mealSelectedCount < (pnrSegList.size() * adultPaxCount)) {
					mealSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.PARTIAL;
				} else if (mealSelectedCount == (pnrSegList.size() * adultPaxCount)) {
					mealSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.ALL;
				}

				if (baggageSelectedCount == 0) {
					baggageSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.NONE;
				} else if (baggageSelectedCount < (pnrSegList.size() * adultPaxCount)) {
					baggageSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.PARTIAL;
				} else if (baggageSelectedCount == (pnrSegList.size() * adultPaxCount)) {
					baggageSelectionStatus = ReservationInternalConstants.SegmentAncillaryStatus.ALL;
				}

				// MARK Status update in T_PNR_SEGMENT
				ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
				ReservationSegment reservationSegment = reservationSegmentDAO.getReservationSegment(pnrSegmentId);
				reservationSegment.setSeatSelectionStatus(seatSelectionStatus);
				reservationSegment.setMealSelectionStatus(mealSelectionStatus);
				reservationSegment.setBaggageSelectionStatus(baggageSelectionStatus);
				reservationSegmentDAO.saveReservationSegment(reservationSegment);

				seatSelectedCount = 0;
				mealSelectedCount = 0;
				baggageSelectedCount = 0;
				adultPaxCount = 0;
			}

		}
	}

	/**
	 * Returns flight segment map to the pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private static Map<Integer, LinkedList<Integer>> getFlightSegmentMap(Reservation reservation) {
		Map<Integer, LinkedList<Integer>> flgtSegmentMap = new HashMap<Integer, LinkedList<Integer>>();

		ReservationSegment reservationSegment;
		LinkedList<Integer> pnrSegIds;
		for (ReservationSegment reservationSegment2 : reservation.getSegments()) {
			reservationSegment = reservationSegment2;

			pnrSegIds = flgtSegmentMap.get(reservationSegment.getFlightSegId());

			if (pnrSegIds == null) {
				pnrSegIds = new LinkedList<Integer>();
				pnrSegIds.add(reservationSegment.getPnrSegId());

				flgtSegmentMap.put(reservationSegment.getFlightSegId(), pnrSegIds);
			} else {
				pnrSegIds.add(reservationSegment.getPnrSegId());
			}
		}

		return flgtSegmentMap;
	}

	public static ReservationSegmentDTO[] getOutboundInboundList(List<ReservationSegmentDTO> reservationSegmentDTOs) {
		Collections.sort(reservationSegmentDTOs);
		List<ReservationSegmentDTO> cloneReservationSegmentDTOs = new ArrayList<ReservationSegmentDTO>();
		List<ReservationSegmentDTO> outboundReservationSegmentDTOs = new ArrayList<ReservationSegmentDTO>();
		List<ReservationSegmentDTO> inboundReservationSegmentDTOs = new ArrayList<ReservationSegmentDTO>();

		for (ReservationSegmentDTO reservationSegmentDTO : reservationSegmentDTOs) {
			if (!reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				ReservationSegmentDTO cloneReservationSegmentDTO = getTempReservationSegmentDTO(reservationSegmentDTO);
				cloneReservationSegmentDTOs.add(cloneReservationSegmentDTO);
				outboundReservationSegmentDTOs.add(cloneReservationSegmentDTO);
			}
		}

		for (ReservationSegmentDTO outBoundReservationSegmentDTO : cloneReservationSegmentDTOs) {
			if (outBoundReservationSegmentDTO.getZuluDepartureDate().after(Calendar.getInstance().getTime())) {
				for (ReservationSegmentDTO inBoundReservationSegmentDTO : cloneReservationSegmentDTOs) {
					if (inBoundReservationSegmentDTO.getZuluDepartureDate().after(Calendar.getInstance().getTime())
							&& !outBoundReservationSegmentDTO.getSegmentCode()
									.equals(inBoundReservationSegmentDTO.getSegmentCode())) {
						String[] outbound = outBoundReservationSegmentDTO.getSegmentCode().split("/");
						String[] inbound = inBoundReservationSegmentDTO.getSegmentCode().split("/");
						if (outbound[0].equals(inbound[inbound.length - 1])) {
							inboundReservationSegmentDTOs.add(inBoundReservationSegmentDTO);
							outboundReservationSegmentDTOs.remove(outBoundReservationSegmentDTO);
						}
					}
				}
			}
			break;
		}
		Collections.sort(outboundReservationSegmentDTOs);
		Collections.sort(inboundReservationSegmentDTOs);
		return new ReservationSegmentDTO[] { outboundReservationSegmentDTOs.get(0),
				inboundReservationSegmentDTOs.size() == 0
						? outboundReservationSegmentDTOs.get(outboundReservationSegmentDTOs.size() - 1)
						: inboundReservationSegmentDTOs.get(inboundReservationSegmentDTOs.size() - 1) };
	}

	private static ReservationSegmentDTO getTempReservationSegmentDTO(ReservationSegmentDTO reservationSegmentDTO) {
		ReservationSegmentDTO cloneReservationSegmentDTO = new ReservationSegmentDTO();
		cloneReservationSegmentDTO.setZuluArrivalDate(reservationSegmentDTO.getArrivalDate());
		cloneReservationSegmentDTO.setZuluDepartureDate(reservationSegmentDTO.getZuluDepartureDate());
		cloneReservationSegmentDTO.setSegmentCode(reservationSegmentDTO.getSegmentCode());
		return cloneReservationSegmentDTO;
	}

	public static ReservationPaxOndCharge getOndFareFromOndCharges(Collection<ReservationPaxOndCharge> paxOndCharges) {
		for (ReservationPaxOndCharge reservationPaxOndCharge : paxOndCharges) {
			if (reservationPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
				return reservationPaxOndCharge;
			}
		}
		return null;
	}

	/**
	 * Return Segment Information
	 * 
	 * @param pnrSegmentIds
	 * @return
	 */
	public static String getSegmentInformationWithCabinClassDetails(Collection<Integer> pnrSegmentIds, String cabinClass)
			throws ModuleException {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Collect the reservation segment information
		Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationSegmentDAO.getSegmentInformation(pnrSegmentIds);
		Iterator<ReservationSegmentDTO> itColReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		StringBuilder changes = new StringBuilder();

		while (itColReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itColReservationSegmentDTO.next();

			changes.append(" Flight No : " + reservationSegmentDTO.getFlightNo());
			changes.append(" Segment Code : " + reservationSegmentDTO.getSegmentCode());
			changes.append(" Departure Date : " + reservationSegmentDTO.getDepartureStringDate("E, dd MMM yyyy HH:mm:ss"));
			changes.append(" Cabin Class : " + cabinClass);
		}

		return changes.toString();
	}

	public static void updateGrpBookingPaymentStatus(Reservation reservation, Map<Integer, IPayment> paxIdsWithPayments) {

		if (reservation.getGroupBookingRequestID() != null) {
			Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();

			ReservationPax reservationPax;
			Map<Integer, LinkedList<Integer>> perPaxFlightSegmentMap;
			BigDecimal totalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();

				if (!ReservationApiUtils.isInfantType(reservationPax)
						&& paxIdsWithPayments.keySet().contains(reservationPax.getPnrPaxId())) {
					PaymentAssembler payment = (PaymentAssembler) paxIdsWithPayments.get(reservationPax.getPnrPaxId());
					totalPayment = totalPayment.add(payment.getTotalPayAmount());
					// Sending the pnr passenger id and the IPayment information per passenger

				}
			}

			ReservationDAOUtils.DAOInstance.GrpBookingDAO.updateBookingPaymentStatus(reservation.getGroupBookingRequestID(),
					totalPayment);
		}
	}

	public static Set<String> getCSOCCarrierCodes(Reservation reservation) {
		Set<String> csOCCarrirs = new HashSet<String>();
		if (reservation.getSegmentsView() != null) {
			for (ReservationSegmentDTO resSegment : reservation.getSegmentsView()) {
				if (!resSegment.getSubStatus().equals(ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED)
						&& resSegment.getCsOcCarrierCode() != null) {
					csOCCarrirs.add(resSegment.getCsOcCarrierCode());
				}
			}
		} else if (reservation.getSegments() != null) {
			for (ReservationSegment resSegment : reservation.getSegments()) {
				if (resSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& resSegment.getCsOcCarrierCode() != null) {
					csOCCarrirs.add(resSegment.getCsOcCarrierCode());
				}
			}
		}
		return csOCCarrirs.isEmpty() ? null : csOCCarrirs;
	}

	public static Set<String> getCSOCCarrierCodesInConfirmedSegments(Reservation reservation) {
		Set<String> csOCCarrirs = new HashSet<String>();
		if (reservation.getSegmentsView() != null) {
			for (ReservationSegmentDTO resSegment : reservation.getSegmentsView()) {
				if (resSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& resSegment.getCsOcCarrierCode() != null) {
					csOCCarrirs.add(resSegment.getCsOcCarrierCode());
				}
			}
		} else if (reservation.getSegments() != null) {
			for (ReservationSegment resSegment : reservation.getSegments()) {
				if (resSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& resSegment.getCsOcCarrierCode() != null) {
					csOCCarrirs.add(resSegment.getCsOcCarrierCode());
				}
			}
		}
		return csOCCarrirs.isEmpty() ? null : csOCCarrirs;
	}

	public static Set<String> getCSOCCarrierCodes(Collection<ReservationSegment> pnrSegments) {
		Set<String> csOCCarrirs = new HashSet<String>();
		if (pnrSegments != null && !pnrSegments.isEmpty()) {
			for (ReservationSegment resSegment : pnrSegments) {
				if (resSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& resSegment.getCsOcCarrierCode() != null) {
					csOCCarrirs.add(resSegment.getCsOcCarrierCode());
				}
			}
		}
		return csOCCarrirs.isEmpty() ? null : csOCCarrirs;
	}

	public static Set<String> getCSOCCarrierCodes(List<TransferSegmentDTO> transferSegmentDTOs) {
		Set<String> csOCCarrirs = new HashSet<String>();
		if (transferSegmentDTOs != null && !transferSegmentDTOs.isEmpty()) {
			for (TransferSegmentDTO transferSegmentDTO : transferSegmentDTOs) {
				if (transferSegmentDTO.getTargetFlightSegment().getCsOcCarrierCode() != null) {
					csOCCarrirs.add(transferSegmentDTO.getTargetFlightSegment().getCsOcCarrierCode());
				}
			}
		}
		return csOCCarrirs.isEmpty() ? null : csOCCarrirs;
	}

	public static void updateSegmentsAutomaticCheckinStatus(Reservation reservation) throws ModuleException {
		ReservationAutomaticCheckinDAO reservationAutomaticCheckindao = ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO;

		reservationAutomaticCheckindao.updateReservationPaxSegAutoCheckin(reservation.getPnrPaxIds());
	}
}
