/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 * 
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.adl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlight;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegment;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegmentMapElement;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassenger;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassportInformation;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSVisaInformation;
import com.isa.connectivity.profiles.dcs.v1.common.dto.PassengersMapElement;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRS;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLDestinationDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.RemarksElementDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLPaxNameInfoDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLRecordDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerCountRecordDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.PnlAdlHistory;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PNLADLElements;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PNLADLStatusCode;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.AdlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.FeaturePack;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdlDeliveryInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses.MessageResponseAdditionals;
import com.isa.thinair.airreservation.core.bl.pnl.PNLBL;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.SSRCode;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.msgbroker.api.dto.PNLADLResponseDTO;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.airreservation.core.utils.CommandNames;

/**
 * This class includes all the business logic for ADL Generations
 * 
 * @author Byorn
 */
public class ADLBL {

	/** Holds the Current Time..set by constructor * */
	private final Timestamp currentTimeStamp;

	/** Holds the current date.. set by constructor * */
	private final Date currentDate;

	/** Holds the ADL Passenger List...set in generateADL method * */
	private Collection<PassengerInformation> addedPassengerList = new ArrayList<PassengerInformation>();

	/** Generated ADL Files set in generateADL method, used for logging * */
	private ArrayList<String> generatedAdlfileNames = new ArrayList<String>();

	/** Per PNR number of pax ids * */
	private final ArrayList<Integer> paxIdsPerRecord = new ArrayList<Integer>();

	/** AuxDAO set in constructor * */
	private ReservationAuxilliaryDAO auxilliaryDAO = null;

	private final ETicketDAO eTicketDAO;

	private String msgType = null;

	/** Holds the Total Number of Passengers in the flight (i.e for all cabic classes also **/
	private HashMap<String, Integer> ccPaxMap = new HashMap<String, Integer>();

	private String lastGroupCode = new String();

	/** Holds the Total Number of Passengers in the flight (i.e for all cabic classes also **/
	private HashMap<String, String> mapPnrPaxSegIDGroupCode = new HashMap<String, String>();

	private static Log log = LogFactory.getLog(ADLBL.class);

	public static final String Email = "E";
	public static final String PNL = "PNL";
	public static final String ADL = "ADL";

	public ADLBL() {

		this.auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		this.eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		this.currentDate = new Date();
		this.currentTimeStamp = new Timestamp(currentDate.getTime());

		this.msgType = PNLConstants.ADLGeneration.ADL;
	}

	/**
	 * NOTE: --------------------------------------------------------------------------- This Operation is used by
	 * Scheduled Service Only. If any errors occurs in this operation, the eror will be thrown and data will get rolled
	 * back. This enables the Resender/ManualUI to search the ADL and can get the correct passengers (as status was not
	 * updated)
	 * 
	 * Email will be sent of the error. ---------------------------------------------------------------------------
	 * 
	 * @param flightId
	 * @param depAirportCode
	 * @param adto
	 * @return
	 * @throws ModuleException
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	public void sendADLDup(int flightId, String depAirportCode) throws ModuleException {

		// only one thread at time please
		synchronized (this) {
			// holds the flight number
			String flightNumber = "";
			// holds the exception to be logged, if occured
			ModuleException exceptiontoLog = null;
			// sitaaddresss - status("y", or "<exception desc>"
			HashMap<String, String> sitaStatusMap = new HashMap<String, String>();
			// generated files content
			String fileContent = null;
			// holds the local date of flight
			Date localDate = null;
			try {

				// There might be scenarios where flight get canceled and reprotected passengers to a new flight and
				// PNL/ADL jobs already scheduled for old flight. This is to check and validate flight id
				FlightBD fightBD = ReservationModuleUtils.getFlightBD();
				Flight flight = fightBD.getFlight(flightId);
				if (flight != null && flight.getStatus().equals(FlightStatusEnum.CANCELLED.getCode())) {
					log.info(" #####################     FLIGHT CANCELLED AND LOADING NEW FLIGHT ID : " + flightId);
					Integer actFlightId = null;
					for (FlightSegement fs : (flight.getFlightSegements())) {
						actFlightId = fightBD.getFlightID(fs.getSegmentCode(), flight.getFlightNumber(),
								fs.getEstTimeDepatureZulu());
						if (actFlightId != null) {
							flightId = actFlightId;
							log.info(" #####################     NEW FLIGHT ID TO SEND ADL : " + flightId);
							break;
						} else {
							log.info(" #####################     UNABLE TO FIND NEW FLIGHT ID AND USING OLD ONE : " + flightId);
						}
					}
				}

				flightNumber = auxilliaryDAO.getFlightNumber(flightId);
				log.debug("###################### Retrieved Flight Num : " + flightNumber + " for flight id: " + flightId
						+ " airport : " + depAirportCode);

				// Check whether PNL has been dispatched already, if not send PNL first.
				Date zuluDate = auxilliaryDAO.getFlightZuluDate(flightId, depAirportCode);
				if (!auxilliaryDAO.hasPnlHistory(flightNumber, zuluDate, depAirportCode)) {
					try {
						new PNLBL().sendPNL(flightId, depAirportCode);
						log.info("PNL dispatched for the flight because the PNL was not sent at the original schedule time "
								+ " Flight Id : " + flightId + " airport code : " + depAirportCode);
					} catch (Exception e) {
						log.error("PNL Operation Failed during retry operation For flghtId :" + flightId + " Flt Num :"
								+ flightNumber + " airport :" + depAirportCode + " Error :" + e.getMessage());

					}
				}
				// get Carrier code from flight number
				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
				
				Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
				String ond = PnlAdlUtil.getOndFromDepartureAirport(depAirportCode, legNumberWiseOndMap);

				// get a list of sita address
				List<String> sitaAddresses = auxilliaryDAO.getActiveSITAAddresses(depAirportCode.trim(), flightNumber, ond,
						carrierCode, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL).get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				log.debug("###################### Retrieved Sita List: fid: " + flightId + " airport : " + depAirportCode
						+ " Carrier code :" + carrierCode);

				// map will have sita address, and failure status.
				PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddresses, null, null);

				// check adl time exceeds flight closure time

				// log.debug("###################### Will Check if Flight is closed: fid: " + flightId + " airport : " +
				// depAirportCode);
				// checkAdlTimeExceedsFlightClosureTime(flightId);

				// get the adl passengers
				Collection<PNLADLDestinationDTO> adlc = decideNextADLDetails(flightId, depAirportCode);
				log.debug("######################After Getting the Next ADL PAX Flight Num : " + flightNumber
						+ " for flight id: " + flightId + " airport : " + depAirportCode);

				// if not passengers code will not come here
				// populate the ADLMetaDataDTO
				ADLMetaDataDTO adlmetadatadto = new ADLMetaDataDTO();
				localDate = auxilliaryDAO.getFlightLocalDate(flightId, depAirportCode);
				String[] monthDay = PnlAdlUtil.getMonthDayArray(localDate);
				adlmetadatadto.setBoardingairport(depAirportCode);
				adlmetadatadto.setDay(monthDay[1]);
				adlmetadatadto.setFlight(flightNumber);
				adlmetadatadto.setMessageIdentifier(msgType);
				adlmetadatadto.setMonth(monthDay[0]);
				adlmetadatadto.setPartnumber(PNLConstants.PNLGeneration.PART_ONE);
				adlmetadatadto.setPart2(true);
				if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
					adlmetadatadto.setRbd(PnlAdlUtil.generateRBDValues(flightId));
					adlmetadatadto.setRbdEnabled(true);
				}

				// generate the adl
				log.debug("###################### Before Generating ADL : " + flightNumber + " for flight id: " + flightId
						+ " airport : " + depAirportCode);
				generateADL(adlc, adlmetadatadto, flightId, depAirportCode);
				log.debug("###################### After Generating ADL : " + flightNumber + " for flight id: " + flightId
						+ " airport : " + depAirportCode);

				// Normal ADL sending process
				// send each generated file to each sita address, return the
				// status of the sita
				
				
				Map<String, List<String>> deliverTypeAddressMap = new HashMap<String, List<String>>();

				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddresses);
				
				sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedAdlfileNames, msgType);
				log.debug("###################### After Transmission of  ADL : " + flightNumber + " for flight id: " + flightId
						+ " airport : " + depAirportCode);

				// update the passengers adl status
				// updateAdlStatus(adlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode);
				updateAdlStatusAndGroupId(addedPassengerList, mapPnrPaxSegIDGroupCode,sitaStatusMap);
				log.debug("###################### After Updating ADL Pax Statuss    : " + flightNumber + " for flight id: "
						+ flightId + " airport : " + depAirportCode);

				// get the file content
				fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedAdlfileNames);

				// before updating pax status check if there was an error in sita map
				PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);
				log.debug("###################### After Getting ADL Content From files    : " + flightNumber + " for flight id: "
						+ flightId + " airport : " + depAirportCode);

			} catch (ModuleException e) {
				exceptiontoLog = e;

			} catch (CommonsDataAccessException e) {
				exceptiontoLog = new ModuleException(e, e.getExceptionCode());

			} catch (Exception e) {
				exceptiontoLog = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

			} finally {

				log.info("######### Before Inserting to History for PNL For FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						depAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoLog,
						PNLConstants.SendingMethod.SCHEDSERVICE, lastGroupCode);
				log.info("######### Inserted To History OK!");

				// if error has occured
				if (exceptiontoLog != null) {

					// taking extra precaution to avoid uncessary rollback of pax status
					try {
						// save an audit
						PnlAdlUtil.savePNLADLErrorLog(flightId, msgType, depAirportCode, flightNumber, currentDate,
								exceptiontoLog);

						// if pax count Record was not found, will attempt to send the pnl.
						if (exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)) {
							new PNLBL().sendPNL(flightId, depAirportCode);
						}

						// dont notify the flight closure error
						if (exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_ADL_ERROR_CODE)
								|| exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.FLIGHT_CLOSED_ADLERROR_CODE)
								|| exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)
								|| exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_SITA_ADDRESS)) {
						}// notify all other errors
						else {

							PnlAdlUtil.notifyAdminForManualAction(flightNumber, localDate, depAirportCode, exceptiontoLog,
									PNLConstants.ADLGeneration.ADL, null);

							log.error("############### ADL SS Error flghtId :" + flightId + " Flt Num :" + flightNumber
									+ " airport :" + depAirportCode, exceptiontoLog);
							throw exceptiontoLog;
						}
					} catch (Throwable e) {
						if (e.equals(exceptiontoLog)) {
							throw exceptiontoLog;
						} else {
							log.error("Error in Finaly Method of sendADL when logging errors or manually sending ", e);
						}
					}

				}

				// No error occured move to sent path
				PnlAdlUtil.moveGeneratedFiles(this.generatedAdlfileNames, msgType);
				log.info("ADL Operation Success For flghtId :" + flightId + " Flt Num :" + flightNumber + " airport :"
						+ depAirportCode);
			}
			log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- " + depAirportCode
					+ " Flight Num : " + flightNumber);
		}
	}

	/**
	 * NOTE: --------------------------------------------------------------------------- This Operation is used by
	 * Scheduled Service Only. If any errors occurs in this operation, the eror will be thrown and data will get rolled
	 * back. This enables the Resender/ManualUI to search the ADL and can get the correct passengers (as status was not
	 * updated)
	 * 
	 * Email will be sent of the error. ---------------------------------------------------------------------------
	 * 
	 * @param flightId
	 * @param depAirportCode
	 * @param adto
	 * @return
	 * @throws ModuleException
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings("unchecked")
	public void sendADLNewDup(int flightId, String depAirportCode) throws ModuleException {

		// only one thread at time please
		synchronized (this) {
			// holds the flight number
			String flightNumber = "";
			// holds the exception to be logged, if occured
			ModuleException exceptiontoLog = null;
			// sitaaddresss - status("y", or "<exception desc>"
			HashMap<String, String> sitaStatusMap = new HashMap<String, String>();
			// generated files content
			String fileContent = null;
			// holds the local date of flight
			Date localDate = null;

			boolean saveWSADLHistory = false;
			try {

				flightNumber = auxilliaryDAO.getFlightNumber(flightId);
				log.debug("###################### Retrieved Flight Num : " + flightNumber + " for flight id: " + flightId
						+ " airport : " + depAirportCode);

				// Check whether PNL has been dispatched already, if not send PNL first.
				Date zuluDate = auxilliaryDAO.getFlightZuluDate(flightId, depAirportCode);
				if (!auxilliaryDAO.hasPnlHistory(flightNumber, zuluDate, depAirportCode)) {
					try {
						new PNLBL().sendPNL(flightId, depAirportCode);
					} catch (Exception e) {
						log.error("PNL Operation Failed during retry operation For flghtId :" + flightId + " Flt Num :"
								+ flightNumber + " airport :" + depAirportCode + " Error :" + e.getMessage());

					}
				}

				// get Carrier code from flight number
				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);

				Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
				String ond = PnlAdlUtil.getOndFromDepartureAirport(depAirportCode, legNumberWiseOndMap);
				
				// get a list of sita address
				Map<String, List<String>> sitaAddressesMap = auxilliaryDAO.getActiveSITAAddresses(depAirportCode.trim(),
						flightNumber, ond, carrierCode, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
				List<String> sitaAddressesList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				List<String> sitaTexAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
				List<String> arincAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
				List<String> sitaAddressAll = new ArrayList<String>();
				sitaAddressAll.addAll(sitaAddressesList);
				sitaAddressAll.addAll(sitaTexAddressList);
				sitaAddressAll.addAll(arincAddresses);
				log.debug("###################### Retrieved Sita List: fid: " + flightId + " airport : " + depAirportCode
						+ " Carrier code :" + carrierCode);

				// map will have sita address, and failure status.
				PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddressAll, null, arincAddresses);

				ADLDTOCreator adlDTOcreator = new ADLDTOCreator();
				ADLDTO adlElements = adlDTOcreator.generateADLElements(flightId, depAirportCode);
				PNLADLResponseDTO response = ReservationModuleUtils.getPNLADLServiceBD().sendADL(adlElements);

				ccPaxMap = response.getCcPaxMap();
				lastGroupCode = response.getLastGroupId();
				generatedAdlfileNames = response.getFileNames();
				mapPnrPaxSegIDGroupCode = response.getPnrPaxSegIDGroupCode();
				addedPassengerList = response.getAddedPassengerList();
				if (response.isWsAdlSuccess()) {
					saveWSADLHistory = true;
				}
				PnlAdlUtil.updatePnlPassengerStatusFromPnlPaxIds(adlElements.getUniquePnlPaxIds());

				// Normal ADL sending process
				// send each generated file to each sita address, return the
				// status of the sita
				Map<String, List<String>> deliverTypeAddressMap = new HashMap<String, List<String>>();

				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddressesList);
				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, arincAddresses);
				
				sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedAdlfileNames, msgType);
				sitaStatusMap = PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddressList, this.generatedAdlfileNames, msgType,
						sitaStatusMap, null);
				log.debug("###################### After Transmission of  ADL : " + flightNumber + " for flight id: " + flightId
						+ " airport : " + depAirportCode);

				if(PnlAdlUtil.isFailedForAllEmails(sitaStatusMap)){
					PnlAdlUtil.updatePnlPassengerStatusFromPnlPaxIds(adlElements.getUniquePnlPaxIds());
				}else{
					ReservationModuleUtils.getReservationAuxilliaryBD().updatePnlPassengers(adlElements.getUniquePnlPaxIds());
				}
				
				// update the passengers adl status
				// updateAdlStatus(adlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode);
				updateAdlStatusAndGroupId(addedPassengerList, mapPnrPaxSegIDGroupCode,sitaStatusMap);
				log.debug("###################### After Updating ADL Pax Statuss    : " + flightNumber + " for flight id: "
						+ flightId + " airport : " + depAirportCode);

				// get the file content
				fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedAdlfileNames);

				// before updating pax status check if there was an error in sita map
				PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);
				log.debug("###################### After Getting ADL Content From files    : " + flightNumber + " for flight id: "
						+ flightId + " airport : " + depAirportCode);

			} catch (ModuleException e) {
				exceptiontoLog = e;

			} catch (CommonsDataAccessException e) {
				exceptiontoLog = new ModuleException(e, e.getExceptionCode());

			} catch (Exception e) {
				exceptiontoLog = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

			} finally {

				log.info("######### Before Inserting to History for PNL For FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						depAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoLog,
						PNLConstants.SendingMethod.SCHEDSERVICE, lastGroupCode);

				// Insert WS ADL history
				if (saveWSADLHistory) {
					PnlAdlUtil.saveWSADLHistory(depAirportCode, flightId, currentTimeStamp,"Y");
				}
				log.info("######### Inserted To History OK!");

				// if error has occured
				if (exceptiontoLog != null) {

					// taking extra precaution to avoid uncessary rollback of pax status
					try {
						// save an audit
						PnlAdlUtil.savePNLADLErrorLog(flightId, msgType, depAirportCode, flightNumber, currentDate,
								exceptiontoLog);

						// if pax count Record was not found, will attempt to send the pnl.
						if (exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)) {
							new PNLBL().sendPNL(flightId, depAirportCode);
						}

						// dont notify the flight closure error
						if (exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_ADL_ERROR_CODE)
								|| exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.FLIGHT_CLOSED_ADLERROR_CODE)
								|| exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)
								|| exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_SITA_ADDRESS)) {
						}// notify all other errors
						else {

							PnlAdlUtil.notifyAdminForManualAction(flightNumber, localDate, depAirportCode, exceptiontoLog,
									PNLConstants.ADLGeneration.ADL, null);

							log.error("############### ADL SS Error flghtId :" + flightId + " Flt Num :" + flightNumber
									+ " airport :" + depAirportCode, exceptiontoLog);
							throw exceptiontoLog;
						}
					} catch (Throwable e) {
						if (e.equals(exceptiontoLog)) {
							throw exceptiontoLog;
						} else {
							log.error("Error in Finaly Method of sendADL when logging errors or manually sending ", e);
						}
					}

				}

				// No error occured move to sent path
				PnlAdlUtil.moveGeneratedFiles(this.generatedAdlfileNames, msgType);
				log.info("ADL Operation Success For flghtId :" + flightId + " Flt Num :" + flightNumber + " airport :"
						+ depAirportCode);
			}
			log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- " + depAirportCode
					+ " Flight Num : " + flightNumber);
		}
	}

	/**
	 * NOTE: --------------------------------------------------------------------------- This Operation is used by the
	 * Manual UI / Resender Job! If any errors occurs in this operation, the eror will be thrown and data will get
	 * rolled back.
	 * 
	 * Email will be sent of the error. ---------------------------------------------------------------------------
	 * 
	 * @param flightNumber
	 * @param depAirportCode
	 * @param dateOfflightLocal
	 * @param sitaaddresses
	 * @param mailserver
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	public void sendADLAuxiliary(String flightNumber, String depAirportCode, Date dateOfflightLocal, String sitaaddresses[],
			String sendingMethod) throws ModuleException {
		// only one thread at time please
		synchronized (this) {

			boolean isFlightsAvailableForManulPNL = false;
			// holds the exception to be logged, if occured
			ModuleException exceptiontoThrow = null;
			// sitaaddresss - status("y", or "<exception desc>"
			HashMap<String, String> sitaStatusMap = new HashMap<String, String>();
			// generated files content
			String fileContent = null;

			// presently from the front end the sita list is sent by an aaray,
			// need a list.
			List<String> sitas = PnlAdlUtil.composeSitaAddresses(sitaaddresses);

			// map will have sita address, and failure status.

			PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitas, null,null);

			// get the flight id
			int flightId = auxilliaryDAO.getFlightID(flightNumber, depAirportCode, dateOfflightLocal);

			if (flightId == -1) {
				isFlightsAvailableForManulPNL = true;
				throw new ModuleException("airreservations.auxilliary.invalidflight", "airreservations");
			}
			// clear the temp folders
			// check if time sent is after the flight has been closed
			try {

				// get the adl passenger data
				Collection<PNLADLDestinationDTO> adlList = decideNextADLDetails(flightId, depAirportCode);

				// if code comes here means that there are adl passengers.

				// populate the ADLMetaDataDTO for adl generation for vel
				// template
				ADLMetaDataDTO adlmetadatadto = new ADLMetaDataDTO();
				String fnumber = auxilliaryDAO.getFlightNumber(flightId);
				// TODO since flight number is passed to the method , Do we need to reload the flight num again ????

				String[] monthDay = PnlAdlUtil.getMonthDayArray(dateOfflightLocal);
				adlmetadatadto.setMonth(monthDay[0]);
				adlmetadatadto.setDay(monthDay[1]);
				adlmetadatadto.setBoardingairport(depAirportCode);

				adlmetadatadto.setFlight(fnumber);
				adlmetadatadto.setMessageIdentifier(msgType);

				adlmetadatadto.setPartnumber(PNLConstants.ADLGeneration.PART_ONE);
				adlmetadatadto.setPart2(true);

				if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
					adlmetadatadto.setRbd(PnlAdlUtil.generateRBDValues(flightId));
					adlmetadatadto.setRbdEnabled(true);
				}

				// generate the adl.
				generateADL(adlList, adlmetadatadto, flightId, depAirportCode);

				// for each sita sent generated files content
				sitaStatusMap = PnlAdlUtil.sendMails(sitas, this.generatedAdlfileNames, msgType);

				// get the generated file content
				fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedAdlfileNames);

				// update the passsenger statuses
				// updateAdlStatus(adlCollectionTobeUpdated);

				// updateAdlStatus(adlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode);

				updateAdlStatusAndGroupId(addedPassengerList, mapPnrPaxSegIDGroupCode,sitaStatusMap);

				// if error in transmission occures roll back.
				PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);

			} catch (ModuleException exception) {

				exceptiontoThrow = exception;
				throw exceptiontoThrow;
			} catch (CommonsDataAccessException e) {

				exceptiontoThrow = new ModuleException(e, e.getExceptionCode());
				throw exceptiontoThrow;
			} catch (Exception e) {

				exceptiontoThrow = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

				throw exceptiontoThrow;

			} finally {

				log.debug("######### Before Inserting to History for ADL For FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						depAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoThrow, sendingMethod, lastGroupCode);
				log.debug("######### Inserted To History OK!");

				// if error has occured
				if (exceptiontoThrow != null) {

					// taking extra precaution to avoid uncessary rollback of pax status
					try {
						// save an audit
						PnlAdlUtil.savePNLADLErrorLog(flightId, msgType, depAirportCode, flightNumber, currentDate,
								exceptiontoThrow);

						// if pax count Record was not found, will attempt to send the pnl.
						if (exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)) {
							new PNLBL().sendPNL(flightId, depAirportCode);
						}

						// dont notify the flight closure error etc etc
						if (exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_ADL_ERROR_CODE)
								|| exceptiontoThrow.getExceptionCode().equals(
										PNLConstants.ERROR_CODES.FLIGHT_CLOSED_ADLERROR_CODE)
								|| exceptiontoThrow.getExceptionCode().equals(
										PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)
								|| exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_SITA_ADDRESS)) {
						}// notify all other errors
						else {
							String sendingOperation = PnlAdlUtil.getSendingOperationDescription(sendingMethod);

							if (!isFlightsAvailableForManulPNL) {
								PnlAdlUtil.notifyAdminForManualAction(flightNumber, dateOfflightLocal, depAirportCode,
										exceptiontoThrow, PNLConstants.ADLGeneration.ADL + " " + sendingOperation, null);

								log.error("############### ADL " + sendingOperation + "Error flghtId :" + flightId + " Flt Num :"
										+ flightNumber + " airport :" + depAirportCode, exceptiontoThrow);
							}
							// throw exception and rollback pax status's
							throw exceptiontoThrow;
						}
					} catch (Throwable e) {
						if (e.equals(exceptiontoThrow)) {
							throw exceptiontoThrow;
						} else {
							log.error("Error in Finaly Method of sendADL when logging errors or emailing errors ", e);
						}

					}

				}

				PnlAdlUtil.moveGeneratedFiles(this.generatedAdlfileNames, msgType);
			}
			log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- " + depAirportCode
					+ " Flight Num : " + flightNumber);
		}
	}

	public void sendADL(String flightNumber, String depAirportCode, Date dateOfflightLocal, String sitaaddresses[],
			String sendingMethod) throws ModuleException {
		sendADLAuxiliaryNew(flightNumber, depAirportCode, dateOfflightLocal, sitaaddresses, sendingMethod);
	}
	
	public void sendADL(int flightId, String depAirportCode) throws ModuleException {
		
		String flightNumber = "";
		Date dateOfflightLocal = null;
		
		flightNumber = auxilliaryDAO.getFlightNumber(flightId);
		log.debug("###################### Retrieved Flight Num : " + flightNumber + " for flight id: " + flightId
				+ " airport : " + depAirportCode);

		// Check whether PNL has been dispatched already, if not send PNL first.
		Date zuluDate = auxilliaryDAO.getFlightZuluDate(flightId, depAirportCode);
		if (!auxilliaryDAO.hasPnlHistory(flightNumber, zuluDate, depAirportCode)) {
			try {
				new PNLBL().sendPNL(flightId, depAirportCode);
			} catch (Exception e) {
				log.error("PNL Operation Failed during retry operation For flghtId :" + flightId + " Flt Num :"
						+ flightNumber + " airport :" + depAirportCode + " Error :" + e.getMessage());

			}
		}

		// get Carrier code from flight number
		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
		
		Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
		String ond = PnlAdlUtil.getOndFromDepartureAirport(depAirportCode, legNumberWiseOndMap);

		// get a list of sita address
		Map<String, List<String>> sitaAddressesMap = auxilliaryDAO.getActiveSITAAddresses(depAirportCode.trim(), flightNumber,
				ond, carrierCode, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
		List<String> sitaAddressesList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
		List<String> sitaTexAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
		List<String> airincAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
		List<String> sitaAddressAll = new ArrayList<String>();
		sitaAddressAll.addAll(sitaAddressesList);
		sitaAddressAll.addAll(sitaTexAddressList);
		sitaAddressAll.addAll(airincAddress);

		log.debug("###################### Retrieved Sita List: fid: " + flightId + " airport : " + depAirportCode
				+ " Carrier code :" + carrierCode);

		dateOfflightLocal = auxilliaryDAO.getFlightLocalDate(flightId, depAirportCode);
		
		sendADLAuxiliaryNew(flightNumber, depAirportCode, dateOfflightLocal,
				(String[]) sitaAddressAll.toArray(new String[sitaAddressAll.size()]), PNLConstants.SendingMethod.SCHEDSERVICE);

		
	}
	
	/**
	 * NOTE: --------------------------------------------------------------------------- This Operation is used by the
	 * Manual UI / Resender Job! If any errors occurs in this operation, the eror will be thrown and data will get
	 * rolled back.
	 * 
	 * Email will be sent of the error. ---------------------------------------------------------------------------
	 * 
	 * @param flightNumber
	 * @param depAirportCode
	 * @param dateOfflightLocal
	 * @param sitaaddresses
	 * @param mailserver
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private void sendADLAuxiliaryNew(String flightNumber, String depAirportCode, Date dateOfflightLocal, String sitaaddresses[],
			String sendingMethod) throws ModuleException {
		// TODO move these history saving and sending part to msgbroker module
		// only one thread at time please
		synchronized (this) {

			boolean isFlightsAvailableForManulPNL = false;
			// holds the exception to be logged, if occured
			ModuleException exceptiontoThrow = null;
			// sitaaddresss - status("y", or "<exception desc>"
			HashMap<String, String> sitaStatusMap = new HashMap<String, String>();
			// generated files content
			String fileContent = null;
			String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
			// presently from the front end the sita list is sent by an aaray,
			// need a list.
			List<String> sitas = PnlAdlUtil.composeSitaAddresses(sitaaddresses);
			
			// get the flight id
			int flightId = auxilliaryDAO.getFlightID(flightNumber, depAirportCode, dateOfflightLocal);
			boolean saveWSADLHistory = false;
			if (flightId == -1) {
				isFlightsAvailableForManulPNL = true;
				throw new ModuleException("airreservations.auxilliary.invalidflight", "airreservations");
			}

			Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
			String ond = PnlAdlUtil.getOndFromDepartureAirport(depAirportCode, legNumberWiseOndMap);
			
			Map<String, List<String>> sitaAddressesMap = PnlAdlUtil.getSitaAddressMap(depAirportCode, flightNumber, ond,
					carrierCode, sitas, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
			List<String> sitaAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
			List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
			List<String> arincAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
			// map will have sita address, and failure status.

			PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddress, sitaTexAddress, arincAddressList);



			try {

				String[] monthDay = PnlAdlUtil.getMonthDayArray(dateOfflightLocal);
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.PNL_ADL_DATA_GENERATOR_MACRO);
				command.setParameter(CommandParamNames.PNL_DATA_CONTEXT, 
						createPnlDataContext(flightNumber, depAirportCode,
								dateOfflightLocal, sitaaddresses, sendingMethod));
				
				ADLDTOCreator adlDTOcreator = new ADLDTOCreator();
				ADLDTO adlElements = adlDTOcreator.generateADLElements(flightId, depAirportCode);

				ServiceResponce serviceResponce = command.execute();
				
				MessageResponseAdditionals additionals = 
						(MessageResponseAdditionals)serviceResponce.getResponseParam
						(CommandParamNames.PNL_MESSAGE_RESPONSE_ADDITIONALS);
				Map<Integer,String> messageParts = (Map)serviceResponce.
						getResponseParam(CommandParamNames.PNL_MESSAGES_PARTS);
				BaseDataContext baseDataContext = (BaseDataContext)serviceResponce.
						getResponseParam(CommandParamNames.PNL_DATA_CONTEXT);
				
				lastGroupCode = additionals.getLastGroupCode();
				ccPaxMap = additionals.getFareClassWisePaxCount();
				this.generatedAdlfileNames = (ArrayList)createMessaeFile(messageParts, 
						flightNumber, depAirportCode, monthDay[0]
						, monthDay[1]);
				PnlAdlUtil.prepareGroupCodeMapWith(additionals.getPnrPaxIdvsSegmentIds(), mapPnrPaxSegIDGroupCode
						, additionals.getPnrPaxVsGroupCodes());
				addedPassengerList = additionals.getPassengerInformations();

				if (baseDataContext.isWebserviceSuccess()) {
					saveWSADLHistory = true;
				}

				Map<String, List<String>> deliverTypeAddressMap = new HashMap<String, List<String>>();

				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddress);
				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, arincAddressList);

				// for each sita sent generated files content
				sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedAdlfileNames, msgType);
				sitaStatusMap = PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddress, this.generatedAdlfileNames, msgType,
						sitaStatusMap, null);

				if(PnlAdlUtil.isFailedForAllEmails(sitaStatusMap)){
					PnlAdlUtil.updatePnlPassengerStatusFromPnlPaxIds(adlElements.getUniquePnlPaxIds());
				}else{
					ReservationModuleUtils.getReservationAuxilliaryBD().updatePnlPassengers(adlElements.getUniquePnlPaxIds());
				}
				
				// get the generated file content
				fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedAdlfileNames);

				updateAdlStatusAndGroupId(addedPassengerList, mapPnrPaxSegIDGroupCode,sitaStatusMap);
				// if error in transmission occures roll back.
				PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);

			} catch (ModuleException exception) {

				exceptiontoThrow = exception;
				throw exceptiontoThrow;
			} catch (CommonsDataAccessException e) {

				exceptiontoThrow = new ModuleException(e, e.getExceptionCode());
				throw exceptiontoThrow;
			} catch (Exception e) {

				exceptiontoThrow = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

				throw exceptiontoThrow;

			} finally {

				log.debug("######### Before Inserting to History for ADL For FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						depAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoThrow, sendingMethod, lastGroupCode);
				log.debug("######### Inserted To History OK!");

				if(AppSysParamsUtil.isDCSConnectivityEnabled()){
					// Insert WS ADL history
					if (saveWSADLHistory) {
						PnlAdlUtil.saveWSADLHistory(depAirportCode, flightId, currentTimeStamp, PNLConstants.SITAMsgTransmission.SUCCESS);
					}else{
						PnlAdlUtil.saveWSADLHistory(depAirportCode, flightId, currentTimeStamp, PNLConstants.SITAMsgTransmission.FAIL);
					}
				}
				
				// if error has occured
				if (exceptiontoThrow != null) {

					// taking extra precaution to avoid uncessary rollback of pax status
					try {
						// save an audit
						PnlAdlUtil.savePNLADLErrorLog(flightId, msgType, depAirportCode, flightNumber, currentDate,
								exceptiontoThrow);

						// if pax count Record was not found, will attempt to send the pnl.
						if (exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)) {
							new PNLBL().sendPNL(flightId, depAirportCode);
						}

						// dont notify the flight closure error etc etc
						if (exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_ADL_ERROR_CODE)
								|| exceptiontoThrow.getExceptionCode().equals(
										PNLConstants.ERROR_CODES.FLIGHT_CLOSED_ADLERROR_CODE)
								|| exceptiontoThrow.getExceptionCode().equals(
										PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL)
								|| exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_SITA_ADDRESS)) {
						}// notify all other errors
						else {
							String sendingOperation = PnlAdlUtil.getSendingOperationDescription(sendingMethod);

							if (!isFlightsAvailableForManulPNL) {
								PnlAdlUtil.notifyAdminForManualAction(flightNumber, dateOfflightLocal, depAirportCode,
										exceptiontoThrow, PNLConstants.ADLGeneration.ADL + " " + sendingOperation, null);

								log.error("############### ADL " + sendingOperation + "Error flghtId :" + flightId + " Flt Num :"
										+ flightNumber + " airport :" + depAirportCode, exceptiontoThrow);
							}
							// throw exception and rollback pax status's
							throw exceptiontoThrow;
						}
					} catch (Throwable e) {
						if (e.equals(exceptiontoThrow)) {
							throw exceptiontoThrow;
						} else {
							log.error("Error in Finaly Method of sendADL when logging errors or emailing errors ", e);
						}

					}

				}

				PnlAdlUtil.moveGeneratedFiles(this.generatedAdlfileNames, msgType);
			}
			log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- " + depAirportCode
					+ " Flight Num : " + flightNumber);
		}
	}

	private List<Integer> getPnlPaxIds(List<PassengerInformation> passengerInformations){
		List<Integer> pnlPaxIds = new ArrayList<Integer>();
		for(PassengerInformation passengerInformation:passengerInformations){
			pnlPaxIds.add(passengerInformation.getPnlPaxId());
		}
		return pnlPaxIds;
	}
	
	private List<String> createMessaeFile(Map<Integer,String> messageParts,
			String flightNo,String boardingAirport,String month,String day) throws ModuleException{
		
		List<String> messageFiles = new ArrayList<String>();
		try{
			for (Map.Entry<Integer, String> entry : messageParts
				.entrySet()) {
				messageFiles.add(createMessageFile(entry.getKey(),
					entry.getValue(),flightNo,
					boardingAirport, month, day));
			}
		}catch (Exception e) {
			log.error("Generate PNL files Error :", e);
			throw new ModuleException(e, PNLConstants.ERROR_CODES.PNL_GENERATION_ERROR_CODE);
		}
		
		return messageFiles;
	}
	
	private String createMessageFile(Integer partNumber,String messageContent,
			String flightNo,String boardingAirport,String month,String day) throws Exception{

		SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
		List<String> filenamelist = new ArrayList<String>();
		if (Integer.parseInt(day) <= 9)
			day = "0" + day;
		String generatedpnlfilepath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
				.getGenadlpath();
		String date = simpleDateFormat.format(new Date());
		String fileName=generatedpnlfilepath + "/" + flightNo + "-" + boardingAirport + "-" + month + "" + day + "-" + "ADL"
				+ "-" + date + "-" + "PART"+ partNumber + ".txt";
		
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(messageContent);
		bw.close();
		return fileName;
	}
	
	private AdlDataContext createPnlDataContext(String flightNumber, String departureAirportCode, Date dateOfflightLocal,
			String sitaaddresses[], String sendingMethod){
		
		FeaturePack featurePack = new FeaturePack();
		featurePack.setRbdEnabled(AppSysParamsUtil.isRBDPNLADLEnabled());
		featurePack.setShowBaggage(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE));
		featurePack.setShowMeals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL));
		featurePack.setShowSeatMap(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP));
		featurePack.setShowEticketDetails(AppSysParamsUtil.isShowTicketDetailsInPnlAdl());
		featurePack.setDcsConnectivityEnabled(AppSysParamsUtil.isDCSConnectivityEnabled());
		
		AdlDataContext adlDataContext = new AdlDataContext();
		adlDataContext.setFlightNumber(flightNumber);
		adlDataContext.setDepartureAirportCode(departureAirportCode);
		adlDataContext.setFlightLocalDate(dateOfflightLocal);
		adlDataContext.setPnlAdlDeliveryInformation(createPnlAdlDeliveryInformation(sitaaddresses, sendingMethod));
		adlDataContext.setFeaturePack(featurePack);
		adlDataContext.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightNumber));		
		return adlDataContext;
		
	}
	
	private PnlAdlDeliveryInformation createPnlAdlDeliveryInformation(String sitaaddresses[], String sendingMethod){
		
		PnlAdlDeliveryInformation pnlAdlDeliveryInformation = new PnlAdlDeliveryInformation();
		pnlAdlDeliveryInformation.setSendingMethod(sendingMethod);
		pnlAdlDeliveryInformation.setSitaaddresses(sitaaddresses);
		return pnlAdlDeliveryInformation;
		
	}
	
	/**
	 * Get Passenger Details of Next ADL
	 * 
	 * @param fid
	 * @param departureStation
	 * @return
	 * @throws ModuleException
	 */
	@Deprecated
	private Collection<PNLADLDestinationDTO> decideNextADLDetails(int fid, String departureStation) throws ModuleException {

		// FIXME this is not the ideal solution but cannot do drastic changes due to the nature of the code.
		String flightNumber = auxilliaryDAO.getFlightNumber(fid);
		String carrierCode = "";
		if (flightNumber != null && !flightNumber.equals("")) {
			carrierCode = flightNumber.substring(0, 2);
		} else {
			carrierCode = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}

		Collection<PNLADLDestinationDTO> adlList = auxilliaryDAO.getADL(fid, departureStation, currentTimeStamp, carrierCode);

		// tmpGetADL();

		if (adlList == null) {
			// ideally the code should never come here.
			Throwable e = null;
			log.error(" Since a PNL Was not Sent, going to send the PNL for ADL " + fid);
			try {
				new PNLBL().sendPNL(fid, departureStation);
			} catch (Exception e1) {
				e = e1;

			}
			throw new ModuleException(e, "airreservations.pnladl.pnlnotsent");
		}

		ArrayList<PNLADLDestinationDTO> testForEmpty = (ArrayList<PNLADLDestinationDTO>) adlList;
		int passsize = 0;
		// adl not sent due to no reservations
		if (testForEmpty.size() == 0) {
			throw new ModuleException("reservationauxilliary.adl.noreservations", "airreservations");
		}

		if (testForEmpty.size() > 0)
			passsize = (testForEmpty.get(0)).getPassenger().size();

		if (adlList.size() == 1 & passsize == 0) {

			log.debug("ADL not sent due to no reservations" + fid);

			throw new ModuleException("reservationauxilliary.adl.noreservations", "airreservations");
		}
		return adlList;
	}

	private int getPaxCount(String ccCode) {
		for (String bc : ccPaxMap.keySet()) {
			if (bc.toUpperCase().charAt(0) == ccCode.toUpperCase().charAt(0)) {
				Object o = this.ccPaxMap.get(bc);
				return ((Integer) o).intValue();
			}
		}
		this.ccPaxMap.put(ccCode, new Integer(0));
		return 0;
	}

	/**
	 * Updateing ADL Status
	 * 
	 * @param passengerList
	 */
	private void updateAdlStatus(Collection<ReservationPaxDetailsDTO> passengerList, HashMap<String, String> groupPaxMap)
			throws ModuleException {

		if (passengerList == null || passengerList.size() == 0) {
			return;
		}

		Iterator<ReservationPaxDetailsDTO> it = passengerList.iterator();
		Collection<Integer> pnrPaxIdsForCNX = new ArrayList<Integer>();
		Collection<Integer> pnrSegIdsForCNX = new ArrayList<Integer>();
		Collection<Integer> pnrPaxIdsForADLSTAT = new ArrayList<Integer>();
		Collection<Integer> pnrSegIdsForADLSTAT = new ArrayList<Integer>();

		try {
			while (it.hasNext()) {

				ReservationPaxDetailsDTO dto1 = it.next();

				if (dto1.getPnrSegId() == -1) {
					continue;
				}

				String status = dto1.getPnrStatus();
				String pnlStatType = dto1.getPnlStatType();

				if ((status.equalsIgnoreCase("CNX") & pnlStatType.equalsIgnoreCase("P"))
						| (status.equalsIgnoreCase("CNX") & pnlStatType.equalsIgnoreCase("D"))
						| (status.equalsIgnoreCase("CNX") & pnlStatType.equalsIgnoreCase("C"))) {

					pnrPaxIdsForCNX.add(dto1.getPnrPaxId());
					pnrSegIdsForCNX.add(dto1.getPnrSegId());

				} else {

					pnrPaxIdsForADLSTAT.add(dto1.getPnrPaxId());
					pnrSegIdsForADLSTAT.add(dto1.getPnrSegId());

				}

			}

			if (pnrPaxIdsForCNX.size() == 0 || pnrSegIdsForCNX.size() == 0) {
				log.error("######### ERROR PNR  PAXIDS CNX || PNR SEG CNX IDS IS ZERO");
			} else {
				PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIdsForCNX, pnrPaxIdsForCNX,
						PNLConstants.PaxPnlAdlStates.ADL_CANCEL_STAT, groupPaxMap, PNLConstants.MessageTypes.ADL);
			}

			if (pnrPaxIdsForADLSTAT.size() == 0 || pnrSegIdsForADLSTAT.size() == 0) {
				log.error("######### ERROR PNR PAXIDS ADD || PNR SEG IDS ADD IS ZERO");
			} else {
				PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIdsForADLSTAT, pnrPaxIdsForADLSTAT,
						PNLConstants.PaxPnlAdlStates.ADL_ADDED_STAT, groupPaxMap, PNLConstants.MessageTypes.ADL);
			}

			log.debug("######### ADL STATUS UPDATED");
		} catch (CommonsDataAccessException e) {
			log.error("Error in updateAdlStatus :", e);
			throw new ModuleException(e, e.getExceptionCode());

		}

	}

	private void updateAdlStatusAndGroupId(Collection<PassengerInformation> passengerList, HashMap<String
			, String> groupPaxMap ,HashMap<String, String> sitaStatusMap)
			throws ModuleException {

		if (passengerList == null || passengerList.size() == 0) {
			return;
		}
		Iterator<PassengerInformation> it = passengerList.iterator();
		Collection<Integer> pnrPaxIdsForADLSTAT = new ArrayList<Integer>();
		Collection<Integer> pnrSegIdsForADLSTAT = new ArrayList<Integer>();
		try {
			while (it.hasNext()) {

				PassengerInformation dto = it.next();

				if (dto.getPnrSegId() == -1) {
					continue;
				}
				pnrPaxIdsForADLSTAT.add(dto.getPnrPaxId());
				pnrSegIdsForADLSTAT.add(dto.getPnrSegId());
			}

			if (pnrPaxIdsForADLSTAT.size() == 0 || pnrSegIdsForADLSTAT.size() == 0) {
				log.error("######### ERROR PNR PAXIDS ADD || PNR SEG IDS ADD IS ZERO");
			} else {
				if(PnlAdlUtil.isFailedForAllEmails(sitaStatusMap)){
					PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIdsForADLSTAT, pnrPaxIdsForADLSTAT,
						PNLConstants.PaxPnlAdlStates.ADL_ADDED_STAT, groupPaxMap, PNLConstants.MessageTypes.ADL);
				}else{
					ReservationModuleUtils.getReservationAuxilliaryBD().updatePaxFareSegments(pnrSegIdsForADLSTAT, pnrPaxIdsForADLSTAT,
							PNLConstants.PaxPnlAdlStates.ADL_ADDED_STAT, groupPaxMap);
				}
				
			}

			log.debug("######### ADL STATUS UPDATED");
		} catch (CommonsDataAccessException e) {
			log.error("Error in updateAdlStatus :", e);
			throw new ModuleException(e, e.getExceptionCode());

		}

	}
	
	private Collection<Integer> getPnrSegIds(Collection<PassengerInformation> passengerList){
		if (passengerList == null || passengerList.size() == 0) {
			return null;
		}
		Iterator<PassengerInformation> it = passengerList.iterator();
		Collection<Integer> pnrSegIdsForADLSTAT = new ArrayList<Integer>();

			while (it.hasNext()) {

				PassengerInformation dto = it.next();

				if (dto.getPnrSegId() == -1) {
					continue;
				}
				pnrSegIdsForADLSTAT.add(dto.getPnrSegId());
			}
			return pnrSegIdsForADLSTAT;
	}

	/**
	 * New Generate ADL
	 * 
	 * @param adlList
	 * @param meta
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Deprecated
	private void generateADL(Collection<PNLADLDestinationDTO> adlList, ADLMetaDataDTO meta, int flightId, String depStation)
			throws ModuleException {

		try {
			String foidNumberTag = "/P/";
			String nicNumberTag = "/I/";

			Map<Integer, String> pnrPaxIdMealMap = new HashMap<Integer, String>();

			Map<Integer, String> pnrPaxIdSeatMap = new HashMap<Integer, String>();

			Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxIdSSRMap = new HashMap<Integer, Collection<PaxSSRDetailDTO>>();

			Map<Integer, String> pnrPaxIdBaggageMap = new HashMap<Integer, String>();

			if (!adlList.isEmpty()
					&& ("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP)))) {
				pnrPaxIdSeatMap = PnlAdlUtil.getSeatMapDetails(adlList, false);
			}

			if (!adlList.isEmpty()
					&& ("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL)))) {
				pnrPaxIdMealMap = PnlAdlUtil.getMealsDetails(adlList, true);
			}

			if (!adlList.isEmpty()
					&& ("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE)))) {
				pnrPaxIdBaggageMap = PnlAdlUtil.getBaggagesWeights(adlList, true, depStation);
			}

			if (!adlList.isEmpty()) {
				pnrPaxIdSSRMap = PnlAdlUtil.getSSRDetails(adlList, true);
			}

			// temporarily edited, to return cabincalss, and object array map
			Object[] o = auxilliaryDAO.getPaxCountInPnlADL(new Integer(flightId), depStation);
			ccPaxMap = (HashMap<String, Integer>) o[0];
			lastGroupCode = (String) o[1];

			PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;

			ArrayList finalList = new ArrayList();
			// keep a collection of passenger for later updateing their pnl status.
			Collection adlUpdateList = new ArrayList();

			ArrayList pnlDataList = new ArrayList();
			// array to store tourIDs
			String[] tourIds = PnlAdlUtil.populateTourIds();

			// iterate the passenger adl list
			Iterator it = adlList.iterator();
			int tour = 0;
			Collection ccCodeList = new ArrayList();
			String destinationAirport = "";

			while (it.hasNext()) {

				PNLADLDestinationDTO dt = (PNLADLDestinationDTO) it.next();
				PassengerCountRecordDTO cdto = new PassengerCountRecordDTO();
				cdto.setDestination(dt.getDestinationAirportCode());
				cdto.setFareClass(dt.getBookingCode());
				cdto.setTotalpassengers(dt.getPassenger().size() - 1);
				ccCodeList.add(dt.getBookingCode());
				destinationAirport = dt.getDestinationAirportCode();

				// Additions List
				ArrayList addList = new ArrayList();
				// Deletions List
				ArrayList delList = new ArrayList();
				// Changed List
				ArrayList changedList = new ArrayList();

				// In Name Change Scenario the CHG's go as Additions
				ArrayList forcedAddList = new ArrayList();

				// retrieves pnr - passenger map
				HashMap<String, ArrayList<ReservationPaxDetailsDTO>> pnrPassengersMap = PnlAdlUtil
						.getPNRPassengerList((ArrayList) dt.getPassenger());
				Iterator<String> itPnr = pnrPassengersMap.keySet().iterator();
				// Iterate the PNRs
				while (itPnr.hasNext()) {
					// pnr
					String pnr = itPnr.next();
					// passenger list per pnr: ReservationPaxDetailsDTO

					Map<String, List<ReservationPaxDetailsDTO>> segStatusWisePaxMap = PnlAdlUtil
							.getSegmentStatusWisePassengers(pnrPassengersMap.get(pnr));

					for (String segmentStatus : segStatusWisePaxMap.keySet()) {
						List<ReservationPaxDetailsDTO> passengers = segStatusWisePaxMap.get(segmentStatus);
						// keep track of adl passengers.
						adlUpdateList.addAll(passengers);

						int dupIndex = 0;
						int tourIdIndex = 0;
						int firstNameCountADL = 0;
						int totalPaxCountForPnr = passengers.size();

						// iterate the passenger details.
						int i = 0;
						while (passengers.size() > 0) {

							i++;
							ReservationPaxDetailsDTO pax = passengers.get(0);

							firstNameCountADL = PnlAdlUtil.getFirstNamesCount(passengers);
							ArrayList passengerList = new ArrayList();
							passengerList.addAll(passengers);

							String status = pax.getPnrStatus();
							String pnlStatus = pax.getPnlStatType();

							ArrayList<NameDTO> firstNames = PnlAdlUtil.getFirstNames(passengers, firstNameCountADL,
									totalPaxCountForPnr, this.paxIdsPerRecord, pnrPaxIdSeatMap);

							ADLRecordDTO adlRecDTO = new ADLRecordDTO();
							if (!firstNames.isEmpty()
									&& PnlAdlUtil.isMultipleSeatsBooked(firstNames.get(0).getPnrPaxId(), pnrPaxIdSeatMap)) {
								adlRecDTO.setMultipleSeatsSelected(true);
							} else {
								Collections.sort(firstNames);
							}
							firstNameCountADL = firstNames.size();

							int pid = pax.getPnrPaxId().intValue();

							adlRecDTO.setNames(firstNames);
							adlRecDTO.setLastname(pax.getLastName().toUpperCase());
							adlRecDTO.setAutomatedPNR(pax.getPnr());
							adlRecDTO.setNumberPAD(firstNameCountADL);
							adlRecDTO.setPnrSegId(pax.getPnrSegId());
							adlRecDTO.setTourID(pax.getGroupId());
							adlRecDTO.setPnrPaxId(pax.getPnrPaxId());

							// Extract PaxSSRDetails from all firstNames list
							// ssrCollectionForFirstNames = null; //FIXME
							for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
								NameDTO tmpNameDTO = (NameDTO) firstNamesIt.next();
								for (Iterator paxIt = passengerList.iterator(); paxIt.hasNext();) {
									ReservationPaxDetailsDTO paxDTO = (ReservationPaxDetailsDTO) paxIt.next();
									if (tmpNameDTO.getPnrPaxId() == paxDTO.getPnrPaxId()) {
										for (PaxSSRDetailDTO paxSSR : paxDTO.getSsrDetails()) {
											if (SSRCode.PSPT.toString().equals(paxSSR.getSsrCode())) {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSR.getSsrCode());
												remarksElementDTO.setStatusCode("HK1");
												StringBuilder ssrText = new StringBuilder();
												ssrText.append(" " + paxSSR.getSsrText().toUpperCase()).append("/")
														.append(paxDTO.getFoidPlace()).append("///").append("-1")
														.append(adlRecDTO.getLastname().toUpperCase()).append("/")
														.append(tmpNameDTO.getFirstname().toUpperCase())
														.append(tmpNameDTO.getTitle());

												remarksElementDTO.setText(ssrText.toString());// FIXME

												adlRecDTO.addRequest(remarksElementDTO);
											} else if (SSRCode.DOCS.toString().equals(paxSSR.getSsrCode())) {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSR.getSsrCode());
												remarksElementDTO.setStatusCode("HK1");
												StringBuilder ssrText = new StringBuilder();
												ssrText.append(paxSSR.isNICSentInPNLADL() ? nicNumberTag : foidNumberTag)
														.append(paxDTO.getFoidPlace()).append("/")
														.append(paxSSR.getSsrText().toUpperCase()).append("/")
														.append(paxDTO.getNationalityIsoCode()).append("/")
														.append(paxDTO.getDob().toUpperCase()).append("/")
														.append(paxDTO.getGender()).append("/")
														.append(paxDTO.getFoidExpiry().toUpperCase()).append("/")
														.append(adlRecDTO.getLastname().toUpperCase()).append("/")
														.append(tmpNameDTO.getFirstname().toUpperCase()).append("-1")
														.append(adlRecDTO.getLastname().toUpperCase()).append("/")
														.append(tmpNameDTO.getFirstname().toUpperCase())
														.append(tmpNameDTO.getTitle());

												remarksElementDTO.setText(ssrText.toString());

												adlRecDTO.addRequest(remarksElementDTO);
											} else if (SSRCode.DOCO.toString().equals(paxSSR.getSsrCode())) {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSR.getSsrCode());
												remarksElementDTO.setStatusCode("HK1");
												String infantIndicator = "";
												if (tmpNameDTO.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)){
													infantIndicator = "/I";
												}
												StringBuilder ssrText = new StringBuilder();
												ssrText.append(paxDTO.getPlaceOfBirth().toUpperCase()).append("/")
														.append(paxDTO.getTravelDocumentType().toUpperCase()).append("/")
														.append(paxDTO.getVisaDocNumber()).append("/")
														.append(paxDTO.getVisaDocPlaceOfIssue().toUpperCase()).append("/").append(paxDTO.getVisaDocIssueDate())
														.append("/").append(paxDTO.getVisaApplicableCountry())
														.append(infantIndicator)
														//.append(pnrc.getLastname().toUpperCase()).append("/")
														//.append(tmpNameDTO.getFirstname().toUpperCase())
														.append("-1")
														.append(adlRecDTO.getLastname().toUpperCase()).append("/")
														.append(tmpNameDTO.getFirstname().toUpperCase())
														.append(tmpNameDTO.getTitle());
												;
												remarksElementDTO.setText(ssrText.toString());

												adlRecDTO.addRequest(remarksElementDTO);
											} else if (SSRCode.TKNA.toString().equals(paxSSR.getSsrCode())
													|| SSRCode.TKNE.toString().equals(paxSSR.getSsrCode())) {
												if (AppSysParamsUtil.isShowTicketDetailsInPnlAdl()) {
													RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
													remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
															+ paxSSR.getSsrCode());
													remarksElementDTO.setStatusCode(PNLADLStatusCode.HK1);
													StringBuilder ssrText = new StringBuilder(PNLADLElements.SPACE);
													ssrText.append(paxSSR.getSsrText().toUpperCase()).append("-1")
															.append(adlRecDTO.getLastname().toUpperCase()).append("/")
															.append(tmpNameDTO.getFirstname().toUpperCase())
															.append(tmpNameDTO.getTitle());
													remarksElementDTO.setText(ssrText.toString());// FIXME

													adlRecDTO.addRequest(remarksElementDTO);
												}
											} else {
												log.error("Generate ADL ERROR : SSR Details are incorrect");
												throw new ModuleException(PNLConstants.ERROR_CODES.PNL_GENERATION_ERROR_CODE);
											}
										}
									}
								}
							}

							if (pax.isWaitingListPax()) {
								adlRecDTO.setHasWaitingList(true);
								adlRecDTO.setWaitingListStart(PNLConstants.PNLADLElements.WAITLIST);
							}

							if (pax.isIDPassenger()) {
								adlRecDTO.setHasIDpax(true);
								adlRecDTO.setStandByIDPax(PNLConstants.PNLADLElements.ID2N2);
							}

							if ("Y".equalsIgnoreCase(ReservationModuleUtils.getGlobalConfig().getBizParam(
									SystemParamKeys.SHOW_SEAT_MAP))) {
								String seatNumber = null;
								NameDTO tmpNameDTO = null;
								Integer tmpPnrPaxId = null;
								if (this.paxIdsPerRecord.size() == 1) {
									seatNumber = pnrPaxIdSeatMap.get(this.paxIdsPerRecord.get(0));
									if (seatNumber != null) {
										// In case multiple seats blocked for single pax
										String seatNumbers[] = seatNumber.split(" ");
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.SEATMAP);
										remarksElementDTO.setStatusCode("HK1");
										remarksElementDTO.setText(" " + seatNumbers[0]);

										adlRecDTO.addRequest(remarksElementDTO);

										if (adlRecDTO.isMultipleSeatsSelected()) {
											// This is to add (extra)exst seat
											RemarksElementDTO exstRemarksElementDTO = new RemarksElementDTO();
											exstRemarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.EXTRA_SEAT);
											exstRemarksElementDTO.setStatusCode("HK1");

											tmpNameDTO = (NameDTO) firstNames.get(1);
											exstRemarksElementDTO.setText(" " + seatNumbers[1] + "-1"
													+ adlRecDTO.getLastname().toUpperCase() + "/"
													+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());

											adlRecDTO.addRequest(exstRemarksElementDTO);
										}
									}
								} else {
									for (Iterator paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
										tmpPnrPaxId = (Integer) paxIdIt.next();
										seatNumber = pnrPaxIdSeatMap.get(tmpPnrPaxId);
										if (seatNumber != null) {
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.SEATMAP);
											remarksElementDTO.setStatusCode("HK1");

											tmpNameDTO = null;
											for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
												tmpNameDTO = (NameDTO) firstNamesIt.next();
												if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
													break;
												}
											}
											remarksElementDTO.setText(" " + seatNumber + "-1"
													+ adlRecDTO.getLastname().toUpperCase() + "/"
													+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
											adlRecDTO.addRequest(remarksElementDTO);
										}
									}
								}
							}

							if (("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL)))) {

								String tmpMealCode = null;
								NameDTO tmpNameDTO = null;
								Integer tmpPnrPaxId = null;
								if (this.paxIdsPerRecord.size() == 1) {
									tmpMealCode = pnrPaxIdMealMap.get(this.paxIdsPerRecord.get(0));
									if (tmpMealCode != null) {
										// Split IATA code and meal code
										String[] mealCodes = tmpMealCode.split("%");
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										if (mealCodes.length > 1) {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ mealCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK1");
											remarksElementDTO.setText(" " + mealCodes[1].toUpperCase());
										} else {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ mealCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK1");
											remarksElementDTO.setText("");
										}
										adlRecDTO.addRequest(remarksElementDTO);
									}
								} else {
									boolean allHasSameMeal = true;
									int mealsCount = 0;
									for (Iterator paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
										tmpPnrPaxId = (Integer) paxIdIt.next();
										if (tmpMealCode != null) {
											if (pnrPaxIdMealMap.get(tmpPnrPaxId) != null) {
												if (!tmpMealCode.equals(pnrPaxIdMealMap.get(tmpPnrPaxId))) {
													allHasSameMeal = false;
													break;
												}
											}
										}
										tmpMealCode = pnrPaxIdMealMap.get(tmpPnrPaxId);
										if (tmpMealCode != null)
											++mealsCount;
									}

									if (allHasSameMeal && this.paxIdsPerRecord.size() == mealsCount) {
										// All the pax has same meal

										// Split IATA code and meal code
										String[] mealCodes = tmpMealCode.split("%");
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										if (mealCodes.length > 1) {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ mealCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK" + mealsCount);
											remarksElementDTO.setText(" " + mealCodes[1].toUpperCase());
										} else {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ mealCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK" + mealsCount);
											remarksElementDTO.setText("");
										}

										adlRecDTO.addRequest(remarksElementDTO);
									} else {
										// Not all the pax has the same meal
										for (Iterator paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
											tmpPnrPaxId = (Integer) paxIdIt.next();
											tmpMealCode = pnrPaxIdMealMap.get(tmpPnrPaxId);
											if (tmpMealCode != null) {
												// Split IATA code and meal code
												String[] mealCodes = tmpMealCode.split("%");
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												if (mealCodes.length > 1) {
													remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
															+ mealCodes[0].toUpperCase());
													remarksElementDTO.setStatusCode("HK1");

													tmpNameDTO = null;
													for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
														tmpNameDTO = (NameDTO) firstNamesIt.next();
														if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
															break;
														}
													}
													remarksElementDTO.setText(" " + mealCodes[1].toUpperCase() + "-1"
															+ adlRecDTO.getLastname().toUpperCase() + "/"
															+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
												} else {
													remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
															+ mealCodes[0].toUpperCase());
													remarksElementDTO.setStatusCode("HK1");

													tmpNameDTO = null;
													for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
														tmpNameDTO = (NameDTO) firstNamesIt.next();
														if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
															break;
														}
													}
													remarksElementDTO.setText("-1" + adlRecDTO.getLastname().toUpperCase() + "/"
															+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
												}
												adlRecDTO.addRequest(remarksElementDTO);
											}
										}
									}
								}

							}

							if (("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE)))) {

								String tmpBaggageCode = null;
								NameDTO tmpNameDTO = null;
								Integer tmpPnrPaxId = null;
								if (this.paxIdsPerRecord.size() == 1) {
									tmpBaggageCode = pnrPaxIdBaggageMap.get(this.paxIdsPerRecord.get(0));
									if (tmpBaggageCode != null) {
										// Split IATA code and meal code
										String[] baggageCodes = tmpBaggageCode.split("%");
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										if (baggageCodes.length > 1) {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ baggageCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK1");
											remarksElementDTO.setText(" " + baggageCodes[1].toUpperCase());
										} else {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ baggageCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK1");
											remarksElementDTO.setText("");
										}
										adlRecDTO.addRequest(remarksElementDTO);
									}
								} else {
									boolean allHasSameBaggage = true;
									int baggagesCount = 0;
									for (Iterator paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
										tmpPnrPaxId = (Integer) paxIdIt.next();
										if (tmpBaggageCode != null) {
											if (pnrPaxIdBaggageMap.get(tmpPnrPaxId) != null) {
												if (!tmpBaggageCode.equals(pnrPaxIdBaggageMap.get(tmpPnrPaxId))) {
													allHasSameBaggage = false;
													break;
												}
											}
										}
										tmpBaggageCode = pnrPaxIdBaggageMap.get(tmpPnrPaxId);
										if (tmpBaggageCode != null)
											++baggagesCount;
									}

									if (allHasSameBaggage && this.paxIdsPerRecord.size() == baggagesCount) {

										String[] baggageCodes = tmpBaggageCode.split("%");
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										if (baggageCodes.length > 1) {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ baggageCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK" + baggagesCount);
											remarksElementDTO.setText(" " + baggageCodes[1].toUpperCase());
										} else {
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ baggageCodes[0].toUpperCase());
											remarksElementDTO.setStatusCode("HK" + baggagesCount);
											remarksElementDTO.setText("");
										}

										adlRecDTO.addRequest(remarksElementDTO);
									} else {

										for (Iterator paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
											tmpPnrPaxId = (Integer) paxIdIt.next();
											tmpBaggageCode = pnrPaxIdBaggageMap.get(tmpPnrPaxId);
											if (tmpBaggageCode != null) {

												String[] baggageCodes = tmpBaggageCode.split("%");
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												if (baggageCodes.length > 1) {
													remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
															+ baggageCodes[0].toUpperCase());
													remarksElementDTO.setStatusCode("HK1");

													tmpNameDTO = null;
													for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
														tmpNameDTO = (NameDTO) firstNamesIt.next();
														if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
															break;
														}
													}
													remarksElementDTO.setText(" " + baggageCodes[1].toUpperCase() + "-1"
															+ adlRecDTO.getLastname().toUpperCase() + "/"
															+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
												} else {
													remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
															+ baggageCodes[0].toUpperCase());
													remarksElementDTO.setStatusCode("HK1");

													tmpNameDTO = null;
													for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
														tmpNameDTO = (NameDTO) firstNamesIt.next();
														if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
															break;
														}
													}
													remarksElementDTO.setText("-1" + adlRecDTO.getLastname().toUpperCase() + "/"
															+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
												}
												adlRecDTO.addRequest(remarksElementDTO);
											}
										}
									}
								}

							}

							// /======================== SSR Details ============================
							// to be changed with appropiate Key
							// String ssrText = null;
							NameDTO tmpNameDTO = null;
							Integer tmpPnrPaxId = null;
							if (this.paxIdsPerRecord.size() == 1) {
								Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = pnrPaxIdSSRMap.get(this.paxIdsPerRecord.get(0));
								if (paxSSRDetailDTOs != null && paxSSRDetailDTOs.size() > 0) {
									for (PaxSSRDetailDTO paxSSRDetailDTO : paxSSRDetailDTOs) {

										if (ValidationUtils.skipAirportService(paxSSRDetailDTO.getSsrCatId())) {
											continue;
										}

										if (paxSSRDetailDTO.getSsrCode() != null) {
											if (paxSSRDetailDTO.getSsrCode().equals(SSRCode.FQTV.toString())) {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSRDetailDTO.getSsrCode());
												remarksElementDTO.setStatusCode(AppSysParamsUtil.getCarrierCode());
												remarksElementDTO.setText(paxSSRDetailDTO.getSsrText() != null
														? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
														: "");
												adlRecDTO.addRequest(remarksElementDTO);
											} else {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSRDetailDTO.getSsrCode());
												remarksElementDTO.setStatusCode("HK1");
												remarksElementDTO.setText(paxSSRDetailDTO.getSsrText() != null
														? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
														: "");
												adlRecDTO.addRequest(remarksElementDTO);
											}
										}
									}
								}
							} else {
								for (Iterator paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
									tmpPnrPaxId = (Integer) paxIdIt.next();
									Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = pnrPaxIdSSRMap.get(tmpPnrPaxId);

									if (paxSSRDetailDTOs != null && paxSSRDetailDTOs.size() > 0) {
										for (PaxSSRDetailDTO paxSSRDetailDTO : paxSSRDetailDTOs) {

											if (ValidationUtils.skipAirportService(paxSSRDetailDTO.getSsrCatId())) {
												continue;
											}

											if (paxSSRDetailDTO.getSsrCode() != null) {
												if (paxSSRDetailDTO.getSsrCode().equals(SSRCode.FQTV.toString())) {
													RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
													remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
															+ paxSSRDetailDTO.getSsrCode());
													remarksElementDTO.setStatusCode(AppSysParamsUtil.getCarrierCode());

													tmpNameDTO = null;
													for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
														tmpNameDTO = (NameDTO) firstNamesIt.next();
														if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
															break;
														}
													}
													remarksElementDTO.setText((paxSSRDetailDTO.getSsrText() != null
															? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
															: "")
															+ "-1"
															+ adlRecDTO.getLastname().toUpperCase()
															+ "/"
															+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
													adlRecDTO.addRequest(remarksElementDTO);
												} else {
													RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
													remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
															+ paxSSRDetailDTO.getSsrCode());
													remarksElementDTO.setStatusCode("HK1");

													tmpNameDTO = null;
													for (Iterator firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
														tmpNameDTO = (NameDTO) firstNamesIt.next();
														if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
															break;
														}
													}
													remarksElementDTO.setText((paxSSRDetailDTO.getSsrText() != null
															? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
															: "")
															+ "-1"
															+ adlRecDTO.getLastname().toUpperCase()
															+ "/"
															+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
													adlRecDTO.addRequest(remarksElementDTO);
												}
											}
										}
									}
								}
							}

							// ====== END SSR Details ==============================================================

							// Setting CreditCard Details
							if (ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SEND_CC_DETAILS_WITH_PNL)
									.equalsIgnoreCase("Y")) {

								String ccDetails = "";
								ccDetails = pax.getCcDigits();
								if (ccDetails != null && !ccDetails.trim().equals("")) {
									adlRecDTO.setHasCCDetails(true);
									adlRecDTO.setCCDetails(" CC/XXXX-XXXX-XXXX-" + BeanUtils.getLast4Digits(ccDetails));
									adlRecDTO.setCCDetailsRequestCode(PNLConstants.PNLADLElements.EPAY);
									adlRecDTO.setCCDetailsStatusCode(" KK" + firstNameCountADL); // Status code ex:
																									// KK2,HK1
								}
							}

							ReservationPax rpa = null;
							// String infantString = "";
							int infantSize = 0;
							// StringBuffer buff = new StringBuffer();
							for (int p = 0; p < paxIdsPerRecord.size(); p++) {
								int paxid = (paxIdsPerRecord.get(p)).intValue();
								rpa = passengerDAO.getPassenger(paxid, false, false, false);
								Set cset = rpa.getInfants();
								Iterator it1 = cset.iterator();
								while (it1.hasNext()) {
									ReservationPax rpat = (ReservationPax) (it1.next());
									String firstName = rpat.getFirstName();
									String lastName = rpat.getLastName();
									String title = rpat.getTitle();
									if (firstName == null) {
										firstName = "";
									}
									if (lastName == null) {
										lastName = "";
									}
									if (title == null) {
										title = "";
									}
									firstName = BeanUtils.removeExtraChars(firstName.toUpperCase());
									firstName = BeanUtils.removeSpaces(firstName).toUpperCase();

									lastName = BeanUtils.removeExtraChars(lastName.toUpperCase());
									lastName = BeanUtils.removeSpaces(lastName).toUpperCase();

									// buff.append(firstName + " ");

									// Add infants as remarks
									RemarksElementDTO infRemarksElementDTO = new RemarksElementDTO();

									infRemarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
											+ PNLADLElements.INFANT2);
									infRemarksElementDTO.setStatusCode(PNLADLStatusCode.HK1);
									StringBuilder infText = new StringBuilder(PNLADLElements.SPACE);
									infText.append(lastName).append("/").append(firstName).append(title);
									infRemarksElementDTO.setText(infText.toString());
									adlRecDTO.addRequest(infRemarksElementDTO);

									// infant e ticket including
									EticketTO eTicket = eTicketDAO.getPassengerETicketDetail(rpat.getPnrPaxId(), flightId);
									if (eTicket != null && AppSysParamsUtil.isShowInfantETicketInPNLADL()
											&& AppSysParamsUtil.isShowTicketDetailsInPnlAdl()) {
										String SsrCode;

										// Add Parent name for infant E-ticket details
										String paFirstName = rpa.getFirstName();
										String paLastName = rpa.getLastName();
										String paTitle = rpa.getTitle();
										if (paFirstName == null) {
											paFirstName = "";
										}
										if (paLastName == null) {
											paLastName = "";
										}
										if (paTitle == null) {
											paTitle = "";
										}
										paFirstName = BeanUtils.removeExtraChars(paFirstName.toUpperCase());
										paFirstName = BeanUtils.removeSpaces(paFirstName).toUpperCase();

										paLastName = BeanUtils.removeExtraChars(paLastName.toUpperCase());
										paLastName = BeanUtils.removeSpaces(paLastName).toUpperCase();

										if (AppSysParamsUtil.isPNLEticketEnabled()) {
											SsrCode = CommonsConstants.PNLticketType.TKNE;
										} else {
											SsrCode = CommonsConstants.PNLticketType.TKNA;
										}
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();

										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK + SsrCode);
										remarksElementDTO.setStatusCode(PNLADLStatusCode.HK1);
										StringBuilder infSsrText = new StringBuilder(PNLADLElements.SPACE);
										infSsrText.append(PNLADLElements.INFANT + eTicket.getEticketNumber().toUpperCase())
												.append("/").append(eTicket.getCouponNo()).append("-1").append(paLastName)
												.append("/").append(paFirstName).append(paTitle);
										remarksElementDTO.setText(infSsrText.toString());// FIXME
										adlRecDTO.addRequest(remarksElementDTO);
									}
									
									// infant DOCO information
									ReservationPaxAdditionalInfo resPaxAdditionalInfo = rpat.getPaxAdditionalInfo();
									if (resPaxAdditionalInfo != null && resPaxAdditionalInfo.getVisaDocNumber() != null
											&& !resPaxAdditionalInfo.getVisaDocNumber().equals("")) {
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										remarksElementDTO
												.setRequestCode(PNLConstants.PNLADLElements.REMARK + SSRCode.DOCO.toString());
										remarksElementDTO.setStatusCode("HK1");
										String infantIndicator = "/I";
										
										String visaDocIssueDate = "";
										if (resPaxAdditionalInfo.getVisaDocIssueDate() != null) {
											SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
											visaDocIssueDate  = sf.format(resPaxAdditionalInfo.getVisaDocIssueDate());
										}									
										String placeOfBirth = ((resPaxAdditionalInfo.getPlaceOfBirth() == null) ? "": resPaxAdditionalInfo.getPlaceOfBirth());
										String travelDocumentType = ((resPaxAdditionalInfo.getTravelDocumentType() == null) ? "": resPaxAdditionalInfo.getTravelDocumentType());									
										String visaDocPlaceOfIssue = ((resPaxAdditionalInfo.getVisaDocPlaceOfIssue() == null) ? "": resPaxAdditionalInfo.getVisaDocPlaceOfIssue());
										String visaApplicableCountry = ((resPaxAdditionalInfo.getVisaApplicableCountry() == null) ? "": resPaxAdditionalInfo.getVisaApplicableCountry());
										
										StringBuilder ssrText = new StringBuilder();
										ssrText.append("/").append(placeOfBirth.toUpperCase()).append("/")
												.append(travelDocumentType.toUpperCase()).append("/")
												.append(resPaxAdditionalInfo.getVisaDocNumber()).append("/")
												.append(visaDocPlaceOfIssue.toUpperCase()).append("/")
												.append(visaDocIssueDate).append("/")
												.append(visaApplicableCountry)
												.append(infantIndicator)
												// .append(pnrc.getLastname().toUpperCase()).append("/")
												// .append(tmpNameDTO.getFirstname().toUpperCase())
												.append("-1").append(rpat.getLastName().toUpperCase()).append("/")
												.append(rpat.getFirstName().toUpperCase()).append(tmpNameDTO.getTitle());
										;
										remarksElementDTO.setText(ssrText.toString());

										adlRecDTO.addRequest(remarksElementDTO);
									}
								}
								infantSize += cset.size();
							}
							paxIdsPerRecord.clear();
							adlRecDTO.setInfantString("");
							// infantString = ".R/" + infantSize + "INF " + buff.toString();
							//
							// if (infantSize == 0) {
							// adlRecDTO.setInfantString("");
							// } else {
							// adlRecDTO.setInfantString(" " + infantString.trim());
							// }
							if (AppSysParamsUtil.isPNLEticketEnabled()) {
								adlRecDTO.setOsicode(CommonsConstants.PNLticketType.TKNE);
							} else {
								adlRecDTO.setOsicode(CommonsConstants.PNLticketType.TKNA);
							}

							ArrayList list = (ArrayList) auxilliaryDAO.getAllOutBoundSequences(
									pax.getPnr(),
									pid,
									depStation,
									flightId,
									status.equalsIgnoreCase("CNF")
											& (pnlStatus.equalsIgnoreCase("N") | pnlStatus.equalsIgnoreCase("C")));

							if (list != null) {
								adlRecDTO.setOnwardconnectionlist(list);
							}

							InboundConnectionDTO idt = auxilliaryDAO.getInBoundConnectionList(
									pax.getPnr(),
									status.equalsIgnoreCase("CNF")
											& (pnlStatus.equalsIgnoreCase("N") | pnlStatus.equalsIgnoreCase("C")), depStation,
									flightId);

							if (idt != null) {
								if (idt.getDate() != null)
									adlRecDTO.setInboundDate(idt.getDate().trim());
								if (idt.getDepartureStation() != null)
									adlRecDTO.setInboundDepartureStation(idt.getDepartureStation().trim());
								if (idt.getFareClass() != null)
									adlRecDTO.setInboundFareClass(idt.getFareClass().trim());
								if (idt.getFlightNumber() != null) {
									adlRecDTO.setInboundFlightNumber(idt.getFlightNumber());
									adlRecDTO.setInboundStart(".I/");
									adlRecDTO.setHasInbound(true);
								}
							}

							adlRecDTO.setTotalSeats(totalPaxCountForPnr);

							// populating the add list
							if (status.equalsIgnoreCase(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
									& pnlStatus.equalsIgnoreCase(PNLConstants.PaxPnlAdlStates.PNL_RES_STAT)
									& !addList.contains(adlRecDTO)) {
								if (totalPaxCountForPnr != firstNameCountADL) {
									adlRecDTO.setDuplicatedPNRaddress(true);
									adlRecDTO.setTourID(tourIds[tourIdIndex]);
									adlRecDTO.setIndex(dupIndex);
									dupIndex++;
								}
								addList.add(adlRecDTO);

							}// populating the deletions list.
							else if ((status.equalsIgnoreCase(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
									& pnlStatus.equalsIgnoreCase(PNLConstants.PaxPnlAdlStates.PNL_PNL_STAT) & !delList
										.contains(adlRecDTO))
									| (status.equalsIgnoreCase(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
											& pnlStatus.equalsIgnoreCase(PNLConstants.PaxPnlAdlStates.ADL_ADDED_STAT) & !delList
												.contains(adlRecDTO))) {

								if (totalPaxCountForPnr != firstNameCountADL) {
									adlRecDTO.setDuplicatedPNRaddress(true);
									adlRecDTO.setIndex(dupIndex);
									dupIndex++;
								}
								delList.add(adlRecDTO);
							}

							// populating the changed list.
							if (status.equalsIgnoreCase(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
									& pnlStatus.equalsIgnoreCase(PNLConstants.PaxPnlAdlStates.ADL_CHANGED_STAT)
									& !changedList.contains(adlRecDTO)) {
								if (!PnlAdlUtil.isNameChangeScenario(delList, addList, adlRecDTO, forcedAddList)) {

									if (totalPaxCountForPnr != firstNameCountADL) {
										adlRecDTO.setDuplicatedPNRaddress(true);
										adlRecDTO.setIndex(dupIndex);
										dupIndex++;

									}
									changedList.add(adlRecDTO);
								}

							}
							// populating the deletions list.
							if (status.equalsIgnoreCase(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
									& pnlStatus.equalsIgnoreCase(PNLConstants.PaxPnlAdlStates.ADL_CHANGED_STAT)
									& !changedList.contains(adlRecDTO)) {

								PnlAdlUtil.removePaxScenario(delList, adlRecDTO);
							}

							if (i > 200) {
								String error = "##### Unexpect Problem! Passenger Size not being removed in PnlADLUtil.getFirstNames Method. Check This. For flight ID :"
										+ flightId;
								log.error(error);
								PnlAdlUtil.notifyAdminForManualAction(null, null, null, null, null, error);
								break;
							}

						}
					}
				}

				tour = PnlAdlUtil.determineNextTourIndex(lastGroupCode, tourIds);
				this.lastGroupCode = PnlAdlUtil.populateTourIDsForADLNew(addList, tour, tourIds);

				PnlAdlUtil.prepareADLGroupCodeMap(addList, mapPnrPaxSegIDGroupCode);

				Collections.sort(addList);
				Collections.sort(delList);
				Collections.sort(changedList);

				cdto.setAddpnlrecords(addList);
				cdto.setDelpnlrecords(delList);
				cdto.setChgRecords(changedList);

				int addTotal = 0;
				int delTotal = 0;
				if (!pnlDataList.contains(cdto)) {
					ArrayList list1 = cdto.getAddpnlrecords();

					for (int i = 0; i < list1.size(); i++) {
						ADLRecordDTO dtc = (ADLRecordDTO) list1.get(i);
						// tot1 += dtc.getNames().size();
						addTotal += dtc.getNumberPAD();
					}

					ArrayList list2 = cdto.getDelpnlrecords();

					for (int i = 0; i < list2.size(); i++) {
						ADLRecordDTO dtc = (ADLRecordDTO) list2.get(i);
						delTotal += dtc.getNumberPAD();
					}
					cdto.setTotalpassengers(delTotal + addTotal);
					pnlDataList.add(cdto);
				}

				// get number of additions and deletions need to show the total in flight
				// in adl
				int newPaxCount = getPaxCount(cdto.getFareClass()) + addTotal - delTotal;
				cdto.setTotalpassengers(newPaxCount);
				ccPaxMap.put(cdto.getFareClass(), new Integer(newPaxCount));

				// For multi-leg flight, tourId should be continued from last tourId used
				tour = PnlAdlUtil.determineNextTourIndex(lastGroupCode, tourIds);
			}
			if (!finalList.contains(pnlDataList)) {
				finalList.add(pnlDataList);
			}
			addedPassengerList = adlUpdateList;
			PnlAdlUtil.checkIFAllCOSToBeShown(PNLConstants.ADLGeneration.ADL, ccCodeList, flightId, destinationAirport,
					finalList, ccPaxMap);
			if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
				Airport airport = ReservationModuleUtils.getAirportBD().getAirport(meta.getBoardingairport());

				if (airport.isWSAADCSEnabled()) {
					DCSADLInfoRQ dcsInfoReq = compileADLDatatoDCS(finalList, meta, flightId, pnrPaxIdMealMap, pnrPaxIdBaggageMap,
							pnrPaxIdSeatMap, pnrPaxIdSSRMap);
					sendADLtoDCS(dcsInfoReq, depStation, flightId);
				}
			}
			// Normal Sita ADL sending
			ADLFormatter adlf = new ADLFormatter(finalList, meta);
			generatedAdlfileNames = adlf.getFileNameList();

		} catch (CommonsDataAccessException e) {

			log.error(" Error Generate ADL", e);
			throw new ModuleException(e, e.getExceptionCode());
		} catch (ModuleException e) {
			log.error(" Error Generate ADL", e);
			throw e;

		} catch (Exception e) {
			log.error(" Error Generate ADL", e);
			throw new ModuleException(e, PNLConstants.ERROR_CODES.ADL_GENERATION_ERROR_CODE);
		}
	}

	@Deprecated
	private DCSADLInfoRQ compileADLDatatoDCS(List<List<PassengerCountRecordDTO>> adlData, ADLMetaDataDTO meta, int flightId,
			Map<Integer, String> pnrPaxIdMealMap, Map<Integer, String> pnrPaxIdBaggageMap, Map<Integer, String> pnrPaxIdSeatMap,
			Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxIdSSRMap) throws ModuleException {
		DCSADLInfoRQ dcsadlInfoRQ = new DCSADLInfoRQ();
		DCSFlight flightInfo = new DCSFlight();
		flightInfo.setDepartureAirportCode(meta.getBoardingairport());
		flightInfo.setFlightNumber(meta.getFlight());

		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		Flight flightObj = flightBD.getFlight(flightId);
		flightInfo.setOriginAirportCode(flightObj.getOriginAptCode());

		Date departureDateZulu = null;
		if (flightObj != null) {
			for (FlightSegement fs : (flightObj.getFlightSegements())) {
				if (fs.getEstTimeDepatureZulu() != null && fs.getSegmentCode().startsWith(meta.getBoardingairport())) {
					departureDateZulu = fs.getEstTimeDepatureZulu();
					break;
				}
			}
		}
		flightInfo.setDepartureTime(departureDateZulu);

		dcsadlInfoRQ.setFlightInfo(flightInfo);
		List<DCSFlightSegmentMapElement> deletedSegmentInfo = new ArrayList<DCSFlightSegmentMapElement>();
		List<DCSFlightSegmentMapElement> addedSegmentInfo = new ArrayList<DCSFlightSegmentMapElement>();
		List<DCSFlightSegmentMapElement> updatedSegmentInfo = new ArrayList<DCSFlightSegmentMapElement>();

		Map<String, Map<String, List<ADLRecordDTO>>> adlAddPaxMap = new HashMap<String, Map<String, List<ADLRecordDTO>>>();
		Map<String, Map<String, List<ADLRecordDTO>>> adlDelPaxMap = new HashMap<String, Map<String, List<ADLRecordDTO>>>();
		Map<String, Map<String, List<ADLRecordDTO>>> adlChangePaxMap = new HashMap<String, Map<String, List<ADLRecordDTO>>>();
		int index = 0;
		for (PassengerCountRecordDTO pcrDTO : adlData.get(index)) {
			String segCode = PnlAdlUtil.findMatchingFlightSegmentcode(flightId, meta.getBoardingairport(),
					pcrDTO.getDestination());

			// preparing add data
			if (adlAddPaxMap.get(pcrDTO.getFareClass()) == null) {
				Map<String, List<ADLRecordDTO>> addMap = new HashMap<String, List<ADLRecordDTO>>();
				addMap.put(segCode, pcrDTO.getAddpnlrecords());
				adlAddPaxMap.put(pcrDTO.getFareClass(), addMap);
			} else {
				if (adlAddPaxMap.get(pcrDTO.getFareClass()).get(segCode) == null) {
					adlAddPaxMap.get(pcrDTO.getFareClass()).put(segCode, pcrDTO.getAddpnlrecords());
				} else {
					// Just in case
					adlAddPaxMap.get(pcrDTO.getFareClass()).get(segCode).addAll(pcrDTO.getAddpnlrecords());
				}
			}
			// preparing del data
			if (adlDelPaxMap.get(pcrDTO.getFareClass()) == null) {
				Map<String, List<ADLRecordDTO>> delMap = new HashMap<String, List<ADLRecordDTO>>();
				delMap.put(segCode, pcrDTO.getDelpnlrecords());
				adlDelPaxMap.put(pcrDTO.getFareClass(), delMap);
			} else {
				if (adlDelPaxMap.get(pcrDTO.getFareClass()).get(segCode) == null) {
					adlDelPaxMap.get(pcrDTO.getFareClass()).put(segCode, pcrDTO.getDelpnlrecords());
				} else {
					// Just in case
					adlDelPaxMap.get(pcrDTO.getFareClass()).get(segCode).addAll(pcrDTO.getDelpnlrecords());
				}
			}
			// preparing change data
			if (adlChangePaxMap.get(pcrDTO.getFareClass()) == null) {
				Map<String, List<ADLRecordDTO>> changeMap = new HashMap<String, List<ADLRecordDTO>>();
				changeMap.put(segCode, pcrDTO.getChgRecords());
				adlChangePaxMap.put(pcrDTO.getFareClass(), changeMap);
			} else {
				if (adlChangePaxMap.get(pcrDTO.getFareClass()).get(segCode) == null) {
					adlChangePaxMap.get(pcrDTO.getFareClass()).put(segCode, pcrDTO.getChgRecords());
				} else {
					// Just in case
					adlChangePaxMap.get(pcrDTO.getFareClass()).get(segCode).addAll(pcrDTO.getChgRecords());
				}
			}
			index++;
		}

		this.compileADLDataList(adlAddPaxMap, addedSegmentInfo, flightId, pnrPaxIdMealMap, pnrPaxIdBaggageMap, pnrPaxIdSeatMap,
				pnrPaxIdSSRMap);
		this.compileADLDataList(adlDelPaxMap, deletedSegmentInfo, flightId, pnrPaxIdMealMap, pnrPaxIdBaggageMap, pnrPaxIdSeatMap,
				pnrPaxIdSSRMap);
		this.compileADLDataList(adlChangePaxMap, updatedSegmentInfo, flightId, pnrPaxIdMealMap, pnrPaxIdBaggageMap,
				pnrPaxIdSeatMap, pnrPaxIdSSRMap);
		dcsadlInfoRQ.getAddedSegmentInfo().addAll(addedSegmentInfo);
		dcsadlInfoRQ.getDeletedSegmentInfo().addAll(deletedSegmentInfo);
		dcsadlInfoRQ.getUpdatedSegmentInfo().addAll(updatedSegmentInfo);

		return dcsadlInfoRQ;
	}

	@Deprecated
	private void compileADLDataList(Map<String, Map<String, List<ADLRecordDTO>>> adlDataPaxMap,
			List<DCSFlightSegmentMapElement> adlDataSegmentInfo, Integer flightId, Map<Integer, String> pnrPaxIdMealMap,
			Map<Integer, String> pnrPaxIdBaggageMap, Map<Integer, String> pnrPaxIdSeatMap,
			Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxIdSSRMap) throws ModuleException {
		for (String fareClass : adlDataPaxMap.keySet()) {
			DCSFlightSegmentMapElement dcsFSME = new DCSFlightSegmentMapElement();
			dcsFSME.setCabinClassCode(fareClass);

			Map<String, List<ADLRecordDTO>> dataMap = adlDataPaxMap.get(fareClass);
			for (String segCode : dataMap.keySet()) {
				Map<String, List<ADLRecordDTO>> pnrPaxMap = new HashMap<String, List<ADLRecordDTO>>();
				DCSFlightSegment dcsFS = new DCSFlightSegment();
				dcsFS.setSegmentCode(segCode);
				List<ADLRecordDTO> adlDataList = dataMap.get(segCode);
				if (adlDataList != null) {
					for (ADLRecordDTO adlRec : adlDataList) {
						if (pnrPaxMap.get(adlRec.getAutomatedPNR()) == null) {
							List<ADLRecordDTO> pnrPax = new ArrayList<ADLRecordDTO>();
							pnrPax.add(adlRec);
							pnrPaxMap.put(adlRec.getAutomatedPNR(), pnrPax);

						} else {
							pnrPaxMap.get(adlRec.getAutomatedPNR()).add(adlRec);
						}
					}
					for (String pnr : pnrPaxMap.keySet()) {
						PassengersMapElement pme = new PassengersMapElement();
						pme.setPnr(pnr);
						List<ADLRecordDTO> adldtos = pnrPaxMap.get(pnr);

						for (ADLRecordDTO adldto : adldtos) {
							for (ADLPaxNameInfoDTO adlPaxName : getPaxNamesFromADLRec(adldto)) {
								ReservationPax rpa = ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPassenger(
										adlPaxName.getPnrPaxId(), false, false, false);
								DCSPassenger dp = new DCSPassenger();
								dp.setTitle(adlPaxName.getTitle());
								dp.setFirstName(adlPaxName.getFirstName());
								dp.setLastName(adlPaxName.getLastName());
								dp.setPaxType(rpa.getPaxType());
								dp.setDateOfBirth(rpa.getDateOfBirth());
								dp.setStandByPax(adldto.isHasIDpax());
								if (rpa.getNationalityCode() != null) {
									Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(
											rpa.getNationalityCode());
									if (nat != null && nat.getIsoCode() != null) {
										dp.setNationality(nat.getIsoCode());
									}
								}
								DCSPassportInformation dpi = new DCSPassportInformation();
								DCSVisaInformation dvi = new DCSVisaInformation();
								if (rpa.getPaxAdditionalInfo() != null) {
									dpi.setIssuedCountry(rpa.getPaxAdditionalInfo().getPassportIssuedCntry());
									dpi.setPassportExpiryDate(rpa.getPaxAdditionalInfo().getPassportExpiry());
									dpi.setPassportNumber(rpa.getPaxAdditionalInfo().getPassportNo());
									
									if (rpa.getPaxAdditionalInfo().getVisaDocNumber() != null) {
										dvi.setVisaDocNumber(rpa.getPaxAdditionalInfo().getVisaDocNumber());

										if (rpa.getPaxAdditionalInfo().getPlaceOfBirth() != null) {
											dvi.setPlaceOfBirth(rpa.getPaxAdditionalInfo().getPlaceOfBirth());
										}
										if (rpa.getPaxAdditionalInfo().getVisaDocPlaceOfIssue() != null) {
											dvi.setVisaDocPlaceOfIssue(rpa.getPaxAdditionalInfo().getVisaDocPlaceOfIssue());
										}
										if (rpa.getPaxAdditionalInfo().getVisaApplicableCountry() != null) {
											dvi.setVisaApplicableCountry(rpa.getPaxAdditionalInfo().getVisaApplicableCountry());
										}
										if (rpa.getPaxAdditionalInfo().getVisaDocIssueDate() != null) {
											dvi.setVisaDocIssueDate(rpa.getPaxAdditionalInfo().getVisaDocIssueDate());
										}
										if (rpa.getPaxAdditionalInfo().getTravelDocumentType() != null) {
											dvi.setTravelDocumentType(rpa.getPaxAdditionalInfo().getTravelDocumentType());
										}
									}
								}
								dp.setPassportInfo(dpi);
								dp.setVisaInfo(dvi);

								EticketTO eTicket = eTicketDAO.getPassengerETicketDetail(adlPaxName.getPnrPaxId(), flightId);
								dp.setETicket(eTicket.getEticketNumber());
								Set<ReservationPax> cset = rpa.getInfants();
								Iterator<ReservationPax> ite = cset.iterator();
								while (ite.hasNext()) {
									ReservationPax infant = (ite.next());
									DCSPassenger dpInfant = new DCSPassenger();
									dpInfant.setTitle(infant.getTitle());
									dpInfant.setFirstName(infant.getFirstName());
									dpInfant.setLastName(infant.getLastName());
									dpInfant.setPaxType(infant.getPaxType());
									if (infant.getNationalityCode() != null) {
										Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(
												infant.getNationalityCode());
										if (nat != null && nat.getIsoCode() != null) {
											dpInfant.setNationality(nat.getIsoCode());
										}
									}
									dpInfant.setDateOfBirth(infant.getDateOfBirth());
									EticketTO infETicket = eTicketDAO.getPassengerETicketDetail(infant.getPnrPaxId(), flightId);
									dpInfant.setETicket(infETicket.getEticketNumber());
									dp.setInfant(dpInfant);
								}
								PnlAdlUtil.compileMealDetails(pnrPaxIdMealMap, adlPaxName.getPnrPaxId(), dp);
								PnlAdlUtil.compileSeatDetails(pnrPaxIdSeatMap, adlPaxName.getPnrPaxId(), dp);
								PnlAdlUtil.compileBaggageDetails(pnrPaxIdBaggageMap, adlPaxName.getPnrPaxId(), dp);
								PnlAdlUtil.compileSsrDetails(pnrPaxIdSSRMap, adlPaxName.getPnrPaxId(), dp);
								pme.getPassengers().add(dp);
							}
						}
						dcsFS.getPnrAndPassengers().add(pme);
					}
				}
				dcsFSME.getFlightSegments().add(dcsFS);
			}
			adlDataSegmentInfo.add(dcsFSME);
		}
	}

	private List<ADLPaxNameInfoDTO> getPaxNamesFromADLRec(ADLRecordDTO adlRec) {
		List<ADLPaxNameInfoDTO> adlPaxNames = new ArrayList<ADLPaxNameInfoDTO>();
		if (adlRec.getNames() != null) {
			for (NameDTO nameDTO : adlRec.getNames()) {
				ADLPaxNameInfoDTO adlPaxName = new ADLPaxNameInfoDTO();
				adlPaxName.setTitle(nameDTO.getTitle());
				adlPaxName.setLastName(adlRec.getLastname());
				adlPaxName.setFirstName(nameDTO.getFirstname());
				adlPaxName.setPnrPaxId(nameDTO.getPnrPaxId());

				adlPaxNames.add(adlPaxName);
			}
		}
		return adlPaxNames;
	}

	@Deprecated
	private void sendADLtoDCS(DCSADLInfoRQ dcsadlInfoRQ, String airportCode, int flightId) throws ModuleException {

		DCSADLInfoRS dcsadlInfoRS = ReservationModuleUtils.getDCSClientBD().sendADLToDCS(dcsadlInfoRQ);
		if (dcsadlInfoRS.getResponseAttributes().getSuccess() != null) {
			// update the passengers adl status
			// updateAdlStatus(adlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode);
			// update history
			PnlAdlHistory adlHistory = new PnlAdlHistory();
			adlHistory.setAirportCode(airportCode);
			adlHistory.setEmail("DCS_WS");
			adlHistory.setFlightID(flightId);
			adlHistory.setMessageType("ADL");// ADL
			adlHistory.setTransmissionStatus("Y");
			adlHistory.setTransmissionTimeStamp(currentTimeStamp);
			ReservationModuleUtils.getReservationAuxilliaryBD().saveDCSPnlAdlHistory(adlHistory);
		}
	}

}