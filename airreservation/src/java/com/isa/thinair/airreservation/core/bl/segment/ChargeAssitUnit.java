package com.isa.thinair.airreservation.core.bl.segment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.FlightCOSDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.SurfaceSegmentUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Assisting unit for the charge calculation related functionality in modification flows
 * 
 * This should contain only the supporting functionalities and all the business related should go inside BL and BO
 * 
 * @author Dilan Anuruddha
 * 
 */
public class ChargeAssitUnit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isForModification;
	private boolean applyCnxCharge;
	private boolean applyModCharge;
	private long version;
	private Collection<Integer> cnPnrSegIds;
	private boolean isRequote;
	private ModifyAssitUnit modifyAsst;
	private Collection<Integer> colSetInversePnrSegIds;
	private Collection<Integer> flownPnrSegIds;
	private Map<Integer, FlightCOSDTO> newFltCOSMap;
	private Collection<Integer> newFltSegIds;

	public ChargeAssitUnit(Reservation reservation, Collection<Integer> cnxPnrSegIds, Collection<Integer> orderdNewFlgSegIds,
			boolean isRequote, boolean isForModification, boolean applyCnxCharge, boolean applyModCharge, long version,
			boolean isVoidOperation, Map<Integer, FlightCOSDTO> newFltCOSMap) throws ModuleException {
		this.isForModification = isForModification;
		this.applyCnxCharge = applyCnxCharge;
		this.applyModCharge = applyModCharge;
		this.isRequote = isRequote;
		this.version = version;
		this.newFltSegIds = orderdNewFlgSegIds;
		this.newFltCOSMap = newFltCOSMap;
		this.modifyAsst = new ModifyAssitUnit(cnxPnrSegIds, orderdNewFlgSegIds, reservation, isVoidOperation);
	}

	public void validate() throws ModuleException {
		// For Modification new flight segment ids should be there always
		if (!isRequote) {
			if (isForModification && isModifyProrate()) {
				if (!modifyAsst.hasNewFltSegments()) {
					throw new ModuleException("airreservations.cancellation.noNewFlgSegIds");
				}
			}
		}

		checkConstraints(getReservation(), getCnxPnrSegIds(), version);
	}

	public Reservation getReservation() throws ModuleException {
		return modifyAsst.getReservation();
	}

	private void checkConstraints(Reservation reservation, Collection<Integer> perOndSegmentIds, long version)
			throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);

		// Checking to see if any restricted fare exist
		ValidationUtils.checkRestrictedSegmentConstraints(reservation, perOndSegmentIds,
				"airreservations.cancellation.restrictedFareSegment");
	}

	public boolean isVoidReservation() throws ModuleException {
		return modifyAsst.isVoidReservation();
	}

	public boolean isSameFlightModification() throws ModuleException {
		boolean isSameFlightMod = modifyAsst.isSameFlightModification();
		if (isSameFlightMod && AppSysParamsUtil.isApplyModChargeForSameFlightHighCOSToLowCOS()) {
			return isSameFlightCOSUpgrade();
		}
		return isSameFlightMod;
	}

	private Collection<Integer> getInversePnrSegIds() throws ModuleException {
		if (colSetInversePnrSegIds == null) {
			this.colSetInversePnrSegIds = CancellationUtils.getInversePnrSegIdsForMultipleOnds(getCnxPnrSegIds(),
					getReservation());
		}
		return colSetInversePnrSegIds;
	}

	public Collection<Integer> getCnxLinkedSurfacePnrSegIds() throws ModuleException {
		return SurfaceSegmentUtil.getLinkedSurfaceSegments(getReservation().getSegments(), getCnxPnrSegIds());
	}

	private boolean isOpenReturnInverseSegments(Collection<Integer> colSetInversePnrSegIds) throws ModuleException {
		return CancellationUtils.isOpenReturnInverseSegments(getReservation(), colSetInversePnrSegIds);
	}

	/**
	 * This is not the same thing as the cnx
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Integer> getCnxPnrSegIds() throws ModuleException {
		if (cnPnrSegIds == null) {
			if (modifyAsst.hasCnxPnrSegments()) {
				this.cnPnrSegIds = CancellationUtils.getAllPnrSegIdsForSameOndGroup(modifyAsst.getCnxPnrSegIds(),
						getReservation());
			} else {
				this.cnPnrSegIds = new ArrayList<Integer>();
			}
		}
		return cnPnrSegIds;

	}

	public Collection<Integer> getFlownPnrSegIds() throws ModuleException {
		if (flownPnrSegIds == null) {
			if (isRequote && modifyAsst.hasFlownPnrSegments()) {
				flownPnrSegIds = CancellationUtils.getAllPnrSegIdsForSameOndGroup(modifyAsst.getFlownPnrSegIds(),
						getReservation());
			} else {
				flownPnrSegIds = new ArrayList<Integer>();
			}
		}
		return flownPnrSegIds;
	}

	public Collection<Integer> getExchangedPnrSegIds() throws ModuleException {
		Collection<Integer> cnxPnrSegIds = getCnxPnrSegIds();
		Collection<Integer> flownPnrSegIds = getFlownPnrSegIds();
		Collection<Integer> exgPnrSegIds = new ArrayList<Integer>(getUserExchangedPnrSegIds());
		exgPnrSegIds.removeAll(cnxPnrSegIds);
		exgPnrSegIds.removeAll(flownPnrSegIds);
		return exgPnrSegIds;
	}

	public boolean isApplyModCharge() {
		return applyModCharge;
	}

	public boolean isApplyCnxCharge() {
		return applyCnxCharge;
	}

	public List<ReservationPaxFare> getTargetPaxFare(Set<ReservationPaxFare> pnrPaxFares) throws ModuleException {
		if (modifyAsst.hasCnxPnrSegments()) {
			return ReservationCoreUtils.getPnrPaxFares(pnrPaxFares, getCnxPnrSegIds());
		}
		return null;
	}

	private boolean hasInverseSegments() throws ModuleException {
		return (getInversePnrSegIds() != null && getInversePnrSegIds().size() > 0);
	}

	public List<ReservationPaxFare> getAffectedOpenRTPaxFare(Set<ReservationPaxFare> pnrPaxFares) throws ModuleException {
		if (hasInverseSegments() && isOpenReturnInverseSegments(getInversePnrSegIds())) {
			return ReservationCoreUtils.getPnrPaxFares(pnrPaxFares, getInversePnrSegIds());
		}
		return null;
	}

	public List<ReservationPaxFare> getAffectedPaxFare(Set<ReservationPaxFare> pnrPaxFares) throws ModuleException {
		if (!isRequote && hasInverseSegments() && !isOpenReturnInverseSegments(getInversePnrSegIds())) {
			return ReservationCoreUtils.getPnrPaxFares(pnrPaxFares, getInversePnrSegIds());
		}
		return null;
	}

	public List<ReservationPaxFare> getExchangedPaxFare(Set<ReservationPaxFare> pnrPaxFares) throws ModuleException {
		if (isRequote) {
			Collection<Integer> exgPnrSegIds = getExchangedPnrSegIds();
			if (exgPnrSegIds != null && exgPnrSegIds.size() > 0) {
				return ReservationCoreUtils.getPnrPaxFares(pnrPaxFares, exgPnrSegIds);
			}
		}
		return null;
	}

	public void adjustOndFares(List<ReservationPaxFare> targetList, List<ReservationPaxFare> affectedList) throws ModuleException {
		if (!isRequote && hasInverseSegments() && targetList != null && affectedList != null
				&& affectedList.size() == targetList.size()) {
			// skip fare adjustments for half return fare modifications
			int i = 0;
			for (ReservationPaxFare target : targetList) {
				ReservationPaxFare affected = affectedList.get(i++);
				if (!(isModifyProrate() && !target.getFareId().equals(affected.getFareId()))) {
					CancellationUtils.adjustFareAmongOnds(target, affected, null, null, null, isModifyProrate());
				}
			}
		}

	}

	public void adjustOndFares(List<ReservationPaxFare> targetList, List<ReservationPaxFare> affectedList,
			Map<Collection<Integer>, String> mapPnrSegIdsAndChanges) throws ModuleException {
		if (mapPnrSegIdsAndChanges != null) {
			if (!isRequote && hasInverseSegments() && targetList != null && affectedList != null
					&& affectedList.size() == targetList.size()) {
				// skip fare adjustments for half return fare modifications
				int i = 0;
				for (ReservationPaxFare target : targetList) {
					ReservationPaxFare affected = affectedList.get(i++);
					if (!(isModifyProrate() && !target.getFareId().equals(affected.getFareId()))) {
						CancellationUtils.adjustFareAmongOnds(target, affected, mapPnrSegIdsAndChanges, getCnxPnrSegIds(),
								getInversePnrSegIds(), isModifyProrate());
					}
				}
			}
		} else {
			adjustOndFares(targetList, affectedList);
		}

	}

	private boolean isModifyProrate() throws ModuleException {
		if (isRequote)
			return false;
		return CancellationUtils.isModifyProrate(getCnxPnrSegIds(), getInversePnrSegIds(), modifyAsst.getNewFltSegIds(),
				getReservation().getSegmentsView());
	}

	public List<ReservationPaxFare> getLinkedGroundFareSegments(Set<ReservationPaxFare> pnrPaxFares) throws ModuleException {
		Collection<Integer> linkedSurfaceSegs = getCnxLinkedSurfacePnrSegIds();
		if (linkedSurfaceSegs != null && linkedSurfaceSegs.size() > 0) {
			return ReservationCoreUtils.getPnrPaxFares(pnrPaxFares, linkedSurfaceSegs);
		}
		return null;
	}

	public Collection<Integer> getAllPnrSegmentsToBeMarkedCancelled() throws ModuleException {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		Collection<Integer> cnxPnrSegs = getCnxPnrSegIds();
		Collection<Integer> cnxOprnRTSegs = getCnxOpenReturnPnrSegIds();
		Collection<Integer> exchPnrSegs = getUserExchangedPnrSegIds();
		Collection<Integer> flownPnrSegs = getFlownPnrSegIds();

		if (cnxPnrSegs != null) {
			pnrSegIds.addAll(cnxPnrSegs);
			// Collect linked surface segments
			Collection<Integer> linkedSurfaceSegments = getLinkedSurfaceSegmentIds(cnxPnrSegs);
			if (linkedSurfaceSegments != null) {
				pnrSegIds.addAll(linkedSurfaceSegments);
			}
		}
		if (exchPnrSegs != null) {
			pnrSegIds.addAll(exchPnrSegs);
		}

		if (cnxOprnRTSegs != null) {
			pnrSegIds.addAll(cnxOprnRTSegs);
		}

		if (flownPnrSegs != null) {
			pnrSegIds.removeAll(flownPnrSegs);
		}

		return pnrSegIds;
	}

	public Collection<Integer> getUserCancelledPnrSegIds() throws ModuleException {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		Collection<Integer> cnxPnrSegs = modifyAsst.getCnxPnrSegIds();
		// linked pnr seg is not accurate.. probably have to fix before bus enabled customer deployment
		// FIXME DILAN
		Collection<Integer> lnkdPnrSegIds = getCnxLinkedSurfacePnrSegIds();
		if (cnxPnrSegs != null) {
			pnrSegIds.addAll(cnxPnrSegs);
		}
		if (lnkdPnrSegIds != null) {
			pnrSegIds.addAll(lnkdPnrSegIds);
		}
		if (isOpenReturnInverseSegments(getInversePnrSegIds())) {
			pnrSegIds.addAll(getInversePnrSegIds());
		}

		return pnrSegIds;
	}

	public Collection<Integer> getAllCancellingPnrSegIds() throws ModuleException {
		Collection<Integer> allPnrSegIds = new ArrayList<Integer>();
		allPnrSegIds.addAll(getUserCancelledPnrSegIds());
		allPnrSegIds.addAll(getUserExchangedPnrSegIds());
		return allPnrSegIds;
	}

	public Collection<Integer> getUserExchangedPnrSegIds() throws ModuleException {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		Collection<Integer> excPnrSegIds = modifyAsst.getExchangedPnrSegIds();
		if (isRequote && excPnrSegIds != null) {
			pnrSegIds.addAll(excPnrSegIds);
		}
		return pnrSegIds;
	}

	public boolean isReservationCancelled() throws ModuleException {
		if (!modifyAsst.hasNewFltSegments() && modifyAsst.hasCnxPnrSegments()) {
			int cnfPnrSegCount = 0;
			for (ReservationSegment resSeg : getReservation().getSegments()) {
				if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					if (!getCnxPnrSegIds().contains(resSeg.getPnrSegId())) {
						cnfPnrSegCount++;
					}
				}
			}
			if (cnfPnrSegCount == 0 && !applyModCharge) {
				return true;
			}
		}
		return false;
	}

	public Collection<Integer> getCnxOpenReturnPnrSegIds() throws ModuleException {
		if (hasInverseSegments() && isOpenReturnInverseSegments(getInversePnrSegIds())) {
			return getInversePnrSegIds();
		}
		return null;
	}

	public Collection<Integer> getLinkedSurfaceSegmentIds(Collection<Integer> pnrSegIds) throws ModuleException {
		Collection<Integer> linkedsurfaceSegments = null;
		if (AppSysParamsUtil.isGroundServiceEnabled()) {
			linkedsurfaceSegments = SurfaceSegmentUtil.getLinkedSurfaceSegments(getReservation().getSegments(), pnrSegIds);
		}
		return linkedsurfaceSegments;
	}

	public boolean hasUserCnxPnrSegIds() throws ModuleException {
		return (getUserCancelledPnrSegIds().size() > 0);
	}

	public boolean skipCancelSegmentOperations() throws ModuleException {
		return (getAllPnrSegmentsToBeMarkedCancelled().size() == 0);
	}

	public boolean skipCancelRevAccounting() throws ModuleException {
		Collection<Integer> exgPnrSegIds = getExchangedPnrSegIds();
		Collection<Integer> flownPnrSegIds = getFlownPnrSegIds();
		if (flownPnrSegIds.containsAll(exgPnrSegIds)) {
			return false;
		}
		return true;
	}

	private boolean isSameFlightCOSUpgrade() {
		if (newFltSegIds != null && newFltSegIds.size() > 0 && newFltCOSMap != null && newFltCOSMap.size() > 0) {
			Iterator<Integer> newFltSegIdsItr = newFltSegIds.iterator();
			while (newFltSegIdsItr.hasNext()) {
				Integer newFltSegId = newFltSegIdsItr.next();
				if (newFltCOSMap.containsKey(newFltSegId)) {
					FlightCOSDTO flightCOSDTO = newFltCOSMap.get(newFltSegId);
					if (!flightCOSDTO.isCOSUpperOrSame()) {
						return false;
					}

				}

			}
		}
		return true;
	}

	public Collection<Integer> getAllNonFlownPnrSegIds() throws ModuleException {
		Collection<Integer> allPnrSegIds = new ArrayList<Integer>();
		allPnrSegIds.addAll(getUserCancelledPnrSegIds());
		allPnrSegIds.addAll(getUserExchangedPnrSegIds());
		allPnrSegIds.removeAll(getFlownPnrSegIds());
		return allPnrSegIds;
	}
}
