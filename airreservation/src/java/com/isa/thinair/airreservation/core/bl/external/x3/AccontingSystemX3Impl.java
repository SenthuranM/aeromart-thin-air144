/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.external.x3;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardSalesStatusDTO;
import com.isa.thinair.airreservation.api.dto.external.AgentTopUpX3DTO;
import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentSageDTO;
import com.isa.thinair.airreservation.api.model.AgentTopUp;
import com.isa.thinair.airreservation.api.model.CreditCardSalesSage;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
//import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.airreservation.core.bl.external.AccountingSystem;
import com.isa.thinair.airreservation.core.bl.external.LatestFirstSortComparatorCCSales;
import com.isa.thinair.airreservation.core.persistence.dao.AgentTopUpTransferDAO;
import com.isa.thinair.airreservation.core.persistence.dao.CreditSalesTransferDAO;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AccontingSystemX3Impl extends AccontingSystemTemplate implements AccountingSystem {

	private static final Log log = LogFactory.getLog(AccontingSystemX3Impl.class);

	/*
	 * Transfers credit card sales to X3 accounting System
	 * 
	 * @Param date
	 * 
	 * @throws ModuleException
	 */
	@Override
	public void transferCreditSales(Date date) throws ModuleException {

		log.info("########### Generating and Transferring CC Sales X3 for:" + date);
		CreditSalesTransferDAO creditSalesTransferDAO = (CreditSalesTransferDAO) ReservationDAOUtils.DAOInstance.x3SystemDAO;
		Collection<CreditCardPaymentSageDTO> colDTOs = null;
		Collection<CreditCardSalesSage> creditCardSalesPOJOs = null;
		try {
			creditSalesTransferDAO.clearCreditCardSalesTable(date);
			colDTOs = creditSalesTransferDAO.getCreditCardPayments(date);
		} catch (CommonsDataAccessException dae) {
			log.error("######### Error in clearing or retriving cc sales X3 :" + date);
			throw new CommonsDataAccessException(dae, "airreservations.auxilliary.getcreditsales",
					AirreservationConstants.MODULE_NAME);
		}

		if (colDTOs != null) {
			try {
				log.debug("######## Going to insert cc sales X3 to internal :" + date);
				creditCardSalesPOJOs = creditSalesTransferDAO.insertCreditCardSalesToInternal(colDTOs);
				log.debug("######## After inserting cc sales to internal :" + date);
			} catch (CommonsDataAccessException dae) {
				log.error("############ Failed when inserting CC sales X3 to internal " + date);
				throw new CommonsDataAccessException(dae, "airreservations.auxilliary.transferinternalcreditsys",
						AirreservationConstants.MODULE_NAME);
			}

			String transferToExternal = ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer();
			if (transferToExternal == null
					|| !(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM.equalsIgnoreCase(transferToExternal))) {
				throw new ModuleException("airreservation.exdb.notconfigured");
			}

			try {
				log.debug("###### Before inserting CC sales X3 to XDB: " + date);
				creditSalesTransferDAO.insertCreditCardSalesToExternal(colDTOs, (AccontingSystemTemplate) this);
				log.debug("###### After inserting CC sales X3 to XDB: " + date);
			} catch (SQLException me) {
				log.error("Failed when Transferring to XDB :", me);
				if (me.getErrorCode() == 2627)
					throw new ModuleException(me, "reservationauxilliary.externaltable.integrityviolation",
							AirreservationConstants.MODULE_NAME);
				else
					throw new ModuleException(me, "reservationauxilliary.externaltable.databaseunavailable",
							AirreservationConstants.MODULE_NAME);

			} catch (ClassNotFoundException nfe) {
				log.error("Transfer Credit Sales/insertCreditCardSalestoExternal Failed...");
				throw new ModuleException(nfe, "reservationauxilliary.externaltable.integrityviolation",
						AirreservationConstants.MODULE_NAME);
			} catch (Exception e) {
				log.error("Transfering to Interface Tables Failed", e);
				throw new ModuleException(e, "transfer.creditcardsales.failed", AirreservationConstants.MODULE_NAME);

			}

			try {
				log.debug("###### Before Updating Status in CC sales X3: " + date);
				creditSalesTransferDAO.updateInternalCreditTable(creditCardSalesPOJOs);
				log.debug("###### After Updating Status in CC sales X3 : " + date);
			} catch (CommonsDataAccessException dae) {
				log.error("Transfer X3 Credit Sales/update External Failed..");
				throw new CommonsDataAccessException(dae, "airreservations.auxilliary.transferinternalcreditsys",
						AirreservationConstants.MODULE_NAME);

			}
		}

	}

	
	private boolean hasEXDB() {
		if (ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer().equalsIgnoreCase("true")) {
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * CreditCard sales details
	 * 
	 * @Param cards
	 * 
	 * @Param fromDate
	 * 
	 * @Param toDate
	 * 
	 * @throws ModuleException
	 * 
	 * @Return CreditCardSalesStatusDTO
	 */
	@Override
	public CreditCardSalesStatusDTO getCreditCardSalesDetails(Integer cards[], Date fromDate, Date toDate) throws ModuleException {

		CreditSalesTransferDAO salesDAO = (CreditSalesTransferDAO) ReservationDAOUtils.DAOInstance.x3SystemDAO;
		CreditCardSalesStatusDTO creditCardSalesstatusDTO = new CreditCardSalesStatusDTO();
		creditCardSalesstatusDTO.setCreditSales(getCreditCardSalesHistory(fromDate, toDate));

		creditCardSalesstatusDTO.setTotAmtGenerated(salesDAO.getGeneratedCreditCardSalesTotal(fromDate, toDate));
		if (hasEXDB()) {
			creditCardSalesstatusDTO.setHasEXDB(true);
			creditCardSalesstatusDTO.setTotAmtTransfered(salesDAO.getTransferedCreditCardSalesTotal(fromDate, toDate,
					(AccontingSystemTemplate) this));
		} else {
			creditCardSalesstatusDTO.setHasEXDB(false);
		}

		return creditCardSalesstatusDTO;
	}

	/*
	 * CreditCard sales history
	 * 
	 * @Param fromDate
	 * 
	 * @Param toDate
	 * 
	 * @Return ArrayList<CCSalesHistoryDTO>
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<CCSalesHistoryDTO> getCreditCardSalesHistory(Date fromDate, Date toDate) {
		CreditSalesTransferDAO salesDAO = (CreditSalesTransferDAO) ReservationDAOUtils.DAOInstance.x3SystemDAO;

		// will return 0-a list of date objects 1-a list of dto's
		Object[] o = salesDAO.getCreditCardSalesHistory(fromDate, toDate);
		// hold the startdate
		GregorianCalendar calendarDate = new GregorianCalendar();
		calendarDate.setTime(fromDate);
		// hold the enddate
		GregorianCalendar calendarDateEnd = new GregorianCalendar();
		calendarDateEnd.setTime(toDate);

		// get the objects from the object array
		ArrayList<CCSalesHistoryDTO> dtoList = (ArrayList<CCSalesHistoryDTO>) o[0];
		ArrayList<?> dateList = (ArrayList<?>) o[1];

		// for all the dates..i.e from start to end, see if retrieved from the database
		// if not create your own dto, add it to the dto list and send it to f.e.
		while (calendarDate.before(calendarDateEnd) || calendarDate.equals(calendarDateEnd)) {

			if (!(dateList.contains(calendarDate.getTime()))) {
				CCSalesHistoryDTO historyDTO = new CCSalesHistoryDTO();
				historyDTO.setDateOfsale(calendarDate.getTime());
				historyDTO.setCardType("Not Generated");
				historyDTO.setTotalDailySales(AccelAeroCalculator.getDefaultBigDecimalZero());
				historyDTO.setTransferStatus("N");
				dtoList.add(historyDTO);
			}

			calendarDate.add(GregorianCalendar.DATE, +1);
		}
		// sort by date
		Collections.sort(dtoList, new LatestFirstSortComparatorCCSales());
		return dtoList;

	}	
	

	@Override
	public void transferTopUps(Date date) throws ModuleException {

		log.info("########### Generating and Transferring TopUp X3 for:" + date);

		AgentTopUpTransferDAO agentTopUpTransferDAO = (AgentTopUpTransferDAO) ReservationDAOUtils.DAOInstance.x3AgentTopUpDAO;
		Collection<AgentTopUpX3DTO> colDTOs = null;
		Collection<AgentTopUp> agentTopUpPOJOs = null;
		try {
			agentTopUpTransferDAO.clearAgentTopUpTable(date);
			log.info("########### clearAgentTopUpTable () : DONE ");

			colDTOs = agentTopUpTransferDAO.getTopUps(date);
			log.info("########### getTopUps () : DONE ");
		} catch (CommonsDataAccessException dae) {
			log.error("######### Error in clearing or retriving agent TopUp X3 :" + date);
			throw new CommonsDataAccessException(dae, "airreservations.auxilliary.getagenttopup",
					AirreservationConstants.MODULE_NAME);
		}

		if (colDTOs != null) {
			try {
				agentTopUpPOJOs = agentTopUpTransferDAO.insertTopUpToInternal(colDTOs);
				log.info("########  insert agent Top up to internal : DONE :" + date);

			} catch (CommonsDataAccessException dae) {
				log.error("############ Failed when inserting  agent Top up X3 to internal " + date);
				throw new CommonsDataAccessException(dae, "airreservations.auxilliary.transferinternalagenttopupys",
						AirreservationConstants.MODULE_NAME);
			}

			String transferToExternal = ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer();
			if (!(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM.equalsIgnoreCase(transferToExternal))) {
				throw new ModuleException("airreservation.exdb.notconfigured");
			}

			try {
				agentTopUpTransferDAO.insertTopUpToExternal(colDTOs, (AccontingSystemTemplate) this);
				log.debug("######  insert  agent Top up X3 to XDB: DONE : " + date);
			} catch (SQLException me) {
				log.error("Failed when Transferring to XDB :", me);
				if (me.getErrorCode() == MYSQL_DUPLICATE_PK) {

					throw new ModuleException(me, "reservationauxilliary.externaltable.integrityviolation",
							AirreservationConstants.MODULE_NAME);
				} else {
					throw new ModuleException(me, "reservationauxilliary.externaltable.databaseunavailable",
							AirreservationConstants.MODULE_NAME);
				}

			} catch (ClassNotFoundException nfe) {
				log.error("Transfer Agent TopUp /insert Agent TopUp to External Failed...");
				throw new ModuleException(nfe, "reservationauxilliary.externaltable.integrityviolation",
						AirreservationConstants.MODULE_NAME);
			} catch (Exception e) {
				log.error("Transfering to Interface Tables Failed", e);
				throw new ModuleException(e, "transfer.agenttopup.failed", AirreservationConstants.MODULE_NAME);

			}

			try {
				agentTopUpTransferDAO.updateInternalTopUpTable(date);
				log.debug("###### Updated Status in internal table Agent TopUp X3 : DONE : " + date);
			} catch (Exception dae) {
				log.error("Transfer X3 agent topup/update External Failed..");
				throw new CommonsDataAccessException(dae, "airreservations.auxilliary.transferinternalagenttopupsys",
						AirreservationConstants.MODULE_NAME);

			}


		}

	}

}
