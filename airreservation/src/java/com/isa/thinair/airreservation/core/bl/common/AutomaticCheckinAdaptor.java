package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutoCheckinUtil;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckin;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckinSeat;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author aravinth.r
 *
 */
public class AutomaticCheckinAdaptor {

	// this two will be updated by scheduler
	private static final String DCS_STATUS = "NS";
	private static final String DCS_RES = "Not Sent";

	/**
	 * Saving passanger's autocheckin details
	 * 
	 * @param automaticCheckinTOs
	 * @param credentialsDTO
	 * @return
	 */
	public static Collection<ReservationPaxSegAutoCheckin> savePaxAutoCheckinDetails(
			Map<String, Collection<PaxAutomaticCheckinTO>> automaticCheckinTOs, CredentialsDTO credentialsDTO) {

		Collection<ReservationPaxSegAutoCheckin> automaticCheckins = new ArrayList<ReservationPaxSegAutoCheckin>();
		for (Collection<PaxAutomaticCheckinTO> colPaxAutoCheckinTOs : automaticCheckinTOs.values()) {
			for (PaxAutomaticCheckinTO automaticCheckinTO : colPaxAutoCheckinTOs) {
				ReservationPaxSegAutoCheckin autoCheckin = new ReservationPaxSegAutoCheckin();
				ReservationPaxSegAutoCheckinSeat passengerAutoCheckinSeat = null;
				autoCheckin.setAmount(automaticCheckinTO.getAmount());
				autoCheckin.setAutoCheckinTemplateId(automaticCheckinTO.getAutoCheckinTemplateId());
				autoCheckin.setCustomerId(credentialsDTO.getCustomerId());
				autoCheckin.setDcsCheckinStatus(DCS_STATUS);
				autoCheckin.setDcsResponseText(DCS_RES);
				autoCheckin.setEmail(automaticCheckinTO.getEmail());
				autoCheckin.setFlightAmSeatId(automaticCheckinTO.getFlightAmSeatId());
				autoCheckin.setNoOfAttempts(0); // Will be updated by scheduler
				autoCheckin.setPnrPaxId(automaticCheckinTO.getPnrPaxId());
				autoCheckin.setPnrSegId(automaticCheckinTO.getPnrSegId());
				autoCheckin.setPpfId(automaticCheckinTO.getPpfId());
				autoCheckin.setRequestTimeStamp(CalendarUtil.getCurrentSystemTimeInZulu());
				autoCheckin.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
				if (automaticCheckinTO.getSeatTypePreference() != null
						&& !automaticCheckinTO.getSeatTypePreference().equalsIgnoreCase(
								AirinventoryCustomConstants.AutoCheckinConstants.SIT_TOGETHER)) {
					passengerAutoCheckinSeat = new ReservationPaxSegAutoCheckinSeat();
					passengerAutoCheckinSeat.setSeatTypePreference(automaticCheckinTO.getSeatTypePreference());
					autoCheckin.setReservationPaxSegAutoCheckinSeat(passengerAutoCheckinSeat);
					passengerAutoCheckinSeat.setReservationPaxSegAutoCheckin(autoCheckin);
				}
				autoCheckin.setStatus(AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE);
				autoCheckin.setUserId(credentialsDTO.getUserId());
				automaticCheckins.add(autoCheckin);
			}
		}
		if (!automaticCheckins.isEmpty()) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO.saveOrUpdate(automaticCheckins);
			ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO.saveOrUpdate(setSitTogetherpnrPaxId(automaticCheckins));
		}
		return automaticCheckins;
	}
	
	private static Collection<ReservationPaxSegAutoCheckin> setSitTogetherpnrPaxId(
			Collection<ReservationPaxSegAutoCheckin> passengerAutoCheckins) {
		Integer parentSitTogetherPnrPadid = 0;
		Optional<ReservationPaxSegAutoCheckin> parentPnrPax = passengerAutoCheckins
				.stream()
				.filter(passengerAutoCheckin -> (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat() != null)
						&& (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_WINDOW)
								|| passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
										.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_AISLE) || passengerAutoCheckin
								.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_MIDDLE))
						|| (passengerAutoCheckin.getFlightAmSeatId() != null)).findFirst();
		parentSitTogetherPnrPadid = parentPnrPax.isPresent() ? parentPnrPax.get().getPnrPaxId() : 0;
		for (ReservationPaxSegAutoCheckin passengerAutoCheckin : passengerAutoCheckins) {
			if (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat() == null
					&& passengerAutoCheckin.getFlightAmSeatId() == null && parentSitTogetherPnrPadid != 0
					&& passengerAutoCheckin.getAmount().compareTo(BigDecimal.ZERO) > 0) {
				passengerAutoCheckin.setSitTogetherpnrPaxId(parentSitTogetherPnrPadid);
			}
		}
		return passengerAutoCheckins;
	}

	/**
	 * This is used to add credit charge
	 * 
	 * @param adjustCreditBO
	 * @param reservation
	 * @param paxAutoCheckin
	 * @param userNotes
	 * @param trackInfo
	 * @param credentialsDTO
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	public static void addPassengerAutoCknChargeAndAdjustCredit(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Map<String, Collection<PaxAutomaticCheckinTO>> paxAutoCheckin, String userNotes, TrackInfoDTO trackInfo,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// Adding charge entry
		Map<String, ReservationPaxOndCharge> paxIdChargeMap = addAutoCheckinCharge(reservation, paxAutoCheckin, credentialsDTO,
				chgTnxGen);
		for (String uniqueId : paxIdChargeMap.keySet()) {
			Integer pnrPaxId = Integer.parseInt(AutoCheckinUtil.getPnrPaxId(uniqueId));
			adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, paxIdChargeMap.get(uniqueId), credentialsDTO,
					reservation.isEnableTransactionGranularity());
		}
	}

	/**
	 * This is to calculate pax wise auto checkin charge
	 * 
	 * @param reservation
	 * @param paxAutoCheckin
	 * @param credentialsDTO
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<String, ReservationPaxOndCharge> addAutoCheckinCharge(Reservation reservation,
			Map<String, Collection<PaxAutomaticCheckinTO>> paxAutoCheckin, CredentialsDTO credentialsDTO,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		Map<String, ReservationPaxOndCharge> pnrPaxIdCharge = new HashMap<String, ReservationPaxOndCharge>();
		Collection<ReservationPaxFareSegment> paxFareSegments;
		Iterator<ReservationPaxFareSegment> paxFareSegmentsIterater;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				paxFareSegments = reservationPaxFare.getPaxFareSegments();
				paxFareSegmentsIterater = paxFareSegments.iterator();
				Collection<Integer> consumedPaxOndChgIds = new ArrayList<Integer>();
				while (paxFareSegmentsIterater.hasNext()) {
					ReservationPaxFareSegment resPaxFareSegment = (ReservationPaxFareSegment) paxFareSegmentsIterater.next();

					String uniqueId = AutoCheckinUtil.makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
							reservationPaxFare.getPnrPaxFareId(), resPaxFareSegment.getPnrSegId());
					Collection<PaxAutomaticCheckinTO> paxAutoCknTos = paxAutoCheckin.get(uniqueId);
					int i = 0;
					if (paxAutoCknTos != null) {
						for (PaxAutomaticCheckinTO paxAutoCkn : paxAutoCknTos) {
							ExternalChgDTO chgDTO = null;
							if (paxAutoCkn != null) {
								chgDTO = paxAutoCkn.getChgDTO();
								paxAutoCkn.setPnrPaxId(reservationPax.getPnrPaxId());
							}

							if (chgDTO != null) {
								chgDTO.setAmount(chgDTO.getAmount());
								PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getAppliedDiscountInfoForRefund(
										reservationPaxFare, chgDTO, consumedPaxOndChgIds);

								ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
										.captureReservationPaxOndCharge(chgDTO.getAmount(), null, chgDTO.getChgRateId(),
												chgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, paxDiscInfo,
												null, null, chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

								pnrPaxIdCharge.put(uniqueId + "|" + i, reservationPaxOndCharge);
								++i;
							}
						}
					}
				}
			}
		}

		if (pnrPaxIdCharge.size() == 0) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		return pnrPaxIdCharge;
	}
	
	public static String
			recordPaxWiseAuditHistoryForModifyAutoCheckin(Collection<ReservationPaxSegAutoCheckin> paxSegAutoCheckin) {
		StringBuilder autoCheckinStr = new StringBuilder();
		for (ReservationPaxSegAutoCheckin pax : paxSegAutoCheckin) {
			String seatPreference = pax.getReservationPaxSegAutoCheckinSeat() != null ? pax.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference() : AirinventoryCustomConstants.AutoCheckinConstants.SIT_TOGETHER;
			String autoCheckinCodeStr = pax.getPnrPaxId().toString() + pax.getPnrSegId().toString() +  seatPreference;
			if (autoCheckinStr.toString().isEmpty()) {
				autoCheckinStr.append(autoCheckinCodeStr);
			} else {
				autoCheckinStr.append("," + autoCheckinCodeStr);
			}
		}

		return autoCheckinStr.toString();
	}

	public static void cancelPaxAutoCheckins(Map<String, Collection<PaxAutomaticCheckinTO>> paxAutoCheckinToRemove,
			Reservation reservation) throws ModuleException {
		List<Integer> pnrPaxSegAutoCheckinIdList = new ArrayList<Integer>();
		for (Collection<PaxAutomaticCheckinTO> colPaxAutoCheckinTOs : paxAutoCheckinToRemove.values()) {
			for (PaxAutomaticCheckinTO paxAutoCheckinTo : colPaxAutoCheckinTOs) {
				if (reservation.getAutoCheckins() != null && !reservation.getAutoCheckins().isEmpty()) {
					for (PaxAutomaticCheckinTO autoCheckin : reservation.getAutoCheckins()) {
						if (autoCheckin.getPnrPaxId().equals(paxAutoCheckinTo.getPnrPaxId())
								&& autoCheckin.getPnrSegId().equals(paxAutoCheckinTo.getPnrSegId())) {
							pnrPaxSegAutoCheckinIdList.add(autoCheckin.getPnrPaxSegAutoCheckinId());
						}
					}
				}
			}
		}
		if (pnrPaxSegAutoCheckinIdList.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO.cancelAutomaticCheckin(pnrPaxSegAutoCheckinIdList);
		}
	}
}
