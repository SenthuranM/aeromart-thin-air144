/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.pnl;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.PnlAdlDTO;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.adl.ADLBL;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 * 
 */
public class ResendPnlAdlBL {

	private static Log log = LogFactory.getLog(ResendPnlAdlBL.class);

	private static final int DEFAULT_SEARCH_RANGE_IN_MINUTES = 60;

	public void checkAndResend() throws ModuleException {
		log.debug("############################ Checking Failed PNL or ADL");
		String searchRange = ReservationModuleUtils.getAirReservationConfig().getSearchRangeInMins();
		int rangeInMins = DEFAULT_SEARCH_RANGE_IN_MINUTES;

		if (searchRange != null) {
			rangeInMins = Integer.valueOf(searchRange).intValue();
		}

		log.info("############################ Going to Search for failed PNLS/ADLS withing range (mins) :" + rangeInMins);

		// List of failed Pnls/sita's for the last 60 mins.
		HashMap<Integer,PnlAdlDTO> mapPnlAdlHistoryDTO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getFailuresWithin(rangeInMins, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);

		if (mapPnlAdlHistoryDTO == null || mapPnlAdlHistoryDTO.keySet().size() == 0) {
			log.info("############################  Failed PNLS/ADLS Found None ");
			return;
		}

		Iterator<Integer> listIterator = mapPnlAdlHistoryDTO.keySet().iterator();
		log.info("############################ Number of Failed PNLs/ADLs : " + mapPnlAdlHistoryDTO.keySet().size());

		// iterate each pnl/adl and send a new one to that failed sita.
		while (listIterator.hasNext()) {
			Integer hisID = (Integer) listIterator.next();
			PnlAdlDTO pnlAdlDTO = (PnlAdlDTO) mapPnlAdlHistoryDTO.get(hisID);
			sendMail(pnlAdlDTO);
		}
	}

	/**
	 * Send Email
	 * 
	 * @param pnlAdlDTO
	 * @throws ModuleException
	 */
	private void sendMail(PnlAdlDTO pnlAdlDTO) throws ModuleException {
		String flightNumber = pnlAdlDTO.getFlightNum();
		String depAirport = pnlAdlDTO.getAirportCode();
		Date dateOfFlightZulu = pnlAdlDTO.getFlightDepartureDateZulu();
		Date dateOfFlightLocal = pnlAdlDTO.getFlightDepartureDateLocal();
		String[] sitaAddress = { pnlAdlDTO.getSitaAddress() };
		String msgType = pnlAdlDTO.getMessageType();

		if (flightNumber == null || depAirport == null || dateOfFlightZulu == null || sitaAddress.length == 0 || msgType == null) {
			log.error("################ CAN NOT RESEND FOR FAILED PNL; NULL PARAM FOUND");
			return;
		}

		log.info("##### GOING TO REGENERATE AND RESEND  " + msgType + " FOR : --");
		log.info("##### FLIGHT NUMBER : " + flightNumber);
		log.info("##### DEP AIRPORT : " + depAirport);
		log.info("##### DATE OF FLIGHT Zulu :" + dateOfFlightZulu);
		log.info("##### DATE OF FLIGHT Local :" + dateOfFlightLocal);
		log.info("##### SITA ADDRESS(s) :" + sitaAddress[0]);

		// To Checks before resending the PNL for the failed SITA
		log.info("##### GOING TO CHECK IF NEED TO SEND ?");
		if (hasExceededMaxAttempt(pnlAdlDTO) || exceedsFlightDepartureTime(dateOfFlightZulu) || hasSucuessfullAttempt(pnlAdlDTO)) {

			log.info("##### WILL NOT RESEND PNL/ADL");
			return;
		}
		try {

			if (PNLConstants.MessageTypes.PNL.equals(pnlAdlDTO.getMessageType())) {
				new PNLBL().sendPNLAuxiliary(flightNumber, depAirport, dateOfFlightLocal, sitaAddress,
						PNLConstants.SendingMethod.RESENDER);

			} else {
				// new ADLBL().sendADLAuxiliary(flightNumber, depAirport, dateOfFlightLocal, sitaAddress,
				// PNLConstants.SendingMethod.RESENDER);
				new ADLBL().sendADL(flightNumber, depAirport, dateOfFlightLocal, sitaAddress,
						PNLConstants.SendingMethod.RESENDER);
			}
		} catch (ModuleException e) {
			log.error("########## Did not resend : " + e.getExceptionCode());
		}

	}

	/**
	 * Gets a count of records in PNLADLTXHISTORY, where sita=? flightid ? and Trnmission status = N
	 * 
	 * @param pnlAdlDTO
	 * @return
	 */
	private boolean hasExceededMaxAttempt(PnlAdlDTO pnlAdlDTO) {
		String retryLimit = ReservationModuleUtils.getAirReservationConfig().getRetryLimit();
		int maxThreshold;
		if (retryLimit == null || retryLimit.equals("0")) {
			maxThreshold = 2;
		} else {
			maxThreshold = Integer.valueOf(retryLimit).intValue();
		}
		int numberOfAttempts = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getNumberOfFailedAttempts(
				pnlAdlDTO.getFlightId(), pnlAdlDTO.getSitaAddress(), pnlAdlDTO.getMessageType());

		if (numberOfAttempts < maxThreshold) {
			return false;
		}
		log.info("##### HAS EXCEEDED MAX ATTEMPTS [NUM OF ATTEMPTS: " + numberOfAttempts + " THRESHOLD :" + maxThreshold + "]");
		return true;

	}

	private boolean exceedsFlightDepartureTime(Date flightDateTime) {
		GregorianCalendar calFlightDate = new GregorianCalendar();
		calFlightDate.setTime(flightDateTime);

		GregorianCalendar calNow = new GregorianCalendar();
		calNow.setTime(new Date());

		if (calNow.after(calFlightDate)) {
			log.info("##### FLIGHT HAS ALREADY DEPARTED FLIGHT TIME ZULU:" + flightDateTime);
			return true;
		}
		return false;
	}

	private boolean hasSucuessfullAttempt(PnlAdlDTO pnlAdlDTO) {
		int numberOfSuccssAttempts = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getNumberOfSuccessAtempts(
				pnlAdlDTO.getFlightId(), pnlAdlDTO.getSitaAddress(), pnlAdlDTO.getMessageType(),
				pnlAdlDTO.getTransmissionTimeStamp());

		// manualy can send adls many time
		if (numberOfSuccssAttempts >= 1) {
			log.info("##### WILL NOT PROCEED! A SUCCESSFUL PNL/ADL WAS FOUND:[DEP AIRPORT:" + pnlAdlDTO.getAirportCode()
					+ " FLIGHT NUMBER :" + pnlAdlDTO.getFlightNum() + " SITA ADD :" + pnlAdlDTO.getSitaAddress() + "]");
			return true;
		}

		return false;
	}
}
