/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.passenger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.service.RevenueAccountBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for composing modification object for adjustments
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cMForAdjustments"
 */
public class CMForAdjustments extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CMForAdjustments.class);

	/**
	 * Execute method of the CMForAdjustments command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal amount = (BigDecimal) this.getParameter(CommandParamNames.MODIFY_AMOUNT);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		// Checking params
		this.validateParams(pnr, pnrPaxId, amount, userNotes, credentialsDTO);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		// Record Modifications
		ReservationAudit reservationAudit = this.composeAudit(pnr, pnrPaxId, amount, userNotes, credentialsDTO, userNoteType);

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Compose audit
	 * 
	 * @param pnr
	 * @param pnrPaxId
	 * @param amount
	 * @param userNotes
	 * @param credentialsDTO
	 * @param userNoteType TODO
	 * @return
	 * @throws ModuleException
	 */
	private ReservationAudit composeAudit(String pnr, String pnrPaxId, BigDecimal amount, String userNotes,
			CredentialsDTO credentialsDTO, String userNoteType) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CREDIT_ADJUSTED.getCode());
		reservationAudit.setUserNote(userNotes);
		reservationAudit.setUserNoteType(userNoteType);

		RevenueAccountBD revenueAccountBD = ReservationModuleUtils.getRevenueAccountBD();
		BigDecimal balance = revenueAccountBD.getPNRPaxBalance(pnrPaxId);

		// Setting the amount
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CreditAdjusted.AMOUNT, amount.toString());
		// Setting the passenger balance
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CreditAdjusted.NEW_PAX_BALANCE, balance == null ? "0"
				: balance.toString());

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CreditAdjusted.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CreditAdjusted.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Validate parameters
	 * 
	 * @param pnr
	 * @param pnrPaxId
	 * @param amount
	 * @param userNotes
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, String pnrPaxId, BigDecimal amount, String userNotes, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || pnrPaxId == null || amount == null || userNotes == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
