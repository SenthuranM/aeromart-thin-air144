/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.TnxAgentPayment;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.revacc.TnxFactory;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for Revenue Accounting per passenger
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="revenuePurchase"
 */
public class RevenuePurchase extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(RevenuePurchase.class);

	/**
	 * Execute method of the RevenueAccounting command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		Map<Integer, PaymentAssembler> pnrPaxIdAndPayments = (Map<Integer, PaymentAssembler>) this
				.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		ReservationPaymentMetaTO reservationPaymentMetaTO = (ReservationPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAYMENT_META_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Collection<CardDetailConfigDTO> cardConfigData = (Collection<CardDetailConfigDTO>) this
				.getParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA);
		boolean enableTransactionGranularity = (Boolean) this.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);
		Boolean isRequote = getParameter(CommandParamNames.IS_REQUOTE, Boolean.FALSE, Boolean.class);
		boolean doRevPurchase = getParameter(CommandParamNames.DO_REV_PURCHASE, Boolean.FALSE, Boolean.class);
		boolean isGoShowProcess = this.getParameter(CommandParamNames.IS_GOSHOW_PROCESS, Boolean.FALSE, Boolean.class);
		boolean isActualPayment = this.getParameter(CommandParamNames.IS_ACTUAL_PAYMENT, Boolean.FALSE, Boolean.class);
		boolean isFirstPayment = this.getParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.FALSE, Boolean.class);
		PaxAdjAssembler paxAdjAssmbler = this.getParameter(CommandParamNames.PAX_ADJ_ASSEMBLER, PaxAdjAssembler.class);

		Map<Integer, Map<String, BigDecimal>> lmsPaxProductRedeemedAmount = (Map<Integer, Map<String, BigDecimal>>) this
				.getParameter(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS);
		List<TransactionSegment> transactionSegments = (List<TransactionSegment>) this
				.getParameter(CommandParamNames.TRNX_SEGMENTS);
		if (isRequote && !doRevPurchase) {
			return new DefaultServiceResponse(true);
		}
		// Checking params
		this.validateParams(pnrPaxIdAndPayments, credentialsDTO);

		Iterator<Integer> itPaxIds = pnrPaxIdAndPayments.keySet().iterator();
		PaymentInfo paymentInfo;
		Iterator<PaymentInfo> itPaymentInfo;
		ITnxPayment iTnxPayment;
		Integer pnrPaxId;
		Collection<ITnxPayment> passengerPayments;
		PaymentAssembler paymentAssembler;
		String cashReceiptNumber = null;
		String onAccountRecieptNumber = null;
		String creditCardReceiptNumber = null;
		String recieptNumber = null;
		BigDecimal totalPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Integer transactionSeq;
		Map<String, BigDecimal> lmsProductRedeemedAmount = null;

		while (itPaxIds.hasNext()) {
			pnrPaxId = itPaxIds.next();

			paymentAssembler = pnrPaxIdAndPayments.get(pnrPaxId);
			itPaymentInfo = paymentAssembler.getPayments().iterator();

			boolean incrementTnxSeq = isRequote;

			if (paxAdjAssmbler != null) {
				incrementTnxSeq = paxAdjAssmbler.newTnxSequenceNeededAtPayment(pnrPaxId);
			}

			transactionSeq = retrieveCorrectTransactionSeq(pnrPaxId, incrementTnxSeq);

			if (lmsPaxProductRedeemedAmount != null && lmsPaxProductRedeemedAmount.size() > 0) {
				lmsProductRedeemedAmount = lmsPaxProductRedeemedAmount.get(pnrPaxId);
			}

			if (reservationPaymentMetaTO != null) {
				reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().get(pnrPaxId)
						.setPerPaxBaseTotalPayAmount(paymentAssembler.getTotalPayAmount());
			}

			passengerPayments = new ArrayList<ITnxPayment>();

			while (itPaymentInfo.hasNext()) {
				paymentInfo = itPaymentInfo.next();
				totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, paymentInfo.getTotalAmount());
				// For Pax Credit Payment
				if (paymentInfo instanceof PaxCreditInfo) {
					PaxCreditDTO paxCreditDTO = ((PaxCreditInfo) paymentInfo).getPaxCredit();
					TnxCreditPayment payment = TnxFactory.getTnxCreditPayment(paxCreditDTO, credentialsDTO.getAgentCode());
					passengerPayments.add(payment);
				}

				// For Card Payment
				else if (paymentInfo instanceof CardPaymentInfo) {
					if (creditCardReceiptNumber != null) {
						recieptNumber = creditCardReceiptNumber;
					} else {
						// If applicable will populate reciept number
						creditCardReceiptNumber = TicketingUtils
								.getRecieptNoForPayment(ReservationInternalConstants.PaymentMode.CREDITCARD);
						recieptNumber = creditCardReceiptNumber;
					}

					CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getCardPaymentInstance(paymentInfo.getTotalAmount(),
							cardPaymentInfo.getType(), cardPaymentInfo.getNo(), cardPaymentInfo.getName(),
							cardPaymentInfo.getAddress(), cardPaymentInfo.getEDate(), cardPaymentInfo.getSecurityCode(),
							cardPaymentInfo.getPnr(), cardPaymentInfo.getPaymentBrokerRefNo(), paymentInfo.getPayCurrencyDTO(),
							cardPaymentInfo.getPaymentReferanceTO(), cardPaymentInfo.getPayCarrier(),
							cardPaymentInfo.getLccUniqueId(), cardPaymentInfo.getAuthorizationId(), null);

					passengerPayments.add(iTnxPayment);
				}

				// For Agent Credit
				else if (paymentInfo instanceof AgentCreditInfo) {
					if (onAccountRecieptNumber != null) {
						recieptNumber = onAccountRecieptNumber;
					} else {
						// If applicable will populate reciept number
						onAccountRecieptNumber = TicketingUtils
								.getRecieptNoForPayment(ReservationInternalConstants.PaymentMode.ONACCOUNT);
						recieptNumber = onAccountRecieptNumber;
					}

					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getOnAccountPaymentInstance(paymentInfo.getTotalAmount(),
							agentCreditInfo.getAgentCode(), paymentInfo.getPayCurrencyDTO(),
							agentCreditInfo.getPaymentReferenceTO(), agentCreditInfo.getPayCarrier(),
							agentCreditInfo.getLccUniqueId(), null);

					if (iTnxPayment instanceof TnxAgentPayment) {
						TnxAgentPayment agentPayment = (TnxAgentPayment) iTnxPayment;
						if (credentialsDTO.getSalesChannelCode() != null && credentialsDTO.getDirectBillId() != null
								&& !credentialsDTO.getDirectBillId().isEmpty()
								&& credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_GOQUO)
								&& agentPayment.getPaymentReferenceTO() != null) {
							agentPayment.getPaymentReferenceTO().setPaymentRef(credentialsDTO.getDirectBillId());
						}

					}
					passengerPayments.add(iTnxPayment);
				}

				// For Cash Payment
				else if (paymentInfo instanceof CashPaymentInfo) {
					if (cashReceiptNumber != null) {
						recieptNumber = cashReceiptNumber;
					} else {
						// If applicable will populate reciept number
						cashReceiptNumber = TicketingUtils.getRecieptNoForPayment(ReservationInternalConstants.PaymentMode.CASH);
						recieptNumber = cashReceiptNumber;
					}

					CashPaymentInfo cashPaymentInfo = (CashPaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getCashPaymentInstance(paymentInfo.getTotalAmount(),
							paymentInfo.getPayCurrencyDTO(), cashPaymentInfo.getPaymentReferanceTO(),
							cashPaymentInfo.getPayCarrier(), cashPaymentInfo.getLccUniqueId(), null);
					passengerPayments.add(iTnxPayment);
				}

				// For Loyalty Payment
				else if (paymentInfo instanceof LMSPaymentInfo) {
					LMSPaymentInfo lmsPaymentInfo = (LMSPaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getLMSPaymentInstance(lmsPaymentInfo.getLoyaltyMemberAccountId(),
							lmsPaymentInfo.getRewardIDs(), paymentInfo.getTotalAmount(), paymentInfo.getPayCurrencyDTO(),
							lmsPaymentInfo.getPayCarrier(), lmsPaymentInfo.getLccUniqueId(), lmsPaymentInfo.getPaymentTnxId());
					passengerPayments.add(iTnxPayment);
				}

				// For voucher Payment
				else if (paymentInfo instanceof VoucherPaymentInfo) {
					VoucherPaymentInfo voucherPaymentInfo = (VoucherPaymentInfo) paymentInfo;
					iTnxPayment = TnxPaymentFactory.getVoucherPaymentInstance(voucherPaymentInfo.getVoucherDTO(),
							paymentInfo.getTotalAmount(), paymentInfo.getPayCurrencyDTO(), voucherPaymentInfo.getPayCarrier(),
							voucherPaymentInfo.getLccUniqueId(), voucherPaymentInfo.getPaymentTnxId());
					passengerPayments.add(iTnxPayment);
				}

			}

			// Record ticket purchase (with credit)
			// If you see any nullpointer in here. Please do not surround it
			// permanently. Please inform Nili

			ReservationModuleUtils.getRevenueAccountBD().recordPurchase(pnrPaxId.toString(),
					paymentAssembler.getTotalChargeAmount(), totalPaymentAmount, passengerPayments,
					reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().get(pnrPaxId), credentialsDTO, recieptNumber,
					cardConfigData, enableTransactionGranularity, isGoShowProcess, isActualPayment, isFirstPayment,
					transactionSeq, lmsProductRedeemedAmount, transactionSegments);
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		if (this.getParameter(CommandParamNames.SPLITTED_PAX_IDS) != null) {
			response.addResponceParam(CommandParamNames.SPLITTED_PAX_IDS, this.getParameter(CommandParamNames.SPLITTED_PAX_IDS));
		}

		log.debug("Exit execute");
		return response;
	}

	private BigDecimal getTotalAmount(PaymentAssembler paymentAssembler) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paymentAssembler != null && paymentAssembler.getPayments() != null) {
			for (PaymentInfo payInfo : paymentAssembler.getPayments()) {
				total = AccelAeroCalculator.add(total, payInfo.getTotalAmount());
			}
		}
		return total;
	}

	private Integer retrieveCorrectTransactionSeq(Integer pnrPaxId, Boolean incrementTransactionSequence) {
		Integer maxTxnSeq = 0;
		try {
			maxTxnSeq = ReservationModuleUtils.getRevenueAccountBD().getPNRPaxMaxTransactionSeq(pnrPaxId);
			if (incrementTransactionSequence) {
				maxTxnSeq = maxTxnSeq + 1;
			}
		} catch (ModuleException e) {
			log.error("Unable to retrieve transaction Sequence");
			maxTxnSeq = 1;
		}
		return maxTxnSeq;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnrPaxIdAndPayments
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Map pnrPaxIdAndPayments, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnrPaxIdAndPayments == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

}
