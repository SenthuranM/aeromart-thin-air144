package com.isa.thinair.airreservation.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.ssr.MedicalSsrDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * Common for Flight Services, Airport Services and Airport Transfers
 * 
 */
public class SSRCommonUtil {

	public static Collection<Integer> getPaxSegmentSSRIds(Map<Integer, Collection<ReservationPaxSegmentSSR>> cancelledSSRMap) {
		Collection<Integer> paxSegmentSSRIds = new ArrayList<Integer>();
		Collection<Collection<ReservationPaxSegmentSSR>> cancelledSSRCols = cancelledSSRMap.values();

		for (Iterator<Collection<ReservationPaxSegmentSSR>> itCancelledSSRCols = cancelledSSRCols.iterator(); itCancelledSSRCols
				.hasNext();) {
			Collection<ReservationPaxSegmentSSR> cancelledSSRs = (Collection<ReservationPaxSegmentSSR>) itCancelledSSRCols.next();

			for (Iterator<ReservationPaxSegmentSSR> itCancelledSSRs = cancelledSSRs.iterator(); itCancelledSSRs.hasNext();) {
				ReservationPaxSegmentSSR segmentSSR = (ReservationPaxSegmentSSR) itCancelledSSRs.next();

				paxSegmentSSRIds.add(segmentSSR.getPnrPaxSegmentSSRId());
			}
		}

		return paxSegmentSSRIds;
	}

	public static void modifySSRs(String pnr, Integer version, Map<Integer, SegmentSSRAssembler> paxSSRMap, IPassenger iPassenger,
			String userNotes, EXTERNAL_CHARGES extChargeType, CredentialsDTO credentialsDTO, boolean cancelOnly)
			throws ModuleException {

		Collection<ReservationPaxSegmentSSR> addedPaxSegSSRs = new ArrayList<ReservationPaxSegmentSSR>();

		Reservation reservation = updateReservationSSRs(pnr, version, paxSSRMap, iPassenger, userNotes, extChargeType,
				credentialsDTO, cancelOnly, addedPaxSegSSRs);

		AdjustCreditBO adjustCreditBO = updateSSRCharges(reservation, paxSSRMap, iPassenger, userNotes, credentialsDTO,
				extChargeType, cancelOnly);

		if (extChargeType == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
			SSRBL.modifySSRs(reservation, adjustCreditBO, pnr, version, paxSSRMap, iPassenger, userNotes, extChargeType,
					credentialsDTO, cancelOnly);
		} else if (extChargeType == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
			SSRBL.modifySSRs(reservation, adjustCreditBO, pnr, version, paxSSRMap, iPassenger, userNotes, extChargeType,
					credentialsDTO, cancelOnly);
		}

		// PassengerAssembler passengerAssembler = (PassengerAssembler)iPassenger;
		// MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(passengerAssembler.getPassengerPaymentsMap(), reservation);
		// medicalSsrDTO.sendEmail();

	}

	private static Reservation updateReservationSSRs(String pnr, Integer version,
			Map<Integer, SegmentSSRAssembler> ssrAssemblerMap, IPassenger iPassenger, String userNotes,
			EXTERNAL_CHARGES extChargeType, CredentialsDTO credentialsDTO, boolean cancelOnly,
			Collection<ReservationPaxSegmentSSR> addedPaxSegSSrs) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);

		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
		ReservationSSRUtil.updatePaxSegmentSSRs(reservation, ssrAssemblerMap, true, addedPaxSegSSrs);
		ReservationSSRUtil.addPaxSegmentSSRs(reservation, ssrAssemblerMap, extChargeType);

		return reservation;

	}

	@SuppressWarnings({ "unused", "unchecked" })
	public static AdjustCreditBO updateSSRCharges(Reservation reservation, Map<Integer, SegmentSSRAssembler> paxSSRAssemblerMap,
			IPassenger iPassenger, String userNotes, CredentialsDTO credentialsDTO, EXTERNAL_CHARGES extCharge,
			boolean cancelOnly) throws ModuleException {

		AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation, true);
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
		SSRExternalChargeDTO externalCharge = ReservationSSRUtil.getSSRExternalCharge(credentialsDTO.getTrackInfoDTO(),
				extCharge);
		Set<ReservationPax> passengers = reservation.getPassengers();

		for (Iterator<ReservationPax> itPaxs = passengers.iterator(); itPaxs.hasNext();) {
			ReservationPax pax = (ReservationPax) itPaxs.next();
			if (!ReservationApiUtils.isInfantType(pax)) {
				Set<ReservationPaxFare> pnrPaxFares = pax.getPnrPaxFares();

				SegmentSSRAssembler paxSSRAsm = paxSSRAssemblerMap.get(pax.getPaxSequence());
				Map<Integer, Integer> canceledSegmentSSRIdMap = (paxSSRAsm == null)
						? new HashMap<Integer, Integer>()
						: paxSSRAsm.getCanceledSegmentSSRIdMap();
				Map<Integer, Collection<PaxSegmentSSRDTO>> newSSRDTOMap = (paxSSRAsm == null)
						? new HashMap<Integer, Collection<PaxSegmentSSRDTO>>()
						: paxSSRAsm.getNewSegmentSSRDTOMap();
				PassengerAssembler passengerAsm = (paxSSRAsm == null)
						? null
						: (PassengerAssembler) paxSSRAsm.getPassengerAssembler();
				Map<Integer, IPayment> paxPaymentMap = null;
				PaymentAssembler paymentAsm = null;
				Collection<ExternalChgDTO> ssrExtCharges = null;
				Collection<Integer> consumedPaxOndChgIds = new ArrayList<Integer>();
				if (!cancelOnly) {
					if (passengerAsm == null) {
						passengerAsm = (PassengerAssembler) iPassenger;
					}

					paxPaymentMap = passengerAsm.getPassengerPaymentsMap();
					paymentAsm = (paxPaymentMap != null) ? (PaymentAssembler) paxPaymentMap.get(pax.getPnrPaxId()) : null;
					ssrExtCharges = (paymentAsm == null) ? null : paymentAsm.getPerPaxExternalCharges(extCharge);

				}

				for (Iterator<ReservationPaxFare> itFares = pnrPaxFares.iterator(); itFares.hasNext();) {
					ReservationPaxFare paxFare = (ReservationPaxFare) itFares.next();
					Set<ReservationPaxFareSegment> fareSegments = paxFare.getPaxFareSegments();
					Set<ReservationPaxOndCharge> fareOndCharge = paxFare.getCharges();

					for (Iterator<ReservationPaxFareSegment> itSegments = fareSegments.iterator(); itSegments.hasNext();) {
						ReservationPaxFareSegment fareSegment = (ReservationPaxFareSegment) itSegments.next();

						// --- add ssr charges
						if (ssrExtCharges != null) {
							for (Iterator<ExternalChgDTO> itSSRExCharges = ssrExtCharges.iterator(); itSSRExCharges.hasNext();) {
								SSRExternalChargeDTO newSSRExCharge = (SSRExternalChargeDTO) itSSRExCharges.next();
								if (FlightRefNumberUtil.getSegmentIdFromFlightRPH(newSSRExCharge.getFlightRPH())
										.intValue() == fareSegment.getSegment().getFlightSegId().intValue()
										&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL
												.equals(fareSegment.getSegment().getStatus())) {
									ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
											.captureReservationPaxOndCharge(newSSRExCharge.getAmount(), null,
													externalCharge.getChgRateId(), externalCharge.getChgGrpCode(), paxFare,
													credentialsDTO, false, null, null, null,
													chgTnxGen.getTnxSequence(pax.getPnrPaxId()));
									adjustCreditBO.addAdjustment(reservation.getPnr(), pax.getPnrPaxId(), userNotes,
											reservationPaxOndCharge, credentialsDTO,
											reservation.isEnableTransactionGranularity());
								}
							}
						}

						// --- cancel ssr charges
						Set<ReservationPaxSegmentSSR> exSSRs = fareSegment.getReservationPaxSegmentSSRs();

						for (Iterator<ReservationPaxSegmentSSR> itExSSRs = exSSRs.iterator(); itExSSRs.hasNext();) {
							ReservationPaxSegmentSSR exSSR = (ReservationPaxSegmentSSR) itExSSRs.next();

							Integer paxSegmentSSRId = canceledSegmentSSRIdMap.get(exSSR.getPnrPaxSegmentSSRId());

							if (paxSegmentSSRId != null) {
								if (checkSSRrefundability(externalCharge.getChgRateId(), extCharge, fareOndCharge)) {
									BigDecimal amount = AccelAeroCalculator.multiply(exSSR.getChargeAmount(), -1);
									ExternalChgDTO chgDTO = externalCharge;
									chgDTO.setAmount(amount);
									PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getAppliedDiscountInfoForRefund(paxFare,
											chgDTO, consumedPaxOndChgIds);
									ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
											.captureReservationPaxOndCharge(amount, null, externalCharge.getChgRateId(),
													externalCharge.getChgGrpCode(), paxFare, credentialsDTO, false, paxDiscInfo,
													null, null, chgTnxGen.getTnxSequence(pax.getPnrPaxId()));
									adjustCreditBO.addAdjustment(reservation.getPnr(), pax.getPnrPaxId(), userNotes,
											reservationPaxOndCharge, credentialsDTO,
											reservation.isEnableTransactionGranularity());
								}
							}
						}
					}
				}
			}
		}

		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		return adjustCreditBO;
	}

	private static boolean checkSSRrefundability(Integer chgRateId, EXTERNAL_CHARGES extCharge,
			Set<ReservationPaxOndCharge> fareOndCharge) {

		if (fareOndCharge != null && !fareOndCharge.isEmpty() && EXTERNAL_CHARGES.INFLIGHT_SERVICES.equals(extCharge)) {
			for (ReservationPaxOndCharge reservationPaxOndCharge : fareOndCharge) {
				if (chgRateId.equals(reservationPaxOndCharge.getChargeRateId())) {
					return "Y".equals(reservationPaxOndCharge.getRefundableOperation()) ? true : false;
				}
			}
		}
		return true;
	}

}
