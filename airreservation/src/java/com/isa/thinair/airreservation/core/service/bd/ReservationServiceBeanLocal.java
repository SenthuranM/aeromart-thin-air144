package com.isa.thinair.airreservation.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airreservation.api.service.ReservationAllServiceBD;

@Local
public interface ReservationServiceBeanLocal extends ReservationAllServiceBD {

}
