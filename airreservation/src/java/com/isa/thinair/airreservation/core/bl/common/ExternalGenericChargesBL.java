/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.InsuranceSegExtChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Reservation related external credit card charges business logic implementation
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class ExternalGenericChargesBL {

	/** Hold the logger instance */
	// private static final Log log = LogFactory.getLog(ExternalGenericChargesBL.class);

	/**
	 * Hide the constructor
	 */
	private ExternalGenericChargesBL() {

	}

	/**
	 * Reflect external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 * @param reservationDiscountDTO
	 * @param externalChgDTO
	 * @throws ModuleException
	 */
	public static void reflectExternalChgsForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		// Apply these changes for the reservation
		applyExternalChargesForAFreshBooking(reservation, credentialsDTO, paxDiscInfo, reservationDiscountDTO);
	}

	/**
	 * Reflect external charges for a payment booking
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	public static void reflectExternalChgForAPaymentBooking(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPayment, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		// Apply these changes for the reservation
		// Haider change Map<Integer, BigDecimal> to Map<Integer, ArrayList<BigDecimal>>
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForAPaymentBooking(reservation,
				passengerPayment, credentialsDTO, chgTnxGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, passengerPayment);
	}

	/**
	 * Reflect external charges for a new segment
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param passengerRevenueMap
	 * @param chgTxnGen
	 * @throws ModuleException
	 */
	public static void reflectExternalChgForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Map<Integer, RevenueDTO> passengerRevenueMap,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen) throws ModuleException {
		// Apply these changes for the reservation
		// Haider change the Map<Integer, BigDecimal> to Map<Integer, ArrayList<BigDecimal>>
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForANewSegment(reservation,
				pnrPaxIdAndPayments, credentialsDTO, chgTxnGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, pnrPaxIdAndPayments);

		// Apply these changes to the passenger revenue map
		addExternalChgsForRevenueMap(mapPnrPaxIdAdjustments, passengerRevenueMap);
	}

	/**
	 * Reflect external charges for a new infant
	 * 
	 * @param reservation
	 * @param passengerPaymentMap
	 * @param tnxSeq
	 * @throws ModuleException
	 */
	public static void reflectExternalChargeForInfants(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPaymentMap, CredentialsDTO credentialsDTO, Collection<ReservationPax> infants,
			Integer tnxSeq) throws ModuleException {

		ReservationPax addedInfant = null;
		for (ReservationPax infant : infants) {
			addedInfant = infant;
			break;
		}
		// Apply these changes for the reservation
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForInfants(reservation,
				passengerPaymentMap, credentialsDTO, addedInfant, tnxSeq);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, passengerPaymentMap);
	}

	/**
	 * Apply external charges for infant
	 * 
	 * @param reservation
	 * @param passengerPaymentMap
	 * @param tnxSeq
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForInfants(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPaymentMap, CredentialsDTO credentialsDTO, ReservationPax addedInfant,
			Integer tnxSeq) throws ModuleException {

		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdOndCharges = new HashMap<Integer, List<ReservationPaxOndCharge>>();

		Map<Integer, ReservationSegmentDTO> mapPnrSegIdAndReservationSegDTO = getSegmentDetailInformation(reservation.getPnr());
		Date currentDate = new Date();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		BigDecimal appropriateChargePerPax;
		PaymentAssembler paymentAssembler;
		BigDecimal[] ondAmounts;
		Collection<Integer> colActiveONDPnrPaxFareIds;
		int ondIndex = 0;
		Collection<ExternalChgDTO> externalChgs;
		boolean updateExist = false;
		List<ReservationPaxOndCharge> arrChargePerPax;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (passengerPaymentMap.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) passengerPaymentMap.get(reservationPax.getPnrPaxId());
				externalChgs = paymentAssembler.getPerPaxGenericExternalCharges();
				includeServiceTaxToExtChrgs(paymentAssembler, externalChgs);

				if (externalChgs.size() > 0) {
					arrChargePerPax = new ArrayList<ReservationPaxOndCharge>();

					for (ExternalChgDTO externalChgDTO : externalChgs) {
						if (externalChgDTO.getExternalChargesEnum() == EXTERNAL_CHARGES.SERVICE_TAX) {
							if (externalChgDTO instanceof ServiceTaxExtChgDTO) {
								ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) externalChgDTO;
								Integer flightSegId = FlightRefNumberUtil
										.getSegmentIdFromFlightRPH(serviceTaxExtChgDTO.getFlightRefNumber());
								reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(addedInfant.getPnrPaxFares(),
										flightSegId, false);
								String taxAppliedCategory = null;
								if (serviceTaxExtChgDTO.isTicketingRevenue()) {
									taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY;
								} else {
									taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_NON_TICKETING_CATEGORY;
								}
								QuotedChargeDTO quotedChargeDTO = ReservationModuleUtils.getChargeBD().getCharge(
										serviceTaxExtChgDTO.getChargeCode(), null, getDummyFareSummaryDTO(), null, false);
								PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
										serviceTaxExtChgDTO.getNonTaxableAmount(), serviceTaxExtChgDTO.getTaxableAmount(),
										taxAppliedCategory, serviceTaxExtChgDTO.getTaxDepositCountryCode(),
										serviceTaxExtChgDTO.getTaxDepositStateCode());

								ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
										.captureReservationPaxOndCharge(serviceTaxExtChgDTO.getAmount(), null,
												quotedChargeDTO.getChargeRateId(), quotedChargeDTO.getChargeGroupCode(),
												reservationPaxFare, credentialsDTO, false, null, null, paxOndChargeDTO, tnxSeq);
								arrChargePerPax.add(reservationPaxOndCharge);
								updateExist = true;
							}

						} else {
							appropriateChargePerPax = externalChgDTO.getAmount();

							colActiveONDPnrPaxFareIds = getApplicableONDPnrPaxFareIds(currentDate,
									reservationPax.getPnrPaxFares(), mapPnrSegIdAndReservationSegDTO,
									reservationPax.geteTickets(), false);
							ondAmounts = AccelAeroCalculator.roundAndSplit(appropriateChargePerPax,
									colActiveONDPnrPaxFareIds.size());
							
							Collection<ReservationPaxFare> infantPaxFareIds = getInfantChargeApplicablePaxFares(reservationPax,
									colActiveONDPnrPaxFareIds, addedInfant.getPnrPaxFares());

							ondIndex = 0;
							itReservationPaxFare = infantPaxFareIds.iterator();
							while (itReservationPaxFare.hasNext()) {
								reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

									ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
											.captureReservationPaxOndCharge(ondAmounts[ondIndex], null,
													externalChgDTO.getChgRateId(), externalChgDTO.getChgGrpCode(),
													reservationPaxFare, credentialsDTO, false, null, null, null, tnxSeq);
									
									if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()) {
										reservationPaxOndCharge.setOperationType(CommonsConstants.ModifyOperation.ADD_INFANT.getOperation());
									}
									
									arrChargePerPax.add(reservationPaxOndCharge);
									ondIndex++;
									updateExist = true;

							}
						}
					}
					mapPnrPaxIdOndCharges.put(reservationPax.getPnrPaxId(), arrChargePerPax);
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdOndCharges;
	}

	private static void includeServiceTaxToExtChrgs(PaymentAssembler paymentAssembler,
			Collection<ExternalChgDTO> ccExternalChgs) {
		// alter JN for CC Charge
		ccExternalChgs.addAll(paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.JN_OTHER));
		ccExternalChgs.addAll(paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.SERVICE_TAX));
	}

	private static FareSummaryDTO getDummyFareSummaryDTO() {
		BigDecimal fare = AccelAeroCalculator.getDefaultBigDecimalZero();
		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
		fareSummaryDTO.setAdultFare(fare.doubleValue());
		fareSummaryDTO.setChildFare(fare.doubleValue());
		fareSummaryDTO.setInfantFare(fare.doubleValue());
		fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		return fareSummaryDTO;
	}

	/**
	 * Apply external charges for a new segment
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param chgTxnGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen)
			throws ModuleException {
		// Haider
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		BigDecimal appropriateChargePerPax;
		BigDecimal[] ondAmounts;
		PaymentAssembler paymentAssembler;
		int ondIndex = 0;
		int ondSize = 0;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());
				ccExternalChgs = paymentAssembler.getPerPaxGenericExternalCharges();

				// Haider collect all charges per pax
				List<ReservationPaxOndCharge> arrChargePerPax = new ArrayList<ReservationPaxOndCharge>();

				if (ccExternalChgs.size() > 0) {
					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {

						// Allowing saving the insurance segment charges per segment
						if (externalChgDTO.getExternalChargesEnum() == EXTERNAL_CHARGES.INSURANCE
								&& externalChgDTO instanceof InsuranceSegExtChgDTO) {
							InsuranceSegExtChgDTO extChg = (InsuranceSegExtChgDTO) externalChgDTO;
							itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
							while (itReservationPaxFare.hasNext()) {
								reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();
								if (isNewOND(reservationPaxFare)) {
									for (ReservationPaxFareSegment fareSeg : reservationPaxFare.getPaxFareSegments()) {
										if (extChg.getFlightSegId().intValue() == fareSeg.getSegment().getFlightSegId()
												.intValue()) {
											ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
													.captureReservationPaxOndCharge(extChg.getAmount(), null,
															externalChgDTO.getChgRateId(), externalChgDTO.getChgGrpCode(),
															reservationPaxFare, credentialsDTO, false, null, null, null,
															chgTxnGen.getTnxSequence(reservationPax.getPnrPaxId()));
											arrChargePerPax.add(reservationPaxOndCharge);
											updateExist = true;
										}
									}
								}
							}
						} else {

							appropriateChargePerPax = externalChgDTO.getAmount();

							ondSize = getNewOndSize(reservationPax.getPnrPaxFares());
							ondAmounts = AccelAeroCalculator.roundAndSplit(appropriateChargePerPax, ondSize);

							ondIndex = 0;
							itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
							while (itReservationPaxFare.hasNext()) {
								reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

								if (isNewOND(reservationPaxFare)) {
									ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
											.captureReservationPaxOndCharge(ondAmounts[ondIndex], null,
													externalChgDTO.getChgRateId(), externalChgDTO.getChgGrpCode(),
													reservationPaxFare, credentialsDTO, false, null, null, null,
													chgTxnGen.getTnxSequence(reservationPax.getPnrPaxId()));
									
									if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()
											&& externalChgDTO.getOperationMode() != null) {
										reservationPaxOndCharge.setOperationType(externalChgDTO.getOperationMode());
									}									
									arrChargePerPax.add(reservationPaxOndCharge);
									ondIndex++;
									updateExist = true;
								}
							}
						}
					}

					mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), arrChargePerPax);
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	/**
	 * Applies the external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 * @param reservationDiscountDTO
	 * @param colExternalChgDTO
	 * @throws ModuleException
	 */
	private static void applyExternalChargesForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		BigDecimal appropriateChargePerPax;
		BigDecimal[] ondAmounts;
		PaymentAssembler paymentAssembler;
		int ondIndex = 0;
		int ondSize = 0;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();
			paymentAssembler = (PaymentAssembler) reservationPax.getPayment();

			ccExternalChgs = paymentAssembler.getPerPaxGenericExternalCharges();

			if (ccExternalChgs.size() > 0) {
				ondSize = reservationPax.getPnrPaxFares().size();
				Collection<DiscountChargeTO> discountChargeTOs = null;
				if (reservationDiscountDTO != null) {
					discountChargeTOs = reservationDiscountDTO.getPaxDiscountChargeTOs(reservationPax.getPaxSequence());
				}

				for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
					appropriateChargePerPax = externalChgDTO.getAmount();
					ondAmounts = AccelAeroCalculator.roundAndSplit(appropriateChargePerPax, ondSize);

					ondIndex = 0;
					itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
					while (itReservationPaxFare.hasNext()) {
						reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

						ExternalChgDTO updatedChgDTO = (ExternalChgDTO) externalChgDTO.clone();
						updatedChgDTO.setAmount(ondAmounts[ondIndex]);
						ReservationApiUtils.updatePaxOndExternalGenericChargeDiscountInfo(reservationPaxFare, paxDiscInfo,
								updatedChgDTO, discountChargeTOs);

						ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(ondAmounts[ondIndex], null,
								externalChgDTO.getChgRateId(), externalChgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO,
								false, paxDiscInfo, null, null, 0);
						if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()) {
							reservationPaxOndCharge.setOperationType(CommonsConstants.ModifyOperation.CREATE_RES.getOperation());
						}
						ondIndex++;
						updateExist = true;
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}
	}

	/**
	 * Apply external charges for a payment booking
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForAPaymentBooking(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPayment, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();

		Map<Integer, ReservationSegmentDTO> mapPnrSegIdAndReservationSegDTO = getSegmentDetailInformation(reservation.getPnr());
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		BigDecimal appropriateChargePerPax;
		BigDecimal[] ondAmounts;
		PaymentAssembler paymentAssembler;
		Collection<Integer> colActiveONDPnrPaxFareIds;
		int ondIndex = 0;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;
		List<ReservationPaxOndCharge> arrChargePerPax;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (passengerPayment.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(reservationPax.getPnrPaxId());
				ccExternalChgs = paymentAssembler.getPerPaxGenericExternalCharges();

				if (ccExternalChgs.size() > 0) {
					arrChargePerPax = new ArrayList<ReservationPaxOndCharge>();

					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
						appropriateChargePerPax = externalChgDTO.getAmount();

						colActiveONDPnrPaxFareIds = getApplicableONDPnrPaxFareIds(currentDate, reservationPax.getPnrPaxFares(),
								mapPnrSegIdAndReservationSegDTO, reservationPax.geteTickets(), false);
						ondAmounts = AccelAeroCalculator.roundAndSplit(appropriateChargePerPax, colActiveONDPnrPaxFareIds.size());

						ondIndex = 0;
						itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
						while (itReservationPaxFare.hasNext()) {
							reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

							// Only adding if it's an active OND
							if (colActiveONDPnrPaxFareIds.contains(reservationPaxFare.getPnrPaxFareId())) {
								ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
										.captureReservationPaxOndCharge(ondAmounts[ondIndex], null, externalChgDTO.getChgRateId(),
												externalChgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, null,
												null, null, chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));
								if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()) {
									reservationPaxOndCharge.setOperationType(externalChgDTO.getOperationMode());
								}
								arrChargePerPax.add(reservationPaxOndCharge);
								ondIndex++;
								updateExist = true;
							}
						}
					}
					// Haider
					mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), arrChargePerPax);
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	/**
	 * Returns the detail segment information
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, ReservationSegmentDTO> getSegmentDetailInformation(String pnr) throws ModuleException {
		Map<Integer, ReservationSegmentDTO> pnrSegIdAndReservationSegmentDTOMap = new HashMap<Integer, ReservationSegmentDTO>();
		Collection<ReservationSegmentDTO> colReservationSegmentDTO = ReservationProxy.getPnrSegmentsView(pnr, false);
		Iterator<ReservationSegmentDTO> itColReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		while (itColReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = (ReservationSegmentDTO) itColReservationSegmentDTO.next();
			pnrSegIdAndReservationSegmentDTOMap.put(reservationSegmentDTO.getPnrSegId(), reservationSegmentDTO);
		}

		return pnrSegIdAndReservationSegmentDTOMap;
	}

	/**
	 * Returns if this OND is a new OND or not
	 * 
	 * @param reservationPaxFare
	 * @return
	 */
	private static boolean isNewOND(ReservationPaxFare reservationPaxFare) {
		if (reservationPaxFare.getPnrPaxFareId() == null) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the active OND's pnr pax fare id(s)
	 * 
	 * @param currentDate
	 * @param colReservationPaxFare
	 * @param mapPnrSegIdAndReservationSegDTO
	 * @param eTicketTOs
	 * @param skipFlownCheck
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Integer> getApplicableONDPnrPaxFareIds(Date currentDate,
			Collection<ReservationPaxFare> colReservationPaxFare,
			Map<Integer, ReservationSegmentDTO> mapPnrSegIdAndReservationSegDTO, Collection<EticketTO> eTicketTOs,
			boolean skipFlownCheck) throws ModuleException {
		Collection<Integer> colPnrPaxFareIds = new HashSet<Integer>();
		Collection<Integer> allPnrPaxFareIds = new HashSet<Integer>();
		Collection<Integer> colPnrSegIds;
		Collection<Integer> cnxPnrSegIds;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		ReservationSegment reservationSegment;
		ReservationSegmentDTO reservationSegmentDTO;

		Iterator<ReservationPaxFare> itColReservationPaxFare = colReservationPaxFare.iterator();
		while (itColReservationPaxFare.hasNext()) {
			reservationPaxFare = (ReservationPaxFare) itColReservationPaxFare.next();

			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
			colPnrSegIds = new ArrayList<Integer>();
			cnxPnrSegIds = new ArrayList<Integer>();
			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();
				reservationSegment = reservationPaxFareSegment.getSegment();
				reservationSegmentDTO = (ReservationSegmentDTO) mapPnrSegIdAndReservationSegDTO
						.get(reservationSegment.getPnrSegId());
				if (reservationSegmentDTO == null) {
					throw new ModuleException("airreservations.externalcharge.corruptedExtCharges");
				}

				if (reservationPaxFareSegment.getPnrPaxFareSegId() != null
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())
						&& AppSysParamsUtil.getFlownCalculateMethod() != FLOWN_FROM.DATE && eTicketTOs != null
						&& eTicketTOs.size() > 0) {
					boolean isFlown = false;
					if (!skipFlownCheck) {
						for (EticketTO eticketTO : eTicketTOs) {
							if (eticketTO.getPnrPaxFareSegId().equals(reservationPaxFareSegment.getPnrPaxFareSegId())) {
								if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET
										&& eticketTO.getTicketStatus().equals(EticketStatus.FLOWN.code())) {
									isFlown = true;
								} else if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.PFS && eticketTO
										.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
									isFlown = true;
								}else if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()
										&&	(EticketStatus.CHECKEDIN.code().equals(eticketTO.getTicketStatus())
												|| EticketStatus.BOARDED.code().equals(eticketTO.getTicketStatus()))){
									isFlown = true;
								}
								break;
							}
						}
					}

					if (!isFlown) {
						colPnrSegIds.add(reservationSegment.getPnrSegId());
					}

				} else {
					if (currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) < 0) {
						if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationSegment.getStatus())) {
							colPnrSegIds.add(reservationSegment.getPnrSegId());
						} else {
							cnxPnrSegIds.add(reservationSegment.getPnrSegId());
						}
					}
				}
			}

			// If all OND's segments are confirmed and any one of that segment is not flown
			if (reservationPaxFare.getPaxFareSegments().size() == colPnrSegIds.size()) {
				colPnrPaxFareIds.add(reservationPaxFare.getPnrPaxFareId());
			}

			if (reservationPaxFare.getPaxFareSegments().size() == cnxPnrSegIds.size()) {
				allPnrPaxFareIds.add(reservationPaxFare.getPnrPaxFareId());
			}
		}

		// In case of all segment cancellation, if there is balance to pay with external charge (e.g. credit card fee)
		// all the cancelled segments will be considered and updated.
		if (colPnrPaxFareIds.size() == 0 && allPnrPaxFareIds.size() > 0) {
			colPnrPaxFareIds = allPnrPaxFareIds;
		}

		if (colPnrPaxFareIds.size() == 0) {
			throw new ModuleException("airreservations.externalcharge.noValidOndFound");
		}

		return colPnrPaxFareIds;
	}

	/**
	 * Returns the ond size
	 * 
	 * @param pnrPaxFares
	 * @param allSegments
	 * @return
	 */
	private static int getNewOndSize(Collection<ReservationPaxFare> pnrPaxFares) {
		Iterator<ReservationPaxFare> itReservationPaxFare = pnrPaxFares.iterator();
		ReservationPaxFare reservationPaxFare;
		int size = 0;
		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

			if (isNewOND(reservationPaxFare)) {
				size++;
			}
		}

		return size;
	}

	/**
	 * Add external charges for payment assembler
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerPayment
	 */
	private static void addExternalChgsForPaymentAssembler(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, PaymentAssembler> passengerPayment) {
		Iterator<Integer> itPnrPaxIds = passengerPayment.keySet().iterator();
		PaymentAssembler paymentAssembler;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> arrChargePerPax;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(pnrPaxId);

				arrChargePerPax = mapPnrPaxIdAdjustments.get(pnrPaxId);
				for (ReservationPaxOndCharge reservationPaxOndCharge : arrChargePerPax) {
					paymentAssembler.setTotalChargeAmount(AccelAeroCalculator.add(paymentAssembler.getTotalChargeAmount(),
							reservationPaxOndCharge.getAmount()));
				}
			}
		}
	}

	/**
	 * Add external charges for the revenue information
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerRevenueMap
	 */
	private static void addExternalChgsForRevenueMap(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, RevenueDTO> passengerRevenueMap) {
		Iterator<Integer> itPnrPaxIds = passengerRevenueMap.keySet().iterator();
		RevenueDTO revenueDTO;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> arrChargePerPax;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				revenueDTO = (RevenueDTO) passengerRevenueMap.get(pnrPaxId);
				arrChargePerPax = mapPnrPaxIdAdjustments.get(pnrPaxId);

				for (ReservationPaxOndCharge reservationPaxOndCharge : arrChargePerPax) {
					revenueDTO.setAddedTotal(
							AccelAeroCalculator.add(revenueDTO.getAddedTotal(), reservationPaxOndCharge.getAmount()));
					revenueDTO.getAddedCharges().add(reservationPaxOndCharge);
				}
			}
		}
	}
	
	private static Collection<ReservationPaxFare> getInfantChargeApplicablePaxFares(ReservationPax parentPax,
			Collection<Integer> applicablePnrPaxFareIds, Set<ReservationPaxFare> addedInfantPaxFares) {
		List<Integer> pnrSegIds = new ArrayList<>();
		Collection<ReservationPaxFare> colPnrPaxFareIds = new HashSet<ReservationPaxFare>();
		if (parentPax != null && applicablePnrPaxFareIds != null && !applicablePnrPaxFareIds.isEmpty()
				&& addedInfantPaxFares != null && !addedInfantPaxFares.isEmpty()) {
			for (ReservationPaxFare parentPaxFare : parentPax.getPnrPaxFares()) {
				if (applicablePnrPaxFareIds.contains(parentPaxFare.getPnrPaxFareId())) {
					//
					for (ReservationPaxFareSegment parentFareSegment : parentPaxFare.getPaxFareSegments()) {
						pnrSegIds.add(parentFareSegment.getPnrSegId());
					}
				}
			}

			for (ReservationPaxFare infantPaxFare : addedInfantPaxFares) {
				for (ReservationPaxFareSegment infantFareSegment : infantPaxFare.getPaxFareSegments()) {
					if (pnrSegIds.contains(infantFareSegment.getPnrSegId())) {
						colPnrPaxFareIds.add(infantPaxFare);
					}

				}
			}
		}

		return colPnrPaxFareIds;

	}
	
	public static void reflectExternalChgForBookingCancellation(Reservation reservation,
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		// Apply these changes for the reservation
		applyExternalChargesForBookingCancellation(reservation, paxExternalCharges, credentialsDTO, chgTnxGen, null);
	}
	
	public static void reflectExternalChgForSegmentCancellation(Reservation reservation,
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen,
			Collection<Integer> allPnrSegsToBeCnx) throws ModuleException {
		// Apply these changes for the reservation
		applyExternalChargesForBookingCancellation(reservation, paxExternalCharges, credentialsDTO, chgTnxGen, allPnrSegsToBeCnx);
	}

	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForBookingCancellation(Reservation reservation,
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen,
			Collection<Integer> allPnrSegsToBeCnx) throws ModuleException {

		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();

		Map<Integer, ReservationSegmentDTO> mapPnrSegIdAndReservationSegDTO = getSegmentDetailInformation(reservation.getPnr());
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		BigDecimal appropriateChargePerPax;
		BigDecimal[] ondAmounts;
		Collection<Integer> colActiveONDPnrPaxFareIds = null;
		int ondIndex = 0;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;
		List<ReservationPaxOndCharge> arrChargePerPax;

		if (paxExternalCharges != null && !paxExternalCharges.isEmpty()) {
			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();

				if (paxExternalCharges.containsKey(reservationPax.getPaxSequence())) {
					ccExternalChgs = (List<ExternalChgDTO>) paxExternalCharges.get(reservationPax.getPaxSequence());

					if (ccExternalChgs.size() > 0) {
						arrChargePerPax = new ArrayList<ReservationPaxOndCharge>();

						for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
							appropriateChargePerPax = externalChgDTO.getAmount();

							if (allPnrSegsToBeCnx != null && !allPnrSegsToBeCnx.isEmpty()) {
								// cancel segment operation handling fee will be applied only to the Cancelled segment
								colActiveONDPnrPaxFareIds = getApplicableONDPnrPaxFareIdsForSegmentCancellation(
										reservationPax.getPnrPaxFares(), allPnrSegsToBeCnx);
							}
							if (colActiveONDPnrPaxFareIds == null || colActiveONDPnrPaxFareIds.isEmpty()
									|| allPnrSegsToBeCnx == null || allPnrSegsToBeCnx.isEmpty()) {
								colActiveONDPnrPaxFareIds = getApplicableONDPnrPaxFareIds(currentDate,
										reservationPax.getPnrPaxFares(), mapPnrSegIdAndReservationSegDTO,
										reservationPax.geteTickets(), false);
							}

							ondAmounts = AccelAeroCalculator.roundAndSplit(appropriateChargePerPax,
									colActiveONDPnrPaxFareIds.size());

							ondIndex = 0;
							itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
							while (itReservationPaxFare.hasNext()) {
								reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

								// Only adding if it's an active OND
								if (colActiveONDPnrPaxFareIds.contains(reservationPaxFare.getPnrPaxFareId())) {
									ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
											.captureReservationPaxOndCharge(ondAmounts[ondIndex], null,
													externalChgDTO.getChgRateId(), externalChgDTO.getChgGrpCode(),
													reservationPaxFare, credentialsDTO, false, null, null, null,
													chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

									if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()) {
										reservationPaxOndCharge.setOperationType(externalChgDTO.getOperationMode());
									}

									arrChargePerPax.add(reservationPaxOndCharge);
									ondIndex++;
									updateExist = true;
								}
							}
						}
						// Haider
						mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), arrChargePerPax);
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	public static Collection<Integer> getApplicableONDPnrPaxFareIdsForSegmentCancellation(
			Collection<ReservationPaxFare> colReservationPaxFare, Collection<Integer> allPnrSegsToBeCnx) throws ModuleException {
		Collection<Integer> colPnrPaxFareIds = new HashSet<Integer>();

		for (Integer pnrSegId : allPnrSegsToBeCnx) {
			for (ReservationPaxFare reservationPaxFare : colReservationPaxFare) {
				for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
					ReservationSegment reservationSegment = reservationPaxFareSegment.getSegment();
					if (pnrSegId.equals(reservationSegment.getPnrSegId())) {
						colPnrPaxFareIds.add(reservationPaxFare.getPnrPaxFareId());
					}
				}
			}
		}

		return colPnrPaxFareIds;
	}
}