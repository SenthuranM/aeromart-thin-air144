package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.RecordCancelTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 */
public class RecordCancelBO {

	private Collection<RecordCancelTO> colRecordCancelTO;

	public RecordCancelBO() {
		this.setColRecordCancelTO(new ArrayList<RecordCancelTO>());
	}

	public void addRecord(String pnrPaxId, BigDecimal creditTotal, BigDecimal chargeAmount, BigDecimal penaltyAmount,
			int actionNC, int chargeNC, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity) throws ModuleException {

		RecordCancelTO recordCancelTO = new RecordCancelTO();
		recordCancelTO.setPnrPaxId(pnrPaxId);
		recordCancelTO.setCreditTotal(creditTotal);
		recordCancelTO.setChargeAmount(chargeAmount);
		recordCancelTO.setActionNC(actionNC);
		recordCancelTO.setChargeNC(chargeNC);
		recordCancelTO.setReservationPaxPaymentMetaTO(reservationPaxPaymentMetaTO);
		recordCancelTO.setCredentialsDTO(credentialsDTO);
		recordCancelTO.setEnableTransactionGranularity(enableTransactionGranularity);
		recordCancelTO.setModificationPenalty(penaltyAmount);
		this.getColRecordCancelTO().add(recordCancelTO);
	}

	public void execute() throws ModuleException {
		if (this.getColRecordCancelTO() != null && this.getColRecordCancelTO().size() > 0) {
			for (RecordCancelTO recordCancelTO : this.getColRecordCancelTO()) {
				ReservationModuleUtils.getRevenueAccountBD().recordCancel(recordCancelTO.getPnrPaxId(),
						recordCancelTO.getCreditTotal(), recordCancelTO.getChargeAmount(),
						recordCancelTO.getModificationPenalty(), recordCancelTO.getActionNC(), recordCancelTO.getChargeNC(),
						recordCancelTO.getReservationPaxPaymentMetaTO(), recordCancelTO.getCredentialsDTO(),
						recordCancelTO.isEnableTransactionGranularity());
			}
		}
	}

	/**
	 * @return the colRecordCancelTO
	 */
	private Collection<RecordCancelTO> getColRecordCancelTO() {
		return colRecordCancelTO;
	}

	/**
	 * @param colRecordCancelTO
	 *            the colRecordCancelTO to set
	 */
	private void setColRecordCancelTO(Collection<RecordCancelTO> colRecordCancelTO) {
		this.colRecordCancelTO = colRecordCancelTO;
	}

}
