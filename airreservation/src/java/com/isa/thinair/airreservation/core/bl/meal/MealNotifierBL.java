/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.meal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jfree.util.Log;

import com.isa.thinair.airreservation.api.dto.meal.MealNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author Indika
 * 
 */
public class MealNotifierBL {

	private ReservationAuxilliaryDAO reservationAuxilliaryDAO = null;
//	private static boolean isHubwiseNotification = false;
	private GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
	private static int defaultMealRequestStartCutoverMins = 0;
	private static int defaultMealNotifyFrequencyMins = -1;
	private static int defaultLastMealNotificationMins = 0;
	private static int maxNoOfAttempts = 1;

	public MealNotifierBL() {

		this.reservationAuxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		String mealRequestStartCutover = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_START_CUTOVER);
		String mealRequestStopCutover = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_STOP_CUTOVER);
		maxNoOfAttempts = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_ATTEMPTS));
		defaultMealRequestStartCutoverMins = Integer.parseInt(mealRequestStartCutover);
		defaultLastMealNotificationMins = Integer.parseInt(mealRequestStopCutover);

	}

	public ArrayList<MealNotificationDetailsDTO> getFlightForMealNotifyScheduling() throws ModuleException {

	//	Date date = new Date();
	//	Object defaultPnldepartureGap;
		List<String> listHubs = new ArrayList<String>();

		// Parameter for hub enabling.
		String hubsBasedAlert = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFY_FOR_HUBS); // Get from app
																								// parameter
		if (hubsBasedAlert != null && !hubsBasedAlert.trim().equals("")) {
			String[] colHubs = hubsBasedAlert.split(",");
			listHubs = Arrays.asList(colHubs);
		}

		// Get max departure time gap

		Collection<String> colMealNotifyTimings = reservationAuxilliaryDAO.getMealNotificationTimings();

		int maxDepartureTimeGap = getMaxMealNotifyTimingInMins(colMealNotifyTimings);

		ArrayList<MealNotificationDetailsDTO> list = reservationAuxilliaryDAO.getFlightsForMealNotificaitionScheduling(
				maxDepartureTimeGap, new Date(), listHubs); // =
															// pnlAdlTimingDAO.getFlightsForPNLScheduling(defaultPnldepartureGap,
															// date);
		Log.info("================================================================");
		Log.info("====Flights returned for notification" + list.size());
		Log.info("================================================================");

		// Get flight level timings
		Map<String, MealNotificationDetailsDTO> mapFlightLevelTimings = getFlightLevelTimings();
		// Get airport level timings
		Map<String, MealNotificationDetailsDTO> mapAirportLevelTimings = getAirportLevelTimings();

		// Get Default timings

		for (MealNotificationDetailsDTO mealNotificationDetailsDTO : list) {

			// If flight level defined. Override with flight level values
			if (mapFlightLevelTimings.containsKey(mealNotificationDetailsDTO.getFlightNumber())) {
				overrideTimings(mealNotificationDetailsDTO,
						mapFlightLevelTimings.get(mealNotificationDetailsDTO.getFlightNumber()));
			}
			// If Airport level defined. override with flight level values
			else if (mapAirportLevelTimings.containsKey(mealNotificationDetailsDTO.getDepartureStation())) {
				overrideTimings(mealNotificationDetailsDTO,
						mapAirportLevelTimings.get(mealNotificationDetailsDTO.getDepartureStation()));
			}
			// Apply default values
			else {
				mealNotificationDetailsDTO.setNotifyStartTime(defaultMealRequestStartCutoverMins);
				mealNotificationDetailsDTO.setNotifyFrequency(defaultMealNotifyFrequencyMins);
				mealNotificationDetailsDTO.setLastNotification(defaultLastMealNotificationMins);
			}
		}
		return list;
	}

	private void overrideTimings(MealNotificationDetailsDTO oldMealNotificationDetailsDTO,
			MealNotificationDetailsDTO newMealNotificationDetailsDTO) {
		oldMealNotificationDetailsDTO.setNotifyStartTime(newMealNotificationDetailsDTO.getNotifyStartTime());
		oldMealNotificationDetailsDTO.setNotifyFrequency(newMealNotificationDetailsDTO.getNotifyFrequency());
		oldMealNotificationDetailsDTO.setLastNotification(newMealNotificationDetailsDTO.getLastNotification());
	}

	private Map<String, MealNotificationDetailsDTO> getFlightLevelTimings() {
		Map<String, MealNotificationDetailsDTO> mapFlightNumberMealNotifications = new HashMap<String, MealNotificationDetailsDTO>();
		Collection<MealNotificationDetailsDTO> mealNotificationDetails = reservationAuxilliaryDAO
				.getMealNotificationDetails(true);

		for (MealNotificationDetailsDTO mealNotificationDetailsDTO : mealNotificationDetails) {
			mapFlightNumberMealNotifications.put(mealNotificationDetailsDTO.getFlightNumber(), mealNotificationDetailsDTO);
		}
		return mapFlightNumberMealNotifications;
	}

	private Map<String, MealNotificationDetailsDTO> getAirportLevelTimings() {
		Map<String, MealNotificationDetailsDTO> mapFlightNumberMealNotifications = new HashMap<String, MealNotificationDetailsDTO>();
		Collection<MealNotificationDetailsDTO> mealNotificationDetails = reservationAuxilliaryDAO
				.getMealNotificationDetails(false);

		for (MealNotificationDetailsDTO mealNotificationDetailsDTO : mealNotificationDetails) {
			mapFlightNumberMealNotifications.put(mealNotificationDetailsDTO.getDepartureStation(), mealNotificationDetailsDTO);
		}
		return mapFlightNumberMealNotifications;
	}

	private int getMaxMealNotifyTimingInMins(Collection<String> colMealNotifyTimings) {
		int maxTiming = 0;
		String mealRequestStartCutover = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_START_CUTOVER);// 16

		// Set Default start cutover
		maxTiming = Integer.parseInt(mealRequestStartCutover) + 15;

		for (Iterator<String> iterator = colMealNotifyTimings.iterator(); iterator.hasNext();) {
			String mealNotifyTiming = (String) iterator.next();
			int notifyTimeInMinutes = Util.getTotalMinutes(mealNotifyTiming);

			if (notifyTimeInMinutes > maxTiming) {
				maxTiming = notifyTimeInMinutes;
			}
		}
		return maxTiming;
	}

	public void sendMealNotificationsNew(List<Integer> flightIds) throws ModuleException {

		Object[] mealsCollection = null;
		List<String> listHubs = new ArrayList<String>();
		// Parameter for hub enabling.
		String hubsBasedAlert = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFY_FOR_HUBS); // Get from app
																								// parameter
		if (hubsBasedAlert != null && !hubsBasedAlert.trim().equals("")) {
			String[] colHubs = hubsBasedAlert.split(",");
			listHubs = Arrays.asList(colHubs);
		}
		mealsCollection = ReservationModuleUtils.getFlightInventoryBD().getMealsListForStation(listHubs, null, null,
				maxNoOfAttempts, flightIds);
		if (mealsCollection != null) {
			ReservationModuleUtils.getFlightInventoryBD().sendMealsListPerRoute(mealsCollection);
		}
	}

}
