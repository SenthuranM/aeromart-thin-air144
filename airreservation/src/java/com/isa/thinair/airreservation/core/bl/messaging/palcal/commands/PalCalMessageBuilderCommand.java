package com.isa.thinair.airreservation.core.bl.messaging.palcal.commands;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessage;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessageResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdditionalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.messagebuilderfactory.PnlAdlMessageBuilderFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses.MessageResponseAdditionals;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 *
 * @isa.module.command name="palCalMessageBuilderCommand"
 */
public class PalCalMessageBuilderCommand extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(PalCalMessageBuilderCommand.class);

	private PnlAdlMessageBuilderFactory dcsPaxMessageBuilderFactory;
	private PnlAdlMessage palCalMessage;
	private PassengerCollection passengerCollection;
	private PnlAdlMessageResponse palCalMessageResponse;
	private BaseDataContext baseDataContext;
	private String messageType;
	private DefaultServiceResponse response;

	@Override
	public ServiceResponce execute() throws ModuleException {

		createDataFactory();
		resolveCommandParameters();
		messageType = getMessageType();
		palCalMessage = dcsPaxMessageBuilderFactory.getPnlAdlMessegeBuilder(messageType);
		palCalMessageResponse = palCalMessage.buildMessage(baseDataContext);
		createResponseObject(response);
		return response;
	
	}

	private void createDataFactory() {
		dcsPaxMessageBuilderFactory = new PnlAdlMessageBuilderFactory();
	}

	private void resolveCommandParameters() {
		baseDataContext = (BaseDataContext) this
				.getParameter(CommandParamNames.PNL_DATA_CONTEXT);
		response = (DefaultServiceResponse) this
				.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		passengerCollection = baseDataContext.getPassengerCollection();
	}

	private String getMessageType() {
		return baseDataContext.getMessageType();
	}
	
	private MessageResponseAdditionals createMessageResponseAdditionals(){
		MessageResponseAdditionals additionals = new MessageResponseAdditionals();
		additionals.setFareClassWisePaxCount((HashMap)palCalMessageResponse.getFareClassWisePaxCount());
		additionals.setPnrCollection(palCalMessageResponse.getPnrCollection());
		additionals.setPnrPaxIdvsSegmentIds(palCalMessageResponse.getPnrPaxIdvsSegmentIds());
		additionals.setPnrPaxVsGroupCodes(palCalMessageResponse.getPnrPaxVsGroupCodes());
		additionals.setLastGroupCode(palCalMessageResponse.getLastGroupCode());
		additionals.setPassengerInformations(passengerCollection.getPassengerInformations());
		return additionals;
	}

	private PnlAdditionalDataContext createAdditionalDataContext() {
		PnlAdditionalDataContext additionalDataContext = new PnlAdditionalDataContext();
		additionalDataContext.setDepartureAirportCode(baseDataContext
				.getDepartureAirportCode());
		additionalDataContext.setPassengerCollection(passengerCollection);
		return additionalDataContext;
	}

	private void createResponseObject(DefaultServiceResponse response) {
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE,
				response);
		response.addResponceParam(CommandParamNames.PNL_DATA_CONTEXT,
				baseDataContext);
		response.addResponceParam(CommandParamNames.PNL_MESSAGES_PARTS,
				palCalMessageResponse.getMessagePartMap());
		response.addResponceParam(CommandParamNames.PNL_MESSAGE_RESPONSE_ADDITIONALS, createMessageResponseAdditionals());
	}
}
