package com.isa.thinair.airreservation.core.bl.tax;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface TaxInvoiceFactory {

	public List<String> generate(Reservation reservation, TransactionSequence transactionSequence, List<TaxInvoice> taxInvoices,
			Collection<ReservationSegmentDTO> segmentDTOsBeforeCancellation)
			throws ModuleException;

	public boolean isApplicable(OperationChgTnx chargeTnx);

}
