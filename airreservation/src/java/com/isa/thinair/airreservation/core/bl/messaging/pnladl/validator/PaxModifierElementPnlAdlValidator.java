package com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator.base.BaseElementBuilderValidator;

public class PaxModifierElementPnlAdlValidator extends BaseElementBuilderValidator {

	@Override
	public boolean validate(String messageType) {
		boolean isValid = false;
		if (MessageTypes.ADL.toString().equals(messageType)) {
			isValid = true;
		}
		return isValid;
	}

	@Override
	public PassengerStoreTypes getOngoingStoreType(String messageType, PassengerStoreTypes paxStoreType) {
		if (MessageTypes.ADL.toString().equals(messageType)) {
			return paxStoreType;
		} else if (MessageTypes.PNL.toString().equals(messageType)) {
			return PassengerStoreTypes.ADDED;
		}
		return null;
	}
}