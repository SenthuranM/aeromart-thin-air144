/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base;

/**
 * @author udithad
 *
 */
public abstract class BaseRule<T> {

	public abstract boolean validateRule(T t);
	
}
