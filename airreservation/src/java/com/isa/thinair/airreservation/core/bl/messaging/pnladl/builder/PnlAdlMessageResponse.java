/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder;

import java.util.List;
import java.util.Map;

/**
 * @author udithad
 *
 */
public class PnlAdlMessageResponse {

	private Map<Integer, String> messagePartMap;
	private Map<Integer, Integer> pnrPaxIdvsSegmentIds;
	private Map<String, Integer> fareClassWisePaxCount;
	private List<String> pnrCollection;
	private Map<Integer, String> pnrPaxVsGroupCodes;
	private String lastGroupCode;
	
	public Map<Integer, String> getMessagePartMap() {
		return messagePartMap;
	}

	public void setMessagePartMap(Map<Integer, String> messagePartMap) {
		this.messagePartMap = messagePartMap;
	}

	public Map<Integer, Integer> getPnrPaxIdvsSegmentIds() {
		return pnrPaxIdvsSegmentIds;
	}

	public void setPnrPaxIdvsSegmentIds(
			Map<Integer, Integer> pnrPaxIdvsSegmentIds) {
		this.pnrPaxIdvsSegmentIds = pnrPaxIdvsSegmentIds;
	}

	public Map<String, Integer> getFareClassWisePaxCount() {
		return fareClassWisePaxCount;
	}

	public void setFareClassWisePaxCount(
			Map<String, Integer> fareClassWisePaxCount) {
		this.fareClassWisePaxCount = fareClassWisePaxCount;
	}

	public List<String> getPnrCollection() {
		return pnrCollection;
	}

	public void setPnrCollection(List<String> pnrCollection) {
		this.pnrCollection = pnrCollection;
	}

	public Map<Integer, String> getPnrPaxVsGroupCodes() {
		return pnrPaxVsGroupCodes;
	}

	public void setPnrPaxVsGroupCodes(Map<Integer, String> pnrPaxVsGroupCodes) {
		this.pnrPaxVsGroupCodes = pnrPaxVsGroupCodes;
	}

	public String getLastGroupCode() {
		return lastGroupCode;
	}

	public void setLastGroupCode(String lastGroupCode) {
		this.lastGroupCode = lastGroupCode;
	}

}
