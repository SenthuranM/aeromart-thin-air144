package com.isa.thinair.airreservation.core.bl.reminder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.NotificationDetailInfoDTO;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.PNRInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.auditor.api.dto.OnlineCheckInReminderAuditDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;

public class OnlineCheckInReminderSenderBL {

	private final Log log = LogFactory.getLog(OnlineCheckInReminderSenderBL.class);

	public void sendOnlineCheckInReminderNotification(OnlineCheckInReminderDTO onlineCheckInReminderDTO,
			CredentialsDTO credentialsDTO) throws ModuleException {

		Integer fltSegEventId = null;
		boolean success = false;
		Throwable exceptionToAudit = null;

		String ibeOnlineCheckInURL = AppSysParamsUtil.getIBEOnlineCheckInURL();
		Integer flightSegId = onlineCheckInReminderDTO.getFltSegId();
		String flightNo = onlineCheckInReminderDTO.getFlightNo();
		Date departureTimeZulu = onlineCheckInReminderDTO.getDepartureDate();
		String jobName = onlineCheckInReminderDTO.getJobName();
		String jobGroupName = onlineCheckInReminderDTO.getJobGroupName();
		String segCode = onlineCheckInReminderDTO.getSegCode();
		String origin = onlineCheckInReminderDTO.getOrigin();
		String ibeURL = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL);
		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		long before = 0;
		long after = 0;

		try {

			List<PNRInfoDTO> pnrInfoDTOs = reservationDAO.getInfoForOnlineCheckinReminder(flightSegId);
			List<NotificationDetailInfoDTO> pnrsListTosendReminder = new ArrayList<NotificationDetailInfoDTO>();

			if (pnrInfoDTOs != null && pnrInfoDTOs.size() > 0) {
				before = new Date().getTime();

				fltSegEventId = flightBD.updateFlightSegmentNotifyStatus(flightSegId,
						onlineCheckInReminderDTO.getFlightSegmentNotificationId(),
						ReservationInternalConstants.FlightSegNotifyEvent.INPROGRESS, "", 0,
						ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER);
				if (log.isDebugEnabled()) {
					log.debug("Online Checkin Reminder:::ReservationServiceBean::  --- UPDATED FLIGHT SEGMENT STATUS AS 'INPROGRCESS'.");
				}

				for (PNRInfoDTO pnrInfoDTO : pnrInfoDTOs) {
					NotificationDetailInfoDTO notificationDetailInfoDTO = new NotificationDetailInfoDTO();
					notificationDetailInfoDTO.setFlightDepartureTimeZulu(departureTimeZulu);
					notificationDetailInfoDTO.setPnr(pnrInfoDTO.getPnr());
					notificationDetailInfoDTO
							.setOrigin(ReservationModuleUtils.getAirportBD().getAirport(origin).getAirportName());
					notificationDetailInfoDTO.setName(pnrInfoDTO.getFirstName().substring(0, 1).toUpperCase()
							+ pnrInfoDTO.getFirstName().substring(1));
					notificationDetailInfoDTO.setLastName(pnrInfoDTO.getLastName().substring(0, 1).toUpperCase()
							+ pnrInfoDTO.getLastName().substring(1));
					notificationDetailInfoDTO.setEmail(pnrInfoDTO.getEmail());
					notificationDetailInfoDTO.setTitle(pnrInfoDTO.getTitle().substring(0, 1)
							+ pnrInfoDTO.getTitle().substring(1).toLowerCase());
					notificationDetailInfoDTO.setPnrSegId(pnrInfoDTO.getPnrSegId());
					notificationDetailInfoDTO.setPnrSegNotificationEventId(pnrInfoDTO.getPnrSegNotificationEventId());
					notificationDetailInfoDTO.setQrtzJobName(jobName);
					notificationDetailInfoDTO.setQrtzJobGroupName(jobGroupName);
					notificationDetailInfoDTO.setIteneraryLanguage(pnrInfoDTO.getPrefferdLanguage());
					notificationDetailInfoDTO.setFlightNo(flightNo);
					notificationDetailInfoDTO.setFlightSegmentId(flightSegId);
					notificationDetailInfoDTO.setOnlineCheckInURL(ibeOnlineCheckInURL);
					notificationDetailInfoDTO.setFlightSegNotificationEventId(fltSegEventId);
					notificationDetailInfoDTO.setIbeURL(ibeURL);

					pnrsListTosendReminder.add(notificationDetailInfoDTO);
					log.debug("Online Checkin Reminder:::ReservationServiceBean::   Send Email For Online Checkin Reminder "
							+ pnrInfoDTO.getEmail());
				}
			}

			if (pnrsListTosendReminder.size() > 0) {
				log.debug("Online Checkin Reminder:::ReservationServiceBean::   Send Email For Online Checkin Reminder ");
				emailOnlineCheckInReminder(pnrsListTosendReminder, credentialsDTO);
			}

			success = true;

		} catch (ModuleException exception) {
			log.error(" ((o)) ModuleException::sendOnlineCheckInReminderNotification ", exception);
			exceptionToAudit = exception;
			throw new ModuleException(exception, exception.getExceptionCode(), exception.getModuleCode());
		} catch (CommonsDataAccessException exception) {
			log.error(" ((o)) ModuleException::sendOnlineCheckInReminderNotification ", exception);
			exceptionToAudit = exception;
			throw new ModuleException(exception, exception.getExceptionCode(), exception.getModuleCode());
		} catch (Exception exception) {
			after = new Date().getTime();
			log.error(" ((o)) ModuleException::sendOnlineCheckInReminderNotification ", exception);
			exceptionToAudit = exception;
			System.out.println(before);
			System.out.println(after);
			System.out.println(after - before);
		} finally {
			if (!success) {
				log.debug("OnlineCheckinReminder:::ReservationServiceBean::   Update Status for Flight by 'FAILED' inside Finally ");
				// Update Flight Segment Status as 'FAILED'
				flightBD.updateFlightSegmentNotifyStatus(flightSegId, fltSegEventId,
						ReservationInternalConstants.FlightSegNotifyEvent.FAILED, exceptionToAudit.getMessage(), 0,
						ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER);
			}
		}

	}

	private static Map<String, NotificationDetailInfoDTO>
			getOnlineCheckInReminderDataMap(NotificationDetailInfoDTO detailInfoDTO) {
		Map<String, NotificationDetailInfoDTO> onlineCheckInDataMap = new HashMap<String, NotificationDetailInfoDTO>();

		onlineCheckInDataMap.put("notificationDetailDto", detailInfoDTO);

		return onlineCheckInDataMap;
	}

	private static void emailOnlineCheckInReminder(List<NotificationDetailInfoDTO> pnrListToSendReminder,
			CredentialsDTO credentialsDTO) throws ModuleException {

		List<MessageProfile> messageProfileList = new ArrayList<MessageProfile>();

		for (NotificationDetailInfoDTO notificationDetailInfoDTO : pnrListToSendReminder) {

			String emailAddress = BeanUtils.nullHandler(notificationDetailInfoDTO.getEmail());

			if (emailAddress.length() > 0) {
				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setFirstName(notificationDetailInfoDTO.getName());
				userMessaging.setLastName(notificationDetailInfoDTO.getLastName());
				userMessaging.setToAddres(emailAddress);

				Locale locale = null;
				if (notificationDetailInfoDTO.getIteneraryLanguage() != null) {
					locale = new Locale(notificationDetailInfoDTO.getIteneraryLanguage().trim());
				} else {
					locale = Locale.getDefault();
				}

				List<UserMessaging> messages = new ArrayList<UserMessaging>();
				messages.add(userMessaging);
				HashMap<String, NotificationDetailInfoDTO> onlineCheckInReminderDataMap = (HashMap<String, NotificationDetailInfoDTO>) getOnlineCheckInReminderDataMap(notificationDetailInfoDTO);

				// Topic
				Topic topic = new Topic();
				topic.setTopicParams(onlineCheckInReminderDataMap);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.ONLINE_CHECKIN_REMINDER_EMAIL);
				topic.setLocale(locale);
				topic.setAttachMessageBody(false);
				topic.setAuditInfo(composeAudit(notificationDetailInfoDTO, credentialsDTO));

				// User Profile
				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				messageProfileList.add(messageProfile);
			}
		}

		if (messageProfileList != null && messageProfileList.size() > 0) {
			ReservationModuleUtils.getMessagingServiceBD().sendMessages(messageProfileList);
		}

	}

	private static List<OnlineCheckInReminderAuditDTO> composeAudit(NotificationDetailInfoDTO notificationDetailInfoDTO,
			CredentialsDTO credentialsDTO) throws ModuleException {
		OnlineCheckInReminderAuditDTO onlineCheckInReminderAuditDTO = new OnlineCheckInReminderAuditDTO();
		onlineCheckInReminderAuditDTO.setPnr(notificationDetailInfoDTO.getPnr());
		onlineCheckInReminderAuditDTO.setFlightSegmentId(notificationDetailInfoDTO.getFlightSegmentId());
		onlineCheckInReminderAuditDTO.setFlightSegNotificationId(notificationDetailInfoDTO.getFlightSegNotificationEventId());
		onlineCheckInReminderAuditDTO.setQrtzJobName(notificationDetailInfoDTO.getQrtzJobName());
		onlineCheckInReminderAuditDTO.setQrtzJobGroupName(notificationDetailInfoDTO.getQrtzJobGroupName());
		onlineCheckInReminderAuditDTO.setPnrSegId(notificationDetailInfoDTO.getPnrSegId());
		onlineCheckInReminderAuditDTO.setPnrSegNotificationEventId(notificationDetailInfoDTO.getPnrSegNotificationEventId());
		onlineCheckInReminderAuditDTO.setUserId(credentialsDTO.getUserId());

		List<OnlineCheckInReminderAuditDTO> colOnlineCheckInReminderAudit = new ArrayList<OnlineCheckInReminderAuditDTO>();
		colOnlineCheckInReminderAudit.add(onlineCheckInReminderAuditDTO);

		return colOnlineCheckInReminderAudit;
	}
}
