/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.InsuranceHelper;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.FlightType;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * Command for finalize the assembled reservation
 * 
 * @author Byorn
 * @since 1.0
 * @isa.module.command name="createInsurance" Changed by Haider 19Apr09 create insurance if and only if the reservation
 *                     for a valid pnr is already created and saved into database saving the reservation is done in
 *                     validateResponse function after the insId added to the reservation object this function check if
 *                     the pnr in the parameter is valid
 */
public class CreateInsurance extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CreateInsurance.class);

	/**
	 * Execute method of the CreateReservation command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside CreateInsurance execute");
		DefaultServiceResponse serRes = new DefaultServiceResponse(true);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Object isForceConfirm = this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);		
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		boolean isGdsSale = getParameter(CommandParamNames.IS_GDS_SALE, Boolean.FALSE, Boolean.class);
		
		
		if (isGdsSale) {
			log.debug("GDS payment capturing skip insurance: " + pnr);
			log.debug("Exit CreateInsurance execute");
			return serRes;
		}
		
		boolean forceConfirm = false;
		if (isForceConfirm != null) {
			forceConfirm = ((Boolean) isForceConfirm).booleanValue();
		} else {
			log.error("Need to set TRIGGER_PNR_FORCE_CONFIRMED parameter for this flow " + pnr);
		}

		try {
			Reservation reservation = null;

			if (pnr != null) {
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
				pnrModesDTO.setLoadSegView(true);
				reservation = ReservationProxy.getReservation(pnrModesDTO);
			}

			if (reservation != null) {
				List<InsuranceRequestAssembler> iRequestAssemblers = (List<InsuranceRequestAssembler>) this
						.getParameter(CommandParamNames.INSURANCE_INFO);

				if (iRequestAssemblers == null) {
					iRequestAssemblers = new ArrayList<InsuranceRequestAssembler>();
				}

				// if paying for an onhold reservation this should be set.
				Boolean checkIfHoldExists = (Boolean) this.getParameter(CommandParamNames.INSURANCE_CHK_HOLD);
				CredentialsDTO credDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

				updateReservationAndInsuranceRequests(checkIfHoldExists, reservation, iRequestAssemblers);

				List<ReservationInsurance> resInsurances = reservation.getReservationInsurance();

				// if no insurance info set exit command
				if (iRequestAssemblers == null || iRequestAssemblers.isEmpty() || resInsurances == null
						|| resInsurances.isEmpty()) {
					return new DefaultServiceResponse(true);
				}

				// need to send the pnr to aig
				Iterator<InsuranceRequestAssembler> it = iRequestAssemblers.iterator();

				BigDecimal totalInsuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				while (it.hasNext()) {
					InsuranceRequestAssembler insuranceReq = it.next();
					insuranceReq.setPnr(reservation.getPnr());
					insuranceReq.setMessageId(resInsurances.get(0).getMessageId());
					totalInsuranceAmount = AccelAeroCalculator.add(totalInsuranceAmount, insuranceReq.getTotalPremiumAmount());
				}
				
				// for onhold reservations this should be set to true
				Boolean insuranceSellLater = (Boolean) this.getParameter(CommandParamNames.INSURANCE_HOLD);
				if (insuranceSellLater != null) {
					if (reservation.getTotalAvailableBalance().negate().compareTo(totalInsuranceAmount) > -1) {
						insuranceSellLater = null;
					}
				}

				ModuleException meException = null;

				List<InsuranceResponse> insuranceResponses = null;

				try {
					if (insuranceSellLater != null && Boolean.TRUE.equals(insuranceSellLater) && !forceConfirm) {
						// do nothing , meaning this is a hold reservation
					} else {

						addExtraDetailsForTune(iRequestAssemblers, reservation);
						List<IInsuranceRequest> insuranceRequests = new ArrayList<IInsuranceRequest>();
						for (IInsuranceRequest ireq : iRequestAssemblers) {
							insuranceRequests.add(ireq);
						}

						insuranceResponses = ReservationModuleUtils.getInsuranceClientBD().sellInsurancePolicy(insuranceRequests);
						validateResponses(iRequestAssemblers, insuranceResponses);
					}
				} catch (ModuleException e) {
					log.error("CreateInsurance Error", e);
					meException = e;
				} catch (Exception e) {
					log.error("CreateInsurance Error", e);
					meException = new ModuleException("airreservation.insurance.sell.failed", e);
				} finally {

					if (meException != null) {
						composeFailuerResponses(meException, iRequestAssemblers, insuranceResponses);
					}

					// record insurance info
					// 1)if fresh web booking will save with policy code
					// 2)if onhold reservation status will 'OHD' and response will always be null as aig sell was not
					// called.
					// 3)if paying for onhold, its similar to 1) exception reservation object should have the
					// ReservationInsurance object already loaded
					try {
						recordReservationInsurances(iRequestAssemblers, insuranceResponses, reservation, credDTO);
						Collection<ReservationAudit> colReservationAudit = (Collection<ReservationAudit>) this
								.getParameter(CommandParamNames.RESERVATION_AUDIT_COLLECTION);
						ReservationAudit reservationAudit = null;
						if (colReservationAudit != null) {
							Iterator<ReservationAudit> itColReservationAudit = colReservationAudit.iterator();
							while (itColReservationAudit.hasNext()) {
								reservationAudit = itColReservationAudit.next();
								break;
							}

							String insuranceAudit = InsuranceHelper.getAudit(insuranceResponses);
							if (insuranceAudit != null) {
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.INSURANCE,
										insuranceAudit);
							}

						}

					} catch (Exception e) {
						log.error("Error ocurred in saving insurance info", e);
					}
				}
				reservation.setUserNote(userNotes);
				reservation.setUserNoteType(userNoteType);
				serRes.addResponceParam(CommandParamNames.RESERVATION, reservation);
				serRes.addResponceParam(CommandParamNames.INSURANCE_RES, insuranceResponses);
			}

			if (reservation != null) {
				if (output == null) {
					output = new DefaultServiceResponse();
					output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
				} else {
					output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
				}
			}
			serRes.addResponceParam(CommandParamNames.PNR, this.getParameter(CommandParamNames.PNR));
			serRes.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS, this.getParameter(CommandParamNames.FLIGHT_SEGMENT_IDS));
			serRes.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
			if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
				serRes.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
			}
		} catch (Exception e) {
			log.error("Error in saving insurance info", e);
		}
		return serRes;
	}

	private void updateReservationAndInsuranceRequests(Boolean checkIfHoldExists, Reservation reservation,
			List<InsuranceRequestAssembler> insuranceRequests) throws ModuleException {

		// if paying for hold reservation inusranceReq obj is always null load the reservation insurance get request
		// information
		if (checkIfHoldExists != null && Boolean.TRUE.equals(checkIfHoldExists) && insuranceRequests.isEmpty()) {

			if (reservation.getPnr() != null) {

				List<ReservationInsurance> persitedReservations = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
						.getOnholdReservationInsurance(reservation.getPnr());
				reservation.setReservationInsurance(persitedReservations);

				List<InsuranceRequestAssembler> derivedRequests = deriveInsuranceRequestsForOnhold(reservation);

				if (derivedRequests != null && !derivedRequests.isEmpty()) {
					insuranceRequests.clear();
					insuranceRequests.addAll(derivedRequests);
				}
			}

		} else {

			if (insuranceRequests != null && !insuranceRequests.isEmpty()) {
				Iterator<InsuranceRequestAssembler> it = insuranceRequests.iterator();

				while (it.hasNext()) {
					InsuranceRequestAssembler insuranceRequest = it.next();
					ReservationInsurance reservationInsurance = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
							.getQuotedReservationInsurance(insuranceRequest.getInsuranceId());

					if (reservationInsurance == null) {
						log.error("Cannot locate reservation Insurance for ins_id : " + insuranceRequest.getInsuranceId());
						throw new ModuleException("airreservation.insurance.cannot.locate");
					}

					reservation.addReservationInsurance(reservationInsurance);
					if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
						insuranceRequest.setFlightSegments(InsuranceHelper.createFlightSegList(reservation));
						insuranceRequest.getInsureSegment().setSalesChanelCode(reservationInsurance.getSalesChannel());
						insuranceRequest.setSsrFeeCode(reservationInsurance.getSsrFeeCode());
						insuranceRequest.setPlanCode(reservationInsurance.getPlanCode());

						boolean matching = false;
						if (Math.floor(insuranceRequest.getQuotedTotalPremiumAmount().doubleValue()) == Math
								.round(reservationInsurance.getQuotedAmount().doubleValue())) {
							matching = true;
						} else if (Math.ceil(insuranceRequest.getQuotedTotalPremiumAmount().doubleValue()) == Math
								.round(reservationInsurance.getQuotedAmount().doubleValue())) {
							matching = true;
						}
						
						if (!matching){
							log.info(" Quoted Amount from F/E " + insuranceRequest.getQuotedTotalPremiumAmount());
							log.info(" Quoted Amount from DB " + reservationInsurance.getQuotedAmount());
							log.error(" >>>> F/E quote amount and value in the DB does not match");
							//throw an exception
						}

						

						insuranceRequest.setQuotedTotalPremiumAmount(reservationInsurance.getQuotedAmount());
						insuranceRequest.setInsuranceId(reservationInsurance.getInsuranceId());

					}
					if (!AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.CCC)
							&& !AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
						insuranceRequest.setQuotedTotalPremiumAmount(reservationInsurance.getQuotedAmount());
					}
				}
			}
		}

	}

	/**
	 * Method will be called only when paying for onhold ress.
	 * 
	 * @param reservation
	 * @return
	 */
	private List<InsuranceRequestAssembler> deriveInsuranceRequestsForOnhold(Reservation reservation) {
		log.debug("inside  CreateInsurance populateRequestInfo");
		List<InsuranceRequestAssembler> listIRA = null;
		// the request obj to populate
		InsuranceRequestAssembler ira = null;
		// get the hold insurance stored in db
		List<ReservationInsurance> listResIns = reservation.getReservationInsurance();
		// contact info should be there for the reservation
		ReservationContactInfo resContactInfo = reservation.getContactInfo();
		// reservation passengers
		Set<ReservationPax> resPaxs = reservation.getPassengers();
		try {
			if (listResIns != null && !listResIns.isEmpty() && resContactInfo != null && resPaxs != null) {
				listIRA = new ArrayList<InsuranceRequestAssembler>();
				Iterator<ReservationInsurance> itInsAssember = listResIns.iterator();
				while (itInsAssember.hasNext()) {
					ReservationInsurance resIns = itInsAssember.next();
					// if the below are available only proceed, otherwise request obj will be null and aig sell will be
					// existed

					String phoneNumber = "";
					if (resContactInfo.getMobileNo() != null && !resContactInfo.getMobileNo().equals("")
							&& !resContactInfo.getMobileNo().equals("--")) {
						phoneNumber = resContactInfo.getMobileNo();
					} else {
						phoneNumber = resContactInfo.getPhoneNo();
					}
					InsureSegment insureSegment = new InsureSegment();
					ira = new InsuranceRequestAssembler();
					// will override the constructor msgid.
					ira.setInsuranceId(resIns.getInsuranceId());
					ira.setMessageId(resIns.getMessageId());
					ira.setPnr(reservation.getPnr());
					ira.setTotalPremiumAmount(resIns.getAmount());
					ira.setQuotedTotalPremiumAmount(resIns.getQuotedAmount());
					ira.setAutoCancellationId(resIns.getAutoCancellationId());
					ira.setSsrFeeCode(resIns.getSsrFeeCode());
					ira.setPlanCode(resIns.getPlanCode());
					insureSegment.setArrivalDate(resIns.getDateOfReturn());
					insureSegment.setDepartureDate(resIns.getDateOfTravel());
					insureSegment.setDepartureDateOffset(resIns.getStartDateOffset());
					insureSegment.setArrivalDateOffset(resIns.getEndDateOffset());
					insureSegment.setFromAirportCode(resIns.getOrigin());
					insureSegment.setToAirportCode(resIns.getDestination());
					insureSegment.setRoundTrip("Y".equals(resIns.getRoundTrip()) ? true : false);
					insureSegment.setSalesChanelCode(resIns.getSalesChannel());

					ira.addFlightSegment(insureSegment);
					for (ReservationPax resPax : resPaxs) {
						if (!resPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
							InsurePassenger insurePassenger = new InsurePassenger();
							insurePassenger.setTitle(resPax.getTitle());
							insurePassenger.setFirstName(resPax.getFirstName());
							insurePassenger.setLastName(resPax.getLastName());
							insurePassenger.setAddressLn1(resContactInfo.getStreetAddress1());
							insurePassenger.setAddressLn2(resContactInfo.getStreetAddress2());
							insurePassenger.setCity(resContactInfo.getCity());
							insurePassenger.setCountryOfAddress(resContactInfo.getCountryCode());
							insurePassenger.setDateOfBirth(resPax.getDateOfBirth());
							if (resPax.getNationalityCode() != null) {
								insurePassenger.setNationality(String.valueOf(resPax.getNationalityCode()));
							}
							insurePassenger.setEmail(resContactInfo.getEmail());
							insurePassenger.setHomePhoneNumber(phoneNumber);
							insurePassenger.setInfant(resPax.getParent() == null ? false : true);
							ira.addPassenger(insurePassenger);
						}
					}

					if (reservation.getSegmentsView() != null) {
						ira.setFlightSegments(InsuranceHelper.createFlightSegList(reservation));
						for (ReservationSegmentDTO segment : reservation.getSegmentsView()) {
							if (segment.getFlightType() != null
									&& segment.getFlightType().equals(FlightType.INTERNATIONL.getType())) {
								ira.setAllDomastic(false);
								break;
							}
						}
					}
					listIRA.add(ira);

				}

			}
		} catch (Exception e) {
			log.error("Error when trying to poplulate insurance request information", e);
		}

		log.debug("exiting  CreateInsurance populateRequestInfo");
		return listIRA;
	}

	/**
	 * Handle ME
	 */
	private void composeFailuerResponses(ModuleException meException, List<InsuranceRequestAssembler> insuranceRequests,
			List<InsuranceResponse> insuranceResponses) throws ModuleException {

		InsuranceResponse response = null;

		for (InsuranceRequestAssembler insuranceRequest : insuranceRequests) {
			// if (isNotEmpty(insuranceResponses)) {
			//
			// response.setAdditionalErrorMessage(meException.getExceptionCode() + "|" +
			// meException.getMessageString());
			//
			// } else {
			// response = new InsuranceResponse(false, meException.getExceptionCode(), meException.getMessageString(),
			// insuranceRequests.get(0).getMessageId(), AccelAeroCalculator.getDefaultBigDecimalZero(),
			// AccelAeroCalculator.getDefaultBigDecimalZero(), "AED"); 
			// }
			// response.setInsuranceRefId(insuranceRequest.getInsuranceId());
			// insuranceResponses.add(response);

		}

	}

	@Deprecated
	/**
	 *TODO shift - 
	 *
	 */
	private List<InsuranceRequestAssembler> addExtraDetailsForTune(List<InsuranceRequestAssembler> listInsuranceReq,
			Reservation reservation) {
		List<InsuranceRequestAssembler> returnlistInsuranceReq = new ArrayList<InsuranceRequestAssembler>();
		if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
			Iterator<InsuranceRequestAssembler> it = listInsuranceReq.iterator();
			while (it.hasNext()) {
				InsuranceRequestAssembler insuranceReq = it.next();
				insuranceReq.setPrefLanguage(reservation.getContactInfo().getPreferredLanguage().toUpperCase());
				insuranceReq.setCurrency(reservation.getLastCurrencyCode());

				ReservationContactInfo resContact = reservation.getContactInfo();

				if (insuranceReq.getLccInsuranceContactInfo() == null && resContact != null) {
					LCCInsuredContactInfoDTO lccInsuranceContactInfo = new LCCInsuredContactInfoDTO();
					lccInsuranceContactInfo.setContactPerson(resContact.getFirstName() + " " + resContact.getLastName());
					lccInsuranceContactInfo.setAddress1(resContact.getStreetAddress1());
					lccInsuranceContactInfo.setAddress2(resContact.getStreetAddress2());
					lccInsuranceContactInfo.setHomePhoneNum(resContact.getPhoneNo());
					lccInsuranceContactInfo.setMobilePhoneNum(resContact.getMobileNo());
					lccInsuranceContactInfo.setCity(resContact.getCity());
					lccInsuranceContactInfo.setCountry(resContact.getCountryCode());
					lccInsuranceContactInfo.setEmailAddress(resContact.getEmail());
					lccInsuranceContactInfo.setPrefLanguage(resContact.getPreferredLanguage());
					insuranceReq.setLccInsuranceContactInfo(lccInsuranceContactInfo);
				}
				// Should be verified
				for (ReservationSegmentDTO flightsegment : reservation.getSegmentsView()) {
					if (!"Y".equalsIgnoreCase(flightsegment.getReturnFlag())) {
						insuranceReq.getInsureSegment().setDepartureFlightNo(flightsegment.getFlightNo());
					} else {
						insuranceReq.getInsureSegment().setArrivalFlightNo(flightsegment.getFlightNo());
					}

				}
				returnlistInsuranceReq.add(insuranceReq);
			}
		}
		return returnlistInsuranceReq;
	}

	private void validateResponses(List<InsuranceRequestAssembler> iRequestAssemblers, List<InsuranceResponse> iResponses)
			throws ModuleException {

		for (InsuranceResponse iResponse : iResponses) {
			if (iResponse.isSuccess()) {

				if (iRequestAssemblers.size() != iResponses.size()) {
					log.error("Number of Insurace quotes and sell requests does not match");
					throw new ModuleException("airreservation.insurance.req.and.response.inconsitent");
				}

				for (InsuranceRequestAssembler iRequestAssembler : iRequestAssemblers) {

					// if (iResponse.getReservationInsurance() != null
					// &&
					// iRequestAssembler.getInsuranceId().equals(iResponse.getReservationInsurance().getInsuranceId()))
					// {
					//
					// validateMessageId(iRequestAssembler.getMessageId(), iResponse.getMessageId());
					//
					// // check if sent amount is equal to the received amount
					// boolean matching = false;
					// // from aig the total premium is a rounded value.
					// BigDecimal resQuotedTotalPremiumAmt = iResponse.getQuotedTotalPremiumAmount();
					// BigDecimal reqQuotedTotalPremiumAmt = iRequestAssembler.getQuotedTotalPremiumAmount();
					//
					// if (Math.floor(reqQuotedTotalPremiumAmt.doubleValue()) == Math.round(resQuotedTotalPremiumAmt
					// .doubleValue())) {
					// matching = true;
					// } else if (Math.ceil(reqQuotedTotalPremiumAmt.doubleValue()) ==
					// Math.round(resQuotedTotalPremiumAmt
					// .doubleValue())) {
					// matching = true;
					// }
					//
					// if (!matching) {
					// log.error("message insurance quoted amounts does not match");
					// throw new ModuleException("airreservation.insurance.amt.error");
					// }
					//
					// }
					//
					// }

					// Note - base currency premium amount is not matched; base currency amount for Insurance is per
					// the exchange rate at the time of Insurance Quote
				}
			}
			log.debug("Insurance Response Validated");
		}
	}

	private void validateMessageId(String requestMsgId, String responseMsgId) throws ModuleException {
		if (requestMsgId != null && requestMsgId.length() > 0 && responseMsgId != null && responseMsgId.length() > 0) {
			if (!requestMsgId.equalsIgnoreCase(responseMsgId)) {
				log.error("message ids does not match");
				throw new ModuleException("airreservation.insurance.msgid.error");
			}
		}
	}

	/**
	 * Record the Insurance to Reservation
	 * 
	 * @param insuraneResponse
	 * @param res
	 * @param credDTO
	 */
	private void recordReservationInsurances(List<InsuranceRequestAssembler> iRequestAssemblers,
			List<InsuranceResponse> insuraneResponses, Reservation res, CredentialsDTO credDTO) throws ModuleException {

		List<ReservationInsurance> resInsurancesToUpdate = new ArrayList<ReservationInsurance>();

		for (InsuranceRequestAssembler iRequestAssembler : iRequestAssemblers) {

			List<ReservationInsurance> resInsurances = res.getReservationInsurance();
			Iterator<ReservationInsurance> resInsuranceIt = resInsurances.iterator();

			if (resInsurances != null && !resInsurances.isEmpty()) {
				while (resInsuranceIt.hasNext()) {

					ReservationInsurance resIns = resInsuranceIt.next();

					if (resIns.getInsuranceId().equals(iRequestAssembler.getInsuranceId())) {

						String[] resultCodes = getResultCodes(insuraneResponses, iRequestAssembler.getInsuranceId());

						String state = resultCodes[0];
						String policyCode = resultCodes[1];
						String errorDescription = resultCodes[2];

						// if state == null that means an onhold res with insurance
						if (state == null) {
							resIns.setState(ReservationInternalConstants.INSURANCE_STATES.OH.toString());
						} else {
							resIns.setState(state);
							resIns.setSellTime(new Date());
						}

						resIns.setDescription(errorDescription);
						resIns.setPolicyCode(policyCode);

						if (credDTO != null) {
							if (SalesChannelsUtil.isSalesChannelNOTWebOrMobile(credDTO.getSalesChannelCode())
									&& resIns.getUserId() == null) {
								resIns.setUserId(credDTO.getUserId());
							}
						}
						/** setting the linkage between reservation and insurance */
						resIns.setPnr(res.getPnr());
						resIns.setInsuranceType(iRequestAssembler.getInsuranceType());

						if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
							resIns.setInsuranceId(iRequestAssembler.getInsuranceId());
							resIns.setSsrFeeCode(iRequestAssembler.getSsrFeeCode());
							resIns.setPlanCode(iRequestAssembler.getPlanCode());
							resIns.setAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
							resIns.setQuotedAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
							resIns.setQuotedNetAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
							resIns.setNetAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
						}

						if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.CCC)) {
							resIns.setAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
							resIns.setNetAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
							resIns.setQuotedAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
							resIns.setQuotedNetAmount(iRequestAssembler.getQuotedTotalPremiumAmount());
						}

						// Add auto cancellation information
						resIns.setAutoCancellationId(iRequestAssembler.getAutoCancellationId());
						resInsurancesToUpdate.add(resIns);
					}
				}
			} else {
				log.error("Recording insurance info failed. ReservationInsurance not found in DB");
			}

		}

		ReservationModuleUtils.getReservationAuxilliaryBD().saveSellInfo(resInsurancesToUpdate);
		/** Commented by Lalanthi, as we are no longer saving insurance Id to the reservation */
		// res.setInsuranceId(resIns.getInsuranceId());
		// Haider 19Apr09 save the reservation
		ReservationProxy.saveReservation(res);

		log.debug("recorded insurance info to reservation");

	}

	private String[] getResultCodes(List<InsuranceResponse> insuraneResponses, Integer insuranceId) {

		String[] results = new String[3];

		String state = null;
		String policyCode = null;
		String errorDescription = null;

		// Onhold reservations will be having empty responses
		if (isNotEmpty(insuraneResponses)) {
			for (InsuranceResponse insuraneResponse : insuraneResponses) {
				if (insuraneResponse.getInsuranceRefId().equals(insuranceId)) {
					errorDescription = insuraneResponse.getErrorCode() + "|" + insuraneResponse.getErrorMessage();
					if (errorDescription != null && errorDescription.length() > 100) {
						errorDescription = errorDescription.substring(0, 98);
					}
					policyCode = insuraneResponse.getPolicyCode();
					state = decideState(insuraneResponse.isSuccess(), insuraneResponse.getErrorCode());
					log.debug("Insuarance Recieved: Policy" + policyCode);
					break;
				}
			}
		}
		results[0] = state;
		results[1] = policyCode;
		results[2] = errorDescription;

		return results;
	}

	private <T> boolean isNotEmpty(Collection<T> params) {
		return params != null && !params.isEmpty();
	}

	/**
	 * Decide the state
	 * 
	 * @param responseSuccess
	 * @param errorCode
	 * @return
	 */
	private String decideState(boolean responseSuccess, String errorCode) {

		String state = null;

		// success state
		if (responseSuccess && "0".equals(errorCode)) {
			state = ReservationInternalConstants.INSURANCE_STATES.SC.toString();
		} else if (!responseSuccess && "wsclient.httpclient.timeout".equals(errorCode)) {
			state = ReservationInternalConstants.INSURANCE_STATES.TO.toString();
		} else if (!responseSuccess || !"0".equals(errorCode)) {
			state = ReservationInternalConstants.INSURANCE_STATES.FL.toString();
		} else {
			state = ReservationInternalConstants.INSURANCE_STATES.RQ.toString();
		}

		return state;
	}

}
