/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.SpaceElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.PNLADLMessageConstants;

/**
 * @author udithad
 *
 */
public abstract class BaseElementBuilder {

	protected BaseElementBuilder loopBackBuilder;
	protected BaseElementBuilder nextElementBuilder;
	protected BaseElementBuilder concatenationElementBuilder;

	public abstract void buildElement(ElementContext context);

	public void setNextElementBuilder(BaseElementBuilder nextElementBuilder) {
		this.nextElementBuilder = nextElementBuilder;
	}

	public void setLoopBackBuilder(BaseElementBuilder loopBackBuilder) {
		this.loopBackBuilder = loopBackBuilder;
	}

	public void setConcatenationELementBuilder(
			BaseElementBuilder concatenationElementBuilder) {
		this.concatenationElementBuilder = concatenationElementBuilder;
	}

	protected void ammendToBaseLine(String currentElement,
			StringBuilder currentLine, StringBuilder messageLine) {
		currentLine.append(currentElement.toUpperCase());
		messageLine.append(currentElement.toUpperCase());
	}

	protected void executeConcatenationElementBuilder(ElementContext heContext) {
		if (concatenationElementBuilder != null) {
			concatenationElementBuilder.buildElement(heContext);
		}
	}

	protected void executeSpaceElementBuilder(ElementContext context) {
		SpaceElementBuilder spaceElementBuilder = new SpaceElementBuilder();
		spaceElementBuilder.buildElement(context);
	}

	protected void ammendmentPreValidation(boolean isStartWithNewLine,
			String currentElement, ElementContext uPContext,
			BaseRuleExecutor baseRuleExecutor) {
		RuleResponse response = null;
		if (isStartWithNewLine && currentElement != null
				&& !currentElement.isEmpty()) {
			executeConcatenationElementBuilder(uPContext);
			response = validateSubElement(currentElement,
					uPContext.getCurrentMessageLine(), baseRuleExecutor);
		} else if (!isStartWithNewLine) {
			if (uPContext.getCurrentMessageLine() != null
					&& uPContext.getCurrentMessageLine().length() > 0) {
				response = validateSubElement(" " + currentElement,
						uPContext.getCurrentMessageLine(), baseRuleExecutor);
			}
		}
		if (response != null) {
			if (response.isProceedNextElement()) {
				ammendMessageDataAccordingTo(" " + currentElement,
						uPContext.getCurrentMessageLine(),
						uPContext.getMessageString());
			} else {
				if (!isStartWithNewLine && currentElement != null
						&& !currentElement.isEmpty()) {
					executeConcatenationElementBuilder(uPContext);
					response = validateSubElement(currentElement,
							uPContext.getCurrentMessageLine(), baseRuleExecutor);
				}
				if (response.isProceedNextElement()) {
					ammendMessageDataAccordingTo(currentElement,
							uPContext.getCurrentMessageLine(),
							uPContext.getMessageString());
				} else {
					if (response.getSuggestedElementText() != null
							&& response.getSuggestedElementText().length > 0) {
						ammendSuggestedElements(response, isStartWithNewLine,
								uPContext);
					}
				}

			}
		}

	}

	private void ammendSuggestedElements(RuleResponse response,
			boolean isStartWithNewLine, ElementContext uPContext) {
		for (int i = 0; i < response.getSuggestedElementText().length; i++) {
			if (i == 0) {
				ammendMessageDataAccordingTo(
						response.getSuggestedElementText()[i],
						uPContext.getCurrentMessageLine(),
						uPContext.getMessageString());
			} else {
				executeConcatenationElementBuilder(uPContext);
				ammendMessageDataAccordingTo(
						MessageComposerConstants.PNLADLMessageConstants.REMARKS_CONT
								+ response.getSuggestedElementText()[i],
						uPContext.getCurrentMessageLine(),
						uPContext.getMessageString());
			}
		}
	}

	private void ammendMessageDataAccordingTo(String element,
			StringBuilder currentLine, StringBuilder messageLine) {
		if (element != null && !element.isEmpty()) {
			ammendToBaseLine(element, currentLine, messageLine);
		}
	}

	private RuleResponse validateSubElement(String elementText,
			StringBuilder currentLine,
			BaseRuleExecutor baggageElementRuleExecutor) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = baggageElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	// Move to another class
	protected String forwardSlash() {
		return PNLADLMessageConstants.FWD;
	}

	protected String space() {
		return PNLADLMessageConstants.SP;
	}

	protected String partText() {
		return PNLADLMessageConstants.PART;
	}

	protected String hyphen() {
		return PNLADLMessageConstants.HYPHEN;
	}

	protected String classCodeElementPrefix() {
		return PNLADLMessageConstants.RBD;
	}

	protected String pnrElementPrefix() {
		return PNLADLMessageConstants.DOT + PNLADLMessageConstants.L
				+ PNLADLMessageConstants.FWD;
	}
	
	protected String seatConfigPrefix() {
		return PNLADLMessageConstants.CFG;
	}

	protected boolean isRemainingPassengersIn(
			PassengerCollection passengerCollection) {

		boolean isAvailablePassengers = false;
		for (Map.Entry<String, List<DestinationFare>> entry : passengerCollection
				.getPnlAdlDestinationMap().entrySet()) {
			for (DestinationFare destinationFare : entry.getValue()) {
				if (destinationFare.getAddPassengers() != null
						&& destinationFare.getAddPassengers().size() > 0) {
					isAvailablePassengers = true;
				}
				if (destinationFare.getDeletePassengers() != null
						&& destinationFare.getDeletePassengers().size() > 0) {
					isAvailablePassengers = true;
				}
				if (destinationFare.getChangePassengers() != null
						&& destinationFare.getChangePassengers().size() > 0) {
					isAvailablePassengers = true;
				}
			}
		}
		return isAvailablePassengers;
	}

}
