package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;


import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleSearchTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.BlacklistPAXRule;
import com.isa.thinair.airreservation.api.model.BlacklistReservation;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;


public interface BlacklistPAXDAO {
	
	/**
	 * Saves the Blacklisted PAX given.
	 * 
	 * @param blacklistPAXCriteriaTO
	 *            Subclass of {@link BlacklistPAX} to be saved.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public void saveBlacklistPAX(BlacklistPAX blacklistPAX);
	
	/**
	 * Searches across Blacklist PAX criteria.
	 * 
	 * @param blacklistPAXCriteriaSearchTO
	 *            Search data DTO with the necessary data set.
	 * @param start
	 *            Starting position of the query results.
	 * @param size
	 *            Size of the query result set.
	 * @return a {@link Page} with the search results enclosed.
	 */
	public Page<BlacklistPAX>
			searchBlacklistPAX(BlacklistPAXCriteriaSearchTO blacklistPAXCriteriaSearchTO, Integer start, Integer size);
	
	/**
	 * Searches across Blacklist PAX Id.
	 * 
	 * @param blacklistPAXCriteriaID
	 *            Search data DTO with the necessary data set.
	 * @return a {@link Page} with the search results enclosed.
	 */
	public BlacklistPAX
			searchBlacklistPAXByBlacklistPAXId(Long blacklistPAXCriteriaID);
	
	
	/**
	 * Updates the blacklist PAX.
	 * 
	 * @param blacklistPAX
	 *            blacklistPAX with the changed data populated.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public long updateBlacklistPAX(BlacklistPAX blacklistPAX);
	
	public Page<BlacklisPaxReservationTO> searchBlacklistPaxResTO(BlacklisPaxReservationTO blPaxResTOSearch, Integer start, Integer size) throws ModuleException;
	
	public void updateBlacklistReservation(BlacklistReservation blacklistResToSave) throws ModuleException;
	
	public BlacklistReservation searchBlacklistReservationById(Long blacklistResId) throws ModuleException;
	
	/**
	 * Searches across Blacklist PAX rules criteria.
	 * 
	 * @param blacklistPAXRuleSearchTO
	 *            Search data DTO with the necessary data set.
	 * @param start
	 *            Starting position of the query results.
	 * @param size
	 *            Size of the query result set.
	 * @return a {@link Page} with the search results enclosed.
	 */
	public Page<BlacklistPAXRule> searchBlacklistPAXRules(BlacklistPAXRuleSearchTO blacklistPAXRuleSearchTO, Integer start,
			Integer size);

	/**
	 * Saves the Blacklist PAX Rule given.
	 * 
	 * @param blacklistPAXRule
	 *            Subclass of {@link BlacklistPAXRule} to be saved.
	 */
	public void saveBlacklistPAXRule(BlacklistPAXRule blacklistPAXRule);

	/**
	 * Fetch all the blacklisting rules available
	 */
	public List<BlacklistPAXRule> getBlacklistPAXRules();
	
	public void saveBlacklistedPnrPassengerList(List<BlacklistReservation> blackListPnrPassengerList) throws ModuleException;
	
	/**
	 * Fetch active blacklisting rules available
	 */
	public List<BlacklistPAXRule> getActiveBlacklistPAXRules();
	
	/**
	 * Fetch blacklisted PAXs for today.
	 */
	public List<BlacklistPAX> getBlacklistedPaxForToday();
	
	public List<BlacklistPAX> getBlacklistedPaXByRule(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO, boolean nameChecked, boolean dobChecked, boolean nationalityChecked, boolean passportChecked);
	
	public Collection<Integer> getBLackListPaxIdVsPnrPaxId(String pnr) throws ModuleException;
	
	public boolean isBlackListPaxAlreadyInReservations(Integer blacklistedPaxId)  throws ModuleException;
	
	public void deleteBlacklistPAX(BlacklistPAX blacklistPAX) throws ModuleException;
}
