package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.util.OnholdRollForwardUtil;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to prepare Selected Flights for OHD Roll Forward
 * 
 * @author rumesh
 * @isa.module.command name="rollForwardOnholdBooking"
 */
public class RollForwardOnholdBooking extends DefaultBaseCommand {

	private AvailableFlightSearchDTO availableFlightSearchDTO = null;
	private final List<ReservationSegmentDTO> segmentFareList = new ArrayList<ReservationSegmentDTO>();
	private final Map<Integer, List<ReservationSegmentDTO>> connectionFareMap = new HashMap<Integer, List<ReservationSegmentDTO>>();
	private final Map<Integer, List<ReservationSegmentDTO>> returnFareMap = new HashMap<Integer, List<ReservationSegmentDTO>>();

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {

		String sourcePnr = (String) this.getParameter(CommandParamNames.PNR);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		String userId = (String) this.getParameter(CommandParamNames.USER_ID);
		Boolean forcedConfirm = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		Boolean capturePayments = (Boolean) this.getParameter(CommandParamNames.IS_CAPTURE_PAYMENT);
		availableFlightSearchDTO = (AvailableFlightSearchDTO) this.getParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO);
		Boolean isDuplicateNameSkip = (Boolean) this.getParameter(CommandParamNames.IS_DUPLICATE_NAME_SKIP);
		List<ReservationSegmentDTO> reservationSegmentDTOs = (List<ReservationSegmentDTO>) this
				.getParameter(CommandParamNames.RES_FLIGHT_SEGMENT_DTO);
		Map<String, RollForwardFlightRQ> rollForwardingFlights = (Map<String, RollForwardFlightRQ>) this
				.getParameter(CommandParamNames.ROLL_FORWARD_FLIGHT_LIST);

		this.validateParams(sourcePnr, credentialsDTO, userId, forcedConfirm, capturePayments, availableFlightSearchDTO,
				reservationSegmentDTOs, rollForwardingFlights);

		// Load source reservation
		Reservation sourceReservation = loadReservation(sourcePnr);

		OnholdRollForwardUtil.populateFlightSegmentOnFare(reservationSegmentDTOs, segmentFareList, connectionFareMap,
				returnFareMap);

		List<String> generatedPnrs = new ArrayList<String>();

		for (Entry<String, RollForwardFlightRQ> entry : rollForwardingFlights.entrySet()) {
			RollForwardFlightRQ rollForwardFlightRQ = entry.getValue();
			List<RollForwardFlightDTO> rollForwardFlightDTOs = rollForwardFlightRQ.getRollForwardFlights();
			Collections.sort(rollForwardFlightDTOs);
			List<SelectedFlightDTO> selectedFlightDTOs = getSelectedFlightDTOList(rollForwardFlightDTOs);
			if (selectedFlightDTOs != null && selectedFlightDTOs.size() > 0) {
				Collection<OndFareDTO> ondFareDTOs = OnholdRollForwardUtil.getOndFareDTOs(selectedFlightDTOs);
				
				if (ondFareDTOs == null || ondFareDTOs.isEmpty()) {
					throw new ModuleException("airinventory.logic.bl.fares.not.available");
				}
				
				OnholdRollForwardUtil.overrideBookingType(ondFareDTOs, availableFlightSearchDTO.getBookingType());
				IReservation iReservation = OnholdRollForwardUtil.getReservationAssembler(ondFareDTOs, sourceReservation,
						rollForwardFlightDTOs, rollForwardFlightRQ.getOnholdReleaseTime(), isDuplicateNameSkip);

				Collection<TempSegBcAlloc> blockKeyId = ReservationModuleUtils.getReservationBD().blockSeats(ondFareDTOs, null);

				// Call create onhold reservation
				ServiceResponce ohdCreateResponse = ReservationModuleUtils.getReservationBD().createOnHoldReservation(
						iReservation, blockKeyId, forcedConfirm, credentialsDTO.getTrackInfoDTO(), capturePayments);
				if (ohdCreateResponse == null) {
					throw new ModuleException("airreservations.ohd.rollfoward.fail");
				} else {
					String generatedPNR = (String) ohdCreateResponse.getResponseParam(CommandParamNames.ON_HOLD_PNR);
					generatedPnrs.add(generatedPNR);
					if (generatedPNR != null) {
						Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = ReservationCoreUtils
								.getSegmentSeatDistsDTOs(ondFareDTOs);
						for (SegmentSeatDistsDTO segmentSeatDistsDTO : segmentSeatDistsDTOs) {
							Collection<SeatDistribution> seatDistributions = segmentSeatDistsDTO.getSeatDistribution();
							for (SeatDistribution seatDist : seatDistributions) {
								if (seatDist.isOverbook() && !seatDist.isSameBookingClassAsExisting()
										&& !seatDist.isBookedFlightCabinBeingSearched()
										&& !segmentSeatDistsDTO.isFixedQuotaSeats()) {
									Collection<String> strFltSegIds = new ArrayList<String>();
									strFltSegIds.add(String.valueOf(segmentSeatDistsDTO.getFlightSegId()));
									ReservationModuleUtils.getFlightInventoryBD().doAuditOverBookings(strFltSegIds,
											segmentSeatDistsDTO.getBookingCode(), null, generatedPNR,
											String.valueOf(segmentSeatDistsDTO.getFlightId()), userId, seatDist.getNoOfSeats());
								}
							}
						}
					}
				}

			}

		}

		DefaultServiceResponse output = new DefaultServiceResponse(true);
		output.addResponceParam(CommandParamNames.PNR_LIST, generatedPnrs);

		// constructing and return command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.PAX_CREDIT_PAYMENTS, null);
		response.addResponceParam(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

		if (generatedPnrs != null && generatedPnrs.size() > 0) {
			ReservationAudit reservationAudit = this.composeReservationAudit(sourcePnr, generatedPnrs,
					credentialsDTO.getTrackInfoDTO());
			Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
			colReservationAudit.add(reservationAudit);
			response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		}

		return response;
	}

	/**
	 * Validating Required Parameters
	 * 
	 * @param pnr
	 * @param credentialsDTO
	 * @param userId
	 * @param forcedConfirm
	 * @param capturePayments
	 * @param availableFlightSearchDTO
	 * @param reservationSegmentDTOs
	 * @param rollForwardingFlights
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, CredentialsDTO credentialsDTO, String userId, Boolean forcedConfirm,
			Boolean capturePayments, AvailableFlightSearchDTO availableFlightSearchDTO,
			List<ReservationSegmentDTO> reservationSegmentDTOs, Map<String, RollForwardFlightRQ> rollForwardingFlights)
			throws ModuleException {
		if (pnr == null || credentialsDTO == null || userId == null || forcedConfirm == null || capturePayments == null
				|| availableFlightSearchDTO == null || reservationSegmentDTOs == null || rollForwardingFlights == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}

	private ReservationAudit composeReservationAudit(String sourcePnr, List<String> generatedPnrs, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(sourcePnr);
		reservationAudit.setModificationType(AuditTemplateEnum.OHD_ROLL_FORWARD.getCode());
		reservationAudit.setUserNote(null);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OHDRollForward.SOURCE_PNR, sourcePnr);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OHDRollForward.NEW_PNRS, generatedPnrs.toString());

		// Setting the ip address
		if (trackInfoDTO != null && trackInfoDTO.getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedContactDetails.IP_ADDDRESS,
					trackInfoDTO.getIpAddress());
		}

		// Setting the origin carrier code
		if (trackInfoDTO != null && trackInfoDTO.getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedContactDetails.ORIGIN_CARRIER,
					trackInfoDTO.getCarrierCode());
		}

		return reservationAudit;
	}

	private List<SelectedFlightDTO> getSelectedFlightDTOList(List<RollForwardFlightDTO> rollForwardFlightDTOs)
			throws ModuleException {
		List<SelectedFlightDTO> selectedFlightDTOs = new ArrayList<SelectedFlightDTO>();

		// Get SelectedFlightDTO for Flight segments with segment fare
		if (segmentFareList != null && segmentFareList.size() > 0) {
			SelectedFlightDTO selectedFlightDTO = getSelectedFlightDTO(segmentFareList, rollForwardFlightDTOs,
					AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS, false);
			if (selectedFlightDTO != null) {
				selectedFlightDTOs.add(selectedFlightDTO);
			}
		}

		// Get SelectedFlightDTOs for Flight segments with connection fare
		if (connectionFareMap != null && !connectionFareMap.isEmpty()) {
			for (Entry<Integer, List<ReservationSegmentDTO>> entry : connectionFareMap.entrySet()) {
				SelectedFlightDTO selectedFlightDTO = getSelectedFlightDTO(entry.getValue(), rollForwardFlightDTOs,
						AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS, true);
				if (selectedFlightDTO != null) {
					selectedFlightDTOs.add(selectedFlightDTO);
				}
			}
		}

		// Get SelectedFlightDTOs for Flight segments with return fare
		if (returnFareMap != null && !returnFareMap.isEmpty()) {
			for (Entry<Integer, List<ReservationSegmentDTO>> entry : returnFareMap.entrySet()) {
				SelectedFlightDTO selectedFlightDTO = getSelectedFlightDTO(entry.getValue(), rollForwardFlightDTOs,
						AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS, false);
				if (selectedFlightDTO != null) {
					selectedFlightDTOs.add(selectedFlightDTO);
				}
			}
		}

		return selectedFlightDTOs;
	}

	private SelectedFlightDTO getSelectedFlightDTO(List<ReservationSegmentDTO> reservationSegmentDTOs,
			List<RollForwardFlightDTO> rollForwardFlightDTOs, int availabilityRestriction, boolean isConnection) throws ModuleException {

		List<RollForwardFlightDTO> filteredFlights = OnholdRollForwardUtil.FilterMatchingRollForwardingFlights(
				rollForwardFlightDTOs, reservationSegmentDTOs);

		List<LinkedHashMap<Integer, RollForwardFlightDTO>> inboundOutboundList = OnholdRollForwardUtil
				.getOutboundInboundFlights(filteredFlights);

		OnholdRollForwardUtil.populateAvailableFlightSearchDTO(inboundOutboundList, availabilityRestriction,
				availableFlightSearchDTO);
		
		List<List<Integer>> flightSegmentIdsList = new ArrayList<List<Integer>>();
		for (RollForwardFlightDTO rollForwardFlightDTO : filteredFlights) {
			if (isConnection) {
				if (rollForwardFlightDTO.getReturnFlag().equals("N")) {
					if (flightSegmentIdsList.isEmpty()) {
						List<Integer> fltSegIds = new ArrayList<Integer>();
						fltSegIds.add(rollForwardFlightDTO.getFlightSegId());
						flightSegmentIdsList.add(fltSegIds);
					} else {
						flightSegmentIdsList.get(0).add(rollForwardFlightDTO.getFlightSegId());
					}
				}
			} else {
				List<Integer> fltSegIds = new ArrayList<Integer>();
				fltSegIds.add(rollForwardFlightDTO.getFlightSegId());
				flightSegmentIdsList.add(fltSegIds);
			}
		}

		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();
		int i = 0;
		for (List<Integer> flightSegmentIds : flightSegmentIdsList) {
			FlightUtil.updateAvailableFlightSegments(flightSegmentIds, availableFlightSearchDTO.getOndInfo(0)
					.getPreferredClassOfService(), selectedFlightDTO, i, false, false, false, null);
			i++;
		}
		for (OriginDestinationInfoDTO ondInfoDTO : availableFlightSearchDTO.getOrderedOriginDestinations()) {
			if (ondInfoDTO.isOpenOnd()) {
				selectedFlightDTO.setOpenReturn(true);
			}
		}
		selectedFlightDTO = ReservationModuleUtils.getFlightInventoryResBD().searchSelectedFlightsSeatAvailability(
				selectedFlightDTO, availableFlightSearchDTO, false);

		return selectedFlightDTO;
	}

	private Reservation loadReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

}
