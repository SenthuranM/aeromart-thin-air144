package com.isa.thinair.airreservation.core.bl.revacc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author indika
 * 
 * @isa.module.command name="extendCredits"
 */
public class ExtendCredits extends DefaultBaseCommand {

	PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;

	@Override
	public ServiceResponce execute() throws ModuleException {
		// getting command params
		Long creditId = (Long) this.getParameter(CommandParamNames.CREDIT_ID);
		Date expiryDate = (Date) this.getParameter(CommandParamNames.EXPIRY_DATE);
		String note = (String) this.getParameter(CommandParamNames.NOTE);
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		if (creditId == null || expiryDate == null || note == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		ReservationCredit oReservationCredit = getReservationCredit(creditId);
		oReservationCredit.setExpiryDate(expiryDate);
		oReservationCredit.setNote(note);

		getReservationCreditDAO().saveReservationCredit(oReservationCredit);

		ReservationAudit reservationAudit = this.composeAudit(pnr, pnrPaxId, note, expiryDate, credentialsDTO);
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		return response;
	}

	private ReservationAudit composeAudit(String pnr, String pnrPaxId, String userNotes, Date expDate,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CREDIT_EXTEND_EXPIRY.getCode());
		reservationAudit.setUserNote(userNotes);

		ReservationPax reservationPax = passengerDAO.getPassenger(Integer.parseInt(pnrPaxId), false, false, false);

		// Setting pax Name
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExtendCreditExpiry.PAX_NAME,
				reservationPax.getFirstName());

		// Setting New Expiry date
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ExtendCreditExpiry.PAX_NEW_EXP_DATE,
				CalendarUtil.getDateInFormattedString("dd-MM-yyyy", expDate));

		return reservationAudit;
	}

	/**
	 * method to get reservation Credit by ID
	 */
	private ReservationCredit getReservationCredit(long creditId) {
		return getReservationCreditDAO().getReservationCredit(creditId);
	}

	private ReservationCreditDAO getReservationCreditDAO() {
		return ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
	}
}
