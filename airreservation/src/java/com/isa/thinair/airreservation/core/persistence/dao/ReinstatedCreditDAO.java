/**
 * 
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import com.isa.thinair.airreservation.api.model.ReinstatedCredit;

/**
 * @author indika
 */
public interface ReinstatedCreditDAO {

	/**
	 * Save ReinstateCredit
	 * 
	 * @param ReinstateCredit
	 */
	public void saveReinstateCredit(ReinstatedCredit reinstateCredit);
}
