package com.isa.thinair.airreservation.core.bl.messaging.palcal.dataContext;

import com.isa.thinair.airreservation.api.utils.PalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;

public class CalDataContext  extends BaseDataContext {

	@Override
	public String getMessageType() {
		return PalConstants.MessageTypes.CAL;
	}

}
