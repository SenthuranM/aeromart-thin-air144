/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate.external;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentSageDTO;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.airreservation.core.persistence.dao.CreditSalesTransferDAOImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * X3CreditSalesDAOImpl is the business DAO hibernate implementation
 * 
 * @isa.module.dao-impl dao-name="X3SystemDAO"
 */

public class X3CreditSalesDAOImpl extends CreditSalesTransferDAOImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(X3CreditSalesDAOImpl.class);

	/*
	 * Inserting CreditCard Sales data to external system
	 * 
	 * @Param Collection<CreditCardPaymentSageDTO>
	 * 
	 * @Param AccontingSystemTemplate
	 * 
	 * @throws ClassNotFoundException, SQLException, Exception
	 */
	public void insertCreditCardSalesToExternal(Collection<CreditCardPaymentSageDTO> col, AccontingSystemTemplate template)
			throws ClassNotFoundException, SQLException, Exception {
		log.info("########### insertCreditCardSalesToExternal in X3 #############");
		Connection connection = null;

		try {
			connection = template.getExternalConnection();
			connection.setAutoCommit(false);
			Iterator<CreditCardPaymentSageDTO> it = col.iterator();
			// common sql query
			CreditCardPaymentSageDTO dto = null;
			String sql = "insert into  INT_T_CREDITCARD_SALES values (?,?,?,?,?,?,?)";

			PreparedStatement stmt = connection.prepareStatement(sql);

			String sqlq = "delete from INT_T_CREDITCARD_SALES where SALE_TIMESTAMP=? ";
			PreparedStatement stmtq = connection.prepareStatement(sqlq);
			int i = 0;
			String cardType = null;

			while (it.hasNext()) {
				dto = it.next();
				if (i == 0) {
					stmtq.setDate(1, new java.sql.Date(dto.getTransactionDate().getTime()));
					stmtq.executeUpdate();
				}

				cardType = dto.getCardType();
				stmt.setDate(1, new java.sql.Date(dto.getTransactionDate().getTime()));
				stmt.setBigDecimal(2, dto.getAmount());
				stmt.setBigDecimal(3, dto.getAmountInPaymentCurrency());
				stmt.setString(4, dto.getPaymentCurrency());
				if (cardType != null && cardType.trim().trim().length() > 6) {
					cardType = cardType.substring(0, 6);
				}
				stmt.setString(5, cardType);
				stmt.setString(6, dto.getPaymentGatewayName());
				stmt.setString(7, dto.getPnr());
				if (log.isDebugEnabled()) {
					log.debug(dto.getTransactionDate().getTime() + " | " + dto.getAccountCode() + " | " + dto.getCardType()
							+ " | " + dto.getAuthorizedCode() + " | " + dto.getAmount() + " | " + dto.getCurrency() + " | "
							+ dto.getPnr());
				}
				stmt.executeUpdate();
				i++;
			}

		} catch (Exception exception) {
			log.error("###### Error Inserting credit card Sales to X3 XDB", exception);
			connection.rollback();
			if (exception instanceof SQLException || exception instanceof ClassNotFoundException) {
				throw exception;
			} else {
				throw new CommonsDataAccessException(exception, "transfer.creditcardsales.failed");
			}
		}

		try {
			log.debug("########## Committing Credit Card Sales to X3 XDB");
			connection.commit();

		} catch (Exception e) {
			log.error("###### Error Committing credit card Sales to X3 XDB", e);
			throw new CommonsDataAccessException(e, "transfer.creditcardsales.failed");
		} finally {
			// connection.setAutoCommit(prevAutoCommitStatus);
			connection.close();
		}
		log.info("############ Successfuly Inserted CreditCard Sales To X3 XDB");

	}

	/*
	 * Returns total of transferred CreditCard sales from external system
	 * 
	 * @Param fromDate
	 * 
	 * @Param toDate
	 * 
	 * @Param AccontingSystemTemplate
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getTransferedCreditCardSalesTotal(Date fromDate, Date toDate, AccontingSystemTemplate template) {
		BigDecimal totAmount = new BigDecimal(-1);
		DataSource ds = template.getExternalDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fromDate, toDate };
		String sql = "SELECT SUM(amount) as amt FROM INT_T_CREDITCARD_SALES WHERE SALE_TIMESTAMP BETWEEN ?  and ? ";
		try {
			totAmount = (BigDecimal) templete.query(sql, params, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					BigDecimal amount = null;
					while (rs.next()) {
						amount = rs.getBigDecimal("amt");
					}
					return amount;

				}
			});
		} catch (Exception ex) {
			log.error("External DataBase error:" + ex);
			throw new CommonsDataAccessException(ex, "exdb.getconnection.retrieval.failed", AirreservationConstants.MODULE_NAME);
		}
		return totAmount;

	}

}
