package com.isa.thinair.airreservation.core.bl.tax;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

public class PaymentTransactionsBasedSequencer implements Sequencer {

	private static final int SEC_TO_ROUND = 3;
	private Reservation reservation;
	private Map<Long, Long> payMap = new HashMap<>();
	private Map<Long, Long> chgMap = new HashMap<>();

	private long nextKey = 0;
	private Map<Date, Long> dateKeymap = new HashMap<>();

	private Map<Long, TnxRange> tnxSeqRangeMap = new HashMap<>();

	public PaymentTransactionsBasedSequencer(Reservation reservation) {
		this.reservation = reservation;
		loadTransactions();
	}

	private void loadTransactions() {
		Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapPerPaxWiseOndPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getPerPaxWiseOndChargeIds(reservation.getPnrPaxIds());
		Map<Integer, Long> paxTnxIds = new HashMap<>();
		for (Map<Long, Collection<ReservationPaxOndPayment>> paxOndMap : mapPerPaxWiseOndPayments.values()) {
			for (Collection<ReservationPaxOndPayment> paxChargeWise : paxOndMap.values()) {
				for (ReservationPaxOndPayment ondPaxPayment : paxChargeWise) {
					if (nextKey <= ondPaxPayment.getTransactionSeq()) {
						nextKey = ondPaxPayment.getTransactionSeq() + 1;
					}
					Long tnxSeq = new Long(ondPaxPayment.getTransactionSeq());
					if ((ondPaxPayment.getOriginalPaxOndPaymentId() == null || ondPaxPayment.getOriginalPaxOndPaymentId() <= 0)
							&& ReservationTnxNominalCode.CREDIT_PAYOFF.getCode() != ondPaxPayment.getNominalCode()) {
						payMap.put(ondPaxPayment.getPaymentTnxId(), tnxSeq);
					}
					if (ondPaxPayment.getPaxOndRefundId() != null) {
						payMap.put(new Long(ondPaxPayment.getPaxOndRefundId()), tnxSeq);
					}
					chgMap.put(ondPaxPayment.getPnrPaxOndChgId(), tnxSeq);

					if (!tnxSeqRangeMap.containsKey(tnxSeq)) {
						tnxSeqRangeMap.put(tnxSeq, new TnxRange());
					}

					paxTnxIds.put(Integer.parseInt(ondPaxPayment.getPaymentTnxId().toString()), tnxSeq);
					ondPaxPayment.getPaymentTnxId();
				}
			}
		}

		List<ReservationTnx> tnxs = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getTransactions(paxTnxIds.keySet());

		for (ReservationTnx tnx : tnxs) {
			Long tnxSeq = paxTnxIds.get(tnx.getTnxId());
			tnxSeqRangeMap.get(tnxSeq).addDate(tnx.getDateTime());
		}
	}

	private Long findMatchingSequenceFromDate(Date date) {
		Long retSeq = -1L;
		for (Long seq : tnxSeqRangeMap.keySet()) {
			if (tnxSeqRangeMap.get(seq).isWithinRange(date)) {
				retSeq = seq;
				break;
			}
		}

		return retSeq;
	}

	@Override
	public Long getKey(ReservationPaxOndCharge charge) {
		Long chargeId = new Long(charge.getPnrPaxOndChgId());
		if (!chgMap.containsKey(chargeId)) {
			Long matchingSeq = findMatchingSequenceFromDate(charge.getZuluChargeDate());
			if (matchingSeq != -1) {
				chgMap.put(chargeId, matchingSeq);
			} else {
				Date date = getNormalizedDate(charge.getZuluChargeDate());
				if (!dateKeymap.containsKey(date)) {
					long sequence = nextKey++;
					dateKeymap.put(date, sequence);
				}
				chgMap.put(chargeId, dateKeymap.get(date));
			}
		} else {
			tnxSeqRangeMap.get(chgMap.get(chargeId)).addDate(charge.getZuluChargeDate());
		}
		Long key = chgMap.get(chargeId);

		return key;
	}

	@Override
	public Long getKey(ReservationTnx resTnx) {
		Long tnxId = new Long(resTnx.getTnxId());
		if (!payMap.containsKey(tnxId)) {
			Long matchingSeq = findMatchingSequenceFromDate(resTnx.getDateTime());
			if (matchingSeq != -1) {
				payMap.put(tnxId, matchingSeq);
			} else {
				Date date = getNormalizedDate(resTnx.getDateTime());
				if (!dateKeymap.containsKey(date)) {
					long sequence = nextKey++;
					dateKeymap.put(date, sequence);
				}
				payMap.put(tnxId, dateKeymap.get(date));
			}
		} else {
			tnxSeqRangeMap.get(payMap.get(tnxId)).addDate(resTnx.getDateTime());
		}
		Long key = payMap.get(tnxId);
		return key;
	}

	private Date getNormalizedDate(Date inputDate) {
		BigDecimal epochBD = BigDecimal.valueOf(inputDate.getTime());
		BigDecimal boundry = BigDecimal.valueOf(SEC_TO_ROUND * 1000);
		BigDecimal breakpoint = BigDecimal.valueOf(SEC_TO_ROUND * 1000 - 1);
		BigDecimal rounded = AccelAeroRounderPolicy.getRoundedValue(epochBD, boundry, breakpoint);

		Date newd = new Date(Long.parseLong(rounded.toPlainString()));

		return newd;
	}

	@Override
	public int getMinmumSequence() {
		return 1;
	}

}
