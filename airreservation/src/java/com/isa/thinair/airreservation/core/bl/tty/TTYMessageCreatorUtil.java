package com.isa.thinair.airreservation.core.bl.tty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.gdsservices.api.dto.external.SegmentDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class TTYMessageCreatorUtil {
	
	private static SimpleDateFormat datefmt = new SimpleDateFormat("dd-MMM-yyyy HHmm");
	
	public static int getPaxCount(Reservation reservation, TypeBRequestDTO typeBRequestDTO) {
		int paxCount = 0;
		int totalPaxCount = getTotalPaxCount(reservation);
		if (typeBRequestDTO.getSplitPaxNames() != null && typeBRequestDTO.getSplitPaxNames().size() > 0) {
			paxCount = typeBRequestDTO.getSplitPaxNames().size();
		} else {
			paxCount = totalPaxCount;
		}
		return paxCount;
	}

	public static int getTotalPaxCount(Reservation reservation) {
		int totalPaxCount = 0;
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (reservationPax != null) {
				if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())) {
					totalPaxCount++;
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType())) {
					totalPaxCount++;
				}
			}
		}
		return totalPaxCount;
	}
	
	public static String getPaxGender(String title) {
		if (title.equals(GDSExternalCodes.PassengerTitle.MR.getCode())
				|| title.equals(GDSExternalCodes.PassengerTitle.MASTER.getCode())) {
			return GDSExternalCodes.Gender.MALE.getCode();
		} else if (title.equals(GDSExternalCodes.PassengerTitle.MRS.getCode())
				|| title.equals(GDSExternalCodes.PassengerTitle.MS.getCode())
				|| title.equals(GDSExternalCodes.PassengerTitle.MISS.getCode())) {
			return GDSExternalCodes.Gender.FEMALE.getCode();
		} else {
			return GDSExternalCodes.Gender.UNDISCLOSED_GENDER.getCode();
		}
	}

	/**
	 * gets formated message
	 * 
	 * @param messageCode
	 * @param args
	 * @return
	 */
	public static String getFormattedMessage(String messageCode, Object... args) {
		String strMsg = new ModuleException(messageCode).getMessageString();
		if (strMsg == null) {
			strMsg = "";
		}
	
		if (args != null && args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				strMsg = strMsg.replace(String.valueOf("#" + (i + 1)), formatToString(args[i]));
			}
		}
		return strMsg;
	}

	private static String formatToString(Object object) {
		if (object instanceof Date) {
			return datefmt.format((Date) object);
		} else {
			return object.toString();
		}
	}

	public static RecordLocatorDTO createOriginatorRecordLocator(Reservation reservation) {
		RecordLocatorDTO originatorRecordLocator = new RecordLocatorDTO();
		originatorRecordLocator.setPnrReference(reservation.getExternalRecordLocator());
		String DATA_SEPARATOR = " ";
		if (reservation.getExternalPos() != null && reservation.getExternalPos().contains(DATA_SEPARATOR)) {
			String[] values = reservation.getExternalPos().split(DATA_SEPARATOR);
			originatorRecordLocator.setBookingOffice(values[0]);
			originatorRecordLocator.setPointOfSales(values[1]);
		}
		return originatorRecordLocator;
	}

	public static RecordLocatorDTO createResponderRecordLocator(Reservation reservation) {
		RecordLocatorDTO responderRecordLocator = new RecordLocatorDTO();
		responderRecordLocator.setPnrReference(reservation.getPnr());
		return responderRecordLocator;
	}

	public static RecordLocatorDTO createResponderRecordLocator(String newPnr) {
		RecordLocatorDTO responderRecordLocator = new RecordLocatorDTO();
		responderRecordLocator.setPnrReference(newPnr);
		return responderRecordLocator;
	}
	
	public static List<SSRDocsDTO> composeSsrDocsInfo(Map<Integer, ReservationPaxAdditionalInfo> additionalInfoChangedPaxMap,
			Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException {
		List<SSRDocsDTO> ssrDocsDTOs = new ArrayList<SSRDocsDTO>();
		SSRDocsDTO ssrDocsDTO = null;

		Set<ReservationPax> passengers = reservation.getPassengers();
		for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
			ReservationPax pax = (ReservationPax) itPax.next();
			ReservationPaxAdditionalInfo srcResPaxAdditionalInfo = additionalInfoChangedPaxMap.get(pax.getPnrPaxId());
			if (srcResPaxAdditionalInfo != null) {
				ReservationPaxAdditionalInfo newResPaxAdditionalInfo = pax.getPaxAdditionalInfo();
				if (srcResPaxAdditionalInfo.getPassportNo() == null || srcResPaxAdditionalInfo.getPassportNo().length() > 0
						&& newResPaxAdditionalInfo != null) {
					ssrDocsDTO = new SSRDocsDTO();
					ssrDocsDTO.setDocType("P");
					ssrDocsDTO.setDocNumber(newResPaxAdditionalInfo.getPassportNo());
					ssrDocsDTO.setDocCountryCode(newResPaxAdditionalInfo.getPassportIssuedCntry());
					ssrDocsDTO.setDocExpiryDate(newResPaxAdditionalInfo.getPassportExpiry());
					ssrDocsDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					if (typeBRequestDTO.getCsOCCarrierCode() != null) {
						ssrDocsDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
					} else {
						ssrDocsDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					}
					ssrDocsDTO.setCodeSSR(GDSExternalCodes.SSRCodes.DOCS.getCode());
					ssrDocsDTO.setNoOfPax(1);
					ssrDocsDTO.setPassengerDateOfBirth(pax.getDateOfBirth());
					ssrDocsDTO.setDocFamilyName(pax.getLastName());
					ssrDocsDTO.setDocGivenName(pax.getFirstName() + (pax.getTitle() == null ? "" : pax.getTitle()));
					ssrDocsDTO.setPassengerGender(TTYMessageCreatorUtil.getPaxGender(pax.getTitle()));
					List<NameDTO> pnrNameDTOs = new ArrayList<NameDTO>();
					NameDTO pnrNameDTO = new NameDTO();
					pnrNameDTO.setFirstName(pax.getFirstName());
					pnrNameDTO.setLastName(pax.getLastName());
					pnrNameDTO.setPaxTitle(pax.getTitle());
					pnrNameDTOs.add(pnrNameDTO);
					ssrDocsDTO.setPnrNameDTOs(pnrNameDTOs);
					if (pax.getNationalityCode() != null) {
						Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(pax.getNationalityCode());
						if (nat != null && nat.getIsoCode() != null) {
							ssrDocsDTO.setPassengerNationality(nat.getIsoCode());
						}
					}
					//ssrDocsDTO.setActionOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					// ssrDocsDTO.setFreeText(msgBrokerSSRDocsDTO.getFreeText());
					// ssrDocsDTO.setFullElement(msgBrokerSSRDocsDTO.getFullElement());
					// ssrDocsDTO.setSsrValue(msgBrokerSSRDocsDTO.getSsrValue());
					ssrDocsDTOs.add(ssrDocsDTO);
				}
			}
		}
		return ssrDocsDTOs;
	}
	
	public static List<SSRDocsDTO> composeSsrDocsInfoForAddedInfants(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {
		List<SSRDocsDTO> ssrDocsDTOs = new ArrayList<SSRDocsDTO>();
		SSRDocsDTO ssrDocsDTO = null;

		Set<ReservationPax> passengers = reservation.getPassengers();
		for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
			ReservationPax pax = (ReservationPax) itPax.next();

			if (typeBRequestDTO.getAddInfantPnrPaxIds().contains(pax.getPnrPaxId())) {
				ReservationPaxAdditionalInfo newResPaxAdditionalInfo = pax.getPaxAdditionalInfo();
				if (newResPaxAdditionalInfo != null && newResPaxAdditionalInfo.getPassportNo() != null
						&& newResPaxAdditionalInfo.getPassportNo().length() > 0) {
					ssrDocsDTO = new SSRDocsDTO();
					ssrDocsDTO.setDocType("P");
					ssrDocsDTO.setDocNumber(newResPaxAdditionalInfo.getPassportNo());
					ssrDocsDTO.setDocCountryCode(newResPaxAdditionalInfo.getPassportIssuedCntry());
					ssrDocsDTO.setDocExpiryDate(newResPaxAdditionalInfo.getPassportExpiry());
					ssrDocsDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					if (typeBRequestDTO.getCsOCCarrierCode() != null) {
						ssrDocsDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
					} else {
						ssrDocsDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					}
					ssrDocsDTO.setCodeSSR(GDSExternalCodes.SSRCodes.DOCS.getCode());
					ssrDocsDTO.setNoOfPax(1);
					ssrDocsDTO.setPassengerDateOfBirth(pax.getDateOfBirth());
					ssrDocsDTO.setDocFamilyName(pax.getLastName());
					ssrDocsDTO.setDocGivenName(pax.getFirstName() + (pax.getTitle() == null ? "" : pax.getTitle()));
					ssrDocsDTO.setPassengerGender(TTYMessageCreatorUtil.getPaxGender(pax.getTitle()));
					List<NameDTO> pnrNameDTOs = new ArrayList<NameDTO>();
					NameDTO pnrNameDTO = new NameDTO();
					pnrNameDTO.setFirstName(pax.getFirstName());
					pnrNameDTO.setLastName(pax.getLastName());
					pnrNameDTO.setPaxTitle(pax.getTitle());
					pnrNameDTOs.add(pnrNameDTO);
					ssrDocsDTO.setPnrNameDTOs(pnrNameDTOs);
					if (pax.getNationalityCode() != null) {
						Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(pax.getNationalityCode());
						if (nat != null && nat.getIsoCode() != null) {
							ssrDocsDTO.setPassengerNationality(nat.getIsoCode());
						}
					}
					ssrDocsDTOs.add(ssrDocsDTO);
				}
			}
		}
		return ssrDocsDTOs;
	}
	
	public static List<SSRDocoDTO> composeSsrDocoInfo(Map<Integer, ReservationPaxAdditionalInfo> additionalInfoChangedPaxMap,
			Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException {
		List<SSRDocoDTO> ssrDocoDTOs = new ArrayList<SSRDocoDTO>();
		SSRDocoDTO ssrDocoDTO = null;

		Set<ReservationPax> passengers = reservation.getPassengers();
		for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
			ReservationPax pax = (ReservationPax) itPax.next();
			ReservationPaxAdditionalInfo srcResPaxAdditionalInfo = additionalInfoChangedPaxMap.get(pax.getPnrPaxId());
			if (srcResPaxAdditionalInfo != null) {
				ReservationPaxAdditionalInfo newResPaxAdditionalInfo = pax.getPaxAdditionalInfo();
				if (newResPaxAdditionalInfo != null && newResPaxAdditionalInfo.getVisaDocNumber() != null && 
						newResPaxAdditionalInfo.getVisaDocNumber().length() > 0 ) {
					ssrDocoDTO = new SSRDocoDTO();
					ssrDocoDTO.setTravelDocType("V");
					ssrDocoDTO.setVisaApplicableCountry(newResPaxAdditionalInfo.getVisaApplicableCountry());
					ssrDocoDTO.setVisaDocIssueDate(newResPaxAdditionalInfo.getVisaDocIssueDate());
					ssrDocoDTO.setVisaDocNumber(newResPaxAdditionalInfo.getVisaDocNumber());
					ssrDocoDTO.setVisaDocPlaceOfIssue(newResPaxAdditionalInfo.getVisaDocPlaceOfIssue());
					ssrDocoDTO.setPlaceOfBirth(newResPaxAdditionalInfo.getPlaceOfBirth());
					ssrDocoDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					if(ReservationInternalConstants.PassengerType.INFANT.equals(pax.getPaxType())){
						ssrDocoDTO.setInfant(true);
					}
					if (typeBRequestDTO.getCsOCCarrierCode() != null) {
						ssrDocoDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
					} else {
						ssrDocoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					}
					ssrDocoDTO.setCodeSSR(GDSExternalCodes.SSRCodes.DOCO.getCode());
					ssrDocoDTO.setNoOfPax(1);
			
					List<NameDTO> pnrNameDTOs = new ArrayList<NameDTO>();
					NameDTO pnrNameDTO = new NameDTO();
					pnrNameDTO.setFirstName(pax.getFirstName());
					pnrNameDTO.setLastName(pax.getLastName());
					pnrNameDTO.setPaxTitle(pax.getTitle());
					pnrNameDTOs.add(pnrNameDTO);
					ssrDocoDTO.setPnrNameDTOs(pnrNameDTOs);

					ssrDocoDTOs.add(ssrDocoDTO);
				}
			}
		}
		return ssrDocoDTOs;
	}
	
	public static List<SSRDocoDTO> composeSsrDocoInfoForAddedInfants(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {
		List<SSRDocoDTO> ssrDocoDTOs = new ArrayList<SSRDocoDTO>();
		SSRDocoDTO ssrDocoDTO = null;

		Set<ReservationPax> passengers = reservation.getPassengers();
		for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
			ReservationPax pax = (ReservationPax) itPax.next();

			if (typeBRequestDTO.getAddInfantPnrPaxIds().contains(pax.getPnrPaxId())) {
				ReservationPaxAdditionalInfo newResPaxAdditionalInfo = pax.getPaxAdditionalInfo();
				if (newResPaxAdditionalInfo != null && newResPaxAdditionalInfo.getVisaDocNumber() != null
						&& newResPaxAdditionalInfo.getVisaDocNumber().length() > 0) {
					ssrDocoDTO = new SSRDocoDTO();
					ssrDocoDTO.setTravelDocType("V");
					ssrDocoDTO.setVisaApplicableCountry(newResPaxAdditionalInfo.getVisaApplicableCountry());
					ssrDocoDTO.setVisaDocIssueDate(newResPaxAdditionalInfo.getVisaDocIssueDate());
					ssrDocoDTO.setVisaDocNumber(newResPaxAdditionalInfo.getVisaDocNumber());
					ssrDocoDTO.setVisaDocPlaceOfIssue(newResPaxAdditionalInfo.getVisaDocPlaceOfIssue());
					ssrDocoDTO.setPlaceOfBirth(newResPaxAdditionalInfo.getPlaceOfBirth());
					ssrDocoDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					if(ReservationInternalConstants.PassengerType.INFANT.equals(pax.getPaxType())){
						ssrDocoDTO.setInfant(true);
					}
					if (typeBRequestDTO.getCsOCCarrierCode() != null) {
						ssrDocoDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
					} else {
						ssrDocoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					}
					ssrDocoDTO.setCodeSSR(GDSExternalCodes.SSRCodes.DOCO.getCode());
					ssrDocoDTO.setNoOfPax(1);
					
					List<NameDTO> pnrNameDTOs = new ArrayList<NameDTO>();
					NameDTO pnrNameDTO = new NameDTO();
					pnrNameDTO.setFirstName(pax.getFirstName());
					pnrNameDTO.setLastName(pax.getLastName());
					pnrNameDTO.setPaxTitle(pax.getTitle());
					pnrNameDTOs.add(pnrNameDTO);
					ssrDocoDTO.setPnrNameDTOs(pnrNameDTOs);

					ssrDocoDTOs.add(ssrDocoDTO);
				}
			}
		}
		return ssrDocoDTOs;
	}
	
	public static List<SSRDetailDTO> composeSsrDetails(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {
		List<SSRDetailDTO> ssrDetailsDTOs = new ArrayList<SSRDetailDTO>();
		SSRDetailDTO ssrDetailDTO = null;
		ReservationSegmentDTO reservationSegment;
		Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps = typeBRequestDTO.getPaxSegmentSSRDTOMaps();

		// Map< Seg-Seq , Map < SSR-Code , SSR-DTO > >
		Map<Integer, Map<String, SSRDetailDTO>> ssrMap = new LinkedHashMap<Integer, Map<String, SSRDetailDTO>>();

		if (paxSegmentSSRDTOMaps != null && !paxSegmentSSRDTOMaps.isEmpty()) {
			for (ReservationPax pax : reservation.getPassengers()) {
				if (!ReservationApiUtils.isInfantType(pax)) {
					if (paxSegmentSSRDTOMaps.get(pax.getPaxSequence()) != null) {
						Map<Integer, Collection<PaxSegmentSSRDTO>> ssrByOnDSeq = paxSegmentSSRDTOMaps.get(pax.getPaxSequence());
	
						for (Entry<Integer, Collection<PaxSegmentSSRDTO>> entry : ssrByOnDSeq.entrySet()) {
							Integer segSeq = entry.getKey();
							reservationSegment = null;
	
							for (ReservationSegmentDTO resSegDTO : reservation.getSegmentsView()) {
								if (segSeq.equals(resSegDTO.getSegmentSeq())) {
									reservationSegment = resSegDTO;
								}
							}
	
							Collection<PaxSegmentSSRDTO> paxSegmentSSRDTOs = entry.getValue();
							for (PaxSegmentSSRDTO paxSegmentSsrDTO : paxSegmentSSRDTOs) {
	
								if (!ssrMap.containsKey(segSeq)) {
									ssrMap.put(segSeq, new LinkedHashMap<String, SSRDetailDTO>());
								}
	
								if (!ssrMap.get(segSeq).containsKey(paxSegmentSsrDTO.getSsrCode())) {
									ssrDetailDTO = new SSRDetailDTO();
									ssrDetailDTO.setCodeSSR(paxSegmentSsrDTO.getSsrCode());
	
									SegmentDTO segmentDTO = new SegmentDTO();
									segmentDTO.setDepartureStation(reservationSegment.getOrigin());
									segmentDTO.setDestinationStation(reservationSegment.getDestination());
									segmentDTO.setDepartureDate(reservationSegment.getDepartureDate());
									segmentDTO.setArrivalDate(reservationSegment.getArrivalDate());
									
									String bookingClass = null;
									if (reservationSegment.getFareTO() != null) {
										bookingClass = reservationSegment.getFareTO().getBookingClassCode();
									} else {
										bookingClass = reservationSegment.getCabinClassCode();
									}
									String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(
											typeBRequestDTO.getCsOCCarrierCode(), bookingClass);
									segmentDTO.setBookingCode(TTYMessageUtil.getExternalCsBCForGdsMappedBC(typeBRequestDTO.getCsOCCarrierCode(),
											gdsBookingClass));
									
									if (reservationSegment.getFlightNo() != null && reservationSegment.getFlightNo().length() > 2) {
										segmentDTO.setFlightNumber(reservationSegment.getFlightNo().substring(2));
									}
									ssrDetailDTO.setSegmentDTO(segmentDTO);
									if (typeBRequestDTO.getCsOCCarrierCode() != null) {
										ssrDetailDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
									} else {
										ssrDetailDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
									}
									ssrDetailDTO.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.NEED.getCode());
									if (typeBRequestDTO.getCsOCCarrierCode() == null
											|| (reservationSegment.getCsOcCarrierCode() != null && reservationSegment
													.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode()))) {
										ssrMap.get(segSeq).put(paxSegmentSsrDTO.getSsrCode(), ssrDetailDTO);
										ssrDetailsDTOs.add(ssrDetailDTO);
									}
								}
								if (ssrMap.get(segSeq).containsKey(paxSegmentSsrDTO.getSsrCode())) {
									ssrDetailDTO = ssrMap.get(segSeq).get(paxSegmentSsrDTO.getSsrCode());
									NameDTO nameDTO = new NameDTO();
									nameDTO.setFirstName(pax.getFirstName());
									nameDTO.setLastName(pax.getLastName());
									nameDTO.setPaxTitle(pax.getTitle());
									ssrDetailDTO.getNameDTOs().add(nameDTO);
									ssrDetailDTO.setNoOfPax(ssrDetailDTO.getNoOfPax() + 1);
								}
							}
						}
					}
				}
			}
		}

		return ssrDetailsDTOs;
	}

	public static BookingSegmentDTO setScheduleChanges(BookingSegmentDTO segment, ReservationSegmentDTO reservationSegmentDTO) {

		int dayOffSet =  CalendarUtil.getTimeDifferenceInDays(reservationSegmentDTO.getDepartureDate(),reservationSegmentDTO.getArrivalDate());
		if (dayOffSet < 0) {
			segment.setDayOffSet(Math.abs(dayOffSet));
			segment.setDayOffSetSign("M");
			segment.setChangeInScheduleExist(true);
		} else if(dayOffSet > 0){
			segment.setDayOffSet(dayOffSet);
			segment.setChangeInScheduleExist(true);
		}
		return segment;
	
	}
	
	public static List<SSRDetailDTO> composeSsrDetails(Map<Integer, SegmentSSRAssembler> paxSSRAssemblerMap,
			Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException {
		List<SSRDetailDTO> ssrDetailsDTOs = new ArrayList<SSRDetailDTO>();
		SSRDetailDTO ssrDetailDTO = null;
		Set<ReservationPax> passengers = reservation.getPassengers();
		ReservationSegmentDTO reservationSegment;

		// Map< Seg-Seq , Map < SSR-Code , SSR-DTO > >
		Map<Integer, Map<String, SSRDetailDTO>> ssrMap = new LinkedHashMap<Integer, Map<String, SSRDetailDTO>>();

		for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
			ReservationPax pax = (ReservationPax) itPax.next();
			if (!ReservationApiUtils.isInfantType(pax)) {
				SegmentSSRAssembler ssrAssembler = paxSSRAssemblerMap.get(pax.getPaxSequence());
				if (ssrAssembler != null) {
					if (ssrAssembler.getNewSegmentSSRDTOMap() != null && ssrAssembler.getNewSegmentSSRDTOMap().size() > 0) {
						for (Entry<Integer, Collection<PaxSegmentSSRDTO>> entry : ssrAssembler.getNewSegmentSSRDTOMap()
								.entrySet()) {
							Integer segSeq = entry.getKey();
							reservationSegment = null;

							for (ReservationSegmentDTO resSegDTO : reservation.getSegmentsView()) {
								if (segSeq.equals(resSegDTO.getSegmentSeq())) {
									reservationSegment = resSegDTO;
								}
							}

							Collection<PaxSegmentSSRDTO> paxSegmentSSRDTOs = entry.getValue();
							for (PaxSegmentSSRDTO paxSegmentSsrDTO : paxSegmentSSRDTOs) {

								if (!ssrMap.containsKey(segSeq)) {
									ssrMap.put(segSeq, new LinkedHashMap<String, SSRDetailDTO>());
								}

								if (!ssrMap.get(segSeq).containsKey(paxSegmentSsrDTO.getSsrCode())) {
									ssrDetailDTO = new SSRDetailDTO();
									ssrDetailDTO.setCodeSSR(paxSegmentSsrDTO.getSsrCode());

									SegmentDTO segmentDTO = new SegmentDTO();
									segmentDTO.setDepartureStation(reservationSegment.getOrigin());
									segmentDTO.setDestinationStation(reservationSegment.getDestination());
									segmentDTO.setDepartureDate(reservationSegment.getDepartureDate());
									segmentDTO.setArrivalDate(reservationSegment.getArrivalDate());
									
									String bookingClass = null;
									if (reservationSegment.getFareTO() != null) {
										bookingClass = reservationSegment.getFareTO().getBookingClassCode();
									} else {
										bookingClass = reservationSegment.getCabinClassCode();
									}
									String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(
											typeBRequestDTO.getCsOCCarrierCode(), bookingClass);
									segmentDTO.setBookingCode(TTYMessageUtil.getExternalCsBCForGdsMappedBC(
											typeBRequestDTO.getCsOCCarrierCode(), gdsBookingClass));
									
									if (reservationSegment.getFlightNo() != null
											&& reservationSegment.getFlightNo().length() > 2) {
										segmentDTO.setFlightNumber(reservationSegment.getFlightNo().substring(2));
									}
									ssrDetailDTO.setSegmentDTO(segmentDTO);
									if (reservationSegment.getCsOcCarrierCode() != null) {
										ssrDetailDTO.setCarrierCode(reservationSegment.getCsOcCarrierCode());
									} else {
										ssrDetailDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
									}
									ssrDetailDTO.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.NEED.getCode());

									if (typeBRequestDTO.getCsOCCarrierCode() == null
											|| (reservationSegment.getCsOcCarrierCode() != null && reservationSegment
													.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode()))) {
										ssrMap.get(segSeq).put(paxSegmentSsrDTO.getSsrCode(), ssrDetailDTO);
										ssrDetailsDTOs.add(ssrDetailDTO);
									}
								}
								if (ssrMap.get(segSeq).containsKey(paxSegmentSsrDTO.getSsrCode())) {
									ssrDetailDTO = ssrMap.get(segSeq).get(paxSegmentSsrDTO.getSsrCode());
									NameDTO nameDTO = new NameDTO();
									nameDTO.setFirstName(pax.getFirstName());
									nameDTO.setLastName(pax.getLastName());
									nameDTO.setPaxTitle(pax.getTitle());
									ssrDetailDTO.getNameDTOs().add(nameDTO);
									ssrDetailDTO.setNoOfPax(ssrDetailDTO.getNoOfPax() + 1);
								}
							}
						}
					}
					if (ssrAssembler.getCanceledSegmentSSRIdMap() != null && ssrAssembler.getCanceledSegmentSSRIdMap().size() > 0) {

						Set<ReservationPaxFare> paxFares = pax.getPnrPaxFares();
						for (Iterator<ReservationPaxFare> itPaxFares = paxFares.iterator(); itPaxFares.hasNext(); ) {
							ReservationPaxFare reservationPaxFare = (ReservationPaxFare) itPaxFares.next();
							Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();

							for (Iterator<ReservationPaxFareSegment> itPaxFareSeg = paxFareSegments.iterator(); itPaxFareSeg
									.hasNext(); ) {
								ReservationPaxFareSegment reservationPaxFareSegment = (ReservationPaxFareSegment) itPaxFareSeg
										.next();
								Set<ReservationPaxSegmentSSR> segmentSSRs = reservationPaxFareSegment
										.getReservationPaxSegmentSSRs();

								for (Iterator<ReservationPaxSegmentSSR> itSegmentSSRs = segmentSSRs.iterator(); itSegmentSSRs
										.hasNext(); ) {
									ReservationPaxSegmentSSR segmentSSR = (ReservationPaxSegmentSSR) itSegmentSSRs.next();
									Integer segmentSSRId = ssrAssembler.getCanceledSegmentSSRIdMap().get(
											segmentSSR.getPnrPaxSegmentSSRId());

									if (segmentSSRId != null) {
										ssrDetailDTO = new SSRDetailDTO();
										ssrDetailDTO.setCodeSSR(ssrAssembler.canceledSegemntSSRCodeMap().get(segmentSSRId));
										ssrDetailDTO.setNoOfPax(1);
										NameDTO nameDTO = new NameDTO();
										nameDTO.setFirstName(pax.getFirstName());
										nameDTO.setLastName(pax.getLastName());
										nameDTO.setPaxTitle(pax.getTitle());
										ssrDetailDTO.getNameDTOs().add(nameDTO);

										for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
											if (reservationSegmentDTO.getSegmentSeq().intValue() == reservationPaxFareSegment
													.getSegment().getSegmentSeq().intValue()) {
												SegmentDTO segmentDTO = new SegmentDTO();
												segmentDTO.setDepartureStation(reservationSegmentDTO.getOrigin());
												segmentDTO.setDestinationStation(reservationSegmentDTO.getDestination());
												segmentDTO.setDepartureDate(reservationSegmentDTO.getDepartureDate());
												segmentDTO.setArrivalDate(reservationSegmentDTO.getArrivalDate());
												String bookingClass = null;
												if (reservationSegmentDTO.getFareTO() != null) {
													bookingClass = reservationSegmentDTO.getFareTO()
															.getBookingClassCode();
												} else {
													bookingClass = reservationSegmentDTO.getCabinClassCode();
												}
												String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(
														typeBRequestDTO.getCsOCCarrierCode(), bookingClass);
												segmentDTO.setBookingCode(TTYMessageUtil.getExternalCsBCForGdsMappedBC(
														typeBRequestDTO.getCsOCCarrierCode(), gdsBookingClass));
												
												if (reservationSegmentDTO.getFlightNo() != null
														&& reservationSegmentDTO.getFlightNo().length() > 2) {
													segmentDTO.setFlightNumber(reservationSegmentDTO.getFlightNo().substring(2));
												}
												if (reservationSegmentDTO.getCsOcCarrierCode() != null) {
													ssrDetailDTO.setCarrierCode(reservationSegmentDTO.getCsOcCarrierCode());
												} else {
													ssrDetailDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
												}
												ssrDetailDTO.setSegmentDTO(segmentDTO);
											}
										}

										ssrDetailDTO.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CANCEL.getCode());
										if (typeBRequestDTO.getCsOCCarrierCode() == null
												|| (ssrDetailDTO.getCarrierCode() != null && ssrDetailDTO.getCarrierCode()
														.equals(typeBRequestDTO.getCsOCCarrierCode()))) {
											ssrDetailsDTOs.add(ssrDetailDTO);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return ssrDetailsDTOs;
	}
	
}
