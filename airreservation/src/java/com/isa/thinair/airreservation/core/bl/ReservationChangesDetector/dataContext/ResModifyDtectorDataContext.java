package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext;

import java.util.Map;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;

public class ResModifyDtectorDataContext {

	private Reservation exisitingReservation;

	private Reservation updatedReservation;

	Map<String, Map<String, String>> observerWiseRulesAndActionMap;

	private Map<String, RuleResponseDTO> ruleWiseResponseMap;

	public Reservation getExisitingReservation() {
		return exisitingReservation;
	}

	public void setExisitingReservation(Reservation exisitingReservation) {
		this.exisitingReservation = exisitingReservation;
	}

	public Reservation getUpdatedReservation() {
		return updatedReservation;
	}

	public void setUpdatedReservation(Reservation updatedReservation) {
		this.updatedReservation = updatedReservation;
	}

	public Map<String, RuleResponseDTO> getRuleWiseResponseMap() {
		return ruleWiseResponseMap;
	}

	public void setRuleWiseResponseMap(Map<String, RuleResponseDTO> ruleWiseResponseMap) {
		this.ruleWiseResponseMap = ruleWiseResponseMap;
	}

	public Map<String, Map<String, String>> getObserverWiseRulesAndActionMap() {
		return observerWiseRulesAndActionMap;
	}

	public void setObserverWiseRulesAndActionMap(Map<String, Map<String, String>> observerWiseRulesAndActionMap) {
		this.observerWiseRulesAndActionMap = observerWiseRulesAndActionMap;
	}

}
