/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules;

import java.util.ArrayList;
import java.util.Arrays;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.AvailableSpaceRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class AdultEticketElementRuleExecutor extends
		BaseRuleExecutor<RulesDataContext> {

	public AdultEticketElementRuleExecutor() {
		if (rulesList == null) {
			rulesList = new ArrayList<BaseRule>();
		}
		rulesList.add(new AvailableSpaceRule());
	}

	@Override
	public RuleResponse validateElementRules(RulesDataContext context) {
		boolean isValied = true;
		response = new RuleResponse();
		for (BaseRule<RulesDataContext> rule : rulesList) {
			isValied = rule.validateRule(context);
			if (!isValied) {
				break;
			}
		}
		buildSuggestion(isValied, context);
		response.setProceedNextElement(isValied);
		return response;
	}
	
	private void buildSuggestion(boolean isValied,RulesDataContext context){
		if (!isValied) {
			String ammendingString = context.getAmmendingLine();
			int endIndex = 0;
			if(context.getCurrentLine() != null){
				endIndex = AvailableSpaceRule.MAXIMUM_CHARACTERS_PER_LINE - context.getCurrentLine().length();
			}
			String first = ammendingString.substring(0, endIndex);
			String second = ammendingString.substring(endIndex);
			String[] splittedByRange = getSplittedByIndex(second, AvailableSpaceRule.MAXIMUM_CHARACTERS_PER_LINE-4);
			
			response.setSuggestedElementText(getOrderedArray(splittedByRange,first));
		}
	}
	
	

	private String[] getSplittedByIndex(String ammendingString, int splitIndex) {
		return ammendingString.split("(?<=\\G.{" + splitIndex + "})");
	}

}
