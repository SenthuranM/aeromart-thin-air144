package com.isa.thinair.airreservation.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airreservation.api.service.ReservationAllServiceBD;

@Remote
public interface ReservationServiceBeanRemote extends ReservationAllServiceBD {

}
