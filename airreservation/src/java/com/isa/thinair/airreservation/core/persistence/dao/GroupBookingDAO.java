package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupPaymentNotificationDTO;
import com.isa.thinair.airreservation.api.model.GroupBookingReqRoutes;
import com.isa.thinair.airreservation.api.model.GroupBookingRequest;
import com.isa.thinair.airreservation.api.model.GroupBookingRequestHistory;
import com.isa.thinair.airreservation.api.model.GroupRequestStation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.UserStation;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.ServiceResponce;

public interface GroupBookingDAO {

	public GroupBookingRequest save(GroupBookingRequest grpReq);

	public ServiceResponce saveGrpBookingRoutes(GroupBookingReqRoutes grpReqRotes);

	public ServiceResponce update(GroupBookingRequest grpReq);

	public ServiceResponce updateApproveStatus(GroupBookingRequest grpRe);

	public ServiceResponce updateAdminStatus(GroupBookingRequest grpReq);

	public ServiceResponce updateAgentsStatus(GroupBookingRequest grpReq);

	public ServiceResponce updateAgentRequote(GroupBookingRequest grpReq);

	public Page<GroupBookingRequest> get(GroupBookingRequest grpReq, Integer start, Integer size, String userCode,boolean mainReqestOnly);

	public ServiceResponce saveRollForward(long requestID, Date from, Date to);

	public GroupBookingRequest find(long requestID);

	public ServiceResponce updateBookingStatus(long requestID, BigDecimal payment);

	public ServiceResponce reservationBookingStatus(long requestID, String pnr);

	public void updateBookingPaymentStatus(long requestID, BigDecimal totalPayment);

	public GroupBookingRequest getValidGroupBookingRequest(long requestID, BigDecimal payment);

	public Collection<GroupPaymentNotificationDTO> getGrooupBookingForReminders();

	public ServiceResponce saveUserStation(UserStation userStation);

	public Collection<UserStation> getUserStation(String userId);

	public ServiceResponce saveGroupBookingStation(GroupRequestStation grpRequestStation);

	public Collection<UserStation> getGroupBookingStation(long groupBookingReqId);

	public Page<GroupBookingRequest> getSharedRequest(GroupBookingRequest grpReq, Integer start, Integer size, String userCode,
			String stations, boolean mainReqestOnly);

	public ServiceResponce saveGroupBookingHistory(GroupBookingRequestHistory grpHistory);

	public Page<GroupBookingRequestHistory> getGroupBookingHistory(long requestID, Integer start, Integer size);

	public Collection<Reservation> getReservation(long groupBookingReqId);

	public String getActualPaymentByPnr(String pnr);
}
