package com.isa.thinair.airreservation.core.bl.tax;

import java.util.Date;

public class TransactionSequence {
	private int tnxSequence = 0;
	private OperationChgTnx chgTnx;
	private OperationPayTnx payTnx;
	private OperationPayTnx snapshotPay;
	private Date date;

	public TransactionSequence(Date date, int tnxSequence, OperationChgTnx chgTnx, OperationPayTnx payTnx,
			OperationPayTnx snapshotPay) {
		this.tnxSequence = tnxSequence;
		this.chgTnx = chgTnx;
		this.payTnx = payTnx;
		this.snapshotPay = snapshotPay;
		this.date = date;
	}

	public OperationChgTnx getChargeTnx() {
		return chgTnx;
	}

	public OperationPayTnx getPaymentTnx() {
		return payTnx;
	}

	public OperationPayTnx getSnapshotPaymentTnx() {
		return snapshotPay;
	}

	public int getTnxSequence() {
		return tnxSequence;
	}

	public Date getDate() {
		return date;
	}

	@Override
	public String toString() {
		return "TransactionSequence [tnxSequence=" + tnxSequence + ", chgTnx=" + chgTnx + ", payTnx=" + payTnx + ", snapshotPay="
				+ snapshotPay + ", date: " + date + "]";
	}

}
