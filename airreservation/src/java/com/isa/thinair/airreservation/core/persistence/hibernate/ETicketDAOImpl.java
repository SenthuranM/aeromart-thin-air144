package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.model.BookingClass.BookingClassType;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegEtktTransition;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.TicketGeneratorCriteriaType;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.MessageIdentifiers;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author eric
 * 
 * @isa.module.dao-impl dao-name="ETicketDAO"
 */
public class ETicketDAOImpl extends PlatformBaseHibernateDaoSupport implements ETicketDAO {

	private static Log log = LogFactory.getLog(ETicketDAOImpl.class);

	@Override
	public void saveOrUpdate(Collection<ReservationPaxFareSegmentETicket> eTickets) {
		super.hibernateSaveOrUpdateAll(eTickets);
	}

	public void removeEtickets(Collection<Integer> pnrPaxFareSegIds) throws ModuleException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "DELETE  FROM T_PNR_PAX_FARE_SEG_E_TICKET WHERE PPFS_ID IN ("
				+ BeanUtils.constructINStringForInts(pnrPaxFareSegIds) + ") ";

		jt.update(sql);
	}

	@Override
	public Collection<ReservationPaxFareSegmentETicket> getReservationPaxFareSegmentETickets(Integer pnrPaxId) {

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT rs FROM ReservationPaxFareSegmentETicket rs WHERE rs.reservationPaxETicket.pnrPaxId = ? ");

		Object[] params = { pnrPaxId };
		return find(sqlBuilder.toString(), params, ReservationPaxFareSegmentETicket.class);
	}

	@Override
	public Collection<EticketTO> getReservationETickets(String pnr) {

		String sql = "SELECT pax.pax_type_code 			AS type_code,"
				+ " ppfst.pnr_pax_fare_seg_e_ticket_id 	AS pnr_pax_fare_seg_e_ticket_id,"
				+ " pet.pnr_pax_id 						AS pnr_pax_id,"
				+ " ppfs.ppfs_id 						AS ppfs_id,"
				+ " ppfs.pnr_seg_id 					AS pnr_seg_id,"
				+ " ppfs.pax_status 					AS pax_status,"
				+ " pet.e_ticket_number 				AS e_ticket_number,"
				+ " ppfst.coupon_number 				AS coupon_number,"
				+ " ppfst.status 						AS status,"
				+ " ppfst.ext_e_ticket_number 			AS ext_e_ticket_number,"
				+ " ppfst.ext_coupon_number 			AS ext_coupon_number,"
				+ " ppfst.ext_coupon_status 			AS ext_coupon_status,"
				+ " ppfst.ext_coupon_control 			AS ext_coupon_control "				
				+ " FROM t_pnr_pax_fare_segment ppfs,"
				+ " t_pnr_pax_fare ppf, t_pnr_pax_fare_seg_e_ticket ppfst, t_pax_e_ticket pet, t_pnr_passenger pax, t_booking_class bc "
				+ " WHERE pax.pnr_pax_id 	= pet.pnr_pax_id " + " AND ppf.pnr_pax_id=pax.pnr_pax_id "
				+ " AND ppf.ppf_id      	= ppfs.ppf_id " + " AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id"
				+ " AND ppfst.ppfs_id 		= ppfs.ppfs_id AND pax.pnr = '" + pnr + "' AND ppfs.booking_code = bc.booking_code(+)"
				+ " AND (bc.bc_type <> '" + BookingClassType.OPEN_RETURN + "' OR bc.bc_type IS NULL ) "
				+ " AND ppfs.pnr_seg_id IN (SELECT ps.pnr_seg_id FROM t_pnr_segment ps " + " WHERE ps.pnr = '" + pnr
				+ "' AND ps.open_rt_confirm_before is null " + ") ORDER BY ppfst.pnr_pax_fare_seg_e_ticket_id";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Collection<EticketTO> colEtickets = (Collection<EticketTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<EticketTO> etickets = new ArrayList<EticketTO>();

				while (rs.next()) {
					EticketTO eticketTO = new EticketTO();
					eticketTO.setEticketId(rs.getInt("pnr_pax_fare_seg_e_ticket_id"));
					eticketTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
					eticketTO.setPnrPaxFareSegId(rs.getInt("ppfs_id"));
					eticketTO.setPnrSegId(rs.getInt("pnr_seg_id"));
					eticketTO.setEticketNumber(rs.getString("e_ticket_number"));
					if (rs.getString("ext_e_ticket_number") != null && rs.getInt("ext_coupon_number") != 0) {
						eticketTO.setExternalEticketNumber(rs.getString("ext_e_ticket_number"));
						eticketTO.setExternalCouponNo(rs.getInt("ext_coupon_number"));
						eticketTO.setExternalCouponStatus(rs.getString("ext_coupon_status"));
						eticketTO.setExternalCouponControl(rs.getString("ext_coupon_control"));
					}
					eticketTO.setTicketStatus(rs.getString("status"));
					eticketTO.setCouponNo(rs.getInt("coupon_number"));
					eticketTO.setPaxStatus(rs.getString("pax_status"));
					etickets.add(eticketTO);
				}
				return etickets;
			}
		});

		return colEtickets;

	}

	@Override
	public String getNextIATAETicketSQID() {

		log.debug("Inside getNextETicketSQID");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " SELECT S_RESERVATION_IATA_ETICKET.NEXTVAL ETICKET FROM DUAL ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String eTicketNo = (String) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String eTicketNo = null;

				if (rs != null) {
					if (rs.next()) {
						eTicketNo = BeanUtils.nullHandler(rs.getString("ETICKET"));
					}
				}

				return eTicketNo;
			}
		});

		log.debug("Exit getNextIATA-ETicketSQID");
		return eTicketNo;

	}

	@Override
	public String getNextIATAETicketSQID(String sequenceName) {

		log.debug("Inside getNextETicketSQID");

		if (sequenceName != null && !sequenceName.isEmpty()) {

			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			String sql = " SELECT " + sequenceName + ".NEXTVAL ETICKET FROM DUAL ";

			log.debug("############################################");
			log.debug(" SQL to excute            : " + sql);
			log.debug("############################################");

			String eTicketNo = (String) jt.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					String eTicketNo = null;

					if (rs != null) {
						if (rs.next()) {
							eTicketNo = BeanUtils.nullHandler(rs.getString("ETICKET"));
						}
					}
					return eTicketNo;
				}
			});

			log.debug("Exit getNextIATA-ETicketSQID");
			return eTicketNo;
		}
		return null;

	}

	@Override
	public String getAgentEticketSequnceName(String agentCode, TicketGeneratorCriteriaType criteriaType)
			throws CommonsDataAccessException {
		log.debug("Inside getEticketSequnceName");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " SELECT tsn.bsp_tkt_seq_name as BSP_TKT_SEQ_NAME " + "FROM t_agent ta, " + " t_station ts    , "
				+ " t_country tc    , " + " T_E_TICKET_SEQUENCE_NAME tsn " + " WHERE ta.station_code =ts.station_code "
				+ " AND tsn.ticket_type ='" + criteriaType.getCode() + "' " + " AND ts.country_code     =tsn.country_code "
				+ " AND ta.agent_code       = '" + agentCode + "'";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String eTicketSequnceName = (String) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String eTicketSequnceName = null;

				if (rs != null) {
					if (rs.next()) {
						eTicketSequnceName = BeanUtils.nullHandler(rs.getString("BSP_TKT_SEQ_NAME"));
					}
				}

				return eTicketSequnceName;
			}
		});
		log.debug("Exit getEticketSequnceName");
		return eTicketSequnceName;

	}

	@SuppressWarnings("unchecked")
	@Override
	public ReservationPaxFareSegmentETicket getPaxFareSegmentEticket(String eticketNumber, int coupNumber,
			String msgType) {
		Object params[] = { eticketNumber, coupNumber };
		StringBuilder sqlBuilder = new StringBuilder();
		if (MessageIdentifiers.PRL.equals(msgType)) {
			sqlBuilder
					.append("SELECT rs FROM ReservationPaxFareSegmentETicket rs WHERE rs.reservationPaxETicket.eticketNumber = ? AND rs.couponNo = ? ");
		} else if (MessageIdentifiers.ETL.equals(msgType)) {
			sqlBuilder
					.append("SELECT rs FROM ReservationPaxFareSegmentETicket rs WHERE rs.reservationPaxETicket.eticketNumber = ? AND rs.couponNo = ? AND (rs.status='"
							+ EticketStatus.OPEN.code() + "' OR rs.status='" + EticketStatus.CHECKEDIN.code()
							+ "' OR rs.status='" + EticketStatus.BOARDED.code() + "')");
		}


		List<ReservationPaxFareSegmentETicket> paxFareSegmentEticket = find(sqlBuilder.toString(), params, ReservationPaxFareSegmentETicket.class);
		if (paxFareSegmentEticket != null && paxFareSegmentEticket.size() > 0) {
			return paxFareSegmentEticket.get(0);
		} else {
			return null;
		}

	}

	@Override
	public EticketTO getPassengerETicketDetail(Integer pnrPaxId, Integer flightId) {

		String sql = " SELECT ppfst.pnr_pax_fare_seg_e_ticket_id AS pnr_pax_fare_seg_e_ticket_id , "
				+ " pet.pnr_pax_id                           AS pnr_pax_id                   , "
				+ " ppfs.ppfs_id                             AS ppfs_id                      , "
				+ " ppfs.pnr_seg_id                          AS pnr_seg_id                   , "
				+ " pet.e_ticket_number                      AS e_ticket_number              , "
				+ " ppfst.coupon_number                      AS coupon_number                , "
				+ " ppfst.status                             AS status                       , "
				+ " fs.flight_id                             AS flightId, "
				+ " ppfst.ext_e_ticket_number    as ext_e_ticket_number, "
				+ " ppfst.ext_coupon_number    as ext_coupon_number  "  
				+ " FROM t_pnr_segment ps            , "
				+ " t_flight_segment fs               , " + " t_pnr_pax_fare_segment ppfs       , "
				+ "  t_pnr_pax_fare_seg_e_ticket ppfst , " + "  t_pax_e_ticket pet " + "  WHERE pet.pnr_pax_id  = " + pnrPaxId
				+ " AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id " + " AND ppfst.ppfs_id       = ppfs.ppfs_id "
				+ " AND ppfs.pnr_seg_id     = ps.pnr_seg_id " + " AND ps.flt_seg_id       = fs.flt_seg_id "
				+ " AND fs.flight_id        = " + flightId + " AND ppfst.pnr_pax_fare_seg_e_ticket_id = "
				+ "  (SELECT MAX(ppfst.pnr_pax_fare_seg_e_ticket_id) " + "  FROM T_PNR_PAX_FARE_SEG_E_TICKET ppfst "
				+ "  WHERE ppfs.ppfs_id = ppfst.ppfs_id " + " GROUP BY ppfst.ppfs_id )"
				+ " ORDER BY ppfst.pnr_pax_fare_seg_e_ticket_id  ";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		EticketTO eticketTO = (EticketTO) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				EticketTO eticketTO = null;
				while (rs.next()) {
					eticketTO = new EticketTO();
					eticketTO.setEticketId(rs.getInt("pnr_pax_fare_seg_e_ticket_id"));
					eticketTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
					eticketTO.setPnrPaxFareSegId(rs.getInt("ppfs_id"));
					eticketTO.setPnrSegId(rs.getInt("pnr_seg_id"));
					eticketTO.setEticketNumber(rs.getString("e_ticket_number"));
					eticketTO.setTicketStatus(rs.getString("status"));
					eticketTO.setCouponNo(rs.getInt("coupon_number"));
					eticketTO.setExternalEticketNumber(rs.getString("ext_e_ticket_number"));
					eticketTO.setExternalCouponNo(rs.getInt("ext_coupon_number"));
					
				}
				return eticketTO;
			}
		});
		return eticketTO;
	}

	@Override
	public void updatePaxFareSegmentsEticketStatus(Collection<Integer> colPpfsId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " UPDATE T_PNR_PAX_FARE_SEG_E_TICKET SET STATUS = '" + EticketStatus.FLOWN.code()
				+ "' , VERSION = VERSION + 1 WHERE PPFS_ID IN (" + Util.buildIntegerInClauseContent(colPpfsId)
				+ ") AND (STATUS = '" + EticketStatus.OPEN.code() + "' OR STATUS = '" + EticketStatus.CHECKEDIN.code()
				+ "' OR STATUS = '" + EticketStatus.BOARDED.code() + "')";

		jt.execute(sql);
	}

	@Override
	public Integer getPassengerIdByETicketNumber(String eticketNumber) {
		Object params[] = { eticketNumber };
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT rs.pnrPaxId FROM ReservationPaxETicket rs WHERE rs.eticketNumber = ?");

		List<Integer> reservationPaxID = find(sqlBuilder.toString(), params, Integer.class);
		if (reservationPaxID.size() > 0) {
			return new Integer(reservationPaxID.get(0).toString());
		} else {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT rs.pnrPaxId FROM ReservationPaxETicket rs,ReservationPaxFareSegmentETicket rpfse WHERE rpfse.externalEticketNumber = ? and rpfse.reservationPaxETicket = rs.paxETicketId");

			reservationPaxID = find(sql.toString(), params, Integer.class);
			
			if (reservationPaxID.size() > 0) {
				return new Integer(reservationPaxID.get(0).toString());
			}			
			return null;
		}

	}

	@Override
	public String getPNRByEticketNo(String eticketNumber) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "select distinct b.pnr from t_pax_e_ticket a, t_pnr_passenger b where a.pnr_pax_id = b.pnr_pax_id"
				+ " and a.e_ticket_number = '" + eticketNumber + "'";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String pnr = (String) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String pnr = null;

				if (rs != null) {
					if (rs.next()) {
						pnr = BeanUtils.nullHandler(rs.getString("PNR"));
					}
				}

				return pnr;
			}
		});
		
		// same eticket may duplicate in external ??? if not can add the EXT_E_TICKET_NUMBER check to above sql it self
		// required only when codeshare/gds PNR
		if (pnr == null) {
			sql = "select distinct b.pnr " +
					"from t_pnr_pax_fare_seg_e_ticket c, t_pax_e_ticket a, t_pnr_passenger b " +
					"where a.pnr_pax_id = b.pnr_pax_id and c.pax_e_ticket_id = a.pax_e_ticket_id " +
					"and c.ext_e_ticket_number = '" + eticketNumber + "'";

			log.debug("############################################");
			log.debug(" SQL to excute            : " + sql);
			log.debug("############################################");

			pnr = (String) jt.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					String pnr = null;

					if (rs != null) {
						if (rs.next()) {
							pnr = BeanUtils.nullHandler(rs.getString("PNR"));
						}
					}

					return pnr;
				}
			});

		}

		return pnr;
	}

	@Override
	public Map<String, Set<String>> getPNRsByEticketNo(List<String> eticketNumbers) throws ModuleException {
		Map<String, Set<String>> etNumbersForPnr = null;
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "select b.pnr pnr, a.e_ticket_number e_ticket_number from t_pax_e_ticket a, t_pnr_passenger b where a.pnr_pax_id = b.pnr_pax_id"
				+ " and a.e_ticket_number in ( :et_numbers ) ";

		etNumbersForPnr = (Map<String, Set<String>>) jt.query(sql, new Object[] { eticketNumbers }, new int[] { Types.ARRAY },
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						Map<String, Set<String>> etForPnr = new HashMap<String, Set<String>>();
						String pnr;
						String etNumber;

						while (resultSet.next()) {
							pnr = resultSet.getString("pnr");
							etNumber = resultSet.getString("e_ticket_number");

							if (!etForPnr.containsKey(pnr)) {
								etForPnr.put(pnr, new HashSet<String>());
							}

							etForPnr.get(pnr).add(etNumber);
						}

						return etForPnr;
					}
				});

		return etNumbersForPnr;
	}

	@Override
	public Collection<ReservationPaxFareSegmentETicket> getReservationPaxFareSegmentETickets(
			Collection<Integer> pnrPaxFareSegmentIds) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT rs FROM ReservationPaxFareSegmentETicket rs WHERE rs.pnrPaxFareSegId IN ("
				+ Util.buildIntegerInClauseContent(pnrPaxFareSegmentIds) + ") ");
		return find(sqlBuilder.toString(), ReservationPaxFareSegmentETicket.class);
	}

	@Override
	public EticketTO getEticketInfo(String eticketNumber, Integer coupNumber) throws ModuleException {
		String sql = " SELECT ps.flt_seg_id, ppfst.ppfs_id, ppfs.pax_status, pet.pnr_pax_id " + " FROM t_pnr_segment ps, "
				+ " t_pnr_pax_fare_segment ppfs, " + " t_pnr_pax_fare_seg_e_ticket ppfst , " + " t_pax_e_ticket pet "
				+ " WHERE pet.e_ticket_number  = '" + eticketNumber + "'" + " AND ppfst.coupon_number = " + coupNumber + " AND ("
				+ "ppfst.status= '" + EticketStatus.OPEN.code() + "'" + " OR ppfst.status= '" + EticketStatus.CHECKEDIN.code()
				+ "'" + " OR ppfst.status= '" + EticketStatus.BOARDED.code() + "')"
				+ " AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id " + " AND ppfst.ppfs_id       = ppfs.ppfs_id "
				+ " AND ppfs.pnr_seg_id     = ps.pnr_seg_id ";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		EticketTO eticketTO = (EticketTO) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				EticketTO eticketTO = null;
				if (rs != null && rs.next()) {
					eticketTO = new EticketTO();
					eticketTO.setFlightSegId(rs.getInt("flt_seg_id"));
					eticketTO.setPnrPaxFareSegId(rs.getInt("ppfs_id"));
					eticketTO.setPaxStatus(rs.getString("pax_status"));
					eticketTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
				}
				return eticketTO;
			}
		});
		return eticketTO;
	}

	@Override
	public LccClientPassengerEticketInfoTO getEticketInfo(Integer couponId) throws ModuleException {
		String sql = "select pet.e_ticket_number, ppfst.status as e_ticket_status, ppfst.coupon_number, "
				+ " f.flight_number, fs.segment_code, fs.est_time_departure_local, ppfs.pax_status as pax_status  "
				+ " FROM t_pnr_pax_fare_seg_e_ticket ppfst, "
				+ " t_pnr_pax_fare_segment ppfs, t_pnr_segment ps, t_flight_segment fs , "
				+ " t_pax_e_ticket pet, t_flight f WHERE ppfst.pnr_pax_fare_seg_e_ticket_id = '" + couponId + "' "
				+ " and ppfst.ppfs_id = ppfs.ppfs_id and ppfs.pnr_seg_id = ps.pnr_seg_id "
				+ " and ps.flt_seg_id = fs.flt_seg_id and pet.pax_e_ticket_id = ppfst.pax_e_ticket_id "
				+ " and fs.flight_id = f.flight_id";
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		LccClientPassengerEticketInfoTO eticketTO = (LccClientPassengerEticketInfoTO) template.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						LccClientPassengerEticketInfoTO eticketTO = null;
						if (rs != null && rs.next()) {
							eticketTO = new LccClientPassengerEticketInfoTO();
							eticketTO.setPaxETicketNo(rs.getString("e_ticket_number"));
							eticketTO.setPaxETicketStatus(rs.getString("e_ticket_status"));
							eticketTO.setPaxStatus(rs.getString("pax_status"));
							eticketTO.setCouponNo(rs.getInt("coupon_number"));
							eticketTO.setFlightNo(rs.getString("flight_number"));
							eticketTO.setSegmentCode(rs.getString("segment_code"));
							eticketTO.setDepartureDate(rs.getDate("est_time_departure_local"));

						}
						return eticketTO;
					}
				});
		return eticketTO;
	}

	@Override
	public void updatePassengerCouponStatus(Integer couponId, String couponStatus) throws ModuleException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " UPDATE T_PNR_PAX_FARE_SEG_E_TICKET SET STATUS = '" + couponStatus
				+ "' , VERSION = VERSION + 1 WHERE PNR_PAX_FARE_SEG_E_TICKET_ID =" + couponId;

		jt.execute(sql);

	}

	@Override
	public void updatePassengerStatus(Integer couponId, String paxStatus) throws ModuleException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " UPDATE t_pnr_pax_fare_segment SET pax_status  ='" + paxStatus + "' WHERE ppfs_id = " + " (SELECT ppfs_id "
				+ "   	FROM t_pnr_pax_fare_seg_e_ticket " + " 	WHERE pnr_pax_fare_seg_e_ticket_id=" + couponId + " )";

		jt.execute(sql);
	}

	@Override
	public String getBookingClassType(String eticketNumber, Integer coupNumber) throws ModuleException {
		String sql = " SELECT bc.bc_type " + " FROM t_pnr_pax_fare_segment ppfs, " + " t_pnr_pax_fare_seg_e_ticket ppfst , "
				+ " t_pax_e_ticket pet, " + " t_booking_class bc " + " WHERE pet.e_ticket_number  = '" + eticketNumber + "'"
				+ " AND ppfst.coupon_number = " + coupNumber + " AND (" + " ppfst.status= '" + EticketStatus.OPEN.code() + "'"
				+ " OR ppfst.status= '" + EticketStatus.CHECKEDIN.code() + "'" + " OR ppfst.status= '"
				+ EticketStatus.BOARDED.code() + "')" + " AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id "
				+ " AND ppfst.ppfs_id       = ppfs.ppfs_id " + " AND bc.booking_code     = ppfs.booking_code ";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String bcType = (String) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String bcType = null;
				if (rs != null && rs.next()) {
					bcType = rs.getString("bc_type");
				}
				return bcType;
			}
		});
		return bcType;
	}

	@Override
	public void saveOrUpdate(ReservationPaxFareSegEtktTransition eTicketTransition) throws ModuleException {
		if (eTicketTransition != null) {
			super.hibernateSaveOrUpdate(eTicketTransition);
		}
	}

	@Override
	public EticketTO getEticketDetailsByExtETicketNumber(String extEticketNumber, Integer extCoupNumber) throws ModuleException {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PET.PNR_PAX_ID,PET.E_TICKET_NUMBER AS E_TICKET_NUMBER,");
		sql.append("	PPFST.COUPON_NUMBER AS COUPON_NUMBER,PPFST.STATUS AS STATUS,");
		sql.append("	PPFST.EXT_E_TICKET_NUMBER AS EXT_E_TICKET_NUMBER,");
		sql.append("	PPFST.EXT_COUPON_NUMBER AS EXT_COUPON_NUMBER  ");
		sql.append("FROM  T_PAX_E_TICKET PET,T_PNR_PAX_FARE_SEG_E_TICKET PPFST ");
		sql.append("WHERE PET.PAX_E_TICKET_ID = PPFST.PAX_E_TICKET_ID ");
		sql.append("	AND PPFST.EXT_E_TICKET_NUMBER  = '" + extEticketNumber + "' AND PPFST.EXT_COUPON_NUMBER =" + extCoupNumber);

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		EticketTO eticketTO = (EticketTO) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				EticketTO eticketTO = null;
				while (rs.next()) {
					eticketTO = new EticketTO();
					eticketTO.setPnrPaxId(rs.getInt("PNR_PAX_ID"));
					eticketTO.setEticketNumber(rs.getString("E_TICKET_NUMBER"));
					eticketTO.setTicketStatus(rs.getString("STATUS"));
					eticketTO.setCouponNo(rs.getInt("COUPON_NUMBER"));
					eticketTO.setExternalEticketNumber(rs.getString("EXT_E_TICKET_NUMBER"));
					eticketTO.setExternalCouponNo(rs.getInt("EXT_COUPON_NUMBER"));

				}
				return eticketTO;
			}
		});
		return eticketTO;
	}

	@Override
	public boolean isCodeSharePNR(String extEticketNumber) throws ModuleException {
		StringBuilder sql = new StringBuilder();
		sql.append("select gd.cs_carrier ");
		sql.append("from t_reservation r, t_pnr_passenger pp, t_pax_e_ticket pe, t_gds gd, t_pnr_pax_fare_seg_e_ticket c ");
		sql.append("where pp.pnr = r.pnr and pe.pnr_pax_id = pp.pnr_pax_id and r.gds_id = gd.gds_id ");
		sql.append("and pe.pax_e_ticket_id = c.pax_e_ticket_id and r.external_rec_locator is not null ");
		sql.append("AND (c.EXT_E_TICKET_NUMBER ='" + extEticketNumber + "' OR PE.E_TICKET_NUMBER ='" + extEticketNumber + "')");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Boolean) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					String codeShareCarrier = rs.getString("CS_CARRIER");
					if (codeShareCarrier != null && "Y".equalsIgnoreCase(codeShareCarrier.trim())) {
						return true;
					}

				}
				return false;
			}
		});

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Integer> getPaxFareSegmentETicketIDs(Collection<Integer> pnrSegIds) throws ModuleException {
		StringBuilder sql = new StringBuilder();
		Collection<Integer> eticketTO = null;
		if (pnrSegIds != null && !pnrSegIds.isEmpty()) {
			sql.append("SELECT SEGETICK.PNR_PAX_FARE_SEG_E_TICKET_ID AS ETKT_ID ");
			sql.append("FROM T_PNR_PAX_FARE_SEGMENT PAXFARE,T_PNR_SEGMENT SEG,T_PNR_PAX_FARE_SEG_E_TICKET SEGETICK ");
			sql.append("WHERE PAXFARE.PNR_SEG_ID=SEG.PNR_SEG_ID ");
			sql.append("AND PAXFARE.PPFS_ID = SEGETICK.PPFS_ID ");
			sql.append("AND SEG.PNR_SEG_ID IN (" + BeanUtils.constructINStringForInts(pnrSegIds) + ") ");

			JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

			eticketTO = (ArrayList<Integer>) template.query(sql.toString(), new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<Integer> list = new ArrayList<Integer>();
					while (rs.next()) {
						list.add(rs.getInt("ETKT_ID"));

					}
					return list;
				}
			});

		}
		return eticketTO;
	}

}
