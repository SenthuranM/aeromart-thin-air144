/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.aig.INSChargeTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationChargeDAO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * ReservationChargeDAOImpl is the business DAO hibernate implmentation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservationChargeDAO"
 */
public class ReservationChargeDAOImpl extends PlatformHibernateDaoSupport implements ReservationChargeDAO {

	/** Holds the logger instance */
	// private static Log log = LogFactory.getLog(ReservationChargeDAOImpl.class);

	/**
	 * Removes Fare Segment charges
	 * 
	 * @param colPnrPaxFareSegIds
	 */
	public void removeFareSegChargesAndPayments(Collection<Integer> colPnrPaxFareSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "DELETE  FROM T_PNR_PAX_SEG_PAYMENTS WHERE PFST_ID IN  "
				+ " (SELECT PFST_ID FROM T_PNR_PAX_SEG_CHARGES WHERE PPFS_ID IN ("
				+ BeanUtils.constructINStringForInts(colPnrPaxFareSegIds) + ")) ";

		jt.update(sql);

		sql = "DELETE FROM T_PNR_PAX_SEG_CHARGES WHERE PPFS_ID IN (" + BeanUtils.constructINStringForInts(colPnrPaxFareSegIds)
				+ ") ";

		jt.update(sql);
	}

	@SuppressWarnings("unchecked")
	public Collection<INSChargeTO> getInsuranceCharges(Collection<Integer> colpnrPaxId, int chargeratId) {

		StringBuilder sql = new StringBuilder();
		sql.append("select chgs.*, fare.pnr_pax_id from t_pnr_pax_ond_charges chgs,  t_pnr_pax_fare fare ");
		sql.append(" where chgs.ppf_id = fare.ppf_id ");
		sql.append(" and  fare.pnr_pax_id in (" + Util.constructINStringForInts(colpnrPaxId) + ") ");
		sql.append(" and chgs.charge_rate_id  = " + chargeratId);

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<INSChargeTO> colInsCharges = (Collection<INSChargeTO>) template.query(sql.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<INSChargeTO> insCharges = new ArrayList<INSChargeTO>();
						INSChargeTO insCharge = null;
						while (rs.next()) {
							insCharge = new INSChargeTO();
							insCharge.setAmount(rs.getBigDecimal("amount"));
							insCharge.setChargeGroup(rs.getString("charge_group_code"));
							insCharge.setChargeRateId(rs.getInt("charge_rate_id"));
							insCharge.setPkey(rs.getInt("pft_id"));
							insCharge.setPnrPaxFareId(rs.getInt("ppf_id"));
							insCharge.setPnrPaxId(rs.getInt("pnr_pax_id"));
							insCharges.add(insCharge);
						}
						return insCharges;
					}
				});
		return colInsCharges;
	}

	/**
	 * Return a map of ReservationPaxOndCharge for a given colPnrPaxOndChgIds from the ReservationPaxOndCharge
	 */
	@SuppressWarnings("unchecked")
	public Map<Long, ReservationPaxOndCharge> getReservationPaxOndCharges(Collection<Long> colPnrPaxOndChgId) {
		StringBuilder sqlCondition = new StringBuilder();
		for (Long pnrPaxOndChgId : colPnrPaxOndChgId) {
			if (sqlCondition.length() == 0) {
				sqlCondition.append(" WHERE pnrPaxOndChgId = " + pnrPaxOndChgId);
			} else {
				sqlCondition.append(" OR pnrPaxOndChgId = " + pnrPaxOndChgId);
			}
		}

		Map<Long, ReservationPaxOndCharge> mapOndCharges = new HashMap<Long, ReservationPaxOndCharge>();
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge = find("FROM ReservationPaxOndCharge " + sqlCondition,
				ReservationPaxOndCharge.class);

		for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
			ReservationPaxOndCharge colReservationPaxOndCharge = mapOndCharges.get(reservationPaxOndCharge.getPnrPaxOndChgId());

			if (colReservationPaxOndCharge == null) {
				mapOndCharges.put(reservationPaxOndCharge.getPnrPaxOndChgId().longValue(), reservationPaxOndCharge);
			}
		}
		return mapOndCharges;
	}

	public Integer getAppliedFareDiscountPercentage(String pnr) {

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT ppc.value_percentage AS value_percentage FROM ");
		sql.append("(SELECT MAX (ppoc.pft_id) OVER (PARTITION BY r.pnr, pp.pnr_pax_id) AS max_pft, ");
		sql.append("ppoc.* FROM t_reservation r, t_pnr_passenger pp, t_pnr_pax_fare ppf, t_pnr_pax_ond_charges ppoc ");
		sql.append("WHERE r.pnr = '" + pnr + "' AND ppf.total_discount > 0 AND pp.pnr = r.pnr ");
		sql.append("AND ppf.pnr_pax_id = pp.pnr_pax_id AND ppoc.ppf_id = ppf.ppf_id ");
		sql.append("AND ppoc.charge_group_code = '" + ReservationInternalConstants.ChargeGroup.FAR + "'  ");
		sql.append("AND ppoc.discount < 0 AND ppoc.value_percentage != 0 ) ppc WHERE ppc.pft_id = ppc.max_pft AND ROWNUM < 2 ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Integer fareDiscountPerc = (Integer) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer fareDiscountPerc = 0;
				while (rs.next()) {
					fareDiscountPerc = rs.getInt("value_percentage");
				}
				return fareDiscountPerc;
			}
		});
		return fareDiscountPerc;
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, String> getPaxONDChargeRefIdMap(Integer pnrPaxId, boolean isInfantCharges) {

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT PPOC.PFT_ID,(SELECT CHA.CHARGE_CODE FROM T_CHARGE_RATE CHR,T_CHARGE CHA ");
		sql.append("WHERE CHR.CHARGE_CODE = CHA.CHARGE_CODE AND CHR.CHARGE_RATE_ID=PPOC.CHARGE_RATE_ID) AS CHARGE_CODE ");

		if (isInfantCharges) {
			sql.append("FROM T_PNR_PASSENGER PP, T_PNR_PAX_FARE PPF, T_PNR_PAX_OND_CHARGES PPOC ");
			sql.append("WHERE PPF.PNR_PAX_ID = PP.PNR_PAX_ID AND PP.PAX_TYPE_CODE = '" + PaxTypeTO.INFANT + "' ");
			sql.append("AND PP.ADULT_ID = '" + pnrPaxId + "' AND PPOC.PPF_ID = PPF.PPF_ID");
		} else {

			sql.append("FROM T_PNR_PAX_FARE PPF, T_PNR_PAX_OND_CHARGES PPOC ");
			sql.append("WHERE PPF.PNR_PAX_ID = '" + pnrPaxId + "' AND PPOC.PPF_ID = PPF.PPF_ID");
		}

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, String> paxOndChgIdMap = (Map<Integer, String>) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> paxOndChgIds = new HashMap<Integer, String>();
				while (rs.next()) {
					paxOndChgIds.put(rs.getInt("PFT_ID"), rs.getString("CHARGE_CODE"));
				}
				return paxOndChgIds;
			}
		});
		return paxOndChgIdMap;

	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<Integer>> getPaxONDChargeRefIdColl(Integer pnrPaxId, boolean withInfant) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PPF.PPF_ID,PPOC.PFT_ID FROM  T_PNR_PASSENGER PP, T_PNR_PAX_FARE PPF, T_PNR_PAX_OND_CHARGES PPOC ");
		sql.append("WHERE  PPF.PNR_PAX_ID = PP.PNR_PAX_ID AND PPOC.PPF_ID = PPF.PPF_ID ");

		if (withInfant) {
			sql.append("AND (PP.ADULT_ID ='" + pnrPaxId + "' OR PP.PNR_PAX_ID='" + pnrPaxId + "') ");
		} else {
			sql.append("AND PP.PNR_PAX_ID='" + pnrPaxId + "' ");
		}
		sql.append("GROUP BY PPF.PPF_ID,PPOC.PFT_ID ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, Collection<Integer>> paxOndChgIdMap = (Map<Integer, Collection<Integer>>) template.query(sql.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map<Integer, Collection<Integer>> chrgeIdMap = new HashMap<Integer, Collection<Integer>>();

						Integer pnrPaxFareId = null;
						Integer pnrPaxChargeId = null;
						while (rs.next()) {

							pnrPaxFareId = rs.getInt("PPF_ID");
							pnrPaxChargeId = rs.getInt("PFT_ID");

							if (chrgeIdMap.get(pnrPaxFareId) == null) {
								chrgeIdMap.put(pnrPaxFareId, new ArrayList<Integer>());
							}

							chrgeIdMap.get(pnrPaxFareId).add(pnrPaxChargeId);

						}
						return chrgeIdMap;
					}
				});

		return paxOndChgIdMap;

	}

	public boolean isRefundableCharge(Long pnrPaxOndChgId) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT F_GET_REF_NOREF_BY_PFT(" + pnrPaxOndChgId + ") AS NEXTVAL FROM DUAL";

		Boolean isRefundableCharge = (Boolean) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String result = null;
				Boolean refundableCharge = new Boolean(false);

				if (rs != null) {
					while (rs.next()) {
						result = BeanUtils.nullHandler(rs.getString("NEXTVAL"));
					}
				}

				if (result != null && "R".equalsIgnoreCase(result)) {
					refundableCharge = true;
				}

				return refundableCharge;
			}
		});

		return isRefundableCharge;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Double> getCurrentTaxCharged(String taxChargeCode, String pnr) {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT pp.pax_type_code, SUM(PPOC.amount) amount ");
		sb.append("FROM T_RESERVATION R, T_PNR_PASSENGER PP, T_PNR_PAX_FARE PPF, ");
		sb.append("T_PNR_PAX_OND_CHARGES PPOC, t_charge_rate CR ");
		sb.append("WHERE R.PNR = ? AND PP.PNR = R.PNR ");
		sb.append("AND PPF.PNR_PAX_ID = PP.PNR_PAX_ID AND ppoc.ppf_id = ppf.ppf_id ");
		sb.append("AND cr.charge_rate_id=ppoc.charge_rate_id AND cr.charge_code=? ");
		sb.append("AND R.status <> 'OHD'");
		sb.append("GROUP BY pp.pnr_pax_id, pp.pax_type_code ");

		Object[] params = new Object[] { pnr, taxChargeCode };

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<String, Double> paxTypeTaxAmounts = (Map<String, Double>) jt.query(sb.toString(), params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Double> paxTypeTaxAmounts = null;

				if (rs != null) {
					paxTypeTaxAmounts = new HashMap<String, Double>();
					while (rs.next()) {
						String paxType = BeanUtils.nullHandler(rs.getString("pax_type_code"));
						Double taxAmount = rs.getDouble("amount");
						paxTypeTaxAmounts.put(paxType, taxAmount);
					}
				}

				return paxTypeTaxAmounts;
			}
		});

		return paxTypeTaxAmounts;
	}

	public Collection<ReservationPaxOndCharge> getPnrPaxOndCharges(String pnr) {
		String query = "select pft.* from T_PNR_PAX_OND_CHARGES pft, t_pnr_pax_fare ppf, T_PNR_PASSENGER pp "
				+ " where pp.pnr = ? and pp.pnr_pax_id = ppf.pnr_pax_id and ppf.ppf_id = pft.ppf_id";

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<ReservationPaxOndCharge> pnrCharges = (Collection<ReservationPaxOndCharge>) jt.query(query,
				new Object[] { pnr }, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<ReservationPaxOndCharge> pnrCharges = null;

						if (rs != null) {
							pnrCharges = new ArrayList<>();
							while (rs.next()) {
								ReservationPaxOndCharge ondCharge = new ReservationPaxOndCharge();
								ondCharge.setPnrPaxOndChgId(rs.getInt("PFT_ID"));
								ondCharge.setTransactionSeq(rs.getInt("TRANSACTION_SEQ"));
								pnrCharges.add(ondCharge);
							}
						}

						return pnrCharges;
					}
				});
		return pnrCharges;

	}
}
