/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.FLEXI_RULE_FLEXIBILITY_TYPE;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Cancellation related business methods
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CancellationUtils {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(CancellationUtils.class);

	/**
	 * Hide the constructor
	 */
	private CancellationUtils() {

	}

	/**
	 * Adjust ond fare amount
	 * 
	 * @param reservationPaxFare
	 * @param amount
	 */
	private static void adjustOndFareAmount(ReservationPaxFare reservationPaxFare, BigDecimal amount) {
		BigDecimal[] totalCharges = ReservationApiUtils.getChargeTypeAmounts();
		ReservationPaxOndCharge reservationPaxOndCharge;

		for (Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge = reservationPaxFare.getCharges().iterator(); itReservationPaxOndCharge
				.hasNext();) {
			reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();

			// If the type is FAR
			if (ReservationInternalConstants.ChargeGroup.FAR.equals(reservationPaxOndCharge.getChargeGroupCode())) {
				reservationPaxOndCharge.setAmount(amount);
			}

			// Getting the total charge
			ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
					reservationPaxOndCharge.getAmount(), totalCharges, reservationPaxOndCharge.getDiscount(), reservationPaxOndCharge.getAdjustment());
		}

		// Setting the total charge
		reservationPaxFare.setTotalChargeAmounts(totalCharges);
	}

	/**
	 * Adjust fare among ond(s)
	 * 
	 * @param reservationPaxFareTarget
	 * @param reservationPaxFareEffected
	 * @param mapPnrSegIdsAndChanges
	 * @param pnrSegmentIds
	 * @param colSetInversePnrSegIds
	 * @param isModifyProrate
	 * @throws ModuleException
	 */
	public static void adjustFareAmongOnds(ReservationPaxFare reservationPaxFareTarget,
			ReservationPaxFare reservationPaxFareEffected, Map<Collection<Integer>, String> mapPnrSegIdsAndChanges,
			Collection<Integer> pnrSegmentIds, Collection<Integer> colSetInversePnrSegIds, boolean isModifyProrate)
			throws ModuleException {

		if (!AppSysParamsUtil.isRequoteEnabled()) {
			int cancelOrModifyOndFarePercentage;
			BigDecimal totalFare = AccelAeroCalculator.add(reservationPaxFareTarget.getTotalFare(),
					reservationPaxFareEffected.getTotalFare());

			// If it's a same return segment modification
			if (isModifyProrate) {
				cancelOrModifyOndFarePercentage = AppSysParamsUtil.getModifyONDFarePercentage();
			} else {
				cancelOrModifyOndFarePercentage = AppSysParamsUtil.getCancelONDFarePercentage();
			}

			BigDecimal adjCancelOndFare = AccelAeroCalculator.divide(
					AccelAeroCalculator.multiply(totalFare, cancelOrModifyOndFarePercentage), 100);
			BigDecimal adjNonCancelOndFare = AccelAeroCalculator.subtract(totalFare, adjCancelOndFare);

			// If mapPnrSegIdsAndChanges not null
			// it means we need to generate the segment fare changes information
			if (mapPnrSegIdsAndChanges != null) {
				// Getting Target Fare Changes information
				String targetChanges = getFareChanges(reservationPaxFareTarget.getTotalFare(),
						reservationPaxFareEffected.getTotalFare(), cancelOrModifyOndFarePercentage, adjCancelOndFare);

				// Getting Inverse Fare Changes information
				String inverseChanges = getFareChanges(reservationPaxFareEffected.getTotalFare(),
						reservationPaxFareTarget.getTotalFare(), (100 - cancelOrModifyOndFarePercentage), adjNonCancelOndFare);

				Collection<Integer> colSetPnrSegmentIds = new HashSet<Integer>();
				colSetPnrSegmentIds.addAll(pnrSegmentIds);

				mapPnrSegIdsAndChanges.put(colSetPnrSegmentIds, targetChanges);
				mapPnrSegIdsAndChanges.put(colSetInversePnrSegIds, inverseChanges);
			}

			CancellationUtils.adjustOndFareAmount(reservationPaxFareTarget, adjCancelOndFare);
			CancellationUtils.adjustOndFareAmount(reservationPaxFareEffected, adjNonCancelOndFare);
		}

	}

	/**
	 * Return fare changes
	 * 
	 * @param reservationPaxFareOriginal
	 * @param reservationPaxFareEffected
	 * @param farePercentage
	 * @param newFareAmount
	 * @return
	 */
	private static String getFareChanges(BigDecimal originalFare, BigDecimal effectedFare, int farePercentage,
			BigDecimal newFareAmount) {
		StringBuffer changes = new StringBuffer();

		changes.append(" Fare : ").append(originalFare).append(", (").append(originalFare).append(" + ").append(effectedFare)
				.append(") x ").append(farePercentage).append("%").append(" = ").append(newFareAmount);

		return changes.toString();
	}

	/**
	 * Finds out if it is a Modify Prorate
	 * 
	 * @param pnrSegmentIds
	 * @param colSetInversePnrSegIds
	 * @param orderdNewFlgSegIds
	 * @param colReservationSegmentDTO
	 * @return
	 */
	public static boolean isModifyProrate(Collection<Integer> pnrSegmentIds, Collection<Integer> colSetInversePnrSegIds,
			Collection<Integer> orderdNewFlgSegIds, Collection<ReservationSegmentDTO> colReservationSegmentDTO) {

		// Doing a precheck to optimize the check
		if (orderdNewFlgSegIds != null && orderdNewFlgSegIds.size() > 0 && colSetInversePnrSegIds.size() > 0) {
			Map<Integer, String> orderedSegmentCodeMap = new TreeMap<Integer, String>();
			ReservationSegmentDTO reservationSegmentDTO;
			for (Iterator<ReservationSegmentDTO> itReservationSegmentDTO = colReservationSegmentDTO.iterator(); itReservationSegmentDTO
					.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

				if (pnrSegmentIds.contains(reservationSegmentDTO.getPnrSegId())) {
					orderedSegmentCodeMap.put(reservationSegmentDTO.getSegmentSeq(), reservationSegmentDTO.getSegmentCode());
				}
			}

			// Picking the Current (Cancellation/Modification) Ond Code
			String currentOndCode = ReservationApiUtils.getOndCode(orderedSegmentCodeMap.values());
			Map<Integer, String> mapFlgSegIdAndSegCode = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
					.getSegmentCodes(orderdNewFlgSegIds);
			Integer flightSegId;
			Collection<String> colNewOndCodes = new ArrayList<String>();

			for (Iterator<Integer> iter = orderdNewFlgSegIds.iterator(); iter.hasNext();) {
				flightSegId = iter.next();
				colNewOndCodes.add(mapFlgSegIdAndSegCode.get(flightSegId));
			}

			// Picking the Newly creating segments Ond Code
			String newOndCode = ReservationApiUtils.getOndCode(colNewOndCodes);

			// If the ond code matches or part of the ond code matches agreed
			// with Ahamed/Nasly 5:46 PM 6/6/2007 (Nili)
			if (currentOndCode.equals(newOndCode) || newOndCode.indexOf(currentOndCode) > 0) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Charge Codes that needs to be exist even after the cancellation process takes place. Handling charge should be
	 * kept in the reservation even though the onhold reservation get's cancelled.
	 * 
	 * @param isModification
	 * @return
	 */
	public static Collection<String> getExcludeChargeCodes(Boolean isModification) {
		Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
		// FIXME Bad hard coding parameterize in DB or configs
		String chargeCode = BeanUtils.nullHandler(externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE.name()));
		// String inschargeCode = BeanUtils.nullHandler(externalChargesMap.get(EXTERNAL_CHARGES.INSURANCE.name()));
		Collection<String> colChargeCodes = new ArrayList<String>();

		if (chargeCode.length() > 0) {
			colChargeCodes.add(chargeCode);
			// colChargeCodes.add(inschargeCode);
		}
		if (isModification != null && isModification) {
			chargeCode = BeanUtils.nullHandler(externalChargesMap.get(EXTERNAL_CHARGES.FLEXI_CHARGES.name()));
			if (chargeCode.length() > 0) {
				colChargeCodes.add(chargeCode);
			}
		}

		return colChargeCodes;
	}

	public static Collection<String> getExchangeExcludeChargeCodes() {
		Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExchangeExcludeChargesMap();
		Map<String, String> adjMap = ReservationModuleUtils.getAirReservationConfig().getChargeAdjustmentMap();

		Collection<String> allChargeCodes = new HashSet<String>();
		if (externalChargesMap != null) {
			allChargeCodes.addAll(externalChargesMap.values());
		}

		if (adjMap != null) {
			allChargeCodes.addAll(adjMap.values());
		}

		Collection<String> colExcludeChargeCodes = new HashSet<String>();
		for (String chargeCode : allChargeCodes) {
			chargeCode = BeanUtils.nullHandler(chargeCode);
			if (chargeCode.length() > 0) {
				colExcludeChargeCodes.add(chargeCode);
			}
		}

		return colExcludeChargeCodes;
	}

	/**
	 * Returns the ond wise groups
	 * 
	 * @param requestedPnrSegmentIds
	 * @param reservation
	 * @return
	 */
	public static Collection<Collection<Integer>> getOndWiseGroups(Collection<Integer> requestedPnrSegmentIds,
			Reservation reservation) {

		Map<Integer, Collection<ReservationSegmentDTO>> mapGrpSegments = ReservationApiUtils
				.getSegSeqOrderedGrpSegments(reservation.getSegmentsView());
		Collection<Collection<Integer>> colSegmentsGroupedOndWise = new ArrayList<Collection<Integer>>();

		for (Iterator<Integer> itOndGrpSegmentsOndKeys = mapGrpSegments.keySet().iterator(); itOndGrpSegmentsOndKeys.hasNext();) {
			Integer ondGroupId = (Integer) itOndGrpSegmentsOndKeys.next();
			Collection<ReservationSegmentDTO> colSegments = (Collection<ReservationSegmentDTO>) mapGrpSegments.get(ondGroupId);

			Collection<Integer> colPnrSegIds = new ArrayList<Integer>();
			for (Iterator<ReservationSegmentDTO> itColSegments = colSegments.iterator(); itColSegments.hasNext();) {
				ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) itColSegments.next();
				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())
						&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(reservationSegmentDTO
								.getSubStatus())) {
					continue;
				}
				colPnrSegIds.add(reservationSegmentDTO.getPnrSegId());
			}

			if (requestedPnrSegmentIds.containsAll(colPnrSegIds)) {
				colSegmentsGroupedOndWise.add(colPnrSegIds);
			}
		}

		return colSegmentsGroupedOndWise;
	}

	public static Collection<FareOndGroupTO> getOndGroupTOList(Collection<Integer> requestedPnrSegIds, Reservation reservation) {

		Map<Integer, FareOndGroupTO> fareGroupMap = new HashMap<Integer, FareOndGroupTO>();

		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (requestedPnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
				Integer ondGroupId = reservationSegmentDTO.getFareGroupId();
				if (!fareGroupMap.containsKey(ondGroupId)) {
					fareGroupMap.put(ondGroupId, new FareOndGroupTO(ondGroupId));
				}

				FareOndGroupTO fareGroupTO = fareGroupMap.get(ondGroupId);
				fareGroupTO.getPnrSegIds().add(reservationSegmentDTO.getPnrSegId());
				fareGroupTO.getFlightSegIds().add(reservationSegmentDTO.getFlightSegId());
			}
		}
		return fareGroupMap.values();
	}

	/**
	 * Return all the pnr seg ids for the given pnr seg id's fare ond groups
	 * 
	 * @param requestedPnrSegmentIds
	 * @param reservation
	 * @return
	 */
	public static Collection<Integer> getAllPnrSegIdsForSameOndGroup(Collection<Integer> requestedPnrSegmentIds,
			Reservation reservation) {
		Map<Integer, Collection<Integer>> ondGroupWisePnrSegs = getOndGroupWisePnrSegIds(requestedPnrSegmentIds, reservation);
		Collection<Integer> allPnrSegIds = new ArrayList<Integer>();
		for (Integer ondGroupId : ondGroupWisePnrSegs.keySet()) {
			allPnrSegIds.addAll(ondGroupWisePnrSegs.get(ondGroupId));
		}

		return allPnrSegIds;
	}

	public static Collection<Integer> getInversePnrSegIdsForMultipleOnds(Collection<Integer> requestedPnrSegmentIds,
			Reservation reservation) throws ModuleException {
		Map<Collection<Integer>, Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds = ReservationApiUtils.getInverseSegments(
				reservation, false);
		if (requestedPnrSegmentIds.size() > 0) {
			Collection<Integer> invPnrSegIds = new ArrayList<Integer>();

			Map<Integer, Collection<Integer>> ondWisePnrsSegs = getOndGroupWisePnrSegIds(requestedPnrSegmentIds, reservation);
			for (Integer ondGroupId : ondWisePnrsSegs.keySet()) {
				if (mapSetPSegIdsAndSetInvPSegIds.containsKey(ondWisePnrsSegs.get(ondGroupId))) {
					invPnrSegIds.addAll(mapSetPSegIdsAndSetInvPSegIds.get(ondWisePnrsSegs.get(ondGroupId)));
				} else {
					throw new ModuleException("airreservations.arg.invalidOnd");
				}
			}
			return invPnrSegIds;
		} else {
			return new ArrayList<Integer>();
		}

	}

	public static Map<Integer, Collection<Integer>> getOndGroupWisePnrSegIds(Collection<Integer> requestedPnrSegmentIds,
			Reservation reservation) {
		Map<Integer, Collection<Integer>> ondGroupWisePnrSegs = new HashMap<Integer, Collection<Integer>>();
		Set<Integer> requestedOndGroups = new HashSet<Integer>();
		for (ReservationSegment resSeg : reservation.getSegments()) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())
					&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(resSeg.getSubStatus())) {
				continue;
			}
			
			if (requestedPnrSegmentIds.contains(resSeg.getPnrSegId())) {
				requestedOndGroups.add(resSeg.getOndGroupId());
			}
			if (!ondGroupWisePnrSegs.containsKey(resSeg.getOndGroupId())) {
				ondGroupWisePnrSegs.put(resSeg.getOndGroupId(), new HashSet<Integer>());
			}
			ondGroupWisePnrSegs.get(resSeg.getOndGroupId()).add(resSeg.getPnrSegId());
		}
		ondGroupWisePnrSegs.keySet().retainAll(requestedOndGroups);
		return ondGroupWisePnrSegs;
	}

	public static Collection<Integer>
			getFltSegIdsForPnrSegIds(Collection<Integer> requestedPnrSegmentIds, Reservation reservation) {
		Collection<Integer> fltSegIds = new ArrayList<Integer>();
		for (ReservationSegment resSeg : reservation.getSegments()) {
			if (requestedPnrSegmentIds.contains(resSeg.getPnrSegId())) {
				fltSegIds.add(resSeg.getFlightSegId());
			}
		}
		return fltSegIds;
	}

	/**
	 * Returns the first ond(s) pnr segment ids
	 * 
	 * @param colOndGroups
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Integer> getFirstOndsPnrSegIds(Collection<Collection<Integer>> colOndGroups) throws ModuleException {

		if (colOndGroups.size() == 0) {
			throw new ModuleException("airreservations.arg.invalidOnd");
		}

		return BeanUtils.getFirstElement(colOndGroups);
	}

	/**
	 * Is Open return inverse segments
	 * 
	 * @param reservation
	 * @param colSetInversePnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isOpenReturnInverseSegments(Reservation reservation, Collection<Integer> colSetInversePnrSegIds)
			throws ModuleException {

		if (colSetInversePnrSegIds != null && colSetInversePnrSegIds.size() > 0) {
			for (ReservationSegmentDTO reservationSegmentDTO : (Collection<ReservationSegmentDTO>) reservation.getSegmentsView()) {
				if (colSetInversePnrSegIds.contains(reservationSegmentDTO.getPnrSegId())
						&& reservationSegmentDTO.isOpenReturnSegment()) {
					return true;
				}
			}
		}

		return false;
	}

	public static boolean[] getApplyFlexibilitiesToOndFlags(ReservationPaxFare reservationPaxFare) throws ModuleException {
		boolean[] bufferFlags = new boolean[FLEXI_RULE_FLEXIBILITY_TYPE.values().length];
		boolean hasModFlexibility = false;
		boolean hasCnxFlexibility = false;

		Collection<Integer> pnrSegIds = new ArrayList<Integer>();

		Reservation reservation = reservationPaxFare.getReservationPax().getReservation();
		Iterator<ReservationPaxFareSegment> iter = reservationPaxFare.getPaxFareSegments().iterator();
		while (iter.hasNext()) {
			ReservationPaxFareSegment seg = (ReservationPaxFareSegment) iter.next();
			pnrSegIds.add(seg.getPnrSegId());
		}

		List<ReservationSegmentDTO> selectedSegments = new ArrayList<ReservationSegmentDTO>(
				ReservationApiUtils.getReservationSegmentDTOs(reservation.getSegmentsView(), pnrSegIds));
		Collections.sort(selectedSegments);
		LinkedList<ReservationSegmentDTO> fareGroupSegments = new LinkedList<ReservationSegmentDTO>();
		for (ReservationSegmentDTO resSeg : selectedSegments) {
			if (fareGroupSegments.size() == 0) {
				fareGroupSegments.add(resSeg);
			} else if (fareGroupSegments.getLast().getFareGroupId().intValue() != resSeg.getFareGroupId().intValue()) {
				fareGroupSegments.add(resSeg);
			}
		}
		HashMap<String, ReservationPaxOndFlexibilityDTO> ondFlexibilities = ReservationApiUtils.getRemainingFlexibilitiesForOnd(
				reservation, pnrSegIds);
		ReservationPaxOndFlexibilityDTO flexibilityDTO = null;

		Date depDateZulu = null;
		Date currentDateZulu = CalendarUtil.getCurrentSystemTimeInZulu();

		for (ReservationSegmentDTO resSeg : fareGroupSegments) {
			depDateZulu = resSeg.getZuluDepartureDate();
			long timeToDeparture = depDateZulu.getTime() - currentDateZulu.getTime();

			if (ondFlexibilities != null && ondFlexibilities.size() > 0) {
				// Check for Flexi modify buffer
				if (ondFlexibilities.containsKey(FLEXI_RULE_FLEXIBILITY_TYPE.MODIFY_OND.name())) {
					flexibilityDTO = (ReservationPaxOndFlexibilityDTO) ondFlexibilities
							.get(FLEXI_RULE_FLEXIBILITY_TYPE.MODIFY_OND.name());
					if (flexibilityDTO != null) {
						long flexiModifyCutOverBufferInMillis = flexibilityDTO.getCutOverBufferInMins() * 60 * 1000;
						if (timeToDeparture < flexiModifyCutOverBufferInMillis)
							hasModFlexibility = false;
						else
							hasModFlexibility = true;

						hasModFlexibility = hasModFlexibility && flexibilityDTO.getAvailableCount() > 0;
					}
				}

				// Check for Flexi Cancel buffer
				if (ondFlexibilities.containsKey(FLEXI_RULE_FLEXIBILITY_TYPE.CANCEL_OND.name())) {
					flexibilityDTO = (ReservationPaxOndFlexibilityDTO) ondFlexibilities
							.get(FLEXI_RULE_FLEXIBILITY_TYPE.CANCEL_OND.name());
					if (flexibilityDTO != null) {
						long flexiCancelCutOverBufferInMillis = flexibilityDTO.getCutOverBufferInMins() * 60 * 1000;
						if (timeToDeparture < flexiCancelCutOverBufferInMillis)
							hasCnxFlexibility = false;
						else
							hasCnxFlexibility = true;

						hasCnxFlexibility = hasCnxFlexibility && flexibilityDTO.getAvailableCount() > 0;
					}
				}

			}
		}
		bufferFlags[0] = hasModFlexibility;
		bufferFlags[1] = hasCnxFlexibility;

		return bufferFlags;
	}

	/**
	 * Returns true if a reservation is ready to be made void
	 * 
	 * Note: This method requires the Segments View to be loaded with the reservation
	 * 
	 * @author Bhagya
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isVoidReservation(Reservation reservation) throws ModuleException {

		if (AppSysParamsUtil.isVoidReservationsEnabled()) {
			log.debug("Void Reservations are enabled.");

			// reservation should not have any cancelled e-tickets if it's to be marked as void
			if (hasCancelledSegments(reservation)) {
				return false;
			}

			Date reservationTime = reservation.getZuluBookingTimestamp();

			if (reservation.getSegmentsView() == null || reservation.getSegmentsView().isEmpty()) {
				throw new ModuleException("airreservations.arg.invalidLoad");
			}

			Date flightDepartureTime = getFirstSegmentZuluDepartureDate(reservation.getSegmentsView());

			if (flightDepartureTime == null) {
				throw new ModuleException("airreservations.arg.invalidLoad");
			}

			Date currentSystemTimeInZulu = CalendarUtil.getCurrentSystemTimeInZulu();

			// add the offset in hours from the booking date
			// booking date + number of hours after booking for voidability
			Date bookingVoidExpiration = CalendarUtil.addHours(reservationTime,
					AppSysParamsUtil.getNumberOfHoursAfterBookingForVoidReservations());

			// flightDepartureVoidExpiration : flight time - number of hours before departure
			// -------------------------------------------------------------------
			Date flightDepartureVoidExpiration = CalendarUtil.addHours(flightDepartureTime,
					AppSysParamsUtil.getNumberOfHoursBeforeDepartureForVoidReservations() * -1);

			if (log.isDebugEnabled()) {
				log.debug("Checking reservation with PNR " + reservation.getPnr() + " to be marked as void....");
				log.debug("Reservation Created on (Zulu): " + CalendarUtil.getSQLToDateInputString(reservationTime));
				log.debug("Current Time (Zulu): " + CalendarUtil.getSQLToDateInputString(currentSystemTimeInZulu));
				log.debug("First flight departure time (Zulu): " + CalendarUtil.getSQLToDateInputString(flightDepartureTime));
				log.debug("Booking Voidability Expires on (Zulu): " + CalendarUtil.getSQLToDateInputString(bookingVoidExpiration));
				log.debug("Fight Departure Reservation lockdown starts on : "
						+ CalendarUtil.getSQLToDateInputString(flightDepartureVoidExpiration));
			}

			if (CalendarUtil.isLessThan(currentSystemTimeInZulu, flightDepartureVoidExpiration)) {
				log.debug("currentSystemTimeInZulu < flightDepartureVoidExpiration");
				// reservation is voidable
				if (CalendarUtil.isLessThan(currentSystemTimeInZulu, bookingVoidExpiration)) {
					log.debug("currentSystemTimeInZulu <  bookingVoidExpiration");
					log.debug("Marking reservation to be ready to be made void.");

					return true;
				}
			}
		}

		log.debug("Reservation cannot be made void.");
		return false;
	}

	/**
	 * Returns true if a reservation has cancelled e-tickets, this is used to identify reservations to stop them from
	 * being marked as void.
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	private static boolean hasCancelledSegments(Reservation reservation) {
		int cancelledFlightCount = 0;
		int exSubStausCount = 0;
		boolean isFlownReservation = false;
		for (Entry<Integer, Collection<Integer>> entry : getPaxWiseFlownPnrSegments(reservation).entrySet()) {
			if (entry.getValue().size() > 0) {
				isFlownReservation = true;
				break;
			}
		}

		if (!isFlownReservation) {
			for (ReservationSegment reservationSegment : (Collection<ReservationSegment>) reservation.getSegments()) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
					if (ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(reservationSegment
							.getSubStatus())) {
						exSubStausCount++;
					}
					cancelledFlightCount++;
				}
			}
			if (cancelledFlightCount == exSubStausCount) {
				return false;
			}
		}
		log.debug("Cancelled segments may have charges for the cancellation.So, cannot mark as VOID");
		return true;
	}

	/**
	 * Method to sort ReservationSegmentDTO by departure date
	 * 
	 * @param setReservationSegments
	 *            A set of ReservationSegmentDTO
	 * @return An array of ReservationSegmentDTO
	 */
	private static Date getFirstSegmentZuluDepartureDate(Collection<ReservationSegmentDTO> setReservationSegments) {
		List<ReservationSegmentDTO> reservationSegments = new ArrayList<ReservationSegmentDTO>(setReservationSegments);
		// sort the reservation segments
		Collections.sort(reservationSegments);
		return reservationSegments.get(0).getZuluDepartureDate();
	}

	/**
	 * Returns any passed in segment id(s) belongs to any canceled flight
	 * 
	 * @param pnrSegIds
	 * @param reservation
	 * @return
	 */
	public static boolean isAnyCancelFlightsExists(ReservationPaxFare reservationPaxFare) {

		// This is due to crazy airarabia requirement. They charge for bookings on flight cancellation too.
		if (AppSysParamsUtil.isApplyBookingCnxModChargesForCancelFlights()) {
			return false;
		}

		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		for (ReservationPaxFareSegment reservationPaxFareSegment : (Collection<ReservationPaxFareSegment>) reservationPaxFare
				.getPaxFareSegments()) {
			pnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
		}

		Reservation reservation = reservationPaxFare.getReservationPax().getReservation();

		for (ReservationSegmentDTO reservationSegmentDTO : (Collection<ReservationSegmentDTO>) reservation.getSegmentsView()) {
			if (pnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
				if ((FlightStatusEnum.CANCELLED.getCode().equals(reservationSegmentDTO.getFlightStatus())) &&
						reservationSegmentDTO.getOpenRtConfirmBeforeZulu() == null){
					return true;
				}
			}
		}

		return false;
	}

	public static boolean isSameFlightsModification(Reservation reservation, Collection<Integer> pnrSegmentIds,
			Collection<Integer> orderdNewFlgSegIds, boolean isModifyChgOperation) {

		if (isModifyChgOperation) {

			// This is due to crazy airarabia requirement. They charge for same flight modifications too.
			if (AppSysParamsUtil.isApplyModChargeForSameFlightBookingModifications()) {
				return false;
			}
			return isSameFlightModification(reservation, pnrSegmentIds, orderdNewFlgSegIds);
		}

		return false;
	}

	public static boolean isSameFlightModification(Reservation reservation, Collection<Integer> pnrSegmentIds,
			Collection<Integer> orderdNewFlgSegIds) {
		Collection<Integer> oldFltSegIds = new HashSet<Integer>();
		for (ReservationSegment reservationSegment : (Collection<ReservationSegment>) reservation.getSegments()) {
			if (pnrSegmentIds.contains(reservationSegment.getPnrSegId())) {
				oldFltSegIds.add(reservationSegment.getFlightSegId());
			}
		}

		orderdNewFlgSegIds = new HashSet<Integer>(orderdNewFlgSegIds);

		if (orderdNewFlgSegIds.size() == oldFltSegIds.size() && orderdNewFlgSegIds.containsAll(oldFltSegIds)) {
			return true;
		}
		return false;
	}

	public static boolean applyCancelChargeForFlown(boolean applyCnxCharge, boolean flownExists) {

		if (AppSysParamsUtil.isSkipCancelChargeForFlown()) {
			return (applyCnxCharge && !flownExists); // if flown exists make cnx chg skip
		}
		return applyCnxCharge;
	}

	/**
	 * Skip cancellation charge for no show segment when app param is enabled
	 * 
	 * @param showCnxCharge
	 * @param reservationPaxFare
	 * @return
	 */
	public static boolean applyCancelChargeForNoShowSegment(boolean applyCnxCharge, ReservationPaxFare reservationPaxFare) {

		boolean skipNoShowSegCnxCharge = AppSysParamsUtil.isSkipNoShowSegmentCancelCharge();

		if (skipNoShowSegCnxCharge && applyCnxCharge) {
			Iterator<ReservationPaxFareSegment> paxFareSegItr = reservationPaxFare.getPaxFareSegments().iterator();
			ReservationPaxFareSegment reservationPaxFareSegment = null;
			while (paxFareSegItr.hasNext()) {
				reservationPaxFareSegment = paxFareSegItr.next();

				if (ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE.equals(reservationPaxFareSegment.getStatus())) {
					return false;
				}

			}
		}

		return applyCnxCharge;
	}

	public static Map<Integer, Collection<Integer>> getPaxWiseFlownPnrSegments(Reservation reservation) {
		if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.DATE) {
			Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
			ReservationSegmentDTO reservationSegmentDTO;
			Date currentDate = new Date();
			Collection<Integer> colFlownPnrSegmentIds = new HashSet<Integer>();
			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = itReservationSegmentDTO.next();
				if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					if (currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) > 0) {
						colFlownPnrSegmentIds.add(reservationSegmentDTO.getPnrSegId());
					}
				}
			}
			Map<Integer, Collection<Integer>> map = new HashMap<Integer, Collection<Integer>>();
			for (ReservationPax pax : reservation.getPassengers()) {
				map.put(pax.getPnrPaxId(), colFlownPnrSegmentIds);
			}
			return map;

		} else {
			List<Integer> cnfPnrSegs = new ArrayList<Integer>();

			for (ReservationSegment resSeg : reservation.getSegments()) {
				if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					cnfPnrSegs.add(resSeg.getPnrSegId());
				}
			}
			boolean skipOnHold = false;
			if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET
					&& reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				skipOnHold = true;
			}

			if (!skipOnHold && cnfPnrSegs.size() > 0) {
				Map<Integer, Collection<Integer>> paxFlownPnrSegMap = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPaxWiseFlownSegments(cnfPnrSegs);
				if (AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
					Map<Integer, Collection<Integer>> paxFlownPnrSegMapET = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
							.getPaxWiseBoardedCheckedInSegments(cnfPnrSegs);
					for (Integer colFlownSeg : paxFlownPnrSegMap.keySet()) {
						if (paxFlownPnrSegMap.get(colFlownSeg).isEmpty() && paxFlownPnrSegMapET.get(colFlownSeg) != null
								&& paxFlownPnrSegMap.get(colFlownSeg) != paxFlownPnrSegMapET.get(colFlownSeg)) {

							paxFlownPnrSegMap.get(colFlownSeg).addAll(paxFlownPnrSegMapET.get(colFlownSeg));
						}
					}
				}
				if (!paxFlownPnrSegMap.isEmpty()) {
					
					return paxFlownPnrSegMap;
				}
			}
			Map<Integer, Collection<Integer>> map = new HashMap<Integer, Collection<Integer>>();
			for (ReservationPax pax : reservation.getPassengers()) {
				map.put(pax.getPnrPaxId(), new ArrayList<Integer>());
			}
			return map;
		}
	}

	public static BigDecimal reValidateCreditAmount(BigDecimal creditAmount, BigDecimal identifiedCharge) {

		creditAmount = AccelAeroCalculator.subtract(creditAmount, identifiedCharge);
		if (creditAmount.compareTo(BigDecimal.ZERO) < 0) {
			creditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		return creditAmount;
	}

	/**
	 * Set Pnr Pax Fare Total Amounts
	 * 
	 * @param reservationPaxFare
	 */
	public static void setPnrPaxFareTotalAmounts(ReservationPaxFare reservationPaxFare) {
		BigDecimal[] totalCharges = ReservationApiUtils.getChargeTypeAmounts();

		Iterator<ReservationPaxOndCharge> itCharges = reservationPaxFare.getCharges().iterator();
		ReservationPaxOndCharge reservationPaxOndCharge;

		while (itCharges.hasNext()) {
			reservationPaxOndCharge = itCharges.next();
			ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
					reservationPaxOndCharge.getAmount(), totalCharges, reservationPaxOndCharge.getDiscount(), reservationPaxOndCharge.getAdjustment());
		}

		reservationPaxFare.setTotalChargeAmounts(totalCharges);
	}

}