/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * Reservation related service tax captured as external charges business logic implementation
 * 
 * @author rumesh
 */
public class ExternalServiceTaxChargesBL {

	private static List<EXTERNAL_CHARGES> taxExternalCharges = Arrays.asList(EXTERNAL_CHARGES.JN_ANCI, EXTERNAL_CHARGES.JN_OTHER,
			EXTERNAL_CHARGES.SERVICE_TAX);

	/**
	 * Hide the constructor
	 */
	private ExternalServiceTaxChargesBL() {

	}

	/**
	 * Reflect external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 *            TODO
	 * @param reservationDiscountDTO
	 * @param externalChgDTO
	 * @throws ModuleException
	 */
	public static void reflectExternalChgsForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		// Apply these changes for the reservation
		applyExternalChargesForAFreshBooking(reservation, credentialsDTO, paxDiscInfo, reservationDiscountDTO);
	}

	/**
	 * Reflect external charges for an open return confirm segment
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param passengerRevenueMap
	 * @throws ModuleException
	 */
	public static void reflectExternalChgForAnOpenReturnConfirmSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO) throws ModuleException {
		// Apply these changes for the reservation
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForANewSegment(reservation,
				pnrPaxIdAndPayments, credentialsDTO, null);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, pnrPaxIdAndPayments);
	}

	/**
	 * Reflect external charges for a new segment
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param passengerRevenueMap
	 * @param chgTxnGen
	 * @throws ModuleException
	 */
	public static void reflectExternalChgForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Map<Integer, RevenueDTO> passengerRevenueMap,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen) throws ModuleException {
		// Apply these changes for the reservation
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForANewSegment(reservation,
				pnrPaxIdAndPayments, credentialsDTO, chgTxnGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, pnrPaxIdAndPayments);

		// Apply these changes to the passenger revenue map
		addExternalChgsForRevenueMap(mapPnrPaxIdAdjustments, passengerRevenueMap);
	}

	/**
	 * Reflect external charges for a payment booking
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	public static void reflectExternalChgForAPaymentBooking(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPayment, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		// Apply these changes for the reservation
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForAPaymentBooking(reservation,
				passengerPayment, credentialsDTO, chgTnxGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, passengerPayment);
	}

	/**
	 * Apply external charges for a new segment
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param chgTxnGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen)
			throws ModuleException {
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());

				for (EXTERNAL_CHARGES taxExternalChg : taxExternalCharges) {
					ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(taxExternalChg);

					if (ccExternalChgs.size() > 0) {
						for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
							if ((externalChgDTO.getExternalChargesEnum() != EXTERNAL_CHARGES.SERVICE_TAX)
									|| (externalChgDTO.getExternalChargesEnum() == EXTERNAL_CHARGES.SERVICE_TAX
											&& !StringUtil.isNullOrEmpty(externalChgDTO.getChargeCode()))) {
								ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) externalChgDTO;

								Integer flightSegId = FlightRefNumberUtil
										.getSegmentIdFromFlightRPH(serviceTaxExtChgDTO.getFlightRefNumber());
								reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(reservationPax.getPnrPaxFares(),
										flightSegId, false);

								String taxAppliedCategory = null;
								if (EXTERNAL_CHARGES.SERVICE_TAX == externalChgDTO.getExternalChargesEnum()) {
									if (serviceTaxExtChgDTO.isTicketingRevenue()) {
										taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY;
									} else {
										taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_NON_TICKETING_CATEGORY;
									}
								}

								PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
										serviceTaxExtChgDTO.getNonTaxableAmount(), serviceTaxExtChgDTO.getTaxableAmount(),
										taxAppliedCategory, serviceTaxExtChgDTO.getTaxDepositCountryCode(),
										serviceTaxExtChgDTO.getTaxDepositStateCode());

								ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
										.captureReservationPaxOndCharge(serviceTaxExtChgDTO.getAmount(), null,
												serviceTaxExtChgDTO.getChgRateId(), serviceTaxExtChgDTO.getChgGrpCode(),
												reservationPaxFare, credentialsDTO, false, null, null, paxOndChargeDTO,
												chgTxnGen.getTnxSequence(reservationPax.getPnrPaxId()));

								lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(reservationPax.getPnrPaxId());

								if (lstReservationPaxOndCharge == null) {
									lstReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();
									lstReservationPaxOndCharge.add(reservationPaxOndCharge);

									mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), lstReservationPaxOndCharge);
								} else {
									lstReservationPaxOndCharge.add(reservationPaxOndCharge);
								}

								updateExist = true;
							}
						}
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	/**
	 * Applies the external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 *            TODO
	 * @param reservationDiscountDTO
	 * @param colExternalChgDTO
	 * @throws ModuleException
	 */
	private static void applyExternalChargesForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			for (EXTERNAL_CHARGES taxExternalChg : taxExternalCharges) {

				if (!ReservationApiUtils.isInfantType(reservationPax)
						|| (EXTERNAL_CHARGES.SERVICE_TAX == taxExternalChg && ReservationApiUtils.isInfantType(reservationPax))) {

					paymentAssembler = (PaymentAssembler) reservationPax.getPayment();
					Collection<DiscountChargeTO> discountChargeTOs = null;
					if (reservationDiscountDTO != null) {
						discountChargeTOs = reservationDiscountDTO.getPaxDiscountChargeTOs(reservationPax.getPaxSequence());
					}

					ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(taxExternalChg);

					if (ccExternalChgs.size() > 0) {
						for (ExternalChgDTO externalChgDTO : ccExternalChgs) {

							if ((externalChgDTO.getExternalChargesEnum() != EXTERNAL_CHARGES.SERVICE_TAX)
									|| (externalChgDTO.getExternalChargesEnum() == EXTERNAL_CHARGES.SERVICE_TAX
											&& !StringUtil.isNullOrEmpty(externalChgDTO.getChargeCode()))) {
								ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) externalChgDTO;
								Integer flightSegId = FlightRefNumberUtil
										.getSegmentIdFromFlightRPH(serviceTaxExtChgDTO.getFlightRefNumber());
								reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(reservationPax.getPnrPaxFares(),
										flightSegId, false);

								String taxAppliedCategory = null;
								if (EXTERNAL_CHARGES.SERVICE_TAX == externalChgDTO.getExternalChargesEnum()) {
									if (serviceTaxExtChgDTO.isTicketingRevenue()) {
										taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY;
									} else {
										taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_NON_TICKETING_CATEGORY;
									}
								}

								PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
										serviceTaxExtChgDTO.getNonTaxableAmount(), serviceTaxExtChgDTO.getTaxableAmount(),
										taxAppliedCategory, serviceTaxExtChgDTO.getTaxDepositCountryCode(),
										serviceTaxExtChgDTO.getTaxDepositStateCode());

								ReservationApiUtils.updatePaxOndExternalChargeDiscountInfo(paxDiscInfo, externalChgDTO,
										discountChargeTOs, reservationPax.getPaxSequence());

								ReservationCoreUtils.captureReservationPaxOndCharge(serviceTaxExtChgDTO.getAmount(), null,
										serviceTaxExtChgDTO.getChgRateId(), serviceTaxExtChgDTO.getChgGrpCode(),
										reservationPaxFare, credentialsDTO, false, paxDiscInfo, null, paxOndChargeDTO, 0);
								updateExist = true;
							}

						}
					}
				}

			}

		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}
	}

	/**
	 * Apply external charges for a payment booking
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForAPaymentBooking(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPayment, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();
		Map<Integer, ReservationSegmentDTO> mapPnrSegIdAndReservationSegDTO = ExternalGenericChargesBL
				.getSegmentDetailInformation(reservation.getPnr());
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<Integer> colActiveONDPnrPaxFareIds;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;
		boolean skipFlownCheck = false;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (passengerPayment.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(reservationPax.getPnrPaxId());

				List<ReservationPaxOndCharge> arrChargePerPax = null;
				for (EXTERNAL_CHARGES taxExternalChg : taxExternalCharges) {
					skipFlownCheck = (taxExternalChg == EXTERNAL_CHARGES.JN_ANCI || taxExternalChg == EXTERNAL_CHARGES.JN_OTHER)
							? true
							: false;
					ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(taxExternalChg);

					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
						ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) externalChgDTO;
						Integer flightSegId = FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(serviceTaxExtChgDTO.getFlightRefNumber());
						colActiveONDPnrPaxFareIds = ExternalGenericChargesBL.getApplicableONDPnrPaxFareIds(currentDate,
								reservationPax.getPnrPaxFares(), mapPnrSegIdAndReservationSegDTO, reservationPax.geteTickets(),
								skipFlownCheck);

						reservationPaxFare = getReservationPaxFare(reservationPax.getPnrPaxFares(), flightSegId);
						// Only adding if it's an active OND
						if (colActiveONDPnrPaxFareIds.contains(reservationPaxFare.getPnrPaxFareId())) {

							String taxAppliedCategory = null;
							if (EXTERNAL_CHARGES.SERVICE_TAX == externalChgDTO.getExternalChargesEnum()) {
								if (serviceTaxExtChgDTO.isTicketingRevenue()) {
									taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY;
								} else {
									taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_NON_TICKETING_CATEGORY;
								}
							}

							PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
									serviceTaxExtChgDTO.getNonTaxableAmount(), serviceTaxExtChgDTO.getTaxableAmount(),
									taxAppliedCategory, serviceTaxExtChgDTO.getTaxDepositCountryCode(),
									serviceTaxExtChgDTO.getTaxDepositStateCode());

							ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(
									serviceTaxExtChgDTO.getAmount(), null, externalChgDTO.getChgRateId(),
									externalChgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, null, null,
									paxOndChargeDTO, chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));
							if (arrChargePerPax == null) {
								arrChargePerPax = new ArrayList<ReservationPaxOndCharge>();
							}
							arrChargePerPax.add(reservationPaxOndCharge);
							updateExist = true;
						}
					}
				}
				mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), arrChargePerPax);

			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	/**
	 * Add external charges for payment assembler
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerPayment
	 */
	private static void addExternalChgsForPaymentAssembler(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, PaymentAssembler> passengerPayment) {
		Iterator<Integer> itPnrPaxIds = passengerPayment.keySet().iterator();
		PaymentAssembler paymentAssembler;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(pnrPaxId);
				lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);
				if (lstReservationPaxOndCharge != null) {
					for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
						paymentAssembler.setTotalChargeAmount(AccelAeroCalculator.add(paymentAssembler.getTotalChargeAmount(),
								reservationPaxOndCharge.getAmount()));
					}
				}
			}
		}
	}

	/**
	 * Add external charges for the revenue information
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerRevenueMap
	 */
	private static void addExternalChgsForRevenueMap(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, RevenueDTO> passengerRevenueMap) {
		Iterator<Integer> itPnrPaxIds = passengerRevenueMap.keySet().iterator();
		RevenueDTO revenueDTO;
		Integer pnrPaxId;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				revenueDTO = (RevenueDTO) passengerRevenueMap.get(pnrPaxId);
				List<ReservationPaxOndCharge> lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);
				if (lstReservationPaxOndCharge != null) {
					for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
						revenueDTO.setAddedTotal(
								AccelAeroCalculator.add(revenueDTO.getAddedTotal(), reservationPaxOndCharge.getAmount()));
						revenueDTO.getAddedCharges().add(reservationPaxOndCharge);
					}
				}
			}
		}
	}

	public static ReservationPaxFare getReservationPaxFare(Collection<ReservationPaxFare> pnrPaxFares, Integer flightSegId)
			throws ModuleException {
		Iterator<ReservationPaxFare> itReservationPaxFare = pnrPaxFares.iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				if (reservationPaxFareSegment.getSegment().getFlightSegId().intValue() == flightSegId.intValue()
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationPaxFareSegment.getSegment().getStatus())) {
					return reservationPaxFare;
				}
			}
		}

		throw new ModuleException("airreservations.seatmap.flightsegments.cannot.locate");
	}
}