package com.isa.thinair.airreservation.core.remoting.ejb;

import java.util.Collection;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.login.util.ForceLoginInvoker;

@MessageDriven(name = "RecordModificationAuditServiceMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/recordAuditQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "4800") })
public class RecordModificationAuditServiceMDB implements MessageListener {

	public void onMessage(Message message) {

		try {
			ForceLoginInvoker.defaultLogin();

			ObjectMessage objMessage = (ObjectMessage) message;
			Collection<ReservationAudit> auditList = (Collection<ReservationAudit>) objMessage.getObject();
			AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
			auditorBD.audit(auditList);
		} catch (Throwable cdaex) {
		} finally {
			ForceLoginInvoker.close();
		}
	}

}