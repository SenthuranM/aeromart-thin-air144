package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author rumesh
 * 
 */
public abstract class BaseExtraFeeCalculator implements IExtraFeeCalculator {

	protected Reservation reservation;

	protected Map<Integer, Collection<ExternalChgDTO>> paxExtraFee = new HashMap<Integer, Collection<ExternalChgDTO>>();

	protected EXTERNAL_CHARGES externalCharge;

	protected Boolean isExtraFeeApplicable = null;

	protected ExternalChgDTO externalChgDTO;

	protected boolean isCancelOndOperation = false;

	protected boolean isAddSegmentOperation = false;

	protected Integer taxApplicableFlightSegId = null;

	protected boolean isTaxApplicableFlightSegNew = false;

	protected String taxApplicableFlightRefNumber;

	protected boolean isExtraFeeApplicable(Collection<OndFareDTO> colOndFareDTOs, Map<String, String> requoteSegmentMap)
			throws ModuleException {
		if (this.isExtraFeeApplicable == null) {
			isExtraFeeApplicable = false;
			if (isAppParameterEnabled()) {
				List<ReservationSegmentDTO> sortedReservationSegmentDTOs = getSortedConfirmedSegmentList();
				ReservationSegmentDTO existingResSegDTO = getNonModifyingFirstConfirmedSegment(sortedReservationSegmentDTOs,
						requoteSegmentMap);

				if (colOndFareDTOs != null && !colOndFareDTOs.isEmpty()) {
					OndFareDTO ondFareDTO = colOndFareDTOs.iterator().next();

					List<FlightSegmentDTO> flightSegmentsDTOs = new ArrayList<FlightSegmentDTO>(
							ondFareDTO.getSegmentsMap().values());
					Collections.sort(flightSegmentsDTOs);

					FlightSegmentDTO newFltSegDto = flightSegmentsDTOs.get(0);

					String departureSegCode = null;
					if (existingResSegDTO != null && newFltSegDto != null) {
						if (existingResSegDTO.getZuluDepartureDate().after(newFltSegDto.getDepartureDateTimeZulu())) {
							departureSegCode = newFltSegDto.getSegmentCode();
							taxApplicableFlightRefNumber = FlightRefNumberUtil.composeFlightRPH(newFltSegDto);
							isTaxApplicableFlightSegNew = true;
						} else {
							departureSegCode = existingResSegDTO.getSegmentCode();
							taxApplicableFlightRefNumber = FlightRefNumberUtil.composeFlightRPH(existingResSegDTO);
						}
					} else if (existingResSegDTO == null && newFltSegDto != null) {
						departureSegCode = newFltSegDto.getSegmentCode();
						taxApplicableFlightRefNumber = FlightRefNumberUtil.composeFlightRPH(newFltSegDto);
						isTaxApplicableFlightSegNew = true;
					}

					if (departureSegCode != null) {
						isExtraFeeApplicable = ReservationModuleUtils.getChargeBD().isValidChargeExists(getChargeCode(),
								departureSegCode);
					}
				}
			}
		}
		return isExtraFeeApplicable;
	}

	private Map<Integer, Collection<ExternalChgDTO>> getPaxExtraFee() {
		return paxExtraFee;
	}

	protected void addPaxFee(Integer pnrPaxId, ExternalChgDTO extChg) {
		if (!getPaxExtraFee().containsKey(pnrPaxId)) {
			getPaxExtraFee().put(pnrPaxId, new ArrayList<>());
		}
		getPaxExtraFee().get(pnrPaxId).add(extChg);
	}

	public Map<Integer, Integer> getPaxSeqPaxIdMap() {
		Map<Integer, Integer> paxSeqPaxIdMap = new HashMap<Integer, Integer>();
		for (ReservationPax pax : reservation.getPassengers()) {
			paxSeqPaxIdMap.put(pax.getPaxSequence(), pax.getPnrPaxId());
		}

		return paxSeqPaxIdMap;
	}

	protected ExternalChgDTO getExternalChgDTO() throws ModuleException {
		if (externalChgDTO == null) {
			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(externalCharge);

			externalChgDTO = BeanUtils
					.getFirstElement(ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null).values());
		}

		return externalChgDTO;
	}

	protected BigDecimal getRatio() throws ModuleException {
		return getExternalChgDTO().getRatioValue();
	}

	protected boolean isAppParameterEnabled() {
		boolean appParamValue;
		switch (this.externalCharge) {
		case JN_ANCI:
			appParamValue = AppSysParamsUtil.isJNTaxApplicableForAnci();
			break;
		case JN_TAX:
			appParamValue = AppSysParamsUtil.isJNTaxApplyForCnxModNoShowCharges();
			break;
		case JN_OTHER:
			appParamValue = AppSysParamsUtil.isJnTaxApplicableForHandlingCCAdjustmentCharges();
			break;
		default:
			appParamValue = false;
			break;
		}
		return appParamValue;
	}

	protected String getChargeCode() {
		String chargeCode;
		switch (this.externalCharge) {
		case JN_ANCI:
			chargeCode = ChargeCodes.JN_ANCI_TAX;
			break;
		case JN_TAX:
			chargeCode = ChargeCodes.JN_MOD_CNX_TAX;
			break;
		case JN_OTHER:
			chargeCode = ChargeCodes.JN_HANDLING_CC_ADJ;
			break;
		default:
			chargeCode = "";
			break;
		}
		return chargeCode;
	}

	protected boolean isOriginalReservationTaxed() throws ModuleException {
		ReservationSegmentDTO firstResSegDTO = getFirstConfirmedSegmet();
		if (firstResSegDTO == null) {
			return false;
		} else {
			return ReservationModuleUtils.getChargeBD().isValidChargeExists(getChargeCode(), firstResSegDTO.getSegmentCode());
		}
	}

	protected ReservationSegmentDTO getFirstConfirmedSegmet() {
		List<ReservationSegmentDTO> sortedReservationSegmentDTOs = getSortedConfirmedSegmentList();
		if (sortedReservationSegmentDTOs.isEmpty()) {
			return null;
		} else {
			return sortedReservationSegmentDTOs.get(0);
		}
	}

	protected List<ReservationSegmentDTO> getSortedConfirmedSegmentList() {
		List<ReservationSegmentDTO> lstReservationSegmentDTOs = new ArrayList<ReservationSegmentDTO>();
		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {
				lstReservationSegmentDTOs.add(reservationSegmentDTO);
			}
		}

		Collections.sort(lstReservationSegmentDTOs);
		return lstReservationSegmentDTOs;
	}

	protected boolean isTaxCalculatiningToExistingSegment() {
		ReservationSegmentDTO firstResSegDTO = getFirstConfirmedSegmet();
		if (firstResSegDTO.getFlightSegId().equals(taxApplicableFlightSegId)) {
			return true;
		}
		return false;
	}

	protected Map<Integer, Integer> getPaxIdPaxSeqMap() {
		Map<Integer, Integer> paxSeqPaxIdMap = new HashMap<Integer, Integer>();
		for (ReservationPax pax : reservation.getPassengers()) {
			paxSeqPaxIdMap.put(pax.getPnrPaxId(), pax.getPaxSequence());
		}

		return paxSeqPaxIdMap;
	}

	protected ReservationSegmentDTO getNonModifyingFirstConfirmedSegment(List<ReservationSegmentDTO> colResSegmentDTO,
			Map<String, String> requoteSegmentMap) {

		Collection<String> modifyingPnrSegIds = requoteSegmentMap.values();
		for (ReservationSegmentDTO reservationSegmentDTO : colResSegmentDTO) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {
				if (modifyingPnrSegIds == null || modifyingPnrSegIds.isEmpty()) {
					return reservationSegmentDTO;
				} else {
					Set<Integer> targetFltSegIds = getTargetFlightSegmentIds(reservationSegmentDTO.getPnrSegId(),
							requoteSegmentMap);
					if (targetFltSegIds.size() == 1 && targetFltSegIds.contains(reservationSegmentDTO.getFlightSegId())) {
						return reservationSegmentDTO;
					}
				}
			}
		}

		return null;
	}

	protected Set<Integer> getTargetFlightSegmentIds(int pnrSegId, Map<String, String> requoteSegmentMap) {
		Set<Integer> targetFltSegIds = new HashSet<Integer>();

		for (Entry<String, String> requoteEntry : requoteSegmentMap.entrySet()) {
			if (requoteEntry.getValue() != null) {
				Integer targetFltSegId = Integer.parseInt(requoteEntry.getKey());
				Integer tmpPnrSegId;
				if (requoteEntry.getValue().indexOf("#") != -1) {
					String arr[] = requoteEntry.getValue().split("#");
					tmpPnrSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]);
				} else {
					tmpPnrSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(requoteEntry.getValue());
				}

				if (pnrSegId == tmpPnrSegId) {
					targetFltSegIds.add(targetFltSegId);
				}
			}
		}
		return targetFltSegIds;
	}

	public Collection<? extends ExternalChgDTO> getPaxExtraFee(Integer pnrPaxId) {
		return getPaxExtraFee().get(pnrPaxId);
	}

	public boolean hasExtraFeeFor(Integer pnrPaxId) {
		return getPaxExtraFee().containsKey(pnrPaxId);
	}

}
