package com.isa.thinair.airreservation.core.bl.ticketing;

import com.isa.thinair.airreservation.api.utils.TicketGeneratorCriteriaType;

/**
 * @author eric
 * 
 */
public class TicketGeneratorCriteria {

	private TicketGeneratorCriteriaType tktGenarateMethod;

	private String sequnceName;

	private String formCode;

	public TicketGeneratorCriteriaType getTktGenarateMethod() {
		return tktGenarateMethod;
	}

	public void setTktGenarateMethod(TicketGeneratorCriteriaType tktGenarateMethod) {
		this.tktGenarateMethod = tktGenarateMethod;
	}

	public String getSequnceName() {
		return sequnceName;
	}

	public void setSequnceName(String sequnceName) {
		this.sequnceName = sequnceName;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	@Override
	public String toString() {
		return "TicketGeneratorCriteria [tktGenarateMethod=" + tktGenarateMethod + ", sequnceName=" + sequnceName + ", formCode="
				+ formCode + "]";
	}
}
