/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.dto.ssr.SSRCountDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.segment.ModifyAssitUnit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.PENALTY_BASED;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Validation utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ValidationUtils {

	private static Log log = LogFactory.getLog(ValidationUtils.class);

	private static String PAX_VALIDATE_NAME = "PAXNAME";
	private static String PAX_VALIDATE_EMAIL = "PAXEMAIL";

	private static String YES = "y";

	/**
	 * Check Any Restricted Segment Exist or not
	 * 
	 * @param colOndFareDTO
	 * @return
	 */
	private static boolean isAnyRestrictedSegmentExist(Collection<OndFareDTO> colOndFareDTO) {
		Iterator<OndFareDTO> itColOndFareDTO = colOndFareDTO.iterator();
		OndFareDTO ondFareDTO;

		while (itColOndFareDTO.hasNext()) {
			ondFareDTO = (OndFareDTO) itColOndFareDTO.next();

			if (ValidationUtils.isRestrictedFareCategory(ondFareDTO.getFareCategoryType())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Invoke Exception Based on fare category
	 * 
	 * @param fareCategoryCode
	 * @param errorCode
	 * @throws ModuleException
	 */
	private static void invokeExceptionBasedOnFareCategory(String fareCategoryCode, String errorCode) throws ModuleException {
		if (ValidationUtils.isRestrictedFareCategory(fareCategoryCode)) {
			throw new ModuleException(errorCode);
		}
	}

	/**
	 * Check Restricted Fare Exist or not
	 * 
	 * @param reservation
	 * @param pnrSegmentIds
	 * @param errorCode
	 * @throws ModuleException
	 */
	public static void checkRestrictedSegmentConstraints(Reservation reservation, Collection<Integer> pnrSegmentIds,
			String errorCode) throws ModuleException {

		Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator();
		ReservationSegment reservationSegment;
		String fareCategoryCode;

		while (itReservationSegment.hasNext()) {
			reservationSegment = (ReservationSegment) itReservationSegment.next();
			fareCategoryCode = BeanUtils.nullHandler(reservationSegment.getFareTO(null).getFareCategoryCode());

			if (pnrSegmentIds != null && pnrSegmentIds.size() > 0) {
				if (pnrSegmentIds.contains(reservationSegment.getPnrSegId())) {
					invokeExceptionBasedOnFareCategory(fareCategoryCode, errorCode);
				}
			} else {
				invokeExceptionBasedOnFareCategory(fareCategoryCode, errorCode);
			}
		}
	}

	/**
	 * Checks the segment constraints
	 * 
	 * @param colOndFareDTO
	 * @param status
	 * @param pnrPaymentTypes
	 * @throws ModuleException
	 */
	public static void checkRestrictedSegmentConstraints(Collection<OndFareDTO> colOndFareDTO, String status,
			Integer pnrPaymentTypes) throws ModuleException {
		boolean segmentExist = isAnyRestrictedSegmentExist(colOndFareDTO);

		if (segmentExist) {
			if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
							.equals(pnrPaymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
							.equals(pnrPaymentTypes)) {
				throw new ModuleException("airreservations.cancellation.restrictedSegPayShouldExist");
			} else {
				if (!ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(status)) {
					throw new ModuleException("airreservations.cancellation.restrictedSegPayShouldExist");
				}
			}
		}
	}

	/**
	 * Is a Restricted fare category
	 * 
	 * @param fareCategoryCode
	 * @return
	 */
	private static boolean isRestrictedFareCategory(String fareCategoryCode) {
		if (AirPricingCustomConstants.FareCategoryType.RESTRICTED.equals(fareCategoryCode)) {
			return true;
		}

		return false;
	}

	/*
	 * ############################### OHD Restriction Validation Methods ##################################
	 */

	/**
	 * check number of attempts from given IP Address is within the configuration limit
	 * 
	 * @param ipAddress
	 * @param salesChannel
	 *            TODO
	 * @return
	 */
	public static boolean isOHDAllowForIp(String ipAddress, int salesChannel) {
		int rescount = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getOnholdReservationCountByIp(ipAddress, salesChannel);
		int maxResCount = AppSysParamsUtil.getMaxPerIpOHDBookings();

		if (maxResCount > rescount) {
			return true;
		} else {
			if (log.isInfoEnabled()) {
				log.info("IBE onhold not allow for IP " + ipAddress);
			}
			return false;
		}
	}

	/**
	 * 
	 * Check the OHD reservation limit for flights are within the configuration limit
	 * 
	 * @param flightSegIdList
	 * @param salesChannel
	 *            TODO
	 * @return
	 */
	public static boolean isOHDAllowForFlight(List<Integer> flightSegIdList, int salesChannel) {
		int maxCount = AppSysParamsUtil.getMaxPerFlightOHDBookings();
		boolean allowOnhold = true;

		for (Integer segId : flightSegIdList) {
			int resCount = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getOnholdReservationCountByFlight(segId, salesChannel);

			if (resCount >= maxCount) {
				allowOnhold = false;
				if (log.isInfoEnabled()) {
					log.info("IBE onhold limit exceeded for flight segment ID: " + segId);
				}
				break;
			}
		}

		return allowOnhold;
	}

	/**
	 * 
	 * Check the booking date is within the departure date cutoff configuration
	 * 
	 * @param depDateList
	 * @return
	 */
	public static boolean isOHDAllowForBookingDate(List<Date> depDateList) {
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
		long maxCutoffDays = AppSysParamsUtil.getOHDBookingCutoffDays();
		boolean allowOnhold = true;

		for (Date depDate : depDateList) {
			long diff = depDate.getTime() - currentDate.getTime();
			long diffInDays = diff / (1000 * 60 * 60 * 24);
			if (maxCutoffDays > diffInDays) {
				allowOnhold = false;
				if (log.isInfoEnabled()) {
					log.info("IBE onhold cutoff days exceeded.");
				}
				break;
			}
		}

		return allowOnhold;
	}

	/**
	 * 
	 * Check the OHD reservations are allow for the country
	 * 
	 * @param ipAddress
	 * @return
	 */
	public static boolean isOHDAllowForCountry(String ipAddress) throws ModuleException {
		long ip = PlatformUtiltiies.getOriginIP(ipAddress);
		Country country = ReservationModuleUtils.getCommonMasterBD().getCountryFromIPAddress(ip);

		if (country != null && YES.equalsIgnoreCase(country.getOnholdEnabled())) {
			return true;
		} else {
			if (log.isInfoEnabled()) {
				log.info("IBE onhold not allow for country having IP " + ipAddress);
			}
			return false;
		}
	}

	/**
	 * 
	 * Check OHD reservation creation allow for selected pax (pax identified by First Name & Last Name or email)
	 * 
	 * @param paxList
	 * @param salesChannel
	 *            TODO
	 * @return
	 */
	public static boolean isOHDAllowForPax(List<ReservationPax> paxList, String email, int salesChannel) {
		String paxValidationType = AppSysParamsUtil.getOHDPaxValidationType();
		int maxCount = AppSysParamsUtil.getMaxPerPaxOHDBookings();

		boolean allowOnhold = true;

		if (PAX_VALIDATE_NAME.equals(paxValidationType)) {

			for (ReservationPax reservationPax : paxList) {
				int resCount = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getOnholdReservationCountByPaxName(reservationPax,
						salesChannel);

				if (resCount >= maxCount) {
					allowOnhold = false;
					if (log.isInfoEnabled()) {
						log.info("IBE onhold limit exceeded by pax " + reservationPax.getFirstName() + " "
								+ reservationPax.getLastName());
					}
					break;
				}
			}

		} else if (PAX_VALIDATE_EMAIL.equals(paxValidationType)) {
			int resCount = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getOnholdReservationCountByPaxEmail(email,
					salesChannel);

			if (resCount >= maxCount) {
				allowOnhold = false;
				if (log.isInfoEnabled()) {
					log.info("IBE onhold limit exceeded by pax " + email);
				}
			}
		}

		return allowOnhold;
	}

	/*
	 * ########################### End of OHD Restriction Validation Methods ###############################
	 */

	/**
	 * skip Airport service insertion to PNL/ADL based on app parameter
	 */
	public static boolean skipAirportService(Integer ssrCategoryId) {
		if (SSRCategory.ID_AIRPORT_SERVICE.equals(ssrCategoryId) && !AppSysParamsUtil.isAirportServicesEnabled()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check selected SSRs are still available before making reservation
	 * 
	 * @param exgReprotectSsrFltSegInv
	 * 
	 */

	public static void checkSSRInventoryAvailability(Reservation reservation,
			Map<Integer, Map<Integer, Integer>> exgReprotectSsrFltSegInv) throws ModuleException {
		if (reservation != null) {
			Map<Integer, SSRCountDTO> fltSegWiseMap = flightSegWiseAnciCount(reservation.getPassengers());

			if (fltSegWiseMap != null) {
				for (Entry<Integer, SSRCountDTO> entry : fltSegWiseMap.entrySet()) {
					Integer fltSegId = entry.getKey();
					SSRCountDTO ssrCountDTO = entry.getValue();
					Map<Integer, Integer> availableSSRCountMap = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
							.getSSRCount(fltSegId, ssrCountDTO.getCabinclass(), ssrCountDTO.getLogicalCabinClass());

					Map<Integer, String> userDefinedSSRMap = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
							.getActiveUserDefinedSSRDetails();

					if (ssrCountDTO.getSsrCountMap() != null) {
						for (Entry<Integer, Integer> ssrEntry : ssrCountDTO.getSsrCountMap().entrySet()) {

							if (userDefinedSSRMap != null && !userDefinedSSRMap.isEmpty()
									&& userDefinedSSRMap.containsKey(ssrEntry.getKey())) {
								continue;
							}

							if (exgReprotectSsrFltSegInv != null && (exgReprotectSsrFltSegInv.get(fltSegId) != null)) {
								if (exgReprotectSsrFltSegInv.get(fltSegId).containsKey(ssrEntry.getKey())
										&& exgReprotectSsrFltSegInv.get(fltSegId).get(ssrEntry.getKey()) > 0) {
									continue;
								}
							}
							if (availableSSRCountMap.containsKey(ssrEntry.getKey())) {
								if (availableSSRCountMap.get(ssrEntry.getKey()) >= 0) {
									continue;
								} else {
									throw new ModuleException("airreservations.anci.ssr.not.available");
								}
							} else {
								throw new ModuleException("airreservations.anci.ssr.not.available");
							}
						}
					}

				}
			}
		}
	}

	private static Map<Integer, SSRCountDTO> flightSegWiseAnciCount(Set<ReservationPax> colResPax) throws ModuleException {
		Map<Integer, SSRCountDTO> fltSegWiseMap = new HashMap<Integer, SSRCountDTO>();

		if (colResPax != null) {
			for (ReservationPax resPax : colResPax) {
				if (resPax != null && resPax.getPnrPaxFares() != null) {
					for (ReservationPaxFare resPaxFare : (Set<ReservationPaxFare>) resPax.getPnrPaxFares()) {
						if (resPaxFare != null && resPaxFare.getPaxFareSegments() != null) {
							for (ReservationPaxFareSegment resPaxFareSeg : (Set<ReservationPaxFareSegment>) resPaxFare
									.getPaxFareSegments()) {

								if (resPaxFareSeg != null
								// AARESAA-16602 TODO Check ETKT status as well
										&& resPaxFareSeg.getStatus().equals(
												ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED)) {
									ReservationSegment resSeg = resPaxFareSeg.getSegment();

									if (resSeg != null
											&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg
													.getStatus())) {
										Set<ReservationPaxSegmentSSR> colResPaxSSRs = resPaxFareSeg
												.getReservationPaxSegmentSSRs();

										if (colResPaxSSRs != null) {
											if (!fltSegWiseMap.containsKey(resSeg.getFlightSegId())) {
												fltSegWiseMap.put(resSeg.getFlightSegId(), new SSRCountDTO());
											}

											SSRCountDTO ssrCountDTO = fltSegWiseMap.get(resSeg.getFlightSegId());
											ssrCountDTO.setCabinclass(resSeg.getCabinClassCode());
											ssrCountDTO.setLogicalCabinClass(resSeg.getLogicalCCCode());
											Map<Integer, Integer> ssrCountMap = ssrCountDTO.getSsrCountMap();

											for (ReservationPaxSegmentSSR resPaxSSR : colResPaxSSRs) {
												/*
												 * If service has an airport code then it is an airport service &
												 * Airport Services need to be skipped from this validation
												 */
												if (resPaxSSR.getAirportCode() != null && !"".equals(resPaxSSR.getAirportCode())) {
													continue;
												}
												int count = 0;
												if (!ssrCountMap.containsKey(resPaxSSR.getSSRId())) {
													count = 1;
												} else {
													count = ssrCountMap.get(resPaxSSR.getSSRId()) + 1;
												}

												ssrCountMap.put(resPaxSSR.getSSRId(), count);
											}

										}
									}

								}

							}
						}
					}
				}
			}
		}

		return fltSegWiseMap;
	}

	public static boolean isSkipPenaltyCalculation(Collection<OndFareDTO> collFares, ModifyAssitUnit modifyAsst,
			Map<Integer, Integer> oldFareIdByFltSegIdMap, Map<Integer, String> ondFareTypeByFareIdMap) throws ModuleException {

		boolean isSkipPenalty = true;
		boolean isPenaltyValidOndExist = false;
		if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs()
				&& AppSysParamsUtil.getPenaltyCalculateMethod() == PENALTY_BASED.OLD_FARE && !modifyAsst.isUserCancellation()) {

			if (oldFareIdByFltSegIdMap != null && oldFareIdByFltSegIdMap.size() > 0 && collFares != null && collFares.size() > 0
					&& ondFareTypeByFareIdMap != null && ondFareTypeByFareIdMap.size() > 0) {

				ArrayList<Integer> skipFareTypeList = new ArrayList<Integer>();
				skipFareTypeList.add(FareTypes.HALF_RETURN_FARE);
				skipFareTypeList.add(FareTypes.RETURN_FARE);

				for (OndFareDTO ondfareDTO : collFares) {

					for (SegmentSeatDistsDTO segSeatDistsDTO : ondfareDTO.getSegmentSeatDistsDTO()) {

						Integer oldFareId = oldFareIdByFltSegIdMap.get(segSeatDistsDTO.getFlightSegId());

						if (oldFareId != null) {
							String oldFareType = ondFareTypeByFareIdMap.get(oldFareId);
							if (oldFareType != null && skipFareTypeList.contains(Integer.parseInt(oldFareType))
									&& segSeatDistsDTO.isFlownOnd() && !skipFareTypeList.contains(ondfareDTO.getFareType())) {
								isPenaltyValidOndExist = true;

							}
						}

						if (isPenaltyValidOndExist && !segSeatDistsDTO.isFlownOnd()) {
							isSkipPenalty = false;
							break;
						}

					}

				}
			}

		} else if (collFares != null && hasInterlineFares(collFares)) {
			isSkipPenalty = true;
		} else {
			isSkipPenalty = modifyAsst.isSkipPenalty();
		}

		return isSkipPenalty;
	}

	private static boolean hasInterlineFares(Collection<OndFareDTO> collFares) throws ModuleException {
		Collection<String> bcCodes = new ArrayList<String>();
		for (OndFareDTO ondFareDTO : collFares) {
			if (ondFareDTO.getFareSummaryDTO() != null && ondFareDTO.getFareSummaryDTO().getBookingClassCode() != null) {
				bcCodes.add(ondFareDTO.getFareSummaryDTO().getBookingClassCode());
			}
		}
		if (bcCodes.size() > 0) {
			Map<String, String> bcTypes = ReservationModuleUtils.getCommonMasterBD().getBookingClassTypes(bcCodes);
			for (String bookingCode : bcTypes.keySet()) {
				if (bcTypes.get(bookingCode).equals(BookingClass.BookingClassType.INTERLINE_FREESELL)) {
					return true;
				}
			}
		}

		return false;
	}
}
