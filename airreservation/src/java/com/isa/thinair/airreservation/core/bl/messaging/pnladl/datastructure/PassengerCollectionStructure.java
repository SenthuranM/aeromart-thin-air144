/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.comparator.CompDestinationFareClass;
import com.isa.thinair.airreservation.api.dto.adl.comparator.CompPassengerPnr;
import com.isa.thinair.airreservation.api.dto.adl.comparator.CompPassengerTitle;
import com.isa.thinair.airreservation.api.dto.adl.comparator.MultiComparator;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;

/**
 * @author udithad
 *
 */
public class PassengerCollectionStructure {

	public PassengerCollection getPassengerCollection(
			Collection<DestinationFare> destinationFares,
			HashMap<String, List<String>> cabinFareRbdMap) {
		PassengerCollection passengerCollection = new PassengerCollection();
		populatePassengerCollectionWith(destinationFares, passengerCollection);
		passengerCollection.setRbdFareCabinMap(cabinFareRbdMap);
		return passengerCollection;
	}

	private void populatePassengerCollectionWith(
			Collection<DestinationFare> destinationInformationList,
			PassengerCollection passengerCollection) {
		if (destinationInformationList != null && passengerCollection != null) {
			for (DestinationFare destinationDTO : destinationInformationList) {
				passengerCollection.populateDestinationElements(destinationDTO);
			}
		}
	}
	
	public void sortDestinationFares(List<DestinationFare> destinationFares){
		MultiComparator.sort(destinationFares,new CompDestinationFareClass() );
	}

}
