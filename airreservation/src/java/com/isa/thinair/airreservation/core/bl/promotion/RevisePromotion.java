package com.isa.thinair.airreservation.core.bl.promotion;

import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;

/**
 * @isa.module.command name="revisePromotion"
 */
public class RevisePromotion extends DefaultBaseCommand {

	@Override
	public ServiceResponce execute() throws ModuleException {
		String pnr;

		// TODO -- currently withdraws if any modification occures

		boolean isPromotionModuleEnabled = AppSysParamsUtil.isPromotionModuleEnabled();
		if (isPromotionModuleEnabled) {
			Object oldPnr = getParameter(CommandParamNames.OLD_PNR);
			if (oldPnr != null) {
				pnr = (String) oldPnr;
			} else {
				pnr = (String) getParameter(CommandParamNames.PNR);
			}

			PromotionManagementBD promotionManagementBD = ReservationModuleUtils.getPromotionManagementBD();
			promotionManagementBD.withdrawPromotionRequest(pnr, null);
		}

		ServiceResponce resp = new DefaultServiceResponse(true);
		return resp;
	}
}
