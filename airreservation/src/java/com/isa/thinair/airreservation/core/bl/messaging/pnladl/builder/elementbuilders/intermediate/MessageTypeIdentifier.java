package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.intermediate;

import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.NameElementBuilderTemplatePalCal;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.NameElementBuilderTemplatePnlAdl;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.TotalByDestinationElementBuilderTemplatePal;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.TotalByDestinationElementBuilderTemplatePnlAdl;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.NameElementBuilderTemplate;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.TotalByDestinationElementBuilderTemplate;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.TotalByDestinationElementBuilderTemplateCAL;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator.PaxModifierElementPalCalValidator;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator.PaxModifierElementPnlAdlValidator;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator.base.BaseElementBuilderValidator;

public abstract class MessageTypeIdentifier extends BaseElementBuilder {

	public BaseElementBuilderValidator getElementBuilderValidator(String messageType) {

		BaseElementBuilderValidator validator = null;

		if (MessageTypes.PNL.toString().equals(messageType) || MessageTypes.ADL.toString().equals(messageType)) {
			validator = new PaxModifierElementPnlAdlValidator();
		} else if (MessageTypes.PAL.toString().equals(messageType) || MessageTypes.CAL.toString().equals(messageType)) {
			validator = new PaxModifierElementPalCalValidator();
		}
		return validator;
	}
	
	public TotalByDestinationElementBuilderTemplate getTotalByDestinationElementBuilderTemplate(String messageType){
		TotalByDestinationElementBuilderTemplate templateBuilder= null;
		if (MessageTypes.PNL.toString().equals(messageType) || MessageTypes.ADL.toString().equals(messageType)) {
			templateBuilder = new TotalByDestinationElementBuilderTemplatePnlAdl();
		} else if (MessageTypes.PAL.toString().equals(messageType)) {
			templateBuilder = new TotalByDestinationElementBuilderTemplatePal();
		}else if(MessageTypes.CAL.toString().equals(messageType)){
			templateBuilder = new TotalByDestinationElementBuilderTemplateCAL();
		}
		return templateBuilder;
	}
	
	public NameElementBuilderTemplate getNameElementBuilderTemplate(String messageType){
		NameElementBuilderTemplate templateBuilder= null;
		if (MessageTypes.PNL.toString().equals(messageType) || MessageTypes.ADL.toString().equals(messageType)) {
			templateBuilder = new NameElementBuilderTemplatePnlAdl();
		}else if (MessageTypes.PAL.toString().equals(messageType) || MessageTypes.CAL.toString().equals(messageType)) {
			templateBuilder = new NameElementBuilderTemplatePalCal();
		}
		return templateBuilder;
	}
	
}
