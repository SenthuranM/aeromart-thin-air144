/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * The main purpose of this is to accept a collection of OndFareDTO which is sent by the inventory module and provide a
 * functionality of a segment bucket which returns unique booking codes.
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class SegmentBucketBO {

	/** Hold segment Seats */
	private Map<Integer, Map<String, Integer>> mapSegmentSeats;
	private Map<Integer, Map<String, Boolean>> mapSegmentBookingTypeOverBook;

	/**
	 * Purposly avoided to prevent bogus bucket
	 */
	private SegmentBucketBO() {
		this.mapSegmentSeats = new HashMap<Integer, Map<String, Integer>>();
		this.mapSegmentBookingTypeOverBook = new HashMap<Integer, Map<String, Boolean>>();
	}

	/**
	 * Constructor with the fares
	 * 
	 * @param colOndFareDTO
	 *            collection of OndFareDTO
	 */
	public SegmentBucketBO(Collection<OndFareDTO> colOndFareDTO) throws ModuleException {

		// Calls the initializing process
		this();

		if (colOndFareDTO == null || colOndFareDTO.size() == 0) {
			throw new ModuleException("airreservations.arg.invalidInfoFromInventory");
		}

		Iterator<OndFareDTO> itColOndFareDTO = colOndFareDTO.iterator();
		OndFareDTO ondFareDTO;

		Iterator<SegmentSeatDistsDTO> itSegmentSeatDistsDTO;
		SegmentSeatDistsDTO segmentSeatDistsDTO;

		Iterator<SeatDistribution> itSeatDistribution;
		SeatDistribution seatDistribution;

		Map<String, Integer> bookingsMap;
		Map<String, Boolean> bookingCodeOverBookMap;

		while (itColOndFareDTO.hasNext()) {
			ondFareDTO = (OndFareDTO) itColOndFareDTO.next();

			if (ondFareDTO.getSegmentSeatDistsDTO() == null || ondFareDTO.getSegmentSeatDistsDTO().size() == 0) {
				throw new ModuleException("airreservations.arg.invalidInfoFromInventory");
			}

			itSegmentSeatDistsDTO = ondFareDTO.getSegmentSeatDistsDTO().iterator();

			while (itSegmentSeatDistsDTO.hasNext()) {
				segmentSeatDistsDTO = (SegmentSeatDistsDTO) itSegmentSeatDistsDTO.next();

				if (segmentSeatDistsDTO.getEffectiveSeatDistribution() == null
						|| segmentSeatDistsDTO.getEffectiveSeatDistribution().size() == 0) {
					throw new ModuleException("airreservations.arg.invalidInfoFromInventory");
				}

				itSeatDistribution = segmentSeatDistsDTO.getEffectiveSeatDistribution().iterator();
				bookingsMap = new HashMap<String, Integer>();
				bookingCodeOverBookMap = new HashMap<String, Boolean>();

				while (itSeatDistribution.hasNext()) {
					seatDistribution = (SeatDistribution) itSeatDistribution.next();

					// No of seats have to be greater than zero
					if (seatDistribution.isOverbook() || seatDistribution.getNoOfSeats() > 0) {
						bookingsMap.put(seatDistribution.getBookingClassCode(), new Integer(seatDistribution.getNoOfSeats()));
					}

					bookingCodeOverBookMap.put(seatDistribution.getBookingClassCode(), seatDistribution.isOverbook());
				}

				this.mapSegmentSeats.put(new Integer(segmentSeatDistsDTO.getFlightSegId()), bookingsMap);
				this.mapSegmentBookingTypeOverBook.put(new Integer(segmentSeatDistsDTO.getFlightSegId()), bookingCodeOverBookMap);
			}
		}
	}

	/**
	 * Returns the segment corresponding booking codes Will return null if no booking code available to deliever
	 * 
	 * @param flightSegId
	 *            flight segment id
	 * @return the booking code
	 * @throws ModuleException
	 */
	public String getBookingCode(int flightSegId) throws ModuleException {
		Map<String, Integer> bookingsMap = (Map<String, Integer>) mapSegmentSeats.get(new Integer(flightSegId));
		Map<String, Boolean> overBookStatusMap = mapSegmentBookingTypeOverBook.get(flightSegId);
		String bookingCode = null;

		if (bookingsMap != null) {
			Iterator<String> itBookingClassCode = bookingsMap.keySet().iterator();
			Integer seats;
			boolean found = false;

			if (itBookingClassCode.hasNext()) {
				bookingCode = (String) itBookingClassCode.next();
				if (!overBookStatusMap.get(bookingCode)) {
					seats = (Integer) bookingsMap.get(bookingCode);
					seats = new Integer(seats.intValue() - 1);

					if (seats.intValue() > 0) {
						bookingsMap.put(bookingCode, seats);
					} else {
						found = true;
					}
				}
			}

			// Removing the booking code if it's zero
			if (found) {
				bookingsMap.remove(bookingCode);
			}
		}

		if (bookingCode == null) {
			throw new ModuleException("airreservations.arg.invalidInfoFromInventory");
		}

		return bookingCode;
	}

}
