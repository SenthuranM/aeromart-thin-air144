package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.model.FareRuleFee;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.FlightType;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;

/**
 * @author Nilindra Fernando
 * @since Nov 9, 2011
 */
public class FeeCalculator {

	private static Log log = LogFactory.getLog(FeeCalculator.class);

	private ReservationPaxFare reservationPaxFare;

	private Collection<FareRuleFeeTO> colFareRuleFeeTOs;

	private Date executionDate;

	private FareRuleFeeTO modifyFee;

	private FareRuleFeeTO cancelFee;

	private FareRuleFeeTO nameChangeFee;

	private boolean isNoonDayFeePolicyEnabled;

	public FeeCalculator(ReservationPaxFare reservationPaxFare, Collection<FareRuleFeeTO> colFareRuleFeeTOs, Date executionDate,
			boolean isNoonDayFeePolicyEnabled) {
		this.setReservationPaxFare(reservationPaxFare);
		this.setColFareRuleFeeTOs(colFareRuleFeeTOs);
		this.setExecutionDate(executionDate);
		this.setNoonDayFeePolicyEnabled(isNoonDayFeePolicyEnabled);
	}

	public void execute() {
		Object[] attributes = getFlightsAttributes(this.getReservationPaxFare());
		Date firstDepartureDate = (Date) attributes[0];
		// Only domestic flights should check the 12noon policy if it's enabled
		// For any internation/domestic combination it is assumed the 12noon policy won't be applied
		// For all international fights 12noon policy is not applied even though it's enabled.
		boolean isDomesticFlight = !((Boolean) attributes[1]);

		if (log.isDebugEnabled()) {
			log.debug(" 1. Processing for PnrPaxFareId : " + this.getReservationPaxFare().getPnrPaxFareId());
			log.debug(" 2. Identified Fare Id : " + this.getReservationPaxFare().getFareId());
			log.debug(" 3. Identified First Departure Date : " + firstDepartureDate);
			log.debug(" 4. Is Domestic Flight : " + isDomesticFlight);
		}

		List<FareRuleFeeTO> modifyFees = getFeesWithTriggerDates(firstDepartureDate, FareRuleFee.CHARGE_TYPES.MODIFICATION);
		List<FareRuleFeeTO> cancelFees = getFeesWithTriggerDates(firstDepartureDate, FareRuleFee.CHARGE_TYPES.CANCELLATION);
		List<FareRuleFeeTO> nameChangeFees = getFeesWithTriggerDates(firstDepartureDate, FareRuleFee.CHARGE_TYPES.NAME_CHANGE);

		Collections.sort(modifyFees);
		Collections.sort(cancelFees);
		Collections.sort(nameChangeFees);

		FareRuleFeeTO modifyFee = getMatchingFee(firstDepartureDate, modifyFees, isDomesticFlight);
		FareRuleFeeTO cancelFee = getMatchingFee(firstDepartureDate, cancelFees, isDomesticFlight);
		FareRuleFeeTO nameChangeFee = getMatchingFee(firstDepartureDate, nameChangeFees, isDomesticFlight);

		if (log.isDebugEnabled()) {
			log.debug(" 4. Modify Fee : " + (modifyFee != null ? ("\n" + XMLStreamer.compose(modifyFee)) : "NULL"));
			log.debug(" 5. Cancel Fee : " + (cancelFee != null ? ("\n" + XMLStreamer.compose(cancelFee)) : "NULL"));
			log.debug(" 6. Name Change Fee : " + (nameChangeFee != null ? ("\n" + XMLStreamer.compose(nameChangeFee)) : "NULL"));
		}

		this.setModifyFee(modifyFee);
		this.setCancelFee(cancelFee);
		this.setNameChangeFee(nameChangeFee);
	}

	private FareRuleFeeTO getMatchingFee(Date firstDepartureDate, Collection<FareRuleFeeTO> fees, boolean isDomesticFlight) {

		if (fees != null && fees.size() > 0) {
			for (FareRuleFeeTO fareRuleFeeTO : fees) {
				Object[] twlNoonMeta = get12NoonDayTriggeredMeta(fareRuleFeeTO, isDomesticFlight);
				Date triggeredDate = (Date) twlNoonMeta[0];
				Date calculatedExecutionDate = this.getCalculatedExecutionDate((Boolean) twlNoonMeta[1]);

				if (FareRuleFee.COMPARE_AGAINTS.BEFORE_DEPARTURE.equals(fareRuleFeeTO.getCompareAgainst())) {
					if (calculatedExecutionDate.compareTo(triggeredDate) < 1
							&& calculatedExecutionDate.compareTo(firstDepartureDate) < 1) {
						return fareRuleFeeTO;
					}
				} else if (FareRuleFee.COMPARE_AGAINTS.AFTER_DEPARTURE.equals(fareRuleFeeTO.getCompareAgainst())) {
					if (calculatedExecutionDate.compareTo(triggeredDate) > -1
							&& calculatedExecutionDate.compareTo(firstDepartureDate) == 1) {
						return fareRuleFeeTO;
					}
				}
			}
		}

		return null;
	}

	private Date getCalculatedExecutionDate(boolean is12NoonPolicyEntry) {

		if (is12NoonPolicyEntry) {
			return CalendarUtil.getTimeZoneConvertedDate(this.getExecutionDate(), "Iran");
		} else {
			return this.getExecutionDate();
		}
	}

	private Object[] get12NoonDayTriggeredMeta(FareRuleFeeTO fareRuleFeeTO, boolean isDomesticFlight) {
		if (isNoonDayFeePolicyEnabled() && isDomesticFlight) {
			if (FareRuleFee.TIME_TYPE.DAYS.equals(fareRuleFeeTO.getTimeType())
					|| FareRuleFee.TIME_TYPE.WEEK.equals(fareRuleFeeTO.getTimeType())
					|| FareRuleFee.TIME_TYPE.MONTH.equals(fareRuleFeeTO.getTimeType())) {
				return new Object[] { CalendarUtil.getHalfTimeOfDate(fareRuleFeeTO.getTriggerDate()), true };
			}
		}

		return new Object[] { fareRuleFeeTO.getTriggerDate(), isDomesticFlight };
	}

	private Date getTriggeredDate(Date firstDepartureDate, String compareAgainst, String timeType, String timeValue) {

		int sign;

		if (compareAgainst.equals(FareRuleFee.COMPARE_AGAINTS.BEFORE_DEPARTURE)) {
			sign = -1;
		} else {
			sign = 1;
		}

		int minutes = 0;
		int hours = 0;
		int days = 0;
		int months = 0;

		if (timeType.equals(FareRuleFee.TIME_TYPE.MINUTES)) {
			minutes = Integer.parseInt(timeValue) * sign;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.HOURS)) {
			hours = Integer.parseInt(timeValue) * sign;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.DAYS)) {
			days = Integer.parseInt(timeValue) * sign;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.WEEK)) {
			days = Integer.parseInt(timeValue) * sign * 7;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.MONTH)) {
			months = Integer.parseInt(timeValue) * sign;
		}

		return CalendarUtil.add(firstDepartureDate, 0, months, days, hours, minutes, 0);
	}

	private List<FareRuleFeeTO> getFeesWithTriggerDates(Date firstDepartureDate, String type) {
		List<FareRuleFeeTO> filtered = new ArrayList<FareRuleFeeTO>();

		if (this.getColFareRuleFeeTOs() != null && this.getColFareRuleFeeTOs().size() > 0) {
			for (FareRuleFeeTO fareRuleFeeTO : this.getColFareRuleFeeTOs()) {
				if (type.equals(fareRuleFeeTO.getChargeType())) {
					Date triggerDate = getTriggeredDate(firstDepartureDate, fareRuleFeeTO.getCompareAgainst(),
							fareRuleFeeTO.getTimeType(), fareRuleFeeTO.getTimeValue());
					fareRuleFeeTO.setTriggerDate(triggerDate);

					filtered.add(fareRuleFeeTO);
				}
			}
		}

		return filtered;
	}

	public static Object[] getFlightsAttributes(ReservationPaxFare reservationPaxFare) {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();

		for (ReservationPaxFareSegment reservationPaxFareSegment : (Collection<ReservationPaxFareSegment>) reservationPaxFare
				.getPaxFareSegments()) {
			pnrSegIds.add(reservationPaxFareSegment.getSegment().getPnrSegId());
		}

		Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationPaxFare.getReservationPax().getReservation()
				.getSegmentsView();

		List<Date> lstLocalDepartureDates = new ArrayList<Date>();
		List<Date> lstZuluDepartureDates = new ArrayList<Date>();
		Collection<String> flightType = new HashSet<String>();

		for (ReservationSegmentDTO reservationSegmentDTO : colReservationSegmentDTO) {
			if (pnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
				lstLocalDepartureDates.add(reservationSegmentDTO.getDepartureDate());
				lstZuluDepartureDates.add(reservationSegmentDTO.getZuluDepartureDate());
				flightType.add(reservationSegmentDTO.getFlightType());
			}
		}

		if (flightType.contains(FlightType.INTERNATIONL.getType())) {
			Collections.sort(lstZuluDepartureDates);
			return new Object[] { BeanUtils.getFirstElement(lstZuluDepartureDates), true };
		} else {
			Collections.sort(lstLocalDepartureDates);
			return new Object[] { BeanUtils.getFirstElement(lstLocalDepartureDates), false };
		}

	}

	/**
	 * @return the reservationPaxFare
	 */
	private ReservationPaxFare getReservationPaxFare() {
		return reservationPaxFare;
	}

	/**
	 * @param reservationPaxFare
	 *            the reservationPaxFare to set
	 */
	private void setReservationPaxFare(ReservationPaxFare reservationPaxFare) {
		this.reservationPaxFare = reservationPaxFare;
	}

	/**
	 * @return the colFareRuleFeeTOs
	 */
	private Collection<FareRuleFeeTO> getColFareRuleFeeTOs() {
		return colFareRuleFeeTOs;
	}

	/**
	 * @param colFareRuleFeeTOs
	 *            the colFareRuleFeeTOs to set
	 */
	private void setColFareRuleFeeTOs(Collection<FareRuleFeeTO> colFareRuleFeeTOs) {
		this.colFareRuleFeeTOs = colFareRuleFeeTOs;
	}

	/**
	 * @return the executionDate
	 */
	private Date getExecutionDate() {
		return executionDate;
	}

	/**
	 * @param executionDate
	 *            the executionDate to set
	 */
	private void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * @return the modifyFee
	 */
	public FareRuleFeeTO getModifyFee() {
		return modifyFee;
	}

	/**
	 * @return the cancelFee
	 */
	public FareRuleFeeTO getCancelFee() {
		return cancelFee;
	}

	/**
	 * @param modifyFee
	 *            the modifyFee to set
	 */
	private void setModifyFee(FareRuleFeeTO modifyFee) {
		this.modifyFee = modifyFee;
	}

	/**
	 * @param cancelFee
	 *            the cancelFee to set
	 */
	private void setCancelFee(FareRuleFeeTO cancelFee) {
		this.cancelFee = cancelFee;
	}

	/**
	 * @return the isNoonDayFeePolicyEnabled
	 */
	private boolean isNoonDayFeePolicyEnabled() {
		return isNoonDayFeePolicyEnabled;
	}

	/**
	 * @param isNoonDayFeePolicyEnabled
	 *            the isNoonDayFeePolicyEnabled to set
	 */
	private void setNoonDayFeePolicyEnabled(boolean isNoonDayFeePolicyEnabled) {
		this.isNoonDayFeePolicyEnabled = isNoonDayFeePolicyEnabled;
	}

	public FareRuleFeeTO getNameChangeFee() {
		return nameChangeFee;
	}

	public void setNameChangeFee(FareRuleFeeTO nameChangeFee) {
		this.nameChangeFee = nameChangeFee;
	}

}
