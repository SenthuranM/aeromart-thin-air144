/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.NameElementRuleContext;

/**
 * @author udithad
 *
 */
public class NameElementPnrRule extends BaseRule<NameElementRuleContext> {

	@Override
	public boolean validateRule(NameElementRuleContext context) {
		boolean isValied = false;
		if (context != null && context.getPreviourPnr() == null
				&& context.getOngoingPnr() != null) {
			isValied = true;
		} else if (context != null && context.getPreviourPnr() != null
				&& context.getOngoingPnr() != null
				&& context.getPreviourPnr().equals(context.getOngoingPnr())) {
			isValied = true;
		}
		return isValied;
	}

}
