package com.isa.thinair.airreservation.core.bl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airreservation.api.dto.adl.FlightInformation;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.audit.PNLADLHistoryAuditConstants;
import com.isa.thinair.airreservation.core.bl.pnl.SendMailUsingAuthentication;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.msgbroker.api.util.MessageComposerApiUtils;

/**
 * This class includes all the business logic for auxilliary reservations
 * 
 * @author isuru
 */
public class ReservationAuxiliaryBL {

	private static Log log = LogFactory.getLog(ReservationAuxiliaryBL.class);
	
	//TODO Refactor resendExistingManualPALCAL resendExistingManualPNLADL mwthods
	public void resendExistingManualPALCAL(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus, String[] sitaaddresses, String mailserver, String userID,
			String flightNumber, Date dateOfFlight) throws ModuleException {
		
			String sentPNLADLText = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getSavedPalCal(flightID, timestamp,
				sitaAddress, departureStation, messageType, transmissionStatus);
		if (sentPNLADLText != null && !sentPNLADLText.isEmpty()) {
			try {				
				String pnlADLMailExtension = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
						.getPalCalMailExtension();
				List<String> pnlList = getMessageTextParts(sentPNLADLText, messageType);
				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
				HashMap<String, String> sitaStatusMap = new HashMap<String, String>();
				
				Map<String, String> legNumberWiseOndMap = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getFlightLegs(flightID);
				String ond = PnlAdlUtil.getOndFromDepartureAirport(departureStation, legNumberWiseOndMap);
				
				Map<String, List<String>> sitaAddressesMap = PnlAdlUtil.getSitaAddressMap(departureStation, flightNumber, ond,
						carrierCode, Arrays.asList(sitaaddresses), DCS_PAX_MSG_GROUP_TYPE.PAL_CAL);
				List<String> sitaAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				List<String> sitaTexAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
				FlightInformation flightInfoElement = new FlightInformation();
				flightInfoElement.setFlightNumber(flightNumber);
				flightInfoElement.setBoardingAirport(departureStation);
				flightInfoElement.setFlightDate(dateOfFlight);
				int partNumber = 1;
				for (Iterator<String> pnlListIter = pnlList.iterator(); pnlListIter.hasNext();) {
					String pnlText = pnlListIter.next();
					for (String address : sitaAddressList) {
						String fullyQualifiedAddress = "";
						boolean isAlternativeSender = false;
						if (address.indexOf("@") != -1) {
							fullyQualifiedAddress = address;
							isAlternativeSender = true;
						} else {
							fullyQualifiedAddress = address + pnlADLMailExtension;
						}
						sendTextViAEmail(fullyQualifiedAddress, mailserver, pnlText, messageType, isAlternativeSender);
					}
					List<String> fileNames = new ArrayList<String>();
					String path = MessageComposerApiUtils.generateFileName(flightInfoElement, messageType, partNumber);
					fileNames.add(path);
					PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddressList, fileNames, messageType, sitaStatusMap, pnlText);

					partNumber++;
				}
				auditMessageTransactionHistory(flightID, sitaAddress, departureStation, messageType, transmissionStatus, userID,
						TasksUtil.MANUALLY_RESEND_PAL_CAL, "xbe");

			} catch (Exception e) {
				throw new ModuleException(e, "reservationauxilliary.pnladl.mailservererror", "airreservations");
			}
		} else {
			throw new ModuleException("reservationauxilliary.palcal.nopalcaltext", "airreservations");
		}

	}

	// used for resending.
	public void resendExistingManualPNLADL(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus, String[] sitaaddresses, String mailserver, String userID,
			String flightNumber, Date dateOfFlight) throws ModuleException {
		String sentPNLADLText = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getSavedPnlAdl(flightID, timestamp,
				sitaAddress, departureStation, messageType, transmissionStatus);
		if (sentPNLADLText != null && !sentPNLADLText.isEmpty()) {
			try {
				
				List<String> allEmailSenderList =  new ArrayList<String>();
				
				// PNL ADL text is available
				String pnlADLMailExtension = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
						.getPnlAdlMailExtension();
				List<String> pnlList = getMessageTextParts(sentPNLADLText, messageType);
				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
				HashMap<String, String> sitaStatusMap = new HashMap<String, String>();
				
				Map<String, String> legNumberWiseOndMap = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getFlightLegs(flightID);
				String ond = PnlAdlUtil.getOndFromDepartureAirport(departureStation, legNumberWiseOndMap);
				
				Map<String, List<String>> sitaAddressesMap = PnlAdlUtil.getSitaAddressMap(departureStation, flightNumber, ond,
						carrierCode, Arrays.asList(sitaaddresses), DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
				List<String> sitaAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				List<String> sitaTexAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
				List<String> arincAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
				
				allEmailSenderList.addAll(sitaAddressList);
				allEmailSenderList.addAll(arincAddressList);
				
				FlightInformation flightInfoElement = new FlightInformation();
				flightInfoElement.setFlightNumber(flightNumber);
				flightInfoElement.setBoardingAirport(departureStation);
				flightInfoElement.setFlightDate(dateOfFlight);
				int partNumber = 1;
				for (Iterator<String> pnlListIter = pnlList.iterator(); pnlListIter.hasNext();) {
					String pnlText = pnlListIter.next();
					if (!allEmailSenderList.isEmpty()) {
						
						for (String address : allEmailSenderList) {

							String pnlTestWithArincAddres = pnlText;

							if (arincAddressList.contains(address)) {

								String arincSenderAddress = ((AirReservationConfig) ReservationModuleUtils.getInstance()
										.getModuleConfig()).getArincSenderAddress();
								String[] arincAddressWithMailExt = extractEmailUser(arincSenderAddress);
								String[] recieverWithMailExt = extractEmailUser(sitaAddress);
								pnlTestWithArincAddres = recieverWithMailExt[0].toUpperCase() + "\n."
										+ arincAddressWithMailExt[0].toUpperCase() + "\n" + pnlText;
							}

							String fullyQualifiedAddress = "";
							boolean isAlternativeSender = false;
							if (address.indexOf("@") != -1) {
								fullyQualifiedAddress = address;
								isAlternativeSender = true;
							} else {
								fullyQualifiedAddress = address + pnlADLMailExtension;
							}
							sendTextViAEmail(fullyQualifiedAddress, mailserver, pnlTestWithArincAddres, messageType,
									isAlternativeSender);
						}
					}
					List<String> fileNames = new ArrayList<String>();
					String path = MessageComposerApiUtils.generateFileName(flightInfoElement, messageType, partNumber);
					fileNames.add(path);
					PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddressList, fileNames, messageType, sitaStatusMap, pnlText);

					partNumber++;
				}
				auditMessageTransactionHistory(flightID, sitaAddress, departureStation, messageType, transmissionStatus, userID,
						TasksUtil.MANUALLY_RESEND_PNL_ADL, "xbe");

			} catch (Exception e) {
				throw new ModuleException(e, "reservationauxilliary.pnladl.mailservererror", "airreservations");
			}
		} else {
			throw new ModuleException("reservationauxilliary.pnladl.nopnladltext", "airreservations");
		}

	}

	/** USed when resending pnl / adl **/
	private List<String> getMessageTextParts(String pnlADLOrgText, String messageType) {
		String pnlADLText = pnlADLOrgText;
		String partSeperator = "ENDPART";
		List<String> pnlsList = new ArrayList<String>();
		String pnlText = "";
		int partCount = 1;
		int startPosition = 0;
		int endPosition = 0;
		if (pnlADLText.indexOf(partSeperator) > 0) {
			// Need to break the PNL / ADL into parts
			while (partCount > 0) {
				startPosition = pnlADLText.indexOf(messageType);
				endPosition = pnlADLText.indexOf(partSeperator + partCount);
				if (endPosition > 0) {
					endPosition += (partSeperator + partCount).length();
					pnlText = pnlADLText.substring(startPosition, endPosition + 1);
					partCount++;
					pnlsList.add(pnlText);
					pnlADLText = pnlADLText.substring(endPosition);
				} else {
					break;
				}
			}
			// Get the rest of the PNL / ADL text
			pnlText = pnlADLText.substring(startPosition);
			pnlsList.add(pnlText);
		} else {
			// No need to break. Send as it is
			pnlsList.add(pnlADLOrgText);
		}
		return pnlsList;
	}

	// used for resending.
	private boolean sendTextViAEmail(String sitaaddress, String mailserver, String emailText, String emailSubject,
			boolean isAlternativeSender) throws Exception {
		try {
			SendMailUsingAuthentication smtpMailSender = new SendMailUsingAuthentication();
			String sitalist[] = { sitaaddress };
			smtpMailSender.postMail(sitalist, emailSubject, emailText, isAlternativeSender);
		} catch (Exception e) {
			log.error("Exception is " + e);
			throw e;
		}
		return true;
	}

	private void auditMessageTransactionHistory(int flightID, String sitaAddress, String departureStation, String messageType,
			String transmissionStatus, String userID, String taskCode, String appCode) throws ModuleException {
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		Audit audit = new Audit();
		LinkedHashMap<String, String> mapContents = new LinkedHashMap<String, String>();
		mapContents.put(PNLADLHistoryAuditConstants.FLIGHT_ID, Integer.toString(flightID));
		mapContents.put(PNLADLHistoryAuditConstants.SITA_ADDRESS, sitaAddress);
		mapContents.put(PNLADLHistoryAuditConstants.DEPARTURE_STATION, departureStation);
		mapContents.put(PNLADLHistoryAuditConstants.MESSAGE_TYPE, messageType);
		mapContents.put(PNLADLHistoryAuditConstants.TRANSMISSION_STATUS, transmissionStatus);
		audit.setUserId(userID);
		audit.setTimestamp(CalendarUtil.getCurrentZuluDateTime());
		audit.setTaskCode(taskCode);
		audit.setAppCode(appCode);

		auditorBD.audit(audit, mapContents);
	}
	
	private static String[] extractEmailUser(String emailAddress) {

		String[] eamilUser = null;
		if (emailAddress.indexOf("@") != -1) {
			eamilUser = emailAddress.split("@");
		} else {
			String[] userArray = { emailAddress };
			eamilUser = userArray;
		}

		return eamilUser;
	}

	/**
	 * @param flightId
	 * @throws ModuleException
	 */
	public void deriveAutoCheckinSeatMap(int flightId) throws ModuleException {

		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Collection<PaxAutomaticCheckinTO> paxAutoCheckinToList = new ArrayList<PaxAutomaticCheckinTO>();
		int blockedSeatRows = Integer.parseInt(ReservationModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.AUTO_CHCK_SEAT_ROW_BLOCK));
		try {
			paxAutoCheckinToList = auxilliaryDAO.getPaxWiseAutoCheckinInfo(flightId);
			Map<String, Collection<PaxAutomaticCheckinTO>> pnrWiseAutocheckinPaxList = new LinkedHashMap<String, Collection<PaxAutomaticCheckinTO>>();
			paxAutoCheckinCollectionToMap(paxAutoCheckinToList, pnrWiseAutocheckinPaxList);

			for (Map.Entry<String, Collection<PaxAutomaticCheckinTO>> pnrWiseAutoCheckInPax : pnrWiseAutocheckinPaxList
					.entrySet()) {

				Collection<PaxAutomaticCheckinTO> paxAutoCheckInList = pnrWiseAutoCheckInPax.getValue();
				LinkedHashMap<Integer, Integer> blockedSeatIds = new LinkedHashMap<Integer, Integer>();

				long sitTogetherCount = paxAutoCheckInList
						.stream()
						.filter(paxAutoCheckIn -> (paxAutoCheckIn.getSitTogetherPnrPaxId() != null && paxAutoCheckIn
								.getSitTogetherPnrPaxId() != 0)).count();
				Map<Integer, String> seatTypePreferences = new LinkedHashMap<Integer, String>();
				Collection<FlightSeatStatusTO> flightsAvailableSeats = auxilliaryDAO.getFlightAvailableSeatMap(flightId,
						blockedSeatRows);

				if (sitTogetherCount == 0) {
					// individual Pax seat map Logic
					paxAutoCheckInList
							.stream()
							.filter(paxAutoCheckInFilter -> (paxAutoCheckInFilter.getFlightAmSeatId() == null || paxAutoCheckInFilter
									.getFlightAmSeatId() == 0))
							.forEachOrdered(
									paxAutoCheckIn -> {
										seatTypePreferences.put(paxAutoCheckIn.getPnrPaxSegAutoCheckinId(),
												paxAutoCheckIn.getSeatTypePreference());
									});

					blockedSeatIds = getIndividualAutoCheckInPaxSeatId(flightsAvailableSeats, seatTypePreferences);
				} else {
					// for the sit together logic
					Optional<PaxAutomaticCheckinTO> firstAutoCheckinPax = paxAutoCheckInList.stream().findFirst();
					if (firstAutoCheckinPax.isPresent()) {
						PaxAutomaticCheckinTO paxAutoCheckin = firstAutoCheckinPax.get();
						// sit together logic with first pax purchased the seat
						if (paxAutoCheckin.getFlightAmSeatId() != null && paxAutoCheckin.getFlightAmSeatId() != 0) {
							blockedSeatIds = getAutoCheckinPaxSitTogtherSeatsByFirstPaxSeatId(paxAutoCheckin, sitTogetherCount,
									paxAutoCheckInList, flightsAvailableSeats);
						} else {
							blockedSeatIds = getAutoCheckInPaxSitTogtherSeats(paxAutoCheckin.getSeatTypePreference(),
									sitTogetherCount, paxAutoCheckInList, flightsAvailableSeats);
						}

					}

					if (blockedSeatIds == null || blockedSeatIds.size() == 0) {
						paxAutoCheckInList
								.stream()
								.skip(1)
								.filter(paxFilter -> (paxFilter.getFlightAmSeatId() == null || paxFilter.getFlightAmSeatId() == 0))
								.forEachOrdered(
										paxAutoCheckIn -> {
											seatTypePreferences.put(paxAutoCheckIn.getPnrPaxSegAutoCheckinId(),
													paxAutoCheckIn.getSeatTypePreference());
										});
						blockedSeatIds = getIndividualAutoCheckInPaxSeatId(flightsAvailableSeats, seatTypePreferences);
					}

				}
				// Update the flight Seat id for Auto Checkin seat derived PNRs
				if (blockedSeatIds != null) {
					//Update Infant Seat ID with Parent Seat ID
					updateInfantSeatId(blockedSeatIds);
					auxilliaryDAO.updatePaxAutoCheckinSeats(blockedSeatIds);
				}
			}

		} catch (CommonsDataAccessException me) {
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		} catch (Exception me) {
			throw new ModuleException(me, "module.runtime.error");
		}

	}

	private void updateInfantSeatId(LinkedHashMap<Integer, Integer> blockedSeatIds) {
		LinkedHashMap<Integer, Integer> blockedSeatAdultIdMap =new LinkedHashMap<Integer, Integer>(blockedSeatIds);
		ReservationAuxilliaryDAO auxilliaryDAO =ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		for(Map.Entry<Integer,Integer> blockedAdultSeat : blockedSeatIds.entrySet()){
			Integer infantPPSACId = auxilliaryDAO.getInfantPpsacId(blockedAdultSeat.getKey());
			if(infantPPSACId != null  && infantPPSACId > 0){
				blockedSeatAdultIdMap.put(infantPPSACId, blockedAdultSeat.getValue());
			}
		}
		if(blockedSeatAdultIdMap != null && blockedSeatAdultIdMap.size()>0){
			blockedSeatIds.putAll(blockedSeatAdultIdMap);
		}
	}


	private LinkedHashMap<Integer, Integer> getAutoCheckinPaxSitTogtherSeatsByFirstPaxSeatId(
			PaxAutomaticCheckinTO paxAutoCheckin, long sitTogetherCount, Collection<PaxAutomaticCheckinTO> paxAutoCheckInList,
			Collection<FlightSeatStatusTO> flightsAvailableSeats) {

		int maxColId = getMaxColRowIds(flightsAvailableSeats, AirinventoryCustomConstants.AutoCheckinConstants.COL);
		int maxRowId = getMaxColRowIds(flightsAvailableSeats, AirinventoryCustomConstants.AutoCheckinConstants.ROW);

		LinkedHashMap<String, FlightSeatStatusTO> availableSeatMap = new LinkedHashMap<String, FlightSeatStatusTO>();
		flightsAvailableSeats
				.stream()
				.filter(flightSeat -> AirinventoryCustomConstants.FlightSeatStatuses.VACANT.equalsIgnoreCase(flightSeat
						.getStatus())).forEach(seat -> {
					String colRowID = String.valueOf(seat.getColId()) + "-" + String.valueOf(seat.getRowGroupId());
					availableSeatMap.put(colRowID, seat);
				});
		Optional<FlightSeatStatusTO> firstPaxSeatObj = flightsAvailableSeats
				.stream()
				.filter(firstPaxSeat -> (firstPaxSeat.getFlightAmSeatId().intValue() == paxAutoCheckin.getFlightAmSeatId()
						.intValue())).findFirst();
		List<Integer> autoCheckinPaxIds = new ArrayList<Integer>();
		List<String[]> sitTogetherSeatChoices = new ArrayList<String[]>();
		LinkedHashMap<Integer, Integer> cnfSeatSeq = new LinkedHashMap<Integer, Integer>();

		int firstPaxId = paxAutoCheckInList.stream().findFirst().map(pax -> pax.getPnrPaxSegAutoCheckinId()).get();
		autoCheckinPaxIds.add(firstPaxId);
		List<Integer> restPaxIds = paxAutoCheckInList
				.stream()
				.skip(1)
				.filter(paxAutoCheckInFilter -> (paxAutoCheckInFilter.getFlightAmSeatId() == null || paxAutoCheckInFilter
						.getFlightAmSeatId() == 0)).map(paxAutoCheckIn -> paxAutoCheckIn.getPnrPaxSegAutoCheckinId())
				.collect(Collectors.toList());
		autoCheckinPaxIds.addAll(restPaxIds);
		int count = autoCheckinPaxIds.size();

		if (firstPaxSeatObj.isPresent()) {
			int colId = firstPaxSeatObj.get().getColId();
			int rowId = firstPaxSeatObj.get().getRowGroupId();
			String seatIdArray[] = new String[count];
			String seatIdReverseArray[] = null;
			Integer[] seatRow = { colId, rowId };
			seatIdArray = getSeatArray(seatRow, count, autoCheckinPaxIds, maxColId, maxRowId);
			if (rowId > 1) {
				seatIdReverseArray = getSeatArrayReverse(seatRow, count, autoCheckinPaxIds, maxColId, maxRowId);
			}
			if (seatIdArray != null) {
				sitTogetherSeatChoices.add(seatIdArray);
			} else if (seatIdReverseArray != null) {
				sitTogetherSeatChoices.add(seatIdReverseArray);
			}
			if (log.isDebugEnabled()) {
				log.debug("list of Auto checkin seat sit together choices for one PNR--" + sitTogetherSeatChoices.size());
			}

			Iterator<String[]> seatOptionItr = sitTogetherSeatChoices.stream().iterator();
			while (seatOptionItr.hasNext()) {
				String[] seatOption = seatOptionItr.next();
				seatOption = Arrays.copyOfRange(seatOption, 1, seatOption.length);
				cnfSeatSeq = getSeatSequence(seatOption, availableSeatMap);
				if (cnfSeatSeq != null) {
					break;
				}
			}
		}
		// This code will execute when the seat prefer not met with parent pax flightseatID
		if (cnfSeatSeq == null) {
			Map<Integer, String> seatTypePrefs = new LinkedHashMap<Integer, String>();
			restPaxIds.stream().forEach(paxAutoCheckIn -> {
				seatTypePrefs.put(paxAutoCheckIn, "");
			});

			cnfSeatSeq = getIndividualAutoCheckInPaxSeatId(flightsAvailableSeats, seatTypePrefs);
		}

		return cnfSeatSeq;
	}

	/**
	 * @param paxAutoCheckInTOList
	 * @param pnrWiseAutoCheckinPaxs
	 */
	private void paxAutoCheckinCollectionToMap(Collection<PaxAutomaticCheckinTO> paxAutoCheckInTOList,
			Map<String, Collection<PaxAutomaticCheckinTO>> pnrWiseAutoCheckinPaxs) {
		paxAutoCheckInTOList.forEach(paxAutoCheckinTO -> {
			String pnr = paxAutoCheckinTO.getPnr();
			Collection<PaxAutomaticCheckinTO> paxAutoCheckInList = new ArrayList<PaxAutomaticCheckinTO>();
			if (!pnrWiseAutoCheckinPaxs.containsKey(paxAutoCheckinTO.getPnr())) {
				paxAutoCheckInList.add(paxAutoCheckinTO);
				pnrWiseAutoCheckinPaxs.put(pnr, paxAutoCheckInList);
			} else {
				paxAutoCheckInList = pnrWiseAutoCheckinPaxs.get(pnr);
				paxAutoCheckInList.add(paxAutoCheckinTO);
				pnrWiseAutoCheckinPaxs.put(pnr, paxAutoCheckInList);
			}
		});
	}

	private LinkedHashMap<Integer, Integer> getAutoCheckInPaxSitTogtherSeats(String seatTypePreference, long count,
			Collection<PaxAutomaticCheckinTO> autoCheckinPaxList, Collection<FlightSeatStatusTO> flightsAvailableSeats) {

		int maxColId = getMaxColRowIds(flightsAvailableSeats, AirinventoryCustomConstants.AutoCheckinConstants.COL);
		int maxRowId = getMaxColRowIds(flightsAvailableSeats, AirinventoryCustomConstants.AutoCheckinConstants.ROW);

		LinkedHashMap<String, FlightSeatStatusTO> availableSeatMap = new LinkedHashMap<String, FlightSeatStatusTO>();
		flightsAvailableSeats
				.stream()
				.filter(flightSeat -> AirinventoryCustomConstants.FlightSeatStatuses.VACANT.equalsIgnoreCase(flightSeat
						.getStatus())).forEach(seat -> {
					String colRowID = String.valueOf(seat.getColId()) + "-" + String.valueOf(seat.getRowGroupId());
					availableSeatMap.put(colRowID, seat);
				});
		List<Integer> autoCheckinPaxIds = new ArrayList<Integer>();
		List<String[]> sitTogetherSeatChoices = new ArrayList<String[]>();
		LinkedHashMap<Integer, Integer> cnfSeatSeq = new LinkedHashMap<Integer, Integer>();

		int firstAutoCheckinPaxId = autoCheckinPaxList.stream().findFirst()
				.map(autoCheckinPax -> autoCheckinPax.getPnrPaxSegAutoCheckinId()).get();
		autoCheckinPaxIds.add(firstAutoCheckinPaxId);
		List<Integer> restPaxIds = autoCheckinPaxList
				.stream()
				.skip(1)
				.filter(paxAutoCheckInFilter -> (paxAutoCheckInFilter.getFlightAmSeatId() == null || paxAutoCheckInFilter
						.getFlightAmSeatId() == 0)).map(paxAutoCheckIn -> paxAutoCheckIn.getPnrPaxSegAutoCheckinId())
				.collect(Collectors.toList());
		autoCheckinPaxIds.addAll(restPaxIds);
		int autoCheckinPaxcount = autoCheckinPaxIds.size();
		Collection<FlightSeatStatusTO> availFirstPaxPrefSeat = flightsAvailableSeats
				.stream()
				.filter(flightSeat -> AirinventoryCustomConstants.FlightSeatStatuses.VACANT.equalsIgnoreCase(flightSeat
						.getStatus()) && flightSeat.getSeatType().equalsIgnoreCase(seatTypePreference))
				.collect(Collectors.toList());

		for (FlightSeatStatusTO seatTO : availFirstPaxPrefSeat) {
			int colId = seatTO.getColId();
			int rowId = seatTO.getRowGroupId();
			String seatIdArray[] = new String[autoCheckinPaxcount];
			String seatIdReverseArray[] = null;
			Integer[] seatRow = { colId, rowId };
			seatIdArray = getSeatArray(seatRow, autoCheckinPaxcount, autoCheckinPaxIds, maxColId, maxRowId);
			if (rowId > 1) {
				seatIdReverseArray = getSeatArrayReverse(seatRow, autoCheckinPaxcount, autoCheckinPaxIds, maxColId, maxRowId);
			}
			if (seatIdArray != null) {
				sitTogetherSeatChoices.add(seatIdArray);
			} else if (seatIdReverseArray != null) {
				sitTogetherSeatChoices.add(seatIdReverseArray);
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("list of Auto checkin seat sit together choices for one PNR--" + sitTogetherSeatChoices.size());
		}

		Iterator<String[]> seatTogtherIterator = sitTogetherSeatChoices.stream().iterator();

		while (seatTogtherIterator.hasNext()) {
			String[] seatChoiceString = seatTogtherIterator.next();
			cnfSeatSeq = getSeatSequence(seatChoiceString, availableSeatMap);
			if (cnfSeatSeq != null) {
				break;
			}
		}
		return cnfSeatSeq;
	}

	/**
	 * @param flightsAvailableSeats
	 * @param string
	 */
	private int getMaxColRowIds(Collection<FlightSeatStatusTO> flightsAvailableSeats, String fieldType) {
		int maxId;
		FlightSeatStatusTO maxColFlightSeatObj;
		if (AirinventoryCustomConstants.AutoCheckinConstants.COL.equals(fieldType)) {
			maxColFlightSeatObj = flightsAvailableSeats.stream().max(Comparator.comparing(FlightSeatStatusTO::getColId)).get();
			maxId = (maxColFlightSeatObj != null) ? maxColFlightSeatObj.getColId() : 0;
		} else {
			maxColFlightSeatObj = flightsAvailableSeats.stream().max(Comparator.comparing(FlightSeatStatusTO::getRowGroupId))
					.get();
			maxId = (maxColFlightSeatObj != null) ? maxColFlightSeatObj.getRowGroupId() : 0;
		}
		return maxId;
	}

	/**
	 * @param seatSequence
	 * @param availableSeatMap
	 * @return
	 */
	private LinkedHashMap<Integer, Integer> getSeatSequence(String[] seatSequence,
			Map<String, FlightSeatStatusTO> availableSeatMap) {
		LinkedHashMap<Integer, Integer> blockSeatIds = new LinkedHashMap<Integer, Integer>();
		for (String sequenceString : seatSequence) {
			String[] sequence = sequenceString.split(":");
			if (!availableSeatMap.containsKey(sequence[0])) {
				return null;
			} else {
				blockSeatIds.put(Integer.valueOf(sequence[1]), availableSeatMap.get(sequence[0]).getFlightAmSeatId());
			}
		}
		return blockSeatIds;
	}

	/**
	 * @param seatRow
	 * @param count
	 * @param autoCheckinPaxIdList
	 * @param maxRowId
	 * @param maxColId
	 * @return
	 */
	private static String[] getSeatArrayReverse(Integer[] seatRow, int count, List<Integer> autoCheckinPaxIdList, int maxColId,
			int maxRowId) {
		String[] seatIdArray = new String[count];
		seatIdArray[0] = seatRow[0] + "-" + seatRow[1] + ":" + autoCheckinPaxIdList.get(0);
		for (int i = 1; i < count; i++) {
			seatRow = prevSeat(seatRow, seatIdArray, maxRowId);
			if (seatRow == null) {
				return null;
			}
			seatIdArray[i] = seatRow[0] + "-" + seatRow[1] + ":" + autoCheckinPaxIdList.get(i);
		}
		return seatIdArray;
	}

	/**
	 * @param seatInt
	 * @param idArray
	 * @param maxRowId
	 * @param maxColId
	 * @return
	 */
	private static Integer[] prevSeat(Integer[] seatInt, String[] idArray, int maxRowId) {
		Integer[] seatRow = new Integer[2];
		if (seatInt[0] > 0 && seatInt[1] > 0) {
			int rowId = seatInt[1];
			int colId = seatInt[0];
			int row = rowId;
			if (row > 1) {
				rowId--;
				seatRow[0] = colId;
				seatRow[1] = rowId;
			} else {
				rowId = maxRowId;
				if (colId > 1) {
					colId--;
					seatRow[0] = colId;
					seatRow[1] = rowId;
				} else {
					seatRow[0] = 0;
					seatRow[1] = 0;
					return null;
				}
			}
		} else {
			return null;
		}

		return seatRow;
	}

	/**
	 * @param count
	 * @param colId
	 * @param rowId
	 * @param count
	 * @param maxRowId
	 * @param maxColId
	 * @param id
	 * @return
	 */
	private static String[] getSeatArray(Integer[] seatRow, int count, List<Integer> autoCheckInPaxIdList, int maxColId,
			int maxRowId) {
		String[] seatIdArray = new String[count];
		seatIdArray[0] = seatRow[0] + "-" + seatRow[1] + ":" + autoCheckInPaxIdList.get(0);
		for (int i = 1; i < count; i++) {
			seatRow = nextSeat(seatRow, seatIdArray, maxColId, maxRowId);
			if (seatRow == null) {
				return null;
			}
			seatIdArray[i] = seatRow[0] + "-" + seatRow[1] + ":" + autoCheckInPaxIdList.get(i);
		}
		return seatIdArray;
	}

	/**
	 * @param seatInt
	 * @param idArray
	 * @param maxRowId
	 * @param maxColId
	 * @return
	 */
	private static Integer[] nextSeat(Integer[] seatInt, String[] idArray, int maxColId, int maxRowId) {
		Integer[] seatRow = new Integer[2];
		if (seatInt[0] > 0 && seatInt[1] > 0) {
			int rowId = seatInt[1];
			int colId = seatInt[0];
			int row = rowId;
			if (row < maxRowId) {
				rowId++;
				seatRow[0] = colId;
				seatRow[1] = rowId;
			} else {
				rowId = 1;
				if (colId < maxColId) {
					colId++;
					seatRow[0] = colId;
					seatRow[1] = rowId;
				} else {
					seatRow[0] = 0;
					seatRow[1] = 0;
				}
			}
		} else {
			return null;
		}
		return seatRow;
	}

	/**
	 * @param flightsAvailableSeats
	 * @param seatTypePreferences
	 * @return
	 */
	private static LinkedHashMap<Integer, Integer> getIndividualAutoCheckInPaxSeatId(
			Collection<FlightSeatStatusTO> flightsAvailableSeats, Map<Integer, String> seatTypePreferences) {
		LinkedHashMap<Integer, Integer> blockedSeatIds = new LinkedHashMap<Integer, Integer>();
		seatTypePreferences.entrySet().forEach(
				seatPreferenceType -> {
					Collection<FlightSeatStatusTO> tmpFlightsAvailableSeats = flightsAvailableSeats;
					Optional<FlightSeatStatusTO> seatObjStream = tmpFlightsAvailableSeats
							.stream()
							.filter(flightSeat -> (AirinventoryCustomConstants.FlightSeatStatuses.VACANT
									.equalsIgnoreCase(flightSeat.getStatus()) && (StringUtil.isNullOrEmpty(seatPreferenceType
									.getValue()) || flightSeat.getSeatType().equalsIgnoreCase(seatPreferenceType.getValue()))))
							.findFirst();

					if (seatObjStream.isPresent()) {
						getSeatObjFromStream(flightsAvailableSeats, blockedSeatIds, seatPreferenceType, seatObjStream);
						return;
					} else {
						Optional<FlightSeatStatusTO> firstAvialableSeatStream = tmpFlightsAvailableSeats.stream().findFirst();
						if (firstAvialableSeatStream.isPresent()) {
							getSeatObjFromStream(flightsAvailableSeats, blockedSeatIds, seatPreferenceType,
									firstAvialableSeatStream);
							return;
						}
					}

				});
		return blockedSeatIds;
	}

	/**
	 * @param flightsAvailableSeats
	 * @param blockedSeatIds
	 * @param seatPreferenceType
	 * @param seatAvlStream
	 */
	private static void getSeatObjFromStream(Collection<FlightSeatStatusTO> flightsAvailableSeats,
			LinkedHashMap<Integer, Integer> blockedSeatIds, Entry<Integer, String> seatPreferenceType,
			Optional<FlightSeatStatusTO> seatObjStream) {
		FlightSeatStatusTO flightSeatStatus = seatObjStream.get();
		blockedSeatIds.put(seatPreferenceType.getKey(), flightSeatStatus.getFlightAmSeatId());
		flightsAvailableSeats.remove(flightSeatStatus);
	}

}