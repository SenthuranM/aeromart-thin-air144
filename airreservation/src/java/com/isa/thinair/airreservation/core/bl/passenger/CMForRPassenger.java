/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.passenger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for creating a modification object for remove passenger
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cMForRPassenger"
 */
public class CMForRPassenger extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CMForRPassenger.class);

	/**
	 * Execute method of the CMForRPassenger command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		String newPnr = (String) this.getParameter(CommandParamNames.PNR);
		String oldPnr = (String) this.getParameter(CommandParamNames.OLD_PNR);
		CustomChargesTO customChargesTO = (CustomChargesTO) this.getParameter(CommandParamNames.CUSTOM_CHARGES);
		Collection<ReservationPax> colMovingPassengers = (Collection<ReservationPax>) this
				.getParameter(CommandParamNames.MOVING_PASSENGERS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		String userNotes = this.getParameter(CommandParamNames.USER_NOTES, String.class);

		// Checking params
		this.validateParams(newPnr, oldPnr, colMovingPassengers, credentialsDTO);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		// Compose Modification object
		Collection<ReservationAudit> colReservationAudit = this.composeAudit(oldPnr, newPnr, colMovingPassengers, credentialsDTO,
				userNotes);
		if (customChargesTO.getCustomAdultCCharge() != null || customChargesTO.getCustomChildCCharge() != null) {
			ReservationAudit reservavtionAudit = new ReservationAudit();
			reservavtionAudit.setPnr(oldPnr);
			if (customChargesTO.getCustomAdultCCharge() != null)
				reservavtionAudit.addContentMap("adultCnxCharge", customChargesTO.getCustomAdultCCharge().toString());
			else
				reservavtionAudit.addContentMap("adultCnxCharge", "-");
			if (customChargesTO.getCustomChildCCharge() != null)
				reservavtionAudit.addContentMap("childCnxCharge", customChargesTO.getCustomChildCCharge().toString());
			else
				reservavtionAudit.addContentMap("childCnxCharge", "-");
			if (customChargesTO.getCustomInfantCCharge() != null)
				reservavtionAudit.addContentMap("infantCnxCharge", customChargesTO.getCustomInfantCCharge().toString());
			else
				reservavtionAudit.addContentMap("infantCnxCharge", "-");
			reservavtionAudit.setModificationType(AuditTemplateEnum.OVERRIDE_CNX_CHARGE.getCode());
			colReservationAudit.add(reservavtionAudit);
		}

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Compose Audit
	 * 
	 * @param oldPnr
	 * @param newPnr
	 * @param colMovingPassengers
	 * @param credentialsDTO
	 * @return
	 */
	private Collection<ReservationAudit> composeAudit(String oldPnr, String newPnr,
			Collection<ReservationPax> colMovingPassengers, CredentialsDTO credentialsDTO, String userNotes) {

		// Setting the split passenger information
		StringBuffer passengeInfo = new StringBuffer();
		Iterator<ReservationPax> itReservationPax = colMovingPassengers.iterator();
		ReservationPax reservationPax;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			passengeInfo.append(" " + BeanUtils.nullHandler(reservationPax.getTitle()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getFirstName()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getLastName()) + " - " + reservationPax.getPaxTypeDescription());
		}

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		ReservationAudit reservationAuditOld = this.composeOldAudit(oldPnr, newPnr, passengeInfo.toString(), credentialsDTO,
				userNotes);
		ReservationAudit reservationAuditNew = this.composeNewAudit(oldPnr, newPnr, passengeInfo.toString(), credentialsDTO,
				userNotes);

		colReservationAudit.add(reservationAuditOld);
		colReservationAudit.add(reservationAuditNew);

		return colReservationAudit;
	}

	/**
	 * Compose the old audit information
	 * 
	 * @param oldPnr
	 * @param newPnr
	 * @param passengerInfo
	 * @param credentialsDTO
	 * @return
	 */
	private ReservationAudit composeOldAudit(String oldPnr, String newPnr, String passengerInfo, CredentialsDTO credentialsDTO,
			String userNotes) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(oldPnr);
		reservationAudit.setModificationType(AuditTemplateEnum.REMOVED_PASSENGER.getCode());
		reservationAudit.setUserNote(userNotes);

		// Setting the new pnr information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.RemovedPassenger.NEW_PNR, newPnr);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.RemovedPassenger.PASSENGER_DETAILS, passengerInfo);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.RemovedPassenger.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.RemovedPassenger.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Compose the new audit information
	 * 
	 * @param oldPnr
	 * @param newPnr
	 * @param passengerInfo
	 * @param credentialsDTO
	 * @return
	 */
	private ReservationAudit composeNewAudit(String oldPnr, String newPnr, String passengerInfo, CredentialsDTO credentialsDTO,
			String userNotes) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(newPnr);
		reservationAudit.setModificationType(AuditTemplateEnum.ADDED_RESERVATION_FOR_REMOVED_PASSENGERS.getCode());
		reservationAudit.setUserNote(userNotes);

		// Setting the new pnr information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedReservationForRemovedPassengers.OLD_PNR, oldPnr);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedReservationForRemovedPassengers.PASSENGER_DETAILS,
				passengerInfo);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedReservationForRemovedPassengers.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedReservationForRemovedPassengers.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param newPnr
	 * @param oldPnr
	 * @param colMovingPassengers
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String newPnr, String oldPnr, Collection<ReservationPax> colMovingPassengers,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (newPnr == null || oldPnr == null || colMovingPassengers == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
