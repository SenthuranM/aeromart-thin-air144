package com.isa.thinair.airreservation.core.bl.common;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferUtil;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author manoj
 *
 */
public class AirportTransferAdaptor {

	private static final String RESERVED_STATUS = "RES";

	public static Collection<ReservationPaxSegAirportTransfer>
			addModifyPaxApts(Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferTos, CredentialsDTO credentialsDTO)
					throws ModuleException {
		Collection<ReservationPaxSegAirportTransfer> passengerAPTs = new ArrayList<ReservationPaxSegAirportTransfer>();

		for (Collection<PaxAirportTransferTO> colPaxAptTOs : paxAirportTransferTos.values()) {
			for (PaxAirportTransferTO paxAirportTransferTo : colPaxAptTOs) {
				ReservationPaxSegAirportTransfer passengerAirportTransfer = new ReservationPaxSegAirportTransfer();
				passengerAirportTransfer.setPnrPaxId(paxAirportTransferTo.getPnrPaxId());
				passengerAirportTransfer.setChargeAmount(paxAirportTransferTo.getChargeAmount());
				passengerAirportTransfer.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
				passengerAirportTransfer.setUserId(credentialsDTO.getUserId());
				passengerAirportTransfer.setPnrSegId(paxAirportTransferTo.getPnrSegId());
				passengerAirportTransfer.setPpfId(paxAirportTransferTo.getPpfId());
				passengerAirportTransfer.setAirportTransferId(paxAirportTransferTo.getAirportTransferId());
				passengerAirportTransfer.setAirportCode(paxAirportTransferTo.getAirportCode());
				passengerAirportTransfer.setBookingTimestamp(paxAirportTransferTo.getBookingTimeStamp());
				try {
					Date requestTimeStamp = CalendarUtil.getParsedTime(paxAirportTransferTo.getRequestTimeStamp(),
							CalendarUtil.PATTERN11);
					passengerAirportTransfer.setRequestTimeStamp(requestTimeStamp);
				} catch (ParseException e) {
					throw new ModuleException("airreservations.airport.tranfer.date.invalid");

				}
				passengerAirportTransfer.setAddress(paxAirportTransferTo.getAddress());
				passengerAirportTransfer.setContactNo(paxAirportTransferTo.getContactNo());
				passengerAirportTransfer.setPickupType(paxAirportTransferTo.getPickupType());
				passengerAirportTransfer.setStatus(RESERVED_STATUS);

				passengerAPTs.add(passengerAirportTransfer);
			}
		}
		if (passengerAPTs.size() > 0) {
			ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO.saveOrUpdate(passengerAPTs);
		}
		return passengerAPTs;
	}

	public static Collection<ReservationPaxSegAirportTransfer>
			cancelPaxApts(Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferToRemove, Reservation reservation)
					throws ModuleException {
		Collection<ReservationPaxSegAirportTransfer> passengerAPTs = new ArrayList<ReservationPaxSegAirportTransfer>();
		List<Integer> pnrPaxSegAptIdList = new ArrayList<Integer>();
		for (Collection<PaxAirportTransferTO> colPaxAptTOs : paxAirportTransferToRemove.values()) {
			for (PaxAirportTransferTO paxAirportTransferTo : colPaxAptTOs) {
				Integer aptId = null;
				if (reservation.getAirportTransfers() != null && !reservation.getAirportTransfers().isEmpty()) {
					for (PaxAirportTransferTO apt : reservation.getAirportTransfers()) {
						if (apt.getPnrPaxId().equals(paxAirportTransferTo.getPnrPaxId())
								&& apt.getPnrSegId().equals(paxAirportTransferTo.getPnrSegId())
								&& apt.getAirportCode().equals(paxAirportTransferTo.getAirportCode())) {
							pnrPaxSegAptIdList.add(apt.getPnrPaxSegAptId());
							aptId = apt.getPnrPaxSegAptId();
						}
					}
				}

				ReservationPaxSegAirportTransfer passengerAirportTransfer = new ReservationPaxSegAirportTransfer();
				passengerAirportTransfer.setPnrPaxSegAirportTransferId(aptId);
				passengerAirportTransfer.setPnrPaxId(paxAirportTransferTo.getPnrPaxId());
				passengerAirportTransfer.setPnrSegId(paxAirportTransferTo.getPnrSegId());
				passengerAirportTransfer.setAirportCode(paxAirportTransferTo.getAirportCode());
				passengerAirportTransfer.setBookingTimestamp(paxAirportTransferTo.getBookingTimeStamp());
				passengerAPTs.add(passengerAirportTransfer);
			}
		}
		if (passengerAPTs.size() > 0) {
			ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO.cancelAirportTransfer(pnrPaxSegAptIdList);
		}
		return passengerAPTs;
	}

	public static void addPassengerAptChargeAndAdjustCredit(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Map<String, Collection<PaxAirportTransferTO>> passengerApt, String userNotes, TrackInfoDTO trackInfoDTO,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxSeq) throws ModuleException {
		// Adding charge entry
		Map<String, ReservationPaxOndCharge> paxIdChargeMap = addAptCharge(reservation, passengerApt, credentialsDTO, chgTnxSeq);

		for (String uniqueId : paxIdChargeMap.keySet()) {
			Integer pnrPaxId = Integer.parseInt(AirportTransferUtil.getPnrPaxId(uniqueId));
			adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, paxIdChargeMap.get(uniqueId), credentialsDTO,
					reservation.isEnableTransactionGranularity());
		}
	}

	@SuppressWarnings("unused")
	private static Map<String, ReservationPaxOndCharge> addAptCharge(Reservation reservation,
			Map<String, Collection<PaxAirportTransferTO>> passengerAptMap, CredentialsDTO credentialsDTO,
			ChargeTnxSeqGenerator chgTnxSeq) throws ModuleException {

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		Map<String, ReservationPaxOndCharge> pnrPaxIdCharge = new HashMap<String, ReservationPaxOndCharge>();
		Integer pnrPaxId = null;
		Collection<ReservationPaxFareSegment> paxFareSegments;
		Iterator<ReservationPaxFareSegment> paxFareSegmentsIterater;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				paxFareSegments = reservationPaxFare.getPaxFareSegments();
				paxFareSegmentsIterater = paxFareSegments.iterator();
				Collection<Integer> consumedPaxOndChgIds = new ArrayList<Integer>();
				while (paxFareSegmentsIterater.hasNext()) {
					ReservationPaxFareSegment resPaxFareSegment = (ReservationPaxFareSegment) paxFareSegmentsIterater.next();

					// In cancel segment, we need this
					// if
					// (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resPaxFareSegment.getSegment()
					// .getStatus())) {
					// continue;
					// }

					String uniqueId = AirportTransferUtil.makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
							reservationPaxFare.getPnrPaxFareId(), resPaxFareSegment.getPnrSegId());
					Collection<PaxAirportTransferTO> paxAptTos = passengerAptMap.get(uniqueId);
					int i = 0;
					if (paxAptTos != null) {
						for (PaxAirportTransferTO paxAptTo : paxAptTos) {
							ExternalChgDTO chgDTO = null;
							if (paxAptTo != null) {
								chgDTO = paxAptTo.getChgDTO();
								paxAptTo.setPnrPaxId(reservationPax.getPnrPaxId());
							}

							if (chgDTO != null) {
								chgDTO.setAmount(chgDTO.getAmount());
								PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils
										.getAppliedDiscountInfoForRefund(reservationPaxFare, chgDTO, consumedPaxOndChgIds);

								ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
										.captureReservationPaxOndCharge(chgDTO.getAmount(), null, chgDTO.getChgRateId(),
												chgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, paxDiscInfo,
												null, null, chgTnxSeq.getTnxSequence(reservationPax.getPnrPaxId()));

								if (ReservationApiUtils.isInfantType(reservationPaxFare.getReservationPax())) {
									pnrPaxId = reservationPaxFare.getReservationPax().getParent().getPnrPaxId();
								} else {
									pnrPaxId = reservationPaxFare.getReservationPax().getPnrPaxId();
								}

								pnrPaxIdCharge.put(uniqueId + "|" + i, reservationPaxOndCharge);
								++i;
							}
						}
					}
				}
			}
		}

		if (pnrPaxIdCharge.size() == 0) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		return pnrPaxIdCharge;
	}

	/**
	 * Audit Changes to Apts
	 * 
	 * @param pnr
	 * @param mapAdded
	 * @param mapRemoved
	 * @param credentialsDTO
	 * @param userNote
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static void recordPaxWiseAuditHistoryForModifyApts(String pnr, Map<String, Collection<PaxAirportTransferTO>> mapAdded,
			Map<String, Collection<PaxAirportTransferTO>> mapRemoved, CredentialsDTO credentialsDTO, String userNote,
			Collection[] colResInfo) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.MODIFY_AIRPORT_TRANSFERS.getCode());
		if (mapAdded != null && !mapAdded.isEmpty()) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.ADDED,
					paxAptModifications(mapAdded, colResInfo));
		}
		if (mapRemoved != null && !mapRemoved.isEmpty()) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.REMOVED,
					paxAptModifications(mapRemoved, colResInfo));
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}
		ReservationBO.recordModification(pnr, reservationAudit, userNote, credentialsDTO);
	}

	public static String recordPaxWiseAuditHistoryForModifyAirportTransfers(String pnr,
			Collection<ReservationPaxSegAirportTransfer> flightAirportTransferTos, CredentialsDTO credentialsDTO, String userNote,
			Collection[] colResPax) throws ModuleException {
		if (flightAirportTransferTos != null && !flightAirportTransferTos.isEmpty()) {
			Map<String, Collection<PaxAirportTransferTO>> mapAirportTransferTo = createDummyAirportTransferMap(
					flightAirportTransferTos);
			return paxAptModifications(mapAirportTransferTo, colResPax);
		}
		return null;
	}

	private static Map<String, Collection<PaxAirportTransferTO>>
			createDummyAirportTransferMap(Collection<ReservationPaxSegAirportTransfer> colAirportTransferTos) {
		Map<String, Collection<PaxAirportTransferTO>> mapAirportTransferMap = new HashMap<String, Collection<PaxAirportTransferTO>>();
		for (ReservationPaxSegAirportTransfer paxAirportTransferTO : colAirportTransferTos) {
			String uuid = paxAirportTransferTO.getPnrPaxId().toString() + paxAirportTransferTO.getPnrSegId().toString()
					+ paxAirportTransferTO.getAirportCode();
			if (mapAirportTransferMap.get(uuid) == null) {
				mapAirportTransferMap.put(uuid, new ArrayList<PaxAirportTransferTO>());
			}
			PaxAirportTransferTO apt = new PaxAirportTransferTO();
			apt.setAddress(paxAirportTransferTO.getAddress());
			apt.setAirportCode(paxAirportTransferTO.getAirportCode());
			apt.setAirportTransferId(paxAirportTransferTO.getAirportTransferId());
			apt.setChargeAmount(paxAirportTransferTO.getChargeAmount());
			apt.setContactNo(paxAirportTransferTO.getContactNo());
			apt.setPickupType(paxAirportTransferTO.getPickupType());
			apt.setPnrPaxId(paxAirportTransferTO.getPnrPaxId());
			apt.setPnrPaxSegAptId(paxAirportTransferTO.getPnrPaxSegAirportTransferId());
			apt.setPnrSegId(paxAirportTransferTO.getPnrSegId());
			apt.setPpfId(paxAirportTransferTO.getPpfId());
			apt.setRequestTimeStamp(paxAirportTransferTO.getRequestTimeStamp().toString());
			apt.setSalesChannelCode(paxAirportTransferTO.getSalesChannelCode());
			apt.setUserId(paxAirportTransferTO.getUserId());
			apt.setBookingTimeStamp(paxAirportTransferTO.getBookingTimestamp());
			// apt.setProvider(provider);

			mapAirportTransferMap.get(uuid).add(apt);
		}
		return mapAirportTransferMap;
	}

	private static String paxAptModifications(Map<String, Collection<PaxAirportTransferTO>> airportTransferMap,
			Collection[] colResInfo) {
		Map<Integer, Collection<PaxAirportTransferTO>> mapSortedAirportTransferInfo = segmentAirportTransferInfo(
				airportTransferMap.values());
		Iterator<Map.Entry<Integer, Collection<PaxAirportTransferTO>>> it = mapSortedAirportTransferInfo.entrySet().iterator();
		StringBuilder strAirportTransferData = new StringBuilder();
		Collection[] arrResInfo = colResInfo;
		Map<Integer, String> resSegmentInfo = compileSegmentInfo(arrResInfo[1], airportTransferMap.values());
		while (it.hasNext()) {
			Map.Entry<Integer, Collection<PaxAirportTransferTO>> map = (Map.Entry<Integer, Collection<PaxAirportTransferTO>>) it
					.next();
			Collection<PaxAirportTransferTO> colPaxAirportTransfers = (Collection<PaxAirportTransferTO>) map.getValue();
			String strSegmentCode = "";
			if (resSegmentInfo != null) {
				int intPnrSeg = (Integer) map.getKey();
				strSegmentCode = resSegmentInfo.get(intPnrSeg);
			}
			if (colPaxAirportTransfers != null && !colPaxAirportTransfers.isEmpty()) {
				Set<ReservationPax> resPax = (Set<ReservationPax>) arrResInfo[0];
				Map<Integer, Collection<PaxAirportTransferTO>> paxAirportTransfersMap = setPaxInfo(colPaxAirportTransfers,
						resPax);
				boolean processedAtleastOnce = false;
				strAirportTransferData.append(strSegmentCode + " =");
				for (Integer pnrPaxId : paxAirportTransfersMap.keySet()) {
					Collection<PaxAirportTransferTO> paxAirportTransfersCol = paxAirportTransfersMap.get(pnrPaxId);
					if (processedAtleastOnce) {
						strAirportTransferData.append(", ");
					}
					String paxName = "";
					for (PaxAirportTransferTO paxAirportTransferTo : paxAirportTransfersCol) {
						strAirportTransferData.append("[" + paxAirportTransferTo.getAirportCode() + " "
								+ paxAirportTransferTo.getChargeAmount() + "] ");
						paxName = paxAirportTransferTo.getPaxName();
					}
					strAirportTransferData.append(" - " + paxName);
					processedAtleastOnce = true;
				}
				strAirportTransferData.append(".");
			}
		}
		return strAirportTransferData.toString();
	}

	private static Map<Integer, Collection<PaxAirportTransferTO>>
			segmentAirportTransferInfo(Collection<Collection<PaxAirportTransferTO>> colAirportTransferInfo) {
		Map<Integer, Collection<PaxAirportTransferTO>> segAirportTransferInfo = new LinkedHashMap<Integer, Collection<PaxAirportTransferTO>>();
		for (Collection<PaxAirportTransferTO> colAirportTransferTOs : colAirportTransferInfo) {
			for (PaxAirportTransferTO paxAirportTansferTO : colAirportTransferTOs) {

				Collection<PaxAirportTransferTO> colExtractedAirportTransferTos = (Collection<PaxAirportTransferTO>) segAirportTransferInfo
						.get(paxAirportTansferTO.getPnrSegId());
				if (colExtractedAirportTransferTos != null) {
					colExtractedAirportTransferTos.add(paxAirportTansferTO);
				} else {
					Collection<PaxAirportTransferTO> colSegAirportTransfer = new ArrayList<PaxAirportTransferTO>();
					colSegAirportTransfer.add(paxAirportTansferTO);
					segAirportTransferInfo.put(paxAirportTansferTO.getPnrSegId(), colSegAirportTransfer);
				}
			}
		}

		return segAirportTransferInfo;
	}

	private static Map<Integer, String> compileSegmentInfo(Collection<ReservationSegmentDTO> colResSeg,
			Collection<Collection<PaxAirportTransferTO>> airportTransferMap) {
		if (colResSeg != null) {
			Map<Integer, String> mapSegmentInfo = new HashMap<Integer, String>();
			for (ReservationSegmentDTO reservationSegmentDTO : colResSeg) {
				mapSegmentInfo.put(reservationSegmentDTO.getPnrSegId(), reservationSegmentDTO.getSegmentCode());
			}
			return mapSegmentInfo;
		} else {
			Collection<Integer> colPnrSegIds = new ArrayList<Integer>();
			for (Collection<PaxAirportTransferTO> colAirportTransferTOs : airportTransferMap)
				for (PaxAirportTransferTO paxMealTO : colAirportTransferTOs) {
					colPnrSegIds.add(paxMealTO.getPnrSegId());
				}
			return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnrSegmentCodes(colPnrSegIds);

		}
	}

	private static Map<Integer, Collection<PaxAirportTransferTO>>
			setPaxInfo(Collection<PaxAirportTransferTO> colAirportTransferMap, Set<ReservationPax> resPaxs) {
		Map<Integer, Collection<PaxAirportTransferTO>> paxAirportTransferMap = new HashMap<Integer, Collection<PaxAirportTransferTO>>();
		Collection<Integer> colPaxMealIds = new ArrayList<Integer>();
		Map<Integer, List<PaxAirportTransferTO>> tempAddAirportTransferShelf = new HashMap<Integer, List<PaxAirportTransferTO>>();

		for (PaxAirportTransferTO paxAirportTransferTO : colAirportTransferMap) {
			colPaxMealIds.add(paxAirportTransferTO.getPnrPaxSegAptId());
			if (!tempAddAirportTransferShelf.containsKey(paxAirportTransferTO.getPnrPaxId())) {
				tempAddAirportTransferShelf.put(paxAirportTransferTO.getPnrPaxId(), new ArrayList<PaxAirportTransferTO>());
			}
			tempAddAirportTransferShelf.get(paxAirportTransferTO.getPnrPaxId()).add(paxAirportTransferTO);
		}
		for (ReservationPax resPax : resPaxs) {

			List<PaxAirportTransferTO> paxAirportTransferTOs = tempAddAirportTransferShelf.get(resPax.getPnrPaxId());
			if (paxAirportTransferTOs != null) {
				for (PaxAirportTransferTO paxAirportTransferTO : paxAirportTransferTOs) {
					if (paxAirportTransferTO != null) {
						paxAirportTransferTO.setPaxName(resPax.getFirstName() + " " + resPax.getLastName());
						paxAirportTransferTO.setProvider(paxAirportTransferTO.getProvider());

						if (!paxAirportTransferMap.containsKey(resPax.getPnrPaxId())) {
							paxAirportTransferMap.put(resPax.getPnrPaxId(), new ArrayList<PaxAirportTransferTO>());
						}
						paxAirportTransferMap.get(resPax.getPnrPaxId()).add(paxAirportTransferTO);
					}
				}
			}
		} // end for

		return paxAirportTransferMap;
	}
}