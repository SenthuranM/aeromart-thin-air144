/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerElement;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.BaseAdditionalInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public class BaggageInformation extends
		PassengerAdditions<List<PassengerInformation>> {

	private String departureAirportCode;

	public BaggageInformation(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> detailsDTOs) throws ModuleException {
		for (PassengerInformation information : detailsDTOs) {
			List<AncillaryDTO> baggageInformation = getSsrAncillaryInformationListBy(information
					.getPnrPaxId());
			if (baggageInformation != null && baggageInformation.size() > 0) {
				information.setBaggages(baggageInformation.get(0));
			}
		}
	}

	private List<AncillaryDTO> getSsrAncillaryInformationListBy(
			Integer passengerId) {
		return baggageAncillaryInformation.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformation) {
		Map<Integer, AncillaryDTO> baggageAncillary = PnlAdlUtil
				.getPassengerBaggagesDetails(passengerInformation,
						departureAirportCode);

		if (baggageAncillary != null && baggageAncillary.entrySet() != null) {
			for (Map.Entry<Integer, AncillaryDTO> entry : baggageAncillary
					.entrySet()) {
				List<AncillaryDTO> ancillaryDTOs = new ArrayList<AncillaryDTO>();
				ancillaryDTOs.add(entry.getValue());
				baggageAncillaryInformation.put((Integer) entry.getKey(),
						ancillaryDTOs);
			}
		}

	}

}
