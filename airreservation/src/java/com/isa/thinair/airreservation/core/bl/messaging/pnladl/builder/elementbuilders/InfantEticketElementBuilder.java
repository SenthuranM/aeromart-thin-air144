/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.AdultEticketElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.MarketingFlightElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class InfantEticketElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> adultEticketRuleExecutor = new AdultEticketElementRuleExecutor();

	private boolean isStartWithNewLine = false;

	@Override
	public void buildElement(ElementContext context) {
		if (context.getFeaturePack() != null
				&& context.getFeaturePack().isShowEticketDetails()) {
			initContextData(context);
			eticketElementTemplate();
		}
		executeNext();
	}

	private void eticketElementTemplate() {
		currentElement = "";
		StringBuilder elementTemplate = new StringBuilder();
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation != null
					&& passengerInformation.getInfant() != null
					&& passengerInformation.getInfant().getEticketNumber() != null
					&& !passengerInformation.getInfant().getEticketNumber()
							.isEmpty()) {
				buildEticketElement(elementTemplate, passengerInformation);
				currentElement = elementTemplate.toString();
				ammendmentPreValidation(isStartWithNewLine, currentElement, uPContext, adultEticketRuleExecutor);
			}
		}
	}

	private void buildEticketElement(StringBuilder elementTemplate,
			PassengerInformation passengerInformation) {
		elementTemplate.setLength(0);
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.TKNE);
		elementTemplate.append(space());
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
		elementTemplate.append(space());
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.INF);
		elementTemplate.append(passengerInformation.getInfant()
				.getEticketNumber());
		elementTemplate.append(MessageComposerConstants.FWD);
		elementTemplate.append(passengerInformation.getInfant().getCoupon());
		elementTemplate.append(MessageComposerConstants.HYPHEN);
		elementTemplate.append(1);
		elementTemplate.append(passengerInformation.getLastName());
		elementTemplate.append(MessageComposerConstants.FWD);
		elementTemplate.append(passengerInformation.getFirstName());
		if (passengerInformation.getTitle() != null) {
			elementTemplate.append(passengerInformation.getTitle());
		}
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
