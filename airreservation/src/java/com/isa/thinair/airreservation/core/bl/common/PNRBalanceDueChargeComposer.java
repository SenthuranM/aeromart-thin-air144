package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.tax.OndChargeUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class PNRBalanceDueChargeComposer {

	@SuppressWarnings("unchecked")
	public static Map<Integer, Set<ChargesDetailDTO>> getPnrPaxSeqWiseBalanceDueCharges(LCCClientPnrModesDTO pnrModesDTO)
			throws ModuleException {

		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		Map<Integer, Set<ReservationPaxOndCharge>> paxSeqWiseOndCharges = new HashMap<Integer, Set<ReservationPaxOndCharge>>();
		Map<Integer, Set<ReservationPaxOndPayment>> paxSeqWiseOndPayments = new HashMap<Integer, Set<ReservationPaxOndPayment>>();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			Integer paxSeq = reservationPax.getPaxSequence();
			if (!reservation.isInfantPaymentRecordedWithInfant()
					&& reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				paxSeq = reservationPax.getParent().getPaxSequence();
			}
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				if (paxSeqWiseOndCharges.get(paxSeq) != null) {
					paxSeqWiseOndCharges.get(paxSeq).addAll(reservationPaxFare.getCharges());
				} else {
					paxSeqWiseOndCharges.put(paxSeq, reservationPaxFare.getCharges());
				}

			}

			for (ReservationTnx reservationTnx : reservationPax.getPaxPaymentTnxView()) {
				if (paxSeqWiseOndPayments.get(paxSeq) != null) {
					paxSeqWiseOndPayments.get(paxSeq).addAll(reservationTnx.getReservationPaxTnxOndBreakdown());
				} else {
					paxSeqWiseOndPayments.put(paxSeq,
							new HashSet<ReservationPaxOndPayment>(reservationTnx.getReservationPaxTnxOndBreakdown()));
				}
			}
		}

		if (paxSeqWiseOndPayments.size() > 0) {
			wipeOffOndChargesFromPayments(paxSeqWiseOndCharges, paxSeqWiseOndPayments);
		}

		setChargeCodeInPaxOndCharges(paxSeqWiseOndCharges);

		Map<Integer, Set<ChargesDetailDTO>> mapPaxWiseChargeDetailInfo = new HashMap<Integer, Set<ChargesDetailDTO>>();

		for (Integer paxSeq : paxSeqWiseOndCharges.keySet()) {
			Set<ReservationPaxOndCharge> paxOndCharges = paxSeqWiseOndCharges.get(paxSeq);
			Set<ChargesDetailDTO> paxChargeDetails = new HashSet<ChargesDetailDTO>();
			for (ReservationPaxOndCharge reservationPaxOndCharge : paxOndCharges) {
				ChargesDetailDTO chargesDetailDTO = new ChargesDetailDTO();
				chargesDetailDTO.setChargeRateId(reservationPaxOndCharge.getChargeRateId());
				chargesDetailDTO.setChargeAmount(reservationPaxOndCharge.getAmount());
				chargesDetailDTO.setChargeGroupCode(reservationPaxOndCharge.getChargeGroupCode());
				chargesDetailDTO.setChargeCode(reservationPaxOndCharge.getChargeCode());
				paxChargeDetails.add(chargesDetailDTO);
			}
			mapPaxWiseChargeDetailInfo.put(paxSeq, paxChargeDetails);
		}

		return mapPaxWiseChargeDetailInfo;
	}

	private static void wipeOffOndChargesFromPayments(Map<Integer, Set<ReservationPaxOndCharge>> paxSeqWiseOndCharges,
			Map<Integer, Set<ReservationPaxOndPayment>> paxSeqWiseOndPayments) {
		for (Integer paxSeq : paxSeqWiseOndCharges.keySet()) {
			Set<ReservationPaxOndPayment> paxOndPayments = paxSeqWiseOndPayments.get(paxSeq);
			Set<ReservationPaxOndCharge> paxOndCharges = paxSeqWiseOndCharges.get(paxSeq);

			if (paxOndPayments != null && paxOndPayments.size() > 0) {

				Iterator<ReservationPaxOndCharge> paxOndChargesIte = paxOndCharges.iterator();

				while (paxOndChargesIte.hasNext()) {
					ReservationPaxOndCharge reservationPaxOndCharge = paxOndChargesIte.next();

					Iterator<ReservationPaxOndPayment> paxOndPaymentsIte = paxOndPayments.iterator();
					while (paxOndPaymentsIte.hasNext()) {
						ReservationPaxOndPayment reservationPaxOndPayment = paxOndPaymentsIte.next();
						if (reservationPaxOndCharge.getPnrPaxOndChgId() == reservationPaxOndPayment.getPnrPaxOndChgId()
								.intValue()) {
							if (reservationPaxOndPayment.getAmount().doubleValue() == reservationPaxOndCharge.getAmount()
									.doubleValue()) {
								paxOndPaymentsIte.remove();
								paxOndChargesIte.remove();
								break;
							} else if (reservationPaxOndCharge.getAmount().doubleValue() > reservationPaxOndPayment.getAmount()
									.doubleValue()) {
								reservationPaxOndCharge.setAmount(AccelAeroCalculator
										.subtract(reservationPaxOndCharge.getAmount(), reservationPaxOndPayment.getAmount()));
								paxOndPaymentsIte.remove();
							}
						}
					}
				}
			}

			paxSeqWiseOndCharges.put(paxSeq, paxOndCharges);

		}
	}

	private static void setChargeCodeInPaxOndCharges(Map<Integer, Set<ReservationPaxOndCharge>> paxSeqWiseOndCharges) {
		Collection<ReservationPaxOndCharge> ondCharges = new ArrayList<ReservationPaxOndCharge>();
		for (Set<ReservationPaxOndCharge> ondChgsPerPax : paxSeqWiseOndCharges.values()) {
			ondChgsPerPax.addAll(ondChgsPerPax);
		}

		OndChargeUtil.setChargeCodeInPaxCharges(ondCharges);

	}

}
