package com.isa.thinair.airreservation.core.bl.segment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.OtherAirlineSegmentTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.OtherAirlineSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.OtherAirlineSegmentUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;


/**
 * @isa.module.command name="changeOtherAirlineSegment"
 */
public class ChangeOtherAirlineSegment extends DefaultBaseCommand { 
	
	private static Log log = LogFactory.getLog(ChangeOtherAirlineSegment.class);

	public ChangeOtherAirlineSegment() {
	}

	/**
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		Collection<OtherAirlineSegmentTO> colExternalSegmentTO = (Collection<OtherAirlineSegmentTO>) this
				.getParameter(CommandParamNames.RESERVATION_OTHER_AIRLINE_SEGMENTS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		// Checking params
		this.validateParams(pnr, colExternalSegmentTO, version, credentialsDTO);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);

		// TODO make the reservation external also
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		if (colExternalSegmentTO != null && colExternalSegmentTO.size() > 0) {

			Map<String, Collection<OtherAirlineSegment>> serverSegmentMap = OtherAirlineSegmentUtil
					.getConfirmedReservationOtherAirlineSegmentInAMap(reservation.getOtherAirlineSegments());
			
			Map<String, Collection<OtherAirlineSegment>> serverAllSegmentMap = OtherAirlineSegmentUtil
					.getAllReservationOtherAirlineSegmentInAMap(reservation.getOtherAirlineSegments());

			String addedSegmentInfo = BeanUtils.nullHandler(OtherAirlineSegmentUtil.addOtherAirlineSegments(
					colExternalSegmentTO, reservation, serverAllSegmentMap));
			String modifySegmentInfo = BeanUtils.nullHandler(OtherAirlineSegmentUtil.modifyExternalSegments(colExternalSegmentTO,
					serverSegmentMap));

			if (addedSegmentInfo.length() > 0 || modifySegmentInfo.length() > 0) {
				ReservationProxy.saveReservation(reservation);
				ReservationAudit reservationAudit = this.composeAudit(pnr, addedSegmentInfo, modifySegmentInfo, credentialsDTO);
				colReservationAudit.add(reservationAudit);
			}
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);

		log.debug("Exit execute");
		return response;
	}
	
	private ReservationAudit composeAudit(String pnr, String addedSegmentInfo, String modifySegmentInfo,
			CredentialsDTO credentialsDTO) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setUserNote(null);

		if (addedSegmentInfo.length() > 0) {
			// Added Segment Information
			reservationAudit.setModificationType(AuditTemplateEnum.ADDED_NEW_OAL_SEGMENT.getCode());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedExternalSegment.ADDED_SEGMENT_INFORMATION,
					"Added[" + addedSegmentInfo + "]");
		}

		if (modifySegmentInfo.length() > 0) {
			// modified Segment Information
			reservationAudit.setModificationType(AuditTemplateEnum.CHANGED_OAL_SEGMENTS.getCode());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedExternalSegment.MODIFIED_SEGMENT_INFORMATION,
					"Modified[" + modifySegmentInfo + "]");
		}

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedExternalSegment.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		return reservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param colExternalSegmentTO
	 * @param version
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Collection<OtherAirlineSegmentTO> colExternalSegmentTO, Long version,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || colExternalSegmentTO == null || colExternalSegmentTO.size() == 0 || version == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

}
