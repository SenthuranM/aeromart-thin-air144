/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext;

/**
 * @author udithad
 *
 */
public class FeaturePack {

	private boolean isRbdEnabled;
	private boolean isShowSeatMap;
	private boolean isShowMeals;
	private boolean isShowBaggage;
	private boolean isShowEticketDetails;
	private boolean isDcsConnectivityEnabled;

	public boolean isRbdEnabled() {
		return isRbdEnabled;
	}

	public void setRbdEnabled(boolean isRbdEnabled) {
		this.isRbdEnabled = isRbdEnabled;
	}

	public boolean isShowSeatMap() {
		return isShowSeatMap;
	}

	public void setShowSeatMap(String value) {
		this.isShowSeatMap = false;
		if ("Y".equals(value)) {
			this.isShowSeatMap = true;
		}
	}

	public boolean isShowMeals() {
		return isShowMeals;
	}

	public void setShowMeals(String value) {
		this.isShowMeals = false;
		if ("Y".equals(value)) {
			this.isShowMeals = true;
		}
	}

	public boolean isShowBaggage() {
		return isShowBaggage;
	}

	public void setShowBaggage(String value) {
		this.isShowBaggage = false;
		if ("Y".equals(value)) {
			this.isShowBaggage = true;
		}
	}

	public boolean isShowEticketDetails() {
		return isShowEticketDetails;
	}

	public void setShowEticketDetails(boolean isShowEticketDetails) {
		this.isShowEticketDetails = isShowEticketDetails;
	}

	public boolean isDcsConnectivityEnabled() {
		return isDcsConnectivityEnabled;
	}

	public void setDcsConnectivityEnabled(boolean isDcsConnectivityEnabled) {
		this.isDcsConnectivityEnabled = isDcsConnectivityEnabled;
	}

}
