package com.isa.thinair.airreservation.core.bl.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.OnholdReleaseTime;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;

public class OnholdReleaseTimeAdaptor {

	private static Log log = LogFactory.getLog(OnholdReleaseTimeAdaptor.class);

	public static void auditForModifyOnholdReleaseTimeConfigModification(UserPrincipal userPrinciple,
			OnholdReleaseTime onholdReleaseTime, Map<String, String[]> contMap, long versionFlg) {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userPrinciple.getUserId());

		if (versionFlg < 0) {
			audit.setTaskCode(TasksUtil.MASTER_ADD_ONHOLD_TIME_CONFIG);
		} else {
			audit.setTaskCode(TasksUtil.MASTER_EDIT_ONHOLD_TIME_CONFIG);
		}

		LinkedHashMap contents = new LinkedHashMap();
		contents.put("config_id", onholdReleaseTime.getRelTimeId());

		if (versionFlg < 0) {
			onholdReleaseTime.setUser(userPrinciple.getUserId());
			contents.put("new_config", createStrOnholdConfigData(onholdReleaseTime));
		}

		if (contMap != null && contMap.size() > 0) {
			for (Entry<String, String[]> entry : contMap.entrySet()) {
				contents.put(entry.getKey(), entry.getValue());
			}
		}

		try {
			ReservationModuleUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {
			log.error("ModuleException::auditForModifyOnholdReleaseTimeConfigModification", e);
		}

	}

	public static void auditForDeleteOnholdReleaseTimeConfigModification(UserPrincipal userPrinciple, int configId) {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userPrinciple.getUserId());
		audit.setTaskCode(TasksUtil.MASTER_DELETE_ONHOLD_TIME_CONFIG);

		LinkedHashMap contents = new LinkedHashMap();
		contents.put("config_id", configId);
		try {
			ReservationModuleUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {
			log.error("ModuleException::auditForDeleteOnholdReleaseTimeConfigModification", e);
		}

	}

	public static String createStrOnholdConfigData(OnholdReleaseTime onholdRelTime) {
		StringBuilder sb = new StringBuilder();
		sb.append("agentCode:" + onholdRelTime.getAgentCode());
		sb.append("-bookingCode:" + onholdRelTime.getBookingCode());
		sb.append("-cabinClassCode:" + onholdRelTime.getCabinClassCode());
		sb.append("-isDomestic:" + onholdRelTime.getIsDomestic());
		sb.append("-moduleCode:" + onholdRelTime.getModuleCode());
		sb.append("-ondCode:" + onholdRelTime.getOndCode());
		sb.append("-ranking:" + onholdRelTime.getRanking());
		sb.append("-startCutover:" + onholdRelTime.getStartCutover());
		sb.append("-endCutover:" + onholdRelTime.getEndCutover());
		sb.append("-releaseTime:" + onholdRelTime.getReleaseTime());
		sb.append("-createdBy:" + onholdRelTime.getUser());
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date today = new Date();
		sb.append("-createdDate:" + formatter.format(today));
		return sb.toString();
	}
}
