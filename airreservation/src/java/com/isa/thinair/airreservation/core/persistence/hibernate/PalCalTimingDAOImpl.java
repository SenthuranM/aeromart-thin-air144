package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.pal.PALTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PalCalTiming;
import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PalCalTimingDAO;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @isa.module.dao-impl dao-name="PalCalTimingDAO"
 */
public class PalCalTimingDAOImpl extends PlatformBaseHibernateDaoSupport implements PalCalTimingDAO {

	@Override
	public void savePalCalTiming(PalCalTiming palcalTiming) {
		hibernateSaveOrUpdate(palcalTiming);

	}

	@Override
	public Page getAllPALCALTimings(String airportCode, int startRec, int numOfRecs, String flightNo) {

		List<ModuleCriterion> criteriaList = new ArrayList<ModuleCriterion>();

		if (flightNo != null) {
			ModuleCriterion moduleCriterion = new ModuleCriterion();
			moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterion.setFieldName("flightNo");
			List<String> values = new ArrayList<String>();
			values.add(flightNo);
			moduleCriterion.setValue(values);
			criteriaList.add(moduleCriterion);
		}

		ModuleCriterion moduleCriterion2 = new ModuleCriterion();
		moduleCriterion2.setCondition(ModuleCriterion.CONDITION_EQUALS);
		moduleCriterion2.setFieldName("airportCode");
		List<String> values2 = new ArrayList<>();
		values2.add(airportCode);
		moduleCriterion2.setValue(values2);

		criteriaList.add(moduleCriterion2);

		return getPagedData(criteriaList, startRec, numOfRecs, PalCalTiming.class, null);

	}

	@Override
	public void deletePAlCalTiming(PalCalTiming palCalTiming) {
		delete(palCalTiming);
	}

	@Override
	public void isduplicatesExistForPalCAlTimings(PalCalTiming palcalTiming) throws ModuleException {
		
	if(!(palcalTiming.getFlightNo() == null) && palcalTiming.getApplyToAllFlights().equals(PalConstants.PalCalTimings.APPLY_TO_ALL_NO)){

	String hql = "select count(*) from PalCalTiming tim where tim.airportCode like :airportcode"
			 	+ " and tim.flightNo like :flightnumber"
			 	+ " and (:fromdate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or :todate between tim.startingZuluDate and tim.endingZuluDate "
				+ " or (:todate<=tim.startingZuluDate and :fromdate >= tim.endingZuluDate) "
				+ " or (:todate>=tim.startingZuluDate and :fromdate <= tim.endingZuluDate)) " 
				+ " and tim.status = :status"
				+ " and tim.applyToAllFlights = :applytoall";
	
		if (palcalTiming.getVersion() != -1) {
			hql = hql + " and tim.palCalTimingId != :timingId ";
		}

		Query query = getSession().createQuery(hql).setParameter("airportcode", palcalTiming.getAirportCode()).setParameter("flightnumber", palcalTiming.getFlightNo())
				.setTimestamp("fromdate", palcalTiming.getStartingZuluDate())
				.setTimestamp("todate", palcalTiming.getEndingZuluDate()).setString("status", PalConstants.Status.ACTIVE)
				.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_NO);
		
		if (palcalTiming.getVersion() != -1) {
			query.setInteger("timingId", palcalTiming.getPalCalTimingId());
		}

		int count = ((Long) query.uniqueResult()).intValue();
		if (count != 0) {
			throw new ModuleException("airreservations.auxilliary.overlappingairporttiming.palcal");
		}
		
	}else{
		
		String hql = "select count(*) from PalCalTiming tim where tim.airportCode like :airportcode"
				 	+ " and (:fromdate between tim.startingZuluDate and tim.endingZuluDate "
					+ " or :todate between tim.startingZuluDate and tim.endingZuluDate "
					+ " or (:todate<=tim.startingZuluDate and :fromdate >= tim.endingZuluDate) "
					+ " or (:todate>=tim.startingZuluDate and :fromdate <= tim.endingZuluDate)) " 
					+ " and tim.status = :status"
					+ " and tim.applyToAllFlights = :applytoall";
		
			if (palcalTiming.getVersion() != -1) {
				hql = hql + " and tim.palCalTimingId != :timingId ";
			}

			Query query = getSession().createQuery(hql).setParameter("airportcode", palcalTiming.getAirportCode())
					.setTimestamp("fromdate", palcalTiming.getStartingZuluDate())
					.setTimestamp("todate", palcalTiming.getEndingZuluDate()).setString("status", PalConstants.Status.ACTIVE)
					.setString("applytoall", PNLConstants.PnlAdlTimings.APPLY_TO_ALL_YES);
			
			if (palcalTiming.getVersion() != -1) {
				query.setInteger("timingId", palcalTiming.getPalCalTimingId());
			}

			int count = ((Long) query.uniqueResult()).intValue();
			if (count != 0) {
				throw new ModuleException("airreservations.auxilliary.overlappingairporttiming.palcal");
			}
	}

	}

	@Override
	public List<PalCalTiming> getActiveAirportPALCAlTimings(Date date) {
	
		String hql = "from PalCalTiming  timing where "
				+ " :date between timing.startingZuluDate and timing.endingZuluDate "
				+ " and timing.status = :status ";
		Query query = getSession().createQuery(hql).setParameter("date", date).setString("status", PalConstants.Status.ACTIVE);
		return query.list();

	}

	@Override
	public ArrayList<PALTransMissionDetailsDTO> getFlightsForPALSchedulingForAirport(Date date, PalCalTiming timing) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		final long internationalCutoffTime = AppSysParamsUtil.getAgentReservationCutoverInMillis();
		final long domesticCutoffTime = AppSysParamsUtil.getReservationCutoverForDomesticInMillis();

		Timestamp dateLowerLimit = new Timestamp(date.getTime());
		Timestamp dateUpperLimit = new Timestamp(date.getTime() + ((24 + (timing.getPalDepGap() / 60)) * 3600 * 1000));
		Object params[] = { dateUpperLimit, dateLowerLimit, "Y", timing.getAirportCode() };

		String sql = " SELECT DISTINCT flight.flight_type, "
					+ " flight.FLIGHT_NUMBER, "
					+ " flightseg.FLIGHT_ID, "
					+ " flightSeg.EST_TIME_DEPARTURE_LOCAL, "
					+ " flightseg.EST_TIME_DEPARTURE_ZULU, "
					+ " SUBSTR(flightseg.segment_code,0,3) AS ori, "
					+ " flightseg.FLT_SEG_ID "
					+ " FROM T_FLIGHT flight, "
					+ " T_FLIGHT_SEGMENT flightseg "
					+ " WHERE flightseg.FLIGHT_ID              =flight.FLIGHT_ID "
					+ " AND flightseg.EST_TIME_DEPARTURE_ZULU <=? "
					+ " AND flightseg.EST_TIME_DEPARTURE_ZULU  >? "
					+ " AND (flight.STATUS                     ='ACT' "
					+ " OR flight.STATUS                       ='CLS') "
					+ " AND flightseg.SEGMENT_VALID_FLAG       =? "
					+ " AND SUBSTR(flightseg.segment_code,0,3) = ? ";
					if (!(timing.getFlightNo() == null || timing.getFlightNo().isEmpty())){
						sql= sql + " AND trim(flight.FLIGHT_NUMBER) = '"+timing.getFlightNo().trim()+ "'  " ;
					}
					
					sql= sql	+ " ORDER BY flightseg.flight_id, "
							+ " flightseg.EST_TIME_DEPARTURE_ZULU ";

		ArrayList<PALTransMissionDetailsDTO> flightList = templete.query(sql, params,
				new ResultSetExtractor<ArrayList<PALTransMissionDetailsDTO>>() {
					public ArrayList<PALTransMissionDetailsDTO> extractData(ResultSet rs) throws SQLException,
							DataAccessException {

						ArrayList<PALTransMissionDetailsDTO> flightList = new ArrayList<PALTransMissionDetailsDTO>();

						while (rs.next()) {

							PALTransMissionDetailsDTO dto = new PALTransMissionDetailsDTO();

							int flightID = rs.getInt("FLIGHT_ID");
							Timestamp timeStampzulu = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");
							Timestamp timeStampLocal = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");
							String departureStation = rs.getString("ori");
							int flightSegmentId = rs.getInt("FLT_SEG_ID");
							String flightNumber = rs.getString("FLIGHT_NUMBER");

							dto.setDepartureStation(departureStation);
							dto.setDepartureTimeZulu(timeStampzulu);
							dto.setFlightId(flightID);
							dto.setFlightSegId(flightSegmentId);
							dto.setDeparturetimeLocal(timeStampLocal);
							dto.setFlightNumber(flightNumber);

							dto.setPalDepartureGap(timing.getPalDepGap());
							dto.setCalRepeatIntervalAfterCutoffTime(timing.getCalRepIntv());
							dto.setLastCalGap(timing.getLastCalGap());
							dto.setCalRepeatIntervalAfterCutoffTime(timing.getCalRepIntvAfterCutoff());

							String flight_type = rs.getString("FLIGHT_TYPE");
							if (FlightUtil.FLIGHT_TYPE_INT.equals(flight_type)) {
								dto.setFlightCutoffTime(internationalCutoffTime);
							} else {
								dto.setFlightCutoffTime(domesticCutoffTime);
							}

							flightList.add(dto);

						}
						return flightList;
					}
				});

		return flightList;

	}

	@Override
	public boolean hasValidSitaConfigsForAirPort(String airportCode) {

		Object params[] = { "ACT", "Y", airportCode };

		String sql = "SELECT COUNT(*) "
				+ " FROM t_sita_address address "
				+ " WHERE address.status     =? "
				+ " AND address.send_pal_cal =? "
				+ " AND address.airport_code =? ";
		
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Integer count = (Integer) jt.query(sql, params, new ResultSetExtractor<Integer>() {

			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer count = 0;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));
				}
				return count;
			}

		});

		return ((Integer) (count)).intValue() > 0;
	}

}
