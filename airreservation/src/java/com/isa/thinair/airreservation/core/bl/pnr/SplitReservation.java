/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.revacc.RefundablesMetaInfoTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckin;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckinSeat;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationSplitUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.CloneBO;
import com.isa.thinair.airreservation.core.bl.common.FeeCalculator;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.revacc.RecordCancelBO;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.segment.FeeBO;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.airreservation.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAirportTransferDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAutomaticCheckinDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airreservation.core.util.DiscountHandlerUtil;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.SplitResOrRemovePaxRq;
import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for separate set of passengers from the rest to a new reservation
 * 
 * Business Rules: (1) Flight segments are same in both reservations (2) Infant contains only charges. Does not include
 * any fares. (3) Infant charges are kept in the infant level (4) Infant won't have any cancellation charge
 * 
 * Happy Path: (1) Will split to a new reservation when passenger ids are passed 22669550 Alternative Path: (1) If the
 * reservation does not exist any more (2) If the reservation does not have any passengers after split operation takes
 * place
 * 
 * 
 * Special Notes: This handles removing an infant(with out a parent) if this mode is enabled
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="splitReservation"
 */
public class SplitReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SplitReservation.class);

	/**
	 * Execute method of the SplitReservation command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String newCarrierPNR = (String) this.getParameter(CommandParamNames.NEW_CARRIER_PNR);
		String newOriginatorPNR = (String) this.getParameter(CommandParamNames.NEW_ORIGINATOR_PNR);
		Collection<Integer> pnrPaxIds = (Collection<Integer>) this.getParameter(CommandParamNames.PNR_PAX_IDS);
		Boolean triggerIsolatedInfantMove = (Boolean) this.getParameter(CommandParamNames.TRIGGER_ISOLATED_INFANT_MOVE);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Boolean isPaxSplitOperation = (Boolean) this.getParameter(CommandParamNames.IS_PAX_SPLIT);
		Boolean isPaxCancel = (Boolean) this.getParameter(CommandParamNames.IS_PAX_CANCEL);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		CustomChargesTO customChargesTO = (CustomChargesTO) this.getParameter(CommandParamNames.CUSTOM_CHARGES);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		String extRecordLocator = (String) this.getParameter(CommandParamNames.EXT_RECORD_LOCATOR);
		Integer gdsNotifyAction = (Integer) this.getParameter(CommandParamNames.GDS_NOTIFY_ACTION);
		if (this.getParameter(CommandParamNames.SPLITTED_PAX_IDS) != null && pnrPaxIds.size() == 0) {
			pnrPaxIds = (Collection<Integer>) this.getParameter(CommandParamNames.SPLITTED_PAX_IDS);
		}

		// expects required action type
		if (gdsNotifyAction == null) {
			gdsNotifyAction = GDSInternalCodes.GDSNotifyAction.SPLIT_PAX.getCode();
		}

		Collection<PaymentInfo> colPaymentInfo = (Collection<PaymentInfo>) this.getParameter(CommandParamNames.PAYMENT_INFO);

		// Checking params
		this.validateParams(pnr, pnrPaxIds, triggerIsolatedInfantMove, isPaxSplitOperation, version, credentialsDTO);

		// Retrieves the Reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		Reservation oldReservation = ReservationProxy.getReservation(pnrModesDTO);

		// Check Split constraints
		ReservationSplitUtil.checkSplitConstraints(oldReservation, pnrPaxIds, version);

		List<Integer> forcedConfirmedPaxSeqsBeforeSplit = ReservationApiUtils.getForcedConfirmedPaxSeqs(oldReservation);

		// Returns the old segment map
		Map<Integer, Integer> oldSegmentMap = ReservationSplitUtil.getSegmentMap(oldReservation);

		// Clones the reservation
		Reservation newReservation = CloneBO.clone(oldReservation, newCarrierPNR, newOriginatorPNR, extRecordLocator);

		boolean isInfantOnly = isInfantOnlySplitting(oldReservation, pnrPaxIds);

		ReservationSplitUtil.composeNewExternalSegments(oldReservation, newReservation, isInfantOnly);

		// Returns the new segment map
		Map<Integer, ReservationSegment> newSegmentMap = this.locateNewSegmentObjects(newReservation, oldSegmentMap);

		Iterator<ReservationPax> itReservationPax = oldReservation.getPassengers().iterator();
		ReservationPax passenger;

		// Holds removing parents/infants and adults
		Set<ReservationPax> splitAdultsParents = new HashSet<ReservationPax>();
		// Holds removing infants
		Set<ReservationPax> splitInfants = new HashSet<ReservationPax>();
		// Holds the moving passengers
		Set<ReservationPax> movingPassengers = new HashSet<ReservationPax>();
		// Holds the remove Infant booking code map
		Map<Integer, String> removeInfantBCMap = new HashMap<Integer, String>();
		// Holds the remove infant parent pax fare id(s)
		Collection<Integer> removeInfantParentPaxFareIds = new HashSet<Integer>();
		List<Integer> splitPaxSeqs = new ArrayList<Integer>();
		Set<ReservationPax> etEligiblePaxForAdl = new HashSet<ReservationPax>();
		// Locating parents/adults and infants
		while (itReservationPax.hasNext()) {
			passenger = itReservationPax.next();

			if (pnrPaxIds.contains(passenger.getPnrPaxId())) {
				// Infant
				if (ReservationApiUtils.isInfantType(passenger)) {
					splitInfants.add(passenger);
				}
				// Adult / child or Parent
				else {
					splitAdultsParents.add(passenger);
				}
				splitPaxSeqs.add(passenger.getPaxSequence());
			}
		}

		RecordCancelBO recordCancelBO = new RecordCancelBO();

		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(oldReservation, false);
		// Checking if there is an invalid infant split exist
		itReservationPax = splitInfants.iterator();
		while (itReservationPax.hasNext()) {
			passenger = itReservationPax.next();

			if (!splitAdultsParents.contains(passenger.getParent())) {
				if (triggerIsolatedInfantMove.booleanValue()) {
					// Remove isolated infant to the new reservation
					this.removeIsolatedInfant(passenger, movingPassengers, isPaxSplitOperation, removeInfantBCMap,
							removeInfantParentPaxFareIds, recordCancelBO, credentialsDTO, customChargesTO, chgTnxGen);
				} else {
					// This case is an exceptional case where an infant was
					// tried to split with out the parent's move
					throw new ModuleException("airreservations.arg.invalidSplitOperation");
				}
			}
		}

		// Adding All the passengers in the old reservation since ADL ADD contains all the pax relevent to split
		etEligiblePaxForAdl.addAll(oldReservation.getPassengers());

		// if remove pax
		if (isPaxCancel == null || !isPaxCancel) {
			// Notify adl and keep track of original reservation
			// TODO remove this
			ADLNotifyer.recordOriginalReservation(oldReservation);
		}

		// Record old reservation for deletion, only if PNL sent
		if (!oldReservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			ADLNotifyer.recordReservation(oldReservation, AdlActions.D, etEligiblePaxForAdl, null);
		}

		List<NameDTO> splitPaxNames = getPassengerNameList(splitAdultsParents);
		List<SSRInfantDTO> splitedInfants = getSplitedInfantList(splitInfants);

		// Handling parents/infants and adults
		itReservationPax = splitAdultsParents.iterator();
		while (itReservationPax.hasNext()) {
			passenger = itReservationPax.next();
			// Removes a passenger
			ReservationSplitUtil.removePassenger(oldReservation, passenger, movingPassengers);
		}

		ReservationModuleUtils.getPromotionManagementBD().removePassengersPromotionRequests(pnrPaxIds);

		String previousStatusOfOldReservation = oldReservation.getStatus();
		// Updates the reservation status
		ReservationSplitUtil.updatePnrStatus(oldReservation);

		// Saves the old reservation
		ReservationProxy.saveReservation(oldReservation);

		// Update adl add records for old reservations
		if (!oldReservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			if (isPaxCancel != null && isPaxCancel) {
				ADLNotifyer.recordReservation(oldReservation, AdlActions.A, etEligiblePaxForAdl, oldReservation.getStatus());
			} else if (isPaxCancel == null || !isPaxCancel) {
				ADLNotifyer.recordReservation(oldReservation, AdlActions.A, oldReservation.getPassengers(),
						oldReservation.getStatus());
			}
		}

		// update the external ref in the pax transaction. //TODO remove the update of lcc uid
		// this.updatePaxTnxExternalRef(newReservation,pnrPaxIds,newLccTnxID);

		Iterator<ReservationPax> itPnrPaxCol = movingPassengers.iterator();
		while (itPnrPaxCol.hasNext()) {
			passenger = itPnrPaxCol.next();

			// Adds the new segment mappings
			ReservationSplitUtil.addNewSegmentMappings(passenger, newSegmentMap);

			// Adds the new passenger
			ReservationSplitUtil.addPassenger(newReservation, passenger);
		}

		String previousStatusOfNewReservation = newReservation.getStatus();

		// Updates the reservation status
		ReservationSplitUtil.updatePnrStatus(newReservation);

		// Saves the new reservation
		ReservationProxy.saveReservation(newReservation);

		saveTaxInvoices(oldReservation, newReservation);

		updateTransactionSegments(newReservation.getPassengers(), newSegmentMap);

		if (!oldReservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			ADLNotifyer.updateADLActionsForRemovePaxScenario(newReservation, oldReservation.getPnr());
		}
		// handle remove pax in the adl.
		if ((isPaxCancel == null || !isPaxCancel) && !oldReservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			// New reservation must go as an addition in adl
			// ADLNotifyer.ensureADLAddition(newReservation);
			// Update adl add records for new reservations, only for split reservation
			ADLNotifyer.recordReservation(newReservation, AdlActions.A, splitAdultsParents, null);
		}

		if (previousStatusOfNewReservation.equals(ReservationStatus.ON_HOLD)
				&& newReservation.getStatus().equals(ReservationStatus.CONFIRMED) && newReservation.getOriginatorPnr() == null) {

			CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
			tktInfoDto.setBSPPayment(ReservationApiUtils.isBSPpayment(colPaymentInfo));
			ETicketBO.saveETickets(ETicketBO.generateIATAETicketNumbersForFreshBooking(newReservation, tktInfoDto),
					newReservation.getPassengers(), newReservation);
		}

		if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.REMOVE_PAX.getCode()) {
			if (splitPaxNames != null && splitPaxNames.size() > 0 && splitedInfants != null && splitedInfants.size() > 0) {
				gdsNotifyAction = GDSInternalCodes.GDSNotifyAction.REMOVE_PAX_INFANT.getCode();
			} else if (splitedInfants != null && splitedInfants.size() > 0) {
				gdsNotifyAction = GDSInternalCodes.GDSNotifyAction.REMOVE_INFANT.getCode();
			}
		}
		if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
			// send typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(oldReservation);
			typeBRequestDTO.setGdsNotifyAction(gdsNotifyAction);
			typeBRequestDTO.setSplitPaxNames(splitPaxNames);
			typeBRequestDTO.setSplitedInfants(splitedInfants);
			typeBRequestDTO.setNewPnr(newReservation.getPnr());
			TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);

		}

		if (oldReservation.getAdminInfo().getOriginChannelId().compareTo(ReservationInternalConstants.SalesChannel.GDS) == 0) {
			// trigger TypeA changes
			GDSNotifyBD gdsNotifyBD = ReservationModuleUtils.getGDSNotifyBD();
			SplitResOrRemovePaxRq splitResOrRemovePaxRq = new SplitResOrRemovePaxRq(ReservationRq.Event.REMOVE_PASSENGER);
			splitResOrRemovePaxRq.setOldPnr(pnr);
			splitResOrRemovePaxRq.setNewPnr(newReservation.getPnr());
			splitResOrRemovePaxRq.setNewExternalRecordLocator(newReservation.getExternalRecordLocator());
			splitResOrRemovePaxRq.setGdsId(newReservation.getGdsId());
			gdsNotifyBD.onReservationModification(splitResOrRemovePaxRq);
		}

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(oldReservation);
		if (csOCCarrirs != null) {
			// send Codeshare typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(oldReservation);
			typeBRequestDTO.setGdsNotifyAction(gdsNotifyAction);
			typeBRequestDTO.setSplitPaxNames(splitPaxNames);
			typeBRequestDTO.setSplitedInfants(splitedInfants);
			typeBRequestDTO.setNewPnr(newReservation.getPnr());
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		}

		// Execute record cancel revacc call
		recordCancelBO.execute();

		if (isPaxCancel != null && isPaxCancel) {
			// new reservation will be cancelled , cause its a remove pax
			// TODO remove and refactor this
			if (!oldReservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
				ADLNotifyer.ensureRemovePax(newReservation, oldReservation.getPnr());
			}
			ReservationProxy.saveReservation(newReservation);
		}

		Reservation oldReservationEtkt = ReservationProxy.getReservation(pnrModesDTO);
		boolean etExists = checkETGenerated(oldReservationEtkt);
		List<Integer> forcedConfirmedPaxSeqsAfterSplit = ReservationApiUtils.getForcedConfirmedPaxSeqs(oldReservationEtkt);
		forcedConfirmedPaxSeqsBeforeSplit.removeAll(splitPaxSeqs);
		Collection<ReservationPax> etGenerationEligiblePax = ReservationApiUtils.getConfirmedPaxByOperationUsingForceCNFSeqs(
				forcedConfirmedPaxSeqsBeforeSplit, forcedConfirmedPaxSeqsAfterSplit, oldReservation);

		if (newReservation.getOriginatorPnr() == null
				&& (previousStatusOfNewReservation.equals(ReservationStatus.ON_HOLD)
						|| !splitPaxSeqs.containsAll(forcedConfirmedPaxSeqsBeforeSplit))
				&& (!forcedConfirmedPaxSeqsBeforeSplit.isEmpty()
						|| (previousStatusOfNewReservation.equals(ReservationStatus.ON_HOLD)
								&& oldReservationEtkt.getStatus().equals(ReservationStatus.CONFIRMED) && !etExists))
				&& !etGenerationEligiblePax.isEmpty()) {
			CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = ETicketBO.updateETickets(
					ETicketBO.generateIATAETicketNumbersForModifyBooking(oldReservationEtkt, tktInfoDto), etGenerationEligiblePax,
					oldReservationEtkt, true);

			if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
				Collection<Integer> affectingPnrSegIds = new ArrayList<Integer>();
				for (ReservationPax resPax : oldReservationEtkt.getPassengers()) {
					Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
					Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
					ReservationPaxFare reservationPaxFare;
					ReservationPaxFareSegment reservationPaxFareSegment;

					while (itReservationPaxFare.hasNext()) {
						reservationPaxFare = itReservationPaxFare.next();
						itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

						while (itReservationPaxFareSegment.hasNext()) {
							reservationPaxFareSegment = itReservationPaxFareSegment.next();
							for (ReservationPaxFareSegmentETicket resPaxFareSegmentETicket : modifiedPaxfareSegmentEtickets) {
								if (resPaxFareSegmentETicket.getPnrPaxFareSegId()
										.equals(reservationPaxFareSegment.getPnrPaxFareSegId())) {
									affectingPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
								}
							}
						}
					}
				}
				ADLNotifyer.recordCanceledSegmentsForAdl(oldReservationEtkt, affectingPnrSegIds, etGenerationEligiblePax);
				ADLNotifyer.recordReservation(oldReservationEtkt, AdlActions.A, etGenerationEligiblePax, null);
			}
		}
		ADLNotifyer.recordReservation(newReservation, AdlActions.A, newReservation.getPassengers(), null);

		// Update the seating info for the newly split reservation
		this.setSeatInfoForNewlySplitReservation(newReservation, oldReservation, pnrPaxIds, newSegmentMap);

		// Update the meal info for the newly split reservation
		this.setMealInfoForNewlySplitReservation(newReservation, oldReservation, pnrPaxIds, newSegmentMap);

		// Update the baggage info for the newly split reservation
		this.setBaggageInfoForNewlySplitReservation(newReservation, oldReservation, pnrPaxIds, newSegmentMap);

		// Update the baggage info for the newly split reservation
		this.setAutoCheckinInfoForNewlySplitReservation(newReservation, oldReservation, pnrPaxIds, newSegmentMap);

		// Update the Insurance info for both new and old reservations
		this.setInsuranceForNewlySplitReservation(newReservation, oldReservation, pnrPaxIds);

		// Update the apt info for the newly split reservation
		this.setAptInfoForNewlySplitReservation(newReservation, oldReservation, pnrPaxIds, newSegmentMap);

		// This will clone alerts to the new segments
		this.cloneAlerts(newSegmentMap);

		// Map ground segments to new reservation
		ReservationSplitUtil.mapGroundSegmentsforNewReservation(newReservation, oldReservation);

		// UPDATE Segments Meal, Baggage & SEAT Selection Status Of Old Reservation
		ReservationCoreUtils.updateSegmentsAncillaryStatus(oldReservation);

		// UPDATE Segments Meal & SEAT Selection Status Of new Reservation
		ReservationCoreUtils.updateSegmentsAncillaryStatus(newReservation);

		// update the external ref in the pax transaction.
		// this.updatePaxTnxExternalRef(newReservation,pnrPaxIds,newLccTnxID);

		// Get the reservation audit
		Collection<ReservationAudit> colReservationAudit = this.composeAudit(pnr, newReservation.getPnr(), movingPassengers,
				credentialsDTO, userNotes, userNoteType);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.PNR, newReservation.getPnr());
		response.addResponceParam(CommandParamNames.OLD_PNR, oldReservation.getPnr());
		response.addResponceParam(CommandParamNames.MOVING_PASSENGERS, movingPassengers);
		response.addResponceParam(CommandParamNames.REMOVE_INFANT_BC_MAP, removeInfantBCMap);
		response.addResponceParam(CommandParamNames.REMOVE_INFANT_PARENT_PAX_FARE_IDS, removeInfantParentPaxFareIds);

		response.addResponceParam(CommandParamNames.VERSION, new Long(newReservation.getVersion()));

		// Return the first passenger last name
		String lastName = ReservationSplitUtil.getFirstPassengerLastName(newReservation);

		// Setting it to the output service response
		if (output == null) {
			output = new DefaultServiceResponse();
			output.addResponceParam(CommandParamNames.PNR, newReservation.getPnr());
			output.addResponceParam(CommandParamNames.FIRST_PASSENGER_LAST_NAME, lastName);
		} else {
			output.addResponceParam(CommandParamNames.PNR, newReservation.getPnr());
			output.addResponceParam(CommandParamNames.FIRST_PASSENGER_LAST_NAME, lastName);
		}

		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	private void setAutoCheckinInfoForNewlySplitReservation(Reservation newReservation, Reservation oldReservation,
			Collection<Integer> pnrPaxIds, Map<Integer, ReservationSegment> newSegmentMap) {
		// Do only if original reservation has AutoCheckins
		if (oldReservation.getAutoCheckins() != null) {
			Collection<PaxAutomaticCheckinTO> oldPaxAutoCheckinTOs = oldReservation.getAutoCheckins();
			Map<Integer, PaxAutomaticCheckinTO> autoCheckinIdPaxAutoCheckinTOMap = new HashMap<Integer, PaxAutomaticCheckinTO>();

			for (PaxAutomaticCheckinTO paxAutoCheckinTO : oldPaxAutoCheckinTOs) {
				if (pnrPaxIds.contains(paxAutoCheckinTO.getPnrPaxId())) {

					ReservationSegment reservationSegment = newSegmentMap.get(paxAutoCheckinTO.getPnrSegId());

					if (reservationSegment != null) {
						paxAutoCheckinTO.setPnrSegId(reservationSegment.getPnrSegId());
						autoCheckinIdPaxAutoCheckinTOMap.put(paxAutoCheckinTO.getPkey(), paxAutoCheckinTO);
					}
				}
			}

			if (autoCheckinIdPaxAutoCheckinTOMap != null && !autoCheckinIdPaxAutoCheckinTOMap.isEmpty()) {
				ReservationAutomaticCheckinDAO autoCheckinDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO;
				// Load the AutoCheckins info
				Collection<ReservationPaxSegAutoCheckin> passengerAutoCheckins = autoCheckinDAO
						.getReservationAutomaticCheckins(autoCheckinIdPaxAutoCheckinTOMap
						.keySet());
				Collection<ReservationPaxSegAutoCheckin> updatablePassengerCheckins = new ArrayList<ReservationPaxSegAutoCheckin>();

				// update the loaded AutoCheckins info with the new pnr seg id
				for (ReservationPaxSegAutoCheckin passengerAutoCheckin : passengerAutoCheckins) {
					PaxAutomaticCheckinTO paxAutoCheckinTO = autoCheckinIdPaxAutoCheckinTOMap.get(passengerAutoCheckin
							.getPnrPaxSegAutoCheckinId());
					passengerAutoCheckin.setPnrSegId(paxAutoCheckinTO.getPnrSegId());
					updatablePassengerCheckins.add(passengerAutoCheckin);
				}

				if (updatablePassengerCheckins != null && !updatablePassengerCheckins.isEmpty()) {
					autoCheckinDAO.saveOrUpdate(updateSeatPreference(updatablePassengerCheckins));
					autoCheckinDAO.saveOrUpdate(setSitTogetherpnrPaxId(updatablePassengerCheckins));
				}
			}
		}
	}
	
	private Collection<ReservationPaxSegAutoCheckin> updateSeatPreference(
			Collection<ReservationPaxSegAutoCheckin> updatablePassengerCheckins) {
		ReservationAutomaticCheckinDAO autoCheckinDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO;
		Optional<ReservationPaxSegAutoCheckin> parentPnrPax = updatablePassengerCheckins
				.stream()
				.filter(passengerAutoCheckin -> (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat() != null)
						&& (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_WINDOW)
								|| passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
										.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_AISLE) || passengerAutoCheckin
								.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_MIDDLE))
						|| (passengerAutoCheckin.getFlightAmSeatId() != null)).findFirst();
		if (!parentPnrPax.isPresent()) {
			ReservationPaxSegAutoCheckin reservationPaxSegAutoCheckin = updatablePassengerCheckins.stream()
					.min(Comparator.comparingInt(ReservationPaxSegAutoCheckin::getPnrPaxId)).get();
			Collection<PaxAutomaticCheckinTO> paxAutomaticCheckinTOs = autoCheckinDAO
					.getReservationAutomaticCheckinsByPaxId(new ArrayList<Integer>(Arrays.asList(reservationPaxSegAutoCheckin
							.getSitTogetherpnrPaxId())));
			PaxAutomaticCheckinTO paxAutomaticCheckinTO = paxAutomaticCheckinTOs != null && paxAutomaticCheckinTOs.size() > 0
					? paxAutomaticCheckinTOs.iterator().next()
					: null;
			String seatTypePreference = null;
			if (paxAutomaticCheckinTO != null && paxAutomaticCheckinTO.getSeatTypePreference() != null) {
				seatTypePreference = getSeatPreference(paxAutomaticCheckinTO.getSeatTypePreference());
			} else if (paxAutomaticCheckinTO != null && paxAutomaticCheckinTO.getFlightAmSeatId() != null) {
				seatTypePreference = getSeatPreference(autoCheckinDAO.getSeatPreferenceByFlightAmSeatId(paxAutomaticCheckinTO
						.getFlightAmSeatId()));
			}
			boolean isParentPrefernceAdded = true;
			for (ReservationPaxSegAutoCheckin updatablePassengerCheckin : updatablePassengerCheckins) {
				if (isParentPrefernceAdded) {
					ReservationPaxSegAutoCheckinSeat autoCheckinSeat = new ReservationPaxSegAutoCheckinSeat();
					autoCheckinSeat.setSeatTypePreference(seatTypePreference);
					updatablePassengerCheckin.setReservationPaxSegAutoCheckinSeat(autoCheckinSeat);
					autoCheckinSeat.setReservationPaxSegAutoCheckin(reservationPaxSegAutoCheckin);
					isParentPrefernceAdded = false;
				}
				updatablePassengerCheckin.setSitTogetherpnrPaxId(null);
			}
		} else {
			updatablePassengerCheckins.forEach(updatablePassengerCheckin -> {
				updatablePassengerCheckin.setSitTogetherpnrPaxId(null);
			});
		}
		return updatablePassengerCheckins;
	}

	private String getSeatPreference(String seatTypePreference) {
		if (AirinventoryCustomConstants.AutoCheckinConstants.SEAT_WINDOW.equalsIgnoreCase(seatTypePreference)) {
			seatTypePreference = AirinventoryCustomConstants.AutoCheckinConstants.WINDOW_NEXT_SEAT;
		} else if (AirinventoryCustomConstants.AutoCheckinConstants.SEAT_MIDDLE.equalsIgnoreCase(seatTypePreference)) {
			seatTypePreference = AirinventoryCustomConstants.AutoCheckinConstants.MIDDLE_NEXT_SEAT;
		} else if (AirinventoryCustomConstants.AutoCheckinConstants.SEAT_AISLE.equalsIgnoreCase(seatTypePreference)) {
			seatTypePreference = AirinventoryCustomConstants.AutoCheckinConstants.AISLE_NEXT_SEAT;
		}
		return seatTypePreference;
	}

	private static Collection<ReservationPaxSegAutoCheckin> setSitTogetherpnrPaxId(
			Collection<ReservationPaxSegAutoCheckin> passengerAutoCheckins) {
		Integer sitTogetherPnrPaxId = 0;
		Optional<ReservationPaxSegAutoCheckin> parentPnrPax = passengerAutoCheckins
				.stream()
				.filter(passengerAutoCheckin -> (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat() != null)
						&& (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_WINDOW)
								|| passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
										.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_AISLE) || passengerAutoCheckin
								.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_MIDDLE))
						|| (passengerAutoCheckin.getFlightAmSeatId() != null)).findFirst();
		sitTogetherPnrPaxId = parentPnrPax.isPresent() ? parentPnrPax.get().getPnrPaxId() : 0;
		for (ReservationPaxSegAutoCheckin passengerAutoCheckin : passengerAutoCheckins) {
			if (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat() == null
					&& passengerAutoCheckin.getFlightAmSeatId() == null && sitTogetherPnrPaxId != 0
					&& passengerAutoCheckin.getAmount().compareTo(BigDecimal.ZERO) > 0) {
				passengerAutoCheckin.setSitTogetherpnrPaxId(sitTogetherPnrPaxId);
			}
		}
		return passengerAutoCheckins;
	}

	private void saveTaxInvoices(Reservation oldReservation, Reservation newReservation) {
		List<TaxInvoice> taxInvoices = ReservationDAOUtils.DAOInstance.TAX_INVOICE_DAO.getTaxInvoiceFor(oldReservation.getPnr());
		for (TaxInvoice taxInvoice : taxInvoices) {
			taxInvoice.addReservation(newReservation);
			ReservationDAOUtils.DAOInstance.TAX_INVOICE_DAO.saveTaxInvoice(taxInvoice);
		}
	}

	private void updateTransactionSegments(Set<ReservationPax> movingPassengers, Map<Integer, ReservationSegment> newSegmentMap) {
		for (ReservationPax pax : movingPassengers) {
			List<Integer> pnrTnxIds = new ArrayList<>();
			Collection<ReservationTnx> reservationTnxs = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
					.getPNRPaxPayments(String.valueOf(pax.getPnrPaxId()));
			for (ReservationTnx tnx : reservationTnxs) {
				pnrTnxIds.add(tnx.getTnxId());
			}
			if (pnrTnxIds.size() > 0) {
				List<TransactionSegment> tnxSegments = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
						.getTransactionSegment(pnrTnxIds);
				for (TransactionSegment tnxSeg : tnxSegments) {
					tnxSeg.setPnrSegId(newSegmentMap.get(tnxSeg.getPnrSegId()).getPnrSegId());
					ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.saveTransactionSegment(tnxSeg);
				}
			}
		}
	}

	private boolean checkETGenerated(Reservation reservation) {
		for (ReservationPax pax : reservation.getPassengers()) {
			if (pax.geteTickets() != null && !pax.geteTickets().isEmpty()) {
				return true;
			}
		}
		return false;
	}

	private boolean isInfantOnlySplitting(Reservation reservation, Collection<Integer> pnrPaxIds) {
		for (ReservationPax passenger : reservation.getPassengers()) {
			if (pnrPaxIds.contains(passenger.getPnrPaxId()) && !ReservationApiUtils.isInfantType(passenger)) {
				return false;
			}
		}
		return true;
	}

	private void removePromotionsOfSplitingPaxs(String pnr, Collection<Integer> affectedPaxIds) throws ModuleException {
		ReservationModuleUtils.getPromotionManagementBD().removePassengersPromotionRequests(affectedPaxIds);
	}

	/**
	 * Sets Seats information for the newly split reservation
	 * 
	 * @param newReservation
	 * @param oldReservation
	 * @param credentialsDTO
	 * @param pnrPaxIds
	 */
	private void setSeatInfoForNewlySplitReservation(Reservation newReservation, Reservation oldReservation,
			Collection<Integer> pnrPaxIds, Map<Integer, ReservationSegment> newSegmentMap) {
		// Do only if original reservation has seats
		if (oldReservation.getSeats() != null) {
			Collection<PaxSeatTO> oldPaxSeatTOs = oldReservation.getSeats();
			Map<Integer, PaxSeatTO> seatingIdPaxSeatTOMap = new HashMap<Integer, PaxSeatTO>();

			for (PaxSeatTO paxSeatTO : oldPaxSeatTOs) {
				if (pnrPaxIds.contains(paxSeatTO.getPnrPaxId())) {
					ReservationSegment reservationSegment = newSegmentMap.get(paxSeatTO.getPnrSegId());

					if (reservationSegment != null) {
						paxSeatTO.setPnrSegId(reservationSegment.getPnrSegId());
						seatingIdPaxSeatTOMap.put(paxSeatTO.getPkey(), paxSeatTO);
					}
				}
			}

			if (seatingIdPaxSeatTOMap != null && !seatingIdPaxSeatTOMap.isEmpty()) {
				SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;

				// Load the seating info
				Collection<PassengerSeating> passengerSeatings = seatMapDAO.getPassengerSeats(seatingIdPaxSeatTOMap.keySet());
				Collection<PassengerSeating> updatablePassengerSeatings = new ArrayList<PassengerSeating>();

				// update the loaded seating info with the new pnr seg id
				for (PassengerSeating passengerSeating : passengerSeatings) {
					PaxSeatTO paxSeatTO = seatingIdPaxSeatTOMap.get(passengerSeating.getPaxSeatingid());
					passengerSeating.setPnrSegId(paxSeatTO.getPnrSegId());
					updatablePassengerSeatings.add(passengerSeating);
				}

				if (updatablePassengerSeatings != null && !updatablePassengerSeatings.isEmpty()) {
					seatMapDAO.saveOrUpdate(updatablePassengerSeatings);
				}
			}
		}
	}

	/**
	 * Sets Meal information for the newly split reservation
	 * 
	 * @param newReservation
	 * @param oldReservation
	 * @param credentialsDTO
	 * @param pnrPaxIds
	 */
	private void setMealInfoForNewlySplitReservation(Reservation newReservation, Reservation oldReservation,
			Collection<Integer> pnrPaxIds, Map<Integer, ReservationSegment> newSegmentMap) {
		// Do only if original reservation has Meals
		if (oldReservation.getMeals() != null) {
			Collection<PaxMealTO> oldPaxMealTOs = oldReservation.getMeals();
			Map<Integer, PaxMealTO> mealIdPaxMealTOMap = new HashMap<Integer, PaxMealTO>();

			for (PaxMealTO paxMealTO : oldPaxMealTOs) {
				if (pnrPaxIds.contains(paxMealTO.getPnrPaxId())) {
					ReservationSegment reservationSegment = newSegmentMap.get(paxMealTO.getPnrSegId());

					if (reservationSegment != null) {
						paxMealTO.setPnrSegId(reservationSegment.getPnrSegId());
						mealIdPaxMealTOMap.put(paxMealTO.getPkey(), paxMealTO);
					}
				}
			}

			if (mealIdPaxMealTOMap != null && !mealIdPaxMealTOMap.isEmpty()) {
				MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
				// Load the meal info
				Collection<PassengerMeal> passengerMeals = mealDAO.getPassengerMeals(mealIdPaxMealTOMap.keySet());
				Collection<PassengerMeal> updatablePassengerMeals = new ArrayList<PassengerMeal>();

				// update the loaded meal info with the new pnr seg id
				for (PassengerMeal passengerMeal : passengerMeals) {
					PaxMealTO paxMealTO = mealIdPaxMealTOMap.get(passengerMeal.getPaxMealId());
					passengerMeal.setPnrSegId(paxMealTO.getPnrSegId());
					updatablePassengerMeals.add(passengerMeal);
				}

				if (updatablePassengerMeals != null && !updatablePassengerMeals.isEmpty()) {
					mealDAO.saveOrUpdate(updatablePassengerMeals);
				}
			}
		}
	}

	/**
	 * Sets Meal information for the newly split reservation
	 * 
	 * @param newReservation
	 * @param oldReservation
	 * @param credentialsDTO
	 * @param pnrPaxIds
	 */
	private void setAptInfoForNewlySplitReservation(Reservation newReservation, Reservation oldReservation,
			Collection<Integer> pnrPaxIds, Map<Integer, ReservationSegment> newSegmentMap) {
		// Do only if original reservation has Apts
		if (oldReservation.getAirportTransfers() != null) {
			Collection<PaxAirportTransferTO> oldPaxAptTOs = oldReservation.getAirportTransfers();
			Map<Integer, PaxAirportTransferTO> aptIdPaxAptTOMap = new HashMap<Integer, PaxAirportTransferTO>();

			for (PaxAirportTransferTO paxAptTO : oldPaxAptTOs) {
				if (pnrPaxIds.contains(paxAptTO.getPnrPaxId())) {
					ReservationSegment reservationSegment = newSegmentMap.get(paxAptTO.getPnrSegId());

					if (reservationSegment != null) {
						paxAptTO.setPnrSegId(reservationSegment.getPnrSegId());
						aptIdPaxAptTOMap.put(paxAptTO.getPnrPaxSegAptId(), paxAptTO);
					}
				}
			}

			if (aptIdPaxAptTOMap != null && !aptIdPaxAptTOMap.isEmpty()) {
				ReservationAirportTransferDAO aptDAO = ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO;
				// Load the apt info
				Collection<ReservationPaxSegAirportTransfer> passengerApts = aptDAO
						.getReservationAirportTransfers(aptIdPaxAptTOMap.keySet());
				Collection<ReservationPaxSegAirportTransfer> updatablePassengerApts = new ArrayList<ReservationPaxSegAirportTransfer>();

				// update the loaded apt info with the new pnr seg id
				for (ReservationPaxSegAirportTransfer passengerApt : passengerApts) {
					PaxAirportTransferTO paxAptTO = aptIdPaxAptTOMap.get(passengerApt.getPnrPaxSegAirportTransferId());
					passengerApt.setPnrSegId(paxAptTO.getPnrSegId());
					updatablePassengerApts.add(passengerApt);
				}

				if (updatablePassengerApts != null && !updatablePassengerApts.isEmpty()) {
					aptDAO.saveOrUpdate(updatablePassengerApts);
				}
			}
		}
	}

	private void setBaggageInfoForNewlySplitReservation(Reservation newReservation, Reservation oldReservation,
			Collection<Integer> pnrPaxIds, Map<Integer, ReservationSegment> newSegmentMap) {
		// Do only if original reservation has Baggages
		if (oldReservation.getBaggages() != null) {
			Collection<PaxBaggageTO> oldPaxBaggageTOs = oldReservation.getBaggages();
			Map<Integer, PaxBaggageTO> baggageIdPaxBaggageTOMap = new HashMap<Integer, PaxBaggageTO>();

			for (PaxBaggageTO paxBaggageTO : oldPaxBaggageTOs) {
				if (pnrPaxIds.contains(paxBaggageTO.getPnrPaxId())) {
					ReservationSegment reservationSegment = newSegmentMap.get(paxBaggageTO.getPnrSegId());

					if (reservationSegment != null) {
						paxBaggageTO.setPnrSegId(reservationSegment.getPnrSegId());
						baggageIdPaxBaggageTOMap.put(paxBaggageTO.getPkey(), paxBaggageTO);
					}
				}
			}

			if (baggageIdPaxBaggageTOMap != null && !baggageIdPaxBaggageTOMap.isEmpty()) {
				BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
				// Load the baggage info
				Collection<PassengerBaggage> passengerBaggages = baggageDAO
						.getPassengerBaggages(baggageIdPaxBaggageTOMap.keySet());
				Collection<PassengerBaggage> updatablePassengerBaggages = new ArrayList<PassengerBaggage>();

				// update the loaded baggage info with the new pnr seg id
				for (PassengerBaggage passengerBaggage : passengerBaggages) {
					PaxBaggageTO paxBaggageTO = baggageIdPaxBaggageTOMap.get(passengerBaggage.getPaxBaggageId());
					passengerBaggage.setPnrSegmentId(paxBaggageTO.getPnrSegId());
					updatablePassengerBaggages.add(passengerBaggage);
				}

				if (updatablePassengerBaggages != null && !updatablePassengerBaggages.isEmpty()) {
					baggageDAO.saveOrUpdate(updatablePassengerBaggages);
				}
			}
		}
	}

	private void setInsuranceForNewlySplitReservation(Reservation newReservation, Reservation oldReservation,
			Collection<Integer> pnrPaxIds) {
		if (oldReservation.getReservationInsurance() != null && !oldReservation.getReservationInsurance().isEmpty()) {
			List<ReservationInsurance> oldResInslist = oldReservation.getReservationInsurance();
			for (ReservationInsurance oldResIns : oldResInslist) {

				// Apply split insurance for insurance amount,netAmount,taxAmount,quotedNetAmount & quotedTaxAmount
				BigDecimal singlePaxInsAmount = AccelAeroCalculator.divide(oldResIns.getAmount(), oldResIns.getTotalPaxCount());
				BigDecimal singlePaxNetAmount = AccelAeroCalculator.divide(oldResIns.getNetAmount(),
						oldResIns.getTotalPaxCount());
				BigDecimal singlePaxTaxAmount = AccelAeroCalculator.divide(oldResIns.getTaxAmount(),
						oldResIns.getTotalPaxCount());
				BigDecimal singlePaxInsQuotedAmount = AccelAeroCalculator.divide(oldResIns.getQuotedAmount(),
						oldResIns.getTotalPaxCount());
				BigDecimal singlePaxInsQuotedNetAmount = AccelAeroCalculator.divide(oldResIns.getQuotedNetAmount(),
						oldResIns.getTotalPaxCount());
				BigDecimal singlePaxInsQuotedTaxAmount = AccelAeroCalculator.divide(oldResIns.getQuotedTaxAmout(),
						oldResIns.getTotalPaxCount());

				BigDecimal paxTotTicketPrice = oldResIns.getInsuranceTotalTicketPrice() == null
						? AccelAeroCalculator.getDefaultBigDecimalZero()
						: oldResIns.getInsuranceTotalTicketPrice();

				BigDecimal singlePaxTotalTicketPrice = AccelAeroCalculator.divide(paxTotTicketPrice,
						oldResIns.getTotalPaxCount());

				oldResIns.setAmount(AccelAeroCalculator.multiply(singlePaxInsAmount,
						oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount()));
				oldResIns.setNetAmount(AccelAeroCalculator.multiply(singlePaxNetAmount,
						oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount()));
				oldResIns.setTaxAmount(AccelAeroCalculator.multiply(singlePaxTaxAmount,
						oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount()));
				oldResIns.setQuotedAmount(AccelAeroCalculator.multiply(singlePaxInsQuotedAmount,
						oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount()));
				oldResIns.setQuotedNetAmount(AccelAeroCalculator.multiply(singlePaxInsQuotedNetAmount,
						oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount()));
				oldResIns.setQuotedTaxAmout(AccelAeroCalculator.multiply(singlePaxInsQuotedTaxAmount,
						oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount()));
				oldResIns.setInsuranceTotalTicketPrice(AccelAeroCalculator.multiply(singlePaxTotalTicketPrice,
						oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount()));
				oldResIns.setTotalPaxCount(oldReservation.getTotalPaxAdultCount() + oldReservation.getTotalPaxChildCount());

				ReservationInsurance newResIns = new ReservationInsurance();

				newResIns.setAmount(AccelAeroCalculator.multiply(singlePaxInsAmount,
						newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount()));
				newResIns.setNetAmount(AccelAeroCalculator.multiply(singlePaxNetAmount,
						newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount()));
				newResIns.setTaxAmount(AccelAeroCalculator.multiply(singlePaxTaxAmount,
						newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount()));
				newResIns.setQuotedAmount(AccelAeroCalculator.multiply(singlePaxInsQuotedAmount,
						newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount()));
				newResIns.setQuotedNetAmount(AccelAeroCalculator.multiply(singlePaxInsQuotedNetAmount,
						newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount()));
				newResIns.setQuotedTaxAmout(AccelAeroCalculator.multiply(singlePaxInsQuotedTaxAmount,
						newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount()));
				newResIns.setInsuranceTotalTicketPrice(AccelAeroCalculator.multiply(singlePaxTotalTicketPrice,
						newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount()));

				if (AppSysParamsUtil.isRakEnabled()) {
					newResIns.setPolicyCode(AppSysParamsUtil.getRakGroupPolicy() + " " + newReservation.getPnr());
				} else {
					newResIns.setPolicyCode(oldResIns.getPolicyCode());
				}

				newResIns.setDateOfReturn(oldResIns.getDateOfReturn());
				newResIns.setSsrFeeCode(oldResIns.getSsrFeeCode());
				newResIns.setPlanCode(oldResIns.getPlanCode());
				newResIns.setDateOfTravel(oldResIns.getDateOfTravel());
				newResIns.setDescription(oldResIns.getDescription());
				newResIns.setDestination(oldResIns.getDestination());
				newResIns.setEndDateOffset(oldResIns.getEndDateOffset());
				newResIns.setMarketingCarrier(oldResIns.getMarketingCarrier());
				newResIns.setMessageId(oldResIns.getMessageId());
				newResIns.setOrigin(oldResIns.getOrigin());
				newResIns.setPnr(newReservation.getPnr());
				newResIns.setQuotedCurrencyCode(oldResIns.getQuotedCurrencyCode());
				newResIns.setQuoteTime(oldResIns.getQuoteTime());
				newResIns.setRoundTrip(oldResIns.getRoundTrip());
				newResIns.setSellTime(oldResIns.getSellTime());
				newResIns.setStartDateOffset(oldResIns.getStartDateOffset());
				newResIns.setState(oldResIns.getState());
				newResIns.setTotalPaxCount(newReservation.getTotalPaxAdultCount() + newReservation.getTotalPaxChildCount());
				newResIns.setUserId(oldResIns.getUserId());
				newResIns.setInsuranceId(oldResIns.getInsuranceId());
				newResIns.setInsuranceType(oldResIns.getInsuranceType());
				// Update both old and new reservation insurance records.
				ReservationProxy.saveOrUpdateReservationInsurance(oldResIns);
				ReservationProxy.saveOrUpdateReservationInsurance(newResIns);
			}
		}
	}

	/**
	 * Clone the alerts
	 * 
	 * @param newSegmentMap
	 * @throws ModuleException
	 */
	private void cloneAlerts(Map<Integer, ReservationSegment> newSegmentMap) throws ModuleException {
		// Holds the alert segments
		Map<Integer, ReservationSegment> alertSegmentMap = new HashMap<Integer, ReservationSegment>();
		Iterator<Integer> itColPnrSegIds = newSegmentMap.keySet().iterator();
		Integer pnrSegId;
		ReservationSegment reservationSegment;

		while (itColPnrSegIds.hasNext()) {
			pnrSegId = itColPnrSegIds.next();
			reservationSegment = newSegmentMap.get(pnrSegId);

			if (reservationSegment.getAlertFlag() == ReservationInternalConstants.AlertTypes.ALERT_TRUE) {
				alertSegmentMap.put(pnrSegId, reservationSegment);
			}
		}

		// If any alert segments exist
		if (alertSegmentMap.size() > 0) {
			AlertingBD alertingBD = ReservationModuleUtils.getAlertingBD();
			Map<Long, Collection<Alert>> mapPnrSegIdAlerts = alertingBD
					.getAlertsForPnrSegments(new ArrayList<Integer>(alertSegmentMap.keySet()));

			itColPnrSegIds = alertSegmentMap.keySet().iterator();
			Collection<Alert> colSaveAlerts = new ArrayList<Alert>();
			Collection<Alert> colAlerts;
			Iterator<Alert> itColAlerts;
			Alert alert;

			while (itColPnrSegIds.hasNext()) {
				pnrSegId = itColPnrSegIds.next();
				reservationSegment = alertSegmentMap.get(pnrSegId);
				colAlerts = mapPnrSegIdAlerts.get(new Long(pnrSegId.intValue()));

				if (colAlerts != null) {
					itColAlerts = colAlerts.iterator();
					while (itColAlerts.hasNext()) {
						alert = itColAlerts.next();
						colSaveAlerts.add(new Alert(alert.getContent(), alert.getTimestamp(), alert.getStatus(),
								alert.getPriorityCode(), alert.getAlertTypeId(),
								new Long(reservationSegment.getPnrSegId().intValue()), alert.getOriginalDepDate()));
					}
				}
			}

			// Saving the clone alerts
			alertingBD.addAlerts(colSaveAlerts);
		}
	}

	/**
	 * Compose the audit
	 * 
	 * @param oldPnr
	 * @param newPnr
	 * @param movingPassengers
	 * @param credentialsDTO
	 * @param userNoteType TODO
	 * @return
	 */
	private Collection<ReservationAudit> composeAudit(String oldPnr, String newPnr, Collection<ReservationPax> movingPassengers,
			CredentialsDTO credentialsDTO, String userNotes, String userNoteType) {

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		ReservationAudit reservationAuditOld = this.composeOldAudit(oldPnr, newPnr, movingPassengers, credentialsDTO);
		ReservationAudit reservationAuditNew = this.composeNewAudit(oldPnr, newPnr, movingPassengers, credentialsDTO);

		if (userNotes != null && !userNotes.isEmpty()) {

			reservationAuditOld.setUserNote(userNotes);
			reservationAuditOld.setUserNoteType(userNoteType);
			reservationAuditNew.setUserNote(userNotes);
			reservationAuditNew.setUserNoteType(userNoteType);
		}

		colReservationAudit.add(reservationAuditOld);
		colReservationAudit.add(reservationAuditNew);

		return colReservationAudit;
	}

	/**
	 * Compose new pnr audit
	 * 
	 * @param oldPnr
	 * @param newPnr
	 * @param movingPassengers
	 * @param credentialsDTO
	 * @return
	 */
	private ReservationAudit composeNewAudit(String oldPnr, String newPnr, Collection<ReservationPax> movingPassengers,
			CredentialsDTO credentialsDTO) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(newPnr);
		reservationAudit.setModificationType(AuditTemplateEnum.ADDED_SPLIT_RESERVATION.getCode());
		reservationAudit.setUserNote(null);

		// Setting the new pnr information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedSplitReservation.OLD_PNR, oldPnr);

		// Setting the split passenger information
		Iterator<ReservationPax> itReservationPax = movingPassengers.iterator();
		ReservationPax reservationPax;
		StringBuffer passengeInfo = new StringBuffer();

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			passengeInfo.append(" " + BeanUtils.nullHandler(reservationPax.getTitle()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getFirstName()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getLastName()) + " - " + reservationPax.getPaxTypeDescription());
		}

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedSplitReservation.PASSENGER_DETAILS,
				passengeInfo.toString());

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedSplitReservation.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedSplitReservation.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Compse the old pnr audit
	 * 
	 * @param oldPnr
	 * @param newPnr
	 * @param movingPassengers
	 * @param credentialsDTO
	 * @return
	 */
	private ReservationAudit composeOldAudit(String oldPnr, String newPnr, Collection<ReservationPax> movingPassengers,
			CredentialsDTO credentialsDTO) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(oldPnr);
		reservationAudit.setModificationType(AuditTemplateEnum.SPLIT_RESERVATION.getCode());
		reservationAudit.setUserNote(null);

		// Setting the new pnr information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SplitReservation.NEW_PNR, newPnr);

		// Setting the split passenger information
		Iterator<ReservationPax> itReservationPax = movingPassengers.iterator();
		ReservationPax reservationPax;
		StringBuffer passengeInfo = new StringBuffer();

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			passengeInfo.append(" " + BeanUtils.nullHandler(reservationPax.getTitle()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getFirstName()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getLastName()) + " - " + reservationPax.getPaxTypeDescription());
		}

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SplitReservation.SPLIT_PASSENGERS,
				passengeInfo.toString());

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SplitReservation.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.SplitReservation.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Remove isolated infant
	 * 
	 * @param infant
	 * @param movingPassengers
	 * @param isPaxSplitOperation
	 * @param removeInfantBCMap
	 * @param removeInfantParentPaxFareIds
	 * @param recordCancelBO
	 * @param credentialsDTO
	 * @param customChargesTO
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	private void removeIsolatedInfant(ReservationPax infant, Set<ReservationPax> movingPassengers, Boolean isPaxSplitOperation,
			Map<Integer, String> removeInfantBCMap, Collection<Integer> removeInfantParentPaxFareIds,
			RecordCancelBO recordCancelBO, CredentialsDTO credentialsDTO, CustomChargesTO customChargesTO,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		// Updates the infant count
		ReservationSplitUtil.updateInfantCount(infant.getReservation(), false);

		// Updates the reservation fare and charges
		ReservationSplitUtil.updatePnrFareAndChgs(infant.getReservation(), infant, false);

		// Track Parent booking codes
		this.trackParentBookingCodes(infant.getParent(), removeInfantBCMap, removeInfantParentPaxFareIds);

		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(infant.getReservation().getStatus())) {
			ReservationSplitUtil.moveInfant(infant, movingPassengers);
		} else {
			RefundablesMetaInfoTO refundablesMetaInfoTO = this.getRefundableInfantCharges(infant, customChargesTO, credentialsDTO,
					chgTnxGen);

			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
			reservationPaxPaymentMetaTO.setPnrPaxId(infant.getParent().getPnrPaxId());
			reservationPaxPaymentMetaTO
					.addMapPerPaxWiseOndRefundableCharges(refundablesMetaInfoTO.getMapRefundablePaxOndChargeMetaInfo());
			if (infant.getReservation().isInfantPaymentRecordedWithInfant()) {
				reservationPaxPaymentMetaTO.setPnrPaxId(infant.getPnrPaxId());
				recordCancelBO.addRecord(infant.getPnrPaxId().toString(), refundablesMetaInfoTO.getRefundableTotal(),
						refundablesMetaInfoTO.getCancellationTotal(), null,
						ReservationSplitUtil.getActionCode(isPaxSplitOperation),
						ReservationTnxNominalCode.CANCEL_CHARGE.getCode(), reservationPaxPaymentMetaTO, credentialsDTO,
						infant.getReservation().isEnableTransactionGranularity());
			} else {
				reservationPaxPaymentMetaTO.setPnrPaxId(infant.getParent().getPnrPaxId());
				recordCancelBO.addRecord(infant.getParent().getPnrPaxId().toString(), refundablesMetaInfoTO.getRefundableTotal(),
						refundablesMetaInfoTO.getCancellationTotal(), null,
						ReservationSplitUtil.getActionCode(isPaxSplitOperation),
						ReservationTnxNominalCode.CANCEL_CHARGE.getCode(), reservationPaxPaymentMetaTO, credentialsDTO,
						infant.getReservation().isEnableTransactionGranularity());
			}

			this.updatePaxFareAndCharges(infant);
			ReservationSplitUtil.moveInfant(infant, movingPassengers);
		}
	}

	/**
	 * Track Parent booking codes
	 * 
	 * @param parent
	 * @param removeInfantBCMap
	 */
	private void trackParentBookingCodes(ReservationPax parent, Map<Integer, String> removeInfantBCMap,
			Collection<Integer> removeInfantParentPaxFareIds) {
		Iterator<ReservationPaxFare> itReservationPaxFare = parent.getPnrPaxFares().iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();
			removeInfantParentPaxFareIds.add(reservationPaxFare.getPnrPaxFareId());

			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();
				// Track the remove infant booking map
				// Holds the flight segment id and the booking code
				removeInfantBCMap.put(reservationPaxFareSegment.getSegment().getFlightSegId(),
						reservationPaxFareSegment.getBookingCode());
			}
		}
	}

	/**
	 * Update passenger fare and charges
	 * 
	 * @param infant
	 */
	private void updatePaxFareAndCharges(ReservationPax infant) {
		Iterator<ReservationPaxFare> itReservationPaxFare = infant.getPnrPaxFares().iterator();
		BigDecimal[] totalPaxFareAmounts = ReservationApiUtils.getChargeTypeAmounts();
		ReservationPaxFare reservationPaxFare;
		ReservationPaxOndCharge reservationPaxOndCharge;
		BigDecimal[] totalCharges;
		Iterator<ReservationPaxOndCharge> itCharges;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			totalCharges = ReservationApiUtils.getChargeTypeAmounts();
			itCharges = reservationPaxFare.getCharges().iterator();

			while (itCharges.hasNext()) {
				reservationPaxOndCharge = itCharges.next();
				ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
						reservationPaxOndCharge.getAmount(), totalCharges, reservationPaxOndCharge.getDiscount(),
						reservationPaxOndCharge.getAdjustment());
			}

			reservationPaxFare.setTotalChargeAmounts(totalCharges);
			ReservationCoreUtils.updateTotals(totalPaxFareAmounts, reservationPaxFare.getTotalChargeAmounts(), true);
		}

		infant.setTotalChargeAmounts(totalPaxFareAmounts);
	}

	/**
	 * Process and reture the refundable meta information
	 * 
	 * @param infant
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	private RefundablesMetaInfoTO getRefundableInfantCharges(ReservationPax infant, CustomChargesTO customChargesTO,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		BigDecimal rPaxTotalRefundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		FareBD fareBD = ReservationModuleUtils.getFareBD();
		Collection<Integer> fareIds = new ArrayList<Integer>();
		Collection<Integer> chgRateIds = new ArrayList<Integer>();

		Iterator<ReservationPaxFare> itReservationPaxFare = infant.getPnrPaxFares().iterator();
		ReservationPaxFare reservationPaxFare;
		ReservationPaxOndCharge reservationPaxOndCharge;
		Iterator<ReservationPaxOndCharge> itCharges;
		Iterator<ReservationPaxOndCharge> itNewOndCharges;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			// If the ond is not already cancelled
			if (!ReservationSplitUtil.isOndCancelled(reservationPaxFare)) {
				itCharges = reservationPaxFare.getCharges().iterator();

				while (itCharges.hasNext()) {
					reservationPaxOndCharge = itCharges.next();

					if (reservationPaxOndCharge.getChargeRateId() != null) {
						chgRateIds.add(reservationPaxOndCharge.getChargeRateId());
					} else if (reservationPaxOndCharge.getFareId() != null) {
						fareIds.add(reservationPaxOndCharge.getFareId());
					}
				}
			}
		}

		// Collect fares and charges information
		FaresAndChargesTO faresAndChargesTO = fareBD.getRefundableStatuses(fareIds, chgRateIds, null);
		boolean isOnHoldReservation = ReservationInternalConstants.ReservationStatus.ON_HOLD
				.equals(infant.getReservation().getStatus()) ? true : false;
		Collection<String> excludeChargeCodes = CancellationUtils.getExcludeChargeCodes(null);
		Map<Integer, FareTO> mapFareTO = faresAndChargesTO.getFareTOs();
		Map<Integer, ChargeTO> mapChgTO = faresAndChargesTO.getChargeTOs();
		Map<Integer, Collection<FareRuleFeeTO>> mapFareRuleFeeTO = faresAndChargesTO.getFareRuleFeeTOs();
		String paxType = infant.getPaxType();
		FareTO fareTO;
		ChargeTO chargeTO;
		BigDecimal ondCancelChg = null;
		BigDecimal ondRefChg = null;
		Collection<FareRuleFeeTO> colFareRuleFeeTO;
		Collection<ReservationPaxOndCharge> newOndCharges;
		RefundablesMetaInfoTO refundablesMetaInfoTO = new RefundablesMetaInfoTO();
		BigDecimal cancelTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<ReservationPaxFare, RefundablesMetaInfoTO> paxFareOndChgMap = new HashMap<ReservationPaxFare, RefundablesMetaInfoTO>();

		itReservationPaxFare = infant.getPnrPaxFares().iterator();
		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			boolean[] applyFlexiFlags = CancellationUtils.getApplyFlexibilitiesToOndFlags(reservationPaxFare);
			boolean hasCnxFlexibility = applyFlexiFlags[1];
			// boolean hasModFlexibility = applyFlexiFlags[0];
			// If the ond is not already cancelled
			if (!ReservationSplitUtil.isOndCancelled(reservationPaxFare)
					&& !ReservationSplitUtil.isOndFlown(reservationPaxFare, infant.getReservation())) {

				itCharges = reservationPaxFare.getCharges().iterator();
				newOndCharges = new ArrayList<ReservationPaxOndCharge>();
				ondCancelChg = AccelAeroCalculator.getDefaultBigDecimalZero();
				ondRefChg = AccelAeroCalculator.getDefaultBigDecimalZero();
				while (itCharges.hasNext()) {
					reservationPaxOndCharge = itCharges.next();
					BigDecimal actualOndChargeAmount = reservationPaxOndCharge.getEffectiveAmount();

					if (reservationPaxOndCharge.getChargeRateId() != null) {
						chargeTO = mapChgTO.get(reservationPaxOndCharge.getChargeRateId());

						PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
								AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero(),
								reservationPaxOndCharge.getTaxAppliedCategory(),
								reservationPaxOndCharge.getTaxDepositCountryCode(),
								reservationPaxOndCharge.getTaxDepositStateCode());

						if (chargeTO.isRefundable()) {
							// Capturing the refundable amount
							rPaxTotalRefundableAmount = AccelAeroCalculator.add(rPaxTotalRefundableAmount, actualOndChargeAmount);
							ondRefChg = AccelAeroCalculator.add(ondRefChg, actualOndChargeAmount);

							// Entering a minus record for the refundable amount
							ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
									reservationPaxOndCharge.getAmount().negate(), null, reservationPaxOndCharge.getChargeRateId(),
									reservationPaxOndCharge.getChargeGroupCode(), credentialsDTO, true, 0,
									reservationPaxOndCharge.getDiscount().negate(),
									reservationPaxOndCharge.getAdjustment().negate(), paxOndChargeDTO,
									chgTnxGen.getTnxSequence(infant.getPnrPaxId()));

							newOndCharges.add(tmpReservationPaxOndCharge);
							refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
									tmpReservationPaxOndCharge);
						} else {
							if (isOnHoldReservation && !excludeChargeCodes.contains(chargeTO.getChargeCode())) {
								// Capturing the refundable amount
								rPaxTotalRefundableAmount = AccelAeroCalculator.add(rPaxTotalRefundableAmount,
										actualOndChargeAmount);
								ondRefChg = AccelAeroCalculator.add(ondRefChg, actualOndChargeAmount);

								// Entering a minus record for the refundable amount
								ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
										reservationPaxOndCharge.getAmount().negate(), null,
										reservationPaxOndCharge.getChargeRateId(), reservationPaxOndCharge.getChargeGroupCode(),
										credentialsDTO, true, 0, reservationPaxOndCharge.getDiscount().negate(),
										reservationPaxOndCharge.getAdjustment().negate(), paxOndChargeDTO,
										chgTnxGen.getTnxSequence(infant.getPnrPaxId()));

								newOndCharges.add(tmpReservationPaxOndCharge);
								refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
										tmpReservationPaxOndCharge);
							}
						}
					} else if (reservationPaxOndCharge.getFareId() != null) {
						fareTO = mapFareTO.get(reservationPaxOndCharge.getFareId());
						colFareRuleFeeTO = mapFareRuleFeeTO.get(fareTO.getFareRuleID());

						if (ReservationApiUtils.isRefundable(fareTO, paxType)) {
							// Capturing the refundable amount
							rPaxTotalRefundableAmount = AccelAeroCalculator.add(rPaxTotalRefundableAmount, actualOndChargeAmount);
							ondRefChg = AccelAeroCalculator.add(ondRefChg, actualOndChargeAmount);

							// Entering the minus record for the refundable amount
							ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
									reservationPaxOndCharge.getAmount().negate(), reservationPaxOndCharge.getFareId(), null,
									reservationPaxOndCharge.getChargeGroupCode(), credentialsDTO, true, 0,
									reservationPaxOndCharge.getDiscount().negate(),
									reservationPaxOndCharge.getAdjustment().negate(), null,
									chgTnxGen.getTnxSequence(infant.getPnrPaxId()));

							newOndCharges.add(tmpReservationPaxOndCharge);
							refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
									tmpReservationPaxOndCharge);
						} else {
							if (isOnHoldReservation) {
								// Capturing the refundable amount
								rPaxTotalRefundableAmount = AccelAeroCalculator.add(rPaxTotalRefundableAmount,
										actualOndChargeAmount);
								ondRefChg = AccelAeroCalculator.add(ondRefChg, actualOndChargeAmount);

								// Entering the minus record for the refundable amount
								ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
										reservationPaxOndCharge.getAmount().negate(), reservationPaxOndCharge.getFareId(), null,
										reservationPaxOndCharge.getChargeGroupCode(), credentialsDTO, true, 0,
										reservationPaxOndCharge.getDiscount().negate(),
										reservationPaxOndCharge.getAdjustment().negate(), null,
										chgTnxGen.getTnxSequence(infant.getPnrPaxId()));

								newOndCharges.add(tmpReservationPaxOndCharge);
								refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
										tmpReservationPaxOndCharge);
							}
						}
						FeeCalculator cal = new FeeCalculator(reservationPaxFare, colFareRuleFeeTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), fareTO.isNoonDayFeePolicyEnabled());
						cal.execute();

						FareRuleFeeTO cancelFee = cal.getCancelFee();
						// FareRuleFeeTO modifyFee = cal.getModifyFee();

						ondCancelChg = FeeBO.getCancellationCharge(fareTO, paxType, reservationPaxOndCharge.getAmount(),
								reservationPaxFare.getCharges(), new PnrChargeDetailTO(), customChargesTO, cancelFee,
								hasCnxFlexibility, false, false, 0);

						// Capturing the cancellation amount
						cancelTotalCharge = AccelAeroCalculator.add(cancelTotalCharge, ondCancelChg);
					}
				}

				// Add the new minus records
				itNewOndCharges = newOndCharges.iterator();

				while (itNewOndCharges.hasNext()) {
					reservationPaxOndCharge = itNewOndCharges.next();
					reservationPaxFare.addCharge(reservationPaxOndCharge);
				}

				RefundablesMetaInfoTO ondRefMetaInfo = new RefundablesMetaInfoTO();
				ondRefMetaInfo.setCancellationTotal(ondCancelChg);
				ondRefMetaInfo.setRefundableTotal(ondRefChg);
				paxFareOndChgMap.put(reservationPaxFare, ondRefMetaInfo);
			}
		}

		// Split Discount amount based on refundable/non-refndable charges
		boolean isFiltered = DiscountHandlerUtil.filterDiscount(refundablesMetaInfoTO);
		if (isFiltered) {
			rPaxTotalRefundableAmount = refundablesMetaInfoTO.getRefundableTotal();
		}

		// Since payments can only be captured for a AD/CH pax, when infant is removed, we will
		// charge the cnx charge from the maximum available credit if the INF cnx chg is larger than the
		// available pax credit/refundable
		boolean adjustCancelChgToMatchRefundable = false;
		if (cancelTotalCharge
				.compareTo(AccelAeroCalculator.add(rPaxTotalRefundableAmount, infant.getTotalAvailableBalance())) > 0) {
			adjustCancelChgToMatchRefundable = true;
		}
		cancelTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPaxFare resPaxFare : paxFareOndChgMap.keySet()) {
			RefundablesMetaInfoTO ondMetaInfo = paxFareOndChgMap.get(resPaxFare);
			BigDecimal ondCnxChg = ondMetaInfo.getCancellationTotal();
			BigDecimal totalRef = AccelAeroCalculator.add(ondMetaInfo.getRefundableTotal(), infant.getTotalAvailableBalance());
			if (adjustCancelChgToMatchRefundable
					&& (AccelAeroCalculator.isGreaterThan(totalRef, AccelAeroCalculator.getDefaultBigDecimalZero())
							&& ondCnxChg.compareTo(totalRef) > 0)) {
				ondCnxChg = totalRef;
			}
			cancelTotalCharge = AccelAeroCalculator.add(cancelTotalCharge, ondCnxChg);
			ReservationPaxOndCharge newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(ondCnxChg, null, null,
					ReservationInternalConstants.ChargeGroup.CNX, credentialsDTO, false, 0, null, null, null,
					chgTnxGen.getTnxSequence(infant.getPnrPaxId()));

			// Entering the plus record for the identified modification amount
			resPaxFare.addCharge(newReservationPaxOndCharge);
		}

		refundablesMetaInfoTO.setRefundableTotal(rPaxTotalRefundableAmount);
		refundablesMetaInfoTO.setCancellationTotal(cancelTotalCharge);
		// It is assumed for infant there won't be any modification charges.
		refundablesMetaInfoTO.setModificationTotal(AccelAeroCalculator.getDefaultBigDecimalZero());
		return refundablesMetaInfoTO;
	}

	/**
	 * Locate the new segment objects map
	 * 
	 * @param newReservation
	 * @param oldSegmentMap
	 * @return
	 */
	private Map<Integer, ReservationSegment> locateNewSegmentObjects(Reservation newReservation,
			Map<Integer, Integer> oldSegmentMap) {
		Iterator<ReservationSegment> itReservationSegment = newReservation.getSegments().iterator();
		ReservationSegment reservationSegment;
		Map<Integer, ReservationSegment> newSegmentsMap = new HashMap<Integer, ReservationSegment>();
		Integer pnrSegId;

		while (itReservationSegment.hasNext()) {
			reservationSegment = itReservationSegment.next();
			pnrSegId = oldSegmentMap.get(reservationSegment.getSegmentSeq());

			if (pnrSegId != null) {
				newSegmentsMap.put(pnrSegId, reservationSegment);
			}
		}

		return newSegmentsMap;
	}

	/**
	 * Validate parameters
	 * 
	 * @param pnr
	 * @param pnrPaxIds
	 * @param triggerIsolatedInfantMove
	 * @param isPaxSplitOperation
	 * @param version
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Collection<Integer> pnrPaxIds, Boolean triggerIsolatedInfantMove,
			Boolean isPaxSplitOperation, Long version, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || pnrPaxIds == null || pnrPaxIds.size() == 0 || triggerIsolatedInfantMove == null
				|| isPaxSplitOperation == null || version == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	/**
	 * Retrieves the unque fare IDs for reservation.
	 * 
	 * @param reservation
	 * @return UniqueFareIds.size()
	 */

	// private static int getUniqueFareIds(Reservation reservation) throws ModuleException {
	// Collection<ReservationSegment> pnrSegments = reservation.getSegments();
	// Map pnrSegIdAndFareTO = ReservationApiUtils.getFareTOs(reservation);
	//
	// Collection<Integer> UniqueFareIds = new HashSet<Integer>();
	//
	// ReservationSegment reservationSegment;
	// for (Iterator itColReservationSegment = pnrSegments.iterator(); itColReservationSegment.hasNext();) {
	// reservationSegment = (ReservationSegment) itColReservationSegment.next();
	// if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {
	// UniqueFareIds.add(reservationSegment.getOndGroupId());
	// }
	//
	// }
	//
	// return UniqueFareIds.size();
	// }

	/**
	 * @param adultsParents
	 * @return
	 */
	private static List<NameDTO> getPassengerNameList(Set<ReservationPax> adultsParents) {
		NameDTO paxName;
		List<NameDTO> passengersNames = new ArrayList<NameDTO>();
		for (ReservationPax reservationPax : adultsParents) {
			paxName = new NameDTO();
			paxName.setFirstName(reservationPax.getFirstName());
			paxName.setLastName(reservationPax.getLastName());
			paxName.setPaxTitle(reservationPax.getTitle());
			paxName.setFamilyID(reservationPax.getFamilyID());
			passengersNames.add(paxName);
		}
		return passengersNames;
	}

	/**
	 * @param Infants
	 * @return
	 */
	private static List<SSRInfantDTO> getSplitedInfantList(Set<ReservationPax> adultsParents) {
		SSRInfantDTO infant;
		List<SSRInfantDTO> infants = new ArrayList<SSRInfantDTO>();
		for (ReservationPax reservationPax : adultsParents) {
			infant = new SSRInfantDTO();
			infant.setInfantFirstName(reservationPax.getFirstName());
			infant.setInfantLastName(reservationPax.getLastName());
			infant.setInfantTitle(reservationPax.getTitle());
			infant.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			infant.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CANCEL.getCode());
			infant.setInfantDateofBirth(reservationPax.getDateOfBirth());
			if (reservationPax.getParent() != null) {
				infant.setGuardianFirstName(reservationPax.getParent().getFirstName());
				infant.setGuardianLastName(reservationPax.getParent().getLastName());
				infant.setGuardianTitle(reservationPax.getParent().getTitle());
			}
			infants.add(infant);
		}
		return infants;
	}
}
