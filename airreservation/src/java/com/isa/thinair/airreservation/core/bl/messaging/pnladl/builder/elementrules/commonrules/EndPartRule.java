/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.EndElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class EndPartRule extends BaseRule<EndElementRuleContext> {

	public static final int MAXIMUM_LINES_PAR_MESSAGE = 60;

	@Override
	public boolean validateRule(EndElementRuleContext context) {
		boolean isValied = false;

		if (context.getPassengerCount() < MAXIMUM_LINES_PAR_MESSAGE) {
			isValied = true;
		}

		return isValied;
	}
}
