package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupPaymentNotificationDTO;
import com.isa.thinair.airreservation.api.model.GroupBookingReqRoutes;
import com.isa.thinair.airreservation.api.model.GroupBookingRequest;
import com.isa.thinair.airreservation.api.model.GroupBookingRequestHistory;
import com.isa.thinair.airreservation.api.model.GroupRequestStation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.UserStation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.GroupBookingDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Gayan
 * @isa.module.dao-impl dao-name="GroupBookingDAO"
 */
public class GroupBookingDAOImpl extends PlatformBaseHibernateDaoSupport implements GroupBookingDAO {

	private enum STATUS {

		Pending_Approval(0),
		Approve(1),
		Reject(2),
		Re_Quote(3),
		Pending_afterRe_Quote(4),
		Request_Release_Seat(5),
		Withdraw(6),
		Released_Seats(7),
		Booked(8);

		private final int value;

		private STATUS(final int newValue) {
			value = newValue;
		}

		private int getValue() {
			return value;
		}
	}

	@Override
	public GroupBookingRequest save(GroupBookingRequest grpReq) {

		long requestId = (Long) hibernateSave(grpReq);
		grpReq.setRequestID(requestId);
		return grpReq;

	}

	public ServiceResponce saveGrpBookingRoutes(GroupBookingReqRoutes grpReqRotes) {

		hibernateSave(grpReqRotes);
		return new DefaultServiceResponse(true);

	}

	@Override
	public ServiceResponce update(GroupBookingRequest grpReq) {

		super.hibernateSaveOrUpdate(grpReq);
		return new DefaultServiceResponse(true);

	}

	@Override
	public ServiceResponce updateApproveStatus(GroupBookingRequest grpReq) {

		StringBuilder hql = new StringBuilder(
				"UPDATE GroupBookingRequest SET agreed_fare=:agreed_fare, status=:status , approval_comments=:approvalComments , min_payment_required=:min_payment_required , target_payment_date=:target_payment_date , fare_valid_date=:fare_valid_date , approval_date=:approval_date, IS_PERCENTAGE=:isPercentage ");
		hql.append("WHERE requestID=:requestID ");

		Query qry = getSession().createQuery(hql.toString()).setParameter("agreed_fare", grpReq.getAgreedFare())
				.setParameter("status", grpReq.getStatusID()).setParameter("approvalComments", grpReq.getApprovalComments())
				.setParameter("requestID", grpReq.getRequestID())
				.setParameter("min_payment_required", grpReq.getMinPaymentRequired())
				.setParameter("target_payment_date", grpReq.getTargetPaymentDate())
				.setParameter("fare_valid_date", grpReq.getFareValidDate()).setParameter("approval_date", new Date())
				.setParameter("isPercentage", grpReq.isApprovedPercentage() ? "Y" : "N");
		qry.executeUpdate();
		return new DefaultServiceResponse(true);

	}

	@Override
	public ServiceResponce updateAdminStatus(GroupBookingRequest grpReq) {

		StringBuilder hql = new StringBuilder(
				"UPDATE GroupBookingRequest SET status=:status , approvalComments=:approvalComments ");
		hql.append("WHERE requestID=:requestID ");

		Query qry = getSession().createQuery(hql.toString()).setParameter("status", grpReq.getStatusID())
				.setParameter("approvalComments", grpReq.getApprovalComments()).setParameter("requestID", grpReq.getRequestID());
		qry.executeUpdate();
		return new DefaultServiceResponse(true);

	}

	@Override
	public ServiceResponce updateAgentsStatus(GroupBookingRequest grpReq) {

		StringBuilder hql = new StringBuilder("UPDATE GroupBookingRequest SET status=:status , agentComments=:agentComments ");
		hql.append("WHERE requestID=:requestID ");

		Query qry = getSession().createQuery(hql.toString()).setParameter("status", grpReq.getStatusID())
				.setParameter("agentComments", grpReq.getAgentComments()).setParameter("requestID", grpReq.getRequestID());
		qry.executeUpdate();
		return new DefaultServiceResponse(true);

	}

	@Override
	public Page<GroupBookingRequest> get(GroupBookingRequest grpReq, Integer start, Integer size, String userCode,
			boolean mainReqestOnly) {

		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder("FROM GroupBookingRequest gbr ");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE gbr.statusID in(:status)");
		StringBuilder promoOrderClause = new StringBuilder(" ORDER BY gbr.requestID DESC ");

		GroupBookingReqRoutes routes = grpReq.getGroupBookingRoutes().iterator().next();

		if (grpReq.getRequestID() != null) {
			promoConditionClause.append(" and gbr.requestID like :requestID ");
		}

		if (!routes.getArrival().equals("")) {
			if (promoCriteriaQueryStringBuilder.toString().indexOf("routes") == -1) {
				promoCriteriaQueryStringBuilder.append(" join gbr.groupBookingRoutes routes ");
			}
			promoConditionClause.append(" and routes.arrival like :arrival ");
		}

		if (!routes.getDeparture().equals("")) {
			if (promoCriteriaQueryStringBuilder.toString().indexOf("routes") == -1) {
				promoCriteriaQueryStringBuilder.append(" join gbr.groupBookingRoutes routes ");
			}
			promoConditionClause.append(" and routes.departure like :departure ");
		}

		if (userCode != null) {
			promoConditionClause.append(" and gbr.craeteUserCode=:craeteUserCode ");
		}

		if (mainReqestOnly) {
			promoConditionClause.append(" and gbr.mstGroupBookingRequestId is null ");
		}

		if (grpReq.getStationCode() != null) {
			promoConditionClause.append(" and gbr.stationCode=:stationCode ");
		}

		if (grpReq.getRequestedAgentCode() != null) {
			promoConditionClause.append(" and gbr.requestedAgentCode=:requestedAgentCode ");
		}

		String promoCriteriaQueryString = null;

		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString()
				+ promoOrderClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);

		if (grpReq.getRequestID() != null) {
			query.setParameter("requestID", grpReq.getRequestID());
		}

		if (grpReq.getStatusID() != -1) {
			query.setParameterList("status", Arrays.asList(grpReq.getStatusID()));
		} else {
			query.setParameterList("status", Arrays.asList(4, 5, 6, 8));
		}

		if (!routes.getArrival().equals("")) {
			query.setParameter("arrival", routes.getArrival());

		}

		if (!routes.getDeparture().equals("")) {
			query.setParameter("departure", routes.getDeparture());
		}

		if (userCode != null) {
			query.setParameter("craeteUserCode", userCode);
		}

		if (grpReq.getStationCode() != null) {
			query.setParameter("stationCode", grpReq.getStationCode());
		}

		if (grpReq.getRequestedAgentCode() != null) {
			query.setParameter("requestedAgentCode", grpReq.getRequestedAgentCode());
		}

		int count = query.list().size();

		query.setFirstResult(start).setMaxResults(size);

		Collection<GroupBookingRequest> results = query.list();

		return new Page<GroupBookingRequest>(count, start, start + results.size(), results);
	}

	@Override
	public ServiceResponce saveRollForward(long requestID, Date from, Date to) {

		GroupBookingRequest existingRequest = find(requestID);

		existingRequest.setRequestID((long) 0);
		existingRequest.setVersion(-1);
		long deltaDays = (to.getTime() - from.getTime()) / (24 * 60 * 60 * 1000);

		// long requestId = (Long) save(existingRequest);

		return new DefaultServiceResponse(true);
	}

	@Override
	public GroupBookingRequest find(long requestID) {

		return (GroupBookingRequest) get(GroupBookingRequest.class, requestID);

	}

	@Override
	public ServiceResponce updateBookingStatus(long requestID, BigDecimal payment) {

		DefaultServiceResponse returnServiceResponce = new DefaultServiceResponse();
		GroupBookingRequest grpBooking = find(requestID);
		if (grpBooking.getStatusID() == STATUS.Released_Seats.getValue()) { // Released seat todo- Replace the status
																			// with enum

			returnServiceResponce.setSuccess(true);

		} else {
			returnServiceResponce.setResponseCode("NoApprovedRequest");
			returnServiceResponce.setSuccess(false);
		}

		return returnServiceResponce;

	}

	@Override
	public void updateBookingPaymentStatus(long requestID, BigDecimal totalPayment) {
		DefaultServiceResponse returnServiceResponce = new DefaultServiceResponse();
		StringBuilder hql = new StringBuilder("UPDATE GroupBookingRequest SET status=:status,payment_amount=:payment_amount ");
		hql.append("WHERE requestID=:requestID and status=7 ");

		Query qry = getSession().createQuery(hql.toString()).setParameter("status", STATUS.Booked.getValue())
				.setParameter("requestID", requestID).setParameter("payment_amount", totalPayment);
		qry.executeUpdate();

	}

	public GroupBookingRequest getValidGroupBookingRequest(long requestID, BigDecimal payment) {

		GroupBookingRequest grpBooking = find(requestID);

		if (grpBooking != null && grpBooking.getStatusID() == STATUS.Released_Seats.getValue()) {
			return grpBooking;
		} else {
			return null;
		}
	}

	@Override
	public Collection<GroupPaymentNotificationDTO> getGrooupBookingForReminders() {
		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder("FROM GroupBookingRequest gbr ");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE gbr.statusID=:status");

		String promoCriteriaQueryString = null;

		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);
		Collection<GroupBookingRequest> results = query.list();

		return null;
	}

	@Override
	public ServiceResponce saveUserStation(UserStation userStation) {

		hibernateSaveOrUpdate(userStation);
		return new DefaultServiceResponse(true);

	}

	@Override
	public Collection<UserStation> getUserStation(String userId) {

		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder("FROM UserStation us ");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE us.userId=:userId");

		String promoCriteriaQueryString = null;

		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);
		if (userId != null) {
			query.setParameter("userId", userId);
		}
		Collection<UserStation> results = query.list();

		return results;

	}

	@Override
	public ServiceResponce saveGroupBookingStation(GroupRequestStation grpRequestStation) {

		hibernateSaveOrUpdate(grpRequestStation);
		return new DefaultServiceResponse(true);

	}

	@Override
	public Collection<UserStation> getGroupBookingStation(long groupBookingReqId) {
		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder("FROM GroupRequestStation gbs ");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE gbs.groupBookingRequestID=:groupBookingReqId");

		String promoCriteriaQueryString = null;

		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);
		if (groupBookingReqId > 0) {
			query.setParameter("groupBookingReqId", groupBookingReqId);
		}
		Collection<UserStation> results = query.list();

		return results;
	}

	@Override
	public ServiceResponce updateAgentRequote(GroupBookingRequest grpReq) {

		StringBuilder hql = new StringBuilder(
				"UPDATE GroupBookingRequest SET status=:status ,requestedFare=:requestedFare, agentComments=:agentComments ");
		hql.append("WHERE requestID=:requestID ");

		Query qry = getSession().createQuery(hql.toString()).setParameter("status", grpReq.getStatusID())
				.setParameter("agentComments", grpReq.getAgentComments()).setParameter("requestID", grpReq.getRequestID())
				.setParameter("requestedFare", grpReq.getRequestedFare());
		qry.executeUpdate();
		return new DefaultServiceResponse(true);
	}

	@Override
	public Page<GroupBookingRequest> getSharedRequest(GroupBookingRequest grpReq, Integer start, Integer size, String userCode,
			String stations, boolean mainReqestOnly) {

		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder("select gbr FROM GroupBookingRequest gbr");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE gbr.statusID in(:status)");
		StringBuilder promoOrderClause = new StringBuilder(" ORDER BY gbr.requestID DESC ");
		if (grpReq.getRequestID() != null) {
			promoConditionClause.append(" and gbr.requestID like :requestID ");
		}
		if ((grpReq.getStationCode() != null && !grpReq.getStationCode().equals("")) || (userCode != null)) {

			promoCriteriaQueryStringBuilder.append(" join gbr.groupBookingRoutes routes ");

			promoConditionClause.append(" and (gbr.stationCode like :stationCode ");

			promoConditionClause.append(" or (routes.arrival like :stationCode or " + " routes.departure like :stationCode or "
					+ " routes.via like :stationCode ))");
		}

		else if (stations != null && !stations.equals("")) {
			String[] tempStr = stations.split(",");
			Collection<String> stationCodes = Arrays.asList(tempStr);
			StringBuilder strString = new StringBuilder();

			for (int i = 0; i < tempStr.length; i++) {
				if (i == tempStr.length - 1) {
					strString.append("'" + tempStr[i] + "'");
				} else {
					strString.append("'" + tempStr[i] + "',");
				}
			}
			// / promoConditionClause.append(" and gbr.craeteUserCode like :userCode");
			// query.setParameter("stationCodes", strString.toString());gbr.craeteUserCode like :userCode or
			promoCriteriaQueryStringBuilder.append(" join gbr.groupStations stations ");
			promoConditionClause.append(" and stations.stationCode in (" + strString.toString() + ")");

		}

		GroupBookingReqRoutes routes = grpReq.getGroupBookingRoutes().iterator().next();
		if (!routes.getArrival().equals("")) {

			if (promoCriteriaQueryStringBuilder.toString().indexOf("routes") == -1) {
				promoCriteriaQueryStringBuilder.append(" join gbr.groupBookingRoutes routes ");
			}
			promoConditionClause.append(" and routes.arrival like :arrival ");

		}
		if (!routes.getDeparture().equals("")) {
			if (promoCriteriaQueryStringBuilder.toString().indexOf("routes") == -1) {
				promoCriteriaQueryStringBuilder.append(" join gbr.groupBookingRoutes routes ");
			}
			promoConditionClause.append(" and routes.departure like :departure ");

		}

		if (grpReq.getRequestedAgentCode() != null && !grpReq.getRequestedAgentCode().equals("")) {
			promoConditionClause.append(" and gbr.requestedAgentCode like :requestedAgentCode ");
		}
		if (mainReqestOnly) {
			promoConditionClause.append(" and gbr.mstGroupBookingRequestId is null ");
		}

		String promoCriteriaQueryString = null;

		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString()
				+ promoOrderClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);

		if (grpReq.getRequestID() != null) {
			query.setParameter("requestID", grpReq.getRequestID());
		}
		if (grpReq.getStatusID() != -1) {
			query.setParameterList("status", Arrays.asList(grpReq.getStatusID()));
		} else {
			query.setParameterList("status", Arrays.asList(4, 5, 6, 8));
		}
		if (grpReq.getRequestedAgentCode() != null && !grpReq.getRequestedAgentCode().equals("")) {
			query.setParameter("requestedAgentCode", grpReq.getRequestedAgentCode());
		}
		if (!routes.getArrival().equals("")) {
			query.setParameter("arrival", routes.getArrival());

		}
		if (!routes.getDeparture().equals("")) {
			query.setParameter("departure", routes.getDeparture());
		}
		if (grpReq.getStationCode() != null && !grpReq.getStationCode().equals("")) {
			query.setParameter("stationCode", "%" + grpReq.getStationCode() + "%");
		} else if (userCode != null) {
			query.setParameter("stationCode", "%" + userCode + "%");
		}

		if (stations != null && !stations.equals("")) {
			String[] tempStr = stations.split(",");
			Collection<String> stationCodes = Arrays.asList(tempStr);
			StringBuilder strString = new StringBuilder();
			strString.append("(");
			for (int i = 0; i < tempStr.length; i++) {
				if (i == tempStr.length - 1) {
					strString.append("'" + tempStr[i] + "')");
				} else {
					strString.append("'" + tempStr[i] + "',");
				}
			}
			// query.setParameter("stationCodes", strString.toString());
		}

		int count = query.list().size();

		query.setFirstResult(start).setMaxResults(size);

		Collection<GroupBookingRequest> results = query.list();

		if (userCode == null) {
			Set setItems = new LinkedHashSet(results);
			results.clear();
			results.addAll(setItems);
		}

		return new Page<GroupBookingRequest>(count, start, start + results.size(), results);
	}

	@Override
	public ServiceResponce saveGroupBookingHistory(GroupBookingRequestHistory grpHistory) {
		hibernateSave(grpHistory);
		return new DefaultServiceResponse(true);
	}

	@Override
	public Page<GroupBookingRequestHistory> getGroupBookingHistory(long requestID, Integer start, Integer size) {
		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder("FROM GroupBookingRequestHistory grph ");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE grph.groupBookingRequestID=:requestID");

		String promoCriteriaQueryString = null;

		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);
		if (requestID > 0) {
			query.setParameter("requestID", requestID);
		}
		Collection<GroupBookingRequestHistory> results = query.list();

		int count = results.size();

		query.setFirstResult(start).setMaxResults(size);

		return new Page<GroupBookingRequestHistory>(count, start, start + results.size(), results);
	}

	@Override
	public Collection<Reservation> getReservation(long groupBookingReqId) {
		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder("FROM Reservation r ");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE r.groupBookingRequestID=:groupBookingReqId");

		String promoCriteriaQueryString = null;

		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);
		if (groupBookingReqId > 0) {
			query.setParameter("groupBookingReqId", groupBookingReqId);
		}
		Collection<Reservation> results = query.list();

		return results;
	}

	@Override
	public ServiceResponce reservationBookingStatus(long requestID, String pnr) {
		// DefaultServiceResponse returnServiceResponce = new DefaultServiceResponse();
		StringBuilder hql = new StringBuilder("UPDATE Reservation SET groupBookingRequestID=:groupBookingRequestID ");
		hql.append("WHERE pnr=:pnr ");

		Query qry = getSession().createQuery(hql.toString()).setParameter("groupBookingRequestID", requestID).setParameter("pnr",
				pnr);
		qry.executeUpdate();
		return new DefaultServiceResponse(true);

	}

	@Override
	public String getActualPaymentByPnr(String pnr) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "select abs(sum(amount)*-1) as amount from t_pax_transaction where pnr_pax_id in (select pnr_pax_id from t_pnr_passenger where pnr in ("
				+ pnr
				+ ") ) union all select sum(amount)*-1 as amount from t_pax_transaction where pnr_pax_id in (select pnr_pax_id from t_pnr_passenger where pnr in ("
				+ pnr + ")) and dr_cr = 'CR' ";

		String actualPaymentDetails = (String) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				StringBuilder paymentDetails = new StringBuilder();
				if (rs != null) {
					boolean firstEle = true;
					while (rs.next()) {
						if (firstEle) {
							paymentDetails
									.append(BeanUtils.nullHandler("Balance to pay : " + rs.getString("amount") + " AED / "));
							firstEle = false;
						} else {
							paymentDetails
									.append(BeanUtils.nullHandler("Total paid amount : " + rs.getString("amount") + " AED "));
						}
					}
				}
				return paymentDetails.toString();
			}
		});

		return BeanUtils.nullHandler(actualPaymentDetails);
	}
}
