package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.revacc.AmountProrateTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;

/**
 * This class is responsible for linking the refundable charges to the corresponding payment when cancellation or
 * modification with NC 3
 * 
 * @author Nilindra Fernando
 * @since August 27, 2010
 */
class AmountProratorBO {

	private Collection<AmountProrateTO> colPlusAmountProrateTO = new ArrayList<AmountProrateTO>();

	private Collection<AmountProrateTO> colMinusAmountProrateTO = new ArrayList<AmountProrateTO>();

	protected void addPlusDifference(ReservationPaxOndCharge reservationPaxOndCharge,
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayments) {
		AmountProrateTO amountProrateTO = new AmountProrateTO();
		amountProrateTO.setReservationPaxOndCharge(reservationPaxOndCharge);
		amountProrateTO.setColReservationPaxOndPayment(colReservationPaxOndPayments);

		this.colPlusAmountProrateTO.add(amountProrateTO);
	}

	protected void addMinusDifference(ReservationPaxOndCharge reservationPaxOndCharge,
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		AmountProrateTO amountProrateTO = new AmountProrateTO();
		amountProrateTO.setReservationPaxOndCharge(reservationPaxOndCharge);
		amountProrateTO.setColReservationPaxOndPayment(colReservationPaxOndPayment);

		this.colMinusAmountProrateTO.add(amountProrateTO);
	}

	protected void execute(Collection<ReservationPaxOndCharge> colReservationPaxOndCharge) {

		Collection<AmountProrateTO> removeProrates = new ArrayList<AmountProrateTO>();
		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		for (AmountProrateTO minusAmountProrateTO : this.colMinusAmountProrateTO) {
			ReservationPaxOndCharge minusReservationPaxOndCharge = minusAmountProrateTO.getReservationPaxOndCharge();

			for (AmountProrateTO plusAmountProrateTO : this.colPlusAmountProrateTO) {
				ReservationPaxOndCharge plusReservationPaxOndCharge = plusAmountProrateTO.getReservationPaxOndCharge();
				// FIXME for correct previous payment .
				if (minusReservationPaxOndCharge.getChargeGroupCode().equals(plusReservationPaxOndCharge.getChargeGroupCode())
						&& minusReservationPaxOndCharge.getEffectiveAmount().abs().longValue() == plusReservationPaxOndCharge.getEffectiveAmount()
								.abs().longValue()) {

					colReservationPaxOndPayment.addAll(TnxGranularityUtils.getRespectiveReservationPaxOndPayment(
							minusAmountProrateTO.getColReservationPaxOndPayment(), minusAmountProrateTO
									.getReservationPaxOndCharge().getEffectiveAmount()));
					colReservationPaxOndPayment.addAll(TnxGranularityUtils.getRespectiveReservationPaxOndPayment(
							plusAmountProrateTO.getColReservationPaxOndPayment(), plusAmountProrateTO
									.getReservationPaxOndCharge().getEffectiveAmount()));

					removeProrates.add(plusAmountProrateTO);
				}
			}
		}

		if (colReservationPaxOndPayment.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
		}

		if (removeProrates.size() > 0) {
			this.colPlusAmountProrateTO.removeAll(removeProrates);
		}

		if (this.colPlusAmountProrateTO.size() > 0) {
			for (AmountProrateTO amountProrateTO : colPlusAmountProrateTO) {
				colReservationPaxOndCharge.add(amountProrateTO.getReservationPaxOndCharge());
			}
		}
	}

}
