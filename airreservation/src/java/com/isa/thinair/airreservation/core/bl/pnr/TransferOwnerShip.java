/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for transfering a reservation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="transferOwnerShip"
 */
public class TransferOwnerShip extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(TransferOwnerShip.class);

	/**
	 * Execute method of the TransferOwnerShip command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("Inside execute");
		}
		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String ownerAgentCode = (String) this.getParameter(CommandParamNames.OWNER_AGENT_CODE);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Boolean isDummyReservation = (Boolean) this.getParameter(CommandParamNames.IS_DUMMY_RESERVATION);

		// Checking params
		this.validateParams(pnr, ownerAgentCode, version, isDummyReservation, credentialsDTO);

		// Loads the reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Check Transfer Ownership constraints if it's not a dummy booking
		if (!isDummyReservation) {
			this.checkTransferOwnershipConstraints(reservation, version);
		}

		String originOwnerAgentCode = "";

		// Set the new owner agent code
		if (reservation.getAdminInfo() == null) {
			ReservationAdminInfo adminInfo = new ReservationAdminInfo();
			adminInfo.setOwnerAgentCode(ownerAgentCode);
			adminInfo.setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
			reservation.setAdminInfo(adminInfo);
		} else {
			originOwnerAgentCode = BeanUtils.nullHandler(reservation.getAdminInfo().getOwnerAgentCode());

			// If the owner agent code is empty and owner channel is IBE then making the origin agent as WEB in the
			// audit
			if (originOwnerAgentCode.equals("")
					&& reservation.getAdminInfo().getOwnerChannelId()
							.equals(new Integer(ReservationInternalConstants.SalesChannel.WEB))) {
				originOwnerAgentCode = "WEB";
			} else if (originOwnerAgentCode.equals("")
					&& reservation.getAdminInfo().getOwnerChannelId()
							.equals(new Integer(ReservationInternalConstants.SalesChannel.IOS))) {
				originOwnerAgentCode = "IOS";
			} else if (originOwnerAgentCode.equals("")
					&& reservation.getAdminInfo().getOwnerChannelId()
							.equals(new Integer(ReservationInternalConstants.SalesChannel.ANDROID))) {
				originOwnerAgentCode = "ANDROID";
			}

			reservation.getAdminInfo().setOwnerAgentCode(ownerAgentCode);
			reservation.getAdminInfo().setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
		}

		// Saves the reservation
		ReservationProxy.saveReservation(reservation);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (!isDummyReservation) {
			// Compose Modification object
			ReservationAudit reservationAudit = this.composeAudit(pnr, originOwnerAgentCode, ownerAgentCode, credentialsDTO);
			Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
			colReservationAudit.add(reservationAudit);
			// Constructing and returning the command response
			response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
			response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		}
		
		response.addResponceParam(CommandParamNames.VERSION, Long.valueOf(reservation.getVersion()));

		if (log.isDebugEnabled()) {
			log.debug("Exit execute");
		}
		return response;
	}

	/**
	 * Check Transfer Ownership Constraints
	 * 
	 * @param reservation
	 * @param version
	 * @throws ModuleException
	 */
	private void checkTransferOwnershipConstraints(Reservation reservation, Long version) throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());
	}

	/**
	 * Compose audit
	 * 
	 * @param pnr
	 * @param originOwnerAgentCode
	 * @param ownerAgentCode
	 * @param credentialsDTO
	 * @return
	 */
	private ReservationAudit composeAudit(String pnr, String originOwnerAgentCode, String ownerAgentCode,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.OWNERSHIP_TRANSFERED.getCode());
		reservationAudit.setUserNote(null);

		List<String> agentsCodeList = new ArrayList<String>();
		agentsCodeList.add(originOwnerAgentCode);
		agentsCodeList.add(ownerAgentCode);

		// We need to append the agent code too. Cause there are some users referring to the agent's from the agent code
		// So it's always good to show both the agent code and the agent desc
		Map<String, Agent> travelAgentsMap = ReservationModuleUtils.getTravelAgentBD().getAgentsMap(agentsCodeList);
		String originAgentDesc = " (" + originOwnerAgentCode + ") ";
		if (travelAgentsMap.get(originOwnerAgentCode) != null) { // Null if originOwnerAgentCode == "WEB"
			originAgentDesc += travelAgentsMap.get(originOwnerAgentCode).getAgentDisplayName();
		}
		String ownerAgentDesc = " (" + ownerAgentCode + ") " + travelAgentsMap.get(ownerAgentCode).getAgentName();

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OwnershipTransfered.CURRENT_OWNER, originAgentDesc);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OwnershipTransfered.NEW_OWNER, ownerAgentDesc);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OwnershipTransfered.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OwnershipTransfered.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param ownerAgentCode
	 * @param version
	 * @param isDummyReservation
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, String ownerAgentCode, Long version, Boolean isDummyReservation,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || ownerAgentCode == null || ((!isDummyReservation) ? (version == null) : false)
				|| credentialsDTO == null || isDummyReservation == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}
}
