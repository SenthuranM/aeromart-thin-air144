package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.ResUniqueIdMapper;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.AdjustFareSurchargeTaxBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.segment.FlownAssitUnit;
import com.isa.thinair.airreservation.core.bl.segment.ModifyAssitUnit;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;

/**
 * Command for handling discount related operations on Fare
 * 
 * @author Dilan
 * 
 * @isa.module.command name="requoteBalanceCalculator"
 */

public class RequoteBalanceCalculator extends DefaultBaseCommand {
	private static Log log = LogFactory.getLog(RequoteBalanceCalculator.class);
	private RequoteBalanceQueryRQ balanceQueryTO = null;
	private Boolean isVoidOperation = false;
	private boolean isFareAdjustmentValid = false;
	private CredentialsDTO credentialsDTO;
	private boolean isUserCancelation = false;
	private Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap = null;
	ReservationDiscountDTO reservationDiscountDTO = null;

	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("RequoteBalanceCalculator entering...");
		setParams();
		validateParameters();

		IBalanceCalculator balanceCalculator = getBalanceCalculator();
		balanceCalculator.calculate();

		ReservationBalanceTO balanceSummary = balanceCalculator.getReservationBalance();
		balanceSummary.setFareAdjustmentValid(isFareAdjustmentValid);

		// on cancel segment operation wipe off the balance due amount from PAX.
		balanceSummary = applySystemChargeAdjustmentForBalanceDueAmount(balanceCalculator, balanceSummary);

		if (AppSysParamsUtil.isLMSEnabled()) {
			updateLMSPaxSummaryWithThresholdValues(balanceSummary);
		}

		// Compose the response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.BALANCE_SUMMARY_DTO, balanceSummary);
		log.debug("RequoteBalanceCalculator leaving...");
		return response;
	}

	private Collection<OndFareDTO> getOndFares(Reservation reservation) throws ModuleException {
		Collection<OndFareDTO> collFares = null;
		if (balanceQueryTO.getFareInfo() != null && balanceQueryTO.getFareInfo().hasOndFares()) {
			collFares = new ArrayList<OndFareDTO>();
			for (OndRebuildCriteria ondRebuildCriteria : balanceQueryTO.getFareInfo().getOndRebuildCriterias()) {
				collFares.addAll(ReservationModuleUtils.getReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria));

			}
			if (balanceQueryTO.getFareInfo().getDiscountedFareDetails() != null) {
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.PRE_PROCESS_DISCOUNT);
				command.setParameter(CommandParamNames.RESERVATION, reservation);
				command.setParameter(CommandParamNames.OND_FARE_DTOS, collFares);
				command.setParameter(CommandParamNames.FARE_DISCOUNT_DTO,
						balanceQueryTO.getFareInfo().getDiscountedFareDetails().getFarePercentage());
				command.setParameter(CommandParamNames.FARE_DISCOUNT_INFO,
						balanceQueryTO.getFareInfo().getDiscountedFareDetails());
				command.setParameter(CommandParamNames.CREDENTIALS_DTO, this.credentialsDTO);
				ServiceResponce serviceResponce = command.execute();
				reservationDiscountDTO = (ReservationDiscountDTO) serviceResponce
						.getResponseParam(CommandParamNames.RESERVATION_DISCOUNT_DTO);
			}

			if (balanceQueryTO.getRequoteSegmentMap() != null && balanceQueryTO.getRequoteSegmentMap().size() > 0) {
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.FARE_ADJUSTMENT);
				command.setParameter(CommandParamNames.PNR, reservation.getPnr());
				command.setParameter(CommandParamNames.OND_FARE_DTOS, collFares);
				command.setParameter(CommandParamNames.REQUOTE_SEGMENT_MAP, balanceQueryTO.getRequoteSegmentMap());
				command.setParameter(CommandParamNames.IS_MODIFY_OND_OPERATION,
						balanceQueryTO.isModifySegment() || balanceQueryTO.isSameFlightModification());

				ServiceResponce serviceResponce = command.execute();
				isFareAdjustmentValid = (Boolean) serviceResponce.getResponseParam(CommandParamNames.FARE_ADJUSTMENT_VALID);

			}

			if (paxWiseAdjustmentAmountMap != null && paxWiseAdjustmentAmountMap.size() > 0) {
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ADJUST_AMOUNT_DUE);
				command.setParameter(CommandParamNames.PNR, reservation.getPnr());
				command.setParameter(CommandParamNames.OND_FARE_DTOS, collFares);
				command.setParameter(CommandParamNames.REQUOTE_SEGMENT_MAP, balanceQueryTO.getRequoteSegmentMap());
				command.setParameter(CommandParamNames.IS_MODIFY_OND_OPERATION,
						balanceQueryTO.isModifySegment() || balanceQueryTO.isSameFlightModification());
				command.setParameter(CommandParamNames.PAX_ADJUSTMENT_AMOUNT_MAP, paxWiseAdjustmentAmountMap);

				command.execute();
			}
		}
		return collFares;
	}

	private Collection<Integer> getFltSegIds(Collection<OndFareDTO> collFares) {
		Collection<Integer> newFltSegIds = null;
		if (collFares != null) {
			newFltSegIds = new HashSet<Integer>();
			for (OndFareDTO ondFare : collFares) {
				newFltSegIds.addAll(ondFare.getFlightSegmentIds());
			}
		}
		return newFltSegIds;
	}

	private IBalanceCalculator getBalanceCalculator() throws ModuleException {
		IBalanceCalculator balanceCalculator = null;
		Reservation reservation = loadReservation();
		// int version = Integer.parseInt(balanceQueryTO.getVersion());

		Set<Integer> cnxPnrSegIds = ResUniqueIdMapper.getPnrSegIds(balanceQueryTO.getRemovedSegmentIds());
		Collection<OndFareDTO> collFares = getOndFares(reservation);

		Collection<Integer> newFltSegIds = getFltSegIds(collFares);
		ModifyAssitUnit modifyAsst = new ModifyAssitUnit(cnxPnrSegIds, newFltSegIds, reservation, isVoidOperation);
		modifyAsst.setReservationDiscountDTO(reservationDiscountDTO);

		// Adjust Tax value applied
		AdjustFareSurchargeTaxBO adjustFareSurchargeTaxBO = new AdjustFareSurchargeTaxBO(collFares, balanceQueryTO.getPnr(),
				reservation, modifyAsst.isUserModification(), modifyAsst.getCnxPnrSegIds());
		adjustFareSurchargeTaxBO.adjustFareSurchargeTax(ChargeCodes.JN_FARE_SURCHARGE);

		Map<Integer, Collection<ExternalChgDTO>> reprotectedAndNewExtCharges = getReprotectedExternalCharges(reservation.getPnr(),
				modifyAsst.getExchangedPnrSegIds(), modifyAsst.getFlownPnrSegIds(), collFares);

		Map<Integer, Integer> oldFareIdByFltSegIdMap = balanceQueryTO.getOldFareIdByFltSegIdMap();
		Map<Integer, String> ondFareTypeByFareIdMap = balanceQueryTO.getOndFareTypeByFareIdMap();

		boolean isSkipPenalty = ValidationUtils.isSkipPenaltyCalculation(collFares, modifyAsst, oldFareIdByFltSegIdMap,
				ondFareTypeByFareIdMap);

		boolean skipPenaltyWithinValidity = balanceQueryTO.isFQWithinValidity()
				&& AppSysParamsUtil.isApplyPenaltyWithinValidity();

		Map<Integer, List<ExternalChgDTO>> paxEffectiveTax = null;

		// Is modify segment
		if (modifyAsst.isUserModification()) {

			if (balanceQueryTO.getNameChangedPaxMap() != null && balanceQueryTO.getNameChangedPaxMap().size() > 0) {
				updateReprotectedExternalChargesWithNameChangeCharge(reprotectedAndNewExtCharges, reservation.getPnr(), collFares,
						balanceQueryTO.getNameChangedPaxMap());
			}
			paxEffectiveTax = updateExternalChargesWithServiceTax(reprotectedAndNewExtCharges, reservation, collFares,
					modifyAsst);

			boolean applyModCharge = modifyAsst.isUserModification();
			if (applyModCharge && AppSysParamsUtil.skipMODChargesForOpenSegmentModification()
					&& modifyAsst.skipMODChargeForOpenSegmentModification(ReservationApiUtils.isOpenReturnBooking(reservation))) {
				applyModCharge = false;
			}
			
			Collection<PnrChargesDTO> pnrPaxCharges = ReservationModuleUtils.getSegmentBD().getOndChargesForRequote(true,
					reservation.getPnr(), cnxPnrSegIds, newFltSegIds, reservation.getVersion(), balanceQueryTO.getCustomCharges(),
					false, modifyAsst.isUserModification(), modifyAsst.isUserCancellation(), applyModCharge,
					collFares, (skipPenaltyWithinValidity || isSkipPenalty), modifyAsst.isVoidReservation(),
					isFareAdjustmentValid, balanceQueryTO.getOndFareTypeByFareIdMap(), credentialsDTO);

			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseCurrCharges = ReservationModuleUtils.getPassengerBD()
					.getPnrPaxIdWiseCurrentCharges(reservation.getPnr(), reservation.getVersion(), false,
							balanceQueryTO.getExcludedSegFarePnrSegIds());
			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseExcludedCharges = null;
			if (balanceQueryTO.getExcludedSegFarePnrSegIds() != null && !balanceQueryTO.getExcludedSegFarePnrSegIds().isEmpty()) {
				mapPnrPaxIdWiseExcludedCharges = ReservationModuleUtils.getPassengerBD().getPnrPaxIdWiseCurrentCharges(
						reservation.getPnr(), reservation.getVersion(), true, balanceQueryTO.getExcludedSegFarePnrSegIds());
			}

			FlownAssitUnit fau = new FlownAssitUnit(modifyAsst, true, collFares, (skipPenaltyWithinValidity || isSkipPenalty),
					balanceQueryTO.getOndFareTypeByFareIdMap());
			balanceCalculator = new ModificationBalanceCalculator(reservation, new Date(), collFares, pnrPaxCharges,
					mapPnrPaxIdWiseCurrCharges, fau, reprotectedAndNewExtCharges, mapPnrPaxIdWiseExcludedCharges, paxEffectiveTax,
					credentialsDTO);
		} else if (modifyAsst.isUserCancellation()) { // only removed segments

			if (balanceQueryTO.getNameChangedPaxMap() != null && balanceQueryTO.getNameChangedPaxMap().size() > 0) {
				updateReprotectedExternalChargesWithNameChangeCharge(reprotectedAndNewExtCharges, reservation.getPnr(), collFares,
						balanceQueryTO.getNameChangedPaxMap());
			}
			paxEffectiveTax = updateExternalChargesWithServiceTax(reprotectedAndNewExtCharges, reservation, collFares,
					modifyAsst);

			Collection<PnrChargesDTO> pnrPaxCharges;

			if (balanceQueryTO.isApplyModificationCharge()) {
				pnrPaxCharges = ReservationModuleUtils.getSegmentBD().getOndChargesForRequote(true, reservation.getPnr(),
						cnxPnrSegIds, newFltSegIds, reservation.getVersion(), balanceQueryTO.getCustomCharges(), false,
						modifyAsst.isUserModification(), false, true, collFares, isSkipPenalty, modifyAsst.isVoidReservation(),
						isFareAdjustmentValid, balanceQueryTO.getOndFareTypeByFareIdMap(), credentialsDTO);
			} else {
				pnrPaxCharges = ReservationModuleUtils.getSegmentBD().getOndChargesForRequote(true, reservation.getPnr(),
						cnxPnrSegIds, newFltSegIds, reservation.getVersion(), balanceQueryTO.getCustomCharges(), false,
						modifyAsst.isUserModification(), modifyAsst.isUserCancellation(), modifyAsst.isUserModification(),
						collFares, isSkipPenalty, modifyAsst.isVoidReservation(), isFareAdjustmentValid,
						balanceQueryTO.getOndFareTypeByFareIdMap(), credentialsDTO);
			}

			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseCurrCharges = ReservationModuleUtils.getPassengerBD()
					.getPnrPaxIdWiseCurrentCharges(reservation.getPnr(), reservation.getVersion(), false,
							balanceQueryTO.getExcludedSegFarePnrSegIds());
			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseExcludedCharges = null;
			if (balanceQueryTO.getExcludedSegFarePnrSegIds() != null && !balanceQueryTO.getExcludedSegFarePnrSegIds().isEmpty()) {
				mapPnrPaxIdWiseExcludedCharges = ReservationModuleUtils.getPassengerBD().getPnrPaxIdWiseCurrentCharges(
						reservation.getPnr(), reservation.getVersion(), true, balanceQueryTO.getExcludedSegFarePnrSegIds());
			}

			FlownAssitUnit fau = new FlownAssitUnit(modifyAsst, true, collFares, isSkipPenalty,
					balanceQueryTO.getOndFareTypeByFareIdMap());

			balanceCalculator = new CancellationBalanceCalculator(reservation, new Date(), pnrPaxCharges,
					mapPnrPaxIdWiseCurrCharges, collFares, fau, reprotectedAndNewExtCharges, mapPnrPaxIdWiseExcludedCharges,
					balanceQueryTO.isApplyModificationCharge(), paxEffectiveTax);
			this.isUserCancelation = true;
		} else if (balanceQueryTO.isAddSegment()) { // only add segments

			if (balanceQueryTO.getNameChangedPaxMap() != null && balanceQueryTO.getNameChangedPaxMap().size() > 0) {
				updateReprotectedExternalChargesWithNameChangeCharge(reprotectedAndNewExtCharges, reservation.getPnr(), collFares,
						balanceQueryTO.getNameChangedPaxMap());
			}

			paxEffectiveTax = updateExternalChargesWithServiceTax(reprotectedAndNewExtCharges, reservation, collFares,
					modifyAsst);

			Collection<PnrChargesDTO> pnrPaxCharges = ReservationModuleUtils.getSegmentBD().getOndChargesForRequote(true,
					reservation.getPnr(), cnxPnrSegIds, newFltSegIds, reservation.getVersion(), null, false, false, false, false,
					collFares, (skipPenaltyWithinValidity || isSkipPenalty), modifyAsst.isVoidReservation(),
					isFareAdjustmentValid, balanceQueryTO.getOndFareTypeByFareIdMap(), credentialsDTO);

			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseCurrCharges = ReservationModuleUtils.getPassengerBD()
					.getPnrPaxIdWiseCurrentCharges(reservation.getPnr(), reservation.getVersion(), false,
							balanceQueryTO.getExcludedSegFarePnrSegIds());
			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseExcludedCharges = null;
			if (balanceQueryTO.getExcludedSegFarePnrSegIds() != null && !balanceQueryTO.getExcludedSegFarePnrSegIds().isEmpty()) {
				mapPnrPaxIdWiseExcludedCharges = ReservationModuleUtils.getPassengerBD().getPnrPaxIdWiseCurrentCharges(
						reservation.getPnr(), reservation.getVersion(), true, balanceQueryTO.getExcludedSegFarePnrSegIds());
			}

			FlownAssitUnit fau = new FlownAssitUnit(modifyAsst, true, collFares, (skipPenaltyWithinValidity || isSkipPenalty),
					balanceQueryTO.getOndFareTypeByFareIdMap());
			balanceCalculator = new AddOndBalanceCalculator(reservation, new Date(), collFares, pnrPaxCharges,
					mapPnrPaxIdWiseCurrCharges, fau, reprotectedAndNewExtCharges, mapPnrPaxIdWiseExcludedCharges,
					paxEffectiveTax, credentialsDTO);
		} else {
			throw new ModuleException("airreservation.modify.reservation.query.invalid.mod.type");
		}
		return balanceCalculator;
	}

	private void mergePaxExternalCharges(Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Map<Integer, List<ExternalChgDTO>> paxIdWiseExtChgMap) {

		if (paxIdWiseExtChgMap != null) {
			for (Entry<Integer, List<ExternalChgDTO>> extChgEntry : paxIdWiseExtChgMap.entrySet()) {
				List<ExternalChgDTO> lccExtChgList = extChgEntry.getValue();
				int pnrPaxId = extChgEntry.getKey();
				if (lccExtChgList != null && lccExtChgList.size() > 0) {
					if (!reprotectedExternalCharges.containsKey(pnrPaxId)) {
						reprotectedExternalCharges.put(pnrPaxId, new ArrayList<ExternalChgDTO>());
					}

					reprotectedExternalCharges.get(pnrPaxId).addAll(lccExtChgList);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Map<Integer, Collection<ExternalChgDTO>> getReprotectedExternalCharges(String pnr,
			Collection<Integer> exchangedPnrSegIds, Collection<Integer> flownPnrSegIds, Collection<OndFareDTO> collFares)
			throws ModuleException {
		if (exchangedPnrSegIds != null) {

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.REPROTECT_EXCHANGED_SEGMENT_ANCILLARY);
			command.setParameter(CommandParamNames.PNR, pnr);
			command.setParameter(CommandParamNames.EXCHANGE_PNR_SEGMENT_IDS, exchangedPnrSegIds);
			command.setParameter(CommandParamNames.FLOWN_PNR_SEGMENT_IDS, flownPnrSegIds);
			command.setParameter(CommandParamNames.OND_FARE_DTOS, collFares);
			command.setParameter(CommandParamNames.REQUOTE_MODIFY_RQ, false);
			command.setParameter(CommandParamNames.PAX_EXTERNAL_CHARGE_MAP,
					convertExternalChgMap(balanceQueryTO.getPaxExtChgMap()));
			ServiceResponce serviceResponce = command.execute();

			return (Map<Integer, Collection<ExternalChgDTO>>) serviceResponce
					.getResponseParam(CommandParamNames.NEW_AND_REPROTECT_EXT_CHARGES);
		}
		return null;
	}

	private Map<Integer, List<ExternalChgDTO>> updateExternalChargesWithServiceTax(
			Map<Integer, Collection<ExternalChgDTO>> allExternalCharges, Reservation reservation,
			Collection<OndFareDTO> collFares, ModifyAssitUnit modifyAsst) throws ModuleException {
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CALCULATE_EFFECTIVE_TAX_FOR_SERVICES);
		command.setParameter(CommandParamNames.RESERVATION, reservation);
		// command.setParameter(CommandParamNames.PAX_TAXING_EXT_CHARGE_MAP, balanceQueryTO.getPaxExtChgMap());
		command.setParameter(CommandParamNames.EXCHG_REPROTECTED_EXTERNAL_CHARGES, allExternalCharges);
		command.setParameter(CommandParamNames.OND_FARE_DTOS, collFares);
		command.setParameter(CommandParamNames.REQUOTE_SEGMENT_MAP, balanceQueryTO.getRequoteSegmentMap());
		command.setParameter(CommandParamNames.FLOWN_PNR_SEGMENT_IDS, modifyAsst.getFlownPnrSegIds());
		command.setParameter(CommandParamNames.IS_CANCEL_OND_OPERATION, modifyAsst.isUserCancellation());
		command.setParameter(CommandParamNames.IS_ADD_OND_OPERATION, balanceQueryTO.isAddSegment());
		ServiceResponce serviceResponce = command.execute();

		return (Map<Integer, List<ExternalChgDTO>>) serviceResponce.getResponseParam(CommandParamNames.EFFECTIVE_PAX_TAX_CHARGES);
	}

	private Map<Integer, List<ExternalChgDTO>> updateReprotectedExternalChargesWithNameChangeCharge(
			Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges, String pnr, Collection<OndFareDTO> collFares,
			Map<Integer, NameDTO> map) throws ModuleException {
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CALCULATE_NAME_CHANGE_CHARGE);
		command.setParameter(CommandParamNames.PNR, pnr);
		command.setParameter(CommandParamNames.EXCHG_REPROTECTED_EXTERNAL_CHARGES, reprotectedExternalCharges);
		command.setParameter(CommandParamNames.NAME_CHANGE_PAX_MAP, balanceQueryTO.getNameChangedPaxMap());
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		ServiceResponce serviceResponce = command.execute();

		return (Map<Integer, List<ExternalChgDTO>>) serviceResponce.getResponseParam(CommandParamNames.EFFECTIVE_PAX_TAX_CHARGES);
	}

	private void setParams() {
		this.balanceQueryTO = (RequoteBalanceQueryRQ) this.getParameter(CommandParamNames.BALANCE_QUERY_DTO);
		this.isVoidOperation = (Boolean) this.getParameter(CommandParamNames.IS_VOID_OPERATION);
		this.credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
	}

	private void validateParameters() throws ModuleException {
		if (this.balanceQueryTO == null) {
			throw new ModuleException("invalid.params");// FIXME proper error code
		}
	}

	private Reservation loadReservation() throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(balanceQueryTO.getPnr());
		pnrModesDTO.setGroupPNR(balanceQueryTO.getGroupPnr());
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	private Map<Integer, List<ExternalChgDTO>> convertExternalChgMap(Map<Integer, List<LCCClientExternalChgDTO>> lccExtChgMap) {
		Map<Integer, List<ExternalChgDTO>> extChgMap = new HashMap<Integer, List<ExternalChgDTO>>();
		for (Entry<Integer, List<LCCClientExternalChgDTO>> extChgEntry : lccExtChgMap.entrySet()) {
			List<LCCClientExternalChgDTO> lccExtChgList = extChgEntry.getValue();
			if (lccExtChgList != null && lccExtChgList.size() > 0) {
				extChgMap.put(extChgEntry.getKey(), ReservationApiUtils.populateExternalCharges(lccExtChgList));
			}

		}
		return extChgMap;
	}

	/**
	 * following function will calculate PAX wise balance due amount.
	 * 
	 */
	private ReservationBalanceTO applySystemChargeAdjustmentForBalanceDueAmount(IBalanceCalculator balanceCalculator,
			ReservationBalanceTO balanceSummary) throws ModuleException {

		if (AppSysParamsUtil.isSystemChargeAdjustmentForDueAmountEnabled() && isUserCancelation && balanceSummary != null
				&& balanceSummary.getTotalAmountDue().doubleValue() > 0) {
			Collection<LCCClientPassengerSummaryTO> passengerSummaryList = balanceSummary.getPassengerSummaryList();
			boolean isReCalculateBalanceSummary = false;
			this.paxWiseAdjustmentAmountMap = new HashMap<Integer, BigDecimal>();
			if (passengerSummaryList != null && passengerSummaryList.size() > 0) {
				for (LCCClientPassengerSummaryTO passengerSummaryTO : passengerSummaryList) {
					String travelRefId = passengerSummaryTO.getTravelerRefNumber();

					if (passengerSummaryTO.getTotalAmountDue().doubleValue() > 0 && !StringUtil.isNullOrEmpty(travelRefId)
							&& (!PaxTypeTO.INFANT.equals(passengerSummaryTO.getPaxType())
									|| balanceSummary.isInfantPaymentSeparated()
											&& PaxTypeTO.INFANT.equals(passengerSummaryTO.getPaxType()))) {
						Integer pnrPaxId = new Integer(travelRefId.split("\\$")[1]);
						paxWiseAdjustmentAmountMap.put(pnrPaxId, passengerSummaryTO.getTotalAmountDue());
						isReCalculateBalanceSummary = true;
					}

				}
			}

			if (isReCalculateBalanceSummary && paxWiseAdjustmentAmountMap != null && paxWiseAdjustmentAmountMap.size() > 0) {

				balanceCalculator = getBalanceCalculator();
				balanceCalculator.calculate();

				balanceSummary = balanceCalculator.getReservationBalance();
				balanceSummary.setFareAdjustmentValid(isFareAdjustmentValid);
				balanceSummary.setPaxWiseAdjustmentAmountMap(paxWiseAdjustmentAmountMap);

			}
		}

		return balanceSummary;

	}

	private void updateLMSPaxSummaryWithThresholdValues(ReservationBalanceTO balanceSummary) {

		try {
			List<LoyaltyProduct> loyaltyProducts = ReservationModuleUtils.getLoyaltyManagementBD().getLoyaltyProducts();
			Double remainingLoyaltyPoints = balanceQueryTO.getRemainingLoyaltyPoints();

			if (remainingLoyaltyPoints != null) {
				for (LCCClientPassengerSummaryTO lccClientPassengerSummaryTO : balanceSummary.getPassengerSummaryList()) {
					if (lccClientPassengerSummaryTO.getCarrierProductCodeWiseAmountDue() != null
							&& lccClientPassengerSummaryTO.getCarrierProductCodeWiseAmountDue().size() > 0) {
						Map<String, BigDecimal> carrierPaxProductAmounts = lccClientPassengerSummaryTO
								.getCarrierProductCodeWiseAmountDue().values().iterator().next();
						for (String productCode : carrierPaxProductAmounts.keySet()) {
							BigDecimal productAmount = carrierPaxProductAmounts.get(productCode);
							if (productAmount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
								for (LoyaltyProduct loyaltyProduct : loyaltyProducts) {
									if (loyaltyProduct.getProductId().equals(productCode)
											&& loyaltyProduct.getThresholdValue() != null
											&& loyaltyProduct.getThresholdValue().compareTo(remainingLoyaltyPoints) > 0) {
										productAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
										carrierPaxProductAmounts.put(productCode, productAmount);
										break;
									}
								}
							}
						}
						Map<String, Map<String, BigDecimal>> carrierProductCodeWiseAmountDueMap = new HashMap<String, Map<String, BigDecimal>>();
						carrierProductCodeWiseAmountDueMap.put(AppSysParamsUtil.getDefaultCarrierCode(),
								carrierPaxProductAmounts);
						lccClientPassengerSummaryTO.getCarrierProductCodeWiseAmountDue().clear();
						lccClientPassengerSummaryTO.setCarrierProductCodeWiseAmountDue(carrierProductCodeWiseAmountDueMap);
					}
				}
			}
		} catch (ModuleException exp) {
			log.error("Unable to retrieve loyalty product codes");
		}
	}
}
