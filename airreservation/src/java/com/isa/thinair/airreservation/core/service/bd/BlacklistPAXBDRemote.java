package com.isa.thinair.airreservation.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;

@Remote
public interface BlacklistPAXBDRemote extends BlacklistPAXBD {

}
