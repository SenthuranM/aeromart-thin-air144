/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.intermediate.MessageTypeIdentifier;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.DestinationElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator.base.BaseElementBuilderValidator;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.PNLADLMessageConstants;

/**
 * @author udithad
 *
 */
public class PaxModifierElementBuilder extends MessageTypeIdentifier {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private String messageType;

	private PassengerElementsContext pEContext;
	private DestinationFare destinationFare;

	private BaseRuleExecutor<RulesDataContext> destinationElementRuleExecutor = new DestinationElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		if (context.isPartCountExceeded()) {
			return;
		}
		initContextData(context);
		BaseElementBuilderValidator validator = getElementBuilderValidator(messageType);
		pEContext.setOngoingStoreType(validator.getOngoingStoreType(messageType, pEContext.getOngoingStoreType()));
		if (validator.validate(messageType)) {
			paxModifierElementTemplate();
			if (currentElement != null && !currentElement.isEmpty()) {
				clearContextGroupCodeMap();
				executeConcatenationElementBuilder(pEContext);
			}
			response = validateSubElement(currentElement);
			ammendMessageDataAccordingTo(response);
		} else {
			executeNext();
		}

	}
	
	private void clearContextGroupCodeMap(){
		if(pEContext.getGroupCodeAvailabilityMap() != null){
			pEContext.getGroupCodeAvailabilityMap().clear();
		}
	}

	private void initContextData(ElementContext context) {
		pEContext = (PassengerElementsContext) context;
		currentLine = pEContext.getCurrentMessageLine();
		messageLine = pEContext.getMessageString();
		messageType = pEContext.getMessageType();
		destinationFare = pEContext.getDestinationFare();
	}

	private void paxModifierElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		elementTemplate.append(getModifierElement());
		currentElement = elementTemplate.toString();
	}

	private String getModifierElement() {
		String modifierElement = "";
		pEContext.setOngoingStoreType(null);
		if (destinationFare != null
				&& destinationFare.getDeletePassengers() != null
				&& destinationFare.getDeletePassengers().size() > 0) {
			modifierElement = PNLADLMessageConstants.ADL_DEL;
			pEContext.setOngoingStoreType(PassengerStoreTypes.DELETED);
		} else if (destinationFare != null
				&& destinationFare.getAddPassengers() != null
				&& destinationFare.getAddPassengers().size() > 0) {
			modifierElement = PNLADLMessageConstants.ADL_ADD;
			pEContext.setOngoingStoreType(PassengerStoreTypes.ADDED);
		} else if (destinationFare != null
				&& destinationFare.getChangePassengers() != null
				&& destinationFare.getChangePassengers().size() > 0) {
			modifierElement = PNLADLMessageConstants.ADL_CHG;
			pEContext.setOngoingStoreType(PassengerStoreTypes.CHANGED);
		}
		return modifierElement;
	}

	private boolean isDataAvailaleForNextStep() {
		boolean isDataAvailable = false;
		if (destinationFare != null
				&& destinationFare.getDeletePassengers() != null
				&& destinationFare.getDeletePassengers().size() > 0) {
			isDataAvailable = true;

		} else if (destinationFare != null
				&& destinationFare.getAddPassengers() != null
				&& destinationFare.getAddPassengers().size() > 0) {

			isDataAvailable = true;
		} else if (destinationFare != null
				&& destinationFare.getChangePassengers() != null
				&& destinationFare.getChangePassengers().size() > 0) {
			isDataAvailable = true;

		}
		return isDataAvailable;
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (response.isProceedNextElement()) {
			if (currentElement != null && !currentElement.isEmpty()) {
				incrementPassengerCount();
				ammendToBaseLine(currentElement, currentLine, messageLine);
			}
		}
		executeNext();
	}
	
	private void incrementPassengerCount(){
		pEContext.setPassengersPerPart(pEContext.getPassengersPerPart()+1);
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = destinationElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null
				&& pEContext.getOngoingStoreType() != null
				&& isDataAvailaleForNextStep()) {
			nextElementBuilder.buildElement(pEContext);
		}
	}

}
