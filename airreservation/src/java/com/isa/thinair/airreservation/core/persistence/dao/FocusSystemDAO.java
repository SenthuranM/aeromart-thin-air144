package com.isa.thinair.airreservation.core.persistence.dao;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentFocusDTO;
import com.isa.thinair.airreservation.api.model.CreditCardSalesFocus;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;

public interface FocusSystemDAO extends ExternalSystemDAO {
	
	public void clearCreditCardSalesTable(Date date);
	
	public Collection<CreditCardSalesFocus> insertCreditCardSalesToInternal(Collection<CreditCardPaymentFocusDTO> col);
	
	public void updateInternalCreditTable(Collection<CreditCardSalesFocus> col);
	
	public Collection<CreditCardPaymentFocusDTO> getCreditCardPayments(Date date);	
	
	public void insertCreditCardSalesToExternal(Collection<CreditCardPaymentFocusDTO> col, AccontingSystemTemplate template) throws ClassNotFoundException, SQLException, Exception;	

}
