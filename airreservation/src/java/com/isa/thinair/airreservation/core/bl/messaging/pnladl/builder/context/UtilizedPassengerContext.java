package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context;

import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;

public class UtilizedPassengerContext extends ElementContext {

	private List<PassengerInformation> utilizedPassengers;
	private PassengerStoreTypes ongoingStoreType;
	private String pnr;
	private int lineCount;

	public List<PassengerInformation> getUtilizedPassengers() {
		return utilizedPassengers;
	}

	public void setUtilizedPassengers(
			List<PassengerInformation> utilizedPassengers) {
		this.utilizedPassengers = utilizedPassengers;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public PassengerStoreTypes getOngoingStoreType() {
		return ongoingStoreType;
	}

	public void setOngoingStoreType(PassengerStoreTypes ongoingStoreType) {
		this.ongoingStoreType = ongoingStoreType;
	}

	public int getLineCount() {
		return lineCount;
	}

	public void setLineCount(int lineCount) {
		this.lineCount = lineCount;
	}

}
