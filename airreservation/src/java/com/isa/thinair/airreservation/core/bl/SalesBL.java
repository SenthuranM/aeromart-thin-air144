/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.CashSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.CashSalesStatusDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardSalesStatusDTO;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.external.AccountingSystem;
import com.isa.thinair.airreservation.core.persistence.dao.ExternalCashSalesTransferDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SalesDAO;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.ExternalAccountingSystem;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;

/**
 * Sales specific business logic
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class SalesBL {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(SalesBL.class);

	/**
	 * Called By Schedule Service and Manual Screen
	 * 
	 * @param date
	 * @throws ModuleException
	 */
	public void transferCashSales(Date date) throws ModuleException {

		List<String> accountingSystemNames = ReservationModuleUtils.getAirReservationConfig().getCashSalesAccountingSystemList();
		for (String accountingSystemName : accountingSystemNames) {
			if (StringUtil.isNullOrEmpty(accountingSystemName)) {
				log.error("Couldn't find accounting system for Cash Sales. Please configure it");
				throw new ModuleException("module.dependencymap.invalid");
			} else {
				// Loads accounting system
				ExternalAccountingSystem accountingSystem = (ExternalAccountingSystem) BLUtil
						.getExternalAccoutingSystemsImplConfig().getAccountingSystemMap().get(accountingSystemName);
				// Loads DAO based on accounting system
				ExternalCashSalesTransferDAO cashSalesDAO = (ExternalCashSalesTransferDAO) ReservationDAOUtils
						.getExternalDAOImplConfig().getAccountingSystemDAOMap().get(accountingSystemName);
				cashSalesDAO.transferCashSales(date, accountingSystem);
			}
		}
	}

	/**
	 * Called By Schedule Service and Manual Screen
	 * 
	 * @param date
	 * @throws ModuleException
	 */
	public void transferCreditSales(Date date) throws ModuleException {

		List<String> accountingSystemNames = ReservationModuleUtils.getAirReservationConfig()
				.getCreditCardSalesAccountingSystemList();
		for (String accountingSystemName : accountingSystemNames) {
			if (StringUtil.isNullOrEmpty(accountingSystemName)) {
				log.error("Couldn't find accounting system for Credit Card sales. Please configure it");
				throw new ModuleException("module.dependencymap.invalid");
			} else {
				// Loads accounting system
				AccountingSystem accountingSystem = (AccountingSystem) ReservationModuleUtils.getImplConfig()
						.getAccountingSystemMap().get(accountingSystemName);
				accountingSystem.transferCreditSales(date);
			}
		}
	}

	/**
	 * Called By Schedule Service and Manual Screen
	 * 
	 * @param date
	 * @throws ModuleException
	 */
	public void transferTopUp(Date date) throws ModuleException {

		List<String> accountingSystemNames = ReservationModuleUtils.getAirReservationConfig().getTopUpAccountingSystemList();
		for (String accountingSystemName : accountingSystemNames) {
			if (StringUtil.isNullOrEmpty(accountingSystemName)) {
				log.error("Couldn't find accounting system for Top Up. Please configure it");
				throw new ModuleException("module.dependencymap.invalid");
			} else {
				// Loads accounting system
				AccountingSystem accountingSystem = (AccountingSystem) ReservationModuleUtils.getImplConfig()
						.getAccountingSystemMap().get(accountingSystemName);
				accountingSystem.transferTopUps(date);
				log.info(" transferTopUp  FINISHED " + date);

			}
		}
	}


	public Collection<AgentTotalSaledDTO> getAgentTotalSales(Date startDate, Date endDate) throws ModuleException {
		return ReservationDAOUtils.DAOInstance.SALES_DAO.getAgentTotalSales(startDate, endDate);
	}

	/**
	 * Called By Manual Screen
	 * 
	 * @param users
	 * @param fromDate
	 * @param toDate
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public CashSalesStatusDTO getCashSalesDetails(String users[], Date fromDate, Date toDate, String agentCode)
			throws ModuleException {
		SalesDAO salesDAO = ReservationDAOUtils.DAOInstance.SALES_DAO;
		CashSalesStatusDTO cashSalesStatusDTO = new CashSalesStatusDTO();
		cashSalesStatusDTO.setCashSales(getCashSalesHistory(fromDate, toDate));

		List<String> accountingSystemNames = ReservationModuleUtils.getAirReservationConfig().getCashSalesAccountingSystemList();
		for (String accountingSystemName : accountingSystemNames) {
			if (StringUtil.isNullOrEmpty(accountingSystemName)) {
				log.error("Couldn't find accounting system for Cash Sales. Please configure it");
				throw new ModuleException("module.dependencymap.invalid");
			} else {
				// Loads accounting system
				ExternalAccountingSystem accountingSystem = (ExternalAccountingSystem) BLUtil
						.getExternalAccoutingSystemsImplConfig().getAccountingSystemMap().get(accountingSystemName);
				cashSalesStatusDTO.setTotAmtGenerated(salesDAO.getGeneratedCashSalesTotal(fromDate, toDate));
				if (hasEXDB()) {
					cashSalesStatusDTO.setHasEXDB(true);
					cashSalesStatusDTO.setTotAmtTransfered(salesDAO.getTransferedCashSalesTotal(fromDate, toDate,
							(XDBConnectionUtil) accountingSystem));
				} else {
					cashSalesStatusDTO.setHasEXDB(false);
				}
			}
		}

		return cashSalesStatusDTO;
	}

	/**
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<CashSalesHistoryDTO> getCashSalesHistory(Date fromDate, Date toDate) {
		SalesDAO salesDAO = ReservationDAOUtils.DAOInstance.SALES_DAO;
		// will return 0-a list of date objects 1-a list of dto's
		Object[] o = salesDAO.getCashSalesHistory(fromDate, toDate);
		// hold the startdate
		GregorianCalendar calendarDate = new GregorianCalendar();
		calendarDate.setTime(fromDate);
		// hold the enddate
		GregorianCalendar calendarDateEnd = new GregorianCalendar();
		calendarDateEnd.setTime(toDate);

		// get the objects from the object array
		ArrayList<CashSalesHistoryDTO> dtoList = (ArrayList<CashSalesHistoryDTO>) o[0];
		ArrayList dateList = (ArrayList) o[1];

		// for all the dates..i.e from start to end, see if retrieved from the database
		// if not create your own dto, add it to the dto list and send it to f.e.
		while (calendarDate.before(calendarDateEnd) || calendarDate.equals(calendarDateEnd)) {

			if (!(dateList.contains(calendarDate.getTime()))) {
				CashSalesHistoryDTO historyDTO = new CashSalesHistoryDTO();
				historyDTO.setDateOfSale(calendarDate.getTime());
				historyDTO.setDisplayName("Not Generated");
				historyDTO.setTotal(AccelAeroCalculator.getDefaultBigDecimalZero());
				historyDTO.setTransferStatus("N");
				dtoList.add(historyDTO);
			}

			calendarDate.add(GregorianCalendar.DATE, +1);
		}
		// sort by date
		Collections.sort(dtoList, new LatestFirstSortComparatorCashSales());
		return dtoList;

	}

	@SuppressWarnings("rawtypes")
	private class LatestFirstSortComparatorCashSales implements Comparator {

		public int compare(Object first, Object second) {

			CashSalesHistoryDTO historyDTO1 = (CashSalesHistoryDTO) first;
			CashSalesHistoryDTO historyDTO2 = (CashSalesHistoryDTO) second;

			return (historyDTO1.getDateOfSale() == null || historyDTO2.getDateOfSale() == null) ? 0 : historyDTO1.getDateOfSale()
					.compareTo(historyDTO2.getDateOfSale()) * -1;
		}
	}

	private boolean hasEXDB() {

		if (ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer()
				.equalsIgnoreCase(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Called By Manual Screen
	 * 
	 * @Param cards
	 * @param fromDate
	 * @param toDate
	 * @throws ModuleException
	 */
	public CreditCardSalesStatusDTO getCreditCardSalesDetails(Integer cards[], Date fromDate, Date toDate) throws ModuleException {
		List<String> accountingSystemNames = ReservationModuleUtils.getAirReservationConfig()
				.getCreditCardSalesAccountingSystemList();
		CreditCardSalesStatusDTO creditCardSalesstatusDTO = null;
		for (String accountingSystemName : accountingSystemNames) {
			if (StringUtil.isNullOrEmpty(accountingSystemName)) {
				log.error("Couldn't find accounting system for Credit Card sales. Please configure it");
				throw new ModuleException("module.dependencymap.invalid");
			} else {
				// Loads accounting system
				AccountingSystem accountingSystem = (AccountingSystem) ReservationModuleUtils.getImplConfig()
						.getAccountingSystemMap().get(accountingSystemName);
				creditCardSalesstatusDTO = accountingSystem.getCreditCardSalesDetails(cards, fromDate, toDate);
			}
		}

		return creditCardSalesstatusDTO;
	}

	/**
	 * 
	 * @param datecollection
	 * @throws ModuleException
	 */
	public void transferManualCashSalesCollection(Collection<Date> dateCollection) throws ModuleException {

		validateDateCollection(dateCollection);

		ReservationAuxilliaryBD auxilliaryBD = ReservationModuleUtils.getReservationAuxilliaryBD();
		Iterator<Date> iter = dateCollection.iterator();
		while (iter.hasNext()) {
			auxilliaryBD.transferCashSales((Date) iter.next());
		}

	}

	/**
	 * 
	 * @param dateCollection
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateDateCollection(Collection dateCollection) throws ModuleException {
		// temporary threshold, to later use mdb for transferring and generating cash sales for more than one date.
		int threshold = new Integer(ReservationModuleUtils.getAirReservationConfig().getMaxNumOfDaysAllowed()).intValue();
		if (dateCollection != null && dateCollection.size() > 0) {

			if (dateCollection.size() > threshold) {
				throw new ModuleException("airreservation.cashsales.datelimit", AirreservationConstants.MODULE_NAME);
			}
		}

	}

	public void transferManualCreditCardSalesCollection(Collection<Date> dateCollection) throws ModuleException {

		validateDateCollection(dateCollection);
		ReservationAuxilliaryBD auxilliaryBD = ReservationModuleUtils.getReservationAuxilliaryBD();
		Iterator<Date> iter = dateCollection.iterator();
		while (iter.hasNext()) {
			auxilliaryBD.transferCreditCardSales((Date) iter.next());
		}

	}

}
