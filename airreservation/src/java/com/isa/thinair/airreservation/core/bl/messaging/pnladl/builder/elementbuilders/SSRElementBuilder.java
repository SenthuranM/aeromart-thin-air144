/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ChildElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.SSRElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class SSRElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> ssrElementRuleExecutor = new SSRElementRuleExecutor();

	private boolean isStartWithNewLine = false;

	@Override
	public void buildElement(ElementContext context) {
		if (context != null && context.getFeaturePack() != null) {
			initContextData(context);
			ssrElementTemplate();
		}
		executeNext();
	}

	private void ssrElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation != null
					&& passengerInformation.getSsrs() != null) {
				List<AncillaryDTO> ssrs = passengerInformation.getSsrs();
				if (ssrs != null) {
					for (AncillaryDTO anci : ssrs) {
						buildSsrElement(elementTemplate, passengerInformation,
								anci);
						currentElement = elementTemplate.toString();
						if (currentElement != null && !currentElement.isEmpty()) {
							ammendmentPreValidation(isStartWithNewLine, currentElement, uPContext, ssrElementRuleExecutor);
						}

					}

				}
			}
		}
	}

	private void buildSsrElement(StringBuilder elementTemplate,
			PassengerInformation passengerInformation, AncillaryDTO anci) {
		elementTemplate.setLength(0);
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
		elementTemplate.append(anci.getCode());
		elementTemplate.append(space());
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
		elementTemplate.append(space());
		elementTemplate.append(anci.getDescription());
		if (passengerInformations.size() > 1) {
			elementTemplate.append(hyphen());
			elementTemplate.append(1);
			elementTemplate.append(passengerInformation.getLastName());
			elementTemplate.append(forwardSlash());
			elementTemplate.append(passengerInformation.getFirstName());
			elementTemplate.append(passengerInformation.getTitle());
		}

	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
