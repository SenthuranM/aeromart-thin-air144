/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;

/**
 * @author udithad
 *
 */
public class NewLineElementBuilder extends BaseElementBuilder {

	private String lineSeparator;
	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	public NewLineElementBuilder() {
		lineSeparator = (String) java.security.AccessController
				.doPrivileged(new sun.security.action.GetPropertyAction(
						"line.separator"));
	}

	private void initContextData(ElementContext context) {
		currentLine = context.getCurrentMessageLine();
		messageLine = context.getMessageString();
		currentElement = lineSeparator;
	}

	@Override
	public void buildElement(ElementContext context) {
		initContextData(context);
		ammendToBaseLine(currentElement, currentLine, messageLine);
		clearCurrentLine(currentLine);
		if(context instanceof UtilizedPassengerContext){
			int lineCount = ((UtilizedPassengerContext) context).getLineCount();
			((UtilizedPassengerContext) context).setLineCount(++lineCount);
		}
	}
	
	private void clearCurrentLine(StringBuilder currentLine){
		currentLine.setLength(0);
	}

}
