/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import com.isa.thinair.airreservation.api.dto.PNRNumberGenerator;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;

/**
 * PNR Number Generator Proxy
 * 
 * @author Nilindra Fernando
 * @since Sep 12, 2008
 */
public class PNRNumberGeneratorProxy {

	/**
	 * Returns the New PNR Number
	 * 
	 * @param originatorPNR
	 * @param nextPNRSeqNumber
	 * @return
	 */
	public static String getNewPNR(boolean isGdsOrCsPnr, String nextPNRSeqNumber) {
		nextPNRSeqNumber = BeanUtils.nullHandler(nextPNRSeqNumber);

		if (nextPNRSeqNumber.length() == 0) {
			nextPNRSeqNumber = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getNextPnrNumber(isGdsOrCsPnr);
		}

		String newPNRNumber;

		// This get's trigger via GDS only. Cause GDS is having 6 digits PNR numbers.
		// If that happens we also need to generate alphanumeric PNR number.
		// TODO Please make it configurable. Than just depending on the length of the PNR number.
		// Nili 4:41 PM 10/29/2009

		// If the system is generating a numeric PNR and need to convert it to six digit alpha numeric, then have to use
		// below code.
		if (isGdsOrCsPnr) {
			PNRNumberGenerator pnrNumberGenerator = new GDSPNRNumberGeneratorImpl();
			newPNRNumber = pnrNumberGenerator.generatePNRNumber(Long.valueOf(nextPNRSeqNumber));
		} else {
			newPNRNumber = nextPNRSeqNumber;
		}

		return newPNRNumber;
	}
}