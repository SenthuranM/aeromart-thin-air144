/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerElement;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.BaseAdditionalInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;

/**
 * @author udithad
 *
 */
public class MealInformation extends
		PassengerAdditions<List<PassengerInformation>> {

	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> detailsDTOs) {
		for (PassengerInformation information : detailsDTOs) {
			List<AncillaryDTO> mealInformation = getMealAncillaryInformationListBy(information
					.getPnrPaxId());
			information.setMeals(mealInformation);
		}
	}

	private List<AncillaryDTO> getMealAncillaryInformationListBy(
			Integer passengerId) {
		return mealAncillaryInformation.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformation) {
		mealAncillaryInformation = PnlAdlUtil
				.getPassengerMealDetails(passengerInformation);
	}

}
