package com.isa.thinair.airreservation.core.bl.tty;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class AddInfantMessageCreator extends TypeBReservationMessageCreator {
	
	public AddInfantMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesComposerForAddInfant();
	}

	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {
		List<SSRDocsDTO> ssrDocsDTOs = TTYMessageCreatorUtil.composeSsrDocsInfoForAddedInfants(reservation, typeBRequestDTO);
		if (!ssrDocsDTOs.isEmpty() && ssrDTOs != null) {
			ssrDTOs.addAll(ssrDocsDTOs);
		}
		List<SSRDocoDTO> ssrDocoDTOs = TTYMessageCreatorUtil.composeSsrDocoInfoForAddedInfants(reservation, typeBRequestDTO);
		if (!ssrDocoDTOs.isEmpty() && ssrDTOs != null) {
			ssrDTOs.addAll(ssrDocoDTOs);
		}
		return ssrDTOs;
	}
	
	@Override
	public boolean composingInfantsSuccess(List<SSRDTO> ssrDTOs) {
		if (ssrDTOs == null || ssrDTOs.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
}
