/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure;

import java.util.HashMap;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;

/**
 * @author udithad
 *
 */
public class DestinationFareStructure {

	public DestinationFare createDestinationFare(String destinationAirportCode,
			String fareClassCode, HashMap<String, Integer> fareClassPaxCountMap) {
		DestinationFare destinationFare = new DestinationFare();
		destinationFare.setDestinationAirportCode(destinationAirportCode);
		destinationFare.setFareClass(fareClassCode);
		destinationFare.setNumberOfPreviousPassengers(getPassengerCountFor(
				fareClassCode, fareClassPaxCountMap));
		return destinationFare;
	}

	private Integer getPassengerCountFor(String fareClass,
			HashMap<String, Integer> fareClassPaxCountMap) {
		Integer passengerCount = 0;
		if (fareClassPaxCountMap != null) {
			if (fareClassPaxCountMap.containsKey(fareClass)) {
				if (fareClassPaxCountMap.get(fareClass) != null
						&& fareClassPaxCountMap.get(fareClass) > 0) {
					passengerCount = fareClassPaxCountMap.get(fareClass);
				}
			}
		}
		return passengerCount;
	}

	public void populateDestinationFareWithPassengerInformation(
			DestinationFare destinationFare,
			PassengerInformation passengerInformation) {
		String action = passengerInformation.getAdlAction();
		switch (AdlActions.valueOf(action)) {
		case A:
			destinationFare.populateAddedPassengersToPnl(passengerInformation);
			break;
		case D:
			destinationFare
					.populateDeletedPassengersToPnl(passengerInformation);
			break;
		case C:
			destinationFare
					.populateChangedPassengersToPnl(passengerInformation);
			break;
		default:
			break;
		}
	}

}
