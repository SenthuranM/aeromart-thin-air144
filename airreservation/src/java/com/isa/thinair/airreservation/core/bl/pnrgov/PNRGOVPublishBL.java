package com.isa.thinair.airreservation.core.bl.pnrgov;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVDTO.SsrTypes;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVDTO.TransmissionStatus;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPassengerDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPaymentDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVSegmentDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.PNRGOVException;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.ADD;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.DAT;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.EQN;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.FOP;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.Group1;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.Group2;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.Group3;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.Group4;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.Group5;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.MON;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.MSG;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.ORG;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.PNRGOVMessage;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.RCI;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.SRC;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.SSR;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.TIF;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.TKT;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.TVL;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.UNB;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.UNG;
import com.isa.thinair.airreservation.api.model.pnrgov.segment.UNH;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.util.PNRGOVUtil;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

public class PNRGOVPublishBL {

	private final Log log = LogFactory.getLog(getClass());

	private final static String SYNTAX_IDENTIFIER = "IATA";
	private final static String SYNTAX_VERSION = "1";
	private final static String RECIPIENT_IDENTIFICATION = "NZCS";
	private final static String FUNCIONAL_GROUP_IDENTIFICATION = "PNRGOV";
	private final static String CONTROLLING_AGENCY = "IA";
	private final static String MESSAGE_TYPE_VERSION = "D";
	private final static String MESSAGE_RELEASE_VERSION = "02B";
	private final static String MESSAGE_VERSION_NUMBER = "11";
	private final static String MESSAGE_RELEASE_NUMBER = "1";
	private final static String FIRST_AND_LAST_TRANSFER = "F";
	private final static String MESSAGE_FUNCTION = "22";
	private final static String BOOKING_TIMESTAMP_DATE_CODE = "710";
	private final static String RESERVATION_CONTACT_ADDRESS_PURPOSE_CODE = "700";
	private final static String STATUS_CODE_CONFIRMED = "HK";
	private final static String SSR_PASSPORT_INDICATOR = "P";
	private final static String SSR_VISA_INDICATOR = "V";
	private final static String SSR_PASSPORT_ELEMENT_SEPARATOR = "/";
	private final static String FILE_NAME_SEPARATOR = "_";

	private String dateOfPreparation;

	private String timeOfPreparation;

	private String carrierCode;

	private String operatingAirport;

	private FlightSegement selectedFlightSegment;

	private Flight selectedFlight;

	public void publishPassengerDataForFlight(int flightSegmentID, String countryCode, String airportCode, int timePeriod,
			String inboundOutbound, UserPrincipal userPrincipal) throws ModuleException {
		Date currentDate = new Date();
		List<PNRGOVDTO> pnrGovDtoList = new ArrayList<PNRGOVDTO>();
		try {

			this.dateOfPreparation = PNRGOVUtil.getDateOfPreparation(currentDate);
			this.timeOfPreparation = PNRGOVUtil.getTimeOfPreparation(currentDate);
			this.carrierCode = PNRGOVUtil.getCarrierCode();
			this.operatingAirport = PNRGOVUtil.getHubAirport();
			this.selectedFlightSegment = PNRGOVUtil.getFlightSegmentForFlightSegID(flightSegmentID);
			this.selectedFlight = PNRGOVUtil.getFlightForFlightID(this.selectedFlightSegment.getFlightId());
			pnrGovDtoList = PNRGOVUtil.composePNRGOVDataDTOsForFlightSegment(flightSegmentID, countryCode, airportCode,
					timePeriod, inboundOutbound);

			PNRGOVMessage pnrGovMessage = populatePNRGOVMessageWithData(pnrGovDtoList);
			if (pnrGovMessage != null && !pnrGovMessage.equals("")) {
				MessageSegment segment = pnrGovMessage.build();
				String message = segment.getEDIFmtData();
				log.debug(message);
				if (message != null && !message.equals("")) {
					log.info("###MQ PUSH STARTED###");
					pushPnrGovMessage(message, countryCode, timePeriod, inboundOutbound);
					log.info("###MQ PUSH COMPLETED###");
					logSegmentData(currentDate, flightSegmentID, countryCode, airportCode, timePeriod, inboundOutbound,
							userPrincipal, TransmissionStatus.SUBMITTED.toString(), null);
					logReservationData(pnrGovDtoList, currentDate, flightSegmentID, countryCode, airportCode, timePeriod,
							inboundOutbound, TransmissionStatus.SUBMITTED.toString(), null);
				} else {
					logSegmentData(currentDate, flightSegmentID, countryCode, airportCode, timePeriod, inboundOutbound,
							userPrincipal, TransmissionStatus.ERROR.toString(), "EMPTY MESSAGE TO PUSH");
				}
			} else {
				logSegmentData(currentDate, flightSegmentID, countryCode, airportCode, timePeriod, inboundOutbound,
						userPrincipal, TransmissionStatus.ERROR.toString(), "EMPTY MESSAGE");
			}

		} catch (PNRGOVException ex) {
			String errorMessage = "ERROR IN PNRGOV. " + ex.getMessage();
			logSegmentData(currentDate, flightSegmentID, countryCode, airportCode, timePeriod, inboundOutbound, userPrincipal,
					TransmissionStatus.ERROR.toString(), errorMessage);
			log.info(ex.getMessage());
			throw new ModuleException("Error Occured while generating PNRGOV. ");
		}

	}

	private PNRGOVMessage populatePNRGOVMessageWithData(List<PNRGOVDTO> pnrGovDtoList) {
		if (pnrGovDtoList != null && !pnrGovDtoList.isEmpty()) {
			PNRGOVMessage pnrGovMessage = new PNRGOVMessage();
			pnrGovMessage.setUnb(createAndPopulateUnb(pnrGovMessage));
			pnrGovMessage.setUng(createAndPopulateUng(pnrGovMessage));
			pnrGovMessage.setUnh(createAndPopulateUnh(pnrGovMessage));
			pnrGovMessage.setMsg(createAndPopulateMsg());
			pnrGovMessage.setOrg(createAndPopulateOrgLevelZero());
			pnrGovMessage.setTvl(createAndPopulateTvlLevelZero());
			pnrGovMessage.setEqn(createAndPopulateEQN(pnrGovDtoList));
			createAndPopulateGroupOneList(pnrGovDtoList, pnrGovMessage);
			return pnrGovMessage;
		} else {
			return null;
		}

	}

	private UNB createAndPopulateUnb(PNRGOVMessage pnrGovMessage) {
		UNB unb = new UNB();
		unb.setSyntaxIdentifier(SYNTAX_IDENTIFIER);
		unb.setSyntaxVersionNumber(SYNTAX_VERSION);
		unb.setCarrierCode(this.carrierCode);
		unb.setRecipientIdentification(RECIPIENT_IDENTIFICATION);
		unb.setDateOfPreparation(this.dateOfPreparation);
		unb.setTimeOfPreparation(this.timeOfPreparation);
		unb.setInterchangeControlReference(pnrGovMessage.getUnbRef());
		return unb;
	}

	private UNG createAndPopulateUng(PNRGOVMessage pnrGovMessage) {
		UNG ung = new UNG();
		ung.setFuntionalGroupIdentification(FUNCIONAL_GROUP_IDENTIFICATION);
		ung.setApplicationSenderIdentification(this.carrierCode);
		ung.setApplicationRecepientIdentification(RECIPIENT_IDENTIFICATION);
		ung.setDateOfPreparation(this.dateOfPreparation);
		ung.setTimeOfPreparation(this.timeOfPreparation);
		ung.setFuntionalGroupReferenceNumber(pnrGovMessage.getUngRef());
		ung.setControlingAgency(CONTROLLING_AGENCY);
		ung.setMessageTypeVersionNumber(MESSAGE_TYPE_VERSION);
		ung.setMessageTypeReleaseNumber(MESSAGE_RELEASE_VERSION);
		return ung;
	}

	private UNH createAndPopulateUnh(PNRGOVMessage pnrGovMessage) {
		UNH unh = new UNH();
		unh.setMessageReferenceNumber(pnrGovMessage.getUnhRef());
		unh.setMessageType(FUNCIONAL_GROUP_IDENTIFICATION);
		unh.setMessageVersionNumber(MESSAGE_VERSION_NUMBER);
		unh.setMessageReleaseNumber(MESSAGE_RELEASE_NUMBER);
		unh.setControllingAgency(CONTROLLING_AGENCY);
		unh.setSequenceOfTransfer("01");
		unh.setFirstAndLastTransfer(FIRST_AND_LAST_TRANSFER);

		return unh;
	}

	private MSG createAndPopulateMsg() {
		MSG msg = new MSG();
		msg.setMessageFunction(MESSAGE_FUNCTION);
		return msg;
	}

	private ORG createAndPopulateOrgLevelZero() {
		ORG org = new ORG();
		org.setCompanyIdentification(this.carrierCode);
		org.setSystemLocationIdentification(this.operatingAirport);

		return org;
	}

	private TVL createAndPopulateTvlLevelZero() {
		TVL tvl = new TVL();
		Date departureDate = this.selectedFlightSegment.getEstTimeDepatureLocal();
		Date arrivalDateTimeLocal = this.selectedFlightSegment.getEstTimeArrivalLocal();
		tvl.setDepartureDate(CalendarUtil.getDateInFormattedString("ddMMyy", departureDate));
		tvl.setDepartureTime(CalendarUtil.getDateInFormattedString("HHmm", departureDate));
		tvl.setArrivalDate(CalendarUtil.getDateInFormattedString("ddMMyy", arrivalDateTimeLocal));
		tvl.setArrivalTime(CalendarUtil.getDateInFormattedString("HHmm", arrivalDateTimeLocal));
		String[] airports = this.getOriginAndDestination(this.selectedFlightSegment.getSegmentCode());
		tvl.setDepartureAirport(airports[0]);
		tvl.setArrivalAirport(airports[airports.length - 1]);
		String flightNumber = "";
		if (selectedFlight.getFlightNumber() != null && !selectedFlight.getFlightNumber().equals("") && this.carrierCode != null
				&& !this.carrierCode.equals("")) {
			flightNumber = selectedFlight.getFlightNumber().substring(this.carrierCode.length());
		} else {
			flightNumber = selectedFlight.getFlightNumber();
		}
		tvl.setFlightNumber(flightNumber);
		tvl.setOperatingAirline(this.carrierCode);
		return tvl;
	}

	private EQN createAndPopulateEQN(List<PNRGOVDTO> pnrGovDtoList) {
		EQN eqn = new EQN();
		int reservationCount = pnrGovDtoList.size();
		eqn.setNumberOfPNRs(String.valueOf(reservationCount));
		return eqn;
	}

	private void createAndPopulateGroupOneList(List<PNRGOVDTO> pnrGovDtoList, PNRGOVMessage pnrGovMessage) {
		for (PNRGOVDTO pnrGovDto : pnrGovDtoList) {
			pnrGovMessage.addGroup1(createAndPopulateGroupOne(pnrGovDto));
		}

	}

	private Group1 createAndPopulateGroupOne(PNRGOVDTO pnrGovDto) {
		Group1 groupOne = new Group1();
		groupOne.setSrc(createAndPopulateSrc());
		groupOne.setRci(createAndPopulateRci(pnrGovDto));
		groupOne.setDat(createAndPopulateDat(pnrGovDto));
		groupOne.setOrg(createAndPopulateOrgAtLevelTwo(pnrGovDto));
		groupOne.addAdressElement(createAndPopulateAdd(pnrGovDto));
		createAndPopulateGroupTwoList(groupOne, pnrGovDto);
		createAndPopulateGroupFiveList(groupOne, pnrGovDto);
		return groupOne;
	}

	private SRC createAndPopulateSrc() {
		SRC src = new SRC();
		return src;
	}

	private RCI createAndPopulateRci(PNRGOVDTO pnrGovDto) {
		RCI rci = new RCI();
		Date bookingTimeStamp = pnrGovDto.getReservationInfo().getBookingTimeStamp();
		rci.setCompanyIdentification(this.carrierCode);
		rci.setPNR(pnrGovDto.getReservationInfo().getPnr());
		rci.setReservationCreatedDate(CalendarUtil.getDateInFormattedString("ddMMyy", bookingTimeStamp));
		rci.setReservationCreatedTime(CalendarUtil.getDateInFormattedString("HHmm", bookingTimeStamp));

		return rci;
	}

	private DAT createAndPopulateDat(PNRGOVDTO pnrGovDto) {
		DAT dat = new DAT();
		Date bookingTimeStamp = pnrGovDto.getReservationInfo().getBookingTimeStamp();
		dat.setDateCode(BOOKING_TIMESTAMP_DATE_CODE);
		dat.setDate(CalendarUtil.getDateInFormattedString("ddMMyy", bookingTimeStamp));
		dat.setTime(CalendarUtil.getDateInFormattedString("HHmm", bookingTimeStamp));

		return dat;
	}

	private ORG createAndPopulateOrgAtLevelTwo(PNRGOVDTO pnrGovDto) {
		ORG org = new ORG();
		org.setCompanyIdentification(this.carrierCode);
		org.setSystemLocationIdentification(this.operatingAirport);
		String ownerAgentCode = pnrGovDto.getReservationInfo().getOwnerAgentCode();
		if (ownerAgentCode != null && !ownerAgentCode.equals("")) {
			String airlineIdentifierCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
			if (airlineIdentifierCode != null && !airlineIdentifierCode.equals("")
					&& ownerAgentCode.contains(airlineIdentifierCode)) {
				ownerAgentCode = ownerAgentCode.replace(airlineIdentifierCode, "");
			}
			org.setReservationSystemAgentIdentificationCode(ownerAgentCode.toUpperCase());
		}

		return org;
	}

	private ADD createAndPopulateAdd(PNRGOVDTO pnrGovDto) {
		ADD add = new ADD();
		String phoneNumber = "";
		String address = "";
		ReservationContactInfo contactInfo = pnrGovDto.getContactInfo();
		add.setAddressPurposeCode(RESERVATION_CONTACT_ADDRESS_PURPOSE_CODE);
		if (contactInfo.getStreetAddress1() != null && !contactInfo.getStreetAddress1().equals("")) {
			address = contactInfo.getStreetAddress1();
		}
		if (contactInfo.getStreetAddress2() != null && !contactInfo.getStreetAddress2().equals("")) {
			address += contactInfo.getStreetAddress2();
		}
		if (address != null && !address.equals("")) {
			address = replaceNonAlphaNumericCharacters(address, " ");
			if (address.length() > EDI_ELEMENT.E3042.getMaxLength()) {
				address = address.substring(0, EDI_ELEMENT.E3042.getMaxLength());
			}
			add.setStreet(address.toUpperCase());
		}
		if (contactInfo.getCity() != null && !contactInfo.getCity().equals("")) {
			String cityName = contactInfo.getCity();
			cityName = replaceNonAlphaNumericCharacters(cityName, " ");
			if (cityName.length() > EDI_ELEMENT.E3164.getMaxLength()) {
				cityName = cityName.substring(0, EDI_ELEMENT.E3164.getMaxLength());
			}
			add.setCityName(cityName.toUpperCase());
		}
		if (contactInfo.getState() != null && !contactInfo.getState().equals("")) {
			String state = contactInfo.getState();
			state = replaceNonAlphaNumericCharacters(state, " ");
			if (state.length() > EDI_ELEMENT.E3229.getMaxLength()) {
				state = state.substring(0, EDI_ELEMENT.E3229.getMaxLength());
			}
			add.setState(state.toUpperCase());
		}

		if (contactInfo.getCountryCode() != null && !contactInfo.getCountryCode().equals("")) {
			add.setCountryCode(contactInfo.getCountryCode().toUpperCase());
		}
		
		if (contactInfo.getPhoneNo() != null && contactInfo.getPhoneNo() != "") {
			phoneNumber = contactInfo.getPhoneNo();
		} else if (contactInfo.getMobileNo() != null && contactInfo.getMobileNo() != "") {
			phoneNumber = contactInfo.getMobileNo();
		}
		if (phoneNumber != null && !phoneNumber.equals("")) {
			phoneNumber = phoneNumber.replaceAll("[^\\d]", "");
			add.setFreeText(phoneNumber);
		}
		return add;
	}

	private void createAndPopulateGroupTwoList(Group1 group1, PNRGOVDTO pnrGovDto) {
		List<PNRGOVPassengerDTO> reservationPaxCol = pnrGovDto.getPassengers();
		for (PNRGOVPassengerDTO passenger : reservationPaxCol) {
			group1.addGroup2(createAndPopulateGroupTwo(passenger));
		}
	}

	private Group2 createAndPopulateGroupTwo(PNRGOVPassengerDTO passenger) {
		Group2 group2 = new Group2();
		Set<PNRGOVPassengerDTO> infants = passenger.getIntants();
		group2.setTif(createAndPopulateTif(passenger));
		if (infants != null && !infants.isEmpty()) {
			group2.addSSR(createAndPopulateSSRInLevelTwo(passenger, SsrTypes.INFT.toString()));
		}
		if (passenger.getPassportNumber() != null && !passenger.getPassportNumber().equals("")) {
			group2.addSSR(createAndPopulateSSRInLevelTwo(passenger, SsrTypes.DOCS.toString()));
		}
		if (passenger.getVisaDocumentNumber() != null && !passenger.getVisaDocumentNumber().equals("")) {
			group2.addSSR(createAndPopulateSSRInLevelTwo(passenger, SsrTypes.DOCO.toString()));
		}
		createAndPopulateGroupThreeList(passenger, group2);
		return group2;
	}

	private TIF createAndPopulateTif(PNRGOVPassengerDTO passenger) {
		TIF tif = new TIF();
		Set<PNRGOVPassengerDTO> infants = passenger.getIntants();
		String surname = passenger.getAirImpStandardSurName();
		String firstName = passenger.getFirstName() + passenger.getTitle();
		if (surname != null && !surname.equals("") && surname.length()> EDI_ELEMENT.E9936.getMaxLength()) {
			surname = surname.substring(0, EDI_ELEMENT.E9936.getMaxLength());
		}
		if (firstName != null && !firstName.equals("") && firstName.length() > EDI_ELEMENT.E9942.getMaxLength()) {
			firstName = firstName.substring(0, EDI_ELEMENT.E9942.getMaxLength());
		}
		tif.setPassengerSurname(surname.toUpperCase());
		tif.setPassengerFirstNameAndTitle(firstName.toUpperCase());
		tif.setPassengerType(passenger.getPnrGovPassengerType());
		tif.setTravellerReferenceNumber(String.valueOf(passenger.getPaxReference()));
		if (infants != null && !infants.isEmpty()) {
			tif.setTravellerWithInfant("1"); // Check the code to set and set it properly
		}
		return tif;
	}

	private SSR createAndPopulateSSRInLevelTwo(PNRGOVPassengerDTO passenger, String ssrCode) {
		SSR ssr = new SSR();
		ssr.setSpecialRequirementType(ssrCode);
		ssr.setStatus(STATUS_CODE_CONFIRMED);
		if (ssrCode.equals(SsrTypes.INFT.toString())) {
			Set<PNRGOVPassengerDTO> infants = passenger.getIntants();
			ssr.setQuantity(String.valueOf(infants.size()));
			for (PNRGOVPassengerDTO infant : infants) {
				String name = infant.getAirImpStandardSurName() + "/" + infant.getFirstName();
				Date dob = infant.getDateOfBirth();
				String dateOfBirth = "";
				if (dob != null) {
					dateOfBirth = CalendarUtil.getDateInFormattedString("ddMMMyy", dob);
				}
				String freeText = name + " " + dateOfBirth;
				if (freeText != null && !freeText.equals("") && freeText.length() > EDI_ELEMENT.E4440.getMaxLength()) {
					freeText = freeText.substring(0, EDI_ELEMENT.E4440.getMaxLength());
				}
				ssr.addFreeText(freeText.toUpperCase());
			}
		} else if (ssrCode.equals(SsrTypes.DOCS.toString())) {
			String passportNo = passenger.getPassportNumber();
			Date passportExpiryDate = passenger.getPassportExpiry();
			String passportExpiry = "";
			String dateOfBirth = "";
			if (passportExpiryDate != null) {
				passportExpiry = CalendarUtil.getDateInFormattedString("ddMMMyy", passportExpiryDate).toUpperCase();
			}
			if (passenger.getDateOfBirth() != null) {
				dateOfBirth = CalendarUtil.getDateInFormattedString("ddMMMyy", passenger.getDateOfBirth()).toUpperCase();
			}
			String freeText = SSR_PASSPORT_INDICATOR + SSR_PASSPORT_ELEMENT_SEPARATOR + passenger.getPassportIssueCountry()
					+ SSR_PASSPORT_ELEMENT_SEPARATOR + passportNo + SSR_PASSPORT_ELEMENT_SEPARATOR
					+ BeanUtils.nullHandler(passenger.getPassengerNationality()) + SSR_PASSPORT_ELEMENT_SEPARATOR + dateOfBirth
					+ SSR_PASSPORT_ELEMENT_SEPARATOR + SSR_PASSPORT_ELEMENT_SEPARATOR + passportExpiry
					+ SSR_PASSPORT_ELEMENT_SEPARATOR + passenger.getAirImpStandardSurName() + SSR_PASSPORT_ELEMENT_SEPARATOR
					+ passenger.getFirstName() + passenger.getTitle();
			if (freeText != null && !freeText.equals("") && freeText.length() > EDI_ELEMENT.E4440.getMaxLength()) {
				freeText = freeText.substring(0, EDI_ELEMENT.E4440.getMaxLength());
			}
			ssr.addFreeText(freeText.toUpperCase());
		} else if (ssrCode.equals(SsrTypes.DOCO.toString())) {
			Date visaIssueDate = passenger.getVisaDateOfIssue();
			String visaDateOfIssue = "";
			
			if (visaIssueDate != null) {
				visaDateOfIssue = CalendarUtil.getDateInFormattedString("ddMMMyy", visaIssueDate).toUpperCase();
			}
			String freeText = BeanUtils.nullHandler(passenger.getPlaceOfBirth()) + SSR_PASSPORT_ELEMENT_SEPARATOR
					+ SSR_VISA_INDICATOR + SSR_PASSPORT_ELEMENT_SEPARATOR
					+ BeanUtils.nullHandler(passenger.getVisaDocumentNumber()) + SSR_PASSPORT_ELEMENT_SEPARATOR
					+ BeanUtils.nullHandler(passenger.getVisaPlaceOfIssue()) + SSR_PASSPORT_ELEMENT_SEPARATOR + visaDateOfIssue
					+ SSR_PASSPORT_ELEMENT_SEPARATOR + BeanUtils.nullHandler(passenger.getVisaApplicableCountry());
			if (freeText != null && !freeText.equals("") && freeText.length() > EDI_ELEMENT.E4440.getMaxLength()) {
				freeText = freeText.substring(0, EDI_ELEMENT.E4440.getMaxLength());
			}
			ssr.addFreeText(freeText.toUpperCase());
		}
		
		ssr.setCompanyIdentification(this.carrierCode);
		
		return ssr;
	}

	private void createAndPopulateGroupThreeList(PNRGOVPassengerDTO passenger, Group2 group2) {
		Collection<String> eTicketCollection = passenger.getPassengerEticket();
		int noOfBooklets = eTicketCollection.size();
		PNRGOVPaymentDTO passengerPayment = passenger.getPassengerPayment();
		for (String eTicket : eTicketCollection) {
			group2.addGroup3(createAndPopulateGroupThree(eTicket, noOfBooklets, passengerPayment));
		}
	}

	private Group3 createAndPopulateGroupThree(String eTicket, int noOfBooklets, PNRGOVPaymentDTO passengerPayment) {
		Group3 group3 = new Group3();
		group3.setTkt(createAndPopulateTKT(eTicket, noOfBooklets));
		if (passengerPayment != null) {
			group3.setMon(createAndPopulateMON(passengerPayment, noOfBooklets));
			group3.setDat(createAndPopulateDATAtLevelFour(passengerPayment));
			group3.addGroup4(createAndPopulateGroupFour(passengerPayment, noOfBooklets));
		}
		return group3;
	}

	private TKT createAndPopulateTKT(String eTicket, int noOfBooklets) {
		TKT tkt = new TKT();
		tkt.setTicketNumber(eTicket);
		tkt.setDocumentType("T");
		if (noOfBooklets != 0) {
			tkt.setNumberOfBooklets(String.valueOf(noOfBooklets));
		}
		return tkt;
	}

	private MON createAndPopulateMON(PNRGOVPaymentDTO passengerPayment, int noOfBooklets) {
		MON mon = new MON();
		BigDecimal ticketPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		ticketPaymentAmount = AccelAeroCalculator.divide(passengerPayment.getPaymentAmount(), noOfBooklets);
		BigDecimal truncatedAmount = ticketPaymentAmount.setScale(0, BigDecimal.ROUND_UP);
		mon.setTicketAmount(truncatedAmount.toString());
		mon.setMonetaryAmountType("T");
		mon.setCurrency(passengerPayment.getPaymentCurrency());

		return mon;
	}

	private DAT createAndPopulateDATAtLevelFour(PNRGOVPaymentDTO passengerPayment) {
		DAT dat = new DAT();
		dat.setDateCode("710");
		Date paymentTimeStamp = passengerPayment.getTnxDate();
		dat.setDate(CalendarUtil.getDateInFormattedString("ddMMyy", paymentTimeStamp));
		dat.setTime(CalendarUtil.getDateInFormattedString("HHmm", paymentTimeStamp));
		return dat;
	}

	private Group4 createAndPopulateGroupFour(PNRGOVPaymentDTO passengerPayment, int noOfBooklets) {
		Group4 group4 = new Group4();
		group4.setFop(createAndPopulateFOP(passengerPayment, noOfBooklets));
		return group4;
	}

	private FOP createAndPopulateFOP(PNRGOVPaymentDTO passengerPayment, int noOfBooklets) {
		FOP fop = new FOP();
		fop.setFormOfPaymentIdentification(passengerPayment.getFormOfPaymentIdentification());
		BigDecimal ticketPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		ticketPaymentAmount = AccelAeroCalculator.divide(passengerPayment.getPaymentAmount(), noOfBooklets);
		BigDecimal truncatedAmount = ticketPaymentAmount.setScale(0, BigDecimal.ROUND_UP);
		fop.setMonetaryAmount(truncatedAmount.toString());
		return fop;
	}

	private void createAndPopulateGroupFiveList(Group1 group1, PNRGOVDTO pnrGovDto) {
		Collection<PNRGOVSegmentDTO> reservationSegmentDTOSet = pnrGovDto.getSegments();
		for (PNRGOVSegmentDTO segmentDTO : reservationSegmentDTOSet) {
			group1.addGroup5(createAndPopulateGroupFive(segmentDTO));
		}
	}

	private Group5 createAndPopulateGroupFive(PNRGOVSegmentDTO segmentDTO) {
		Group5 group5 = new Group5();
		group5.setTvl(createAndPopulateTvlLevelTwo(segmentDTO));

		SSR ssr = createAndPopulateSSRGroup5(segmentDTO);
		if (ssr != null) {
			group5.addSsr(ssr);
		}
		return group5;
	}

	private TVL createAndPopulateTvlLevelTwo(PNRGOVSegmentDTO segmentDTO) {
		TVL tvl = new TVL();
		Date departureDate = segmentDTO.getDepartureDate();
		Date arrivalDate = segmentDTO.getArrivalDate();
		String segmentCode = segmentDTO.getSegmentCode();
		String[] airportCodes = getDepartureArrivalAirports(segmentCode);
		tvl.setDepartureDate(CalendarUtil.getDateInFormattedString("ddMMyy", departureDate));
		tvl.setDepartureTime(CalendarUtil.getDateInFormattedString("HHmm", departureDate));
		tvl.setArrivalDate(CalendarUtil.getDateInFormattedString("ddMMyy", arrivalDate));
		tvl.setArrivalTime(CalendarUtil.getDateInFormattedString("HHmm", arrivalDate));
		tvl.setDepartureAirport(airportCodes[0].toUpperCase());
		tvl.setArrivalAirport(airportCodes[airportCodes.length - 1].toUpperCase());
		tvl.setFlightNumber(segmentDTO.getFlightNo());
		tvl.setOperatingAirline(segmentDTO.getOperatingAirlineCode());

		return tvl;
	}

	private String[] getDepartureArrivalAirports(String segmentCode) {
		String[] airportCodes = segmentCode.split("/");
		return airportCodes;
	}

	private SSR createAndPopulateSSRGroup5(PNRGOVSegmentDTO segmentDTO) {
		Map<Integer, PaxSeatTO> bookedSeat = segmentDTO.getBookedSeat();
		if (bookedSeat != null && !bookedSeat.isEmpty()) {
			String[] airportCodes = getDepartureArrivalAirports(segmentDTO.getSegmentCode());
			SSR ssr = new SSR();
			ssr.setSpecialRequirementType(SsrTypes.SEAT.toString());
			ssr.setStatus(STATUS_CODE_CONFIRMED);
			ssr.setQuantity(String.valueOf(bookedSeat.size()));
			ssr.setCompanyIdentification(this.carrierCode);
			ssr.setBoardCity(airportCodes[0]);
			ssr.setOffCity(airportCodes[airportCodes.length - 1]);
			Set<Integer> keys = bookedSeat.keySet();
			for (Integer paxRef : keys) {
				PaxSeatTO paxSeat = bookedSeat.get(paxRef);
				String seatID = paxSeat.getSeatCode();
				String travellerReference = paxRef.toString();
				ssr.setSpecialRequirementData(seatID, null, travellerReference, null);
			}
			return ssr;
		}
		return null;
	}

	private void pushPnrGovMessage(String message, String countryCode, int timePeriod, String inboundOutbound) throws PNRGOVException {
		String fileName = this.getFileName(timePeriod, countryCode, inboundOutbound);
		MessagingServiceBD messagingBD = ReservationModuleUtils.getMessagingServiceBD();
		try {
			messagingBD.saveAndPushPNRGOVMessage(message, fileName, countryCode);
		} catch (ModuleException e) {
			throw new PNRGOVException(e.getMessage());
		}

	}

	private void logSegmentData(Date transmissionTimeStamp, int flightSegmentID, String countryCode, String airportCode,
			int timePeriod, String inboundOutbound, UserPrincipal userPrincipal, String transmissionStatus, String description) throws ModuleException {
		PNRGOVUtil.logPnrGovMessageData(flightSegmentID, transmissionTimeStamp, countryCode, airportCode, inboundOutbound,
				timePeriod, userPrincipal, transmissionStatus, description);

	}

	private void logReservationData(List<PNRGOVDTO> pnrGovDtoList, Date transmissionTimeStamp, int flightSegmentID,
			String countryCode, String airportCode, int timePeriod, String inboundOutbound, String transmissionStatus,
			String description) {
		if (pnrGovDtoList != null && !pnrGovDtoList.isEmpty()) {
			PNRGOVUtil.logPnrGovReservationDataLog(pnrGovDtoList, transmissionTimeStamp, flightSegmentID, countryCode,
					airportCode, timePeriod, inboundOutbound, transmissionStatus, description);
		}
	}

	private String replaceNonAlphaNumericCharacters(String inputString, String replaceWithString) {
		String output = "";
		output = inputString.replaceAll("[^a-zA-Z\\d\\s]", replaceWithString);
		return output;
	}
	
	private String getFileName(int timePeriod, String countryCode, String inboundOutbound) {
		String fileName = "";
		String[] airports = getOriginAndDestination(this.selectedFlightSegment.getSegmentCode());
		fileName = this.selectedFlight.getFlightNumber() + FILE_NAME_SEPARATOR + airports[0] + FILE_NAME_SEPARATOR
				+ airports[airports.length - 1] + FILE_NAME_SEPARATOR
				+ CalendarUtil.getDateInFormattedString("ddMMMyy", this.selectedFlightSegment.getEstTimeDepatureLocal())
				+ FILE_NAME_SEPARATOR + countryCode + FILE_NAME_SEPARATOR + inboundOutbound + FILE_NAME_SEPARATOR + timePeriod;
		return fileName;
	}
	
	private String[] getOriginAndDestination(String segmentCode) {
		String[] airports = StringUtils.split(segmentCode, "/");

		return airports;
	}

}
