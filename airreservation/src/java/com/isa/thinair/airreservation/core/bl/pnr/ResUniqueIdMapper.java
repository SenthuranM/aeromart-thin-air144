package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.HashSet;
import java.util.Set;

public class ResUniqueIdMapper {

	public static Set<Integer> getPnrSegIds(Set<String> resUniqueSegmentIds) {
		if (resUniqueSegmentIds != null) {
			Set<Integer> cnxPnrSegIds = new HashSet<Integer>();
			for (String uniqueId : resUniqueSegmentIds) {
				cnxPnrSegIds.add(Integer.parseInt(uniqueId));
			}
			return cnxPnrSegIds;
		}
		return null;
	}

	public static Set<String> getUniqueSegmentIds(Set<Integer> pnrSegIds) {
		if (pnrSegIds != null) {
			Set<String> cnxPnrSegIds = new HashSet<String>();
			for (Integer uniqueId : pnrSegIds) {
				cnxPnrSegIds.add(uniqueId.toString());
			}
			return cnxPnrSegIds;
		}
		return null;
	}

	public static Integer getPnrPaxId(String uniqueId) {
		if (uniqueId != null) {
			return new Integer(uniqueId);
		}
		return null;
	}
}
