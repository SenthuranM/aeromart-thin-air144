/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.transfer;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.TransferSeatBcCount;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.TransferSeatList;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.TransferSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PaxPnlAdlStates;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.airporttransfer.AirportTransferBL;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BaggageTransferChecker;
import com.isa.thinair.airreservation.core.bl.common.BundledServiceAdaptor;
import com.isa.thinair.airreservation.core.bl.common.MealAdapter;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.SSRAdaptor;
import com.isa.thinair.airreservation.core.bl.common.SeatMapAdapter;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.constants.MessageCodes;

/**
 * command for transfer reservation
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="transferReservationSegment"
 */
public class TransferReservationSegment extends DefaultBaseCommand {
	/** Holds the segment DAO */
	private final ReservationSegmentDAO segmentDAO;

	private final Log log = LogFactory.getLog(getClass());

	/** Holds the reservation DAO */
	private final ReservationDAO resDAO;

	/** Holds the alerting delegate */
	private final AlertingBD alertingBD;

	/** Holds the credentials information */
	private CredentialsDTO credentialsDTO;

	private boolean hasTransfered = false;

	/** Re protected flight segment IDs */
	private List<Integer> reprotectFlightSegIds;

	/** Flag to save data for IBE re protection */
	private String IBEVisibleFlag;

	/**
	 * constructor of the TransferSeats command
	 */
	public TransferReservationSegment() {

		segmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		resDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;

		alertingBD = ReservationModuleUtils.getAlertingBD();
	}

	/**
	 * execute method of the TransferSeats command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		Integer stTemplate = null;
		Integer tTemplate = null;
		Integer mealSTemplate = null;
		Integer mealTTemplate = null;
		boolean isPnrSegmentFound = false;
		int currentOndGroupId = 0;
		ReservationSegment newReservationSegment = new ReservationSegment();
		Boolean isNoReprotectNoReturn = new Boolean(false);
		Boolean isSegmentTransferredByNorecProcess = new Boolean(false);
		Map<Integer, Integer> pnrSegIdAndSequnceMap = new HashMap<Integer, Integer>();
		// getting command params
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Map pnrSegIdAndNewFlgSegId = (Map) this.getParameter(CommandParamNames.PNR_FLIGHT_SEG_MAP);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		Boolean isAlert = (Boolean) this.getParameter(CommandParamNames.IS_ALEART);
		credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		isNoReprotectNoReturn = (Boolean) this.getParameter(CommandParamNames.SEAT_NO_REPROTECT_No_RETURN);
		isSegmentTransferredByNorecProcess = (Boolean) this
				.getParameter(CommandParamNames.IS_SEGMENT_TRANSFERRED_BY_NOREC_PROCESS);
		this.reprotectFlightSegIds = (List<Integer>) this.getParameter(CommandParamNames.REPROTECT_FLT_SEG_IDS);
		this.IBEVisibleFlag = (String) this.getParameter(CommandParamNames.VISIBLE_IBE_ONLY);
		String salesChannelKey = (String) this.getParameter(CommandParamNames.SALES_CHANNEL_KEY);
		// keep a list to add all the pnrsegids that change..for alert purposes.
		List pnrSegIdList = new ArrayList();
		Set<ReservationSegment> reservationSegments = new HashSet<ReservationSegment>();
		Set<ReservationSegment> newReservationSegments = new HashSet<ReservationSegment>();

		// checking params
		this.checkParams(pnr, pnrSegIdAndNewFlgSegId, isAlert, version);

		// updateing the reservations flight segment id to the new target flight
		// segment id.
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		// Return the reservation
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		Set<String> originalCsOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);

		if (reservation == null) {
			throw new ModuleException("module.hibernate.objectnotfound");
		}

		// Checking to see if any concurrent update exist
		if (!version.equals(new Long(-1))) {
			ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());
		}

		int sourceFlightSegId = 0;
		Set setSegements = reservation.getSegments();
		if (setSegements == null || setSegements.size() == 0) {
			throw new ModuleException("airreservation.cmdTransferReservationSegment.segments");
		}

		reservationSegments.addAll(setSegements);

		int maxOndGroupId = ReservationCoreUtils.getMaxOndGroupId(reservationSegments) + 1;
		int returnOndGroupId = ReservationCoreUtils.getMaxReturnOndGroupId(reservationSegments) + 1;
		int maxSegmentSeq = getMaxSegmentSequence(reservationSegments);
		int maxJourneySeq = ReservationCoreUtils.getMaxJourneySequence(reservationSegments);

		Integer pnrSegId;
		Integer targetFlightSegmentId;
		AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation, false);
		List colOpenRTSegments = new ArrayList();

		for (Object element : reservation.getSegments()) {
			ReservationSegment reservationSegment = (ReservationSegment) element;
			// open return segments from list
			if (reservationSegment.getBookingType() != null
					&& reservationSegment.getBookingType().equalsIgnoreCase(BookingClass.BookingClassType.OPEN_RETURN)) {
				colOpenRTSegments.add(reservationSegment);
			}

		}

		List<TransferSegmentDTO> transferSegmentDTOs = new ArrayList<TransferSegmentDTO>();
		for (Iterator iter = pnrSegIdAndNewFlgSegId.keySet().iterator(); iter.hasNext();) {

			pnrSegId = (Integer) iter.next();
			targetFlightSegmentId = (Integer) pnrSegIdAndNewFlgSegId.get(pnrSegId);

			BundledFareDTO bundledFareDTO = BundledServiceAdaptor.refundBundledServiceChargeIfApplicable(adjustCreditBO,
					reservation, pnrSegId, credentialsDTO, chgTnxGen);

			// populate the transfer seat dto target.
			TransferSeatDTO transferSeatDTO = new TransferSeatDTO();
			transferSeatDTO.setTargetFlightSegId(targetFlightSegmentId.intValue());

			Integer sIdForTemplate = getSourceFlightSegmentForTemplate(reservation, pnrSegId);

			stTemplate = SeatMapAdapter.getReservationSeatTemplate(sIdForTemplate);

			// get the targetSegment seatTemplateID if exist.
			tTemplate = SeatMapAdapter.getReservationSeatTemplate(targetFlightSegmentId);

			Collection<PaxSeatTO> canceledSeats = new ArrayList<PaxSeatTO>();
			if (stTemplate != null && tTemplate != null && stTemplate.intValue() == tTemplate.intValue()
					&& ((AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption() && !BundledServiceAdaptor
							.isServiceIncludedInBundle(bundledFareDTO, EXTERNAL_CHARGES.SEAT_MAP.toString()))
							|| !AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption())) {
				// JIRA ID AARESAA-1352
				if (isNoReprotectNoReturn != null && isNoReprotectNoReturn) {
					canceledSeats = SeatMapAdapter.cancellSeatsWhenTransferringSegment(adjustCreditBO, reservation, pnrSegId,
							credentialsDTO, credentialsDTO.getTrackInfoDTO(), chgTnxGen);
				} else {
					canceledSeats = SeatMapAdapter.cancellUpdateSeatsWhenTransferringSegment(adjustCreditBO, reservation,
							pnrSegId, credentialsDTO, targetFlightSegmentId, credentialsDTO.getTrackInfoDTO(), chgTnxGen);
				}

			} else {
				// cancel the selected seats and credit the amounts
				canceledSeats = SeatMapAdapter.cancellSeatsWhenTransferringSegment(adjustCreditBO, reservation, pnrSegId,
						credentialsDTO, credentialsDTO.getTrackInfoDTO(), chgTnxGen);
			}

			// This is for transfer meal to new segment or credit if meals not linked with new segment
			// JIRA ID AARESAA-6465
			mealSTemplate = MealAdapter.getReservationMealTemplate(sIdForTemplate);

			// get the targetSegment mealTemplateID if exist.
			mealTTemplate = MealAdapter.getReservationMealTemplate(targetFlightSegmentId);

			Collection<PaxMealTO> canceledMeals = new ArrayList<PaxMealTO>();
			if (mealSTemplate != null && mealTTemplate != null && mealSTemplate.intValue() == mealTTemplate.intValue()
					&& ((AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption()
							&& !BundledServiceAdaptor.isServiceIncludedInBundle(bundledFareDTO, EXTERNAL_CHARGES.MEAL.toString()))
							|| !AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption())) {

				// TODO have a seperate flag for meal
				if (isNoReprotectNoReturn != null && isNoReprotectNoReturn) {
					canceledMeals = MealAdapter.cancellMealWhenTransferringSegment(adjustCreditBO, reservation, pnrSegId,
							credentialsDTO, chgTnxGen);
				} else {
					canceledMeals = MealAdapter.cancellUpdateMealsWhenTransferringSegment(adjustCreditBO, reservation, pnrSegId,
							credentialsDTO, targetFlightSegmentId, chgTnxGen);
				}

			} else {
				// cancel the selected meals and credit the amounts
				canceledMeals = MealAdapter.cancellMealWhenTransferringSegment(adjustCreditBO, reservation, pnrSegId,
						credentialsDTO, chgTnxGen);
			}
			// End meal transfer

			BaggageTransferChecker baggageTransferChecker = new BaggageTransferChecker(sIdForTemplate, pnrSegId, reservation);
			boolean isSourceAndTargetBaggageTemplateMatches = baggageTransferChecker
					.isSrcAndTargetBaggageTemplateMatches(targetFlightSegmentId);

			Collection<PaxBaggageTO> canceledBaggages = new ArrayList<PaxBaggageTO>();
			if ((isSourceAndTargetBaggageTemplateMatches || AppSysParamsUtil.isONDBaggaeEnabled()) && ((AppSysParamsUtil
					.isBundledServiceFeeRefundableForFlightDisruption()
					&& !BundledServiceAdaptor.isServiceIncludedInBundle(bundledFareDTO, EXTERNAL_CHARGES.BAGGAGE.toString()))
					|| !AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption())) {
				// TODO have a seperate flag for baggage if required
				if (isNoReprotectNoReturn != null && isNoReprotectNoReturn) {
					canceledBaggages = BaggageAdaptor.cancelBaggageWhenTransferringSegment(adjustCreditBO, reservation, pnrSegId,
							credentialsDTO, chgTnxGen);
				} else {
					canceledBaggages = BaggageAdaptor.cancelUpdateBaggagesWhenTransferringSegment(adjustCreditBO, reservation,
							pnrSegId, credentialsDTO, targetFlightSegmentId, chgTnxGen);
				}
			} else {
				// cancel the selected baggages and credit the amounts
				canceledBaggages = BaggageAdaptor.cancelBaggageWhenTransferringSegment(adjustCreditBO, reservation, pnrSegId,
						credentialsDTO, chgTnxGen);
			}
			// End baggage transfer

			// Load In-flight Services & Airport Service
			SSRAdaptor.populateSSRInfo(reservation, pnrSegId);

			// Re-protect In-flight services
			SSRAdaptor.reprotectInflightServices(adjustCreditBO, reservation, sIdForTemplate, targetFlightSegmentId, pnrSegId,
					credentialsDTO, chgTnxGen);

			// Airport Service Transfer
			Map<String, List<PaxSSRDTO>> paxSSRToRemove = SSRAdaptor.reprotectAirportServices(adjustCreditBO, reservation,
					pnrSegId, credentialsDTO, targetFlightSegmentId, isSegmentTransferredByNorecProcess, bundledFareDTO,
					chgTnxGen);

			HashMap bcMap = segmentDAO.getBCCodesWithAvailability(pnrSegId.intValue(), true);
			if (bcMap.size() == 0) {
				throw new ModuleException("airreservation.cmdTransferReservationSegment.bcMap");
			}

			String existingResStatus = reservation.getStatus();
			String existingSegStatus = ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED;

			// iterate through the reservation segments.
			Iterator<ReservationSegment> segmentsIterator = reservationSegments.iterator();
			while (segmentsIterator.hasNext()) {
				ReservationSegment oldReservationSegment = segmentsIterator.next();
				currentOndGroupId = oldReservationSegment.getOndGroupId().intValue();
				if (oldReservationSegment.getPnrSegId().intValue() == pnrSegId.intValue()) {
					isPnrSegmentFound = true;
					try {
						newReservationSegment = (ReservationSegment) oldReservationSegment.clone();
						// Cancel the existing segment
						newReservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
						newReservationSegment.setAlertFlag(0);
						newReservationSegment.setOndGroupId(new Integer(maxOndGroupId));
						newReservationSegment.setSegmentSeq(maxSegmentSeq);
						newReservationSegment.setReturnOndGroupId(returnOndGroupId);
						newReservationSegment.setJourneySequence(maxJourneySeq);

						// Set values for new segment
						existingSegStatus = oldReservationSegment.getStatus();
						sourceFlightSegId = oldReservationSegment.getFlightSegId().intValue();
						oldReservationSegment.setFlightSegId(new Integer(transferSeatDTO.getTargetFlightSegId()));

						/**
						 * When a transfer operation is carried out on a CANCELLED segment, transfered to segment is
						 * made CONFIRMED
						 */
						if (existingSegStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
							oldReservationSegment.setStatus((ReservationInternalConstants.ReservationPaxStatus.CONFIRMED));
							ReservationApiUtils.captureSegmentStatusChgInfo(oldReservationSegment, credentialsDTO);
						}

						pnrSegIdList.add(oldReservationSegment.getPnrSegId());
						hasTransfered = true;
						if (log.isDebugEnabled()) {
							log.debug("Transfering passenger segment : " + oldReservationSegment.getPnrSegId()
									+ " to a new flight segment :" + transferSeatDTO.getTargetFlightSegId());

						}
						if (isAlert.booleanValue()) {
							oldReservationSegment.setAlertFlag(1);
						} else {
							oldReservationSegment.setAlertFlag(0);
						}
						transferSeatDTO.setLogicalCCCode(oldReservationSegment.getLogicalCCCode());
						transferSeatDTO.setTargetCabinClassCode(oldReservationSegment.getCabinClassCode());
						reservation.addSegment(newReservationSegment);
						pnrSegIdAndSequnceMap.put(oldReservationSegment.getPnrSegId(), maxSegmentSeq);
						reCalculateOpenReturnConfirmBeforeDate(oldReservationSegment, colOpenRTSegments, targetFlightSegmentId);
						ADLNotifyer.recordAddedSegmentsForAdl(oldReservationSegment);

					} catch (Exception exception) {
						log.error("Transfer segment failed for pnr " + reservation.getPnr() + " pnr seg id " + pnrSegId);
						throw new ModuleException("airreservation.TransferReservationSegment.failed");
					}

					maxSegmentSeq = maxSegmentSeq + 1;
				}

			}

			if (!hasTransfered) {
				throw new ModuleException("airreservation.arg.pnrseg.notfound", "airreservation.desc");
			}

			/**
			 * When a transfer operation is carried out on a CANCELLED reservation, reservation is made CONFIRMED (and
			 * pax statuses are made CONFIRMED)
			 */
			if (existingResStatus.equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
				reservation.setStatus((ReservationInternalConstants.ReservationStatus.CONFIRMED));

				Iterator resPaxIt = reservation.getPassengers().iterator();
				while (resPaxIt.hasNext()) {
					ReservationPax reservationPax = (ReservationPax) resPaxIt.next();
					reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED);
				}
			}

			resDAO.saveReservation(reservation);

			if (log.isDebugEnabled()) {
				log.debug("updating reservation :" + reservation.getPnr());
			}

			// Add dummy records
			Collection<ReservationPaxFareSegment> colReservationReservationPaxFareSegment;
			Map<Integer, ReservationPaxFare> newReservationPaxFares = new HashMap<Integer, ReservationPaxFare>();

			for (Iterator<ReservationPax> paxIterator = reservation.getPassengers().iterator(); paxIterator.hasNext();) {
				ReservationPax reservationPax = paxIterator.next();

				for (Iterator<ReservationPaxFare> paxFareIterator = reservationPax.getPnrPaxFares().iterator(); paxFareIterator
						.hasNext();) {
					ReservationPaxFare oldReservationPaxFare = paxFareIterator.next();
					ReservationPaxFare newReservationPaxFare = (ReservationPaxFare) oldReservationPaxFare.clone();

					colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
					for (Iterator<ReservationPaxFareSegment> paxFareSegmentIterator = oldReservationPaxFare.getPaxFareSegments()
							.iterator(); paxFareSegmentIterator.hasNext();) {
						ReservationPaxFareSegment reservationPaxFareSegment = paxFareSegmentIterator.next();

						if (reservationPaxFareSegment.getSegment() != null
								&& pnrSegIdList.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
							ReservationPaxFareSegment newReservationPaxFareSegment = (ReservationPaxFareSegment) reservationPaxFareSegment
									.clone();
							newReservationPaxFareSegment.setSegment(newReservationSegment);
							colReservationReservationPaxFareSegment.add(newReservationPaxFareSegment);

							// Add the new segments for LCC update purpose
							newReservationSegments.add(newReservationSegment);
							// PNL ADL purposes , new pax pnl status should be N for new segments
							reservationPaxFareSegment.setPnlStat(PaxPnlAdlStates.PNL_RES_STAT);

							// Sometimes they transfer pfs processed segments, and future segments should not be in
							// NOSHO, NOREC etc...
							reservationPaxFareSegment.setStatus(ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED);
						}
					}

					if (colReservationReservationPaxFareSegment.size() > 0) {
						ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
								.getOndFareFromOndCharges(oldReservationPaxFare.getCharges());
						if (reservationPaxOndCharge != null) {
							ReservationPaxOndCharge newReservationPaxOndDRCharge = reservationPaxOndCharge
									.cloneForNew(credentialsDTO, false);
							newReservationPaxOndDRCharge.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
							newReservationPaxOndDRCharge.setZuluChargeDate(CalendarUtil.getCurrentSystemTimeInZulu());
							newReservationPaxOndDRCharge.setDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
							newReservationPaxOndDRCharge.setDiscountValuePercentage(new Integer(0));
							newReservationPaxFare.addCharge(newReservationPaxOndDRCharge);
							newReservationPaxFare.setTotalFare(new BigDecimal(0));
							newReservationPaxFare.setTotalTaxCharge(new BigDecimal(0));
							newReservationPaxFare.setTotalSurCharge(new BigDecimal(0));
							newReservationPaxFare.setTotalCancelCharge(new BigDecimal(0));
							newReservationPaxFare.setTotalModificationCharge(new BigDecimal(0));
							newReservationPaxFare.setTotalAdjustmentCharge(new BigDecimal(0));
							newReservationPaxFare.setTotalDiscount(new BigDecimal(0));

							for (ReservationPaxFareSegment reservationPaxFareSegment : colReservationReservationPaxFareSegment) {
								newReservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
							}
							newReservationPaxFares.put(reservationPax.getPnrPaxId(), newReservationPaxFare);
						}
					}
				}
			}

			for (Iterator<ReservationPax> paxIterator = reservation.getPassengers().iterator(); paxIterator.hasNext();) {
				ReservationPax reservationPax = paxIterator.next();
				for (Integer pnrPaxId : newReservationPaxFares.keySet()) {
					if (pnrPaxId.intValue() == reservationPax.getPnrPaxId().intValue()) {
						reservationPax.addPnrPaxFare(newReservationPaxFares.get(pnrPaxId));
					}
				}
			}

			resDAO.saveReservation(reservation);

			FlightSegmentDTO srcFligthSegmentDTO = segmentDAO.getFlightSegment(sourceFlightSegId);

			FlightSegmentDTO trgFligthSegmentDTO = segmentDAO.getFlightSegment(targetFlightSegmentId.intValue());

			if (srcFligthSegmentDTO == null) {
				throw new ModuleException("airreservation.arg.srcfltseg.notfound", "airreservation.desc");
			}

			if (trgFligthSegmentDTO == null) {
				throw new ModuleException("airreservation.arg.trgfltseg.notfound", "airreservation.desc");
			}

			// Check Pax DOB and passport expire Constraints
			validatePassengerDetails(trgFligthSegmentDTO, reservation.getPassengers());

			// segmentDAO.get
			Collection seatList = new ArrayList();
			TransferSeatList transferSeatList = new TransferSeatList();

			// get the onhold and confirmed infant count from reservation
			if (reservation.getTotalPaxInfantCount() > 0) {
				Set resPaxs = reservation.getPassengers();
				if (resPaxs != null) {
					Iterator iterResPaxs = resPaxs.iterator();
					while (iterResPaxs.hasNext()) {
						ReservationPax pax = (ReservationPax) iterResPaxs.next();
						if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
							if (pax.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
								transferSeatList.setConfirmedInfantCount(transferSeatList.getConfirmedInfantCount() + 1);
							} else {
								transferSeatList.setOnHoldInfantCount(transferSeatList.getOnHoldInfantCount() + 1);
							}
						}
					}
				}
			}

			transferSeatList.setTransferSeatCount(reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount());
			transferSeatList.setBcCount(bcMap);
			transferSeatList.setSourceSegmentCode(srcFligthSegmentDTO.getSegmentCode());
			transferSeatList.setSourceFlightId(srcFligthSegmentDTO.getFlightId().intValue());
			transferSeatList.setSourceSegmentId(sourceFlightSegId);
			seatList.add(transferSeatList);

			transferSeatDTO.setTargetSegCode(trgFligthSegmentDTO.getSegmentCode());
			transferSeatDTO.setTargetFlightId(trgFligthSegmentDTO.getFlightId());
			transferSeatDTO.setSourceFltSegSeatDists(seatList);

			if (log.isDebugEnabled()) {
				log.debug("==========Constructed TransferSeatDTO==========");
				log.debug("==========Target flight id" + transferSeatDTO.getTargetFlightId());
				log.debug("==========Logical cabin class code" + transferSeatDTO.getLogicalCCCode());
				log.debug("==========Targe flight seg id" + transferSeatDTO.getTargetFlightSegId());
				log.debug("==========Target seg code" + transferSeatDTO.getTargetSegCode());
				log.debug("==========source flight seg seat dists size " + transferSeatDTO.getSourceFltSegSeatDists().size());
				TransferSeatList seatList2 = (TransferSeatList) transferSeatDTO.getSourceFltSegSeatDists().iterator().next();
				if (seatList2 != null) {
					log.debug("==========source confirmed infant count" + seatList2.getConfirmedInfantCount());
					log.debug("==========source onhold ,,   ,, " + seatList2.getOnHoldInfantCount());
					log.debug("==========src flg id" + seatList2.getSourceFlightId());
					log.debug("==========src transfer seat count" + seatList2.getTransferSeatCount());
					log.debug("==========bc map size" + seatList2.getBcCount().size());
					String bookingCode = seatList2.getBcCount().keySet().iterator().next();
					if (bookingCode != null) {
						log.debug("==========booking code" + bookingCode);
					}
					TransferSeatBcCount bcCount = seatList2.getBcCount().get(bookingCode);
					if (bcCount != null) {
						log.debug("==========on hold count " + bcCount.getOnHoldCount());
						log.debug("==========sold count " + bcCount.getSoldCount());
					}
					log.debug("==========Finished Constructing TransferSeatDTO==========");
				}
			}

			// update inventory
			if (existingSegStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				// reprotect seats without releasing source flight seats
				ReservationModuleUtils.getFlightInventoryResBD().transferWithoutRelease(transferSeatDTO);
			} else {
				// reprotect seats, after releasing the source flight seats
				ReservationModuleUtils.getFlightInventoryResBD().transferSeats(transferSeatDTO);
			}
			// if isAlert set to false no need to send alerts
			if (isAlert.booleanValue()) {
				sendAlert(pnr, srcFligthSegmentDTO, trgFligthSegmentDTO, pnrSegIdList, salesChannelKey);
			}

			// save reservation modifications.
			saveReservationModification(srcFligthSegmentDTO, trgFligthSegmentDTO, pnr);

			if ((credentialsDTO == null || credentialsDTO.getSalesChannelCode() == null
					|| !credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_GDS))
					&& srcFligthSegmentDTO != null && trgFligthSegmentDTO != null) {
				TransferSegmentDTO transferSegmentDTO = new TransferSegmentDTO();
				transferSegmentDTO.setSourceFlightSegment(srcFligthSegmentDTO);
				transferSegmentDTO.setTargetFlightSegment(trgFligthSegmentDTO);
				transferSegmentDTO.setPnrSegId(pnrSegId);
				transferSegmentDTOs.add(transferSegmentDTO);
			}

			// Clear the pnr segId list
			pnrSegIdList = new ArrayList();
		}

		if (!transferSegmentDTOs.isEmpty()) {
			sendCSTypeBRequest(reservation, transferSegmentDTOs, originalCsOCCarrirs);
		}

		adjustCreditBO.callRevenueAccountingAfterReservationSave();

		Collection<String> newPnrSegIds = new ArrayList<String>();
		Collection<Integer> cancelledPnrSegIds = new ArrayList<Integer>();
		for (ReservationSegment createdReservationSegment : newReservationSegments) {
			newPnrSegIds.add(createdReservationSegment.getPnrSegId().toString());
			cancelledPnrSegIds.add(createdReservationSegment.getPnrSegId());
		}

		if (cancelledPnrSegIds.size() != 0) {
			ADLNotifyer.recordCanceledSegmentsForAdl(reservation, cancelledPnrSegIds, null);
		}

		if (pnrSegIdAndSequnceMap.keySet().size() > 0) {
			Collection<ReservationPaxSegAirportTransfer> userCancelledAptMap = ReservationApiUtils.cancelSegmentApts(reservation,
					pnrSegIdAndSequnceMap.keySet(), credentialsDTO, chgTnxGen);
			AirportTransferBL.sendAirportTransferCancelNotifications(reservation, pnrSegIdAndSequnceMap.keySet(), null,
					credentialsDTO, null);
		}

		// modify e ticket numbers
		ETicketBO.updateETickets(reservation, pnrSegIdAndSequnceMap);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.setResponseCode(ResponseCodes.TRANSFER_RESERVATION_SEGMENT_SUCCESSFULL);
		response.addResponceParam(ResponseCodes.TRANSFER_RESERVATION_SEGMENT_SUCCESSFULL, reservationSegments);
		response.setResponseCode(ResponseCodes.TRANSFERED_RESERVATION_SEGMENTS);
		response.addResponceParam(ResponseCodes.TRANSFERED_RESERVATION_SEGMENTS, newPnrSegIds);
		response.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));

		return response;
	}

	private void sendCSTypeBRequest(Reservation reservation, List<TransferSegmentDTO> transferSegmentDTOs,
			Set<String> originalCsOCCarrirs) throws ModuleException {

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
		Set<String> csOCCarriersFromTransferSeg = ReservationCoreUtils.getCSOCCarrierCodes(transferSegmentDTOs);

		if (csOCCarriersFromTransferSeg != null) {
			if (csOCCarrirs == null) {
				csOCCarrirs = new HashSet<String>();
			}
			csOCCarrirs.addAll(csOCCarriersFromTransferSeg);
		}

		if (csOCCarrirs != null) {
			if (originalCsOCCarrirs == null) {
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CREATE_ONHOLD.getCode());
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			} else {
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setTransferSegmentDTOs(transferSegmentDTOs);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.TRANSFER_SEGMENT.getCode());
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			}
		}

		// send the Eticket message for the new codeshare booking
		if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
			if (csOCCarrirs != null) {
				Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = getEticketInfo(reservation);
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.seteTicketInfo(eTicketInfo);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.TICKETING.getCode());
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			}
		}
	}

	private static String getLastAirportCode(String segmentCode) {
		segmentCode = BeanUtils.nullHandler(segmentCode);

		if (segmentCode.length() > 0) {
			String[] segments = segmentCode.split("/");
			return segments[segments.length - 1];
		} else {
			return "";
		}
	}

	private void reCalculateOpenReturnConfirmBeforeDate(ReservationSegment rSegment,
			Collection<ReservationSegment> colOpenRTSegments, int targetFlightSegId) throws ModuleException {
		Iterator<ReservationSegment> iterOpenRTSegments = colOpenRTSegments.iterator();
		while (iterOpenRTSegments.hasNext()) {
			ReservationSegment rOpenRTSegment = iterOpenRTSegments.next();

			if (rOpenRTSegment.getReturnGrpId().equals(rSegment.getReturnGrpId())) { // matching open return segment
				FlightSegmentDTO targetFlightSegDTO = segmentDAO.getFlightSegment(targetFlightSegId);
				if (rSegment.getFareTO(null) != null) {
					FareSummaryDTO fareSummaryDTO = new FareSummaryDTO(rSegment.getFareTO(null));
					String airportCode = getLastAirportCode(targetFlightSegDTO.getSegmentCode());
					if (fareSummaryDTO != null && targetFlightSegDTO.getArrivalDateTimeZulu() != null
							&& !BeanUtils.nullHandler(airportCode).isEmpty()) {
						long openRTConfPeriodInMins = fareSummaryDTO.getOpenRTConfPeriodInMins();
						long openRTConfPeriodInMonths = fareSummaryDTO.getOpenRTConfPeriodInMonths();
						long maximumStayOverInMins = fareSummaryDTO.getMaximumStayOverInMins();
						long maximumStayOverInMonths = fareSummaryDTO.getMaximumStayOverInMonths();

						// For an open return booking maximum stay over should not be zero
						Date travelExpiryDate = CalendarUtil.add(targetFlightSegDTO.getArrivalDateTimeZulu(), 0,
								(int) maximumStayOverInMonths, 0, 0, (int) maximumStayOverInMins, 0);
						Date confirmBeforeDate;

						// If the user hasn't specified confirm before. This will be same as travel expiry date
						if (openRTConfPeriodInMins == 0 && openRTConfPeriodInMonths == 0) {
							confirmBeforeDate = travelExpiryDate;
						} else {
							confirmBeforeDate = CalendarUtil.add(travelExpiryDate, 0, (int) -openRTConfPeriodInMonths, 0, 0,
									(int) -openRTConfPeriodInMins, 0);
						}
						rOpenRTSegment.setOpenReturnConfirmBeforeZulu(confirmBeforeDate);
					}
				}
			}
		} //
	}

	/**
	 * Check passenger age constraints before transfer segments
	 * 
	 * @param flightSegmentDTO
	 * @param colpaxs
	 * @throws ModuleException
	 */
	private void validatePassengerDetails(FlightSegmentDTO flightSegmentDTO, Collection<ReservationPax> colpaxs)
			throws ModuleException {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		int adultCutOffAge = globalConfig.getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears();
		// int childCutOffAge = globalConfig.getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears();
		int infantAgeLowerBoundaryInDays = globalConfig.getPaxTypeMap().get(PaxTypeTO.INFANT).getAgeLowerBoundaryInDays();

		if (flightSegmentDTO != null && colpaxs != null) {
			Date paxDOB = null;
			Date depDateTime = null;
			Date passportExpiry = null;
			for (ReservationPax pax : colpaxs) {
				paxDOB = pax.getDateOfBirth();
				depDateTime = flightSegmentDTO.getDepartureDateTime();
				ReservationPaxAdditionalInfo paxAdditionalInfo = pax.getPaxAdditionalInfo();
				// Validate Pax DOB
				// TODO Apply DOB validation according to application parameter
				if (paxDOB != null) {
					if (pax.getPaxType().equals(PaxTypeTO.ADULT)) {
						int maxAgeLimit = adultCutOffAge;

						if (!Util.compareAge(paxDOB, depDateTime, maxAgeLimit)) {
							throw new ModuleException(MessageCodes.PAX_DOB_RANGE_CONFLICT, "airreservation.desc");
						}

					} else if (pax.getPaxType().equals(PaxTypeTO.INFANT) || pax.getPaxType().equals(PaxTypeTO.CHILD)) {
						// Validate minimum age limit for child and infant
						if (!Util.checkInfantMinAgeInDays(paxDOB, depDateTime, infantAgeLowerBoundaryInDays)) {
							throw new ModuleException(MessageCodes.PAX_DOB_RANGE_CONFLICT, "airreservation.desc");
						}
					}
				}
				// Passport Expired validation
				if (paxAdditionalInfo != null) {
					passportExpiry = paxAdditionalInfo.getPassportExpiry();
					if (passportExpiry != null) {
						if (depDateTime.compareTo(passportExpiry) > 0) {
							throw new ModuleException(MessageCodes.PAX_PASSPORT_EXPIRED, "airreservation.desc");
						}
					}
				}
			}
		}
	}

	private int getMaxSegmentSequence(Set<ReservationSegment> reservationSegments) {
		int segmentSeq = 0;
		for (ReservationSegment reservationSegment : reservationSegments) {
			if (segmentSeq < reservationSegment.getSegmentSeq().intValue()) {
				segmentSeq = reservationSegment.getSegmentSeq().intValue();
			}
		}

		int maxSegmentSeq = segmentSeq + 1;
		return maxSegmentSeq;
	}

	/**
	 * Method will return source segment id
	 * 
	 * @param res
	 * @param pnrSegId
	 * @return
	 * @throws ModuleException
	 */
	private Integer getSourceFlightSegmentForTemplate(Reservation res, Integer pnrSegId) throws ModuleException {
		Set<ReservationSegment> resSegements = res.getSegments();
		// iterate through the reservation segments.
		Iterator<ReservationSegment> resSegmentIterator = resSegements.iterator();
		while (resSegmentIterator.hasNext()) {

			ReservationSegment rSegment = resSegmentIterator.next();
			if (rSegment.getPnrSegId().intValue() == pnrSegId.intValue()) {
				return rSegment.getFlightSegId();
			}
		}

		throw new ModuleException("airreservations.flight.segment.id.can.not.be.located");
	}

	/**
	 * will send an alert Method to create the reservation alert objects
	 * 
	 * @param sourceFlgSegment
	 * @param targetFlgSegment
	 * @param salesCahnnelKey TODO
	 * @param pnrSegId
	 * 
	 * @throws ModuleException
	 */

	private void sendAlert(String pnr, FlightSegmentDTO sourceFlgSegment, FlightSegmentDTO targetFlgSegment,
			List<Integer> pnrSegIdList, String salesCahnnelKey) throws ModuleException {
		List<Alert> colResAlert = new ArrayList<Alert>();
		String alertContent = constuctResModAlertContent(pnr, sourceFlgSegment, targetFlgSegment);

		Iterator<Integer> iterator = pnrSegIdList.iterator();
		while (iterator.hasNext()) {
			Integer pnrSegId = iterator.next();
			Alert alert = new Alert(alertContent, pnrSegId);
			if (this.reprotectFlightSegIds != null && this.reprotectFlightSegIds.size() > 0) {
				Set<Integer> reprotectFltSegIdsList = new HashSet<Integer>(this.reprotectFlightSegIds);
				alert.setFligthSegIds(reprotectFltSegIdsList);
				alert.setTransferSelectedFlights("Y");
			}
			if (this.IBEVisibleFlag != null) {
				alert.setVisibleIbeOnly(this.IBEVisibleFlag);
			}

			if (sourceFlgSegment.getDepartureDateTime() != null) {
				alert.setOriginalDepDate(new Date(sourceFlgSegment.getDepartureDateTime().getTime()));
			}
			
			if(salesCahnnelKey != null && salesCahnnelKey.equals(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY)){
				alert.setIbeActoined("Y");
			}
			colResAlert.add(alert);
		}

		alertingBD.addAlerts(colResAlert);

	}

	/**
	 * will write a reservation modification Method to construct the reservation modification object
	 * 
	 * @param sourceFlgSegment
	 * @param targetFlgSegment
	 * @param pnr
	 */
	private void saveReservationModification(FlightSegmentDTO sourceFlgSegment, FlightSegmentDTO targetFlgSegment, String pnr)
			throws ModuleException {

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.TRANSFERED_SEGMENT.getCode());

		// Construct the audit content
		this.constuctResModAuditContent(sourceFlgSegment, targetFlgSegment, reservationAudit);

		ReservationBO.recordModification(pnr, reservationAudit, null, credentialsDTO);
	}

	/** Method to construct the content **/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String constuctResModAlertContent(String pnr, FlightSegmentDTO sourceFlgSegment, FlightSegmentDTO targetFlgSegment)
			throws ModuleException {

		/** get the alert template **/
		AlertTemplate alertTemplate = alertingBD.getAlertTemplate(AlertTemplateEnum.TEMPLATE_TRANSFER_PAX);

		if (alertTemplate == null) {
			throw new ModuleException("airreservation.arg.alerttemplate.notfound", "airreservation.desc");
		}

		/** Holds the date,time format */
		SimpleDateFormat sdfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		String templateContent = alertTemplate.getAlertTemplateContent();
		HashMap alertParams = new HashMap();

		String completeAlertMessage = null;
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_FLIGHT_NO, sourceFlgSegment.getFlightNumber());
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_DEPARTURE,
				sdfmt.format(sourceFlgSegment.getDepartureDateTime()));
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_CODE, sourceFlgSegment.getSegmentCode());
		alertParams.put(AlertTemplateEnum.TemplateParams.NEW_FLIGHT_NO, targetFlgSegment.getFlightNumber());
		alertParams.put(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_DEPARTURE,
				sdfmt.format(targetFlgSegment.getDepartureDateTime()));
		alertParams.put(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_CODE, targetFlgSegment.getSegmentCode());
		alertParams.put(AlertTemplateEnum.TemplateParams.CANCELED_MEALS, "");
		alertParams.put(AlertTemplateEnum.TemplateParams.CANCELED_SEATS, "");
		alertParams.put(AlertTemplateEnum.TemplateParams.PNR, pnr);
		alertParams.put(AlertTemplateEnum.TemplateParams.IMMEDIATE_CONNECTING_FLIGHT_DETAIL, "");
		completeAlertMessage = Util.setStringToPlaceHolderIndex(templateContent, alertParams);
		return completeAlertMessage;
	}

	/**
	 * Construct the audit content
	 * 
	 * @param sourceFlgSegment
	 * @param targetFlgSegment
	 * @param reservationAudit
	 * @return
	 * @throws ModuleException
	 */
	private void constuctResModAuditContent(FlightSegmentDTO sourceFlgSegment, FlightSegmentDTO targetFlgSegment,
			ReservationAudit reservationAudit) throws ModuleException {

		/** Holds the date,time format */
		SimpleDateFormat sdfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.OLD_FLIGHT_SEGMENT,
				sourceFlgSegment.getSegmentCode());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.OLD_FLIGHT_NUMBER,
				sourceFlgSegment.getFlightNumber());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.OLD_FLIGHT_DATE,
				sdfmt.format(sourceFlgSegment.getDepartureDateTime()));
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.NEW_FLIGHT_SEGMENT,
				targetFlgSegment.getSegmentCode());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.NEW_FLIGHT_NUMBER,
				targetFlgSegment.getFlightNumber());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.NEW_FLIGHT_DATE,
				sdfmt.format(targetFlgSegment.getDepartureDateTime()));
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.CANCELED_MEALS, "");
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.CANCELED_SEATS, "");
	}

	/**
	 * Validate parameters
	 * 
	 * @param pnr
	 * @param pnrSegId
	 * @param targetFlightSegmentId
	 * @param isAlert
	 * @param version
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkParams(String pnr, Map pnrSegIdAndNewFlgSegId, Boolean isAlert, Long version) throws ModuleException {

		if (pnr == null || pnrSegIdAndNewFlgSegId == null || version == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (isAlert == null) {
			isAlert = new Boolean(true);
		}

	}

	private Map<Integer, Map<Integer, EticketDTO>> getEticketInfo(Reservation reservation) {
		Map<Integer, EticketDTO> segEticket = new HashMap<Integer, EticketDTO>();
		Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = new HashMap<Integer, Map<Integer, EticketDTO>>();

		for (ReservationPax pax : reservation.getPassengers()) {

			for (EticketTO eticket : pax.geteTickets()) {
				EticketDTO eticketDTO = new EticketDTO(eticket.getEticketNumber(), eticket.getCouponNo());
				for (ReservationSegment segment : reservation.getSegments()) {
					if (segment.getPnrSegId().equals(eticket.getPnrSegId())) {
						segEticket.put(segment.getSegmentSeq(), eticketDTO);
					}
				}
			}

			eTicketInfo.put(pax.getPaxSequence(), segEticket);
			segEticket = new HashMap<Integer, EticketDTO>();
		}

		return eTicketInfo;
	}

}
