package com.isa.thinair.airreservation.core.bl.payment;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.PaymentGatewayBO;
import com.isa.thinair.airreservation.core.util.PaymentUtil;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.platform.api.ServiceResponce;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @isa.module.command name="handlePayments"
 */
public class HandlePayments extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(HandlePayments.class);

	private CreditCardTransaction ccTransaction;
	private TempPaymentTnx tempPaymentTnx;
	private CredentialsDTO credentialsDTO;

	public HandlePayments() {

	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {

		handleOfflinePayments();

		ServiceResponce rs = new DefaultServiceResponse(true);
		return rs;
	}


	private void handleOfflinePayments() throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO;
		Reservation reservation;

		String newPnr = (String) this.getParameter(CommandParamNames.PNR);
		String oldPnr = (String) this.getParameter(CommandParamNames.OLD_PNR);
		credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		AppIndicatorEnum appIndicator = credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null ? credentialsDTO
				.getTrackInfoDTO().getAppIndicator() : null;

		String pnr = oldPnr != null ? oldPnr : newPnr;

		ccTransaction = ReservationModuleUtils.getPaymentBrokerBD().loadOfflineTransactionByReference(pnr);
		
		if(ccTransaction != null) {
			String ipgName = ccTransaction.getPaymentGatewayConfigurationName();
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgName);
			if (ReservationModuleUtils.getPaymentBrokerBD().isOfflinePaymentsCancellationAllowed(ipgIdentificationParamsDTO)) {
				tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(ccTransaction.getTemporyPaymentId());

				if (oldPnr != null) {
					cancelPreviousOfflinePaymentRequest(oldPnr, appIndicator);
					generatePayment(pnr, oldPnr);

					pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(newPnr);
					reservation = ReservationModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

					if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
						generatePayment(pnr, newPnr);
					}

				} else {
					cancelPreviousOfflinePaymentRequest(newPnr, appIndicator);

					pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(newPnr);
					reservation = ReservationModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

					if (!reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
						generatePayment(pnr, newPnr);
					}

				}
			}
			
		}

	}

	private void cancelPreviousOfflinePaymentRequest(String pnr, AppIndicatorEnum appIndicator) {

		if (isOfflinePaymentRequestCancelable()) {
			PaymentUtil.cancelPreviousOfflineTransaction(pnr, appIndicator, true);
		}
	}

	private boolean isOfflinePaymentRequestCancelable() {
		return true;
	}

	private void generatePayment(String sourcePnr, String pnr) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = ReservationModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
		BigDecimal balanceToPay = getTotalBalToPay(reservation);

		if (balanceToPay.compareTo(BigDecimal.ZERO) > 0) {

			String[] ipgInfo = ccTransaction.getPaymentGatewayConfigurationName().split("_");
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(new Integer(ipgInfo[0]), ipgInfo[1]);
			ipgIdentificationParamsDTO.setFQIPGConfigurationName(ccTransaction.getPaymentGatewayConfigurationName());

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(new Date());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(tempPaymentTnx.getPaymentCurrencyCode());

			PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(currExRate.getCurrency().getCurrencyCode(), currExRate.getMultiplyingExchangeRate(),
					currExRate.getCurrency().getBoundryValue(), currExRate.getCurrency().getBreakPoint());

			// TODO -- currency
			OfflinePaymentInfo offlinePaymentInfo = getOfflinePayment(balanceToPay, payCurrencyDTO, ipgIdentificationParamsDTO);

			Integer temporyPaymentId = ReservationModuleUtils.getReservationBD()
					.recordTemporyPaymentEntryForOffline(reservation.getContactInfo(), offlinePaymentInfo, credentialsDTO, false, true, pnr);

			PaymentGatewayBO.getRequestData(offlinePaymentInfo, temporyPaymentId, reservation.getContactInfo(), reservation.getZuluReleaseTime());
		}
	}


	public OfflinePaymentInfo getOfflinePayment(BigDecimal amount, PayCurrencyDTO payCurrencyDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		OfflinePaymentInfo offlinePaymentInfo = new OfflinePaymentInfo();
		BigDecimal totalPayAmountWithServiceCharges = amount;
		offlinePaymentInfo.setTotalAmount(amount);
		offlinePaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		offlinePaymentInfo.setPayCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		offlinePaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		payCurrencyDTO.setTotalPayCurrencyAmount(AccelAeroCalculator.multiplyDefaultScale(totalPayAmountWithServiceCharges,
				payCurrencyDTO.getPayCurrMultiplyingExchangeRate()));
		offlinePaymentInfo.setPayCurrencyDTO(payCurrencyDTO);

		return offlinePaymentInfo;
	}

	public BigDecimal getTotalBalToPay(Reservation res) {
		BigDecimal balToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationPax> reservationPaxes = res.getPassengers();
		for (ReservationPax traveler : reservationPaxes) {
			if (!traveler.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				balToPay = AccelAeroCalculator.add(balToPay, traveler.getTotalChargeAmount().subtract(traveler.getTotalPaidAmount()));
			}
		}
		return balToPay;
	}
}
