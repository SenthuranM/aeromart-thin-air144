package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.InsuranceSegExtChgDTO;
import com.isa.thinair.airreservation.api.dto.NameChangeExtChgDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.CHAEGE_CODES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;

public class ExternalChargeProcessor {
	private Date quotedDate;
	private Map<Integer, Collection<ChargeMetaTO>> paxAnciMetaCharges = new HashMap<Integer, Collection<ChargeMetaTO>>();

	public ExternalChargeProcessor(Date quotedDate, Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges) {
		this.quotedDate = quotedDate;

		if (reprotectedExternalCharges != null) {
			for (Integer pnrPaxId : reprotectedExternalCharges.keySet()) {
				Collection<ExternalChgDTO> extChgs = reprotectedExternalCharges.get(pnrPaxId);
				if (extChgs != null && extChgs.size() > 0) {
					for (ExternalChgDTO extChg : extChgs) {
						addAncillaryCharge(pnrPaxId, extChg.getAmount(), extChg);
					}
				}
			}
		}
	}

	private void addCharge(Integer pnrPaxId, ChargeMetaTO metaTO) {
		if (!paxAnciMetaCharges.containsKey(pnrPaxId)) {
			paxAnciMetaCharges.put(pnrPaxId, new ArrayList<ChargeMetaTO>());
		}
		paxAnciMetaCharges.get(pnrPaxId).add(metaTO);
	}

	private void addAncillaryCharge(Integer pnrPaxId, BigDecimal chargeAmount, ExternalChgDTO extChg) {
		if (chargeAmount.doubleValue() > 0) {
			ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(ReservationInternalConstants.ChargeGroup.SUR, chargeAmount,
					quotedDate, null, null);
			if (extChg instanceof SMExternalChgDTO) {
				metaTO.setChargeCode(CHAEGE_CODES.SM.toString());
			} else if (extChg instanceof MealExternalChgDTO) {
				metaTO.setChargeCode(CHAEGE_CODES.ML.toString());
			} else if (extChg instanceof BaggageExternalChgDTO) {
				metaTO.setChargeCode(CHAEGE_CODES.BG.toString());
			} else if (extChg instanceof InsuranceSegExtChgDTO) {
				Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
				metaTO.setChargeCode(externalChargesMap.get(EXTERNAL_CHARGES.INSURANCE.toString()));
			} else if (extChg instanceof SSRExternalChargeDTO) {
				if (extChg.getChargeCode().equals(CHAEGE_CODES.HALA.toString())) {
					metaTO.setChargeCode(CHAEGE_CODES.HALA.toString());
				} else if (extChg.getChargeCode().equals(CHAEGE_CODES.SSR.toString())) {
					metaTO.setChargeCode(CHAEGE_CODES.SSR.toString());
				} else if (extChg.getChargeCode().equals(CHAEGE_CODES.AT.toString())) {
					metaTO.setChargeCode(CHAEGE_CODES.AT.toString());
				}
			} else if (extChg instanceof FlexiExternalChgDTO) {
				metaTO.setChargeCode(CHAEGE_CODES.FL.toString());
			} else if (extChg instanceof ServiceTaxExtChgDTO) {
				if (extChg.getExternalChargesEnum() == EXTERNAL_CHARGES.SERVICE_TAX) {
					metaTO.setChargeCode(extChg.getChargeCode());
				} else {
					metaTO.setChargeCode(ChargeCodes.JN_ANCI_TAX);
				}
				metaTO.setChargeGroupCode(extChg.getChgGrpCode());
			} else if (extChg instanceof NameChangeExtChgDTO) {
				metaTO.setChargeCode(ChargeCodes.NAME_CHANGE_CHARGE);
			}else if(extChg.getExternalChargesEnum() == EXTERNAL_CHARGES.HANDLING_CHARGE){
				metaTO.setChargeCode(extChg.getChargeCode());
			}
			addCharge(pnrPaxId, metaTO);
		}
	}

	public Collection<ChargeMetaTO> getPaxExternalCharges(Integer pnrPaxId) {
		if (paxAnciMetaCharges.containsKey(pnrPaxId)) {
			return paxAnciMetaCharges.get(pnrPaxId);
		}
		return new ArrayList<ChargeMetaTO>();
	}
}
