package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageUtil;
import com.isa.thinair.airreservation.api.dto.baggage.PassengerBaggageAssembler;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author mano
 * 
 */
public class BaggageAdaptor {

	private static Log log = LogFactory.getLog(BaggageAdaptor.class);

	/**
	 * 
	 * @param res
	 * @param paxBaggageAssembler
	 * @param trackInfo
	 * @throws ModuleException
	 */
	public static void prepareBaggagesToModify(Reservation res, PassengerBaggageAssembler paxBaggageAssembler,
			TrackInfoDTO trackInfo) throws ModuleException {

		Map<Integer, Integer> flightBaggageIdpnrSegId = paxBaggageAssembler.getFlightBaggageIdPnrSegIdsMap();

		// get the pnr pax ids
		Collection<Integer> pnrPaxIds = res.getPnrPaxIds();
		BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
		// get the existing baggage list
		Collection<PassengerBaggage> passengerBaggage = baggageDAO.getFlightBaggagesForPnrPax(pnrPaxIds);

		Collection<PassengerBaggage> statusToCNx = new ArrayList<PassengerBaggage>();

		if (passengerBaggage != null && passengerBaggage.size() > 0
				&& paxBaggageAssembler.getPaxBaggageTORemove().values().size() > 0) {

			Map<String, PassengerBaggage> mapPaxExistingBaggages = new HashMap<String, PassengerBaggage>();

			for (PassengerBaggage baggage : passengerBaggage) {
				String uniqueId = BaggageUtil.makeUniqueIdForModifications(baggage.getPnrPaxId(), baggage.getpPFID(),
						baggage.getPnrSegmentId());
				mapPaxExistingBaggages.put(uniqueId, baggage);

			}
			for (PaxBaggageTO paxBaggageTO : paxBaggageAssembler.getPaxBaggageTORemove().values()) {
				String uniqueId = BaggageUtil.makeUniqueIdForModifications(paxBaggageTO.getPnrPaxId(),
						paxBaggageTO.getPnrPaxFareId(), paxBaggageTO.getPnrSegId());
				PassengerBaggage baggage = mapPaxExistingBaggages.get(uniqueId);
				if (baggage != null) {
					// marked for deletion
					paxBaggageAssembler.removePassengerFromFlightBaggage(baggage.getPnrPaxId(), baggage.getpPFID(),
							baggage.getPnrSegmentId(), baggage.getFlightBgChargeId(), baggage.getChargeAmount(),
							baggage.getBaggageId(), null, trackInfo);
					baggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.PAX_CNX);
					statusToCNx.add(baggage);
				} else {
					// remove from collection as the info already exists in db
					// keysToRemove.add(uniqueId);
					throw new ModuleException("airreservation.modify.baggage.invalidbaggagemodificationattempted");
				}
			}

			/*
			 * for (String uniqueId : keysToRemove) { mapPaxBaggages.remove(uniqueId);
			 * 
			 * }
			 */
			// update the stautus to CNX for ones which should get removed
			baggageDAO.saveOrUpdate(statusToCNx);
		}

		// add the rest
		Collection<PaxBaggageTO> newFlghtBaggages = paxBaggageAssembler.getPaxBaggageInfo().values();
		Iterator<PaxBaggageTO> newFlightBaggagesIter = newFlghtBaggages.iterator();
		while (newFlightBaggagesIter.hasNext()) {
			PaxBaggageTO paxBaggageTO = (PaxBaggageTO) newFlightBaggagesIter.next();
			Integer pnrSegId = flightBaggageIdpnrSegId.get(paxBaggageTO.getPnrPaxFareId());
			paxBaggageAssembler.addPassengertoNewFlightBaggage(paxBaggageTO.getPnrPaxId(), paxBaggageTO.getPnrPaxFareId(),
					pnrSegId, paxBaggageTO.getSelectedFlightBaggageId(), paxBaggageTO.getChgDTO().getAmount(),
					paxBaggageTO.getBaggageName(), paxBaggageTO.getBaggageId(), null, paxBaggageTO.getBaggageChrgId(),
					paxBaggageTO.getBaggageOndGroupId(), paxBaggageTO.getAutoCancellationId(), trackInfo);
		}
	}

	public static void addPassengerBaggageChargeAndAdjustCredit(AdjustCreditBO adjustCreditBO, Reservation reservation,
			Map<String, PaxBaggageTO> passengerBaggage, String userNotes, TrackInfoDTO trackInfoDTO,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// Adding charge entry
		Map<String, ReservationPaxOndCharge> paxIdChargeMap = addBaggageCharge(reservation, passengerBaggage, credentialsDTO,
				chgTnxGen);

		for (String uniqueId : paxIdChargeMap.keySet()) {
			Integer pnrPaxId = Integer.parseInt(BaggageUtil.getPnrPaxId(uniqueId));
			ReservationPaxOndCharge reservationPaxOndCharge = paxIdChargeMap.get(uniqueId);
			if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() != 0) {
				adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, reservationPaxOndCharge, credentialsDTO,
						reservation.isEnableTransactionGranularity());
			}
		}

	}

	@SuppressWarnings("unused")
	private static Map<String, ReservationPaxOndCharge> addBaggageCharge(Reservation reservation,
			Map<String, PaxBaggageTO> passengerBaggageMap, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		Map<String, ReservationPaxOndCharge> pnrPaxIdCharge = new HashMap<String, ReservationPaxOndCharge>();
		Integer pnrPaxId = null;
		Collection<ReservationPaxFareSegment> paxFareSegments;
		Iterator<ReservationPaxFareSegment> paxFareSegmentsIterater;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
			Collection<Integer> consumedPaxOndChgIds = new ArrayList<Integer>();
			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				paxFareSegments = reservationPaxFare.getPaxFareSegments();
				paxFareSegmentsIterater = paxFareSegments.iterator();
				while (paxFareSegmentsIterater.hasNext()) {
					ReservationPaxFareSegment resPaxFareSegment = (ReservationPaxFareSegment) paxFareSegmentsIterater.next();

					if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL
							.equals(resPaxFareSegment.getSegment().getStatus())) {
						continue;
					}

					String uniqueId = BaggageUtil.makeUniqueIdForModifications(reservationPax.getPnrPaxId(),
							reservationPaxFare.getPnrPaxFareId(), resPaxFareSegment.getPnrSegId());
					PaxBaggageTO paxBaggageTo = passengerBaggageMap.get(uniqueId);

					ExternalChgDTO chgDTO = null;
					if (paxBaggageTo != null) {
						chgDTO = paxBaggageTo.getChgDTO();
						paxBaggageTo.setPnrPaxId(reservationPax.getPnrPaxId());
					}

					if (chgDTO != null) {
						PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getAppliedDiscountInfoForRefund(reservationPaxFare,
								chgDTO, consumedPaxOndChgIds);
						ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(
								chgDTO.getAmount(), null, chgDTO.getChgRateId(), chgDTO.getChgGrpCode(), reservationPaxFare,
								credentialsDTO, false, paxDiscInfo, null, null,
								chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

						if (ReservationApiUtils.isInfantType(reservationPaxFare.getReservationPax())) {
							pnrPaxId = reservationPaxFare.getReservationPax().getParent().getPnrPaxId();
						} else {
							pnrPaxId = reservationPaxFare.getReservationPax().getPnrPaxId();
						}

						pnrPaxIdCharge.put(uniqueId, reservationPaxOndCharge);
					}
				}
			}
		}

		if (pnrPaxIdCharge.size() == 0) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		return pnrPaxIdCharge;
	}

	public static void modifyBaggages(Map<String, PaxBaggageTO> paxBaggageToAdd, Map<String, PaxBaggageTO> paxBaggageToRemove)
			throws ModuleException {
		// to add
		Collection<Integer> fltStIds = new ArrayList<Integer>();
		Collection<PaxBaggageTO> toAdd = paxBaggageToAdd.values();
		for (PaxBaggageTO paxBaggageTo : toAdd) {
			fltStIds.add(paxBaggageTo.getSelectedFlightBaggageId());
		}

		// to remove
		Collection<Integer> flightAMSeatIds = new ArrayList<Integer>();
		if (paxBaggageToRemove != null) {

			Collection<PaxBaggageTO> toRem = paxBaggageToRemove.values();
			for (PaxBaggageTO paxBaggageTo : toRem) {
				flightAMSeatIds.add(paxBaggageTo.getSelectedFlightBaggageId());
			}

		}
		if (!AppSysParamsUtil.isONDBaggaeEnabled()) {
			ReservationModuleUtils.getBaggageBD().modifyBaggage(fltStIds, flightAMSeatIds);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void recordPaxWiseAuditHistoryForModifyBaggages(String pnr, Map<String, PaxBaggageTO> mapAdded,
			Map<String, PaxBaggageTO> mapRemoved, CredentialsDTO credentialsDTO, String userNote, Collection[] colResInfo)
			throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.MODIFY_BAGGAGES.getCode());
		if (mapAdded != null && !mapAdded.isEmpty()) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.ADDED,
					ripPaxBaggageModifications(mapAdded, colResInfo));
		}
		if (mapRemoved != null && !mapRemoved.isEmpty()) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.REMOVED,
					ripPaxBaggageModifications(mapRemoved, colResInfo));
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}
		ReservationBO.recordModification(pnr, reservationAudit, userNote, credentialsDTO);
	}

	@SuppressWarnings("rawtypes")
	public static String recordPaxWiseAuditHistoryForModifyBaggages(String pnr, Collection<PaxBaggageTO> flightBaggageTos,
			CredentialsDTO credentialsDTO, String userNote, Collection[] colResPax) throws ModuleException {
		if (flightBaggageTos != null && !flightBaggageTos.isEmpty()) {
			Map<String, PaxBaggageTO> mapBaggageTo = createDummyBaggageMap(flightBaggageTos);
			return ripPaxBaggageModifications(mapBaggageTo, colResPax);
		}
		return null;
	}

	private static Map<String, PaxBaggageTO> createDummyBaggageMap(Collection<PaxBaggageTO> colBaggageTos) {
		Map<String, PaxBaggageTO> mapBaggageMap = new HashMap<String, PaxBaggageTO>();
		for (PaxBaggageTO paxBaggageTO : colBaggageTos) {
			String uuid = paxBaggageTO.getPnrPaxId().toString() + paxBaggageTO.getPnrSegId().toString();
			mapBaggageMap.put(uuid, paxBaggageTO);
		}
		return mapBaggageMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static String ripPaxBaggageModifications(Map<String, PaxBaggageTO> baggageMap, Collection[] colResInfo) {

		Map<Integer, Collection<PaxBaggageTO>> mapSortedBaggageInfo = segmentBaggageInfo(baggageMap.values());
		Iterator it = mapSortedBaggageInfo.entrySet().iterator();
		StringBuilder strBaggageData = new StringBuilder();
		Collection[] arrResInfo = colResInfo;
		Map<Integer, String> resSegmentInfo = compileSegmentInfo(arrResInfo[1], baggageMap.values());
		while (it.hasNext()) {
			Map.Entry map = (Map.Entry) it.next();
			Collection colPaxBaggage = (Collection) map.getValue();
			String strSegmentCode = "";
			if (resSegmentInfo != null) {
				int intPnrSeg = (Integer) map.getKey();
				strSegmentCode = resSegmentInfo.get(intPnrSeg);
			}
			if (colPaxBaggage != null && !colPaxBaggage.isEmpty()) {
				Set resPax = (Set) arrResInfo[0];
				Collection<PaxBaggageTO> colToAdd = setPaxInfo(colPaxBaggage, resPax);
				boolean processedAtleastOnce = false;
				strBaggageData.append(strSegmentCode + " =");
				for (PaxBaggageTO paxBaggageTo : colToAdd) {
					if (processedAtleastOnce) {
						strBaggageData.append(", ");
					}
					String baggetmplatestr = "";
					if (paxBaggageTo.getBaggageChrgId() != null) { // If ond baggage is enabled
						Integer ondBaggageChargeTemplateId = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO
								.getOndBaggageChargeTemplateId(paxBaggageTo.getBaggageChrgId());
						if (ondBaggageChargeTemplateId != null) {
							baggetmplatestr = "[Template ID =" + ondBaggageChargeTemplateId + "]";
						}
					}
					strBaggageData.append("[" + paxBaggageTo.getBaggageId() + "] " + baggetmplatestr
							+ paxBaggageTo.getBaggageName() + " - " + paxBaggageTo.getPaxName());
					processedAtleastOnce = true;
				}
				strBaggageData.append(".");
			}
		}
		return strBaggageData.toString();
	}

	private static Map<Integer, Collection<PaxBaggageTO>> segmentBaggageInfo(Collection<PaxBaggageTO> colBaggageInfo) {

		Map<Integer, Collection<PaxBaggageTO>> segBaggageInfo = new LinkedHashMap<Integer, Collection<PaxBaggageTO>>();
		for (PaxBaggageTO paxBaggageTO : colBaggageInfo) {

			Collection<PaxBaggageTO> colExtractedBaggageTos = (Collection<PaxBaggageTO>) segBaggageInfo
					.get(paxBaggageTO.getPnrSegId());
			if (colExtractedBaggageTos != null) {
				colExtractedBaggageTos.add(paxBaggageTO);
			} else {
				Collection<PaxBaggageTO> colSegBaggage = new ArrayList<PaxBaggageTO>();
				colSegBaggage.add(paxBaggageTO);
				segBaggageInfo.put(paxBaggageTO.getPnrSegId(), colSegBaggage);
			}
		}

		return segBaggageInfo;
	}

	private static Map<Integer, String> compileSegmentInfo(Collection<ReservationSegmentDTO> colResSeg,
			Collection<PaxBaggageTO> baggageMap) {
		if (colResSeg != null) {
			Map<Integer, String> mapSegmentInfo = new HashMap<Integer, String>();
			for (ReservationSegmentDTO reservationSegmentDTO : colResSeg) {
				mapSegmentInfo.put(reservationSegmentDTO.getPnrSegId(), reservationSegmentDTO.getSegmentCode());
			}
			return mapSegmentInfo;
		} else {
			Collection<Integer> colPnrSegIds = new ArrayList<Integer>();
			for (PaxBaggageTO paxBaggageTO : baggageMap) {
				colPnrSegIds.add(paxBaggageTO.getPnrSegId());
			}
			return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnrSegmentCodes(colPnrSegIds);

		}
	}

	private static Collection<PaxBaggageTO> setPaxInfo(Collection<PaxBaggageTO> colBaggageMap, Set<ReservationPax> resPaxs) {
		Collection<PaxBaggageTO> colBaggages = new ArrayList<PaxBaggageTO>();
		Collection<Integer> colPaxBaggageIds = new ArrayList<Integer>();
		Map<Integer, PaxBaggageTO> tempAddBaggageShelf = new HashMap<Integer, PaxBaggageTO>();

		for (PaxBaggageTO paxBaggageTO : colBaggageMap) {
			colPaxBaggageIds.add(paxBaggageTO.getBaggageId());
			tempAddBaggageShelf.put(paxBaggageTO.getPnrPaxId(), paxBaggageTO);
		}

		for (ReservationPax resPax : resPaxs) {

			PaxBaggageTO paxBaggageTO = (PaxBaggageTO) tempAddBaggageShelf.get(resPax.getPnrPaxId());
			if (paxBaggageTO != null) {
				paxBaggageTO.setPaxName(resPax.getFirstName() + " " + resPax.getLastName());
				Map<Integer, String> mapBaggageNames = fillBaggageNames(colPaxBaggageIds);
				if (mapBaggageNames != null) {
					paxBaggageTO.setBaggageName(mapBaggageNames.get(paxBaggageTO.getBaggageId()));
				}
				colBaggages.add(paxBaggageTO);
			}
		}

		return colBaggages;
	}

	private static Map<Integer, String> fillBaggageNames(Collection<Integer> colBaggageIds) {
		return getBaggageCodesForAudit(colBaggageIds);
	}

	private static Map<Integer, String> getBaggageCodesForAudit(Collection<Integer> flightBaggageIds) {
		if (flightBaggageIds != null && flightBaggageIds.size() > 0) {
			Map<Integer, String> baggageNames = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO
					.getBaggageDetailNames(flightBaggageIds);

			return baggageNames;

		}
		return null;
	}

	/**
	 * Populate Baggage information
	 * 
	 * @param res
	 * @param pnrSegId
	 * @throws ModuleException
	 */
	public static Integer getReservationBaggageTemplate(int flightSegId) throws ModuleException {

		Integer templateId = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.getTemplateForSegmentId(flightSegId);

		return templateId;

	}

	/**
	 * Cancels baggage when transfering segments
	 * 
	 * @param res
	 * @param chgTnxGen
	 * 
	 * @return : The canceled PaxBaggageTos.
	 * 
	 * @throws ModuleException
	 */
	public static Collection<PaxBaggageTO> cancelBaggageWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res,
			Integer pnrSegId, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// hold the existing res baggages
		Collection<PaxBaggageTO> paxBaggageDTOs = res.getBaggages();
		// hold the baggages to cancel
		Collection<PaxBaggageTO> affectivePaxBaggageTOs = new ArrayList<PaxBaggageTO>();

		// if no baggages do nothing
		if (paxBaggageDTOs == null || paxBaggageDTOs.size() == 0) {
			return affectivePaxBaggageTOs;
		}
		// populate the baggages to cancel
		for (PaxBaggageTO paxBaggageTo : paxBaggageDTOs) {
			if (paxBaggageTo.getPnrSegId().equals(pnrSegId)) {
				affectivePaxBaggageTOs.add(paxBaggageTo);
			}
		}

		// if no effective baggages to cancel not found, do nothing
		if (affectivePaxBaggageTOs == null || affectivePaxBaggageTOs.size() == 0) {
			return affectivePaxBaggageTOs;
		}

		// use the assembler to gather information on baggages to remove
		PassengerBaggageAssembler p = new PassengerBaggageAssembler(null, false);

		// add the removing baggages one by one to the assembler
		Collection<Integer> flightBaggageIds = new ArrayList<Integer>();

		for (PaxBaggageTO paxBaggageTo : affectivePaxBaggageTOs) {
			p.removePassengerFromFlightBaggage(paxBaggageTo.getPnrPaxId(), paxBaggageTo.getPnrPaxFareId(), pnrSegId,
					paxBaggageTo.getSelectedFlightBaggageId(), paxBaggageTo.getChgDTO().getAmount(), paxBaggageTo.getBaggageId(),
					null, credentialsDTO.getTrackInfoDTO());
			flightBaggageIds.add(paxBaggageTo.getSelectedFlightBaggageId());
		}
		// refund the charges
		addPassengerBaggageChargeAndAdjustCredit(adjustCreditBO, res, p.getPaxBaggageTORemove(), " ", null, credentialsDTO,
				chgTnxGen);

		// Save the reservation
		ReservationProxy.saveReservation(res);

		// release the baggages
		if (!AppSysParamsUtil.isONDBaggaeEnabled()) {
			ReservationModuleUtils.getBaggageBD().vacateBaggages(flightBaggageIds);
		}

		// update the passenger baggages table
		updateBaggagesToCancel(affectivePaxBaggageTOs);

		return affectivePaxBaggageTOs;
	}

	/**
	 * Update Baggage to cancel
	 * 
	 * @param flightAMBaggageIds
	 */
	private static void updateBaggagesToCancel(Collection<PaxBaggageTO> affectedBaggageTOs) throws ModuleException {

		Collection<Integer> pnrSegBaggageId = new ArrayList<Integer>();
		for (PaxBaggageTO paxBaggageTo : affectedBaggageTOs) {
			pnrSegBaggageId.add(paxBaggageTo.getPkey());
		}

		BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
		Collection<PassengerBaggage> passengerBaggages = baggageDAO.getPassengerBaggages(pnrSegBaggageId);

		if (passengerBaggages != null && passengerBaggages.size() > 0) {
			for (PassengerBaggage ps : passengerBaggages) {
				ps.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.PAX_CNX);
				ps.setTimestamp(new Date());
			}
			baggageDAO.saveOrUpdate(passengerBaggages);
		} else {
			throw new ModuleException("airreservation.seat.inconsistent");
		}
	}

	/**
	 * Method will cancell all reserved baggages from existing flight segment. if the existing passenger's baggage name
	 * available in the transferring segment then same baggage will be allocated for the pax, else baggage charge and
	 * baggage will be removed . : Using the one in the seatmap as it works fine
	 * 
	 * @param res
	 *            existing reservation
	 * @param flightSegId
	 *            target flight segment id.
	 * @param chgTnxGen
	 * 
	 * @return : The canceled PaxBaggageTos.
	 * 
	 * @throws ModuleException
	 */
	public static Collection<PaxBaggageTO> cancelUpdateBaggagesWhenTransferringSegment(AdjustCreditBO adjustCreditBO,
			Reservation res, Integer pnrSegId, CredentialsDTO credentialsDTO, Integer flightSegId,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		// hold the existing res baggages
		Collection<PaxBaggageTO> paxBaggageDTOs = res.getBaggages();
		// hold the baggage to cancel
		Collection<PaxBaggageTO> affectivePaxBaggageTOs = new ArrayList<PaxBaggageTO>();

		Collection<PaxBaggageTO> toReservedPaxBaggageTOs = new ArrayList<PaxBaggageTO>();

		Collection<PaxBaggageTO> toCancelPaxBaggageTOs = new ArrayList<PaxBaggageTO>();

		Integer newOndBaggageGroupId = null;

		/* Hold reservation baggages */
		Collection<Integer> baggageCollection = new ArrayList<Integer>();

		// if no baggages do nothing
		if (paxBaggageDTOs == null || paxBaggageDTOs.size() == 0) {
			return toCancelPaxBaggageTOs;
		}
		// populate the baggages to cancel
		for (PaxBaggageTO paxBaggageTO : paxBaggageDTOs) {
			if (paxBaggageTO.getPnrSegId().equals(pnrSegId)) {
				affectivePaxBaggageTOs.add(paxBaggageTO);
			}
		}

		// if no effective baggages to cancel not found, do nothing
		if (affectivePaxBaggageTOs == null || affectivePaxBaggageTOs.size() == 0) {
			return toCancelPaxBaggageTOs;
		} else {
			newOndBaggageGroupId = AirinventoryUtils.getNewBaggageONDGroupId(null);
		}

		// use the assembler to gather information on baggages to remove
		Collection<Integer> flightBaggageIds = new ArrayList<Integer>();

		for (PaxBaggageTO paxBaggageTo : affectivePaxBaggageTOs) {
			flightBaggageIds.add(paxBaggageTo.getSelectedFlightBaggageId());
		}

		for (Iterator<PaxBaggageTO> baggageCodeIterator = affectivePaxBaggageTOs.iterator(); baggageCodeIterator.hasNext();) {
			PaxBaggageTO paxBaggageDTO = (PaxBaggageTO) baggageCodeIterator.next();
			baggageCollection.add(paxBaggageDTO.getBaggageId());
		}

		// if no effective baggage code to cancel - since need to reprotect the corresponding baggage codes
		if (baggageCollection == null || baggageCollection.size() == 0) {
			return toCancelPaxBaggageTOs;
		}

		if (log.isDebugEnabled()) {
			log.debug("EXISTING Baggages : " + baggageCollection.toString());
		}

		int sBaggageId = 0;
		int tBaggageId = -1;
		PaxBaggageTO sourcePaxBaggageTO;
		PaxBaggageTO tempPaxBaggageTO;

		BaggageBusinessDelegate baggageBD = ReservationModuleUtils.getBaggageBD();
		Collection<FlightBaggageDTO> flightbaggageDTOs;

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {
			FlightSegement flightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(flightSegId);
			List<FlightSegmentTO> lstFlightSegmentTO = new ArrayList<FlightSegmentTO>();
			lstFlightSegmentTO.add(new FlightSegmentTO(flightSegment));

			// Ignore checking cutover time while processing pfs
			Map<Integer, List<FlightBaggageDTO>> baggageMap = ReservationModuleUtils.getBaggageBD()
					.getONDBaggages(lstFlightSegmentTO, false, false);
			flightbaggageDTOs = BeanUtils.getFirstElement(baggageMap.values());
		} else {
			flightbaggageDTOs = baggageBD.getAvailableBaggageDtos(flightSegId.intValue(), baggageCollection);
		}

		boolean flag = false;

		for (Iterator<PaxBaggageTO> iterator = affectivePaxBaggageTOs.iterator(); iterator.hasNext();) {

			flag = false;
			sourcePaxBaggageTO = (PaxBaggageTO) iterator.next();
			sBaggageId = sourcePaxBaggageTO.getBaggageId().intValue();

			for (Iterator<FlightBaggageDTO> baggageIterator = flightbaggageDTOs.iterator(); baggageIterator.hasNext();) {
				tBaggageId = -1;
				FlightBaggageDTO baggageDTO = (FlightBaggageDTO) baggageIterator.next();
				tBaggageId = baggageDTO.getBaggageId().intValue();
				if (tBaggageId == sBaggageId) {
					tempPaxBaggageTO = new PaxBaggageTO();
					tempPaxBaggageTO = sourcePaxBaggageTO;

					tempPaxBaggageTO.setBaggageChrgId(baggageDTO.getChargeId());
					tempPaxBaggageTO.setBaggageOndGroupId(String.valueOf(newOndBaggageGroupId));
					tempPaxBaggageTO.setSelectedFlightBaggageId(baggageDTO.getFlightBaggageId());
					tempPaxBaggageTO.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);

					toReservedPaxBaggageTOs.add(tempPaxBaggageTO);
					flag = true;
					break;

				}
			}

			if (!flag) {
				toCancelPaxBaggageTOs.add(sourcePaxBaggageTO);
			}

		}

		PassengerBaggageAssembler p = new PassengerBaggageAssembler(null, false);

		if (toCancelPaxBaggageTOs != null && toCancelPaxBaggageTOs.size() > 0) {
			// add the removing baggages one by one to the assembler
			for (PaxBaggageTO paxBaggageTo : toCancelPaxBaggageTOs) {
				p.removePassengerFromFlightBaggage(paxBaggageTo.getPnrPaxId(), paxBaggageTo.getPnrPaxFareId(),
						paxBaggageTo.getPnrSegId(), paxBaggageTo.getSelectedFlightBaggageId(),
						paxBaggageTo.getChgDTO().getAmount(), paxBaggageTo.getBaggageId(), null,
						credentialsDTO.getTrackInfoDTO());
			}
			// refund the charges
			addPassengerBaggageChargeAndAdjustCredit(adjustCreditBO, res, p.getPaxBaggageTORemove(), " ", null, credentialsDTO,
					chgTnxGen);
		}

		// Save the reservation
		ReservationProxy.saveReservation(res);

		// release the baggages
		if (!AppSysParamsUtil.isONDBaggaeEnabled()) {
			ReservationModuleUtils.getBaggageBD().vacateBaggages(flightBaggageIds);
		}

		// update the passenger baggages table
		updateBaggagesToCancel(affectivePaxBaggageTOs);

		if (toReservedPaxBaggageTOs != null && toReservedPaxBaggageTOs.size() > 0) {
			for (Iterator<PaxBaggageTO> iter = toReservedPaxBaggageTOs.iterator(); iter.hasNext();) {
				PaxBaggageTO paxBaggage = (PaxBaggageTO) iter.next();
				p.addPassengertoNewFlightBaggage(paxBaggage.getPnrPaxId(), paxBaggage.getPnrPaxFareId(), pnrSegId,
						paxBaggage.getSelectedFlightBaggageId(), paxBaggage.getChgDTO().getAmount(), paxBaggage.getBaggageName(),
						paxBaggage.getBaggageId(), null, null, String.valueOf(newOndBaggageGroupId),
						paxBaggage.getAutoCancellationId(), credentialsDTO.getTrackInfoDTO());

			}
			// reserve vacant baggages in target flight.
			modifyBaggages(p.getPaxBaggageTOAdd(), null);
			// save passenger- baggage info
			BaggageAdaptor.savePaxBaggageInfo(toReservedPaxBaggageTOs, credentialsDTO);
		}

		return toCancelPaxBaggageTOs;
	}

	/**
	 * Populate Baggage information
	 * 
	 * @param res
	 * @param pnrSegId
	 * @throws ModuleException
	 */
	public static void populateBaggageInformation(Reservation res, Integer pnrSegId) throws ModuleException {
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		pnrSegIds.add(pnrSegId);

		Collection<Integer> pnrPaxIds = res.getPnrPaxIds();
		Collection<PaxBaggageTO> paxBaggageTos = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.getReservationBaggages(pnrPaxIds,
				pnrSegIds, AppSysParamsUtil.isONDBaggaeEnabled(), null);
		res.setBaggages(paxBaggageTos);
	}

	/**
	 * Tracks baggage cancellations
	 * 
	 * @param pnrPaxFareIds
	 * @return
	 */
	public static Collection<Integer> trackBaggageCancellations(Collection<Integer> pnrPaxFareIds) {
		Collection<Integer> flightBaggages = null;

		if (pnrPaxFareIds != null && pnrPaxFareIds.size() > 0) {
			BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
			Collection<PassengerBaggage> passengerBaggages = baggageDAO.getFlightBaggagesForPnrPaxFare(pnrPaxFareIds);

			if (passengerBaggages != null && passengerBaggages.size() > 0) {
				flightBaggages = new HashSet<Integer>();
				for (PassengerBaggage pb : passengerBaggages) {
					if (pb.getFlightBgChargeId() != null) {
						flightBaggages.add(pb.getFlightBgChargeId());
					}
					pb.setStatus(AirinventoryCustomConstants.FlightMealStatuses.PAX_CNX);
					pb.setTimestamp(CalendarUtil.getCurrentSystemTimeInZulu());
				}
				baggageDAO.saveOrUpdate(passengerBaggages);
			}
		}

		return flightBaggages;
	}

	public static void savePaxBaggageInfo(Collection<PaxBaggageTO> paxBaggageTos, CredentialsDTO credentialsDTO) {

		Collection<PassengerBaggage> passengerBaggages = new ArrayList<PassengerBaggage>();
		Map<Integer, String> pnrSegIdWiseOndBaggageGrpId = new HashMap<Integer, String>();

		for (PaxBaggageTO paxBaggageTo : paxBaggageTos) {

			PassengerBaggage passengerBaggage = new PassengerBaggage();
			passengerBaggage.setPnrPaxId(paxBaggageTo.getPnrPaxId());
			passengerBaggage.setChargeAmount(paxBaggageTo.getChgDTO().getAmount());
			passengerBaggage.setStatus(paxBaggageTo.getStatus());
			passengerBaggage.setOriginSalesChannelCode(credentialsDTO.getSalesChannelCode());
			passengerBaggage.setOriginUserId(credentialsDTO.getUserId());
			passengerBaggage.setpPFID(paxBaggageTo.getPnrPaxFareId());
			passengerBaggage.setPnrSegmentId(paxBaggageTo.getPnrSegId());
			passengerBaggage.setTimestamp(new Date());
			passengerBaggage.setOriginCustomerId(credentialsDTO.getCustomerId());
			passengerBaggage.setBaggageId(paxBaggageTo.getBaggageId());
			passengerBaggage.setAutoCancellationId(paxBaggageTo.getAutoCancellationId());

			if (AppSysParamsUtil.isONDBaggaeEnabled()) {
				pnrSegIdWiseOndBaggageGrpId.put(paxBaggageTo.getPnrSegId(), paxBaggageTo.getBaggageOndGroupId());
				passengerBaggage.setOndChargeId(paxBaggageTo.getBaggageChrgId());
				passengerBaggage.setOndBaggage("Y");
			} else {
				passengerBaggage.setOndBaggage("N");
				passengerBaggage.setFlightBgChargeId(paxBaggageTo.getSelectedFlightBaggageId());
			}

			passengerBaggages.add(passengerBaggage);
		}

		if (passengerBaggages.size() > 0) {
			ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.saveOrUpdate(passengerBaggages);
		}

		if (pnrSegIdWiseOndBaggageGrpId.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.updateBaggageOndGrpIds(pnrSegIdWiseOndBaggageGrpId);
		}
	}

	/**
	 * Save Baggage information
	 * 
	 * @param paxIdsWithPayments
	 * @param flgtSegmentMap
	 * @param reservation
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static Collection[] saveBaggages(Map<Integer, IPayment> paxIdsWithPayments,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, Reservation reservation, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Collection<BaggageExternalChgDTO> allbaggageInfo = ReservationBO.getBaggageExternalChgDTOs(paxIdsWithPayments,
				flgtSegmentMap, reservation);

		Collection<PaxBaggageTO> colPaxBaggageTO = new ArrayList<PaxBaggageTO>();
		Collection<PassengerBaggage> passengerBaggages = new ArrayList<PassengerBaggage>();
		Collection<Integer> fltBaggageIds = new ArrayList<Integer>();
		Map<Integer, String> pnrSegIdWiseOndBaggageGrpId = new HashMap<Integer, String>();

		PassengerBaggage passengerBaggage;
		for (BaggageExternalChgDTO baggageExternalChgDTO : allbaggageInfo) {

			passengerBaggage = new PassengerBaggage();
			passengerBaggage.setPnrPaxId(baggageExternalChgDTO.getPnrPaxId());
			passengerBaggage.setChargeAmount(baggageExternalChgDTO.getAmount());

			if (AppSysParamsUtil.isONDBaggaeEnabled()) {
				if ((baggageExternalChgDTO.getOndBaggageChargeGroupId() != null
						&& baggageExternalChgDTO.getOndBaggageGroupId() != null)) {
					int baggageId = getONDBaggageInfo(baggageExternalChgDTO.getOndBaggageChargeGroupId(),
							baggageExternalChgDTO.getOndBaggageGroupId());
					baggageExternalChgDTO.setBaggageId(baggageId);

					passengerBaggage.setFlightBgChargeId(null);
					passengerBaggage.setOndChargeId(new Integer(baggageExternalChgDTO.getOndBaggageChargeGroupId()));
					passengerBaggage.setOndBaggage("Y");
					pnrSegIdWiseOndBaggageGrpId.put(baggageExternalChgDTO.getPnrSegId(),
							baggageExternalChgDTO.getOndBaggageGroupId());
				}

			} else {
				FlightBaggageDTO flightBaggageDTO = getFlightBaggageDTO(baggageExternalChgDTO.getBaggageCode(),
						baggageExternalChgDTO.getFlightSegId());
				baggageExternalChgDTO.setFlightBaggageId(flightBaggageDTO.getFlightBaggageId());
				baggageExternalChgDTO.setBaggageId(flightBaggageDTO.getBaggageId());

				passengerBaggage.setFlightBgChargeId(baggageExternalChgDTO.getFlightBaggageId());
				passengerBaggage.setOndChargeId(null);
				passengerBaggage.setOndBaggage("N");

				fltBaggageIds.add(baggageExternalChgDTO.getFlightBaggageId());
			}

			passengerBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);
			passengerBaggage.setOriginSalesChannelCode(credentialsDTO.getSalesChannelCode());
			passengerBaggage.setOriginUserId(credentialsDTO.getUserId());
			passengerBaggage.setPnrSegmentId(baggageExternalChgDTO.getPnrSegId());
			passengerBaggage.setTimestamp(CalendarUtil.getCurrentSystemTimeInZulu());
			passengerBaggage.setOriginCustomerId(credentialsDTO.getCustomerId());
			passengerBaggage.setpPFID(baggageExternalChgDTO.getPnrPaxFareId());
			passengerBaggage.setBaggageId(baggageExternalChgDTO.getBaggageId());

			passengerBaggages.add(passengerBaggage);

			PaxBaggageTO paxBaggageTO = new PaxBaggageTO();
			paxBaggageTO.setPnrPaxId(baggageExternalChgDTO.getPnrPaxId());
			paxBaggageTO.setBaggageId(baggageExternalChgDTO.getBaggageId());
			paxBaggageTO.setSelectedFlightBaggageId(baggageExternalChgDTO.getFlightBaggageId());
			paxBaggageTO.setPnrSegId(baggageExternalChgDTO.getPnrSegId());
			try {
				paxBaggageTO.setBaggageChrgId(Integer.parseInt(baggageExternalChgDTO.getOndBaggageChargeGroupId()));
			} catch (NumberFormatException nfs) {
				log.info("Invalid ond_baggage_charge_id");
			}
			colPaxBaggageTO.add(paxBaggageTO);
		} // end for

		if (passengerBaggages.size() > 0) {
			ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.saveOrUpdate(passengerBaggages);
		}

		if (pnrSegIdWiseOndBaggageGrpId.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.updateBaggageOndGrpIds(pnrSegIdWiseOndBaggageGrpId);
		}

		return new Collection[] { colPaxBaggageTO, fltBaggageIds };
	}

	private static FlightBaggageDTO getFlightBaggageDTO(String baggageCode, Integer flightSegId) throws ModuleException {
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		flightSegIdWiseCos.put(flightSegId, null);

		Map<Integer, List<FlightBaggageDTO>> baggageMap = ReservationModuleUtils.getBaggageBD().getBaggages(flightSegIdWiseCos,
				false, false);
		List<FlightBaggageDTO> flightBaggageDTOs = baggageMap.get(flightSegId);
		for (FlightBaggageDTO baggageDTO : flightBaggageDTOs) {
			if (baggageDTO.getBaggageName().equals(baggageCode)) {
				return baggageDTO;
			}
		}

		throw new ModuleException("airreservations.invalid.baggage.code");
	}

	private static int getONDBaggageInfo(String baggageChargeGroupId, String baggageGroupId) throws ModuleException {
		baggageChargeGroupId = BeanUtils.nullHandler(baggageChargeGroupId);

		if (baggageChargeGroupId.length() == 0) {
			if (log.isDebugEnabled()) {
				log.debug(" baggageChargeGroupId : [" + baggageChargeGroupId + "] ");
			}

			throw new ModuleException("airreservations.ond.baggages.invalid.params");
		}

		Integer baggageId = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.getONDBaggageId(new Long(baggageChargeGroupId));

		if (baggageId == null) {
			if (log.isDebugEnabled()) {
				log.debug(" baggageId : [" + baggageId + "] ");
			}
			throw new ModuleException("airreservations.ond.baggages.invalid.params");
		}

		return baggageId;
	}

	/**
	 * If the existing passenger's baggage name available in the transferring segment then same baggage will be
	 * allocated for the pax.
	 * 
	 * @param res
	 *            existing reservation
	 * @param flightSegId
	 *            target flight segment id.
	 * 
	 * 
	 * @throws ModuleException
	 */
	public static void updateBaggagesWhenTransferringSegment(AdjustCreditBO adjustCreditBO, Reservation res, Integer pnrSegId,
			CredentialsDTO credentialsDTO, Integer flightSegId) throws ModuleException {
		// hold the existing res baggages
		Collection<PaxBaggageTO> paxBaggageDTOs = res.getBaggages();
		// hold the baggage to cancel
		Collection<PaxBaggageTO> affectivePaxBaggageTOs = new ArrayList<PaxBaggageTO>();

		Collection<PaxBaggageTO> toReservedPaxBaggageTOs = new ArrayList<PaxBaggageTO>();

		/* Hold reservation baggages */
		Collection<Integer> baggageCollection = new ArrayList<Integer>();

		// populate the baggages to cancel
		for (PaxBaggageTO paxBaggageTO : paxBaggageDTOs) {
			if (paxBaggageTO.getPnrSegId().equals(pnrSegId)) {
				affectivePaxBaggageTOs.add(paxBaggageTO);
			}
		}

		for (Iterator<PaxBaggageTO> baggageCodeIterator = affectivePaxBaggageTOs.iterator(); baggageCodeIterator.hasNext();) {
			PaxBaggageTO paxBaggageDTO = (PaxBaggageTO) baggageCodeIterator.next();
			baggageCollection.add(paxBaggageDTO.getBaggageId());
		}

		if (log.isDebugEnabled()) {
			log.debug("EXISTING Baggages : " + baggageCollection.toString());
		}

		int sBaggageId = 0;
		int tBaggageId = -1;
		PaxBaggageTO sourcePaxBaggageTO;
		PaxBaggageTO tempPaxBaggageTO;

		BaggageBusinessDelegate baggageBD = ReservationModuleUtils.getBaggageBD();
		Collection<FlightBaggageDTO> flightbaggageDTOs = baggageBD.getAvailableBaggageDtos(flightSegId.intValue(),
				baggageCollection);

		for (Iterator<PaxBaggageTO> iterator = affectivePaxBaggageTOs.iterator(); iterator.hasNext();) {

			sourcePaxBaggageTO = (PaxBaggageTO) iterator.next();
			sBaggageId = sourcePaxBaggageTO.getBaggageId().intValue();

			for (Iterator<FlightBaggageDTO> baggageIterator = flightbaggageDTOs.iterator(); baggageIterator.hasNext();) {
				tBaggageId = -1;
				FlightBaggageDTO baggageDTO = (FlightBaggageDTO) baggageIterator.next();
				tBaggageId = baggageDTO.getBaggageId().intValue();
				if (tBaggageId == sBaggageId) {
					tempPaxBaggageTO = new PaxBaggageTO();
					tempPaxBaggageTO = sourcePaxBaggageTO;
					tempPaxBaggageTO.setSelectedFlightBaggageId(baggageDTO.getFlightBaggageId());
					tempPaxBaggageTO.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);
					toReservedPaxBaggageTOs.add(tempPaxBaggageTO);
					break;

				}
			}

		}

		PassengerBaggageAssembler p = new PassengerBaggageAssembler(null, false);

		// Save the reservation
		ReservationProxy.saveReservation(res);

		if (toReservedPaxBaggageTOs != null && toReservedPaxBaggageTOs.size() > 0) {
			for (Iterator<PaxBaggageTO> iter = toReservedPaxBaggageTOs.iterator(); iter.hasNext();) {
				PaxBaggageTO paxBaggage = (PaxBaggageTO) iter.next();
				p.addPassengertoNewFlightBaggage(paxBaggage.getPnrPaxId(), paxBaggage.getPnrPaxFareId(), pnrSegId,
						paxBaggage.getSelectedFlightBaggageId(), paxBaggage.getChgDTO().getAmount(), paxBaggage.getBaggageName(),
						paxBaggage.getBaggageId(), null, null, paxBaggage.getBaggageOndGroupId(),
						paxBaggage.getAutoCancellationId(), credentialsDTO.getTrackInfoDTO());

			}
			// reserve vacant baggages in target flight.
			modifyBaggages(p.getPaxBaggageTOAdd(), null);
			// save passenger- baggage info
			BaggageAdaptor.savePaxBaggageInfo(toReservedPaxBaggageTOs, credentialsDTO);
		}

	}

}
