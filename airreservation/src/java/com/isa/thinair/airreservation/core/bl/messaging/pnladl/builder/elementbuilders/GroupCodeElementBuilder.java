/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.GroupCodeElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class GroupCodeElementBuilder extends BaseElementBuilder {
	
	Log logger = LogFactory.getLog(GroupCodeElementBuilder.class);

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement = "";

	private String lastGroupCode;
	private String[] groupCodes;
	private String pnr;
	private String availableGroupCode;

	private UtilizedPassengerContext uPContext;

	private List<PassengerInformation> utilizedPassengers;

	private BaseRuleExecutor<RulesDataContext> groupCodeElementRuleExecutor = new GroupCodeElementRuleExecutor();
	
	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;

		initContextData(context);
		groupCodeElementTemplate();
		if (currentElement != null && !currentElement.isEmpty()) {
			response = validateSubElement(currentElement);
			ammendMessageDataAccordingTo(response);
		} else {
			executeNext();
		}
	}

	private void initContextData(ElementContext context) {
		currentElement = "";
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		lastGroupCode = uPContext.getLastGroupCode();
		pnr = uPContext.getPnr();
		groupCodes = uPContext.getGroupCodesList();
		availableGroupCode = getPnrWiseGroupCodeBy(pnr);
		if (uPContext.getUtilizedPassengers() != null) {
			utilizedPassengers = uPContext.getUtilizedPassengers();
		}
	}

	private String getPnrWiseGroupCodeBy(String pnr) {
		String pnrCode = "";
		if (uPContext.getPnrWiseGroupCodes() != null) {
			pnrCode = uPContext.getPnrWiseGroupCodes().get(pnr);
		}
		return pnrCode;
	}

	private void groupCodeElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		String groupCode = "";
		groupCode = buildGroupCode(lastGroupCode);
		if (!groupCode.isEmpty()) {
			elementTemplate.append(hyphen());
			elementTemplate.append(groupCode);
			currentElement = elementTemplate.toString();
		}
	}

	private String buildGroupCode(String lastGroupCode) {
		String groupCode = "";
		String tmpGroupCode = "";
		if (availableGroupCode != null && !availableGroupCode.isEmpty()) {
			if (validateGroupCodeNecessity(pnr) || validateGroupCodeNecessity()) {
				groupCode = getValidGroupCode(pnr, lastGroupCode);
			} else if (getPnrWisePassengerCount() > 1
					&& !validateGroupCodeNecessity(pnr)
					&& !validateGroupCodeNecessity()) {
				groupCode = getValidGroupCode(pnr, lastGroupCode);
			}
		} else {
			if (getPnrWisePassengerCount() > 1
					&& !validateGroupCodeNecessity(pnr)) {
				tmpGroupCode = getExistingGroupCode();
				if (tmpGroupCode != null && !tmpGroupCode.isEmpty()) {
					groupCode = tmpGroupCode;
				} else {
					groupCode = determineNextGroupId(lastGroupCode, groupCodes);
					setLastGroupCode(groupCode);
					groupCode = groupCode + getPnrWisePassengerCount();
				}
			}
		}
		if(groupCode != null && !groupCode.isEmpty()){
			addPnrWiseGroupCode(pnr, groupCode);
			addPnrPaxIdWiseGroupCode(groupCode);
		}
		return groupCode;
	}

	private String getValidGroupCode(String pnr, String providedLastGroupCode) {

		Pattern pattern = Pattern.compile("(\\d+)");
		Matcher matcher = pattern.matcher(availableGroupCode);

		if (matcher.find() && !getPnrWisePassengerCount().equals(Integer.valueOf(matcher.group(1)))
				&& getPnrWisePassengerCount() > 1) {
			String groupCodeForPnr = getGroupCodeOfPnr(pnr);
			if (groupCodeForPnr != null && !groupCodeForPnr.isEmpty()) {
				availableGroupCode = groupCodeForPnr.split("(?<=\\D)(?=\\d)")[0] + getPnrWisePassengerCount();
			} else {
				availableGroupCode = determineNextGroupId(lastGroupCode, groupCodes) + getPnrWisePassengerCount();
			}
		}

		return availableGroupCode;
	}

	private boolean validateGroupCodeNecessity(String pnr) {
		boolean isValied = false;
		if (uPContext.getGroupCodeAvailabilityMap() != null
				&& !uPContext.getGroupCodeAvailabilityMap().isEmpty()) {
			if (uPContext.getGroupCodeAvailabilityMap().get(pnr) != null) {
				isValied = true;
			}
		}
		return isValied;
	}

	private void addPnrPaxIdWiseGroupCode(String groupCode) {
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			for (PassengerInformation passengerInformation : uPContext
					.getUtilizedPassengers()) {
				uPContext.getPnrPaxVsGroupCodes().put(
						passengerInformation.getPnrPaxId(), groupCode);
			}
		}
	}

	private void addPnrWiseGroupCode(String pnr, String groupCode) {
		if (uPContext.getPnrWiseGroupCodes() != null) {
			uPContext.getPnrWiseGroupCodes().put(pnr, groupCode);
		}
	}

	private String getGroupCodeOfPnr(String pnr) {
		if (uPContext.getPnrWiseGroupCodes() != null) {
			return uPContext.getPnrWiseGroupCodes().get(pnr);
		}
		return null;
	}

	private String getPnrOfGroupCode(String groupCode) {
		String pnr = "";
		if (uPContext.getPnrWiseGroupCodes() != null
				&& !uPContext.getPnrWiseGroupCodes().isEmpty()) {
			for (Map.Entry<String, String> entry : uPContext
					.getPnrWiseGroupCodes().entrySet()) {
				if (entry.getValue() != null
						&& groupCode.equals(entry.getValue())) {
					pnr = entry.getKey();
				}

			}
		}
		return pnr;
	}

	private String determineNextGroupId(String lastGroupId,
			String tourIds[]) {
		int retVal = 0;
		for (int i = 0; i < tourIds.length; i++) {
			if (lastGroupId.equals(tourIds[i])) {
				retVal = ++i;
				break;
			}
		}
		return tourIds[retVal];
	}

	private String getExistingGroupCode() {
		String groupCode = "";
		// May need to compare all the group codes in future...
		for (PassengerInformation passengerInformation : utilizedPassengers) {
			groupCode = passengerInformation.getGroupId();

			if (groupCode != null && !groupCode.isEmpty()) {
				for (String code : uPContext.getPnrWiseGroupCodes().values()) {
					if (!uPContext.getPnrWiseGroupCodes().keySet().contains(passengerInformation.getPnr())
							&& code.startsWith(groupCode.split("(?<=\\D)(?=\\d)")[0])) {
						groupCode = "";
						break;
					}
				}
				String pnr = getPnrOfGroupCode(groupCode);
				if (pnr != null && !pnr.isEmpty()
						&& !pnr.equals(passengerInformation.getPnr())) {
					groupCode = "";
					break;
				}
			}
			
		}
		return groupCode;
	}

	private boolean validateGroupCodeNecessity() {
		boolean isNecessary = false;
		Integer pnrWiseRemainingPassengerCount = getPnrWiseRemainingPassengerCount();
		if (pnrWiseRemainingPassengerCount != 0) {
			isNecessary = true;
		}
		return isNecessary;
	}

	private Integer getPnrWisePassengerCount() {
		Integer pnrWisePassengerCount = 0;
		if (uPContext.getPnrWisePassengerCount() != null) {
			pnrWisePassengerCount = uPContext.getPnrWisePassengerCount()
					.get(uPContext.getOngoingStoreType()).get(pnr);
		}
		return pnrWisePassengerCount;
	}

	private Integer getPnrWiseRemainingPassengerCount() {
		Integer pnrWisePassengerCount = 0;
		if (uPContext.getPnrWisePassengerReductionCount() != null) {
			pnrWisePassengerCount = uPContext
					.getPnrWisePassengerReductionCount()
					.get(uPContext.getOngoingStoreType()).get(pnr);
			if (pnrWisePassengerCount == null) {
				pnrWisePassengerCount = 0;
			}
		}
		return pnrWisePassengerCount;
	}

	private void setLastGroupCode(String groupCode) {
		lastGroupCode = groupCode;
		uPContext.setLastGroupCode(groupCode);
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (response.isProceedNextElement()) {
			ammendToBaseLine(currentElement, currentLine, messageLine);
		} else {
			executeConcatenationElementBuilder(uPContext);
			ammendToBaseLine(currentElement, currentLine, messageLine);
		}
		executeNext();
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext("0"
				+ currentLine.toString(), elementText, 0);
		ruleResponse = groupCodeElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
