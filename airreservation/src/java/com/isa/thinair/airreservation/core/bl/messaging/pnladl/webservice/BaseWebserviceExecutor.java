/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice;

import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public abstract class BaseWebserviceExecutor {

	public abstract boolean executewebService(BaseDataContext baseContext)  throws ModuleException ;
	
}
