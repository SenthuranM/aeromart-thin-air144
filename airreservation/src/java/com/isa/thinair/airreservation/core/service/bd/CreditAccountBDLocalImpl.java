/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.service.bd;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.service.CreditAccountBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Lasantha Pambagoda
 * @isa.module.bd-impl id="creditaccount.service.local" description="local impl of the CreditAccountBD"
 */
public class CreditAccountBDLocalImpl extends PlatformBaseServiceDelegate implements CreditAccountBD {

	/**
	 * method to adjust credit
	 */
	public ServiceResponce balanceCredit(String pnrPaxId, BigDecimal amount) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.AMOUNT, amount);
		return command.execute();
	}

	/**
	 * method to carry forward credit
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce carryForwardCredit(PaxCreditDTO paxCreditDTO, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CARRY_FORWARD_PAX_CREDIT_MACRO);
		command.setParameter(CommandParamNames.PAX_CREDIT_TO, paxCreditDTO);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		return command.execute();
	}

	/**
	 * method to brought forward credit
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce broughtForwardCredit(PaxCreditDTO paxCreditDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.BROUGHT_FORWARD_CREDIT);
		command.setParameter(CommandParamNames.PAX_CREDIT_TO, paxCreditDTO);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		return command.execute();
	}

	/**
	 * method to get reservation Credit by ID
	 */
	public ReservationCredit getReservationCredit(long creditId) {
		ReservationCreditDAO reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		return reservationCreditDao.getReservationCredit(creditId);
	}

}
