package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;

public class RecordAuditViaMDB extends PlatformBaseServiceDelegate {
	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME = "queue/recordAuditQueue";
	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public void sendMessageViaMDB(ArrayList<ReservationAudit> reservationAudit) {
		try {			
			sendMessage(reservationAudit, ReservationModuleUtils.getAirReservationConfig().getJndiProperties(), DESTINATION_JNDI_NAME,
					CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}

}