package com.isa.thinair.airreservation.core.bl.GroupBooking;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingReqRoutesTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestHistoryTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupPaymentNotificationDTO;
import com.isa.thinair.airreservation.api.model.GroupBookingReqRoutes;
import com.isa.thinair.airreservation.api.model.GroupBookingRequest;
import com.isa.thinair.airreservation.api.model.GroupBookingRequestHistory;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.core.persistence.dao.UserDAO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;

public class GroupBookingRequestBL {

	public GroupBookingRequest setGroupBookingReqObject(GroupBookingRequestTO grpReqTo) {
		GroupBookingRequest grpReq = new GroupBookingRequest();
		grpReq.setRequestDate(grpReqTo.getRequestDate());
		grpReq.setAdultAmount(grpReqTo.getAdultAmount());
		grpReq.setChildAmount(grpReqTo.getChildAmount());
		grpReq.setInfantAmount(grpReqTo.getInfantAmount());
		grpReq.setOalFare(grpReqTo.getOalFare());
		grpReq.setRequestDate(grpReqTo.getRequestedDate());
		grpReq.setRequestedFare(grpReqTo.getRequestedFare());
		grpReq.setAgreedFare(grpReqTo.getAgreedFare());
		grpReq.setAgentComments(grpReqTo.getAgentComments());
		grpReq.setApprovalComments(grpReq.getApprovalComments());
		grpReq.setCraeteUserCode(grpReqTo.getCraeteUserCode());
		grpReq.setCreatedDate(grpReqTo.getRequestedDate());
		grpReq.setApprovalUserCode(grpReqTo.getApprovalUserCode());
		grpReq.setMinPaymentRequired(grpReqTo.getMinPaymentRequired());
		grpReq.setApprovalComments(grpReqTo.getApprovalComments());
		grpReq.setStatusID(grpReqTo.getStatusID());
		grpReq.setVersion(grpReqTo.getVersion());
		grpReq.setFareValidDate(grpReqTo.getFareValidDate());
		grpReq.setApprovalDate(grpReqTo.getApprovalDate());
		grpReq.setTargetPaymentDate(grpReqTo.getTargetPaymentDate());
		grpReq.setStationCode(grpReqTo.getStationCode());
		grpReq.setRequestedAgentCode(grpReqTo.getRequestedAgentCode());
		grpReq.setApprovedPercentage(grpReqTo.isAgreedFarePercentage());
		if (grpReqTo.getRequestID() != 0) {

			grpReq.setRequestID(grpReqTo.getRequestID());
		}

		return grpReq;
	}

	public GroupBookingRequestTO setGroupBookingReqToObject(GroupBookingRequestTO grpReqTo) {
		GroupBookingRequestTO grpReq = new GroupBookingRequestTO();
		grpReq.setRequestDate(grpReqTo.getRequestDate());
		grpReq.setAdultAmount(grpReqTo.getAdultAmount());
		grpReq.setChildAmount(grpReqTo.getChildAmount());
		grpReq.setInfantAmount(grpReqTo.getInfantAmount());
		grpReq.setPaxCount(grpReqTo.getAdultAmount(), grpReqTo.getChildAmount(), grpReqTo.getInfantAmount());
		grpReq.setMainRequestID(grpReqTo.getMainRequestID());
		grpReq.setAgent(grpReqTo.getAgent());
		grpReq.setOalFare(grpReqTo.getOalFare());
		grpReq.setRequestDate(grpReqTo.getRequestedDate());
		grpReq.setRequestedFare(grpReqTo.getRequestedFare());
		grpReq.setAgreedFare(grpReqTo.getAgreedFare());
		grpReq.setAgentComments(grpReqTo.getAgentComments());
		grpReq.setApprovalComments(grpReq.getApprovalComments());
		grpReq.setCraeteUserCode(grpReqTo.getCraeteUserCode());
		grpReq.setApprovalUserCode(grpReqTo.getApprovalUserCode());
		grpReq.setMinPaymentRequired(grpReqTo.getMinPaymentRequired());
		grpReq.setApprovalComments(grpReqTo.getApprovalComments());
		grpReq.setStatusID(grpReqTo.getStatusID());
		grpReq.setVersion(grpReqTo.getVersion());
		grpReq.setFareValidDate(grpReqTo.getFareValidDate());
		grpReq.setApprovalDate(grpReqTo.getApprovalDate());
		grpReq.setStationCode(grpReqTo.getStationCode());
		grpReq.setTargetPaymentDate(grpReqTo.getTargetPaymentDate());
		grpReq.setRequestedAgentCode(grpReqTo.getRequestedAgentCode());
		grpReq.setAgreedFarePercentage(grpReqTo.isAgreedFarePercentage());
		return grpReq;
	}

	public GroupBookingRequest setSearchGroupBookingReqObject(GroupBookingRequestTO grpReqTo) {
		GroupBookingRequest grpReq = new GroupBookingRequest();
		grpReq.setRequestDate(grpReqTo.getRequestDate());
		grpReq.setAdultAmount(grpReqTo.getAdultAmount());
		grpReq.setChildAmount(grpReqTo.getChildAmount());
		grpReq.setInfantAmount(grpReqTo.getInfantAmount());
		grpReq.setOalFare(grpReqTo.getOalFare());
		grpReq.setRequestDate(grpReqTo.getRequestedDate());
		grpReq.setAgreedFare(grpReqTo.getAgreedFare());
		grpReq.setAgentComments(grpReqTo.getAgentComments());
		grpReq.setApprovalComments(grpReq.getApprovalComments());
		grpReq.setCraeteUserCode(grpReqTo.getCraeteUserCode());
		grpReq.setCreatedDate(grpReqTo.getRequestedDate());
		grpReq.setApprovalUserCode(grpReqTo.getApprovalUserCode());
		grpReq.setMinPaymentRequired(grpReqTo.getMinPaymentRequired());
		grpReq.setApprovalComments(grpReqTo.getApprovalComments());
		grpReq.setStatusID(grpReqTo.getStatusID());
		grpReq.setApprovedPercentage(grpReqTo.isAgreedFarePercentage());
		grpReq.setRequestedAgentCode(grpReqTo.getRequestedAgentCode());
		grpReq.setStationCode(grpReqTo.getStationCode());
		if (grpReqTo.getRequestID() != null) {

			grpReq.setRequestID(grpReqTo.getRequestID());
		}

		return grpReq;
	}

	public GroupBookingRequest createNewGroupBookingReqObject(GroupBookingRequest grpReqTo) {
		GroupBookingRequest grpReq = new GroupBookingRequest();
		grpReq.setRequestDate(grpReqTo.getRequestDate());
		grpReq.setAdultAmount(grpReqTo.getAdultAmount());
		grpReq.setChildAmount(grpReqTo.getChildAmount());
		grpReq.setInfantAmount(grpReqTo.getInfantAmount());
		grpReq.setOalFare(grpReqTo.getOalFare());
		grpReq.setRequestDate(grpReqTo.getRequestDate());
		grpReq.setRequestedFare(grpReqTo.getRequestedFare());
		grpReq.setAgreedFare(grpReqTo.getAgreedFare());
		grpReq.setAgentComments(grpReqTo.getAgentComments());
		grpReq.setApprovalComments(grpReq.getApprovalComments());
		grpReq.setCraeteUserCode(grpReqTo.getCraeteUserCode());
		grpReq.setCreatedDate(grpReqTo.getRequestDate());
		grpReq.setApprovalUserCode(grpReqTo.getApprovalUserCode());
		grpReq.setMinPaymentRequired(grpReqTo.getMinPaymentRequired());
		grpReq.setStationCode(grpReqTo.getStationCode());
		grpReq.setRequestedAgentCode(grpReqTo.getRequestedAgentCode());
		grpReq.setApprovedPercentage(grpReqTo.isApprovedPercentage());
		return grpReq;
	}

	public GroupBookingRequestTO setGroupBookingReqToObject(GroupBookingRequest grpReqTo) {
		GroupBookingRequestTO grpReq = new GroupBookingRequestTO();
		grpReq.setRequestDate(grpReqTo.getRequestDate());
		grpReq.setAdultAmount(grpReqTo.getAdultAmount());
		grpReq.setChildAmount(grpReqTo.getChildAmount());
		grpReq.setInfantAmount(grpReqTo.getInfantAmount());
		grpReq.setPaxCount(grpReqTo.getAdultAmount(), grpReqTo.getChildAmount(), grpReqTo.getInfantAmount());
		grpReq.setOalFare(grpReqTo.getOalFare());
		grpReq.setRequestedFare(grpReqTo.getRequestedFare());
		grpReq.setMinPaymentRequired(grpReqTo.getMinPaymentRequired());
		grpReq.setFareValidDate(grpReqTo.getFareValidDate());
		grpReq.setApprovalComments(grpReqTo.getApprovalComments());
		grpReq.setApprovalDate(grpReqTo.getApprovalDate());
		grpReq.setApprovalUserCode(grpReqTo.getApprovalUserCode());
		grpReq.setAgentComments(grpReqTo.getAgentComments());
		grpReq.setRequestID(grpReqTo.getRequestID());
		grpReq.setVersion(grpReqTo.getVersion());
		grpReq.setStatusID(grpReqTo.getStatusID());
		grpReq.setStatusName(grpReqTo.getStatusID());
		grpReq.setAgreedFare(grpReqTo.getAgreedFare());
		grpReq.setApprovalDate(grpReqTo.getApprovalDate());
		grpReq.setTargetPaymentDate(grpReqTo.getTargetPaymentDate());
		grpReq.setCraeteUserCode(grpReqTo.getCraeteUserCode());
		grpReq.setPaymentAmount(grpReqTo.getPaymentAmount());
		grpReq.setAgentName(grpReqTo.getUserName());
		grpReq.setRequestedAgentCode(grpReqTo.getRequestedAgentCode());
		grpReq.setAgreedFarePercentage(grpReqTo.isApprovedPercentage());
		grpReq.setMainRequestID(grpReqTo.getMstGroupBookingRequestId());
		grpReq.setStationCode(grpReqTo.getStationCode());
		if (grpReqTo.getGroupBookingRoutes() != null) {
			grpReq.setGroupBookingRoutes(setGroupBookingReqRoutesToObject(grpReqTo.getGroupBookingRoutes()));
			StringBuilder onds = new StringBuilder();
			boolean firstElement = true;
			for (GroupBookingReqRoutes tempGNRR : grpReqTo.getGroupBookingRoutes()) {
				if (firstElement) {
					onds.append(tempGNRR.getDeparture() + "/" + tempGNRR.getArrival());
					firstElement = false;
				} else {
					onds.append(" | " + tempGNRR.getDeparture() + "/" + tempGNRR.getArrival());
				}
			}
			grpReq.setOnds(onds.toString());
		}

		grpReq.getSubRequests().addAll(transformFroupBookingRequests(grpReqTo.getSubRequests()));

		return grpReq;
	}

	public Set<GroupBookingRequestTO> transformFroupBookingRequests(Set<GroupBookingRequest> grpRequests) {
		Set<GroupBookingRequestTO> requests = new HashSet<GroupBookingRequestTO>();
		if (grpRequests != null) {
			for (GroupBookingRequest request : grpRequests) {
				requests.add(setGroupBookingReqToObject(request));
			}
		}
		return requests;
	}

	public Set<GroupBookingRequest> transformFroupBookingRequestTOs(Set<GroupBookingRequestTO> grpRequestTOs) {
		Set<GroupBookingRequest> requests = new HashSet<GroupBookingRequest>();
		if (grpRequestTOs != null) {
			for (GroupBookingRequestTO requestTO : grpRequestTOs) {
				requests.add(setGroupBookingReqObject(requestTO));
			}
		}
		return requests;
	}

	public Set<GroupBookingReqRoutes> setGroupBookingReqRoutesObject(Set<GroupBookingReqRoutesTO> grpBookRouteTo) {

		Set<GroupBookingReqRoutes> grpBookRouteReq = new HashSet<GroupBookingReqRoutes>();
		if (grpBookRouteTo != null) {
			for (GroupBookingReqRoutesTO tempGNRR : grpBookRouteTo) {
				GroupBookingReqRoutes gnRR = new GroupBookingReqRoutes();
				gnRR.setArrival(tempGNRR.getArrival());
				gnRR.setDeparture(tempGNRR.getDeparture());
				gnRR.setReturningDate(tempGNRR.getReturningDate());
				gnRR.setDepartureDate(tempGNRR.getDepartureDate());
				gnRR.setFlightNumber(tempGNRR.getFlightNumber());
				gnRR.setVia(tempGNRR.getVia());
				gnRR.setId(tempGNRR.getId());
				grpBookRouteReq.add(gnRR);
			}
		}
		return grpBookRouteReq;
	}

	public Set<GroupBookingReqRoutesTO> setGroupBookingReqRoutesToObject(Set<GroupBookingReqRoutes> grpBookRoute) {
		Set<GroupBookingReqRoutesTO> grpBookRouteReq = new HashSet<GroupBookingReqRoutesTO>();

		for (GroupBookingReqRoutes tempGNRR : grpBookRoute) {
			GroupBookingReqRoutesTO gnRR = new GroupBookingReqRoutesTO();
			gnRR.setArrival(tempGNRR.getArrival());
			gnRR.setDeparture(tempGNRR.getDeparture());
			gnRR.setReturningDate(tempGNRR.getReturningDate());
			gnRR.setFlightNumber(tempGNRR.getFlightNumber());
			gnRR.setVia(tempGNRR.getVia());
			gnRR.setDepartureDate(tempGNRR.getDepartureDate());
			gnRR.setRequestID(tempGNRR.getRequestID());

			gnRR.setId(tempGNRR.getId());
			grpBookRouteReq.add(gnRR);
		}

		return grpBookRouteReq;
	}

	public void sendDuePaymentReminder(CredentialsDTO credentialsDTO) throws ModuleException {

		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		List<GroupPaymentNotificationDTO> groupBookingNotifications = reservationDAO.getGrooupBookingForReminders();
		if (groupBookingNotifications != null && groupBookingNotifications.size() > 0) {
			emailDuePaymnetInReminder(groupBookingNotifications, credentialsDTO);
		}
	}

	public GroupBookingRequestHistory setGroupBookingHistory(GroupBookingRequest groupReq, String userCode, int previosStatus) {
		GroupBookingRequestHistory grpHistory = new GroupBookingRequestHistory();

		grpHistory.setGroupBookingRequestID(groupReq.getRequestID());
		grpHistory.setAddedBy(userCode);
		grpHistory.setNewStatusID(groupReq.getStatusID());
		grpHistory.setPreviosuStatusID(previosStatus);
		StringBuilder strDesc = new StringBuilder();
		if (groupReq.getAgentComments() != null) {
			strDesc.append("Agent comments : " + groupReq.getAgentComments());
		}
		if (groupReq.getApprovalComments() != null) {
			strDesc.append("Approver comments : " + groupReq.getApprovalComments());
		}
		grpHistory.setGroupBookingDetails(strDesc.toString());
		grpHistory.setAddedDate(new Date());

		return grpHistory;

	}

	public GroupBookingRequestHistoryTO setGroupBookingReqHistoryObject(GroupBookingRequestHistory grpReq) {
		GroupBookingRequestHistoryTO grpBookingHisTo = new GroupBookingRequestHistoryTO();
		grpBookingHisTo.setHistoryID(grpReq.getHistoryID());
		grpBookingHisTo.setGroupBookingRequestID(grpReq.getGroupBookingRequestID());
		grpBookingHisTo.setGroupBookingDetails(grpReq.getGroupBookingDetails());
		grpBookingHisTo.setNewStatusID(grpReq.getNewStatusID());
		grpBookingHisTo.setPreviosuStatusID(grpReq.getPreviosuStatusID());
		grpBookingHisTo.setAddedBy(grpReq.getAddedBy());
		grpBookingHisTo.setAddedDate(grpReq.getAddedDate());
		return grpBookingHisTo;
	}

	public GroupBookingReqRoutesTO
			findMatchingRoute(Collection<GroupBookingReqRoutesTO> routes, GroupBookingReqRoutes routeToFind) {
		GroupBookingReqRoutesTO matchingRoute = null;

		for (GroupBookingReqRoutesTO route : routes) {
			if (route.getDeparture().equals(routeToFind.getDeparture())
					&& StringUtils.equals(route.getVia(), routeToFind.getVia())
					&& route.getArrival().equals(routeToFind.getArrival())) {
				matchingRoute = route;
			}
		}
		return matchingRoute;
	}

	private static void emailDuePaymnetInReminder(List<GroupPaymentNotificationDTO> groupBookingNotifications,
			CredentialsDTO credentialsDTO) throws ModuleException {

		List<MessageProfile> messageProfileList = new ArrayList<MessageProfile>();

		for (GroupPaymentNotificationDTO groupBookingNotification : groupBookingNotifications) {

			String emailAddress = BeanUtils.nullHandler(groupBookingNotification.getEmail());

			if (emailAddress.length() > 0) {
				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setFirstName(groupBookingNotification.getName());
				userMessaging.setLastName(groupBookingNotification.getLastName());
				userMessaging.setToAddres(emailAddress);

				Locale locale = Locale.ENGLISH;

				List<UserMessaging> messages = new ArrayList<UserMessaging>();
				messages.add(userMessaging);
				HashMap<String, GroupPaymentNotificationDTO> partialPaymentReminderDataMap = (HashMap<String, GroupPaymentNotificationDTO>) getPartialPaymentReminderDataMap(groupBookingNotification);

				// Topic
				Topic topic = new Topic();
				topic.setTopicParams(partialPaymentReminderDataMap);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.PARTIAL_PAYMENT_REMINDER_EMAIL);
				topic.setLocale(locale);
				topic.setAttachMessageBody(false);

				// User Profile
				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				messageProfileList.add(messageProfile);
			}
		}

		if (messageProfileList != null && messageProfileList.size() > 0) {
			ReservationModuleUtils.getMessagingServiceBD().sendMessages(messageProfileList);
		}

	}

	private static Map<String, GroupPaymentNotificationDTO> getPartialPaymentReminderDataMap(
			GroupPaymentNotificationDTO groupBookingNotification) {
		Map<String, GroupPaymentNotificationDTO> groupBookingInDataMap = new HashMap<String, GroupPaymentNotificationDTO>();

		groupBookingInDataMap.put("notificationDetailDto", groupBookingNotification);

		return groupBookingInDataMap;
	}

	private UserDAO lookupUserDAO() {
		return (UserDAO) AirSecurityUtil.getInstance().getLocalBean("UserDAOImplProxy");
	}

	public static List sendGroupBookingNotificationMessages(LCCClientReservation ownReservation, String action)
			throws ModuleException {

		List messagesList = new ArrayList();

		Agent agent = ReservationModuleUtils.getTravelAgentBD().getAgent(ownReservation.getAdminInfo().getOwnerAgentCode());
		Collection<String> recipientsAddresses = ReservationModuleUtils.getAirReservationConfig()
				.getGroupBookingEmailRecipientList();

		List<LinkedHashMap<String, String>> flightDetails = new ArrayList<LinkedHashMap<String, String>>();
		List<LCCClientReservationSegment> flightSegments = new ArrayList<LCCClientReservationSegment>(
				ownReservation.getSegments());
		Collections.sort(flightSegments);

		for (LCCClientReservationSegment seg : flightSegments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(seg.getStatus())) {

				String flightDate = CalendarUtil.getFormattedDate(seg.getDepartureDate(), CalendarUtil.PATTERN11);
				LinkedHashMap<String, String> flightDataMap = new LinkedHashMap<String, String>();
				flightDataMap.put("flightNo", seg.getFlightNo());
				flightDataMap.put("flightDate", flightDate);
				flightDataMap.put("segmentCode", seg.getSegmentCode());

				flightDetails.add(flightDataMap);
			}
		}

		if (recipientsAddresses != null && !recipientsAddresses.isEmpty()) {
			for (String recipient : recipientsAddresses) {
				// User Message
				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setFirstName(recipient);
				userMessaging.setToAddres(recipient);

				HashMap<String, Object> emailDataMap = new HashMap<String, Object>();
				emailDataMap.put("pnr", ownReservation.getPNR());
				emailDataMap.put("userName", recipient);
				emailDataMap.put("agentCode", ownReservation.getAdminInfo().getOwnerAgentCode() != null ? ownReservation
						.getAdminInfo().getOwnerAgentCode() : "");
				emailDataMap.put("agentName", agent != null ? agent.getAgentName() : "");
				emailDataMap
						.put("dateOfBooking",
								CalendarUtil.getFormattedDate(ownReservation.getZuluBookingTimestamp(), CalendarUtil.PATTERN2)
										.toString());
				emailDataMap.put("action", action);
				emailDataMap.put("flightList", flightDetails);

				try {
					agent = ReservationModuleUtils.getTravelAgentBD().getAgent(ownReservation.getAdminInfo().getOwnerAgentCode());
				} catch (ModuleException e) {
					e.printStackTrace();
				}

				List messages = new ArrayList();
				messages.add(userMessaging);

				// Topic
				Topic topic = new Topic();
				topic.setTopicParams(emailDataMap);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.GROUP_BOOKING_EMAIL);
				// User Profile
				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				messagesList.add(messageProfile);
			}
		}

		return messagesList;
	}

}
