/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.AutoCancellationSchDTO;
import com.isa.thinair.airreservation.api.dto.ConnectionPaxInfoTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPassengerDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * PassengerDAO is the business DAO interface for the reservation passenger service apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface PassengerDAO {

	/**
	 * Returns a passenger
	 * 
	 * @param pnrPaxId
	 * @param loadFares
	 * @param loadInfantPaymentFlag TODO
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public ReservationPax getPassenger(int pnrPaxId, boolean loadFares, boolean loadResAdminInfo, boolean loadInfantPaymentFlag)
			throws CommonsDataAccessException;

	/**
	 * Return reservation passengers
	 * 
	 * @param pnrStatus
	 *            reservation status
	 * @param toDate
	 *            release date end on
	 * @return collection of pnr as String objects
	 */
	public Map<String, Long> getReservationPassengers(String pnrStatus, Date toDate);

	/**
	 * Return Passenger Titles
	 * 
	 * @return
	 */
	public Map<String, String> getPassengerTitles();

	public Integer getPnrPaxId(String pnr, Integer paxSequence);

	/**
	 * Returns marketing perspective lcc reservation(s)
	 * 
	 * @param pnrStatus
	 * @param toDate
	 * @param carrierCode
	 * @return
	 */
	public Collection<String> getMarketingLccReservations(String pnrStatus, Date toDate, String carrierCode);

	/**
	 * Return expired infant(s) based on auto cancellation flag
	 * 
	 * @param cancellationIds
	 * @param marketingCarrier
	 * @return
	 */
	public List<AutoCancellationSchDTO> getExpiredInfants(Collection<Integer> cancellationIds, String marketingCarrier);

	/**
	 * Gets the pnr pax fare IDs for noshow passengers.
	 * 
	 * @param pnrPaxId
	 *            PNR Pax ID of the particular passenger
	 * @return A Map<String,Collection<String>> with key -> pnrSegID and value -> a collection of pnr pax fare ids or an
	 *         empty map if the selected passenger has no NoShow segments.
	 */
	public Map<String, Collection<String>> getNoShowPPDIDsForPax(String pnrPaxId);

	/**
	 * Returns marketing perspective lcc reservation(s) expired infants based on auto cancellation flag
	 * 
	 * @param marketingCarrier
	 * @param cancellationIds
	 * 
	 * @return
	 */
	public Map<String, Integer> getExpiredLccInfantsInfo(String marketingCarrier, Collection<Integer> cancellationIds);

	/**
	 * Save reservation passengers
	 * 
	 * @param passengers
	 */
	public void savePassengers(Collection<ReservationPax> passengers);

	public List<PNRGOVPassengerDTO> getPassengersForPNR(String pnr);

	public Boolean hasMismatchOccured(int flightID, String segmentCode, String bookingCode);

	public List<Map<String, String>> getInventoryDataForMismatchTracking(String pnr);

	public Map<String, Collection<String>> getPassengerTitleTypes(Collection<String> excludingPaxTypes);

    public List<ConnectionPaxInfoTO> getConnectionPax(Date fromDate, Date toDate, int connectionTime, List<String> segments);
}