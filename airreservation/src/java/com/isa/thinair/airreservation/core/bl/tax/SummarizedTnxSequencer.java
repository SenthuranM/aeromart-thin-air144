package com.isa.thinair.airreservation.core.bl.tax;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;

public class SummarizedTnxSequencer implements Sequencer {

	private Map<ReservationTnx, Long> tnxMap;
	private Map<ReservationPaxOndCharge, Long> chargeMap;
	private int minimumSequence;

	public SummarizedTnxSequencer() {
		this.tnxMap = new HashMap<>();
		this.chargeMap = new HashMap<>();
		this.minimumSequence = -1;
	}

	@Override
	public Long getKey(ReservationPaxOndCharge charge) {
		return chargeMap.get(charge);
	}

	@Override
	public Long getKey(ReservationTnx resTnx) {
		return tnxMap.get(resTnx);
	}

	public void processTnx(TransactionSequence paxLastPaidTnx) {

		for (ReservationTnx paxTnx : paxLastPaidTnx.getSnapshotPaymentTnx().getPayments()) {
			tnxMap.put(paxTnx, captureMinimumSequence((long) paxLastPaidTnx.getTnxSequence()));
		}

		for (ReservationPaxOndCharge ondCharge : paxLastPaidTnx.getChargeTnx().getChargeList()) {
			chargeMap.put(ondCharge, captureMinimumSequence((long) paxLastPaidTnx.getTnxSequence()));
		}
	}

	private long captureMinimumSequence(long value) {
		if (minimumSequence == -1 || value < minimumSequence) {
			minimumSequence = (int) value;
		}
		return value;
	}

	@Override
	public int getMinmumSequence() {
		return minimumSequence;
	}

}
