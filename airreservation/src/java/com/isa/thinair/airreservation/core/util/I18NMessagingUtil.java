package com.isa.thinair.airreservation.core.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class I18NMessagingUtil {
	private enum MESSAGE_LOCALE_FOR {
		ITINERARY, IBE_ONHOLD_EMAIL, IBE_ONHOLD_PAYMENT_EMAIL
	};

	private static final String ITINERARY_MESSAGES = "resources/i18n/ItineraryMessages";
	private static final String IBE_ONHOLD_EMAIL_MESSAGES = "resources/i18n/IBEOnHoldEmailMessages";
	private static final String IBE_ONHOLD__PAYMENT_REMINDER_EMAIL_MESSAGES = "resources/i18n/IBEOnHoldPaymentReminderEmailMessages";

	private static ResourceBundle getResourceBundle(Locale locale, MESSAGE_LOCALE_FOR ml) {
		ResourceBundle rb = null;
		if (ml == MESSAGE_LOCALE_FOR.ITINERARY) {
			rb = ResourceBundle.getBundle(ITINERARY_MESSAGES, locale);
		} else if (ml == MESSAGE_LOCALE_FOR.IBE_ONHOLD_EMAIL) {
			rb = ResourceBundle.getBundle(IBE_ONHOLD_EMAIL_MESSAGES, locale);
		} else if (ml == MESSAGE_LOCALE_FOR.IBE_ONHOLD_PAYMENT_EMAIL) {
			rb = ResourceBundle.getBundle(IBE_ONHOLD__PAYMENT_REMINDER_EMAIL_MESSAGES, locale);
		}
		return rb;
	}

	private static Map<String, String> getMessages(Locale locale, MESSAGE_LOCALE_FOR ml) {
		Map<String, String> labels = new HashMap<String, String>();
		ResourceBundle rb = getResourceBundle(locale, ml);
		if (rb != null) {
			Enumeration<String> keyItr = rb.getKeys();
			while (keyItr.hasMoreElements()) {
				String key = keyItr.nextElement();
				labels.put(key, rb.getString(key));
			}
		}
		return labels;
	}

	public static String getMessage(String messageCode, Locale locale) {
		String message = "";
		try {
			ResourceBundle rb = getResourceBundle(locale, MESSAGE_LOCALE_FOR.ITINERARY);
			if (rb != null) {
				message = rb.getString(messageCode);
			}
		} catch (NullPointerException e) {
		} catch (MissingResourceException mx) {
		} catch (ClassCastException cce) {
		}
		return message;
	}

	public static Map<String, String> getItineraryMessages(Locale locale) {
		return getMessages(locale, MESSAGE_LOCALE_FOR.ITINERARY);
	}

	public static Map<String, String> getIBEOnHoldEmailMessages(Locale locale) {
		return getMessages(locale, MESSAGE_LOCALE_FOR.IBE_ONHOLD_EMAIL);
	}
	
	public static Map<String, String> getIBEOnHoldPaymentReminderEmailMessages(Locale locale) {
		return getMessages(locale, MESSAGE_LOCALE_FOR.IBE_ONHOLD_PAYMENT_EMAIL);
	}
}
