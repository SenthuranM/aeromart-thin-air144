package com.isa.thinair.airreservation.core.bl.common;

import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.flightmanifest.AirportSummaryDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.CabinClassSummaryDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestRecordDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.LogicalCCSummaryDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.ManifestCountDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.SegmentSummaryDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.commons.api.dto.PaxCategoryTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;

public class FlightManifestBL {

	private enum PaxType {
		MALE, FEMALE, CHILD, INFANT, UNKNOWN
	};

	@SuppressWarnings("unused")
	public static Boolean sendFlightManifestAsEmail(FlightManifestOptionsDTO manifestOptions) throws ModuleException {

		FlightManifestDTO manifestData = processFlightManifestContent(manifestOptions);
		HashMap<String, Object> manifestDataMap = (HashMap<String, Object>) prepareDataMapForFlightManifest(manifestData,
				manifestOptions);

		Topic topic = new Topic();
		UserMessaging usermsg = new UserMessaging();

		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		usermsg.setToAddres(manifestOptions.getEmailId());
		usermsg.setFirstName("");
		usermsg.setLastName("");
		messageList.add(usermsg);

		MessageProfile msgProfile = new MessageProfile();
		msgProfile.setUserMessagings(messageList);

		String invoiceEmails = getEmailString(CommonsServices.getGlobalConfig().getBizParam(
				SystemParamKeys.EMAIL_ADDRESS_FOR_ALERT_ON_ROLE_CHANGE));

		manifestDataMap.put("user", usermsg);
		manifestDataMap.put("notice", " ----------------------------------------------- <br> "
				+ " This E-mail is system generated. <br> " + " Please do not reply to the sender's address <br> "
				+ " For any queries please reply to : <br>" + invoiceEmails + "<br> "
				+ " -----------------------------------------------<br>");

		topic.setTopicParams(manifestDataMap);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.FLIGHT_MANIFEST_EMAIL);
		topic.setAttachMessageBody(true);

		msgProfile.setTopic(topic);

		if (msgProfile != null) {
			ReservationModuleUtils.getMessagingServiceBD().sendMessage(msgProfile);
			return true;
		} else {
			return false;
		}

	}

	/**
	 * get invoice reply email Address
	 * 
	 * @param replyEmailAddrs
	 * @return
	 */
	private static String getEmailString(String replyEmailAddrs) {

		String[] sArr = replyEmailAddrs.split(",");
		String str = "";
		if (sArr != null && sArr.length > 1) {
			str = sArr[0] + " or " + sArr[1];
		} else {
			str = sArr[0];
		}

		return str;
	}

	/**
	 * Sends generated manifest HTML content to the action class
	 * 
	 * @throws ModuleException
	 */
	public static String viewFlightManifest(FlightManifestOptionsDTO manifestOptions) throws ModuleException {

		FlightManifestDTO manifestData = processFlightManifestContent(manifestOptions);

		HashMap<String, Object> manifestDataMap = prepareDataMapForFlightManifest(manifestData, manifestOptions);

		return getManifestContent(manifestDataMap);

	}

	/**
	 * Prepares data map for emailing and viewing flight manifest
	 * 
	 * @return
	 */
	private static HashMap<String, Object> prepareDataMapForFlightManifest(FlightManifestDTO manifestData,
			FlightManifestOptionsDTO manifestOptions) {
		HashMap<String, Object> manifestDataMap = new HashMap<String, Object>();

		Date dStartDate = new Date();
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat timeFmt = new SimpleDateFormat("HH:mm:ss");
		String startDate = formatter.format(dStartDate);
		String startTime = timeFmt.format(dStartDate);
		manifestDataMap.put("startDate", startDate);
		manifestDataMap.put("startTIme", startTime);
		manifestDataMap.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		manifestDataMap.put("logoImageName",
				StaticFileNameUtil.getCorrected("LogoAni" + AppSysParamsUtil.getDefaultCarrierCode() + ".gif"));
		manifestDataMap.put("manifestLegend", getFlightManifestLegend());
		manifestDataMap.put("manifestData", manifestData);
		manifestDataMap.put("manifestOptions", manifestOptions);

		return manifestDataMap;
	}

	/**
	 * Prepares full manifest data to be sent to template engine
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private static FlightManifestDTO processFlightManifestContent(FlightManifestOptionsDTO manifestOptions)
			throws ModuleException {

		// Extract flight manifest data from the backend
		ReservationAuxilliaryDAO reservationAuxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;

		Collection<FlightManifestRecordDTO> manifestRecords = reservationAuxilliaryDAO.getFlightManifest(manifestOptions);
		String passengersSummary = reservationAuxilliaryDAO.getFlightPassengerSummary(manifestOptions);

		FlightManifestDTO manifestData = new FlightManifestDTO();
		manifestData.setFlightNo(manifestOptions.getFlightNo());
		manifestData.setRoute(manifestOptions.getRoute());
		manifestData.setFlightDate(manifestOptions.getFlightDate());

		manifestData.setPassengersSummary(passengersSummary);

		// FlightLoadStatus will always be called whether or not records present
		// or not
		setFlightLoadStatusSummary(manifestRecords, manifestData);

		// Other summaries will only be taken into account pax records available
		if (manifestRecords != null) {

			// Sets COS summary
			if (manifestOptions.isFilterByCabinClass() || manifestOptions.isFilterByLogicalCabinClass()) {
				setCOSwiseSummary(manifestRecords, manifestData);
			}

			if (FlightManifestOptionsDTO.SUMMARY_OPTIONS_SEGMENTWISE.equalsIgnoreCase(manifestOptions.getSummaryMode())) {
				setSegmentwiseSummary(manifestRecords, manifestData);
			} else if (FlightManifestOptionsDTO.SUMMARY_OPTIONS_INOUT.equalsIgnoreCase(manifestOptions.getSummaryMode())) {
				setAirportSummary(manifestRecords, manifestOptions.getFlightId(), manifestData);
			} else {
				setSegmentwiseSummary(manifestRecords, manifestData);
			}

		}

		// Prepare data for generate report
		formatAndSetDataForDisplay(manifestRecords, manifestData, manifestOptions.getUserPrincipal());

		return manifestData;
	}

	/**
	 * Sets flight load status summary
	 * 
	 * @param manifestRecords
	 * @param manifestData
	 */
	private static void setFlightLoadStatusSummary(Collection<FlightManifestRecordDTO> manifestRecords,
			FlightManifestDTO manifestData) {

		ManifestCountDTO flightLoadStatus = new ManifestCountDTO();

		if (manifestRecords != null) {

			for (FlightManifestRecordDTO manifestRecord : manifestRecords) {
				PaxType type = getPassengerCategory(manifestRecord);

				switch (type) {
				case MALE:
					flightLoadStatus.setMaleCount(flightLoadStatus.getMaleCount() + 1);
					break;
				case FEMALE:
					flightLoadStatus.setFemaleCount(flightLoadStatus.getFemaleCount() + 1);
					break;
				case CHILD:
					flightLoadStatus.setChildCount(flightLoadStatus.getChildCount() + 1);
					break;
				case INFANT:
					flightLoadStatus.setInfantCount(flightLoadStatus.getInfantCount() + 1);
					break;
				case UNKNOWN:
					flightLoadStatus.setUnknownCount(flightLoadStatus.getUnknownCount() + 1);
					break;
				}
				// This is to count total excess baggage weight
				flightLoadStatus.setTotalExcessBaggageWeight(flightLoadStatus.getTotalExcessBaggageWeight()
						+ manifestRecord.getBaggageWeight());
			}
		}

		flightLoadStatus.setTotalCount(flightLoadStatus.getMaleCount() + flightLoadStatus.getFemaleCount()
				+ flightLoadStatus.getChildCount() + flightLoadStatus.getInfantCount() + flightLoadStatus.getUnknownCount());
		manifestData.setFlightLoadStatus(flightLoadStatus);

	}

	/**
	 * Sets segment wise summaries of passenger count
	 * 
	 * @param manifestRecords
	 * @param segmentCodes
	 * @return
	 */
	private static void
			setSegmentwiseSummary(Collection<FlightManifestRecordDTO> manifestRecords, FlightManifestDTO manifestData) {

		Map<String, SegmentSummaryDTO> segmentSummary = new HashMap<String, SegmentSummaryDTO>();

		Collection<String> segmentCodes = getSegmentCodes(manifestRecords);

		for (String segmentCode : segmentCodes) {
			segmentSummary.put(segmentCode, new SegmentSummaryDTO(segmentCode));
		}

		for (FlightManifestRecordDTO manifestRecord : manifestRecords) {

			PaxType type = getPassengerCategory(manifestRecord);
			String segmentCode = generateSegmentCode(manifestRecord);

			switch (type) {
			case MALE:
				segmentSummary.get(segmentCode).setMaleCount(segmentSummary.get(segmentCode).getMaleCount() + 1);
				segmentSummary.get(segmentCode).setTotalCount(segmentSummary.get(segmentCode).getTotalCount() + 1);
				break;
			case FEMALE:
				segmentSummary.get(segmentCode).setFemaleCount(segmentSummary.get(segmentCode).getFemaleCount() + 1);
				segmentSummary.get(segmentCode).setTotalCount(segmentSummary.get(segmentCode).getTotalCount() + 1);
				break;
			case CHILD:
				segmentSummary.get(segmentCode).setChildCount(segmentSummary.get(segmentCode).getChildCount() + 1);
				segmentSummary.get(segmentCode).setTotalCount(segmentSummary.get(segmentCode).getTotalCount() + 1);
				break;
			case INFANT:
				segmentSummary.get(segmentCode).setInfantCount(segmentSummary.get(segmentCode).getInfantCount() + 1);
				segmentSummary.get(segmentCode).setTotalCount(segmentSummary.get(segmentCode).getTotalCount() + 1);
				break;
			case UNKNOWN:
				segmentSummary.get(segmentCode).setUnknownCount(segmentSummary.get(segmentCode).getUnknownCount() + 1);
				segmentSummary.get(segmentCode).setTotalCount(segmentSummary.get(segmentCode).getTotalCount() + 1);
				break;
			}
			// This is to calculate total excess baggage weight
			segmentSummary.get(segmentCode).setTotalExcessBaggageWeight(
					segmentSummary.get(segmentCode).getTotalExcessBaggageWeight() + manifestRecord.getBaggageWeight());

		}

		manifestData.setSegmentSummaries(segmentSummary);
	}

	/**
	 * Sets COS wise summaries of passenger count
	 * 
	 * @param manifestRecords
	 * @param manifestData
	 */
	private static void setCOSwiseSummary(Collection<FlightManifestRecordDTO> manifestRecords, FlightManifestDTO manifestData) {

		Map<String, CabinClassSummaryDTO> cosWiseSummary = getCabinClassSummery(manifestRecords);

		String cabinClassCode;
		String logicalCCCode;

		Map<Integer, FlightManifestRecordDTO> parentManifestRecords = getParentManifestRecordMap(manifestRecords);

		for (FlightManifestRecordDTO manifestRecord : manifestRecords) {

			PaxType type = getPassengerCategory(manifestRecord);

			if (type.equals(PaxType.INFANT)) {
				FlightManifestRecordDTO parentManifest = parentManifestRecords.get(manifestRecord.getAdultPassengerID());
				if (parentManifest == null) {
					// This is to skip open return infants.
					// FIXME : Ideally this should not come to the result set.
					continue;
				}
				cabinClassCode = parentManifest.getCabinClassCode();
				logicalCCCode = parentManifest.getLogicalCCCode();
			} else {
				cabinClassCode = manifestRecord.getCabinClassCode();
				logicalCCCode = manifestRecord.getLogicalCCCode();
			}

			switch (type) {
			case MALE:
				cosWiseSummary.get(cabinClassCode).setMaleCount(cosWiseSummary.get(cabinClassCode).getMaleCount() + 1);
				cosWiseSummary.get(cabinClassCode).setTotalCount(cosWiseSummary.get(cabinClassCode).getTotalCount() + 1);
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setMaleCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getMaleCount() + 1);
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setTotalCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getTotalCount() + 1);
				}
				break;
			case FEMALE:
				cosWiseSummary.get(cabinClassCode).setFemaleCount(cosWiseSummary.get(cabinClassCode).getFemaleCount() + 1);
				cosWiseSummary.get(cabinClassCode).setTotalCount(cosWiseSummary.get(cabinClassCode).getTotalCount() + 1);
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setFemaleCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getFemaleCount() + 1);
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setTotalCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getTotalCount() + 1);
				}
				break;
			case CHILD:
				cosWiseSummary.get(cabinClassCode).setChildCount(cosWiseSummary.get(cabinClassCode).getChildCount() + 1);
				cosWiseSummary.get(cabinClassCode).setTotalCount(cosWiseSummary.get(cabinClassCode).getTotalCount() + 1);
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setChildCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getChildCount() + 1);
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setTotalCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getTotalCount() + 1);
				}
				break;
			case INFANT:
				cosWiseSummary.get(cabinClassCode).setInfantCount(cosWiseSummary.get(cabinClassCode).getInfantCount() + 1);
				cosWiseSummary.get(cabinClassCode).setTotalCount(cosWiseSummary.get(cabinClassCode).getTotalCount() + 1);
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setInfantCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getInfantCount() + 1);
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setTotalCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getTotalCount() + 1);
				}
				break;
			case UNKNOWN:
				cosWiseSummary.get(cabinClassCode).setUnknownCount(cosWiseSummary.get(cabinClassCode).getUnknownCount() + 1);
				cosWiseSummary.get(cabinClassCode).setTotalCount(cosWiseSummary.get(cabinClassCode).getTotalCount() + 1);
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setUnknownCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getUnknownCount() + 1);
					cosWiseSummary
							.get(cabinClassCode)
							.getLogicalCCWiseSummary()
							.get(logicalCCCode)
							.setTotalCount(
									cosWiseSummary.get(cabinClassCode).getLogicalCCWiseSummary().get(logicalCCCode)
											.getTotalCount() + 1);
				}
				break;
			}

		}
		manifestData.setCabinClassSummaries(cosWiseSummary);

	}

	/**
	 * Sets airport wise get in/out summaries
	 * 
	 * @param manifestRecords
	 * @param flightId
	 * @param manifestData
	 * @throws ModuleException
	 */
	private static void setAirportSummary(Collection<FlightManifestRecordDTO> manifestRecords, Integer flightId,
			FlightManifestDTO manifestData) throws ModuleException {

		List<String> airportSeq = ReservationModuleUtils.getFlightBD().getAirportSequenceListForFlight(flightId);

		Map<String, AirportSummaryDTO> airportSummary = new LinkedHashMap<String, AirportSummaryDTO>();

		for (String airport : airportSeq) {
			airportSummary.put(airport, new AirportSummaryDTO(airport));
		}

		for (FlightManifestRecordDTO manifestRecord : manifestRecords) {

			String[] airportCodes = manifestRecord.getSegmentCode().split("/");
			String origin = airportCodes[0];
			String dest = airportCodes[airportCodes.length - 1];

			if (airportSummary.containsKey(origin)) {
				airportSummary.get(origin).setGetInCount(airportSummary.get(origin).getGetInCount() + 1);
			} else {
				airportSummary.get(airportSeq.get(0)).setGetInCount(airportSummary.get(airportSeq.get(0)).getGetInCount() + 1);
			}

			if (airportSummary.containsKey(dest)) {
				airportSummary.get(dest).setGetDownCount(airportSummary.get(dest).getGetDownCount() + 1);
			} else {
				airportSummary.get(airportSeq.get(airportSeq.size() - 1)).setGetDownCount(
						airportSummary.get(airportSeq.get(airportSeq.size() - 1)).getGetDownCount() + 1);
			}

		}

		manifestData.setAirportSummaries(airportSummary);

	}

	/**
	 * Prepares and formats manifest data to display
	 * 
	 * @param manifestRecords
	 * @param manifestData
	 * @param userPrinicipal
	 * @throws ModuleException
	 */
	private static void formatAndSetDataForDisplay(Collection<FlightManifestRecordDTO> manifestRecords,
			FlightManifestDTO manifestData, UserPrincipal userPrinicipal) throws ModuleException {

		if (manifestRecords != null) {

			// Formatting data for to send to template engine
			Map<Integer, FlightManifestRecordDTO> parentManifestRecords = getParentManifestRecordMap(manifestRecords);

			for (FlightManifestRecordDTO finalManifestRecord : parentManifestRecords.values()) {

				DecimalFormat decFormat = new DecimalFormat("#,##0.00");
				if (finalManifestRecord.getBalance() != null) {

					finalManifestRecord.setFormatAmt(decFormat.format(finalManifestRecord.getBalance()));
				}

				finalManifestRecord.setFoidNumber(PlatformUtiltiies.nullHandler(finalManifestRecord.getFoidNumber()));
				finalManifestRecord.setPaxCategory(PlatformUtiltiies.nullHandler(finalManifestRecord.getPaxCategory()));
				finalManifestRecord.setNationality(PlatformUtiltiies.nullHandler(finalManifestRecord.getNationality()));
				finalManifestRecord.setCc4Digit(PlatformUtiltiies.nullHandler(BeanUtils.getLast4Digits(finalManifestRecord
						.getCc4Digit())));
				finalManifestRecord.setSeatCode(PlatformUtiltiies.nullHandler(finalManifestRecord.getSeatCode()));
				finalManifestRecord.setMealCode(PlatformUtiltiies.nullHandler(finalManifestRecord.getMealCode()));
				finalManifestRecord.setBaggageCode(PlatformUtiltiies.nullHandler(finalManifestRecord.getBaggageCode()));
				finalManifestRecord.setHalaServices(PlatformUtiltiies.nullHandler(finalManifestRecord.getHalaServices()));
				finalManifestRecord.setAutoCancelExpireTime(PlatformUtiltiies.nullHandler(finalManifestRecord
						.getAutoCancelExpireTime()));

				if (PlatformUtiltiies.nullHandler(finalManifestRecord.getEticket()) != "") {
					finalManifestRecord.setEticket(PlatformUtiltiies.nullHandler(AppSysParamsUtil.getEticketFormat()) + " "
							+ finalManifestRecord.getEticket());
				}

				finalManifestRecord.setSsrText(PlatformUtiltiies.nullHandler(finalManifestRecord.getSsrText()));

				if (ReservationStatus.ON_HOLD.equalsIgnoreCase(finalManifestRecord.getReservationStatus())) {

					Timestamp releaseTimeZulu = Timestamp.valueOf(finalManifestRecord.getReleaseTimeStamp());

					String releaseTimeAgentLocal = DateUtil.getAgentLocalTime((Date) releaseTimeZulu,
							userPrinicipal.getColUserDST(), userPrinicipal.getAgentStation(), "dd/MM/yyyy HH:mm:ss");
					finalManifestRecord.setReleaseTimeStamp(releaseTimeAgentLocal);

				} else {
					finalManifestRecord.setReleaseTimeStamp("");
				}

			}

			// Setting formatted manifest records(data is fetched into a new
			// arraylist as returning objects from .values() function is not
			// serializable)
			manifestData.setPaxRecords(new ArrayList<FlightManifestRecordDTO>(parentManifestRecords.values()));

		}
	}

	/**
	 * Returns map of parent data records
	 * 
	 * @param manifestRecords
	 * @return
	 */
	private static Map<Integer, FlightManifestRecordDTO> getParentManifestRecordMap(
			Collection<FlightManifestRecordDTO> manifestRecords) {

		// Filtering infants to adults

	///	Map<Integer, FlightManifestRecordDTO> infantManifestRecords = new LinkedHashMap<Integer, FlightManifestRecordDTO>();
		Map<Integer, FlightManifestRecordDTO> parentManifestRecords = new LinkedHashMap<Integer, FlightManifestRecordDTO>();

		for (FlightManifestRecordDTO manifestRecord : manifestRecords) {

			FlightManifestRecordDTO manifestRecordClone = (FlightManifestRecordDTO) manifestRecord.clone();
			// Adult passenger id null means, its an adult record
		//	if (manifestRecord.getAdultPassengerID() == null) {
				parentManifestRecords.put(manifestRecordClone.getPnrPaxId(), manifestRecordClone);
		//	} else {
		//		infantManifestRecords.put(manifestRecordClone.getPnrPaxId(), manifestRecordClone);
		//	}
		}

		// Attaching infants to adults - adult and attached infant will be
		// shown
		// as one record in the manifest (this is necessary only if there
		// are
		// infant records)

	/*	if (infantManifestRecords.size() > 0) {
			for (FlightManifestRecordDTO infantManifestRecord : infantManifestRecords.values()) {

				FlightManifestRecordDTO parentManifestRecord = parentManifestRecords.get(infantManifestRecord
						.getAdultPassengerID());

				if (parentManifestRecord != null) {

					parentManifestRecord.setPassengerName(parentManifestRecord.getPassengerName() + " <br> "
							+ infantManifestRecord.getPassengerName());
					parentManifestRecord.setSsr(PlatformUtiltiies.nullHandler(parentManifestRecord.getSsr()) + " <br> "
							+ PlatformUtiltiies.nullHandler(infantManifestRecord.getSsr()));

					String parentSsrText = PlatformUtiltiies.nullHandler(parentManifestRecord.getSsrText());
					String infantSsrText = PlatformUtiltiies.nullHandler(infantManifestRecord.getSsrText());

					if (!"".equals(parentSsrText) && !"".equals(infantSsrText)) {
						parentManifestRecord.setSsrText(parentSsrText + "/INF-" + infantSsrText);
					} else {
						parentManifestRecord.setSsrText(parentSsrText);
					}

					String parentETicketNo = PlatformUtiltiies.nullHandler(parentManifestRecord.getEticket());
					String infantETicketNo = PlatformUtiltiies.nullHandler(infantManifestRecord.getEticket());
					if (parentETicketNo.length() > 0 && infantETicketNo.length() > 0) {
						parentManifestRecord.setEticket(parentETicketNo + ", " + infantETicketNo);
					}
				}
			}
		} */

		return parentManifestRecords;

	}

	/**
	 * Outputs manifest HTML content from the template engine
	 * 
	 * @param manifestDataMap
	 * @return
	 * @throws ModuleException
	 */
	private static String getManifestContent(HashMap<String, Object> manifestDataMap) throws ModuleException {

		String templateName = ReservationModuleUtils.getAirReservationConfig().getFlightManifestPrintTemplate();

		StringWriter writer = new StringWriter();
		try {
			new TemplateEngine().writeTemplate(manifestDataMap, templateName, writer);
		} catch (Exception e) {
			throw new ModuleException("airreservations.itinerary.errorCreatingFromTemplate", e);
		}

		return writer.toString();

	}

	/**
	 * Returns passenger type for a given FlightManifestRecordDTO instance
	 * 
	 * @param manifestRecord
	 * @return
	 */
	private static PaxType getPassengerCategory(FlightManifestRecordDTO manifestRecord) {

		String passengerType = PlatformUtiltiies.nullHandler(manifestRecord.getPassengerType());
		String passengerTitle = PlatformUtiltiies.nullHandler(manifestRecord.getPassengerTitle());

		if (passengerType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
			if (passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MR)
					|| passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MASTER)) {

				return PaxType.MALE;
			} else if (passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MS)
					|| passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MRS)
					|| passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MISS)) {
				return PaxType.FEMALE;
			} else {
				return PaxType.UNKNOWN;
			}
		} else if (passengerType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
			return PaxType.CHILD;
		} else if (passengerType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
			return PaxType.INFANT;
		} else {
			return PaxType.UNKNOWN;
		}
	}

	/**
	 * Generates custom segment code for the given FlightManifestRecordDTO instance TODO: Make logic configurable
	 * 
	 * @param manifestRecord
	 * @return
	 */
	private static String generateSegmentCode(FlightManifestRecordDTO manifestRecord) {

		return manifestRecord.getOrigin().concat("/").concat(manifestRecord.getDestination());

	}

	/**
	 * Generates segment code list from the input records to be used in summarizing
	 * 
	 * @param manifestRecords
	 * @return
	 */
	private static Collection<String> getSegmentCodes(Collection<FlightManifestRecordDTO> manifestRecords) {

		List<String> segmentCodeList = new ArrayList<String>();
		String segmentCode;

		for (FlightManifestRecordDTO manifestRecord : manifestRecords) {

			segmentCode = generateSegmentCode(manifestRecord);

			if (!segmentCodeList.contains(segmentCode)) {
				segmentCodeList.add(segmentCode);
			}

		}

		return segmentCodeList;
	}

	/**
	 * Generates available cabin class/logical cabin class details
	 * 
	 * @param manifestRecords
	 * @return
	 */
	private static Map<String, CabinClassSummaryDTO> getCabinClassSummery(Collection<FlightManifestRecordDTO> manifestRecords) {

		Map<String, CabinClassSummaryDTO> cosWiseSummery = new HashMap<String, CabinClassSummaryDTO>();

		for (FlightManifestRecordDTO manifestRecord : manifestRecords) {
			if (manifestRecord.getCabinClassCode() != null) {
				if (!cosWiseSummery.containsKey(manifestRecord.getCabinClassCode())) {
					Map<String, LogicalCCSummaryDTO> logicalCCWiseSummary = new HashMap<String, LogicalCCSummaryDTO>();
					if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
						logicalCCWiseSummary.put(manifestRecord.getLogicalCCCode(),
								new LogicalCCSummaryDTO(manifestRecord.getLogicalCCName(), manifestRecord.getLogicalCCCode()));
					}
					cosWiseSummery.put(manifestRecord.getCabinClassCode(),
							new CabinClassSummaryDTO(manifestRecord.getCabinClassName(), manifestRecord.getCabinClassCode(),
									logicalCCWiseSummary));
				} else {
					if (AppSysParamsUtil.isLogicalCabinClassEnabled()
							&& !cosWiseSummery.get(manifestRecord.getCabinClassCode()).getLogicalCCWiseSummary()
									.containsKey(manifestRecord.getLogicalCCCode())) {
						cosWiseSummery
								.get(manifestRecord.getCabinClassCode())
								.getLogicalCCWiseSummary()
								.put(manifestRecord.getLogicalCCCode(),
										new LogicalCCSummaryDTO(manifestRecord.getLogicalCCName(), manifestRecord
												.getLogicalCCCode()));
					}
				}
			}
		}

		return cosWiseSummery;
	}

	/**
	 * Generates flight manifest legend
	 * 
	 * @return
	 */
	private static String getFlightManifestLegend() {

		String manifestLegend = "Passengers Carry [Standby *";

		if (AppSysParamsUtil.isOpenEndedReturnEnabled()) {
			manifestLegend += ",Open Return #]";
		} else {
			manifestLegend += "]";
		}
		manifestLegend += " with their booking class";
		if (AppSysParamsUtil.showPassportInItinerary()) {
			int count = 0;

			Collection<PaxCategoryTO> paxCategories = ReservationModuleUtils.getGlobalConfig().getPaxCategory();

			if (paxCategories != null) {

				for (PaxCategoryTO paxCategory : paxCategories) {
					if (count == 0) {
						manifestLegend += "/ Pax Identification (";
					} else {
						manifestLegend += ", ";
					}

					manifestLegend += paxCategory.getPaxCategoryCode() + " - " + paxCategory.getPaxCategoryDesc();
					count++;
				}
				if (count > 0) {
					manifestLegend += ")";
				}
			}
		}

		return manifestLegend;
	}

}
