package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVTrasmissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PNRGOVReservationLog;
import com.isa.thinair.airreservation.api.model.PNRGOVTxHistoryLog;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PnrGovDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * FocusSalesDAOImpl is the business DAO hibernate implementation
 * 
 * 
 * @isa.module.dao-impl dao-name="PnrGovDAO"
 */
public class PnrGovDAOImpl extends PlatformBaseHibernateDaoSupport implements PnrGovDAO {

	private static Log log = LogFactory.getLog(PnrGovDAOImpl.class);

	@Override
	public List<PNRGOVTrasmissionDetailsDTO> getFlightsForPnrGovScheduling(String airportCode, int timePeriod,
			int pnrGovTrasmissionGap, Date date, boolean isOutbound) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		Timestamp dateLowerLimit = new Timestamp(date.getTime() + ((timePeriod) * 3600 * 1000));
		Timestamp dateUpperLimit = new Timestamp(date.getTime() + ((24 + timePeriod + (pnrGovTrasmissionGap / 60)) * 3600 * 1000));
		Object params[] = { dateUpperLimit, dateLowerLimit, "Y" };
		final List<PNRGOVTrasmissionDetailsDTO> trasmitingFlightDetails = new ArrayList<PNRGOVTrasmissionDetailsDTO>();

		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT DISTINCT tfs.flt_seg_id as flt_seg_id,");
		sqlBuffer.append("tf.origin as origin, ");
		sqlBuffer.append("tf.destination as destination, ");
		sqlBuffer.append("tf.flight_number as flight_number, ");
		sqlBuffer.append("tfs.flight_id as flight_id, ");
		sqlBuffer.append("tfs.est_time_departure_zulu as est_time_departure_zulu ");
		sqlBuffer.append("FROM t_flight tf,");
		sqlBuffer.append("t_flight_segment tfs, ");
		sqlBuffer.append("t_airport ta ");
		sqlBuffer.append("WHERE tf.flight_id = tfs.flight_id ");
		if (isOutbound) {
			sqlBuffer.append("AND tfs.segment_code LIKE " + "'" + airportCode + "%' ");
			sqlBuffer.append("AND tf.destination = ta.airport_code ");
		} else {
			sqlBuffer.append("AND tfs.segment_code LIKE " + "'%" + airportCode + "' ");
			sqlBuffer.append("AND tf.origin = ta.airport_code ");
		}
		sqlBuffer.append("AND ta.is_surface_station='N' ");
		sqlBuffer.append("AND tfs.est_time_departure_zulu <=?");
		sqlBuffer.append(" AND tfs.est_time_departure_zulu  >?");
		sqlBuffer.append(" AND (tf.status                   ='ACT'");
		sqlBuffer.append(" OR tf.status                     ='CLS')");
		sqlBuffer.append(" AND tfs.segment_valid_flag       =?");
		sqlBuffer.append("ORDER BY tfs.flt_seg_id, ");
		sqlBuffer.append("tfs.flight_id,");
		sqlBuffer.append("tfs.est_time_departure_zulu");

		String query = sqlBuffer.toString();

		templete.query(query, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					PNRGOVTrasmissionDetailsDTO details = new PNRGOVTrasmissionDetailsDTO();
					details.setFltSegID(rs.getInt("flt_seg_id"));
					details.setDepartureAirport(rs.getString("origin"));
					details.setArrivalAirport(rs.getString("destination"));
					details.setFlightNo(rs.getString("flight_number"));
					details.setFlightID(rs.getInt("flight_id"));
					details.setDepartureTimeZulu(rs.getTimestamp("est_time_departure_zulu"));
					trasmitingFlightDetails.add(details);
				}
				return trasmitingFlightDetails;
			}
		});

		return trasmitingFlightDetails;

	}

	@Override
	public void saveOrUpdatePnrGovMessageHistoryLog(PNRGOVTxHistoryLog txHistory) {
		log.debug("######INSIDE SAVING MESSAGE HISTORY LOG########");
		hibernateSaveOrUpdate(txHistory);
	}

	@Override
	public boolean hasPnrGovMessageSent(int fltSegId, String countryCode, String airportCode, boolean isOutbound, int timePeriod) {
		StringBuffer sqlBuffer = new StringBuffer();

		sqlBuffer.append("SELECT pnrgov_tx_history_id ");
		sqlBuffer.append("FROM t_pnrgov_tx_history ");
		sqlBuffer.append("WHERE flt_seg_id    ='" + fltSegId + "' ");
		sqlBuffer.append("AND country_code    ='" + countryCode + "' ");
		sqlBuffer.append("AND airport_code    ='" + airportCode + "' ");
		if (isOutbound) {
			sqlBuffer.append("AND inbound_outbound='OUTBOUND' ");
		} else {
			sqlBuffer.append("AND inbound_outbound='INBOUND' ");
		}
		sqlBuffer.append("AND time_period = '" + String.valueOf(timePeriod) + "' ");

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object intObj = template.query(sqlBuffer.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int numOfRecs = 0;
		if (intObj != null) {
			numOfRecs = ((Integer) (intObj)).intValue();
		}

		if (numOfRecs >= 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public void saveOrUpdatePnrGovReservationLog(PNRGOVReservationLog resLog) {
		log.debug("######INSIDE SAVING MESSAGE HISTORY LOG########");
		hibernateSaveOrUpdate(resLog);
	}

	@Override
	public void saveOrUpdatePnrGovReservationLogList(List<PNRGOVReservationLog> resLogList) {
		log.debug("######INSIDE SAVING MESSAGE HISTORY LOG LIST########");
		hibernateSaveOrUpdateAll(resLogList);
	}

	@Override
	public Page getPNRGOVReports(int startrec, int numofrecs) throws ModuleException {
		List ob = new ArrayList();
		ob.add("pnrGovReportId");
		return getPagedData(null, startrec, numofrecs, PNRGOVTxHistoryLog.class, ob);
	}

	@Override
	public Page getPNRGOVReports(int startrec, int numofrecs, List criteria) throws ModuleException {
		return getPagedData(criteria, startrec, numofrecs, PNRGOVTxHistoryLog.class, null);
	}

	@Override
	public PNRGOVTxHistoryLog getHistoryLog(String logId) throws ModuleException {
		PNRGOVTxHistoryLog historyLog = null;
		List logList = null;
		logList = find(
				"select pnrGovHstLog from PNRGOVTxHistoryLog " + "as pnrGovHstLog where pnrGovHstLog.pnrGovTxHistoryID=?", logId, PNRGOVTxHistoryLog.class);
		if (logList != null && !logList.isEmpty()) {
			historyLog = (PNRGOVTxHistoryLog) logList.get(0);
		}
		return historyLog;
	}

	@Override
	public PNRGOVTxHistoryLog getAlreadyLoggedEntry(int fltSegId, String countryCode, String airportCode, String inboundOutbound,
			int timePeriod) throws ModuleException {
		PNRGOVTxHistoryLog historyLog = null;
		List logList = null;
		String hql = "select pnrGovHstLog from PNRGOVTxHistoryLog " + "as pnrGovHstLog where pnrGovHstLog.fltSegID = '"
				+ fltSegId + "' and pnrGovHstLog.countryCode = '" + countryCode + "' and pnrGovHstLog.airportCode = '"
				+ airportCode + "' and pnrGovHstLog.timePeriod = '" + timePeriod + "' and pnrGovHstLog.inboundOutbound = '"
				+ inboundOutbound + "'";
		logList = find(hql, PNRGOVTxHistoryLog.class);
		if (logList != null && !logList.isEmpty()) {
			historyLog = (PNRGOVTxHistoryLog) logList.get(0);
		}
		return historyLog;
	}
}
