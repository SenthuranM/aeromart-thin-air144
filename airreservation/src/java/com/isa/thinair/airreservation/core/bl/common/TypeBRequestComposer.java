package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.TypeBResponseDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.tty.TypeBReservationMessageCreator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRS;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class TypeBRequestComposer {

	private static Log log = LogFactory.getLog(TypeBRequestComposer.class);

	public static void sendTypeBRequest(TypeBRequestDTO typeBRequestDTO) {
		try {
			if (eligibleToSendTypeBSyncMessage(typeBRequestDTO)) {
				Reservation reservation = loadReservation(typeBRequestDTO.getReservation().getPnr());

				TypeBMessageCreatorFactory typeBMessageCreatorFactory = new TypeBReservationMessageCreatorFactory();
				TypeBReservationMessageCreator typeBReservationMessageCreator = typeBMessageCreatorFactory
						.createTypeBReservationMessageCreator(GDSInternalCodes.GDSNotifyAction
								.resolveGDSNotifyActionType(typeBRequestDTO.getGdsNotifyAction()));
				TypeBRQ typeBRQ = typeBReservationMessageCreator.createSyncRequest(reservation, typeBRequestDTO);

				ReservationModuleUtils.getTypeBServiceBD().composeAndSendTypeBMessage(typeBRQ);
			}
		} catch (Exception e) {
			log.error(" ERROR OCCURED WHILE SENDING TYPE B MESSAGES");
		}
	}

	public static void sendCSTypeBRequest(TypeBRequestDTO typeBRequestDTO, Set<String> csCarrierList) throws ModuleException {
		List<TypeBRS> typeBRSs = new ArrayList<TypeBRS>();
		TypeBResponseDTO summarizedTypeBRS = new TypeBResponseDTO();
		try {
			if (eligibleToSendTypeBCSMessage(typeBRequestDTO, csCarrierList)) {
				for (String csCarrier : csCarrierList) {
					Reservation reservation = loadReservation(typeBRequestDTO.getReservation().getPnr());
					typeBRequestDTO.setCsOCCarrierCode(csCarrier);

					TypeBMessageCreatorFactory typeBMessageCreatorFactory = new TypeBReservationMessageCreatorFactory();
					TypeBReservationMessageCreator typeBReservationMessageCreator = typeBMessageCreatorFactory
							.createTypeBReservationMessageCreator(GDSInternalCodes.GDSNotifyAction
									.resolveGDSNotifyActionType(typeBRequestDTO.getGdsNotifyAction()));
					TypeBRQ typeBRQ = typeBReservationMessageCreator.createCSRequest(reservation, typeBRequestDTO);

					if (typeBRQ.isMsgCreationSuccess()) {
						typeBRSs.add(ReservationModuleUtils.getTypeBServiceBD().composeAndSendTypeBMessage(typeBRQ));
					} else {
						summarizedTypeBRS.setSuccess(true);
					}
				}
				summarizedTypeBRS = createSummarizedTypeBRS(typeBRSs);
			} else {
				summarizedTypeBRS.setSuccess(true);
			}
		} catch (Exception e) {
			log.error(" ERROR OCCURED WHILE SENDING TYPE B MESSAGES");
		}
		handleCSTypebReqFailures(summarizedTypeBRS);
	}

	private static boolean eligibleToSendTypeBSyncMessage(TypeBRequestDTO typeBRequestDTO) {
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))
				&& typeBRequestDTO.getReservation().getGdsId() != null
				&& (typeBRequestDTO.getGdsNotifyAction() != GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode())) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean eligibleToSendTypeBCSMessage(TypeBRequestDTO typeBRequestDTO, Set<String> csCarrierList) {
		if (AppSysParamsUtil.isCodeShareEnable() && csCarrierList != null
				&& (typeBRequestDTO.getGdsNotifyAction() != GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode())) {
			return true;
		} else {
			return false;
		}
	}

	private static void handleCSTypebReqFailures(TypeBResponseDTO summarizedTypeBRS) throws ModuleException {
		if (summarizedTypeBRS.isSynchronousCommunication() && !summarizedTypeBRS.isSuccess()) {
			throw new ModuleException("airreservations.codeshare.process.failed");
		}
	}

	private static TypeBResponseDTO createSummarizedTypeBRS(List<TypeBRS> typeBRSs) {
		TypeBResponseDTO summarizedTypeBRS = new TypeBResponseDTO();
		if (!typeBRSs.isEmpty()) {
			summarizedTypeBRS.setSynchronousCommunication(true);
			summarizedTypeBRS.setSuccess(true);
			for (TypeBRS typeBRS : typeBRSs) {
				if (!typeBRS.isSynchronousCommunication()) {
					summarizedTypeBRS.setSynchronousCommunication(false);
				}
				if (typeBRS.getResponseStatus().equals(TypeBRS.ResponseStatus.FAILED)) {
					summarizedTypeBRS.setSuccess(false);
				}
			}
		}
		return summarizedTypeBRS;
	}

	private static Reservation loadReservation(String pnr) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		return ReservationProxy.getReservation(pnrModesDTO);
	}
}
