package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;

public class FareOndGroupTO {
	private Collection<Integer> pnrSegIds;
	private Collection<Integer> flightSegIds;
	private int fareOndGroup = -1;

	public FareOndGroupTO(int fareOndGroup) {
		this.fareOndGroup = fareOndGroup;
	}

	public Collection<Integer> getPnrSegIds() {
		if (pnrSegIds == null) {
			this.pnrSegIds = new ArrayList<Integer>();
		}
		return pnrSegIds;
	}

	public void setPnrSegIds(Collection<Integer> pnrSegIds) {
		this.pnrSegIds = pnrSegIds;
	}

	public Collection<Integer> getFlightSegIds() {
		if (flightSegIds == null) {
			this.flightSegIds = new ArrayList<Integer>();
		}
		return flightSegIds;
	}

	public void setFlightSegIds(Collection<Integer> flightSegIds) {
		this.flightSegIds = flightSegIds;
	}

	public int getFareOndGroup() {
		return fareOndGroup;
	}

}
