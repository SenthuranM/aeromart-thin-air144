package com.isa.thinair.airreservation.core.bl.tax;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;

public class TransactionSequencer {
	private OperationChgTnx currentChgTnx;
	private OperationPayTnx currentPayTnx;
	private final Log log = LogFactory.getLog(TransactionSequencer.class);
	private boolean isDirty = false;

	private TransactionSequence lastSettlementTnxSnapshot = null;
	private int tnxSequence = 1;
	private TransactionSequence lastDiffTnxSequence = null;

	public TransactionSequencer(int tnxSequence) {
		currentChgTnx = new OperationChgTnx(null, null);
		currentPayTnx = new OperationPayTnx(null, null);
		this.tnxSequence = tnxSequence;
	}

	public void addCharge(OperationChgTnx chgs) {
		if (chgs != null) {
			isDirty = true;
			for (ReservationPaxOndCharge charge : chgs.getChargeKeyWiseSummarizedCharges().values()) {
				currentChgTnx.addCharge(charge);
			}
		}

	}

	public void attempCreditPayment(Long tnxDate) {
		if (tnxDate != null) {
			addPayment(null, tnxDate);
		} else {
			throw new RuntimeException("Invalid payment");
		}

	}

	public void addPayment(OperationPayTnx payment) {
		if (payment != null) {
			addPayment(payment, null);
		} else {
			throw new RuntimeException("Invalid payment");
		}

	}

	private void addPayment(OperationPayTnx payment, Long tnxDate2) {
		if (isDirty) {
			if (payment != null || (lastSettlementTnxSnapshot != null && payment == null)) {
				if (payment != null) {
					currentPayTnx.addPaymentTnx(payment.getTotalPaymentTnx());
				}
				if (currentChgTnx.getTotalChargeAmount().compareTo(currentPayTnx.getTotalPayment()) <= 0) {

					Date tnxDate = null;
					if (payment != null) {
						tnxDate = payment.getTnxDate();
					} else {
						tnxDate = new Date(tnxDate2);
					}
					int nextTnxSequence = tnxSequence;
					OperationPayTnx payTnx = currentPayTnx.clone(tnxDate, (long) nextTnxSequence);
					TransactionSequence ts = new TransactionSequence(tnxDate, nextTnxSequence,
							currentChgTnx.clone(tnxDate, (long) nextTnxSequence), payTnx, payTnx);

					if (lastSettlementTnxSnapshot != null) {
						OperationChgTnx diffChg = ts.getChargeTnx().diffWith(lastSettlementTnxSnapshot.getChargeTnx());
						OperationPayTnx diffPay = ts.getPaymentTnx().diffWith(lastSettlementTnxSnapshot.getPaymentTnx(), tnxDate);
						OperationPayTnx snapshotPay = getSnapshotTnx(diffChg, tnxDate, diffPay.getKey());
						this.lastDiffTnxSequence = new TransactionSequence(tnxDate, nextTnxSequence, diffChg, diffPay,
								snapshotPay);
					} else {
						OperationPayTnx payTx = ts.getPaymentTnx().clone(tnxDate, (long) nextTnxSequence);
						this.lastDiffTnxSequence = new TransactionSequence(tnxDate, nextTnxSequence, ts.getChargeTnx().clone(),
								payTx, payTx);
					}
					lastSettlementTnxSnapshot = ts;
					log.debug("LASTDIFF|||" + lastDiffTnxSequence);
					log.debug("LASTSETT|||" + lastSettlementTnxSnapshot);
					isDirty = false;
					tnxSequence++;
				}
			}

		} else if (!payment.isDummyTnx() && !payment.isZeroPayment()) {
			throw new RuntimeException("Invalid payment");
		}
	}

	private OperationPayTnx getSnapshotTnx(OperationChgTnx diffChg, Date tnxDate, Long key) {
		OperationPayTnx snapshotPay = new OperationPayTnx(tnxDate, key);
		ReservationTnx paymenTnx = new ReservationTnx();
		paymenTnx.setAmount(diffChg.getTotalChargeAmount().negate());
		snapshotPay.addPaymentTnx(paymenTnx);
		return snapshotPay;
	}

	public boolean isChargesFullyPaid() {
		return !isDirty && currentChgTnx.getChargeList().size() > 0;
	}

	public OperationChgTnx getUnpaidChargeTnx() {
		if (!isChargesFullyPaid() && lastSettlementTnxSnapshot != null) {
			return currentChgTnx.clone().diffWith(lastSettlementTnxSnapshot.getChargeTnx());
		}
		return null;
	}

	public TransactionSequence getLastPaidTnxSequence() {
		return lastDiffTnxSequence;
	}
}
