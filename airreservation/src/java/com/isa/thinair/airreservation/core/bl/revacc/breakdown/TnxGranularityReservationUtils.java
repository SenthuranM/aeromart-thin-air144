package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Mediates with the Reservation object and provides the required data for AccelAero detail transactions.
 * 
 * @author Nilindra Fernando
 * @since August 5, 2010
 */
public class TnxGranularityReservationUtils {

	public static ReservationPaymentMetaTO getReservationPaymentMetaTO(Reservation reservation) throws ModuleException {

		ReservationPaymentMetaTO reservationPaymentMetaTO = new ReservationPaymentMetaTO();

		if (AppSysParamsUtil.isEnableTransactionGranularity() && reservation.isEnableTransactionGranularity()) {
			Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapPerPaxWiseOndPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getPerPaxWiseOndChargeIds(reservation.getPnrPaxIds());

			for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
				Integer pnrPaxId = reservationPax.getPnrPaxId();

				if (!reservation.isInfantPaymentRecordedWithInfant() && ReservationApiUtils.isInfantType(reservationPax)) {
					pnrPaxId = reservationPax.getAccompaniedPaxId();
				}				
				ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo()
						.get(pnrPaxId);

				if (reservationPaxPaymentMetaTO == null) {
					reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
					reservationPaxPaymentMetaTO.setPnrPaxId(pnrPaxId);
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(getColReservationPaxOndCharge(reservationPax,
							mapPerPaxWiseOndPayments.get(pnrPaxId)));
					reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().put(pnrPaxId, reservationPaxPaymentMetaTO);
				} else {
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(getColReservationPaxOndCharge(reservationPax,
							mapPerPaxWiseOndPayments.get(pnrPaxId)));
				}
			}
		} else {
			Map<Integer, Collection<ReservationTnx>> trnsMap = ReservationBO.getPNRPaxPaymentsAndRefunds(
					reservation.getPnrPaxIds(), false, false);
			for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
				Integer pnrPaxId = reservationPax.getPnrPaxId();

				if (ReservationApiUtils.isInfantType(reservationPax)) {
					pnrPaxId = reservationPax.getAccompaniedPaxId();
				}

				ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo()
						.get(pnrPaxId);

				if (reservationPaxPaymentMetaTO == null) {
					reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
					reservationPaxPaymentMetaTO.setPnrPaxId(pnrPaxId);
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(getAllColReservationPaxOndCharges(reservationPax,
							trnsMap.get(pnrPaxId)));
					reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().put(pnrPaxId, reservationPaxPaymentMetaTO);
				} else {
					reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(getAllColReservationPaxOndCharges(reservationPax,
							trnsMap.get(pnrPaxId)));
				}
			}
		}

		return reservationPaymentMetaTO;
	}

	private static Collection<ReservationPaxOndCharge> getAllColReservationPaxOndCharges(ReservationPax reservationPax,
			Collection<ReservationTnx> colReservationTnx) {

		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();

		if (colReservationTnx != null && colReservationTnx.size() > 0) {
		} else {
			for (ReservationPaxFare reservationPaxFare : (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
				for (ReservationPaxOndCharge reservationPaxOndCharge : (Collection<ReservationPaxOndCharge>) reservationPaxFare
						.getCharges()) {
					colReservationPaxOndCharge.add(reservationPaxOndCharge);
				}
			}
		}

		return colReservationPaxOndCharge;
	}

	private static Collection<ReservationPaxOndCharge> getColReservationPaxOndCharge(ReservationPax reservationPax,
			Map<Long, Collection<ReservationPaxOndPayment>> ondChargeIdsWiseAmounts) {

		Collection<Long> ondChargesIds = new HashSet<Long>();

		if (ondChargeIdsWiseAmounts != null) {
			ondChargesIds.addAll(ondChargeIdsWiseAmounts.keySet());
		}

		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();
		AmountProratorBO amountProratorBO = new AmountProratorBO();

		for (ReservationPaxFare reservationPaxFare : (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
			TnxGranularityChargesEliminator tnxGranularityChargesEliminator = new TnxGranularityChargesEliminator(
					reservationPaxFare, ondChargesIds);

			Collection<ReservationPaxOndCharge> applicableCharges = tnxGranularityChargesEliminator.execute();

			for (ReservationPaxOndCharge reservationPaxOndCharge : (Collection<ReservationPaxOndCharge>) applicableCharges) {
				// < 0 will not be taken, since that will be totally taken care from the cancellation flows.

				if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() > 0) {

					if (ondChargesIds.size() == 0) {
						colReservationPaxOndCharge.add(reservationPaxOndCharge);
					} else {
						if (!ondChargesIds.contains(Long.valueOf(reservationPaxOndCharge.getPnrPaxOndChgId()))) {
							colReservationPaxOndCharge.add(reservationPaxOndCharge);
						} else {
							Collection<ReservationPaxOndPayment> colDifferenceReservationPaxOndPayments = ondChargeIdsWiseAmounts
									.get(Long.valueOf(reservationPaxOndCharge.getPnrPaxOndChgId()));

							BigDecimal amount = TnxGranularityUtils.getTotalAmount(colDifferenceReservationPaxOndPayments);

							if (amount.doubleValue() != 0) {
								// BigDecimal difference =
								// AccelAeroCalculator.subtract(reservationPaxOndCharge.getAmount(), amount);
								BigDecimal difference = AccelAeroCalculator.subtract(
										reservationPaxOndCharge.getEffectiveAmount(), amount);

								if (difference.doubleValue() > 0) {
									ReservationPaxOndCharge clonedReservationPaxOndCharge = reservationPaxOndCharge
											.cloneForShallowCopy();
									clonedReservationPaxOndCharge.setAmount(difference);
									// when calculating difference, Discount & Adjustment amount were considered by
									// getEffectiveAmount(). Therefore these amounts are no need to set again and should
									// be ignored.
									clonedReservationPaxOndCharge.setDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
									clonedReservationPaxOndCharge.setAdjustment(AccelAeroCalculator.getDefaultBigDecimalZero());

									amountProratorBO.addPlusDifference(clonedReservationPaxOndCharge,
											colDifferenceReservationPaxOndPayments);
									// This means some one did change the charges. At the time of development
									// This could be via RES_34 or RES_38
								} else if (difference.doubleValue() < 0) {
									ReservationPaxOndCharge clonedReservationPaxOndCharge = reservationPaxOndCharge
											.cloneForShallowCopy();
									clonedReservationPaxOndCharge.setAmount(difference);
									
									// when calculating difference, Discount & Adjustment amount were considered by
									// getEffectiveAmount(). Therefore these amounts are no need to set again and should
									// be ignored.
									clonedReservationPaxOndCharge.setDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
									clonedReservationPaxOndCharge.setAdjustment(AccelAeroCalculator.getDefaultBigDecimalZero());

									amountProratorBO.addMinusDifference(clonedReservationPaxOndCharge,
											colDifferenceReservationPaxOndPayments);
								}
							}

						}
					}
				}
			}
		}

		amountProratorBO.execute(colReservationPaxOndCharge);
		return colReservationPaxOndCharge;
	}

	public static ReservationPaxPaymentMetaTO getReservationPaymentMetaTOForTransferInfants(Integer pnrPaxId,
			ReservationPax reservationPax, boolean enableTransactionGranularity) throws ModuleException {

		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
		reservationPaxPaymentMetaTO.setPnrPaxId(pnrPaxId);

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			for (ReservationPaxFare reservationPaxFare : (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
				for (ReservationPaxOndCharge reservationPaxOndCharge : (Collection<ReservationPaxOndCharge>) reservationPaxFare
						.getCharges()) {
					// We track only non zero charges
					if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() != 0) {
						reservationPaxOndCharge = reservationPaxOndCharge.cloneForShallowCopy();
						reservationPaxOndCharge.setAmount(reservationPaxOndCharge.getAmount().abs().negate());
						reservationPaxPaymentMetaTO.addMapPerPaxWiseOndRefundableCharge(
								new Long(reservationPaxOndCharge.getPnrPaxOndChgId()), reservationPaxOndCharge);
					}
				}
			}
		}

		return reservationPaxPaymentMetaTO;
	}

	public static ReservationPaxPaymentMetaTO getReservationPaymentMetaTOForCreditsCarryForward(Integer pnrPaxId,
			ReservationPaxPaymentMetaTO oldReservationPaxPaymentMetaTO, boolean enableTransactionGranularity) {

		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
		reservationPaxPaymentMetaTO.setPnrPaxId(pnrPaxId);

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			for (ReservationPaxOndCharge reservationPaxOndCharge : oldReservationPaxPaymentMetaTO
					.getMapPerPaxWiseOndRefundableCharges().values()) {
				reservationPaxOndCharge = reservationPaxOndCharge.cloneForShallowCopy();
				reservationPaxOndCharge.setAmount(reservationPaxOndCharge.getAmount().abs());
				reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharge(reservationPaxOndCharge);
			}
		}

		return reservationPaxPaymentMetaTO;
	}
	
}
