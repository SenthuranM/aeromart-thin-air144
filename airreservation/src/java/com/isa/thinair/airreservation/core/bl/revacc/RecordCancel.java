/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to record cancel
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="recordCancel"
 */
public class RecordCancel extends DefaultBaseCommand {

	// Dao's
	private ReservationTnxDAO reservationTnxDao;

	/**
	 * constructor of the RecordCancel command
	 */
	public RecordCancel() {

		// looking up daos
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;

	}

	/**
	 * execute method of the RecordCancel command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal creditTotal = (BigDecimal) this.getParameter(CommandParamNames.CREDIT_TOTAL);
		BigDecimal chargeAmount = (BigDecimal) this.getParameter(CommandParamNames.CHARGE_AMOUNT);
		Integer actionNC = (Integer) this.getParameter(CommandParamNames.ACTION_NC);
		Integer chargeNC = (Integer) this.getParameter(CommandParamNames.CHARGE_NC);
		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = (ReservationPaxPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		boolean enableTransactionGranularity = (Boolean) this
				.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);
		BigDecimal penaltyAmount = this.getParameter(CommandParamNames.PENALTY_AMOUNT, BigDecimal.class);

		this.checkParams(pnrPaxId, creditTotal, chargeAmount, actionNC, chargeNC, credentialsDTO);

		ReservationTnx refundableReservationTnx = null;
		ReservationTnx cancellationReservationTnx = null;

		Integer transactionSeq = retrieveCorrectTransactionSeq(pnrPaxId);

		// Adding the Credit Amount
		if (creditTotal.doubleValue() != 0) {
			if (creditTotal.doubleValue() > 0) {
				refundableReservationTnx = TnxFactory.getCreditInstance(pnrPaxId, creditTotal.abs(),
						actionNC.intValue(), credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null, null,
						null, false, true);
			} else {
				refundableReservationTnx = TnxFactory.getDebitInstance(pnrPaxId, creditTotal.abs(), actionNC.intValue(),
						credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
			}
			reservationTnxDao.saveTransaction(refundableReservationTnx);
		}

		TnxGranularityFactory.saveReservationPaxTnxBreakdownForRefundables(pnrPaxId, actionNC,
				reservationPaxPaymentMetaTO, enableTransactionGranularity, transactionSeq);

		// Adding the Charge Amount
		if (chargeAmount.doubleValue() != 0) {
			cancellationReservationTnx = TnxFactory.getDebitInstance(pnrPaxId, chargeAmount, chargeNC.intValue(),
					credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
			reservationTnxDao.saveTransaction(cancellationReservationTnx);
		}

		if (penaltyAmount != null && penaltyAmount.compareTo(BigDecimal.ZERO) > 0) {

			ReservationTnx penaltyReservationTnx = TnxFactory.getDebitInstance(pnrPaxId, penaltyAmount,
					ReservationTnxNominalCode.PENALTY_CHARGE.getCode(), credentialsDTO,
					CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
			reservationTnxDao.saveTransaction(penaltyReservationTnx);
		}

		TnxGranularityFactory.saveReservationPaxTnxBrkForCreditsComponsatingNewCharges(pnrPaxId,
				ReservationTnxNominalCode.CREDIT_BF, reservationPaxPaymentMetaTO, enableTransactionGranularity,
				transactionSeq);

		return new DefaultServiceResponse(true);
	}

	private Integer retrieveCorrectTransactionSeq(String pnrPaxId) {
		Integer maxTxnSeq = 0;
		try {
			if (pnrPaxId != null) {
				maxTxnSeq = ReservationModuleUtils.getRevenueAccountBD()
						.getPNRPaxMaxTransactionSeq(Integer.parseInt(pnrPaxId));
			}
		} catch (ModuleException e) {
			maxTxnSeq = 1;
		}
		return maxTxnSeq;
	}

	/**
	 * Privte method to validate parameters
	 * 
	 * @param credentialsDTO
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxId, BigDecimal segmentTotal, BigDecimal chargeAmount, Integer actionNC,
			Integer chargeNC, CredentialsDTO credentialsDTO) throws ModuleException {

		if (pnrPaxId == null || segmentTotal == null || chargeAmount == null || actionNC == null || chargeNC == null
				|| credentialsDTO == null)// throw exception
			throw new ModuleException("airreservations.arg.invalid.null");
	}

}
