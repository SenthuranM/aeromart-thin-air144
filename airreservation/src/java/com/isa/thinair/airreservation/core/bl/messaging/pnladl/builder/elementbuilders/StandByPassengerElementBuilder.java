/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.MarketingFlightElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.StandByElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class StandByPassengerElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private PassengerInformation passengerInformation;
	private boolean isStandbyPassenger;

	private BaseRuleExecutor<RulesDataContext> standByElementRuleExecutor = new StandByElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;

		initContextData(context);
		standbyPassengerElementTemplate();
		response = validateSubElement(currentElement);
		ammendMessageDataAccordingTo(response);
	}

	private void standbyPassengerElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.ID2N2);
		currentElement = elementTemplate.toString();
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformation = getFirstPassengerInUtilizedList();
		isStandbyPassenger = isStandbyPassenger(getFirstPassengerInUtilizedList());
	}
	
	private boolean isStandbyPassenger(PassengerInformation passengerInformation){
		return passengerInformation.isStandByPassenger();
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (currentElement != null && !currentElement.isEmpty() && isStandbyPassenger) {
			if (response.isProceedNextElement()) {
				executeSpaceElementBuilder(uPContext);
			} else {
				executeConcatenationElementBuilder(uPContext);
			}
			ammendToBaseLine(currentElement, currentLine, messageLine);
		}
		executeNext();
	}

	private PassengerInformation getFirstPassengerInUtilizedList() {
		PassengerInformation passengerInformation = null;
		int index = 0;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformation = uPContext.getUtilizedPassengers().get(index);
		}
		return passengerInformation;
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = standByElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
