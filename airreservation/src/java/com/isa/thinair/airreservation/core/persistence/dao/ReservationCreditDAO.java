/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditUsageDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxPaymentCredit;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * ReservationCreditDAO is the business DAO interface for the reservation credit apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationCreditDAO {
	/**
	 * Save reservation credit
	 * 
	 * @param reservationCredit
	 */
	public void saveReservationCredit(ReservationCredit reservationCredit);

	/**
	 * Save all reservation credits
	 * 
	 * @param colReservationCredit
	 */
	public void saveReservationCredit(Collection<ReservationCredit> colReservationCredit);

	/**
	 * Return reservation credit
	 * 
	 * @param creditId
	 * @return
	 */
	public ReservationCredit getReservationCredit(long creditId);

	/**
	 * Return sum of reservation credits
	 * 
	 * @param pnrPaxId
	 * @return
	 */
	public BigDecimal getSumOfReservationCredits(String pnrPaxId);

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 */
	public Collection<PaxCreditDTO> getReservationCredits(int customerId);

	/**
	 * Return Pax Credit for List of PNRs
	 * 
	 * @param pnrList
	 * @return
	 */
	public Collection<PaxCreditDTO> getPaxCreditForPnrs(List<String> pnrList);

	/**
	 * Return available Credit
	 * 
	 * @param pnrPaxIds
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Map<Integer, Collection<CreditInfoDTO>> getAvailableCredit(Collection<Integer> pnrPaxIds) throws CommonsDataAccessException;

	/**
	 * Return Acquired Credit
	 * 
	 * @param pnrPaxIds
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Map<Integer, Collection<CreditInfoDTO>> getAcquiredCredit(Collection<Integer> pnrpaxIds) throws CommonsDataAccessException;

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 */
	public Collection<LoyaltyCreditDTO> getMashreqCredits(int customerId);

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 */
	public Collection<LoyaltyCreditUsageDTO> getMashreqCreditUsage(int customerId);

	/**
	 * Return reservation credits
	 * 
	 * @param pnrPaxId
	 * @param isAssending
	 * @return
	 */
	public Collection<ReservationCredit> getReservationCreditsOrderByExpiryDate(String pnrPaxId, boolean isAssending);

	public Collection<ReservationCredit> getReservationCredits(String pnrPaxId, Collection<Integer> tnxIds);
	
	public void deleteInvalidPaxPaymentCredits();
	
	public Collection<ReservationPaxPaymentCredit> getReservationPaxPaymentCredit(Long paymentTnxId);
}