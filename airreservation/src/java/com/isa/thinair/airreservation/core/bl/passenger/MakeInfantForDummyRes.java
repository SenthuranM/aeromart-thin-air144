package com.isa.thinair.airreservation.core.bl.passenger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Add infant for dummy reservation
 * 
 * @author malaka
 * @isa.module.command name="makeInfantForDummyReservation"
 */
public class MakeInfantForDummyRes extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(MakeInfantForDummyRes.class);

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {

		log.debug("Inside execute");
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Collection<ReservationPax> colReservationPax = (Collection<ReservationPax>) this.getParameter(CommandParamNames.INFANTS);

		// Checking params
		this.validateParams(reservation, colReservationPax);

		int totalPaxInfantCount = 0;
		for (ReservationPax infant : colReservationPax) {
			reservation.addPassenger(infant);
			totalPaxInfantCount++;
		}

		Map<Integer, ReservationPax> allPassengersMap = new HashMap<Integer, ReservationPax>();
		for (ReservationPax pax : (Collection<ReservationPax>) reservation.getPassengers()) {
			allPassengersMap.put(pax.getPaxSequence(), pax);
		}

		trackParentInfantMappings(reservation, allPassengersMap);

		// Adding it to the main reservation
		reservation.setTotalPaxInfantCount(reservation.getTotalPaxInfantCount() + totalPaxInfantCount);

		changeInfantStates(reservation);

		// Saves the reservation
		ReservationProxy.saveReservation(reservation);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.VERSION, Long.valueOf(reservation.getVersion()));
		log.debug("Exit execute");

		return response;
	}

	private void validateParams(Reservation reservation, Collection<ReservationPax> colReservationPax) throws ModuleException {

		if (reservation == null || colReservationPax == null || colReservationPax.isEmpty()) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}

	// Sets the parent infant mappings for the dummy reservation.
	private void trackParentInfantMappings(Reservation reservation, Map<Integer, ReservationPax> allPassengers) {
		if (reservation != null && allPassengers != null && allPassengers.size() > 0) {
			// if the reservation is a dummy one we need to add the parent infant relationships correctly.
			if (ReservationInternalConstants.DummyBooking.YES == reservation.getDummyBooking()) {
				ReservationPax mappingPax;
				Integer parentOrInfantId;

				for (ReservationPax currentPax : (Collection<ReservationPax>) reservation.getPassengers()) {
					// Carrying out the mapping
					// Parent
					if (ReservationApiUtils.isParentBeforeSave(currentPax)) {
						parentOrInfantId = currentPax.getIndexId();
						if (parentOrInfantId != null) {
							mappingPax = (ReservationPax) allPassengers.get(parentOrInfantId);
							currentPax.addInfants(mappingPax);
						}
					}
					// Infant
					else if (ReservationApiUtils.isInfantType(currentPax)) {
						if (currentPax.getParent() == null) {
							parentOrInfantId = currentPax.getIndexId();
							if (parentOrInfantId != null) {
								mappingPax = (ReservationPax) allPassengers.get(parentOrInfantId);
								currentPax.addParent(mappingPax);
							} else {
								log.error("No parent sequence linked for infant");
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Change infant states
	 * 
	 * @param reservation
	 *            Modified to set CANCEL state as well, this case will only get fired for interline reservations.
	 */
	private void changeInfantStates(Reservation reservation) {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())) {
				if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
					reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED);
				} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
					reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD);
				} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
					reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CANCEL);
				}
			}
		}
	}

}
