package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airreservation.api.model.TermsTemplate;
import com.isa.thinair.airreservation.api.model.TermsTemplateAudit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.TermsTemplateSearchDTO;

/**
 * The interface defining data access operations related to {@link TermsTemplate} and it's auditing mechanism.
 * 
 * @author thihara
 * 
 */
public interface TermsTemplateDAO {

	/**
	 * Retrieves all of the terms template data.
	 * 
	 * @return Collection of {@link TermsTemplate}
	 */
	public List<TermsTemplate> getAllTermsTemplates();

	/**
	 * Updates a terms template and the audit data.
	 * 
	 * @param termsTemplate
	 *            New data to be updated.
	 * @param termsTemplateAudit
	 *            Audit data to be created with the update.
	 */
	public void updateTermsTemplate(TermsTemplate termsTemplate, TermsTemplateAudit termsTemplateAudit);

	/**
	 * 
	 * Searches terms templates according to the provided criteria and returns results in a paginated context.
	 * 
	 * @param searchCriteria
	 *            Search parameters to run the query.
	 * @param start
	 *            Result set's start position.
	 * @param recSize
	 *            Size of the results to be returned.
	 * @return A {@link Page} encapsulating the result data in a paginated context.
	 */
	public Page<TermsTemplate> getTermsTemplatePage(TermsTemplateSearchDTO searchCriteria, Integer start, Integer recSize);
}
