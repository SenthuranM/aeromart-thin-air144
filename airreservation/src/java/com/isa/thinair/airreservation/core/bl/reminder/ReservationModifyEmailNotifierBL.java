package com.isa.thinair.airreservation.core.bl.reminder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationModifiedEmailInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ResModifyEmailAgentModificationConst;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

public class ReservationModifyEmailNotifierBL {

	private MessagingServiceBD messagingServiceBD;
	private Log log = LogFactory.getLog(ReservationModifyEmailNotifierBL.class);
	private String DATE_TIME_FORMAT = "dd MMM yyyy HH:mm";

	public ReservationModifyEmailNotifierBL() {
		this.messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
	}

	public void sendNotificationsToAgent(CredentialsDTO credentialsDTO, Collection<String> pnrCollection, String agentCode,
			String modificationType, Map<String, Map<String, Date>> onHoldTimeMap) throws ModuleException {

		Agent agent = ReservationModuleUtils.getTravelAgentBD().getAgent(agentCode);

		if (agent != null) {

			String agentEmail = BeanUtils.nullHandler(agent.getEmailId());

			String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();

			if (agentEmail != null && agentEmail.length() > 0 && agentEmail.indexOf("@") != -1) {
				UserMessaging user = new UserMessaging();
				user.setToAddres(agentEmail);

				List<UserMessaging> messageList = new ArrayList<UserMessaging>();
				messageList.add(user);

				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messageList);

				Topic topic = new Topic();
				HashMap<String, Object> map = new HashMap<String, Object>();

				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.RESERVATION_MODIFIED_EMAIL_AGENT);

				map.put("user", BeanUtils.nullHandler(agent.getAgentName()));
				map.put("companyAddress", ReservationApiUtils.getCompanyAddressInformation(defaultCarrier));
				map.put("ibeURL", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
				map.put("phone", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
				map.put("agentAlertInfoDTO",
						getAgentReservationModifiedInfoDTO(credentialsDTO, pnrCollection, modificationType, onHoldTimeMap));
				map.put("logoImageName", getLogoImageName(defaultCarrier));

				topic.setTopicParams(map);
				topic.setAttachMessageBody(false);

				List<ReservationAudit> audits = new ArrayList<ReservationAudit>();
				Iterator<String> pnrItr = pnrCollection.iterator();
				while (pnrItr.hasNext()) {
					String pnr = pnrItr.next();

					ReservationAudit reservationAudit = new ReservationAudit();
					reservationAudit.setModificationType(AuditTemplateEnum.RESERVATION_MODIFIED_EMAIL_AGENT.getCode());
					ReservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.EMAIL_ADDRESS,
							agentEmail);

					if (ReservationInternalConstants.ResModifyEmailAgentModificationType.CANCEL_RESERVATION
							.equals(modificationType)) {
						reservationAudit.addContentMap(
								AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.ADDITIONAL_INFO,
								"Reservation cancelled by " + credentialsDTO.getUserId());
					} else if (ReservationInternalConstants.ResModifyEmailAgentModificationType.EXT_ON_HOLD
							.equals(modificationType)) {
						Date currReleaseTime = null;
						Date extendedReleaseTime = null;

						if (onHoldTimeMap != null) {
							Map<String, Date> pnrExtOnHoldTimeMap = onHoldTimeMap.get(pnr);

							currReleaseTime = pnrExtOnHoldTimeMap.get(ResModifyEmailAgentModificationConst.OLD_OHD_TIME);
							extendedReleaseTime = pnrExtOnHoldTimeMap.get(ResModifyEmailAgentModificationConst.EXT_OHD_TIME);

						}

						reservationAudit.addContentMap(
								AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.ADDITIONAL_INFO,
								"On Hold extended from: " + BeanUtils.parseDateFormat(currReleaseTime, DATE_TIME_FORMAT) + " "
										+ ReservationTemplateUtils.ZULU_TIME + " to: "
										+ BeanUtils.parseDateFormat(extendedReleaseTime, DATE_TIME_FORMAT) + " "
										+ ReservationTemplateUtils.ZULU_TIME);
					}

					audits.add(reservationAudit);
				}

				topic.setAuditInfo(audits);

				messageProfile.setTopic(topic);

				messagingServiceBD.sendMessage(messageProfile);
			}
		}

	}

	private Collection<ReservationModifiedEmailInfoDTO> getAgentReservationModifiedInfoDTO(CredentialsDTO credentialsDTO,
			Collection<String> pnrCollection, String modificationType, Map<String, Map<String, Date>> onHoldTimeMap) {
		Collection<ReservationModifiedEmailInfoDTO> resModifiedEmailInfoDTOs = new ArrayList<ReservationModifiedEmailInfoDTO>();

		Iterator<String> pnrItr = pnrCollection.iterator();
		while (pnrItr.hasNext()) {
			String pnr = pnrItr.next();
			ReservationModifiedEmailInfoDTO resModifiedEmailInfoDTO = new ReservationModifiedEmailInfoDTO();
			resModifiedEmailInfoDTO.setPnr(pnr);
			resModifiedEmailInfoDTO.setModifiedBy(credentialsDTO.getUserId());
			resModifiedEmailInfoDTO.setModifiedDate(new Date());
			resModifiedEmailInfoDTO.setModificationType(modificationType);

			if (ReservationInternalConstants.ResModifyEmailAgentModificationType.CANCEL_RESERVATION.equals(modificationType)) {
				resModifiedEmailInfoDTO.setDescription("Reservation Cancelled");
			} else if (ReservationInternalConstants.ResModifyEmailAgentModificationType.EXT_ON_HOLD.equals(modificationType)) {
				Date currReleaseTime = null;
				Date extendedReleaseTime = null;

				if (onHoldTimeMap != null) {
					Map<String, Date> pnrExtOnHoldTimeMap = onHoldTimeMap.get(pnr);

					currReleaseTime = pnrExtOnHoldTimeMap.get(ResModifyEmailAgentModificationConst.OLD_OHD_TIME);
					extendedReleaseTime = pnrExtOnHoldTimeMap.get(ResModifyEmailAgentModificationConst.EXT_OHD_TIME);

				}
				resModifiedEmailInfoDTO.setOldReleaseOnHoldDate(currReleaseTime);
				resModifiedEmailInfoDTO.setNewReleaseOnHoldDate(extendedReleaseTime);
				resModifiedEmailInfoDTO.setDescription("On Hold extended from: "
						+ BeanUtils.parseDateFormat(currReleaseTime, DATE_TIME_FORMAT) + " " + ReservationTemplateUtils.ZULU_TIME
						+ " to: " + BeanUtils.parseDateFormat(extendedReleaseTime, DATE_TIME_FORMAT) + " "
						+ ReservationTemplateUtils.ZULU_TIME);
			}
			else if(ReservationInternalConstants.ResModifyEmailAgentModificationType.CNF_WAITLISTED.equals(modificationType)){
				resModifiedEmailInfoDTO.setDescription("Wait Listed Reservation Confirmed");
			}

			resModifiedEmailInfoDTOs.add(resModifiedEmailInfoDTO);
		}

		return resModifiedEmailInfoDTOs;
	}

	/**
	 * Returns the logo image name
	 * 
	 * @param carrierCode
	 * @return
	 */
	private String getLogoImageName(String carrierCode) {
		String logoImageName = "LogoAni.gif";
		if (carrierCode != null) {
			logoImageName = "LogoAni" + carrierCode + ".gif";
		}
		return StaticFileNameUtil.getCorrected(logoImageName);
	}
}
