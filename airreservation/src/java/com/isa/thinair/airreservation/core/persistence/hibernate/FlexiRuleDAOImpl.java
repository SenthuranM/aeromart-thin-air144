package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.FlexiRuleDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author Charith Paranaliyanage
 * @isa.module.dao-impl dao-name="FlexiRuleDAO"
 */

public class FlexiRuleDAOImpl extends PlatformBaseHibernateDaoSupport implements FlexiRuleDAO {

	private static Log log = LogFactory.getLog(FlexiRuleDAOImpl.class);

	public void saveOrUpdate(Collection<ReservationPaxOndFlexibility> paxOndFlexibilities) {
		super.hibernateSaveOrUpdateAll(paxOndFlexibilities);
	}

	public Collection<ReservationPaxOndFlexibility> getPaxOndFlexibilities(int ppfId) {
		return find("from ReservationPaxOndFlexibility ondflx where ondflx.ppfId = ? order by ondflx.pnrPaxOndFlexiId asc",
				ppfId, ReservationPaxOndFlexibility.class);
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> getReservationPaxOndFlexibilities(
			Collection<Integer> collPpfIds) {

		if(collPpfIds.size() <= 0){
			return new HashMap<Integer, Collection<ReservationPaxOndFlexibilityDTO>>();
		}

		if (log.isDebugEnabled())
			log.debug("Inside getReservationPaxOndFlexibilities");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql2 = "SELECT DISTINCT ppof.ppondflx_id,  frr.flexi_rule_id,  ppof.ppf_id,  ppof.flexi_rule_rate_id,  "
				+ "ppof.flexi_rule_flexi_type_id,  ppof.available_count,  ppof.utilized_count,  ppof.status,  frf.cut_over_mins,"
				+ "  flxtype.description,  est_time_departure_zulu FROM t_pnr_pax_ond_flexibility ppof,  t_flexi_rule_rate frr, "
				+ " t_flexi_rule_flexibility frf,  t_flexi_rule_flexi_type flxtype,  t_pnr_pax_fare_segment ppfs,   "
				+ " t_pnr_segment ps,  t_flight_segment fs WHERE ppof.ppf_id in (" + Util.constructINStringForInts(collPpfIds)
				+ ") and ppof.ppf_id = ppfs.ppf_id  and ppfs.PNR_SEG_ID = ps.PNR_SEG_ID  and ps.FLT_SEG_ID  = fs.FLT_SEG_ID "
				+ " AND frr.flexi_rule_rate_id           = ppof.flexi_rule_rate_id AND frr.flexi_rule_id   = frf.flexi_rule_id "
				+ " AND flxtype.flexi_rule_flexi_type_id = ppof.flexi_rule_flexi_type_id "
				+ " AND fs.est_time_departure_zulu   > (sysdate + (frf.cut_over_mins/1440))"
				+ " order by fs.est_time_departure_zulu";

		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> reseravtionPaxOndFlexibilities = (Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>>) jt
				.query(sql2, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> reseravtionPaxOndFlexibilities = new HashMap<Integer, Collection<ReservationPaxOndFlexibilityDTO>>();
						Set<String> keys = new HashSet<String>();
						if (rs != null) {
							while (rs.next()) {
								String key = rs.getInt("ppf_id") + "|" + rs.getInt("flexi_rule_flexi_type_id");
								if (!keys.contains(key)) {
									ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO = new ReservationPaxOndFlexibilityDTO();
									reservationPaxOndFlexibilityDTO.setPpOndFlxId(rs.getInt("ppondflx_id"));
									reservationPaxOndFlexibilityDTO.setPpfId(rs.getInt("ppf_id"));
									reservationPaxOndFlexibilityDTO.setFlexiRateId(rs.getInt("flexi_rule_rate_id"));
									reservationPaxOndFlexibilityDTO.setFlexibilityTypeId(rs.getInt("flexi_rule_flexi_type_id"));
									reservationPaxOndFlexibilityDTO.setAvailableCount(rs.getInt("available_count"));
									reservationPaxOndFlexibilityDTO.setUtilizedCount(rs.getInt("utilized_count"));
									reservationPaxOndFlexibilityDTO.setStatus(rs.getString("status"));
									reservationPaxOndFlexibilityDTO.setCutOverBufferInMins(rs.getInt("cut_over_mins"));
									reservationPaxOndFlexibilityDTO.setDescription(rs.getString("description"));
									reservationPaxOndFlexibilityDTO.setFlexiRuleID(rs.getInt("flexi_rule_id"));

									if (!reseravtionPaxOndFlexibilities.containsKey(reservationPaxOndFlexibilityDTO.getPpfId())) {
										reseravtionPaxOndFlexibilities.put(reservationPaxOndFlexibilityDTO.getPpfId(),
												new ArrayList<ReservationPaxOndFlexibilityDTO>());
									}
									reseravtionPaxOndFlexibilities.get(reservationPaxOndFlexibilityDTO.getPpfId()).add(
											reservationPaxOndFlexibilityDTO);
									keys.add(key);
								}
							}
						}
						return reseravtionPaxOndFlexibilities;
					}
				});

		if (log.isDebugEnabled())
			log.debug("Exit getReservationPaxOndFlexibilities");

		return reseravtionPaxOndFlexibilities;
	}
}
