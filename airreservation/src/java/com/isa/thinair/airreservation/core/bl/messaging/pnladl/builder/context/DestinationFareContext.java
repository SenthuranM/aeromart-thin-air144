/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;

/**
 * @author udithad
 *
 */
public class DestinationFareContext extends MessageHeaderElementContext {

	private Map<String, List<DestinationFare>> destinationFareElement;
	

	public List<DestinationFare> getDestinationFareElement() {
		List<DestinationFare> destinationFares = new ArrayList<DestinationFare>();
		for (Map.Entry<String, List<DestinationFare>> entry : destinationFareElement
				.entrySet()) {
			destinationFares.addAll(entry.getValue());
		}
		return destinationFares;
	}

	public void setDestinationFareElement(
			Map<String, List<DestinationFare>> destinationFareElement) {
		this.destinationFareElement = destinationFareElement;
	}

}
