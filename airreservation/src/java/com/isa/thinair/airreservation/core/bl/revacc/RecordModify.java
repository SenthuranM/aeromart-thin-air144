/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * commanad to modify record
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="recordModify"
 */
public class RecordModify extends DefaultBaseCommand {

	// Dao's
	private ReservationTnxDAO reservationTnxDao;

	/**
	 * constructor of the RecordModify command
	 */
	public RecordModify() {
		// looking up daos
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	/**
	 * execute method of the RecordModify command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal cancelTotal = (BigDecimal) this.getParameter(CommandParamNames.CANCEL_TOTAL);
		BigDecimal addedTotal = (BigDecimal) this.getParameter(CommandParamNames.ADDED_TOTAL);
		BigDecimal chargeAmount = (BigDecimal) this.getParameter(CommandParamNames.CHARGE_AMOUNT);
		BigDecimal penaltyAmount = this.getParameter(CommandParamNames.PENALTY_AMOUNT, BigDecimal.class);
		Integer actionNC = (Integer) this.getParameter(CommandParamNames.ACTION_NC);
		Integer chargeNC = (Integer) this.getParameter(CommandParamNames.CHARGE_NC);
		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = (ReservationPaxPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Collection paymentCollection = (Collection) this.getParameter(CommandParamNames.PAYMENT_LIST);
		String receiptNumber = (String) this.getParameter(CommandParamNames.RECIEPT_NUMBER);
		List<CardDetailConfigDTO> cardConfigData = (List<CardDetailConfigDTO>) this
				.getParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA);
		boolean enableTransactionGranularity = (Boolean) this
				.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);
		boolean isFirstPayment = getParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.FALSE, Boolean.class);
		boolean isActualPayment = this.getParameter(CommandParamNames.IS_ACTUAL_PAYMENT, Boolean.FALSE, Boolean.class);
		Integer transactionSeq = (Integer) this.getParameter(CommandParamNames.TRANSACTION_SEQ);
		Map<String, BigDecimal> lmsPaxProductRedeemedAmount = (Map<String, BigDecimal>) this
				.getParameter(CommandParamNames.LOYALTY_PRODUCT_REDEEMED_AMOUNTS);
		List<TransactionSegment> transactionSegments = (List<TransactionSegment>) this
				.getParameter(CommandParamNames.TRNX_SEGMENTS);
		// checking params
		this.checkParams(pnrPaxId, cancelTotal, addedTotal, chargeAmount, actionNC, chargeNC, credentialsDTO);

		ReservationTnx refundableReservationTnx = null;
		ReservationTnx addedReservationTnx = null;
		ReservationTnx modifyReservationTnx = null;

		if (cancelTotal.doubleValue() != 0) {
			if (cancelTotal.doubleValue() > 0) {
				refundableReservationTnx = TnxFactory.getCreditInstance(pnrPaxId, cancelTotal.abs(),
						actionNC.intValue(), credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null, null,
						null, false, true);
			} else {
				refundableReservationTnx = TnxFactory.getDebitInstance(pnrPaxId, cancelTotal.abs(), actionNC.intValue(),
						credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
			}

			reservationTnxDao.saveTransaction(refundableReservationTnx);
		}

		TnxGranularityFactory.saveReservationPaxTnxBreakdownForRefundables(pnrPaxId, actionNC,
				reservationPaxPaymentMetaTO, enableTransactionGranularity, transactionSeq);

		if (addedTotal.doubleValue() != 0) {
			addedReservationTnx = TnxFactory.getDebitInstance(pnrPaxId, addedTotal,
					ReservationTnxNominalCode.ADDED_CHARGES.getCode(), credentialsDTO,
					CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
			reservationTnxDao.saveTransaction(addedReservationTnx);
		}

		if (chargeAmount.doubleValue() != 0) {
			modifyReservationTnx = TnxFactory.getDebitInstance(pnrPaxId, chargeAmount, chargeNC.intValue(),
					credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
			reservationTnxDao.saveTransaction(modifyReservationTnx);
		}

		if (penaltyAmount != null && penaltyAmount.compareTo(BigDecimal.ZERO) > 0) {

			ReservationTnx penaltyReservationTnx = TnxFactory.getDebitInstance(pnrPaxId, penaltyAmount,
					ReservationTnxNominalCode.PENALTY_CHARGE.getCode(), credentialsDTO,
					CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
			reservationTnxDao.saveTransaction(penaltyReservationTnx);
		}

		// record payment
		if (paymentCollection != null && !paymentCollection.isEmpty()) {
			Command recordPaymentCmd = (Command) ReservationModuleUtils.getBean(CommandNames.RECORD_PAYMENT);
			recordPaymentCmd.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
			recordPaymentCmd.setParameter(CommandParamNames.PAYMENT_LIST, paymentCollection);
			recordPaymentCmd.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO,
					reservationPaxPaymentMetaTO);
			recordPaymentCmd.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			recordPaymentCmd.setParameter(CommandParamNames.RECIEPT_NUMBER, receiptNumber);
			recordPaymentCmd.setParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA, cardConfigData);
			recordPaymentCmd.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY,
					enableTransactionGranularity);
			recordPaymentCmd.setParameter(CommandParamNames.IS_FIRST_PAYMENT, isFirstPayment);
			recordPaymentCmd.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, isActualPayment);
			recordPaymentCmd.setParameter(CommandParamNames.TRANSACTION_SEQ, transactionSeq);
			recordPaymentCmd.setParameter(CommandParamNames.LOYALTY_PRODUCT_REDEEMED_AMOUNTS,
					lmsPaxProductRedeemedAmount);
			recordPaymentCmd.setParameter(CommandParamNames.TRNX_SEGMENTS, transactionSegments);

			recordPaymentCmd.execute();
		} else {
			TnxGranularityFactory.saveReservationPaxTnxBrkForCreditsComponsatingNewCharges(pnrPaxId,
					ReservationTnxNominalCode.CREDIT_BF, reservationPaxPaymentMetaTO, enableTransactionGranularity,
					transactionSeq);
		}

		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(true);
		return defaultServiceResponse;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param pnrPaxId
	 * @param cancelTotal
	 * @param addedTotal
	 * @param chargeAmount
	 * @param actionNC
	 * @param chargeNC
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxId, BigDecimal cancelTotal, BigDecimal addedTotal, BigDecimal chargeAmount,
			Integer actionNC, Integer chargeNC, CredentialsDTO credentialsDTO) throws ModuleException {

		if (pnrPaxId == null || cancelTotal == null || chargeAmount == null || actionNC == null || chargeNC == null
				|| addedTotal == null || credentialsDTO == null)
			throw new ModuleException("airreservations.arg.invalid.null");
	}

}
