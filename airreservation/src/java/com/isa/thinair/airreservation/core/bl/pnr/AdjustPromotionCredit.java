package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.DiscountDetails;
import com.isa.thinair.promotion.api.model.FlightPeriod;
import com.isa.thinair.promotion.api.model.PromoCode;
import com.isa.thinair.promotion.api.model.RegistrationPeriod;
import com.isa.thinair.promotion.api.model.SysGenPromotionCriteria;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * Command class for a applying promotion discount as user credit & auto consume credit as new promotion
 * 
 * @author rumesh
 * @isa.module.command name="adjustPromotionCredit"
 */
public class AdjustPromotionCredit extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(AdjustPromotionCredit.class);

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside AdjustPromotionCredit");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection<OndFareDTO> colOndFareDTO = (Collection<OndFareDTO>) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DiscountedFareDetails discFareDetailsDTO = this.getParameter(CommandParamNames.FARE_DISCOUNT_INFO,
				DiscountedFareDetails.class);
		BigDecimal totalDiscount = (BigDecimal) this.getParameter(CommandParamNames.USED_PROMOTION_CREDITS);
		String fareDiscountAudit = (String) this.getParameter(CommandParamNames.FARE_DISCOUNT_AUDIT);

		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		ReservationDiscountDTO reservationDiscountDTO = (ReservationDiscountDTO) this
				.getParameter(CommandParamNames.RESERVATION_DISCOUNT_DTO);
		

		this.validateParams(colOndFareDTO, credentialsDTO);

		if (discFareDetailsDTO != null && discFareDetailsDTO.getPromotionId() != null) {

			if (discFareDetailsDTO.isSystemGenerated()) {

				ReservationModuleUtils.getPromotionCriteriaAdminBD().updateSystemGeneratedPromotion(
						discFareDetailsDTO.getPromotionId(), totalDiscount, PromotionCriteriaConstants.PromotionStatus.INACTIVE);

			} else if (PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discFareDetailsDTO.getDiscountAs())) {
				Reservation reservation = null;
				if (pnr != null) {
					// Retrieves the Reservation
					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(pnr);
					pnrModesDTO.setLoadFares(true);
					pnrModesDTO.setLoadPaxAvaBalance(true);
					reservation = ReservationProxy.getReservation(pnrModesDTO);
				}

				Set<ReservationPax> resPaxs = reservation.getPassengers();
				boolean update = false;
				BigDecimal totDiscountedCredits = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (ReservationPax reservationPax : resPaxs) {
					BigDecimal paxTotalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

					for (OndFareDTO ondFareDTO : colOndFareDTO) {

						PaxDiscountDetailTO paxDiscountDetailTO = null;
						if (reservationDiscountDTO != null) {
							paxDiscountDetailTO = reservationDiscountDTO.getPaxDiscountDetail(reservationPax.getPaxSequence());
							if (paxDiscountDetailTO != null && reservationDiscountDTO.isDiscountExist()) {
								paxTotalDiscount = paxDiscountDetailTO.getDiscountTotalByFlightSegments(
										ondFareDTO.getFlightSegmentIds(), true);
								update = true;

								totDiscountedCredits = AccelAeroCalculator.add(totDiscountedCredits, paxTotalDiscount);
							}

						}
					}
				}

				if (update) {
					// Create system generated promotion with promo code
					SysGenPromotionCriteria sysGenPromo = createSystemGeneratedPromotion(discFareDetailsDTO.getPromotionId(),
							totDiscountedCredits, reservation.getPnr(), reservation.getStatus(), colOndFareDTO);

					if (!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())
							&& AccelAeroCalculator.isGreaterThan(sysGenPromo.getDiscount().getDiscountValue(),
									AccelAeroCalculator.getDefaultBigDecimalZero())) {
						// Email generated promo code to passenger
						ReservationModuleUtils.getPromotionManagementBD().sendPromoCodeEmailNotification(sysGenPromo,
								reservation.getContactInfo());
					}
					StringBuilder promoCodeVal = new StringBuilder("");
					if(sysGenPromo.getPromoCodes()!=null){
						
						for(PromoCode promoCodeOb : sysGenPromo.getPromoCodes()){
							promoCodeVal.append(promoCodeOb.getPromoCode()+ " ");
						}
					}					
					
					String sysGenPromoAudit = ", Generated Promo Code: " + promoCodeVal.toString();
					if (fareDiscountAudit == null) {
						fareDiscountAudit = sysGenPromoAudit;
					} else {
						fareDiscountAudit += sysGenPromoAudit;
					}

					reservation = ReservationProxy.saveReservation(reservation);
				}
			}
			
			if (discFareDetailsDTO.getPromoCode() != null) {
				ReservationModuleUtils.getPromotionCriteriaAdminBD().updatePromoCodeUtilization(discFareDetailsDTO.getPromoCode());
			}

		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.FARE_DISCOUNT_AUDIT, fareDiscountAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit AdjustPromotionCredit");
		return response;
	}

	private SysGenPromotionCriteria createSystemGeneratedPromotion(Long originPromoId, BigDecimal totDiscountedCredits,
			String originPnr, String reservationStatus, Collection<OndFareDTO> colOndFareDTO) throws ModuleException {

		PromotionCriteriaTO promotionCriteriaTO = ReservationModuleUtils.getPromotionCriteriaAdminBD().findPromotionCriteria(
				originPromoId);

		SysGenPromotionCriteria sysGenPromo = new SysGenPromotionCriteria();
		sysGenPromo.setSystemGenerated(true);
		
		// date periods updated for multiple period design
		RegistrationPeriod resPeriods = new RegistrationPeriod();
		resPeriods.setFromDate(promotionCriteriaTO.getConstValidFrom());
		resPeriods.setToDate(promotionCriteriaTO.getConstValidTo());
		resPeriods.setPromotionCriteria(sysGenPromo);
		sysGenPromo.setRegistrationPeriods(new HashSet<RegistrationPeriod>(Arrays.asList(resPeriods)));

		FlightPeriod flightPeriods = new FlightPeriod();
		flightPeriods.setFromDate(promotionCriteriaTO.getConstFlightFrom());
		flightPeriods.setToDate(promotionCriteriaTO.getConstFlightTo());
		flightPeriods.setPromotionCriteria(sysGenPromo);
		sysGenPromo.setFlightPeriods(new HashSet<FlightPeriod>(Arrays.asList(flightPeriods)));
		
		sysGenPromo.setUsedCredit(AccelAeroCalculator.getDefaultBigDecimalZero());
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
			sysGenPromo.setStatus(PromotionCriteriaConstants.PromotionStatus.INACTIVE);
		} else {
			sysGenPromo.setStatus(PromotionCriteriaConstants.PromotionStatus.ACTIVE);
		}
		String promoCode = ReservationModuleUtils.getPromotionManagementBD().generatePromotionCode();
		
		//To remove according to new behavior
		sysGenPromo.setPromoCode(promoCode);
		sysGenPromo.setPromoName(PromotionCriteriaConstants.PromotionCriteriaTypesDesc.SYS_GEN_PROMO);
		sysGenPromo.setPromoCodeEnabled(true);
		sysGenPromo.setOriginPnr(originPnr);
		DiscountDetails discountDetails = new DiscountDetails();
		discountDetails.setApplyAs(PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY);
		discountDetails.setApplyTo(Integer.toString(PromotionCriteriaConstants.DiscountApplyTo.RESERVATION.ordinal()));
		discountDetails.setDiscountType(PromotionCriteriaConstants.DiscountTypes.VALUE);
		discountDetails.setDiscountValue(totDiscountedCredits);
		discountDetails.setConstPaxName(promotionCriteriaTO.getConstPaxName());
		sysGenPromo.setDiscount(discountDetails);

		if (promotionCriteriaTO.getConstPaxSector()) {
			sysGenPromo.setApplicableONDs(getSameSectorList(colOndFareDTO));
			sysGenPromo.setApplicableForOneway(promotionCriteriaTO.getApplicableForOneway());
			sysGenPromo.setApplicableForReturn(promotionCriteriaTO.getApplicableForReturn());
			sysGenPromo.setApplicability(promotionCriteriaTO.getApplicability());
			discountDetails.setConstPaxSector(promotionCriteriaTO.getConstPaxSector());
		} else {
			sysGenPromo.setApplicableForOneway(true);
			sysGenPromo.setApplicableForReturn(true);
			sysGenPromo.setApplicability(PromotionCriteriaConstants.ApplicableRoute.RETURN);
		}
		
		//Multiple Code Generation Code re-factor
		PromoCode promoCodeOb = new PromoCode();
		promoCodeOb.setPromoCode(promoCode);
		promoCodeOb.setPromoCriteria(sysGenPromo);
		promoCodeOb.setFullyUtilized(false);
		
		Set<PromoCode> promoCodes = new HashSet<PromoCode>();
		promoCodes.add(promoCodeOb);		
		sysGenPromo.setPromoCodes(promoCodes);

		sysGenPromo = (SysGenPromotionCriteria) ReservationModuleUtils.getPromotionCriteriaAdminBD().savePromotionCriteria(
				sysGenPromo);

		return sysGenPromo;
	}

	private Set<String> getSameSectorList(Collection<OndFareDTO> colOndFareDTOs) throws ModuleException {
		LinkedHashMap<Integer, List<String>> ondWiseSegCodes = new LinkedHashMap<Integer, List<String>>();

		for (OndFareDTO ondFareDTO : colOndFareDTOs) {
			if (!ondWiseSegCodes.containsKey(ondFareDTO.getOndSequence())) {
				ondWiseSegCodes.put(ondFareDTO.getOndSequence(), new ArrayList<String>());
			}

			ondWiseSegCodes.get(ondFareDTO.getOndSequence()).add(ondFareDTO.getOndCode());
		}

		Set<String> ondSet = new HashSet<String>();
		ondSet.add(ReservationApiUtils.getOndCode(ondWiseSegCodes.get(OndSequence.OUT_BOUND)));

		return ondSet;
	}

	private void validateParams(Collection<OndFareDTO> colOndFareDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (colOndFareDTO == null || colOndFareDTO.size() == 0 || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}

}
