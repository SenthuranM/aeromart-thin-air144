package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.ChangedNamesDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class PassengerNamesComposerForNameChange implements PassengerNamesComposingStrategy {

	@Override
	public List<SSRDTO> composePassengerNames(Reservation reservation, BookingRequestDTO bookingRequestDTO,
			TypeBRequestDTO typeBRequestDTO) {
		Map<Integer, NameDTO> changedPaxNamesMap = typeBRequestDTO.getChangedPaxNamesMap();
		List<SSRDTO> ssrDTOs = new ArrayList<SSRDTO>();
		List<ChangedNamesDTO> changedNamesDTOs = new ArrayList<ChangedNamesDTO>();
		NameDTO paxName = null;
		NameDTO oldPaxName = null;
		SSRChildDTO childDTO = null;
		SSRChildDTO oldChildDTO = null;
		SSRInfantDTO infantDTO = null;
		SSRInfantDTO oldInfantDTO = null;
		ChangedNamesDTO changedNamesDTO = null;
		List<NameDTO> oldNameDTOs = new ArrayList<NameDTO>();
		List<NameDTO> newNameDTOs = new ArrayList<NameDTO>();

		changedNamesDTO = new ChangedNamesDTO();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (reservationPax != null) {
				if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())) {
					paxName = new NameDTO();
					TypeBPassengerAdopter.adoptPax(paxName, reservationPax);
					newNameDTOs.add(paxName);
					if (changedPaxNamesMap.get(reservationPax.getPnrPaxId()) != null) {
						oldNameDTOs.add(changedPaxNamesMap.get(reservationPax.getPnrPaxId()));
					} else {
						oldNameDTOs.add(paxName);
					}
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType())) {
					paxName = new NameDTO();
					TypeBPassengerAdopter.adoptPax(paxName, reservationPax);
					newNameDTOs.add(paxName);
					childDTO = new SSRChildDTO();
					TypeBPassengerAdopter.adoptChild(childDTO, reservationPax);
					if (typeBRequestDTO.getCsOCCarrierCode() != null) {
						childDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
						childDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode());
					} else {
						childDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						childDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					}	
					ssrDTOs.add(childDTO);
					if (changedPaxNamesMap.get(reservationPax.getPnrPaxId()) != null) {
						oldPaxName = changedPaxNamesMap.get(reservationPax.getPnrPaxId());
						oldNameDTOs.add(oldPaxName);
						/* AIRIMP manual has not given any examples for child name change
						oldChildDTO = new SSRChildDTO();
						oldChildDTO.setTitle(oldPaxName.getPaxTitle());
						oldChildDTO.setFirstName(oldPaxName.getFirstName());
						oldChildDTO.setLastName(oldPaxName.getLastName());
						oldChildDTO.setDateOfBirth(reservationPax.getDateOfBirth());
						oldChildDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						oldChildDTO.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CANCELLED.getCode());
						ssrDTOs.add(oldChildDTO);
						*/
					} else {
						oldNameDTOs.add(paxName);
					}
				} else {
					for (BookingSegmentDTO bookingSegment : bookingRequestDTO.getBookingSegmentDTOs()) {
						if (changedPaxNamesMap.get(reservationPax.getPnrPaxId()) != null) {
							paxName = new NameDTO();
							paxName.setPaxTitle(reservationPax.getTitle());
							paxName.setFirstName(reservationPax.getFirstName());
							paxName.setLastName(reservationPax.getLastName());
							paxName.setFamilyID(reservationPax.getFamilyID());
							newNameDTOs.add(paxName);
						}						
						infantDTO = new SSRInfantDTO();
						infantDTO.setInfantTitle(reservationPax.getTitle());
						infantDTO.setInfantFirstName(reservationPax.getFirstName());
						infantDTO.setInfantLastName(reservationPax.getLastName());
						infantDTO.setInfantDateofBirth(reservationPax.getDateOfBirth());
						if (changedPaxNamesMap.get(reservationPax.getParent().getPnrPaxId()) != null) {
							infantDTO.setGuardianFirstName(changedPaxNamesMap.get(reservationPax.getParent().getPnrPaxId())
									.getFirstName());
							infantDTO.setGuardianLastName(changedPaxNamesMap.get(reservationPax.getParent().getPnrPaxId())
									.getLastName());
						} else {
							infantDTO.setGuardianFirstName(reservationPax.getParent().getFirstName());
							infantDTO.setGuardianLastName(reservationPax.getParent().getLastName());
						}
						infantDTO.setGuardianTitle(reservationPax.getParent().getTitle());
						if (typeBRequestDTO.getCsOCCarrierCode() != null) {
							infantDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
						} else {
							infantDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						}
						infantDTO.setAdviceOrStatusCode(bookingSegment.getAdviceOrStatusCode());
						infantDTO.setSegmentDTO(bookingSegment);
						ssrDTOs.add(infantDTO);
						if (changedPaxNamesMap.get(reservationPax.getPnrPaxId()) != null) {
							oldPaxName = changedPaxNamesMap.get(reservationPax.getPnrPaxId());
							oldNameDTOs.add(oldPaxName);
							/* AIRIMP manual has not given any examples for infant name change
							oldInfantDTO = new SSRInfantDTO();
							oldInfantDTO.setInfantTitle(oldPaxName.getPaxTitle());
							oldInfantDTO.setInfantFirstName(oldPaxName.getFirstName());
							oldInfantDTO.setInfantLastName(oldPaxName.getLastName());
							oldInfantDTO.setInfantDateofBirth(reservationPax.getDateOfBirth());
							oldInfantDTO.setGuardianFirstName(reservationPax.getParent().getFirstName());
							oldInfantDTO.setGuardianLastName(reservationPax.getParent().getLastName());
							oldInfantDTO.setGuardianTitle(reservationPax.getParent().getTitle());
							if (typeBRequestDTO.getCsOCCarrierCode() != null) {
								oldInfantDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
							} else {
								oldInfantDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
							}
							oldInfantDTO.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CANCELLED.getCode());
							oldInfantDTO.setSegmentDTO(bookingSegment);
							ssrDTOs.add(oldInfantDTO);
							 */
						}
					}
				}
			}
		}
		changedNamesDTO.setOldNameDTOs(oldNameDTOs);
		changedNamesDTO.setNewNameDTOs(newNameDTOs);
		changedNamesDTOs.add(changedNamesDTO);
		bookingRequestDTO.setChangedNameDTOs(changedNamesDTOs);

		return ssrDTOs;
	}

}
