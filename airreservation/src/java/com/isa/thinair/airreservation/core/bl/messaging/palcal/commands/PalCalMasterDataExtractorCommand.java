package com.isa.thinair.airreservation.core.bl.messaging.palcal.commands;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PnlAdlFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlAbstractDataFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlDataFactoryProducer;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="palCalMasterDataExtractorCommand"
 */
public class PalCalMasterDataExtractorCommand extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(PalCalMasterDataExtractorCommand.class);

	private PnlAdlAbstractDataFactory<BasePassengerCollection, BaseDataContext> dcsPaxMsgAbstractDataFactory;
	private MessageDataStructure<BasePassengerCollection, BaseDataContext> messageDataStructure;
	private PassengerCollection passengerCollection;
	private BaseDataContext baseDataContext;
	private String messageType;

	/**
	 * Execute method of the PalCalMasterDataExtractor command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		createDataFactory();
		resolveCommandParameters();
		messageType = getMessageType();
		messageDataStructure = dcsPaxMsgAbstractDataFactory.createDataStructure(messageType);
		passengerCollection = (PassengerCollection) messageDataStructure.getDataStructure(baseDataContext);
		baseDataContext.setPassengerCollection(passengerCollection);
		createResponseObject(response);
		return response;
	}

	private void createDataFactory() {
		dcsPaxMsgAbstractDataFactory = PnlAdlDataFactoryProducer.getFactory(PnlAdlFactory.PNLADL_MASTER_DATA);
	}

	private void resolveCommandParameters() {
		baseDataContext = (BaseDataContext) this.getParameter(CommandParamNames.PNL_DATA_CONTEXT);
	}

	private String getMessageType() {
		return baseDataContext.getMessageType();
	}

	private void createResponseObject(DefaultServiceResponse response) {
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, response);
		response.addResponceParam(CommandParamNames.PNL_DATA_CONTEXT, baseDataContext);
	}

}
