package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.AdjustCreditTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 */
public class AdjustCreditBO {

	private List<AdjustCreditTO> colAdjustCreditTO;

	private Reservation reservation;

	private boolean checkForNewTransaction = false;

	private Map<Integer, BigDecimal> paxWiseAdjustment = null;
	private Map<Integer, Integer> paxWiseNewSequence = null;

	public AdjustCreditBO(Reservation reservation) {
		this.setColAdjustCreditTO(new ArrayList<AdjustCreditTO>());
		this.setReservation(reservation);
	}

	public AdjustCreditBO(Reservation reservation, boolean checkForNewTransaction) {
		this(reservation);
		this.checkForNewTransaction = checkForNewTransaction;
		if (this.checkForNewTransaction) {
			paxWiseAdjustment = new HashMap<>();
			paxWiseNewSequence = new HashMap<>();
		}
	}

	public void addAdjustment(String pnr, Integer pnrPaxId, String userNotes, ReservationPaxOndCharge reservationPaxOndCharge,
			CredentialsDTO credentialsDTO, boolean enableTransactionGranularity) throws ModuleException {

		AdjustCreditTO adjustCreditTO = new AdjustCreditTO();
		adjustCreditTO.setPnr(pnr);
		adjustCreditTO.setPnrPaxId(pnrPaxId);
		adjustCreditTO.setAmount(reservationPaxOndCharge.getEffectiveAmount());
		adjustCreditTO.setUserNotes(userNotes);
		adjustCreditTO.setReservationPaxOndCharge(reservationPaxOndCharge);
		adjustCreditTO.setCredentialsDTO(credentialsDTO);
		adjustCreditTO.setEnableTransactionGranularity(enableTransactionGranularity);
		this.getColAdjustCreditTO().add(adjustCreditTO);

		updatePaxWiseAdjustment(pnrPaxId, adjustCreditTO.getAmount());
	}

	private void updatePaxWiseAdjustment(Integer pnrPaxId, BigDecimal amount) {
		if (checkForNewTransaction) {
			if (!paxWiseAdjustment.containsKey(pnrPaxId)) {
				paxWiseAdjustment.put(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
			}

			paxWiseAdjustment.put(pnrPaxId, AccelAeroCalculator.add(paxWiseAdjustment.get(pnrPaxId), amount));
		}
	}

	private boolean hasPositiveAdjustment(Integer pnrPaxId) {
		if (paxWiseAdjustment.containsKey(pnrPaxId)) {
			return paxWiseAdjustment.get(pnrPaxId).compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0;
		}
		return false;
	}

	public void callRevenueAccountingAfterReservationSave() throws ModuleException {

		if (this.getColAdjustCreditTO().size() > 0) {
			ReservationPaymentMetaTO reservationPaymentMetaTO = TnxGranularityReservationUtils
					.getReservationPaymentMetaTO(this.getReservation());
			Collection<ReservationPaxOndCharge> currentAdjustments = getCurrentAdjustments(this.getColAdjustCreditTO());

			Collections.sort(this.getColAdjustCreditTO());
			for (AdjustCreditTO adjustCreditTO : this.getColAdjustCreditTO()) {
				ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo()
						.get(adjustCreditTO.getPnrPaxId());

				ReservationPaxPaymentMetaTO composedResPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
				composedResPaxPaymentMetaTO.setPnrPaxId(adjustCreditTO.getPnrPaxId());
				composedResPaxPaymentMetaTO.addColPerPaxWiseOndNewAdjustment(adjustCreditTO.getReservationPaxOndCharge());
				composedResPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(
						getPaxWiseNewOndCharges(adjustCreditTO, reservationPaxPaymentMetaTO.getColPerPaxWiseOndNewCharges()));
				composedResPaxPaymentMetaTO.removeColPerPaxWiseOndNewChargeAll(currentAdjustments);

				ReservationModuleUtils.getRevenueAccountBD().adjustCredits(adjustCreditTO.getPnr(),
						adjustCreditTO.getPnrPaxId().toString(), adjustCreditTO.getAmount(), adjustCreditTO.getUserNotes(),
						composedResPaxPaymentMetaTO, adjustCreditTO.getCredentialsDTO(),
						adjustCreditTO.isEnableTransactionGranularity(), getTnxSequenceForPnrPax(adjustCreditTO.getPnrPaxId()));
			}
		}
	}

	private Integer getTnxSequenceForPnrPax(Integer pnrPaxId) {
		if (checkForNewTransaction) {
			if (!paxWiseNewSequence.containsKey(pnrPaxId)) {
				paxWiseNewSequence.put(pnrPaxId, retrieveCorrectTransactionSeq(pnrPaxId, hasPositiveAdjustment(pnrPaxId)));
			}
			return paxWiseNewSequence.get(pnrPaxId);
		}

		return null;
	}

	private Integer retrieveCorrectTransactionSeq(Integer pnrPaxId, Boolean incrementTransactionSequence) {
		Integer maxTxnSeq = 0;
		try {
			maxTxnSeq = ReservationModuleUtils.getRevenueAccountBD().getPNRPaxMaxTransactionSeq(pnrPaxId);
			if (incrementTransactionSequence) {
				maxTxnSeq = maxTxnSeq + 1;
			}
		} catch (ModuleException e) {
			maxTxnSeq = 1;
		}
		return maxTxnSeq;
	}

	private Collection<ReservationPaxOndCharge> getPaxWiseNewOndCharges(AdjustCreditTO adjustCreditTO,
			Collection<ReservationPaxOndCharge> colPerPaxWiseOndNewCharges) {

		if (adjustCreditTO.getAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
			if (adjustCreditTO.getReservationPaxOndCharge() != null) {
				ReservationPaxOndCharge found = null;
				for (ReservationPaxOndCharge ondCharge : colPerPaxWiseOndNewCharges) {
					if (adjustCreditTO.getReservationPaxOndCharge().getChargeRateId() == ondCharge.getChargeRateId()
							&& AccelAeroCalculator.isEqual(
									adjustCreditTO.getReservationPaxOndCharge().getEffectiveAmount().negate(),
									ondCharge.getEffectiveAmount())) {
						found = ondCharge;
						break;
					}
				}

				if (found != null) {
					Collection<ReservationPaxOndCharge> newCharges = new ArrayList<>();
					newCharges.add(found);
					return newCharges;
				}
			}
		}
		return colPerPaxWiseOndNewCharges;
	}

	private Collection<ReservationPaxOndCharge> getCurrentAdjustments(Collection<AdjustCreditTO> colAdjustCreditTO) {
		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();
		if (colAdjustCreditTO != null && colAdjustCreditTO.size() > 0) {
			for (AdjustCreditTO adjustCreditTO : colAdjustCreditTO) {
				colReservationPaxOndCharge.add(adjustCreditTO.getReservationPaxOndCharge());
			}
		}

		return colReservationPaxOndCharge;
	}

	/**
	 * @return the colAdjustCreditTO
	 */
	private List<AdjustCreditTO> getColAdjustCreditTO() {
		return colAdjustCreditTO;
	}

	/**
	 * @param colAdjustCreditTO
	 *            the colAdjustCreditTO to set
	 */
	private void setColAdjustCreditTO(List<AdjustCreditTO> colAdjustCreditTO) {
		this.colAdjustCreditTO = colAdjustCreditTO;
	}

	/**
	 * @return the reservation
	 */
	private Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation
	 *            the reservation to set
	 */
	private void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

}
