package com.isa.thinair.airreservation.core.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Utility to generate the e Ticket Number
 * <p>
 * [format] AGENTCODE SYSTEM_DATE RUNNING_SEQUENCE
 * 
 * @author Duminda G
 * @version %G%
 */
public class ETicketGenerator {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ETicketGenerator.class);

	private static final String[] arrAlphaNumeric = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E",
			"F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

	private static final int LOG_36_1 = 36;

	private static final int LOG_36_2 = 1296;

	//private static final String SEQ_ALPHA_NUMERIC_CONV = "S_RESERVATION_ETICKET";

	public ETicketGenerator() {

	}

	/**
	 * Create the Sequence Number
	 * 
	 * @param sequenceNo
	 * @return
	 */
	private static String createETicketSequenceNumber(int sequenceNo) {
		int base2 = (sequenceNo) / LOG_36_2;
		int base1Div = (sequenceNo - base2 * LOG_36_2) / LOG_36_1;
		int base1Mod = (sequenceNo - base2 * LOG_36_2) % LOG_36_1;
		String eTktSeq = arrAlphaNumeric[base2] + arrAlphaNumeric[base1Div] + arrAlphaNumeric[base1Mod];

		return eTktSeq;
	}

	private String getSysDate(boolean addHours) {
		String formatedDate = "";
		SimpleDateFormat sdFmt = null;
		if (addHours) {
			sdFmt = new SimpleDateFormat("ddMMyyhh");
		} else {
			sdFmt = new SimpleDateFormat("ddMMyy");
		}
		formatedDate = sdFmt.format(CalendarUtil.getCurrentSystemTimeInZulu());

		return formatedDate;
	}

	/**
	 * Compose the e Ticket using agentCode ReservationDate Sequence Number
	 * 
	 * @param agentCode
	 * @param sequence
	 * @return
	 */
	public String composeETicket(String agentCode, String sequence, Map<String,String> format) {
		String runningSequence = null;
		String sysDate = null;
		boolean addHours = false;
		if ("true".equals((String) format.get("addHours"))) {
			addHours = true;
		}
		sysDate = getSysDate(addHours);
		StringBuilder eticket = new StringBuilder();

		// runningSequence = ETicketGenerator.createETicketSequenceNumber(Integer.parseInt(sequence));
		runningSequence = sequence; // IA 2010Sep16, None of the Airline required AlphaNumeric ticket number for the
									// moment. Therefore by passing alpha numeric sequence to numeric sequence

		if (sysDate != null && runningSequence != null) {
			String icaoCode = (String) format.get("icaoCode");
			if (!icaoCode.isEmpty()) {
				eticket.append(icaoCode).append(sysDate).append(runningSequence);
			} else {
				eticket.append(agentCode).append(sysDate).append(runningSequence);
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("E ticket Number " + eticket.toString());
		}
		return eticket.toString();
	}

	/**
	 * Compose the e Ticket using agentCode ReservationDate Sequence Number No of Pax
	 * 
	 * @param agentCode
	 * @param sequence
	 * @param noOfPax
	 * @return
	 */
	public Collection<StringBuilder> composeETicket(String agentCode, String[] sequence, int noOfPax, Map<String,String> format) {
		String sysDate = null;
		boolean addHours = false;
		if ("true".equals((String) format.get("addHours"))) {
			addHours = true;
		}
		sysDate = getSysDate(addHours);
		Collection<StringBuilder> etickets = new HashSet<StringBuilder>();
		boolean isL3Seq = false; // isValidforAlphaNumeric((String)format.get("extendSeq")); //FIXME IA 2010Sep19, None
									// of the Airline required AlphaNumeric ticket number for the moment. Therefore by
									// passing alpha numeric sequence to numeric sequence
		boolean addAgentCode = isAddFeild((String) format.get("addAgentCode"));
		boolean addSysDate = isAddFeild((String) format.get("addSysDate"));
		for (int a = 0; a < noOfPax; a++) {
			String runningSequence = null;

			if (isL3Seq) {
				runningSequence = ETicketGenerator.createETicketSequenceNumber(Integer.parseInt(sequence[a]));
			} else {
				runningSequence = sequence[a];
			}

			StringBuilder eticket = new StringBuilder();
			if (sysDate != null && runningSequence != null) {
				String icaoCode = (String) format.get("icaoCode");

				if (!icaoCode.isEmpty() && !isL3Seq && addSysDate) {
					//
					etickets.add(eticket.append(icaoCode).append(sysDate).append(runningSequence));
				} else if (!icaoCode.isEmpty() && isL3Seq && !addSysDate) {
					//
					etickets.add(eticket.append(icaoCode).append(runningSequence));
				} else if (!icaoCode.isEmpty() && isL3Seq && addSysDate) {
					//
					etickets.add(eticket.append(icaoCode).append(sysDate).append(runningSequence));
				} else if (!icaoCode.isEmpty() && !isL3Seq && !addAgentCode) {
					//
					etickets.add(eticket.append(icaoCode).append(runningSequence));
				} else if (!icaoCode.isEmpty() && isL3Seq && addAgentCode) {
					//
					etickets.add(eticket.append(icaoCode).append(agentCode).append(runningSequence));
				} else {
					//
					etickets.add(eticket.append(agentCode).append(sysDate).append(runningSequence));
				}
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("E ticket Numbers " + etickets.toString());
		}
		return etickets;
	}

	private boolean isAddFeild(String value) {
		return value.equalsIgnoreCase("true");
	}
	
	public static Collection<ReservationPax> getETGenerationEligiblePaxInCNFBookings(Reservation reservation,
			Map<Integer, IPayment> passengerPayment, Boolean triggerPaidAmountError, Boolean triggerPnrForceConfirmed,
			Boolean acceptMorePaymentsThanItsCharges, Boolean isPaymentForAcquirCredit, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Collection<ReservationPax> confirmedPax = new ArrayList<ReservationPax>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		if (!triggerPnrForceConfirmed.booleanValue()
				&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				if (reservationPax.getStatus().equals(ReservationPaxStatus.CONFIRMED)) {
					if (ReservationApiUtils.isInfantType(reservationPax)){
						
						if(reservation.isInfantPaymentRecordedWithInfant()){
							if(ReservationApiUtils.checkPassengerConfirmationForAModifyBooking(reservationPax,
									passengerPayment, acceptMorePaymentsThanItsCharges, isPaymentForAcquirCredit)) {
								
								confirmedPax.add(reservationPax);
							
							}
						}else{
							if(ReservationApiUtils.checkPassengerConfirmationForAModifyBooking(reservationPax.getParent(),
									passengerPayment, acceptMorePaymentsThanItsCharges, isPaymentForAcquirCredit)) {
								
								confirmedPax.add(reservationPax);
							
							}
						}
			
					} else if (!ReservationApiUtils.isInfantType(reservationPax)
							&& ReservationApiUtils.checkPassengerConfirmationForAModifyBooking(reservationPax, passengerPayment,
									acceptMorePaymentsThanItsCharges, isPaymentForAcquirCredit)) {
						confirmedPax.add(reservationPax);
					}
				}
			}
		}
		return confirmedPax;
	}	
}