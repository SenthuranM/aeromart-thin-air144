/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AirportTransferAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.InsuranceHelper;
import com.isa.thinair.airreservation.core.bl.common.MealAdapter;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

/**
 * Command for creating a modification object for create on hold reservation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cMForCreateOnHoldReservation"
 */
public class CMForCreateOnHoldReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CMForCreateOnHoldReservation.class);

	/**
	 * Execute method of the CMForCreateOnHoldReservation command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Map<Integer, PaymentAssembler> pnrPaxIdAndPayments = (Map<Integer, PaymentAssembler>) this
				.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		Boolean triggerPnrForceConfirmed = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Map flightSeatIdsAndPaxTypes = (Map) this.getParameter(CommandParamNames.FLIGHT_SEAT_IDS_AND_PAX_TYPES);
		List<InsuranceResponse> insuranceResponses = (List<InsuranceResponse>) this.getParameter(CommandParamNames.INSURANCE_RES);
		Collection[] flightMealInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_MEAL_IDS);
		Collection[] airportTransferInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_AIRPORT_TRANSFER_IDS);
		List<Integer> openRTFltSegIds = (List<Integer>) this.getParameter(CommandParamNames.OPENRT_SEGMENT_IDS);
		String bookingType = (String) this.getParameter(CommandParamNames.BOOKING_TYPE);
		Collection[] flightBaggageInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_BAGGAGE_IDS);
		Collection<FlexiExternalChgDTO> flexiCharges = (Collection<FlexiExternalChgDTO>) this
				.getParameter(CommandParamNames.FLEXI_INFO);

		Set<FlightSegmentDTO> flightSegmentDTOs = new HashSet<FlightSegmentDTO>();
		if (this.getParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET) != null) {
			flightSegmentDTOs = (Set<FlightSegmentDTO>) this.getParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET);
		}

		String fareDiscountAudit = (String) this.getParameter(CommandParamNames.FARE_DISCOUNT_AUDIT);

		// Checking params
		this.validateParams(reservation, triggerPnrForceConfirmed, pnrPaxIdAndPayments, credentialsDTO);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		// Get the seat information
		String seatCodes = getSeatCodesForAudit(flightSeatIdsAndPaxTypes);

		// Get the seat information
		Collection[] colResInfo = { reservation.getPassengers(), reservation.getSegmentsView() };
		String mealCodes = MealAdapter.recordPaxWiseAuditHistoryForModifyMeals(reservation.getPnr(), flightMealInfo[0],
				credentialsDTO, null, colResInfo);

		String airportTransfers = null;
		if (airportTransferInfo != null && airportTransferInfo.length > 0) {
			airportTransfers = AirportTransferAdaptor.recordPaxWiseAuditHistoryForModifyAirportTransfers(reservation.getPnr(),
					airportTransferInfo[0], credentialsDTO, null, colResInfo);
		}

		String baggageNames = BaggageAdaptor.recordPaxWiseAuditHistoryForModifyBaggages(reservation.getPnr(),
				flightBaggageInfo[0], credentialsDTO, null, colResInfo);

		String insuranceAudit = InsuranceHelper.getAudit(insuranceResponses);

		// Get flexi information
		String flexiInfo = null;
		if (flexiCharges != null && flexiCharges.size() > 0) {
			flexiInfo = ReservationApiUtils.getFlexiInfoForAudit(flexiCharges, reservation, null);
		}

		String segmentInformation = ReservationApiUtils
				.getSegmentInformationForReservation(new ArrayList<FlightSegmentDTO>(flightSegmentDTOs), openRTFltSegIds);

		// Compose Modification object
		Collection colReservationAudit = this.composeAudit(reservation, reservation.getUserNote(), triggerPnrForceConfirmed,
				pnrPaxIdAndPayments, credentialsDTO, seatCodes, insuranceAudit, mealCodes, airportTransfers, baggageNames,
				flexiInfo, segmentInformation, fareDiscountAudit, bookingType);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Returns the seat codes for audit
	 * 
	 * @param flightSeatIdsAndPaxTypes
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getSeatCodesForAudit(Map flightSeatIdsAndPaxTypes) {
		String seatCodes = null;

		if (flightSeatIdsAndPaxTypes != null && flightSeatIdsAndPaxTypes.keySet().size() > 0) {
			Collection<String> seatCodesValues = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
					.getSeatCodes(flightSeatIdsAndPaxTypes.keySet());

			if (seatCodesValues != null && seatCodesValues.size() > 0) {
				seatCodes = SMUtil.getSeatAuditString(seatCodesValues);
			}
		}

		return seatCodes;
	}

	/**
	 * Compose Audit
	 * 
	 * @param reservation
	 * @param userNote
	 * @param triggerPnrForceConfirmed
	 * @param pnrPaxIdAndPayments
	 * @param credentialsDTO
	 * @param fareDiscountInfo
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationAudit> composeAudit(Reservation reservation, String userNote, Boolean triggerPnrForceConfirmed,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, String seatCodes,
			String insuranceStr, String strMeals, String strAirportTransfers, String strBaggages, String flexiInfo,
			String flightSegmentInfo, String fareDiscountInfo, String bookingType) throws ModuleException {
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(reservation.getPnr());
		reservationAudit.setUserNote(userNote);
		reservationAudit.setUserNoteType(reservation.getUserNoteType());

		// Get the passenger payment information
		String audit = ReservationCoreUtils.getPassengerPaymentInfo(pnrPaxIdAndPayments, reservation);

		// Setting the new pnr information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.PASSENGER_DETAILS, audit);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		if (reservation.getOriginCountryOfCall() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.ORIGIN_COUNTRY_OF_CALL,
					reservation.getOriginCountryOfCall());
		}

		if (seatCodes != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.SEATS, seatCodes);
		}

		if (insuranceStr != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.INSURANCE, insuranceStr);
		}

		if (strMeals != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.MEALS, strMeals);
		}

		if (strAirportTransfers != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.AIRPORTTRANSFERS,
					strAirportTransfers);
		}

		if (strBaggages != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.BAGGAGES, strBaggages);
		}

		if (flexiInfo != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Flexibilities.FLEXIBILITIES, flexiInfo);
		}

		String itineraryFareMask = reservation.getItineraryFareMaskFlag();

		if (itineraryFareMask != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ItineraryFareMask.ITINERARY_FARE_MAKS,
					itineraryFareMask);
		}

		if (triggerPnrForceConfirmed.booleanValue()) {
			reservationAudit.setModificationType(AuditTemplateEnum.ADDED_CONFIRMED_RESERVATION.getCode());

			// Set reservation contact information
			this.setReservationContactInformation(reservation, reservationAudit, true);

			// Add the force confirm information
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.FORCE_CONFIRM,
					ReservationTemplateUtils.AUDIT_FORCE_CONFIRM_TEXT);
		} else {
			reservationAudit.setModificationType(AuditTemplateEnum.ADDED_ONHOLD_RESERVATION.getCode());

			// Set reservation contact information
			this.setReservationContactInformation(reservation, reservationAudit, false);
		}

		if (StringUtils.isNotEmpty(flightSegmentInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.FLIGHT_SEGMENT_DETAILS,
					flightSegmentInfo);
		}

		if (fareDiscountInfo != null && !"".equals(fareDiscountInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.FareDiscount.FARE_DISCOUNT_INFO, fareDiscountInfo);
		}

		if (bookingType != null && bookingType.equals("OVERBOOK")) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.BOOKING_TYPE,
					"OverBooking");
		} else if (bookingType != null && bookingType.equals(ReservationInternalConstants.ReservationType.WL.getDescription())) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.BOOKING_TYPE,
					"WaitListing");
		}

		colReservationAudit.add(reservationAudit);

		return colReservationAudit;
	}

	/**
	 * Set reservation contact information
	 * 
	 * @param reservation
	 * @param reservationAudit
	 * @param isForceConfirm
	 * @throws ModuleException
	 */
	private void setReservationContactInformation(Reservation reservation, ReservationAudit reservationAudit,
			boolean isForceConfirm) throws ModuleException {

		if (isForceConfirm) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.FIRST_NAME,
					reservation.getContactInfo().getFirstName());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.LAST_NAME,
					reservation.getContactInfo().getLastName());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.STREET_ADDRESS_1,
					reservation.getContactInfo().getStreetAddress1());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.STREET_ADDRESS_2,
					reservation.getContactInfo().getStreetAddress2());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.CITY,
					reservation.getContactInfo().getCity());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.STATE,
					reservation.getContactInfo().getState());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.COUNTRY,
					reservation.getContactInfo().getCountryCode());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.PHONE_NUMBER,
					reservation.getContactInfo().getPhoneNo());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.MOBILE_NUMBER,
					reservation.getContactInfo().getMobileNo());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.EMAIL,
					reservation.getContactInfo().getEmail());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.FAX,
					reservation.getContactInfo().getFax());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.NATIONALITY_CODE,
					reservation.getContactInfo().getNationalityCode());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.TOTAL_CHARGES,
					reservation.getTotalChargeAmount().toString());
		} else {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.FIRST_NAME,
					reservation.getContactInfo().getFirstName());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.LAST_NAME,
					reservation.getContactInfo().getLastName());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.STREET_ADDRESS_1,
					reservation.getContactInfo().getStreetAddress1());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.STREET_ADDRESS_2,
					reservation.getContactInfo().getStreetAddress2());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.CITY,
					reservation.getContactInfo().getCity());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.STATE,
					reservation.getContactInfo().getState());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.COUNTRY,
					reservation.getContactInfo().getCountryCode());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.PHONE_NUMBER,
					reservation.getContactInfo().getPhoneNo());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.MOBILE_NUMBER,
					reservation.getContactInfo().getMobileNo());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.EMAIL,
					reservation.getContactInfo().getEmail());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.FAX,
					reservation.getContactInfo().getFax());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.NATIONALITY_CODE,
					reservation.getContactInfo().getNationalityCode());

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.RELEASE_TIME,
					reservation.getReleaseTimeStampString("E, dd MMM yyyy HH:mm:ss", true));

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedOnHoldReservation.TOTAL_CHARGES,
					reservation.getTotalChargeAmount().toString());

		}

	}

	/**
	 * Validate Parameters
	 * 
	 * @param reservation
	 * @param triggerPnrForceConfirmed
	 * @param pnrPaxIdAndPayments
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Reservation reservation, Boolean triggerPnrForceConfirmed, Map pnrPaxIdAndPayments,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (reservation == null || reservation.getContactInfo() == null || reservation.getSegments() == null
				|| reservation.getSegments().size() == 0 || reservation.getPassengers() == null
				|| reservation.getPassengers().size() == 0 || triggerPnrForceConfirmed == null || pnrPaxIdAndPayments == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}
}
