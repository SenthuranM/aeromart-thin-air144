package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ObserverActionDataContext;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.commons.api.exception.ModuleException;

public abstract class BaseObseverActionTemplate<T> {

	public abstract void executeTemplateAction(T t) throws ModuleException;

	private Set<String> removeAllSSROnUpdate;

	private static final String KEY_SEPARATOR = "_";
	private static final String ADD = "A";
	private static final String DEL = "D";
	private static final String CHG = "C";
	private static final String OMIT = "O";
	private static final String CHK_RM_ALL_SSR = "CHK_RM_ALL_SSR";
	//private static final String DEL_REFRESH = "DEL_REFRESH";
	private static final String ADD_REFRESH = "ADD_REFRESH";
	
	protected static ObserverActionDataContext dataContext;
	protected ReservationAuxilliaryDAO auxilliaryDAO;

	protected List<AirportMessagePassenger> alreadyExistpaxListToUpdate = new ArrayList();

	protected BaseObseverActionTemplate() {
		auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
	}

	public static final Map<String, String> STATUS_MAP;

	static {
		Map<String, String> statusMap = new HashMap<String, String>();
		statusMap.put("A_C", CHK_RM_ALL_SSR);
		statusMap.put("A_D", OMIT);
		statusMap.put("A_A", ADD_REFRESH);
		statusMap.put("C_C", CHG);
		statusMap.put("C_D", DEL);
		statusMap.put("D_D", DEL);
		statusMap.put(ADD_REFRESH, ADD);
		//statusMap.put(DEL_REFRESH, DEL);
		
		STATUS_MAP = Collections.unmodifiableMap(statusMap);
	}

	protected void checkForExist(Map<Integer, Set<Integer>> paxMap, AdlActions action, String pnr) throws ModuleException {
		List<AirportMessagePassenger> existingPaxList = auxilliaryDAO.getAirportMessagePassengers(pnr);

		if (!isEmptyPaxMap(paxMap)) {
			for (AirportMessagePassenger apPax : existingPaxList) {
				if (paxMap.get(apPax.getPnrPaxId()) != null && paxMap.get(apPax.getPnrPaxId()).contains(apPax.getPnrSegId())) {

					if (isValidTransition(apPax.getPalStatus(), action.toString())) {
						paxMap.get(apPax.getPnrPaxId()).remove(apPax.getPnrSegId()); // remove seg id from paxwise map
						alreadyExistpaxListToUpdate.add(updatePax(apPax, getUpdatedStatus(apPax.getPalStatus(), action.toString())));
					}
				}
			}
		}
	}

	protected boolean isEmptyPaxMap(Map<Integer, Set<Integer>> paxMap) {
		boolean hasValues = true;
		for (Integer paxId : paxMap.keySet()) {
			if (paxMap.get(paxId) != null && !paxMap.get(paxId).isEmpty()) {
				hasValues = false;
				break;
			}
		}

		return hasValues;
	}

	protected AirportMessagePassenger updatePax(AirportMessagePassenger apPax, String status) throws ModuleException {
		if (OMIT.equals(status)) {
			apPax.setStatus(PNLConstants.PAlCALSendingStatus.SEND);
		} else if (CHK_RM_ALL_SSR.equals(status)) {
			
			if (removeAllSSROnUpdate.contains(getKey(apPax.getPnrPaxId(), apPax.getPnrSegId()))) {
				apPax.setStatus(PNLConstants.PAlCALSendingStatus.SEND);
			}
			
		}else if(ADD_REFRESH.equals(status)){
			updateWithUpdateResData(apPax);
						
		} else {
			apPax.setPalStatus(status);
		}
		return apPax;
	}
	
	private static void updateWithUpdateResData(AirportMessagePassenger apMsgPax) throws ModuleException {
		ReservationPax pax = getReservationPaxFromReservation(dataContext.getNewReservation(), apMsgPax.getPnrPaxId());
		ReservationSegment seg = getReservationSegFromReservation(dataContext.getNewReservation(),apMsgPax.getPnrSegId());

		apMsgPax.setTitle(pax.getTitle());
		apMsgPax.setFirstName(pax.getFirstName());
		apMsgPax.setLastName(pax.getLastName());

		apMsgPax.setBcType(seg.getBookingType());
		apMsgPax.setBookingCode(seg.getBookingCode());
		apMsgPax.setCabinClassCode(seg.getCabinClassCode());
		apMsgPax.setLogicalCCCode(seg.getLogicalCCCode());
		
	}

	private String getKey(Integer paxId, Integer segId){
		return paxId + "#" + segId;
	}

	protected String getUpdatedStatus(String existingStatus, String newStatus) {
		return STATUS_MAP.get(getKey(existingStatus, newStatus));
	}

	protected boolean isValidTransition(String existingStatus, String newStatus) {
		return STATUS_MAP.keySet().contains(getKey(existingStatus, newStatus));
	}

	protected String getKey(String existingStatus, String newStatus) {
		return existingStatus + KEY_SEPARATOR + newStatus;
	}

	protected AirportMessagePassenger createAirportMessagePassenger(Reservation reservation, Integer paxId, Integer segId,
			String action, String status) throws ModuleException {

		ReservationPax pax = getReservationPaxFromReservation(reservation, paxId);
		ReservationSegment seg = getReservationSegFromReservation(reservation, segId);

		AirportMessagePassenger apMsgPax = new AirportMessagePassenger();

		apMsgPax.setTitle(pax.getTitle());
		apMsgPax.setFirstName(pax.getFirstName());
		apMsgPax.setLastName(pax.getLastName());
		apMsgPax.setPnr(reservation.getPnr());
		apMsgPax.setPalStatus(action);
		apMsgPax.setPnrPaxId(pax.getPnrPaxId());
		apMsgPax.setPaxType(pax.getPaxType());
		apMsgPax.setPnrSegId(seg.getPnrSegId());
		apMsgPax.setFlightSegId(seg.getFlightSegId());
		apMsgPax.setStatus(status);
		apMsgPax.setBcType(seg.getBookingType());
		apMsgPax.setBookingCode(seg.getBookingCode());
		apMsgPax.setCabinClassCode(seg.getCabinClassCode());
		apMsgPax.setLogicalCCCode(seg.getLogicalCCCode());

		return apMsgPax;
	}

	protected List<AirportMessagePassenger> createUnexistPax(Map<Integer, Set<Integer>> paxWiseSegMap, Reservation reservation,
			String action) throws ModuleException {

		List<AirportMessagePassenger> paxList = new ArrayList<AirportMessagePassenger>();
		for (Integer pnrpaxId : paxWiseSegMap.keySet()) {
			Set<Integer> segIds = paxWiseSegMap.get(pnrpaxId);
			for (Integer segId : segIds) {

				if (ADD.equals(action) || (DEL.equals(action) && isPalMessagesDeliveredTo(pnrpaxId, segId)) || CHG.equals(action)) {
					paxList.add(createAirportMessagePassenger(reservation, pnrpaxId, segId, action,
							PNLConstants.PAlCALSendingStatus.NOT_SEND));
				}
			}
		}

		return paxList;
	}

	private static ReservationSegment getReservationSegFromReservation(Reservation reservation, Integer segId) {
		ReservationSegment selectedSeg = null;

		for (ReservationSegment seg : reservation.getSegments())
			if (seg.getPnrSegId().equals(segId)) {
				selectedSeg = seg;
				break;
			}
		return selectedSeg;
	}

	private static ReservationPax getReservationPaxFromReservation(Reservation reservation, Integer paxId) {
		ReservationPax selectedPax = null;

		for (ReservationPax pax : reservation.getPassengers())
			if (pax.getPnrPaxId().equals(paxId)) {
				selectedPax = pax;
				break;
			}
		return selectedPax;
	}

	public void setRemoveAllSSROnUpdate(Set<String> removeAllSSROnUpdate) {
		this.removeAllSSROnUpdate = removeAllSSROnUpdate;
	}

	private static boolean isPalMessagesDeliveredTo(Integer pnrPaxId,Integer pnrSegId){
		return ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_SEG_DAO.isPaxAvailableForCalDelete(pnrPaxId, pnrSegId);
	}
}
