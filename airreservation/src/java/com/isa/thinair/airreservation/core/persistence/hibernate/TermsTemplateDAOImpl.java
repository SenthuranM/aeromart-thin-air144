package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.isa.thinair.airreservation.api.model.TermsTemplate;
import com.isa.thinair.airreservation.api.model.TermsTemplateAudit;
import com.isa.thinair.airreservation.core.persistence.dao.TermsTemplateDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.TermsTemplateSearchDTO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * Class implementing data access operations related to {@link TermsTemplate} and it's auditing mechanism,
 * {@link TermsTemplateAudit}
 * 
 * @author thihara
 * 
 * @isa.module.dao-impl dao-name="TermsTemplateDAO"
 */
public class TermsTemplateDAOImpl extends PlatformBaseHibernateDaoSupport implements TermsTemplateDAO {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TermsTemplate> getAllTermsTemplates() {
		return loadAll(TermsTemplate.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateTermsTemplate(TermsTemplate termsTemplate, TermsTemplateAudit termsTemplateAudit) {

		TermsTemplate originalTemplate = (TermsTemplate) load(TermsTemplate.class,
				termsTemplate.getTemplateID());

		termsTemplateAudit.setPreviousContent(termsTemplate.getTemplateContent());
		termsTemplateAudit.setOwningTemplate(originalTemplate);

		termsTemplate.setAuditHistory(originalTemplate.getAuditHistory());
		termsTemplate.getAuditHistory().add(termsTemplateAudit);

		/*
		 * Need to call merge since an object with the same ID is already loaded.
		 */
		merge(termsTemplate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page<TermsTemplate> getTermsTemplatePage(TermsTemplateSearchDTO searchCriteria, Integer start, Integer recSize) {

		/*
		 * Two criteria instances are maintained to keep the total record count retrieval functioning. With just one
		 * instance total result projection count fails when starting record number is not 0.
		 */
		Criteria query = getSession().createCriteria(TermsTemplate.class);
		Criteria queryForSize = getSession().createCriteria(TermsTemplate.class);

		if (searchCriteria.getName() != null) {
			query.add(Restrictions.like("templateName", searchCriteria.getName()));
			queryForSize.add(Restrictions.like("templateName", searchCriteria.getName()));
		}

		if (searchCriteria.getLanguage() != null) {
			query.add(Restrictions.eq("languageCode", searchCriteria.getLanguage()));
			queryForSize.add(Restrictions.eq("languageCode", searchCriteria.getLanguage()));
		}

		if (searchCriteria.getCarriers() != null) {
			query.add(Restrictions.eq("carriers", searchCriteria.getCarriers()));
			queryForSize.add(Restrictions.eq("carriers", searchCriteria.getCarriers()));
		}

		@SuppressWarnings("unchecked")
		List<TermsTemplate> resultList = query.setFirstResult(start).setMaxResults(recSize).list();

		Long totalRecords = (Long) queryForSize.setProjection(Projections.rowCount()).uniqueResult();

		return new Page<TermsTemplate>(totalRecords.intValue(), start, start + recSize, resultList);
	}
}
