/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.model.PassengerMeal;

/**
 * MealsDAO is the business DAO interface for the sales service apis
 * 
 * @since 2.0
 */
public interface MealDAO {

	public void saveOrUpdate(Collection<PassengerMeal> meals);

	public Collection<String> getMealCodes(Collection<Integer> flightmealIds);

	public Collection<PaxMealTO> getReservationMeals(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds);

	public Collection<PaxMealTO> getReservationMeals(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			String preferredLanguage);

	public Collection<PassengerMeal> getFlightMealsForPnrPaxFare(Collection<Integer> ppfIds);

	public Collection<PassengerMeal> getFlightMealsForPnrPax(Collection<Integer> pnrPaxIds);

	public Map<Integer, String> getReservationMealsForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds);

	public Map<Integer, List<AncillaryDTO>> getReservationMealsForPNLADL(Collection<Integer> pnrPaxIds,
			Collection<Integer> pnrSegIds);

	public Integer getTemplateForSegmentId(int flightSegId);

	public Collection<PassengerMeal> getPassengerMeals(Collection<Integer> paxMealIds);

	public Map<Integer, String> getMealDetailNames(Collection<Integer> flightMealIds);

	/**
	 * Return expired meal(s) based on auto cancellation flag
	 * 
	 * @param cancellationIds
	 * @param marketingCarrier
	 * @return
	 */
	public Map<ReservationLiteDTO, List<PaxMealTO>> getExpiredMeals(Collection<Integer> cancellationIds, String marketingCarrier);

	/**
	 * Return expired lcc reservations due to meal expire based on auto cancellation flag
	 * 
	 * @param marketingCarrier
	 * @param cancellationIds
	 * 
	 * @return
	 */
	public Map<String, Integer> getExpiredLccMealsInfo(String marketingCarrier, Collection<Integer> cancellationIds);
}
