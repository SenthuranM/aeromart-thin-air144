package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CreateReservationIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public CreateReservationIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {
		RuleResponseDTO responseDTO = getResponseDTO();

		if (!isNotNull(dataContext.getExisitingReservation()) && isNotNull(dataContext.getUpdatedReservation())
				&& isCNFReservation((dataContext.getUpdatedReservation()))) {
			updatePaxWiseAffectedSegmentListToAllGivenPax(responseDTO, getUncancelledSegIds(dataContext.getUpdatedReservation()),
					null, getUncancelledPaxIds(dataContext.getUpdatedReservation()));
		}

		return responseDTO;
	}

}
