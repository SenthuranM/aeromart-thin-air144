/**
 * 
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.NSRefundReservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.NSRefundReservationDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * NSRefundReservationDAOImpl is the business DAO hibernate implementation
 * 
 * @author Jagath
 * @since 1.0
 * @isa.module.dao-impl dao-name="NSRefundReservationDAO"
 */
public class NSRefundReservationDAOImpl extends PlatformBaseHibernateDaoSupport implements NSRefundReservationDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PaxFinalSalesDAOImpl.class);

	@Override
	public Map<String, NSRefundReservation> getNSRefundHoldPNRs() {
		String nsRefundHoldDuration = AppSysParamsUtil.getNSAutoRefundHoldDuration();
		int refundHoldDuration = Integer.parseInt(nsRefundHoldDuration);
		String hql = " FROM NSRefundReservation AS nsrr where "
				+ " nsrr.nsProcessedTime < (sysdate - NUMTODSINTERVAL(?,'HOUR') )"
				+ " and ( nsrr.refundProcessedStatus = ? or nsrr.refundProcessedStatus = ? ) ";

		Collection<NSRefundReservation> nsRefundReservations = find(
				hql,
				new Object[] { refundHoldDuration, ReservationInternalConstants.NSRefundProcessStatus.NOT_PROCESSED,
						ReservationInternalConstants.NSRefundProcessStatus.ERROR_OCCURED }, NSRefundReservation.class);
		Map<String, NSRefundReservation> nsRefundHoldPNRs = new HashMap<String, NSRefundReservation>();
		if (nsRefundReservations != null && nsRefundReservations.size() > 0) {
			Iterator<NSRefundReservation> autoRefundPnrItr = nsRefundReservations.iterator();
			while (autoRefundPnrItr.hasNext()) {
				NSRefundReservation nsRefundReservation = autoRefundPnrItr.next();
				nsRefundHoldPNRs.put(nsRefundReservation.getPnr(), nsRefundReservation);
			}
		}
		return nsRefundHoldPNRs;
	}

	@Override
	public void saveNSRefundReservation(NSRefundReservation nsRefundReservation) {

		log.debug("Inside saveNSRefundReservation");

		// Save NSRefundReservation
		hibernateSaveOrUpdate(nsRefundReservation);

		log.debug("Exit saveNSRefundReservation");

	}

	@Override
	public boolean hasUnprocessedNSReservation(String pnr) {

		String sql = "SELECT count(ns_refund_id)" + " FROM t_ns_refund_pnr " + " where pnr = ? and "
				+ " ( refundProcessedStatus = ? or refundProcessedStatus = ? ) ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { pnr, ReservationInternalConstants.NSRefundProcessStatus.NOT_PROCESSED,
				ReservationInternalConstants.NSRefundProcessStatus.ERROR_OCCURED };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		}
		return false;
	}

}
