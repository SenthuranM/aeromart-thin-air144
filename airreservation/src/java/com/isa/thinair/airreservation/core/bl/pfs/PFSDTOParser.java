/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pfs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.PFSDestinationDTO;
import com.isa.thinair.airreservation.api.dto.PFSMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.PFSRecordDTO;
import com.isa.thinair.airreservation.api.dto.PaxFinalSalesDTO;
import com.isa.thinair.airreservation.api.dto.PfsLogDTO;
import com.isa.thinair.airreservation.api.dto.RecordDTO;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PaxFinalSalesDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * The utility to parse a PFS document
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PFSDTOParser {

	/**
	 * Returns Pfs meta data transfer information
	 * 
	 * @param f
	 * @return
	 * @throws ModuleException
	 */
	private static PFSMetaDataDTO getPFSMetaDataDTO(File f) throws ModuleException {

		Throwable exception = null;
		String pfsContent = "";
		try {

			BufferedReader buffer = new BufferedReader(new FileReader(f));
			StringBuffer parserFriendlyText = new StringBuffer();
			StringBuffer originalPfsText = new StringBuffer();
			boolean destinationFound = false;
			String strHolder = null;
			String fromAddress = "";
			boolean started = false;
			boolean flightNumberPass = false;

			// TODO: PFS Indika
			while ((strHolder = buffer.readLine()) != null) {

				// Capturing the from address
				if (strHolder.length() > 5 && strHolder.substring(0, 5).equalsIgnoreCase("From:")) {
					fromAddress = BeanUtils.nullHandler(strHolder.substring(5));
					int start = fromAddress.indexOf("<");
					int end = fromAddress.lastIndexOf(">");

					// This means Email Address is nilindra@abc.com
					if (start == -1 || end == -1) {
						fromAddress = BeanUtils.nullHandler(fromAddress);
						// This mean Email Address is Nilindra <nilindra@abc.com>
					} else {
						fromAddress = BeanUtils.nullHandler(fromAddress.substring(start + 1, end));
					}
				}
				// Processing the pfs part
				else if (strHolder.equalsIgnoreCase("PFS") || started) {
					originalPfsText.append(strHolder + "\n");
					started = true;

					if (strHolder.startsWith("RBD")) {
						continue;
					}
					// If it's PFS label
					if (strHolder.equalsIgnoreCase("PFS")) {
						parserFriendlyText.append(strHolder + "\n");
						// Process down the PFS label
					} else if (!isNumber(strHolder.trim().substring(0, 3))) {
						if ((strHolder.charAt(0) == '-') && destinationFound == false) {
							parserFriendlyText.append(strHolder + "\n");
							destinationFound = true;
						} else if ((strHolder.charAt(0) == '-') && destinationFound == true) {
							parserFriendlyText.append(strHolder + "\n");
						} else if (strHolder.charAt(3) == ' ') {
							parserFriendlyText.append(strHolder.substring(0, 7).replaceAll("/", "").trim() + "\n");
						} else if (strHolder.substring(0, 3).equalsIgnoreCase(".L/")) {
							parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);
							parserFriendlyText.append(strHolder + "\n");
						}
						// Ignoring Remarks and remarks continuation elements and FQTV number(if it is in new line) for
						// the moment
						else if (strHolder.substring(0, 3).equalsIgnoreCase(".R/")
								|| strHolder.substring(0, 4).equalsIgnoreCase(".RN/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".F/")) {
							// parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);
							// parserFriendlyText.append(" " + strHolder + "\n");
						} else {
							// If it's the end of the pfs making it as process ended
							if (strHolder.indexOf("ENDPFS") != -1 || strHolder.indexOf("ENDPART") != -1) {
								started = false;
							}

							if (flightNumberPass) {
								if (isNumber(strHolder.substring(0, 1))) {
									if (isNumber(strHolder.substring(1, 2))) {
										strHolder = new StringBuffer(strHolder).insert(2, " ").toString();
									} else {
										strHolder = new StringBuffer(strHolder).insert(1, " ").toString();
									}

								}
							} else {
								if (checkCarrierCode(strHolder.substring(0, 2))) {
									flightNumberPass = true;
								}
							}
							parserFriendlyText.append(strHolder.trim() + "\n");
						}
					}
				}
			}

			String text = parserFriendlyText.toString();
			text.replaceAll("\n", "<BR/>");
			pfsContent = text.replaceAll("\n", "<br>");
			PFSMetaDataDTO pfsMetaDataDTO = PFSParser.process(text);
			// Until we handle them in Future, excluding IDPAD pax
			excludeIDPADpax(pfsMetaDataDTO);
			pfsMetaDataDTO.setFromAddress(fromAddress);
			pfsMetaDataDTO.setPfsContent(originalPfsText.toString());
			return pfsMetaDataDTO;
		} catch (IOException e) {
			exception = e;
		} catch (ModuleException e) {
			exception = e;
			throw new ModuleException("airreservations.pfs.cannotParsePfs");
		} finally {
			// If exception occured mail to relavent parties.
			if (exception != null) {
				PfsLogDTO pfsLogDTO = new PfsLogDTO();
				pfsLogDTO.setExceptionDescription(exception.getMessage());
				pfsLogDTO.setStackTraceElements(exception.getStackTrace());
				pfsLogDTO.setPfsContent(pfsContent);
				pfsLogDTO.setFileName(f.getName());
				pfsLogDTO.setCustomDescription("Could Not Parse PFS File");
				PfsMailError.notifyError(pfsLogDTO);
			}
		}
		return null;
	}

	private static void excludeIDPADpax(PFSMetaDataDTO pfsMetaDataDTO) {
		Collection<PFSDestinationDTO> collection = pfsMetaDataDTO.getPfsDestinationDTO();
		if (collection != null) {
			for (PFSDestinationDTO pfsDestinationDTO : collection) {
				Collection<PFSRecordDTO> pfsRecords = pfsDestinationDTO.getColPfsRecordDTO();
				if (pfsRecords != null) {
					Iterator<PFSRecordDTO> pfsRecordsItr = pfsRecords.iterator();
					while(pfsRecordsItr.hasNext()) {
						PFSRecordDTO pfsRecordDTO = pfsRecordsItr.next();
						if (pfsRecordDTO.getCategory().equals("IDPAD")) {							
							pfsRecordsItr.remove();
						}
					}
				}
			}
		}

	}

	private static boolean checkCarrierCode(String carrierCode) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		Collection<String> colCarrierCodes = reservationDAO.getCarrierCodes();
		for (String string : colCarrierCodes) {
			if (string.equalsIgnoreCase(carrierCode)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns passenger final sales data transfer information
	 * 
	 * @param file
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<PaxFinalSalesDTO> getPaxFinalSalesDTO(File file) throws ModuleException {
		PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;
		Collection<String> colPaxTitles = new ArrayList<String>(passengerDAO.getPassengerTitles().keySet());
		PaxFinalSalesDTO paxFinalSalesDTO;
		PFSDestinationDTO pfsDestinationDTO;
		PFSRecordDTO pfsRecordDTO;
		RecordDTO recordDTO;
		String arrivalAirportCode;
		String category;
		String cabinClassCode;
		String bookingClass;

		PFSMetaDataDTO pfsMetaDataDTO = getPFSMetaDataDTO(file);
		ArrayList<PaxFinalSalesDTO> colPaxFinalSalesDTO = new ArrayList<PaxFinalSalesDTO>();

		if ((pfsMetaDataDTO.getDepartureDate().compareTo(new Date()) < 0)
				|| ((pfsMetaDataDTO.getDepartureDate().compareTo(CalendarUtil.getEndTimeOfDate(new Date())) <= 0) && AppSysParamsUtil
						.isAllowProcessPfsBeforeFlightDeparture())) {

			// Get the flight error occured status
			boolean flightErrorOccured = isRealDepartureDateErrorOccured(pfsMetaDataDTO);
			Set<CodeShareMCFlight> codeShareMCFlights = getCodeShareMCFlights(pfsMetaDataDTO);
			Flight flight = getMarketingCarrierFlight(pfsMetaDataDTO);
			String flightNo = (flight != null) ? flight.getFlightNumber() : pfsMetaDataDTO.getActualFlightNumber();
			pfsMetaDataDTO.setActualFlightNumber(flightNo);
			checkIfPfsAlreadyExists(pfsMetaDataDTO);

			Pfs pfs = new Pfs();
			pfs.setFlightNumber(pfsMetaDataDTO.getActualFlightNumber());
			pfs.setFromAirport(pfsMetaDataDTO.getBoardingAirport());
			pfs.setDateDownloaded(pfsMetaDataDTO.getDateDownloaded());
			pfs.setPfsContent(pfsMetaDataDTO.getPfsContent().trim().replace("\n", "<br>"));
			pfs.setFromAddress(pfsMetaDataDTO.getFromAddress());
			pfs.setDepartureDate(pfsMetaDataDTO.getRealDepartureDate());

			// checkIfPfsReceivedBeforeFlightDeparture(pfsMetaDataDTO);
			checkIfPfsReceivedBeforeFlightDeparture(pfsMetaDataDTO);
			if (pfsMetaDataDTO.getPfsDestinationDTO() != null && !pfsMetaDataDTO.getPfsDestinationDTO().isEmpty()) {
				for (Iterator<PFSDestinationDTO> itPFSDestinationDTO = pfsMetaDataDTO.getPfsDestinationDTO().iterator(); itPFSDestinationDTO
						.hasNext();) {
					pfsDestinationDTO = (PFSDestinationDTO) itPFSDestinationDTO.next();

					arrivalAirportCode = pfsDestinationDTO.getArrivalAirport();

					if (pfsDestinationDTO.getColPfsRecordDTO() != null) {
						for (Iterator<PFSRecordDTO> itPFSRecordDTO = pfsDestinationDTO.getColPfsRecordDTO().iterator(); itPFSRecordDTO
								.hasNext();) {
							pfsRecordDTO = (PFSRecordDTO) itPFSRecordDTO.next();

							category = pfsRecordDTO.getCategory();
							cabinClassCode = pfsRecordDTO.getCabinClassCode();							

							// Skipped categories that are not currently supported in our processing.
							if (category != null && !category.equalsIgnoreCase("CFMWL") && !category.equalsIgnoreCase("FQTVN")
									&& !category.equalsIgnoreCase("OFFLN") && !category.equalsIgnoreCase("GOSHN")
									&& !category.equalsIgnoreCase("CHGCL") && !category.equalsIgnoreCase("INVOL")
									&& !category.equalsIgnoreCase("CHGSG") && !category.equalsIgnoreCase("CHGFL")
									&& !category.equalsIgnoreCase("IDPAD") && !category.equalsIgnoreCase("PXLST")
									&& !category.equalsIgnoreCase("PXLST") && !category.equalsIgnoreCase("APIPX")) {

								// Skipping norec processing if they are processing via ETL
								if (category.equalsIgnoreCase("NOREC") && !AppSysParamsUtil.isProcessNorecWithPFS()) {
									continue;
								}
								String pfsFromOC = null;
								boolean verifiedPfsSource = false;
								for (Iterator<RecordDTO> itRecordDTO = pfsRecordDTO.getRecords().iterator(); itRecordDTO
										.hasNext();) {
									recordDTO = (RecordDTO) itRecordDTO.next();

									String pnr = recordDTO.getPnrNumber();
									
									if(!verifiedPfsSource){
										ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
										pfsFromOC = reservationDAO.isPFSReceivedFromOperatingCarrier(pnr,pfs.getFlightNumber(), pfs.getDepartureDate(), category) ;
									}
									verifiedPfsSource = true;
									
									paxFinalSalesDTO = new PaxFinalSalesDTO();
									
									// paxFinalSalesDTO.getCabinClassCode() can be cabin class/ logical cabin class or booking class, depends on
									// the configurations
									if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
										if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
											paxFinalSalesDTO.setCabinClassCode(cabinClassCode);
										} else {
											paxFinalSalesDTO.setBookingClassCode(cabinClassCode);
										}
									} else {
										paxFinalSalesDTO.setCabinClassCode(cabinClassCode);
									}
									
									
									paxFinalSalesDTO.setArrivalAirportCode(arrivalAirportCode);
									paxFinalSalesDTO.setMarketingFlight(recordDTO.getMarketingFlight());	
									paxFinalSalesDTO.setCategory(category);
									
									// Check Constrains
									checkConstrains(pnr, flightErrorOccured, paxFinalSalesDTO, pfs, pfsFromOC, codeShareMCFlights);

									// Setting the PaxFinalSalesDTO information
									paxFinalSalesDTO.setTitle(getPassengerDerivedInfo(recordDTO.getParseTitle(), colPaxTitles)[0]
											.toString());
									String midName = recordDTO.getMidName();
									if (midName != null) {
										midName = getPassengerDerivedInfo(midName.trim(), colPaxTitles)[1].toString();
									}
									Object[] passengerInfo = getPassengerDerivedInfo(recordDTO.getFirstNameWithTitle(),
											colPaxTitles);
									paxFinalSalesDTO.setFirstName(passengerInfo[1].toString()
											+ ((midName != null) ? (" " + midName) : ""));
									paxFinalSalesDTO.setPaxType(passengerInfo[2].toString());

									paxFinalSalesDTO.setLastName(recordDTO.getLastName());
									paxFinalSalesDTO.setDay(pfsMetaDataDTO.getDay());
									paxFinalSalesDTO.setMonth(pfsMetaDataDTO.getMonth());
									paxFinalSalesDTO.setDepartureAirportCode(pfsMetaDataDTO.getBoardingAirport());									
									paxFinalSalesDTO.setFlightNumber(pfsMetaDataDTO.getActualFlightNumber());
									paxFinalSalesDTO.setDateDownloaded(pfsMetaDataDTO.getDateDownloaded());
									paxFinalSalesDTO.setRealFlightDate(pfsMetaDataDTO.getRealDepartureDate());
																	

									colPaxFinalSalesDTO.add(paxFinalSalesDTO);
								}
							}
						}
					}
				}
			} else {
				// This means empty pfs received
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);

				// Adding empty paxFinalSalesDTO to list in order to get pfs id later. This has been implemented in a
				// way
				// that no other way to find pfs id if we received empty pfs
				paxFinalSalesDTO = new PaxFinalSalesDTO();

				if (pfsMetaDataDTO != null && pfsMetaDataDTO.getActualFlightNumber() != null) {
					paxFinalSalesDTO.setFlightNumber(pfsMetaDataDTO.getActualFlightNumber());
				}

				if (pfsMetaDataDTO != null && pfsMetaDataDTO.getBoardingAirport() != null) {
					paxFinalSalesDTO.setDepartureAirportCode(pfsMetaDataDTO.getBoardingAirport());
				}

				if (pfsMetaDataDTO != null && pfsMetaDataDTO.getRealDepartureDate() != null) {
					paxFinalSalesDTO.setRealFlightDate(pfsMetaDataDTO.getRealDepartureDate());
				}

				if (flight != null && !StringUtil.isNullOrEmpty(flight.getCsOCFlightNumber())
						&& !StringUtil.isNullOrEmpty(flight.getCsOCCarrierCode())) {
					paxFinalSalesDTO.setCodeShareEmptyPfs(true);
				}
				colPaxFinalSalesDTO.add(paxFinalSalesDTO);
			}

			if (pfs.getProcessedStatus() == null) { // if only NOREC is there in PFS processed status update here
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);
			}
			PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
			paxFinalSalesDAO.savePfsEntry(pfs);

			for (Iterator<PaxFinalSalesDTO> itColPaxFinalSalesDTO = colPaxFinalSalesDTO.iterator(); itColPaxFinalSalesDTO
					.hasNext();) {
				paxFinalSalesDTO = (PaxFinalSalesDTO) itColPaxFinalSalesDTO.next();
				paxFinalSalesDTO.setPfsId(pfs.getPpId());
			}
		}

		return colPaxFinalSalesDTO;
	}

	/**
	 * 
	 * @param pfsMetaDataDTO
	 */
	// private static void validateFlightNumber(PFSMetaDataDTO pfsMetaDataDTO) {
	// String flightNumber = pfsMetaDataDTO.getFlightNumber();
	// if (flightNumber.charAt(2) == '0') {
	// String fltNum = PlatformUtiltiies.replaceCharAt(flightNumber, 2, ' ');
	// pfsMetaDataDTO.setFlightNumber(fltNum);
	// }
	//
	// }

	/**
	 * Returns the titles and first names
	 * 
	 * It is assumed that MR, MS, CHILD codes should be there for any lines. However there could be more codes too. The
	 * first priority will be given for the MR, MS and CHILD codes TODO Find ways to make it improve
	 * 
	 * @param name
	 * @param colTitles
	 * @return
	 */
	private static Object[] getPassengerDerivedInfo(String name, Collection<String> colTitles) {
		if (name == null) {
			name = " ";
		}

		String title = ReservationInternalConstants.PassengerTitle.MR;
		String paxType = ReservationInternalConstants.PassengerType.ADULT;
		String lastFName = name;
		String dbTitle;

		if (name.endsWith("MR")) {
			title = ReservationInternalConstants.PassengerTitle.MR;
			lastFName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("MS")) {
			title = ReservationInternalConstants.PassengerTitle.MS;
			lastFName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("MRS")) {
			title = ReservationInternalConstants.PassengerTitle.MRS;
			lastFName = name.substring(0, name.length() - 3);
		} else if (name.endsWith("CHD")) {
			title = ReservationInternalConstants.PassengerTitle.CHILD;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			lastFName = name.substring(0, name.length() - 3);
		} else if (name.endsWith("MISS")) {
			title = ReservationInternalConstants.PassengerTitle.MISS;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			lastFName = name.substring(0, name.length() - 4);
		} else if (name.endsWith("MSTR")) {
			title = ReservationInternalConstants.PassengerTitle.MASTER;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			lastFName = name.substring(0, name.length() - 4);
		} else if (name.endsWith("INF")) {
		//	title = ReservationInternalConstants.PassengerTitle.CHILD;
			paxType = ReservationInternalConstants.PassengerType.INFANT;
			lastFName = name.substring(0, name.length() - 3);
		} else {
			for (Iterator<String> itColTitles = colTitles.iterator(); itColTitles.hasNext();) {
				dbTitle = (String) itColTitles.next();

				if (name.endsWith(dbTitle)) {
					title = dbTitle;
					lastFName = name.substring(0, name.length() - dbTitle.length());
				}
			}
		}

		return new Object[] { title, lastFName, paxType };
	}

	/**
	 * Will Check if identical PFS already exists in the database
	 * 
	 * @param dataDTO
	 * @throws ModuleException
	 */
	private static void checkIfPfsAlreadyExists(PFSMetaDataDTO dataDTO) throws ModuleException {
		PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;

		if (paxFinalSalesDAO.hasPFS(dataDTO.getActualFlightNumber(), dataDTO.getBoardingAirport(), dataDTO.getDepartureDate())) {
			throw new ModuleException("airreservations.arg.pfsEntry.duplicate");
		}

	}

	/**
	 * Will Check if PFS received before flight departure
	 * 
	 * @param dataDTO
	 * @throws ModuleException
	 */
	private static void checkIfPfsReceivedBeforeFlightDeparture(PFSMetaDataDTO dataDTO) throws ModuleException {
		if (dataDTO.getRealDepartureDateZulu() != null
				&& dataDTO.getRealDepartureDateZulu().after(CalendarUtil.getCurrentSystemTimeInZulu())) {
			throw new ModuleException("airreservation.pfs.flightTS.lessthanToday");
		}

	}

	/**
	 * Finds out if the real departure date error occured or not
	 * 
	 * @param pfsMetaDataDTO
	 * @return
	 * @throws ModuleException
	 */
	private static boolean isRealDepartureDateErrorOccured(PFSMetaDataDTO pfsMetaDataDTO) throws ModuleException {
		boolean errorOccured = false;
		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		Date departureDate = pfsMetaDataDTO.getDepartureDate();
		WildcardFlightSearchDto searchDto;
		WildcardFlightSearchRespDto resp;
		FlightSegmentTO flightSegmentTO;
		String arrivalAirport = "";
		if(pfsMetaDataDTO.getPfsDestinationDTO() != null 
				&& pfsMetaDataDTO.getPfsDestinationDTO().iterator().hasNext()){
			arrivalAirport = pfsMetaDataDTO.getPfsDestinationDTO().iterator().next().getArrivalAirport();
		}else if(pfsMetaDataDTO.getMapDestinationAndNoPax() != null 
				&& pfsMetaDataDTO.getMapDestinationAndNoPax().entrySet().iterator().hasNext()){
			arrivalAirport = pfsMetaDataDTO.getMapDestinationAndNoPax().entrySet().iterator().next().getKey();
		}
		
		// Checking for flight segment information
		try {			
			if(pfsMetaDataDTO.getPfsDestinationDTO()!=null && !pfsMetaDataDTO.getPfsDestinationDTO().isEmpty()){
				arrivalAirport = pfsMetaDataDTO.getPfsDestinationDTO().iterator().next().getArrivalAirport();
			}

			searchDto = new WildcardFlightSearchDto();
			searchDto.setFlightNumber(pfsMetaDataDTO.getFlightNumber());
			searchDto.setDepartureAirport(pfsMetaDataDTO.getBoardingAirport());
			searchDto.setArrivalAirport(arrivalAirport);
			searchDto.setDepartureDate(departureDate);
			searchDto.setDepartureDateExact(false);
			searchDto.setNonCNXFlight(true);

			resp = flightBD.searchFlightsWildcardBased(searchDto);

			if (resp.getBestMatch() != null && !resp.isDepartureTimeAmbiguous()) {
				flightSegmentTO = resp.getBestMatch();
				pfsMetaDataDTO.setActualFlightNumber(flightSegmentTO.getFlightNumber());
				pfsMetaDataDTO.setRealDepartureDate(flightSegmentTO.getDepartureDateTime());
				pfsMetaDataDTO.setRealDepartureDateZulu(flightSegmentTO.getDepartureDateTimeZulu());
			} else {
				pfsMetaDataDTO.setRealDepartureDate(departureDate);
				pfsMetaDataDTO.setActualFlightNumber(pfsMetaDataDTO.getFlightNumber());
				errorOccured = true;
			}

		} catch (ModuleException e) {
			pfsMetaDataDTO.setRealDepartureDate(departureDate);
			errorOccured = true;
		}

		return errorOccured;
	}

	/**
	 * Check PFS Constrains
	 * 
	 * @param pnr
	 * @param flightErrorOccured
	 * @param paxFinalSalesDTO
	 * @param pfs
	 * @param pfsFromOperatingCarrier
	 * @param codeShareMCFlights TODO
	 * @throws ModuleException
	 */
	private static void checkConstrains(String pnr, boolean flightErrorOccured,PaxFinalSalesDTO paxFinalSalesDTO, Pfs pfs,
			String pfsFromOperatingCarrier, Set<CodeShareMCFlight> codeShareMCFlights) throws ModuleException {

		boolean pnrErrorOccured = false;

		// Checking the pnr number
		try {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			boolean isExternalPnr = false;		
			if (!StringUtil.isNullOrEmpty(paxFinalSalesDTO.getMarketingFlight())
					&& paxFinalSalesDTO.getMarketingFlight().length() > 10) {
				pnrModesDTO.setExtRecordLocatorId(pnr);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadFares(true);
			} else if (!StringUtil.isNullOrEmpty(pfsFromOperatingCarrier)) {
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegViewBookingTypes(true);
			} else {
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				pnrModesDTO.setPnr(pnr);
			}
			
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);		
			if (!StringUtil.isNullOrEmpty(pfsFromOperatingCarrier)) {
				String actualBookingCode = "";
				if(ReservationInternalConstants.PfsCategories.NO_REC.equals(paxFinalSalesDTO.getCategory())){
					if(paxFinalSalesDTO.getCabinClassCode() != null){
						actualBookingCode = paxFinalSalesDTO.getCabinClassCode();
					}else if(paxFinalSalesDTO.getBookingClassCode() != null){
						actualBookingCode = paxFinalSalesDTO.getBookingClassCode();
					}
					String gdsBookingCode = getActualBCForGDSMappedBC(pfsFromOperatingCarrier,actualBookingCode);
					paxFinalSalesDTO.setBookingClassCode(gdsBookingCode);
				}else{
					ReservationSegment reservationSegment = getReservationSegment(reservation, pfs, paxFinalSalesDTO, false, null);
					if (reservationSegment != null) {
						actualBookingCode = reservationSegment.getBookingCode();
						String gdsBookingCode = getGdsMappedBCForActualBC(pfsFromOperatingCarrier,
								actualBookingCode);
						if (gdsBookingCode != null && gdsBookingCode.equals(paxFinalSalesDTO.getBookingClassCode())) {
							paxFinalSalesDTO.setBookingClassCode(actualBookingCode);
						}
	
					}
				}

			}
			
			if(reservation==null && !isExternalPnr){
				pnrModesDTO.setExtRecordLocatorId(pnr);
				pnrModesDTO.setLoadFares(true);
				reservation = ReservationProxy.getReservation(pnrModesDTO);
			}
			
			if(reservation!=null){
				paxFinalSalesDTO.setPnr(pnr);
			}
			
			if (reservation != null && ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())
					&& !StringUtil.isNullOrEmpty(reservation.getExternalRecordLocator())) {

				paxFinalSalesDTO.setExternalRecordLocator(reservation.getExternalRecordLocator());
				paxFinalSalesDTO.setPnr(reservation.getPnr());
				getReservationSegment(reservation, pfs, paxFinalSalesDTO, true, codeShareMCFlights);

			}	
		} catch (CommonsDataAccessException e) {
			pnrErrorOccured = true;
		} catch (ModuleException e) {
			pnrErrorOccured = true;
		}

		// Checking the flight validation
		if (!flightErrorOccured) {

			// Checking the pnr validation
			if (pfs.getProcessedStatus() == null
					|| ReservationInternalConstants.PfsStatus.UN_PARSED.equals(pfs.getProcessedStatus())) {

				if (!pnrErrorOccured) {
					pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);
				} else {
					pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
				}
			} else {
				pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
			}

		} else {
			pfs.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
		}
	}

	/**
	 * Find out whether it's a number or not
	 * 
	 * @param str
	 * @return
	 */
	private static boolean isNumber(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * Returns all passenger final sales DTO(s)
	 * 
	 * @param path
	 * @param strMsgName
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<PaxFinalSalesDTO> getAllPaxFinalSalesDTOs(String path, String strMsgName) throws ModuleException {
		File fileMsgName = new File(path, strMsgName);
		return getPaxFinalSalesDTO(fileMsgName);
	}

	/**
	 * Insert to the pfs entries
	 * 
	 * @param colMsgNames
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, PFSMetaDataDTO> insertPfsToDatabase(String strMsgName) throws ModuleException {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		PaxFinalSalesDAO paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
		String pfsProcessPath = airReservationConfig.getPfsProcessPath();
		Collection<PaxFinalSalesDTO> colPaxFinalSalesDTO = getAllPaxFinalSalesDTOs(pfsProcessPath, strMsgName);
		PaxFinalSalesDTO paxFinalSalesDTO;
		PfsPaxEntry pfsPaxEntry;
		String category;
		int pfsId = 0;
		String CREW_INFO = "XXXCREW";
		boolean isMetaDataFilled = false;
		PFSMetaDataDTO pfsMetaDataDTO = new PFSMetaDataDTO();
		Map<Integer, PFSMetaDataDTO> parsedMetaPfs = new HashMap<Integer, PFSMetaDataDTO>();

		for (Iterator<PaxFinalSalesDTO> iter = colPaxFinalSalesDTO.iterator(); iter.hasNext();) {
			paxFinalSalesDTO = (PaxFinalSalesDTO) iter.next();
			pfsPaxEntry = new PfsPaxEntry();

			category = paxFinalSalesDTO.getCategory();

			if (category == null) {
				pfsId = paxFinalSalesDTO.getPfsId();
				if (paxFinalSalesDTO.isCodeShareEmptyPfs()) {
					parsedMetaPfs.put(pfsId, null);
				} else {
					if (paxFinalSalesDTO != null) {
						pfsMetaDataDTO.setActualFlightNumber(paxFinalSalesDTO.getFlightNumber());
						pfsMetaDataDTO.setBoardingAirport(paxFinalSalesDTO.getDepartureAirportCode());
						pfsMetaDataDTO.setRealDepartureDate(paxFinalSalesDTO.getRealFlightDate());
					}

					parsedMetaPfs.put(pfsId, pfsMetaDataDTO);
				}
				isMetaDataFilled = true;
				continue;
			}
			if (category != null && category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.GO_SHOW)
					&& CREW_INFO.equals(paxFinalSalesDTO.getLastName())) {
				if (!isMetaDataFilled) {
					pfsMetaDataDTO.setFlightNumber(paxFinalSalesDTO.getFlightNumber());
					pfsMetaDataDTO.setBoardingAirport(paxFinalSalesDTO.getDepartureAirportCode());
					pfsMetaDataDTO.setRealDepartureDate(paxFinalSalesDTO.getRealFlightDate());
					pfsId = paxFinalSalesDTO.getPfsId();
					parsedMetaPfs.put(pfsId, pfsMetaDataDTO);
					isMetaDataFilled = true;
				}
				continue;
			}
			if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.GO_SHOW)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.GO_SHORE);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.NO_SHOW)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.NO_SHORE);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.NO_REC)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.NO_REC);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.OFF_LK)
					|| category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.OFF_LN)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.OFF_LD);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.IN_VOL)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.IN_VOL);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.CH_GCL)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.CH_GCL);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.CF_MWL)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.CF_MWL);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.GO_SHN)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.GO_SHN);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.CH_GSG)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.CH_GSG);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.CH_GFL)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.CH_GFL);
			} else if (category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.ID_PAD)) {
				pfsPaxEntry.setEntryStatus(ReservationInternalConstants.PfsPaxStatus.ID_PAD);
			}

			// paxFinalSalesDTO.getCabinClassCode() can be cabin class/ logical cabin class or booking class, depends on
			// the configurations
			if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					pfsPaxEntry.setLogicalCabinClassCode(paxFinalSalesDTO.getCabinClassCode());
				} else {
					pfsPaxEntry.setBookingClassCode(paxFinalSalesDTO.getBookingClassCode());
				}
			} else {
				pfsPaxEntry.setCabinClassCode(paxFinalSalesDTO.getCabinClassCode());
			}
			pfsPaxEntry.setDepartureAirport(paxFinalSalesDTO.getDepartureAirportCode());
			pfsPaxEntry.setTitle(paxFinalSalesDTO.getTitle());
			pfsPaxEntry.setPaxType(paxFinalSalesDTO.getPaxType());
			pfsPaxEntry.setFirstName(paxFinalSalesDTO.getFirstName());
			pfsPaxEntry.setLastName(paxFinalSalesDTO.getLastName());
			pfsPaxEntry.setFlightNumber(paxFinalSalesDTO.getFlightNumber());
			pfsPaxEntry.setPnr(paxFinalSalesDTO.getPnr());
			pfsPaxEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED);
			pfsPaxEntry.setFlightDate(paxFinalSalesDTO.getRealFlightDate());
			pfsPaxEntry.setArrivalAirport(paxFinalSalesDTO.getArrivalAirportCode());
			pfsPaxEntry.setReceivedDate(paxFinalSalesDTO.getDateDownloaded());
			pfsPaxEntry.setPfsId(new Integer(paxFinalSalesDTO.getPfsId()));
			pfsPaxEntry.setMarketingFlightElement(paxFinalSalesDTO.getMarketingFlight());
			pfsPaxEntry.setExternalRecordLocator(paxFinalSalesDTO.getExternalRecordLocator());
			pfsPaxEntry.setCodeShareBc(paxFinalSalesDTO.getCodeShareBc());
			pfsPaxEntry.setCodeShareFlightNo(paxFinalSalesDTO.getCodeShareFlightNo());
			pfsPaxEntry.setGdsId(paxFinalSalesDTO.getGdsId());
			pfsPaxEntry.setCabinClassCode(paxFinalSalesDTO.getCabinClassCode());

			// Saving the pfs parse entry
			paxFinalSalesDAO.savePfsParseEntry(pfsPaxEntry);

			if (!isMetaDataFilled
					&& category != null
					&& !(category.equalsIgnoreCase(ReservationInternalConstants.PfsCategories.GO_SHOW) && CREW_INFO
							.equals(paxFinalSalesDTO.getLastName()))) {
				pfsMetaDataDTO.setFlightNumber(paxFinalSalesDTO.getFlightNumber());
				pfsMetaDataDTO.setActualFlightNumber(paxFinalSalesDTO.getFlightNumber());
				pfsMetaDataDTO.setBoardingAirport(paxFinalSalesDTO.getDepartureAirportCode());
				pfsMetaDataDTO.setRealDepartureDate(paxFinalSalesDTO.getRealFlightDate());
				pfsId = paxFinalSalesDTO.getPfsId();
				parsedMetaPfs.put(pfsId, pfsMetaDataDTO);
				isMetaDataFilled = true;
			}
		}

		return parsedMetaPfs;
	}

	/**
	 * @param codeShareMCFlights TODO
	 * @param args
	 * 
	 *            Temporary Main Method
	 */
	// public static void main(String[] args) throws Exception {
	// // Collection msgNames = PFSFormatUtils.getMessageNames("C:\\isaconfig\\pfsprocesspath");
	// Collection<String> colFileNames = new ArrayList<String>();
	// colFileNames.add("TEST.txt");
	// // colFileNames.addAll(msgNames);
	//
	// try {
	// System.out.println(" ############### ABOUT TO PARSE " + colFileNames.size() + " DOCUMENTS ");
	// Iterator<String> itColFileNames = colFileNames.iterator();
	// String fileName;
	// while (itColFileNames.hasNext()) {
	// fileName = (String) itColFileNames.next();
	//
	// File f = new File("C:\\isaconfig\\pfsprocesspath", fileName);
	// PFSMetaDataDTO pfsMetaDataDTO = PFSDTOParser.getPFSMetaDataDTO(f);
	//
	// }
	// System.out.println(" ############### FINISHED PARSING " + colFileNames.size() + " DOCUMENTS ");
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	
	private static ReservationSegment getReservationSegment(Reservation reservation, Pfs pfs, PaxFinalSalesDTO paxFinalSalesDTO,
			boolean isCodeSharePnr, Set<CodeShareMCFlight> codeShareMCFlights) throws ModuleException {
		ReservationSegment reservationSegment = null;
		if (reservation != null && pfs != null && paxFinalSalesDTO != null) {
			Collection<FlightReconcileDTO> colFlightReconcileDTO = null;
			FlightReconcileDTO flightReconcileDTO = null;
			colFlightReconcileDTO = ReservationModuleUtils.getSegmentBD().getPnrSegmentsForReconcilation(pfs.getFlightNumber(),
					pfs.getDepartureDate(), pfs.getFromAirport(), paxFinalSalesDTO.getArrivalAirportCode(), reservation.getPnr());
			Integer flighSegId = null;
			for (Iterator<FlightReconcileDTO> iterFlightReconcileDTO = colFlightReconcileDTO.iterator(); iterFlightReconcileDTO
					.hasNext();) {
				flightReconcileDTO = (FlightReconcileDTO) iterFlightReconcileDTO.next();
				flighSegId = flightReconcileDTO.getFlightSegId();
			}

			Set<ReservationSegment> segs = reservation.getSegments();

			for (Iterator<ReservationSegment> iterSegs = segs.iterator(); iterSegs.hasNext();) {
				reservationSegment = (ReservationSegment) iterSegs.next();
				if (reservationSegment.getFlightSegId().equals(flighSegId)
						&& !reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					break;
				}
			}

			if (isCodeSharePnr && reservationSegment != null) {
				
				String csFlightNo = null;
				GDSStatusTO gdsStatusTO= ReservationApiUtils.getGDSShareCarrierTO(reservation.getGdsId());
				for(CodeShareMCFlight codeShareMCFlight: codeShareMCFlights){
					if(codeShareMCFlight.getCsMCCarrierCode().equals(gdsStatusTO.getCarrierCode())){
						csFlightNo = codeShareMCFlight.getCsMCFlightNumber();
					}
				}
				
				String csBookingClass = reservationSegment.getCodeShareBc();

				paxFinalSalesDTO.setCodeShareBc(csBookingClass);
				paxFinalSalesDTO.setCodeShareFlightNo(csFlightNo);
				paxFinalSalesDTO.setCabinClassCode(reservationSegment.getCabinClassCode());
				paxFinalSalesDTO.setGdsId(reservation.getGdsId());

				if (StringUtil.isNullOrEmpty(paxFinalSalesDTO.getMarketingFlight()) && flightReconcileDTO!=null) {					
					paxFinalSalesDTO.setMarketingFlight(ReservationApiUtils.getMarketingFlightData(csFlightNo, csBookingClass,
							flightReconcileDTO.getSegementCode(), flightReconcileDTO.getDepartureDateTimeLocal()));
				}

			}

		}

		return reservationSegment;
	}
	
	private static String getGdsMappedBCForActualBC(String gdsCarrierCode, String bookingCode) {

		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				Integer gdsId = gdsStatusTO.getGdsId();

				Map<Integer, Map<String, String>> gdsBookingClassMap = CommonsServices.getGlobalConfig().getGdsBookingClassMap();
				if (!gdsBookingClassMap.get(gdsId).isEmpty()) {
					Map<String, String> bookingClassMap = gdsBookingClassMap.get(gdsId);
					return bookingClassMap.get(bookingCode);
				}

			}
		}
		return null;
	}
	
	private static String getActualBCForGDSMappedBC(String gdsCarrierCode, String gdsBookingCode) {

		String bookingClass = null;
		Integer gdsId = null;
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				gdsId = gdsStatusTO.getGdsId();
				break;
			}
		}
		
		Map<Integer, Map<String, String>> gdsBookingClassMap = CommonsServices.getGlobalConfig().getGdsBookingClassMap();
		if (gdsId != null && !gdsBookingClassMap.get(gdsId).isEmpty()) {
			Map<String, String> bookingClassMap = gdsBookingClassMap.get(gdsId);
			for (Entry<String, String> bcEntry : bookingClassMap.entrySet()) {
				if (bcEntry.getValue().equals(gdsBookingCode)) {
					bookingClass = bcEntry.getKey();
				}
			}
		}
		
		if(bookingClass != null){
			return bookingClass;
		}
		return gdsBookingCode;
	}

	private static Flight getMarketingCarrierFlight(PFSMetaDataDTO pfsMetaDataDTO) throws ModuleException {
		String arrivalAirport = null;
		if (pfsMetaDataDTO.getPfsDestinationDTO() != null && !pfsMetaDataDTO.getPfsDestinationDTO().isEmpty()) {
			arrivalAirport = pfsMetaDataDTO.getPfsDestinationDTO().iterator().next().getArrivalAirport();
		}
		FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
		searchCriteria.setCsOcFlightNumber(pfsMetaDataDTO.getActualFlightNumber());
		searchCriteria.setEstDepartureDate(pfsMetaDataDTO.getDepartureDate());
		searchCriteria.setOrigin(pfsMetaDataDTO.getBoardingAirport());
		searchCriteria.setDestination(arrivalAirport);
		return ReservationModuleUtils.getFlightBD().getSpecificFlightDetail(searchCriteria);
	}
	
	private static Set<CodeShareMCFlight> getCodeShareMCFlights(PFSMetaDataDTO pfsMetaDataDTO) throws ModuleException {

		FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
		searchCriteria.setFlightNumber(pfsMetaDataDTO.getActualFlightNumber());
		searchCriteria.setEstDepartureDate(pfsMetaDataDTO.getDepartureDate());
		searchCriteria.setOrigin(pfsMetaDataDTO.getBoardingAirport());

		Flight flight =  ReservationModuleUtils.getFlightBD().getSpecificFlightDetail(searchCriteria);
		if(flight == null){
			return null;
		}
		return flight.getCodeShareMCFlights();
				
	}
}
