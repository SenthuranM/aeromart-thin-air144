/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.StationContactDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.service.RevenueAccountBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.SortUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.TaxInvoiceDAO;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.PaxStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Reservation Proxy is the proxy for the reservation dao related save and get functions The main purpose of this is to
 * expose an interface for dao specific proxy functionalities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationProxy {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ReservationProxy.class);

	/** Holds the pnr number of generation number of times */
	// private static final int PNR_NO_GENERATION_NO_OF_TIMES = 3;

	/** Holds the pnr number generation mapped error code */
	// private static final String PNR_NO_GENERATION_MAPPED_ERROR_CODE = "module.duplicate.key";

	/**
	 * Hide the constructor
	 */
	private ReservationProxy() {

	}

	/**
	 * Load the reservation
	 * 
	 * @param pnrModesDTO
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO) throws ModuleException {
		log.debug("Inside getReservation");

		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		// In order to load ond charges we need to have the fares already...
		// Therefore force loading
		if (pnrModesDTO.isLoadOndChargesView() && pnrModesDTO.isLoadFares() == false) {
			pnrModesDTO.setLoadFares(true);
		}

		String originatorPnr = BeanUtils.nullHandler(pnrModesDTO.getGroupPNR());
		String extRLoc = BeanUtils.nullHandler(pnrModesDTO.getExtRecordLocatorId());
		Reservation reservation;

		if (originatorPnr.length() > 0) {
			reservation = reservationDAO.getReservationByOriginatorPnr(originatorPnr, pnrModesDTO.isLoadFares());
			if (reservation == null) {
				return reservation;
			} else {
				pnrModesDTO.setPnr(reservation.getPnr());
			}
		} else if (extRLoc.length() > 0) {
			reservation = reservationDAO.getReservationByExtRecordLocator(extRLoc, pnrModesDTO.getBookingCreationDate(),
					pnrModesDTO.isLoadFares());
			if (reservation == null) {
				return reservation;
			} else {
				pnrModesDTO.setPnr(reservation.getPnr());
			}
		} else {
			reservation = reservationDAO.getReservation(pnrModesDTO.getPnr(), pnrModesDTO.isLoadFares());
		}

		if (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.YES) {
			pnrModesDTO.setLoadSeatingInfo(false);
			pnrModesDTO.setLoadMealInfo(false);
			pnrModesDTO.setLoadSSRInfo(false);
			pnrModesDTO.setLoadBaggageInfo(false);
			pnrModesDTO.setLoadAirportTransferInfo(false);
			pnrModesDTO.setLoadAutoCheckinInfo(false);
		}

		// link external flight segment info to external pnr segment
		linkExternalFlightInfoToExternalSegments(reservation);

		// Loading the segment display information if needed
		if (pnrModesDTO.isLoadSegView()) {
			loadSegmentView(reservation, pnrModesDTO);
		}

		// Loading Station Info
		loadStationInfo(reservation);

		// Loading the last user note
		if (pnrModesDTO.isLoadLastUserNote()) {
			AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
			String userNote = auditorBD.getLastUserNote(pnrModesDTO.getPnr(), pnrModesDTO.isLoadClassifyUN(),
					pnrModesDTO.getAgentCode());
			reservation.setUserNote(userNote);
		}

		// Loading the available passenger balance if needed
		if (pnrModesDTO.isLoadPaxAvaBalance()) {
			loadPaxAvaBalance(reservation);
		}

		// Loading the Ond Charges View if needed
		if (pnrModesDTO.isLoadOndChargesView()) {
			loadOndChargesView(reservation);
		}

		// Loading the pax payment tnx with ond breakdown view
		if (pnrModesDTO.isLoadPaxPaymentOndBreakdownView()) {
			loadPaxPaymentTnxAndOndBreakdownView(reservation);
		}

		// Loading the external pax payment tnx
		if (pnrModesDTO.isLoadExternalPaxPayments()) {
			loadExternalPaxPaymentView(reservation);
		}

		// Loading the passenger seating information
		if (pnrModesDTO.isLoadSeatingInfo()) {
			loadSeatingInfo(reservation);
		}

		// Loading the passenger meal information
		if (pnrModesDTO.isLoadMealInfo()) {
			loadMealInfo(reservation, pnrModesDTO.getPreferredLanguage());
		}

		if (pnrModesDTO.isLoadAirportTransferInfo()) {
			loadAirportTransferInfo(reservation);
		}

		// Loading the passenger auto checkin information
		if (pnrModesDTO.isLoadAutoCheckinInfo()) {
			loadAutoCheckinInfo(reservation);
		}

		if (pnrModesDTO.isLoadSSRInfo()) {
			loadSSRInfo(reservation);
		}

		// Loading the passenger baggage information
		if (pnrModesDTO.isLoadBaggageInfo()) {
			loadBaggageInfo(reservation, pnrModesDTO.getPreferredLanguage());
		}
		// load the insurance information if any{
		loadInsuranceInfo(reservation);

		// Loading the passenger Auto Checkin
		if (pnrModesDTO.isLoadAutoCheckinInfo()) {
			loadAutoCheckinInfo(reservation);
		}

		// Link flexi information to the reservation
		if (pnrModesDTO.isLoadFares()) {
			loadFlexiInfo(reservation);
		}

		// Loading the passenger e ticket information
		if (pnrModesDTO.isLoadEtickets()) {
			loadEticketInfo(reservation);
			populateNoShowRefundableInfo(reservation);
		}

		// Loading refundable charge adjustment.
		if (pnrModesDTO.isLoadRefundableTaxInfo()) {
			loadRefundableTaxAmounts(reservation, pnrModesDTO);
		}

		// Loading the origin country code of the reservation
		if (pnrModesDTO.isLoadOriginCountry()) {
			setOriginAgentCountryCode(reservation);
		}

		// Loading Auto Canceling Information
		if (AppSysParamsUtil.isAutoCancellationEnabled()) {
			loadAutoCancellationInfo(reservation);
		}

		// Loading External Reference Id
		if (pnrModesDTO.isLoadGOQUOAmounts()) {
			loadTotalGOQUOAmount(reservation);
		}		
		
		if (pnrModesDTO.getPnr() != null && !pnrModesDTO.getPnr().equals("")){
			setInvoiceList(reservation, pnrModesDTO);
		}
		
		log.debug("Exit getReservation");
		return reservation;
	}

	private static void loadTotalGOQUOAmount(Reservation reservation) {
		BigDecimal goquoAmount = null;
		if (reservation.getAdminInfo().getOriginChannelId() == SalesChannelsUtil.SALES_CHANNEL_GOQUO) {
			goquoAmount = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getExternalReferenceGOQUOPayments(reservation.getPnr());
		}
		if (goquoAmount != null) {
			reservation.setTotalGoquoAmount(goquoAmount);
		}

	}

	/**
	 * Sets the origin country code. This will be determined by the ip address for IBE bookings. And by originAgent's
	 * country for other bookings. Sets null to originCountryCode if there's no matching country found for IP address to
	 * country mapping.
	 * 
	 * @param reservation
	 *            Reservation which's origin country need to be determined.
	 * @throws ModuleException
	 *             If IP to Country retrieval or COuntry retrieval for originAgent throws any exception.
	 */
	private static void setOriginAgentCountryCode(Reservation reservation) throws ModuleException {

		// If it's web(IBE) or Mobile we are getting the country from the originIP address
		if (isIBEOrMobileReservation(reservation)) {
			long ip = PlatformUtiltiies.getOriginIP(reservation.getAdminInfo().getOriginIPAddress());
			Country country = ReservationModuleUtils.getCommonMasterBD().getCountryFromIPAddress(ip);
			String countryCode = country == null ? null : country.getCountryCode();
			reservation.getAdminInfo().setOriginCountryCode(countryCode);
		}
		// Otherwise from the originAgent's country
		else {
			String originAgentCountry = ReservationModuleUtils.getTravelAgentBD()
					.getAgentCountry(reservation.getAdminInfo().getOriginAgentCode());
			reservation.getAdminInfo().setOriginCountryCode(originAgentCountry);
		}
	}

	/**
	 * Checks if a reservation is made through IBE or not. TODO: Record originChannel properly when reservations are
	 * from LCC
	 * 
	 * @param reservation
	 * @return true if reservation is made form IBE or false otherwise.
	 */
	private static boolean isIBEOrMobileReservation(Reservation reservation) {
		int originChannelID = reservation.getAdminInfo().getOriginChannelId();
		boolean isIBEOrMobile = SalesChannelsUtil.isSalesChannelWebOrMobile(originChannelID);

		/*
		 * If the reservation is made from LCC there's a problem because IBE and XBE reservations both are set as LCC
		 * reservations if the booking comes through LCC. So if it's through LCC we need to check the owner channel id.
		 * XBE reservations will never be transferred to IBE so this should be safe
		 */
		if (!isIBEOrMobile && ReservationInternalConstants.SalesChannel.LCC == originChannelID
				&& SalesChannelsUtil.isSalesChannelWebOrMobile(reservation.getAdminInfo().getOwnerChannelId())) {
			isIBEOrMobile = true;
		}
		return isIBEOrMobile;
	}

	private static void linkExternalFlightInfoToExternalSegments(Reservation reservation) {
		if (reservation.getExternalReservationSegment() != null && !reservation.getExternalReservationSegment().isEmpty()) {
			for (ExternalPnrSegment externalPnrSegment : (Collection<ExternalPnrSegment>) reservation
					.getExternalReservationSegment()) {
				ExternalFlightSegment externalFlightSegment = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
						.getExternalFlightSegment(externalPnrSegment.getExternalFltSegId());
				externalPnrSegment.setExternalFlightSegment(externalFlightSegment);
			}
		}
	}

	private static void loadInsuranceInfo(Reservation reservation) throws ModuleException {

		List<ReservationInsurance> insuranceList = new ArrayList<ReservationInsurance>(
				ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getReservationInsuranceList(reservation.getPnr()));

		if (insuranceList != null && insuranceList.size() > 0) {
			if (insuranceList.size() < 1) {
				throw new ModuleException("airreservations.load.reservation");
			}
		}
		// reservation.setReservationInsurance(resInsurance);
		reservation.setReservationInsurance(insuranceList);
	}

	/**
	 * Load passenger selected seats
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadSeatingInfo(Reservation reservation) throws ModuleException {
		Collection<Integer> pnrSegIds = reservation.getPnrSegIds();
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (pnrSegIds != null && pnrSegIds.size() > 0 && pnrPaxIds != null && pnrPaxIds.size() > 0) {
			Collection<PaxSeatTO> paxSeatDTOs = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO.getReservationSeats(pnrPaxIds,
					pnrSegIds);
			reservation.setSeats(paxSeatDTOs);
		} else {
			throw new ModuleException("airreservations.load.reservation");
		}
	}

	/**
	 * Load passenger selected seats
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadMealInfo(Reservation reservation, String preferredLanguage) throws ModuleException {
		Collection<Integer> pnrSegIds = ReservationApiUtils.getPnrSegIds(reservation.getSegmentsView());
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (preferredLanguage == null) {
			preferredLanguage = reservation.getContactInfo().getPreferredLanguage();
		}

		if (pnrSegIds != null && pnrSegIds.size() > 0 && pnrPaxIds != null && pnrPaxIds.size() > 0) {
			// To display meal name in selected language - Jira 4640
			Collection<PaxMealTO> paxMealDTOs = null;
			if (preferredLanguage == null || preferredLanguage.equals("") || preferredLanguage.equalsIgnoreCase("en"))
				paxMealDTOs = ReservationDAOUtils.DAOInstance.MEAL_DAO.getReservationMeals(pnrPaxIds, pnrSegIds);
			else
				paxMealDTOs = ReservationDAOUtils.DAOInstance.MEAL_DAO.getReservationMeals(pnrPaxIds, pnrSegIds,
						preferredLanguage);

			reservation.setMeals(paxMealDTOs);
		} else {
			throw new ModuleException("airreservations.load.reservation");
		}
	}

	/**
	 * Load passenger selected automatic checkins
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadAutoCheckinInfo(Reservation reservation) throws ModuleException {
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (AppSysParamsUtil.isAutomaticCheckinEnabled()) {
			if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
				Collection<PaxAutomaticCheckinTO> paxAutoCheckinDTOs = null;
				paxAutoCheckinDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO
						.getReservationAutomaticCheckinsByPaxId(pnrPaxIds);
				reservation.setAutoCheckins(paxAutoCheckinDTOs);
			} else {
				throw new ModuleException("airreservations.load.reservation");
			}
		}
	}

	/**
	 * Load passenger selected airport transfers
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadAirportTransferInfo(Reservation reservation) throws ModuleException {
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
				Collection<PaxAirportTransferTO> paxAirportTransferDTOs = null;
				paxAirportTransferDTOs = ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO
						.getReservationAirportTransfersByPaxId(pnrPaxIds);
				reservation.setAirportTransfers(paxAirportTransferDTOs);
			} else {
				throw new ModuleException("airreservations.load.reservation");
			}
		}
	}

	/**
	 * Loads the SSR Details to reservation
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadSSRInfo(Reservation reservation) throws ModuleException {
		Collection<Integer> pnrSegIds = ReservationApiUtils.getPnrSegIds(reservation.getSegmentsView());
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (pnrSegIds != null && pnrSegIds.size() > 0 && pnrPaxIds != null && pnrPaxIds.size() > 0) {
			Collection<PaxSSRDTO> paxSSRDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getReservationSSR(pnrPaxIds,
					pnrSegIds, reservation.getContactInfo().getPreferredLanguage());
			Iterator<ReservationPax> iter = reservation.getPassengers().iterator();
			while (iter.hasNext()) {
				// Removed indikas changes with sudheeras approval
				ReservationPax resPax = (ReservationPax) iter.next();
				Collection<PaxSSRDTO> colPaxSSR = new ArrayList<PaxSSRDTO>();
				Iterator<PaxSSRDTO> ssriter = paxSSRDTOs.iterator();
				while (ssriter.hasNext()) {
					PaxSSRDTO paxSSRDTO = (PaxSSRDTO) ssriter.next();
					if (paxSSRDTO.getPnrPaxId().intValue() == resPax.getPnrPaxId().intValue()) {
						colPaxSSR.add(paxSSRDTO);
					}
				}
				resPax.setPaxSSR(colPaxSSR);
			}
		} else {
			throw new ModuleException("airreservations.load.reservation");
		}
	}

	/**
	 * In order to load ond charges view
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	public static void loadOndChargesView(Reservation reservation) throws ModuleException {
		// Getting all charges information
		Collection<ChargesDetailDTO> colChargesDetailDTO = ItineraryBL.getChargeDetails(reservation, null);

		Map<Integer, Collection<ChargesDetailDTO>> paxWiseCharges = new HashMap<Integer, Collection<ChargesDetailDTO>>();
		ChargesDetailDTO chargesDetailDTO;
		ReservationPax reservationPax;
		Collection<ChargesDetailDTO> paxColChargesDetailDTO;

		for (Iterator<ChargesDetailDTO> itChargesDetailDTO = colChargesDetailDTO.iterator(); itChargesDetailDTO.hasNext();) {
			chargesDetailDTO = (ChargesDetailDTO) itChargesDetailDTO.next();
			paxColChargesDetailDTO = (Collection<ChargesDetailDTO>) paxWiseCharges.get(chargesDetailDTO.getPnrPaxId());

			if (paxColChargesDetailDTO == null) {
				paxColChargesDetailDTO = new ArrayList<ChargesDetailDTO>();
				paxColChargesDetailDTO.add(chargesDetailDTO);

				paxWiseCharges.put(chargesDetailDTO.getPnrPaxId(), paxColChargesDetailDTO);
			} else {
				paxColChargesDetailDTO.add(chargesDetailDTO);
			}
		}

		// Setting the Ond Charges for each passenger
		for (Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = (ReservationPax) itReservationPax.next();

			paxColChargesDetailDTO = (Collection<ChargesDetailDTO>) paxWiseCharges.get(reservationPax.getPnrPaxId());

			if (paxColChargesDetailDTO != null) {
				reservationPax.setOndChargesView(paxColChargesDetailDTO);
			}
		}
	}

	/**
	 * Add the pax payment tnx and paxTnxOndBreakdown info to the ressevationPax
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadPaxPaymentTnxAndOndBreakdownView(Reservation reservation) throws ModuleException {

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			Collection<ReservationTnx> reservationTnxs = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
					.getPNRPaxPayments(String.valueOf(reservationPax.getPnrPaxId()));

			if (reservationTnxs != null && !reservationTnxs.isEmpty()) {
				if (AppSysParamsUtil.isEnableTransactionGranularity() && reservation.isEnableTransactionGranularity()) {
					Collection<Long> paxTnxIds = new ArrayList<Long>();
					for (ReservationTnx tnx : reservationTnxs) {
						paxTnxIds.add(Long.valueOf(tnx.getTnxId()));
					}
					Map<Integer, Collection<ReservationPaxOndPayment>> map = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
							.getPerPaxPaymentWiseOndPayments(reservationPax.getPnrPaxId(), paxTnxIds);
					for (ReservationTnx reservationTnx : reservationTnxs) {
						Collection<ReservationPaxOndPayment> paxOndPayments = map.get(reservationTnx.getTnxId());
						// till the zero payment issue is sorted out
						// if (paxOndPayments == null || paxOndPayments.isEmpty()) {
						// throw new ModuleException("airreservations.load.reservation");
						// }
						reservationTnx.setReservationPaxTnxOndBreakdown(paxOndPayments);
					}
				}
				reservationPax.setPaxPaymentTnxView(reservationTnxs);
			}
		}
	}

	/**
	 * add the external pax payments to the reservation pax
	 * 
	 * @param reservation
	 */
	private static void loadExternalPaxPaymentView(Reservation reservation) {

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			Collection<ReservationPaxExtTnx> reservationPaxExtTnxs = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getExternalPaxPayments(reservationPax.getPnrPaxId());

			if (reservationPaxExtTnxs != null && !reservationPaxExtTnxs.isEmpty()) {
				reservationPax.setExternalPaxPaymentTnxView(reservationPaxExtTnxs);
			}
		}
	}

	/**
	 * Returns the Segment View
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ReservationSegmentDTO> getPnrSegmentsView(String pnr, boolean loadLocalOpenReturnConfirmBeforeDate)
			throws ModuleException {

		Collection<ReservationSegmentDTO> colReservationSegmentDTO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
				.getPnrSegmentsView(pnr);

		if (loadLocalOpenReturnConfirmBeforeDate) {
			ZuluLocalTimeConversionHelper zuluLocalTimeConversionHelper = new ZuluLocalTimeConversionHelper(
					ReservationModuleUtils.getAirportBD());
			String airportCode;
			Date localDate;
			for (ReservationSegmentDTO reservationSegmentDTO : colReservationSegmentDTO) {
				if (reservationSegmentDTO.getOpenRtConfirmBeforeZulu() != null) {
					airportCode = reservationSegmentDTO.getSegmentCode().split("/")[0];
					localDate = zuluLocalTimeConversionHelper.getLocalDateTime(airportCode,
							reservationSegmentDTO.getOpenRtConfirmBeforeZulu());
					reservationSegmentDTO.setOpenRtConfirmBeforeLocal(localDate);
				}
			}
		}

		return colReservationSegmentDTO;
	}

	/**
	 * In order to load segment view information
	 * 
	 * @param reservation
	 * @param pnrModesDTO
	 * @throws ModuleException
	 */
	private static void loadSegmentView(Reservation reservation, LCCClientPnrModesDTO pnrModesDTO) throws ModuleException {
		Collection<ReservationSegmentDTO> segments = getPnrSegmentsView(pnrModesDTO.getPnr(), true);
		reservation.setSegmentsView(segments);

		// This is needed cause in order to find the fare expiry date we need to have the booking type loaded.
		if (pnrModesDTO.isLoadSegViewFareCategoryTypes() && !pnrModesDTO.isLoadSegViewBookingTypes()) {
			pnrModesDTO.setLoadSegViewBookingTypes(true);
		}

		Map<Integer, String> mapPnrSegIdAndBookingType = new HashMap<Integer, String>();
		ReservationSegment reservationSegment;
		ReservationSegmentDTO reservationSegmentDTO;
		for (Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator(); itReservationSegment
				.hasNext();) {
			reservationSegment = (ReservationSegment) itReservationSegment.next();
			mapPnrSegIdAndBookingType.put(reservationSegment.getPnrSegId(), reservationSegment.getBookingType());
		}

		for (Iterator<ReservationSegmentDTO> itReservationSegmentDTO = segments.iterator(); itReservationSegmentDTO.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
			reservationSegmentDTO.setBookingType((String) mapPnrSegIdAndBookingType.get(reservationSegmentDTO.getPnrSegId()));
		}

		// Loading the return group id if needed. Use only for open return bookings
		if (pnrModesDTO.isLoadSegViewReturnGroupId()) {
			Map<Integer, Integer> mapPnrSegIdAndReturnGroupId = new HashMap<Integer, Integer>();

			for (Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator(); itReservationSegment
					.hasNext();) {
				reservationSegment = (ReservationSegment) itReservationSegment.next();
				mapPnrSegIdAndReturnGroupId.put(reservationSegment.getPnrSegId(), reservationSegment.getReturnGrpId());
			}

			for (Iterator<ReservationSegmentDTO> itReservationSegmentDTO = segments.iterator(); itReservationSegmentDTO
					.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
				reservationSegmentDTO
						.setReturnGroupId((Integer) mapPnrSegIdAndReturnGroupId.get(reservationSegmentDTO.getPnrSegId()));
			}
		}

		// Loading the booking types if needed
		if (pnrModesDTO.isLoadSegViewBookingTypes()) {
			Map<Integer, String> mapPnrSegIdAndCabinClassCode = new HashMap<Integer, String>();
			Map<Integer, String> mapPnrSegIdAndLogicalCCCode = new HashMap<Integer, String>();

			for (Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator(); itReservationSegment
					.hasNext();) {
				reservationSegment = (ReservationSegment) itReservationSegment.next();
				mapPnrSegIdAndCabinClassCode.put(reservationSegment.getPnrSegId(), reservationSegment.getCabinClassCode());
				mapPnrSegIdAndLogicalCCCode.put(reservationSegment.getPnrSegId(), reservationSegment.getLogicalCCCode());
			}

			for (Iterator<ReservationSegmentDTO> itReservationSegmentDTO = segments.iterator(); itReservationSegmentDTO
					.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
				reservationSegmentDTO
						.setCabinClassCode((String) mapPnrSegIdAndCabinClassCode.get(reservationSegmentDTO.getPnrSegId()));
				reservationSegmentDTO
						.setLogicalCCCode((String) mapPnrSegIdAndLogicalCCCode.get(reservationSegmentDTO.getPnrSegId()));
			}
		}

		// Loading the Fare Category Information if needed
		if (pnrModesDTO.isLoadSegViewFareCategoryTypes()) {
			Map<Integer, FareTO> mapPnrSegIdAndFareTO = new HashMap<Integer, FareTO>();

			for (Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator(); itReservationSegment
					.hasNext();) {
				reservationSegment = (ReservationSegment) itReservationSegment.next();
				mapPnrSegIdAndFareTO.put(reservationSegment.getPnrSegId(),
						reservationSegment.getFareTO(pnrModesDTO.getPreferredLanguage()));
			}

			for (Iterator<ReservationSegmentDTO> itReservationSegmentDTO = segments.iterator(); itReservationSegmentDTO
					.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
				reservationSegmentDTO.setFareTO((FareTO) mapPnrSegIdAndFareTO.get(reservationSegmentDTO.getPnrSegId()));
			}

			ZuluLocalTimeConversionHelper zuluLocalTimeConversionHelper = new ZuluLocalTimeConversionHelper(
					ReservationModuleUtils.getAirportBD());
			Map<Integer, Date> segmentExpiryDate = getFareExpiryDate(segments);
			Iterator<ReservationSegmentDTO> itReservationSegmentDTO = segments.iterator();
			Date zuluExpiryDate;
			String airportCode;
			Date localDate;

			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
				zuluExpiryDate = segmentExpiryDate.get(reservationSegmentDTO.getPnrSegId());

				if (zuluExpiryDate != null) {
					reservationSegmentDTO.setSegmentExpiryDateZulu(zuluExpiryDate);
					airportCode = reservationSegmentDTO.getSegmentCode().split("/")[0];
					localDate = zuluLocalTimeConversionHelper.getLocalDateTime(airportCode, zuluExpiryDate);
					reservationSegmentDTO.setSegmentExpiryDateLocal(localDate);
				}
			}
		}

		composeReservationSegmentDisplayStates(segments);

	}

	public static void composeReservationSegmentDisplayStates(Collection<ReservationSegmentDTO> segments) throws ModuleException {
		for (ReservationSegmentDTO reservationSegmentDTO : segments) {
			if (FlightStatusEnum.CANCELLED.getCode().equals(reservationSegmentDTO.getFlightStatus())
					&& (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())
							|| ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING
									.equals(reservationSegmentDTO.getStatus()))
					&& (!BookingClass.BookingClassType.OPEN_RETURN.equals(reservationSegmentDTO.getBookingType()))) {
				reservationSegmentDTO.setDisplayStatus(ReservationInternalConstants.ReservationSegmentStatus.FLIGHT_CANCEL);
			} else {
				reservationSegmentDTO.setDisplayStatus(reservationSegmentDTO.getStatus());
			}
		}
	}

	/**
	 * In order to load passenger available balances
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadPaxAvaBalance(Reservation reservation) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (reservation.isInfantPaymentRecordedWithInfant()) {
				pnrPaxIds.add(reservationPax.getPnrPaxId());
			}else{
				if (!ReservationApiUtils.isInfantType(reservationPax)) {
					pnrPaxIds.add(reservationPax.getPnrPaxId());
				}
			}
			
		}

		BigDecimal pnrBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal paidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Avoding a fatal error if by any chance reservation found with a infant left over
		if (pnrPaxIds.size() > 0) {
			// Getting the passenger balances for all the passengers
			RevenueAccountBD revenueAccountBD = ReservationModuleUtils.getRevenueAccountBD();
			Map<Integer, BigDecimal> mapPaxBalances = revenueAccountBD.getPNRPaxBalances(pnrPaxIds);

			itReservationPax = reservation.getPassengers().iterator();

			BigDecimal paxBalance;

			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();

				if (reservation.isInfantPaymentRecordedWithInfant() || !ReservationApiUtils.isInfantType(reservationPax)) {
					paxBalance = (BigDecimal) mapPaxBalances.get(reservationPax.getPnrPaxId());

					if (paxBalance == null) {
						reservationPax.setAccountHolder(false);
						reservationPax.setTotalPaidAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						if (reservation.isInfantPaymentRecordedWithInfant()) {
							reservationPax.setTotalAvailableBalance((reservationPax.getTotalChargeAmountWhenInfantHasPayment()));
						} else {
							reservationPax.setTotalAvailableBalance((reservationPax.getTotalChargeAmount()));
						}
					} else {
						// Total Paid Amount = Total Charges - Passenger Balance
						reservationPax.setAccountHolder(true);
						BigDecimal totalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (reservation.isInfantPaymentRecordedWithInfant()) {
							totalChargeAmount = reservationPax.getTotalChargeAmountWhenInfantHasPayment();
						} else {
							totalChargeAmount = reservationPax.getTotalChargeAmount();
						}
						reservationPax.setTotalPaidAmount(
								AccelAeroCalculator.subtract(totalChargeAmount, paxBalance));
						reservationPax.setTotalAvailableBalance(paxBalance);
					}

					paidAmount = AccelAeroCalculator.add(paidAmount, reservationPax.getTotalPaidAmount());
					pnrBalance = AccelAeroCalculator.add(pnrBalance, reservationPax.getTotalAvailableBalance());
				}
			}
		}

		// Setting the reservation total available balance and paid amounts
		reservation.setTotalPaidAmount(paidAmount);
		reservation.setTotalAvailableBalance(pnrBalance);

	}

	/**
	 * Performs proxy activities before saving or updating a reservation
	 * 
	 * @param reservation
	 * @return TODO
	 */
	public static Reservation saveReservation(Reservation reservation) {
		return saveOrUpdateReservation(reservation);
	}

	/**
	 * Save or Update the reservation
	 * 
	 * @param reservation
	 * @return The saved reservation
	 */
	private static Reservation saveOrUpdateReservation(Reservation reservation) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;

		// Save the reservation
		return reservationDAO.saveReservation(reservation);
	}

	/**
	 * Get Fare Expiry Date If fare is a return fare and has maximum stayover
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static Map<Integer, Date> getFareExpiryDate(Collection<ReservationSegmentDTO> segments) throws ModuleException {
		boolean isSegmentExpiryDateDisplayEnabled = AppSysParamsUtil.isSegmentExpiryDateDisplayEnabled();
		Map<Integer, Date> segMap = new HashMap<Integer, Date>();
		List<List<ReservationSegmentDTO>> ondWiseResSegList = ondWiseNotCancelledReservationSegmentList(segments);

		if (ondWiseResSegList.size() == 2) {
			Date expDate = new Date();
			Date expDateOpRet = new Date();
			List<ReservationSegmentDTO> obSegList = ondWiseResSegList.get(OndSequence.OUT_BOUND);
			List<ReservationSegmentDTO> ibSegList = ondWiseResSegList.get(OndSequence.IN_BOUND);

			ReservationSegmentDTO lastOBSegment = obSegList.get(obSegList.size() - 1);

			expDate = CalendarUtil.add(lastOBSegment.getZuluArrivalDate(), 0,
					(int) lastOBSegment.getFareTO().getMaximumStayOverMonths(), 0, 0,
					(int) lastOBSegment.getFareTO().getMaximumStayOver(), 0);

			if (AppSysParamsUtil.isAllowHalfReturnFaresForOpenReturnBookings()) {

				ReservationSegmentDTO lastIBSegment = ibSegList.get(ibSegList.size() - 1);

				expDateOpRet = CalendarUtil.add(lastOBSegment.getZuluArrivalDate(), 0,
						(int) lastIBSegment.getFareTO().getMaximumStayOverMonths(), 0, 0,
						(int) lastIBSegment.getFareTO().getMaximumStayOver(), 0);

				if (expDate.compareTo(expDateOpRet) > 0) {
					expDate = expDateOpRet;
				}
			}

			if (ibSegList != null && ibSegList.size() > 0) {
				for (ReservationSegmentDTO ibResSegment : ibSegList) {
					if ((isSegmentExpiryDateDisplayEnabled && ibResSegment.getFareTO().isPrintExpiryDate())
							|| ibResSegment.isOpenReturnSegment()) {
						segMap.put(ibResSegment.getPnrSegId(), expDate);
					}
				}
			}
		}

		return segMap;
	}

	private static List<List<ReservationSegmentDTO>>
			ondWiseNotCancelledReservationSegmentList(Collection<ReservationSegmentDTO> segments) {
		List<ReservationSegmentDTO> lstSegments = new ArrayList<ReservationSegmentDTO>(segments);
		Collections.sort(lstSegments);
		LinkedHashMap<Integer, List<ReservationSegmentDTO>> ondWiseResSegList = new LinkedHashMap<Integer, List<ReservationSegmentDTO>>();
		for (ReservationSegmentDTO reservationSegmentDTO : segments) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())) {
				continue;
			}
			if (!ondWiseResSegList.containsKey(reservationSegmentDTO.getJourneySeq())) {
				ondWiseResSegList.put(reservationSegmentDTO.getJourneySeq(), new ArrayList<ReservationSegmentDTO>());
			}

			ondWiseResSegList.get(reservationSegmentDTO.getJourneySeq()).add(reservationSegmentDTO);
		}

		List<List<ReservationSegmentDTO>> lstOndWiseSegments = new ArrayList<List<ReservationSegmentDTO>>();

		int count = 0;
		for (Entry<Integer, List<ReservationSegmentDTO>> entry : ondWiseResSegList.entrySet()) {
			lstOndWiseSegments.add(count++, entry.getValue());
		}

		return lstOndWiseSegments;
	}

	private static void loadFlexiInfo(Reservation reservation) {
		Collection<Integer> collPpfId = new ArrayList<Integer>();
		for (Iterator<ReservationPax> paxIterator = reservation.getPassengers().iterator(); paxIterator.hasNext();) {
			ReservationPax reservationPax = (ReservationPax) paxIterator.next();
			for (Iterator<ReservationPaxFare> paxFareIterator = reservationPax.getPnrPaxFares().iterator(); paxFareIterator
					.hasNext();) {
				collPpfId.add(((ReservationPaxFare) paxFareIterator.next()).getPnrPaxFareId());
			}
		}
		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> reservationFlexies = ReservationDAOUtils.DAOInstance.FLEXI_RULE_DAO
				.getReservationPaxOndFlexibilities(collPpfId);
		reservation.setPaxOndFlexibilities(reservationFlexies);
	}

	public static void saveOrUpdateReservationInsurance(ReservationInsurance reservationInsurance) {
		ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveReservationInsurance(reservationInsurance);

	}

	private static void loadBaggageInfo(Reservation reservation, String preferredLanguage) throws ModuleException {

		Collection<Integer> pnrSegIds = ReservationApiUtils.getPnrSegIds(reservation.getSegmentsView());
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();

		if (preferredLanguage == null) {
			preferredLanguage = reservation.getContactInfo().getPreferredLanguage();
		}

		if (pnrSegIds != null && pnrSegIds.size() > 0 && pnrPaxIds != null && pnrPaxIds.size() > 0) {

			Collection<PaxBaggageTO> paxBaggageDTOs = null;

			paxBaggageDTOs = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.getReservationBaggages(pnrPaxIds, pnrSegIds,
					AppSysParamsUtil.isONDBaggaeEnabled(), preferredLanguage);

			reservation.setBaggages(paxBaggageDTOs);
		} else {
			throw new ModuleException("airreservations.load.reservation");
		}
	}

	/**
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void loadEticketInfo(Reservation reservation) throws ModuleException {

		Collection<EticketTO> paxEticketTOs = ReservationDAOUtils.DAOInstance.ETicketDAO
				.getReservationETickets(reservation.getPnr());
		Iterator<ReservationPax> passengerIterator = reservation.getPassengers().iterator();
		while (passengerIterator.hasNext()) {
			ReservationPax resPax = passengerIterator.next();

			Map<Integer, Integer> eTicketSequnceSegmentWise = new HashMap<Integer, Integer>();
			Collection<EticketTO> eTickets = new ArrayList<EticketTO>();
			for (EticketTO eticketTO : paxEticketTOs) {

				if (eticketTO.getPnrPaxId().intValue() == resPax.getPnrPaxId().intValue()) {

					if (eTicketSequnceSegmentWise.get(eticketTO.getPnrSegId()) == null) {
						eTicketSequnceSegmentWise.put(eticketTO.getPnrSegId(), 1);
					} else {
						eTicketSequnceSegmentWise.put(eticketTO.getPnrSegId(),
								eTicketSequnceSegmentWise.get(eticketTO.getPnrSegId()) + 1);
					}
					eticketTO.seteTicketSequnce(eTicketSequnceSegmentWise.get(eticketTO.getPnrSegId()));
					eTickets.add(eticketTO);
				}
			}
			resPax.seteTickets(eTickets);
		}
	}

	/**
	 * Load the refundable tax amounts to the reservation.
	 * 
	 * @param reservation
	 *            Reservation the refundable tax amounts need to be loaded to.
	 * @param pnrModesDTO
	 *            With the calling carrier set.
	 */
	private static void loadRefundableTaxAmounts(Reservation reservation, LCCClientPnrModesDTO pnrModesDTO) {

		Map<Integer, Collection<RefundableChargeDetailDTO>> rfChargeAmounts = ReservationBO
				.getRefunndableChargeAmount(reservation.getPnrPaxIds(), ReservationInternalConstants.ChargeGroup.TAX);

		reservation.setRefundableChargeDetails(rfChargeAmounts);
	}

	private static void loadAutoCancellationInfo(Reservation reservation) throws ModuleException {
		Integer autoCnxId = getAutoCancellationId(reservation);
		if (autoCnxId != null && autoCnxId != 0) {
			AutoCancellationInfo autoCancellationInfo = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
					.getAutoCancellationInfo(autoCnxId);
			reservation.setAutoCancellationInfo(autoCancellationInfo);
		}
	}

	private static void loadStationInfo(Reservation reservation) throws ModuleException {
		if (reservation.getSegmentsView() != null && reservation.getSegmentsView().size() > 0) {
			ReservationSegmentDTO[] resSegs = SortUtil.sortByDepDate(reservation.getSegmentsView());
			// List<Map<String, String>> stationList = new ArrayList<Map<String, String>>();
			Set<String> tempAirports = new LinkedHashSet<String>();
			Set<StationContactDTO> stationSet = new HashSet<StationContactDTO>();

			for (ReservationSegmentDTO segment : resSegs) {

				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())) {
					continue;
				}

				String[] airportCodes = segment.getSegmentCode().split("/");
				for (String airportCode : airportCodes) {
					tempAirports.add(airportCode);
				}
			}

			for (String strAirport : tempAirports) {
				Map<String, String> stationContactDetails = AirproxyModuleUtils.getLocationDB()
						.getStationContactDetails(strAirport);

				if (stationContactDetails != null) {
					StationContactDTO stationContactDTO = new StationContactDTO();
					stationContactDTO.setStationCode(stationContactDetails.get("stationCode"));
					stationContactDTO.setStationName(stationContactDetails.get("stationName"));
					stationContactDTO.setStationContact(stationContactDetails.get("stationContact"));
					stationContactDTO.setStationTelephone(stationContactDetails.get("stationTelephone"));
					stationSet.add(stationContactDTO);
				}
			}
			reservation.setStations(stationSet);
		}

	}

	private static Integer getAutoCancellationId(Reservation reservation) throws ModuleException {
		Set<ReservationSegment> segments = reservation.getSegments();
		if (segments != null) {
			for (ReservationSegment reservationSegment : segments) {
				if (reservationSegment.getAutoCancellationId() != null) {
					return reservationSegment.getAutoCancellationId();
				}
			}
		}

		Set<ReservationPax> passengers = reservation.getPassengers();
		if (passengers != null) {
			for (ReservationPax reservationPax : passengers) {
				if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())
						&& reservationPax.getAutoCancellationId() != null) {
					return reservationPax.getAutoCancellationId();
				} else {
					Collection<PaxSSRDTO> paxSSRs = reservationPax.getPaxSSR();
					if (paxSSRs != null && !paxSSRs.isEmpty()) {
						for (PaxSSRDTO paxSSRDTO : paxSSRs) {
							if (paxSSRDTO.getAutoCancellationId() != null) {
								return paxSSRDTO.getAutoCancellationId();
							}
						}
					}
				}
			}
		}

		Collection<PaxSeatTO> seats = reservation.getSeats();
		if (seats != null && !seats.isEmpty()) {
			for (PaxSeatTO paxSeatTO : seats) {
				if (paxSeatTO.getAutoCancellationId() != null) {
					return paxSeatTO.getAutoCancellationId();
				}
			}
		}

		Collection<PaxMealTO> meals = reservation.getMeals();
		if (meals != null && !meals.isEmpty()) {
			for (PaxMealTO paxMealTO : meals) {
				if (paxMealTO.getAutoCancellationId() != null) {
					return paxMealTO.getAutoCancellationId();
				}
			}
		}

		Collection<PaxBaggageTO> baggages = reservation.getBaggages();
		if (baggages != null && !baggages.isEmpty()) {
			for (PaxBaggageTO paxBaggageTO : baggages) {
				if (paxBaggageTO.getAutoCancellationId() != null) {
					return paxBaggageTO.getAutoCancellationId();
				}
			}
		}

		Collection<ReservationInsurance> insurances = reservation.getReservationInsurance();
		if (insurances != null && !insurances.isEmpty()) {
			for (ReservationInsurance reservationInsurance : insurances) {
				if (reservationInsurance != null && reservationInsurance.getAutoCancellationId() != null) {
					return reservationInsurance.getAutoCancellationId();
				}
			}
		}

		return null;
	}

	public static Reservation getReservation(long promoCriteriaID) {

		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		String criteriaParam = BeanUtils.nullHandler(promoCriteriaID);

		Reservation reservation = null;

		if (criteriaParam.length() > 0) {
			reservation = reservationDAO.getReservationByPromoCriteria(criteriaParam, promoCriteriaID);
		}
		return reservation;
	}

	public static Collection<Reservation> getReservationsByPnr(Collection<String> pnrList, boolean loadFares)
			throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getReservationsByPnr(pnrList, loadFares);
	}

	public static Collection<Reservation> getReservationsByFlightSegmentId(Integer flightSegId, boolean loadFares)
			throws ModuleException {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getReservationsByFlightSegmentId(flightSegId, loadFares);
	}
	
	private static void populateNoShowRefundableInfo(Reservation reservation) {
		BigDecimal refundableAmount;
		boolean isPaxFareSegRefundable = false;
		for (ReservationPax resPax : reservation.getPassengers()) {
			refundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			List<Integer> paxFareIds = new ArrayList<Integer>();
			for (EticketTO eticket : resPax.geteTickets()) {
				if (EticketStatus.OPEN.code().equals(eticket.getTicketStatus())
						&& PaxStatus.NO_SHOW.code().equals(eticket.getPaxStatus())) {
					isPaxFareSegRefundable = false;
					PAX_FARE: for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
						if (paxFareIds.contains(reservationPaxFare.getPnrPaxFareId())) {
							continue PAX_FARE;
						}

						isPaxFareSegRefundable = isValidPaxFareForNoShowTaxReverse(reservationPaxFare.getPaxFareSegments(),
								eticket);

						if (isPaxFareSegRefundable && !paxFareIds.contains(reservationPaxFare.getPnrPaxFareId())) {
							paxFareIds.add(reservationPaxFare.getPnrPaxFareId());
							refundableAmount = AccelAeroCalculator.add(refundableAmount,
									getRefundableTaxAmount(reservationPaxFare.getCharges(), resPax.getOndChargesView()));

						}
					}
				}
			}
			resPax.setNoShowRefundable(refundableAmount.doubleValue() > 0 ? true : false);
			resPax.setNoShowRefundableAmount(refundableAmount);
		}
	}

	private static String getChargeCode(Integer chargeRateId, Collection<ChargesDetailDTO> ondChargesView) {
		if (ondChargesView != null && !ondChargesView.isEmpty()) {
			for (ChargesDetailDTO chargesDetailDTO : ondChargesView) {
				if (chargesDetailDTO.getChargeRateId() != null && chargesDetailDTO.getChargeRateId().equals(chargeRateId)) {
					return chargesDetailDTO.getChargeCode();
				}
			}
		}
		return null;
	}

	private static BigDecimal getRefundableTaxAmount(Set<ReservationPaxOndCharge> charges,
			Collection<ChargesDetailDTO> ondChargesView) {

		BigDecimal refundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (charges != null && !charges.isEmpty()) {
			for (ReservationPaxOndCharge reservationPaxOndCharge : charges) {
				if (ReservationInternalConstants.ChargeGroup.TAX.equals(reservationPaxOndCharge.getChargeGroupCode())) {
					try {
						Charge charge = ReservationModuleUtils.getChargeBD()
								.getCharge(getChargeCode(reservationPaxOndCharge.getChargeRateId(), ondChargesView));
						if (charge.getRefundableChargeFlag() == 1) {
							refundableAmount = AccelAeroCalculator.add(refundableAmount, reservationPaxOndCharge.getAmount());
						}
					} catch (ModuleException e) {
						log.error(" Fetching ChargeRateId:" + reservationPaxOndCharge.getChargeRateId() + " error :" + e);
					}
				}
			}
		}

		return refundableAmount;
	}

	private static boolean isValidPaxFareForNoShowTaxReverse(Set<ReservationPaxFareSegment> paxFareSegments, EticketTO eticket) {
		boolean allowRefund = false;
		boolean validPaxFareSegment = false;
		if (paxFareSegments != null && !paxFareSegments.isEmpty()) {
			for (ReservationPaxFareSegment reservationPaxFareSegment : paxFareSegments) {
				if (reservationPaxFareSegment.getPnrPaxFareSegId().equals(eticket.getPnrPaxFareSegId())) {
					validPaxFareSegment = true;
				}
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
						.equals(reservationPaxFareSegment.getSegment().getStatus())
						&& ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE
								.equals(reservationPaxFareSegment.getStatus())) {
					allowRefund = true;
				} else {
					allowRefund = false;
					break;
				}
			}

			if (validPaxFareSegment && allowRefund) {
				return true;
			}

		}

		return false;
	}
	
	private static void setInvoiceList(Reservation reservation, LCCClientPnrModesDTO pnrModesDTO) throws ModuleException {
		TaxInvoiceDAO taxInvoiceDAO = ReservationDAOUtils.DAOInstance.TAX_INVOICE_DAO;
		List<TaxInvoice> taxInvoicesList = taxInvoiceDAO.getTaxInvoiceForPrintInvoice(pnrModesDTO.getPnr());
		if (taxInvoicesList != null && !taxInvoicesList.isEmpty()){
			setAdditionalInfoForTaxInvoice(reservation, taxInvoicesList);
			reservation.setTaxInvoicesList(taxInvoicesList);
		}
	}
	
	private static void setAdditionalInfoForTaxInvoice(Reservation reservation, List<TaxInvoice> taxInvoicesList) throws ModuleException {
		String nameOfTheAirline = AppSysParamsUtil.getDefaultCarrierName();
		for (TaxInvoice taxInvoice :taxInvoicesList){
			taxInvoice.setNameOfTheAirline(nameOfTheAirline);
			if (taxInvoice.getStateCode() != null) {
				State state = ReservationModuleUtils.getCommonMasterBD().getState(taxInvoice.getStateCode());
				String gstinForInvoiceState = state.getAirlineOfficeTaxRegNo();
				taxInvoice.setAirlineOfficeSTAddress1(state.getAirlineOfficeSTAddress1());
				taxInvoice.setAirlineOfficeSTAddress2(state.getAirlineOfficeSTAddress2());
				taxInvoice.setAirlineOfficeCity(state.getAirlineOfficeCity());
				taxInvoice.setGstinForInvoiceState(gstinForInvoiceState);
				taxInvoice.setDigitalSignForInvoiceState(state.getDigitalSignature());
				if (state.getCountryCode() != null){
					Country country = ReservationModuleUtils.getCommonMasterBD().getCountryByName(state.getCountryCode());
					if (country != null)
						taxInvoice.setAirlineOfficeCountryCode(country.getCountryName());
				}
			}
			if (reservation.getContactInfo() != null) {
				taxInvoice.setNameOfTheRecipient(reservation.getContactInfo().getFirstName() + " "
						+ reservation.getContactInfo().getLastName());
				taxInvoice.setGstinOfTheRecipient(reservation.getContactInfo().getTaxRegNo());
				taxInvoice.setRecipientStreetAddress1(reservation.getContactInfo().getStreetAddress1());
				taxInvoice.setRecipientStreetAddress2(reservation.getContactInfo().getStreetAddress2());
				taxInvoice.setRecipientCity(reservation.getContactInfo().getCity());
				if (reservation.getContactInfo().getState() != null) {
					State stateContactInfo = ReservationModuleUtils.getCommonMasterBD().getState(
							reservation.getContactInfo().getState());
					if (stateContactInfo != null) {
						taxInvoice.setStateOfTheRecipient(stateContactInfo.getStateName());
						taxInvoice.setStateCodeOfTheRecipient(stateContactInfo.getStateCode());
					}
				}
				if (reservation.getContactInfo().getCountryCode() != null) {
					Country country = ReservationModuleUtils.getCommonMasterBD().getCountryByName(
							reservation.getContactInfo().getCountryCode());
					if (country != null)
						taxInvoice.setRecipientCountryCode(country.getCountryName());
				}
			}
			taxInvoice.setAccountCodeOfService(AppSysParamsUtil.getGstHsnSac());
			taxInvoice.setDescriptionOfService(AppSysParamsUtil.getGstGoodsServices());
			taxInvoice.setTotalValueOfService(AccelAeroCalculator.add(taxInvoice.getTaxableAmount(),
					taxInvoice.getNonTaxableAmount()));
			taxInvoice.setTotalTaxAmount(AccelAeroCalculator.add(taxInvoice.getTaxRate1Amount(),
					taxInvoice.getTaxRate2Amount(), taxInvoice.getTaxRate3Amount()));

			State taxApplicableState = getApplicableTaxState(taxInvoice);
			if (taxApplicableState != null) {
				taxInvoice.setPlaceOfSupply(taxApplicableState.getStateName());
				taxInvoice.setPlaceOfSupplyState(taxApplicableState.getStateCode());
			}

			if (taxInvoice.getTaxRate1Id() != null) {
				taxInvoice.setTaxRate1Percentage(getChargeRateValuePercentage(taxInvoice.getTaxRate1Id()));
			}
			if (taxInvoice.getTaxRate2Id() != null) {
				taxInvoice.setTaxRate2Percentage(getChargeRateValuePercentage(taxInvoice.getTaxRate2Id()));
			}
			if (taxInvoice.getTaxRate3Id() != null) {
				taxInvoice.setTaxRate3Percentage(getChargeRateValuePercentage(taxInvoice.getTaxRate3Id()));
			}
			
			if (taxInvoice.getOriginalTaxInvoiceId() != null){
				taxInvoice.setDateOfIssueOriginalTaxInvoice(getOriginalTaxInvoiceIssueDate(taxInvoice.getOriginalTaxInvoiceId()));
			}
		}
	}
	
	private static Date getOriginalTaxInvoiceIssueDate(Integer originalTaxInvoiceId) throws ModuleException {
		TaxInvoiceDAO taxInvoiceDAO = ReservationDAOUtils.DAOInstance.TAX_INVOICE_DAO;
		Date originalTaxInvoiceIssueDate = new Date();
		originalTaxInvoiceIssueDate = taxInvoiceDAO.getTaxInvoiceFor(originalTaxInvoiceId).getDateOfIssue();
		return originalTaxInvoiceIssueDate;
	}
	
	private static State getApplicableTaxState(TaxInvoice taxInvoice) throws ModuleException {
		State taxApllicableState = ReservationModuleUtils.getCommonMasterBD().getTaxApplicableState(
				getAppliedTaxRateID(taxInvoice));
		return taxApllicableState;
	}

	private static double getChargeRateValuePercentage(int chargeRateId) throws ModuleException {
		ChargeRate chargeRate = ReservationModuleUtils.getCommonMasterBD().getChargeRateValue(chargeRateId);
		return chargeRate.getChargeValuePercentage();
	}
	
	private static Integer getAppliedTaxRateID(TaxInvoice taxInvoice) {
		Integer appliedTax = -1;
		if (taxInvoice.getTaxRate2Amount().signum() > 0) {
			appliedTax = taxInvoice.getTaxRate2Id();
		} else if (taxInvoice.getTaxRate3Amount().signum() > 0) {
			appliedTax = taxInvoice.getTaxRate3Id();
		}
		return appliedTax;
	}

}
