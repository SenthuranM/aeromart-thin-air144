package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.PassengerCheckinDTO;
import com.isa.thinair.airreservation.api.dto.revacc.AdjustCreditTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * ReservationTnxDAO is the business DAO interface for the reservation payment apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationTnxDAO {

	public ReservationTnx getTransaction(long tnxId);

	/**
	 * Method to get multiple transaction with one invocation.
	 * 
	 * @param tnxIDs
	 *            A colleciton of transaction IDs.
	 * @return A list of fully loaded ReservationTnx objects related to the Ids passed
	 */
	public List<ReservationTnx> getTransactions(Collection<Integer> tnxIDs);

	public Map<Integer, ReservationPaxTnxBreakdown> getReservationPaxTnxBreakdown(Collection<Integer> tnxIds);

	public List<Integer> getAllPnrPaxIds();

	public ReservationTnx getLccTransaction(String pnrPaxId, String lccUniqueId);

	public void saveTransaction(ReservationTnx tnx);

	public BigDecimal getPNRPaxBalance(String pnrPaxId);

	public Map<Integer, BigDecimal> getPNRPaxBalances(Collection<Integer> pnrPaxIds) throws CommonsDataAccessException;

	public Collection<ReservationTnx> getPNRPaxPayments(String pnrPaxId);

	public Collection<AdjustCreditTO> getExpiredCredits(Date date) throws CommonsDataAccessException;

	/**
	 * Return pnr passenger payments
	 * 
	 * Key --> pnrPaxId(java.lang.Integer) Value --> Collection of ReservationTnx
	 * 
	 * @param pnrPaxIds
	 * @return
	 */
	public Map<Integer, Collection<ReservationTnx>> getPNRPaxPaymentsAndRefunds(Collection<Integer> pnrPaxIds);

	public Map<Integer, Collection<ReservationTnx>> getPNRPaxRecords(Collection<Integer> pnrPaxIds, List<Integer> nominalRecords);

	public void saveTransaction(ReservationPaxTnxBreakdown tnx);

	public Map<Integer, Collection<ReservationTnx>> getAllBalanceTransactions(Collection<Integer> pnrPaxIds);

	public Collection<PassengerCheckinDTO> getPNRPaxPendingPayments(Collection<PassengerCheckinDTO> colPassengerCheckinDTO);

	/**
	 * Retrieve ReservationPaxExtTnx for a given id
	 * 
	 * @param paxExternalTnxId
	 * @return
	 */
	public ReservationPaxExtTnx getExternalTransaction(long paxExternalTnxId);

	public boolean hasPayments(String pnr);

	public boolean isVoidReservationByPnrPaxIdOrPayTnxId(String pnrPaxId, String type);

	public void saveTransactionSegment(TransactionSegment trnxSegment);

	public List<TransactionSegment> getTransactionSegment(List<Integer> txnIds);

	public Collection<ReservationTnx> getPNRPaxTransactions(String pnrPaxId);
	

}
