/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.util.PaymentUtil;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * commanad to modify record
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="recordCreditAdjust"
 */
public class RecordCreditAdjust extends DefaultBaseCommand {

	// Dao's
	private ReservationTnxDAO reservationTnxDao;

	private ReservationCreditDAO reservationCreditDAO;

	/**
	 * constructor of the RecordCreditAdjust command
	 */
	public RecordCreditAdjust() {
		// looking up daos
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		reservationCreditDAO = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
	}

	/**
	 * execute method of the RecordCreditAdjust command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal adjustedAmount = (BigDecimal) this.getParameter(CommandParamNames.MODIFY_AMOUNT);
		ReservationTnxNominalCode reservationTnxNominalCode = (ReservationTnxNominalCode) this
				.getParameter(CommandParamNames.ACTION_NC);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Date expiryDate = (Date) this.getParameter(CommandParamNames.EXPIRY_DATE);
		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = (ReservationPaxPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO);
		boolean enableTransactionGranularity = (Boolean) this.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);
		Integer originalPaymentTnxID = (Integer) this.getParameter(CommandParamNames.ORIGINAL_PAYMENT_TNX_ID);
		Set<Long> refundedPaxOndChargeIds = new HashSet<Long>();

		Boolean isManualRefund = getParameter(CommandParamNames.IS_MANUAL_REFUND, Boolean.FALSE, Boolean.class);
		Integer transactionSeq = getParameter(CommandParamNames.TRANSACTION_SEQ, Integer.class);

		// checking params
		this.checkParams(pnrPaxId, reservationTnxNominalCode, adjustedAmount, credentialsDTO);

		ReservationTnx reservationTnx = null;
		if (transactionSeq == null) {
			transactionSeq = retrieveCorrectTransactionSeq(pnrPaxId);
		}

		if (0 < adjustedAmount.doubleValue()) {

			reservationTnx = TnxFactory.getDebitInstance(pnrPaxId, adjustedAmount, reservationTnxNominalCode.getCode(),
					credentialsDTO, new Date(), null, null, false);
			reservationTnx.setRemarks(userNotes);
			reservationTnxDao.saveTransaction(reservationTnx);

		} else if (adjustedAmount.doubleValue() < 0) {
			reservationTnx = TnxFactory.getCreditInstance(pnrPaxId, adjustedAmount.negate(), reservationTnxNominalCode.getCode(),
					credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null, null, null, false, true);
			reservationTnx.setRemarks(userNotes);
			reservationTnxDao.saveTransaction(reservationTnx);

			// If expiryDate is null, system will pick default system expire period
			ReservationCredit credit = CreditFactory.getInstance(reservationTnx.getTnxId().intValue(), pnrPaxId,
					adjustedAmount.abs(), expiryDate);
			// re-instate credit tnx id updated as reference, required in refund flow to check the tnx-breakdown flow
			Integer paymentRefTxnId = (Integer) this.getParameter(CommandParamNames.PAX_TXN_ID);
			if (ReservationTnxNominalCode.CREDIT_ACQUIRE.equals(reservationTnxNominalCode) && paymentRefTxnId != null) {
				credit.setCreditReInstatedTnxId(paymentRefTxnId);
			}
			reservationCreditDAO.saveReservationCredit(credit);
		} else {
			// If FOC meals are enabled or in-flight services are enabled then adjusted amount can be zero
			if (!AppSysParamsUtil.isFOCMealSelectionEnabled() && !AppSysParamsUtil.isInFlightServicesEnabled()) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}
		}

		if (reservationTnxNominalCode.equals(ReservationTnxNominalCode.CREDIT_ACQUIRE)) {
			// We have to treat credit acquire separately. We don't have a ond charge for credit acquire similar to
			// refund.
			PayCurrencyDTO payCurrencyDTO = ReservationApiUtils
					.getPayCurrencyDTOFromBaseCurrency(AppSysParamsUtil.getBaseCurrency());
			reservationTnx.setOriginalPaymentTnxID(originalPaymentTnxID);

			Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx = new HashMap<Long, Map<String, BigDecimal>>();

			TnxGranularityFactory.saveReservationPaxTnxBreakdownForRefund(reservationTnx, payCurrencyDTO,
					enableTransactionGranularity, null, null, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);
			// Have to execute balance credit command separately. We can push this to
			// saveReservationPaxTnxBreakdownForRefund and remove this from recordRefund class
			Command cmdBalanceCredit = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
			cmdBalanceCredit.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
			cmdBalanceCredit.setParameter(CommandParamNames.PAX_PAY_CREDIT_UTILIZED_MAP_BY_TNX, creditUtilizedMapByTnx);

			cmdBalanceCredit.execute();
		} else {
			TnxGranularityFactory.saveReservationPaxTnxBreakdownForAdjustments(pnrPaxId, reservationTnx,
					reservationPaxPaymentMetaTO, enableTransactionGranularity, transactionSeq);
		}
		
		AppIndicatorEnum appIndicatorEnum = credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null ? credentialsDTO.getTrackInfoDTO().getAppIndicator() : null;
		PaymentUtil.cancelPreviousOfflineTransaction(pnr, appIndicatorEnum, true);

		return new DefaultServiceResponse(true);
	}

	private Integer retrieveCorrectTransactionSeq(String pnrPaxId) {
		Integer maxTxnSeq = 0;
		try {
			if (pnrPaxId != null) {
				maxTxnSeq = ReservationModuleUtils.getRevenueAccountBD().getPNRPaxMaxTransactionSeq(Integer.parseInt(pnrPaxId));
			}
		} catch (ModuleException e) {
			maxTxnSeq = 1;
		}
		return maxTxnSeq;
	}

	/**
	 * Private method to validate parameters
	 * 
	 * @param pnrPaxId
	 * @param reservationTnxNominalCode
	 * @param chargeAmount
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxId, ReservationTnxNominalCode reservationTnxNominalCode, BigDecimal chargeAmount,
			CredentialsDTO credentialsDTO) throws ModuleException {

		if (pnrPaxId == null || chargeAmount == null || reservationTnxNominalCode == null || credentialsDTO == null)
			throw new ModuleException("airreservations.arg.invalid.null");
	}
}
