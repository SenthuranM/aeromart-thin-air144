package com.isa.thinair.airreservation.core.bl.pnrgov;

import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVTrasmissionDetailsDTO;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PnrGovDAO;

public class PNRGOVSchedulingBL {

	private PnrGovDAO pnrGovTimingDAO = null;

	public PNRGOVSchedulingBL() {
		this.pnrGovTimingDAO = ReservationDAOUtils.DAOInstance.PNRGOV_DAO;
	}

	public List<PNRGOVTrasmissionDetailsDTO> getFlightsForPnrGovScheuduling(String airportCode, int timePeriod,
			int pnrGovTrasmissionGap, boolean isOutbound) {
		Date date = new Date();
		return pnrGovTimingDAO.getFlightsForPnrGovScheduling(airportCode, timePeriod, pnrGovTrasmissionGap, date, isOutbound);
	}

}
