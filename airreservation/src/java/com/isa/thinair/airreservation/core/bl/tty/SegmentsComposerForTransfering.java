package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import com.isa.thinair.airinventory.api.dto.seatavailability.CodeshareMcFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TransferSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class SegmentsComposerForTransfering implements SegmentsComposingStrategy {

	@Override
	public List<BookingSegmentDTO> composeSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {

		int paxCount = TTYMessageCreatorUtil.getPaxCount(reservation, typeBRequestDTO);
		List<BookingSegmentDTO> bookingSegmentDTOs = new ArrayList<BookingSegmentDTO>();
		BookingSegmentDTO segment;
		BookingSegmentDTO newSegment;
		String mcCarrierCode = null;
		if (ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())) {
			mcCarrierCode = ReservationApiUtils.getGDSShareCarrierTO(reservation.getGdsId()).getCarrierCode();
		}

		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (!ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equalsIgnoreCase(reservationSegmentDTO
					.getSubStatus())) {

				if (reservationSegmentDTO.getPnrSegId().equals(typeBRequestDTO.getReprotectedPnrSegId())
						&& typeBRequestDTO.getReprotectedSourceFltSeg() != null
						&& typeBRequestDTO.getReprotectedTargetFltSeg() != null) {

					FlightSegmentDTO sourceFlgSegment = typeBRequestDTO.getReprotectedSourceFltSeg();
					segment = TypeBSegmentAdopter.adoptFlightSegmentToTypeBSegment(sourceFlgSegment, reservationSegmentDTO,
							GDSApiUtils.getGDSCarrierCode(reservation.getGdsId()), paxCount);
					if (reservationSegmentDTO.getCodeShareFlightNo() != null
							&& reservationSegmentDTO.getCodeShareFlightNo().length() > 2) {
						segment.setCsFlightNumber(reservationSegmentDTO.getCodeShareFlightNo().substring(2));
						segment.setCsCarrierCode(reservationSegmentDTO.getCodeShareFlightNo().substring(0, 2));
						segment.setCsBookingCode(reservationSegmentDTO.getCodeShareBc());
					}
					segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED.getCode());
					bookingSegmentDTOs.add(segment);

					FlightSegmentDTO targetFlgSegment = typeBRequestDTO.getReprotectedTargetFltSeg();
					newSegment = TypeBSegmentAdopter.adoptFlightSegmentToTypeBSegment(targetFlgSegment, reservationSegmentDTO,
							GDSApiUtils.getGDSCarrierCode(reservation.getGdsId()), paxCount);
					if (mcCarrierCode != null) {
						String mcFlightNumber = getMCFlightNumber(targetFlgSegment, mcCarrierCode);
						if (mcFlightNumber != null && mcFlightNumber.length() > 2) {
							newSegment.setCsFlightNumber(mcFlightNumber.substring(2));
							newSegment.setCsCarrierCode(mcFlightNumber.substring(0, 2));
							newSegment.setCsBookingCode(reservationSegmentDTO.getCodeShareBc());
						}
					}
					newSegment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED.getCode());
					bookingSegmentDTOs.add(newSegment);

				} else {
					segment = TypeBSegmentAdopter.adoptResSegmentToTypeBSegment(reservationSegmentDTO,
							GDSApiUtils.getGDSCarrierCode(reservation.getGdsId()), paxCount);
					if (reservationSegmentDTO.getCodeShareFlightNo() != null
							&& reservationSegmentDTO.getCodeShareFlightNo().length() > 2) {
						segment.setCsFlightNumber(reservationSegmentDTO.getCodeShareFlightNo().substring(2));
						segment.setCsCarrierCode(reservationSegmentDTO.getCodeShareFlightNo().substring(0, 2));
						segment.setCsBookingCode(reservationSegmentDTO.getCodeShareBc());
					}
					if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
						segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					} else {
						segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CANCELLED.getCode());
					}
					bookingSegmentDTOs.add(segment);
				}
			}
		}
		return bookingSegmentDTOs;
	}

	@Override
	public List<BookingSegmentDTO> composeCSSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {

		int paxCount = TTYMessageCreatorUtil.getPaxCount(reservation, typeBRequestDTO);
		List<BookingSegmentDTO> bookingSegmentDTOs = new ArrayList<BookingSegmentDTO>();
		BookingSegmentDTO segment;
		BookingSegmentDTO newSegment;
		Map<Integer, TransferSegmentDTO> transferSegmentMap = new HashMap<Integer, TransferSegmentDTO>();
		for (TransferSegmentDTO transferSegmentDTO : typeBRequestDTO.getTransferSegmentDTOs()) {
			transferSegmentMap.put(transferSegmentDTO.getPnrSegId(), transferSegmentDTO);
		}

		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		GDSStatusTO gdsStatusTO = globalConfig.getActiveGdsMap().values().stream().filter(new Predicate<GDSStatusTO>() {
			public boolean test(GDSStatusTO gdsStatusTO) {
				return gdsStatusTO.getCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode());
			}
		}).findFirst().get();

		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (!ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equalsIgnoreCase(reservationSegmentDTO
					.getSubStatus())) {

				if (transferSegmentMap.keySet().contains(reservationSegmentDTO.getPnrSegId())) {
					TransferSegmentDTO transferSegmentDTO = transferSegmentMap.get(reservationSegmentDTO.getPnrSegId());
					FlightSegmentDTO sourceFlgSegment = transferSegmentDTO.getSourceFlightSegment();
					FlightSegmentDTO targetFlgSegment = transferSegmentDTO.getTargetFlightSegment();

					if (sourceFlgSegment.getCsOcCarrierCode() != null
							&& sourceFlgSegment.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())) {
						segment = TypeBSegmentAdopter.adoptFlightSegmentToTypeBCSSegment(sourceFlgSegment, reservationSegmentDTO,
								typeBRequestDTO.getCsOCCarrierCode(), paxCount);
						segment.setPrimeFlight(gdsStatusTO.getPrimeFlightEnabled());
						segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED.getCode());
						bookingSegmentDTOs.add(segment);

					} else if (TTYMessageUtil.getOALSegmentsSendingEligibility(typeBRequestDTO.getCsOCCarrierCode())) {
						segment = TypeBSegmentAdopter.adoptFlightSegmentToTypeBSegment(sourceFlgSegment, reservationSegmentDTO,
																						typeBRequestDTO.getCsOCCarrierCode(), paxCount);
						segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED.getCode());
						bookingSegmentDTOs.add(segment);
					}

					if (targetFlgSegment.getCsOcCarrierCode() != null
							&& targetFlgSegment.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())) {
						newSegment = TypeBSegmentAdopter.adoptFlightSegmentToTypeBCSSegment(targetFlgSegment,
								reservationSegmentDTO, typeBRequestDTO.getCsOCCarrierCode(), paxCount);
						newSegment.setPrimeFlight(gdsStatusTO.getPrimeFlightEnabled());
						newSegment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED.getCode());
						bookingSegmentDTOs.add(newSegment);

					} else if (TTYMessageUtil.getOALSegmentsSendingEligibility(typeBRequestDTO.getCsOCCarrierCode())) {
						newSegment = TypeBSegmentAdopter.adoptFlightSegmentToTypeBSegment(targetFlgSegment,
								reservationSegmentDTO, typeBRequestDTO.getCsOCCarrierCode(), paxCount);
						newSegment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED.getCode());
						bookingSegmentDTOs.add(newSegment);
					}

				} else if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equalsIgnoreCase(reservationSegmentDTO
						.getStatus())) {

					if (reservationSegmentDTO.getCsOcCarrierCode() != null
							&& reservationSegmentDTO.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())) {
						segment = TypeBSegmentAdopter.adoptResSegmentToTypeBCSSegment(reservationSegmentDTO,
								typeBRequestDTO.getCsOCCarrierCode(), paxCount);
						segment.setPrimeFlight(gdsStatusTO.getPrimeFlightEnabled());
						if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
							segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
						} else {
							segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CANCELLED.getCode());
						}
						bookingSegmentDTOs.add(segment);
					} else if (TTYMessageUtil.getOALSegmentsSendingEligibility(typeBRequestDTO.getCsOCCarrierCode())) {

						segment = TypeBSegmentAdopter.adoptResSegmentToTypeBSegment(reservationSegmentDTO,
								typeBRequestDTO.getCsOCCarrierCode(), paxCount);
						if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
							segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
						} else {
							segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CANCELLED.getCode());
						}
						bookingSegmentDTOs.add(segment);
					}
				}
			}
		}
		return bookingSegmentDTOs;
	}

	private static String getMCFlightNumber(FlightSegmentDTO flgSegment, String mcCarrierCode) {
		if (flgSegment.getCsMcFlights() != null && !flgSegment.getCsMcFlights().isEmpty()) {
			for (CodeshareMcFlightDTO csMcFlightDTO : flgSegment.getCsMcFlights()) {
				if (csMcFlightDTO.getCsMcCarrierCode().equals(mcCarrierCode)) {
					return csMcFlightDTO.getCsMcFlightNumber();
				}
			}
		}
		return null;
	}
}
