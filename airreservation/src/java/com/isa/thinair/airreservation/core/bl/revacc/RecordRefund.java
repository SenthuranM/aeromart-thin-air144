/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TnxAgentPayment;
import com.isa.thinair.airreservation.api.model.TnxCardPayment;
import com.isa.thinair.airreservation.api.model.TnxLoyaltyPayment;
import com.isa.thinair.airreservation.api.model.TnxPayment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * commanad to adjust refund
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="recordRefund"
 */
public class RecordRefund extends DefaultBaseCommand {

	// Dao's
	private ReservationTnxDAO reservationTnxDao;

	// private ReservationCreditDAO reservationCreditDao;

	private ReservationPaymentDAO reservationPaymentDAO;

	// private TravelAgentBD travelAgentBD;

	/**
	 * constructor of the AcquireCredit command
	 */
	public RecordRefund() {

		// looking up daos
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		// reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		// travelAgentBD = ReservationModuleUtils.getTravelAgentBD();
	}

	/**
	 * execute method of the AcquireCredit command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal refundAmount = (BigDecimal) this.getParameter(CommandParamNames.AMOUNT);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Collection payments = (Collection) this.getParameter(CommandParamNames.PAYMENT_LIST);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		Collection<String> preferredPaymentOrder = (Collection<String>) this
				.getParameter(CommandParamNames.PREFERRED_REFUND_ORDER);
		Collection<Long> pnrPaxOndChgIds = (Collection<Long>) this.getParameter(CommandParamNames.PNR_PAX_OND_CHARGE_IDS);
		boolean enableTransactionGranularity = (Boolean) this.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);

		Boolean isManualRefund = getParameter(CommandParamNames.IS_MANUAL_REFUND, Boolean.FALSE, Boolean.class);

		// Validate the parameters
		this.checkParams(pnrPaxId, refundAmount, payments, credentialsDTO);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		// Get the passenger payment amount
		BigDecimal pnrPaxPaymentAmount = this.getPassengerPaymentAmount(pnrPaxId);
		Set<Long> refundedPaxOndChargeIds = new HashSet<Long>(); // TODO Remove: as not needed any more

		Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx = new HashMap<Long, Map<String, BigDecimal>>();

		if (pnrPaxPaymentAmount.doubleValue() < 0) {

			// +pnrPaxPaymentAmount >= refundAmount (if not throw exception)
			// Requesting refund for more than the paid amount.
			if (refundAmount.compareTo(pnrPaxPaymentAmount.negate()) > 0) {

				BigDecimal difference = refundAmount.subtract(pnrPaxPaymentAmount.negate()).abs();

				// We allow up to 0.03 difference
				if (difference.doubleValue() > 0.03) {
					throw new ModuleException("revac.payments.invalid");
				}

				// Now set the refund amount to be system total paid amount
				refundAmount = pnrPaxPaymentAmount.negate();
			}

			if (pnrPaxPaymentAmount.negate().compareTo(refundAmount) >= 0) {

				// Debit acount pnr pax transaction
				Iterator itPayments = payments.iterator();

				while (itPayments.hasNext()) {
					ReservationTnx debitTnx = null;
					ITnxPayment payment = (ITnxPayment) itPayments.next();

					// Validation is done for total payment so coming to payment level...
					refundAmount = payment.getAmount();

					// Card Diners
					if (payment.getPaymentType() == PaymentType.CARD_DINERS.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_DINERS.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxCardPayment cardPayment = (TnxCardPayment) payment;
						setPayReference(cardPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);

						saveCardInfo(cardPayment, pnrPaxId, debitTnx.getTnxId().intValue());
					}

					// Card Visa
					if (payment.getPaymentType() == PaymentType.CARD_VISA.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_VISA.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxCardPayment cardPayment = (TnxCardPayment) payment;
						setPayReference(cardPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);

						saveCardInfo(cardPayment, pnrPaxId, debitTnx.getTnxId().intValue());
					}

					// Card Amex
					if (payment.getPaymentType() == PaymentType.CARD_AMEX.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_AMEX.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxCardPayment cardPayment = (TnxCardPayment) payment;
						setPayReference(cardPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);

						saveCardInfo(cardPayment, pnrPaxId, debitTnx.getTnxId().intValue());
					}

					// Card Generic
					if (payment.getPaymentType() == PaymentType.CARD_GENERIC.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_GENERIC.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxCardPayment cardPayment = (TnxCardPayment) payment;
						setPayReference(cardPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);

						saveCardInfo(cardPayment, pnrPaxId, debitTnx.getTnxId().intValue());
					}

					// Card CMI
					if (payment.getPaymentType() == PaymentType.CARD_CMI.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_CMI.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxCardPayment cardPayment = (TnxCardPayment) payment;
						setPayReference(cardPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);

						saveCardInfo(cardPayment, pnrPaxId, debitTnx.getTnxId().intValue());
					}

					// Card Master
					if (payment.getPaymentType() == PaymentType.CARD_MASTER.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_MASTER.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxCardPayment cardPayment = (TnxCardPayment) payment;
						setPayReference(cardPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);

						saveCardInfo(cardPayment, pnrPaxId, debitTnx.getTnxId().intValue());
					}

					// On Account
					if (payment.getPaymentType() == PaymentType.ON_ACCOUNT.getTypeValue()) {
						TnxAgentPayment agentPayment = (TnxAgentPayment) payment;

						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_ONACCOUNT.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());
						debitTnx.setAgentCode(BeanUtils.nullHandler(agentPayment.getAgentCode()));

						String creditSharedAgent = ReservationModuleUtils.getTravelAgentBD().getCreditSharingAgentCode(
								debitTnx.getAgentCode());
						if (!debitTnx.getAgentCode().equals(creditSharedAgent)) {
							debitTnx.setCreditSharedAgentCode(creditSharedAgent);
						}

						setPayReference(agentPayment.getPaymentReferanceTO(), debitTnx);
						saveOnAccountTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity,
								preferredPaymentOrder, pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund,
								creditUtilizedMapByTnx);
					}
					// BSP refund
					if (payment.getPaymentType() == PaymentType.BSP.getTypeValue()) {
						TnxAgentPayment agentPayment = (TnxAgentPayment) payment;

						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_BSP.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());
						debitTnx.setAgentCode(BeanUtils.nullHandler(agentPayment.getAgentCode()));

						// TODO:RW handle for BSP
						// String creditSharedAgent =
						// ReservationModuleUtils.getTravelAgentBD().getCreditSharingAgentCode(
						// debitTnx.getAgentCode());
						// if (!debitTnx.getAgentCode().equals(creditSharedAgent)) {
						// debitTnx.setCreditSharedAgentCode(creditSharedAgent);
						// }

						setPayReference(agentPayment.getPaymentReferanceTO(), debitTnx);
						saveOnAccountTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity,
								preferredPaymentOrder, pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund,
								creditUtilizedMapByTnx);
					}

					// Cash Payment
					if (payment.getPaymentType() == PaymentType.CASH.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_CASH.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);

						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxPayment cashPayment = (TnxPayment) payment;
						setPayReference(cashPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);
					}

					// PAYPAL
					if (payment.getPaymentType() == PaymentType.PAYPAL.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_PAYPAL.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxCardPayment cardPayment = (TnxCardPayment) payment;
						setPayReference(cardPayment.getPaymentReferanceTO(), debitTnx);
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);

						saveCardInfo(cardPayment, pnrPaxId, debitTnx.getTnxId().intValue());
					}

					// LMS Payments
					if (payment.getPaymentType() == PaymentType.LOYALTY_PAYMENT.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, payment.getAmount(),
								ReservationTnxNominalCode.LOYALTY_PAYMENT.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());

						TnxLoyaltyPayment loyaltyPayment = (TnxLoyaltyPayment) payment;
						debitTnx.setExternalReference(loyaltyPayment.getLoyaltyMemberAccountId());

						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);
					}
					
					// Offline Payments
					if (payment.getPaymentType() == PaymentType.OFFLINE.getTypeValue()) {
						debitTnx = TnxFactory.getDebitInstance(pnrPaxId, refundAmount,
								ReservationTnxNominalCode.REFUND_GENERIC.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), payment.getPaymentCarrier(),
								payment.getLccUniqueTnxId(), false);
						debitTnx.setRemarks(userNotes);
						debitTnx.setOriginalPaymentTnxID(payment.getPaymentTnxId());
						saveTnxAndDebit(payment, debitTnx, credentialsDTO, enableTransactionGranularity, preferredPaymentOrder,
								pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund, creditUtilizedMapByTnx);
					}
				}
			} else {
				throw new ModuleException("revac.payments.invalid");
			}

			Command cmdBalanceCredit = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
			cmdBalanceCredit.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
			cmdBalanceCredit.setParameter(CommandParamNames.PAX_PAY_CREDIT_UTILIZED_MAP_BY_TNX, creditUtilizedMapByTnx);

			response = (DefaultServiceResponse) cmdBalanceCredit.execute();
			response.addResponceParam(CommandParamNames.REFUNDED_PAX_OND_CHG_IDS, refundedPaxOndChargeIds);
		} else {
			throw new ModuleException("revac.payments.invalid");
		}

		return response;
	}

	/**
	 * Record on account tnx and if relevant record the dry tnx as well .
	 * 
	 * Note : Dry agents can also refund to an operating carrier agent. Thus we will be adding dry tnx only if the
	 * refund is done to marketing carrier agent. Otherwise normal on account refund will happen.
	 * 
	 * @param payment
	 * @param debitTnx
	 * @param credentialsDTO
	 * @param preferredRefundOrder
	 *            Order the refund should deduct charges. (TAX,SUR etc.)
	 * @param pnrPaxOndChgIds
	 *            PaxOndChgIds related to the particular segment that charge should be deducted from.
	 * @param isManualRefund
	 * @param creditUtilizedMapByTnx
	 *            TODO
	 * @throws ModuleException
	 */
	private void saveOnAccountTnxAndDebit(ITnxPayment payment, ReservationTnx debitTnx, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity, Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds,
			Set<Long> refundedPaxOndChargeIds, Boolean isManualRefund, Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx)
			throws ModuleException {

		debitTnx.setPayCurrencyAmount(payment.getPayCurrencyDTO().getTotalPayCurrencyAmount());
		debitTnx.setPayCurrencyCode(payment.getPayCurrencyDTO().getPayCurrencyCode());

		reservationTnxDao.saveTransaction(debitTnx);
		TnxGranularityFactory.saveReservationPaxTnxBreakdownForRefund(debitTnx, payment.getPayCurrencyDTO(),
				enableTransactionGranularity, preferredRefundOrder, pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund,
				creditUtilizedMapByTnx);

	}

	private void saveTnxAndDebit(ITnxPayment payment, ReservationTnx debitTnx, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity, Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds,
			Set<Long> refundedPaxOndChargeIds, Boolean isManualRefund, Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx)
			throws ModuleException {

		debitTnx.setPayCurrencyAmount(payment.getPayCurrencyDTO().getTotalPayCurrencyAmount());
		debitTnx.setPayCurrencyCode(payment.getPayCurrencyDTO().getPayCurrencyCode());

		reservationTnxDao.saveTransaction(debitTnx);
		TnxGranularityFactory.saveReservationPaxTnxBreakdownForRefund(debitTnx, payment.getPayCurrencyDTO(),
				enableTransactionGranularity, preferredRefundOrder, pnrPaxOndChgIds, refundedPaxOndChargeIds, isManualRefund,
				creditUtilizedMapByTnx);

	}

	private void setPayReference(PaymentReferenceTO paymentReferanceTO, ReservationTnx reservationTnx) {
		if (paymentReferanceTO != null) {
			String paymentRef = BeanUtils.nullHandler(paymentReferanceTO.getPaymentRef());
			Integer actPaymentMode = (paymentReferanceTO.getActualPaymentMode() != null
					&& paymentReferanceTO.getActualPaymentMode() != 0 ? paymentReferanceTO.getActualPaymentMode() : null);
			reservationTnx.setExternalReference(paymentRef);
			reservationTnx.setExternalReferenceType(actPaymentMode);
		}
	}

	/**
	 * Return the passenger payment amount
	 * 
	 * @param pnrPaxId
	 * @return
	 */
	private BigDecimal getPassengerPaymentAmount(String pnrPaxId) {
		Collection<ReservationTnx> conReservationTnx = reservationTnxDao.getPNRPaxPayments(pnrPaxId);
		Iterator<ReservationTnx> itReservationTnx = conReservationTnx.iterator();
		ReservationTnx reservationTnx;
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

		while (itReservationTnx.hasNext()) {
			reservationTnx = (ReservationTnx) itReservationTnx.next();
			amount = AccelAeroCalculator.add(amount, reservationTnx.getAmount());
		}

		return amount;
	}

	/**
	 * Save Card information
	 * 
	 * @param cardPayment
	 * @param pnrPaxId
	 * @param transactionId
	 */
	private void saveCardInfo(TnxCardPayment cardPayment, String pnrPaxId, int transactionId) {

		CreditCardDetail cardDetail = new CreditCardDetail();

		cardDetail.setAddress(cardPayment.getAddress());
		cardDetail.setEDate(cardPayment.getCardExpiryDate());
		cardDetail.setName(cardPayment.getCardName());
		cardDetail.setNoFirstDigits(BeanUtils.getFirst6Digits(cardPayment.getCardNumber()));
		cardDetail.setNoLastDigits(BeanUtils.getLast4Digits(cardPayment.getCardNumber()));
		cardDetail.setTransactionId(transactionId);
		cardDetail.setType(cardPayment.getPaymentType());
		cardDetail.setPnrPaxId(new Integer(pnrPaxId));
		cardDetail.setSecurityCode(cardPayment.getSecCode());
		cardDetail.setPnr(cardPayment.getPnr());
		cardDetail.setPaymentBrokerRefNo(cardPayment.getPaymentBrokerRefNumber());

		reservationPaymentDAO.saveCreditCardDetail(cardDetail);
	}

	/**
	 * Private method to validate parameters
	 * 
	 * @param pnrPaxId
	 * @param amount
	 * @param payments
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkParams(String pnrPaxId, BigDecimal amount, Collection payments, CredentialsDTO credentialsDTO)
			throws ModuleException {

		if (pnrPaxId == null || amount == null || amount.doubleValue() <= 0 || payments.size() == 0 || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}
}
