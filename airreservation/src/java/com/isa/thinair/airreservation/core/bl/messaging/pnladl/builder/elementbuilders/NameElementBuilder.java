/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.intermediate.MessageTypeIdentifier;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.EndElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.NameElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.EndElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.NameElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.NameElementBuilderTemplate;

/**
 * @author udithad
 *
 */
public class NameElementBuilder extends MessageTypeIdentifier {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private PassengerStoreTypes ongoingStoreType;
	private PassengerElementsContext pEContext;
	private UtilizedPassengerContext utilizedPassengerContext;
	private UtilizedPassengerContext dummyUtilizedPassengerContext;

	private List<PassengerInformation> utilizedPassengerInformations;
	private List<PassengerInformation> dummyUtilizedPassengerInformations;
	private DestinationFare destinationFare;
	private Map<String, Integer> pnrWiseUtilizedPassengerCount;

	private String previousPnr;
	private String ongoingPnr;

	private final int INITIAL_LINE_COUNT = 5;

	private Integer passengerCount = 0;
	private Integer lineCount = INITIAL_LINE_COUNT;

	private BaseRuleExecutor<RulesDataContext> nameElementRuleExecutor = new NameElementRuleExecutor();
	private BaseRuleExecutor<RulesDataContext> endPartRuleExecutor = new EndElementRuleExecutor();

	private boolean isNoPassengersAmmended = false;
	
	Log logger = LogFactory.getLog(NameElementBuilder.class);

	@Override
	public void buildElement(ElementContext context) {
		initContextData(context);
		nameElementTemplate();
		loopBack(context);
	}

	private void initContextData(ElementContext context) {
		utilizedPassengerInformations = new ArrayList<PassengerInformation>();
		dummyUtilizedPassengerInformations = new ArrayList<PassengerInformation>();
		utilizedPassengerContext = new UtilizedPassengerContext();
		dummyUtilizedPassengerContext = new UtilizedPassengerContext();

		pnrWiseUtilizedPassengerCount = new HashMap<String, Integer>();

		pEContext = (PassengerElementsContext) context;
		currentLine = pEContext.getCurrentMessageLine();
		messageLine = pEContext.getMessageString();
		destinationFare = pEContext.getDestinationFare();
		ongoingStoreType = pEContext.getOngoingStoreType();
		if (pEContext.getPassengersPerPart() != null) {
			passengerCount = context.getPassengersPerPart();
		}
		createUtilizedPassengerContext(context);
		createDummyUtilizedPassengerContext(context);
		clearCurrentElement();
	}

	private void setLastGroupCode() {
		pEContext.setLastGroupCode(utilizedPassengerContext.getLastGroupCode());
	}

	private void createUtilizedPassengerContext(ElementContext context) {
		utilizedPassengerContext.setCurrentMessageLine(context.getCurrentMessageLine());
		utilizedPassengerContext.setMessageString(context.getMessageString());
		utilizedPassengerContext.setLastGroupCode(context.getLastGroupCode());
		utilizedPassengerContext.setMessageType(context.getMessageType());

		utilizedPassengerContext.setUtilizedPassengers(utilizedPassengerInformations);
		utilizedPassengerContext.setPnrWisePassengerCount(context.getPnrWisePassengerCount());
		utilizedPassengerContext.setPnrWisePassengerReductionCount(context.getPnrWisePassengerReductionCount());
		utilizedPassengerContext.setGroupCodesList(context.getGroupCodesList());
		utilizedPassengerContext.setPnrWiseGroupCodes(context.getPnrWiseGroupCodes());
		utilizedPassengerContext.setFeaturePack(context.getFeaturePack());
		utilizedPassengerContext.setOngoingStoreType(pEContext.getOngoingStoreType());
		utilizedPassengerContext.setLineCount(0);
		utilizedPassengerContext.setPnrPaxVsGroupCodes(pEContext.getPnrPaxVsGroupCodes());
		utilizedPassengerContext.setDepartureAirportCode(pEContext.getDepartureAirportCode());
		utilizedPassengerContext.setDestinationAirportCode(pEContext.getDestinationFare().getDestinationAirportCode());
		utilizedPassengerContext.setGroupCodeAvailabilityMap(pEContext.getGroupCodeAvailabilityMap());
	}

	private void createDummyUtilizedPassengerContext(ElementContext context) {
		StringBuilder dummyCurrentLine = new StringBuilder(context.getCurrentMessageLine().toString());
		HashMap<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerCount = (HashMap<PassengerStoreTypes, Map<String, Integer>>) context
				.getPnrWisePassengerCount();
		HashMap<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerCountCloned = (HashMap<PassengerStoreTypes, Map<String, Integer>>) pnrWisePassengerCount
				.clone();

		HashMap<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerReductionCount = (HashMap<PassengerStoreTypes, Map<String, Integer>>) context
				.getPnrWisePassengerReductionCount();
		HashMap<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerReductionCountCloned = (HashMap<PassengerStoreTypes, Map<String, Integer>>) pnrWisePassengerReductionCount
				.clone();

		dummyUtilizedPassengerContext.setCurrentMessageLine(dummyCurrentLine);
		dummyUtilizedPassengerContext.setMessageString(new StringBuilder());
		dummyUtilizedPassengerContext.setLastGroupCode(context.getLastGroupCode());
		dummyUtilizedPassengerContext.setMessageType(context.getMessageType());
		dummyUtilizedPassengerInformations.clear();
		dummyUtilizedPassengerContext.setUtilizedPassengers(dummyUtilizedPassengerInformations);
		dummyUtilizedPassengerContext.setPnrWisePassengerCount(pnrWisePassengerCountCloned);
		dummyUtilizedPassengerContext.setPnrWisePassengerReductionCount(pnrWisePassengerReductionCountCloned);
		dummyUtilizedPassengerContext.setGroupCodesList(context.getGroupCodesList());
		dummyUtilizedPassengerContext.setPnrWiseGroupCodes(context.getPnrWiseGroupCodes());
		dummyUtilizedPassengerContext.setFeaturePack(context.getFeaturePack());
		dummyUtilizedPassengerContext.setOngoingStoreType(pEContext.getOngoingStoreType());
		dummyUtilizedPassengerContext.setLineCount(0);
	}

	private boolean isdummyGroupCodeAvailale(String pnr) {
		boolean isValied = false;
		if (dummyUtilizedPassengerContext != null && dummyUtilizedPassengerContext.getPnrWiseGroupCodes() != null
				&& dummyUtilizedPassengerContext.getPnrWiseGroupCodes().containsKey(pnr)) {
			isValied = true;
		}
		return isValied;
	}

	private String getGroupCodeBy(String pnr) {
		String groupCode = "";
		if (dummyUtilizedPassengerContext != null && dummyUtilizedPassengerContext.getPnrWiseGroupCodes() != null
				&& dummyUtilizedPassengerContext.getPnrWiseGroupCodes().containsKey(pnr)) {
			groupCode = dummyUtilizedPassengerContext.getPnrWiseGroupCodes().get(pnr);
		}
		return groupCode;
	}

	private Map<String, List<PassengerInformation>> getPassengersMap() {
		Map<String, List<PassengerInformation>> passengerInformations = null;
		passengerInformations = destinationFare.getPassengersMap(pEContext.getOngoingStoreType());
		return passengerInformations;
	}

	private void nameElementTemplate() {
		Map<String, List<PassengerInformation>> passengerMap = getPassengersMap();
		clearPnrPack();
		StringBuilder elementTemplate = new StringBuilder();
		createDummyUtilizedPassengerContext(pEContext);
		iterateLastNameWisePassengers(passengerMap, elementTemplate);
		setLastGroupCodeForUP();
		if (!isNoPassengersAmmended) {
			ammendMessageData();
		}
		markUtilizedPassengers();
		removeUtilizedPassengers();
		if (utilizedPassengersAvailability()) {
			setLastGroupCode();
			executeNext();
		}
		cleatTemplateString(elementTemplate);
		clearUtilizedPassengerInformations();
		if (!passengerMap.isEmpty() && !pEContext.isPartCountExceeded()) {
			nameElementTemplate();
		}
	}

	private boolean utilizedPassengersAvailability() {
		boolean available = false;
		if (utilizedPassengerContext != null && utilizedPassengerContext.getUtilizedPassengers() != null
				&& !utilizedPassengerContext.getUtilizedPassengers().isEmpty()) {
			available = true;
		}
		return available;
	}

	private void
			iterateLastNameWisePassengers(Map<String, List<PassengerInformation>> passengerMap, StringBuilder elementTemplate) {
		
		if (passengerMap != null && passengerMap.size() > 0) {
			for (Map.Entry<String, List<PassengerInformation>> entry : passengerMap.entrySet()) {
				executeConcatenationElementBuilder(utilizedPassengerContext);
				if (entry.getValue() != null && !entry.getValue().isEmpty()) {
					firstNameElementIterator(entry.getValue(), elementTemplate, entry.getKey());
					break;
				}

			}
		}
	}

	private void resetValidationParams() {
		isNoPassengersAmmended = true;
	}

	protected void ammendToBaseLine(String currentElement, StringBuilder currentLine, StringBuilder messageLine) {
		if (currentElement != null && !currentElement.isEmpty()) {
			messageLine.append(currentElement);
		}
	}

	private void incrementPnrWiseUtilizedPaxCount(String pnr) {
		Integer passengerCount = 0;
		if (pnrWiseUtilizedPassengerCount.containsKey(pnr)) {
			passengerCount = pnrWiseUtilizedPassengerCount.get(pnr);
		}
		pnrWiseUtilizedPassengerCount.put(pnr, ++passengerCount);

	}

	private void clearPnrPack() {
		previousPnr = null;
		ongoingPnr = null;
	}

	private void setUtilizedPnrCode(String pnr, UtilizedPassengerContext utilizedPassengerContext) {
		previousPnr = pnr;
		utilizedPassengerContext.setPnr(pnr);
	}

	private void setOngoingPnrCode(String pnr) {
		ongoingPnr = pnr;
	}

	private void firstNameElementIterator(List<PassengerInformation> passengers, StringBuilder elementTemplate, String lastName) {
		resetValidationParams();
		ammendLastName(elementTemplate, lastName);
		int i = 1;
		for (PassengerInformation passengerInformation : passengers) {
			addToDummyutilizedPax(passengerInformation);
			if (!validateEndElement("XXXXXX", 1)) {
				break;
			}

			if (!passengerInformation.isUtilized() && !firstNameElementAmmender(i, passengerInformation, elementTemplate)) {
				break;
			}

			NameElementBuilderTemplate nameElementBuilderTemplate = getNameElementBuilderTemplate(pEContext.getMessageType());
			if (!nameElementBuilderTemplate.isMorePaxToProceed()) {
				break;
			}
			i++;
		}
		ammendPassengerCountBeforeThePax(elementTemplate);
		currentElement = elementTemplate.toString();
	}

	private void addToDummyutilizedPax(PassengerInformation passengerInformation) {
		dummyUtilizedPassengerInformations.add(passengerInformation);
		dummyUtilizedPassengerContext.setPnr(passengerInformation.getPnr());
		dummyUtilizedPassengerContext.setLineCount(0);
	}

	private void incrementPassengerCountPerPart(Integer passengerCount) {
		pEContext.setPassengersPerPart(passengerCount);
	}

	private void ammendLastName(StringBuilder elementTemplate, String lastName) {
		elementTemplate.insert(0, lastName);
		currentLine.append(lastName);
	}

	private void ammendPassengerCountBeforeThePax(StringBuilder elementTemplate) {
		if (utilizedPassengerInformations != null) {
			elementTemplate.insert(0, utilizedPassengerInformations.size());
		}
	}

	private void cleatTemplateString(StringBuilder elementTemplate) {
		elementTemplate.setLength(0);
	}

	private void clearUtilizedPassengerInformations() {
		utilizedPassengerInformations.clear();
	}

	private void clearCurrentElement() {
		currentElement = "";
	}

	private Integer reducdePnrWiseRemainingPassengerCount(String pnr, String action) {
		Integer pnrWisePassengerCount = 0;
		Map<String, Integer> pnrWisePassengersTotal = utilizedPassengerContext.getPnrWisePassengerReductionCount().get(
				ongoingAction(action));
		if (pnrWisePassengersTotal != null) {
			pnrWisePassengerCount = pnrWisePassengersTotal.get(pnr);
			pnrWisePassengersTotal.put(pnr, --pnrWisePassengerCount);
		}
		return pnrWisePassengerCount;
	}

	private PassengerStoreTypes ongoingAction(String action) {
		PassengerStoreTypes passengerStoreTypes = null;
		switch (AdlActions.valueOf(action)) {
		case A:
			passengerStoreTypes = PassengerStoreTypes.ADDED;
			break;
		case D:
			passengerStoreTypes = PassengerStoreTypes.DELETED;
			break;
		case C:
			passengerStoreTypes = PassengerStoreTypes.CHANGED;
			break;
		default:
			break;
		}
		return passengerStoreTypes;
	}

	private void markUtilizedPassengers() {
		for (PassengerInformation passengerInformation : utilizedPassengerInformations) {
			passengerInformation.setUtilized(true);
		}
	}

	private void removeUtilizedPassengers() {
		for (PassengerInformation passengerInformation : utilizedPassengerInformations) {
			destinationFare.removePassenger(ongoingStoreType, passengerInformation);
		}
	}

	private boolean firstNameElementAmmender(int passengerCount, PassengerInformation passengerInformation,
			StringBuilder elementTemplate) {
		boolean proceedNext = false;
		boolean isFirstPassenger = false;
		setOngoingPnrCode(passengerInformation.getPnr());
		if (passengerCount == 1) {
			isFirstPassenger = true;
			if (!isdummyGroupCodeAvailale(passengerInformation.getPnr())) {
				if (getLengthOfGivenString("0" + currentLine.toString() + "/" + passengerInformation.getFirstName()
						+ passengerInformation.getTitle()) > 64) {
					String firstName = passengerInformation.getFirstName();
					firstName = firstName.substring(0, firstName.length() - 4);
					passengerInformation.setFirstName(firstName);
				}
			} else {
				if ((getLengthOfGivenString("0" + currentLine.toString() + "/" + passengerInformation.getFirstName()
						+ passengerInformation.getTitle()) + getLengthOfGivenString(getGroupCodeBy(passengerInformation.getPnr()))) >= 64) {
					int reducecount = getLengthOfGivenString("0" + currentLine.toString() + "/"
							+ passengerInformation.getFirstName() + passengerInformation.getTitle()) - 64;
					reducecount = reducecount + getLengthOfGivenString("-" + getGroupCodeBy(passengerInformation.getPnr()));
					String firstName = passengerInformation.getFirstName();
					firstName = firstName.substring(0, firstName.length() - reducecount);
					passengerInformation.setFirstName(firstName);
				}
			}

		}
		RuleResponse response = validateSubElement(passengerInformation.getFirstName() + passengerInformation.getTitle(),
				isFirstPassenger);

		if (response.isProceedNextElement()) {
			elementTemplate.append(forwardSlash());
			appendToCurrentLine(forwardSlash());
			elementTemplate.append(passengerInformation.getFirstName().toUpperCase());
			appendToCurrentLine(passengerInformation.getFirstName().toUpperCase());
			elementTemplate.append(passengerInformation.getTitle());
			appendToCurrentLine(passengerInformation.getTitle());
			proceedNext = true;
			addToUtilizedPassengers(passengerInformation);
			setUtilizedPnrCode(passengerInformation.getPnr(), utilizedPassengerContext);
			reducdePnrWiseRemainingPassengerCount(passengerInformation.getPnr(), passengerInformation.getAdlAction());
			incrementPnrWiseUtilizedPaxCount(passengerInformation.getPnr());
			isNoPassengersAmmended = false;
		}
		return proceedNext;
	}

	private void appendToCurrentLine(String element) {
		currentLine.append(element);
	}

	private void addToUtilizedPassengers(PassengerInformation passengerInformation) {
		utilizedPassengerInformations.add(passengerInformation);
	}

	private void ammendMessageData() {
		ammendToBaseLine(currentElement, currentLine, messageLine);
	}

	private RuleResponse validateSubElement(String elementText, boolean isFirstPassenger) {
		int keepSpaceValue = 5;
		if (isFirstPassenger) {
			if (getLengthOfGivenString("0" + currentLine.toString() + "/" + elementText) > 59) {
				keepSpaceValue = 0;
			}
		}
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext("0" + currentLine.toString() + "/", elementText, keepSpaceValue);
		ruleResponse = nameElementRuleExecutor.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private int getLengthOfGivenString(String text) {
		int length = 0;
		if (text != null) {
			length = text.length();
		}
		return length;
	}

	private boolean validateEndElement(String elementText, Integer paxCount) {

		RuleResponse ruleResponse = new RuleResponse();
		executeNextDummy();

		setPassengersPerMessagePart(lineCount);
		incrementPassengerCountPerPart(lineCount);
		RulesDataContext rulesDataContext = createEndRuleDataContext(currentLine.toString(), elementText, 0, lineCount);
		ruleResponse = endPartRuleExecutor.validateElementRules(rulesDataContext);

		ruleResponse.setProceedNextElement(isCountExceeded());

		setPartCountExceeded(ruleResponse.isProceedNextElement());
		return ruleResponse.isProceedNextElement();
	}

	private boolean isCountExceeded() {
		boolean isValied = true;
		String messageLine = pEContext.getMessageString().toString();
		if (messageLine != null && !messageLine.isEmpty()) {
			String[] separated = messageLine.split("\n");
			if (separated != null && (separated.length + getDummyLineCount() > 60)) {
				isValied = false;
			}
		}
		return isValied;
	}

	private int getDummyLineCount() {
		int lineCount = 0;
		String messageLine = dummyUtilizedPassengerContext.getMessageString().toString();
		if (messageLine != null && !messageLine.isEmpty()) {
			String[] separated = messageLine.split("\n");
			lineCount = separated.length;
		}
		return lineCount;
	}

	private void setPartCountExceeded(boolean isExceeded) {
		pEContext.setPartCountExceeded(!isExceeded);
	}

	private void setPassengersPerMessagePart(Integer paxPerPart) {
		pEContext.setPassengersPerPart(paxPerPart);
	}
	
	private void setLastGroupCodeForUP(){
		utilizedPassengerContext.setLastGroupCode(dummyUtilizedPassengerContext.getLastGroupCode());
		pEContext.setLastGroupCode(utilizedPassengerContext.getLastGroupCode());
	}

	private RulesDataContext createRuleDataContext(String currentLine, String ammendingLine, int reductionLength) {
		NameElementRuleContext rulesDataContext = new NameElementRuleContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		rulesDataContext.setOngoingPnr(ongoingPnr);
		rulesDataContext.setPreviourPnr(previousPnr);
		return rulesDataContext;
	}

	private RulesDataContext createEndRuleDataContext(String currentLine, String ammendingLine, int reductionLength,
			int passengerCount) {
		EndElementRuleContext rulesDataContext = new EndElementRuleContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		rulesDataContext.setPassengerCount(passengerCount);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(utilizedPassengerContext);
		}
	}

	private void executeNextDummy() {
		if (nextElementBuilder != null) {
			dummyUtilizedPassengerContext.getCurrentMessageLine().setLength(0);
			dummyUtilizedPassengerContext.getMessageString().setLength(0);
			nextElementBuilder.buildElement(dummyUtilizedPassengerContext);
		}
	}

	private void loopBack(ElementContext context) {
		if (loopBackBuilder != null) {
			pEContext.setLastGroupCode(context.getLastGroupCode());
			loopBackBuilder.buildElement(pEContext);
		}
	}

	public BaseRuleExecutor<RulesDataContext> getEndPartRuleExecutor() {
		return endPartRuleExecutor;
	}

	public void setEndPartRuleExecutor(BaseRuleExecutor<RulesDataContext> endPartRuleExecutor) {
		this.endPartRuleExecutor = endPartRuleExecutor;
	}

}
