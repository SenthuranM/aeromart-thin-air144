package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Primary objective of this charges eliminator is to eliminate any charges which are not required to be tracked from
 * the transaction granularity
 * 
 * @author Nilindra Fernando
 * @since April 24, 2012
 */
public class TnxGranularityChargesEliminator {

	private ReservationPaxFare reservationPaxFare;

	private Collection<Long> ondChargesIds;

	public TnxGranularityChargesEliminator(ReservationPaxFare reservationPaxFare, Collection<Long> ondChargesIds) {
		this.reservationPaxFare = reservationPaxFare;
		this.ondChargesIds = ondChargesIds;
	}

	public Collection<ReservationPaxOndCharge> execute() {
		Collection<ReservationPaxOndCharge> applicableCharges = new ArrayList<ReservationPaxOndCharge>();

		if (isReservationPaxFareCancelled(reservationPaxFare)
				&& !isAnyOndChargeTrackedInGranularity(reservationPaxFare, ondChargesIds)) {

			if (reservationPaxFare.getTotalChargeAmount().doubleValue() != 0) {
				applicableCharges.addAll(getApplicableCharges(reservationPaxFare.getCharges()));
			}
		} else {
			applicableCharges.addAll(reservationPaxFare.getCharges());
		}

		return applicableCharges;
	}

	private Collection<ReservationPaxOndCharge> getApplicableCharges(
			Collection<ReservationPaxOndCharge> colReservationPaxOndCharge) {
		Collection<ReservationPaxOndCharge> applicableCharges = new ArrayList<ReservationPaxOndCharge>();

		Map<Integer, Collection<ReservationPaxOndCharge>> fareIdWiseOndCharges = new HashMap<Integer, Collection<ReservationPaxOndCharge>>();
		Map<Integer, Collection<ReservationPaxOndCharge>> chargeRateIdWiseOndCharges = new HashMap<Integer, Collection<ReservationPaxOndCharge>>();

		for (ReservationPaxOndCharge reservationPaxOndCharge : colReservationPaxOndCharge) {
			if (reservationPaxOndCharge.getFareId() != null) {
				Collection<ReservationPaxOndCharge> colFareWiseOndCharge = fareIdWiseOndCharges.get(reservationPaxOndCharge
						.getFareId());

				if (colFareWiseOndCharge == null) {
					colFareWiseOndCharge = new ArrayList<ReservationPaxOndCharge>();
					colFareWiseOndCharge.add(reservationPaxOndCharge);
					fareIdWiseOndCharges.put(reservationPaxOndCharge.getFareId(), colFareWiseOndCharge);
				} else {
					colFareWiseOndCharge.add(reservationPaxOndCharge);
				}
			} else if (reservationPaxOndCharge.getChargeRateId() != null) {
				Collection<ReservationPaxOndCharge> colChargeRateWiseOndCharge = chargeRateIdWiseOndCharges
						.get(reservationPaxOndCharge.getChargeRateId());

				if (colChargeRateWiseOndCharge == null) {
					colChargeRateWiseOndCharge = new ArrayList<ReservationPaxOndCharge>();
					colChargeRateWiseOndCharge.add(reservationPaxOndCharge);
					chargeRateIdWiseOndCharges.put(reservationPaxOndCharge.getChargeRateId(), colChargeRateWiseOndCharge);
				} else {
					colChargeRateWiseOndCharge.add(reservationPaxOndCharge);
				}
			}
		}

		if (fareIdWiseOndCharges.size() > 0) {
			for (Collection<ReservationPaxOndCharge> colFareOndCharge : fareIdWiseOndCharges.values()) {
				BigDecimal amount = getPriceAmount(colFareOndCharge);

				if (amount.doubleValue() != 0) {
					boolean isPlusValue = true;
					if (amount.doubleValue() < 0) {
						isPlusValue = false;
					}

					ReservationPaxOndCharge applicableReservationPaxOndCharge = getGreatestChargeAmount(colFareOndCharge,
							isPlusValue);
					if (applicableReservationPaxOndCharge != null) {
						applicableReservationPaxOndCharge = applicableReservationPaxOndCharge.cloneForShallowCopy();
						applicableReservationPaxOndCharge.setAmount(amount);
						applicableCharges.add(applicableReservationPaxOndCharge);
					}				
				}
			}
		}

		if (chargeRateIdWiseOndCharges.size() > 0) {
			for (Collection<ReservationPaxOndCharge> colChargeRateCharge : chargeRateIdWiseOndCharges.values()) {
				BigDecimal amount = getPriceAmount(colChargeRateCharge);

				if (amount.doubleValue() != 0) {
					boolean isPlusValue = true;
					if (amount.doubleValue() < 0) {
						isPlusValue = false;
					}

					ReservationPaxOndCharge applicableReservationPaxOndCharge = getGreatestChargeAmount(colChargeRateCharge,
							isPlusValue);
					if (applicableReservationPaxOndCharge != null) {
						applicableReservationPaxOndCharge = applicableReservationPaxOndCharge.cloneForShallowCopy();
						applicableReservationPaxOndCharge.setAmount(amount);
						applicableCharges.add(applicableReservationPaxOndCharge);
					}
				}
			}
		}

		return applicableCharges;
	}

	private ReservationPaxOndCharge
			getGreatestChargeAmount(Collection<ReservationPaxOndCharge> colOndCharge, boolean isPlusValue) {

		ReservationPaxOndCharge applicableReservationPaxOndCharge = null;

		for (ReservationPaxOndCharge reservationPaxOndCharge : colOndCharge) {

			if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() != 0) {
				if (isPlusValue && reservationPaxOndCharge.getEffectiveAmount().doubleValue() > 0) {
					if (applicableReservationPaxOndCharge == null) {
						applicableReservationPaxOndCharge = reservationPaxOndCharge;
					} else {
						if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() > applicableReservationPaxOndCharge.getEffectiveAmount()
								.doubleValue()) {
							applicableReservationPaxOndCharge = reservationPaxOndCharge;
						}
					}
				}

				if (!isPlusValue && reservationPaxOndCharge.getEffectiveAmount().doubleValue() < 0) {
					if (applicableReservationPaxOndCharge == null) {
						applicableReservationPaxOndCharge = reservationPaxOndCharge;
					} else {
						if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() > applicableReservationPaxOndCharge.getEffectiveAmount()
								.doubleValue()) {
							applicableReservationPaxOndCharge = reservationPaxOndCharge;
						}
					}
				}
			}
		}

		return applicableReservationPaxOndCharge;
	}

	private BigDecimal getPriceAmount(Collection<ReservationPaxOndCharge> colReservationPaxOndCharge) {
		BigDecimal totalPriceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPaxOndCharge reservationPaxOndCharge : colReservationPaxOndCharge) {
			totalPriceAmount = AccelAeroCalculator.add(totalPriceAmount, reservationPaxOndCharge.getAmount());
		}
		return totalPriceAmount;
	}

	private boolean isAnyOndChargeTrackedInGranularity(ReservationPaxFare reservationPaxFare, Collection<Long> ondChargIds) {

		for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxFare.getCharges()) {
			if (ondChargIds.contains(Long.valueOf(reservationPaxOndCharge.getPnrPaxOndChgId()))) {
				return true;
			}
		}

		return false;
	}

	private boolean isReservationPaxFareCancelled(ReservationPaxFare reservationPaxFare) {

		for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationPaxFareSegment.getSegment()
					.getStatus())) {
				return true;
			}
		}

		return false;
	}

}
