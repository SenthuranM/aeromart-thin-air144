/**
 * 
 */
package com.isa.thinair.airreservation.core.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.core.bl.pnr.AlertReservations;
import com.isa.thinair.commons.api.dto.CommonTemplatingDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.LanguageCodesEnum;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.messaging.api.model.Message;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.TopicConfig;

/**
 * @author indika
 * 
 */
public class AlertReservationsUtil {

	private static Log log = LogFactory.getLog(AlertReservations.class);

	@SuppressWarnings("rawtypes")
	public static String getMessage(FlightReservationAlertDTO flightReservationAlertDTO, String preferredLanguage, HashMap map,
			String templateName) throws ModuleException {
		String message = null;
		if (flightReservationAlertDTO.getSmsMessageDesc() == null) {
			message = getGeneratedNotificationMessage(flightReservationAlertDTO, preferredLanguage, map, templateName);
		}
		return message;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String getGeneratedNotificationMessage(FlightReservationAlertDTO flightReservationAlertDTO,
			String preferredLanguage, HashMap map, String templateName) throws ModuleException {

		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightReservationAlertDTO.getOldFlightAlertInfoDTO()
				.getFlightNumber());
		CommonTemplatingDTO commonTemplatingDTO = AppSysParamsUtil.composeCommonTemplatingDTO(carrierCode);

		if (commonTemplatingDTO != null) {
			map.put("commonTemplatingDTO", commonTemplatingDTO);
		}

		TemplateEngine engine = null;
		TopicConfig topicConfig = (TopicConfig) ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
				.getTopicConfigurationMap().get(templateName);
		String templateFileName = topicConfig.getTemplateFileName();
		String prefLang = Locale.ENGLISH.toString().toUpperCase();

		// TODO Available languages
		if (LanguageCodesEnum.LANGUAGE.ARABIC.getLanguage().equalsIgnoreCase(preferredLanguage)
				|| LanguageCodesEnum.LANGUAGE.FRENCH.getLanguage().equalsIgnoreCase(preferredLanguage)) {
			prefLang = preferredLanguage.toUpperCase();
		}

		if (templateFileName.indexOf(".") > 0) {
			templateFileName = templateFileName.substring(0, templateFileName.indexOf(".")) + "_" + prefLang
					+ templateFileName.substring(templateFileName.indexOf("."));
		} else {
			templateFileName = templateFileName + "_" + prefLang;
		}

		StringWriter writer = null;
		try {
			engine = new TemplateEngine();
			writer = new StringWriter();
		} catch (Exception e) {
			throw new ModuleException("alerting.errorCreatingFromTemplate", e);
		}
		if (map != null) {
			engine.writeTemplate(map, templateFileName, writer);
		}

		// Convert to input stream
		ByteArrayInputStream inputStream = null;
		try {
			byte arr[] = writer.toString().getBytes(Message.CONTENT_BYTE_ARRAY_TYPE);
			inputStream = new ByteArrayInputStream(arr);
		} catch (Exception e) {
			log.error("Preparing message failed", e);
		}

		return parseMessage(inputStream);
	}

	private static String parseMessage(InputStream inputStream) {
		StringBuffer content = new StringBuffer();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilderFactory.setCoalescing(true);
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(inputStream);

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("body");

			int intRecords = listOfRecords.getLength();

			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				content.append(menuNode.getFirstChild().getNodeValue());
			}

		} catch (SAXParseException spe) {
			log.error("Parsing error" + ", line " + spe.getLineNumber() + ", uri " + spe.getSystemId(), spe);
		} catch (SAXException e) {
			log.error(e);
		} catch (Throwable t) {
			log.error(t);
		}

		return content.toString();
	}

	@SuppressWarnings("rawtypes")
	public static String getMessageForAnOfficer(FlightReservationAlertDTO flightReservationAlertDTO, HashMap map,
			String templateName) throws ModuleException {
		String message = null;
		if (flightReservationAlertDTO.getSmsMessageDesc() == null) {
			message = getGeneratedNotificationMessageForAnOfficer(flightReservationAlertDTO, map, templateName);
		}
		return message;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String getGeneratedNotificationMessageForAnOfficer(FlightReservationAlertDTO flightReservationAlertDTO,
			HashMap map, String templateName) throws ModuleException {

		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightReservationAlertDTO.getOldFlightAlertInfoDTO()
				.getFlightNumber());

		TemplateEngine engine = null;
		TopicConfig topicConfig = (TopicConfig) ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
				.getTopicConfigurationMap().get(templateName);
		String templateFileName = topicConfig.getTemplateFileName();
		StringWriter writer = null;

		try {
			engine = new TemplateEngine();
			writer = new StringWriter();
		} catch (Exception e) {
			throw new ModuleException("alerting.errorCreatingFromTemplate", e);
		}
		if (map != null) {
			engine.writeTemplate(map, templateFileName, writer);
		}

		// Convert to input stream
		ByteArrayInputStream inputStream = null;
		try {
			byte arr[] = writer.toString().getBytes(Message.CONTENT_BYTE_ARRAY_TYPE);
			inputStream = new ByteArrayInputStream(arr);
		} catch (Exception e) {
			log.error("Preparing message failed", e);
		}

		return parseMessage(inputStream);
	}

}
