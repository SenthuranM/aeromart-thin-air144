/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.aig.INSChargeTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;

/**
 * ReservationChargeDAO is the business DAO interface for the reservation service all charges apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationChargeDAO {

	/**
	 * Removes Fare Segment charges
	 * 
	 * @param colPnrPaxFareSegIds
	 */
	public void removeFareSegChargesAndPayments(Collection<Integer> colPnrPaxFareSegIds);

	/**
	 * Gets the Insurance charges for Pnr Pax wise
	 * 
	 * @param colpnrPaxId
	 * @param chargeratId
	 * @return
	 */
	public Collection<INSChargeTO> getInsuranceCharges(Collection<Integer> colpnrPaxId, int chargeratId);

	/**
	 * Gets the PaxOndCharges
	 * 
	 * @param colPnrPaxOndChgIds
	 * @return
	 */
	public Map<Long, ReservationPaxOndCharge> getReservationPaxOndCharges(Collection<Long> colPnrPaxOndChgId);

	public Integer getAppliedFareDiscountPercentage(String pnr);

	public Map<Integer, String> getPaxONDChargeRefIdMap(Integer pnrPaxId, boolean isInfantCharges);

	public Map<Integer, Collection<Integer>> getPaxONDChargeRefIdColl(Integer pnrPaxId, boolean withInfant);

	public boolean isRefundableCharge(Long pnrPaxOndChgId);

	public Map<String, Double> getCurrentTaxCharged(String taxChargeCode, String pnr);

	public Collection<ReservationPaxOndCharge> getPnrPaxOndCharges(String pnr);
}