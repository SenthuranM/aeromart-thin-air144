/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.PnrElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.PnrElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class PnrElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private String lastGroupCode;
	private String[] groupCodes;
	private String pnr;
	private String externalPnr = null;
	private String availableGroupCode;
	private boolean isDeletedPassengers = false;

	private UtilizedPassengerContext uPContext;

	private BaseRuleExecutor<RulesDataContext> pnrElementRuleExecutor = new PnrElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		currentElement = "";
		initContextData(context);
		groupCodeElementTemplate();
		response = validateSubElement(currentElement);
		ammendMessageDataAccordingTo(response);
	}

	private void groupCodeElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		if (pnr != null) {
			elementTemplate.append(pnrElementPrefix());
			if (externalPnr != null) {
				elementTemplate.append(externalPnr);
			} else {
				elementTemplate.append(pnr);
			}
			currentElement = elementTemplate.toString();
		}
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		pnr = uPContext.getPnr();
		if (uPContext.getOngoingStoreType().equals(PassengerStoreTypes.DELETED)) {
			isDeletedPassengers = true;
		} else {
			isDeletedPassengers = false;
		}
		setExternalPnr(uPContext.getUtilizedPassengers());
	}

	private void setExternalPnr(List<PassengerInformation> utilizedPassengers) {
		if (utilizedPassengers != null && !utilizedPassengers.isEmpty()) {
			for (PassengerInformation passengerInformation : utilizedPassengers) {
				if (passengerInformation != null
						&& passengerInformation.getExternalPnr() != null
						&& !passengerInformation.getExternalPnr().isEmpty()) {
					externalPnr = passengerInformation.getExternalPnr();
					break;
				}
			}
		}
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (response.isProceedNextElement()) {
			currentElement = " " + currentElement;
			ammendToBaseLine(currentElement, currentLine, messageLine);
			setPnrToAvailableList();
		} else {
			if (response.isPnrValied()) {
				executeConcatenationElementBuilder(uPContext);
				ammendToBaseLine(currentElement, currentLine, messageLine);
				setPnrToAvailableList();
			}
		}
		executeNext();
	}

	private void setPnrToAvailableList() {
		if (uPContext.getGroupCodeAvailabilityMap() != null) {
			uPContext.getGroupCodeAvailabilityMap().put(pnr, true);
		}
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext("0"
				+ currentLine.toString() + " ", elementText, 0);
		ruleResponse = pnrElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		PnrElementRuleContext rulesDataContext = new PnrElementRuleContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		rulesDataContext.setPnrWiseGroupCodes(uPContext.getPnrWiseGroupCodes());
		rulesDataContext.setPnrWisePassengerCount(uPContext
				.getPnrWisePassengerCount()
				.get(uPContext.getOngoingStoreType()));
		rulesDataContext.setPnrWisePaxReductionCout(uPContext
				.getPnrWisePassengerReductionCount().get(
						uPContext.getOngoingStoreType()));
		rulesDataContext.setUtilizedPassengerCount(uPContext
				.getUtilizedPassengers().size());
		rulesDataContext.setPnr(uPContext.getPnr());
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null && !isDeletedPassengers) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
