/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.adl.comparator.CompPassengerPnr;
import com.isa.thinair.airreservation.api.dto.adl.comparator.CompPassengerTitle;
import com.isa.thinair.airreservation.api.dto.adl.comparator.MultiComparator;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;

/**
 * @author udithad
 *
 */
public abstract class PnlADlBaseDataStructure {

	private Map<String, Map<String, DestinationFare>> destinationFareBucket = new HashMap<String, Map<String, DestinationFare>>();
	private Collection<DestinationFare> destinationFares = new ArrayList<DestinationFare>();
	protected HashMap<String, Integer> fareClassPaxCountMap = null;
	protected HashMap<String, List<String>> cabinFareRbdMap = null;
	protected ReservationAuxilliaryDAO auxilliaryDAO = null;
	protected String lastGroupCode = null;
	private DestinationFareStructure destinationFareStructure;
	protected Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerCount;
	protected Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerReductionCount;
	protected List<String> destinationAirportCodes;

	protected Map<Integer, Integer> pnrPaxIdvsSegmentIds;
	protected Map<String, Integer> fareClassWisePaxCount;
	protected List<String> pnrCollection;
	protected List<PassengerInformation> passengerInformations;
	protected Map<String, LinkedHashMap<String,String>> seatConfigurationElements;

	public PnlADlBaseDataStructure() {
		destinationFareStructure = new DestinationFareStructure();
		this.auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		pnrWisePassengerCount = new HashMap<PassengerStoreTypes, Map<String, Integer>>();
		pnrWisePassengerReductionCount = new HashMap<PassengerStoreTypes, Map<String, Integer>>();

		pnrPaxIdvsSegmentIds = new HashMap<Integer, Integer>();
		fareClassWisePaxCount = new HashMap<String, Integer>();
		pnrCollection = new ArrayList<String>();
		passengerInformations = new ArrayList<PassengerInformation>();
		destinationAirportCodes = new ArrayList<String>();
	}

	protected void populateFlightIdentityInformation(BaseDataContext context) {
		if (context.getFlightId() != null) {
			context.setFlightNumber(getFlightNumberByFlightId(context
					.getFlightId()));
		} else {
			context.setFlightId(auxilliaryDAO.getFlightID(
					context.getFlightNumber(),
					context.getDepartureAirportCode(),
					context.getFlightLocalDate()));
		}
	}

	protected void populateFareCabinRbdMap(Integer flightId) {
		cabinFareRbdMap = refineRBDCabinMap(ReservationModuleUtils
				.getFlightInventoryBD()
				.getAllocatedBookingClassesForCabinClasses(flightId));
	}

	private HashMap<String, List<String>> refineRBDCabinMap(
			HashMap<String, List<String>> cabinFares) {
		HashMap<String, List<String>> rbdCbinMap = new HashMap<String, List<String>>();
		if (cabinFares != null) {
			for (Map.Entry<String, List<String>> entry : cabinFares.entrySet()) {
				List<String> fareMap = new ArrayList<String>();
				for (String fareClassCode : entry.getValue()) {
					String fareClassFirstCharacter = String
							.valueOf(fareClassCode.charAt(0));
					if (!fareMap.contains(fareClassFirstCharacter)) {
						fareMap.add(fareClassFirstCharacter);
					}
				}
				Collections.sort(fareMap);
				rbdCbinMap.put(entry.getKey(), fareMap);
			}
		}
		return rbdCbinMap;
	}

	protected List<DestinationFare> createDummyEntriesForNonUsedFareClasses(
			String destinationAirportCode) {
		List<DestinationFare> destinationFares = new ArrayList<DestinationFare>();
		if (fareClassWisePaxCount != null /*&& !fareClassWisePaxCount.isEmpty()*/) {
			for (Map.Entry<String, List<String>> entry : cabinFareRbdMap
					.entrySet()) {
				for (String fareClassCode : entry.getValue()) {
					Character fareClassFirstCharacter = fareClassCode.charAt(0);
					if (fareClassWisePaxCount.get(fareClassFirstCharacter
							.toString()) == null) {
						if (isDestinationFareExist(
								fareClassFirstCharacter.toString(),
								destinationFares)) {
							destinationFares.add(createDummyDestinationFare(
									fareClassFirstCharacter.toString(),
									destinationAirportCode));
						}
					}
				}
			}
		}
		return destinationFares;
	}

	private boolean isDestinationFareExist(String destinationFareClass,
			List<DestinationFare> destinationFares) {
		boolean isValied = true;
		if (destinationFares != null) {
			for (DestinationFare destinationFare : destinationFares) {
				if (destinationFare.getFareClass() != null
						&& destinationFare.getFareClass().equals(
								destinationFareClass)) {
					isValied = false;
				}
			}
		}
		return isValied;
	}

	private DestinationFare createDummyDestinationFare(String fareClassCode,
			String destinationAirportCode) {
		DestinationFare destinationFare = new DestinationFare();
		destinationFare.setFareClass(fareClassCode);
		destinationFare.setNumberOfPreviousPassengers(0);
		destinationFare.setDestinationAirportCode(destinationAirportCode);
		return destinationFare;
	}

	private String getFlightNumberByFlightId(Integer flightId) {
		return auxilliaryDAO.getFlightNumber(flightId);
	}

	protected Collection<DestinationFare> getDestinationFareCollectionBy(
			List<PassengerInformation> passengerInformations) {
		return createDestinationFareCollection(passengerInformations);
	}

	private Collection<DestinationFare> createDestinationFareCollection(
			List<PassengerInformation> passengerInformations) {
		DestinationFare destinationFare = null;
		String destinationAirportCode;
		String fareClassCode;
		if (passengerInformations != null) {
			for (PassengerInformation passengerInformation : passengerInformations) {
				if (passengerInformation != null) {
					rebuildWithPnr(passengerInformation, passengerInformations);
					destinationAirportCode = getDestinationAirportCodeBy(passengerInformation);
					fareClassCode = getFareClassCodeBy(passengerInformation);
					destinationFare = getDestinationFare(
							destinationAirportCode, fareClassCode);
					destinationFareStructure
							.populateDestinationFareWithPassengerInformation(
									destinationFare, passengerInformation);
					populatePnrWisePassengerCount(passengerInformation);
					populatePnrWisePassengerReductionCount(passengerInformation);
					populatePnrPaxIdVsPnrSegmentIds(passengerInformation);
					populatePnrCollection(passengerInformation);
					populateFareClassByPassegerCount(passengerInformation);
					createPassengerInformationsList(passengerInformation);
					populateDestinationsList(destinationAirportCode);
				}

			}
		}
		sortPassengers(destinationFares);
		return destinationFares;
	}

	private void rebuildWithPnr(PassengerInformation pax,
			List<PassengerInformation> passengerInformations) {
		if (pax.getGroupId() == null) {
			for (PassengerInformation passengerInformation : passengerInformations) {
				if (passengerInformation.getAdlAction() != null
						&& passengerInformation.getAdlAction().equals(
								AdlActions.D.toString())
						&& passengerInformation.getGroupId() != null
						&& passengerInformation.getPnr().equals(pax.getPnr())) {
						pax.setGroupId(passengerInformation.getGroupId());
				}
			}
		}

	}

	private void populateDestinationsList(String destinationAirportCode) {
		if (destinationAirportCode != null && !destinationAirportCode.isEmpty()
				&& !destinationAirportCodes.contains(destinationAirportCode)) {
			destinationAirportCodes.add(destinationAirportCode);
		}
	}

	private void createPassengerInformationsList(
			PassengerInformation passengerInformation) {
		passengerInformations.add(passengerInformation);
	}

	private void populateFareClassByPassegerCount(
			PassengerInformation passengerInformation) {
		if (passengerInformation != null
				&& passengerInformation.getRbdClassOfService() != null
				&& !passengerInformation.getRbdClassOfService().isEmpty()) {
			String fareClass = passengerInformation.getRbdClassOfService();
			Integer previousCount = 0;
			Integer finalCount = 0;

			if (fareClassWisePaxCount.containsKey(fareClass)) {
				previousCount = fareClassWisePaxCount.get(fareClass);
			} else {
				if (fareClassPaxCountMap != null) {
					previousCount = fareClassPaxCountMap.get(fareClass);
				}
				if (previousCount == null) {
					previousCount = 0;
				}
			}
			if (previousCount < 0) {
				previousCount = 0;
			}
			finalCount = getCount(passengerInformation.getAdlAction(),
					previousCount);
			fareClassWisePaxCount.put(fareClass, finalCount);
		}

	}

	private Integer getCount(String action, Integer count) {
		switch (AdlActions.valueOf(action)) {
		case A:
			count = count + 1;
			break;
		case D:
			if (count > 0) {
				count = count - 1;
			}
			break;
		default:
			break;
		}
		return count;
	}

	private PassengerStoreTypes ongoingAction(String action) {
		PassengerStoreTypes passengerStoreTypes = null;
		switch (AdlActions.valueOf(action)) {
		case A:
			passengerStoreTypes = PassengerStoreTypes.ADDED;
			break;
		case D:
			passengerStoreTypes = PassengerStoreTypes.DELETED;
			break;
		case C:
			passengerStoreTypes = PassengerStoreTypes.CHANGED;
			break;
		default:
			break;
		}
		return passengerStoreTypes;
	}

	private void populatePnrPaxIdVsPnrSegmentIds(
			PassengerInformation passengerInformation) {
		if (passengerInformation != null) {
			if (pnrPaxIdvsSegmentIds.containsKey(passengerInformation
					.getPnrPaxId())) {
				if (pnrPaxIdvsSegmentIds
						.get(passengerInformation.getPnrPaxId()) < passengerInformation
						.getPnrSegId()) {
					pnrPaxIdvsSegmentIds.put(
							passengerInformation.getPnrPaxId(),
							passengerInformation.getPnrSegId());
				}
			} else {
				pnrPaxIdvsSegmentIds.put(passengerInformation.getPnrPaxId(),
						passengerInformation.getPnrSegId());
			}
		}
	}

	private void populatePnrCollection(PassengerInformation passengerInformation) {
		if (passengerInformation != null
				&& passengerInformation.getPnr() != null
				&& !passengerInformation.getPnr().isEmpty()) {
			if (!pnrCollection.contains(passengerInformation.getPnr())) {
				pnrCollection.add(passengerInformation.getPnr());
			}
		}
	}

	private void populatePnrWisePassengerCount(
			PassengerInformation passengerInformation) {
		Integer passengerCount = 0;
		Map<String, Integer> pnrWisePassengersTotal = pnrWisePassengerCount
				.get(ongoingAction(passengerInformation.getAdlAction()));
		if (pnrWisePassengersTotal == null) {
			pnrWisePassengersTotal = new HashMap<String, Integer>();
			pnrWisePassengerCount.put(
					ongoingAction(passengerInformation.getAdlAction()),
					pnrWisePassengersTotal);
		}

		if (pnrWisePassengersTotal.containsKey(passengerInformation.getPnr())) {
			passengerCount = pnrWisePassengersTotal.get(passengerInformation
					.getPnr());
			pnrWisePassengersTotal.put(passengerInformation.getPnr(),
					++passengerCount);
		} else {
			pnrWisePassengersTotal.put(passengerInformation.getPnr(),
					++passengerCount);
		}
	}

	private void populatePnrWisePassengerReductionCount(
			PassengerInformation passengerInformation) {
		Integer passengerCount = 0;
		Map<String, Integer> pnrWisePassengersTotal = pnrWisePassengerReductionCount
				.get(ongoingAction(passengerInformation.getAdlAction()));
		if (pnrWisePassengersTotal == null) {
			pnrWisePassengersTotal = new HashMap<String, Integer>();
			pnrWisePassengerReductionCount.put(
					ongoingAction(passengerInformation.getAdlAction()),
					pnrWisePassengersTotal);
		}

		if (pnrWisePassengersTotal.containsKey(passengerInformation.getPnr())) {
			passengerCount = pnrWisePassengersTotal.get(passengerInformation
					.getPnr());
			pnrWisePassengersTotal.put(passengerInformation.getPnr(),
					++passengerCount);
		} else {
			pnrWisePassengersTotal.put(passengerInformation.getPnr(),
					++passengerCount);
		}
	}

	private void sortPassengers(Collection<DestinationFare> destinationFares) {
		for (DestinationFare destinationFare : destinationFares) {
			sort(destinationFare.getAddPassengers());
			sort(destinationFare.getDeletePassengers());
			sort(destinationFare.getChangePassengers());
		}

	}

	private void sort(Map<String, List<PassengerInformation>> passengerMap) {
		for (Map.Entry<String, List<PassengerInformation>> entry : passengerMap
				.entrySet()) {
			MultiComparator.sort(entry.getValue(), new CompPassengerPnr(),
					new CompPassengerTitle());
		}

	}

	private DestinationFare getDestinationFare(String destinationAirportCode,
			String fareClassCode) {
		DestinationFare destinationFare = null;
		Map<String, DestinationFare> destinationFareMap = getDestinationFareMap(destinationAirportCode);
		if (destinationFareMap.containsKey(fareClassCode)) {
			destinationFare = destinationFareMap.get(fareClassCode);
		} else {
			destinationFare = destinationFareStructure
					.createDestinationFare(destinationAirportCode,
							fareClassCode, fareClassPaxCountMap);
			destinationFareMap.put(fareClassCode, destinationFare);
			destinationFares.add(destinationFare);
		}
		return destinationFare;
	}

	private Map<String, DestinationFare> getDestinationFareMap(
			String destinationAirportCode) {
		Map<String, DestinationFare> destinationFareMap = null;
		if (destinationFareBucket.containsKey(destinationAirportCode)) {
			destinationFareMap = destinationFareBucket
					.get(destinationAirportCode);
		} else {
			destinationFareMap = new HashMap<String, DestinationFare>();
			destinationFareBucket.put(destinationAirportCode,
					destinationFareMap);
		}
		return destinationFareMap;
	}

	private String getDestinationAirportCodeBy(
			PassengerInformation passengerInformation) {
		String destinationAirportCode = passengerInformation
				.getDestinationAirportCode();
		return destinationAirportCode;
	}

	private String getFareClassCodeBy(PassengerInformation passengerInformation) {
		String fareClass = passengerInformation.getRbdClassOfService();
		return fareClass;
	}

	protected void setSeatConfigurationElement(BaseDataContext context){
		
		Integer flightId =0;
		
		if (context.getFlightId() != null) {
			flightId = context.getFlightId();
		} else {
			flightId = auxilliaryDAO.getFlightID(context.getFlightNumber(), context.getDepartureAirportCode(),
					context.getFlightLocalDate());
		}
		
		seatConfigurationElements = auxilliaryDAO.getFlightSeatConfigDetails(flightId);
		context.setSeatConfigurationElements(seatConfigurationElements);
	}
}
