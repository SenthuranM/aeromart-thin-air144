/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airproxy.api.dto.ReservationPaxSegmentPaymentTO;
import com.isa.thinair.airproxy.api.dto.SegmentChargePaymentTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreditCardInformationDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PaymentDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaymentModeDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPaymentDTO;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.persistence.extractor.ReservationPaxPaymentExtractor;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * ReservationPaymentDAOImpl is the business DAO hibernate implmentation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservationPaymentDAO"
 */
public class ReservationPaymentDAOImpl extends PlatformHibernateDaoSupport implements ReservationPaymentDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationPaymentDAOImpl.class);

	/**
	 * Returns tempory payment information
	 * 
	 * @param tnxId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public TempPaymentTnx getTempPaymentInfo(int tnxId) throws CommonsDataAccessException {
		log.debug("Inside getTempPaymentInfo");

		// Returns tempory payment information
		TempPaymentTnx tempPaymentTnx = (TempPaymentTnx) get(TempPaymentTnx.class, new Integer(tnxId));

		if (tempPaymentTnx == null) {
			throw new CommonsDataAccessException("airreservations.arg.temporyPaymentDoesNotExist");
		}

		log.debug("Exit getTempPaymentInfo");
		return tempPaymentTnx;
	}

	/**
	 * Return Temporary Payment Information
	 * 
	 * @param pnr
	 * @return
	 * @throws CommonsDataAccessException
	 */

	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr) throws CommonsDataAccessException {
		Collection<TempPaymentTnx> tempPaymentTnxs = find("from TempPaymentTnx tpt where tpt.pnr = ?", pnr, TempPaymentTnx.class);

		if (tempPaymentTnxs == null) {
			throw new CommonsDataAccessException("airreservations.arg.temporyPaymentDoesNotExist");
		}
		return tempPaymentTnxs;
	}

	/**
	 * Return Temporary Payment Information
	 * 
	 * @param pnr
	 * @param status
	 * @param productType
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr, String status, char productType)
			throws CommonsDataAccessException {
		Collection<TempPaymentTnx> tempPaymentTnxs;

		tempPaymentTnxs = find("from TempPaymentTnx tpt where tpt.pnr = ? AND tpt.status = ? AND tpt.productType = ? ",
				new Object[] { pnr, status, productType }, TempPaymentTnx.class);

		if (tempPaymentTnxs == null) {
			throw new CommonsDataAccessException("airreservations.arg.temporyPaymentDoesNotExist");
		}
		return tempPaymentTnxs;
	}

	/**
	 * Return Temporary Payment Information
	 * 
	 * @param pnr
	 * @param productType
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr, char productType)
			throws CommonsDataAccessException {
		Collection<TempPaymentTnx> tempPaymentTnxs;

		tempPaymentTnxs = find("from TempPaymentTnx tpt where tpt.pnr = ? AND tpt.productType = ? ",
				new Object[] { pnr, productType }, TempPaymentTnx.class);

		if (tempPaymentTnxs == null) {
			throw new CommonsDataAccessException("airreservations.arg.temporyPaymentDoesNotExist");
		}
		return tempPaymentTnxs;
	}

	/**
	 * Return Tempory Payment Information
	 * 
	 * @param colTnxId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<TempPaymentTnx> getTempPaymentInfo(Collection<Integer> colTnxId) {
		Collection<TempPaymentTnx> colTempPaymentTnx = new ArrayList<TempPaymentTnx>();

		if (colTnxId != null && colTnxId.size() > 0) {
			StringBuilder sqlCondition = new StringBuilder();
			for (Integer tptId : colTnxId) {
				if (sqlCondition.length() == 0) {
					sqlCondition.append(" where tnxId = " + tptId);
				} else {
					sqlCondition.append(" OR tnxId = " + tptId);
				}
			}

			colTempPaymentTnx = find("from TempPaymentTnx " + sqlCondition, TempPaymentTnx.class);
		}

		return colTempPaymentTnx;
	}

	/**
	 * Save all tempory payment objects
	 */
	public void saveTempPaymentInfoAll(Collection<TempPaymentTnx> colTempPaymentTnx) {
		log.debug("Inside saveTempPaymentInfoAll");

		// Save the credit card detail
		hibernateSaveOrUpdateAll(colTempPaymentTnx);

		log.debug("Exit saveTempPaymentInfoAll");
	}

	/**
	 * Saves tempory payment information
	 */
	public void saveTempPaymentInfo(TempPaymentTnx tempPaymentTnx) {
		log.debug("Inside saveTempPaymentInfo");

		// Saving the tempory payment information
		hibernateSaveOrUpdate(tempPaymentTnx);

		log.debug("Exit saveTempPaymentInfo");
	}

	/**
	 * Save the credit card detail information
	 */
	public void saveCreditCardDetail(CreditCardDetail creditCardDetail) {
		log.debug("Inside saveCreditCardDetail");

		// Save the credit card detail
		hibernateSaveOrUpdate(creditCardDetail);

		log.debug("Exit saveCreditCardDetail");
	}

	/**
	 * Return credit card detail information
	 * 
	 * @throws CommonsDataAccessException
	 */
	public CreditCardDetail getCreditCardDetail(int ccdId) throws CommonsDataAccessException {
		log.debug("Inside getCreditCardDetail");

		// Returns credit card detail information
		CreditCardDetail creditCardDetail = (CreditCardDetail) get(CreditCardDetail.class, new Integer(ccdId));

		if (creditCardDetail == null) {
			throw new CommonsDataAccessException("airreservations.arg.pnrCreditCardInforDoesNotExit");
		}

		log.debug("Exit getCreditCardDetail");
		return creditCardDetail;
	}

	/**
	 * Returns a Collection of CreditCardDetail Objects for a payment Broker Reference ID
	 * 
	 * @param paymentBrokerRefId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<CreditCardDetail> getCreditCardDetails(int paymentBrokerRefId) throws CommonsDataAccessException {
		log.debug("Inside getCreditCardDetails");

		// Returns credit card detail information
		return find("from CreditCardDetail card where card.paymentBrokerRefNo=?", new Integer(paymentBrokerRefId),
				CreditCardDetail.class);

	}

	/**
	 * Return Credit card information
	 * 
	 * @param colTnxIds
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, CardPaymentInfo> getCreditCardInfo(Collection<Integer> colTnxIds, boolean isOwnTnx) {
		log.debug("Inside getCreditCardInfo");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String tnxField = isOwnTnx ? "TXN_ID" : "EXT_TXN_ID";

		Iterator<Integer> itColTnxIds = colTnxIds.iterator();
		String sql = "";
		int i = 0;
		while (itColTnxIds.hasNext()) {
			itColTnxIds.next();

			if (i == 0) {
				sql += " cc.# = ? ";
			} else {
				sql += " OR cc.# = ? ";
			}
			i++;
		}

		sql = " SELECT cc.RES_PCD_ID R_PCD_ID, cc.NO C_LAST_NUMBERS, cc.FIRST_DIGITS_OF_NUMBER C_FIRST_NUMBERS, cc.NAME C_NAME, cc.E_DATE C_EDATE, cc.SEC_CODE C_SCODE, "
				+ " cc.ADDRESS C_ADDRESS, cc.CARD_TYPE_ID C_CARDTYPE, cc.PNR_PAX_ID PAX_ID, cc.PNR PNR, cc.TRANSACTION_REF_NO T_REF_NO, "
				+ " cc.# TXN_ID, tmpPayTnx.PAYMENT_CURRENCY_CODE, tmpPayTnx.PAYMENT_CURRENCY_AMOUNT, tmpPayTnx.AMOUNT, ccardpaystatus.aid_cccompnay AID_COMPANY "
				+ " FROM T_RES_PCD cc, T_TEMP_PAYMENT_TNX tmpPayTnx, T_CCARD_PAYMENT_STATUS ccardPayStatus "
				+ " WHERE cc.TRANSACTION_REF_NO = ccardPayStatus.TRANSACTION_REF_NO AND  "
				+ " ccardPayStatus.TPT_ID = tmpPayTnx.TPT_ID " + (sql.length() > 0 ? " AND (" + sql + ")" : "")
				+ " ORDER BY cc.#";

		sql = StringUtils.replace(sql, "#", tnxField);

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL parameters           : " + colTnxIds);
		log.debug("############################################");

		Map<Integer, CardPaymentInfo> paymentMap = (Map<Integer, CardPaymentInfo>) jt.query(sql, colTnxIds.toArray(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, CardPaymentInfo> paymentMap = new HashMap<Integer, CardPaymentInfo>();
						CardPaymentInfo cardPaymentInfo;
						PayCurrencyDTO payCurrencyDTO;
						int tnxId;

						if (rs != null) {
							while (rs.next()) {
								cardPaymentInfo = new CardPaymentInfo();
								tnxId = rs.getInt("TXN_ID");

								cardPaymentInfo.setCcdId(BeanUtils.parseInteger(rs.getInt("R_PCD_ID")));

								cardPaymentInfo.setNoFirstDigits(BeanUtils.nullHandler(rs.getString("C_FIRST_NUMBERS")));
								cardPaymentInfo.setNoLastDigits(BeanUtils.nullHandler(rs.getString("C_LAST_NUMBERS")));
								cardPaymentInfo.setName(BeanUtils.nullHandler(rs.getString("C_NAME")));
								cardPaymentInfo.setEDate(BeanUtils.nullHandler(rs.getString("C_EDATE")));
								cardPaymentInfo.setSecurityCode(BeanUtils.nullHandler(rs.getString("C_SCODE")));
								cardPaymentInfo.setAddress(BeanUtils.nullHandler(rs.getString("C_ADDRESS")));
								cardPaymentInfo.setType(rs.getInt("C_CARDTYPE"));
								cardPaymentInfo.setPnrPaxId(BeanUtils.parseInteger(rs.getInt("PAX_ID")));
								cardPaymentInfo.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));
								cardPaymentInfo.setTransactionId(tnxId);
								cardPaymentInfo.setPaymentBrokerRefNo(rs.getInt("T_REF_NO"));

								BigDecimal baseAmount = rs.getBigDecimal("AMOUNT");
								cardPaymentInfo.setTotalAmount(baseAmount);
								cardPaymentInfo.setAuthorizationId(rs.getString("AID_COMPANY"));

								payCurrencyDTO = new PayCurrencyDTO(BeanUtils.nullHandler(rs.getString("PAYMENT_CURRENCY_CODE")),
										null);
								cardPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);

								paymentMap.put(new Integer(tnxId), cardPaymentInfo);
							}
						}

						return paymentMap;
					}
				});
		log.debug("Exit getCreditCardInfo");
		return paymentMap;
	}

	/**
	 * Return Credit card information
	 * 
	 * @param colTnxIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, CardPaymentInfo> getCreditCardInfoFromRESPCD(Collection<Integer> colTnxIds, boolean isOwnTnx) {
		Map<Integer, CardPaymentInfo> paymentMap = new HashMap<Integer, CardPaymentInfo>();
		if (colTnxIds != null && colTnxIds.size() > 0) {

			if (log.isDebugEnabled()) {
				log.debug("Inside getCreditCardInfoFromRESPCD");
			}
			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

			String sql = creditCardInfoFromRESPCDBuilder(colTnxIds, isOwnTnx);
			if (log.isDebugEnabled()) {
				log.debug("############################################");
				log.debug(" SQL to excute            : " + sql);
				log.debug(" SQL parameters           : " + colTnxIds);
				log.debug("############################################");
			}
			paymentMap = (Map<Integer, CardPaymentInfo>) jt.query(sql, colTnxIds.toArray(), new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Map<Integer, CardPaymentInfo> paymentMap = new HashMap<Integer, CardPaymentInfo>();
					CardPaymentInfo cardPaymentInfo;
					int tnxId;

					if (rs != null) {
						while (rs.next()) {
							cardPaymentInfo = new CardPaymentInfo();
							tnxId = rs.getInt("TXN_ID");
							cardPaymentInfo.setNo(BeanUtils.nullHandler(rs.getString("NO")));
							cardPaymentInfo.setName(BeanUtils.nullHandler(rs.getString("C_NAME")));
							cardPaymentInfo.setEDate(BeanUtils.nullHandler(rs.getString("E_DATE")));
							cardPaymentInfo.setNoFirstDigits(BeanUtils.nullHandler(rs.getString("FIRST_DIGITS_OF_NUMBER")));
							cardPaymentInfo.setAuthorizationId(rs.getString("AUTHORIZATION_ID"));
							cardPaymentInfo.setPaymentBrokerRefNo(rs.getInt("TRANSACTION_REF_NO"));
							cardPaymentInfo.setNoLastDigits(BeanUtils.nullHandler(rs.getString("NO")));
							paymentMap.put(new Integer(tnxId), cardPaymentInfo);
						}
					}

					return paymentMap;
				}
			});

			if (log.isDebugEnabled())
				log.debug("Exit getCreditCardInfoFromRESPCD");
		}
		return paymentMap;
	}

	private String creditCardInfoFromRESPCDBuilder(Collection<Integer> colTnxIds, boolean isOwnTnx) {

		String tnxField = isOwnTnx ? "TXN_ID" : "EXT_TXN_ID";

		Iterator<Integer> itColTnxIds = colTnxIds.iterator();
		String sql = "";
		int i = 0;
		while (itColTnxIds.hasNext()) {
			itColTnxIds.next();
			if (i == 0) {
				sql += " cc.# = ? ";
			} else {
				sql += " OR cc.# = ? ";
			}
			i++;
		}
		sql = " SELECT cc.NO NO, cc.NAME C_NAME, cc.E_DATE E_DATE, cc.FIRST_DIGITS_OF_NUMBER FIRST_DIGITS_OF_NUMBER, cc.# TXN_ID, cc.AUTHORIZATION_ID AUTHORIZATION_ID, cc.TRANSACTION_REF_NO TRANSACTION_REF_NO FROM T_RES_PCD cc WHERE ("
				+ sql + ") ORDER BY cc.#";

		return StringUtils.replace(sql, "#", tnxField);
	}

	/**
	 * Returns Collection TempPaymentTnx
	 * 
	 * @param colTnxIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<TempPaymentTnx> getTempPaymentDetails(int paymentBrokerRefNo) {
		log.debug("Inside getTempPaymentDetails");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT t.tpt_id, t.timestamp, t.contact_person, t.amount,"
				+ " t.telephone_no, t.user_id, t.customer_id, t.channel_id,"
				+ " t.cc_last_4_digits, t.version, t.dr_cr, t.pnr, t.status,"
				+ " t.ip_address, t.mobile_no, t.status_history, t.comments,"
				+ " t.payment_currency_amount, t.payment_currency_code" + " FROM t_temp_payment_tnx t, t_ccard_payment_status c"
				+ " WHERE t.tpt_id = c.tpt_id" + " AND c.transaction_ref_no=" + paymentBrokerRefNo + "";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		// log.debug(" SQL parameters : " + strTnxInclause);
		log.debug("############################################");

		Collection<TempPaymentTnx> mapTnxIdAndAuthorizationId = (Collection<TempPaymentTnx>) jt.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<TempPaymentTnx> colTempTxn = new ArrayList<TempPaymentTnx>();
						// Integer tnxId;
						// String ccLast4Digits;
						TempPaymentTnx tempPaymentTnx = new TempPaymentTnx();

						if (rs != null) {
							while (rs.next()) {
								tempPaymentTnx.setTnxId(BeanUtils.parseInteger(rs.getInt("tpt_id")));
								tempPaymentTnx.setLast4DigitsCC(BeanUtils.nullHandler(rs.getString("cc_last_4_digits")));

								colTempTxn.add(tempPaymentTnx);
							}
						}

						return colTempTxn;
					}
				});

		log.debug("Exit getTempPaymentDetails");
		return mapTnxIdAndAuthorizationId;
	}

	/**
	 * Return Credit card information
	 * 
	 * @param colTnxIds
	 * @param lstNominalCodes
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<PaymentDetailDTO>> getDetailPNRPaxPayments(Collection<Integer> strPnrPaxIds,
			List<Integer> lstNominalCodes) {
		log.debug("Inside getDetailPNRPaxPayments");

		String strSqlNorminalCodes = BeanUtils.constructINString(lstNominalCodes);
		String strSqlPnrPaxIds = Util.buildIntegerInClauseContent(strPnrPaxIds);

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT a.TXN_ID, a.TNX_DATE, a.PNR_PAX_ID, a.AMOUNT, a.NOMINAL_CODE, b.NO, c.PAYMENT_CURRENCY_CODE, a.receipt_no, "
				+ " c.TOTAL_AMOUNT_PAYCUR, ac.payment_mode "
				+ "FROM T_PAX_TRANSACTION a, T_RES_PCD b, T_PAX_TXN_BREAKDOWN_SUMMARY c, t_actual_payment_method ac "
				+ " WHERE b.TXN_ID (+) = a.TXN_ID AND c.PAX_TXN_ID (+) = a.TXN_ID "
				+ " AND a.payment_mode_id = ac.payment_mode_id (+)" + " AND a.PNR_PAX_ID IN (" + strSqlPnrPaxIds
				+ ") AND a.NOMINAL_CODE IN (" + strSqlNorminalCodes + ") ORDER BY a.PNR_PAX_ID, a.NOMINAL_CODE ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL parameters           : " + strPnrPaxIds);
		log.debug("############################################");

		Map<Integer, Collection<PaymentDetailDTO>> mapPnrPaxIdAndColPaymentDetailDTO = (Map<Integer, Collection<PaymentDetailDTO>>) jt
				.query(sql, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, Collection<PaymentDetailDTO>> mapPnrPaxIdAndColPaymentDetailDTO = new HashMap<Integer, Collection<PaymentDetailDTO>>();
						PaymentDetailDTO paymentDetailDTO;
						Collection<PaymentDetailDTO> colHolder;
						Integer pnrPaxId;

						if (rs != null) {
							while (rs.next()) {
								pnrPaxId = BeanUtils.parseInteger(rs.getInt("PNR_PAX_ID"));
								paymentDetailDTO = new PaymentDetailDTO();

								paymentDetailDTO.setPnrPaxId(pnrPaxId);
								paymentDetailDTO.setPassengerName("");
								paymentDetailDTO.setAuthorizationId("");
								paymentDetailDTO.setPaymentDate(rs.getDate("TNX_DATE"));
								paymentDetailDTO.setAmount(rs.getBigDecimal("AMOUNT"));
								paymentDetailDTO.setNominalCode(BeanUtils.parseInteger(rs.getInt("NOMINAL_CODE")));
								paymentDetailDTO.setTnxId(BeanUtils.parseInteger(rs.getInt("TXN_ID")));
								paymentDetailDTO.setLast4DigitsOfCCNo(
										BeanUtils.getLast4Digits(BeanUtils.nullHandler(rs.getString("NO"))));
								paymentDetailDTO.setRecieptNumber(BeanUtils.nullHandler(rs.getString("receipt_no")));

								paymentDetailDTO.setPayCurrencyCode(BeanUtils.nullHandler(rs.getString("PAYMENT_CURRENCY_CODE")));
								paymentDetailDTO.setPayCurrencyAmount(rs.getBigDecimal("TOTAL_AMOUNT_PAYCUR"));
								paymentDetailDTO.setPaymentMode(BeanUtils.nullHandler(rs.getString("payment_mode")));

								// If pnr pax id exist
								if (mapPnrPaxIdAndColPaymentDetailDTO.containsKey(pnrPaxId)) {
									colHolder = (Collection<PaymentDetailDTO>) mapPnrPaxIdAndColPaymentDetailDTO.get(pnrPaxId);
									colHolder.add(paymentDetailDTO);
								} else {
									colHolder = new ArrayList<PaymentDetailDTO>();
									colHolder.add(paymentDetailDTO);
									mapPnrPaxIdAndColPaymentDetailDTO.put(pnrPaxId, colHolder);
								}
							}
						}

						return mapPnrPaxIdAndColPaymentDetailDTO;
					}
				});

		log.debug("Exit getDetailPNRPaxPayments");
		return mapPnrPaxIdAndColPaymentDetailDTO;
	}

	/**
	 * Return Pnrs for the credit card information
	 * 
	 * @param no
	 * @param eDate
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getPnrsForCreditCardInfo(String no, String eDate, String ccAuthCode)
			throws CommonsDataAccessException {
		log.debug("Inside getPnrsForCreditCardInfo");

		// Validating the parameters
		if ((no == null || no.equals("") || eDate == null) && ccAuthCode == null) {
			throw new CommonsDataAccessException("airreservations.arg.invalidCNoAndEDate");
		}

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT DISTINCT cc.PNR FROM T_RES_PCD cc WHERE cc.NO = ? AND cc.E_DATE = ? ";
		Object[] parameters = new Object[] { no, eDate };

		if (ccAuthCode != null) {
			sql = " SELECT DISTINCT cc.PNR FROM T_RES_PCD cc, T_CCARD_PAYMENT_STATUS ac "
					+ " WHERE cc.TRANSACTION_REF_NO = ac.TRANSACTION_REF_NO ";
		}

		if (no != null && ccAuthCode != null) {
			sql += " AND cc.NO = ? AND cc.E_DATE = ? AND ac.AID_CCCOMPNAY = ? ";
			parameters = new Object[] { no, eDate, ccAuthCode };
		}

		if (no == null && ccAuthCode != null) {
			sql += " AND ac.AID_CCCOMPNAY = ? ";
			parameters = new Object[] { ccAuthCode };
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL parameters           : " + parameters);
		log.debug("############################################");

		Collection<String> pnrs = (Collection<String>) jt.query(sql, parameters, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> pnrs = new HashSet<String>();

				if (rs != null) {
					while (rs.next()) {
						pnrs.add(BeanUtils.nullHandler(rs.getString("PNR")));
					}
				}
				return pnrs;
			}
		});

		log.debug("Exit getPnrsForCreditCardInfo");
		return pnrs;
	}

	/**
	 * Returns credit card information
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<CreditCardInformationDTO> getCreditCardInformation() {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "select CARD_TYPE_ID, CARD_TYPE, PAYMENT_NC, REUND_NC from T_CREDITCARD_TYPE";

		Collection<CreditCardInformationDTO> colCreditCardInformationDTO = (Collection<CreditCardInformationDTO>) jt.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<CreditCardInformationDTO> colCreditCardInformationDTO = new ArrayList<CreditCardInformationDTO>();
						CreditCardInformationDTO ccdto;

						if (rs != null) {
							while (rs.next()) {
								ccdto = new CreditCardInformationDTO();
								ccdto.setId(rs.getInt("CARD_TYPE_ID"));
								ccdto.setCardType(BeanUtils.nullHandler(rs.getString("CARD_TYPE")));
								ccdto.setPaymentNominalCode(rs.getInt("PAYMENT_NC"));
								ccdto.setRefundNominalCode(rs.getInt("REUND_NC"));

								colCreditCardInformationDTO.add(ccdto);
							}
						}

						return colCreditCardInformationDTO;
					}
				});

		return colCreditCardInformationDTO;
	}

	/**
	 * Returns the tempory payment information
	 * 
	 * @param date
	 * @param colStatus
	 * @param colIPGIdentificationDTO
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<IPGQueryDTO> getTemporyPaymentInfo(Date reconcileEndDate, Collection<String> colStatus,
			Collection<String> colStateHistory, Collection<IPGIdentificationParamsDTO> colIPGIdentificationDTO) {
		log.debug("Inside getTemporyPaymentInfo");

		// Invoking this requires some valid parameters. This operations is expensive
		if (reconcileEndDate == null || colStatus == null || colStatus.size() == 0 || colIPGIdentificationDTO == null
				|| colIPGIdentificationDTO.size() == 0) {
			throw new CommonsDataAccessException("airreservations.arg.invalid.null");
		}

		int noOfBackDays = AppSysParamsUtil.getNoOfBackDaysToReconcileCardPayments();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, -1 * noOfBackDays);
		Date reconcileStartDate = cal.getTime();

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<String> ipgNames = new ArrayList<String>();
		for (IPGIdentificationParamsDTO ipg : colIPGIdentificationDTO) {
			ipgNames.add(ipg.getFQIPGConfigurationName());
		}
		String strStatesInclause = Util.buildStringInClauseContent(colStatus);
		String strHistoryStatesInclause = Util.buildStringInClauseContent(colStateHistory);
		String strPaymentGatewayInclause = Util.buildStringInClauseContent(ipgNames);

		String sql = " SELECT A.TPT_ID, A.USER_ID, A.PAYMENT_CURRENCY_CODE, A.PAYMENT_CURRENCY_AMOUNT, A.AMOUNT, A.TIMESTAMP,"
				+ " B.NM_TRANSACTION_REFERENCE, B.TRANSACTION_REF_NO, B.GATEWAY_NAME, B.RESPONSE_TEXT, A.PRODUCT_TYPE, "
				+ " A.CONTACT_PERSON, A.CC_LAST_4_DIGITS, B.NM_TRANSACTION_REFERENCE, C.PNR, C.ORIGINATOR_PNR, C.STATUS, C.TOTAL_FARE, C.TOTAL_TAX, C.TOTAL_SURCHARGES, C.TOTAL_CNX, C.TOTAL_MOD, C.TOTAL_ADJ, B.GATEWAY_NAME "
				+ " FROM T_TEMP_PAYMENT_TNX A, T_CCARD_PAYMENT_STATUS B, T_RESERVATION C WHERE A.TPT_ID = B.TPT_ID AND B.GATEWAY_NAME IN ("
				+ strPaymentGatewayInclause + ")  " + " AND ( A.STATUS IN (" + strStatesInclause + ") OR A.STATUS_HISTORY IN ("
				+ strHistoryStatesInclause + ") ) " + " AND A.PNR = C.PNR(+) "
				+ " AND ( A.TIMESTAMP BETWEEN ? AND ? ) ORDER BY A.TPT_ID, B.TRANSACTION_REF_NO ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL parameters           : " + reconcileStartDate + ", " + reconcileEndDate + ", " + ipgNames + ", "
				+ colStatus + ", " + colStateHistory);
		log.debug("############################################");

		Collection<IPGQueryDTO> colIPGQueryDTO = (Collection<IPGQueryDTO>) jt.query(sql,
				new Object[] { BeanUtils.getTimestamp(reconcileStartDate), BeanUtils.getTimestamp(reconcileEndDate) },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<Integer> colTmpPayIds = new HashSet<Integer>();
						Collection<IPGQueryDTO> colIPGQueryDTO = new ArrayList<IPGQueryDTO>();
						IPGQueryDTO ipgQueryDTO;
						int temporyPaymentId;
						String referenceIdForIPG;
						String key;
						AppIndicatorEnum appIndicatorEnum = null;

						if (rs != null) {
							while (rs.next()) {
								temporyPaymentId = rs.getInt("TPT_ID");
								referenceIdForIPG = BeanUtils.nullHandler(rs.getString("NM_TRANSACTION_REFERENCE"));

								// Theoritically NM_TRANSACTION_REFERENCE should always have a value
								if (referenceIdForIPG.length() > 1) {
									key = referenceIdForIPG.substring(0, 1);

									// If it's a IBE
									if (AppIndicatorEnum.APP_IBE.toString().equals(key)) {
										appIndicatorEnum = AppIndicatorEnum.APP_IBE;
										// If it's a XBE
									} else if (AppIndicatorEnum.APP_XBE.toString().equals(key)) {
										appIndicatorEnum = AppIndicatorEnum.APP_XBE;
									} else if (AppIndicatorEnum.APP_ANDROID.toString().equals(key)) {
										appIndicatorEnum = AppIndicatorEnum.APP_ANDROID;
									} else if (AppIndicatorEnum.APP_IOS.toString().equals(key)) {
										appIndicatorEnum = AppIndicatorEnum.APP_IOS;
									}

									// Theoritically this will belongs to IBE or XBE
									if (appIndicatorEnum != null) {
										ipgQueryDTO = new IPGQueryDTO();
										ipgQueryDTO.setTemporyPaymentId(temporyPaymentId);
										ipgQueryDTO.setPaymentBrokerRefNo(rs.getInt("TRANSACTION_REF_NO"));
										ipgQueryDTO.setAmount(rs.getBigDecimal("PAYMENT_CURRENCY_AMOUNT"));
										ipgQueryDTO.setProductType(rs.getString("PRODUCT_TYPE"));
										ipgQueryDTO.setBaseAmount(BeanUtils.nullToZero(rs.getBigDecimal("AMOUNT")));
										ipgQueryDTO.setIpgIdentificationParamsDTO(new IPGIdentificationParamsDTO(
												BeanUtils.nullHandler(rs.getString("GATEWAY_NAME"))));
										ipgQueryDTO.setAppIndicatorEnum(appIndicatorEnum);
										ipgQueryDTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));

										String groupPNR = BeanUtils.nullHandler(rs.getString("ORIGINATOR_PNR"));
										ipgQueryDTO.setGroupPnr(groupPNR.length() > 0 ? groupPNR : null);

										ipgQueryDTO.setPnrStatus(BeanUtils.nullHandler(rs.getString("STATUS")));
										ipgQueryDTO.setPnrTotalCharge(
												AccelAeroCalculator.add(BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_FARE")),
														BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_TAX")),
														BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_SURCHARGES")),
														BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_CNX")),
														BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_MOD")),
														BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_ADJ"))));
										ipgQueryDTO.setGatewayName(BeanUtils.nullHandler(rs.getString("GATEWAY_NAME")));
										ipgQueryDTO.setTimestamp(rs.getTimestamp("TIMESTAMP"));
										ipgQueryDTO.setCreditCardNo(BeanUtils.nullHandler(rs.getString("CC_LAST_4_DIGITS")));
										ipgQueryDTO.setAuthorizationNo(
												BeanUtils.nullHandler(rs.getString("NM_TRANSACTION_REFERENCE")));
										ipgQueryDTO.setPaymentCurrencyCode(
												BeanUtils.nullHandler(rs.getString("PAYMENT_CURRENCY_CODE")));
										ipgQueryDTO.setUserId(BeanUtils.nullHandler(rs.getString("USER_ID")));

										// Nili April 25, 2012
										// TODO. Logic specific to ogone. Need to port for other payment gateways as
										// well.
										// Currently in t_temp_payment we don't have the card type. Since this process
										// queries to
										// create reservations... I need to locate the exact credit card type.
										// Currency for ogone this way will work. But for other payment gateways it will
										// go as generic.
										// We can fix this, if we store the card type in the t_temp_payment request it
										// self.
										String responseText = BeanUtils.nullHandler(rs.getString("RESPONSE_TEXT"));
										if (responseText.length() > 0) {
											if (responseText.indexOf("BRAND : MasterCard") != -1) {
												ipgQueryDTO.setPaymentType(PaymentType.CARD_MASTER);
											} else if (responseText.indexOf("BRAND : VISA") != -1) {
												ipgQueryDTO.setPaymentType(PaymentType.CARD_VISA);
											} else {
												ipgQueryDTO.setPaymentType(PaymentType.CARD_GENERIC);
											}
										} else {
											ipgQueryDTO.setPaymentType(PaymentType.CARD_GENERIC);
										}

										if (!colTmpPayIds.contains(temporyPaymentId)) {
											colIPGQueryDTO.add(ipgQueryDTO);
											colTmpPayIds.add(temporyPaymentId);
										}
									} else {
										log.error(" Can not locate whether this record belongs to IBE or XBE [TPT ID is "
												+ temporyPaymentId + "] [NM_TRANSACTION_REFERENCE is " + referenceIdForIPG
												+ "] ");
									}
								} else {
									log.error(" Can not locate NM_TRANSACTION_REFERENCE reference [TPT ID is " + temporyPaymentId
											+ "] [NM_TRANSACTION_REFERENCE is " + referenceIdForIPG + "] ");
								}
							}
						}

						return colIPGQueryDTO;
					}
				});

		log.debug("Exit getTemporyPaymentInfo");
		return colIPGQueryDTO;
	}

	/**
	 * Return TemporyPaymentInfo
	 */
	public IPGQueryDTO getTemporyPaymentInfo(int paymentBrokerRefNo) {
		if (log.isDebugEnabled())
			log.debug("Inside getTemporyPaymentInfo");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append(
				" SELECT A.TPT_ID, A.PAYMENT_CURRENCY_CODE, A.PAYMENT_CURRENCY_AMOUNT, A.AMOUNT, A.TIMESTAMP, B.NM_TRANSACTION_REFERENCE, B.TRANSACTION_REF_NO, B.GATEWAY_NAME, B.RESPONSE_TEXT, A.USER_ID,  ");
		sql.append(
				" A.CONTACT_PERSON, A.CC_LAST_4_DIGITS, B.NM_TRANSACTION_REFERENCE, C.PNR, C.ORIGINATOR_PNR, C.STATUS, C.TOTAL_FARE, C.TOTAL_TAX, C.TOTAL_SURCHARGES, C.TOTAL_CNX, C.TOTAL_MOD, C.TOTAL_ADJ, B.GATEWAY_NAME ");
		sql.append(" FROM T_TEMP_PAYMENT_TNX A, T_CCARD_PAYMENT_STATUS B, T_RESERVATION C WHERE ");
		sql.append(" B.TRANSACTION_REF_NO = ").append(paymentBrokerRefNo);
		sql.append(" AND A.TPT_ID = B.TPT_ID ");
		sql.append(" AND A.STATUS = '").append(ReservationInternalConstants.TempPaymentTnxTypes.INITIATED).append("' ");
		sql.append(" AND A.PNR = C.PNR(+) ");

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql.toString());
		log.debug("############################################");

		IPGQueryDTO IPGQueryDTO = (IPGQueryDTO) jt.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colTmpPayIds = new HashSet<Integer>();
				IPGQueryDTO ipgQueryDTO = null;
				int temporyPaymentId;
				String referenceIdForIPG;
				String key;
				AppIndicatorEnum appIndicatorEnum = null;

				if (rs != null) {
					if (rs.next()) {
						temporyPaymentId = rs.getInt("TPT_ID");
						referenceIdForIPG = BeanUtils.nullHandler(rs.getString("NM_TRANSACTION_REFERENCE"));

						if (referenceIdForIPG.length() > 1) {
							key = referenceIdForIPG.substring(0, 1);
							/* If it's a IBE */
							if (AppIndicatorEnum.APP_IBE.toString().equals(key)) {
								appIndicatorEnum = AppIndicatorEnum.APP_IBE;
								/* If it's a XBE */
							} else if (AppIndicatorEnum.APP_XBE.toString().equals(key)) {
								appIndicatorEnum = AppIndicatorEnum.APP_XBE;
							} else if (AppIndicatorEnum.APP_ANDROID.toString().equals(key)) {
								appIndicatorEnum = AppIndicatorEnum.APP_ANDROID;
							} else if (AppIndicatorEnum.APP_IOS.toString().equals(key)) {
								appIndicatorEnum = AppIndicatorEnum.APP_IOS;
							}

							/* Theoretically this will belongs to IBE or XBE */
							if (appIndicatorEnum != null) {
								ipgQueryDTO = new IPGQueryDTO();
								ipgQueryDTO.setTemporyPaymentId(temporyPaymentId);
								ipgQueryDTO.setPaymentBrokerRefNo(rs.getInt("TRANSACTION_REF_NO"));
								ipgQueryDTO.setAmount(rs.getBigDecimal("PAYMENT_CURRENCY_AMOUNT"));
								ipgQueryDTO.setBaseAmount(BeanUtils.nullToZero(rs.getBigDecimal("AMOUNT")));
								ipgQueryDTO.setIpgIdentificationParamsDTO(
										new IPGIdentificationParamsDTO(BeanUtils.nullHandler(rs.getString("GATEWAY_NAME"))));
								ipgQueryDTO.setAppIndicatorEnum(appIndicatorEnum);
								ipgQueryDTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));

								String groupPNR = BeanUtils.nullHandler(rs.getString("ORIGINATOR_PNR"));
								ipgQueryDTO.setGroupPnr(groupPNR.length() > 0 ? groupPNR : null);

								ipgQueryDTO.setPnrStatus(BeanUtils.nullHandler(rs.getString("STATUS")));
								ipgQueryDTO.setPnrTotalCharge(
										AccelAeroCalculator.add(BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_FARE")),
												BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_TAX")),
												BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_SURCHARGES")),
												BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_CNX")),
												BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_MOD")),
												BeanUtils.nullToZero(rs.getBigDecimal("TOTAL_ADJ"))));
								ipgQueryDTO.setGatewayName(BeanUtils.nullHandler(rs.getString("GATEWAY_NAME")));
								ipgQueryDTO.setTimestamp(rs.getTimestamp("TIMESTAMP"));
								ipgQueryDTO.setCreditCardNo(BeanUtils.nullHandler(rs.getString("CC_LAST_4_DIGITS")));
								ipgQueryDTO.setAuthorizationNo(BeanUtils.nullHandler(rs.getString("NM_TRANSACTION_REFERENCE")));
								ipgQueryDTO.setPaymentCurrencyCode(BeanUtils.nullHandler(rs.getString("PAYMENT_CURRENCY_CODE")));
								ipgQueryDTO.setUserId(BeanUtils.nullHandler(rs.getString("USER_ID")));

								if (!colTmpPayIds.contains(temporyPaymentId)) {
									colTmpPayIds.add(temporyPaymentId);
								}
							} else {
								log.error(" Can not locate whether this record belongs to IBE or XBE [TPT ID is "
										+ temporyPaymentId + "] [NM_TRANSACTION_REFERENCE is " + referenceIdForIPG + "] ");
							}
						} else {
							log.error(" Can not locate NM_TRANSACTION_REFERENCE reference [TPT ID is " + temporyPaymentId
									+ "] [NM_TRANSACTION_REFERENCE is " + referenceIdForIPG + "] ");
						}
					}
				}

				return ipgQueryDTO;
			}
		});

		log.debug("Exit getTemporyPaymentInfo");
		return IPGQueryDTO;
	}

	/**
	 * Returns map key --> TnxId(java.lang.Integer) Value --> Authorization Id(java.lang.String)
	 * 
	 * @param colTnxIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> getAuthorizationIdsForCCPayments(Collection<Integer> colTnxIds) {
		log.debug("Inside getAuthorizationIdsForCCPayments");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String strTnxInclause = Util.buildIntegerInClauseContent(colTnxIds);

		String sql = " SELECT resPCD.TXN_ID, payments.AID_CCCOMPNAY FROM T_RES_PCD resPCD, "
				+ " T_CCARD_PAYMENT_STATUS payments WHERE resPCD.TRANSACTION_REF_NO = payments.TRANSACTION_REF_NO "
				+ " AND payments.TRANSACTION_RESULT_CODE = 1 AND payments.RESULT_CODE = 0 " + " AND resPCD.TXN_ID IN ("
				+ strTnxInclause + ") " + " ORDER BY resPCD.TXN_ID, payments.AID_CCCOMPNAY ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL parameters           : " + strTnxInclause);
		log.debug("############################################");

		Map<Integer, String> mapTnxIdAndAuthorizationId = (Map<Integer, String>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> mapTnxIdAndAuthorizationId = new HashMap<Integer, String>();
				Integer tnxId;
				String authorizationId;

				if (rs != null) {
					while (rs.next()) {
						tnxId = BeanUtils.parseInteger(rs.getInt("TXN_ID"));
						authorizationId = BeanUtils.nullHandler(rs.getString("AID_CCCOMPNAY"));

						mapTnxIdAndAuthorizationId.put(tnxId, authorizationId);
					}
				}

				return mapTnxIdAndAuthorizationId;
			}
		});

		log.debug("Exit getAuthorizationIdsForCCPayments");
		return mapTnxIdAndAuthorizationId;
	}

	/**
	 * Save/Update External Payment Transaction Information
	 */
	public void saveOrUpdateExternalPayTxInfo(ExternalPaymentTnx externalPaymentTnx) {
		hibernateSaveOrUpdate(externalPaymentTnx);
	}

	/**
	 * Return external payment transactions for the specified criteria.
	 * 
	 * @param criteriaDTO
	 * @return Map<PNR, PNRExtTransactionsTO>
	 */
	@SuppressWarnings("unchecked")
	public Map<String, PNRExtTransactionsTO> getExtPayTransactions(ExtPayTxCriteriaDTO criteriaDTO) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Map<String, PNRExtTransactionsTO> transactionsMap = (Map<String, PNRExtTransactionsTO>) jt
				.query(new ExternalPaymentTnxPrepareStmtCreator(criteriaDTO), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, PNRExtTransactionsTO> transactionsMap = new HashMap<String, PNRExtTransactionsTO>();
						if (rs != null) {
							while (rs.next()) {
								ExternalPaymentTnx tnx = new ExternalPaymentTnx();
								tnx.setExtPayTxnId(new Integer(rs.getInt("EPT_ID")));
								tnx.setExternalPayId(rs.getString("EXTERNAL_PAY_ID"));
								tnx.setInternalPayId(new Integer(rs.getInt("INTERNAL_PAY_ID")));
								tnx.setBalanceQueryKey(rs.getString("BAL_QUERY_KEY"));
								tnx.setAmount(rs.getBigDecimal("AMOUNT"));
								tnx.setStatus(rs.getString("STATUS"));
								tnx.setExternalPayStatus(rs.getString("EXTERNAL_PAY_STATUS"));
								tnx.setInternalTnxStartTimestamp(rs.getTimestamp("INTERNAL_START_TIME"));
								tnx.setInternalTnxEndTimestamp(rs.getTimestamp("INTERNAL_END_TIME"));
								tnx.setExternalTnxEndTimestamp(rs.getTimestamp("EXTERNAL_END_TIME"));
								tnx.setPnr(rs.getString("PNR"));
								tnx.setUserId(rs.getString("USER_ID"));
								tnx.setChannel(rs.getString("CHANNEL"));
								tnx.setAdditionalResults(rs.getString("ADDITIONAL_RESULTS"));
								tnx.setRemarks(rs.getString("REMARKS"));
								tnx.setReconcilationStatus(rs.getString("RECONCILATION_STATUS"));
								tnx.setReconciledTimestamp(rs.getTimestamp("RECONCILIATION_TIME"));
								tnx.setReconciliationSource(rs.getString("RECONCILIATION_SOURCE"));
								tnx.setManualReconBy(rs.getString("MANUAL_RECON_BY"));
								tnx.setVersion(rs.getLong("VERSION"));
								tnx.setAgentCode(rs.getString("AGENT_CODE"));
								tnx.setPaxCount(new Integer(rs.getInt("PAX_COUNT")));
								tnx.setExternalReqID(rs.getString("EXTERNAL_REQ_ID"));

								if (transactionsMap.containsKey(tnx.getPnr())) {
									PNRExtTransactionsTO pnrTx = (PNRExtTransactionsTO) transactionsMap.get(tnx.getPnr());
									pnrTx.addExtPayTransactions(tnx);
								} else {
									PNRExtTransactionsTO pnrTx = new PNRExtTransactionsTO();
									pnrTx.setPnr(tnx.getPnr());
									pnrTx.addExtPayTransactions(tnx);
									transactionsMap.put(tnx.getPnr(), pnrTx);
								}

							}
						}
						return transactionsMap;
					}
				});

		return transactionsMap;
	}

	private final class SequenceExtractor implements ResultSetExtractor {
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			Integer transactionSeq = -1;
			if (rs != null) {
				while (rs.next()) {
					transactionSeq = rs.getInt("SEQ");
				}
			}
			return transactionSeq;
		}
	}

	private class ExternalPaymentTnxPrepareStmtCreator implements PreparedStatementCreator {

		private String queryString;

		@SuppressWarnings("rawtypes")
		private List queryValues;

		@SuppressWarnings({ "rawtypes", "unchecked" })
		private ExternalPaymentTnxPrepareStmtCreator(ExtPayTxCriteriaDTO criteriaDTO) {

			List values = new ArrayList();

			StringBuilder sql = new StringBuilder();
			int criteriaCount = 0;

			sql.append(
					" Select EPT.*,(Select agent_code  from T_User  where user_id = ept.user_id) Agent_code from t_external_payment_tx EPT Where ");
			if (criteriaDTO.getPnr() != null) {
				sql.append(" EPT.pnr like ?");
				values.add(criteriaDTO.getPnr());
				++criteriaCount;
			}

			if (criteriaDTO.getBalanceQueryKey() != null) {
				sql.append((criteriaCount > 0 ? " AND " : "") + " EPT.bal_Query_Key like ?");
				values.add(criteriaDTO.getBalanceQueryKey());
				++criteriaCount;
			}

			if (criteriaDTO.getStartTimestamp() != null && criteriaDTO.getEndTimestamp() != null) {
				sql.append((criteriaCount > 0 ? " AND " : "") + " EPT.internal_Start_Time between ? and ? ");
				values.add(new java.sql.Timestamp(criteriaDTO.getStartTimestamp().getTime()));
				values.add(new java.sql.Timestamp(criteriaDTO.getEndTimestamp().getTime()));
				++criteriaCount;
			}

			if (criteriaDTO.getStatuses() != null && criteriaDTO.getStatuses().size() > 0) {
				sql.append((criteriaCount > 0 ? " AND " : "") + " EPT.status in ("
						+ Util.buildStringInClauseContent(criteriaDTO.getStatuses()) + ") ");
				++criteriaCount;
			}

			if (criteriaDTO.getReconStatusToInclude() != null && criteriaDTO.getReconStatusToInclude().size() > 0) {
				if (criteriaDTO.isIncludeAbortedManualReconTxns()) {
					Collection failedStatuses = new ArrayList();
					failedStatuses.add(ReservationInternalConstants.ExtPayTxStatus.ABORTED);
					sql.append((criteriaCount > 0 ? " AND " : "") + " ( EPT.reconcilation_Status in ("
							+ Util.buildStringInClauseContent(criteriaDTO.getReconStatusToInclude()) + ") OR "
							+ " ( EPT.reconciliation_Source = ? AND " + " EPT.status in ("
							+ Util.buildStringInClauseContent(failedStatuses) + ") ) ) ");
					values.add(ReservationInternalConstants.ExtPayTxReconSourceType.MANUAL);

				} else {
					sql.append((criteriaCount > 0 ? " AND " : "") + " EPT.reconcilation_Status in ("
							+ Util.buildStringInClauseContent(criteriaDTO.getReconStatusToInclude()) + ") ");
				}
				++criteriaCount;
			}

			if (criteriaDTO.getAgentCode() != null) {
				sql.append((criteriaCount > 0 ? " AND " : "")
						+ " EPT.user_Id in (SELECT usr.user_Id FROM T_User usr WHERE usr.agent_Code = ?) ");
				values.add(criteriaDTO.getAgentCode());
				++criteriaCount;
			}

			if (criteriaDTO.getReconStatusToInclude() == null && criteriaDTO.isIncludeAbortedManualReconTxns()) {
				Collection failedStatuses = new ArrayList();
				failedStatuses.add(ReservationInternalConstants.ExtPayTxStatus.ABORTED);
				sql.append((criteriaCount > 0 ? " AND " : "") + " ( EPT.reconciliation_Source = ? AND " + " EPT.status in ("
						+ Util.buildStringInClauseContent(failedStatuses) + ") ) ");
				values.add(ReservationInternalConstants.ExtPayTxReconSourceType.MANUAL);

				++criteriaCount;
			}

			sql.append(" order by EPT.pnr, EPT.internal_Start_Time DESC ");

			this.queryString = sql.toString();
			this.queryValues = values;

		}

		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			for (int i = 0; i < queryValues.size(); i++) {
				Object value = queryValues.get(i);
				if (value instanceof String) {
					prepStmt.setString((i + 1), (String) value);
				} else if (value instanceof Integer) {
					prepStmt.setInt((i + 1), ((Integer) value).intValue());
				} else if (value instanceof Long) {
					prepStmt.setLong((i + 1), ((Long) value).longValue());
				} else if (value instanceof java.sql.Timestamp) {
					prepStmt.setTimestamp((i + 1), (java.sql.Timestamp) value);
				} else if (value instanceof Date) {
					prepStmt.setDate((i + 1), new java.sql.Date(((Date) value).getTime()));
				}
			}

			return prepStmt;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, PaymentModeDTO> getPaymentModes() {
		log.debug("Inside getPaymentModes");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "SELECT payment_code, description, generate_reciept, unique_sequence from t_payment_method";

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" SQL to excute            : " + sql);
			log.debug("############################################");
		}

		Map<String, PaymentModeDTO> mapPaymentModes = (Map<String, PaymentModeDTO>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, PaymentModeDTO> mapPaymentModes = new HashMap<String, PaymentModeDTO>();

				if (rs != null) {
					while (rs.next()) {
						PaymentModeDTO paymentModeDTO = new PaymentModeDTO();
						paymentModeDTO.setPaymentMode(BeanUtils.nullHandler(rs.getString("payment_code")));
						String enableReciept = BeanUtils.nullHandler(rs.getString("generate_reciept"));
						// String uniqueReciept = BeanUtils.nullHandler(rs.getString("unique_sequence"));

						if (enableReciept.equalsIgnoreCase("Y")) {
							paymentModeDTO.setGenerateReceipt(true);
						} else {
							paymentModeDTO.setGenerateReceipt(false);
						}

						if (enableReciept.equalsIgnoreCase("Y")) {
							paymentModeDTO.setGenerateUniqueRecieptforMode(true);
						} else {
							paymentModeDTO.setGenerateUniqueRecieptforMode(false);
						}

						mapPaymentModes.put(paymentModeDTO.getPaymentMode(), paymentModeDTO);
					}
				}
				return mapPaymentModes;
			}
		});

		log.debug("Exit getPaymentModes");
		return mapPaymentModes;
	}

	private String getNexttRecieptNumber(String mode) {
		log.debug("Inside getNextRecieptNumber");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql;
		if (mode == null) {
			sql = "SELECT f_get_receipt_no('ALL') as NEXTVAL FROM DUAL"; // TODO
																			// "SELECT S_RECIEPT_NO.NEXTVAL from dual";
		} else {
			sql = "SELECT f_get_receipt_no('" + mode + "') as NEXTVAL FROM DUAL"; // TODO
																					// "SELECT
																					// S_"+mode+"_RECIEPT_NO.NEXTVAL
																					// from dual";
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String recieptNumber = (String) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String recieptNumber = null;
				if (rs != null) {
					while (rs.next()) {
						recieptNumber = BeanUtils.nullHandler(rs.getString("NEXTVAL"));
					}
				}
				return recieptNumber;
			}
		});

		log.debug("Exit getNextRecieptNumber");
		return recieptNumber;
	}

	public String getNextCommonRecieptNumber() {
		return getNexttRecieptNumber(null);
	}

	public String getNextUniqueRecieptNumber(String paymentMode) {
		return getNexttRecieptNumber(paymentMode);
	}

	public void removeReservationPaxOndPayments(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		deleteAll(colReservationPaxOndPayment);
	}

	public void saveReservationPaxOndPayment(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		hibernateSaveOrUpdateAll(colReservationPaxOndPayment);
	}

	public Collection<ReservationPaxOndPayment> getReservationPaxOndPayment(Collection<Long> colPaxOndPaymentId) {
		StringBuilder sqlCondition = new StringBuilder();
		for (Long paxOndPaymentId : colPaxOndPaymentId) {
			if (sqlCondition.length() == 0) {
				sqlCondition.append(" WHERE paxOndPaymentId = ? ");
			} else {
				sqlCondition.append(" OR paxOndPaymentId = ? ");
			}
		}

		return find("FROM ReservationPaxOndPayment " + sqlCondition, colPaxOndPaymentId.toArray(),
				ReservationPaxOndPayment.class);
	}

	/**
	 * Return a map of PnrPaxOndChgId,Col<ReservationPaxOndPayment> for a given pax and colPnrPaxOndChgId from the
	 * ReservationPaxOndPayment
	 */
	@SuppressWarnings("unchecked")
	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPaymentByOndChargeId(Integer pnrPaxId,
			Collection<Long> colPnrPaxOndChgId) {
		Map<Long, Collection<ReservationPaxOndPayment>> mapOndPayments = new HashMap<Long, Collection<ReservationPaxOndPayment>>();
		if (colPnrPaxOndChgId != null && colPnrPaxOndChgId.size() > 0) {
			StringBuilder sqlCondition = new StringBuilder();
			for (Long pnrPaxOndChgId : colPnrPaxOndChgId) {
				if (sqlCondition.length() == 0) {
					sqlCondition.append(" WHERE pnrPaxOndChgId = ? ");
				} else {
					sqlCondition.append(" OR pnrPaxOndChgId = ? ");
				}
			}

			List<ReservationPaxOndPayment> lstReservationPaxOndPayment = find("FROM ReservationPaxOndPayment " + sqlCondition,
					colPnrPaxOndChgId.toArray(), ReservationPaxOndPayment.class);

			for (ReservationPaxOndPayment reservationPaxOndPayment : lstReservationPaxOndPayment) {
				if (BeanUtils.nullHandler(reservationPaxOndPayment.getPnrPaxId()).equals(BeanUtils.nullHandler(pnrPaxId))) {
					Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = mapOndPayments
							.get(reservationPaxOndPayment.getPnrPaxOndChgId());

					if (colReservationPaxOndPayment == null) {
						colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();
					}

					colReservationPaxOndPayment.add(reservationPaxOndPayment);
					mapOndPayments.put(reservationPaxOndPayment.getPnrPaxOndChgId(), colReservationPaxOndPayment);
				}
			}
		}

		return mapOndPayments;
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>>
			getPerPaxWiseOndChargeIds(Collection<Integer> pnrPaxIds) {
		Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapOndPayments = new HashMap<Integer, Map<Long, Collection<ReservationPaxOndPayment>>>();
		if (pnrPaxIds != null && pnrPaxIds.size() > 0) {

			String strPnrPaxIds = PlatformUtiltiies.constructINStringForInts(pnrPaxIds);

			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			String sql = " SELECT PPOP.PPOP_ID ,  PPOP.PFT_ID,  PPOP.NOMINAL_CODE,  PPOP.AMOUNT,  PPOP.PAYMENT_TXN_ID, "
					+ " PPOP.CHARGE_GROUP_CODE,   PPOP.PNR_PAX_ID,  PPOP.ORIGINAL_PPOP_ID,  PPOP.REFUND_TXN_ID, CR.CHARGE_CODE, PPOP.TRANSACTION_SEQ "
					+ " FROM T_PNR_PAX_OND_CHARGES PPOC,  T_PNR_PAX_OND_PAYMENTS PPOP,  T_CHARGE_RATE CR "
					+ " WHERE PPOC.PFT_ID  = PPOP.PFT_ID " + " AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID(+)"
					+ " AND PPOP.PNR_PAX_ID    IN (" + strPnrPaxIds + ") ";

			mapOndPayments = (Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>>) jt.query(sql,
					new ReservationPaxPaymentExtractor());
		}

		return mapOndPayments;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Collection<ReservationPaxOndPayment>> getPerPaxPaymentWiseOndPayments(Integer pnrPaxId,
			Collection<Long> paxTnxIds) {

		String hql = "select ondPaxPayment from ReservationPaxOndPayment ondPaxPayment where "
				+ " ondPaxPayment.pnrPaxId = :pnrPaxId and ondPaxPayment.paymentTnxId in (:paxTnxIds)";

		if (log.isDebugEnabled()) {
			log.debug("getting ond wise payemts for pax " + pnrPaxId + " and pax tns " + paxTnxIds + " ");
		}

		Query query = getSession().createQuery(hql).setLong("pnrPaxId", Long.valueOf(pnrPaxId)).setParameterList("paxTnxIds",
				paxTnxIds);

		Map<Integer, Collection<ReservationPaxOndPayment>> mapOndOayments = new HashMap<Integer, Collection<ReservationPaxOndPayment>>();
		List<ReservationPaxOndPayment> lstReservationPaxOndPayment = query.list();

		for (ReservationPaxOndPayment reservationPaxOndPayment : lstReservationPaxOndPayment) {
			Collection<ReservationPaxOndPayment> paxOndPayments = mapOndOayments
					.get(reservationPaxOndPayment.getPaymentTnxId().intValue());
			if (paxOndPayments == null) {
				paxOndPayments = new ArrayList<ReservationPaxOndPayment>();
				paxOndPayments.add(reservationPaxOndPayment);
				mapOndOayments.put(reservationPaxOndPayment.getPaymentTnxId().intValue(), paxOndPayments);
			} else {
				paxOndPayments.add(reservationPaxOndPayment);
			}
		}

		return mapOndOayments;
	}

	@SuppressWarnings("unchecked")
	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPayments(Collection<Long> colPaymentTnxIds) {

		Map<Long, Collection<ReservationPaxOndPayment>> mapOndPayments = new HashMap<Long, Collection<ReservationPaxOndPayment>>();
		if (colPaymentTnxIds != null && colPaymentTnxIds.size() > 0) {
			StringBuilder sqlCondition = new StringBuilder();
			for (Long paymentTnxId : colPaymentTnxIds) {
				if (sqlCondition.length() == 0) {
					sqlCondition.append(" WHERE paymentTnxId = " + paymentTnxId);
				} else {
					sqlCondition.append(" OR paymentTnxId = " + paymentTnxId);
				}
			}
			sqlCondition.append(" AND paxOndRefundId IS NULL");

			List<ReservationPaxOndPayment> lstReservationPaxOndPayment = find("FROM ReservationPaxOndPayment " + sqlCondition,
					ReservationPaxOndPayment.class);

			for (ReservationPaxOndPayment reservationPaxOndPayment : lstReservationPaxOndPayment) {
				Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = mapOndPayments
						.get(reservationPaxOndPayment.getPaymentTnxId());

				if (colReservationPaxOndPayment == null) {
					colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();
					colReservationPaxOndPayment.add(reservationPaxOndPayment);

					mapOndPayments.put(reservationPaxOndPayment.getPaymentTnxId(), colReservationPaxOndPayment);
				} else {
					colReservationPaxOndPayment.add(reservationPaxOndPayment);
				}
			}
		}

		return mapOndPayments;
	}

	@Override
	public String getActualPaymentMethodNameById(Integer actualPaymentMethodId) {
		log.debug("Inside getActualPaymentMethod");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT PAYMENT_MODE FROM T_ACTUAL_PAYMENT_METHOD WHERE PAYMENT_MODE_ID = " + actualPaymentMethodId;

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String actualPaymentMethod = (String) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String actualPaymentMethod = null;
				if (rs != null) {
					while (rs.next()) {
						actualPaymentMethod = BeanUtils.nullHandler(rs.getString("PAYMENT_MODE"));
					}
				}
				return actualPaymentMethod;
			}
		});

		log.debug("Exit getActualPaymentMethod");
		return BeanUtils.nullHandler(actualPaymentMethod);
	}

	@Override
	public void saveReservationPaxExtTnx(ReservationPaxExtTnx paxExtTnx) {

		if (log.isDebugEnabled()) {
			log.debug("inside saving paxExtTnx");

		}

		hibernateSaveOrUpdate(paxExtTnx);

		if (log.isDebugEnabled()) {
			log.debug("exit saving paxExtTnx");

		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<ReservationPaxExtTnx> getExternalPaxPayments(Integer pnrPaxId) {

		if (log.isDebugEnabled()) {
			log.debug("inside the getExternalPaxPayments for paxId " + pnrPaxId + "");
		}

		String inSection = BeanUtils.constructINString(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes());
		String hql = "select extPaxTnx from ReservationPaxExtTnx extPaxTnx where extPaxTnx.pnrPaxId = ? and extPaxTnx.nominalCode in ("
				+ inSection + ")";
		List<ReservationPaxExtTnx> externalTnx = find(hql, pnrPaxId, ReservationPaxExtTnx.class);

		if (log.isDebugEnabled()) {
			log.debug("exit the getExternalPaxPayments for paxId " + pnrPaxId + "");
		}

		return externalTnx;
	}

	@Override
	public BigDecimal getExternalReferenceGOQUOPayments(String pnr) {

		if (log.isDebugEnabled()) {
			log.debug("inside the getAeroMartPayPaxPayments for pnr " + pnr + "");
		}

		String totalPaxAmountHQL = " select sum(pt.amount) from TempPaymentTnx tpt, ReservationPax pp, ReservationTnx pt "
				+ "where  tpt.pnr = pt.externalReference and pt.pnrPaxId = pp.pnrPaxId and pp.reservation.pnr = ? "
				+ " and tpt.productType = ? and tpt.status = ?";

		BigDecimal totalCCAmount = (BigDecimal) getSession().createQuery(totalPaxAmountHQL).setParameter(0, pnr)
				.setParameter(1, PromotionCriteriaConstants.ProductType.GOQUO)
				.setParameter(2, ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS).list().get(0);

		BigDecimal totalAgentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (totalCCAmount == null || totalCCAmount.compareTo(BigDecimal.ZERO) == 0) {
			totalAgentAmount = getPaxGoQuoOnAccountPayAmount(pnr);
		}

		BigDecimal totalBSPAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		totalBSPAmount = getPaxBSPAmount(pnr);

		BigDecimal totalOnAccAmount = getPaxOnAccAmount(pnr);

		BigDecimal totalPaxAmount = AccelAeroCalculator.add(((totalCCAmount == null) ? new BigDecimal(0) : totalCCAmount.abs()),
				totalAgentAmount.abs(), totalBSPAmount.abs());

		String totalTnxAmountHQL = " SELECT sum(tpt.amount) FROM TempPaymentTnx tpt WHERE tpt.pnr in ( "
				+ " SELECT DISTINCT(pt.externalReference) FROM ReservationTnx pt, TempPaymentTnx tpt1, ReservationPax pp "
				+ " WHERE tpt1.pnr = pt.externalReference AND pp.reservation.pnr = ? AND tpt1.productType = ? AND tpt.status = ? AND pt.pnrPaxId    = pp.pnrPaxId) ";

		BigDecimal totalTnxAmount = (BigDecimal) getSession().createQuery(totalTnxAmountHQL).setParameter(0, pnr)
				.setParameter(1, PromotionCriteriaConstants.ProductType.GOQUO)
				.setParameter(2, ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS).list().get(0);

		BigDecimal goquoAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		BigDecimal subTotalTnxAmount = AccelAeroCalculator
				.add(((totalTnxAmount == null) ? new BigDecimal(0) : totalTnxAmount.abs()), totalOnAccAmount.abs());

		if (totalPaxAmount != null && subTotalTnxAmount != null) {
			goquoAmount = AccelAeroCalculator.subtract(subTotalTnxAmount.abs(), totalPaxAmount.abs());
		}

		return goquoAmount;
	}

	@SuppressWarnings("unchecked")
	private BigDecimal getPaxGoQuoOnAccountPayAmount(String pnr) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql;
		sql = " SELECT sum(at.amount) as PAX_OA_TOTAL from T_AGENT_TRANSACTION at where at.pnr = '" + pnr
				+ "' AND at.product_type = '" + PromotionCriteriaConstants.ProductType.PNR_RESERVATION
				+ "' and at.pnr not in (select tpt.pnr from t_temp_payment_tnx tpt where tpt.pnr='" + pnr + "') ";

		BigDecimal paxOnAccAmount = (BigDecimal) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal paxOnAccAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (rs != null) {
					if (rs.next()) {
						ExternalPaymentTnx tnx = new ExternalPaymentTnx();
						paxOnAccAmount = rs.getBigDecimal("PAX_OA_TOTAL") != null
								? rs.getBigDecimal("PAX_OA_TOTAL")
								: paxOnAccAmount;

					}
				}
				return paxOnAccAmount;
			}
		});

		return paxOnAccAmount;
	}

	@SuppressWarnings("unchecked")
	private BigDecimal getPaxOnAccAmount(String pnr) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql;
		sql = " SELECT sum(at.amount) as PAX_OA_TOTAL from T_AGENT_TRANSACTION at where at.pnr = '" + pnr
				+ "' AND at.product_type = '" + PromotionCriteriaConstants.ProductType.GOQUO
				+ "' and at.pnr not in (select tpt.pnr from t_temp_payment_tnx tpt where tpt.pnr='" + pnr + "') ";

		BigDecimal paxOnAccAmount = (BigDecimal) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal paxOnAccAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (rs != null) {
					if (rs.next()) {
						ExternalPaymentTnx tnx = new ExternalPaymentTnx();
						paxOnAccAmount = rs.getBigDecimal("PAX_OA_TOTAL") != null
								? rs.getBigDecimal("PAX_OA_TOTAL")
								: paxOnAccAmount;

					}
				}
				return paxOnAccAmount;
			}
		});

		return paxOnAccAmount;
	}

	@SuppressWarnings("unchecked")
	private BigDecimal getPaxBSPAmount(String pnr) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql;
		sql = " SELECT sum(pt.amount) as BSP_TOTAL FROM t_pax_transaction pt, " + " t_pnr_passenger pax "
				+ " WHERE pt.pnr_pax_id     = pax.pnr_pax_id " + " AND pt.nominal_code      IN (36,37) "
				+ " AND pt.lcc_unique_txn_id IS NULL " + " AND pax.PNR='" + pnr + "'";

		BigDecimal paxBSPTotal = (BigDecimal) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal paxBSPTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (rs != null) {
					if (rs.next()) {
						ExternalPaymentTnx tnx = new ExternalPaymentTnx();
						paxBSPTotal = rs.getBigDecimal("BSP_TOTAL") != null ? rs.getBigDecimal("BSP_TOTAL") : paxBSPTotal;

					}
				}
				return paxBSPTotal;
			}
		});

		return paxBSPTotal;
	}

	/**
	 * Return external payment transactions for Request UID.
	 * 
	 * @param reqUID
	 * @return Map<PNR, PNRExtTransactionsTO>
	 */
	@SuppressWarnings("unchecked")
	public Map<String, PNRExtTransactionsTO> getExtPayTransactionsForReqUID(String reqUID) {
		log.debug("Inside getExtPayTransactionsForReqUID");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql;
		sql = " SELECT E.EPT_ID,E.BAL_QUERY_KEY,E.AMOUNT,E.STATUS,E.USER_ID,E.PNR,E.INTERNAL_START_TIME,E.INTERNAL_PAY_ID,E.CHANNEL,E.EXTERNAL_REQ_ID "
				+ " from T_EXTERNAL_PAYMENT_TX E where E.EXTERNAL_REQ_ID = '" + reqUID + "' ";

		Map<String, PNRExtTransactionsTO> transactionsMap = (Map<String, PNRExtTransactionsTO>) jt.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, PNRExtTransactionsTO> transactionsMap = new HashMap<String, PNRExtTransactionsTO>();
						if (rs != null) {
							while (rs.next()) {
								ExternalPaymentTnx tnx = new ExternalPaymentTnx();
								tnx.setExtPayTxnId(new Integer(rs.getInt("EPT_ID")));
								tnx.setInternalPayId(new Integer(rs.getInt("INTERNAL_PAY_ID")));
								tnx.setBalanceQueryKey(rs.getString("BAL_QUERY_KEY"));
								tnx.setAmount(rs.getBigDecimal("AMOUNT"));
								tnx.setStatus(rs.getString("STATUS"));
								tnx.setInternalTnxStartTimestamp(rs.getTimestamp("INTERNAL_START_TIME"));
								tnx.setPnr(rs.getString("PNR"));
								tnx.setUserId(rs.getString("USER_ID"));
								tnx.setChannel(rs.getString("CHANNEL"));
								tnx.setExternalReqID(rs.getString("EXTERNAL_REQ_ID"));

								if (transactionsMap.containsKey(tnx.getPnr())) {
									PNRExtTransactionsTO pnrTx = (PNRExtTransactionsTO) transactionsMap.get(tnx.getPnr());
									pnrTx.addExtPayTransactions(tnx);
								} else {
									PNRExtTransactionsTO pnrTx = new PNRExtTransactionsTO();
									pnrTx.setPnr(tnx.getPnr());
									pnrTx.addExtPayTransactions(tnx);
									transactionsMap.put(tnx.getPnr(), pnrTx);
								}

							}
						}
						return transactionsMap;
					}
				});

		return transactionsMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>
			getReservationPaxSegmentPaymentsForOwnReservation(Collection<Integer> colPaymentTnxIds,
					Collection<Integer> requestingNominalCodes) {

		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT fltseg.segment_code AS segment_code,");
		sb.append(" fltseg.est_time_departure_zulu AS est_time_departure_zulu,");
		sb.append(" paxondcharge.charge_group_code AS charge_group_code,");
		sb.append(" SUM(paxsegpayment.amount) AS amount,");
		sb.append(" paxondpayment.nominal_code AS nominal_code,");
		sb.append(" paxtnx.txn_id AS txn_id,");
		sb.append(" pax.pax_sequence AS pax_sequence");
		sb.append(" FROM t_pax_transaction paxtnx,");
		sb.append(" t_pnr_pax_ond_payments paxondpayment,");
		sb.append(" t_pnr_pax_seg_payments paxsegpayment,");
		sb.append(" t_pnr_pax_ond_charges paxondcharge,");
		sb.append(" t_pnr_pax_seg_charges paxsegcharge,");
		sb.append(" t_pnr_passenger pax,");
		sb.append(" t_pnr_pax_fare_segment paxfareseg,");
		sb.append(" t_pnr_segment pnrseg,");
		sb.append(" t_flight_segment fltseg");
		sb.append(" WHERE paxtnx.txn_id= paxondpayment.payment_txn_id");
		sb.append(" AND paxondpayment.pnr_pax_id = pax.pnr_pax_id");
		sb.append(" AND paxsegpayment.ppop_id= paxondpayment.ppop_id");
		sb.append(" AND paxondcharge.pft_id= paxondpayment.pft_id");
		sb.append(" AND paxsegpayment.pfst_id= paxsegcharge.pfst_id");
		sb.append(" AND paxsegcharge.ppfs_id = paxfareseg.ppfs_id");
		sb.append(" AND paxfareseg.pnr_seg_id= pnrseg.pnr_seg_id");
		sb.append(" AND pnrseg.flt_seg_id= fltseg.flt_seg_id");
		sb.append(" AND paxsegcharge.pft_id= paxondcharge.pft_id");
		sb.append(" AND " + Util.getReplaceStringForIN("paxondpayment.nominal_code", requestingNominalCodes));
		sb.append(" and paxtnx.SEG_MAPPED = 'N'");
		sb.append(" AND " + Util.getReplaceStringForIN("paxtnx.txn_id", colPaymentTnxIds));
		sb.append(" GROUP BY fltseg.segment_code,");
		sb.append(" fltseg.est_time_departure_zulu,");
		sb.append(" paxondcharge.charge_group_code,");
		sb.append(" paxondpayment.nominal_code,");
		sb.append(" paxtnx.txn_id,");
		sb.append(" pax.pax_sequence ");
		sb.append(" UNION ");
		sb.append(" SELECT fltseg.segment_code AS segment_code,");
		sb.append(" fltseg.est_time_departure_zulu AS est_time_departure_zulu,");
		sb.append(" paxondcharge.charge_group_code AS charge_group_code,");
		sb.append(" SUM(paxsegpayment.amount) AS amount,");
		sb.append(" paxondpayment.nominal_code AS nominal_code,");
		sb.append(" paxtnx.txn_id AS txn_id,");
		sb.append(" pax.pax_sequence AS pax_sequence");
		sb.append(" FROM t_pax_transaction paxtnx,");
		sb.append(" t_pnr_pax_ond_payments paxondpayment,");
		sb.append(" t_pnr_pax_seg_payments paxsegpayment,");
		sb.append(" t_pnr_pax_ond_charges paxondcharge,");
		sb.append(" t_pnr_pax_seg_charges paxsegcharge,");
		sb.append(" t_pnr_passenger pax,");
		sb.append(" t_pnr_pax_fare_segment paxfareseg,");
		sb.append(" T_PAX_TRNX_SEGMENT pts,");
		sb.append(" t_flight_segment fltseg");
		sb.append(" WHERE paxtnx.txn_id= paxondpayment.payment_txn_id");
		sb.append(" AND paxondpayment.pnr_pax_id = pax.pnr_pax_id");
		sb.append(" AND paxsegpayment.ppop_id= paxondpayment.ppop_id");
		sb.append(" AND paxondcharge.pft_id= paxondpayment.pft_id");
		sb.append(" AND paxsegpayment.pfst_id= paxsegcharge.pfst_id");
		sb.append(" AND paxsegcharge.ppfs_id = paxfareseg.ppfs_id");
		sb.append(" AND paxfareseg.pnr_seg_id= pts.pnr_seg_id");
		sb.append(" and pts.TXN_ID = paxtnx.TXN_ID");
		sb.append(" AND pts.flt_seg_id= fltseg.flt_seg_id");
		sb.append(" AND paxsegcharge.pft_id= paxondcharge.pft_id");
		sb.append(" AND " + Util.getReplaceStringForIN("paxondpayment.nominal_code", requestingNominalCodes));
		sb.append(" and paxtnx.SEG_MAPPED = 'Y'");
		sb.append(" AND " + Util.getReplaceStringForIN("paxtnx.txn_id", colPaymentTnxIds));
		sb.append(" GROUP BY fltseg.segment_code,");
		sb.append(" fltseg.est_time_departure_zulu,");
		sb.append(" paxondcharge.charge_group_code,");
		sb.append(" paxondpayment.nominal_code,");
		sb.append(" paxtnx.txn_id,");
		sb.append(" pax.pax_sequence ");

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" Method :getReservationPaxSegmentPaymentsForOwnReservation(..) SQL to excute : " + sb.toString());
			log.debug("############################################");
		}

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> reservationPaxSegmentPayments = (Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>) jt
				.query(sb.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> reservationPaxSegmentPaymentsMap = new HashMap<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>();

						if (rs != null) {
							while (rs.next()) {
								Pair<String, Integer> key = Pair.of(rs.getString("txn_id"), rs.getInt("pax_sequence"));

								ReservationPaxSegmentPaymentTO resPaxsegPayTO = new ReservationPaxSegmentPaymentTO();
								resPaxsegPayTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
								resPaxsegPayTO.setDepartureDateZulu(rs.getTimestamp("est_time_departure_zulu"));
								resPaxsegPayTO.setPaxSequence(rs.getInt("pax_sequence"));
								resPaxsegPayTO.setSegmentCode(rs.getString("segment_code"));
								resPaxsegPayTO.setTransactionId(rs.getString("txn_id"));

								SegmentChargePaymentTO segChargePayTo = new SegmentChargePaymentTO();
								segChargePayTo.setAmount(rs.getBigDecimal("amount"));
								segChargePayTo.setNominalCode(rs.getInt("nominal_code"));
								segChargePayTo.setChargeGroupCode(rs.getString("charge_group_code"));

								if (reservationPaxSegmentPaymentsMap.containsKey(key)) {
									Collection<ReservationPaxSegmentPaymentTO> resPaxSegPayTOCollection = reservationPaxSegmentPaymentsMap
											.get(key);
									if (resPaxSegPayTOCollection.contains(resPaxsegPayTO)) {
										for (ReservationPaxSegmentPaymentTO to : resPaxSegPayTOCollection) {
											if (to.equals(resPaxsegPayTO)) {
												to.getSegmentChargePaymentCollection().add(segChargePayTo);
												break;
											}
										}
									} else {
										resPaxsegPayTO.getSegmentChargePaymentCollection().add(segChargePayTo);
										resPaxSegPayTOCollection.add(resPaxsegPayTO);
									}
								} else {
									// we need to create a new collection to be put to the map along with the key.
									Collection<ReservationPaxSegmentPaymentTO> resPaxSegPayTOCollection = new ArrayList<ReservationPaxSegmentPaymentTO>();
									resPaxsegPayTO.getSegmentChargePaymentCollection().add(segChargePayTo);
									resPaxSegPayTOCollection.add(resPaxsegPayTO);
									reservationPaxSegmentPaymentsMap.put(key, resPaxSegPayTOCollection);
								}
							}
						}
						return reservationPaxSegmentPaymentsMap;
					}
				});

		return reservationPaxSegmentPayments;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>
			getReservationPaxSegmentPaymentsForGroupReservation(Collection<String> paxSeqLccUniqueTIDs,
					Collection<Integer> requestingNominalCodes) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT segpayrepo.segment_code AS segment_code, ");
		sb.append("  segpayrepo.departure_date_zulu AS departure_date_zulu, ");
		sb.append("  SUM(segpayrepo.amount * LCC_F_GET_EXCHANGE_RATE(ondpayrepo.carrier_code, '"
				+ AppSysParamsUtil.getDefaultCarrierCode() + "', ondpayrepo.timestamp)) AS amount, ");
		sb.append("  ondpayrepo.lcc_unique_tnx_id AS lcc_unique_tnx_id, ");
		sb.append("  ondpayrepo.pax_sequence AS pax_sequence, ");
		sb.append("  ondpayrepo.carrier_code AS carrier_code, ");
		sb.append("  ondpayrepo.charge_group_code AS charge_group_code, ");
		sb.append("  ondpayrepo.nominal_code  AS nominal_code ");
		sb.append(" FROM lcc_t_pnr_pax_ond_payment_repo ondpayrepo, ");
		sb.append("  lcc_t_pnr_pax_seg_payment_repo segpayrepo ");
		sb.append(" WHERE ondpayrepo.ppop_id = segpayrepo.ppop_id ");
		sb.append(" AND " + Util.getReplaceStringForIN("ondpayrepo.nominal_code", requestingNominalCodes));
		sb.append(" AND " + Util.getReplaceStringForIN("ondpayrepo.lcc_unique_tnx_id", paxSeqLccUniqueTIDs));
		sb.append(" GROUP BY segpayrepo.segment_code, ");
		sb.append("  segpayrepo.departure_date_zulu, ");
		sb.append("  ondpayrepo.lcc_unique_tnx_id, ");
		sb.append("  ondpayrepo.pax_sequence, ");
		sb.append("  ondpayrepo.charge_group_code, ");
		sb.append("  ondpayrepo.carrier_code, ");
		sb.append("  ondpayrepo.nominal_code");

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" Method :getReservationPaxSegmentPaymentsForGroupReservation(..) SQL to excute : " + sb.toString());
			log.debug("############################################");
		}

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getLCCDatasource());

		Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> reservationPaxSegmentPayments = (Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>) jt
				.query(sb.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> reservationPaxSegmentPaymentsMap = new HashMap<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>();

						if (rs != null) {
							while (rs.next()) {
								Pair<String, Integer> key = Pair.of(rs.getString("lcc_unique_tnx_id"), rs.getInt("pax_sequence"));

								ReservationPaxSegmentPaymentTO resPaxsegPayTO = new ReservationPaxSegmentPaymentTO();
								resPaxsegPayTO.setCarrierCode(rs.getString("carrier_code"));
								resPaxsegPayTO.setDepartureDateZulu(rs.getTimestamp("departure_date_zulu"));
								resPaxsegPayTO.setPaxSequence(rs.getInt("pax_sequence"));
								resPaxsegPayTO.setSegmentCode(rs.getString("segment_code"));
								resPaxsegPayTO.setTransactionId(rs.getString("lcc_unique_tnx_id"));

								SegmentChargePaymentTO segChargePayTo = new SegmentChargePaymentTO();
								segChargePayTo.setAmount(rs.getBigDecimal("amount"));
								segChargePayTo.setNominalCode(rs.getInt("nominal_code"));
								segChargePayTo.setChargeGroupCode(rs.getString("charge_group_code"));

								if (reservationPaxSegmentPaymentsMap.containsKey(key)) {
									Collection<ReservationPaxSegmentPaymentTO> resPaxSegPayTOCollection = reservationPaxSegmentPaymentsMap
											.get(key);
									if (resPaxSegPayTOCollection.contains(resPaxsegPayTO)) {
										for (ReservationPaxSegmentPaymentTO to : resPaxSegPayTOCollection) {
											if (to.equals(resPaxsegPayTO)) {
												to.getSegmentChargePaymentCollection().add(segChargePayTo);
												break;
											}
										}
									} else {
										resPaxsegPayTO.getSegmentChargePaymentCollection().add(segChargePayTo);
										resPaxSegPayTOCollection.add(resPaxsegPayTO);
									}
								} else {
									// we need to create a new collection to be put to the map along with the key.
									Collection<ReservationPaxSegmentPaymentTO> resPaxSegPayTOCollection = new ArrayList<ReservationPaxSegmentPaymentTO>();
									resPaxsegPayTO.getSegmentChargePaymentCollection().add(segChargePayTo);
									resPaxSegPayTOCollection.add(resPaxsegPayTO);
									reservationPaxSegmentPaymentsMap.put(key, resPaxSegPayTOCollection);
								}
							}
						}
						return reservationPaxSegmentPaymentsMap;
					}
				});

		return reservationPaxSegmentPayments;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Pair<String, Integer>, BigDecimal> getHandlingChargesForOwnResPaxTnxs(Collection<Integer> transactionIds,
			String handlingChargeCode) {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT paxtnx.txn_id AS txn_id, ");
		sb.append("  pax.pax_sequence           AS pax_sequence, ");
		sb.append("  SUM(paxondpayments.amount) AS handling_charge ");
		sb.append("FROM t_pnr_pax_ond_payments paxondpayments, ");
		sb.append("  t_pax_transaction paxtnx, ");
		sb.append("  t_pnr_pax_ond_charges paxondcharges, ");
		sb.append("  t_charge_rate crate, ");
		sb.append("  t_pnr_passenger pax ");
		sb.append("WHERE crate.charge_rate_id = paxondcharges.charge_rate_id ");
		sb.append("AND paxtnx.txn_id          = paxondpayments.payment_txn_id ");
		sb.append("AND paxondpayments.pft_id  = paxondcharges.pft_id ");
		sb.append("AND pax.pnr_pax_id         = paxtnx.pnr_pax_id ");
		sb.append("AND crate.charge_code      = '" + handlingChargeCode + "' ");
		sb.append("AND " + Util.getReplaceStringForIN("paxtnx.txn_id", transactionIds));
		sb.append("GROUP BY paxtnx.txn_id, pax.pax_sequence ");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" getHandlingChargesForOwnResPaxTnxs() : SQL to excute: " + sb.toString());
			log.debug("############################################");
		}

		Map<Pair<String, Integer>, BigDecimal> handlingChargesForOwnResPaxTnxs = (Map<Pair<String, Integer>, BigDecimal>) jt
				.query(sb.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map<Pair<String, Integer>, BigDecimal> handlingChargesForOwnResPaxTnxs = new HashMap<Pair<String, Integer>, BigDecimal>();
						if (rs != null) {
							while (rs.next()) {
								Pair<String, Integer> key = Pair.of(rs.getString("txn_id"), rs.getInt("pax_sequence"));
								handlingChargesForOwnResPaxTnxs.put(key, rs.getBigDecimal("handling_charge"));
							}
						}
						return handlingChargesForOwnResPaxTnxs;
					}
				});

		return handlingChargesForOwnResPaxTnxs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Pair<String, Integer>, BigDecimal> getHandlingChargesForGroupResPaxTnxs(Collection<String> transactionIds,
			String handlingChargeCode) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ondpayrepo.lcc_unique_tnx_id AS lcc_unique_tnx_id, ");
		sb.append("  ondpayrepo.pax_sequence AS pax_sequence,");
		sb.append("  SUM(ondpayrepo.amount * LCC_F_GET_EXCHANGE_RATE(ondpayrepo.carrier_code, '"
				+ AppSysParamsUtil.getDefaultCarrierCode() + "', ondpayrepo.timestamp)) AS handling_charge ");
		sb.append("FROM lcc_t_pnr_pax_ond_payment_repo ondpayrepo ");
		sb.append("WHERE ondpayrepo.charge_code      = '" + handlingChargeCode + "' ");
		sb.append("AND " + Util.getReplaceStringForIN("ondpayrepo.lcc_unique_tnx_id", transactionIds));
		sb.append("GROUP BY ondpayrepo.lcc_unique_tnx_id, ");
		sb.append("  ondpayrepo.pax_sequence");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getLCCDatasource());

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" getHandlingChargesForGroupResPaxTnxs() : SQL to excute: " + sb.toString());
			log.debug("############################################");
		}

		Map<Pair<String, Integer>, BigDecimal> handlingChargesForGroupResPaxTnxs = (Map<Pair<String, Integer>, BigDecimal>) jt
				.query(sb.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map<Pair<String, Integer>, BigDecimal> handlingCharges = new HashMap<Pair<String, Integer>, BigDecimal>();

						if (rs != null) {
							while (rs.next()) {
								Pair<String, Integer> key = Pair.of(rs.getString("lcc_unique_tnx_id"), rs.getInt("pax_sequence"));
								handlingCharges.put(key, rs.getBigDecimal("handling_charge"));
							}
						}
						return handlingCharges;
					}
				});

		return handlingChargesForGroupResPaxTnxs;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ReservationPaxOndPayment> getRefundablePayment(String pnrPaxId, String chargeType, Collection<String> ppfIDs,
			String chargeCode) {

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM T_PNR_PAX_OND_PAYMENTS PPOP WHERE PPOP.pnr_pax_id = ? AND PPOP.CHARGE_GROUP_CODE = ? ");

		sb.append(" AND PPOP.pft_id IN (SELECT Z.pft_id FROM t_charge_rate X, t_charge Y, t_pnr_pax_ond_charges Z ");
		sb.append(
				" WHERE X.charge_code = Y.charge_code AND Z.CHARGE_RATE_ID = X.CHARGE_RATE_ID AND Y.REFUNDABLE_CHARGE_FLAG = 1 ");

		if (chargeCode != null) {
			sb.append(" AND X.CHARGE_CODE = ? ");
		}
		sb.append(" AND Z.PPF_ID IN(");
		for (String str : ppfIDs) {
			sb.append("'");
			sb.append(str);
			sb.append("',");
		}
		sb.append(" ''))");

		Object[] queryParams = (chargeCode == null
				? new Object[] { pnrPaxId, chargeType }
				: new Object[] { pnrPaxId, chargeType, chargeCode });

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (List<ReservationPaxOndPayment>) jt.query(sb.toString(), queryParams, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<ReservationPaxOndPayment> refundableCharges = new ArrayList<ReservationPaxOndPayment>();

				while (rs.next()) {
					ReservationPaxOndPayment payment = new ReservationPaxOndPayment();
					payment.setPaxOndPaymentId(rs.getLong("PPOP_ID"));
					payment.setAmount(rs.getBigDecimal("AMOUNT"));
					payment.setPaymentTnxId(rs.getLong("PAYMENT_TXN_ID"));
					payment.setPnrPaxOndChgId(rs.getLong("PFT_ID"));
					refundableCharges.add(payment);
				}

				return refundableCharges;
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PNRGOVPaymentDTO> getPaxPaymentDTOs(String pnrPaxID) {
		StringBuffer sql = new StringBuffer();
		String inSection = BeanUtils.constructINString(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes());

		sql.append("select pnr_pax_id,");
		sql.append("sum(amount) as payment_amount,");
		sql.append("nominalCode AS nominal_code, ");
		sql.append("dr_cr AS dr_cr,");
		sql.append("tnx_date AS tnx_date ");
		sql.append(" FROM ");
		sql.append("(SELECT pnr_pax_id,");
		sql.append("amount         AS amount,");
		sql.append("nominal_code as nominalCode, ");
		sql.append("dr_cr AS dr_cr,");
		sql.append("to_date(TO_CHAR(tnx_date, 'dd-mon-yyyy')) AS tnx_date ");
		sql.append("FROM t_pax_transaction ");
		sql.append("WHERE pnr_pax_id      =" + pnrPaxID);
		sql.append(" AND nominal_code IN (" + inSection + ")");
		sql.append(" UNION ");
		sql.append(" SELECT pnr_pax_id,");
		sql.append(" amount AS amount, ");
		sql.append("nominal_code AS nominalCode,");
		sql.append("dr_cr AS dr_cr, ");
		sql.append("to_date(TO_CHAR(txn_timestamp, 'dd-mon-yyyy')) AS tnx_date ");
		sql.append(" FROM t_pax_ext_carrier_transactions ");
		sql.append("WHERE pnr_pax_id      =" + pnrPaxID);
		sql.append(" AND nominal_code IN (" + inSection + "))");
		sql.append(" GROUP BY pnr_pax_id,");
		sql.append("nominalCode, dr_cr, tnx_date");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Object[] param = {};

		return (List<PNRGOVPaymentDTO>) jt.query(sql.toString(), param, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PNRGOVPaymentDTO> passengerPayments = new ArrayList<PNRGOVPaymentDTO>();
				if (rs != null) {
					while (rs.next()) {
						PNRGOVPaymentDTO payment = new PNRGOVPaymentDTO();
						payment.setPaymentAmount(rs.getBigDecimal("payment_amount"));
						payment.setNominalCode(rs.getInt("nominal_code"));
						payment.setTnxType(rs.getString("dr_cr"));
						payment.setTnxDate(rs.getDate("tnx_date"));
						passengerPayments.add(payment);
					}
				}
				return passengerPayments;
			}

		});

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Long> getPaxCreditTransactionIds(Collection<Long> colPaymentTnxIds) {
		StringBuffer sql = new StringBuffer();

		if (colPaymentTnxIds != null && colPaymentTnxIds.size() > 0) {
			Collection<Integer> paymentTnxIds = new ArrayList<Integer>();
			for (Long paymentTnxId : colPaymentTnxIds) {
				if (paymentTnxId != null) {
					paymentTnxIds.add(paymentTnxId.intValue());
				}
			}

			Collection<Integer> nominalCodes = ReservationTnxNominalCode.getNonRefundableTypePaymentCodes();
			String strTnxInclause = Util.buildIntegerInClauseContent(paymentTnxIds);

			sql.append("SELECT PC.TXN_ID FROM T_PAX_CREDIT PC,T_PAX_TRANSACTION TNX ");
			sql.append("WHERE BALANCE > 0 AND TNX.TXN_ID=PC.TXN_ID ");
			if (nominalCodes != null && !nominalCodes.isEmpty()) {
				sql.append("AND TNX.NOMINAL_CODE NOT IN (" + Util.buildIntegerInClauseContent(nominalCodes) + ") ");
			}
			sql.append("AND PC.TXN_ID NOT IN (" + strTnxInclause + ") AND PC.PNR_PAX_ID IN ");
			sql.append("(SELECT PNR_PAX_ID FROM T_PAX_CREDIT WHERE TXN_ID IN (" + strTnxInclause + ") ) ");

			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			Object[] param = {};

			return (Collection<Long>) jt.query(sql.toString(), param, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Collection<Long> paxTnxIds = new ArrayList<Long>();
					if (rs != null) {
						while (rs.next()) {
							paxTnxIds.add(rs.getLong("TXN_ID"));

						}
					}
					return paxTnxIds;
				}

			});
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isPaymentReferenceExist(String paymentReference) {
		StringBuffer sql = new StringBuffer();

		sql.append("select EXT_REFERENCE from T_PAX_TRANSACTION WHERE EXT_REFERENCE='");
		sql.append(paymentReference);
		sql.append("'");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Object[] param = {};

		return (boolean) jt.query(sql.toString(), param, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				boolean isPaymentReferenceExist = false;
				if (rs != null) {
					while (rs.next()) {
						isPaymentReferenceExist = true;
					}
				}
				return isPaymentReferenceExist;
			}

		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getAgentPayments(String paymentReference) {
		StringBuffer sql = new StringBuffer();

		sql.append("select EXT_REFERENCE,AMOUNT,PNR from  T_AGENT_TRANSACTION where EXT_REFERENCE='");
		sql.append(paymentReference);
		sql.append("'");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Object[] param = {};

		return (Map<String, String>) jt.query(sql.toString(), param, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, String> paymentMap = new HashMap<String, String>();
				if (rs != null) {
					while (rs.next()) {
						paymentMap.put(rs.getString("EXT_REFERENCE"), rs.getString("AMOUNT"));
					}
				}
				return paymentMap;
			}

		});
	}

	@Override
	public Long getCreditReInstatedTnxIds(Long paymentTnxId) {
		StringBuffer sql = new StringBuffer();

		if (paymentTnxId != null) {
			sql.append("SELECT REINSTATED_TNX_ID AS TXN_ID FROM T_PAX_CREDIT WHERE BALANCE > 0 ");
			sql.append("AND TXN_ID= '" + paymentTnxId + "' AND REINSTATED_TNX_ID  IS NOT NULL");

			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			Object[] param = {};

			return (Long) jt.query(sql.toString(), param, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Long paxTnxId = null;
					if (rs != null) {
						while (rs.next()) {
							paxTnxId = rs.getLong("TXN_ID");
						}
					}
					return paxTnxId;
				}

			});
		}
		return null;

	}

	public Collection<ReservationPaxOndPayment> getCreditExpiredPaxOndPayments(Long paymentTnxId) {

		Collection<ReservationPaxOndPayment> ondPaymentsColl = new ArrayList<ReservationPaxOndPayment>();
		if (paymentTnxId != null && paymentTnxId.intValue() > 0) {

			ondPaymentsColl = find("FROM ReservationPaxOndPayment WHERE nominalCode="
					+ ReservationTnxNominalCode.CREDIT_ACQUIRE.getCode() + " AND paxOndRefundId = " + paymentTnxId,
					ReservationPaxOndPayment.class);
		}

		return ondPaymentsColl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String> getChargeCodesForPaxOndPayments(Collection<Integer> ppocIds) {
		StringBuffer sql = new StringBuffer();

		if (ppocIds != null && ppocIds.size() > 0) {

			String strPPOCInclause = Util.buildIntegerInClauseContent(ppocIds);

			sql.append(" SELECT PPOC.PFT_ID, CR.CHARGE_CODE FROM T_PNR_PAX_OND_CHARGES PPOC, T_CHARGE_RATE CR ");
			sql.append(" WHERE PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
			sql.append(" AND PPOC.PFT_ID IN (" + strPPOCInclause + ") ");

			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			Object[] param = {};

			return (Map<Long, String>) jt.query(sql.toString(), param, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Map<Long, String> ppocChargeCodeMap = new HashMap<Long, String>();
					if (rs != null) {
						while (rs.next()) {
							ppocChargeCodeMap.put(rs.getLong("PFT_ID"), rs.getString("CHARGE_CODE"));
						}
					}
					return ppocChargeCodeMap;
				}

			});
		}
		return null;
	}

	public Integer getPNRPaxMaxPaymentTnxSequence(Integer pnrPaxId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "SELECT MAX(TRANSACTION_SEQ) AS SEQ FROM T_PNR_PAX_OND_PAYMENTS PPOP WHERE PPOP.PNR_PAX_ID = ? ";

		Integer maxSeq = (Integer) jt.query(sql, new Object[] { pnrPaxId }, new SequenceExtractor());

		return maxSeq;
	}

	public Integer getPNRMaxPaymentTnxSequence(String pnr) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "select max(ppop.transaction_seq) AS SEQ from T_PNR_PAX_OND_PAYMENTS ppop, t_pnr_passenger pp "
				+ " where pp.pnr = ? and pp.pnr_pax_id = ppop.pnr_pax_id;";

		Integer maxSeq = (Integer) jt.query(sql, new Object[] { pnr }, new SequenceExtractor());

		return maxSeq;
	}

	public Integer getPNRMaxChargeTnxSequence(String pnr) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "select max(pft.transaction_seq) AS SEQ from T_PNR_PAX_OND_CHARGES pft, t_pnr_pax_fare ppf, t_pnr_passenger pp "
				+ " where pp.pnr = ? and pp.pnr_pax_id = ppf.pnr_pax_id and ppf.ppf_id = pft.ppf_id ";

		Integer maxSeq = (Integer) jt.query(sql, new Object[] { pnr }, new SequenceExtractor());

		return maxSeq;
	}

	public Integer getPNRPaxMaxChargeTnxSequence(Integer pnrPaxId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "select max(pft.transaction_seq) AS SEQ from T_PNR_PAX_OND_CHARGES pft, t_pnr_pax_fare ppf"
				+ " where ppf.pnr_pax_id = ? and ppf.ppf_id = pft.ppf_id ";

		Integer maxSeq = (Integer) jt.query(sql, new Object[] { pnrPaxId }, new SequenceExtractor());

		return maxSeq;
	}
}