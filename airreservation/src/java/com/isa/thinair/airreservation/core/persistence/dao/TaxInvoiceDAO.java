package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airreservation.api.model.TaxInvoice;

public interface TaxInvoiceDAO {

	public TaxInvoice saveTaxInvoice(TaxInvoice taxInvoice);

	public List<TaxInvoice> getTaxInvoiceFor(String pnr);
	
	public List<TaxInvoice> getTaxInvoiceForPrintInvoice(String pnr);

	public TaxInvoice getTaxInvoiceFor(Integer taxInvoiceId);
	
	public List<TaxInvoice> getTaxInvoiceFor(List<String> invoiceNumbers);

	public Integer getTaxInvoiceNumber(String stateCode, String invoiceType);
}
