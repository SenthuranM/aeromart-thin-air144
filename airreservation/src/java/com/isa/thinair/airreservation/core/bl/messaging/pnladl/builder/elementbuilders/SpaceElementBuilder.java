/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;

/**
 * @author udithad
 *
 */
public class SpaceElementBuilder extends BaseElementBuilder {


	private String spaceElement;
	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;
	
	public SpaceElementBuilder() {
		spaceElement = space();
	}
	
	private void initContextData(ElementContext context) {
		currentLine = context.getCurrentMessageLine();
		messageLine = context.getMessageString();
		currentElement = spaceElement;
	}

	@Override
	public void buildElement(ElementContext context) {
		initContextData(context);
		ammendToBaseLine(currentElement, currentLine, messageLine);
	}
	

}
