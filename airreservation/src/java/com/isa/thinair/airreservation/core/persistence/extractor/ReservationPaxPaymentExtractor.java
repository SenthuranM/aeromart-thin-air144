package com.isa.thinair.airreservation.core.persistence.extractor;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;

public class ReservationPaxPaymentExtractor implements ResultSetExtractor {

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> mapOndPayments = new HashMap<Integer, Map<Long, Collection<ReservationPaxOndPayment>>>();

		Long paxOndPaymentId;
		String pnrPaxId;
		Long pnrPaxOndChgId;
		Long paymentTnxId;
		BigDecimal amount;
		Integer nominalCode;
		String chargeGroupCode;
		Long originalPaxOndPaymentId;
		Integer paxOndRefundId;
		String chargeCode;
		Integer transactionSeq;

		if (rs != null) {
			while (rs.next()) {
				paxOndPaymentId = rs.getLong("PPOP_ID");
				pnrPaxId = rs.getString("PNR_PAX_ID");
				pnrPaxOndChgId = rs.getLong("PFT_ID");
				paymentTnxId = rs.getLong("PAYMENT_TXN_ID");
				amount = rs.getBigDecimal("AMOUNT");
				nominalCode = rs.getInt("NOMINAL_CODE");
				chargeGroupCode = rs.getString("CHARGE_GROUP_CODE");
				originalPaxOndPaymentId = rs.getLong("ORIGINAL_PPOP_ID");
				paxOndRefundId = rs.getInt("REFUND_TXN_ID");
				chargeCode = rs.getString("CHARGE_CODE");
				transactionSeq = rs.getInt("TRANSACTION_SEQ");

				ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
				reservationPaxOndPayment.setPaxOndPaymentId(paxOndPaymentId);
				reservationPaxOndPayment.setPnrPaxId(pnrPaxId);
				reservationPaxOndPayment.setPnrPaxOndChgId(pnrPaxOndChgId);
				reservationPaxOndPayment.setPaymentTnxId(paymentTnxId);
				reservationPaxOndPayment.setAmount(amount);
				reservationPaxOndPayment.setNominalCode(nominalCode);
				reservationPaxOndPayment.setChargeGroupCode(chargeGroupCode);
				reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
				reservationPaxOndPayment.setPaxOndRefundId(paxOndRefundId);
				reservationPaxOndPayment.setChargeCode(chargeCode);
				reservationPaxOndPayment.setTransactionSeq(transactionSeq);

				Map<Long, Collection<ReservationPaxOndPayment>> mapOndChargeIdWiseAmounts = mapOndPayments.get(new Integer(
						reservationPaxOndPayment.getPnrPaxId()));

				if (mapOndChargeIdWiseAmounts == null) {
					mapOndChargeIdWiseAmounts = new HashMap<Long, Collection<ReservationPaxOndPayment>>();

					Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();
					colReservationPaxOndPayment.add(reservationPaxOndPayment);

					mapOndChargeIdWiseAmounts.put(reservationPaxOndPayment.getPnrPaxOndChgId(), colReservationPaxOndPayment);

					mapOndPayments.put(new Integer(reservationPaxOndPayment.getPnrPaxId()), mapOndChargeIdWiseAmounts);
				} else {
					Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = mapOndChargeIdWiseAmounts
							.get(reservationPaxOndPayment.getPnrPaxOndChgId());

					if (colReservationPaxOndPayment == null) {
						colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();
					}

					colReservationPaxOndPayment.add(reservationPaxOndPayment);
					mapOndChargeIdWiseAmounts.put(reservationPaxOndPayment.getPnrPaxOndChgId(), colReservationPaxOndPayment);
				}

			}
		}
		return mapOndPayments;
	}

}
