/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ExternalSegmentUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for changing external segments
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="changeExternalSegment"
 */
public class ChangeExternalSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ChangeExternalSegment.class);

	/** Construct the ChangeExternalSegment object */
	public ChangeExternalSegment() {
	}

	/**
	 * Execute method of the ChangeExternalSegment command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		Collection<ExternalSegmentTO> colExternalSegmentTO = (Collection<ExternalSegmentTO>) this
				.getParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		// Checking params
		this.validateParams(pnr, colExternalSegmentTO, version, credentialsDTO);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);

		// TODO make the reservation external also
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		if (colExternalSegmentTO != null && colExternalSegmentTO.size() > 0) {
			// save the external reservation segments
			saveAndInjectExtSegs(colExternalSegmentTO);

			Map<String, Collection<ExternalPnrSegment>> serverSegmentMap = ExternalSegmentUtil
					.getConfirmedReservationExternalSegmentInAMap(reservation.getExternalReservationSegment());
			
			Map<String, Collection<ExternalPnrSegment>> serverAllSegmentMap = ExternalSegmentUtil
					.getAllReservationExternalSegmentInAMap(reservation.getExternalReservationSegment());

			String addedSegmentInfo = BeanUtils.nullHandler(ExternalSegmentUtil.addExternalSegments(
					colExternalSegmentTO, reservation, serverAllSegmentMap));
			String modifySegmentInfo = BeanUtils.nullHandler(ExternalSegmentUtil.modifyExternalSegments(colExternalSegmentTO,
					serverSegmentMap));

			if (addedSegmentInfo.length() > 0 || modifySegmentInfo.length() > 0) {
				ReservationProxy.saveReservation(reservation);
				ReservationAudit reservationAudit = this.composeAudit(pnr, addedSegmentInfo, modifySegmentInfo, credentialsDTO);
				colReservationAudit.add(reservationAudit);
			}
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);

		log.debug("Exit execute");
		return response;
	}

	private void saveAndInjectExtSegs(Collection<ExternalSegmentTO> colExternalSegmentTO) {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		for (ExternalSegmentTO segTO : colExternalSegmentTO) {
			Integer extFlightSegRef = null;
			ExternalFlightSegment externalFlightSegment = null;

			if (segTO.getFlightRefNumber() != null) {
				extFlightSegRef = FlightRefNumberUtil.getSegmentIdFromFlightRPH(segTO.getFlightRefNumber());
			}

			if (extFlightSegRef != null) {
				externalFlightSegment = reservationSegmentDAO.getExternalFlightSegment(segTO.getOperatingAirline(),
						extFlightSegRef);
			}

			if (externalFlightSegment == null) {
				externalFlightSegment = new ExternalFlightSegment();
			}

			externalFlightSegment.setExternalFlightSegRef(extFlightSegRef);
			externalFlightSegment.setFlightNumber(segTO.getFlightNo());
			externalFlightSegment.setSegmentCode(segTO.getSegmentCode());
			externalFlightSegment.setExternalCarrierCode(segTO.getOperatingAirline());
			externalFlightSegment.setStatus(ReservationInternalConstants.ReservationExternalFlightStatus.OPEN);
			externalFlightSegment.setEstTimeArrivalLocal(segTO.getArrivalDate());
			externalFlightSegment.setEstTimeArrivalZulu(segTO.getArrivalDate());
			externalFlightSegment.setEstTimeDepatureLocal(segTO.getDepartureDate());
			externalFlightSegment.setEstTimeDepatureZulu(segTO.getDepartureDate());
			// save or update external flight segment
			reservationSegmentDAO.saveExternalFlightSegment(externalFlightSegment);

			// Inject the flight segment Id from the saved object
			segTO.setExternalFlightSegId(externalFlightSegment.getExternalFlightSegId());
		}

	}

	/**
	 * Modify External Segments
	 * 
	 * @param clientSegmentMap
	 * @param serverSegmentMap
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
//	private String modifyExternalSegments(Collection<ExternalSegmentTO> colExternalSegmentTO,
//			Map<String, Collection<ExternalPnrSegment>> serverSegmentMap, Reservation reservation) throws ModuleException {
//
//		StringBuilder modifySegInfo = new StringBuilder();
//		for (ExternalSegmentTO externalSegmentTO : colExternalSegmentTO) {
//
//			if (ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL.equals(externalSegmentTO.getStatus())) {
//				String uniqueSegmentKeyWithOutStatus = ReservationApiUtils.getUniqueSegmentKey(externalSegmentTO.getFlightNo(),
//						externalSegmentTO.getSegmentCode(), externalSegmentTO.getDepartureDate(),
//						externalSegmentTO.getCabinClassCode(), null);
//
//				if (serverSegmentMap.containsKey(uniqueSegmentKeyWithOutStatus)) {
//					Collection<ExternalPnrSegment> colReservationExternalSegment = serverSegmentMap
//							.get(uniqueSegmentKeyWithOutStatus);
//					ExternalPnrSegment reservationExternalSegment = getConfirmedReservationExternalSegment(colReservationExternalSegment);
//
//					if (modifySegInfo.length() == 0) {
//						modifySegInfo.append(reservationExternalSegment.toString() + " --> " + externalSegmentTO.toString());
//					} else {
//						modifySegInfo.append(" , " + reservationExternalSegment.toString() + " --> "
//								+ externalSegmentTO.toString());
//					}
//
//					reservationExternalSegment.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
//				}
//			}
//
//		}
//
//		return modifySegInfo.toString();
//	}

	/**
	 * Returns the confirmed reservation external segment
	 * 
	 * @param colReservationExternalSegment
	 * @return
	 * @throws ModuleException
	 */
//	private ExternalPnrSegment
//			getConfirmedReservationExternalSegment(Collection<ExternalPnrSegment> colReservationExternalSegment)
//					throws ModuleException {
//
//		if (colReservationExternalSegment != null && colReservationExternalSegment.size() > 0) {
//			for (ExternalPnrSegment reservationExternalSegment : colReservationExternalSegment) {
//				if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(reservationExternalSegment
//						.getStatus())) {
//					return reservationExternalSegment;
//				}
//			}
//		}
//
//		throw new ModuleException("airreservations.arg.corruptedExternalSegmentsForAmmending");
//	}

	/**
	 * Add External Segments
	 * 
	 * @param colExternalSegmentTO
	 * @param reservation
	 * @return
	 */
//	private String addExternalSegments(Collection<ExternalSegmentTO> colExternalSegmentTO, Reservation reservation) {
//
//		Collection<ExternalPnrSegment> colReservationExternalSegment = new ArrayList<ExternalPnrSegment>();
//		int nextSegmentSeq = getNextSegmentSeq(reservation.getExternalReservationSegment());
//
//		for (ExternalSegmentTO externalSegmentTO : colExternalSegmentTO) {
//			if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(externalSegmentTO.getStatus())) {
//				colReservationExternalSegment.add(externalSegmentTO.toExternalPnrSegment(nextSegmentSeq));
//				nextSegmentSeq++;
//			}
//		}
//
//		StringBuilder info = new StringBuilder();
//		if (colReservationExternalSegment.size() > 0) {
//			for (ExternalPnrSegment reservationExternalSegment : colReservationExternalSegment) {
//				reservation.addExternalReservationSegment(reservationExternalSegment);
//
//				if (info.length() == 0) {
//					info.append(reservationExternalSegment.toString());
//				} else {
//					info.append(" , " + reservationExternalSegment.toString());
//				}
//			}
//		}
//
//		return info.toString();
//	}

//	private int getNextSegmentSeq(Collection<ExternalPnrSegment> externalSegments) {
//		List<Integer> lstSegmentSeq = new ArrayList<Integer>();
//
//		if (externalSegments != null && externalSegments.size() > 0) {
//			for (ExternalPnrSegment reservationExternalSegment : externalSegments) {
//				lstSegmentSeq.add(reservationExternalSegment.getSegmentSeq());
//			}
//
//			Collections.sort(lstSegmentSeq);
//			int i = lstSegmentSeq.get(lstSegmentSeq.size() - 1);
//			return i++;
//		} else {
//			return new Integer(1);
//		}
//	}

//	private Map<String, Collection<ExternalPnrSegment>> getConfirmedReservationExternalSegmentInAMap(
//			Collection<ExternalPnrSegment> externalSegments) {
//		Map<String, Collection<ExternalPnrSegment>> segmentMap = new HashMap<String, Collection<ExternalPnrSegment>>();
//		String uniqueSegmentKeyWithOutStatus;
//
//		for (ExternalPnrSegment reservationExternalSegment : externalSegments) {
//
//			if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(reservationExternalSegment
//					.getStatus())) {
//				uniqueSegmentKeyWithOutStatus = reservationExternalSegment.getUniqueSegmentKey();
//				Collection<ExternalPnrSegment> colReservationExternalSegment = segmentMap.get(uniqueSegmentKeyWithOutStatus);
//
//				if (colReservationExternalSegment == null) {
//					colReservationExternalSegment = new ArrayList<ExternalPnrSegment>();
//					colReservationExternalSegment.add(reservationExternalSegment);
//					segmentMap.put(uniqueSegmentKeyWithOutStatus, colReservationExternalSegment);
//				} else {
//					colReservationExternalSegment.add(reservationExternalSegment);
//				}
//			}
//		}
//
//		return segmentMap;
//	}

	/**
	 * Compose the audit
	 * 
	 * @param pnr
	 * @param addedSegmentInfo
	 * @param modifySegmentInfo
	 * @param credentialsDTO
	 * @return
	 */
	private ReservationAudit composeAudit(String pnr, String addedSegmentInfo, String modifySegmentInfo,
			CredentialsDTO credentialsDTO) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CHANGED_EXTERNAL_SEGMENTS.getCode());
		reservationAudit.setUserNote(null);

		if (addedSegmentInfo.length() > 0) {
			// Added Segment Information
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedExternalSegment.ADDED_SEGMENT_INFORMATION,
					"Added[" + addedSegmentInfo + "]");
		}

		if (modifySegmentInfo.length() > 0) {
			// modified Segment Information
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedExternalSegment.MODIFIED_SEGMENT_INFORMATION,
					"Modified[" + modifySegmentInfo + "]");
		}

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedExternalSegment.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		return reservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param colExternalSegmentTO
	 * @param version
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Collection<ExternalSegmentTO> colExternalSegmentTO, Long version,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || colExternalSegmentTO == null || colExternalSegmentTO.size() == 0 || version == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
