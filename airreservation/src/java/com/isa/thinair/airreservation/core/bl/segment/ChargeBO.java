/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.bl.segment;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ChargeResponseDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.FlightCOSDTO;
import com.isa.thinair.airreservation.api.dto.NameChangeExtChgDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.FeeCalculator;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.util.DiscountHandlerUtil;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * Reservation related business charge amount methods
 * 
 * Notes: This method is for sending per ond segment ids
 * 
 * Business Rules: (1) Cancellation charges apply for per passenger per fare (OND). (2) Infant won't have any
 * cancellation or modification charge.
 * 
 * Happy Path: (1) Will calculate the cancellation or modification total charge amounts
 * 
 * Alternative Path: (1) If the corresponding ond segment ids are not found
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ChargeBO {

	/**
	 * Holds whether or not to load meta amounts of charges / payments. In the bottom up approach of currency conversion
	 * these information are vital for the conversion.
	 */
	private boolean loadMetaAmounts;

	private CredentialsDTO credentialsDTO;

	public ChargeBO(boolean loadMetaAmounts, CredentialsDTO credentialsDTO) {
		this.loadMetaAmounts = loadMetaAmounts;
		this.credentialsDTO = credentialsDTO;
	}

	/**
	 * Return the corresponding infant reservation passenger fare
	 * 
	 * @param set
	 *            infants
	 * @throws ModuleException
	 */
	private ReservationPaxFare getInfantReservationPaxFare(Collection<ReservationPax> infants,
			Collection<Integer> parentFareSegIds) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = infants.iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFare reservationPaxFareInfant = null;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> infantSegIds;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();
			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();
				itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

				infantSegIds = new ArrayList<Integer>();

				while (itReservationPaxFareSegment.hasNext()) {
					reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();

					if (reservationPaxFareSegment.getPnrSegId() != null) {
						infantSegIds.add(reservationPaxFareSegment.getPnrSegId());
					}
				}

				if (parentFareSegIds.size() == infantSegIds.size() && parentFareSegIds.containsAll(infantSegIds)) {
					reservationPaxFareInfant = reservationPaxFare;
					break;
				}
			}
		}

		// If the infant ond fare is not found. Practically this can not happend
		if (reservationPaxFareInfant == null) {
			throw new ModuleException("airreservations.arg.invalidSegInfant");
		}

		return reservationPaxFareInfant;
	}

	/**
	 * Return segment ids for an OND
	 * 
	 * @param reservationPaxFare
	 * @return collection of pnr segment ids
	 */
	private Collection<Integer> getFareSegmentIds(ReservationPaxFare reservationPaxFare) {
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> segmentIds = new ArrayList<Integer>();

		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();

			if (reservationPaxFareSegment.getPnrSegId() != null) {
				segmentIds.add(reservationPaxFareSegment.getPnrSegId());
			}
		}

		return segmentIds;
	}

	/**
	 * Return identified cancellation amount
	 * 
	 * @param creditAmount
	 * @param actualChargeAmount
	 * @param availableBalance
	 * @return
	 */
	private BigDecimal getAdjustedCnxPenCharge(BigDecimal creditAmount, BigDecimal actualChargeAmount,
			BigDecimal availableBalance) {
		BigDecimal adjustedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalAvCredits = AccelAeroCalculator.add(availableBalance, creditAmount.negate());

		if (totalAvCredits.doubleValue() > 0) {
			adjustedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else {
			if (AccelAeroCalculator.add(totalAvCredits, actualChargeAmount).doubleValue() <= 0) {
				adjustedAmount = actualChargeAmount;
			} else {
				adjustedAmount = totalAvCredits.negate();
			}
		}

		return adjustedAmount;
	}

	/**
	 * Return identified cancellation amount
	 * 
	 * @param reservationPaxFare
	 * @param creditAmount
	 * @param cancellationCharge
	 * @param availableBalance
	 * @param colReservationTnx
	 * @param hasExternalPayments
	 * @param adjustCNXCharge
	 * @return
	 */
	private BigDecimal getIdentifiedCancellationCharge(ReservationPaxFare reservationPaxFare, BigDecimal creditAmount,
			BigDecimal cancellationCharge, BigDecimal availableBalance, Collection<ReservationTnx> colReservationTnx,
			boolean hasExternalPayments, boolean adjustCNXCharge) {

		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (hasExternalPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			// Find out whether any non expired segment exists
			boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFare);

			if (isAnyNonExpiredSegExists && !adjustCNXCharge) {
				identifiedAmount = cancellationCharge;
			} else {
				identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
			}
		} else {
			identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
		}

		return identifiedAmount;
	}

	private FeeHolder getFeeHolderForCancellation(ReservationPaxFare reservationPaxFare, BigDecimal creditAmount,
			BigDecimal cancellationCharge, BigDecimal availableBalance, Collection<ReservationTnx> colReservationTnx,
			boolean hasExternalPayments, boolean adjustCNXCharge, ServiceFeeBO serviceFeeBO, Integer pnrPaxId) throws ModuleException {
		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		BigDecimal maxAmount = null;
		if (hasExternalPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			// Find out whether any non expired segment exists
			boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFare);

			if (isAnyNonExpiredSegExists && !adjustCNXCharge) {
				identifiedAmount = cancellationCharge;
			} else {
				identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
				maxAmount = AccelAeroCalculator.add(availableBalance, creditAmount.negate()).negate();
			}
		} else {
			identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
			maxAmount = AccelAeroCalculator.add(availableBalance, creditAmount.negate()).negate();
		}

		if (maxAmount != null && maxAmount.doubleValue() < 0) {
			maxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		FeeHolder fee;

		if (serviceFeeBO.isExtraFeeApplicable()) {
			String cabinClassCode = ReservationCoreUtils.getCabinClassFromPaxFare(reservationPaxFare);
			fee = serviceFeeBO.getFeeHolder(identifiedAmount, maxAmount, ReservationInternalConstants.ChargeGroup.CNX,
					cabinClassCode, pnrPaxId);
		} else {
			fee = new FeeHolder(identifiedAmount);
		}
		return fee;
	}

	private BigDecimal getIdentifiedPenaltyCharge(ReservationPaxFare reservationPaxFare, BigDecimal creditAmount,
			BigDecimal penaltyCharge, BigDecimal availableBalance, Collection<ReservationTnx> colReservationTnx,
			boolean hasExternalPayments) {

		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (hasExternalPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			// Find out whether any non expired segment exists
			boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFare);

			if (isAnyNonExpiredSegExists) {
				identifiedAmount = penaltyCharge;
			} else {
				identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, penaltyCharge, availableBalance);
			}
		} else {
			identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, penaltyCharge, availableBalance);
		}

		return identifiedAmount;
	}

	/**
	 * Return amounts
	 * 
	 * @param reservationPaxFare
	 * @param mapFareTO
	 * @param mapChgTO
	 * @param isExchanged
	 *            TODO
	 * @param modOndPosition
	 *            TODO
	 * @param applyNameChangeCharge
	 * @return
	 * @throws ModuleException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ChargeResponseDTO getOndAmounts(ReservationPaxFare reservationPaxFare, Map mapFareTO, Map mapFareRuleFeeTO,
			Map mapChgTO, Collection<String> excludeChargeCodes, CustomChargesTO customChargesTO, boolean isCancelFlightsExists,
			boolean isSameFlightsModification, boolean isVoidReservation, Date executionDate, boolean isExchanged,
			boolean isAModification, int modOndPosition, boolean applyNameChangeCharge) throws ModuleException {

		boolean isOnHoldReservation = ReservationInternalConstants.ReservationStatus.ON_HOLD
				.equals(reservationPaxFare.getReservationPax().getReservation().getStatus()) ? true : false;

		BigDecimal[] refundAmounts = ReservationApiUtils.getChargeTypeAmounts();
		BigDecimal cancelTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal modifyTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal nameChangeTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ChargeMetaTO> colRefundableChargeMetaTO = new ArrayList<ChargeMetaTO>();
		Collection<ChargeMetaTO> colNonRefundableChargeMetaTO = new ArrayList<ChargeMetaTO>();

		String paxType = reservationPaxFare.getReservationPax().getPaxType();
		Iterator itCharges = reservationPaxFare.getCharges().iterator();
		ReservationPaxOndCharge reservationPaxOndCharge;
		FareTO fareTO;
		Collection<FareRuleFeeTO> colFareRuleFeeTO;
		ChargeTO chargeTO;

		boolean[] applyFlexiFlags = CancellationUtils.getApplyFlexibilitiesToOndFlags(reservationPaxFare);
		boolean hasCnxFlexibility = applyFlexiFlags[1];
		boolean hasModFlexibility = applyFlexiFlags[0];
		Collection<PnrChargeDetailTO> colCnxChargeDetailDTO = new ArrayList<PnrChargeDetailTO>();
		Collection<PnrChargeDetailTO> colModChargeDetailDTO = new ArrayList<PnrChargeDetailTO>();
		Collection<PnrChargeDetailTO> colNameChangeChargeDetailDTO = new ArrayList<PnrChargeDetailTO>();

		Collection<String> exchangeExcludeChargeCodes = new HashSet<String>();
		if (isExchanged) {
			exchangeExcludeChargeCodes = CancellationUtils.getExchangeExcludeChargeCodes();
		}

		boolean isRefundNonRefundableFaresInEx = AppSysParamsUtil.isRefundNonRefundableFaresInExchange();

		while (itCharges.hasNext()) {
			reservationPaxOndCharge = (ReservationPaxOndCharge) itCharges.next();

			if (reservationPaxOndCharge.getChargeRateId() != null) {
				chargeTO = (ChargeTO) mapChgTO.get(reservationPaxOndCharge.getChargeRateId());

				// If it's refundable
				if ((chargeTO.isRefundable() && (isExchanged || !chargeTO.isRefundableOnlyForMOD()
						|| (chargeTO.isRefundableOnlyForMOD() && isAModification)))
						|| (isOnHoldReservation && !excludeChargeCodes.contains(chargeTO.getChargeCode()))
						|| (isVoidReservation && !exchangeExcludeChargeCodes.contains(chargeTO.getChargeCode()))) {
					// Capturing the refundable amount
					ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
							reservationPaxOndCharge.getAmount(), refundAmounts, reservationPaxOndCharge.getDiscount(),
							reservationPaxOndCharge.getAdjustment());

					if (this.loadMetaAmounts) {
						colRefundableChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, mapChgTO));
					}
					// If it's non refundable
				} else {
					if (this.loadMetaAmounts) {
						if (!isOnHoldReservation && (isExchanged && chargeTO.isRefundableWhenExchange())) {
							ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
									reservationPaxOndCharge.getAmount(), refundAmounts, reservationPaxOndCharge.getDiscount(),
									reservationPaxOndCharge.getAdjustment());
							colRefundableChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, mapChgTO));
						} else {
							colNonRefundableChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, mapChgTO));
						}
					}
				}
			} else if (reservationPaxOndCharge.getFareId() != null) {
				fareTO = (FareTO) mapFareTO.get(reservationPaxOndCharge.getFareId());
				colFareRuleFeeTO = (Collection<FareRuleFeeTO>) mapFareRuleFeeTO.get(fareTO.getFareRuleID());

				// If it's refundable
				if (ReservationApiUtils.isRefundable(fareTO, paxType) || isOnHoldReservation || isVoidReservation) {
					// Capturing the refundable amount
					ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
							reservationPaxOndCharge.getAmount(), refundAmounts, reservationPaxOndCharge.getDiscount(),
							reservationPaxOndCharge.getAdjustment());

					if (this.loadMetaAmounts) {
						colRefundableChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, null));
					}
					// If it's non refundable
				} else {
					if (this.loadMetaAmounts) {
						if (!isOnHoldReservation && isExchanged && isRefundNonRefundableFaresInEx) {
							ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
									reservationPaxOndCharge.getAmount(), refundAmounts, reservationPaxOndCharge.getDiscount(),
									reservationPaxOndCharge.getAdjustment());
							colRefundableChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, null));
						} else {
							colNonRefundableChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, null));
						}
					}
				}

				FeeCalculator cal = new FeeCalculator(reservationPaxFare, colFareRuleFeeTO, executionDate,
						fareTO.isNoonDayFeePolicyEnabled());
				cal.execute();

				if (!applyNameChangeCharge) {
					FareRuleFeeTO cancelFee = cal.getCancelFee();
					FareRuleFeeTO modifyFee = cal.getModifyFee();
					PnrChargeDetailTO cnxChargeDetailDTO = new PnrChargeDetailTO();
					PnrChargeDetailTO modChargeDetailDTO = new PnrChargeDetailTO();

					// Capturing the cancellation amount
					cancelTotalCharge = AccelAeroCalculator.add(cancelTotalCharge,
							FeeBO.getCancellationCharge(fareTO, paxType, reservationPaxOndCharge.getAmount(),
									reservationPaxFare.getCharges(), cnxChargeDetailDTO, customChargesTO, cancelFee,
									hasCnxFlexibility, isCancelFlightsExists, isVoidReservation, modOndPosition));

					modifyTotalCharge = AccelAeroCalculator.add(modifyTotalCharge,
							FeeBO.getModificationCharge(fareTO, paxType, reservationPaxOndCharge.getAmount(),
									reservationPaxFare.getCharges(), modChargeDetailDTO, customChargesTO, modifyFee,
									hasModFlexibility, isCancelFlightsExists, isSameFlightsModification, modOndPosition));

					colCnxChargeDetailDTO.add(cnxChargeDetailDTO);
					colModChargeDetailDTO.add(modChargeDetailDTO);
				} else {
					FareRuleFeeTO nameChangeFee = cal.getNameChangeFee();
					PnrChargeDetailTO nameChangeChargeDetailDTO = new PnrChargeDetailTO();
					nameChangeTotalCharge = AccelAeroCalculator.add(nameChangeTotalCharge,
							FeeBO.getNameChangeCharge(fareTO, reservationPaxOndCharge.getAmount(),
									reservationPaxFare.getCharges(), nameChangeChargeDetailDTO, nameChangeFee));
					colNameChangeChargeDetailDTO.add(nameChangeChargeDetailDTO);
				}
			}
		} // end while

		ChargeResponseDTO chargeResponseDTO = new ChargeResponseDTO();

		chargeResponseDTO.setRefundAmounts(refundAmounts);
		chargeResponseDTO.setCancelTotalCharge(cancelTotalCharge);
		chargeResponseDTO.setModifyTotalCharge(modifyTotalCharge);
		chargeResponseDTO.setNameChangeTotalCharge(nameChangeTotalCharge);
		chargeResponseDTO.setRefundableChargeMetaTOs(colRefundableChargeMetaTO);
		chargeResponseDTO.setNonRefundableChargeMetaTOs(colNonRefundableChargeMetaTO);
		chargeResponseDTO.setCnxChargeDetailDTO(BeanUtils.getFirstElement(colCnxChargeDetailDTO));
		chargeResponseDTO.setModChargeDetailDTO(BeanUtils.getFirstElement(colModChargeDetailDTO));
		chargeResponseDTO.setNameChangeChargeDetailDTO(BeanUtils.getFirstElement(colNameChangeChargeDetailDTO));

		// Split Discount amount based on refundable/non-refndable charges
		DiscountHandlerUtil.filterDiscount(chargeResponseDTO);

		return chargeResponseDTO;
	}

	/**
	 * Return reservation charges data transfer information
	 * 
	 * @param reservationPax
	 * @param reservationPaxFare
	 * @param customChargesTO
	 * @param hasExternalPayments
	 * @param isSameFlightsModification
	 * @param isVoidReservation
	 * @param flownAsstUnit
	 * @param isExchanged
	 * @param modOndPosition
	 * @param isInfantPaymentSeparated
	 *            TODO
	 * @param showCnxCharge
	 * @param showModCharge
	 * @param isFromNameChange
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private PnrChargesDTO getPassengerOndAmounts(ReservationPax reservationPax, ReservationPaxFare reservationPaxFare,
			boolean applyCnxCharge, boolean applyModCharge, CustomChargesTO customChargesTO, boolean hasExternalPayments,
			boolean isSameFlightsModification, boolean isVoidReservation, FlownAssitUnit flownAsstUnit, boolean isExchanged,
			int modOndPosition, boolean applyNameChangeCharge, boolean isInfantPaymentSeparated) throws ModuleException {

		Date executionDate = new Date();
		ReservationPaxFare reservationPaxFareInfant = null;
		Collection<Integer> fareIds = new HashSet<Integer>();
		Collection<Integer> chgRateIds = new HashSet<Integer>();
		PnrChargesDTO pnrChargesDTO = new PnrChargesDTO();

		boolean isCancelFlightsExists = CancellationUtils.isAnyCancelFlightsExists(reservationPaxFare);

		Collection<Integer> newFltSegIds = null;
		Collection<OndFareDTO> ondFares = null;
		if (flownAsstUnit != null) {
			newFltSegIds = flownAsstUnit.getNewFltSegIds();
			ondFares = flownAsstUnit.getOndFares();
		}

		ExtraFeeBO extraFeeBO = new ExtraFeeBO(reservationPax.getReservation(), newFltSegIds);
		ServiceFeeBO serviceFeeBO = new ServiceFeeBO(reservationPax.getReservation(), getComposeUserPrincipal(), ondFares);

		chgRateIds.addAll(extraFeeBO.getChargeRateIds());

		// Parent
		if (!isInfantPaymentSeparated && ReservationApiUtils.isParentAfterSave(reservationPax)) {
			Collection<Integer> parentFareSegIds = this.getFareSegmentIds(reservationPaxFare);
			// Locate the corresponding infant ond
			reservationPaxFareInfant = this.getInfantReservationPaxFare(reservationPax.getInfants(), parentFareSegIds);
			PaxFareUtil.getFareIdsAndChargeIds(reservationPaxFare, fareIds, chgRateIds);
			PaxFareUtil.getFareIdsAndChargeIds(reservationPaxFareInfant, fareIds, chgRateIds);
			pnrChargesDTO.setParent(true);
		}
		// Adult
		else if (ReservationApiUtils.isSingleAfterSave(reservationPax)
				|| (isInfantPaymentSeparated && ReservationApiUtils.isParentAfterSave(reservationPax))) {
			PaxFareUtil.getFareIdsAndChargeIds(reservationPaxFare, fareIds, chgRateIds);
		}
		// Child
		else if (ReservationApiUtils.isChildType(reservationPax)) {
			PaxFareUtil.getFareIdsAndChargeIds(reservationPaxFare, fareIds, chgRateIds);
		}
		// Infant
		else if (ReservationApiUtils.isInfantType(reservationPax)) {
			PaxFareUtil.getFareIdsAndChargeIds(reservationPaxFare, fareIds, chgRateIds);
		}
		
		boolean isOnlyCancellation = applyCnxCharge && !applyModCharge ? true : false;
		
		// check if segment is no show then skip cancellation charge
		applyCnxCharge = CancellationUtils.applyCancelChargeForFlown(applyCnxCharge,
				flownAsstUnit != null ? flownAsstUnit.hasFlownInternationalSegments() : false);
		applyCnxCharge = CancellationUtils.applyCancelChargeForNoShowSegment(applyCnxCharge, reservationPaxFare);

		FareBD fareBD = ReservationModuleUtils.getFareBD();
		// Collect fares and charges information
		FaresAndChargesTO faresAndChargesTO = fareBD.getRefundableStatuses(fareIds, chgRateIds, null);
		Boolean isUserModification = null;

		if (flownAsstUnit != null && flownAsstUnit.getModifyAsst() != null) {
			isUserModification = flownAsstUnit.getModifyAsst().isUserModification();
		} else {
			isUserModification = applyModCharge;
		}
		Collection<String> excludeChargeCodes = CancellationUtils.getExcludeChargeCodes(isUserModification);
		Map<Integer, FareTO> mapFareTO = faresAndChargesTO.getFareTOs();
		Map<Integer, ChargeTO> mapChgTO = faresAndChargesTO.getChargeTOs();
		Map<Integer, Collection<FareRuleFeeTO>> mapFareRuleFeeTO = faresAndChargesTO.getFareRuleFeeTOs();

		PnrChargeDetailTO cnxChargeDetailTO = new PnrChargeDetailTO();
		PnrChargeDetailTO modChargeDetailTO = new PnrChargeDetailTO();
		PnrChargeDetailTO nameChangeChargeDetailTO = new PnrChargeDetailTO();

		boolean adjustCNXCharge = AppSysParamsUtil.isSystemChargeAdjustmentForDueAmountEnabled() && applyCnxCharge;

		// Finding the refundable and cancellation charges
		// Parent
		if (!isInfantPaymentSeparated && ReservationApiUtils.isParentAfterSave(reservationPax)) {

			ChargeResponseDTO adultChargeResponseDTO = this.getOndAmounts(reservationPaxFare, mapFareTO, mapFareRuleFeeTO,
					mapChgTO, excludeChargeCodes, customChargesTO, isCancelFlightsExists, isSameFlightsModification,
					isVoidReservation, executionDate, isExchanged, applyModCharge, modOndPosition, applyNameChangeCharge);
			ChargeResponseDTO infantChargeResponseDTO = this.getOndAmounts(reservationPaxFareInfant, mapFareTO, mapFareRuleFeeTO,
					mapChgTO, excludeChargeCodes, customChargesTO, isCancelFlightsExists, isSameFlightsModification,
					isVoidReservation, executionDate, isExchanged, applyModCharge, modOndPosition, applyNameChangeCharge);

			BigDecimal[] adultRefundAmounts = adultChargeResponseDTO.getRefundAmounts();

			Collection<ChargeMetaTO> refundableAdultChargeMetaTO = adultChargeResponseDTO.getRefundableChargeMetaTOs();
			Collection<ChargeMetaTO> nonRefundableAdultChargeMetaTO = adultChargeResponseDTO.getNonRefundableChargeMetaTOs();

			BigDecimal[] infantRefundAmounts = infantChargeResponseDTO.getRefundAmounts();

			Collection<ChargeMetaTO> refundableInfantChargeMetaTO = infantChargeResponseDTO.getRefundableChargeMetaTOs();
			Collection<ChargeMetaTO> nonRefundableInfantChargeMetaTO = infantChargeResponseDTO.getNonRefundableChargeMetaTOs();

			// Infant will have a fare or not
			BigDecimal[] totalRefundableAmount = ReservationApiUtils.getUpdatedTotals(adultRefundAmounts, infantRefundAmounts,
					true);
			BigDecimal totalCredit = ReservationApiUtils.getTotalChargeAmount(totalRefundableAmount);

			BigDecimal cancellationCharge = AccelAeroCalculator.add(adultChargeResponseDTO.getCancelTotalCharge(),
					infantChargeResponseDTO.getCancelTotalCharge());
			BigDecimal modificationCharge = AccelAeroCalculator.add(adultChargeResponseDTO.getModifyTotalCharge(),
					infantChargeResponseDTO.getModifyTotalCharge());
			// BigDecimal nameChangeCharge = AccelAeroCalculator.add(adultChargeResponseDTO.getNameChangeTotalCharge(),
			// infantChargeResponseDTO.getNameChangeTotalCharge());
			BigDecimal nameChangeCharge = adultChargeResponseDTO.getNameChangeTotalCharge();
			BigDecimal availableBalance = reservationPax.getTotalAvailableBalance();

			if (customChargesTO != null && customChargesTO.isCompareWithExistingModificationCharges()) {
				cancellationCharge = getEffectiveCancellationCharge(customChargesTO, cancellationCharge, PaxTypeTO.PARENT);
			}

			// Get the payment information
			Collection<ReservationTnx> colReservationTnx = this.getPaymentInformation(reservationPax.getPnrPaxId());

			BigDecimal identifiedPenaltyAmt = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (flownAsstUnit != null) {
				if (flownAsstUnit.isPenaltyApplicablePaxFare(reservationPaxFare)) {
					BigDecimal penalty = flownAsstUnit.getPenaltyForPaxFare(reservationPaxFare);
					identifiedPenaltyAmt = this.getIdentifiedPenaltyCharge(reservationPaxFare, totalCredit, penalty,
							availableBalance, colReservationTnx, hasExternalPayments);

					totalCredit = CancellationUtils.reValidateCreditAmount(totalCredit, identifiedPenaltyAmt);
				}

				if (flownAsstUnit.isPenaltyApplicablePaxFare(reservationPaxFareInfant)) {
					BigDecimal penalty = flownAsstUnit.getPenaltyForPaxFare(reservationPaxFareInfant);
					BigDecimal identifiedPenaltyAmtInf = this.getIdentifiedPenaltyCharge(reservationPaxFareInfant, totalCredit,
							penalty, availableBalance, colReservationTnx, hasExternalPayments);
					identifiedPenaltyAmt = AccelAeroCalculator.add(identifiedPenaltyAmt, identifiedPenaltyAmtInf);

					totalCredit = CancellationUtils.reValidateCreditAmount(totalCredit, identifiedPenaltyAmtInf);
				}
			}

			// Get the identified cancellation amount
			FeeHolder cnxHolder = this.getFeeHolderForCancellation(reservationPaxFare, totalCredit, cancellationCharge,
					availableBalance, colReservationTnx, hasExternalPayments, adjustCNXCharge, serviceFeeBO,
					reservationPax.getPnrPaxId());

			BigDecimal identifiedCAmount = cnxHolder.getChargeAmount();

			totalCredit = CancellationUtils.reValidateCreditAmount(totalCredit, cnxHolder.getTotal());

			// Get the identified modification amount
			FeeHolder modHolder = this.getFeeHolderForModification(modificationCharge, reservationPaxFare, colReservationTnx,
					hasExternalPayments, serviceFeeBO, reservationPax.getPnrPaxId());
			BigDecimal identifiedMAmount = modHolder.getChargeAmount();

			// Get the identified extra fee amount
			BigDecimal totalExtraFee = this.getTotalExtraFees(applyCnxCharge, identifiedCAmount, applyModCharge,
					identifiedMAmount, reservationPaxFare, totalCredit, availableBalance, colReservationTnx, hasExternalPayments,
					extraFeeBO);

			if (applyCnxCharge) {
				totalExtraFee = AccelAeroCalculator.add(cnxHolder.getTotalTaxAmount(), totalExtraFee);
			}
			if (applyModCharge) {
				totalExtraFee = AccelAeroCalculator.add(modHolder.getTotalTaxAmount(), totalExtraFee);
			}

			// Setting pnr and pax id
			pnrChargesDTO.setPnr(reservationPax.getReservation().getPnr());
			pnrChargesDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			pnrChargesDTO.setAvailableBalance(AccelAeroCalculator.add(totalExtraFee, reservationPax.getTotalAvailableBalance()));

			this.applyCancellationCharges(applyCnxCharge, cancellationCharge, identifiedCAmount, pnrChargesDTO);
			this.applyModificationCharges(applyModCharge, modificationCharge, identifiedMAmount, pnrChargesDTO);
			this.applyPenaltyCharges(identifiedPenaltyAmt, pnrChargesDTO, reservationPax, flownAsstUnit, isOnlyCancellation);

			// this.applyExtraFee(totalExtraFee, reservationPaxFare, refundableAdultChargeMetaTO,
			// nonRefundableAdultChargeMetaTO,
			// mapChgTO, extraFeeBO, pnrChargesDTO, applyModCharge);

			this.applyExtraFee(totalExtraFee, pnrChargesDTO);

			pnrChargesDTO.setPaxType(reservationPax.getPaxType());
			// Set the payment information
			pnrChargesDTO.setPaymentInfo(colReservationTnx);

			// Refundable fare and tax/sur amounts
			pnrChargesDTO.setRefundableAmounts(totalRefundableAmount);

			// Actual fare and tax/sur amounts
			pnrChargesDTO.setActualAmounts(ReservationApiUtils.getUpdatedTotals(reservationPaxFare.getTotalChargeAmounts(),
					reservationPaxFareInfant.getTotalChargeAmounts(), true));

			// Meta data information
			pnrChargesDTO
					.setRefundableChargeMetaAmounts(ChargeMetaTO.sum(refundableAdultChargeMetaTO, refundableInfantChargeMetaTO));
			pnrChargesDTO.setNonRefundableChargeMetaAmounts(
					ChargeMetaTO.sum(nonRefundableAdultChargeMetaTO, nonRefundableInfantChargeMetaTO));
			pnrChargesDTO.setActualChargeMetaAmounts(ChargeMetaTO.sum(convert(reservationPaxFare.getCharges(), mapChgTO),
					convert(reservationPaxFareInfant.getCharges(), mapChgTO)));
			cnxChargeDetailTO = adultChargeResponseDTO.getCnxChargeDetailDTO();
			modChargeDetailTO = adultChargeResponseDTO.getModChargeDetailDTO();

			nameChangeChargeDetailTO = adultChargeResponseDTO.getNameChangeChargeDetailDTO();
			pnrChargesDTO.setNameChangeChargeAmount(nameChangeCharge);
		}
		// Adult or Child
		else if (ReservationApiUtils.isSingleAfterSave(reservationPax) || ReservationApiUtils.isChildType(reservationPax)
				|| (ReservationApiUtils.isParentAfterSave(reservationPax) && isInfantPaymentSeparated)) {

			ChargeResponseDTO chargeResponseDTO = this.getOndAmounts(reservationPaxFare, mapFareTO, mapFareRuleFeeTO, mapChgTO,
					excludeChargeCodes, customChargesTO, isCancelFlightsExists, isSameFlightsModification, isVoidReservation,
					executionDate, isExchanged, applyModCharge, modOndPosition, applyNameChangeCharge);

			BigDecimal[] adultOrChildRefundAmounts = chargeResponseDTO.getRefundAmounts();

			Collection<ChargeMetaTO> refundableAdultOrChildChargeMetaTO = chargeResponseDTO.getRefundableChargeMetaTOs();
			Collection<ChargeMetaTO> nonRefundableAdultOrChildChargeMetaTO = chargeResponseDTO.getNonRefundableChargeMetaTOs();

			BigDecimal totalCredit = ReservationApiUtils.getTotalChargeAmount(adultOrChildRefundAmounts);

			BigDecimal cancellationCharge = chargeResponseDTO.getCancelTotalCharge();
			BigDecimal modificationCharge = chargeResponseDTO.getModifyTotalCharge();
			BigDecimal nameChangeCharge = chargeResponseDTO.getNameChangeTotalCharge();
			BigDecimal availableBalance = reservationPax.getTotalAvailableBalance();

			if (customChargesTO != null && customChargesTO.isCompareWithExistingModificationCharges()) {
				cancellationCharge = getEffectiveCancellationCharge(customChargesTO, cancellationCharge, PaxTypeTO.ADULT);
			}

			// Get the payment information
			Collection<ReservationTnx> colReservationTnx = this.getPaymentInformation(reservationPax.getPnrPaxId());

			BigDecimal identifiedPenaltyAmt = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (flownAsstUnit != null) {
				if (flownAsstUnit.isPenaltyApplicablePaxFare(reservationPaxFare)) {
					BigDecimal penalty = flownAsstUnit.getPenaltyForPaxFare(reservationPaxFare);
					identifiedPenaltyAmt = this.getIdentifiedPenaltyCharge(reservationPaxFare, totalCredit, penalty,
							availableBalance, colReservationTnx, hasExternalPayments);

					totalCredit = CancellationUtils.reValidateCreditAmount(totalCredit, identifiedPenaltyAmt);
				}
			}

			// Get the identified cancellation amount
			FeeHolder cnxHolder = this.getFeeHolderForCancellation(reservationPaxFare, totalCredit, cancellationCharge,
					availableBalance, colReservationTnx, hasExternalPayments, adjustCNXCharge, serviceFeeBO,
					reservationPax.getPnrPaxId());

			BigDecimal identifiedCAmount = cnxHolder.getChargeAmount();

			totalCredit = CancellationUtils.reValidateCreditAmount(totalCredit, cnxHolder.getTotal());

			// Get the identified modification amount
			FeeHolder modHolder = this.getFeeHolderForModification(modificationCharge, reservationPaxFare, colReservationTnx,
					hasExternalPayments, serviceFeeBO, reservationPax.getPnrPaxId());
			BigDecimal identifiedMAmount = modHolder.getChargeAmount();

			// Get the identified extra fee amount
			BigDecimal totalExtraFee = this.getTotalExtraFees(applyCnxCharge, identifiedCAmount, applyModCharge,
					identifiedMAmount, reservationPaxFare, totalCredit, availableBalance, colReservationTnx, hasExternalPayments,
					extraFeeBO);

			if (applyCnxCharge) {
				totalExtraFee = AccelAeroCalculator.add(cnxHolder.getTotalTaxAmount(), totalExtraFee);
			}
			if (applyModCharge) {
				totalExtraFee = AccelAeroCalculator.add(modHolder.getTotalTaxAmount(), totalExtraFee);
			}

			// Setting pnr and pax id
			pnrChargesDTO.setPnr(reservationPax.getReservation().getPnr());
			pnrChargesDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			pnrChargesDTO.setAvailableBalance(reservationPax.getTotalAvailableBalance());

			this.applyCancellationCharges(applyCnxCharge, cancellationCharge, identifiedCAmount, pnrChargesDTO);
			this.applyModificationCharges(applyModCharge, modificationCharge, identifiedMAmount, pnrChargesDTO);
			this.applyPenaltyCharges(identifiedPenaltyAmt, pnrChargesDTO, reservationPax, flownAsstUnit, isOnlyCancellation);
			// this.applyExtraFee(totalExtraFee, reservationPaxFare, refundableAdultOrChildChargeMetaTO,
			// nonRefundableAdultOrChildChargeMetaTO, mapChgTO, extraFeeBO, pnrChargesDTO, applyModCharge);

			this.applyExtraFee(totalExtraFee, pnrChargesDTO);

			pnrChargesDTO.setPaxType(reservationPax.getPaxType());

			// Set the payment information
			pnrChargesDTO.setPaymentInfo(colReservationTnx);

			// Refundable fare and tax/sur amount
			pnrChargesDTO.setRefundableAmounts(adultOrChildRefundAmounts);

			// Actual fare and tax/sur amounts
			pnrChargesDTO.setActualAmounts(reservationPaxFare.getTotalChargeAmounts());

			// Meta data information
			pnrChargesDTO.setRefundableChargeMetaAmounts(refundableAdultOrChildChargeMetaTO);
			pnrChargesDTO.setNonRefundableChargeMetaAmounts(nonRefundableAdultOrChildChargeMetaTO);
			pnrChargesDTO.setActualChargeMetaAmounts(convert(reservationPaxFare.getCharges(), mapChgTO));

			cnxChargeDetailTO = chargeResponseDTO.getCnxChargeDetailDTO();
			modChargeDetailTO = chargeResponseDTO.getModChargeDetailDTO();
			nameChangeChargeDetailTO = chargeResponseDTO.getNameChangeChargeDetailDTO();
			pnrChargesDTO.setNameChangeChargeAmount(nameChangeCharge);

		}
		// Infant
		else if (ReservationApiUtils.isInfantType(reservationPax)) {
			// BigDecimal charges[] = new BigDecimal[] { cancelTotalCharge, modifyTotalCharge };

			// Object[] response = new Object[] { refundAmounts, charges, colRefundableChargeMetaTO,
			// colNonRefundableChargeMetaTO,
			// BeanUtils.getFirstElement(colCnxChargeDetailDTO), BeanUtils.getFirstElement(colModChargeDetailDTO) };
			ChargeResponseDTO infantChargeResponseDTO = this.getOndAmounts(reservationPaxFare, mapFareTO, mapFareRuleFeeTO,
					mapChgTO, excludeChargeCodes, customChargesTO, isCancelFlightsExists, isSameFlightsModification,
					isVoidReservation, executionDate, isExchanged, applyModCharge, modOndPosition, applyNameChangeCharge);

			BigDecimal[] infantRefundAmounts = (BigDecimal[]) infantChargeResponseDTO.getRefundAmounts();

			Collection<ChargeMetaTO> refundableInfantChargeMetaTO = infantChargeResponseDTO.getRefundableChargeMetaTOs();
			Collection<ChargeMetaTO> nonRefundableInfantChargeMetaTO = infantChargeResponseDTO.getNonRefundableChargeMetaTOs();

			BigDecimal totalCredit = ReservationApiUtils.getTotalChargeAmount(infantRefundAmounts);

			BigDecimal cancellationCharge = infantChargeResponseDTO.getCancelTotalCharge();
			BigDecimal modificationCharge = infantChargeResponseDTO.getModifyTotalCharge();
			BigDecimal nameChangeCharge = infantChargeResponseDTO.getNameChangeTotalCharge();
			BigDecimal availableBalance = reservationPax.getParent().getTotalAvailableBalance();

			if (customChargesTO != null && customChargesTO.isCompareWithExistingModificationCharges()) {
				cancellationCharge = getEffectiveCancellationCharge(customChargesTO, cancellationCharge, PaxTypeTO.INFANT);
			}

			// Get the payment information
			Collection<ReservationTnx> colReservationTnx;
			if (reservationPax.getReservation().isInfantPaymentRecordedWithInfant()) {
				colReservationTnx = this.getPaymentInformation(reservationPax.getPnrPaxId());
			} else {
				colReservationTnx = this.getPaymentInformation(reservationPax.getAccompaniedPaxId());
			}

			BigDecimal identifiedPenaltyAmt = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (flownAsstUnit != null) {
				if (flownAsstUnit.isPenaltyApplicablePaxFare(reservationPaxFare)) {
					BigDecimal penalty = flownAsstUnit.getPenaltyForPaxFare(reservationPaxFare);
					identifiedPenaltyAmt = this.getIdentifiedPenaltyCharge(reservationPaxFare, totalCredit, penalty,
							availableBalance, colReservationTnx, hasExternalPayments);

					totalCredit = CancellationUtils.reValidateCreditAmount(totalCredit, identifiedPenaltyAmt);
				}
			}

			// Get the identified cancellation amount
			FeeHolder cnxHolder = this.getFeeHolderForCancellation(reservationPaxFare, totalCredit, cancellationCharge,
					availableBalance, colReservationTnx, hasExternalPayments, adjustCNXCharge, serviceFeeBO,
					reservationPax.getPnrPaxId());

			BigDecimal identifiedCAmount = cnxHolder.getChargeAmount();

			totalCredit = CancellationUtils.reValidateCreditAmount(totalCredit, cnxHolder.getTotal());

			// Get the identified modification amount
			FeeHolder modHolder = this.getFeeHolderForModification(modificationCharge, reservationPaxFare, colReservationTnx,
					hasExternalPayments, serviceFeeBO, reservationPax.getPnrPaxId());
			BigDecimal identifiedMAmount = modHolder.getChargeAmount();

			// Get the identified extra fee amount
			BigDecimal totalExtraFee = this.getTotalExtraFees(applyCnxCharge, identifiedCAmount, applyModCharge,
					identifiedMAmount, reservationPaxFare, totalCredit, availableBalance, colReservationTnx, hasExternalPayments,
					extraFeeBO);
			if (applyCnxCharge) {
				totalExtraFee = AccelAeroCalculator.add(cnxHolder.getTotalTaxAmount(), totalExtraFee);
			}
			if (applyModCharge) {
				totalExtraFee = AccelAeroCalculator.add(modHolder.getTotalTaxAmount(), totalExtraFee);
			}
			// Setting pnr and pax id
			pnrChargesDTO.setPnr(reservationPax.getReservation().getPnr());
			pnrChargesDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			pnrChargesDTO.setAvailableBalance(
					AccelAeroCalculator.add(totalExtraFee, reservationPax.getParent().getTotalAvailableBalance()));

			this.applyCancellationCharges(applyCnxCharge, cancellationCharge, identifiedCAmount, pnrChargesDTO);
			this.applyModificationCharges(applyModCharge, modificationCharge, identifiedMAmount, pnrChargesDTO);
			this.applyPenaltyCharges(identifiedPenaltyAmt, pnrChargesDTO, reservationPax, flownAsstUnit, isOnlyCancellation);
			// this.applyExtraFee(totalExtraFee, reservationPaxFare, refundableInfantChargeMetaTO,
			// nonRefundableInfantChargeMetaTO,
			// mapChgTO, extraFeeBO, pnrChargesDTO, applyModCharge);

			this.applyExtraFee(totalExtraFee, pnrChargesDTO);

			pnrChargesDTO.setPaxType(reservationPax.getPaxType());

			// Set the payment information
			pnrChargesDTO.setPaymentInfo(colReservationTnx);

			// Refundable fare and tax/sur amount
			pnrChargesDTO.setRefundableAmounts(infantRefundAmounts);

			// Actual fare and tax/sur amounts
			pnrChargesDTO.setActualAmounts(reservationPaxFare.getTotalChargeAmounts());

			// Meta data information
			pnrChargesDTO.setRefundableChargeMetaAmounts(refundableInfantChargeMetaTO);
			pnrChargesDTO.setNonRefundableChargeMetaAmounts(nonRefundableInfantChargeMetaTO);
			pnrChargesDTO.setActualChargeMetaAmounts(convert(reservationPaxFare.getCharges(), mapChgTO));
			cnxChargeDetailTO = infantChargeResponseDTO.getCnxChargeDetailDTO();
			modChargeDetailTO = infantChargeResponseDTO.getModChargeDetailDTO();
			nameChangeChargeDetailTO = infantChargeResponseDTO.getNameChangeChargeDetailDTO();
			pnrChargesDTO.setNameChangeChargeAmount(nameChangeCharge);
		}
		pnrChargesDTO.setCnxChargeDetailTO(cnxChargeDetailTO);
		pnrChargesDTO.setModChargeDetailTO(modChargeDetailTO);
		pnrChargesDTO.setNameChangeChargeDetailTO(nameChangeChargeDetailTO);
		return pnrChargesDTO;
	}

	private UserPrincipal getComposeUserPrincipal() {
		UserPrincipal userPrincipal = new UserPrincipal();
		return (UserPrincipal) userPrincipal.createIdentity(null, credentialsDTO.getSalesChannelCode(),
				credentialsDTO.getAgentCode(), credentialsDTO.getAgentStation(), null, credentialsDTO.getUserId(),
				credentialsDTO.getColUserDST(), credentialsDTO.getPassword(), null, null, null, null,
				credentialsDTO.getAgentCurrencyCode(), null, null, null, null);
	}

	private void applyExtraFee(BigDecimal totalExtraFee, PnrChargesDTO pnrChargesDTO) {
		if (totalExtraFee.doubleValue() > 0) {
			pnrChargesDTO.setIdentifiedExtraFeeAmount(totalExtraFee);
		} else {
			pnrChargesDTO.setIdentifiedExtraFeeAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}
	}

	private BigDecimal getTotalExtraFees(boolean applyCnxCharge, BigDecimal identifiedCAmount, boolean applyModCharge,
			BigDecimal identifiedMAmount, ReservationPaxFare reservationPaxFare, BigDecimal creditAmount,
			BigDecimal availableBalance, Collection<ReservationTnx> colReservationTnx, boolean hasExternalPayments,
			ExtraFeeBO extraFeeBO) throws ModuleException {

		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (extraFeeBO.isExtraFeeApplicable()) {

			String cabinClassCode = ReservationCoreUtils.getCabinClassFromPaxFare(reservationPaxFare);
			BigDecimal applicableTotalExtraFee = extraFeeBO.getFee(applyCnxCharge, identifiedCAmount, applyModCharge,
					identifiedMAmount, cabinClassCode);

			if (applyModCharge) {
				if (hasExternalPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
					identifiedAmount = applicableTotalExtraFee;
				} else {
					identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				}
			} else {
				if (hasExternalPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
					// Find out whether any non expired segment exists
					boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFare);

					if (isAnyNonExpiredSegExists) {
						identifiedAmount = applicableTotalExtraFee;
					} else {
						identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, applicableTotalExtraFee, availableBalance);
					}
				} else {
					identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, applicableTotalExtraFee, availableBalance);
				}
			}
		}

		return identifiedAmount;
	}

	/**
	 * Apply modification charges
	 * 
	 * @param showModCharge
	 * @param modificationCharge
	 * @param identifiedMAmount
	 * @param pnrChargesDTO
	 */
	private void applyModificationCharges(boolean applyModCharge, BigDecimal modificationCharge, BigDecimal identifiedMAmount,
			PnrChargesDTO pnrChargesDTO) {

		if (applyModCharge) {
			pnrChargesDTO.setActualModificationAmount(modificationCharge);
			pnrChargesDTO.setIdentifiedModificationAmount(identifiedMAmount);
		} else {
			pnrChargesDTO.setActualModificationAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			pnrChargesDTO.setIdentifiedModificationAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}
	}

	/**
	 * Apply cancellation charges
	 * 
	 * @param showCnxCharge
	 * @param cancellationCharge
	 * @param identifiedCAmount
	 * @param pnrChargesDTO
	 */
	private void applyCancellationCharges(boolean applyCnxCharge, BigDecimal cancellationCharge, BigDecimal identifiedCAmount,
			PnrChargesDTO pnrChargesDTO) {

		if (applyCnxCharge) {
			pnrChargesDTO.setActualCancellationAmount(cancellationCharge);
			pnrChargesDTO.setIdentifiedCancellationAmount(identifiedCAmount);
		} else {
			pnrChargesDTO.setActualCancellationAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			pnrChargesDTO.setIdentifiedCancellationAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}

	}

	private void applyPenaltyCharges(BigDecimal penaltyCharge, PnrChargesDTO pnrChargesDTO, ReservationPax reservationPax,
			FlownAssitUnit flownAsstUnit, boolean isOnlyCancellation) {

		try {
			if (penaltyCharge != null && penaltyCharge.compareTo(BigDecimal.ZERO) > 0) {

				Collection<String> onTicketingRevenueChargeGroups = AppSysParamsUtil
						.getChargeGroupsToQuoteGSTOnNonTicketingRevenue();

				if (onTicketingRevenueChargeGroups != null
						&& onTicketingRevenueChargeGroups.contains(ReservationInternalConstants.ChargeGroup.PEN)) {

					if (credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_AGENT)
							|| credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_LCC)) {

						UserPrincipal userPrincipal = (UserPrincipal) UserPrincipal.createIdentity(null,
								credentialsDTO.getSalesChannelCode(), credentialsDTO.getAgentCode(),
								credentialsDTO.getAgentStation(), null, credentialsDTO.getUserId(),
								credentialsDTO.getColUserDST(), credentialsDTO.getPassword(), null, null, null, null,
								credentialsDTO.getAgentCurrencyCode(), null, null, null, null);

						ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO = ReservationApiUtils
								.createPaxWiseNonTicketingServiceTaxQuoteRQForPenalty(penaltyCharge, reservationPax,
										isOnlyCancellation);

						ServiceTaxQuoteForNonTicketingRevenueRS nonTicketingServiceTaxPenaltyRS = ReservationModuleUtils
								.getAirproxyReservationBD().quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteCriteriaDTO,
										credentialsDTO.getTrackInfoDTO(), userPrincipal);

						if (nonTicketingServiceTaxPenaltyRS != null
								&& nonTicketingServiceTaxPenaltyRS.getPaxWiseCnxModPenServiceTaxes() != null
								&& nonTicketingServiceTaxPenaltyRS.getPaxWiseCnxModPenServiceTaxes().get(1) != null
								&& AccelAeroCalculator.isGreaterThan(
										nonTicketingServiceTaxPenaltyRS.getPaxWiseCnxModPenServiceTaxes().get(1)
												.getEffectiveCnxModPenChargeAmount(),
										AccelAeroCalculator.getDefaultBigDecimalZero())) {
							if (isOnlyCancellation) {
								penaltyCharge = nonTicketingServiceTaxPenaltyRS.getPaxWiseCnxModPenServiceTaxes().get(1)
										.getEffectiveCnxModPenChargeAmount();
							}
							for (ServiceTaxDTO serviceTaxDTO : nonTicketingServiceTaxPenaltyRS.getPaxWiseCnxModPenServiceTaxes()
									.get(1).getCnxModPenServiceTaxes()) {
								ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(serviceTaxDTO.getChargeGroupCode(),
										serviceTaxDTO.getAmount(), new Date(), AccelAeroCalculator.getDefaultBigDecimalZero(),
										AccelAeroCalculator.getDefaultBigDecimalZero(), serviceTaxDTO.getChargeCode(), null);
								pnrChargesDTO.getServiceTaxPenaltyCharges().add(metaTO);
							}
						}

					}

				} else {

					ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ = ReservationApiUtils
							.createPaxWiseTicketingServiceTaxQuoteRQForPenalty(penaltyCharge, flownAsstUnit, reservationPax,
									isOnlyCancellation);
					ServiceTaxQuoteForTicketingRevenueRS serviceTaxForTicketingRS = ReservationModuleUtils.getChargeBD()
							.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTktRevRQ);

					BigDecimal paxServiceTaxForPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (serviceTaxForTicketingRS != null && serviceTaxForTicketingRS.getPaxWiseServiceTaxes() != null
							&& !serviceTaxForTicketingRS.getPaxWiseServiceTaxes().isEmpty()) {

						for (ServiceTaxDTO serviceTaxDTO : serviceTaxForTicketingRS.getPaxWiseServiceTaxes()
								.get(reservationPax.getPaxSequence())) {
							ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(serviceTaxDTO.getChargeGroupCode(),
									serviceTaxDTO.getAmount(), new Date(), AccelAeroCalculator.getDefaultBigDecimalZero(),
									AccelAeroCalculator.getDefaultBigDecimalZero(), serviceTaxDTO.getChargeCode(), null);
							pnrChargesDTO.getServiceTaxPenaltyCharges().add(metaTO);
							paxServiceTaxForPenalty = AccelAeroCalculator.add(paxServiceTaxForPenalty, serviceTaxDTO.getAmount());

						}
					}

					if (isOnlyCancellation) {
						penaltyCharge = AccelAeroCalculator
								.scaleValueDefault(AccelAeroCalculator.subtract(penaltyCharge, paxServiceTaxForPenalty));
					}

				}
				pnrChargesDTO.setModificationPenalty(penaltyCharge);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Converts the collection of ReservationPaxOndCharge to collection of ChargeMetaTO
	 * 
	 * @param charges
	 * @param mapChgTO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ChargeMetaTO> convert(Collection<ReservationPaxOndCharge> charges, Map<Integer, ChargeTO> mapChgTO)
			throws ModuleException {
		Collection<ChargeMetaTO> colChargeMetaTO = new ArrayList<ChargeMetaTO>();

		for (ReservationPaxOndCharge reservationPaxOndCharge : charges) {
			if (loadMetaAmounts) {
				colChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, mapChgTO));
			}
		}

		return colChargeMetaTO;
	}

	/**
	 * Return the identified modification charge
	 * 
	 * @param modificationCharge
	 * @param colReservationTnx
	 * @param hasExternalPayments
	 * @return
	 */
	private BigDecimal getIdentifiedModificationCharge(BigDecimal modificationCharge,
			Collection<ReservationTnx> colReservationTnx, boolean hasExternalPayments) {
		BigDecimal identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		// If any payment exist modification charge is the default charge
		if (hasExternalPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			identifiedModificationCharge = modificationCharge;
		}
		// If no payment exist modification charge is zero
		else {
			identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		return identifiedModificationCharge;
	}

	private FeeHolder
			getFeeHolderForModification(BigDecimal modificationCharge, ReservationPaxFare reservationPaxFare,
					Collection<ReservationTnx> colReservationTnx, boolean hasExternalPayments, ServiceFeeBO serviceFeeBO,
					Integer pnrPaxId) throws ModuleException {
		BigDecimal identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		// If any payment exist modification charge is the default charge
		if (hasExternalPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			identifiedModificationCharge = modificationCharge;
		}
		// If no payment exist modification charge is zero
		else {
			identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		FeeHolder fee = null;
		if (serviceFeeBO.isExtraFeeApplicable()) {
			String cabinClassCode = ReservationCoreUtils.getCabinClassFromPaxFare(reservationPaxFare);
			fee = serviceFeeBO.getFeeHolder(identifiedModificationCharge, null, ReservationInternalConstants.ChargeGroup.MOD,
					cabinClassCode, pnrPaxId);
		} else {
			fee = new FeeHolder(identifiedModificationCharge);
		}
		return fee;
	}

	/**
	 * Get payment information
	 * 
	 * @param pnrPaxId
	 * @return
	 */
	private Collection<ReservationTnx> getPaymentInformation(Integer pnrPaxId) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getPNRPaxPayments(pnrPaxId.toString());
	}

	/**
	 * Charges Summer
	 * 
	 * @param pnrChargesDTO
	 * @param perPaxOndAmounts
	 * @return
	 */
	private PnrChargesDTO chargesSummer(PnrChargesDTO pnrChargesDTO, PnrChargesDTO perPaxOndAmounts) {
		PnrChargesDTO pnrChargesDTOHolder = new PnrChargesDTO();

		// Setting the Pnr Number
		pnrChargesDTOHolder.setPnr(perPaxOndAmounts.getPnr());

		// Setting the Pnr Pax id
		pnrChargesDTOHolder.setPnrPaxId(perPaxOndAmounts.getPnrPaxId());

		// Setting the passenger type
		pnrChargesDTOHolder.setPaxType(perPaxOndAmounts.getPaxType());

		// Setting the available credit amount
		pnrChargesDTOHolder.setAvailableBalance(perPaxOndAmounts.getAvailableBalance());

		// Setting the actual cancellation amount
		pnrChargesDTOHolder.setActualCancellationAmount(AccelAeroCalculator.add(pnrChargesDTO.getActualCancellationAmount(),
				perPaxOndAmounts.getActualCancellationAmount()));

		// Setting the actual modification amount
		pnrChargesDTOHolder.setActualModificationAmount(AccelAeroCalculator.add(pnrChargesDTO.getActualModificationAmount(),
				perPaxOndAmounts.getActualModificationAmount()));

		// Setting the identified cancellation amount
		pnrChargesDTOHolder.setIdentifiedCancellationAmount(AccelAeroCalculator
				.add(pnrChargesDTO.getIdentifiedCancellationAmount(), perPaxOndAmounts.getIdentifiedCancellationAmount()));

		// Setting the identified modification amount
		pnrChargesDTOHolder.setIdentifiedModificationAmount(AccelAeroCalculator
				.add(pnrChargesDTO.getIdentifiedModificationAmount(), perPaxOndAmounts.getIdentifiedModificationAmount()));

		// Setting the identified extra fees amount
		pnrChargesDTOHolder.setIdentifiedExtraFeeAmount(AccelAeroCalculator.add(pnrChargesDTO.getIdentifiedExtraFeeAmount(),
				perPaxOndAmounts.getIdentifiedExtraFeeAmount()));

		// Setting the refundable amounts
		pnrChargesDTOHolder.setRefundableAmounts(ReservationApiUtils.getUpdatedTotals(pnrChargesDTO.getRefundableAmounts(),
				perPaxOndAmounts.getRefundableAmounts(), true));

		// Setting the actual amounts
		pnrChargesDTOHolder.setActualAmounts(ReservationApiUtils.getUpdatedTotals(pnrChargesDTO.getActualAmounts(),
				perPaxOndAmounts.getActualAmounts(), true));

		// Setting the payment information
		pnrChargesDTOHolder.addPaymentInfo(pnrChargesDTO.getPaymentInfo(), perPaxOndAmounts.getPaymentInfo());

		// Setting the Refundable charge meta amounts
		pnrChargesDTOHolder.addRefundableChargeMetaAmounts(pnrChargesDTO.getRefundableChargeMetaAmounts(),
				perPaxOndAmounts.getRefundableChargeMetaAmounts());

		// Setting the non refundable charge meta amounts
		pnrChargesDTOHolder.addNonRefundableChargeMetaAmounts(pnrChargesDTO.getNonRefundableChargeMetaAmounts(),
				perPaxOndAmounts.getNonRefundableChargeMetaAmounts());

		// Setting the actual charge meta amounts
		pnrChargesDTOHolder.addActualChargeMetaAmounts(pnrChargesDTO.getActualChargeMetaAmounts(),
				perPaxOndAmounts.getActualChargeMetaAmounts());

		// setting the is parent or not
		pnrChargesDTOHolder.setParent(perPaxOndAmounts.isParent());

		pnrChargesDTOHolder.setModChargeDetailTO(perPaxOndAmounts.getModChargeDetailTO());

		pnrChargesDTOHolder.setCnxChargeDetailTO(perPaxOndAmounts.getCnxChargeDetailTO());

		pnrChargesDTOHolder.setNameChangeChargeAmount(
				AccelAeroCalculator.add(pnrChargesDTO.getNameChangeChargeAmount(), perPaxOndAmounts.getNameChangeChargeAmount()));
		pnrChargesDTOHolder.setNameChangeChargeDetailTO(pnrChargesDTO.getNameChangeChargeDetailTO());

		pnrChargesDTOHolder.setModificationPenalty(
				AccelAeroCalculator.add(pnrChargesDTO.getModificationPenalty(), perPaxOndAmounts.getModificationPenalty()));

		pnrChargesDTOHolder.addExtraFeeChargeMetaAmounts(pnrChargesDTO.getExtraFeeChargeMetaAmounts(),
				perPaxOndAmounts.getExtraFeeChargeMetaAmounts());

		return pnrChargesDTOHolder;
	}

	/**
	 * Check Constraints
	 * 
	 * @param reservation
	 * @param perOndSegmentIds
	 * @param version
	 * @throws ModuleException
	 */
	private void checkConstraints(Reservation reservation, Collection<Integer> perOndSegmentIds, long version)
			throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);

		// Checking to see if any restricted fare exist
		ValidationUtils.checkRestrictedSegmentConstraints(reservation, perOndSegmentIds,
				"airreservations.cancellation.restrictedFareSegment");
	}

	/**
	 * Return reservation charges for all the passengers
	 * 
	 * @param pnr
	 * @param version
	 * @param hasExternalPayments
	 * @param includeFlown
	 * @param isVoidOperation
	 * @param autoCancelingPnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getPnrCharges(String pnr, long version, CustomChargesTO customChargesTO,
			boolean hasExternalPayments, boolean includeFlown, boolean applyCnxCharges, boolean isVoidOperation,
			List<Integer> autoCancelingPnrSegIds) throws ModuleException {
		// Loads the reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Check Constraints
		checkConstraints(reservation, null, version);

		// Get the flown reservation segment ids
		Map<Integer, Collection<Integer>> paxFlownPnrSegIds = new HashMap<Integer, Collection<Integer>>();
		if (!includeFlown) {
			paxFlownPnrSegIds = this.getPaxWiseFlownPnrSegmentIds(reservation);
		}

		// Capture inverse pnr segment ids
		Map<Collection<Integer>, Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds = ReservationApiUtils
				.getInverseSegments(reservation, false);

		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPax> itrReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFare reservationPaxFareEffected;
		// ReservationPaxFare reservationPaxFareGroundSegments;
		PnrChargesDTO pnrChargesDTO;
		Collection<Integer> colSetInversePnrSegIds;
		Collection<Integer> colSetPnrSegIds;
		Collection<Integer> colProcessedResPaxFareId;
		Collection<PnrChargesDTO> colPnrChargesDTO = new ArrayList<PnrChargesDTO>();
		boolean isInfantPaymentSeparated = reservation.isInfantPaymentRecordedWithInfant();
		while (itrReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itrReservationPax.next();

			pnrChargesDTO = new PnrChargesDTO();
			pnrChargesDTO.setPnrPaxId(reservationPax.getPnrPaxId());

			colProcessedResPaxFareId = new ArrayList<Integer>();
			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = itReservationPaxFare.next();

				// if exists, when calculating balance summary for remove infant auto canceling segments will be skipped
				// to avoid considering balance to pay for auto canceling segment twice
				if (autoCancelingPnrSegIds != null && skipAutoCancellingPnrSegment(autoCancelingPnrSegIds, reservationPaxFare)) {
					continue;
				}

				// Get only the confirmed segments
				if (this.getConfirmedSegments(reservationPaxFare, paxFlownPnrSegIds.get(reservationPax.getPnrPaxId()),
						includeFlown)) {
					colSetPnrSegIds = ReservationCoreUtils.getPnrSegIds(reservationPaxFare);
					colSetInversePnrSegIds = ReservationApiUtils.getInversePnrSegIds(mapSetPSegIdsAndSetInvPSegIds,
							colSetPnrSegIds);

					// This means effected reservation pax fare exist
					if (colSetInversePnrSegIds.size() > 0) {
						reservationPaxFareEffected = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
								colSetInversePnrSegIds);

						if ((!colProcessedResPaxFareId.contains(reservationPaxFare.getPnrPaxFareId())
								&& (!colProcessedResPaxFareId.contains(reservationPaxFareEffected.getPnrPaxFareId())))) {
							// Adjust fare among ond(s)
							CancellationUtils.adjustFareAmongOnds(reservationPaxFare, reservationPaxFareEffected, null, null,
									null, false);
							colProcessedResPaxFareId.add(reservationPaxFare.getPnrPaxFareId());
							colProcessedResPaxFareId.add(reservationPaxFareEffected.getPnrPaxFareId());
						}
					}

					pnrChargesDTO = this.chargesSummer(pnrChargesDTO,
							this.getPassengerOndAmounts(reservationPax, reservationPaxFare, applyCnxCharges, false,
									customChargesTO, hasExternalPayments, false, isVoidOperation, null, false, 0, false,
									isInfantPaymentSeparated));
				}
			}

			colPnrChargesDTO.add(pnrChargesDTO);
		}

		return colPnrChargesDTO;
	}

	/**
	 * Returns the flown pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private Map<Integer, Collection<Integer>> getPaxWiseFlownPnrSegmentIds(Reservation reservation) {
		return CancellationUtils.getPaxWiseFlownPnrSegments(reservation);
	}

	/**
	 * Return confirmed segments
	 * 
	 * @param reservationPaxFare
	 * @param colFlownPnrSegIds
	 * @param includeFlown
	 * @return
	 */
	private boolean getConfirmedSegments(ReservationPaxFare reservationPaxFare, Collection<Integer> colFlownPnrSegIds,
			boolean includeFlown) {
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegments = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;
		int i = 0;

		while (itReservationPaxFareSegments.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegments.next();

			// Checking to see if the segment is confirmed
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
					.equals(reservationPaxFareSegment.getSegment().getStatus())) {
				// Checking to see if the segment is not flown
				if (includeFlown
						|| (colFlownPnrSegIds != null && !colFlownPnrSegIds.contains(reservationPaxFareSegment.getPnrSegId()))) {
					i++;
				}
			}
		}

		if (reservationPaxFare.getPaxFareSegments().size() == i) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return ond charges for all the passengers for all applicable segments
	 * 
	 * @param pnr
	 * @param perOndSegmentIds
	 * @param version
	 * @param hasExternalPayments
	 * @param isRequote
	 * @param ondFares
	 * @param skipPenaltyCharges
	 * @param isVoidOperation
	 * @param ondFareTypeByFareIdMap
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getOndChargesForMultipleSegments(String pnr, Collection<Integer> perOndSegmentIds,
			Collection<Integer> orderdNewFlgSegIds, boolean isForModification, boolean enableModifyProrating,
			boolean applyCnxCharge, boolean applyModCharge, long version, CustomChargesTO customChargesTO,
			boolean hasExternalPayments, boolean isRequote, Collection<OndFareDTO> ondFares, boolean skipPenaltyCharges,
			boolean isVoidOperation, Map<Integer, String> ondFareTypeByFareIdMap) throws ModuleException {
		FlownAssitUnit fau = new FlownAssitUnit(pnr, perOndSegmentIds, orderdNewFlgSegIds, isRequote, isForModification, ondFares,
				skipPenaltyCharges, ondFareTypeByFareIdMap);

		Map<Integer, FlightCOSDTO> newFltCOSMap = isSameFlightCOSUpgrade(fau.getReservation(), ondFares, orderdNewFlgSegIds);

		ChargeAssitUnit cau = new ChargeAssitUnit(fau.getReservation(), perOndSegmentIds, orderdNewFlgSegIds, isRequote,
				isForModification, applyCnxCharge, applyModCharge, version, isVoidOperation, newFltCOSMap);
		return getOndChargesForMultipleSegments(cau, fau, customChargesTO, hasExternalPayments);
	}

	/**
	 * Return ond charges for all the passengers for all applicable segments
	 * 
	 * @param hasExternalPayments
	 * @param fau
	 * @param pnr
	 * @param perOndSegmentIds
	 * @param version
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private Collection<PnrChargesDTO> getOndChargesForMultipleSegments(ChargeAssitUnit chargeAsstUnit,
			FlownAssitUnit flownAsstUnit, CustomChargesTO customChargesTO, boolean hasExternalPayments) throws ModuleException {

		// validate for any condition violation
		chargeAsstUnit.validate();

		boolean isVoidReservation = false;
		boolean isSameFlightsModification = chargeAsstUnit.isSameFlightModification();

		Iterator<ReservationPax> itrReservationPax = chargeAsstUnit.getReservation().getPassengers().iterator();
		ReservationPax reservationPax;
		List<ReservationPaxFare> reservationPaxFareTarget = null;
		List<ReservationPaxFare> reservationPaxFareOpenRT = null;
		List<ReservationPaxFare> reservationPaxFareEffected;
		List<ReservationPaxFare> reservationPaxFareGroundSegments = null;
		List<ReservationPaxFare> reservationPaxFareExchanged = null;

		Map<Integer, PnrChargesDTO> mapPaxONDCharges = new HashMap<Integer, PnrChargesDTO>();
		boolean overideCustomCharges = false;
		while (itrReservationPax.hasNext()) {
			reservationPax = itrReservationPax.next();
			reservationPaxFareTarget = chargeAsstUnit.getTargetPaxFare(reservationPax.getPnrPaxFares());
			reservationPaxFareEffected = chargeAsstUnit.getAffectedPaxFare(reservationPax.getPnrPaxFares());
			chargeAsstUnit.adjustOndFares(reservationPaxFareTarget, reservationPaxFareEffected);
		}
		boolean isInfantPaymentSeparated = chargeAsstUnit.getReservation().isInfantPaymentRecordedWithInfant();
		itrReservationPax = chargeAsstUnit.getReservation().getPassengers().iterator();

		while (itrReservationPax.hasNext()) {
			reservationPax = itrReservationPax.next();

			// Either no inverse segment exists for the selected segment OR Inverse segment exists but it should be
			// cancelled
			reservationPaxFareTarget = chargeAsstUnit.getTargetPaxFare(reservationPax.getPnrPaxFares());
			reservationPaxFareGroundSegments = chargeAsstUnit.getLinkedGroundFareSegments(reservationPax.getPnrPaxFares());
			reservationPaxFareOpenRT = chargeAsstUnit.getAffectedOpenRTPaxFare(reservationPax.getPnrPaxFares());
			reservationPaxFareEffected = chargeAsstUnit.getAffectedPaxFare(reservationPax.getPnrPaxFares());
			reservationPaxFareExchanged = chargeAsstUnit.getExchangedPaxFare(reservationPax.getPnrPaxFares());

			if (reservationPaxFareGroundSegments != null && !reservationPaxFareGroundSegments.isEmpty()
					&& reservationPaxFareExchanged != null && !reservationPaxFareExchanged.isEmpty()) {
				for (ReservationPaxFare exchanged : reservationPaxFareExchanged) {
					Iterator<ReservationPaxFare> groundFareSegIterator = reservationPaxFareGroundSegments.iterator();
					while (groundFareSegIterator.hasNext()) {
						ReservationPaxFare groundFareSeg = groundFareSegIterator.next();
						if (groundFareSeg.getPnrPaxFareId().equals(exchanged.getPnrPaxFareId())) {
							groundFareSegIterator.remove();
						}
					}

				}
			}

			chargeAsstUnit.adjustOndFares(reservationPaxFareTarget, reservationPaxFareEffected);

			if (!overideCustomCharges && reservationPaxFareTarget != null && reservationPaxFareTarget.size() > 0) {
				FeeBO.calculateCustomChargesONDWise(customChargesTO, reservationPaxFareTarget.size());
				overideCustomCharges = true;
			}

			addPassengerOndCharges(reservationPax, mapPaxONDCharges, reservationPaxFareTarget, chargeAsstUnit.isApplyCnxCharge(),
					chargeAsstUnit.isApplyModCharge(), customChargesTO, hasExternalPayments, isSameFlightsModification,
					isVoidReservation, flownAsstUnit, false, false, isInfantPaymentSeparated);
			addPassengerOndCharges(reservationPax, mapPaxONDCharges, reservationPaxFareOpenRT, false, false, null,
					hasExternalPayments, isSameFlightsModification, isVoidReservation, flownAsstUnit, false, false,
					isInfantPaymentSeparated);
			addPassengerOndCharges(reservationPax, mapPaxONDCharges, reservationPaxFareGroundSegments,
					chargeAsstUnit.isApplyCnxCharge(), chargeAsstUnit.isApplyModCharge(), null, hasExternalPayments,
					isSameFlightsModification, isVoidReservation, flownAsstUnit, false, false, isInfantPaymentSeparated);
			addPassengerOndCharges(reservationPax, mapPaxONDCharges, reservationPaxFareExchanged, false, false, null,
					hasExternalPayments, isSameFlightsModification, isVoidReservation, flownAsstUnit, true, false,
					isInfantPaymentSeparated);
		}

		return new ArrayList<PnrChargesDTO>(mapPaxONDCharges.values());
	}

	/**
	 * Add passenger ond charges
	 * 
	 * @param reservationPax
	 * @param mapPaxONDCharges
	 * @param reservationPaxFares
	 * @param hasExternalPayments
	 * @param flownAsstUnit
	 * @param isExchanged
	 * @param isFromNameChange
	 * @param isInfantPaymentSeparated
	 *            TODO
	 * @param showCnxCharge
	 * @param showModCharge
	 * @throws ModuleException
	 */
	private void addPassengerOndCharges(ReservationPax reservationPax, Map<Integer, PnrChargesDTO> mapPaxONDCharges,
			List<ReservationPaxFare> reservationPaxFares, boolean applyCnxCharge, boolean applyModCharge,
			CustomChargesTO customChargesTO, boolean hasExternalPayments, boolean isSameFlightsModification,
			boolean isVoidReservation, FlownAssitUnit flownAsstUnit, boolean isExchanged, boolean isFromNameChange,
			boolean isInfantPaymentSeparated) throws ModuleException {

		if (reservationPaxFares != null) {
			int count = 0;
			for (ReservationPaxFare reservationPaxFare : reservationPaxFares) {
				PnrChargesDTO pnrChargesDTO = this.getPassengerOndAmounts(reservationPax, reservationPaxFare, applyCnxCharge,
						applyModCharge, customChargesTO, hasExternalPayments, isSameFlightsModification, isVoidReservation,
						flownAsstUnit, isExchanged, count, isFromNameChange, isInfantPaymentSeparated);
				composePnrCharges(reservationPax, mapPaxONDCharges, pnrChargesDTO);
				count++;
			}
		}
	}

	/**
	 * Compose Pnr Charges
	 * 
	 * @param reservationPax
	 * @param mapPaxONDCharges
	 * @param pnrChargesDTO
	 */
	private void composePnrCharges(ReservationPax reservationPax, Map<Integer, PnrChargesDTO> mapPaxONDCharges,
			PnrChargesDTO pnrChargesDTO) {
		PnrChargesDTO pnrChargesDTOTarget = (PnrChargesDTO) mapPaxONDCharges.get(reservationPax.getPnrPaxId());

		if (pnrChargesDTOTarget != null) {
			pnrChargesDTOTarget = chargesSummer(pnrChargesDTOTarget, pnrChargesDTO);
			mapPaxONDCharges.put(reservationPax.getPnrPaxId(), pnrChargesDTOTarget);
		} else {
			mapPaxONDCharges.put(reservationPax.getPnrPaxId(), pnrChargesDTO);
		}
	}

	private boolean skipAutoCancellingPnrSegment(List<Integer> autoCancellingPnrSegId, ReservationPaxFare reservationPaxFare) {
		for (ReservationPaxFareSegment resPaxSeg : reservationPaxFare.getPaxFareSegments()) {
			if (autoCancellingPnrSegId.contains(resPaxSeg.getPnrSegId())) {
				return true;
			}
		}
		return false;
	}

	private BigDecimal getEffectiveCancellationCharge(CustomChargesTO customChargesTO, BigDecimal cnxCharge, String paxType) {

		BigDecimal effectiveCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (ReservationInternalConstants.PassengerType.PARENT.equals(paxType)) {
			if (customChargesTO.getExistingModificationChargeAdult() == null
					&& customChargesTO.getExistingModificationChargeInfant() == null) {
				effectiveCnxCharge = cnxCharge;
			} else {
				BigDecimal parentExistingModCharge = AccelAeroCalculator.add(customChargesTO.getExistingModificationChargeAdult(),
						customChargesTO.getExistingModificationChargeInfant());

				if (cnxCharge.compareTo(parentExistingModCharge) == 1) {
					effectiveCnxCharge = AccelAeroCalculator.subtract(cnxCharge, parentExistingModCharge);
				}
			}
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			BigDecimal existingModCharge = null;
			existingModCharge = customChargesTO.getExistingModificationChargeInfant();

			if (existingModCharge == null) {
				effectiveCnxCharge = cnxCharge;
			} else if (cnxCharge != null && cnxCharge.compareTo(existingModCharge) == 1) {
				effectiveCnxCharge = AccelAeroCalculator.subtract(cnxCharge, existingModCharge);
			}
		} else {
			BigDecimal existingModCharge = null;
			if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
				existingModCharge = customChargesTO.getExistingModificationChargeAdult();
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
				existingModCharge = customChargesTO.getExistingModificationChargeChild();
			}

			if (existingModCharge == null) {
				effectiveCnxCharge = cnxCharge;
			} else if (cnxCharge.compareTo(existingModCharge) == 1) {
				effectiveCnxCharge = AccelAeroCalculator.subtract(cnxCharge, existingModCharge);
			}
		}

		return effectiveCnxCharge;
	}

	public Map<Integer, FlightCOSDTO> isSameFlightCOSUpgrade(Reservation reservation, Collection<OndFareDTO> ondFares,
			Collection<Integer> orderdNewFlgSegIds) throws ModuleException {

		Map<Integer, FlightCOSDTO> newfltCOSMap = new HashMap<Integer, FlightCOSDTO>();

		Map<String, Integer> cosInfoMap = ReservationModuleUtils.getFlightInventoryBD().getAvailableCabinClassesInfo();

		if (orderdNewFlgSegIds != null && orderdNewFlgSegIds.size() > 0 && cosInfoMap != null && cosInfoMap.size() > 1) {
			Iterator<Integer> newFlgSegIdsItr = orderdNewFlgSegIds.iterator();

			while (newFlgSegIdsItr.hasNext()) {

				String oldCCCode = null;
				String newCCCode = null;
				boolean isCOSUpgrad = false;
				Integer newFltSegId = newFlgSegIdsItr.next();

				if (reservation != null && reservation.getSegments() != null && reservation.getSegments().size() > 0) {
					Iterator<ReservationSegment> resSegmentItr = reservation.getSegments().iterator();

					while (resSegmentItr.hasNext()) {
						ReservationSegment resSegment = resSegmentItr.next();
						if (newFltSegId.intValue() == resSegment.getFlightSegId().intValue()) {
							oldCCCode = resSegment.getCabinClassCode();
							break;
						}
					}

				}

				if (ondFares != null && ondFares.size() > 0) {
					Iterator<OndFareDTO> ondFaresItr = ondFares.iterator();
					while (ondFaresItr.hasNext()) {
						OndFareDTO ondFareDTO = ondFaresItr.next();
						if (ondFareDTO.getSegmentSeatDistsDTO() != null && ondFareDTO.getSegmentSeatDistsDTO().size() > 0) {
							Iterator<SegmentSeatDistsDTO> segSeatDistsDTOItr = ondFareDTO.getSegmentSeatDistsDTO().iterator();

							while (segSeatDistsDTOItr.hasNext()) {
								SegmentSeatDistsDTO segSeatDistsDTO = segSeatDistsDTOItr.next();

								if (newFltSegId.intValue() == segSeatDistsDTO.getFlightSegId()) {
									newCCCode = segSeatDistsDTO.getCabinClassCode();
									break;
								}
							}

						}
					}
				}
				if (oldCCCode != null && newCCCode != null && cosInfoMap.get(oldCCCode) >= cosInfoMap.get(newCCCode)) {
					isCOSUpgrad = true;
				}

				FlightCOSDTO flightCOSDTO = new FlightCOSDTO();
				flightCOSDTO.setFlightSegId(newFltSegId);
				flightCOSDTO.setCOSUpperOrSame(isCOSUpgrad);
				flightCOSDTO.setOldCabinClassCode(oldCCCode);
				flightCOSDTO.setNewCabinClassCode(newCCCode);

				if (oldCCCode != null)
					flightCOSDTO.setOldRanking(cosInfoMap.get(oldCCCode));

				if (newCCCode != null)
					flightCOSDTO.setNewRanking(cosInfoMap.get(newCCCode));

				newfltCOSMap.put(newFltSegId, flightCOSDTO);
			}

		}

		return newfltCOSMap;
	}

	public List<ExternalChgDTO> getOndChargesForNameChange(ReservationPax reservationPax,
			List<ReservationPaxFare> reservationPaxFareTarget, Map<Integer, List<Integer>> fareIdFltSegIdMap,
			boolean isInfantPaymentSeparated) throws ModuleException {

		Map<Integer, PnrChargesDTO> mapPaxONDCharges = new HashMap<Integer, PnrChargesDTO>();
		boolean isFromNameChange = true;

		addPassengerOndCharges(reservationPax, mapPaxONDCharges, reservationPaxFareTarget, false, false, null, false, false,
				false, null, false, isFromNameChange, isInfantPaymentSeparated);

		Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
		EXTERNAL_CHARGES externalChgType = EXTERNAL_CHARGES.NAME_CHANGE_CHARGE;
		String chargeCode = BeanUtils.nullHandler(externalChargesMap.get(externalChgType.name()));

		List<ExternalChgDTO> paxExternalChgDTOs = new ArrayList<ExternalChgDTO>();

		for (Integer pnrPaxId : mapPaxONDCharges.keySet()) {
			PnrChargesDTO pnrChargesDTO = mapPaxONDCharges.get(pnrPaxId);
			if (pnrChargesDTO.getNameChangeChargeAmount().doubleValue() > 0) {
				QuotedChargeDTO quotedChargeDTO = ReservationModuleUtils.getChargeBD().getCharge(chargeCode,
						CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);

				quotedChargeDTO.setChargeValuePercentage(pnrChargesDTO.getNameChangeChargeAmount().doubleValue());
				NameChangeExtChgDTO nameChangeExtChgDTO = new NameChangeExtChgDTO();
				ReservationApiUtils.getExternalChgDTO(quotedChargeDTO.getChargeCode(), quotedChargeDTO.getChargeDescription(),
						quotedChargeDTO.getChargeRateId(), externalChgType, quotedChargeDTO.isChargeValueInPercentage(),
						new BigDecimal(Double.toString(quotedChargeDTO.getChargeValuePercentage())), nameChangeExtChgDTO,
						quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint());
				nameChangeExtChgDTO.setAmount(pnrChargesDTO.getNameChangeChargeAmount());
				nameChangeExtChgDTO.setFlightSegId(
						getCorrectFltSegId(pnrChargesDTO.getNameChangeChargeDetailTO().getFareID(), fareIdFltSegIdMap));
				paxExternalChgDTOs.add(nameChangeExtChgDTO);
			}
		}

		return paxExternalChgDTOs;
	}

	private Integer getCorrectFltSegId(Integer fareID, Map<Integer, List<Integer>> fareIdFltSegIdMap) {
		Integer fltSegId = null;
		if (fareIdFltSegIdMap.containsKey(fareID)) {
			Iterator<Integer> fltSegIdIte = fareIdFltSegIdMap.get(fareID).iterator();
			while (fltSegIdIte.hasNext()) {
				fltSegId = fltSegIdIte.next();
				fltSegIdIte.remove();
				break;
			}
		}

		return fltSegId;

	}
}