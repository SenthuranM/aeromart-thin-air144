package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.base.BaseObseverActionTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ObserverActionDataContext;
import com.isa.thinair.commons.api.exception.ModuleException;

public class DelAirportMessagePaxTemplate extends BaseObseverActionTemplate<ObserverActionDataContext> {

	public DelAirportMessagePaxTemplate() {
		super();
	}

	@Override
	public void executeTemplateAction(ObserverActionDataContext dataContext) throws ModuleException {

		Map<Integer, Set<Integer>> delMap = dataContext.getDelMap();

		// del MAP remove with existing data
		checkForExist(delMap, PNLConstants.AdlActions.D, dataContext.getOldReservation().getPnr());

		List<AirportMessagePassenger> paxList = createUnexistPax(delMap, dataContext.getOldReservation(),
				PNLConstants.AdlActions.D.toString());

		paxList.addAll(alreadyExistpaxListToUpdate);
		
		if(!paxList.isEmpty()){
			auxilliaryDAO.saveAirportMessagePassenger(paxList);
		}
	}

}
