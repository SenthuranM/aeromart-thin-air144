/*
 * ==============================================================================

 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.aig.INSChargeTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * Command for removing insurance
 * 
 * @since 1.0
 * @isa.module.command name="removeInsurance"
 * 
 */
public class RemoveInsurance extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(RemoveInsurance.class);

	/**
	 * Execute method of the RemoveInsurance command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside RemoveInsurance execute");

		DefaultServiceResponse serRes = new DefaultServiceResponse(true);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		try {
			// Getting command parameters
			String pnr = (String) this.getParameter(CommandParamNames.PNR);
			CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
			Collection<Integer> pnrSegmentIds = (Collection<Integer>) this.getParameter(CommandParamNames.PNR_SEGMENT_IDS);
			List<InsuranceRequestAssembler> insuranceReqs = (List<InsuranceRequestAssembler>) this
					.getParameter(CommandParamNames.INSURANCE_INFO);
			Boolean isSplitPax = (Boolean) this.getParameter(CommandParamNames.IS_PAX_SPLIT);
			Collection<Integer> oldSegmentIds = (Collection<Integer>) this.getParameter(CommandParamNames.PNR_SEGMENT_IDS);
			Reservation reservation = null;
			if (pnr != null) {
				// Retrieves the Reservation
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
				pnrModesDTO.setLoadSegView(true);
				reservation = ReservationProxy.getReservation(pnrModesDTO);
			}
			if (reservation != null && reservation.getReservationInsurance() != null
					&& !reservation.getReservationInsurance().isEmpty()) {
				List<ReservationInsurance> listResInsurance = reservation.getReservationInsurance();

				for (ReservationInsurance resInsurance : listResInsurance) {

					if (ReservationInternalConstants.INSURANCE_STATES.OH.toString().equals(resInsurance.getState())) {
						Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
						colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);

						Map<EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationBO
								.getQuotedExternalCharges(colEXTERNAL_CHARGES, credentialsDTO.getAgentCode(), null);
						ExternalChgDTO externalChgDTO = (ExternalChgDTO) ((ExternalChgDTO) mapExternalChgs
								.get(EXTERNAL_CHARGES.INSURANCE)).clone();

						Set<ReservationPax> passengers = reservation.getPassengers();
						Collection<Integer> colpnrPaxId = new ArrayList<Integer>();

						for (Iterator<ReservationPax> itPaxs = passengers.iterator(); itPaxs.hasNext();) {
							ReservationPax pax = (ReservationPax) itPaxs.next();
							colpnrPaxId.add(pax.getPnrPaxId());
						}

						boolean isRefundableCharge = isInsuranceChargeRefundable(reservation, externalChgDTO.getChargeCode());

						AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);
						ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation, false);

						if (isRefundableCharge) {
							Collection<INSChargeTO> colInsDtos = ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO
									.getInsuranceCharges(colpnrPaxId, externalChgDTO.getChgRateId());
							if (colInsDtos != null && !colInsDtos.isEmpty()) {
								for (INSChargeTO insDto : colInsDtos) {
									ReservationBO.adjustCreditManual(adjustCreditBO, reservation, insDto.getPnrPaxFareId(),
											insDto.getChargeRateId(), insDto.getAmount().negate(), insDto.getChargeGroup(),
											"Remove Insurance", credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxGen);
									// ReservationBO.adjustCreditManual(adjustCreditBO, reservation,
									// insDto.getPnrPaxFareId(),
									// insDto.getChargeRateId(), insDto.getAmount(), insDto.getChargeGroup(),
									// "Remove Insurance",
									// credentialsDTO.getTrackInfoDTO(), credentialsDTO);
								}
							}
						}

						reservation.setInsuranceId(null);
						ReservationProxy.saveReservation(reservation);
						if (isRefundableCharge) {
							adjustCreditBO.callRevenueAccountingAfterReservationSave();
						}

						/** Update reservation insurance with cancel state */
						if (!resInsurance.getState()
								.equalsIgnoreCase(ReservationInternalConstants.INSURANCE_STATES.CX.toString())) {
							resInsurance.setState(ReservationInternalConstants.INSURANCE_STATES.CX.toString());
							ReservationProxy.saveOrUpdateReservationInsurance(resInsurance);
						}
					}

					serRes.addResponceParam(CommandParamNames.VERSION, Long.valueOf(reservation.getVersion()));
				}
			}

			if (reservation != null) {
				if (output == null) {
					output = new DefaultServiceResponse();
					output.addResponceParam(CommandParamNames.VERSION, Long.valueOf(reservation.getVersion()));
				} else {
					output.addResponceParam(CommandParamNames.VERSION, Long.valueOf(reservation.getVersion()));
				}
			}

			serRes.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
			if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
				serRes.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
			}
			if (this.getParameter(CommandParamNames.SELECTED_ANCI_MAP) != null) {
				serRes.addResponceParam(CommandParamNames.SELECTED_ANCI_MAP,
						this.getParameter(CommandParamNames.SELECTED_ANCI_MAP));
			}
		} catch (Exception e) {
			log.error("Error in saving insurance info", e);
		}
		return serRes;
	}

	@Deprecated
	private boolean isWholeReservationCanceled(Reservation reservation, Collection<Integer> pnrSegmentIds) {
		Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator();
		ReservationSegment reservationSegment;
		if (pnrSegmentIds != null && pnrSegmentIds.size() > 0) {
			int i = 0;
			while (itReservationSegment.hasNext()) {
				reservationSegment = (ReservationSegment) itReservationSegment.next();

				if (pnrSegmentIds.contains(reservationSegment.getPnrSegId())) {
					reservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
				}

				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
					i++;
				}
			}

			if (i == reservation.getSegments().size()) {
				return true;
			}
		}
		return false;
	}

	@Deprecated
	private void updateNewONDForCancelSegments(Reservation reservation, Collection<Integer> pnrSegmentIds,
			ReservationInsurance resInsurance, Collection<Integer> oldSegmentIds) throws ModuleException {
		ReservationInsurance cloneReservationInsurance = (ReservationInsurance) resInsurance.clone();
		Collection<ReservationSegmentDTO> colSegDTO = new ArrayList<ReservationSegmentDTO>();
		for (Iterator<ReservationSegmentDTO> iterator = reservation.getSegmentsView().iterator(); iterator.hasNext();) {
			ReservationSegmentDTO reservationSegmentDTO = iterator.next();
			boolean found = false;
			if (oldSegmentIds != null) {
				for (Integer segId : oldSegmentIds) {
					if (reservationSegmentDTO.getPnrSegId().intValue() == segId.intValue())
						found = true;
				}
				if (!found)
					colSegDTO.add(reservationSegmentDTO);
			}
		}

		if (colSegDTO.size() > 0) {
			ReservationSegmentDTO[] outboundInbound = ReservationCoreUtils
					.getOutboundInboundList(new ArrayList<ReservationSegmentDTO>(colSegDTO));
			ReservationSegmentDTO outBoundReservationSegmentDTO = outboundInbound[0];
			ReservationSegmentDTO inBoundReservationSegmentDTO = outboundInbound[1];
			if (outBoundReservationSegmentDTO != null) {
				String roundTrip = "Y";
				String[] inbound = inBoundReservationSegmentDTO.getSegmentCode().split("/");
				Date departureDate = outBoundReservationSegmentDTO.getZuluDepartureDate();
				Date arrivalDate = inBoundReservationSegmentDTO.getZuluArrivalDate();
				cloneReservationInsurance.setOrigin(outBoundReservationSegmentDTO.getSegmentCode().split("/")[0]);
				cloneReservationInsurance.setDateOfTravel(departureDate);
				cloneReservationInsurance.setDestination(inbound[inbound.length - 1]);
				cloneReservationInsurance.setDateOfReturn(arrivalDate);
				long dateDifferenceInMillis = arrivalDate.getTime() - departureDate.getTime();
				long diffrenceInDays = (dateDifferenceInMillis / (1000 * 60 * 60 * 24)) + 1;
				if (diffrenceInDays < 31)
					roundTrip = "N";
				cloneReservationInsurance.setRoundTrip(roundTrip);
				InsuranceResponse insuranceResponse = quoteRakInsurance(reservation, outBoundReservationSegmentDTO,
						inBoundReservationSegmentDTO, (roundTrip == "Y" ? true : false));
				cloneReservationInsurance.setAmount(insuranceResponse.getTotalPremiumAmount());
				cloneReservationInsurance.setQuotedAmount(insuranceResponse.getQuotedTotalPremiumAmount());
				cloneReservationInsurance.setQuotedNetAmount(insuranceResponse.getQuotedTotalPremiumAmount());
				cloneReservationInsurance.setQuotedTaxAmout(insuranceResponse.getQuotedTotalPremiumAmount());
			}
			ReservationProxy.saveOrUpdateReservationInsurance(cloneReservationInsurance);
		}
		resInsurance.setState(ReservationInternalConstants.INSURANCE_STATES.CX.toString());
		ReservationProxy.saveOrUpdateReservationInsurance(resInsurance);
	}

	@Deprecated
	private boolean isModifiedBeforeCutover(Reservation reservation) throws ModuleException {

		Collection<FlightSegmentDTO> flightSegments = ReservationApiUtils.getFlightSegments(reservation);
		int modificationCutover = Integer.parseInt(AppSysParamsUtil.getRakModificationCutoverTime());
		for (FlightSegmentDTO flightSeg : flightSegments) {
			GregorianCalendar calendar2 = new GregorianCalendar();
			calendar2.setTime(new Date(flightSeg.getDepartureDateTimeZulu().getTime()));
			calendar2.add(GregorianCalendar.MINUTE, -modificationCutover);
			// TODO:check whether the segments are ordered
			if (calendar2.getTimeInMillis() >= new Date().getTime()) {
				return true;
			}

		}
		return false;
	}

	private InsuranceResponse quoteRakInsurance(Reservation reservation, ReservationSegmentDTO outBound,
			ReservationSegmentDTO inBound, boolean roundTrip) throws ModuleException {
		IInsuranceRequest insureRequest = new InsuranceRequestAssembler();
		for (int i = 0; i < reservation.getPassengers().size(); i++) {
			InsurePassenger insurePassenger = new InsurePassenger();
			insureRequest.addPassenger(insurePassenger);
		}
		InsureSegment insureSegment = new InsureSegment();
		insureSegment.setDepartureDate(outBound.getZuluDepartureDate());
		insureSegment.setFromAirportCode(outBound.getSegmentCode().split("/")[0]);
		insureSegment.setArrivalDate(inBound.getZuluArrivalDate());
		insureSegment.setRoundTrip(roundTrip);
		insureRequest.addFlightSegment(insureSegment);
		return ReservationModuleUtils.getWSClientBD().quoteRAKInsurancePolicy(insureRequest);
	}

	private boolean isInsuranceChargeRefundable(Reservation reservation, String chargeCode) throws ModuleException {
		Charge charge = ReservationModuleUtils.getChargeBD().getCharge(chargeCode);
		boolean chargeRefundable = (charge.getRefundableChargeFlag() == Charge.REFUNDABLE_CHARGE);
		return (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus()) || chargeRefundable);
	}
}
