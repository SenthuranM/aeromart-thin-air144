/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.PassengerCheckinDTO;
import com.isa.thinair.airreservation.api.dto.revacc.AdjustCreditTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * Implementation of the ReservationTnxDAO
 * 
 * @author Lasantha Pambagoda
 * 
 * @author Isuru
 * @isa.module.dao-impl dao-name="ReservationTnxDAO"
 */
public class ReservationTnxDAOImpl extends PlatformBaseHibernateDaoSupport implements ReservationTnxDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationTnxDAOImpl.class);

	/**
	 * Return reservation passenger balances
	 * 
	 * @param pnrPaxIds
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, BigDecimal> getPNRPaxBalances(Collection<Integer> pnrPaxIds) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		if (pnrPaxIds == null | pnrPaxIds.size() == 0) {
			throw new CommonsDataAccessException("airreservations.arg.invalid.null");
		}

		StringBuilder sqlCondition = new StringBuilder();
		Iterator<Integer> itPnrPaxIds = pnrPaxIds.iterator();
		Integer pnrPaxId;
		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (sqlCondition.length() == 0) {
				sqlCondition.append(" WHERE PNR_PAX_ID = " + pnrPaxId);
			} else {
				sqlCondition.append(" OR PNR_PAX_ID = " + pnrPaxId);
			}
		}

		// Final SQL
		String sql = " SELECT PNR_PAX_ID PAX_ID, SUM(AMOUNT) AMOUNT " + " FROM T_PAX_TRANSACTION " + sqlCondition
				+ " GROUP BY PNR_PAX_ID ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Map<Integer, BigDecimal> mapPaxBalances = (Map<Integer, BigDecimal>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, BigDecimal> mapPaxBalances = new HashMap<Integer, BigDecimal>();

				if (rs != null) {
					while (rs.next()) {
						mapPaxBalances.put(BeanUtils.parseInteger(rs.getInt("PAX_ID")), rs.getBigDecimal("AMOUNT"));
					}
				}

				return mapPaxBalances;
			}
		});

		return mapPaxBalances;
	}

	/**
	 * method to get transaction
	 */
	public ReservationTnx getTransaction(long tnxId) {
		return (ReservationTnx) get(ReservationTnx.class, new Integer((int) tnxId));
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ReservationTnx> getTransactions(Collection<Integer> txnIds) {
		if (txnIds.isEmpty()) {
			/*
			 * If the parameter is an empty list it throws an error instead of returning an empty list. Probably an bug
			 * in hibernate version. So returning an empty result manually.
			 */
			return new ArrayList<ReservationTnx>();
		}

		String hql = "SELECT txn FROM ReservationTnx txn WHERE txn.tnxId IN(:transactionIDs)";
		Query query = getSession().createQuery(hql);
		query.setParameterList("transactionIDs", txnIds);
		return query.list();
	}

	@Override
	public ReservationPaxExtTnx getExternalTransaction(long paxExternalTnxId) {
		ReservationPaxExtTnx paxExtTnx = (ReservationPaxExtTnx) get(ReservationPaxExtTnx.class,
				new Integer((int) paxExternalTnxId));
		return paxExtTnx;
	}

	public Map<Integer, ReservationPaxTnxBreakdown> getReservationPaxTnxBreakdown(Collection<Integer> tnxIds) {
		
		String hql = "select breakdown from ReservationPaxTnxBreakdown breakdown where " + " breakdown.txnId in (:txnId)";
		
		Query query = getSession().createQuery(hql).setParameterList("txnId", tnxIds);
		Collection<ReservationPaxTnxBreakdown> colReservationPaxTnxBreakdown = query.list();
		
		Map<Integer, ReservationPaxTnxBreakdown> map = new HashMap<Integer, ReservationPaxTnxBreakdown>();

		if (colReservationPaxTnxBreakdown != null && !colReservationPaxTnxBreakdown.isEmpty()) {
			Iterator<ReservationPaxTnxBreakdown> itColReservationPaxTnxBreakdown = colReservationPaxTnxBreakdown.iterator();
			ReservationPaxTnxBreakdown reservationPaxTnxBreakdown;

			while (itColReservationPaxTnxBreakdown.hasNext()) {
				reservationPaxTnxBreakdown = (ReservationPaxTnxBreakdown) itColReservationPaxTnxBreakdown.next();
				map.put(reservationPaxTnxBreakdown.getTxnId(), reservationPaxTnxBreakdown);
			}
		}

		return map;
	}

	/**
	 * method to get all transactions
	 */
	public ReservationTnx getLccTransaction(String pnrPaxId, String lccUniqueId) {

		String hql = "select tnx from ReservationTnx tnx " + "where tnx.pnrPaxId = :pnrPaxId and tnx.lccUniqueId = :lccUniqueId";

		Query createQuery = getSession().createQuery(hql);
		createQuery.setParameter("pnrPaxId", pnrPaxId);
		createQuery.setParameter("lccUniqueId", lccUniqueId);

		return (ReservationTnx) createQuery.uniqueResult();
	}

	/**
	 * save reservation transaction
	 */
	public void saveTransaction(ReservationTnx tnx) {
		hibernateSaveOrUpdate(tnx);
	}

	/**
	 * get pnr pax balance for pnt pax id
	 */
	public BigDecimal getPNRPaxBalance(String pnrPaxId) {
		String hql = "select sum(tnx.amount) from ReservationTnx tnx " + "where tnx.pnrPaxId = :pnrPaxId";

		return (BigDecimal) getSession().createQuery(hql).setParameter("pnrPaxId", pnrPaxId).uniqueResult();
	}

	/**
	 * Return expired credits
	 * 
	 * @param date
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	public Collection<AdjustCreditTO> getExpiredCredits(Date date) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		if (date == null) {
			throw new CommonsDataAccessException("airreservations.arg.invalid.null");
		}

		// Final SQL
		String sql = " SELECT pax.PNR, pax.PNR_PAX_ID, credit.BALANCE, credit.DATE_EXP, res.ENABLE_TRANSACTION_GRANULARITY, credit.TXN_ID "
				+ " FROM T_PAX_CREDIT credit, T_PNR_PASSENGER pax, T_RESERVATION res"
				+ " WHERE res.PNR = pax.PNR and pax.PNR_PAX_ID = credit.PNR_PAX_ID AND credit.balance > 0 AND credit.DATE_EXP <= ? ";

		if (log.isDebugEnabled())
			log.debug("SQL to be execute for expire credits " + sql + " upto " + date);

		Collection<AdjustCreditTO> colAdjustCreditDTO = (Collection<AdjustCreditTO>) jt.query(sql, new Object[] { date },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<AdjustCreditTO> colAdjustCreditDTO = new ArrayList<AdjustCreditTO>();
						if (rs != null) {
							while (rs.next()) {
								AdjustCreditTO adjustCreditTO = new AdjustCreditTO();
								adjustCreditTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));
								adjustCreditTO.setAmount(rs.getBigDecimal("BALANCE"));
								adjustCreditTO.setPnrPaxId(rs.getInt("PNR_PAX_ID"));
								adjustCreditTO.setUserNotes("Expire credits by Scheduler");
								adjustCreditTO.setEnableTransactionGranularity(
										rs.getString("ENABLE_TRANSACTION_GRANULARITY").equals("Y") ? true : false);
								adjustCreditTO.setOriginalPaymentTnxID(rs.getInt("TXN_ID"));
								colAdjustCreditDTO.add(adjustCreditTO);
							}
						}
						return colAdjustCreditDTO;
					}
				});
		return colAdjustCreditDTO;
	}

	/**
	 * Return all reservation passenger ids
	 */
	public List<Integer> getAllPnrPaxIds() {
		return find("select distinct tnx.pnrPaxId from ReservationTnx tnx", Integer.class);
	}

	public boolean hasPayments(String pnr) {
		String inSection = BeanUtils.constructINString(ReservationTnxNominalCode.getPaymentTypeNominalCodes());

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT count(*) as count   FROM t_pax_transaction  WHERE pnr_pax_id IN"
				+ "  ( SELECT pnr_pax_id FROM t_pnr_passenger WHERE pnr = ?  ) " + " AND nominal_code IN (" + inSection + ") ";

		int colAdjustCreditDTO = jt.queryForObject(sql, new Object[] { pnr }, Integer.class);
		return colAdjustCreditDTO > 0;
	}

	/**
	 * Return reservation passenger payments
	 */
	public Collection<ReservationTnx> getPNRPaxPayments(String pnrPaxId) {
		String inSection = BeanUtils.constructINString(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes());
		String hql = "select tnx from ReservationTnx tnx where " + " tnx.pnrPaxId = ? and tnx.nominalCode in (" + inSection + ")";

		return find(hql, pnrPaxId, ReservationTnx.class);
	}

	/**
	 * 
	 * 
	 * @param colPassengerCheckinDTO
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<PassengerCheckinDTO> getPNRPaxPendingPayments(Collection<PassengerCheckinDTO> colPassengerCheckinDTO) {
		Collection<Integer> colPnrPaxIds = new ArrayList<Integer>();
		Map<Integer, PassengerCheckinDTO> mapPassengerCheckinDTO = new HashMap<Integer, PassengerCheckinDTO>();
		for (PassengerCheckinDTO passengerCheckinDTO : colPassengerCheckinDTO) {
			colPnrPaxIds.add(passengerCheckinDTO.getPnrPaxId());
			mapPassengerCheckinDTO.put(passengerCheckinDTO.getPnrPaxId(), passengerCheckinDTO);
		}

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		if (colPnrPaxIds == null | colPnrPaxIds.size() == 0) {
			throw new CommonsDataAccessException("airreservations.arg.invalid.null");
		}

		StringBuilder sqlCondition = new StringBuilder();
		Iterator<Integer> itPnrPaxIds = colPnrPaxIds.iterator();
		Integer pnrPaxId;
		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (sqlCondition.length() == 0) {
				sqlCondition.append(" WHERE PNR_PAX_ID = " + pnrPaxId);
			} else {
				sqlCondition.append(" OR PNR_PAX_ID = " + pnrPaxId);
			}
		}

		// Final SQL
		String sql = " SELECT PNR_PAX_ID PAX_ID, SUM(AMOUNT) AMOUNT " + " FROM T_PAX_TRANSACTION " + sqlCondition
				+ " GROUP BY PNR_PAX_ID ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Map<Integer, BigDecimal> mapPaxBalances = (Map<Integer, BigDecimal>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, BigDecimal> mapPaxBalances = new HashMap<Integer, BigDecimal>();

				if (rs != null) {
					while (rs.next()) {
						mapPaxBalances.put(BeanUtils.parseInteger(rs.getInt("PAX_ID")), rs.getBigDecimal("AMOUNT"));
					}
				}

				return mapPaxBalances;
			}
		});

		Iterator<Map.Entry<Integer, BigDecimal>> it = mapPaxBalances.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, BigDecimal> pairs = (Map.Entry<Integer, BigDecimal>) it.next();
			if (mapPassengerCheckinDTO.containsKey(pairs.getKey())) {
				Integer intPnrPaxId = (Integer) pairs.getKey();
				BigDecimal amount = (BigDecimal) pairs.getValue();
				PassengerCheckinDTO passengerCheckinDTO = (PassengerCheckinDTO) mapPassengerCheckinDTO.get(intPnrPaxId);

				if (amount.doubleValue() > 0) {
					passengerCheckinDTO.setUnpaid(true);
				}
			}
		}

		return new ArrayList<PassengerCheckinDTO>(mapPassengerCheckinDTO.values());
	}

	/**
	 * Return pnr passenger payments
	 * 
	 * Key --> pnrPaxId(java.lang.Integer) Value --> Collection of ReservationTnx
	 * 
	 * @param pnrPaxIds
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<ReservationTnx>> getPNRPaxPaymentsAndRefunds(Collection<Integer> pnrPaxIds) {
		String strNorminalCodes = BeanUtils.constructINString(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes());
		String strPnrPaxIds = Util.buildIntegerInClauseContent(pnrPaxIds);

		String hql = "select tnx from ReservationTnx tnx where  tnx.pnrPaxId in ( :pnrPaxIds ) and tnx.nominalCode in ( :nominalCodes ) order by tnx.pnrPaxId, tnx.nominalCode ";

		Map<Integer, Collection<ReservationTnx>> mapPnrPaxIdAndColReservationTnx = new HashMap<Integer, Collection<ReservationTnx>>();

		Query query = getSession().createQuery(hql);
		
		// this is added because when the list is empty hibernate throws an exception.
		// since -1 does not exist for sure for pnrpaxid
		if (pnrPaxIds.isEmpty()) {
			pnrPaxIds.add(-1);
		}
		query.setParameterList("pnrPaxIds", pnrPaxIds, StandardBasicTypes.INTEGER);
		
		query.setParameterList("nominalCodes", ReservationTnxNominalCode.getPaymentAndRefundNominalCodes(),
				StandardBasicTypes.INTEGER);
		Collection<ReservationTnx> colReservationTnx = query.list();

		Iterator<ReservationTnx> itColReservationTnx = colReservationTnx.iterator();
		ReservationTnx reservationTnx;
		Collection<ReservationTnx> colHolder;
		Integer pnrPaxId;

		while (itColReservationTnx.hasNext()) {
			reservationTnx = (ReservationTnx) itColReservationTnx.next();
			pnrPaxId = new Integer(reservationTnx.getPnrPaxId());

			// If pnr pax id exist
			if (mapPnrPaxIdAndColReservationTnx.containsKey(pnrPaxId)) {
				colHolder = (Collection<ReservationTnx>) mapPnrPaxIdAndColReservationTnx.get(pnrPaxId);
				colHolder.add(reservationTnx);
			} else {
				colHolder = new ArrayList<ReservationTnx>();
				colHolder.add(reservationTnx);
				mapPnrPaxIdAndColReservationTnx.put(pnrPaxId, colHolder);
			}
		}

		return mapPnrPaxIdAndColReservationTnx;
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<ReservationTnx>> getPNRPaxRecords(Collection<Integer> pnrPaxIds, List<Integer> nominalRecords) {
		String strNorminalCodes = BeanUtils.constructINString(nominalRecords);
		String strPnrPaxIds = Util.buildIntegerInClauseContent(pnrPaxIds);
		String hql;
		List<ReservationTnx> colReservationTnx = null;
		if(pnrPaxIds !=null && pnrPaxIds.isEmpty()){
			hql = "select tnx from ReservationTnx tnx where " + " tnx.pnrPaxId in (" + strPnrPaxIds
					+ ") and tnx.nominalCode in (" + strNorminalCodes + ") order by tnx.pnrPaxId, tnx.nominalCode ";
			colReservationTnx = find(hql, ReservationTnx.class);
		}else{
			hql = "select tnx from ReservationTnx tnx where " + " tnx.pnrPaxId in ( :pnrPaxIds ) "
					+ "and tnx.nominalCode in ( :nominalCodes ) order by tnx.pnrPaxId, tnx.nominalCode ";
			Query query = getSession().createQuery(hql);
			query.setParameterList("pnrPaxIds", pnrPaxIds, StandardBasicTypes.INTEGER);
			query.setParameterList("nominalCodes", ReservationTnxNominalCode.getPaymentAndRefundNominalCodes(), StandardBasicTypes.INTEGER);
			colReservationTnx = query.list();
		}
		
		Map<Integer, Collection<ReservationTnx>> mapPnrPaxIdAndColReservationTnx = new HashMap<Integer, Collection<ReservationTnx>>();

		Iterator<ReservationTnx> itColReservationTnx = colReservationTnx.iterator();
		ReservationTnx reservationTnx;
		Collection<ReservationTnx> colHolder;
		Integer pnrPaxId;

		while (itColReservationTnx.hasNext()) {
			reservationTnx = (ReservationTnx) itColReservationTnx.next();
			pnrPaxId = new Integer(reservationTnx.getPnrPaxId());

			// If pnr pax id exist
			if (mapPnrPaxIdAndColReservationTnx.containsKey(pnrPaxId)) {
				colHolder = (Collection<ReservationTnx>) mapPnrPaxIdAndColReservationTnx.get(pnrPaxId);
				colHolder.add(reservationTnx);
			} else {
				colHolder = new ArrayList<ReservationTnx>();
				colHolder.add(reservationTnx);
				mapPnrPaxIdAndColReservationTnx.put(pnrPaxId, colHolder);
			}
		}

		return mapPnrPaxIdAndColReservationTnx;
	}

	public void saveTransaction(ReservationPaxTnxBreakdown tnx) {
		hibernateSaveOrUpdate(tnx);
	}

	public Map<Integer, Collection<ReservationTnx>> getAllBalanceTransactions(Collection<Integer> pnrPaxIds) {
		String strPnrPaxIds = Util.buildIntegerInClauseContent(pnrPaxIds);

		String hql = "select tnx from ReservationTnx tnx where " + " tnx.pnrPaxId in (" + strPnrPaxIds
				+ ") order by tnx.pnrPaxId, tnx.nominalCode ";

		Map<Integer, Collection<ReservationTnx>> mapPnrPaxIdAndColReservationTnx = new HashMap<Integer, Collection<ReservationTnx>>();
		Collection<ReservationTnx> colReservationTnx = find(hql, ReservationTnx.class);
		Iterator<ReservationTnx> itColReservationTnx = colReservationTnx.iterator();
		ReservationTnx reservationTnx;
		Collection<ReservationTnx> colHolder;
		Integer pnrPaxId;

		while (itColReservationTnx.hasNext()) {
			reservationTnx = (ReservationTnx) itColReservationTnx.next();

			// We don't need Zero records for any balance transaction(s)
			if (reservationTnx.getAmount().doubleValue() != 0) {
				pnrPaxId = new Integer(reservationTnx.getPnrPaxId());

				// If pnr pax id exist
				if (mapPnrPaxIdAndColReservationTnx.containsKey(pnrPaxId)) {
					colHolder = (Collection<ReservationTnx>) mapPnrPaxIdAndColReservationTnx.get(pnrPaxId);
					colHolder.add(reservationTnx);
				} else {
					colHolder = new ArrayList<ReservationTnx>();
					colHolder.add(reservationTnx);
					mapPnrPaxIdAndColReservationTnx.put(pnrPaxId, colHolder);
				}
			}
		}

		return mapPnrPaxIdAndColReservationTnx;
	}	

	public boolean isVoidReservationByPnrPaxIdOrPayTnxId(String pnrPaxId, String type) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder sb = new StringBuilder();
		Boolean isVoidReservation = false;

		if (pnrPaxId != null && !pnrPaxId.trim().equals("")) {

			if (type != null && type.equalsIgnoreCase("T")) {
				// "T" => by payment tnx-id
				sb.append("SELECT R.VOID_RESERVATION AS VOID_RESERVATION FROM T_RESERVATION R, T_PNR_PASSENGER PP WHERE PP.PNR = R.PNR	");
				sb.append("AND pp.PNR_PAX_ID IN (SELECT TXN.PNR_PAX_ID  FROM t_pax_txn_breakdown_summary pxtx,  ");
				sb.append("t_pax_transaction txn  WHERE pxtx.pax_txn_id=txn.txn_id	  AND txn.txn_id       ='" + pnrPaxId + "'  )");
			} else {
				// "P" => by pnr-pax-id
				sb.append("SELECT R.VOID_RESERVATION AS VOID_RESERVATION FROM  T_RESERVATION R, T_PNR_PASSENGER PP  WHERE PP.PNR_PAX_ID='"
						+ pnrPaxId + "' AND PP.PNR = R.PNR");
			}

			isVoidReservation = (Boolean) jt.query(sb.toString(), new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					String result = null;
					Boolean voidReservation = new Boolean(false);

					if (rs != null) {
						while (rs.next()) {
							result = BeanUtils.nullHandler(rs.getString("VOID_RESERVATION"));
						}
					}

					if (result != null && "Y".equalsIgnoreCase(result)) {
						voidReservation = true;
					}

					return voidReservation;
				}
			});

		}

		return isVoidReservation;
	}

	@Override
	public void saveTransactionSegment(TransactionSegment trnxSegment) {
		hibernateSaveOrUpdate(trnxSegment);
	}

	@Override
	public List<TransactionSegment> getTransactionSegment(List<Integer> txnIds) {
		if (txnIds.isEmpty()) {
			/*
			 * If the parameter is an empty list it throws an error instead of returning an empty list. Probably an bug
			 * in hibernate version. So returning an empty result manually.
			 */
			return new ArrayList<TransactionSegment>();
		}

		String hql = "SELECT txnSeg FROM TransactionSegment txnSeg WHERE txnSeg.tnxId IN(:transactionIDs)";
		Query query = getSession().createQuery(hql);
		query.setParameterList("transactionIDs", txnIds);
		return query.list();
	}

	public Collection<ReservationTnx> getPNRPaxTransactions(String pnrPaxId) {
		String hql = "select tnx from ReservationTnx tnx where  tnx.pnrPaxId = ? ";

		return find(hql, pnrPaxId, ReservationTnx.class);
	}
	
}
