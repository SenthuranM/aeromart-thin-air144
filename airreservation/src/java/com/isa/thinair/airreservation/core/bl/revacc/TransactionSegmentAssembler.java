package com.isa.thinair.airreservation.core.bl.revacc;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author dilan
 * 
 * @isa.module.command name="transactionSegmentAssembler"
 */
public class TransactionSegmentAssembler extends DefaultBaseCommand {
	@Override
	public ServiceResponce execute() throws ModuleException {
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		boolean isFirstPayment = this.getParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.FALSE, Boolean.class);

		if (reservation == null) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadEtickets(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
		}

		List<TransactionSegment> transactionSegments = new ArrayList<>();
		if (isFirstPayment) {
			for (ReservationSegment rs : reservation.getSegments()) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(rs.getStatus())) {
					TransactionSegment ts = new TransactionSegment();
					ts.setPnrSegId(rs.getPnrSegId());
					ts.setFltSegId(rs.getFlightSegId());
					transactionSegments.add(ts);
				}
			}
		}
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.TRNX_SEGMENTS, transactionSegments);
		return response;
	}

}
