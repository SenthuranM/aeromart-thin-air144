package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationSplitUtil;
import com.isa.thinair.airreservation.core.bl.common.CloneBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Handle the split reservation for dummy bookings
 * 
 * @author malaka
 * @isa.module.command name="splitDummyReservation"
 */
public class SplitDummyReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SplitDummyReservation.class);

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("Inside execute");
		}

		String newCarrierPNR = (String) this.getParameter(CommandParamNames.NEW_ORIGINATOR_PNR);
		Collection<Integer> pnrPaxIds = (Collection<Integer>) this.getParameter(CommandParamNames.PNR_PAX_IDS);
		Reservation oldReservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Boolean isPaxCancel = (Boolean) this.getParameter(CommandParamNames.IS_PAX_CANCEL);

		this.validateParams(newCarrierPNR, pnrPaxIds, oldReservation, isPaxCancel);

		// Clones the reservation
		Reservation newReservation = CloneBO.clone(oldReservation, newCarrierPNR, newCarrierPNR, null);

		ReservationSplitUtil.composeNewExternalSegments(oldReservation, newReservation, false);

		// Holds removing parents/infants and adults
		Set<ReservationPax> splitAdultsParents = new HashSet<ReservationPax>();
		// Holds removing infants
		Set<ReservationPax> splitInfants = new HashSet<ReservationPax>();
		// Holds the moving passengers
		Set<ReservationPax> movingPassengers = new HashSet<ReservationPax>();

		Iterator<ReservationPax> itReservationPax = oldReservation.getPassengers().iterator();
		ReservationPax passenger;
		// Locating parents/adults and infants
		while (itReservationPax.hasNext()) {
			passenger = itReservationPax.next();
			if (pnrPaxIds.contains(passenger.getPnrPaxId())) {
				if (ReservationApiUtils.isInfantType(passenger)) { // Infant
					splitInfants.add(passenger);
				} else { // Adult / child or Parent
					splitAdultsParents.add(passenger);
				}
			}
		}

		// Checking if there is an invalid infant split exist
		itReservationPax = splitInfants.iterator();
		while (itReservationPax.hasNext()) {
			passenger = (ReservationPax) itReservationPax.next();
			if (!splitAdultsParents.contains(passenger.getParent())) {
				// Updates the infant count
				ReservationSplitUtil.updateInfantCount(passenger.getReservation(), false);
				// add the infant to the move pax
				ReservationSplitUtil.moveInfant(passenger, movingPassengers);
			}
		}

		// Handling parents/infants and adults
		itReservationPax = splitAdultsParents.iterator();
		while (itReservationPax.hasNext()) {
			passenger = (ReservationPax) itReservationPax.next();
			// Removes a passenger
			ReservationSplitUtil.removePassenger(oldReservation, passenger, movingPassengers);
		}

		// Updates the reservation status
		ReservationSplitUtil.updatePnrStatus(oldReservation);
		// Saves the old reservation
		ReservationProxy.saveReservation(oldReservation);

		Iterator<ReservationPax> itPnrPaxCol = movingPassengers.iterator();

		while (itPnrPaxCol.hasNext()) {
			passenger = itPnrPaxCol.next();

			if (isPaxCancel) { // setting the pax status to CNX if remove pax
				passenger.setStatus(ReservationPaxStatus.CANCEL);
			}

			// Adds the new passenger
			ReservationSplitUtil.addPassenger(newReservation, passenger);
		}

		// Updates the reservation status
		ReservationSplitUtil.updatePnrStatus(newReservation);

		if (isPaxCancel) {
			// Update the external segment status
			ReservationSplitUtil.updateExternalSegmentStatus(newReservation);
		}

		// Saves the new reservation
		ReservationProxy.saveReservation(newReservation);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.VERSION, Long.valueOf(oldReservation.getVersion()));

		if (log.isDebugEnabled()) {
			log.debug("Exit execute");
		}
		return response;
	}

	private void validateParams(String newCarrierPNR, Collection<Integer> pnrPaxIds, Reservation oldReservation, Boolean isPaxCancel)
			throws ModuleException {
		if (newCarrierPNR == null || pnrPaxIds == null || pnrPaxIds.isEmpty() || oldReservation == null || isPaxCancel == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}

}
