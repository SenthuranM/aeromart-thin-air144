package com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;

public class TotalByDestinationElementBuilderTemplateCAL extends TotalByDestinationElementBuilderTemplate {

	@Override
	public String buildElementTemplate(PassengerElementsContext context) {

		StringBuilder elementTemplate = new StringBuilder();

		if ((context.getDestinationFare().getAddPassengers() != null && !context.getDestinationFare().getAddPassengers()
				.isEmpty())
				|| (context.getDestinationFare().getDeletePassengers() != null && !context.getDestinationFare()
						.getDeletePassengers().isEmpty())
				|| (context.getDestinationFare().getChangePassengers() != null && !context.getDestinationFare()
						.getChangePassengers().isEmpty())) {
			elementTemplate.append(hyphen());
			elementTemplate.append(context.getDestinationFare().getDestinationAirportCode());
			elementTemplate.append(space());
			elementTemplate.append(context.getDestinationFare().getFareClass());
		}
		return elementTemplate.toString();
	}

}
