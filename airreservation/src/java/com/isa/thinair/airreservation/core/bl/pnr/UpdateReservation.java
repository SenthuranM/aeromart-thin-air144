/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ChangePaxDetailsRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRq;
import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargeAdjustmentDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameChangeExtChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.PaxNameTranslations;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.service.RevenueAccountBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.CHARGE_ADJUSTMENTS;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.auditor.api.model.NameChangeChargeAudit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for updating a reservation
 * 
 * Business Rules: (1) Only updates PNR constant information, passenger title, first name, last name, date of birth,
 * nationality code and Transferring an infant to an adult
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="updateReservation"
 */
public class UpdateReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(UpdateReservation.class);

	/** Holds the revenue accounting delegate */
	private final RevenueAccountBD revenueAccountBD;

	/** Holds the credentials information */
	private CredentialsDTO credentialsDTO;

	private String passengerWiseInformationChanges = "";

	private List<Integer> pnrPaxIdsList = null;

	/**
	 * Construct the UpdateReservation
	 */
	public UpdateReservation() {
		revenueAccountBD = ReservationModuleUtils.getRevenueAccountBD();
	}

	/**
	 * Set Arguments
	 * 
	 * @param credentialsDTO
	 */
	private void setArguments(CredentialsDTO credentialsDTO) {
		this.credentialsDTO = credentialsDTO;
	}

	/**
	 * Execute method of the UpdateReservation command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		Boolean isFromNameChange = (Boolean) this.getParameter(CommandParamNames.IS_FROM_NAME_CHANGE);
		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (isFromNameChange != null && isFromNameChange.booleanValue()) {
			// Getting command parameters
			Reservation clientReservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
			CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
			DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
			Boolean isDummyReservation = (Boolean) this.getParameter(CommandParamNames.IS_DUMMY_RESERVATION);
			Boolean isTBANameChange = (Boolean) this.getParameter(CommandParamNames.IS_TBA_NAME_CHANGE);
			// flag for duplicate name check
			Boolean dupNameCheck = (Boolean) this.getParameter(CommandParamNames.CHECK_DUPLICATE_NAMES);
			Boolean applyNameChangeCharge = (Boolean) this.getParameter(CommandParamNames.APPLY_NAME_CHANGE_CHARGE);
			Collection<ReservationAudit> colReservationAudit = (Collection<ReservationAudit>) this
					.getParameter(CommandParamNames.RESERVATION_AUDIT_COLLECTION);

			Map<Integer, com.isa.thinair.airreservation.api.dto.NameDTO> nameChangePaxMap = (HashMap<Integer, com.isa.thinair.airreservation.api.dto.NameDTO>) this
					.getParameter(CommandParamNames.NAME_CHANGE_PAX_MAP);
			String reasonForAllowBlPax = (String) this.getParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX);
			
			// Checking params
			this.validateParams(clientReservation, isDummyReservation, credentialsDTO);

			if (!applyNameChangeCharge && nameChangePaxMap != null) {
				updatePaxNames(clientReservation, nameChangePaxMap);
			}

			// Set Arguments
			this.setArguments(credentialsDTO);

			// Get the user note
			String clientUserNote = clientReservation.getUserNote();

			// Get the user note Type
			String clientUserNoteType = clientReservation.getUserNoteType();

			// Keep the client contact information
			ReservationContactInfo clientContInfo = clientReservation.getContactInfo();

			// Get the server reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(clientReservation.getPnr());

			// we don't have any seat or fare info for dummy bookings
			if (isDummyReservation) {
				pnrModesDTO.setGroupPNR(clientReservation.getOriginatorPnr());
			} else {
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSeatingInfo(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
			}

			Reservation srvReservation = ReservationProxy.getReservation(pnrModesDTO);

			// Checking to see if concurrent update exist
			if (srvReservation.getVersion() != clientReservation.getVersion()) {
				throw new ModuleException("airreservations.arg.concurrentUpdate");
			}

			boolean paxNameChangeExist = isPassengerNameChangeExist(srvReservation, clientReservation);

			Map<Integer, NameDTO> changedPaxNamesMap = getNameChangedPassengerList(srvReservation, clientReservation);
			Collection<Integer> nameChangedPassengerList = changedPaxNamesMap.keySet();
			if (changedPaxNamesMap.size() == 0) {
				changedPaxNamesMap = getTitleChangedPassengerList(srvReservation, clientReservation);
			}
			Map<Integer, ReservationPaxAdditionalInfo> additionalInfoChangedPaxMap = getAdditionalInfoChangedPaxList(
					srvReservation, clientReservation);

			if (dupNameCheck && paxNameChangeExist) {
				Collection<Integer> colFlightSegID = new ArrayList<Integer>();
				Set<ReservationSegment> setResSeg = clientReservation.getSegments();
				Set<ReservationPax> resPaxs = removeNonNameChangesFromExistReservation(srvReservation, clientReservation);

				Iterator<ReservationSegment> iter = setResSeg.iterator();
				while (iter.hasNext()) {
					ReservationSegment resSeg = iter.next();
					Integer fliSegId = resSeg.getFlightSegId();
					colFlightSegID.add(fliSegId);
				}

				ReservationApiUtils.checkEndValidations(colFlightSegID, resPaxs, clientReservation.getPnr());
			}

			// Keep the server contact information
			ReservationContactInfo srvContInfo = srvReservation.getContactInfo();

			// Tracks ADL Changes if not a dummy booking
			trackADLChanges(srvReservation, clientReservation, changedPaxNamesMap.keySet(), paxNameChangeExist,
					isDummyReservation, false);

			Collection<ReservationPax> srvPassengers = srvReservation.getPassengers();
			Iterator<ReservationPax> itServerPassengers = srvPassengers.iterator();

			ReservationPax reservationPaxSrv;
			ReservationPax reservationPaxCln;

			Iterator<ReservationPax> itClientPassengers;
			Map<Integer, Integer> transferMap = new HashMap<Integer, Integer>();
			String paxChanges = "";
			String endorsementChanges = "";

			boolean isGdsReservation = srvReservation.getAdminInfo().getOriginChannelId()
					.compareTo(ReservationInternalConstants.SalesChannel.GDS) == 0;
			List<TransitionTo<Passenger>> changesList = new ArrayList<TransitionTo<Passenger>>();
			TransitionTo<Passenger> change;
			Passenger passenger;
			Set<Integer> changedPaxSet = new HashSet<Integer>();
			while (itServerPassengers.hasNext()) {
				reservationPaxSrv = itServerPassengers.next();

				itClientPassengers = clientReservation.getPassengers().iterator();

				while (itClientPassengers.hasNext()) {
					reservationPaxCln = itClientPassengers.next();
					if (reservationPaxSrv.getInfants() != null) {
						changedPaxSet.add(reservationPaxSrv.getPnrPaxId());
					}
					if (reservationPaxSrv.getPnrPaxId().equals(reservationPaxCln.getPnrPaxId())) {

						if (reservationPaxSrv.getVersion() != reservationPaxCln.getVersion()) {
							throw new ModuleException("airreservations.arg.concurrentUpdate");
						}

						if (isGdsReservation) {
							change = new TransitionTo<Passenger>();
							passenger = new Passenger();
							passenger.setTitle(reservationPaxSrv.getTitle());
							passenger.setFirstName(reservationPaxSrv.getFirstName());
							passenger.setLastName(reservationPaxSrv.getLastName());
							change.setOldVal(passenger);

							passenger = new Passenger();
							passenger.setTitle(reservationPaxCln.getTitle());
							passenger.setFirstName(reservationPaxCln.getFirstName());
							passenger.setLastName(reservationPaxCln.getLastName());
							change.setNewVal(passenger);

							changesList.add(change);
						}

						// Track Passenger wise changes
						passengerWiseInformationChanges = this.trackPaxChanges(reservationPaxSrv, reservationPaxCln,
								isDummyReservation);
						paxChanges = paxChanges + passengerWiseInformationChanges;

						if (passengerWiseInformationChanges != null && !passengerWiseInformationChanges.isEmpty()
								&& !isDummyReservation && !paxNameChangeExist) {
							pnrPaxIdsList = new ArrayList<Integer>();
							populatePnrPaxIdsList(reservationPaxSrv, pnrPaxIdsList);
							addADLChangeRequest(reservationPaxSrv, srvReservation, pnrPaxIdsList);
						}

						reservationPaxSrv.setTitle(reservationPaxCln.getTitle());
						reservationPaxSrv.setFirstName(reservationPaxCln.getFirstName());
						reservationPaxSrv.setLastName(reservationPaxCln.getLastName());
						reservationPaxSrv.setDateOfBirth(reservationPaxCln.getDateOfBirth());
						reservationPaxSrv.setNationalityCode(reservationPaxCln.getNationalityCode());

						if (!isDummyReservation) {
							ReservationSSRUtil.updatePaxSegmentSSRs(reservationPaxCln, reservationPaxSrv);
						}
						reservationPaxSrv.setPaxAdditionalInfo(reservationPaxCln.getPaxAdditionalInfo());
						PaxNameTranslations PaxNameTranslations = reservationPaxCln.getPaxNameTranslation();
						if (PaxNameTranslations != null) {
							reservationPaxSrv.setPaxNameTranslationList(reservationPaxCln.getPaxNameTranslation());
						}
						reservationPaxSrv.setPaxCategory(reservationPaxCln.getPaxCategory());

						if (ReservationApiUtils.isInfantType(reservationPaxCln)) {
							if (reservationPaxCln.getIndexId() != null) {

								// This means a change has happened
								if (!reservationPaxCln.getIndexId().equals(reservationPaxCln.getParent().getPaxSequence())) {
									// Track infant's that are getting transfered
									// Holds the parent id , infant instance
									transferMap.put(reservationPaxCln.getIndexId(), reservationPaxCln.getPnrPaxId());
									changedPaxSet.add(reservationPaxCln.getPnrPaxId());
									changedPaxSet.add(reservationPaxSrv.getPnrPaxId());
								}
							}
						}
					}
				}
			}

			// Assign Parent and children
			paxChanges = paxChanges
					+ this.assignParentAndInfants(srvReservation, transferMap, clientReservation, isDummyReservation);
			paxChanges = this.getDefaultMsgForPaxDetailsChange(paxChanges);

			if (transferMap.size() > 0) {
				trackADLChanges(srvReservation, clientReservation, changedPaxSet, paxNameChangeExist, isDummyReservation, true);
			}

			// Check whether infants are valid
			this.checkInfantsAreValidOrNot(srvReservation);

			// Set client contact information
			srvReservation.setContactInfo(clientContInfo);

			// Set endorsements
			if (BeanUtils.nullHandler(clientReservation.getEndorsementNote()).length() > 0) {
				if (!clientReservation.getEndorsementNote().equals(srvReservation.getEndorsementNote())) {
					srvReservation.setEndorsementNote(clientReservation.getEndorsementNote());
					endorsementChanges = "Endorsement Added : " + clientReservation.getEndorsementNote();
				}
			} else if (BeanUtils.nullHandler(srvReservation.getEndorsementNote()).length() > 0) {
				endorsementChanges = "Endorsement Removed : " + srvReservation.getEndorsementNote();
				srvReservation.setEndorsementNote(null);
			}
			// set booking category
			String bookingCategoryChange = "";
			if (clientReservation.getBookingCategory() != null
					&& clientReservation.getBookingCategory() != srvReservation.getBookingCategory()) {
				bookingCategoryChange = srvReservation.getBookingCategory() + " changed to "
						+ clientReservation.getBookingCategory();
				srvReservation.setBookingCategory(clientReservation.getBookingCategory());
			}

			String originCountryOfCallChange = "";
			if (clientReservation.getOriginCountryOfCall() != null
					&& !clientReservation.getOriginCountryOfCall().equals(srvReservation.getOriginCountryOfCall())) {
				originCountryOfCallChange = srvReservation.getOriginCountryOfCall() + " changed to " + clientReservation
						.getOriginCountryOfCall();
				srvReservation.setOriginCountryOfCall(clientReservation.getOriginCountryOfCall());
			}

			String itineraryFareMaskChange = "";
			String itineraryFaremask = clientReservation.getItineraryFareMaskFlag();
			if (itineraryFaremask != null && !itineraryFaremask.equals(srvReservation.getItineraryFareMaskFlag())) {
				itineraryFareMaskChange = " Itinerary Fare Mask " + srvReservation.getItineraryFareMaskFlag() + " changed to "
						+ itineraryFaremask;
				srvReservation.setItineraryFareMaskFlag(itineraryFaremask);
			}

			if (!isTBANameChange && !isDummyReservation) {
				Integer nameChangeCount = srvReservation.getNameChangeCount();
				srvReservation.setNameChangeCount(nameChangeCount + 1);
			}

			if (applyNameChangeCharge && !nameChangedPassengerList.isEmpty() && !isDummyReservation
					&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(srvReservation.getStatus())) {

				AdjustCreditBO adjustCreditBO = new AdjustCreditBO(srvReservation, true);

				this.applyNameChangeCharge(srvReservation, adjustCreditBO, nameChangedPassengerList);
				// Saves the reservation
				ReservationProxy.saveReservation(srvReservation);

				adjustCreditBO.callRevenueAccountingAfterReservationSave();
			} else {
				// Saves the reservation
				ReservationProxy.saveReservation(srvReservation);
			}

			// Generate ETickets for required PAX
			if (transferMap != null && !transferMap.isEmpty() && !isDummyReservation) {
				pnrModesDTO.setLoadPaxAvaBalance(true);
				Reservation modifiedReservation = ReservationProxy.getReservation(pnrModesDTO);

				Collection<Integer> balanceDueInfantPaxId = new ArrayList<Integer>();
				Collection<Integer> balanceDuePaxId = new ArrayList<Integer>();
				Set<ReservationPax> oldPassengers = clientReservation.getPassengers();
				if (oldPassengers != null && !oldPassengers.isEmpty()) {
					for (ReservationPax reservationPax : oldPassengers) {
						if (ReservationApiUtils.isAdultType(reservationPax)
								&& reservationPax.getTotalAvailableBalance().doubleValue() > 0) {
							balanceDuePaxId.add(reservationPax.getPnrPaxId());
							if (reservationPax.getAccompaniedPaxId() != null && reservationPax.getInfants() != null
									&& !reservationPax.getInfants().isEmpty()) {
								Iterator<ReservationPax> itReservationPax = reservationPax.getInfants().iterator();
								ReservationPax infantPax;
								if (itReservationPax.hasNext()) {
									infantPax = (ReservationPax) itReservationPax.next();
									balanceDueInfantPaxId.add(infantPax.getPnrPaxId());
								}

							}
						}

					}

				}

				if (!balanceDueInfantPaxId.isEmpty() || !balanceDuePaxId.isEmpty()) {
					Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = null;
					Iterator<ReservationPax> itReservationPax = modifiedReservation.getPassengers().iterator();
					ReservationPax reservationPaxAdultCurrent;
					ReservationPax reservationPaxInfant;
					List<ReservationPax> colTKTReservationPax = new ArrayList<ReservationPax>();
					while (itReservationPax.hasNext()) {
						reservationPaxAdultCurrent = itReservationPax.next();
						if (transferMap.containsKey(reservationPaxAdultCurrent.getPaxSequence())) {
							// Infants can be transfer to adults only
							if (ReservationApiUtils.isAdultType(reservationPaxAdultCurrent)) {
								Integer pnrPaxId = transferMap.get(reservationPaxAdultCurrent.getPaxSequence());
								if (reservationPaxAdultCurrent.getTotalAvailableBalance().doubleValue() <= 0) {
									reservationPaxInfant = this.getPassenger(modifiedReservation.getPassengers(), pnrPaxId);

									if (balanceDuePaxId.contains(reservationPaxAdultCurrent.getPnrPaxId())) {
										colTKTReservationPax.add(reservationPaxAdultCurrent);
									}

									if (balanceDueInfantPaxId.contains(reservationPaxInfant.getPnrPaxId())) {
										colTKTReservationPax.add(reservationPaxInfant);
									}

								}
							}

						}
					}

					if (!colTKTReservationPax.isEmpty()) {
						List<Integer> paxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(srvReservation);
						List<Integer> paxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(srvReservation);
						Collection<ReservationPax> etGenerationEligiblePaxForAdl = ReservationApiUtils
								.getConfirmedPaxByOperationUsingForceCNFSeqs(paxSeqsBeforeChange, paxSeqsAfterChange,
										srvReservation);
						CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
						eTicketInfo = ETicketBO.generateIATAETicketNumbersForModifyBooking(srvReservation, colTKTReservationPax,
								tktInfoDto);
						if (eTicketInfo != null) {
							Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
							modifiedPaxfareSegmentEtickets = ETicketBO.updateETickets(eTicketInfo, colTKTReservationPax,
									srvReservation, false);

							if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
								Collection<Integer> affectingPnrSegIds = new ArrayList<Integer>();

								for (ReservationPax resPax : srvReservation.getPassengers()) {
									Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
									Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
									ReservationPaxFare reservationPaxFare;
									ReservationPaxFareSegment reservationPaxFareSegment;

									while (itReservationPaxFare.hasNext()) {
										reservationPaxFare = itReservationPaxFare.next();
										itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

										while (itReservationPaxFareSegment.hasNext()) {
											reservationPaxFareSegment = itReservationPaxFareSegment.next();
											for (ReservationPaxFareSegmentETicket resPaxFareSegmentETicket : modifiedPaxfareSegmentEtickets) {
												if (resPaxFareSegmentETicket.getPnrPaxFareSegId()
														.equals(reservationPaxFareSegment.getPnrPaxFareSegId())) {
													affectingPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());

												}
											}
										}
									}
								}
								ADLNotifyer.recordCanceledSegmentsForAdl(srvReservation, affectingPnrSegIds,
										etGenerationEligiblePaxForAdl);
							}
							ADLNotifyer.recordReservation(srvReservation, AdlActions.A, etGenerationEligiblePaxForAdl, null);
						}
					}
				}
			}
			if(AppSysParamsUtil.isBlacklistpassengerCheckEnabled()){
				ReservationBO.saveBlacklistedPnrPaxIfExist(srvReservation ,String.valueOf(this.getParameter(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX)));
			}
			

			if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
				if (paxNameChangeExist && changedPaxNamesMap.size() != 0) {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(srvReservation);
					typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.NAME_CHANGE.getCode());
					typeBRequestDTO.setChangedPaxNamesMap(changedPaxNamesMap);
					TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
				}
				if (additionalInfoChangedPaxMap != null && additionalInfoChangedPaxMap.size() > 0) {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(srvReservation);
					typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CHANGE_PAX_DETAILS.getCode());
					typeBRequestDTO.setAdditionalInfoChangedPaxMap(additionalInfoChangedPaxMap);
					TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
				}
				if (clientUserNote != null && clientUserNote.length() > 0) {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(srvReservation);
					typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CHANGE_USER_NOTE.getCode());
					typeBRequestDTO.setUserNote(clientUserNote);
					TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
				}
			}

			Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodesInConfirmedSegments(srvReservation);
			if (csOCCarrirs != null) {
				if (paxNameChangeExist && changedPaxNamesMap.size() != 0) {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(srvReservation);
					typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.NAME_CHANGE.getCode());
					typeBRequestDTO.setChangedPaxNamesMap(changedPaxNamesMap);
					TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
				}
				if (additionalInfoChangedPaxMap != null && additionalInfoChangedPaxMap.size() > 0) {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(srvReservation);
					typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CHANGE_PAX_DETAILS.getCode());
					typeBRequestDTO.setAdditionalInfoChangedPaxMap(additionalInfoChangedPaxMap);
					TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
				}
				/*
				 * Due to conflict with Response processing, temporary stopped syncing usernotes for CS if
				 * (clientUserNote != null && clientUserNote.length() > 0) { TypeBRequestDTO typeBRequestDTO = new
				 * TypeBRequestDTO(); typeBRequestDTO.setReservation(srvReservation);
				 * typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CHANGE_USER_NOTE.getCode());
				 * typeBRequestDTO.setUserNote(clientUserNote); TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO,
				 * csOCCarrirs); }
				 */
			}

			if (srvReservation.getAdminInfo().getOriginChannelId()
					.compareTo(ReservationInternalConstants.SalesChannel.GDS) == 0) {
				// trigger TypeA changes
				GDSNotifyBD gdsNotifyBD = ReservationModuleUtils.getGDSNotifyBD();
				ChangePaxDetailsRq changePaxDetailsRq = new ChangePaxDetailsRq(ReservationRq.Event.CHANGE_PAX_DETAILS);
				changePaxDetailsRq.setPnr(srvReservation.getPnr());
				changePaxDetailsRq.setNameChanges(changesList);
				gdsNotifyBD.onReservationModification(changePaxDetailsRq);
			}

			// Constructing and returning the command response
			response.addResponceParam(CommandParamNames.VERSION, Long.valueOf(srvReservation.getVersion()));

			if (!isDummyReservation) {
				// Track Contact information changes if it is not a dummy booking
				String contactChanges = this.trackContInfoChanges(srvContInfo, clientContInfo);
				ReservationAudit reservationAudit = this.composeAudit(clientReservation.getPnr(), contactChanges, paxChanges,
						clientUserNote, endorsementChanges, bookingCategoryChange, originCountryOfCallChange,itineraryFareMaskChange, clientUserNoteType);
				if (colReservationAudit == null) {
					colReservationAudit = new ArrayList<ReservationAudit>();
				}
				colReservationAudit.add(reservationAudit);
				response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
				response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
			}
			log.debug("Exit execute");
			response.addResponceParam(CommandParamNames.PNR, clientReservation.getPnr());
		}
		return response;
	}

	private void addADLChangeRequest(ReservationPax reservationPax, Reservation reservation, List<Integer> pnrPaxIdsList)
			throws ModuleException {
		Collection<ReservationPax> paxs = new ArrayList<ReservationPax>();
		for (Integer paxId : pnrPaxIdsList) {
			ReservationPax pax = new ReservationPax();
			pax.setPnrPaxId(paxId);
			paxs.add(pax);
		}
		ADLNotifyer.recordReservation(reservation, AdlActions.C, paxs, null);
	}

	private NameChangeChargeAudit composeNameChangeAudit(Reservation reservation, Map<Integer, IPayment> pnrPaxIdAndPayments,
			Integer salesChannelCode, String agentCode, int nameChangePaxCount) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> nameChangeExternalChgs;
		List<Integer> pnrSegIds = new ArrayList<Integer>();
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());
				nameChangeExternalChgs = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.NAME_CHANGE_CHARGE);

				if (nameChangeExternalChgs.size() > 0) {
					for (ExternalChgDTO externalChgDTO : nameChangeExternalChgs) {
						NameChangeExtChgDTO nameChangeExternalChgDTO = (NameChangeExtChgDTO) externalChgDTO;

						for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
								if (reservationPaxFareSegment.getSegment().getStatus()
										.equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
										&& reservationPaxFareSegment.getSegment().getFlightSegId()
												.equals(nameChangeExternalChgDTO.getFlightSegId())) {
									Integer pnrSegId = reservationPaxFareSegment.getPnrSegId();
									if (!pnrSegIds.contains(pnrSegId)) {
										pnrSegIds.add(pnrSegId);
									}
								}
							}
						}
					}
				}
			}
		}

		NameChangeChargeAudit nameChangeChargeAudit = new NameChangeChargeAudit();
		nameChangeChargeAudit.setCreatedDate(new Date());
		nameChangeChargeAudit.setPnr(reservation.getPnr());
		nameChangeChargeAudit.setSalesChannelCode(salesChannelCode);
		nameChangeChargeAudit.setAgentCode(agentCode);
		nameChangeChargeAudit.setNameChangePaxCount(nameChangePaxCount);
		if (pnrSegIds.size() > 0) {
			Set<Integer> pnrSegmentIds = new HashSet<Integer>(pnrSegIds);
			nameChangeChargeAudit.setPnrSegmentIds(pnrSegmentIds);
		}

		return nameChangeChargeAudit;
	}

	private void updatePaxNames(Reservation reservation,
			Map<Integer, com.isa.thinair.airreservation.api.dto.NameDTO> nameChangedPaxMap) {
		for (Integer pnrPaxId : nameChangedPaxMap.keySet()) {
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {
					com.isa.thinair.airreservation.api.dto.NameDTO nameDTO = nameChangedPaxMap.get(pnrPaxId);
					reservationPax.setFirstName(nameDTO.getFirstname());
					reservationPax.setLastName(nameDTO.getLastName());
					reservationPax.setTitle(nameDTO.getTitle());
					reservationPax.getPaxAdditionalInfo().setFfid(nameDTO.getFFID());
					break;
				}
			}
		}
	}

	/**
	 * @param reservation
	 * @param adjustCreditBO
	 * @param nameChangePassengerList
	 * @throws ModuleException
	 */
	private void applyNameChangeCharge(Reservation reservation, AdjustCreditBO adjustCreditBO,
			Collection<Integer> nameChangePassengerList) throws ModuleException {

		Collection<CHARGE_ADJUSTMENTS> colCHARGES = new ArrayList<ReservationInternalConstants.CHARGE_ADJUSTMENTS>();

		if (this.hasInternationalFlightSegments(reservation.getSegmentsView())) {
			colCHARGES.add(CHARGE_ADJUSTMENTS.NAME_CHANGE_CHARGE_INT);
		}
		if (this.hasDomesticFlightSegments(reservation.getSegmentsView())) {
			colCHARGES.add(CHARGE_ADJUSTMENTS.NAME_CHANGE_CHARGE_DOM);
		}

		if (!colCHARGES.isEmpty()) {

			Map<CHARGE_ADJUSTMENTS, ChargeAdjustmentDTO> chargesMap = ReservationBO.getQuoteAdjustment(colCHARGES);
			if (!chargesMap.isEmpty()) {

				ChargeAdjustmentDTO selectedAdjustment = null;
				for (ChargeAdjustmentDTO adjustment : chargesMap.values()) {
					if (selectedAdjustment == null || (selectedAdjustment != null
							&& adjustment.getRatioValue().compareTo(selectedAdjustment.getRatioValue()) > 0)) {
						selectedAdjustment = adjustment;
					}
				}

				if (selectedAdjustment != null) {
					ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
					for (ReservationPax passenger : reservation.getPassengers()) {
						if (nameChangePassengerList.contains(passenger.getPnrPaxId())) {

							int ondSize = getOndSize(passenger.getPnrPaxFares());

							BigDecimal[] ondAmounts = AccelAeroCalculator.roundAndSplit(selectedAdjustment.getRatioValue(),
									ondSize);
							int seq = 0;
							for (ReservationPaxFare paxFare : passenger.getPnrPaxFares()) {
								if (isConfirmSegment(paxFare)) {
									// Adjusting the credit manually
									ReservationBO.adjustCreditManual(adjustCreditBO, reservation, paxFare.getPnrPaxFareId(),
											selectedAdjustment.getChgRateId(), ondAmounts[seq],
											ReservationInternalConstants.ChargeGroup.ADJ, "", credentialsDTO.getTrackInfoDTO(),
											credentialsDTO, chgTnxGen);
									seq++;
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * @param paxFares
	 * @return
	 */
	private int getOndSize(Set<ReservationPaxFare> paxFares) {
		Iterator<ReservationPaxFare> itReservationPaxFare = paxFares.iterator();
		ReservationPaxFare reservationPaxFare;
		int size = 0;
		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			if (isConfirmSegment(reservationPaxFare)) {
				size++;
			}
		}

		return size;
	}

	private void populatePnrPaxIdsList(ReservationPax reservationPax, List<Integer> pnrPaxIds) {
		if (pnrPaxIds != null) {
			if (reservationPax.getPaxType().equalsIgnoreCase(ReservationInternalConstants.PassengerType.INFANT.toString())) {
				if (reservationPax.getParent() != null) {
					pnrPaxIds.add(reservationPax.getParent().getPnrPaxId());
				}
			} else {
				pnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}
	}

	/**
	 * @param reservationPaxFare
	 * @return
	 */
	private boolean isConfirmSegment(ReservationPaxFare reservationPaxFare) {

		for (ReservationPaxFareSegment paxFareSegment : reservationPaxFare.getPaxFareSegments()) {

			if (paxFareSegment.getSegment().getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * @param segments
	 * @return
	 */
	private boolean hasInternationalFlightSegments(Collection<ReservationSegmentDTO> segments) {

		for (ReservationSegmentDTO resSegment : segments) {
			if (resSegment.isInternationalFlight()
					&& resSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param segments
	 * @return
	 */
	private boolean hasDomesticFlightSegments(Collection<ReservationSegmentDTO> segments) {

		for (ReservationSegmentDTO resSegment : segments) {
			if (!resSegment.isInternationalFlight()
					&& resSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Tracks the ADL Changes Business Rules : If any Name change or SSR Change delete it and add it.
	 * 
	 * @param srcReservation
	 * @param clnReservation
	 * @param isDummyReservation
	 * @throws ModuleException
	 */
	private void trackADLChanges(Reservation srvReservation, Reservation clnReservation, Set<Integer> changedPnrPaxIds,
			boolean nameChangeExist, Boolean isDummyReservation, boolean isTransferInfant) throws ModuleException {
		if (!isDummyReservation) {
			// First checking if any names change exist. If it's true can update the STAT change at once
			if (isTransferInfant || nameChangeExist) {
				// TODO remove this
				ADLNotifyer.recordOriginalReservation(srvReservation);
				// new ADL implementation
				ADLNotifyer.recordReservation(srvReservation, AdlActions.D, getSelectedPaxFrom(srvReservation, changedPnrPaxIds),
						null);
				ADLNotifyer.recordReservation(clnReservation, AdlActions.A, getSelectedPaxFrom(clnReservation, changedPnrPaxIds),
						null);
				// This means there are no Name change but checking to see if any SSR changes
			} else if (isAnySSRChangesForTheReservation(srvReservation, clnReservation)) {
				ADLNotifyer.updateStatusToCHG(srvReservation);
			}
		}
	}

	private Collection<ReservationPax> getSelectedPaxFrom(Reservation reservation, Set<Integer> pnrPaxIds) {
		Collection<ReservationPax> paxList = new ArrayList<ReservationPax>();
		for (ReservationPax pax : reservation.getPassengers()) {
			if (pnrPaxIds != null && pnrPaxIds.contains(pax.getPnrPaxId())) {
				paxList.add(pax);
				if (pax.getParent() != null && !pnrPaxIds.contains(pax.getParent().getPnrPaxId())) {
					paxList.add(pax.getParent());
				}
			}
		}
		return paxList;
	}

	/**
	 * Returns if any SSR changes exist for the given reservation
	 * 
	 * @param srcReservation
	 * @param clnReservation
	 * @return
	 */
	private boolean isAnySSRChangesForTheReservation(Reservation srcReservation, Reservation clnReservation) {
		for (Iterator<ReservationPax> itSrcReservationPax = srcReservation.getPassengers().iterator(); itSrcReservationPax
				.hasNext();) {
			ReservationPax reservationPaxSrv = itSrcReservationPax.next();

			for (Iterator<ReservationPax> itClnReservationPax = clnReservation.getPassengers().iterator(); itClnReservationPax
					.hasNext();) {
				ReservationPax reservationPaxCln = itClnReservationPax.next();

				if (reservationPaxSrv.getPnrPaxId().equals(reservationPaxCln.getPnrPaxId())) {
					if (trackSSRChangesForThePax(reservationPaxSrv, reservationPaxCln).length() > 0) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * @param srcReservation
	 * @param clnReservation
	 * @return
	 */
	private boolean isPassengerNameChangeExist(Reservation srcReservation, Reservation clnReservation) {

		ReservationPax reservationPaxSrv;
		ReservationPax reservationPaxCln;

		for (Iterator<ReservationPax> itSrcReservationPax = srcReservation.getPassengers().iterator(); itSrcReservationPax
				.hasNext();) {
			reservationPaxSrv = itSrcReservationPax.next();

			for (Iterator<ReservationPax> itClnReservationPax = clnReservation.getPassengers().iterator(); itClnReservationPax
					.hasNext();) {
				reservationPaxCln = itClnReservationPax.next();

				if (reservationPaxSrv.getPnrPaxId().equals(reservationPaxCln.getPnrPaxId())) {

					if ((BeanUtils.compare(reservationPaxSrv.getTitle(), reservationPaxCln.getTitle(), "Title").length() > 0)
							|| (BeanUtils
									.compare(reservationPaxSrv.getFirstName(), reservationPaxCln.getFirstName(), "First Name")
									.length() > 0)
							|| (BeanUtils.compare(reservationPaxSrv.getLastName(), reservationPaxCln.getLastName(), "LastName")
									.length() > 0)) {
						return true;
					}
					break;
				}
			}
		}

		return false;
	}

	/**
	 * @param srcReservation
	 * @param clnReservation
	 * @return
	 */
	private Map<Integer, ReservationPaxAdditionalInfo> getAdditionalInfoChangedPaxList(Reservation srcReservation,
			Reservation clnReservation) throws ModuleException {

		Map<Integer, ReservationPaxAdditionalInfo> paxIdsMap = new HashMap<Integer, ReservationPaxAdditionalInfo>();
		ReservationPax reservationPaxSrv;
		ReservationPax reservationPaxCln;

		for (Iterator<ReservationPax> itSrcReservationPax = srcReservation.getPassengers().iterator(); itSrcReservationPax
				.hasNext();) {
			reservationPaxSrv = itSrcReservationPax.next();
			ReservationPaxAdditionalInfo srcPaxAdditionalInfo = reservationPaxSrv.getPaxAdditionalInfo();

			for (Iterator<ReservationPax> itClnReservationPax = clnReservation.getPassengers().iterator(); itClnReservationPax
					.hasNext();) {
				reservationPaxCln = itClnReservationPax.next();

				if (reservationPaxSrv.getPnrPaxId().equals(reservationPaxCln.getPnrPaxId())) {
					ReservationPaxAdditionalInfo clnPaxAdditionalInfo = reservationPaxCln.getPaxAdditionalInfo();

					if ((srcPaxAdditionalInfo.getPassportNo() == null && clnPaxAdditionalInfo.getPassportNo() != null
							&& clnPaxAdditionalInfo.getPassportNo().length() > 0)
							|| (srcPaxAdditionalInfo.getPassportNo() != null
									&& !srcPaxAdditionalInfo.getPassportNo().equals(clnPaxAdditionalInfo.getPassportNo()))
							|| (srcPaxAdditionalInfo.getPassportExpiry() != null
									&& clnPaxAdditionalInfo.getPassportExpiry() != null
									&& !CalendarUtil.isDateEqual(srcPaxAdditionalInfo.getPassportExpiry(),
											clnPaxAdditionalInfo.getPassportExpiry()))
							|| (srcPaxAdditionalInfo.getPassportIssuedCntry() != null && !srcPaxAdditionalInfo
									.getPassportIssuedCntry().equals(clnPaxAdditionalInfo.getPassportIssuedCntry()))
							|| (srcPaxAdditionalInfo.getVisaDocNumber() == null && clnPaxAdditionalInfo.getVisaDocNumber() != null
									&& clnPaxAdditionalInfo.getVisaDocNumber().length() > 0)
							|| (srcPaxAdditionalInfo.getVisaDocNumber() != null
									&& !srcPaxAdditionalInfo.getVisaDocNumber().equals(clnPaxAdditionalInfo.getVisaDocNumber()))
							|| (srcPaxAdditionalInfo.getVisaDocIssueDate() != null
									&& !CalendarUtil.isDateEqual(srcPaxAdditionalInfo.getVisaDocIssueDate(),
											clnPaxAdditionalInfo.getVisaDocIssueDate()))
							|| (srcPaxAdditionalInfo.getVisaDocPlaceOfIssue() != null && !srcPaxAdditionalInfo
									.getVisaDocPlaceOfIssue().equals(clnPaxAdditionalInfo.getVisaDocPlaceOfIssue()))
							|| (srcPaxAdditionalInfo.getVisaApplicableCountry() != null && !srcPaxAdditionalInfo
									.getVisaApplicableCountry().equals(clnPaxAdditionalInfo.getVisaApplicableCountry()))
							|| (srcPaxAdditionalInfo.getPlaceOfBirth() != null
									&& !srcPaxAdditionalInfo.getPlaceOfBirth().equals(clnPaxAdditionalInfo.getPlaceOfBirth()))) {
						paxIdsMap.put(reservationPaxSrv.getPnrPaxId(), clnPaxAdditionalInfo);
					}
				}
			}
		}
		return paxIdsMap;
	}

	/**
	 * Returns changed name passengers from existing reservation
	 * 
	 * This method will remove passengers, who has no name changes in existing reservation
	 * 
	 * @param srcReservation
	 * @param clnReservation
	 * @return
	 */
	private Set<ReservationPax> removeNonNameChangesFromExistReservation(Reservation srcReservation, Reservation clnReservation) {
		ReservationPax reservationPaxSrv;
		ReservationPax reservationPaxCln;
		Set<ReservationPax> resPax = new HashSet<ReservationPax>();

		for (Iterator<ReservationPax> itSrcReservationPax = srcReservation.getPassengers().iterator(); itSrcReservationPax
				.hasNext();) {
			reservationPaxSrv = itSrcReservationPax.next();

			for (Iterator<ReservationPax> itClnReservationPax = clnReservation.getPassengers().iterator(); itClnReservationPax
					.hasNext();) {
				reservationPaxCln = itClnReservationPax.next();

				if (reservationPaxSrv.getPnrPaxId().equals(reservationPaxCln.getPnrPaxId())) {
					// Integer pnrPaxID = reservationPaxCln.getPnrPaxId();
					if ((BeanUtils.compare(reservationPaxSrv.getTitle(), reservationPaxCln.getTitle(), "Title").length() > 0)
							|| (BeanUtils
									.compare(reservationPaxSrv.getFirstName(), reservationPaxCln.getFirstName(), "First Name")
									.length() > 0)
							|| (BeanUtils.compare(reservationPaxSrv.getLastName(), reservationPaxCln.getLastName(), "LastName")
									.length() > 0)) {
						resPax.add(reservationPaxCln);
					}
				}
			}
		}

		return resPax;
	}

	/**
	 * Check infants are valid or not
	 * 
	 * @param srvReservation
	 * @throws ModuleException
	 */
	private void checkInfantsAreValidOrNot(Reservation srvReservation) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = srvReservation.getPassengers().iterator();
		ReservationPax reservationPax;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
				this.checkIsOneInfantPerParent(reservationPax);
			}
		}

	}

	/**
	 * Assign Parent and infants
	 * 
	 * @param srvReservation
	 * @param transferMap
	 * @param isDummyReservation
	 * @throws ModuleException
	 */
	private String assignParentAndInfants(Reservation srvReservation, Map<Integer, Integer> transferMap, Reservation cReservation,
			Boolean isDummyReservation) throws ModuleException {
		// If no transfer happened no point of iterating
		StringBuffer changes = new StringBuffer();

		if (transferMap.size() > 0) {
			Iterator<ReservationPax> itReservationPax = srvReservation.getPassengers().iterator();
			ReservationPax reservationPaxAdultCurrent;
			ReservationPax reservationPaxInfant;
			ReservationPax reservationPaxAdultOld;

			while (itReservationPax.hasNext()) {
				reservationPaxAdultCurrent = itReservationPax.next();

				if (transferMap.containsKey(reservationPaxAdultCurrent.getPaxSequence())) {

					// Infants can be transfer to adults only
					if (ReservationApiUtils.isAdultType(reservationPaxAdultCurrent)) {
						Integer pnrPaxId = transferMap.get(reservationPaxAdultCurrent.getPaxSequence());
						reservationPaxInfant = this.getPassenger(srvReservation.getPassengers(), pnrPaxId);
						reservationPaxAdultOld = reservationPaxInfant.getParent();

						if (!isDummyReservation) {
							// Call track changes
							this.trackInfantChanges(changes, reservationPaxInfant, reservationPaxAdultOld,
									reservationPaxAdultCurrent);
							ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = TnxGranularityReservationUtils
									.getReservationPaymentMetaTOForTransferInfants(reservationPaxAdultOld.getPnrPaxId(),
											reservationPaxInfant, srvReservation.isEnableTransactionGranularity());

							if (!srvReservation.isInfantPaymentRecordedWithInfant()) {
								// Calling revenue accounting
								revenueAccountBD.recordTransferInfant(reservationPaxAdultOld.getPnrPaxId().toString(),
										reservationPaxAdultCurrent.getPnrPaxId().toString(),
										reservationPaxInfant.getTotalChargeAmount(), reservationPaxPaymentMetaTO, credentialsDTO,
										srvReservation.isEnableTransactionGranularity());
							}

						}
						// Remove the infant
						reservationPaxAdultOld.removeInfants(reservationPaxInfant);

						if (!isDummyReservation) {
							Map<Integer, String> flightSeatIdPaxType = new HashMap<Integer, String>();
							Map<Integer, List<Integer>> pnrSegFlightSeats = new HashMap<Integer, List<Integer>>();
							Map<Integer, Integer> paxFlightSeatSwapMap = new HashMap<Integer, Integer>();
							composeFlightSeatIdPaxType(srvReservation.getSeats(reservationPaxAdultOld.getPnrPaxId()),
									ReservationInternalConstants.PassengerType.ADULT, flightSeatIdPaxType, pnrSegFlightSeats);
							composeFlightSeatIdPaxType(srvReservation.getSeats(reservationPaxAdultCurrent.getPnrPaxId()),
									ReservationInternalConstants.PassengerType.PARENT, flightSeatIdPaxType, pnrSegFlightSeats);

							composePaxFlightSeatSwapMap(flightSeatIdPaxType, paxFlightSeatSwapMap, pnrSegFlightSeats);

							if (flightSeatIdPaxType != null && flightSeatIdPaxType.size() > 0) {
								ReservationModuleUtils.getSeatMapBD().updatePaxType(flightSeatIdPaxType, paxFlightSeatSwapMap);
							}
						}
						// Add the new infant to the new parent
						reservationPaxAdultCurrent.addInfants(reservationPaxInfant);
					} else {
						throw new ModuleException("airreservations.modifyPNR.invalidInfantAssign");
					}
				}
			}
		}

		return changes.toString();
	}

	private void composeFlightSeatIdPaxType(Collection<PaxSeatTO> colPaxSeatTO, String type,
			Map<Integer, String> flightSeatIdPaxType, Map<Integer, List<Integer>> pnrSegFlightSeats) {
		for (PaxSeatTO paxSeatTO : colPaxSeatTO) {
			flightSeatIdPaxType.put(paxSeatTO.getSelectedFlightSeatId(), type);

			if (pnrSegFlightSeats.containsKey(paxSeatTO.getPnrSegId())) {
				pnrSegFlightSeats.get(paxSeatTO.getPnrSegId()).add(paxSeatTO.getSelectedFlightSeatId());
			} else {
				List<Integer> seatsList = new ArrayList<Integer>();
				seatsList.add(paxSeatTO.getSelectedFlightSeatId());
				pnrSegFlightSeats.put(paxSeatTO.getPnrSegId(), seatsList);
			}

		}
	}

	private void composePaxFlightSeatSwapMap(Map<Integer, String> flightSeatIdPaxType, Map<Integer, Integer> paxFlightSeatSwapMap,
			Map<Integer, List<Integer>> pnrSegFlightSeats) {

		for (Integer pnrSeg : pnrSegFlightSeats.keySet()) {
			List<Integer> segmentSeats = pnrSegFlightSeats.get(pnrSeg);
			Integer parentSeatRef = null;
			Integer adultSeatRef = null;
			for (Integer fltSeatAmId : flightSeatIdPaxType.keySet()) {
				if (segmentSeats.contains(fltSeatAmId)) {
					if (flightSeatIdPaxType.get(fltSeatAmId).equals(ReservationInternalConstants.PassengerType.PARENT)) {
						parentSeatRef = fltSeatAmId;
					} else if (flightSeatIdPaxType.get(fltSeatAmId).equals(ReservationInternalConstants.PassengerType.ADULT)) {
						adultSeatRef = fltSeatAmId;
					}
				}
			}
			paxFlightSeatSwapMap.put(parentSeatRef, adultSeatRef);

		}
	}

	/**
	 * Checks whether it's an one infant per parent
	 * 
	 * @param reservationPax
	 * @throws ModuleException
	 */
	private void checkIsOneInfantPerParent(ReservationPax parent) throws ModuleException {
		if (parent.getInfants() != null) {
			if (parent.getInfants().size() > 1) {
				throw new ModuleException("airreservations.arg.invalidInfantAssign");
			}
		}
	}

	/**
	 * Get the passenger
	 * 
	 * @param colReservationPax
	 * @param pnrPaxId
	 * @return
	 * @throws ModuleException
	 */
	private ReservationPax getPassenger(Collection<ReservationPax> colReservationPax, Integer pnrPaxId) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = colReservationPax.iterator();
		ReservationPax reservationPax = null;
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {
				break;
			}
		}

		if (reservationPax == null) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		return reservationPax;
	}

	/**
	 * Construct infant track changes information
	 * 
	 * @param paxChanges
	 * @param reservationPaxInfant
	 * @param reservationPaxAdultOld
	 * @param reservationPaxAdultCurrent
	 */
	private void trackInfantChanges(StringBuffer changes, ReservationPax reservationPaxInfant,
			ReservationPax reservationPaxAdultOld, ReservationPax reservationPaxAdultCurrent) {
		changes.append(" ").append(reservationPaxInfant.getFirstName()).append(", ").append(reservationPaxInfant.getLastName())
				.append(" - ").append(reservationPaxInfant.getPaxTypeDescription())

				.append(" transfered from ").append(reservationPaxAdultOld.getFirstName()).append(", ")
				.append(reservationPaxAdultOld.getLastName()).append(" - ").append(reservationPaxAdultOld.getPaxTypeDescription())

				.append(" to ").append(reservationPaxAdultCurrent.getFirstName()).append(", ")
				.append(reservationPaxAdultCurrent.getLastName()).append(" - ")
				.append(reservationPaxAdultCurrent.getPaxTypeDescription());
	}

	/**
	 * Compose audit
	 * 
	 * @param pnr
	 * @param contChanges
	 * @param paxChanges
	 * @param userNote
	 * @param userNoteType
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	private ReservationAudit composeAudit(String pnr, String contChanges, String paxChanges, String userNote,
			String endorsementChanges, String bookingCategoryChange, String originCountryOfCallChange,
			String itineraryFareMaskChange, String userNoteType) throws ModuleException {

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CHANGED_PASSENGER_DETAILS.getCode());
		reservationAudit.setUserNote(userNote);
		reservationAudit.setUserNoteType(userNoteType);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedPassengerDetails.PASSENGER_DETAILS,
				contChanges + paxChanges + endorsementChanges + bookingCategoryChange + originCountryOfCallChange
						+ itineraryFareMaskChange);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedPassengerDetails.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedPassengerDetails.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Track Contact information changes
	 * 
	 * @param srvContInfo
	 * @param clientContInfo
	 * @return
	 * @throws ModuleException
	 */
	private String trackContInfoChanges(ReservationContactInfo srvContInfo, ReservationContactInfo clientContInfo)
			throws ModuleException {
		return BeanUtils.compare(clientContInfo, srvContInfo);
	}

	/**
	 * Track Passenger vise changes
	 * 
	 * @param reservationPaxSrv
	 * @param reservationPaxCln
	 * @param isDummyReservation
	 * @return
	 */
	private String trackPaxChanges(ReservationPax reservationPaxSrv, ReservationPax reservationPaxCln,
			Boolean isDummyReservation) {

		// PaxCategoryFoidTO paxCategoryFoidTOSrv = null;
		// PaxCategoryFoidTO paxCategoryFoidTOCln = null;
		SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy");

		StringBuffer changes = new StringBuffer();

		if (!isDummyReservation) {
			changes.append(BeanUtils.compare(reservationPaxSrv.getTitle(), reservationPaxCln.getTitle(), "Title"));
			changes.append(BeanUtils.compare(reservationPaxSrv.getFirstName(), reservationPaxCln.getFirstName(), "FirstName"));
			changes.append(BeanUtils.compare(reservationPaxSrv.getLastName(), reservationPaxCln.getLastName(), "LastName"));
			if ((reservationPaxSrv.getDateOfBirth() != null && reservationPaxCln.getDateOfBirth() != null
					&& (reservationPaxSrv.getDateOfBirth().getTime() != reservationPaxCln.getDateOfBirth().getTime()))
					|| (reservationPaxSrv.getDateOfBirth() == null && reservationPaxCln.getDateOfBirth() != null)
					|| (reservationPaxSrv.getDateOfBirth() != null && reservationPaxCln.getDateOfBirth() == null)) {

				changes.append((reservationPaxSrv.getTitle() != null)
						? reservationPaxSrv.getTitle() + " " + reservationPaxSrv.getFirstName() + " "
								+ reservationPaxSrv.getLastName()
						: "" + " " + reservationPaxSrv.getFirstName() + " " + reservationPaxSrv.getLastName());
				changes.append(BeanUtils.compare(
						(reservationPaxSrv.getDateOfBirth() != null) ? // Fixed:AARESAA-1782
								format.format(reservationPaxSrv.getDateOfBirth()) : reservationPaxSrv.getDateOfBirth(),
						(reservationPaxCln.getDateOfBirth() != null)
								? format.format(reservationPaxCln.getDateOfBirth())
								: reservationPaxCln.getDateOfBirth(),
						"DateOfBirth"));
			}
			changes.append(BeanUtils.compare(reservationPaxSrv.getNationalityCode(), reservationPaxCln.getNationalityCode(),
					"NationalityCode"));
			changes.append(trackSSRChangesForThePax(reservationPaxSrv, reservationPaxCln));

			ReservationPaxAdditionalInfo reservationPaxAdditionalInfoSrv = reservationPaxSrv.getPaxAdditionalInfo();
			ReservationPaxAdditionalInfo reservationPaxAdditionalInfoCln = reservationPaxCln.getPaxAdditionalInfo();
			if (reservationPaxAdditionalInfoSrv != null && reservationPaxAdditionalInfoCln != null) {
				changes.append(trackChangesForPaxAdditionalInformation(reservationPaxAdditionalInfoSrv,
						reservationPaxAdditionalInfoCln));
			}

		}
		return changes.toString();
	}

	private String trackChangesForPaxAdditionalInformation(ReservationPaxAdditionalInfo additionalInfoSrv,
			ReservationPaxAdditionalInfo additionalInfoCln) {
		String changedInformation = null;
		StringBuffer changes = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy");

		trackStringChange(additionalInfoSrv.getPassportNo(), additionalInfoCln.getPassportNo(), changes, "FOIDNumber");

		trackDateChange(additionalInfoSrv.getPassportExpiry(), additionalInfoCln.getPassportExpiry(), changes, "FOIDExpire",
				format);

		trackStringChange(additionalInfoSrv.getPassportIssuedCntry(), additionalInfoCln.getPassportIssuedCntry(), changes,
				"FOIDIssuedCountry");

		trackStringChange(additionalInfoSrv.getPlaceOfBirth(), additionalInfoCln.getPlaceOfBirth(), changes, "PlaceOfBirth");

		trackStringChange(additionalInfoSrv.getVisaDocNumber(), additionalInfoCln.getVisaDocNumber(), changes, "VisaDocNumber");

		trackStringChange(additionalInfoSrv.getVisaDocPlaceOfIssue(), additionalInfoCln.getVisaDocPlaceOfIssue(), changes,
				"VisaDocPlaceOfIssue");

		trackDateChange(additionalInfoSrv.getVisaDocIssueDate(), additionalInfoCln.getVisaDocIssueDate(), changes,
				"VisaDocIssueDate", format);

		trackStringChange(additionalInfoSrv.getVisaApplicableCountry(), additionalInfoCln.getVisaApplicableCountry(), changes,
				"VisaDocApplicableCountry");

		changedInformation = changes.toString();
		return changedInformation;
	}

	private void trackStringChange(String originalString, String compairingString, StringBuffer recorder,
			String compairingField) {
		recorder.append(BeanUtils.compare(originalString, compairingString, compairingField));
	}

	private void trackDateChange(Date originalDate, Date compairingDate, StringBuffer recorder, String compairingField,
			SimpleDateFormat format) {
		if (!isIdenticalDates(originalDate, compairingDate)) {
			recorder.append(BeanUtils.compare(getFormattedDate(format, originalDate), getFormattedDate(format, compairingDate),
					compairingField));
		}
	}

	private boolean isIdenticalDates(Date originalDate, Date compareDate) {
		boolean isIdentical = false;

		long originalDateTime = getTimeOfDate(originalDate);
		long compareDateTime = getTimeOfDate(compareDate);
		if (originalDateTime == compareDateTime) {
			isIdentical = true;
		}
		return isIdentical;
	}

	private long getTimeOfDate(Date date) {

		long time = 0;

		if (date != null) {
			time = date.getTime();
		}
		return time;
	}

	private String getFormattedDate(SimpleDateFormat format, Date date) {

		String formattedDate = "";
		if (date != null) {
			formattedDate = format.format(date);
		}
		return formattedDate;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param reservation
	 * @param isDummyReservation
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(Reservation reservation, Boolean isDummyReservation, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (reservation == null || reservation.getPnr() == null || reservation.getVersion() == -1
				|| reservation.getPassengers() == null || reservation.getPassengers().size() == 0
				|| reservation.getContactInfo() == null || credentialsDTO == null || isDummyReservation == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (!isDummyReservation && (reservation.getSegments() == null || reservation.getSegments().size() == 0)) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	private static Map<Integer, Set<ReservationPaxSegmentSSR>> getPaxSegSSRCollectionMap(ReservationPax reservationPax) {
		Map<Integer, Set<ReservationPaxSegmentSSR>> segmentSSRMap = new HashMap<Integer, Set<ReservationPaxSegmentSSR>>();
		Set<ReservationPaxFare> resPaxFare = reservationPax.getPnrPaxFares();

		for (Iterator<ReservationPaxFare> itPaxFare = resPaxFare.iterator(); itPaxFare.hasNext();) {
			ReservationPaxFare reservationPaxFare = itPaxFare.next();

			Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();

			for (Iterator<ReservationPaxFareSegment> itPaxFareSegment = paxFareSegments.iterator(); itPaxFareSegment.hasNext();) {
				ReservationPaxFareSegment reservationPaxFareSegment = itPaxFareSegment.next();

				Set<ReservationPaxSegmentSSR> paxSegmentSSR = reservationPaxFareSegment.getReservationPaxSegmentSSRs();
				segmentSSRMap.put(reservationPaxFareSegment.getPnrSegId(), paxSegmentSSR);
			}
		}

		return segmentSSRMap;
	}

	private String trackSSRChangesForThePaxSegment(Set<ReservationPaxSegmentSSR> paxSegmentSSRsSrv,
			Set<ReservationPaxSegmentSSR> paxSegmentSSRsCln) {
		Map<Integer, ReservationPaxSegmentSSR> paxSegSSRMapCln = extractPaxSegSSRMap(paxSegmentSSRsCln);
		StringBuilder sb = new StringBuilder();

		if (paxSegmentSSRsSrv != null && !paxSegmentSSRsSrv.isEmpty()) {
			for (Iterator<ReservationPaxSegmentSSR> iterator = paxSegmentSSRsSrv.iterator(); iterator.hasNext();) {
				ReservationPaxSegmentSSR reservationPaxSegmentSSRSrv = iterator.next();
				ReservationPaxSegmentSSR reservationPaxSegmentSSRCln = paxSegSSRMapCln
						.get(reservationPaxSegmentSSRSrv.getPnrPaxSegmentSSRId());

				if (reservationPaxSegmentSSRCln == null) {
					sb.append(BeanUtils.logRemovedValue(reservationPaxSegmentSSRSrv.getSSRId(), "SSRCode"));
					sb.append(BeanUtils.logRemovedValue(reservationPaxSegmentSSRSrv.getSSRText(), "SSRText"));
				} else {
					sb.append(BeanUtils.compare(reservationPaxSegmentSSRSrv.getSSRId(), reservationPaxSegmentSSRCln.getSSRId(),
							"SSRCode"));
					sb.append(BeanUtils.compare(reservationPaxSegmentSSRSrv.getSSRText(),
							reservationPaxSegmentSSRCln.getSSRText(), "SSRText"));
				}
			}
		}

		if (paxSegmentSSRsCln != null && !paxSegmentSSRsCln.isEmpty()) {
			for (Iterator<ReservationPaxSegmentSSR> iterator = paxSegmentSSRsCln.iterator(); iterator.hasNext();) {
				ReservationPaxSegmentSSR reservationPaxSegmentSSRCln = iterator.next();
				ReservationPaxSegmentSSR reservationPaxSegmentSSRSrv = paxSegSSRMapCln
						.get(reservationPaxSegmentSSRCln.getPnrPaxSegmentSSRId());

				if (reservationPaxSegmentSSRSrv == null) {
					sb.append(BeanUtils.logAddedValue(reservationPaxSegmentSSRCln.getSSRId(), "SSRCode"));
					sb.append(BeanUtils.logAddedValue(reservationPaxSegmentSSRCln.getSSRText(), "SSRText"));
				}
			}
		}

		return sb.toString();
	}

	private static Map<Integer, ReservationPaxSegmentSSR> extractPaxSegSSRMap(Set<ReservationPaxSegmentSSR> paxSegmentSSRs) {
		Map<Integer, ReservationPaxSegmentSSR> paxSegSSRMap = new HashMap<Integer, ReservationPaxSegmentSSR>();
		if (paxSegmentSSRs != null && !paxSegmentSSRs.isEmpty()) {
			for (Iterator<ReservationPaxSegmentSSR> iterator = paxSegmentSSRs.iterator(); iterator.hasNext();) {
				ReservationPaxSegmentSSR reservationPaxSegmentSSR = iterator.next();

				paxSegSSRMap.put(reservationPaxSegmentSSR.getPnrPaxSegmentSSRId(), reservationPaxSegmentSSR);
			}
		}

		return paxSegSSRMap;
	}

	private String trackSSRChangesForThePax(ReservationPax reservationPaxSrv, ReservationPax reservationPaxCln) {

		StringBuilder sb = new StringBuilder();
		Map<Integer, Set<ReservationPaxSegmentSSR>> paxSegmentSSRMapSrv = getPaxSegSSRCollectionMap(reservationPaxSrv);
		Map<Integer, Set<ReservationPaxSegmentSSR>> paxSegmentSSRMapCln = getPaxSegSSRCollectionMap(reservationPaxCln);

		for (Iterator<Integer> iterator = paxSegmentSSRMapSrv.keySet().iterator(); iterator.hasNext();) {
			Integer pnrPaxSegId = iterator.next();

			Set<ReservationPaxSegmentSSR> paxSegmentSSRsSrv = paxSegmentSSRMapSrv.get(pnrPaxSegId);
			Set<ReservationPaxSegmentSSR> paxSegmentSSRsCln = paxSegmentSSRMapCln.get(pnrPaxSegId);

			sb.append(trackSSRChangesForThePaxSegment(paxSegmentSSRsSrv, paxSegmentSSRsCln));
		}

		return sb.toString();
	}

	private String getDefaultMsgForPaxDetailsChange(String content) {
		if (!BeanUtils.nullHandler(content).equals("")) {
			return content;
		} else {
			return " No Change in Pax Details, ";
		}
	}

	/**
	 * @param srcReservation
	 * @param clnReservation
	 * @return
	 */
	private static Map<Integer, NameDTO> getNameChangedPassengerList(Reservation srcReservation, Reservation clnReservation) {
		ReservationPax reservationPaxSrv;
		ReservationPax reservationPaxCln;
		NameDTO paxName = null;
		Map<Integer, NameDTO> changedPassengersNames = new HashMap<Integer, NameDTO>();
		for (Iterator<ReservationPax> itSrcReservationPax = srcReservation.getPassengers().iterator(); itSrcReservationPax
				.hasNext();) {
			reservationPaxSrv = itSrcReservationPax.next();

			for (Iterator<ReservationPax> itClnReservationPax = clnReservation.getPassengers().iterator(); itClnReservationPax
					.hasNext();) {
				reservationPaxCln = itClnReservationPax.next();
				if (compareReservationObjects(reservationPaxSrv, reservationPaxCln, changedPassengersNames)) {
					break;
				}
			}
		}
		return changedPassengersNames;
	}

	private static boolean compareReservationObjects(ReservationPax reservationPaxSrv, ReservationPax reservationPaxCln,
			Map<Integer, NameDTO> changedPassengersNames) {
		boolean isBreakLoop = false;
		if (identicalPnrPaxIds(reservationPaxSrv.getPnrPaxId(), reservationPaxCln.getPnrPaxId())) {
			if (reservationPaxSrv.getTitle() != null && reservationPaxCln.getTitle() != null) {
				compareAndCreatePaxNameDTO(reservationPaxSrv, reservationPaxCln, changedPassengersNames);
				isBreakLoop = true;
			} else if (reservationPaxSrv.getTitle() == null && reservationPaxCln.getTitle() == null) {
				compareAndCreatePaxNameDTO(reservationPaxSrv, reservationPaxCln, changedPassengersNames);
				isBreakLoop = true;
			} else if (reservationPaxSrv.getTitle() == null && reservationPaxCln.getTitle() != null) {
				compareAndCreatePaxNameDTO(reservationPaxSrv, reservationPaxCln, changedPassengersNames);
				isBreakLoop = true;
			}
		}
		return isBreakLoop;
	}

	private static void compareAndCreatePaxNameDTO(ReservationPax reservationPaxSrv, ReservationPax reservationPaxCln,
			Map<Integer, NameDTO> changedPassengersNames) {
		if (isNameChanged(reservationPaxSrv, reservationPaxCln)) {
			createAndStoreNameDTO(reservationPaxSrv, changedPassengersNames);
		}
	}

	private static void createAndStoreNameDTO(ReservationPax reservationPax, Map<Integer, NameDTO> changedPassengersNames) {
		NameDTO paxName = new NameDTO();
		paxName.setFirstName(reservationPax.getFirstName());
		paxName.setLastName(reservationPax.getLastName());
		paxName.setPaxTitle(reservationPax.getTitle());
		paxName.setFamilyID(reservationPax.getFamilyID());
		changedPassengersNames.put(reservationPax.getPnrPaxId(), paxName);
	}

	private static boolean isNameChanged(ReservationPax reservationPaxSrv, ReservationPax reservationPaxCln) {
		boolean isEqual = false;
		if ((BeanUtils.compare(reservationPaxSrv.getFirstName().toUpperCase(), reservationPaxCln.getFirstName().toUpperCase(),
				"First Name").length() > 0)
				|| (BeanUtils.compare(reservationPaxSrv.getLastName().toUpperCase(),
						reservationPaxCln.getLastName().toUpperCase(), "LastName").length() > 0)) {
			isEqual = true;
		}
		return isEqual;
	}

	private static boolean identicalPnrPaxIds(Integer originalPaxId, Integer comparingPaxId) {
		return originalPaxId.equals(comparingPaxId);
	}

	/**
	 * @param srcReservation
	 * @param clnReservation
	 * @return
	 */
	private static Map<Integer, NameDTO> getTitleChangedPassengerList(Reservation srcReservation, Reservation clnReservation) {
		ReservationPax reservationPaxSrv;
		ReservationPax reservationPaxCln;
		NameDTO paxName;
		Map<Integer, NameDTO> changedPassengersNames = new HashMap<Integer, NameDTO>();
		for (Iterator<ReservationPax> itSrcReservationPax = srcReservation.getPassengers().iterator(); itSrcReservationPax
				.hasNext();) {
			reservationPaxSrv = itSrcReservationPax.next();

			if (!ReservationApiUtils.isTbaPassenger(reservationPaxSrv)) {

				for (Iterator<ReservationPax> itClnReservationPax = clnReservation.getPassengers().iterator(); itClnReservationPax
						.hasNext();) {
					reservationPaxCln = itClnReservationPax.next();

					if (reservationPaxSrv.getPnrPaxId().equals(reservationPaxCln.getPnrPaxId())) {
						if (reservationPaxSrv.getTitle() != null && reservationPaxCln.getTitle() != null) {
							if (BeanUtils.compare(reservationPaxSrv.getTitle().toUpperCase(),
									reservationPaxCln.getTitle().toUpperCase(), "Title").length() > 0) {
								paxName = new NameDTO();
								paxName.setFirstName(reservationPaxSrv.getFirstName());
								paxName.setLastName(reservationPaxSrv.getLastName());
								paxName.setPaxTitle(reservationPaxSrv.getTitle());
								paxName.setFamilyID(reservationPaxSrv.getFamilyID());
								changedPassengersNames.put(reservationPaxSrv.getPnrPaxId(), paxName);
							}
							break;
						} else if ((reservationPaxSrv.getTitle() == null && reservationPaxCln.getTitle() != null)
								|| (reservationPaxSrv.getTitle() != null && reservationPaxCln.getTitle() == null)) {
							paxName = new NameDTO();
							paxName.setFirstName(reservationPaxSrv.getFirstName());
							paxName.setLastName(reservationPaxSrv.getLastName());
							paxName.setPaxTitle(reservationPaxSrv.getTitle());
							paxName.setFamilyID(reservationPaxSrv.getFamilyID());
							changedPassengersNames.put(reservationPaxSrv.getPnrPaxId(), paxName);
							break;
						}

					}
				}
			}
		}
		return changedPassengersNames;
	}

}
