/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.messagebuilderfactory;

import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.builder.CALMessageBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.builder.PALMessageBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.AdlMessageBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessage;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlMessageBuilder;

/**
 * @author udithad
 *
 */
public class PnlAdlMessageBuilderFactory {

	public PnlAdlMessage getPnlAdlMessegeBuilder(String messageType){
		PnlAdlMessage message=null;
		if(MessageTypes.PNL.equals(messageType.toString())){
			message = new PnlMessageBuilder();
		}else if(MessageTypes.ADL.equals(messageType.toString())){
			message = new AdlMessageBuilder();
		}else if(MessageTypes.PAL.equals(messageType.toString())){
			message = new PALMessageBuilder();
		}else if(MessageTypes.CAL.equals(messageType.toString())){
			message = new CALMessageBuilder(messageType.toString());
		}
		return message;
	}
	
}
