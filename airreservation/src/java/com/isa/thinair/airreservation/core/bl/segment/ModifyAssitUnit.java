package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Assisting unit for the modify segment related functionality
 * 
 * This should contain only the supporting functionalities and all the business related should go inside BL and BO
 * 
 * @author Dilan Anuruddha
 * 
 */
public class ModifyAssitUnit {
	private Collection<Integer> cnxPnrSegIds;
	private Collection<Integer> newFltSegIds;
	private Collection<Integer> flownPnrSegIds;
	private Reservation reservation;
	private String pnr;
	private boolean fareGroupProcessed = false;
	private boolean voidOperation = false;

	// derived
	private int currentMaxSegSeq;
	private int currentMaxOndSeq;
	private int currentMaxReturnSeq;
	private int currentMaxJourneySeq;
	private Collection<Integer> exchangedPnrSegIds;
	private Map<Integer, Integer> exchangedPnrSegByFltSegIdMap = new HashMap<Integer, Integer>();
	private Map<Integer, Map<Integer, String>> exchangedPnrSegPaxStatusByPaxIdMap = new HashMap<Integer, Map<Integer, String>>();
	private ReservationDiscountDTO reservationDiscountDTO = null;

	public ModifyAssitUnit(Collection<Integer> cnxPnrSegIds, Collection<Integer> newFltSegIds, Reservation reservation,
			boolean isVoidOperation) {
		this.cnxPnrSegIds = cnxPnrSegIds;
		this.newFltSegIds = newFltSegIds;
		this.reservation = reservation;
		this.voidOperation = isVoidOperation;
	}

	public ModifyAssitUnit(Collection<Integer> cnxPnrSegIds, Collection<Integer> newFltSegIds, String pnr) {
		this.cnxPnrSegIds = cnxPnrSegIds;
		this.newFltSegIds = newFltSegIds;
		this.pnr = pnr;
	}

	public Collection<Integer> getNewFltSegIds() {
		return newFltSegIds;
	}

	public Collection<Integer> getFlownPnrSegIds() throws ModuleException {
		if (flownPnrSegIds == null) {
			Map<Integer, Collection<Integer>> paxWiseMap = CancellationUtils.getPaxWiseFlownPnrSegments(getReservation());
			if (paxWiseMap.keySet().size() != getReservation().getPassengers().size()) {
				throw new ModuleException("airreservation.requote.inconsistent.pax.flown.status");
			}
			int flowSegCount = -1;
			flownPnrSegIds = new ArrayList<Integer>();
			for (Integer paxId : paxWiseMap.keySet()) {
				if (flowSegCount == -1) {
					flowSegCount = paxWiseMap.get(paxId).size();
					flownPnrSegIds.addAll(paxWiseMap.get(paxId));
				} else if (flowSegCount != paxWiseMap.get(paxId).size() || !flownPnrSegIds.containsAll(paxWiseMap.get(paxId))) {
					throw new ModuleException("airreservation.requote.inconsistent.pax.flown.status");
				}
			}
		}
		return flownPnrSegIds;
	}

	public Collection<Integer> getCnxPnrSegIds() {
		return cnxPnrSegIds;
	}

	public boolean isVoidReservation() throws ModuleException {
		return !isUserModification() && voidOperation;
	}

	public boolean isSameFlightModification() throws ModuleException {
		List<Integer> allModPnrSegIds = new ArrayList<Integer>(cnxPnrSegIds);
		allModPnrSegIds.addAll(getExchangedPnrSegIds());
		return CancellationUtils.isSameFlightsModification(getReservation(), allModPnrSegIds, newFltSegIds, isUserModification());
	}

	public Reservation getReservation() throws ModuleException {
		// Loads the reservation
		if (this.reservation == null && pnr != null) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);

			this.reservation = ReservationProxy.getReservation(pnrModesDTO);
		}
		return reservation;
	}

	public boolean hasCnxPnrSegments() {
		return (cnxPnrSegIds != null && cnxPnrSegIds.size() > 0);
	}

	public boolean hasNewFltSegments() {
		return (newFltSegIds != null && newFltSegIds.size() > 0);
	}

	private boolean isCnxPnrSegId(Integer pnrSegId) {
		if (cnxPnrSegIds != null && cnxPnrSegIds.contains(pnrSegId)) {
			return true;
		}
		return false;
	}

	private boolean hasNewFlightSegment(Integer flightSegId) {
		if (newFltSegIds != null && newFltSegIds.contains(flightSegId)) {
			return true;
		}
		return false;
	}

	public boolean isUserCancellation() throws ModuleException {
		if (hasCnxPnrSegments()) {
			if (hasNewFltSegments()) {
				if (getExchangedPnrSegIds() != null && (getExchangedPnrSegIds().size()) == getNewFltSegIds().size()) {
					return true;
				}
			} else {
				return true;
			}
		}
		return false;
	}

	public boolean isCompleteExchange() throws ModuleException {
		if (hasNewFltSegments() && !hasCnxPnrSegments()) {
			if (getExchangedPnrSegIds() != null && (getExchangedPnrSegIds().size()) == getNewFltSegIds().size()) {
				return true;
			}
		}
		return false;
	}

	public boolean isUserModification() throws ModuleException {
		if (hasCnxPnrSegments()) {
			if (hasNewFltSegments()) {
				if (getExchangedPnrSegIds() != null && (getExchangedPnrSegIds().size() != getNewFltSegIds().size())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean skipMODChargeForOpenSegmentModification(boolean isOpenReturn) throws ModuleException {
		if (hasCnxPnrSegments() && isOpenReturn) {
			Collection<Integer> openPnrSegIds = new ArrayList<>();
			Collection<Integer> cnfFltSegIds = new ArrayList<>();
			Collection<Integer> cnfPnrSegIds = new ArrayList<>();
			if (hasNewFltSegments()) {
				if (getReservation() != null && getReservation().getSegments() != null
						&& !getReservation().getSegments().isEmpty()) {
					for (ReservationSegment reservationSegment : getReservation().getSegments()) {
						if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationSegment.getStatus())) {
							if (reservationSegment.getOpenReturnConfirmBeforeZulu() != null) {
								openPnrSegIds.add(reservationSegment.getPnrSegId());
							} else {
								cnfFltSegIds.add(reservationSegment.getFlightSegId());
							}

						}
					}
					return getCnxPnrSegIds().containsAll(openPnrSegIds)
							|| (isOpenReturn && isPossibleValidityExtend(cnfFltSegIds, cnfPnrSegIds));
				}

			}

		}
		return false;
	}

	private boolean isPossibleValidityExtend(Collection<Integer> cnfSegFlightIds, Collection<Integer> cnfPnrSegIds) {
		return (getNewFltSegIds().containsAll(cnfSegFlightIds) && getCnxPnrSegIds().containsAll(cnfPnrSegIds));
	}

	public boolean isSkipPenalty() throws ModuleException {
		if (isUserModification()) {
			List<Integer> allCnxPnrSegIds = new ArrayList<Integer>(cnxPnrSegIds);
			allCnxPnrSegIds.addAll(getExchangedPnrSegIds());
			return CancellationUtils.isSameFlightModification(getReservation(), allCnxPnrSegIds, newFltSegIds);
		} else if (isCompleteExchange()) {
			return true;
		}
		return false;
	}

	public boolean hasFlownPnrSegments() throws ModuleException {
		return (getFlownPnrSegIds() != null && getFlownPnrSegIds().size() > 0);
	}

	public Collection<Integer> getExchangedPnrSegIds() throws ModuleException {
		if (this.exchangedPnrSegIds == null) {
			processExchangedPnrSeg();
		}
		return exchangedPnrSegIds;
	}

	private void processExchangedPnrSeg() throws ModuleException {
		if (this.exchangedPnrSegIds == null) {
			this.exchangedPnrSegIds = new HashSet<Integer>();
			for (ReservationSegment resSeg : getReservation().getSegments()) {				
				if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& !BookingClass.BookingClassType.OPEN_RETURN.equals(resSeg.getBookingType())) {
					if (!isCnxPnrSegId(resSeg.getPnrSegId()) && hasNewFlightSegment(resSeg.getFlightSegId())) {
						exchangedPnrSegIds.add(resSeg.getPnrSegId());
						exchangedPnrSegByFltSegIdMap.put(resSeg.getFlightSegId(), resSeg.getPnrSegId());
						exchangedPnrSegPaxStatusByPaxIdMap.put(resSeg.getPnrSegId(), getPaxStatusByPnrPaxId(resSeg));
					}
				}
			}
		}
	}

	private Map<Integer, String> getPaxStatusByPnrPaxId(ReservationSegment resSegment) {
		Map<Integer, String> paxFareStatusByPnrPaxIdMap = new HashMap<Integer, String>();
		if (resSegment != null) {
			Reservation reservation = resSegment.getReservation();
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				Integer pnrPaxId = reservationPax.getPnrPaxId();

				for (ReservationPaxFare resPaxFare : reservationPax.getPnrPaxFares()) {
					for (ReservationPaxFareSegment resPaxFareSegment : resPaxFare.getPaxFareSegments()) {

						if (resSegment.getPnrSegId().equals(resPaxFareSegment.getPnrSegId())) {

							paxFareStatusByPnrPaxIdMap.put(pnrPaxId, resPaxFareSegment.getStatus());
						}
					}
				}

			}
		}

		return paxFareStatusByPnrPaxIdMap;
	}

	public Map<Integer, Integer> getExchangedPnrSegByFltSegIdMap() throws ModuleException {
		if (this.exchangedPnrSegByFltSegIdMap == null) {
			processExchangedPnrSeg();
		}

		return exchangedPnrSegByFltSegIdMap;
	}

	public Map<Integer, Map<Integer, String>> getExchangedPnrSegPaxStatusByPaxIdMap() throws ModuleException {
		if (this.exchangedPnrSegPaxStatusByPaxIdMap == null) {
			processExchangedPnrSeg();
		}

		return exchangedPnrSegPaxStatusByPaxIdMap;
	}

	public int getNextOndFareGroup() throws ModuleException {
		deriveNextFareGrpIds();
		return currentMaxOndSeq;
	}

	public int getNextReturnFareGroup() throws ModuleException {
		deriveNextFareGrpIds();
		return currentMaxReturnSeq;
	}

	public int getNextJourneyGroup() throws ModuleException {
		deriveNextFareGrpIds();
		return currentMaxJourneySeq;
	}

	private void deriveNextFareGrpIds() throws ModuleException {
		if (!fareGroupProcessed) {
			currentMaxSegSeq = -1;
			currentMaxOndSeq = -1;
			currentMaxReturnSeq = -1;
			currentMaxJourneySeq = -1;
			for (ReservationSegment pnrSeg : getReservation().getSegments()) {
				if (currentMaxSegSeq == -1 || currentMaxSegSeq < pnrSeg.getSegmentSeq()) {
					currentMaxSegSeq = pnrSeg.getSegmentSeq();
				}

				if (currentMaxOndSeq == -1 || currentMaxOndSeq < pnrSeg.getOndGroupId()) {
					currentMaxOndSeq = pnrSeg.getOndGroupId();
				}

				if (currentMaxReturnSeq == -1 || currentMaxReturnSeq < pnrSeg.getReturnOndGroupId()) {
					currentMaxReturnSeq = pnrSeg.getReturnOndGroupId();
				}

				if (currentMaxJourneySeq == -1 || currentMaxJourneySeq < pnrSeg.getJourneySequence()) {
					currentMaxJourneySeq = pnrSeg.getJourneySequence();
				}
			}
			if (currentMaxSegSeq == -1) { // Cannot ever happen (in requote) just to be on safe side
				currentMaxSegSeq = 0;
				currentMaxOndSeq = 0;
				currentMaxReturnSeq = 0;
				currentMaxJourneySeq = 0;
			}
			currentMaxSegSeq++;
			currentMaxOndSeq++;
			currentMaxReturnSeq++;
			currentMaxJourneySeq++;
			fareGroupProcessed = true;
		}
	}

	public int getNextSegmentSequence() throws ModuleException {
		deriveNextFareGrpIds();
		return currentMaxSegSeq;
	}

	public ReservationDiscountDTO getReservationDiscountDTO() {
		return reservationDiscountDTO;
	}

	public void setReservationDiscountDTO(ReservationDiscountDTO reservationDiscountDTO) {
		this.reservationDiscountDTO = reservationDiscountDTO;
	}
	
	public Map<String, BigDecimal> extractFareDiscountByPaxType(Collection<Integer> fltSegIds) {
		Map<String, BigDecimal> fareDiscountMap = new HashMap<String, BigDecimal>();
		fareDiscountMap.put(PaxTypeTO.ADULT, AccelAeroCalculator.getDefaultBigDecimalZero());
		fareDiscountMap.put(PaxTypeTO.CHILD, AccelAeroCalculator.getDefaultBigDecimalZero());
		fareDiscountMap.put(PaxTypeTO.INFANT, AccelAeroCalculator.getDefaultBigDecimalZero());
		if (fltSegIds != null && !fltSegIds.isEmpty() && reservationDiscountDTO != null
				&& reservationDiscountDTO.isDiscountExist()) {
			Map<Integer, PaxDiscountDetailTO> paxDiscountDetails = reservationDiscountDTO.getPaxDiscountDetails();

			if (paxDiscountDetails != null && !paxDiscountDetails.isEmpty()) {
				for (Integer paxSeq : paxDiscountDetails.keySet()) {
					PaxDiscountDetailTO paxDiscountDetailTO = paxDiscountDetails.get(paxSeq);

					if (paxDiscountDetailTO != null) {
						String paxType = paxDiscountDetailTO.getPaxType();
						if (fareDiscountMap.get(paxType).equals(AccelAeroCalculator.getDefaultBigDecimalZero())){
							Collection<DiscountChargeTO> discChargeTOs = paxDiscountDetailTO.getDiscountTOsBySegmentChargeCode(
									fltSegIds, ReservationInternalConstants.ChargeGroup.FAR);
							if (discChargeTOs != null && !discChargeTOs.isEmpty()) {
								BigDecimal discount = fareDiscountMap.get(paxType);
								for (DiscountChargeTO discChargeTO : discChargeTOs) {
									discount = AccelAeroCalculator.add(discount, discChargeTO.getDiscountAmount());
								}
	
								fareDiscountMap.put(paxType, discount);
	
							}
						}
					}

				}
			}
		}
		return fareDiscountMap;
	}
}
