/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.BaggageElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ChildElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class BaggageElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> baggageElementRuleExecutor = new BaggageElementRuleExecutor();

	private boolean isStartWithNewLine = false;

	@Override
	public void buildElement(ElementContext context) {
		if (context != null && context.getFeaturePack() != null
				&& context.getFeaturePack().isShowBaggage()) {
			initContextData(context);
			baggageElementTemplate();
		}
		executeNext();
	}

	private void baggageElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		buildBaggageElement(elementTemplate, passengerInformations);
	}

	private void ammender(StringBuilder elementTemplate) {
		currentElement = elementTemplate.toString();
		if (currentElement != null && !currentElement.isEmpty()) {
			ammendmentPreValidation(isStartWithNewLine, currentElement,
					uPContext, baggageElementRuleExecutor);
		}

	}

	// FIXME - REFACTOR TO SMALLE METHODS
	private void buildBaggageElement(StringBuilder elementTemplate,
			List<PassengerInformation> passengers) {
		int count = 0;
		elementTemplate.setLength(0);
		StringBuilder dummy = new StringBuilder();
		if (passengers.size() == 1) {
			AncillaryDTO baggage = passengers.get(0).getBaggages();
			if (baggage != null) {
				elementTemplate
						.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				elementTemplate.append(baggage.getCode() != null ? baggage
						.getCode()
						: MessageComposerConstants.PNLADLMessageConstants.SPBG);
				elementTemplate.append(MessageComposerConstants.SP);
				elementTemplate
						.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
				elementTemplate.append(MessageComposerConstants.SP);

				count = count + 1;
				elementTemplate.append(baggage.getDescription());
				ammender(elementTemplate);
				elementTemplate.setLength(0);
			}
		} else {
			boolean hasSameBaggage = true;
			String baggageDes = null;
			String bagCode = null;
			for (PassengerInformation passenger : passengers) {

				AncillaryDTO baggage = passenger.getBaggages();
				if (baggage != null) {
					bagCode = baggage.getCode();
					count = count + 1;
					if (baggageDes != null
							&& !baggageDes.equals(baggage.getDescription())) {
						hasSameBaggage = false;
					}
					baggageDes = baggage.getDescription();

				}
			}

			bagCode = (bagCode != null ? bagCode
					: MessageComposerConstants.PNLADLMessageConstants.SPBG);
			if (hasSameBaggage && count == passengers.size()) {
				elementTemplate
						.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				elementTemplate.append(bagCode);
				elementTemplate.append(MessageComposerConstants.SP);
				elementTemplate
						.append(MessageComposerConstants.PNLADLMessageConstants.HK
								+ count);
				elementTemplate.append(MessageComposerConstants.SP);
				elementTemplate.append(baggageDes);
				elementTemplate.append(MessageComposerConstants.SP);
				ammender(elementTemplate);
				elementTemplate.setLength(0);
			} else {
				for (PassengerInformation passenger : passengers) {
					AncillaryDTO baggage = passenger.getBaggages();
					if (baggage != null) {
						elementTemplate
								.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
						elementTemplate.append(bagCode);
						elementTemplate.append(MessageComposerConstants.SP);
						elementTemplate
								.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
						elementTemplate.append(MessageComposerConstants.SP);
						count = count + 1;
						elementTemplate.append(baggage.getDescription());
						elementTemplate.append(MessageComposerConstants.HYPHEN);
						elementTemplate.append(1);
						elementTemplate.append(passenger.getLastName());
						elementTemplate.append(MessageComposerConstants.FWD);
						elementTemplate.append(passenger.getFirstName());
						elementTemplate.append(passenger.getTitle());
						elementTemplate.append(MessageComposerConstants.SP);
						ammender(elementTemplate);
						elementTemplate.setLength(0);
					}
				}
			}
		}
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
