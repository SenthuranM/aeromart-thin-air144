package com.isa.thinair.airreservation.core.util;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PaymentUtil {

	private static Log log = LogFactory.getLog(PaymentUtil.class);

	public static void cancelPreviousOfflineTransaction(String pnr, AppIndicatorEnum appIndicator, boolean isCancelRequest) {
		try {
			CreditCardTransaction ccTransaction = ReservationModuleUtils.getPaymentBrokerBD()
					.loadOfflineTransactionByReference(pnr);
			if (ccTransaction != null) {
				String ipgName = ccTransaction.getPaymentGatewayConfigurationName();
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgName);
				if (ReservationModuleUtils.getPaymentBrokerBD().isOfflinePaymentsCancellationAllowed(ipgIdentificationParamsDTO)) {
					CreditCardPayment creditCardPayment = new CreditCardPayment();
					creditCardPayment.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
					creditCardPayment.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
					creditCardPayment.setTemporyPaymentId(ccTransaction.getTemporyPaymentId());
					creditCardPayment.setOfflinePaymentCancel(isCancelRequest);
					creditCardPayment.setPnr(pnr);
					ReservationModuleUtils.getPaymentBrokerBD().reverse(creditCardPayment, pnr, appIndicator, TnxModeEnum.SECURE_3D);
				}
			}

		} catch (ModuleException e) {
			log.error("Calling remote PaymentBrokerBD error ");

		}
	}

}
