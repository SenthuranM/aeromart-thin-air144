/**
 * 
 */
package com.isa.thinair.airreservation.core.util;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.StringTokenizer;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;

/**
 * @author nafly
 * 
 */
public class ReservationReminderUtil {

	/**
	 * 
	 * @param reservation
	 * @return
	 */
	public static String getFirstDepartureDate(Reservation reservation) {
		Collection<ReservationSegmentDTO> segments = reservation.getSegmentsView();

		Date departureDate = null;
		for (ReservationSegmentDTO reservationSegmentDTO : segments) {
			Date segmentDepartureDate = reservationSegmentDTO.getDepartureDate();
			if (departureDate == null || departureDate.compareTo(segmentDepartureDate) > 0) {
				departureDate = segmentDepartureDate;
			}
		}

		for (ExternalPnrSegment externalSegment : reservation.getExternalReservationSegment()) {
			Date segmentDepartureDate = externalSegment.getExternalFlightSegment().getEstTimeDepatureLocal();
			if (departureDate == null || departureDate.compareTo(segmentDepartureDate) > 0) {
				departureDate = segmentDepartureDate;
			}
		}
		return CalendarUtil.getDateInFormattedString("dd/MM/yyyy", departureDate);
	}

	/**
	 * 
	 * @param segments
	 * @return
	 * @throws ModuleException
	 */
	public static String getFinalDestination(Set<LCCClientReservationSegment> segments) throws ModuleException {

		Date departureDateOfLastSegment = null;
		String airPortCode = null;
		for (LCCClientReservationSegment lccClientReservationSegment : segments) {
			if (lccClientReservationSegment.getReturnFlag().equals("N")) {
				Date segmentDepartureDate = lccClientReservationSegment.getDepartureDate();

				if (departureDateOfLastSegment == null || departureDateOfLastSegment.compareTo(segmentDepartureDate) < 0) {
					departureDateOfLastSegment = segmentDepartureDate;
					airPortCode = getDestinationFromSegmentCode(lccClientReservationSegment.getSegmentCode());
				}
			}
		}

		return ReservationModuleUtils.getAirportBD().getAirport(airPortCode).getAirportName();
	}

	/**
	 * 
	 * @param reservation
	 * @return
	 */
	public static String getPrefLanguage(LCCClientReservation reservation) {
		String prefLanguage = AppSysParamsUtil.getSystemDefaultLanguage();
		if (reservation != null && reservation.getContactInfo() != null) {
			String language = reservation.getContactInfo().getPreferredLanguage();
			if (language != null && !language.trim().isEmpty()) {
				prefLanguage = language;
			}
		}
		return prefLanguage;
	}

	/**
	 * 
	 * @param segmentCode
	 * @return
	 */
	public static String getDestinationFromSegmentCode(String segmentCode) {
		return segmentCode.substring(segmentCode.lastIndexOf('/') + 1);
	}

	public static String getLogoImageName(String carrierCode) {
		String logoImageName = "LogoAni.gif";
		if (carrierCode != null) {
			logoImageName = "LogoAni" + carrierCode + ".gif";
		}
		return StaticFileNameUtil.getCorrected(logoImageName);
	}

	/**
	 * 
	 * @param bookingChannel
	 * @return
	 */
	public static String getBookingChannel(String bookingChannel) {
		String originBokingChannel = "";
		StringTokenizer st = new StringTokenizer(bookingChannel, ",");
		while (st.hasMoreTokens()) {
			originBokingChannel = st.nextToken();
			if (originBokingChannel.indexOf("|") > -1) {
				originBokingChannel = originBokingChannel.substring(originBokingChannel.indexOf("|") + 1);
				break;
			}

		}
		return originBokingChannel;

	}
}
