package com.isa.thinair.airreservation.core.bl.external;

import java.util.Comparator;

import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;

public class LatestFirstSortComparatorCCSales implements Comparator<Object> {
	
	public int compare(Object first, Object second) {

		CCSalesHistoryDTO historyDTO1 = (CCSalesHistoryDTO) first;
		CCSalesHistoryDTO historyDTO2 = (CCSalesHistoryDTO) second;

		return (historyDTO1.getDateOfsale() == null || historyDTO2.getDateOfsale() == null) ? 0 : historyDTO1.getDateOfsale()
				.compareTo(historyDTO2.getDateOfsale()) * -1;
	}

}
