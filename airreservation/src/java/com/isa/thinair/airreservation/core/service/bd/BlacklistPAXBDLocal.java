package com.isa.thinair.airreservation.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;

@Local
public interface BlacklistPAXBDLocal extends BlacklistPAXBD {

}
