package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ExternalFlexiChargesBL {

	public static void reflectExternalChgsForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		applyExternalChargesForAFreshBooking(reservation, credentialsDTO, paxDiscInfo, reservationDiscountDTO);
	}

	private static void applyExternalChargesForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		Set<ReservationPax> reservationPaxSet = reservation.getPassengers();
		boolean updateExist = false;

		for (ReservationPax reservationPax : reservationPaxSet) {
			PaymentAssembler paymentAssembler = (PaymentAssembler) reservationPax.getPayment();

			if (paymentAssembler != null) {
				Collection<ExternalChgDTO> externalCharges = paymentAssembler
						.getPerPaxExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);

				Collection<DiscountChargeTO> discountChargeTOs = null;
				if (reservationDiscountDTO != null) {
					discountChargeTOs = reservationDiscountDTO.getPaxDiscountChargeTOs(reservationPax.getPaxSequence());
				}

				for (ExternalChgDTO externalChgDTO : externalCharges) {
					Collection<ReservationPaxFare> colReservationPaxFare;
					if (externalChgDTO.isAppliesToInfant()) {
						ReservationPax infant = BeanUtils
								.getFirstElement((Collection<ReservationPax>) reservationPax.getInfants());
						colReservationPaxFare = infant.getPnrPaxFares();
					} else {
						colReservationPaxFare = reservationPax.getPnrPaxFares();
					}

					FlexiExternalChgDTO flexiExternalChgDTO = (FlexiExternalChgDTO) externalChgDTO;
					ReservationPaxFare reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(colReservationPaxFare,
							flexiExternalChgDTO.getFlightSegId(), true);

					ReservationApiUtils.updatePaxOndExternalChargeDiscountInfo(paxDiscInfo, externalChgDTO, discountChargeTOs,
							reservationPax.getPaxSequence());

					ReservationCoreUtils.captureReservationPaxOndCharge(flexiExternalChgDTO.getAmount(), null,
							flexiExternalChgDTO.getChgRateId(), flexiExternalChgDTO.getChgGrpCode(), reservationPaxFare,
							credentialsDTO, false, paxDiscInfo, null, null, 0);

					updateExist = true;
				}
			}
		}

		if (updateExist) {
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}
	}

	public static void reflectExternalChgForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Map<Integer, RevenueDTO> passengerRevenueMap,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen) throws ModuleException {
		// Apply these changes for the reservation
		Map<Integer, BigDecimal> mapPnrPaxIdAdjustments = applyExternalChargesForANewSegment(reservation, pnrPaxIdAndPayments,
				credentialsDTO, chgTxnGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, pnrPaxIdAndPayments);

		// Apply these changes to the passenger revenue map
		addExternalChgsForRevenueMap(reservation, mapPnrPaxIdAdjustments, passengerRevenueMap);
	}

	private static Map<Integer, BigDecimal> applyExternalChargesForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen)
			throws ModuleException {
		Map<Integer, BigDecimal> mapPnrPaxIdAdjustments = new HashMap<Integer, BigDecimal>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		BigDecimal appropriateChargePerPax;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;
		BigDecimal perPaxTotalAmount;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());

				if (paymentAssembler != null) {
					ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);

					if (ccExternalChgs != null && ccExternalChgs.size() > 0) {
						for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
							FlexiExternalChgDTO flexiExternalChgDTO = (FlexiExternalChgDTO) externalChgDTO;
							Collection<ReservationPaxFare> colReservationPaxFare;

							if (externalChgDTO.isAppliesToInfant()) {
								ReservationPax infant = BeanUtils
										.getFirstElement((Collection<ReservationPax>) reservationPax.getInfants());
								colReservationPaxFare = infant.getPnrPaxFares();
							} else {
								colReservationPaxFare = reservationPax.getPnrPaxFares();
							}

							reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(colReservationPaxFare,
									flexiExternalChgDTO.getFlightSegId(), true);
							ReservationCoreUtils.captureReservationPaxOndCharge(flexiExternalChgDTO.getAmount(), null,
									flexiExternalChgDTO.getChgRateId(), flexiExternalChgDTO.getChgGrpCode(), reservationPaxFare,
									credentialsDTO, false, null, null, null,
									chgTxnGen.getTnxSequence(reservationPax.getPnrPaxId()));

							appropriateChargePerPax = flexiExternalChgDTO.getAmount();
							perPaxTotalAmount = mapPnrPaxIdAdjustments.get(reservationPax.getPnrPaxId());

							if (perPaxTotalAmount == null) {
								mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), appropriateChargePerPax);
							} else {
								mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(),
										AccelAeroCalculator.add(perPaxTotalAmount, appropriateChargePerPax));
							}

							updateExist = true;
						}
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	/**
	 * Add external charges for payment assembler
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerPayment
	 */
	private static void addExternalChgsForPaymentAssembler(Map<Integer, BigDecimal> mapPnrPaxIdAdjustments,
			Map<Integer, PaymentAssembler> passengerPayment) {
		Iterator<Integer> itPnrPaxIds = passengerPayment.keySet().iterator();
		PaymentAssembler paymentAssembler;
		Integer pnrPaxId;
		BigDecimal chgAmount;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(pnrPaxId);
				if (paymentAssembler != null) {
					chgAmount = (BigDecimal) mapPnrPaxIdAdjustments.get(pnrPaxId);
					paymentAssembler
							.setTotalChargeAmount(AccelAeroCalculator.add(paymentAssembler.getTotalChargeAmount(), chgAmount));
				}
			}
		}
	}

	/**
	 * Add external charges for the revenue information
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerRevenueMap
	 */
	private static void addExternalChgsForRevenueMap(Reservation reservation, Map<Integer, BigDecimal> mapPnrPaxIdAdjustments,
			Map<Integer, RevenueDTO> passengerRevenueMap) {

		for (Iterator<ReservationPax> resPaxIterator = reservation.getPassengers().iterator(); resPaxIterator.hasNext();) {
			ReservationPax resPassenger = resPaxIterator.next();
			Integer pnrPaxId = resPassenger.getPnrPaxId();
			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId) && !ReservationApiUtils.isInfantType(resPassenger)) {
				RevenueDTO revenueDTO = (RevenueDTO) passengerRevenueMap.get(pnrPaxId);
				if (revenueDTO != null) {
					BigDecimal chgAmount = (BigDecimal) mapPnrPaxIdAdjustments.get(pnrPaxId);
					revenueDTO.setAddedTotal(AccelAeroCalculator.add(revenueDTO.getAddedTotal(), chgAmount));
				}
			}
		}
	}

}