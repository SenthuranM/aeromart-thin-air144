package com.isa.thinair.airreservation.core.util;

import java.util.ArrayList;
import java.util.List;

public class StringBufferedHandler implements Cloneable {
	private List<String> list = new ArrayList<String>();
	private String currentString = "";
	private int currentLine = -1;
	private int markedLine = -1;

	/**
	 * Line separator string. This is the value of the line.separator property at the moment that the stream was
	 * created.
	 */
	private String lineSeparator;

	public StringBufferedHandler() {
		lineSeparator = System.getProperty("line.separator");
	}

	public void write(String str) {
		currentString += str;
	}

	public void newLine() {
		list.add(currentString + lineSeparator);
		currentString = "";
	}

	/**
	 * Read a line of text.
	 */
	public String readLine() {
		if (list.size() > 0) {
			currentLine++;
			if (currentLine >= list.size()) {
				// EOF
				return null;
			}
			return (String) list.get(currentLine);
		} else {
			return null;
		}
	}

	/**
	 * Mark the current line
	 * 
	 * @param readAheadLimit
	 */
	public void mark(int readAheadLimit) {
		markedLine = currentLine;
	}

	/**
	 * Reset the next reading line to the marked line
	 * 
	 */
	public void reset() throws Exception {
		if (markedLine < 0) {
			throw new Exception("No line marked to reset");
		}
		currentLine = markedLine;
	}

	public void close() {
		list = new ArrayList<String>();
		currentString = "";
		currentLine = -1;
		markedLine = -1;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
