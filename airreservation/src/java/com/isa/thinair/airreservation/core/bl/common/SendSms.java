/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for sending a sms
 * 
 * @author Thushara Fernando
 * @since 1.0
 * @isa.module.command name="sendSms"
 */
public class SendSms extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SendSms.class);

	/**
	 * Execute method of the SendSms command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		ReservationContactInfo reservationContactInfo = (ReservationContactInfo) this
				.getParameter(CommandParamNames.RESERVATION_CONTACT_INFO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		// Checking params
		this.validateParams(pnr, credentialsDTO, reservationContactInfo);

		// Send sms if they need to pay
		try {
			if (AppSysParamsUtil.sendSMSForOnholdReservations()) {
				ReservationBO.sendSMSForOnHoldReservations(pnr, reservationContactInfo.getMobileNo(), credentialsDTO);
			}
		} catch (Exception exception) {
			log.error(exception);
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param reservation
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, CredentialsDTO credentialsDTO, ReservationContactInfo reservationContactInfo)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || credentialsDTO == null || reservationContactInfo == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}

}
