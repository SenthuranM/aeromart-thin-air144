package com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;

public abstract class NameElementBuilderTemplate extends BaseTemplateBuilder<PassengerElementsContext> {
	
	public abstract boolean isMorePaxToProceed();
	
	@Override
	public String buildElementTemplate(PassengerElementsContext t) {
		return null;
	}
}
