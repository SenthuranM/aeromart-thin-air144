package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.factory.ResModifyIdentificationRuleFactory;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @isa.module.command name="ruleRegistry"
 * 
 */
public class RuleRegistry extends DefaultBaseCommand {

	private ResModifyDtectorDataContext dataContext;
	private List<ResModifyIdentificationRule<RuleExecuterDatacontext>> ruleList = new ArrayList<>();
	private Map<String, Map<String, String>> observerWiseRulesAndAction;

	
	@Override
	public ServiceResponce execute() throws ModuleException {
		resolveCommandParameters();
		Set<String> ruleRefList = optimiseRuleOccurance();
		populateRuleList(ruleRefList);
		return createResponseObject();
	}

	private Set<String> optimiseRuleOccurance() {
		Set<String> ruleSet = new HashSet<>();

		if (observerWiseRulesAndAction != null) {

			for (String observerRef : observerWiseRulesAndAction.keySet()) {
				Map<String, String> ruleWiseActionMap = observerWiseRulesAndAction.get(observerRef);
					ruleSet.addAll(ruleWiseActionMap.keySet());
			}
		}
		return ruleSet;
	}

	private void populateRuleList(Set<String> ruleRefSet) {

		ResModifyIdentificationRule<RuleExecuterDatacontext> rule=null;
		
		Iterator<String> itr = ruleRefSet.iterator();
		ResModifyIdentificationRuleFactory factory = new ResModifyIdentificationRuleFactory(dataContext);
		while (itr.hasNext()) {
			String ruleRef = (String) itr.next();
			rule = factory.getResModifyIdentificationRule(ruleRef);
			if(rule != null){
				ruleList.add(rule);
			}
		}
	}

	private ServiceResponce createResponseObject() {
		ServiceResponce response = new DefaultServiceResponse();
		dataContext.setObserverWiseRulesAndActionMap(observerWiseRulesAndAction);
		response.addResponceParam(CommandParamNames.RES_MODIFY_DETECT_DATA_CONTEXT, dataContext);
		response.addResponceParam(CommandParamNames.RES_MODIFY_OPTIMIZED_RULE_LIST, ruleList);
		return response;
	}

	private void resolveCommandParameters() {
		dataContext = (ResModifyDtectorDataContext) this.getParameter(CommandParamNames.RES_MODIFY_DETECT_DATA_CONTEXT);
		observerWiseRulesAndAction = ReservationModuleUtils.getAirReservationConfig().getModificationDetectorObserverWiseRulesMap();	
	}

}
