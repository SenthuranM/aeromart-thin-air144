package com.isa.thinair.airreservation.core.bl.pal.dataStructure.masterinformation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.dataContext.CalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerCollectionStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PnlADlBaseDataStructure;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CalMasterDataStructure<K extends BasePassengerCollection, T extends BaseDataContext> extends PnlADlBaseDataStructure
		implements MessageDataStructure<PassengerCollection, CalDataContext> {

	private PassengerCollection passengerCollection = null;
	private CalDataContext context;
	private Collection<DestinationFare> destinationFares;
	private PassengerCollectionStructure passengerCollectionStructure;
	private List<DestinationFare> dummyDestinationFares = null;

	public CalMasterDataStructure() {
		passengerCollectionStructure = new PassengerCollectionStructure();
	}

	@Override
	public PassengerCollection getDataStructure(CalDataContext dataContext) throws ModuleException {

		this.context = dataContext;
		populateFlightIdentityInformation(dataContext);
		passengerCollectionStructure = new PassengerCollectionStructure();
		populateFareClassPassengerCountAndGroupCode(dataContext.getDepartureAirportCode(), dataContext.getFlightId());
		populateFareCabinRbdMap(dataContext.getFlightId());
		destinationFares = getPnlRelatedDestinationInformation(dataContext.getFlightId(), dataContext.getDepartureAirportCode(),
				dataContext.getFlightNumber());
		passengerCollection = passengerCollectionStructure.getPassengerCollection(destinationFares, cabinFareRbdMap);
		populatePnrWisePassengerCountsToPaxCollection();
		populateLastGroupCode();
		populateDummyDestinationFares();
		sortDestinationFareCollection(passengerCollection.getPnlAdlDestinationMap());
		return passengerCollection;

	}

	private void sortDestinationFareCollection(Map<String, List<DestinationFare>> destinationFares) {
		for (Map.Entry<String, List<DestinationFare>> entry : destinationFares.entrySet()) {
			passengerCollectionStructure.sortDestinationFares(entry.getValue());
		}
	}

	private void populateDummyDestinationFares() {
		for (String destinationAirportCode : destinationAirportCodes) {
			dummyDestinationFares = createDummyEntriesForNonUsedFareClasses(destinationAirportCode);
			if (dummyDestinationFares != null && !dummyDestinationFares.isEmpty()) {
				if (passengerCollection.getPnlAdlDestinationMap().containsKey(destinationAirportCode)) {
					passengerCollection.getPnlAdlDestinationMap().get(destinationAirportCode).addAll(dummyDestinationFares);
				}
			}
		}
	}

	private void populateLastGroupCode() {
		passengerCollection.setLastGroupCode(lastGroupCode);
	}

	@SuppressWarnings("unchecked")
	private void populatePnrWisePassengerCountsToPaxCollection() {
		passengerCollection.setGroupCodes(PnlAdlUtil.populateTourIds());
		passengerCollection.setPnrWisePassengerCount(pnrWisePassengerCount);
		passengerCollection.setPnrWisePassengerReductionCount(pnrWisePassengerReductionCount);
		passengerCollection.setPnrPaxIdvsSegmentIds(pnrPaxIdvsSegmentIds);
		passengerCollection.setPnrCollection(pnrCollection);
		passengerCollection.setFareClassWisePaxCount(fareClassWisePaxCount);
		passengerCollection.setPassengerInformations(passengerInformations);
	}

	private Collection<DestinationFare> getPnlRelatedDestinationInformation(Integer flightId, String departureAirportCode,
			String flightNumber) throws ModuleException {

		Collection<DestinationFare> destinationFares = null;
		Timestamp currentTimeStamp = new Timestamp(new Date().getTime());
		List<PassengerInformation> passengerInformation = auxilliaryDAO.getCALPassengerDetails(flightId, departureAirportCode,
				currentTimeStamp, context.getCarrierCode());

		List<PassengerInformation> allDeletedAdlPassengers = auxilliaryDAO.getDeletedCALPassengerDetails(flightId,
				departureAirportCode, currentTimeStamp, context.getCarrierCode());
		if (passengerInformation == null) {
			passengerInformation = new ArrayList<PassengerInformation>();
		}
		populateAdditionalPassengers(passengerInformation, allDeletedAdlPassengers);

		if (passengerInformation != null && !passengerInformation.isEmpty()) {
			destinationFares = getDestinationFareCollectionBy(passengerInformation);
		} else {
			throw new ModuleException(PNLConstants.ERROR_CODES.EMPTY_CAL_ERROR_CODE, "airreservations");
		}

		return destinationFares;
	}

	private void populateAdditionalPassengers(List<PassengerInformation> passengerInformations,
			List<PassengerInformation> allDeletedCALPassengers) {
		if (allDeletedCALPassengers != null && !allDeletedCALPassengers.isEmpty()) {
			for (PassengerInformation passengerInformation : allDeletedCALPassengers) {
				if (!passengerInformations.contains(passengerInformation)) {
					passengerInformations.add(passengerInformation);
				}
			}

		}
	}

	private void populateFareClassPassengerCountAndGroupCode(String departureAirportCode, Integer flightId) {
		Object[] pnlInformation = auxilliaryDAO.getPaxCountInPALCAL(flightId, departureAirportCode);
		fareClassPaxCountMap = (HashMap<String, Integer>) pnlInformation[0];
		lastGroupCode = (String) pnlInformation[1];
	}

}
