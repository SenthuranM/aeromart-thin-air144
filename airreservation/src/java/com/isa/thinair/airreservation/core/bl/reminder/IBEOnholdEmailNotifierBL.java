package com.isa.thinair.airreservation.core.bl.reminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.OnholdReservatoinSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.OnholdAlert;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.PnrZuluProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.util.I18NMessagingUtil;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

public class IBEOnholdEmailNotifierBL {
	private final ReservationDAO reservationDAO;
	private final MessagingServiceBD messagingServiceBD;
	private final Log log = LogFactory.getLog(IBEOnholdEmailNotifierBL.class);

	public IBEOnholdEmailNotifierBL() {
		this.reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		this.messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
	}

	public void sendNotifications(CredentialsDTO credentialsDTO, OnholdReservatoinSearchDTO onHoldReservationSearchDTO)
			throws ModuleException {
		Collection<String> pnrs = reservationDAO.getOnHoldReservationPnrsForEmailNotification(onHoldReservationSearchDTO);
		List<MessageProfile> messageProfileList = new ArrayList<MessageProfile>();
		for (String pnr : pnrs) {

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadLocalTimes(true);
			pnrModesDTO.setLoadFares(true);
			Reservation reservation = PnrZuluProxy.getReservationWithLocalTimes(pnrModesDTO, credentialsDTO);

			ReservationContactInfo contactInfo = reservation.getContactInfo();
			UserMessaging userMessaging = new UserMessaging();
			userMessaging.setFirstName(contactInfo.getFirstName());
			userMessaging.setLastName(contactInfo.getLastName());
			userMessaging.setToAddres(contactInfo.getEmail());

			Locale locale = new Locale(getPrefLanguage(reservation));

			List<UserMessaging> messages = new ArrayList<UserMessaging>();
			messages.add(userMessaging);

			HashMap<String, Object> onHoldEmailDataMap = getOnHoldEmailDataMap(reservation);
			// if onhold displayRelease time == 0:0 ; not seding the payment reminder email
			String displayReleasTime = onHoldEmailDataMap.get("displayReleaseTime").toString();

			if (Util.getHHMMTimeDurationInMints(displayReleasTime) == 0) {
				continue;
			}
			String preferredLanguage = reservation.getContactInfo().getPreferredLanguage();
			Map<String, String> labels = I18NMessagingUtil.getIBEOnHoldEmailMessages(locale);
			onHoldEmailDataMap.put("label", labels);
			String direction = "ltr";
			if (preferredLanguage.equals("ar")) {
				direction = "rtl";
			}
			onHoldEmailDataMap.put("direction", direction);

			Topic topic = new Topic();
			topic.setTopicParams(onHoldEmailDataMap);
			topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.IBE_ONHOLD_EMAIL);
			topic.setAttachMessageBody(true);
			List<ReservationAudit> audits = new ArrayList<ReservationAudit>();

			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setModificationType(AuditTemplateEnum.ONHOLD_EMAIL.getCode());
			ReservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OnholdAlert.EMAIL_ADDRESS, contactInfo.getEmail());

			audits.add(reservationAudit);
			topic.setAuditInfo(audits);

			MessageProfile messageProfile = new MessageProfile();
			messageProfile.setUserMessagings(messages);
			messageProfile.setTopic(topic);
			OnholdAlert onholdAlert;
			messageProfileList.add(messageProfile);
			onholdAlert = reservationDAO.getOnholdAlert(pnr);
			if (onholdAlert == null) {
				onholdAlert = new OnholdAlert();
				onholdAlert.setPnr(pnr);
				onholdAlert.setChannelCode(onHoldReservationSearchDTO.getOwnerChannelCode());
				onholdAlert.setAlertSent(ReservationInternalConstants.OnholdAlertStatus.ATTEMPTED);
				onholdAlert.setAlertType(ReservationInternalConstants.OHDAlertTypes.PAYMENT_FAILURES.code());
				onholdAlert.setSentTimeStamp(new Date());
			} else {
				onholdAlert.setSentTimeStamp(new Date());
			}
			reservationDAO.saveOnholdAlert(onholdAlert);
		}

		messagingServiceBD.sendMessages(messageProfileList);
	}

	private String getPrefLanguage(Reservation reservation) {
		String prefLanguage = AppSysParamsUtil.getSystemDefaultLanguage();
		if (reservation != null && reservation.getContactInfo() != null) {
			String language = reservation.getContactInfo().getPreferredLanguage();
			if (language != null && !language.trim().isEmpty()) {
				prefLanguage = language;
			}
		}
		return prefLanguage;
	}

	private HashMap<String, Object> getOnHoldEmailDataMap(Reservation reservation) throws ModuleException {

		Date currentTime = CalendarUtil.getCurrentSystemTimeInZulu();
		String ibeURL = AppSysParamsUtil.getSecureIBEUrl();
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pnr", reservation.getPnr());
		String departureDate = getFirstDepartureDate(reservation);
		String lastName = reservation.getContactInfo().getLastName();
		String pnr = reservation.getPnr();
		Date releaseTimeStamp = reservation.getReleaseTimeStamps()[0];
		String releaseTime = CalendarUtil.getDateInFormattedString("HH:mm", releaseTimeStamp);
		long timeDifference = 0;

		if (releaseTimeStamp != null) {
			timeDifference = releaseTimeStamp.getTime() - currentTime.getTime();
			if (timeDifference <= 0) {
				timeDifference = 0;
			}
		}

		dataMap.put("releaseTime", releaseTime);
		dataMap.put("destination",
				getFinalDestination(reservation.getSegmentsView(), reservation.getExternalReservationSegment()));
		ReservationContactInfo contactInfo = reservation.getContactInfo();
		dataMap.put("customerName",
				(new StringBuilder()).append(contactInfo.getTitle()).append(" ").append(contactInfo.getFirstName()).append(" ")
						.append(contactInfo.getLastName()).toString());
		dataMap.put("displayReleaseTime", Util.getTimeHHMM(timeDifference));
		String encoding = "UTF-8";
		StringBuilder urlBuild = new StringBuilder();
		try {
			urlBuild.append(AppSysParamsUtil.getSecureServiceAppIBEUrl());
			if (!urlBuild.toString().endsWith("/"))
				urlBuild.append("/");
			if (!urlBuild.toString().contains("ibe"))
				urlBuild.append("ibe/");
			urlBuild.append("reservation.html#/modify/reservation/");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
			Date d = sdf.parse(departureDate);
			sdf.applyPattern("yyyy-mm-dd");
			String departureDateNew = sdf.format(d);
			urlBuild.append((new StringBuilder()).append(getPrefLanguage(reservation).toUpperCase()).append("/")
							.append(reservation.getLastCurrencyCode()).append("//").append(pnr).append("/").append(lastName).append("/")
							.append(departureDateNew).toString());
			// urlBuild.append("showReservation.action?");
			// urlBuild.append("hdnMode=").append("MAKEPAYMENT");
			// urlBuild.append("&hdnParamData=").append(
			// URLEncoder.encode((new StringBuilder()).append(getPrefLanguage(reservation).toUpperCase()).append("^MB^")
			// .append(pnr).append("^").append(lastName).append("^").append(departureDate).toString(), encoding));
		} catch (Exception e) {
			log.error(e);
		}
		dataMap.put("url", urlBuild.toString());
		dataMap.put("callCenterUrl", AppSysParamsUtil.getCallCenterUrl());
		dataMap.put("salesShopUrl", AppSysParamsUtil.getSalesShopUrl());
		dataMap.put("cashCollectionUrl", AppSysParamsUtil.getCashCollectionUrl());
		ibeURL = ibeURL.replace("/public/","");
		dataMap.put("ibeURL", ibeURL);
		return dataMap;
	}

	private String getFirstDepartureDate(Reservation reservation) {
		Collection<ReservationSegmentDTO> segments = reservation.getSegmentsView();

		Date departureDate = null;
		for (ReservationSegmentDTO reservationSegmentDTO : segments) {
			Date segmentDepartureDate = reservationSegmentDTO.getDepartureDate();
			if (departureDate == null || departureDate.compareTo(segmentDepartureDate) > 0) {
				departureDate = segmentDepartureDate;
			}
		}

		for (ExternalPnrSegment externalSegment : reservation.getExternalReservationSegment()) {
			Date segmentDepartureDate = externalSegment.getExternalFlightSegment().getEstTimeDepatureLocal();
			if (departureDate == null || departureDate.compareTo(segmentDepartureDate) > 0) {
				departureDate = segmentDepartureDate;
			}
		}
		return CalendarUtil.getDateInFormattedString("dd/MM/yyyy", departureDate);
	}

	private String getFinalDestination(Collection<ReservationSegmentDTO> segments,
			Collection<ExternalPnrSegment> externalPnrSegments) throws ModuleException {

		Date departureDateOfLastSegment = null;
		String airPortCode = null;
		for (ReservationSegmentDTO reservationSegmentDTO : segments) {
			Date segmentDepartureDate = reservationSegmentDTO.getDepartureDate();

			if (departureDateOfLastSegment == null || departureDateOfLastSegment.compareTo(segmentDepartureDate) < 0) {
				departureDateOfLastSegment = segmentDepartureDate;
				airPortCode = getDestinationFromSegmentCode(reservationSegmentDTO.getSegmentCode());
			}
		}

		for (ExternalPnrSegment externalSegment : externalPnrSegments) {
			Date segmentDepartureDate = externalSegment.getExternalFlightSegment().getEstTimeDepatureLocal();

			if (departureDateOfLastSegment == null || departureDateOfLastSegment.compareTo(segmentDepartureDate) < 0) {
				departureDateOfLastSegment = segmentDepartureDate;
				airPortCode = getDestinationFromSegmentCode(externalSegment.getExternalFlightSegment().getSegmentCode());
			}
		}

		return ReservationModuleUtils.getAirportBD().getAirport(airPortCode).getAirportName();
	}

	private String getDestinationFromSegmentCode(String segmentCode) {
		return segmentCode.substring(segmentCode.lastIndexOf('/') + 1);
	}

	public void updateOnHoldAlert(String pnr, char newStatus) {
		OnholdAlert onholdAlert = reservationDAO.getOnholdAlert(pnr);
		if (onholdAlert != null) {
			onholdAlert.setAlertSent(newStatus);
			reservationDAO.saveOnholdAlert(onholdAlert);
		}
	}
}
