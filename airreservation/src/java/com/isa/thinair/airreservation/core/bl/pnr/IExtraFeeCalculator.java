package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author rumesh
 * 
 */
public interface IExtraFeeCalculator {
	public void calculate() throws ModuleException;

	public Collection<? extends ExternalChgDTO> getPaxExtraFee(Integer pnrPaxId);

	public boolean hasExtraFeeFor(Integer pnrPaxId);
}
