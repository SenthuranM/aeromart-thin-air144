package com.isa.thinair.airreservation.core.bl.pal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pal.PALTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PalCalTiming;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.pnl.PnlSchedulingBL;
import com.isa.thinair.airreservation.core.persistence.dao.PalCalTimingDAO;

public class PalScheduleBL {

	private static Log log = LogFactory.getLog(PnlSchedulingBL.class);

	private PalCalTimingDAO palCalTimingDAO = null;

	public PalScheduleBL() {
		this.palCalTimingDAO = ReservationDAOUtils.DAOInstance.PAL_CAL_TIMINGS_DAO;
	}

	public ArrayList<PALTransMissionDetailsDTO> getFlightForPalCalScheduling() {

		ArrayList<PALTransMissionDetailsDTO> palTransmitionDetailsList = new ArrayList<PALTransMissionDetailsDTO>();

		try {

			Date date = new Date();
			List<PalCalTiming> actPalCAlTiming = palCalTimingDAO.getActiveAirportPALCAlTimings(date);

			for (PalCalTiming timing : actPalCAlTiming) {
				if(palCalTimingDAO.hasValidSitaConfigsForAirPort(timing.getAirportCode())){
					ArrayList<PALTransMissionDetailsDTO> airportFlights = palCalTimingDAO.getFlightsForPALSchedulingForAirport(date,
						timing);
					palTransmitionDetailsList.addAll(airportFlights);
				}
			}
			
			
		} catch (Exception e) {
			log.error("############# [PALCAL SCHEDULING] ERROR OCCURED WHEN MANIPULATING NEW TIMINGS", e);
		}

		return palTransmitionDetailsList;
	}
}
