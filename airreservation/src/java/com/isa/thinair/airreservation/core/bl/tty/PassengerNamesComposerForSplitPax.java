package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;

public class PassengerNamesComposerForSplitPax implements PassengerNamesComposingStrategy {

	@Override
	public List<SSRDTO> composePassengerNames(Reservation reservation, BookingRequestDTO bookingRequestDTO,
			TypeBRequestDTO typeBRequestDTO) {
		List<NameDTO> splitPaxNames = typeBRequestDTO.getSplitPaxNames();
		List<SSRDTO> ssrDTOs = new ArrayList<SSRDTO>();
		bookingRequestDTO.setNewNameDTOs(splitPaxNames);
		return ssrDTOs;
	}
}
