package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.dto.FareInfoTOV2;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityRules;
import com.isa.thinair.airreservation.core.bl.tax.OndChargeUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;

public class BalanceToPayBreakdownBL {

	private static Log log = LogFactory.getLog(BalanceToPayBreakdownBL.class);

	// TODO need to refactor these hardcoded tax codes
	private static enum JN_ESU_TAXES {
		JNANCI, JNEXT, JNOTHER;
	};

	public static void constructBalanceToPayBreakdown(ReservationPax passenger, SegmentSummaryTOV2 balanceSummary,
			LCCClientPassengerSummaryTO paxSummary, boolean isInfantPaymentSeparated) {
		FareInfoTOV2 currentNonRefundables = balanceSummary.getCurrentNonRefundableAmounts();
		Map<Integer, List<ChargeMetaTO>> nonRefundableCharges = new HashMap<Integer, List<ChargeMetaTO>>();
		List<BigDecimal> nonRefundableFares = new ArrayList<BigDecimal>();

		if (currentNonRefundables.getTotalPrice().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			for (ChargeMetaTO chargeMetaTO : currentNonRefundables.getFare()) {
				nonRefundableFares.add(chargeMetaTO.getChargeAmount());
			}
			for (ChargeMetaTO chargeMetaTO : currentNonRefundables.getSurcharges()) {
				if (nonRefundableCharges.get(chargeMetaTO.getChargeRateId()) != null) {
					nonRefundableCharges.get(chargeMetaTO.getChargeRateId()).add(chargeMetaTO);
				} else {
					List<ChargeMetaTO> nonRefCharges = new ArrayList<ChargeMetaTO>();
					nonRefCharges.add(chargeMetaTO);
					nonRefundableCharges.put(chargeMetaTO.getChargeRateId(), nonRefCharges);
				}
			}
			for (ChargeMetaTO chargeMetaTO : currentNonRefundables.getTaxes()) {
				if (nonRefundableCharges.get(chargeMetaTO.getChargeRateId()) != null) {
					nonRefundableCharges.get(chargeMetaTO.getChargeRateId()).add(chargeMetaTO);
				} else {
					List<ChargeMetaTO> nonRefCharges = new ArrayList<ChargeMetaTO>();
					nonRefCharges.add(chargeMetaTO);
					nonRefundableCharges.put(chargeMetaTO.getChargeRateId(), nonRefCharges);
				}
			}
			for (ChargeMetaTO chargeMetaTO : currentNonRefundables.getAdjustments()) {
				if (nonRefundableCharges.get(chargeMetaTO.getChargeRateId()) != null) {
					nonRefundableCharges.get(chargeMetaTO.getChargeRateId()).add(chargeMetaTO);
				} else {
					List<ChargeMetaTO> nonRefCharges = new ArrayList<ChargeMetaTO>();
					nonRefCharges.add(chargeMetaTO);
					nonRefundableCharges.put(chargeMetaTO.getChargeRateId(), nonRefCharges);
				}
			}
			// TODO add other non-refundables
		}

		Collection<Integer> ppocIds = new ArrayList<Integer>();
		Collection<ReservationPaxOndCharge> colReservationPaxOndCharges = new ArrayList<ReservationPaxOndCharge>();
		for (ReservationPaxFare reservationPaxFare : passenger.getPnrPaxFares()) {
			for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxFare.getCharges()) {
				if (!checkNonRefundableCharges(reservationPaxOndCharge, nonRefundableCharges, nonRefundableFares)) {
					colReservationPaxOndCharges.add(reservationPaxOndCharge);
					ppocIds.add(reservationPaxOndCharge.getPnrPaxOndChgId());
				}
			}
		}

		// Add infant charges for parent
		if (!isInfantPaymentSeparated && passenger.getInfants() != null && passenger.getInfants() != null) {
			for (ReservationPax reservationPax : passenger.getInfants()) {
				for (ReservationPaxFare infantPaxFare : reservationPax.getPnrPaxFares()) {
					for (ReservationPaxOndCharge reservationPaxOndCharge : infantPaxFare.getCharges()) {
						if (!checkNonRefundableCharges(reservationPaxOndCharge, nonRefundableCharges, nonRefundableFares)) {
							colReservationPaxOndCharges.add(reservationPaxOndCharge);
							ppocIds.add(reservationPaxOndCharge.getPnrPaxOndChgId());
						}

					}
				}
			}
		}

		OndChargeUtil.setChargeCodeInPaxCharges(colReservationPaxOndCharges, ppocIds);

		FareInfoTOV2 newCharges = balanceSummary.getNewAmounts();
		ArrayList<ChargeMetaTO> colNewCharges = new ArrayList<ChargeMetaTO>();
		colNewCharges.addAll(newCharges.getSurcharges());
		colNewCharges.addAll(newCharges.getTaxes());
		colNewCharges.addAll(newCharges.getFare());
		colNewCharges.addAll(newCharges.getAdjustments());
		colNewCharges.addAll(newCharges.getCancellations());
		colNewCharges.addAll(newCharges.getModifications());
		colNewCharges.addAll(newCharges.getDiscounts());
		colNewCharges.addAll(newCharges.getPenalty());

		BigDecimal currentModificationChg = balanceSummary.getIdentifiedTotalModCharge();
		if (currentModificationChg.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(ReservationInternalConstants.ChargeGroup.MOD,
					currentModificationChg, new Date(), new BigDecimal(0), null);
			colNewCharges.add(metaTO);
		}
		BigDecimal currentCancellationChg = balanceSummary.getIdentifiedTotalCnxCharge();
		if (currentCancellationChg.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(ReservationInternalConstants.ChargeGroup.CNX,
					currentCancellationChg, new Date(), new BigDecimal(0), null);
			colNewCharges.add(metaTO);
		}
		BigDecimal newPenalty = balanceSummary.getModificationPenatly();
		if (newPenalty.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			ChargeMetaTO metaTO = ChargeMetaTO.createBasicChargeMetaTO(ReservationInternalConstants.ChargeGroup.PEN, newPenalty,
					new Date(), new BigDecimal(0), null);
			colNewCharges.add(metaTO);
		}

		Comparator<ChargeMetaTO> chargeCodeComparator = new Comparator<ChargeMetaTO>() {
			public int compare(ChargeMetaTO c1, ChargeMetaTO c2) {

				if (c1.getChargeCode() != null && c2.getChargeCode() != null) {
					return c1.getChargeCode().compareTo(c2.getChargeCode());
				} else if (c1.getChargeCode() == null && c2.getChargeCode() != null) {
					return 1;
				} else if (c1.getChargeCode() != null && c2.getChargeCode() == null) {
					return -1;
				} else {
					return 0;
				}
			}
		};

		Collections.sort(colNewCharges, chargeCodeComparator);

		for (ChargeMetaTO chargeMetaTO : colNewCharges) {
			if (chargeMetaTO.getChargeCode() != null && (chargeMetaTO.getChargeCode().endsWith(JN_ESU_TAXES.JNANCI.toString())
					|| chargeMetaTO.getChargeCode().endsWith(JN_ESU_TAXES.JNEXT.toString())
					|| chargeMetaTO.getChargeCode().endsWith(JN_ESU_TAXES.JNOTHER.toString()))) {
				chargeMetaTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.TAX);
			}
		}

		consumeExactCreditAmounts(colReservationPaxOndCharges, colNewCharges);

		if (colReservationPaxOndCharges.size() > 0) {
			// consume charges from same group
			consumeRemainingCreditAmounts(colReservationPaxOndCharges, colNewCharges, false);
			if (colReservationPaxOndCharges.size() > 0) {
				// consume charges from same groups
				consumeRemainingCreditAmounts(colReservationPaxOndCharges, colNewCharges, true);
			}
		}

		if (colNewCharges.size() > 0 && colReservationPaxOndCharges.size() == 0) {
			groupRemainingChargesForProducts(colNewCharges, paxSummary);
		} else {
			// This should not occur. If occur there should be issue in charge consume logic or balance to pay amount
		}

	}

	private static boolean checkNonRefundableCharges(ReservationPaxOndCharge reservationPaxOndCharge,
			Map<Integer, List<ChargeMetaTO>> nonRefundableCharges, List<BigDecimal> nonRefundableFares) {
		if (reservationPaxOndCharge.getChargeRateId() != null
				&& nonRefundableCharges.containsKey(reservationPaxOndCharge.getChargeRateId())
				&& nonRefundableCharges.get(reservationPaxOndCharge.getChargeRateId()).size() > 0) {
			List<ChargeMetaTO> nonRefCharges = nonRefundableCharges.get(reservationPaxOndCharge.getChargeRateId());
			Iterator<ChargeMetaTO> nonRefChargesIte = nonRefCharges.iterator();
			while (nonRefChargesIte.hasNext()) {
				ChargeMetaTO chargeMetaTO = nonRefChargesIte.next();
				if (chargeMetaTO.getChargeAmount().equals(reservationPaxOndCharge.getAmount())) {
					nonRefChargesIte.remove();
					break;
				}
			}
			return true;
		} else if (reservationPaxOndCharge.getChargeRateId() == null && nonRefundableFares.size() > 0) {
			Iterator<BigDecimal> nonRefFareIte = nonRefundableFares.iterator();
			while (nonRefFareIte.hasNext()) {
				BigDecimal nonRefundFare = nonRefFareIte.next();
				if (nonRefundFare.equals(reservationPaxOndCharge.getAmount())) {
					nonRefFareIte.remove();
					return true;
				}
			}
		}
		return false;
	}

	private static void groupRemainingChargesForProducts(ArrayList<ChargeMetaTO> colNewCharges,
			LCCClientPassengerSummaryTO paxSummary) {
		try {
			Map<String, Map<String, BigDecimal>> carrierProductCodeWiseAmountDueMap = new HashMap<String, Map<String, BigDecimal>>();
			Map<String, BigDecimal> productCodeWiseAmountDue = new HashMap<String, BigDecimal>();
			List<LoyaltyProduct> loyaltyProducts = ReservationModuleUtils.getLoyaltyManagementBD().getLoyaltyProducts();
			resetProductCodeWiseMap(productCodeWiseAmountDue, loyaltyProducts);
			if (colNewCharges.size() > 0 && loyaltyProducts != null && loyaltyProducts.size() > 0) {
				for (ChargeMetaTO chargeMetaTO : colNewCharges) {
					allocateChargetoProduct(chargeMetaTO, loyaltyProducts, productCodeWiseAmountDue);
				}
			}
			carrierProductCodeWiseAmountDueMap.put(AppSysParamsUtil.getDefaultCarrierCode(), productCodeWiseAmountDue);
			paxSummary.setCarrierProductCodeWiseAmountDue(carrierProductCodeWiseAmountDueMap);
		} catch (ModuleException exp) {
			log.error("Unable to retrieve loyalty product codes");
		}

	}

	private static void resetProductCodeWiseMap(Map<String, BigDecimal> productCodeWiseAmountDue,
			List<LoyaltyProduct> loyaltyProducts) {
		for (LoyaltyProduct loyaltyProduct : loyaltyProducts) {
			productCodeWiseAmountDue.put(loyaltyProduct.getProductId(), AccelAeroCalculator.getDefaultBigDecimalZero());
		}
		// TAX,CNX,MOD,ADJ,PEN
		productCodeWiseAmountDue.put("OTHER", AccelAeroCalculator.getDefaultBigDecimalZero());

	}

	private static void allocateChargetoProduct(ChargeMetaTO chargeMetaTO, List<LoyaltyProduct> loyaltyProducts,
			Map<String, BigDecimal> productCodeWiseAmountDue) {
		if (chargeMetaTO.getChargeCode() != null) {
			boolean chargeAllocated = false;
			for (LoyaltyProduct loyaltyProduct : loyaltyProducts) {
				for (String chargeCode : loyaltyProduct.getChargeCodes()) {
					if (chargeMetaTO.getChargeCode().equals(chargeCode)) {
						BigDecimal productCharge = AccelAeroCalculator
								.add(productCodeWiseAmountDue.get(loyaltyProduct.getProductId()), chargeMetaTO.getChargeAmount());
						productCodeWiseAmountDue.put(loyaltyProduct.getProductId(), productCharge);
						chargeAllocated = true;
						break;
					}
				}
			}
			if (!chargeAllocated) {
				if (chargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.SUR)) {
					BigDecimal productCharge = AccelAeroCalculator.add(
							productCodeWiseAmountDue.get(ReservationInternalConstants.LOYALTY_PRODUCTS.FLIGHT.toString()),
							chargeMetaTO.getChargeAmount());
					productCodeWiseAmountDue.put(ReservationInternalConstants.LOYALTY_PRODUCTS.FLIGHT.toString(), productCharge);
				} else {
					BigDecimal productCharge = AccelAeroCalculator.add(productCodeWiseAmountDue.get("OTHER"),
							chargeMetaTO.getChargeAmount());
					productCodeWiseAmountDue.put("OTHER", productCharge);
				}
			}
		} else if (chargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
			BigDecimal productCharge = AccelAeroCalculator.add(
					productCodeWiseAmountDue.get(ReservationInternalConstants.LOYALTY_PRODUCTS.FLIGHT.toString()),
					chargeMetaTO.getChargeAmount());
			productCodeWiseAmountDue.put(ReservationInternalConstants.LOYALTY_PRODUCTS.FLIGHT.toString(), productCharge);
		} else {
			BigDecimal productCharge = AccelAeroCalculator.add(productCodeWiseAmountDue.get("OTHER"),
					chargeMetaTO.getChargeAmount());
			productCodeWiseAmountDue.put("OTHER", productCharge);
		}

	}

	private static void consumeRemainingCreditAmounts(Collection<ReservationPaxOndCharge> colReservationPaxOndCharges,
			ArrayList<ChargeMetaTO> colNewCharges, boolean consumeFromOtherGroup) {

		Collection<String> colChargeGroupOrder = TnxGranularityRules.getPaymentChargeGroupFulFillmentOrder();
		for (String chargeGroupCode : colChargeGroupOrder) {
			Collection<ChargeMetaTO> colSelectedChargesForConsume = getChargesForSelectedGroup(chargeGroupCode, colNewCharges);
			if (colReservationPaxOndCharges.size() > 0 && colSelectedChargesForConsume.size() > 0) {
				Iterator<ReservationPaxOndCharge> resPaxOndCreditIte = colReservationPaxOndCharges.iterator();
				while (resPaxOndCreditIte.hasNext()) {
					ReservationPaxOndCharge reservationPaxOndCharge = resPaxOndCreditIte.next();

					if (!consumeFromOtherGroup && !reservationPaxOndCharge.getChargeGroupCode().equals(chargeGroupCode)) {
						continue;
					}

					boolean creditFullyConsumed = false;
					Iterator<ChargeMetaTO> newChargesIte = colSelectedChargesForConsume.iterator();
					while (newChargesIte.hasNext()) {
						ChargeMetaTO newCharge = newChargesIte.next();
						if (newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) == 0) {
							creditFullyConsumed = true;
							consumeFromOriginalCharges(colNewCharges, newCharge, newCharge.getChargeAmount());
							newChargesIte.remove();
							break;
						} else if (newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) < 0) {
							BigDecimal remeiningCredit = AccelAeroCalculator.subtract(reservationPaxOndCharge.getAmount(),
									newCharge.getChargeAmount());
							reservationPaxOndCharge.setAmount(remeiningCredit);
							consumeFromOriginalCharges(colNewCharges, newCharge, newCharge.getChargeAmount());
							newChargesIte.remove();
						} else if (newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) > 0) {
							consumeFromOriginalCharges(colNewCharges, newCharge, reservationPaxOndCharge.getAmount());
							creditFullyConsumed = true;
							break;
						}
					}
					if (creditFullyConsumed) {
						resPaxOndCreditIte.remove();
					}
				}
			}
		}
	}

	private static void consumeFromOriginalCharges(ArrayList<ChargeMetaTO> colNewCharges, ChargeMetaTO newCharge,
			BigDecimal consumedChargeAmount) {
		Iterator<ChargeMetaTO> newChargesIte = colNewCharges.iterator();
		while (newChargesIte.hasNext()) {
			ChargeMetaTO chargeMetaTO = newChargesIte.next();
			if ((newCharge.getChargeRateId() != null && newCharge.getChargeRateId().equals(chargeMetaTO.getChargeRateId()))
					|| (newCharge.getChargeRateId() == null
							&& chargeMetaTO.getChargeGroupCode().equals(newCharge.getChargeGroupCode()))) {
				if (consumedChargeAmount.equals(chargeMetaTO.getChargeAmount())) {
					newChargesIte.remove();
				} else {
					BigDecimal remainingChg = AccelAeroCalculator.subtract(chargeMetaTO.getChargeAmount(), consumedChargeAmount);
					chargeMetaTO.setChargeAmount(remainingChg);
				}
				break;
			}
		}
	}

	private static Collection<ChargeMetaTO> getChargesForSelectedGroup(String chargeGroupCode,
			ArrayList<ChargeMetaTO> colNewCharges) {
		Collection<ChargeMetaTO> colSelectedCharges = new ArrayList<ChargeMetaTO>();
		for (ChargeMetaTO chargeMetaTO : colNewCharges) {
			if (chargeMetaTO.getChargeGroupCode().equals(chargeGroupCode)) {
				colSelectedCharges.add(chargeMetaTO);
			}
		}
		return colSelectedCharges;
	}

	private static void consumeExactCreditAmounts(Collection<ReservationPaxOndCharge> colReservationPaxOndCharges,
			ArrayList<ChargeMetaTO> colNewCharges) {
		Iterator<ReservationPaxOndCharge> resPaxOndCreditIte = colReservationPaxOndCharges.iterator();
		while (resPaxOndCreditIte.hasNext()) {
			ReservationPaxOndCharge reservationPaxOndCharge = resPaxOndCreditIte.next();
			boolean creditFullyConsumed = false;
			if (reservationPaxOndCharge.getChargeRateId() != null) { // consume charges
				Iterator<ChargeMetaTO> newChargesIte = colNewCharges.iterator();
				while (newChargesIte.hasNext()) {
					ChargeMetaTO newCharge = newChargesIte.next();
					if (newCharge.getChargeCode() != null
							&& newCharge.getChargeCode().equals(reservationPaxOndCharge.getChargeCode())
							&& newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) == 0) {
						creditFullyConsumed = true;
						newChargesIte.remove();
						break;
					} else if (newCharge.getChargeCode() != null
							&& newCharge.getChargeCode().equals(reservationPaxOndCharge.getChargeCode())
							&& newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) < 0) {
						BigDecimal remeiningCredit = AccelAeroCalculator.subtract(reservationPaxOndCharge.getAmount(),
								newCharge.getChargeAmount());
						reservationPaxOndCharge.setAmount(remeiningCredit);
						newChargesIte.remove();
					} else if (newCharge.getChargeCode() != null
							&& newCharge.getChargeCode().equals(reservationPaxOndCharge.getChargeCode())
							&& newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) > 0) {
						BigDecimal remainingCharge = AccelAeroCalculator.subtract(newCharge.getChargeAmount(),
								reservationPaxOndCharge.getAmount());
						newCharge.setChargeAmount(remainingCharge);
						creditFullyConsumed = true;
						break;
					}
				}
				if (creditFullyConsumed) {
					resPaxOndCreditIte.remove();
				}
			} else if (reservationPaxOndCharge.getChargeRateId() == null) { // Consume Fare/Mod/Cnx
				Iterator<ChargeMetaTO> newChargesIte = colNewCharges.iterator();
				while (newChargesIte.hasNext()) {
					ChargeMetaTO newCharge = newChargesIte.next();
					if (newCharge.getChargeGroupCode().equals(reservationPaxOndCharge.getChargeGroupCode())
							&& newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) == 0) {
						creditFullyConsumed = true;
						newChargesIte.remove();
						break;
					} else if (newCharge.getChargeGroupCode().equals(reservationPaxOndCharge.getChargeGroupCode())
							&& newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) < 0) {
						BigDecimal remeiningCredit = AccelAeroCalculator.subtract(reservationPaxOndCharge.getAmount(),
								newCharge.getChargeAmount());
						reservationPaxOndCharge.setAmount(remeiningCredit);
						newChargesIte.remove();
					} else if (newCharge.getChargeGroupCode().equals(reservationPaxOndCharge.getChargeGroupCode())
							&& newCharge.getChargeAmount().compareTo(reservationPaxOndCharge.getAmount()) > 0) {
						BigDecimal remainingCharge = AccelAeroCalculator.subtract(newCharge.getChargeAmount(),
								reservationPaxOndCharge.getAmount());
						newCharge.setChargeAmount(remainingCharge);
						creditFullyConsumed = true;
						break;
					}
				}
				if (creditFullyConsumed) {
					resPaxOndCreditIte.remove();
				}
			}
		}

	}

}
