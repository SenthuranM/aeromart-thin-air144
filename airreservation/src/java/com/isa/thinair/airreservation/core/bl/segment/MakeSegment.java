/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for making a segment. This will create segment required objects structure in order to persist in the database
 * 
 * Business Rules: (1) Infants won't have any booking codes (2) Adults/Children will be having booking codes (3) A
 * booking code is unique through out the suite
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="makeSegment"
 */
public class MakeSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(MakeSegment.class);

	/**
	 * Execute method of the MakeSegment command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		Collection<OndFareDTO> colOndFareDTO = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);
		CredentialsDTO credentialsDTO = this.getParameter(CommandParamNames.CREDENTIALS_DTO, CredentialsDTO.class);
		Map<Integer, ReservationSegment> segObjectsMap = this.getParameter(CommandParamNames.SEG_OBJECTS_MAP, Map.class);
		DefaultServiceResponse output = this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE,
				DefaultServiceResponse.class);
		Collection<?> linkedGroundStations = this.getParameter(CommandParamNames.CANCELLED_SURFACE_SEGMENT_LIST,
				Collection.class);
		Boolean isRequote = getParameter(CommandParamNames.IS_REQUOTE, Boolean.FALSE, Boolean.class);
		FlownAssitUnit flownAsstUnit = getParameter(CommandParamNames.FLOWN_ASSIT_UNIT, FlownAssitUnit.class);
		String pnr = this.getParameter(CommandParamNames.PNR, String.class);

		ChargeTnxSeqGenerator chgTnxGen = this.getParameter(CommandParamNames.CHG_TRNX_GEN, ChargeTnxSeqGenerator.class);
		if (chgTnxGen == null) {
			chgTnxGen = new ChargeTnxSeqGenerator(pnr);
		}

		// only when cnx is present in requote
		if (isRequote && (colOndFareDTO == null || colOndFareDTO.size() == 0)) {
			return new DefaultServiceResponse(true);
		}

		Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap = new HashMap<Integer, Map<Integer, Double>>();

		// Checking params
		this.validateParams(colOndFareDTO, segObjectsMap, credentialsDTO);

		// Build ReservationPaxFare Object for adult
		Collection<ReservationPaxFare> pnrFaresForAdult = this.buildReservationPaxFareForPassenger(colOndFareDTO, segObjectsMap,
				ReservationInternalConstants.PassengerType.ADULT, credentialsDTO, flownAsstUnit, ondWisePaxDueAdjMap, chgTnxGen);

		// Build ReservationPaxFare Object for child
		Collection<ReservationPaxFare> pnrFaresForChild = this.buildReservationPaxFareForPassenger(colOndFareDTO, segObjectsMap,
				ReservationInternalConstants.PassengerType.CHILD, credentialsDTO, flownAsstUnit, ondWisePaxDueAdjMap, chgTnxGen);

		// Build ReservationPaxFare Object for infant
		Collection<ReservationPaxFare> pnrFaresForInfant = this.buildReservationPaxFareForPassenger(colOndFareDTO, segObjectsMap,
				ReservationInternalConstants.PassengerType.INFANT, credentialsDTO, flownAsstUnit, ondWisePaxDueAdjMap, chgTnxGen);

		Integer connectingPnrSegment = (Integer) this.getParameter(CommandParamNames.CONNECTING_FLT_SEG_ID);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESERVATION_PAX_FARE_FOR_ADULT, pnrFaresForAdult);
		response.addResponceParam(CommandParamNames.RESERVATION_PAX_FARE_FOR_CHILD, pnrFaresForChild);
		response.addResponceParam(CommandParamNames.RESERVATION_PAX_FARE_FOR_INFANT, pnrFaresForInfant);
		response.addResponceParam(CommandParamNames.CANCELLED_SURFACE_SEGMENT_LIST, linkedGroundStations);
		response.addResponceParam(CommandParamNames.CONNECTING_FLT_SEG_ID, connectingPnrSegment);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.OND_WISE_PAX_DUE_AMOUNT_MAP, ondWisePaxDueAdjMap);
		response.addResponceParam(CommandParamNames.RESERVATION_STATUS_BEFORE_UPDATE,
				this.getParameter(CommandParamNames.RESERVATION_STATUS_BEFORE_UPDATE));

		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		if (this.getParameter(CommandParamNames.SELECTED_ANCI_MAP) != null) {
			response.addResponceParam(CommandParamNames.SELECTED_ANCI_MAP,
					this.getParameter(CommandParamNames.SELECTED_ANCI_MAP));
		}

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Build ReservationPaxFare object for a passenger
	 * 
	 * @param colOndFareDTO
	 * @param segObjectsMap
	 * @param paxType
	 * @param flownAsstUnit
	 * @param ondWisePaxDueAdjMap
	 * @param txnGen
	 * @param paxTypeRevenueMap
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationPaxFare> buildReservationPaxFareForPassenger(Collection<OndFareDTO> colOndFareDTO,
			Map<Integer, ReservationSegment> segObjectsMap, String paxType, CredentialsDTO credentialsDTO,
			FlownAssitUnit flownAsstUnit, Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap, ChargeTnxSeqGenerator txnGen)
			throws ModuleException {

		Iterator<OndFareDTO> itColOndFareDTO = colOndFareDTO.iterator();
		OndFareDTO ondFareDTO;
		ReservationPaxFare reservationPaxFare;
		Collection<ReservationPaxFare> colReservationPaxFare = new HashSet<ReservationPaxFare>();

		while (itColOndFareDTO.hasNext()) {
			ondFareDTO = itColOndFareDTO.next();
			if (flownAsstUnit != null && flownAsstUnit.isFlownOnd(ondFareDTO)) {
				continue;
			}
			reservationPaxFare = new ReservationPaxFare();

			BigDecimal fareAdjustment = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (ondFareDTO.getFareSummaryDTO() != null) {
				fareAdjustment = AccelAeroCalculator
						.parseBigDecimal(ondFareDTO.getFareSummaryDTO().getEffectiveROEAdjustmentAmount(paxType));
			}

			// For Adult
			if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
				if (ReservationApiUtils.isFareExist(ondFareDTO.getAdultFare())) {
					// If the fare exist fare id should always be there
					reservationPaxFare.setFareId(new Integer(ondFareDTO.getFareId()));

					// Capture reservation passenger ond charge
					ReservationCoreUtils.captureReservationPaxOndCharge(
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare()), new Integer(ondFareDTO.getFareId()),
							null, ReservationInternalConstants.ChargeGroup.FAR, reservationPaxFare, credentialsDTO, false, null,
							fareAdjustment, null, txnGen.getTnxSequence(paxType));
				}
				// For Child
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
				if (ReservationApiUtils.isFareExist(ondFareDTO.getChildFare())) {
					// If the fare exist fare id should always be there
					reservationPaxFare.setFareId(new Integer(ondFareDTO.getFareId()));

					// Capture reservation passenger ond charge
					ReservationCoreUtils.captureReservationPaxOndCharge(
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare()), new Integer(ondFareDTO.getFareId()),
							null, ReservationInternalConstants.ChargeGroup.FAR, reservationPaxFare, credentialsDTO, false, null,
							fareAdjustment, null, txnGen.getTnxSequence(paxType));
				}
				// For Infant
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
				if (ReservationApiUtils.isFareExist(ondFareDTO.getInfantFare())) {
					// If the fare exist fare id should always be there
					reservationPaxFare.setFareId(new Integer(ondFareDTO.getFareId()));

					// Capture reservation passenger ond charge
					ReservationCoreUtils.captureReservationPaxOndCharge(
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()), new Integer(ondFareDTO.getFareId()),
							null, ReservationInternalConstants.ChargeGroup.FAR, reservationPaxFare, credentialsDTO, false, null,
							fareAdjustment, null, txnGen.getTnxSequence(paxType));
				}
				// This can not happen. Precautionary measure
			} else {
				throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
			}

			// ############# Setting Ond Fare Charges ###########################
			this.setPassengerOndFareCharges(reservationPaxFare, ondFareDTO, paxType, credentialsDTO, ondWisePaxDueAdjMap,
					txnGen.getTnxSequence(paxType));
			// ############# Setting Ond Fare Charges ###########################

			// ############# Setting Fare Segments ##############################
			this.setPassengerOndFareSegments(reservationPaxFare, ondFareDTO, segObjectsMap, paxType);
			// ############# Setting Fare Segments ##############################

			if (flownAsstUnit != null && flownAsstUnit.isPenaltyApplicableOnd(ondFareDTO)) {
				BigDecimal penalty = flownAsstUnit.getPenaltyForOnd(ondFareDTO, paxType);
				if (penalty.compareTo(BigDecimal.ZERO) > 0) {
					ReservationCoreUtils.captureReservationPaxOndCharge(penalty, null, null, ChargeGroup.PEN, reservationPaxFare,
							credentialsDTO, false, null, null, null, txnGen.getTnxSequence(paxType));
				}
			}

			colReservationPaxFare.add(reservationPaxFare);
		}

		return colReservationPaxFare;
	}

	/**
	 * Set the passenger ond fare charges
	 * 
	 * @param reservationPaxFare
	 * @param ondFareDTO
	 * @param paxType
	 * @param ondWisePaxDueAdjMap
	 * @param tnxSequence
	 * @throws ModuleException
	 */
	private void setPassengerOndFareCharges(ReservationPaxFare reservationPaxFare, OndFareDTO ondFareDTO, String paxType,
			CredentialsDTO credentialsDTO, Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap, Integer tnxSequence)
			throws ModuleException {

		// Setting the charges information
		if (ondFareDTO.getAllCharges() != null) {
			Iterator<QuotedChargeDTO> itQuotedChargeDTO = ondFareDTO.getAllCharges().iterator();
			QuotedChargeDTO quotedChargeDTO;

			while (itQuotedChargeDTO.hasNext()) {
				quotedChargeDTO = (QuotedChargeDTO) itQuotedChargeDTO.next();

				if (ChargeGroup.ADJ.equals(quotedChargeDTO.getChargeGroupCode())
						&& ChargeCodes.CNX_SEGMENT_SYSTEM_ADJ.equals(quotedChargeDTO.getChargeCode())
						&& ondWisePaxDueAdjMap != null && ondWisePaxDueAdjMap.get(ondFareDTO.getFareId()) == null) {
					ondWisePaxDueAdjMap.put(new Integer(ondFareDTO.getFareId()), quotedChargeDTO.getPaxWiseBalanceDueAdj());
				}

				// Capture if the charge is for an adult
				if (PassengerType.ADULT.equals(paxType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						ReservationCoreUtils.captureReservationPaxOndCharge(
								AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT)),
								null, new Integer(quotedChargeDTO.getChargeRateId()), quotedChargeDTO.getChargeGroupCode(),
								reservationPaxFare, credentialsDTO, false, null, null, null, tnxSequence);
					}
				}
				// Capture if the charge is for a child
				else if (PassengerType.CHILD.equals(paxType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						ReservationCoreUtils.captureReservationPaxOndCharge(
								AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD)),
								null, new Integer(quotedChargeDTO.getChargeRateId()), quotedChargeDTO.getChargeGroupCode(),
								reservationPaxFare, credentialsDTO, false, null, null, null, tnxSequence);
					}
				}
				// Capture if the charge is for an infant
				else if (PassengerType.INFANT.equals(paxType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {
						ReservationCoreUtils.captureReservationPaxOndCharge(
								AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT)),
								null, new Integer(quotedChargeDTO.getChargeRateId()), quotedChargeDTO.getChargeGroupCode(),
								reservationPaxFare, credentialsDTO, false, null, null, null, tnxSequence);
					}
					// This can not happen. Precautionary measure
				} else {
					throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
				}
			}
		}
	}

	/**
	 * Set the passenger ond fare segments
	 * 
	 * @param reservationPaxFare
	 * @param ondFareDTO
	 * @param segObjectsMap
	 * @param paxType
	 * @throws ModuleException
	 */
	private void setPassengerOndFareSegments(ReservationPaxFare reservationPaxFare, OndFareDTO ondFareDTO,
			Map<Integer, ReservationSegment> segObjectsMap, String paxType) throws ModuleException {
		Iterator<SegmentSeatDistsDTO> itSegmentSeatDistsDTO = ondFareDTO.getSegmentSeatDistsDTO().iterator();

		SegmentSeatDistsDTO segmentSeatDistsDTO;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itSegmentSeatDistsDTO.hasNext()) {
			segmentSeatDistsDTO = (SegmentSeatDistsDTO) itSegmentSeatDistsDTO.next();
			reservationPaxFareSegment = new ReservationPaxFareSegment();

			// Set the Reservation Segment for relationship tracking
			reservationPaxFareSegment
					.setSegment((ReservationSegment) segObjectsMap.get(new Integer(segmentSeatDistsDTO.getFlightSegId())));

			if (segmentSeatDistsDTO.getSeatDistribution() != null && segmentSeatDistsDTO.getSeatDistribution().size() > 1) {
				reservationPaxFareSegment.setBcNested(ReservationInternalConstants.BCNested.YES);
			} else {
				reservationPaxFareSegment.setBcNested(ReservationInternalConstants.BCNested.NO);
			}

			reservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
		}
	}

	/**
	 * Validating the parameters
	 * 
	 * @param colOndFareDTO
	 * @param segObjectsMap
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Collection colOndFareDTO, Map segObjectsMap, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (segObjectsMap == null || colOndFareDTO == null || colOndFareDTO.size() == 0 || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}
}
