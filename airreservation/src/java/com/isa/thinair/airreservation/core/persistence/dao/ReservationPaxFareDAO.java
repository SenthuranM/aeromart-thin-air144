/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * ReservationPaxFareDAO is the business DAO interface for the reservation passenger fare apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationPaxFareDAO {

	/**
	 * Returns Object[]{pnrPaxId, pnrPaxFareId, paxType} corresponding to first adult or child pax with pnrPaxFareId,
	 * paxType linked to first segment.
	 * 
	 * @param pnr
	 * @return Object[]{pnrPaxId, pnrPaxFareId, paxType}
	 */
	public Object[] getFirstPaxPnrPaxFareId(String pnr);

	/**
	 * Returns a {@link List} of {@link ReservationPaxFare} based on the pnrPaxId
	 * 
	 * @param pnrPaxId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public List<ReservationPaxFare> getReservationPaxFare(Integer pnrPaxId) throws CommonsDataAccessException;

	public Integer getReservationPaxFareId(Integer pnrSegmentId);
	
	public BigDecimal getTotalReturnJourneyFare(Integer pnrPaxId, Integer pnrPaxFareId);
}