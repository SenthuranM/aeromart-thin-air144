/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airreservation.api.model.AlertAction;

/**
 * @author byorn
 * @since 1.0
 */
public interface AlertActionDAO {

	public Collection<AlertAction> getAlertActions();

}
