/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.LinkedList;

import com.isa.thinair.airreservation.api.dto.PNRNumberGenerator;

/**
 * GDS PNR Number Generator Implementation
 * 
 * @author Nilindra Fernando
 * @since Sep 12, 2008
 */
class GDSPNRNumberGeneratorImpl implements PNRNumberGenerator {

	private static final String[] CHARACTERS = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F",
			"G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

	private static final long LOG_BASE_FOR_CHARACTERS = CHARACTERS.length;

	private static final long NO_OF_CHARACTERS = 6;

	/**
	 * Returns the divided value
	 * 
	 * @param value
	 * @return
	 */
	private static long divValue(long value) {
		return value / LOG_BASE_FOR_CHARACTERS;
	}

	/**
	 * Returns the remaining value
	 * 
	 * @param value
	 * @return
	 */
	private static long modValue(long value) {
		return value % LOG_BASE_FOR_CHARACTERS;
	}

	/**
	 * Returns the index number of the value specified
	 * 
	 * @param value
	 * @return
	 */
	private static long getValueIndex(String value) {
		for (int i = 0; i < CHARACTERS.length; i++) {
			if (value.equals(CHARACTERS[i])) {
				return i;
			}
		}

		// Practically this case won't occure
		throw new RuntimeException("Value cannot be located");
	}

	/**
	 * Convert based on the sequence number specified
	 * 
	 * @param sequenceNo
	 * @return
	 */
	private static String convert(long sequenceNo) {
		LinkedList<Long> remainders = new LinkedList<Long>();

		long div = divValue(sequenceNo);
		long mod = modValue(sequenceNo);
		remainders.addFirst(mod);

		while (div >= LOG_BASE_FOR_CHARACTERS) {
			long holder = divValue(div);
			mod = modValue(div);
			remainders.addFirst(mod);
			div = holder;
		}

		remainders.addFirst(div);

		StringBuilder convertedNo = new StringBuilder();
		for (Long element : remainders) {
			convertedNo.append(CHARACTERS[element.intValue()]);
		}

		return convertedNo.toString();
	}

	/**
	 * Generates the PNR Number from the sequence
	 * 
	 * @param sequenceNo
	 * @return
	 */
	public String generatePNRNumber(long sequenceNo) {
		StringBuilder newNumber = new StringBuilder();
		String runningSequence = GDSPNRNumberGeneratorImpl.convert(sequenceNo);

		long difference = NO_OF_CHARACTERS - runningSequence.length();

		if (difference > 0) {
			for (int i = 0; i < difference; i++) {
				newNumber.append(CHARACTERS[0]);
			}
		}

		newNumber.append(runningSequence);
		return newNumber.toString();
	}

	/**
	 * Generates the Sequence from the PNR Number
	 * 
	 * @param pnrNumber
	 * @return
	 */
	public long generateSequenceNo(String pnrNumber) {
		LinkedList<String> elements = new LinkedList<String>();
		char[] characters = pnrNumber.toCharArray();

		for (int i = 0; i < characters.length; i++) {
			String element = String.valueOf(characters[i]);
			elements.addFirst(element);
		}

		int i = 0;
		long finalAmount = 0;
		for (String element : elements) {
			long index = getValueIndex(element);
			finalAmount += new Double(Math.pow(LOG_BASE_FOR_CHARACTERS, i)).longValue() * index;
			i++;
		}

		return finalAmount;
	}

	public static void main(String[] args) {
		GDSPNRNumberGeneratorImpl gdsPNRNumberGeneratorImpl = new GDSPNRNumberGeneratorImpl();

		long startSequenceNo = 1;
		long endSequenceNo = 99999999;

		while (startSequenceNo < endSequenceNo) {
			String pnrNumber = gdsPNRNumberGeneratorImpl.generatePNRNumber(startSequenceNo);
			long sequenceNo = gdsPNRNumberGeneratorImpl.generateSequenceNo(pnrNumber);

			System.out.println("Processing ::: " + startSequenceNo + " [" + pnrNumber + "] [" + sequenceNo + "] ");

			if (startSequenceNo != sequenceNo) {
				System.out.println("::::::::: We have a mismatch :::::::::");
				System.out.println(" :: Start Sequence " + startSequenceNo);
				System.out.println(" :: Derived Sequence " + sequenceNo);
				break;
			}

			startSequenceNo++;
		}

		System.out.println(gdsPNRNumberGeneratorImpl.generatePNRNumber(10000212));
		System.out.println(gdsPNRNumberGeneratorImpl.generateSequenceNo("06N8FC"));
		System.out.println(gdsPNRNumberGeneratorImpl.generateSequenceNo("0000Z0"));
		System.out.println(gdsPNRNumberGeneratorImpl.generatePNRNumber(1225));
		System.out.println(gdsPNRNumberGeneratorImpl.generateSequenceNo("000100"));
		System.out.println(gdsPNRNumberGeneratorImpl.generatePNRNumber(99999999));
	}
}