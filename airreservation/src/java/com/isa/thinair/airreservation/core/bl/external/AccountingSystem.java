/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.external;

import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CreditCardSalesStatusDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface AccountingSystem {

	public void transferCreditSales(Date date) throws ModuleException;

	public CreditCardSalesStatusDTO getCreditCardSalesDetails(Integer cards[], Date fromDate, Date toDate) throws ModuleException;

	public void transferTopUps(Date date) throws ModuleException;


}
