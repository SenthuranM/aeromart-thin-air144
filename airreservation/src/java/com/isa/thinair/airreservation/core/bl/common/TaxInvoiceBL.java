package com.isa.thinair.airreservation.core.bl.common;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.taxInvoice.TaxInvoiceDTO;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Tax Invoice related business methods
 * 
 * @since 1.0
 */
public class TaxInvoiceBL {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(TaxInvoiceBL.class);

	/** Holds the airreservation config instance */
	private static final AirReservationConfig config = ReservationModuleUtils.getAirReservationConfig();

	/**
	 * Hide the constructor
	 */
	private TaxInvoiceBL() {

	}

	/**
	 * Gets invoice for print
	 * 
	 * @param List
	 *            <TaxInvoice>
	 * @throws ModuleException
	 */
	public static String getTaxInvoiceForPrint(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation)
			throws ModuleException {
		String templateName = config.getTaxInvoicePrintTemplate();

		HashMap<String, Object> invoiceMap = (HashMap<String, Object>) getInvoiceDataMap(taxInvoicesList, reservation);
		TemplateEngine engine = null;
		Locale locale = new Locale("en");
		String localeSpecificTemplateName = templateName;
		StringWriter writer = null;

		if ((locale != null) && (!locale.getLanguage().equals(Locale.getDefault().getLanguage()))) {
			if (templateName.indexOf(".") > 0) {
				localeSpecificTemplateName = templateName.substring(0, templateName.indexOf(".")) + "_"
						+ locale.getLanguage().trim().toUpperCase() + templateName.substring(templateName.indexOf("."));
			} else {
				localeSpecificTemplateName = templateName + "_" + locale.getLanguage().trim();
			}
		}

		try {
			engine = new TemplateEngine();
			writer = new StringWriter();
		} catch (Exception e) {
			throw new ModuleException("airreservations.taxInvoice.errorCreatingFromTemplate", e);
		}
		if (invoiceMap != null) {
			engine.writeTemplate(invoiceMap, localeSpecificTemplateName, writer);
		}

		return writer.toString();
	}

	/**
	 * Returns the Invoice Data Map
	 * 
	 * @param List
	 *            <TaxInvoice>
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private static Map<String, Object> getInvoiceDataMap(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation)
			throws ModuleException {
		log.debug("Inside getTaxInvoiceMap");

		List<TaxInvoiceDTO> taxInvoiceDtoList = new ArrayList<TaxInvoiceDTO>();

		for (TaxInvoice taxInvoice : taxInvoicesList) {
			try {
				TaxInvoiceDTO taxInvoiceDto = new TaxInvoiceDTO();
				taxInvoiceDto.setTaxInvoice(taxInvoice);

				taxInvoiceDto.setNameOfTheAirline(taxInvoice.getNameOfTheAirline());
				taxInvoiceDto.setAirlineOfficeSTAddress1(taxInvoice.getAirlineOfficeSTAddress1());
				taxInvoiceDto.setAirlineOfficeSTAddress2(taxInvoice.getAirlineOfficeSTAddress2());
				taxInvoiceDto.setAirlineOfficeCity(taxInvoice.getAirlineOfficeCity());
				taxInvoiceDto.setAirlineOfficeCountryCode(taxInvoice.getAirlineOfficeCountryCode());
				taxInvoiceDto.setGstinForInvoiceState(taxInvoice.getGstinForInvoiceState());
				taxInvoiceDto.setDigitalSignForInvoiceState(taxInvoice.getDigitalSignForInvoiceState());
				taxInvoiceDto.setNameOfTheRecipient(taxInvoice.getNameOfTheRecipient());
				taxInvoiceDto.setGstinOfTheRecipient(taxInvoice.getGstinOfTheRecipient());
				taxInvoiceDto.setStateOfTheRecipient(taxInvoice.getStateOfTheRecipient());
				taxInvoiceDto.setStateCodeOfTheRecipient(taxInvoice.getStateCodeOfTheRecipient());
				taxInvoiceDto.setRecipientStreetAddress1(taxInvoice.getRecipientStreetAddress1());
				taxInvoiceDto.setRecipientStreetAddress2(taxInvoice.getRecipientStreetAddress2());
				taxInvoiceDto.setRecipientCity(taxInvoice.getRecipientCity());
				taxInvoiceDto.setRecipientCountryCode(taxInvoice.getRecipientCountryCode());
				taxInvoiceDto.setAccountCodeOfService(taxInvoice.getAccountCodeOfService());
				taxInvoiceDto.setDescriptionOfService(taxInvoice.getDescriptionOfService());
				taxInvoiceDto.setTotalValueOfService(taxInvoice.getTotalValueOfService());
				taxInvoiceDto.setTotalTaxAmount(taxInvoice.getTotalTaxAmount());
				taxInvoiceDto.setPlaceOfSupply(taxInvoice.getPlaceOfSupply());
				taxInvoiceDto.setPlaceOfSupplyState(taxInvoice.getPlaceOfSupplyState());
				taxInvoiceDto.setTaxRate1Percentage(taxInvoice.getTaxRate1Percentage());
				taxInvoiceDto.setTaxRate2Percentage(taxInvoice.getTaxRate2Percentage());
				taxInvoiceDto.setTaxRate3Percentage(taxInvoice.getTaxRate3Percentage());
				taxInvoiceDto.setDateOfIssueOriginalTaxInvoice(formatDate(taxInvoice.getDateOfIssueOriginalTaxInvoice()));
				taxInvoiceDto.setDateOfIssue(formatDate(taxInvoice.getDateOfIssue()));

				taxInvoiceDtoList.add(taxInvoiceDto);
			} catch (NullPointerException npe) {
				throw new ModuleException("airreservations.taxInvoice.errorCreatingFromTemplate", npe);
			}
		}

		// Setting the Parameters
		Map<String, Object> taxInvoiceDatamap = new HashMap<String, Object>();
		taxInvoiceDatamap.put("taxInvoiceDtoList", taxInvoiceDtoList);
		log.debug("Exit getTaxInvoiceMap");
		return taxInvoiceDatamap;
	}

	private static String formatDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if (date != null){
			return dateFormat.format(date);
		} else{
			return "";
		}
	}

	/**
	 * Encrypts a given string
	 * 
	 * @param msg
	 * @return
	 * @throws ModuleException
	 */
	private static String encrypt(String msg) throws ModuleException {
		return ReservationModuleUtils.getCryptoServiceBD().encrypt(msg);
	}

	/**
	 * Generates the barcode <img ... /img> URL
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public static String generateBarcode(String pnr, List<String> segmentsCodesOrderdByDep) throws ModuleException {
		Map<String, String> barCodeConfMap = config.getBarCodeConfMap();
		String showBarcode = (String) barCodeConfMap.get("showBarcode");

		StringBuilder URL = new StringBuilder();

		// create URL
		if (showBarcode.equalsIgnoreCase("true")) {
			String carrierCode = AppSysParamsUtil.getCarrierCode();

			String barcodeType = (String) barCodeConfMap.get("barcodeType");
			String humanReadable = (String) barCodeConfMap.get("humanReadable");
			String format = (String) barCodeConfMap.get("format");
			String resolution = (String) barCodeConfMap.get("resolution");
			String height = (String) barCodeConfMap.get("height");
			String path = PlatformUtiltiies.nullHandler(CommonsServices.getGlobalConfig().getBizParam(
					SystemParamKeys.ACCELAERO_IBE_URL))
					+ "/public";
			String showAirline = (String) barCodeConfMap.get("showAirline");
			String mw = (String) barCodeConfMap.get("widthFactor");
			String showSegments = (String) barCodeConfMap.get("showSegments");
			String showEticket = (String) barCodeConfMap.get("showEticket");
			String showPnr = (String) barCodeConfMap.get("showPnr");
			StringBuilder barcodeMsg = new StringBuilder();
			if (showAirline.equalsIgnoreCase("true")) {
				barcodeMsg.append(carrierCode);
			}
			if (showPnr.equalsIgnoreCase("true")) {
				barcodeMsg.append(pnr);
			} else if (showPnr.equalsIgnoreCase("false") && showEticket.equalsIgnoreCase("false")) {
				barcodeMsg.append(pnr);
			}

			if (showSegments.equalsIgnoreCase("true")) {
				StringBuilder strSeg = new StringBuilder();
				for (String segCode : segmentsCodesOrderdByDep) {
					strSeg.append(segCode);
				}
				barcodeMsg.append(strSeg.toString());
			}
			String enptBarCodeMsg = encrypt(barcodeMsg.toString());
			String humanReadablePhaseEnableOrNot = encrypt(humanReadable);

			try {
				enptBarCodeMsg = URLEncoder.encode(enptBarCodeMsg, "UTF-8");
				humanReadablePhaseEnableOrNot = URLEncoder.encode(humanReadablePhaseEnableOrNot, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				log.error("Barcode could not encode messege or parameter ");
				throw new ModuleException("airreservations.barcode.encodingNotSupported");
			}

			URL.append("<img src='").append(path).append("/genbc?type=").append(barcodeType).append("&msg=")
					.append(enptBarCodeMsg).append("&hrp=").append(humanReadablePhaseEnableOrNot).append("&fmt=").append(format)
					.append("&res=").append(resolution).append("&height=").append(height).append("&mw=").append(mw).append("'/>");

			log.debug("Barcode URL created");
		}

		return URL.toString();
	}

}
