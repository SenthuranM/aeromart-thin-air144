package com.isa.thinair.airreservation.core.bl.segment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.flexi.FlexiRetainDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for retaining the selected flexibilities when modifying a segment
 * 
 * @isa.module.command name="retainFlexibilities"
 */
public class RetainFlexibilities extends DefaultBaseCommand {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(RetainFlexibilities.class);

	/** Construct the CancelSegment object */
	public RetainFlexibilities() {
	}

	/**
	 * Execute method of the CancelSegment command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection<Integer> newSegmentSeqNos = (Collection<Integer>) this
				.getParameter(CommandParamNames.NEW_SEGMENT_SEQUENCE_NUMBERS);
		Collection<Integer> cancelledSegments = (Collection<Integer>) this.getParameter(CommandParamNames.PNR_SEGMENT_IDS);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		FlexiRetainDTO flexiRetainDTO = (FlexiRetainDTO) this.getParameter(CommandParamNames.FLEXI_RETAIN_DTO);
		Map<String, String> requoteSegmentMap = (Map<String, String>) this.getParameter(CommandParamNames.REQUOTE_SEGMENT_MAP);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		// Checking params
		this.validateParams(pnr, version, credentialsDTO);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);

		// Get the reservation
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		if (flexiRetainDTO != null && flexiRetainDTO.hasRetainableFlexi() && newSegmentSeqNos != null) {
			// retains standard SSRs of modifying segments with new segments
			retainFlexibilities(reservation, flexiRetainDTO, newSegmentSeqNos, cancelledSegments, requoteSegmentMap);
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (output == null) {
			output = new DefaultServiceResponse();
			output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		} else {
			output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		}

		response.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS, this.getParameter(CommandParamNames.FLIGHT_SEGMENT_IDS));
		response.addResponceParam(CommandParamNames.PNR, this.getParameter(CommandParamNames.PNR));
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}

		log.debug("Exit execute");
		return response;
	}

	@SuppressWarnings("rawtypes")
	private void retainFlexibilities(Reservation reservation, FlexiRetainDTO flexiRetainDTO,
			Collection<Integer> newSegmentSeqNos, Collection<Integer> cancelledPnrSegIds, Map<String, String> requoteSegmentMap) throws ModuleException {
		Set<ReservationPax> paxs = reservation.getPassengers();
		Set<Integer> newPnrSegIds = new HashSet<Integer>();
				
		Map<Integer, ReservationSegmentDTO> pnrSegIdsWiseSegments = new HashMap<Integer, ReservationSegmentDTO>();
		for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
			if (newSegmentSeqNos.contains(seg.getSegmentSeq())) {
				newPnrSegIds.add(seg.getPnrSegId());
			}
			pnrSegIdsWiseSegments.put(seg.getPnrSegId(), seg);
		}

		Map<Integer, Collection<Integer>> cancelledGroupWisePnrSegIds = CancellationUtils.getOndGroupWisePnrSegIds(
				cancelledPnrSegIds, reservation);

		Map<Integer, Collection<Integer>> newlyAddedGroupWisePnrSegIds = CancellationUtils.getOndGroupWisePnrSegIds(newPnrSegIds,
				reservation);
		
		
		Map<Integer, Integer> newGroupIdForRetainingSegment = new HashMap<Integer, Integer>();

		Map<Integer, Integer> reprotectedCnxAgainstNewGroupIds = new HashMap<Integer, Integer>();
		for (Integer groupId : newlyAddedGroupWisePnrSegIds.keySet()) {
			for (Integer pnrSegId : newlyAddedGroupWisePnrSegIds.get(groupId)) {
				ReservationSegmentDTO seg = pnrSegIdsWiseSegments.get(pnrSegId);
				String flightSegId = seg.getFlightSegId().toString();
				if (requoteSegmentMap.containsKey(flightSegId) && requoteSegmentMap.get(flightSegId) != null) {
					Integer extractedPnrSegId = FlightRefNumberUtil.getPnrSegIdFromPnrSegRPH(requoteSegmentMap.get(flightSegId));
					if (cancelledPnrSegIds.contains(extractedPnrSegId)) {
						newGroupIdForRetainingSegment.put(extractedPnrSegId, groupId);
					}
				}
			}
		}
		
		Map<Collection<Integer>, Integer> pnrSegWiseGroupIds = new HashMap<Collection<Integer>, Integer>();
		
		for (Integer groupId : cancelledGroupWisePnrSegIds.keySet()) {
			for (Integer cancelledPnrSegId : cancelledGroupWisePnrSegIds.get(groupId)) {
				if (!reprotectedCnxAgainstNewGroupIds.containsKey(groupId) && !newGroupIdForRetainingSegment.isEmpty()
						&& newGroupIdForRetainingSegment.containsKey(cancelledPnrSegId)) {
					reprotectedCnxAgainstNewGroupIds.put(groupId, newGroupIdForRetainingSegment.get(cancelledPnrSegId));
				}
			}
			pnrSegWiseGroupIds.put(cancelledGroupWisePnrSegIds.get(groupId), groupId);
		}
		
		
		if (flexiRetainDTO.hasRetainableFlexi() && !reprotectedCnxAgainstNewGroupIds.isEmpty()) {
			Collection<ReservationPaxOndFlexibility> paxOndFlexibilities = new ArrayList<ReservationPaxOndFlexibility>();

			for (Iterator<ReservationPax> itPaxs = paxs.iterator(); itPaxs.hasNext();) {
				ReservationPax reservationPax = (ReservationPax) itPaxs.next();

				if (!flexiRetainDTO.hasRetainableFlexiForPax(reservationPax.getPnrPaxId())) {
					continue;
				}
				Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> paxFareFlexibilites = reservation
						.getPaxOndFlexibilities();
				Map<Collection<Integer>, Collection<ReservationPaxOndFlexibilityDTO>> segIdWiseFlexibility = flexiRetainDTO
						.getPnrSegWiseFlexibilityForPax(reservationPax.getPnrPaxId());

				Set<ReservationPaxFare> paxFares = reservationPax.getPnrPaxFares();
				for (Collection<Integer> groupWisePnrSegIds : segIdWiseFlexibility.keySet()) {
					Integer cnxGroupId = pnrSegWiseGroupIds.get(groupWisePnrSegIds);
					if (!reprotectedCnxAgainstNewGroupIds.containsKey(cnxGroupId)) {
						continue;
					}
					Integer newGroupId = reprotectedCnxAgainstNewGroupIds.get(cnxGroupId);

					ReservationPaxFare paxFare = ReservationCoreUtils.getPnrPaxFare(paxFares,
							newlyAddedGroupWisePnrSegIds.get(newGroupId));

					Collection<ReservationPaxOndFlexibilityDTO> updatedFlexibilities = segIdWiseFlexibility
							.get(groupWisePnrSegIds);

					if (updatedFlexibilities != null) {
						for (ReservationPaxOndFlexibilityDTO paxOndFlexibilityDTO : updatedFlexibilities) {
							if (!paxFareFlexibilites.containsKey(Integer.valueOf(paxFare.getPnrPaxFareId()))
									|| paxFareFlexibilites.get(Integer.valueOf(paxFare.getPnrPaxFareId())) == null
									|| ((ArrayList) paxFareFlexibilites.get(Integer.valueOf(paxFare.getPnrPaxFareId())))
											.isEmpty()) { // Remaining flexibilities should be stored only for new
															// pnr pax fares
								if (paxOndFlexibilityDTO.getAvailableCount() > 0) {
									ReservationPaxOndFlexibility reservationPaxOndFlexibility = new ReservationPaxOndFlexibility();
									reservationPaxOndFlexibility.setAvailableCount(paxOndFlexibilityDTO.getAvailableCount());
									reservationPaxOndFlexibility.setUtilizedCount(paxOndFlexibilityDTO.getUtilizedCount());
									reservationPaxOndFlexibility.setFlexiRuleRateId(paxOndFlexibilityDTO.getFlexiRateId());
									reservationPaxOndFlexibility.setFlexiRuleTypeId(paxOndFlexibilityDTO.getFlexibilityTypeId());
									reservationPaxOndFlexibility.setPpfId(paxFare.getPnrPaxFareId());
									reservationPaxOndFlexibility.setStatus(ReservationPaxOndFlexibility.STATUS_ACTIVE);
									paxOndFlexibilities.add(reservationPaxOndFlexibility);
								}
							}
						}
					}

					if (paxOndFlexibilities.size() > 0) {
						ReservationDAOUtils.DAOInstance.FLEXI_RULE_DAO.saveOrUpdate(paxOndFlexibilities);
					}

				}
			}
		}
	}

	private String buildOndCode(Collection<Integer> pnrSegIds, Map<Integer, ReservationSegmentDTO> pnrSegIdsWiseSegments) {
		List<ReservationSegmentDTO> segments = new ArrayList<ReservationSegmentDTO>();
		for (Integer pnrSegId : pnrSegIds) {
			segments.add(pnrSegIdsWiseSegments.get(pnrSegId));
		}
		Collections.sort(segments, new Comparator<ReservationSegmentDTO>() {
			@Override
			public int compare(ReservationSegmentDTO first, ReservationSegmentDTO second) {
				return first.getZuluDepartureDate().compareTo(second.getZuluDepartureDate());
			}

		});
		StringBuffer sb = new StringBuffer();
		for (ReservationSegmentDTO seg : segments) {
			if (sb.length() > 0) {
				sb.append("/");
			}
			sb.append(seg.getOrigin() + "/" + seg.getDestination());
		}
		return sb.toString();
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param version
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Long version, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || version == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

}
