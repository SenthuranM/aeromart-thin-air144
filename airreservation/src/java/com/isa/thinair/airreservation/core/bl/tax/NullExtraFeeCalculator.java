package com.isa.thinair.airreservation.core.bl.tax;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.core.bl.pnr.IExtraFeeCalculator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class NullExtraFeeCalculator implements IExtraFeeCalculator {

	@Override
	public void calculate() throws ModuleException {

	}

	@Override
	public Collection<? extends ExternalChgDTO> getPaxExtraFee(Integer pnrPaxId) {
		return new ArrayList<ExternalChgDTO>();
	}

	@Override
	public boolean hasExtraFeeFor(Integer pnrPaxId) {
		return false;
	}

}
