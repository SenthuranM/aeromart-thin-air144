/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerElement;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;

/**
 * @author udithad
 *
 */
public class SeatInformation extends
		PassengerAdditions<List<PassengerInformation>> {

	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> detailsDTOs) {
		for (PassengerInformation information : detailsDTOs) {
			List<AncillaryDTO> seatInformation = getSeatAncillaryInformationListBy(information
					.getPnrPaxId());
			information.setSeats(seatInformation);
		}
	}

	private List<AncillaryDTO> getSeatAncillaryInformationListBy(
			Integer passengerId) {
		return seatAncillaryInformation.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformation) {
		seatAncillaryInformation = PnlAdlUtil
				.getPassengerSeatMapDetails(passengerInformation);
	}

}
