/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardTypeDTO;
import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentSageDTO;
import com.isa.thinair.airreservation.api.model.CreditCardSalesSage;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * CreditSalesTransferDAOImpl is the business DAO hibernate implementation
 */

public abstract class CreditSalesTransferDAOImpl extends PlatformBaseHibernateDaoSupport implements CreditSalesTransferDAO {

	private static final Log log = LogFactory.getLog(CreditSalesTransferDAOImpl.class);

	/*
	 * Clear sales in internal summary tables for a date
	 * 
	 * @Param date
	 */
	public void clearCreditCardSalesTable(Date date) {

		String hql = "select creditSales from CreditCardSalesSage creditSales where creditSales.dateOfSale=?";
		Collection<?> pojos = getSession().createQuery(hql).setDate(0, date).list();
		if (pojos != null && pojos.size() > 0) {
			deleteAll(pojos);
		}
	}

	public void clearAgentTopUpTable(Date date) {
		// TODO Auto-generated method stub

	}
	
	/*
	 * Gets CC payments for a date
	 * 
	 * @Param date
	 */
	@SuppressWarnings("unchecked")
	public Collection<CreditCardPaymentSageDTO> getCreditCardPayments(Date date) {
		log.info("########### getCreditCardPayments #############");

		String thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();
		Collection<CreditCardPaymentSageDTO> creditDTO = new ArrayList<CreditCardPaymentSageDTO>();
		HashMap<String, CreditCardPaymentSageDTO> nominalCodeAmountMap = new HashMap<String, CreditCardPaymentSageDTO>();
		Collection<CreditCardTypeDTO> creditCardTypeList = getCreditCardPaymentRefundNominalCodes();
		Collection<Integer> nominalCodes = getNominalCodes(creditCardTypeList);

		if (nominalCodes.size() != 0) {

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pt.tnx_date AS transaction_date, ");
			sql.append("pt.amount_paycur AS amount_paycur, ");
			sql.append("pt.amount AS amount_bascur, ");
			sql.append("pt.payment_currency_code as paymt_curr, ");
			sql.append("pt.nominal_code AS nominal_code, ");
			sql.append("pp.pnr AS PNR, ");
			sql.append("pg.provier_name AS payment_gateway, ");
			sql.append("rp.authorization_id AS auth_code ");
			sql.append("FROM ");
			sql.append("T_PAX_TRANSACTION pt, ");
			sql.append("T_PNR_PASSENGER pp, ");
			sql.append("T_RES_PCD rp, ");
			sql.append("t_ccard_payment_status ps, ");
			sql.append("t_payment_gateway_currency pgc, ");
			sql.append("t_payment_gateway pg ");
			sql.append("WHERE ");
			sql.append("pt.pnr_pax_id = pp.pnr_pax_id ");
			sql.append("AND nominal_code IN (" + Util.buildIntegerInClauseContent(nominalCodes) + ") ");
			sql.append("AND (PAYMENT_CARRIER_CODE = '" + thisAirlineCode + "' ");
			sql.append("OR PAYMENT_CARRIER_CODE  IS NULL) ");
			sql.append("AND TRUNC(pt.tnx_date) = ? ");
			sql.append("AND pt.txn_id = rp.txn_id ");
			sql.append("AND rp.transaction_ref_no = ps.transaction_ref_no ");
			sql.append("AND ps.gateway_name = pgc.payment_gateway_currency_code ");
			sql.append("AND pgc.payment_gateway_id = pg.payment_gateway_id ");

			sql.append("UNION ALL ");

			sql.append("SELECT pect.txn_timestamp AS transaction_date, ");
			sql.append("pect.amount_paycur AS amount_paycur, ");
			sql.append("pect.amount AS amount_bascur, ");
			sql.append("pect.paycur_code as paymt_curr, ");
			sql.append("pect.nominal_code AS nominal_code, ");
			sql.append("pp.pnr AS PNR, ");
			sql.append("pg.provier_name AS payment_gateway, ");
			sql.append("rp.authorization_id AS auth_code ");
			sql.append("FROM ");
			sql.append("T_PAX_EXT_CARRIER_TRANSACTIONS pect, ");
			sql.append("t_pnr_passenger pp, ");
			sql.append("T_RES_PCD rp, ");
			sql.append("t_ccard_payment_status ps, ");
			sql.append("t_payment_gateway_currency pgc, ");
			sql.append("t_payment_gateway pg ");
			sql.append("WHERE ");
			sql.append("pect.pnr_pax_id = pp.pnr_pax_id ");
			sql.append("AND nominal_code IN (" + Util.buildIntegerInClauseContent(nominalCodes) + ") ");
			sql.append("AND TRUNC(pect.txn_timestamp) = ? ");
			sql.append("AND pect.pax_ext_carrier_txn_id = rp.ext_txn_id ");
			sql.append("AND rp.transaction_ref_no  = ps.transaction_ref_no ");
			sql.append("AND ps.gateway_name = pgc.payment_gateway_currency_code ");
			sql.append("AND pgc.payment_gateway_id = pg.payment_gateway_id");

			if (log.isDebugEnabled()) {
				log.debug("SQL:" + sql.toString() + "date1:" + date + " Date2:" + date);
			}
			log.info("########### " + sql.toString() + " #############");
			Object input[] = new Object[2];
			input[0] = date;
			input[1] = date;
			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate template = new JdbcTemplate(ds);
			nominalCodeAmountMap = (HashMap<String, CreditCardPaymentSageDTO>) template.query(sql.toString(), input,
					new ResultSetExtractor() {
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							HashMap<String, CreditCardPaymentSageDTO> amountMap = new HashMap<String, CreditCardPaymentSageDTO>();
							if (rs != null) {

								BigDecimal amountInPaymentCurrency = null;
								BigDecimal amountInBaseCurrency = null;
								Integer nominalCode = null;
								CreditCardPaymentSageDTO cardSales = null;
								String pnr = null;
								String mapKey = null;
								String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
								String authCode = null;

								while (rs.next()) {
									nominalCode = new Integer(rs.getInt("nominal_code"));
									amountInBaseCurrency = rs.getBigDecimal("amount_bascur");
									amountInPaymentCurrency = rs.getBigDecimal("amount_paycur");
									pnr = rs.getString("pnr");

									if ((nominalCode != null) && (amountInBaseCurrency != null) && (pnr != null)
											&& amountInPaymentCurrency != null) {
										mapKey = pnr + "_" + nominalCode;
										if (amountMap.containsKey(mapKey)) {
											cardSales = amountMap.get(mapKey);
											cardSales.setAmount(AccelAeroCalculator.add(cardSales.getAmount(),
													amountInBaseCurrency));
											cardSales.setAmountInPaymentCurrency(AccelAeroCalculator.add(
													cardSales.getAmountInPaymentCurrency(), amountInPaymentCurrency));
										} else {
											cardSales = new CreditCardPaymentSageDTO();
											cardSales.setTransactionDate(rs.getDate("transaction_date"));
											cardSales.setAmountInPaymentCurrency(amountInPaymentCurrency);
											cardSales.setAmount(amountInBaseCurrency);
											cardSales.setCurrency(baseCurrencyCode);
											cardSales.setPaymentCurrency(rs.getString("paymt_curr"));
											cardSales.setPaymentGatewayName(rs.getString("payment_gateway"));
											cardSales.setPnr(pnr);
											authCode = rs.getString("auth_code");
											if (authCode == null || authCode.trim().isEmpty())
												authCode = "-";
											cardSales.setAuthorizedCode(authCode);
											amountMap.put(mapKey, cardSales);
										}
									}
								}
							}
							return amountMap;
						}
					});
		}

		if (nominalCodeAmountMap != null) {
			creditDTO.addAll(setCardDetailsToDTO(nominalCodeAmountMap, date, creditCardTypeList));
		}

		return creditDTO;
	}

	/*
	 * Setting card details to CreditCardPaymentSageDTO
	 * 
	 * @Param Map: key- pnr_nominalCode, value- CreditCardPaymentSageDTO
	 * 
	 * @Param date
	 * 
	 * @Param List:CreditCardTypeDTO
	 */
	private Collection<CreditCardPaymentSageDTO> setCardDetailsToDTO(
			HashMap<String, CreditCardPaymentSageDTO> nominalCodeAmountMap, Date date,
			Collection<CreditCardTypeDTO> creditCardTypeList) {

		String key = null;
		String[] pnrNominal = null;
		CreditCardTypeDTO cardTypeDTO = null;
		CreditCardPaymentSageDTO cardPaymentDTO = null;
		Collection<CreditCardPaymentSageDTO> creditDTO = new ArrayList<CreditCardPaymentSageDTO>();
		for (Map.Entry<String, CreditCardPaymentSageDTO> entry : nominalCodeAmountMap.entrySet()) {
			key = entry.getKey();
			if (key != null && !key.isEmpty()) {
				pnrNominal = key.split("_");
				if (pnrNominal.length == 2) {
					key = pnrNominal[1];
				}
			}
			// Get Card Details with reference to nominal code
			cardTypeDTO = getCreditCardTypeDTO(creditCardTypeList, key);
			cardPaymentDTO = entry.getValue();
			cardPaymentDTO.setAmount(cardPaymentDTO.getAmount().negate());
			cardPaymentDTO.setAmountInPaymentCurrency(cardPaymentDTO.getAmountInPaymentCurrency().negate());
			if (cardTypeDTO != null) {
				cardPaymentDTO.setCardTypeName(cardTypeDTO.getCardType());
				cardPaymentDTO.setAccountCode(cardTypeDTO.getAccountCode());
				cardPaymentDTO.setCardType(cardTypeDTO.getCardType());
				cardPaymentDTO.setCardTypeId(cardTypeDTO.getCardTypeId());
				cardPaymentDTO.setTransactionDate(date);
				creditDTO.add(cardPaymentDTO);
			}

		}
		return creditDTO;

	}

	/*
	 * Adding payment, refund nominal codes for credit card types
	 */
	private Collection<Integer> getNominalCodes(Collection<CreditCardTypeDTO> creditCardTypeList) {

		Collection<Integer> nominalCodes = new ArrayList<Integer>();
		Iterator<CreditCardTypeDTO> it = creditCardTypeList.iterator();
		while (it.hasNext()) {
			CreditCardTypeDTO ccTypeDTO = it.next();
			if (ccTypeDTO.getPaymentNominalCode() != null) {
				nominalCodes.add(ccTypeDTO.getPaymentNominalCode());
			}
			if (ccTypeDTO.getRefundNominalCode() != null) {
				nominalCodes.add(ccTypeDTO.getRefundNominalCode());
			}
		}
		return nominalCodes;
	}

	/*
	 * Returns the credit card type information
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<CreditCardTypeDTO> getCreditCardPaymentRefundNominalCodes() {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		String sql = "SELECT card_type_id, card_type, payment_nc, reund_nc,account_code " + " FROM T_CREDITCARD_TYPE ";
		return (ArrayList<CreditCardTypeDTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<CreditCardTypeDTO> creditCardTypes = new ArrayList<CreditCardTypeDTO>();
				if (rs != null) {
					while (rs.next()) {
						CreditCardTypeDTO ccTypeDTO = new CreditCardTypeDTO();
						ccTypeDTO.setCardTypeId(new Integer(rs.getInt("card_type_id")));
						ccTypeDTO.setCardType(rs.getString("card_type"));
						ccTypeDTO.setPaymentNominalCode(new Integer(rs.getInt("payment_nc")));
						ccTypeDTO.setRefundNominalCode(new Integer(rs.getInt("reund_nc")));
						ccTypeDTO.setAccountCode(rs.getString("account_code"));
						creditCardTypes.add(ccTypeDTO);
					}
				}
				return creditCardTypes;
			}
		});
	}

	/*
	 * Returns Card Details with reference to nominal code
	 * 
	 * @Param nominalCode
	 * 
	 * @Param cardTypes
	 */
	private CreditCardTypeDTO getCreditCardTypeDTO(Collection<CreditCardTypeDTO> cardTypes, String nominalCode) {
		Integer paymentNC = null;
		Integer refundNC = null;
		CreditCardTypeDTO cardTypeDTORet = null;
		if (cardTypes != null && nominalCode != null) {
			int nominalCodeInt = new Integer(nominalCode);
			for (CreditCardTypeDTO cardTypeDTO : cardTypes) {
				paymentNC = cardTypeDTO.getPaymentNominalCode();
				refundNC = cardTypeDTO.getRefundNominalCode();

				if (paymentNC != null && refundNC != null) {
					if (paymentNC == nominalCodeInt || refundNC == nominalCodeInt) {
						cardTypeDTORet = cardTypeDTO;
						break;
					}
				}
			}
		}
		return cardTypeDTORet;
	}

	/*
	 * Inserting Credit Card Sales to internal summary table via Hiberatne.
	 */
	public Collection<CreditCardSalesSage> insertCreditCardSalesToInternal(Collection<CreditCardPaymentSageDTO> col) {
		log.info("########### insertCreditCardSalesToInternal #############");
		Collection<CreditCardSalesSage> creditCardPOJOS = new ArrayList<CreditCardSalesSage>();
		Date now = new Date();
		Iterator<CreditCardPaymentSageDTO> it = col.iterator();

		if (col != null) {
			while (it.hasNext()) {
				CreditCardPaymentSageDTO dto = (CreditCardPaymentSageDTO) it.next();

				CreditCardSalesSage cardSales = new CreditCardSalesSage();
				cardSales.setDateOfSale(dto.getTransactionDate());
				cardSales.setAmountInPaymentCurrency(dto.getAmountInPaymentCurrency());
				cardSales.setAmount(dto.getAmount());
				cardSales.setCurrency(dto.getCurrency());
				cardSales.setPaymentCurrency(dto.getPaymentCurrency());
				cardSales.setCardTypeId(dto.getCardTypeId());
				cardSales.setPaymentGatewayName(dto.getPaymentGatewayName());
				cardSales.setPnr(dto.getPnr());
				cardSales.setAccountCode(dto.getAccountCode());
				cardSales.setAuthorizedCode(dto.getAuthorizedCode());
				cardSales.setTransferStatus('N');
				cardSales.setTransferTimeStamp(now);

				creditCardPOJOS.add(cardSales);
			}
		}
		saveOrUpdateAllCreditCardSales(creditCardPOJOS);
		log.info("######## Finished Inserting Credit Card Sales To Internal");
		return creditCardPOJOS;
	}

	/*
	 * insert in hibernate pojos
	 * 
	 * @param creditCardSalesPOJOS
	 */
	private void saveOrUpdateAllCreditCardSales(Collection<CreditCardSalesSage> creditCardSalesPOJOS) {
		hibernateSaveOrUpdateAll(creditCardSalesPOJOS);
	}

	/*
	 * Updates transferStatus in internal Summary table from 'N' to 'Y'
	 * 
	 * @Param Collection<CreditCardSalesSage>
	 */
	public void updateInternalCreditTable(Collection<CreditCardSalesSage> pojos) {

		Iterator<CreditCardSalesSage> it = pojos.iterator();
		while (it.hasNext()) {
			CreditCardSalesSage creditCardSales = it.next();
			creditCardSales.setTransferStatus('Y');
		}
		saveOrUpdateAllCreditCardSales(pojos);
	}

	/*
	 * Returns total of generated CreditCard sales from internal summary table
	 * 
	 * @Param fromDate
	 * 
	 * @Param toDate
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getGeneratedCreditCardSalesTotal(Date fromDate, Date toDate) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { fromDate, toDate };
		String sql = "select sum(amount) as tot from t_creditcard_sales_v2 where date_of_sale between ?  and ?";

		BigDecimal d = (BigDecimal) templete.query(sql, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal amount = null;
				while (rs.next()) {
					amount = rs.getBigDecimal("tot");
				}
				return amount;
			}
		});

		return d;
	}

	/*
	 * Returns total of CreditCard Sales on each date
	 * 
	 * @Param fromDate
	 * 
	 * @Param toDate
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] getCreditCardSalesHistory(Date fromDate, Date toDate) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		final ArrayList<CCSalesHistoryDTO> dtoList = new ArrayList<CCSalesHistoryDTO>();
		final ArrayList<java.sql.Date> dateList = new ArrayList<java.sql.Date>();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String from = sdf.format(fromDate);
		String to = sdf.format(toDate);
		StringBuffer sql = new StringBuffer();

		sql.append(" select t.date_of_sale as dos, sum(t.amount) as total, t.transfer_timestamp as transfertime, t.transfer_status as tnsstatus from t_creditcard_sales_v2 t where ");
		sql.append(" trunc(t.date_of_sale) between '");
		sql.append(from);
		sql.append("' and '");
		sql.append(to);
		sql.append("' group by t.date_of_sale, t.transfer_timestamp, t.transfer_status order by t.date_of_sale");

		templete.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					CCSalesHistoryDTO dto = new CCSalesHistoryDTO();

					dto.setDateOfsale(rs.getDate("dos"));
					dto.setCardType("All Cards Total");
					dto.setTotalDailySales(rs.getBigDecimal("total"));
					dto.setTransferStatus(rs.getString("tnsstatus"));
					dto.setTransferTimeStamp(rs.getTimestamp("transfertime"));

					dateList.add(rs.getDate("dos"));
					dtoList.add(dto);
				}
				return dtoList;
			}
		});

		return new Object[] { dtoList, dateList };

	}

	public abstract void insertCreditCardSalesToExternal(Collection<CreditCardPaymentSageDTO> col,
			AccontingSystemTemplate template) throws ClassNotFoundException, SQLException, Exception;

}
