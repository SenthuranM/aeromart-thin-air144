package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CancelSegmentIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public CancelSegmentIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {

		RuleResponseDTO response = getResponseDTO();
		List<Integer> affectedResSegId = new ArrayList<Integer>();

		if (!isCNXReservation(dataContext.getExisitingReservation()) && !isCNXReservation(dataContext.getUpdatedReservation())) {

			for (ReservationSegment existingSeg : dataContext.getExisitingReservation().getSegments()) {
				if (!isCNXSegment(existingSeg)) {
					for (ReservationSegment updatedSeg : dataContext.getUpdatedReservation().getSegments()) {
						if (existingSeg.getSegmentSeq().equals(updatedSeg.getSegmentSeq()) && isCNXSegment(updatedSeg)
								&& !isExchangeSeg(updatedSeg)) {
							affectedResSegId.add(existingSeg.getPnrSegId());
							break;
						}
					}
				}
			}

		}

		updatePaxWiseAffectedSegmentListToAllGivenPax(response, null, affectedResSegId,
				getUncancelledPaxIds(dataContext.getUpdatedReservation()));
		return response;

	}

}