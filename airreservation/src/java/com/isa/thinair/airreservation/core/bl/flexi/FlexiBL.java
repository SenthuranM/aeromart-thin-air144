package com.isa.thinair.airreservation.core.bl.flexi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.FLEXI_RULE_FLEXIBILITY_TYPE;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 * @since August 6, 2010
 */
public class FlexiBL {

	public static boolean reflectFlexiBalancesForCancelReservation(Reservation reservation, boolean isCancelChgOperation,
			boolean isModifyChgOperation) throws ModuleException {
		// Flexi is availed for any one pax fare, should be treated as flexibility applied
		boolean isFlexiApplied = false;

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				boolean isFlexiAppliedForPaxFare = updateFlexibilityBalances(reservationPaxFare, isCancelChgOperation,
						isModifyChgOperation);
				if (isFlexiAppliedForPaxFare) {
					isFlexiApplied = true;
				}
			}
		}

		return isFlexiApplied;
	}

	public static boolean reflectFlexiBalancesForCancelSegment(Reservation reservation, Collection<Integer> pnrSegmentIds,
			boolean applyCnx, boolean applyMod, FlexiRetainDTO flexiRetainDTO) throws ModuleException {
		boolean isFlexiApplied = false;
		Map<Integer, Collection<Integer>> ondWisePnrSegIds = CancellationUtils.getOndGroupWisePnrSegIds(pnrSegmentIds,
				reservation);
		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> reservationFlexibilities = reservation.getPaxOndFlexibilities();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (Integer ondGroupId : ondWisePnrSegIds.keySet()) {
				ReservationPaxFare reservationPaxFareTarget = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
						ondWisePnrSegIds.get(ondGroupId));
				Collection<ReservationPaxOndFlexibilityDTO> paxOndFlexibilities = reservationFlexibilities
						.get(reservationPaxFareTarget.getPnrPaxFareId());		
				if (paxOndFlexibilities != null && !paxOndFlexibilities.isEmpty()) {
					boolean currentFlexiAppliedStatus = applyFlexibilityBalanceUpdationForModifyCancelOnd(
							reservationPaxFareTarget, applyCnx, applyMod, paxOndFlexibilities);
					flexiRetainDTO.addRetainFlexi(ondWisePnrSegIds.get(ondGroupId), reservationPax.getPnrPaxId(),
							paxOndFlexibilities);
					// associating with passenger to carry forward
					if (currentFlexiAppliedStatus) {
						isFlexiApplied = true;
					}
				}
			}
		}
		return isFlexiApplied;
	}

	private static boolean updateFlexibilityBalances(ReservationPaxFare reservationPaxFare, boolean applyCnx, boolean applyMod)
			throws ModuleException {
		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> reservationFlexibilities = reservationPaxFare
				.getReservationPax().getReservation().getPaxOndFlexibilities();
		Collection<ReservationPaxOndFlexibilityDTO> paxOndFlexibilities = reservationFlexibilities.get(reservationPaxFare
				.getPnrPaxFareId());
		return applyFlexibilityBalanceUpdationForModifyCancelOnd(reservationPaxFare, applyCnx, applyMod, paxOndFlexibilities);
	}

	private static boolean applyFlexibilityBalanceUpdationForModifyCancelOnd(ReservationPaxFare reservationPaxFare,
			boolean applyCnx, boolean applyMod, Collection<ReservationPaxOndFlexibilityDTO> paxFareFlexibilites)
			throws ModuleException {
		Map<String, String> flexiruleFlexibilityTypeMap = AirpricingUtils.getAirpricingConfig().getFlexiruleFlexibilityTypeMap();
		Collection<ReservationPaxOndFlexibility> flexibilitiesToUpdate = new ArrayList<ReservationPaxOndFlexibility>();
		boolean isFlexiApplied = false;

		if (paxFareFlexibilites != null) {
			for (ReservationPaxOndFlexibilityDTO flexiChgDTO : paxFareFlexibilites) {
				String flexibilityTypeID = String.valueOf(flexiChgDTO.getFlexibilityTypeId());

				if (((applyMod && flexibilityTypeID.equals(BeanUtils.nullHandler(flexiruleFlexibilityTypeMap
						.get(FLEXI_RULE_FLEXIBILITY_TYPE.MODIFY_OND.name())))) || (applyCnx && flexibilityTypeID.equals(BeanUtils
						.nullHandler(flexiruleFlexibilityTypeMap.get(FLEXI_RULE_FLEXIBILITY_TYPE.CANCEL_OND.name())))))
						&& flexiChgDTO.getStatus().equals(ReservationPaxOndFlexibility.STATUS_ACTIVE)) {

					if (!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationPaxFare.getReservationPax()
							.getReservation().getStatus())) {
						flexiChgDTO.setAvailableCount(flexiChgDTO.getAvailableCount() - 1);
						flexiChgDTO.setUtilizedCount(flexiChgDTO.getUtilizedCount() + 1);
						isFlexiApplied = true; // this variable is used for audit purpose
					}

					flexiChgDTO.setStatus(ReservationPaxOndFlexibility.STATUS_INACTIVE);
				} else {
					flexiChgDTO.setStatus(ReservationPaxOndFlexibility.STATUS_INACTIVE);
				}
				addToUpdateFlexibilitiesList(flexibilitiesToUpdate, flexiChgDTO);
			}
		}

		if (!flexibilitiesToUpdate.isEmpty()) {
			ReservationDAOUtils.DAOInstance.FLEXI_RULE_DAO.saveOrUpdate(flexibilitiesToUpdate);
		}

		return isFlexiApplied;
	}

	private static void addToUpdateFlexibilitiesList(Collection<ReservationPaxOndFlexibility> flexibilitiesToUpdate,
			ReservationPaxOndFlexibilityDTO flexiChgDTO) {
		Collection<ReservationPaxOndFlexibility> colFlexiForPaxFare = ReservationDAOUtils.DAOInstance.FLEXI_RULE_DAO
				.getPaxOndFlexibilities(flexiChgDTO.getPpfId());
		for (ReservationPaxOndFlexibility paxOndFlexibility : colFlexiForPaxFare) {
			if (paxOndFlexibility.getPnrPaxOndFlexiId().intValue() == flexiChgDTO.getPpOndFlxId()) {
				paxOndFlexibility.setAvailableCount(flexiChgDTO.getAvailableCount());
				paxOndFlexibility.setUtilizedCount(flexiChgDTO.getUtilizedCount());
				paxOndFlexibility.setStatus(flexiChgDTO.getStatus());
				flexibilitiesToUpdate.add(paxOndFlexibility);
			}
		}
	}

}
