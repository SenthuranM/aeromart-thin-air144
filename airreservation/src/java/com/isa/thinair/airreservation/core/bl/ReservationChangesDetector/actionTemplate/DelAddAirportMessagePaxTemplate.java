package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.base.BaseObseverActionTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ObserverActionDataContext;
import com.isa.thinair.commons.api.exception.ModuleException;

public class DelAddAirportMessagePaxTemplate extends BaseObseverActionTemplate<ObserverActionDataContext> {

	public DelAddAirportMessagePaxTemplate() {
		super();
	}
	
	List<AirportMessagePassenger> existingPaxList;

	@Override
	public void executeTemplateAction(ObserverActionDataContext dataContext) throws ModuleException {

		this.dataContext = dataContext;
		setExistAirportMessagePassengerFroReservation(dataContext.getNewReservation().getPnr());
		
		
		Map<Integer, Set<Integer>> addMap = dataContext.getAddMap();
		Map<Integer, Set<Integer>> delMap = dataContext.getDelMap();

		// del MAP remove with existing data
		this.checkForExist(addMap, PNLConstants.AdlActions.A, dataContext.getNewReservation().getPnr());

		List<AirportMessagePassenger> addPaxList = createUnexistPax(addMap, dataContext.getNewReservation(),
				PNLConstants.AdlActions.A.toString());

		this.checkForExist(delMap, PNLConstants.AdlActions.D, dataContext.getNewReservation().getPnr());

		List<AirportMessagePassenger> delPaxList = createUnexistPax(delMap, dataContext.getOldReservation(),
				PNLConstants.AdlActions.D.toString());

		alreadyExistpaxListToUpdate.addAll(addPaxList);
		alreadyExistpaxListToUpdate.addAll(delPaxList);
		if (!alreadyExistpaxListToUpdate.isEmpty()) {
			auxilliaryDAO.saveAirportMessagePassenger(alreadyExistpaxListToUpdate);
		}
	}

	
	private void setExistAirportMessagePassengerFroReservation(String pnr) {
		existingPaxList = auxilliaryDAO.getAirportMessagePassengers(pnr);
		existingPaxList = existingPaxList != null ? existingPaxList : new ArrayList<>();
	}

	@Override
	protected void checkForExist(Map<Integer, Set<Integer>> paxMap, AdlActions action, String pnr) throws ModuleException {
		

		if (!isEmptyPaxMap(paxMap)) {
			/*for (AirportMessagePassenger apPax : existingPaxList)*/
			Iterator<AirportMessagePassenger> apPaxItr = existingPaxList.iterator();
			while(apPaxItr.hasNext()){//+
				AirportMessagePassenger apPax = apPaxItr.next();
				if (paxMap.get(apPax.getPnrPaxId()) != null && paxMap.get(apPax.getPnrPaxId()).contains(apPax.getPnrSegId())
						/*&& apPax.getPalStatus().equals(action.toString())*/) {

					if (isValidTransition(apPax.getPalStatus(), action.toString())) {
						paxMap.get(apPax.getPnrPaxId()).remove(apPax.getPnrSegId()); // remove seg id from paxwise map
						alreadyExistpaxListToUpdate.add(updatePax(apPax, getUpdatedStatus(apPax.getPalStatus(), action.toString())));
						apPaxItr.remove();
					}
				}
			}//+
		}
	}
}
