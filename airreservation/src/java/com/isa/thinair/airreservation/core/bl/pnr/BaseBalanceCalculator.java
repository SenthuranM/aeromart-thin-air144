package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.FareInfoTOV2;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.CHAEGE_CODES;
import com.isa.thinair.airreservation.core.bl.common.BalanceToPayBreakdownBL;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public abstract class BaseBalanceCalculator implements IBalanceCalculator {
	private Map<Integer, SegmentSummaryTOV2> paxBalanceSummary = new HashMap<Integer, SegmentSummaryTOV2>();

	protected abstract Set<ReservationPax> getReservationPassengers();

	protected abstract Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseCurrentCharges();

	protected boolean showCnxCharge = false;

	protected boolean showModCharge = false;

	protected Map<Integer, List<ExternalChgDTO>> paxEffectiveTax;
	
	protected boolean infantPaymentSeparated = false;

	protected SegmentSummaryTOV2 getSegmentSummary(Integer pnrPaxId) {
		if (!paxBalanceSummary.containsKey(pnrPaxId)) {
			paxBalanceSummary.put(pnrPaxId, new SegmentSummaryTOV2());
		}
		return paxBalanceSummary.get(pnrPaxId);
	}

	@Override
	public ReservationBalanceTO getReservationBalance() {
		Collection<LCCClientPassengerSummaryTO> paxSummaryList = new ArrayList<LCCClientPassengerSummaryTO>();
		BigDecimal currentAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentCnxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentModAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentNonRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentPenaltyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentSeatAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentMealAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentBaggageAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentInsuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentSSRAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		BigDecimal newAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newCnxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newModAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newPenaltyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newSeatAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newMealAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newBaggageAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newInsuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newSSRAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newExtraFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		LCCClientSegmentSummaryTO segmentSummary = new LCCClientSegmentSummaryTO();
		for (ReservationPax passenger : getReservationPassengers()) {

			SegmentSummaryTOV2 balanceSummary = getSegmentSummary(passenger.getPnrPaxId());

			LCCClientPassengerSummaryTO paxSummary = new LCCClientPassengerSummaryTO();

			// paxSummary.setPnrPaxId(passenger.getPnrPaxId());
			String travelerRefNumber = AppSysParamsUtil.getDefaultCarrierCode() + "|" + PaxTypeUtils.travelerReference(passenger);

			paxSummary.setTravelerRefNumber(travelerRefNumber);
			paxSummary.setPaxName(ReservationApiUtils.getPassengerName("", passenger.getTitle(), passenger.getFirstName(),
					passenger.getLastName(), false));
			paxSummary.setPaxFirstName(passenger.getFirstName());
			paxSummary.setPaxLastName(passenger.getLastName());
			paxSummary.setPaxType(passenger.getPaxType());
			if (!infantPaymentSeparated && passenger.getAccompaniedPaxId() != null && passenger.getInfants() != null && passenger.getInfants().size() > 0) {
				ReservationPax accompaniedPax = passenger.getInfants().iterator().next();
				paxSummary.setInfantName(ReservationApiUtils.getPassengerName("", accompaniedPax.getTitle(),
						accompaniedPax.getFirstName(), accompaniedPax.getLastName(), false));
				String accmpTrvlRef = AppSysParamsUtil.getDefaultCarrierCode() + "|"
						+ PaxTypeUtils.travelerReference(accompaniedPax);
				paxSummary.setAccompaniedTravellerRef(accmpTrvlRef);
			}
			paxSummary.setSegmentSummaryTO(balanceSummary.getSegmentSummaryTO());
			// paxSummary.setCnxChargeDetailTO(cnxChargeDetailTO);
			// paxSummary.setModChargeDetailTO(modChargeDetailTO);

			BigDecimal totalRefundables = balanceSummary.getCurrentRefundableAmounts().getTotalPrice();
			BigDecimal currentModificationChg = balanceSummary.getIdentifiedTotalModCharge();
			BigDecimal currentCancellationChg = balanceSummary.getIdentifiedTotalCnxCharge();
			BigDecimal newONDCharges = balanceSummary.getNewAmounts().getTotalPrice();
			BigDecimal newPenalty = balanceSummary.getModificationPenatly();
			BigDecimal totalExcludedSegmentsChg = balanceSummary.getExcludedSegCharge();

			FareInfoTOV2 fareInfoTOV2 = new FareInfoTOV2();
			fareInfoTOV2.addMetaCharges(getPnrPaxIdWiseCurrentCharges().get(passenger.getPnrPaxId()));
			BigDecimal currentTotalCharges = fareInfoTOV2.getTotalPrice();

			BigDecimal paxTotalPrice = AccelAeroCalculator.add(currentTotalCharges, totalRefundables.negate(),
					currentModificationChg, currentCancellationChg, newONDCharges, newPenalty,
					balanceSummary.getIdentifiedExtraFeeAmount());

			BigDecimal totalAmountDue = AccelAeroCalculator.add(paxTotalPrice, totalExcludedSegmentsChg, passenger
					.getTotalPaidAmount().negate());
			BigDecimal totalCredits = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (totalAmountDue.compareTo(BigDecimal.ZERO) < 0) {
				totalCredits = totalAmountDue.negate();
				totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
			}

			if (AppSysParamsUtil.isLMSEnabled()
					&& (!passenger.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT) || infantPaymentSeparated)
					&& totalAmountDue.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				BalanceToPayBreakdownBL.constructBalanceToPayBreakdown(passenger, balanceSummary, paxSummary, infantPaymentSeparated);
			}

			paxSummary.setTotalPrice(paxTotalPrice);
			paxSummary.setTotalAmountDue(totalAmountDue);
			paxSummary.setTotalCreditAmount(totalCredits);
			paxSummary.setTotalPaidAmount(passenger.getTotalPaidAmount());
			paxSummary.setTotalCnxCharge(balanceSummary.getIdentifiedTotalCnxCharge());
			paxSummary.setTotalModCharge(balanceSummary.getIdentifiedTotalModCharge());
			paxSummary.setTotalPenalty(newPenalty);
			paxSummary.setTotalExcludedSegAmount(totalExcludedSegmentsChg);
			// paxSummary.setTotalFareDiscountAmount(totalPaxFareDiscount);

			currentAdjAmount = AccelAeroCalculator.add(currentAdjAmount, balanceSummary.getCurrentAmounts().getTotalAdjCharge());
			currentCnxAmount = AccelAeroCalculator.add(currentCnxAmount, balanceSummary.getCurrentAmounts().getTotalCanCharge());
			currentFareAmount = AccelAeroCalculator.add(currentFareAmount, balanceSummary.getCurrentAmounts().getTotalFare());
			currentModAmount = AccelAeroCalculator.add(currentModAmount, balanceSummary.getCurrentAmounts().getTotalModCharge());
			currentNonRefunds = AccelAeroCalculator.add(currentNonRefunds, balanceSummary.getCurrentNonRefundableAmounts()
					.getTotalPrice());
			currentRefunds = AccelAeroCalculator
					.add(currentRefunds, balanceSummary.getCurrentRefundableAmounts().getTotalPrice());
			currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, balanceSummary.getCurrentAmounts()
					.getTotalSurcharge());
			if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
				for (ChargeMetaTO charge : balanceSummary.getCurrentAmounts().getSurcharges()) {
					if (charge.getChargeCode().equals(CHAEGE_CODES.SM.toString())) {
						currentSeatAmount = AccelAeroCalculator.add(currentSeatAmount, charge.getChargeAmount());
						currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount()
								.negate());
					} else if (charge.getChargeCode().equals(CHAEGE_CODES.ML.toString())) {
						currentMealAmount = AccelAeroCalculator.add(currentMealAmount, charge.getChargeAmount());
						currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount()
								.negate());
					} else if (charge.getChargeCode().equals(CHAEGE_CODES.BG.toString())) {
						currentBaggageAmount = AccelAeroCalculator.add(currentBaggageAmount, charge.getChargeAmount());
						currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount()
								.negate());
					} else if (charge.getChargeCode().equals(CHAEGE_CODES.IN.toString())) {
						currentInsuranceAmount = AccelAeroCalculator.add(currentInsuranceAmount, charge.getChargeAmount());
						currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount()
								.negate());
					} else if (charge.getChargeCode().equals(CHAEGE_CODES.SSR.toString())) {
						currentSSRAmount = AccelAeroCalculator.add(currentSSRAmount, charge.getChargeAmount());
						currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount()
								.negate());
					}
				}
			}
			currentTaxAmount = AccelAeroCalculator.add(currentTaxAmount, balanceSummary.getCurrentAmounts().getTotalTax());
			currentTotalPrice = AccelAeroCalculator.add(currentTotalPrice, balanceSummary.getCurrentAmounts().getTotalPrice());
			currentDiscount = AccelAeroCalculator.add(currentDiscount, balanceSummary.getCurrentAmounts().getTotalDiscount());
			currentPenaltyAmount = AccelAeroCalculator.add(currentPenaltyAmount, balanceSummary.getCurrentAmounts()
					.getTotalPenalty());

			newAdjAmount = AccelAeroCalculator.add(newAdjAmount, balanceSummary.getNewAmounts().getTotalAdjCharge());
			newCnxAmount = AccelAeroCalculator.add(newCnxAmount, balanceSummary.getIdentifiedTotalCnxCharge());
			newFareAmount = AccelAeroCalculator.add(newFareAmount, balanceSummary.getNewAmounts().getTotalFare());
			newModAmount = AccelAeroCalculator.add(newModAmount, balanceSummary.getIdentifiedTotalModCharge());
			newExtraFeeAmount = AccelAeroCalculator.add(newExtraFeeAmount, balanceSummary.getIdentifiedExtraFeeAmount());
			newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, balanceSummary.getNewAmounts().getTotalSurcharge());
			if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
				for (ChargeMetaTO charge : balanceSummary.getNewAmounts().getSurcharges()) {
					if (CHAEGE_CODES.SM.toString().equals(charge.getChargeCode())) {
						newSeatAmount = AccelAeroCalculator.add(newSeatAmount, charge.getChargeAmount());
						newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
					} else if (CHAEGE_CODES.ML.toString().equals(charge.getChargeCode())) {
						newMealAmount = AccelAeroCalculator.add(newMealAmount, charge.getChargeAmount());
						newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
					} else if (CHAEGE_CODES.BG.toString().equals(charge.getChargeCode())) {
						newBaggageAmount = AccelAeroCalculator.add(newBaggageAmount, charge.getChargeAmount());
						newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
					} else if (CHAEGE_CODES.IN.toString().equals(charge.getChargeCode())) {
						newInsuranceAmount = AccelAeroCalculator.add(newInsuranceAmount, charge.getChargeAmount());
						newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
					} else if (CHAEGE_CODES.SSR.toString().equals(charge.getChargeCode())) {
						newSSRAmount = AccelAeroCalculator.add(newSSRAmount, charge.getChargeAmount());
						newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
					}
				}
			}
			newTaxAmount = AccelAeroCalculator.add(newTaxAmount, balanceSummary.getNewAmounts().getTotalTax());
			newPenaltyAmount = AccelAeroCalculator.add(newPenaltyAmount, newPenalty);
			newTotalPrice = AccelAeroCalculator.add(newTotalPrice, balanceSummary.getNewAmounts().getTotalPrice(),
					balanceSummary.getIdentifiedTotalCnxCharge(), balanceSummary.getIdentifiedTotalModCharge(), newPenalty);
			newDiscount = AccelAeroCalculator.add(newDiscount, balanceSummary.getNewAmounts().getTotalDiscount());

			paxSummaryList.add(paxSummary);
		}

		segmentSummary.setCurrentAdjAmount(currentAdjAmount);
		segmentSummary.setCurrentCnxAmount(currentCnxAmount);
		segmentSummary.setCurrentFareAmount(currentFareAmount);
		segmentSummary.setCurrentModAmount(currentModAmount);
		segmentSummary.setCurrentNonRefunds(currentNonRefunds);
		segmentSummary.setCurrentRefunds(currentRefunds);
		segmentSummary.setCurrentSurchargeAmount(currentSurchargeAmount);
		segmentSummary.setCurrentSeatAmount(currentSeatAmount);
		segmentSummary.setCurrentMealAmount(currentMealAmount);
		segmentSummary.setCurrentBaggageAmount(currentBaggageAmount);
		segmentSummary.setCurrentInsuranceAmount(currentInsuranceAmount);
		segmentSummary.setCurrentSSRAmount(currentSSRAmount);
		segmentSummary.setCurrentTaxAmount(currentTaxAmount);
		segmentSummary.setCurrentTotalPrice(currentTotalPrice);
		segmentSummary.setCurrentDiscount(currentDiscount);
		segmentSummary.setCurrentModificationPenatly(currentPenaltyAmount);

		segmentSummary.setNewAdjAmount(newAdjAmount);
		segmentSummary.setNewCnxAmount(newCnxAmount);
		segmentSummary.setNewFareAmount(newFareAmount);
		segmentSummary.setNewModAmount(newModAmount);
		segmentSummary.setNewSurchargeAmount(newSurchargeAmount);
		segmentSummary.setNewSeatAmount(newSeatAmount);
		segmentSummary.setNewMealAmount(newMealAmount);
		segmentSummary.setNewBaggageAmount(newBaggageAmount);
		segmentSummary.setNewInsuranceAmount(newInsuranceAmount);
		segmentSummary.setNewSSRAmount(newSSRAmount);
		segmentSummary.setNewTaxAmount(newTaxAmount);
		segmentSummary.setModificationPenalty(newPenaltyAmount);
		segmentSummary.setNewTotalPrice(newTotalPrice);
		segmentSummary.setNewDiscount(newDiscount);
		segmentSummary.setNewExtraFeeAmount(newExtraFeeAmount);

		ReservationBalanceTO reservationBalanceTO = new ReservationBalanceTO();
		reservationBalanceTO.setInfantPaymentSeparated(infantPaymentSeparated);
		reservationBalanceTO.addPaxSummaryList(paxSummaryList);
		reservationBalanceTO.setSegmentSummary(segmentSummary);
		reservationBalanceTO.setShowCnxCharge(showCnxCharge);
		reservationBalanceTO.setShowModCharge(showModCharge);

		reservationBalanceTO.setPaxEffectiveTax(this.paxEffectiveTax);

		return reservationBalanceTO;
	}

}
