package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.segment.FlownAssitUnit;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for PRE & POST handling discount related operations on charges
 * 
 * @author Dilan
 * 
 * @isa.module.command name="preProcessDiscount"
 */

public class PreProcessDiscount extends DefaultBaseCommand {
	private static Log log = LogFactory.getLog(PreProcessDiscount.class);

	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside PreProcessDiscount");
		@SuppressWarnings("unchecked")
		Collection<OndFareDTO> ondFareDTOs = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		DiscountedFareDetails discFareDetailsDTO = this.getParameter(CommandParamNames.FARE_DISCOUNT_INFO,
				DiscountedFareDetails.class);

		String auditStr = null;
		ReservationDiscountDTO reservationDiscountDTO = null;
		DISCOUNT_METHOD discountMethod = null;
		Reservation reservation = this.getParameter(CommandParamNames.RESERVATION, Reservation.class);

		if (reservation == null || ondFareDTOs == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (discFareDetailsDTO != null) {
			if (discFareDetailsDTO.getPromotionId() == null) {
				// Handle Fare discount
				Float fareDiscountPercentage = 0F;
				boolean domDisApplied = false;
				Integer domFareDiscPerc = 0;
				ArrayList<String> domesticSegmentCodeList = null;

				if (discFareDetailsDTO != null) {
					fareDiscountPercentage = discFareDetailsDTO.getFarePercentage();
					domDisApplied = discFareDetailsDTO.isDomesticDiscountApplied();
					domFareDiscPerc = discFareDetailsDTO.getDomFareDiscountPercentage();
					domesticSegmentCodeList = discFareDetailsDTO.getDomesticSegmentCodeList();
				}

				// TODO clean up as per below comments
				// unify the domestic/normal/promo discount applying part and take out the audits from logic
				// move global configs like domestic discount percentage from front ends to backend as it's a global
				// configuration
				// add proper discount validations
				//
				if (domDisApplied && domesticSegmentCodeList != null && domesticSegmentCodeList.size() > 0
						&& AppSysParamsUtil.isDomesticFareDiscountEnabled()) {
					discountMethod = DISCOUNT_METHOD.DOM_FARE_DISCOUNT;
					log.debug("domestic fare discount ");

				} else if (fareDiscountPercentage != null && fareDiscountPercentage > 0) {
					discountMethod = DISCOUNT_METHOD.FARE_DISCOUNT;
					log.debug("fare discount ");
				} else {
					log.debug("Fare Discount Type is not defined ");
				}
			} else {
				discountMethod = DISCOUNT_METHOD.PROMOTION;
				log.debug("promotion discount ");
			}
		}

		// calling new DiscountCalculator
		if (discountMethod != null) {

			DiscountRQ promotionRQ = new DiscountRQ(discountMethod);
			promotionRQ.setDiscountInfoDTO(discFareDetailsDTO);
			promotionRQ.setValidateCriteria(true);
			promotionRQ.setPaxChargesList(ReservationApiUtils.transformPaxList(reservation.getPassengers(), discFareDetailsDTO));
			promotionRQ.setPaxQtyList(null);
			promotionRQ.setOndFareDTOs(ondFareDTOs);

			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.DISCOUNT_CALCULATOR);
			command.setParameter(CommandParamNames.PROMO_CALCULATOR_RQ, promotionRQ);
			command.setParameter(CommandParamNames.RESERVATION, reservation);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			ServiceResponce serviceResponce = command.execute();
			reservationDiscountDTO = (ReservationDiscountDTO) serviceResponce
					.getResponseParam(CommandParamNames.RESERVATION_DISCOUNT_DTO);

			auditStr = composeDiscountAudit(reservationDiscountDTO, discountMethod);
			
			FlownAssitUnit flownAsstUnit = getParameter(CommandParamNames.FLOWN_ASSIT_UNIT, FlownAssitUnit.class);
			if (reservationDiscountDTO != null && reservationDiscountDTO.isDiscountExist() && flownAsstUnit != null
					&& flownAsstUnit.getModifyAsst() != null) {
				flownAsstUnit.getModifyAsst().setReservationDiscountDTO(reservationDiscountDTO);
			}
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.FARE_DISCOUNT_AUDIT, auditStr);
		response.addResponceParam(CommandParamNames.RESERVATION_DISCOUNT_DTO, reservationDiscountDTO);

		if (reservationDiscountDTO != null) {
			response.addResponceParam(CommandParamNames.USED_PROMOTION_CREDITS, reservationDiscountDTO.getTotalDiscount());
		}

		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}

		log.debug("Exit PreProcessDiscount");
		return response;
	}

	private String composeDiscountAudit(ReservationDiscountDTO reservationDiscountDTO, DISCOUNT_METHOD discountMethod) {
		StringBuffer auditStr = new StringBuffer();
		if (reservationDiscountDTO != null && reservationDiscountDTO.getTotalDiscount().doubleValue() > 0) {

			Map<Collection<Integer>, Map<String, BigDecimal>> paxTypeTotalDiscountBySegment = new HashMap<Collection<Integer>, Map<String, BigDecimal>>();

			Map<Integer, PaxDiscountDetailTO> paxDiscountDetails = reservationDiscountDTO.getPaxDiscountDetails();
			if (paxDiscountDetails != null && !paxDiscountDetails.isEmpty()) {

				for (Integer paxSequence : paxDiscountDetails.keySet()) {
					PaxDiscountDetailTO paxDiscountDetailTO = paxDiscountDetails.get(paxSequence);

					Map<Collection<Integer>, BigDecimal> discountByFltSegIdMap = paxDiscountDetailTO
							.getDiscountTotalMapByFltSegId();

					if (discountByFltSegIdMap != null && !discountByFltSegIdMap.isEmpty()) {
						for (Collection<Integer> fltSegIds : discountByFltSegIdMap.keySet()) {

							BigDecimal paxTypeDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

							BigDecimal paxTotalDiscount = discountByFltSegIdMap.get(fltSegIds);

							if (paxTotalDiscount != null) {
								if (paxTypeTotalDiscountBySegment.get(fltSegIds) == null) {
									Map<String, BigDecimal> discountByPaxType = new HashMap<String, BigDecimal>();
									discountByPaxType.put(PaxTypeTO.ADULT, new BigDecimal(0));
									discountByPaxType.put(PaxTypeTO.CHILD, new BigDecimal(0));
									discountByPaxType.put(PaxTypeTO.INFANT, new BigDecimal(0));
									paxTypeTotalDiscountBySegment.put(fltSegIds, discountByPaxType);
								}

								Map<String, BigDecimal> discountByPaxType = paxTypeTotalDiscountBySegment.get(fltSegIds);

								if (discountByPaxType.get(paxDiscountDetailTO.getPaxType()) == null) {
									paxTypeDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

								}

								paxTypeDiscount = AccelAeroCalculator.add(paxTypeDiscount, paxTotalDiscount);
								discountByPaxType.put(paxDiscountDetailTO.getPaxType(), paxTypeDiscount);

								paxTypeTotalDiscountBySegment.put(fltSegIds, discountByPaxType);
							}

						}
					}

				}
			}

			Map<Collection<Integer>, String> segCodeByFltSegId = reservationDiscountDTO.getSegCodeByFltSegId();
			if (segCodeByFltSegId != null && !segCodeByFltSegId.isEmpty()) {
				boolean first = true;

				for (Collection<Integer> fltSegIds : segCodeByFltSegId.keySet()) {
					String segmentCode = segCodeByFltSegId.get(fltSegIds);
					Map<String, BigDecimal> discountByPaxType = paxTypeTotalDiscountBySegment.get(fltSegIds);
					BigDecimal totAdult = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal totChild = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal totInfant = AccelAeroCalculator.getDefaultBigDecimalZero();
					boolean hasAdult = false, hasChild = false, hasInfant = false;

					if (discountByPaxType != null) {
						if (discountByPaxType.get(PaxTypeTO.ADULT) != null) {
							totAdult = discountByPaxType.get(PaxTypeTO.ADULT);
							hasAdult = true;
						}
						if (discountByPaxType.get(PaxTypeTO.CHILD) != null) {
							totChild = discountByPaxType.get(PaxTypeTO.CHILD);
							hasChild = true;
						}
						if (discountByPaxType.get(PaxTypeTO.INFANT) != null) {
							totInfant = discountByPaxType.get(PaxTypeTO.INFANT);
							hasInfant = true;
						}
					}

					if (first
							&& (DISCOUNT_METHOD.FARE_DISCOUNT != discountMethod || DISCOUNT_METHOD.DOM_FARE_DISCOUNT != discountMethod)
							&& reservationDiscountDTO.getDiscountPercentage() > 0) {
						auditStr.append(" percentage:");
						auditStr.append(reservationDiscountDTO.getDiscountPercentage());
						auditStr.append("% ");
					}

					if (!first) {
						auditStr.append(", ");
					}

					auditStr.append(segmentCode);
					auditStr.append(" Org[");
					if (hasAdult) {
						auditStr.append(" AD:" + totAdult);
					}
					if (hasChild) {
						auditStr.append(", CH:" + totChild);
					}
					if (hasInfant) {
						auditStr.append(", IN:" + totInfant);
					}
					auditStr.append("]");

					first = false;

				}
				auditStr.append(", Total discount:" + reservationDiscountDTO.getTotalDiscount());
			}

		}

		return auditStr.toString();
	}
}
