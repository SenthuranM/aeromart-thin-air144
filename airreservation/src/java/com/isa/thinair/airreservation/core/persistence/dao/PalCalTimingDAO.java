package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.pal.PALTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PalCalTiming;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface PalCalTimingDAO {

	public void savePalCalTiming(PalCalTiming palcalTiming);

	public Page getAllPALCALTimings(String airportCode, int startRec, int numOfRecs, String flightNo);

	public void deletePAlCalTiming(PalCalTiming palCalTiming);

	public void isduplicatesExistForPalCAlTimings(PalCalTiming palcalTiming) throws ModuleException;

	public List<PalCalTiming> getActiveAirportPALCAlTimings(Date date);

	public ArrayList<PALTransMissionDetailsDTO> getFlightsForPALSchedulingForAirport(Date date, PalCalTiming timing);

	public boolean hasValidSitaConfigsForAirPort(String airportCode);
	
}
