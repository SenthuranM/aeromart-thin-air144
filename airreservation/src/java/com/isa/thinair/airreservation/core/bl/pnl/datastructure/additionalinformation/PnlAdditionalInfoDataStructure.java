/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.pnl.datastructure.additionalinformation;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.BaggageInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.InboundConnectionInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.InfantInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.MealInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.OnwardConnectionsInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.SeatInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.SsrInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.WaitListInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.AdditionalInformationBaseStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.BaseAdditionalInformation;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public class PnlAdditionalInfoDataStructure<K extends BasePassengerCollection, T extends BaseDataContext>
extends AdditionalInformationBaseStructure
{

	@Override
	public void buildSeatInfomration() throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildSeatInformation = new SeatInformation();
		buildSeatInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	public void buildMealInfomration() throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildMealInformation = new MealInformation();
		buildMealInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	public void buildBaggageInfomration(String departureAirportCode) throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildBaggageInformation = new BaggageInformation(
				departureAirportCode);
		buildBaggageInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	public void buildSsrInfomration() throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildSsrInformation = new SsrInformation();
		buildSsrInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	public void buildWaitlistPassengerInformation() throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildWaitlistPassengerInformation = new WaitListInformation();
		buildWaitlistPassengerInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	public void buildInboundConnectionsInformation(
			String departureAirportCode, Integer flightId) throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildInboundConnectionInformation = new InboundConnectionInformation(
				departureAirportCode, flightId);
		buildInboundConnectionInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}
	
	public void buildOnwardConnectionsInformation(
			String departureAirportCode, Integer flightId) throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildOnwardConnectionInformation = new OnwardConnectionsInformation(
				departureAirportCode, flightId);
		buildOnwardConnectionInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	public void buildInfantsInformation(Integer flightId) throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildInfantInformation = new InfantInformation(flightId);
		buildInfantInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}
	

}
