/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.TnxAgentPayment;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.revacc.TnxFactory;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for Revenue modification per passenger
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="revenueModification"
 */
public class RevenueModification extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(RevenueModification.class);

	/**
	 * Execute method of the RevenueAccounting command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		Map<Integer, PaymentAssembler> pnrPaxIdAndPayments = (Map<Integer, PaymentAssembler>) this
				.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		Map<Integer, RevenueDTO> passengerRevenueMap = (Map<Integer, RevenueDTO>) this
				.getParameter(CommandParamNames.PASSENGER_REVENUE_MAP);
		ReservationPaymentMetaTO reservationPaymentMetaTO = (ReservationPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAYMENT_META_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Collection<CardDetailConfigDTO> cardConfigData = (Collection<CardDetailConfigDTO>) this
				.getParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA);
		boolean enableTransactionGranularity = (Boolean) this.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);
		Boolean isRequote = getParameter(CommandParamNames.IS_REQUOTE, Boolean.FALSE, Boolean.class);
		boolean doRevPurchase = getParameter(CommandParamNames.DO_REV_PURCHASE, Boolean.FALSE, Boolean.class);
		boolean isFirstPayment = getParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.FALSE, Boolean.class);
		boolean isActualPayment = this.getParameter(CommandParamNames.IS_ACTUAL_PAYMENT, Boolean.FALSE, Boolean.class);
		Map<Integer, Map<String, BigDecimal>> lmsPaxProductRedeemedAmount = (Map<Integer, Map<String, BigDecimal>>) this
				.getParameter(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS);
		List<TransactionSegment> transactionSegments = (List<TransactionSegment>) this
				.getParameter(CommandParamNames.TRNX_SEGMENTS);

		if (isRequote && doRevPurchase) {
			return new DefaultServiceResponse(true);
		}
		// boolean isCancelChgOperation = this.getParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION, false,
		// Boolean.class);

		// if (isCancelChgOperation) {
		// DefaultServiceResponse response = new DefaultServiceResponse(true);
		// response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		// return response;
		// }

		// Checking params
		this.validateParams(pnrPaxIdAndPayments, passengerRevenueMap, credentialsDTO);

		Iterator<Integer> itPaxIds = passengerRevenueMap.keySet().iterator();
		Integer pnrPaxId;
		RevenueDTO revenueDTO;
		Collection<ITnxPayment> passengerPayments = null;
		String cashReceiptNumber = null;
		String onAccountRecieptNumber = null;
		String creditCardReceiptNumber = null;
		String recieptNumber = null;
		Integer transactionSeq;
		Map<String, BigDecimal> lmsProductRedeemedAmount = null;

		while (itPaxIds.hasNext()) {
			pnrPaxId = itPaxIds.next();
			revenueDTO = passengerRevenueMap.get(pnrPaxId);

			PaymentInfo paymentInfo;
			ITnxPayment iTnxPayment;

			PaymentAssembler paymentAssembler = pnrPaxIdAndPayments.get(pnrPaxId);

			transactionSeq = retrieveCorrectTransactionSeq(pnrPaxId, isRequote);

			if (lmsPaxProductRedeemedAmount != null && lmsPaxProductRedeemedAmount.size() > 0) {
				lmsProductRedeemedAmount = lmsPaxProductRedeemedAmount.get(pnrPaxId);
			}

			passengerPayments = new ArrayList<ITnxPayment>();
			// Payment happening from the user
			if (paymentAssembler != null) {
				if (reservationPaymentMetaTO != null) {
					reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo().get(pnrPaxId)
							.setPerPaxBaseTotalPayAmount(paymentAssembler.getTotalPayAmount());
				}
				Iterator<PaymentInfo> itPaymentInfo = paymentAssembler.getPayments().iterator();				

				while (itPaymentInfo.hasNext()) {
					paymentInfo = itPaymentInfo.next();

					// For Pax Credit Payment
					if (paymentInfo instanceof PaxCreditInfo) {
						PaxCreditDTO paxCreditDTO = ((PaxCreditInfo) paymentInfo).getPaxCredit();
						TnxCreditPayment payment = TnxFactory.getTnxCreditPayment(paxCreditDTO, credentialsDTO.getAgentCode());
						passengerPayments.add(payment);
					}

					// For Card Payment
					else if (paymentInfo instanceof CardPaymentInfo) {
						if (creditCardReceiptNumber != null) {
							recieptNumber = creditCardReceiptNumber;
						} else {
							// If applicable will populate reciept number
							creditCardReceiptNumber = TicketingUtils
									.getRecieptNoForPayment(ReservationInternalConstants.PaymentMode.CREDITCARD);
							recieptNumber = creditCardReceiptNumber;
						}

						CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) paymentInfo;
						iTnxPayment = TnxPaymentFactory.getCardPaymentInstance(paymentInfo.getTotalAmount(),
								cardPaymentInfo.getType(), cardPaymentInfo.getNo(), cardPaymentInfo.getName(),
								cardPaymentInfo.getAddress(), cardPaymentInfo.getEDate(), cardPaymentInfo.getSecurityCode(),
								cardPaymentInfo.getPnr(), cardPaymentInfo.getPaymentBrokerRefNo(),
								paymentInfo.getPayCurrencyDTO(), cardPaymentInfo.getPaymentReferanceTO(),
								cardPaymentInfo.getPayCarrier(), cardPaymentInfo.getLccUniqueId(),
								cardPaymentInfo.getAuthorizationId(), null);
						passengerPayments.add(iTnxPayment);
					}

					// For Agent Credit
					else if (paymentInfo instanceof AgentCreditInfo) {
						if (onAccountRecieptNumber != null) {
							recieptNumber = onAccountRecieptNumber;
						} else {
							// If applicable will populate reciept number
							onAccountRecieptNumber = TicketingUtils
									.getRecieptNoForPayment(ReservationInternalConstants.PaymentMode.ONACCOUNT);
							recieptNumber = onAccountRecieptNumber;
						}

						AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
						iTnxPayment = TnxPaymentFactory.getOnAccountPaymentInstance(paymentInfo.getTotalAmount(),
								agentCreditInfo.getAgentCode(), paymentInfo.getPayCurrencyDTO(),
								agentCreditInfo.getPaymentReferenceTO(), agentCreditInfo.getPayCarrier(),
								agentCreditInfo.getLccUniqueId(), null);

						if (iTnxPayment instanceof TnxAgentPayment) {
							TnxAgentPayment agentPayment = (TnxAgentPayment) iTnxPayment;
							if (credentialsDTO.getSalesChannelCode() != null && credentialsDTO.getDirectBillId() != null
									&& !credentialsDTO.getDirectBillId().isEmpty()
									&& credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_GOQUO)
									&& agentPayment.getPaymentReferenceTO() != null) {
								agentPayment.getPaymentReferenceTO().setPaymentRef(credentialsDTO.getDirectBillId());
							}

						}

						passengerPayments.add(iTnxPayment);
					}

					// For Cash Payment
					else if (paymentInfo instanceof CashPaymentInfo) {
						if (cashReceiptNumber != null) {
							recieptNumber = cashReceiptNumber;
						} else {
							// If applicable will populate reciept number
							cashReceiptNumber = TicketingUtils
									.getRecieptNoForPayment(ReservationInternalConstants.PaymentMode.CASH);
							recieptNumber = cashReceiptNumber;
						}

						CashPaymentInfo cashPaymentInfo = (CashPaymentInfo) paymentInfo;
						/**
						 * We can't skip the zero cash payment if we are to record payment breakdown
						 */
						iTnxPayment = TnxPaymentFactory.getCashPaymentInstance(paymentInfo.getTotalAmount(),
								paymentInfo.getPayCurrencyDTO(), cashPaymentInfo.getPaymentReferanceTO(),
								cashPaymentInfo.getPayCarrier(), cashPaymentInfo.getLccUniqueId(), null);
						passengerPayments.add(iTnxPayment);
					}

					// For Loyalty Payment
					else if (paymentInfo instanceof LMSPaymentInfo) {
						LMSPaymentInfo lmsPaymentInfo = (LMSPaymentInfo) paymentInfo;

						iTnxPayment = TnxPaymentFactory.getLMSPaymentInstance(lmsPaymentInfo.getLoyaltyMemberAccountId(),
								lmsPaymentInfo.getRewardIDs(), paymentInfo.getTotalAmount(), paymentInfo.getPayCurrencyDTO(),
								lmsPaymentInfo.getPayCarrier(), lmsPaymentInfo.getLccUniqueId(),
								lmsPaymentInfo.getPaymentTnxId());
						passengerPayments.add(iTnxPayment);
					} else if (paymentInfo instanceof VoucherPaymentInfo) {
						VoucherPaymentInfo voucherPaymentInfo = (VoucherPaymentInfo) paymentInfo;
						iTnxPayment = TnxPaymentFactory.getVoucherPaymentInstance(voucherPaymentInfo.getVoucherDTO(),
								paymentInfo.getTotalAmount(), paymentInfo.getPayCurrencyDTO(), voucherPaymentInfo.getPayCarrier(),
								voucherPaymentInfo.getLccUniqueId(), voucherPaymentInfo.getPaymentTnxId());
						passengerPayments.add(iTnxPayment);
					} else if (paymentInfo instanceof VoucherPaymentInfo) {
						VoucherPaymentInfo voucherPaymentInfo = (VoucherPaymentInfo) paymentInfo;
						iTnxPayment = TnxPaymentFactory.getVoucherPaymentInstance(voucherPaymentInfo.getVoucherDTO(),
								paymentInfo.getTotalAmount(), paymentInfo.getPayCurrencyDTO(),
								voucherPaymentInfo.getPayCarrier(), voucherPaymentInfo.getLccUniqueId(),
								voucherPaymentInfo.getPaymentTnxId());
						passengerPayments.add(iTnxPayment);
					}
				}
			}
			if (reservationPaymentMetaTO != null && revenueDTO.getPnrPaxId() != null) {
				ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo()
						.get(revenueDTO.getPnrPaxId());
				reservationPaxPaymentMetaTO.addMapPerPaxWiseOndRefundableCharges(revenueDTO.getMapRefundableCharges());

				// Record the modification with the payments
				ReservationModuleUtils.getRevenueAccountBD().recordModification(revenueDTO.getPnrPaxId().toString(),
						revenueDTO.getCreditTotal(), revenueDTO.getAddedTotal(), revenueDTO.getCancelOrModifyTotal(),
						revenueDTO.getModificationPenalty(), revenueDTO.getActionNC(), revenueDTO.getChargeNC(),
						passengerPayments, reservationPaxPaymentMetaTO, credentialsDTO, recieptNumber, cardConfigData,
						enableTransactionGranularity, isFirstPayment, isActualPayment, transactionSeq, lmsProductRedeemedAmount,
						transactionSegments);
			}
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS, this.getParameter(CommandParamNames.FLIGHT_SEGMENT_IDS));
		response.addResponceParam(CommandParamNames.PNR, this.getParameter(CommandParamNames.PNR));
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		log.debug("Exit execute");
		return response;
	}

	private Integer retrieveCorrectTransactionSeq(Integer pnrPaxId, Boolean isRequote) {
		Integer maxTxnSeq = 0;
		try {
			maxTxnSeq = ReservationModuleUtils.getRevenueAccountBD().getPNRPaxMaxTransactionSeq(pnrPaxId);
			if (isRequote) {
				maxTxnSeq = maxTxnSeq + 1;
			}
		} catch (ModuleException e) {
			log.error("Unable to retrieve transaction Sequence");
			maxTxnSeq = 1;
		}
		return maxTxnSeq;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnrPaxIdAndPayments
	 * @param passengerRevenueMap
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Map pnrPaxIdAndPayments, Map passengerRevenueMap, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (pnrPaxIdAndPayments == null || passengerRevenueMap == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

}
