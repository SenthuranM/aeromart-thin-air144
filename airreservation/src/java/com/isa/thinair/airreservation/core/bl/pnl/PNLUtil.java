package com.isa.thinair.airreservation.core.bl.pnl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.core.util.SSRUtil;

public class PNLUtil {
	public static String[] getPaxSSRData(Collection<PaxSSRDetailDTO> ssrDTOs, List<String> skipSsrsCodes) {
		String[] ssrData = new String[2];

		if (ssrDTOs != null) {
			String ssrCode = null;
			StringBuilder ssrText = new StringBuilder(ssrDTOs.size() * 15);

			for (Iterator<PaxSSRDetailDTO> itSSRDTOs = ssrDTOs.iterator(); itSSRDTOs.hasNext();) {
				PaxSSRDetailDTO paxSSRDetailDTO = (PaxSSRDetailDTO) itSSRDTOs.next();
				SSRInfoDTO ssrInfo = SSRUtil.getSSRInfo(paxSSRDetailDTO.getSsrId());

				String nextSSRCode = ssrInfo.getSSRCode();

				// FIXME: nasly SSR.Code.HAJ need to put it to separate category

				if (nextSSRCode != null && isNotSSRHALA(nextSSRCode)) {

					if (skipSsrsCodes != null && skipSsrsCodes.contains(nextSSRCode)) {
						continue;
					}

					if (ssrCode == null) {
						ssrCode = nextSSRCode;
						ssrText.append(BeanUtils.nullHandler(paxSSRDetailDTO.getSsrText()));
					} else {
						ssrText.append(" [");
						ssrText.append(nextSSRCode);
						ssrText.append(" ");
						ssrText.append(BeanUtils.nullHandler(paxSSRDetailDTO.getSsrText()));
						ssrText.append("]");
					}
				}
			}

			ssrData[0] = ssrCode;
			ssrData[1] = ssrText.toString();
		} else {
			ssrData[0] = "";
			ssrData[1] = "";
		}

		return ssrData;
	}

	/**
	 * @author lalanthi
	 * @param ssrCode
	 * @return boolean JIRA : 3051 - Issue 4.1 (HalaServices displayed as SSR)
	 */
	private static boolean isNotSSRHALA(String ssrCode) {
		if (ssrCode.equalsIgnoreCase(SSR.Code.MAAS) || ssrCode.equalsIgnoreCase(SSR.Code.LGBC)
				|| ssrCode.equalsIgnoreCase(SSR.Code.LGFC)) {
			return false;
		} else {
			return true;
		}
	}
}
