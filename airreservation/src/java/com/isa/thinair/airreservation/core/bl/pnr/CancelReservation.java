/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.SeatsForSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.airporttransfer.AirportTransferBL;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.ExternalGenericChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.flexi.FlexiBL;
import com.isa.thinair.airreservation.core.bl.segment.CancelSegmentBO;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.service.PromotionCriteriaAdminBD;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * Command for canceling an existing reservation
 * 
 * Business Rules: (1) Cancellation charges apply for per passenger per fare (ond) only. (2) Infants won't have any
 * cancellation charges (2) After cancellation takes place keep the reservation as cancelled without deleting it. (This
 * is in order to support reporting module)
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cancelReservation"
 */
public class CancelReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CancelReservation.class);

	/** Construct the CancelReservation */
	public CancelReservation() {

	}

	/**
	 * Execute method of the CancelReservation command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String oldPnr = (String) this.getParameter(CommandParamNames.OLD_PNR);
		Boolean isRemovePax = (Boolean) this.getParameter(CommandParamNames.IS_REMOVE_PAX);
		CustomChargesTO customChargesTO = (CustomChargesTO) this.getParameter(CommandParamNames.CUSTOM_CHARGES);
		Boolean isPaxCancel = (Boolean) this.getParameter(CommandParamNames.IS_PAX_CANCEL);
		Boolean isPaxSplit = (Boolean) this.getParameter(CommandParamNames.IS_PAX_SPLIT);
		Boolean isCancelChgOperation = (Boolean) this.getParameter(CommandParamNames.IS_CANCEL_CHG_OPERATION);
		Boolean isModifyChgOperation = (Boolean) this.getParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION);
		Boolean utilizedAllRefundables = (Boolean) this.getParameter(CommandParamNames.UTILIZED_REFUNDABLES);
		Boolean cnxFlownSegs = (Boolean) this.getParameter(CommandParamNames.IS_CNX_FLOWN_SEGS);
		Map removeInfantBCMap = (Map) this.getParameter(CommandParamNames.REMOVE_INFANT_BC_MAP);
		Collection<Integer> removeInfantParentPaxFareIds = (Collection<Integer>) this
				.getParameter(CommandParamNames.REMOVE_INFANT_PARENT_PAX_FARE_IDS);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Boolean isVoidReservation = (Boolean) this.getParameter(CommandParamNames.IS_VOID_OPERATION);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		Integer gdsNotifyAction = (Integer) this.getParameter(CommandParamNames.GDS_NOTIFY_ACTION);

		Boolean isOndRelease = (Boolean) this.getParameter(CommandParamNames.IS_OHD_RELEASE);
		Map<Integer, List<ExternalChgDTO>> paxExternalCharges  = this.getParameter(CommandParamNames.PAX_EXTERNAL_CHARGE_MAP,
				Map.class);				

		// Checking params
		this.validateParams(pnr, isPaxCancel, isCancelChgOperation, isModifyChgOperation, utilizedAllRefundables, version,
				customChargesTO, cnxFlownSegs, credentialsDTO);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);

		// Get the reservation
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Check cancel reservation constraints
		this.checkCancelReservationConstraints(reservation, version, isOndRelease);
		Collection<ReservationSegmentDTO> segmentsBeforeCancellation = reservation.getSegmentsView();
		Boolean isOHDReservation = reservation.getStatus().equals(ReservationStatus.ON_HOLD) ? true : false;

		// Get the flown reservation segment ids
		// Collection<Integer> colFlownPnrSegIds = this.getFlownPnrSegmentIds(reservation, cnxFlownSegs);
		Collection<Integer> colFlownPnrSegIds = null;
		Map<Integer, Collection<Integer>> paxWiseFlownPnrSegIds = CancellationUtils.getPaxWiseFlownPnrSegments(reservation);
		if (paxWiseFlownPnrSegIds.keySet().size() != reservation.getPassengers().size()) {
			throw new ModuleException("airreservation.requote.inconsistent.pax.flown.status");
		}

		// Capture inverse pnr segment ids
		Map<Collection<Integer>, Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds = ReservationApiUtils
				.getInverseSegments(reservation, false);

		// Track Inverse Segment changes
		Map mapPnrSegIdsAndChanges = new HashMap();

		BigDecimal totalCharges = reservation.getTotalChargeAmount();
		BigDecimal totalPayments = reservation.getTotalPaidAmount();

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		// Create the cancel segment business object
		CancelSegmentBO cancelSegmentBO = new CancelSegmentBO(isPaxCancel, customChargesTO, isCancelChgOperation,
				isModifyChgOperation, utilizedAllRefundables, false, credentialsDTO, new HashSet<Integer>(), false, null,
				reservation.isInfantPaymentRecordedWithInfant());

		boolean isAllPassengersAreInfants = true;

		for (ReservationPax pax : reservation.getPassengers()) {
			if (!ReservationApiUtils.isInfantType(pax)) {
				isAllPassengersAreInfants = false;
				break;
			}
		}

		Map<Integer, Collection<ReservationPaxSegmentSSR>> cancelledSSRMap = new HashMap<Integer, Collection<ReservationPaxSegmentSSR>>();

		boolean isPromotionCancelEligible = false;
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
			isPromotionCancelEligible = true;
		}
		
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			// Getting relavant flown segment ids for the pax
			colFlownPnrSegIds = paxWiseFlownPnrSegIds.get(reservationPax.getPnrPaxId());
			// For an adult / child or parent
			if (!ReservationApiUtils.isInfantType(reservationPax)
					|| (reservation.isInfantPaymentRecordedWithInfant() && !isAllPassengersAreInfants)) {
				// Process cancellation per passenger
				this.processCancellation(cancelSegmentBO, reservationPax, reservationPax.getPnrPaxFares(), colFlownPnrSegIds,
						mapSetPSegIdsAndSetInvPSegIds, mapPnrSegIdsAndChanges, isCancelChgOperation, isModifyChgOperation,
						isVoidReservation, chgTnxGen);
			}
			// Infant left over situvation with out having any parent
			// This occurs from Remove infant
			else if (ReservationApiUtils.isInfantType(reservationPax) && reservationPax.getAccompaniedPaxId() == null) {
				this.releaseInfantSeats(cancelSegmentBO, reservationPax, colFlownPnrSegIds);
				ADLNotifyer.updateStatusToCHG(reservation);
			}

			// Cancelling the reservation passenger
			if (ReservationInternalConstants.ReservationPaxStatus.ON_HOLD.equals(reservationPax.getStatus())
					|| colFlownPnrSegIds.size() == 0) {
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CANCEL);
				// isOHDReservation = true;
			}

			Collection<ReservationPaxSegmentSSR> paxCancelledSSRs = ReservationSSRUtil.cancelPaxSSRs(reservationPax, null);
			cancelledSSRMap.put(reservationPax.getPnrPaxId(), paxCancelledSSRs);
		}

		boolean isFlexiApplied = FlexiBL.reflectFlexiBalancesForCancelReservation(reservation, isCancelChgOperation,
				isModifyChgOperation);

		// Setting the infant only booking class map
		cancelSegmentBO.setInfantOnlyBCMap(removeInfantBCMap);
		cancelSegmentBO.setRemoveInfantParentPaxFareIds(removeInfantParentPaxFareIds);

		// Release Block Seats at a once for all passengers
		cancelSegmentBO.releaseInventory();

		// valid only for handling fee on cancel reservation
		this.addExternalCharges(reservation, paxExternalCharges, credentialsDTO);
		
		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		// Cancells the segment states
		this.cancelPnrAndSegmentStates(reservation, colFlownPnrSegIds, credentialsDTO, isVoidReservation);
		
		// Releases the promotion applied to the reservation if applicable
		if (isPromotionCancelEligible) {
			this.releasePromotion(reservation);
		}

		// Saves the Reservation
		ReservationProxy.saveReservation(reservation);

		// records tnx record for handling fee on cancel reservation operation
		this.addTransactionRecords(reservation, paxExternalCharges, credentialsDTO);

		// Trigger ADL if not remove pax scenario (If pax split is true then its its remove pax scenario)
		if (isPaxSplit == null && !isOHDReservation) {
			ADLNotifyer.recordReservation(reservation, AdlActions.D, reservation.getPassengers(), null);
		}

		if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
			// send typeB message
			if (!gdsNotifyAction.equals(Integer.valueOf(GDSInternalCodes.GDSNotifyAction.REMOVE_PAX.getCode()))
					&& reservation.getGdsId() != null) {
				if (ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())) {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(reservation);
					typeBRequestDTO.setGdsNotifyAction(gdsNotifyAction);
					Set<String> csMCCarrier = new HashSet<String>();
					csMCCarrier.add(GDSApiUtils.getGDSCarrierCode(reservation.getGdsId()));
					TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csMCCarrier);
				} else {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(reservation);
					typeBRequestDTO.setGdsNotifyAction(gdsNotifyAction);
					TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
				}
			}
		}

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodesInConfirmedSegments(reservation);
		if (csOCCarrirs != null
				&& !gdsNotifyAction.equals(Integer.valueOf(GDSInternalCodes.GDSNotifyAction.REMOVE_PAX.getCode()))) {
			// send Codeshare typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setGdsNotifyAction(gdsNotifyAction);
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		}

		// cancel e tickets
		ETicketBO.cancelETickets(reservation);

		cancelSegmentBO.propergateTheSavedChargeRevenueForAccounting(reservation);

		// send SSR add notifications
		SSRBL.sendSSRCancelNotifications(reservation, cancelledSSRMap, credentialsDTO);

		Map<Integer, Collection<PaxAirportTransferTO>> cancelledAptMap = AirportTransferBL
				.getpaxSegAptForReservation(reservation);

		if (cancelledAptMap != null && !cancelledAptMap.isEmpty()) {
			if (oldPnr != null && isRemovePax != null && isRemovePax) {
				AirportTransferBL.sendAirportTransferCancelNotifications(reservation, null, cancelledAptMap, credentialsDTO,
						oldPnr);
			} else {
				AirportTransferBL.sendAirportTransferCancelNotifications(reservation, null, cancelledAptMap, credentialsDTO,
						null);
			}
		}

		// UPDATE Reservation Segment Seat,Baggage and Meal Selection Status
		ReservationCoreUtils.updateSegmentsAncillaryStatus(reservation);
		
		// UPDATE Reservation Segment AUTO_CHECKIN Selection Status
		ReservationCoreUtils.updateSegmentsAutomaticCheckinStatus(reservation);
	
		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		ReservationAudit reservationAudit = this.composeAudit(reservation.getPnr(), totalCharges, totalPayments,
				mapPnrSegIdsAndChanges, credentialsDTO, isFlexiApplied, userNotes, userNoteType);
		if (Integer.valueOf(GDSInternalCodes.GDSNotifyAction.ONHOLD_RELEASE.getCode()).equals(gdsNotifyAction)) {
			reservationAudit.setUserId(User.SCHEDULER_USER_ID);
			reservationAudit.setDisplayName(User.SCHEDULER_USER_DISPLAY_NAME);
			credentialsDTO.setDisplayName(User.SCHEDULER_USER_DISPLAY_NAME);
			credentialsDTO.setUserId(User.SCHEDULER_USER_ID);
			response.addResponceParam(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		}
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

//		AppIndicatorEnum appIndicatorEnum = credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null ? credentialsDTO.getTrackInfoDTO().getAppIndicator() : null;
//		PaymentUtil.cancelPreviousOfflineTransaction(reservation.getPnr(), appIndicatorEnum);

		if (customChargesTO.getCustomAdultCCharge() != null || customChargesTO.getCustomChildCCharge() != null) {
			ReservationAudit reservavtionAudit = new ReservationAudit();
			reservavtionAudit.setPnr(pnr);
			if (customChargesTO.getCustomAdultCCharge() != null)
				reservavtionAudit.addContentMap("adultCnxCharge", customChargesTO.getCustomAdultCCharge().toString());
			else
				reservavtionAudit.addContentMap("adultCnxCharge", "-");
			if (customChargesTO.getCustomChildCCharge() != null)
				reservavtionAudit.addContentMap("childCnxCharge", customChargesTO.getCustomChildCCharge().toString());
			else
				reservavtionAudit.addContentMap("childCnxCharge", "-");
			if (customChargesTO.getCustomInfantCCharge() != null)
				reservavtionAudit.addContentMap("infantCnxCharge", customChargesTO.getCustomInfantCCharge().toString());
			else
				reservavtionAudit.addContentMap("infantCnxCharge", "-");
			reservavtionAudit.setModificationType(AuditTemplateEnum.OVERRIDE_CNX_CHARGE.getCode());
			colReservationAudit.add(reservavtionAudit);
		}

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.CANCEL_RESERVATION_SEGMENTS, segmentsBeforeCancellation);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Returns the flown pnr segment ids
	 * 
	 * @param reservation
	 * @param cnxFlownSegs
	 * @return
	 */
	private Collection<Integer> getFlownPnrSegmentIds(Reservation reservation, boolean cnxFlownSegs) {
		Collection<Integer> colFlownPnrSegmentIds = new ArrayList<Integer>();

		if (!cnxFlownSegs) {
			Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
			ReservationSegmentDTO reservationSegmentDTO;
			Date currentDate = new Date();

			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = itReservationSegmentDTO.next();

				// If segment is confirmed
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())) {

					// Capture flown pnr segment ids
					if (currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) > 0) {
						colFlownPnrSegmentIds.add(reservationSegmentDTO.getPnrSegId());
					}
				}
			}
		}

		return colFlownPnrSegmentIds;
	}

	/**
	 * Release infant seats
	 * 
	 * @param cancelSegmentBO
	 * @param reservationPax
	 * @param colFlownPnrSegIds
	 */
	private void releaseInfantSeats(CancelSegmentBO cancelSegmentBO, ReservationPax reservationPax,
			Collection<Integer> colFlownPnrSegIds) {

		ReservationPaxFareSegment reservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		SeatsForSegmentDTO seatsForSegmentDTO;

		Iterator<ReservationPaxFare> itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
		boolean isOnHoldSeat = reservationPax.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD);

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			// Get the confirmed segments
			reservationPaxFare = this.isConfirmedSegments(reservationPaxFare, colFlownPnrSegIds, reservationPax.getStatus());

			if (reservationPaxFare != null) {
				for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
					reservationPaxFareSegment = reservationPaxFareSegment2;

					boolean isWaitListedSeat = ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING
							.equals(reservationPaxFareSegment.getSegment().getStatus());

					seatsForSegmentDTO = new SeatsForSegmentDTO(reservationPaxFare.getPnrPaxFareId().intValue(),
							reservationPaxFareSegment.getPnrPaxFareSegId().intValue(),
							reservationPaxFareSegment.getSegment().getFlightSegId().intValue(),
							reservationPaxFareSegment.getBookingCode(), 0, 1, isOnHoldSeat, isWaitListedSeat);
					cancelSegmentBO.addInfantOnlySeatsForSegmentDTO(seatsForSegmentDTO);
				}
			}
		}
	}

	/**
	 * Process cancellation
	 * 
	 * @param cancelSegmentBO
	 * @param reservationPax
	 * @param pnrPaxFares
	 * @param colFlownPnrSegIds
	 * @param mapSetPSegIdsAndSetInvPSegIds
	 * @param mapPnrSegIdsAndChanges
	 * @param isCancelChgOperation
	 * @param isModifyChgOperation
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	private void processCancellation(CancelSegmentBO cancelSegmentBO, ReservationPax reservationPax,
			Set<ReservationPaxFare> pnrPaxFares, Collection<Integer> colFlownPnrSegIds,
			Map<Collection<Integer>, Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds,
			Map<Collection<Integer>, String> mapPnrSegIdsAndChanges, Boolean isCancelChgOperation, Boolean isModifyChgOperation,
			boolean isVoidReservation, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		Iterator<ReservationPaxFare> itReservationPaxFare = pnrPaxFares.iterator();
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFare reservationPaxFareEffected;
		Collection<ReservationPaxFare> colReservationPaxFare = new ArrayList<ReservationPaxFare>();
		Collection<Integer> colProcessedResPaxFareId = new ArrayList<Integer>();
		Collection<Integer> colSetInversePnrSegIds;
		Collection<Integer> colSetPnrSegIds;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			// Get confirmed segments
			reservationPaxFare = this.isConfirmedSegments(reservationPaxFare, colFlownPnrSegIds, reservationPax.getStatus());

			// If the ond is confirm ond
			if (reservationPaxFare != null) {
				colReservationPaxFare.add(reservationPaxFare);

				colSetPnrSegIds = ReservationCoreUtils.getPnrSegIds(reservationPaxFare);
				colSetInversePnrSegIds = ReservationApiUtils.getInversePnrSegIds(mapSetPSegIdsAndSetInvPSegIds, colSetPnrSegIds);

				// This means effected reservation pax fare exist
				if (colSetInversePnrSegIds.size() > 0) {
					reservationPaxFareEffected = ReservationCoreUtils.getPnrPaxFare(reservationPax.getPnrPaxFares(),
							colSetInversePnrSegIds);

					if ((!colProcessedResPaxFareId.contains(reservationPaxFare.getPnrPaxFareId())
							&& (!colProcessedResPaxFareId.contains(reservationPaxFareEffected.getPnrPaxFareId())))) {
						// Adjust fare among ond(s)
						CancellationUtils.adjustFareAmongOnds(reservationPaxFare, reservationPaxFareEffected,
								mapPnrSegIdsAndChanges, colSetPnrSegIds, colSetInversePnrSegIds, false);
						colProcessedResPaxFareId.add(reservationPaxFare.getPnrPaxFareId());
						colProcessedResPaxFareId.add(reservationPaxFareEffected.getPnrPaxFareId());
					}
				}
			}
		}

		// Process credit for onds
		if (colReservationPaxFare.size() > 0) {
			CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
			cancelSegmentBO.setTrackInfoDTO(credentialsDTO.getTrackInfoDTO());
			cancelSegmentBO.setUserPrincipal((UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL));
			cancelSegmentBO.processPaxCreditForOnds(reservationPax, colReservationPaxFare, isCancelChgOperation,
					isModifyChgOperation, isVoidReservation, false, true, chgTnxGen);
		}
	}

	/**
	 * Return confirmed segments
	 * 
	 * @param reservationPaxFare
	 * @param colFlownPnrSegIds
	 * @param paxStatus
	 */
	private ReservationPaxFare isConfirmedSegments(ReservationPaxFare reservationPaxFare, Collection<Integer> colFlownPnrSegIds,
			String paxStatus) {
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegments = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;
		int i = 0;

		while (itReservationPaxFareSegments.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegments.next();

			// Checking to see if the segment is confirmed
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
					.equals(reservationPaxFareSegment.getSegment().getStatus())
					|| ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING
							.equals(reservationPaxFareSegment.getSegment().getStatus())) {
				// Checking to see if the segment is not flown
				if (ReservationInternalConstants.ReservationPaxStatus.ON_HOLD.equals(paxStatus)
						|| !colFlownPnrSegIds.contains(reservationPaxFareSegment.getPnrSegId())) {
					i++;
				}
			}
		}

		if (reservationPaxFare.getPaxFareSegments().size() == i) {
			return reservationPaxFare;
		} else {
			return null;
		}
	}

	/**
	 * Check cancel reservation constraints
	 * 
	 * @param reservation
	 * @param version
	 * @param isOndRelease
	 * @throws ModuleException
	 */
	private void checkCancelReservationConstraints(Reservation reservation, Long version, Boolean isOndRelease)
			throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		// Check to see if the reservation is already cancelled
		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			throw new ModuleException("airreservations.arg.invalidPnrCancel");
		}

		if (isOndRelease != null
				&& (isOndRelease && !ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus()))) {
			throw new ModuleException("airreservations.arg.invalidOnholdCancel");
		}

		// Checking any restricted fare exist or not
		ValidationUtils.checkRestrictedSegmentConstraints(reservation, null,
				"airreservations.cancellation.restrictedFareSegment");
	}

	/**
	 * Cancells the pnr and segment states
	 * 
	 * @param reservation
	 * @param colFlownPnrSegIds
	 * @param credentialsDTO
	 * @param isVoidReservation
	 * @throws ModuleException
	 */
	private void cancelPnrAndSegmentStates(Reservation reservation, Collection<Integer> colFlownPnrSegIds,
			CredentialsDTO credentialsDTO, boolean isVoidReservation) throws ModuleException {

		boolean isOnholdReservation = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus());
		// Cancels the reservation
		if (isOnholdReservation || colFlownPnrSegIds.size() == 0) {
			if (isVoidReservation) {
				// only reservations are to be marked as void.
				reservation.setIsVoidReservation(ReservationInternalConstants.VoidReservation.YES);
			} else {
				reservation.setIsVoidReservation(ReservationInternalConstants.VoidReservation.NO);
			}

			reservation.setStatus(ReservationInternalConstants.ReservationStatus.CANCEL);
		}

		// Cancel the segments
		Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator();
		ReservationSegment reservationSegment;

		while (itReservationSegment.hasNext()) {
			reservationSegment = itReservationSegment.next();
			// If it's not flown pnr segment then cancel it.
			if (!colFlownPnrSegIds.contains(reservationSegment.getPnrSegId()) || isOnholdReservation) {
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
					reservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
					ReservationApiUtils.captureSegmentStatusChgInfo(reservationSegment, credentialsDTO);
				}
			}
		}
	}

	/**
	 * Compose Modification
	 * 
	 * @param pnr
	 * @param totalCharges
	 * @param totalPayments
	 * @param credentialsDTO
	 * @param userNoteType TODO
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ReservationAudit composeAudit(String pnr, BigDecimal totalCharges, BigDecimal totalPayments,
			Map mapPnrSegIdsAndChanges, CredentialsDTO credentialsDTO, boolean isFlexiApplied, String userNotes, String userNoteType) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CANCELED_RESERVATION.getCode());
		reservationAudit.setUserNote(userNotes);
		reservationAudit.setUserNoteType(userNoteType);

		// If any inverse segment changes exist
		if (mapPnrSegIdsAndChanges.size() > 0) {
			StringBuffer cancelInformation = new StringBuffer();
			Collection colPnrSegIds;
			String segmentInfo;
			String segmentChanges;

			for (Iterator itPnrSegIds = mapPnrSegIdsAndChanges.keySet().iterator(); itPnrSegIds.hasNext();) {
				colPnrSegIds = (Collection) itPnrSegIds.next();
				segmentInfo = ReservationCoreUtils.getSegmentInformation(colPnrSegIds);
				segmentChanges = (String) mapPnrSegIdsAndChanges.get(colPnrSegIds);
				cancelInformation.append(segmentInfo).append(segmentChanges);
			}

			// Setting the new pnr information
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledReservation.CANCEL_INFO,
					cancelInformation.toString());
		}

		// Setting the new pnr information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledReservation.TOTAL_CHARGES,
				String.valueOf(totalCharges));
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledReservation.TOTAL_PAYMENTS,
				String.valueOf(totalPayments));

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Flexibilities.FLEXIBILITIES_INFO,
				(isFlexiApplied) ? "Utilized Flexibility" : "");

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledReservation.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledReservation.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Validate parameters
	 * 
	 * @param pnr
	 * @param isPaxCancel
	 * @param isCancelChgOperation
	 * @param isModifyChgOperation
	 * @param utilizeRefundables
	 * @param version
	 * @param customChargesTO
	 * @param cnxFlownSegs
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Boolean isPaxCancel, Boolean isCancelChgOperation, Boolean isModifyChgOperation,
			Boolean utilizeRefundables, Long version, CustomChargesTO customChargesTO, Boolean cnxFlownSegs,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || isPaxCancel == null || isCancelChgOperation == null || isModifyChgOperation == null
				|| utilizeRefundables == null || version == null || customChargesTO == null || cnxFlownSegs == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
	
	private void addExternalCharges(Reservation reservation, Map<Integer, List<ExternalChgDTO>> paxExternalCharges,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
		ExternalGenericChargesBL.reflectExternalChgForBookingCancellation(reservation, paxExternalCharges, credentialsDTO,
				chgTnxGen);
	}

	private void addTransactionRecords(Reservation reservation, Map<Integer, List<ExternalChgDTO>> paxExternalCharges,
			CredentialsDTO credentialsDTO) throws ModuleException {
		if (reservation.getPassengers() != null && !reservation.getPassengers().isEmpty() && paxExternalCharges != null
				&& !paxExternalCharges.isEmpty()) {
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				Integer pnrPaxId = reservationPax.getPnrPaxId();
				if (paxExternalCharges.containsKey(reservationPax.getPaxSequence())) {
					Collection<ExternalChgDTO> ccExternalChgs = (List<ExternalChgDTO>) paxExternalCharges
							.get(reservationPax.getPaxSequence());

					BigDecimal totalChargeAmount = getTotalExternalCharges(ccExternalChgs);

					if (totalChargeAmount.doubleValue() > 0) {

						ReservationModuleUtils.getRevenueAccountBD().recordHandlingFeeOnCancellation(pnrPaxId.toString(),
								totalChargeAmount, credentialsDTO);

					}

				}
			}
		}
	}

	private BigDecimal getTotalExternalCharges(Collection<ExternalChgDTO> ccExternalChgs) {
		BigDecimal totalChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (ccExternalChgs != null && !ccExternalChgs.isEmpty()) {
			for (ExternalChgDTO chg : ccExternalChgs) {
				totalChargeAmount = AccelAeroCalculator.add(totalChargeAmount, chg.getAmount());
			}
		}
		return totalChargeAmount;
	}

	/**
	 * Cancels the promotion applied to the reservation
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private void releasePromotion(Reservation reservation) throws ModuleException {
		try {
			if (reservation.getPromotionId() != null) {
				PromotionCriteriaAdminBD promoCriteriaAdminBD = ReservationModuleUtils.getPromotionCriteriaAdminBD();
				PromotionCriteriaTO promoCriteria = promoCriteriaAdminBD.findPromotionCriteria(reservation.getPromotionId());
				if (null != promoCriteria && promoCriteria.getSystemGenerated()) {
					if (promoCriteria.getPromoCodeEnabled()
							&& PromotionCriteriaConstants.PromotionStatus.INACTIVE.equals(promoCriteria.getStatus())) {
						String endorsementNotes = "PNRCancellationNote: Promotion Removed";
						appendEndorsementNotes(reservation, endorsementNotes);
						promoCriteriaAdminBD
								.updateSystemGeneratedPromotionforOHD(reservation.getPromotionId(),
										AccelAeroCalculator.getDefaultBigDecimalZero(),
										PromotionCriteriaConstants.PromotionStatus.ACTIVE);
					}
				}
			}
		} catch (Exception ex) {
			log.error("Error in cancelling promotions applied to ON-HOLD reservation");
		}

	}

	private void appendEndorsementNotes(Reservation reservation, String endorsementNotes) {
		if (BeanUtils.nullHandler(endorsementNotes).length() > 0) {
			String newNote = endorsementNotes;
			if (BeanUtils.nullHandler(reservation.getEndorsementNote()).length() > 0) {
				newNote = reservation.getEndorsementNote() + " " + endorsementNotes;
			}
			reservation.setEndorsementNote(newNote);
		}
	}

}