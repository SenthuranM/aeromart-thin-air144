package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.factory;

import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.ResChangesDetectorConstants;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers.PalCalMessageObserver;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers.base.AirportMessageObserver;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers.base.BaseResModifyObserver;

public class ResModifyIdentificationObserverFactory {

	private ResModifyIdentificationObserverFactory() {

	}

	public static BaseResModifyObserver<ResModifyDtectorDataContext> getResModifyIdentificationObserver(String observerRef) {

		BaseResModifyObserver<ResModifyDtectorDataContext> observer = null;

		if (ResChangesDetectorConstants.ObserverRef.PALCAL_MESSAGE_NOTIFIER_OBSERVER.equals(observerRef)) {

			observer = new PalCalMessageObserver(observerRef);

		}

		return observer;
	}
}
