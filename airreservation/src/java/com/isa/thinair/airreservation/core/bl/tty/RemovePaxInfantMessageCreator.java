package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 	
 */
public class RemovePaxInfantMessageCreator extends TypeBReservationMessageCreator {
	
	public RemovePaxInfantMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForCancelAllSegments();
		passengerNamesComposingStrategy = new PassengerNamesComposerForRemovePax();
	}

	@Override
	public List<OSIDTO> addOSIDetails(Reservation reservation, TypeBRequestDTO typeBRequestDTO) {
		List<OSIDTO> osiDTOs = new ArrayList<OSIDTO>();
		OSIDTO osiDTO = new OSIDTO();
		osiDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		int totalPaxCount = TTYMessageCreatorUtil.getTotalPaxCount(reservation);
		osiDTO.setCodeOSI(GDSExternalCodes.OSICodes.REMAINING_PASSENGERS.getCode() + totalPaxCount);
		osiDTOs.add(osiDTO);
		return osiDTOs;
	}
}
