package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author Jagath
 * 
 * @isa.module.command name="calculateNameChangeCharge"
 */
public class CalculateNameChangeCharge extends DefaultBaseCommand {

	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges = this
				.getParameter(CommandParamNames.EXCHG_REPROTECTED_EXTERNAL_CHARGES, Map.class);
		Collection<OndFareDTO> ondFareDTOs = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);
		Map<Integer, IPayment> pnrPaxIdAndPayments = this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, Map.class);
		// Map<String, String> requoteSegmentMap = (Map<String, String>)
		// this.getParameter(CommandParamNames.REQUOTE_SEGMENT_MAP);
		Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExtChgMap = this
				.getParameter(CommandParamNames.PAX_TAXING_EXT_CHARGE_MAP, Map.class);
		Map<Integer, NameDTO> changedPaxNamesMap = (HashMap<Integer, NameDTO>) this
				.getParameter(CommandParamNames.NAME_CHANGE_PAX_MAP);
		Collection<PnrChargesDTO> pnrPaxCharges = (ArrayList<PnrChargesDTO>) this.getParameter(CommandParamNames.PNR_PAX_CHARGES);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		IExtraFeeCalculator extraFeeCalculator = new NameChangeChargeCalculator(pnr, reprotectedExternalCharges,
				changedPaxNamesMap, pnrPaxIdAndPayments, credentialsDTO);
		extraFeeCalculator.calculate();

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.EXCHG_REPROTECTED_EXTERNAL_CHARGES, reprotectedExternalCharges);
		response.addResponceParam(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, pnrPaxIdAndPayments);

		return response;
	}

}
