package com.isa.thinair.airreservation.core.bl.tty;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class RemoveInfantMessageCreator extends TypeBReservationMessageCreator {
	
	public RemoveInfantMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesComposerForRemoveInfant();
	}
}
