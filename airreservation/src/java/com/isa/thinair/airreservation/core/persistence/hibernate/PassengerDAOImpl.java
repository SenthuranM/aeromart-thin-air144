/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airreservation.api.dto.ConnectionPaxInfoTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.AutoCancellationSchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPassengerDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * PassengerDAOImpl is the business DAO hibernate implementation
 * 
 * @author Nilindra Fernando
 * @author Isuru
 * @since 1.0
 * @isa.module.dao-impl dao-name="PassengerDAO"
 */
public class PassengerDAOImpl extends PlatformHibernateDaoSupport implements PassengerDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PassengerDAOImpl.class);

	/**
	 * Returns a Passenger
	 * 
	 * @param pnrPaxId
	 * @param loadFares
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public ReservationPax getPassenger(int pnrPaxId, boolean loadFares, boolean loadAdminInfo, boolean loadInfantPaymentFlag) throws CommonsDataAccessException {
		log.debug("Inside getPassenger");

		ReservationPax reservationPax = (ReservationPax) get(ReservationPax.class, new Integer(pnrPaxId));

		if (reservationPax == null) {
			throw new CommonsDataAccessException("airreservations.arg.paxNoLongerExist");
		} else {
			// Force lazy loading...
			// It will trigger when we perform some meaningful action like
			// size, isEmpty, iterator etc
			if (loadFares) {
				reservationPax.getPnrPaxFares().isEmpty();
			}
			if (loadAdminInfo) {
				reservationPax.getReservation().getAdminInfo().getOriginChannelId();
			}
			
			if(loadInfantPaymentFlag){
				reservationPax.getReservation().isInfantPaymentRecordedWithInfant();
			}
			
		}

		log.debug("Exit getPassenger");
		return reservationPax;
	}

	public Integer getPnrPaxId(String pnr, Integer paxSequence) {
		if (log.isDebugEnabled())
			log.debug("Inside getPnrPaxId");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " SELECT p.PNR_PAX_ID PAX_ID " + " FROM T_PNR_PASSENGER p WHERE p.PNR = ? AND p.PAX_SEQUENCE = ? ";

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" SQL to excute            : " + sql);
			log.debug("############################################");
		}

		Object[] object = new Object[] { pnr, paxSequence };
		Integer pnrPaxId = (Integer) jt.query(sql, object, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null && rs.next()) {
					return BeanUtils.parseInteger(rs.getInt("PAX_ID"));
				}
				return null;
			}
		});

		if (log.isDebugEnabled())
			log.debug("Exit getPnrPaxId");
		return pnrPaxId;
	}

	/**
	 * Return reservation passengers
	 * 
	 * @param pnrStatus
	 *            reservation status
	 * @param toDate
	 *            release date end on
	 * @return collection of pnr of type String
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Long> getReservationPassengers(String pnrStatus, Date toDate) {
		log.debug("Inside getReservationPassengers");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = " SELECT DISTINCT r.PNR PNR, r.VERSION VERSION FROM T_RESERVATION r, T_PNR_PASSENGER p "
				+ " WHERE r.PNR = p.PNR AND r.STATUS = ? AND p.RELEASE_TIMESTAMP <= ? AND r.ORIGIN_CHANNEL_CODE <> ?  AND r.DUMMY_BOOKING = ? ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL Parameters           : " + pnrStatus + " " + toDate);
		log.debug("############################################");

		Object[] object = new Object[] { pnrStatus, BeanUtils.getTimestamp(toDate),
				ReservationInternalConstants.SalesChannel.LCC, String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		Map<String, Long> mapPnrs = (Map<String, Long>) jt.query(sql, object, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Long> mapPnrs = new HashMap<String, Long>();

				if (rs != null) {
					while (rs.next()) {
						mapPnrs.put(BeanUtils.nullHandler(rs.getString("PNR")), new Long(rs.getLong("VERSION")));
					}
				}

				return mapPnrs;
			}
		});

		log.debug("Exit getReservationPassengers");
		return mapPnrs;
	}

	/**
	 * Returns marketing perspective lcc reservation(s)
	 * 
	 * @param pnrStatus
	 * @param toDate
	 * @param carrierCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getMarketingLccReservations(String pnrStatus, Date toDate, String carrierCode) {
		log.debug("Inside getMarketingLccReservations");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = " SELECT DISTINCT r.ORIGINATOR_PNR ORIGINATOR_PNR FROM T_RESERVATION r, T_PNR_PASSENGER p "
				+ " WHERE r.PNR = p.PNR AND r.STATUS = ? AND p.RELEASE_TIMESTAMP <= ? AND r.ORIGIN_CHANNEL_CODE = ? AND r.ORIGIN_CARRIER_CODE = ?  AND r.DUMMY_BOOKING = ? "
				+ " UNION "
				+ " SELECT DISTINCT r.PNR ORIGINATOR_PNR FROM T_PNR_PASSENGER p, T_RESERVATION r LEFT JOIN T_EXT_PNR_SEGMENT ps ON r.PNR = ps.PNR  "
				+ " WHERE r.PNR = p.PNR AND r.STATUS = ? AND p.RELEASE_TIMESTAMP <= ? AND r.ORIGIN_CHANNEL_CODE = ? AND r.DUMMY_BOOKING = ? AND (ps.PNR is null or ps.STATUS = ? )";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL Parameters           : " + pnrStatus + " " + toDate);
		log.debug("############################################");

		Object[] object = new Object[] { pnrStatus, BeanUtils.getTimestamp(toDate),
				ReservationInternalConstants.SalesChannel.LCC, carrierCode,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO), pnrStatus, BeanUtils.getTimestamp(toDate),
				ReservationInternalConstants.SalesChannel.LCC, String.valueOf(ReservationInternalConstants.DummyBooking.NO),
				ReservationInternalConstants.ReservationStatus.CANCEL };

		Collection<String> pnrs = (Collection<String>) jt.query(sql, object, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> pnrs = new HashSet<String>();
				String groupPNR;

				if (rs != null) {
					while (rs.next()) {
						groupPNR = BeanUtils.nullHandler(rs.getString("ORIGINATOR_PNR"));

						if (groupPNR.length() > 0) {
							pnrs.add(groupPNR);
						}
					}
				}

				return pnrs;
			}
		});

		log.debug("Exit getMarketingLccReservations");
		return pnrs;
	}

	/**
	 * Return Passenger Titles
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getPassengerTitles() {
		log.debug("Inside getPassengerTitles");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = " SELECT TITLE_CODE, TITLE_DESCRIPTION, REG_VISIBILITY FROM T_PAX_TITLE ORDER BY TITLE_CODE ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Map<String, String> mapPaxTitles = (Map<String, String>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, String> mapPaxTitles = new HashMap<String, String>();

				if (rs != null) {
					while (rs.next()) {
						mapPaxTitles.put(BeanUtils.nullHandler(rs.getString("TITLE_CODE")),
								BeanUtils.nullHandler(rs.getString("TITLE_DESCRIPTION")));
					}
				}

				return mapPaxTitles;
			}
		});

		log.debug("Exit getPassengerTitles");
		return mapPaxTitles;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AutoCancellationSchDTO> getExpiredInfants(Collection<Integer> cancellationIds, String marketingCarrier) {
		boolean isDry = AppSysParamsUtil.getDefaultCarrierCode().equals(marketingCarrier) ? false : true;

		StringBuilder query = new StringBuilder();
		query.append("SELECT r.pnr, pp.pnr_pax_id, r.version, pp.auto_cancellation_id FROM t_reservation r, t_pnr_passenger pp WHERE ");
		query.append("r.pnr=pp.pnr AND r.status = ? AND pp.status= ? AND r.DUMMY_BOOKING = ? AND pp.pax_type_code=? AND ");
		query.append(Util.getReplaceStringForIN("pp.auto_cancellation_id", cancellationIds));
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.ORIGIN_CHANNEL_CODE ");
		if (isDry) {
			query.append("= ?");
		} else {
			query.append("<> ?");
		}

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO), ReservationInternalConstants.PassengerType.INFANT,
				marketingCarrier, ReservationInternalConstants.SalesChannel.LCC };

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (List<AutoCancellationSchDTO>) jt.query(query.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AutoCancellationSchDTO> autoCnxDtos = new ArrayList<AutoCancellationSchDTO>();
				if (rs != null) {
					Map<ReservationLiteDTO, List<Integer>> pnrWiseInfants = new HashMap<ReservationLiteDTO, List<Integer>>();
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						long version = rs.getLong("version");
						Integer autoCancellationId = rs.getInt("auto_cancellation_id");

						ReservationLiteDTO resDTO = new ReservationLiteDTO(pnr, version, autoCancellationId);
						if (!pnrWiseInfants.containsKey(resDTO)) {
							pnrWiseInfants.put(resDTO, new ArrayList<Integer>());
						}

						pnrWiseInfants.get(resDTO).add(rs.getInt("pnr_pax_id"));

					}

					for (Entry<ReservationLiteDTO, List<Integer>> resPaxEntry : pnrWiseInfants.entrySet()) {
						ReservationLiteDTO resDTO = resPaxEntry.getKey();
						AutoCancellationSchDTO autoCancellationSchDTO = new AutoCancellationSchDTO();
						autoCancellationSchDTO.setAutoCancellationId(resDTO.getAutoCancellationId());
						autoCancellationSchDTO.setPnr(resDTO.getPnr());
						autoCancellationSchDTO.setVersion(resDTO.getVersion());
						autoCancellationSchDTO.setCancellingPaxIds(resPaxEntry.getValue());
						autoCnxDtos.add(autoCancellationSchDTO);
					}

				}

				return autoCnxDtos;
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Collection<String>> getNoShowPPDIDsForPax(String pnrPaxId) {
		String sql = "SELECT PPFS.*,SEG.OND_GROUP_ID FROM T_PNR_PAX_FARE_SEGMENT PPFS, T_PNR_PAX_FARE PPF, T_PNR_SEGMENT SEG "
				+ "WHERE PPFS.PAX_STATUS = 'N' "
				+ "AND PPF.PNR_PAX_ID = ? AND PPFS.PNR_SEG_ID=SEG.PNR_SEG_ID AND PPFS.PPF_ID IN(PPF.PPF_ID)";
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Map<String, Collection<String>>) jt.query(sql, new String[] { pnrPaxId }, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Collection<String>> resultMap = new HashMap<String, Collection<String>>();
				List<Integer> ondGroupIDs = new ArrayList<Integer>();
				while (rs.next()) {
					// For connection flights with many segments should only take one no show segment only.
					Integer tmpOndGroup = rs.getInt("OND_GROUP_ID");
					if (!ondGroupIDs.contains(tmpOndGroup)) {
						String pnrSegID = rs.getString("PNR_SEG_ID");
						if (resultMap.get(pnrSegID) == null) {
							resultMap.put(pnrSegID, new HashSet<String>());
						}
						resultMap.get(pnrSegID).add(rs.getString("PPF_ID"));
					}
					ondGroupIDs.add(tmpOndGroup);
				}

				return resultMap;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getExpiredLccInfantsInfo(String marketingCarrier, Collection<Integer> cancellationIds) {
		String query = "SELECT DISTINCT r.pnr, pp.auto_cancellation_id FROM t_reservation r, t_pnr_passenger pp WHERE "
				+ "r.pnr=pp.pnr AND r.status = ? AND pp.status= ? AND r.ORIGIN_CHANNEL_CODE = ? AND r.ORIGIN_CARRIER_CODE = ? "
				+ "AND r.DUMMY_BOOKING = ? AND pp.pax_type_code=? AND pp.auto_cancellation_id IN ("
				+ BeanUtils.constructINStringForInts(cancellationIds) + ")";

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED, ReservationInternalConstants.SalesChannel.LCC,
				marketingCarrier, String.valueOf(ReservationInternalConstants.DummyBooking.NO),
				ReservationInternalConstants.PassengerType.INFANT };

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Map<String, Integer>) jt.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> autoCnxPNRs = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						Integer autoCnxId = rs.getInt("auto_cancellation_id");
						autoCnxPNRs.put(pnr, autoCnxId);
					}
				}

				return autoCnxPNRs;
			}
		});
	}

	@Override
	public void savePassengers(Collection<ReservationPax> passengers) {
		log.debug("Inside save passengers");
		// Saves reservation passengers
		hibernateSaveOrUpdateAll(passengers);
		log.debug("Exit save passengers");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PNRGOVPassengerDTO> getPassengersForPNR(String pnr) {
		StringBuffer query = new StringBuffer();
		query.append("SELECT pp.pnr_pax_id  AS pnr_pax_id,");
		query.append("pp.title            AS title,");
		query.append("pp.upper_first_name AS first_name,");
		query.append("pp.upper_last_name  AS last_name,");
		query.append("pp.pax_type_code    AS passenger_type,");
		query.append("pp.adult_id         AS adult_id, ");
		query.append("pp.date_of_birth    AS date_of_birth,");
		query.append("pp.pax_sequence     AS pax_sequence,");
		query.append("ppai.passport_number AS passport_number,");
		query.append("ppai.passport_expiry AS passport_expiry,");
		query.append("ppai.passport_issued_cntry AS pp_issue_country, ");
		query.append("ppai.place_of_birth AS place_of_birth, ");
		query.append("ppai.travel_doc_type AS travel_document_type, ");
		query.append("ppai.visa_doc_number AS visa_document_number, ");
		query.append("ppai.visa_doc_place_of_issue AS visa_place_of_issue, ");
		query.append("ppai.visa_doc_issue_date AS visa_date_of_issue, ");
		query.append("ppai.visa_applicable_country AS visa_applicable_country, ");
		query.append("tn.iso_code AS Nationality_CODE ");
		query.append("FROM t_pnr_pax_additional_info ppai, t_pnr_passenger pp ");
		query.append(" LEFT JOIN t_nationality tn on pp.nationality_code=tn.nationality_code ");
		query.append("WHERE pp.pnr ='"+pnr+"' ");
		query.append("AND pp.status='CNF' ");
		query.append("AND pp.pnr_pax_id=ppai.pnr_pax_id ");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Object[] param = {};

		return (List<PNRGOVPassengerDTO>) jt.query(query.toString(), param, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PNRGOVPassengerDTO> passengerListForPnr = new ArrayList<PNRGOVPassengerDTO>();
				if (rs != null) {
					while (rs.next()) {
						PNRGOVPassengerDTO passenger = new PNRGOVPassengerDTO();
						passenger.setPnrPaxId(rs.getInt("pnr_pax_id"));
						passenger.setTitle(BeanUtils.nullHandler(rs.getString("title")));
						String firstName = BeanUtils.nullHandler(rs.getString("first_name"));
						String lastName = BeanUtils.nullHandler(rs.getString("last_name"));
						String upperFName = BeanUtils.removeExtraChars(firstName.toUpperCase());
						String upperLName = BeanUtils.removeExtraChars(lastName.toUpperCase());
						String tmpFirstName = BeanUtils.removeSpaces(upperFName);
						String tmpLastName = BeanUtils.removeSpaces(upperLName);
						if (tmpFirstName.equals("TBA") || tmpLastName.equals("TBA")) {
							continue;
						}
						passenger.setFirstName(upperFName);
						passenger.setLastName(upperLName);
						passenger.setPassengerType(rs.getString("passenger_type"));
						passenger.setAdultID(rs.getInt("adult_id"));
						passenger.setDateOfBirth(rs.getDate("date_of_birth"));
						passenger.setPaxReference(rs.getInt("pax_sequence"));
						passenger.setPassportNumber(BeanUtils.nullHandler(rs.getString("passport_number")));
						passenger.setPassportExpiry(rs.getDate("passport_expiry"));
						passenger.setPassportIssueCountry(BeanUtils.nullHandler(rs.getString("pp_issue_country")));
						passenger.setPlaceOfBirth(BeanUtils.nullHandler(rs.getString("place_of_birth")));
						passenger.setTravelDocumentType(rs.getString("travel_document_type"));
						passenger.setVisaDocumentNumber(BeanUtils.nullHandler(rs.getString("visa_document_number")));
						passenger.setVisaPlaceOfIssue(BeanUtils.nullHandler(rs.getString("visa_place_of_issue")));
						passenger.setVisaDateOfIssue(rs.getDate("visa_date_of_issue"));
						passenger.setVisaApplicableCountry(BeanUtils.nullHandler(rs.getString("visa_applicable_country")));
						passenger.setPassengerNationality(BeanUtils.nullHandler(rs.getString("Nationality_CODE")));
						passengerListForPnr.add(passenger);

					}
				}
				return passengerListForPnr;
			}

		});

	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getInventoryDataForMismatchTracking(String pnr) {

		StringBuffer query = new StringBuffer();
		 query.append("SELECT fs.flight_id AS fltId,");
				 query.append("fs.segment_code   AS segmentCode,");
				 query.append("ppfs.booking_code AS bookingCode");
				 query.append(" FROM t_pnr_segment ps,");
				 query.append("t_pnr_pax_fare_segment ppfs,");
				 query.append("t_flight_segment fs");
				 query.append(" WHERE ps.pnr = '"+ pnr +"'");
				 query.append(" AND ppfs.pnr_seg_id = ps.pnr_seg_id");
				 query.append(" AND fs.flt_seg_id   = ps.flt_seg_id");
				 query.append(" AND ppfs.booking_code IS NOT NULL");
				 query.append(" GROUP BY fs.flight_id,");
				 query.append(" fs.flt_seg_id,");
				 query.append(" fs.segment_code ,");
				 query.append(" ppfs.booking_code"); 
				 
		JdbcTemplate jt = new JdbcTemplate(
				ReservationModuleUtils.getDatasource());
		Object[] param = {};

		return (List<Map<String, String>>) jt.query(query.toString(), param,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<Map<String, String>> result = new ArrayList<Map<String, String>>();
						if (rs != null) {
							while (rs.next()) {
								Map<String, String> data = new LinkedHashMap<String, String>();							
								  data.put("fltId", rs.getString("fltId"));
								  data.put("segmentCode", rs.getString("segmentCode"));
								  data.put("bookingCode", rs.getString("bookingCode"));								  													
								  result.add(data);
							}
						}
						return result;
					}

				});
	}
	
	public Boolean hasMismatchOccured(int flightID, String segmentCode, String bookingCode){
		
		 StringBuffer query = new StringBuffer();
		 query.append("SELECT (res.ADULT_ONHOLD_QTY-inv.onhold_seats) as diff");
		 query.append(" FROM");
		 query.append("  (SELECT COUNT(DISTINCT DECODE(R.STATUS,'OHD',DECODE(PPFS.BOOKING_CODE,NULL,NULL, PPF.pnr_pax_id),NULL))ADULT_ONHOLD_QTY");
		 query.append("  FROM t_pnr_segment PS,");
		 query.append("    t_flight_segment FS,");
		 query.append("    T_RESERVATION r,");
		 query.append("    t_pnr_pax_fare_segment PPFS,");
		 query.append("    t_pnr_pax_fare PPF");
		 query.append("  WHERE PS.flt_seg_id   =FS.flt_seg_id");
		 query.append("  AND PPFS.ppf_id       =PPF.ppf_id");
		 query.append("  AND PPFS.pnr_seg_id   =PS.pnr_seg_id");
		 query.append("  AND PS.STATUS         ='CNF'");
		 query.append("  AND PS.PNR            =r.PNR");
		 query.append("  AND r.STATUS      IN('OHD')");
		 query.append("  AND FS.flight_id      = "+flightID);
		 query.append("  AND ppfs.booking_code = '"+bookingCode+"'");
		 query.append("  AND fs.segment_code   = '"+segmentCode+"'");
		 query.append("  GROUP BY FLIGHT_ID,");
		 query.append("    SEGMENT_CODE,");
		 query.append("    BOOKING_CODE");
		 query.append("  ) res,");
		 query.append(" (SELECT (bc.onhold_seats - NVL(");
		 query.append(" (SELECT SUM(onhold_seats)");
		 query.append(" FROM t_fcc_seg_bc_nesting");
		 query.append(" WHERE from_fccsba_id=bc.fccsba_id");
		 query.append(" ),0)+ NVL(");
		 query.append(" (SELECT SUM(onhold_seats)");
		 query.append(" FROM t_fcc_seg_bc_nesting");
		 query.append(" WHERE to_fccsba_id=bc.fccsba_id");
		 query.append(" ),0) - NVL(");
		 query.append(" (SELECT SUM(seats_blocked)");
		 query.append(" FROM t_temp_seg_bc_alloc t");
		 query.append(" WHERE t.fccsba_id = bc.fccsba_id");		 
		 query.append(" ),0)) onhold_seats");
		 query.append("  FROM t_fcc_seg_bc_alloc bc");
		 query.append("  WHERE bc.flight_id  = "+flightID);
		 query.append("  AND bc.segment_code = '"+segmentCode+"'");	
		 query.append("  AND bc.booking_code = '"+bookingCode+"'");
		 query.append("  ) inv");
		 
		 JdbcTemplate jt = new JdbcTemplate(
					ReservationModuleUtils.getDatasource());
			Object[] param = {};
			
			return (Boolean) jt.query(query.toString(), param,	
					new ResultSetExtractor() {

						@Override
						public Object extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							int diff = 0;
							boolean hasMismatchOccured = false;
							if (rs != null) {							
								if(rs.next()) {
									diff = rs.getInt("diff");
									if(diff != 0){
										hasMismatchOccured = true;
									}
								}															
							}
							return hasMismatchOccured;
						}

					});			
	}
	
	public Map<String, Collection<String>> getPassengerTitleTypes(Collection<String> excludingPaxTypes) {
		log.debug("Inside getPassengerTitles");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = " SELECT TITLE_CODE, PAX_TYPE_CODE FROM T_PAX_TITLE_TYPE ";
		if (excludingPaxTypes.size() > 0) {
			sql += "WHERE PAX_TYPE_CODE NOT IN (" + BeanUtils.constructINString(excludingPaxTypes) + ")";
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Map<String, Collection<String>> mapPaxTitleTypes = (Map<String, Collection<String>>) jt.query(sql,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Collection<String>> mapPaxTitles = new HashMap<String, Collection<String>>();

						if (rs != null) {
							while (rs.next()) {
								if (mapPaxTitles.get(BeanUtils.nullHandler(rs.getString("TITLE_CODE"))) == null) {
									mapPaxTitles.put(BeanUtils.nullHandler(rs.getString("TITLE_CODE")), new ArrayList<String>());
								}
								mapPaxTitles.get(BeanUtils.nullHandler(rs.getString("TITLE_CODE"))).add(
										BeanUtils.nullHandler(rs.getString("PAX_TYPE_CODE")));
							}
						}

						return mapPaxTitles;
					}
				});

		log.debug("Exit getPassengerTitleTypes");
		return mapPaxTitleTypes;
	}

    @SuppressWarnings("unchecked")
    @Override
    public List<ConnectionPaxInfoTO> getConnectionPax(Date fromDate,Date toDate, int connectionTime,List<String> segments) {

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(new JdbcTemplate(ReservationModuleUtils.getDatasource()).getDataSource());

        final List<ConnectionPaxInfoTO> connectionPaxInfoTOs = new ArrayList<>();

        String sql = "" +
                "SELECT TRUNC(fs.est_time_departure_zulu) flight_date, " +
                "  fs.segment_code, " +
                "  COUNT(*) total_pax, " +
                "  SUM( " +
                "  (SELECT 1 " +
                "  FROM t_pnr_passenger pp2 " +
                "  JOIN t_pnr_segment ps2 " +
                "  ON pp2.pnr = ps2.pnr " +
                "  JOIN t_flight_segment fs2 " +
                "  ON ps2.flt_seg_id = fs2.flt_seg_id " +
                "  JOIN t_flight f2 " +
                "  ON fs2.flight_id   = f2.flight_id " +
                "  WHERE pp2.status   = 'CNF' " +
                "  AND ps2.status     = 'CNF' " +
                "  AND pp2.pnr_pax_id = pp.pnr_pax_id " +
                "  AND f2.destination = f.origin " +
                "  AND f2.origin     <> f.destination " +
                "  AND fs2.est_time_arrival_zulu BETWEEN fs.est_time_departure_zulu - :conTime/1440 AND fs.est_time_departure_zulu " +
                "  )) connection_pax " +
                "FROM t_pnr_passenger pp " +
                "JOIN t_pnr_segment ps " +
                "ON pp.pnr = ps.pnr " +
                "JOIN t_flight_segment fs " +
                "ON ps.flt_seg_id = fs.flt_seg_id " +
                "JOIN t_flight f " +
                "ON fs.flight_id = f.flight_id " +
                "WHERE pp.status = 'CNF' " +
                "AND ps.status   = 'CNF' " +
                "AND fs.segment_code in (:segments) " +
                "AND fs.est_time_departure_zulu BETWEEN :startDate AND :endDate " +
                "GROUP BY TRUNC(fs.est_time_departure_zulu), " +
                "  fs.segment_code";


        Map<String,Object> params = new HashMap<>();

        params.put("conTime", connectionTime);
        params.put("startDate", fromDate);
        params.put("endDate", toDate);
        params.put("segments",segments);

        jdbcTemplate.query(sql,params,new ResultSetExtractor() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()) {

                    ConnectionPaxInfoTO connectionPaxInfoTO = new ConnectionPaxInfoTO();
                    connectionPaxInfoTO.setDate(rs.getDate("flight_date"));
                    connectionPaxInfoTO.setSegmentCode(rs.getString("segment_code"));
                    connectionPaxInfoTO.setTotalPax(rs.getInt("total_pax"));
                    connectionPaxInfoTO.setConnectionPax(rs.getInt("connection_pax"));

                    connectionPaxInfoTOs.add(connectionPaxInfoTO);

                }
                return connectionPaxInfoTOs;
            }
        });

        return connectionPaxInfoTOs;
    }
}
