package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers.base;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public abstract class BaseResModifyObserver<T> {

	protected String observerRef;

	protected ResModifyDtectorDataContext dataContext;

	protected List<RuleResponseDTO> observerValidRuleResponses = new ArrayList<>();

	public abstract void notifyObserver(T t) throws ModuleException;

	public BaseResModifyObserver(String observerRef) {
		this.observerRef = observerRef;
	}

}
