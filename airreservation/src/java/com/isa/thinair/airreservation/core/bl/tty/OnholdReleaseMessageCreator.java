package com.isa.thinair.airreservation.core.bl.tty;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class OnholdReleaseMessageCreator extends TypeBReservationMessageCreator {

	public OnholdReleaseMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForSyncCancelAllSegments();
		passengerNamesComposingStrategy = new PassengerNamesComposerForCancellation();
	}

	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {

		SSROTHERSDTO othsDTO = new SSROTHERSDTO();
		othsDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		othsDTO.setSsrValue(TTYMessageCreatorUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CANCELED_ON_HOLD
				.getCode()));
		ssrDTOs.add(othsDTO);
		return ssrDTOs;
	}

}
