package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.DestinationFareContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.MessageHeaderElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

public class SeatConfigurationElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private String flightNumber;
	private Date departureDate;
	private String departureAirportCode;
	private String partNumber;

	private MessageHeaderElementContext heContext;
	private DestinationFareContext pEContext;

	private Map<String, LinkedHashMap<String, String>> seatConfigurationElements;

	private BaseRuleExecutor<RulesDataContext> seatConfigurationRuleExecutor = new HeaderElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		RulesDataContext rulesContext = null;

		initContextData(context);
		saeatConfigElementTemplate();

		executeNext();

	}

	private void initContextData(ElementContext context) {
		heContext = (MessageHeaderElementContext) context;
		pEContext = (DestinationFareContext) context;
		currentLine = heContext.getCurrentMessageLine();
		messageLine = heContext.getMessageString();
		flightNumber = heContext.getFlightNumber();
		departureDate = heContext.getDepartureDate();
		departureAirportCode = heContext.getDepartureAirportCode();
		partNumber = getPartNumber(heContext.getPartNumber());
		seatConfigurationElements = heContext.getSeatConfigurationElements();
	}

	private String getPartNumber(Integer partNumber) {
		String partNumberToString = "";
		if (partNumber != null) {
			partNumberToString = partNumber.toString();
		}
		return partNumberToString;
	}

	private void saeatConfigElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();

		if (!seatConfigurationElements.isEmpty()) {

			elementTemplate.append(seatConfigPrefix());
			elementTemplate.append(forwardSlash());

			Set<String> modelSet = seatConfigurationElements.keySet();

			for (String model : modelSet) {

				LinkedHashMap<String, String> capacityMap = seatConfigurationElements.get(model);
				Set<String> capacityKeySet = capacityMap.keySet();

				for (String cabinClassCode : capacityKeySet) {

					String capacity = capacityMap.get(cabinClassCode);
					int capcitySize = capacity.length();
					String capacityToAdd = "";

					switch (capcitySize) {
					case 1:
						capacityToAdd = "00" + capacity;
						elementTemplate.append(capacityToAdd);
						break;

					case 2:
						capacityToAdd = "0" + capacity;
						elementTemplate.append(capacityToAdd);
						break;

					case 3:
						elementTemplate.append(capacity);
						break;
					}

					elementTemplate.append(cabinClassCode);

				}

				elementTemplate.append(forwardSlash());
				elementTemplate.append(model);
			}

			currentElement = elementTemplate.toString();
		}

		if (currentElement != null) {
			ammendmentPreValidation(true, currentElement, pEContext, seatConfigurationRuleExecutor);
		}
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(pEContext);
		}
	}

}
