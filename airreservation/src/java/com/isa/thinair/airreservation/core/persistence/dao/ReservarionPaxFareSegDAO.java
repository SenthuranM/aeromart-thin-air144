/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;

/**
 * ReservarionPaxFareSegDAO is the business DAO interface for the reservation passenger fare segment apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservarionPaxFareSegDAO {

	/**
	 * Saves reservation passenger fare segments
	 * 
	 * @param reservationPaxFareSegments
	 */

	public void savePaxFareSegments(Collection<ReservationPaxFareSegment> reservationPaxFareSegments);

	public void updatePaxFareSegments(Collection<Integer> paxFareSegmentsWithoutGroupId,
			Map<String, Collection<Integer>> paxFareSegmentsWithGroupIdMap, String status);
	
	public Collection<ReservationPaxFareSegment> getReservationPaxFareSegmentsList(Collection<Integer> pnrPaxFareSegIds);
	
	public ReservationPaxFareSegment getReservationPaxFareSegment(Integer pnrPaxFareSegId);

	public void removePnrPaxFareSegments(Collection<Integer> pnrPaxFareSegIds);
	
	public boolean isSegmentPaxSentWithPNLorADL(Integer pnrSegmentId);
	
	public boolean isPaxAvailableFor(Integer pnrPaxId,Integer pnrSegId);
	
	public boolean isPaxAvailableForCalDelete(Integer pnrPaxId,Integer pnrSegId);
	
}
