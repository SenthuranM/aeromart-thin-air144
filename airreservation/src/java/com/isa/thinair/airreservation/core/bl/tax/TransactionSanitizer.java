package com.isa.thinair.airreservation.core.bl.tax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

public class TransactionSanitizer {
	private Map<Long, OperationChgTnx> sequencedCharges = new HashMap<>();
	private Map<Long, OperationPayTnx> sequencedPayments = new HashMap<>();
	private Sequencer sequencer = null;

	public TransactionSanitizer() {
		sequencer = new DateBasedSequencer();
	}

	public TransactionSanitizer(Sequencer sequencer) {
		if (sequencer != null) {
			this.sequencer = sequencer;
		} else {
			this.sequencer = new DateBasedSequencer();
		}
	}

	public void addCharges(Collection<ReservationPaxOndCharge> charges) {
		OndChargeUtil.setChargeCodeInPaxCharges(charges);
		for (ReservationPaxOndCharge charge : charges) {
			addCharge(charge);
		}
	}

	public void addOperationChgTnx(OperationChgTnx chgTnx) {
		if (!sequencedCharges.containsKey(chgTnx.getKey())) {
			sequencedCharges.put(chgTnx.getKey(), chgTnx.clone());
		} else {
			for (ReservationPaxOndCharge ondCharge : chgTnx.getChargeList()) {
				sequencedCharges.get(chgTnx.getKey()).addCharge(ondCharge);
			}
		}
	}

	public void addOperationPayTnx(OperationPayTnx payTnx) {
		if (!sequencedPayments.containsKey(payTnx.getKey())) {
			sequencedPayments.put(payTnx.getKey(), payTnx.clone());
		} else {
			for (ReservationTnx resTnx : payTnx.getPayments()) {
				sequencedPayments.get(payTnx.getKey()).addPaymentTnx(resTnx);
			}
		}
	}

	public void addCharge(ReservationPaxOndCharge charge) {
		Long key = sequencer.getKey(charge);
		if (!sequencedCharges.containsKey(key)) {
			sequencedCharges.put(key, new OperationChgTnx(charge.getZuluChargeDate(), key));
		}
		sequencedCharges.get(key).addCharge(charge);
	}

	public void addPaymentTnx(ReservationTnx resTnx) {
		Long key = sequencer.getKey(resTnx);
		if (!sequencedPayments.containsKey(key)) {
			sequencedPayments.put(key, new OperationPayTnx(resTnx.getDateTime(), key));
		}
		sequencedPayments.get(key).addPaymentTnx(resTnx);
	}

	public TransactionSequencer getTransactionSequencer() {
		purgeZeroChgTransactions();
		Set<Long> distinctSequences = new HashSet<>();
		distinctSequences.addAll(sequencedCharges.keySet());
		distinctSequences.addAll(sequencedPayments.keySet());
		List<Long> orderedSequence = new ArrayList<>();
		orderedSequence.addAll(distinctSequences);
		Collections.sort(orderedSequence);
		TransactionSequencer transactionSequencer = new TransactionSequencer(this.sequencer.getMinmumSequence());
		for (Long sequenceEntry : orderedSequence) {
			OperationChgTnx chgs = sequencedCharges.get(sequenceEntry);
			OperationPayTnx payments = sequencedPayments.get(sequenceEntry);
			transactionSequencer.addCharge(chgs);
			if (payments != null) {
				transactionSequencer.addPayment(payments);
			} else {
				transactionSequencer.attempCreditPayment(chgs.getTnxDate().getTime());
			}
		}
		return transactionSequencer;

	}

	private void purgeZeroChgTransactions() {
		if (sequencedCharges.size() > sequencedPayments.size()) {
			Iterator<Entry<Long, OperationChgTnx>> chgEntryItr = sequencedCharges.entrySet().iterator();
			while (chgEntryItr.hasNext()) {
				Entry<Long, OperationChgTnx> chgEntry = chgEntryItr.next();
				Long sequenceEntry = chgEntry.getKey();
				OperationChgTnx opChg = sequencedCharges.get(sequenceEntry);
				if (opChg.getTotalChargeAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 0) {

					if (opChg.getChargeList().size() > 0) {
						Long prevEntry = sequenceEntry - 1;
						if (prevEntry > -1 && sequencedCharges.containsKey(prevEntry)) {
							sequencedCharges.get(prevEntry).addCharges(opChg.getChargeList());
						}
					}

					chgEntryItr.remove();
				}
			}
		}
	}

}

interface Sequencer {
	public Long getKey(ReservationPaxOndCharge charge);

	public Long getKey(ReservationTnx resTnx);

	public int getMinmumSequence();
}

class DateBasedSequencer implements Sequencer {

	private static final int SEC_TO_ROUND = 3;

	@Override
	public Long getKey(ReservationPaxOndCharge charge) {
		return getNormalizedDate(charge.getZuluChargeDate()).getTime();
	}

	@Override
	public Long getKey(ReservationTnx resTnx) {
		return getNormalizedDate(resTnx.getDateTime()).getTime();
	}

	private Date getNormalizedDate(Date inputDate) {
		BigDecimal epochBD = BigDecimal.valueOf(inputDate.getTime());
		BigDecimal boundry = BigDecimal.valueOf(SEC_TO_ROUND * 1000);
		BigDecimal breakpoint = BigDecimal.valueOf(SEC_TO_ROUND * 1000 - 1);
		BigDecimal rounded = AccelAeroRounderPolicy.getRoundedValue(epochBD, boundry, breakpoint);

		Date newd = new Date(Long.parseLong(rounded.toPlainString()));

		return newd;
	}

	@Override
	public int getMinmumSequence() {
		return 1;
	}

}