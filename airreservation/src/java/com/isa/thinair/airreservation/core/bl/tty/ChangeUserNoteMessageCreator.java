package com.isa.thinair.airreservation.core.bl.tty;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ChangeUserNoteMessageCreator extends TypeBReservationMessageCreator {

	public ChangeUserNoteMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}

	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {
		SSROTHERSDTO othsDTO = new SSROTHERSDTO();
		othsDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		othsDTO.setSsrValue(typeBRequestDTO.getUserNote());
		ssrDTOs.add(othsDTO);
		return ssrDTOs;
	}
}
