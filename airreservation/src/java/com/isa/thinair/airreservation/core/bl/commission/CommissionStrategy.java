package com.isa.thinair.airreservation.core.bl.commission;

import java.util.List;

import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;
/**
 * Super interface to agent commission granting strategies
 */
public interface CommissionStrategy {

	public boolean execute(PreferenceAnalyzer preferenceAnalyzer, String ondCode, List<String> journeyOnds, List<List<String>> journeyOndSummary);

}
