/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext;

/**
 * @author udithad
 *
 */
public class NameElementRuleContext extends RulesDataContext {

	private String previourPnr;
	private String ongoingPnr;

	public String getPreviourPnr() {
		return previourPnr;
	}

	public void setPreviourPnr(String previourPnr) {
		this.previourPnr = previourPnr;
	}

	public String getOngoingPnr() {
		return ongoingPnr;
	}

	public void setOngoingPnr(String ongoingPnr) {
		this.ongoingPnr = ongoingPnr;
	}

}
