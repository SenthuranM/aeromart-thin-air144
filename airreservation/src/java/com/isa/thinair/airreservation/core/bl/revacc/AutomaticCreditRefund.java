/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to refund PAX credit to credit card via IPG
 * 
 * @author M.Rikaz
 * @isa.module.command name="automaticCreditRefund"
 */
public class AutomaticCreditRefund extends DefaultBaseCommand {

	// Dao's
	private ReservationCreditDAO reservationCreditDao;
	private static Log log = LogFactory.getLog(AutomaticCreditRefund.class);

	private static final String CO_TYPE_AGENT = "CO";

	/**
	 * constructor of the AutomaticCreditRefund command
	 */
	public AutomaticCreditRefund() {
		// looking up daos
		reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
	}

	/**
	 * execute method of the AutomaticCreditRefund command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Boolean refundOnlyNSPax = (Boolean) this.getParameter(CommandParamNames.IS_REFUND_ONLY_NOSHOWPAX);
		Boolean isVoidReservation = (Boolean) this.getParameter(CommandParamNames.IS_VOID_OPERATION);
		String channelIdStr = (String) this.getParameter(CommandParamNames.OWNER_CHANNEL_ID);		

		boolean paxCreditAutoRefundEnabled = AppSysParamsUtil.isIBEPaxCreditAutoRefundEnabled();
		boolean isOwnerAgentTransferEnabled = AppSysParamsUtil.isOwnerAgentTransferEnabled();
		boolean autoRefundVoidReservation = isVoidReservation && AppSysParamsUtil.isVoidAutoRefundEnabled();

		Reservation reservation = null;
		Integer ownerChannelId = null;
		boolean isOriginChannelWebAndrioidOrIOS = false;
		boolean isOwnerChannelWebAndrioidOrIOS = false;

		if (pnr != null) {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadEtickets(true);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
			if (reservation.getAdminInfo() != null) {

				isOriginChannelWebAndrioidOrIOS = SalesChannelsUtil.isSalesChannelWebOrMobile(reservation.getAdminInfo()
						.getOriginChannelId());

				ownerChannelId = !StringUtil.isNullOrEmpty(channelIdStr) ? new Integer(channelIdStr) : reservation.getAdminInfo()
						.getOwnerChannelId();

				isOwnerChannelWebAndrioidOrIOS = SalesChannelsUtil.isSalesChannelWebOrMobile(ownerChannelId);

			}
		}
		
		

		if (((paxCreditAutoRefundEnabled && isOwnerAgentTransferEnabled) || autoRefundVoidReservation) && reservation != null
				&& (isOriginChannelWebAndrioidOrIOS) && (isOwnerChannelWebAndrioidOrIOS)) {

			log.info("Calling PaymentGatewayCreditRefund sales-channel - " + credentialsDTO.getSalesChannelCode());

			Map<Integer, BigDecimal> paxWiseCreditMap = new HashMap<Integer, BigDecimal>();
			Collection<Integer> colPnrPaxIds = new ArrayList<Integer>();

			Iterator<ReservationPax> paxColItr = reservation.getPassengers().iterator();

			while (paxColItr.hasNext()) {
				ReservationPax reservationPax = paxColItr.next();

				if (reservationPax.getTotalAvailableBalance() != null
						&& reservationPax.getTotalAvailableBalance().doubleValue() < 0 && !refundOnlyNSPax) {

					paxWiseCreditMap.put(reservationPax.getPnrPaxId(), reservationPax.getTotalAvailableBalance().negate());
					colPnrPaxIds.add(reservationPax.getPnrPaxId());

				} else if (reservationPax.getTotalAvailableBalance() != null
						&& reservationPax.getTotalAvailableBalance().doubleValue() < 0 && refundOnlyNSPax
						&& hasAnyNSETicket(reservationPax.geteTickets())) {
					paxWiseCreditMap.put(reservationPax.getPnrPaxId(), reservationPax.getTotalAvailableBalance().negate());
					colPnrPaxIds.add(reservationPax.getPnrPaxId());
				}
			}

			Map<Integer, Collection<ReservationTnx>> pnrPaxIdAndColReserTnxMap = ReservationBO.getPNRPaxPaymentsAndRefunds(
					colPnrPaxIds, true, true);

			boolean isAnySuccessRefund = false;
			for (Integer pnrPaxId : pnrPaxIdAndColReserTnxMap.keySet()) {
				Collection<ReservationTnx> transactionCol = pnrPaxIdAndColReserTnxMap.get(pnrPaxId);
				Iterator<ReservationTnx> transactionItr = transactionCol.iterator();
				// Available Total Balance
				BigDecimal paxTotalCredit = paxWiseCreditMap.get(pnrPaxId);

				if (paxTotalCredit != null && paxTotalCredit.doubleValue() > 0) {
					while (transactionItr.hasNext()) {
						boolean isRefundSuccess = false;
						ReservationTnx reservationTnx = transactionItr.next();
						Integer txnId = reservationTnx.getTnxId();
						// do we need to allow only Saman PG to refund??
						if (ReservationTnxNominalCode.getCreditCardNominalCodes().contains(reservationTnx.getNominalCode())) {
							isRefundSuccess = paxCreditRefundRequest(pnrPaxId, txnId, reservationTnx, reservation,
									paxTotalCredit, credentialsDTO);
						} else {
							log.info("Transaction " + txnId + " is not a Credit Card Transaction.");
						}

						if (!isAnySuccessRefund && isRefundSuccess) {
							isAnySuccessRefund = true;
						}
					}
				}

			}
			// at least one successful refund
			response.addResponceParam(PaymentConstants.PARAM_SUCCESSFULL_REFUND, isAnySuccessRefund);

		} else if ((AppSysParamsUtil.isAgentCreditRefundEnabled() || autoRefundVoidReservation)
				&& (SalesChannelsUtil.SALES_CHANNEL_WEB != reservation.getAdminInfo().getOriginChannelId())
				&& (SalesChannelsUtil.SALES_CHANNEL_WEB != reservation.getAdminInfo().getOwnerChannelId())) {

			if (!AppSysParamsUtil.getAgentTypesWhichAgentCreditRefundEnabled().isEmpty()) {

				Map<Integer, BigDecimal> paxWiseCreditMap = new HashMap<Integer, BigDecimal>();
				Collection<Integer> colPnrPaxIds = new ArrayList<Integer>();

				Iterator<ReservationPax> paxColItr = reservation.getPassengers().iterator();

				if (paxColItr != null) {

					while (paxColItr.hasNext()) {
						ReservationPax reservationPax = paxColItr.next();

						if (reservationPax.getTotalAvailableBalance() != null
								&& reservationPax.getTotalAvailableBalance().doubleValue() < 0) {

							paxWiseCreditMap.put(reservationPax.getPnrPaxId(), reservationPax.getTotalAvailableBalance()
									.negate());
							colPnrPaxIds.add(reservationPax.getPnrPaxId());

						}

					}

					Map<Integer, Collection<ReservationTnx>> pnrPaxIdAndColResTnxMap = ReservationBO
							.getPNRPaxPaymentsAndRefunds(colPnrPaxIds, true, true);

					Map<Integer, Collection<ReservationTnx>> filteredPnrPaxIdAndColReserTnxMap = filteredPerPaxOnAccountPaymentTransactions(
							pnrPaxIdAndColResTnxMap, AppSysParamsUtil.getAgentTypesWhichAgentCreditRefundEnabled());

					
					boolean isAnySuccessRefund = false;
					BigDecimal totalRefundAmt = BigDecimal.ZERO;
					
					if (filteredPnrPaxIdAndColReserTnxMap != null) {
						for (Integer pnrPaxId : filteredPnrPaxIdAndColReserTnxMap.keySet()) {
							Collection<ReservationTnx> transactionCol = filteredPnrPaxIdAndColReserTnxMap.get(pnrPaxId);
							Iterator<ReservationTnx> transactionItr = transactionCol.iterator();
							// Available Total Balance
							BigDecimal paxTotalCredit = paxWiseCreditMap.get(pnrPaxId);
							if (paxTotalCredit != null && paxTotalCredit.doubleValue() > 0) {
								while (transactionItr.hasNext()) {
									boolean isRefundSuccess = false;
									ReservationTnx reservationTnx = transactionItr.next();
									Integer txnId = reservationTnx.getTnxId();
									isRefundSuccess = paxCreditRefund(pnrPaxId, txnId, reservationTnx, reservation,
											paxTotalCredit, credentialsDTO);
									totalRefundAmt = AccelAeroCalculator.add(totalRefundAmt, paxTotalCredit);
									if (!isAnySuccessRefund && isRefundSuccess) {
										isAnySuccessRefund = true;
									}
								}
							}

						}
					}
					
					log.info("Total amount automatically refunded from PNR:" +pnr+ "is " +totalRefundAmt);
					
					// at least one successful refund
					response.addResponceParam(PaymentConstants.PARAM_SUCCESSFULL_REFUND, isAnySuccessRefund);
				}
			}
		}

		if (response.isSuccess()) {
			return response;
		} else {
			throw new ModuleException("airreservation.revacc.error");
		}

	}

	private boolean paxCreditRefund(Integer pnrPaxId, Integer txnId, ReservationTnx reservationTnx, Reservation reservation,
			BigDecimal paxTotalCredit, CredentialsDTO credentialsDTO) throws ModuleException {
		Collection<ReservationCredit> paxCreditColl = getPaxCredits(pnrPaxId, txnId);
		BigDecimal paymentAmount = reservationTnx.getAmount().negate();
		BigDecimal totalConsumedAmt = BigDecimal.ZERO;
		String pnr = reservation.getPnr();
		boolean isAnySuccessRefund = false;
		if (paxCreditColl != null && paxCreditColl.size() > 0) {
			List<ReservationCredit> paxCreditList = new ArrayList<ReservationCredit>(paxCreditColl);

			for (int i = paxCreditList.size(); i > 0; i--) {

				ReservationCredit reservationCredit = paxCreditList.get(i - 1);

				if (reservationCredit != null) {

					boolean isCreditExpired = CalendarUtil.isLessThan(reservationCredit.getExpiryDate(), new Date());
					BigDecimal refundAmount = reservationCredit.getBalance();

					log.info(pnrPaxId + " Credit Details {AMOUNT,EXP_DATE,CRED_EXPIRED} - " + refundAmount + ","
							+ reservationCredit.getExpiryDate() + "," + isCreditExpired);

					if (!isCreditExpired && refundAmount.doubleValue() > 0) {
						BigDecimal actualBalance = AccelAeroCalculator.subtract(paymentAmount, totalConsumedAmt);

						if ((paymentAmount.doubleValue() >= refundAmount.doubleValue())
								&& (actualBalance.doubleValue() >= refundAmount.doubleValue())) {
							ReservationPax reservationPax = getReservationPax(reservation.getPassengers(), pnrPaxId);

							IPGIdentificationParamsDTO ipgIdentParamsDTO = getIPGIdentParamDto(txnId);

							if (reservationPax != null) {

								IPassenger iPassenger = getPaxAssmblerForAgentCredit(refundAmount, reservationPax, null,
										ipgIdentParamsDTO, credentialsDTO, reservation.getAdminInfo(), txnId, reservationTnx);

								ServiceResponce sr = new DefaultServiceResponse(false);
								String paxInfo = reservationPax.getTitle() + ", " + reservationPax.getFirstName() + ", "
										+ reservationPax.getLastName();
								String txnDetails = "TXN_ID:" + txnId + ",CRD_ID:" + reservationCredit.getCreditId();

								try {

									sr = ReservationModuleUtils.getPassengerBD().passengerRefundWithRequiresNew(
											reservation.getPnr(), iPassenger, "Agent Auto Refund", true,
											getReservationLastVersion(reservation.getPnr()), null, false);

									composeAudit(pnr, paxInfo, refundAmount,
											AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.TXN_SUCCESS, txnDetails,
											credentialsDTO, 2, reservation.getAdminInfo());
								} catch (ModuleException e) {
									// TODO: handle exception
									log.error(e);
									composeAudit(pnr, paxInfo, refundAmount,
											AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.TXN_FAILED, txnDetails,
											credentialsDTO, 2, reservation.getAdminInfo());
								}

								if (sr.isSuccess()) {
									isAnySuccessRefund = true;
									paxTotalCredit = AccelAeroCalculator.subtract(paxTotalCredit, refundAmount);
									totalConsumedAmt = AccelAeroCalculator.add(totalConsumedAmt, refundAmount);
									log.info("Successfully refunded {PNR_PAX_ID,TXN_ID,REFUND_AMT} - " + pnrPaxId + "," + txnId
											+ "," + refundAmount);
								} else {
									log.info("Refund Failed {PNR_PAX_ID,TXN_ID,REFUND_AMT} - " + pnrPaxId + "," + txnId + ","
											+ refundAmount);
								}

							} else {
								log.info("Can't Refund,some required arguments are missing ");
							}
						}

					} else {
						log.info("This credit is expired,can't Refund this amount");
					}

				}

			}

		}
		return isAnySuccessRefund;
	}

	private boolean paxCreditRefundRequest(Integer pnrPaxId, Integer txnId, ReservationTnx reservationTnx,
			Reservation reservation, BigDecimal paxTotalCredit, CredentialsDTO credentialsDTO) throws ModuleException {
		Collection<ReservationCredit> paxCreditColl = getPaxCredits(pnrPaxId, txnId);
		BigDecimal paymentAmount = reservationTnx.getAmount().negate();
		BigDecimal totalConsumedAmt = BigDecimal.ZERO;
		String pnr = reservation.getPnr();
		boolean isAnySuccessRefund = false;
		if (paxCreditColl != null && paxCreditColl.size() > 0) {
			List<ReservationCredit> paxCreditList = new ArrayList<ReservationCredit>(paxCreditColl);

			for (int i = paxCreditList.size(); i > 0; i--) {

				ReservationCredit reservationCredit = paxCreditList.get(i - 1);

				if (reservationCredit != null) {

					boolean isCreditExpired = CalendarUtil.isLessThan(reservationCredit.getExpiryDate(), new Date());
					BigDecimal refundAmount = reservationCredit.getBalance();

					log.info(pnrPaxId + " Credit Details {AMOUNT,EXP_DATE,CRED_EXPIRED} - " + refundAmount + ","
							+ reservationCredit.getExpiryDate() + "," + isCreditExpired);

					if (!isCreditExpired && refundAmount.doubleValue() > 0) {
						BigDecimal actualBalance = AccelAeroCalculator.subtract(paymentAmount, totalConsumedAmt);

						if ((paymentAmount.doubleValue() >= refundAmount.doubleValue())
								&& (actualBalance.doubleValue() >= refundAmount.doubleValue())) {
							ReservationPax reservationPax = getReservationPax(reservation.getPassengers(), pnrPaxId);

							IPGIdentificationParamsDTO ipgIdentParamsDTO = getIPGIdentParamDto(txnId);

							CardPaymentInfo cardPaymentInfo = getCreditCardInfo(txnId);

							if (reservationPax != null && cardPaymentInfo != null && ipgIdentParamsDTO != null) {

								IPassenger iPassenger = getPaxAssembler(refundAmount, reservationPax, cardPaymentInfo,
										ipgIdentParamsDTO, txnId);

								ServiceResponce sr = new DefaultServiceResponse(false);
								String paxInfo = reservationPax.getTitle() + ", " + reservationPax.getFirstName() + ", "
										+ reservationPax.getLastName();
								String txnDetails = "TXN_ID:" + txnId + ",CRD_ID:" + reservationCredit.getCreditId();

								try {

									sr = ReservationModuleUtils.getPassengerBD().passengerRefundWithRequiresNew(
											reservation.getPnr(), iPassenger, "Auto Refund", true,
											getReservationLastVersion(reservation.getPnr()), null, false);

									composeAudit(pnr, paxInfo, refundAmount,
											AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.TXN_SUCCESS, txnDetails,
											credentialsDTO, 1, null);
								} catch (ModuleException e) {
									// TODO: handle exception
									log.error(e);
									composeAudit(pnr, paxInfo, refundAmount,
											AuditTemplateEnum.TemplateParams.PaxCreditAutoRefund.TXN_FAILED, txnDetails,
											credentialsDTO, 1, null);
								}

								if (sr.isSuccess()) {
									isAnySuccessRefund = true;
									paxTotalCredit = AccelAeroCalculator.subtract(paxTotalCredit, refundAmount);
									totalConsumedAmt = AccelAeroCalculator.add(totalConsumedAmt, refundAmount);
									log.info("Successfully refunded {PNR_PAX_ID,TXN_ID,REFUND_AMT} - " + pnrPaxId + "," + txnId
											+ "," + refundAmount);
								} else {
									log.info("Refund Failed {PNR_PAX_ID,TXN_ID,REFUND_AMT} - " + pnrPaxId + "," + txnId + ","
											+ refundAmount);
								}

							} else {
								log.info("Can't Refund,some required arguments are missing ");
							}
						}

					} else {
						log.info("This credit is expired,can't Refund this amount");
					}

				}

			}

		}
		return isAnySuccessRefund;
	}

	private static IPassenger getPaxAssembler(BigDecimal refundAmount, ReservationPax reservationPax,
			CardPaymentInfo cardPaymentInfo, IPGIdentificationParamsDTO ipgIdentParamsDTO, Integer originalTnxId)
			throws ModuleException {
		IPassenger iPassenger = new PassengerAssembler(null);

		Integer pnrPaxId = reservationPax.getPnrPaxId();

		IPayment iPayment = new PaymentAssembler();

		iPayment.addCardPayment(cardPaymentInfo.getType(), cardPaymentInfo.getEDate(), cardPaymentInfo.getNo(),
				cardPaymentInfo.getName(), cardPaymentInfo.getAddress(), cardPaymentInfo.getSecurityCode(), refundAmount,
				AppIndicatorEnum.APP_IBE, cardPaymentInfo.getTnxMode(), new Integer(cardPaymentInfo.getCcdId()),
				ipgIdentParamsDTO, null, null, cardPaymentInfo.getPayCarrier(), cardPaymentInfo.getPayCurrencyDTO(),
				cardPaymentInfo.getLccUniqueId(), cardPaymentInfo.getPaymentBrokerRefNo() + "",
				new Integer(cardPaymentInfo.getPaymentBrokerRefNo()), cardPaymentInfo.getUserInputDTO(), originalTnxId);

		iPassenger.addPassengerPayments(pnrPaxId, iPayment);
		return iPassenger;

	}

	private static IPassenger getPaxAssmblerForAgentCredit(BigDecimal refundAmount, ReservationPax reservationPax,
			AgentCreditInfo agentCreditInfo, IPGIdentificationParamsDTO ipgIdentParamsDTO, CredentialsDTO credentialsDTO,
			ReservationAdminInfo adminInfo, Integer paymentTnxId, ReservationTnx reservationTnx) throws ModuleException {

		IPassenger iPassenger = new PassengerAssembler(null);

		Integer pnrPaxId = reservationPax.getPnrPaxId();

		IPayment iPayment = new PaymentAssembler();
		
		BigDecimal exRate = new ExchangeRateProxy(reservationTnx.getDateTime()).getExchangeRate(AppSysParamsUtil.getBaseCurrency(), reservationTnx.getPayCurrencyCode());
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(credentialsDTO.getAgentCurrencyCode(), exRate);
		
		iPayment.addAgentCreditPayment(reservationTnx.getAgentCode(), refundAmount, null, null, adminInfo.getOriginCarrierCode(),
				payCurrencyDTO, null, "OA", paymentTnxId);

		iPassenger.addPassengerPayments(pnrPaxId, iPayment);
		return iPassenger;
	}

	private CardPaymentInfo getCreditCardInfo(Integer txnId) throws ModuleException {

		Collection<Integer> txnIds = new ArrayList<Integer>();
		txnIds.add(txnId);

		Map<Integer, CardPaymentInfo> mapCardPayInfo = ReservationModuleUtils.getReservationBD().getCreditCardInfo(txnIds, true);
		CardPaymentInfo cardPaymentInfo = null;

		for (Integer txnIdTemp : mapCardPayInfo.keySet()) {
			cardPaymentInfo = mapCardPayInfo.get(txnIdTemp);
			break;
		}
		return cardPaymentInfo;
	}

	private static IPGIdentificationParamsDTO getIPGIdentParamDto(Integer paxTxnId) throws ModuleException {
		String paymentGatewayName = ReservationModuleUtils.getPaymentBrokerBD().getPaymentGatewayNameForCCTransaction(paxTxnId,
				true);
		IPGIdentificationParamsDTO ipgIdentParamsDTO = null;
		if (paymentGatewayName != null && !paymentGatewayName.trim().equals("")) {

			String[] ipgInfo = paymentGatewayName.split("_");

			ipgIdentParamsDTO = new IPGIdentificationParamsDTO(new Integer(ipgInfo[0]), ipgInfo[1]);
			ipgIdentParamsDTO.setFQIPGConfigurationName(paymentGatewayName);

		}
		return ipgIdentParamsDTO;

	}

	private static ReservationPax getReservationPax(Collection<ReservationPax> reservationPaxs, Integer pnrPaxId) {
		Iterator<ReservationPax> paxColItr = reservationPaxs.iterator();
		ReservationPax reservationPax = null;
		while (paxColItr.hasNext()) {
			reservationPax = paxColItr.next();
			// // if (!reservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
			if (pnrPaxId.intValue() == reservationPax.getPnrPaxId().intValue()) {
				return reservationPax;
			}

		}
		return null;

	}

	private Collection<ReservationCredit> getPaxCredits(Integer pnrPaxId, Integer txnId) {
		Collection<ReservationCredit> paxCreditColl = new ArrayList<ReservationCredit>();

		Collection<Integer> txnIds = new ArrayList<Integer>();
		txnIds.add(txnId);
		paxCreditColl = reservationCreditDao.getReservationCredits(pnrPaxId + "", txnIds);
		return paxCreditColl;
	}

	private void composeAudit(String pnr, String paxInfo, BigDecimal amount, String status, String txnDetails,
			CredentialsDTO credentialsDTO, int auditOption, ReservationAdminInfo adminInfo) throws ModuleException {
		PassengerBD passengerBD = ReservationModuleUtils.getPassengerBD();
		passengerBD.auditPaxAutoRefund(pnr, paxInfo, amount, status, txnDetails, credentialsDTO, auditOption, adminInfo);
	}

	private static long getReservationLastVersion(String pnr) throws ModuleException {
		Reservation reservation = null;
		long version = 0l;
		if (pnr != null) {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(false);
			pnrModesDTO.setLoadPaxAvaBalance(false);
			pnrModesDTO.setLoadSegView(false);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
			version = reservation.getVersion();
		}
		return version;
	}

	private boolean hasAnyNSETicket(Collection<EticketTO> eTickets) {
		// if Pax has at least one e-ticket with status as NS then that pax credit eligible to auto refund
		if (eTickets.size() > 0) {
			Iterator<EticketTO> eTicketIte = eTickets.iterator();
			while (eTicketIte.hasNext()) {
				EticketTO eticketTO = eTicketIte.next();
				if (eticketTO.getPaxStatus().equalsIgnoreCase(ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * On account refund will only happen,
	 *  1. If all the on account payments are from the same agent for all the passengers
	 *  2. It is the only payment method used for the PNR
	 *  3. Agent's type is a allowed type for automatic refund
	 * @param pnrPaxIdAndColReserTnxMap
	 * @param autoRefundAllowedAgentTypes
	 * @return
	 * @throws ModuleException
	 */
	private Map<Integer, Collection<ReservationTnx>> filteredPerPaxOnAccountPaymentTransactions(
			Map<Integer, Collection<ReservationTnx>> pnrPaxIdAndColReserTnxMap, List<String> autoRefundAllowedAgentTypes)
			throws ModuleException {

		String refundingAgentCode = null;
		boolean valid = true;
		for (Integer pnrPaxId : pnrPaxIdAndColReserTnxMap.keySet()) {
			Collection<ReservationTnx> allPaymentTransactions = pnrPaxIdAndColReserTnxMap.get(pnrPaxId);

			Iterator<ReservationTnx> transactionItr = allPaymentTransactions.iterator();

			while (transactionItr.hasNext()) {
				ReservationTnx currentTransaction = transactionItr.next();
				if (currentTransaction.getAmount().compareTo(BigDecimal.ZERO) == 0) {
					transactionItr.remove();
					continue;
				}

				if (!ReservationTnxNominalCode.getOnAccountTypeNominalCodes().contains(
						currentTransaction.getNominalCode())) {
					valid = false;
					break;
				}

				if (refundingAgentCode == null) {
					String agentTypeCode = ReservationModuleUtils.getTravelAgentBD().getAgentTypeByAgentId(
							currentTransaction.getAgentCode());
					if (autoRefundAllowedAgentTypes.contains(agentTypeCode)) {
						refundingAgentCode = currentTransaction.getAgentCode();
					} else {
						valid = false;
						break;
					}
				} else {
					if (!refundingAgentCode.equals(currentTransaction.getAgentCode())) {
						valid = false;
						break;
					}
				}
			}

			valid = (!allPaymentTransactions.isEmpty() && valid);

			if (!valid) {
				break;
			}

		}
		return valid ? pnrPaxIdAndColReserTnxMap : null;
	}
	
}
