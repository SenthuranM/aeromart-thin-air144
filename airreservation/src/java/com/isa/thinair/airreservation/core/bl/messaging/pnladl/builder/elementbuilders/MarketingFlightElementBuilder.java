/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.MarketingFlightElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.PnrElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class MarketingFlightElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private PassengerInformation passengerInformation;

	private BaseRuleExecutor<RulesDataContext> marketingBaseRuleExecutor = new MarketingFlightElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		currentElement = "";
		initContextData(context);
		marketingFlightElementTemplate();
		response = validateSubElement(currentElement);
		ammendMessageDataAccordingTo(response);
	}

	private void marketingFlightElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		if (passengerInformation != null
				&& passengerInformation.getCsFlightNo() != null
				&& passengerInformation.getCsBookingClass() != null
				&& passengerInformation.getCsOrginDestination() != null
				&& passengerInformation.getCsDepatureDateTime() != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd");
			SimpleDateFormat tf = new SimpleDateFormat("HHmm");
			elementTemplate.append(MessageComposerConstants.POINT);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.M);
			elementTemplate.append(forwardSlash());
			elementTemplate.append(passengerInformation.getCsFlightNo());
			elementTemplate.append(passengerInformation.getCsBookingClass());
			elementTemplate.append(df.format(passengerInformation
					.getCsDepatureDateTime()));
			elementTemplate.append(PnlAdlUtil.getOndCode(passengerInformation
					.getCsOrginDestination()));
			elementTemplate.append(tf.format(passengerInformation
					.getCsDepatureDateTime()));
			elementTemplate.append(MessageComposerConstants.HK);
			currentElement = elementTemplate.toString();
		}
	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformation = getFirstPassengerInUtilizedList();
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (currentElement != null && !currentElement.isEmpty()) {
			if (response.isProceedNextElement()) {
				if (response.isSpaceAvailable()) {
					executeSpaceElementBuilder(uPContext);
				} else {
					executeConcatenationElementBuilder(uPContext);
				}
				ammendToBaseLine(currentElement, currentLine, messageLine);
			}
		}
		executeNext();
	}

	private PassengerInformation getFirstPassengerInUtilizedList() {
		PassengerInformation passengerInformation = null;
		int index = 0;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformation = uPContext.getUtilizedPassengers().get(index);
		}
		return passengerInformation;
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				"0"+currentLine.toString()+" ", elementText, 0);
		ruleResponse = marketingBaseRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		PnrElementRuleContext rulesDataContext = new PnrElementRuleContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		rulesDataContext.setPnrWiseGroupCodes(uPContext.getPnrWiseGroupCodes());
		rulesDataContext.setPnrWisePassengerCount(uPContext
				.getPnrWisePassengerCount()
				.get(uPContext.getOngoingStoreType()));
		rulesDataContext.setPnrWisePaxReductionCout(uPContext
				.getPnrWisePassengerReductionCount().get(
						uPContext.getOngoingStoreType()));
		rulesDataContext.setUtilizedPassengerCount(uPContext
				.getUtilizedPassengers().size());
		rulesDataContext.setPnr(uPContext.getPnr());
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
