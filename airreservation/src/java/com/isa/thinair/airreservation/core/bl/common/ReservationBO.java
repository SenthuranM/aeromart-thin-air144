/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.InvDowngradeDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.AutoCancellationSchDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ChargeAdjustmentDTO;
import com.isa.thinair.airreservation.api.dto.ChargeRateDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO.MODULE;
import com.isa.thinair.airreservation.api.dto.OpenReturnOndTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServicePenaltyExtChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.WLConfirmationRequestDTO;
import com.isa.thinair.airreservation.api.dto.WLConfirmationResponseDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutoCheckinExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PassengerBaggageAssembler;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckResponseDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.FraudStatus;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.PassengerMealAssembler;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PassengerSeatingAssembler;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.BlacklistReservation;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckin;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckinSeat;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.BlacklistPAXUtil;
import com.isa.thinair.airreservation.api.utils.CCFraudCheckRequestDTOBuilder;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.CHARGE_ADJUSTMENTS;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ResModifyEmailAgentModificationType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.api.utils.SortUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.pnr.PNRNumberGeneratorProxy;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airreservation.core.util.ETicketGenerator;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateJourneyType;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.wsclient.api.service.WSClientBD;

/**
 * Reservation related business methods
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationBO {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ReservationBO.class);

	/** Holds the airreservation config instance */
	private static final AirReservationConfig config = ReservationModuleUtils.getAirReservationConfig();
	private static boolean hasPrivAddSeatsInCutover = false;

	// caching quotedChargeDTOs
	private static QuotedChargeDTO baggageQuotedChargeDTO = null;
	private static QuotedChargeDTO seatQuotedChargeDTO = null;
	private static QuotedChargeDTO mealQuotedChargeDTO = null;
	private static QuotedChargeDTO autoCheckinQuotedChargeDTO = null;

	/**
	 * Hide the constructor
	 */
	private ReservationBO() {

	}

	/**
	 * Return available flights with all fares (For WEB USERS) All flights - fare exist Selected flight - fare exist
	 * 
	 * @param userPrincipal
	 * @param trackInfo
	 */
	public static AvailableFlightDTO getAvailableFlightsWithAllFares(AvailableFlightSearchDTO availableFlightSearchDTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {
		log.debug("Inside getAvailableFlightsWithAllFares");
		if (config.getFlightSearchDateGap() != null) {
			availableFlightSearchDTO.setFlightSearchGap(new Integer(config.getFlightSearchDateGap()));
		}

		// Calling available schedules information
		ServiceResponce response = ReservationModuleUtils.getFlightBD().searchAvailableFlights(availableFlightSearchDTO);
		AvailableFlightDTO availableFlightDTO = (AvailableFlightDTO) response
				.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);

		if (AppSysParamsUtil.showHalfReturnFaresInIBECalendar() &&
				availableFlightSearchDTO.isFareCalendarSearch() &&
				availableFlightSearchDTO.isIncludeHRTFares()) {
			SelectedFlightDTO selectedFlightDTO = availableFlightDTO.getSelectedFlight();
			if (selectedFlightDTO != null && availableFlightSearchDTO.isQuoteFares()) {
				availableFlightDTO = ReservationModuleUtils.getFlightInventoryResBD()
						.searchAvailableFlightsSeatAvailability(availableFlightDTO, availableFlightSearchDTO);
			}
			// Pick applicable promotion, if any
			setPromotionDetails(availableFlightSearchDTO, availableFlightDTO.getSelectedFlight(),userPrincipal,trackInfo);
			availableFlightDTO = ReservationModuleUtils.getFlightInventoryResBD()
					.nonSelectedDatesFareQuoteInIBE(availableFlightDTO, availableFlightSearchDTO, true);
		} else {
			SelectedFlightDTO selectedFlightDTO = availableFlightDTO.getSelectedFlight();
			// Selected flight DTO should not be null, out bound flights should not be empty and if return search in bound
			// flights should not be empty
			boolean includeSelectedDate = false;
			if (selectedFlightDTO != null && availableFlightSearchDTO.isQuoteFares()) {
				availableFlightDTO = ReservationModuleUtils.getFlightInventoryResBD()
						.searchAvailableFlightsSeatAvailability(availableFlightDTO, availableFlightSearchDTO);
				selectedFlightDTO = availableFlightDTO.getSelectedFlight();

				ReservationApiUtils.validateTotalJourneyFareQuoteForBusFare(selectedFlightDTO);

				if (!selectedFlightDTO.isSeatsAvailable()) {
					includeSelectedDate = true;
				}
			}

			// Pick applicable promotion, is any
			setPromotionDetails(availableFlightSearchDTO, availableFlightDTO.getSelectedFlight(),userPrincipal,trackInfo);

			if (availableFlightSearchDTO.isFareCalendarSearch()) {
				availableFlightDTO = ReservationModuleUtils.getFlightInventoryResBD()
						.nonSelectedDatesFareQuoteInIBE(availableFlightDTO, availableFlightSearchDTO, includeSelectedDate);
			}
		}
		log.debug("Exit getAvailableFlightsWithAllFares");
		return availableFlightDTO;
	}

	/**
	 * Return Available day flights with selected fare (For WEB USERS)
	 * 
	 * @param userPrincipal
	 */
	public static AvailableFlightDTO getAvailableFlightsOnly(AvailableFlightSearchDTO availableFlightSearchDTO,
			UserPrincipal userPrincipal) throws ModuleException {
		log.debug("Inside getAvailableFlightsOnly");

		// Calling available schedules information
		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		if (config.getFlightSearchDateGap() != null) {
			availableFlightSearchDTO.setFlightSearchGap(new Integer(config.getFlightSearchDateGap()));
		}
		ServiceResponce responce = flightBD.searchAvailableFlights(availableFlightSearchDTO);
		AvailableFlightDTO availableFlightDTO = (AvailableFlightDTO) responce
				.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);

		// availableFlightDTO.setBookingType(availableFlightSearchDTO.getBookingType());

		log.debug("Exit getAvailableFlightsOnly");
		return availableFlightDTO;
	}

	/**
	 * Return Quote fare for a selected flight
	 * 
	 * @param availableFlightSearchDTO
	 * @param userPrincipal
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static SelectedFlightDTO getFareQuote(AvailableFlightSearchDTO availableFlightSearchDTO, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo, boolean isTransactional) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Inside getFareQuote" + availableFlightSearchDTO.getSummary());
		}
		if (config.getFlightSearchDateGap() != null) {
			availableFlightSearchDTO.setFlightSearchGap(new Integer(config.getFlightSearchDateGap()));
		}
		ServiceResponce serviceResponce = ReservationModuleUtils.getFlightBD().getFilledSelectedFlight(availableFlightSearchDTO,
				isTransactional);
		SelectedFlightDTO selectedFlightDTO = (SelectedFlightDTO) serviceResponce
				.getResponseParam(ResponceCodes.FILL_SELECTED_FLIGHT_SUCCESSFULL);

		// Pick applicable promotion, is any
		if (availableFlightSearchDTO.getJourneyType() == JourneyType.SINGLE_SECTOR
				|| availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP) {
			setPromotionDetails(availableFlightSearchDTO, selectedFlightDTO, userPrincipal,trackInfo);
		}
		if (log.isDebugEnabled()) {
			log.debug("Exit getFareQuote" + availableFlightSearchDTO.getSummary());
		}
		return selectedFlightDTO;
	}

	private static void setPromotionDetails(AvailableFlightSearchDTO availFlightSearchDTO, SelectedFlightDTO selFlightDTO,UserPrincipal userPrincipal,TrackInfoDTO trackInfo)
			throws ModuleException {
		if (availFlightSearchDTO.isPromoApplicable() && !availFlightSearchDTO.isModifyBooking()
				&& AppSysParamsUtil.isPromoCodeEnabled() && selFlightDTO != null && selFlightDTO.getSelectedOndFlights() != null
				&& !selFlightDTO.getSelectedOndFlights().isEmpty() && !availFlightSearchDTO.isRequoteFlightSearch()) {
			ServiceResponce promoResponce = ReservationModuleUtils.getPromotionManagementBD()
					.pickApplicablePromotion(PromotionsUtils.buildPromoSelectionCriteria(selFlightDTO, availFlightSearchDTO,userPrincipal,trackInfo));
			if (promoResponce.isSuccess()) {
				ApplicablePromotionDetailsTO promoDto = (ApplicablePromotionDetailsTO) promoResponce
						.getResponseParam(CommandParamNames.APPLICABLE_PROMOTION_DETAILS);

				long diffMils = 3600000;
				long lngBufferTime = 0;

				if (promoDto.getApplicableAncillaries().contains(PromotionCriteriaConstants.ANCILLARIES.SEAT_MAP.getCode())) {
					diffMils = AppSysParamsUtil.getXBESeatmapStopCutoverInMillis();
				} else if (promoDto.getApplicableAncillaries().contains(PromotionCriteriaConstants.ANCILLARIES.MEAL.getCode())) {
					diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
				} else if (promoDto.getApplicableAncillaries()
						.contains(PromotionCriteriaConstants.ANCILLARIES.BAGGAGE.getCode())) {
					if (availFlightSearchDTO.isAllowTillFinalCutOver()) {
						diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getBaggageFinalCutOverTime());
					} else {
						diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getFreshBaggageCutOverTime());
					}
				} else if (promoDto.getApplicableAncillaries().contains(PromotionCriteriaConstants.ANCILLARIES.SSR.getCode())) {
					diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();
				} else if (promoDto.getApplicableAncillaries()
						.contains(PromotionCriteriaConstants.ANCILLARIES.INSURANCE.getCode())) {
					diffMils = lngBufferTime;
				}
				Calendar currentTime = Calendar.getInstance();

				for (AvailableIBOBFlightSegment fltSegment : selFlightDTO.getSelectedOndFlights()) {
					if (promoDto.getApplicableAncillaries()
							.contains(PromotionCriteriaConstants.ANCILLARIES.AIRPORT_SERVICE.getCode())) {
						long cutOver = fltSegment.getDepartureDateZulu().getTime() - currentTime.getTimeInMillis();
						Map<AirportServiceKeyTO, FlightSegmentDTO> airportService = AncillaryCommonUtil
								.populateAiportWiseFltDTOSegments(fltSegment.getFlightSegmentDTOs());
						boolean isAirportSearviceExist = ReservationModuleUtils.getSsrServiceBD().isAirportServicesAvailable(
								airportService, availFlightSearchDTO.getAppIndicator(), 2,
								availFlightSearchDTO.getPreferredLanguage(), fltSegment.getCabinClassCode(),
								fltSegment.getSelectedLogicalCabinClass());

						if (isAirportSearviceExist) {

							selFlightDTO.setPromotionDetails(promoDto);

						}

					} else {
						if ((fltSegment.getDepartureDateZulu().getTime() - diffMils) < currentTime.getTimeInMillis()) {

							if (availFlightSearchDTO.isHasPrivAddSeatsInCutover() && promoDto.getApplicableAncillaries()
									.contains(PromotionCriteriaConstants.ANCILLARIES.SEAT_MAP.getCode())) {
								if (promoDto != null) {
									selFlightDTO.setPromotionDetails(promoDto);
								}
							}
						} else {
							if (promoDto != null) {
								selFlightDTO.setPromotionDetails(promoDto);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param onHoldReleaseTimeDTOs
	 * @param rulesDTO
	 * @return
	 */
	public static Long getOnHoldReleaseTime(OnHoldReleaseTimeDTO rulesDTO) {
		Collection<OnHoldReleaseTimeDTO> onHoldReleaseTimeDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
				.getOnHoldReleaseTime(rulesDTO);
		// OnHold release time is filtering based on agent code ,module and Rank
		// First priority is for the AgentCode
		// Second Priority is Module(IBE, XBE etc)
		// Rank parameter will be used to filter user applicable release time
		Long releaseTimeMM = null;
		OnHoldReleaseTimeDTO selectedHoldReleaseTimeDTO = null;
		MODULE module = MODULE.ANY;
		String moduleCode = rulesDTO.getModuleCode();
		if (moduleCode != null) {
			if (AppIndicatorEnum.APP_XBE.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.XBE;
			} else if (AppIndicatorEnum.APP_IBE.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.IBE;
			} else if (AppIndicatorEnum.APP_KIOSK.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.KIOSK;
			} else if (AppIndicatorEnum.APP_WS.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.API;
			} else if (AppIndicatorEnum.APP_ANDROID.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.ANDROID;
			} else if (AppIndicatorEnum.APP_IOS.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.IOS;
			}
		}

		if (onHoldReleaseTimeDTOs != null) {
			TreeSet<OnHoldReleaseTimeDTO> agentTree = new TreeSet<OnHoldReleaseTimeDTO>();
			TreeSet<OnHoldReleaseTimeDTO> generalTree = new TreeSet<OnHoldReleaseTimeDTO>();
			for (OnHoldReleaseTimeDTO onHoldReleaseTimeDTO : onHoldReleaseTimeDTOs) {
				if (onHoldReleaseTimeDTO.getFareRuleId() != 0) {
					selectedHoldReleaseTimeDTO = onHoldReleaseTimeDTO;
					break;
				} else if (rulesDTO.getAgentCode() != null
						&& rulesDTO.getAgentCode().equalsIgnoreCase(onHoldReleaseTimeDTO.getAgentCode())) {
					agentTree.add(onHoldReleaseTimeDTO);
				} else {
					generalTree.add(onHoldReleaseTimeDTO);
				}
			}
			if (selectedHoldReleaseTimeDTO == null) {
				if (!agentTree.isEmpty()) {
					selectedHoldReleaseTimeDTO = agentTree.first();
				} else {
					boolean isFound = false;
					boolean isFirstAnyModule = false;
					OnHoldReleaseTimeDTO tempFirstAnyModule = null;
					for (OnHoldReleaseTimeDTO onHoldReleaseTimeDTO : generalTree) {
						if (module.toString().equalsIgnoreCase(onHoldReleaseTimeDTO.getModuleCode())) {
							selectedHoldReleaseTimeDTO = onHoldReleaseTimeDTO;
							isFound = true;
							break;
						} else if (module.toString().equalsIgnoreCase(MODULE.ANY.toString()) && isFirstAnyModule == false) {
							isFirstAnyModule = true;
							tempFirstAnyModule = onHoldReleaseTimeDTO;
						}
					}
					if (isFound == false) {
						if (isFirstAnyModule) {
							selectedHoldReleaseTimeDTO = tempFirstAnyModule;
						} else if (!generalTree.isEmpty()) {
							selectedHoldReleaseTimeDTO = generalTree.first();
						}
					}
				}
			}

			if (selectedHoldReleaseTimeDTO != null) {
				if (selectedHoldReleaseTimeDTO.isReleaseTimeFromBookingDate()) {
					releaseTimeMM = selectedHoldReleaseTimeDTO.getReleaseTime();
				} else {
					releaseTimeMM = (rulesDTO.getFlightDepartureDate().getTime() / (1000 * 60)
							- selectedHoldReleaseTimeDTO.getReleaseTime()) - rulesDTO.getBookingDate().getTime() / (1000 * 60);
				}
			}
		}

		return releaseTimeMM;
	}

	/**
	 * Blocks a seat for reservation
	 * 
	 * @param colOndFare
	 * @return Collection of block ids
	 * @throws ModuleException
	 */
	public static Collection<TempSegBcAlloc> blockSeats(Collection<OndFareDTO> colOndFare, Collection<Integer> flightAmSeatIds)
			throws ModuleException {
		log.debug("Inside blockSeats");

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();

		Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = ReservationCoreUtils.getSegmentSeatDistsDTOs(colOndFare);
		Collection<TempSegBcAlloc> colBlockSeatsForRes = flightInventoryResBD.blockSeatsForRes(segmentSeatDistsDTOs,
				flightAmSeatIds);

		log.debug("Exit blockSeats");
		return colBlockSeatsForRes;
	}

	/**
	 * Move blocked seats to the Sold seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	public static void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedIds) throws ModuleException {
		log.debug("Inside moveBlockedSeatsToSold");

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		flightInventoryResBD.moveBlockedSeatsToSold(blockedIds);

		log.debug("Exit moveBlockedSeatsToSold");
	}

	/**
	 * Move blocked seats to the Sold seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	public static void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedIds,
			Map<Integer, String> flightSeatIdsAndPaxTypes, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException {
		log.debug("Inside moveBlockedSeatsToSold");

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		flightInventoryResBD.moveBlockedSeatsToSold(blockedIds, flightSeatIdsAndPaxTypes, flightMealIds, flightBaggageIds);

		log.debug("Exit moveBlockedSeatsToSold");
	}

	/**
	 * Update go shore seats
	 * 
	 * @param colSegmentSeatDistsDTO
	 * @throws ModuleException
	 */
	public static void updateGoShore(SegmentSeatDistsDTO segmentSeatDistsDTO) throws ModuleException {
		log.debug("Inside updateGoShore");

		FlightInventoryBD flightInventoryBD = ReservationModuleUtils.getFlightInventoryBD();
		flightInventoryBD.updateInvForGoshowPassengers(segmentSeatDistsDTO);

		log.debug("Exit updateGoShore");
	}

	/**
	 * Move blocked seats to the on hold seats
	 * 
	 * @param blockedIds
	 */
	public static void moveBlockSeats(Collection<TempSegBcAlloc> blockedIds, Integer numberOfConfirmAdultSeats,
			Integer numberOfConfirmInfantSeats) throws ModuleException {
		log.debug("Inside moveBlockSeatsToOnhold");

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		flightInventoryResBD.partiallyMoveBlockedSeatsToSold(blockedIds, numberOfConfirmAdultSeats.intValue(),
				numberOfConfirmInfantSeats.intValue());

		log.debug("Exit moveBlockSeatsToOnhold");
	}

	public static void moveBlockSeats(Collection<TempSegBcAlloc> blockedIds, Integer numberOfConfirmAdultSeats,
			Integer numberOfConfirmInfantSeats, Map<Integer, String> flightSeatIdsAndPaxTypes, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException {
		log.debug("Inside partiallyMoveBlockedSeatsToSold numberOfConfirmAdultSeats:" + numberOfConfirmAdultSeats
				+ "numberOfConfirmInfantSeats:" + numberOfConfirmInfantSeats);

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		flightInventoryResBD.partiallyMoveBlockedSeatsToSold(blockedIds, numberOfConfirmAdultSeats.intValue(),
				numberOfConfirmInfantSeats.intValue(), flightSeatIdsAndPaxTypes, flightMealIds, flightBaggageIds);

		log.debug("Exit partiallyMoveBlockedSeatsToSold");
	}

	/**
	 * Release the blocked seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	public static void releaseBlockedSeats(Collection<TempSegBcAlloc> blockedIds) throws ModuleException {
		log.debug("Inside releaseBlockedSeats");

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		flightInventoryResBD.releaseBlockedSeats(blockedIds, false);

		log.debug("Exit releaseBlockedSeats");
	}

	/**
	 * Release sold seats
	 * 
	 * @param colSegmentSeatDistsDTOForRelease
	 * @return
	 * @throws ModuleException
	 */
	public static void releaseSoldOrOnholdFlightInventory(
			Collection<SegmentSeatDistsDTOForRelease> colSegmentSeatDistsDTOForRelease) throws ModuleException {

		log.debug("Inside releaseSoldOrOnholdSeats");

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		flightInventoryResBD.releaseFlightInventory(colSegmentSeatDistsDTOForRelease);

		log.debug("Exit releaseSoldOrOnholdSeats");
	}

	public static ReservationAudit releaseAncillaries(Collection<Integer> flightAMSeatIds, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException {
		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		ReservationAudit reservationAudit = flightInventoryResBD.releaseAncillaryInventory(flightAMSeatIds, flightMealIds,
				flightBaggageIds);
		return reservationAudit;
	}

	/**
	 * Move on hold seats to confirm
	 * 
	 * @param colSegmentSeatDistsDTO
	 * @param exchangedSegInvIds
	 *            TODO
	 * @throws ModuleException
	 */
	private static void moveOnholdSeatsToConfirm(Collection<SegmentSeatDistsDTO> colSegmentSeatDistsDTO,
			Collection<Integer> exchangedSegInvIds) throws ModuleException {
		log.debug("Inside moveOnholdSeatsToConfirm");

		FlightInventoryResBD flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		flightInventoryResBD.confirmOnholdSeats(colSegmentSeatDistsDTO, exchangedSegInvIds);

		log.debug("Exit moveOnholdSeatsToConfirm");
	}

	/**
	 * Return the sold seats
	 * 
	 * @param colReservationPax
	 * @param confirmedPassengers
	 * @param exchangedSegInvIds
	 *            TODO
	 * @throws ModuleException
	 */
	public static void moveOnholdSeatsToConfirm(Collection<ReservationPax> colReservationPax,
			Collection<Integer> confirmedPassengers, Collection<Integer> exchangedSegInvIds) throws ModuleException {
		log.debug("Inside moveOnholdSeatsToConfirm");

		Iterator<ReservationPax> itReservationPax = colReservationPax.iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		Map<Integer, Map<String, Integer>> mapFlightSegments = new HashMap<Integer, Map<String, Integer>>();
		Map<String, Integer> mapBookingCodes;
		Map<Integer, Map<String, String>> mapFlightSegIdWiseNested = new HashMap<Integer, Map<String, String>>();
		Map<String, String> mapBCWiseNested;
		Integer fltSegmentId;
		Integer bookingCodeCount;
		String bookingCode;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (confirmedPassengers.contains(reservationPax.getPnrPaxId())) {
				itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

				while (itReservationPaxFare.hasNext()) {
					reservationPaxFare = itReservationPaxFare.next();

					itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

					while (itReservationPaxFareSegment.hasNext()) {
						reservationPaxFareSegment = itReservationPaxFareSegment.next();
						fltSegmentId = reservationPaxFareSegment.getSegment().getFlightSegId();
						bookingCode = BeanUtils.nullHandler(reservationPaxFareSegment.getBookingCode());

						// Pick only confirmed segments
						if (reservationPaxFareSegment.getSegment().getVersion() != -1
								&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
										.equals(reservationPaxFareSegment.getSegment().getStatus())) {
							if (mapFlightSegments.containsKey(fltSegmentId)) {
								mapBookingCodes = mapFlightSegments.get(fltSegmentId);

								if (mapBookingCodes.containsKey(bookingCode)) {
									bookingCodeCount = mapBookingCodes.get(bookingCode);
									mapBookingCodes.put(bookingCode, new Integer(bookingCodeCount.intValue() + 1));
								} else {
									mapBookingCodes.put(bookingCode, new Integer(1));
								}
							} else {
								mapBookingCodes = new HashMap<String, Integer>();
								mapBookingCodes.put(bookingCode, new Integer(1));
								mapFlightSegments.put(fltSegmentId, mapBookingCodes);
							}
						}

						if (mapFlightSegIdWiseNested.containsKey(fltSegmentId)) {
							mapBCWiseNested = mapFlightSegIdWiseNested.get(fltSegmentId);
							if (mapBCWiseNested.containsKey(bookingCode) && mapBCWiseNested.get(bookingCode) != null
									&& !mapBCWiseNested.get(bookingCode).equals(ReservationInternalConstants.BCNested.YES)) {
								mapBCWiseNested.put(bookingCode, reservationPaxFareSegment.getBcNested());
							} else if (!mapBCWiseNested.containsKey(bookingCode)) {
								mapBCWiseNested.put(bookingCode, reservationPaxFareSegment.getBcNested());
							}
						} else {
							mapBCWiseNested = new HashMap<String, String>();
							mapBCWiseNested.put(bookingCode, reservationPaxFareSegment.getBcNested());
							mapFlightSegIdWiseNested.put(fltSegmentId, mapBCWiseNested);
						}
					}
				}
			}
		}

		// Building up the SegmentSeatDistsDTO and SeatDistribution
		Iterator<Integer> itFlightSegment = mapFlightSegments.keySet().iterator();
		Iterator<String> itMapBookingCodes;
		SegmentSeatDistsDTO segmentSeatDistsDTO;
		SeatDistribution seatDistribution;
		Collection<SegmentSeatDistsDTO> colSegmentSeatDistsDTO = new ArrayList<SegmentSeatDistsDTO>();

		while (itFlightSegment.hasNext()) {
			fltSegmentId = itFlightSegment.next();
			mapBookingCodes = mapFlightSegments.get(fltSegmentId);

			// Setting the flight segment information
			segmentSeatDistsDTO = new SegmentSeatDistsDTO();
			segmentSeatDistsDTO.setFlightSegId(fltSegmentId.intValue());

			itMapBookingCodes = mapBookingCodes.keySet().iterator();

			while (itMapBookingCodes.hasNext()) {
				bookingCode = itMapBookingCodes.next();

				// If it's an infant
				// Remember infant won't have any booking code
				if (bookingCode.equals("")) {
					bookingCodeCount = mapBookingCodes.get("");
					segmentSeatDistsDTO.setNoOfInfantSeats(bookingCodeCount.intValue());
				}
				// Adult, Parent booking code
				else {
					seatDistribution = new SeatDistribution();
					seatDistribution.setBookingClassCode(bookingCode);
					bookingCodeCount = mapBookingCodes.get(bookingCode);
					seatDistribution.setNoOfSeats(bookingCodeCount.intValue());

					mapBCWiseNested = mapFlightSegIdWiseNested.get(fltSegmentId);

					String isBCNested = mapBCWiseNested.get(bookingCode);
					if (isBCNested != null && isBCNested.equals(ReservationInternalConstants.BCNested.YES)) {
						seatDistribution.setNestedSeats(true);
					}

					segmentSeatDistsDTO.addSeatDistribution(seatDistribution);
				}
			}

			colSegmentSeatDistsDTO.add(segmentSeatDistsDTO);
		}

		ReservationBO.moveOnholdSeatsToConfirm(colSegmentSeatDistsDTO, exchangedSegInvIds);

		log.debug("Exit moveOnholdSeatsToConfirm");
	}

	/**
	 * Add a charge
	 * 
	 * @param reservation
	 * @param pnrPaxFareId
	 * @param chargeRateID
	 * @param amount
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, ReservationPaxOndCharge> addCharge(Reservation reservation, int pnrPaxFareId,
			Integer chargeRateID, BigDecimal amount, String chargeGroup, String taxAppliedCategory, CredentialsDTO credentialsDTO,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		log.debug("Inside addAdjustmentCharge");

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		Map<Integer, ReservationPaxOndCharge> perPaxOndCharge = new HashMap<Integer, ReservationPaxOndCharge>();
		Integer pnrPaxId = null;
		boolean searchAgain = true;

		while (searchAgain && itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (searchAgain && itReservationPaxFare.hasNext()) {
				reservationPaxFare = itReservationPaxFare.next();

				if (reservationPaxFare.getPnrPaxFareId().equals(new Integer(pnrPaxFareId))) {
					ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
							.captureReservationPaxOndCharge(amount, null, chargeRateID, chargeGroup, reservationPaxFare,
									taxAppliedCategory, credentialsDTO, false, null, null, null,
									chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

					if (!reservation.isInfantPaymentRecordedWithInfant()
							&& ReservationApiUtils.isInfantType(reservationPaxFare.getReservationPax())) {
						pnrPaxId = reservationPaxFare.getReservationPax().getParent().getPnrPaxId();
					} else {
						pnrPaxId = reservationPaxFare.getReservationPax().getPnrPaxId();
					}

					perPaxOndCharge.put(pnrPaxId, reservationPaxOndCharge);
					searchAgain = false;
				}
			}

		}

		if (perPaxOndCharge.size() == 0) {
			throw new ModuleException("airreservations.arg.paxNoLongerExist");
		}

		// Set reservation and passenger total amounts
		ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

		log.debug("Exit addAdjustmentCharge");
		return perPaxOndCharge;
	}

	/**
	 * Record modification
	 * 
	 * @param pnr
	 * @param reservationAudit
	 * @param userNotes
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public static void recordModification(String pnr, ReservationAudit reservationAudit, String userNotes,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside recordModification");

		ReservationAudit.createReservationAudit(reservationAudit, pnr, userNotes, credentialsDTO);

		// Get the auditor deleagate
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();

		// Saves the reservation
		auditorBD.audit(reservationAudit, reservationAudit.getContentMap());

		log.debug("Exit recordModification");
	}

	/**
	 * Record alert modifications
	 * 
	 * @param colReservationSegment
	 * @param credentialsDTO
	 * @param auditTemplateEnum
	 * @param mapPnrSegActions
	 * @param strReservationAlertType
	 * @throws ModuleException
	 */
	public static void recordAlertModifications(Collection<ReservationSegment> colReservationSegment,
			CredentialsDTO credentialsDTO, AuditTemplateEnum auditTemplateEnum, Map<Integer, String> mapPnrSegActions,
			String strReservationAlertType) throws ModuleException {
		log.debug("Inside recordAlertModifications");

		// Get the auditor deleagate
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		Collection<Integer> colPnrSegmentIds = new ArrayList<Integer>();
		ReservationSegment reservationSegment;
		ReservationAudit reservationAudit;
		ReservationSegmentDTO reservationSegmentDTO;

		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		Iterator<ReservationSegment> itReservationSegment = colReservationSegment.iterator();
		log.debug("Start Time " + Calendar.getInstance().getTime().toString());
		while (itReservationSegment.hasNext()) {
			reservationSegment = itReservationSegment.next();
			colPnrSegmentIds.add(reservationSegment.getPnrSegId());
		}

		// Get the segment information
		Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationSegmentDAO
				.getSegmentInformation(colPnrSegmentIds);

		for (ReservationSegment reservationSegment2 : colReservationSegment) {
			reservationSegment = reservationSegment2;

			reservationAudit = new ReservationAudit();
			ReservationAudit.createReservationAudit(reservationAudit, reservationSegment.getReservation().getPnr(), null,
					credentialsDTO);
			reservationAudit.setModificationType(auditTemplateEnum.getCode());

			reservationSegmentDTO = getReservationSegmentDTO(colReservationSegmentDTO, reservationSegment.getPnrSegId());

			// If cleared an alert
			if (auditTemplateEnum.equals(AuditTemplateEnum.CLEARED_ALERT)) {
				StringBuffer changes = new StringBuffer("");

				changes.append(" Alert flag is removed for ");
				changes.append(" Flight No : " + reservationSegmentDTO.getFlightNo());
				changes.append(" Segment Code : " + reservationSegmentDTO.getSegmentCode());
				changes.append(" Departure Date : " + reservationSegmentDTO.getDepartureStringDate("E, dd MMM yyyy HH:mm:ss"));
				changes.append(" Arrival Date : " + reservationSegmentDTO.getArrivalStringDate("E, dd MMM yyyy HH:mm:ss"));

				if (mapPnrSegActions != null) {
					String actionTaken = BeanUtils.nullHandler(mapPnrSegActions.get(reservationSegment.getPnrSegId()));
					changes.append(" " + actionTaken + " ");
					// Inorder to support JIRA THINAIR-275
					reservationAudit.setUserNote(actionTaken);
				}

				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ClearedAlert.ALERT_DETAILS, changes.toString());
			}
			// If created an alert
			else if (auditTemplateEnum.equals(AuditTemplateEnum.CREATED_ALERT)) {
				StringBuffer changes = new StringBuffer("");

				changes.append(" Alert flag is set for ");
				changes.append(" Flight No : " + reservationSegmentDTO.getFlightNo());
				changes.append(" Segment Code : " + reservationSegmentDTO.getSegmentCode());
				changes.append(" Departure Date : " + reservationSegmentDTO.getDepartureStringDate("E, dd MMM yyyy HH:mm:ss"));
				changes.append(" Arrival Date : " + reservationSegmentDTO.getArrivalStringDate("E, dd MMM yyyy HH:mm:ss"));

				// If alert types are available
				if (strReservationAlertType != null) {
					// Due to flight cancellation
					if (ReservationInternalConstants.ReservationAlertTypes.CANCEL_FLIGHT.equals(strReservationAlertType)) {
						changes.append(" Type : " + ReservationTemplateUtils.FLIGHT_CANCELLATION);
					}
					// Due to Reschedule flight
					else if (ReservationInternalConstants.ReservationAlertTypes.RESCHEDULE_FLIGHT
							.equals(strReservationAlertType)) {
						changes.append(" Type : " + ReservationTemplateUtils.RESCHEDULE_FLIGHT);
					}
				}
				
				if (mapPnrSegActions != null) {//AEROMART-3386
					String actionTaken = BeanUtils.nullHandler(mapPnrSegActions.get(reservationSegment.getPnrSegId()));
					changes.append(" " + actionTaken + " ");
					reservationAudit.setUserNote(actionTaken);
				}

				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ClearedAlert.ALERT_DETAILS, changes.toString());
			}
			reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
			// Saves the reservation audit
			auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
		}
		log.debug("End Time " + Calendar.getInstance().getTime().toString());
		log.debug("Exit recordAlertModifications");
	}

	/**
	 * Return the reservation segment data transfer information
	 * 
	 * @param colReservationSegmentDTO
	 * @param pnrSegId
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationSegmentDTO getReservationSegmentDTO(Collection<ReservationSegmentDTO> colReservationSegmentDTO,
			Integer pnrSegId) throws ModuleException {
		Iterator<ReservationSegmentDTO> itReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO = null;

		while (itReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itReservationSegmentDTO.next();

			if (reservationSegmentDTO.getPnrSegId().equals(pnrSegId)) {
				break;
			}
		}

		if (reservationSegmentDTO == null) {
			throw new ModuleException("airreservations.arg.segNoLongerExist");
		}

		return reservationSegmentDTO;
	}

	public static BigDecimal getReservationTotalPaymentAmount(Collection<PaymentInfo> colPaymentInfo) {
		BigDecimal reservationTotalPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (colPaymentInfo != null) {
			for (PaymentInfo paymentInfo : colPaymentInfo) {
				reservationTotalPaymentAmount = AccelAeroCalculator.add(reservationTotalPaymentAmount,
						paymentInfo.getPayCurrencyDTO().getTotalPayCurrencyAmount());
			}
		}

		return reservationTotalPaymentAmount;

	}

	public static BigDecimal getTotalCardPayCurrencymentAmount(ReservationAssembler assembler) {

		BigDecimal totalCardPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<Integer, CardPaymentInfo> TnxIds = assembler.getTnxIds();
		if (TnxIds != null) {
			for (Integer temPaymentID : TnxIds.keySet()) {
				totalCardPayment = AccelAeroCalculator.add(totalCardPayment,
						TnxIds.get(temPaymentID).getPayCurrencyDTO().getTotalPayCurrencyAmount());
			}
		}

		return totalCardPayment;

	}

	/**
	 * Return payment information
	 * 
	 * @param assembler
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<PaymentInfo> getPaymentInfo(ReservationAssembler assembler, String pnr) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = assembler.getDummyReservation().getPassengers().iterator();
		ReservationPax reservationPax;
		PaymentAssembler pAssembler;
		IPayment iPayment;
		Collection<PaymentInfo> colPaymentInfo = new ArrayList<PaymentInfo>();

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (reservationPax.getPayment() != null) {
				iPayment = reservationPax.getPayment();
				pAssembler = (PaymentAssembler) iPayment;
				colPaymentInfo.addAll(pAssembler.getPayments());
			}
		}

		// Set the pnr number
		ReservationBO.setPnrNumber(colPaymentInfo, pnr);

		PayCurrencyRounderAdaptor.round(assembler);

		return colPaymentInfo;
	}

	/**
	 * Return payment information
	 * 
	 * @param assembler
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<PaymentInfo> getPaymentInfo(SegmentAssembler assembler, String pnr) throws ModuleException {
		Iterator<IPayment> itIPayment = assembler.getPassengerPaymentsMap().values().iterator();
		IPayment iPayment;
		PaymentAssembler pAssembler;
		Collection<PaymentInfo> colPaymentInfo = new ArrayList<PaymentInfo>();

		while (itIPayment.hasNext()) {
			iPayment = itIPayment.next();
			pAssembler = (PaymentAssembler) iPayment;

			colPaymentInfo.addAll(pAssembler.getPayments());
		}

		// Set the pnr number
		ReservationBO.setPnrNumber(colPaymentInfo, pnr);

		PayCurrencyRounderAdaptor.round(assembler);

		return colPaymentInfo;
	}

	public static Collection<PaymentInfo> getPaymentInfo(Collection<IPayment> payments, String pnr) throws ModuleException {
		Iterator<IPayment> itIPayment = payments.iterator();
		IPayment iPayment;
		PaymentAssembler pAssembler;
		Collection<PaymentInfo> colPaymentInfo = new ArrayList<PaymentInfo>();

		while (itIPayment.hasNext()) {
			iPayment = itIPayment.next();
			pAssembler = (PaymentAssembler) iPayment;

			colPaymentInfo.addAll(pAssembler.getPayments());
		}

		// Set the pnr number
		ReservationBO.setPnrNumber(colPaymentInfo, pnr);

		PayCurrencyRounderAdaptor.round(payments);

		return colPaymentInfo;
	}

	/**
	 * Return payment information
	 * 
	 * @param assembler
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static Collection<PaymentInfo> getPaymentInfo(PassengerAssembler assembler, String pnr) throws ModuleException {
		Iterator<IPayment> itIPayment = assembler.getPassengerPaymentsMap().values().iterator();
		IPayment iPayment;
		PaymentAssembler pAssembler;
		Collection<PaymentInfo> colPaymentInfo = new ArrayList<PaymentInfo>();

		while (itIPayment.hasNext()) {
			iPayment = itIPayment.next();
			pAssembler = (PaymentAssembler) iPayment;

			colPaymentInfo.addAll(pAssembler.getPayments());
		}

		// Set the pnr number
		ReservationBO.setPnrNumber(colPaymentInfo, pnr);

		PayCurrencyRounderAdaptor.round(assembler);

		return colPaymentInfo;
	}

	/**
	 * Returns the refund information
	 * 
	 * @param assembler
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static Collection<PaymentInfo> getRefundInfo(PassengerAssembler assembler, String pnr) throws ModuleException {
		Iterator<IPayment> itIPayment = assembler.getPassengerPaymentsMap().values().iterator();
		IPayment iPayment;
		PaymentAssembler pAssembler;
		Collection<PaymentInfo> colPaymentInfo = new ArrayList<PaymentInfo>();

		while (itIPayment.hasNext()) {
			iPayment = itIPayment.next();
			pAssembler = (PaymentAssembler) iPayment;

			colPaymentInfo.addAll(pAssembler.getPayments());
		}

		// Set the pnr number
		ReservationBO.setPnrNumber(colPaymentInfo, pnr);

		PayCurrencyRounderAdaptor.roundForRefund(assembler);

		return colPaymentInfo;
	}

	/**
	 * Returns payment information
	 * 
	 * @param assembler
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public static void getPaymentInfo(PaymentAssembler assembler, String pnr) throws ModuleException {
		ReservationBO.setPnrNumber(assembler.getPayments(), pnr);

		PayCurrencyRounderAdaptor.round(assembler);
	}

	/**
	 * Set the pnr number
	 * 
	 * @param colPaymentInfo
	 * @param pnr
	 */
	private static void setPnrNumber(Collection<PaymentInfo> colPaymentInfo, String pnr) {
		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		CardPaymentInfo cardPaymentInfo;

		while (itColPaymentInfo.hasNext()) {
			paymentInfo = itColPaymentInfo.next();

			if (paymentInfo instanceof CardPaymentInfo) {
				cardPaymentInfo = (CardPaymentInfo) paymentInfo;
				cardPaymentInfo.setPnr(pnr);
			}
		}
	}

	/**
	 * Return the next pnr number
	 * 
	 * @param mapTnxIds
	 * @param rAssembler
	 * @return
	 * @throws ModuleException
	 */
	public static String getNextPnrNumber(Map<Integer, CardPaymentInfo> mapTnxIds, ReservationAssembler rAssembler)
			throws ModuleException {
		if (mapTnxIds == null || mapTnxIds.size() == 0) {
			boolean isGdsOrCsPnr = false;
			Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(rAssembler.getDummyReservation());
			if (ReservationApiUtils.isGdsReservation(rAssembler.getDummyReservation())) {
				isGdsOrCsPnr = true;
			} else if (csOCCarrirs != null && !csOCCarrirs.isEmpty()) {
				isGdsOrCsPnr = true;
			}
			return PNRNumberGeneratorProxy.getNewPNR(isGdsOrCsPnr, rAssembler.getDummyReservation().getPnr());
		} else {
			ReservationPaymentDAO reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
			Collection<TempPaymentTnx> colTempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(mapTnxIds.keySet());
			Collection<String> colPnrs = new HashSet<String>();

			for (TempPaymentTnx tnx : colTempPaymentTnx) {
				colPnrs.add(tnx.getPnr());
			}

			if (colPnrs.size() == 1) {
				return BeanUtils.getFirstElement(colPnrs).toString();
			}

			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}
	}

	/**
	 * Generate sequence
	 * 
	 * @return
	 */
	// private static String getETicketSQNumber() {
	// ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
	// return reservationDAO.getNextETicketSQID(null);
	// }

	/**
	 * Generate sequence
	 * 
	 * @param numberCount
	 * @return
	 */
	// private static String[] getETicketSQNumber(int numberCount, String strExtSeq) {
	// ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
	// String[] sequence = new String[numberCount];
	// for (int a = 0; a < numberCount; a++) {
	// sequence[a] = reservationDAO.getNextETicketSQID(strExtSeq);
	// }
	// return sequence;
	// }

	/**
	 * Get Number of pax to generate pax e ticket
	 * 
	 * @param pax
	 * @return
	 */
	// private static int getNoOfPax(Collection<ReservationPax> pax) {
	// int paxCount = 0;
	// ReservationPax reservationPax;
	// for (Iterator iter = pax.iterator(); iter.hasNext();) {
	// reservationPax = (ReservationPax) iter.next();
	// paxCount++;
	// }
	// return paxCount;
	// }

	public static Collection<ReservationPax> getConfirmedPassengers(Reservation reservation) {
		Collection<ReservationPax> passengers = new ArrayList<ReservationPax>();

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			for (ReservationPax reservationPax2 : reservation.getPassengers()) {
				ReservationPax reservationPax = reservationPax2;
				passengers.add(reservationPax);
			}
		}

		return passengers;
	}

	/**
	 * Adjust credit manually
	 * 
	 * @param chgTnxGen
	 */
	public static void adjustCreditManual(AdjustCreditBO adjustCreditBO, Reservation reservation, int pnrPaxFareId,
			Integer chargeRateID, BigDecimal amount, String chargeGroup, String userNotes, TrackInfoDTO trackInfoDTO,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		adjustCreditManual(adjustCreditBO, reservation, pnrPaxFareId, chargeRateID, amount, chargeGroup, null, userNotes,
				trackInfoDTO, credentialsDTO, chgTnxGen);
	}

	public static void adjustCreditManual(AdjustCreditBO adjustCreditBO, Reservation reservation, int pnrPaxFareId,
			Integer chargeRateID, BigDecimal amount, String chargeGroup, String taxAppliedCategory, String userNotes,
			TrackInfoDTO trackInfoDTO, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		// Adding charge entry
		Map<Integer, ReservationPaxOndCharge> perPaxOndCharge = ReservationBO
				.addCharge(reservation, pnrPaxFareId, chargeRateID, amount, chargeGroup, taxAppliedCategory, credentialsDTO,
						chgTnxGen);

		for (Entry<Integer, ReservationPaxOndCharge> entry : perPaxOndCharge.entrySet()) {
			Integer pnrPaxId = entry.getKey();
			ReservationPaxOndCharge reservationPaxOndCharge = entry.getValue();

			adjustCreditBO.addAdjustment(reservation.getPnr(), pnrPaxId, userNotes, reservationPaxOndCharge, credentialsDTO,
					reservation.isEnableTransactionGranularity());
		}

	}

	public static void sendSMSForOnHoldReservations(String pnr, String strMobileNo, CredentialsDTO credentialsDTO)
			throws ModuleException {
		// Narrow downing the condition in order to load the reservation
		// Should not load reservation with out any purpose.
		log.debug("Is SMS agent  : " + ((credentialsDTO != null) ? isSmsAgent(credentialsDTO.getAgentCode()) : null));
		log.debug("Is SMS number : " + ((strMobileNo != null) ? isSmsNumber(strMobileNo) : null));
		if (pnr != null && strMobileNo != null && credentialsDTO != null && isSmsAgent(credentialsDTO.getAgentCode())
				&& isSmsNumber(strMobileNo)) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setPnr(pnr);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// Nili Oct 12, 2011. Only onhold own bookings are allowed to send sms.
			if (reservation != null && ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())
					&& BeanUtils.nullHandler(reservation.getOriginatorPnr()).length() == 0) {
				strMobileNo = strMobileNo.replace("-", "");
				ReservationSegmentDTO reservationSegmentDTO = ReservationCoreUtils
						.getFirstConfirmedSegment((List<ReservationSegmentDTO>) reservation.getSegmentsView());

				String firstDepartureAirPort = reservationSegmentDTO != null ? reservationSegmentDTO.getOrigin() : "";

				String strFirstCNFSegDepDate = "";

				if (reservationSegmentDTO != null) {
					strFirstCNFSegDepDate = reservationSegmentDTO.getDepartureStringDate("ddMM");
				}

				sendSMS(strMobileNo, pnr, strFirstCNFSegDepDate, firstDepartureAirPort, reservation);
			}
		}
	}

	/**
	 * Sends SMS
	 * 
	 * @param strMobileNo
	 * @param pnr
	 * @param strFirstCNFSegDepDate
	 * @param reservation
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendSMS(String strMobileNo, String pnr, String strFirstCNFSegDepDate,String firstDepartureAirPort, Reservation reservation) {
		UserMessaging userMsg = new UserMessaging();
		userMsg.setToAddres(strMobileNo);

		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		messageList.add(userMsg);

		HashMap map = new HashMap();
		map.put("user", userMsg);
		map.put("pnr", pnr);
		map.put("firstDeparture", strFirstCNFSegDepDate);
		map.put("releaseDate", reservation.getReleaseTimeStampString("dd.MMM.yyyy @ HH:mm", true));
		map.put("title", BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getTitle()));
		map.put("firstName", BeanUtils.getFirst20Characters(reservation.getContactInfo().getFirstName(), 20));

		if (StringUtils.isNoneEmpty(firstDepartureAirPort)) {
			ZuluLocalTimeConversionHelper timeConversionHelper = new ZuluLocalTimeConversionHelper(ReservationModuleUtils.getAirportBD());
			Date localTime;
			try {
				localTime = timeConversionHelper.getLocalDateTime(firstDepartureAirPort, reservation.getZuluReleaseTimeStamp());
				String datetime = BeanUtils.parseDateFormat(localTime, "dd.MMM.yyyy HH:mm");
				String[] dateAndTime = datetime.split("\\s");//splits the string based on whitespase
				map.put("releaseTime", dateAndTime[1]);
				map.put("releaseDate", dateAndTime[0]);
			} catch (ModuleException e) {
				log.debug("Date parsing exception", e);
			}
		}

		Topic topic = new Topic();
		topic.setTopicParams(map);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.ONHOLD_SMS_TEMPLATE);

		String language = reservation.getContactInfo().getPreferredLanguage();
		if (language != null && (!language.trim().equals(""))) {
			topic.setLocale(new Locale(language));
		} else {
			topic.setLocale(Locale.ENGLISH);
		}

		MessageProfile msgProfile = new MessageProfile();
		msgProfile.setUserMessagings(messageList);
		msgProfile.setTopic(topic);

		List<MessageProfile> profList = new ArrayList<MessageProfile>();
		profList.add(msgProfile);

		ReservationModuleUtils.getMessagingServiceBD().sendMessages(profList);
	}

	/**
	 * Method to check if the agent is available for sms list
	 * 
	 * @param agentCode
	 *            the owner agent
	 * @return bolean true | false
	 */
	private static boolean isSmsAgent(String agentCode) {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		Collection<String> colAgents = airReservationConfig.getSmsAgents();
		Collection<String> colExcludeAgents = airReservationConfig.getSmsExcludeAgents();
		String agent = "";
		Iterator<String> iter = null;
		Iterator<String> exIter = null;

		if (colAgents != null) {
			iter = colAgents.iterator();
			if (iter != null) {
				while (iter.hasNext()) {
					agent = iter.next();
					if (!BeanUtils.nullHandler(agent).equals("") && !BeanUtils.nullHandler(agentCode).equals("")
							&& agent.equals(agentCode)) {
						if (colExcludeAgents != null) {
							exIter = colExcludeAgents.iterator();
							if (iter != null) {
								while (exIter.hasNext()) {
									agent = exIter.next();
									if (!BeanUtils.nullHandler(agent).equals("") && !BeanUtils.nullHandler(agentCode).equals("")
											&& agent.equals(agentCode)) {
										return false;
									}
								}
							}
						}
						return true;
					}
				}
			}
		}
		return false;
	}

	private static boolean isSmsNumber(String strNumber) {
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		Collection<String> colIDDCode = airReservationConfig.getSmsIDDCode();
		Collection<String> colAreaCode = airReservationConfig.getSmsAreaCode();
		String[] numberArray = strNumber.split("-");
		if (numberArray.length > 2) {
			numberArray[0] = addZero(numberArray[0], 4);
			numberArray[1] = addZero(numberArray[1], 2);
			String code = "";
			Iterator<String> iter = null;
			Iterator<String> areaIter = null;

			if (colIDDCode != null) {
				iter = colIDDCode.iterator();
				if (iter != null) {
					while (iter.hasNext()) {
						code = iter.next();
						if (!BeanUtils.nullHandler(code).equals("") && !BeanUtils.nullHandler(numberArray[0]).equals("")
								&& code.equals(numberArray[0])) {

							if (colAreaCode != null) {
								areaIter = colAreaCode.iterator();
								if (iter != null) {
									while (areaIter.hasNext()) {
										code = areaIter.next();
										if (!BeanUtils.nullHandler(code).equals("")
												&& !BeanUtils.nullHandler(numberArray[1]).equals("")
												&& code.equals(numberArray[1])) {
											return true;
										}

									}
								}
							}
						}

					}
				}
			}
		}

		return false;
	}

	private static String addZero(String strNumber, int length) {
		String s = "0000" + strNumber;
		return s.substring(s.length() - length);
	}

	/**
	 * @param colCHARGES
	 * @return
	 * @throws ModuleException
	 */
	public static Map<CHARGE_ADJUSTMENTS, ChargeAdjustmentDTO> getQuoteAdjustment(Collection<CHARGE_ADJUSTMENTS> colCHARGES)
			throws ModuleException {

		Map<String, String> chargeAdjustmentMap = ReservationModuleUtils.getAirReservationConfig().getChargeAdjustmentMap();
		Map<CHARGE_ADJUSTMENTS, ChargeAdjustmentDTO> mapCharges = new HashMap<CHARGE_ADJUSTMENTS, ChargeAdjustmentDTO>();
		if (chargeAdjustmentMap != null) {
			ChargeBD chargeBD = ReservationModuleUtils.getChargeBD();
			BigDecimal fare = AccelAeroCalculator.getDefaultBigDecimalZero();
			FareSummaryDTO fareSummaryDTO;
			for (CHARGE_ADJUSTMENTS CHARGE : colCHARGES) {
				String chargeCode = BeanUtils.nullHandler(chargeAdjustmentMap.get(CHARGE.name()));

				fareSummaryDTO = new FareSummaryDTO();
				fareSummaryDTO.setAdultFare(fare.doubleValue());
				fareSummaryDTO.setChildFare(fare.doubleValue());
				fareSummaryDTO.setInfantFare(fare.doubleValue());
				fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
				fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);

				// specifically for pax name change, both ranges of effectiveDate and salesDate should contain current
				// zulu date
				QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, CalendarUtil.getCurrentSystemTimeInZulu(),
						fareSummaryDTO, null, true);
				if (quotedChargeDTO != null) {

					ChargeAdjustmentDTO ChgAjustmentDTO = new ChargeAdjustmentDTO();
					ReservationApiUtils.getBasicChgDTO(quotedChargeDTO.getChargeCode(), quotedChargeDTO.getChargeDescription(),
							quotedChargeDTO.getChargeRateId(), quotedChargeDTO.isChargeValueInPercentage(),
							AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getChargeValuePercentage()), ChgAjustmentDTO,
							quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint());
					ChgAjustmentDTO.setChargeAdjustment(CHARGE);
					ChgAjustmentDTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.ADJ);

					mapCharges.put(CHARGE, ChgAjustmentDTO);
				}
			}
		}
		return mapCharges;
	}

	/**
	 * Returns the Quoted External charges
	 * 
	 * @param colEXTERNAL_CHARGES
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static Map<EXTERNAL_CHARGES, ExternalChgDTO> getQuotedExternalCharges(Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES,
			String agentCode, Integer operationalMode) throws ModuleException {
		Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
		Map<EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();

		if (externalChargesMap != null) {
			BigDecimal fare = AccelAeroCalculator.getDefaultBigDecimalZero();
			ChargeBD chargeBD = ReservationModuleUtils.getChargeBD();
			TravelAgentBD travelAgentBD = ReservationModuleUtils.getTravelAgentBD();

			EXTERNAL_CHARGES externalChgType;
			FareSummaryDTO fareSummaryDTO;

			for (EXTERNAL_CHARGES external_CHARGES : colEXTERNAL_CHARGES) {
				externalChgType = external_CHARGES;

				String chargeCode = BeanUtils.nullHandler(externalChargesMap.get(externalChgType.name()));
				if (chargeCode.isEmpty() && externalChgType != EXTERNAL_CHARGES.SERVICE_TAX) {
					throw new ModuleException("airreservations.externalcharge.cannotLocateTheExternalCharges");
				}

				if (externalChgType == EXTERNAL_CHARGES.CREDIT_CARD) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					// check whether operational specific charge rates( make / modify ) are defined,
					// if not available : default charge rate is loaded

					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, CalendarUtil.getCurrentSystemTimeInZulu(),
							fareSummaryDTO, operationalMode, false);
					if (quotedChargeDTO == null
							&& (operationalMode != null && operationalMode.intValue() != ChargeRateOperationType.ANY)) {
						quotedChargeDTO = chargeBD.getCharge(chargeCode, CalendarUtil.getCurrentSystemTimeInZulu(),
								fareSummaryDTO, ChargeRateOperationType.ANY, false);
					}
					// even default charge rate is also not defined , throws exception.
					if (quotedChargeDTO == null) {
						throw new ModuleException("airreservations.externalcharge.chargerateNotFound");
					}

					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_RESERVATION) {
						ExternalChgDTO externalChgDTO = new ExternalChgDTO();
						updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
						mapExternalChgs.put(externalChgType, externalChgDTO);

					} else {
						throw new ModuleException("airreservations.externalcharge.canBeApplicableToPNROnly");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.HANDLING_CHARGE) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					// QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null);
					List<QuotedChargeDTO> quotedChargeDTOs = chargeBD.getCharges(chargeCode, null, fareSummaryDTO,
							operationalMode);
					if (quotedChargeDTOs.isEmpty()
							&& (operationalMode != null && operationalMode.intValue() != ChargeRateOperationType.ANY)) {
						quotedChargeDTOs = chargeBD.getCharges(chargeCode, CalendarUtil.getCurrentSystemTimeInZulu(),
								fareSummaryDTO, ChargeRateOperationType.ANY);
					}

					// since handling fee is not applicable when modifying bookings.
					if (operationalMode != null && operationalMode.intValue() == ChargeRateOperationType.MODIFY_ONLY
							&& (quotedChargeDTOs == null || (quotedChargeDTOs != null && quotedChargeDTOs.isEmpty()))) {
						continue;
					}

					ExternalChgDTO externalChgDTO = new ExternalChgDTO();
					externalChgDTO = ReservationApiUtils.getExternalChgDTO(quotedChargeDTOs, externalChgDTO, externalChgType);

					agentCode = BeanUtils.nullHandler(agentCode);
					// handling fee amount (defined as charge rate in admin ) is overridden by the agent
					// level handling fee.
					if (agentCode.length() > 0) {
						Collection<String> colAgentCodes = new ArrayList<String>();
						colAgentCodes.add(agentCode);
						Collection<Agent> colAgent = travelAgentBD.getAgents(colAgentCodes);

						Agent agent = BeanUtils.getFirstElement(colAgent);
						// has setup oneway and return charge rates
						if (externalChgDTO.getChargeRates() != null && externalChgDTO.getChargeRates().size() == 1
								&& externalChgDTO.getJourneyType() == ChargeRateJourneyType.ANY) {
							// oneway rate
							ChargeRateDTO chgRateDTOOneway = new ChargeRateDTO(ChargeRateJourneyType.ONEWAY);
							chgRateDTOOneway.setChgRateId(externalChgDTO.getChgRateId());
							externalChgDTO.getChargeRates().add(chgRateDTOOneway);

							// return rate
							ChargeRateDTO chgRateDTOReturn = new ChargeRateDTO(ChargeRateJourneyType.RETURN);
							chgRateDTOReturn.setChgRateId(externalChgDTO.getChgRateId());
							externalChgDTO.getChargeRates().add(chgRateDTOReturn);

							ReservationApiUtils.updateHandlingFeeFromAgent(agent, externalChgDTO);

						} else if (externalChgDTO.getChargeRates() != null) {
							ReservationApiUtils.updateHandlingFeeFromAgent(agent, externalChgDTO);
						}

					}

					mapExternalChgs.put(externalChgType, externalChgDTO);

				} else if (externalChgType == EXTERNAL_CHARGES.NO_SHORE) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.noshow.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new ExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.noshore.canBeApplicableToAllPassengers");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.REFUND) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.refund.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new ExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.refund.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.SEAT_MAP) {
					QuotedChargeDTO quotedChargeDTO = null;
					if (seatQuotedChargeDTO == null) {
						fareSummaryDTO = getDummyFareSummaryDTO();
						seatQuotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					}

					quotedChargeDTO = seatQuotedChargeDTO.clone();

					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.seatmap.percentageValueNotSupported");
						} else {
							SMExternalChgDTO externalChgDTO = new SMExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.seatmap.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.INSURANCE) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.insurance.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new ExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.insurance.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.MEAL) {
					QuotedChargeDTO quotedChargeDTO = null;
					if (mealQuotedChargeDTO == null) {
						fareSummaryDTO = getDummyFareSummaryDTO();
						mealQuotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					}

					quotedChargeDTO = mealQuotedChargeDTO.clone();

					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.seatmap.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new MealExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.seatmap.canBeApplicableToAdultsAndChildren");
					}
				} // for baggage charges
				else if (externalChgType == EXTERNAL_CHARGES.BAGGAGE) {
					QuotedChargeDTO quotedChargeDTO = null;
					if (baggageQuotedChargeDTO == null) {
						fareSummaryDTO = getDummyFareSummaryDTO();
						baggageQuotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					}

					quotedChargeDTO = baggageQuotedChargeDTO.clone();

					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.seatmap.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new BaggageExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.seatmap.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO != null && quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException(
									"airreservations.externalcharge.inflightservice.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new SSRExternalChargeDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.inflightservice.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO != null && quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.halaservice.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new SSRExternalChargeDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.halaservice.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.AIRPORT_TRANSFER) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO != null && quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.halaservice.percentageValueNotSupported"); // FIXME
						} else {
							ExternalChgDTO externalChgDTO = new AirportTransferExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.halaservice.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.AUTOMATIC_CHECKIN) {
					QuotedChargeDTO quotedChargeDTO = null;
					if (autoCheckinQuotedChargeDTO == null) {
						fareSummaryDTO = getDummyFareSummaryDTO();
						autoCheckinQuotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					}

					quotedChargeDTO = autoCheckinQuotedChargeDTO.clone();
					if (quotedChargeDTO != null && quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.autoCheckin.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new AutoCheckinExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.autoCheckin.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.REF_ADJ) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException(
									"airreservations.externalcharge.refundable.adjustment.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new ExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.refundable.adjustment.canBeApplicableToAllPassengers");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.NON_REF_ADJ) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException(
									"airreservations.externalcharge.non.refundable.adjustment.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new ExternalChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.non.refundable.adjustment.canBeApplicableToAllPassengers");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.FLEXI_CHARGES) {
					// Flexi charges
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
						ExternalChgDTO externalChgDTO = new FlexiExternalChgDTO();
						updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
						mapExternalChgs.put(externalChgType, externalChgDTO);
					} else {
						throw new ModuleException("airreservations.flexicharge.canBeApplicableToAllPassengers");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.PROMOTION_REWARD) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
						ExternalChgDTO externalChgDTO = new ExternalChgDTO();
						updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
						mapExternalChgs.put(externalChgType, externalChgDTO);
					} else {
						throw new ModuleException("airreservations.flexicharge.canBeApplicableToAllPassengers");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
						ExternalChgDTO externalChgDTO = new ExternalChgDTO();
						updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
						mapExternalChgs.put(externalChgType, externalChgDTO);
					} else {
						throw new ModuleException("airreservations.flexicharge.canBeApplicableToAllPassengers");
					}

				} else if (externalChgType == EXTERNAL_CHARGES.JN_ANCI || externalChgType == EXTERNAL_CHARGES.JN_OTHER) {
					fareSummaryDTO = new FareSummaryDTO();
					fareSummaryDTO.setAdultFare(fare.doubleValue());
					fareSummaryDTO.setChildFare(fare.doubleValue());
					fareSummaryDTO.setInfantFare(fare.doubleValue());
					fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
					fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);

					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);

					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_OND
							&& quotedChargeDTO.isChargeValueInPercentage()) {
						ServiceTaxExtChgDTO externalChgDTO = new ServiceTaxExtChgDTO();
						ReservationApiUtils.getExternalChgDTO(quotedChargeDTO.getChargeCode(),
								quotedChargeDTO.getChargeDescription(), quotedChargeDTO.getChargeRateId(), externalChgType,
								quotedChargeDTO.isChargeValueInPercentage(),
								new BigDecimal(Double.toString(quotedChargeDTO.getChargeValuePercentage())), externalChgDTO,
								quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint());
						mapExternalChgs.put(externalChgType, externalChgDTO);
					} else {
						throw new ModuleException("airreservations.externalcharge.chargerateNotFound");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.JN_TAX) {
					fareSummaryDTO = new FareSummaryDTO();
					fareSummaryDTO.setAdultFare(fare.doubleValue());
					fareSummaryDTO.setChildFare(fare.doubleValue());
					fareSummaryDTO.setInfantFare(fare.doubleValue());
					fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
					fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);

					List<QuotedChargeDTO> quotedChargeDTOs = chargeBD.getCharges(chargeCode, null, fareSummaryDTO, null);

					if (!quotedChargeDTOs.isEmpty() && quotedChargeDTOs.get(0).getApplicableTo() == Charge.APPLICABLE_TO_OND
							&& quotedChargeDTOs.get(0).isChargeValueInPercentage()) {
						ServiceTaxExtChgDTO externalChgDTO = new ServiceTaxExtChgDTO();
						ReservationApiUtils.getExternalChgDTO(quotedChargeDTOs, externalChgDTO, externalChgType);

						mapExternalChgs.put(externalChgType, externalChgDTO);
					} else {
						throw new ModuleException("airreservations.externalcharge.chargerateNotFound");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.ANCI_PENALTY) {
					fareSummaryDTO = getDummyFareSummaryDTO();
					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, null, fareSummaryDTO, null, false);
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						if (quotedChargeDTO.isChargeValueInPercentage()) {
							throw new ModuleException("airreservations.externalcharge.anciPenalty.percentageValueNotSupported");
						} else {
							ExternalChgDTO externalChgDTO = new ServicePenaltyExtChgDTO();
							updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
							mapExternalChgs.put(externalChgType, externalChgDTO);
						}
					} else {
						throw new ModuleException("airreservations.anciPenalty.canBeApplicableToAdultsAndChildren");
					}
				} else if (externalChgType == EXTERNAL_CHARGES.SERVICE_TAX) {
					QuotedChargeDTO quotedChargeDTO = null;

					List<String> chargeCodes = chargeBD.getAllActiveServiceTaxChargeCodes();
					ServiceTaxExtCharges serviceTaxExtCharges = new ServiceTaxExtCharges();
					serviceTaxExtCharges.setChgGrpCode(ReservationInternalConstants.ChargeGroup.TAX);
					serviceTaxExtCharges.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);

					if (chargeCodes != null && !chargeCodes.isEmpty()) {
						for (String serviceChargeCode : chargeCodes) {
							fareSummaryDTO = getDummyFareSummaryDTO();
							quotedChargeDTO = chargeBD.getCharge(serviceChargeCode, null, fareSummaryDTO, null, false);
							if (quotedChargeDTO != null) {
								ServiceTaxExtChgDTO externalChgDTO = new ServiceTaxExtChgDTO();
								updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
								serviceTaxExtCharges.addServiceTaxes(externalChgDTO);

							}

						}
					}

					mapExternalChgs.put(externalChgType, serviceTaxExtCharges);

				} else if (externalChgType == EXTERNAL_CHARGES.BSP_FEE) {
					fareSummaryDTO = getDummyFareSummaryDTO();

					QuotedChargeDTO quotedChargeDTO = chargeBD.getCharge(chargeCode, CalendarUtil.getCurrentSystemTimeInZulu(),
							fareSummaryDTO, operationalMode, false);
					if (quotedChargeDTO == null
							&& (operationalMode != null && operationalMode.intValue() != ChargeRateOperationType.ANY)) {
						quotedChargeDTO = chargeBD.getCharge(chargeCode, CalendarUtil.getCurrentSystemTimeInZulu(),
								fareSummaryDTO, ChargeRateOperationType.ANY, false);
					}
					// even default charge rate is also not defined , throws exception.
					if (quotedChargeDTO == null) {
						throw new ModuleException("airreservations.externalcharge.chargerateNotFound");
					}

					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
						ExternalChgDTO externalChgDTO = new ExternalChgDTO();
						updateExternalChargeDTO(quotedChargeDTO, externalChgType, externalChgDTO);
						mapExternalChgs.put(externalChgType, externalChgDTO);

					} else {
						throw new ModuleException("airreservations.externalcharge.canBeApplicableToAllPassengers");
					}
				}

			}
		}

		return mapExternalChgs;

	}

	private static void updateExternalChargeDTO(QuotedChargeDTO quotedChargeDTO, EXTERNAL_CHARGES externalChgType,
			ExternalChgDTO externalChgDTO) throws ModuleException {
		ReservationApiUtils.getExternalChgDTO(quotedChargeDTO.getChargeCode(), quotedChargeDTO.getChargeDescription(),
				quotedChargeDTO.getChargeRateId(), externalChgType, quotedChargeDTO.isChargeValueInPercentage(),
				AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getChargeValuePercentage()), externalChgDTO,
				quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint());
	}

	private static FareSummaryDTO getDummyFareSummaryDTO() {
		BigDecimal fare = AccelAeroCalculator.getDefaultBigDecimalZero();
		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
		fareSummaryDTO.setAdultFare(fare.doubleValue());
		fareSummaryDTO.setChildFare(fare.doubleValue());
		fareSummaryDTO.setInfantFare(fare.doubleValue());
		fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		return fareSummaryDTO;
	}

	/**
	 * Returns the IP number for the given IP address
	 * 
	 * @param ip
	 * @return
	 */
	private static long getReservationOriginIPAddress(String ip) {
		ip = BeanUtils.nullHandler(ip);
		if (ip.length() > 0) {
			String[] splitIP = ip.split("\\.");

			try {
				return (Long.parseLong(splitIP[0]) * 16777216) + (Long.parseLong(splitIP[1]) * 65536)
						+ (Long.parseLong(splitIP[2]) * 256) + (Long.parseLong(splitIP[3]));
			} catch (Exception e) {
				return 0;
			}
		}
		return 0;
	}

	public static void injectIPToCountry(ReservationAssembler rAssembler, TrackInfoDTO trackInfoDTO) {
		if (trackInfoDTO != null) {
			String ipAddress = BeanUtils.nullHandler(trackInfoDTO.getIpAddress());

			if (ipAddress.length() > 0) {
				long ipNumber = ReservationBO.getReservationOriginIPAddress(ipAddress);
				trackInfoDTO.setOriginIPNumber(ipNumber);
			}
		}
	}

	public static Map<Integer, Collection<ReservationTnx>> getPNRPaxPaymentsAndRefunds(Collection<Integer> colPnrPaxId,
			boolean loadOnlyPayments, boolean loadPayCurrInfo) throws ModuleException {
		Map<Integer, Collection<ReservationTnx>> mapTnxs;

		if (loadOnlyPayments) {
			List<Integer> nominalCodes = ReservationTnxNominalCode.getPaymentTypeNominalCodes();
			mapTnxs = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getPNRPaxRecords(colPnrPaxId, nominalCodes);
		} else {
			mapTnxs = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getPNRPaxPaymentsAndRefunds(colPnrPaxId);
		}

		if (loadPayCurrInfo) {
			Collection<ReservationTnx> colReservationTnx = CurrencyConvertorUtils.getAllReservationTnxs(mapTnxs.values());
			Collection<Integer> tnxIds = CurrencyConvertorUtils.getTnxIds(colReservationTnx);

			if (!tnxIds.isEmpty()) {
				Map<Integer, ReservationPaxTnxBreakdown> mapBreakDowns = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
						.getReservationPaxTnxBreakdown(tnxIds);
				ReservationPaxTnxBreakdown reservationPaxTnxBreakdown;

				for (ReservationTnx reservationTnx : colReservationTnx) {
					reservationPaxTnxBreakdown = mapBreakDowns.get(reservationTnx.getTnxId());
					reservationTnx.setReservationPaxTnxBreakdown(reservationPaxTnxBreakdown);
				}
			}
		}

		return mapTnxs;
	}

	/**
	 * Save Seat information
	 * 
	 * @param paxIdsWithPayments
	 * @param flgtSegmentMap
	 * @param reservation
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, String> saveSeats(Map<Integer, IPayment> paxIdsWithPayments,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, Reservation reservation, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Collection<SMExternalChgDTO> allSeatsInfo = new ArrayList<SMExternalChgDTO>();
		ReservationPax reservationPax;
		Map<Integer, LinkedList<Integer>> perPaxFlightSegmentMap;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)
					&& paxIdsWithPayments.keySet().contains(reservationPax.getPnrPaxId())) {
				// Sending the pnr passenger id and the IPayment information per passenger

				perPaxFlightSegmentMap = cloneFlightSegmentMap(flgtSegmentMap);
				trackSeatsInformation(reservationPax, allSeatsInfo, perPaxFlightSegmentMap,
						(PaymentAssembler) paxIdsWithPayments.get(reservationPax.getPnrPaxId()));
			}
		}

		Collection<PassengerSeating> passengerSeats = new ArrayList<PassengerSeating>();
		Map<Integer, String> flightSeatIdsAndPaxTypes = new HashMap<Integer, String>();

		for (SMExternalChgDTO smExternalChgDTO : allSeatsInfo) {
			PassengerSeating passengerSeat = new PassengerSeating();
			passengerSeat.setPnrPaxId(smExternalChgDTO.getPnrPaxId());
			passengerSeat.setChargeAmount(smExternalChgDTO.getAmount());

			if (smExternalChgDTO.getFlightSeatId() == null) {
				SeatMapBD seatMapBD = ReservationModuleUtils.getSeatMapBD();
				Integer flightAmSeatId = seatMapBD.getFlightAmSeatID(smExternalChgDTO.getFlightSegId(),
						smExternalChgDTO.getSeatNumber());
				smExternalChgDTO.setFlightSeatId(flightAmSeatId);
			}
			passengerSeat.setFlightSeatId(smExternalChgDTO.getFlightSeatId());
			passengerSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
			passengerSeat.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
			passengerSeat.setUserId(credentialsDTO.getUserId());
			passengerSeat.setPpfId(smExternalChgDTO.getPnrPaxFareId());
			passengerSeat.setPnrSegId(smExternalChgDTO.getPnrSegId());
			passengerSeat.setTimestamp(CalendarUtil.getCurrentSystemTimeInZulu());
			passengerSeat.setCustomerId(credentialsDTO.getCustomerId());

			passengerSeats.add(passengerSeat);
			flightSeatIdsAndPaxTypes.put(smExternalChgDTO.getFlightSeatId(), smExternalChgDTO.getPaxType());
		}

		if (passengerSeats.size() > 0) {
			ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO.saveOrUpdate(passengerSeats);
		}

		return flightSeatIdsAndPaxTypes.size() == 0 ? null : flightSeatIdsAndPaxTypes;
	}

	/**
	 * Save Meal information
	 * 
	 * @param paxIdsWithPayments
	 * @param flgtSegmentMap
	 * @param reservation
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static Collection[] saveMeals(Map<Integer, IPayment> paxIdsWithPayments,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, Reservation reservation, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Collection<MealExternalChgDTO> allmealInfo = new ArrayList<MealExternalChgDTO>();
		ReservationPax reservationPax;
		Map<Integer, LinkedList<Integer>> perPaxMealSegmentMap;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)
					&& paxIdsWithPayments.keySet().contains(reservationPax.getPnrPaxId())) {
				// Sending the pnr passenger id and the IPayment information per passenger

				perPaxMealSegmentMap = cloneFlightSegmentMap(flgtSegmentMap);
				trackMealsInformation(reservationPax, allmealInfo, perPaxMealSegmentMap,
						(PaymentAssembler) paxIdsWithPayments.get(reservationPax.getPnrPaxId()));
			}
		}

		Map<Integer, Integer> segWiseBundleMealTemplates = MealAdapter.getPnrSegMealTemplateIds(reservation);
		Collection<PaxMealTO> colPaxMealTO = new ArrayList<PaxMealTO>();
		Collection<PassengerMeal> passengerMeals = new ArrayList<PassengerMeal>();
		Collection<Integer> fltMealIds = new ArrayList<Integer>();
		PassengerMeal passengerMeal;
		for (MealExternalChgDTO mealExternalChgDTO : allmealInfo) {

			passengerMeal = new PassengerMeal();
			passengerMeal.setPnrPaxId(mealExternalChgDTO.getPnrPaxId());
			passengerMeal.setChargeAmount(mealExternalChgDTO.getAmount());

			if (mealExternalChgDTO.getFlightMealId() == null) {
				String cabinClass = ReservationApiUtils.getSelectedCabinClass(reservation.getSegments(),
						mealExternalChgDTO.getFlightSegId());
				String logicalCCCode = ReservationApiUtils.getSelectedLogicalCabinClass(reservation.getSegments(),
						mealExternalChgDTO.getFlightSegId());

				FlightMealDTO newlyCreatedMeal = null;
				if (mealExternalChgDTO.isAnciOffer()) {
					newlyCreatedMeal = createFlightMealChargeIfDoesntExist(mealExternalChgDTO, null);
				} else if (segWiseBundleMealTemplates.containsKey(mealExternalChgDTO.getPnrSegId())
						&& segWiseBundleMealTemplates.get(mealExternalChgDTO.getPnrSegId()) != null) {
					newlyCreatedMeal = createFlightMealChargeIfDoesntExist(mealExternalChgDTO,
							segWiseBundleMealTemplates.get(mealExternalChgDTO.getPnrSegId()));
				}

				FlightMealDTO flightMealDTO = newlyCreatedMeal == null
						? getFlightMealDTO(mealExternalChgDTO.getMealCode(), mealExternalChgDTO.getFlightSegId(), cabinClass,
								logicalCCCode, credentialsDTO.getMarketingBookingChannel())
						: newlyCreatedMeal;

				if (flightMealDTO == null) {
					flightMealDTO = createFlightMealChargeIfDoesntExist(mealExternalChgDTO, null);
				}

				if (flightMealDTO != null) {
					mealExternalChgDTO.setFlightMealId(flightMealDTO.getFlightMealId());
					mealExternalChgDTO.setMealId(flightMealDTO.getMealId());
				}
			}

			if (mealExternalChgDTO.getFlightMealId() != null && mealExternalChgDTO.getMealId() != null) {
				passengerMeal.setFlightMealId(mealExternalChgDTO.getFlightMealId());
				passengerMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.RESERVED);
				passengerMeal.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
				passengerMeal.setUserId(credentialsDTO.getUserId());
				passengerMeal.setPnrSegId(mealExternalChgDTO.getPnrSegId());
				passengerMeal.setTimestamp(CalendarUtil.getCurrentSystemTimeInZulu());
				passengerMeal.setCustomerId(credentialsDTO.getCustomerId());
				passengerMeal.setpPFID(mealExternalChgDTO.getPnrPaxFareId());
				passengerMeal.setMealId(mealExternalChgDTO.getMealId());

				passengerMeals.add(passengerMeal);
				fltMealIds.add(mealExternalChgDTO.getFlightMealId());
				PaxMealTO paxMealTO = new PaxMealTO();
				paxMealTO.setPnrPaxId(mealExternalChgDTO.getPnrPaxId());
				paxMealTO.setMealId(mealExternalChgDTO.getMealId());
				paxMealTO.setSelectedFlightMealId(mealExternalChgDTO.getFlightMealId());
				paxMealTO.setPnrSegId(mealExternalChgDTO.getPnrSegId());
				colPaxMealTO.add(paxMealTO);
			}

		} // end for

		if (passengerMeals.size() > 0) {
			ReservationDAOUtils.DAOInstance.MEAL_DAO.saveOrUpdate(passengerMeals);
		}
		// return fltMealIds.size() == 0 ? null : fltMealIds;
		return new Collection[] { colPaxMealTO, fltMealIds };
	}

	/**
	 * Save Airport Transfer information
	 * 
	 * @param paxIdsWithPayments
	 * @param flgtSegmentMap
	 * @param reservation
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static Collection[] saveAirportTransfers(Map<Integer, IPayment> paxIdsWithPayments,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, Reservation reservation, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Collection<AirportTransferExternalChgDTO> allAirportTransferInfo = new ArrayList<AirportTransferExternalChgDTO>();
		ReservationPax reservationPax;
		Map<Integer, LinkedList<Integer>> perPaxAirportTransferSegmentMap;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)
					&& paxIdsWithPayments.keySet().contains(reservationPax.getPnrPaxId())) {
				perPaxAirportTransferSegmentMap = cloneFlightSegmentMap(flgtSegmentMap);
				trackAirportTransferInformation(reservationPax, allAirportTransferInfo, perPaxAirportTransferSegmentMap,
						(PaymentAssembler) paxIdsWithPayments.get(reservationPax.getPnrPaxId()));
			}
		}

		Collection<ReservationPaxSegAirportTransfer> passengerAirportTransfers = new ArrayList<ReservationPaxSegAirportTransfer>();
		Collection<Integer> fltAirportTransferIds = new ArrayList<Integer>();
		ReservationPaxSegAirportTransfer passengerAirportTransfer;
		for (AirportTransferExternalChgDTO aptExternalChgDTO : allAirportTransferInfo) {

			passengerAirportTransfer = new ReservationPaxSegAirportTransfer();
			passengerAirportTransfer.setPnrPaxId(aptExternalChgDTO.getPnrPaxId());
			passengerAirportTransfer.setChargeAmount(aptExternalChgDTO.getAmount());

			passengerAirportTransfer.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
			passengerAirportTransfer.setUserId(credentialsDTO.getUserId());
			passengerAirportTransfer.setPnrSegId(aptExternalChgDTO.getPnrSegId());
			passengerAirportTransfer.setPpfId(aptExternalChgDTO.getPnrPaxFareId());
			passengerAirportTransfer.setAirportTransferId(aptExternalChgDTO.getAirportTransferId());
			passengerAirportTransfer.setAirportCode(aptExternalChgDTO.getAirportCode());
			fltAirportTransferIds.add(aptExternalChgDTO.getAirportTransferId());
			try {
				Date requestTimeStamp = CalendarUtil.getParsedTime(aptExternalChgDTO.getTransferDate(), CalendarUtil.PATTERN11);
				passengerAirportTransfer.setRequestTimeStamp(requestTimeStamp);
			} catch (ParseException e) {
				throw new ModuleException("airreservations.airport.tranfer.date.invalid");
			}
			passengerAirportTransfer.setAddress(aptExternalChgDTO.getTransferAddress());
			passengerAirportTransfer.setContactNo(aptExternalChgDTO.getTransferContact());
			passengerAirportTransfer.setPickupType(aptExternalChgDTO.getTransferType());
			passengerAirportTransfer.setStatus("RES");
			passengerAirportTransfer.setBookingTimestamp(new Date());
			passengerAirportTransfers.add(passengerAirportTransfer);
		}

		if (passengerAirportTransfers.size() > 0) {
			ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO.saveOrUpdate(passengerAirportTransfers);
		}

		return new Collection[] { passengerAirportTransfers, fltAirportTransferIds };
	}

	/**
	 * save auto checkin information
	 * 
	 * @param paxIdsWithPayments
	 * @param flgtSegmentMap
	 * @param reservation
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	public static Collection[] saveAutoCheckins(Map<Integer, IPayment> paxIdsWithPayments,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, Reservation reservation, CredentialsDTO credentialsDTO)
			throws ModuleException {

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Collection<AutoCheckinExternalChgDTO> allAutoCheckinInfo = new ArrayList<AutoCheckinExternalChgDTO>();
		ReservationPax reservationPax;
		Map<Integer, LinkedList<Integer>> perPaxAutoCheckinSegmentMap;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)
					&& paxIdsWithPayments.keySet().contains(reservationPax.getPnrPaxId())) {
				perPaxAutoCheckinSegmentMap = cloneFlightSegmentMap(flgtSegmentMap);
				trackAutoCheckinInformation(reservationPax, allAutoCheckinInfo, perPaxAutoCheckinSegmentMap,
						(PaymentAssembler) paxIdsWithPayments.get(reservationPax.getPnrPaxId()));
			} else if (ReservationApiUtils.isInfantType(reservationPax)
					&& paxIdsWithPayments.keySet().contains(reservationPax.getPnrPaxId())) {
				// Storing parent information for infant in-order to do check-in
				perPaxAutoCheckinSegmentMap = cloneFlightSegmentMap(flgtSegmentMap);
				trackAutoCheckinInformation(reservationPax, allAutoCheckinInfo, perPaxAutoCheckinSegmentMap,
						(PaymentAssembler) paxIdsWithPayments.get(reservationPax.getParent().getPnrPaxId()));
			}
		}
		Collection<ReservationPaxSegAutoCheckin> passengerAutoCheckins = new ArrayList<ReservationPaxSegAutoCheckin>();
		Collection<Integer> fltAutoCheckinIds = new ArrayList<Integer>();
		ReservationPaxSegAutoCheckin passengerAutoCheckin;
		ReservationPaxSegAutoCheckinSeat passengerAutoCheckinSeat;
		for (AutoCheckinExternalChgDTO autoCknExternalChgDTO : allAutoCheckinInfo) {

			passengerAutoCheckin = new ReservationPaxSegAutoCheckin();
			passengerAutoCheckin.setPnrPaxId(autoCknExternalChgDTO.getPnrPaxId());
			passengerAutoCheckin.setAmount(autoCknExternalChgDTO.getAmount());
			passengerAutoCheckin.setCustomerId(credentialsDTO.getCustomerId());
			passengerAutoCheckin.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
			passengerAutoCheckin.setUserId(credentialsDTO.getUserId());
			passengerAutoCheckin.setPnrSegId(autoCknExternalChgDTO.getPnrSegId());
			passengerAutoCheckin.setPpfId(autoCknExternalChgDTO.getPnrPaxFareId());
			passengerAutoCheckin.setAutoCheckinTemplateId(autoCknExternalChgDTO.getAutoCheckinId());
			fltAutoCheckinIds.add(autoCknExternalChgDTO.getAutoCheckinId());
			passengerAutoCheckin.setStatus(AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE);
			passengerAutoCheckin.setRequestTimeStamp(CalendarUtil.getCurrentSystemTimeInZulu());
			passengerAutoCheckin.setEmail(autoCknExternalChgDTO.getEmail());
			passengerAutoCheckin.setFlightAmSeatId(autoCknExternalChgDTO.getFlightSeatId());
			passengerAutoCheckin.setNoOfAttempts(AirinventoryCustomConstants.AutoCheckinConstants.DEFAULT_ATTEMPT);
			passengerAutoCheckin.setDcsCheckinStatus(AirinventoryCustomConstants.AutoCheckinConstants.DCS_STATUS_NS);
			if (autoCknExternalChgDTO.getSeatTypePreference() != null
					&& autoCknExternalChgDTO.getSeatTypePreference().length() > 0
					&& !autoCknExternalChgDTO.getSeatTypePreference().equalsIgnoreCase(
							AirinventoryCustomConstants.AutoCheckinConstants.SIT_TOGETHER)) {
				passengerAutoCheckinSeat = new ReservationPaxSegAutoCheckinSeat();
				passengerAutoCheckinSeat.setSeatTypePreference(autoCknExternalChgDTO.getSeatTypePreference());
				passengerAutoCheckinSeat.setReservationPaxSegAutoCheckin(passengerAutoCheckin);
				passengerAutoCheckin.setReservationPaxSegAutoCheckinSeat(passengerAutoCheckinSeat);
			}
			passengerAutoCheckins.add(passengerAutoCheckin);
		}

		if (passengerAutoCheckins.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO.saveOrUpdate(passengerAutoCheckins);
			ReservationDAOUtils.DAOInstance.RESERVATION_AUTO_CHECKIN_DAO.saveOrUpdate(setSitTogetherpnrPaxId(passengerAutoCheckins));
		}
		return new Collection[] { passengerAutoCheckins, fltAutoCheckinIds };
	}

	private static Collection<ReservationPaxSegAutoCheckin> setSitTogetherpnrPaxId(
			Collection<ReservationPaxSegAutoCheckin> passengerAutoCheckins) {
		Integer parentSitTogetherPnrPadid = 0;
		Optional<ReservationPaxSegAutoCheckin> parentPnrPax = passengerAutoCheckins
				.stream()
				.filter(passengerAutoCheckin -> (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat() != null)
						&& (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_WINDOW)
								|| passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
										.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_AISLE) || passengerAutoCheckin
								.getReservationPaxSegAutoCheckinSeat().getSeatTypePreference()
								.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.SEAT_MIDDLE))
						|| (passengerAutoCheckin.getFlightAmSeatId() != null)).findFirst();
		parentSitTogetherPnrPadid = parentPnrPax.isPresent() ? parentPnrPax.get().getPnrPaxId() : 0;
		for (ReservationPaxSegAutoCheckin passengerAutoCheckin : passengerAutoCheckins) {
			if (passengerAutoCheckin.getReservationPaxSegAutoCheckinSeat() == null
					&& passengerAutoCheckin.getFlightAmSeatId() == null && parentSitTogetherPnrPadid != 0
					&& passengerAutoCheckin.getAmount().compareTo(BigDecimal.ZERO) > 0) {
				passengerAutoCheckin.setSitTogetherpnrPaxId(parentSitTogetherPnrPadid);
			}
		}
		return passengerAutoCheckins;
	}

	static Collection<BaggageExternalChgDTO> getBaggageExternalChgDTOs(Map<Integer, IPayment> paxIdsWithPayments,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, Reservation reservation) throws ModuleException {
		Collection<BaggageExternalChgDTO> allbaggageInfo = new ArrayList<BaggageExternalChgDTO>();
		Map<Integer, LinkedList<Integer>> perPaxBaggageSegmentMap;

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (!ReservationApiUtils.isInfantType(reservationPax)
					&& paxIdsWithPayments.keySet().contains(reservationPax.getPnrPaxId())) {
				// Sending the pnr passenger id and the IPayment information per passenger

				perPaxBaggageSegmentMap = cloneFlightSegmentMap(flgtSegmentMap);
				trackBaggagesInformation(reservationPax, allbaggageInfo, perPaxBaggageSegmentMap,
						(PaymentAssembler) paxIdsWithPayments.get(reservationPax.getPnrPaxId()));
			}
		}
		return allbaggageInfo;
	}

	private static FlightMealDTO createFlightMealChargeIfDoesntExist(MealExternalChgDTO mealExternalChgDTO,
			Integer mealTemplateId) throws ModuleException {

		if (mealTemplateId == null) {
			mealTemplateId = mealExternalChgDTO.getOfferedTemplateID();
		}

		return ReservationModuleUtils.getMealBD().createFlightMealTemplateIfDoesntExist(mealTemplateId,
				mealExternalChgDTO.getFlightSegId(), mealExternalChgDTO.getMealCode());
	}

	private static FlightMealDTO getFlightMealDTO(String mealCode, Integer flightSegId, String cabinClass,
			String logicalCabinClass, int salesChannelCode) throws ModuleException {
		FlightMealDTO flightMealDTO = null;

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		flightSegIdWiseCos.put(flightSegId, new ClassOfServiceDTO(cabinClass, logicalCabinClass, null));

		Map<Integer, List<FlightMealDTO>> mealMap = ReservationModuleUtils.getMealBD().getMeals(flightSegIdWiseCos, null, false,
				null, salesChannelCode);
		List<FlightMealDTO> flightMealDTOs = mealMap.get(flightSegId);
		if (flightMealDTOs != null && flightMealDTOs.size() > 0) {
			for (FlightMealDTO mealDTO : flightMealDTOs) {
				if (mealDTO.getMealCode().equals(mealCode)) {
					flightMealDTO = mealDTO;
					break;
				}
			}
		}
		return flightMealDTO;
	}

	/**
	 * Clonse the flight segment map
	 * 
	 * @param originalFlgtSegmentMap
	 * @return
	 */
	private static Map<Integer, LinkedList<Integer>>
			cloneFlightSegmentMap(Map<Integer, LinkedList<Integer>> originalFlgtSegmentMap) {
		Map<Integer, LinkedList<Integer>> clonedFlightSegmentMap = new HashMap<Integer, LinkedList<Integer>>();
		LinkedList<Integer> originalPnrSegIds;
		LinkedList<Integer> clonedPnrSegIds;

		for (Integer flightSegmentId : originalFlgtSegmentMap.keySet()) {
			originalPnrSegIds = originalFlgtSegmentMap.get(flightSegmentId);

			clonedPnrSegIds = new LinkedList<Integer>();

			for (Integer pnrSegId : originalPnrSegIds) {
				clonedPnrSegIds.add(new Integer(pnrSegId));
			}

			clonedFlightSegmentMap.put(new Integer(flightSegmentId), clonedPnrSegIds);
		}

		return clonedFlightSegmentMap;
	}

	/**
	 * Tracks the seats information
	 * 
	 * @param reservationPax
	 * @param allSeatsInfo
	 * @param flgtSegmentMap
	 * @param paymentAssembler
	 * @throws ModuleException
	 */
	private static void trackSeatsInformation(ReservationPax reservationPax, Collection<SMExternalChgDTO> allSeatsInfo,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, PaymentAssembler paymentAssembler) throws ModuleException {
		Collection<ExternalChgDTO> seatCharges = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.SEAT_MAP);
		for (ExternalChgDTO externalChgDTO : seatCharges) {
			SMExternalChgDTO smExternalChgDTO = (SMExternalChgDTO) externalChgDTO;
			Integer pnrSegId = flgtSegmentMap.get(smExternalChgDTO.getFlightSegId()).pop();
			ReservationPaxFareSegment reservationPaxFareSegment = ReservationCoreUtils
					.getReservationPaxFareSegment(reservationPax.getPnrPaxFares(), smExternalChgDTO.getFlightSegId(), pnrSegId);
			smExternalChgDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			smExternalChgDTO.setPnrSegId(reservationPaxFareSegment.getSegment().getPnrSegId());
			smExternalChgDTO.setPnrPaxFareId(reservationPaxFareSegment.getReservationPaxFare().getPnrPaxFareId());
			smExternalChgDTO.setPaxType(SMUtil.getPaxTypeCode(reservationPax));

			allSeatsInfo.add(smExternalChgDTO);
		}
	}

	/**
	 * Tracks the meals information
	 * 
	 * @param reservationPax
	 * @param allSeatsInfo
	 * @param flgtSegmentMap
	 * @param paymentAssembler
	 * @throws ModuleException
	 */
	private static void trackMealsInformation(ReservationPax reservationPax, Collection<MealExternalChgDTO> allmealInfo,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, PaymentAssembler paymentAssembler) throws ModuleException {
		Collection<ExternalChgDTO> mealCharges = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.MEAL);
		ReservationPaxFareSegment reservationPaxFareSegment;
		MealExternalChgDTO mealExternalChgDTO;
		Integer pnrSegId;

		for (ExternalChgDTO externalChgDTO : mealCharges) {
			mealExternalChgDTO = (MealExternalChgDTO) externalChgDTO;
			pnrSegId = flgtSegmentMap.get(mealExternalChgDTO.getFlightSegId()).peek(); // to support multimeals

			reservationPaxFareSegment = ReservationCoreUtils.getReservationPaxFareSegment(reservationPax.getPnrPaxFares(),
					mealExternalChgDTO.getFlightSegId(), pnrSegId);

			mealExternalChgDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			mealExternalChgDTO.setPnrSegId(reservationPaxFareSegment.getSegment().getPnrSegId());
			mealExternalChgDTO.setPnrPaxFareId(reservationPaxFareSegment.getReservationPaxFare().getPnrPaxFareId());
			allmealInfo.add(mealExternalChgDTO);
		}
	}

	/**
	 * Tracks the airport transfer information
	 * 
	 * @param reservationPax
	 * @param allSeatsInfo
	 * @param flgtSegmentMap
	 * @param paymentAssembler
	 * @throws ModuleException
	 */
	private static void trackAirportTransferInformation(ReservationPax reservationPax,
			Collection<AirportTransferExternalChgDTO> allAirportTransferInfo, Map<Integer, LinkedList<Integer>> flgtSegmentMap,
			PaymentAssembler paymentAssembler) throws ModuleException {
		Collection<ExternalChgDTO> airportTransferCharges = paymentAssembler
				.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
		ReservationPaxFareSegment reservationPaxFareSegment;
		AirportTransferExternalChgDTO airportTransferExternalChgDTO;
		Integer pnrSegId;

		for (ExternalChgDTO externalChgDTO : airportTransferCharges) {
			airportTransferExternalChgDTO = (AirportTransferExternalChgDTO) externalChgDTO;
			pnrSegId = flgtSegmentMap.get(airportTransferExternalChgDTO.getFlightSegId()).peek(); // to support
																									// multimeals

			reservationPaxFareSegment = ReservationCoreUtils.getReservationPaxFareSegment(reservationPax.getPnrPaxFares(),
					airportTransferExternalChgDTO.getFlightSegId(), pnrSegId);

			airportTransferExternalChgDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			airportTransferExternalChgDTO.setPnrSegId(reservationPaxFareSegment.getSegment().getPnrSegId());
			airportTransferExternalChgDTO.setPnrPaxFareId(reservationPaxFareSegment.getReservationPaxFare().getPnrPaxFareId());
			allAirportTransferInfo.add(airportTransferExternalChgDTO);
		}
	}

	/**
	 * Tracks the baggage information
	 * 
	 * @param reservationPax
	 * @param allSeatsInfo
	 * @param flgtSegmentMap
	 * @param paymentAssembler
	 * @throws ModuleException
	 */
	private static void trackBaggagesInformation(ReservationPax reservationPax, Collection<BaggageExternalChgDTO> allbaggageInfo,
			Map<Integer, LinkedList<Integer>> flgtSegmentMap, PaymentAssembler paymentAssembler) throws ModuleException {

		Collection<ExternalChgDTO> baggageCharges = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.BAGGAGE);
		ReservationPaxFareSegment reservationPaxFareSegment;
		BaggageExternalChgDTO baggageExternalChgDTO;
		Integer pnrSegId;

		for (ExternalChgDTO externalChgDTO : baggageCharges) {
			baggageExternalChgDTO = (BaggageExternalChgDTO) externalChgDTO;
			pnrSegId = flgtSegmentMap.get(baggageExternalChgDTO.getFlightSegId()).pop();

			reservationPaxFareSegment = ReservationCoreUtils.getReservationPaxFareSegment(reservationPax.getPnrPaxFares(),
					baggageExternalChgDTO.getFlightSegId(), pnrSegId);

			baggageExternalChgDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			baggageExternalChgDTO.setPnrSegId(reservationPaxFareSegment.getSegment().getPnrSegId());
			baggageExternalChgDTO.setPnrPaxFareId(reservationPaxFareSegment.getReservationPaxFare().getPnrPaxFareId());

			allbaggageInfo.add(baggageExternalChgDTO);
		}
	}

	/**
	 * Tracks the auto checkin information
	 * 
	 * @param reservationPax
	 * @param allAutoCheckinInfo
	 * @param flgtSegmentMap
	 * @param paymentAssembler
	 * @throws ModuleException
	 */
	private static void trackAutoCheckinInformation(ReservationPax reservationPax,
			Collection<AutoCheckinExternalChgDTO> allAutoCheckinInfo, Map<Integer, LinkedList<Integer>> flgtSegmentMap,
			PaymentAssembler paymentAssembler) throws ModuleException {
		Collection<ExternalChgDTO> autoCheckinCharges = paymentAssembler
				.getPerPaxExternalCharges(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);

		for (ExternalChgDTO externalChgDTO : autoCheckinCharges) {
			AutoCheckinExternalChgDTO autoCknExternalChgDTO = (AutoCheckinExternalChgDTO) externalChgDTO.clone();
			Integer pnrSegId = flgtSegmentMap.get(autoCknExternalChgDTO.getFlightSegId()).pop();
			ReservationPaxFareSegment reservationPaxFareSegment = ReservationCoreUtils.getReservationPaxFareSegment(
					reservationPax.getPnrPaxFares(), autoCknExternalChgDTO.getFlightSegId(), pnrSegId);
			autoCknExternalChgDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			autoCknExternalChgDTO.setPnrSegId(reservationPaxFareSegment.getSegment().getPnrSegId());
			autoCknExternalChgDTO.setPnrPaxFareId(reservationPaxFareSegment.getReservationPaxFare().getPnrPaxFareId());
			/**
			 * checking weather the user purchased seat.
			 */
			if (autoCknExternalChgDTO.getSeatCode() != null) {
				getFlightAmSeatId(autoCknExternalChgDTO);
			}
			/**
			 * Amount should be 0 for infant
			 */
			if (ReservationApiUtils.isInfantType(reservationPax)) {
				autoCknExternalChgDTO.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
				autoCknExternalChgDTO.setSeatTypePreference(null);
			}

			allAutoCheckinInfo.add(autoCknExternalChgDTO);
		}
	}

	/**
	 * This is for getting flight seat id if the passenger purchased seat from seat ancillary.
	 * 
	 * @param autoCknExternalChgDTO
	 */
	private static void getFlightAmSeatId(AutoCheckinExternalChgDTO autoCknExternalChgDTO) {
		SeatMapBD seatMapBD = ReservationModuleUtils.getSeatMapBD();
		Integer flightAmSeatId = seatMapBD.getFlightAmSeatID(autoCknExternalChgDTO.getFlightSegId(),
				autoCknExternalChgDTO.getSeatCode());
		autoCknExternalChgDTO.setFlightSeatId(flightAmSeatId);
	}

	/**
	 * Tracks meal cancellations
	 * 
	 * @param pnrPaxFareIds
	 * @return
	 */
	public static Collection<Integer> trackMealCancellations(Collection<Integer> pnrPaxFareIds) {
		Collection<Integer> flightMeals = null;

		if (pnrPaxFareIds != null && pnrPaxFareIds.size() > 0) {
			MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
			Collection<PassengerMeal> passengerMeals = mealDAO.getFlightMealsForPnrPaxFare(pnrPaxFareIds);

			if (passengerMeals != null && passengerMeals.size() > 0) {
				flightMeals = new ArrayList<Integer>();
				for (PassengerMeal pm : passengerMeals) {
					flightMeals.add(pm.getFlightMealId());
					pm.setStatus(AirinventoryCustomConstants.FlightMealStatuses.PAX_CNX);
					pm.setTimestamp(CalendarUtil.getCurrentSystemTimeInZulu());
				}
				mealDAO.saveOrUpdate(passengerMeals);
			}
		}

		return flightMeals;
	}

	/**
	 * Tracks seat map cancellations
	 * 
	 * @param pnrPaxFareIds
	 * @return
	 */
	public static Collection<Integer> trackSeatMapCancellations(Collection<Integer> pnrPaxFareIds) {
		Collection<Integer> flightSeats = null;

		if (pnrPaxFareIds != null && pnrPaxFareIds.size() > 0) {
			SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;
			Collection<PassengerSeating> passengerSeating = seatMapDAO.getFlightSeatsForPnrPaxFare(pnrPaxFareIds);

			if (passengerSeating != null && passengerSeating.size() > 0) {
				flightSeats = new HashSet<Integer>();
				for (PassengerSeating ps : passengerSeating) {
					flightSeats.add(ps.getFlightSeatId());
				}
				seatMapDAO.deleteReservationSeats(passengerSeating);
			}
		}

		return flightSeats;
	}

	/**
	 * Expire Open Return Segments
	 * 
	 * @param executionDate
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 */
	public static void expireOpenReturnSegments(Date executionDate, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge) {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		Collection<OpenReturnOndTO> colOpenReturnOndTOs = reservationSegmentDAO.getOpenReturnSegments(executionDate);
		OpenReturnOndTO openReturnOndTO;
		LCCClientPnrModesDTO pnrModesDTO;
		Reservation reservation;
		Collection<Integer> pnrSegIds;

		for (OpenReturnOndTO openReturnOndTO2 : colOpenReturnOndTOs) {
			openReturnOndTO = openReturnOndTO2;

			try {
				pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(openReturnOndTO.getPnr());
				pnrModesDTO.setLoadFares(true);

				reservation = ReservationProxy.getReservation(pnrModesDTO);

				pnrSegIds = getOndPnrSegmentIds(reservation, openReturnOndTO.getOndGroupId());

				if (pnrSegIds != null && pnrSegIds.size() > 0) {
					if (log.isDebugEnabled()) {
						log.debug("Started Cancelling the Open Return Segments of [PNR:" + openReturnOndTO.getPnr()
								+ "][SEGMENTS:" + pnrSegIds + "]");
					}

					CustomChargesTO customChargesTO = new CustomChargesTO(customAdultCancelCharge, customChildCancelCharge,
							customInfantCancelCharge, null, null, null);

					ReservationModuleUtils.getSegmentBD().cancelSegments(reservation.getPnr(), pnrSegIds, customChargesTO,
							reservation.getVersion(), true, null, false, null, true);

					if (log.isDebugEnabled()) {
						log.debug("Completed Cancelling the Open Return Segments of [PNR:" + openReturnOndTO.getPnr()
								+ "][SEGMENTS:" + pnrSegIds + "]");
					}
				}
			} catch (Exception e) {
				log.info(" ######################################## ");
				log.info(" ERROR OCCURED WHILE CANCELLING THE OPEN RETURN SEGMENTS [PNR:" + openReturnOndTO.getPnr()
						+ "][OND_GROUP_ID:" + openReturnOndTO.getOndGroupId() + "] ");

				if (e instanceof ModuleException) {
					ModuleException moduleException = (ModuleException) e;
					log.info(" ERROR " + moduleException.getMessageString());
				} else {
					log.info(" ERROR " + e.getMessage());
				}

				log.info(" PLEASE CORRECT THIS MANUALLY INORDER TO CANCEL THE OPEN RETURN SEGMENTS ");
				log.info(" ######################################## ");
			}
		}
	}

	private static Collection<Integer> getOndPnrSegmentIds(Reservation reservation, int ondGroupId) {
		Collection<Integer> pnrSegIds = new HashSet<Integer>();
		ReservationSegment reservationSegment;

		for (ReservationSegment reservationSegment2 : reservation.getSegments()) {
			reservationSegment = reservationSegment2;

			if (reservationSegment.getOndGroupId() == ondGroupId) {
				pnrSegIds.add(reservationSegment.getPnrSegId());
			}

		}

		return pnrSegIds;
	}

	public static void expireOnholdReservations(Date toDate, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge) {
		PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;

		// Get the on hold pnrs
		Map<String, Long> mapPnrs = passengerDAO.getReservationPassengers(ReservationInternalConstants.ReservationStatus.ON_HOLD,
				toDate);

		if (mapPnrs != null && mapPnrs.size() > 0) {
			Long version;
			Collection<String> pnrCollection = new ArrayList<String>();
			for (String pnr : mapPnrs.keySet()) {
				version = mapPnrs.get(pnr);
				if (log.isDebugEnabled()) {
					log.debug(" Started cancellation for PNR " + pnr);
				}

				try {
					CustomChargesTO customChargesTO = new CustomChargesTO(customAdultCancelCharge, customChildCancelCharge,
							customInfantCancelCharge, null, null, null);

					ReservationModuleUtils.getReservationBD().cancelReservation(pnr, customChargesTO, version.longValue(), true,
							true, false, null, false, null, GDSInternalCodes.GDSNotifyAction.ONHOLD_RELEASE.getCode(), null);

				} catch (Exception e) {
					log.info(" ######################################## ");
					log.info(" ERROR occured while capturing credits from PNR " + pnr);
					if (e instanceof ModuleException) {
						ModuleException moduleException = (ModuleException) e;
						log.info(" ERROR " + moduleException.getMessageString());
					} else {
						log.info(" ERROR " + e.getMessage());
					}
					log.info(" PLEASE CORRECT THIS MANUALLY INORDER TO EXPIRE CREDITS ");
					log.info(" ######################################## ");
				}

				if (log.isDebugEnabled()) {
					log.debug(" Finished cancellation for PNR " + pnr);
				}

				pnrCollection.add(pnr);
				if (ReservationModuleUtils.getAirReservationConfig().getInventoryMismatchTrackingForOHDRelease()) {
					logInventoryMismatches(pnr);
				}
			}

			if (AppSysParamsUtil.isSendAgentEmailOnReservationModified()) {
				emailToAgentForCanceledPNR(pnrCollection);
			}
			
			// cancel LMS Blocked credit payments
			cancelLMSBlockedCredits(pnrCollection);
		}

		// Cancelling the LCC bookings. We can't cancel LCC bookings from AccelAero only.
		// We need to cancel that via the LCC in order to propagate the credit(s) correctly
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			Collection<String> lccPnrs = passengerDAO.getMarketingLccReservations(
					ReservationInternalConstants.ReservationStatus.ON_HOLD, toDate, AppSysParamsUtil.getDefaultCarrierCode());

			if (lccPnrs != null && lccPnrs.size() > 0) {

				for (String groupPNR : lccPnrs) {
					if (log.isDebugEnabled()) {
						log.debug(" Started cancellation for LCC GROUP PNR " + groupPNR);
					}

					try {
						LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
						pnrModesDTO.setGroupPNR(groupPNR);
						pnrModesDTO.setLoadFares(true);
						pnrModesDTO.setLoadLastUserNote(true);
						pnrModesDTO.setLoadLocalTimes(true);
						pnrModesDTO.setLoadOndChargesView(true);
						pnrModesDTO.setLoadPaxAvaBalance(true);
						pnrModesDTO.setLoadSegView(true);
						pnrModesDTO.setLoadSegViewBookingTypes(true);
						pnrModesDTO.setLoadSegViewFareCategoryTypes(true);

						// You may think why just loading the PNR. We can cancel it directly. You are wrong my friend.
						// If some one performs any operation on this LCC parallely while canceling this the reservation
						// will
						// go for an inconsistent state. Therefore we need to prevent that.
						LCCClientReservation lccClientReservation = ReservationModuleUtils.getLCCReservationBD()
								.searchReservationByPNR(pnrModesDTO, null, new TrackInfoDTO());

						ReservationModuleUtils.getLCCReservationBD()
								.cancelReservation(LCCClientResAlterModesTO.composeCancelReservationRequest(groupPNR, null, null,
										null, lccClientReservation.getVersion(), null), null);
					} catch (Exception e) {
						log.info(" ######################################## ");
						log.info(" ERROR occured while capturing credits from LCC GROUP PNR " + groupPNR);
						if (e instanceof ModuleException) {
							ModuleException moduleException = (ModuleException) e;
							log.info(" ERROR " + moduleException.getMessageString());
						} else {
							log.info(" ERROR " + e.getMessage());
						}
						log.info(" PLEASE CORRECT THIS MANUALLY INORDER TO EXPIRE CREDITS ");
						log.info(" ######################################## ");
					}

					if (log.isDebugEnabled()) {
						log.debug(" Finished cancellation for LCC GROUP PNR " + groupPNR);
					}
				}
			}
		}
	}

	private static void logInventoryMismatches(String pnr) {
		PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;
		if (pnr != null && pnr != "") {
			List<Map<String, String>> reservationData = passengerDAO.getInventoryDataForMismatchTracking(pnr);
			boolean hasMisMatchOccured = false;
			for (Map<String, String> data : reservationData) {
				int flightId = Integer.parseInt(data.get("fltId"));
				String segmentCode = data.get("segmentCode");
				String bookingCode = data.get("bookingCode");
				hasMisMatchOccured = passengerDAO.hasMismatchOccured(flightId, segmentCode, bookingCode);
				if (hasMisMatchOccured) {
					log.warn("MisMatchOccured while cancelling PNR: " + pnr + ". Booking class:" + bookingCode + ", Segment Code:"
							+ segmentCode + ", Flight ID:" + flightId);
				} else {
					log.warn("MismatchNotFound with PNR: " + pnr + ". Booking class:" + bookingCode + ", Segment Code:"
							+ segmentCode + ", Flight ID:" + flightId);
				}

			}
		}
	}

	public static Collection<FlexiExternalChgDTO> saveFlexiCharges(Map<Integer, IPayment> paxPaymentMap,
			Map<Integer, LinkedList<Integer>> flightSegmentMap, Reservation reservation, CredentialsDTO credentialsDTO)
			throws ModuleException {
		if (paxPaymentMap == null || flightSegmentMap == null) {
			throw new ModuleException("Error in saving flexicharges", "airreservations.arg.invalid.null");
		}

		Collection<FlexiExternalChgDTO> reservationFlexiCharges = new ArrayList<FlexiExternalChgDTO>();
		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			ReservationPax reservationPax = reservationPax2;
			if (!ReservationApiUtils.isInfantType(reservationPax)
					&& paxPaymentMap.keySet().contains(reservationPax.getPnrPaxId())) {
				Map<Integer, LinkedList<Integer>> cloneFlightSegmentMap = cloneFlightSegmentMap(flightSegmentMap);
				tracFlexiCharges(reservationPax, reservationFlexiCharges,
						(PaymentAssembler) paxPaymentMap.get(reservationPax.getPnrPaxId()), cloneFlightSegmentMap);
			}
		}

		Map<String, ReservationPaxOndFlexibility> paxOndFlexibilities = new HashMap<String, ReservationPaxOndFlexibility>();
		for (FlexiExternalChgDTO flexiExternalChgDTO : reservationFlexiCharges) {
			for (ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO : flexiExternalChgDTO
					.getReservationPaxOndFlexibilityDTOs()) {
				String mappingKey = reservationPaxOndFlexibilityDTO.getFlexibilityTypeId() + "_"
						+ reservationPaxOndFlexibilityDTO.getPpfId();
				if (paxOndFlexibilities.get(mappingKey) == null) {
					ReservationPaxOndFlexibility reservationPaxOndFlexibility = new ReservationPaxOndFlexibility();
					reservationPaxOndFlexibility.setAvailableCount(reservationPaxOndFlexibilityDTO.getAvailableCount());
					reservationPaxOndFlexibility.setUtilizedCount(reservationPaxOndFlexibilityDTO.getUtilizedCount());
					reservationPaxOndFlexibility.setFlexiRuleRateId(reservationPaxOndFlexibilityDTO.getFlexiRateId());
					reservationPaxOndFlexibility.setFlexiRuleTypeId(reservationPaxOndFlexibilityDTO.getFlexibilityTypeId());
					reservationPaxOndFlexibility.setPpfId(reservationPaxOndFlexibilityDTO.getPpfId());
					reservationPaxOndFlexibility.setStatus(reservationPaxOndFlexibilityDTO.getStatus());
					paxOndFlexibilities.put(mappingKey, reservationPaxOndFlexibility);
				}
			}
		}

		if (paxOndFlexibilities.size() > 0) {
			ReservationDAOUtils.DAOInstance.FLEXI_RULE_DAO.saveOrUpdate(paxOndFlexibilities.values());
		}

		return reservationFlexiCharges;
	}

	private static void tracFlexiCharges(ReservationPax reservationPax, Collection<FlexiExternalChgDTO> reservationFlexiCharges,
			PaymentAssembler paymentAssembler, Map<Integer, LinkedList<Integer>> flightSegmentMap) throws ModuleException {
		Collection<ExternalChgDTO> flexiCharges = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);
		for (Integer flightSegId : flightSegmentMap.keySet()) {
			Integer pnrSegId = flightSegmentMap.get(flightSegId).pop();
			for (ExternalChgDTO externalChgDTO : flexiCharges) {
				FlexiExternalChgDTO flexiExternalChgDTO = (FlexiExternalChgDTO) externalChgDTO.clone();
				// One pnr segment might have two separate flexibility rules
				if (flexiExternalChgDTO.getFlightSegId().intValue() == flightSegId.intValue()) {
					ReservationPaxFareSegment reservationPaxFareSegment = ReservationCoreUtils.getReservationPaxFareSegment(
							reservationPax.getPnrPaxFares(), flexiExternalChgDTO.getFlightSegId(), pnrSegId);
					reservationFlexiCharges.add(flexiExternalChgDTO);
					for (ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO : flexiExternalChgDTO
							.getReservationPaxOndFlexibilityDTOs()) {
						reservationPaxOndFlexibilityDTO
								.setPpfId(reservationPaxFareSegment.getReservationPaxFare().getPnrPaxFareId());
						if (reservationPaxOndFlexibilityDTO.getDescription() == null) {
							if (reservationPaxOndFlexibilityDTO.getFlexibilityTypeId() == 1) {
								reservationPaxOndFlexibilityDTO.setDescription("Modification");
							} else if (reservationPaxOndFlexibilityDTO.getFlexibilityTypeId() == 2) {
								reservationPaxOndFlexibilityDTO.setDescription("Cancellation");
							}
						}
					}
				}
			}
		}
	}

	public static String getTheCurrentETicket(String agentCode) {
		ETicketGenerator eTicketGenerator = new ETicketGenerator();
		String eticketNumber = eTicketGenerator.composeETicket(agentCode, ReservationBO.getCurrentETicketSQNumber(),
				getEtktFormat());

		return eticketNumber;
	}

	/**
	 * Returns etkt configMap
	 * 
	 * @return
	 */
	private static Map<String, String> getEtktFormat() {
		return config.getEtktConfMap();
	}

	private static String getCurrentETicketSQNumber() {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getCurrentETicketSQID(null);
	}

	public static String getCurrentTicketNoOnly(String agnCode) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getCurrentETicketSQID(null);
	}

	/**
	 * EuroCommerce Fraud Checking
	 * 
	 */
	public static void processCCFraudCheck(String pnr, Collection<OndFareDTO> ondFareDTOs, Collection<Integer> colTnxIds,
			CredentialsDTO credentialsDTO, Reservation reservation, TrackInfoDTO trackInfo,
			Collection<CardPaymentInfo> colCardPaymentInfo, Boolean enableCCFraudCheck, String reservationType)
			throws ModuleException {

		if (enableCCFraudCheck != null && enableCCFraudCheck.booleanValue()) {
			if (log.isDebugEnabled()) {
				log.debug("Inside processCCFraudCheck() to check CC with EUROCommerce Fraud Check WebService for PNR [" + pnr
						+ "]");
			}
			CardPaymentInfo cardPayInfo = null;
			Iterator<CardPaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
			while (itColCardPaymentInfo.hasNext()) {
				cardPayInfo = itColCardPaymentInfo.next();
				if (log.isDebugEnabled()) {
					log.debug("EUROCommerce Fraud Check for the payment for [" + cardPayInfo.getNoFirstDigits()
							+ "-xxxx-xxxx-xxxx-" + cardPayInfo.getNoLastDigits() + "]");
				}
				CCFraudCheckRequestDTOBuilder ccFraudBuilder = assembleCCFraud(colTnxIds, pnr, cardPayInfo, ondFareDTOs,
						reservation, credentialsDTO, trackInfo, reservationType);
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				CCFraudCheckResponseDTO result = ebiWebervices
						.validateCCPaymentForFraud(ccFraudBuilder.getCcFraudCheckRequestDTO());
				if (log.isDebugEnabled()) {
					log.debug("Result from EUROCommerce Fraud Check [status:" + result.getStatus() + "] [message="
							+ result.getMessage() + "] [ref=" + result.getRef() + "]");
				}
				ReservationModuleUtils.getReservationBD().updateTempPaymentWithFC(Integer.valueOf(ccFraudBuilder.getExternalId()),
						result);
				// stop the payment if the CC fails the fraud check.
				if (FraudStatus.DECLINED.equals(result.getStatus())) {
					if (log.isDebugEnabled()) {
						log.debug("After Failing EUROCommerce Fraud Check with Status [" + result.getStatus()
								+ "] Terminates Transaction");
					}
					throw new ModuleException("airreservations.payment.validation.failed");
				}
			}
		}
	}

	/**
	 * Populate the Fraud Request DTO
	 * 
	 * @param colTnxIds
	 * @param pnr
	 * @param cardPayInfo
	 * @param ondFareDTOs
	 * @param reservation
	 * @param credentialsDTO
	 * @param trackInfo
	 * @return CCFraudCheckRequestDTOBuilder
	 */
	private static CCFraudCheckRequestDTOBuilder assembleCCFraud(Collection<Integer> colTnxIds, String pnr,
			CardPaymentInfo cardPayInfo, Collection<OndFareDTO> ondFareDTOs, Reservation reservation,
			CredentialsDTO credentialsDTO, TrackInfoDTO trackInfo, String reservationType) {
		CCFraudCheckRequestDTOBuilder ccFraudBuilder = new CCFraudCheckRequestDTOBuilder();
		// Setting the external ref as the temp_transaction id.
		Integer[] tnx = new Integer[colTnxIds.size()];
		colTnxIds.toArray(tnx);
		ccFraudBuilder.setExternalId(tnx[0].toString());
		ccFraudBuilder.setPnr(pnr);
		ccFraudBuilder.setCardPayInfo(cardPayInfo);
		ccFraudBuilder.setOndFareDtos(ondFareDTOs);
		ccFraudBuilder.setReservation(reservation);
		ccFraudBuilder.setCredentials(credentialsDTO);
		ccFraudBuilder.setTrackInfo(trackInfo);
		ccFraudBuilder.setReservationType(reservationType);
		return ccFraudBuilder;
	}

	public static String getLastActualEticketForAgent(String agentCode) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getLastActualEticketForAgent(agentCode);
	}

	/**
	 * Recreate the ondFareDTO form the given criteria. This method is lighter than reqouting
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<OndFareDTO> recreateFareSegCharges(OndRebuildCriteria criteria) throws ModuleException {
		return ReservationModuleUtils.getFlightInventoryResBD().recreateFareSegCharges(criteria);
	}

	/**
	 * Check for the inventory avilability
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public static boolean checkInventoryAvailability(OndRebuildCriteria criteria) throws ModuleException {
		return ReservationModuleUtils.getFlightInventoryResBD().checkInventoryAvailability(criteria);
	}

	/**
	 * Return Available Flights for OHD rollfoward facility
	 * 
	 * @param rollForwardFlightSearch
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static List<RollForwardFlightAvailRS>
			getAvaialableFlightForRollForward(RollForwardFlightSearchDTO rollForwardFlightSearch) throws ModuleException {
		ServiceResponce response = ReservationModuleUtils.getFlightBD().searchAvailableFlights(rollForwardFlightSearch);

		if (response.getResponseParam(ResponceCodes.ROLL_FORWARD_AVAILABILITY_SEARCH_SUCCESSFULL) != null) {
			return (List<RollForwardFlightAvailRS>) response
					.getResponseParam(ResponceCodes.ROLL_FORWARD_AVAILABILITY_SEARCH_SUCCESSFULL);
		}

		return null;
	}

	public static void changeReservationCabinClass(FlightSegement flightSeg, Collection<String> cabinClassesToBeDeleted,
			Map<String, Integer> availableCabinClasses, CredentialsDTO credentialsDTO) throws ModuleException {

		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		Integer flightSegId = flightSeg.getFltSegId();
		Collection<String> cabinClasses = new ArrayList<String>();
		for (String cc : availableCabinClasses.keySet()) {
			cabinClasses.add(cc);
		}

		Collection<Reservation> toBeUpdatedReservations = new ArrayList<Reservation>();
		ReservationSegmentDAO segmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		Collection<Reservation> reservationList = segmentDAO.getReservationSegmentsForTransfer(flightSegId, false, null);
		int transferedAdultCount = 0;
		int transferedOnHoldAdultCount = 0;
		int transferedInfantCount = 0;
		int transferedOnHoldInfantCount = 0;
		int oprtransferedAdultCount = 0;
		int oprtransferedOnHoldAdultCount = 0;
		int oprtransferedInfantCount = 0;
		int oprtransferedOnHoldInfantCount = 0;
		String opnReturnBC = "";
		List<Integer> pnrSegIds = new ArrayList<Integer>();
		List<Integer> transferedSegmentIds = new ArrayList<Integer>();
		String originalCabinClass = null;
		FareSummaryDTO fareSummaryDTO = null;
		int chargeRateId = -1;
		BigDecimal fareDifference = new BigDecimal(0);
		Entry<InvDowngradeDTO, Integer> invDowngradeDTOEntry = null;

		Map<InvDowngradeDTO, Integer> invDowngradeDTOs = ReservationModuleUtils.getFlightInventoryResBD()
				.getAvailableBookingClassInventory(flightSegId, cabinClasses);

		int soldInfantCount = 0;
		int availbaleInfantCount = 0;
		if (reservationList != null && reservationList.size() > 0) {
			if (invDowngradeDTOs == null || invDowngradeDTOs.isEmpty()) {
				throw new ModuleException("airinventory.logic.bl.booking.class.unavailable");
			}

			invDowngradeDTOEntry = invDowngradeDTOs.entrySet().iterator().next();
			soldInfantCount = soldInfantCount + invDowngradeDTOEntry.getKey().getSoldInfant()
					+ invDowngradeDTOEntry.getKey().getOnholdInfant();
			availbaleInfantCount = invDowngradeDTOEntry.getKey().getInfantAllocation();
			// fixed seats count can not be greater than total segment allocation
			/*
			 * if (invDowngradeDTOEntry.getKey().getTotalFixedSeatsInSegment() > invDowngradeDTOEntry.getKey()
			 * .getTotalAllocatedSeatsInSegment()) { throw new
			 * ModuleException("airinventory.logic.bl.fixedalloc.exceeds"); }
			 */
			// If infant allocation is not enough will not allow to downgrade

		}

		// This is to get trasfering pax count if any to validate against fixed seats
		for (Reservation res : reservationList) {
			if (res.getGdsId() != null && !invDowngradeDTOs.containsValue(res.getGdsId())) {
				throw new ModuleException("airinventory.logic.bl.gdsbc.not.available");
			}

			for (ReservationSegment resSeg : res.getSegments()) {
				if (!resSeg.getBookingType().equals(BookingClass.BookingClassType.STANDBY)
						// && !resSeg.getBookingType().equals(BookingClass.BookingClassType.OPEN_RETURN)
						&& resSeg.getFlightSegId().intValue() == flightSegId.intValue()
						&& !resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					String cabinClass = ReservationModuleUtils.getReservationBD().getReservationCabinClass(res.getPnr(),
							resSeg.getPnrSegId());
					if (cabinClassesToBeDeleted.contains(cabinClass)) {
						pnrSegIds.add(resSeg.getPnrSegId());
						originalCabinClass = cabinClass;
						if (res.getStatus().equals(ReservationStatus.CONFIRMED)) {
							if (resSeg.getBookingType().equals(BookingClass.BookingClassType.OPEN_RETURN)) {
								oprtransferedAdultCount = oprtransferedAdultCount
										+ (res.getTotalPaxAdultCount() + res.getTotalPaxChildCount());
								oprtransferedInfantCount = oprtransferedInfantCount + res.getTotalPaxInfantCount();
							} else {
								transferedAdultCount = transferedAdultCount
										+ (res.getTotalPaxAdultCount() + res.getTotalPaxChildCount());
								transferedInfantCount = transferedInfantCount + res.getTotalPaxInfantCount();
							}
						} else if (res.getStatus().equals(ReservationStatus.ON_HOLD)) {
							if (resSeg.getBookingType().equals(BookingClass.BookingClassType.OPEN_RETURN)) {
								oprtransferedOnHoldAdultCount = oprtransferedOnHoldAdultCount
										+ (res.getTotalPaxAdultCount() + res.getTotalPaxChildCount());
								oprtransferedOnHoldInfantCount = oprtransferedOnHoldInfantCount + res.getTotalPaxInfantCount();
							} else {
								transferedOnHoldAdultCount = transferedOnHoldAdultCount
										+ (res.getTotalPaxAdultCount() + res.getTotalPaxChildCount());
								transferedOnHoldInfantCount = transferedOnHoldInfantCount + res.getTotalPaxInfantCount();

							}
						}
						toBeUpdatedReservations.add(res);
					}
				}
			}
		}
		if (reservationList != null && reservationList.size() > 0) {
			if ((transferedInfantCount + transferedOnHoldInfantCount + soldInfantCount) > availableCabinClasses
					.get(invDowngradeDTOEntry.getKey().getCabinClass())) {
				throw new ModuleException("airinventory.logic.bl.infant.allocation.unavailable");
			}
		}
		/*
		 * if ((transferedInfantCount + transferedOnHoldInfantCount + soldInfantCount) > availbaleInfantCount) { throw
		 * new ModuleException("airinventory.logic.bl.infant.allocation.unavailable"); }
		 */
		ReservationAudit reservationAudit = null;

		if (toBeUpdatedReservations.size() > 0 && AppSysParamsUtil.isAdjustFareDiffWhileCabinClassChange()) {
			fareSummaryDTO = getAvailableFareForExistingCabinClass(flightSeg, invDowngradeDTOEntry.getKey().getCabinClass());
			QuotedChargeDTO quotedChargeDTO = ReservationModuleUtils.getChargeBD().getCharge(ChargeCodes.CABIN_CHANGE_ADJUSTMENT,
					CalendarUtil.getCurrentSystemTimeInZulu(), getDummyFareSummaryDTO(), null, false);
			if (quotedChargeDTO == null) {
				throw new ModuleException("airinventory.cabin.downgrade.charge.not.defined");
			}
			chargeRateId = quotedChargeDTO.getChargeRateId();
		}

		InvDowngradeDTO invDowngradeDTO = null;

		for (Reservation res : toBeUpdatedReservations) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(res.getPnr());
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation, false);
			Set<ReservationSegment> reservationSegments = new HashSet<ReservationSegment>();
			Set<ReservationSegment> setSegements = reservation.getSegments();
			AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);
			if (setSegements == null || setSegements.size() == 0) {
				// No reservation segments to transfer
				continue;
			}

			for (Entry<InvDowngradeDTO, Integer> entry : invDowngradeDTOs.entrySet()) {
				if (entry.getValue() != null && res.getGdsId() != null && entry.getValue().equals(res.getGdsId())) {
					invDowngradeDTO = entry.getKey();
					break;
				} else if (res.getGdsId() == null) {
					invDowngradeDTO = entry.getKey();
					break;
				}
			}

			reservationSegments.addAll(setSegements);
			Iterator<ReservationSegment> segmentsIterator = reservationSegments.iterator();
			while (segmentsIterator.hasNext()) {

				ReservationSegment reservationSegment = segmentsIterator.next();
				// TODO remove these conditions as there are already checked
				if (!reservationSegment.getBookingType().equals(BookingClass.BookingClassType.STANDBY)
						// && !reservationSegment.getBookingType().equals(BookingClass.BookingClassType.OPEN_RETURN)
						&& reservationSegment.getFlightSegId().intValue() == flightSegId.intValue()
						&& !reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					String cabinClass = ReservationModuleUtils.getReservationBD().getReservationCabinClass(res.getPnr(),
							reservationSegment.getPnrSegId());
					if (cabinClassesToBeDeleted.contains(cabinClass)) {
						// what we do here is cancel the existing
						// segment and add new one with new details as
						// in transfer reservation segment

						int maxOndGroupId = ReservationCoreUtils.getMaxOndGroupId(reservationSegments) + 1;
						int returnOndGroupId = ReservationCoreUtils.getMaxReturnOndGroupId(reservationSegments) + 1;
						int maxSegmentSeq = ReservationCoreUtils.getMaxSegmentSequence(reservationSegments);
						ReservationSegment newReservationSegment = new ReservationSegment();
						newReservationSegment = (ReservationSegment) reservationSegment.clone();
						newReservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
						newReservationSegment.setAlertFlag(0);
						newReservationSegment.setOndGroupId(new Integer(maxOndGroupId));
						newReservationSegment.setSegmentSeq(maxSegmentSeq);
						newReservationSegment.setReturnOndGroupId(returnOndGroupId);
						newReservationSegment.setJourneySequence(ReservationCoreUtils.getMaxJourneySequence(reservationSegments));
						reservation.addSegment(newReservationSegment);

						// reservationSegment.setFlightSegId(new
						// Integer(transferSeatDTO.getTargetFlightSegId()));

						Collection<ReservationPaxFareSegment> colReservationReservationPaxFareSegment;
						Map<Integer, ReservationPaxFare> newReservationPaxFares = new HashMap<Integer, ReservationPaxFare>();
						if (reservationSegment.getBookingType().equals(BookingClass.BookingClassType.OPEN_RETURN)) {
							for (ReservationPax reservationPax2 : reservationSegment.getReservation().getPassengers()) {
								ReservationPax reservationPax = reservationPax2;

								for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
									ReservationPaxFare oldReservationPaxFare = reservationPaxFare;
									ReservationPaxFare newReservationPaxFare = (ReservationPaxFare) oldReservationPaxFare.clone();
									colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
									colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
									for (ReservationPaxFareSegment reservationPaxFareSegment2 : oldReservationPaxFare
											.getPaxFareSegments()) {
										ReservationPaxFareSegment reservationPaxFareSegment = reservationPaxFareSegment2;

										if (reservationPaxFareSegment.getSegment() != null
												&& pnrSegIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
											transferedSegmentIds.add(reservationPaxFareSegment.getSegment().getPnrSegId());
											ReservationPaxFareSegment newReservationPaxFareSegment = (ReservationPaxFareSegment) reservationPaxFareSegment
													.clone();
											if (!reservationPax.getPaxType()
													.equals(ReservationInternalConstants.PassengerType.INFANT)) {

												String availClass = (String) cabinClasses.toArray()[0];
												opnReturnBC = "OPENRT" + availClass;
												reservationPaxFareSegment.setBookingCode("OPENRT" + availClass);

											}
											newReservationPaxFareSegment.setSegment(newReservationSegment);
											colReservationReservationPaxFareSegment.add(newReservationPaxFareSegment);

										}
									}

									if (colReservationReservationPaxFareSegment.size() > 0) {
										ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
												.getOndFareFromOndCharges(oldReservationPaxFare.getCharges());
										if (reservationPaxOndCharge != null) {
											ReservationPaxOndCharge newReservationPaxOndDRCharge = reservationPaxOndCharge
													.cloneForNew(credentialsDTO, false);
											newReservationPaxOndDRCharge
													.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
											newReservationPaxOndDRCharge
													.setZuluChargeDate(CalendarUtil.getCurrentSystemTimeInZulu());
											newReservationPaxOndDRCharge
													.setDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
											newReservationPaxOndDRCharge.setDiscountValuePercentage(new Integer(0));
											newReservationPaxFare.addCharge(newReservationPaxOndDRCharge);
											newReservationPaxFare.setTotalFare(new BigDecimal(0));
											newReservationPaxFare.setTotalTaxCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalSurCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalCancelCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalModificationCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalAdjustmentCharge(new BigDecimal(0));

											for (ReservationPaxFareSegment reservationPaxFareSegment : colReservationReservationPaxFareSegment) {
												newReservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
												if (fareSummaryDTO != null
														&& reservationPaxFareSegment.getSegment().getFlightSegId()
																.intValue() == flightSeg.getFltSegId().intValue()
														&& AppSysParamsUtil.isAdjustFareDiffWhileCabinClassChange()) {
													fareDifference = (reservationPaxFare.getTotalFare()
															.subtract(reservationPaxFare.getTotalDiscount().add(new BigDecimal(
																	fareSummaryDTO.getFareAmount(reservationPax.getPaxType())))));

													if (fareDifference.compareTo(BigDecimal.ZERO) == 1) {
														// Adjusting the credit manually
														ReservationBO.adjustCreditManual(adjustCreditBO, reservation,
																reservationPaxFare.getPnrPaxFareId(), chargeRateId,
																fareDifference.negate(),
																ReservationInternalConstants.ChargeGroup.ADJ,
																"Charge Adjustment for Cabin Class Change due to aircraft model change ",
																credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxGen);
													}
												}
											}
											newReservationPaxFares.put(reservationPax.getPnrPaxId(), newReservationPaxFare);
										}
									}
								}
							}
						} else {
							for (ReservationPax reservationPax2 : reservationSegment.getReservation().getPassengers()) {
								ReservationPax reservationPax = reservationPax2;

								for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
									ReservationPaxFare oldReservationPaxFare = reservationPaxFare;
									ReservationPaxFare newReservationPaxFare = (ReservationPaxFare) oldReservationPaxFare.clone();
									colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
									colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
									for (ReservationPaxFareSegment reservationPaxFareSegment2 : oldReservationPaxFare
											.getPaxFareSegments()) {
										ReservationPaxFareSegment reservationPaxFareSegment = reservationPaxFareSegment2;

										if (reservationPaxFareSegment.getSegment() != null
												&& pnrSegIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
											transferedSegmentIds.add(reservationPaxFareSegment.getSegment().getPnrSegId());
											ReservationPaxFareSegment newReservationPaxFareSegment = (ReservationPaxFareSegment) reservationPaxFareSegment
													.clone();
											if (!reservationPax.getPaxType()
													.equals(ReservationInternalConstants.PassengerType.INFANT)) {
												if (invDowngradeDTO != null) {
													reservationPaxFareSegment.setBookingCode(invDowngradeDTO.getBookingClass());
												}
											}
											newReservationPaxFareSegment.setSegment(newReservationSegment);
											colReservationReservationPaxFareSegment.add(newReservationPaxFareSegment);

										}
									}

									if (colReservationReservationPaxFareSegment.size() > 0) {
										ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
												.getOndFareFromOndCharges(oldReservationPaxFare.getCharges());
										if (reservationPaxOndCharge != null) {
											ReservationPaxOndCharge newReservationPaxOndDRCharge = reservationPaxOndCharge
													.cloneForNew(credentialsDTO, false);
											newReservationPaxOndDRCharge
													.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
											newReservationPaxOndDRCharge
													.setZuluChargeDate(CalendarUtil.getCurrentSystemTimeInZulu());
											newReservationPaxFare.addCharge(newReservationPaxOndDRCharge);
											newReservationPaxFare.setTotalFare(new BigDecimal(0));
											newReservationPaxFare.setTotalTaxCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalSurCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalCancelCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalModificationCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalAdjustmentCharge(new BigDecimal(0));

											for (ReservationPaxFareSegment reservationPaxFareSegment : colReservationReservationPaxFareSegment) {
												newReservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
												if (fareSummaryDTO != null
														&& reservationPaxFareSegment.getSegment().getFlightSegId()
																.intValue() == flightSeg.getFltSegId().intValue()
														&& AppSysParamsUtil.isAdjustFareDiffWhileCabinClassChange()) {
													fareDifference = (reservationPaxFare.getTotalFare()
															.subtract(reservationPaxFare.getTotalDiscount().add(new BigDecimal(
																	fareSummaryDTO.getFareAmount(reservationPax.getPaxType())))));

													if (fareDifference.compareTo(BigDecimal.ZERO) == 1) {
														// Adjusting the credit manually
														ReservationBO.adjustCreditManual(adjustCreditBO, reservation,
																reservationPaxFare.getPnrPaxFareId(), chargeRateId,
																fareDifference.negate(),
																ReservationInternalConstants.ChargeGroup.ADJ,
																"Charge Adjustment for Cabin Class Change due to aircraft model change ",
																credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxGen);
													}
												}
											}
											newReservationPaxFares.put(reservationPax.getPnrPaxId(), newReservationPaxFare);
										}
										newReservationPaxFares.put(reservationPax.getPnrPaxId(), newReservationPaxFare);
									}
								}
							}
						}

						for (ReservationPax reservationPax2 : reservationSegment.getReservation().getPassengers()) {
							ReservationPax reservationPax = reservationPax2;
							for (Integer pnrPaxId : newReservationPaxFares.keySet()) {
								if (pnrPaxId.intValue() == reservationPax.getPnrPaxId().intValue()) {
									reservationPax.addPnrPaxFare(newReservationPaxFares.get(pnrPaxId));
								}
							}
						}
					}
				}

			}

			ReservationProxy.saveReservation(reservation);
			adjustCreditBO.callRevenueAccountingAfterReservationSave();
			// Save reservation audit
			reservationAudit = new ReservationAudit();
			ReservationAudit.createReservationAudit(reservationAudit, reservation.getPnr(), null, credentialsDTO);
			reservationAudit.setModificationType(AuditTemplateEnum.CANCELED_SEGMENT.getCode());
			reservationAudit.setUserNote("Reservation segment Cabin Class Changed due to aircraft model change");
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.SEGMENT_INFORMATION,
					ReservationCoreUtils.getSegmentInformationWithCabinClassDetails(transferedSegmentIds, originalCabinClass));
			reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
			auditorBD.audit(reservationAudit, reservationAudit.getContentMap());

			if (invDowngradeDTO != null) {
				reservationAudit = new ReservationAudit();
				ReservationAudit.createReservationAudit(reservationAudit, reservation.getPnr(), null, credentialsDTO);
				reservationAudit.setModificationType(AuditTemplateEnum.ADDED_NEW_SEGMENT.getCode());
				reservationAudit.setUserNote("Reservation segment Cabin Class Changed due to aircraft model change");
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewSegment.SEGMENT_INFORMATION,
						ReservationCoreUtils.getSegmentInformationWithCabinClassDetails(transferedSegmentIds,
								invDowngradeDTO.getCabinClass()));
				reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
				auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
			}

			if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
				// send typeB message
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CABIN_CHANGED.getCode());
				typeBRequestDTO.setTransferedSegmentIds(transferedSegmentIds);
				TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
			}

			Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
			if (csOCCarrirs != null) {
				// send Codeshare typeB message
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CABIN_CHANGED.getCode());
				typeBRequestDTO.setTransferedSegmentIds(transferedSegmentIds);
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			}
		}

		if (invDowngradeDTO != null) {
			// Finaly update the BC and segment inventories with new sold count
			invDowngradeDTO.setSoldSeats(transferedAdultCount);
			invDowngradeDTO.setOnholdSeats(transferedOnHoldAdultCount);
			ReservationModuleUtils.getFlightInventoryBD().updateFCCSegBCInventories(invDowngradeDTO, transferedInfantCount,
					transferedOnHoldInfantCount);
			if (!opnReturnBC.equals("")) {
				ReservationModuleUtils.getFlightInventoryBD().updateOpenReturnFCCSegBCInventories(flightSegId, opnReturnBC,
						oprtransferedAdultCount, oprtransferedOnHoldAdultCount, oprtransferedInfantCount,
						oprtransferedOnHoldInfantCount);

			}
		}
	}

	private static FareSummaryDTO getAvailableFareForExistingCabinClass(FlightSegement flightSeg, String cabinClassCode)
			throws ModuleException {
		FareSummaryDTO fareSummaryDTO = null;
		AvailableFlightSegment availFlightSegment = new AvailableFlightSegment();
		FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
		flightSegmentDTO.setDepartureDateTime(flightSeg.getEstTimeDepatureLocal());
		flightSegmentDTO.setDepartureDateTimeZulu(flightSeg.getEstTimeDepatureZulu());
		flightSegmentDTO.setArrivalDateTime(flightSeg.getEstTimeArrivalLocal());
		flightSegmentDTO.setArrivalDateTimeZulu(flightSeg.getEstTimeArrivalZulu());
		flightSegmentDTO.setFlightId(flightSeg.getFlightId());
		flightSegmentDTO.setFlightStatus(flightSeg.getStatus());
		flightSegmentDTO.setSegmentId(flightSeg.getFltSegId());
		flightSegmentDTO.setSegmentCode(flightSeg.getSegmentCode());
		availFlightSegment.getFlightSegmentDTOs().add(flightSegmentDTO);
		availFlightSegment.setOndSequence(0);
		availFlightSegment.setCabinClassCode(cabinClassCode);

		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();
		ondInfoDTO.setOrigin(flightSeg.getSegmentCode().split("/")[0]);
		ondInfoDTO.setDestination(flightSeg.getSegmentCode().split("/")[1]);
		ondInfoDTO.setDepartureDateTimeStart(flightSeg.getEstTimeDepatureLocal());
		ondInfoDTO.setArrivalDateTimeStart(flightSeg.getEstTimeArrivalLocal());
		availableFlightSearchDTO.addOriginDestination(ondInfoDTO);
		// Set sales channel as public to get only public fares
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC);
		List<FilteredBucketsDTO> filteredBucketsDTOs = ReservationModuleUtils.getFlightInventoryResBD()
				.getSingleFlightOneWayCheapestFare(availFlightSegment, availableFlightSearchDTO);
		if (filteredBucketsDTOs != null && filteredBucketsDTOs.size() > 0) {
			List<AvailableBCAllocationSummaryDTO> availableBCAllocationSummaryDTOs = filteredBucketsDTOs.get(0)
					.getCheapestBCAllocationSummaryDTOs();
			AvailableBCAllocationSummaryDTO minFareSummaryDTO = availableBCAllocationSummaryDTOs
					.get(availableBCAllocationSummaryDTOs.size() - 1);
			if (minFareSummaryDTO != null) {
				fareSummaryDTO = minFareSummaryDTO.getFareSummaryDTO();
			}
		}

		return fareSummaryDTO;
	}

	/**
	 * Send email to agent for canceled PNR by scheduler
	 */
	private static void emailToAgentForCanceledPNR(Collection<String> pnrCollection) {

		try {
			Iterator<String> pnrItr = pnrCollection.iterator();
			Map<String, Collection<String>> cnxPnrByAgentMap = new HashMap<String, Collection<String>>();
			while (pnrItr.hasNext()) {
				String pnr = pnrItr.next();

				Reservation reservation = ReservationModuleUtils.getReservationBD().getReservation(pnr, false);
				String agentCode = reservation.getAdminInfo().getOwnerAgentCode();

				if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
					if (cnxPnrByAgentMap.get(agentCode) != null) {
						cnxPnrByAgentMap.get(agentCode).add(pnr);
					} else {
						Collection<String> pnrList = new ArrayList<String>();
						pnrList.add(pnr);
						cnxPnrByAgentMap.put(agentCode, pnrList);
					}

				}
			}

			for (String agentCode : cnxPnrByAgentMap.keySet()) {
				if (cnxPnrByAgentMap.get(agentCode) != null) {
					ArrayList<String> pnrList = (ArrayList<String>) cnxPnrByAgentMap.get(agentCode);

					if (pnrList != null && pnrList.size() > 0) {
						ReservationModuleUtils.getReservationBD().sendBookingChangesEmailToAgent(pnrList, agentCode,
								ResModifyEmailAgentModificationType.CANCEL_RESERVATION, null);
					}

				}

			}
		} catch (Exception e) {
			log.error(" ERROR OCCURED WHILE SENDING EMAIL TO AGENT FOR CANCELED PNR BY SCHEDULER");
		}

	}

	/**
	 * Send email to agent for wait listed confirmed PNRs
	 */
	private static void emailToAgentForConfirmedWaitListedPNR(Collection<String> pnrCollection) {

		try {
			Iterator<String> pnrItr = pnrCollection.iterator();
			Map<String, Collection<String>> cnxPnrByAgentMap = new HashMap<String, Collection<String>>();
			while (pnrItr.hasNext()) {
				String pnr = pnrItr.next();

				Reservation reservation = ReservationModuleUtils.getReservationBD().getReservation(pnr, false);
				String agentCode = reservation.getAdminInfo().getOwnerAgentCode();

				if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
					if (cnxPnrByAgentMap.get(agentCode) != null) {
						cnxPnrByAgentMap.get(agentCode).add(pnr);
					} else {
						Collection<String> pnrList = new ArrayList<String>();
						pnrList.add(pnr);
						cnxPnrByAgentMap.put(agentCode, pnrList);
					}

				}
			}

			for (String agentCode : cnxPnrByAgentMap.keySet()) {
				if (cnxPnrByAgentMap.get(agentCode) != null) {
					ArrayList<String> pnrList = (ArrayList<String>) cnxPnrByAgentMap.get(agentCode);

					if (pnrList != null && pnrList.size() > 0) {
						ReservationModuleUtils.getReservationBD().sendBookingChangesEmailToAgent(pnrList, agentCode,
								ResModifyEmailAgentModificationType.CNF_WAITLISTED, null);
					}

				}

			}
		} catch (Exception e) {
			log.error(" ERROR OCCURED WHILE SENDING EMAIL TO AGENT FOR CONFIRMED WAITLISTED PNR");
		}

	}

	public static void executeAutoCancellation(Date currentDate, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge) throws ModuleException {

		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;

		List<Integer> autoCnxIds = reservationDAO.getExpiredAutoCancellationInfoList(currentDate);
		Collection<Integer> successAutoCnxIds = null;
		if (autoCnxIds != null && !autoCnxIds.isEmpty()) {

			successAutoCnxIds = new HashSet<Integer>();

			// Firstly, check for segments with expiring cancellation ID, if exists cancel those segments
			List<AutoCancellationSchDTO> autoCnxSchDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
					.getExpiredSegments(autoCnxIds, AppSysParamsUtil.getDefaultCarrierCode());

			if (autoCnxSchDTOs != null && !autoCnxSchDTOs.isEmpty()) {
				for (AutoCancellationSchDTO autoCnxSchDTO : autoCnxSchDTOs) {
					long version = autoCnxSchDTO.getVersion();
					Map<Integer, List<Integer>> ondGroupWiseSegments = autoCnxSchDTO.getOndGroudWiseCancellingSegIds();

					for (Entry<Integer, List<Integer>> segEntry : ondGroupWiseSegments.entrySet()) {
						CustomChargesTO customChargesTO = new CustomChargesTO(customAdultCancelCharge, customChildCancelCharge,
								customInfantCancelCharge, null, null, null);
						List<Integer> cancelingSegsIds = segEntry.getValue();
						BigDecimal[] existingModCharges = AutoCancellationBO
								.calculateExistingModificationCharges(cancelingSegsIds);

						if (existingModCharges != null) {
							customChargesTO.setCompareWithExistingModificationCharges(true);
							customChargesTO.setExistingModificationChargeAdult(existingModCharges[0]);
							customChargesTO.setExistingModificationChargeChild(existingModCharges[1]);
							customChargesTO.setExistingModificationChargeInfant(existingModCharges[2]);
						}

						if (cancelingSegsIds != null && !cancelingSegsIds.isEmpty()) {
							if (log.isDebugEnabled()) {
								log.debug("Started Cancelling the Expired Segments of [PNR:" + autoCnxSchDTO.getPnr()
										+ "][SEGMENTS:" + cancelingSegsIds + "]");
							}

							ReservationModuleUtils.getSegmentBD().cancelSegments(autoCnxSchDTO.getPnr(), cancelingSegsIds,
									customChargesTO, version++, true, null, false, null, true);

							if (log.isDebugEnabled()) {
								log.debug("Completed Cancelling Expired Segments of [PNR:" + autoCnxSchDTO.getPnr()
										+ "][SEGMENTS:" + cancelingSegsIds + "]");
							}

							successAutoCnxIds.add(autoCnxSchDTO.getAutoCancellationId());
						}
					}
				}
			}

			// Secondly, check for ancillaries with expiring cancellation ID, if exists cancel those ancillaries
			autoCnxSchDTOs = getExpiredAncillaries(autoCnxIds);
			if (autoCnxSchDTOs != null) {
				for (AutoCancellationSchDTO autoCnxSchDTO : autoCnxSchDTOs) {

					String pnr = autoCnxSchDTO.getPnr();
					int version = Long.valueOf(autoCnxSchDTO.getVersion()).intValue();

					if (log.isDebugEnabled()) {
						log.debug("Started Cancelling Ancillaries of [PNR:" + autoCnxSchDTO.getPnr() + "]");
					}

					ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();

					if (autoCnxSchDTO.getCancellingSeats() != null) {
						reservationBD.modifySeats(autoCnxSchDTO.getCancellingSeats(), pnr, "", version++, true, null);
					}

					if (autoCnxSchDTO.getCancellingMeals() != null) {
						reservationBD.modifyMeals(autoCnxSchDTO.getCancellingMeals(), pnr, "", version++, true, null);
					}

					if (autoCnxSchDTO.getCancellingBaggages() != null) {
						reservationBD.modifyBaggages(autoCnxSchDTO.getCancellingBaggages(), pnr, "", version++, true, null);
					}

					if (autoCnxSchDTO.getCancellingInflightServices() != null
							&& !autoCnxSchDTO.getCancellingInflightServices().isEmpty()) {
						reservationBD.modifySSRs(autoCnxSchDTO.getCancellingInflightServices(), pnr, "", null, version++, true,
								EXTERNAL_CHARGES.INFLIGHT_SERVICES, null, true);
					}

					if (autoCnxSchDTO.getCancellingAirportService() != null
							&& !autoCnxSchDTO.getCancellingAirportService().isEmpty()) {
						reservationBD.modifySSRs(autoCnxSchDTO.getCancellingAirportService(), pnr, "", null, version++, true,
								EXTERNAL_CHARGES.AIRPORT_SERVICE, null, true);
					}

					if (autoCnxSchDTO.isInsuranceExists()) {
						reservationBD.removeInsurance(pnr, version++);
					}

					successAutoCnxIds.add(autoCnxSchDTO.getAutoCancellationId());

					if (log.isDebugEnabled()) {
						log.debug("Completed Cancelling Ancillaries of [PNR:" + autoCnxSchDTO.getPnr() + "]");
					}
				}
			}

			// Finally, check for infants with expiring cancellation ID, if exists remove those infants
			autoCnxSchDTOs = ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getExpiredInfants(autoCnxIds,
					AppSysParamsUtil.getDefaultCarrierCode());
			if (autoCnxSchDTOs != null && !autoCnxSchDTOs.isEmpty()) {
				CustomChargesTO customChargesTO = new CustomChargesTO(customAdultCancelCharge, customChildCancelCharge,
						customInfantCancelCharge, null, null, null);
				for (AutoCancellationSchDTO autoCnxSchDTO : autoCnxSchDTOs) {
					if (log.isDebugEnabled()) {
						log.debug("Started Removing infants of [PNR:" + autoCnxSchDTO.getPnr() + "][INFANT IDs:"
								+ autoCnxSchDTO.getCancellingPaxIds() + "]");
					}
					ReservationModuleUtils.getPassengerBD().removePassengers(autoCnxSchDTO.getPnr(), null,
							autoCnxSchDTO.getCancellingPaxIds(), customChargesTO, autoCnxSchDTO.getVersion(), null, null, null);
					successAutoCnxIds.add(autoCnxSchDTO.getAutoCancellationId());
					if (log.isDebugEnabled()) {
						log.debug("Completed Removing infants of [PNR:" + autoCnxSchDTO.getPnr() + "][INFANT IDs:"
								+ autoCnxSchDTO.getCancellingPaxIds() + "]");
					}
				}
			}
		}

		// If LCC connectivity enabled, handle dry/interline cancellation via LCC
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			ReservationModuleUtils.getLCCReservationBD().executeAutoCancellation();
		}

		if (successAutoCnxIds != null && !successAutoCnxIds.isEmpty()) {
			// After executing auto cancellation, update processedByScheduler value
			reservationDAO.updateAutoCancelSchedulerStatus(new ArrayList<Integer>(successAutoCnxIds),
					AutoCancellationInfo.PROCESSED);
		}

	}

	private static List<AutoCancellationSchDTO> getExpiredAncillaries(Collection<Integer> autoCnxIds) throws ModuleException {
		List<AutoCancellationSchDTO> autoCancellationSchDTOs = new ArrayList<AutoCancellationSchDTO>();

		// getting expired seats
		Map<ReservationLiteDTO, List<PaxSeatTO>> resWiseSeats = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
				.getExpiredSeats(autoCnxIds, AppSysParamsUtil.getDefaultCarrierCode());
		if (resWiseSeats != null && !resWiseSeats.isEmpty()) {
			for (Entry<ReservationLiteDTO, List<PaxSeatTO>> seatEntry : resWiseSeats.entrySet()) {
				ReservationLiteDTO resDto = seatEntry.getKey();
				List<PaxSeatTO> paxSeats = seatEntry.getValue();

				if (paxSeats != null && !paxSeats.isEmpty()) {
					AutoCancellationSchDTO autoCnxDto = new AutoCancellationSchDTO(resDto.getPnr(), resDto.getVersion(),
							resDto.getAutoCancellationId());
					PassengerSeatingAssembler ipaxSeating = new PassengerSeatingAssembler(null, false);

					int elemIndex = autoCancellationSchDTOs.indexOf(autoCnxDto);
					if (elemIndex != -1) {
						autoCnxDto = autoCancellationSchDTOs.get(elemIndex);
					} else {
						autoCancellationSchDTOs.add(autoCnxDto);
					}

					for (PaxSeatTO paxSeatTO : paxSeats) {
						ipaxSeating.removePassengerFromFlightSeat(paxSeatTO.getPnrPaxId(), paxSeatTO.getPnrPaxFareId(),
								paxSeatTO.getPnrSegId(), paxSeatTO.getSelectedFlightSeatId(), paxSeatTO.getChgDTO().getAmount(),
								null);
					}

					autoCnxDto.setCancellingSeats(ipaxSeating);
				}

			}
		}

		// Getting expired meals
		Map<ReservationLiteDTO, List<PaxMealTO>> resWiseMeals = ReservationDAOUtils.DAOInstance.MEAL_DAO
				.getExpiredMeals(autoCnxIds, AppSysParamsUtil.getDefaultCarrierCode());
		if (resWiseMeals != null && !resWiseMeals.isEmpty()) {
			for (Entry<ReservationLiteDTO, List<PaxMealTO>> mealEntry : resWiseMeals.entrySet()) {
				ReservationLiteDTO resDto = mealEntry.getKey();
				List<PaxMealTO> paxMeals = mealEntry.getValue();

				if (paxMeals != null && !paxMeals.isEmpty()) {
					AutoCancellationSchDTO autoCnxDto = new AutoCancellationSchDTO(resDto.getPnr(), resDto.getVersion(),
							resDto.getAutoCancellationId());
					PassengerMealAssembler ipaxMeal = new PassengerMealAssembler(null, false);

					int elemIndex = autoCancellationSchDTOs.indexOf(autoCnxDto);
					if (elemIndex != -1) {
						autoCnxDto = autoCancellationSchDTOs.get(elemIndex);
					} else {
						autoCancellationSchDTOs.add(autoCnxDto);
					}

					for (PaxMealTO paxMealTO : paxMeals) {
						ipaxMeal.removePassengerFromFlightMeal(paxMealTO.getPnrPaxId(), paxMealTO.getPnrPaxFareId(),
								paxMealTO.getPnrSegId(), paxMealTO.getSelectedFlightMealId(), paxMealTO.getChgDTO().getAmount(),
								paxMealTO.getMealId(), null, null, paxMealTO.getAllocatedQty());
					}

					autoCnxDto.setCancellingMeals(ipaxMeal);
				}

			}
		}

		// Getting expired baggage details
		Map<ReservationLiteDTO, List<PaxBaggageTO>> resWiseBaggages = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO
				.getExpiredBaggages(autoCnxIds, AppSysParamsUtil.getDefaultCarrierCode());
		if (resWiseBaggages != null && !resWiseBaggages.isEmpty()) {
			for (Entry<ReservationLiteDTO, List<PaxBaggageTO>> baggageEntry : resWiseBaggages.entrySet()) {
				ReservationLiteDTO resDto = baggageEntry.getKey();
				List<PaxBaggageTO> paxBaggages = baggageEntry.getValue();

				if (paxBaggages != null && !paxBaggages.isEmpty()) {
					AutoCancellationSchDTO autoCnxDto = new AutoCancellationSchDTO(resDto.getPnr(), resDto.getVersion(),
							resDto.getAutoCancellationId());
					PassengerBaggageAssembler ipaxBaggage = new PassengerBaggageAssembler(null, false);

					int elemIndex = autoCancellationSchDTOs.indexOf(autoCnxDto);
					if (elemIndex != -1) {
						autoCnxDto = autoCancellationSchDTOs.get(elemIndex);
					} else {
						autoCancellationSchDTOs.add(autoCnxDto);
					}

					for (PaxBaggageTO paxBaggageTO : paxBaggages) {
						ipaxBaggage.removePassengerFromFlightBaggage(paxBaggageTO.getPnrPaxId(), paxBaggageTO.getPnrPaxFareId(),
								paxBaggageTO.getPnrSegId(), paxBaggageTO.getSelectedFlightBaggageId(),
								paxBaggageTO.getChgDTO().getAmount(), paxBaggageTO.getBaggageId(), null, null);
					}

					autoCnxDto.setCancellingBaggages(ipaxBaggage);
				}

			}
		}

		// Getting expired in-flight services & airport services
		Map<ReservationLiteDTO, List<PaxSSRDTO>> resWiseSSRs = ReservationDAOUtils.DAOInstance.RESERVATION_SSR
				.getExpiredSSRs(autoCnxIds, AppSysParamsUtil.getDefaultCarrierCode());
		if (resWiseSSRs != null && !resWiseSSRs.isEmpty()) {
			for (Entry<ReservationLiteDTO, List<PaxSSRDTO>> ssrEntry : resWiseSSRs.entrySet()) {
				ReservationLiteDTO resDto = ssrEntry.getKey();
				List<PaxSSRDTO> paxSSRs = ssrEntry.getValue();

				if (paxSSRs != null && !paxSSRs.isEmpty()) {
					AutoCancellationSchDTO autoCnxDto = new AutoCancellationSchDTO(resDto.getPnr(), resDto.getVersion(),
							resDto.getAutoCancellationId());
					Map<Integer, SegmentSSRAssembler> paxInflightServiceMap = new HashMap<Integer, SegmentSSRAssembler>();
					Map<Integer, SegmentSSRAssembler> paxAirportServicesMap = new HashMap<Integer, SegmentSSRAssembler>();

					int elemIndex = autoCancellationSchDTOs.indexOf(autoCnxDto);
					if (elemIndex != -1) {
						autoCnxDto = autoCancellationSchDTOs.get(elemIndex);
					} else {
						autoCancellationSchDTOs.add(autoCnxDto);
					}

					for (PaxSSRDTO paxSSRDTO : paxSSRs) {
						SegmentSSRAssembler ssrAssem = null;
						if (paxSSRDTO.getAirportCode() != null && !"".equals(paxSSRDTO.getAirportCode())) {
							if (!paxAirportServicesMap.containsKey(paxSSRDTO.getPaxSeq())) {
								paxAirportServicesMap.put(paxSSRDTO.getPaxSeq(), new SegmentSSRAssembler());
							}

							ssrAssem = paxAirportServicesMap.get(paxSSRDTO.getPaxSeq());
						} else {
							if (!paxInflightServiceMap.containsKey(paxSSRDTO.getPaxSeq())) {
								paxInflightServiceMap.put(paxSSRDTO.getPaxSeq(), new SegmentSSRAssembler());
							}

							ssrAssem = paxInflightServiceMap.get(paxSSRDTO.getPaxSeq());
						}

						if (ssrAssem != null) {
							ssrAssem.addCanceledSegemntSSRCodeMap(paxSSRDTO.getPnrPaxSegmentSSRId(), paxSSRDTO.getSsrCode());
							ssrAssem.addCanceledPaxSegmentSSR(paxSSRDTO.getPnrPaxSegmentSSRId());
						}
					}

					autoCnxDto.setCancellingInflightServices(paxInflightServiceMap);
					autoCnxDto.setCancellingAirportService(paxAirportServicesMap);
				}
			}
		}

		// Getting expired insurance details
		List<ReservationLiteDTO> insCnxDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
				.getExpiredInsuranceAvailability(autoCnxIds, AppSysParamsUtil.getDefaultCarrierCode());
		if (insCnxDTOs != null && !insCnxDTOs.isEmpty()) {
			for (ReservationLiteDTO resDto : insCnxDTOs) {
				AutoCancellationSchDTO autoCnxDto = new AutoCancellationSchDTO(resDto.getPnr(), resDto.getVersion(),
						resDto.getAutoCancellationId());
				int elemIndex = autoCancellationSchDTOs.indexOf(autoCnxDto);

				if (elemIndex != -1) {
					autoCnxDto = autoCancellationSchDTOs.get(elemIndex);
				} else {
					autoCancellationSchDTOs.add(autoCnxDto);
				}

				autoCnxDto.setInsuranceExists(true);
			}
		}

		return autoCancellationSchDTOs;
	}

	/**
	 * Gets the refundable payment amount data for the provided charge type and passengers. Encapsulated in
	 * {@link RefundableChargeDetailDTO}
	 * 
	 * Current implementation returns the total refundable amount - already refunded amount. (If the total TAX
	 * refundable charges are 50 and 30 has already been refunded from TAX charges, returns 20.) And the related
	 * PnrPaxChgIds for the particular calculation. (Related to the particular segment)
	 * 
	 * @param pnrPaxIDList
	 *            Collection of pnrPax IDs
	 * @param chargeType
	 *            Charge type (TAX,SUR etc.)
	 * 
	 * @return Map<String, Collection<RefundableChargeDetailDTO>> key -> pnrPaxID value -> collection of
	 *         RefundableChargeDetailDTO containing the refundable charge amount data
	 * 
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public static Map<Integer, Collection<RefundableChargeDetailDTO>> getRefunndableChargeAmount(Collection<Integer> pnrPaxIDList,
			String chargeType) {

		ReservationPaymentDAO reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		ReservationTnxDAO txnDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		PassengerDAO paxDao = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;

		Map<Integer, Collection<RefundableChargeDetailDTO>> results = new HashMap<Integer, Collection<RefundableChargeDetailDTO>>();

		// Per pnrPaxID iteration
		for (Integer intPnrPaxID : pnrPaxIDList) {
			String pnrPaxID = intPnrPaxID.toString();

			Collection<RefundableChargeDetailDTO> refundableChargeDTOs = new ArrayList<RefundableChargeDetailDTO>();

			Map<String, Collection<String>> ppfIDMap = paxDao.getNoShowPPDIDsForPax(pnrPaxID);

			// Per segment ppfIDList iteration
			for (Entry<String, Collection<String>> ppfIDEntry : ppfIDMap.entrySet()) {
				Map<String, BigDecimal> chargeAmountMap = new HashMap<String, BigDecimal>();

				List<Long> pnrPaxOndChargeIDs = new ArrayList<Long>();

				/*
				 * Get the charge amounts for this charge type. When adding refunded amounts and payment amounts will
				 * cancel each other out and give the correct result.
				 */
				List<ReservationPaxOndPayment> paymentList = reservationPaymentDAO.getRefundablePayment(pnrPaxID, chargeType,
						ppfIDEntry.getValue(), null);

				String adjustmentCode = ReservationModuleUtils.getAirReservationConfig().getChargeAdjustmentMap()
						.get(ReservationInternalConstants.CHARGE_ADJUSTMENTS.AUTOMATED_REFUND_TAX.toString());
				List<ReservationPaxOndPayment> refundedChargeAdjustmentList = reservationPaymentDAO.getRefundablePayment(pnrPaxID,
						ReservationInternalConstants.ChargeGroup.ADJ, ppfIDEntry.getValue(), adjustmentCode);

				/*
				 * Sorts the ReservationPaxOndPayment according to the paymentTnxId and puts the grouped into a Map
				 */
				Map<Long, List<ReservationPaxOndPayment>> txnWisePaymentList = ReservationApiUtils
						.getTransactionWisePaymentMap(paymentList);
				Map<Long, List<ReservationPaxOndPayment>> txnWiseAdjustmentList = ReservationApiUtils
						.getTransactionWisePaymentMap(refundedChargeAdjustmentList);

				BigDecimal adjustmentTotal = ReservationApiUtils.getTxnWisePaymentMapTotal(txnWiseAdjustmentList);

				for (Entry<Long, List<ReservationPaxOndPayment>> entry : txnWisePaymentList.entrySet()) {

					ReservationTnx txn = txnDao.getTransaction(entry.getKey());
					BigDecimal refundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

					for (ReservationPaxOndPayment payment : entry.getValue()) {
						refundableAmount = AccelAeroCalculator.add(refundableAmount, payment.getAmount());

						/*
						 * We only need the positive ReservationPaxOndPayments ond charge ids because that's what will
						 * be used in refund.
						 */
						if (payment.getAmount().compareTo(BigDecimal.ZERO) == 1) {
							pnrPaxOndChargeIDs.add(payment.getPnrPaxOndChgId());
						}
					}

					/*
					 * If amount is greater than 0 and there's an tax refund adjustment that mean's there haven't been
					 * any refund only an adjustment. In that case only we need to add the negative tax refund
					 * adjustment so proper tax that should be refunded will come
					 */
					if (refundableAmount.compareTo(BigDecimal.ZERO) == 1) {
						refundableAmount = AccelAeroCalculator.add(refundableAmount, adjustmentTotal);
						if (refundableAmount.compareTo(BigDecimal.ZERO) == -1) {
							/*
							 * If the total of the adjustment amount is negative that means there's more
							 * adjustmentAmount than the total refundable amount in this transaction. So keeping keeping
							 * the remaining adjustmentAmount to be deducted from the next transaction if any.
							 */
							adjustmentTotal = refundableAmount.negate();
							refundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
					}

					chargeAmountMap.put(txn.getTnxId().toString(), AccelAeroCalculator.scaleValueDefault(refundableAmount));
				}

				RefundableChargeDetailDTO rcdd = new RefundableChargeDetailDTO();
				rcdd.setPnrSegID(ppfIDEntry.getKey());
				rcdd.setChargeType(chargeType);
				rcdd.setChargeAmountMap(chargeAmountMap);
				rcdd.setPnrPaxOndChgIDs(pnrPaxOndChargeIDs);
				rcdd.setPpfIDs(new ArrayList<String>(ppfIDEntry.getValue()));
				refundableChargeDTOs.add(rcdd);
			}
			results.put(intPnrPaxID, refundableChargeDTOs);
		}

		return results;
	}

	public static ServiceResponce createAlert(String pnr, Collection<Integer> pnrSegIds, String description, long alertTypeId,
			CredentialsDTO credentialsDTO) throws ModuleException {
		if (pnrSegIds.size() > 0) {
			
			ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
			
			Collection<Alert> colAlert = new ArrayList<Alert>();

			for (Integer pnrSegId : pnrSegIds) {
				FlightSegement flightSegment = ReservationModuleUtils.getFlightBD().getSpecificFlightSegment(pnrSegId);

				if (flightSegment != null) {
					Alert alert = new Alert();
					alert.setAlertTypeId(alertTypeId);
					alert.setContent(description);
					alert.setPnrSegId(new Long(pnrSegId));
					alert.setPriorityCode(new Long(1));
					alert.setOriginalDepDate(flightSegment.getEstTimeDepatureZulu());
					alert.setTimestamp(CalendarUtil.getCurrentZuluDateTime());

					colAlert.add(alert);
				} else {
					throw new ModuleException("airreservations.create.alert.flt.seg.not.found");
				}
			}

			if (colAlert.size() > 0) {
				ReservationModuleUtils.getAlertingBD().addAlerts(colAlert);
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			Collection<ReservationSegment> colReservationSegment = new ArrayList<ReservationSegment>();

			for (ReservationSegment reservationSegment : reservation.getSegments()) {
				if (pnrSegIds.contains(reservationSegment.getPnrSegId())) {
					colReservationSegment.add(reservationSegment);
				}
			}
			//AEROMART-3386
			Map<Integer, String> mapPnrSegActions = new HashMap<Integer, String>();
			for (int segID : pnrSegIds) {
				mapPnrSegActions.put(segID, " Action Taken : " + description);
			}

			ReservationBO.recordAlertModifications(colReservationSegment, credentialsDTO, AuditTemplateEnum.CREATED_ALERT, mapPnrSegActions,
					null);
			//AEROMART-3386
			reservationSegmentDAO.updateReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_TRUE, null,pnrSegIds, null);
		}

		return new DefaultServiceResponse(true);
	}

	public static void saveReservationWLPriority(String pnr, Integer AdultChildCount, Integer infantCount,
			Collection<Integer> flightSegIds) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		Collection<ReservationWLPriority> reservationWLPriorities = new ArrayList<ReservationWLPriority>();
		ReservationWLPriority reservationWLPriority = null;
		for (Integer flightSegId : flightSegIds) {
			Integer currentPriority = reservationDAO.getWLReservationPriority(flightSegId);
			reservationWLPriority = new ReservationWLPriority();
			reservationWLPriority.setPnr(pnr);
			reservationWLPriority.setFlightSegId(flightSegId);
			reservationWLPriority.setPriority(currentPriority == null ? 1 : currentPriority + 1);
			reservationWLPriority.setTotalPassengerCount(AdultChildCount);
			reservationWLPriority.setInfantCount(infantCount);

			reservationWLPriority.setStatus(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING);
			reservationWLPriorities.add(reservationWLPriority);
		}

		reservationDAO.saveWLReservationPriority(reservationWLPriorities);
	}

	public static WLConfirmationResponseDTO confirmWaitListedReservations(List<WLConfirmationRequestDTO> wlRequests,
			CredentialsDTO credentialsDTO) throws ModuleException {

		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		int confirmedWLSeats = 0;
		Map<String, Integer> confirmedWLSeatInBc = new HashMap<String, Integer>();
		int confirmedWLInfants = 0;
		List<String> pnrs = new ArrayList<String>();
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		Collection<ReservationWLPriority> updatedWLPriorities = new ArrayList<ReservationWLPriority>();
		for (WLConfirmationRequestDTO wlRequest : wlRequests) {
			// Resetting the state variables
			confirmedWLSeatInBc.clear();
			confirmedWLSeats = 0;
			confirmedWLInfants = 0;
			// Get wait listed reservations for the given flight segment and seats
			Collection<ReservationWLPriority> wlPriorities = reservationDAO.getWaitListedReservations(wlRequest.getFlightSegId());
			// Find matching reservation depending on the pax count, priority etc...
			for (ReservationWLPriority reservationWLPriority : wlPriorities) {
				// if (reservationWLPriority.getTotalPassengerCount() <= (wlRequest.getSegmentAvailabileCount() -
				// confirmedWLSeats)
				// && reservationWLPriority.getInfantCount() <= (wlRequest.getSegmentAvailableInfantCount() -
				// confirmedWLInfants)) {
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
				pnrModesDTO.setPnr(reservationWLPriority.getPnr());
				Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

				if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)
						|| reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
					// Loading segment bc details
					Map<Integer, BookingClass> mapPnrSegIdAndBookingClass = ReservationApiUtils.getBookingClasses(reservation);
					Iterator<ReservationSegment> ite = reservation.getSegments().iterator();
					Collection<Integer> pnrSegmentIds = new ArrayList<Integer>();
					while (ite.hasNext()) {
						ReservationSegment resSeg = ite.next();
						String bookingCode = mapPnrSegIdAndBookingClass.get(resSeg.getPnrSegId()).getBookingCode();
						Integer bcAvailableCount = wlRequest.getSegmentBcAvailableCount().get(bookingCode);
						if (bcAvailableCount == null) {
							continue;
						}
						if (confirmedWLSeatInBc.get(bookingCode) != null) {
							bcAvailableCount = bcAvailableCount - confirmedWLSeatInBc.get(bookingCode);
						}
						if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationType.WL.toString())
								&& resSeg.getFlightSegId().equals(wlRequest.getFlightSegId())
								&& mapPnrSegIdAndBookingClass.get(resSeg.getPnrSegId()) != null
								&& (reservation.getTotalPaxAdultCount()
										+ reservation.getTotalPaxChildCount()) <= (wlRequest.getSegmentAvailabileCount()
												- confirmedWLSeats)
								&& reservation.getTotalPaxInfantCount() <= (wlRequest.getSegmentAvailableInfantCount()
										- confirmedWLInfants)
								&& bcAvailableCount >= (reservation.getTotalPaxAdultCount()
										+ reservation.getTotalPaxChildCount())) {

							wlRequest.setTotalAdultCount(
									reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount());
							wlRequest.setTotalInfantCount(reservation.getTotalPaxInfantCount());
							wlRequest.setBookingClass(mapPnrSegIdAndBookingClass.get(resSeg.getPnrSegId()).getBookingCode());

							// Update the Inventory
							ReservationModuleUtils.getFlightInventoryResBD().confirmWaitListedSeats(wlRequest,
									reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED));

							resSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
							pnrSegmentIds.add(resSeg.getPnrSegId());

							reservationWLPriority.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
							updatedWLPriorities.add(reservationWLPriority);
							pnrs.add(reservationWLPriority.getPnr());

							// Save reservation
							ReservationProxy.saveReservation(reservation);

							// ADD reservation Audit
							ReservationAudit reservationAudit = new ReservationAudit();
							ReservationAudit.createReservationAudit(reservationAudit, reservation.getPnr(), null, credentialsDTO);

							reservationAudit.setModificationType(AuditTemplateEnum.MODIFIED_SEGMENT.getCode());
							reservationAudit.setUserNote("Confirmed Wait Listed Segment");

							reservationAudit.addContentMap(
									AuditTemplateEnum.TemplateParams.ModifiedSegment.OLD_SEGMENT_INFORMATION,
									ReservationCoreUtils.getSegmentInformation(pnrSegmentIds));
							reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
							auditorBD.audit(reservationAudit, reservationAudit.getContentMap());

							confirmedWLSeats = confirmedWLSeats + reservation.getTotalPaxAdultCount()
									+ reservation.getTotalPaxChildCount();
							confirmedWLInfants = confirmedWLInfants + reservation.getTotalPaxInfantCount();

							if (confirmedWLSeatInBc.get(bookingCode) == null) {
								confirmedWLSeatInBc.put(bookingCode,
										(reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount()));
							} else {
								confirmedWLSeatInBc.put(bookingCode, (confirmedWLSeatInBc.get(bookingCode)
										+ reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount()));
							}
						}
					}

					// }
				}
			}
		}
		reservationDAO.saveWLReservationPriority(updatedWLPriorities);
		// send notification to agent
		if (!pnrs.isEmpty()) {
			emailToAgentForConfirmedWaitListedPNR(pnrs);
		}
		WLConfirmationResponseDTO response = new WLConfirmationResponseDTO();
		response.setConfirmedAdultCount(confirmedWLSeats);
		response.setConfirmedInfantCount(confirmedWLInfants);
		return response;
	}

	/**
	 * following function Re-Organize the journey sequence, segment sequence,OND group id & OND return group id when
	 * re-quoted without all available CNF segment.
	 * 
	 */
	public static void arrangeResOnDSequenceOnRequote(Reservation reservation) throws ModuleException {

		if (reservation != null && reservation.getSegments() != null && reservation.getSegments().size() > 0) {

			Collection<ReservationSegmentDTO> segmentView = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
					.getPnrSegmentsView(reservation.getPnr());

			ReservationSegmentDTO[] resSegs = SortUtil.sortByDepDate(segmentView);

			// contains pnrSegId to be skipped [past OND Segment/Segment Status not CNF]
			List<Integer> skipONDList = new ArrayList<Integer>();
			List<Integer> sortedOndPnrSegIdList = new ArrayList<Integer>();

			// existing ID's which cannot be touched when grouping and ordering the OND segments.
			List<Integer> skipONDSeqList = new ArrayList<Integer>();
			List<Integer> skipONDGroupIdList = new ArrayList<Integer>();
			List<Integer> skipRetGroupIdList = new ArrayList<Integer>();
			List<Integer> skipJourneySeqList = new ArrayList<Integer>();

			Map<Integer, List<Integer>> journeySeqMap = new HashMap<Integer, List<Integer>>();
			Map<Integer, List<Integer>> ondGroupIdMap = new HashMap<Integer, List<Integer>>();
			Map<Integer, List<Integer>> ondRetGroupIdMap = new HashMap<Integer, List<Integer>>();

			Date currentDate = CalendarUtil.getCurrentZuluDateTime();

			int nextOndSeq = 0;
			int nextOndGroupId = 0;
			int nextRetOndGroupId = 0;
			int nextJourneySeq = 0;

			if (resSegs != null && resSegs.length > 1) {

				List<Integer> journeySeqSegIdList = new ArrayList<Integer>();
				List<Integer> ondGroupSegIdList = new ArrayList<Integer>();
				List<Integer> ondRetGroupSegIdList = new ArrayList<Integer>();

				for (int i = 0; i < resSegs.length; i++) {
					ReservationSegmentDTO segViewDTO = resSegs[i];

					if (segViewDTO != null) {
						Integer pnrSegId = segViewDTO.getPnrSegId();
						sortedOndPnrSegIdList.add(pnrSegId);

						getOndGroupingMap(pnrSegId, ondGroupIdMap, segViewDTO.getFareGroupId());
						getOndGroupingMap(pnrSegId, ondRetGroupIdMap, segViewDTO.getReturnOndGroupId());
						getOndGroupingMap(pnrSegId, journeySeqMap, segViewDTO.getJourneySeq());

						// only CNF & future ONDs should be considered
						if (!ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segViewDTO.getStatus())
								|| currentDate.compareTo(segViewDTO.getZuluDepartureDate()) > 0) {
							skipONDSeqList.add(segViewDTO.getSegmentSeq());
							skipONDGroupIdList.add(segViewDTO.getFareGroupId());
							skipRetGroupIdList.add(segViewDTO.getReturnOndGroupId());
							skipJourneySeqList.add(segViewDTO.getJourneySeq());

							skipONDList.add(pnrSegId);
						}
					}

				}

				Iterator<Integer> sortedSegIdItr = sortedOndPnrSegIdList.iterator();
				Integer prevPnrSegId = -1;
				while (sortedSegIdItr.hasNext()) {

					Integer segId = sortedSegIdItr.next();
					Iterator<ReservationSegment> segSetItr = reservation.getSegments().iterator();
					while (segSetItr.hasNext()) {
						ReservationSegment resSegment = segSetItr.next();
						Integer pnrSegId = resSegment.getPnrSegId();
						if (segId.equals(pnrSegId)) {
							List<Integer> groupSegIdList = new ArrayList<Integer>();
							if (prevPnrSegId != -1) {
								groupSegIdList.add(prevPnrSegId);
							}

							groupSegIdList.add(pnrSegId);

							if (pnrSegId != null && !skipONDList.contains(pnrSegId)) {

								do {
									nextOndSeq++;
								} while (skipONDSeqList.contains(nextOndSeq));

								// generate next OND group id
								nextOndGroupId = getNestOndGroupId(groupSegIdList, ondGroupIdMap, skipONDGroupIdList,
										nextOndGroupId);

								// generate next OND return group id
								nextRetOndGroupId = getNestOndGroupId(groupSegIdList, ondRetGroupIdMap, skipRetGroupIdList,
										nextRetOndGroupId);

								// generate next journey sequence id
								nextJourneySeq = getNestOndGroupId(groupSegIdList, journeySeqMap, skipJourneySeqList,
										nextJourneySeq);

								resSegment.setSegmentSeq(nextOndSeq);
								resSegment.setOndGroupId(nextOndGroupId);
								resSegment.setReturnOndGroupId(nextRetOndGroupId);
								resSegment.setJourneySequence(nextJourneySeq);

							}

							prevPnrSegId = pnrSegId;
						}

					}

				}

			}

			ReservationProxy.saveReservation(reservation);

		}

	}

	public static ServiceTaxContainer getApplicableServiceTax(EXTERNAL_CHARGES externalCharge,
			ReservationSegmentDTO reservationSegmentDTO) throws ModuleException {
		ServiceTaxContainer applicableServiceTax = null;

		Set<EXTERNAL_CHARGES> taxableCharges = new HashSet<EXTERNAL_CHARGES>();

		if (reservationSegmentDTO != null) {
			boolean appParamEnabled;
			switch (externalCharge) {
			case JN_OTHER:
				appParamEnabled = AppSysParamsUtil.isJnTaxApplicableForHandlingCCAdjustmentCharges();
				taxableCharges.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
				taxableCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
				break;

			default:
				appParamEnabled = false;
				break;
			}

			if (appParamEnabled) {

				String ondCode = reservationSegmentDTO.getSegmentCode();

				Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
				colEXTERNAL_CHARGES.add(externalCharge);

				ExternalChgDTO externalChgDTO = BeanUtils
						.getFirstElement(ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null).values());

				boolean isChargeExists = AirproxyModuleUtils.getChargeBD().isValidChargeExists(externalChgDTO.getChargeCode(),
						ondCode);

				if (isChargeExists) {
					applicableServiceTax = new ServiceTaxContainer();
					applicableServiceTax.setExternalCharge(externalCharge);
					applicableServiceTax.setTaxApplicable(isChargeExists);
					applicableServiceTax.setTaxRatio(externalChgDTO.getRatioValue());
					applicableServiceTax.setTaxableExternalCharges(taxableCharges);
					applicableServiceTax
							.setTaxApplyingFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(reservationSegmentDTO));
				}

			}
		}

		return applicableServiceTax;
	}

	public static ServiceTaxContainer getApplicableServiceTax(EXTERNAL_CHARGES externalCharge,
			AvailableIBOBFlightSegment availableIBOBFlightSegment) throws ModuleException {
		ServiceTaxContainer applicableServiceTax = null;

		Set<EXTERNAL_CHARGES> taxableCharges = new HashSet<EXTERNAL_CHARGES>();

		if (availableIBOBFlightSegment != null) {
			boolean appParamEnabled;
			switch (externalCharge) {
			case JN_OTHER:
				appParamEnabled = AppSysParamsUtil.isJnTaxApplicableForHandlingCCAdjustmentCharges();
				taxableCharges.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
				taxableCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
				break;

			default:
				appParamEnabled = false;
				break;
			}

			if (appParamEnabled) {

				String ondCode = availableIBOBFlightSegment.getOndCode();

				FlightSegmentDTO flightSegmentDTO = null;
				if (availableIBOBFlightSegment instanceof AvailableFlightSegment) {
					flightSegmentDTO = ((AvailableFlightSegment) availableIBOBFlightSegment).getFlightSegmentDTO();
				} else if (availableIBOBFlightSegment instanceof AvailableConnectedFlight) {
					flightSegmentDTO = ((AvailableConnectedFlight) availableIBOBFlightSegment).getFirstFlightSegment();
				}

				Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
				colEXTERNAL_CHARGES.add(externalCharge);

				ExternalChgDTO externalChgDTO = BeanUtils
						.getFirstElement(ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null).values());

				boolean isChargeExists = AirproxyModuleUtils.getChargeBD().isValidChargeExists(externalChgDTO.getChargeCode(),
						ondCode);

				if (isChargeExists) {
					applicableServiceTax = new ServiceTaxContainer();
					applicableServiceTax.setExternalCharge(externalCharge);
					applicableServiceTax.setTaxApplicable(isChargeExists);
					applicableServiceTax.setTaxRatio(externalChgDTO.getRatioValue());
					applicableServiceTax.setTaxableExternalCharges(taxableCharges);
					applicableServiceTax.setTaxApplyingFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentDTO));
				}

			}
		}

		return applicableServiceTax;
	}

	/**
	 * following utility used for grouping the existing OND Group, Return Group & Journey sequence from current
	 * reservation segment
	 * 
	 */
	private static void getOndGroupingMap(Integer pnrSegId, Map<Integer, List<Integer>> ondGroupMap, Integer sequence) {
		List<Integer> journeySeqSegIdList = null;
		if (ondGroupMap != null) {

			if (ondGroupMap.get(sequence) == null) {
				journeySeqSegIdList = new ArrayList<Integer>();
			} else {
				journeySeqSegIdList = ondGroupMap.get(sequence);
			}
			journeySeqSegIdList.add(pnrSegId);

			ondGroupMap.put(sequence, journeySeqSegIdList);
		}

	}

	/**
	 * used to generates next OND Group, Return Group & Journey sequence for the re-quote flow
	 * 
	 */
	private static int getNestOndGroupId(List<Integer> groupSegIdList, Map<Integer, List<Integer>> ondGroupMap,
			List<Integer> skipONDGroupIdList, int nextOndGroupId) {

		boolean increamentCount = true;
		if (groupSegIdList.size() > 1) {
			for (Integer ondGroupId : ondGroupMap.keySet()) {
				List<Integer> ondGroupSegIdTemp = ondGroupMap.get(ondGroupId);
				if (ondGroupSegIdTemp.containsAll(groupSegIdList)) {
					increamentCount = false;
					break;
				}
			}
		}

		if (increamentCount) {
			do {
				nextOndGroupId++;
			} while (skipONDGroupIdList.contains(nextOndGroupId));
		}
		return nextOndGroupId;

	}

	public static BigDecimal getTotalReturnJourneyFare(Integer pnrPaxId, Integer pnrPaxFareId) {
		BigDecimal totalRetJourneyFare = ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_DAO
				.getTotalReturnJourneyFare(pnrPaxId, pnrPaxFareId);
		return totalRetJourneyFare;
	}

	public static void addServiceTaxAdjustmentBalance(Reservation reservation, CredentialsDTO credentialsDTO,
			Integer pnrPaxFareId, String userNotes, AdjustCreditBO adjustCreditBO,
			ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		if (serviceTaxRS != null && serviceTaxRS.getPaxWiseServiceTaxes() != null) {
			for (ReservationPax resPax : reservation.getPassengers()) {
				List<ServiceTaxTO> serviceTaxTOs = serviceTaxRS.getPaxWiseServiceTaxes().get(resPax.getPaxSequence());

				if (serviceTaxTOs != null && !serviceTaxTOs.isEmpty()) {
						Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
						ReservationPaxFare reservationPaxFare;
						while (itReservationPaxFare.hasNext()) {
							reservationPaxFare = itReservationPaxFare.next();

							if (reservationPaxFare.getPnrPaxFareId().equals(pnrPaxFareId)) {
								Iterator<ReservationPaxFareSegment> resPaxFareSegIterator = reservationPaxFare.getPaxFareSegments().iterator();
								while(resPaxFareSegIterator.hasNext()){
									ReservationPaxFareSegment reservationPaxFareSegment = resPaxFareSegIterator.next();
									for (ServiceTaxTO serviceTaxTO : serviceTaxTOs) {
										if(reservationPaxFareSegment.getSegment().getFlightSegId().toString().equals(
												serviceTaxTO.getFlightRefNumber().split("\\$")[2])){
											PaxOndChargeDTO paxOndChargeDTO = new PaxOndChargeDTO();
											paxOndChargeDTO.setTaxableAmount(serviceTaxTO.getTaxableAmount());
											paxOndChargeDTO.setNonTaxableAmount(serviceTaxTO.getNonTaxableAmount());
											paxOndChargeDTO.setTaxDepositCountryCode(serviceTaxRS.getServiceTaxDepositeCountryCode());
											paxOndChargeDTO.setTaxDepositStateCode(serviceTaxRS.getServiceTaxDepositeStateCode());
											paxOndChargeDTO
													.setTaxAppliedCategory(ReservationPaxOndCharge.TAX_APPLIED_FOR_NON_TICKETING_CATEGORY);

											ReservationPaxOndCharge reservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
													serviceTaxTO.getAmount(), null, new Integer(serviceTaxTO.getChargeRateId()),
													ReservationInternalConstants.ChargeGroup.TAX, credentialsDTO, false, 0, null, null,
													paxOndChargeDTO, chgTnxGen.getTnxSequence(resPax.getPnrPaxId()));

											BigDecimal[] totalCharges = reservationPaxFare.getTotalChargeAmounts();

											// Getting the total charge
											ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
													reservationPaxOndCharge.getAmount(), totalCharges, reservationPaxOndCharge.getDiscount(),
													reservationPaxOndCharge.getAdjustment());

											// Setting the total charge
											reservationPaxFare.setTotalChargeAmounts(totalCharges);
											// Adding the new charge
											reservationPaxFare.addCharge(reservationPaxOndCharge);

											// Set reservation and passenger total amounts
											ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

											adjustCreditBO.addAdjustment(reservation.getPnr(), resPax.getPnrPaxId(), userNotes,
													reservationPaxOndCharge, credentialsDTO, reservation.isEnableTransactionGranularity());
										}
									}
									
								}
								
							}

						}

				}

			}
		}

	}
	
	private static void cancelLMSBlockedCredits(Collection<String> pnrCollection) {
		log.info("########## Starting Cancel LMSBlocked Credits For Expire ON-HOLD ###########");
		try {

			if (pnrCollection != null && !pnrCollection.isEmpty()) {

				Collection<Integer> tempTnxIds = ReservationModuleUtils.getReservationBD()
						.getLMSBlockedTnxTempPayIdsByPnr(pnrCollection);

				if (tempTnxIds != null && !tempTnxIds.isEmpty()) {
					ReservationModuleUtils.getReservationBD().cancelFailedLMSBlockedCreditPaymentsByTempIds(tempTnxIds);
				}
			}

		} catch (Exception e) {
			log.error("Cancel LMSBlocked Credits Operation ::: FAILED ");
		}
		log.info("########## End Cancel LMSBlocked Credits For Expire ON-HOLD ###########");
	}
	@SuppressWarnings("unchecked")
	public static void saveBlacklistedPnrPaxIfExist(Reservation reservation, String reasonToAllow) throws ModuleException {
		if(AppSysParamsUtil.isBlacklistpassengerCheckEnabled()){
			
			List<ReservationPax> paxList = new ArrayList<ReservationPax>(reservation.getPassengers());
			if(paxList!=null){
				 try {
					removeAlreadyBlacklistedPax(reservation.getPnr(), paxList); 
					List<BlacklisPaxReservationTO> paxToList = BlacklistPAXUtil.convertReservationPaxToBlacklisPaxReservationTO(paxList);
					List<BlacklistPAX> blacklistPaxList = ReservationModuleUtils.getBlacklisPAXBD().getBalcklistedPaxReservation(paxToList, false);
					if(blacklistPaxList != null){
						List<BlacklistReservation> blPnrPaxList = getBlacklistedPnrPaxList(paxToList,blacklistPaxList,reasonToAllow);
						ReservationModuleUtils.getBlacklisPAXBD().saveBlacklistedPnrPassengers(blPnrPaxList);
					}
					
				 } catch (ModuleException e) {					
					throw new ModuleException(e, e.getExceptionCode());
				}
			}
		}
	}

	private static List<BlacklistReservation> getBlacklistedPnrPaxList(List<BlacklisPaxReservationTO> paxToList,
			List<BlacklistPAX> blacklistPaxList, String reasonToAllow) throws ModuleException {
		
		List<BlacklistReservation> blPnrPaxList = new ArrayList<BlacklistReservation>();
		Map<String, BlacklisPaxReservationTO> map = new HashMap<String, BlacklisPaxReservationTO>();
		
		for(BlacklisPaxReservationTO paxTO : paxToList){
			if(paxTO !=null){
			
				map.put(paxTO.getFullName(), paxTO);
			}
		}
		for(BlacklistPAX blPax : blacklistPaxList){
			BlacklistReservation blPnrPax = new BlacklistReservation();
			blPnrPax.setBlacklistPaxId(blPax.getBlacklistPAXCriteriaID().intValue());
			blPnrPax.setIsActioned("N");
			blPnrPax.setReasonToAllow(reasonToAllow);
			blPnrPax.setPnrPaxId(map.get(blPax.getFullName()).getPnrPaxId());
			
			blPnrPaxList.add(blPnrPax);
		}
		
		return blPnrPaxList;
	}
	
	private static void removeAlreadyBlacklistedPax(String pnr, List<ReservationPax> paxList) throws ModuleException{
		try {
			if(pnr != null){
			Collection<Integer> blacklistedPnrPaxId = ReservationModuleUtils.getBlacklisPAXBD().getBLackListPaxIdVsPnrPaxId(pnr);
			if(blacklistedPnrPaxId!=null && !blacklistedPnrPaxId.isEmpty()){
			Iterator<ReservationPax> itr = paxList.iterator();
			while (itr.hasNext()) {
				ReservationPax pax = itr.next();
				if (blacklistedPnrPaxId.contains(pax.getPnrPaxId())) {
					itr.remove();
				}
			}
			}
		}
		} catch (Exception e) {			
			throw new ModuleException(e, e.getMessage());
		}
		
	}
	public static ExternalChgDTO getQuotedChargeForModificationHandlingFee(String agentCode, Integer operationalMode)
			throws ModuleException {
		ExternalChgDTO externalChgDTO = null;
		if (AppSysParamsUtil.applyHandlingFeeOnModification() && !StringUtil.isNullOrEmpty(agentCode)) {
			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);

			externalChgDTO = BeanUtils
					.getFirstElement(ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, agentCode, null).values());

			TravelAgentBD travelAgentBD = ReservationModuleUtils.getTravelAgentBD();

			Collection<String> colAgentCodes = new ArrayList<String>();
			colAgentCodes.add(agentCode);
			Collection<Agent> colAgent = travelAgentBD.getAgents(colAgentCodes);

			Agent agent = BeanUtils.getFirstElement(colAgent);
			ReservationApiUtils.updateHandlingFeeFromAgentOnModification(agent, externalChgDTO, operationalMode);
			externalChgDTO.setOperationMode(operationalMode);
		}

		return externalChgDTO;

	}
	
	public static void addHandlingFeeForAdjustmentBalance(Reservation reservation, CredentialsDTO credentialsDTO,
			Integer pnrPaxFareId, String userNotes, AdjustCreditBO adjustCreditBO, ChargeTnxSeqGenerator chgTnxGen,
			Map<Integer, List<ExternalChgDTO>> handlingFeeByPax) throws ModuleException {

		if (handlingFeeByPax != null && !handlingFeeByPax.isEmpty()) {

			for (ReservationPax resPax : reservation.getPassengers()) {
				List<ExternalChgDTO> charges = handlingFeeByPax.get(resPax.getPaxSequence());
				if (charges != null && !charges.isEmpty()) {
					Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
					ReservationPaxFare reservationPaxFare;
					while (itReservationPaxFare.hasNext()) {
						reservationPaxFare = itReservationPaxFare.next();

						if (reservationPaxFare.getPnrPaxFareId().equals(pnrPaxFareId)) {
							Iterator<ReservationPaxFareSegment> resPaxFareSegIterator = reservationPaxFare.getPaxFareSegments()
									.iterator();
							while (resPaxFareSegIterator.hasNext()) {
								ReservationPaxFareSegment reservationPaxFareSegment = resPaxFareSegIterator.next();
								for (ExternalChgDTO charge : charges) {
									if (reservationPaxFareSegment.getSegment().getFlightSegId().equals(charge.getFlightSegId())) {
										ReservationPaxOndCharge reservationPaxOndCharge = TOAssembler
												.createReservationPaxOndCharge(charge.getAmount(), null, charge.getChgRateId(),
														ReservationInternalConstants.ChargeGroup.SUR, credentialsDTO, false, 0,
														null, null, null, chgTnxGen.getTnxSequence(resPax.getPnrPaxId()));
										reservationPaxOndCharge.setOperationType(charge.getOperationMode());
										BigDecimal[] totalCharges = reservationPaxFare.getTotalChargeAmounts();

										// Getting the total charge
										ReservationCoreUtils.captureOndCharges(reservationPaxOndCharge.getChargeGroupCode(),
												reservationPaxOndCharge.getAmount(), totalCharges,
												reservationPaxOndCharge.getDiscount(), reservationPaxOndCharge.getAdjustment());

										// Setting the total charge
										reservationPaxFare.setTotalChargeAmounts(totalCharges);
										// Adding the new charge
										reservationPaxFare.addCharge(reservationPaxOndCharge);

										// Set reservation and passenger total amounts
										ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);

										adjustCreditBO.addAdjustment(reservation.getPnr(), resPax.getPnrPaxId(), userNotes,
												reservationPaxOndCharge, credentialsDTO,
												reservation.isEnableTransactionGranularity());
									}

								}

							}

						}

					}

				}

			}
		}

	}
}