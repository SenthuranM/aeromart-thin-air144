/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.ExternalGenericChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalServicePenaltyChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalServiceTaxChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.airreservation.core.util.ETicketGenerator;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.model.SysGenPromotionCriteria;

/**
 * Command for confirming the reservation for an already saved reservation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="confirmReservation"
 */
public class ConfirmReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ConfirmReservation.class);

	/**
	 * Execute method of the ConfirmReservation command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("Inside execute");
		}

		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Boolean triggerPaidAmountError = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR);
		Boolean triggerPnrForceConfirmed = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		Boolean acceptMorePaymentsThanItsCharges = (Boolean) this
				.getParameter(CommandParamNames.ACCEPT_MORE_PAYMENTS_THAN_ITS_CHARGES);
		Boolean isPaymentForAcquirCredit = (Boolean) this.getParameter(CommandParamNames.IS_PAYMENT_FOR_ACQUIRE_CREDITS);
		Map passengerPayment = (Map) this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		String lastCurrencyCode = (String) this.getParameter(CommandParamNames.LAST_CURRENCY_CODE);
		Boolean isOtherCarrierPaxCreditUse = (Boolean) this.getParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED);
		Collection<PaymentInfo> colPaymentInfo = (Collection<PaymentInfo>) this.getParameter(CommandParamNames.PAYMENT_INFO);
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Collection<ReservationPax> etGenerationEligiblePaxForAdl = new ArrayList<ReservationPax>();

		// Checking params
		this.validateParams(pnr, triggerPaidAmountError, triggerPnrForceConfirmed, passengerPayment,
				acceptMorePaymentsThanItsCharges, isPaymentForAcquirCredit, version, credentialsDTO);

		if (reservation == null) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadEtickets(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
		}

		// Check Constraints
		this.checkConstraints(reservation, passengerPayment, version);

		boolean isOriginalReservationOnHold = reservation.getStatus()
				.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD) ? true : false;

		// Adds any external charges for confirmed passengers
		this.addExternalCharges(reservation, passengerPayment, credentialsDTO);

		// Sets the passenger information
		Collection<ReservationPax> confirmedPassengers = this.trackPassengerInformation(reservation, passengerPayment,
				triggerPaidAmountError, triggerPnrForceConfirmed, acceptMorePaymentsThanItsCharges, isPaymentForAcquirCredit,
				credentialsDTO);

		Collection<ReservationPax> etGenerationEligiblePax = ETicketGenerator.getETGenerationEligiblePaxInCNFBookings(reservation,
				passengerPayment, triggerPaidAmountError, triggerPnrForceConfirmed, acceptMorePaymentsThanItsCharges,
				isPaymentForAcquirCredit, credentialsDTO);

		Collection<Integer> confirmedPaxIds = getPnrPaxIds(confirmedPassengers);
		Collection<Integer> confirmedPassengerIds = new ArrayList<Integer>();
		Collection<Integer> splitingPassengerIds = new ArrayList<Integer>();

		if (!reservation.getStatus().equals(ReservationPaxStatus.CONFIRMED)) {
			confirmedPassengerIds.addAll(confirmedPaxIds);
		}

		if (reservation.getStatus().equals(ReservationPaxStatus.CONFIRMED) && confirmedPassengerIds.size() == 0) {
			splitingPassengerIds.addAll(confirmedPaxIds);
		}

		// update the group pnr if it use other carrier pax
		if (isOtherCarrierPaxCreditUse) {
			reservation.setOriginatorPnr(pnr);
		}

		// Saves the reservation
		ReservationProxy.saveReservation(reservation);

		if (isOriginalReservationOnHold && !triggerPnrForceConfirmed) {
			// Adding adl add record if pnl already sent
			ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePax, null);
		}
		etGenerationEligiblePaxForAdl = etGenerationEligiblePax;

		if (isOriginalReservationOnHold) {
			if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.ONHOLD_PAY_CONFIRM.getCode());
				TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
			}
		}

		Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
		Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = null;

		// save own booking e-ticket numbers
		if ((reservation.getOriginatorPnr() == null || (reservation.getOriginatorPnr() != null
				&& isOtherCarrierPaxCreditUse != null && isOtherCarrierPaxCreditUse.booleanValue()))) {

			CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
			tktInfoDto.setBSPPayment(ReservationApiUtils.isBSPpayment(colPaymentInfo));

			if (!etGenerationEligiblePax.isEmpty()) {
				if (isOriginalReservationOnHold) {
					// Generate ET when doing payment for OHD booking
					eTicketInfo = ETicketBO.generateIATAETicketNumbersForFreshBooking(etGenerationEligiblePax, reservation,
							tktInfoDto);
					ETicketBO.saveETickets(eTicketInfo, etGenerationEligiblePax, reservation);
					etGenerationEligiblePaxForAdl = etGenerationEligiblePax;

				} else {
					// Generate ET for previously forced confirmed pax in actual payment
					List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
					List<Integer> confirmedPaxSeqsAfterChange = ReservationApiUtils.getPaxSeqs(etGenerationEligiblePax);
					Collection<ReservationPax> confirmedPaxByOperation = ReservationApiUtils.getConfirmedPaxByOperation(
							forcedConfirmedPaxSeqsBeforeChange, confirmedPaxSeqsAfterChange, reservation);

					eTicketInfo = ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto);
					modifiedPaxfareSegmentEtickets = ETicketBO.updateETickets(eTicketInfo, confirmedPaxByOperation, reservation,
							true);
					etGenerationEligiblePaxForAdl = confirmedPaxByOperation;
					// TEMP FIX
					ADLNotifyer.recordReservation(reservation, AdlActions.D, etGenerationEligiblePaxForAdl, null);
				}
			} else if (isOriginalReservationOnHold && triggerPnrForceConfirmed.booleanValue()
					&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {

				// Generate ET for pax who haven't balance to pay when force confirm a OHD reservation
				List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getPaxSeqs(reservation);
				List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
				Collection<ReservationPax> confirmedPaxByOperation = ReservationApiUtils
						.getConfirmedPaxByOperationUsingForceCNFSeqs(forcedConfirmedPaxSeqsBeforeChange,
								forcedConfirmedPaxSeqsAfterChange, reservation);
				eTicketInfo = ETicketBO.generateIATAETicketNumbersForFreshBooking(confirmedPaxByOperation, reservation,
						tktInfoDto);
				ETicketBO.saveETickets(eTicketInfo, confirmedPaxByOperation, reservation);
				etGenerationEligiblePaxForAdl = confirmedPaxByOperation;
			}
		} else {
			if (!etGenerationEligiblePax.isEmpty()) {
				if (!isOriginalReservationOnHold) {
					List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
					List<Integer> confirmedPaxSeqsAfterChange = ReservationApiUtils.getPaxSeqs(etGenerationEligiblePax);
					etGenerationEligiblePaxForAdl = ReservationApiUtils.getConfirmedPaxByOperation(
							forcedConfirmedPaxSeqsBeforeChange, confirmedPaxSeqsAfterChange, reservation);
				}
			} else {
				List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
				List<Integer> confirmedPaxSeqsAfterChange = ReservationApiUtils.getPaxSeqs(etGenerationEligiblePax);
				etGenerationEligiblePaxForAdl = ReservationApiUtils.getConfirmedPaxByOperation(forcedConfirmedPaxSeqsBeforeChange,
						confirmedPaxSeqsAfterChange, reservation);
			}
		}

		if (isOriginalReservationOnHold) {
			activateSystemGeneratedPromotion(reservation);
		} else {
			if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
				Collection<Integer> affectingPnrSegIds = new ArrayList<Integer>();
				Collection<Integer> affectingFltSegIds = new ArrayList<Integer>();
				for (ReservationPax resPax : reservation.getPassengers()) {
					Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
					Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
					ReservationPaxFare reservationPaxFare;
					ReservationPaxFareSegment reservationPaxFareSegment;

					while (itReservationPaxFare.hasNext()) {
						reservationPaxFare = itReservationPaxFare.next();
						itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

						while (itReservationPaxFareSegment.hasNext()) {
							reservationPaxFareSegment = itReservationPaxFareSegment.next();
							for (ReservationPaxFareSegmentETicket resPaxFareSegmentETicket : modifiedPaxfareSegmentEtickets) {
								if (resPaxFareSegmentETicket.getPnrPaxFareSegId()
										.equals(reservationPaxFareSegment.getPnrPaxFareSegId())) {
									affectingPnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
									// affectingFltSegIds.add(reservationPaxFareSegment.getSegment().getFlightSegId());
								}
							}
						}
					}
				}
				ADLNotifyer.recordExchangedETicketForAdl(modifiedPaxfareSegmentEtickets, reservation,
						etGenerationEligiblePaxForAdl);
			}
			for (ReservationPax resPax : reservation.getPassengers()) {
				if (!resPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
					Iterator<ReservationPaxFare> itReservationPaxFare = resPax.getPnrPaxFares().iterator();
					Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
					ReservationPaxFare reservationPaxFare;
					ReservationPaxFareSegment reservationPaxFareSegment;
					String pnlAdlStat;
					boolean adlNotified = false;

					while (itReservationPaxFare.hasNext()) {
						reservationPaxFare = itReservationPaxFare.next();
						itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

						while (itReservationPaxFareSegment.hasNext()) {
							reservationPaxFareSegment = itReservationPaxFareSegment.next();
							pnlAdlStat = BeanUtils.nullHandler(reservationPaxFareSegment.getPnlStat());
							// Only adding if PNL didn't send
							if (pnlAdlStat.equals(PNLConstants.PaxPnlAdlStates.PNL_RES_STAT)) {
								ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePaxForAdl,
										reservation.getStatus());
								adlNotified = true;
							}
						}
					}

					if (adlNotified) {
						break;
					}
				}
			}
		}

		// Adding adl add record if pnl already sent
		ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePaxForAdl, null);

		// Release the on hold seats
		this.releaseonHoldSeats(reservation, isOriginalReservationOnHold, confirmedPaxIds);

		ReservationPaymentMetaTO reservationPaymentMetaTO = TnxGranularityReservationUtils
				.getReservationPaymentMetaTO(reservation);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		Map<Integer, Map<String, BigDecimal>> lmsPaxProductRedeemedAmount = (Map<Integer, Map<String, BigDecimal>>) this
				.getParameter(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS);

		if (lmsPaxProductRedeemedAmount != null && lmsPaxProductRedeemedAmount.size() > 0) {
			Map<Integer, Map<String, BigDecimal>> lmsPnrPaxIdRedeemedAmounts = new HashMap<Integer, Map<String, BigDecimal>>();
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				if (lmsPaxProductRedeemedAmount.containsKey(reservationPax.getPaxSequence())) {
					lmsPnrPaxIdRedeemedAmounts.put(reservationPax.getPnrPaxId(),
							lmsPaxProductRedeemedAmount.get(reservationPax.getPaxSequence()));
				}
			}

			response.addResponceParam(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS, lmsPnrPaxIdRedeemedAmounts);
		}
		response.addResponceParam(CommandParamNames.RESERVATION, reservation);
		response.addResponceParam(CommandParamNames.RESERVATION_PAYMENT_META_TO, reservationPaymentMetaTO);
		response.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());
		response.addResponceParam(CommandParamNames.SPLITTED_PAX_IDS, splitingPassengerIds);

		if (log.isDebugEnabled()) {
			log.debug("Exit execute");
		}
		return response;
	}

	/**
	 * Return reservation passenger id(s)
	 * 
	 * @param confirmedPassengers
	 * @return
	 */
	private Collection<Integer> getPnrPaxIds(Collection<ReservationPax> confirmedPassengers) {
		Collection<Integer> pnrPaxIds = new HashSet<Integer>();

		for (ReservationPax reservationPax : confirmedPassengers) {
			pnrPaxIds.add(reservationPax.getPnrPaxId());
		}

		return pnrPaxIds;
	}

	/**
	 * Adds external charges
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @throws ModuleException
	 */
	private void addExternalCharges(Reservation reservation, Map<Integer, PaymentAssembler> passengerPayment,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
		ExternalGenericChargesBL.reflectExternalChgForAPaymentBooking(reservation, passengerPayment, credentialsDTO, chgTnxGen);
		ExternalServiceTaxChargesBL.reflectExternalChgForAPaymentBooking(reservation, passengerPayment, credentialsDTO,
				chgTnxGen);
		ExternalServicePenaltyChargesBL.reflectExternalChgForAPaymentBooking(reservation, passengerPayment, credentialsDTO,
				chgTnxGen);
	}

	/**
	 * Check Constraints
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @param version
	 * @throws ModuleException
	 */
	private void checkConstraints(Reservation reservation, Map<Integer, PaymentAssembler> passengerPayment, Long version)
			throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		// Check if it's valid payments or not
		ReservationCoreUtils.checkValidPaxPaymentsForReservation(reservation, passengerPayment, null);
	}

	/**
	 * Release on hold seats
	 * 
	 * @param reservation
	 * @param isOriginalReservationOnHold
	 * @param confirmedPassengers
	 * @throws ModuleException
	 */
	private void releaseonHoldSeats(Reservation reservation, boolean isOriginalReservationOnHold,
			Collection<Integer> confirmedPassengers) throws ModuleException {

		// If the original reservation is a on hold reservation
		// Remember if the reservation is confirm we don't need to carry out these stuff
		if (isOriginalReservationOnHold && confirmedPassengers.size() > 0) {
			ReservationBO.moveOnholdSeatsToConfirm(reservation.getPassengers(), confirmedPassengers, null);
		}
	}

	/**
	 * Track Passenger information
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @param triggerPaidAmountError
	 * @param triggerPnrForceConfirmed
	 * @param acceptMorePaymentsThanItsCharges
	 * @param isPaymentForAcquirCredit
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationPax> trackPassengerInformation(Reservation reservation, Map<Integer, IPayment> passengerPayment,
			Boolean triggerPaidAmountError, Boolean triggerPnrForceConfirmed, Boolean acceptMorePaymentsThanItsCharges,
			Boolean isPaymentForAcquirCredit, CredentialsDTO credentialsDTO) throws ModuleException {
		Collection<ReservationPax> confirmedPassengers = new ArrayList<ReservationPax>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		boolean confirmReservation;

		// Forcing the pnr to be confirmed if this is enabled
		if (triggerPnrForceConfirmed.booleanValue()) {
			// This is force confirmation therefore
			// If IBE user(s) exist should change the channel and the agent code
			if (AppSysParamsUtil.isOwnerAgentTransferEnabled()) {
				reservation.getAdminInfo().setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
				reservation.getAdminInfo().setOwnerAgentCode(credentialsDTO.getAgentCode());
			}
			// Updating the reservation passenger information
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				if (!reservationPax.getStatus().equals(ReservationPaxStatus.CONFIRMED)
						&& !reservation.getStatus().equals(ReservationStatus.CONFIRMED)) {
					reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
					confirmedPassengers.add(reservationPax);
				}
			}

			reservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
		} else {
			// If by any chance for a CNF booking user tries to do another payment.
			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();

					// You might see this call as an idiot's call. It is not. It checks whether
					// the user is carrying out a payment exceeding the due amount. If it is this will
					// throw an exception. We need this to cater double payment requests. Please visit AARESAA-1233
					// Other thing is from seat edit selection user could add/modify seat with out performing any
					// payment
					// in this case the reservation should be left on with CNF state. It should not move to OHD state at
					// any
					// cost. Please visit issue 2 of AARESAA-1881
					if (ReservationApiUtils.isPassengerConfirmedForAModifyBooking(reservationPax, passengerPayment,
							acceptMorePaymentsThanItsCharges, isPaymentForAcquirCredit)) {
						confirmedPassengers.add(reservationPax);

						if (AppSysParamsUtil.isPaxWiseTicketingEnabled()
								&& ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationPax.getStatus())) {
							reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
						}
					}
				}
				// If the reservation is not already confirmed then processed with logic.
				// Cos there can be users carrying out payment for confirmed reservation too. Since this type of users
				// goes through this command it's a responsibility to support this feature
			} else {
				int i = 0;
				// To Identify the offline payment
				boolean isPaymentExist = false;
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();
					PaymentAssembler payment = (PaymentAssembler) passengerPayment.get(reservationPax.getPnrPaxId());
					if (payment != null) {
						isPaymentExist = payment.isPaymentExist();
					}			
					if (ReservationApiUtils.isPassengerConfirmedForAModifyBooking(reservationPax, passengerPayment,
							acceptMorePaymentsThanItsCharges, isPaymentForAcquirCredit) && isPaymentExist) {
						
						// You might see this call as an idiot's call. But the QA wants to pay for cancelled
						// reservations
						if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
							reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
						}

						confirmedPassengers.add(reservationPax);
						i++;
					} else {
						if (triggerPaidAmountError.booleanValue()) {
							throw new ModuleException("airreservations.arg.paidAmountError");
						} else {
							reservationPax.setStatus(ReservationPaxStatus.ON_HOLD);
						}
					}
				}

				// We don't want to change the res status for payments done for AcquirCredit
				if (!isPaymentForAcquirCredit) {
					confirmReservation = false;

					confirmReservation = reservation.getPassengers().size() == i;
					confirmReservation |= AppSysParamsUtil.isPaxWiseTicketingEnabled();

					// TODO -- channel-wise

					if (confirmReservation) {
						// You might see this call as an idiot's call. But the QA wants to pay for cancelled
						// reservations
						if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())
								&& isPaymentExist) {
							reservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
						}

					} else {
						reservation.setStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
					}
				}
			}
		}

		return confirmedPassengers;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param triggerPaidAmountError
	 * @param triggerPnrForceConfirmed
	 * @param passengerPayment
	 * @param acceptMorePaymentsThanItsCharges
	 * @param isPaymentForAcquirCredit
	 * @param version
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(String pnr, Boolean triggerPaidAmountError, Boolean triggerPnrForceConfirmed,
			Map passengerPayment, Boolean acceptMorePaymentsThanItsCharges, Boolean isPaymentForAcquirCredit, Long version,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || triggerPaidAmountError == null || triggerPnrForceConfirmed == null || passengerPayment == null
				|| acceptMorePaymentsThanItsCharges == null || isPaymentForAcquirCredit == null || version == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}

	/**
	 * Activate system generated after payment made for onhold reservation
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private void activateSystemGeneratedPromotion(Reservation reservation) throws ModuleException {
		if (reservation.getPromotionId() != null && reservation.getPromotionId() != 0) {

			PromotionCriteria promotionCriteria = ReservationModuleUtils.getPromotionCriteriaAdminBD()
					.getPromotionCriteria(reservation.getPnr());
			if (promotionCriteria instanceof SysGenPromotionCriteria) {
				SysGenPromotionCriteria sysGenPromo = (SysGenPromotionCriteria) promotionCriteria;
				if (AccelAeroCalculator.isGreaterThan(sysGenPromo.getDiscount().getDiscountValue(),
						AccelAeroCalculator.getDefaultBigDecimalZero())) {
					// Email generated promo code to passenger
					ReservationModuleUtils.getPromotionManagementBD().sendPromoCodeEmailNotification(promotionCriteria,
							reservation.getContactInfo());
				}
				ReservationModuleUtils.getPromotionCriteriaAdminBD().activateSystemGeneratedPromotion(reservation.getPnr());
			}
		}
	}
}
