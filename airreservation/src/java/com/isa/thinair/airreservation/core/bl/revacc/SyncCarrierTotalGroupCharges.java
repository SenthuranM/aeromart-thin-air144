/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command useful when AccelAero charges need to synchronized according to another carrier[GDS] total group charges such
 * as FARE/TAX/SUR.
 * 
 * TODO:when infant charges passed should be adjusted from parent charges only
 * 
 * 
 * @author M.Rikaz
 * @isa.module.command name="syncCarrierTotalGroupCharges"
 */
public class SyncCarrierTotalGroupCharges extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(SyncCarrierTotalGroupCharges.class);

	/**
	 * constructor of the SyncCarrierTotalGroupCharges command
	 */
	public SyncCarrierTotalGroupCharges() {

	}

	/**
	 * execute method of the SyncCarrierTotalGroupCharges command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.info("Inside execute");
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		@SuppressWarnings("unchecked")
		Map<Integer, GDSPaxChargesTO> gdsChargesByPax = (Map<Integer, GDSPaxChargesTO>) this
				.getParameter(CommandParamNames.GDS_CHARGE_BY_PAX);

		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		TrackInfoDTO trackInfoDTO = null;
		if (credentialsDTO != null) {
			trackInfoDTO = credentialsDTO.getTrackInfoDTO();
		}

		String pnr = (String) this.getParameter(CommandParamNames.PNR);

		// reject lcc pnr/group pnr

		Reservation reservation = null;
		if (pnr != null) {
			// Retrieves the Reservation
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadEtickets(true);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
		}

		Long version = reservation.getVersion();

		if (gdsChargesByPax != null && !gdsChargesByPax.isEmpty()) {
			Map<Integer, BigDecimal> totalPlusAdjByPaxFareId = new HashMap<Integer, BigDecimal>();
			Map<Integer, Map<Integer, Map<String, BigDecimal>>> paxWiseAjustments = new HashMap<Integer, Map<Integer, Map<String, BigDecimal>>>();
			for (Integer pnrPaxId : gdsChargesByPax.keySet()) {
				GDSPaxChargesTO paxChargesTO = gdsChargesByPax.get(pnrPaxId);		
				Map<Integer, Map<String, BigDecimal>> paxAdjCharges = getAAPaxGroupWiseChargeDiscrepancy(reservation, pnrPaxId,
						paxChargesTO, totalPlusAdjByPaxFareId);				
				
				if (paxAdjCharges != null && !paxAdjCharges.isEmpty()) {
					paxWiseAjustments.put(pnrPaxId, paxAdjCharges);
				}				
			}
			
			AirproxyModuleUtils.getPassengerBD().adjustGDSChargesBulkManual(pnr, paxWiseAjustments, version, "GDS ticketing",
					totalPlusAdjByPaxFareId, trackInfoDTO);
		}

		log.info("Exit execute");

		if (response.isSuccess()) {
			return response;
		} else {
			throw new ModuleException("airreservation.revacc.error");
		}

	}

	private static Map<Integer, Map<String, BigDecimal>> getAAPaxGroupWiseChargeDiscrepancy(Reservation reservation,
			Integer pnrPaxId, GDSPaxChargesTO paxChargesTO, Map<Integer, BigDecimal> totalPlusAdjByPaxFareId)
			throws ModuleException {

		Map<Integer, Map<String, BigDecimal>> paxAdjCharges = new HashMap<Integer, Map<String, BigDecimal>>();
		boolean adjustmentRequired;

		for (ReservationPax pax : reservation.getPassengers()) {
			if (pax.getPnrPaxId().equals(pnrPaxId)) {

				adjustmentRequired = !paxChargesTO.getFareAmount().equals(pax.getTotalFare())
						|| !paxChargesTO.getTaxAmount().equals(pax.getTotalTaxCharge())
						|| !paxChargesTO.getSurChargeAmount().equals(pax.getTotalSurCharge());

				BigDecimal otherCharges = AccelAeroCalculator.add(pax.getTotalAdjustmentCharge(),
						pax.getTotalModificationCharge(), pax.getTotalModificationPenalty(), pax.getTotalCancelCharge(),
						pax.getTotalDiscount());

				adjustmentRequired = adjustmentRequired || !otherCharges.equals(BigDecimal.ZERO);

				if (adjustmentRequired) {
					// charge adjustment required for AA side charges

					Map<String, BigDecimal> adjCharges = new HashMap<String, BigDecimal>();
					// calculate FARE group discrepancies
					addRequiredGroupAdjustment(ChargeGroup.FAR, paxChargesTO.getFareAmount(), pax.getTotalFare(), adjCharges);

					// calculate TAX group discrepancies
					addRequiredGroupAdjustment(ChargeGroup.TAX, paxChargesTO.getTaxAmount(), pax.getTotalTaxCharge(), adjCharges);

					// calculate SUR group discrepancies
					addRequiredGroupAdjustment(ChargeGroup.SUR, paxChargesTO.getSurChargeAmount(), pax.getTotalSurCharge(),
							adjCharges);

					if (AccelAeroCalculator.getDefaultBigDecimalZero().compareTo(otherCharges) != 0) {
						adjCharges.put(ChargeGroup.ANY, otherCharges.negate());
					}

					LL: for (ReservationPaxFare reservationPaxFare : pax.getPnrPaxFares()) {

						if (!paxChargesTO.isInactiveSegmentsApplicable()) {
							for (ReservationPaxFareSegment resSeg : reservationPaxFare.getPaxFareSegments()) {
								if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL
										.equals(resSeg.getSegment().getStatus())) {
									continue LL;
								}
							}
						}
						BigDecimal totalPlusAdjustment = getTotalPlusAdjustment(adjCharges);
						if (totalPlusAdjByPaxFareId != null && totalPlusAdjustment.doubleValue() > 0) {
							totalPlusAdjByPaxFareId.put(reservationPaxFare.getPnrPaxFareId(), totalPlusAdjustment);
						}

						BigDecimal totalONDCharges = AccelAeroCalculator.add(totalPlusAdjustment,
								reservationPaxFare.getTotalChargeAmount());

						Map<String, BigDecimal> segAdjCharges = new HashMap<String, BigDecimal>();
						for (String chargeGroup : adjCharges.keySet()) {
							BigDecimal adjAmount = adjCharges.get(chargeGroup);
							BigDecimal consumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							if (adjAmount.doubleValue() != 0) {
								if (adjAmount.doubleValue() < 0 && totalONDCharges.doubleValue() > 0) {
									BigDecimal balance = AccelAeroCalculator.subtract(totalONDCharges, adjAmount.negate());
									if (balance.doubleValue() < 0) {
										consumedAmount = totalONDCharges.negate();
										totalONDCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
									} else {
										totalONDCharges = AccelAeroCalculator.subtract(totalONDCharges, adjAmount.negate());
										consumedAmount = adjAmount;
									}
									segAdjCharges.put(chargeGroup, consumedAmount);
									adjCharges.put(chargeGroup, AccelAeroCalculator.subtract(adjAmount, consumedAmount));

								} else if (adjAmount.doubleValue() > 0) {
									segAdjCharges.put(chargeGroup, adjAmount);
									adjCharges.put(chargeGroup, AccelAeroCalculator.getDefaultBigDecimalZero());
								}
							}
						}
						paxAdjCharges.put(reservationPaxFare.getPnrPaxFareId(), segAdjCharges);
					}
				}
			}
		}
		return paxAdjCharges;

	}

	private static void addRequiredGroupAdjustment(String chargeGroup, BigDecimal gdsChargeGrpTotal, BigDecimal aaChargeGrpTotal,
			Map<String, BigDecimal> adjCharges) {

		if (chargeGroup != null && gdsChargeGrpTotal != null && aaChargeGrpTotal != null && adjCharges != null) {
			BigDecimal groupChargeAdj = AccelAeroCalculator.subtract(gdsChargeGrpTotal, aaChargeGrpTotal);

			groupChargeAdj = AccelAeroCalculator.scaleValueDefault(groupChargeAdj);
			if (AccelAeroCalculator.getDefaultBigDecimalZero().compareTo(groupChargeAdj) != 0) {
				adjCharges.put(chargeGroup, groupChargeAdj);
			}
		}

	}

	private static BigDecimal getTotalPlusAdjustment(Map<String, BigDecimal> adjCharges) {
		BigDecimal totalPositiveAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (adjCharges != null && !adjCharges.isEmpty()) {
			for (String chargeGroup : adjCharges.keySet()) {
				BigDecimal chgGroupAdjAmount = adjCharges.get(chargeGroup);

				if (chgGroupAdjAmount.doubleValue() > 0)
					totalPositiveAdj = AccelAeroCalculator.add(totalPositiveAdj, chgGroupAdjAmount);

			}

		}
		return totalPositiveAdj;
	}
}
