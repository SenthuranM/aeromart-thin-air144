package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.FareOndGroupTO;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.PENALTY_BASED;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FlownAssitUnit {

	private ModifyAssitUnit modifyAsst;
	private Collection<Integer> flownPnrSegIds;
	private boolean isRequote;
	private FlownFareMapper flownFareMapper = new FlownFareMapper();
	private OndFareDTO penaltyTargetOnd = null;
	private List<Integer> penaltyTargePnrPaxFareIds = new ArrayList<Integer>();
	private boolean penaltyPopulated = false;
	private Map<String, Integer> paxTypePnrPaxIdMap = new HashMap<String, Integer>();
	private boolean skipPenaltyCharges = false;
	private StringBuilder auditDetails = new StringBuilder();
	private Map<Integer, String> ondFareTypeByFareIdMap;	
	private static Log log = LogFactory.getLog(FlownAssitUnit.class);
	private Collection<OndFareDTO> ondFares;

	public FlownAssitUnit(String pnr, Collection<Integer> cnxPnrSegIds, Collection<Integer> orderdNewFlgSegIds,
			boolean isRequote, boolean isForModification, Collection<OndFareDTO> ondFares, boolean skipPenaltyCharges,
			Map<Integer, String> ondFareTypeByFareIdMap) throws ModuleException {
		this.isRequote = isRequote;
		this.skipPenaltyCharges = skipPenaltyCharges;
		this.ondFareTypeByFareIdMap = ondFareTypeByFareIdMap;
		this.modifyAsst = new ModifyAssitUnit(cnxPnrSegIds, orderdNewFlgSegIds, pnr);
		this.ondFares = ondFares;
		processFlownOndFares(ondFares);
	}

	public FlownAssitUnit(ModifyAssitUnit modifyAsst, boolean isRequote, Collection<OndFareDTO> ondFares,
			boolean skipPenaltyCharges, Map<Integer, String> ondFareTypeByFareIdMap) throws ModuleException {
		this.isRequote = isRequote;
		this.skipPenaltyCharges = skipPenaltyCharges;
		this.modifyAsst = modifyAsst;
		this.ondFareTypeByFareIdMap = ondFareTypeByFareIdMap;
		this.ondFares = ondFares;
		processFlownOndFares(ondFares);
	}

	// public FlownAssitUnit(Reservation reservation, Collection<Integer> cnxPnrSegIds, Collection<Integer>
	// orderdNewFlgSegIds,
	// boolean isRequote, boolean isForModification, Collection<OndFareDTO> ondFares) throws ModuleException {
	// this.isRequote = isRequote;
	// this.modifyAsst = new ModifyAssitUnit(cnxPnrSegIds, orderdNewFlgSegIds, reservation);
	// processFlownOndFares(ondFares);
	// }

	private class FlownFareMapper {
		Map<OndFareDTO, Collection<FareOndGroupTO>> ondFareGroupList = new HashMap<OndFareDTO, Collection<FareOndGroupTO>>();
		private Map<Collection<Integer>, Map<Integer, ReservationPaxFare>> pnrPaxFareMap;
		Map<Integer, FlownOndPaxFare> paxMap = new HashMap<Integer, FlownAssitUnit.FlownOndPaxFare>();
		private Map<Integer, BigDecimal> existingPenalties;

		public Collection<FareOndGroupTO> getFlownOndFareGroups(OndFareDTO ondFare) {
			if (!ondFareGroupList.containsKey(ondFare)) {
				ondFareGroupList.put(ondFare, new ArrayList<FareOndGroupTO>());
			}
			return ondFareGroupList.get(ondFare);
		}

		private FlownOndPaxFare getPaxOndFare(int pnrPaxId) {
			calculatePaxWiseOndPenalties();
			return paxMap.get(pnrPaxId);
		}

		private FlownOndPaxFare getPaxOndFare(int pnrPaxId, String paxType) {
			if (!paxMap.containsKey(pnrPaxId)) {
				paxMap.put(pnrPaxId, new FlownOndPaxFare(pnrPaxId, paxType, existingPenalties.get(pnrPaxId)));
			}
			return paxMap.get(pnrPaxId);
		}

		public boolean hasOnd(OndFareDTO ondFare) {
			return ondFareGroupList.containsKey(ondFare);
		}

		public Collection<OndFareDTO> getFlownOndFares() {
			return ondFareGroupList.keySet();
		}

		public void setPnrSegWisePaxFareMap(Map<Collection<Integer>, Map<Integer, ReservationPaxFare>> pnrPaxFareMap) {
			this.pnrPaxFareMap = pnrPaxFareMap;
		}

		public BigDecimal getPaxPenalty(String paxType) {
			BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
			calculatePaxWiseOndPenalties();
			if (paxTypePnrPaxIdMap.containsKey(paxType)) {
				getPaxOndFare(paxTypePnrPaxIdMap.get(paxType)).resetTotalPaxPen();
				for (OndFareDTO ond : ondFareGroupList.keySet()) {
					Collection<Integer> ondFltSegIds = ond.getFlightSegmentIds();
					BigDecimal penalty = getPaxPenalty(paxTypePnrPaxIdMap.get(paxType), ondFltSegIds);
					total = AccelAeroCalculator.add(total, penalty);
				}
			}
			return total;
		}

		public BigDecimal getPaxPenalty(int pnrPaxId) {
			BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (!ondFareGroupList.keySet().isEmpty()) {
				getPaxOndFare(pnrPaxId).resetTotalPaxPen();
			}
			for (OndFareDTO ond : ondFareGroupList.keySet()) {
				Collection<Integer> ondFltSegIds = ond.getFlightSegmentIds();
				BigDecimal penalty = getPaxPenalty(pnrPaxId, ondFltSegIds);
				total = AccelAeroCalculator.add(total, penalty);
			}
			return total;
		}

		private BigDecimal getPaxPenalty(int pnrPaxId, Collection<Integer> ondFltSegIds) {
			calculatePaxWiseOndPenalties();
			return getPaxOndFare(pnrPaxId).getTotalCalculatedPenalty(ondFltSegIds);
		}

		public void calculatePaxWiseOndPenalties() {
			if (!penaltyPopulated) {
				for (OndFareDTO flownOnd : getFlownOndFares()) {
					for (FareOndGroupTO fareGroup : getFlownOndFareGroups(flownOnd)) {
						// Map<Integer, ReservationPaxFare> paxWiseFare = pnrPaxFareMap.get(fareGroup.getPnrSegIds());
						Map<Integer, ReservationPaxFare> paxWiseFare = getPaxWiseFare(fareGroup.getPnrSegIds());
						for (Integer pnrPaxId : paxWiseFare.keySet()) {
							ReservationPaxFare pnrPaxFare = paxWiseFare.get(pnrPaxId);
							paxTypePnrPaxIdMap.put(pnrPaxFare.getReservationPax().getPaxType(), pnrPaxId);
							getPaxOndFare(pnrPaxId, pnrPaxFare.getReservationPax().getPaxType()).getOndPaxFares(flownOnd).add(
									pnrPaxFare);
						}
					}
				}
				penaltyPopulated = true;
			}

		}

		public void setExistingPenalties(Map<Integer, BigDecimal> existingPenalties) {
			this.existingPenalties = existingPenalties;
		}

		/**
		 * function used to update the map since the key is used as collection when the index order changes
		 * match/contains wont work as expected to over come implemented
		 * updatePnrPaxFareMapKey,updatePnrPaxFareMapValues & isCollectionContainsAll
		 * 
		 * TODO: must use a different unique key without using collection
		 * */
		private Map<Integer, ReservationPaxFare> getPaxWiseFare(Collection<Integer> pnrSegIds) {
			if (pnrSegIds != null && pnrSegIds.size() > 0 && pnrPaxFareMap != null && pnrPaxFareMap.size() > 0) {
				for (Collection<Integer> pnrPaxId : pnrPaxFareMap.keySet()) {
					if (isCollectionContainsAll(pnrSegIds, pnrPaxId)) {
						return pnrPaxFareMap.get(pnrPaxId);
					}
				}
			}

			return null;
		}

	}

	private class FlownOndPaxFare {

		private int pnrPaxId;
		private String paxType;
		private BigDecimal totalPaxPen = AccelAeroCalculator.getDefaultBigDecimalZero();
		private BigDecimal balanceTotalPaxPen = AccelAeroCalculator.getDefaultBigDecimalZero();

		public FlownOndPaxFare(int pnrPaxId, String paxType, BigDecimal totalPaxPen) {
			this.pnrPaxId = pnrPaxId;
			this.paxType = paxType;
			if (totalPaxPen != null) {
				this.totalPaxPen = totalPaxPen;
			}
		}

		public void resetTotalPaxPen() {
			balanceTotalPaxPen = totalPaxPen;
		}

		Map<OndFareDTO, Collection<ReservationPaxFare>> ondPaxFares = new HashMap<OndFareDTO, Collection<ReservationPaxFare>>();
		Map<Collection<Integer>, OndFareDTO> fltSegIdOndMap = new HashMap<Collection<Integer>, OndFareDTO>();

		public Collection<ReservationPaxFare> getOndPaxFares(OndFareDTO ondFare) {
			if (!ondPaxFares.containsKey(ondFare)) {
				ondPaxFares.put(ondFare, new ArrayList<ReservationPaxFare>());
				fltSegIdOndMap.put(ondFare.getFlightSegmentIds(), ondFare);
			}
			return ondPaxFares.get(ondFare);
		}

		public BigDecimal getTotalCalculatedPenalty(Collection<Integer> fltSegIds) {

			OndFareDTO ondFare = fltSegIdOndMap.get(fltSegIds);
			Collection<ReservationPaxFare> paxFares = ondPaxFares.get(ondFare);
			Integer oldFareId = null;
			BigDecimal ondFareAmt = getTotalFareFor(ondFare);
			BigDecimal pnrPaxFareAmt = getTotalFareFor(paxFares, ondFare);

			// BigDecimal newPenalty = AccelAeroCalculator.subtract(ondFareAmt, pnrPaxFareAmt);
			BigDecimal newPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
			StringBuilder tempAudit = new StringBuilder();
			Integer pnrPaxFareId = null;

			for (ReservationPaxFare reservationPaxFare : paxFares) {
				oldFareId = reservationPaxFare.getFareId();
				pnrPaxFareId = reservationPaxFare.getPnrPaxFareId();
				tempAudit.append("PAX TYPE:" + reservationPaxFare.getReservationPax().getPaxType() + ", OLD FARE ID:"
						+ reservationPaxFare.getFareId() + ", OLD FARE AMOUNT:" + pnrPaxFareAmt);
			}
			
			/*
			 * same fare-id check is required when fare updated without split fare enabled, therefore when fare id is
			 * same system should skip the penalty calculation. this will prevent applying for same fare id with
			 * different amount.
			 */
			if (ondFare.getFareId() != oldFareId.intValue()) {
				if (AppSysParamsUtil.getPenaltyCalculateMethod() == PENALTY_BASED.OLD_FARE) {
					ArrayList<Integer> skipFareTypeList = new ArrayList<Integer>();
					skipFareTypeList.add(FareTypes.HALF_RETURN_FARE);
					skipFareTypeList.add(FareTypes.RETURN_FARE);
					int penaltyOndFarePercentage = 0;
					try {
						penaltyOndFarePercentage = AppSysParamsUtil.getPenaltyONDFarePercentage();
					} catch (Exception e) {
						penaltyOndFarePercentage = 0;
					}

					BigDecimal calculatedNewFare = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (oldFareId != null && pnrPaxFareId != null && pnrPaxId > 0) {
						String fareTypeCode = ondFareTypeByFareIdMap.get(oldFareId);
						BigDecimal totalRetJourneyFare = ReservationBO.getTotalReturnJourneyFare(pnrPaxId, pnrPaxFareId);
						if (fareTypeCode != null && skipFareTypeList.contains(Integer.parseInt(fareTypeCode))) {

							calculatedNewFare = AccelAeroCalculator.calculatePercentage(totalRetJourneyFare,
									penaltyOndFarePercentage);

							newPenalty = AccelAeroCalculator.subtract(calculatedNewFare, pnrPaxFareAmt);

						}
					}

					tempAudit.append(", NEW FARE AMOUNT " + penaltyOndFarePercentage + "%:");
					tempAudit.append(calculatedNewFare + ", Penalty Charge:" + calculatedNewFare + " - " + pnrPaxFareAmt + " = "
							+ newPenalty);

				} else {
					newPenalty = AccelAeroCalculator.subtract(ondFareAmt, pnrPaxFareAmt);

					tempAudit.append(", NEW FARE ID:" + ondFare.getFareId() + ", NEW FARE AMOUNT:" + ondFareAmt);
					tempAudit.append(", Penalty Charge:" + ondFareAmt + " - " + pnrPaxFareAmt + " = " + newPenalty);
				}
			} else {
				if (log.isWarnEnabled()) {
					log.warn("Penalty Calculation skipped, identified same fare id with different fare amount : "
							+ ondFare.getFareId());
				}
			}


			if (newPenalty.compareTo(BigDecimal.ZERO) > 0) {
				if (balanceTotalPaxPen.compareTo(BigDecimal.ZERO) > 0) {
					if (balanceTotalPaxPen.compareTo(newPenalty) > 0) {
						balanceTotalPaxPen = AccelAeroCalculator.subtract(balanceTotalPaxPen, newPenalty);
						newPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {
						newPenalty = AccelAeroCalculator.subtract(newPenalty, balanceTotalPaxPen);
						balanceTotalPaxPen = AccelAeroCalculator.getDefaultBigDecimalZero();
					}
				}
				tempAudit.append(", Final Penalty:" + newPenalty);

				if (auditDetails.length() > 0)
					auditDetails.append(",");

				auditDetails.append(tempAudit.toString());
				return newPenalty;
			} else {
				// Final Penalty:0.00
				// tempAudit.append(", Final Penalty:0.00 ");
			}

			return AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		private BigDecimal getTotalFareFor(Collection<ReservationPaxFare> paxFares, OndFareDTO ondFare) {
			BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (ReservationPaxFare paxFare : paxFares) {
				BigDecimal paxTotalFare = AccelAeroCalculator.subtract(paxFare.getTotalFare(), paxFare.getTotalDiscount());
				if (ondFare.getSegmentsMap().size() < paxFare.getPaxFareSegments().size()) {
					paxTotalFare = AccelAeroCalculator.divide(paxTotalFare, paxFare.getPaxFareSegments().size());
				}
				total = AccelAeroCalculator.add(total, paxTotalFare);
			}
			return total;

		}

		private BigDecimal getTotalFareFor(OndFareDTO ondFare) {

			BigDecimal adultDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal childDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (modifyAsst != null && modifyAsst.getReservationDiscountDTO() != null) {
				Map<String, BigDecimal> fareDiscountMap = modifyAsst.extractFareDiscountByPaxType(ondFare.getFlightSegmentIds());

				adultDiscount = fareDiscountMap.get(PaxTypeTO.ADULT);
				childDiscount = fareDiscountMap.get(PaxTypeTO.CHILD);
				infantDiscount = fareDiscountMap.get(PaxTypeTO.INFANT);

			}

			if (paxType.equals(PaxTypeTO.ADULT)) {
				return AccelAeroCalculator.add(AccelAeroCalculator.parseBigDecimal(ondFare.getAdultFare()), adultDiscount.negate());
			} else if (paxType.equals(PaxTypeTO.CHILD)) {
				return AccelAeroCalculator.add(AccelAeroCalculator.parseBigDecimal(ondFare.getChildFare()), childDiscount.negate());
			} else if (paxType.equals(PaxTypeTO.INFANT)) {
				return AccelAeroCalculator.add(AccelAeroCalculator.parseBigDecimal(ondFare.getInfantFare()), infantDiscount.negate());
			}

			return null;
		}

	}

	public Reservation getReservation() throws ModuleException {
		return modifyAsst.getReservation();
	}

	private void processFlownOndFares(Collection<OndFareDTO> ondFares) throws ModuleException {

		if (isRequote && ondFares != null) {
			Collection<Integer> flwnPnrSegIds = getFlownPnrSegIds();
			Collection<FareOndGroupTO> ondFareGroupList = CancellationUtils.getOndGroupTOList(flwnPnrSegIds, getReservation());
			for (OndFareDTO ondFare : ondFares) {
				Collection<Integer> fltSegIds = ondFare.getFlightSegmentIds();
				for (FareOndGroupTO ondGroup : ondFareGroupList) {
					if (fltSegIds.containsAll(ondGroup.getFlightSegIds()) || ondGroup.getFlightSegIds().containsAll(fltSegIds)) {
						fltSegIds.removeAll(ondGroup.getFlightSegIds());
						flownFareMapper.getFlownOndFareGroups(ondFare).add(ondGroup);
						if (fltSegIds.size() == 0) {
							break;
						}
					}
				}
				if (flownFareMapper.hasOnd(ondFare) && fltSegIds.size() > 0) {
					throw new ModuleException("airreservation.requote.inconsistent.flown.segment.ond.fare");
				}
				if (fltSegIds.size() > 0) {
					if (penaltyTargetOnd == null
							|| penaltyTargetOnd.getFirstDepartureDateZulu().after(ondFare.getFirstDepartureDateZulu())) {
						penaltyTargetOnd = ondFare;
					}
				}
			}
			Collection<Integer> cnxPnrSegIds = modifyAsst.getCnxPnrSegIds();
			if (cnxPnrSegIds == null) {
				cnxPnrSegIds = new ArrayList<Integer>();
			} else if (cnxPnrSegIds.size() > 0) {
				cnxPnrSegIds = CancellationUtils.getFirstOndsPnrSegIds(CancellationUtils.getOndWiseGroups(cnxPnrSegIds,
						getReservation()));
			}

			Map<Collection<Integer>, Map<Integer, ReservationPaxFare>> pnrPaxFareMap = new HashMap<Collection<Integer>, Map<Integer, ReservationPaxFare>>();
			Map<Integer, BigDecimal> existingPenalties = new HashMap<Integer, BigDecimal>();
			for (ReservationPax pax : getReservation().getPassengers()) {
				for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
					List<Integer> pnrSegIds = getPnrSegIdsFor(paxFare);

					// if (!pnrPaxFareMap.containsKey(pnrSegIds)) {
					// pnrPaxFareMap.put(pnrSegIds, new HashMap<Integer, ReservationPaxFare>());
					// }

					updatePnrPaxFareMapKey(pnrPaxFareMap, pnrSegIds);

					if (penaltyTargetOnd == null) {
						if (pnrSegIds.containsAll(cnxPnrSegIds)) {
							penaltyTargePnrPaxFareIds.add(paxFare.getPnrPaxFareId());
						}
					}

					updateCurrentPenalties(existingPenalties, pax.getPnrPaxId(), paxFare);

					// pnrPaxFareMap.get(pnrSegIds).put(pax.getPnrPaxId(), paxFare);
					updatePnrPaxFareMapValues(pnrPaxFareMap, pnrSegIds, paxFare, pax.getPnrPaxId());
				}
			}

			flownFareMapper.setPnrSegWisePaxFareMap(pnrPaxFareMap);
			flownFareMapper.setExistingPenalties(existingPenalties);

		}
	}

	/**
	 * function used to update the map since the key is used as collection when the index order changes match/contains
	 * wont work as expected to over come implemented updatePnrPaxFareMapKey,updatePnrPaxFareMapValues &
	 * isCollectionContainsAll
	 * 
	 * TODO: must use a different unique key without using collection
	 * */
	private void updatePnrPaxFareMapKey(Map<Collection<Integer>, Map<Integer, ReservationPaxFare>> pnrPaxFareMap,
			Collection<Integer> pnrSegIds) {
		if (pnrSegIds != null && pnrSegIds.size() > 0 && pnrPaxFareMap != null) {
			if (pnrPaxFareMap.keySet().isEmpty()) {
				pnrPaxFareMap.put(pnrSegIds, new HashMap<Integer, ReservationPaxFare>());
			} else if (pnrPaxFareMap.size() > 0) {

				Set<Collection<Integer>> pnrPaxFareKeyColl = pnrPaxFareMap.keySet();

				Iterator<Collection<Integer>> pnrPaxFareKeyItr = pnrPaxFareKeyColl.iterator();
				boolean isKeyExist = false;

				while (pnrPaxFareKeyItr.hasNext()) {
					Collection<Integer> pnrPaxId = pnrPaxFareKeyItr.next();
					if (isCollectionContainsAll(pnrSegIds, pnrPaxId)) {
						isKeyExist = true;
						break;
					}
				}

				if (!isKeyExist) {
					pnrPaxFareMap.put(pnrSegIds, new HashMap<Integer, ReservationPaxFare>());
				}

			}
		}
	}

	private void updatePnrPaxFareMapValues(Map<Collection<Integer>, Map<Integer, ReservationPaxFare>> pnrPaxFareMap,
			Collection<Integer> pnrSegIds, ReservationPaxFare paxFare, Integer paxId) {
		if (pnrSegIds != null && pnrSegIds.size() > 0 && pnrPaxFareMap != null && paxFare != null && paxId != null) {
			for (Collection<Integer> pnrPaxId : pnrPaxFareMap.keySet()) {
				if (isCollectionContainsAll(pnrSegIds, pnrPaxId)) {
					pnrPaxFareMap.get(pnrPaxId).put(paxId, paxFare);
					break;
				}

			}
		}
	}

	private void updateCurrentPenalties(Map<Integer, BigDecimal> existingPenalties, Integer pnrPaxId, ReservationPaxFare paxFare) {
		if (!existingPenalties.containsKey(pnrPaxId)) {
			existingPenalties.put(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
		}
		existingPenalties
				.put(pnrPaxId, AccelAeroCalculator.add(paxFare.getTotalPenaltyCharge(), existingPenalties.get(pnrPaxId)));
	}

	private List<Integer> getPnrSegIdsFor(ReservationPaxFare paxFare) {
		List<Integer> pnrSegIds = new ArrayList<Integer>();
		for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
			pnrSegIds.add(paxFareSeg.getPnrSegId());
		}
		return pnrSegIds;
	}

	public boolean isFlownOnd(OndFareDTO ondFare) {
		if (ondFare != null) {
			return flownFareMapper.hasOnd(ondFare);
		}
		return false;
	}

	public boolean isPenaltyApplicableOnd(OndFareDTO ondFare) {
		if (penaltyTargetOnd != null && penaltyTargetOnd.equals(ondFare)) {
			return true;
		}
		return false;
	}

	public boolean isPenaltyApplicablePaxFare(ReservationPaxFare paxFare) {
		if (penaltyTargePnrPaxFareIds.contains(paxFare.getPnrPaxFareId())) {
			return true;
		}
		return false;
	}

	public BigDecimal getPenaltyForPaxFare(ReservationPaxFare paxFare) throws ModuleException {
		if (isPenaltyApplicablePaxFare(paxFare) && !skipPenaltyCharges) {
			return flownFareMapper.getPaxPenalty(paxFare.getReservationPax().getPnrPaxId());
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public BigDecimal getPenaltyForOnd(OndFareDTO ondFare, String paxType) {
		if (isPenaltyApplicableOnd(ondFare) && !skipPenaltyCharges) {
			return flownFareMapper.getPaxPenalty(paxType);
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public boolean hasFlownSegments() throws ModuleException {
		return modifyAsst.hasFlownPnrSegments();
	}

	public boolean hasFlownInternationalSegments() throws ModuleException {
		if (hasFlownSegments()) {
			for (ReservationSegmentDTO resSegDTO : modifyAsst.getReservation().getSegmentsView()) {
				if (getFlownPnrSegIds().contains(resSegDTO.getPnrSegId())) {
					if (resSegDTO.isInternationalFlight()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public Collection<Integer> getFlownPnrSegIds() throws ModuleException {
		if (flownPnrSegIds == null) {
			if (modifyAsst.hasFlownPnrSegments()) {
				flownPnrSegIds = CancellationUtils.getAllPnrSegIdsForSameOndGroup(modifyAsst.getFlownPnrSegIds(),
						getReservation());
			} else {
				flownPnrSegIds = new ArrayList<Integer>();
			}
		}
		return flownPnrSegIds;
	}

	public ModifyAssitUnit getModifyAsst() {
		return modifyAsst;
	}

	public StringBuilder getAuditDetails() {
		return auditDetails;
	}

	private boolean isCollectionContainsAll(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxId) {
		@SuppressWarnings("unchecked")
		Collection<Integer> disJointList = CollectionUtils.disjunction(pnrSegIds, pnrPaxId);
		if (disJointList == null || disJointList.size() == 0) {
			return true;
		}
		return false;
	}

	public Map<Integer, String> getOndFareTypeByFareIdMap() {
		return ondFareTypeByFareIdMap;
	}
	
	public Collection<Integer> getNewFltSegIds() {
		if (modifyAsst != null && modifyAsst.getNewFltSegIds() != null) {
			return modifyAsst.getNewFltSegIds();
		}

		return null;
	}

	public OndFareDTO getPenaltyTargetOnd() {
		return penaltyTargetOnd;
	}

	public void setPenaltyTargetOnd(OndFareDTO penaltyTargetOnd) {
		this.penaltyTargetOnd = penaltyTargetOnd;
	}

	public Collection<OndFareDTO> getOndFares() {
		return ondFares;
	}
}
