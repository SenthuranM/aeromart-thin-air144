package com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.TotalByDestinationElementBuilderTemplate;

public class TotalByDestinationElementBuilderTemplatePnlAdl extends TotalByDestinationElementBuilderTemplate {

	@Override
	public String buildElementTemplate(PassengerElementsContext context) {

		StringBuilder elementTemplate = new StringBuilder();
		elementTemplate.append(hyphen());
		elementTemplate.append(context.getDestinationFare().getDestinationAirportCode());
		elementTemplate.append(getNumberOfPassengers(context.getDestinationFare().getNumberOfPreviousPassengers()));
		elementTemplate.append(context.getDestinationFare().getFareClass());

		return elementTemplate.toString();

	}

	private String getNumberOfPassengers(Integer numberOfPassengers) {
		String passengerCountElement = "000";
		if (numberOfPassengers == null) {
			numberOfPassengers = 0;
		} else if (numberOfPassengers < 0) {
			numberOfPassengers = 0;
		}
		passengerCountElement = passengerCountElement + numberOfPassengers;
		return passengerCountElement.substring(passengerCountElement.length() - 3, passengerCountElement.length());
	}

}
