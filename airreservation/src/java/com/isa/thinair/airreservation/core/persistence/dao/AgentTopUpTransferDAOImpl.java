/**
 * 
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.external.AgentTopUpX3DTO;
import com.isa.thinair.airreservation.api.model.AgentTopUp;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * AgentTopUpTransferDAOImpl is the business DAO hibernate implementation
 * 
 * 
 * @isa.module.dao-impl dao-name="x3AgentTopUpTransferDAO"
 */

public abstract class AgentTopUpTransferDAOImpl extends PlatformBaseHibernateDaoSupport implements AgentTopUpTransferDAO {

	private static final Log log = LogFactory.getLog(AgentTopUpTransferDAOImpl.class);
	
	/*
	 * Clear agent topUp in internal summary tables for a date
	 * 
	 * @Param date
	 */
	private static boolean tranferStatus;

	public void clearAgentTopUpTable(Date date) {

		String sql = "DELETE FROM AgentTopUp a where a.tnxDate BETWEEN :starttime AND :endtime";
		Query query = getSession().createQuery(sql).setDate("starttime", CalendarUtil.getStartTimeOfDate(date))
				.setDate("endtime",CalendarUtil.getEndTimeOfDate(date));
		query.executeUpdate();

	}	
	
	public Collection<AgentTopUp> insertTopUpToInternal(Collection<AgentTopUpX3DTO> col) {
		Collection<AgentTopUp> agentTopUpPOJOS = new ArrayList<AgentTopUp>();
		Date now = new Date();
		Iterator<AgentTopUpX3DTO> it = col.iterator();

		if (col != null) {
			while (it.hasNext()) {
				AgentTopUpX3DTO dto = (AgentTopUpX3DTO) it.next();

				AgentTopUp agentTopup = new AgentTopUp();
				tranferStatus = false;
				agentTopup.setTnxId(dto.getTnxId());
				agentTopup.setTnxDate(dto.getTnxDate());
				agentTopup.setAgentCode(dto.getAgentCode());
				agentTopup.setAmount(dto.getAmount());
				agentTopup.setAmountLocal(dto.getAmountLocal());
				agentTopup.setCurrencyCode(dto.getCorrencyCode());
				agentTopup.setNcDestription(dto.getNcDestription());
				agentTopup.setVersion(dto.getVersion());
				agentTopup.setTransferStatus(tranferStatus);
				agentTopup.setTransferTimeStamp(now);

				agentTopUpPOJOS.add(agentTopup);
			}
		}
		saveOrUpdateAllAgentTopUp(agentTopUpPOJOS);
		return agentTopUpPOJOS;
	}

	/**
	 * insertin hibernate pojos
	 * 
	 * @param agentTopUpPOJOS
	 */
	private void saveOrUpdateAllAgentTopUp(Collection<AgentTopUp> agentTopUpPOJOS) {
		hibernateSaveOrUpdateAll(agentTopUpPOJOS);
	}
	

	@Override
	public void updateInternalTopUpTable(Date date) {

		try {
			String hql = "UPDATE AgentTopUp SET transferStatus = :transferStatus WHERE tnxDate BETWEEN :starttime AND :endtime";
			Query query = getSession().createQuery(hql).setBoolean("transferStatus", true)
					.setDate("starttime", CalendarUtil.getStartTimeOfDate(date))
					.setDate("endtime",
					CalendarUtil.getEndTimeOfDate(date));
			query.executeUpdate();

		} catch (Exception exception) {
			log.error("###### Error Update agent top up to Internal DB", exception);
			throw exception;
		}

		log.info("############ Successfuly update  Agent Topup :  Internal DB");

	}

	/*
	 * Gets agent topUp for a date
	 * 
	 * @Param date
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<AgentTopUpX3DTO> getTopUps(Date date) {
		log.info("########### getTopUps #############");
		Collection<AgentTopUpX3DTO> agentTopupDTO = new ArrayList<AgentTopUpX3DTO>();

		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT att.txn_id,");
		sql.append(" att.agent_code, ");
		sql.append(" att.amount, ");
		sql.append(" att.tnx_date, ");
		sql.append(" att_nc.description,");
		sql.append(" att.amount_local,");
		sql.append(" att.currency_code,");
		sql.append(" att.version");
		sql.append(" FROM ");
		sql.append(" t_agent_transaction att, ");
		sql.append(" t_agent_transaction_nc att_nc");
		sql.append(" WHERE ");
		sql.append(" att.nominal_code IN (2,3,6,8,9,14)");
		// sql.append("AND nominal_code IN (" + Util.buildIntegerInClauseContent(nominalCodes) + ") ");  (2,6,8,9,14)
		sql.append(" AND att.tnx_date between ? and ? ");
		sql.append(" AND att.nominal_code = att_nc.nominal_code ");

		if (log.isDebugEnabled()) {
			log.debug("SQL:" + sql.toString() + "date1:" + date);
		}
		log.info("########### " + sql.toString() + " #############");
		// Object input[] = new Object[1];
		// input[0] = date;
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		@SuppressWarnings("rawtypes")
		Collection<AgentTopUpX3DTO> agentTopUpData = (Collection<AgentTopUpX3DTO>) template.query(sql.toString(),
				new Object[] { CalendarUtil.getStartTimeOfDate(date), CalendarUtil.getEndTimeOfDate(date) },
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						if (rs != null) {
							while (rs.next()) {

								AgentTopUpX3DTO agentTopUpdetails = new AgentTopUpX3DTO();
								agentTopUpdetails.setTnxId(rs.getInt("txn_id"));
								agentTopUpdetails.setAgentCode(rs.getString("agent_code"));
								agentTopUpdetails.setAmount(rs.getBigDecimal("amount"));
								agentTopUpdetails.setTnxDate(rs.getDate("tnx_date"));
								agentTopUpdetails.setNcDestription(rs.getString("description"));
								agentTopUpdetails.setAmountLocal(rs.getBigDecimal("amount_local"));
								agentTopUpdetails.setCorrencyCode(rs.getString("currency_code"));
								agentTopUpdetails.setVersion(rs.getInt("version"));
								agentTopupDTO.add(agentTopUpdetails);

							}
						}
						return agentTopupDTO;
					}
				}
		);
		return agentTopUpData;

	}


	public abstract void insertTopUpToExternal(Collection<AgentTopUpX3DTO> col, AccontingSystemTemplate template)
			throws ClassNotFoundException, SQLException, Exception;
	
}
