package com.isa.thinair.airreservation.core.activators;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSMeal;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassenger;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSSSRRequest;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLDestinationDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLEmailDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLLogDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLRecordDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerCountRecordDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.PNLRecordDTO;
import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.model.PalCalHistory;
import com.isa.thinair.airreservation.api.model.PalCalPaxCount;
import com.isa.thinair.airreservation.api.model.PnlAdlHistory;
import com.isa.thinair.airreservation.api.model.PnlAdlPaxCount;
import com.isa.thinair.airreservation.api.model.PnlPassenger;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.AnciTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.pnl.SendMailUsingAuthentication;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservarionPaxFareSegDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.messaging.api.dto.LogDetailDTO;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.msgbroker.api.dto.OutMessageTO;
import com.isa.thinair.msgbroker.api.util.MessageComposerApiUtils;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.platform.api.constants.PlatformConstants;

public class PnlAdlUtil {

	private static Log log = LogFactory.getLog(PnlAdlUtil.class);
	private static AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
	private static final String Email = "E";
	private static final String Untransmitted = "N";

	public static List<String> composeSitaAddresses(String[] sitaAddresses) {
		List<String> lstAddresses = null;

		if (sitaAddresses != null) {
			lstAddresses = new ArrayList<String>();
			for (int i = 0; i < sitaAddresses.length; i++) {
				String address = BeanUtils.nullHandler(sitaAddresses[i]);

				if (address.length() > 0) {
					lstAddresses.add(address);
				}
			}
		}

		return lstAddresses;

	}

	public static void populateSitaAddresses(HashMap<String, String> sitaStatusMap, List<String> sitaAddresses,
			List<String> sitaTexAddresses, List<String> airincAddressList) throws ModuleException {
		if ((sitaAddresses == null || sitaAddresses.size() == 0) && (sitaTexAddresses == null || sitaTexAddresses.size() == 0)
				&& (airincAddressList == null || airincAddressList.size() == 0)) {
			throw new ModuleException(PNLConstants.ERROR_CODES.EMPTY_SITA_ADDRESS);
		}

		Iterator<String> iterator = sitaAddresses.iterator();
		while (iterator.hasNext()) {
			sitaStatusMap.put(iterator.next(), PNLConstants.SITAMsgTransmission.FAIL);
		}
		
		if(sitaStatusMap != null && sitaStatusMap.isEmpty()){
			Iterator<String> iteratorTextAddresses = sitaTexAddresses.iterator();
			while (iteratorTextAddresses.hasNext()) {
				sitaStatusMap.put(iteratorTextAddresses.next(), PNLConstants.SITAMsgTransmission.FAIL);
			}
		}
		
		if(sitaStatusMap != null && sitaStatusMap.isEmpty()){
			Iterator<String> iteratorArincAddresses = airincAddressList.iterator();
			while (iteratorArincAddresses.hasNext()) {
				sitaStatusMap.put(iteratorArincAddresses.next(), PNLConstants.SITAMsgTransmission.FAIL);
			}
		}
	}

	/**
	 * 
	 * Gets the Fisrt Name count from the passenger list
	 */
	public static int getFirstNamesCount(List<ReservationPaxDetailsDTO> passengerList) {
		String lastNameOfFirstPax = null;
		String lastNameOfOtherPax = null;
		int firstNameCount = 0;
		int i = 0;
		Iterator<ReservationPaxDetailsDTO> it = passengerList.iterator();
		while (it.hasNext()) {
			ReservationPaxDetailsDTO pax = it.next();
			if (pax != null) {
				if (i == 0) {
					lastNameOfFirstPax = pax.getLastName();
				}
				lastNameOfOtherPax = pax.getLastName();
			}
			if ((lastNameOfFirstPax == null) || (lastNameOfFirstPax.trim().equalsIgnoreCase(""))) {
				break;
			}
			if ((lastNameOfOtherPax != null) && (lastNameOfFirstPax.trim().equalsIgnoreCase(lastNameOfOtherPax.trim()))) {
				firstNameCount++;
			}
			i++;
		}
		return firstNameCount;
	}

	/**
	 * Method to get the first names in a pnl record When the name element exceeds 64 characters the rest of passenger
	 * names will be dropped.
	 * 
	 * @param passengerList
	 * @param firstNameCount
	 * @param totalCountForPnr
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<NameDTO> getFirstNames(List<ReservationPaxDetailsDTO> passengerList, int firstNameCount,
			int totalCountForPnr, ArrayList<Integer> paxIdsPerRecord, Map<Integer, String> pnrPaxIdSeatMap)
			throws ModuleException {
		// String seatNumbers = null;
		String lastNameOfFirstPax = null;
		String lastNameOfOtherPax = null;
		String ssrCode = null;
		// String ssrText = null;

		Iterator<ReservationPaxDetailsDTO> it = passengerList.iterator();
		while (it.hasNext()) {
			ReservationPaxDetailsDTO pax = it.next();
			if ((pax != null) && (pax.getLastName() != null)) {
				lastNameOfFirstPax = pax.getLastName();

			}
			break;
		}
		ArrayList<NameDTO> firstNameList = new ArrayList<NameDTO>();
		int lastNameLength = lastNameOfFirstPax.trim().length();
		int titlelength = 0;
		int firstNameLength = 0;
		int totalNameLength = 0;
		boolean tourCodeExists = false;
		int totalPaxPerPnrLength = new Integer(passengerList.size()).toString().length();
		int firstNameCountLength = new Integer(firstNameCount).toString().length();
		int tourCodeLength = 0;
		if (passengerList.size() != totalCountForPnr) {
			tourCodeExists = true;
		}
		if (tourCodeExists) {
			tourCodeLength = 2 + totalPaxPerPnrLength; // 2 is for the tourId (eg: -A)
		}
		it = passengerList.iterator();
		while (it.hasNext()) {
			ReservationPaxDetailsDTO pax = it.next();
			if (pax != null) {
				lastNameOfOtherPax = pax.getLastName();
				if ((lastNameOfOtherPax != null) && (lastNameOfFirstPax != null)
						&& (lastNameOfOtherPax.trim().equalsIgnoreCase(lastNameOfFirstPax.trim()))) {
					NameDTO nameDTO = new NameDTO();
					Integer pnrPaxId = pax.getPnrPaxId();
					nameDTO.setPnrPaxId(pnrPaxId);

					titlelength = pax.getTitle().trim().length();
					// +1 is for "/" (ie: the name separator)
					firstNameLength = (pax.getFirstName().trim().length() + 1);
					nameDTO.setFirstname(pax.getFirstName().toUpperCase());
					nameDTO.setTitle(pax.getTitle());
					nameDTO.setPaxType(pax.getPaxType());
					ssrCode = pax.getSsrCode();

					if (ssrCode == null) {
						constructBlanks(nameDTO);
					}

					int totalLength = firstNameCountLength + lastNameLength + firstNameLength + titlelength + tourCodeLength;
					int maxTotalLenghtAlwd = PNLConstants.PNLADLElements.MAX_SINGLE_PAX_FULLNAME_WITH_TITLE_AND_TOURID_LENGTH;
					if (totalLength > maxTotalLenghtAlwd) {
						adjustFirstNameLength(nameDTO);
						totalLength = firstNameCountLength + lastNameLength + nameDTO.getFirstname().length() + titlelength
								+ tourCodeLength;
						// Code would never come here as the front end limits the length of the sir name.
						if (totalLength > maxTotalLenghtAlwd) {
							log.error("###$$$$$$$$############### An Unnaturally large SIR NAME found!");
							return firstNameList;
						}
					}

					totalNameLength += totalLength;

					if (totalNameLength <= 55) {
						if (!paxIdsPerRecord.contains(pnrPaxId)) {
							paxIdsPerRecord.add(pnrPaxId);
						}
						firstNameList.add(nameDTO);
						tourCodeLength = 0;
						lastNameLength = 0;
						firstNameLength = 0;
						firstNameCountLength = 0;
						if (isMultipleSeatsBooked(pax.getPnrPaxId(), pnrPaxIdSeatMap)) {
							NameDTO exstNameDTO = new NameDTO();
							exstNameDTO.setPnrPaxId(0);
							exstNameDTO.setFirstname(PNLConstants.PNLADLElements.EXST);
							exstNameDTO.setTitle("");
							firstNameList.add(exstNameDTO);
							passengerList.remove(pax);
							return firstNameList;
						} else {
							passengerList.remove(pax);
							it = passengerList.iterator();
						}
					} else {
						break;
					}
				}
			}
		}

		Collections.sort(firstNameList);

		return firstNameList;
	}

	/**
	 * If the line has exceeded the limit [An extremely large name that exceeds length 55]
	 * 
	 * @param nameDTO
	 */
	private static void adjustFirstNameLength(NameDTO nameDTO) {
		if (nameDTO != null) {
			// set the initial of the first name only.
			nameDTO.setFirstname(BeanUtils.nullHandler(nameDTO.getFirstname()).substring(0, 1));
		}
	}

	public static String[] getMonthDayArray(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd");
		String s = dateFormat.format(date);

		return new String[] { s.substring(5, 8).toUpperCase(), s.substring(9) };

	}

	@SuppressWarnings("unchecked")
	private static ArrayList<ReservationPaxDetailsDTO> getRecordsPerPnr(ArrayList<ReservationPaxDetailsDTO> list, String pnr) {
		ArrayList<ReservationPaxDetailsDTO> recordList = new ArrayList<ReservationPaxDetailsDTO>();
		for (int i = 0; i < list.size(); i++) {
			ReservationPaxDetailsDTO rPax = list.get(i);
			if ((rPax != null) && (rPax.getPnr() != null) && (rPax.getPnr().trim().equalsIgnoreCase(pnr.trim()))) {
				recordList.add(rPax);
			}
		}
		Collections.sort(recordList);
		return recordList;
	}

	public static HashMap<String, ArrayList<ReservationPaxDetailsDTO>> getPNRPassengerList(
			ArrayList<ReservationPaxDetailsDTO> recordarray) {
		String pnr = "";
		HashMap<String, ArrayList<ReservationPaxDetailsDTO>> pnrPax = new LinkedHashMap<String, ArrayList<ReservationPaxDetailsDTO>>();
		ArrayList<ReservationPaxDetailsDTO> passengers = new ArrayList<ReservationPaxDetailsDTO>();
		for (int i = 0; i < recordarray.size(); i++) {
			ReservationPaxDetailsDTO rpaxDTO = recordarray.get(i);
			pnr = rpaxDTO.getPnr();
			if ((pnr != null) && (!pnrPax.containsKey(pnr))) {
				passengers = getRecordsPerPnr(recordarray, pnr);
				if ((passengers != null) && (passengers.size() != 0)) {
					pnrPax.put(pnr, passengers);
				}
			}
		}
		return pnrPax;
	}

	/**
	 * @param reservationPaxDetailsDTOs
	 * @return
	 */
	public static Map<String, List<ReservationPaxDetailsDTO>> getSegmentStatusWisePassengers(
			ArrayList<ReservationPaxDetailsDTO> reservationPaxDetailsDTOs) {

		Map<String, List<ReservationPaxDetailsDTO>> passengerMap = new HashMap<String, List<ReservationPaxDetailsDTO>>();

		for (ReservationPaxDetailsDTO paxDetailsDTO : reservationPaxDetailsDTOs) {

			if (passengerMap.get(paxDetailsDTO.getPnrStatus()) == null) {
				passengerMap.put(paxDetailsDTO.getPnrStatus(), new ArrayList<ReservationPaxDetailsDTO>());
			}
			passengerMap.get(paxDetailsDTO.getPnrStatus()).add(paxDetailsDTO);
		}
		return passengerMap;

	}

	/**
	 * For Each Sita Address, each generated files contents are sent if error occures for one generated file, the sita
	 * address will carry that error in the hashmap
	 * 
	 * @param sitaAddresses
	 * @param generatedFilenames
	 * @param messageType
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Deprecated
	public static HashMap sendMails(List<String> sitaAddresses, List<String> generatedFilenames, String messageType)
			throws ModuleException {

		HashMap sitaStatusMap = new HashMap();
		Iterator<String> iterator = sitaAddresses.iterator();
		SendMailUsingAuthentication smtpMailSender = new SendMailUsingAuthentication();

		while (iterator.hasNext()) {
			String sitaAddress = iterator.next();
			sitaStatusMap.put(sitaAddress, PNLConstants.SITAMsgTransmission.SUCCESS);
			String[] sitaAddressWithMailExt = attachExtensionToAddress(sitaAddress);

			boolean isUserAlternateSender = false;
			if (sitaAddressWithMailExt[1].equals("true")) {
				isUserAlternateSender = true;
			}

			String filename = null;
			String messageBody = null;
			int i = 0;
			try {
				for (i = 0; i < generatedFilenames.size(); i++) {
					filename = generatedFilenames.get(i);
					File f = new File(filename);

					messageBody = SendMailUsingAuthentication.readPnlFile(f.getAbsolutePath());

					String sitalist[] = { sitaAddressWithMailExt[0] };

					smtpMailSender.postMail(sitalist, messageType, messageBody, isUserAlternateSender);

					if (log.isDebugEnabled())
						log.debug("Sending PNL/ADL SUCCESS: FILE[" + i + "]=" + filename + "\n\r SITA address=" + sitaAddress
								+ "\n\r Subject=" + messageType + "\n\r Content=" + messageBody);
				}
			} catch (Throwable e) {
				log.error("Sending PNL/ADL FAILED: FILE[" + i + "]=" + filename + "\n\r SITA address=" + sitaAddress
						+ "\n\r Subject=" + messageType, e);
				sitaStatusMap.put(sitaAddress, e);
			}
		}
		return sitaStatusMap;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap sendMails(Map<String,List<String>> sitaDeliverTypeAddressMap, List<String> generatedFilenames, String messageType)
			throws ModuleException {

		HashMap sitaStatusMap = new HashMap();

		if (!sitaDeliverTypeAddressMap.isEmpty()) {
			for (String delivaryMethod : sitaDeliverTypeAddressMap.keySet()) {

				List<String> sitaAddresses = sitaDeliverTypeAddressMap.get(delivaryMethod);
				Iterator<String> iterator = sitaAddresses.iterator();
				SendMailUsingAuthentication smtpMailSender = new SendMailUsingAuthentication();

				while (iterator.hasNext()) {
					String sitaAddress = iterator.next();
					sitaStatusMap.put(sitaAddress, PNLConstants.SITAMsgTransmission.SUCCESS);
					String[] sitaAddressWithMailExt = attachExtensionToAddress(sitaAddress);

					boolean isUserAlternateSender = false;
					if (sitaAddressWithMailExt[1].equals("true")) {
						isUserAlternateSender = true;
					}

					String filename = null;
					String messageBody = null;
					int i = 0;
					try {
						for (i = 0; i < generatedFilenames.size(); i++) {
							filename = generatedFilenames.get(i);
							File f = new File(filename);

							messageBody = SendMailUsingAuthentication.readPnlFile(f.getAbsolutePath());

							if (delivaryMethod.equals(SITAAddress.SITA_MESSAGE_VIA_ARINC)) {

								String arincSenderAddress = ((AirReservationConfig) ReservationModuleUtils.getInstance()
										.getModuleConfig()).getArincSenderAddress();

								String[] arincAddressWithMailExt = extractEmailUser(arincSenderAddress);
								String[] recieverWithMailExt = extractEmailUser(sitaAddress);
								messageBody =  recieverWithMailExt[0].toUpperCase() + "\n."
										+ arincAddressWithMailExt[0].toUpperCase() + "\n" + messageBody;
							}

							String sitalist[] = { sitaAddressWithMailExt[0] };

							smtpMailSender.postMail(sitalist, messageType, messageBody, isUserAlternateSender);

							if (log.isDebugEnabled())
								log.debug("Sending PNL/ADL SUCCESS: FILE[" + i + "]=" + filename + "\n\r SITA address="
										+ sitaAddress + "\n\r Subject=" + messageType + "\n\r Content=" + messageBody);
						}
					} catch (Throwable e) {
						log.error("Sending PNL/ADL FAILED: FILE[" + i + "]=" + filename + "\n\r SITA address=" + sitaAddress
								+ "\n\r Subject=" + messageType, e);
						sitaStatusMap.put(sitaAddress, e);
					}
				}
			}
		}

		return sitaStatusMap;
	}

	@SuppressWarnings("rawtypes")
	public static void checkErrorsInSitaStatusMap(HashMap sitaStatusMap) throws ModuleException {

		Set set = sitaStatusMap.keySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Object object = sitaStatusMap.get(iterator.next());
			if (!object.equals(PNLConstants.SITAMsgTransmission.SUCCESS)) {
				if (object instanceof Throwable) {

					throw new ModuleException((Throwable) object, PNLConstants.ERROR_CODES.SITA_STATUS_ERROR);
				} else {
					throw new ModuleException(PNLConstants.ERROR_CODES.SITA_STATUS_ERROR);
				}
			}
		}

	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isFailedForAllEmails(HashMap sitaStatusMap){
		boolean isFailedForAllEmails = true;
		Set set = sitaStatusMap.keySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Object object = sitaStatusMap.get(iterator.next());
			if (object.equals(PNLConstants.SITAMsgTransmission.SUCCESS)) {
				isFailedForAllEmails = false;
			}
		}
		return isFailedForAllEmails;
	}

	@SuppressWarnings("rawtypes")
	private static String decideTransmissionStatus(HashMap sitaStatusMap) throws ModuleException {

		boolean errorFound = false;
		boolean successFound = true;

		Set set = sitaStatusMap.keySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Object object = sitaStatusMap.get(iterator.next());
			if (!object.equals(PNLConstants.SITAMsgTransmission.SUCCESS)) {
				errorFound = true;
			} else {
				successFound = true;
			}

		}

		if (successFound && errorFound) {
			return PNLConstants.SITAMsgTransmission.PARTIALLY;
		}

		if (successFound && errorFound == false) {
			return PNLConstants.SITAMsgTransmission.SUCCESS;
		}

		if (successFound == false && errorFound == true) {
			return PNLConstants.SITAMsgTransmission.FAIL;
		}

		// code will never come here!
		return PNLConstants.SITAMsgTransmission.PARTIALLY;

	}

	/**
	 * 
	 * @param sitaaddresses
	 * @return
	 * @throws ModuleException
	 */
	private static String[] attachExtensionToAddress(String sitaaddresses) throws ModuleException {
		if (sitaaddresses.indexOf("@") != -1) {
			return new String[] { sitaaddresses, "true" };
		} else {
			String fullAddress = sitaaddresses
					+ ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig()).getPnlAdlMailExtension();
			return new String[] { fullAddress, "false" };
		}
	}

	/**
	 * Will Write SITA status to history.
	 * 
	 * @param sitaMap
	 * @param flightId
	 * @param depAirport
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void insertToHistory(String msgType, HashMap sitaMap, int flightId, String depAirport, String fileContent,
			Timestamp now, HashMap ccPaxCountMap, ModuleException customError, String sendingMethodType, String lastGroupCode,
			String userID) throws ModuleException {

		// debugLog()
		String transmissionStatus = decideTransmissionStatus(sitaMap);
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Set set = sitaMap.keySet();
		Iterator iterator = set.iterator();
		String blobString = BeanUtils.nullHandler(fileContent);
		while (iterator.hasNext()) {
			String sitaAddress = (String) iterator.next();

			// status will be 'Y' 'a@b.com invalid address' or 'localized msg upto 0-98 chars
			String status = decideStatus(sitaMap.get(sitaAddress));

			PnlAdlHistory pnlAdlHistory = new PnlAdlHistory();
			pnlAdlHistory.setAirportCode(depAirport);
			pnlAdlHistory.setFlightID(new Integer(flightId));
			pnlAdlHistory.setMessageType(msgType);

			if (sitaAddress.indexOf('@') != -1) {
				pnlAdlHistory.setEmail(sitaAddress);
			} else {
				pnlAdlHistory.setSitaAddress(sitaAddress);
			}
			pnlAdlHistory.setSentMethod(sendingMethodType);
			pnlAdlHistory.setUserID(userID);
			HashSet paxCountSet = new HashSet();
			Iterator iter = ccPaxCountMap.keySet().iterator();
			while (iter.hasNext()) {
				String ccCode = (String) iter.next();
				Integer paxCount = (Integer) ccPaxCountMap.get(ccCode);
				PnlAdlPaxCount pnlAdlPaxCount = new PnlAdlPaxCount();
				if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
					if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
						pnlAdlPaxCount.setLogicalCabinClassCode(ccCode);
					} else {
						pnlAdlPaxCount.setBookingCode(ccCode);
					}
				} else {
					pnlAdlPaxCount.setCabinClassCode(ccCode);
				}

				pnlAdlPaxCount.setPaxCount(paxCount);
				pnlAdlPaxCount.setPnlAdlHistory(pnlAdlHistory);
				pnlAdlPaxCount.setLastGroupCode(lastGroupCode);

				paxCountSet.add(pnlAdlPaxCount);
				// pnlAdlPaxCount.set
			}
			pnlAdlHistory.setCcPaxCountSet(paxCountSet);

			if (status.equals(PNLConstants.SITAMsgTransmission.SUCCESS)) {
				pnlAdlHistory.setTransmissionStatus(transmissionStatus);
			} else {
				pnlAdlHistory.setTransmissionStatus(PNLConstants.SITAMsgTransmission.FAIL);
				pnlAdlHistory.setErrorDescription(status);
			}
			pnlAdlHistory.setTransmissionTimeStamp(now);
			pnlAdlHistory.setSitaMessage(DataConverterUtil.createBlob(blobString.getBytes()));

			if (customError != null) {

				// if unexpect error occurs show the message, else show the msg from prop file
				if (customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE)) {
					status = status + "||" + customError.getMessage();
				} else {
					status = status + "||" + customError.getMessageString();
				}

				// if empty adl then change the status to "E"
				if (customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_ADL_ERROR_CODE)
						|| customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.FLIGHT_CLOSED_ADLERROR_CODE)
						|| customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.FLIGHT_CLOSED_PNLERROR_CODE)) {
					pnlAdlHistory.setTransmissionStatus(PNLConstants.SITAMsgTransmission.ADL_EMPTY);
				}

				// db length cannot exceed 100
				if (status.length() > 99) {
					status = status.substring(0, 99);
				} else {
					pnlAdlHistory.setErrorDescription(status);
				}
			}

			auxilliaryDAO.savePnlAdlHistory(pnlAdlHistory);

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void insertToPalCalHistory(String msgType, HashMap sitaMap, int flightId, String depAirport, String fileContent,
			Timestamp now, HashMap ccPaxCountMap, ModuleException customError, String sendingMethodType, String lastGroupCode,
 String userID) throws ModuleException {

		// debugLog()
		String transmissionStatus = decideTransmissionStatus(sitaMap);
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Set set = sitaMap.keySet();
		Iterator iterator = set.iterator();
		String blobString = BeanUtils.nullHandler(fileContent);
		while (iterator.hasNext()) {
			String sitaAddress = (String) iterator.next();

			// status will be 'Y' 'a@b.com invalid address' or 'localized msg upto 0-98 chars
			String status = decideStatus(sitaMap.get(sitaAddress));

			PalCalHistory palCalHistory = new PalCalHistory();
			palCalHistory.setAirportCode(depAirport);
			palCalHistory.setFlightID(new Integer(flightId));
			palCalHistory.setMessageType(msgType);

			if (sitaAddress.indexOf('@') != -1) {
				palCalHistory.setEmail(sitaAddress);
			} else {
				palCalHistory.setSitaAddress(sitaAddress);
			}
			palCalHistory.setSentMethod(sendingMethodType);
			palCalHistory.setUserID(userID);

			HashSet paxCountSet = new HashSet();
			Iterator iter = ccPaxCountMap.keySet().iterator();
			while (iter.hasNext()) {
				String ccCode = (String) iter.next();
				Integer paxCount = (Integer) ccPaxCountMap.get(ccCode);
				PalCalPaxCount palCalPaxCount = new PalCalPaxCount();
				if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
					if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
						palCalPaxCount.setLogicalCabinClassCode(ccCode);
					} else {
						palCalPaxCount.setBookingCode(ccCode);
					}
				} else {
					palCalPaxCount.setCabinClassCode(ccCode);
				}

				palCalPaxCount.setPaxCount(paxCount);
				palCalPaxCount.setPalCalHistory(palCalHistory);
				palCalPaxCount.setLastGroupCode(lastGroupCode);

				paxCountSet.add(palCalPaxCount);
			}
			palCalHistory.setCcPaxCountSet(paxCountSet);

			if (status.equals(PNLConstants.SITAMsgTransmission.SUCCESS)) {
				palCalHistory.setTransmissionStatus(transmissionStatus);
			} else {
				palCalHistory.setTransmissionStatus(PNLConstants.SITAMsgTransmission.FAIL);
				palCalHistory.setErrorDescription(status);
			}
			palCalHistory.setTransmissionTimeStamp(now);
			palCalHistory.setSitaMessage(DataConverterUtil.createBlob(blobString.getBytes()));

			if (customError != null) {

				// if unexpect error occurs show the message, else show the msg from prop file
				if (customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE)) {
					status = status + "||" + customError.getMessage();
				} else {
					status = status + "||" + customError.getMessageString();
				}

				// if empty adl then change the status to "E"
				if (customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_CAL_ERROR_CODE)
						|| customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.FLIGHT_CLOSED_CALERROR_CODE)
						|| customError.getExceptionCode().equals(PNLConstants.ERROR_CODES.FLIGHT_CLOSED_PALERROR_CODE)) {
					palCalHistory.setTransmissionStatus(PNLConstants.SITAMsgTransmission.ADL_EMPTY);
				}

				// db length cannot exceed 100
				if (status.length() > 99) {
					status = status.substring(0, 99);
				} else {
					palCalHistory.setErrorDescription(status);
				}
			}

			auxilliaryDAO.savePALCALTXHistory(palCalHistory);

		}
	}

	/**
	 * 
	 * @param object
	 * @return
	 */
	private static String decideStatus(Object object) {

		String status = "";

		if (object instanceof String) {
			status = (String) object;
			return status;
		}

		if (object instanceof SendFailedException) {
			SendFailedException exception = (SendFailedException) object;
			Address[] invalid = exception.getInvalidAddresses();
			if (invalid != null && invalid.length > 0) {
				return status = invalid[0].toString() + " invalid address";
			} else {
				status = exception.getMessage();
				if (status.length() > 100) {
					return status.substring(0, 98);
				} else {
					return status;
				}
			}

		}

		if (object instanceof MessagingException) {
			MessagingException exception = (MessagingException) object;
			String msg = exception.getLocalizedMessage();
			if (msg.length() > 100) {
				return status = msg.substring(0, 98);
			} else {
				return status = msg;
			}

		}

		if (object instanceof Exception) {
			Exception exception = (Exception) object;
			String msg = exception.getLocalizedMessage();
			if (msg.length() > 100) {
				return status = msg.substring(0, 98);
			} else {
				return status = msg;
			}

		}
		return status;
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getGenerateFilesContent(List<String> generatedFileNames) throws ModuleException {
		StringBuffer sbuffer = null;
		sbuffer = new StringBuffer();

		for (int i = 0; i < generatedFileNames.size(); i++) {
			File f = new File(generatedFileNames.get(i));

			try {

				FileReader r = new FileReader(f);
				BufferedReader buf = new BufferedReader(r);
				String tmp = null;
				while ((tmp = buf.readLine()) != null) {
					sbuffer.append(tmp + "\n");
				}
				sbuffer.append("\n\n\n");

				buf.close();
				r.close();
			} catch (IOException ioe) {
				log.error("Error in getting generated files content", ioe);
				throw new ModuleException(ioe, "airreservations.auxilliary.inputoutputerror", "airreservations");
			} catch (Exception e) {
				log.error("Error in getting generated files content", e);
				throw new ModuleException(e, "airreservations.auxilliary.inputoutputerror", "airreservations");
			}

		}
		return sbuffer.toString();

	}

	/**
	 * Move and Delete Adls/Pnls
	 * 
	 */
	public static void moveGeneratedFiles(List<String> generatedFileNames, String msgType) {

		try {

			String folderSentPath = "";

			if (msgType.equals(PNLConstants.PNLGeneration.PNL)) {
				folderSentPath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig()).getSentpnlpath();
			} else if (msgType.equals(PNLConstants.ADLGeneration.ADL)) {
				folderSentPath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig()).getSentadlpath();
			}else if (msgType.equals(PNLConstants.MessageTypes.PAL)) {
				folderSentPath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig()).getSentPalPath();
			}else if(msgType.equals(PNLConstants.MessageTypes.CAL)){
				folderSentPath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig()).getSentcalPath();
			}

			for (int i = 0; i < generatedFileNames.size(); i++) {
				File f = new File(generatedFileNames.get(i));

				File dir = new File(folderSentPath);

				// If directory does not exist create one
				if (!dir.exists()) {
					dir.mkdirs();
				}

				// Move file to new directory
				f.renameTo(new File(dir, f.getName()));
				f.delete();
			}

		} catch (Throwable e) {
			log.error("################# Error in moving to SENT folders", e);
		}

	}

	/**
	 * For saving the Error Log Called In Scheduled Service Context Only
	 * 
	 * @param logDto
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void savePNLADLErrorLog(int flightId, String msgType, String depAirportCode, String flightNumber,
			Date currentDate, ModuleException moduleException) throws ModuleException {// PNLADLLogDTO logDto) {

		try {

			PNLADLLogDTO logDto = new PNLADLLogDTO();
			logDto.setMessageType(msgType);
			logDto.setGeneratedMessageType(msgType);
			logDto.setProcessStartedDate(currentDate);
			logDto.setFlightID(String.valueOf(flightId));
			logDto.setDepartureAirPort(depAirportCode);
			logDto.setExceptionDescription(moduleException.getMessageString());
			logDto.setExceptionOccurredTime(currentDate);

			if (moduleException.getCause() != null) {
				logDto.setStackTraceElements(moduleException.getCause().getStackTrace());
			} else {
				logDto.setStackTraceElements(moduleException.getStackTrace());
			}

			LogDetailDTO logDetailDTO = getLogDetailDto(msgType, depAirportCode, flightNumber, currentDate);
			HashMap map = new HashMap();
			map.put("pnlADLLogDTO", logDto);
			logDetailDTO.setTemplateParams(map);

			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.recordMessage(logDetailDTO);

		} catch (Throwable e) {
			log.error("Error occured when trying to save to Audit for :" + msgType + " Fliggt Id :" + flightId, e);
		}

	}

	private static LogDetailDTO getLogDetailDto(String msgType, String depAirportCode, String flightNumber, Date currentDate) {

		LogDetailDTO logDetailDTO = new LogDetailDTO();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
		String fileName = "";
		fileName = msgType + "-" + depAirportCode + "-" + flightNumber + "-" + simpleDateFormat.format(currentDate) + ".log";
		
		if(PNLConstants.MessageTypes.PNL.equals(msgType) || PNLConstants.MessageTypes.ADL.equals(msgType)){
			logDetailDTO.setSaveFileName(PlatformConstants.getAbsPnlAdlAuditLogsPath() + "/" + fileName);
			logDetailDTO.setTemplateName(PNLConstants.LogTemplateName.PNL_ADL_LOG);
		
		} else if(PNLConstants.MessageTypes.PAL.equals(msgType) || PNLConstants.MessageTypes.CAL.equals(msgType)){
			logDetailDTO.setSaveFileName(PlatformConstants.getAbsPalCalAuditLogsPath() + "/" + fileName);
			logDetailDTO.setTemplateName(PNLConstants.LogTemplateName.PAL_CAL_LOG);
		
		}
		return logDetailDTO;

	}

	public static void updatePNLADLStatusInPaxFareSegmentsNew(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds,
			String status, Map<String, String> pnrPaxIdSegIdGroupId, String messageType) throws ModuleException {

		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		ReservarionPaxFareSegDAO paxFareSegDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_SEG_DAO;

		if ((pnrSegIds == null || pnrSegIds.size() == 0) || (pnrPaxIds == null || pnrPaxIds.size() == 0)) {

			return;
		}

		try {
			// Map<ppfsId, pnrpaxId+pnrsegId>
			Map<Integer, String> ppfsIds = auxilliaryDAO.getPnrPaxFareSegIds(pnrSegIds, pnrPaxIds);

			Collection<Integer> paxFareSegmentsWithoutGroupId = new ArrayList<Integer>();
			Map<String, Collection<Integer>> paxFareSegmentsWithGroupIdMap = new HashMap<String, Collection<Integer>>();
			Collection<ReservationPaxFareSegment> paxFareSegments = paxFareSegDAO.getReservationPaxFareSegmentsList(ppfsIds
					.keySet());
			if (paxFareSegments != null) {
				Iterator<ReservationPaxFareSegment> iterator = paxFareSegments.iterator();
				while (iterator.hasNext()) {
					ReservationPaxFareSegment fareSegment = iterator.next();
					fareSegment.setPnlStat(status);
					// String pnrPaxId =
					// fareSegment.getReservationPaxFare().getReservationPax().getPnrPaxId().toString();
					// String pnrSegId = fareSegment.getPnrSegId().toString();
					String uniqueId = ppfsIds.get(fareSegment.getPnrPaxFareSegId());

					if (pnrPaxIdSegIdGroupId.get(uniqueId) != null) {
						fareSegment.setGroupId(pnrPaxIdSegIdGroupId.get(uniqueId));

						if (paxFareSegmentsWithGroupIdMap.get(pnrPaxIdSegIdGroupId.get(uniqueId)) != null) {
							paxFareSegmentsWithGroupIdMap.get(pnrPaxIdSegIdGroupId.get(uniqueId)).add(
									fareSegment.getPnrPaxFareSegId());
						} else {
							Collection<Integer> paxFareSegmentsWithGroupId = new ArrayList<Integer>();
							paxFareSegmentsWithGroupId.add(fareSegment.getPnrPaxFareSegId());
							paxFareSegmentsWithGroupIdMap.put(pnrPaxIdSegIdGroupId.get(uniqueId), paxFareSegmentsWithGroupId);
						}
					} else {
						paxFareSegmentsWithoutGroupId.add(fareSegment.getPnrPaxFareSegId());
					}
				}
				// User jdbc update mechanism to avoid entire pnl status update failure. otherwise pax details will be
				// duplicated in DCS side.
				// paxFareSegDAO.savePaxFareSegments(paxFareSegments);

			}

			if (PNLConstants.MessageTypes.PNL.equals(messageType) || PNLConstants.MessageTypes.ADL.equals(messageType)) {
				paxFareSegDAO.updatePaxFareSegments(paxFareSegmentsWithoutGroupId, paxFareSegmentsWithGroupIdMap, status);
			} else if (PNLConstants.MessageTypes.PAL.equals(messageType) || PNLConstants.MessageTypes.CAL.equals(messageType)) {
				updatePalStatus(paxFareSegmentsWithoutGroupId, paxFareSegmentsWithGroupIdMap, status);
			}
		} catch (CommonsDataAccessException e) {
			log.error("Error in updatePNLADLStatusInPaxFareSegment", e);
			throw new ModuleException(e, e.getExceptionCode());
		} catch (Exception e) {
			log.error("Error in updatePNLADLStatusInPaxFareSegment", e);
			throw new ModuleException(PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE, e);
		}

	}

	private static void updatePalStatus(Collection<Integer> paxFareSegmentsWithoutGroupId,
			Map<String, Collection<Integer>> paxFareSegmentsWithGroupIdMap, String status) {

		if (paxFareSegmentsWithGroupIdMap != null) {
			for (String groupId : paxFareSegmentsWithGroupIdMap.keySet()) {
				if (paxFareSegmentsWithGroupIdMap.get(groupId) != null && !paxFareSegmentsWithGroupIdMap.get(groupId).isEmpty()) {
					paxFareSegmentsWithoutGroupId.addAll(paxFareSegmentsWithGroupIdMap.get(groupId));
				}
			}
		}

		List<ReservationPaxFareSegment> paxFareSegList = (List<ReservationPaxFareSegment>) ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_SEG_DAO
				.getReservationPaxFareSegmentsList(paxFareSegmentsWithoutGroupId);
		if (paxFareSegList != null && !paxFareSegList.isEmpty()) {
			for (ReservationPaxFareSegment ppfs : paxFareSegList) {
				ppfs.setCalStat(status);
			}
			ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_SEG_DAO.savePaxFareSegments(paxFareSegList);
		}
	}

	public static void updatePnlPassengerStatus(Collection<Integer> pnrSegIds) {

		List<PnlPassenger> passengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getPnlPassegers(pnrSegIds);
		if (!passengers.isEmpty()) {
			for (PnlPassenger passenger : passengers) {
				passenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
			}
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(passengers);
		}

	}
		
	public static void updatePnlPassengers(Collection<Integer> pnlPaxIds) {
		ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.updatePnlPassegerStatusToSent(pnlPaxIds);
	}
	
	public static void updateAirportMessagePaxStatus(Collection<Integer> pnrSegIds) {

		List<AirportMessagePassenger> passengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
				.getAirportMessagePassengersFromPnrSegIds(pnrSegIds);
		if (passengers!=null && !passengers.isEmpty()) {
			for (AirportMessagePassenger passenger : passengers) {
				passenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
			}
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.saveAirportMessagePassenger(passengers);
		}
	}

	public static void updatePnlPassengerStatusFromPnlPaxIds(Collection<Integer> pnlPaxIds) {

		List<PnlPassenger> passengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
				.getPnlPassegersFromPnlPaxIds(pnlPaxIds);
		if (!passengers.isEmpty()) {
			for (PnlPassenger passenger : passengers) {
				passenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
			}
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.savePnlPassengers(passengers);
		}

	}
	
	public static void updateAirportMessagePassengerStatusFromPnlPaxIds(Collection<Integer> airportMsgPaxIds) {

		List<AirportMessagePassenger> passengers = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO
				.getAirportMessagePassengers(airportMsgPaxIds);
		if (!passengers.isEmpty()) {
			for (AirportMessagePassenger passenger : passengers) {
				passenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
			}
			ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.saveAirportMessagePassenger(passengers);
		}

	}

	/**
	 * 
	 * @param flightNumber
	 * @param departureZulu
	 * @param AirportCode
	 * @param e
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void notifyAdminForManualAction(String flightNumber, Date departureZulu, String AirportCode, ModuleException e,
			String msgType, String additonalCustomDetailsOfError) {

		try {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			PNLADLEmailDTO edto = new PNLADLEmailDTO();
			edto.setAirportCode(AirportCode);
			edto.setDepartureDate(departureZulu);
			edto.setFlightNumber(flightNumber);
			edto.setMessageType(msgType);
			edto.setExceptionDetails(returnRootCauseDetails(e));
			if (e != null) {
				edto.setStackTraceElements(e.getStackTrace());
			}

			edto.setCustomMessage(BeanUtils.nullHandler(additonalCustomDetailsOfError) + " " + e.getMessageString());

			Properties props = ReservationModuleUtils.getAirReservationConfig().getEmailPnlAdlErrorTo();
			props.putAll(ReservationModuleUtils.getAirReservationConfig().getEmailPalCalErrorTo());

			Enumeration enumeration = props.elements();

			while (enumeration.hasMoreElements()) {

				String emailAddress = (String) enumeration.nextElement();
				MessageProfile profile = new MessageProfile();
				UserMessaging user = new UserMessaging();

				user.setToAddres(emailAddress);
				user.setFirstName(emailAddress);
				List messageList = new ArrayList();
				messageList.add(user);
				profile.setUserMessagings(messageList);

				Topic topic = new Topic();
				HashMap map = new HashMap();

				map.put("pnlADLEmailDTO", edto);

				topic.setTopicParams(map);
				topic.setTopicName("pnl_adl_email");

				profile.setTopic(topic);

				List list = new ArrayList();
				list.add(profile);

				messagingServiceBD.sendMessages(list);
			}
		} catch (Throwable e1) {
			log.error("Error When Trying to Notify Admin [Flight Number: " + flightNumber + "" + " Flt Dep : " + departureZulu
					+ " Airport :" + AirportCode + " ", e1);

			if (e != null) {
				log.error("[Exception wanted to log was -- > : ", e);
			}

		}

	}

	/**
	 * Will return the root cause details in the module exception
	 * 
	 * @param e
	 * @return
	 */
	private static String returnRootCauseDetails(ModuleException e) {
		if (e == null) {
			return null;
		}
		String NEWLINE = System.getProperty("line.separator");

		StackTraceElement[] stackTrace = e.getStackTrace();
		StringBuffer stackBuf = new StringBuffer();
		if (stackTrace != null) {
			int lines = Math.min(500, stackTrace.length);// Restricting max number of lines to 500
			for (int i = 0; i < lines; i++) {
				if (stackTrace[i] != null) {
					stackBuf.append(stackTrace[i].toString()).append(NEWLINE);
				}
			}
		}
		return stackBuf.toString();
	}

	/**
	 * 
	 * @param msgType
	 * @param ccCodeListInPaxRecords
	 * @param flightId
	 * @param depAirport
	 * @param finalList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void checkIFAllCOSToBeShown(String msgType, Collection ccCodeListInPaxRecords, int flightId, String depAirport,
			Collection finalList, HashMap ccPaxMap) {

		Collection pnlList = null;
		Iterator iteratorFinalList = finalList.iterator();

		while (iteratorFinalList.hasNext()) {
			pnlList = (Collection) iteratorFinalList.next();
		}

		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Collection<String> c = auxilliaryDAO.getClassOfServicesForFlight(flightId);

		// if only one cc code, no need to check the rest!
		if (c != null && c.size() == 1) {
			return;
		}
		Iterator cosIter = c.iterator();
		while (cosIter.hasNext()) {
			String cos = (String) cosIter.next();
			if (!ccCodeListInPaxRecords.contains(cos)) {

				if (msgType.equals(PNLConstants.ADLGeneration.ADL)) {

					PassengerCountRecordDTO countRecordDTO = new PassengerCountRecordDTO();
					countRecordDTO.setFareClass(cos);
					countRecordDTO.setDestination(depAirport);
					Integer currentPaxCount = (Integer) ccPaxMap.get(cos);
					if (currentPaxCount == null) {
						countRecordDTO.setTotalpassengers(0);
					} else {
						countRecordDTO.setTotalpassengers(currentPaxCount.intValue());
					}

					countRecordDTO.setAddpnlrecords(new ArrayList());
					countRecordDTO.setDelpnlrecords(new ArrayList());
					pnlList.add(countRecordDTO);

				}

				if (msgType.equals(PNLConstants.PNLGeneration.PNL)) {

					com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO countRecordDTO = new com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO();
					countRecordDTO.setFareClass(cos);
					countRecordDTO.setDestination(depAirport);
					countRecordDTO.setTotalpassengers(0);

					pnlList.add(countRecordDTO);

				}

			}
		}

		// sort the pnl list..to show higher cabin class first in the template.
		if (pnlList != null) {

			if (msgType.equals(PNLConstants.ADLGeneration.ADL)) {
				Collections.sort((List) pnlList, new COSPriorityFirstSortComparatorForADL());
			}
			if (msgType.equals(PNLConstants.PNLGeneration.PNL)) {
				Collections.sort((List) pnlList, new COSPriorityFirstSortComparatorForPNL());
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private static class COSPriorityFirstSortComparatorForADL implements Comparator {

		@Override
		public int compare(Object first, Object second) {

			PassengerCountRecordDTO countFirst = (PassengerCountRecordDTO) first;
			PassengerCountRecordDTO countSecond = (PassengerCountRecordDTO) second;
			if (countFirst == null || countSecond == null) {
				return 0;
			} else {

			}
			return (countFirst.getFareClass().compareTo(countSecond.getFareClass()));

		}
	}

	@SuppressWarnings("rawtypes")
	private static class COSPriorityFirstSortComparatorForPNL implements Comparator {

		@Override
		public int compare(Object first, Object second) {

			com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO countFirst = (com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO) first;
			com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO countSecond = (com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO) second;

			if (countFirst == null || countSecond == null) {
				return 0;
			}

			return (countFirst.getFareClass().compareTo(countSecond.getFareClass()));

		}
	}

	public static String getSendingOperationDescription(String fromMethod) {

		if (fromMethod.equals(PNLConstants.SendingMethod.MANUALLY)) {
			return "Manually";
		}

		if (fromMethod.equals(PNLConstants.SendingMethod.RESENDER)) {
			return "Resender";
		}

		if (fromMethod.equals(PNLConstants.SendingMethod.SCHEDSERVICE)) {
			return "Scheduled Service";
		}
		return fromMethod;
	}

	/**
	 * Populate TourIDs.
	 * 
	 * @return
	 */
	public static String[] populateTourIds() {
		String letters[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
				"U", "V", "W", "X", "Y", "Z" };

		String[] tourIds = new String[702];

		for (int k = 0; k < 26; k++) {
			tourIds[k] = letters[k];
		}
		for (int i = 0; i < 26; i++) {
			for (int j = 0; j < 26; j++) {
				tourIds[i * 26 + j + 26] = letters[i].concat(letters[j]);
			}
		}
		return tourIds;
	}

	public static boolean isNumber(char c) {
		boolean isNumber = false;
		String str = String.valueOf(c);
		try {
			Integer.parseInt(str);
			isNumber = true;
		} catch (NumberFormatException ne) {
			isNumber = false;
		}
		return isNumber;
	}

	public static int linesRequired(String str) {
		int lines;
		int maxLength = PNLConstants.PNLADLElements.MAX_LINE_LENGTH_IATA;
		int length = str.length();
		if (length % maxLength == 0) {
			lines = length / maxLength;
		} else {
			lines = length / maxLength + 1;
		}
		return lines;
	}

	public static int determineNextTourIndex(String lastGroupId, String tourIds[]) {
		int retVal = 0;
		for (int i = 0; i < tourIds.length; i++) {
			if (lastGroupId.equals(tourIds[i])) {
				retVal = ++i;
				break;
			}
		}
		return retVal;
	}

	@SuppressWarnings("finally")
	public static String populateTourIDsForADLNew(ArrayList<ADLRecordDTO> list, int tourIdIndex, String tourIds[]) {
		try {
			if (list != null) {

				boolean firstTourIndexConsumed = false;
				for (int index = 0; index < list.size(); index++) {
					ADLRecordDTO adlRecordDTO = list.get(index);
					if (index == 0) {
						if (adlRecordDTO.getTourID() != null) {
							adlRecordDTO.setTourID(tourIds[tourIdIndex]);
							firstTourIndexConsumed = true;
						}
					} else {
						if (adlRecordDTO.getTourID() != null) {
							ADLRecordDTO previousADLRecordDTO = list.get(index - 1);
							if (adlRecordDTO.getAutomatedPNR().equals(previousADLRecordDTO.getAutomatedPNR())) {
								adlRecordDTO.setTourID(tourIds[tourIdIndex]);
								if (!firstTourIndexConsumed)
									firstTourIndexConsumed = true;
							} else {
								if (firstTourIndexConsumed)
									++tourIdIndex;
								adlRecordDTO.setTourID(tourIds[tourIdIndex]);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Error in PnlADLUtil populateTourIDsForPNL", e);
		} finally {
			return tourIds[tourIdIndex];
		}
	}

	public static void prepareADLGroupCodeMap(ArrayList<ADLRecordDTO> adlRecDTOs, Map<String, String> map) {
		for (ADLRecordDTO adlRecDTO : adlRecDTOs) {
			String uniqueId = adlRecDTO.getPnrPaxId() + "|" + adlRecDTO.getPnrSegId();
			map.put(uniqueId, adlRecDTO.getTourID());
		}

	}

	public static void prepareGroupCodeMap(ArrayList<PNLRecordDTO> pnlRecDTOs, Map<String, String> map) {
		for (PNLRecordDTO pnlRecordDTO : pnlRecDTOs) {
			String uniqueId = pnlRecordDTO.getPnrPaxId() + "|" + pnlRecordDTO.getPnrSegId();
			map.put(uniqueId, pnlRecordDTO.getTourID());
		}

	}

	public static void prepareGroupCodeMapWith(Map<Integer,Integer> pnrPaxIdsvsSegmentIds, Map<String, String> map
			,Map<Integer,String> groupMap) {
		for (Map.Entry<Integer,Integer> entry : pnrPaxIdsvsSegmentIds
				.entrySet()) {
			if(groupMap.containsKey(entry.getKey())){
				String uniqueId = entry.getKey() + "|" + entry.getValue();
				map.put(uniqueId,groupMap.get(entry.getKey()));
			}

		}

	}
	
	// returns the last groupCode used in the pnl
	@SuppressWarnings("finally")
	public static String populateTourIDsForPNLNew(ArrayList<PNLRecordDTO> list, int tourIdIndex, String tourIds[]) {

		try {

			if (list != null) {

				boolean firstTourIndexConsumed = false;
				for (int index = 0; index < list.size(); index++) {
					PNLRecordDTO pnlRecordDTO = list.get(index);
					if (index == 0) {
						if (pnlRecordDTO.getTourID() != null) {
							pnlRecordDTO.setTourID(tourIds[tourIdIndex]);
							firstTourIndexConsumed = true;
						}
					} else {
						if (pnlRecordDTO.getTourID() != null) {
							PNLRecordDTO previousPNLRecordDTO = list.get(index - 1);
							if (pnlRecordDTO.getAutomatedPNR().equals(previousPNLRecordDTO.getAutomatedPNR())) {
								pnlRecordDTO.setTourID(tourIds[tourIdIndex]);
								if (!firstTourIndexConsumed)
									firstTourIndexConsumed = true;
							} else {
								if (firstTourIndexConsumed)
									++tourIdIndex;
								pnlRecordDTO.setTourID(tourIds[tourIdIndex]);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Error in PnlADLUtil populateTourIDsForPNL", e);
		} finally {

			return tourIds[tourIdIndex];

		}
		/*
		 * try{
		 * 
		 * String prevPnr =""; for (int i = 0; i < list.size() - 1; i++) { PNLRecordDTO dto1 = (PNLRecordDTO)
		 * list.get(i); PNLRecordDTO dto2 = (PNLRecordDTO) list.get(i + 1);
		 * 
		 * if (dto1.getTourID() != null & dto2.getTourID() != null) {
		 * 
		 * if (dto1.getAutomatedPNR().equals(dto2.getAutomatedPNR())) {
		 * 
		 * 
		 * dto1.setTourID(tourIds[tour]); dto2.setTourID(tourIds[tour]);
		 * 
		 * 
		 * if(!prevPnr.equals(dto1.getAutomatedPNR()) && !prevPnr.equals("")){ tour++; dto1.setTourID(tourIds[tour]);
		 * dto2.setTourID(tourIds[tour]); }else{ prevPnr = dto2.getAutomatedPNR(); } } } } //tour++; }catch(Exception
		 * e){ log.error("Error in PnlADLUtil populateTourIDsForPNL",e); } finally{ if(tour>0){ return tourIds[--tour];
		 * } return tourIds[0]; }
		 */
	}

	/*
	 * public static void populateTourIDsForPNL(ArrayList<PNLRecordDTO> list, int tour, String tourIds[]){ try{ // int
	 * tour = 0; for (int i = 0; i < list.size() - 1; i++) { PNLRecordDTO dto1 = (PNLRecordDTO) list.get(i);
	 * PNLRecordDTO dto2 = (PNLRecordDTO) list.get(i + 1); if (dto1.getTourID() != null & dto2.getTourID() != null) { if
	 * (dto1.getAutomatedPNR().equals(dto2.getAutomatedPNR())) { // dto1.setDuplicatedPNRaddress(true); //
	 * dto2.setDuplicatedPNRaddress(true); dto1.setTourID(tourIds[tour]); dto2.setTourID(tourIds[tour]); } else {
	 * tour++; } // tour++; } else if (dto1.getTourID() == null & dto2.getTourID() != null) { tour++; } } tour++;
	 * }catch(Exception e){ log.error("Error in PnlADLUtil populateTourIDsForPNL",e); } }
	 */

	private static boolean dTOAlreadyProcessed(ADLRecordDTO chg_adlRecordDTO, ArrayList<ADLRecordDTO> forcedADDList) {
		for (ADLRecordDTO recordDTO : forcedADDList) {
			if (recordDTO.getPnrSegId().equals(chg_adlRecordDTO.getPnrSegId())
					|| recordDTO.getAutomatedPNR().equals(chg_adlRecordDTO.getAutomatedPNR())) {
				return true;
			}
		}
		return false;
	}

	public static void removePaxScenario(ArrayList<ADLRecordDTO> delList, ADLRecordDTO chg_adlRecordDTO) {

		try {

			ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			// if no pnl passenger names return with false.
			List<PnlPassenger> list = auxilliaryDAO.getPnlPassegers(chg_adlRecordDTO.getPnrSegId(),
					PNLConstants.PnlPassengerStates.NOT_UPDATED, false);
			String groupId = null;
			if (list == null || list.size() == 0) {
				return;
			} else {
				for (PnlPassenger pnlPassenger : list) {
					if (groupId == null) {
						groupId = pnlPassenger.getGroupId();
					}
				}
			}
			Map<String, ADLRecordDTO> surNameDelRecordDTOMap = new HashMap<String, ADLRecordDTO>();

			for (PnlPassenger pnlPassenger : list) {
				String surName = BeanUtils.removeSpaces(pnlPassenger.getLastName().toUpperCase());
				surName = BeanUtils.removeExtraChars(surName);

				ADLRecordDTO del_adlRecordDTO = getAppropriateAdlRecordTO(surNameDelRecordDTOMap, surName);

				if (del_adlRecordDTO == null) {
					del_adlRecordDTO = new ADLRecordDTO();
					del_adlRecordDTO.setNames(new ArrayList<NameDTO>());
					del_adlRecordDTO.setAutomatedPNR(pnlPassenger.getPnr());
					del_adlRecordDTO.setPnrSegId(pnlPassenger.getPnrSegId());
					del_adlRecordDTO.setLastname(surName);
					del_adlRecordDTO.setTotalSeats(list.size());
					del_adlRecordDTO.setInfantString("");
					del_adlRecordDTO.setTourID(groupId);

				}

				NameDTO nameDTO = new NameDTO();
				String firstName = BeanUtils.removeSpaces(pnlPassenger.getFirstName().toUpperCase());
				firstName = BeanUtils.removeExtraChars(firstName);
				nameDTO.setFirstname(firstName);
				nameDTO.setPnrPaxId(pnlPassenger.getPnrPaxId());

				String paxType = pnlPassenger.getPaxType();
				String title = BeanUtils.nullHandler(pnlPassenger.getTitle()).toUpperCase();

				// Child
				if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
					nameDTO.setTitle(PNLConstants.PNLADLElements.CHILD);
					// Adult / Infant
				} else {
					nameDTO.setTitle(title);
				}

				constructBlanks(nameDTO);

				del_adlRecordDTO.getNames().add(nameDTO);
				surNameDelRecordDTOMap.put(surName, del_adlRecordDTO);

				pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);

			}

			// the ADLRecordDTO with 'C', which is the comming parameter will be put as an addition
			// and the ADLRecordDTO from the table (the PNL Passenger) will be shown as a deletion.
			prepareDelRecordList(delList, surNameDelRecordDTOMap);

			// if(chg_adlRecordDTO.getTourID()!=null){
			// chg_adlRecordDTO.setIndex(1);
			chg_adlRecordDTO.setTourID("A");
			chg_adlRecordDTO.setDuplicatedPNRaddress(true);

			auxilliaryDAO.savePnlPassengers(list);

		} catch (Exception e) {
			log.error("PnlAdlUtil remove pax scenario failed", e);

		}
	}

	public static boolean isNameChangeScenario(ArrayList<ADLRecordDTO> delList, ArrayList<ADLRecordDTO> addList,
			ADLRecordDTO chg_adlRecordDTO, ArrayList<ADLRecordDTO> forcedADDList) {

		try {

			if (dTOAlreadyProcessed(chg_adlRecordDTO, forcedADDList)) {

				chg_adlRecordDTO.setTourID("A");
				chg_adlRecordDTO.setIndex(1);
				chg_adlRecordDTO.setDuplicatedPNRaddress(true);

				addList.add(chg_adlRecordDTO);
				return true;
			}

			ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
			// if no pnl passenger names return with false.
			List<PnlPassenger> list = auxilliaryDAO.getPnlPassegers(chg_adlRecordDTO.getPnrSegId(),
					PNLConstants.PnlPassengerStates.NOT_UPDATED, false);

			/**
			 * byorn. fixme - there is a bug in the pnl bl logic, where the group id is sometimes skipped in
			 * pax_fare_segment table .. this will impact remove pax, and name change, below is a workaround
			 * 
			 */
			String groupId = null;
			if (list == null || list.size() == 0) {
				return false;
			} else {
				for (PnlPassenger pnlPassenger : list) {
					if (groupId == null) {
						groupId = pnlPassenger.getGroupId();
					}
				}
				if (groupId == null) {
					groupId = "A";
				}
			}

			Map<String, ADLRecordDTO> surNameDelRecordDTOMap = new HashMap<String, ADLRecordDTO>();
			int totalNameLength = 0;
			int index = 0;
			boolean isFirstRecord = true;
			String lastSurname = null;
			boolean isSurnameAltered = false;
			for (PnlPassenger pnlPassenger : list) {
				String surName = BeanUtils.removeSpaces(pnlPassenger.getLastName().toUpperCase());
				surName = BeanUtils.removeExtraChars(surName);
				if (!isSurnameAltered) {
					lastSurname = surName;
				}
				ADLRecordDTO del_adlRecordDTO = getAppropriateAdlRecordTO(surNameDelRecordDTOMap, lastSurname);

				if (del_adlRecordDTO == null) {
					del_adlRecordDTO = new ADLRecordDTO();
					del_adlRecordDTO.setNames(new ArrayList<NameDTO>());
					del_adlRecordDTO.setAutomatedPNR(pnlPassenger.getPnr());
					del_adlRecordDTO.setPnrSegId(pnlPassenger.getPnrSegId());
					del_adlRecordDTO.setPnrPaxId(pnlPassenger.getPnrPaxId());
					del_adlRecordDTO.setLastname(surName);
					del_adlRecordDTO.setTotalSeats(list.size());
					del_adlRecordDTO.setInfantString("");
					// ensuring the group id was existing for all the saved pnl passengers. if not existing for any,
					// then fine.
					del_adlRecordDTO.setTourID(groupId);
				}

				String firstName = BeanUtils.removeSpaces(pnlPassenger.getFirstName().toUpperCase());
				firstName = BeanUtils.removeExtraChars(firstName);
				String title = BeanUtils.nullHandler(pnlPassenger.getTitle()).toUpperCase();
				int paxNameLength = 0;
				if (isFirstRecord) {
					// + 1 for the /
					paxNameLength = surName.length() + firstName.length() + title.length() + groupId.length() + 1;
					isFirstRecord = false;
				} else {
					paxNameLength = firstName.length() + title.length() + 1;
				}
				totalNameLength = totalNameLength + paxNameLength;

				if (totalNameLength > 55) {
					// If the length is greater will have new adl record
					totalNameLength = surName.length() + firstName.length() + title.length() + groupId.length() + 1;
					del_adlRecordDTO = new ADLRecordDTO();
					del_adlRecordDTO.setNames(new ArrayList<NameDTO>());
					del_adlRecordDTO.setAutomatedPNR(pnlPassenger.getPnr());
					del_adlRecordDTO.setPnrSegId(pnlPassenger.getPnrSegId());
					del_adlRecordDTO.setPnrPaxId(pnlPassenger.getPnrPaxId());
					del_adlRecordDTO.setLastname(surName);
					del_adlRecordDTO.setTotalSeats(list.size());
					del_adlRecordDTO.setInfantString("");
					// ensuring the group id was existing for all the saved pnl passengers. if not existing for any,
					// then fine.
					del_adlRecordDTO.setTourID(groupId);
					index++;
					lastSurname = surName + index;
					isSurnameAltered = true;
				} else {
					isSurnameAltered = false;
				}

				NameDTO nameDTO = new NameDTO();
				nameDTO.setFirstname(firstName);
				nameDTO.setPnrPaxId(pnlPassenger.getPnrPaxId());
				String paxType = pnlPassenger.getPaxType();

				// Child
				if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
					nameDTO.setTitle(PNLConstants.PNLADLElements.CHILD);
					// Adult / Infant
				} else {
					nameDTO.setTitle(title);
				}
				constructBlanks(nameDTO);

				del_adlRecordDTO.getNames().add(nameDTO);

				surNameDelRecordDTOMap.put(lastSurname, del_adlRecordDTO);
				pnlPassenger.setStatus(PNLConstants.PnlPassengerStates.UPDATED);
			}

			// the ADLRecordDTO with 'C', which is the comming parameter will be put as an addition
			// and the ADLRecordDTO from the table (the PNL Passenger) will be shown as a deletion.
			prepareDelRecordList(delList, surNameDelRecordDTOMap);

			// if(chg_adlRecordDTO.getTourID()!=null){
			// chg_adlRecordDTO.setIndex(1);
			chg_adlRecordDTO.setTourID("A");
			chg_adlRecordDTO.setDuplicatedPNRaddress(true);

			// }
			addList.add(chg_adlRecordDTO);

			forcedADDList.add(chg_adlRecordDTO);
			auxilliaryDAO.savePnlPassengers(list);

			return true;
		} catch (Exception e) {
			log.error("PnlAdlUtil isNameChangeScenario failed", e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private static void prepareDelRecordList(ArrayList<ADLRecordDTO> delList, Map<String, ADLRecordDTO> surNameDelRecordDTOMap) {
		try {
			// boolean tourIDsExist = surNameDelRecordDTOMap.size() > 1 ? true : false;
			List<String> keys = new ArrayList<String>(surNameDelRecordDTOMap.keySet());
			Collections.sort(keys);

			int i = 0;
			for (String ky : keys) {
				ADLRecordDTO recordDTO = surNameDelRecordDTOMap.get(ky);
				recordDTO.setNumberPAD(recordDTO.getNames().size());
				Collections.sort(recordDTO.getNames());
				recordDTO.setDuplicatedPNRaddress(recordDTO.getNames().size() != recordDTO.getTotalSeats());
				if (i != 0) {
					recordDTO.setIndex(i);
				}

				delList.add(recordDTO);

				i++;
			}
		} catch (Exception e) {
			log.error("Error in  PnlAdlUtil prepareDelRecordList", e);
		}

	}

	private static ADLRecordDTO getAppropriateAdlRecordTO(Map<String, ADLRecordDTO> surNameDelRecordDTOMap, String sirName) {
		return surNameDelRecordDTOMap.get(sirName);

	}

	private static void constructBlanks(NameDTO nameDTO) {
		nameDTO.setSsrText("");
		nameDTO.setSsrCode("");
		nameDTO.setRemarkText("");
		nameDTO.setLastName("");
		nameDTO.setSsrFirstName("");
		nameDTO.setSsrTitle("");
		nameDTO.setPad("");
	}

	public static Map<Integer, String> getSeatMapDetails(Collection<PNLADLDestinationDTO> passengerDetails, boolean isPNL) {

		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();
			for (PNLADLDestinationDTO destinationDTO : passengerDetails) {
				Collection<ReservationPaxDetailsDTO> res = destinationDTO.getPassenger();
				for (ReservationPaxDetailsDTO resPaxDetailsDTO : res) {
					pnrPaxIds.add(resPaxDetailsDTO.getPnrPaxId());
					pnrSegIds.add(resPaxDetailsDTO.getPnrSegId());
				}
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;
				return seatMapDAO.getReservationSeatsForPNL(pnrPaxIds, pnrSegIds);
			}
		} catch (CommonsDataAccessException e) {
			log.error("############# getSeatMapDetails [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("Seat Map", new Date(), "Seat Map", m, "Seat Map",
					"Retrieving Seat Map Details");
		} catch (Exception e) {
			log.error("############# getSeatMapDetails [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("Seat Map", new Date(), "Seat Map", m, "Seat Map",
					"Retrieving Seat Map Details");
		}

		return new HashMap<Integer, String>();
	}

	public static Map<Integer, List<AncillaryDTO>> getPassengerSeatMapDetails(List<PassengerInformation> passengerDetails) {

		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();
			for (PassengerInformation passenger : passengerDetails) {
				pnrPaxIds.add(passenger.getPnrPaxId());
				pnrSegIds.add(passenger.getPnrSegId());
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;
				return seatMapDAO.getReservationSeatsForPNLADL(pnrPaxIds, pnrSegIds);
			}
		} catch (CommonsDataAccessException e) {
			log.error("############# getSeatMapDetails [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("Seat Map", new Date(), "Seat Map", m, "Seat Map",
					"Retrieving Seat Map Details");
		} catch (Exception e) {
			log.error("############# getSeatMapDetails [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("Seat Map", new Date(), "Seat Map", m, "Seat Map",
					"Retrieving Seat Map Details");
		}

		return new HashMap<Integer, List<AncillaryDTO>>();
	}

	/**
	 * Method extract Meal details from flight to send the Pnl
	 * 
	 * @param passengerDetails
	 * @param isPNL
	 * @return
	 */
	public static Map<Integer, String> getMealsDetails(Collection<PNLADLDestinationDTO> passengerDetails, boolean isPNL) {

		Map<Integer, String> mealMap = new HashMap<Integer, String>();
		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();

			for (PNLADLDestinationDTO destinationDTO : passengerDetails) {
				Collection<ReservationPaxDetailsDTO> res = destinationDTO.getPassenger();
				for (ReservationPaxDetailsDTO resPaxDetailsDTO : res) {
					pnrPaxIds.add(resPaxDetailsDTO.getPnrPaxId());
					pnrSegIds.add(resPaxDetailsDTO.getPnrSegId());
				}
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
				mealMap = mealDAO.getReservationMealsForPNL(pnrPaxIds, pnrSegIds);
			}
		} catch (CommonsDataAccessException e) {
			log.error("############# get Meal Details [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("Meal", new Date(), "Meal", m, "Meal", "Retrieving Meal Details");
		} catch (Exception e) {
			log.error("############# get Meal Details [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("Meal", new Date(), "Meal", m, "Meal", "Retrieving Meal Details");
		}

		return mealMap;
	}

	public static Map<Integer, List<AncillaryDTO>> getPassengerMealDetails(List<PassengerInformation> passengerDetails) {

		Map<Integer, List<AncillaryDTO>> mealMap = new HashMap<Integer, List<AncillaryDTO>>();
		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();

			for (PassengerInformation passenger : passengerDetails) {
				pnrPaxIds.add(passenger.getPnrPaxId());
				pnrSegIds.add(passenger.getPnrSegId());
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
				mealMap = mealDAO.getReservationMealsForPNLADL(pnrPaxIds, pnrSegIds);
			}
		} catch (CommonsDataAccessException e) {
			log.error("############# get Meal Details [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("Meal", new Date(), "Meal", m, "Meal", "Retrieving Meal Details");
		} catch (Exception e) {
			log.error("############# get Meal Details [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("Meal", new Date(), "Meal", m, "Meal", "Retrieving Meal Details");
		}

		return mealMap;
	}

	/**
	 * Method extract Baggage details from flight to send the Pnl
	 * 
	 * @param passengerDetails
	 * @param isPNL
	 * @return
	 */
	public static Map<Integer, String> getBaggagesDetails(Collection<PNLADLDestinationDTO> passengerDetails, boolean isPNL,
			String airportCode) {

		Map<Integer, String> baggageMap = new HashMap<Integer, String>();
		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();

			for (PNLADLDestinationDTO destinationDTO : passengerDetails) {
				Collection<ReservationPaxDetailsDTO> res = destinationDTO.getPassenger();
				for (ReservationPaxDetailsDTO resPaxDetailsDTO : res) {
					pnrPaxIds.add(resPaxDetailsDTO.getPnrPaxId());
					pnrSegIds.add(resPaxDetailsDTO.getPnrSegId());
				}
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
				String ssrCode = baggageDAO.getBaggageSsrCodesToSendPNL(airportCode);
				baggageMap = baggageDAO.getReservationBaggagesForPNL(pnrPaxIds, pnrSegIds, ssrCode);
			}
		} catch (CommonsDataAccessException e) {
			log.error("############# get Baggage Details [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("Baggage", new Date(), "Baggage", m, "Baggage", "Retrieving Baggage Details");
		} catch (Exception e) {
			log.error("############# get Baggage Details [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("Baggage", new Date(), "Baggage", m, "Baggage", "Retrieving Baggage Details");
		}

		return baggageMap;
	}

	public static Map<Integer, AncillaryDTO> getPassengerBaggagesDetails(Collection<PassengerInformation> passengers,
			String airportCode) {

		Map<Integer, AncillaryDTO> baggageMap = new HashMap<Integer, AncillaryDTO>();
		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();

			for (PassengerInformation passenger : passengers) {
				pnrPaxIds.add(passenger.getPnrPaxId());
				pnrSegIds.add(passenger.getPnrSegId());
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
				String ssrCode = baggageDAO.getBaggageSsrCodesToSendPNL(airportCode);
				if (ssrCode != null && !ssrCode.trim().equals("")) {
					Map<Integer, String> baggageStrMap = baggageDAO.getReservationBaggagesForPNL(pnrPaxIds, pnrSegIds, ssrCode);
					for (Integer pnrPaxId : baggageStrMap.keySet()) {
						String baggage = baggageStrMap.get(pnrPaxId);
						if (baggage != null) {
							String baggageArr[] = baggage.split("%");
							AncillaryDTO anci = new AncillaryDTO();
							anci.setAnciType(AnciTypes.BAGGAGE);
							anci.setCode(baggageArr[0]);
							if (baggageArr.length > 1) {
								anci.setDescription(baggageArr[1]);
							}
							baggageMap.put(pnrPaxId, anci);
						}
					}
				} else {
					baggageMap = baggageDAO.getReservationBaggagesForPNLADL(pnrPaxIds, pnrSegIds);
				}

			}
		} catch (CommonsDataAccessException e) {
			log.error("############# get Baggage Details [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("Baggage", new Date(), "Baggage", m, "Baggage", "Retrieving Baggage Details");
		} catch (Exception e) {
			log.error("############# get Baggage Details [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("Baggage", new Date(), "Baggage", m, "Baggage", "Retrieving Baggage Details");
		}

		return baggageMap;
	}

	/**
	 * Method extract Baggage details from flight to send the Pnl
	 * 
	 * @param passengerDetails
	 * @param isPNL
	 * @return
	 */
	public static Map<Integer, String> getBaggagesWeights(Collection<PNLADLDestinationDTO> passengerDetails, boolean isPNL,
			String airportCode) {

		Map<Integer, String> baggageMap = new HashMap<Integer, String>();
		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();

			for (PNLADLDestinationDTO destinationDTO : passengerDetails) {
				Collection<ReservationPaxDetailsDTO> res = destinationDTO.getPassenger();
				for (ReservationPaxDetailsDTO resPaxDetailsDTO : res) {
					pnrPaxIds.add(resPaxDetailsDTO.getPnrPaxId());
					pnrSegIds.add(resPaxDetailsDTO.getPnrSegId());
				}
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
				baggageMap = baggageDAO.getReservationBaggageWeightsForPNL(pnrPaxIds, pnrSegIds);

			}
		} catch (CommonsDataAccessException e) {
			log.error("############# get Baggage Details [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("Baggage", new Date(), "Baggage", m, "Baggage", "Retrieving Baggage Details");
		} catch (Exception e) {
			log.error("############# get Baggage Details [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("Baggage", new Date(), "Baggage", m, "Baggage", "Retrieving Baggage Details");
		}

		return baggageMap;
	}

	public static Map<Integer, Collection<PaxSSRDetailDTO>> getSSRDetails(Collection<PNLADLDestinationDTO> passengerDetails,
			boolean isPNL) {

		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();
			Map<Integer, Integer> mapPnrPaxSegIds = new HashMap<Integer, Integer>();
			for (PNLADLDestinationDTO destinationDTO : passengerDetails) {
				Collection<ReservationPaxDetailsDTO> res = destinationDTO.getPassenger();
				for (ReservationPaxDetailsDTO resPaxDetailsDTO : res) {
					pnrPaxIds.add(resPaxDetailsDTO.getPnrPaxId());
					pnrSegIds.add(resPaxDetailsDTO.getPnrSegId());
					mapPnrPaxSegIds.put(resPaxDetailsDTO.getPnrPaxId(), resPaxDetailsDTO.getPnrSegId());
				}
			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				return ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getPNRPaxSSRs(mapPnrPaxSegIds, isPNL, false, false);
			}
		} catch (CommonsDataAccessException e) {
			log.error("############# getSSRDetails [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("SSR Details", new Date(), "SSR Details", m, "SSR Details",
					"Retrieving SSR Details");
		} catch (Exception e) {
			log.error("############# getSSRDetails [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("SSR Details", new Date(), "SSR Details", m, "SSR Details",
					"Retrieving SSR Details");
		}

		return new HashMap<Integer, Collection<PaxSSRDetailDTO>>();
	}

	public static Map<Integer, List<AncillaryDTO>> getPassengerSSRDetails(Collection<PassengerInformation> passengers, DCS_PAX_MSG_GROUP_TYPE msgType) {

		try {
			Collection<Integer> pnrPaxIds = new HashSet<Integer>();
			Collection<Integer> pnrSegIds = new HashSet<Integer>();
			Map<Integer, Integer> mapPnrPaxSegIds = new HashMap<Integer, Integer>();
			for (PassengerInformation passenger : passengers) {
				pnrPaxIds.add(passenger.getPnrPaxId());
				pnrSegIds.add(passenger.getPnrSegId());
				switch (msgType) {
				case PAL_CAL_AC:
					if (passenger.getPnrStatus().equals("CNF")) {
						mapPnrPaxSegIds.put(passenger.getPnrPaxId(), passenger.getPnrSegId());
					}
					break;
				case PAL_CAL_D:
					//if (passenger.getPnrStatus().equals("CNX")) {
						mapPnrPaxSegIds.put(passenger.getPnrPaxId(), passenger.getPnrSegId());
					//}
					break;
				default:
					mapPnrPaxSegIds.put(passenger.getPnrPaxId(), passenger.getPnrSegId());

				}

			}

			if (pnrPaxIds.size() > 0 && pnrSegIds.size() > 0) {
				return ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getPasssengerSSRDetails(mapPnrPaxSegIds, msgType);
			}
		} catch (CommonsDataAccessException e) {
			log.error("############# getSSRDetails [CommonsDataAccessException] ", e);
			ModuleException m = new ModuleException(e, e.getExceptionCode());
			PnlAdlUtil.notifyAdminForManualAction("SSR Details", new Date(), "SSR Details", m, "SSR Details",
					"Retrieving SSR Details");
		} catch (Exception e) {
			log.error("############# getSSRDetails [Exception] ", e);
			ModuleException m = new ModuleException(e, e.getMessage());
			PnlAdlUtil.notifyAdminForManualAction("SSR Details", new Date(), "SSR Details", m, "SSR Details",
					"Retrieving SSR Details");
		}

		return new HashMap<Integer, List<AncillaryDTO>>();
	}

	public static String findGender(String title) {
		String gender = ReservationInternalConstants.Gender.UNDISCLOSED;
		if (title != null && !title.equals("")) {
			if (ReservationInternalConstants.PassengerTitle.MR.equals(title)
					|| ReservationInternalConstants.PassengerTitle.MASTER.equals(title)) {
				gender = ReservationInternalConstants.Gender.MALE;
			} else if (ReservationInternalConstants.PassengerTitle.MS.equals(title)
					|| ReservationInternalConstants.PassengerTitle.MRS.equals(title)
					|| ReservationInternalConstants.PassengerTitle.MISS.equals(title)) {
				gender = ReservationInternalConstants.Gender.FEMALE;
			}
		}
		return gender;
	}

	public static String generateRBDValues(Integer flightId) throws ModuleException {
		StringBuilder sb = new StringBuilder(PNLConstants.PNLGeneration.RBD);
		sb.append(PNLConstants.PNLADLElements.SPACE);
		LinkedHashMap<String, List<String>> rbdDetails = ReservationModuleUtils.getFlightInventoryBD()
				.getAllocatedBookingClassesForCabinClasses(flightId);

		boolean isNotFirstValue = false;
		List<String> addedBCs = new ArrayList<String>();
		for (String cabinClass : rbdDetails.keySet()) {
			addedBCs.clear();
			if (isNotFirstValue) {
				sb.append(PNLConstants.PNLADLElements.SPACE);
			}
			sb.append(cabinClass);
			sb.append(PNLConstants.PNLADLElements.B_SLASH);
			List<String> allLcc = rbdDetails.get(cabinClass);
			for (String lcc : allLcc) {
				String bc = String.valueOf(lcc.charAt(0));
				if (!addedBCs.contains(bc)) {
					sb.append(bc);
					addedBCs.add(bc);
				}
			}
			isNotFirstValue = true;
		}

		return sb.toString();
	}

	public static boolean isMultipleSeatsBooked(Integer pnrPaxId, Map<Integer, String> bookedSeatMapDetails) {
		if (bookedSeatMapDetails.get(pnrPaxId) != null) {
			String seatMapDetails = bookedSeatMapDetails.get(pnrPaxId);
			if (seatMapDetails.split(" ").length > 1) {
				return true;
			}
		}
		return false;
	}

	public static String loadPassengerNationality(Integer nationalityId) {
		try {
			if (nationalityId == null) {
				nationalityId = 0;
			}
			Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(nationalityId);
			if (nat != null && nat.getIsoCode() != null) {
				return nat.getIsoCode();
			}
		} catch (ModuleException ex) {

		}
		return "";
	}

	public static String findMatchingFlightSegmentcode(int flightId, String departure, String arrival) {
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Collection<String> flightSegmentCodes = auxilliaryDAO.getAllflightSegments(flightId);
		for (String segCode : flightSegmentCodes) {
			if (segCode.startsWith(departure) && segCode.endsWith(arrival)) {
				return segCode;
			}
		}
		// This can not be happened. but return this just in case
		return departure + "/" + arrival;
	}

	public static void compileMealDetails(Map<Integer, String> pnrPaxIdMealMap, Integer pnrPaxId, DCSPassenger dp) {
		String mealDetails = pnrPaxIdMealMap.get(pnrPaxId);
		if (mealDetails != null) {
			String arrMealDetails[] = mealDetails.split("%");
			DCSMeal dcsMeal = new DCSMeal();
			dcsMeal.setIataMealCode(arrMealDetails[0]);
			if (arrMealDetails.length > 2) {
				dcsMeal.setMealDescription(arrMealDetails[1]);
			}
			dp.getMeals().add(dcsMeal);
		}
	}

	public static void compileBaggageDetails(Map<Integer, String> pnrPaxIdBaggageMap, Integer pnrPaxId, DCSPassenger dp) {
		if (pnrPaxIdBaggageMap.get(pnrPaxId) != null) {
			StringBuffer baggageDetails = new StringBuffer(pnrPaxIdBaggageMap.get(pnrPaxId));
			dp.getBaggageInformation().add(baggageDetails.append("kg").toString());
		}
	}

	public static void compileSeatDetails(Map<Integer, String> pnrPaxIdSeatMap, Integer pnrPaxId, DCSPassenger dp) {
		String seatNumber = pnrPaxIdSeatMap.get(pnrPaxId);
		if (seatNumber != null) {
			dp.getSeatNumbers().add(seatNumber);
		}
	}

	public static void compileSsrDetails(Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxIdSSRMap, Integer pnrPaxId,
			DCSPassenger dp) {
		Collection<PaxSSRDetailDTO> paxSsrDetails = pnrPaxIdSSRMap.get(pnrPaxId);
		if (paxSsrDetails != null) {
			for (PaxSSRDetailDTO paxSsrDto : paxSsrDetails) {
				DCSSSRRequest dssr = new DCSSSRRequest();
				dssr.setIataSSRCode(paxSsrDto.getSsrCode());
				dssr.setSsrDescription(paxSsrDto.getSsrDesc());
				dp.getSsrRequests().add(dssr);
			}
		}

	}

	public static void saveWSADLHistory(String airportCode, int flightId, Timestamp currentTimeStamp,String transmissionStatus) throws ModuleException {

		PnlAdlHistory adlHistory = new PnlAdlHistory();
		adlHistory.setAirportCode(airportCode);
		adlHistory.setEmail("DCS_WS");
		adlHistory.setFlightID(flightId);
		adlHistory.setMessageType("ADL");// ADL
		adlHistory.setTransmissionStatus(transmissionStatus);
		adlHistory.setTransmissionTimeStamp(currentTimeStamp);
		ReservationModuleUtils.getReservationAuxilliaryBD().saveDCSPnlAdlHistory(adlHistory);

	}

	public static HashMap sendPNLADLViaSitatex(List<String> sitaAddresses, List<String> generatedFilenames, String messageType,
			HashMap sitaStatusMap, String messageText) throws ModuleException {

		Iterator<String> iterator = sitaAddresses.iterator();

		while (iterator.hasNext()) {

			String sitaAddress = iterator.next();
			sitaStatusMap.put(sitaAddress, PNLConstants.SITAMsgTransmission.SUCCESS);

			String filename = null;
			String messageBody = null;
			int i = 0;
			if (messageText == null) {
				try {

					for (i = 0; i < generatedFilenames.size(); i++) {
						filename = generatedFilenames.get(i);
						File f = new File(filename);

						OutMessageTO outMessageTo = new OutMessageTO();

						outMessageTo.setRecipientTTYAddress(sitaAddress);
						try {
							outMessageTo.setAirLineCode(AppSysParamsUtil.getCarrierCode());
						} catch (ModuleException e) {
						}
						outMessageTo.setMessageType(messageType);
						outMessageTo.setModeOfCommunication(Email);
						outMessageTo.setSentTTYAddress(getSitaTexSenderAddress(messageType));
						outMessageTo.setRecipientTTYAddress(sitaAddress);

						messageBody = SendMailUsingAuthentication.readPnlFile(f.getAbsolutePath());
						outMessageTo.setOutMessageText(messageBody);
						outMessageTo.setReceivedDate(new Date());
						outMessageTo.setStatusIndicator(Untransmitted);

						MessageComposerApiUtils.saveOutMessage(outMessageTo);

						if (log.isDebugEnabled())
							log.debug("PNL message content : " + messageBody);
					}
				} catch (Throwable e) {
					log.debug("Error in PNL message content.");
				}
			} else {
				OutMessageTO outMessageTo = new OutMessageTO();
				messageBody = messageText;

				try {
					outMessageTo.setAirLineCode(AppSysParamsUtil.getCarrierCode());
				} catch (ModuleException e) {
				}
				outMessageTo.setRecipientTTYAddress(sitaAddress);
				outMessageTo.setMessageType(messageType);
				outMessageTo.setModeOfCommunication(Email);
				outMessageTo.setSentTTYAddress(getSitaTexSenderAddress(messageType));
				outMessageTo.setOutMessageText(messageBody);
				outMessageTo.setReceivedDate(new Date());
				outMessageTo.setStatusIndicator(Untransmitted);
				MessageComposerApiUtils.saveOutMessage(outMessageTo);
			}

		}
		return sitaStatusMap;
	}
	
	private static String getSitaTexSenderAddress(String msgType){
		String sitaTexSenderAddress =null;
		if(PNLConstants.MessageTypes.PNL.equals(msgType)||PNLConstants.MessageTypes.ADL.equals(msgType)){
			sitaTexSenderAddress = airReservationConfig.getSitaTexSenderAddress();
		}else if(PNLConstants.MessageTypes.PAL.equals(msgType)||PNLConstants.MessageTypes.CAL.equals(msgType)){
			sitaTexSenderAddress = airReservationConfig.getPalCalSitaTexSenderAddress();
		}
		return sitaTexSenderAddress;
	}
	
	public static String getOndFromDepartureAirport(String departureAirport, Map<String, String> legNumberWiseOndMap){
		String ondFromDepartureAirport = "";
		Iterator it = legNumberWiseOndMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        String ond = (String) pair.getValue();
	        if (departureAirport.equals(ond.substring(0, 3))){
	        	ondFromDepartureAirport = ond;
	        	break;
	        }
	    }
	    return ondFromDepartureAirport;
	}

	public static Map<String, List<String>> getSitaAddressMap(String depAirportCode, String flightNumber, String ond, String carrierCode, List<String> sitas, DCS_PAX_MSG_GROUP_TYPE msgType) {
		ReservationAuxilliaryDAO auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		Map<String, List<String>> sitaAddressesMap = auxilliaryDAO.getActiveSITAAddresses(depAirportCode.trim(), flightNumber, ond, carrierCode, msgType);
		List<String> sitaAddressesList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
		List<String> sitaTexAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
		List<String> airincAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
		

		List<String> sitaTexAddress = new ArrayList<String>();
		List<String> sitaAddress = new ArrayList<String>();
		List<String> airincAddresses = new ArrayList<String>();
		for (String sitaAdd : sitaTexAddressList) {
			if (sitas.contains(sitaAdd) && !sitaTexAddress.contains(sitaAdd)) {
				sitaTexAddress.add(sitaAdd);
			}
		}
		for (String sitaAdd : sitaAddressesList) {
			if (sitas.contains(sitaAdd) && !sitaAddress.contains(sitaAdd)) {
				sitaAddress.add(sitaAdd);
			}
		}
		for (String airincAddress : airincAddressList) {
			if (sitas.contains(airincAddress) && !sitaAddress.contains(airincAddress)) {
				airincAddresses.add(airincAddress);
			}
		}

		Map<String, List<String>> selectedSitaAddressMap = new HashMap<String, List<String>>();
		selectedSitaAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddress);
		selectedSitaAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_SITATEX, sitaTexAddress);
		selectedSitaAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, airincAddresses);
		return selectedSitaAddressMap;
	}

	public static String compileDepartureDate(Date departureDate) {
		StringBuffer data = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMM");
		data.append(dateFormat.format(departureDate));
		return data.toString().toUpperCase();
	}
	
	public static String composeMarketingFlightElement(ReservationPaxDetailsDTO pax) {

		StringBuilder data = new StringBuilder();
		if (pax != null && pax.getCsFlightNo() != null && pax.getCsBookingClass() != null && pax.getCsOrginDestination() != null
				&& pax.getCsDepatureDateTime() != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd");
			SimpleDateFormat tf = new SimpleDateFormat("HHmm");
			data.append(MessageComposerConstants.POINT);
			data.append(MessageComposerConstants.PNLADLMessageConstants.M);
			data.append(MessageComposerConstants.FWD);
			data.append(pax.getCsFlightNo());
			data.append(pax.getCsBookingClass());
			data.append(df.format(pax.getCsDepatureDateTime()));
			data.append(getOndCode(pax.getCsOrginDestination()));
			data.append(tf.format(pax.getCsDepatureDateTime()));
			data.append(MessageComposerConstants.HK);
		}

		return data.toString();
	}
	
	public static String getOndCode(String segmentCode) {
		if (segmentCode != null && segmentCode.length() > 6) {
			segmentCode = segmentCode.substring(0, 3) + "" + segmentCode.substring(segmentCode.length() - 3);
		}
		return segmentCode;
	}

	private static String[] extractEmailUser(String emailAddress) {

		String[] eamilUser = null;
		if (emailAddress.indexOf("@") != -1) {
			eamilUser = emailAddress.split("@");
		} else {
			String[] userArray = { emailAddress };
			eamilUser = userArray;
		}

		return eamilUser;
	}
	
}
