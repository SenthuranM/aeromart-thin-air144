/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.passenger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for composing a modification for passenger refund
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cMForPaxRefund"
 */
public class CMForPaxRefund extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CMForPaxRefund.class);

	/**
	 * Execute method of the PaxRefund command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Map<Integer, PaymentAssembler> pnrPaxIdAndPayments = (Map<Integer, PaymentAssembler>) this
				.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		String userNotes = (String) this.getParameter(CommandParamNames.USER_NOTES);
		String userNoteType = (String) this.getParameter(CommandParamNames.USER_NOTE_TYPE);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		Boolean isAutoRefund = (Boolean) this.getParameter(CommandParamNames.PAX_REFUND_TYPE);

		// Checking params
		this.validateParams(pnr, pnrPaxIdAndPayments, userNotes, credentialsDTO);

		// Compose Modification object
		Collection<ReservationAudit> colReservationAudit = this.composeAudit(pnr, pnrPaxIdAndPayments, userNotes, credentialsDTO,
				isAutoRefund, userNoteType);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		return response;
	}

	/**
	 * Compose the audit information
	 * 
	 * @param pnr
	 * @param pnrPaxIdAndPayments
	 * @param userNotes
	 * @param credentialsDTO
	 * @param isAutoRefund
	 *            TODO
	 * @param userNoteType TODO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationAudit> composeAudit(String pnr, Map<Integer, PaymentAssembler> pnrPaxIdAndPayments,
			String userNotes, CredentialsDTO credentialsDTO, boolean isAutoRefund, String userNoteType) throws ModuleException {
		// Get the passenger payment information
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
		String audit = ReservationCoreUtils.getPassengerPaymentInfo(pnrPaxIdAndPayments, reservation);

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.MADE_REFUND.getCode());
		reservationAudit.setUserNote(userNotes);
		reservationAudit.setUserNoteType(userNoteType);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.MadeRefund.PASSENGER_DETAILS, audit);

		String refundTypeStr = AuditTemplateEnum.TemplateParams.MadeRefund.REFUND_TYPE_MANUAL;

		if (isAutoRefund) {
			refundTypeStr = AuditTemplateEnum.TemplateParams.MadeRefund.REFUND_TYPE_AUTO;
		}

		// to identify pax refund is auto or manual
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.MadeRefund.REFUND_TYPE, refundTypeStr);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.MadeRefund.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.MadeRefund.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		colReservationAudit.add(reservationAudit);

		return colReservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param pnrPaxIdAndPayments
	 * @param userNotes
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, String userNotes,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || pnrPaxIdAndPayments == null || userNotes == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");

	}
}
