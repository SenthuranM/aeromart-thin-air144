/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.service.bd;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.service.RevenueAccountBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Lasantha Pambagoda
 * @isa.module.bd-impl id="revenueaccount.service.local" description="local impl of the RevenueAccountBD"
 */
public class RevenueAccountBDLocalImpl extends PlatformBaseServiceDelegate implements RevenueAccountBD {

	/**
	 * mehtod to record cancel
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce recordCancel(String pnrPaxId, BigDecimal creditTotal, BigDecimal chargeAmount,
			BigDecimal penaltyAmount, int actionNC, int chargeNC, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO,
			CredentialsDTO credentialsDTO, boolean enableTransactionGranularity) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECORD_CANCEL);

		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.CREDIT_TOTAL, creditTotal);
		command.setParameter(CommandParamNames.CHARGE_AMOUNT, chargeAmount);
		command.setParameter(CommandParamNames.PENALTY_AMOUNT, penaltyAmount);
		command.setParameter(CommandParamNames.ACTION_NC, new Integer(actionNC));
		command.setParameter(CommandParamNames.CHARGE_NC, new Integer(chargeNC));
		command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);

		return command.execute();
	}

	/**
	 * method to record modification
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce recordModification(String pnrPaxId, BigDecimal cancelTotal, BigDecimal addedTotal,
			BigDecimal chargeAmt, BigDecimal penaltyAmount, int actionNC, int chargeNC, Collection<ITnxPayment> colPayment,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO, String receiptNumber,
			Collection<CardDetailConfigDTO> cardConfigData, boolean enableTransactionGranularity, boolean isFirstPayment,
			boolean isActualPayment, Integer transactionSeq, Map<String, BigDecimal> lmsProductRedeemedAmount,
			List<TransactionSegment> transactionSegments) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECORD_MODIFY);

		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.CANCEL_TOTAL, cancelTotal);
		command.setParameter(CommandParamNames.ADDED_TOTAL, addedTotal);
		command.setParameter(CommandParamNames.CHARGE_AMOUNT, chargeAmt);
		command.setParameter(CommandParamNames.PENALTY_AMOUNT, penaltyAmount);
		command.setParameter(CommandParamNames.ACTION_NC, new Integer(actionNC));
		command.setParameter(CommandParamNames.CHARGE_NC, new Integer(chargeNC));
		command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.PAYMENT_LIST, colPayment);
		command.setParameter(CommandParamNames.RECIEPT_NUMBER, receiptNumber);
		command.setParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA, cardConfigData);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		command.setParameter(CommandParamNames.IS_FIRST_PAYMENT, isFirstPayment);
		command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, isActualPayment);
		command.setParameter(CommandParamNames.TRANSACTION_SEQ, transactionSeq);
		command.setParameter(CommandParamNames.LOYALTY_PRODUCT_REDEEMED_AMOUNTS, lmsProductRedeemedAmount);
		command.setParameter(CommandParamNames.TRNX_SEGMENTS, transactionSegments);
		return command.execute();
	}

	/**
	 * mehtod to record refund
	 * @param preferredRefundOrder
	 *            Order the refund should deduct charges. (TAX,SUR etc.)
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce recordRefund(String pnrPaxId, BigDecimal refundAmount, Collection<ITnxPayment> payments,
			String userNotes, CredentialsDTO credentialsDTO, boolean enableTransactionGranularity,
			Collection<String> prefferedRefundOrder, Collection<Long> pnrPaxOndChgIds, Boolean isManualRefund, String userNoteType)
			throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECORD_REFUND);

		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.AMOUNT, refundAmount);
		command.setParameter(CommandParamNames.PAYMENT_LIST, payments);
		command.setParameter(CommandParamNames.USER_NOTES, userNotes);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		command.setParameter(CommandParamNames.PREFERRED_REFUND_ORDER, prefferedRefundOrder);
		command.setParameter(CommandParamNames.PNR_PAX_OND_CHARGE_IDS, pnrPaxOndChgIds);
		command.setParameter(CommandParamNames.IS_MANUAL_REFUND, isManualRefund);
		command.setParameter(CommandParamNames.USER_NOTE_TYPE, userNoteType);

		return command.execute();
	}

	/**
	 * method to record credit adjust
	 * 
	 * Nili August 9, 2010, You might be thinking why this method is not exposed in the interface. This is mainly
	 * because the revenue account is re-structured linking payment with it's charges. This operation does not used any
	 * more thus in the future iterations if this is needed, the respective payment with charges mapping should be done
	 * accordingly. Until then please don't expose this method as an interface method.
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce recordDebitAccount(String pnrPaxId, BigDecimal amount, int actionNC, CredentialsDTO credentialsDTO)
			throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.DEBIT_ACCOUT);

		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.AMOUNT, amount);
		command.setParameter(CommandParamNames.ACTION_NC, new Integer(actionNC));
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);

		return command.execute();
	}

	/**
	 * method to record credit adjust
	 * 
	 * Nili August 9, 2010, You might be thinking why this method is not exposed in the interface. This is mainly
	 * because the revenue account is re-structured linking payment with it's charges. This operation does not used any
	 * more thus in the future iterations if this is needed, the respective payment with charges mapping should be done
	 * accordingly. Until then please don't expose this method as an interface method.
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce recordCreditAccount(String pnrPaxId, BigDecimal amount, int actionNC, CredentialsDTO credentialsDTO)
			throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.CREDIT_ACCOUNT);

		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.AMOUNT, amount);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		return command.execute();
	}

	/**
	 * method to record payment
	 * 
	 * Nili August 9, 2010, You might be thinking why this method is not exposed in the interface. This is mainly
	 * because the revenue account is re-structured linking payment with it's charges. This operation does not used any
	 * more thus in the future iterations if this is needed, the respective payment with charges mapping should be done
	 * accordingly. Until then please don't expose this method as an interface method.
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce recordPayment(String pnrPaxId, Collection<ITnxPayment> payments, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECORD_PAYMENT);

		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.PAYMENT_LIST, payments);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		return command.execute();
	}

	/**
	 * method to record ticket purchase
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce recordPurchase(String pnrPaxId, BigDecimal segmentTotal, BigDecimal totalPaymentAmount,
			Collection<ITnxPayment> payments, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO,
			CredentialsDTO credentialsDTO, String recieptNumber, Collection<CardDetailConfigDTO> cardConfigData,
			boolean enableTransactionGranularity, boolean isGoShowProcess, boolean isActualPayment, boolean isFirstPayment,
			Integer transactionSeq, Map<String, BigDecimal> lmsProductRedeemedAmount,
			List<TransactionSegment> transactionSegments) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.TICKET_PURCHASE);

		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		command.setParameter(CommandParamNames.SEGMENT_TOTAL, segmentTotal);
		command.setParameter(CommandParamNames.TOTAL_PAYMENT_AMOUNT, totalPaymentAmount);
		command.setParameter(CommandParamNames.PAYMENT_LIST, payments);
		command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		// FIXME - Temp fix for having single timestamp for pax payment txns when there are multiple pax
		command.setParameter(CommandParamNames.CURRENT_TIMESTAMP, CalendarUtil.getCurrentSystemTimeInZulu());
		command.setParameter(CommandParamNames.RECIEPT_NUMBER, recieptNumber);
		command.setParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA, cardConfigData);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, isGoShowProcess);
		command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, isActualPayment);
		command.setParameter(CommandParamNames.IS_FIRST_PAYMENT, isFirstPayment);
		command.setParameter(CommandParamNames.TRANSACTION_SEQ, transactionSeq);
		command.setParameter(CommandParamNames.LOYALTY_PRODUCT_REDEEMED_AMOUNTS, lmsProductRedeemedAmount);
		command.setParameter(CommandParamNames.TRNX_SEGMENTS, transactionSegments);
		return command.execute();
	}

	/**
	 * method to get the PNR Pax Balance
	 */
	@Override
	public BigDecimal getPNRPaxBalance(String pnrPaxId) {
		ReservationTnxDAO reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		return reservationTnxDao.getPNRPaxBalance(pnrPaxId);
	}

	@Override
	public ServiceResponce recordTransferInfant(String fromPnrPaxId, String toPnrPaxId, BigDecimal amount,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity) throws ModuleException {
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.TRANSFER_INFANT);

		command.setParameter(CommandParamNames.PNR_PAX_ID, fromPnrPaxId);
		command.setParameter(CommandParamNames.PNR_PAX_ID_TO, toPnrPaxId);
		command.setParameter(CommandParamNames.AMOUNT, amount);
		command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		return command.execute();
	}

	/**
	 * Return Pnr passenger balances
	 * 
	 * @param pnrPaxIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public Map<Integer, BigDecimal> getPNRPaxBalances(Collection<Integer> pnrPaxIds) throws ModuleException {
		ReservationTnxDAO reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		return reservationTnxDao.getPNRPaxBalances(pnrPaxIds);
	}

	@Override
	public ServiceResponce extendCredits(long creditId, Date expiryDate, String note, String pnr, int pnrPaxId,
			CredentialsDTO credentialsDTO) throws ModuleException {
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.EXTEND_CREDITS_MACRO);

		command.setParameter(CommandParamNames.PNR, pnr);
		command.setParameter(CommandParamNames.PNR_PAX_ID, Integer.toString(pnrPaxId));
		command.setParameter(CommandParamNames.CREDIT_ID, creditId);
		command.setParameter(CommandParamNames.EXPIRY_DATE, expiryDate);
		command.setParameter(CommandParamNames.NOTE, note);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		return command.execute();
	}

	@Override
	public ServiceResponce reinstateCredit(String pnr, int pnrPaxId, int paxTxnId, BigDecimal amount, Date expDate,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO, String note,
			boolean enableTransactionGranularity) throws ModuleException {
		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.REINSTATE_CREDIT_MACRO);

		command.setParameter(CommandParamNames.PNR, pnr);
		command.setParameter(CommandParamNames.PNR_PAX_ID, Integer.toString(pnrPaxId));
		command.setParameter(CommandParamNames.MODIFY_AMOUNT, amount);
		command.setParameter(CommandParamNames.ACTION_NC, ReservationTnxNominalCode.CREDIT_ACQUIRE);
		command.setParameter(CommandParamNames.PAX_TXN_ID, paxTxnId);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.EXPIRY_DATE, expDate);
		command.setParameter(CommandParamNames.NOTE, note);
		command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		return command.execute();
	}

	@Override
	public ServiceResponce adjustCredits(String pnr, String pnrPaxId, BigDecimal amount, String userNotes,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity, Integer transactionSequence) throws ModuleException {

		Command command = (Command) ReservationModuleUtils.getBean(CommandNames.ADJUST_CREDIT_MACRO);

		command.setParameter(CommandParamNames.PNR, pnr);
		command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId.toString());
		command.setParameter(CommandParamNames.MODIFY_AMOUNT, amount);
		command.setParameter(CommandParamNames.USER_NOTES, userNotes);
		command.setParameter(CommandParamNames.ACTION_NC, ReservationTnxNominalCode.CREDIT_ADJUST);
		command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
		command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
		command.setParameter(CommandParamNames.TRANSACTION_SEQ, transactionSequence);

		return command.execute();
	}

	@Override
	public Integer getPNRPaxMaxTransactionSeq(Integer pnrPaxId) {
		ReservationPaymentDAO reservationPaymentDao = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		return reservationPaymentDao.getPNRPaxMaxPaymentTnxSequence(pnrPaxId);
	}

	@Override
	public ServiceResponce recordHandlingFeeOnCancellation(String pnrPaxId, BigDecimal amount, CredentialsDTO credentialsDTO) throws ModuleException {
		return recordDebitAccount(pnrPaxId, amount, ReservationTnxNominalCode.CREDIT_ADJUST.getCode(), credentialsDTO);
	}

}
