/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.TnxPaymentFactory;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityRules;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.UniqueIDGenerator;
import com.isa.thinair.lccclient.api.service.LCCPaxCreditBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * commenad to carry forward credit
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="carryForwardCredit"
 */
public class CarryForwardCredit extends DefaultBaseCommand {

	// Dao's
	private ReservationCreditDAO reservationCreditDao;
	private ReservationTnxDAO reservationTnxDao;
	private final double ERROR_MARGIN = 0.03;
	// Bd's
	LCCPaxCreditBD lccPaxCreditBD = null;

	/**
	 * constructor of the CarryForwardCredit command
	 */
	public CarryForwardCredit() {

		// looking up daos
		reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		lccPaxCreditBD = ReservationModuleUtils.getLCCPaxCreditBD();
	}

	/**
	 * execute method of the CarryForwardCredit command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = new HashMap<Integer, Collection<TnxCreditPayment>>();
		Collection<ReservationAudit> reservationAudit = new ArrayList<ReservationAudit>();
		// getting command params
		PaxCreditDTO paxCreditDTO = (PaxCreditDTO) this.getParameter(CommandParamNames.PAX_CREDIT_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		boolean enableTransactionGranularity = (Boolean) this.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);

		// checking params
		this.checkParams(paxCreditDTO, credentialsDTO);
		carryForwardCredit(paxCreditDTO, true, credentialsDTO, paxCreditPayments, reservationAudit, enableTransactionGranularity);

		// constructing and return command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.PAX_CREDIT_PAYMENTS, paxCreditPayments);
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, reservationAudit);

		return response;
	}

	private void carryForwardCredit(PaxCreditDTO paxCreditDTO, boolean isOtherCarrierRequestedCF, CredentialsDTO credentialsDTO,
			Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments, Collection<ReservationAudit> reservationAudit,
			boolean enableTransactionGranularity) throws ModuleException {
		String pnrPaxIdFrom = paxCreditDTO.getDebitPaxId();
		if (pnrPaxIdFrom == null) {
			// utilizing credit from pax credit search should not have a null pnrPaxId.
			// but for LCC credit shuffling we do not have carrier wise pnrPaxIds, but we need to
			// derive it from paxSequence and pnr
			pnrPaxIdFrom = ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPnrPaxId(paxCreditDTO.getPnr(),
					paxCreditDTO.getPaxSequence()).toString();
			paxCreditDTO.setDebitPaxId(pnrPaxIdFrom);
		}
		BigDecimal amount = paxCreditDTO.getBalance();
		Collection<TnxCreditPayment> creditPayments = new ArrayList<TnxCreditPayment>();

		TnxGranularityFactory.saveReservationPaxTnxBreakdownForCarryForwardCredit(pnrPaxIdFrom, amount,
				ReservationTnxNominalCode.CREDIT_CF, enableTransactionGranularity);

		BigDecimal balance = reservationTnxDao.getPNRPaxBalance(pnrPaxIdFrom);
		if (balance.doubleValue() < 0) {

			if ((AccelAeroCalculator.add(balance, amount).doubleValue() <= 0 && !isOtherCarrierRequestedCF)
					|| (isOtherCarrierRequestedCF && AccelAeroCalculator.add(balance, amount).doubleValue() <= ERROR_MARGIN)) {

				// Pax credit list for selected passenger.
				List<ReservationCredit> creditList = new ArrayList<ReservationCredit>();
				creditList.addAll(TnxGranularityRules.getCreditsCarryForwardPolicies(pnrPaxIdFrom));

				for (ReservationCredit reservationCredit : creditList) {
					if (0 < reservationCredit.getBalance().doubleValue() && amount.doubleValue() > 0) {
						BigDecimal carryForwardAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						String lccUniqueTnxId = UniqueIDGenerator.generate();
						if (amount.doubleValue() < reservationCredit.getBalance().doubleValue()) {
							reservationCredit.setBalance(AccelAeroCalculator.subtract(reservationCredit.getBalance(), amount));
							carryForwardAmount = amount;
							amount = AccelAeroCalculator.getDefaultBigDecimalZero();
						} else {
							carryForwardAmount = reservationCredit.getBalance();
							amount = AccelAeroCalculator.subtract(amount, reservationCredit.getBalance());
							reservationCredit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
						}

						// Credit wise carry forward record
						ReservationTnx debitTnx = TnxFactory.getDebitInstance(pnrPaxIdFrom, carryForwardAmount,
								ReservationTnxNominalCode.CREDIT_CF.getCode(), credentialsDTO,
								CalendarUtil.getCurrentSystemTimeInZulu(), AppSysParamsUtil.getDefaultCarrierCode(),
								lccUniqueTnxId, false);
						// Set base currency
						debitTnx.setPayCurrencyAmount(carryForwardAmount);
						debitTnx.setPayCurrencyCode(AppSysParamsUtil.getBaseCurrency());
						reservationTnxDao.saveTransaction(debitTnx);
						
						// Credit Carry Forward order
						Collection<String> colChargeGroupCodes = TnxGranularityRules
								.getCreditCarryForwardChargeGroupFulFillmentOrder();

						// adjust the payment charges collection in ReservationCredit
						TnxGranularityUtils.validateAndAdjustWithReservationCredit(reservationCredit, colChargeGroupCodes);						

						// Have to save credit object before BF record as we need primary key for credit reversal.
						reservationCreditDao.saveReservationCredit(reservationCredit);

						// calculating the pay currency amount for the credit_bf
						BigDecimal paymentAmmount = AccelAeroRounderPolicy.convertAndRound(carryForwardAmount, paxCreditDTO
								.getPayCurrencyDTO().getPayCurrMultiplyingExchangeRate(), paxCreditDTO.getPayCurrencyDTO()
								.getBoundaryValue(), paxCreditDTO.getPayCurrencyDTO().getBreakPointValue());

						PayCurrencyDTO payDto = (PayCurrencyDTO) paxCreditDTO.getPayCurrencyDTO().clone();
						payDto.setTotalPayCurrencyAmount(paymentAmmount);

						// Credit BF record.

						TnxCreditPayment payment = (TnxCreditPayment) TnxPaymentFactory.getCreditPaymentInstance(pnrPaxIdFrom,
								paxCreditDTO.getPnr(), credentialsDTO.getAgentCode(), carryForwardAmount, payDto,
								AppSysParamsUtil.getDefaultCarrierCode(), lccUniqueTnxId, null);
						payment.setPaxCreditId(String.valueOf(reservationCredit.getCreditId()));
						payment.setExpiryDate(reservationCredit.getExpiryDate());

						creditPayments.add(payment);
					}
				}
				if ((0 < amount.doubleValue() && !isOtherCarrierRequestedCF)
						|| (isOtherCarrierRequestedCF && ERROR_MARGIN < amount.doubleValue())) {
					throw new ModuleException("revac.invalid.amount");
				}
			} else {
				throw new ModuleException("revac.paxbalance.tooless.amount");
			}
			paxCreditPayments.put(paxCreditDTO.getPaxSequence(), creditPayments);
		} else {
			throw new ModuleException("revac.paxbalance.invalid");
		}

		// construct audit
		reservationAudit.addAll(ReservationCoreUtils.composeAuditForCarryForwardCredit(paxCreditDTO));

	}

	/**
	 * private method to validate parameters
	 * 
	 * @param carrier
	 *            carrier the credit is belongs to
	 * @param credentialsDTO
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(PaxCreditDTO paxCreditDTO, CredentialsDTO credentialsDTO) throws ModuleException {
		if ((paxCreditDTO.getDebitPaxId() == null && (paxCreditDTO.getPaxSequence() == null || paxCreditDTO.getPnr() == null))
		// we can get the pax debit id from pax sequence and pnr if it is null
				|| paxCreditDTO.getPayCarrier() == null || paxCreditDTO.getBalance() == null || credentialsDTO == null) // throw
																														// exception

			throw new ModuleException("airreservations.arg.invalid.null");
	}
}
