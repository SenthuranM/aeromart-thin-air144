package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.core.bl.segment.FlownAssitUnit;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CancellationBalanceCalculator extends BaseBalanceCalculator implements IBalanceCalculator {
	private Map<Integer, PnrChargesDTO> paxModifyingPnrChargeMap;
	private Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseCurrCharges;
	private Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseExcludedCharges;
	private Reservation reservation;
	private ONDFareChargeProcessor ondFareChargeProcessor;
	private ExternalChargeProcessor extChargeProcessor;
	private boolean applyModChargeAsCnxCharge = false;

	public CancellationBalanceCalculator(Reservation reservation, Date quotedDate, Collection<PnrChargesDTO> modifyingPnrCharges,
			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseCurrCharges, Collection<OndFareDTO> ondFares,
			FlownAssitUnit flownAssitUnit, Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Map<Integer, Collection<ChargeMetaTO>> mapPnrPaxIdWiseExcludedCharges, boolean applyModChargeAsCnxCharge,
			Map<Integer, List<ExternalChgDTO>> paxEffectiveTax) {
		this.ondFareChargeProcessor = new ONDFareChargeProcessor(ondFares, quotedDate, flownAssitUnit, null, null);
		this.paxModifyingPnrChargeMap = AirProxyReservationUtil.indexOndChargesByPax(modifyingPnrCharges);
		this.mapPnrPaxIdWiseCurrCharges = mapPnrPaxIdWiseCurrCharges;
		this.mapPnrPaxIdWiseExcludedCharges = mapPnrPaxIdWiseExcludedCharges;
		this.extChargeProcessor = new ExternalChargeProcessor(quotedDate, reprotectedExternalCharges);
		this.reservation = reservation;
		this.showCnxCharge = true;
		this.applyModChargeAsCnxCharge = applyModChargeAsCnxCharge;
		this.paxEffectiveTax = paxEffectiveTax;
		this.infantPaymentSeparated = reservation.isInfantPaymentRecordedWithInfant();
	}

	@Override
	public void calculate() {
		for (ReservationPax passenger : getReservationPassengers()) {
			if (!passenger.getPaxType().equals(PaxTypeTO.INFANT) || infantPaymentSeparated) {
				SegmentSummaryTOV2 balanceSummary = getSegmentSummary(passenger.getPnrPaxId());
				balanceSummary.addNewMetaCharges(ondFareChargeProcessor.getChargesPerPax(passenger.getPaxType()));
				if (!infantPaymentSeparated && passenger.hasInfant()) {
					balanceSummary.addNewMetaCharges(ondFareChargeProcessor.getChargesPerPax(PaxTypeTO.INFANT));
				}

				// add balance due adjustments
				balanceSummary.addNewMetaCharges(ondFareChargeProcessor.getPaxDueAdjustmentCharges(passenger.getPnrPaxId(),
						passenger.getPaxType()));

				balanceSummary.addNewMetaCharges(extChargeProcessor.getPaxExternalCharges(passenger.getPnrPaxId()));
				if (!infantPaymentSeparated && passenger.hasInfant()) {
					balanceSummary.addNewMetaCharges(extChargeProcessor.getPaxExternalCharges(passenger.getAccompaniedPaxId()));
				}

				PnrChargesDTO modifyingCharges = paxModifyingPnrChargeMap.get(passenger.getPnrPaxId());
				mapPnrPaxIdWiseCurrCharges.get(passenger.getPnrPaxId()).addAll(modifyingCharges.getExtraFeeChargeMetaAmounts());

				balanceSummary.addCurrentMetaCharges(mapPnrPaxIdWiseCurrCharges.get(passenger.getPnrPaxId()));
				balanceSummary.addCurrentNonRefundableMetaCharges(modifyingCharges.getNonRefundableChargeMetaAmounts());
				balanceSummary.addCurrentRefundableMetaCharges(modifyingCharges.getRefundableChargeMetaAmounts());

				if (applyModChargeAsCnxCharge) {
					this.showCnxCharge = false;
					this.showModCharge = true;
					balanceSummary.setIdentifiedTotalModCharge(modifyingCharges.getIdentifiedModificationAmount());
				} else {
					balanceSummary.setIdentifiedTotalCnxCharge(modifyingCharges.getIdentifiedCancellationAmount());
				}
				if (ondFareChargeProcessor.hasPenaltyChargesPerPax(passenger.getPaxType())) {
					balanceSummary.setModificationPenalty(ondFareChargeProcessor.getPenaltyChargesPerPax(passenger.getPaxType()));
				} else if (modifyingCharges.getModificationPenalty().compareTo(BigDecimal.ZERO) > 0) {
					balanceSummary.addNewMetaCharges(modifyingCharges.getServiceTaxPenaltyCharges());
					balanceSummary.setModificationPenalty(modifyingCharges.getModificationPenalty());
				}

				if (!infantPaymentSeparated && passenger.hasInfant() && ondFareChargeProcessor.hasPenaltyChargesPerPax(PaxTypeTO.INFANT)) {
					balanceSummary.setModificationPenalty(AccelAeroCalculator.add(balanceSummary.getModificationPenatly(),
							ondFareChargeProcessor.getPenaltyChargesPerPax(PaxTypeTO.INFANT)));
				}

				if (mapPnrPaxIdWiseExcludedCharges != null && !mapPnrPaxIdWiseExcludedCharges.isEmpty()) {
					balanceSummary.setExcludedSegCharge(mapPnrPaxIdWiseExcludedCharges.get(passenger.getPnrPaxId()));
				}

				balanceSummary.setIdentifiedExtraFeeAmount(modifyingCharges.getIdentifiedExtraFeeAmount());
			}
		}
	}

	@Override
	protected Set<ReservationPax> getReservationPassengers() {
		return reservation.getPassengers();
	}

	@Override
	protected Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseCurrentCharges() {
		return mapPnrPaxIdWiseCurrCharges;
	}

	protected Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseExcludedCharges() {
		return mapPnrPaxIdWiseExcludedCharges;
	}

}
