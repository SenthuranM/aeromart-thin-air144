package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.factory;

import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.ResChangesDetectorConstants;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.SSRModifyIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.BookingClassModifyIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.CabinClassModifyIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.CancelSegmentIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.ConfirmOnholdReservationIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.CreateReservationIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.ModifySegmentIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.NameChangeIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.RemovePaxIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.cancelReservationIdentificationRule;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;

public class ResModifyIdentificationRuleFactory {

	boolean isModifyFlow;

	public ResModifyIdentificationRuleFactory(ResModifyDtectorDataContext dataContext) {

		if (dataContext.getExisitingReservation() != null) {
			isModifyFlow = true;
		}

	}

	public ResModifyIdentificationRule<RuleExecuterDatacontext> getResModifyIdentificationRule(String ruleRef) {
		ResModifyIdentificationRule<RuleExecuterDatacontext> rule = null;

		if (ResChangesDetectorConstants.RuleRef.CREATE_CNF_RESERVATION_RULE.equals(ruleRef) && !isModifyFlow) {

			rule = new CreateReservationIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.CONFIRM_OHD_RESERVATION_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new ConfirmOnholdReservationIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.SEG_MODIFY_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new ModifySegmentIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.SEG_CNX_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new CancelSegmentIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.RES_CNX_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new cancelReservationIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.NAME_CHANGE_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new NameChangeIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.SSR_UPDATE_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new SSRModifyIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.REMOVE_PAX_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new RemovePaxIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.BOOKING_CLASS_MOD_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new BookingClassModifyIdentificationRule(ruleRef);

		} else if (ResChangesDetectorConstants.RuleRef.CABIN_CLASS_MOD_RULE.equals(ruleRef) && isModifyFlow) {

			rule = new CabinClassModifyIdentificationRule(ruleRef);

		}

		return rule;
	}
}