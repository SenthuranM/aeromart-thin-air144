package com.isa.thinair.airreservation.core.bl.tax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.SimplifiedChargeDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedFlightSegmentDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedPaxDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.FareConvertUtil;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.pnr.IExtraFeeCalculator;
import com.isa.thinair.airreservation.core.bl.segment.PaxFareUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class EffectiveServiceTaxCalculator implements IExtraFeeCalculator {

	private Reservation reservation;
	private Collection<OndFareDTO> ondFareDTOs;
	private Map<Integer, Collection<ExternalChgDTO>> externalCharges;
	private Map<Integer, Collection<? extends ExternalChgDTO>> paxExtraFee;
	private boolean ticketingRevenue = false;
	private Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap;
	private Set<String> serviceChargeCodes = null;
	private Collection<Integer> flownPnrSegIds;
	private Collection<Integer> flownFltSegIds;
	private Boolean isUserModification;

	private EffectiveServiceTaxCalculator(Reservation reservation, Collection<OndFareDTO> colOndFareDTO,
			Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExtChgMap, Collection<Integer> flownPnrSegIds,
			boolean ticketingRevenue) throws ModuleException {
		this.reservation = reservation;
		this.ondFareDTOs = colOndFareDTO;

		this.externalCharges = new HashMap<>();

		Map<Integer, Integer> paxIdSeqMap = new HashMap<>();

		for (ReservationPax pax : reservation.getPassengers()) {
			paxIdSeqMap.put(pax.getPnrPaxId(), pax.getPaxSequence());
		}

		if (reprotectedExternalCharges != null) {
			for (Integer pnrPaxId : reprotectedExternalCharges.keySet()) {
				if (!externalCharges.containsKey(pnrPaxId)) {
					externalCharges.put(paxIdSeqMap.get(pnrPaxId), new ArrayList<>());
				}
				externalCharges.get(paxIdSeqMap.get(pnrPaxId)).addAll(reprotectedExternalCharges.get(pnrPaxId));
			}
		}
		if (paxSeqWiseExtChgMap != null) {
			for (Integer paxSequence : paxSeqWiseExtChgMap.keySet()) {
				if (!externalCharges.containsKey(paxSequence)) {
					externalCharges.put(paxSequence, new ArrayList<>());
				}
				externalCharges.get(paxSequence).addAll(adaptToExternalChgDTOs(paxSeqWiseExtChgMap.get(paxSequence)));
			}
		}
		this.ticketingRevenue = ticketingRevenue;

		Collection<Charge> charges = TaxUtil.getServiceTaxes();
		serviceChargeCodes = new HashSet<>();
		for (Charge chg : charges) {
			serviceChargeCodes.add(chg.getChargeCode());
		}

		this.flownPnrSegIds = flownPnrSegIds;

		this.flownFltSegIds = new HashSet<>();
		for (ReservationSegment resSeg : reservation.getSegments()) {
			if (isFlownPnrSegment(resSeg.getPnrSegId())) {
				flownFltSegIds.add(resSeg.getFlightSegId());
			}
		}
	}

	private boolean isFlownPnrSegment(Integer pnrSegId) {
		if (flownPnrSegIds != null && flownPnrSegIds.contains(pnrSegId)) {
			return true;
		}
		return false;
	}

	private boolean isFlownFltSegment(String fltRefNo) {
		if (flownFltSegIds != null && flownFltSegIds.contains(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo))) {
			return true;
		}
		return false;
	}

	private Collection<? extends ExternalChgDTO> adaptToExternalChgDTOs(List<LCCClientExternalChgDTO> lccChargeList) {
		Collection<ExternalChgDTO> extChgDTOList = new ArrayList<>();

		if (lccChargeList != null) {
			for (LCCClientExternalChgDTO lccExtChg : lccChargeList) {
				if (!lccExtChg.isServiceTaxAppliedToCCFee()){
					ExternalChgDTO extChg = new ExternalChgDTO();
					extChg.setApplicableChargeRate(lccExtChg.getJourneyType());
					extChg.setAmountConsumedForPayment(lccExtChg.isAmountConsumedForPayment());
					extChg.setAmount(lccExtChg.getAmount());
					extChg.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(lccExtChg.getFlightRefNumber()));
					extChgDTOList.add(extChg);
				}
			}
		}

		return extChgDTOList;
	}

	public static IExtraFeeCalculator getCalculatorForTicketingRevenue(Reservation reservation,
			Collection<OndFareDTO> colOndFareDTO, Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExtChgMap, Collection<Integer> flownPnrSegIds)
			throws ModuleException {
		if (colOndFareDTO != null) {
			return new EffectiveServiceTaxCalculator(reservation, colOndFareDTO, reprotectedExternalCharges, paxSeqWiseExtChgMap,
					flownPnrSegIds, true);
		} else {
			return new NullExtraFeeCalculator();
		}
	}

	private Set<ServiceTaxExtChgDTO> getServiceTaxForPax(OperationChgTnx effectiveChg,
			ServiceTaxQuoteForTicketingRevenueRS serviceTaxForTicketing, String paxType, Integer paxSeqId)
			throws ModuleException {

		Set<String> fltRefNos = new HashSet<>();
		if (serviceTaxForTicketing.getPaxTypeWiseServiceTaxes() != null) {
			for (ServiceTaxDTO tax : serviceTaxForTicketing.getPaxTypeWiseServiceTaxes().get(paxType)) {
				if (isValidServiceTaxEntry(tax)) {
					fltRefNos.add(tax.getFlightRefNumber());
				}
			}
		}
		if (serviceTaxForTicketing.getPaxWiseServiceTaxes() != null
				&& serviceTaxForTicketing.getPaxWiseServiceTaxes().get(paxSeqId) != null) {
			for (ServiceTaxDTO tax : serviceTaxForTicketing.getPaxWiseServiceTaxes().get(paxSeqId)) {
				if (isValidServiceTaxEntry(tax)) {
					fltRefNos.add(tax.getFlightRefNumber());
				}
			}
		}

		// TODO revise the split strategy to something more appropriate
		Set<ServiceTaxExtChgDTO> serviceTaxes = new HashSet<>();
		for (ReservationPaxOndCharge charge : effectiveChg.getChargeKeyWiseSummarizedCharges().values()) {
			BigDecimal chargeAmount = AccelAeroCalculator.getNullSafeValue(charge.getAmount(), BigDecimal.ZERO);
			BigDecimal taxableAmount = AccelAeroCalculator.getNullSafeValue(charge.getTaxableAmount(), BigDecimal.ZERO);
			BigDecimal nonTaxableAmount = AccelAeroCalculator.getNullSafeValue(charge.getNonTaxableAmount(), BigDecimal.ZERO);

			BigDecimal[] chgAmtList = new BigDecimal[0];
			BigDecimal[] taxableAmtList = new BigDecimal[0];
			BigDecimal[] nonTaxableAmtList = new BigDecimal[0];

			if (fltRefNos.size() > 0) {
				chgAmtList = AccelAeroCalculator.roundAndSplit(chargeAmount, fltRefNos.size());
				taxableAmtList = AccelAeroCalculator.roundAndSplit(taxableAmount, fltRefNos.size());
				nonTaxableAmtList = AccelAeroCalculator.roundAndSplit(nonTaxableAmount, fltRefNos.size());
			}

			int index = 0;
			for (String fltRefNo : fltRefNos) {
				ServiceTaxExtChgDTO serviceTax = getServiceTax(charge.getChargeCode());
				serviceTax.setAmount(chgAmtList[index]);
				serviceTax.setTaxableAmount(taxableAmtList[index]);
				serviceTax.setNonTaxableAmount(nonTaxableAmtList[index]);
				serviceTax.setChargeCode(charge.getChargeCode());
				serviceTax.setTicketingRevenue(ticketingRevenue);
				serviceTax.setFlightRefNumber(fltRefNo);
				serviceTax.setFlightSegId(FlightRefNumberUtil.getPnrSegIdFromPnrSegRPH(fltRefNo));
				serviceTax.setTaxDepositCountryCode(serviceTaxForTicketing.getServiceTaxDepositeCountryCode());
				serviceTax.setTaxDepositStateCode(serviceTaxForTicketing.getServiceTaxDepositeStateCode());
				serviceTaxes.add(serviceTax);
				index++;
			}
		}

		return serviceTaxes;
	}

	private ServiceTaxExtChgDTO getServiceTax(String chargeCode) throws ModuleException {

		ServiceTaxExtChgDTO externalChg = (ServiceTaxExtChgDTO) ((ServiceTaxExtCharges) getExternalChargesMap()
				.get(EXTERNAL_CHARGES.SERVICE_TAX)).getRespectiveServiceChgDTO(chargeCode);
		if (externalChg != null) {
			externalChg = (ServiceTaxExtChgDTO) externalChg.clone();
		}
		return externalChg;
	}

	private Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap() throws ModuleException {
		if (this.extExternalChgDTOMap == null) {
			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SERVICE_TAX);

			this.extExternalChgDTOMap = ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
					ChargeRateOperationType.MODIFY_ONLY);
		}

		return extExternalChgDTOMap;

	}

	private boolean isServiceTax(ReservationPaxOndCharge ondCharge) {
		return ondCharge.getChargeCode() != null && serviceChargeCodes.contains(ondCharge.getChargeCode());
	}

	private Map<Integer, Collection<ReservationPaxOndCharge>>
			paxWiseNewPNRCharges(ServiceTaxQuoteForTicketingRevenueRS serviceTaxForTicketing) {
		Map<Integer, Collection<ReservationPaxOndCharge>> paxWiseCharges = new HashMap<>();
		for (ReservationPax pax : reservation.getPassengers()) {
			if (!paxWiseCharges.containsKey(pax.getPaxSequence())) {
				paxWiseCharges.put(pax.getPaxSequence(), new ArrayList<>());
			}
			List<ServiceTaxDTO> allTaxes = new ArrayList<>();
			if (serviceTaxForTicketing.getPaxTypeWiseServiceTaxes() != null
					&& serviceTaxForTicketing.getPaxTypeWiseServiceTaxes().get(pax.getPaxType()) != null) {
				allTaxes.addAll(serviceTaxForTicketing.getPaxTypeWiseServiceTaxes().get(pax.getPaxType()));
			}
			if (serviceTaxForTicketing.getPaxWiseServiceTaxes() != null
					&& serviceTaxForTicketing.getPaxWiseServiceTaxes().get(pax.getPaxSequence()) != null) {
				allTaxes.addAll(serviceTaxForTicketing.getPaxWiseServiceTaxes().get(pax.getPaxSequence()));
			}
			for (ServiceTaxDTO serviceTax : allTaxes) {
				if (!isFlownFltSegment(serviceTax.getFlightRefNumber())) {
					ReservationPaxOndCharge ondCharge = new ReservationPaxOndCharge();
					ondCharge.setAmount(serviceTax.getAmount());
					ondCharge.setChargeRateId(null);
					ondCharge.setChargeCode(serviceTax.getChargeCode());
					ondCharge.setChargeGroupCode(serviceTax.getChargeGroupCode());
					ondCharge.setFareId(null);
					ondCharge.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.TAX);
					ondCharge.setZuluChargeDate(CalendarUtil.getCurrentSystemTimeInZulu());
					ondCharge.setDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());

					ondCharge.setAdjustment(AccelAeroCalculator.getDefaultBigDecimalZero());
					Integer discValuePer = 0;
					ondCharge.setDiscountValuePercentage(discValuePer);

					// ondCharge.setAgentCode(credentialsDTO.getAgentCode());
					// ondCharge.setUserId(credentialsDTO.getUserId());
					ondCharge.setRefundableOperation("N");

					ondCharge.setNonTaxableAmount(serviceTax.getNonTaxableAmount());
					ondCharge.setTaxableAmount(serviceTax.getTaxableAmount());
					ondCharge.setTaxDepositStateCode(serviceTaxForTicketing.getServiceTaxDepositeStateCode());
					ondCharge.setTaxDepositCountryCode(serviceTaxForTicketing.getServiceTaxDepositeCountryCode());
					ondCharge.setTaxAppliedCategory(ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY);
					paxWiseCharges.get(pax.getPaxSequence()).add(ondCharge);
				}
			}
		}

		return paxWiseCharges;
	}

	private Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>>
			getPaxWiseNonRefundables(Map<Integer, SimplifiedFlightSegmentDTO> fltSegIdSegments) throws ModuleException {
		Collection<Integer> chgRateIds = new HashSet<>();
		Collection<Integer> fareIds = new HashSet<>();
		PaxFareUtil.getFareIdsAndChargeIds(reservation, fareIds, chgRateIds);

		FaresAndChargesTO faresAndChargesTO = ReservationModuleUtils.getFareBD().getRefundableStatuses(fareIds, chgRateIds, null);
		Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = new HashMap<>();

		Map<Integer, ReservationSegmentDTO> segMap = new HashMap<>();
		for (ReservationSegmentDTO resSeg : reservation.getSegmentsView()) {
			segMap.put(resSeg.getPnrSegId(), resSeg);
		}
		boolean inOnhold = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus());
		Collection<String> exclusionChargeCodes = CancellationUtils.getExcludeChargeCodes(isUserModification);

		Map<String, SimplifiedFlightSegmentDTO> fltSegCodeMap = new HashMap<>();
		SimplifiedFlightSegmentDTO firstSeg = null;
		for (SimplifiedFlightSegmentDTO simpleFltSegDTO : fltSegIdSegments.values()) {
			fltSegCodeMap.put(simpleFltSegDTO.getSegmentCode(), simpleFltSegDTO);
			if (firstSeg == null
					|| firstSeg.getDepartureDateTimeZulu().compareTo(simpleFltSegDTO.getDepartureDateTimeZulu()) > 0) {

				firstSeg = simpleFltSegDTO;
			}
		}
		for (ReservationPax pax : reservation.getPassengers()) {
			List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
			for (ReservationPaxFare fare : pax.getPnrPaxFares()) {
				ReservationPaxFareSegment firstFareSeg = fare.getPaxFareSegments().iterator().next();
				for (ReservationPaxOndCharge ondCharge : fare.getCharges()) {
					if (ReservationPaxOndCharge.TAX_APPLIED_FOR_OTHER_CATEGORY.equals(ondCharge.getTaxAppliedCategory())) {
						boolean addCharge = false;
						String chargeCode = null;
						if (ondCharge.getChargeRateId() != null) {
							ChargeTO chargeTO = faresAndChargesTO.getChargeTO(ondCharge.getChargeRateId());
							if (!(inOnhold && !exclusionChargeCodes.contains(chargeTO.getChargeCode())
									|| chargeTO.isRefundable())) {
								addCharge = true;
								chargeCode = faresAndChargesTO.getChargeTO(ondCharge.getChargeRateId()).getChargeCode();
							}
						} else if (ondCharge.getFareId() != null) {
							if (!ReservationApiUtils.isRefundable(faresAndChargesTO.getFareTO(ondCharge.getFareId()),
									pax.getPaxType())) {
								addCharge = true;
								chargeCode = ReservationInternalConstants.ChargeGroup.FAR;
							}
						}

						if (addCharge) {
							ReservationSegmentDTO segment = segMap.get(firstFareSeg.getPnrSegId());
							SimplifiedFlightSegmentDTO simpleFltSeg = null;
							if (fltSegIdSegments.containsKey(segment.getFlightSegId())) {
								simpleFltSeg = fltSegIdSegments.get(segment.getFlightSegId());
							} else if (fltSegCodeMap.containsKey(segment.getSegmentCode())) {
								simpleFltSeg = fltSegCodeMap.get(segment.getSegmentCode());
							} else if (firstSeg != null) {
								simpleFltSeg = firstSeg;
							}
							if (simpleFltSeg != null) {
								SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
								chargeDTO.setAmount(ondCharge.getEffectiveAmount());
								chargeDTO.setChargeGroupCode(ondCharge.getChargeGroupCode());
								chargeDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
								chargeDTO.setChargeCode(chargeCode);
								chargeDTO.setFlightRefNumber(simpleFltSeg.getFlightRefNumber());
								chargeDTO.setSegmentCode(simpleFltSeg.getSegmentCode());
								chargeDTO.setSegmentSequence(simpleFltSeg.getSegmentSequence());
								chargesList.add(chargeDTO);
							}
						}
					}
				}
			}
			if (chargesList.size() > 0) {
				SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
				paxDTO.setPaxSequence(pax.getPaxSequence());
				paxDTO.setPaxType(pax.getPaxType());
				paxWiseCharges.put(paxDTO, chargesList);
			}
		}

		return paxWiseCharges;

	}

	private ServiceTaxQuoteForTicketingRevenueRQ createRequest() throws ModuleException {
		ServiceTaxQuoteForTicketingRevenueRQ request = new ServiceTaxQuoteForTicketingRevenueRQ();
		Map<Integer, Collection<OndFareDTO>> jouneyONDWiseFareONDs = new HashMap<Integer, Collection<OndFareDTO>>();
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			if (jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()) == null) {
				jouneyONDWiseFareONDs.put(ondFareDTO.getOndSequence(), new ArrayList<OndFareDTO>());
			}
			jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()).add(ondFareDTO);
		}

		IPaxCountAssembler paxAssm = new PaxCountAssembler(reservation.getTotalPaxAdultCount(),
				reservation.getTotalPaxChildCount(), reservation.getTotalPaxInfantCount());
		List<Collection<OndFareDTO>> jouneyONDWiseFareONDList = new ArrayList<Collection<OndFareDTO>>(
				jouneyONDWiseFareONDs.values());
		FareTypeTO fareTypeTO = FareConvertUtil.getFareTypeTO(jouneyONDWiseFareONDList, paxAssm, null, null, null);

		Map<String, List<SimplifiedChargeDTO>> paxTypeWiseCharges = ServiceTaxConverterUtil.composePaxTypeWiseCharges(fareTypeTO);
		request.setPaxTypeWiseCharges(paxTypeWiseCharges);

		Map<Integer, SimplifiedFlightSegmentDTO> fltSegIdSegments = new HashMap<>();

		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<SimplifiedFlightSegmentDTO>();
		boolean isReturnFareEligible = isReturnFareEligible(jouneyONDWiseFareONDList);
		for (OndFareDTO ondFare : ondFareDTOs) {

			Map<Integer, SegmentSeatDistsDTO> fltSegIdSD = new HashMap<>();
			for (SegmentSeatDistsDTO segSeatDist : ondFare.getSegmentSeatDistsDTO()) {
				fltSegIdSD.put(segSeatDist.getFlightSegId(), segSeatDist);
			}
			boolean returnFlag = isReturnFareEligible && ondFare.getOndSequence() == OndSequence.IN_BOUND;
			for (FlightSegmentDTO flightSegmentTO : ondFare.getSegmentsMap().values()) {
				SegmentSeatDistsDTO segSeatDist = fltSegIdSD.get(flightSegmentTO.getSegmentId());
				if (segSeatDist != null) {
					SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
					simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
					simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
					simplifiedFlightSegmentDTO.setOndSequence(ondFare.getOndSequence());
					simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
					simplifiedFlightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
					simplifiedFlightSegmentDTO.setLogicalCabinClassCode(segSeatDist.getLogicalCabinClass());
					simplifiedFlightSegmentDTO.setReturnFlag(returnFlag);
					simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getCarrierCode());
					fltSegIdSegments.put(flightSegmentTO.getSegmentId(), simplifiedFlightSegmentDTO);
					simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
				}
			}
		}
		Collections.sort(simplifiedFlightSegmentDTOs);
		int segmentSequence = 0;
		for (SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO : simplifiedFlightSegmentDTOs) {
			simplifiedFlightSegmentDTO.setSegmentSequence(segmentSequence);
			segmentSequence++;
		}

		Map<Integer, String> paxTypeMap = new HashMap<>();
		for (ReservationPax pax : reservation.getPassengers()) {
			paxTypeMap.put(pax.getPaxSequence(), pax.getPaxType());
		}
		Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = null;
		paxWiseCharges = new HashMap<SimplifiedPaxDTO, List<SimplifiedChargeDTO>>();

		Map<Integer, SimplifiedPaxDTO> paxSeqSimplifiedPaxDTO = new HashMap<>();
		for (Integer paxSequence : externalCharges.keySet()) {
			List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
			SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
			paxDTO.setPaxSequence(paxSequence);
			paxDTO.setPaxType(paxTypeMap.get(paxSequence));
			for (ExternalChgDTO externalChgDTO : externalCharges.get(paxSequence)) {
				SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
				chargeDTO.setAmount(externalChgDTO.getAmount());
				chargeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
				chargeDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				chargeDTO.setChargeCode(externalChgDTO.getChargeCode());

				SimplifiedFlightSegmentDTO flightSegmentTO = fltSegIdSegments.get(externalChgDTO.getFlightSegId());
				if (flightSegmentTO != null){
					chargeDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
					chargeDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
					chargeDTO.setSegmentSequence(flightSegmentTO.getSegmentSequence());
				}
				chargesList.add(chargeDTO);
			}
			paxSeqSimplifiedPaxDTO.put(paxSequence, paxDTO);
			paxWiseCharges.put(paxDTO, chargesList);
		}

		Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> nonRefundables = getPaxWiseNonRefundables(fltSegIdSegments);

		if (nonRefundables != null && nonRefundables.size() > 0) {
			for (SimplifiedPaxDTO paxDTO : nonRefundables.keySet()) {
				if (paxSeqSimplifiedPaxDTO.containsKey(paxDTO.getPaxSequence())) {
					paxWiseCharges.get(paxSeqSimplifiedPaxDTO.get(paxDTO.getPaxSequence())).addAll(nonRefundables.get(paxDTO));
				} else {
					paxWiseCharges.put(paxDTO, nonRefundables.get(paxDTO));
				}
			}
		}

		request.setPaxWiseExternalCharges(paxWiseCharges);

		request.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);
		request.setPaxState(reservation.getContactInfo().getState());
		request.setPaxCountryCode(reservation.getContactInfo().getCountryCode());
		request.setPaxTaxRegistered(
				reservation.getContactInfo().getTaxRegNo() != null && !"".equals(reservation.getContactInfo().getTaxRegNo()));

		return request;
	}

	private boolean isReturnFareEligible(List<Collection<OndFareDTO>> jouneyONDWiseFareONDs) {

		if (jouneyONDWiseFareONDs.size() == 2) {
			List<FlightSegmentDTO> fltSegments = new ArrayList<>();
			for (OndFareDTO ondFare : jouneyONDWiseFareONDs.get(0)) {
				fltSegments.addAll(ondFare.getSegmentsMap().values());
			}
			Collections.sort(fltSegments);
			String outboundOrigin = fltSegments.get(0).getFromAirport();
			String outboundDestination = fltSegments.get(fltSegments.size() - 1).getToAirport();

			fltSegments = new ArrayList<>();
			for (OndFareDTO ondFare : jouneyONDWiseFareONDs.get(1)) {
				fltSegments.addAll(ondFare.getSegmentsMap().values());
			}
			Collections.sort(fltSegments);
			String inboundOrigin = fltSegments.get(0).getFromAirport();
			String inboundDestination = fltSegments.get(fltSegments.size() - 1).getToAirport();

			if (inboundOrigin.equals(outboundDestination) && outboundOrigin.equals(inboundDestination)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void calculate() throws ModuleException {
		ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ = createRequest();
		ServiceTaxQuoteForTicketingRevenueRS serviceTaxForTicketing = ReservationModuleUtils.getChargeBD()
				.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTktRevRQ);

		Map<Integer, Collection<ReservationPaxOndCharge>> paxNewOndCharges = paxWiseNewPNRCharges(serviceTaxForTicketing);
		Map<Integer, Collection<? extends ExternalChgDTO>> paxChgMap = new HashMap<>();
		for (ReservationPax pax : reservation.getPassengers()) {
			OperationChgTnx existingChg = new OperationChgTnx(CalendarUtil.getCurrentSystemTimeInZulu(), null);
			OperationChgTnx newChg = new OperationChgTnx(CalendarUtil.getCurrentSystemTimeInZulu(), null);
			if (!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
				for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
					if (!isFlownPaxFare(paxFare)) {
						OndChargeUtil.setChargeCodeInPaxCharges(paxFare.getCharges());
						for (ReservationPaxOndCharge ondCharge : paxFare.getCharges()) {
							if (isServiceTax(ondCharge)) {
								existingChg.addCharge(ondCharge);
							}
						}
					}
				}
			}

			Collection<ReservationPaxOndCharge> newCharges = paxNewOndCharges.get(pax.getPaxSequence());
			newChg.addCharges(newCharges);
			OperationChgTnx effecitveChg = newChg.diffWith(existingChg);

			Collection<? extends ExternalChgDTO> serviceTaxes = getServiceTaxForPax(effecitveChg, serviceTaxForTicketing,
					pax.getPaxType(), pax.getPaxSequence());

			paxChgMap.put(pax.getPnrPaxId(), serviceTaxes);
		}

		this.paxExtraFee = paxChgMap;
	}

	private boolean isFlownPaxFare(ReservationPaxFare paxFare) {
		for (ReservationPaxFareSegment paxFareSegment : paxFare.getPaxFareSegments()) {
			if (isFlownPnrSegment(paxFareSegment.getPnrSegId())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<? extends ExternalChgDTO> getPaxExtraFee(Integer pnrPaxId) {
		return paxExtraFee.get(pnrPaxId);
	}

	@Override
	public boolean hasExtraFeeFor(Integer pnrPaxId) {
		return paxExtraFee.containsKey(pnrPaxId);
	}

	private boolean isValidServiceTaxEntry(ServiceTaxDTO tax) {
		if (!isFlownFltSegment(tax.getFlightRefNumber())
				&& AccelAeroCalculator.isGreaterThan(tax.getAmount(), AccelAeroCalculator.getDefaultBigDecimalZero())) {
			return true;
		}
		return false;
	}

}
