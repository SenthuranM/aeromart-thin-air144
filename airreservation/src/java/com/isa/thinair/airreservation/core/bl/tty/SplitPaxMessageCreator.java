package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageIdentifier;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.SyncMessageIdentifier;
import com.isa.thinair.msgbroker.api.util.MessageComposerApiUtils;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class SplitPaxMessageCreator extends TypeBReservationMessageCreator {
	
	public SplitPaxMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesComposerForSplitPax();
	}

	@Override
	public List<OSIDTO> addOSIDetails(Reservation reservation, TypeBRequestDTO typeBRequestDTO) {
		List<OSIDTO> osiDTOs = new ArrayList<OSIDTO>();
		OSIDTO osiDTO = new OSIDTO();
		if (typeBRequestDTO.getCsOCCarrierCode() != null) {
			osiDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
		} else {
			osiDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		}
		osiDTO.setCodeOSI(GDSExternalCodes.OSICodes.RECORD_LOCATOR.getCode());
		String osiValue = MessageComposerApiUtils.generateBookingOffice(AppSysParamsUtil.getDefaultCarrierCode()) + " " + reservation.getPnr();
		osiDTO.setOsiValue(osiValue);
		osiDTOs.add(osiDTO);
		return osiDTOs;
	}

	@Override
	public String getMessageIdentifier() {
		return SyncMessageIdentifier.DIVIDE.getCode();
	}
	
	@Override
	public String getCSMessageIdentifier() {
		return MessageIdentifier.DEVIDE_BOOKING.getCode();
	}

	@Override
	public RecordLocatorDTO getResponderRecordLocator(Reservation reservation, TypeBRequestDTO typeBRequestDTO){
		RecordLocatorDTO responderRecordLocator = new RecordLocatorDTO();
		responderRecordLocator.setPnrReference(typeBRequestDTO.getNewPnr());
		return responderRecordLocator;
	}

}
