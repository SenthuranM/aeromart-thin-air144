/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for purchase tickets
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="ticketPurchase"
 */
public class TicketPurchase extends DefaultBaseCommand {

	// Dao's
	private ReservationTnxDAO reservationTnxDao;

	/**
	 * constructor of the TicketPurchase command
	 */
	public TicketPurchase() {

		// looking up daos
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;

	}

	/**
	 * execute method of the TicketPurchase command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public ServiceResponce execute() throws ModuleException {
		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal segmentTotal = (BigDecimal) this.getParameter(CommandParamNames.SEGMENT_TOTAL);
		BigDecimal totalPaymentAmount = (BigDecimal) this.getParameter(CommandParamNames.TOTAL_PAYMENT_AMOUNT);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Collection paymentCollection = (Collection) this.getParameter(CommandParamNames.PAYMENT_LIST);
		Date currentTimestamp = (Date) this.getParameter(CommandParamNames.CURRENT_TIMESTAMP);
		String recieptNumber = (String) this.getParameter(CommandParamNames.RECIEPT_NUMBER);
		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = (ReservationPaxPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO);
		Collection cardConfigData = (Collection) this.getParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA);
		boolean enableTransactionGranularity = (Boolean) this.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);
		boolean isGoShowProcess = this.getParameter(CommandParamNames.IS_GOSHOW_PROCESS, Boolean.FALSE, Boolean.class);
		boolean isActualPayment = this.getParameter(CommandParamNames.IS_ACTUAL_PAYMENT, Boolean.FALSE, Boolean.class);
		boolean isFirstPayment = this.getParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.FALSE, Boolean.class);
		Integer transactionSeq = (Integer) this.getParameter(CommandParamNames.TRANSACTION_SEQ);
		Map<String, BigDecimal> lmsPaxProductRedeemedAmount = (Map<String, BigDecimal>) this
				.getParameter(CommandParamNames.LOYALTY_PRODUCT_REDEEMED_AMOUNTS);
		
		List<TransactionSegment> transactionSegments = (List<TransactionSegment>) this.getParameter(CommandParamNames.TRNX_SEGMENTS);

		if (currentTimestamp == null) { // Have this till you handle all the paths this command can be called is covered
			currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		}

		// checking params
		this.checkParams(pnrPaxId, segmentTotal, totalPaymentAmount, credentialsDTO);

		// No need to set payment carrier for ticket purchase as it does not include payment
		// Added this condition to avoid additional zero record ticket purchase in pax transaction.
		// For some clients, total ticket price may be zero. we need to record that flow
		// Additional record will be still recorded for force confirm. This can also be improved
		if (segmentTotal.doubleValue() > 0 || (segmentTotal.doubleValue() == 0 && totalPaymentAmount.doubleValue() == 0)) {
			ReservationTnx tnx = TnxFactory.getDebitInstance(pnrPaxId, segmentTotal,
					ReservationTnxNominalCode.TICKET_PURCHASE.getCode(), credentialsDTO, currentTimestamp, null, null,
					isGoShowProcess);
			reservationTnxDao.saveTransaction(tnx);
		}

		if (paymentCollection != null && !paymentCollection.isEmpty()) {
			Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RECORD_PAYMENT);
			command.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
			command.setParameter(CommandParamNames.PAYMENT_LIST, paymentCollection);
			command.setParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO, reservationPaxPaymentMetaTO);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.CURRENT_TIMESTAMP, currentTimestamp);
			command.setParameter(CommandParamNames.RECIEPT_NUMBER, recieptNumber);
			command.setParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA, cardConfigData);
			command.setParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, enableTransactionGranularity);
			command.setParameter(CommandParamNames.IS_GOSHOW_PROCESS, isGoShowProcess);
			command.setParameter(CommandParamNames.IS_ACTUAL_PAYMENT, isActualPayment);
			command.setParameter(CommandParamNames.IS_FIRST_PAYMENT, isFirstPayment);
			command.setParameter(CommandParamNames.TRANSACTION_SEQ, transactionSeq);
			command.setParameter(CommandParamNames.LOYALTY_PRODUCT_REDEEMED_AMOUNTS, lmsPaxProductRedeemedAmount);
			command.setParameter(CommandParamNames.TRNX_SEGMENTS, transactionSegments);
			command.execute();
		} else {
			TnxGranularityFactory.saveReservationPaxTnxBrkForCreditsComponsatingNewCharges(pnrPaxId,
					ReservationTnxNominalCode.CREDIT_BF, reservationPaxPaymentMetaTO, enableTransactionGranularity,
					transactionSeq);
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param credentialsDTO
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxId, BigDecimal segmentTotal, BigDecimal totalPaymentAmount,
			CredentialsDTO credentialsDTO) throws ModuleException {

		if (pnrPaxId == null || segmentTotal == null || credentialsDTO == null || totalPaymentAmount == null)
			// throw exception
			throw new ModuleException("airreservations.arg.invalid.null");
	}
}
