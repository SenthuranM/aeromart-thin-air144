package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CustomerPayment;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Nilindra Fernando
 * @since 6:43 PM 1/14/2009
 */
class PayCurrencyRounderAdaptorUtils {

	protected static Collection<PaymentAssembler> getTotalPaymentAssemblers(PaymentAssembler assembler) {
		Collection<PaymentAssembler> colPaymentAssembler = new ArrayList<PaymentAssembler>();

		if (assembler != null) {
			colPaymentAssembler.add(assembler);
		}

		return colPaymentAssembler;
	}

	protected static Collection<PaymentAssembler> getTotalPaymentAssemblers(Collection<IPayment> iPayments) {
		Collection<PaymentAssembler> colPaymentAssembler = new ArrayList<PaymentAssembler>();

		Iterator<IPayment> itIPayment = iPayments.iterator();
		IPayment iPayment;
		PaymentAssembler pAssembler;

		while (itIPayment.hasNext()) {
			iPayment = (IPayment) itIPayment.next();
			pAssembler = (PaymentAssembler) iPayment;

			if (pAssembler != null) {
				colPaymentAssembler.add(pAssembler);
			}
		}

		return colPaymentAssembler;
	}

	protected static Collection<PaymentAssembler> getTotalPaymentAssemblers(PassengerAssembler assembler) {
		Collection<PaymentAssembler> colPaymentAssembler = new ArrayList<PaymentAssembler>();
		Iterator<IPayment> itIPayment = assembler.getPassengerPaymentsMap().values().iterator();
		IPayment iPayment;
		PaymentAssembler pAssembler;

		while (itIPayment.hasNext()) {
			iPayment = (IPayment) itIPayment.next();
			pAssembler = (PaymentAssembler) iPayment;

			if (pAssembler != null) {
				colPaymentAssembler.add(pAssembler);
			}
		}

		return colPaymentAssembler;
	}

	protected static Collection<PaymentAssembler> getTotalPaymentAssemblers(SegmentAssembler assembler) {
		Collection<PaymentAssembler> colPaymentAssembler = new ArrayList<PaymentAssembler>();
		Iterator<IPayment> itIPayment = assembler.getPassengerPaymentsMap().values().iterator();
		IPayment iPayment;
		PaymentAssembler pAssembler;

		while (itIPayment.hasNext()) {
			iPayment = (IPayment) itIPayment.next();
			pAssembler = (PaymentAssembler) iPayment;

			if (pAssembler != null) {
				colPaymentAssembler.add(pAssembler);
			}
		}

		return colPaymentAssembler;
	}

	protected static Collection<PaymentAssembler> getTotalPaymentAssemblers(ReservationAssembler assembler) {
		Collection<PaymentAssembler> colPaymentAssembler = new ArrayList<PaymentAssembler>();
		Iterator<ReservationPax> itReservationPax = assembler.getDummyReservation().getPassengers().iterator();
		ReservationPax reservationPax;
		PaymentAssembler pAssembler;
		IPayment iPayment;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();
			iPayment = reservationPax.getPayment();

			if (iPayment != null) {
				pAssembler = (PaymentAssembler) iPayment;
				colPaymentAssembler.add(pAssembler);
			}
		}

		return colPaymentAssembler;
	}

	private static BigDecimal getTotalPayAmountInBase(Collection<PaymentAssembler> payments) {
		BigDecimal totalPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (PaymentAssembler paymentAssembler : payments) {
			totalPayAmount = AccelAeroCalculator.add(totalPayAmount, paymentAssembler.getTotalPayAmount());
		}

		return totalPayAmount;
	}

	private static Collection<CustomerPayment> getPayTypesRoundedInPayCurr(PaymentAssembler paymentAssembler)
			throws ModuleException {

		Collection<PaymentInfo> payTypes = paymentAssembler.getPayments();
		Collection<CustomerPayment> payTypeObjects = new ArrayList<CustomerPayment>();
		Iterator<PaymentInfo> itPaymentInfo = payTypes.iterator();
		PaymentInfo paymentInfo;
		PaxCreditInfo paxCreditInfo;
		BigDecimal totalPerPaxPerTypePayAmountRounded;

		while (itPaymentInfo.hasNext()) {
			paymentInfo = itPaymentInfo.next();

			// For Pax Credit Payment
			if (paymentInfo instanceof PaxCreditInfo) {
				paxCreditInfo = (PaxCreditInfo) paymentInfo;
				PaxCreditDTO paxCreditDTO = paxCreditInfo.getPaxCredit();

				totalPerPaxPerTypePayAmountRounded = AccelAeroRounderPolicy.convertAndRound(paxCreditDTO.getBalance(),
						paxCreditDTO.getPayCurrencyDTO().getPayCurrMultiplyingExchangeRate(), paxCreditDTO.getPayCurrencyDTO()
								.getBoundaryValue(), paxCreditDTO.getPayCurrencyDTO().getBreakPointValue());

				paxCreditDTO.getPayCurrencyDTO().setTotalPayCurrencyAmount(totalPerPaxPerTypePayAmountRounded);
				payTypeObjects.add(paxCreditDTO);
			}

			// For Card Payment
			else if (paymentInfo instanceof CardPaymentInfo) {
				CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) paymentInfo;
				// Allow frontend calculated value to be saved for card payments if flag is true
				if (!cardPaymentInfo.getPayCurrencyDTO().getSkipCurrencyConversion()) {
					totalPerPaxPerTypePayAmountRounded = AccelAeroRounderPolicy.convertAndRound(cardPaymentInfo.getTotalAmount(),
							cardPaymentInfo.getPayCurrencyDTO().getPayCurrMultiplyingExchangeRate(), cardPaymentInfo
									.getPayCurrencyDTO().getBoundaryValue(), cardPaymentInfo.getPayCurrencyDTO()
									.getBreakPointValue());

					cardPaymentInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(totalPerPaxPerTypePayAmountRounded);
				}
				payTypeObjects.add(cardPaymentInfo);
			}

			// For Agent Credit
			else if (paymentInfo instanceof AgentCreditInfo) {
				AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
				totalPerPaxPerTypePayAmountRounded = AccelAeroRounderPolicy
						.convertAndRound(agentCreditInfo.getTotalAmount(), agentCreditInfo.getPayCurrencyDTO()
								.getPayCurrMultiplyingExchangeRate(), agentCreditInfo.getPayCurrencyDTO().getBoundaryValue(),
								agentCreditInfo.getPayCurrencyDTO().getBreakPointValue());

				agentCreditInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(totalPerPaxPerTypePayAmountRounded);
				payTypeObjects.add(agentCreditInfo);
			}

			// For Cash Payment
			else if (paymentInfo instanceof CashPaymentInfo) {
				CashPaymentInfo cashPaymentInfo = (CashPaymentInfo) paymentInfo;
				totalPerPaxPerTypePayAmountRounded = AccelAeroRounderPolicy
						.convertAndRound(cashPaymentInfo.getTotalAmount(), cashPaymentInfo.getPayCurrencyDTO()
								.getPayCurrMultiplyingExchangeRate(), cashPaymentInfo.getPayCurrencyDTO().getBoundaryValue(),
								cashPaymentInfo.getPayCurrencyDTO().getBreakPointValue());

				cashPaymentInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(totalPerPaxPerTypePayAmountRounded);
				payTypeObjects.add(cashPaymentInfo);
			}

			// For Loyalty Payment
			else if (paymentInfo instanceof LMSPaymentInfo) {
				LMSPaymentInfo lmsPaymentInfo = (LMSPaymentInfo) paymentInfo;
				totalPerPaxPerTypePayAmountRounded = AccelAeroRounderPolicy.convertAndRound(lmsPaymentInfo.getTotalAmount(),
						lmsPaymentInfo.getPayCurrencyDTO().getPayCurrMultiplyingExchangeRate(), lmsPaymentInfo
								.getPayCurrencyDTO().getBoundaryValue(), lmsPaymentInfo.getPayCurrencyDTO().getBreakPointValue());

				lmsPaymentInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(totalPerPaxPerTypePayAmountRounded);
				payTypeObjects.add(lmsPaymentInfo);
			}
		}

		return payTypeObjects;
	}

	protected static void convertAndRoundToPayCurr(Collection<PaymentAssembler> payments) throws ModuleException {
		BigDecimal totalPayAmountInBase = PayCurrencyRounderAdaptorUtils.getTotalPayAmountInBase(payments);

		if (totalPayAmountInBase.doubleValue() > 0) {
			// PayCurrencyRounderAdaptorUtils.updateWithRoundedValuesPerPax(payments, totalPayAmountInBase);

			// Rounding between each passenger's payments
			for (PaymentAssembler paymentAssembler : payments) {
				PayCurrencyRounderAdaptorUtils.getPayTypesRoundedInPayCurr(paymentAssembler);
				// PayCurrencyRounderAdaptorUtils.updatePerPaxPaymentsWithRoundedPayValue(colPayTyeObjects,
				// perPaxPayCurrRoundedAmountInPayCurr);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	protected static void updatePayCurrReferences(Collection<PaymentAssembler> payments) throws ModuleException {
		Iterator itPaymentInfo;
		Object paymentInfo;

		for (PaymentAssembler paymentAssembler : payments) {

			itPaymentInfo = paymentAssembler.getPayments().iterator();

			while (itPaymentInfo.hasNext()) {
				paymentInfo = (Object) itPaymentInfo.next();

				// For Card Payment
				if (paymentInfo instanceof CardPaymentInfo) {
					CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) paymentInfo;
					cardPaymentInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(
							convertAmount(cardPaymentInfo.getTotalAmount(), cardPaymentInfo.getPayCurrencyDTO(),
									cardPaymentInfo.getPaymentTnxId()));
				}

				// For Agent Credit
				else if (paymentInfo instanceof AgentCreditInfo) {
					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					agentCreditInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(
							convertAmount(agentCreditInfo.getTotalAmount(), agentCreditInfo.getPayCurrencyDTO(),
									agentCreditInfo.getPaymentTnxId()));
				}

				// For Cash Payment
				else if (paymentInfo instanceof CashPaymentInfo) {
					CashPaymentInfo cashPaymentInfo = (CashPaymentInfo) paymentInfo;
					cashPaymentInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(
							convertAmount(cashPaymentInfo.getTotalAmount(), cashPaymentInfo.getPayCurrencyDTO(),
									cashPaymentInfo.getPaymentTnxId()));
				}

				// For Loyalty Payment
				else if (paymentInfo instanceof LMSPaymentInfo) {
					LMSPaymentInfo loPaymentInfo = (LMSPaymentInfo) paymentInfo;
					loPaymentInfo.getPayCurrencyDTO().setTotalPayCurrencyAmount(
							convertAmount(loPaymentInfo.getTotalAmount(), loPaymentInfo.getPayCurrencyDTO(),
									loPaymentInfo.getPaymentTnxId()));
				}
			}
		}

	}

	private static BigDecimal
			convertAmount(BigDecimal totalPayAmount, PayCurrencyDTO payCurrencyDTO, Integer originalPaymentTnxId)
					throws ModuleException {
		if (AppSysParamsUtil.getBaseCurrency().equals(payCurrencyDTO.getPayCurrencyCode())) {
			return totalPayAmount;
		} else {

			if (AppSysParamsUtil.isEnableTransactionGranularity() && !AppSysParamsUtil.isLCCConnectivityEnabled()) {
				BigDecimal totalPayCurrencyAmount;
				totalPayCurrencyAmount = TnxGranularityFactory.getPayCurrencyAmountByTnxBreakdown(originalPaymentTnxId,
						totalPayAmount, payCurrencyDTO);
				return totalPayCurrencyAmount.negate();
			} else {
				return AccelAeroRounderPolicy.convertAndRound(totalPayAmount, payCurrencyDTO.getPayCurrMultiplyingExchangeRate(),
						payCurrencyDTO.getBoundaryValue(), payCurrencyDTO.getBreakPointValue());
			}

		}
	}

}
