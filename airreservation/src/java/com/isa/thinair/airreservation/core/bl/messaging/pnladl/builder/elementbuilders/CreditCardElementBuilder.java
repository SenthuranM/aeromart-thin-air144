/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ChildElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.CreditCardElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class CreditCardElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> creditRuleExecutor = new CreditCardElementRuleExecutor();

	private boolean isStartWithNewLine = false;

	@Override
	public void buildElement(ElementContext context) {
		if (context != null && context.getFeaturePack() != null
				&& context.getFeaturePack().isShowEticketDetails()) {
			initContextData(context);
			creditElementTemplate();
		}
		executeNext();
	}

	private void creditElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation != null
					&& passengerInformation.getCcDigits() != null
					&& !passengerInformation.getCcDigits().isEmpty()) {
				buildCreditCardInformations(elementTemplate,
						passengerInformation);
				currentElement = elementTemplate.toString();
				if(currentElement != null && !currentElement.isEmpty()){
					ammendmentPreValidation(isStartWithNewLine, currentElement, uPContext, creditRuleExecutor);
				}
				break;
			}
		}
	}

	private void buildCreditCardInformations(StringBuilder elementTemplate,
			PassengerInformation passengerInformation) {
			
		if (passengerInformation.getCcDigits() != null
				&& !passengerInformation.getCcDigits().isEmpty()) {
			elementTemplate.setLength(0);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.EPAY);
			elementTemplate.append(MessageComposerConstants.SP);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.KK + passengerInformations.size());
			elementTemplate.append(MessageComposerConstants.SP);
			elementTemplate.append(" CC/XXXX-XXXX-XXXX-");
			elementTemplate.append(passengerInformation.getCcDigits());
		}

	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
