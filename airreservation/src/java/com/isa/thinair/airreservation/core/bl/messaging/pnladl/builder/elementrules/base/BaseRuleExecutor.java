/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;

/**
 * @author udithad
 *
 */
public abstract class BaseRuleExecutor<T> {

	protected RuleResponse response;

	protected List<BaseRule> rulesList;

	public abstract RuleResponse validateElementRules(T t);

	public void addRules(BaseRule rule) {
		rulesList.add(rule);
	}

	public void clearRules() {
		rulesList.clear();
	}
	
	protected String[] getOrderedArray(String[] splittedByRange,String firstElement){
		String[] returnArray = new String[splittedByRange.length+1];
		int index=0;
		returnArray[0]=firstElement;
		for(String element:splittedByRange){
				returnArray[++index]=element;
		}
		return returnArray;
	}

}
