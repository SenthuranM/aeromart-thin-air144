package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVTrasmissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PNRGOVReservationLog;
import com.isa.thinair.airreservation.api.model.PNRGOVTxHistoryLog;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface PnrGovDAO {

	public List<PNRGOVTrasmissionDetailsDTO> getFlightsForPnrGovScheduling(String airportCode, int timePeriod,
			int pnrGovTrasmissionGap, Date date, boolean isOutbound);

	public void saveOrUpdatePnrGovMessageHistoryLog(PNRGOVTxHistoryLog txHistory);

	public boolean hasPnrGovMessageSent(int fltSegId, String countryCode, String airportCode, boolean isOutbound, int timePeriod);

	public void saveOrUpdatePnrGovReservationLog(PNRGOVReservationLog resLog);
	
	public void saveOrUpdatePnrGovReservationLogList(List<PNRGOVReservationLog> resLogList);
	
	public Page getPNRGOVReports(int startrec, int numofrecs) throws ModuleException;

	public Page getPNRGOVReports(int startrec, int numofrecs, List criteria) throws ModuleException;
	
	public PNRGOVTxHistoryLog getHistoryLog(String logId) throws ModuleException;
	
	public PNRGOVTxHistoryLog getAlreadyLoggedEntry(int fltSegId, String countryCode, String airportCode, String inboundOutbound, int timePeriod) throws ModuleException;
}
