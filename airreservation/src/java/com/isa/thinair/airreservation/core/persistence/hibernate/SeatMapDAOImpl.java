/* ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.AnciTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * SalesDAOImpl is the business DAO hibernate implementation
 * 
 * @author Byorn
 * @since 1.0
 * @isa.module.dao-impl dao-name="SeatMapDAO"
 */
public class SeatMapDAOImpl extends PlatformBaseHibernateDaoSupport implements SeatMapDAO {

	@Override
	public void saveOrUpdate(Collection<PassengerSeating> seating) {
		super.hibernateSaveOrUpdateAll(seating);
	}

	public void deleteReservationSeats(Collection<PassengerSeating> seating) {
		deleteAll(seating);
	}

	public Collection<PassengerSeating> getFlightSeatsForPnrPaxFare(Collection<Integer> ppfIds) {
		String hql = "from PassengerSeating s where s.status = '" + AirinventoryCustomConstants.FlightSeatStatuses.RESERVED
				+ "' and s.ppfId in (" + Util.buildIntegerInClauseContent(ppfIds) + ")";
		return find(hql, PassengerSeating.class);
	}

	@Override
	public Collection<PassengerSeating> getFlightSeats(Collection<Integer> pnrPaxIds) {
		String hql = "from PassengerSeating s where s.status = '" + AirinventoryCustomConstants.FlightSeatStatuses.RESERVED + "'"
				+ " and s.pnrPaxId in (" + Util.buildIntegerInClauseContent(pnrPaxIds) + ")";
		return find(hql, PassengerSeating.class);

	}

	@Override
	public Collection<PassengerSeating> getReservedFlightSeats(Collection<Integer> flightAMSeatIds) {
		String hql = "from PassengerSeating s where s.status = '" + AirinventoryCustomConstants.FlightSeatStatuses.RESERVED + "'"
				+ " and s.flightSeatId in (" + Util.buildIntegerInClauseContent(flightAMSeatIds) + ")";
		return find(hql, PassengerSeating.class);
	}

	@Override
	public Collection<PassengerSeating> getPassengerSeats(Collection<Integer> pkeys) {
		String hql = "from PassengerSeating s where s.status = '" + AirinventoryCustomConstants.FlightSeatStatuses.RESERVED + "'"
				+ " and s.paxSeatingid in (" + Util.buildIntegerInClauseContent(pkeys) + ")";
		return find(hql, PassengerSeating.class);
	}

	@SuppressWarnings("unchecked")
	public Collection<PaxSeatTO> getReservationSeats(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) {

		String sql = " SELECT a.ppsam_seat_id, a.flight_am_seat_id, a.pnr_pax_id, a.pnr_seg_id, nvl(d.seat_code,'N/A') as seat_code, a.charge_amount, a.ppf_id, b.pax_type, a.auto_cancellation_id "
				+ " FROM sm_t_pnr_pax_seg_am_seat a left outer join sm_t_flight_am_seat b on a.flight_am_seat_id=b.flight_am_seat_id "
				+ " left outer join  sm_t_am_seat_charge c on b.ams_charge_id      = c.ams_charge_id left outer join sm_t_aircraft_model_seats d on c.am_seat_id         = d.am_seat_id "
				+ " WHERE  "
				+ " a.status='"
				+ AirinventoryCustomConstants.FlightSeatStatuses.RESERVED
				+ "'"
				+ " AND a.pnr_pax_id in ("
				+ Util.constructINStringForInts(pnrPaxIds)
				+ ")"
				+ " AND a.pnr_seg_id in ("
				+ Util.constructINStringForInts(pnrSegIds) + ")";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<PaxSeatTO> colSeats = (Collection<PaxSeatTO>) template.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<PaxSeatTO> passengerSeats = new ArrayList<PaxSeatTO>();
				while (rs.next()) {
					PaxSeatTO paxSeatTO = new PaxSeatTO();
					paxSeatTO.setSelectedFlightSeatId(new Integer(rs.getInt("flight_am_seat_id")));
					paxSeatTO.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
					paxSeatTO.setPnrSegId(new Integer(rs.getInt("pnr_seg_id")));
					paxSeatTO.setPnrPaxFareId(new Integer(rs.getInt("ppf_id")));
					paxSeatTO.setSeatCode(rs.getString("seat_code"));
					paxSeatTO.setPaxType(rs.getString("pax_type"));
					paxSeatTO.setPkey(new Integer(rs.getInt("ppsam_seat_id")));

					if (AppSysParamsUtil.isAutoCancellationEnabled() && rs.getInt("auto_cancellation_id") != 0) {
						paxSeatTO.setAutoCancellationId(rs.getInt("auto_cancellation_id"));
					}

					SMExternalChgDTO externalChgDTO = new SMExternalChgDTO();
					externalChgDTO.setAmount(rs.getBigDecimal("charge_amount"));
					paxSeatTO.setChgDTO(externalChgDTO);
					passengerSeats.add(paxSeatTO);
				}
				return passengerSeats;
			}
		});

		return colSeats;
	}

	/**
	 * @
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, String> getReservationSeatsForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) {

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT a.flight_am_seat_id, a.pnr_pax_id, a.pnr_seg_id, d.seat_code, e.pnr");
		sql.append(" FROM sm_t_pnr_pax_seg_am_seat a,  sm_t_flight_am_seat b, sm_t_am_seat_charge c, sm_t_aircraft_model_seats d, t_pnr_passenger e ");
		sql.append(" WHERE a.flight_am_seat_id=b.flight_am_seat_id and ");
		sql.append(" b.ams_charge_id = c.ams_charge_id ");
		sql.append(" AND c.am_seat_id = d.am_seat_id ");
		sql.append(" AND a.pnr_pax_id = e.pnr_pax_id ");
		// AARESAA-9891 , seat charge status not applicable after making the booking.
		// sql.append(" AND c.status = '" + CommonsConstants.STATUS_ACTIVE + "'");
		sql.append(" AND a.status='" + AirinventoryCustomConstants.FlightSeatStatuses.RESERVED + "'");
		sql.append(" AND a.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ")");
		sql.append(" AND a.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		sql.append(" order by a.pnr_pax_id");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, String> pnrSeatsMap = (Map<Integer, String>) template.query(sql.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> pnrSeatMap = new HashMap<Integer, String>();

				while (rs.next()) {
					Integer pnrPaxId = new Integer(rs.getInt("pnr_pax_id"));
					String paxSeatNum = pnrSeatMap.get(pnrPaxId);

					if (paxSeatNum == null) {
						paxSeatNum = rs.getString("seat_code");

					} else {
						paxSeatNum += " " + rs.getString("seat_code");
					}
					pnrSeatMap.put(pnrPaxId, paxSeatNum);
				}
				return pnrSeatMap;
			}
		});

		return pnrSeatsMap;
	}

	public Map<Integer, List<AncillaryDTO>> getReservationSeatsForPNLADL(Collection<Integer> pnrPaxIds,
			Collection<Integer> pnrSegIds) {

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT a.flight_am_seat_id, a.pnr_pax_id, a.pnr_seg_id, d.seat_code, e.pnr");
		sql.append(" FROM sm_t_pnr_pax_seg_am_seat a,  sm_t_flight_am_seat b, sm_t_am_seat_charge c, sm_t_aircraft_model_seats d, t_pnr_passenger e ");
		sql.append(" WHERE a.flight_am_seat_id=b.flight_am_seat_id and ");
		sql.append(" b.ams_charge_id = c.ams_charge_id ");
		sql.append(" AND c.am_seat_id = d.am_seat_id ");
		sql.append(" AND a.pnr_pax_id = e.pnr_pax_id ");
		sql.append(" AND c.status = '" + CommonsConstants.STATUS_ACTIVE + "'");
		sql.append(" AND a.status='" + AirinventoryCustomConstants.FlightSeatStatuses.RESERVED + "'");
		sql.append(" AND a.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ")");
		sql.append(" AND a.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		sql.append(" order by a.pnr_pax_id");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Map<Integer, List<AncillaryDTO>> pnrSeatsMap = (Map<Integer, List<AncillaryDTO>>) template.query(sql.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, List<AncillaryDTO>> pnrSeatMap = new TreeMap<Integer, List<AncillaryDTO>>();

						while (rs.next()) {
							Integer pnrPaxId = new Integer(rs.getInt("pnr_pax_id"));
							String seatCode = rs.getString("seat_code").toUpperCase();
							setMapSeatAncillaryByPaxId(pnrSeatMap, pnrPaxId, seatCode);
						}
						return pnrSeatMap;
					}
				});

		pnrSeatsMap = addAutocheckinSeatMap(pnrSeatsMap, pnrSegIds, pnrPaxIds);

		return pnrSeatsMap;
	}

	private Map<Integer, List<AncillaryDTO>> addAutocheckinSeatMap(Map<Integer, List<AncillaryDTO>> pnrSeatsMap,
			Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT a.FLIGHT_AM_SEAT_ID, a.PNR_PAX_ID, a.PNR_SEG_ID, c.SEAT_CODE, d.PNR");
		sql.append(" FROM T_PNR_PAX_SEG_AUTO_CHECKIN a,  SM_T_FLIGHT_AM_SEAT b, SM_T_AIRCRAFT_MODEL_SEATS c, T_PNR_PASSENGER d  ");
		sql.append(" WHERE a.FLIGHT_AM_SEAT_ID=b.FLIGHT_AM_SEAT_ID ");
		sql.append(" AND b.AM_SEAT_ID = c.AM_SEAT_ID ");
		sql.append(" AND a.PNR_PAX_ID = d.PNR_PAX_ID  ");
		sql.append(" AND a.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ")");
		sql.append(" AND a.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		sql.append(" order by a.pnr_pax_id");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		template.queryForList(sql.toString()).stream().filter(s -> (s.get("FLIGHT_AM_SEAT_ID") != null))
				.forEach(rs -> map.put(Integer.valueOf(rs.get("PNR_PAX_ID").toString()), ((String) rs.get("SEAT_CODE"))));
		map.entrySet().stream().forEach(s -> {
			Integer pnrPaxID = s.getKey();
			String seatCode = s.getValue();
			setMapSeatAncillaryByPaxId(pnrSeatsMap, pnrPaxID, seatCode);
		});

		return pnrSeatsMap;
	}

	/**
	 * @param pnrSeatsMap
	 * @param pnrPaxID
	 * @param seatCode
	 */
	private Map<Integer, List<AncillaryDTO>> setMapSeatAncillaryByPaxId(Map<Integer, List<AncillaryDTO>> pnrSeatsMap,
			Integer pnrPaxID, String seatCode) {
		if (pnrSeatsMap.get(pnrPaxID) == null) {
			List<AncillaryDTO> seats = new ArrayList<AncillaryDTO>();
			AncillaryDTO anci = new AncillaryDTO();
			anci.setAnciType(AnciTypes.SEAT);
			anci.setDescription(seatCode);
			seats.add(anci);
			pnrSeatsMap.put(pnrPaxID, seats);

		} else {
			AncillaryDTO anci = new AncillaryDTO();
			anci.setAnciType(AnciTypes.SEAT);
			anci.setDescription(seatCode);
			pnrSeatsMap.get(pnrPaxID).add(anci);

		}
		return pnrSeatsMap;
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getSeatCodes(Collection<Integer> flightAmSeatIds) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT d.seat_code");
		sql.append(" FROM sm_t_flight_am_seat b, sm_t_am_seat_charge c, sm_t_aircraft_model_seats d ");
		sql.append(" WHERE ");
		sql.append(" b.ams_charge_id = c.ams_charge_id ");
		sql.append(" AND c.am_seat_id = d.am_seat_id ");
		sql.append(" AND b.status = '" + AirinventoryCustomConstants.FlightSeatStatuses.RESERVED + "'");
		sql.append(" AND b.flight_am_seat_id in (" + Util.constructINStringForInts(flightAmSeatIds) + ")");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<String> colSeats = (Collection<String>) template.query(sql.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> passengerSeatCodess = new ArrayList<String>();
				while (rs.next()) {
					passengerSeatCodess.add(rs.getString("seat_code"));
				}
				return passengerSeatCodess;
			}
		});

		return colSeats;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<String> getSeatCodesAndSeatCharge(Collection<Integer> flightAmSeatIds) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT d.seat_code, c.charge_amount");
		sql.append(" FROM sm_t_flight_am_seat b, sm_t_am_seat_charge c, sm_t_aircraft_model_seats d ");
		sql.append(" WHERE ");
		sql.append(" b.ams_charge_id = c.ams_charge_id ");
		sql.append(" AND c.am_seat_id = d.am_seat_id ");
		sql.append(" AND b.flight_am_seat_id in (" + Util.constructINStringForInts(flightAmSeatIds) + ")");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<String> colSeats = (Collection<String>) template.query(sql.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> passengerSeatCodess = new ArrayList<String>();
				while (rs.next()) {
					passengerSeatCodess.add(rs.getString("seat_code") + " [" + rs.getDouble("charge_amount") + "/-]");
				}
				return passengerSeatCodess;
			}
		});

		return colSeats;
	}

	/**
	 * Method will return template Id for particular flight segment id.
	 */
	@Override
	public Integer getTemplateForSegmentId(int flightSegId) {

		String sql = "SELECT amc_template_id FROM sm_t_am_charge_template WHERE amc_template_id"
				+ " IN( SELECT c.amc_template_id FROM sm_t_am_seat_charge c WHERE c.ams_charge_id"
				+ " IN( SELECT b.ams_charge_id FROM sm_t_flight_am_seat b " + " WHERE b.flt_seg_id =" + flightSegId + "))";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Integer) template.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer templateId = null;
				while (rs.next()) {
					templateId = rs.getInt("amc_template_id");
				}
				return templateId;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<ReservationLiteDTO, List<PaxSeatTO>> getExpiredSeats(Collection<Integer> cancellationIds, String marketingCarrier) {
		boolean isDry = AppSysParamsUtil.getDefaultCarrierCode().equals(marketingCarrier) ? false : true;

		StringBuilder query = new StringBuilder();
		query.append("SELECT r.pnr, pp.pnr_pax_id, ps.pnr_seg_id, sm.ppf_id, sm.flight_am_seat_id seat_id, sm.charge_amount charge, r.version, sm.auto_cancellation_id");
		query.append(" FROM t_reservation r, t_pnr_passenger pp, t_pnr_segment ps, sm_t_pnr_pax_seg_am_seat sm");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id = sm.pnr_pax_id AND sm.pnr_seg_id = sm.pnr_seg_id");
		query.append(" AND r.status = ? AND pp.status = ? AND ps.status =? AND sm.status <>? AND r.DUMMY_BOOKING = ? AND ");
		query.append(Util.getReplaceStringForIN("sm.auto_cancellation_id", cancellationIds));
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.ORIGIN_CHANNEL_CODE ");
		if (isDry) {
			query.append("= ?");
		} else {
			query.append("<> ?");
		}

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO), marketingCarrier,
				ReservationInternalConstants.SalesChannel.LCC };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Map<ReservationLiteDTO, List<PaxSeatTO>>) template.query(query.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<ReservationLiteDTO, List<PaxSeatTO>> pnrWiseSeats = null;
				if (rs != null) {
					pnrWiseSeats = new HashMap<ReservationLiteDTO, List<PaxSeatTO>>();
					while (rs.next()) {
						String pnr = rs.getString("pnr");
						long version = rs.getLong("version");
						Integer autoCancellationId = rs.getInt("auto_cancellation_id");

						ReservationLiteDTO resDTO = new ReservationLiteDTO(pnr, version, autoCancellationId);

						if (!pnrWiseSeats.containsKey(resDTO)) {
							pnrWiseSeats.put(resDTO, new ArrayList<PaxSeatTO>());
						}

						PaxSeatTO paxSeatTO = new PaxSeatTO();
						paxSeatTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
						paxSeatTO.setPnrSegId(rs.getInt("pnr_seg_id"));
						paxSeatTO.setPnrPaxFareId(rs.getInt("ppf_id"));
						paxSeatTO.setSelectedFlightSeatId(rs.getInt("seat_id"));
						paxSeatTO.setChgDTO(new ExternalChgDTO());
						paxSeatTO.getChgDTO().setAmount(rs.getBigDecimal("charge"));
						pnrWiseSeats.get(resDTO).add(paxSeatTO);

					}
				}
				return pnrWiseSeats;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getExpiredLccSeatsInfo(String marketingCarrier, Collection<Integer> cancellationIds) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT r.pnr, sm.auto_cancellation_id ");
		query.append(" FROM t_reservation r, t_pnr_passenger pp, t_pnr_segment ps, sm_t_pnr_pax_seg_am_seat sm");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id = sm.pnr_pax_id AND sm.pnr_seg_id = sm.pnr_seg_id");
		query.append(" AND r.status = ? AND pp.status = ? AND ps.status =? AND sm.status <>? AND r.ORIGIN_CHANNEL_CODE = ? ");
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.DUMMY_BOOKING = ? AND sm.auto_cancellation_id IN (");
		query.append(BeanUtils.constructINStringForInts(cancellationIds) + ")");

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL, ReservationInternalConstants.SalesChannel.LCC,
				marketingCarrier, String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Map<String, Integer>) template.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> autoCnxPNRs = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						Integer autoCnxId = rs.getInt("auto_cancellation_id");
						autoCnxPNRs.put(pnr, autoCnxId);
					}
				}

				return autoCnxPNRs;
			}
		});
	}
}
