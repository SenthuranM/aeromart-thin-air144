package com.isa.thinair.airreservation.core.bl.tty;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageIdentifier;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class TransferSegmentMessageCreator extends TypeBReservationMessageCreator {
	
	public TransferSegmentMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForTransfering();
		passengerNamesComposingStrategy = new PassengerNamesComposerForCancellation();
	}

	//createRequest method calls when segment re-protection happens in OC side for GDS/CS pnrs

	@Override
	public String getMessageIdentifier() {
		return MessageIdentifier.SCHEDULE_CHANGE.getCode();
	}
	
	@Override
	public String getCSMessageIdentifier() {
		return MessageIdentifier.SCHEDULE_CHANGE.getCode();
	}
	
}
