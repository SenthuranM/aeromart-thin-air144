/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservarionPaxFareSegDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * ReservarionPaxFareDAOImpl is the business DAO hibernate implmentation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservarionPaxFareSegDAO"
 */
public class ReservationPaxFareSegDAOImpl extends PlatformHibernateDaoSupport implements ReservarionPaxFareSegDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationPaxFareSegDAOImpl.class);

	/**
	 * Saves reservation passenger fare segments
	 * 
	 * @param pnrPaxFare
	 */
	public void savePaxFareSegments(Collection<ReservationPaxFareSegment> reservationPaxFareSegments) {
		log.debug("Inside savePaxFareSegments");

		// Saving the information
		hibernateSaveOrUpdateAll(reservationPaxFareSegments);

		log.debug("Exit savePaxFareSegments");
	}

	/**
	 * Update reservation passenger fare segments with pnl status and goupd id if any
	 * 
	 * @param pnrPaxFare
	 */
	public void updatePaxFareSegments(Collection<Integer> paxFareSegmentsWithoutGroupId,
			Map<String, Collection<Integer>> paxFareSegmentsWithGroupIdMap, String status)  {
		log.debug("Inside updatePaxFareSegmentsJDBC");
		String sql = "";
		sql = " update T_PNR_PAX_FARE_SEGMENT set PNL_STAT = '" + status + "', VERSION = VERSION + 1 " + " where PPFS_ID IN ("
				+ Util.buildIntegerInClauseContent(paxFareSegmentsWithoutGroupId) + ") ";
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		// update records with no group ids
		jt.update(sql);

		for (String groupId : paxFareSegmentsWithGroupIdMap.keySet()) {
			if (groupId != null) {
				sql = " update T_PNR_PAX_FARE_SEGMENT set PNL_STAT = '" + status + "', VERSION = VERSION + 1 " + ", GROUP_ID='"
						+ groupId + "' where PPFS_ID IN ("
						+ Util.buildIntegerInClauseContent(paxFareSegmentsWithGroupIdMap.get(groupId)) + ") ";

				// update records with group ids
				jt.update(sql);
			}
		}
	}



	public Collection<ReservationPaxFareSegment> getReservationPaxFareSegmentsList(
			Collection<Integer> pnrPaxFareSegIds) {

		String inStr = PlatformUtiltiies
				.constructINStringForCharactors(pnrPaxFareSegIds);

		Collection<ReservationPaxFareSegment> resPaxFareSegments = null;
		if (inStr != null && !inStr.isEmpty()) {
			resPaxFareSegments = find("from ReservationPaxFareSegment r "
					+ "where r.pnrPaxFareSegId in (" + inStr + ")",
					ReservationPaxFareSegment.class);

		}

		return resPaxFareSegments;

	}

	public void removePnrPaxFareSegments(Collection<Integer> pnrPaxFareSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "DELETE FROM T_PNR_PAX_FARE_SEGMENT WHERE PPFS_ID IN ("
				+ BeanUtils.constructINStringForInts(pnrPaxFareSegIds) + ") ";

		jt.update(sql);
	}
	
	public ReservationPaxFareSegment getReservationPaxFareSegment(Integer pnrPaxFareSegId) {
		return (ReservationPaxFareSegment)get(ReservationPaxFareSegment.class, pnrPaxFareSegId);
	}	
	
	@Override
	public boolean isPaxAvailableFor(Integer pnrPaxId, Integer pnrSegId) {
		String sql = "SELECT count(*) " 
				+ " FROM T_PNR_PAX_FARE paxFare, "
				+ " T_PNR_PAX_FARE_SEGMENT fareSeg "
				+ " WHERE paxFare.PPF_ID = fareSeg.PPF_ID "
				+ " and fareSeg.PNR_SEG_ID = ? "
				+ " and paxFare.PNR_PAX_ID= ? "
				+ " and fareSeg.PNL_STAT <> 'N' ";	
		int passengerCount = 0;	
		
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		passengerCount = jt.queryForObject(sql, new Object[]{pnrSegId,pnrPaxId}, Integer.class);
   
		if(passengerCount > 0){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isSegmentPaxSentWithPNLorADL(Integer pnrSegmentId) {
		
		String sql = "SELECT count(*) FROM T_PNR_PAX_FARE_SEGMENT where PNL_STAT = 'N' AND PNR_SEG_ID = ?" ;
		int notSentCount = 0;	
		
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		notSentCount = jt.queryForObject(sql, new Object[]{pnrSegmentId}, Integer.class);
   
		if(notSentCount > 0){
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean isPaxAvailableForCalDelete(Integer pnrPaxId, Integer pnrSegId) {
		/*String sql = "SELECT count(*) " 
				+ " FROM T_PNR_PAX_FARE paxFare, "
				+ " T_PNR_PAX_FARE_SEGMENT fareSeg "
				+ " WHERE paxFare.PPF_ID = fareSeg.PPF_ID "
				+ " and fareSeg.PNR_SEG_ID = ? "
				+ " and paxFare.PNR_PAX_ID= ? "
				+ " and fareSeg.CAL_STAT <> 'N' ";	*/
		String sql = "SELECT count(*) FROM T_PNR_PAX_FARE_SEGMENT where CAL_STAT <> 'N' AND PNR_SEG_ID = ?" ;
		int passengerCount = 0;	
		
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		passengerCount = jt.queryForObject(sql, new Object[]{pnrSegId}, Integer.class);
   
		if(passengerCount > 0){
			return true;
		} else {
			return false;
		}
	}

}
