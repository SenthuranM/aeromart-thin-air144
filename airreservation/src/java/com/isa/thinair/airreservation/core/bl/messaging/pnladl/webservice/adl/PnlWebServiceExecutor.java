/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.adl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlight;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegment;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegmentMapElement;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassenger;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassportInformation;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSVisaInformation;
import com.isa.connectivity.profiles.dcs.v1.common.dto.PassengersMapElement;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRS;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airreservation.api.dto.PNLADLDestinationDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLPassengerData;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PNLMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.BaseWebserviceExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.pnl.AdlWebserviceExecutor;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.PassengerType;

/**
 * @author udithad
 *
 */
public class PnlWebServiceExecutor extends BaseWebserviceExecutor {

	private static Log log = LogFactory.getLog(PnlWebServiceExecutor.class);
	private BaseDataContext baseContext;
	ETicketDAO eTicketDAO;

	public PnlWebServiceExecutor() {
		this.eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
	}

	@Override
	public boolean executewebService(BaseDataContext baseContext)
			throws ModuleException {

		this.baseContext = baseContext;
		return adlServiceCaller();
		
	}

	private boolean adlServiceCaller() throws ModuleException {
		boolean isInvokedSuccess = false;

		isInvokedSuccess = sendPNLToDCS(getAllPassengers(baseContext
				.getPassengerCollection()));

		return isInvokedSuccess;
	}

	private List<PNLADLDestinationDTO> getAllPassengers(
			PassengerCollection passengerCollection) {
		List<PassengerInformation> passengerInformations = null;
		PNLADLDestinationDTO destinationDTO = null;
		List<PNLADLDestinationDTO> destinationDTOs = new ArrayList<PNLADLDestinationDTO>();

		for (Map.Entry<String, List<DestinationFare>> entry : passengerCollection
				.getPnlAdlDestinationMap().entrySet()) {
			for (DestinationFare destinationFare : entry.getValue()) {
				destinationDTO = new PNLADLDestinationDTO();
				passengerInformations = new ArrayList<PassengerInformation>();
				addPassengers(destinationFare.getAddPassengers(),
						passengerInformations);
				destinationDTO
						.setPassenger(createDestinationDTOs(passengerInformations));
				destinationDTO.setDestinationAirportCode(destinationFare.getDestinationAirportCode());
				
				if (baseContext != null
						&& baseContext.getPassengerCollection() != null &&
						baseContext.getPassengerCollection().getRbdFareCabinMap() != null){
					Map<String, List<String>> map = baseContext.getPassengerCollection().getRbdFareCabinMap();
					if(map != null){
						for (Map.Entry<String, List<String>> rbdEntry : map
								.entrySet()) {
							if (rbdEntry.getValue() != null) {
								for (String fareClass : rbdEntry.getValue()) {
									if (fareClass != null
											&& !fareClass.isEmpty()
											&& fareClass.equals(destinationFare
													.getFareClass())) {
										destinationDTO
												.setCabinClassCode(rbdEntry
														.getKey());
										
										break;
									}
								}
							}
						}
					}
					
				}	
				destinationDTOs.add(destinationDTO);
			}
		}
		return destinationDTOs;
	}

	private void addPassengers(
			Map<String, List<PassengerInformation>> passengerCollection,
			List<PassengerInformation> addedPassengers) {
		for (Map.Entry<String, List<PassengerInformation>> entry : passengerCollection
				.entrySet()) {
			addedPassengers.addAll(entry.getValue());
		}
	}

	private Collection<ReservationPaxDetailsDTO> createDestinationDTOs(
			List<PassengerInformation> passengerInformations) {

		Collection<ReservationPaxDetailsDTO> paxDetailsDTOs = new ArrayList<ReservationPaxDetailsDTO>();
		for (PassengerInformation passengerInformation : passengerInformations) {
			paxDetailsDTOs.add(createDestinationDTOBy(passengerInformation));
		}

		return paxDetailsDTOs;
	}

	private ReservationPaxDetailsDTO createDestinationDTOBy(
			PassengerInformation passengerInformation) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"dd-MM-yyyy-HH-mm-ss");
		ReservationPaxDetailsDTO detailsDTO = new ReservationPaxDetailsDTO();
		detailsDTO.setTitle(passengerInformation.getTitle());
		detailsDTO.setFirstName(passengerInformation.getFirstName());
		detailsDTO.setLastName(passengerInformation.getLastName());
		detailsDTO.setGender(passengerInformation.getGender());
		detailsDTO.setCcDigits(passengerInformation.getCcDigits());
		detailsDTO.setDateOfBirth(passengerInformation.getDob());
		detailsDTO.setDepartureIntlFlightNo(baseContext.getFlightNumber());
		detailsDTO.setNationalityIsoCode(passengerInformation.getNationality());
		detailsDTO.setEticketNumber(passengerInformation.getEticketNumber());
		detailsDTO.setCouponNumber(passengerInformation.getCoupon().toString());
		if (passengerInformation.getFoidExpiry() != null) {
			detailsDTO.setFoidExpiry(simpleDateFormat
					.format(passengerInformation.getFoidExpiry()));
		}
		detailsDTO.setFoidNumber(passengerInformation.getFoidNumber());
		detailsDTO.setFoidPlace(passengerInformation.getFoidIssuedCountry());
		detailsDTO.setIDPassenger(passengerInformation.isStandByPassenger());
		detailsDTO.setPnr(passengerInformation.getPnr());
		detailsDTO.setPaxType(passengerInformation.getPaxType());
		detailsDTO.setPlaceOfBirth(passengerInformation.getPlaceOfBirth());
		detailsDTO.setPnrPaxId(passengerInformation.getPnrPaxId());
		detailsDTO.setPnrSegId(passengerInformation.getPnrSegId());
		detailsDTO.setPnrStatus(passengerInformation.getPnrStatus());
		detailsDTO.setTravelDocumentType(passengerInformation
				.getTravelDocumentType());
		detailsDTO.setVisaApplicableCountry(passengerInformation
				.getVisaApplicableCountry());
		if (passengerInformation.getVisaDocIssueDate() != null) {
			detailsDTO.setVisaDocIssueDate(simpleDateFormat
					.format(passengerInformation.getVisaDocIssueDate()));
		}
		detailsDTO.setVisaDocNumber(passengerInformation.getVisaDocNumber());
		detailsDTO.setVisaDocPlaceOfIssue(passengerInformation
				.getVisaDocPlaceOfIssue());
		detailsDTO.setWaitingListPax(passengerInformation
				.isWaitListedPassenger());

		if (passengerInformation.getPaxType().equals(PassengerType.CHILD)) {
			if (passengerInformation.getDob() != null) {
				detailsDTO.setChildDob(simpleDateFormat
						.format(passengerInformation.getDob()));
				detailsDTO.setChildFirstName(passengerInformation
						.getFirstName());
				detailsDTO.setChildLastName(passengerInformation.getLastName());
				detailsDTO.setChildFoid(passengerInformation.getFoidNumber());
				detailsDTO.setChildTitle(passengerInformation.getTitle());
			}
		}

		return detailsDTO;

	}

	private boolean sendPNLToDCS(Collection<PNLADLDestinationDTO> pdlc)
			throws ModuleException {
		boolean isWebServiceSuccess = false;
		DCSPNLInfoRQ pnlInfo = new DCSPNLInfoRQ();

		DCSFlight dcsFlight = new DCSFlight();
		DCSPNLInfoRS dcsPNLInfoRS = null;
		try {
			dcsFlight.setDepartureAirportCode(baseContext.getDepartureAirportCode());
			dcsFlight.setFlightNumber(baseContext.getFlightNumber());
			dcsFlight.setDepartureTime(baseContext.getFlightLocalDate());
			dcsFlight.setOriginAirportCode(baseContext
					.getFlightOriginAirportCode());
			pnlInfo.setFlightInfo(dcsFlight);

			this.compileFlightSegmentDetails(pdlc, pnlInfo,
					baseContext.getDepartureAirportCode(),
					baseContext.getFlightId());
			dcsPNLInfoRS = ReservationModuleUtils.getDCSClientBD()
					.sendPNLToDCS(pnlInfo);
		} catch (Exception ex) {
			log.error("PNL WEBSERVICE FAILED." + ex.getMessage());
			isWebServiceSuccess = false;
		}

		if (dcsPNLInfoRS != null) {
			if (dcsPNLInfoRS.getResponseAttributes().getSuccess() != null) {
				isWebServiceSuccess = true;
			}
		}

		return isWebServiceSuccess;
	}

	private void compileFlightSegmentDetails(
			Collection<PNLADLDestinationDTO> pdlc, DCSPNLInfoRQ pnlInfo,
			String depAirportCode, int flightId) throws ModuleException {
		Collection<ReservationPaxDetailsDTO> pnlCollectionTobeUpdated = null;
		// Loading meal details
		Map<Integer, String> pnrPaxIdMealMap = PnlAdlUtil.getMealsDetails(pdlc,
				true);
		// Loading setMap details
		Map<Integer, String> pnrPaxIdSeatMap = PnlAdlUtil.getSeatMapDetails(
				pdlc, true);
		// Loading ssr details
		Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxIdSSRMap = PnlAdlUtil
				.getSSRDetails(pdlc, true);
		// Loading baggage details
		Map<Integer, String> pnrPaxIdBaggageMap = PnlAdlUtil
				.getBaggagesWeights(pdlc, true, depAirportCode);

		for (PNLADLDestinationDTO pnlAdlDto : pdlc) {
			Map<String, List<ReservationPaxDetailsDTO>> pnrPaxMap = new HashMap<String, List<ReservationPaxDetailsDTO>>();
			DCSFlightSegmentMapElement dfsme = new DCSFlightSegmentMapElement();
			if (pnlAdlDto.getCabinClassCode() != null
					&& pnlAdlDto.getCabinClassCode().length() > 0) {
				dfsme.setCabinClassCode(pnlAdlDto.getCabinClassCode());

				DCSFlightSegment dfs = new DCSFlightSegment();

				dfs.setSegmentCode(PnlAdlUtil.findMatchingFlightSegmentcode(
						flightId, depAirportCode,
						pnlAdlDto.getDestinationAirportCode()));
				for (ReservationPaxDetailsDTO rpddto : pnlAdlDto.getPassenger()) {
					if (pnrPaxMap.get(rpddto.getPnr()) == null) {
						List<ReservationPaxDetailsDTO> pnrPax = new ArrayList<ReservationPaxDetailsDTO>();
						pnrPax.add(rpddto);
						pnrPaxMap.put(rpddto.getPnr(), pnrPax);

					} else {
						pnrPaxMap.get(rpddto.getPnr()).add(rpddto);
					}
				}
				for (String pnr : pnrPaxMap.keySet()) {
					PassengersMapElement pme = new PassengersMapElement();
					pme.setPnr(pnr);
					List<ReservationPaxDetailsDTO> rpddtos = pnrPaxMap.get(pnr);
					if (pnlCollectionTobeUpdated == null) {
						pnlCollectionTobeUpdated = new ArrayList<ReservationPaxDetailsDTO>();

					}
					pnlCollectionTobeUpdated.addAll(rpddtos);
					// load first pax details to load cc Details
					ReservationPaxDetailsDTO ccDetails = (ReservationPaxDetailsDTO) BeanUtils
							.getFirstElement(rpddtos);
					if (ccDetails.getCcDigits() != null
							&& !ccDetails.getCcDigits().trim().equals("")) {
						pme.setCcDigits("CC/XXXX-XXXX-XXXX-"
								+ ccDetails.getCcDigits());
					}

					for (ReservationPaxDetailsDTO rpddto : rpddtos) {
						DCSPassenger dp = new DCSPassenger();
						dp.setTitle(rpddto.getTitle());
						dp.setFirstName(rpddto.getFirstName());
						dp.setLastName(rpddto.getLastName());
						dp.setPaxType(rpddto.getPaxType());
						dp.setGroupID(rpddto.getGroupId());
						dp.setStandByPax(rpddto.isIDPassenger());
						dp.setNationality(rpddto.getNationalityIsoCode());
						dp.setETicket(rpddto.getEticketNumber());
						dp.setCouponNumber(rpddto.getCouponNumber());

						DCSPassportInformation dpi = new DCSPassportInformation();
						if (rpddto.getFoidPlace() != null) {
							dpi.setIssuedCountry(rpddto.getFoidPlace());
						}
						if (rpddto.getFoidExpiry() != null) {
							try {
								DateFormat formatter = new SimpleDateFormat(
										"dd-MM-yyyy-HH-mm-ss");
								dpi.setPassportExpiryDate(formatter
										.parse(rpddto.getFoidExpiry()));
							} catch (Exception e) {

							}
						}
						if (rpddto.getFoidNumber() != null) {
							dpi.setPassportNumber(rpddto.getFoidNumber());
						}
						dp.setPassportInfo(dpi);

						if (rpddto.getDateOfBirth() != null) {
							dp.setDateOfBirth(rpddto.getDateOfBirth());
						}

						DCSVisaInformation dvi = new DCSVisaInformation();
						if (rpddto.getVisaDocNumber() != null) {
							dvi.setVisaDocNumber(rpddto.getVisaDocNumber());

							if (rpddto.getPlaceOfBirth() != null) {
								dvi.setPlaceOfBirth(rpddto.getPlaceOfBirth());
							}
							if (rpddto.getVisaDocPlaceOfIssue() != null) {
								dvi.setVisaDocPlaceOfIssue(rpddto
										.getVisaDocPlaceOfIssue());
							}
							if (rpddto.getVisaApplicableCountry() != null) {
								dvi.setVisaApplicableCountry(rpddto
										.getVisaApplicableCountry());
							}
							if (rpddto.getVisaDocIssueDate() != null) {
								try {
									SimpleDateFormat sf = new SimpleDateFormat(
											"ddMMMyy");
									dvi.setVisaDocIssueDate(sf.parse(rpddto
											.getVisaDocIssueDate()));
								} catch (Exception e) {
								}
							}
							if (rpddto.getTravelDocumentType() != null) {
								dvi.setTravelDocumentType(rpddto
										.getTravelDocumentType());
							}
						}
						dp.setVisaInfo(dvi);

						ReservationPax rpa = ReservationDAOUtils.DAOInstance.PASSENGER_DAO
								.getPassenger(rpddto.getPnrPaxId(), false,
										false, false);
						Set<ReservationPax> cset = rpa.getInfants();
						Iterator<ReservationPax> ite = cset.iterator();
						while (ite.hasNext()) {
							ReservationPax infant = (ite.next());
							ReservationPaxAdditionalInfo paxAddnInfo = (infant.getPaxAdditionalInfo() == null)
									? new ReservationPaxAdditionalInfo()
									: infant.getPaxAdditionalInfo();
							DCSPassenger dpInfant = new DCSPassenger();
							dpInfant.setTitle(infant.getTitle());
							dpInfant.setFirstName(infant.getFirstName() != null ? BeanUtils
									.removeSpaces(infant.getFirstName()) : "");
							dpInfant.setLastName(infant.getLastName() != null ? BeanUtils
									.removeSpaces(infant.getLastName()) : "");
							dpInfant.setPaxType(infant.getPaxType());
							dpInfant.setGroupID(rpddto.getGroupId());
							dpInfant.setDateOfBirth(infant.getDateOfBirth());
							EticketTO eTicket = eTicketDAO
									.getPassengerETicketDetail(
											infant.getPnrPaxId(), flightId);
							if (eTicket != null) {
								dpInfant.setETicket(eTicket.getEticketNumber());
							}
							if (eTicket.getCouponNo() !=null){
								dpInfant.setCouponNumber(String.valueOf(eTicket.getCouponNo()));
							}
							if (infant.getNationalityCode() != null) {
								Nationality nat = ReservationModuleUtils
										.getCommonMasterBD().getNationality(
												infant.getNationalityCode());
								if (nat != null && nat.getIsoCode() != null) {
									dpInfant.setNationality(nat.getIsoCode());
								}
							}
							DCSPassportInformation dpinfo = new DCSPassportInformation();
							if (paxAddnInfo.getPassportIssuedCntry() != null) {
								dpinfo.setIssuedCountry(paxAddnInfo.getPassportIssuedCntry());
							}
							if (paxAddnInfo.getPassportExpiry() != null) {
								dpinfo.setPassportExpiryDate((paxAddnInfo.getPassportExpiry()));
							}
							if (paxAddnInfo.getPassportNo() != null) {
								dpinfo.setPassportNumber(paxAddnInfo.getPassportNo());
							}
							dpInfant.setPassportInfo(dpinfo);
							dp.setInfant(dpInfant);
						}
						PnlAdlUtil.compileMealDetails(pnrPaxIdMealMap,
								rpddto.getPnrPaxId(), dp);
						PnlAdlUtil.compileSeatDetails(pnrPaxIdSeatMap,
								rpddto.getPnrPaxId(), dp);
						PnlAdlUtil.compileBaggageDetails(pnrPaxIdBaggageMap,
								rpddto.getPnrPaxId(), dp);
						PnlAdlUtil.compileSsrDetails(pnrPaxIdSSRMap,
								rpddto.getPnrPaxId(), dp);
						pme.getPassengers().add(dp);
					}
					dfs.getPnrAndPassengers().add(pme);
				}
				dfsme.getFlightSegments().add(dfs);

				pnlInfo.getSegmentInfo().add(dfsme);
			}
		}
	}

}
