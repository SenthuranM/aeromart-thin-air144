/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ChildElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.MealElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.SSRElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.SeatElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class SeatElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> seatElementRuleExecutor = new SeatElementRuleExecutor();

	private boolean isStartWithNewLine = false;

	@Override
	public void buildElement(ElementContext context) {
		if (context != null && context.getFeaturePack() != null) {
			initContextData(context);
			if (context.getFeaturePack().isShowSeatMap()) {
				seatElementTemplate();
			}
		}
		executeNext();
	}

	private void seatElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation != null
					&& passengerInformation.getSeats() != null) {
				List<AncillaryDTO> seats = passengerInformation.getSeats();
				if (seats != null) {
					boolean isFirstSeat = true;
					for (AncillaryDTO anci : seats) {
						buildMealElement(elementTemplate, passengerInformation,
								anci, isFirstSeat);
						currentElement = elementTemplate.toString();
						if (currentElement != null && !currentElement.isEmpty()) {
							ammendmentPreValidation(isStartWithNewLine, currentElement, uPContext, seatElementRuleExecutor);
							isFirstSeat = false;
						}

					}

				}
			}
		}
	}

	private void buildMealElement(StringBuilder elementTemplate,
			PassengerInformation passengerInformation, AncillaryDTO anciliary,
			boolean isFirstSeat) {
		elementTemplate.setLength(0);
		if (isFirstSeat) {
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.RQST);
			elementTemplate.append(space());
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
			elementTemplate.append(space());
			elementTemplate.append(anciliary.getDescription());
			if (passengerInformations.size() > 1) {
				elementTemplate.append(hyphen());
				elementTemplate.append(1);
				elementTemplate.append(passengerInformation.getLastName());
				elementTemplate.append(forwardSlash());
				elementTemplate.append(passengerInformation.getFirstName());
				elementTemplate.append(passengerInformation.getTitle());
			}

		} else {
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.EXST);
			elementTemplate.append(MessageComposerConstants.SP);
			elementTemplate
					.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
			elementTemplate.append(MessageComposerConstants.SP);
			elementTemplate.append(anciliary.getDescription());
			if (passengerInformations.size() > 1) {
				elementTemplate.append("-1");
				elementTemplate.append(passengerInformation.getLastName());
				elementTemplate.append(MessageComposerConstants.FWD);
				elementTemplate.append(passengerInformation.getFirstName());
				elementTemplate.append(passengerInformation.getTitle());
			}

		}

	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
