package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.util.Collection;

import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Nilindra Fernando
 * 
 * @since August 5, 2010
 */
public class TnxGranularityRules {

	public static Collection<String> getPaymentChargeGroupFulFillmentOrder() {
		return AppSysParamsUtil.getChargeGroupFulFillmentOrderForPayment();
	}

	public static Collection<String> getRefundChargeGroupFulFillmentOrder() {
		return AppSysParamsUtil.getChargeGroupFulFillmentOrderForRefund();
	}

	public static Collection<String> getCreditCarryForwardChargeGroupFulFillmentOrder() {
		return AppSysParamsUtil.getChargeGroupFulFillmentOrderForPayment();
	}

	public static Collection<ReservationCredit> getCreditsUtilizationPolicies(String pnrPaxId) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.getReservationCreditsOrderByExpiryDate(pnrPaxId, true);
	}

	public static Collection<ReservationCredit> getCreditsCarryForwardPolicies(String pnrPaxId) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.getReservationCreditsOrderByExpiryDate(pnrPaxId, false);
	}

}
