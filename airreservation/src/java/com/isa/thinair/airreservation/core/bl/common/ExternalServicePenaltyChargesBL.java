/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServicePenaltyExtChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Reservation Ancillary related penalty charges captured as external charges business logic implementation
 * 
 * @author rumesh
 */
public class ExternalServicePenaltyChargesBL {

	private static List<EXTERNAL_CHARGES> penaltyExternalCharges = Arrays.asList(EXTERNAL_CHARGES.ANCI_PENALTY);

	/**
	 * Hide the constructor
	 */
	private ExternalServicePenaltyChargesBL() {

	}

	/**
	 * Reflect external charges for a payment booking
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	public static void reflectExternalChgForAPaymentBooking(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPayment, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {
		// Apply these changes for the reservation
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForAPaymentBooking(reservation,
				passengerPayment, credentialsDTO, chgTnxGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, passengerPayment);
	}

	/**
	 * Apply external charges for a payment booking
	 * 
	 * @param reservation
	 * @param passengerPayment
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForAPaymentBooking(Reservation reservation,
			Map<Integer, PaymentAssembler> passengerPayment, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTnxGen)
			throws ModuleException {

		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();
		Map<Integer, ReservationSegmentDTO> mapPnrSegIdAndReservationSegDTO = ExternalGenericChargesBL
				.getSegmentDetailInformation(reservation.getPnr());
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<Integer> colActiveONDPnrPaxFareIds;
		Collection<ExternalChgDTO> ccExternalChgs;
		boolean updateExist = false;
		boolean skipFlownCheck = false;
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		Collection<ReservationAudit> resModificationDetails = new ArrayList<ReservationAudit>();

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (passengerPayment.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(reservationPax.getPnrPaxId());

				List<ReservationPaxOndCharge> arrChargePerPax = null;
				for (EXTERNAL_CHARGES taxExternalChg : penaltyExternalCharges) {
					skipFlownCheck = (taxExternalChg == EXTERNAL_CHARGES.ANCI_PENALTY) ? true : false;
					ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(taxExternalChg);

					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
						ServicePenaltyExtChgDTO servicePenaltyExtChgDTO = (ServicePenaltyExtChgDTO) externalChgDTO;
						Integer flightSegId = FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(servicePenaltyExtChgDTO.getFlightRefNumber());
						colActiveONDPnrPaxFareIds = ExternalGenericChargesBL.getApplicableONDPnrPaxFareIds(currentDate,
								reservationPax.getPnrPaxFares(), mapPnrSegIdAndReservationSegDTO, reservationPax.geteTickets(),
								skipFlownCheck);

						reservationPaxFare = getReservationPaxFare(reservationPax.getPnrPaxFares(), flightSegId);
						// Only adding if it's an active OND
						if (colActiveONDPnrPaxFareIds.contains(reservationPaxFare.getPnrPaxFareId())) {
							ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(
									servicePenaltyExtChgDTO.getAmount(), null, externalChgDTO.getChgRateId(),
									externalChgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, null, null, null,
									chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));
							if (arrChargePerPax == null) {
								arrChargePerPax = new ArrayList<ReservationPaxOndCharge>();
							}
							arrChargePerPax.add(reservationPaxOndCharge);
							updateExist = true;
						}

						// if ANCI_PENALTY
						if (skipFlownCheck) {
							ReservationAudit ra = new ReservationAudit();
							ReservationAudit.createReservationAudit(ra, reservation.getPnr(), "", credentialsDTO);
							ra.setModificationType(AuditTemplateEnum.ANCI_PENALTY_CHARGE.getCode());
							ra.addContentMap(
									AuditTemplateEnum.TemplateParams.AncillaryModificationPenalty.ANCILLARY_MODIFICATION_PENALTY,
									externalChgDTO.getChargeDescription());
							ra.addContentMap(
									AuditTemplateEnum.TemplateParams.AncillaryModificationPenalty.ANCILLARY_PENALTY_AMOUNT,
									externalChgDTO.getAmount().toString());
							resModificationDetails.add(ra);
						}
					}
				}
				mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), arrChargePerPax);

			}
		}

		// audit ANCI_PENALTYs
		for (ReservationAudit reservationAudit : resModificationDetails) {
			auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	/**
	 * Add external charges for payment assembler
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerPayment
	 */
	private static void addExternalChgsForPaymentAssembler(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, PaymentAssembler> passengerPayment) {
		Iterator<Integer> itPnrPaxIds = passengerPayment.keySet().iterator();
		PaymentAssembler paymentAssembler;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(pnrPaxId);
				lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);
				if (lstReservationPaxOndCharge != null) {
					for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
						paymentAssembler.setTotalChargeAmount(AccelAeroCalculator.add(paymentAssembler.getTotalChargeAmount(),
								reservationPaxOndCharge.getAmount()));
					}
				}
			}
		}
	}

	public static ReservationPaxFare getReservationPaxFare(Collection<ReservationPaxFare> pnrPaxFares, Integer flightSegId)
			throws ModuleException {
		Iterator<ReservationPaxFare> itReservationPaxFare = pnrPaxFares.iterator();
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();

			itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

			while (itReservationPaxFareSegment.hasNext()) {
				reservationPaxFareSegment = itReservationPaxFareSegment.next();

				if (reservationPaxFareSegment.getSegment().getFlightSegId().intValue() == flightSegId.intValue()
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationPaxFareSegment.getSegment().getStatus())) {
					return reservationPaxFare;
				}
			}
		}

		throw new ModuleException("airreservations.seatmap.flightsegments.cannot.locate");
	}
}