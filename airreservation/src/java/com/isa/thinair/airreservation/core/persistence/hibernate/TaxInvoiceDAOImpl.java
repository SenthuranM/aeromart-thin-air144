package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.Query;

import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.core.persistence.dao.TaxInvoiceDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import org.hibernate.jdbc.Work;

/**
 * 
 * @author dilan
 * @isa.module.dao-impl dao-name="TaxInvoiceDAO"
 * 
 */
public class TaxInvoiceDAOImpl extends PlatformBaseHibernateDaoSupport implements TaxInvoiceDAO {

	private final Log log = LogFactory.getLog(getClass());

	@Override
	public TaxInvoice saveTaxInvoice(TaxInvoice taxInvoice) {
		hibernateSaveOrUpdate(taxInvoice);
		return taxInvoice;
	}

	@Override
	public List<TaxInvoice> getTaxInvoiceFor(String pnr) {
		return find("SELECT ti FROM TaxInvoice ti join ti.reservations r WHERE r.pnr = ?", pnr, TaxInvoice.class);
	}

	@Override
	public List<TaxInvoice> getTaxInvoiceForPrintInvoice(String pnr) {
		List<TaxInvoice> taxInvoiceList = new ArrayList<TaxInvoice>();
		Iterator queryIterator = getSession()
				.createSQLQuery("select ti.TI_ID, ti.INVOICE_TYPE, ti.TNX_SEQ, ti.DATE_OF_ISSUE, ti.TAX_REG_NO, ti.STATE , "
						+ " ti.TAX1_RATE_ID, ti.TAX1_AMOUNT , ti.TAX2_RATE_ID , ti.TAX2_AMOUNT , ti.TAX3_RATE_ID , ti.TAX3_AMOUNT , "
						+ " ti.ORIGINAL_PNR, ti.ORIGINAL_TI_ID, ti.CURRENCY_CODE, ti.DISCOUNT, ti.TAXABLE_AMOUNT, ti.NON_TAXABLE_AMOUNT ,"
						+ " ti.INVOICE_NUMBER ,"
						+ " oti.invoice_number original_invoice_no, s.state_name, ti.JOURNEY_OND from t_tax_invoice ti, t_tax_invoice oti , t_state s where ti.original_ti_id = oti.ti_id (+) and ti.state = s.state_code and ti.ti_id in (select ti_id from t_pnr_tax_invoice where pnr = ? )")
				.addScalar("TI_ID", Hibernate.INTEGER)
				.addScalar("INVOICE_TYPE", Hibernate.STRING)
				.addScalar("TNX_SEQ", Hibernate.INTEGER)
				.addScalar("DATE_OF_ISSUE", Hibernate.TIMESTAMP)
				.addScalar("TAX_REG_NO", Hibernate.STRING)
				.addScalar("STATE", Hibernate.STRING)
				.addScalar("TAX1_RATE_ID", Hibernate.INTEGER)
				.addScalar("TAX1_AMOUNT", Hibernate.BIG_DECIMAL)
				.addScalar("TAX2_RATE_ID", Hibernate.INTEGER)
				.addScalar("TAX2_AMOUNT", Hibernate.BIG_DECIMAL)
				.addScalar("TAX3_RATE_ID", Hibernate.INTEGER)
				.addScalar("TAX3_AMOUNT", Hibernate.BIG_DECIMAL)
				.addScalar("ORIGINAL_PNR", Hibernate.STRING)
				.addScalar("ORIGINAL_TI_ID", Hibernate.INTEGER)
				.addScalar("CURRENCY_CODE", Hibernate.STRING)
				.addScalar("DISCOUNT", Hibernate.BIG_DECIMAL)
				.addScalar("TAXABLE_AMOUNT", Hibernate.BIG_DECIMAL)
				.addScalar("NON_TAXABLE_AMOUNT", Hibernate.BIG_DECIMAL)
				.addScalar("INVOICE_NUMBER", Hibernate.STRING)
				.addScalar("original_invoice_no", Hibernate.STRING)
				.addScalar("state_name", Hibernate.STRING)
	    	 	.addScalar("JOURNEY_OND", Hibernate.STRING)
				.setParameter(0, pnr)
				.list().iterator();
		while (queryIterator.hasNext()) {
			Object[] taxInvoiceObj = (Object[]) queryIterator.next();

			TaxInvoice taxInvoice = new TaxInvoice();
			taxInvoice.setTaxInvoiceId((Integer) taxInvoiceObj[0]);
			taxInvoice.setInvoiceType((String) taxInvoiceObj[1]);
			taxInvoice.setTnxSeq((Integer) taxInvoiceObj[2]);
			taxInvoice.setDateOfIssue((Date) taxInvoiceObj[3]);
			taxInvoice.setTaxRegistrationNumber((String) taxInvoiceObj[4]);
			taxInvoice.setStateCode((String) taxInvoiceObj[5]);
			taxInvoice.setTaxRate1Id((Integer) taxInvoiceObj[6]);
			taxInvoice.setTaxRate1Amount((BigDecimal) taxInvoiceObj[7]);
			taxInvoice.setTaxRate2Id((Integer) taxInvoiceObj[8]);
			taxInvoice.setTaxRate2Amount((BigDecimal) taxInvoiceObj[9]);
			taxInvoice.setTaxRate3Id((Integer) taxInvoiceObj[10]);
			taxInvoice.setTaxRate3Amount((BigDecimal) taxInvoiceObj[11]);
			taxInvoice.setOriginalPnr((String) taxInvoiceObj[12]);
			taxInvoice.setOriginalTaxInvoiceId((Integer) taxInvoiceObj[13]);
			taxInvoice.setCurrencyCode((String) taxInvoiceObj[14]);
			taxInvoice.setTotalDiscount((BigDecimal) taxInvoiceObj[15]);
			taxInvoice.setTaxableAmount((BigDecimal) taxInvoiceObj[16]);
			taxInvoice.setNonTaxableAmount((BigDecimal) taxInvoiceObj[17]);
			taxInvoice.setTaxInvoiceNumber((String) taxInvoiceObj[18]);
			taxInvoice.setOriginalTaxInvoiceNumber((String) taxInvoiceObj[19]);
			taxInvoice.setJourneyOND((String) taxInvoiceObj[21]);
			//taxInvoice.setStateOfTheRecipient((String) taxInvoiceObj[20]);

			taxInvoiceList.add(taxInvoice);
		}

		return taxInvoiceList;
	}

	@Override
	public TaxInvoice getTaxInvoiceFor(Integer taxInvoiceId) {
		return get(TaxInvoice.class, taxInvoiceId);
	}

	@Override
	public List<TaxInvoice> getTaxInvoiceFor(List<String> invoiceNumbers) {
		List<TaxInvoice> taxInvoiceList = new ArrayList<TaxInvoice>();

		if (invoiceNumbers != null && !invoiceNumbers.isEmpty()) {
			List<Integer> invoiceNumbersInt = new ArrayList<>();

			try {
				for (String invoiceNumberStr : invoiceNumbers)
					invoiceNumbersInt.add(Integer.valueOf(invoiceNumberStr));
			} catch (NumberFormatException nfe) {
				log.error("invalid int value", nfe);
			}

			if (!invoiceNumbersInt.isEmpty()) {
//				Iterator queryIterator = getSession()
//						.createQuery(
//								"SELECT ti.taxInvoiceId, ti.invoiceType, ti.tnxSeq, ti.dateOfIssue, ti.taxRegistrationNumber, ti.stateCode, ti.taxRate1Id, ti.taxRate1Amount, ti.taxRate2Id, ti.taxRate2Amount, ti.taxRate3Id, ti.taxRate3Amount, ti.originalPnr, ti.originalTaxInvoiceId, ti.currencyCode, ti.totalDiscount, ti.taxableAmount, ti.nonTaxableAmount, ti.taxInvoiceNumber, oti.taxInvoiceNumber FROM TaxInvoice ti left join TaxInvoice oti on ti.originalTaxInvoiceId = oti.taxInvoiceId WHERE  ti.taxInvoiceId in (:invoiceNumbersInt)")
//						.setParameterList("invoiceNumbersInt", invoiceNumbersInt).list().iterator();


				Iterator queryIterator = getSession()
						.createSQLQuery("select ti.TI_ID, ti.INVOICE_TYPE, ti.TNX_SEQ, ti.DATE_OF_ISSUE, ti.TAX_REG_NO, ti.STATE , "
								+ " ti.TAX1_RATE_ID, ti.TAX1_AMOUNT , ti.TAX2_RATE_ID , ti.TAX2_AMOUNT , ti.TAX3_RATE_ID , ti.TAX3_AMOUNT , "
								+ " ti.ORIGINAL_PNR, ti.ORIGINAL_TI_ID, ti.CURRENCY_CODE, ti.DISCOUNT, ti.TAXABLE_AMOUNT, ti.NON_TAXABLE_AMOUNT ,"
								+ " ti.INVOICE_NUMBER ,  "
								+ " oti.invoice_number original_invoice_no, s.state_name,ti.JOURNEY_OND from t_tax_invoice ti, t_tax_invoice oti , t_state s where ti.original_ti_id = oti.ti_id (+) and ti.state = s.state_code and  ti.TI_ID in ( ? ) ")
						.addScalar("TI_ID", Hibernate.INTEGER).addScalar("INVOICE_TYPE", Hibernate.STRING)
						.addScalar("TNX_SEQ", Hibernate.INTEGER)
						.addScalar("DATE_OF_ISSUE", Hibernate.TIMESTAMP)
						.addScalar("TAX_REG_NO", Hibernate.STRING)
						.addScalar("STATE", Hibernate.STRING)
						.addScalar("TAX1_RATE_ID", Hibernate.INTEGER)
						.addScalar("TAX1_AMOUNT", Hibernate.BIG_DECIMAL)
						.addScalar("TAX2_RATE_ID", Hibernate.INTEGER)
						.addScalar("TAX2_AMOUNT", Hibernate.BIG_DECIMAL)
						.addScalar("TAX3_RATE_ID", Hibernate.INTEGER)
						.addScalar("TAX3_AMOUNT", Hibernate.BIG_DECIMAL)
						.addScalar("ORIGINAL_PNR", Hibernate.STRING)
						.addScalar("ORIGINAL_TI_ID", Hibernate.INTEGER)
						.addScalar("CURRENCY_CODE", Hibernate.STRING)
						.addScalar("DISCOUNT", Hibernate.BIG_DECIMAL)
						.addScalar("TAXABLE_AMOUNT", Hibernate.BIG_DECIMAL)
						.addScalar("NON_TAXABLE_AMOUNT", Hibernate.BIG_DECIMAL)
						.addScalar("INVOICE_NUMBER", Hibernate.STRING)
						.addScalar("original_invoice_no", Hibernate.STRING)
						.addScalar("state_name", Hibernate.STRING)
						.addScalar("JOURNEY_OND", Hibernate.STRING)
						.setParameter(0, BeanUtils.constructINStringForInts(invoiceNumbersInt))
						.list().iterator();

				while (queryIterator.hasNext()) {
					Object[] taxInvoiceObj = (Object[]) queryIterator.next();

					TaxInvoice taxInvoice = new TaxInvoice();
					taxInvoice.setTaxInvoiceId((Integer) taxInvoiceObj[0]);
					taxInvoice.setInvoiceType((String) taxInvoiceObj[1]);
					taxInvoice.setTnxSeq((Integer) taxInvoiceObj[2]);
					taxInvoice.setDateOfIssue((Date) taxInvoiceObj[3]);
					taxInvoice.setTaxRegistrationNumber((String) taxInvoiceObj[4]);
					taxInvoice.setStateCode((String) taxInvoiceObj[5]);
					taxInvoice.setTaxRate1Id((Integer) taxInvoiceObj[6]);
					taxInvoice.setTaxRate1Amount((BigDecimal) taxInvoiceObj[7]);
					taxInvoice.setTaxRate2Id((Integer) taxInvoiceObj[8]);
					taxInvoice.setTaxRate2Amount((BigDecimal) taxInvoiceObj[9]);
					taxInvoice.setTaxRate3Id((Integer) taxInvoiceObj[10]);
					taxInvoice.setTaxRate3Amount((BigDecimal) taxInvoiceObj[11]);
					taxInvoice.setOriginalPnr((String) taxInvoiceObj[12]);
					taxInvoice.setOriginalTaxInvoiceId((Integer) taxInvoiceObj[13]);
					taxInvoice.setCurrencyCode((String) taxInvoiceObj[14]);
					taxInvoice.setTotalDiscount((BigDecimal) taxInvoiceObj[15]);
					taxInvoice.setTaxableAmount((BigDecimal) taxInvoiceObj[16]);
					taxInvoice.setNonTaxableAmount((BigDecimal) taxInvoiceObj[17]);
					taxInvoice.setTaxInvoiceNumber((String) taxInvoiceObj[18]);
					taxInvoice.setOriginalTaxInvoiceNumber((String) taxInvoiceObj[19]);
					taxInvoice.setJourneyOND((String) taxInvoiceObj[21]);
					//taxInvoice.setStateOfTheRecipient((String) taxInvoiceObj[20]);

					taxInvoiceList.add(taxInvoice);
				}
			}
		}

		return taxInvoiceList;
	}

	public Integer getTaxInvoiceNumber(String stateCode, String invoiceType) {

		final AtomicReference<Integer> out = new AtomicReference<>();

		getSession().doWork(new Work() {
			public void execute(Connection connection) throws SQLException {

				CallableStatement function = connection.prepareCall("{? = call f_get_next_state_invoice_no(?, ?)}");
				function.registerOutParameter(1, Types.INTEGER);
				function.setString(2, stateCode);
				function.setString(3, invoiceType);
				function.execute();

				out.set(function.getInt(1));

			}
		});
		return out.get();
	}
}
