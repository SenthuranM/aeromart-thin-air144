package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.util.PFSUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author eric
 */
public class ProcessNoRecrules {

	private Reservation reservation;

	private final PfsPaxEntry pfsParsedEntry;

	private final Collection<FlightReconcileDTO> colFlightReconcileDTO;

	public ProcessNoRecrules(Reservation reservation, PfsPaxEntry pfsParsedEntry,
			Collection<FlightReconcileDTO> colFlightReconcileDTO) throws ModuleException {
		this.reservation = reservation;
		this.pfsParsedEntry = pfsParsedEntry;
		this.colFlightReconcileDTO = colFlightReconcileDTO;
	}

	/**
	 * @throws ModuleException
	 */
	public Reservation process() throws ModuleException {
		// rule 1 . cancel the future segments , if the no rec segment is an international flight
		boolean isProcessed = this.processPFSPassenger();

		if (isProcessed) {
			return this.loadReservation(this.reservation.getPnr());
		} else {
			return reservation;
		}
	}

	/**
	 * @throws ModuleException
	 */
	private boolean processPFSPassenger() throws ModuleException {

		Map<Integer, FlightReconcileDTO> mapPnrSegments = this.getPnrSegmentMap();

		ReservationPax reservationPax = this.getPfsPassenger();

		boolean isProcessed = false;

		// If pnr segments found
		if (mapPnrSegments.size() > 0 && reservationPax != null) {

			isProcessed = true;

			FlightReconcileDTO flightReconcileDTO = mapPnrSegments.values().iterator().next();

			if (flightReconcileDTO.isInternationalFlight()) {
				if (AppSysParamsUtil.isCancelOutOfSequnceSegments()) {
					// international flight segment found
					this.processInternationalSegment(flightReconcileDTO, reservationPax);
				}
			} else {
				if (AppSysParamsUtil.isCancelNoShowSegment()) {
					// cancel the domestic segment
					this.processDomesticSegment(flightReconcileDTO, reservationPax);
				}
			}
		}

		return isProcessed;
	}

	/**
	 * @param flightReconcileDTO
	 * @param reservationPax
	 * @throws ModuleException
	 */
	private void processInternationalSegment(FlightReconcileDTO flightReconcileDTO, ReservationPax reservationPax)
			throws ModuleException {
		int adultCount = this.getTotalPaxCount();
		if (adultCount > 1) {
			Collection<Integer> colPassengers = new ArrayList<Integer>();
			colPassengers.add(reservationPax.getPnrPaxId());
			// split the reservation
			reservation = this.splitReservation(colPassengers);
		}
		Collection<Integer> pnrSegmentIds = this.getFutureSegments(reservation, flightReconcileDTO.getFlightSegId());
		if (!pnrSegmentIds.isEmpty()) {
			this.cancelSegments(reservation, pnrSegmentIds, true);
		}
	}

// following part implemnt by dhy	
	private void processLinkFlighttoBus(FlightReconcileDTO flightReconcileDTO, ReservationPax reservationPax)
			throws ModuleException {
		int adultCount = this.getTotalPaxCount();
		if (adultCount > 1) {
			Collection<Integer> colPassengers = new ArrayList<Integer>();
			colPassengers.add(reservationPax.getPnrPaxId());
			// split the reservation
			reservation = this.splitReservation(colPassengers);
		}
		Collection<Integer> pnrSegmentIds = this.getFutureSegments(reservation, flightReconcileDTO.getFlightSegId());
		if (!pnrSegmentIds.isEmpty()) {
			this.cancelSegments(reservation, pnrSegmentIds, true);
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * @param flightReconcileDTO
	 * @param reservationPax
	 * @throws ModuleException
	 */
	private void processDomesticSegment(FlightReconcileDTO flightReconcileDTO, ReservationPax reservationPax)
			throws ModuleException {

		int adultCount = this.getTotalPaxCount();
		Integer pnrSegId;
		if (adultCount > 1) {
			Collection<Integer> colPassengers = new ArrayList<Integer>();
			colPassengers.add(reservationPax.getPnrPaxId());
			// split the reservation
			reservation = this.splitReservation(colPassengers);
			pnrSegId = this.getPnrSegmentIdOfNoshowSegment(reservation, flightReconcileDTO.getFlightSegId());

		} else {
			pnrSegId = flightReconcileDTO.getPnrSegId();
		}
		// cancel noshow segment
		if (pnrSegId != null) {
			Collection<Integer> pnrSegmentIds = new ArrayList<Integer>();
			pnrSegmentIds.add(pnrSegId);
			this.cancelSegments(reservation, pnrSegmentIds, true);
		}
	}

	/**
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	private Reservation loadReservation(String pnr) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	/**
	 * @param colPassengers
	 * @return
	 * @throws ModuleException
	 */
	private Reservation splitReservation(Collection<Integer> colPassengers) throws ModuleException {

		ServiceResponce splitResponse = ReservationModuleUtils.getReservationBD().splitReservation(pfsParsedEntry.getPnr(),
				colPassengers, reservation.getVersion(), null, null, null,
				"Split due to PFS NO SHOW Update operation with NOSHOW charge", null);

		if (!splitResponse.isSuccess()) {
			throw new ModuleException("airreservations.arg.invalidSplit");
		}

		String splitNewPnr = (String) splitResponse.getResponseParam(CommandParamNames.PNR);
		return loadReservation(splitNewPnr);

	}

	/**
	 * @param newReservation
	 * @param pnrSegmentIds
	 * @throws ModuleException
	 */
	private void cancelSegments(Reservation newReservation, Collection<Integer> pnrSegmentIds, boolean cancelFlownSegments)
			throws ModuleException {

		if (pnrSegmentIds.size() > 0) {
			Collection<Collection<Integer>> colOndGroups = CancellationUtils.getOndWiseGroups(pnrSegmentIds, newReservation);
			if (colOndGroups.size() > 0) {
				Iterator<Collection<Integer>> ondIterator = colOndGroups.iterator();
				CustomChargesTO customChargesTO = new CustomChargesTO(AccelAeroCalculator.getDefaultBigDecimalZero(),
						AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero(), null,
						null, null, null, null, null, true);
				long version = newReservation.getVersion();

				boolean isOriginChannelWebAndrioidOrIOS = SalesChannelsUtil.isSalesChannelWebOrMobile(reservation.getAdminInfo().getOriginChannelId());

				boolean isOwnerChannelWebAndrioidOrIOS = SalesChannelsUtil.isSalesChannelWebOrMobile(reservation.getAdminInfo().getOwnerChannelId());
				
				while (ondIterator.hasNext()) {
					Collection<Integer> pnrSegIds = ondIterator.next();
					ServiceResponce cancelSegmentResponse = ReservationModuleUtils.getSegmentBD().cancelSegments(
							newReservation.getPnr(), pnrSegIds, customChargesTO, version, cancelFlownSegments, null, false,
							"Cancel segment due to PFS NO SHOW Update operation", false);
					if (!cancelSegmentResponse.isSuccess()) {
						throw new ModuleException("airreservations.arg.invalidCancelSegment");
					}

					if (AppSysParamsUtil.isAllowNOSHOPaxAutoRefundForIBEBookings()
							&& AppSysParamsUtil.isIBEPaxCreditAutoRefundEnabled()) {
						if (cancelSegmentResponse.isSuccess() && reservation != null && reservation.getAdminInfo() != null
								&& (isOriginChannelWebAndrioidOrIOS) && (isOwnerChannelWebAndrioidOrIOS)) {
							this.pfsParsedEntry.setCheckCreditAutoRefund(true);
						}
					}

					version = (Long) cancelSegmentResponse.getResponseParam(CommandParamNames.VERSION);
				}
			}
		}
	}

	/**
	 * @param newReservation
	 * @param flightSegId
	 * @return
	 */
	private Integer getPnrSegmentIdOfNoshowSegment(Reservation newReservation, int flightSegId) {

		Iterator<ReservationSegmentDTO> segemntIte = newReservation.getSegmentsView().iterator();

		while (segemntIte.hasNext()) {
			ReservationSegmentDTO segment = segemntIte.next();
			if (segment.getFlightSegId().equals(flightSegId)
					&& !segment.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
				return segment.getPnrSegId();
			}
		}
		return null;

	}

	/**
	 * @param newReservation
	 * @param flightSegId
	 * @return
	 */
	private Collection<Integer> getFutureSegments(Reservation newReservation, int flightSegId) {

		List<ReservationSegmentDTO> segmentView = new ArrayList<ReservationSegmentDTO>(newReservation.getSegmentsView());
		Collections.sort(segmentView);
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		Collection<Integer> pnrSegmentIds = new ArrayList<Integer>();
		boolean skipSegment = true;
		Integer norecSegFareGroupId = null;		
		for (ReservationSegmentDTO segment : segmentView) {
			if (segment.getFlightSegId().equals(flightSegId)) {
				skipSegment = false;
				norecSegFareGroupId = segment.getFareGroupId();
				continue;
			}
			if (!skipSegment
					&& (segment.getZuluDepartureDate().compareTo(currentDate) > 0 || segment.getFareGroupId().equals(
							norecSegFareGroupId))
					&& !segment.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
				pnrSegmentIds.add(segment.getPnrSegId());

				if (segment.getFareGroupId().equals(norecSegFareGroupId)) {
					for (ReservationSegmentDTO seg : segmentView) {
						if (seg.getFlightSegId().equals(flightSegId)
								&& !seg.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
							pnrSegmentIds.add(seg.getPnrSegId());
						}
					}
				}
			}
		}
		return pnrSegmentIds;
	}

	/**
	 * @return
	 */
	private ReservationPax getPfsPassenger() {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax = null;
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			if (PFSUtil.isPaxFound(reservationPax, pfsParsedEntry)
					&& !reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				return reservationPax;
			}
		}
		return null;
	}

	/**
	 * @return total pax count (Adult + Children)
	 */
	private int getTotalPaxCount() {
		return reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
	}

	/**
	 * @return
	 */
	private Map<Integer, FlightReconcileDTO> getPnrSegmentMap() {
		Iterator<FlightReconcileDTO> itColFlightReconcileDTO = colFlightReconcileDTO.iterator();
		FlightReconcileDTO flightReconcileDTO;
		Map<Integer, FlightReconcileDTO> mapPnrSegId = new HashMap<Integer, FlightReconcileDTO>();

		while (itColFlightReconcileDTO.hasNext()) {
			flightReconcileDTO = itColFlightReconcileDTO.next();

			// Since go shore reservations are there this can come with ""
			if (flightReconcileDTO.getPnr() != null && !flightReconcileDTO.getPnr().equals("")) {
				if (BeanUtils.nullHandler(flightReconcileDTO.getPnr()).equals(BeanUtils.nullHandler(pfsParsedEntry.getPnr()))) {
					mapPnrSegId.put(flightReconcileDTO.getPnrSegId(), flightReconcileDTO);
				}
			}
		}
		return mapPnrSegId;
	}
}