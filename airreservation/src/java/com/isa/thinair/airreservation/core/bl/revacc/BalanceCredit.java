/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFullFillmentDeriverBO;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityRules;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * commenad to debit credit
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="balanceCredit"
 */
public class BalanceCredit extends DefaultBaseCommand {

	// Dao's
	private ReservationCreditDAO reservationCreditDao;

	private ReservationTnxDAO reservationTnxDao;

	/**
	 * constructor of the BalanceCredit command
	 */
	public BalanceCredit() {
		// looking up daos
		reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	/**
	 * execute method of the BalanceCredit command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = (Collection<ReservationPaxOndPayment>) this
				.getParameter(CommandParamNames.REFUNDABLE_CHARGES);
		
		Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx = (HashMap<Long, Map<String, BigDecimal>>) this
				.getParameter(CommandParamNames.PAX_PAY_CREDIT_UTILIZED_MAP_BY_TNX);

		// checking params
		this.checkParams(pnrPaxId);

		// BigDecimal.
		BigDecimal pnrPaxBalance = reservationTnxDao.getPNRPaxBalance(pnrPaxId);
		BigDecimal sumOfCredits = reservationCreditDao.getSumOfReservationCredits(pnrPaxId);
		
		List<ReservationCredit> creditList = new ArrayList<ReservationCredit>();
		creditList.addAll(TnxGranularityRules.getCreditsUtilizationPolicies(pnrPaxId));

		// nominal value of pnr pax balance is compared against the sum or credits....
		// If the values match do nothing..else regain or utilize credit.
		if (pnrPaxBalance.negate().compareTo(sumOfCredits) != 0) {

			// pnr pax balance is a negative value
			if (pnrPaxBalance.doubleValue() < 0) {
				// if the nominal pnrPaxBalance is greater to the sum of credit
				if (pnrPaxBalance.negate().compareTo(sumOfCredits) > 0) {
					// to get the plus value difference
					BigDecimal diff = pnrPaxBalance.negate().subtract(sumOfCredits);

					if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
						regainCredit(creditList, colReservationPaxOndPayment, diff);
					} else {
						regainCredit(creditList, diff);
					}
				}
				// if the nominal pnrPaxBalance is lesser to the sum of credit
				else {
					// get the difference
					BigDecimal diff = sumOfCredits.subtract(pnrPaxBalance.negate());
					BigDecimal d = utilizeCredit(creditList, diff, creditUtilizedMapByTnx);
					response.addResponceParam(CommandParamNames.RAC_NEW_CREDIT_BALANCE, d);
				}
			} else {
				if (sumOfCredits.doubleValue() != 0) {
					neutralizeCredit(creditList);
				}
			}
		}
		
		// validate available ReservationCredit amount with total ReservationPaxPaymentCredit
		TnxGranularityFullFillmentDeriverBO.validateAndAdjustWithReservationCredit(creditList);

		// remove zero payment credit generated records from T_PNR_PAX_PAYMENT_CREDITS
		reservationCreditDao.deleteInvalidPaxPaymentCredits();
		

		if (response.isSuccess()) {
			return response;
		} else {
			throw new ModuleException("airreservation.revacc.error");
		}

	}

	private void neutralizeCredit(List<ReservationCredit> creditList) {
		Collection<ReservationCredit> updatableReservationCredit = new ArrayList<ReservationCredit>();

		for (ReservationCredit reservationCredit : creditList) {
			if (reservationCredit.getBalance().doubleValue() != 0) {
				reservationCredit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
				updatableReservationCredit.add(reservationCredit);
			}
		}

		if (updatableReservationCredit.size() > 0) {
			reservationCreditDao.saveReservationCredit(updatableReservationCredit);
		}
	}

	private BigDecimal utilizeCredit(List<ReservationCredit> creditList, BigDecimal diff,
			Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx) {

		if (creditUtilizedMapByTnx != null && creditUtilizedMapByTnx.size() > 0) {
			Iterator<ReservationCredit> itCredits = creditList.iterator();			
			while (itCredits.hasNext()) {
				ReservationCredit credit = (ReservationCredit) itCredits.next();

				Map<String, BigDecimal> paymentCreditUtilizedMap = creditUtilizedMapByTnx.get(credit.getTnxId());

				if (paymentCreditUtilizedMap != null && paymentCreditUtilizedMap.size() > 0) {
					BigDecimal creditBalance = credit.getBalance();

					BigDecimal consumedCredit = TnxGranularityUtils.getTotalConsumedCreditAmount(paymentCreditUtilizedMap);

					BigDecimal availableBalance = AccelAeroCalculator.subtract(creditBalance, consumedCredit);

					if (availableBalance.doubleValue() > 0) {
						credit.setBalance(availableBalance);
					} else {
						credit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
					}
					
					TnxGranularityUtils.utilizeReservationPaymentCredit(credit, paymentCreditUtilizedMap);	
					
					
					// TODO - wipe of credit payment consumed group wise

					// if (diff.compareTo(creditBalance) > 0) {
					// diff = diff.subtract(creditBalance);
					// credit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
					//
					// } else {
					// if (diff.compareTo(creditBalance) == 0) {
					// credit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
					// allUtilized = true;
					// } else {
					// credit.setBalance(credit.getBalance().subtract(diff));
					// allUtilized = true;
					// }
					// }
					reservationCreditDao.saveReservationCredit(credit);

				}

			}

		} else {
			Collections.sort(creditList, new OldestFirstSortComparator());

			Iterator<ReservationCredit> itCredits = creditList.iterator();
			boolean allUtilized = false;
			while (itCredits.hasNext() && !allUtilized) {
				ReservationCredit credit = (ReservationCredit) itCredits.next();
				BigDecimal creditBalance = credit.getBalance();
				if (diff.compareTo(creditBalance) > 0) {
					diff = diff.subtract(creditBalance);
					credit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());

				} else {
					if (diff.compareTo(creditBalance) == 0) {
						credit.setBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
						allUtilized = true;
					} else {
						credit.setBalance(credit.getBalance().subtract(diff));
						allUtilized = true;
					}
				}
				reservationCreditDao.saveReservationCredit(credit);
			}
			if (!allUtilized) {
				return diff;
			}
		}

		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	private void regainCredit(List<ReservationCredit> creditList, BigDecimal diff) throws ModuleException {

		Collections.sort(creditList, new LatestFirstSortComparator());
		Iterator<ReservationCredit> itCredits = creditList.iterator();

		boolean allConsumed = false;

		while (itCredits.hasNext() && !allConsumed) {

			ReservationCredit credit = (ReservationCredit) itCredits.next();

			BigDecimal tnxAmout = reservationTnxDao.getTransaction(credit.getTnxId()).getAmount();
			// since every payment is negative, we get the nominal amount here
			tnxAmout = tnxAmout.negate();

			BigDecimal creditBalance = credit.getBalance();

			// transaction amount remainder is fully sufficient to regain the diff amount
			if (tnxAmout.subtract(creditBalance).compareTo(diff) == 0) {
				credit.setBalance(tnxAmout);
				allConsumed = true;
			} else {
				// transaction amount remainder is lesser than diff amount
				if (tnxAmout.subtract(creditBalance).compareTo(diff) < 0) {
					diff = diff.subtract(tnxAmout.subtract(creditBalance));
					credit.setBalance(tnxAmout);
				} else {
					// transaction amount remainder is greater than diff amount
					credit.setBalance(diff.add(creditBalance));
					allConsumed = true;
				}
			}
			reservationCreditDao.saveReservationCredit(credit);
		}

		if (!allConsumed) {
			throw new ModuleException("revacc.regain.inconsitent.credit");
		}
	}

	private void regainCredit(List<ReservationCredit> creditList,
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment, BigDecimal diff) throws ModuleException {

// 		TODO Nilindra July 27, We have an interesting situvation
// 		When a reservation is having an outstanding balance left. This condition will not make sense.
// 		The diff speaks with regards to the balance and the refundable amounts speaks about the current refundable amounts
// 		Therefore no point in keeping this validation
// 		Tempory commented. If it is working.. Please remove
		
//		BigDecimal totalRefundables = getTotalRefundables(colReservationPaxOndPayment, null);
//
//		if (totalRefundables.doubleValue() != diff.doubleValue()) {
//			throw new ModuleException("revacc.regain.inconsitent.credit");
//		}

		for (ReservationCredit credit : creditList) {
			BigDecimal paymentRefundables = getTotalRefundables(colReservationPaxOndPayment, credit.getTnxId(), credit);

			if (paymentRefundables.doubleValue() > 0) {
				credit.setBalance(AccelAeroCalculator.add(credit.getBalance(), paymentRefundables));
				reservationCreditDao.saveReservationCredit(credit);
			}
		}
	}

	private BigDecimal getTotalRefundables(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment, Long paymentTnxId,
			ReservationCredit credit) {
		BigDecimal totalRefundableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
			if (paymentTnxId == null || reservationPaxOndPayment.getPaymentTnxId().longValue() == paymentTnxId.longValue()) {
				totalRefundableAmount = AccelAeroCalculator.add(totalRefundableAmount, reservationPaxOndPayment.getAmount());
				if (reservationPaxOndPayment.getAmount().doubleValue() > 0) {
					credit.addPayments(reservationPaxOndPayment.transformToPaymentCredit());
				}

			}
		}

		return totalRefundableAmount;
	}

	/**
	 * private method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxId) throws ModuleException {
		if (pnrPaxId == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}

	private class OldestFirstSortComparator implements Comparator<ReservationCredit> {

		@Override
		public int compare(ReservationCredit credit, ReservationCredit compareTo) {
			return (credit.getExpiryDate() == null || compareTo.getExpiryDate() == null) ? 0 : credit.getExpiryDate().compareTo(
					compareTo.getExpiryDate());
		}
	}

	private class LatestFirstSortComparator implements Comparator<ReservationCredit> {

		@Override
		public int compare(ReservationCredit credit, ReservationCredit compareTo) {
			return (credit.getExpiryDate() == null || compareTo.getExpiryDate() == null) ? 0 : credit.getExpiryDate().compareTo(
					compareTo.getExpiryDate())
					* -1;
		}
	}
}
