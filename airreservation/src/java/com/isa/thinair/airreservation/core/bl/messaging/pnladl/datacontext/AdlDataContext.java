/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext;

import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;

/**
 * @author udithad
 *
 */
public class AdlDataContext extends BaseDataContext {

	@Override
	public String getMessageType() {
		return MessageTypes.ADL;
	}

}
