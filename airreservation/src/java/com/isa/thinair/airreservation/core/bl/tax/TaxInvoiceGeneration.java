package com.isa.thinair.airreservation.core.bl.tax;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils.DAOInstance;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import org.springframework.util.CollectionUtils;

/**
 * 
 * @author dilan
 * @isa.module.command name="taxInvoiceGeneration"
 */
public class TaxInvoiceGeneration extends DefaultBaseCommand {

	private final Log log = LogFactory.getLog(TaxInvoiceGeneration.class);

	@Override
	public ServiceResponce execute() throws ModuleException {
		Collection<String> pnrCollection = getPnrs();
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		if (!CollectionUtils.isEmpty(pnrCollection)) {
			for (String pnr : pnrCollection) {
				try {
					Reservation reservation = loadReservation(pnr);
					if (taxInvGeneratable(reservation)) {
						log.debug("==========================TAX-INVOICE==============================");
						TransactionSequencer tnxSequencer = getTransactionSequencer(reservation);
						List<TaxInvoice> taxInvoices = getTaxInvoices(reservation.getPnr());
						TaxInvoice lastTaxInvoice = getLastTaxInvoice(taxInvoices);

						boolean generateTaxInvoices = isTaxInvoiceGeneratable(tnxSequencer, lastTaxInvoice);
						List<String> invoiceNumbers = generateTaxInvoices(reservation, tnxSequencer, taxInvoices,
								generateTaxInvoices);
						log.debug("==========================TAX-INVOICE==============================");

						response.addResponceParam(CommandParamNames.TAX_INVOICE_NUMBERS, invoiceNumbers);

						log.debug("================AUDIT-TAX-INVOICE-START====================");
						auditTaxInvoice(pnr, invoiceNumbers);
						log.debug("=================AUDIT-TAX-INVOICE-END=====================");
					} else {
						log.debug("=================TAX-INVOICE-GENERATION-SKIPPED=====================");
					}
				} catch (Exception e) {
					response.setSuccess(false);
					log.error("INVOICE_GEN_ERROR::PNR=" + (pnr != null ? pnr : "null"), e);
				}
			}
		}
		return response;
	}

	private Collection<String> getPnrs() {
		Collection<String> pnrs;
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		if (StringUtils.isEmpty(pnr)) {
			pnrs = (Collection<String>) this.getParameter(CommandParamNames.PNR_LIST);
		} else {
			pnrs = Collections.singleton(pnr);
		}
		return pnrs;
	}

	private boolean taxInvGeneratable(Reservation reservation) {
		if (reservation != null) {
			for (ReservationPax pax : reservation.getPassengers()) {
				for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
					for (ReservationPaxOndCharge ondCharge : paxFare.getCharges()) {
						if (!ReservationPaxOndCharge.TAX_APPLIED_FOR_OTHER_CATEGORY.equals(ondCharge.getTaxAppliedCategory())) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	private List<String> generateTaxInvoices(Reservation reservation, TransactionSequencer tnxSequencer,
			List<TaxInvoice> taxInvoices, boolean generateTaxInvoices) throws ModuleException {
		List<String> invoiceNumbers = new ArrayList<>();
		if (generateTaxInvoices) {
			TaxInvoiceGeneratorFactory gstTxFact = new TaxInvoiceGeneratorFactory();
			for (TaxInvoiceFactory invoiceFac : gstTxFact.getGenerators(tnxSequencer.getLastPaidTnxSequence())) {
				invoiceNumbers.addAll(invoiceFac.generate(reservation, tnxSequencer.getLastPaidTnxSequence(), taxInvoices,
						(Collection<ReservationSegmentDTO>) this.getParameter(CommandParamNames.CANCEL_RESERVATION_SEGMENTS)));
			}
			log.debug("Invoice numbers :" + invoiceNumbers);
		} else {
			log.debug("No new invoices to generate");
		}
		return invoiceNumbers;
	}

	private boolean isTaxInvoiceGeneratable(TransactionSequencer tnxSequencer, TaxInvoice lastTaxInvoice) {
		boolean generateTaxInvoices = tnxSequencer.isChargesFullyPaid() && (lastTaxInvoice == null
				|| tnxSequencer.getLastPaidTnxSequence().getTnxSequence() > lastTaxInvoice.getTnxSeq());
		return generateTaxInvoices;
	}

	private List<TaxInvoice> getTaxInvoices(String pnr) {
		List<TaxInvoice> taxInvoices = DAOInstance.TAX_INVOICE_DAO.getTaxInvoiceFor(pnr);
		taxInvoices = sortInvoices(taxInvoices);
		return taxInvoices;
	}

	private TaxInvoice getLastTaxInvoice(List<TaxInvoice> taxInvoices) {
		TaxInvoice lastTaxInvoice = null;
		if (taxInvoices != null && taxInvoices.size() > 0) {
			lastTaxInvoice = taxInvoices.get(taxInvoices.size() - 1);
		}

		return lastTaxInvoice;
	}

	private TransactionSequencer getTransactionSequencer(Reservation reservation) {
		// PaymentTransactionsBasedSequencer tnxKeySequencer = new PaymentTransactionsBasedSequencer(reservation);
		ChargeTransactionsBasedSequencer tnxKeySequencer = new ChargeTransactionsBasedSequencer(reservation);
		SummarizedTnxSequencer summarizedTnxKeySequcenr = new SummarizedTnxSequencer();
		TransactionSanitizer tnxSanitizer = new TransactionSanitizer(summarizedTnxKeySequcenr);

		for (ReservationPax pax : reservation.getPassengers()) {

			if (ReservationInternalConstants.PassengerType.INFANT.equals(pax.getPaxType())
					&& !reservation.isInfantPaymentRecordedWithInfant()) {
				continue;
			}

			PaxWiseSequencer paxSequencer = new PaxWiseSequencer(tnxKeySequencer);
			TransactionSanitizer paxTnxSantizer = new TransactionSanitizer(paxSequencer);

			List<ReservationPax> paxList = new ArrayList<>();
			paxList.add(pax);
			if (!reservation.isInfantPaymentRecordedWithInfant()) {
				if (pax.hasInfant()) {
					paxList.addAll(pax.getInfants());
				}
			}
			for (ReservationPax indPax : paxList) {
				for (ReservationPaxFare fare : indPax.getPnrPaxFares()) {
					paxTnxSantizer.addCharges(fare.getCharges());
				}
				// ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO.getPNRPaxTransactions(String.valueOf(pax.getPnrPaxId()));
				Collection<ReservationTnx> reservationTnxs = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
						.getPNRPaxPayments(String.valueOf(indPax.getPnrPaxId()));
				for (ReservationTnx resTnx : reservationTnxs) {

					if (ReservationTnxNominalCode.getPaymentAndRefundAndCreditAcquireNominalCodes()
							.contains(resTnx.getNominalCode())) {
						paxTnxSantizer.addPaymentTnx(resTnx);
					}
				}
			}

			TransactionSequencer tnxSequencer = paxTnxSantizer.getTransactionSequencer();
			TransactionSequence paxLastPaidTnx = tnxSequencer.getLastPaidTnxSequence();

			if (tnxSequencer.isChargesFullyPaid()) {
				summarizedTnxKeySequcenr.processTnx(paxLastPaidTnx);
				if (paxLastPaidTnx != null) {
					tnxSanitizer.addOperationChgTnx(paxLastPaidTnx.getChargeTnx());
					tnxSanitizer.addOperationPayTnx(paxLastPaidTnx.getSnapshotPaymentTnx());
				}
			}
		}
		TransactionSequencer tnxSequencer = tnxSanitizer.getTransactionSequencer();
		if (tnxSequencer.isChargesFullyPaid()) {
			log.debug("RES FULLY PAID");
			log.debug(tnxSequencer.getLastPaidTnxSequence().toString());
		} else {
			log.debug("RES NOT FULLY PAID");
		}
		return tnxSequencer;
	}

	private Reservation loadReservation(String pnr) throws ModuleException {
		Reservation reservation = null;
		if (pnr != null) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadEtickets(true);

			reservation = ReservationProxy.getReservation(pnrModesDTO);
		}
		return reservation;
	}

	private List<TaxInvoice> sortInvoices(List<TaxInvoice> taxInvoices) {
		if (taxInvoices != null) {
			Collections.sort(taxInvoices, new Comparator<TaxInvoice>() {
				@Override
				public int compare(TaxInvoice o1, TaxInvoice o2) {
					return o1.getTaxInvoiceId().compareTo(o2.getTaxInvoiceId());
				}
			});
		}
		return taxInvoices;
	}

	private void auditTaxInvoice(String pnr, List<String> invoiceNumbers) throws ModuleException {
		if (pnr != null && invoiceNumbers != null && !invoiceNumbers.isEmpty()) {
			CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
			if (credentialsDTO != null) {
				log.debug("AUDIT TAX INVOICE: BEGIN");
				ReservationModuleUtils.getAuditorBD().doTaxInvoiceAudit(pnr, invoiceNumbers, credentialsDTO);
				log.debug("AUDIT TAX INVOICE: END");
			} else {
				log.debug("AUDITTAXINVOICE: EMPTY CREDENTIALS_DTO");
			}
		} else {
			log.debug("AUDITTAXINVOICE: EMPTY PNR OR INVOICE_NUMBER_LIST");
		}
	}

	// private String get
}
