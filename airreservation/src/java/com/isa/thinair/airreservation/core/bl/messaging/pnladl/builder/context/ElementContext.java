/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.FeaturePack;

/**
 * @author udithad
 *
 */
public class ElementContext implements Cloneable {

	private String messageType;
	private StringBuilder messageString;
	private StringBuilder currentMessageLine;
	private String[] groupCodesList;
	private Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerCount;
	private Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerReductionCount;
	private Map<String, String> pnrWiseGroupCodes;
	private Integer totalMessageLines=0;

	private Map<Integer, Integer> pnrPaxIdvsSegmentIds;
	private Map<String, Integer> fareClassWisePaxCount;
	private List<String> pnrCollection;
	private Map<Integer, String> pnrPaxVsGroupCodes;

	private String lastGroupCode;
	private FeaturePack featurePack;
	private Map<Integer, String> partNumbers;
	private Integer passengersPerPart;
	private boolean partCountExceeded=false;
	private String departureAirportCode;
	private String destinationAirportCode;
	private Map<String,Boolean> groupCodeAvailabilityMap;

	public String getLastGroupCode() {
		if (this.lastGroupCode == null) {
			lastGroupCode = "";
		}
		return lastGroupCode;
	}

	public void setLastGroupCode(String lastGroupCode) {
		this.lastGroupCode = lastGroupCode;
	}

	public ElementContext() {
		messageString = new StringBuilder();
		currentMessageLine = new StringBuilder();

		pnrPaxIdvsSegmentIds = new HashMap<Integer, Integer>();
		fareClassWisePaxCount = new HashMap<String, Integer>();
		pnrCollection = new ArrayList<String>();
		pnrPaxVsGroupCodes = new HashMap<Integer, String>();
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public StringBuilder getMessageString() {
		return messageString;
	}

	public void setMessageString(StringBuilder messageString) {
		this.messageString = messageString;
	}

	public StringBuilder getCurrentMessageLine() {
		return currentMessageLine;
	}

	public void setCurrentMessageLine(StringBuilder currentMessageLine) {
		this.currentMessageLine = currentMessageLine;
	}

	public String[] getGroupCodesList() {
		return groupCodesList;
	}

	public void setGroupCodesList(String[] groupCodesList) {
		this.groupCodesList = groupCodesList;
	}

	public Map<PassengerStoreTypes, Map<String, Integer>> getPnrWisePassengerCount() {
		return pnrWisePassengerCount;
	}

	public void setPnrWisePassengerCount(
			Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerCount) {
		this.pnrWisePassengerCount = pnrWisePassengerCount;
	}

	public Map<PassengerStoreTypes, Map<String, Integer>> getPnrWisePassengerReductionCount() {
		return pnrWisePassengerReductionCount;
	}

	public void setPnrWisePassengerReductionCount(
			Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerReductionCount) {
		this.pnrWisePassengerReductionCount = pnrWisePassengerReductionCount;
	}

	public Map<String, String> getPnrWiseGroupCodes() {
		return pnrWiseGroupCodes;
	}

	public void setPnrWiseGroupCodes(Map<String, String> pnrWiseGroupCodes) {
		this.pnrWiseGroupCodes = pnrWiseGroupCodes;
	}

	public FeaturePack getFeaturePack() {
		return featurePack;
	}

	public void setFeaturePack(FeaturePack featurePack) {
		this.featurePack = featurePack;
	}

	public Map<Integer, String> getPartNumbers() {
		return partNumbers;
	}

	public void setPartNumbers(Map<Integer, String> partNumbers) {
		this.partNumbers = partNumbers;
	}

	public Map<Integer, Integer> getPnrPaxIdvsSegmentIds() {
		return pnrPaxIdvsSegmentIds;
	}

	public void setPnrPaxIdvsSegmentIds(
			Map<Integer, Integer> pnrPaxIdvsSegmentIds) {
		this.pnrPaxIdvsSegmentIds = pnrPaxIdvsSegmentIds;
	}

	public Map<String, Integer> getFareClassWisePaxCount() {
		return fareClassWisePaxCount;
	}

	public void setFareClassWisePaxCount(
			Map<String, Integer> fareClassWisePaxCount) {
		this.fareClassWisePaxCount = fareClassWisePaxCount;
	}

	public List<String> getPnrCollection() {
		return pnrCollection;
	}

	public void setPnrCollection(List<String> pnrCollection) {
		this.pnrCollection = pnrCollection;
	}

	public Map<Integer, String> getPnrPaxVsGroupCodes() {
		return pnrPaxVsGroupCodes;
	}

	public void setPnrPaxVsGroupCodes(Map<Integer, String> pnrPaxVsGroupCodes) {
		this.pnrPaxVsGroupCodes = pnrPaxVsGroupCodes;
	}

	public Integer getPassengersPerPart() {
		return passengersPerPart;
	}

	public void setPassengersPerPart(Integer passengersPerPart) {
		this.passengersPerPart = passengersPerPart;
	}

	public boolean isPartCountExceeded() {
		return partCountExceeded;
	}

	public void setPartCountExceeded(boolean partCountExceeded) {
		this.partCountExceeded = partCountExceeded;
	}

	public Integer getTotalMessageLines() {
		return totalMessageLines;
	}

	public void setTotalMessageLines(Integer totalMessageLines) {
		this.totalMessageLines = totalMessageLines;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	public Map<String, Boolean> getGroupCodeAvailabilityMap() {
		return groupCodeAvailabilityMap;
	}

	public void setGroupCodeAvailabilityMap(
			Map<String, Boolean> groupCodeAvailabilityMap) {
		this.groupCodeAvailabilityMap = groupCodeAvailabilityMap;
	}

}
