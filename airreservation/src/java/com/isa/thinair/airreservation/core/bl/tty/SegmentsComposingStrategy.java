package com.isa.thinair.airreservation.core.bl.tty;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;

public interface SegmentsComposingStrategy {
	
	public List<BookingSegmentDTO> composeCSSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException;
	
	public List<BookingSegmentDTO> composeSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException;

}
