/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.adl.datastructure;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.dto.pnl.PNLMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.AdlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerCollectionStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PnlADlBaseDataStructure;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public class AdlMasterDataStructure<K extends BasePassengerCollection, T extends BaseDataContext>
		extends PnlADlBaseDataStructure implements
		MessageDataStructure<PassengerCollection, AdlDataContext> {

	private PassengerCollection passengerCollection = null;
	private AdlDataContext context;
	private Collection<DestinationFare> destinationFares;
	private PassengerCollectionStructure passengerCollectionStructure;
	private List<DestinationFare> dummyDestinationFares = null;

	public AdlMasterDataStructure() {
		passengerCollectionStructure = new PassengerCollectionStructure();
	}

	@Override
	public PassengerCollection getDataStructure(AdlDataContext context)
			throws ModuleException {
		this.context = context;
		populateFlightIdentityInformation(context);
		passengerCollectionStructure = new PassengerCollectionStructure();
		setSeatConfigurationElement(context);
		populateFareClassPassengerCountAndGroupCode(
				context.getDepartureAirportCode(), context.getFlightId());
		populateFareCabinRbdMap(context.getFlightId());
		destinationFares = getPnlRelatedDestinationInformation(
				context.getFlightId(), context.getDepartureAirportCode(),
				context.getFlightNumber());
		passengerCollection = passengerCollectionStructure
				.getPassengerCollection(destinationFares, cabinFareRbdMap);
		populatePnrWisePassengerCountsToPaxCollection();
		populateLastGroupCode();
		populateDummyDestinationFares();
		sortDestinationFareCollection(passengerCollection.getPnlAdlDestinationMap());
		return passengerCollection;
	}

	private void populateDummyDestinationFares() {
		for (String destinationAirportCode : destinationAirportCodes) {
			dummyDestinationFares = createDummyEntriesForNonUsedFareClasses(destinationAirportCode);
			if (dummyDestinationFares != null
					&& !dummyDestinationFares.isEmpty()) {
				if (passengerCollection.getPnlAdlDestinationMap().containsKey(
						destinationAirportCode)) {
					passengerCollection.getPnlAdlDestinationMap()
							.get(destinationAirportCode)
							.addAll(dummyDestinationFares);
				}
			}
		}
	}

	private void sortDestinationFareCollection(Map<String,List<DestinationFare>> destinationFares){
		for (Map.Entry<String, List<DestinationFare>> entry : destinationFares
				.entrySet()) {
			passengerCollectionStructure.sortDestinationFares(entry.getValue());
		}
	}
	
	private Collection<DestinationFare> getPnlRelatedDestinationInformation(
			Integer flightId, String departureAirportCode, String flightNumber)
			throws ModuleException {

		Collection<DestinationFare> destinationFares = null;
		Timestamp currentTimeStamp = new Timestamp(new Date().getTime());
		List<PassengerInformation> passengerInformation = auxilliaryDAO
				.getADLPassengerDetails(flightId, departureAirportCode,
						currentTimeStamp, context.getCarrierCode());

		List<PassengerInformation> allDeletedAdlPassengers = auxilliaryDAO
				.getDeletedADLPassengerDetails(flightId, departureAirportCode,
						currentTimeStamp, context.getCarrierCode());
		if (passengerInformation == null) {
			passengerInformation = new ArrayList<PassengerInformation>();
		}
		populateAdditionalPassengers(passengerInformation,
				allDeletedAdlPassengers);

		if (passengerInformation != null && !passengerInformation.isEmpty()) {
			destinationFares = getDestinationFareCollectionBy(passengerInformation);
		} else {
			throw new ModuleException(
					"reservationauxilliary.adl.noreservations",
					"airreservations");
		}

		return destinationFares;

	}

	private void populateAdditionalPassengers(
			List<PassengerInformation> passengerInformations,
			List<PassengerInformation> allDeletedAdlPassengers) {
		if (allDeletedAdlPassengers != null
				&& !allDeletedAdlPassengers.isEmpty()) {
			for (PassengerInformation passengerInformation : allDeletedAdlPassengers) {
				if (!passengerInformations.contains(passengerInformation)) {
					passengerInformations.add(passengerInformation);
				}
			}

		}
	}

	@SuppressWarnings("unchecked")
	private void populatePnrWisePassengerCountsToPaxCollection() {
		passengerCollection.setGroupCodes(PnlAdlUtil.populateTourIds());
		passengerCollection.setPnrWisePassengerCount(pnrWisePassengerCount);
		passengerCollection
				.setPnrWisePassengerReductionCount(pnrWisePassengerReductionCount);
		passengerCollection.setPnrPaxIdvsSegmentIds(pnrPaxIdvsSegmentIds);
		passengerCollection.setPnrCollection(pnrCollection);
		passengerCollection.setFareClassWisePaxCount(fareClassWisePaxCount);
		passengerCollection.setPassengerInformations(passengerInformations);
	}

	private void populateFareClassPassengerCountAndGroupCode(
			String departureAirportCode, Integer flightId) {
		Object[] pnlInformation = auxilliaryDAO.getPaxCountInPnlADL(flightId,
				departureAirportCode);
		fareClassPaxCountMap = (HashMap<String, Integer>) pnlInformation[0];
		lastGroupCode = (String) pnlInformation[1];
	}

	private void populateLastGroupCode() {
		passengerCollection.setLastGroupCode(lastGroupCode);
	}

}
